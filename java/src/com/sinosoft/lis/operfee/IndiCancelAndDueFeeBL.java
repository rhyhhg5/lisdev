package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.xb.PRnewDueFeeBL;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.utility.XmlExport;
import org.jdom.input.SAXBuilder;
import java.io.InputStream;
import com.sinosoft.lis.db.LPEdorAppPrintDB;
import com.sinosoft.lis.schema.LPEdorAppPrintSchema;
import com.sinosoft.utility.ListTable;
import com.sinosoft.lis.pubfun.PubSubmit;
import java.util.Set;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.bq.BQ;

/**
 * <p>Title: lis</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class IndiCancelAndDueFeeBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;  //完整的操作员信息
    private VData mInputData = null;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private LJSPaySet mLJSpaySet = new LJSPaySet();
    private LPEdorAppSchema mLPEdorAppSchema = null;  //存储保全受理信息
    private String message = "";
    private boolean mNeedDueFee = false;
    private LPEdorAppPrintSchema mLPEdorAppPrintSchema = null;
    private TransferData newGetNoticeno = new TransferData();

    public IndiCancelAndDueFeeBL(GlobalInput tGlobalInput)
    {
        mGlobalInput = tGlobalInput;
    }

    /**
     *
     * @param cInputData VData：包含
     1、	GlobalInput对象，完整的登陆用户信息
     2、	LPEdorApp对象，只需对EdorAcceptNo赋值即可。
     * @param cOperate String: 操作类型，此可为空字符串“”
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }

    private boolean dealData()
    {
        return true;
    }

    /**
     * 校验险种期交保费发生变化、发生险种增减或收费时间发生了变化
     * @return boolean
     */
    private MMap createPrtXQXCue(String edorNo)
    {
        boolean needCueFlag = false;
        ExeSQL tExeSQL = new ExeSQL ();


        ListTable tlistTable = new ListTable();
        tlistTable.setName("XQCue");

        for(int i = 1; i <= mLJSpaySet.size(); i++)
        {
            boolean flag = true;  //做了保全就一直提示

            LJSPayDB tLJSPayDB = new LJSPayDB();
            String newNo = (String)newGetNoticeno.getValueByName(mLJSpaySet.get(i).getOtherNo());
            tLJSPayDB.setGetNoticeNo(newNo);
            //新应收记录使用新应收号
            if(!tLJSPayDB.getInfo())
            {
                mErrors.addOneError("查询新的应收记录失败。");
                return null;
            }
            //应缴费和缴费截止日期发生变化
            String oldPayDate = mLJSpaySet.get(i).getPayDate();
            String newPayDate = tLJSPayDB.getPayDate();
            double dif = Math.abs(tLJSPayDB.getSumDuePayMoney()
                        - mLJSpaySet.get(i).getSumDuePayMoney());
            if(dif > 0.0001)
            {
                flag = true;
            }
            if(!oldPayDate.equals(newPayDate))
            {
                flag = true;
            }

            //险种期交保费发生变化、发生险种增减都会体现为LPEdorItem中有收退费
            String sql = "";
            //个单
            if(mLJSpaySet.get(i).getOtherNoType().equals("2"))
            {
                sql = "select getMoney from LPEdorItem where edorNo = '"
                      + edorNo+ "' "
                      + "   and getMoney != 0 "
                      + "   and contNo = '"
                      + mLJSpaySet.get(i).getOtherNo() + "' ";
            }
            //团单
            else
            {
                sql = "select getMoney from LPGrpEdorItem where edorNo = '"
                      + edorNo + "' "
                      + "   and getMoney != 0 "
                      + "   and grpContNo = '"
                      + mLJSpaySet.get(i).getOtherNo() + "' ";
            }
            String getMoney = tExeSQL.getOneValue(sql);
            if(!getMoney.equals("") && !getMoney.equals("null"))
            {
                flag = true;
            }

            if(flag)
            {
                needCueFlag = true;
                String[] strArr = new String[1];
                strArr[0] = "其它注意事项：由于本保全业务，保单号为"
                                + mLJSpaySet.get(i).getOtherNo()
                                + "应收号为" + mLJSpaySet.get(i).getGetNoticeNo()
                                + "的续保续期通知书已经作废，新的续保续期通知书为"
                                +newNo+"号，敬请留意。";
                tlistTable.add(strArr);
            }
        }

        //需要提示
        if(!needCueFlag)
        {
            return null;
        }

        XmlExport xmlExport = new XmlExport();
        try
        {
            xmlExport.setDocument(getDocument(edorNo));
        }
        catch(Exception e)
        {
            mErrors.addOneError("没有查询到通知书信息。");
            return null;
        }

        xmlExport.addListTable(tlistTable, new String[1]);
        xmlExport.addDisplayControl("displayXQCue");

        mLPEdorAppPrintSchema.setEdorInfo(xmlExport.getInputStream());
        mLPEdorAppPrintSchema.setModifyDate(PubFun.getCurrentDate());
        mLPEdorAppPrintSchema.setModifyTime(PubFun.getCurrentTime());

        MMap mMap = new MMap();
        mMap.put(mLPEdorAppPrintSchema, "BLOBUPDATE");

        return mMap;
    }

    /**
     * 把xml数据转换成Document
     * @return Document
     */
    private org.jdom.Document getDocument(String edorNo) throws Exception
    {
        InputStream xmlStream = getXmlStream(edorNo);
        SAXBuilder saxBuilder = new SAXBuilder();
        return saxBuilder.build(xmlStream);
    }

    /**
     * 从数据库中查出生成批单的xml数据
     * @return InputStream
     */
    private InputStream getXmlStream(String edorNo)
    {
        LPEdorAppPrintDB tLPEdorAppPrintDB = new LPEdorAppPrintDB();
        tLPEdorAppPrintDB.setEdorAcceptNo(edorNo);
        tLPEdorAppPrintDB.getInfo();
        mLPEdorAppPrintSchema = tLPEdorAppPrintDB.getSchema();

        return tLPEdorAppPrintDB.getEdorInfo();
    }

    /**
     * 在此方法里从LJSPay中得到需要处理的保单，
     * 逐个调用个案程序进行应收作废。
     * @return MMap：操作后的数据
     */
    public MMap getCancelData(String edorNo)
    {
        mLJSpaySet.clear();
        MMap map = new MMap();

        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LJSPay a, LPEdorMain b ")
            .append("where a.otherNo = b.contNo ")
            .append("   and a.otherNoType = '2' ")
            .append("   and b.edorNo = '")
            .append(edorNo)
            .append("' ");
        System.out.println(sql.toString());
        mLJSpaySet = new LJSPayDB().executeQuery(sql.toString());

        for(int i = 1; i <= mLJSpaySet.size(); i++)
        {
//            mNeedDueFee = true;  原来的程序判断如果有应收就会把抽档条件置成true，在后面做自动抽档。现在修改成判断保全项目的类型，如果保全项目是ZF就mNeedDueFee=false；
            LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
            String slqtype = "select * from LPEdorItem where  EdorAcceptNo ='"+edorNo+"'";
            tLPEdorItemSet = tLPEdorItemDB.executeQuery(slqtype);

            for (int k = 1 ; k<= tLPEdorItemSet.size() ; k++)
            {
                tLPEdorItemSchema = tLPEdorItemSet.get(i).getSchema();
                if(BQ.EDORTYPE_ZF.equals(tLPEdorItemSchema.getEdorType()))
                {
                    mNeedDueFee = false ;
                }
                else{
                    mNeedDueFee = true ;
                }
            }

            TransferData tTransferData= new TransferData();
            tTransferData.setNameAndValue("CancelMode","3");

            VData data = new VData();
            data.add(mLJSpaySet.get(i).getSchema());
            data.add(mGlobalInput);
            data.add(tTransferData);

            IndiLJSCancelBL bl = new IndiLJSCancelBL();
            MMap tMMap = bl.getSubmitMap(data, "INSERT");
            if(tMMap == null)
            {
                mErrors.copyAllErrors(bl.mCErrors);
            }
            map.add(tMMap);
        }

        return map;
    }

    /**
     * 得到是否需要抽档标志
     * @return boolean: 需要true
     */
    public boolean needDueFee()
    {
        return mNeedDueFee;
    }

    /**
     * 对本次保单进行续期续保催收
     * @return MMap
     */
    public boolean getPRnewDueFeeMap(String edorNo)
    {
        MMap mMap = new MMap();

        if(needDueFee())
        {
            //重抽档
            for (int i = 1; i <= this.mLJSpaySet.size(); i++) {
                System.out.println("开始续期重抽档" + mLJSpaySet.get(i).getGetNoticeNo());

                LCContSchema tLCContSchema = new LCContSchema(); // 个人保单表
                tLCContSchema.setContNo(mLJSpaySet.get(i).getOtherNo());
                //杨红于2005-07-16 添加页面输入的  起始时间   和终止时间  ，目的是传到后台作校验动作。
                TransferData tempTransferData = new TransferData();
                tempTransferData.setNameAndValue("StartDate", "1000-01-01");
                tempTransferData.setNameAndValue("EndDate", "3000-12-31");
                //qulq modify 2007-4-6 日 ，现在改为全部使用新号码，不在使用旧号了。
        //            tempTransferData.setNameAndValue(FeeConst.GETNOTICENO,
        //                                             mLJSpaySet.get(i).getGetNoticeNo());

                VData tVData = new VData();
                tVData.add(tLCContSchema);
                tVData.add(this.mGlobalInput);
                tVData.add(tempTransferData);

                PRnewDueFeeBL bl = new PRnewDueFeeBL();
                MMap tMMap = bl.getSubmitMap(tVData, "INSERT");
                if (tMMap == null) {
                    message += bl.mErrors.getFirstError() + "\n";
                    continue;
                }
                //取得新的交费记录号码 QULQ 2007-4-6
                if (tMMap != null && tMMap.keySet().size() != 0) {
                    Set set = tMMap.keySet();
                    for (int j = 0; j < set.size(); j++) {
                        Object o = null;
                        String className = "";
                        o = tMMap.getOrder().get(String.valueOf(j + 1));
                        className = o.getClass().getName();
                        if (className.indexOf("LJSPaySchema") != -1) {
                            LJSPaySchema temp = (LJSPaySchema) o;
                            newGetNoticeno.setNameAndValue(temp.getOtherNo(),
                                    temp.getGetNoticeNo());
                        }
                    }
                }
                mMap.add(tMMap);
            }
        }
        VData d = new VData();
        d.add(mMap);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            message += "保全确认及作废应收数据成功，但是生成应收数据失败，"
                + "请重到续期模块进行个案催收，或等待批处理程序抽档";
            System.out.println(p.mErrors.getErrContent());
            return false;
        }


        //生成批单提示信息
        MMap tMMap = createPrtXQXCue(edorNo);
        mMap.add(tMMap);

        d.clear();
        d.add(tMMap);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(d, ""))
        {
            message += "保全确认及作废应收数据成功，生成应收数据成功，"
                + "发生了保费或险种的变化，但生成批单提示出错";
            System.out.println(tPubSubmit.mErrors.getErrContent());
            return false;
        }

        return true;
    }

    /**
     * 得到出错信息
     * @return String
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * 得到外部传入的数据，将出入的数据获取到本类变量中。
     * @param inputData VData：传入submitData中的VData对象
     * @return boolean：操作成功true，否则false
     */
    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLPEdorAppSchema = (LPEdorAppSchema) inputData
                           .getObjectByObjectName("LPEdorAppSchema", 0);
        if(mGlobalInput == null
            || mLPEdorAppSchema == null
            || mLPEdorAppSchema.getEdorAcceptNo() == null
             || mLPEdorAppSchema.getEdorAcceptNo().equals(""))
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        return true;
    }
}
