package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

import java.util.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class LCPolQueryBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    /** 保费项表 */
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LCPolSet mLCPolSet = new LCPolSet();
    private TransferData mTransferData = new TransferData();

    public LCPolQueryBL()
    {
    }

    public static void main(String[] args)
    {
        LCPolQueryBL tLCPolQueryBL = new LCPolQueryBL();
        LCPolSchema tLCPolSchema = new LCPolSchema();

        if (tLCPolQueryBL.queryLCPol2(tLCPolSchema))
        {
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        System.out.println("---getInputData---");

        //进行业务处理
        if (!queryLCPol())
        {
            return false;
        }

        System.out.println("---queryLCPol---");

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        // 检验查询条件
        mLCPolSchema.setSchema((LCPolSchema) cInputData.getObjectByObjectName(
                "LCPolSchema", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData",
                0);

        if (mLCPolSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "请输入查询条件!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 查询个人保单表信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    public boolean queryLCPol()
    {
        //拼写SQL串,多条件查询
        String PolNo = mLCPolSchema.getPolNo();
        String GrpPolNo = mLCPolSchema.getGrpPolNo();
        String ManageCom = mLCPolSchema.getManageCom();
        String RiskCode = mLCPolSchema.getRiskCode(); //险种编码对应催交标志
        int PayIntv = mLCPolSchema.getPayIntv();
        String CurrentDate = "";
        String SubDate = "";
        String BeforeDate = "";

        if (mTransferData == null)
        {
            //下面是催收的时间范围--当天至当天+提前日期
            CurrentDate = PubFun.getCurrentDate();

            String AheadDays = ""; //催收提前天数--默认30天
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("aheaddays");

            if (tLDSysVarDB.getInfo() == false)
            {
                AheadDays = "30";
            }
            else
            {
                AheadDays = tLDSysVarDB.getSysVarValue();
            }

            //判断当前日期<=交至日期且当前日期+AheadDays>=交至日期
            FDate tD = new FDate();
            Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),
                    Integer.parseInt(AheadDays), "D", null);
            SubDate = tD.getString(AfterDate);

            Date PreDate = PubFun.calDate(tD.getDate(CurrentDate), -30, "D",
                    null);
            BeforeDate = tD.getString(PreDate);

            //因为存在已经过了催缴时间的保单-为简易操作-可以将系统变量-提前催收天数改为负数，则可以灵活处理-对个单和批量都有效
            if (tD.getDate(CurrentDate).before(tD.getDate(SubDate)))
            {
                //如果系统变量-催收天数是正数，那么SubDate大于CurrentDate，则催收范围是>=CurrentDate&&<=SubDate
                System.out.println("Current Date:" + CurrentDate);
                System.out.println("AfterDate:" + SubDate);
            }
            else
            {
                //如果系统变量-催收天数是负数，那么SubDate小于CurrentDate，则催收范围是<=CurrentDate&&>=SubDate
                //为了统一处理，则将CurrentDate和SubDate值互换,则后续催收范围保持为>=CurrentDate&&<=SubDate
                System.out.println("Current Date:" + CurrentDate);
                System.out.println("BeforeDate:" + SubDate);
                CurrentDate = SubDate;
                SubDate = PubFun.getCurrentDate();
            }
        }
        else
        {
            CurrentDate = (String) mTransferData.getValueByName("StartDate");
            SubDate = (String) mTransferData.getValueByName("EndDate");
        }

        String sqlStr = "select * from LCPol";

        if (ManageCom == null)
        {
            ManageCom = "";
        }

        if (RiskCode == null)
        {
            RiskCode = "";
        }

        if ((GrpPolNo == null) || (GrpPolNo == "")) //如果是查询个人保单（集体保单号=20个"0",则查询条件GrpPolNo设为空值）
        {
            if ((PolNo != null) && (PolNo != "")) //个单查询
            {
                sqlStr = sqlStr + " where PolNo='" + PolNo +
                    "' and AppFlag='1' ";

                if (ManageCom != "")
                {
                    String MaxManageCom = PubFun.RCh(ManageCom, "9", 8);
                    String MinManageCom = PubFun.RCh(ManageCom, "0", 8);
                    ;
                    sqlStr = sqlStr + " and ManageCom>='" + MinManageCom +
                        "' and ManageCom<='" + MaxManageCom + "'";
                }

                //0-趸交，-1=不定期交都不需要催收
                if (PayIntv == -1)
                {
                    sqlStr = sqlStr + " and PayIntv>0  ";
                }

                //sqlStr=sqlStr+" and (PaytoDate>='"+BeforeDate+"' and PaytoDate<='"+SubDate+"' and PaytoDate<PayEndDate)";
                sqlStr = sqlStr + " and  PaytoDate<PayEndDate";
                sqlStr = sqlStr +
                    " and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'";

                if (RiskCode.equals("N")) //如果险种编码对应不催交
                {
                    sqlStr = sqlStr +
                        " and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='N') ";
                }
                else if (RiskCode.equals("Y")) //如果险种编码对应催交
                {
                    sqlStr = sqlStr +
                        " and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='Y') ";
                }
            }
            else //批量查询，日期范围
            {
                //sqlStr=sqlStr+" where (PaytoDate>='"+startDate+"' and PaytoDate<='"+endDate+"' and PaytoDate<PayEndDate ) and AppFlag='1'";
                sqlStr = sqlStr + " where (PaytoDate>='" + CurrentDate +
                    "' and PaytoDate<='" + SubDate +
                    "' and PaytoDate<PayEndDate ) and AppFlag='1'";

                if (ManageCom != "")
                {
                    String MaxManageCom = PubFun.RCh(ManageCom, "9", 8);
                    String MinManageCom = PubFun.RCh(ManageCom, "0", 8);
                    sqlStr = sqlStr + " and ManageCom>='" + MinManageCom +
                        "' and ManageCom<='" + MaxManageCom + "'";
                }

                if (PayIntv == -1)
                {
                    sqlStr = sqlStr + " and PayIntv>0";
                }

                sqlStr = sqlStr +
                    " and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'";

                if (RiskCode.equals("N")) //如果险种编码对应不催交
                {
                    sqlStr = sqlStr +
                        " and RiskCode in (select RiskCode from LMRiskPay where UrgePayFlag='N') ";
                }
                else if (RiskCode.equals("Y")) //如果险种编码对应催交
                {
                    sqlStr = sqlStr +
                        " and RiskCode in (select RiskCode from LMRiskPay where UrgePayFlag='Y') ";
                }
            }
        }
        else //查询集体保单下的个人保单，则查询条件GrpPolNo不为空
        {
            sqlStr = sqlStr + " where GrpPolNo='" + GrpPolNo +
                "' and AppFlag='1'";

            if (ManageCom != "")
            {
                String MaxManageCom = PubFun.RCh(ManageCom, "9", 8);
                String MinManageCom = PubFun.RCh(ManageCom, "0", 8);
                ;
                sqlStr = sqlStr + " and ManageCom>='" + MinManageCom +
                    "' and ManageCom<='" + MaxManageCom + "'";
            }

            if (PayIntv == -1)
            {
                sqlStr = sqlStr + " and PayIntv>0";
            }

            sqlStr = sqlStr + " and (StopFlag='0' or StopFlag is null)";
            sqlStr = sqlStr + " and (PaytoDate<PayEndDate) ";

            if (RiskCode.equals("N")) //如果险种编码对应不催交
            {
                sqlStr = sqlStr +
                    " and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='N') ";
            }
            else if (RiskCode.equals("Y")) //如果险种编码对应催交
            {
                sqlStr = sqlStr +
                    " and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='Y') ";
            }
        }

        System.out.println("in BL SQL=" + sqlStr);

        LCPolDB tLCPolDB = new LCPolDB();
        mLCPolSet = tLCPolDB.executeQuery(sqlStr);

        if (tLCPolDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPolDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "保单表查询失败!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();

            return false;
        }

        if (mLCPolSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "没有查询到符合条件的保单表!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();

            return false;
        }

        mResult.clear();
        mResult.add(mLCPolSet);

        return true;
    }

    public boolean queryLCPol(LCPolSchema tLCPolSchema)
    {
        mLCPolSchema = tLCPolSchema;

        if (queryLCPol() == false)
        {
            return false;
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.clear();

        try
        {
            mResult.add(mLCPolSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 查询可以催收个人保单--不包括已经催收过的个人单--交至日期可以变化
     * @return
     */
    public boolean queryLCPol2(LCPolSchema tLCPolSchema)
    {
        if (tLCPolSchema == null)
        {
            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "queryLCPol2";
            tError.errorMessage = "保单表无效!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //拼写SQL串,多条件查询
        String ManageCom = tLCPolSchema.getManageCom();
        String RiskCode = tLCPolSchema.getRiskCode();
        String CurrentDate = tLCPolSchema.getPaytoDate(); //存放那天可以催收的日期
        String PolNo = tLCPolSchema.getPolNo();
        int PayIntv = tLCPolSchema.getPayIntv();

        //默认是系统第二天
        if (CurrentDate == null)
        {
            CurrentDate = PubFun.getCurrentDate();
        }

        String AheadDays = ""; //催收提前天数--默认30天
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("aheaddays");

        if (tLDSysVarDB.getInfo() == false)
        {
            AheadDays = "30";
        }
        else
        {
            AheadDays = tLDSysVarDB.getSysVarValue();
        }

        //判断当前日期<=交至日期且当前日期+AheadDays>=交至日期
        FDate tD = new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),
                Integer.parseInt(AheadDays), "D", null);
        String SubDate = tD.getString(AfterDate);

        //因为存在已经过了催缴时间的保单-为简易操作-可以将系统变量-提前催收天数改为负数，则可以灵活处理-对个单和批量都有效
        if (tD.getDate(CurrentDate).before(tD.getDate(SubDate)))
        {
            //如果系统变量-催收天数是正数，那么SubDate大于CurrentDate，则催收范围是>=CurrentDate&&<=SubDate
            System.out.println("Current Date:" + CurrentDate);
            System.out.println("AfterDate:" + SubDate);
        }
        else
        {
            //如果系统变量-催收天数是负数，那么SubDate小于CurrentDate，则催收范围是<=CurrentDate&&>=SubDate
            //为了统一处理，则将CurrentDate和SubDate值互换,则后续催收范围保持为>=CurrentDate&&<=SubDate
            System.out.println("Current Date:" + CurrentDate);
            System.out.println("BeforeDate:" + SubDate);
            CurrentDate = SubDate;
            SubDate = PubFun.getCurrentDate();
        }
        String sqlStr = "select GrpContNo,GrpPolNo,ContNo, PolNo, ProposalNo, PrtNo,ContType,PolTypeFlag,MainPolNo, MasterPolNo, KindCode, RiskCode, RiskVersion, ManageCom, AgentCom, AgentType,getUniteCode(AgentCode) AgentCode,AgentGroup,AgentCode1,SaleChnl, Handler, InsuredNo,InsuredName, InsuredSex, InsuredBirthday, InsuredAppAge, InsuredPeoples,OccupationType, AppntNo, AppntName, CValiDate, SignCom,SignDate,SignTime, FirstPayDate, PayEndDate, PaytoDate, GetStartDate, EndDate, AcciEndDate,GetYearFlag,GetYear, PayEndYearFlag, PayEndYear, InsuYearFlag,InsuYear,AcciYearFlag,AcciYear, GetStartType,SpecifyValiDate, PayMode,PayLocation, PayIntv, PayYears, Years, ManageFeeRate, FloatRate, PremToAmnt, Mult, StandPrem, Prem, SumPrem, Amnt, RiskAmnt, LeavingMoney, EndorseTimes, ClaimTimes, LiveTimes, RenewCount,LastGetDate, LastLoanDate, LastRegetDate, LastEdorDate, LastRevDate, RnewFlag, StopFlag, ExpiryFlag, AutoPayFlag, InterestDifFlag,SubFlag, BnfFlag, HealthCheckFlag,ImpartFlag, ReinsureFlag, AgentPayFlag, AgentGetFlag, LiveGetMode, DeadGetMode, BonusGetMode, BonusMan, DeadFlag, SmokeFlag, Remark, ApproveFlag, ApproveCode, ApproveDate, ApproveTime, UWFlag,UWCode,UWDate, UWTime,PolApplyDate, AppFlag, PolState, StandbyFlag1,StandbyFlag2, StandbyFlag3, Operator, MakeDate, MakeTime, ModifyDate, ModifyTime, WaitPeriod, PayRuleCode, AscriptionRuleCode, SaleChnlDetail,RiskSeqNo, Copys, ComFeeRate,BranchFeeRate,ProposalContNo,ContPlanCode,CessAmnt,StateFlag,SupplementaryPrem,AccType,InitFeeRate,ExPayMode from lcpol" + " where paytodate<payenddate" +
            " and exists (select riskcode from lmriskpay WHERE urgepayflag='Y' and lmriskpay.riskcode=lcpol.riskcode)" +
            " and (StopFlag='0' or StopFlag is null)" +
            " and not exists (SELECT polno FROM LJSPayPerson WHERE  LJSPayPerson.polno=lcpol.polno)" +
            " and paytodate>='" + CurrentDate + "' and paytodate<='" + SubDate +
            "' and appflag='1'" + " and grppolno='00000000000000000000'" +
            " and ManageCom like '" + ManageCom + "%'" + " and payintv>0";


        if ((PayIntv != -1) && (PayIntv != 0))
        {
            sqlStr = sqlStr + " and PayIntv=" + PayIntv;
        }

        //        else
        //        {
        //            sqlStr = sqlStr + " and PayIntv=" + PayIntv + "";
        //        }
        if (PolNo != null)
        {
            sqlStr = sqlStr + " and PolNo='" + PolNo + "'";
        }

        if (RiskCode != null)
        {
            sqlStr = sqlStr + " and riskcode='" + RiskCode + "'";
        }

        //        else
        //        {
        //            sqlStr = sqlStr +
        //                " and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')";
        //        }
        //        sqlStr = sqlStr +
        //            " and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'";
        //        sqlStr = sqlStr +
        //            " and PolNo not in (select OtherNo from ljspay where OtherNoType='2')";
        System.out.println("in BL SQL=" + sqlStr);

        LCPolDB tLCPolDB = new LCPolDB();
        mLCPolSet = tLCPolDB.executeQuery(sqlStr);

        if (tLCPolDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPolDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "保单表查询失败!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();

            return false;
        }

        if (mLCPolSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "没有查询到符合条件的保单表!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();

            return false;
        }

        mResult.clear();
        mResult.add(mLCPolSet);

        return true;
    }

    /**
     * 查询可以催收个人保单--交至日期可以变化
     * @return
     */
    public boolean queryLCPol3(LCPolSchema tLCPolSchema)
    {
        if (tLCPolSchema == null)
        {
            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "queryLCPol2";
            tError.errorMessage = "保单表无效!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //拼写SQL串,多条件查询
        String ManageCom = tLCPolSchema.getManageCom();
        String RiskCode = tLCPolSchema.getRiskCode();
        String CurrentDate = tLCPolSchema.getPaytoDate(); //存放那天可以催收的日期
        String PolNo = tLCPolSchema.getPolNo();
        int PayIntv = tLCPolSchema.getPayIntv();

        //默认是系统第二天
        if (CurrentDate == null)
        {
            CurrentDate = PubFun.getCurrentDate();
        }

        String AheadDays = ""; //催收提前天数--默认30天
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("aheaddays");

        if (tLDSysVarDB.getInfo() == false)
        {
            AheadDays = "30";
        }
        else
        {
            AheadDays = tLDSysVarDB.getSysVarValue();
        }

        //判断当前日期<=交至日期且当前日期+AheadDays>=交至日期
        FDate tD = new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),
                Integer.parseInt(AheadDays), "D", null);
        String SubDate = tD.getString(AfterDate);

        //因为存在已经过了催缴时间的保单-为简易操作-可以将系统变量-提前催收天数改为负数，则可以灵活处理-对个单和批量都有效
        if (tD.getDate(CurrentDate).before(tD.getDate(SubDate)))
        {
            //如果系统变量-催收天数是正数，那么SubDate大于CurrentDate，则催收范围是>=CurrentDate&&<=SubDate
            System.out.println("Current Date:" + CurrentDate);
            System.out.println("AfterDate:" + SubDate);
        }
        else
        {
            //如果系统变量-催收天数是负数，那么SubDate小于CurrentDate，则催收范围是<=CurrentDate&&>=SubDate
            //为了统一处理，则将CurrentDate和SubDate值互换,则后续催收范围保持为>=CurrentDate&&<=SubDate
            System.out.println("Current Date:" + CurrentDate);
            System.out.println("BeforeDate:" + SubDate);
            CurrentDate = SubDate;
            SubDate = PubFun.getCurrentDate();
        }

        String sqlStr = "select * from LCPol";
        sqlStr = sqlStr + " where (PaytoDate>='" + CurrentDate +
            "' and PaytoDate<='" + SubDate +
            "' and PaytoDate<PayEndDate ) and AppFlag='1'";

        if (ManageCom != null)
        {
            String MaxManageCom = PubFun.RCh(ManageCom, "9", 8);
            String MinManageCom = PubFun.RCh(ManageCom, "0", 8);
            sqlStr = sqlStr + " and ManageCom>='" + MinManageCom +
                "' and ManageCom<='" + MaxManageCom + "'";
        }

        if ((PayIntv == -1) || (PayIntv == 0))
        {
            sqlStr = sqlStr + " and PayIntv>0";
        }
        else
        {
            sqlStr = sqlStr + " and PayIntv=" + PayIntv + "";
        }

        if (PolNo != null)
        {
            sqlStr = sqlStr + " and PolNo='" + PolNo + "'";
        }

        if (RiskCode != null)
        {
            sqlStr = sqlStr +
                " and 1=(select 1 from LMRiskPay where UrgePayFlag='Y' and riskcode='" +
                RiskCode + "') ";
        }
        else
        {
            sqlStr = sqlStr +
                " and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')";
        }

        sqlStr = sqlStr +
            " and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'";

        System.out.println("in BL SQL=" + sqlStr);

        LCPolDB tLCPolDB = new LCPolDB();
        mLCPolSet = tLCPolDB.executeQuery(sqlStr);

        if (tLCPolDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPolDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "保单表查询失败!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();

            return false;
        }

        if (mLCPolSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "没有查询到符合条件的保单表!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();

            return false;
        }

        mResult.clear();
        mResult.add(mLCPolSet);

        return true;
    }
    public boolean queryLCPol4(LCContSchema tLCContSchema)
    {
        if (tLCContSchema == null)
        {
            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "queryLCPol2";
            tError.errorMessage = "保单表无效!";
            this.mErrors.addOneError(tError);

            return false;
        }
         String CurrentDate="";
        //拼写SQL串,多条件查询
        String ManageCom = tLCContSchema.getManageCom();
        //是否支持录入催收时间待考虑
        //String CurrentDate = tLCPolSchema.getPaytoDate(); //存放那天可以催收的日期
        String ContNo = tLCContSchema.getContNo();
        //默认是系统第二天
        if (CurrentDate == null||CurrentDate.equals(""))
        {
            CurrentDate = PubFun.getCurrentDate();
        }

        String AheadDays = ""; //催收提前天数--默认30天
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("aheaddays");

        if (tLDSysVarDB.getInfo() == false)
        {
            AheadDays = "30";
        }
        else
        {
            AheadDays = tLDSysVarDB.getSysVarValue();
        }
        //判断当前日期<=交至日期且当前日期+AheadDays>=交至日期
        FDate tD = new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate = tD.getString(AfterDate);
        //因为存在已经过了催缴时间的保单-为简易操作-可以将系统变量-提前催收天数改为负数，则可以灵活处理-对个单和批量都有效
        if (tD.getDate(CurrentDate).before(tD.getDate(SubDate)))
        {
            //如果系统变量-催收天数是正数，那么SubDate大于CurrentDate，则催收范围是>=CurrentDate&&<=SubDate
            System.out.println("Current Date:" + CurrentDate);
            System.out.println("AfterDate:" + SubDate);
        }
        else
        {
            //如果系统变量-催收天数是负数，那么SubDate小于CurrentDate，则催收范围是<=CurrentDate&&>=SubDate
            //为了统一处理，则将CurrentDate和SubDate值互换,则后续催收范围保持为>=CurrentDate&&<=SubDate
            System.out.println("Current Date:" + CurrentDate);
            System.out.println("BeforeDate:" + SubDate);
            CurrentDate = SubDate;
            SubDate = PubFun.getCurrentDate();
        }
        String sqlStr = "select * from lcpol" + " where paytodate<payenddate" +
            " and exists (select riskcode from lmriskpay WHERE urgepayflag='Y' and lmriskpay.riskcode=lcpol.riskcode)" +
            " and (StopFlag='0' or StopFlag is null)" +
            " and not exists (SELECT polno FROM ljspayperson WHERE  ljspayperson.polno=lcpol.polno)" +
            " and paytodate>='" + CurrentDate + "' and paytodate<='" + SubDate +
            "' and appflag='1'" + " and grppolno='00000000000000000000'" +
            " and ManageCom like '" + ManageCom + "%'" + " and payintv>0"+
            " and ContNo='"+ContNo+"'";

        //        else
        //        {
        //            sqlStr = sqlStr +
        //                " and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')";
        //        }
        //        sqlStr = sqlStr +
        //            " and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'";
        //        sqlStr = sqlStr +
        //            " and PolNo not in (select OtherNo from ljspay where OtherNoType='2')";
        System.out.println("in BL SQL=" + sqlStr);

        LCPolDB tLCPolDB = new LCPolDB();
        mLCPolSet = tLCPolDB.executeQuery(sqlStr);

        if (tLCPolDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPolDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "保单表查询失败!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();

            return false;
        }

        if (mLCPolSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "没有查询到符合条件的保单表!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();

            return false;
        }

        mResult.clear();
        mResult.add(mLCPolSet);

        return true;
    }

}
