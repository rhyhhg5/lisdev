package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LCGrpAddressDB;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.vschema.LCGrpAddressSet;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.f1print.PrintService;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpPayComptPrintBL  implements PrintService {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private SSRS mSSRS;
    private String mGrpContNo = ""; //应收止期，批量核销
    private LJAPaySchema tLJAPaySchema; //单号，个案核销
    private String mVerifyType = ""; //1：批量核销；2：个案核销
    TextTag textTag = new TextTag();
    private LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    private LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private String mManageCom = "";
    private String mManageName = "";
    private String mPayNo = ""; //收据号
    private int needPrt = -1;//判断是否需要打印,0-需要打印

    public GrpPayComptPrintBL() {
//        try {
//            jbInit();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        if (!dealPrintMag()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            LOPRTManagerSchema xLOPRTManagerSchema = new LOPRTManagerSchema();
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);

            xLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
            if(xLOPRTManagerSchema == null)
            {
                tLJAPaySchema = (LJAPaySchema) cInputData.getObjectByObjectName("LJAPaySchema", 0);
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~"+tLJAPaySchema.getPayNo());
            }
            else
            {
                LJAPayDB tLJAPayDB = new LJAPayDB();
                mLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
                tLJAPayDB.setPayNo(mLOPRTManagerSchema.getStandbyFlag2());
                if(!tLJAPayDB.getInfo())
                {
                     mErrors.addOneError("传入的数据不完整。");
                     return false;
                }

                tLJAPaySchema = tLJAPayDB.getSchema();
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~"+tLJAPaySchema.getPayNo());
            }

        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }

        return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {

        TextTag tag = new TextTag();
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpPayComPrint.vts", "printer"); //最好紧接着就初始化xml文档

        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setPayNo(tLJAPaySchema.getPayNo());
        if (!tLJAPayDB.getInfo()) {
            CError.buildErr(this, "实收表缺少数据");
            return false;
        }

        mPayNo = tLJAPayDB.getPayNo();
        mManageCom = tLJAPayDB.getManageCom();
        String GrpContNo = tLJAPayDB.getIncomeNo();
        textTag.add("CreateDate", tLJAPayDB.getMakeDate());

        mGrpContNo = GrpContNo;
        System.out.println("GrpContNo=" + GrpContNo);
        String sql = "";
        String sqlone = "";
        //获取团体保单信息
        sqlone = "select * from lcgrpcont where grpcontno='" + mGrpContNo + "'";
        System.out.println("sqlone=" + sqlone);
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(sqlone);
        if (tLCGrpContSet.size() == 0) {
            CError.buildErr(this, "集体保单表缺少数据");
            return false;
        }

        tLCGrpContSchema = tLCGrpContSet.get(1).getSchema();

        //设置公司地址等信息
        if (!setFixedInfo()) {
            CError.buildErr(this,
                            "机构信息表缺少数据");
            return false;

        }

        textTag.add("GrpContNo", tLCGrpContSchema.getGrpContNo());
        textTag.add("CValiDate", tLCGrpContSchema.getCValiDate());

        String showPaymode = "";
        String showPayintv = "";
        switch (Integer.parseInt(tLCGrpContSchema.getPayMode())) {
        case 1:
            showPaymode = "现金";
            break;

        case 2:
            showPaymode = "现金支票";
            break;
        case 3:
            showPaymode = "转帐支票";
            break;

        case 4:
            showPaymode = "银行转账";
            break;

        case 5:
            showPaymode = "内部转帐";
            break;
        case 6:
            showPaymode = "银行托收";
            break;
        case 7:
            showPaymode = "业务员信用卡";
            break;
        case 9:
            showPaymode = "其他";
            break;
        }
        ;

        //交费频次
        showPayintv = Integer.toString(tLCGrpContSchema.getPayIntv());
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("payintv");
        tLDCodeDB.setCode(showPayintv);

        if (!(tLDCodeDB.getInfo())) {
            CError.buildErr(this, "集体保单表缺少数据");
            return false;
        }

        //交费频次，交费方式设置
        textTag.add("PayIntv", tLDCodeDB.getCodeName());
        textTag.add("PayMode", showPaymode);

        //获取投保人地址
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(GrpContNo);

        if (!tLCGrpAppntDB.getInfo()) {
            CError.buildErr(this,
                            "团单投保人表中缺少数据");
            return false;
        }
        sql = "select * from LCGrpAddress where CustomerNo=(select appntno from LCGrpCont where grpcontno='" +
              GrpContNo + "') AND AddressNo = '" + tLCGrpAppntDB.getAddressNo() +
              "'";
        System.out.println("sql=" + sql);
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        LCGrpAddressSet tLCGrpAddressSet = tLCGrpAddressDB.executeQuery(sql);
        if (tLCGrpAddressSet.size() == 0) {
            CError.buildErr(this,
                            "团体客户地址表中缺少数据");
            return false;
        }

        tLCGrpAddressSchema = tLCGrpAddressSet.get(tLCGrpAddressSet.size()).
                              getSchema();

        textTag.add("GrpZipCode", tLCGrpAddressSchema.getGrpZipCode());
        textTag.add("GrpAddress", tLCGrpAddressSchema.getGrpAddress());
        textTag.add("LinkMan1", tLCGrpAddressSchema.getLinkMan1());
        String appntPhoneStr = "";
        if (tLCGrpAddressSchema.getPhone1() != null)
        {
            appntPhoneStr = tLCGrpAddressSchema.getPhone1();
        } else if (tLCGrpAddressSchema.getMobile1() != null)
        {
            appntPhoneStr = tLCGrpAddressSchema.getMobile1();
        }
        textTag.add("AppntPhone", appntPhoneStr);
        textTag.add("AppntNo", tLCGrpContSchema.getAppntNo());
        textTag.add("GrpName", tLCGrpContSchema.getGrpName());
        textTag.add("PayNo", tLJAPayDB.getPayNo());
        textTag.add("SumDuePayMoney", CommonBL.bigDoubleToCommonString(
                tLJAPayDB.getSumActuPayMoney(), "0.00"));
        textTag.add("AccGetBala", getSumDuePayMoney(tLJAPayDB, "YEL"));
        textTag.add("AccBala", getAccBala(tLCGrpContSchema.getAppntNo()));
        textTag.add("EnterAccDate", tLJAPayDB.getEnterAccDate());
        textTag.add("GetNoticeNo", tLJAPayDB.getGetNoticeNo());

//        String money = CommonBL.bigDoubleToCommonString(
//            tLJAPayDB.getSumActuPayMoney(), "0.00");
        String money = getSumDuePayMoney(tLJAPayDB, "ZC");
        textTag.add("ActuDuePayMoney", money);

        textTag.add("BarCode1", tLJAPayDB.getPayNo());
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        System.out.println("ActuGetNo" + tLJAPayDB.getPayNo());

        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }

        tag.add("ManageName", mManageName);
        xmlexport.addTextTag(tag);

        String[] title = {"序号", "交费性质", "保费范围", "应收保费", "实交金额",
                         "交费时间", "交费方式", "欠交保费"};
        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        xmlexport.addListTable(getListTable(), title);
        mResult = new VData();
        mResult.addElement(xmlexport);
        xmlexport.outputDocumentToFile("C:\\","GrpPayCompPrintBLhuxl");
        //放入打印列表
        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setStandbyFlag2(tLJAPayDB.getPayNo());
        tLOPRTManagerDB.setCode("90");
        tLOPRTManagerSet = tLOPRTManagerDB.query();
        needPrt = tLOPRTManagerSet.size();
        if(needPrt==0)
        {//没有数据,进行封装
            String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            mLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
            mLOPRTManagerSchema.setOtherNoType("01");
            mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_PAY);
            mLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
            mLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
            mLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
            mLOPRTManagerSchema.setReqOperator(tLCGrpContSchema.getOperator());
            mLOPRTManagerSchema.setPrtType("0");
            mLOPRTManagerSchema.setStateFlag("0");
            mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            mLOPRTManagerSchema.setStandbyFlag2(tLJAPayDB.getPayNo()); //这里存放 （也为交费收据号）
        }
        return true;
    }

    /**
     * 查询投保人账户余额
     * @param appntNo String
     * @return String
     */
    private String getAccBala(String customerNo) {
        StringBuffer sql = new StringBuffer();
        sql.append("select accBala ")
                .append("from LCAppAcc ")
                .append("where customerNo = '")
                .append(customerNo)
                .append("' ");
        String bala = new ExeSQL().getOneValue(sql.toString());
        if (bala.equals("") || bala.equals("null")) {
            return "0.00";
        } else {
            return CommonBL.bigDoubleToCommonString(Double.parseDouble(bala),
                    "0.00");
        }
    }

    /**
     * 计算总应收
     * @param tLJAPayDB LJAPayDB
     * @return String
     */
    private String getSumDuePayMoney(LJAPayDB tLJAPayDB, String payType) {
        StringBuffer sql = new StringBuffer();
        sql.append("select sum(abs(sumDuePayMoney)) ")
                .append("from LJAPayGrp ")
                .append("where payNo = '")
                .append(tLJAPayDB.getPayNo())
                .append("'  and payType = '").append(payType).append("' ");
        String money = new ExeSQL().getOneValue(sql.toString());
        if (money.equals("") || money.equals("null")) {
            return "0.00";
        } else {
            money = CommonBL.bigDoubleToCommonString(Double.parseDouble(money),
                    "0.00");
        }
        return money;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {

        String peoples2;
        if (isWMD(tLCGrpContSchema.getGrpContNo())) {
            peoples2 = " (select peoples2Input from LCNoNamePremTrace "
                       + "where getNoticeNo = b.getNoticeNo "
                       + "   and contPlanCode = a.contPlanCode) ";
        } else {
            peoples2 = " count(distinct a.insuredNo) ";
        }

        StringBuffer sqlSB = new StringBuffer();
        sqlSB.append("select a.contPlanCode, ")
                .append(peoples2)
                .append(
                ", rtrim(char(min(b.LastPayToDate)))||'~'||rtrim(char(min(b.CurPayToDate))), ")
                .append(
                " varchar(sum(b.sumActuPayMoney)), varchar(sum(b.sumActuPayMoney)) ")
                .append("from LCPol a, LJAPayPerson b ")
                .append("where a.polNo = b.polNo ")
                .append("and b.payNo = '")
                .append(mPayNo)
                .append("' group by a.contPlanCode, b.getNoticeNo ");

//           sqlSB.append("select rtrim(char(a.LastPayToDate))||' '||rtrim(char(a.CurPayToDate)),")
//                   .append(" sum(a.SumDuePayMoney),sum(a.SumActuPayMoney),b.PayDate")
//                   .append(" ,(select codename from ldcode where codetype='paymode' and code=")
//                   .append(" (select PayMode from lcgrpcont where grpContNo=a.GrpContNo)),''")
//                   .append("  from LJAPayGrp a ,LJAPay b ")
//                   .append(" where a.PayNo=b.PayNo")
//                   .append(" and b.GetNoticeNo in (select GetNoticeNo from LJSPayB WHERE OtherNoType='1' and DealState='1')")
//                   .append(" and a.PayNo='")
//                   .append(mPayNo)
//                   .append("'")
//                   .append(" group by a.LastPayToDate,a.CurPayToDate,b.PayDate,a.GrpContNo,a.PayNo")
//                   .append(" order by a.PayNo desc")
//                   ;

        String tSQL = sqlSB.toString();
        System.out.println(tSQL);
        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[8];
            info[0] = Integer.toString(i);
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {

                info[j] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }

        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 校验保单是否无名单
     * @param grpContNo String
     * @return boolean
     */
    private boolean isWMD(String grpContNo) {
        StringBuffer sql = new StringBuffer();
        sql.append("select 1 ")
                .append("from LCCont ")
                .append("where polType = '1' ")
                .append("   and grpContNo = '")
                .append(grpContNo)
                .append("' ");
        String t = new ExeSQL().getOneValue(sql.toString());
        if (t.equals("") || t.equals("null")) {
            return false;
        }

        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
    public CErrors getErrors() {
        return mErrors;
    }

    public static void main(String[] args) {
        GrpPayComptPrintBL p = new GrpPayComptPrintBL();
        String tPayNo = "32000013048";
        GlobalInput mGI = new GlobalInput();
        mGI.Operator = "wuser";
        mGI.ComCode = "86";
        mGI.ManageCom = "86";

        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        tLJAPaySchema.setPayNo(tPayNo);
        VData v = new VData();
        v.addElement(mGI);
        v.addElement(tLJAPaySchema);

        if (!p.submitData(v, "PRINT")) {
            System.out.println(p.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }
    }

    private void jbInit() throws Exception {
    }

    //查询公司名称
    private String getCompanyName() {
        //通过LDCode得到保单号，然后得到公司名称
        String manageCode = mGlobalInput.ManageCom;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.setCode(manageCode);
        LDCodeSet set = tLDCodeDB.query();
        if (set == null || set.size() == 0) {

            mErrors.addOneError("没有查到公司名");
            System.out.println("没有查到公司名" + tLDCodeDB.mErrors.getErrContent());
            return null;
        } else {
            return set.get(1).getCodeName();
        }
    }

    /**
     * 设置i通知书的公司信息等
     */
    private boolean setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mManageCom);
        if (!tLDComDB.getInfo()) {
            CError.buildErr(this,
                            "管理结构表中缺少数据");
            return false;
        }
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        mManageName = tLDComDB.getLetterServiceName();
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        textTag.add("Fax", tLDComDB.getFax());
        textTag.add("MakeDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        //textTag.add("Operator", mGlobalInput.Operator);

        //业务员信息
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCGrpContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        textTag.add("Phone", tLaAgentDB.getPhone());
        textTag.add("XI_ManageCom", mManageCom);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tLCGrpContSchema.getAgentGroup());
        tLABranchGroupDB.getInfo();
        textTag.add("AgentGroup", tLABranchGroupDB.getName());

        return true;
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        if(needPrt==0)
        {
            MMap tMap = new MMap();
            tMap.put(mLOPRTManagerSchema, "INSERT");
            VData tInputData = new VData();
            tInputData.add(tMap);
            PubSubmit tPubSubmit = new PubSubmit();
            if (tPubSubmit.submitData(tInputData, "") == false) {
                this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
                return false;
            }
        }
        return true;
    }


}
