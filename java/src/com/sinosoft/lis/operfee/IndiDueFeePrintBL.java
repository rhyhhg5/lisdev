package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.vschema.LDCodeSet;

//程序名称：IndiDueFeePrintBL.java
//程序功能：打印、下载应收清单
//创建日期：2008-01-07
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class IndiDueFeePrintBL {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
   
    private TransferData mTransferData = new TransferData();
    
    private XmlExport xmlexport = null;
    
    private VData mResult = null;
    
    private SSRS mSSRS;

    private String mCurrentTime;
    
    private String mSubTime;
    
    private String mContNo;
    
    private String mManageCom;
    
    private String mAgentCode;
    
    private String mAppntNo;
   
    public IndiDueFeePrintBL() 
    {
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {
        //取得外部传入数据
        if (!getInputData(cInputData)) 
        {
            return false;
        }

        //打印所需数据
        if (!printData()) 
        {
            return false;
        }
        return true;
    }

//  取得外部传入数据
    private boolean getInputData(VData cInputData) 
    {
        try 
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        } 
        catch (Exception e) 
        {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }
        mCurrentTime = (String) mTransferData.getValueByName("CurrentTime");
        mSubTime = (String) mTransferData.getValueByName("SubTime");
        mContNo = (String) mTransferData.getValueByName("ContNo");
        mManageCom = (String) mTransferData.getValueByName("ManageCom");
        mAgentCode = (String) mTransferData.getValueByName("AgentCode");
        mAppntNo = (String) mTransferData.getValueByName("AppntNo");
        return true;
    }

    //  打印所需数据
    private boolean printData() 
    {
        TextTag tag = new TextTag();
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("IndiDueFee.vts", "printer"); //初始化xml文档

        //得到公司名
        String grpName = this.getCompanyName();
        System.out.println("ManageName" + grpName);
        if (grpName == null) 
        {
            return false;
        }
        tag.add("ManageName", grpName);
        tag.add("PrintOperator", mGlobalInput.Operator);
        tag.add("PrintDate", PubFun.getCurrentDate());

        xmlexport.addTextTag(tag);

        String[] title = {"保单号", "印刷号", "投保人", "特别约定有无", "生效时间","已交保费合计","余额", "应交保费", "应收时间","收费方式", "管理机构","代理人编码","联系电话","联系地址"};
        xmlexport.addListTable(getListTable(), title);
       // xmlexport.outputDocumentToFile("C:\\", "aa");
        mResult = new VData();
        mResult.addElement(xmlexport);
        return true;
    }

    // 获取列表数据
    private ListTable getListTable() 
    {
        ListTable tListTable = new ListTable();
        String sql = "select ContNo,PrtNo,AppntName,'',CValiDate,SumPrem,nvl((select AccGetMoney from LCAppAcc where CustomerNo=a.appntno),0),prem,paytodate,(select codename from ldcode where codetype='paymode' and code=PayMode),ShowManageName(ManageCom), "
        	       + "(select Name from LABranchGroup where AgentGroup = a.AgentGroup), AgentCode, "                                                                                                                                                                                                                                                                               
	               + "case when e.Mobile is null then '' else e.mobile end ||' '||case when e.CompanyPhone is null then '' else e.CompanyPhone end ||' '|| case when e.HomePhone is null then '' else e.homephone end, "
	        	   + "e.PostalAddress "
	               + "from LCCont a, lcaddress e " 
	        	   + "where AppFlag='1' and (StateFlag is null or StateFlag = '1') " 
	        	   + "and a.appntno =e.customerno "  
	        	   + "and e.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) "
		           +" and ContNo not in (select otherno from ljspay where othernotype='2') "  
		           +" and (cardflag='0' or cardflag is null) "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
		           +" and ContNo in (select ContNo from LCPol where "       
		           //续期                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
		           +" ( paytodate<payenddate "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
		           +"   and payintv>0 "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
		           +"   and exists (select riskcode from lmrisk WHERE cPayFlag='Y' and lmrisk.riskcode=lcpol.riskcode) "                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		           + "  or  "      
		           //续保                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
		           +"   exists (select riskcode from lmrisk WHERE rnewFlag!='N' and lmrisk.riskcode=lcpol.riskcode) "                                                                                                                                                                                                                                                                                                                                                                                                                                                                
		           +"   and exists (select riskCode from LMRiskApp where riskCode = LCPol.riskCode and riskType5 = '2' and (endDate > LCPol.endDate or endDate is null)) "
		           +"   and not exists(select contNo from LCRnewStateLog where contNo = a.contNo and state in('4','5')) "
		           +" )"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		           + " and contno=a.contno"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
		           +" and (polstate is null or (polstate is not null and polstate not like '02%' and polstate not like '03%'))"                                                                                                                                                                                                                                                                                                                                                                                                                                                  
		           +" and PolNo not in (select PolNo from LJSPayPerson where contNo = a.contNo) "                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
		           +" and (StopFlag='0' or StopFlag is null) "	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
		           +" and grppolno = '00000000000000000000' "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
		           +" and managecom like '"+mManageCom+"%' "			                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
		           +" and paytodate>='" + mCurrentTime + "' and paytodate<='" + mSubTime +"') ";
        if(!("".equals(mContNo) || "null".equals(mContNo) || mContNo == null))
        {
        	sql = sql + "and ContNo = '" + mContNo + "' ";
        }
        if(!("".equals(mAgentCode) || "null".equals(mAgentCode) || mAgentCode == null))
        {
        	sql = sql + "and AgentCode = '" + mAgentCode + "' ";
        }
        if(!("".equals(mAppntNo) || "null".equals(mAppntNo) || mAppntNo == null))
        {
        	sql = sql + "and AppntNo = '" + mAppntNo + "' ";
        }
        sql = sql + "order by ContNo with ur";
        System.out.println(sql);
        mSSRS = new ExeSQL().execSQL(sql);
       
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) 
        {
            String[] info = new String[15];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) 
            {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("ZT");
        return tListTable;
    }

   
    // 返回清单数据
    public VData getResult() 
    {
        return mResult;
    }

    //查询公司名称
    private String getCompanyName() 
    {
        //通过LDCode得到保单号，然后得到公司名称
        String manageCode = mGlobalInput.ManageCom;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.setCode(manageCode);
        LDCodeSet set = tLDCodeDB.query();
        if (set == null || set.size() == 0) 
        {
            mErrors.addOneError("没有查到公司名");
            System.out.println("没有查到公司名" + tLDCodeDB.mErrors.getErrContent());
            return null;
        } 
        else 
        {
            return set.get(1).getCodeName();
        }
    }

}
