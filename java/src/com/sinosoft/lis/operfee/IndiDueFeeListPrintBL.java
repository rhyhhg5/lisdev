package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.vschema.LDCodeSet;
import java.sql.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class IndiDueFeeListPrintBL {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = new TransferData();
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private SSRS mSSRS;
    private SSRS mRiskSSRS;
    private String mStartDate = ""; //起始日
    private String mEndDate = ""; //截止日
    private String mDealState = ""; //催收状态
    private String msql = "";

    public static void main(String[] args)
    {
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";
        tGI.ManageCom = "86";

        String tSQL = "";
//        tSQL  = "select ROW_Number() over(), b.makedate,ShowManageName(a.ManageCom),";
//        tSQL += " (select name from LABranchGroup where agentgroup = a.agentgroup),";
//        tSQL += " a.ContNo,b.GetNoticeNo,(select codename  from ldcode where codetype='dealstate' and code=b.dealstate),";
//        tSQL += " a.AppntName,(select Companyphone from lcaddress where customerno = a.appntno and addressno = '1'),";
//        tSQL += " (select Postaladdress from lcaddress where customerno = a.appntno and addressno = '1'),a.CValiDate,";
//        tSQL += " (select AccGetMoney from LCAppAcc where CustomerNo=a.appntno),";
//        tSQL += " b.getnoticeno,b.SumDuePayMoney,(select min(LastPayToDate) from ljspaypersonb where getnoticeno = b.getnoticeno),";
//        tSQL += " (select codename from ldcode where codetype='paymode' and code=a.PayMode),";
//        tSQL += " a.AgentCode,(select phone from laagent where AgentCode= a.AgentCode)";
//        tSQL += " (select Name from laagent where AgentCode= a.AgentCode),b.PayDate ";
//        tSQL += " from LCCont a,ljspayB b,ljspaypersonB c where 1=1 ";
        tSQL += " and b.othernotype='2' and a.ContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo ";
        tSQL += " and c.LastPayToDate>='2005-1-1' and  c.LastPayToDate<='2006-11-05' and a.managecom like '8611";
//        tSQL += " group by b.makedate,a.contno,a.CValiDate,a.managecom,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate,a.appntno,a.agentgroup,a.AppntName,b.SumDuePayMoney,b.paydate";
//        tSQL += " order by b.GetNoticeNo desc";

        IndiDueFeeListPrintBL tIndiDueFeeListPrintBL = new IndiDueFeeListPrintBL();
        TransferData tTransferData= new TransferData();
        tTransferData.setNameAndValue("strsql",tSQL);

        VData tVData = new VData();
        tVData.addElement(tGI);
        tVData.addElement(tTransferData);

        if(!tIndiDueFeeListPrintBL.submitData(tVData, "PRINT"))
        {
            System.out.println("失败！");
        }else
        {
            System.out.println("成功！");
        }
    }

    public IndiDueFeeListPrintBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {

            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }
        //mStartDate = (String) mTransferData.getValueByName("SingleStartDate");
       // mEndDate = (String) mTransferData.getValueByName("SingleEndDate");
       // mDealState = (String) mTransferData.getValueByName("DealState");
        msql = (String) mTransferData.getValueByName("strsql");
        System.out.println(msql);
        return true;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
    /*   StringBuffer sqlSB = new StringBuffer();
       String sqlCons = "";
       if (!(mStartDate == null || mStartDate.equals(""))) {
           sqlCons = " and c.LastPayToDate>='" + mStartDate + "'";
        }
        if (!(mEndDate == null || mEndDate.equals(""))) {
            sqlCons = sqlCons + " and c.LastPayToDate<='" + mEndDate + "'";
        }
        if (!mDealState.equals("3")) {
            sqlCons = sqlCons + " and b.DealState='" + mDealState + "'";
        }
        sqlSB.append( "select  a.ContNo,a.AppntName,a.CValiDate,a.Dif,b.SumDuePayMoney,min(c.LastPayToDate),")
             .append(" (select codename  from ldcode where codetype='paymode' and code=a.PayMode),")
             .append("ShowManageName(a.ManageCom),a.AgentCode,b.GetNoticeNo")
             .append(" from LCCont a,LJSPayB b,LJSPayPersonB c")
             .append(" where   a.AppFlag='1' and b.OtherNo=a.ContNo and b.OtherNoType='2'")
             .append(" and b.GetNoticeNo = c.GetNoticeNo")
             .append(sqlCons)
             .append(" group by  a.ContNo,a.AppntName,a.CValiDate,a.Dif,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode")
             .append(" order by b.GetNoticeNo desc")
             ;
     String tSQL = sqlSB.toString();*/
        String tSQL = "";
        tSQL  = "select '1', b.makedate,ShowManageName(a.ManageCom) mana,";
//        tSQL += " (select name from LABranchGroup where agentgroup = a.agentgroup),";
        //显示部级销售机构
        tSQL += " (select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode) agen,";
        tSQL += " a.ContNo,b.GetNoticeNo,(select codename  from ldcode where codetype='dealstate' and code=b.dealstate),";
        tSQL += " a.AppntName,(select codename  from ldcode where codetype='sex' and code=a.appntsex),";
//        tSQL += " case when e.Mobile is null then '' else e.mobile end ||' '||case when e.CompanyPhone is null then '' else e.CompanyPhone end ||' '|| case when e.HomePhone is null then '' else e.homephone end ,";
        tSQL += " e.mobile, (case when e.phone is null then e.homephone else e.phone end)," ;
        tSQL += " e.Postaladdress,'',a.CValiDate,";
        //add by xp 添加原始生效日一列
        tSQL += " (case when (select count(1) from lcrnewstatelog where contno=a.contno and state='6')>0 then (select min(cvalidate) from lbcont  where prtno = a.prtno) else a.cvalidate end),";
        tSQL += " nvl((select nvl(AccGetMoney,0) from LCAppAcc where CustomerNo=a.appntno),0),";
        tSQL += " b.SumDuePayMoney,(select min(LastPayToDate) from ljspaypersonb where getnoticeno = b.getnoticeno),";
        tSQL += " (select codename from ldcode where codetype='paymode' and code=a.PayMode),";
//updated by suyanlin 2007/11/21
//        tSQL += " a.AgentCode,case when d.Mobile is null then '' else d.mobile end ||' '||case when d.Phone is null then '' else d.Phone end , d.Name,b.PayDate ";
        tSQL += " a.AgentCode,d.Mobile,d.Phone, d.Name,b.PayDate ";
        tSQL +=" ,codename('sex',d.sex) ";
        tSQL += " ,(select max(paydate) From ljapay where b.getnoticeno=getnoticeno and b.dealstate in ('0','1','2','3','4') ),  ";
       //20090326 zhanggm 增加作废原因 cbs00024806
        tSQL += " (case when (b.dealstate ='0' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno ";
        tSQL += " and riskcode in ('320106','120706')) is not null ) then '等待客户反馈' ";
        tSQL += " when (b.dealstate ='1' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno and riskcode in ('320106','120706')) is not null ) then '续保成功' ";
        tSQL += " when (b.dealstate ='3' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno  and riskcode in ('320106','120706')	) is not null ) then '满期给付' ";
        tSQL +=" else (select codename  from ldcode where codetype='dealstate' and code=b.dealstate)  end),";
        tSQL += "(case b.dealstate when '2' then (select codename from ldcode where codetype ='feecancelreason' and code = b.Cancelreason) end) ";
        
        tSQL += " from LCCont a,ljspayB b,ljspaypersonB c,laagent d ,lcaddress e ";
        tSQL += " where 1=1 and a.AgentCode = d.AgentCode and a.appntno =e.customerno ";
        tSQL += " and e.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) ";
        tSQL += msql;
        tSQL += "%' group by b.makedate,a.contno,a.prtno,a.CValiDate,a.managecom,a.AgentCode,b.GetNoticeNo,a.PayMode,";
//        tSQL += " b.dealstate,a.appntno,a.agentgroup,a.AppntName,b.SumDuePayMoney,b.paydate,e.Mobile,e.CompanyPhone,e.HomePhone,e.Postaladdress,d.Name,d.Mobile,d.Phone,a.appntsex" ;
        tSQL += " b.dealstate,a.appntno,a.agentgroup,a.AppntName,b.SumDuePayMoney,b.paydate,e.Mobile,e.CompanyPhone,e.HomePhone,e.Postaladdress,d.Name,d.Mobile,d.Phone,a.appntsex,e.mobile,e.homephone,d.sex,b.Cancelreason,e.phone " ;
        tSQL += " order by mana,agen,a.AgentCode,b.GetNoticeNo desc with ur";
        System.out.println("msql:" + tSQL);
        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

//        ResultSet rs = null;
//        rs = execQuery(this.msql);
//
//        mRS = rs;

        return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {

        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("IndiDueFeeList.vts", "printer"); //最好紧接着就初始化xml文档

        // updated by suyanlin 2007/11/21 start
		//String[] title = {"序", "抽档日期", "管理机构", "营销部门", "保单号",
        //                "应收记录号", "催收状态", "投保人","性别","移动电话 ","联系电话", "投保人联系地址",
        //                 "保单生效日","可抵余额","应交险种","应交保费","应收时间","收费方式",
        //                 "代理人编码","代理人电话","代理人姓名","缴费截至日期","实交日期"};

        String[] title = {"序", "抽档日期", "管理机构", "营销部门", "保单号",
                         "应收记录号", "催收状态", "投保人","性别","移动电话 ","联系电话", "投保人联系地址",
                         "保单生效日","原始保单生效日","可抵余额","应交险种","应交保费","应收时间","收费方式",
                         "代理人编码","代理人手机","代理人固话","代理人姓名","代理人性别","缴费截至日期","实交日期","催收状态","作废原因"};
        // updated by suyanlin 2007/11/21 end

        xmlexport.addListTable(getListTable(), title);
        xmlexport.outputDocumentToFile("C:\\", "GrpPremDueFeeList");
        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
//            updated by suyanlin 2007/11/21
//            String[] info = new String[24];
            String[] info = new String[28];
            //标记序列号
            info[0] = String.valueOf(i);
            for (int j = 2; j <= mSSRS.getMaxCol(); j++) {
                if(j == 13)
                {
                    //获取险种信息
                    String tRiskcodeSQL = "select distinct RiskCode from LJSPayPersonB where GetNoticeNO = '" + mSSRS.GetText(i, 6)+"'";
                    ExeSQL tExeSQL = new ExeSQL();
                    mRiskSSRS = tExeSQL.execSQL(tRiskcodeSQL);
//                    System.out.println("查看语句 :::"+tRiskcodeSQL);
                    info[12] = "";
                    for( int m = 1 ; m < mRiskSSRS.getMaxRow(); m++ )
                    {
                        info[12] += mRiskSSRS.GetText(m,1)+ "、";
                    }
                    info[12] += mRiskSSRS.GetText(mRiskSSRS.getMaxRow(),1);
                }
                else{
                    info[j - 1] = mSSRS.GetText(i, j);
                }
            }
            tListTable.add(info);
        }
        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


    private void jbInit() throws Exception {
    }

    //查询公司名称
    private String getCompanyName() {
        //通过LDCode得到保单号，然后得到公司名称
        String manageCode = mGlobalInput.ManageCom;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.setCode(manageCode);
        LDCodeSet set = tLDCodeDB.query();
        if (set == null || set.size() == 0) {

            mErrors.addOneError("没有查到公司名");
            System.out.println("没有查到公司名" + tLDCodeDB.mErrors.getErrContent());
            return null;
        } else {
            return set.get(1).getCodeName();
        }
    }

}
