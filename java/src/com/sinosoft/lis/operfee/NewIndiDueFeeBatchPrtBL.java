package com.sinosoft.lis.operfee;

import java.io.File;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.f1print.IndiDueFeeChildPrintBL;
import com.sinosoft.lis.f1print.PDFPrintBatchManagerBL;
import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LDSysVarSchema;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>
 * Title: 保险业务系统
 * </p>
 * <p>
 * Description: 应收通知书批量打印
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author
 */
public class NewIndiDueFeeBatchPrtBL {
	private String mOperate;

	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private GlobalInput mG = new GlobalInput();

	public NewIndiDueFeeBatchPrtBL() {
	}

	private LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();

	private LJSPayBSet mLJSPayBSet = new LJSPayBSet();

	private boolean operFlag = true;

	private String strLogs = "";

	private String Content = "";

	private String mxmlFileName = "";

	private int mCount = 0;

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap mMap = new MMap();

	private PubSubmit tPubSubmit = new PubSubmit();
	
	private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		cInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!dealData()) {
			return false;
		}
		
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	private boolean getInputData(VData cInputData) {
		mLJSPayBSet = (LJSPayBSet) cInputData.getObjectByObjectName(
				"LJSPayBSet", 0);
		if (mLJSPayBSet == null) { // （服务事件关联表）
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "IndiDueFeeBatchPrtBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的数据为空!";
			this.mErrors.addOneError(tError);
			return false;
		}
		// 20080728 zhanggm 增加对mLJSPayBSet中每个Schema的非空校验
		for (int i = 1; i <= mLJSPayBSet.size(); i++) {
			LJSPayBSchema tLJSPayBSchema = mLJSPayBSet.get(i);
			String tGetNoticeno = tLJSPayBSchema.getGetNoticeNo().trim();
			if ("".equals(tGetNoticeno) || "null".equals(tGetNoticeno)
					|| null == tGetNoticeno) {
				CError tError = new CError();
				tError.moduleName = "IndiDueFeeBatchPrtBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "传入的通知书号码为空，请确认是否已经抽档成功";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		// ----------------------------------------------------------
		mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mLDSysVarSchema.setSchema((LDSysVarSchema) cInputData
				.getObjectByObjectName("LDSysVarSchema", 0));
		return true;
	}

	public int getCount() {
		return mCount;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {
		if (!prepareOutputData()) {
			return false;
		}
	    VData tVData = new VData();
        tVData.add(mG);
        tVData.add(mLOPRTManagerSet);
        PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
        if(!tPDFPrintBatchManagerBL.submitData(tVData,mOperate)){
             mErrors.addOneError(tPDFPrintBatchManagerBL.mErrors.getError(1));
             return false;
        }
		
		return true;
	}

	public void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "IndiDueFeeBatchPrtBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ComCode = "86";
		tGlobalInput.ManageCom = "86";
		tGlobalInput.Operator = "001";
		LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
		mLDSysVarSchema.setSysVarValue("E:\\workspace\\ui\\");

		LJSPayBSet tLJSPayBSet = new LJSPayBSet();

		LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
		tLJSPayBSchema.setGetNoticeNo("31000006028");
		tLJSPayBSet.add(tLJSPayBSchema);
		LJSPayBSchema tLJSPayBSchema2 = new LJSPayBSchema();
		tLJSPayBSchema2.setGetNoticeNo("31000005931");
		tLJSPayBSet.add(tLJSPayBSchema2);
		LJSPayBSchema tLJSPayBSchema3 = new LJSPayBSchema();
		tLJSPayBSchema3.setGetNoticeNo("31000000575");
		tLJSPayBSet.add(tLJSPayBSchema3);

		VData aVData = new VData();
		aVData.add(tGlobalInput);
		aVData.add(tLJSPayBSet);
		aVData.add(mLDSysVarSchema);
		/*
		 * String aUserCode = "000011"; VData aVData = new VData();
		 * aVData.addElement(aUserCode);
		 */
		IndiDueFeeBatchPrtBL tIndiDueFeeBatchPrtBL = new IndiDueFeeBatchPrtBL();
		tIndiDueFeeBatchPrtBL.submitData(aVData, "PRINT");
		int tCount = tIndiDueFeeBatchPrtBL.getCount();
		System.out.println("~~~~~~~~~~~~~~~~~~~" + tCount);
		String tFileName[] = new String[tCount];
		VData tResult = new VData();
		tResult = tIndiDueFeeBatchPrtBL.getResult();
		tFileName = (String[]) tResult.getObject(0);
		System.out.println("~~~~~~~~~~~~~~~~~~~2" + tFileName);

	}

	private boolean callPrintService(LOPRTManagerSchema aLOPRTManagerSchema) {

		// 查找打印服务
		String strSQL = "SELECT * FROM LDCode WHERE CodeType = 'print_service'";
		strSQL += " AND Code = '" + aLOPRTManagerSchema.getCode() + "'";
		strSQL += " AND OtherSign = '0'";
		System.out.println(strSQL);
		LDCodeSet tLDCodeSet = new LDCodeDB().executeQuery(strSQL);

		if (tLDCodeSet.size() == 0) {
			buildError("dealData", "找不到对应的打印服务类(Code = '"
					+ aLOPRTManagerSchema.getCode() + "')");
			return false;
		}

		// 调用打印服务
		LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);

		try {
			Class cls = Class.forName(tLDCodeSchema.getCodeAlias());
			PrintService ps = (PrintService) cls.newInstance();

			// 准备数据
			String strOperate = tLDCodeSchema.getCodeName();

			VData vData = new VData();

			vData.add(mG);
			vData.add(aLOPRTManagerSchema);

			if (!ps.submitData(vData, strOperate)) {
				mErrors.copyAllErrors(ps.getErrors());
				return false;
			}

			mResult = ps.getResult();

		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("callPrintService", ex.toString());
			return false;
		}

		return true;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData() {
		mCount = mLJSPayBSet.size();
		for (int i = 1; i <= mCount; i++) {
			LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
			LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();

			// 080312 modifide by zhanggm-------------------------
			String tGetNoticeNo = mLJSPayBSet.get(i).getGetNoticeNo();
			String sql = "select RiskCode from LJSPayPersonB where GetNoticeNo = '"
					+ tGetNoticeNo + "'";
			String tRiskCode = new ExeSQL().getOneValue(sql);
			if ("120706".equals(tRiskCode) || "320106".equals(tRiskCode)) {
				tLOPRTManagerDB.setCode("XB001");
			} else {
				tLOPRTManagerDB.setCode("93");
			}
			// //-------------------------------------------------
	        //如果保单下只有6201、6202、6203、6204险种被催收的话，不在打印催收通知书
	        sql = "select 1 from ljspaypersonb a  where getnoticeno='"+mLJSPayBSet.get(i).getGetNoticeNo()+"' "
	        	 +" and riskcode in (select riskcode from lmriskapp where risktype ='M' ) "
	        	 +" and not exists (select 1 from ljspaypersonb where contno=a.contno and getnoticeno=a.getnoticeno and riskcode<>a.riskcode) "
	        	 +" with ur ";
	        ExeSQL tExeSQL = new ExeSQL();
	        String jianGuanRisk = tExeSQL.getOneValue(sql);
	        if(!jianGuanRisk.equals("") && null!=jianGuanRisk)
	        {
	            CError.buildErr(this,"应收记录："+mLJSPayBSet.get(i).getOtherNo()+"中只有建管险种，不生成续期交费通知书！");
	            continue;
	        }

			System.out.println("*************"
					+ mLJSPayBSet.get(i).getGetNoticeNo());
			tLOPRTManagerDB.setStandbyFlag2(mLJSPayBSet.get(i)
					.getGetNoticeNo());
			tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);
			if (tLOPRTManagerSchema == null) {
				System.out
						.println("*******LOPRTManagerSchema is null******");
				String ContNo = "";
				LJSPayBDB tLJSPayBDB = new LJSPayBDB();
				tLJSPayBDB.setGetNoticeNo(mLJSPayBSet.get(i)
						.getGetNoticeNo());
				System.out
						.println("*******mLJSPayBSet.get(i).getGetNoticeNo()******"
								+ mLJSPayBSet.get(i).getGetNoticeNo());
				if (!tLJSPayBDB.getInfo()) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "dealData";
					tError.functionName = "getInputData";
					tError.errorMessage = "传入的数据为空!";
					this.mErrors.addOneError(tError);
					continue;

				} else {
					ContNo = tLJSPayBDB.getOtherNo();
					System.out.println("*******ContNo is ******" + ContNo);
				}

				LCContSchema tLCContSchema = new LCContSchema();
				tLCContSchema.setContNo(ContNo);

				VData xVData = new VData();
				xVData.addElement(tLCContSchema);
				xVData.addElement(mG);
				xVData.addElement(tLJSPayBDB);
				if ("120706".equals(tRiskCode)
						|| "320106".equals(tRiskCode)) {
					IndiDueFeeChildPrintBL tIndiDueFeeChildPrintBL = new IndiDueFeeChildPrintBL();
					if (!tIndiDueFeeChildPrintBL.submitData(xVData,
							"INSERT")) {
						operFlag = false;
						Content = tIndiDueFeeChildPrintBL.mErrors
								.getErrContent();
					}
				} else {
					com.sinosoft.lis.f1print.IndiDueFeePrintBL tIndiDueFeePrintBL = new com.sinosoft.lis.f1print.IndiDueFeePrintBL();
					if (!tIndiDueFeePrintBL.submitData(xVData, "INSERT")) {
						operFlag = false;
						Content = tIndiDueFeePrintBL.mErrors
								.getErrContent();
					}
				}

				tLOPRTManagerDB.setStandbyFlag2(mLJSPayBSet.get(i)
						.getGetNoticeNo());
				tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);
			}
			// 下面封装要更新的打印管理信息
			tLOPRTManagerSchema.setStateFlag("1");
			tLOPRTManagerSchema.setPrintTimes(tLOPRTManagerSchema
					.getPrintTimes() + 1);
			tLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
			tLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());
			mLOPRTManagerSet.add(tLOPRTManagerSchema);

			
			XmlExport txmlExport = (XmlExport) mResult
					.getObjectByObjectName("XmlExport", 0);
			if (txmlExport == null) {
				Content = "印刷号" + tLOPRTManagerSchema.getPrtSeq()
						+ "没有得到要显示的数据文件！";
				strLogs = strLogs + Content;
				continue;
			}
			if (operFlag == true) {
				File f = new File(mLDSysVarSchema.getSysVarValue());
				f.mkdir();
				System.out.println("PATH : "
						+ mLDSysVarSchema.getSysVarValue());
				System.out.println("PATH2 : "
						+ mLDSysVarSchema.getSysVarValue()
								.substring(
										0,
										mLDSysVarSchema.getSysVarValue()
												.length() - 1));
				txmlExport.outputDocumentToFile(
						mLDSysVarSchema.getSysVarValue()
								.substring(
										0,
										mLDSysVarSchema.getSysVarValue()
												.length() - 1)
								+ File.separator
								+ "printdata"
								+ File.separator
								+ "data"
								+ File.separator
								+ "brief" + File.separator, mxmlFileName);
			}
		}
		return true;
	}
}
