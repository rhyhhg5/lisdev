package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.bq.ExpirBenfitIntoInsuAcc;
import com.sinosoft.lis.bq.PEdorAppItemUI;
import com.sinosoft.lis.db.LCInsureAccTraceDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMInsuAccRateDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCInsureAccTraceSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.lis.vschema.LJSGetDrawSet;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMInsuAccRateSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.*;

import java.lang.String;



/**
 * 附加万能保单自动满期给付处理
 * @author lzy
 *
 */
public class ExpirBenefitAutoGetBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();
    
    private LCContSchema mLCContSchema = null;//保单信息
    private LCPolSchema mLCPolSchema = null;//满期主险信息
    
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
	private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
	
	private LPContSchema mLPContschema = new LPContSchema();
	private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();

    /** 数据操作字符串 */
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mserNo = ""; //批次号
    private ExeSQL mExeSQL=new ExeSQL();
    private MMap mMMap =new MMap();
    
    

    public ExpirBenefitAutoGetBL() {
    }

    /**
     * @param cInputData VData，包含：
     * 1、	GlobalInput对象，完整的登陆用户信息
     * 2、	TransferData对象，包含StartDate， EndDate， ManageCom
     * @param cOperate String：操作类型，此可为空字符串“”
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //接收传入数据
        if (!getInputData(cInputData)) {
            return false;
        }
        //校验是否可自动满期
		if (!checkData()) {
            return false;
        }
        
        //处理数据
        if (!dealData()) {
            return false;
        }
        
        return true;
    }
    
    /**
     * 从输入数据中得到所有对象
     * @param mInputData:
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

        mLCContSchema = ((LCContSchema) mInputData.getObjectByObjectName("LCContSchema",0));
        mLCPolSchema = ((LCPolSchema) mInputData.getObjectByObjectName("LCPolSchema",0));
        mserNo = ((String) mInputData.getObjectByObjectName("String",0));
        
        if(null == mLCContSchema || null == mLCPolSchema){
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitBatchBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有接收到保单信息!";
            this.mErrors.addOneError(tError);
        }

        if (tGI == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitBatchBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }
    
    private boolean checkData(){
		//校验保全
        if (!checkBQ())
        {
            return false ;
        }
		//校验理赔
        if(!checkLP()){
        	return false ;
        }
        if(!checkJSGet()){
        	return false ;
        }
        
		return true;
	}
	
	private boolean checkBQ()
    {
        //校验保全项目
        String sql = "select a.edorNo "
                     + "from LPEdorItem a, LPEdorApp b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and a.ContNo = '" + mLCContSchema.getContNo() + "' "
                     + "   and b.edorState != '" + BQ.EDORSTATE_CONFIRM + "' ";
        String edorNo = mExeSQL.getOneValue(sql);
        if (!edorNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "checkBQ";
            tError.errorMessage = "保单" + mLCContSchema.getContNo() + "正在做保全业务：" +
                                  edorNo + "，不能给付抽档";
            System.out.println(tError.errorMessage);
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
	
	private boolean checkLP()
    {
        //正在做理赔不允许做满期给付
       String sql = "select 1 from llcase where customerno in (select insuredno from lcinsured where contno ='" +
       			mLCContSchema.getContNo() + "')"
              + " and endcasedate is null and rgtstate not in('11','12','14') ";  //11- 通知 12-给付 14-扯件
              
        String rgtNo = mExeSQL.getOneValue(sql);
        if (rgtNo != null && !rgtNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "checkLP";
            tError.errorMessage = "保单有未结案的理赔。";
            mErrors.addOneError(tError);
            System.out.println("保单"+mLCContSchema.getContNo()+"有未结案的理赔");
            return false;
        }
        return true;
    }
	
	private boolean checkJSGet(){
		//保单下存在满期抽档未确认的给付记录的不能自动满期处理
		String str="select a.getnoticeno from ljsget a ,ljsgetdraw d "
			+ " where a.getnoticeno = d.getnoticeno  "
			+ " and a.othernotype='20' and (feefinatype != 'YEI' or d.riskcode = '330501') "
			+ " and d.contno='" + mLCContSchema.getContNo() + "' "
			+ " and not exists (select 1 from ljaget where actugetno = a.getnoticeno) "
			+ " with ur";
		String getnoticeno = mExeSQL.getOneValue(str);
		if(null != getnoticeno && !"".equals(getnoticeno)){
			CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "checkLP";
            tError.errorMessage = "保单有给付抽档未确认的记录，给付通知号："+getnoticeno;
            mErrors.addOneError(tError);
            System.out.println("保单"+mLCContSchema.getContNo()+"有给付抽档未确认的记录");
            return false;
		}
		return true;
	}

    /**
     *自动满期程序处理步骤：
     *1、对附加万能的主险自动满期生成抽档数据
     *2、满期给付实付
     *3、生成追加保费工单，满期金进入附加万能账户
     *4、对账户进行结算
     * @return boolean
     */
    public boolean dealData() {
    	//获取当前主险下附加万能险种信息
    	String subRiskStr="select * from lcpol "
    					+ " where appflag='1' and stateflag='1' "
    					+ " and contno='"+mLCPolSchema.getContNo()+"'"
    					+ " and insuredno='"+mLCPolSchema.getInsuredNo()+"' "
//    					+ " and mainpolno='"+mLCPolSchema.getPolNo()+"' "//有些保单的mainpolno不准
    					+ " and (riskcode in (select riskcode from lmriskapp where risktype4='4' and subriskflag='S' and riskprop='I' ) "
    					+ " or riskcode in ('340501','340601','340602') )"
    					+ " with ur";
    	LCPolSchema subLCPolSchema = new LCPolSchema(); 
    	LCPolDB tLCPolDB=new LCPolDB();
    	LCPolSet tLCPolSet = tLCPolDB.executeQuery(subRiskStr);
    	System.out.println("附加万能险查询sql:"+subRiskStr);
    	if(tLCPolSet.size() == 0){
    		CError.buildErr(this, "查询附加万能险种失败");
    		return false;
    	}
    	subLCPolSchema = tLCPolSet.get(1);
    	
    	String subRiskValidate=subLCPolSchema.getCValiDate();
    	if(null == subRiskValidate || "".equals(subRiskValidate)){
    		CError.buildErr(this, "没有获取附加万能险种生效日期");
    		return false;
    	}
        //接收外部传入数据
	    VData tVData = new VData();
	    tVData.add(mLCContSchema);
	    tVData.add(tGI);
	    TransferData mTransferData = new TransferData();
	    mTransferData.setNameAndValue("serNo", mserNo);
	    mTransferData.setNameAndValue("StartDate", "2008-01-01");//自动满期对起始日期不限制，但后边抽档需要
	    mTransferData.setNameAndValue("EndDate", CurrentDate);
	    mTransferData.setNameAndValue("subRiskValidate", subRiskValidate);
	    mTransferData.setNameAndValue("PayMode", "Q");
	    //存在多主险的情况，所以逐险种处理，只对当前处理的满期主险进行抽档
	    mTransferData.setNameAndValue("PolNo", mLCPolSchema.getPolNo());
	    tVData.add(mTransferData);
	    
	    if(!atuoGet(tVData,subLCPolSchema)){
	    	return false;
	    }
	    
        return true;
    }
    
    private boolean atuoGet(VData tVData,LCPolSchema subLCPolSchema){
    	//调用满期给付抽档程序
	    ExpirBenefitGetBL  tExpirBenefitGetBL = new ExpirBenefitGetBL();
	    if (!tExpirBenefitGetBL.submitData(tVData,"INSERT"))
	    {
	    	String errorinfo="保单号为：" + mLCContSchema.getContNo() + "的保单给付抽档失败："
	    					+ tExpirBenefitGetBL.mErrors.getFirstError();
	        CError.buildErr(this, errorinfo);
	        return false;
	    }
	    //获取满期抽档处理结果
	    MMap ljsDrawMap=tExpirBenefitGetBL.getResult();
	    
	    if(null == ljsDrawMap){
	    	CError.buildErr(this, "保单号为：" + mLCContSchema.getContNo() + "的给付抽档数据获取失败");
	    	return false;
	    }
	    LJSGetDrawSet tLJSGetDrawSet=(LJSGetDrawSet)ljsDrawMap.getObjectByObjectName("LJSGetDrawSet",0);
	    LJSGetSchema tLJSGetSchema=(LJSGetSchema)ljsDrawMap.getObjectByObjectName("LJSGetSchema",0);
	    if(null == tLJSGetDrawSet || null == tLJSGetSchema){
	    	CError.buildErr(this, "保单号为：" + mLCContSchema.getContNo() + "的给付抽档数据获取失败");
	    	return false;
	    }
	    
	    tLJSGetSchema.setDealState("1");
	    tLJSGetSchema.setPayMode("5");//内部转账
	    
	    Reflections ref = new Reflections();
	    LJAGetSchema tLJAGetSchema = new LJAGetSchema();
	    ref.transFields(tLJAGetSchema, tLJSGetSchema);
	    tLJAGetSchema.setActuGetNo(tLJSGetSchema.getGetNoticeNo());
	    tLJAGetSchema.setPayMode("5"); 
	    tLJAGetSchema.setEnterAccDate(CurrentDate);
        tLJAGetSchema.setConfDate(CurrentDate);
        tLJAGetSchema.setMakeDate(CurrentDate);
        tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAGetSchema.setModifyDate(CurrentDate);
        tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
//        mMMap.put(tLJSGetSchema,SysConst.INSERT);
        mMMap.put(tLJAGetSchema,SysConst.DELETE_AND_INSERT);
        
        //生成财务数据
//        LJFIGetSet tLJFIGetSet = new LJFIGetSet();
        LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
        ref.transFields(tLJFIGetSchema,tLJAGetSchema);
        tLJFIGetSchema.setGetMoney(tLJAGetSchema.getSumGetMoney());
        tLJFIGetSchema.setPolicyCom(tLJAGetSchema.getManageCom());
//        tLJFIGetSet.add(tLJFIGetSchema);
        mMMap.put(tLJFIGetSchema,SysConst.DELETE_AND_INSERT);
        
        mMMap.add(ljsDrawMap);
        
     	/**
     	 * 自动抽档的满期金应该是每一笔生成一个对应的追加保费工单（满期金应进账户日期可能不一致），
     	 * 因无法将多个追加保费的工单整在一个事务中
     	 * 所以在上边自动满期抽时一次只抽出来一笔。
     	 * 满期抽档数据也存在提交失败的情况，所以先提交满期数据，后生成工单，
     	 * 若工单生成失败则撤销满期抽档记录
     	 */
	    for(int i=1;i<=tLJSGetDrawSet.size(); i++){
	    	tLJSGetDrawSet.get(i).setFeeFinaType("YEI");
	    	//一笔满期金生成一个工单，所以在这里处理完一笔提交一笔。
	        if(!submit()){
				return false;
			}else{
				//生成追加保费工单
				try {
					if(!createZBEdor(tLJSGetDrawSet.get(i),subLCPolSchema)){
						cancelGetDraw(tLJSGetDrawSet.get(i),tLJSGetSchema,tLJAGetSchema,tLJFIGetSchema);
						System.out.println("追加保费失败，将满期抽档记录撤销");
			        	return false;
			        }
				} catch (Exception e) {
					cancelGetDraw(tLJSGetDrawSet.get(i),tLJSGetSchema,tLJAGetSchema,tLJFIGetSchema);
					System.out.println("追加保费失败，将满期抽档记录撤销");
					return false;
				}
			}
	    }
	    
	    //自动满期金额是一笔一笔的抽，所以看当前险种下是否还有可领取的满期金
	    LCGetSet leaveGetSet = tExpirBenefitGetBL.getLCGetNeedGet(CurrentDate, mLCPolSchema.getContNo(), mLCPolSchema.getInsuredNo());
	    if(null == leaveGetSet || leaveGetSet.size() == 0){
	    	return true;
	    }else{
	    	System.out.println("***&&***被保人"+mLCPolSchema.getInsuredNo()+"还有"+leaveGetSet.size()+"笔满期金");
	    	if(!atuoGet(tVData,subLCPolSchema)){
		    	return false;
		    }
	    }
	    
	    return true;
    }
    
    private boolean createZBEdor(LJSGetDrawSchema tLJSGetDrawSchema,LCPolSchema subLCPolSchema){
    	String riskCvalidate = subLCPolSchema.getCValiDate();//附加险的生效日期
    	String getDate = tLJSGetDrawSchema.getLastGettoDate();//满期应领取日
    	String strSQL = null;
    	//获取满期金当期保费的核销日期：若满期日晚于缴费期间则取最后一期保费的核销日期，
    	//若满期日处于缴费期间，则取当期保费的核销日期
    	
    	//判断险种是否为健康一生类型的
    	String riskcodeSQL = "select * from lmriskapp where riskcode = '"+subLCPolSchema.getRiskCode()+"' and riskname like '健康一生%'"; 
    	String riskcode = new ExeSQL().getOneValue(riskcodeSQL);
    	if (null == riskcode || "".equals(riskcode)) {
    		strSQL = "select (case when payenddate<'"+getDate+"' then "
				+ " (select min(confdate) from ljapayperson where polno=a.polno and curpaytodate>=a.payenddate) "
				+ " else (select min(confdate) from ljapayperson where polno=a.polno and curpaytodate>='"+getDate+"') end) "
				+ " from lcpol a where contno='"+subLCPolSchema.getContNo()+"' "
				+ " and polno='"+subLCPolSchema.getPolNo()+"' ";
		}else {
			strSQL = "select (case when payenddate<'"+getDate+"' then "
				+ " (select min(confdate) from ljapayperson where polno=a.polno) "
				+ " else (select min(confdate) from ljapayperson where polno=a.polno) end) "
				+ " from lcpol a where contno='"+subLCPolSchema.getContNo()+"' "
				+ " and polno='"+subLCPolSchema.getPolNo()+"' ";
		}
    	String premConfDate = new ExeSQL().getOneValue(strSQL);
    	if(null == premConfDate || "".equals(premConfDate)){
    		String errorinfo="获取保单保费核销日期失败！";
    		CError.buildErr(this, errorinfo);
    		return false;
    	}
    	//追加保费的保全生效日期为以下三个日期的最晚的
    	// 1、满期金应领取日
    	// 2、满期金当期保费的核销日期
    	// 3、附加万能险生效日期
    	String edorvalidate = riskCvalidate;
    	FDate tFDate=new FDate();
    	if(tFDate.getDate(getDate).after(tFDate.getDate(edorvalidate))){
    		edorvalidate = getDate;
    	}
    	if(tFDate.getDate(premConfDate).after(tFDate.getDate(edorvalidate))){
    		edorvalidate = premConfDate;
    	}
    	//进入账户金额=满期金+利息
    	double getMoney =CommonBL.carry(tLJSGetDrawSchema.getGetMoney() + tLJSGetDrawSchema.getGetInterest());
    	if(getMoney<=0){
    		System.out.println("满期金额为0，不生成保全工单");
    		return true;
    	}
    	String money = String.valueOf(getMoney);
    	VData tVData = new VData();
    	tVData.add(tGI);
    	tVData.add(subLCPolSchema);
    	tVData.add(mLCContSchema);
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("EdorValidate", edorvalidate);
    	tTransferData.setNameAndValue("Money", money);
    	tTransferData.setNameAndValue("GetNoticeno", tLJSGetDrawSchema.getGetNoticeNo());
    	tVData.add(tTransferData);
    	
    	ExpirBenfitIntoInsuAcc tExpirBenfitIntoInsuAcc = new ExpirBenfitIntoInsuAcc();
    	
    	if(!tExpirBenfitIntoInsuAcc.submitData(tVData)){
    		CError.buildErr(this,tExpirBenfitIntoInsuAcc.mErrors.getFirstError());
    		return false;
    	}
    	
    	return true;
    }
    
    //撤销自动满期抽档记录
    private boolean cancelGetDraw(LJSGetDrawSchema tDrawSchema,LJSGetSchema tLJSGetSchema,LJAGetSchema tLJAGetSchema,LJFIGetSchema tLjfiGetSchema){
    	String getSQL= "update lcget set gettodate='"+tDrawSchema.getLastGettoDate()+"' "
					+ " ,modifydate=current date,modifytime=current time where contno='"+tDrawSchema.getContNo()+"' and insuredno='"+tDrawSchema.getInsuredNo()+"' "
					+ "  and getdutykind='"+tDrawSchema.getGetDutyKind()+"' and GetDutyCode = '"+tDrawSchema.getGetDutyCode()+"'";
    	mMMap = new MMap();
		mMMap.put(getSQL, SysConst.UPDATE);
		mMMap.put(tDrawSchema, SysConst.DELETE);
		mMMap.put(tLJSGetSchema, SysConst.DELETE);
		mMMap.put(tLJAGetSchema, SysConst.DELETE);
		mMMap.put(tLjfiGetSchema, SysConst.DELETE);
    	
		if(!submit()){
    		return false;
    	}
    	return true;
    }
    
    /**
	 * 
	 * @return boolean
	 */
	private boolean submit() {
		try {
			VData tData = new VData();
			tData.add(mMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tData, "")) {
				mMMap =new MMap();
				System.out.print("保存数据返回");
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "GetTempFeeSeparateBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception ex) {
			mMMap =new MMap();
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		mMMap =new MMap();
		return true;
	}
    
    public VData getResult() {
        VData t = new VData();
        t.add(0,mserNo);
        return t;
    }
    
    /**
        * 加入到日志表数据,记录因为各种原因抽档失败的单子
        * @param
        * @return boolean
        */
    private boolean dealErrorLog(LCPolSchema tLCPolSchema, String Error) {
           LGErrorLogSchema tLGErrorLogSchema = new LGErrorLogSchema();
           tLGErrorLogSchema.setSerialNo(mserNo);
           tLGErrorLogSchema.setErrorType("0003");//附加万能满期
           tLGErrorLogSchema.setErrorTypeSub("03");
           tLGErrorLogSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
           tLGErrorLogSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
           tLGErrorLogSchema.setContNo(tLCPolSchema.getContNo());
           tLGErrorLogSchema.setPolNo(tLCPolSchema.getPolNo());
           tLGErrorLogSchema.setOperator(tGI.Operator);
           tLGErrorLogSchema.setMakeDate(CurrentDate);
           tLGErrorLogSchema.setMakeTime(CurrentTime);
           tLGErrorLogSchema.setModifyDate(CurrentDate);
           tLGErrorLogSchema.setModifyTime(CurrentTime);
           tLGErrorLogSchema.setDescribe(Error);

           MMap tMap = new MMap();
           tMap.put(tLGErrorLogSchema, "DELETE&INSERT");
           VData tInputData = new VData();
           tInputData.add(tMap);
           PubSubmit tPubSubmit = new PubSubmit();
           if (tPubSubmit.submitData(tInputData, "") == false) {
               this.mErrors.addOneError("PubSubmit:处理LGErrorLog 表失败!");
               return false;
           }
           return true;
    }

}
