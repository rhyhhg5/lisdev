package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.util.*;
import com.sinosoft.lis.bq.*;

//程序名称：ExpirBenefitConfirmAppAccBL.java
//程序功能：个险给付确认转入投保人账户
//创建日期：2011-2-14 
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class ExpirBenefitConfirmAppAccBL 
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData saveData = new VData();
    private MMap mMMap = new MMap();
    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    private String mContNo = null;  //保单号
    private String mGetNoticeNo = null;  //通知书号
    private String mHandler = null;  //申请人类别 0-投保人 1-委托代办
    private String mDrawerCode = null;  //给付对象 0-投保人 1-被保人 2-转入投保人账户
    private String mPayMode = null;  //给付方式 1-现金 4-银行转账
    
    private Reflections ref = new Reflections();

    //个人保单表
    private LCContSchema mLCContSchema = new LCContSchema();

    //应付个人交费表
    private	LJSGetSchema mLJSGetSchema = new LJSGetSchema();
    
    public ExpirBenefitConfirmAppAccBL() {
    }

    public static void main(String[] args) {

    }
    
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData)
    {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0); 
        if(tGI == null || mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmAppAccBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mGetNoticeNo = (String)mTransferData.getValueByName("getnoticeno");  //通知书号
        
        if(mGetNoticeNo == null || mGetNoticeNo.equals(""))
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmAppAccBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入通知书号-mGetNoticeNo失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LJSGetDB tLJSGetDB = new LJSGetDB();
        tLJSGetDB.setGetNoticeNo(mGetNoticeNo);
        if(!tLJSGetDB.getInfo())
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmAppAccBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "通过mGetNoticeNo获取LJSGet失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLJSGetSchema.setSchema(tLJSGetDB.getSchema());
        
        mContNo = (String)mTransferData.getValueByName("contno");
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if(!tLCContDB.getInfo())
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmAppAccBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "通过tContNo获取LCCont失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mLCContSchema.setSchema(tLCContDB.getSchema());

        mHandler = (String)mTransferData.getValueByName("handler");  //申请人类别 0-投保人 1-委托代办
        if(mHandler == null || mHandler.equals(""))
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmAppAccBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入申请人类别-mHandler失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mDrawerCode = (String)mTransferData.getValueByName("drawercode");  //给付对象 0-投保人 1-被保人 2-转入投保人账户
        if(mDrawerCode == null || mDrawerCode.equals(""))
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmAppAccBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入给付对象-mDrawerCode失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        return true;
    }

    /**
     * 校验是否有理赔，保全
     * @return boolean：通过true，否则false
     */
    public boolean checkData() 
    {
        //校验保全项目
        String sql = "select a.edorNo "
                     + "from LPEdorItem a, LPEdorApp b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "and a.ContNo = '" + mLCContSchema.getContNo() + "' "
                     + "and b.edorState <> '" + BQ.EDORSTATE_CONFIRM + "' ";
        String edorNo = new ExeSQL().getOneValue(sql);
        if (edorNo!= null && !edorNo.equals("")) 
        {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmAppAccBL";
            tError.functionName = "checkData";
            tError.errorMessage = "保单"+mLCContSchema.getContNo()+"正在做保全业务：" + edorNo + "，不能给付确认。";
            mErrors.addOneError(tError);
            return false;
        }
        //校验是否正在理赔
        sql = "select 1 from llcase a where exists (select 1 from lcinsured where contno ='" + mLCContSchema.getContNo() 
            + "' and insuredno = a.customerno) and endcasedate is null and rgtstate <> '14' ";
        String rgtNo = new ExeSQL().getOneValue(sql);
        if (rgtNo != null && !rgtNo.equals("")) 
        {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmAppAccBL";
            tError.functionName = "checkData";
            tError.errorMessage = "保单"+mLCContSchema.getContNo()+"有理陪，不能给付确认。";
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public VData getSubmitData(VData cInputData, String cOperate) 
    {
        if (!getInputData(cInputData)) 
        {
            return null;
        }
        System.out.println("After getinputdata");
        if (!checkData()) 
        {
            return null;
        }
        //进行业务处理
        if (!dealData()) 
        {
            return null;
        }

        return saveData;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public MMap getSubmitMMap(VData cInputData, String cOperate) 
    {
        getSubmitData(cInputData, cOperate);
        return mMMap;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate) 
    {
        VData data = getSubmitData(cInputData, cOperate);
        if (data == null) 
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) 
        {
            this.mErrors.addOneError("PubSubmit:生成给付失败!");
            return false;
        }
        return true;
    }

    /**
     * 生成给付记录
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
//防止并发，加锁--------------
    	MMap tCekMap = null;
    	tCekMap = lockLJAGet(mContNo);
        if (tCekMap == null)
        {
            return false;
        }
        mMMap.add(tCekMap);
//--------------------------
        
        //存储给付对象
        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
        tLPEdorEspecialDataSchema.setEdorAcceptNo(mGetNoticeNo);
        tLPEdorEspecialDataSchema.setEdorNo(mGetNoticeNo);
        tLPEdorEspecialDataSchema.setEdorType("MJ");
        tLPEdorEspecialDataSchema.setDetailType("DrawerCode");
        tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
        tLPEdorEspecialDataSchema.setEdorValue(mDrawerCode);
        
        mMMap.put(tLPEdorEspecialDataSchema, SysConst.DELETE_AND_INSERT);
        
        mLJSGetSchema.setPayMode(mPayMode);
        if(mDrawerCode.equals("2")) //转入投保人账户
        {
        	mLJSGetSchema.setDrawer(mLCContSchema.getAppntName());
        	mLJSGetSchema.setDrawerID(mLCContSchema.getAppntIDNo());
        	mLJSGetSchema.setPayMode("5");
        	mLJSGetSchema.setDealState("1");
        	mLJSGetSchema.setGetDate(CurrentDate);
            mLJSGetSchema.setOperator(tGI.Operator);
            mLJSGetSchema.setModifyDate(CurrentDate);
            mLJSGetSchema.setModifyTime(CurrentTime);
            
            mMMap.put(mLJSGetSchema, SysConst.UPDATE);
            
            LJAGetSchema tLJAGetSchema = new LJAGetSchema();
            ref.transFields(tLJAGetSchema, mLJSGetSchema);
            tLJAGetSchema.setActuGetNo(mLJSGetSchema.getGetNoticeNo());
            tLJAGetSchema.setShouldDate(CurrentDate);
            tLJAGetSchema.setConfDate(CurrentDate);
            tLJAGetSchema.setEnterAccDate(CurrentDate);
            tLJAGetSchema.setOperator(tGI.Operator);
            tLJAGetSchema.setMakeDate(CurrentDate);
            tLJAGetSchema.setMakeTime(CurrentTime);
            tLJAGetSchema.setModifyDate(CurrentDate);
            tLJAGetSchema.setModifyTime(CurrentTime);
            
            mMMap.put(tLJAGetSchema, SysConst.DELETE_AND_INSERT);
            
            LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
            ref.transFields(tLJFIGetSchema, tLJAGetSchema);
            tLJFIGetSchema.setGetMoney(mLJSGetSchema.getSumGetMoney());
            mMMap.put(tLJFIGetSchema, SysConst.DELETE_AND_INSERT);
            
            LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
            tLJSGetDrawDB.setGetNoticeNo(mGetNoticeNo);
            tLJSGetDrawDB.setContNo(mContNo);
            
            LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();
            tLJSGetDrawSet = tLJSGetDrawDB.query();
            for(int i=1;i<= tLJSGetDrawSet.size(); i++)
            {
            	LJSGetDrawSchema tLJSGetDrawSchema = tLJSGetDrawSet.get(i).getSchema();
            	tLJSGetDrawSchema.setHandler(mHandler);
            	tLJSGetDrawSchema.setDrawer(mLJSGetSchema.getDrawer());
            	tLJSGetDrawSchema.setDrawerID(mLJSGetSchema.getDrawerID());
            	tLJSGetDrawSchema.setFeeFinaType("YEI");
            	tLJSGetDrawSchema.setOperator(tGI.Operator);
            	tLJSGetDrawSchema.setModifyDate(CurrentDate);
            	tLJSGetDrawSchema.setModifyTime(CurrentTime);
            	mMMap.put(tLJSGetDrawSchema, SysConst.UPDATE);
            }
        }
        else 
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmAppAccBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有选择转入投保人账户!";
            this.mErrors.addOneError(tError);
            return false;
        }
        

        String tAppntNo = mLCContSchema.getAppntNo();
        LCAppAccDB tLCAppAccDB = new LCAppAccDB();
        tLCAppAccDB.setCustomerNo(tAppntNo);
        if(!tLCAppAccDB.getInfo())
        {
        	AppAcc tAppAcc = new AppAcc();
            MMap map = new MMap();
            LCAppAccTraceSchema schema = new LCAppAccTraceSchema();
            schema.setCustomerNo(mLCContSchema.getAppntNo());
            schema.setOtherNo(mLCContSchema.getContNo());
            schema.setOtherType("2");
            schema.setMoney(0);
            schema.setOperator(mLCContSchema.getOperator());

            map.add(tAppAcc.accShiftToXQY(schema, "0"));

            VData dd = new VData();
            dd.add(map);
            PubSubmit pub = new PubSubmit();
            if(!pub.submitData(dd, ""))
            {
                System.out.println(pub.mErrors.getErrContent());
                mErrors.addOneError("自动创建帐户,提交失败!");
                return false;
            }
        }
        
        tLCAppAccDB.setCustomerNo(tAppntNo);
        if(!tLCAppAccDB.getInfo())
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmAppAccBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询投保人账户信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        LCAppAccSchema tLCAppAccSchema = tLCAppAccDB.getSchema();
        
        double oldAccBala = tLCAppAccSchema.getAccBala();
        double oldAccGetMoney = tLCAppAccSchema.getAccGetMoney();
        
        double newAccBala = oldAccBala + mLJSGetSchema.getSumGetMoney();
        double newAccGetMoney = oldAccGetMoney + mLJSGetSchema.getSumGetMoney();
        
        tLCAppAccSchema.setAccBala(newAccBala);
        tLCAppAccSchema.setAccGetMoney(newAccGetMoney);
        tLCAppAccSchema.setModifyDate(CurrentDate);
        tLCAppAccSchema.setModifyTime(CurrentTime);
        tLCAppAccSchema.setOperator(tGI.Operator);
        
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        ref.transFields(tLCAppAccTraceSchema, tLCAppAccSchema);
        tLCAppAccTraceSchema.setMoney(mLJSGetSchema.getSumGetMoney());
        
        String sql = "  select coalesce(max(cast(serialno as integer)),0)+1 serialno from lcappacctrace "
            + "where customerno = '" + tAppntNo + "'";
        
        ExeSQL tExeSQL = new ExeSQL();
        String tSerialNo = tExeSQL.getOneValue(sql);

        tLCAppAccTraceSchema.setAccType("1");//0抵扣或领取,1转入
        tLCAppAccTraceSchema.setSerialNo(tSerialNo);
        tLCAppAccTraceSchema.setOtherNo(mContNo);
        tLCAppAccTraceSchema.setOtherType("2"); 
        tLCAppAccTraceSchema.setDestSource("11"); 
        /*
        查询LDCODE CODETYPE='appaccdestsource'
        	用途：
        	保单服务费用抵扣（后相关号码显示受理号）
        	续期抵扣（后相关号码显示保单号）
        	新契约抵扣（后相关号码显示保单号）
        	领取（后相关号码显示退费通知号）；
        	来源：
        	保单服务退费转入（后相关号码显示受理号）
        	新契约溢交转入（后相关号码显示保单号）
        	续收溢交转入（后相关号码显示保单号）
        	续收预交转入（后相关号码显示保单号）
        	*/
        tLCAppAccTraceSchema.setConfirmDate(CurrentDate);
        tLCAppAccTraceSchema.setConfirmTime(CurrentTime);
        tLCAppAccTraceSchema.setMakeDate(CurrentDate);
        tLCAppAccTraceSchema.setMakeTime(CurrentTime);
        tLCAppAccTraceSchema.setModifyDate(CurrentDate);
        tLCAppAccTraceSchema.setModifyTime(CurrentTime);
        tLCAppAccTraceSchema.setOperator(tGI.Operator);
        tLCAppAccTraceSchema.setBakNo(mGetNoticeNo);
        
        mMMap.put(tLCAppAccSchema, SysConst.UPDATE);
        mMMap.put(tLCAppAccTraceSchema, SysConst.INSERT);
        
        saveData.clear();
        saveData.add(mMMap);
        return true;
    }
    
    /**
     * 锁定动作。
     * @param cLCContSchema
     * @return
     */
    private MMap lockLJAGet(String aContNo)
    {
        MMap tMMap = null;
        /**满期给付锁定标志为："M2"*/
        String tLockNoType = "M2";
        /** 锁定有效时间*/
        String tAIS = "3600";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", aContNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }
}
