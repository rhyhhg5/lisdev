package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.db.LJSPayGrpBDB;
import com.sinosoft.lis.vschema.LJSPayGrpBSet;
import com.sinosoft.lis.db.LJSPayPersonBDB;
import com.sinosoft.lis.vschema.LJSPayPersonBSet;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 改变财务状态
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.3
 */
public class ChangeFinFeeStateBL
{
    /** 错误处理类 */
    public  CErrors mErrors = new CErrors();
    private LJSPaySchema mLJSPaySchema = null;   //财务应收信息，只需要getNoticeNo即可
    private GlobalInput mGlobalInput = null;  //完整用户信息
    private String mState = null;   //变更后的状态
    private MMap map = new MMap();

    /**
     * 传入变更后的状态
     * @param state String
     */
    public ChangeFinFeeStateBL(String state)
    {
        mState = state;
    }

    /**
     * 改变续期财务数据状态
     * @return boolean：成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(getSubmitMap(cInputData, cOperate) == null
           && mErrors.needDealError())
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            mErrors.addOneError("改变应收状态出错。");
            return false;
        }

        return true;
    }

    /**
     * 改变续期财务数据状态
     * @return MMap：处理后的数据集
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }
        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作的合法性
     * @return boolean
     */
    private boolean checkData()
    {
        if(mState == null || mState.equals(""))
        {
            mErrors.addOneError("请传入所需状态");
            return false;
        }

        return true;
    }

    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
        LJSPayBSet set = tLJSPayBDB.query();
        if(set.size() == 0)
        {
            System.out.println("没有查询到应收记录号为"
                                + mLJSPaySchema.getGetNoticeNo()
                                + "的应收记录， 可能不是续期业务");
            return false;
        }

        LJSPayBSchema tLJSPayBSchema = set.get(1);

        //团单
        if(tLJSPayBSchema.getOtherNoType().equals("1"))
        {
            if(!dealLJSPayB(tLJSPayBSchema.getSchema()))
            {
                return false;
            }

            LJSPayGrpBDB tLJSPayGrpBDB = new LJSPayGrpBDB();
            tLJSPayGrpBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
            LJSPayGrpBSet tLJSPayGrpBSet = tLJSPayGrpBDB.query();
            for(int i = 1; i <= tLJSPayGrpBSet.size(); i++)
            {
                tLJSPayGrpBSet.get(i).setDealState("4");
                tLJSPayGrpBSet.get(i).setOperator(mGlobalInput.Operator);
                tLJSPayGrpBSet.get(i).setModifyDate(PubFun.getCurrentDate());
                tLJSPayGrpBSet.get(i).setModifyTime(PubFun.getCurrentTime());
            }
            map.put(tLJSPayGrpBSet, "UPDATE");
        }
        //个单
        else if(tLJSPayBSchema.getOtherNoType().equals("2"))
        {
            if(!dealLJSPayB(tLJSPayBSchema.getSchema()))
            {
                return false;
            }

            LJSPayPersonBDB tLJSPayPersonBDB = new LJSPayPersonBDB();
            tLJSPayPersonBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
            LJSPayPersonBSet tLJSPayPersonBSet = tLJSPayPersonBDB.query();
            for(int i = 1; i <= tLJSPayPersonBSet.size(); i++)
            {
                tLJSPayPersonBSet.get(i).setDealState("4");
                tLJSPayPersonBSet.get(i).setOperator(mGlobalInput.Operator);
                tLJSPayPersonBSet.get(i).setModifyDate(PubFun.getCurrentDate());
                tLJSPayPersonBSet.get(i).setModifyTime(PubFun.getCurrentTime());
            }
            map.put(tLJSPayPersonBSet, "UPDATE");
        }

        return true;
    }

    /**
     * 变更LJSPayB的状态
     * @param schema LJSPayBSchema：应收备份表
     * @return boolean：处理成功true
     */
    private boolean dealLJSPayB(LJSPayBSchema schemaBeforeDealt)
    {
        LJSPayBSchema tLJSPayBSchema = schemaBeforeDealt.getSchema();

        if(tLJSPayBSchema.getDealState().equals("1")
            || tLJSPayBSchema.getDealState().equals("2")
             || tLJSPayBSchema.getDealState().equals("3"))
        {
            mErrors.addOneError("状态"+ tLJSPayBSchema.getDealState()+ "不能变更");
            return false;
        }

        tLJSPayBSchema.setDealState(mState);
        tLJSPayBSchema.setOperator(mGlobalInput.Operator);
        tLJSPayBSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSPayBSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLJSPayBSchema, "UPDATE");

        return true;
    }

    /**
     * 得到传入的数据
     * @param data VData: 包括LJSPaySchema， GlobalInput
     * @return boolean：成功true
     */
    private boolean getInputData(VData data)
    {
        mGlobalInput = (GlobalInput)data
                       .getObjectByObjectName("GlobalInput", 0);
        mLJSPaySchema = (LJSPaySchema)data
                        .getObjectByObjectName("LJSPaySchema", 0);

        if(mLJSPaySchema == null || mLJSPaySchema.getGetNoticeNo() == null
            || mLJSPaySchema.getGetNoticeNo().equals("")
             || mGlobalInput == null || mGlobalInput.Operator == null)
        {
            mErrors.addOneError("没有传入收数据");
            return false;
        }


        return true;
    }

    public static void main(String[] a)
    {
        LJSPaySchema schema = new LJSPaySchema();
        schema.setGetNoticeNo("31000003143");

        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";

        VData d = new VData();
        d.add(g);
        d.add(schema);

        ChangeFinFeeStateBL bl = new ChangeFinFeeStateBL("4");
        if(!bl.submitData(d, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}

