package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>
 * 用户接口类，接收页面传入的数据，传入后台进行批量抽档业务逻辑的处理
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class IndiDueFeeMultiUI {

    //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public IndiDueFeeMultiUI() {
    }

    public static void main(String[] args) {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setGetStartDate(PubFun.getCurrentDate());
        tLCPolSchema.setPayEndDate(PubFun.getCurrentDate());
        //tLCPolSchema.setPayEndDate("2004-2-25");
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("StartDate", "2006-05-01");
        tTransferData.setNameAndValue("EndDate", "2006-7-23");
        tTransferData.setNameAndValue("ManageCom", "86");
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tTransferData);


       /* VData tv = new VData();
        //HashMap map = (HashMap)mInputData.getObjectByObjectName("HashMap", 0);
        //考虑添加系统描述：针对特约保单的催收 1-都催收 2-只催收有特约的 3-只催收没有特约的
        //mDuebySpecFlag,Spec
        tv.add(tLCPolSchema);
        tv.add(tGI);*/
        IndiDueFeeMultiUI tIndiDueFeeMultiUI = new IndiDueFeeMultiUI();
        if (tIndiDueFeeMultiUI.submitData(tVData, "INSERT")) {
            System.out.println("个单批处理催收成功");
        } else {
            System.out.println("个单批处理催收失败：" +
                               tIndiDueFeeMultiUI.mErrors.getFirstError());
        }

    }

    /**
     * 用户接口类，接收页面传入的数据，传入后台进行批量抽档业务逻辑的处理
     * @param cInputData VData:包括
     * 1、	GlobalInput对象，完整的登陆用户信息
     * 2、	TransferData对象，包含StartDate， EndDate， ManageCom
     * @param cOperate String：操作类型，此可为空字符串“”
     * @return boolean，成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        IndiDueFeeMultiBL tIndiDueFeeMultiBL = new IndiDueFeeMultiBL();
        System.out.println("Start IndiDueFeeMulti UI Submit...");
        tIndiDueFeeMultiBL.submitData(mInputData, cOperate);

        System.out.println("End IndiDueFeeMulti UI Submit...");

        mInputData = null;
        //如果有需要处理的错误，则返回
        if (tIndiDueFeeMultiBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tIndiDueFeeMultiBL.mErrors);
            return false;
        }
        System.out.println("error num=" + mErrors.getErrorCount());
        return true;
    }

}
