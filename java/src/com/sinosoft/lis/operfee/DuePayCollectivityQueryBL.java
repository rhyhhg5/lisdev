 package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class DuePayCollectivityQueryBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();

  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  /** 应收集体交费表 */
  private LJSPayGrpSet  mLJSPayGrpSet = new LJSPayGrpSet() ;
  private LJSPayGrpSchema  mLJSPayGrpSchema = new LJSPayGrpSchema() ;  
  
  public DuePayCollectivityQueryBL() {}

  public static void main(String[] args) {
  }

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //进行业务处理，查询到数据返回错误
	    if (!queryLJSPayGrp())
	      return false;

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 检验查询条件 
    mLJSPayGrpSchema.setSchema((LJSPayGrpSchema)cInputData.getObjectByObjectName("LJSPayGrpSchema",0));

    if(mLJSPayGrpSchema == null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "DuePayCollectivityQueryBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "请输入查询条件!";
      this.mErrors.addOneError(tError) ;
      return false;
    }    
    return true;
  }

  /**
   * 查询应收集体交费表信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean queryLJSPayGrp()
  {
  	// 应收集体交费表信息
  	
      LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
      tLJSPayGrpDB.setSchema(mLJSPayGrpSchema);
      mLJSPayGrpSet = tLJSPayGrpDB.query();
      if (tLJSPayGrpDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJSPayGrpDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "DuePayCollectivityQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "应收集体交费表查询失败!";
      this.mErrors.addOneError(tError);
      mLJSPayGrpSet.clear();
      return false;
    }
    if (mLJSPayGrpSet.size() > 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "DuePayCollectivityQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "找到相关数据!不能催收";
      this.mErrors.addOneError(tError);
      mLJSPayGrpSet.clear();
      return false;
    }
System.out.println("LJSPayGrp num="+mLJSPayGrpSet.size());
	return true;
  }

}
