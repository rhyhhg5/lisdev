package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.vschema.LDCodeSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CardQueryReportBL {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = new TransferData();
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private SSRS mSSRS;
    private String mCardType = ""; //卡号类型
    private String mCardNo = ""; //卡号
    private String mHandleDate = ""; //下发日期
    private String mSQL = "";


    public CardQueryReportBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {

            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }
        mCardType = (String) mTransferData.getValueByName("CardType");
        mCardNo = (String) mTransferData.getValueByName("CardNo");
        mHandleDate = (String) mTransferData.getValueByName("HandleDate");
        mSQL = (String) mTransferData.getValueByName("SQL");

        return true;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
        String tSQL = mSQL;
        System.out.println("ffffffff"+tSQL);
        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {

        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("CardQueryReport.vts", "printer"); //最好紧接着就初始化xml文档

        String[] title = {"卡号", "类型", "面值", "状态",
                         "被保险人", "生效日期", "终止日期", "录入人员", "录入日期"};
        xmlexport.addListTable(getListTable(), title);
       // xmlexport.outputDocumentToFile("C:\\", "GrpPPolPauseList");
        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[9];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }

        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {
        CardQueryReportBL p = new CardQueryReportBL();
        LPEdorAppSchema schema = new LPEdorAppSchema();
        schema.setEdorAcceptNo("20050915000022");
        VData v = new VData();
        v.add(schema);
        if (!p.submitData(v, "")) {
            System.out.println(p.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }
    }

    private void jbInit() throws Exception {
    }



}
