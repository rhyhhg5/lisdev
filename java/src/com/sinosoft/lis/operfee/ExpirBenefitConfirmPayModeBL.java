package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.util.*;
import com.sinosoft.lis.bq.*;

//程序名称：ExpirBenefitConfirmPayModeBL.java
//程序功能：修改给付方式
//创建日期：2010-06-10 16:06
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class ExpirBenefitConfirmPayModeBL 
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData saveData = new VData();
    private MMap mMMap = new MMap();
    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    private String mContNo = null;  //保单号
    private String mGetNoticeNo = null;  //通知书号
    private String mDrawerCode = null;  //给付对象 0-投保人 1-被保人
    private String mPayMode = null;  //给付方式 1-现金 4-银行转账
    
    private String mBankCode = null;
    private String mAccName = null;
    private String mBankAccNo = null;

    private ExeSQL mExeSQL = new ExeSQL();

    //个人保单表
    private LCContSchema mLCContSchema = new LCContSchema();

    //应付个人交费表
    private	LJSGetSchema mLJSGetSchema = new LJSGetSchema();
    
    public ExpirBenefitConfirmPayModeBL() {
    }

    public static void main(String[] args) {

    }
    
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData)
    {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0); 
        if(tGI == null || mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmPayModeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mGetNoticeNo = (String)mTransferData.getValueByName("getnoticeno");  //通知书号
        
        if(mGetNoticeNo == null || mGetNoticeNo.equals(""))
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmPayModeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入通知书号-mGetNoticeNo失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LJSGetDB tLJSGetDB = new LJSGetDB();
        tLJSGetDB.setGetNoticeNo(mGetNoticeNo);
        if(!tLJSGetDB.getInfo())
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmPayModeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "通过mGetNoticeNo获取LJSGet失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLJSGetSchema.setSchema(tLJSGetDB.getSchema());
        
        mContNo = (String)mTransferData.getValueByName("contno");
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if(!tLCContDB.getInfo())
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmPayModeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "通过tContNo获取LCCont失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mLCContSchema.setSchema(tLCContDB.getSchema());

        mDrawerCode = (String)mTransferData.getValueByName("drawercode");  //给付对象 0-投保人 1-被保人
        if(mDrawerCode == null || mDrawerCode.equals(""))
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmPayModeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入给付对象-mDrawerCode失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mPayMode = (String)mTransferData.getValueByName("paymode");  
        if(mPayMode == null || mPayMode.equals(""))
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmPayModeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入缴费方式-mPayMode失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mBankCode = (String)mTransferData.getValueByName("bankcode");  
        mAccName = (String)mTransferData.getValueByName("accname");  
        mBankAccNo = (String)mTransferData.getValueByName("bankaccno"); 
        
        if(mPayMode.equals("4"))
        {
            if(mBankCode == null || mBankCode.equals(""))
            {
            	CError tError = new CError();
                tError.moduleName = "ExpirBenefitConfirmPayModeBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "传入缴费方式-mBankCode失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if(mAccName == null || mAccName.equals(""))
            {
            	CError tError = new CError();
                tError.moduleName = "ExpirBenefitConfirmPayModeBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "传入缴费方式-mAccName失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if(mBankAccNo == null || mBankAccNo.equals(""))
            {
            	CError tError = new CError();
                tError.moduleName = "ExpirBenefitConfirmPayModeBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "传入缴费方式-mBankAccNo失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 校验是否有理赔，保全
     * @return boolean：通过true，否则false
     */
    public boolean checkData() 
    {
        return true;
    }
    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public VData getSubmitData(VData cInputData, String cOperate) 
    {
        if (!getInputData(cInputData)) 
        {
            return null;
        }
        System.out.println("After getinputdata");
        if (!checkData()) 
        {
            return null;
        }
        //进行业务处理
        if (!dealData()) 
        {
            return null;
        }

        return saveData;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public MMap getSubmitMMap(VData cInputData, String cOperate) 
    {
        getSubmitData(cInputData, cOperate);
        return mMMap;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate) 
    {
        VData data = getSubmitData(cInputData, cOperate);
        if (data == null) 
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) 
        {
            this.mErrors.addOneError("PubSubmit:生成给付失败!");
            return false;
        }
        return true;
    }

    /**
     * 生成给付记录
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
    	LJAGetDB tLJAGetDB = new LJAGetDB();
    	tLJAGetDB.setActuGetNo(mGetNoticeNo);
    	boolean tConfFlag = false;//给付确认标志，false-未给付确认
    	if(tLJAGetDB.getInfo())
       	{
    		tConfFlag = true;
    		System.out.println(mGetNoticeNo+"已给付确认");
    		
    		if("1".equals(tLJAGetDB.getBankOnTheWayFlag()))
           	{
           		CError tError = new CError();
           		tError.moduleName = "ExpirBenefitCancelBL";
           		tError.functionName = "cancelData";
           		tError.errorMessage = "给付任务" + tLJAGetDB.getOtherNo() + "银行在途";
           		System.out.println(tError.errorMessage);
           		this.mErrors.addOneError(tError);
           		return false;
           	}
            if(null != tLJAGetDB.getEnterAccDate() && !tLJAGetDB.getEnterAccDate().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ExpirBenefitCancelBL";
                tError.functionName = "cancelData";
                tError.errorMessage = "给付任务" + tLJAGetDB.getOtherNo() + "银行已经回盘";
                System.out.println(tError.errorMessage);
                this.mErrors.addOneError(tError);
                return false;
            }
            if(null != tLJAGetDB.getConfDate() && !tLJAGetDB.getConfDate().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ExpirBenefitCancelBL";
                tError.functionName = "cancelData";
                tError.errorMessage = "给付任务" + tLJAGetDB.getOtherNo() + "已经付费成功";
                System.out.println(tError.errorMessage);
                this.mErrors.addOneError(tError);
                return false;
            }
       	}
        //存储给付对象
        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
        tLPEdorEspecialDataSchema.setEdorAcceptNo(mGetNoticeNo);
        tLPEdorEspecialDataSchema.setEdorNo(mGetNoticeNo);
        tLPEdorEspecialDataSchema.setEdorType("MJ");
        tLPEdorEspecialDataSchema.setDetailType("DrawerCode");
        tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
        tLPEdorEspecialDataSchema.setEdorValue(mDrawerCode);
        
        mMMap.put(tLPEdorEspecialDataSchema, SysConst.DELETE_AND_INSERT);
        
        mLJSGetSchema.setPayMode(mPayMode);
        if(mDrawerCode.equals("0")) //投保人
        {
        	mLJSGetSchema.setDrawer(mLCContSchema.getAppntName());
        	mLJSGetSchema.setDrawerID(mLCContSchema.getAppntIDNo());
        	if(mPayMode.equals("4") || mPayMode.equals("9"))
        	{
        		mLJSGetSchema.setBankCode(mBankCode);
            	mLJSGetSchema.setBankAccNo(mBankAccNo);
            	mLJSGetSchema.setAccName(mAccName);
        	}
        	else
        	{
        		mLJSGetSchema.setBankCode(null);
            	mLJSGetSchema.setBankAccNo(null);
            	mLJSGetSchema.setAccName(null);
        	}
        }
        else if(mDrawerCode.equals("1")) //被保人
        {
//        	取得被保人信息
        	String getInsuredNo = "select distinct insuredno from lcget a,lmdutygetalive b "
                    			+" where a.getdutycode = b.getdutycode and contno ='" + mLCContSchema.getContNo() + "' ";
        	String tInsuredNo = mExeSQL.getOneValue(getInsuredNo);
        	LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        	tLCInsuredDB.setContNo(mContNo);
        	tLCInsuredDB.setInsuredNo(tInsuredNo);
        	if(!tLCInsuredDB.getInfo())
        	{
        		CError tError = new CError();
                tError.moduleName = "ExpirBenefitConfirmPayModeBL";
                tError.functionName = "dealData";
                tError.errorMessage = "获取被保人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
        	}
        	mLJSGetSchema.setDrawer(tLCInsuredDB.getName());
        	mLJSGetSchema.setDrawerID(tLCInsuredDB.getIDNo());
        	if(mPayMode.equals("4") || mPayMode.equals("9"))
        	{
        		mLJSGetSchema.setBankCode(mBankCode);
            	mLJSGetSchema.setBankAccNo(mBankAccNo);
            	mLJSGetSchema.setAccName(mAccName);
        	}
        	else
        	{
        		mLJSGetSchema.setBankCode(null);
            	mLJSGetSchema.setBankAccNo(null);
            	mLJSGetSchema.setAccName(null);
        	}
        }
        
        mLJSGetSchema.setOperator(tGI.Operator);
        mLJSGetSchema.setModifyDate(CurrentDate);
        mLJSGetSchema.setModifyTime(CurrentTime);
        
        mMMap.put(mLJSGetSchema, SysConst.UPDATE);
        
        if(tConfFlag)
        {
        	LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    		tLJAGetSchema.setSchema(tLJAGetDB.getSchema());
    		tLJAGetSchema.setPayMode(mLJSGetSchema.getPayMode());
    		tLJAGetSchema.setBankAccNo(mLJSGetSchema.getBankAccNo());
    		tLJAGetSchema.setBankCode(mLJSGetSchema.getBankCode());
    		tLJAGetSchema.setAccName(mLJSGetSchema.getAccName());
    		tLJAGetSchema.setOperator(tGI.Operator);
    		tLJAGetSchema.setModifyDate(CurrentDate);
    		tLJAGetSchema.setModifyTime(CurrentTime);
    		mMMap.put(tLJAGetSchema, SysConst.UPDATE);
        }
        
        LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
        tLJSGetDrawDB.setGetNoticeNo(mGetNoticeNo);
        tLJSGetDrawDB.setContNo(mContNo);
        
        LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();
        tLJSGetDrawSet = tLJSGetDrawDB.query();
        for(int i=1;i<= tLJSGetDrawSet.size(); i++)
        {
        	LJSGetDrawSchema tLJSGetDrawSchema = tLJSGetDrawSet.get(i).getSchema();
        	tLJSGetDrawSchema.setDrawer(mLJSGetSchema.getDrawer());
        	tLJSGetDrawSchema.setDrawerID(mLJSGetSchema.getDrawerID());
        	tLJSGetDrawSchema.setBankCode(mLJSGetSchema.getBankCode());
        	tLJSGetDrawSchema.setBankAccNo(mLJSGetSchema.getBankAccNo());
        	tLJSGetDrawSchema.setAccName(mLJSGetSchema.getAccName());
        	tLJSGetDrawSchema.setOperator(tGI.Operator);
        	tLJSGetDrawSchema.setModifyDate(CurrentDate);
        	tLJSGetDrawSchema.setModifyTime(CurrentTime);
        	mMMap.put(tLJSGetDrawSchema, SysConst.UPDATE);
        }
        
        saveData.clear();
        saveData.add(mMMap);
        return true;
    }

}
