package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class IndiDueFeeUI {

  //业务处理相关变量
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();

  public IndiDueFeeUI() {
  }
  public static void main(String[] args) {

      GlobalInput tGI = new GlobalInput();
      tGI.ComCode="86";
      tGI.Operator="wuser";
      tGI.ManageCom="86";
      TransferData tempTransferData=new TransferData();
      tempTransferData.setNameAndValue("StartDate","2005-01-01");
      tempTransferData.setNameAndValue("EndDate","2006-3-18");

      LCContSchema  tLCContSchema = new LCContSchema();  // 个人保单表
      tLCContSchema.setContNo("00002581101");
      VData tVData = new VData();
      tVData.add(tGI);
      tVData.add(tLCContSchema);
      tVData.add(tempTransferData);
      IndiDueFeeUI tIndiDueFeeUI = new IndiDueFeeUI();
      tIndiDueFeeUI.submitData(tVData,"INSERT");

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    IndiDueFeeBL tIndiDueFeeBL=new IndiDueFeeBL();
    System.out.println("Start IndiDueFee UI Submit...");
    tIndiDueFeeBL.submitData(mInputData,cOperate);
    System.out.println("End IndiDueFee UI Submit...");

    mInputData=null;
    //如果有需要处理的错误，则返回
    if (tIndiDueFeeBL .mErrors .needDealError() )
       {
       this.mErrors .copyAllErrors(tIndiDueFeeBL.mErrors ) ;
       return false;
       }
System.out.println("error num="+mErrors.getErrorCount());
    return true;
  }

}
