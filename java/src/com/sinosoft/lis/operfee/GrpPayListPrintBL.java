package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.vschema.LDCodeSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpPayListPrintBL {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = new TransferData();
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private SSRS mSSRS;
    private String mSerialNo = ""; //批次号
    private String mGrpContNo = ""; //保单号
    private boolean mLoadFlag = false;   //是否无名单标记

    public GrpPayListPrintBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {

            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }
        mSerialNo = (String) mTransferData.getValueByName("SerialNo");
        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        String flag = (String) mTransferData.getValueByName("LoadFlag");
        System.out.println(mSerialNo);
        System.out.println(mGrpContNo);
        System.out.println(flag);

        if(flag != null && flag.equals("WMD"))
        {
            this.mLoadFlag = true;
        }

        return true;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
        StringBuffer sqlSB = new StringBuffer();
        String sqlCons = "";
        if (!(mSerialNo == null || mSerialNo.equals(""))) {
            sqlCons = " and b.SerialNo='" + mSerialNo + "'";
        }
        if (!(mGrpContNo == null || mGrpContNo.equals(""))) {
            sqlCons = sqlCons + " and a.GrpContNo='" + mGrpContNo + "'";
        }
        String dealWMD = " exists (select 1 from LCCont where grpContNo = a.grpContNo and poltype = '1')";
        dealWMD = mLoadFlag ? "  and " + dealWMD : "  and not " + dealWMD;

        sqlSB.append("select distinct b.PayNo,a.GrpName,a.GrpContNo,(select varchar(sum(SumDuePayMoney)) from LJAPayGrp where grpcontno =a.GrpContNo and PayNo=b.PayNo),(select min(LastPayToDate) from LJAPayGrp where grpcontno =a.GrpContNo and PayNo=b.PayNo),varchar(b.SumActuPayMoney),b.MakeDate,")
            .append(" (select userName from LDUser where userCode = b.operator) ")
          //added by suyanlin start
            .append(",c.Name, (select Codename from LDCode where Codetype = 'sex' and Code = c.Sex), c.Mobile")
            .append(" from LCGrpCont a,LJAPay b,LAAgent c")
//            .append(" from LCGrpCont a,LJAPay b")
//            .append("  where 1=1 ")
            .append(" where 1=1 and a.Agentcode = c.Agentcode ")
          //added by suyanlin end
            .append(" AND a.GrpContNo=b.IncomeNo and b.IncomeType='1'")
            .append(" and a.AppFlag='1'")
            .append(sqlCons)
            .append(dealWMD)
            .append(" and b.GetNoticeNo in (select GetNoticeNo from LJSPayB WHERE OtherNoType='1' and DealState='1')")
            //added by suyanlin start
//            .append(" group by b.PayNo,a.GrpName,a.GrpContNo,b.SumActuPayMoney,b.MakeDate,b.Operator")
            .append(" group by b.PayNo,a.GrpName,a.GrpContNo,b.SumActuPayMoney,b.MakeDate,b.Operator,c.Name,sex,c.Mobile")
             //added by suyanlin end
            .append(" order by a.GrpContNo ,b.PayNo desc")
            ;

        String tSQL = sqlSB.toString();
        System.out.println(tSQL);
        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {
        TextTag tag = new TextTag();
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpPayList.vts", "printer"); //最好紧接着就初始化xml文档

        //得到公司名
        String grpName = this.getCompanyName();
        System.out.println("ManageName" + grpName);
        if (grpName == null) {
            return false;
        }
        tag.add("ManageName", grpName);
        tag.add("PrintOperator", mGlobalInput.Operator);
        tag.add("PrintDate", PubFun.getCurrentDate());

        xmlexport.addTextTag(tag);

        String[] title = {"收据号", "投保人", "保单号", "应收金额", "应缴时间","实收金额",
                         "核销时间", "经办人", "代理人姓名", "代理人性别", "代理人电话"};
        xmlexport.addListTable(getListTable(), title);
       // xmlexport.outputDocumentToFile("C:\\", "GrpPayList");
        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
//        updated by suyanlin at 2007-11-16 15:05 start
//        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
//            String[] info = new String[9];
//            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
//                info[j - 1] = mSSRS.GetText(i, j);
//            }
          for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[12];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
           }
////        updated by suyanlin at 2007-11-16 15:05 end

           //计算交费人数
            StringBuffer sql = new StringBuffer();
            if(isWMD(info[2]))
            {
                sql.append("select sum(peoples2Input) ")
                    .append("from LCNoNamePremTrace a, LJAPay b ")
                    .append("where a.getNoticeNo = b.getNoticeNo ")
                    .append("   and b.PayNo = '")
                    .append(info[0])
                    .append("' ");
            }
            else
            {
                sql.append("select peoples2 ")
                    .append("from LCGrpCont ")
                    .append("where grpContNo = '").append(info[2])
                    .append("' ");
            }
            String peoples2 = new ExeSQL().getOneValue(sql.toString());
            if(peoples2.equals("") || peoples2.equals("null"))
            {
                peoples2 = "0";
            }
            info[11] = peoples2;
            tListTable.add(info);
        }
        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 校验是否无名单
     * @param grpContNo String
     * @return boolean
     */
    private boolean isWMD(String grpContNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select 1 from LCCont ")
            .append("where grpContNo = '").append(grpContNo)
            .append("'  and polType = '1' ");
        String t = new ExeSQL().getOneValue(sql.toString());
        if(t.equals("") || t.equals("null"))
        {
            return false;
        }

        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {
        GrpPayListPrintBL p = new GrpPayListPrintBL();
        LPEdorAppSchema schema = new LPEdorAppSchema();
        schema.setEdorAcceptNo("20050915000022");
        VData v = new VData();
        v.add(schema);
        if (!p.submitData(v, "")) {
            System.out.println(p.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }
    }

    private void jbInit() throws Exception {
    }

    //查询公司名称
    private String getCompanyName() {
        //通过LDCode得到保单号，然后得到公司名称
        String manageCode = mGlobalInput.ManageCom;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.setCode(manageCode);
        LDCodeSet set = tLDCodeDB.query();
        if (set == null || set.size() == 0) {

            mErrors.addOneError("没有查到公司名");
            System.out.println("没有查到公司名" + tLDCodeDB.mErrors.getErrContent());
            return null;
        } else {
            return set.get(1).getCodeName();
        }
    }

}
