package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.xb.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 接收IndiDueFeeBackUI传入的数据，进行业务逻辑的处理。
 * 通过生成对应实收记录的负费记录方式进行原实收保费的抵消，实现需求的实收保费转出。
 * 1、	生成对应的负费记录，实现转出财务数据LJAPayPerson、LJAPayGrp、LJAPay操作
 * 2、	回退保单数据到续保续保抽档前状态。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class MOmniIndiDueFeeBackBL
{
    /**错误的容器。*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;  //完整的操作员信息
    private LJSPayBSchema mLJSPayBSchema = null;  //应收备份数据。
    private LJAPaySchema mLJAPaySchema = null;  //本次回退的应收数据。
    private LCContSchema mLCContSchema = null;  //本保单信息
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mPayNo = null;
    private boolean mIsXBPol = false;  //险种是续保险种标志
    private boolean mHasXBPol = false; //本次实收是否有续保险种
    private Reflections ref = new Reflections();
    private LCRnewStateLogSchema mLCRnewStateLogSchema = null;  //险种续保日志
    private boolean mDoBackOtherTask = false;  //其他任务正在做转出

    private MMap map = new MMap();  //待提交的数据集合

    public MOmniIndiDueFeeBackBL()
    {
    }

    /**
     * 操作的提交方法，进行续期续保实收保费转出业务逻辑处理并提交数据库。
     * 开始时将应收状态更新为正转出5，成功后为转出6，若操作失败则回退为核销成功1
     * @param cInputData VData:
     * A.	GlobalInput对象，完整的登陆用户信息。
     * B.	LJSPayB对象，应收记录备份信息，只需GetNoticeNo即可。
     * @param cOperate String：此为“”
     * @return boolean：boolean, 成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if(!dealSubmit(cInputData, cOperate))
            {
//                if(!mDoBackOtherTask)
//                {
//                    dealUrgeLog(SysConst.DELETE);
//                }
                return false;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            dealUrgeLog(SysConst.DELETE);
            return false;
        }
        dealUrgeLog(SysConst.DELETE);
        return true;
    }

    /**
     * 执行回退动作
     * @param cInputData VData：submitData中串入的VData
     * @param cOperate String：“”
     * @return boolean：boolean, 成功true，否则false
     */
    private boolean dealSubmit(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }
        
        if(!checkData())
        {
            return false;
        }

        //进行业务处理
        if(!dealData())
        {
            return false;
        }

        VData d = new VData();
        d.add(map);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            System.out.println(p.mErrors.getErrContent());
            mErrors.addOneError("提交数据错误");
            return false;
        }

        return true;
    }

    /**
     * 进行业务逻辑处理。
     i.	生成对应的负费记录，实现转出财务数据LJAPayPerson、LJAPay操作
     ii.调用类PRnewContBackBL进行保单数据的回退。
     * @return boolean：操作成功true，否则false
     */
    private boolean dealData()
    {
        //得到收据号
        String limit = PubFun.getNoLimit("86110000");
        mPayNo = PubFun1.CreateMaxNo("PayNo",limit);
        if(!backFinace())
        {
            return false;
        }
        
        if(!backInsuacc())
        {
            return false;
        }

        if(!backPol())
        {
            return false;
        }

        //回退保单层的数据
        backDueFeeCont();


        if(!dealAcc())
        {
            return false;
        }

        map.put(changeState(FeeConst.DEALSTATE_BACK_SUCC), "UPDATE");

        return true;
    }
    
    
    /**
     * 反冲帐户金额
     * @return boolean
     */
    private boolean backInsuacc()
    {
    	//查找出续期管理费的轨迹,作为查找相关表的基础表
    	LCInsureAccTraceSchema aLCInsureAccTraceSchema=new LCInsureAccTraceSchema();
    	//续期进入帐户总金额
    	double sumMoney=Double.parseDouble(new ExeSQL().getOneValue("Select Sum(Money) From LCInsureAccTrace Where ContNo='"+mLJSPayBSchema.getOtherNo()+"' and Otherno='"+mLJSPayBSchema.getGetNoticeNo()+"'"));
    	
    	
    	//先对ACC轨迹表进行反冲,创建日期和修改日期都需要重置
    	LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
		tLCInsureAccTraceDB.setContNo(mLJSPayBSchema.getOtherNo());
		tLCInsureAccTraceDB.setOtherNo(mLJSPayBSchema.getGetNoticeNo());
		LCInsureAccTraceSet tLCInsureAccTraceSet =tLCInsureAccTraceDB.query(); 
        if (!(tLCInsureAccTraceSet.size()>0))
        {
            System.out.println("LCInsureAccTrace的otherno"+mLJSPayBSchema.getGetNoticeNo());
            mErrors.addOneError("没有查询到续期轨迹表的记录");
            return false;
        }
        for (int i = 1; i <= tLCInsureAccTraceSet.size(); i++) {
        	LCInsureAccTraceSchema tLCInsureAccTraceSchema=tLCInsureAccTraceSet.get(i);
        	if(tLCInsureAccTraceSchema.getMoneyType().equals("GL"))
        	{
        		aLCInsureAccTraceSchema=tLCInsureAccTraceSet.get(i);
        	}
        	tLCInsureAccTraceSchema.setSerialNo(PubFun1.CreateMaxNo("SERIALNO",tLCInsureAccTraceSchema.getManageCom()));
        	tLCInsureAccTraceSchema.setMoney(-tLCInsureAccTraceSchema.getMoney());
        	tLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
            tLCInsureAccTraceSchema.setMakeDate(mCurrentDate);
            tLCInsureAccTraceSchema.setMakeTime(mCurrentTime);
            tLCInsureAccTraceSchema.setModifyDate(mCurrentDate);
            tLCInsureAccTraceSchema.setModifyTime(mCurrentTime);
            tLCInsureAccTraceSchema.setMoneyNoTax(null);
            tLCInsureAccTraceSchema.setMoneyTax(null);
            tLCInsureAccTraceSchema.setBusiType(null);
            tLCInsureAccTraceSchema.setTaxRate(null); 
    		map.put(tLCInsureAccTraceSchema,"INSERT");
			
		}
        
      //再对ACCFEE轨迹表进行反冲,创建日期和修改日期都需要重置
    	LCInsureAccFeeTraceDB tLCInsureAccFeeTraceDB = new LCInsureAccFeeTraceDB();
		tLCInsureAccFeeTraceDB.setContNo(mLJSPayBSchema.getOtherNo());
		tLCInsureAccFeeTraceDB.setOtherNo(mLJSPayBSchema.getGetNoticeNo());
		LCInsureAccFeeTraceSet tLCInsureAccFeeTraceSet =tLCInsureAccFeeTraceDB.query(); 
        if (!(tLCInsureAccFeeTraceSet.size()>0))
        {
            System.out.println("LCInsureAccFeeTrace的otherno"+mLJSPayBSchema.getGetNoticeNo());
            mErrors.addOneError("没有查询到续期费用轨迹表的记录");
            return false;
        }
        for (int i = 1; i <= tLCInsureAccFeeTraceSet.size(); i++) {
        	LCInsureAccFeeTraceSchema tLCInsureAccFeeTraceSchema=tLCInsureAccFeeTraceSet.get(i);
        	tLCInsureAccFeeTraceSchema.setSerialNo(PubFun1.CreateMaxNo("SERIALNO",tLCInsureAccFeeTraceSchema.getManageCom()));
        	tLCInsureAccFeeTraceSchema.setFee(-tLCInsureAccFeeTraceSchema.getFee());
        	tLCInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
            tLCInsureAccFeeTraceSchema.setMakeDate(mCurrentDate);
            tLCInsureAccFeeTraceSchema.setMakeTime(mCurrentTime);
            tLCInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
            tLCInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
            tLCInsureAccFeeTraceSchema.setMoneyNoTax(null);
            tLCInsureAccFeeTraceSchema.setMoneyTax(null);
            tLCInsureAccFeeTraceSchema.setBusiType(null);
            tLCInsureAccFeeTraceSchema.setTaxRate(null);    
    		map.put(tLCInsureAccFeeTraceSchema,"INSERT");
			
		}
        
        //维护帐户总表
        LCInsureAccDB tLCInsureAccDB=new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(aLCInsureAccTraceSchema.getPolNo());
        tLCInsureAccDB.setInsuAccNo(aLCInsureAccTraceSchema.getInsuAccNo());
        if(!tLCInsureAccDB.getInfo())
        {
        	System.out.println("LCInsureAcc的polno"+aLCInsureAccTraceSchema.getPolNo());
            mErrors.addOneError("没有查询到万能帐户的记录");
            return false;
        }
        LCInsureAccSchema tLCInsureAccSchema=tLCInsureAccDB.getSchema();
        tLCInsureAccSchema.setInsuAccBala(tLCInsureAccSchema.getInsuAccBala()-sumMoney);
        tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
        tLCInsureAccSchema.setModifyDate(mCurrentDate);
        tLCInsureAccSchema.setModifyTime(mCurrentTime);
        map.put(tLCInsureAccSchema,"UPDATE");
        
        //维护费用总表
        LCInsureAccFeeDB tLCInsureAccFeeDB=new LCInsureAccFeeDB();
        tLCInsureAccFeeDB.setPolNo(aLCInsureAccTraceSchema.getPolNo());
        tLCInsureAccFeeDB.setInsuAccNo(aLCInsureAccTraceSchema.getInsuAccNo());
        if(!tLCInsureAccFeeDB.getInfo())
        {
        	System.out.println("LCInsureAccFee的polno"+aLCInsureAccTraceSchema.getPolNo());
            mErrors.addOneError("没有查询到万能帐户的记录");
            return false;
        }
        LCInsureAccFeeSchema tLCInsureAccFeeSchema=tLCInsureAccFeeDB.getSchema();
        tLCInsureAccFeeSchema.setFee(tLCInsureAccFeeSchema.getFee()-Math.abs(aLCInsureAccTraceSchema.getMoney()));
        tLCInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
        tLCInsureAccFeeSchema.setModifyDate(mCurrentDate);
        tLCInsureAccFeeSchema.setModifyTime(mCurrentTime);
        map.put(tLCInsureAccFeeSchema,"UPDATE");
        

        //再对ACCClass表进行维护
    	LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
		tLCInsureAccClassDB.setPolNo(aLCInsureAccTraceSchema.getPolNo());
		tLCInsureAccClassDB.setInsuAccNo(aLCInsureAccTraceSchema.getInsuAccNo());
		tLCInsureAccClassDB.setPayPlanCode(aLCInsureAccTraceSchema.getPayPlanCode());
		LCInsureAccClassSet tLCInsureAccClassSet =tLCInsureAccClassDB.query(); 
        if (!(tLCInsureAccClassSet.size()>0))
        {
            System.out.println("LCInsureAccClass的PolNo"+aLCInsureAccTraceSchema.getPolNo());
            mErrors.addOneError("没有查询到ACCClass表的记录");
            return false;
        }
        LCInsureAccClassSchema tLCInsureAccClassSchema=tLCInsureAccClassSet.get(1);
        tLCInsureAccClassSchema.setInsuAccBala(tLCInsureAccClassSchema.getInsuAccBala()-sumMoney);
        tLCInsureAccClassSchema.setOperator(mGlobalInput.Operator);
        tLCInsureAccClassSchema.setModifyDate(mCurrentDate);
        tLCInsureAccClassSchema.setModifyTime(mCurrentTime);
        map.put(tLCInsureAccClassSchema,"UPDATE");
        
        
        //再对ACCFEEClass表进行维护
        LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
		tLCInsureAccClassFeeDB.setPolNo(aLCInsureAccTraceSchema.getPolNo());
		tLCInsureAccClassFeeDB.setInsuAccNo(aLCInsureAccTraceSchema.getInsuAccNo());
		tLCInsureAccClassFeeDB.setPayPlanCode(aLCInsureAccTraceSchema.getPayPlanCode());
		LCInsureAccClassFeeSet tLCInsureAccClassFeeSet =tLCInsureAccClassFeeDB.query(); 
        if (!(tLCInsureAccClassFeeSet.size()>0))
        {
            System.out.println("LCInsureAccClassFee的PolNo"+aLCInsureAccTraceSchema.getPolNo());
            mErrors.addOneError("没有查询到ACCClassFee表的记录");
            return false;
        }
        LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema=tLCInsureAccClassFeeSet.get(1);
        tLCInsureAccClassFeeSchema.setFee(tLCInsureAccClassFeeSchema.getFee()-Math.abs(aLCInsureAccTraceSchema.getMoney()));
        tLCInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
        tLCInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
        tLCInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
        map.put(tLCInsureAccClassFeeSchema,"UPDATE");

        return true;
    }

    /**
     * 更新应收状态为state
     */
    private String changeState(String state)
    {
        String sql = "  update LJSPayB "
                     + "set dealState = '" + state + "' "
                     + "where getNoticeNo = '"
                     + this.mLJAPaySchema.getGetNoticeNo() + "' ";
        return sql;
    }

    /**
     * 将转出的实收保费转入帐户
     * @return boolean
     */
    private boolean dealAcc()
    {

        LCAppAccDB tLCAppAccDB =new LCAppAccDB();
        tLCAppAccDB.setCustomerNo(mLCContSchema.getAppntNo());
        LCAppAccSet tLCAppAccSet = tLCAppAccDB.query();

        if(tLCAppAccSet.size()==0)
        {
            AppAcc mAppAcc = new AppAcc();

            MMap mMMap = new MMap();
            LCAppAccTraceSchema schema = new LCAppAccTraceSchema();
            schema.setCustomerNo(mLCContSchema.getAppntNo());

            schema.setOtherNo(mLJSPayBSchema.getGetNoticeNo());
            schema.setOtherType("1");
            schema.setMoney(0);
            schema.setOperator(mLCContSchema.getOperator());

            mMMap.add(mAppAcc.accShiftToXQY(schema, "0"));

            VData dd = new VData();
            dd.add(mMMap);
            PubSubmit pub = new PubSubmit();
            if(!pub.submitData(dd, ""))
            {
                System.out.println(pub.mErrors.getErrContent());
                mErrors.addOneError("自动创建帐户,提交失败!");
                return false;
            }
        }

        AppAcc tAppAcc = new AppAcc();

        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(mLCContSchema.getAppntNo());
        tLCAppAccTraceSchema.setAccType("1");
        tLCAppAccTraceSchema.setOtherNo(mLCContSchema.getContNo());
        tLCAppAccTraceSchema.setBakNo(mLJSPayBSchema.getGetNoticeNo());
        tLCAppAccTraceSchema.setOtherType("2");
        tLCAppAccTraceSchema.setMoney(getSumDuePayMoney());
        tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);
        MMap tMap = tAppAcc.accShiftToXSYIJ(tLCAppAccTraceSchema);
        if(tMap == null)
        {
            mErrors.copyAllErrors(tAppAcc.mErrors);
            return false;
        }
        map.add(tMap);

        return true;
    }

    /**
     * 计算本次续期总缴费，包括财务缴费和余额抵扣
     * @return double
     */
    private double getSumDuePayMoney()
    {
        String sql = "  select sum(sumDuePayMoney) "
                     + "from LJAPayPerson "
                     + "where getNoticeNo = '"
                     + mLJSPayBSchema.getGetNoticeNo() + "' "
                     + "   and payType = 'ZC' "
                     + "   and sumDuePayMoney > 0 ";
        String moneyStr = new ExeSQL().getOneValue(sql);
        if(moneyStr.equals("") || moneyStr.equals("null"))
        {
            mErrors.addOneError("查询续期总缴费出错");
            return Double.MIN_VALUE;
        }
        else
        {
            return Double.parseDouble(moneyStr);
        }
    }

    /**
     * 回退只有续期险种的保单数据
     * @return boolean
     */
    private boolean backDueFeeCont()
    {
        String sql = "update LCCont a "
              + "set (prem, sumPrem, payToDate) = "
              + "   (select sum(prem), sum(sumPrem), min(payToDate) "
              + "   from LCPol "
              + "   where contNo = a.contNo), "
              + "   operator = '" + this.mGlobalInput.Operator + "', "
              + "   modifyDate = '" + this.mCurrentDate + "', "
              + "   modifyTime = '" + this.mCurrentTime + "' "
              + "where contNo = '" + this.mLCContSchema.getContNo() + "' ";
        map.put(sql, "UPDATE");

        return true;
    }

    /**
     * 回退险种数据
     * 将保单续期对于险种的数据修改回退
     * @return boolean：操作成功true，否则false
     */
    private boolean backPol()
    {
        //缴费次数还没搞定
        LCPolDB tLCPolDB = new LCPolDB();
        /* modify by fuxin 2008-6-12
         * 原程序根据保单的险种做实收保费转出，现在根据实收对应的险种做转出。
         * 为了解决少儿险和其它险种一起销售的问题。
         */
        String  sql =" select a.* From LCPol a where a.contno ='"+mLCContSchema.getContNo()+"' "
                    +" and polno in(select distinct polno from ljapayperson where getNoticeNo='"+mLJSPayBSchema.getGetNoticeNo()+"' and a.contno= contno ) "
                    +" with ur "
                  ;
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);

        if(tLCPolSet.size() == 0)
        {
            mErrors.addOneError("没有查询到保单" + mLCContSchema.getContNo()
                                + "的险种信息");
            return false;
        }

        for(int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            backDueFeePol(tLCPolSchema);
        }
        return true;
    }



    /**
     * 回退续期险种
     * @param tLCPolSchema LCPolSchema：待回退险中
     * @return boolean
     */
    private boolean backDueFeePol(LCPolSchema tLCPolSchema)
    {
        //回退缴费项
        String sql = "update LCPrem a "
                     + "set paytimes = paytimes -1 , "
                     + "   sumPrem = sumPrem - prem, "
                     + "   Operator = '" + mGlobalInput.Operator + "', "
                     + "   ModifyDate = '" + mCurrentDate + "', "
                     + "   Modifytime = '" + mCurrentTime + "', "
                     + "   (prem, payToDate) = "
                     + "      (select sum(sumDuePayMoney / (1 - a.FreeRate)), "
                     + "         min(lastPayToDate) "
                     + "      from LJAPayPerson "
                     + "      where polNo = a.polNo and dutyCode = a.dutyCode "
                     + "         and payPlanCode = a.payPlanCode "
                     + " and  paytype = 'ZC' "  // 在统计保费时,只取正常的 2007-9-9 by fuxin
                     + "         and payNo = '"
                     + mLJAPaySchema.getPayNo() + "') "
                     + "where PolNo = '" + tLCPolSchema.getPolNo() + "' ";
        map.put(sql, "UPDATE");

        String[] tables = {" LCPol", " LCDuty"};
        for(int i = 0; i < tables.length; i++)
        {
            sql = "update " + tables[i] + " a "
                  + "set (prem, sumPrem, payToDate) = "
                  + "   (select sum(prem), sum(sumPrem), min(payToDate) "
                  + "   from LCPrem "
                  + "   where polNo = a.polNo), "
                  + "   operator = '" + this.mGlobalInput.Operator + "', "
                  + "   modifyDate = '" + this.mCurrentDate + "', "
                  + "   modifyTime = '" + this.mCurrentTime + "' "
                  + "where polNo = '" + tLCPolSchema.getPolNo() + "' ";
            map.put(sql, "UPDATE");
        }

        return true;
    }




    /**
     * 财务回退
     */
    private boolean backFinace()
    {
        //总实收表回退
        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        ref.transFields(tLJAPaySchema, mLJAPaySchema);
        tLJAPaySchema.setSumActuPayMoney( -mLJAPaySchema.getSumActuPayMoney());
        tLJAPaySchema.setPayDate(mCurrentDate);
        tLJAPaySchema.setEnterAccDate(mCurrentDate);
        tLJAPaySchema.setConfDate(mCurrentDate);
        tLJAPaySchema.setPayNo(mPayNo);
        tLJAPaySchema.setMakeDate(mCurrentDate);
        tLJAPaySchema.setMakeTime(mCurrentTime);
        tLJAPaySchema.setModifyDate(mCurrentDate);
        tLJAPaySchema.setModifyTime(mCurrentTime);
        tLJAPaySchema.setOperator(mGlobalInput.Operator);
        tLJAPaySchema.setDueFeeType("1");//modify by fuxin 2008-7-3 新财务接口提数要求。
        map.put(tLJAPaySchema, "INSERT");

        //个人财务实收表回退
        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        tLJAPayPersonDB.setPayNo(mLJAPaySchema.getPayNo());
        LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
        if(tLJAPayPersonSet.size() == 0)
        {
            mErrors.addOneError("没有查询到被保人实收数据");
            return false;
        }

        LJAPayPersonSet tLJAPayPersonSetDealt = new LJAPayPersonSet();
        for(int i = 1; i <= tLJAPayPersonSet.size(); i++)
        {
            LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
            ref.transFields(tLJAPayPersonSchema, tLJAPayPersonSet.get(i));

            tLJAPayPersonSchema.setSumDuePayMoney(
                -tLJAPayPersonSchema.getSumDuePayMoney());
            tLJAPayPersonSchema.setSumActuPayMoney(
                -tLJAPayPersonSchema.getSumActuPayMoney());
            tLJAPayPersonSchema.setMoneyNoTax(null);
            tLJAPayPersonSchema.setMoneyTax(null);
            tLJAPayPersonSchema.setBusiType(null);
            tLJAPayPersonSchema.setTaxRate(null);          
            tLJAPayPersonSchema.setPayDate(this.mCurrentDate);
            tLJAPayPersonSchema.setEnterAccDate(this.mCurrentDate);
            tLJAPayPersonSchema.setPayNo(mPayNo);
            tLJAPayPersonSchema.setConfDate(mCurrentDate);
            tLJAPayPersonSchema.setEnterAccDate(mCurrentDate);
            tLJAPayPersonSchema.setMakeDate(this.mCurrentDate);
            tLJAPayPersonSchema.setMakeTime(this.mCurrentTime);
            tLJAPayPersonSchema.setModifyDate(this.mCurrentDate);
            tLJAPayPersonSchema.setModifyTime(this.mCurrentTime);
            tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);

            tLJAPayPersonSetDealt.add(tLJAPayPersonSchema);
        }
        map.put(tLJAPayPersonSetDealt, "INSERT");

        return true;
    }

    /**
     * 得到待转出的实收信息
     * @return boolean：操作成功true，否则false
     */
    private boolean queryContInfo()
    {
        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setGetNoticeNo(mLJSPayBSchema.getGetNoticeNo());
        LJAPaySet tLJAPaySet = tLJAPayDB.query();
        if(tLJAPaySet.size() == 0)
        {
            mErrors.addOneError("没有查询都实收记录");
            return false;
        }
        mLJAPaySchema = tLJAPaySet.get(1);

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLJSPayBSchema.getOtherNo());
        tLCContDB.getInfo();
        mLCContSchema = tLCContDB.getSchema();

        return true;
    }

    /**
     * 校验是否可进行实收保费转出操作，校验细节间数据流图“校验部分”。
     * @return boolean，校验通过true，否则false
     */
    private boolean checkData()
    {
        if(!queryLJSPayBInfo())
        {
            return false;
        }
        if(!queryContInfo())
        {
            return false;
        }

//        if(!canBack())
//        {
//            return false;
//        }
//        if(!dealUrgeLog(SysConst.INSERT))
//        {
//           return false;
//        }
//
//        if(!checkOrder())
//        {
//            return false;
//        }
//        if(!checkDueFee())
//        {
//            return false;
//        }
//        if(!checkClaim())
//        {
//            return false;
//        }
//        if(!checkBQ())
//        {
//            return false;
//        }
//        if(!checkPolState())
//        {
//            return false;
//        }
//        if(!checkSixMonth())
//        {
//        	return false;
//        }

        return true;
    }

    /**
     * 校验是否月结的实收保费转出在6个月之内
     * @return boolean
     */
    private boolean checkSixMonth()
    {
    	String  sql = "select max(confdate) from ljapayperson where contno='"+mLJSPayBSchema.getOtherNo()+"'  "
        + "and paytypeflag='0' and paytype<>'YEL' group by getnoticeno having sum(sumduepaymoney)<>0 " 
        + "order by max(confdate) desc "
        + "with ur";
    	String result=new ExeSQL().getOneValue(sql);
    	System.out.println(result);
    	if((result!=null)&&(!result.equals("null"))&&(!result.equals("")))
    	{
	    	int day=PubFun.calInterval(result, mCurrentDate, "D");
	    	if(day>180)
	    	{
	    		mErrors.addOneError("续期已超过6个月，实收保费不能转出。");
	    		return false;
	    	}
	    	
    	}
    	else
    	{
    		mErrors.addOneError("该单没有续期，实收保费不能转出。");
    		return false;
    	}

        
        return true;
    }
    /**
     * 若其他任务正在做实收保费转出，则不能再做实收保费转出
     * @return boolean
     */
    private boolean canBack()
    {
        String sql = "  select 1 from LCUrgeVerifyLog "
                     + "where serialNo = '"
                     + mLJSPayBSchema.getSerialNo() + "' "
                     + "   and OperateType = '3' ";
        String temp = new ExeSQL().getOneValue(sql);
        if(temp.equals("") || temp.equals("null"))
        {
            //可以进行实收保费转出
            return true;
        }
        mDoBackOtherTask = true;

        mErrors.addOneError("其他任务正在进行转出操作");
        return false;
    }

    /**
     * 加入到催收核销日志表数据
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealUrgeLog(String dealType)
    {
        LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new
                                               LCUrgeVerifyLogDB();
        tLCUrgeVerifyLogDB.setSerialNo(mLJSPayBSchema.getSerialNo());
        tLCUrgeVerifyLogDB.setRiskFlag("2");
        tLCUrgeVerifyLogDB.setOperateType("3"); //1：续期催收操作,2：续期核销操作,3：实收保费转出
        tLCUrgeVerifyLogDB.setOperateFlag("1"); //1：个案操作,2：批次操作
        tLCUrgeVerifyLogDB.setOperator(mGlobalInput.Operator);
        tLCUrgeVerifyLogDB.setDealState("1"); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成

        tLCUrgeVerifyLogDB.setContNo(mLJSPayBSchema.getOtherNo());
        tLCUrgeVerifyLogDB.setMakeDate(mCurrentDate);
        tLCUrgeVerifyLogDB.setMakeTime(mCurrentTime);
        tLCUrgeVerifyLogDB.setModifyDate(mCurrentDate);
        tLCUrgeVerifyLogDB.setModifyTime(mCurrentTime);

        if(dealType.equals(SysConst.INSERT))
        {
            if(!tLCUrgeVerifyLogDB.insert())
            {
                mErrors.addOneError("生成处理记录时出错");
                return false;
            }
        }
        else if(dealType.equals(SysConst.DELETE))
        {
            if(!tLCUrgeVerifyLogDB.delete())
            {
                mErrors.addOneError("删除处理记录时出错");
                return false;
            }
        }

        return true;
    }


    /**
     * 校验保单状态
     * @return boolean
     */
    private boolean checkPolState()
    {
        String sql = " select 1 from LCContState a "
                     + " where contNo = '" + mLJSPayBSchema.getOtherNo() + "' "
                     + "   and startDate < '" + mCurrentDate + "' "
                     + "   and ( endDate is null or endDate > '"
                     + mCurrentDate + "') "
                     + " and state = '1' "
                     +" and  exists (select 1 from ljapayperson where a.polno=polno and payno = '"
                     +  mLJAPaySchema.getPayNo() + "')"
                     ;

        String result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("当前保单或其险种处于失效或终止状态，不能转出。");
            return false;
        }
        return true;
    }

    /**
     * 校验保全
     * @return boolean
     */
    private boolean checkBQ()
    {
        //是否有未处理完毕的保全业务
        String sql = " select b.edorNo from LPEdorApp a, LPEdorItem b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and b.contNo = '" + mLJSPayBSchema.getOtherNo() + "' "
                     + "   and a.edorState != '" + BQ.EDORSTATE_CONFIRM + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow() != 0)
        {
            String edorNos = "";
            for(int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                edorNos += tSSRS.GetText(i, 1) + ", ";
            }
            mErrors.addOneError("保单有未处理完毕的全业务，不能转出，受理号为："
                                + edorNos);
            return false;
        }
        
      //是否有个单迁移
        sql = " select b.edorNo from LPEdorApp a, LPEdorItem b "
                    + "where a.edorAcceptNo = b.edorAcceptNo "
                    + "   and b.contNo = '" + mLJSPayBSchema.getOtherNo() + "' "
                    + "   and b.edortype='PR' "
                    + "   and a.confdate > '" + mLJSPayBSchema.getModifyDate() + "' " ;
       System.out.println(sql);
       String resultPR = new ExeSQL().getOneValue(sql);
       if(!(resultPR.equals("")))
       {
       	mErrors.addOneError("保单续期后做过个单迁移,不能回退,工单号为" + resultPR);
           return false;
       }


        //续期续保核销后发生过收退费的项目
        sql = " select sum(getmoney) from LJAGetEndorse "
              + "where contNo = '" + mLJSPayBSchema.getOtherNo() + "' "
              + "   and makeDate > '" + mLJAPaySchema.getMakeDate() + "' " 
              + "   and feeoperationtype not in (select Code from LDCode where CodeType = 'edortypechangeprem') ";
        String result = new ExeSQL().getOneValue(sql);
        if(!(result.equals("0.00")|result.equals("")))
        {
            mErrors.addOneError("保单有过对保费有影响的保全业务，不能转出。");
            return false;
        }

        //校验个险种保费与实交是否相等
        sql = "select 1 from LCPol a "
              + "where prem != "
              + "      nvl((select sum(sumDuePayMoney) from LJAPayPerson "
              + "      where polNo = a.polNo "
              + "         and payType = 'ZC' "
              + "         and getNoticeNo = '"
              + mLJSPayBSchema.getGetNoticeNo() + "'), -1) "
              + "   and polNo in "
              + "      (select polNo from LJAPayPerson "
              + "      where contNo = '" + mLJSPayBSchema.getOtherNo() + "' and getNoticeNo='"+mLJSPayBSchema.getGetNoticeNo()+"')";
        result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("本次转出的保费与保单实交保费金额不等，不能转出。");
            return false;
        }

//        //校验整单保费与实交是否相等
//        sql = "select 1 from LCCont a "
//              + "where prem != "
//              + "      nvl((select sum(sumDuePayMoney) from LJAPayPerson "
//              + "      where contNo = a.contNo "
//              + "         and payType = 'ZC' "
//              + "         and getNoticeNo = '"
//              + mLJSPayBSchema.getGetNoticeNo() + "'), -1) "
//              + "   and contNo = '" + mLJSPayBSchema.getOtherNo() + "' ";
//        result = new ExeSQL().getOneValue(sql);
//        if(!result.equals(""))
//        {
//            mErrors.addOneError("保单有过对保费有影响的保全业务，不能转出。");
//            return false;
//        }

        return true;
    }

    /**
     * 校验报单续期续保后是否发生过理赔
     * @return boolean: 发生true
     */
    private boolean checkClaim()
    {
        String sql = " select 1 from LLClaimDetail a "
                     + "where a.contNo = '"
                     + mLJSPayBSchema.getOtherNo() + "' "
                     + "   and a.makeDate >  (select max(lastPayToDate) from "
                     +" ljspaypersonb where contno = a.contno and dealstate ='1') ";
        String result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("保单正在做理赔业务或发生过结案的理陪，不能转出。");
            return false;
        }

        return true;
    }

    /**
     * 只能按实收保费核销时间从后往前的顺序进行实收保费转出
     * @return boolean
     */
    private boolean checkOrder()
    {
        ExeSQL tExeSQL = new ExeSQL();
        String sql = " select 1 "
                     + "from LJSPayB "
                     + "where otherno = '"
                     + mLJSPayBSchema.getOtherNo()
                     + "' and getnoticeno > '"
                     + mLJSPayBSchema.getGetNoticeNo() + "' "
                     + "  and dealstate = '"
                     + FeeConst.DEALSTATE_URGESUCCEED + "' ";
        String result = tExeSQL.getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("只能按实收保费核销时间从后往前的顺序进行实收保费转出");
            return false;
        }
        return true;
    }

    /**
     * 校验报单是否正在做续期续保
     * @return boolean
     */
    private boolean checkDueFee()
    {
        if(mLJSPayBSchema.getDealState().equals(FeeConst.DEALSTATE_BACK))
        {
            mErrors.addOneError("正在进行实收转出，不能再次进行");
            return false;
        }

        String sql = " select getNoticeNo from ljspay "
              + "where OtherNo = '" + mLJSPayBSchema.getOtherNo() + "' "
              + "  and othernotype = '2'";
        String result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("保单正在做续期续保业务，不能转出，应收记录号" + result);
            return false;
        }
        sql = "  select 1 from LCRnewStateLog "
              + "where contNo = '" + mLJSPayBSchema.getOtherNo() + "' "
              + "   and state in('" +  XBConst.RNEWSTATE_APP + "', '"
              + XBConst.RNEWSTATE_UNUW + "', '" + XBConst.RNEWSTATE_UNHASTEN
              + "')";
        result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("保单正在做续期续保业务，不能转出。");
            return false;
        }

        return true;

    }

    /**
     * 查询应收备份信息
     * @return boolean
     */
    private boolean queryLJSPayBInfo()
    {
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo(mLJSPayBSchema.getGetNoticeNo());
        tLJSPayBDB.setDealState(FeeConst.DEALSTATE_URGESUCCEED);
        LJSPayBSet set = tLJSPayBDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到以下通知书号的应收备份信息"
                                + mLJSPayBSchema.getGetNoticeNo());
            return false;
        }
        mLJSPayBSchema = set.get(1);

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * @param inputData VData: submitData中传入的VData
     * @return boolean: 操作成功true，否则false
     */
    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLJSPayBSchema = (LJSPayBSchema) inputData
                       .getObjectByObjectName("LJSPayBSchema", 0);
        if(mGlobalInput == null || mLJSPayBSchema == null
           || mLJSPayBSchema.getGetNoticeNo() == null
            || mLJSPayBSchema.getGetNoticeNo().equals(""))
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "xpbak";
        g.ComCode = "86";

        LJSPayBSet set = new LJSPayBDB().executeQuery("select * from ljspayb where otherno='013933883000001' and getnoticeno>='31000606151' and dealstate='1' order by getnoticeno desc with ur");
    
        for (int i = 1; i <= set.size(); i++) {
        	LJSPayBSchema lj = set.get(i);

            VData d = new VData();
            d.add(g);
            d.add(lj);

            MOmniIndiDueFeeBackBL bl = new MOmniIndiDueFeeBackBL();
            if(!bl.submitData(d, ""))
            {
                System.out.println(bl.mErrors.getErrContent());
            }
            else
            {
                System.out.println("OK");
            }
		}
    }
}
