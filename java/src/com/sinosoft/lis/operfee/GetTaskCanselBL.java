package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.util.*;
import com.sinosoft.lis.bq.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 1.0
 */
public class GetTaskCanselBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData saveData = new VData();
    private MMap mMMap = new MMap();
    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mserNo = "1"; //批次号
    private String mSerGrpNo = ""; //接受外部参数

    private String mGetNoticeNo = null;  //通知书号

		private String payMode = "Q"; //判断前台点击的是现金给付，还是普通给付。
    //保单缴费方式
    private String mBankCode = "";
    private String mBankAccNo = "";
    private String mAccName = "";

    private String StartDate = ""; //应付开始时间
    private String EndDate = ""; //应付结束时间

    private Reflections ref = new Reflections();

    //个人保单表
    private LJSGetSchema mLJSGetSchema = new LJSGetSchema();

    //应付个人明细表
    private LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();
    //实付表
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();

    private String mPrtSeqNo = null;


    public GetTaskCanselBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 校验是否有理赔，保全
     * @return boolean：通过true，否则false
     */
    public boolean checkData() {
       	LJSGetDB tLJSGetDB = new LJSGetDB();
       	tLJSGetDB.setSchema(mLJSGetSchema);
       	LJSGetSet tLJSGetSet = tLJSGetDB.query();
       	if(tLJSGetSet.size()<1)
       	{
       		CError tError = new CError();
          tError.moduleName = "GetTaskCanselBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "没有得到足够的数据，请您确认!";
          this.mErrors.addOneError(tError);
       		return false;
       	}
       	mLJSGetSchema = tLJSGetSet.get(1);
       	//查询是否正在银行转帐中
       	LJAGetDB tLJAGetDB = new LJAGetDB();
       	tLJAGetDB.setActuGetNo(mLJSGetSchema.getGetNoticeNo());
       	tLJAGetDB.getInfo();
       	if("1".equals(tLJAGetDB.getBankOnTheWayFlag()))
       	{
       		CError tError = new CError();
          tError.moduleName = "GetTaskCanselBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "银行在途，不能撤销!";
          this.mErrors.addOneError(tError);
       		return false;
       	}
        if(null != tLJAGetDB.getEnterAccDate() && !tLJAGetDB.getEnterAccDate().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "GetTaskCanselBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "银行已经回盘不能做撤销!";
            this.mErrors.addOneError(tError);
            return false;
        }
       	mLJAGetSchema.setSchema(tLJAGetDB.getSchema());
        return true;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LJSGetSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public VData getSubmitData(VData cInputData, String cOperate) {

        if (!getInputData(cInputData)) {
            return null;
        }
        System.out.println("After getinputdata");
        if (!checkData()) {
            return null;
        }
        //进行业务处理
        if (!dealData()) {
            return null;
        }

        return saveData;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LJSGetSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public MMap getSubmitMMap(VData cInputData, String cOperate) {
        getSubmitData(cInputData, cOperate);
        return mMMap;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LJSGetSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate) {

        VData data = getSubmitData(cInputData, cOperate);
        if (data == null) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) {
            this.mErrors.addOneError("PubSubmit:给付撤销失败!");
            return false;
        }
        return true;
    }

    /**
     * 生成给付记录
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
    	mLJAGetSchema.setCanSendBank("1");//不能发盘
        mLJAGetSchema.setFinState("2");//财务冲抵
        mLJAGetSchema.setOperator(tGI.Operator);
        mLJAGetSchema.setModifyDate(CurrentDate);
        mLJAGetSchema.setModifyTime(CurrentTime);
        mMMap.put(mLJAGetSchema, "UPDATE");

    	mLJSGetSchema.setDealState("2");//应付撤销
        mLJSGetSchema.setOperator(tGI.Operator);
        mLJSGetSchema.setModifyDate(CurrentDate);
        mLJSGetSchema.setModifyTime(CurrentTime);
    	mMMap.put(mLJSGetSchema, "UPDATE");
        if(!createNo())
        {
           return false;
        }
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        tLJAGetSchema.setActuGetNo(mGetNoticeNo);
        tLJAGetSchema.setGetNoticeNo(mLJAGetSchema.getGetNoticeNo());
        tLJAGetSchema.setOtherNo(mLJAGetSchema.getOtherNo());
        tLJAGetSchema.setOtherNoType(mLJAGetSchema.getOtherNoType());
        tLJAGetSchema.setPayMode(mLJAGetSchema.getPayMode());
        tLJAGetSchema.setManageCom(mLJAGetSchema.getManageCom());
        tLJAGetSchema.setAgentCom(mLJAGetSchema.getAgentCom());
        tLJAGetSchema.setAgentType(mLJAGetSchema.getAgentType());
        tLJAGetSchema.setAgentCode(mLJAGetSchema.getAgentCode());
        tLJAGetSchema.setAgentGroup(mLJAGetSchema.getAgentGroup());
        tLJAGetSchema.setAppntNo(mLJAGetSchema.getAppntNo());
        tLJAGetSchema.setAccName(mLJAGetSchema.getAccName());
        tLJAGetSchema.setSumGetMoney(-mLJAGetSchema.getSumGetMoney());
        tLJAGetSchema.setEnterAccDate(CurrentDate);
        tLJAGetSchema.setBankAccNo(mLJAGetSchema.getBankAccNo());
        tLJAGetSchema.setBankCode(mLJAGetSchema.getBankCode());
        tLJAGetSchema.setDrawer(mLJAGetSchema.getDrawer());
        tLJAGetSchema.setDrawerID(mLJAGetSchema.getDrawerID());
        tLJAGetSchema.setSerialNo(mserNo);
        tLJAGetSchema.setOperator(tGI.Operator);
        tLJAGetSchema.setMakeDate(CurrentDate);
        tLJAGetSchema.setMakeTime(CurrentTime);
        tLJAGetSchema.setModifyDate(CurrentDate);
        tLJAGetSchema.setModifyTime(CurrentTime);
        tLJAGetSchema.setCanSendBank("1");//不能发盘
        tLJAGetSchema.setFinState("2");//财务冲抵
        mMMap.put(tLJAGetSchema, "DELETE&INSERT");

        saveData.clear();
        saveData.add(mMMap);
        return true;
    }

    /**
     * 生成相关流水号
     * @return boolean：成功true，否则false
     */
    private boolean createNo()
    {
        String tLimit = PubFun.getNoLimit(mLJSGetSchema.getManageCom());
        //产生退费通知书号
        if(mGetNoticeNo == null || mGetNoticeNo.equals(""))
        {
            mGetNoticeNo = PubFun1.CreateMaxNo("GETNO", tLimit);
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData)
    {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLJSGetSchema = ((LJSGetSchema) mInputData.getObjectByObjectName(
            "LJSGetSchema",
            0));
        if((tGI == null) || (mLJSGetSchema == null))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GetTaskCanselBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

}
