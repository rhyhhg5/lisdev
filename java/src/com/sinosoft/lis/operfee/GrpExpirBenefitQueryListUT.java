package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpExpirBenefitQueryListUT {
    /**错误的容器*/
    public CErrors mErrors = null;
    private VData mResult = null;

    public GrpExpirBenefitQueryListUT() {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        GrpExpirBenefitQueryListBL tGrpExpirBenefitQueryListBL = new GrpExpirBenefitQueryListBL();

        if (!tGrpExpirBenefitQueryListBL.submitData(cInputData, cOperate)) {
            mErrors.copyAllErrors(tGrpExpirBenefitQueryListBL.mErrors);
            return false;
        }
        mResult = tGrpExpirBenefitQueryListBL.getResult();

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}

