package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class Cal330501 implements CalOther {
    private CErrors mCErrors ;
    private static final double default30050 = 52;
    private static final double default35165 = 61;
    private static final double default50050 = 103;
    private static final double default55165 = 115;
    private static final double defaultA0050 = 300;
    private static final double defaultA5165 = 315;
    private String ver = "89";

    public Cal330501() {
    }
    public double cal(String polno){
        double money = 0.0;
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(polno);
        if(!tLCPolDB.getInfo())
        {
            mCErrors.addOneError("Cal330501:查询险种信息错误!");
            return 0.0;
        }
        LCPolSchema tLCPolSchema = tLCPolDB.getSchema();
        //获取版本号
        String verSql = "select codename from ldcode where code like '330501V%'"
                        +" and date(codealias)<'"+tLCPolSchema.getCValiDate()
                        +"' order by code desc"
                        ;
        ver = new ExeSQL().getOneValue(verSql);
        if(ver.equals(""))
            ver = "89";
        switch(tLCPolSchema.getInsuYear())
        {
        case 3:
            money = getMoney3(tLCPolSchema);
            break;
        case 5:
            money = getMoney5(tLCPolSchema);
             break;
        case 10:
            money = getMoneyA(tLCPolSchema);
             break;
        }
        return money;
    }
    private double getMoney3(LCPolSchema aLCPolSchema)
    {
        double money =0.0;
        double base = 0.0;
        double add = 0.0;
        if(aLCPolSchema.getInsuredAppAge()>=0 && aLCPolSchema.getInsuredAppAge()<=50)
        {
            base = default30050;
        }else if(aLCPolSchema.getInsuredAppAge()>=51 && aLCPolSchema.getInsuredAppAge()<=65)
        {
            base = default35165;
        }
        double baseRate = 0.0;
        LDBankRateSet tLDBankRateSet = getBankRate(aLCPolSchema,3);
        if (tLDBankRateSet.size() < 1) {
            mCErrors.addOneError("查询利息表错误!");
            return 0.0;
        }
        if(ver.equals("89"))
        {
            baseRate = 0.029;
            add = getAdd(aLCPolSchema, tLDBankRateSet, baseRate, 2);
        }
        else if(ver.equals("919"))
        {
            baseRate =getBaseRate(aLCPolSchema.getInsuredAppAge(),3);
            add = getAdd(aLCPolSchema, tLDBankRateSet, baseRate, 1);
        }
        money = base*(aLCPolSchema.getAmnt()/1000) + add;
        return money;
    }
    private double getAdd(LCPolSchema aLCPolSchema, LDBankRateSet tLDBankRateSet,double baseRate,int k)
    {

        double result =0.0;
        for(int i=1;i<=tLDBankRateSet.size();i++)
        {
            LDBankRateSchema tLDBankRateSchema = tLDBankRateSet.get(i);
            String intvSql = "select (select case when days('"+aLCPolSchema.getEndDate()+"') between days('"+tLDBankRateSchema.getStartDate()+"') and days('"+tLDBankRateSchema.getEndDate()+"') "
                           + " then days('"+aLCPolSchema.getEndDate()+"') else days('"+tLDBankRateSchema.getEndDate()+"') end from dual) - "
                           + " (select case when days('"+aLCPolSchema.getCValiDate()+"') between days('"+tLDBankRateSchema.getStartDate()+"') and days('"+tLDBankRateSchema.getEndDate()+"') "
                           + " then days('"+aLCPolSchema.getCValiDate()+"') else days('"+tLDBankRateSchema.getStartDate()+"') end from dual) "
                           + " from dual "
                           ;
           int intv =0;
           try{
               intv = Integer.parseInt(new ExeSQL().getOneValue(intvSql));
           }catch(Exception e)
           {
           }
           double rate = tLDBankRateSchema.getRate()*0.8-baseRate;
           if(rate<0)
           {
               rate =0;
           }
           result +=intv*rate;
        }
        return aLCPolSchema.getSumPrem()*result/(k*365);
    }
    private LDBankRateSet getBankRate(LCPolSchema aLCPolSchema,int year)
    {
        String sql = "select * from ldbankrate where rateintvflag ='Y'"
                     +" and rateintv ='"+year+"' and ratetype ='S' "
                     +" and ((startdate <='"+aLCPolSchema.getCValiDate()
                     +"' and enddate >='"+aLCPolSchema.getCValiDate()+"')"
                     +" or (startdate <='"+aLCPolSchema.getEndDate()+"' and enddate >='"+aLCPolSchema.getEndDate()+"')"
                     +" or (startdate >='"+aLCPolSchema.getCValiDate()+"' and enddate <='"+aLCPolSchema.getEndDate()+"')"
                     +") ORDER BY STARTdate asc "
                     ;
        LDBankRateDB tLDBankRateDB = new LDBankRateDB();
        return tLDBankRateDB.executeQuery(sql);
    }
    private double getMoney5(LCPolSchema aLCPolSchema)
    {
        double money =0.0;
        double base = 0.0;
        double add = 0.0;
        if(aLCPolSchema.getInsuredAppAge()>=0 && aLCPolSchema.getInsuredAppAge()<=50)
        {
            base = default50050;
        }else if (aLCPolSchema.getInsuredAppAge() >= 51 &&
                   aLCPolSchema.getInsuredAppAge() <= 65) {
            base = default55165;
        }
        double baseRate = 0.0;
        LDBankRateSet tLDBankRateSet = getBankRate(aLCPolSchema, 5);
        if (tLDBankRateSet.size() < 1) {
            mCErrors.addOneError("查询利息表错误!");
            return 0.0;
        }
        if (ver.equals("89")) {
            baseRate = 0.035;
            add = getAdd(aLCPolSchema, tLDBankRateSet, baseRate, 2);
        } else if (ver.equals("919")) {
            baseRate =getBaseRate(aLCPolSchema.getInsuredAppAge(),5);
            add = getAdd(aLCPolSchema, tLDBankRateSet, baseRate, 1);
        }

        money = base*(aLCPolSchema.getAmnt()/1000) + add;
        return money;
    }

    private double getMoneyA(LCPolSchema aLCPolSchema) {
        double money =0.0;
        double base = 0.0;
        double add = 0.0;
        if(aLCPolSchema.getInsuredAppAge()>=0 && aLCPolSchema.getInsuredAppAge()<=50)
        {
            base = defaultA0050;
        } else if (aLCPolSchema.getInsuredAppAge() >= 51 &&
                   aLCPolSchema.getInsuredAppAge() <= 65) {
            base = defaultA5165;
        }
        double baseRate = 0.0;
        LDBankRateSet tLDBankRateSet = getBankRate(aLCPolSchema, 5);
        if (tLDBankRateSet.size() < 1) {
            mCErrors.addOneError("查询利息表错误!");
            return 0.0;
        }
        if (ver.equals("89")) {
            baseRate = 0.0396;
            add = getAdd(aLCPolSchema, tLDBankRateSet, baseRate, 3);
        } else if (ver.equals("919")) {
            baseRate =getBaseRate(aLCPolSchema.getInsuredAppAge(),10);
            add = getAdd(aLCPolSchema, tLDBankRateSet, baseRate, 1);
        }
        money = base*(aLCPolSchema.getAmnt()/1000) + add;
        return money;
    }

    public CErrors getError(){
        return mCErrors;
    }

    private double getBaseRate(int age,int Y)
    {
        double rate =0.0;
        switch(Y)
        {
        case 3 :
            if(age<=50)
            {
                rate = 0.038;
            }else if(age<=65)
            {
                rate = 0.037;
            }
            break;
         case 5 :
                if (age <= 50) {
                    rate = 0.044;
                } else if (age <= 65) {
                    rate = 0.044;
                }
                break;
             case 10:
                 if (age <= 50) {
                     rate = 0.066;
                 } else if (age <= 65) {
                     rate = 0.065;
                    }
                    break;
        }
        return rate;
    }

    public static void main(String arg[])
    {
        Cal330501 t = new Cal330501();
        t.cal("");
    }
}
