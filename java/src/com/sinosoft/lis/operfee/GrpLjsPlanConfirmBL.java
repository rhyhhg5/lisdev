package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
//import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
//import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

import java.text.*;
import java.util.Date;
import com.sinosoft.lis.taskservice.NextFeeHastenTask;
import com.sinosoft.task.TaskPhoneFinishBL;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author HZM
 * @version 1.0
 */

public class GrpLjsPlanConfirmBL {
	// 错误处理类，每个需要错误处理的类中都放置该类
	public CErrors mErrors = new CErrors();

	private GlobalInput tGI = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	private String mGetNoticeNo = "";

	private String serNo = "";

	private String tLimit = "";

	private String payNo = ""; // 交费收据号

	private MMap mAccMap = new MMap();

	private TransferData mTransferData;
	
	private Reflections mRef = new Reflections();

	private LCGrpContSchema mLCGrpContSchema;

	private LCGrpPayPlanSet mLCGrpPayPlanSet = new LCGrpPayPlanSet();

	private LCGrpPayPlanDetailSet mLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet();

	private LCGrpPayActuSet mLCGrpPayActuSet = new LCGrpPayActuSet();
	private LCGrpPayDueSet mUpdateLCGrpPayDueSet = new LCGrpPayDueSet();
	private LCGrpPayDueSet mInsertLCGrpPayDueSet = new LCGrpPayDueSet();
	private LCGrpPayDueDetailSet mLCGrpPayDueDetailSet = new LCGrpPayDueDetailSet();
	

	private LCGrpPayActuDetailSet mLCGrpPayActuDetailSet = new LCGrpPayActuDetailSet();

	private StringBuffer tSBql = new StringBuffer(128);

	/* 转换精确位数的对象 */
	private String FORMATMODOL = "0.000"; // 保费保额计算出来后的精确位数

	private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); // 数字转换对象

	// 暂收费表

	private LJTempFeeSchema mLJTempFeeSchema = new LJTempFeeSchema();

	private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();

	private LJTempFeeSet mLJTempFeeNewSet = new LJTempFeeSet();

	// 暂收费分类表
	private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();

	private LJTempFeeClassSet mLJTempFeeClassNewSet = new LJTempFeeClassSet();

	private String mPayDate = ""; // 缴费日期

	private String mEnterAccDate = ""; // 到帐日期

	private String mConfDate = ""; // 财务确认日期

	// 应收集体交费表
	private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();

	// 实收集体交费表
	private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();

	private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();

	// 应收和实收总表
	private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

	private LJAPaySchema mLJAPaySchema = new LJAPaySchema();

	private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();
	
	private FDate tFDate = new FDate();

	// 提交数据
	private MMap mMMap = new MMap();

	private VData mResult = new VData();

	// 集体保单表
	private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();

	private String mOperateFlag = "1"; // 1 : 个案核销，2：批量核销

	// 业务处理相关变量
	public GrpLjsPlanConfirmBL() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) {
		GlobalInput mGI = new GlobalInput();
		mGI.Operator = "pa0003";
		mGI.ComCode = "86";
		mGI.ManageCom = "86";

		GrpLjsPlanConfirmBL tNormPayCollOperBL = new GrpLjsPlanConfirmBL();
		VData tVData = new VData();
		tVData.add(mGI);
		tNormPayCollOperBL.submitData(tVData, "VERIFY");
	}

	public boolean checkData() {
		System.out.println("约定缴费续期抽档校验");

		// 下面校验lcgrppayplan是否有符合条件的记录
		tSBql = new StringBuffer(128);
		tSBql.append("select otherno from ljspayb where  getnoticeno='").append(mGetNoticeNo).append("'");
		String grpcontno = tSBql.toString();
		System.out.println(grpcontno);
		String tGrpContNo=new ExeSQL().getOneValue(grpcontno);
		if(tGrpContNo==null||tGrpContNo.equals("")){
			CError.buildErr(this, "应收号:" + mGetNoticeNo + "下的保单号查询失败！");
			return false;
		}
		
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(tGrpContNo);
		if(!tLCGrpContDB.getInfo()){
			CError.buildErr(this, "应收号:" + mGetNoticeNo + "下的团单查询失败！");
			return false;
		}
		mLCGrpContSchema=tLCGrpContDB.getSchema();
		return true;
	}

	// 传输数据的公共方法
	public boolean submitData(VData cInputData, String cOperate) {

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("After getinputdata");

		// 得到外部传入的数据,将数据备份到本类中
		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("After dealData！");
		if (!prepareData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(mResult, "") == false) {
			// System.out.println("delete fail!");
			CError.buildErr(this, "系统处理数据异常！");
			// 添加数据到催收核销日志表
			if (!dealUrgeLog("2", "插入LJAPay等表的数据失败", "UPDATE")) {
				return false;
			}

			return false;
		}
		// 添加数据到催收核销日志表
		if (!dealUrgeLog("3", "", "UPDATE")) {
			return false;
		}
		return true;
	}

	/**
	 * 团单下个人 核销处理逻辑 杨红于2005-07-22创建新的dealData方法 只将选中的应收个人核销并转实收(inputflag='1')
	 * 说明：dealAccount的逻辑还没有处理，待添加
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

		// 防止并发加锁
		MMap tCekMap1 = null;
		tCekMap1 = lockLJAGet();
		if (tCekMap1 == null) {
			return false;
		}
		mMMap.add(tCekMap1);
		// --------------------------


		
		String ManageCom = tGI.ManageCom;
		String Operator = tGI.Operator;

		tLimit = PubFun.getNoLimit(ManageCom);
		serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); // 给应收表生成统一的流水号
		payNo = PubFun1.CreateMaxNo("PayNo", tLimit);
		
		// 添加数据到催收核销日志表
		if (!dealUrgeLog("1", "", "INSERT")) {
			return false;
		}

		String str = "select a.edorAcceptNo " + "from LPEdorApp a, LPGrpEdorItem b " + "where a.edorAcceptNo = b.edorNo " + "   and b.grpContNo = '" + mLCGrpContSchema.getGrpContNo() + "' "
				+ "   and a.edorState not in('0')";
		String temp = new ExeSQL().getOneValue(str);
		if (!temp.equals("")) {
			mErrors.addOneError("保单正在做保全不能核销" + mLCGrpContSchema.getGrpContNo());
			return false;
		}

		// 根据grpcontno获取应收总表记录
		LJSPayDB tLJSPayDB = new LJSPayDB();
		LJSPaySet tLJSPaySet = new LJSPaySet();
		tLJSPayDB.setOtherNo(mLCGrpContSchema.getGrpContNo());
		tLJSPayDB.setOtherNoType("1");
		tLJSPaySet = tLJSPayDB.query();
		System.out.println(" 续期核销时，取LJSPay信息");
		if (tLJSPaySet.size() != 1) {
			CError.buildErr(this, "团单没被催收，请催收后再核销！");
			return false;
		}
		mLJSPaySchema.setSchema(tLJSPaySet.get(1)); // 核销时，记录会被删除


		System.out.println(" 续期核销时，取PayNo:" + payNo + "信息");
		String ljspaybSQL = "select * from LJSPayB where GetNoticeNo = '" + mLJSPaySchema.getGetNoticeNo() + "' and OtherNoType = '1' and dealstate = '4'";
		LJSPayBDB tLJSPayBDB = new LJSPayBDB();
		LJSPayBSet tLJSPayBSet = tLJSPayBDB.executeQuery(ljspaybSQL);
		System.out.println(" 续期核销时，取LJSPayB信息");
		if (tLJSPayBSet.size() != 1) {
			CError.buildErr(this, "团单下的备份数据缺失，请检查后再核销！");
			return false;
		}
		mLJSPayBSchema = tLJSPayBSet.get(1);

		// 下面获取应收集体数据，准备核销时删除！
		LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
		tLJSPayGrpDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		tLJSPayGrpDB.setGetNoticeNo(mLJSPayBSchema.getGetNoticeNo());
		mLJSPayGrpSet = tLJSPayGrpDB.query(); // 获取应收集体数据，准备核销时删除！
		// 下面判断在LJSPayGrp记录中是否有YET类型的数据，如果有，则走自动核销，不需要 暂交费
		if (mLJSPayGrpSet.size() == 0) {
			CError.buildErr(this, "团单下的集体催收数据缺失，不能核销！");
			return false;
		}
		System.out.println(" 续期核销时，取LJSPayGrp信息");
		// 下面判断在LJSPayGrp记录中是否有YET类型的数据，如果有，则走自动核销，不需要 暂交费

		// 下面获取应收集体数据，准备核销时删除！备份
		LJSPayGrpBDB tLJSPayGrpBDB = new LJSPayGrpBDB();
		tLJSPayGrpBDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		tLJSPayGrpBDB.setDealState("4");
		tLJSPayGrpBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
		LJSPayGrpBSet tLJSPayGrpBSet = new LJSPayGrpBSet();
		tLJSPayGrpBSet = tLJSPayGrpBDB.query(); // 获取应收集体数据，准备核销时删除！
		System.out.println(" 续期核销时，取LJSPayGrpB信息");
		// 下面判断在LJSPayGrp记录中是否有YET类型的数据，如果有，则走自动核销，不需要 暂交费
		if (tLJSPayGrpBSet.size() == 0) {
			CError.buildErr(this, "团单下的集体催收数据缺失，不能核销！");
			return false;
		}

		// 下面判断暂交费是否到帐，否则不能核销！
		LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
		tLJTempFeeDB.setTempFeeNo(mLJSPaySchema.getGetNoticeNo()); // 说明：催收产生的催缴通知书号对应交费通知书号
		tLJTempFeeDB.setConfFlag("0");
		mLJTempFeeSet = tLJTempFeeDB.query();
		System.out.println(" 续期核销时，取LJTempFee信息");
		if (mLJSPaySchema.getSumDuePayMoney() > 0 && mLJTempFeeSet.size() == 0) {
			CError.buildErr(this, "财务未交费，请财务交费后再做核销！");
			return false;
		}
		// 不需要财务缴费
		else if (Math.abs(0 - mLJSPaySchema.getSumDuePayMoney()) < 0.00001) {
			mPayDate = CurrentDate;
			mEnterAccDate = CurrentDate;
		} else {
			mLJTempFeeSchema = mLJTempFeeSet.get(1).getSchema(); // 至于暂交费金额与应收总金额(inputflag='1')的比较
			mPayDate = mLJTempFeeSchema.getPayDate();
			mEnterAccDate = mLJTempFeeSchema.getEnterAccDate();
		}
		mConfDate = CurrentDate;

		LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
		tLJTempFeeClassDB.setTempFeeNo(mLJSPaySchema.getGetNoticeNo()); // 说明：催收产生的催缴通知书号对应交费通知书号
		tLJTempFeeClassDB.setConfFlag("0");
		mLJTempFeeClassSet = tLJTempFeeClassDB.query();
		System.out.println(" 续期核销时，取LJTempFeeClass信息");
		if (mLJSPaySchema.getSumDuePayMoney() > 0 && mLJTempFeeClassSet.size() == 0) {
			CError.buildErr(this, "暂交费分类信息缺失，不能核销！");
			return false;
		}
		// 要到后面，根据ljspayperson相关信息判断
		if (mLJSPaySchema.getSumDuePayMoney() > 0 && mLJTempFeeSchema.getEnterAccDate() == null) {
			CError.buildErr(this, "财务未到帐，请到帐并作会计确认后再做核销！");
			return false;
		}
		// 说明：核销时只处理应收个人表中inputflag='1'的记录
		String getNoticeNo = mLJSPaySchema.getGetNoticeNo();
		
		LCGrpPolDB tLCGrpPolDB=new LCGrpPolDB();
		tLCGrpPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		mLCGrpPolSet=tLCGrpPolDB.query();
		for (int i = 1; i <= mLCGrpPolSet.size(); i++) {
			// 下面处理集体险种信息，并将LJSPayGrp转为LJAPayGrp
			LJSPayGrpDB tempLJSPayGrpDB = new LJSPayGrpDB();
			tempLJSPayGrpDB.setGrpPolNo(mLCGrpPolSet.get(i).getGrpPolNo());
			tempLJSPayGrpDB.setGetNoticeNo(getNoticeNo);
			tempLJSPayGrpDB.setPayType("ZC");
			LJSPayGrpSet tempLJSPayGrpSet = new LJSPayGrpSet();
			tempLJSPayGrpSet = tempLJSPayGrpDB.query();
			if(tempLJSPayGrpSet.size()!=1){
				CError.buildErr(this, "获取险种应收信息失败！");
				return false;
			}
			LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
			tLJSPayGrpSchema.setSchema(tempLJSPayGrpSet.get(1).getSchema());
			// 下面应收转实收
			LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
			tLJAPayGrpSchema.setGrpPolNo(mLCGrpPolSet.get(i).getGrpPolNo());
			tLJAPayGrpSchema.setGrpContNo(tLJSPayGrpSchema.getGrpContNo());
			tLJAPayGrpSchema.setPayCount(tLJSPayGrpSchema.getPayCount());
			tLJAPayGrpSchema.setAppntNo(tLJSPayGrpSchema.getAppntNo());
			tLJAPayGrpSchema.setPayNo(payNo);
			tLJAPayGrpSchema.setSumDuePayMoney(tLJSPayGrpSchema.getSumDuePayMoney()); // 总应交金额
			tLJAPayGrpSchema.setSumActuPayMoney(tLJSPayGrpSchema.getSumActuPayMoney()); // 总实交金额
			tLJAPayGrpSchema.setPayIntv(tLJSPayGrpSchema.getPayIntv());
			tLJAPayGrpSchema.setPayDate(mPayDate);
			tLJAPayGrpSchema.setPayType("ZC"); // repair:交费类型
			tLJAPayGrpSchema.setEnterAccDate(mEnterAccDate);
			tLJAPayGrpSchema.setConfDate(mConfDate); // 确认日期
			tLJAPayGrpSchema.setLastPayToDate(tLJSPayGrpSchema.getLastPayToDate()); // 原交至日期
			tLJAPayGrpSchema.setCurPayToDate(tLJSPayGrpSchema.getCurPayToDate());
			tLJAPayGrpSchema.setApproveCode(mLCGrpPolSet.get(i).getApproveCode());
			tLJAPayGrpSchema.setApproveDate(mLCGrpPolSet.get(i).getApproveDate());
			tLJAPayGrpSchema.setSerialNo(serNo);
			tLJAPayGrpSchema.setOperator(Operator);
			tLJAPayGrpSchema.setMakeDate(CurrentDate);
			tLJAPayGrpSchema.setMakeTime(CurrentTime);
			tLJAPayGrpSchema.setGetNoticeNo(tLJSPayGrpSchema.getGetNoticeNo()); // 通知书号码
			tLJAPayGrpSchema.setModifyDate(CurrentDate);
			tLJAPayGrpSchema.setModifyTime(CurrentTime);
			tLJAPayGrpSchema.setManageCom(mLCGrpPolSet.get(i).getManageCom()); // 管理机构
			tLJAPayGrpSchema.setAgentCom(mLCGrpPolSet.get(i).getAgentCom()); // 代理机构
			tLJAPayGrpSchema.setAgentType(mLCGrpPolSet.get(i).getAgentType()); // 代理机构内部分类
			tLJAPayGrpSchema.setRiskCode(mLCGrpPolSet.get(i).getRiskCode()); // 险种编码
			tLJAPayGrpSchema.setAgentCode(mLCGrpPolSet.get(i).getAgentCode());
			tLJAPayGrpSchema.setAgentGroup(mLCGrpPolSet.get(i).getAgentGroup());
			mLJAPayGrpSet.add(tLJAPayGrpSchema);
		}
		// 将类型为YEL的LJSPayGrp应收转为LJAPayGrp实收，（财务会做相关处理）
		tSBql = new StringBuffer(128);
		tSBql.append("select * from ljspaygrp where GrpContNo='").append(mLCGrpContSchema.getGrpContNo()).append("'").append(" and GetNoticeNo='" + mLJSPaySchema.getGetNoticeNo()).append("' and PayType='YEL'");
		String yelSqlStr = tSBql.toString();
		LJSPayGrpDB tempLJSPayGrpDB = new LJSPayGrpDB();
		LJSPayGrpSet tempLJSPayGrpSet = new LJSPayGrpSet();
		tempLJSPayGrpSet = tempLJSPayGrpDB.executeQuery(yelSqlStr);
		System.out.println(" 续期核销时，取余额");
		if (tempLJSPayGrpSet.size() > 0) {
			// 将类型为YEL的LJSPayGrp应收转为LJAPayGrp实收
			LJSPayGrpBL yelLJSPayGrpBL = new LJSPayGrpBL();
			yelLJSPayGrpBL.setSchema(tempLJSPayGrpSet.get(1).getSchema());
			LJAPayGrpBL yelLJAPayGrpBL = new LJAPayGrpBL();
			yelLJAPayGrpBL.setGrpContNo(yelLJSPayGrpBL.getGrpContNo());
			yelLJAPayGrpBL.setPayCount(yelLJSPayGrpBL.getPayCount());
			yelLJAPayGrpBL.setGrpPolNo(yelLJSPayGrpBL.getGrpPolNo());
			yelLJAPayGrpBL.setPayCount(yelLJSPayGrpBL.getPayCount());
			yelLJAPayGrpBL.setAppntNo(mLCGrpContSchema.getAppntNo());
			yelLJAPayGrpBL.setPayNo(payNo); // 交费收据号码
			yelLJAPayGrpBL.setSumDuePayMoney(yelLJSPayGrpBL.getSumDuePayMoney());
			yelLJAPayGrpBL.setSumActuPayMoney(yelLJSPayGrpBL.getSumActuPayMoney());
			yelLJAPayGrpBL.setPayIntv(yelLJSPayGrpBL.getPayIntv());
			yelLJAPayGrpBL.setLastPayToDate(yelLJSPayGrpBL.getLastPayToDate());
			yelLJAPayGrpBL.setCurPayToDate(yelLJSPayGrpBL.getCurPayToDate());
			yelLJAPayGrpBL.setPayDate(mPayDate);
			yelLJAPayGrpBL.setEnterAccDate(mEnterAccDate);
			yelLJAPayGrpBL.setPayType("YEL");
			yelLJAPayGrpBL.setSerialNo(serNo);
			yelLJAPayGrpBL.setGetNoticeNo(yelLJSPayGrpBL.getGetNoticeNo());
			yelLJAPayGrpBL.setOperator(Operator);
			yelLJAPayGrpBL.setMakeDate(CurrentDate);
			yelLJAPayGrpBL.setMakeTime(CurrentTime);
			yelLJAPayGrpBL.setModifyDate(CurrentDate);
			yelLJAPayGrpBL.setModifyTime(CurrentTime);
			yelLJAPayGrpBL.setManageCom(yelLJSPayGrpBL.getManageCom()); // 管理机构
			yelLJAPayGrpBL.setAgentCom(yelLJSPayGrpBL.getAgentCom()); // 代理机构
			yelLJAPayGrpBL.setAgentType(yelLJSPayGrpBL.getAgentType()); // 代理机构内部分类
			yelLJAPayGrpBL.setRiskCode(yelLJSPayGrpBL.getRiskCode()); // 险种编码
			yelLJAPayGrpBL.setAgentCode(yelLJSPayGrpBL.getAgentCode());
			yelLJAPayGrpBL.setAgentGroup(yelLJSPayGrpBL.getAgentGroup());
			mLJAPayGrpSet.add(yelLJAPayGrpBL);
			dealAppAcc(yelLJAPayGrpBL);
		}

		// 核销时在ljapaygrp中出入YET的记录
		tSBql = new StringBuffer(128);
		tSBql.append("select * from ljspaygrp where GrpContNo='").append(mLCGrpContSchema.getGrpContNo()).append("'").append(" and GetNoticeNo='" + mLJSPaySchema.getGetNoticeNo()).append("' and PayType='YET'");
		String yetSqlStr = tSBql.toString();
		LJSPayGrpDB tempYELJSPayGrpDB = new LJSPayGrpDB();
		LJSPayGrpSet tempYELJSPayGrpSet = new LJSPayGrpSet();
		tempYELJSPayGrpSet = tempYELJSPayGrpDB.executeQuery(yetSqlStr);
		System.out.println(" 续期核销时，退回账户金额");
		if (tempYELJSPayGrpSet.size() > 0) {
			// 将类型为YET的LJSPayGrp应收转为LJAPayGrp实收
			LJSPayGrpBL yetLJSPayGrpBL = new LJSPayGrpBL();
			yetLJSPayGrpBL.setSchema(tempYELJSPayGrpSet.get(1).getSchema());
			LJAPayGrpBL yetLJAPayGrpBL = new LJAPayGrpBL();
			yetLJAPayGrpBL.setGrpContNo(yetLJSPayGrpBL.getGrpContNo());
			yetLJAPayGrpBL.setPayCount(yetLJSPayGrpBL.getPayCount());
			yetLJAPayGrpBL.setGrpPolNo(yetLJSPayGrpBL.getGrpPolNo());
			yetLJAPayGrpBL.setPayCount(yetLJSPayGrpBL.getPayCount());
			yetLJAPayGrpBL.setAppntNo(mLCGrpContSchema.getAppntNo());
			yetLJAPayGrpBL.setPayNo(payNo); // 交费收据号码
			yetLJAPayGrpBL.setSumDuePayMoney(yetLJSPayGrpBL.getSumDuePayMoney());
			yetLJAPayGrpBL.setSumActuPayMoney(yetLJSPayGrpBL.getSumActuPayMoney());
			yetLJAPayGrpBL.setPayIntv(yetLJSPayGrpBL.getPayIntv());
			yetLJAPayGrpBL.setLastPayToDate(yetLJSPayGrpBL.getLastPayToDate());
			yetLJAPayGrpBL.setCurPayToDate(yetLJSPayGrpBL.getCurPayToDate());
			yetLJAPayGrpBL.setPayDate(mPayDate);
			yetLJAPayGrpBL.setEnterAccDate(this.mEnterAccDate);
			yetLJAPayGrpBL.setPayType("YET");
			yetLJAPayGrpBL.setSerialNo(serNo);
			yetLJAPayGrpBL.setGetNoticeNo(yetLJSPayGrpBL.getGetNoticeNo());
			yetLJAPayGrpBL.setOperator(Operator);
			yetLJAPayGrpBL.setMakeDate(CurrentDate);
			yetLJAPayGrpBL.setMakeTime(CurrentTime);
			yetLJAPayGrpBL.setModifyDate(CurrentDate);
			yetLJAPayGrpBL.setModifyTime(CurrentTime);
			yetLJAPayGrpBL.setManageCom(yetLJSPayGrpBL.getManageCom()); // 管理机构
			yetLJAPayGrpBL.setAgentCom(yetLJSPayGrpBL.getAgentCom()); // 代理机构
			yetLJAPayGrpBL.setAgentType(yetLJSPayGrpBL.getAgentType()); // 代理机构内部分类
			yetLJAPayGrpBL.setRiskCode(yetLJSPayGrpBL.getRiskCode()); // 险种编码
			yetLJAPayGrpBL.setAgentCode(yetLJSPayGrpBL.getAgentCode());
			yetLJAPayGrpBL.setAgentGroup(yetLJSPayGrpBL.getAgentGroup());
			mLJAPayGrpSet.add(yetLJAPayGrpBL);
		}

		// 应收总表转实收总表
		mLJAPaySchema.setPayNo(payNo); // 交费收据号码
		mLJAPaySchema.setIncomeNo(mLJSPaySchema.getOtherNo()); // 应收/实收编号,集体合同号
		mLJAPaySchema.setIncomeType(mLJSPaySchema.getOtherNoType()); // 应收/实收编号类型
		mLJAPaySchema.setAppntNo(mLJSPaySchema.getAppntNo()); // 投保人客户号码
		mLJAPaySchema.setSumActuPayMoney(mLJSPaySchema.getSumDuePayMoney()); // 总实交金额
		mLJAPaySchema.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo()); // 交费收据号,暂时先取应收总表的通知号
		mLJAPaySchema.setEnterAccDate(mEnterAccDate); // 到帐日期
		mLJAPaySchema.setPayDate(mPayDate); // 交费日期,仍然取了最小日期
		mLJAPaySchema.setConfDate(mConfDate); // 确认日期
		mLJAPaySchema.setApproveCode(mLJSPaySchema.getApproveCode()); // 复核人编码
		mLJAPaySchema.setApproveDate(mLJSPaySchema.getApproveDate()); // 复核日期
		mLJAPaySchema.setSerialNo(serNo); // 流水号
		mLJAPaySchema.setOperator(Operator); // 操作员
		mLJAPaySchema.setMakeDate(CurrentDate); // 入机时间
		mLJAPaySchema.setMakeTime(CurrentTime); // 入机时间
		mLJAPaySchema.setModifyDate(CurrentDate); // 最后一次修改日期
		mLJAPaySchema.setModifyTime(CurrentTime); // 最后一次修改时间
		mLJAPaySchema.setRiskCode(mLJSPaySchema.getRiskCode()); // 险种编码
		mLJAPaySchema.setManageCom(mLJSPaySchema.getManageCom());
		mLJAPaySchema.setAgentCode(mLJSPaySchema.getAgentCode());
		mLJAPaySchema.setAgentGroup(mLJSPaySchema.getAgentGroup());
		mLJAPaySchema.setDueFeeType("1"); // add by fuxin 2008-7-3 新财务接口

		mLJAPaySchema.setSaleChnl(mLJSPaySchema.getSaleChnl());
		mLJAPaySchema.setAgentCom(mLCGrpContSchema.getAgentCom());
		mLJAPaySchema.setMarketType(mLJSPaySchema.getMarketType());

		// 下面更新lcgrpcont里面的信息
		mLCGrpContSchema.setSumPrem(mLCGrpContSchema.getSumPrem() + mLJSPaySchema.getSumDuePayMoney());
		mLCGrpContSchema.setModifyDate(CurrentDate); // 最后一次修改日期
		mLCGrpContSchema.setModifyTime(CurrentTime); // 最后一次修改时间

		// 最后将暂交费和 暂交费分类信息 置核销标志
		System.out.println(" 续期核销时，更新LJTempFee ");
		for (int i = 1; i <= mLJTempFeeSet.size(); i++) {
			LJTempFeeBL tLJTempFeeBL = new LJTempFeeBL();
			tLJTempFeeBL.setSchema(mLJTempFeeSet.get(i));
			tLJTempFeeBL.setConfFlag("1");
			tLJTempFeeBL.setConfDate(CurrentDate);
			mLJTempFeeNewSet.add(tLJTempFeeBL);
		}
		System.out.println(" 续期核销时，更新LJTempFeeClass ");
		for (int i = 1; i <= mLJTempFeeClassSet.size(); i++) {
			LJTempFeeClassBL tLJTempFeeClassBL = new LJTempFeeClassBL();
			tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i));
			tLJTempFeeClassBL.setConfFlag("1");
			tLJTempFeeClassBL.setConfDate(CurrentDate);
			mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);
		}
		// 设置应收总表备份表
		mLJSPayBSchema.setDealState("1");
		mLJSPayBSchema.setConfFlag("1");
		mLJSPayBSchema.setModifyDate(CurrentDate);
		mLJSPayBSchema.setModifyTime(CurrentTime);

		// 设置应收集体表备份表
		System.out.println(" 续期核销时，更新LJSPayGrpBS");
		for (int s = 1; s <= tLJSPayGrpBSet.size(); s++) {
			LJSPayGrpBSchema tLJSPayGrpBSchema = new LJSPayGrpBSchema();
			tLJSPayGrpBSchema = tLJSPayGrpBSet.get(s);
			tLJSPayGrpBSchema.setDealState("1");
			tLJSPayGrpBSchema.setConfFlag("1");
			tLJSPayGrpBSchema.setModifyDate(CurrentDate);
			tLJSPayGrpBSchema.setModifyTime(CurrentTime);
			mLJSPayGrpBSet.add(tLJSPayGrpBSchema);
		}
		
		//约定缴费添加缴费计划核销数据的保存
		if(tFDate.getDate(mLCGrpContSchema.getCValiDate()).
        		after(tFDate.getDate("2012-12-31"))){
			
			if(!dealPayDue()){
				
				return false;
			}
		}
		
		return true;
	}

	/**
	 * 投保人帐户余额抵扣生效
	 * 
	 * @param yelLJAPayGrpBL
	 *            LJAPayGrpBL
	 */
	private void dealAppAcc(LJAPayGrpBL yelLJAPayGrpBL) {
		LCAppAccTraceDB tLCAppAccTraceDB = new LCAppAccTraceDB();
		tLCAppAccTraceDB.setCustomerNo(yelLJAPayGrpBL.getAppntNo());
		tLCAppAccTraceDB.setOtherNo(yelLJAPayGrpBL.getGrpContNo());
		tLCAppAccTraceDB.setBakNo(yelLJAPayGrpBL.getGetNoticeNo());
		LCAppAccTraceSet set = tLCAppAccTraceDB.query();
		if (set.size() == 0) {
			return;
		}
		double money = 0;
		 
		for (int i = 1; i <= set.size(); i++) {

			LCAppAccTraceSchema schema = set.get(i);
			if (schema.getMoney() > 0) {
				money = schema.getMoney(); // 账户余额更新成正记录，否则为零
			}
			schema.setAccBala(money);
			schema.setState("1");
			mAccMap.put(schema, "UPDATE");
		}

		String sql = "  update LCAppAcc " + "set accBala =  " + money + ", " + " accGetMoney =  " + money + ", " + "   state = '1' " + "where customerNo = '" + set.get(1).getCustomerNo() + "' ";
		mAccMap.put(sql, "UPDATE");
	}

	/**
	 * 加入到催收核销日志表数据
	 * 
	 * @param pmDealState
	 * @param pmReason
	 * @param pmOpreat :
	 *            INSERT,UPDATE,DELETE
	 * @return
	 */
	private boolean dealUrgeLog(String pmDealState, String pmReason, String pmOpreat) {

		LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new LCUrgeVerifyLogSchema();
		// 加到催收核销日志表
		if (pmOpreat.equals("INSERT")) {
			tLCUrgeVerifyLogSchema.setSerialNo(serNo);
			tLCUrgeVerifyLogSchema.setRiskFlag("1");
			tLCUrgeVerifyLogSchema.setOperateType("2"); // 1：续期催收操作,2：续期核销操作
			tLCUrgeVerifyLogSchema.setOperateFlag(mOperateFlag); // 1：个案操作,2：批次操作
			tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
			tLCUrgeVerifyLogSchema.setDealState(pmDealState); // 1 : 正在处理中,2 :
			// 处理错误,3 :
			// 处理已完成

			tLCUrgeVerifyLogSchema.setContNo(mLCGrpContSchema.getGrpContNo());
			tLCUrgeVerifyLogSchema.setMakeDate(CurrentDate);
			tLCUrgeVerifyLogSchema.setMakeTime(CurrentTime);
			tLCUrgeVerifyLogSchema.setModifyDate(CurrentDate);
			tLCUrgeVerifyLogSchema.setModifyTime(CurrentTime);
			tLCUrgeVerifyLogSchema.setErrReason(pmReason);
		} else {
			LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
			LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new LCUrgeVerifyLogSet();
			tLCUrgeVerifyLogDB.setSerialNo(serNo);
			tLCUrgeVerifyLogDB.setOperateType("1");
			tLCUrgeVerifyLogDB.setOperateFlag(mOperateFlag);
			tLCUrgeVerifyLogDB.setRiskFlag("1");
			tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
			if (!(tLCUrgeVerifyLogSet == null) && tLCUrgeVerifyLogSet.size() > 0) {
				tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
				tLCUrgeVerifyLogSchema.setModifyDate(CurrentDate);
				tLCUrgeVerifyLogSchema.setModifyTime(CurrentTime);
				tLCUrgeVerifyLogSchema.setDealState(pmDealState);
				tLCUrgeVerifyLogSchema.setErrReason(pmReason);
			} else {
				return false;
			}

		}

		MMap tMap = new MMap();
		tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
		VData tInputData = new VData();
		tInputData.add(tMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(tInputData, "") == false) {
			this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
			return false;
		}
		return true;
	}

	private boolean dealPayDue(){
		
		LCGrpPayActuDB tLCGrpPayActuDB = new LCGrpPayActuDB();
		tLCGrpPayActuDB.setGetNoticeNo(mGetNoticeNo);
		tLCGrpPayActuDB.setPrtNo(mLCGrpContSchema.getPrtNo());
		mLCGrpPayActuSet = tLCGrpPayActuDB.query();
		if (mLCGrpPayActuSet.size() > 0) {
			for (int i = 1; i <= mLCGrpPayActuSet.size(); i++) {
				
				LCGrpPayActuSchema tLCGrpPayActuSchema = new LCGrpPayActuSchema();
				tLCGrpPayActuSchema = mLCGrpPayActuSet.get(i);
				String tGetPayDueSql = "select * from LCGrpPayDue where prtno='"+tLCGrpPayActuSchema.getPrtNo()+"' and confstate='02' " +
//						" and int(plancode)<=int('"+tLCGrpPayActuSchema.getPlanCode()+"') " +
								" and contplancode='"+tLCGrpPayActuSchema.getContPlanCode()+"' order by paytodate fetch first 1 rows only ";
				System.out.println(tGetPayDueSql);
				LCGrpPayDueDB tLCGrpPayDueDB = new LCGrpPayDueDB();
				LCGrpPayDueSet tLCGrpPayDueSet = new LCGrpPayDueSet();
				tLCGrpPayDueSet = tLCGrpPayDueDB.executeQuery(tGetPayDueSql);
//				if (tLCGrpPayDueSet.size() > 0) {
					for (int t = 1; t <= tLCGrpPayDueSet.size(); t++) {
						
						LCGrpPayDueSchema tLCGrpPayDueSchema = new LCGrpPayDueSchema();
						tLCGrpPayDueSchema = tLCGrpPayDueSet.get(t);
						double tPayDueMoney = tLCGrpPayDueSchema.getPrem();//预存的缴费金额
						double tPayActuMoney = tLCGrpPayActuSchema.getPrem();//本次实际缴费金额
						if(!calData(tPayDueMoney,tPayActuMoney,tLCGrpPayDueSchema)){
							
							this.mErrors.addOneError("处理缴费计划对应信息及其明细信息错误");
							return false;
							
						}
						
						
					}
					
//				}
//				else{
//					this.mErrors.addOneError("查询应缴计划信息失败!");
//					return false;
//					
//				}
			}
			
		}
		else{
			
			this.mErrors.addOneError("查询缴费计划信息失败");
			return false;
		}
		
			
		
		 
		
		return true;
	}
	
	private boolean calData(double tPayDueMoney,double tPayActuMoney,LCGrpPayDueSchema tLCGrpPayDueSchema){
		
		if(tPayDueMoney < tPayActuMoney){
			try{
				updatePayDue(tLCGrpPayDueSchema,tPayDueMoney,"01");
				insertPayDueDetail(tLCGrpPayDueSchema,tPayDueMoney);
				String tGetPayDueSql = "select * from LCGrpPayDue where prtno='"+tLCGrpPayDueSchema.getPrtNo()+"' and confstate='02' " +
				" and int(plancode)>int('"+tLCGrpPayDueSchema.getPlanCode()+"') " +
						" and contplancode='"+tLCGrpPayDueSchema.getContPlanCode()+"' order by paytodate fetch first 1 rows only ";
				System.out.println(tGetPayDueSql);
				LCGrpPayDueDB tLCGrpPayDueDB = new LCGrpPayDueDB();
				LCGrpPayDueSet tLCGrpPayDueSet = new LCGrpPayDueSet();
				tLCGrpPayDueSet = tLCGrpPayDueDB.executeQuery(tGetPayDueSql);
				double aPayActuMoney = tPayActuMoney-tPayDueMoney;
				if (tLCGrpPayDueSet.size() > 0) {
					for (int t = 1; t <= tLCGrpPayDueSet.size(); t++) {
						
						LCGrpPayDueSchema aLCGrpPayDueSchema = new LCGrpPayDueSchema();
						aLCGrpPayDueSchema = tLCGrpPayDueSet.get(t);
						double aPayDueMoney = aLCGrpPayDueSchema.getPrem();//预存的缴费金额
						
						if(!calData(aPayDueMoney,aPayActuMoney,aLCGrpPayDueSchema)){
							
							this.mErrors.addOneError("处理当期期后续缴费计划对应信息及其明细信息错误");
							return false;
							
						}
						
						
					}
				
				}
			}
			catch(Exception ex) {
				
				ex.printStackTrace();
			}
			
		}
		else if(tPayDueMoney == tPayActuMoney){
			
			try{
				updatePayDue(tLCGrpPayDueSchema,tPayDueMoney,"01");
				insertPayDueDetail(tLCGrpPayDueSchema,tPayDueMoney);
			}
			catch(Exception ex) {
				
				ex.printStackTrace();
			}
			
		}
		else if(tPayDueMoney > tPayActuMoney){
			
			try{
				
				insertPayDue(tLCGrpPayDueSchema,tPayActuMoney);
				insertPayDueDetail(tLCGrpPayDueSchema,tPayActuMoney);
				updatePayDue(tLCGrpPayDueSchema,tPayDueMoney-tPayActuMoney,"02");
				
			}
			catch(Exception ex) {
				
				ex.printStackTrace();
			}
		}
		
		return true;
	}
	
	
	
	/***
	 * 更新LCGrpPayDue数据为核销或更新金额
	 * @param tLCGrpPayDueSchema
	 * @param tPayMoney
	 * @param tFlag
	 * @return
	 */
	private void updatePayDue(LCGrpPayDueSchema tLCGrpPayDueSchema,double tPayMoney,String tFlag){
		
		if("01".equals(tFlag)){
			
//			tLCGrpPayDueSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
//			tLCGrpPayDueSchema.setGetNoticeNo(mGetNoticeNo);
//			tLCGrpPayDueSchema.setPayNo(payNo);
//			tLCGrpPayDueSchema.setConfDate(mConfDate);
//			tLCGrpPayDueSchema.setConfState(tFlag);
			String sql = "update LCGrpPayDue set GrpContNo = '"+mLCGrpContSchema.getGrpContNo()+"'," +
							"GetNoticeNo='"+mGetNoticeNo+"',PayNo='"+payNo+"'," +
									"ConfDate='"+mConfDate+"',ConfState='"+tFlag+"'," +
											"Prem="+tPayMoney+",Operator='"+tGI.Operator+"'," +
													" ModifyDate='"+CurrentDate+"'," +
															" ModifyTime='"+CurrentTime+"' " +
									" where  prtno='"+tLCGrpPayDueSchema.getPrtNo()+"'" +
									" and contplancode='"+tLCGrpPayDueSchema.getContPlanCode()+"'" +
									" and paytodate='"+tLCGrpPayDueSchema.getPaytoDate()+"' and ConfState='02' ";
			System.out.println(sql);
			mMMap.put(sql, "UPDATE");
		}
		else{
			tLCGrpPayDueSchema.setPrem(tPayMoney);
			tLCGrpPayDueSchema.setOperator(tGI.Operator);
			tLCGrpPayDueSchema.setModifyDate(CurrentDate);
			tLCGrpPayDueSchema.setModifyTime(CurrentTime);
			mUpdateLCGrpPayDueSet.add(tLCGrpPayDueSchema);
		}
		
		
		
	}
	
	
	/***
	 * 插入LCGrpPayDue核销数据
	 * @param tLCGrpPayDueSchema
	 * @param tPayMoney
	 * @return
	 */
	private void insertPayDue(LCGrpPayDueSchema tLCGrpPayDueSchema,double tPayMoney){
		
		LCGrpPayDueSchema aLCGrpPayDueSchema = new LCGrpPayDueSchema();
		mRef.transFields(aLCGrpPayDueSchema, tLCGrpPayDueSchema);
		
		aLCGrpPayDueSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		aLCGrpPayDueSchema.setGetNoticeNo(mGetNoticeNo);
		aLCGrpPayDueSchema.setPayNo(payNo);
		aLCGrpPayDueSchema.setConfDate(mConfDate);
		aLCGrpPayDueSchema.setConfState("01");
		aLCGrpPayDueSchema.setPrem(tPayMoney);
		aLCGrpPayDueSchema.setOperator(tGI.Operator);
		aLCGrpPayDueSchema.setModifyDate(CurrentDate);
		aLCGrpPayDueSchema.setModifyTime(CurrentTime);
		mInsertLCGrpPayDueSet.add(aLCGrpPayDueSchema);
		
		
	}
	
	/***
	 * 插入核销的LCGrpPayDueDetail
	 * @param tLCGrpPayDueSchema
	 * @param tPayMoney
	 * @return
	 */
	private void insertPayDueDetail(LCGrpPayDueSchema tLCGrpPayDueSchema,double tPayMoney){
			
		LCGrpPayPlanDetailSet tLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet();
		LCGrpPayPlanDetailDB tLCGrpPayPlanDetailDB = new LCGrpPayPlanDetailDB();
		double tcal = 0;
		//tLCGrpPayPlanDetailDB.setGetNoticeNo(mGetNoticeNo);
		tLCGrpPayPlanDetailDB.setPrtNo(tLCGrpPayDueSchema.getPrtNo());
		tLCGrpPayPlanDetailDB.setContPlanCode(tLCGrpPayDueSchema.getContPlanCode());
		tLCGrpPayPlanDetailDB.setPlanCode(tLCGrpPayDueSchema.getPlanCode());
		tLCGrpPayPlanDetailSet = tLCGrpPayPlanDetailDB.query();
		if (tLCGrpPayPlanDetailSet.size() > 0) {
			for (int t = 1; t <= tLCGrpPayPlanDetailSet.size(); t++) {
						
				LCGrpPayPlanDetailSchema tLCGrpPayPlanDetailSchema = new LCGrpPayPlanDetailSchema();
				tLCGrpPayPlanDetailSchema = tLCGrpPayPlanDetailSet.get(t);
				LCGrpPayDueDetailSchema tLCGrpPayDueDetailSchema = new  LCGrpPayDueDetailSchema();
				tLCGrpPayDueDetailSchema.setPrtNo(tLCGrpPayDueSchema.getPrtNo());
				tLCGrpPayDueDetailSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
				tLCGrpPayDueDetailSchema.setGetNoticeNo(mGetNoticeNo);
				tLCGrpPayDueDetailSchema.setPayNo(payNo);
				tLCGrpPayDueDetailSchema.setProposalGrpContNo(tLCGrpPayDueSchema.getProposalGrpContNo());
				tLCGrpPayDueDetailSchema.setPlanCode(tLCGrpPayDueSchema.getPlanCode());
				tLCGrpPayDueDetailSchema.setPaytoDate(tLCGrpPayDueSchema.getPaytoDate());
				tLCGrpPayDueDetailSchema.setContPlanCode(tLCGrpPayDueSchema.getContPlanCode());
				tLCGrpPayDueDetailSchema.setPrem(getCalPrem(tLCGrpPayPlanDetailSchema,tPayMoney));
				tLCGrpPayDueDetailSchema.setConfState("01");
				tLCGrpPayDueDetailSchema.setConfDate(mConfDate);
				tLCGrpPayDueDetailSchema.setRiskCode(tLCGrpPayPlanDetailSchema.getRiskCode());
				tLCGrpPayDueDetailSchema.setOperator(tGI.Operator);
				tLCGrpPayDueDetailSchema.setMakeDate(CurrentDate);
				tLCGrpPayDueDetailSchema.setMakeTime(CurrentTime);
				tLCGrpPayDueDetailSchema.setModifyDate(CurrentDate);
				tLCGrpPayDueDetailSchema.setModifyTime(CurrentTime);
				tcal+=tLCGrpPayDueDetailSchema.getPrem();				
				mLCGrpPayDueDetailSet.add(tLCGrpPayDueDetailSchema);
				
			}
		}
//		补齐4舍5入差额
		if(tcal!=tPayMoney){
			for (int j = 1; j <= mLCGrpPayDueDetailSet.size(); j++) {
				
				if(mLCGrpPayDueDetailSet.get(j).getContPlanCode().equals(tLCGrpPayDueSchema.getContPlanCode())&&
						mLCGrpPayDueDetailSet.get(j).getPlanCode().equals(tLCGrpPayDueSchema.getPlanCode())){
					mLCGrpPayDueDetailSet.get(j).setPrem(Arith.round(mLCGrpPayDueDetailSet.get(j).getPrem()+tPayMoney-tcal,2));
					break;
				}
			}
		}
				
	}
	
	/***
	 * 按比例计算LCGrpPayDueDetail下每个险种的缴费金额
	 * @param tLCGrpPayPlanDetailSchema
	 * @param actu
	 * @return
	 */
	public double getCalPrem(LCGrpPayPlanDetailSchema tLCGrpPayPlanDetailSchema ,double actu){
		String allprem= new ExeSQL().getOneValue("select prem from LCGrpPayPlan where " +
				"prtno='"+mLCGrpContSchema.getPrtNo()+"' " +
						"and plancode='"+tLCGrpPayPlanDetailSchema.getPlanCode()+"' " +
								"and contplancode='"+tLCGrpPayPlanDetailSchema.getContPlanCode()+"'");
		double tallprem=Double.parseDouble(allprem);
		if(tallprem==0){
			return 0;
		}
		else
		return Arith.round(actu*tLCGrpPayPlanDetailSchema.getPrem()/Double.parseDouble(allprem),2);
	}
	
	private boolean prepareData() {
		mMMap.put(mLJSPaySchema, "DELETE");
		mMMap.put(mLJSPayBSchema, "UPDATE");
		mMMap.put(mLJAPaySchema, "INSERT");
		mMMap.put(mLJSPayGrpSet, "DELETE");
		mMMap.put(mLJSPayGrpBSet, "UPDATE");
		mMMap.put(mLJAPayGrpSet, "INSERT");
		mMMap.put(mLCGrpContSchema, "UPDATE");
		mMMap.put(mLJTempFeeNewSet, "UPDATE");
		mMMap.put(mLJTempFeeClassNewSet, "UPDATE");

		String sql1 = "update lcgrppayactu set State = '1',modifydate='"+CurrentDate+"',modifytime='"+CurrentTime+"' where GetNoticeNo = '" + mLJSPayBSchema.getGetNoticeNo() + "' ";
		System.out.println(sql1);
		mMMap.put(sql1, "UPDATE");
		String sql2 = "update lcgrppayactudetail set State = '1',modifydate='"+CurrentDate+"',modifytime='"+CurrentTime+"' where GetNoticeNo = '" + mLJSPayBSchema.getGetNoticeNo() + "' ";
		System.out.println(sql2);
		mMMap.put(sql2, "UPDATE");
		String sql3 = "update lcgrppayplandetail set State = '1',modifydate='"+CurrentDate+"',modifytime='"+CurrentTime+"' where prtno = '" + mLCGrpContSchema.getPrtNo()+"' and plancode=(select plancode from lcgrppayactu where getnoticeno='"+ mLJSPayBSchema.getGetNoticeNo() + "' fetch first 1 row only) ";
		System.out.println(sql3);
		mMMap.put(sql3, "UPDATE");
		if(mUpdateLCGrpPayDueSet.size()>0){
			
			mMMap.put(mUpdateLCGrpPayDueSet, "UPDATE");
		}
		
		mMMap.put(mInsertLCGrpPayDueSet, "INSERT");
		mMMap.put(mLCGrpPayDueDetailSet, "INSERT");
		
		mMMap.add(mAccMap);
		mResult.add(mMMap);

		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData mInputData) {
		// 应收总表
		System.out.println("in getinputdata");
		tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
		mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		mGetNoticeNo = (String) mTransferData.getValueByName("GetNoticeNo");
		if (mGetNoticeNo == null || mGetNoticeNo.equals("")) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrpLjsPlanConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的数据，请您确认!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private MMap lockLJAGet() {

		MMap tMMap = null;
		/** 锁定标志"TX" */
		String tLockNoType = "YH";
		/** 锁定时间 */
		String tAIS = "3600";
		System.out.println("设置了60分的解锁时间");
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("LockNoKey", mGetNoticeNo);
		tTransferData.setNameAndValue("LockNoType", tLockNoType);
		tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

		VData tVData = new VData();
		tVData.add(tTransferData);
		tVData.add(tGI);
		LockTableActionBL tLockTableActionBL = new LockTableActionBL();
		tMMap = tLockTableActionBL.getSubmitMap(tVData, null);

		if (tMMap == null) {
			mErrors.copyAllErrors(tLockTableActionBL.mErrors);
			return null;
		}
		return tMMap;
	}

	private void jbInit() throws Exception {
	}

}
