package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.xb.XBConst;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 续保删除业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author lh
 * @version 1.0
 */
//功能：查询出个人批改主表中本次申请的批改类型
//入口参数：个单的保单号、批单号
//出口参数：每条记录的个单的保单号、批单号和批改类型

public class GRnewAppCancelBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    public TransferData mTransferData = new TransferData();
    
    /** 数据操作字符串 */
    private String mOperate;

    private GlobalInput mGlobalInput = null;

    private LCGrpContSchema mLCGrpContSchema = null; //需要虚报撤销的保单号：旧保单号
    private LCGrpPolSet mRnewLCGrpPolSet = null; //需要撤销续保的险种：新险种
    private LCRnewStateLogSet mLCRnewStateLogSet = null;

    private String mNewGrpContNo = null;
    private LJSPaySchema mLJSPaySchema = null;

    //操作时间戳
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LCRnewStateLogSet mInitLCRnewStateLogSet = new LCRnewStateLogSet();


    private MMap map = new MMap();

    public GRnewAppCancelBL()
    {}

    /**
     * 得到续保撤销处理的操作信息
     * @param cInputData VData，需要LCContSchema（存储保单号即可），GlobalInput
     * @param cOperate String
     * @return MMap
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        System.out.println("Beginning of PRnewAppCanCelBL->getSubmitData");

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        System.out.println("after checkData...");

        if(!dealData())
        {
            return null;
        }

        //准备往后台的数据
        if(!prepareOutputData())
        {
            return null;
        }

        return map;
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.getSubmitMap(cInputData, cOperate);

        if(map == null && mErrors.needDealError())
        {
            return false;
        }

        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            mErrors.addOneError("撤销续保数据失败");
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String[] tableNames =
            {"LCGrpCont","LCCont","LCGrpPol","LCPol","lcgrpappnt", "LCDuty", "LCPrem", "LCGet",
            "LCInsureAcc", "LCInsureAccClass", "LCInsureAccTrace"
        };


        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = new LCPolSet();
        for(int i = 0; i < tableNames.length; i++)
        {
        	// ：2018年7月16日 分别获取团单险种下的每一个险种的责任信息，并做撤销操作
        	if(tableNames[i].equals("LCDuty")){
        		String polsql = "select * from lcpol "
        						+ "where grpcontno = '"+ this.mNewGrpContNo +"' ";
        		tLCPolSet = tLCPolDB.executeQuery(polsql);
        		if(tLCPolSet == null || "".equals(tLCPolSet)){
        			mErrors.addOneError("没有获取到险种信息！");
                    return false;
        		}
        		
        		//循环，根据得到的险种，分别获取责任信息
        		for (int j = 1; j <= tLCPolSet.size(); j++) {
					map.put("delete from LCDuty "
							+ "where polno = '"+ tLCPolSet.get(j).getPolNo() +"'", "DELETE");
				}
        		
        	}else{
            map.put("delete from " + tableNames[i]
                    + " where GrpContNo = '" + this.mNewGrpContNo + "' ", "DELETE");
        	}
        }

        map.put("delete from LCRnewStateLog "
                + " where newGrpContNo = '" + this.mNewGrpContNo + "' "
                + "   and state not in('" + XBConst.RNEWSTATE_CONFIRM + "', '"
                + XBConst.RNEWSTATE_DELIVERED + "', '"
                + XBConst.RNEWSTATE_XBSTOP + "') ",
                "DELETE");
        //撤销险种的续保终止处理
        if(mLJSPaySchema != null && mLJSPaySchema.getGetNoticeNo() != null)
        {
            map.put("delete from LCGrpContState "
                    + "where GrpcontNo = '" + mLCGrpContSchema.getGrpContNo() + "' "
                    + "   and otherNo = '" + mLJSPaySchema.getGetNoticeNo() + "' "
                    + "   and StateType = '" + XBConst.TERMINATE + "' "
                    + "   and StateReason = '11' ",
                    "DELETE");
        }
        
        String cancelmode = (String) mTransferData.getValueByName("tCancelMode");
        //如果作废原因选择的是0-客户终止缴费，则需要将险种设为缴费终止状态
        for (int m = 1; m <= tLCPolSet.size(); m++) {
        	if ("0".equals(cancelmode) && cancelmode != null ) {
        		map.put("update LCPol "
        				+ "polstate = '"+ BQ.POLSTATE_ZFEND +"' "
        				+ " where polno = '"+ tLCPolSet.get(m).getPolNo() +"'", "UPDATE");
        	}
		}
        
        // modify by fuxin  2008-7-22 根基应收的polno来置polstate,避免整单更新
        map.put("update LCPol "
                + "set polState = '' "
                + " where polno in (select polno From ljspaypersonb where getnoticeno ='"+mLJSPaySchema.getGetNoticeNo()+"')"
                + " and PolState not in ('" + BQ.POLSTATE_XBEND + "','" + BQ.POLSTATE_ZFEND + "')",
                "UPDATE");

        return true;

    }

    //准备提交数据
    private boolean prepareOutputData()
    {

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);
        mLCGrpContSchema = (LCGrpContSchema) data
                        .getObjectByObjectName("LCGrpContSchema", 0);
        mLJSPaySchema = (LJSPaySchema)data
                        .getObjectByObjectName("LJSPaySchema", 0);
        //2018年7月16日添加
        mTransferData = (TransferData) data.getObjectByObjectName(
                "TransferData", 0);
        
        
        if(mGlobalInput == null || mLCGrpContSchema == null
           || mLCGrpContSchema.getGrpContNo() == null)
        {
            mErrors.addOneError("转入的数据不完整。");
            return false;
        }

        return true;
    }

    /**
     *检查数据的合法性
     * 1、若状态为XBConst.RNEWSTATE_CONFIRM，则不能撤销
     * 2、若主险续保撤销，则发生续保的附加险也同时续保撤销
     **/
    private boolean checkData()
    {
        String sql = "  select * "
                     + "from LCRnewStateLog a "
                     + "where grpcontNo = '" + mLCGrpContSchema.getGrpContNo() + "' "
                     + " and state != '" + XBConst.RNEWSTATE_DELIVERED + "' "
                     + " and exists(select 1 from ljspaypersonb where a.grpcontno= grpcontno  and a.grppolno = grppolno and getnoticeno='"+mLJSPaySchema.getGetNoticeNo()+"')"
                     ; // modify by fuxin 根据续期抽当的polno来撤销保单信息。2008-7-22
        System.out.println(sql);
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        mLCRnewStateLogSet = tLCRnewStateLogDB.executeQuery(sql);

        sql = "  select a.* "
              + "from LCGrpPol a, LCRnewStateLog b "
              + "where a.grppolNo = b.newgrpPolNo "
              + "   and b.grpcontNo = '" + mLCGrpContSchema.getGrpContNo() + "' "
              + "   and b.state != '" + XBConst.RNEWSTATE_DELIVERED + "' ";
        System.out.println(sql);
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        mRnewLCGrpPolSet = tLCGrpPolDB.executeQuery(sql);

        if(mLCRnewStateLogSet.size() == 0 || mRnewLCGrpPolSet.size() == 0)
        {
            mErrors.addOneError("没有查询到需要续保撤销的数据");
            return false;
        }
        String state = mLCRnewStateLogSet.get(1).getState();
        if(state.equals(XBConst.RNEWSTATE_CONFIRM))
        {
            mErrors.addOneError("财务已核销，不能再进行续保撤销");
            return false;
        }

        mNewGrpContNo = mLCRnewStateLogSet.get(1).getNewGrpContNo();

        //若主险续保撤销，则发生续保的附加险也同时续保撤销
//        for(int i = 1; i <= mRnewLCGrpPolSet.size(); i++)
//        {
//            if(!mRnewLCGrpPolSet.get(i).getGrpPolNo()
//               .equals(mRnewLCGrpPolSet.get(i).getMainPolNo()))
//            {
//                continue;
//            }
//
//            //tLCPolSet.get(i)不是主险
//            for(int j = 1; j <= mLCRnewStateLogSet.size(); j++)
//            {
//                if(mRnewLCGrpPolSet.get(i).getMainPolNo()
//                   .equals(mLCRnewStateLogSet.get(j).getPolNo()))
//                {
//                    tLCPolDB.setPolNo(mLCRnewStateLogSet.get(j).getNewPolNo());
//                    tLCPolDB.getInfo();
//                    mRnewLCPolSet.add(tLCPolDB.getSchema());
//                    break;
//                }
//            }
//        }

        return true;
    }


    public static void main(String[] args)
    {
    	GRnewAppCancelBL tPRnewAppCancelBL = new GRnewAppCancelBL();
        VData tVData = new VData();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setPolNo("86330020050110041380");
        tLCPolSchema.setPrtNo("86110100145480");
        tVData.add(tLCPolSchema);
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";
        tVData.add(tGlobalInput);
        tPRnewAppCancelBL.submitData(tVData, "DELETE||Rnew");
    }
}
