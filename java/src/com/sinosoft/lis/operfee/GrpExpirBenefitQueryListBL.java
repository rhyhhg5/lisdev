package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.vschema.LDCodeSet;
import java.sql.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpExpirBenefitQueryListBL {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = new TransferData();
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private SSRS mSSRS;
    private SSRS mRiskSSRS;
    private String mStartDate = ""; //起始日
    private String mEndDate = ""; //截止日
    private String mDealState = ""; //催收状态
    private String msql = "";
    private String mGrpContNo = "";
    private String mOtherNo ="";
    private TextTag textTag = new TextTag(); //新建一个TextTag的实例
    public GrpExpirBenefitQueryListBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {

            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            mOtherNo =(String)mTransferData.getValueByName("otherNo");
        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }
        //mStartDate = (String) mTransferData.getValueByName("SingleStartDate");
       // mEndDate = (String) mTransferData.getValueByName("SingleEndDate");
       // mDealState = (String) mTransferData.getValueByName("DealState");

        return true;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
        String tSQL =" select b.getnoticeno,(select insuredname from lcpol where b.contno = contno and b.polno=polno) "
                    +" ,(select codename('sex',insuredsex) from lcpol where b.contno = contno and b.polno=polno) "
                    +" ,(select insuredbirthday from lcpol where b.contno = contno and b.polno=polno) "
                    +" ,(select occupationtype from lcpol where b.contno = contno and b.polno=polno ) "
                    +" ,(select codename('idtype',idtype) from lcinsured where b.contno = contno ) "
                    +" ,(select idno from lcinsured where b.contno = contno )  "
                    +" ,(select contplancode from lcpol where b.contno = contno and b.polno = polno ) "
                    +" ,(select bankname from ldbank where b.bankcode = bankcode),b.accname,b.bankaccno "
                    +" ,(case when dealstate ='0' then '待给付' when dealstate ='1' then '已付费' when dealstate ='2' then '已撤销' end ),b.GrpContNo "
                    +" ,b.getmoney "
                    +" From ljaget a,ljsgetdraw b,ljsget c  where a.actugetno = b.getnoticeno and a.actugetno = c.getnoticeno  and a.sumgetmoney >0  "
                    +" and a.otherno ='"+mOtherNo+"'"
                    +" and a.othernotype ='21' group by b.getnoticeno,contno,polno,b.accname,b.bankaccno,b.bankcode,grpcontno,actugetno,b.getmoney,dealstate "
                    +" with ur	"
                    ;


        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {

        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpExpirBenefitQueryList.vts", "printer"); //最好紧接着就初始化xml文档

        String[] title = {"序", "给付号", "被保人名称", "性别", "出生日期",
                         "职业类别", "证件类型", "证件号","保险计划","给付银行 ","户名", "账户",
                         "转帐成功与否"};

        xmlexport.addListTable(getListTable(), title);
        xmlexport.outputDocumentToFile("C:\\", "GrpExpirBenefitQueryList");
        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }

        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[15];
            //标记序列号
            info[0] = String.valueOf(i);
            info[1] = mSSRS.GetText(i,1);
            info[2] = mSSRS.GetText(i,2);
            info[3] = mSSRS.GetText(i,3);
            info[4] = mSSRS.GetText(i,4);
            info[5] = mSSRS.GetText(i,5);
            info[6] = mSSRS.GetText(i,6);
            info[7] = mSSRS.GetText(i,7);
            info[8] = mSSRS.GetText(i,8);
            info[9] = mSSRS.GetText(i,9);
            info[10] = mSSRS.GetText(i,10);
            info[11] = mSSRS.GetText(i,11);
            info[12] = mSSRS.GetText(i,12);
            mGrpContNo = mSSRS.GetText(i,13);
            info[14] = mSSRS.GetText(i,14);
            tListTable.add(info);
        }
        textTag.add("GrpContNo", mGrpContNo);
        textTag.add("otherNo",mOtherNo);
        textTag.add("GrpName",new ExeSQL().getOneValue("select grpName from lcgrpcont where grpcontno ='"+mGrpContNo+"'"));
        tListTable.setName("List");
        return tListTable;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


    private void jbInit() throws Exception {
    }

    //查询公司名称
    private String getCompanyName() {
        //通过LDCode得到保单号，然后得到公司名称
        String manageCode = mGlobalInput.ManageCom;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.setCode(manageCode);
        LDCodeSet set = tLDCodeDB.query();
        if (set == null || set.size() == 0) {

            mErrors.addOneError("没有查到公司名");
            System.out.println("没有查到公司名" + tLDCodeDB.mErrors.getErrContent());
            return null;
        } else {
            return set.get(1).getCodeName();
        }
    }

}
