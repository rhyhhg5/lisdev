package com.sinosoft.lis.operfee;
//程序名称：ULIUnlockHuanBL.java
//程序功能：万能缓交状态恢复
//创建日期：2009-7-20
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.task.Task;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.util.*;

public class ULIUnlockHuanBL extends LJSUnlockBL
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	
	private MMap map = new MMap();
	
	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	
	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput() ;
	
	/** 操作员 */
	private String mOperater ;
	
	private String mContNo = null ;
	
	public ULIUnlockHuanBL() 
	{
	}
	
	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
    public boolean submitData(VData cInputData)
	{
        //将操作数据拷贝到本类中
	    if (!getInputData(cInputData))
	    {
	        return false;
	    }
	    
        //数据校验
        if (!checkData()) 
        {
            return false;
        }
	    
	    //进行业务处理
	    if (!dealData())
	    {
	        // @@错误处理
            CError tError = new CError();
	        tError.moduleName = "ULIUnlockHuanBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败ULIUnlockHuanBL-->dealData!";
	        this.mErrors .addOneError(tError) ;
	        return false;
	    }
	    
	    //提交
	    if(!submitData())
	    {
	    	return false;
	    }
	    
	    return true;
	}
	 /**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean dealData()
	{
		StringBuffer sql = new StringBuffer();
		sql.append("update lcpol set standbyflag1 = null, ")
		   .append("operator = '").append(mOperater).append("', ")
		   .append("modifydate = '").append(mCurrentDate).append("', ")
		   .append("modifytime = '").append(mCurrentTime).append("' ")
		   .append("where ContNo = '").append(mContNo).append("'");
		map.put(sql.toString(), SysConst.UPDATE);
	    return true;
	}
	
	 /**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
	    this.mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0);
	    this.mOperater = this.mGlobalInput.Operator;
	    TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
	    mContNo = (String)tTransferData.getValueByName("ContNo");
		return true;
	}
	 
    private boolean submitData()
    {
    	VData tVData = new VData();
    	tVData.add(map);
    	PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tVData, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ULIUnlockHuanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
	    return true;
    }
    
    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() 
    {
        System.out.println("进行校验.....");
        if (mGlobalInput == null) 
        {
        	CError tError = new CError();
            tError.moduleName = "ULIUnlockHuanBL";
            tError.functionName = "checkData";
            tError.errorMessage = "传入的mGlobalInput为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mContNo == null || "".equals(mContNo) || "null".equals(mContNo)) 
        {
        	CError tError = new CError();
            tError.moduleName = "ULIUnlockHuanBL";
            tError.functionName = "checkData";
            tError.errorMessage = "传入的mContNo为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String sql = "select StateFlag,StandByFlag1 from lcpol where mainpolno = polno and contno = '"+mContNo+"' ";
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        String tStateFlag = tSSRS.GetText(1, 1);
        String tStandByFlag1 = tSSRS.GetText(1, 2);
        if(!tStandByFlag1.equals(BQ.ULI_HUAN))
        {
        	CError tError = new CError();
            tError.moduleName = "ULIUnlockHuanBL";
            tError.functionName = "checkData";
            String errorInfo = "该保单现在不是缓交状态，不需要进行缓交恢复!";
            tError.errorMessage = errorInfo;
            this.mErrors.addOneError(tError);
            return false;
        }
        if(!"1".equals(tStateFlag))
        {
        	CError tError = new CError();
            tError.moduleName = "ULIUnlockHuanBL";
            tError.functionName = "checkData";
            String errorInfo = "该保单现在是" + ("2".equals(tStateFlag)?"失效":"终止") + "状态，不能进行缓交恢复!";
            tError.errorMessage = errorInfo;
            this.mErrors.addOneError(tError);
            return false;
        }
        if(!CommonBL.checkULIHuan(mContNo))
        {
        	CError tError = new CError();
            tError.moduleName = "ULIUnlockHuanBL";
            tError.functionName = "checkData";
            tError.errorMessage = "账户价值不足以抵扣风险保费和账户管理费，不能进行缓交恢复!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
