package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 期交保费录入用户接口，作为用户接口接收前台页面传入的数据，并向后台程序递交操作。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class GrpDueFeePremInpuUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = null;

    public GrpDueFeePremInpuUI()
    {
    }

    /**
     * 外部操作的提交方法，处理录入保费和交费人数
     * @param data VData
     * @param operate String
     * @return boolean：成功true，否则false
     */
    public boolean submitData(VData data, String operate)
    {
        GrpDueFeePremInpuBL bl = new GrpDueFeePremInpuBL();
        if(!bl.submitData(data, operate))
        {
            mErrors = bl.mErrors;
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        GrpDueFeePremInpuUI grpduefeepreminpuui = new GrpDueFeePremInpuUI();
    }
}
