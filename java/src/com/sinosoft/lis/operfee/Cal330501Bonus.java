package com.sinosoft.lis.operfee;

import java.util.Date;
import java.util.HashMap;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.text.DecimalFormat;
//程序名称：Cal330501Bonus.java
//程序功能：计算常无忧B忠诚奖
//创建日期：2010-06-07
//创建人  ：zhanggm
//更新记录：  
public class Cal330501Bonus 
{
    private CErrors mCErrors ;
    
    private String mFormula = null; //B-公式二；C-公式三
    private double mAmnt = 0.0; //保额
    private double mPrem = 0.0; //保费
    private String mCValiDate = null; //保单生效日
    private String mEndDate = null; //保单终止日
    private int InsuYear = 0; //保险年期
    private int InsuredAppAge = 0; //被保人投保年龄
    private double InitBonus = 0.0; //初始忠诚奖
    private double I = 0.0; //初始固定利率
    private double K = 0.0; //比例参数
    
    private String mRateFlag = "N"; //息降我不降 N-不需要息降我不降 Y-需要息降我不降
    
    private LCPolSchema mLCPolSchema = new LCPolSchema();

    public Cal330501Bonus(LCPolSchema aLCPolSchema) 
    {
    	mLCPolSchema.setSchema(aLCPolSchema);
	}
	public Cal330501Bonus() {
    }
    public HashMap getBonus(String formula, String rateflag)
    {
    	HashMap hashMap = new HashMap();
    	
    	if(formula==null || formula.equals(""))
        {
        	mCErrors.addOneError("获取计算公式错误!");
        	return hashMap;
        }
    	
    	mFormula = formula;
    	System.out.println("按照 公式" + formula + " 进行计算忠诚奖：");
    	
    	if(formula.equals("B") || formula.equals("C"))
    	{
    		if(!getInputData(formula,rateflag))
        	{
        		return hashMap;
        	}
        	
        	double addMoney = 0.0; //外加忠诚奖
        	double initMoney = 0.0; //初始忠诚奖
        	
        	String tYear = null;
        	if(3==InsuYear)
        	{
        		tYear = "3";
        	}
        	else if(5==InsuYear || 10==InsuYear)
        	{
        		tYear = "5";
        	}
            LMBankRateSet tLMBankRateSet = getBankRate("Y", tYear, "S", "H");
            if (tLMBankRateSet.size() == 0) 
            {
                mCErrors.addOneError("查询利息表错误!");
                return hashMap;
            }
            double param = getParam(tLMBankRateSet);
            
            //外加忠诚奖
            addMoney =  mPrem * param / 365 / K; 
            
            String content = null;
            if(mFormula.equals("C"))
            {
            	content = "外加忠诚奖 = 趸缴保费 * 参数 / 365 = " 
     	           + String.valueOf(mPrem) + " * " + PubFun.setPrecision(param,"0.00000") + " / 365 = " + String.valueOf(addMoney);
            }
            else
            {
            	content = "外加忠诚奖 = 趸缴保费 * 参数 / 365 / K = " 
     	           + String.valueOf(mPrem) + " * " + PubFun.setPrecision(param,"0.00000") + " / 365 / " + String.valueOf(K) + " = " + String.valueOf(addMoney);
            }
            System.out.println(content);
            
//          初始忠诚奖
            initMoney = (mAmnt/1000) * InitBonus;
            content = "初始忠诚奖 = 保额/1000 * 千元保额初始忠诚奖 = " 
    	            + String.valueOf(mAmnt) + "/1000 * " + String.valueOf(InitBonus) + " = " + String.valueOf(initMoney);
            System.out.println(content);
            
            hashMap.put("InitMoney", PubFun.setPrecision2(initMoney));
            hashMap.put("AddMoney", PubFun.setPrecision2(addMoney));
    	}
    	else if(formula.equals("D"))
    	{
    		mAmnt = mLCPolSchema.getAmnt();
            InsuYear = mLCPolSchema.getInsuYear();
            InsuredAppAge = mLCPolSchema.getInsuredAppAge();
            mCValiDate = mLCPolSchema.getCValiDate();
            
            StringBuffer content = new StringBuffer();
            content.append("准备计算要素\n保额：").append(mAmnt).append("\n生效日期：").append(mCValiDate).append("\n保单期限：").append(InsuYear).append("\n被保人投保年龄：").append(InsuredAppAge);
            System.out.println(content);
            
            String sql = "select Rate from Formula4_330501 where InsuYear = " + InsuYear 
                       + " and StartDate <= '" + mCValiDate + "' and EndDate >= '" + mCValiDate 
                       + "' and MinAge <= " + InsuredAppAge + " and MaxAge >= " + InsuredAppAge + " with ur ";;
            String baseBonus = new ExeSQL().getOneValue(sql);
            if(baseBonus.equals(""))
            {
            	System.out.println("查询公式四基础数据错误Formula4_330501！");
            	baseBonus = "0";
            }
            
            double addMoney = 0.0; //外加忠诚奖
        	double initMoney = 0.0; //初始忠诚奖
        	
            initMoney = mAmnt/1000* Double.parseDouble(baseBonus);
            
            String content1 = null;
            content1 = "初始忠诚奖 = 保额/1000 * 参数 = " + String.valueOf(mAmnt) + "/1000 * " + baseBonus + " = " + initMoney;
            System.out.println(content1);
            
            hashMap.put("InitMoney", PubFun.setPrecision2(initMoney));
            hashMap.put("AddMoney", PubFun.setPrecision2(addMoney));
    	}
    	else if(formula.equals("E"))
    	{
        	mAmnt = mLCPolSchema.getAmnt();
            InsuYear = mLCPolSchema.getInsuYear();
            InsuredAppAge = mLCPolSchema.getInsuredAppAge();
            mPrem = getPrem();
            if(mPrem==0.0)
            {
            	mCErrors.addOneError("获取保费错误!");
            	return hashMap;
            }
            
            StringBuffer content = new StringBuffer();
            content.append("准备计算要素\n保额：").append(mAmnt).append("\n保费：").append(mPrem).append("\n保单期限：").append(InsuYear).append("\n被保人投保年龄：").append(InsuredAppAge);
            System.out.println(content);
            
//          满期客户忠诚奖=趸缴保费*（1+3*5.381%）-保额
            double addMoney = 0.0; //外加忠诚奖
        	double initMoney = 0.0; //初始忠诚奖
        	
        	double paramE = 0.0;
        	if(InsuredAppAge<=50)
        	{
        		paramE = 0.05381;
        	}
        	else
        	{
        		paramE = 0.05403;
        	}
        	initMoney = mPrem*(1+InsuYear * paramE)-mAmnt;
        	
        	String content1 = null;
            content1 = "初始忠诚奖 = 趸缴保费 * ( 1 + 保单期限 * " + (InsuredAppAge<=50?"5.381":"5.403") + "% ) - 保额 = " + mPrem + " * ( 1 + " + InsuYear + " * " + paramE + ") - " + mAmnt 
                      + " = " + PubFun.setPrecision2(initMoney);
            System.out.println(content1);
        	
        	hashMap.put("InitMoney", PubFun.setPrecision2(initMoney));
            hashMap.put("AddMoney", PubFun.setPrecision2(addMoney));
    	}
    	else if(formula.equals("F"))
    	{
        	mAmnt = mLCPolSchema.getAmnt();
            InsuYear = mLCPolSchema.getInsuYear();
            InsuredAppAge = mLCPolSchema.getInsuredAppAge();
            mPrem = getPrem();
            if(mPrem==0.0)
            {
            	mCErrors.addOneError("获取保费错误!");
            	return hashMap;
            }
            
            StringBuffer content = new StringBuffer();
            content.append("准备计算要素\n保额：").append(mAmnt).append("\n保费：").append(mPrem).append("\n保单期限：").append(InsuYear).append("\n被保人投保年龄：").append(InsuredAppAge);
            System.out.println(content);
            
//            满期客户忠诚奖=趸缴保费*（1+公式六比例*3）-保额
//          三年期：
//          年龄：0-50岁：938×（1+5.38%×3）-1000=89.39
//          年龄：51-65岁：948×（1+5.4%×3）-1000=101.58
//          五年期：
//          年龄：0-50岁：898×（1+5.72%×5）-1000=154.83
//          年龄：51-65岁：908×（1+5.72%×5）-1000=167.69
            //InitFixedInterest 中，公式F存的是百分比
            
            double addMoney = 0.0; //外加忠诚奖
        	double initMoney = 0.0; //初始忠诚奖
        	
        	double paramF = getI() ;
        	
        	initMoney = mPrem*(1 + InsuYear * paramF * 0.01)-mAmnt;
        	
        	String content1 = null;
            content1 = "初始忠诚奖 = 趸缴保费 * ( 1 + 保单期限 * " + paramF + "% ) - 保额 = " + mPrem + " * ( 1 + " + InsuYear + " * " + paramF + ") - " + mAmnt 
                      + " = " + PubFun.setPrecision2(initMoney);
            System.out.println(content1);
        	
        	hashMap.put("InitMoney", PubFun.setPrecision2(initMoney));
            hashMap.put("AddMoney", PubFun.setPrecision2(addMoney));
    	}
    	else if(formula.equals("G"))
    	{
        	mAmnt = mLCPolSchema.getAmnt();
            InsuYear = mLCPolSchema.getInsuYear();
            InsuredAppAge = mLCPolSchema.getInsuredAppAge();
            mPrem = getPrem();
            if(mPrem==0.0)
            {
            	mCErrors.addOneError("获取保费错误!");
            	return hashMap;
            }
            
            StringBuffer content = new StringBuffer();
            content.append("准备计算要素\n保额：").append(mAmnt).append("\n保费：").append(mPrem).append("\n保单期限：").append(InsuYear).append("\n被保人投保年龄：").append(InsuredAppAge);
            System.out.println(content);

//          公式七：按照保单保费为基数，以5.381%（三年期产品）为年化收益率，计算保单的满期金总额，即：保单满期金=保单总保费*（1+5.381%*3）。
            
            double addMoney = 0.0; //外加忠诚奖
        	double initMoney = 0.0; //初始忠诚奖
        	
        	double paramG = getI() ;
        	
        	initMoney = mPrem*(1 + InsuYear * paramG * 0.01)-mAmnt;
        	
        	String content1 = null;
            content1 = "初始忠诚奖 = 趸缴保费 * ( 1 + 保单期限 * " + paramG + "% ) - 保额 = " + mPrem + " * ( 1 + " + InsuYear + " * " + paramG + "%) - " + mAmnt 
                      + " = " + PubFun.setPrecision2(initMoney);
            System.out.println(content1);
        	
        	hashMap.put("InitMoney", PubFun.setPrecision2(initMoney));
            hashMap.put("AddMoney", PubFun.setPrecision2(addMoney));
    	}

        return hashMap;
    }
    private boolean getInputData(String formula, String rateflag) 
    {
    	mFormula = formula;
    	if(mFormula==null || mFormula.equals(""))
        {
        	mCErrors.addOneError("获取计算公式错误!");
        	return false;
        }

    	mRateFlag = rateflag;
    	if(mRateFlag==null || mRateFlag.equals("") || !(mRateFlag.equals("Y") || mRateFlag.equals("N")))
        {
        	mCErrors.addOneError("获取息降我不降标志错误!");
        	return false;
        }
    	if(mRateFlag.equals("Y"))
		{
			System.out.println("-------息降我不降---------");
		}
    	mAmnt = mLCPolSchema.getAmnt();
        mCValiDate = mLCPolSchema.getCValiDate();
        mEndDate = mLCPolSchema.getEndDate();
        InsuYear = mLCPolSchema.getInsuYear();
        InsuredAppAge = mLCPolSchema.getInsuredAppAge();
        mPrem = getPrem();
        if(mPrem==0.0)
        {
        	mCErrors.addOneError("获取保费错误!");
            return false;
        }
        InitBonus = getInitBonus();
        if(InitBonus==0.0)
        {
        	mCErrors.addOneError("获取初始忠诚奖错误!");
            return false;
        }
        I = getI();
        if(I==0.0)
        {
        	mCErrors.addOneError("获取初始固定利率错误!");
        	return false;
        }
        K = getK();
        if(K==0.0)
        {
        	mCErrors.addOneError("获取比例系数K错误!");
        	return false;
        }
        
        StringBuffer content = new StringBuffer();
        content.append("准备计算要素\n保额：").append(mAmnt).append("\n保费：").append(mPrem).append("\n生效日：").append(mCValiDate)
               .append("\n终止日：").append(PubFun.calDate(mEndDate, -1, "D", "")).append("\n保单期限：").append(InsuYear).append("\n被保人投保年龄：").append(InsuredAppAge)
               .append("\n初始忠诚奖：").append(InitBonus).append("\n初始固定利率：").append(I);
        System.out.println(content);
		return true;
	}
    private double getPrem() //合并保费
	{
    	String sql = null;
//    	//从LCCont表取不靠谱
//		从费率表算
		sql = "select sum(prem) from (select * from rate330501 union all select * from rate530301) as a  "
			+ "where InsuYear = " + InsuYear + " and MinAge <= " + InsuredAppAge + " and MaxAge >= " + InsuredAppAge + " with ur ";
		String basePrem = new ExeSQL().getOneValue(sql);
		if(basePrem.equals(""))
        {
			basePrem = "0";
        }
		return mAmnt/1000*Double.parseDouble(basePrem);
	}
	private double getI() //初始固定利率
	{
		String sql = "select Interest from InitFixedInterest where Type = '" + mFormula + "' and InsuYear = " + InsuYear 
		           + " and MinAge <= " + InsuredAppAge + " and MaxAge >= " + InsuredAppAge + " with ur ";
		String interest = new ExeSQL().getOneValue(sql);
		if(interest.equals(""))
        {
			interest = "0";
        }
		return Double.parseDouble(interest); 
	}
	private double getK() //比例参数 公式一和公式二中，K：3年期和5年期的产品K值为2，10年期的产品K值为3；公式三没有，所以K=1
	{
		double tK = 0.0;
		if(mFormula.equals("A") || mFormula.equals("B"))
		{
			if(InsuYear==3 || InsuYear==5)
			{
				tK = 2;
			}
			else if(InsuYear==10)
			{
				tK = 3;
			}
			else
			{
				tK = 1;
			}
		}
		else if(mFormula.equals("C"))
		{
			tK = 1;
		}
		else
		{
			tK = 1;
		}
		return tK; 
	}
	private double getInitBonus() //初始忠诚奖
	{
		String sql = "select Rate from InitCustLoy where InsuYear = " + InsuYear + " and MinAge <= " + InsuredAppAge + " and MaxAge >= " + InsuredAppAge + " with ur ";
		String rate = new ExeSQL().getOneValue(sql);
		if(rate.equals(""))
        {
			rate = "0";
        }
		return Double.parseDouble(rate); 
	}
	private double getParam(LMBankRateSet tLMBankRateSet)
    {
		System.out.println("开始计算算费参数：");
        double result =0.0;
        
        LMBankRateSet tempLMBankRateSet = new LMBankRateSet();
        for(int i=1;i<=tLMBankRateSet.size();i++)
        {
            LMBankRateSchema tLMBankRateSchema = tLMBankRateSet.get(i).getSchema();
            tempLMBankRateSet.add(tLMBankRateSchema);
            int intv =0;
            String content = null;
            try
            {
            	if(i==1)
                {
                	intv = PubFun.calInterval(mCValiDate, tLMBankRateSchema.getEndDate(), "D");
                	content="起始日期：" + mCValiDate + "零时，终止日期：" + tLMBankRateSchema.getEndDate() + "零时，间隔天数:" + intv;
                	
                }
                else if (i==tLMBankRateSet.size())
                {
                	intv = PubFun.calInterval(tLMBankRateSchema.getStartDate(), mEndDate, "D");
                	content="起始日期：" + tLMBankRateSchema.getStartDate() + "零时，终止日期：" + mEndDate + "零时，间隔天数:" + intv;
                	
                }
                else
                {
                	intv = PubFun.calInterval(tLMBankRateSchema.getStartDate(), tLMBankRateSchema.getEndDate(), "D");
                	content="起始日期：" + tLMBankRateSchema.getStartDate() + "零时，终止日期：" + tLMBankRateSchema.getEndDate() + "零时，间隔天数:" + intv;
        
                }
            	
            }
            catch(Exception e)
            {
            	mCErrors.addOneError("计算利率间隔天数错误!");
                return 0.0;
            }
           System.out.println(content);
           
           double tBankRate = tLMBankRateSchema.getRate();
           System.out.println("银行利率 = " + String.valueOf(tBankRate));
           
           //息降我不降
           if("Y".equals(mRateFlag))
           {
        	   tBankRate = getMaxRate(tempLMBankRateSet);
        	   System.out.println("息降我不降取银行利率 = " + String.valueOf(tBankRate));
           }
           double rate = tBankRate-I;
           rate = PubFun.setPrecision(rate,"0.00000");
           
           if(rate<0)
           {
               rate = 0;
           }
           System.out.println("银行利率 - 初始固定利率 = " + String.valueOf(rate));
           double tempresult = PubFun.setPrecision(intv*rate,"0.00000");
           System.out.println("计算参数 = 间隔天数 * Max((银行利率 - 初始固定利率),0) = " + intv + " * " + rate + " = " + String.valueOf(tempresult));

           result += tempresult;
           System.out.println("循环累计参数值：" + PubFun.setPrecision(result,"0.00000"));
           
        }
        return result;
    }
	
	private double getMaxRate(LMBankRateSet aLMBankRateSet) 
	{
		double maxRate = 0;
		for(int i=1; i<=aLMBankRateSet.size(); i++)
		{
			double tempRate = aLMBankRateSet.get(i).getRate();
			maxRate = tempRate>maxRate ? tempRate : maxRate;
		}
		return maxRate;
	}
	/**
     * 获取银行利率
     * @param aRateIntvFlag String 利率间隔标记 Y －－ 年利率 M －－ 月利率 D －－ 日利率
     * @param aRateIntv String 利率间隔 0-活期
     * @param aRateType String 利率类型 S －－ 单利 C －－ 复利
     * @param aRateType2 String 利率类型2 Q －－ 税前利率 H －－ 税后利率
     * @return LMBankRateSet
     */
	
    private LMBankRateSet getBankRate(String aRateIntvFlag, String aRateIntv, String aRateType, String aRateType2)
    {
    	String sql = "select * from LMBankRate where RateIntvFlag = '" + aRateIntvFlag + "' "
    	           + "and RateIntv = '" + aRateIntv + "' and RateType = '" + aRateType + "' and RateType2 = '" + aRateType2 + "' "
                   + "and ((StartDate <= '" + mCValiDate + "' and EndDate > '" + mCValiDate + "') "
                   + "or (StartDate >= '" + mCValiDate + "' and EndDate < '" + mEndDate + "') "
                   + "or (StartDate < '" + mEndDate + "' and EndDate >= '" + mEndDate + "') " 
                   + ") order by StartDate asc with ur ";
    	System.out.println(sql);
    	LMBankRateDB tLMBankRateDB = new LMBankRateDB();
    	return tLMBankRateDB.executeQuery(sql);
    }

    public CErrors getError()
    {
        return mCErrors;
    }
    
    //1000.00,CommonBL.stringToDate("2007-7-15"),3,40
    public static void main(String arg[])
    {
    	String sql = "select * "
                   + "from lcpol a where riskcode = '330501' " 
                   + "and contno in " 
                   + "('000917201000066') "
                   + "order by contno with ur";
    	
    	LCPolDB tLCPolDB = new LCPolDB();
    	LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);
    	
    	for(int i=1; i<= tLCPolSet.size(); i++)
    	{
    		LCPolSchema tLCPolSchema = tLCPolSet.get(i).getSchema();
    		System.out.println("计算保单 = " + tLCPolSchema.getContNo());
    		Cal330501Bonus t = new Cal330501Bonus(tLCPolSchema);
            HashMap hashMap = t.getBonus("F","Y");; //忠诚奖
            System.out.println("initMoney = " + hashMap.get("InitMoney"));
            System.out.println("addMoney = " + hashMap.get("AddMoney"));
            System.out.println("initMoney+addMoney = " + String.valueOf(Double.parseDouble((String)hashMap.get("AddMoney"))+Double.parseDouble((String)hashMap.get("InitMoney"))));
    	}
    	
    	/* 公式二，公式三
    	double aAmnt = 100000.0;
        String aCValiDate = "2007-7-15";
        int aInsuYear = 3; 
        String aEndDate = PubFun.calDate(aCValiDate, aInsuYear, "Y", "");
      //  aEndDate = PubFun.calDate(aEndDate, -1, "D", "");
      //  System.out.println(aEndDate);
        int aInsuredAppAge = 25;
        
    	LCPolSchema tLCPolSchema = new LCPolSchema();
    	tLCPolSchema.setAmnt(aAmnt);
    	tLCPolSchema.setCValiDate(aCValiDate);
    	tLCPolSchema.setEndDate(aEndDate);
    	tLCPolSchema.setInsuYear(aInsuYear);
    	tLCPolSchema.setInsuredAppAge(aInsuredAppAge);
    	Cal330501Bonus t = new Cal330501Bonus(tLCPolSchema);
        HashMap hashMap = t.getBonus("C","N"); //忠诚奖
        System.out.println("initMoney = " + hashMap.get("InitMoney"));
        System.out.println("addMoney = " + hashMap.get("AddMoney"));
        System.out.println("initMoney+addMoney = " + CommonBL.carry(Double.parseDouble((String)hashMap.get("AddMoney"))+Double.parseDouble((String)hashMap.get("InitMoney"))));
        HashMap hashMap1 = t.getBonus("C","Y"); //忠诚奖
        System.out.println("initMoney = " + hashMap1.get("InitMoney"));
        System.out.println("addMoney = " + hashMap1.get("AddMoney"));
        System.out.println("initMoney+addMoney = " + CommonBL.carry(Double.parseDouble((String)hashMap1.get("AddMoney"))+Double.parseDouble((String)hashMap1.get("InitMoney"))));
        HashMap hashMap2 = t.getBonus("B","N"); //忠诚奖
        System.out.println("initMoney = " + hashMap2.get("InitMoney"));
        System.out.println("addMoney = " + hashMap2.get("AddMoney"));
        System.out.println("initMoney+addMoney = " + CommonBL.carry(Double.parseDouble((String)hashMap2.get("AddMoney"))+Double.parseDouble((String)hashMap2.get("InitMoney"))));
        HashMap hashMap3 = t.getBonus("B","Y"); //忠诚奖
        System.out.println("initMoney = " + hashMap3.get("InitMoney"));
        System.out.println("addMoney = " + hashMap3.get("AddMoney"));
        System.out.println("initMoney+addMoney = " + CommonBL.carry(Double.parseDouble((String)hashMap3.get("AddMoney"))+Double.parseDouble((String)hashMap3.get("InitMoney"))));
        
        System.out.println("CN:initMoney+addMoney = " + CommonBL.carry(Double.parseDouble((String)hashMap.get("AddMoney"))+Double.parseDouble((String)hashMap.get("InitMoney"))));
        System.out.println("CY:initMoney+addMoney = " + CommonBL.carry(Double.parseDouble((String)hashMap1.get("AddMoney"))+Double.parseDouble((String)hashMap1.get("InitMoney"))));
        System.out.println("BN:initMoney+addMoney = " + CommonBL.carry(Double.parseDouble((String)hashMap2.get("AddMoney"))+Double.parseDouble((String)hashMap2.get("InitMoney"))));
        System.out.println("BY:initMoney+addMoney = " + CommonBL.carry(Double.parseDouble((String)hashMap3.get("AddMoney"))+Double.parseDouble((String)hashMap3.get("InitMoney"))));
        */
    	
    	/*
    	double aAmnt = 10000.0;
        String aCValiDate = "2008-4-2";
        int aInsuYear = 10; 
        String aEndDate = PubFun.calDate(aCValiDate, aInsuYear, "Y", "");
        int aInsuredAppAge = 50;
        LCPolSchema tLCPolSchema = new LCPolSchema();
    	tLCPolSchema.setAmnt(aAmnt);
    	tLCPolSchema.setCValiDate(aCValiDate);
    	tLCPolSchema.setEndDate(aEndDate);
    	tLCPolSchema.setInsuYear(aInsuYear);
    	tLCPolSchema.setInsuredAppAge(aInsuredAppAge);
    	Cal330501Bonus t = new Cal330501Bonus(tLCPolSchema);
        HashMap hashMap3 = t.getBonus("E","N"); //忠诚奖
        System.out.println("initMoney = " + hashMap3.get("InitMoney"));
        System.out.println("addMoney = " + hashMap3.get("AddMoney"));
        System.out.println("initMoney+addMoney = " + CommonBL.carry(Double.parseDouble((String)hashMap3.get("AddMoney"))+Double.parseDouble((String)hashMap3.get("InitMoney"))));
        */
    }
}
