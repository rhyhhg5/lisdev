package com.sinosoft.lis.operfee;

import com.sinosoft.lis.f1print.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft </p>
 *
 * @author qulq
 * @version 1.0
 */
public class IndiHospitalQueryBL implements PrintService {
    public CErrors mErrors = new CErrors();
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private String mManaCom;
    private String mPayNo;
    private String mOperate = "";
    private int needPrt = 0;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    public IndiHospitalQueryBL() {
    }

    /**
     * getErrors
     *
     * @return CErrors
     * @todo Implement this com.sinosoft.lis.f1print.PrintService method
     */
    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * getResult
     *
     * @return VData
     * @todo Implement this com.sinosoft.lis.f1print.PrintService method
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * submitData
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     * @todo Implement this com.sinosoft.lis.f1print.PrintService method
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }
        mOperate = cOperate;
        if (!checkData()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (mOperate.equals("INSERT") && needPrt == 0) {
            if (!dealPrintMag()) {
                return false;
            }
        }
        return true;
    }

    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                               getObjectByObjectName("LOPRTManagerSchema", 0);
        if (ttLOPRTManagerSchema == null) { //为空--VTS打印入口
            TransferData mTransferData = (TransferData) cInputData.
                                         getObjectByObjectName(
                                                 "TransferData", 0);
            if (mTransferData == null) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpDueFeeBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的数据，请您确认!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mManaCom = (String) mTransferData.getValueByName("ManageCom");
            this.mPayNo = (String) mTransferData.getValueByName("PayNo");
        } else { //不为空--PDF打印入口
            System.out.println("---------为空--VTS打印入口！");
            if (ttLOPRTManagerSchema.getStandbyFlag2().indexOf("86") == 0) {
                this.mManaCom = ttLOPRTManagerSchema.getStandbyFlag2();
            } else {
                this.mPayNo = ttLOPRTManagerSchema.getStandbyFlag2();
            }
        }
        return true;
    }

    private boolean checkData() {
        if ((mManaCom == null || mManaCom.equals("")) &&
            (mPayNo == null || mPayNo.equals(""))) {
            mErrors.addOneError("传入的参数有问题。mManaCom和mPayNo不能都为空");
            return false;
        }
        return true;
    }

    private boolean dealData() {
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("ManageCom", "printer");
        TextTag textTag = new TextTag();
        textTag.add("XI_ManageCom",mManaCom);
        if (mManaCom != null && !mManaCom.equals("")) {
            if (!getHospital(xmlexport)) {
                return false;
            }
            if (!getSpecHospital(xmlexport)) {
                return false;
            }
//            xmlexport.outputDocumentToFile("C:\\", "indiHospital_com");
            mResult = new VData();
            mResult.addElement(xmlexport);
            if (mOperate.equals("INSERT")) {
                LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
                tLOPRTManagerDB.setStandbyFlag2(mManaCom);
                tLOPRTManagerDB.setCode("97");
                needPrt = tLOPRTManagerDB.query().size();
                if (needPrt == 0) { //没有数据，进行封装
                    String tLimit = PubFun.getNoLimit(mManaCom);
                    String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                    mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                    mLOPRTManagerSchema.setOtherNo(mManaCom);
                    mLOPRTManagerSchema.setOtherNoType("08"); //暂定为08其他
                    mLOPRTManagerSchema.setCode(PrintPDFManagerBL.CODE_INDIPAYH);
                    mLOPRTManagerSchema.setPrtType("0");
                    mLOPRTManagerSchema.setStateFlag("0");
                    mLOPRTManagerSchema.setReqCom(mManaCom);
                    mLOPRTManagerSchema.setReqOperator("001");
                    mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                    mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                    mLOPRTManagerSchema.setStandbyFlag2(mManaCom);
                }
            }
        } else {
            LJAPayDB tLJAPayDB = new LJAPayDB();
            tLJAPayDB.setPayNo(this.mPayNo);
            if (!tLJAPayDB.getInfo()) {
                CError.buildErr(this, "实收表缺少数据");
                return false;
            }
            String ContNo = tLJAPayDB.getIncomeNo();
            if (!getHospital(xmlexport, ContNo)) {
                return false;
            }
            if (!getSpecHospital(xmlexport, ContNo)) {
                return false;
            }
//            xmlexport.outputDocumentToFile("C:\\", "indiHospital_con");
            mResult = new VData();
            mResult.addElement(xmlexport);
            if (mOperate.equals("INSERT")) {
                LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
                tLOPRTManagerDB.setStandbyFlag2(mPayNo);
                tLOPRTManagerDB.setCode("97");
                needPrt = tLOPRTManagerDB.query().size();
                if (needPrt == 0) { //没有数据，进行封装
                    String tLimit = PubFun.getNoLimit(tLJAPayDB.getManageCom());
                    String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                    mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                    mLOPRTManagerSchema.setOtherNo(tLJAPayDB.getManageCom());
                    mLOPRTManagerSchema.setOtherNoType("08");
                    mLOPRTManagerSchema.setCode(PrintPDFManagerBL.CODE_INDIPAYH);
                    mLOPRTManagerSchema.setPrtType("0");
                    mLOPRTManagerSchema.setStateFlag("0");
                    mLOPRTManagerSchema.setReqCom(tLJAPayDB.getManageCom());
                    mLOPRTManagerSchema.setReqOperator("001");
                    mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                    mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                    mLOPRTManagerSchema.setStandbyFlag2(mPayNo);
                }
            }
        }
        return true;
    }

    /**
     * 返回BOOL值，真为成功，
     */
    private boolean getHospital(XmlExport aXmlexport, String aContNo) {
        //查询ldcode表，获取是否需要打印医院信息
        if (aContNo == null || aContNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "IndiPayComptPrintBL";
            tError.functionName = "getHospital";
            tError.errorMessage = "保单号不能为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String sql = "select count(1) from ldcode where codetype='printriskcode' and codealias='1' and code in (select riskcode from LCPol where ContNo='"
                     + aContNo + "')";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount = tExeSQL.getOneValue(sql);
        //如果查询的结果是0，表示该合同下没有险种需要打印医院信息
        if (tCount.equals("0")) {
            CError tError = new CError();
            tError.moduleName = "IndiPayComptPrintBL";
            tError.functionName = "getHospital";
            tError.errorMessage = "该合同下的险种不需要打印医院信息！";
            this.mErrors.addOneError(tError);
            return false;
        } else {
            sql = "select b.HospitName,b.Address from LDHospital b,LCCont a " +
                  " where associateclass in ('1','2') and b.managecom = substr(a.managecom,1,4) " +
                  " and a.ContNo='" + aContNo + "'";
            SSRS tSSRS = tExeSQL.execSQL(sql);
            if (tSSRS.getMaxRow() < 1) {
                CError tError = new CError();
                tError.moduleName = "IndiPayComptPrintBL";
                tError.functionName = "getHospital";
                tError.errorMessage = "该保单对应的机构下无指定医院！";
                this.mErrors.addOneError(tError);
                return false;
            }
            ListTable tListTable = new ListTable();
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                String[] info = new String[2];
                for (int j = 1; j <= tSSRS.getMaxCol(); j++) {
                    info[j - 1] = tSSRS.GetText(i, j);
                }
                tListTable.add(info);
            }
            String[] title = {"指定医院名称", "联系地址"};
            tListTable.setName("Hospital");
            aXmlexport.addListTable(tListTable, title);
        }
        return true;
    }

    /**
     * 返回BOOL值，真为成功，
     */
    private boolean getSpecHospital(XmlExport aXmlexport, String aContNo) {
        if (aContNo == null || aContNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "IndiPayComptPrintBL";
            tError.functionName = "getHospital";
            tError.errorMessage = "保单号不能为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
//        String sql = "select count(1) from ldcode where codetype='printriskcode' and othersign='1' and code in (select riskcode from LCPol where ContNo='"
//                     + aContNo + "')";
        ExeSQL tExeSQL = new ExeSQL();
//        String tCount = tExeSQL.getOneValue(sql);
//        //如果查询的结果是0，表示该合同下没有险种需要打印医院信息
//        if (tCount.equals("0")) {
//            return true;
//        } else {

        String sql =
                " select b.HospitName ,b.Address from LDHospital b,LCCont a " +
                " where associateclass = '1' and b.managecom = substr(a.managecom,1,4)" +
                " and a.ContNo='" + aContNo + "'";
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() < 1) {
            CError tError = new CError();
            tError.moduleName = "IndiPayComptPrintBL";
            tError.functionName = "getSpecHospital";
            tError.errorMessage = "该机构下没有无推荐医院";
            this.mErrors.addOneError(tError);
            return true;
        }
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] info = new String[2];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++) {
                info[j - 1] = tSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        String[] title = {"推荐医院名称", "联系地址"};
        tListTable.setName("SpecHospital");
        aXmlexport.addListTable(tListTable, title);

        return true;
    }


    /**
     * 返回BOOL值，真为成功，
     */
    private boolean getHospital(XmlExport aXmlexport) {
        String sql = "select b.HospitName,b.Address from LDHospital b " +
                     " where associateclass in ('1','2') and b.managecom like '" +
                     this.mManaCom + "%'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() < 1) {
            CError tError = new CError();
            tError.moduleName = "IndiPayComptPrintBL";
            tError.functionName = "getHospital";
            tError.errorMessage = "该机构没有查询到指定医院";
            this.mErrors.addOneError(tError);
            return false;
        }
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] info = new String[2];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++) {
                info[j - 1] = tSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        String[] title = {"指定医院名称", "联系地址"};
        tListTable.setName("Hospital");
        aXmlexport.addListTable(tListTable, title);
        return true;
    }

    /**
     * 返回BOOL值，真为成功，
     */
    private boolean getSpecHospital(XmlExport aXmlexport) {
        String sql = " select b.HospitName ,b.Address from LDHospital b "
                     + " where associateclass = '1' and b.managecom like '"
                     + this.mManaCom + "%'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() < 1) {
            CError tError = new CError();
            tError.moduleName = "IndiPayComptPrintBL";
            tError.functionName = "getSpecHospital";
            tError.errorMessage = "该机构下没有无推荐医院！";
            this.mErrors.addOneError(tError);
            return true;
        }
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] info = new String[2];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++) {
                info[j - 1] = tSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        String[] title = {"推荐医院名称", "联系地址"};
        tListTable.setName("SpecHospital");
        aXmlexport.addListTable(tListTable, title);
        return true;
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, "INSERT");
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }
}
