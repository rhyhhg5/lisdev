package com.sinosoft.lis.operfee;

import java.io.*;
import java.sql.*;
import java.text.*;
import org.jdom.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpDifGetPrintBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();
    private SSRS mSSRS;
    private LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    private LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema();

    private LJAGetDB mLJAGetDB = new LJAGetDB();
    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    public GrpDifGetPrintBL() {
    }


    //By WangJH

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);

        mLJAGetDB = (LJAGetDB) cInputData.getObjectByObjectName(
                "LJAGetDB", 0);

        if (mLJAGetDB == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean getPrintData() {
        String GrpContNo = mLJAGetDB.getOtherNo();
        System.out.println("GrpContNo=" + GrpContNo);
        String sql = "";
        String sqlone = "";
        mResult.clear();

        //设置公司地址等信息
        if (!setFixedInfo())
        {
            CError.buildErr(this,
                           "机构信息表缺少数据");
           return false;

        }

        //获取团体保单信息
        sqlone = "select * from lcgrpcont where grpcontno='" + GrpContNo + "'";
        System.out.println("sqlone=" + sqlone);
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(sqlone);
        if (tLCGrpContSet.size() == 0) {
           CError.buildErr(this,
                           "集体保单表缺少数据");

           return false;
       }

        tLCGrpContSchema = tLCGrpContSet.get(1).getSchema();


        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例

        String showPaymode = "";
       String showPayintv = "";

       switch (Integer.parseInt(mLJAGetDB.getPayMode())) {
       case 1:
           showPaymode = "现金";
           xmlexport.createDocument("GrpDifGetPrint.vts", "printer"); //最好紧接着就初始化xml文档
           break;
       case 2:
           showPaymode = "现金支票";
           xmlexport.createDocument("GrpDifGetPrint.vts", "printer"); //最好紧接着就初始化xml文档
           break;
       case 3:
           showPaymode = "转帐支票";
           xmlexport.createDocument("GrpDifGetBankPrint.vts", "printer"); //最好紧接着就初始化xml文档
           LDCodeDB tLDCodeDB = new LDCodeDB();
           tLDCodeDB.setCodeType("bank");
           tLDCodeDB.setCode(mLJAGetDB.getBankCode());

           if (!tLDCodeDB.getInfo()) {
               CError.buildErr(this,
                               "公用代码表表中缺少数据");
               return false;
           }
           textTag.add("BankName", tLDCodeDB.getCodeName());

           break;
       case 4:
           showPaymode = "银行转账";
           xmlexport.createDocument("GrpDifGetBankPrint.vts", "printer"); //最好紧接着就初始化xml文档
           LDCodeDB tBankLDCodeDB = new LDCodeDB();
           tBankLDCodeDB.setCodeType("bank");
           tBankLDCodeDB.setCode(mLJAGetDB.getBankCode());

           if (!tBankLDCodeDB.getInfo()) {
               CError.buildErr(this,
                               "公用代码表表中缺少数据");
               return false;
           }
           textTag.add("BankName", tBankLDCodeDB.getCodeName());

           break;
       case 5:
           showPaymode = "内部转帐";
           xmlexport.createDocument("GrpDifGetBankPrint.vts", "printer"); //最好紧接着就初始化xml文档
           break;
       case 6:
           showPaymode = "银行托收";
           break;
       case 7:
           showPaymode = "业务员信用卡";
           break;
       case 9:
           showPaymode = "其他";
           break;
       }
       ;

       System.out.println("showPaymode=" + showPaymode);

        //获取投保人地址
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(GrpContNo);

        if ( !tLCGrpAppntDB.getInfo()){
           CError.buildErr(this,
                           "团单投保人表中缺少数据");
           return false;
       }

        sql = "select * from LCGrpAddress where CustomerNo=(select appntno from LCGrpCont where grpcontno='" +
              GrpContNo + "') AND AddressNo = '" + tLCGrpAppntDB.getAddressNo() + "'";
         System.out.println("sql=" + sql);
         LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
         LCGrpAddressSet tLCGrpAddressSet = tLCGrpAddressDB.executeQuery(sql);
         if (tLCGrpAddressSet.size() == 0) {
           CError.buildErr(this,
                           "团体客户地址表中缺少数据");

           return false;
       }

         tLCGrpAddressSchema = tLCGrpAddressSet.get(tLCGrpAddressSet.size()).
                              getSchema();

        textTag.add("GrpZipCode", tLCGrpAddressSchema.getGrpZipCode());
        textTag.add("GrpAddress", tLCGrpAddressSchema.getGrpAddress());
        textTag.add("LinkMan1", tLCGrpAddressSchema.getLinkMan1());
        textTag.add("AppntNo", tLCGrpContSchema.getAppntNo());
        textTag.add("GrpName", tLCGrpContSchema.getGrpName());
        textTag.add("ActuGetNo", mLJAGetDB.getActuGetNo());
        textTag.add("Drawer", mLJAGetDB.getDrawer());
        textTag.add("DrawerID", mLJAGetDB.getDrawerID());
        textTag.add("SumGetMoney", mLJAGetDB.getSumGetMoney());
        textTag.add("PayDate", CommonBL.decodeDate(mLJAGetDB.getEnterAccDate()));
        textTag.add("BankAccNo", mLJAGetDB.getBankAccNo());

        //textTag.add("PayMode", showPaymode);
        //textTag.add("PayIntv", showPayintv);

        textTag.add("BarCode1", mLJAGetDB.getActuGetNo());
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        System.out.println("ActuGetNo" + mLJAGetDB.getActuGetNo());

        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }
       String[] title = {"投保人", "客户号", "保单号", "当前保费金额", "本次领取保费余额",
                        "领款人", "身份证号"};
        xmlexport.addListTable(getListTable(), title);
        xmlexport.outputDocumentToFile("C:\\", "GrpDifGetPrintBL.xml");

        mResult.addElement(xmlexport);
        return true;
    }

    /**
    * 设置i通知书的公司信息等
     */
    private boolean setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mGlobalInput.ComCode);
        if ( !tLDComDB.getInfo()){
           CError.buildErr(this,
                           "管理结构表中缺少数据");
           return false;
       }
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        textTag.add("Fax", tLDComDB.getFax());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("MakeDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Operator", mGlobalInput.Operator);
        return true;
    }


    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        String[] info = new String[7];

        info[0] = tLCGrpContSchema.getGrpName();
        info[1] = tLCGrpContSchema.getAppntNo();
        info[2] = tLCGrpContSchema.getGrpContNo();
        info[3] = Double.toString(tLCGrpContSchema.getDif());
        info[4] =  Double.toString(mLJAGetDB.getSumGetMoney());
        info[5] =  mLJAGetDB.getDrawer();
        info[6] = mLJAGetDB.getDrawerID();

        tListTable.add(info);

        tListTable.setName("ZT");
        return tListTable;
    }


}
