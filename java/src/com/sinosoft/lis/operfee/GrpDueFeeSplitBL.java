package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.CommonBL;
import java.util.*;
import java.lang.String;
import com.sinosoft.lis.bq.AppAcc;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author HZM
 * @version 1.0
 */

public class GrpDueFeeSplitBL {
	// 错误处理类，每个需要错误处理的类中都放置该类
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private GlobalInput tGI = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate = "";

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();
	
	private Reflections mRef = new Reflections();

	private String mGrpContNo = ""; // 团体保单号

	private String mPrtNo = ""; // 印刷号

	private String mProposalGrpContNo = ""; // 团体投保单号

	private String mPlanCode = ""; // 缴费代码
	
	private String mPayDate = "";//拆分后最后一期约定缴费时间
	
	double totalPay = 0;//一共收费

	private String tNo = ""; //缴费通知书号
	
	private String prtSeqNo = "";
	
	private TransferData mTransferData = new TransferData();

	/** 数据表 保存数据 */
	// 集体保单表
	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

	// 集体险种保单表
	private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();

	private LCGrpPayPlanSet mLCGrpPayPlanSet = new LCGrpPayPlanSet();
	private LCGrpPayPlanSet muLCGrpPayPlanSet = new LCGrpPayPlanSet();
	private LCGrpPayPlanSet miLCGrpPayPlanSet = new LCGrpPayPlanSet();
	
	private LCGrpPayPlanDetailSet mLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet(); 
	private LCGrpPayPlanDetailSet muLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet(); 
	private LCGrpPayPlanDetailSet miLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet(); 
	private LCGrpPayDueSet mLCGrpPayDueSet = new LCGrpPayDueSet();
	private LCGrpPayDueSet muLCGrpPayDueSet = new LCGrpPayDueSet();
	private LCGrpPayDueSet miLCGrpPayDueSet = new LCGrpPayDueSet();

	private LCGrpPayActuSet mLCGrpPayActuSet = new LCGrpPayActuSet();
	private LPEdorEspecialDataSet mLPEdorEspecialDataSet = new LPEdorEspecialDataSet();
	
	
	// 集体险种保单表,接受页面传入的参数
	private LCGrpPayPlanSet tLCGrpPayPlanSet = new LCGrpPayPlanSet();

	// 应收总表
	private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

	private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();

	// 应收总表集
	private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();

	private LJSPayGrpSet mYelLJSPayGrpSet = new LJSPayGrpSet();

	// 应收总表集备份表
	private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();

	private LJSPayGrpBSet mYelLJSPayGrpBSet = new LJSPayGrpBSet();

	// 应收总表交费日期
	private String strNewLJSPayDate;

	// 应收总表最早交费日期
	private String strNewLJSStartDate;
	
	// 团单生效日
	private String mLastPayToDate="";

	// 团单交至日
	private String mCurPayToDate="";

	// 应收总表费额
	private double totalsumPay = 0;
	
	

	private LCUrgeVerifyLogSchema mLCUrgeVerifyLogSchema = new LCUrgeVerifyLogSchema();

	// 缴费通知书
	private LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();

	private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

	private LOPRTManagerSubSet mLOPRTManagerSubSet = new LOPRTManagerSubSet();

	private MMap map = new MMap();
	
	private MMap mapAcc = new MMap();

	private String mOpertor = "INSERT";

	private String mOperateFlag = "1"; // 1 : 个案催收，2：批量催收

	private String mserNo = ""; // 批次号

	public GrpDueFeeSplitBL() {
	}

	public static void main(String[] args) {

		GlobalInput tGt = new GlobalInput();
		tGt.ComCode = "86";
		tGt.Operator = "wuser";
		tGt.ManageCom = "86";

		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		tLCGrpContSchema.setGrpContNo("0000019706");

		TransferData tempTransferData = new TransferData();
		tempTransferData.setNameAndValue("StartDate", "2005-01-01");
		tempTransferData.setNameAndValue("EndDate", "2008-8-13");
		tempTransferData.setNameAndValue("ManageCom", "86");
		tempTransferData.setNameAndValue("LoadFlag", "WMD");

		VData tVData = new VData();
		tVData.add(tLCGrpContSchema);
		tVData.add(tGt);
		tVData.add(tempTransferData);

	

		GrpDueFeePlanBL GrpDueFeeBL1 = new GrpDueFeePlanBL();
		GrpDueFeeBL1.submitData(tVData, "INSERT");
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData mInputData) {

		tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
		tLCGrpPayPlanSet = ((LCGrpPayPlanSet) mInputData.getObjectByObjectName("LCGrpPayPlanSet", 0));
		mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		if (tGI == null || tLCGrpPayPlanSet == null || mTransferData == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrpDueFeeBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的数据，请您确认!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mPrtNo = (String) mTransferData.getValueByName("PrtNo");
		mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
		mProposalGrpContNo = (String) mTransferData.getValueByName("ProposalGrpContNo");
		mPlanCode = (String) mTransferData.getValueByName("PlanCode");
		mPayDate = (String) mTransferData.getValueByName("PayDate");
		if (mPrtNo == null || mPrtNo.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入印刷号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		if (mGrpContNo == null || mGrpContNo.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入保单号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		if (mProposalGrpContNo == null || mProposalGrpContNo.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入投保号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		if (mPlanCode == null || mPlanCode.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入约定缴费代码号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 按照GrpContNo查询是否有保全案件正在处理
	 * 
	 * @param aPolNo
	 *            String
	 * @return boolean
	 */
	public boolean checkEdorItemState(String aGrpContNo) {
		String sql = "select a.* " + "from LPGrpEdorItem a, LPEdorApp b " + "where a.EdorAcceptNo = b.EdorAcceptNo " + "   and a.grpcontno = '" + aGrpContNo + "' " + "   and b.EdorState != '0' ";
		System.out.println(sql);
		LPEdorItemSet set = new LPEdorItemDB().executeQuery(sql);
		if (set.size() > 0) {
			String edorInfo = "";
			for (int i = 1; i <= set.size(); i++) {
				edorInfo += set.get(i).getEdorNo() + " " + set.get(i).getEdorType() + ", ";
			}
			mErrors.addOneError("保单有未结案的保全项目:" + edorInfo);
			return false;
		}
		return true;
	}

	/**
	 * 按照GrpContNo查询是否有理赔案件正在处理
	 * 
	 * @param aPolNo
	 *            String
	 * @return boolean
	 */
	public boolean checkClaimState(String aGrpContNo) {
		boolean tClaimFlag = true;
		String tSQL = "SELECT 1 FROM llcasepolicy a,llcase b WHERE a.caseno=b.caseno " + "AND b.rgtstate NOT IN ('11','12','14')" // 11
				// 通知状态
				// 12
				// 给付状态
				// 14
				// 撤件状态
				+ " AND a.grpcontno='" + aGrpContNo + "'";
		ExeSQL exesql = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = exesql.execSQL(tSQL);
		if (tSSRS.getMaxRow() > 0) {
			tClaimFlag = false;
		}
		return tClaimFlag;
	}

	/**
	 * 按照GrpContNo查询是否已经被催收
	 * 
	 * @param aPolNo
	 *            String
	 * @return boolean
	 */
	public boolean checkLjsState(String aGrpContNo) {
		boolean tClaimFlag = true;
		String tSQL = "SELECT 1 FROM ljspayb a WHERE   a.dealstate IN ('0','4','5')" + " AND a.otherno='" + aGrpContNo + "' and a.othernotype='1' ";
		ExeSQL exesql = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = exesql.execSQL(tSQL);
		if (tSSRS.getMaxRow() > 0) {
			tClaimFlag = false;
		}
		return tClaimFlag;
	}

	/**
	 * 杨红于2005-07-19添加数据校验，判断页面中查询出的信息是否已被催收
	 * 
	 * @return boolean
	 */
	public boolean checkData() {
		System.out.println("约定缴费续期抽档校验");

		// 下面校验lcgrppayplan是否有符合条件的记录
		StringBuffer tSBql = new StringBuffer(128);
		tSBql.append("select * from lcgrppayplan where  prtno='").append(mPrtNo).append("' and plancode='").append(mPlanCode).append("'");
		String GrpPayPlanStr = tSBql.toString();
		System.out.println(GrpPayPlanStr);
		LCGrpPayPlanDB tLCGrpPayPlanDB = new LCGrpPayPlanDB();
		mLCGrpPayPlanSet = tLCGrpPayPlanDB.executeQuery(GrpPayPlanStr);
		if (tLCGrpPayPlanDB.mErrors.needDealError()) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下的集体险种查询失败！");
			return false;
		}
		if (mLCGrpPayPlanSet.size() == 0) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下没有符合条件的集体险种！");
			return false;
		}
		
		
		// 获取lcgrppayplandetail是否有符合条件的记录
		tSBql = new StringBuffer(128);
		tSBql.append("select * from lcgrppayplandetail where state='2' and prtno='").append(mPrtNo).append("' and plancode='").append(mPlanCode).append("'");
		String GrpPayPlanDeStr = tSBql.toString();
		System.out.println(GrpPayPlanDeStr);
		LCGrpPayPlanDetailDB tLCGrpPayPlanDetailDB = new LCGrpPayPlanDetailDB();
		mLCGrpPayPlanDetailSet = tLCGrpPayPlanDetailDB.executeQuery(GrpPayPlanDeStr);
		
		tSBql = new StringBuffer(128);
		tSBql.append("select * from lcgrppaydue where confstate='02' and prtno='").append(mPrtNo).append("' and plancode='").append(mPlanCode).append("'");
		String GrpPayPlanDueStr = tSBql.toString();
		System.out.println(GrpPayPlanDueStr);
		LCGrpPayDueDB tLCGrpPayDueDB = new LCGrpPayDueDB();
		mLCGrpPayDueSet = tLCGrpPayDueDB.executeQuery(GrpPayPlanDueStr);
		
		if (tLCGrpPayPlanDetailDB.mErrors.needDealError()) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下的集体险种查询失败！");
			return false;
		}
		if (mLCGrpPayPlanDetailSet.size() == 0) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下没有符合催收条件的集体险种！");
			return false;
		}

//		if (!checkEdorItemState(mGrpContNo)) {
//			CError.buildErr(this, "团单号:" + mGrpContNo + "催收错误:正在做保全!");
//			return false;
//		}
//		if (!checkClaimState(mGrpContNo)) {
//			CError.buildErr(this, "团单号:" + mGrpContNo + "催收错误:正在做理赔!");
//			return false;
//		}
		if (!checkLjsState(mGrpContNo)) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "催收错误:该团单已经尚未缴费核销的催收!");
			return false;
		}

		tSBql = new StringBuffer(128);
		tSBql.append("select * from LCGrpPol where GrpContNo='").append(mGrpContNo).append("'  and AppFlag='1' and (StateFlag is null or StateFlag = '1') ");
		String GrpPolStr = tSBql.toString();
		LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
		System.out.println(GrpPolStr);
		mLCGrpPolSet = tLCGrpPolDB.executeQuery(GrpPolStr);
		if (tLCGrpPolDB.mErrors.needDealError()) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下的集体险种查询失败！");
			return false;
		}
		if (mLCGrpPolSet.size() == 0) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下没有符合条件的集体险种！");
			return false;
		}
		tSBql = new StringBuffer(128);
		tSBql.append("select max(paytodate) from lcgrppayplandetail where state='2' and  prtno='").append(mPrtNo).append("'  and plancode='").append(mPlanCode).append("'");
		String tMaxPayDate = new ExeSQL().getOneValue(tSBql.toString()); 
		FDate tFDate = new FDate();
		if (tFDate.getDate(tMaxPayDate).after(tFDate.getDate(
                mPayDate))||tFDate.getDate(tMaxPayDate).equals(tFDate.getDate(
                        mPayDate))) {
            mErrors.addOneError(new CError("录入的最后一期缴费日期早于或等于承保时保单约定的最后一期缴费日期，请录入晚于承保约定的最后一期缴费时间" +
            		tMaxPayDate + "的日期！"));
            return false;
        }

		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mGrpContNo);
		if (!tLCGrpContDB.getInfo()) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下的集体保单查询失败！");
			return false;
		}
		mLCGrpContSchema=tLCGrpContDB.getSchema();
		if (tFDate.getDate(mPayDate).after(tFDate.getDate(
				mLCGrpContSchema.getCInValiDate()))) {
            mErrors.addOneError(new CError("录入的最后一期缴费日期晚于保单终止日期，请录入早于保单终止日" +
            		mLCGrpContSchema.getCInValiDate() + "的日期！"));
            return false;
        }
		return true;
	}

	public double getCalPrem(LCGrpPayPlanDetailSchema tLCGrpPayPlanDetailSchema ,double actu){
		String allprem= new ExeSQL().getOneValue("select prem from LCGrpPayPlan where prtno='"+mPrtNo+"' and plancode='"+tLCGrpPayPlanDetailSchema.getPlanCode()+"' and contplancode='"+tLCGrpPayPlanDetailSchema.getContPlanCode()+"'");
		double tallprem=Double.parseDouble(allprem);
		if(tallprem==0){
			return 0;
		}
		else
		return Arith.round(actu*tLCGrpPayPlanDetailSchema.getPrem()/Double.parseDouble(allprem),2);
	}
	
	
	// 为更新数据库准备数据
	public boolean prepareData() {

		map.put(muLCGrpPayPlanSet, "UPDATE");
		map.put(muLCGrpPayPlanDetailSet, "UPDATE");
		map.put(muLCGrpPayDueSet, "UPDATE");
		map.put(miLCGrpPayPlanSet, "INSERT");
		map.put(miLCGrpPayPlanDetailSet, "INSERT");
		map.put(miLCGrpPayDueSet, "INSERT");
		map.put(mLPEdorEspecialDataSet, "INSERT");
		
		//updateDealState();
		mInputData.add(map);
		return true;
	}

	// 传输数据的公共方法
	public boolean submitData(VData cInputData, String cOperate) {

		System.out.println("GrpDueFeeBL Begin......");
		this.mOperate = cOperate;

		if (!getInputData(cInputData)) {
			return false;
		}
		//校验保单之前存储是否正确同时校验页面录入的最后一次缴费日期是否大于契约录入的最后一次约定缴费日期
		if (!checkData()) {
			return false;
		}


		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		if (!prepareData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(mInputData, "") == false) {


			this.mErrors.addOneError("PubSubmit:处理数据库失败!");

			return false;
		}


		return true;
	}
	
	// 更新旧的期次的金额
	public boolean dealUpdatePrem() {
		for (int i = 1; i <= tLCGrpPayPlanSet.size(); i++) {
			LCGrpPayPlanSchema tLCGrpPayPlanSchema=new LCGrpPayPlanSchema();
			mRef.transFields(tLCGrpPayPlanSchema, tLCGrpPayPlanSet.get(i));
//			对录入的金额四舍五入
			double tResidualPrem = getResidualPrem(tLCGrpPayPlanSchema.getPrtNo(),tLCGrpPayPlanSchema.getPlanCode(),tLCGrpPayPlanSchema.getContPlanCode());
			if(tResidualPrem==-1){
				
				this.mErrors.addOneError("获取之前约定缴费剩余情况时出现错误");

				return false;
				
			}
			//20150729若拆分之前某一个计划的保费为0，则允许录入为0的情况。
			String strSQL="select prem from LCGrpPayPlan a where prtno='"+tLCGrpPayPlanSchema.getPrtNo()+"' "
						+" and contplancode='"+tLCGrpPayPlanSchema.getContPlanCode()+"' and plancode='"+tLCGrpPayPlanSchema.getPlanCode()+"'"
						+" and exists (select 1 from LCGrpPayPlan where prtno=a.prtno and plancode=a.plancode and prem>0)";
			String oldMoney=new ExeSQL().getOneValue(strSQL);
			double oldPrem=Double.parseDouble(oldMoney);
			if(tLCGrpPayPlanSchema.getPrem()==0 && oldPrem > 0){
				
				this.mErrors.addOneError("录入的本次缴费金额不能为0");

				return false;
			}
			double tDuePrem = tLCGrpPayPlanSchema.getPrem();
			double tPrePrem = tLCGrpPayPlanSchema.getPrem() + tResidualPrem;
						;
			tLCGrpPayPlanSchema.setPrem(Arith.round(tPrePrem,2));
		
			tLCGrpPayPlanSchema.setOperator(tGI.Operator);
			tLCGrpPayPlanSchema.setMakeDate(CurrentDate);
			tLCGrpPayPlanSchema.setMakeTime(CurrentTime);
			tLCGrpPayPlanSchema.setModifyDate(CurrentDate);
			tLCGrpPayPlanSchema.setModifyTime(CurrentTime);
			muLCGrpPayPlanSet.add(tLCGrpPayPlanSchema);
		

			for (int j = 1; j <= mLCGrpPayPlanDetailSet.size(); j++) {
				if(tLCGrpPayPlanSchema.getContPlanCode().equals(mLCGrpPayPlanDetailSet.get(j).getContPlanCode())){
					LCGrpPayPlanDetailSchema tLCGrpPayPlanDetailSchema=new LCGrpPayPlanDetailSchema();
					mRef.transFields(tLCGrpPayPlanDetailSchema, mLCGrpPayPlanDetailSet.get(j));
					
					tLCGrpPayPlanDetailSchema.setPrem(getCalPrem(mLCGrpPayPlanDetailSet.get(j),tPrePrem));
					tLCGrpPayPlanDetailSchema.setOperator(tGI.Operator);
					tLCGrpPayPlanDetailSchema.setMakeDate(CurrentDate);
					tLCGrpPayPlanDetailSchema.setMakeTime(CurrentTime);
					tLCGrpPayPlanDetailSchema.setModifyDate(CurrentDate);
					tLCGrpPayPlanDetailSchema.setModifyTime(CurrentTime);
					
					tLCGrpPayPlanDetailSchema.setMoneyNoTax(null);
					tLCGrpPayPlanDetailSchema.setMoneyTax(null);
					tLCGrpPayPlanDetailSchema.setTaxRate(null);
					tLCGrpPayPlanDetailSchema.setBusiType(null);
					muLCGrpPayPlanDetailSet.add(tLCGrpPayPlanDetailSchema);
					
				}
			}
			
			for (int j = 1; j <= mLCGrpPayDueSet.size(); j++) {
				if(tLCGrpPayPlanSchema.getContPlanCode().equals(mLCGrpPayDueSet.get(j).getContPlanCode())){
					LCGrpPayDueSchema tLCGrpPayDueSchema=new LCGrpPayDueSchema();
					mRef.transFields(tLCGrpPayDueSchema, mLCGrpPayDueSet.get(j));
					
					tLCGrpPayDueSchema.setPrem(Arith.round(tDuePrem,2));
					tLCGrpPayDueSchema.setOperator(tGI.Operator);
					tLCGrpPayDueSchema.setMakeDate(CurrentDate);
					tLCGrpPayDueSchema.setMakeTime(CurrentTime);
					tLCGrpPayDueSchema.setModifyDate(CurrentDate);
					tLCGrpPayDueSchema.setModifyTime(CurrentTime);
					muLCGrpPayDueSet.add(tLCGrpPayDueSchema);
					
				}
			}

		}
		return true;
	}

	
	public boolean dealInsertData() {

		for (int i = 1; i <= tLCGrpPayPlanSet.size(); i++) {
			LCGrpPayPlanSchema tLCGrpPayPlanSchema=new LCGrpPayPlanSchema();
			LCGrpPayPlanSchema ttLCGrpPayPlanSchema=new LCGrpPayPlanSchema();
			mRef.transFields(tLCGrpPayPlanSchema, tLCGrpPayPlanSet.get(i));
//			对录入的金额四舍五入
			double tResidualPrem = getResidualPrem(tLCGrpPayPlanSchema.getPrtNo(),tLCGrpPayPlanSchema.getPlanCode(),tLCGrpPayPlanSchema.getContPlanCode());
			if(tResidualPrem==-1){
				
				this.mErrors.addOneError("获取之前约定缴费剩余情况时出现错误");

				return false;
				
			}
			for (int j = 1; j <= mLCGrpPayPlanSet.size(); j++) {
				if(tLCGrpPayPlanSchema.getContPlanCode().equals(mLCGrpPayPlanSet.get(j).getContPlanCode())){
					
					mRef.transFields(ttLCGrpPayPlanSchema, mLCGrpPayPlanSet.get(j));
					
					
				}
			}
			double tPrePrem = ttLCGrpPayPlanSchema.getPrem()-tLCGrpPayPlanSchema.getPrem() - tResidualPrem;
						
			if(tPrePrem<=0){
				if(tPrePrem==0 && ttLCGrpPayPlanSchema.getPrem() == 0){
					System.out.println("原计划保费为0 ，可拆分");
				}else{
					this.mErrors.addOneError(ttLCGrpPayPlanSchema.getContPlanCode()+"保障计划录入的本次缴费金额与之前溢缴金额的总和" +
							"大于或等于契约时约定的最后一次缴费金额，约定最后一期缴费金额为"+ttLCGrpPayPlanSchema.getPrem()+",之前溢缴金额为"+tResidualPrem+",如要做拆分需要录入小于二者之和的金额");
	
					return false;
				}				
			}
			int temp = Integer.parseInt(tLCGrpPayPlanSchema.getPlanCode());
			
			String tPlanCode =  String.valueOf(temp+1);
			
			tLCGrpPayPlanSchema.setPrem(Arith.round(tPrePrem,2));
			tLCGrpPayPlanSchema.setPlanCode(tPlanCode);
			tLCGrpPayPlanSchema.setPaytoDate(mPayDate);
			tLCGrpPayPlanSchema.setOperator(tGI.Operator);
			tLCGrpPayPlanSchema.setMakeDate(CurrentDate);
			tLCGrpPayPlanSchema.setMakeTime(CurrentTime);
			tLCGrpPayPlanSchema.setModifyDate(CurrentDate);
			tLCGrpPayPlanSchema.setModifyTime(CurrentTime);
			miLCGrpPayPlanSet.add(tLCGrpPayPlanSchema);
		

			for (int j = 1; j <= mLCGrpPayPlanDetailSet.size(); j++) {
				if(tLCGrpPayPlanSchema.getContPlanCode().equals(mLCGrpPayPlanDetailSet.get(j).getContPlanCode())){
					LCGrpPayPlanDetailSchema tLCGrpPayPlanDetailSchema=new LCGrpPayPlanDetailSchema();
					mRef.transFields(tLCGrpPayPlanDetailSchema, mLCGrpPayPlanDetailSet.get(j));
					
					tLCGrpPayPlanDetailSchema.setPrem(getCalPrem(mLCGrpPayPlanDetailSet.get(j),tPrePrem));
					tLCGrpPayPlanDetailSchema.setPlanCode(tPlanCode);
					tLCGrpPayPlanDetailSchema.setPaytoDate(mPayDate);
					tLCGrpPayPlanDetailSchema.setOperator(tGI.Operator);
					tLCGrpPayPlanDetailSchema.setMakeDate(CurrentDate);
					tLCGrpPayPlanDetailSchema.setMakeTime(CurrentTime);
					tLCGrpPayPlanDetailSchema.setModifyDate(CurrentDate);
					tLCGrpPayPlanDetailSchema.setModifyTime(CurrentTime);
					
					tLCGrpPayPlanDetailSchema.setMoneyNoTax(null);
					tLCGrpPayPlanDetailSchema.setMoneyTax(null);
					tLCGrpPayPlanDetailSchema.setTaxRate(null);
					tLCGrpPayPlanDetailSchema.setBusiType(null);
					miLCGrpPayPlanDetailSet.add(tLCGrpPayPlanDetailSchema);
					
				}
			}
			
			for (int j = 1; j <= mLCGrpPayDueSet.size(); j++) {
				if(tLCGrpPayPlanSchema.getContPlanCode().equals(mLCGrpPayDueSet.get(j).getContPlanCode())){
					LCGrpPayDueSchema tLCGrpPayDueSchema=new LCGrpPayDueSchema();
					mRef.transFields(tLCGrpPayDueSchema, mLCGrpPayDueSet.get(j));
					
					tLCGrpPayDueSchema.setPrem(Arith.round(tPrePrem,2));
					tLCGrpPayDueSchema.setPlanCode(tPlanCode);
					tLCGrpPayDueSchema.setPaytoDate(mPayDate);
					tLCGrpPayDueSchema.setOperator(tGI.Operator);
					tLCGrpPayDueSchema.setMakeDate(CurrentDate);
					tLCGrpPayDueSchema.setMakeTime(CurrentTime);
					tLCGrpPayDueSchema.setModifyDate(CurrentDate);
					tLCGrpPayDueSchema.setModifyTime(CurrentTime);
					miLCGrpPayDueSet.add(tLCGrpPayDueSchema);
					
				}
			}

		}
		return true;
	}
	/**
	 * Yangh于2005-07-19创建新的dealData的处理逻辑
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

	 

       
		//更新当期金额
		if(!dealUpdatePrem()){
			
			return false;
		}
		
		//插入新的一期数据
		if(!dealInsertData()){
			return false;
		}
	
		//存储到记录表
		if(!dealTrace()){
			
			this.mErrors.addOneError("保存拆分轨迹时出现错误");

			return false;
			
		}
		

		
		

		

		return true;
	}

	

	

	/**
	 * 得到投保人帐户余额
	 * 
	 * @param appntNo
	 *            String：投保人
	 * @return double：帐户余额
	 */
	private double getAccBala(String appntNo) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select accGetMoney ").append("from LCAppAcc ").append("where customerNo = '").append(appntNo).append("' ");
		String accGetBala = new ExeSQL().getOneValue(sql.toString());
		if (accGetBala.equals("") || accGetBala.equals("null")) {
			return Double.MIN_VALUE;
		}
		return Double.parseDouble(accGetBala);
	}

	/**
	 * 获取最后一期缴费之前尚有剩余的金额
	 * @param tPrtno
	 * @param tPlanCode
	 * @param tContPlanCode
	 * @return
	 */
	private double getResidualPrem(String tPrtno,String tPlanCode,String tContPlanCode) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select nvl(sum(prem),0) ").append("from lcgrppaydue ").append("where prtno = '").append(tPrtno).
		append("' and plancode = '").append(tPlanCode).append("' and contplancode = '").append(tContPlanCode).append("' ").
		append(" and confstate = '01' ");
		String ResidualPrem = new ExeSQL().getOneValue(sql.toString());
		if (ResidualPrem.equals("") || ResidualPrem.equals("null")) {
			return -1;
		}
		return Double.parseDouble(ResidualPrem);
	}

	
	public boolean dealTrace(){
		

		for (int j = 1; j <= mLCGrpPayPlanSet.size(); j++) {
			
			LCGrpPayPlanSchema tLCGrpPayPlanSchema=new LCGrpPayPlanSchema();
			mRef.transFields(tLCGrpPayPlanSchema, mLCGrpPayPlanSet.get(j));
			String strSQL="select int(edortype)+1 from LPedorEspecialData"
					+ " where EdorAcceptNo='" + mGrpContNo + "'"
					+ " and EdorNo='" + mPrtNo + "' and DetailType='SplitDueFee' "
					+ " order by int(edortype) desc "
					+ " with ur";
			ExeSQL mExeSQL=new ExeSQL();
			String SplitCount=mExeSQL.getOneValue(strSQL);
			if(null==SplitCount || ""==SplitCount){
				SplitCount="1";
			}
			LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
			tLPEdorEspecialDataSchema.setEdorAcceptNo(mGrpContNo); // 通知书号
			tLPEdorEspecialDataSchema.setEdorNo(mPrtNo);
//			tLPEdorEspecialDataSchema.setEdorType("01");
			tLPEdorEspecialDataSchema.setEdorType(SplitCount);	//20141216不限制拆分次数，累计此字段
			tLPEdorEspecialDataSchema.setDetailType("SplitDueFee");
			tLPEdorEspecialDataSchema.setState(tLCGrpPayPlanSchema.getPlanCode());
			tLPEdorEspecialDataSchema.setEdorValue(String.valueOf(tLCGrpPayPlanSchema.getPrem()));
			tLPEdorEspecialDataSchema.setPolNo(tLCGrpPayPlanSchema.getContPlanCode());
			//20141216添加拆分时间记录
			LPEdorEspecialDataSchema tSplitTrace = new LPEdorEspecialDataSchema();
			tSplitTrace.setEdorAcceptNo(mGrpContNo); // 通知书号
			tSplitTrace.setEdorNo(mPrtNo);
			tSplitTrace.setEdorType(SplitCount);	//20141216不限制拆分次数，累计此字段
			tSplitTrace.setDetailType("MakeTime");
			tSplitTrace.setState(tLCGrpPayPlanSchema.getPlanCode());
			tSplitTrace.setEdorValue(PubFun.getCurrentDate()+"  "+PubFun.getCurrentTime());
			tSplitTrace.setPolNo(tLCGrpPayPlanSchema.getContPlanCode());
			
			mLPEdorEspecialDataSet.add(tSplitTrace);
			mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema);
		}
		
		
			

		return true;
	
	}
		


	



}
