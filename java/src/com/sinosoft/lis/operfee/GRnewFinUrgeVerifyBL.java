package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
//import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.xb.GRnewGrpContSignBL;
import com.sinosoft.lis.xb.PRnewContSignBL;
import com.sinosoft.lis.xb.XBConst;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.pubfun.*;

import java.math.BigDecimal;
import java.text.*;
import java.util.Date;
import com.sinosoft.lis.taskservice.NextFeeHastenTask;
import com.sinosoft.task.TaskPhoneFinishBL;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GRnewFinUrgeVerifyBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();;

    /** 数据操作字符串 */
    private String mOperate;
    private GlobalInput tGI = new GlobalInput();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    private MMap mAccMap = new MMap();

    private TransferData mTransferData;
    private LCGrpContSchema mLCGrpContSchema;
    //暂收费表
    private LJTempFeeSchema mLJTempFeeSchema = new LJTempFeeSchema();
    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
    private LJTempFeeSet mLJTempFeeNewSet = new LJTempFeeSet();
    //暂收费分类表
    private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
    private LJTempFeeClassSet mLJTempFeeClassNewSet = new LJTempFeeClassSet();
    private String mPayDate = ""; //缴费日期
    private String mEnterAccDate = ""; //到帐日期
    private String mConfDate = ""; //财务确认日期
    private String mRnewState = null; //
    

    //实收个人交费表
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();

    //应收集体交费表

    private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();

    //实收集体交费表
    private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();
    private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();

    //应收总表
    private LJSPayBL mLJSPayBL = new LJSPayBL();
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();
    //实收总表
    private LJAPayBL mLJAPayBL = new LJAPayBL();
    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();
    
    //个人险种表
    private LCPolSet mLCPolNewSet = new LCPolSet();
    private MMap map = new MMap();
    
    //集体保单表
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private LCGrpPolSet mLCGrpPolNewSet = new LCGrpPolSet();    
    
    //保费项表
    private LCPremSet mLCPremNewSet = new LCPremSet();
    

    //保险责任表LCDuty
    private LCDutySet mLCDutyNewSet = new LCDutySet();
    
    private String mOperateFlag = "1"; // 1 : 个案核销，2：批量核销
    private String mserNo = null; //流水号
    private String mSqlLjsPayPersonB = "";
    private String mSqlLjsPayPerson = "";
    private String mGetNoticeNo = "";
    private boolean mUrgeFlag = true; // 是否进行核销操作
    private boolean mPayMode = true; // 交费方式，默认为现金模式
    

    //业务处理相关变量
    public GRnewFinUrgeVerifyBL() {

    }

    public static void main(String[] args) {
        GlobalInput mGI = new GlobalInput();
        mGI.Operator = "pa0001";
        mGI.ComCode = "86";
        mGI.ManageCom = "86";

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("00113761000001");

        GRnewFinUrgeVerifyBL tGRnewFinUrgeVerifyBL = new GRnewFinUrgeVerifyBL();
        VData tVData = new VData();
        tVData.add(tLCGrpContSchema);
        TransferData  tTransferData = new TransferData();
        tTransferData.setNameAndValue("GetNoticeNo","31000629469");
        tVData.add(tTransferData);
        tVData.add(mGI);

        tGRnewFinUrgeVerifyBL.submitData(tVData, "VERIFY");
    }
    
    
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {

        System.out.println("-----------in getInputData------------");
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
        mGetNoticeNo =(String) mTransferData.getValueByName("GetNoticeNo");
        mLCGrpContSchema = (LCGrpContSchema) mInputData.getObjectByObjectName("LCGrpContSchema", 0);

        if (mLCGrpContSchema == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GRnewFinUrgeVerifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        

        String tGrpContNo = mLCGrpContSchema.getGrpContNo();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(tGrpContNo);
        // 校验合同表，如果查询失败，则保单已经解约，或者其他原因，所以此保单不能交费
        if (!tLCGrpContDB.getInfo()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GRnewFinUrgeVerifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "团单保单信息不存在，请查询!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());

        double sumDueMoney = 0.0;
        // 不能仅仅根据合同号＆类型进行查询，还需要对应收的顺序做排序，以便核销第一笔数据
        // 同时需要添加交至日的条件，应为如果未到交至日，则需要将交费放入余额
        StringBuffer tSBql = new StringBuffer(128);
        tSBql.append("select * from LJSPay where OtherNoType = '1' and GetNoticeNO = '");
        tSBql.append(mGetNoticeNo);
        tSBql.append("'");
        
        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(tSBql.toString());

        if (tLJSPaySet == null || tLJSPaySet.size() <= 0) {
            mUrgeFlag = false;
            CError.buildErr(this, "系统不存在满足条件的应收记录!");
            return false;

        }

        sumDueMoney = tLJSPaySet.get(1).getSumDuePayMoney();

        mLJSPayBL.setSchema(tLJSPaySet.get(1).getSchema());

        double tempMoney = 0.0; // 交费的金额
        boolean tNeedPay = true; // 是否需要交费
        // 如果应交总额为0，则不需要有暂交费信息（即不需要预交续保保费），且存在应收记录
        if (sumDueMoney <= 0.0 && mUrgeFlag) {
            // 设置交费标志为可不交费
            tNeedPay = false;
        }

        // 查询此保单下的所有未核销的暂收交费记录
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tSBql = new StringBuffer(128);
        tSBql.append("select * from ljtempfee where TempFeeType='2' and TempFeeNo='");
        tSBql.append(mLJSPayBL.getGetNoticeNo());
        tSBql.append("' and (ConfFlag='0' or ConfFlag is null) and EnterAccDate is not null");
        System.out.println(tSBql.toString());
        mLJTempFeeSet = tLJTempFeeDB.executeQuery(tSBql.toString());
        if (mLJTempFeeSet == null || mLJTempFeeSet.size() == 0) {
            // 只有当需要交费，且没有交费记录的时候才报错
            if (tNeedPay) {
                CError.buildErr(this, "系统不存在满足条件的预交费记录!");
                return false;
            }

            //需要财务缴费，将所需的财务日期设为核销当天
            mPayDate = CurrentDate;
            mEnterAccDate = CurrentDate;
        } else {
        	mLJTempFeeSchema = mLJTempFeeSet.get(1);
            // 查询此保单下的所有未核销的暂收交费记录
            LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
            tSBql = new StringBuffer(128);
            tSBql.append("select * from ljtempfeeclass where TempFeeNo in (select TempFeeNo from ljtempfee where TempFeeType='2' and TempFeeNo='");
            tSBql.append(mLJSPayBL.getGetNoticeNo());
            tSBql.append("' and (ConfFlag='0' or ConfFlag is null) and EnterAccDate is not null)");
            mLJTempFeeClassSet = tLJTempFeeClassDB.executeQuery(tSBql.toString());
            if (mLJTempFeeClassSet == null) {
                CError.buildErr(this, "系统不存在满足条件的预交费记录!");
                return false;
            }
            // 判定交费方式
            if (mLJTempFeeClassSet.get(1).getPayMode().equals("4")) {
                // 设置交费方式为转账模式
                mPayMode = false;
            }
            //取得缴费日期
            mPayDate = mLJTempFeeSet.get(1).getPayDate();
            mEnterAccDate = mLJTempFeeSet.get(1).getEnterAccDate();
            tempMoney = 0.0;
            for (int i = 1; i <= mLJTempFeeSet.size(); i++) {
                // 该保单下实际交费金额
                tempMoney += mLJTempFeeSet.get(i).getPayMoney();

            }
        }
        mConfDate = CurrentDate;

        if (tempMoney < sumDueMoney) {
            mUrgeFlag = false;
            return true;
        }

        return true;
    }

    /*
     *判断保单是否有理赔:如果有返回false,没有返回true;
     *@return boolean
     */

    private boolean checkLPState()
    {
  	  String tGrpContNo=mLCGrpContSchema.getGrpContNo();
  	  ExeSQL tExeSQL = new ExeSQL();
  	  String getContNoSQL = "select contno from lccont where grpcontno = '"+tGrpContNo+"'";
  	  SSRS tSSRS2 = tExeSQL.execSQL(getContNoSQL);
  	  if(tSSRS2.getMaxRow()>0){
  		  for(int i = 1;i<tSSRS2.getMaxRow();i++){
  			  String tContNo = tSSRS2.GetText(i, 1);
  			  if(tContNo!=null && !tContNo.equals(""))
  			  {
  				  //校验保单下被保人如果正在理赔不能续期抽档
  				  String sql ="SELECT a.CusTomerName FROM LLCase a,LCPol b "
  					  +"WHERE a.CusTomerNo = b.InsuredNo "
  					  +"AND a.RGTState NOT IN ('11','12','14') AND b.contno='" + tContNo + "' ";
  				  System.out.println(sql);
  				  
  				  SSRS tSSRS = tExeSQL.execSQL(sql);
  				  if(tSSRS.getMaxRow()>0)
  				  {  
  					  this.mErrors.addOneError("被保人"+tSSRS.GetText(1, 1)+"有未结案的理赔。");//
  					  return false;
  				  }
  				  
  			  }
  		  }
  	  }
        return true;
    }
    
    
    
    
    
    
    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("After getInputData");

        //进行业务处理
        if (!dealData()) {
            return false;
        }
        if(!dealPhoneHasten())
        {
            return false;
        }
        System.out.println("After dealData！");
        if (!prepareData()) {
            return false;
        }
        
        
        if(!dealGRnewSign())
        {
            return false;
        }
        
        
        
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(mInputData, "") == false) {
            CError.buildErr(this, "系统处理数据异常！");
            
            //添加数据到催收核销日志表
            if (!dealUrgeLog("2", "插入LJAPay等表的数据失败", "UPDATE")) {
                return false;
            }

            return false;
        }

        //添加数据到催收核销日志表,非批次核销
        if (!dealUrgeLog("3", "", "UPDATE")) {
            return false;
        }

        return true;
    }

    
    /**
     * 处理保单续保确认
     * @return boolean
     */
    private boolean dealGRnewSign()
    {
        if(!needPRnewOperate())
        {
            return true;
        }

        if(mRnewState.equals(XBConst.RNEWSTATE_UNCONFIRM))
        {
            VData data = new VData();
            LCGrpPolDB db = new LCGrpPolDB();
            String sqlGrpPolSet ="select a.* From lcgrppol a, ljspaygrp b  where a.GrpContNo ='"+mLCGrpContSchema.getGrpContNo()+"'"
                             +" and a.grppolno = b.grppolno "
                             +" and b.getnoticeno = '"+mGetNoticeNo+"'";
            LCGrpPolSet tLCGrpPolSet = db.executeQuery(sqlGrpPolSet);

            data.add(tLCGrpPolSet);
            data.add(tGI);
            data.add(new TransferData());

            System.out.println("Beginning of GRnewDueVerifyBL.dealData: GRnewContSign");
            GRnewGrpContSignBL tGRnewGrpContSignBL = new GRnewGrpContSignBL();
            MMap tMMap = tGRnewGrpContSignBL.getSubmitMap(data, mOperate);
            if(tMMap == null && tGRnewGrpContSignBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tGRnewGrpContSignBL.mErrors);
                System.out.println(tGRnewGrpContSignBL.mErrors.getErrContent());
                return false;
            }
            map.add(tMMap);
            mInputData.add(map);
        }

        return true;
    }
    
    /**
     * 校验是否需要续保确认和数据转移
     * 1、没有续保轨迹，不需要续保转移，false
     * 2、有续保轨迹但状态不是待确认、确认成功，转移成功，false
     * @return boolean
     */
    private boolean needPRnewOperate()
    {
        String sql = "  select * "
                     + "from LCRnewStateLog a,ljspay b "
                     + "where GrpContNo = '" + mLCGrpContSchema.getGrpContNo() + "' "
                     + "   and newGrpContNo = "
                     + "      (select max(newGrpContNo) "
                     + "      from LCRnewStateLog "
                     + "      where GrpContNo = a.GrpContNo "
                     + "         and state != '6') "
                     + " and a.GrpContNo = b.otherno and getnoticeno='"+mLJSPayBL.getGetNoticeNo()+"' "
                     ;
        System.out.println(sql);
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        LCRnewStateLogSet tLCRnewStateLogSet
            = tLCRnewStateLogDB.executeQuery(sql);
        if(tLCRnewStateLogSet.size() == 0)
        {
            return false;
        }
        mRnewState = tLCRnewStateLogSet.get(1).getState();
        if(tLCRnewStateLogSet.get(1).getState()
           .compareTo(XBConst.RNEWSTATE_UNCONFIRM) < 0)
        {
            return false;
        }

        return true;
    }
    
    /**
     * 个单应收记录作废/撤销类，在IndiLJSCancelBL.getSubmitMap方法 中增加对催缴工单的处理
     * @return boolean
     */
    private boolean dealPhoneHasten()
    {
        String sql = "select * "
                     + "from LGPhoneHasten "
                     + "where getNoticeNo = '"
                     + mLJSPayBL.getGetNoticeNo() + "' "
                     + "   and HastenType = '"
                     + NextFeeHastenTask.HASTEN_TYPE_NEXTFEEG + "' "
                     + "   and FinishType is null ";
        LGPhoneHastenSet set = new LGPhoneHastenDB().executeQuery(sql);
        if(set.size() == 0)
        {
            //若无催缴就查不出来也
            return true;
        }

        LGPhoneHastenSchema schema = set.get(1);
        schema.setFinishType("4");
        schema.setRemark("应收记录" + mLJSPayBL.getGetNoticeNo() + "已收费");

        VData data = new VData();
        data.add(schema);
        data.add(tGI);

        TaskPhoneFinishBL bl = new TaskPhoneFinishBL();
        MMap tMMap = bl.getSubmitMap(data, "");
        if(tMMap == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        map.add(tMMap);

        return true;
    }


    /**
     *
     * @return boolean
     */
    private boolean dealData() {
    	//防止并发加锁
    	MMap tCekMap1 = null;
    	tCekMap1 = lockLJAGet(mLCGrpContSchema);
        if (tCekMap1 == null){
            return false;
        } 
        VData  tableInputData = new VData();
		tableInputData.add(tCekMap1);
		PubSubmit tablePubSubmit = new PubSubmit();
		if (tablePubSubmit.submitData(tableInputData, "") == false) {
			System.out.println(tablePubSubmit.mErrors.getFirstError());
			CError tError = new CError();
			tError.moduleName = "PubSubmit";
			tError.functionName = "submitData";
			tError.errorMessage = "该团单正进行核销操作，请不要重复操作";
			this.mErrors.addOneError(tError);
			return false;
		}
    	
        String ManageCom = tGI.ManageCom;
        String Operator = tGI.Operator;
        String tLimit = "";
        String serNo = "";
        String payNo = "";
        String grpContNo = mLCGrpContSchema.getGrpContNo(); //获取从前台页面传入的团单合同号
        String str = "select a.edorAcceptNo "
                     + "from LPEdorApp a, LPGrpEdorItem b "
                     + "where a.edorAcceptNo = b.edorNo "
                     + "   and b.grpContNo = '" + mLCGrpContSchema.getGrpContNo()
                     + "' "
                     + "   and a.edorState not in('0')";
        String temp = new ExeSQL().getOneValue(str);
        if(!temp.equals("")){
            mErrors.addOneError("保单正在做保全不能核销" + mLCGrpContSchema.getGrpContNo());
            return false;
        }
        
        if(!checkLPState())
        {
            return false;
        }
        
        ExeSQL tExeSQL = new ExeSQL();
        StringBuffer tSBql = new StringBuffer(128); 
    	tLimit = PubFun.getNoLimit(ManageCom);
    	serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //给应收表生成统一的流水号
    	mOperateFlag = "1";//暂时只开发个案核销
    	mserNo = serNo;
    	//添加数据到催收核销日志表
    	if (!dealUrgeLog("1", "", "INSERT")) {
    		return false;
    	}

        payNo = PubFun1.CreateMaxNo("PayNo", tLimit); //给实收表生成统一的　收据号
        System.out.println(" 续保核销时，取PayNo:" + payNo + "信息");
        String tempdealstate = "4";
        String ljspaybSQL = "select * from LJSPayB where GetNoticeNo = '"+mLJSPayBL.getGetNoticeNo()+
                            "' and OtherNoType = '1' and dealstate = '"+tempdealstate+"'";
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        LJSPayBSet tLJSPayBSet = tLJSPayBDB.executeQuery(ljspaybSQL);
        System.out.println(" 续保核销时，取LJSPayB信息");
        if (tLJSPayBSet.size() == 0) {
            CError.buildErr(this, "团单下的备份数据缺失，请催收后再核销！");
            return false;
        }
        mLJSPayBSchema = tLJSPayBSet.get(1);

        //下面获取应收集体数据，准备核销时删除！
        LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
        tLJSPayGrpDB.setGrpContNo(grpContNo);
        mLJSPayGrpSet = tLJSPayGrpDB.query(); 
        if (mLJSPayGrpSet.size() == 0) {
            CError.buildErr(this, "团单下的集体催收数据缺失，不能核销！");
            return false;
        }
        System.out.println(" 续保核销时，取LJSPayGrp信息");
        
        //下面获取应收集体备份数据，准备核销时变更状态
        LJSPayGrpBDB tLJSPayGrpBDB = new LJSPayGrpBDB();
        tLJSPayGrpBDB.setGrpContNo(grpContNo);
        tLJSPayGrpBDB.setGetNoticeNo(mLJSPayBL.getGetNoticeNo());
        LJSPayGrpBSet tLJSPayGrpBSet = new LJSPayGrpBSet();
        tLJSPayGrpBSet = tLJSPayGrpBDB.query(); 
        System.out.println(" 续保核销时，取LJSPayGrpB信息");
        if (tLJSPayGrpBSet.size() == 0) {
            CError.buildErr(this, "团单下的集体催收数据缺失，不能核销！");
            return false;
        }

        //下面获取应收个人数据
        String strPerCntSQL =
                " SELECT count(*) FROM LJsPayPerson where GrpContNo='"
                + mLCGrpContSchema.getGrpContNo() + "' with ur";
        tExeSQL = new ExeSQL();
        String strPerCnt = tExeSQL.getOneValue(strPerCntSQL);
        System.out.println(" 续保核销时，取LJsPayPerson条数:" + strPerCnt + "信息");
        if (tExeSQL.mErrors.needDealError()) {
            CError.buildErr(this, "查询数据 LJsPayPerson 出错！");
            return false;
        }
        if (strPerCnt == null || strPerCnt.equals("0")) {
            CError.buildErr(this, "团单下的个人催收数据缺失，不能核销！");
            return false;
        }

        LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        tLJTempFeeClassDB.setTempFeeNo(mLJSPayBL.getGetNoticeNo()); //说明：催收产生的催缴通知书号对应交费通知书号
        tLJTempFeeClassDB.setConfFlag("0");
        mLJTempFeeClassSet = tLJTempFeeClassDB.query();
        System.out.println(" 续保核销时，取LJTempFeeClass信息");
        if (mLJSPayBL.getSumDuePayMoney() > 0 && mLJTempFeeClassSet.size() == 0) {
            CError.buildErr(this, "暂交费分类信息缺失，不能核销！");
            return false;
        }


        if (mLJSPayBL.getSumDuePayMoney() > 0 && mLJTempFeeSchema.getEnterAccDate() == null) {
            CError.buildErr(this, "财务未到帐，请到帐并作会计确认后再做核销！");
            return false;
        }
        
        if(!checkWLPYH()){
        	return false;
        }
        
        String strSQL = tSBql.toString();

        //TODO 获取团单下的新险种信息
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tSBql = new StringBuffer(128);
        tSBql.append("select * from LCGrpPol where GrpContNo=")
                .append("(select max(newgrpcontno) from lcrnewstatelog where grpcontno = '")
                .append(grpContNo)
                .append("' and state = '")
                .append(XBConst.RNEWSTATE_UNCONFIRM)
                .append("' and Grppolno in (select GrpPolNo from LJSPayGrp where GetNoticeNo = '")
                .append(mLJSPayBL.getGetNoticeNo())
                .append("')) with ur"); 
        String grpPolSqlStr = tSBql.toString();
        System.out.println(grpPolSqlStr);
        System.out.println(" 续保核销时，取LCGrpPol信息");
        mLCGrpPolSet = tLCGrpPolDB.executeQuery(grpPolSqlStr);
        if (mLCGrpPolSet.size() == 0) {
            CError.buildErr(this, "团单下的集体催收数据缺失，不能核销！");
            return false;
        }

        String getNoticeNo = mLJSPayBL.getGetNoticeNo();
        double grpContActuPremTotal = 0.0;
        double grpContPremTotal = 0.0;
        /**
         * 循环处理团单下临时险种
         */
        for (int i = 1; i <= mLCGrpPolSet.size(); i++) {
        	LCGrpPolBL tLCGrpPolBL = new LCGrpPolBL();
        	tLCGrpPolBL.setSchema(mLCGrpPolSet.get(i).getSchema());
        	String grpPolNo = mLCGrpPolSet.get(i).getGrpPolNo();
        	//获取原险种信息
        	LCGrpPolSchema oldLCGrpPolSchema = getOldLCGrpPolSchema(tLCGrpPolBL);
        	String grpPolPayToDate = "";
        	double grpPolPremTotal = 0.0;
        	
        	/**
        	 * 获取该临时团单险种信息下的个险临时险种数据
        	 */
    		tSBql = new StringBuffer(128);
    		//查询每个分单下的临时险种信息
    		tSBql.append("select * ")
                .append("from LCPol ")
                .append("where grppolno = '")
                .append(grpPolNo)
                .append("' and GrpcontNo = ")
                .append("      (select max(newGrpContNo) ")
                .append("      from LCRnewStateLog ")
                .append("      where NewGrpPolNo = '")
                .append(grpPolNo)
                .append("'        and state = '")
                .append(XBConst.RNEWSTATE_UNCONFIRM)
                .append("' ")
                .append("and polno in (select polno from LJSPayPerson where GetNoticeNo='"+mGetNoticeNo+"') ")
                .append(") ");
    		
    		String polSqlStr = tSBql.toString();
    		System.out.println(polSqlStr);
    		System.out.println("获取个险表临时数据");    		
    		LCPolDB tLCPolDB = new LCPolDB();
    		LCPolSet tLCPolSet = tLCPolDB.executeQuery(polSqlStr);
    		
    		String polPayToDate = ""; // 险种的交至日期    		
    		for (int k = 1; k <= tLCPolSet.size(); k++) {
                LCPolBL tempLCPolBL = new LCPolBL();
                tempLCPolBL.setSchema(tLCPolSet.get(k));
                //获取原险种信息
                LCPolSchema oldLCPolSchema = getOldLCPolSchema(tempLCPolBL);
                if(oldLCPolSchema == null){
                    return false;
                }
                double polTotalPrem = 0.0; // 保存单个险种下的所交保费总和
                String contNo = tempLCPolBL.getContNo(); // 临时保单险种号
                String polNo = tempLCPolBL.getPolNo(); // 临时保单险种号    			
				    				
	            // 查询责任项
	            LCDutyDB tLCDutyDB = new LCDutyDB();
	            tLCDutyDB.setPolNo(polNo);
	            LCDutySet tLCDutySet = tLCDutyDB.query();
	            if (tLCDutyDB.mErrors.needDealError()) {
	                // @@错误处理
	                this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "IndiFinUrgeVerifyBL";
	                tError.functionName = "dealData";
	                tError.errorMessage = "险种责任表查询失败!";
	                this.mErrors.addOneError(tError);
	                return false;
	            }
	            if (tLCDutySet.size() == 0) {
	                CError.buildErr(this,
	                                "保单号:" + contNo + "，险种号：" + polNo +
	                                "下的险种责任信息缺失，不能核销！");
	                return false;
	            }
	            
	            int polCount = 0; // 判断该险种下是否有对应的应收数据信息，有则修改险种信息，否则不变	            
	            for (int m = 1; m <= tLCDutySet.size(); m++) {
	                LCDutyBL tLCDutyBL = new LCDutyBL();
	                tLCDutyBL.setSchema(tLCDutySet.get(m));
	                // 责任编码
	                String dutyCode = tLCDutySet.get(m).getDutyCode();

	                LCPremDB tLCPremDB = new LCPremDB();
	                tLCPremDB.setPolNo(polNo);
	                tLCPremDB.setDutyCode(dutyCode);
	                LCPremSet tLCPremSet = tLCPremDB.query();
	                if (tLCPremDB.mErrors.needDealError()) {
	                    // @@错误处理
	                    this.mErrors.copyAllErrors(tLCPremDB.mErrors);
	                    CError tError = new CError();
	                    tError.moduleName = "IndiFinUrgeVerifyBL";
	                    tError.functionName = "dealData";
	                    tError.errorMessage = "保费项信息查询失败!";
	                    this.mErrors.addOneError(tError);
	                    return false;
	                }
	                if (tLCPremSet.size() == 0) {
	                    CError.buildErr(this,
	                                    "保单号:" + contNo + "，险种号：" + polNo +
	                                    ",责任编码:" +
	                                    dutyCode + "下的险种责任信息缺失，不能核销！");
	                    return false;
	                }

	                int dutyCount = 0; // 责任的保费项个数
	                double totalPrem = 0.0; // 责任下的保费项保费
	                String PaytoDate = ""; // 责任下的保费项交至日期
	                for (int n = 1; n <= tLCPremSet.size(); n++) {
	                    LCPremBL tLCPremBL = new LCPremBL();
	                    tLCPremBL.setSchema(tLCPremSet.get(n));
	                    // 交费计划编码
	                    String payPlanCode = tLCPremSet.get(n).getPayPlanCode();

	                    // 查询应收个人交费表中的正常交费记录
	                    LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
	                    tLJSPayPersonDB.setPolNo(oldLCPolSchema.getPolNo());
	                    tLJSPayPersonDB.setDutyCode(dutyCode);
	                    tLJSPayPersonDB.setPayPlanCode(payPlanCode);
	                    // 由于支持多次抽档，因此所有的LJS表查询都需要加上GetNoticeNo条件
	                    tLJSPayPersonDB.setGetNoticeNo(mLJSPayBL.getGetNoticeNo());
	                    LJSPayPersonSet tLJSPayPersonSet = tLJSPayPersonDB.query();
	                    if (tLJSPayPersonDB.mErrors.needDealError()) {
	                        // @@错误处理
	                        this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);
	                        CError tError = new CError();
	                        tError.moduleName = "IndiFinUrgeVerifyBL";
	                        tError.functionName = "dealData";
	                        tError.errorMessage = "应收个人表查询失败!";
	                        this.mErrors.addOneError(tError);
	                        return false;
	                    }
	                    if (tLJSPayPersonSet.size() == 0) {
	                        continue;
	                    } else {
	                        tLCPremBL.setPayTimes(tLCPremBL.getPayTimes() + 1); // 已交费次数
	                        tLCPremBL.setSumPrem(tLCPremBL.getSumPrem() +tLCPremBL.getPrem()); // 累计保费
	                        tLCPremBL.setPaytoDate(tLJSPayPersonSet.get(1).getCurPayToDate()); // 交至日期
	                        tLCPremBL.setModifyDate(CurrentDate); // 最后一次修改日期
	                        tLCPremBL.setModifyTime(CurrentTime); // 最后一次修改时间
	                        mLCPremNewSet.add(tLCPremBL);

	                        //取保费项较早的paytodate作为责任表的paytodate
	                        if (PaytoDate.equals("")) {
	                            PaytoDate = tLCPremBL.getPaytoDate();
	                        } else {
	                            FDate tFDate = new FDate();
	                            Date tDate = tFDate.getDate(PaytoDate);
	                            Date tDate1 = tFDate.getDate(tLCPremBL.getPaytoDate());
	                            if (tDate1.before(tDate)) {
	                                PaytoDate = tLCPremBL.getPaytoDate();
	                            }
	                        }
	                        polPayToDate = tLCPremBL.getPaytoDate(); // 险种的交至日期，随便取一个应收表的信息即可
	                        grpPolPayToDate = tLCPremBL.getPaytoDate(); // 团单险种的交至日期，随便取一个应收表的信息即可
	                        // 说明
	                        totalPrem += tLCPremBL.getPrem();
	                        polTotalPrem += tLCPremBL.getPrem();
	                        dutyCount++;
	                        polCount++;

	                        // 多少应收信息产生多少实收信息，方便以后统计使用
	                        for (int t = 1; t <= tLJSPayPersonSet.size(); t++) {
	                            // 将应收个人表的数据映射到实收个人表中去
	                            LJAPayPersonBL tLJAPayPersonBL = new LJAPayPersonBL();
	                            tLJAPayPersonBL.setPolNo(tLJSPayPersonSet.get(t).getPolNo()); // 保单号码
	                            tLJAPayPersonBL.setPayCount(tLJSPayPersonSet.get(t).getPayCount()); // 第几次交费
	                            tLJAPayPersonBL.setGrpContNo(tLJSPayPersonSet.get(t).getGrpContNo()); // 集体保单号码
	                            tLJAPayPersonBL.setGrpPolNo(tLJSPayPersonSet.get(t).getGrpPolNo()); // 集体保单号码
	                            tLJAPayPersonBL.setContNo(tLJSPayPersonSet.get(t).getContNo()); // 总单/合同号码
	                            tLJAPayPersonBL.setAppntNo(tLJSPayPersonSet.get(t).getAppntNo()); // 投保人客户号码
	                            tLJAPayPersonBL.setPayNo(payNo); // 交费收据号码
	                            tLJAPayPersonBL.setPayTypeFlag(tLJSPayPersonSet.get(t).getPayTypeFlag());
	                            tLJAPayPersonBL.setPayAimClass(tLJSPayPersonSet.get(t).getPayAimClass()); // 交费目的分类
	                            tLJAPayPersonBL.setDutyCode(tLJSPayPersonSet.get(t).getDutyCode()); // 责任编码
	                            tLJAPayPersonBL.setPayPlanCode(tLJSPayPersonSet.get(t).getPayPlanCode()); // 交费计划编码
	                            tLJAPayPersonBL.setSumDuePayMoney(tLJSPayPersonSet.get(t).getSumDuePayMoney()); // 总应交金额
	                            tLJAPayPersonBL.setSumActuPayMoney(tLJSPayPersonSet.get(t).getSumActuPayMoney()); // 总实交金额
	                            tLJAPayPersonBL.setPayIntv(tLJSPayPersonSet.get(t).getPayIntv()); // 交费间隔
	                            tLJAPayPersonBL.setPayDate(mPayDate); // 交费日期
	                            tLJAPayPersonBL.setPayType(tLJSPayPersonSet.get(t).getPayType()); // 交费类型
                                tLJAPayPersonBL.setEnterAccDate(mEnterAccDate); // 到帐日期
                                tLJAPayPersonBL.setConfDate(mConfDate); // 确认日期
	                            tLJAPayPersonBL.setLastPayToDate(tLJSPayPersonSet.get(t).getLastPayToDate()); // 原交至日期
	                            tLJAPayPersonBL.setCurPayToDate(tLJSPayPersonSet. get(t).getCurPayToDate()); // 现交至日期
	                            tLJAPayPersonBL.setInInsuAccState(tLJSPayPersonSet.get(t).getInInsuAccState()); // 转入保险帐户状态
	                            tLJAPayPersonBL.setApproveCode(tLJSPayPersonSet.get(t).getApproveCode()); // 复核人编码
	                            tLJAPayPersonBL.setApproveDate(tLJSPayPersonSet.get(t).getApproveDate()); // 复核日期
	                            tLJAPayPersonBL.setApproveTime(tLJSPayPersonSet.get(t).getApproveTime()); // 复核时间
	                            tLJAPayPersonBL.setAgentCode(tLJSPayPersonSet.get(t).getAgentCode());
	                            tLJAPayPersonBL.setAgentGroup(tLJSPayPersonSet.get(t).getAgentGroup());
	                            tLJAPayPersonBL.setSerialNo(mserNo); // 流水号
	                            tLJAPayPersonBL.setOperator(Operator); // 实际截转为实收的操作员
	                            tLJAPayPersonBL.setMakeDate(CurrentDate); // 入机日期
	                            tLJAPayPersonBL.setMakeTime(CurrentTime); // 入机时间
	                            tLJAPayPersonBL.setGetNoticeNo(tLJSPayPersonSet.get(t).getGetNoticeNo()); // 通知书号码
	                            tLJAPayPersonBL.setModifyDate(CurrentDate); // 最后一次修改日期
	                            tLJAPayPersonBL.setModifyTime(CurrentTime); // 最后一次修改时间
	                            tLJAPayPersonBL.setManageCom(tLJSPayPersonSet.get(t).getManageCom()); // 管理机构
	                            tLJAPayPersonBL.setAgentCom(tLJSPayPersonSet.get(t).getAgentCom()); // 代理机构
	                            tLJAPayPersonBL.setAgentType(tLJSPayPersonSet.get(t).getAgentType()); // 代理机构内部分类
	                            tLJAPayPersonBL.setRiskCode(tLJSPayPersonSet.get(t).getRiskCode()); // 险种编码
	                            mLJAPayPersonSet.add(tLJAPayPersonBL);
	                        }
	                    }
	                }
	                if (dutyCount == 0) {
	                    continue;
	                } else {
	                    tLCDutyBL.setPrem(totalPrem); // 实际保费
	                    tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem() + totalPrem); // 累计保费
	                    tLCDutyBL.setPaytoDate(PaytoDate); // 交至日期
	                    tLCDutyBL.setModifyDate(CurrentDate); // 最后一次修改日期
	                    tLCDutyBL.setModifyTime(CurrentTime); // 最后一次修改时间
	                    mLCDutyNewSet.add(tLCDutyBL);
	                }
	            }
	            if (polCount == 0) {
	                continue;
	            } else {
	                // 修改险种信息
	                tempLCPolBL.setPaytoDate(polPayToDate); // 交至日期
	                tempLCPolBL.setSumPrem(tempLCPolBL.getSumPrem() + polTotalPrem); // 总累计保费
	                // 求总余额:如果应收总表应收款=0，表明有余额，且这次的余额值存放在责任编码为
	                // "yet"的应收个人交费纪录中（见个人催收流程图）,取出放在个人保单表的余额字段中
	                // 否则，个人保单表余额纪录置为0
	                tempLCPolBL.setModifyDate(CurrentDate); // 最后一次修改日期
	                tempLCPolBL.setModifyTime(CurrentTime); // 最后一次修改时间
	                mLCPolNewSet.add(tempLCPolBL);
	            }
	            grpPolPremTotal += polTotalPrem;
    		}
    		
    		tLCGrpPolBL.setPrem(grpPolPremTotal);
    		tLCGrpPolBL.setSumPrem(tLCGrpPolBL.getPrem()+grpPolPremTotal);
    		tLCGrpPolBL.setPaytoDate(grpPolPayToDate);
    		tLCGrpPolBL.setModifyDate(CurrentDate); // 最后一次修改日期
    		tLCGrpPolBL.setModifyTime(CurrentTime); // 最后一次修改时间
            mLCGrpPolNewSet.add(tLCGrpPolBL);
        
            


            //下面处理集体险种信息，并将LJSPayGrp转为LJAPayGrp
            LJSPayGrpDB tempLJSPayGrpDB = new LJSPayGrpDB();
            tempLJSPayGrpDB.setGrpPolNo(oldLCGrpPolSchema.getGrpPolNo());
            tempLJSPayGrpDB.setGetNoticeNo(getNoticeNo);
            tempLJSPayGrpDB.setPayType("ZC");
            LJSPayGrpSet tempLJSPayGrpSet = new LJSPayGrpSet();
            tempLJSPayGrpSet = tempLJSPayGrpDB.query();
            LJSPayGrpBL tLJSPayGrpBL = new LJSPayGrpBL();
            tLJSPayGrpBL.setSchema(tempLJSPayGrpSet.get(1).getSchema());
            //下面应收转实收
            LJAPayGrpBL tLJAPayGrpBL = new LJAPayGrpBL();
            tLJAPayGrpBL.setGrpPolNo(tLJSPayGrpBL.getGrpPolNo());
            tLJAPayGrpBL.setGrpContNo(grpContNo);
            tLJAPayGrpBL.setPayCount(tLJSPayGrpBL.getPayCount());
            tLJAPayGrpBL.setAppntNo(tLJSPayGrpBL.getAppntNo());
            tLJAPayGrpBL.setPayNo(payNo);
            tLJAPayGrpBL.setSumDuePayMoney(tLJSPayGrpBL.getSumDuePayMoney()); //总应交金额
            tLJAPayGrpBL.setSumActuPayMoney(tLJSPayGrpBL.getSumActuPayMoney()); //总实交金额
            tLJAPayGrpBL.setPayIntv(tLJSPayGrpBL.getPayIntv());
            tLJAPayGrpBL.setPayDate(mPayDate);
            tLJAPayGrpBL.setPayType("ZC"); //repair:交费类型
            tLJAPayGrpBL.setEnterAccDate(this.mEnterAccDate);
            tLJAPayGrpBL.setConfDate(this.mConfDate); //确认日期
            tLJAPayGrpBL.setLastPayToDate(tLJSPayGrpBL.getLastPayToDate()); //原交至日期
            tLJAPayGrpBL.setCurPayToDate(tLJSPayGrpBL.getCurPayToDate());
            tLJAPayGrpBL.setApproveCode(mLCGrpPolSet.get(i).getApproveCode());
            tLJAPayGrpBL.setApproveDate(mLCGrpPolSet.get(i).getApproveDate());
            tLJAPayGrpBL.setSerialNo(mserNo);
            tLJAPayGrpBL.setOperator(Operator);
            tLJAPayGrpBL.setMakeDate(PubFun.getCurrentDate());
            tLJAPayGrpBL.setMakeTime(PubFun.getCurrentTime());
            tLJAPayGrpBL.setGetNoticeNo(tLJSPayGrpBL.getGetNoticeNo()); //通知书号码
            tLJAPayGrpBL.setModifyDate(PubFun.getCurrentDate());
            tLJAPayGrpBL.setModifyTime(PubFun.getCurrentTime());
            tLJAPayGrpBL.setManageCom(mLCGrpPolSet.get(i).getManageCom()); //管理机构
            tLJAPayGrpBL.setAgentCom(mLCGrpPolSet.get(i).getAgentCom()); //代理机构
            tLJAPayGrpBL.setAgentType(mLCGrpPolSet.get(i).getAgentType()); //代理机构内部分类
            tLJAPayGrpBL.setRiskCode(mLCGrpPolSet.get(i).getRiskCode()); //险种编码
            tLJAPayGrpBL.setAgentCode(mLCGrpPolSet.get(i).getAgentCode());
            tLJAPayGrpBL.setAgentGroup(mLCGrpPolSet.get(i).getAgentGroup());
            
            mLJAPayGrpSet.add(tLJAPayGrpBL);
            

            grpContPremTotal += grpPolPremTotal;
            grpContActuPremTotal += tLJAPayGrpBL.getSumActuPayMoney();

        }
        //将类型为YEL的LJSPayGrp应收转为LJAPayGrp实收，（财务会做相关处理）
        tSBql = new StringBuffer(128);
        tSBql.append("select * from ljspaygrp where GrpContNo='")
                .append(grpContNo)
                .append("'")
                .append(" and GetNoticeNo='" + mLJSPayBL.getGetNoticeNo())
                .append("' and PayType='YEL'")
                ;
        String yelSqlStr = tSBql.toString();
        LJSPayGrpDB tempLJSPayGrpDB = new LJSPayGrpDB();
        LJSPayGrpSet tempLJSPayGrpSet = new LJSPayGrpSet();
        tempLJSPayGrpSet = tempLJSPayGrpDB.executeQuery(yelSqlStr);
        System.out.println(" 续保核销时，取余额");
        if (tempLJSPayGrpSet.size() > 0) {
            //将类型为YEL的LJSPayGrp应收转为LJAPayGrp实收
            LJSPayGrpBL yelLJSPayGrpBL = new LJSPayGrpBL();
            yelLJSPayGrpBL.setSchema(tempLJSPayGrpSet.get(1).getSchema());
            LJAPayGrpBL yelLJAPayGrpBL = new LJAPayGrpBL();
            yelLJAPayGrpBL.setGrpContNo(yelLJSPayGrpBL.getGrpContNo());
            yelLJAPayGrpBL.setPayCount(yelLJSPayGrpBL.getPayCount());
            yelLJAPayGrpBL.setGrpPolNo(yelLJSPayGrpBL.getGrpPolNo());
            yelLJAPayGrpBL.setPayCount(yelLJSPayGrpBL.getPayCount());
            yelLJAPayGrpBL.setAppntNo(mLCGrpContSchema.getAppntNo());
            yelLJAPayGrpBL.setPayNo(payNo); //交费收据号码
            yelLJAPayGrpBL.setSumDuePayMoney(yelLJSPayGrpBL.
                                             getSumDuePayMoney());
            yelLJAPayGrpBL.setSumActuPayMoney(yelLJSPayGrpBL.
                                              getSumActuPayMoney());
            yelLJAPayGrpBL.setPayIntv(yelLJSPayGrpBL.getPayIntv());
            yelLJAPayGrpBL.setLastPayToDate(yelLJSPayGrpBL.
                                            getLastPayToDate());
            yelLJAPayGrpBL.setCurPayToDate(yelLJSPayGrpBL.getCurPayToDate());
            yelLJAPayGrpBL.setPayDate(mPayDate);
            yelLJAPayGrpBL.setEnterAccDate(this.mEnterAccDate);
            yelLJAPayGrpBL.setPayType("YEL");
            yelLJAPayGrpBL.setSerialNo(serNo);
            yelLJAPayGrpBL.setGetNoticeNo(yelLJSPayGrpBL.getGetNoticeNo());
            yelLJAPayGrpBL.setOperator(Operator);
            yelLJAPayGrpBL.setMakeDate(PubFun.getCurrentDate());
            yelLJAPayGrpBL.setMakeTime(PubFun.getCurrentTime());
            yelLJAPayGrpBL.setModifyDate(PubFun.getCurrentDate());
            yelLJAPayGrpBL.setModifyTime(PubFun.getCurrentTime());
            yelLJAPayGrpBL.setManageCom(yelLJSPayGrpBL.getManageCom()); //管理机构
            yelLJAPayGrpBL.setAgentCom(yelLJSPayGrpBL.getAgentCom()); //代理机构
            yelLJAPayGrpBL.setAgentType(yelLJSPayGrpBL.getAgentType()); //代理机构内部分类
            yelLJAPayGrpBL.setRiskCode(yelLJSPayGrpBL.getRiskCode()); //险种编码
            yelLJAPayGrpBL.setAgentCode(yelLJSPayGrpBL.getAgentCode());
            yelLJAPayGrpBL.setAgentGroup(yelLJSPayGrpBL.getAgentGroup());
            mLJAPayGrpSet.add(yelLJAPayGrpBL);

            grpContActuPremTotal += yelLJAPayGrpBL.getSumActuPayMoney();

            dealAppAcc(yelLJAPayGrpBL);
        }
        
         //核销时在ljapaygrp中出入YET的记录
        tSBql = new StringBuffer(128);
        tSBql.append("select * from ljspaygrp where GrpContNo='")
                .append(grpContNo)
                .append("'")
                .append(" and GetNoticeNo='" + mLJSPayBL.getGetNoticeNo())
                .append("' and PayType='YET'");
        String yetSqlStr = tSBql.toString();
        LJSPayGrpDB tempYELJSPayGrpDB = new LJSPayGrpDB();
        LJSPayGrpSet tempYELJSPayGrpSet = new LJSPayGrpSet();
        tempYELJSPayGrpSet = tempYELJSPayGrpDB.executeQuery(yetSqlStr);
        System.out.println(" 续保核销时，退回账户金额");     
        
        if (tempYELJSPayGrpSet.size() > 0) {
            //将类型为YET的LJSPayGrp应收转为LJAPayGrp实收
            LJSPayGrpBL yetLJSPayGrpBL = new LJSPayGrpBL();
            yetLJSPayGrpBL.setSchema(tempYELJSPayGrpSet.get(1).getSchema());
            LJAPayGrpBL yetLJAPayGrpBL = new LJAPayGrpBL();
            yetLJAPayGrpBL.setGrpContNo(yetLJSPayGrpBL.getGrpContNo());
            yetLJAPayGrpBL.setPayCount(yetLJSPayGrpBL.getPayCount());
            yetLJAPayGrpBL.setGrpPolNo(yetLJSPayGrpBL.getGrpPolNo());
            yetLJAPayGrpBL.setPayCount(yetLJSPayGrpBL.getPayCount());
            yetLJAPayGrpBL.setAppntNo(mLCGrpContSchema.getAppntNo());
            yetLJAPayGrpBL.setPayNo(payNo); //交费收据号码
            yetLJAPayGrpBL.setSumDuePayMoney(yetLJSPayGrpBL.
                                             getSumDuePayMoney());
            yetLJAPayGrpBL.setSumActuPayMoney(yetLJSPayGrpBL.
                                              getSumActuPayMoney());
            yetLJAPayGrpBL.setPayIntv(yetLJSPayGrpBL.getPayIntv());
            yetLJAPayGrpBL.setLastPayToDate(yetLJSPayGrpBL.
                                            getLastPayToDate());
            yetLJAPayGrpBL.setCurPayToDate(yetLJSPayGrpBL.getCurPayToDate());
            yetLJAPayGrpBL.setPayDate(mPayDate);
            yetLJAPayGrpBL.setEnterAccDate(this.mEnterAccDate);
            yetLJAPayGrpBL.setPayType("YET");
            yetLJAPayGrpBL.setSerialNo(serNo);
            yetLJAPayGrpBL.setGetNoticeNo(yetLJSPayGrpBL.getGetNoticeNo());
            yetLJAPayGrpBL.setOperator(Operator);
            yetLJAPayGrpBL.setMakeDate(PubFun.getCurrentDate());
            yetLJAPayGrpBL.setMakeTime(PubFun.getCurrentTime());
            yetLJAPayGrpBL.setModifyDate(PubFun.getCurrentDate());
            yetLJAPayGrpBL.setModifyTime(PubFun.getCurrentTime());
            yetLJAPayGrpBL.setManageCom(yetLJSPayGrpBL.getManageCom()); //管理机构
            yetLJAPayGrpBL.setAgentCom(yetLJSPayGrpBL.getAgentCom()); //代理机构
            yetLJAPayGrpBL.setAgentType(yetLJSPayGrpBL.getAgentType()); //代理机构内部分类
            yetLJAPayGrpBL.setRiskCode(yetLJSPayGrpBL.getRiskCode()); //险种编码
            yetLJAPayGrpBL.setAgentCode(yetLJSPayGrpBL.getAgentCode());
            yetLJAPayGrpBL.setAgentGroup(yetLJSPayGrpBL.getAgentGroup());
            mLJAPayGrpSet.add(yetLJAPayGrpBL);

            grpContActuPremTotal += yetLJAPayGrpBL.getSumActuPayMoney();

            dealAppAcc(yetLJAPayGrpBL);
        }
        
        // 在这里处理合同级的PayToDate
        mLCGrpContSchema.setPrem(grpContPremTotal);
        mLCGrpContSchema.setSumPrem(grpContPremTotal);
        mLCGrpContSchema.setModifyDate(CurrentDate); // 最后一次修改日期
        mLCGrpContSchema.setModifyTime(CurrentTime); // 最后一次修改时间
        
        //处理　暂收费表余额
        double tempFeeSum = 0;
        for (int n = 1; n <= mLJTempFeeSet.size(); n++) {
            tempFeeSum += mLJTempFeeSet.get(n).getPayMoney();
        }
        grpContPremTotal = Arith.round(grpContPremTotal, 2);

        //暂交费金额大于应收费金额
        if(tempFeeSum > mLJSPayBL.getSumDuePayMoney()){
        	grpContActuPremTotal += (tempFeeSum - mLJSPayBL.getSumDuePayMoney());
        	addAccBala(grpContActuPremTotal);
        }
        
        //应收总表转实收总表
        mLJAPayBL.setPayNo(payNo); //交费收据号码
        mLJAPayBL.setIncomeNo(mLJSPayBL.getOtherNo()); //应收/实收编号,集体合同号
        mLJAPayBL.setIncomeType(mLJSPayBL.getOtherNoType()); //应收/实收编号类型
        mLJAPayBL.setAppntNo(mLJSPayBL.getAppntNo()); //  投保人客户号码
        mLJAPayBL.setSumActuPayMoney(grpContActuPremTotal); // 总实交金额
        mLJAPayBL.setGetNoticeNo(mLJSPayBL.getGetNoticeNo()); //交费收据号,暂时先取应收总表的通知号
        mLJAPayBL.setEnterAccDate(mEnterAccDate); // 到帐日期
        mLJAPayBL.setPayDate(mPayDate); //交费日期,仍然取了最小日期
        mLJAPayBL.setConfDate(mConfDate); //确认日期
        mLJAPayBL.setApproveCode(mLJSPayBL.getApproveCode()); //复核人编码
        mLJAPayBL.setApproveDate(mLJSPayBL.getApproveDate()); //  复核日期
        mLJAPayBL.setSerialNo(serNo); //流水号
        mLJAPayBL.setOperator(Operator); // 操作员
        mLJAPayBL.setMakeDate(PubFun.getCurrentDate()); //入机时间
        mLJAPayBL.setMakeTime(PubFun.getCurrentTime()); //入机时间
        mLJAPayBL.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
        mLJAPayBL.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
        mLJAPayBL.setRiskCode(mLJSPayBL.getRiskCode()); // 险种编码
        mLJAPayBL.setManageCom(mLJSPayBL.getManageCom());
        mLJAPayBL.setAgentCode(mLJSPayBL.getAgentCode());
        mLJAPayBL.setAgentGroup(mLJSPayBL.getAgentGroup());
        mLJAPayBL.setDueFeeType("1");         
        mLJAPayBL.setSaleChnl(mLJSPayBL.getSaleChnl());
        mLJAPayBL.setAgentCom(mLCGrpContSchema.getAgentCom());
        mLJAPayBL.setMarketType(mLJSPayBL.getMarketType());

        //最后将暂交费和　暂交费分类信息　置核销标志
        System.out.println(" 续保核销时，更新LJTempFee ");
        for (int i = 1; i <= mLJTempFeeSet.size(); i++) {
            LJTempFeeBL tLJTempFeeBL = new LJTempFeeBL();
            tLJTempFeeBL.setSchema(mLJTempFeeSet.get(i));
            tLJTempFeeBL.setConfFlag("1");
            tLJTempFeeBL.setConfDate(CurrentDate);
            mLJTempFeeNewSet.add(tLJTempFeeBL);
        }
        System.out.println(" 续保核销时，更新LJTempFeeClass ");
        for (int i = 1; i <= mLJTempFeeClassSet.size(); i++) {
            LJTempFeeClassBL tLJTempFeeClassBL = new LJTempFeeClassBL();
            tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i));
            tLJTempFeeClassBL.setConfFlag("1");
            tLJTempFeeClassBL.setConfDate(PubFun.getCurrentDate());
            mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);
        }
        //设置应收总表备份表
        mLJSPayBSchema.setDealState("1");
        mLJSPayBSchema.setConfFlag("1");
        mLJSPayBSchema.setModifyDate(PubFun.getCurrentDate());
        mLJSPayBSchema.setModifyTime(PubFun.getCurrentTime());
        
        tSBql = new StringBuffer(128);
        tSBql.append(
                "UPDATE  LJSPayPerSonB SET  DealState ='1' , ConfFlag='1'")
                .append(" , ModifyDate ='")
                .append(PubFun.getCurrentDate())
                .append("' , ModifyTime ='")
                .append(PubFun.getCurrentTime())
                .append("'")
                .append(" where GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' and GetNoticeNo='")
                .append(mLJSPayBL.getGetNoticeNo())
                .append("'")
                ;
        mSqlLjsPayPersonB = tSBql.toString();
        System.out.println(mSqlLjsPayPersonB);
        System.out.println(" 续保核销时，更新LJSPayPerSonB");

        tSBql = new StringBuffer(128);
        tSBql.append("DELETE from LJSPayPerSon where GrpContNo='")
        	.append(mLCGrpContSchema.getGrpContNo())
        	.append("'");
        mSqlLjsPayPerson = tSBql.toString();        
        
        
        //设置应收集体表备份表
        System.out.println(" 续保核销时，更新LJSPayGrpBS");
        for (int s = 1; s <= tLJSPayGrpBSet.size(); s++) {
            LJSPayGrpBSchema tLJSPayGrpBSchema = new LJSPayGrpBSchema();
            tLJSPayGrpBSchema = tLJSPayGrpBSet.get(s);
            tLJSPayGrpBSchema.setDealState("1");
            tLJSPayGrpBSchema.setConfFlag("1");
            tLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime());
            mLJSPayGrpBSet.add(tLJSPayGrpBSchema);
        }
        return true;
    }

    /**
     * 投保人帐户余额抵扣生效
     * @param yelLJAPayGrpBL LJAPayGrpBL
     */
    private void dealAppAcc(LJAPayGrpBL yelLJAPayGrpBL)
    {
        LCAppAccTraceDB tLCAppAccTraceDB = new LCAppAccTraceDB();
        tLCAppAccTraceDB.setCustomerNo(yelLJAPayGrpBL.getAppntNo());
        tLCAppAccTraceDB.setOtherNo(yelLJAPayGrpBL.getGrpContNo());
        tLCAppAccTraceDB.setBakNo(yelLJAPayGrpBL.getGetNoticeNo());
        LCAppAccTraceSet set = tLCAppAccTraceDB.query();
        if(set.size() == 0)
        {
            return;
        }
       double money=0;
       for(int j=1;j<=set.size();j++)
       {
	        LCAppAccTraceSchema schema = set.get(j);
	        if(schema.getMoney()>0)
	        {
	        	money=schema.getMoney(); //账户余额更新成正记录，否则为零
	        } 
       }
       
       for(int i=1;i<=set.size();i++)
       {    	
	        LCAppAccTraceSchema schema = set.get(i);
	        if(schema.getMoney()>0)
	        {
	        	money=schema.getMoney(); //账户余额更新成正记录，否则为零
	        }
	        schema.setAccBala(money);
	        schema.setState("1");
	        mAccMap.put(schema, "UPDATE");
       }

        String sql = "  update LCAppAcc "
                     + "set accBala =  " + money + ", "
                     + " accGetMoney =  " + money + ", "
                     + "   state = '1' "
                     + "where customerNo = '" + set.get(1).getCustomerNo() + "' ";
        mAccMap.put(sql, "UPDATE");
    }
            

    /**
     * 加入到催收核销日志表数据
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealUrgeLog(String pmDealState, String pmReason,
                                String pmOpreat) {

        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                LCUrgeVerifyLogSchema();
        //加到催收核销日志表
        if (pmOpreat.equals("INSERT")) {
            tLCUrgeVerifyLogSchema.setSerialNo(mserNo);
            tLCUrgeVerifyLogSchema.setRiskFlag("1");//1：团险,2：个险
            tLCUrgeVerifyLogSchema.setOperateType("2"); //1：续保催收操作,2：续保核销操作
            tLCUrgeVerifyLogSchema.setOperateFlag(mOperateFlag); //1：个案操作,2：批次操作
            tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
            tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成

            tLCUrgeVerifyLogSchema.setContNo(mLCGrpContSchema.getGrpContNo());
            tLCUrgeVerifyLogSchema.setMakeDate(PubFun.getCurrentDate());
            tLCUrgeVerifyLogSchema.setMakeTime(PubFun.getCurrentTime());
            tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
            tLCUrgeVerifyLogSchema.setErrReason(pmReason);
        } else {
            LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new
                    LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mserNo);
            tLCUrgeVerifyLogDB.setOperateType("2");
            tLCUrgeVerifyLogDB.setOperateFlag(mOperateFlag);
            tLCUrgeVerifyLogDB.setRiskFlag("1");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
            if (!(tLCUrgeVerifyLogSet == null) &&
                tLCUrgeVerifyLogSet.size() > 0) {
                tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                tLCUrgeVerifyLogSchema.setDealState(pmDealState);
                tLCUrgeVerifyLogSchema.setErrReason(pmReason);
            } else {
                return false;
            }

        }

        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
            return false;
        }
        return true;
    }


    private boolean prepareData() {
        mInputData = new VData();
        try {
            mLJAPaySchema.setSchema(mLJAPayBL);
            mLJSPaySchema.setSchema(mLJSPayBL);
            // qulq modify 2007-3-16 如果是续保保单就不用更新原保单信息。
            map.put(mLCPremNewSet, "UPDATE");
            map.put(mLJAPayPersonSet, "INSERT");
            map.put(mLCDutyNewSet, "UPDATE");
            map.put(mLCPolNewSet, "UPDATE");
            map.put(mLCGrpPolNewSet, "UPDATE");
            map.put(mLJAPayGrpSet, "INSERT");
            map.put(mLCGrpContSchema, "UPDATE");
            map.put(mLJAPaySchema, "INSERT");
            map.put(mLJTempFeeNewSet, "UPDATE");
            map.put(mLJTempFeeClassNewSet, "UPDATE");
            map.put(mLJSPaySchema, "DELETE");
            map.put(mLJSPayBSchema, "UPDATE");
            map.put(mSqlLjsPayPersonB, "UPDATE");
            map.put(mSqlLjsPayPerson, "DELETE");
            map.put(mLJSPayGrpBSet, "UPDATE");
            map.put(mLJSPayGrpSet, "DELETE");
            map.add(mAccMap);

            mInputData.add(map);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 得到原团体险种信息
     * @param tLCPolSchema LCPolSchema:当前险种信息
     * @return LCPolSchema：原险种信息
     */
    private LCGrpPolSchema getOldLCGrpPolSchema(LCGrpPolSchema tLCGrpPolSchema)
    {

        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LCGrpPol a, LCRnewStateLog b ")
            .append("where a.GrpPolNo = b.GrpPolNo ")
            .append("   and b.newGrpPolNo = '")
            .append(tLCGrpPolSchema.getGrpPolNo()).append("' ");
        System.out.println(sql.toString());

        LCGrpPolSet set = new LCGrpPolDB().executeQuery(sql.toString());
        if(set.size() == 0)
        {
            mErrors.addOneError("险种" + tLCGrpPolSchema.getRiskCode()
                                + "是续保险种，但是没有查询到原险种信息。");
            return null;
        }
        return set.get(1);
    }
    

    /**
     * 得到原险种信息
     * @param tLCPolSchema LCPolSchema:当前险种信息
     * @return LCPolSchema：原险种信息
     */
    private LCPolSchema getOldLCPolSchema(LCPolSchema tLCPolSchema){

        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LCPol a, LCRnewStateLog b ")
            .append("where a.polNo = b.polNo ")
            .append("   and a.renewCount = b.renewCount -1 ")  //原险种续保次数比续保临时数据少1次
            .append("   and b.newPolNo = '")
            .append(tLCPolSchema.getPolNo()).append("' ");
        System.out.println(sql.toString());

        LCPolSet set = new LCPolDB().executeQuery(sql.toString());
        if(set.size() == 0)
        {
            mErrors.addOneError("险种" + tLCPolSchema.getRiskCode()
                                + "是续保险种，但是没有查询到原险种信息。");
            return null;
        }
        return set.get(1);
    }
    
    private MMap lockLJAGet(LCGrpContSchema mLCGrpContSchema){    	   	
        MMap tMMap = null;
        /**锁定标志"TX"*/
        String tLockNoType = "TX";
        /**锁定时间*/
        String tAIS = "3600";
        System.out.println("设置了60分的解锁时间");
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", mLCGrpContSchema.getGrpContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
       
        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(tGI);
        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        
        if (tMMap == null)
        {           	
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }

    /**
     * 锁定动作
     * @param cGetNoticeNo
     * @return 
     */
    private MMap lockLJAPay(String cGetNoticeNo)
    {
        MMap tMMap = null;
        /**实收核销标志"HX"*/
        String tLockNoType = "HX";
        /**锁定有效时间*/
        String tAIS = "3600";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cGetNoticeNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }

    /**
     * 往帐户里增钱
     * @param accBala double
     * @return boolean
     */
    private boolean addAccBala(double accBala)
    {
        AppAcc tAppAcc = new AppAcc();

        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
        tLCAppAccTraceSchema.setAccType("1");
        tLCAppAccTraceSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLCAppAccTraceSchema.setBakNo(mLJSPayBL.getGetNoticeNo());
        tLCAppAccTraceSchema.setOtherType("1");
        tLCAppAccTraceSchema.setMoney(accBala);
        tLCAppAccTraceSchema.setOperator(tGI.Operator);
        MMap tMap = tAppAcc.accShiftToXSYIJ(tLCAppAccTraceSchema);
        if(tMap == null)
        {
            mErrors.copyAllErrors(tAppAcc.mErrors);
            return false;
        }
        map.add(tMap);

        return true;
    }
    
    private boolean checkWLPYH(){
    	 ExeSQL tExeSQL = new ExeSQL();
    	 String sqlWLP = "select 1 from ljspayperson where grpcontno='"+ mLJSPayBL.getOtherNo() +"' and getnoticeno='"+ mLJSPayBL.getGetNoticeNo() +"' and riskcode in ('162601','162801') and paytype='ZC'";
    	 String RHX = tExeSQL.getOneValue(sqlWLP);    
    	 if ("1".equals(RHX)) {
    		 String mContInfosSQL = "select paytodate,current date,paytodate - 3 years from lcgrppol where grpcontno='" + mLJSPayBL.getOtherNo() + "'";
    		 SSRS tContInfosSSRS = null;
    		 tContInfosSSRS = tExeSQL.execSQL(mContInfosSQL);
    		 String date = tContInfosSSRS.GetText(1, 1);
    		 String date2 = tContInfosSSRS.GetText(1, 2);
    		 String date3 = tContInfosSSRS.GetText(1, 3);
    		 SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    		 // 判断是否打过9折
    		 String sqlljs = "select sum(sumDuePayMoney) from LJSPayPersonB where grpcontno = '"
    			 + mLJSPayBL.getOtherNo()
    			 + "' and paytype = 'ZC' and getnoticeno='"
    			 + mLJSPayBL.getGetNoticeNo()
    			 + "' and riskcode in ('162601','162801')";
    		 double ljsprem = Double.parseDouble(tExeSQL.getOneValue(sqlljs));
    		 double WLPRate = 0.9;
 	    	 double GYHRate = getGYHRate();
    		 double LjsMoney = ljsprem / WLPRate / GYHRate;
    		 BigDecimal money = new BigDecimal(LjsMoney);
    		 money = money.setScale(2, BigDecimal.ROUND_HALF_UP);
    		 double money1 = money.doubleValue() + 0.01;
    		 double money2 = money.doubleValue() - 0.01;
    		 double money3 = money.doubleValue();
    		 String sqlprem = "select sum(prem) from lcpol where polno in (select newpolno from LCRnewStateLog where state !='6' and polno in (select polno from ljspayperson where grpcontno = '"
    			 + mLJSPayBL.getOtherNo()
    			 + "' and riskcode in ('162601','162801') and paytype='ZC'))";
    		 String resultprem = new ExeSQL().getOneValue(sqlprem);
    		 if (!"".equals(resultprem) && !"null".equals(resultprem)) {
    			 double sumpol = Double.parseDouble(resultprem);
    			 if (money1 == sumpol || money2 == sumpol || money3 == sumpol) {
    				 try {
    					 Date dt1 = df.parse(date);
    					 Date dt2 = df.parse(date2);
    					 if (dt1.after(dt2)) {
    						 CError tError = new CError();
    						 tError.moduleName = "GRnewFinUrgeVerifyBL";
    						 tError.functionName = "checkWLPYH";
    						 tError.errorMessage = "未到满期日，暂时无法核销";
    						 this.mErrors.addOneError(tError);
    						 return false;
    					 }
    				 } catch (ParseException e) {
    					 e.printStackTrace();
    				 }
    			 }
    		 }
    	 }

    	return true;
    }
    
    /**
     * 获取家庭单团体优惠费率
     * @return
     */
    private double getGYHRate(){
    	double rate = 1;
    	String tlcpolSQL = "select * from lcpol where grpcontno = '" + mLJSPayBL.getOtherNo() + "'";
    	LCPolSet tLCPolSet = new LCPolSet();
    	LCPolDB tLCPolDB = new LCPolDB();
    	tLCPolSet = tLCPolDB.executeQuery(tlcpolSQL);
    	int peoples = tLCPolSet.size();
    	if(peoples<3){
    		return 1;
    	}
    	for(int i = 1;i<tLCPolSet.size();i++){
    		String birthday = tLCPolSet.get(i).getInsuredBirthday();
    		int year = PubFun.calInterval(birthday,tLCPolSet.get(i).getEndDate(),"Y");
    		if(year >= 75){
    			peoples--;
    		}
    	}
        if(peoples>=3){
        	rate = 0.9;
        }       
        
    	return rate;
    }
    
    
    
    
}
