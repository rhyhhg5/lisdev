package com.sinosoft.lis.operfee;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author GUOXIANG
 * @version 1.0
 */

import java.util.*;
import java.text.*;
import java.lang.*;
import java.io.*;
import com.f1j.ss.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bl.LCContBL;


public class ShowDueFeeList {

  public CErrors mErrors=new CErrors();
  private VData mResult = new VData();

  private GlobalInput mGlobalInput =new GlobalInput() ;
  private String mOperate="";
  private String mManageCom="";
  private String mStartDate="";
  private String mEndDate="";
  private String mPolType="";
  private TransferData mTransferData=new TransferData();
  private BookModelImpl m_book = new BookModelImpl();
  private String  mFileModeDesc = "DueFeeMode.xls"; //描述的模版名称
  private String  mFilePathDesc = "CreatListPath";  //描述的文件路径
  private String  mModeRealPath="";
  private String  mFilePath     = "";  //通过描述得到的文件路径
  private String  mFileName    = "";  //要生成的文件名
  private String  mRelationFlag="0"; //续期和续保是否关联标记 0-不相关 1-相关
  private int mCurrentRow=1; //行数
  private int mCount=100; //每次循环处理的纪录数


  public ShowDueFeeList()
  {
  }

  public static void main(String[] args)
  {
      ShowDueFeeList tShowDueFeeList =new ShowDueFeeList();
      VData tVData=new VData();
      TransferData tTransferData =new TransferData();
      tTransferData.setNameAndValue("ManageCom","86");
      tTransferData.setNameAndValue("StartDate","2004-10-1");
      tTransferData.setNameAndValue("EndDate","2007-10-11");
      tTransferData.setNameAndValue("PolType","0");
      GlobalInput tGI=new GlobalInput();
      tGI.ComCode="86";
      tGI.Operator="001";
      tVData.add(tTransferData);
      tVData.add(tGI);
      tShowDueFeeList.submitData(tVData,"");

  }

  /**
   * 传输数据的公共方法
   * @param cInputData
   * @param cOperate
   * @return
   */
  public boolean submitData(VData cInputData, String cOperate) {

      mOperate = cOperate;

      if( getInputData(cInputData)==false )
          return false;

      if(dealData()==false)
          return false;

      return true;

  }

  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
      mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
      mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);

      if(mTransferData==null||mGlobalInput==null)
      {
          buildError("getInputData","没有得到传入的数据");
          return false;
      }
      mManageCom=(String)mTransferData.getValueByName("ManageCom");
      mStartDate=(String)mTransferData.getValueByName("StartDate");
      mEndDate=(String)mTransferData.getValueByName("EndDate");
      mPolType=(String)mTransferData.getValueByName("PolType");//在职-1/孤儿单-0
      mModeRealPath=(String)mTransferData.getValueByName("ModeRealPath");
      if(mStartDate==null||mEndDate==null||mStartDate.equals("")||mEndDate.equals(""))
      {
          buildError("getInputData","没有得到起始日期和终止日期");
          return false;
      }
      int d=PubFun.calInterval(mStartDate,mEndDate,"D");
      if(d<0)
      {
          buildError("getInputData","起始日期不能小于终止日期");
          return false;
      }
      if(mPolType==null||mPolType.equals(""))
      {
          buildError("getInputData","必须录入在职或孤儿单标记");
          return false;
      }
      if(mManageCom==null||mManageCom.equals("")) mManageCom=mGlobalInput.ComCode;

      return true;
  }

  public VData getResult()
  {
      return this.mResult;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError( );
      cError.moduleName = "ShowDueFeeList";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      this.mErrors.addOneError(cError);
  }

  /**
   * 准备所有要打印的数据
   * @return
   */
  private boolean dealData()
  {
      if(checkDesc()==false)
          return false;

      try
      {
          //得到指定路径下的模版文件--其实还是不知道如何直接得到excel文件
          m_book.read(mModeRealPath+mFilePath+mFileModeDesc,new com.f1j.ss.ReadParams());
          //m_book.read("E:/temp/"+mFileModeDesc ,new com.f1j.ss.ReadParams());
          m_book.setSheetSelected(0,true);
          m_book.setCol(0);

          //处理需要续期缴费的主险保单
          //杨红于2005-07-26说明：注意要考虑多个主险的情况，（这里只考虑了单个主险！以后不易 扩展）
         // if(dealDueMainPol()==false)
           if(!dealMulContData())
              return false;

          //如果主险续期和附加险自动续保关联并且主险自动续保记入主险续期
          /*
          if(mRelationFlag.equals("2"))
          {
              //处理需要续保的主险保单
              if(dealRnewMainPol()==false)
                  return false;
              //处理需要续保的附加险，其主险不需要续期缴费和续保
              if(dealRnewSpecSubPol()==false)
                  return false;
          }*/

          //生成文件
          m_book.write(mModeRealPath+mFilePath+mFileName,new com.f1j.ss.WriteParams(com.f1j.ss.BookModelImpl.eFileExcel97));
          //m_book.write("E:/temp/"+mFileName,new com.f1j.ss.WriteParams(com.f1j.ss.BookModelImpl.eFileExcel97));
      }
      catch(Exception ex)
      {
          buildError("FormateFile","操作失败："+ex);
          return false;
      }

      return true;
  }

  /**
   * 得到文件名
   * @return
   */
  private String getFileName()
  {
      String StartDate[]=PubFun.split(mStartDate,"-");
      String EndDate[]=PubFun.split(mEndDate,"-");
      //文件名=DueFeeList+"_"+操作员代码+"_"+起始年月日+终止年月日+.xls.z
      String filename="DueFeeList_"+mGlobalInput.Operator+"_";
      filename= filename+StartDate[0];
      if(StartDate[1].length()==1) filename= filename+"0";
      filename= filename+StartDate[1];
      if(StartDate[2].length()==1) filename= filename+"0";
      filename= filename+StartDate[2];
      filename= filename+"_";
      filename= filename+EndDate[0];
      if(EndDate[1].length()==1) filename= filename+"0";
      filename= filename+EndDate[1];
      if(EndDate[2].length()==1) filename= filename+"0";
      filename= filename+EndDate[2];
      filename= filename+"_";
      filename= filename+mPolType;
      filename= filename+".xls.z";
      System.out.println("生成文件名:"+filename);
      return filename;
  }

  /**
   * 添加每个保单的信息-针对续期主险
   * @param tLCPolSchema
   * @param t_book
   * @param n
   * @return
   */
  private boolean dealSinglePol(LCPolSchema tLCPolSchema)
  {
      int t=0;
      int subNum=0;
      String AgentName="";//业务员姓名
      String InsuName="";//被保人姓名
      String AppName="";   //  客户姓名
      String SamePerson="N";
      try
      {
          AgentName=ChangeCodetoName.getAgentName(tLCPolSchema.getAgentCode()).trim();
          AppName=tLCPolSchema.getAppntName().trim();
          InsuName=tLCPolSchema.getInsuredName().trim();
          if(AgentName.equals(AppName)&&AgentName.equals(InsuName))
          {
              SamePerson="Y";
          }

          //如有数据错误，可以写在excel里
          m_book.setEntry(mCurrentRow,t+0,tLCPolSchema.getManageCom());//管理机构
          m_book.setEntry(mCurrentRow,t+1,AgentName);//业务员姓名
          m_book.setEntry(mCurrentRow,t+2,tLCPolSchema.getAgentCode());//业务员代码
          String AgentBranch=findAgentBranch(tLCPolSchema.getAgentGroup());
          m_book.setEntry(mCurrentRow,t+3,AgentBranch);//业务员组别
          m_book.setEntry(mCurrentRow,t+4,AppName);//客户姓名
          m_book.setEntry(mCurrentRow,t+5,tLCPolSchema.getPolNo());//保单号
          m_book.setEntry(mCurrentRow,t+6,tLCPolSchema.getPaytoDate());//缴费日期
          String PayMode=findPayMode(tLCPolSchema.getPolNo(),tLCPolSchema.getPayLocation());
          m_book.setEntry(mCurrentRow,t+7,String.valueOf(PayMode));//缴费方式

          m_book.setEntry(mCurrentRow,t+23,ChangeCodetoName.getSaleChnl(tLCPolSchema.getSaleChnl()));//销售渠道
          m_book.setEntry(mCurrentRow,t+24,SamePerson);//代理人和投保人和被保人是否同一人标记
          //险种信息
          //得到主险保费
          double mainPolPrem=0;
          mainPolPrem=getMainPolPrem(tLCPolSchema.getPolNo());
          LJSPayDB tLJSPayDB=new LJSPayDB();
          tLJSPayDB.setOtherNo(tLCPolSchema.getPolNo());//这里对应的是合同号
          tLJSPayDB.setOtherNoType("2");
          LJSPaySet tLJSPaySet=tLJSPayDB.query();
          if(tLJSPaySet==null||tLJSPaySet.size()==0)
          {
              mainPolPrem=0;
          }

          m_book.setEntry(mCurrentRow,t+8,ChangeCodetoName.getRiskName(tLCPolSchema.getRiskCode()));//主险险种
          m_book.setEntry(mCurrentRow,t+9,String.valueOf(mainPolPrem));//主险保费
          double sumprem=0.0;//保费合计
          sumprem=sumprem+mainPolPrem;
          //如果主险续期和附加险自动续保关联-查询需要续保的附加险
         /*杨红于2005-07-26将该部分注销，以后 续保时 再添加！
           if(!mRelationFlag.equals("0"))
          {
              LCPolSet tLCPolSet=findSubPol(tLCPolSchema);
              if(tLCPolSet.size()>0)
              {
                  String prem="0";

                  //如果多于一条，则在excel的下一行的同一列位置显示，同时mSubNum增长
                  for(int i=1;i<=tLCPolSet.size();i++)
                  {
                      String SQL_SubPol="select SumDuePayMoney from LJSPayPerson where riskcode='"+tLCPolSet.get(i).getRiskCode()+"' and PayType='ZC' and PayTypeFlag='1'" ;
                      SQL_SubPol=SQL_SubPol+"and getnoticeno in (select getnoticeno from ljspay where othernotype='2' and otherno='"+tLCPolSchema.getPolNo()+"')";
                      ExeSQL tExeSQL = new ExeSQL();
                      SSRS tSSRS=tExeSQL.execSQL(SQL_SubPol);
                      if(tSSRS!=null&&tSSRS.getMaxRow()>0)
                      {
                          prem=tSSRS.GetText(1,1);
                      }
                      m_book.setEntry(mCurrentRow+i-1,t+10,ChangeCodetoName.getRiskName(tLCPolSet.get(i).getRiskCode()));//附险险种
                      m_book.setEntry(mCurrentRow+i-1,t+11,prem);//附险保费
                      sumprem=sumprem+Double.parseDouble(prem) ;
                  }
                  subNum=subNum+tLCPolSet.size()-1;
              }
          }*/

          m_book.setEntry(mCurrentRow,t+12,String.valueOf(sumprem));//保费合计
          //投保人信息
          LCAppntIndDB tLCAppntIndDB =new LCAppntIndDB();
          tLCAppntIndDB.setPolNo(tLCPolSchema.getPolNo());
          tLCAppntIndDB.setCustomerNo(tLCPolSchema.getAppntNo());
          if(tLCAppntIndDB.getInfo()==true)
          {
              m_book.setEntry(mCurrentRow,t+13,tLCAppntIndDB.getPostalAddress());//收费地址
              m_book.setEntry(mCurrentRow,t+14,tLCAppntIndDB.getZipCode());//邮编
              m_book.setEntry(mCurrentRow,t+15,tLCAppntIndDB.getPhone());//电话
              m_book.setEntry(mCurrentRow,t+22,ChangeCodetoName.getSexName(tLCAppntIndDB.getSex()));//性别

          }
          m_book.setEntry(mCurrentRow,t+16,tLCPolSchema.getInsuredName());//被保人姓名
          String BankInfo[]=findBankInfo(tLCPolSchema);
          m_book.setEntry(mCurrentRow,t+17,BankInfo[0]);//银行帐号
          m_book.setEntry(mCurrentRow,t+18,ChangeCodetoName.getBankCodeName(BankInfo[1]));//银行编码
          m_book.setEntry(mCurrentRow,t+19,BankInfo[2]);//户名

          LAAgentDB tLAAgentDB=new LAAgentDB();
          tLAAgentDB.setAgentCode(tLCPolSchema.getAgentCode());
          String AgentTel="";
          if(tLAAgentDB.getInfo()==true)
          {
              AgentTel=tLAAgentDB.getPhone();
              if(AgentTel==null||AgentTel.equals(""))
                  AgentTel=tLAAgentDB.getMobile();
              if(AgentTel==null)
                  AgentTel="";
          }
          m_book.setEntry(mCurrentRow,t+20,AgentTel);//代理人电话
          LJAPayDB tLJAPayDB=new LJAPayDB();
          tLJAPayDB.setIncomeNo(tLCPolSchema.getPolNo());
          tLJAPayDB.setIncomeType("2");
          LJAPaySet tLJAPaySet=tLJAPayDB.query();
          m_book.setEntry(mCurrentRow,t+21,String.valueOf(tLJAPaySet.size()));//交费期数



      }
      catch(Exception ex)
      {
          try
          {
              m_book.setEntry(mCurrentRow,t+0,"***处理保单"+tLCPolSchema.getPolNo()+" 出错："+ex);//管理机构
              mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
          }
          catch(Exception ex2)
          {
              buildError("dealSinglePol",ex.toString());
              return false;
          }
          return true;
      }
      mCurrentRow=mCurrentRow+subNum;//如果有超过1个以上附加险，添加新的行存放第二个（以上）附加险信息
      mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
      return true;
  }

  /**
   * 添加每个保单的信息-针对自动续保主险
   * @param tLCPolSchema
   * @param t_book
   * @param n
   * @return
   */
  private boolean dealSingleRnewPol(LCPolSchema tLCPolSchema)
  {
      int t=0;
      int subNum=0;
      String AgentName="";//业务员姓名
      String InsuName="";//被保人姓名
      String AppName="";   //  客户姓名
      String SamePerson="N";
      try
      {
          AgentName=ChangeCodetoName.getAgentName(tLCPolSchema.getAgentCode()).trim();
          AppName=tLCPolSchema.getAppntName().trim();
          InsuName=tLCPolSchema.getInsuredName().trim();
          if(AgentName.equals(AppName)&&AgentName.equals(InsuName))
          {
              SamePerson="Y";
          }
          //如有数据错误，可以写在excel里
          m_book.setEntry(mCurrentRow,t+0,tLCPolSchema.getManageCom());//管理机构
          m_book.setEntry(mCurrentRow,t+1,AgentName);//业务员姓名
          m_book.setEntry(mCurrentRow,t+2,tLCPolSchema.getAgentCode());//业务员代码
          String AgentBranch=findAgentBranch(tLCPolSchema.getAgentGroup());
          m_book.setEntry(mCurrentRow,t+3,AgentBranch);//业务员组别
          m_book.setEntry(mCurrentRow,t+4,tLCPolSchema.getAppntName());//客户姓名
          m_book.setEntry(mCurrentRow,t+5,tLCPolSchema.getPolNo());//保单号
          m_book.setEntry(mCurrentRow,t+6,tLCPolSchema.getPaytoDate());//缴费日期
          String PayMode=findPayMode(tLCPolSchema.getPolNo(),tLCPolSchema.getPayLocation());
          m_book.setEntry(mCurrentRow,t+7,String.valueOf(PayMode));//缴费方式

          m_book.setEntry(mCurrentRow,t+23,ChangeCodetoName.getSaleChnl(tLCPolSchema.getSaleChnl()));//销售渠道
          m_book.setEntry(mCurrentRow,t+24,SamePerson);//代理人和投保人和被保人是否同一人标记

          //险种信息
          //得到主险保费
          double mainPolPrem=0;
          //mainPolPrem=getMainPolPrem(tLCPolSchema.getPolNo());
          String tGetNoticeNo="";
          String SQL_SubPol="select GetNoticeNo,SumDuePayMoney from ljspay where otherno='"+tLCPolSchema.getPolNo()+"' and othernotype='2'" ;
          ExeSQL tExeSQL = new ExeSQL();
          SSRS tSSRS=tExeSQL.execSQL(SQL_SubPol);
          if(tSSRS!=null&&tSSRS.getMaxRow()>0)
          {
              tGetNoticeNo=tSSRS.GetText(1,1);
              mainPolPrem=Double.parseDouble(tSSRS.GetText(1,2));
          }
          m_book.setEntry(mCurrentRow,t+8,ChangeCodetoName.getRiskName(tLCPolSchema.getRiskCode()));//主险险种
          m_book.setEntry(mCurrentRow,t+9,String.valueOf(mainPolPrem));//主险保费
          double sumprem=0.0;//保费合计
          sumprem=sumprem+mainPolPrem;

          LCPolSet tLCPolSet=findSubPol(tLCPolSchema);
          if(tLCPolSet.size()>0)
          {
              String prem="0";
              //如果多于一条，则在excel的下一行的同一列位置显示，同时mSubNum增长
              for(int i=1;i<=tLCPolSet.size();i++)
              {
                  SQL_SubPol="select SumDuePayMoney from LJSPayPerson where riskcode='"+tLCPolSet.get(i).getRiskCode()+"' and PayType='ZC' and PayTypeFlag='1'" ;
                  SQL_SubPol=SQL_SubPol+" and GetNoticeNo='"+tGetNoticeNo+"'";
                  tExeSQL = new ExeSQL();
                  tSSRS=tExeSQL.execSQL(SQL_SubPol);
                  if(tSSRS!=null&&tSSRS.getMaxRow()>0)
                  {
                      prem=tSSRS.GetText(1,1);
                  }
                  m_book.setEntry(mCurrentRow+i-1,t+10,ChangeCodetoName.getRiskName(tLCPolSet.get(i).getRiskCode()));//附险险种
//                  m_book.setEntry(mCurrentRow+i-1,t+11,String.valueOf(tLCPolSet.get(i).getPrem()));//附险保费
//                  sumprem=sumprem+tLCPolSet.get(i).getPrem();
                  m_book.setEntry(mCurrentRow+i-1,t+11,prem);//附险保费
                  sumprem=sumprem+Double.parseDouble(prem) ;
              }
              subNum=subNum+tLCPolSet.size()-1;
          }

          m_book.setEntry(mCurrentRow,t+12,String.valueOf(sumprem));//保费合计
          //投保人信息
          LCAppntIndDB tLCAppntIndDB =new LCAppntIndDB();
          tLCAppntIndDB.setPolNo(tLCPolSchema.getPolNo());
          tLCAppntIndDB.setCustomerNo(tLCPolSchema.getAppntNo());
          if(tLCAppntIndDB.getInfo()==true)
          {
              m_book.setEntry(mCurrentRow,t+13,tLCAppntIndDB.getPostalAddress());//收费地址
              m_book.setEntry(mCurrentRow,t+14,tLCAppntIndDB.getZipCode());//邮编
              m_book.setEntry(mCurrentRow,t+15,tLCAppntIndDB.getPhone());//电话
              m_book.setEntry(mCurrentRow,t+22,ChangeCodetoName.getSexName(tLCAppntIndDB.getSex()));//性别
          }
          m_book.setEntry(mCurrentRow,t+16,tLCPolSchema.getInsuredName());//被保人姓名
          String BankInfo[]=findBankInfo(tLCPolSchema);
          m_book.setEntry(mCurrentRow,t+17,BankInfo[0]);//银行帐号
          m_book.setEntry(mCurrentRow,t+18,ChangeCodetoName.getBankCodeName(BankInfo[1]));//银行编码
          m_book.setEntry(mCurrentRow,t+19,BankInfo[2]);//户名

          LAAgentDB tLAAgentDB=new LAAgentDB();
          tLAAgentDB.setAgentCode(tLCPolSchema.getAgentCode());
          String AgentTel="";
          if(tLAAgentDB.getInfo()==true)
          {
              AgentTel=tLAAgentDB.getPhone();
              if(AgentTel==null||AgentTel.equals(""))
                  AgentTel=tLAAgentDB.getMobile();
              if(AgentTel==null)
                  AgentTel="";
          }
          m_book.setEntry(mCurrentRow,t+20,AgentTel);//代理人电话
          LJAPayDB tLJAPayDB=new LJAPayDB();
          tLJAPayDB.setIncomeNo(tLCPolSchema.getPolNo());
          tLJAPayDB.setIncomeType("2");
          LJAPaySet tLJAPaySet=tLJAPayDB.query();
          m_book.setEntry(mCurrentRow,t+21,String.valueOf(tLJAPaySet.size()));//交费期数

      }
      catch(Exception ex)
      {
          try
          {
              m_book.setEntry(mCurrentRow,t+0,"***处理保单"+tLCPolSchema.getPolNo()+" 出错："+ex);//管理机构
              mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
          }
          catch(Exception ex2)
          {
              buildError("dealSinglePol",ex.toString());
              return false;
          }
          return true;
      }
      mCurrentRow=mCurrentRow+subNum;//如果有超过1个以上附加险，添加新的行存放第二个（以上）附加险信息
      mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
      return true;
  }


  /**
   * 处理单个需要自动续保的附加险的清单数据
   * @param tLCPolSchema
   * @return
   */
  private boolean dealSingleSubPol(LCPolSchema tLCPolSchema)
  {
      int t=0;
      String AgentName="";//业务员姓名
      String InsuName="";//被保人姓名
      String AppName="";   //  客户姓名
      String SamePerson="N";
      try
      {
          AgentName=ChangeCodetoName.getAgentName(tLCPolSchema.getAgentCode()).trim();
          AppName=tLCPolSchema.getAppntName().trim();
          InsuName=tLCPolSchema.getInsuredName().trim();
          if(AgentName.equals(AppName)&&AgentName.equals(InsuName))
          {
              SamePerson="Y";
          }
          //如有数据错误，可以写在excel里
          m_book.setEntry(mCurrentRow,t+0,tLCPolSchema.getManageCom());//管理机构
          m_book.setEntry(mCurrentRow,t+1,AgentName);//业务员姓名
          m_book.setEntry(mCurrentRow,t+2,tLCPolSchema.getAgentCode());//业务员代码
          String AgentBranch=findAgentBranch(tLCPolSchema.getAgentGroup());
          m_book.setEntry(mCurrentRow,t+3,AgentBranch);//业务员组别
          m_book.setEntry(mCurrentRow,t+4,tLCPolSchema.getAppntName());//客户姓名
          m_book.setEntry(mCurrentRow,t+5,tLCPolSchema.getPolNo());//保单号
          m_book.setEntry(mCurrentRow,t+6,tLCPolSchema.getPaytoDate());//缴费日期
          String PayMode=findPayMode(tLCPolSchema.getPolNo(),tLCPolSchema.getPayLocation());
          m_book.setEntry(mCurrentRow,t+7,String.valueOf(PayMode));//缴费方式

          m_book.setEntry(mCurrentRow,t+23,ChangeCodetoName.getSaleChnl(tLCPolSchema.getSaleChnl()));//销售渠道
          m_book.setEntry(mCurrentRow,t+24,SamePerson);//代理人和投保人和被保人是否同一人标记
          //险种信息
          //m_book.setEntry(mCurrentRow,t+8,ChangeCodetoName.getRiskName(tLCPolSchema.getRiskCode()));//主险险种
          //m_book.setEntry(mCurrentRow,t+9,String.valueOf(tLCPolSchema.getPrem()));//主险保费
          double sumprem=0.0;//保费合计
          m_book.setEntry(mCurrentRow,t+10,ChangeCodetoName.getRiskName(tLCPolSchema.getRiskCode()));//附险险种
          m_book.setEntry(mCurrentRow,t+11,String.valueOf(tLCPolSchema.getPrem()));//附险保费
          sumprem=sumprem+tLCPolSchema.getPrem();
          m_book.setEntry(mCurrentRow,t+12,String.valueOf(sumprem));//保费合计

          //投保人信息
          LCAppntIndDB tLCAppntIndDB =new LCAppntIndDB();
          tLCAppntIndDB.setPolNo(tLCPolSchema.getPolNo());
          tLCAppntIndDB.setCustomerNo(tLCPolSchema.getAppntNo());
          if(tLCAppntIndDB.getInfo()==true)
          {
              m_book.setEntry(mCurrentRow,t+13,tLCAppntIndDB.getPostalAddress());//收费地址
              m_book.setEntry(mCurrentRow,t+14,tLCAppntIndDB.getZipCode());//邮编
              m_book.setEntry(mCurrentRow,t+15,tLCAppntIndDB.getPhone());//电话
              m_book.setEntry(mCurrentRow,t+22,ChangeCodetoName.getSexName(tLCAppntIndDB.getSex()));//性别
          }
          m_book.setEntry(mCurrentRow,t+16,tLCPolSchema.getInsuredName());//被保人姓名
          String BankInfo[]=findBankInfo(tLCPolSchema);
          m_book.setEntry(mCurrentRow,t+17,BankInfo[0]);//银行帐号
          m_book.setEntry(mCurrentRow,t+18,ChangeCodetoName.getBankCodeName(BankInfo[1]));//银行编码
          m_book.setEntry(mCurrentRow,t+19,BankInfo[2]);//户名

          LAAgentDB tLAAgentDB=new LAAgentDB();
          tLAAgentDB.setAgentCode(tLCPolSchema.getAgentCode());
          String AgentTel="";
          if(tLAAgentDB.getInfo()==true)
          {
              AgentTel=tLAAgentDB.getPhone();
              if(AgentTel==null||AgentTel.equals(""))
                  AgentTel=tLAAgentDB.getMobile();
              if(AgentTel==null)
                  AgentTel="";
          }
          m_book.setEntry(mCurrentRow,t+20,AgentTel);//代理人电话
          LJAPayDB tLJAPayDB=new LJAPayDB();
          tLJAPayDB.setIncomeNo(tLCPolSchema.getPolNo());
          tLJAPayDB.setIncomeType("2");
          LJAPaySet tLJAPaySet=tLJAPayDB.query();
          m_book.setEntry(mCurrentRow,t+21,String.valueOf(tLJAPaySet.size()));//交费期数

      }
      catch(Exception ex)
      {
          try
          {
              m_book.setEntry(mCurrentRow,t+0,"***处理保单"+tLCPolSchema.getPolNo()+" 出错："+ex);//管理机构
              mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
          }
          catch(Exception ex2)
          {
              buildError("dealSinglePol",ex.toString());
              return false;
          }
          return true;
      }
      mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
      return true;
  }

  /**
   * 根据条件判断：代理人是否在职
   * @param agentCode
   * @return
   */
  private boolean judgePolType(String agentCode)
  {
      if(!mPolType.equals("1")&&!mPolType.equals("0"))
      {
          return true;
      }

      LAAgentDB tLAAgentDB=new LAAgentDB();
      tLAAgentDB.setAgentCode(agentCode);
      if(tLAAgentDB.getInfo()==false)
      {
          return false;
      }
      String AgentState=tLAAgentDB.getAgentState();
      //如果需要是在职
      if(mPolType.equals("1"))
      {
          if(AgentState.equals("01")||AgentState.equals("02"))
          {
              return true;
          }
          return false;
      }
      else //如果需要是不在职
      {
          if(AgentState.equals("01")||AgentState.equals("02"))
          {
              return false;
          }
          return true;
      }
  }

  /**
   *查询续期需要自动续保缴费的附加险
   */
  private LCPolSet findSubPol(LCPolSchema tLCPolSchema)
  {

      LCPolDB tLCPolDB=new LCPolDB();
      String strSQL="select * from lcpol where mainpolno='"+tLCPolSchema.getPolNo() +"' ";
      strSQL=strSQL+" and polno!='"+tLCPolSchema.getPolNo() +"' ";
      strSQL=strSQL+" and appflag='1' and paytodate='"+tLCPolSchema.getPaytoDate()+"' and paytodate=payenddate";
      strSQL=strSQL+" and RnewFlag=-1 ";

      LCPolSet tLCPolSet=tLCPolDB.executeQuery(strSQL);
      return tLCPolSet;
  }


  /**
   * 取描述信息
   * @return
   */
  private boolean checkDesc()
  {
      //查询系统变量-是否主险续期和附加险的自动续保关联-如果关联，则续期应收保费和续保应收保费数据存放在同一个清单
      LDSysVarDB tLDSysVarDB=new LDSysVarDB();
      tLDSysVarDB.setSysVar("relationflag");
      if(tLDSysVarDB.getInfo()==false)
      {
      }
      else
      {
          mRelationFlag=tLDSysVarDB.getSysVarValue();
      }
      //取清单模版文件存放路径（即要生成文件的存放路径）
      tLDSysVarDB=new LDSysVarDB();
      tLDSysVarDB.setSysVar(mFilePathDesc);
      if(tLDSysVarDB.getInfo()==false)
      {
          buildError("checkDesc","LDSysVar取文件路径("+mFilePathDesc+")描述失败");
          return false;
      }
      mFilePath=tLDSysVarDB.getSysVarValue();
      mFileName=getFileName();
      return true;
  }
  /**
   * 杨红于2005-07-26增添新的方法：根据页面输入的条件判断
   * @return boolean
   */
  private boolean dealMulContData()
  {
    String contSqlStr="select * from lccont where contno in (select contno from lcpol"
        +" where ManageCom like '"+mManageCom+"%%' and PayIntv>0 and AppFlag='1'"
        +" and (PaytoDate>='"+mStartDate+"' and paytodate<='"+mEndDate+"' and PaytoDate<PayEndDate)"
        +" and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000')"
        +" and appflag='1' order by PaytoDate,AgentGroup";
    LCContDB tLCContDB=new LCContDB();
    LCContSet tLCContSet=new LCContSet();
    tLCContSet=tLCContDB.executeQuery(contSqlStr);
    if(tLCContSet.size()==0)
    {
      CError.buildErr(this, "在系统中未找到符合查询条件的个单！");
      return false;
    }
    //注意这里是以合同为单位  作交费动作的 而不是针对单个险种！
    for(int i=1;i<=tLCContSet.size();i++)
    {
      LCContBL tLCContBL=new LCContBL();
      tLCContBL.setSchema(tLCContSet.get(i).getSchema());
      if(judgePolType(tLCContBL.getAgentCode())==false)
       continue;
      String contNo=tLCContSet. get(i).getContNo();
      //获取该合同下所有应交保费总和
      double contPremTotal=getContPremTotal(contNo);
      String AgentName="";
      if(tLCContBL.getAgentCode()!=null)
          AgentName=ChangeCodetoName.getAgentName(tLCContBL.getAgentCode());
      String AgentBranch="";
      if(tLCContBL.getAgentGroup()!=null)
         AgentBranch = findAgentBranch(tLCContBL.getAgentGroup());
      String PayMode="";
      if(tLCContBL.getPayLocation()!=null)
         PayMode = findPayMode(tLCContBL.getPayLocation());
      LCAppntDB tLCAppntDB =new LCAppntDB();
      tLCAppntDB.setContNo(contNo);
    //  tLCAppntDB.setAppntNo(tLCContBL.getAppntNo());
      String BankCode="";
      if(tLCContBL.getBankCode()!=null)
         BankCode = tLCContBL.getBankCode();
      String BankAccNo="";
      if(tLCContBL.getBankAccNo()!=null)
        BankAccNo =  tLCContBL.getBankAccNo();
      String AccName="";
      if(tLCContBL.getAccName()!=null)
         AccName = tLCContBL.getAccName();
      String PostalAddress="";//收费地址
      String ZipCode="";
      String Phone="";
      String SexName="";
      String SaleChnl="";
      if(tLCContBL.getSaleChnl()!=null)
        SaleChnl=ChangeCodetoName.getSaleChnl(tLCContBL.getSaleChnl());
      if(tLCContBL.getAppntSex()!=null)
         SexName = ChangeCodetoName.getSexName(tLCContBL.getAppntSex());
      if(tLCAppntDB.getInfo())
      {
        //PostalAddress=tLCAppntDB.getPo
        //ZipCode=tLCAppntDB.getzip
        String addressNo=tLCAppntDB.getAddressNo();
        LCAddressDB tLCAddressDB=new LCAddressDB();
        tLCAddressDB.setAddressNo(addressNo);
        tLCAddressDB.setCustomerNo(tLCContBL.getAppntNo());
        if(tLCAddressDB.getInfo())
        {
          PostalAddress=tLCAddressDB.getPostalAddress();
          ZipCode=tLCAddressDB.getZipCode();
          Phone=tLCAddressDB.getPhone();
        }
      }
      try
      {
      m_book.setEntry(mCurrentRow,0,tLCContBL.getManageCom());//管理机构
      m_book.setEntry(mCurrentRow,1,AgentName);//业务员姓名
      m_book.setEntry(mCurrentRow,2,tLCContBL.getAgentCode());//业务员代码
      m_book.setEntry(mCurrentRow,3,AgentBranch);//业务员组别
      m_book.setEntry(mCurrentRow,4,tLCContBL.getAppntName());//客户姓名
      m_book.setEntry(mCurrentRow,6,tLCContBL.getContNo());//保单号
      m_book.setEntry(mCurrentRow,7,tLCContBL.getPaytoDate());//缴费日期
      m_book.setEntry(mCurrentRow,8,String.valueOf(PayMode));//缴费方式
      m_book.setEntry(mCurrentRow,10,PostalAddress);//收费地址
      m_book.setEntry(mCurrentRow,11,ZipCode);//邮编
      m_book.setEntry(mCurrentRow,12,Phone);//电话contPremTotal
      m_book.setEntry(mCurrentRow,9,String.valueOf(contPremTotal));
      m_book.setEntry(mCurrentRow,5,SexName);
      m_book.setEntry(mCurrentRow,16,SaleChnl);
      m_book.setEntry(mCurrentRow,13,BankAccNo);//银行帐号
      m_book.setEntry(mCurrentRow,14,BankCode);//银行编码
      m_book.setEntry(mCurrentRow,15,AccName);//

      }
      catch(Exception e)
      {
        e.printStackTrace();//仅供调试
      }
      mCurrentRow++;
      //针对单个合同进行处理
      //dealSingleContData(contNo);
    }
    return true;
  }
  private double getContPremTotal(String contno)
  {
    double premTotal=0.0;
    String sqlStr="select * from ljspay where otherno='"+contno+"'";
    LJSPayDB tLJSPayDB=new LJSPayDB();
    LJSPaySet tLJSPaySet=new LJSPaySet();
    tLJSPaySet=tLJSPayDB.executeQuery(sqlStr);
    if(tLJSPaySet.size()>0)
    {
      premTotal=tLJSPaySet. get(1).getSumDuePayMoney();
    }
    return premTotal;
  }
  /**
   * 杨红于2005-07-26说明：新增方法
   * @return boolean
   */
  private boolean dealSingleContData(String contno)
  {
    String sqlStr="select * from lcpol where contno='"+contno+"'"
        +" and  PayIntv>0 and AppFlag='1'"
        +" and (PaytoDate>='"+mStartDate+"' and paytodate<='"+mEndDate+"' and PaytoDate<PayEndDate)"
        +" and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'"
        +" and mainpolno=polno";
    return true;
  }
  /**
   * 处理需要续期缴费的主险保单--如果续期和续保关联，则取附加险信息
   * @return
   */
  private boolean dealDueMainPol()
  {
      String sqlhead="select count(*) from LCPol";
      String sqlStr=" where (PaytoDate>='"+mStartDate+"' and PaytoDate<='"+mEndDate+"' and PaytoDate<PayEndDate ) ";
      sqlStr=sqlStr+" and PayIntv>0";
      sqlStr=sqlStr+" and AppFlag='1'";
      sqlStr=sqlStr+" and PolNo=MainPolNo";
      //sqlStr=sqlStr+" and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')" ;
      sqlStr=sqlStr+" and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'" ;
      String MaxmManageCom=PubFun.RCh(mManageCom,"9",8);
      String MinmManageCom=PubFun.RCh(mManageCom,"0",8);
      sqlStr=sqlStr+" and ManageCom>='"+MinmManageCom+"' and ManageCom<='"+MaxmManageCom+"'";
      sqlStr=sqlStr+" order by PaytoDate,AgentGroup";
      String SQL_Count=sqlhead+sqlStr;
      System.out.println("查询续期主险:"+SQL_Count);

      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS=tExeSQL.execSQL(SQL_Count);
      String strCount=tSSRS.GetText(1,1);
      int SumCount = Integer.parseInt(strCount);
      int CurrentCounter=1;
      String SQL_PolNo="select PolNo from lcpol"+sqlStr;

      LCPolDB tLCPolDB = new LCPolDB();
      LCPolSchema tLCPolSchema = new LCPolSchema();
      //如果基数大与个人保单纪录数，跳出循环
      while(CurrentCounter<=SumCount)
      {
          tExeSQL = new ExeSQL();
          tSSRS=tExeSQL.execSQL(SQL_PolNo,CurrentCounter,mCount);
          if(tSSRS==null)
          {
              buildError("dealDueMainPol","保单读取失败");
              return false;
          }

          for( int i = 1; i <=tSSRS.getMaxRow(); i++ )
          {
              tLCPolSchema = new LCPolSchema();
              tLCPolDB = new LCPolDB();
              tLCPolDB.setPolNo(tSSRS.GetText(i,1));
              tLCPolDB.getInfo();
              tLCPolSchema.setSchema(tLCPolDB.getSchema());
              //判断在职/孤儿单--如果不符合条件-继续下一条
              if(judgePolType(tLCPolSchema.getAgentCode())==false)
                  continue;
              if(dealSinglePol(tLCPolSchema)==false)
                  return false;
          }

          CurrentCounter=CurrentCounter+mCount; //计数器增加
          //考虑是否有方法减少内存的大数据量，可以将m_book数据写到excel文件,m_book清空
          //打开excel文件，从mCurrentRow位置继续写数据
      }
      return true;
  }

  /**
   * 处理需要自动续保的主险保单
   * @return
   */
  private boolean dealRnewMainPol()
  {
      String sqlhead="select count(*) from LCPol ";
      String sqlStr=" where (PaytoDate>='"+mStartDate+"' and PaytoDate<='"+mEndDate+"' and PaytoDate=PayEndDate ) ";
      sqlStr=sqlStr+" and AppFlag='1'";
      sqlStr=sqlStr+" and PolNo=MainPolNo";
      sqlStr=sqlStr+" and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'" ;
      sqlStr=sqlStr+" and RnewFlag=-1 ";
      String MaxmManageCom=PubFun.RCh(mManageCom,"9",8);
      String MinmManageCom=PubFun.RCh(mManageCom,"0",8);
      sqlStr=sqlStr+" and ManageCom>='"+MinmManageCom+"' and ManageCom<='"+MaxmManageCom+"' order by PaytoDate,AgentGroup";

      String SQL_Count=sqlhead+sqlStr;
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS=tExeSQL.execSQL(SQL_Count);
      String strCount=tSSRS.GetText(1,1);
      int SumCount = Integer.parseInt(strCount);
      int CurrentCounter=1;
      String SQL_PolNo="select PolNo from lcpol "+sqlStr;

      if(SumCount>0)
      {
          addPrompt("***以下是主险自动续保数据***");
      }
      LCPolDB tLCPolDB = new LCPolDB();
      LCPolSchema tLCPolSchema = new LCPolSchema();
      //如果基数大与个人保单纪录数，跳出循环
      while(CurrentCounter<=SumCount)
      {
          tExeSQL = new ExeSQL();
          tSSRS=tExeSQL.execSQL(SQL_PolNo,CurrentCounter,mCount);
          if(tSSRS==null)
          {
              buildError("dealDueMainPol","保单读取失败");
              return false;
          }

          for( int i = 1; i <=tSSRS.getMaxRow(); i++ )
          {
              tLCPolSchema = new LCPolSchema();
              tLCPolDB = new LCPolDB();
              tLCPolDB.setPolNo(tSSRS.GetText(i,1));
              tLCPolDB.getInfo();
              tLCPolSchema.setSchema(tLCPolDB.getSchema());
              //判断在职/孤儿单--如果不符合条件-继续下一条
              if(judgePolType(tLCPolSchema.getAgentCode())==false)
                  continue;
              if(dealSingleRnewPol(tLCPolSchema)==false)
                  return false;
          }

          CurrentCounter=CurrentCounter+mCount; //计数器增加
          //考虑是否有方法减少内存的大数据量，可以将m_book数据写到excel文件,m_book清空
          //打开excel文件，从mCurrentRow位置继续写数据
      }
      return true;
  }

  /**
   * 处理需要续保的附加险，其主险不需要续期缴费和续保
   * @return
   */
  private boolean dealRnewSpecSubPol()
  {

      String sqlhead="select count(*) from LCPol ";
      String sqlStr=" where (PaytoDate>='"+mStartDate+"' and PaytoDate<='"+mEndDate+"' and PaytoDate=PayEndDate ) ";
      sqlStr=sqlStr+" and AppFlag='1'";
      sqlStr=sqlStr+" and PolNo!=MainPolNo";
      sqlStr=sqlStr+" and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'" ;
      sqlStr=sqlStr+" and RnewFlag=-1 ";
      String MaxmManageCom=PubFun.RCh(mManageCom,"9",8);
      String MinmManageCom=PubFun.RCh(mManageCom,"0",8);
      sqlStr=sqlStr+" and ManageCom>='"+MinmManageCom+"' and ManageCom<='"+MaxmManageCom+"' order by ManageCom";

      String SQL_Count=sqlhead+sqlStr;
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS=tExeSQL.execSQL(SQL_Count);
      String strCount=tSSRS.GetText(1,1);
      int SumCount = Integer.parseInt(strCount);
      int CurrentCounter=1;
      String SQL_PolNo="select PolNo,MainPolNo,PayEndDate from lcpol "+sqlStr;

      if(SumCount>0)
      {
          addPrompt("***以下是附加险自动续保数据(其主险不需要续期缴费和续保)***");
      }
      LCPolDB tLCPolDB = new LCPolDB();
      LCPolSchema tLCPolSchema = new LCPolSchema();
      //如果基数大与个人保单纪录数，跳出循环
      while(CurrentCounter<=SumCount)
      {
          tExeSQL = new ExeSQL();
          tSSRS=tExeSQL.execSQL(SQL_PolNo,CurrentCounter,mCount);
          if(tSSRS==null)
          {
              buildError("dealDueMainPol","保单读取失败");
              return false;
          }

          for( int i = 1; i <=tSSRS.getMaxRow(); i++ )
          {
              //判断主险是续期或自动续保-跳过
              if(judgeMainRisk(tSSRS.GetText(i,2),tSSRS.GetText(i,3))==false)
                  continue;
              tLCPolSchema = new LCPolSchema();
              tLCPolDB = new LCPolDB();
              tLCPolDB.setPolNo(tSSRS.GetText(i,1));
              tLCPolDB.getInfo();
              tLCPolSchema.setSchema(tLCPolDB.getSchema());
              //判断在职/孤儿单--如果不符合条件-继续下一条
              if(judgePolType(tLCPolSchema.getAgentCode())==false)
                  continue;
              if(dealSingleSubPol(tLCPolSchema)==false)
                  return false;
          }

          CurrentCounter=CurrentCounter+mCount; //计数器增加
          //考虑是否有方法减少内存的大数据量，可以将m_book数据写到excel文件,m_book清空
          //打开excel文件，从mCurrentRow位置继续写数据
      }
      return true;
  }

  /**
   * 不同类型的数据之间添加说明
   * @return
   */
  private boolean addPrompt(String sPrompt)
  {
      try
      {
          //如有数据错误，可以写在excel里
          m_book.setEntry(mCurrentRow,0,sPrompt);//管理机构
          mCurrentRow=mCurrentRow+1;
      }
      catch(Exception ex)
      {
//          buildError("addPrompt","添加说明“"+sPrompt+"失败");
//          return false;
      }
      return true;
  }

  /**
   * 判断主险是否符合条件：非续期，非续保，未到期
   * @param tPolNo   主险号
   * @param tPayEndDate  附加险终交日期
   * @return
   */
  private boolean judgeMainRisk(String tPolNo,String tPayEndDate)
  {
      LCPolDB tLCPolDB=new LCPolDB();
      String strSQL="select * from lcpol where polno='"+ tPolNo +"' ";
      strSQL=strSQL+" and appflag='1'";
      strSQL=strSQL+" and RnewFlag!=-1 "; //不是自动续保的
      strSQL=strSQL+" and payintv=0 ";//不是续期的
      strSQL=strSQL+" and payenddate>'"+tPayEndDate+"'";//未到期的
      //如果不符合条件-则返回false
      LCPolSet tLCPolSet=tLCPolDB.executeQuery(strSQL);
      if(tLCPolSet.size()==0)
          return false;

      return true;
  }

  /**
   * 得到续期保单对应某个日期应该交纳的费用--从保费项查询--因为保单纪录所有阶段应该缴纳的总和
   * @param tPolNo   主险号
   * @return
   */
  private double getMainPolPrem(String tPolNo)
  {
      String sql="select sum(prem) from LCPrem where polno='"+tPolNo+"' and PayEndDate>'"+mEndDate+"' and PayStartDate<='"+mStartDate+"'";
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS=tExeSQL.execSQL(sql);
      double sumPrem = Double.parseDouble(tSSRS.GetText(1,1));
      return sumPrem;
  }

  /**
   * 得到展业机构外部编码--中文名称
   * @param tAgentGroup
   * @return
   */
  private String findAgentBranch(String tAgentGroup)
  {
      LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
      tLABranchGroupDB.setAgentGroup(tAgentGroup) ;
      if(tLABranchGroupDB.getInfo()==false)
          return "unknow";
      else
          //return tLABranchGroupDB.getBranchAttr();
          return tLABranchGroupDB.getName();
  }

  /**
   * 找交费方式
   * @param tPolNo
   * @return
   */
  private String findPayMode2(String tPolNo)
  {

      String SQL="select tempfeeno from LJTempFee where otherno='"+tPolNo+"' and confflag='1' order by makedate desc";
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS=tExeSQL.execSQL(SQL);
      if(tSSRS==null)
          return "unknow";
      String tempfeeno=tSSRS.GetText(1,1);

      LJTempFeeClassDB tLJTempFeeClassDB=new LJTempFeeClassDB();
      tLJTempFeeClassDB.setTempFeeNo(tempfeeno);
      LJTempFeeClassSet tLJTempFeeClassSet=tLJTempFeeClassDB.query();
      if(tLJTempFeeClassSet.size()==0)
          return "unknow";
      else
          return ChangeCodetoName.getPayModeName(tLJTempFeeClassSet.get(1).getPayMode());
  }

  /**
   * 找银行信息
   * @param tPolNo
   * @return
   */
  private String[] findBankInfo(String tPolNo)
  {
      String bankInfo[]=new String[3];
      String SQL="select tempfeeno from LJTempFee where otherno='"+tPolNo+"' and confflag='1' order by makedate desc";
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS=tExeSQL.execSQL(SQL);
      if(tSSRS==null)
          return bankInfo;
      String tempfeeno=tSSRS.GetText(1,1);

      LJTempFeeClassDB tLJTempFeeClassDB=new LJTempFeeClassDB();
      tLJTempFeeClassDB.setTempFeeNo(tempfeeno);
      LJTempFeeClassSet tLJTempFeeClassSet=tLJTempFeeClassDB.query();
      if(tLJTempFeeClassSet.size()==0)
          return bankInfo;
      else
      {
          bankInfo[0]=tLJTempFeeClassSet.get(1).getBankAccNo();
          bankInfo[1]=tLJTempFeeClassSet.get(1).getBankCode();
          bankInfo[2]=tLJTempFeeClassSet.get(1).getAccName();
      }
          return bankInfo;
  }

  /**
   * 找交费方式
   * @param tPolNo
   * @return
   */
  private String findPayMode(String tPolNo,String PayLocation)
  {
      if(PayLocation==null)
          return findPayMode2(tPolNo);
      else
          return ChangeCodetoName.getPayLocationName(PayLocation) ;
  }
  private String findPayMode(String PayLocation)
  {
    return ChangeCodetoName.getPayLocationName(PayLocation) ;
  }
  /**
   * 找银行信息
   * @param tPolNo
   * @return
   */
  private String[] findBankInfo(LCPolSchema tLCPolSchema)
  {
      if(tLCPolSchema.getPayLocation()==null)
          return findBankInfo(tLCPolSchema.getPolNo());
      else
      {
          String bankInfo[]=new String[3];
          /*Lis5.3 upgrade get
          bankInfo[0]=tLCPolSchema.getBankAccNo();
          bankInfo[1]=tLCPolSchema.getBankCode();
          bankInfo[2]=tLCPolSchema.getAccName();
          */
          return bankInfo;
      }
  }



}

