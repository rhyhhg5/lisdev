package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpPhoneVisitSaveBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */

    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private TransferData mTransferData = new TransferData();
    /** 数据表  保存数据*/

    //集体保单表
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    //个人保单表
    private LCContSet mLCContSet = new LCContSet();
    //集体险种表
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    //个人险种表
    private LCPolSet mLCPolSet = new LCPolSet();

    //应收团体交费表
    private LJSPayGrpSet tLJSPayGrpSet = new LJSPayGrpSet();
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
    private LJSPayPersonBSet mLJSPayPersonBSet = new LJSPayPersonBSet();
    //应收总表
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();
    //应收总表集
    private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();
    //应收总表集备份表
    private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();
    //应收总表交费日期
    private String mPayDate;
    //集体保单的付费方式
    private String mPayMode;
    private String mGetNoticeNo;

    //缴费通知书
    private MMap map = new MMap();
    public GrpPhoneVisitSaveBL() {
    }

    public static void main(String[] args) {
        GrpPhoneVisitSaveBL GrpPhoneVisitSaveBL1 = new GrpPhoneVisitSaveBL();
        GlobalInput tGt = new GlobalInput();
        tGt.ComCode = "86";
        tGt.Operator = "001";
        tGt.ManageCom = "86";
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("9028000000005588");
        //tLCGrpContSchema.setPrtNo(PrtNo);//Yangh于2005-07-19将以前的注销
        TransferData tempTransferData = new TransferData();
        tempTransferData.setNameAndValue("StartDate", "2005-01-01");
        tempTransferData.setNameAndValue("EndDate", "2005-8-13");
        VData tVData = new VData();
        tVData.add(tLCGrpContSchema);
        tVData.add(tGt);
        tVData.add(tempTransferData);
        GrpPhoneVisitSaveBL1.submitData(tVData, "INSERT");
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {

        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        if (!prepareData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(mInputData, "") == false) {
            System.out.println("delete fail!");
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        mLCGrpContSchema = ((LCGrpContSchema) mInputData.getObjectByObjectName(
                "LCGrpContSchema", 0));
        if (tGI == null || mTransferData == null || mLCGrpContSchema == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpPhoneVisitOverBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * Yangh于2005-07-19创建新的dealData的处理逻辑
     * @return boolean
     */
    private boolean dealData() {

        //获取从前台页面传入的数据
        mPayMode = (String) mTransferData.getValueByName("PayMode");
        mPayDate = (String) mTransferData.getValueByName("PayDate");
        mGetNoticeNo = (String)mTransferData.getValueByName("GetNoticeNo");

        //集体保单, 主要是更新一些字段信息
        String grpContNo = mLCGrpContSchema.getGrpContNo(); //获取从前台页面传入的团单合同号
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        if (!tLCGrpContDB.getInfo()) {
            CError.buildErr(this, "团单：" + grpContNo + "系统中信息缺失，不能重设交费事项！");
            return false;
        }
        mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        mLCGrpContSchema.setPayMode(mPayMode);
        mLCGrpContSchema.setModifyDate(CurrentDate); //最后一次修改日期
        mLCGrpContSchema.setModifyTime(CurrentTime);
        //个人保单
        //下面处理lccont,即团单下个人的信息 主要是更新一些字段信息
        String contSql = "select * from lccont where grpcontno='" + grpContNo +
                         "'"
                         +
                         " and contno in (select contno from lcpol where polno"
                         +
                         " in (select polno from ljspayperson where inputflag='1' and GrpContNo='"
                         + grpContNo + "'))";
        System.out.println(contSql);
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = new LCContSet();
        tLCContSet = tLCContDB.executeQuery(contSql);
        if (tLCContSet.size() == 0) {

            CError.buildErr(this, "团单：" + grpContNo + "下的个单信息缺失，不能重设交费事项！");
            return false;
        }
        for (int t = 1; t <= tLCContSet.size(); t++) {
            LCContBL tLCContBL = new LCContBL();
            tLCContBL.setSchema(tLCContSet.get(t).getSchema());
            tLCContBL.setPayMode(mPayMode);
            tLCContBL.setModifyDate(CurrentDate); //最后一次修改日期
            tLCContBL.setModifyTime(CurrentTime);
            mLCContSet.add(tLCContBL);
        }
        ////获取有对应ljsGrppay的lcgrppol集合
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        String grpPolSqlStr = "select * from LCGrpPol where GrpContNo='" +
                              grpContNo + "'"
                              +
                              " and GrpPolNo in (select GrpPolNo from LJSPayGrp where paytype='ZC')"; //校验类型是ZC的应收集体记录
        System.out.println(grpPolSqlStr);
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        tLCGrpPolSet = tLCGrpPolDB.executeQuery(grpPolSqlStr);
        if (tLCGrpPolSet.size() == 0) {
            CError.buildErr(this, "团单下的集体催收数据缺失，不能重设交费事项！");
            return false;
        }

        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            String grpPolNo = tLCGrpPolSet.get(i).getGrpPolNo();
            LCGrpPolBL tLCGrpPolBL = new LCGrpPolBL();
            tLCGrpPolBL.setSchema(tLCGrpPolSet.get(i).getSchema());
            tLCGrpPolBL.setPayMode(mPayMode);
            tLCGrpPolBL.setModifyDate(CurrentDate); //最后一次修改日期
            tLCGrpPolBL.setModifyTime(CurrentTime);
            mLCGrpPolSet.add(tLCGrpPolBL);
            //获取有对应ljspayperson的lcpol集合
            String polSqlStr = "select * from lcpol where GrpPolNo='" +
                               grpPolNo +
                               "'"
                               +
                               " and polno in (select polno from ljspayperson where InputFlag='1')";
            System.out.println(polSqlStr);
            LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet tLCPolSet = new LCPolSet();
            tLCPolSet = tLCPolDB.executeQuery(polSqlStr);
            if (tLCPolSet.size() == 0) {
                CError.buildErr(this, "团单下的个人催收数据缺失，不能重设交费事项！");
                return false;
            }
            for (int j = 1; j <= tLCPolSet.size(); j++) {
                LCPolBL tLCPolBL = new LCPolBL();
                tLCPolBL.setSchema(tLCPolSet.get(j).getSchema());
                tLCPolBL.setPayMode(mPayMode); //获取缴费日期
                tLCPolBL.setModifyDate(CurrentDate); //最后一次修改日期
                tLCPolBL.setModifyTime(CurrentTime); //最后一次修改时间
                mLCPolSet.add(tLCPolBL);
            }
        }

        //应收总表, 主要是更新一些字段信息
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setGetNoticeNo(mGetNoticeNo);
        tLJSPayDB.setOtherNoType("1");
        if (!tLJSPayDB.getInfo()) {
            CError.buildErr(this, "团单：" + grpContNo + "在应收总表中信息缺失，不能重设交费事项！");
            return false;
        }
        mLJSPaySchema.setSchema(tLJSPayDB.getSchema());
        mLJSPaySchema.setPayDate(mPayDate);

        //应收总表备份表, 主要是更新一些字段信息
       LJSPayBDB tLJSPayBDB = new LJSPayBDB();
       tLJSPayBDB.setGetNoticeNo(mGetNoticeNo);
       tLJSPayBDB.setOtherNoType("1");
       if (!tLJSPayBDB.getInfo()) {
           CError.buildErr(this, "团单：" + grpContNo + "在应收总表备份表中信息缺失，不能重设交费事项！");
           return false;
       }
       mLJSPayBSchema.setSchema(tLJSPayBDB.getSchema());
       mLJSPayBSchema.setPayDate(mPayDate);
       //重设交费事项！
       mLJSPayBSchema.setCancelReason("2");

        //集体应收表
        String strNotice = mGetNoticeNo;
        LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
        String grpPaySql = "select * from LJSPayGrp where GetNoticeNo='" +
                           mGetNoticeNo + "'"
                           ;
                          // "  and paytype='ZC'"; //校验类型是ZC的应收集体记录

        tLJSPayGrpSet = tLJSPayGrpDB.executeQuery(grpPaySql);
        System.out.println(grpPaySql);
        if (tLJSPayGrpSet.size() == 0) {
            CError.buildErr(this, "团单：" + grpContNo + "在应收集体表中信息缺失，不能重设交费事项！");
            return false;
        }
        for (int k = 1; k <= tLJSPayGrpSet.size(); k++) {
            LJSPayGrpBL tLJSPayGrpBL = new LJSPayGrpBL();
            tLJSPayGrpBL.setSchema(tLJSPayGrpSet.get(k).getSchema());
            tLJSPayGrpBL.setPayDate(mPayDate); //获取缴费日期
            tLJSPayGrpBL.setModifyDate(CurrentDate); //最后一次修改日期
            tLJSPayGrpBL.setModifyTime(CurrentTime); //最后一次修改时间
            mLJSPayGrpSet.add(tLJSPayGrpBL);

        }
        //集体应收表备份表
        LJSPayGrpBDB tLJSPayGrpBDB = new LJSPayGrpBDB();
        String grpPayBSql = "select * from LJSPayGrpB where GetNoticeNo='" +
                           mGetNoticeNo + "'"
                          //+ "  and paytype='ZC'"
                          ; //校验类型是ZC的应收集体记录
        LJSPayGrpBSet tLJSPayGrpBSet = new LJSPayGrpBSet();
        tLJSPayGrpBSet = tLJSPayGrpBDB.executeQuery(grpPayBSql);
        System.out.println(grpPayBSql);
        if (tLJSPayGrpBSet.size() == 0) {
            CError.buildErr(this, "团单：" + grpContNo + "在应收集体备份表中信息缺失，不能重设交费事项！");
            return false;
        }
        for (int l = 1; l <= tLJSPayGrpBSet.size(); l++) {
            LJSPayGrpBSchema tLJSPayGrpBSchema = new LJSPayGrpBSchema();
            tLJSPayGrpBSchema.setSchema(tLJSPayGrpBSet.get(l).getSchema());
            tLJSPayGrpBSchema.setPayDate(mPayDate); //获取缴费日期
            tLJSPayGrpBSchema.setCancelReason("2");//重设交费事项
            tLJSPayGrpBSchema.setModifyDate(CurrentDate); //最后一次修改日期
            tLJSPayGrpBSchema.setModifyTime(CurrentTime); //最后一次修改时间
            mLJSPayGrpBSet.add(tLJSPayGrpBSchema);

        }

        //个人应收
        String personSql = "select * from LJSPayPerson where GetNoticeNo='" +
                           mGetNoticeNo + "'"      ;
        LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
        LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
        tLJSPayPersonSet = tLJSPayPersonDB.executeQuery(personSql);
        System.out.println(personSql);
        if (tLJSPayPersonSet.size() == 0) {
            CError.buildErr(this, "团单：" + grpContNo + "在应收个人表中信息缺失，不能重设交费事项！");
            return false;
        }
        for (int l = 1; l <= tLJSPayPersonSet.size(); l++) {
            LJSPayPersonSchema tLJSPayPersonSchema = new LJSPayPersonSchema();
            tLJSPayPersonSchema.setSchema(tLJSPayPersonSet.get(l).getSchema());
            tLJSPayPersonSchema.setPayDate(mPayDate); //获取缴费日期
            tLJSPayPersonSchema.setModifyDate(CurrentDate); //最后一次修改日期
            tLJSPayPersonSchema.setModifyTime(CurrentTime); //最后一次修改时间
            mLJSPayPersonSet.add(tLJSPayPersonSchema);
        }
/*
        //个人应收B
        String personBSql = "select * from LJSPayPersonB where GetNoticeNo='" +
                           mGetNoticeNo + "'"      ;
       LJSPayPersonBDB tLJSPayPersonBDB = new LJSPayPersonBDB();
        LJSPayPersonBSet tLJSPayPersonBSet = new LJSPayPersonBSet();
        tLJSPayPersonBSet = tLJSPayPersonBDB.executeQuery(personBSql);
        System.out.println(personBSql);
        if (tLJSPayPersonBSet.size() == 0) {
            CError.buildErr(this, "团单：" + grpContNo + "在应收个人备份表中信息缺失，不能重设交费事项！");
            return false;
        }
        for (int l = 1; l <= tLJSPayPersonBSet.size(); l++) {
            LJSPayPersonBSchema tLJSPayPersonBSchema = new LJSPayPersonBSchema();
            tLJSPayPersonBSchema.setSchema(tLJSPayPersonBSet.get(l).getSchema());
            tLJSPayPersonBSchema.setPayDate(mPayDate); //获取缴费日期
            tLJSPayPersonBSchema.setCancelReason("2");//重设交费事项
            tLJSPayPersonBSchema.setModifyDate(CurrentDate); //最后一次修改日期
            tLJSPayPersonBSchema.setModifyTime(CurrentTime); //最后一次修改时间
            mLJSPayPersonBSet.add(tLJSPayPersonBSchema);
        }
*/
        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        mInputData = new VData();
        return true;
    }

    public boolean prepareData() {

        map.put(mLJSPaySchema, "UPDATE");
        map.put(mLJSPayBSchema, "UPDATE");
        map.put(mLJSPayGrpSet, "UPDATE");
        map.put(mLJSPayGrpBSet, "UPDATE");
        map.put(mLJSPayPersonSet, "UPDATE");
        map.put(mLJSPayPersonBSet, "UPDATE");
        map.put(mLCGrpContSchema, "UPDATE");
        map.put(mLCContSet, "UPDATE");
        map.put(mLCGrpPolSet, "UPDATE");
        map.put(mLCPolSet, "UPDATE");

        mInputData.add(map);
        return true;
    }


}
