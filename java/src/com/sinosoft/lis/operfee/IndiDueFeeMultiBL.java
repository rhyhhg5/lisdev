package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

import java.lang.String;

import java.util.*;
import com.sinosoft.lis.xb.PRnewDueFeeBL;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */
public class IndiDueFeeMultiBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    //   private VData saveData = new VData();
    //应收总表交费日期
    //  private String  strNewLJSPayDate;
    //应收总表最早交费日期
    //   private String  strNewLJSStartDate;

    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String StartDate = ""; //应收开始时间
    private String EndDate = ""; //应收结束时间
    private String mserNo = ""; //批次号


    /** 数据表  保存数据*/

    //3种情况：1 保单全部催收 2 只催收有特约的保单 3 只催收没有特约的保单
    //如果mDuebySpecFlag为真，根据mSpecFlag标记判断是催收有特约的还是没有特约的保单
    //否则保单全部催收
    private boolean mDuebySpecFlag = false;

    //如果根据特约催收，那么mSpecFlag="1" 只催收有特约的保单 ；否则只催收没有特约的保单
    private String mSpecFlag = "";
    private TransferData mTransferData = new TransferData();
    private String mBusinessFlag="";
    public IndiDueFeeMultiBL() {
    }

    public static void main(String[] args) {
        //  LCPolSchema tLCPolSchema = new LCPolSchema();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("StartDate", "2017-11-25");
        tTransferData.setNameAndValue("EndDate", "2017-11-25");
        tTransferData.setNameAndValue("ManageCom", "863");
        tTransferData.setNameAndValue("BusinessFlag", "");

//        String sql = "select ContNo,PrtNo,AppntName,CValiDate,SumPrem,  (select AccGetMoney from LCAppAcc  where CustomerNo=lccont.appntno),prem,min(paytodate),  (select codename from ldcode where codetype='paymode' and code=PayMode),  ShowManageName(ManageCom),AgentCode,'' "
//                     + "from LCCont "
//                     + "where contNo = '00000930401' and AppFlag='1'  and ContType='1' and (StateFlag is null or StateFlag = '1') "
//                     + "    and ContNo not in "
//                     + "        (select otherno from ljspay where othernotype='2' and OtherNo=ContNo)"
//                     + "    and exists"
//                     + "        (select 'X' from LCPol "
//                     + "        where contno=lccont.contno "
//                     + "          and (PaytoDate>='2005-01-01' and PaytoDate<='2009-09-25'               and (polstate is null or (polstate is not null and polstate not like '02%%' and polstate not like '03%%'))and managecom like '86%%'              and ( PaytoDate<EndDate                     and PayIntv>0                     and exists (select riskcode from lmrisk WHERE cPayFlag='Y' and lmrisk.riskcode=lcpol.riskcode)                   or                     exists (select riskcode from lmrisk WHERE rnewFlag!='N' and lmrisk.riskcode=lcpol.riskcode)                     and exists (select riskCode from LMRiskApp where riskCode = LCPol.riskCode and riskType5 = '2' and (endDate > LCPol.endDate or endDate is null))                     and not exists(select contNo from LCRnewStateLog where contNo = LCCont.contNo and state != '6')) and AppFlag='1' and (StateFlag is null or StateFlag = '1') and (StopFlag='0' or StopFlag is null)              and PolNo not in (select PolNo from LJSPayPerson a where a.contno=contno)              )       ) "
//                     + "group by contNo,PrtNo,AppntName,CValiDate,SumPrem,appntno,prem,PayMode,ManageCom,AgentCode order by contNo desc with ur ";
//        tTransferData.setNameAndValue("QuerySql", sql);

        IndiDueFeeMultiBL IndiDueFeeMultiBL1 = new IndiDueFeeMultiBL();
        IndiDueFeeMultiBL1.mBusinessFlag = "";
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";
        tGI.ManageCom = "86";
        VData tv = new VData();

        //HashMap map = (HashMap)mInputData.getObjectByObjectName("HashMap", 0);
        //考虑添加系统描述：针对特约保单的催收 1-都催收 2-只催收有特约的 3-只催收没有特约的
        //mDuebySpecFlag,Spec
        tv.add(tGI);
        //  tv.add(tLCPolSchema);
        tv.add(tTransferData);

        if (IndiDueFeeMultiBL1.submitData(tv, "INSERT")) {
            System.out.println("个单批处理催收完成" + IndiDueFeeMultiBL1.mErrors.getErrContent());
        } else {
            System.out.println("个单批处理催收信息提示：" +
                               IndiDueFeeMultiBL1.mErrors.getFirstError());
        }
    }

    /**
     * @param cInputData VData，包含：
     * 1、	GlobalInput对象，完整的登陆用户信息
     * 2、	TransferData对象，包含StartDate， EndDate， ManageCom
     * @param cOperate String：操作类型，此可为空字符串“”
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;
        //接收传入数据
        if (!getInputData(cInputData)) {
            return false;
        }
        //处理数据
        if (!dealData()) {
            return false;
        }
        if (!dealUrgeLog("3", "UPDATE")) {
                   return false;
        }
        return true;
    }

    /**
     * Yangh于2005-07-18添加新的逻辑处理方法
     * @return boolean
     */
    public boolean dealData() {
        //接收外部传入数据
        StartDate = (String) mTransferData.getValueByName("StartDate");
        EndDate = (String) mTransferData.getValueByName("EndDate");
        String manageCom = (String) mTransferData.getValueByName("ManageCom");
        String querySql = (String)mTransferData.getValueByName("QuerySql");
        //add by 张彦梅
        String queryCondition="";
        mBusinessFlag =(String) mTransferData.getValueByName("BusinessFlag");
        mBusinessFlag = mBusinessFlag==null?"":mBusinessFlag;


        //批量抽档数据查询

        if(querySql == null || querySql.equals("") )
        {  
        	if(mOperate.toString().equals("INSERT")){
             if(mBusinessFlag.trim().equals("omnip")){
        	 querySql = "select ContNo,PrtNo,AppntName,CValiDate,SumPrem, "
                       + "(select AccGetMoney from LCAppAcc  where CustomerNo=lccont.appntno),prem,min(paytodate), "
                       + "(select codename from ldcode where codetype='paymode' and code=PayMode), "
                       + "ShowManageName(ManageCom),AgentCode,'' "
                       + "from LCCont "
                       + "where AppFlag='1'  and ContType='1' and (StateFlag is null or StateFlag = '1') "
      //                 + "and exists (select 1 from lcpol a,lmriskapp b where grpcontno = '00000000000000000000' and a.riskcode = b.RiskCode and (a.StandbyFlag1 is null or a.StandbyFlag1 = '0') and b.risktype4 ='4' and a.Contno = lccont.ContNO) "
      //                 + "and (select count(1) from LCContState where StateType='GracePeriod' and state='0' and  LCContState.contNo = LCCont.ContNo) = 0 "
                       + "and (select count(1) from ljspay where othernotype='2' and OtherNo= LCCont.ContNo)=0  "
                       + "and exists "
                       + "(select 'X' from LCPol "
                       + "where contno=lccont.contno and ContType = '1' "
                       + "and PaytoDate>='" + StartDate
                       + "' and PaytoDate<='" + EndDate + "' "
                       + "and (polstate is null or (polstate is not null and polstate not like '02%' and polstate not like '03%')) "
                       + "and managecom like '" + manageCom + "%' "
                       + " and managecom not in (select code from ldcode where codetype='qymanagecom') "//临时屏销售迁移的机构
                       + " and not exists (select 1 from ldcode where codetype='pbmanagecom'  and comcode =lcpol.managecom and code=lcpol.riskcode) "//临时屏蔽机构的某些产品
                       + "and ((PaytoDate<payEndDate "
			           + "and PayIntv>0 "
			           + "and exists (select riskcode from lmrisk WHERE cPayFlag='Y' and lmrisk.riskcode=lcpol.riskcode)) "
			           + " or (PaytoDate>=payEndDate "//添加万能产品续保
			           + "and exists (select riskcode from lmrisk WHERE rnewFlag!='N' and lmrisk.riskcode=lcpol.riskcode) "
			           + "and exists (select riskCode from LMRiskApp where riskCode = LCPol.riskCode and riskType5 = '2')) "
			           + " ) "
			           + "and exists (select riskcode from lmriskapp WHERE RiskType4='4' and riskcode not in ('332301','334801','340501','340601','340602') and riskcode=lcpol.riskcode) "
			           + "and AppFlag='1' and (StopFlag='0' or StopFlag is null) "
			           + "and (StandbyFlag1 is null or StandbyFlag1 = '0') "
	                   + "and (select count(1) from LJSPayPerson a where a.contno=lcpol.contno and a.polno=lcpol.PolNo)=0 "
                       + ") "
                       + "and not exists (select 1 from lccontstate where contno=lccont.contno and state='1' and statetype='"+BQ.STATETYPE_ULIDEFER+"') "
                       + "group by contNo,PrtNo,AppntName,CValiDate,SumPrem,appntno,prem,PayMode,ManageCom,AgentCode "
			           + "order by contNo "
			           + "with ur ";

        }else{
        	 querySql = "select ContNo,PrtNo,AppntName,CValiDate,SumPrem,"
        	     + "  nvl((select AccGetMoney from LCAppAcc  where CustomerNo=lccont.appntno),0),prem,min(paytodate),"
        	     + "  (select codename from ldcode where codetype='paymode' and code=PayMode),"
        	     + "  ShowManageName(ManageCom),AgentCode,'' "
        	     + "from LCCont "
        	     + "where not exists (select 1 from lcpol a,lmriskapp b where a.riskcode = b.RiskCode and b.riskcode not in ('332301','334801','340501','340601','340602') and b.risktype4 ='4' and a.Contno = lccont.ContNO)"
//        	     由于外测和模拟版本不一致，经lh协商，模拟为紧急更新时版本，在模拟基础上添加情况
        	     +" and AppFlag='1'  and ContType='1' and (StateFlag is null or StateFlag = '1') "
           	     + " and (cardflag in ('0','5','9','8','c','d') or cardflag is null "
        	     + "  or (cardflag ='6' and exists(select 1 from lcpol where contno = lccont.contno and riskcode in('240501','320106','333501','532401','532501'))) " 
        	     + " or (cardflag  not in  ('0','b') and payintv = 1) or (cardflag != '0' and exists(select 1 from lcpol where contno = lccont.contno and riskcode ='330307')) "
        	     +	" or (cardflag ='2'  and exists (select 1 from lcpol where contno = lccont.contno and riskcode in ('5201','1206','121101','510901','520401','122001','520601','232101','334301','290201')) "
        	     +"      			or exists (select 1 from lcpol where contno=lccont.contno and  riskcode in (select riskcode from ldriskwrap where riskwrapcode in(select wrapcode from ldwrapxq where state='1'))))"
        	  //   + " or  (cardflag = 'b' and exists (select 1 from ldcode where codetype = 'DSXQ' and (code = agentcode or code = agentcom))) "
        	     + " ) "	 
        	     + "    and exists "
        	     + "       (select 'X' from LCPol "
        			 + "       where contno=lccont.contno "
        			 //保单性质
        			 + " and riskcode not in ('320106','120706','123201','220601','123202','220602')"       			 + "          and (PaytoDate>='"+StartDate+"' and PaytoDate<='"+EndDate + "' "
        			 + "              and (polstate is null or (polstate is not null and polstate not like '02%' and polstate not like '03%'))"
        			 + "              and managecom like '"+manageCom+"%'"
        			 //续期
        			 + "              and ( PaytoDate<payEndDate "
        			 + "                    and PayIntv>0 "
        			 + "                    and exists (select riskcode from lmrisk WHERE cPayFlag='Y' and lmrisk.riskcode=lcpol.riskcode) "
        			 + "                  or "
        			 //续保
        			 + "                    exists (select riskcode from lmrisk WHERE rnewFlag!='N' and lmrisk.riskcode=lcpol.riskcode) "
        			 //2008-8-13少儿险停售后仍可续保
//        			2010-2-26所有险种停售后不可卖新单但仍可续保 cbs00035250
        			 + "                    and exists (select riskCode from LMRiskApp where riskCode = LCPol.riskCode and (riskType5 = '2' or riskcode in ('510901','511001') ) ) " // and (endDate > LCPol.endDate or endDate is null or riskcode in ('320106','120706'))) "
        			 + "                    and not exists(select contNo from LCRnewStateLog where polno = LCpol.polno and state in('4','5')) "
        			 + "                  )"
        			 + "              and AppFlag='1' and (StopFlag='0' or StopFlag is null)"
               + "              and PolNo not in (select PolNo from LJSPayPerson a where a.contno=contno)"
               + "              ) "
               + "      )  and grpcontno='00000000000000000000' "
               + "   and cardflag <> 'b'    "   //续期电商分开提
               + " and not exists  (select 1 from lcpol where contno=lccont.contno and  riskcode  in (select code from ldcode where codetype='ybkriskcode') and (prtno like 'YWX%' or prtno like 'YBK%'))"
               +" and managecom not in (select code from ldcode where codetype='qymanagecom') "//20160121临时屏销售迁移的机构
               + "group by contNo,PrtNo,AppntName,CValiDate,SumPrem,appntno,prem,PayMode,ManageCom,AgentCode "
        			 + "order by contNo "
        			 + "with ur "
        			 ;

        }
        	}
        	else if (mOperate.toString().equals("DSINSERT")){
        		querySql = "select ContNo,PrtNo,AppntName,CValiDate,SumPrem,"
           	     + "  nvl((select AccGetMoney from LCAppAcc  where CustomerNo=lccont.appntno),0),prem,min(paytodate),"
           	     + "  (select codename from ldcode where codetype='paymode' and code=PayMode),"
           	     + "  ShowManageName(ManageCom),AgentCode,'' "
           	     + "from LCCont "
           	     + "where not exists (select 1 from lcpol a,lmriskapp b where a.riskcode = b.RiskCode and b.riskcode not in ('332301','334801','340501','340601','340602') and b.risktype4 ='4' and a.Contno = lccont.ContNO)"
//           	     由于外测和模拟版本不一致，经lh协商，模拟为紧急更新时版本，在模拟基础上添加情况
           	     +" and AppFlag='1'  and ContType='1' and (StateFlag is null or StateFlag = '1') "
           	  //   + " and (cardflag in ('0','5','9','8','c','d') or cardflag is null "
        	  //   + "  or (cardflag ='6' and exists(select 1 from lcpol where contno = lccont.contno and riskcode in('240501','320106','333501','532401','532501'))) " 
        	   //  + " or (cardflag  not in  ('0','b') and payintv = 1) or (cardflag != '0' and exists(select 1 from lcpol where contno = lccont.contno and riskcode ='330307')) "
        	    // +	" or (cardflag ='2'  and exists (select 1 from lcpol where contno = lccont.contno and riskcode in ('5201','1206','121101','510901','520401','122001','520601','232101','334301','290201')) "
        	  //   +"      			or exists (select 1 from lcpol where contno=lccont.contno and  riskcode in (select riskcode from ldriskwrap where riskwrapcode in(select wrapcode from ldwrapxq where state='1'))))"
        	     + " cardflag = 'b' and exists (select 1 from ldcode where codetype = 'DSXQ' and (code = agentcode or code = agentcom)) "
        	   //  + " ) "	 
        	     + "    and exists "
        	     + "       (select 'X' from LCPol "
        			 + "       where contno=lccont.contno "
        			 //保单性质
        			 + " and riskcode not in ('320106','120706','123201','220601','123202','220602')"
           			 + "          and (PaytoDate>='"+StartDate+"' and PaytoDate<='"+EndDate + "' "
           			 + "              and (polstate is null or (polstate is not null and polstate not like '02%' and polstate not like '03%'))"
           			 + "              and managecom like '"+manageCom+"%'"
           			 //续期
           			 + "              and ( PaytoDate<payEndDate "
           			 + "                    and PayIntv>0 "
           			 + "                    and exists (select riskcode from lmrisk WHERE cPayFlag='Y' and lmrisk.riskcode=lcpol.riskcode) "
           			 + "                  or "
           			 //续保
           			 + "                    exists (select riskcode from lmrisk WHERE rnewFlag!='N' and lmrisk.riskcode=lcpol.riskcode) "
           			 //2008-8-13少儿险停售后仍可续保
//           			2010-2-26所有险种停售后不可卖新单但仍可续保 cbs00035250
           			 + "                    and exists (select riskCode from LMRiskApp where riskCode = LCPol.riskCode and (riskType5 = '2' or riskcode in ('510901','511001') ) ) " // and (endDate > LCPol.endDate or endDate is null or riskcode in ('320106','120706'))) "
           			 + "                    and not exists(select contNo from LCRnewStateLog where polno = LCpol.polno and state in('4','5')) "
           			 + "                  )"
           			 + "              and AppFlag='1' and (StopFlag='0' or StopFlag is null)"
                  + "              and PolNo not in (select PolNo from LJSPayPerson a where a.contno=contno)"
                  + "              ) "
                  + "      )  and grpcontno='00000000000000000000' "
                  + " and not exists  (select 1 from lcpol where contno=lccont.contno and  riskcode  in (select code from ldcode where codetype='ybkriskcode') and (prtno like 'YWX%' or prtno like 'YBK%'))"
                  +" and managecom not in (select code from ldcode where codetype='qymanagecom') "//20160121临时屏销售迁移的机构
                  + "group by contNo,PrtNo,AppntName,CValiDate,SumPrem,appntno,prem,PayMode,ManageCom,AgentCode "
           			 + "order by contNo "
           			 + "with ur "
           			 ;
        	}
             
        }
  
        System.out.println(querySql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(querySql);
        if (tSSRS == null || tSSRS.getMaxRow() == 0) {
            CError.buildErr(this, "系统中没有符合催收时间范围的保单信息！");
            return false;
        }
        System.out.println("本次续期续保的保单数为====================="+tSSRS.getMaxRow());

        //抽档批次号
        String tLimit = PubFun.getNoLimit(manageCom);
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        mserNo = serNo;
        //插入催收核销日志表
        if (!dealUrgeLog("1", "INSERT")) {
            return false;
        }


        //循环处理数据
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

            String contNo = tSSRS.GetText(i, 1);
            LCContSchema tempLCContSchema = new LCContSchema();
            tempLCContSchema.setContNo(contNo);

            System.out.println("本次续期的保单号为============================="+contNo);
            VData tVData = new VData();
            tVData.add(tempLCContSchema);
            tVData.add(tGI);
            tVData.add(serNo);
            mTransferData.setNameAndValue("serNo",serNo);
            tVData.add(mTransferData);

            //针对保单进行抽档操作（例如：生成应收数据，应收抽档通知书等）
            try {
				PRnewDueFeeBL tPRnewDueFeeBL = new PRnewDueFeeBL();
				if (!tPRnewDueFeeBL.submitData(tVData, "INSERT")) {
				    CError.buildErr(this, "保单号为：" + contNo + "的保单催收失败:"
				        + tPRnewDueFeeBL.mErrors.getFirstError());
				    continue;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return true;
    }

    //批量续期处理
    /**
         private boolean dealData()
         {
      String Operator = tGI.Operator; //保存登陆管理员账号
      String ManageCom = tGI.ComCode; //保存登陆区站,ManageCom=内存中登陆区站代码

        //保存纪录的条数
         //    int recordCount = 0;
         //    int i = 0;
         //    int iMax = 0;
        String tLimit = "";
        String tNo = ""; //产生的通知书号
        String serNo = ""; //流水号
        //保存个人保单表的查询到的纪录数
        int queryCount = 0;

        //保存成功处理的纪录数
        int operSuccCount = 0;

        //事务处理时发生错误数
        int errorCount = 0;
       //添加符合条件的lccont从表lccont和lcpol中获取
       String CurrentDate2="";
       //默认是系统第二天
        if (CurrentDate2 == null||CurrentDate2.equals(""))
        {
            CurrentDate2 = PubFun.getCurrentDate();
        }
        String AheadDays = ""; //催收提前天数--默认30天
        LDSysVarDB tLDSysVarDB2 = new LDSysVarDB();
        tLDSysVarDB2.setSysVar("aheaddays");

        if (tLDSysVarDB2.getInfo() == false)
        {
            AheadDays = "30";
        }
        else
        {
            AheadDays = tLDSysVarDB2.getSysVarValue();
        }
        //判断当前日期<=交至日期且当前日期+AheadDays>=交至日期
        FDate tD2 = new FDate();
        Date AfterDate = PubFun.calDate(tD2.getDate(CurrentDate2),Integer.parseInt(AheadDays),"D",null);
        String SubDate = tD2.getString(AfterDate);
        //因为存在已经过了催缴时间的保单-为简易操作-可以将系统变量-提前催收天数改为负数，则可以灵活处理-对个单和批量都有效
        if (tD2.getDate(CurrentDate2).before(tD2.getDate(SubDate)))
        {
            //如果系统变量-催收天数是正数，那么SubDate大于CurrentDate，则催收范围是>=CurrentDate&&<=SubDate
            System.out.println("Current Date:" + CurrentDate2);
            System.out.println("AfterDate:" + SubDate);
        }
        else
        {
            //如果系统变量-催收天数是负数，那么SubDate小于CurrentDate，则催收范围是<=CurrentDate&&>=SubDate
            //为了统一处理，则将CurrentDate和SubDate值互换,则后续催收范围保持为>=CurrentDate&&<=SubDate
            System.out.println("Current Date:" + CurrentDate2);
            System.out.println("BeforeDate:" + SubDate);
            CurrentDate2 = SubDate;
            SubDate = PubFun.getCurrentDate();
        }
        String sqlStr = "select * from lccont where contno in (select distinct contno from lcpol"
            + " where paytodate<payenddate" +
           " and exists (select riskcode from lmriskpay WHERE urgepayflag='Y' and lmriskpay.riskcode=lcpol.riskcode)" +
           " and (StopFlag='0' or StopFlag is null)" +
           " and not exists (SELECT polno FROM ljspayperson WHERE  ljspayperson.polno=lcpol.polno)" +
           " and paytodate>='" + CurrentDate + "' and paytodate<='" + SubDate +
           "' and appflag='1'" + " and grppolno='00000000000000000000'" +
           " and ManageCom like '" + ManageCom + "%'" + " and payintv>0)";
       System.out.println("in BL SQL=" + sqlStr);
       LCContDB tLCContDB=new LCContDB();
       mLCContSet=tLCContDB.executeQuery(sqlStr);
       if (tLCContDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCContDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "IndiDueFeeMultiBL";
            tError.functionName = "queryData";
            tError.errorMessage = "保单表与合同表联合查询失败!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();
            return false;
        }
        if (mLCContSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiDueFeeMultiBL";
            tError.functionName = "queryData";
            tError.errorMessage = "没有查询到符合条件的合同表!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();
            return false;
        }
      //  mLCContSet=tLCContDB.query();

        for(int j=1;j<=mLCContSet.size();j++)
        {
          VData saveData = new VData();
          VData tVData=new VData();
          String mSpecFlag = "";
          String  strNewLJSPayDate=null;
          String  strNewLJSStartDate=null;
          LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
          LJSPaySchema tLJSPaySchema = new LJSPaySchema();
          LCDutySet mLCDutySet = new LCDutySet();
          LCPremSet mLCPremSet = new LCPremSet();
          LJSPaySchema mLJSPaySchema = new LJSPaySchema();
          LOPRTManagerSubSet tLOPRTManagerSubSet= new LOPRTManagerSubSet();
          double contsumPay = 0;//保存合同上的累计和的变量
          LCContSchema mLCContSchema=mLCContSet.get(j);
          tLimit = PubFun.getNoLimit(ManageCom);
          serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
          tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
          tNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);
          //产生打印通知书号
          String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
          String bankCode = "";
          String bankAccNo = "";
          String accName = "";
          //判断个人保单表中是否有余额的标志
          boolean yelFlag = false; //默认没有
          //判断余额是否>(保费项表.实际保费*保险责任表.免交比率)累计和 的标志
          boolean yetFlag = false; //默认不是（yelFlag=true 是前提条件）
          //关于银行转账方式的的处理，作为关键点处理
          if (mLCContSchema.getPayMode() != null) {
          if (mLCContSchema.getPayLocation().equals("0") ||
                mLCContSchema.getPayLocation().equals("8"))
          {
            bankCode = mLCContSchema.getBankCode();
            bankAccNo = mLCContSchema.getBankAccNo();
            accName = mLCContSchema.getAccName();
          }
          }
          double LeavingMoney = mLCContSchema.getDif();
          double YetLeavingMoney=LeavingMoney;
          LCPolQueryBL tLCPolQueryBL = new LCPolQueryBL();
          LCPolSet tLCPolSet = new LCPolSet();
          tLCPolQueryBL.queryLCPol4(mLCContSchema);
     tLCPolSet = ( (LCPolSet) tLCPolQueryBL.getResult().getObjectByObjectName(
        "LCPolSet",
        0));
          for (int m = 1; m <= tLCPolSet.size(); m++)
          {
              LCPolSchema tLCPolSchema=tLCPolSet.get(m);
            //死亡报案的不用催收
               ExeSQL tExeSQL = new ExeSQL();
               String count = tExeSQL.getOneValue(
                       "select count(*) from ldsystrace WHERE polstate=4001 AND valiflag='1' and polno='" +
                       tLCPolSchema.getPolNo() + "'");

               if (tExeSQL.mErrors.needDealError())
               {
                   this.mErrors.addOneError("保单号:" + tLCPolSchema.getPolNo() +
                       "查询死亡报案时错误:" + tExeSQL.mErrors.getFirstError());
                   errorCount = errorCount + 1; //失败纪录数

                   continue;
               }

               if ((count != null) && !count.equals("0"))
               {
                   this.mErrors.addOneError("保单号:" + tLCPolSchema.getPolNo() +
                       "催收错误:已有死亡报案记录!");
                   errorCount = errorCount + 1; //失败纪录数

                   continue;
               }

               //如果正在做保全，不能催收
               tExeSQL = new ExeSQL();
               count = null;
               count = tExeSQL.getOneValue(
     "SELECT COUNT(*) FROM lpedormain WHERE edorstate<>'0' AND contno='" +
                       mLCContSchema.getContNo() + "'");

               if (tExeSQL.mErrors.needDealError())
               {
                   this.mErrors.addOneError("保单号:" + tLCPolSchema.getPolNo() +
                       "查询保全状态时错误:" + tExeSQL.mErrors.getFirstError());
                   errorCount = errorCount + 1; //失败纪录数

                   continue;
               }

               if ((count != null) && !count.equals("0"))
               {
                   this.mErrors.addOneError("保单号:" + tLCPolSchema.getPolNo() +
                       "催收错误:正在做保全!请手工催收!");
                   errorCount = errorCount + 1; //失败纪录数

                   continue;
               }

               //如果续期续保关联，且附加险续保，则置标记跳过
               if (checkRnew(tLCPolSchema) == true)
               {
                   continue;
               }

            //Step 1 : set variable
            LJSPayPersonSchema yelLJSPayPersonSchema; //如果有余额，添加一个应收个人交费schema
     LJSPayPersonSchema yetLJSPayPersonSchema; //如果余额>累计和，添加一个应收个人交费schema
            double sumPay = 0; //保存险种上的累计和的变量

            //保存纪录的条数
            int recordCount = 0;
            int i = 0;
            int iMax = 0;


            //Step 2 :query from LCPol table
        //    LCPolSchema tLCPolSchema = new LCPolSchema();
         //   tLCPolSchema = (LCPolSchema) tLCPolSet.get(j);
            String PolNo=tLCPolSchema.getPolNo();
            //计算新的交至日期
            System.out.println("原交至日期" + tLCPolSchema.getPaytoDate());
            FDate tD = new FDate();
            Date newBaseDate = new Date();
            String CurrentPayToDate = tLCPolSchema.getPaytoDate();
            //为总表取得最早交费日期,取所有集体险种中最大的
            if(strNewLJSStartDate==null||strNewLJSStartDate.equals(""))
            {
                strNewLJSStartDate=CurrentPayToDate;
            }
            else
            {
     strNewLJSStartDate=PubFun.getLaterDate(strNewLJSStartDate,CurrentPayToDate) ;
            }
            int PayInterval = tLCPolSchema.getPayIntv();
            newBaseDate = tD.getDate(CurrentPayToDate);

     Date NewPayToDate = PubFun.calDate(newBaseDate, PayInterval, "M", null);
            String strNewPayToDate = tD.getString(NewPayToDate);
            System.out.println("现交至日期" + strNewPayToDate);

            //交费日期=失效日期=原交至日期+2个月
            Date NewPayDate = PubFun.calDate(newBaseDate, 2, "M", null);
            String strNewPayDate = tD.getString(NewPayDate);
            //为总表取得交费日期,取所有集体险种中最小的
            if(strNewLJSPayDate==null)
            {
                strNewLJSPayDate=strNewPayDate;
            }
            else
            {
     strNewLJSPayDate=PubFun.getBeforeDate(strNewLJSPayDate,strNewPayDate) ;
            }
            //Step 3: query  from LJSPayPerson table
            tVData.clear();
            LJSPayPersonSchema tLJSPayPersonSchema = new LJSPayPersonSchema();
            tLJSPayPersonSchema.setPolNo(PolNo);
            tVData.add(tLJSPayPersonSchema);

            DuePayPersonFeeSimQueryBL tDuePayPersonFeeSimQueryBL = new
                DuePayPersonFeeSimQueryBL();
            if (!tDuePayPersonFeeSimQueryBL.submitData(tVData, "QUERY")) {
              this.mErrors.copyAllErrors(tDuePayPersonFeeSimQueryBL.mErrors);
              return false;
            }

            //Step 4:query from LCPrem table
            tVData.clear();
            LCPremSchema tLCPremSchema = new LCPremSchema();
            tLCPremSchema.setPolNo(PolNo);
            tLCPremSchema.setUrgePayFlag("Y");
            tVData.add(tLCPremSchema);

            LCPremQueryBL tLCPremQueryBL = new LCPremQueryBL();

            if (!tLCPremQueryBL.submitData(tVData, "QUERY")) {
              this.mErrors.copyAllErrors(tLCPremQueryBL.mErrors);

              return false;
            }

            tVData.clear();
            mLCPremSet = new LCPremSet();
            tVData = tLCPremQueryBL.getResult();

            //可能有多条纪录，所以保存记录集
     mLCPremSet.set( (LCPremSet) tVData.getObjectByObjectName("LCPremSet", 0));

            //Step 4 : query from LCDuty
            recordCount = mLCPremSet.size();
            mLCDutySet = new LCDutySet(); //保存保险责任表纪录集合

            LCDutySchema tLCDutySchema = new LCDutySchema();
            LCDutySet tempLCDutySet = new LCDutySet(); //临时存放保险责任表纪录集合的容器
            LCPremSet saveLCPremSet = new LCPremSet(); //保存过滤后的保费项纪录集

            for (i = 1; i <= recordCount; i++) {
              tLCPremSchema = new LCPremSchema();
              tLCPremSchema = (LCPremSchema) mLCPremSet.get(i);
              tLCDutySchema.setPolNo(tLCPremSchema.getPolNo());
              tLCDutySchema.setDutyCode(tLCPremSchema.getDutyCode());
              tVData.clear();
              tVData.add(tLCDutySchema);

              //*.java内置查询条件:免交比率<1.0
                  LCDutyQueryBL tLCDutyQueryBL = new LCDutyQueryBL();

                  if (!tLCDutyQueryBL.submitData(tVData, "QUERY")) {
                    System.out.println("没有查到符合条件的责任纪录");
                  }
                  else {
                    tVData.clear();
                    tVData = tLCDutyQueryBL.getResult();
      tempLCDutySet.set( (LCDutySet) tVData.getObjectByObjectName(
                        "LCDutySet", 0));
                    tLCDutySchema = (LCDutySchema) tempLCDutySet.get(1);
                    mLCDutySet.add(tLCDutySchema);
                    saveLCPremSet.add(tLCPremSchema);
                    sumPay = sumPay +
         ( (1 - tLCDutySchema.getFreeRate()) * tLCPremSchema.getPrem());
                  }
                }
                YetLeavingMoney=YetLeavingMoney-sumPay;//确定是否添加类型为"YET"的数据
                //end for()
                if (saveLCPremSet.size() == 0) { //如果过滤后的保费项表纪录数=0
                  this.mErrors.addOneError("查询保险责任表失败，原因是:都为免交");

                  return false;
                }


                //Step 5:
                yelLJSPayPersonSchema = new LJSPayPersonSchema();
                yetLJSPayPersonSchema = new LJSPayPersonSchema();

                //计算交费日期和现交至日期 而定义一些处理变量：
                Date baseDate = new Date();
                Date paytoDate = new Date(); //交至日期
                Date payDate = new Date(); //交费日期
                int interval = 0; //交费间隔
                String unit = ""; //日期单位（Y:年 M:月 D:日）
                FDate fDate = new FDate();

                //取一条保费项表纪录，将相关字段值赋给添加的应收个人表字段
                tLCPremSchema = (LCPremSchema) saveLCPremSet.get(1);
                System.out.println("余额是："+LeavingMoney);
                if (LeavingMoney > 0&&j==1) { //如果有余额，根据余额设置一条应收个人Schema
                  yelFlag = true; //设置标志位
                  yelLJSPayPersonSchema.setPolNo(PolNo);
                  yelLJSPayPersonSchema.setSumDuePayMoney( -LeavingMoney);
                  yelLJSPayPersonSchema.setSumActuPayMoney( -LeavingMoney);
         yelLJSPayPersonSchema.setPayCount(tLCPremSchema.getPayTimes() + 1);
         yelLJSPayPersonSchema.setGrpContNo(mLCContSchema.getGrpContNo());
      yelLJSPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                  yelLJSPayPersonSchema.setContNo(tLCPolSchema.getContNo());
       yelLJSPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
         yelLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
                  yelLJSPayPersonSchema.setGetNoticeNo(tNo); // 通知书号
                  yelLJSPayPersonSchema.setPayAimClass("1"); //交费目的分类=个人
                  yelLJSPayPersonSchema.setPayIntv(tLCPolSchema.getPayIntv());

                  //:交费日期=个人保单表的交至日期
                  yelLJSPayPersonSchema.setPayDate(strNewPayDate);
                  yelLJSPayPersonSchema.setPayType("YEL"); //交费类型=yel
                  yelLJSPayPersonSchema.setAppntNo(tLCPremSchema.getAppntNo());

                  //:这条纪录的//原交至日期=个人保单表的交至日期
                  //现交至日期CurPayToDate=个人保单表的交至日期+交费间隔 待修改
         yelLJSPayPersonSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());
                  yelLJSPayPersonSchema.setCurPayToDate(strNewPayToDate);
                  yelLJSPayPersonSchema.setBankCode(bankCode);
                  yelLJSPayPersonSchema.setBankAccNo(bankAccNo);
                  yelLJSPayPersonSchema.setBankOnTheWayFlag("0");
                  yelLJSPayPersonSchema.setBankSuccFlag("0");
         yelLJSPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());
         yelLJSPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());
        yelLJSPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
      yelLJSPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
        yelLJSPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
      yelLJSPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
                  yelLJSPayPersonSchema.setSerialNo(serNo); //统一一个流水号
                  yelLJSPayPersonSchema.setOperator(Operator);
                  yelLJSPayPersonSchema.setMakeDate(CurrentDate); //入机日期
                  yelLJSPayPersonSchema.setMakeTime(CurrentTime); //入机时间
                  yelLJSPayPersonSchema.setModifyDate(CurrentDate); //最后一次修改日期
                  yelLJSPayPersonSchema.setModifyTime(CurrentTime); //最后一次修改时间
        yelLJSPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
         yelLJSPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
                  mLJSPayPersonSet.add(yelLJSPayPersonSchema);
                }
      if (YetLeavingMoney > 0&&j==tLCPolSet.size()) { //如果个人保单中的余额>应收款，设置一条纪录
                    yetFlag = true; //设置标志位
                    yetLJSPayPersonSchema.setPolNo(PolNo);
         yetLJSPayPersonSchema.setPayCount(tLCPremSchema.getPayTimes() +
                                                      1);
         yetLJSPayPersonSchema.setGrpContNo(mLCContSchema.getGrpContNo());
        yetLJSPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                    yetLJSPayPersonSchema.setContNo(tLCPolSchema.getContNo());
         yetLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
         yetLJSPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
                    yetLJSPayPersonSchema.setSumDuePayMoney(YetLeavingMoney);
                    yetLJSPayPersonSchema.setSumActuPayMoney(YetLeavingMoney);
                    yetLJSPayPersonSchema.setGetNoticeNo(tNo); // 通知书号
                    yetLJSPayPersonSchema.setPayAimClass("1"); //交费目的分类=个人
      yetLJSPayPersonSchema.setPayIntv(tLCPolSchema.getPayIntv());

                    //: 交费日期=个人保单表的交至日期 待计算
                    yetLJSPayPersonSchema.setPayDate(strNewPayDate);
                    yetLJSPayPersonSchema.setPayType("YET"); //交费类型=YET
       yetLJSPayPersonSchema.setAppntNo(tLCPremSchema.getAppntNo());

                    //:这条纪录的//原交至日期=个人保单表的交至日期
                    //:现交至日期CurPayToDate=个人保单表的交至日期
         yetLJSPayPersonSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());
                    yetLJSPayPersonSchema.setCurPayToDate(strNewPayToDate);
                    yetLJSPayPersonSchema.setBankCode(bankCode);
                    yetLJSPayPersonSchema.setBankAccNo(bankAccNo);
                    yetLJSPayPersonSchema.setBankOnTheWayFlag("0");
                    yetLJSPayPersonSchema.setBankSuccFlag("0");
         yetLJSPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());
         yetLJSPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());
         yetLJSPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
        yetLJSPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
         yetLJSPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
        yetLJSPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
                    yetLJSPayPersonSchema.setSerialNo(serNo); //统一一个流水号
                    yetLJSPayPersonSchema.setOperator(Operator);
                    yetLJSPayPersonSchema.setMakeDate(CurrentDate); //入机日期
                    yetLJSPayPersonSchema.setMakeTime(CurrentTime); //入机时间
      yetLJSPayPersonSchema.setModifyDate(CurrentDate); //最后一次修改日期
      yetLJSPayPersonSchema.setModifyTime(CurrentTime); //最后一次修改时间
         yetLJSPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
         yetLJSPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
                    //添加一条纪录
                    mLJSPayPersonSet.add(yetLJSPayPersonSchema);
                  }

                //因此，mLCPremSet中记录总是=mLCDutySet中的纪录，这两个记录集中相同序号的纪录是一一对应的.
                //这将有助于我们根据mLCDutySet的记录数，循环处理（两个记录集中记录序号是对应的）
                //在整理应收个人表纪录时，添加完保费项集合中的纪录后，不要忘了判断：yelFlag和yetFlag
                //根据判断，添加 yelLJSPayPersonSchema ，yetLJSPayPersonSchema 到应收个人纪录，并纪录到应收总表中
                //Step 6 : transaction:将个人保单纪录，保费项表集合，保险责任表集合整理成应收总表纪录，应收个人表集合
                tLJSPayPersonSchema = new LJSPayPersonSchema();
                for (int index = 1; index <= saveLCPremSet.size(); index++) {
                  tLJSPayPersonSchema = new LJSPayPersonSchema();
                  tLCDutySchema = new LCDutySchema();
                  tLCPremSchema = saveLCPremSet.get(index); //保费项纪录
                  tLCDutySchema = mLCDutySet.get(index); //保险责任纪录

                  //设置应收个人表(从个人保单，保费项，保险责任中取)
                  tLJSPayPersonSchema = new LJSPayPersonSchema();
                  tLJSPayPersonSchema.setPolNo(PolNo);
         tLJSPayPersonSchema.setPayCount(tLCPremSchema.getPayTimes() + 1);
       tLJSPayPersonSchema.setGrpContNo(mLCContSchema.getGrpContNo());
                  tLJSPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                  tLJSPayPersonSchema.setContNo(tLCPolSchema.getContNo());
                  tLJSPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());
                  tLJSPayPersonSchema.setGetNoticeNo(tNo); //通知书号
                  tLJSPayPersonSchema.setPayAimClass("1"); //交费目的分类=个人
                  tLJSPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
         tLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
         tLJSPayPersonSchema.setSumDuePayMoney(tLCPremSchema.getPrem() * (1 -
                      tLCDutySchema.getFreeRate()));
         tLJSPayPersonSchema.setSumActuPayMoney(tLCPremSchema.getPrem() * (1 -
                      tLCDutySchema.getFreeRate()));
                  tLJSPayPersonSchema.setPayIntv(tLCPremSchema.getPayIntv());

                  //-------------------------------------------------------
                  // 交费日期=失效日期 =现交至日期+2个月 （待修订）repair:
                  //下面计算 现交至日期
                  baseDate = fDate.getDate(tLCPremSchema.getPaytoDate());
                  interval = tLCPremSchema.getPayIntv();

                  if (interval == -1) {
                    interval = 0; // 不定期缴费
                  }

                  unit = "M";

                  //repair:日期不能为空，否则下面的函数处理会有问题
                  String strPayDate = "";
                  String strPayToDate = "";

                  if (baseDate != null) {
                    //针对每个保费项，计算新的交至日期
                    paytoDate = PubFun.calDate(baseDate, interval, unit, null);
                    System.out.println("交至日期:" + paytoDate);

                    //计算交费日期=失效日期=旧的交至日期+2个月
                    payDate = PubFun.calDate(baseDate, 2, unit, null);
                    System.out.println("失效日期:" + payDate);

                    //得到日期型，转换成String型
                    strPayToDate = fDate.getString(paytoDate);
                    strPayDate = fDate.getString(payDate);
                  }

                  tLJSPayPersonSchema.setPayDate(strPayDate);
                  tLJSPayPersonSchema.setPayType("ZC"); //交费类型=ZC即"正常"
         tLJSPayPersonSchema.setLastPayToDate(tLCPremSchema.getPaytoDate());

                  //原交至日期=保费项表的交至日期
                  //现交至日期CurPayToDate=交至日期+日期（interval=12 12个月-年交，interval=1 即1个月-月交）待修改
                  tLJSPayPersonSchema.setCurPayToDate(strPayToDate);
                  tLJSPayPersonSchema.setBankCode(bankCode);
                  tLJSPayPersonSchema.setBankAccNo(bankAccNo);
                  tLJSPayPersonSchema.setBankOnTheWayFlag("0");
                  tLJSPayPersonSchema.setBankSuccFlag("0");
         tLJSPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());
         tLJSPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());
      tLJSPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
                  tLJSPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
      tLJSPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
                  tLJSPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
                  tLJSPayPersonSchema.setSerialNo(serNo); //统一一个流水号
                  tLJSPayPersonSchema.setOperator(Operator);
                  tLJSPayPersonSchema.setMakeDate(CurrentDate); //入机日期
                  tLJSPayPersonSchema.setMakeTime(CurrentTime); //入机时间
                  tLJSPayPersonSchema.setModifyDate(CurrentDate); //最后一次修改日期
                  tLJSPayPersonSchema.setModifyTime(CurrentTime); //最后一次修改时间
      tLJSPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
        tLJSPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());

                  mLJSPayPersonSet.add(tLJSPayPersonSchema);
                }
                  contsumPay=contsumPay+sumPay;
                  //杨红添加开始
                  if(tLOPRTManagerSubSet==null||tLOPRTManagerSubSet.size()==0)
                  {
                    LOPRTManagerSubSchema tLOPRTManagerSubSchema = new
                        LOPRTManagerSubSchema();
                    tLOPRTManagerSubSchema.setPrtSeq(prtSeqNo);
                    tLOPRTManagerSubSchema.setGetNoticeNo(tNo);
       tLOPRTManagerSubSchema.setOtherNo(mLCContSchema.getContNo());
                    tLOPRTManagerSubSchema.setOtherNoType("00");
         tLOPRTManagerSubSchema.setRiskCode(tLCPolSchema.getRiskCode());
                    tLOPRTManagerSubSchema.setDuePayMoney(sumPay);
         tLOPRTManagerSubSchema.setAppntName(mLCContSchema.getAppntName());
                    tLOPRTManagerSubSchema.setTypeFlag("1");
                    tLOPRTManagerSubSet.add(tLOPRTManagerSubSchema);
                  }
                  else
                  {
                    LOPRTManagerSubSchema tLOPRTManagerSubSchema = new
                        LOPRTManagerSubSchema();
                    boolean isRepeatFlag=false;
                    for(int n=1;n<=tLOPRTManagerSubSet.size();n++)
                    {
      if(tLOPRTManagerSubSet.get(n).getRiskCode().equals(tLCPolSchema.getRiskCode()))
                      {
                        isRepeatFlag=true;
                        tLOPRTManagerSubSet.get(n).setDuePayMoney(tLOPRTManagerSubSet.get(n).getDuePayMoney()+sumPay);
                        break;
                      }
                    }
                    if(!isRepeatFlag)
                    {
                      tLOPRTManagerSubSchema.setPrtSeq(prtSeqNo);
                    tLOPRTManagerSubSchema.setGetNoticeNo(tNo);
       tLOPRTManagerSubSchema.setOtherNo(mLCContSchema.getContNo());
                    tLOPRTManagerSubSchema.setOtherNoType("00");
         tLOPRTManagerSubSchema.setRiskCode(tLCPolSchema.getRiskCode());
                    tLOPRTManagerSubSchema.setDuePayMoney(sumPay);
         tLOPRTManagerSubSchema.setAppntName(mLCContSchema.getAppntName());
                    tLOPRTManagerSubSchema.setTypeFlag("1");
                    tLOPRTManagerSubSet.add(tLOPRTManagerSubSchema);

                    }
                  }

              }
              //根据不同情况，添加应收总表的应收总额
           System.out.println("现在的标志yelfalg:"+yelFlag);
         System.out.println("现在的标志yetfalg:"+yetFlag);
            if (yelFlag) {
         //添加一条纪录
         if (yetFlag) {
           //添加应收总表 =0
           tLJSPaySchema.setSumDuePayMoney(0);
         }
         else {
           tLJSPaySchema.setSumDuePayMoney(contsumPay - LeavingMoney);
         }
            }
            else {
         tLJSPaySchema.setSumDuePayMoney(contsumPay);
            }

            tLJSPaySchema.setGetNoticeNo(tNo); //通知书号
            tLJSPaySchema.setOtherNo(mLCContSchema.getContNo());
            tLJSPaySchema.setOtherNoType("2");
            tLJSPaySchema.setAppntNo(mLCContSchema.getAppntNo());

            tLJSPaySchema.setPayDate(strNewLJSPayDate);
       tLJSPaySchema.setStartPayDate(strNewLJSStartDate); //交费最早应缴日期保存上次交至日期
            tLJSPaySchema.setBankOnTheWayFlag("0");
            tLJSPaySchema.setBankSuccFlag("0");
            tLJSPaySchema.setSendBankCount(0); //送银行次数
            tLJSPaySchema.setApproveCode(mLCContSchema.getApproveCode());
            tLJSPaySchema.setApproveDate(mLCContSchema.getApproveDate());
            tLJSPaySchema.setRiskCode("000000");
            tLJSPaySchema.setBankAccNo(bankAccNo);
            tLJSPaySchema.setBankCode(bankCode);
            tLJSPaySchema.setSendBankCount(0);
            tLJSPaySchema.setSerialNo(serNo); //流水号
            tLJSPaySchema.setOperator(Operator);
            tLJSPaySchema.setManageCom(mLCContSchema.getManageCom());
            tLJSPaySchema.setAgentCom(mLCContSchema.getAgentCom());
            tLJSPaySchema.setAgentCode(mLCContSchema.getAgentCode());
            tLJSPaySchema.setAgentType(mLCContSchema.getAgentType());
            tLJSPaySchema.setAgentGroup(mLCContSchema.getAgentGroup());
            tLJSPaySchema.setAccName(accName);
            tLJSPaySchema.setMakeDate(CurrentDate);
            tLJSPaySchema.setMakeTime(CurrentTime);
            tLJSPaySchema.setModifyDate(CurrentDate);
            tLJSPaySchema.setModifyTime(CurrentTime);

      VData prtData = getPrintData(mLCContSchema, tLJSPaySchema,prtSeqNo);

            //    LOPRTManagerSchema tLOPRTManagerSchema=getPrintData(tLCPolSchema,tLJSPaySchema);
            if (prtData == null) {
         CError tError = new CError();
         tError.moduleName = "AssignBonus";
         tError.functionName = "getByCash";
         tError.errorMessage = "打印数据准备失败!";
         this.mErrors.addOneError(tError);

         return false;
            }

        LOPRTManagerSchema tLOPRTManagerSchema = (LOPRTManagerSchema) prtData.
           getObjectByObjectName("LOPRTManagerSchema",
                                 0);
        LOPRTManagerSubSchema tLOPRTManagerSubSchema = (LOPRTManagerSubSchema)
           prtData.getObjectByObjectName("LOPRTManagerSubSchema",
                                         0);

            //处理特殊的应收个人表纪录。譬如加费，增额交清产生的保费项对应的应收个人表纪录，将同险种的合并，这样财务交费才能录入
            //dealSpecLJSPayPerson(mLJSPayPersonSet);//改在财务处理
            tVData.clear();
            tVData.add(tLJSPaySchema);
            tVData.add(mLJSPayPersonSet);
            tVData.add(tLOPRTManagerSchema);
            tVData.add(tLOPRTManagerSubSet);

            String autoVerifyFlag = "N";
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("AutoVerifyStation");

            if (tLDSysVarDB.getInfo() == true) {
         //如果系统变量为催收时核销并且应收总表应收款=0，那么催收时核销
         //AutoVerifyStation 自动核销的位置--即续期交费时，保单余额足够支付保费且选择为抵交续期保费时的自动核销
         //1 财务核销时处理 2 催收时处理
         if (tLDSysVarDB.getSysVarValue().equals("2") &&
             (tLJSPaySchema.getSumDuePayMoney() == 0)) {
           autoVerifyFlag = "Y";
         }
            }

            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("autoVerifyFlag", autoVerifyFlag);
            tVData.add(tTransferData);

            saveData = tVData;
            System.out.println("After dealData！");

            IndiDueFeeBLS tIndiDueFeeBLS = new IndiDueFeeBLS();

            if(tIndiDueFeeBLS.submitData(saveData, "INSERT"))
            {
         operSuccCount++;
            }




             }
             if(operSuccCount<mLCContSet.size())
             {
          // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "IndiDueFeeBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "部分数据添加失败!";
                    this.mErrors .addOneError(tError) ;
                    return false;
             }
          return true;
             }*/
     //根据前面的输入数据，进行逻辑处理
     //如果在处理过程中出错，则返回false,否则返回true
     /**
      private boolean dealData()
      {
          String PolNo = "";
          String Operator = tGI.Operator; //保存登陆管理员账号
          String ManageCom = tGI.ComCode; //保存登陆区站,ManageCom=内存中登陆区站代码

          //判断个人保单表中是否有余额的标志
          boolean yelFlag = false; //默认没有
          LJSPayPersonSchema yelLJSPayPersonSchema; //如果有余额，添加一个应收个人交费schema
          double LeavingMoney = 0; //如果有余额，保存余额

          //判断余额是否>(保费项表.实际保费*保险责任表.免交比率)累计和 的标志
          boolean yetFlag = false; //默认不是（yelFlag=true 是前提条件）
          LJSPayPersonSchema yetLJSPayPersonSchema; //如果余额>累计和，添加一个应收个人交费schema
          double sumPay = 0; //保存累计和的变量

          //保存纪录的条数
          int recordCount = 0;
          int i = 0;
          int iMax = 0;
          String tLimit = "";
          String tNo = ""; //产生的通知书号
          String serNo = ""; //流水号

          //保存个人保单表的查询到的纪录数
          int queryCount = 0;

          //保存成功处理的纪录数
          int operCount = 0;

          //事务处理时发生错误数
          int errorCount = 0;

          //Step 2 :query from LCPol table
          LCPolSchema tLCPolSchema = new LCPolSchema();
          tLCPolSchema.setManageCom(ManageCom);
          tLCPolSchema.setRiskCode("Y"); //设置催交标志为Y
          tLCPolSchema.setPayIntv(-1); //设置交费间隔不等于0:注意是!=0

          VData tVData = new VData();
          tVData.add(tLCPolSchema);

          if (mTransferData != null)
          {
              tVData.add(mTransferData);
          }

          LCPolQueryBL tLCPolQueryBL = new LCPolQueryBL();

          if (!tLCPolQueryBL.submitData(tVData, "QUERY"))
          {
              this.mErrors.copyAllErrors(tLCPolQueryBL.mErrors);

              return false;
          }

          tVData.clear();
          mLCPolSet = new LCPolSet();
          tVData = tLCPolQueryBL.getResult();
      mLCPolSet.set((LCPolSet) tVData.getObjectByObjectName("LCPolSet", 0));

          //增加特约的判断，add by Minim ,repair by hzm
          //3种情况：1 保单全部催收 2 只催收有特约的保单 3 只催收没有特约的保单
          //如果mDuebySpecFlag为真，根据mSpecFlag标记判断是催收有特约的还是没有特约的保单
          //否则保单全部催收
          try
          {
              System.out.println("Start 特约的判断");

              if (mDuebySpecFlag)
              {
                  LCSpecDB tLCSpecDB;
                  LCSpecSet tLCSpecSet;

                  for (int k = 0; k < mLCPolSet.size(); k++)
                  {
                      //获取特约记录
                      tLCSpecDB = new LCSpecDB();
                      tLCSpecDB.setPolNo(mLCPolSet.get(k + 1).getPolNo());
                      tLCSpecSet = tLCSpecDB.query();

                      //如果不催收特约单，那么有特约的保单剔除
                      if ((tLCSpecSet.size() > 0) && !mSpecFlag.equals("1"))
                      {
                          mLCPolSet.remove(mLCPolSet.get(k + 1));
                          k--;
                      }
                      else
                      //如果催收特约单，那么没有特约的保单剔除.
                      if ((tLCSpecSet.size() == 0) && mSpecFlag.equals("1"))
                      {
                          mLCPolSet.remove(mLCPolSet.get(k + 1));
                          k--;
                      }
                  }
              }
          }
          catch (Exception ex)
          {
              ex.printStackTrace();
          }

          queryCount = mLCPolSet.size();

          //计算新的交至日期
          FDate tD = new FDate();
          Date newBaseDate = new Date();
          String CurrentPayToDate = "";
          int PayInterval = 0;
          Date NewPayToDate = new Date();
          Date NewPayDate = new Date();
          String strNewPayToDate = "";
          String strNewPayDate = "";
          String bankCode = "";
          String bankAccNo = "";
          String accName = "";

          // 产生统一流水号
          tLimit = PubFun.getNoLimit(ManageCom);
          serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

          try
          {
              //批量处理:
              for (int n = 1; n <= queryCount; n++)
              {
                  yetFlag = false;
                  yelFlag = false;
                  LeavingMoney = 0;
                  sumPay = 0.0;
                  bankCode = "";
                  bankAccNo = "";
                  accName = "";
                  tLCPolSchema = (LCPolSchema) mLCPolSet.get(n); //保存后用,序号从1开始

                  //死亡报案的不用催收
                  ExeSQL tExeSQL = new ExeSQL();
                  String count = tExeSQL.getOneValue(
                          "select count(*) from ldsystrace WHERE polstate=4001 AND valiflag='1' and polno='" +
                          tLCPolSchema.getPolNo() + "'");

                  if (tExeSQL.mErrors.needDealError())
                  {
      this.mErrors.addOneError("保单号:" + tLCPolSchema.getPolNo() +
                          "查询死亡报案时错误:" + tExeSQL.mErrors.getFirstError());
                      errorCount = errorCount + 1; //失败纪录数

                      continue;
                  }

                  if ((count != null) && !count.equals("0"))
                  {
      this.mErrors.addOneError("保单号:" + tLCPolSchema.getPolNo() +
                          "催收错误:已有死亡报案记录!");
                      errorCount = errorCount + 1; //失败纪录数

                      continue;
                  }

                  //如果正在做保全，不能催收
                  tExeSQL = new ExeSQL();
                  count = null;
                  count = tExeSQL.getOneValue(
      "SELECT COUNT(*) FROM lpedormain WHERE edorstate<>'0' AND polno='" +
                          tLCPolSchema.getPolNo() + "'");

                  if (tExeSQL.mErrors.needDealError())
                  {
      this.mErrors.addOneError("保单号:" + tLCPolSchema.getPolNo() +
                          "查询保全状态时错误:" + tExeSQL.mErrors.getFirstError());
                      errorCount = errorCount + 1; //失败纪录数

                      continue;
                  }

                  if ((count != null) && !count.equals("0"))
                  {
      this.mErrors.addOneError("保单号:" + tLCPolSchema.getPolNo() +
                          "催收错误:正在做保全!请手工催收!");
                      errorCount = errorCount + 1; //失败纪录数

                      continue;
                  }

                  //如果续期续保关联，且附加险续保，则置标记跳过
                  if (checkRnew(tLCPolSchema) == true)
                  {
                      continue;
                  }

                  PolNo = tLCPolSchema.getPolNo();
                  System.out.println("原交至日期" + tLCPolSchema.getPaytoDate());
                  CurrentPayToDate = tLCPolSchema.getPaytoDate();
                  PayInterval = tLCPolSchema.getPayIntv();
                  newBaseDate = tD.getDate(CurrentPayToDate);
                  NewPayToDate = PubFun.calDate(newBaseDate, PayInterval, "M",
                          null);
                  strNewPayToDate = tD.getString(NewPayToDate);
                  System.out.println("现交至日期" + strNewPayToDate);

                  //交费日期=失效日期=原交至日期+2个月
                  NewPayDate = PubFun.calDate(newBaseDate, 2, "M", null);
                  strNewPayDate = tD.getString(NewPayDate);

                  if (tLCPolSchema.getPayLocation() != null)
                  {
                    Lis5.3 upgrade set
                      if (tLCPolSchema.getPayLocation().equals("0") ||
                              tLCPolSchema.getPayLocation().equals("8"))
                      {
                          bankCode = tLCPolSchema.getBankCode();
                          bankAccNo = tLCPolSchema.getBankAccNo();
                          accName = tLCPolSchema.getAccName();
                      }

                  }

                  //Step 3: query  from LJSPayPerson table
                  tVData.clear();

      LJSPayPersonSchema tLJSPayPersonSchema = new LJSPayPersonSchema();
                  tLJSPayPersonSchema.setPolNo(PolNo);
                  tVData.add(tLJSPayPersonSchema);

                  DuePayPersonFeeSimQueryBL tDuePayPersonFeeSimQueryBL = new DuePayPersonFeeSimQueryBL();

                  if (!tDuePayPersonFeeSimQueryBL.submitData(tVData, "QUERY"))
                  {
                      continue;
                  }

                  //Step 4:query from LCPrem table
                  tVData.clear();

                  LCPremSchema tLCPremSchema = new LCPremSchema();
                  tLCPremSchema.setPolNo(PolNo);
                  tLCPremSchema.setUrgePayFlag("Y");
                  tVData.add(tLCPremSchema);

                  LCPremQueryBL tLCPremQueryBL = new LCPremQueryBL();

                  if (!tLCPremQueryBL.submitData(tVData, "QUERY"))
                  {
                      continue;
                  }

                  tVData.clear();
                  mLCPremSet = new LCPremSet();
                  tVData = tLCPremQueryBL.getResult();
                  mLCPremSet.set((LCPremSet) tVData.getObjectByObjectName(
                          "LCPremSet", 0));
                  System.out.println("2222222222222222222222");

                  //Step 4 : query from LCDuty
                  recordCount = mLCPremSet.size();
                  mLCDutySet = new LCDutySet(); //保存保险责任表纪录集合

                  LCDutySchema tLCDutySchema = new LCDutySchema();
                  LCDutySet tempLCDutySet = new LCDutySet(); //临时存放保险责任表纪录集合的容器
                  LCPremSet saveLCPremSet = new LCPremSet(); //保存过滤后的保费项纪录集

                  for (i = 1; i <= recordCount; i++)
                  {
                      tLCPremSchema = new LCPremSchema();
                      tLCPremSchema = (LCPremSchema) mLCPremSet.get(i);
                      tLCDutySchema.setPolNo(tLCPremSchema.getPolNo());
                      tLCDutySchema.setDutyCode(tLCPremSchema.getDutyCode());
                      tVData.clear();
                      tVData.add(tLCDutySchema);

                      //java内置查询条件:免交比率<1.0
                      LCDutyQueryBL tLCDutyQueryBL = new LCDutyQueryBL();

                      if (!tLCDutyQueryBL.submitData(tVData, "QUERY"))
                      {
                          System.out.println("没有查到符合条件的责任纪录");
                      }
                      else
                      {
                          tVData.clear();
                          tVData = tLCDutyQueryBL.getResult();
      tempLCDutySet.set((LCDutySet) tVData.getObjectByObjectName(
                                  "LCDutySet", 0));
                          tLCDutySchema = (LCDutySchema) tempLCDutySet.get(1);

                          //保费项对应的催收选项。并且保费项是和应收个人表对应，因此催收时需要用保费项为主线
                          //一条责任项对应一条或多条保费项。因此通过保费项去查询责任表，可能会保存重复的责任项，
                          //不过，保存的责任项只是为保费项对应的应收个人表填充数据，因此是不会影响的。
                          mLCDutySet.add(tLCDutySchema);
                          saveLCPremSet.add(tLCPremSchema);
                          sumPay = sumPay +
      ((1 - tLCDutySchema.getFreeRate()) * tLCPremSchema.getPrem());
                      }
                  }

                  //end for()
                  if (saveLCPremSet.size() == 0) //如果过滤后的保费项表纪录数=0
                  {
                      continue;
                  }

                  // 产生通知书号
                  tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
                  tNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);

                  //Step 5:
                  yelLJSPayPersonSchema = new LJSPayPersonSchema();
                  yetLJSPayPersonSchema = new LJSPayPersonSchema();

                  //计算交费日期和现交至日期 而定义一些处理变量：
                  Date baseDate = new Date();
                  Date paytoDate = new Date(); //交至日期
                  Date payDate = new Date(); //交费日期
                  int interval = 0; //交费间隔
                  String unit = ""; //日期单位（Y:年 M:月 D:日）
                  FDate fDate = new FDate();

                  //取一条保费项表纪录，将相关字段值赋给添加的应收个人表字段
                  tLCPremSchema = (LCPremSchema) saveLCPremSet.get(1);

      if (tLCPolSchema.getLeavingMoney() > 0) //如果有余额，根据余额设置一条应收个人Schema
                  {
                      yelFlag = true;
                      LeavingMoney = tLCPolSchema.getLeavingMoney();
                      yelLJSPayPersonSchema.setPolNo(PolNo);
      yelLJSPayPersonSchema.setSumDuePayMoney(-tLCPolSchema.getLeavingMoney());
      yelLJSPayPersonSchema.setSumActuPayMoney(-tLCPolSchema.getLeavingMoney());
      yelLJSPayPersonSchema.setPayCount(tLCPremSchema.getPayTimes() +
                          1);
      yelLJSPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
      yelLJSPayPersonSchema.setContNo(tLCPolSchema.getContNo());
      yelLJSPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
      yelLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
                      yelLJSPayPersonSchema.setGetNoticeNo(tNo); // 通知书号
                      yelLJSPayPersonSchema.setPayAimClass("1"); //交费目的分类=个人
      yelLJSPayPersonSchema.setPayIntv(tLCPolSchema.getPayIntv());

                      //repair:交费日期=个人保单表的交至日期
                      yelLJSPayPersonSchema.setPayDate(strNewPayDate);
                      yelLJSPayPersonSchema.setPayType("YEL"); //交费类型=yel
      yelLJSPayPersonSchema.setAppntNo(tLCPremSchema.getAppntNo());

                      //现交至日期CurPayToDate=个人保单表的交至日期+交费间隔
      yelLJSPayPersonSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());
                      yelLJSPayPersonSchema.setCurPayToDate(strNewPayToDate);
                      yelLJSPayPersonSchema.setBankCode(bankCode);
                      yelLJSPayPersonSchema.setBankAccNo(bankAccNo);
                      yelLJSPayPersonSchema.setBankOnTheWayFlag("0");
                      yelLJSPayPersonSchema.setBankSuccFlag("0");
      yelLJSPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());
      yelLJSPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());
      yelLJSPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
      yelLJSPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
      yelLJSPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
      yelLJSPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
                      yelLJSPayPersonSchema.setSerialNo(serNo); //统一一个流水号
                      yelLJSPayPersonSchema.setOperator(Operator);
      yelLJSPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
      yelLJSPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
                  }

                  if (yelFlag) //如果个人保单有余额
                  {
                      if (LeavingMoney > sumPay) //如果个人保单中的余额>应收款，设置一条纪录
                      {
                          yetFlag = true; //设置标志位
                          yetLJSPayPersonSchema.setPolNo(PolNo);
      yetLJSPayPersonSchema.setPayCount(tLCPremSchema.getPayTimes() +
                              1);
      yetLJSPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
      yetLJSPayPersonSchema.setContNo(tLCPolSchema.getContNo());
      yetLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
      yetLJSPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
      yetLJSPayPersonSchema.setSumDuePayMoney(LeavingMoney -
                              sumPay);
      yetLJSPayPersonSchema.setSumActuPayMoney(LeavingMoney -
                              sumPay);
                          yetLJSPayPersonSchema.setGetNoticeNo(tNo); // 产生通知书号
      yetLJSPayPersonSchema.setPayAimClass("1"); //交费目的分类=个人
      yetLJSPayPersonSchema.setPayIntv(tLCPolSchema.getPayIntv());
                          yetLJSPayPersonSchema.setPayDate(strNewPayDate);
                          yetLJSPayPersonSchema.setPayType("YET"); //交费类型=YET
      yetLJSPayPersonSchema.setAppntNo(tLCPremSchema.getAppntNo());
      yetLJSPayPersonSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());
      yetLJSPayPersonSchema.setCurPayToDate(strNewPayToDate);
                          yetLJSPayPersonSchema.setBankCode(bankCode);
                          yetLJSPayPersonSchema.setBankAccNo(bankAccNo);
                          yetLJSPayPersonSchema.setBankOnTheWayFlag("0");
                          yetLJSPayPersonSchema.setBankSuccFlag("0");
      yetLJSPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());
      yetLJSPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());
      yetLJSPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
      yetLJSPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
      yetLJSPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
      yetLJSPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
                          yetLJSPayPersonSchema.setSerialNo(serNo); //统一一个流水号
                          yetLJSPayPersonSchema.setOperator(Operator);
      yetLJSPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
      yetLJSPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
                      }
                  }

                  //因此，mLCPremSet中记录总是=mLCDutySet中的纪录，这两个记录集中相同序号的纪录是一一对应的.
                  //这将有助于我们根据mLCDutySet的记录数，循环处理（两个记录集中记录序号是对应的）
                  //在整理应收个人表纪录时，添加完保费项集合中的纪录后，不要忘了判断：yelFlag和yetFlag
                  //根据判断，添加 yelLJSPayPersonSchema ，yetLJSPayPersonSchema 到应收个人纪录，并纪录到应收总表中
                  //Step 6 : transaction:将个人保单纪录，保费项表集合，保险责任表集合整理成应收总表纪录，应收个人表集合
                  tLJSPayPersonSchema = new LJSPayPersonSchema();
                  mLJSPayPersonSet = new LJSPayPersonSet();

                  LJSPaySchema tLJSPaySchema = new LJSPaySchema();

                  for (int index = 1; index <= saveLCPremSet.size(); index++)
                  {
                      tLJSPayPersonSchema = new LJSPayPersonSchema();
                      tLCDutySchema = new LCDutySchema();
                      tLCPremSchema = saveLCPremSet.get(index); //保费项纪录
                      tLCDutySchema = mLCDutySet.get(index); //保险责任纪录

                      //设置应收个人表(从个人保单，保费项，保险责任中取)
                      tLJSPayPersonSchema = new LJSPayPersonSchema();
                      tLJSPayPersonSchema.setPolNo(PolNo);
      tLJSPayPersonSchema.setPayCount(tLCPremSchema.getPayTimes() +
                          1);
      tLJSPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                      tLJSPayPersonSchema.setContNo(tLCPolSchema.getContNo());
      tLJSPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());
                      tLJSPayPersonSchema.setGetNoticeNo(tNo);
                      tLJSPayPersonSchema.setPayAimClass("1"); //交费目的分类=个人
      tLJSPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
      tLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
      tLJSPayPersonSchema.setSumDuePayMoney(tLCPremSchema.getPrem() * (1 -
                          tLCDutySchema.getFreeRate()));
      tLJSPayPersonSchema.setSumActuPayMoney(tLCPremSchema.getPrem() * (1 -
                          tLCDutySchema.getFreeRate()));
      tLJSPayPersonSchema.setPayIntv(tLCPremSchema.getPayIntv());

                      //-------------------------------------------------------
                      // 交费日期=失效日期 =现交至日期+2个月 （待修订）repair:
                      //下面计算 现交至日期
                      baseDate = fDate.getDate(tLCPremSchema.getPaytoDate());
                      interval = tLCPremSchema.getPayIntv();

                      if (interval == -1)
                      {
                          interval = 0; // 不定期缴费
                      }

                      unit = "M";

                      //repair:日期不能为空，否则下面的函数处理会有问题
                      String strPayDate = "";
                      String strPayToDate = "";

                      if (baseDate != null)
                      {
                          //针对每个保费项，计算新的交至日期
                          paytoDate = PubFun.calDate(baseDate, interval, unit,
                                  null);

                          //计算交费日期=失效日期=旧的交至日期+2个月
                          payDate = PubFun.calDate(baseDate, 2, unit, null);

                          //得到日期型，转换成String型
                          strPayToDate = fDate.getString(paytoDate);
                          strPayDate = fDate.getString(payDate);
                      }

                      tLJSPayPersonSchema.setPayDate(strPayDate);
                      tLJSPayPersonSchema.setPayType("ZC"); //交费类型=ZC即"正常"
      tLJSPayPersonSchema.setLastPayToDate(tLCPremSchema.getPaytoDate());

                      //原交至日期=保费项表的交至日期
                      //现交至日期CurPayToDate=交至日期+日期（interval=12 12个月-年交，interval=1 即1个月-月交）待修改
                      tLJSPayPersonSchema.setCurPayToDate(strPayToDate);
                      tLJSPayPersonSchema.setBankCode(bankCode);
                      tLJSPayPersonSchema.setBankAccNo(bankAccNo);
                      tLJSPayPersonSchema.setBankOnTheWayFlag("0");
                      tLJSPayPersonSchema.setBankSuccFlag("0");
      tLJSPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());
      tLJSPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());

      tLJSPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
      tLJSPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
      tLJSPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
      tLJSPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());

                      tLJSPayPersonSchema.setSerialNo(serNo); //统一一个流水号
                      tLJSPayPersonSchema.setOperator(Operator);
      tLJSPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
      tLJSPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
                      mLJSPayPersonSet.add(tLJSPayPersonSchema);
                  }

                  //根据不同情况，添加应收总表的应收总额
                  if (yelFlag)
                  {
                      //添加一条纪录
                      mLJSPayPersonSet.add(yelLJSPayPersonSchema);

                      if (yetFlag)
                      {
                          //添加一条纪录
                          mLJSPayPersonSet.add(yetLJSPayPersonSchema);

                          //添加应收总表 =0
                          tLJSPaySchema.setSumDuePayMoney(0);
                      }
                      else
                      {
      tLJSPaySchema.setSumDuePayMoney(sumPay - LeavingMoney);
                      }
                  }
                  else
                  {
                      tLJSPaySchema.setSumDuePayMoney(sumPay);
                  }

                  tLJSPaySchema.setGetNoticeNo(tNo); // 通知书号
                  tLJSPaySchema.setOtherNo(PolNo);
                  tLJSPaySchema.setOtherNoType("2");
                  tLJSPaySchema.setAppntNo(tLCPolSchema.getAppntNo());

                  //计算交费日期 =个人保单表交至日期+2个月
                  String strPayDate = "";
                  paytoDate = fDate.getDate(tLCPolSchema.getPaytoDate());

                  if (paytoDate != null) //下面的函数第一个参数不能为空
                  {
                      payDate = PubFun.calDate(paytoDate, 2, "M", null);
                      strPayDate = fDate.getString(payDate);
                  }

                  tLJSPaySchema.setPayDate(strPayDate);
      tLJSPaySchema.setStartPayDate(tLCPolSchema.getPaytoDate()); //交费最早应缴日期保存上次交至日期
                  tLJSPaySchema.setBankOnTheWayFlag("0");
                  tLJSPaySchema.setBankSuccFlag("0");
                  tLJSPaySchema.setSendBankCount(0); //送银行次数
                  tLJSPaySchema.setApproveCode(tLCPolSchema.getApproveCode());
                  tLJSPaySchema.setApproveDate(tLCPolSchema.getApproveDate());
                  tLJSPaySchema.setRiskCode(tLCPolSchema.getRiskCode());
                  tLJSPaySchema.setBankAccNo(bankAccNo);
                  tLJSPaySchema.setBankCode(bankCode);
                  tLJSPaySchema.setAccName(accName);
                  tLJSPaySchema.setSerialNo(serNo); //流水号
                  tLJSPaySchema.setOperator(Operator);
                  tLJSPaySchema.setManageCom(tLCPolSchema.getManageCom());
                  tLJSPaySchema.setAgentCom(tLCPolSchema.getAgentCom());
                  tLJSPaySchema.setAgentCode(tLCPolSchema.getAgentCode());
                  tLJSPaySchema.setAgentType(tLCPolSchema.getAgentType());
                  tLJSPaySchema.setAgentGroup(tLCPolSchema.getAgentGroup());

                  VData prtData = getPrintData(tLCPolSchema, tLJSPaySchema);

                  //    LOPRTManagerSchema tLOPRTManagerSchema=getPrintData(tLCPolSchema,tLJSPaySchema);
                  if (prtData == null)
                  {
                      CError tError = new CError();
                      tError.moduleName = "AssignBonus";
                      tError.functionName = "getByCash";
                      tError.errorMessage = "打印数据准备失败!";
                      this.mErrors.addOneError(tError);

                      return false;
                  }

                  LOPRTManagerSchema tLOPRTManagerSchema = (LOPRTManagerSchema) prtData.getObjectByObjectName("LOPRTManagerSchema",
                          0);
                  LOPRTManagerSubSchema tLOPRTManagerSubSchema = (LOPRTManagerSubSchema) prtData.getObjectByObjectName("LOPRTManagerSubSchema",
                          0);

                  tVData.clear();
                  tVData.add(tLJSPaySchema);
                  tVData.add(mLJSPayPersonSet);
                  tVData.add(tLOPRTManagerSchema);
                  tVData.add(tLOPRTManagerSubSchema);

                  DuePayFeeUI tDuePayFeeUI = new DuePayFeeUI();
                  tDuePayFeeUI.submitData(tVData, "INSERT");

                  if (tDuePayFeeUI.mErrors.needDealError())
                  {
                      this.mErrors.addOneError("保单号：" + PolNo + ",错误:" +
                          tDuePayFeeUI.mErrors.getFirstError());
                      errorCount = errorCount + 1; //失败纪录数

                      continue;
                  }

                  operCount = operCount + 1; //成功纪录数
              }

              //批处理结束
          }
          catch (Exception ex)
          {
              CError.buildErr(this, "错误:" + ex);

              return false;
          }

          return true;
      }*/

     /**
      * 从输入数据中得到所有对象
      * @param mInputData:
      *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
      */
     private boolean getInputData(VData mInputData) {
         tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

         //   HashMap map = (HashMap) mInputData.getObjectByObjectName("HashMap", 0);
         mTransferData = (TransferData) mInputData.getObjectByObjectName(
                 "TransferData",
                 0);
         System.out.println("mTransferData:" + mTransferData);

         /*Yangh 于2005-07-18将此段注销
          if (map != null)
          {
              mDuebySpecFlag = true;
              mSpecFlag = (String) map.get("spec");
              System.out.println("mSpecFlag:" + mSpecFlag);
          }*/

         if (tGI == null) {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "IndiDueFeeMultiBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "没有得到足够的数据，请您确认!";
             this.mErrors.addOneError(tError);

             return false;
         }

         return true;
     }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        mInputData = new VData();

        return true;
    }

    private VData getResult() {
        mInputData.clear();

        return mInputData;
    }
    /**
        * 加入到催收核销日志表数据
        * @param
        * @return boolean
        */
       private boolean dealUrgeLog(String pmDealState, String pmOpreat) {

           LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                   LCUrgeVerifyLogSchema();
           //加到催收核销日志表
           tLCUrgeVerifyLogSchema.setSerialNo(mserNo);
           tLCUrgeVerifyLogSchema.setRiskFlag("2");
           tLCUrgeVerifyLogSchema.setStartDate(StartDate);
           tLCUrgeVerifyLogSchema.setEndDate(EndDate);

           tLCUrgeVerifyLogSchema.setOperateType("1"); //1：续期催收操作,2：续期核销操作
           tLCUrgeVerifyLogSchema.setOperateFlag("2"); //1：个案操作,2：批次操作
           tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
           tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成
           if (pmOpreat.equals("INSERT")) {
               tLCUrgeVerifyLogSchema.setMakeDate(CurrentDate);
               tLCUrgeVerifyLogSchema.setMakeTime(CurrentTime);
               tLCUrgeVerifyLogSchema.setModifyDate(CurrentDate);
               tLCUrgeVerifyLogSchema.setModifyTime(CurrentTime);
               tLCUrgeVerifyLogSchema.setErrReason("");

           } else {
               LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
               LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new
                       LCUrgeVerifyLogSet();
               tLCUrgeVerifyLogDB.setSerialNo(mserNo);
               tLCUrgeVerifyLogDB.setOperateType("1");
               tLCUrgeVerifyLogDB.setOperateFlag("2");
               tLCUrgeVerifyLogDB.setRiskFlag("2");
               tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
                if (!(tLCUrgeVerifyLogSet ==null )&& tLCUrgeVerifyLogSet.size() > 0) {
                    tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                    tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                    tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                    tLCUrgeVerifyLogSchema.setDealState(pmDealState);
                }else{
                   return false;
               }


           }

           MMap tMap = new MMap();
           tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
           VData tInputData = new VData();
           tInputData.add(tMap);
           PubSubmit tPubSubmit = new PubSubmit();
           if (tPubSubmit.submitData(tInputData, "") == false) {
               this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
               return false;
           }
           return true;
       }


    /**
     * 准备打印数据
     * @param tLCPolSchema
     * @return
     */
    public VData getPrintData(LCContSchema tLCContchema,
                              LJSPaySchema tLJSPaySchema, String prtSeqNo) {
        VData tVData = new VData();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        LOPRTManagerSubSchema tLOPRTManagerSubSchema = new
                LOPRTManagerSubSchema();

        try {
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(tLCContchema.getContNo());
            tLOPRTManagerSchema.setOtherNoType("00");
            tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_PRnewNotice);
            tLOPRTManagerSchema.setManageCom(tLCContchema.getManageCom());
            tLOPRTManagerSchema.setAgentCode(tLCContchema.getAgentCode());
            tLOPRTManagerSchema.setReqCom(tLCContchema.getManageCom());
            tLOPRTManagerSchema.setReqOperator(tLCContchema.getOperator());
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            tLOPRTManagerSchema.setOldPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setStandbyFlag1(tLCContchema.getInsuredName());
            tLOPRTManagerSchema.setStandbyFlag2(tLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSchema.setPatchFlag("0");
        } catch (Exception ex) {
            return null;
        }

        tVData.add(tLOPRTManagerSchema);
        tVData.add(tLOPRTManagerSubSchema);

        return tVData;
    }

    /**
     * 准备打印数据
     * @param tLCPolSchema
     * @return
     */
    public VData getPrintData(LCPolSchema tLCPolSchema,
                              LJSPaySchema tLJSPaySchema) {
        VData tVData = new VData();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        LOPRTManagerSubSchema tLOPRTManagerSubSchema = new
                LOPRTManagerSubSchema();

        try {
            String tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(tLCPolSchema.getPrtNo());
            tLOPRTManagerSchema.setOtherNoType("00");
            tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_PRnewNotice);
            tLOPRTManagerSchema.setManageCom(tLCPolSchema.getManageCom());
            tLOPRTManagerSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLOPRTManagerSchema.setReqCom(tLCPolSchema.getManageCom());
            tLOPRTManagerSchema.setReqOperator(tLCPolSchema.getOperator());
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            tLOPRTManagerSchema.setOldPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setStandbyFlag1(tLCPolSchema.getInsuredName());
            tLOPRTManagerSchema.setStandbyFlag2(tLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSchema.setStandbyFlag2(tLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSchema.setPatchFlag("0");

            tLOPRTManagerSubSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSubSchema.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSubSchema.setOtherNo(tLCPolSchema.getPolNo());
            tLOPRTManagerSubSchema.setOtherNoType("00");
            tLOPRTManagerSubSchema.setRiskCode(tLCPolSchema.getRiskCode());
            tLOPRTManagerSubSchema.setDuePayMoney(tLJSPaySchema.
                                                  getSumDuePayMoney());
            tLOPRTManagerSubSchema.setAppntName(tLCPolSchema.getAppntName());
            tLOPRTManagerSubSchema.setTypeFlag("1");
        } catch (Exception ex) {
            return null;
        }

        tVData.add(tLOPRTManagerSchema);
        tVData.add(tLOPRTManagerSubSchema);

        return tVData;
    }

    /**
     * 检查续保续期
     * @param tLCPolSchema
     * @return
     */
    private boolean checkRnew(LCPolSchema tLCPolSchema) {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("relationflag");

        if (tLDSysVarDB.getInfo() == false) {
            return false;
        }

        if (tLDSysVarDB.getSysVarValue().equals("0")) { //0-续期续保不关联，1，2为关联
            return false;
        }

        String strSQL = "select count(*) from LCRnewStateLog where PrtNo='" +
                        tLCPolSchema.getPrtNo() + "' and State not in('5','6')";
        ExeSQL tExeSQL = new ExeSQL();

        if (tExeSQL.getOneValue(strSQL).equals("0")) {
            return false;
        }

        strSQL = "update LCRnewStateLog set MainRiskStatus='1' where prtno='" +
                 tLCPolSchema.getPrtNo() + "'";
        tExeSQL = new ExeSQL();
        tExeSQL.execUpdateSQL(strSQL);

        return true;
    }
}
