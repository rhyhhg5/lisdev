package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CardQueryReportUI {
    /**错误的容器*/
    public CErrors mErrors = null;
    private VData mResult = null;

    public CardQueryReportUI() {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
       CardQueryReportBL bl = new CardQueryReportBL();

        if (!bl.submitData(cInputData, cOperate)) {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        mResult = bl.getResult();

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public static void main(String[] args) {
        CardQueryReportUI p = new CardQueryReportUI();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";
        tGI.ManageCom = "86";

        TransferData tTransferData = new TransferData();
        String tStartDate = "2005-01-01"; //起始日
        String tEndDate = "2005-12-30"; //截止日
        String tOrderSql = " order by b.GrpName"; //排序SQL
        tTransferData.setNameAndValue("StartDate", tStartDate);
        tTransferData.setNameAndValue("EndDate", tEndDate);
        tTransferData.setNameAndValue("OrderSql", tOrderSql);

        VData tVData = new VData();
        tVData.addElement(tGI);
        tVData.addElement(tTransferData);
        p.submitData(tVData, "PRINT");

    }

}
