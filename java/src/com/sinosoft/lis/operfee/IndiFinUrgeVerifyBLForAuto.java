package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class IndiFinUrgeVerifyBLForAuto  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  private VData mResult=new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private String serNo="";//流水号
  private String tLimit="";
  private String payNo="";//交费收据号

//应收个人交费表
  private LJSPayPersonSet    mLJSPayPersonSet      = new LJSPayPersonSet();
  private LJSPayPersonSet    mLJSPayPersonNewSet   = new LJSPayPersonSet();
//应收总表
  private LJSPayBL           mLJSPayBL             = new LJSPayBL();
//实收个人交费表
  private LJAPayPersonSet    mLJAPayPersonSet      = new LJAPayPersonSet();
//实收总表
  private LJAPayBL           mLJAPayBL             = new LJAPayBL();
//个人保单表
  private LCPolBL            mLCPolBL              = new LCPolBL();
  private LCPolSet           mLCPolSet             = new LCPolSet();
  private LCContSchema       mLCContSchema         =new LCContSchema();
//保费项表
  private LCPremSet          mLCPremSet            = new LCPremSet();
  private LCPremSet          mLCPremNewSet         = new LCPremSet();
//保险责任表LCDuty
  private LCDutySet          mLCDutySet            = new LCDutySet();
  private LCDutySet          mLCDutyNewSet         = new LCDutySet();
//保险帐户表
  private LCInsureAccSet     mLCInsureAccSet       = new LCInsureAccSet();
//保险帐户表记价履历表
  private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
  private MMap map = new MMap();//Yangh于2005-07-18添加该变量，通过pubsubmit往DB提交数据
  //业务处理相关变量
  public IndiFinUrgeVerifyBLForAuto() {
  }
  public static void main(String[] args) {

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("After getinputdata");

//如果附加险有续保，置标记退出
/* 杨红于2005-07-16将此段注销，等做续保的时候再添加相关逻辑
     if(checkRnew()==true)
{
    System.out.println("有附加险续保，在续保核销时一并处理");
    return true;
}*/

    //进行业务处理
    if (!dealData())
      return false;
System.out.println("After dealData！");
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
System.out.println("After prepareOutputData");

    System.out.println("Start IndiFinUrgeVerify BL Submit...");
    PubSubmit tPubSubmit = new PubSubmit();
   if (tPubSubmit.submitData(mResult, mOperate)) {
     System.out.println("Commit over,Operater successful!");
     return true;
   }
   else
   {
     System.out.println("Operater Failure!");
    // CError.buildErr(this, tPubSubmit.mErrors);
   }

/**杨红于2005-07-18将此段注释掉
    IndiFinUrgeVerifyBLForAutoS tIndiFinUrgeVerifyBLForAutoS=new IndiFinUrgeVerifyBLForAutoS();
    tIndiFinUrgeVerifyBLForAutoS.submitData(mInputData,cOperate);

    System.out.println("End LJIndiFinUrgeVerify BL Submit...");


    if (tIndiFinUrgeVerifyBLForAutoS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tIndiFinUrgeVerifyBLForAutoS.mErrors ) ;
    }
*/
    mInputData=null;
    return true;
  }
  /**
   * 杨红于2005-07-18修改
   * @return boolean
   */
  private boolean dealData()
  {
    //这里的主要逻辑是没有暂收表，直接根据lccont的余额直接核销

    //step one-查询数据
        String sqlStr = "";
        String ContNo = mLJSPayBL.getOtherNo();
        //mLJSPayPersonSet
        LJSPayPersonDB tmpLJSPayPersonDB=new LJSPayPersonDB();
        tmpLJSPayPersonDB.setContNo(ContNo);
        tmpLJSPayPersonDB.setGetNoticeNo(mLJSPayBL.getGetNoticeNo());
        mLJSPayPersonSet= tmpLJSPayPersonDB.query();
        if(mLJSPayPersonSet.size()==0)
        {
          CError.buildErr(this, "系统中没有相关应收个人信息！");
          return false;
        }
        double ContactuMoney = 0;
        DealAccount tDealAccount = new DealAccount();
      //  boolean actuVerifyFlag = false;
//0-判断应收总表中的应收是否=0
      /**  if (mLJSPayBL.getSumDuePayMoney() == 0) {
          actuVerifyFlag = true;
          //    mLJTempFeeBL = new LJTempFeeBL();
          // "应收总表应收款为0!请执行自动核销";
        }
        else {
          if (mLJTempFeeBL.getEnterAccDate() == null) {
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "财务缴费还没有到帐!（暂交费收据号：" +
                mLJTempFeeBL.getTempFeeNo().trim() + "）";
            this.mErrors.addOneError(tError);
            return false;
          }
//          if(PubFun.calInterval(PubFun.getCurrentDate(),mLJSPayBL.getPayDate(),"D")<0)
//          {
//              CError tError = new CError();
//              tError.moduleName = "IndiFinUrgeVerifyBL";
//              tError.functionName = "ReturnData";
//              tError.errorMessage = "保单已过失效日期:应收纪录超过失效期核销 ";
//              this.mErrors .addOneError(tError) ;
//              return false;
//          }
        }*/

//保险帐户表
        LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
//保险帐户表记价履历表
        LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
//0-查询保单表
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = new LCContSet();
        sqlStr = "select * from LCCont where ContNo='" + ContNo + "'";
        System.out.println("查询保单表:" + sqlStr);
        tLCContSet = tLCContDB.executeQuery(sqlStr);
        if (tLCContDB.mErrors.needDealError() == true) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCContDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "IndiFinUrgeVerifyBL";
          tError.functionName = "dealData";
          tError.errorMessage = "个人保单表查询失败!";
          this.mErrors.addOneError(tError);
          tLCContSet.clear();
          return false;
        }
        else {
          mLCContSchema.setSchema(tLCContSet.get(1));
          //  leaveMoney = mLCContSchema.getDif();杨红于2005-07-17说明：合同的余额需要从应收个人表的yet里面获取

        }
        //产生流水号
        tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        //产生交费收据号
        tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        payNo = PubFun1.CreateMaxNo("PayNo", tLimit);

//1-查询保单险种表
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = new LCPolSet();
        sqlStr = "select * from LCPol where ContNo='" + ContNo + "' and polno"
            +" in (select polno from ljspayperson)";
        System.out.println("查询保单险种表:" + sqlStr);
        tLCPolSet = tLCPolDB.executeQuery(sqlStr);
        if (tLCPolDB.mErrors.needDealError() == true) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "IndiFinUrgeVerifyBL";
          tError.functionName = "dealData";
          tError.errorMessage = "个人保单险种表查询失败!";
          this.mErrors.addOneError(tError);
          tLCPolSet.clear();
          return false;
        }
        if (tLCPolSet.size() == 0) {
          CError.buildErr(this, "保单号为" + ContNo + "下的险种信息缺失，不能核销！");
          return false;
        }
        for (int n = 1; n <= tLCPolSet.size(); n++) {
          LCPolBL tempLCPolBL = new LCPolBL();
          tempLCPolBL.setSchema(tLCPolSet.get(n));
          String polPayToDate = "";
          double polTotalPrem = 0.0; //保存单个险种下的所交保费总和
          int polCount = 0; //判断该险种下是否有对应的应收数据信息，有则修改险种信息，否则不变，杨红于2005-07-17说明
          String polNo = tLCPolSet.get(n).getPolNo(); //保单险种号
          LCDutyDB tLCDutyDB = new LCDutyDB();
          LCDutySet tLCDutySet = new LCDutySet();
          tLCDutyDB.setPolNo(polNo);
          tLCDutySet = tLCDutyDB.query();
          if (tLCDutyDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "险种责任表查询失败!";
            this.mErrors.addOneError(tError);
            // tLCPolSet.clear();
            return false;
          }
          if (tLCDutySet.size() == 0) {
            CError.buildErr(this,
                            "保单号:" + ContNo + "，险种号：" + polNo + "下的险种责任信息缺失，不能核销！");
            return false;
          }
          for (int m = 1; m <= tLCDutySet.size(); m++) {
            LCDutyBL tLCDutyBL = new LCDutyBL();
            tLCDutyBL.setSchema(tLCDutySet.get(m));
            String dutyCode = tLCDutySet.get(m).getDutyCode();
            LCPremDB tLCPremDB = new LCPremDB();
            LCPremSet tLCPremSet = new LCPremSet();
            tLCPremDB.setPolNo(polNo);
            tLCPremDB.setDutyCode(dutyCode);
            tLCPremSet = tLCPremDB.query();
            if (tLCPremDB.mErrors.needDealError() == true) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCPremDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "IndiFinUrgeVerifyBL";
              tError.functionName = "dealData";
              tError.errorMessage = "保费项信息查询失败!";
              this.mErrors.addOneError(tError);
              // tLCPolSet.clear();
              return false;
            }
            if (tLCPremSet.size() == 0) {
              CError.buildErr(this,
                              "保单号:" + ContNo + "，险种号：" + polNo + ",责任编码:" + dutyCode +
                              "下的保费项信息缺失，不能核销！");
              return false;
            }
            int dutyCount = 0;
            double totalPrem = 0.0;
            String PaytoDate = "";
            for (int t = 1; t <= tLCPremSet.size(); t++) {
              LCPremBL tLCPremBL = new LCPremBL();
              tLCPremBL.setSchema(tLCPremSet.get(t));
              String payPlanCode = tLCPremSet.get(t).getPayPlanCode();
              LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
              LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
              tLJSPayPersonDB.setPolNo(polNo);
              tLJSPayPersonDB.setDutyCode(dutyCode);
              tLJSPayPersonDB.setPayPlanCode(payPlanCode);
              tLJSPayPersonDB.setPayType("ZC");
              tLJSPayPersonSet = tLJSPayPersonDB.query();
              if (tLJSPayPersonDB.mErrors.needDealError() == true) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "IndiFinUrgeVerifyBL";
                tError.functionName = "dealData";
                tError.errorMessage = "应收个人表查询失败!";
                this.mErrors.addOneError(tError);
                // mLJSPayPersonSet.clear();
                return false;
              }
              if (tLJSPayPersonSet.size() == 0) {
                continue;
              }
              else { //这里tLJSPayPersonSet.size()肯定为1
                tLCPremBL.setPayTimes(tLCPremBL.getPayTimes() + 1); //已交费次数
                tLCPremBL.setSumPrem(tLCPremBL.getSumPrem() + tLJSPayPersonSet.get(1).getSumDuePayMoney()); //累计保费
                tLCPremBL.setPaytoDate(tLJSPayPersonSet.get(1).getCurPayToDate()); //交至日期
                tLCPremBL.setModifyDate(CurrentDate); //最后一次修改日期
                tLCPremBL.setModifyTime(CurrentTime); //最后一次修改时间
                mLCPremNewSet.add(tLCPremBL);

                //杨红说明：取保费项较早的paytodate作为责任表的paytodate
                if(PaytoDate.equals(""))
                {
                  PaytoDate = tLCPremBL.getPaytoDate();
                }
                else
                {
                  FDate tFDate = new FDate();
                  Date tDate = tFDate.getDate(PaytoDate);
                  Date tDate1 = tFDate.getDate(tLCPremBL.getPaytoDate());
                  if (tDate1.before(tDate))
                  {
                   PaytoDate = tLCPremBL.getPaytoDate();
                  }
                }
                polPayToDate = tLCPremBL.getPaytoDate(); //险种的交至日期，随便取一个应收表的信息即可，杨红 说明

                totalPrem += tLCPremBL.getPrem();
                polTotalPrem += tLCPremBL.getPrem();
                dutyCount++;
                polCount++;
                //将应收个人表的数据映射到实收个人表中去
                LJAPayPersonBL tLJAPayPersonBL = new LJAPayPersonBL();
                tLJAPayPersonBL.setPolNo(tLJSPayPersonSet.get(1).getPolNo()); //保单号码
                tLJAPayPersonBL.setPayCount(tLJSPayPersonSet.get(1).getPayCount()); //第几次交费
                tLJAPayPersonBL.setGrpContNo(tLJSPayPersonSet.get(1).getGrpContNo()); //集体保单号码
                tLJAPayPersonBL.setGrpPolNo(tLJSPayPersonSet.get(1).getGrpPolNo()); //集体保单号码
                tLJAPayPersonBL.setContNo(tLJSPayPersonSet.get(1).getContNo()); //总单/合同号码
                tLJAPayPersonBL.setAppntNo(tLJSPayPersonSet.get(1).getAppntNo()); //投保人客户号码
                tLJAPayPersonBL.setPayNo(payNo); //交费收据号码
                tLJAPayPersonBL.setPayAimClass(tLJSPayPersonSet.get(1).
                                               getPayAimClass()); //交费目的分类
                tLJAPayPersonBL.setDutyCode(tLJSPayPersonSet.get(1).getDutyCode()); //责任编码
                tLJAPayPersonBL.setPayPlanCode(tLJSPayPersonSet.get(1).
                                               getPayPlanCode()); //交费计划编码
                tLJAPayPersonBL.setSumDuePayMoney(tLJSPayPersonSet.get(1).
                                                  getSumDuePayMoney()); //总应交金额
                tLJAPayPersonBL.setSumActuPayMoney(tLJSPayPersonSet.get(1).
                                                   getSumActuPayMoney()); //总实交金额
                tLJAPayPersonBL.setPayIntv(tLJSPayPersonSet.get(1).getPayIntv()); //交费间隔
                tLJAPayPersonBL.setPayDate(tLJSPayPersonSet.get(1).getPayDate()); //交费日期
                tLJAPayPersonBL.setPayType(tLJSPayPersonSet.get(1).getPayType()); //交费类型


                tLJAPayPersonBL.setEnterAccDate(CurrentDate); //到帐日期
                tLJAPayPersonBL.setConfDate(CurrentDate); //确认日期

                tLJAPayPersonBL.setLastPayToDate(tLJSPayPersonSet.get(1).
                                                 getLastPayToDate()); //原交至日期
                tLJAPayPersonBL.setCurPayToDate(tLJSPayPersonSet.get(1).
                                                getCurPayToDate()); //现交至日期
                tLJAPayPersonBL.setInInsuAccState(tLJSPayPersonSet.get(1).
                                                  getInInsuAccState()); //转入保险帐户状态
                tLJAPayPersonBL.setApproveCode(tLJSPayPersonSet.get(1).
                                               getApproveCode()); //复核人编码
                tLJAPayPersonBL.setApproveDate(tLJSPayPersonSet.get(1).
                                               getApproveDate()); //复核日期
                tLJAPayPersonBL.setAgentCode(tLJSPayPersonSet.get(1).getAgentCode());
                tLJAPayPersonBL.setAgentGroup(tLJSPayPersonSet.get(1).getAgentGroup());
                tLJAPayPersonBL.setSerialNo(serNo); //流水号
                tLJAPayPersonBL.setOperator(tLJSPayPersonSet.get(1).getOperator()); //操作员
                tLJAPayPersonBL.setMakeDate(CurrentDate); //入机日期
                tLJAPayPersonBL.setMakeTime(CurrentTime); //入机时间
                tLJAPayPersonBL.setGetNoticeNo(tLJSPayPersonSet.get(1).
                                               getGetNoticeNo()); //通知书号码
                tLJAPayPersonBL.setModifyDate(CurrentDate); //最后一次修改日期
                tLJAPayPersonBL.setModifyTime(CurrentTime); //最后一次修改时间
                tLJAPayPersonBL.setManageCom(tLJSPayPersonSet.get(1).getManageCom()); //管理机构
                tLJAPayPersonBL.setAgentCom(tLJSPayPersonSet.get(1).getAgentCom()); //代理机构
                tLJAPayPersonBL.setAgentType(tLJSPayPersonSet.get(1).getAgentType()); //代理机构内部分类
                tLJAPayPersonBL.setRiskCode(tLJSPayPersonSet.get(1).getRiskCode()); //险种编码
                mLJAPayPersonSet.add(tLJAPayPersonBL);
             //   mLJSPayPersonSet.add(tLJSPayPersonSet.get(1));
                //应收个人 转 实收个人 结束
              }
            }
            if (dutyCount == 0) {
              continue;
            }
            else {
              tLCDutyBL.setPrem(totalPrem); //实际保费
              tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem() + totalPrem); //累计保费
              tLCDutyBL.setPaytoDate(PaytoDate); //交至日期
              tLCDutyBL.setModifyDate(CurrentDate); //最后一次修改日期
              tLCDutyBL.setModifyTime(CurrentTime); //最后一次修改时间
              mLCDutyNewSet.add(tLCDutyBL);
            }

          }
          if (polCount == 0) {
            continue;
          }
          else {
            //修改险种信息
            tempLCPolBL.setPaytoDate(polPayToDate); //交至日期
            tempLCPolBL.setSumPrem(tempLCPolBL.getSumPrem() + polTotalPrem); //总累计保费
            //求总余额:如果应收总表应收款=0，表明有余额，且这次的余额值存放在责任编码为
            //"yet"的应收个人交费纪录中（见个人催收流程图）,取出放在个人保单表的余额字段中
            //否则，个人保单表余额纪录置为0
            tempLCPolBL.setModifyDate(CurrentDate); //最后一次修改日期
            tempLCPolBL.setModifyTime(CurrentTime); //最后一次修改时间
            mLCPolSet.add(tempLCPolBL);

          }
          ContactuMoney += polTotalPrem;
        }
        //杨红于2005-07-17说明:  下面处理合同的余额问题，续期中很重要的一个环节
        sqlStr = "select * from ljspayperson where polno in (select polno from lcpol where contno='"
            + ContNo + "') and paytype='YET'";
        LJSPayPersonDB tyetLJSPayPersonDB = new LJSPayPersonDB();
        LJSPayPersonSet tyetLJSPayPersonSet = tyetLJSPayPersonDB.executeQuery(
            sqlStr);
      //  double tempFeeAndDueSumDif = mLJTempFeeBL.getPayMoney() -
      //      mLJSPayBL.getSumDuePayMoney();
        //上面的变量保存暂交费与应交费之差，作为放到合同dif字段的一部分，杨红于2005-07-17说明
        if (tyetLJSPayPersonSet.size() == 0) {
          mLCContSchema.setDif(0.0);
        }
        else {
          mLCContSchema.setDif(tyetLJSPayPersonSet.get(1).getSumActuPayMoney());
        }
        mLCContSchema.setPrem(ContactuMoney);
        mLCContSchema.setSumPrem(mLCContSchema.getSumPrem() + ContactuMoney);
        mLCContSchema.setModifyDate(CurrentDate); //最后一次修改日期
        mLCContSchema.setModifyTime(CurrentTime); //最后一次修改时间
        //在这里处理合同级的PayToDate
        //获取交费分类表的信息，准备核销动作。
      /**  LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
            sqlStr = "select * from LJTempFeeClass where TempFeeNo='" +
                mLJTempFeeBL.getTempFeeNo() + "'";
            System.out.println("查询暂交费分类表:" + sqlStr);
            mLJTempFeeClassSet = tLJTempFeeClassDB.executeQuery(sqlStr);
            if (tLJTempFeeClassDB.mErrors.needDealError() == true) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "IndiFinUrgeVerifyBL";
              tError.functionName = "dealData";
              tError.errorMessage = "暂交费分类表表查询失败!";
              this.mErrors.addOneError(tError);
              mLJTempFeeClassSet.clear();
              return false;
            }
            //2-暂交费表核销标志置为1
            mLJTempFeeBL.setConfFlag("1"); //核销标志置为1
            mLJTempFeeBL.setConfDate(CurrentDate);
            //将暂收分类表核销标志置1
            for (int p = 1; p <= mLJTempFeeClassSet.size(); p++) {
                LJTempFeeClassBL tLJTempFeeClassBL = new LJTempFeeClassBL();
                tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(p).getSchema());
                tLJTempFeeClassBL.setConfFlag("1"); //核销标志置为1
                tLJTempFeeClassBL.setConfDate(CurrentDate);
                mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);
              }*/
              mLJAPayBL.setPayNo(payNo); //交费收据号码
             mLJAPayBL.setIncomeNo(mLJSPayBL.getOtherNo()); //应收/实收编号
             mLJAPayBL.setIncomeType(mLJSPayBL.getOtherNoType()); //应收/实收编号类型
             mLJAPayBL.setAppntNo(mLJSPayBL.getAppntNo()); //  投保人客户号码
              mLJAPayBL.setSumActuPayMoney(mLJSPayBL.getSumDuePayMoney()); // 总实交金额
          //   if (actuVerifyFlag == false) { //如果不需要自动核销

               mLJAPayBL.setEnterAccDate(CurrentDate); // 到帐日期
               mLJAPayBL.setConfDate(CurrentDate); //确认日期


             mLJAPayBL.setGetNoticeNo(mLJSPayBL.getGetNoticeNo()); //交费通知书号码
             mLJAPayBL.setPayDate(mLJSPayBL.getPayDate()); //交费日期
             mLJAPayBL.setApproveCode(mLJSPayBL.getApproveCode()); //复核人编码
             mLJAPayBL.setApproveDate(mLJSPayBL.getApproveDate()); //  复核日期
             mLJAPayBL.setSerialNo(serNo); //流水号
             mLJAPayBL.setOperator(mLJSPayBL.getOperator()); // 操作员
             mLJAPayBL.setMakeDate(CurrentDate); //入机时间
             mLJAPayBL.setMakeTime(CurrentTime); //入机时间
             mLJAPayBL.setModifyDate(CurrentDate); //最后一次修改日期
             mLJAPayBL.setModifyTime(CurrentTime); //最后一次修改时间
             mLJAPayBL.setBankCode(mLJSPayBL.getBankCode()); //银行编码
             mLJAPayBL.setBankAccNo(mLJSPayBL.getBankAccNo()); //银行帐号
             mLJAPayBL.setRiskCode(mLJSPayBL.getRiskCode()); // 险种编码
             mLJAPayBL.setManageCom(mLJSPayBL.getManageCom());
             mLJAPayBL.setAgentCode(mLJSPayBL.getAgentCode());
             mLJAPayBL.setAgentGroup(mLJSPayBL.getAgentGroup());


        //下面给合同表修改相应信息

        return true;


  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
 /**
  private boolean dealData()
  {
   boolean tReturn =false;

//step one-查询数据
   String sqlStr="";
   String PolNo= mLJSPayBL.getOtherNo();
   String GetNoticeNo= mLJSPayBL.getGetNoticeNo();
   double actuMoney=0;
   double leaveMoney=0;
   double newLeavingMoney=0;
   DealAccount tDealAccount = new DealAccount();
   VData tempVData = new VData();
   boolean actuVerifyFlag=false;

//保险帐户表
   LCInsureAccSet     tLCInsureAccSet       = new LCInsureAccSet();
//保险帐户表记价履历表
   LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();

//1-查询保单表
      LCPolDB tLCPolDB   = new LCPolDB();
      LCPolSet tLCPolSet = new LCPolSet();
      sqlStr = "select * from LCPol where PolNo='"+PolNo+"'";
System.out.println("查询保单表:"+sqlStr);
      tLCPolSet = tLCPolDB.executeQuery(sqlStr);
      if(tLCPolDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "IndiFinUrgeVerifyBLForAuto";
      tError.functionName = "dealData";
      tError.errorMessage = "个人保单表查询失败!";
      this.mErrors.addOneError(tError);
      tLCPolSet.clear();
      return false;
      }
      if(tLCPolSet.size()==0)
      {
          CError tError = new CError();
          tError.moduleName = "IndiFinUrgeVerifyBLForAuto";
          tError.functionName = "dealData";
          tError.errorMessage = "个人保单表查询失败!";
          this.mErrors.addOneError(tError);
          return false;
      }

      mLCPolBL.setSchema(tLCPolSet.get(1));
      leaveMoney=mLCPolBL.getLeavingMoney();
      tReturn=true;

      //产生流水号
     tLimit=PubFun.getNoLimit(mLCPolBL.getManageCom());
     serNo=PubFun1.CreateMaxNo("SERIALNO",tLimit);
     //产生交费收据号
     tLimit=PubFun.getNoLimit(mLCPolBL.getManageCom());
     payNo=PubFun1.CreateMaxNo("PayNo",tLimit);
//2-查询应收个人表
      LJSPayPersonDB tLJSPayPersonDB   = new LJSPayPersonDB();
      sqlStr = "select * from LJSPayPerson where PolNo='"+PolNo+"'";
System.out.println("查询应收个人表:"+sqlStr);
      mLJSPayPersonSet = tLJSPayPersonDB.executeQuery(sqlStr);
      if(tLJSPayPersonDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "IndiFinUrgeVerifyBLForAuto";
      tError.functionName = "dealData";
      tError.errorMessage = "应收个人表查询失败!";
      this.mErrors.addOneError(tError);
      mLJSPayPersonSet.clear();
      return false;
      }
      if(mLJSPayPersonSet.size() == 0)
      {
      CError tError = new CError();
      tError.moduleName = "IndiFinUrgeVerifyBLForAuto";
      tError.functionName = "dealData";
      tError.errorMessage = "应收个人表查询失败!";
      this.mErrors.addOneError(tError);
      return false;
      }
      tReturn=true;

//3-查询保费项表//根据应收个人交费表查询该表项
System.out.println("查询保费项表:");
      LJSPayPersonBL tLJSPayPersonBL;
      LCPremSet tLCPremSet;
      LCPremBL tLCPremBL;
      LCPremDB tLCPremDB   = new LCPremDB();
      for(int num=1;num<=mLJSPayPersonSet.size();num++)
      {
      	tLJSPayPersonBL=new LJSPayPersonBL();
      	tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(num));
      	//得到个人实际交款总和:交费方式为ZC的纪录
        if(tLJSPayPersonBL.getPayType().equals("ZC"))
      	   actuMoney=actuMoney+tLJSPayPersonBL.getSumDuePayMoney();
        if(tLJSPayPersonBL.getPayType().equals("YET"))//存放新余额的应收纪录
           newLeavingMoney=tLJSPayPersonBL.getSumActuPayMoney();
System.out.println("getPayType():"+tLJSPayPersonBL.getPayType());
System.out.println("actuMoney:"+actuMoney);
System.out.println("newLeavingMoney:"+newLeavingMoney);

      	sqlStr="select * from LCPrem where PolNo='"+tLJSPayPersonBL.getPolNo()+"'";
      	sqlStr=sqlStr+" and DutyCode='"+tLJSPayPersonBL.getDutyCode()+"'";
      	sqlStr=sqlStr+" and PayPlanCode='"+tLJSPayPersonBL.getPayPlanCode()+"'";
      	tLCPremSet = new LCPremSet();
        tLCPremDB   = new LCPremDB();
      	tLCPremSet=tLCPremDB.executeQuery(sqlStr);
System.out.println("sql:"+sqlStr);
        if(tLCPremDB.mErrors.needDealError() == true)
         {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCPremDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "IndiFinUrgeVerifyBLForAuto";
          tError.functionName = "dealData";
          tError.errorMessage = "保费项表查询失败!";
          this.mErrors.addOneError(tError);
          tLCPremSet.clear();
          mLCPremSet.clear();
          return false;
         }
      	tLCPremBL=new LCPremBL();
        tLCPremBL.setSchema(tLCPremSet.get(1).getSchema());
      	mLCPremSet.add(tLCPremBL);

        //处理帐户
        tempVData = tDealAccount.addPrem(tLCPremSet.get(1).getSchema(),"2",GetNoticeNo,"2","BF",null);
        if(tempVData!=null)
        {
         tempVData=tDealAccount.updateLCInsureAccTraceDate(mLJSPayBL.getPayDate(),tempVData);
         if(tempVData==null)
         {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "IndiFinUrgeVerifyBLForAuto";
          tError.functionName = "dealData";
          tError.errorMessage = "修改保险帐户表记价履历表纪录的交费日期时出错!";
          this.mErrors.addOneError(tError);
          return false;
         }
        tLCInsureAccSet=(LCInsureAccSet)tempVData.getObjectByObjectName("LCInsureAccSet",0);
        tLCInsureAccTraceSet=(LCInsureAccTraceSet)tempVData.getObjectByObjectName("LCInsureAccTraceSet",0);
        mLCInsureAccSet.add(tLCInsureAccSet);
        mLCInsureAccTraceSet.add(tLCInsureAccTraceSet);
       }
      }
        tReturn=true;

//5-查询保险责任表
      LCDutyDB tLCDutyDB   = new LCDutyDB();
      sqlStr="select * from LCDuty where PolNo='"+PolNo+"'";
System.out.println("查询保险责任表:"+sqlStr);
      mLCDutySet=tLCDutyDB.executeQuery(sqlStr);
      if(tLCDutyDB.mErrors.needDealError() == true)
       {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "IndiFinUrgeVerifyBLForAuto";
        tError.functionName = "dealData";
        tError.errorMessage = "查询保险责任表!";
        this.mErrors.addOneError(tError);
        mLCDutySet.clear();
        return false;
       }
      tReturn=true;

//step two-处理数据
    //实收款=actuMoney
    //1 ZC=100 YEL=0 YET=0  actuMoney=100
    //2 ZC=100 YEL=-50 YET=0 actuMoney=50
    //3 ZC=100 YEL=-200 YET=100 actuMoney=0

    int i,iMax;
    //添加纪录
    if(this.mOperate.equals("VERIFY"))
    {
//1-应收总表和暂交费表数据填充实收总表
     mLJAPayBL.setPayNo(payNo);                              //交费收据号码
     mLJAPayBL.setIncomeNo(mLJSPayBL.getOtherNo());          //应收/实收编号
     mLJAPayBL.setIncomeType(mLJSPayBL.getOtherNoType());    //应收/实收编号类型
     mLJAPayBL.setAppntNo(mLJSPayBL.getAppntNo());           //  投保人客户号
     mLJAPayBL.setSumActuPayMoney(0); // 总实交金额
     mLJAPayBL.setEnterAccDate(CurrentDate);   // 到帐日期
     mLJAPayBL.setConfDate(CurrentDate);      //确认日期
     mLJAPayBL.setGetNoticeNo(mLJSPayBL.getGetNoticeNo());    //交费通知书号码
     mLJAPayBL.setPayDate(mLJSPayBL.getPayDate());           //交费日期
     mLJAPayBL.setApproveCode(mLJSPayBL.getApproveCode());   //复核人编码
     mLJAPayBL.setApproveDate(mLJSPayBL.getApproveDate());   //  复核日期
     mLJAPayBL.setSerialNo(serNo);                           //流水号
     mLJAPayBL.setOperator(mLJSPayBL.getOperator());         // 操作员
     mLJAPayBL.setMakeDate(CurrentDate);                     //入机时间
     mLJAPayBL.setMakeTime(CurrentTime);                     //入机时间
     mLJAPayBL.setModifyDate(CurrentDate);                   //最后一次修改日期
     mLJAPayBL.setModifyTime(CurrentTime);                   //最后一次修改时间

     mLJAPayBL.setBankCode(mLJSPayBL.getBankCode());      //银行编码
     mLJAPayBL.setBankAccNo(mLJSPayBL.getBankAccNo());   //银行帐号
     mLJAPayBL.setRiskCode(mLJSPayBL.getRiskCode());   // 险种编码
     mLJAPayBL.setManageCom(mLJSPayBL.getManageCom());
     mLJAPayBL.setAgentCode(mLJSPayBL.getAgentCode());
     mLJAPayBL.setAgentGroup(mLJSPayBL.getAgentGroup());

//4-应收个人表填充实收个人表

    LJAPayPersonBL tLJAPayPersonBL;
    iMax=mLJSPayPersonSet.size() ;
    for (i=1;i<=iMax;i++)
    {
      tLJSPayPersonBL = new LJSPayPersonBL();
      tLJAPayPersonBL = new LJAPayPersonBL();
      tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(i).getSchema());
      tLJAPayPersonBL.setPolNo(tLJSPayPersonBL.getPolNo());           //保单号码
      tLJAPayPersonBL.setPayCount(tLJSPayPersonBL.getPayCount());     //第几次交费
      tLJAPayPersonBL.setGrpPolNo(tLJSPayPersonBL.getGrpPolNo());     //集体保单号码
      tLJAPayPersonBL.setContNo(tLJSPayPersonBL.getContNo());         //总单/合同号码
      tLJAPayPersonBL.setAppntNo(tLJSPayPersonBL.getAppntNo());       //投保人客户号码
      tLJAPayPersonBL.setPayNo(payNo);          //交费收据号码
      tLJAPayPersonBL.setPayAimClass(tLJSPayPersonBL.getPayAimClass());//交费目的分类
      tLJAPayPersonBL.setDutyCode(tLJSPayPersonBL.getDutyCode());      //责任编码
      tLJAPayPersonBL.setPayPlanCode(tLJSPayPersonBL.getPayPlanCode());//交费计划编码
      tLJAPayPersonBL.setSumDuePayMoney(tLJSPayPersonBL.getSumDuePayMoney());//总应交金额
      tLJAPayPersonBL.setSumActuPayMoney(tLJSPayPersonBL.getSumActuPayMoney());//总实交金额
      tLJAPayPersonBL.setPayIntv(tLJSPayPersonBL.getPayIntv());        //交费间隔
      tLJAPayPersonBL.setPayDate(tLJSPayPersonBL.getPayDate());        //交费日期
      tLJAPayPersonBL.setPayType(tLJSPayPersonBL.getPayType());        //交费类型
      tLJAPayPersonBL.setEnterAccDate(CurrentDate); //到帐日期
      tLJAPayPersonBL.setConfDate(CurrentDate);         //确认日期
      tLJAPayPersonBL.setLastPayToDate(tLJSPayPersonBL.getLastPayToDate());  //原交至日期
      tLJAPayPersonBL.setCurPayToDate(tLJSPayPersonBL.getCurPayToDate());    //现交至日期
      tLJAPayPersonBL.setInInsuAccState(tLJSPayPersonBL.getInInsuAccState());//转入保险帐户状态
      tLJAPayPersonBL.setApproveCode(tLJSPayPersonBL.getApproveCode());      //复核人编码
      tLJAPayPersonBL.setApproveDate(tLJSPayPersonBL.getApproveDate());      //复核日期
      tLJAPayPersonBL.setAgentCode(tLJSPayPersonBL.getAgentCode());
      tLJAPayPersonBL.setAgentGroup(tLJSPayPersonBL.getAgentGroup());
      tLJAPayPersonBL.setSerialNo(serNo);                              //流水号
      tLJAPayPersonBL.setOperator(tLJSPayPersonBL.getOperator());      //操作员
      tLJAPayPersonBL.setMakeDate(CurrentDate);                        //入机日期
      tLJAPayPersonBL.setMakeTime(CurrentTime);                        //入机时间
      tLJAPayPersonBL.setGetNoticeNo(tLJSPayPersonBL.getGetNoticeNo());//通知书号码
      tLJAPayPersonBL.setModifyDate(CurrentDate);                      //最后一次修改日期
      tLJAPayPersonBL.setModifyTime(CurrentTime);                      //最后一次修改时间

      tLJAPayPersonBL.setManageCom(tLJSPayPersonBL.getManageCom());    //管理机构
      tLJAPayPersonBL.setAgentCom(tLJSPayPersonBL.getAgentCom());//代理机构
      tLJAPayPersonBL.setAgentType(tLJSPayPersonBL.getAgentType());      //代理机构内部分类
      tLJAPayPersonBL.setRiskCode(tLJSPayPersonBL.getRiskCode());      //险种编码

      mLJAPayPersonSet.add(tLJAPayPersonBL);
      tReturn=true;
    }
//5-更新保单表字段，取第一个应收个人交费纪录
    tLJSPayPersonBL = new LJSPayPersonBL();
    tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(1).getSchema());
    mLCPolBL.setPaytoDate(tLJSPayPersonBL.getCurPayToDate());                //交至日期
    mLCPolBL.setSumPrem(mLCPolBL.getSumPrem()+actuMoney);//总累计保费
    //求总余额:如果应收总表应收款=0，表明有余额，且这次的余额值存放在责任编码为
    //"yet"的应收个人交费纪录中（见个人催收流程图）,取出放在个人保单表的余额字段中
    //否则，个人保单表余额纪录置为0
    mLCPolBL.setLeavingMoney(newLeavingMoney);//取应收纪录中交费方式为“YET”的金额

    mLCPolBL.setModifyDate(CurrentDate);//最后一次修改日期
    mLCPolBL.setModifyTime(CurrentTime);//最后一次修改时间
    tReturn=true;
//6-更新保费项表字段
    for(int num=1;num<=mLCPremSet.size();num++)
    {
      tLJSPayPersonBL=new LJSPayPersonBL();
      tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(num));
      tLCPremBL=new LCPremBL();
      tLCPremBL.setSchema(mLCPremSet.get(num));
      tLCPremBL.setPayTimes(tLCPremBL.getPayTimes()+1); //已交费次数
      //tLCPremBL.setPrem(tLJSPayPersonBL.getSumActuPayMoney());//实际保费
      tLCPremBL.setSumPrem(tLCPremBL.getSumPrem()+tLCPremBL.getPrem());//累计保费
      tLCPremBL.setPaytoDate(tLJSPayPersonBL.getCurPayToDate());//交至日期
      tLCPremBL.setModifyDate(CurrentDate); //最后一次修改日期
      tLCPremBL.setModifyTime(CurrentTime); //最后一次修改时间
      mLCPremNewSet.add(tLCPremBL);
System.out.println("更新保险责任表:");
//6-2更新保险责任表
      LCDutyBL tLCDutyBL ;
      iMax=mLCDutySet.size();
      for(i=1;i<=iMax;i++);
      {
       tLCDutyBL=new LCDutyBL();
       tLCDutyBL.setSchema(mLCDutySet.get(1));      //repair:???
       if(tLCPremBL.getPolNo().equals(tLCDutyBL.getPolNo())&&tLCPremBL.getDutyCode().equals(tLCDutyBL.getDutyCode()))
        {
         tLCDutyBL.setPrem(tLCPremBL.getPrem());//实际保费
         tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem()+tLCPremBL.getPrem());//累计保费
         tLCDutyBL.setPaytoDate(tLCPremBL.getPaytoDate());//交至日期
         tLCDutyBL.setModifyDate(CurrentDate);//最后一次修改日期
         tLCDutyBL.setModifyTime(CurrentTime);//最后一次修改时间

         mLCDutyNewSet.add(tLCDutyBL);
         break;
        }//end if
      } //end for
    } //end for
    tReturn=true;
//更新完毕
 }
  return tReturn;
}*/

 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData)
  {
   //应收总表
    mLJSPayBL.setSchema((LJSPaySchema)mInputData.getObjectByObjectName("LJSPaySchema",0));
    if(mLJSPayBL==null )
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="IndiFinUrgeVerifyBLForAuto";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到应收总表，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    //这里自动核销的只是应收总额为0才做自动核销的动作
    if(mLJSPayBL.getSumDuePayMoney()>0)
    {
        // @@错误处理
        CError tError =new CError();
        tError.moduleName="IndiFinUrgeVerifyBLForAuto";
        tError.functionName="getInputData";
        tError.errorMessage="应收总表金额不为0，请您确认!";
        this.mErrors .addOneError(tError) ;
        return false;
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    //mInputData=new VData();
    try
    {
     // mLJAPaySchema.setSchema(mLJAPayBL);
    // mLJSPaySchema.setSchema(mLJSPayBL);
   //  mLJTempFeeSchema.setSchema(mLJTempFeeBL);
     map.put(mLJAPayBL.getSchema(), "INSERT");
     map.put(mLJSPayBL.getSchema(), "DELETE");
   //  map.put(mLJTempFeeSchema, "UPDATE");
  //   map.put(mLJTempFeeClassNewSet, "UPDATE");
     map.put(mLJAPayPersonSet, "INSERT");
     map.put(mLJSPayPersonSet, "DELETE");
     map.put(mLCContSchema, "UPDATE");
     map.put(mLCPolSet, "UPDATE");
     map.put(mLCPremNewSet, "UPDATE");
     map.put(mLCDutyNewSet, "UPDATE");
     mResult.add(map);
      /**
    mInputData.add(mLJAPayBL);	          //实收总表（插入）
    mInputData.add(mLJSPayBL);            //应收总表（删除）
    mInputData.add(mLJAPayPersonSet);     //实收个人表（插入）
    mInputData.add(mLJSPayPersonSet);     //应收个人交费表（删除）
    mInputData.add(mLCPolBL);             //个人保单表（更新）
    mInputData.add(mLCPremNewSet);        //保费项表（更新）
    mInputData.add(mLCDutyNewSet);          //保险责任表（更新）
    mInputData.add(mLCInsureAccSet);        //保险帐户表(更新或插入：处理时先删除再插入)
    mInputData.add(mLCInsureAccTraceSet);   //保险帐户表记价履历表（插入）
*/
System.out.println("prepareOutputData:");
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="IndiFinUrgeVerifyBLForAuto";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 公共核销程序
   * @param TempFeeNo 暂交费号
   * @return 包含纪录的集合(纪录如何处理具体见prepareOutputData函数)
   */
  public VData ReturnData(String TempFeeNo)
  {
     if(TempFeeNo==null)
     {
       // @@错误处理
       CError tError =new CError();
       tError.moduleName="IndiFinUrgeVerifyBLForAuto";
       tError.functionName="ReturnData";
       tError.errorMessage="传入暂交费号不能为空";
       this.mErrors .addOneError(tError) ;
       return null;
     }

//1-查询暂交费表，将TempFeeNo输入Schema中传入，查询得到Set集
      VData tVData = new VData();
      LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
      LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
      TempFeeQueryUI tTempFeeQueryUI = new TempFeeQueryUI();
      tLJTempFeeSchema.setTempFeeNo(TempFeeNo);
      tLJTempFeeSchema.setTempFeeType("2");//交费类型为2：续期催收交费
      tVData.add(tLJTempFeeSchema);
      if(!tTempFeeQueryUI.submitData(tVData,"QUERY"))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tTempFeeQueryUI.mErrors);
        CError tError =new CError();
        tError.moduleName="IndiFinUrgeVerifyBLForAuto";
        tError.functionName="ReturnData";
        tError.errorMessage="暂交费查询失败";
        this.mErrors .addOneError(tError) ;
        return null;
      }
      tVData.clear();
      tVData = tTempFeeQueryUI.getResult();
      tLJTempFeeSet.set((LJTempFeeSet)tVData.getObjectByObjectName("LJTempFeeSet",0));
      tLJTempFeeSchema=(LJTempFeeSchema)tLJTempFeeSet.get(1);
      double tempMoney=tLJTempFeeSchema.getPayMoney();
//2-查询实收总表
      tVData.clear();
      LJSPaySchema tLJSPaySchema = new LJSPaySchema();
      LJSPaySet tLJSPaySet = new LJSPaySet();
      VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();
      tLJSPaySchema.setGetNoticeNo(TempFeeNo);
      tVData.add(tLJSPaySchema);
      if(!tVerDuePayFeeQueryUI.submitData(tVData,"QUERY"))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tVerDuePayFeeQueryUI.mErrors);
        CError tError =new CError();
        tError.moduleName="IndiFinUrgeVerifyBLForAuto";
        tError.functionName="ReturnData";
        tError.errorMessage="实收总查询失败";
        this.mErrors .addOneError(tError) ;
        return null;
      }
      tVData.clear();
      tVData = tVerDuePayFeeQueryUI.getResult();
      tLJSPaySet.set((LJSPaySet)tVData.getObjectByObjectName("LJSPaySet",0));
      tLJSPaySchema=(LJSPaySchema)tLJSPaySet.get(1);
      double sumDueMoney = tLJSPaySchema.getSumDuePayMoney();
      if(sumDueMoney!=tempMoney)
      {
        // @@错误处理
        CError tError =new CError();
        tError.moduleName="IndiFinUrgeVerifyBLForAuto";
        tError.functionName="ReturnData";
        tError.errorMessage="实收总表纪录中的金额和暂交费纪录中的金额不相等！";
        this.mErrors .addOneError(tError) ;
        return null;
      }
      tVData.clear();
      tVData.add(tLJTempFeeSchema);
      tVData.add(tLJSPaySchema);
//3-调用核销程序
      //将操作类型字符串
      this.mOperate ="VERIFY";
      //得到外部传入的数据,将数据备份到本类中
      if (!getInputData(tVData))
        return null;
System.out.println("After getinputdata");

      //进行业务处理
      if (!dealData())
        return null;
System.out.println("After dealData！");
      //准备往后台的数据
      if (!prepareOutputData())
        return null;

      return mInputData;
  }


  /**
   * 检查续保续期
   * @param tLCPolSchema
   * @return
   */
  private boolean checkRnew()
  {

      LDSysVarDB tLDSysVarDB=new LDSysVarDB();
      tLDSysVarDB.setSysVar("relationflag");
      if(tLDSysVarDB.getInfo()==false)
          return false;
      if(tLDSysVarDB.getSysVarValue().equals("0"))//0-续期续保不关联，1，2为关联
          return false;

      LCPolDB tLCPolDB=new LCPolDB();
      tLCPolDB.setPolNo(mLJSPayBL.getOtherNo());
      if(tLCPolDB.getInfo()==false)
      {
          return false;
      }

      LCRnewStateLogDB tLCRnewStateLogDB=new LCRnewStateLogDB();
      tLCRnewStateLogDB.setPrtNo(tLCPolDB.getPrtNo());
      LCRnewStateLogSet tLCRnewStateLogSet = tLCRnewStateLogDB.query();
      if(tLCRnewStateLogSet.size()>0)
          return true;

      return false;

  }

}

