package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class NormPayCollMultOperBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    /** 数据表  保存数据*/
    //个人保单表
    private LCPolSet mLCPolSet = new LCPolSet();
    private LCPolSchema mLCPolSchema = new LCPolSchema();

    //保费项表
    private LCPremSet mLCPremSet = new LCPremSet();
    private LCPremSchema mLCPremSchema = new LCPremSchema();

    //应收个人交费表
    private LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
    private LJSPayPersonSchema mLJSPayPersonSchema = new LJSPayPersonSchema();

    //因收总表
    private LJSPaySet tLJSPaySet = new LJSPaySet();
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

    //保险责任表
    private LCDutySet mLCDutySet = new LCDutySet();
    private LCDutySchema mLCDutySchema = new LCDutySchema();

    //集体保单表
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();

    //应收集体交费表
    private LJSPayGrpSchema tLJSPayGrpSchema;
    private LJSPayGrpSet tLJSPayGrpSet = new LJSPayGrpSet();
    //通知书打印表
    private LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
    private String mOpertor = "INSERT";
    private String mOperateFlag = "1"; // 1 : 个案催收，2：批量催收
    private String mserNo = "1"; //批次号
    private String EndDate = ""; //应收结束日期
    private String loadFlag = ""; //无名单标志


    private TransferData mTransferData = new TransferData();

    public NormPayCollMultOperBL() {
    }

    public static void main(String[] args) {
        NormPayCollMultOperBL tNormPayCollMultOperBL = new
                NormPayCollMultOperBL();

        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "000006";
        tGI.ManageCom = "86";
        VData tVData = new VData();
        tVData.add(tGI);

        if (tNormPayCollMultOperBL.submitData(tVData, "INSERT") == false) {
            System.out.println("团单批处理核销信息提示：：" +
                               tNormPayCollMultOperBL.mErrors.getFirstError());
        } else {
            System.out.println("团单批处理核销完成");
        }
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        if (!dealUrgeLog("3", "UPDATE")) {
            return false;
        }

        return true;
    }

    private boolean dealData() {
        //先查出符合条件的团单，然后接着调用GrpDueFeeBL的Submit");
        EndDate = (String) mTransferData.getValueByName("PayDate");
        String managecom = (String) tGI.ManageCom;
        String GrpContSql = "select distinct b.*"
                            + " from LJSPayGrp a ,LCGrpCont  b,LJSPay c "
                            + " where a.LastPayToDate <='" + EndDate + "' "
                            + " and a.GrpContNo=b.GrpContNo"
                            + " and c.OtherNo=a.GrpContNo"
                            + " and b.PayIntv>0 and b.AppFlag='1' and (b.StateFlag is null or b.StateFlag = '1')"
                            + " and c.othernotype='1' and c.OtherNo=b.GrpContNo"
                            + " and b.managecom like '" + managecom + "%'"
                            + " and ((select count(*) from LJTempFee where TempFeeNo= a.GetNoticeNo  and ConfFlag='0') > 0" 
                            + " or exists (select 1 from LJSPayGrp where getnoticeno = a.getnoticeno and paytype = 'YEL'))" 
                            + " and a.getnoticeno = c.getnoticeno "
                            + " and exists(select RiskCode from LMRiskPay where UrgePayFlag='Y' and RiskCode=a.RiskCode)"
                            + " and not exists(select 1 from LCCont where grpContNo = b.grpContNo and polType = '1')"
                            ;
        if(loadFlag != null && loadFlag.equals("WMD"))
        {
        	GrpContSql  = "select distinct b.*"
		                + " from LJSPayGrp a ,LCGrpCont  b,LJSPay c "
		                + " where a.LastPayToDate <='" + EndDate + "' "
		                + " and a.GrpContNo=b.GrpContNo"
		                + " and c.OtherNo=a.GrpContNo"
		                + " and b.PayIntv>0 and b.AppFlag='1' and (b.StateFlag is null or b.StateFlag = '1')"
		                + " and c.othernotype='1' and c.OtherNo=b.GrpContNo"
		                + " and b.managecom like '" + managecom + "%'"
		                + " and ((select count(*) from LJTempFee where TempFeeNo= a.GetNoticeNo  and ConfFlag='0') > 0"
		                + " or exists (select 1 from LJSPayGrp where getnoticeno = a.getnoticeno and paytype = 'YEL'))" 
                        + " and  a.getnoticeno = c.getnoticeno "
		                + " and  exists(select RiskCode from LMRiskPay where UrgePayFlag='Y' and RiskCode=a.RiskCode)"
		                + " and  exists(select 1 from LCCont where grpContNo = b.grpContNo and polType = '1')"
		                ;
        }
        System.out.println(GrpContSql);
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        tLCGrpContSet = tLCGrpContDB.executeQuery(GrpContSql);
        if (tLCGrpContDB.mErrors.needDealError()) {
            CError.buildErr(this, "查询符合核销条件的团单失败！");
            return false;
        }
        if (tLCGrpContSet.size() == 0) {
            CError.buildErr(this, "没有符合核销条件的团单信息！");
            return false;
        }
        int succCount = 0;
        String tLimit = PubFun.getNoLimit(managecom);
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        mserNo = serNo;
        if (!dealUrgeLog("1", "INSERT")) {
            return false;
        }

        for (int i = 1; i <= tLCGrpContSet.size(); i++) {
            String grpContNo = tLCGrpContSet.get(i).getGrpContNo();
            LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
            tLCGrpContSchema.setGrpContNo(grpContNo);
            VData tVData = new VData();
            tVData.add(loadFlag);
            tVData.add(tLCGrpContSchema);
            tVData.add(tGI);
            mTransferData.setNameAndValue("GrpSerNo", serNo);
            tVData.add(mTransferData);
            NormPayCollOperUI tNormPayCollOperUI = new NormPayCollOperUI();
            if (!tNormPayCollOperUI.submitData(tVData, "VERIFY")) {
                //如果处理失败
                this.mErrors.addOneError("团单号" + grpContNo + "核销失败，原因如下：" +
                                         tNormPayCollOperUI.mErrors.
                                         getFirstError());

            } else {
                succCount++;
            }
        }
        if (succCount == tLCGrpContSet.size()) {
            System.out.println("本次批量核销全部成功！");
        }
        return true;
    }

    //Yangh于2005-07-20将下面代码注销
    /** private boolean dealData() {
       String Operator = tGI.Operator; //保存登陆管理员账号
       String ManageCom = tGI.ComCode; //保存登陆区站,ManageCom=内存中登陆区站代码

       String tLimit = "";
       String tNo = ""; //产生的通知书号
       String serNo = ""; //流水号
       int operCount = 0; //保存成功处理的纪录数
       int errorCount = 0; //事务处理时发生错误数
       VData tVData = new VData();
       //判断一下查询条件先,鉴于在此处添加了判断，应将LCGrpPolQueryBL中的相应判断删除
       if(mTransferData!=null)
       {
     String tCurrentDate=(String)mTransferData.getValueByName("StartDate");
           String tSubDate=(String)mTransferData.getValueByName("EndDate");
           if(tCurrentDate==null||tSubDate==null||tCurrentDate.equals("")||tSubDate.equals(""))
           {
               CError.buildErr(this,"必须录入日期范围!");
               return false;
           }
           FDate tD=new FDate();
           if(!tD.getDate(tCurrentDate).before(tD.getDate(tSubDate)))
           {
               CError.buildErr(this,"日期范围录入错误!");
               return false;
           }
       }
       else
       {
           CError.buildErr(this,"必须录入日期范围!");
           return false;
       }
       //产生流水号
       tLimit = PubFun.getNoLimit(ManageCom);
       serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
       //Step 0: query from LCGrpCont table
       LCGrpContSet tLCGrpContSet=new LCGrpContSet();
       LCGrpPolQueryBL ttLCGrpPolQueryBL = new LCGrpPolQueryBL();
       tLCGrpContSet=ttLCGrpPolQueryBL.queryLJGrpCont(mTransferData,ManageCom);
       //查询结束
       if(tLCGrpContSet.size()==0)
       {
           CError tError = new CError();
           tError.moduleName = "GrpDueFeeMultiBL";
           tError.functionName = "queryLJGrpCont";
           tError.errorMessage = "没有符合条件集体保单，请您确认!";
           this.mErrors.addOneError(tError);
           return false;
       }
       for(int x=1;x<=tLCGrpContSet.size();x++)
       {
       //应收总表费额
       double totalsumPay = 0;
       double LeavingMoney = 0; //如果有余额，保存余额
       //应收总表交费日期
       double totalPay=0;//计算paytype='ZC'交费总和
       String  strNewLJSPayDate="";
       String  strNewLJSStartDate="";
       LCGrpContSchema tLCGrpContSchema=new LCGrpContSchema();
       tLCGrpContSchema=tLCGrpContSet.get(x);
       LeavingMoney = tLCGrpContSchema.getDif();//余额从合同上取，险种进行分配
       //Step 1: query from LCGrpPol table
       //注意：startDate是放在首期交费日期字段保存，endDate是放在终交日期字段放置

       LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
       String GrpContNo=tLCGrpContSchema.getGrpContNo();
       tLCGrpPolSchema.setGrpContNo(GrpContNo);
       tLCGrpPolSchema.setManageCom(ManageCom);
       tLCGrpPolSchema.setRiskCode("Y"); //设置催交标记
       tVData.add(tLCGrpPolSchema);
       tVData.add(mTransferData);
       LCGrpPolQueryBL tLCGrpPolQueryBL = new LCGrpPolQueryBL();
       if (!tLCGrpPolQueryBL.submitData(tVData, "QUERY")) {
         this.mErrors.copyAllErrors(tLCGrpPolQueryBL.mErrors);
         return false;
       }
       tVData.clear();
       mLCGrpPolSet = new LCGrpPolSet();
       tVData = tLCGrpPolQueryBL.getResult();
     mLCGrpPolSet.set( (LCGrpPolSet) tVData.getObjectByObjectName("LCGrpPolSet",
           0));
       if (mLCGrpPolSet.size() == 0)
         return true;
       //计算新的交至日期
       FDate tD = new FDate();
       Date newBaseDate = new Date();
       String CurrentDate = PubFun.getCurrentDate();
       String CurrentPayToDate = "";
       int PayInterval = 0;
       Date NewPayToDate = new Date();
       Date NewPayDate = new Date();
       String strNewPayToDate = "";
       String strNewPayDate = "";
       LJSPaySchema tLJSPaySchema = new LJSPaySchema();
       LOPRTManagerSchema  tLOPRTManagerSchema =new LOPRTManagerSchema ();
       LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();
       LJSPayPersonSet  mLJSPayPersonSet=new  LJSPayPersonSet();
       // 产生通知书号-集体和集体下个人共用一个交费通知书号
       boolean yelFlag = false;
       boolean yetFlag = false;
       tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
       tNo = PubFun1.CreateMaxNo("PayNoticeNo", tLimit);
       //-----------查询到批量集体，开始批处理循环催收--------------------
       for (int j = 1; j <= mLCGrpPolSet.size(); j++) {
           //初始化相关变量
          // boolean yelFlag = false; //默认没有//判断集体保单表中是否有差额的标志
           LJSPayGrpSchema yelLJSPayGrpSchema = new LJSPayGrpSchema(); //如果有差额，添加一个应收集体交费schema

           //判断余额是否>(保费项表.实际保费*保险责任表.免交比率)累计和 的标志
          // boolean yetFlag = false; //默认不是（yelFlag=true 是前提条件）
           LJSPayGrpSchema yetLJSPayGrpSchema = new LJSPayGrpSchema(); //如果差额>累计和，添加一个应收集体交费schema
           double sumPay = 0; //保存累计和的变量
           int recordCount = 0; //返回的符合查询条件的纪录的数目
           int queryCount = 0; //纪录集体保单中的个人客户数目
           int payPersonCount = 0; //添加的应收个人交费纪录的数目：如果=0 表明催收无效
           int payCount = 0; //纪录交费次数：集体下个人的最大交费次数
           String GrpPolNo = "";
           LCPolSchema tLCPolSchema = new LCPolSchema();
           LJSPayPersonSchema tLJSPayPersonSchema = new LJSPayPersonSchema();
           LCPremSchema tLCPremSchema = new LCPremSchema();
           LCDutySchema tLCDutySchema = new LCDutySchema();
           //初始化变量结束
     tLCGrpPolSchema = (LCGrpPolSchema) mLCGrpPolSet.get(j); //保存后用,序号从1开始
           GrpPolNo = tLCGrpPolSchema.getGrpPolNo();

           System.out.println("原交至日期" + tLCGrpPolSchema.getPaytoDate());
           CurrentPayToDate = tLCGrpPolSchema.getPaytoDate();
           //为总表取得最早交费日期,取所有集体险种中最大的
           if(strNewLJSStartDate==null||strNewLJSStartDate.equals(""))
           {
               strNewLJSStartDate=CurrentPayToDate;
           }
           else
           {
     strNewLJSStartDate=PubFun.getLaterDate(strNewLJSStartDate,CurrentPayToDate) ;
           }

           PayInterval = tLCGrpPolSchema.getPayIntv();
           newBaseDate = tD.getDate(CurrentPayToDate);
           NewPayToDate = PubFun.calDate(newBaseDate, PayInterval, "M", null);
           strNewPayToDate = tD.getString(NewPayToDate);
           System.out.println("现交至日期" + strNewPayToDate);
           //交费日期=失效日期=原交至日期+2个月
           NewPayDate = PubFun.calDate(newBaseDate, 2, "M", null);
           strNewPayDate = tD.getString(NewPayDate);
           //为总表取得交费日期,取所有集体险种中最小的
           if(strNewLJSPayDate==null||strNewLJSPayDate.equals(""))
           {
               strNewLJSPayDate=strNewPayDate;
           }
           else
           {
     strNewLJSPayDate=PubFun.getBeforeDate(strNewLJSPayDate,strNewPayDate) ;
           }
           //Step 2: query from LCPol table
           tVData.clear();
           tLCPolSchema = new LCPolSchema();
           tLCPolSchema.setGrpPolNo(GrpPolNo);
           tLCPolSchema.setManageCom(ManageCom);
           tLCPolSchema.setRiskCode("Y"); //设置催交标记
           tLCPolSchema.setPayIntv( -1); //设置交费间隔不等于0:注意是!=0

           tVData.add(tLCPolSchema);
           LCPolQueryBL tLCPolQueryBL = new LCPolQueryBL();
           if (!tLCPolQueryBL.submitData(tVData, "QUERY"))
               continue;
           tVData.clear();
           mLCPolSet = new LCPolSet();
           tVData = tLCPolQueryBL.getResult();
           mLCPolSet.set((LCPolSet) tVData.getObjectByObjectName(
                   "LCPolSet", 0));
           String PolNo = "";
           queryCount = mLCPolSet.size();
           //----------------开始循环处理 1 (由多条个人保单纪录整理出多条应收个人纪录)----------------
           for (int n = 1; n <= queryCount; n++)
           {
               tLCPolSchema = (LCPolSchema) mLCPolSet.get(n); //保存后用,序号从1开始
               PolNo = tLCPolSchema.getPolNo();
               //Step 3 :query  from LJSPayPerson table
               tVData.clear();
               tLJSPayPersonSchema = new LJSPayPersonSchema();
               tLJSPayPersonSchema.setPolNo(PolNo);
               tVData.add(tLJSPayPersonSchema);
               DuePayPersonFeeSimQueryBL tDuePayPersonFeeSimQueryBL = new
                       DuePayPersonFeeSimQueryBL();
               if (!tDuePayPersonFeeSimQueryBL.submitData(tVData, "QUERY"))
                   continue;
               //Step 4:query from LCPrem table
               tVData.clear();
               tLCPremSchema = new LCPremSchema();
               tLCPremSchema.setPolNo(PolNo);
               tLCPremSchema.setUrgePayFlag("Y");
               tVData.add(tLCPremSchema);
               LCPremQueryBL tLCPremQueryBL = new LCPremQueryBL();
               if (!tLCPremQueryBL.submitData(tVData, "QUERY"))
                   continue;

               tVData.clear();
               mLCPremSet = new LCPremSet();
               tVData = tLCPremQueryBL.getResult();
               mLCPremSet.set((LCPremSet) tVData.getObjectByObjectName(
                       "LCPremSet", 0));

               //Step 5 : query from LCDuty
               //循环处理
               //计算交费日期和现交至日期 而定义一些处理变量：
               Date baseDate = new Date();
               Date paytoDate = new Date(); //交至日期
               Date payDate = new Date(); //交费日期
               int interval = 0; //交费间隔
               String unit = ""; //日期单位（Y:年 M:月 D:日）
               FDate fDate = new FDate();
               recordCount = mLCPremSet.size();
               mLCDutySet = new LCDutySet(); //保存保险责任表纪录集合
               tLCDutySchema = new LCDutySchema();
               LCDutySet tempLCDutySet = new LCDutySet(); //临时存放保险责任表纪录集合的容器
               LCPremSet saveLCPremSet = new LCPremSet(); //保存过滤后的保费项纪录集
               for (int i = 1; i <= recordCount; i++)
               {
                   tLCPremSchema = null;
                   tLCPremSchema = (LCPremSchema) mLCPremSet.get(i);
                   tLCDutySchema.setPolNo(tLCPremSchema.getPolNo());
                   tLCDutySchema.setDutyCode(tLCPremSchema.getDutyCode());
                   tVData.clear();
                   tVData.add(tLCDutySchema);
                   //*.java内置查询条件:免交比率>0.0
                     LCDutyQueryBL tLCDutyQueryBL = new LCDutyQueryBL();
                     if (!tLCDutyQueryBL.submitData(tVData, "QUERY"))
                     {
                         ;
                     }
                     else
                     {
                         tVData.clear();
                         tVData = tLCDutyQueryBL.getResult();
                         tempLCDutySet.set((LCDutySet) tVData.
                                           getObjectByObjectName(
                                 "LCDutySet", 0));
                         tLCDutySchema = (LCDutySchema) tempLCDutySet.get(1);
                         mLCDutySet.add(tLCDutySchema);
                         saveLCPremSet.add(tLCPremSchema);
                         sumPay = sumPay +
                                  (1 - tLCDutySchema.getFreeRate()) *
                                  tLCPremSchema.getPrem();
                         //杨红于 2005-5-25 添加
                         totalPay+=sumPay;
                     }
                 } //end for()

                 if (saveLCPremSet.size() == 0)
                 { //如果过滤后的保费项表纪录数=0
                     continue;
                 }
                 else
                 {
                     //Step 6 :将个人保单纪录，保费项表集合，保险责任表集合整理成应收个人交费纪录集合
                     tLJSPayPersonSchema = new LJSPayPersonSchema();
                     for (int index = 1; index <= saveLCPremSet.size();
                                      index++)
                     {
                         tLCPremSchema = saveLCPremSet.get(index); //保费项纪录
                         tLCDutySchema = mLCDutySet.get(index); //保险责任纪录
                         //设置应收个人表(从个人保单，保费项，保险责任中取)
                         tLJSPayPersonSchema = new LJSPayPersonSchema();
                         tLJSPayPersonSchema.setPolNo(PolNo);
                         tLJSPayPersonSchema.setPayCount(tLCPremSchema.
                                 getPayTimes() + 1);
                         tLJSPayPersonSchema.setGrpPolNo(tLCPolSchema.
                                 getGrpPolNo());
                         tLJSPayPersonSchema.setGrpContNo(GrpContNo);
                         tLJSPayPersonSchema.setContNo(tLCPolSchema.
                                 getContNo());
                         tLJSPayPersonSchema.setAppntNo(tLCPolSchema.
                                 getAppntNo());
      tLJSPayPersonSchema.setGetNoticeNo(tNo); //集体和个人共用一个tNo
                         tLJSPayPersonSchema.setPayAimClass("2"); //交费目的分类=集体
                         tLJSPayPersonSchema.setDutyCode(tLCPremSchema.
                                 getDutyCode());
                         tLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.
                                 getPayPlanCode());
                         tLJSPayPersonSchema.setSumDuePayMoney(tLCPremSchema.
                                 getPrem() *
                                 (1 -
                                  tLCDutySchema.getFreeRate()));
                         tLJSPayPersonSchema.setSumActuPayMoney(
                                 tLCPremSchema.getPrem() *
                                 (1 - tLCDutySchema.getFreeRate()));
                         tLJSPayPersonSchema.setPayIntv(tLCPremSchema.
                                 getPayIntv());
                         // 交费日期=失效日期 =现交至日期+2个月 （待修订）repair:
                         //下面计算 现交至日期
      baseDate = fDate.getDate(tLCPremSchema.getPaytoDate());
                         interval = tLCPremSchema.getPayIntv();
                         if (interval == -1) interval = 0; // 不定期缴费
                         unit = "M";
                         //repair:日期不能为空，否则下面的函数处理会有问题
                         String strPayDate = "";
                         String strPayToDate = "";
                         if (baseDate != null)
                         {
                             paytoDate = PubFun.calDate(baseDate, interval,
                                     unit, null);
                             //计算失效日期
      payDate = PubFun.calDate(paytoDate, 2, unit, null);
                             //得到日期型，转换成String型
                             strPayToDate = fDate.getString(paytoDate);
                             strPayDate = fDate.getString(payDate);
                         }
                         tLJSPayPersonSchema.setPayDate(strPayDate);
                         //-----------------------------------------------------------
                         tLJSPayPersonSchema.setPayType("ZC"); //交费类型=ZC即"正常"
                         tLJSPayPersonSchema.setLastPayToDate(tLCPremSchema.
                                 getPaytoDate());
                         //原交至日期=保费项表的交至日期
                         //现交至日期CurPayToDate=交至日期+日期（interval=12 12个月-年交，interval=1 即1个月-月交）
                         tLJSPayPersonSchema.setCurPayToDate(strPayToDate);
                         //tLJSPayPersonSchema.setBankCode(tLCPolSchema.getBankCode());
                         //tLJSPayPersonSchema.setBankAccNo(tLCPolSchema.getBankAccNo());
                         tLJSPayPersonSchema.setBankOnTheWayFlag("0");
                         tLJSPayPersonSchema.setBankSuccFlag("0");
                         tLJSPayPersonSchema.setApproveCode(tLCPolSchema.
                                 getApproveCode());
                         tLJSPayPersonSchema.setApproveDate(tLCPolSchema.
                                 getApproveDate());
                         tLJSPayPersonSchema.setSerialNo(serNo); //批次统一流水号
                         tLJSPayPersonSchema.setOperator(Operator);
                         tLJSPayPersonSchema.setManageCom(tLCPolSchema.
                                 getManageCom());
                         tLJSPayPersonSchema.setAgentCom(tLCPolSchema.
                                 getAgentCom());
                         tLJSPayPersonSchema.setAgentType(tLCPolSchema.
                                 getAgentType());
                         tLJSPayPersonSchema.setRiskCode(tLCPolSchema.
                                 getRiskCode());
                         tLJSPayPersonSchema.setAgentCode(tLCPolSchema.
                                 getAgentCode());
                         tLJSPayPersonSchema.setAgentGroup(tLCPolSchema.
                                 getAgentGroup());
                         mLJSPayPersonSet.add(tLJSPayPersonSchema);
                         payPersonCount = payPersonCount + 1;
                         if (payCount < tLCPremSchema.getPayTimes() + 1)
                         {
                             payCount = tLCPremSchema.getPayTimes() + 1;
                         }
                     } //end for
                 }
             }

             //---------------整理数据（多条应收个人交费纪录，应收集体交费纪录和一条应收总表纪录）
             if (payPersonCount > 0)
             { //如果有应收个人交费纪录，催收有效
                 //将应收个人交费纪录取出，为了后面的字段赋值方便
                 tLJSPayPersonSchema = (LJSPayPersonSchema) mLJSPayPersonSet.
                                       get(1);
                 //第1步--设置应收集体交费纪录
                 tLJSPayGrpSchema = new LJSPayGrpSchema();
                 tLJSPayGrpSchema.setGrpPolNo(GrpPolNo);
                 tLJSPayGrpSchema.setPayCount(payCount); //交费次数
      tLJSPayGrpSchema.setGrpContNo(tLCGrpContSchema.getGrpContNo());
                 tLJSPayGrpSchema.setAppntNo(tLJSPayPersonSchema.getAppntNo());
                 tLJSPayGrpSchema.setGetNoticeNo(tNo); //同一集体单下用同一个tNo
                 tLJSPayGrpSchema.setSumDuePayMoney(sumPay);
                 tLJSPayGrpSchema.setSumActuPayMoney(sumPay);
                 tLJSPayGrpSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
                 tLJSPayGrpSchema.setPayDate(strNewPayDate); //交费类型=ZC 正常交费
                 tLJSPayGrpSchema.setPayType("ZC");
                 tLJSPayGrpSchema.setLastPayToDate(tLCGrpPolSchema.
                         getPaytoDate());
                 tLJSPayGrpSchema.setCurPayToDate(strNewPayToDate);
                 tLJSPayGrpSchema.setApproveCode(tLCGrpPolSchema.
                         getApproveCode());
                 tLJSPayGrpSchema.setApproveDate(tLCGrpPolSchema.
                         getApproveDate());
                 tLJSPayGrpSchema.setManageCom(ManageCom);
                 tLJSPayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
                 tLJSPayGrpSchema.setAgentType(tLCGrpPolSchema.getAgentType());
                 tLJSPayGrpSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
                 tLJSPayGrpSchema.setSerialNo(serNo);
                 tLJSPayGrpSchema.setOperator(Operator);
                 tLJSPayGrpSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
                 tLJSPayGrpSchema.setAgentGroup(tLCGrpPolSchema.
                         getAgentGroup());
                 tLJSPayGrpSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
                 tLJSPayGrpSchema.setAgentGroup(tLCGrpPolSchema.
                         getAgentGroup());
                 mLJSPayGrpSet.add(tLJSPayGrpSchema);
                 //第2步--判断差额，设置集体交费纪录
                 //2005-5-25更新余额应从合同的字段去取
                // LeavingMoney = tLCGrpPolSchema.getDif();
                //2005-5-25修正，yel的记录只有一条
                 if (LeavingMoney > 0&&j==1)
                 { //如果差额>0
                     //2-1 设置交费方式为YEL的集体交费纪录
                     yelFlag=true;
                     yelLJSPayGrpSchema = new LJSPayGrpSchema();
                     yelLJSPayGrpSchema.setGrpPolNo(GrpPolNo);
       yelLJSPayGrpSchema.setGrpContNo(tLCGrpContSchema.getGrpContNo());
                     yelLJSPayGrpSchema.setAppntNo(tLJSPayPersonSchema.
                             getAppntNo());
                     yelLJSPayGrpSchema.setGetNoticeNo(tNo);
                     yelLJSPayGrpSchema.setSumDuePayMoney( -LeavingMoney);
                     yelLJSPayGrpSchema.setSumActuPayMoney( -LeavingMoney);
                     yelLJSPayGrpSchema.setPayIntv(tLCGrpPolSchema.
                             getPayIntv());
                     yelLJSPayGrpSchema.setPayDate(strNewPayDate); //交费日期
                     yelLJSPayGrpSchema.setPayType("YEL"); //交费类型=yel
                     yelLJSPayGrpSchema.setLastPayToDate(tLCGrpPolSchema.
                             getPaytoDate());
                     yelLJSPayGrpSchema.setCurPayToDate(strNewPayToDate);
                     yelLJSPayGrpSchema.setApproveCode(tLCGrpPolSchema.
                             getApproveCode());
                     yelLJSPayGrpSchema.setApproveDate(tLCGrpPolSchema.
                             getApproveDate());
                     yelLJSPayGrpSchema.setManageCom(ManageCom);
                     yelLJSPayGrpSchema.setAgentCom(tLCGrpPolSchema.
                             getAgentCom());
                     yelLJSPayGrpSchema.setAgentType(tLCGrpPolSchema.
                             getAgentType());
                     yelLJSPayGrpSchema.setRiskCode(tLCGrpPolSchema.
                             getRiskCode());
                     yelLJSPayGrpSchema.setAgentCode(tLCGrpPolSchema.
                             getAgentCode());
                     yelLJSPayGrpSchema.setAgentGroup(tLCGrpPolSchema.
                             getAgentGroup());
                     yelLJSPayGrpSchema.setSerialNo(serNo);
                     yelLJSPayGrpSchema.setOperator(Operator);
                   //于2005-5-25将此段注销
                     double tsumPay=sumPay;//暂存一下，备后面余额用
                     sumPay = sumPay - LeavingMoney; //应交集体表和应交总表中存放应缴款
                     if(sumPay>=0)//如果该险种将上次余额被用光
                     {
                       LeavingMoney=0;
                     }
                     else//如果上次余额在支付当前险种本次缴费后尚有剩余
                     {
                         sumPay=0;
                         LeavingMoney=LeavingMoney-tsumPay;
                     }
                     //
                     mLJSPayGrpSet.add(yelLJSPayGrpSchema);
                 }
                     //2-2 如果余额>应交款 设置差额 YET纪录
                     if(j==mLCGrpPolSet.size()){
                     if (totalPay-LeavingMoney < 0)
                     {
                         yetFlag=true;
                       //  sumPay = 0; //应交集体表和应交总表中存放应缴款
                         yetLJSPayGrpSchema = new LJSPayGrpSchema();
                         yetLJSPayGrpSchema.setGrpPolNo(GrpPolNo);
       yetLJSPayGrpSchema.setGrpContNo(tLCGrpContSchema.getGrpContNo());
                         yetLJSPayGrpSchema.setAppntNo(tLJSPayPersonSchema.
                                 getAppntNo());
                         yetLJSPayGrpSchema.setGetNoticeNo(tNo);
       yetLJSPayGrpSchema.setSumDuePayMoney(LeavingMoney-totalPay);
       yetLJSPayGrpSchema.setSumActuPayMoney(LeavingMoney-totalPay);
                         //如果余额>应缴款，那么用另一种核销方式
                         yetLJSPayGrpSchema.setPayIntv(tLCGrpPolSchema.
                                 getPayIntv());
                         yetLJSPayGrpSchema.setPayDate(strNewPayDate); //交费日期
                         yetLJSPayGrpSchema.setPayType("YET"); //交费类型=yet
                         yetLJSPayGrpSchema.setLastPayToDate(tLCGrpPolSchema.
                                 getPaytoDate());
                         yetLJSPayGrpSchema.setCurPayToDate(strNewPayToDate);
                         yetLJSPayGrpSchema.setApproveCode(tLCGrpPolSchema.
                                 getApproveCode());
                         yetLJSPayGrpSchema.setApproveDate(tLCGrpPolSchema.
                                 getApproveDate());
                         yetLJSPayGrpSchema.setManageCom(ManageCom);
                         yetLJSPayGrpSchema.setAgentCom(tLCGrpPolSchema.
                                 getAgentCom());
                         yetLJSPayGrpSchema.setAgentType(tLCGrpPolSchema.
                                 getAgentType());
                         yetLJSPayGrpSchema.setRiskCode(tLCGrpPolSchema.
                                 getRiskCode());
                         yetLJSPayGrpSchema.setAgentCode(tLCGrpPolSchema.
                                 getAgentCode());
                         yetLJSPayGrpSchema.setAgentGroup(tLCGrpPolSchema.
                                 getAgentGroup());
                         yetLJSPayGrpSchema.setSerialNo(serNo);
                         yetLJSPayGrpSchema.setOperator(Operator);

                         mLJSPayGrpSet.add(yetLJSPayGrpSchema);
                     }
                 }//对应495行
             }
             //计算应收总表的交费金额
           //  totalsumPay = totalsumPay + sumPay;
         }
         //于2005-5-24添加开始
        if(yelFlag)
        {
          //如果团单上有余额,并且余额足够支付团单下应交的所有费用
          if(yetFlag)
          {
            totalsumPay=0;
          }
          else
          {
            totalsumPay=totalPay-LeavingMoney;
          }
        }
        else
          totalsumPay=totalPay;
       //于2005-5-24添加结束



         tLJSPayPersonSet.add(mLJSPayPersonSet);//将集体险种的应收个人集添加到合同中
         tLJSPayGrpSet.add(mLJSPayGrpSet);//将集体险种的应收集体集添加到合同中
         //第三步：设置应收总表纪录
         tLJSPaySchema.setGetNoticeNo(tNo);
         tLJSPaySchema.setOtherNo(GrpContNo);
         tLJSPaySchema.setOtherNoType("1");
         tLJSPaySchema.setAppntNo(tLCGrpContSchema.getAppntNo());
         tLJSPaySchema.setSumDuePayMoney(totalsumPay);
         tLJSPaySchema.setPayDate(strNewLJSPayDate);//有冲突,暂定为取集体险种最早的日期
         tLJSPaySchema.setStartPayDate(strNewLJSStartDate); //交费最早应缴日期保存上次交至日期,暂定为取集体险种最晚的日期/
         tLJSPaySchema.setBankOnTheWayFlag("0");
         tLJSPaySchema.setBankSuccFlag("0");
         tLJSPaySchema.setSendBankCount(0);
         tLJSPaySchema.setApproveCode(tLCGrpContSchema.getApproveCode());
         tLJSPaySchema.setApproveDate(tLCGrpContSchema.getApproveDate());
         //tLJSPaySchema.setRiskCode(tLCGrpPolSchema.getRiskCode());lis6.0 update by yuanaq
         //tLJSPaySchema.setBankAccNo(tLCGrpPolSchema.getBankAccNo());
         //tLJSPaySchema.setBankCode(tLCGrpPolSchema.getBankCode());
         tLJSPaySchema.setSerialNo(serNo);
         tLJSPaySchema.setOperator(Operator);
         tLJSPaySchema.setManageCom(tLCGrpContSchema.getManageCom());
         tLJSPaySchema.setAgentCom(tLCGrpContSchema.getAgentCom());
         tLJSPaySchema.setAgentCode(tLCGrpContSchema.getAgentCode());
         tLJSPaySchema.setAgentType(tLCGrpContSchema.getAgentType());
         tLJSPaySchema.setAgentGroup(tLCGrpContSchema.getAgentGroup());
         tLJSPaySet.add(tLJSPaySchema);
         tLOPRTManagerSchema = getPrintData(
                 tLCGrpContSchema);
         if (tLOPRTManagerSchema == null)
         {
             CError tError = new CError();
             tError.moduleName = "GrpDueFeeMultBL";
             tError.functionName = "dealData";
             tError.errorMessage = "打印数据准备失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
         tLOPRTManagerSet.add(tLOPRTManagerSchema);
       }
       tVData.clear();
       tVData.add(tLJSPayPersonSet);
       tVData.add(tLJSPayGrpSet);
       tVData.add(tLJSPaySet);
       tVData.add(tLOPRTManagerSet);

       //提交到后台事务处理：添加纪录到应收集体，个人，总表
       DuePayFeeGrpUI tDuePayFeeGrpUI = new DuePayFeeGrpUI();
       tDuePayFeeGrpUI.submitData(tVData, "INSERT");
       if (tDuePayFeeGrpUI.mErrors.needDealError())
       {
           this.mErrors.addOneError("保单出现错误:" +
                                    tDuePayFeeGrpUI.mErrors.getFirstError());
           errorCount = errorCount + 1; //失败纪录数
       }
       else
       {
           operCount = operCount + 1; //成功纪录数
       }

       return true;
       }*/


     /**
      * 从输入数据中得到所有对象
      *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
      */
     private boolean getInputData(VData mInputData) {
    	 loadFlag = (String)mInputData.getObjectByObjectName("String", 0);
         tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
         mTransferData = (TransferData) mInputData.getObjectByObjectName(
                 "TransferData", 0);
         if (tGI == null || mTransferData == null) {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "GrpDueFeeMultiBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "没有得到足够的数据，请您确认!";
             this.mErrors.addOneError(tError);
             return false;
         }
         return true;
     }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        mInputData = new VData();
        return true;
    }

    /**
     * 加入到催收核销日志表数据
     * @param
     * @return boolean
     */
    private boolean dealUrgeLog(String pmDealState, String pmOpreat) {

        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                LCUrgeVerifyLogSchema();
        //加到催收核销日志表
        tLCUrgeVerifyLogSchema.setSerialNo(mserNo);
        tLCUrgeVerifyLogSchema.setRiskFlag("1");
        tLCUrgeVerifyLogSchema.setEndDate(EndDate);

        tLCUrgeVerifyLogSchema.setOperateType("2"); //1：续期催收操作,2：续期核销操作
        tLCUrgeVerifyLogSchema.setOperateFlag("2"); //1：个案操作,2：批次操作
        tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
        tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成
        if (pmOpreat.equals("INSERT")) {
            tLCUrgeVerifyLogSchema.setMakeDate(CurrentDate);
            tLCUrgeVerifyLogSchema.setMakeTime(CurrentTime);
            tLCUrgeVerifyLogSchema.setModifyDate(CurrentDate);
            tLCUrgeVerifyLogSchema.setModifyTime(CurrentTime);
            tLCUrgeVerifyLogSchema.setErrReason("");

        } else {
            LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new
                    LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mserNo);
            tLCUrgeVerifyLogDB.setOperateType("2");
            tLCUrgeVerifyLogDB.setOperateFlag("2");
            tLCUrgeVerifyLogDB.setRiskFlag("1");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
            if (!(tLCUrgeVerifyLogSet == null) &&
                tLCUrgeVerifyLogSet.size() > 0) {
                tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                tLCUrgeVerifyLogSchema.setDealState(pmDealState);
            }else{
                return false;
            }
        }

        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
            return false;
        }
        return true;
    }

    /**
     * 准备打印数据
     * @param tLCPolSchema
     * @return
     */
    public LOPRTManagerSchema getPrintData(LCGrpContSchema tLCGrpContSchema) {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        try {
            String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
            tLOPRTManagerSchema.setOtherNoType("01");
            tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPAY);
            tLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
            tLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setReqOperator(tLCGrpContSchema.getOperator());
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        } catch (Exception ex) {
            return null;
        }
        return tLOPRTManagerSchema;
    }

}
