package com.sinosoft.lis.operfee;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.utility.SysConst;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.task.Task;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.schema.LJSPayBSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author by fuxin
 * @version 1.0
 */
public class ChangePayModeForChildBL {

    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private MMap map = new MMap();

    private String mPayMode ;

    private String mBankCode ;

    private String mAccName ;

    private String mBankAccNo ;

    private String mGetNoticeNo ;

    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

    private LJSPaySchema tLJSPaySchema = new LJSPaySchema();

    private LCContSchema tLCContSchema = new LCContSchema();

    private LCContSchema tLCContSchemaOld = new LCContSchema();

    public ChangePayModeForChildBL() {
    }

    /**
     * 提交变更给付方式接口
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate) {
        MMap tMMap = this.getSubmitMap(data, operate);
        if (tMMap == null) {
            return false;
        }

        VData tVData = new VData();
        tVData.add(tMMap);

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tVData, "")) {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("提交数库失败");
            return false;
        }

        return true;
    }

    /**
     * 提交变更给付方式接口
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public MMap getSubmitMap(VData data, String operate) {
        if (!getInputData(data)) {
            return null;
        }

        if (!checkData()) {
            return null;
        }

        if (!dealData()) {
            return null;
        }

        return map;
    }

    private boolean getInputData(VData data)
    {
        mGI.setSchema((GlobalInput) data.getObjectByObjectName("GlobalInput", 0));
        mLJSPaySchema = (LJSPaySchema) data.getObjectByObjectName("LJSPaySchema",0);
        mTransferData = (TransferData) data.getObjectByObjectName("TransferData", 0);
        mPayMode =(String) mTransferData.getValueByName("PayMode");
        mBankCode=(String) mTransferData.getValueByName("BankCode");
        mAccName =(String) mTransferData.getValueByName("AccName");
        mBankAccNo= (String) mTransferData.getValueByName("BankAccNo");

        if(mLJSPaySchema==null)
        {
            mErrors.addOneError("获取应收失败！");
            return false ;
        }

        return true ;
    }

    private boolean checkData()
    {
        return true ;
    }

    private boolean dealData()
    {
        LJSPayDB tLSJPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = new LJSPaySet();

        tLSJPayDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
        if(!tLSJPayDB.getInfo())
        {
            mErrors.addOneError("查询应收记录失败！");
            return false ;
        }
        tLJSPaySchema = tLSJPayDB.getSchema();
        String tContNo = tLJSPaySchema.getOtherNo();
        System.out.println("保单号：：：："+tContNo);

        tLJSPaySchema.setAccName(mAccName);
        tLJSPaySchema.setBankAccNo(mBankAccNo);
        tLJSPaySchema.setBankCode(mBankCode);
        tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());

        map.put(tLJSPaySchema,SysConst.UPDATE);

        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
        if(!tLJSPayBDB.getInfo())
        {
            mErrors.addOneError("查询应收记录失败！");
            return false ;
        }

        LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
        tLJSPayBSchema = tLJSPayBDB.getSchema();

        tLJSPayBSchema.setAccName(mAccName);
        tLJSPayBSchema.setBankAccNo(mBankAccNo);
        tLJSPayBSchema.setBankCode(mBankCode);
        tLJSPayBSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSPayBSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(tLJSPayBSchema,SysConst.UPDATE);

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        if(!tLCContDB.getInfo())
        {
            mErrors.addOneError("查询保单信息失败！");
        }

        tLCContSchema = tLCContDB.getSchema();
        tLCContSchemaOld = tLCContDB.getSchema();
        tLCContSchema.setAccName(mAccName);
        tLCContSchema.setBankCode(mBankCode);
        tLCContSchema.setBankAccNo(mBankAccNo);
        tLCContSchema.setPayMode(mPayMode);
        tLCContSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(tLCContSchema, SysConst.UPDATE);

        MMap mMap = createTask();

        if(mMap == null)
        {
            return false;
        }

        map.add(mMap);

        return true ;
    }

    /**
     *  生成轨迹记录。
     *  return MMap
     */
    private MMap createTask()
    {
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(tLJSPaySchema.getAppntNo());
        tLGWorkSchema.setInnerSource(tLJSPaySchema.getGetNoticeNo());
        tLGWorkSchema.setAcceptWayNo(Task.ACCEPTWAY_INNER);
        tLGWorkSchema.setStatusNo(Task.WORKSTATUS_DONE);
        tLGWorkSchema.setApplyName("自动");
        tLGWorkSchema.setTypeNo("03");

        String payModeNameOld = CommonBL.getCodeName("paymode",tLCContSchemaOld.getPayMode());
        String payModeName = CommonBL.getCodeName("paymode",tLCContSchema.getPayMode());
        if (payModeNameOld == null || payModeName == null) {
            return null;
        }

        tLGWorkSchema.setRemark("自动批注：个险给付变更付费方式，由[缴费方式"
                                + tLCContSchemaOld.getPayMode()
                                + "(" + payModeNameOld + "), "
                                + "银行编码" +
                                StrTool.cTrim(tLCContSchemaOld.getBankCode())
                                + ", 账户名" +
                                StrTool.cTrim(tLCContSchemaOld.getAccName())
                                + ", 账号" +
                                StrTool.cTrim(tLCContSchemaOld.getBankAccNo())
                                + "] 变更为 [缴费方式"
                                + tLCContSchema.getPayMode()
                                + "(" + payModeName + "), "
                                + "银行编码" + StrTool.cTrim(tLCContSchema.getBankCode())
                                + ", 账户名" + StrTool.cTrim(tLCContSchema.getAccName())
                                + ", 账号" + StrTool.cTrim(tLCContSchema.getBankAccNo())
                                + "]");

        String workBoxNo = getWorkBox();
        if (workBoxNo == null) {
            return null;
        }

        VData tVData = new VData();
        tVData.add(tLGWorkSchema);
        tVData.add(mGI);
        tVData.add(workBoxNo);

        TaskInputBL tTaskInputBL = new TaskInputBL();
        MMap tMMap = tTaskInputBL.getSubmitData(tVData, "");
        if (tMMap == null) {
            mErrors.addOneError("生成工单信息失败:\n"
                                + tTaskInputBL.mErrors.getFirstError());
            return null;
        }

        return tMMap;
    }

    /**
     * getWorkBox
     *
     * @return String
     */
    private String getWorkBox() {
        String sql = "select WorkBoxNo from LGWorkBox "
                     + "where OwnerNo = '" + mGI.Operator + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String tWorkBox = tExeSQL.getOneValue(sql);
        if (tWorkBox == null || tWorkBox.equals("") || tWorkBox.equals("null")) {
            mErrors.addOneError("没有查询到您的工单信息，不能继续处理业务");
            return null;
        }

        return tWorkBox;
    }
}
