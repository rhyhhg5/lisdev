package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.CheckData;
import com.sinosoft.utility.*;

import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.xb.XBConst;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @author Yuanaq
 * @version 1.0
 */
public class IndiDueFeeBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData saveData = new VData();
    private MMap mMMap = new MMap();
    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mOperateFlag = "1"; // 1 : 个案催收，2：批量催收
    private String mserNo = "1"; //批次号
    private String mSerGrpNo = ""; //接受外部参数
    private boolean mSubmitFlag = true; //提交标志，若需要在本类提交入库，true，否则，false
    private String mGetNoticeNo = null;  //通知书号
    private boolean hasePolNeedFee = true;

    //保单缴费方式
    private String mBankCode = "";
    private String mBankAccNo = "";
    private String mAccName = "";
    private boolean mIsXBPol = false;  //险种是否续保标志

    private Reflections ref = new Reflections();

    //个人保单表
    private LCContSchema mLCContSchema = new LCContSchema();

    //应收个人交费表
    LJSPaySchema tLJSPaySchema = new LJSPaySchema();
    //定义应收总表备份表
    LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();

    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
    //个人应收表备份表
    private LJSPayPersonBSet mLJSPayPersonBSet = new LJSPayPersonBSet();

    //应收总表交费日期
    private String mPayDate;
    //应收总表最早交费日期
    private String mStartPayDate;

    //qulq 少儿险不收费，但是不能直接到待核销状态
    private boolean NotNeedWait = true;
    //打印子表
    private LOPRTManagerSubSet tLOPRTManagerSubSet = new LOPRTManagerSubSet();
    private String mNewPayToDate = null;  //下期交至日期
    private double mSumContPrem = 0;   //本期总期交保费
    private String mPrtSeqNo = null;
    private double mAccGetMoney = Double.MIN_VALUE;  //保单投保人账户可领金额
    private String queryType = null; //保单类型，2是普通保单，3是银代少儿

    public IndiDueFeeBL() {
    }

    public static void main(String[] args) {
        IndiDueFeeBL IndiDueFeeBL1 = new IndiDueFeeBL();
        LCContSchema tLCContSchema = new LCContSchema(); // 个人保单表
        tLCContSchema.setContNo("00001346601");
        TransferData tempTransferData = new TransferData();
        tempTransferData.setNameAndValue("StartDate", "2006-01-10");
        tempTransferData.setNameAndValue("EndDate", "2007-12-19");

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "wuser";
        tGI.ManageCom = "86";
        tGI.ComCode = "86";

        VData tv = new VData();
        tv.add(tGI);
        tv.add(tLCContSchema);
        tv.add(tempTransferData);
//        IndiDueFeeBL1.submitData(tv, "INSERT");
    }

    public boolean getHasePolNeedFee()
    {
        return hasePolNeedFee;
    }

    /**
     * Yangh于20050716添加校验，判断是否做保全，dealData
     * @return boolean：通过true，否则false
     */
    public boolean checkData() {

        //关于银行转账方式的的处理，作为关键点处理
        if(mLCContSchema.getPayMode() == null)
        {
            mErrors.addOneError("保单" + mLCContSchema.getContNo()
                                + "的缴费方式为空，请确认。");
            return false;
        }
        //qulq 2007-2-7 modify
//        if (mLCContSchema.getPayMode().equals("3")
//            || mLCContSchema.getPayMode().equals("4")
//            || mLCContSchema.getPayMode().equals("5")) {
        /**
         * 针对电商已配置的续期续保保单，增加对其银行账户的校验
         */
        boolean isNeed = false;
        if(mLCContSchema.getCardFlag().equals("b")){
        	String checkSql = "select 1 from ldcode where codetype = 'DSXQ' and (code = '"+mLCContSchema.getAgentCode()+"' or code = '"+mLCContSchema.getAgentCom()+"')";
        	ExeSQL tExeSQL = new ExeSQL();
        	SSRS tSSRS = tExeSQL.execSQL(checkSql);
        	if(tSSRS!=null && tSSRS.getMaxRow()>0){
        		isNeed = "1".equals(tSSRS.GetText(1, 1));
        	}        	
        }
          if (mLCContSchema.getPayMode().equals("4")&&(!mLCContSchema.getCardFlag().equals("b")||isNeed)){
            mBankCode = mLCContSchema.getBankCode();
            mBankAccNo = mLCContSchema.getBankAccNo();
            mAccName = mLCContSchema.getAccName();

            String errMsg = "";
            boolean flag = true;
            if(mBankCode == null || mBankCode.equals(""))
            {
                errMsg += "银行代码,";
                flag = false;
            }
            if(mBankAccNo == null || mBankAccNo.equals(""))
            {
                errMsg += "银行账号,";
                flag = false;
            }
            if(mAccName == null || mAccName.equals(""))
            {
                errMsg += "账户名,";
                flag = false;
            }
            if(!flag)
            {
                mErrors.addOneError(
                    "缴费方式为银行转账，但是"
                    + errMsg.substring(0, errMsg.lastIndexOf(","))
                    + "为空，请确认。");
                return false;
            }
        }
        //20141217付费方式为医保个人账户的将医保信息记录到应收中
        if(mLCContSchema.getPayMode().equals("8"))
        {
          	mBankCode = mLCContSchema.getBankCode();
            mBankAccNo = mLCContSchema.getBankAccNo();
            mAccName = mLCContSchema.getAccName();
          	String errMsg = "";
            boolean flag = true;
            if(mBankCode == null || mBankCode.equals(""))
            {
                errMsg += "医保机构代码,";
                flag = false;
            }
            if(mBankAccNo == null || mBankAccNo.equals(""))
            {
                errMsg += "医保卡账号,";
                flag = false;
            }
            if(mAccName == null || mAccName.equals(""))
            {
                errMsg += "医保卡账户名,";
                flag = false;
            }
            if(!flag)
            {
                mErrors.addOneError(
                    "缴费方式为医保个人账户，但是"
                    + errMsg.substring(0, errMsg.lastIndexOf(","))
                    + "为空，请确认。");
                return false;
            }
          }

        return true;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public VData getSubmitData(VData cInputData, String cOperate) {

        if (!getInputData(cInputData)) {
            return null;
        }
        System.out.println("After getinputdata");
        if (!checkData()) {
            return null;
        }
        //进行业务处理
        if (!dealData()) {
            return null;
        }

        if (!createSuccessFulUrgLog()) {
            return null;
        }

        return saveData;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public MMap getSubmitMMap(VData cInputData, String cOperate) {
        getSubmitData(cInputData, cOperate);
        return mMMap;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate) {

        VData data = getSubmitData(cInputData, cOperate);
        if (data == null) {
            return false;
        }

        IndiDueFeeBLS tIndiDueFeeBLS = new IndiDueFeeBLS();
        tIndiDueFeeBLS.submitData(saveData, "INSERT");

        if (tIndiDueFeeBLS.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tIndiDueFeeBLS.mErrors);

            return false;
        }

        createSuccessFulUrgLog();

        return true;
    }

    private boolean createSuccessFulUrgLog() {
        //添加数据到催收核销日志表
        //非批次核销
        if (mSerGrpNo == null || mSerGrpNo.equals("")) {
            if (!dealUrgeLog("3", "", "UPDATE")) {
                return false;
            }
        }
        return true;
    }
    /**
        * 得到管理机构为mangeCom在时间段statrDate到endDate的contNo保单中需要续期催收的险种
        * stopflag：停交标志 0正常，1停交，null正常
        * paytodate：当期保费的交至日期
        * payenddate：保单的终交日期
        * appflag：投保单/保单标志 0投保单，1保单
        * conttype：保单类型 1个人单，2团单
        * managecom：保单所在管理机构
        * payintv：交费间隔 0趸交，1月交，3季交，6半年教，12年交
        * polstate：保单状态 02开头为解约，03开头为失效，04开头为暂停（理赔引起）、
        * LMRiskApp表中的参数含义
        * urgepayflag：催收标志 Y可催收，N不可催收
        * @param startDate String：开始时间
        * @param endDate String：结束时间
        * @param manageCom String：机构
        * @param contNo String：保单号
        * @return LCPolSet：险种集合
        */
       public LCPolSet getLCPolNeedDueFee(String startDate,
                                           String endDate,
                                           String contNo,String queryType)
       {
           StringBuffer tSBql = new StringBuffer(128);

           //20080423 zhanggm
           if("3".equals(queryType))
           {
        	   tSBql.append("select * from LCPol a where polNo in")
               .append("     (select newPolNo from LCRnewStateLog ")
               .append("      where contNo = '" + contNo + "' ")
               .append("         and state = '"
                       + XBConst.RNEWSTATE_UNHASTEN + "')");
           }
           else
           {
              tSBql.append("select * from lcpol where paytodate<payenddate")
                   .append(" and (StopFlag='0' or StopFlag is null)")
                   .append(" and PolNo not in (select PolNo from LJSPayPerson where polNo = lcpol.polNo)")
                   .append(" and paytodate>='" + startDate + "' and paytodate<='" +
                           endDate)
                   .append("' and appflag='1' and (StateFlag is null or StateFlag = '1')" +
                           " and grppolno='00000000000000000000'")
                   .append(" and ManageCom like '" + tGI.ManageCom + "%'" +
                           " and payintv>0")
                   .append(" and ContNo='" + contNo + "' ")
                   .append(" and (polstate is null or (polstate is not null and polstate not like '02%' and polstate not like '03%')) ")
                   .append("union ")
                   .append("select * from LCPol a where polNo in")
                   .append("     (select newPolNo from LCRnewStateLog ")
                   .append("      where contNo = '" + contNo + "' ")
                   .append("         and state = '"
                           + XBConst.RNEWSTATE_UNHASTEN + "')")
                   .append(" and (polstate is null or (polstate is not null and polstate not like '02%' and polstate not like '03%' ")// 续期时不对满期或终止缴费的险种抽档 modify by fuxin 2008-6-6
                   .append(" or (polstate='"+XBConst.POLSTATE_LPXBSTOP+"' and exists (select code from ldcode where codetype='bzXB' and code=a.riskcode)) )) ");//添加对保证续保产品的处理 modify by lzy 201506
           }
           LCPolSet tLCPolSet = new LCPolSet();
           LCPolDB tLCPolDB = new LCPolDB();
           tLCPolSet = tLCPolDB.executeQuery(tSBql.toString());
           System.out.println(tSBql.toString());

           if (tLCPolDB.mErrors.needDealError() == true) {
               // @@错误处理
               this.mErrors.copyAllErrors(tLCPolDB.mErrors);

               CError tError = new CError();
               tError.moduleName = "LCPolQueryBL";
               tError.functionName = "queryData";
               tError.errorMessage = "保单表查询失败!";
               this.mErrors.addOneError(tError);
               return null;
           }
           if (tLCPolSet.size() == 0) {
               CError.buildErr(this,
                               "保单号为：" + contNo
                               + "的保单已被其他业务员催收,或者该保单可能已经失效或保全终止！");
               return null;
           }

           //去除续保终止的险种
           String edorNo = (String)mTransferData.getValueByName("EdorNo");
           if(edorNo != null && !edorNo.equals("") && !edorNo.equals("null"))
           {
               LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
               tLPUWMasterDB.setEdorNo(edorNo);
               tLPUWMasterDB.setEdorType(BQ.EDORTYPE_XB);
               for(int i = 1; i <= tLCPolSet.size(); i++)
               {
                   tLPUWMasterDB.setPolNo(tLCPolSet.get(i).getPolNo());
                   if(!tLPUWMasterDB.getInfo())
                   {
                       continue;
                   }
                   //若续保终止
                   else if(tLPUWMasterDB.getPassFlag()
                           .equals(BQ.PASSFLAG_STOPXB))
                   {
                       tLCPolSet.removeRange(i, i);
                       //qulq add 东西减掉了，长度就会变，所以2就变为1了。
                       //呵呵 不然续保收费会有问题的
                       i--;
                   }
                   //若核保结论为条件通过
                   else if(tLPUWMasterDB.getPassFlag()
                           .equals(BQ.PASSFLAG_CONDITION))
                   {
                       //客户不同意且不同意处理为终止续保
                       if(tLPUWMasterDB.getCustomerReply() != null
                          && tLPUWMasterDB.getCustomerReply().equals("2")
                           && tLPUWMasterDB.getDisagreeDeal() != null
                           && tLPUWMasterDB.getDisagreeDeal().equals("3"))
                       {
                           tLCPolSet.removeRange(i, i);
                           //qulq add 东西减掉了，长度就会变，所以2就变为1了。
                           //呵呵 不然续保收费会有问题的
                           i--;
                       }
                   }
               }
               if(tLCPolSet.size() == 0)
               {
                   mErrors.addOneError("续保险种全部终止续保且无需要期的险种。");
                   hasePolNeedFee = false;
                   return null;
               }
           }
           return tLCPolSet;
    }

    /**
     * 生成应收记录
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
        VData tVData = new VData();

        //得到需要催收的险种
        String startDate = (String) mTransferData.getValueByName("StartDate");
        String endDate = (String) mTransferData.getValueByName("EndDate");
        LCPolSet tLCPolSet = getLCPolNeedDueFee(startDate, endDate,
                                                mLCContSchema.getContNo(),queryType);
        if(tLCPolSet == null)
        {
            return false;
        }

        //计算第一次转帐日和应交日
        if(!calPayDate(tLCPolSet, mLCContSchema.getPayMode()))
        {
            return false;
        }

        if(!createNo())
        {
            return false;
        }

        //循环每个险种进行处理
        for(int j = 1; j <= tLCPolSet.size(); j++)
        {
            double sumPolPrem = 0; //险种期交保费合计

            //获取合同下的每条险种的明细信息
            LCPolSchema tLCPolSchema = tLCPolSet.get(j).getSchema();
            LCPolSchema tOldLCPolSchema = tLCPolSet.get(j).getSchema(); //对应的原险种信息，若是续期则为险种本身
            mIsXBPol = isXBPol(tOldLCPolSchema.getPolNo());
            if(mIsXBPol)
            {
                tOldLCPolSchema = getOldLCPolSchema(tOldLCPolSchema);
                if(tOldLCPolSchema == null)
                {
                    return false;
                }
            }

            //得到下期交至日期
            mNewPayToDate = getNewPayToDate(tOldLCPolSchema);

            //得到险种下责任信息
            LCDutyDB tLCDutyDB = new LCDutyDB();
            tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
            LCDutySet tLCDutySet = tLCDutyDB.query();
            if(tLCDutySet.size() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "IndiDueFeeBL";
                tError.functionName = "dealData";
                tError.errorMessage = "没有查询到险种的责任信息："
                                      + tLCPolSchema.getInsuredNo() + " "
                                      + tLCPolSchema.getRiskCode();
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            for(int dutyCount = 1; dutyCount <= tLCDutySet.size(); dutyCount++)
            {
                LCDutySchema tLCDutySchema = tLCDutySet.get(dutyCount);

                //得到责任下所有缴费
                LCPremDB tLCPremDB = new LCPremDB();
                tLCPremDB.setPolNo(tLCDutySchema.getPolNo());
                tLCPremDB.setDutyCode(tLCDutySchema.getDutyCode());
                LCPremSet tLCPremSet = tLCPremDB.query();
                if(tLCPremSet.size() == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "IndiDueFeeBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "没有查询到责任的缴费项信息："
                                          + tLCPolSchema.getInsuredNo() + " "
                                          + tLCPolSchema.getRiskCode() + " "
                                          + tLCDutySchema.getDutyCode();
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                    return false;
                }

                for(int premCount = 1; premCount <= tLCPremSet.size(); premCount++)
                {
                	//医诊专家无理赔奖励校验
                	double rate = 1;
                	ExeSQL tExeSQL = new ExeSQL(); 
                	//direct by lc 2017-1-20 配置信息
                	String risksql = "select code1 ,codealias from ldcode1 where codetype='WLPYH' and code='" + tOldLCPolSchema.getRiskCode() + "'";
                	SSRS tContInfosSSRS = new SSRS();
                	 tContInfosSSRS = tExeSQL.execSQL(risksql);
                	 if(tContInfosSSRS.getMaxRow()>0){
                	 String WLPRate = tContInfosSSRS.GetText(1, 1);	  
                	 String tyears = tContInfosSSRS.GetText(1, 2);	  
                	if(!"".equals(WLPRate) && !"null".equals(WLPRate) && !"".equals(tyears) && !"null".equals(tyears))
                	{
	                	String mContInfosSQL = "select paytodate - "+tyears+" years, paytodate, year(paytodate) from lcpol where contno='" + mLCContSchema.getContNo() + "' and stateflag='1' and riskcode = '" + tOldLCPolSchema.getRiskCode() + "'";
	              //  	SSRS tContInfosSSRS = null;
	                	tContInfosSSRS = tExeSQL.execSQL(mContInfosSQL);
	                	String date = tContInfosSSRS.GetText(1, 1);	  
	                	String date2 = tContInfosSSRS.GetText(1, 2);
	                	double date3 = Double.parseDouble(tContInfosSSRS.GetText(1, 3));
	                	//契约录入时的生效日
	                	String csql ="";
	                	if (Integer.parseInt(tyears) >1){
	                		csql= "select year(cvalidate) from lbcont where contno in (select newcontno from lcrnewstatelog where contno='" + mLCContSchema.getContNo() +"' and state='6' order by paytodate fetch first 1 rows only)";
	                	}
	                	else {
	                		csql ="select year(cvalidate) from lccont where contno='" + mLCContSchema.getContNo() +"'";
	                	}
	                	String yeartest = tExeSQL.getOneValue(csql);
	                	if(!"".equals(yeartest) && !"null".equals(yeartest))
	                	{
		                	double year = Double.parseDouble(yeartest);
		                	//计算年期
		                	double count =  date3 - year;
		                	if(!"".equals(date) && !"null".equals(date)	&& !"".equals(date2) && !"null".equals(date2))
		                	{	                
		                		if(count >= Integer.parseInt(tyears) ){
		                			String sql2 = "SELECT NVL(sum(a.realpay),0) "
		                    			+ " FROM llclaimpolicy a ,llcase b "
		                    			+ " WHERE a.caseno = b.caseno"
		                    			+ " AND a.contno = '" + mLCContSchema.getContNo() + "'"
		                    			+ " AND b.EndCaseDate between '" + date + "' and '" + date2 + "'"
		                    			+ " AND b.rgtstate IN ('09','11','12')";
		                    		String LPyn = tExeSQL.getOneValue(sql2);
		                    		if("0".equals(LPyn) || "0.0".equals(LPyn) || "0.00".equals(LPyn))
		                    		{
		                    			rate = Double.parseDouble(WLPRate);
		                    		} else {
		                    			rate = 1;
		                    		}
		                		}                    		
		                	}
	                	}
                	}
                	else if (!"".equals(WLPRate) && !"null".equals(WLPRate)){
	                	String tfosSQL = "select  paytodate, year(paytodate) from lcpol where contno='" + mLCContSchema.getContNo() + "' and stateflag='1' and riskcode = '" + tOldLCPolSchema.getRiskCode() + "'";
	  	  
	  	                	tContInfosSSRS = tExeSQL.execSQL(tfosSQL);
	  	                	String date = tContInfosSSRS.GetText(1, 1);	  
	  	        
                		String sql2 = "SELECT NVL(sum(a.realpay),0) "
                			+ " FROM llclaimpolicy a ,llcase b "
                			+ " WHERE a.caseno = b.caseno"
                			+ " AND a.contno = '" + mLCContSchema.getContNo() + "'"
                			+ " AND b.EndCaseDate <='"  + date + "'"
                			+ " AND b.rgtstate IN ('09','11','12')";
                		String LPyn = tExeSQL.getOneValue(sql2);
                		if("0".equals(LPyn) || "0.0".equals(LPyn) || "0.00".equals(LPyn))
                		{
                			rate = Double.parseDouble(WLPRate);
                		} else {
                			rate = 1;
                		}
                		
                	  }
                	 }
                	 else {
                		 rate = 1;
                	 }
                    LCPremSchema tLCPremSchema = tLCPremSet.get(premCount); //保费项纪录
                    double prem = tLCPremSchema.getPrem()
                                  * (1 - tLCDutySchema.getFreeRate()) * rate;

                    setOneLJSPayPerson(tOldLCPolSchema, tLCPolSchema,
                                       tLCPremSchema, prem, "ZC");
                    sumPolPrem += prem;
                
                }
            }

            //保单期交保费合计
            mSumContPrem += sumPolPrem;

            //生成险种打印信息
            setLOPrtManagerSub(tLCPolSchema, sumPolPrem);
        }

        
        if(!setLJSPay())
        {
            return false;
        }

        VData prtData = getPrintData(mLCContSchema, tLJSPaySchema);
        if (prtData == null)
        {
            CError tError = new CError();
            tError.moduleName = "AssignBonus";
            tError.functionName = "getByCash";
            tError.errorMessage = "打印数据准备失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        LOPRTManagerSchema tLOPRTManagerSchema
            = (LOPRTManagerSchema) prtData.getObjectByObjectName(
                "LOPRTManagerSchema", 0);

        tVData.clear();
        tVData.add(tLJSPaySchema);
        mMMap.put(tLJSPaySchema, "DELETE&INSERT");
        tVData.add(mLJSPayPersonSet);

        String sql = "delete from LJSPayPerson where GetNoticeNo = '"
                     + tLJSPaySchema.getGetNoticeNo() + "' ";
        mMMap.put(sql, SysConst.DELETE);
        mMMap.put(mLJSPayPersonSet, SysConst.INSERT);
        tVData.add(tLJSPayBSchema);
        mMMap.put(tLJSPayBSchema, "DELETE&INSERT");

        sql = "delete from LJSPayPersonB where GetNoticeNo = '"
              + tLJSPaySchema.getGetNoticeNo() + "' ";
        mMMap.put(sql, SysConst.DELETE);
        tVData.add(mLJSPayPersonBSet);
        mMMap.put(mLJSPayPersonBSet, SysConst.INSERT);

        sql = "delete from LOPRTManager where StandbyFlag2 = '"
              + tLJSPayBSchema.getGetNoticeNo() + "' ";
        mMMap.put(sql, SysConst.DELETE);
        tVData.add(tLOPRTManagerSchema);
        mMMap.put(tLOPRTManagerSchema, "DELETE&INSERT");

        sql = "delete from LOPRTManagerSub where GetNoticeNo = '"
              + tLJSPayBSchema.getGetNoticeNo() + "' ";
        mMMap.put(sql, SysConst.DELETE);
        tVData.add(tLOPRTManagerSubSet);
        mMMap.put(tLOPRTManagerSubSet, "DELETE&INSERT");

        //续保状态变更为已催收
        sql = "  update LCRnewStateLog "
                     + "set state = '" + XBConst.RNEWSTATE_UNCONFIRM + "', "
                     + "   operator = '" + tGI.Operator + "', "
                     + "   modifyDate = '" + CurrentDate + "' , "
                     + "   modifyTime = '" + CurrentTime + "' "
                     + "where contNo = '" + mLCContSchema.getContNo() + "' "
                     + "   and state = '" + XBConst.RNEWSTATE_UNHASTEN + "' ";
        mMMap.put(sql, "UPDATE");

        //若不需要缴费，则可直接核销 modify by qulq 少儿险续保不能直接核销
        if((mAccGetMoney >= mSumContPrem)&&NotNeedWait)
        {
            String table = "LJSPayB";
            sql = "update " + table + " "
                  + "set dealState = '"
                  + FeeConst.DEALSTATE_WAITCONFIRM + "' "
                  + "where getNoticeNo = '" + mGetNoticeNo + "' ";
            mMMap.put(sql, SysConst.UPDATE);
            mMMap.put(sql.replaceFirst(table, "LJSPayPersonB"), SysConst.UPDATE);
        }

        String autoVerifyFlag = "N";
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("AutoVerifyStation");

        if (tLDSysVarDB.getInfo() == true) {
            //如果系统变量为催收时核销并且应收总表应收款=0，那么催收时核销
            //AutoVerifyStation 自动核销的位置--即续期交费时，保单余额足够支付保费且选择为抵交续期保费时的自动核销
            //1 财务核销时处理 2 催收时处理
            if (tLDSysVarDB.getSysVarValue().equals("2") &&
                (tLJSPaySchema.getSumDuePayMoney() == 0)) {
                autoVerifyFlag = "Y";
            }
        }

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("autoVerifyFlag", autoVerifyFlag);

        tVData.add(tTransferData);
        saveData = tVData;

        return true;
    }

    /**
     * 生成总应收数据
     * @return boolean：成功true，否则false
     */
    private boolean setLJSPay()
    {
        double accMoneyUsed = Double.MIN_VALUE;  //使用的余额

        AppAcc tAppAcc = new AppAcc();
        LCAppAccSchema tLCAppAccSchema
            = tAppAcc.getLCAppAcc(mLCContSchema.getAppntNo());

//少儿险不应当从投保人帐户中取出余额抵充续保保费 add by xp 090526 NotNeedWait为false表示为少儿险
        if((tLCAppAccSchema != null)&&NotNeedWait)
        {
            mAccGetMoney = tLCAppAccSchema.getAccGetMoney();
        }

        //若账户无余额，则本次续期续保应收保费为新保单期交保费
        if(mAccGetMoney - 0 < 0.00001)
        {
            tLJSPaySchema.setSumDuePayMoney(mSumContPrem);
        }
        //账户余额足够抵扣应收保费
        else if(mAccGetMoney >= mSumContPrem)
        {
            tLJSPaySchema.setSumDuePayMoney(0);
            accMoneyUsed = mSumContPrem;
        }
        //需要收取部分保费
        else
        {
            tLJSPaySchema.setSumDuePayMoney(mSumContPrem - mAccGetMoney);
            accMoneyUsed = mAccGetMoney;
        }

        if(tLCAppAccSchema != null && accMoneyUsed != Double.MIN_VALUE)
        {
            MMap tMMap = dealAccBala(tLCAppAccSchema, accMoneyUsed);
            if(tMMap!=null){
            	mMMap.add(tMMap);
            }

            //生成余额抵扣财物记录
            LJSPayPersonSchema tLJSPayPersonSchema
                = mLJSPayPersonSet.get(1).getSchema();
            tLJSPayPersonSchema.setSumActuPayMoney(-accMoneyUsed);
            tLJSPayPersonSchema
                .setSumDuePayMoney(tLJSPayPersonSchema.getSumActuPayMoney());
            tLJSPayPersonSchema.setPayType("YEL");
            mLJSPayPersonSet.add(tLJSPayPersonSchema);

            LJSPayPersonBSchema tLJSPayPersonBSchema = new LJSPayPersonBSchema();
            ref.transFields(tLJSPayPersonBSchema, tLJSPayPersonSchema);
            tLJSPayPersonBSchema.setDealState(FeeConst.DEALSTATE_WAITCHARGE);
            mLJSPayPersonBSet.add(tLJSPayPersonBSchema);
        }

        tLJSPaySchema.setGetNoticeNo(mGetNoticeNo); //通知书号
        tLJSPaySchema.setOtherNo(mLCContSchema.getContNo());
        tLJSPaySchema.setOtherNoType("2");
        tLJSPaySchema.setAppntNo(mLCContSchema.getAppntNo());
        tLJSPaySchema.setPayDate(mPayDate);
        tLJSPaySchema.setStartPayDate(mStartPayDate); //交费最早应缴日期保存上次交至日期
        tLJSPaySchema.setBankOnTheWayFlag("0");
        tLJSPaySchema.setBankSuccFlag("0");
        tLJSPaySchema.setSendBankCount(0); //送银行次数
        tLJSPaySchema.setApproveCode(mLCContSchema.getApproveCode());
        tLJSPaySchema.setApproveDate(mLCContSchema.getApproveDate());
        tLJSPaySchema.setRiskCode("000000");
        tLJSPaySchema.setBankAccNo(mBankAccNo);
        tLJSPaySchema.setBankCode(mBankCode);
        tLJSPaySchema.setSendBankCount(0);
        tLJSPaySchema.setSerialNo(mserNo); //流水号
        tLJSPaySchema.setOperator(tGI.Operator);
        tLJSPaySchema.setManageCom(mLCContSchema.getManageCom());
        tLJSPaySchema.setAgentCom(mLCContSchema.getAgentCom());
        tLJSPaySchema.setAgentCode(mLCContSchema.getAgentCode());
        tLJSPaySchema.setAgentType(mLCContSchema.getAgentType());
        tLJSPaySchema.setAgentGroup(mLCContSchema.getAgentGroup());
        tLJSPaySchema.setAccName(mAccName);
        tLJSPaySchema.setMakeDate(CurrentDate);
        tLJSPaySchema.setMakeTime(CurrentTime);
        tLJSPaySchema.setModifyDate(CurrentDate);
        tLJSPaySchema.setModifyTime(CurrentTime);
        
        String sql = "select LF_SaleChnl('1', '"+ mLCContSchema.getContNo()+"'),'1' from dual where 1=1";
        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow()>0)
        {
        	String tSaleChnl = tSSRS.GetText(1, 1);
            String tMarketType = tSSRS.GetText(1, 2);
            tLJSPaySchema.setSaleChnl(tSaleChnl);
            tLJSPaySchema.setMarketType(tMarketType);
        }

        //添加应收总表备份表
        ref.transFields(tLJSPayBSchema, tLJSPaySchema);
        //若不需要缴费则为待核销状态
        tLJSPayBSchema.setDealState(FeeConst.DEALSTATE_WAITCHARGE);

        return true;
    }

    /**
     * 生成相关流水号
     * @return boolean：成功true，否则false
     */
    private boolean createNo()
    {
        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());

        String serMultNo = (String) mTransferData.getValueByName("serNo");
        mSerGrpNo = serMultNo;

        if(serMultNo == null || serMultNo.equals(""))
        {
            //生成流水号
            mserNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
            mOperateFlag = "1";

            //若为个案催收，添加催收核销日志表
            if(!dealUrgeLog("1", "", "INSERT"))
            {
                return false;
            }
        }
        else
        {
            mserNo = serMultNo;
            mOperateFlag = "2";
        }

        //产生通知书号
        if(mGetNoticeNo == null || mGetNoticeNo.equals(""))
        {
            mGetNoticeNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);
        }

        //生成打印管理流水号
        mPrtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);

        return true;
    }

    /**
     * 生成险种打印信息
     * @param tLCPolSchema LCPolSchema：险种信息
     * @param sumPolPrem double：险种收费
     */
    private void setLOPrtManagerSub(LCPolSchema tLCPolSchema,
                                    double sumPolPrem)
    {
        //杨红添加开始
        if(tLOPRTManagerSubSet == null || tLOPRTManagerSubSet.size() == 0)
        {
            LOPRTManagerSubSchema tLOPRTManagerSubSchema = new
                LOPRTManagerSubSchema();
            tLOPRTManagerSubSchema.setPrtSeq(mPrtSeqNo);
            tLOPRTManagerSubSchema.setGetNoticeNo(mGetNoticeNo);
            tLOPRTManagerSubSchema.setOtherNo(mLCContSchema.getContNo());
            tLOPRTManagerSubSchema.setOtherNoType("00");
            tLOPRTManagerSubSchema.setRiskCode(tLCPolSchema.getRiskCode());
            tLOPRTManagerSubSchema.setDuePayMoney(sumPolPrem);
            tLOPRTManagerSubSchema.setAppntName(mLCContSchema.getAppntName());
            tLOPRTManagerSubSchema.setTypeFlag("1");
            tLOPRTManagerSubSet.add(tLOPRTManagerSubSchema);
        }
        else
        {
            LOPRTManagerSubSchema tLOPRTManagerSubSchema = new
                LOPRTManagerSubSchema();
            boolean isRepeatFlag = false;
            for(int m = 1; m <= tLOPRTManagerSubSet.size(); m++)
            {
                if(tLOPRTManagerSubSet.get(m).getRiskCode().equals(
                    tLCPolSchema.getRiskCode()))
                {
                    isRepeatFlag = true;
                    tLOPRTManagerSubSet.get(m).setDuePayMoney(
                        tLOPRTManagerSubSet.get(m).getDuePayMoney() +
                        sumPolPrem);
                    break;
                }
            }
            if(!isRepeatFlag)
            {
                tLOPRTManagerSubSchema.setPrtSeq(mPrtSeqNo);
                tLOPRTManagerSubSchema.setGetNoticeNo(mGetNoticeNo);
                tLOPRTManagerSubSchema.setOtherNo(mLCContSchema.getContNo());
                tLOPRTManagerSubSchema.setOtherNoType("00");
                tLOPRTManagerSubSchema.setRiskCode(tLCPolSchema.getRiskCode());
                tLOPRTManagerSubSchema.setDuePayMoney(sumPolPrem);
                tLOPRTManagerSubSchema.setAppntName(mLCContSchema.
                    getAppntName());
                tLOPRTManagerSubSchema.setTypeFlag("1");
                tLOPRTManagerSubSet.add(tLOPRTManagerSubSchema);
            }
        }
    }

    /**
     * 计算险种的新交至日期
     * @param tLCPolSchema LCPolSchema：原险种信息
     * @return String
     */
    private String getNewPayToDate(LCPolSchema tLCPolSchema)
    {
        System.out.println("原交至日期" + tLCPolSchema.getPaytoDate());

        FDate tD = new FDate();
        Date newBaseDate = new Date();
        Date NewPayToDate;  //计算得到的交至日期

        //获取保单现在的交至日
        String CurrentPayToDate = tLCPolSchema.getPaytoDate();
        //获取当前险种的交费间隔
        int PayInterval = tLCPolSchema.getPayIntv();

        try
        {
            newBaseDate = tD.getDate(CurrentPayToDate);
            //20161125月交的 应该参考保单的生效日期
            //20170519公共方法修改对其他模块有影响，使用新的方法
            NewPayToDate = PubFun.calOFDate(newBaseDate, PayInterval, "M", tD.getDate(tLCPolSchema.getCValiDate()));
            //20171212续保险种，并且缴费频次为趸交的保单，续保时按12个月处理
            if(mIsXBPol && PayInterval==0)
            {
            	PayInterval=12;
                NewPayToDate = PubFun.calOFDate(newBaseDate, PayInterval, "M", tD.getDate(tLCPolSchema.getCValiDate()));
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "IndiDueFeeBL";
            tError.functionName = "getNewPayToDate";
            tError.errorMessage = "险种交至日期由问题"
                                  + tLCPolSchema.getPaytoDate();
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        String strNewPayToDate = tD.getString(NewPayToDate);

        System.out.println("新交至日期" + strNewPayToDate);

        return strNewPayToDate;
    }

    /**
     * 生成个人应收记录
     * @param tOldLCPolSchema LCPolSchema：原险种信息，若为续保，则是续保全险种信息
     * @param tLCPolSchema LCPolSchema：需生成应收记录的险种信息，若为续保则是保费重算后的数据
     * @param tLCPremSchema LCPremSchema：缴费项数据
     * @param money double：应缴费
     * @param payType String：缴费类型
     */
    private void setOneLJSPayPerson(
        LCPolSchema tOldLCPolSchema,
        LCPolSchema tLCPolSchema,
        LCPremSchema tLCPremSchema,
        double money,
        String payType)
    {
        LJSPayPersonSchema schema = new LJSPayPersonSchema();

        schema.setPolNo(tOldLCPolSchema.getPolNo());
        schema.setSumDuePayMoney(money);
        schema.setSumActuPayMoney(money);
        schema.setPayCount(tLCPremSchema.getPayTimes() + 1);
        schema.setGrpContNo(tOldLCPolSchema.getGrpContNo());
        schema.setGrpPolNo(tOldLCPolSchema.getGrpPolNo());
        schema.setContNo(tOldLCPolSchema.getContNo());
        schema.setDutyCode(tLCPremSchema.getDutyCode());
        schema.setPayPlanCode(tLCPremSchema.
                                             getPayPlanCode());
        schema.setGetNoticeNo(mGetNoticeNo); // 通知书号
        schema.setPayAimClass("1"); //交费目的分类=个人
        schema.setPayIntv(tOldLCPolSchema.getPayIntv());

        //:交费日期=个人保单表的交至日期
        schema.setPayDate(mPayDate);
        schema.setPayType(payType); //交费类型=yel
        schema.setAppntNo(mLCContSchema.getAppntNo());

        //:这条纪录的//原交至日期=个人保单表的交至日期
        //现交至日期CurPayToDate=个人保单表的交至日期+交费间隔 待修改
        schema.setLastPayToDate(tLCPolSchema.getPaytoDate());
        schema.setCurPayToDate(mNewPayToDate);
        schema.setBankCode(mBankCode);
        schema.setBankAccNo(mBankAccNo);
        schema.setBankOnTheWayFlag("0");
        schema.setBankSuccFlag("0");
        schema.setPayTypeFlag(mIsXBPol ? "1" : "0");
        schema.setApproveCode(tLCPolSchema.getApproveCode());
        schema.setApproveDate(tLCPolSchema.getApproveDate());
        schema.setManageCom(tLCPolSchema.getManageCom());
        schema.setAgentCom(tLCPolSchema.getAgentCom());
        schema.setAgentType(tLCPolSchema.getAgentType());
        schema.setRiskCode(tLCPolSchema.getRiskCode());
        schema.setSerialNo(mserNo); //统一一个流水号
        schema.setOperator(tGI.Operator);
        schema.setMakeDate(CurrentDate); //入机日期
        schema.setMakeTime(CurrentTime); //入机时间
        schema.setModifyDate(CurrentDate); //最后一次修改日期
        schema.setModifyTime(CurrentTime); //最后一次修改时间
        schema.setAgentCode(tLCPolSchema.getAgentCode());
        schema.setAgentGroup(tLCPolSchema.getAgentGroup());
        mLJSPayPersonSet.add(schema);

        //生成B表信息
        LJSPayPersonBSchema schemaB = new LJSPayPersonBSchema();
        ref.transFields(schemaB, schema);
        schemaB.setDealState(FeeConst.DEALSTATE_WAITCHARGE);
        mLJSPayPersonBSet.add(schemaB);
    }

    /**
     * 计算缴费日期
     * @return boolean
     */
    private boolean calPayDate(LCPolSet tLCPolSetXQ, String payMode)
    {
        CalPayDateBL bl = new CalPayDateBL();
        HashMap dates = bl.calPayDateP(tLCPolSetXQ, payMode);
        if(dates == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        mStartPayDate = (String) dates.get(CalPayDateBL.STARTPAYDATE);
        mPayDate = (String) dates.get(CalPayDateBL.PAYDATE);
        if(mStartPayDate == null || mStartPayDate.equals("")
           || mPayDate == null || mPayDate.equals(""))
        {
            mErrors.addOneError("计算失败，没有得到缴费日期。");
            return false;
        }

        String str = mPayDate;

        //Date String 转换类
        FDate tFDate =new FDate();

        //构造GregorianCalendar对象
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(tFDate.getDate(mPayDate));
        int week = gc.get(Calendar.DAY_OF_WEEK);
        if(week == Calendar.SATURDAY){
                //如果是周六 就把当前日期+2天 如遇到月底的情况  自动顺延到下月
                gc.add(Calendar.DATE,2);
                //格式化日期
                str = tFDate.getString(gc.getTime());
            }else if(week == Calendar.SUNDAY){
                    //如果是周日 就把当前日期+1天 如遇到月底的情况  自动顺延到下月
                    gc.add(Calendar.DATE,1);
                    str = tFDate.getString(gc.getTime());
                }
        //更改后的日期给mPayDate
        mPayDate = str ;
        //qulq 有些险种不需要缴费，但是为了处理方便生成了应收。但是不能被财务提取
        //所以处理成了一个超大日期
        for(int i=1;i<=tLCPolSetXQ.size();i++)
        {
            if ("320106".equals(tLCPolSetXQ.get(i).getRiskCode())||"120706".equals(tLCPolSetXQ.get(i).getRiskCode()))
            {
                mStartPayDate = "3000-1-1";
                mPayDate = "3000-1-1";
                NotNeedWait = false;
                break;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData)
    {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLCContSchema = ((LCContSchema) mInputData.getObjectByObjectName(
            "LCContSchema",
            0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
            "TransferData", 0); //传输页面输入的起止时间
        //20080423 zhanggm "3"少儿单,"2"普通单
        queryType = (String)mInputData.getObject(4);
        if(queryType == null)
        {
        	queryType="2";
        }
        if((tGI == null) || (mLCContSchema == null) || mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLCContSchema.getContNo());
        if(tLCContDB.getInfo() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;

        }
        mLCContSchema.setSchema(tLCContDB.getSchema());
        String submitFlag = (String) mTransferData.getValueByName("Submit");
        if(submitFlag == null)
        {
            mSubmitFlag = true;
        }
        else
        {
            mSubmitFlag = false;
        }

        mGetNoticeNo = (String) mTransferData
                       .getValueByName(FeeConst.GETNOTICENO);

        return true;
    }

    /**
     * 外部使用的公共函数
     * @param cInputData （含LCPolSchema且其保单号属性不为空，GlobalInput）
     * @return VData(含LJSPaySchema,LJSPayPersonSet)
     */
    public VData ReturnData(VData cInputData) {
        if (!getInputData(cInputData)) {
            return null;
        }

        if (!dealData()) {
            return null;
        }

        return saveData;
    }

    /**
     * 准备打印数据
     * @param tLCPolSchema：保单信息
     * @param tLJSPaySchema：财务应收信息
     * @return VData: 处理完的数据
     */
    public VData getPrintData(LCContSchema tLCContchema,
                              LJSPaySchema tLJSPaySchema) {
        VData tVData = new VData();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();

        tLOPRTManagerSchema.setPrtSeq(mPrtSeqNo);
        tLOPRTManagerSchema.setOtherNo(tLCContchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType("00");
        tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPAY);
        tLOPRTManagerSchema.setManageCom(tLCContchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(tLCContchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(tLCContchema.getManageCom());
        tLOPRTManagerSchema.setReqOperator(tLCContchema.getOperator());
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag("0");
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        tLOPRTManagerSchema.setOldPrtSeq(mPrtSeqNo);
        tLOPRTManagerSchema.setStandbyFlag1(tLCContchema.getInsuredName());
        tLOPRTManagerSchema.setStandbyFlag2(tLJSPaySchema.getGetNoticeNo());
        tLOPRTManagerSchema.setPatchFlag("0");

        tVData.add(tLOPRTManagerSchema);

        return tVData;
    }

    /**
     * 加入到催收核销日志表数据
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealUrgeLog(String pmDealState, String pmReason,
                                String pmOpreat) {

        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                LCUrgeVerifyLogSchema();
        //加到催收核销日志表
        if (pmOpreat.equals("INSERT")) {
            tLCUrgeVerifyLogSchema.setSerialNo(mserNo);
            tLCUrgeVerifyLogSchema.setRiskFlag("2");
            tLCUrgeVerifyLogSchema.setOperateType("1"); //1：续期催收操作,2：续期核销操作
            tLCUrgeVerifyLogSchema.setOperateFlag(mOperateFlag); //1：个案操作,2：批次操作
            tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
            tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成

            tLCUrgeVerifyLogSchema.setContNo(mLCContSchema.getContNo());
            tLCUrgeVerifyLogSchema.setMakeDate(CurrentDate);
            tLCUrgeVerifyLogSchema.setMakeTime(CurrentTime);
            tLCUrgeVerifyLogSchema.setModifyDate(CurrentDate);
            tLCUrgeVerifyLogSchema.setModifyTime(CurrentTime);
            tLCUrgeVerifyLogSchema.setErrReason(pmReason);
        } else {
            LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new
                    LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mserNo);
            tLCUrgeVerifyLogDB.setOperateType("1");
            tLCUrgeVerifyLogDB.setOperateFlag(mOperateFlag);
            tLCUrgeVerifyLogDB.setRiskFlag("2");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
            if (!(tLCUrgeVerifyLogSet == null) &&
                tLCUrgeVerifyLogSet.size() > 0) {
                tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                tLCUrgeVerifyLogSchema.setDealState(pmDealState);
                tLCUrgeVerifyLogSchema.setErrReason(pmReason);
            } else {
                return false;
            }
        }

        //若不需要立即提交，则存储到全局变量
        if(!mSubmitFlag && pmDealState != "1")
        {
            saveData.add(tLCUrgeVerifyLogSchema);
            mMMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
            return true;
        }
        else
        {
            MMap tMap = new MMap();
            tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);

            VData tInputData = new VData();
            tInputData.add(tMap);

            PubSubmit tPubSubmit = new PubSubmit();
            if(tPubSubmit.submitData(tInputData, "") == false)
            {
                this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
                return false;
            }
        }

        return true;
    }

    /**
     * 处理投保人帐户信息
     * @param tLCAppAccSchema LCAppAccSchema：投保人帐户
     * @param accMoneyUsed double：本次使用的账户余额
     * @return MMap：处理后的数据集合
     */
    private MMap dealAccBala(LCAppAccSchema tLCAppAccSchema,
                             double accMoneyUsed)
    {
        LCAppAccTraceSchema tLCAppAccTraceShema = new LCAppAccTraceSchema();
        tLCAppAccTraceShema.setCustomerNo(mLCContSchema.getAppntNo());
        tLCAppAccTraceShema.setAccType("0");
        tLCAppAccTraceShema.setOtherNo(mLCContSchema.getContNo());
        tLCAppAccTraceShema.setBakNo(mGetNoticeNo);
        tLCAppAccTraceShema.setOtherType("2");
        tLCAppAccTraceShema.setDestSource("02");
        tLCAppAccTraceShema.setMoney( -accMoneyUsed);
        tLCAppAccTraceShema.setOperator(tGI.Operator);

        AppAcc tAppAcc = new AppAcc();
        MMap tMap = tAppAcc.accTakeOutXQ(tLCAppAccTraceShema);

        if(tMap!=null){
        LCAppAccTraceSchema traceSchema
            = (LCAppAccTraceSchema) tMap
              .getObjectByObjectName("LCAppAccTraceSchema", 0);
        traceSchema.setState("0");
        traceSchema.setAccBala(tLCAppAccSchema.getAccBala());
        traceSchema.setBakNo(mGetNoticeNo);

        //账户主表
        tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney()
            - accMoneyUsed);
        tLCAppAccSchema.setOperator(tGI.Operator);
        tLCAppAccSchema.setModifyDate(CurrentDate);
        tLCAppAccSchema.setModifyTime(CurrentTime);

        MMap tMMap = new MMap();
        tMMap.put(tLCAppAccSchema, SysConst.UPDATE);
        tMMap.put(traceSchema, SysConst.INSERT);

        return tMMap;
        }else{
        	return null;
        }
    }

    /**
    * 判断险种是否续保险种
    * @param polNo String
    * @return boolean
    */
   private boolean isXBPol(String polNo)
   {
       LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
       tLCRnewStateLogDB.setNewPolNo(polNo);
       tLCRnewStateLogDB.setState(XBConst.RNEWSTATE_UNHASTEN);

       return (tLCRnewStateLogDB.query().size() > 0) ? true : false;
   }

   /**
    * 根据临时险种查询原险种信息
    * @param schema LCPolSchema：临时险种信息
    * @return LCPolSchema：原险种信息
    */
   private LCPolSchema getOldLCPolSchema(LCPolSchema schema)
   {
       StringBuffer sql = new StringBuffer();
       sql.append("select a.* ")
           .append("from LCPol a, LCRnewStateLog b ")
           .append("where a.polNo = b.polNo ")
           .append("   and b.newPolNo = '")
           .append(schema.getPolNo())
           .append("' ");
       LCPolSet set = new LCPolDB().executeQuery(sql.toString());
       if(set == null)
       {
           mErrors.addOneError("没有查询到续保临时险种" + schema.getRiskCode()
               + "的原险种信息");
           return null;
       }
       return set.get(1);
   }
}
