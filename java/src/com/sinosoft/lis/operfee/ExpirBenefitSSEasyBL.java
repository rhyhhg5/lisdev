package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.LJSGetSchema;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSGetDrawSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.vschema.LMDutyGetAliveSet;
import java.util.*;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.db.LJSGetDB;
import com.sinosoft.lis.db.LMDutyGetAliveDB;
import com.sinosoft.lis.db.LJSGetDrawDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContStateSchema;
import com.sinosoft.lis.schema.LMDutyGetAliveSchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.utility.SysConst;

//程序名称：ExpirBenefitSSEasyBL.java
//程序功能：常无忧B简易试算
//创建日期：2010-08-25
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class ExpirBenefitSSEasyBL {

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    
    private String mFormula = null;
    
    private String mRateFlag = null;
    
    
    private String mCValiDate = null;
    
    private String mEndDate1 = null;
    
    private String mInsuredAppAge = null;
    
    private String mInsuYear = null;
    
    private String mInformation="";
    
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    
    private String mInitMoney = null;
    
    private String mAddMoney = null;
    
    private String mAmnt = null;
    
    private String mI = null;
    
    public void setLCPol(LCPolSchema tLCPolSchema)
    {
    	mLCPolSchema.setSchema(tLCPolSchema);
    }
    
    public void setFormula(String tFormula)
    {
    	mFormula = tFormula;
    }
    
    public void setRateFlag(String tRateFlag)
    {
    	mRateFlag = tRateFlag;
    }

    public ExpirBenefitSSEasyBL() {
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate) 
    {
    	System.out.println("in ExpirBenefitSSEasyBL----------------");

    	if (!getInputData(cInputData)) 
        {
            return false;
        }
        System.out.println("After ExpirBenefitSSEasyBL getInputData");
        
        //进行业务处理
        if (!dealData()) 
        {
            return false;
        }
        System.out.println("After ExpirBenefitSSEasyBL dealData");
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) 
    {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0); 
        
        if ((tGI == null) || mTransferData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        
        mFormula = (String)mTransferData.getValueByName("formula");
        
        mRateFlag = (String)mTransferData.getValueByName("rateflag");
          
        mAmnt = (String)mTransferData.getValueByName("Amnt");
        
        mCValiDate = (String)mTransferData.getValueByName("CValiDate");
        
        mEndDate1 = (String)mTransferData.getValueByName("EndDate1");
        
        mInsuredAppAge = (String)mTransferData.getValueByName("InsuredAppAge");
        
        mInsuYear = (String)mTransferData.getValueByName("InsuYear");
        
        if (mFormula == null || mFormula == "" || mFormula.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mFormula，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if(mFormula.equals("1"))
        {
          mFormula="A";
        }
         if(mFormula.equals("2"))
        {
          mFormula="B";
        }
         if(mFormula.equals("3"))
        {
          mFormula="C";
        }
         if(mFormula.equals("4"))
        {
          mFormula="D";
        }
         if(mFormula.equals("5"))
        {
          mFormula="E";
        }
        
        if (mRateFlag == null || mRateFlag == "" || mRateFlag.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mRateFlag，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mAmnt == null || mAmnt == "" || mAmnt.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mAmnt，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mCValiDate == null || mCValiDate == "" || mCValiDate.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mCValiDate，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mEndDate1 == null || mEndDate1 == "" || mEndDate1.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mEndDate1，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mInsuredAppAge == null || mInsuredAppAge == "" || mInsuredAppAge.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mInsuredAppAge，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mInsuYear == null || mInsuYear == "" || mInsuYear.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mInsuYear，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }

    	mLCPolSchema.setAmnt(mAmnt);
    	mLCPolSchema.setCValiDate(mCValiDate);
    	mLCPolSchema.setEndDate(mEndDate1);
    	mLCPolSchema.setInsuYear(mInsuYear);
    	mLCPolSchema.setInsuredAppAge(mInsuredAppAge);       
        return true;
    }

    /**
     * 业务处理
     */
    private boolean dealData() 
    {
    	Cal330501BonusEasy t = new Cal330501BonusEasy(mLCPolSchema);
   	    HashMap hashMap3 = t.getBonus(mFormula,mRateFlag); //忠诚奖
   	    mInitMoney = (String) hashMap3.get("InitMoney");
   	    mAddMoney = (String) hashMap3.get("AddMoney");
   	    mAmnt = (String) hashMap3.get("Amnt");
   	    mI = (String) hashMap3.get("I");
        
        String [][] tRateGrid = (String[][]) hashMap3.get("RateGrid");
        System.out.println(String.valueOf(tRateGrid.length));
        for(int i=0;i< tRateGrid.length;i++)
        {
        	if(i==tRateGrid.length-1)
        	{
        		mInformation += tRateGrid[i][0]+"|"+tRateGrid[i][1]+"|"+tRateGrid[i][2]+"|"+tRateGrid[i][3]+"|"+tRateGrid[i][4];
        	}
        	else
        	{
        		mInformation += tRateGrid[i][0]+"|"+tRateGrid[i][1]+"|"+tRateGrid[i][2]+"|"+tRateGrid[i][3]+"|"+tRateGrid[i][4]+"|";
        	}
        }
   	    return true;
    }
    
    public String getInformation()
    {
    	return mInformation;
    }
    
//  1000.00,CommonBL.stringToDate("2007-7-15"),3,40
    public static void main(String arg[])
    {
    	
    	double aAmnt = 10000.0;
        String aCValiDate = "2007-8-2";
        int aInsuYear = 3; 
        String aEndDate = PubFun.calDate(aCValiDate, aInsuYear, "Y", "");
        int aInsuredAppAge = 50;
        LCPolSchema tLCPolSchema = new LCPolSchema();
    	tLCPolSchema.setAmnt(aAmnt);
    	tLCPolSchema.setCValiDate(aCValiDate);
    	tLCPolSchema.setEndDate(aEndDate);
    	tLCPolSchema.setInsuYear(aInsuYear);
    	tLCPolSchema.setInsuredAppAge(aInsuredAppAge);
    	
    	ExpirBenefitSSEasyBL d = new ExpirBenefitSSEasyBL();
    	d.setLCPol(tLCPolSchema);
    	d.setFormula("C");
    	d.setRateFlag("N");
    	d.dealData();
    }

	public String getAddMoney() 
	{
		return mAddMoney;
	}

	public String getAmnt() 
	{
		return mAmnt;
	}

	public String getI() 
	{
		return mI;
	}

	public String getInitMoney() 
	{
		return mInitMoney;
	}

}
