package com.sinosoft.lis.operfee;

import java.math.BigDecimal;

import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBContPlanDB;
import com.sinosoft.lis.db.LBContPlanDutyParamDB;
import com.sinosoft.lis.db.LBContPlanFactoryDB;
import com.sinosoft.lis.db.LBContPlanParamDB;
import com.sinosoft.lis.db.LBContPlanRiskDB;
import com.sinosoft.lis.db.LBDutyDB;
import com.sinosoft.lis.db.LBGetDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.lis.db.LBGrpPolDB;
import com.sinosoft.lis.db.LBInsureAccClassDB;
import com.sinosoft.lis.db.LBInsureAccDB;
import com.sinosoft.lis.db.LBInsureAccTraceDB;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LBPremDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContPlanDB;
import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCContPlanFactoryDB;
import com.sinosoft.lis.db.LCContPlanParamDB;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpContStateDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCRnewStateLogDB;
import com.sinosoft.lis.db.LCUrgeVerifyLogDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LBContPlanFactorySchema;
import com.sinosoft.lis.schema.LBContPlanParamSchema;
import com.sinosoft.lis.schema.LBContPlanRiskSchema;
import com.sinosoft.lis.schema.LBContPlanSchema;
import com.sinosoft.lis.schema.LBRnewStateLogSchema;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanFactorySchema;
import com.sinosoft.lis.schema.LCContPlanParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCInsureAccTraceSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LCRnewStateLogSchema;
import com.sinosoft.lis.schema.LCUrgeVerifyLogSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.vschema.LBContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LBContPlanFactorySet;
import com.sinosoft.lis.vschema.LBContPlanParamSet;
import com.sinosoft.lis.vschema.LBContPlanRiskSet;
import com.sinosoft.lis.vschema.LBContPlanSet;
import com.sinosoft.lis.vschema.LBDutySet;
import com.sinosoft.lis.vschema.LBGetSet;
import com.sinosoft.lis.vschema.LBGrpPolSet;
import com.sinosoft.lis.vschema.LBInsureAccClassSet;
import com.sinosoft.lis.vschema.LBInsureAccSet;
import com.sinosoft.lis.vschema.LBInsureAccTraceSet;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LBPremSet;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanFactorySet;
import com.sinosoft.lis.vschema.LCContPlanParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.lis.vschema.LCGrpContStateSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCRnewStateLogSet;
import com.sinosoft.lis.vschema.LCUrgeVerifyLogSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.xb.XBConst;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class GrpRnewDueFeeBackBL {

	  private FeeConst tDEAL = new FeeConst();
	    public CErrors mErrors; //错误的容器。
	    private GlobalInput mGlobalInput; //完整的操作员信息
	    private String mCurrentDate; //当前日期
	    private String mCurrentTime; //当前时间
	    private VData mInputData = new VData();
	    private MMap map = new MMap();
	    //实收个人保费表
	    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
	    private LJAPayPersonSchema mLJAPayPersonSchema = new LJAPayPersonSchema();

	    //实收团体交费表
	    private LJAPayGrpSet mLJaPayGrpSet = new LJAPayGrpSet();
	    private LJAPayGrpSchema mLJAPayGrpSchema = new LJAPayGrpSchema();
	    private LJAPaySet mLJAPaySet = new LJAPaySet();
	    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();

	    //应收总表备份表
	    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();
	    private Reflections ref = new Reflections();
	    private LCRnewStateLogSchema mLCRnewStateLogSchema = null;  //险种续保日志
	    private LCRnewStateLogSchema mGrpLCRnewStateLogSchema = null;  //险种续保日志
	    private LCGrpContSchema mLCGrpContSchema = null;  //本保单信息
	    
	    private LCContSchema  mLCContSchema = null;

	    //应收总表集
//	    private LJAPaySet tLJSPaySet = new LJAPaySet();
//	    private LJAPayGrpSet mLJSPayGrpSet = new LJAPayGrpSet();
//	    private LJAPayGrpSet mYelLJSPayGrpSet = new LJAPayGrpSet();

	    private boolean flag = true;


	    public GrpRnewDueFeeBackBL() {
	        mCurrentDate = PubFun.getCurrentDate();
	        mCurrentTime = PubFun.getCurrentTime();
	        mErrors = new CErrors();
	    }

	    public boolean submitData(VData cInputData, String cOperate) {
	        if(!this.getInputData(cInputData))
	        {
	            return false;
	        }
	        if(!this.checkData())
	        {
	            //清除正在转出状态。
	            if(flag == true)
	            {
	                ExeSQL tExeSQL = new ExeSQL();
	                String sqlLock = " update ljspayb set DealState = '"
	                                 + tDEAL.DEALSTATE_URGESUCCEED
	                                 + "' where GetNoticeNo = '"
	                                 + this.mLJSPayBSchema.getGetNoticeNo() + "'";
	                if (!tExeSQL.execUpdateSQL(sqlLock))
	                    this.addError("GrpDueFeeBackBL", "getInputData",
	                                  "实收转出状态撤消错误!");
	            }
	            return false;
	        }

	        if(!this.dealData())
	        {
	            return false;
	        }
	        return true;
	    }

	    /**
	     * 从输入数据中得到所有对象
	     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	     */

	    private boolean getInputData(VData data) {
	        mGlobalInput = (GlobalInput) data.getObject(1);
	        mLJSPayBSchema = (LJSPayBSchema) data.getObject(0);
	        if (mGlobalInput == null || mLJSPayBSchema == null) {
	            // @@错误处理
	            this.addError("GrpDueFeeBackBL","getInputData","没有得到足够的数据，请您确认!");
	            return false;
	        }
	        LJSPayBSet tLJSPayBSet = new LJSPayBSet();
	        LJSPayBDB tLJSPayBDB = mLJSPayBSchema.getDB();
	        String sql = "select * from  LJSPayB where getnoticeno = '"
	                     + mLJSPayBSchema.getGetNoticeNo()
	                     +"'";
	        tLJSPayBSet = tLJSPayBDB.executeQuery(sql);
	        if (tLJSPayBDB.mErrors.needDealError()) {
	        this.mErrors.addOneError("查询错误!");
	        return false;
	        }

	        if (tLJSPayBSet == null || tLJSPayBSet.size() == 0) {
	            // @@错误处理
	            this.addError("GrpDueFeeBL","getInputData","没有得到足够的数据，请您确认!");
	            return false;
	        }
	        mLJSPayBSchema.setSchema(tLJSPayBSet.get(1));
	        
	        LCGrpContDB tLCgrpContDB = new LCGrpContDB();
	        tLCgrpContDB.setGrpContNo(mLJSPayBSchema.getOtherNo());
	        tLCgrpContDB.getInfo();
	        mLCGrpContSchema = tLCgrpContDB.getSchema();
	        return true;
	    }


	    private boolean checkData() {
	        ExeSQL tExeSQL = new ExeSQL();
	        if (mLJSPayBSchema.getDealState().equals(tDEAL.DEALSTATE_BACK) &&
	            !checkErrorFlag()) {
	            this.addError("GrpRnewDueFeeBL", "checkData",
	                          "您好，保单存在进行实收保费转出处理，不能再次执行。");
	            flag = false;
	            return false;
	        }
	        else
	        {
	            //清除错误标记
	            if (!delErrorLog()) {
	                this.setErrorLog();
	                this.addError("", "dealdata", "清除处理错误标志操作失败!");
	                return false;

	            }

	            //将表的处理状态标记改为 5  锁定记录
	            if (!this.changeState(mLJSPayBSchema.getGetNoticeNo())) {
	                this.addError("GrpDueFeeBackBL", "dealData", "更新记录状态错误，无法完成操作");
	                return false;
	            }
	        }

	        String sql = " select * from LJSPayB where otherno = '"
	                     + mLJSPayBSchema.getOtherNo()
	                     + "' and getnoticeno > '"
	                     + mLJSPayBSchema.getGetNoticeNo()
	                     + "' and dealstate in ('1','5') "
	                     ;
	        String result = tExeSQL.getOneValue(sql);
	        if (!result.equals("")) {
	            this.addError("GrpDueFeeBackBL", "checkDate",
	                          "只能按实收保费核销时间从后往前的顺序进行实收保费转出，请您确认!");
	            return false;
	        }


	        sql = " select * from ljspayperson where grpcontno = '"
	              + mLJSPayBSchema.getOtherNo()
	              + "' and getnoticeno > '"
	              + mLJSPayBSchema.getGetNoticeNo()
	              + "'"
	              ;
	        result = tExeSQL.getOneValue(sql);
	        if (!result.equals("")) {
	            this.addError("GrpDueFeeBackBL", "checkData",
	                          "您好，保单有在进行中的续期业务，不能转出。");
	            return false;

	        }
	        
	        sql = "  select 1 from LCRnewStateLog "
	              + "where grpcontno = '" + mLJSPayBSchema.getOtherNo() + "' "
	              + "   and state in('" +  XBConst.RNEWSTATE_APP + "', '"
	              + XBConst.RNEWSTATE_UNUW + "', '" + XBConst.RNEWSTATE_UNHASTEN
	              + "')";
	        result = new ExeSQL().getOneValue(sql);
	        if(!result.equals(""))
	        {
	            mErrors.addOneError("保单有未完成的续期续保业务，不能转出。");
	            return false;
	        }
	        //new校验是否有险种保单发生过理赔

//	        sql = " select * from LLRegister a where RgtObjNo = '"
//	              + mLJSPayBSchema.getOtherNo()
//	              + "' and rgtdate >= "
//	              + "       (select min(lastPayTodate) from LJAPayPerson "
//	              + "       where grpContNo = '"
//	              + mLJSPayBSchema.getOtherNo()
//	              + "'          and getNoticeNo = '"
//	              + mLJSPayBSchema.getGetNoticeNo() + "' )"
//	              ;
	//
//	        result = tExeSQL.getOneValue(sql);
//	        if (!result.equals("")) {
//	            this.addError("GrpDueFeeBL", "checkData",
//	                          "您好，保单正在做理赔业务或发生过结案的理陪，不能转出。");
	//
//	            return false;
	//
//	        }

	        //校验报单续期后是否发生过理赔


//	        sql = " select * from llclaimdetail a where GrpcontNo = '"
//	              + mLJSPayBSchema.getOtherNo()
//	              + "' and makedate >= "
//	              + "       (select min(lastPayTodate) from LJAPayPerson "
//	              + "       where grpContNo = '"
//	              + mLJSPayBSchema.getOtherNo()
//	              + "'          and getNoticeNo = '"
//	              + mLJSPayBSchema.getGetNoticeNo() + "' )"
//	              ;
	//
//	        result = tExeSQL.getOneValue(sql);
//	        if (!result.equals("")) {
//	            this.addError("GrpDueFeeBL", "checkData",
//	                          "您好，保单正在做理赔业务或发生过结案的理陪，不能转出。");
	//
//	            return false;
	//
//	        }
	        
	        //校验是否有险种在某保障年度发生过理赔
	        
//	      先查看是否有正在进行的理赔
	    	String currentclaimsql = "select 1 from llcase a where " +
			       "exists (select 1 from lcinsured where grpcontno ='" + mLJSPayBSchema.getOtherNo() 
	             + "' and insuredno = a.customerno) and endcasedate is null and rgtstate <> '14' ";
	    	System.out.println(currentclaimsql);
	        String rgtNo = new ExeSQL().getOneValue(currentclaimsql);
	        if (rgtNo != null && !rgtNo.equals("")) 
	           {
	            mErrors.addOneError("保单"+mLJSPayBSchema.getOtherNo()+"有理陪，不能给付确认。");
	            return false;
	           }
	        //然后查看是否有某个保障年度操作的理赔
	    	System.out.println("续收保费号"+mLJSPayBSchema.getGetNoticeNo());
	        String sqlClaim = "select  1 from " +
	    			"llcase a," +
	    			"llcaserela b," +
	    			"llsubreport c," +
	    			"llclaimpolicy d " +
	    			"where a.caseno=b.caseno " +
	    			"and b.subrptno=c.subrptno " +
	    			"and a.caseno=d.caseno " +
	    			"and b.caserelano=d.caserelano " +
	    			"and a.rgtstate in ('09','11','12') " +
	    			"and a.endcasedate is not null " +
	    			"and c.accdate <(select max(curpaytodate) from ljapayperson where getnoticeno='"+mLJSPayBSchema.getGetNoticeNo()+"')" +
	    			"and c.accdate >(select min(lastpaytodate) from ljapayperson where getnoticeno='"+mLJSPayBSchema.getGetNoticeNo()+"')" +
	    			"and d.grpcontno='"+mLJSPayBSchema.getOtherNo()+"' with ur";	
	    	System.out.println(sqlClaim);
	    	String result1 = new ExeSQL().getOneValue(sqlClaim);
	    	if(!result1.equals(""))
	        {
	            mErrors.addOneError("该保障年度内发生过结案的理陪，不能转出。");
	            return false;
	        }
	        

	    	//正在做保全
	        sql = " select * from lpedorapp , lpgrpedoritem "
	              + " where LPGrpEdorItem.GrpContNO = '"
	              + mLJSPayBSchema.getOtherNo()
	              +"' and lpedorapp.EdorAcceptNo = lpgrpedoritem.EdorAcceptNo "
	              + " and lpedorapp.edorstate <> '0'";

	        result = tExeSQL.getOneValue(sql);
	        if (!result.equals("")) {
	            this.addError("GrpDueFeeBL", "checkDate",
	                          "您好，保单有未结案的保全业务，不能转出。");
	            return false;
	        }

	        sql = " select * from LJAGetEndorse "
	              + "where LJAGetEndorse.GrpContNo = '"
	              + mLJSPayBSchema.getOtherNo()
	              + "' and LJAGetEndorse.getmoney <> 0 "
	              + " and makedate >= "
	              + " (select min(lastPayTodate) from LJAPayPerson "
	              + "  where grpContNo = '"
	              + mLJSPayBSchema.getOtherNo()
	              + "'       and getNoticeNo = '"
	              + mLJSPayBSchema.getGetNoticeNo() + "' )"
	              ;
	        result = tExeSQL.getOneValue(sql);
	        if (!result.equals("")) {
	            this.addError("GrpDueFeeBL", "checkData",
	                          "您好，保单有过对保费有影响的保全业务，不能转出。");
	            return false;
	        }

	        sql = " select * from lpgrpedoritem a "
	              +
	              "where EdorType in ( 'NI' ,'ZT')"
	              + "and GrpContNo = '"
	              + mLJSPayBSchema.getOtherNo()
	              + "'  and edorAppDate >= "
	              + "       (select makeDate from LJAPay "
	              + "       where incomeNo = a.grpContNo "
	              + "          and getNoticeNo = '"
	              + mLJSPayBSchema.getGetNoticeNo() + "' )"
	              ;
	        result = tExeSQL.getOneValue(sql);
	        if (!result.equals("")) {
	            this.addError("GrpDueFeeBL",
	                          "checkDate",
	                          "您好，保单有过对保费有影响的保全业务，不能转出。");
	            return false;
	        }

	        sql = " select * from lcgrpcontstate "
	              +
	              "where lcgrpcontstate.GrpcontNo = '"
	              + mLJSPayBSchema.getOtherNo()
	              +
	              "' and lcgrpcontstate.startdate < '"
	              + this.mCurrentDate
	              +
	              "' and ( lcgrpcontstate.endDate is null or endDate > '"
	              + this.mCurrentDate
	              + "' )"
	              ;
	        result = tExeSQL.getOneValue(sql);
	        if (!result.equals("")) {
	            this.addError("GrpDueFeeBL",
	                          "checkDate",
	                          "您好，当前保单或其险种处于失效或终止状态，不能转出。");
	            return false;
	        }

	        if(!checkPrem()){
	        	return false;
	        }
	        return true;

	    }

	    private boolean setErrorLog()
	    {

	        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new LCUrgeVerifyLogSchema();
	        //加到催收核销日志表
	        tLCUrgeVerifyLogSchema.setSerialNo(mLJSPayBSchema.getSerialNo());
	        tLCUrgeVerifyLogSchema.setRiskFlag("1");
	        tLCUrgeVerifyLogSchema.setOperateType("3"); //1：续期催收操作,2：续期核销操作,3:实收保费转出
	        tLCUrgeVerifyLogSchema.setOperateFlag("1"); //1：个案操作,2：批次操作
	        tLCUrgeVerifyLogSchema.setOperator(mGlobalInput.Operator);
	        tLCUrgeVerifyLogSchema.setDealState("2"); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成

	        tLCUrgeVerifyLogSchema.setContNo(mLJSPayBSchema.getOtherNo());
	        tLCUrgeVerifyLogSchema.setMakeDate(mCurrentDate);
	        tLCUrgeVerifyLogSchema.setMakeTime(mCurrentTime);
	        tLCUrgeVerifyLogSchema.setModifyDate(mCurrentDate);
	        tLCUrgeVerifyLogSchema.setModifyTime(mCurrentTime);

	        MMap tMap = new MMap();
	        tMap.put(tLCUrgeVerifyLogSchema, "INSERT");

	        VData tInputData = new VData();
	        tInputData.add(tMap);

	        PubSubmit tPubSubmit = new PubSubmit();
	        if (tPubSubmit.submitData(tInputData, "") == false) {
	            this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
	            return false;
	        }
	        return true;
	}

	    private boolean changeState(String getNoticeNo){
	        ExeSQL tExeSQL = new ExeSQL();
	        String  sqlLock = " update ljspayb set DealState = '"
	                          + tDEAL.DEALSTATE_BACK +
	                          "' where GetNoticeNo = '"+getNoticeNo+"'";
	        return tExeSQL.execUpdateSQL(sqlLock);
	    }

	    private boolean dealData() {

	        //产生新的PayNo
	        String tPayNo = PubFun1.CreateMaxNo("PayNo",PubFun.getNoLimit(this.mLJSPayBSchema.getManageCom()));

	        LJAPayDB tLJAPayDB = new LJAPayDB();
	        //获取数据
	        StringBuffer tSBql = new StringBuffer(128);
	        tSBql.append("select * from LJAPay where GetNoticeNo='")
	                    .append(mLJSPayBSchema.getGetNoticeNo())
	                    .append("'");
	        String sqlStr = tSBql.toString();

	        mLJAPaySet = tLJAPayDB.executeQuery(sqlStr);

	        tSBql = new StringBuffer(128);
	        tSBql.append("select * ")
	                .append("from LJAPayPerson ")
	                .append("where payno = '")
	                .append(mLJAPaySet.get(1).getPayNo())
	                .append("' with ur ");
	        LJAPayPersonSet set = new LJAPayPersonSet();
	        RSWrapper rswrapper = new RSWrapper();
	      //  LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
	        rswrapper.prepareData(this.mLJAPayPersonSet,tSBql.toString());
	        Reflections tReflections = new Reflections();

	        do {

	            rswrapper.getData();
	            //循环取得5000条数据，进行处理

	            set.clear();
	            for (int j = 1; j <= this.mLJAPayPersonSet.size(); j++) {

	                LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
	                this.mLJAPayPersonSchema = this.mLJAPayPersonSet.get(j);
	                //交换记录
	                tReflections.transFields(tLJAPayPersonSchema,
	                                         this.mLJAPayPersonSchema);
	                tLJAPayPersonSchema.setSumDuePayMoney( -this.
	                        mLJAPayPersonSchema.getSumDuePayMoney());
	                tLJAPayPersonSchema.setSumActuPayMoney( -this.
	                        mLJAPayPersonSchema.getSumActuPayMoney());
	                tLJAPayPersonSchema.setPayDate(this.mCurrentDate);
	                tLJAPayPersonSchema.setEnterAccDate(this.mCurrentDate);
	                tLJAPayPersonSchema.setConfDate(this.mCurrentDate);
	                tLJAPayPersonSchema.setPayNo(tPayNo);
	                tLJAPayPersonSchema.setMakeDate(this.mCurrentDate);
	                tLJAPayPersonSchema.setMakeTime(this.mCurrentTime);
	                tLJAPayPersonSchema.setMoneyNoTax(null);
                    tLJAPayPersonSchema.setMoneyTax(null);
                    tLJAPayPersonSchema.setBusiType(null);
                    tLJAPayPersonSchema.setTaxRate(null); 
	                tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
	                tLJAPayPersonSchema.setModifyDate(this.mCurrentDate);
	                tLJAPayPersonSchema.setModifyTime(this.mCurrentTime);
	                set.add(tLJAPayPersonSchema);                
	                
	            }

	            MMap tMMap = new MMap();
	            tMMap.put(set, "INSERT");
	            VData data = new VData();
	            data.add(tMMap);
	            PubSubmit submit = new PubSubmit();
	            submit.submitData(data, "INSERT");
	            if (submit.mErrors.needDealError()) {
	                this.setErrorLog();
	                this.addError("GrpRnewDueFeeBackBL", "dealData", "生成负实收记录错误");
	                return false;
	            }
	              	            
	        }while (this.mLJAPayPersonSet.size() > 0);
	        
	        
	       String tpolno = "select distinct contno from LJAPayPerson where grpcontno='"+mLJAPayPersonSchema.getGrpContNo()+"' and getnoticeno = '"+mLJSPayBSchema.getGetNoticeNo()+"'";
	        
	       SSRS tSSRS = new ExeSQL().execSQL(tpolno);
	       if(tSSRS.getMaxRow()>0){
	    	   for(int m=1;m<=tSSRS.getMaxRow();m++){
	    		   
	    	        LCContDB tLCContDB = new LCContDB();
	    	        tLCContDB.setContNo(tSSRS.GetText(m, 1));
	    	        tLCContDB.getInfo();
	    	        mLCContSchema = tLCContDB.getSchema();
	                
	                LCPolDB tLCPolDB = new LCPolDB();
//	              tLCPolDB.setContNo(mLCContSchema.getContNo());
	              /* modify by fuxin 2008-6-12
	               * 原程序根据保单的险种做实收保费转出，现在根据实收对应的险种做转出。
	               * 为了解决少儿险和其它险种一起销售的问题。
	               */
	              String  sql =" select a.* From LCPol a where a.contno ='"+mLCContSchema.getContNo()+"' "
	                          +" and polno in(select distinct polno from ljapayperson where  a.contno= contno ) "
	                          +" with ur "
	                        ;
	              LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);

	              if(tLCPolSet.size() == 0)
	              {
	                  mErrors.addOneError("没有查询到保单" + mLCContSchema.getContNo()
	                                      + "的险种信息");
	                  return false;
	              }

	              for(int i = 1; i <= tLCPolSet.size(); i++)
	              {
	                  LCPolSchema tLCPolSchema = tLCPolSet.get(i);
	                boolean  mIsXBPol = isXBPol(tLCPolSchema);
	            //    boolean  mHasXBPol = mHasXBPol || mIsXBPol;
	                  if(mIsXBPol)
	                  {
	                      if(!backXBPrem(tLCPolSchema)
	                         || !backXBGet(tLCPolSchema)
	                         || !backXBDuty(tLCPolSchema)
	                         || !backXBPol(tLCPolSchema)
	                         || !backXBInsureAcc(tLCPolSchema)
	                         || !backOtherXBPolInfo()
	                         || !changeXBPolState())
	                      {
	                          return false;
	                      }
	                  }
	           
	              }
	              //add by xp 处理相关保单同时做实收保费转出时的错误
	              if(mLCRnewStateLogSchema!=null)
	              {	
	              String sqlcontstate = "delete from LCContState "
	                  + "where contno = '"
	                  + mLCRnewStateLogSchema.getNewContNo() + "' "
	                  + "   and stateType = '" + XBConst.TERMINATE + "' "
	                  + "   and state = '1' "
	                  + "   and StateReason = 'XB' and polno='000000' ";
	              map.put(sqlcontstate, "DELETE");
	              }
	                
	              if(!backXBCont()){
	            	  return false;
	              }
	    	   }
	       }
	        
	        //生成负团单险种实收记录
	         tSBql = new StringBuffer(128);
	         tSBql.append("select * from LJAPayGrp where GetNoticeNo = '")
	                 .append(this.mLJSPayBSchema.getGetNoticeNo())
	                 .append("'");
	         String sql = tSBql.toString();
	         this.mLJaPayGrpSet = new LJAPayGrpDB().executeQuery(sql);
	         for(int j = 1;j<=this.mLJaPayGrpSet.size();j++)
	         {
	         this.mLJAPayGrpSchema = this.mLJaPayGrpSet.get(j);
	         LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
	         tReflections.transFields(tLJAPayGrpSchema,this.mLJAPayGrpSchema);
	         tLJAPayGrpSchema.setSumActuPayMoney(-this.mLJAPayGrpSchema.getSumActuPayMoney());
	         tLJAPayGrpSchema.setSumDuePayMoney(-mLJAPayGrpSchema.getSumDuePayMoney());
	         tLJAPayGrpSchema.setPayDate(this.mCurrentDate);
	         tLJAPayGrpSchema.setEnterAccDate(this.mCurrentDate);
	         tLJAPayGrpSchema.setPayNo(tPayNo);
	         tLJAPayGrpSchema.setMakeDate(this.mCurrentDate);
	         tLJAPayGrpSchema.setMakeTime(this.mCurrentTime);
	         tLJAPayGrpSchema.setMoneyNoTax(null);
	         tLJAPayGrpSchema.setMoneyTax(null);
	         tLJAPayGrpSchema.setBusiType(null);
	         tLJAPayGrpSchema.setTaxRate(null); 
	         tLJAPayGrpSchema.setOperator(mGlobalInput.Operator);
	         tLJAPayGrpSchema.setConfDate(this.mCurrentDate);
	         tLJAPayGrpSchema.setModifyDate(this.mCurrentDate);
	         tLJAPayGrpSchema.setModifyTime(this.mCurrentTime);
	         map.put(tLJAPayGrpSchema,"INSERT");
	         }

	         //生成负团单实收记录
	         tSBql = new StringBuffer(128);
	         tSBql.append("select * from LJAPay where GetNoticeNo = '")
	                 .append(this.mLJSPayBSchema.getGetNoticeNo())
	                 .append("'");
	         String sql1 = tSBql.toString();
	         this.mLJAPaySchema = new LJAPayDB().executeQuery(sql1).get(1);
	         LJAPaySchema tLJAPaySchema = new LJAPaySchema();
	         tReflections.transFields(tLJAPaySchema, this.mLJAPaySchema);
	         tLJAPaySchema.setSumActuPayMoney(-this.mLJAPaySchema.getSumActuPayMoney());
	         tLJAPaySchema.setPayDate(this.mCurrentDate);
	         tLJAPaySchema.setEnterAccDate(this.mCurrentDate);
	         tLJAPaySchema.setPayNo(tPayNo);
	         tLJAPaySchema.setMakeDate(this.mCurrentDate);
	         tLJAPaySchema.setMakeTime(this.mCurrentTime);
	         tLJAPaySchema.setOperator(mGlobalInput.Operator);
	         tLJAPaySchema.setConfDate(this.mCurrentDate);
	         tLJAPaySchema.setModifyDate(this.mCurrentDate);
	         tLJAPaySchema.setModifyTime(this.mCurrentTime);
	         map.put(tLJAPaySchema,"INSERT");
	         
	         LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
	         String tsql="";
             if(null != mLJAPayGrpSchema.getGetNoticeNo() && !mLJAPayGrpSchema.getGetNoticeNo().equals("")){
	                tsql=" select a.* From LCgrpPol a where a.grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"' "
	                    +" and grppolno in(select distinct grppolno from ljapaygrp where getNoticeNo='"+mLJAPayGrpSchema.getGetNoticeNo()+"' and a.grpcontno= grpcontno ) "
	                    +" with ur "
	                   ;
              }else {
	                tsql=" select a.* From LCgrpPol a where a.grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"' "
                        +" and grppolno in(select distinct grppolno from ljapaygrp where  a.grpcontno= grpcontno ) "
                        +" with ur "
                        ;
               }

	        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.executeQuery(tsql);

	        if(tLCGrpPolSet.size() == 0)
	        {
	            mErrors.addOneError("没有查询到保单" + mLCGrpContSchema.getGrpContNo()
	                                + "的险种信息");
	            return false;
	        }

	        for(int i = 1; i <= tLCGrpPolSet.size(); i++){
	        	 LCGrpPolSchema tLCPolSchema = tLCGrpPolSet.get(i);
	        	 
	             boolean  mIsXBPol = isGrpXBPol(tLCPolSchema);
		                  if(mIsXBPol)
		                  {
		                      if(
		                    	//	  !backXBPrem(tLCPolSchema)
		                    //     || !backXBGet(tLCPolSchema)
		                    //     || !backXBDuty(tLCPolSchema)
		                          !backGrpXBPol(tLCPolSchema)
		                         || !backGrpPlan()
		                 //        || !backXBInsureAcc(tLCPolSchema)
		                    //     || !backOtherXBPolInfo()
		                         || !changeGrpXBPolState())
		                      {
		                          return false;
		                      }
		                  }
	        	 

	        }
	        if(mGrpLCRnewStateLogSchema!=null)
	        {	
	        String sqlcontstate = "delete from LCgrpContState "
	            + "where grpcontno = '"
	            + mGrpLCRnewStateLogSchema.getNewGrpContNo() + "' "
	            + "   and stateType = '" + XBConst.TERMINATE + "' "
	            + "   and state = '1' "
	            + "   and StateReason = 'XB' and grppolno='000000' ";
	        map.put(sqlcontstate, "DELETE");
	        }
	        
	         if(!backGrpXBCont()){
           	  return false;
             }
	        
	        mInputData.add(map);
	        PubSubmit tPubSubmit = new PubSubmit();
	         if (tPubSubmit.submitData(mInputData, "") == false) {
	             //添加失败记录到日志表
	            this.setErrorLog();
	            this.addError("","dealdata","回退操作失败!");
	            return false;
	         }
	         MMap tMMap = addAppAcc();
	         if (tMMap == null) {
	             return false;
	         }
	         String  sqlBackSucc = " update ljspayb set dealstate = '"
	                    +tDEAL.DEALSTATE_BACK_SUCC+
	                    "' where GetNoticeNo = '"
	                    +this.mLJSPayBSchema.getGetNoticeNo()+"'";

	         tMMap.put(sqlBackSucc,"UPDATE");
	         tPubSubmit = new PubSubmit();
	         VData t = new VData();
	         t.add(tMMap);
	         if (tPubSubmit.submitData(t,"") == false) {
	             //添加失败记录到日志表
	            this.setErrorLog();
	            this.addError("","dealdata","回退操作失败!");
	            return false;
	         }
	         if(!delErrorLog())
	         {
	             this.setErrorLog();
	             this.addError("", "dealdata", "修改标志操作失败!");
	             return false;

	         }
	         return true;
	    }

	    private void addError(String moduleName, String functionName,
	                          String errorMessage) {
	        CError tError = new CError();
	        tError.moduleName = moduleName;
	        tError.functionName = functionName;
	        tError.errorMessage = errorMessage;
	        this.mErrors.addOneError(tError);
	    }
	    /**
	     * 回退责任信息
	     * @param tLCPolSchema LCPolSchema
	     * @return boolean
	     */
	    private boolean backXBPrem(LCPolSchema tLCPolSchema)
	    {
	        //从LBPrem表得到原缴费项信息，回退缴费项数据
	        LBPremDB tLBPremDB = new LBPremDB();
	        tLBPremDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
	        LBPremSet tLBPremSet = tLBPremDB.query();
	        if(tLBPremSet.size() == 0)
	        {
	            mErrors.addOneError("没有查询到险种的缴费项信息"
	                                + tLCPolSchema.getRiskCode());
	            return false;
	        }
	        for(int i = 1; i <= tLBPremSet.size(); i++)
	        {
	            LCPremSchema tLCPremSchema = new LCPremSchema();
	            ref.transFields(tLCPremSchema, tLBPremSet.get(i));

	            tLCPremSchema.setContNo(mLCRnewStateLogSchema.getContNo());  //号码换成续保前号码
	            tLCPremSchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
	            tLCPremSchema.setOperator(mGlobalInput.Operator);
	            tLCPremSchema.setModifyDate(this.mCurrentDate);
	            tLCPremSchema.setModifyTime(this.mCurrentTime);
	            map.put(tLCPremSchema, "DELETE&INSERT");
	        }
	        map.put(tLBPremSet, "DELETE");

	        return true;
	    }
	    
	    private boolean backXBCont()
	    {
	        //从LBPrem表得到原缴费项信息，回退缴费项数据
	        LBContDB tLBContDB = new LBContDB();
	        tLBContDB.setContNo(mLCRnewStateLogSchema.getNewContNo());
	        if(!tLBContDB.getInfo())
	        {
	            mErrors.addOneError("没有查询到险种的缴费项信息"
	                                + mLCRnewStateLogSchema.getContNo());
	            return false;
	        }

	        LCContSchema schema = new LCContSchema();
	        ref.transFields(schema, tLBContDB.getSchema());
	        schema.setGrpContNo(mLCRnewStateLogSchema.getGrpContNo());
	        schema.setContNo(mLCContSchema.getContNo()); //号码换成续保前号码
	        schema.setProposalContNo(mLCContSchema.getProposalContNo());
	        schema.setOperator(mGlobalInput.Operator);
	        schema.setModifyDate(this.mCurrentDate);
	        schema.setModifyTime(this.mCurrentTime);
	        schema.setStateFlag(BQ.STATE_FLAG_SIGN);
	        map.put(schema, "DELETE&INSERT");
	        map.put(tLBContDB.getSchema(), "DELETE");

	        //删除lccontstate数据 modify by wanghl
	        LCContStateDB tLCContStateDB=new LCContStateDB();
	        tLCContStateDB.setOtherNo(tLBContDB.getSchema().getEdorNo());
	        tLCContStateDB.setContNo(tLBContDB.getSchema().getContNo());
	        LCContStateSet LCContStateSet=tLCContStateDB.query();
	        if(LCContStateSet!=null&&LCContStateSet.size()>0)
	        {
	        	 map.put(LCContStateSet, "DELETE");	
	        }
	       
	        
	        //删除直接从C表复制到B表的数据
	        String[] tables = {"LBCustomerImpart", "LBCustomerImpartDetail",
	                          "LBCustomerImpartParams", "LBAppnt", "LBInsured"};
	        for(int i = 0; i < tables.length; i++)
	        {
	            String sql = "delete from " + tables[i]
	                         + " where contNo = '"
	                         + mLCRnewStateLogSchema.getNewContNo() + "' ";
	            map.put(sql, "DELETE");
	        }

	        return true;
	    }
	    
	    
	    private boolean backGrpXBCont()
	    {
	        //从LBPrem表得到原缴费项信息，回退缴费项数据
	        LBGrpContDB tLBContDB = new LBGrpContDB();
	        tLBContDB.setGrpContNo(mGrpLCRnewStateLogSchema.getNewGrpContNo());
	        if(!tLBContDB.getInfo())
	        {
	            mErrors.addOneError("没有查询到险种的缴费项信息"
	                                + mGrpLCRnewStateLogSchema.getGrpContNo());
	            return false;
	        }

	        LCGrpContSchema schema = new LCGrpContSchema();
	        ref.transFields(schema, tLBContDB.getSchema());

	        schema.setGrpContNo(mLCGrpContSchema.getGrpContNo()); //号码换成续保前号码
	        schema.setProposalGrpContNo(mLCGrpContSchema.getProposalGrpContNo());
	        schema.setOperator(mGlobalInput.Operator);
	        schema.setModifyDate(this.mCurrentDate);
	        schema.setModifyTime(this.mCurrentTime);
	        schema.setStateFlag(BQ.STATE_FLAG_SIGN);
	        schema.setState(null);
	        map.put(schema, "DELETE&INSERT");
	        map.put(tLBContDB.getSchema(), "DELETE");

	        //删除lccontstate数据 
	        LCGrpContStateDB tLCContStateDB=new LCGrpContStateDB();
	        tLCContStateDB.setOtherNo(tLBContDB.getSchema().getEdorNo());
	        tLCContStateDB.setGrpContNo(tLBContDB.getSchema().getGrpContNo());
	        LCGrpContStateSet LCContStateSet=tLCContStateDB.query();
	        if(LCContStateSet!=null&&LCContStateSet.size()>0)
	        {
	        	 map.put(LCContStateSet, "DELETE");	
	        }
	       
	        
	        //删除直接从C表复制到B表的数据
	        String[] tables = {"LBGrpAppnt"};
	        for(int i = 0; i < tables.length; i++)
	        {
	            String sql = "delete from " + tables[i]
	                         + " where GrpcontNo = '"
	                         + mGrpLCRnewStateLogSchema.getNewGrpContNo() + "' ";
	            map.put(sql, "DELETE");
	        }

	        return true;
	    }
	    /**
	     * backXBDuty
	     *
	     * @param tLCPolSchema LCPolSchema
	     */
	    private boolean backXBDuty(LCPolSchema tLCPolSchema)
	    {
	        //从LBDuty表得到原缴费项信息，回退责任项数据
	        LBDutyDB tLBDutyDB = new LBDutyDB();
	        tLBDutyDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
	        LBDutySet tLBDutySet = tLBDutyDB.query();
	        if(tLBDutySet.size() == 0)
	        {
	            mErrors.addOneError("没有查询到险种的缴费项信息"
	                                + tLCPolSchema.getRiskCode());
	            return false;
	        }
	        for(int i = 1; i <= tLBDutySet.size(); i++)
	        {
	            LCDutySchema tLCDutySchema = new LCDutySchema();
	            ref.transFields(tLCDutySchema, tLBDutySet.get(i));

	            tLCDutySchema.setContNo(mLCRnewStateLogSchema.getContNo());  //号码换成续保前号码
	            tLCDutySchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
	            tLCDutySchema.setOperator(mGlobalInput.Operator);
	            tLCDutySchema.setModifyDate(this.mCurrentDate);
	            tLCDutySchema.setModifyTime(this.mCurrentTime);
	            map.put(tLCDutySchema, "DELETE&INSERT");
	        }
	        map.put(tLBDutySet, "DELETE");

	        return true;
	    }
	    /**
	     *  回退计划
	     * @param tLCPolSchema
	     * @return
	     */
	    
	    private boolean backGrpPlan( ){
	    	
	    	  LBContPlanDB tLCContPlanDB = new LBContPlanDB();
	          tLCContPlanDB.setGrpContNo(mGrpLCRnewStateLogSchema.getNewGrpContNo());
	          LBContPlanSet tLCContPlanSet = tLCContPlanDB.query();
	          for(int i = 1; i <= tLCContPlanSet.size(); i++)
	          {
	              LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
	              ref.transFields(tLCContPlanSchema, tLCContPlanSet.get(i));
	        
	              tLCContPlanSchema.setGrpContNo(mGrpLCRnewStateLogSchema.getGrpContNo());
	              tLCContPlanSchema.setProposalGrpContNo(mLCGrpContSchema.getProposalGrpContNo());
	              tLCContPlanSchema.setOperator(mGlobalInput.Operator);
	             // PubFun.fillDefaultField(tLCContPlanSchema);
	              tLCContPlanSchema.setModifyDate(this.mCurrentDate);
	              tLCContPlanSchema.setModifyTime(this.mCurrentTime);
	              map.put(tLCContPlanSchema, "DELETE&INSERT");

	         
	          }
	          map.put(tLCContPlanSet, "DELETE");

	          //保单险种保险计划-----------------
	          LBContPlanRiskDB tLCContPlanRiskDB = new LBContPlanRiskDB();
	          tLCContPlanRiskDB.setGrpContNo(mGrpLCRnewStateLogSchema.getNewGrpContNo());
	          LBContPlanRiskSet tLCContPlanRiskSet = tLCContPlanRiskDB.query();
	          for(int i = 1; i <= tLCContPlanRiskSet.size(); i++)
	          {
	              LCContPlanRiskSchema tLBContPlanRiskSchema = new LCContPlanRiskSchema();
	              ref.transFields(tLBContPlanRiskSchema, tLCContPlanRiskSet.get(i));
	              tLBContPlanRiskSchema.setGrpContNo(mGrpLCRnewStateLogSchema.getGrpContNo());
	              tLBContPlanRiskSchema.setProposalGrpContNo(mLCGrpContSchema.getProposalGrpContNo());;
	              tLBContPlanRiskSchema.setOperator(mGlobalInput.Operator);
	              tLBContPlanRiskSchema.setModifyDate(this.mCurrentDate);
	              tLBContPlanRiskSchema.setModifyTime(this.mCurrentTime);
	              map.put(tLBContPlanRiskSchema, "DELETE&INSERT");
	          }
	          map.put(tLCContPlanRiskSet, "DELETE");

	          //保险计划责任要素值-----------------
	          LBContPlanDutyParamDB tLCContPlanDutyParamDB = new LBContPlanDutyParamDB();
	          tLCContPlanDutyParamDB.setGrpContNo(mGrpLCRnewStateLogSchema.getNewGrpContNo());
	          LBContPlanDutyParamSet tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.query();
	          for(int i = 1; i <= tLCContPlanDutyParamSet.size(); i++)
	          {
	              LCContPlanDutyParamSchema tLBContPlanDutyParamSchema
	                  = new LCContPlanDutyParamSchema();
	              ref.transFields(tLBContPlanDutyParamSchema,
	                              tLCContPlanDutyParamSet.get(i));
	              tLBContPlanDutyParamSchema.setGrpContNo(mGrpLCRnewStateLogSchema.getGrpContNo());
	              tLBContPlanDutyParamSchema.setProposalGrpContNo(
	            		  mLCGrpContSchema.getProposalGrpContNo());
	              tLBContPlanDutyParamSchema.setGrpPolNo(mGrpLCRnewStateLogSchema.getGrpPolNo());
	              map.put(tLBContPlanDutyParamSchema, "DELETE&INSERT");
	          }
	          map.put(tLCContPlanDutyParamSet, "DELETE");
	          //LCContPlanDutyParam表没有需要变更的字段

	          //保险计划责任要素值-----------------
	          LBContPlanFactoryDB tLCContPlanFactoryDB = new LBContPlanFactoryDB();
	          tLCContPlanFactoryDB.setGrpContNo(mGrpLCRnewStateLogSchema.getNewGrpContNo());
	          LBContPlanFactorySet tLCContPlanFactorySet = tLCContPlanFactoryDB.query();
	          for(int i = 1; i <= tLCContPlanFactorySet.size(); i++)
	          {
	              LCContPlanFactorySchema tLBContPlanFactorySchema
	                  = new LCContPlanFactorySchema();
	              ref.transFields(tLBContPlanFactorySchema,
	                              tLCContPlanFactorySet.get(i));

	              tLBContPlanFactorySchema.setGrpContNo(mGrpLCRnewStateLogSchema.getGrpContNo());
	              tLBContPlanFactorySchema.setProposalGrpContNo(
	            		  mLCGrpContSchema.getProposalGrpContNo());

	              tLBContPlanFactorySchema.setModifyDate(this.mCurrentDate);
	              tLBContPlanFactorySchema.setModifyTime(this.mCurrentTime);        
	              map.put(tLBContPlanFactorySchema, "DELETE&INSERT");	            
	          }
	          map.put(tLCContPlanFactorySet, "DELETE");

	          //LCContPlanParam-----------------
	          LBContPlanParamDB tLCContPlanParamDB = new LBContPlanParamDB();
	          tLCContPlanParamDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
	          LBContPlanParamSet tLCContPlanParamSet = tLCContPlanParamDB.query();
	          for(int i = 1; i <= tLCContPlanParamSet.size(); i++)
	          {
	              LCContPlanParamSchema tLBContPlanParamSchema
	                  = new LCContPlanParamSchema();
	              ref.transFields(tLBContPlanParamSchema,
	                              tLCContPlanParamSet.get(i));
	             
	              tLBContPlanParamSchema.setGrpContNo(mGrpLCRnewStateLogSchema.getGrpContNo());
	              tLBContPlanParamSchema.setProposalGrpContNo(
	            		  mLCGrpContSchema.getProposalGrpContNo());        
	              tLBContPlanParamSchema.setModifyDate(this.mCurrentDate);
	              tLBContPlanParamSchema.setModifyTime(this.mCurrentTime);
	              map.put(tLBContPlanParamSchema, "DELETE&INSERT");
	          }
	          map.put(tLCContPlanParamSet, "DELETE");
	    	
	    	
	        return true;
	    }
	    
	    
	    /**
	     * 回退续保险种
	     * @param tLCPolSchema LCPolSchema：待回退险中
	     * @return boolean：成功true，否则false
	     */
	    private boolean backGrpXBPol(LCGrpPolSchema tLCPolSchema)
	    {
	        //从LBPrem表得到原缴费项信息，回退缴费项数据
	        LBGrpPolDB tLBPolDB = new LBGrpPolDB();
	        tLBPolDB.setGrpPolNo(mGrpLCRnewStateLogSchema.getNewGrpPolNo());
	        LBGrpPolSet tLBPolSet = tLBPolDB.query();
	        if(tLBPolSet.size() == 0)
	        {
	            mErrors.addOneError("没有查询到险种的缴费项信息"
	                                + tLCPolSchema.getRiskCode());
	            return false;
	        }
	        for(int i = 1; i <= tLBPolSet.size(); i++)
	        {
	            LCGrpPolSchema schema = new LCGrpPolSchema();
	            ref.transFields(schema, tLBPolSet.get(i));

	            schema.setGrpContNo(mGrpLCRnewStateLogSchema.getGrpContNo());  //号码换成续保前号码
	            schema.setGrpPolNo(mGrpLCRnewStateLogSchema.getGrpPolNo());
	            schema.setOperator(mGlobalInput.Operator);
	            schema.setState(null);
	            schema.setModifyDate(this.mCurrentDate);
	            schema.setModifyTime(this.mCurrentTime);
	            schema.setStateFlag(BQ.STATE_FLAG_SIGN);
	            map.put(schema, "DELETE&INSERT");
	        }
	        map.put(tLBPolSet, "DELETE");

	        return true;
	    }
	    
	    /**
	     * 回退续保险种
	     * @param tLCPolSchema LCPolSchema：待回退险中
	     * @return boolean：成功true，否则false
	     */
	    private boolean backXBPol(LCPolSchema tLCPolSchema)
	    {
	        //从LBPrem表得到原缴费项信息，回退缴费项数据
	        LBPolDB tLBPolDB = new LBPolDB();
	        tLBPolDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
	        LBPolSet tLBPolSet = tLBPolDB.query();
	        if(tLBPolSet.size() == 0)
	        {
	            mErrors.addOneError("没有查询到险种的缴费项信息"
	                                + tLCPolSchema.getRiskCode());
	            return false;
	        }
	        for(int i = 1; i <= tLBPolSet.size(); i++)
	        {
	            LCPolSchema schema = new LCPolSchema();
	            ref.transFields(schema, tLBPolSet.get(i));
	            schema.setGrpContNo(mLCRnewStateLogSchema.getGrpContNo()); 
	            schema.setGrpPolNo(mLCRnewStateLogSchema.getGrpPolNo()); 
	            schema.setContNo(mLCRnewStateLogSchema.getContNo());  //号码换成续保前号码
	            schema.setPolNo(mLCRnewStateLogSchema.getPolNo());
	            schema.setOperator(mGlobalInput.Operator);
	            schema.setModifyDate(this.mCurrentDate);
	            schema.setModifyTime(this.mCurrentTime);
	            schema.setStateFlag(BQ.STATE_FLAG_SIGN);
	            map.put(schema, "DELETE&INSERT");
	        }
	        map.put(tLBPolSet, "DELETE");

	        return true;
	    }
	    /**
	     * backXBInsureAcc
	     * 回退帐户信息
	     * @param tLCPolSchema LCPolSchema：待回退帐户信息
	     * @return boolean：操作成功true，否则false
	     */
	    private boolean backXBInsureAcc(LCPolSchema tLCPolSchema)
	    {
	        //从LBInsureAcc表得到原缴费项信息，回退帐户数据
	        LBInsureAccDB tLBInsureAccDB = new LBInsureAccDB();
	        tLBInsureAccDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
	        LBInsureAccSet tLBInsureAccSet = tLBInsureAccDB.query();

	        //有的险种可能没有帐户
	        if(tLBInsureAccSet.size() == 0)
	        {
	            return true;
	        }
	        for(int i = 1; i <= tLBInsureAccSet.size(); i++)
	        {
	            LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
	            ref.transFields(tLCInsureAccSchema, tLBInsureAccSet.get(i));
	            tLCInsureAccSchema.setGrpContNo(mLCRnewStateLogSchema.getGrpContNo()); 
	            tLCInsureAccSchema.setContNo(mLCRnewStateLogSchema.getContNo());  //号码换成续保前号码
	            tLCInsureAccSchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
	            tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
	            tLCInsureAccSchema.setModifyDate(this.mCurrentDate);
	            tLCInsureAccSchema.setModifyTime(this.mCurrentTime);
	            map.put(tLCInsureAccSchema, "DELETE&INSERT");
	        }
	        map.put(tLBInsureAccSet, "DELETE");

	        //从LBInsureAccClass表得到原缴费项信息，回退帐户分类数据
	        LBInsureAccClassDB tLBInsureAccClassDB = new LBInsureAccClassDB();
	        tLBInsureAccClassDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
	        LBInsureAccClassSet tLBInsureAccClassSet = tLBInsureAccClassDB.query();
	        if(tLBInsureAccClassSet.size() == 0)
	        {
	            mErrors.addOneError("没有查询到险种的缴费项信息"
	                                + tLCPolSchema.getRiskCode());
	            return false;
	        }
	        for(int i = 1; i <= tLBInsureAccClassSet.size(); i++)
	        {
	            LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
	            ref.transFields(tLCInsureAccClassSchema, tLBInsureAccClassSet.get(i));
	            tLCInsureAccClassSchema.setGrpContNo(mLCRnewStateLogSchema.getGrpContNo()); 
	            tLCInsureAccClassSchema.setGrpPolNo(mLCRnewStateLogSchema.getGrpPolNo()); 
	            tLCInsureAccClassSchema.setContNo(mLCRnewStateLogSchema.getPolNo());  //号码换成续保前号码
	            tLCInsureAccClassSchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
	            tLCInsureAccClassSchema.setOperator(mGlobalInput.Operator);
	            tLCInsureAccClassSchema.setModifyDate(this.mCurrentDate);
	            tLCInsureAccClassSchema.setModifyTime(this.mCurrentTime);
	            map.put(tLCInsureAccClassSchema, "DELETE&INSERT");
	        }
	        map.put(tLBInsureAccClassSet, "DELETE");

	        //从LBInsureAccClass表得到原缴费项信息，回退帐户轨迹数据
	        LBInsureAccTraceDB tLBInsureAccTraceDB = new LBInsureAccTraceDB();
	        tLBInsureAccTraceDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
	        LBInsureAccTraceSet tLBInsureAccTraceSet = tLBInsureAccTraceDB.query();
	        if(tLBInsureAccTraceSet.size() == 0)
	        {
	            mErrors.addOneError("没有查询到险种的缴费项信息"
	                                + tLCPolSchema.getRiskCode());
	            return false;
	        }
	        for(int i = 1; i <= tLBInsureAccTraceSet.size(); i++)
	        {
	            LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
	            ref.transFields(tLCInsureAccTraceSchema, tLBInsureAccTraceSet.get(i));
	            tLCInsureAccTraceSchema.setGrpContNo(mLCRnewStateLogSchema.getGrpContNo()); 
	            tLCInsureAccTraceSchema.setGrpPolNo(mLCRnewStateLogSchema.getGrpPolNo()); 
	            tLCInsureAccTraceSchema.setContNo(mLCRnewStateLogSchema.getPolNo());  //号码换成续保前号码
	            tLCInsureAccTraceSchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
	            tLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
	            tLCInsureAccTraceSchema.setModifyDate(this.mCurrentDate);
	            tLCInsureAccTraceSchema.setModifyTime(this.mCurrentTime);
	            map.put(tLCInsureAccTraceSchema, "DELETE&INSERT");
	        }
	        map.put(tLBInsureAccTraceSet, "DELETE");

	        return true;
	    }
	    private boolean backOtherXBPolInfo()
	    {
	        String[] tables =
	            {"LBBnf", "LBInsuredRelated", "LBPremToAcc",
	            "LBGetToAcc", "LBInsureAccFee", "LBInsureAccClassFee"};
	        for(int i = 0; i < tables.length; i++)
	        {
	            String sql = "delete from " + tables[i]
	                         + " where polNo = '"
	                         + mLCRnewStateLogSchema.getNewPolNo() + "' ";
	            map.put(sql, "DELETE");
	        }

	        return true;
	    }
	    /**
	     * 若续保时险种续保终止，则需要将LCContState表记录删除
	     * @return boolean
	     */
	    private boolean changeGrpXBPolState()
	    {
	        String sql = "delete from LCGrpContState "
	                     + "where grppolNo = '"
	                     + mGrpLCRnewStateLogSchema.getGrpPolNo() + "' "
	                     + "   and stateType = '" + XBConst.TERMINATE + "' "
	                     + "   and state = '1' "
	                     + "   and StateReason = '11' ";
	        map.put(sql, "DELETE");

	        map.put(mGrpLCRnewStateLogSchema, "DELETE");
	        //qulq add 2007-8-25
	        LBRnewStateLogSchema tLBRnewStateLogSchema = new LBRnewStateLogSchema();

	        Reflections tReflections = new Reflections();

	        tReflections.transFields(tLBRnewStateLogSchema,mGrpLCRnewStateLogSchema);

	        tLBRnewStateLogSchema.setOperator(this.mGlobalInput.Operator);
	        tLBRnewStateLogSchema.setMakeDate(this.mCurrentDate);
	        tLBRnewStateLogSchema.setModifyDate(this.mCurrentDate);
	        tLBRnewStateLogSchema.setMakeTime(this.mCurrentTime);
	        tLBRnewStateLogSchema.setModifyTime(this.mCurrentTime);
	        map.put(tLBRnewStateLogSchema, "INSERT");

	        return true;
	    }
	    
	    /**
	     * 若续保时险种续保终止，则需要将LCContState表记录删除
	     * @return boolean
	     */
	    private boolean changeXBPolState()
	    {
	        String sql = "delete from LCContState "
	                     + "where polNo = '"
	                     + mLCRnewStateLogSchema.getPolNo() + "' "
	                     + "   and stateType = '" + XBConst.TERMINATE + "' "
	                     + "   and state = '1' "
	                     + "   and StateReason = '11' ";
	        map.put(sql, "DELETE");

	        map.put(mLCRnewStateLogSchema, "DELETE");
	        //qulq add 2007-8-25
	        LBRnewStateLogSchema tLBRnewStateLogSchema = new LBRnewStateLogSchema();

	        Reflections tReflections = new Reflections();

	        tReflections.transFields(tLBRnewStateLogSchema,mLCRnewStateLogSchema);

	        tLBRnewStateLogSchema.setOperator(this.mGlobalInput.Operator);

	        tLBRnewStateLogSchema.setMakeDate(this.mCurrentDate);
	        tLBRnewStateLogSchema.setModifyDate(this.mCurrentDate);
	        tLBRnewStateLogSchema.setMakeTime(this.mCurrentTime);
	        tLBRnewStateLogSchema.setModifyTime(this.mCurrentTime);
	        map.put(tLBRnewStateLogSchema, "INSERT");

	        return true;
	    }
	    /**
	     * backXBGet
	     *回退责任项数据
	     * @param tLCPolSchema LCPolSchema：待回退险种信息
	     */
	    private boolean backXBGet(LCPolSchema tLCPolSchema)
	    {
	        //从LBGet表得到原缴费项信息，回退责任项数据
	        LBGetDB tLBGetDB = new LBGetDB();
	        tLBGetDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
	        LBGetSet tLBGetSet = tLBGetDB.query();
	        if(tLBGetSet.size() == 0)
	        {
	            mErrors.addOneError("没有查询到险种的缴费项信息"
	                                + tLCPolSchema.getRiskCode());
	            return false;
	        }
	        for(int i = 1; i <= tLBGetSet.size(); i++)
	        {
	            LCGetSchema tLCGetSchema = new LCGetSchema();
	            ref.transFields(tLCGetSchema, tLBGetSet.get(i));
	            tLCGetSchema.setGrpContNo(mLCRnewStateLogSchema.getGrpContNo());           
	            tLCGetSchema.setContNo(mLCRnewStateLogSchema.getContNo());  //号码换成续保前号码
	            tLCGetSchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
	            tLCGetSchema.setOperator(mGlobalInput.Operator);
	            tLCGetSchema.setModifyDate(this.mCurrentDate);
	            tLCGetSchema.setModifyTime(this.mCurrentTime);
	            map.put(tLCGetSchema, "DELETE&INSERT");
	        }
	        map.put(tLBGetSet, "DELETE");

	        return true;
	    }
	    private boolean isGrpXBPol(LCGrpPolSchema tLCPolSchema)
	    {
	        String sql = "select * from LCRnewStateLog "
	                     + "where grppolNo = '" + tLCPolSchema.getGrpPolNo() + "' "
	                    + "   and contno = '"
	                    + tLCPolSchema.getGrpContNo() + "' order by renewCount desc";
	        LCRnewStateLogSet tLCRnewStateLogSet
	            = new LCRnewStateLogDB().executeQuery(sql);
	        if(tLCRnewStateLogSet.size() > 0)
	        {
	            this.mGrpLCRnewStateLogSchema = tLCRnewStateLogSet.get(1);
	            return true;
	        }
	        else
	        {
	            return false;
	        }
	    }
	    
	    private boolean isXBPol(LCPolSchema tLCPolSchema)
	    {
	        String sql = "select * from LCRnewStateLog "
	                     + "where polNo = '" + tLCPolSchema.getPolNo() + "' "
	                     + "   and contno = '"
		                 + tLCPolSchema.getContNo() + "' "
	                    + "   and renewCount = "
	                    + tLCPolSchema.getRenewCount() + " ";
	        LCRnewStateLogSet tLCRnewStateLogSet
	            = new LCRnewStateLogDB().executeQuery(sql);
	        if(tLCRnewStateLogSet.size() > 0)
	        {
	            this.mLCRnewStateLogSchema = tLCRnewStateLogSet.get(1);
	            return true;
	        }
	        else
	        {
	            return false;
	        }
	    }
	/**
	*检测是否有回退错误发生，如果有返回true
	*/
	    private boolean checkErrorFlag() {


	        LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
	        LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new LCUrgeVerifyLogSet();
	        tLCUrgeVerifyLogDB.setSerialNo(mLJSPayBSchema.getSerialNo());
	        tLCUrgeVerifyLogDB.setOperateType("3");//3：实收保费转出
	        tLCUrgeVerifyLogDB.setOperateFlag("1");
	        tLCUrgeVerifyLogDB.setRiskFlag("1");
	        tLCUrgeVerifyLogDB.setDealState("2");
	        tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
	        if ((tLCUrgeVerifyLogSet == null) || tLCUrgeVerifyLogSet.size() == 0)
	        {
	            return false;
	        }
	        return true;

	    }
	    /**
	     * 余额存入帐户(使用的是保单服务退费)
	     * */
	    private MMap addAppAcc(){
	        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
	        tLCAppAccTraceSchema.setCustomerNo(mLJSPayBSchema.getAppntNo());
	        tLCAppAccTraceSchema.setOtherNo(mLJSPayBSchema.getOtherNo());
	        tLCAppAccTraceSchema.setOtherType("1");
	        tLCAppAccTraceSchema.setMoney(getSumPayMoney());
	        tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);
	        tLCAppAccTraceSchema.setBakNo(mLJSPayBSchema.getGetNoticeNo());//续期应收号码，modify by fuxin 2009-3-2
	        AppAcc tAppAcc = new AppAcc();
	        MMap tMMap = tAppAcc.accShiftToSSZC(tLCAppAccTraceSchema);
	        if(tMMap == null || tMMap.size() == 0)
	        {
	            this.setErrorLog();
	            mErrors.copyAllErrors(tAppAcc.mErrors);
	            return null;
	        }

	        return tMMap;

	    }

	    /**
	     * 得到原续期所交保费，包括客户交费和帐户抵扣余额
	     * @return double
	     */
	    private double getSumPayMoney()
	    {
	        String sql = "select sum(sumDuePayMoney) "
	                     + "from LJAPayGrp "
	                     + "where payType = 'ZC' "
	                     + "   and sumDuePayMoney >= 0  and grpcontno= '"
	                     + mLJAPaySchema.getIncomeNo() + "' ";
	        if(null !=mLJAPaySchema.getGetNoticeNo() && !mLJAPaySchema.getGetNoticeNo().equals("")){
	        	sql = sql      + "   and getNoticeNO = '"
                   + mLJAPaySchema.getGetNoticeNo() + "' ";
	        }
	             
	        String moneyStr = new ExeSQL().getOneValue(sql);
	        if(moneyStr.equals("") || moneyStr.equals("null"))
	        {
	            return Double.MIN_VALUE;
	        }
	        else
	        {
	            return Double.parseDouble(moneyStr);
	        }
	    }

	    private boolean delErrorLog()
	    {

	        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new LCUrgeVerifyLogSchema();
	        //加到催收核销日志表
	        tLCUrgeVerifyLogSchema.setSerialNo(mLJSPayBSchema.getSerialNo());
	        tLCUrgeVerifyLogSchema.setRiskFlag("1");
	        tLCUrgeVerifyLogSchema.setOperateType("3"); //1：续期催收操作,2：续期核销操作,3:实收保费转出
	        tLCUrgeVerifyLogSchema.setOperateFlag("1"); //1：个案操作,2：批次操作

	        MMap tMap = new MMap();
	        tMap.put(tLCUrgeVerifyLogSchema, "DELETE");

	        VData tInputData = new VData();
	        tInputData.add(tMap);
	        PubSubmit tPubSubmit = new PubSubmit();
	        if (tPubSubmit.submitData(tInputData, "") == false) {
	            this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
	            return false;
	        }
	        return true;

	    }
	    
	    /**
	     * 校验转出保费金额和实收金额是否相等
	     * @return
	     */
	    private boolean checkPrem(){
	        ExeSQL tExeSQL = new ExeSQL();
	    	String sqlprem = "select sum(prem) from lcpol where contno in (select contno from ljapayperson where grpcontno = '" + mLJSPayBSchema.getOtherNo() + "' and GetNoticeNo = '"+mLJSPayBSchema.getGetNoticeNo()+"')";
	    	double a = Double.parseDouble(tExeSQL.getOneValue(sqlprem));
	    	String sqllja = "select sum(sumDuePayMoney) from ljspaypersonb where grpcontno = '" + mLJSPayBSchema.getOtherNo() + "' and paytype = 'ZC' and getnoticeno='" + mLJSPayBSchema.getGetNoticeNo() +"'";
	    	double b = Double.parseDouble(tExeSQL.getOneValue(sqllja));
	    	double WLPRate = 0.9;
	    	double GYHRate = getGYHRate();
	    	double lja = b / WLPRate / GYHRate;  
	    	double ljab = b/GYHRate;
	    	BigDecimal c= new BigDecimal(lja);
	    	c = c.setScale(2,BigDecimal.ROUND_HALF_UP);
	    	double d = c.doubleValue() + 0.01;
	    	double e = c.doubleValue() - 0.01;
	    	double f = c.doubleValue();
	    	BigDecimal h= new BigDecimal(ljab);
	     	h = h.setScale(2,BigDecimal.ROUND_HALF_UP);
	    	double m = h.doubleValue() + 0.01;
	    	double n = h.doubleValue() - 0.01;
	    	double p = h.doubleValue();
	    	if(a != f && a != d && a != e && a != p && a != m && a != n){
	    		if(a != b){
	    			mErrors.addOneError("本次转出的保费与保单实交保费金额不等，不能转出。");
	    			return false;
	    		}        		
	    	}
	    	return true;
	    }
	    
	    /**
	     * 获取家庭单团体优惠费率
	     * @return
	     */
	    private double getGYHRate(){
	    	double rate = 1;
	    	String tlcpolSQL = "select * from lcpol where grpcontno = '" + mLCGrpContSchema.getGrpContNo() + "'";
	    	LCPolSet tLCPolSet = new LCPolSet();
	    	LCPolDB tLCPolDB = new LCPolDB();
	    	tLCPolSet = tLCPolDB.executeQuery(tlcpolSQL);
	    	int peoples = tLCPolSet.size();
	    	if(peoples<3){
	    		return 1;
	    	}
	    	for(int i = 1;i<tLCPolSet.size();i++){
	    		String birthday = tLCPolSet.get(i).getInsuredBirthday();
	    		int year = PubFun.calInterval(birthday,tLCPolSet.get(i).getEndDate(),"Y");
	    		if(year >= 75){
	    			peoples--;
	    		}
	    	}
	        if(peoples>=3){
	        	rate = 0.9;
	        }       
	        
	    	return rate;
	    }
	}

