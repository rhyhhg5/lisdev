package com.sinosoft.lis.operfee;

//程序名称：PrtContContinueListBL.java
//程序功能：个险保单继续率统计,查询保单继续率,下载清单。
//创建日期：2011-02-22 
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

import java.util.ArrayList;
import java.util.Date;

import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PrtContContinueListBL {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();

  private VData mResult = new VData();
  
  private CreateExcelList mCreateExcelList = new CreateExcelList("");
  
  private TransferData mTransferData = new TransferData();
  
  private GlobalInput mGlobalInput =new GlobalInput();
  
  //对比起始日期
  private String mStartDate="";
  private String mEndDate="";
  private String mManageCom=""; 
  
  public PrtContContinueListBL() 
  {
  }

/**
  传输数据的公共方法
*/
    public CreateExcelList getsubmitData(VData cInputData, String cOperate)
    {

      // 得到外部传入的数据，将数据备份到本类中
      if(!getInputData(cInputData)) 
      {
        return null;
      }

      if(cOperate.equals("1")) //传统险
      {
    	  if(!getPrintData()) 
          {
            return null;
          }
      }
      if(cOperate.equals("2")) //万能险
      {
          if(!getULIPrintData()) 
          {
            return null;
          }
      }

      if(mCreateExcelList==null)
      {
    	  buildError("submitData", "Excel数据为空");
          return null;
      }
      return mCreateExcelList;
    }
  
  public static void main(String[] args)
  {
      
	  PrtContContinueListBL tbl =new PrtContContinueListBL();
      GlobalInput tGlobalInput = new GlobalInput();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("StartDate", "2009-1-23");
      tTransferData.setNameAndValue("EndDate", "2009-2-23");
      tTransferData.setNameAndValue("ManageCom", "8691");
      tTransferData.setNameAndValue("ContType", "0");
      tTransferData.setNameAndValue("SaleChnl", "00");
      tTransferData.setNameAndValue("ContinueType", "2");
//      tTransferData.setNameAndValue("ContNo", "004583137000001");
      tGlobalInput.ManageCom="86";
      tGlobalInput.Operator="zgm";
      VData tData = new VData();
      tData.add(tGlobalInput);
      tData.add(tTransferData);

      CreateExcelList tCreateExcelList=new CreateExcelList();
      tCreateExcelList=tbl.getsubmitData(tData,"2");
      if(tCreateExcelList==null)
      {
    	  System.out.println("没取道数据…………");
      }
      else
      {
    	  try
          {
        	  tCreateExcelList.write("c:\\contactcompare.xls");
          }catch(Exception e)
          {
        	  System.out.println("EXCEL生成失败！");
          }
      }
  }


  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
	  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
	  if(mGlobalInput==null) 
	  {
	    buildError("getInputData", "没有得到公共信息！");
	    return false;
	  }
	  mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
	  mStartDate = (String) mTransferData.getValueByName("StartDate");
	  mEndDate = (String) mTransferData.getValueByName("EndDate");
	  mManageCom = (String) mTransferData.getValueByName("ManageCom");
	  if(mStartDate.equals("")||mEndDate.equals("")||mStartDate==null||mEndDate==null) 
	  {
	    buildError("getInputData", "没有得到足够的信息:起始和终止日期不能为空！");
	    return false;
	  }
	  return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "LCGrpContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean getPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"管理机构", "管理机构代码", "营销单位代码","销售单位名称", "保单号", "保单生效日", "险种生效保费", "续期应缴日", "实收日期", 
    	  "险种实收保费", "险种代码", "投保人", "移动电话", "联系电话", "投保人联系地址", "收费方式", "代理人编码", 
    	  "代理人手机", "代理人固话", "代理人姓名", "代理机构编码", "代理机构名称", "缴费截止日期", "核销日期", 
    	  "销售渠道", "保单类型", "状态", "缴费年期", "原代理人代码", "原代理人", "险种状态", "保单服务状态"}};
      
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) 
      {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
//    0|^0|全部^1|个险^2|银保^3|互动^4|其他
      String tContType = (String) mTransferData.getValueByName("ContType");
      System.out.println("保单类型："+tContType);
      String sql1 = "";
      if(tContType.equals("0"))
      {
    	  sql1+=" and a.salechnl in ('01','04','10','13','14','15') ";
    	  System.out.println("保单类型：0|全部");  
      }
      else if (tContType.equals("1"))
      {
    	  sql1 = " and a.salechnl in ('01','10') ";
    	  System.out.println("保单类型：1|个险");
      }
      else if (tContType.equals("2"))
      {
    	  sql1 = " and a.salechnl in ('04','13') ";
    	  System.out.println("保单类型：2|银保");
      }
      else if (tContType.equals("3"))
      {
    	  sql1 = " and a.salechnl in ('14','15') ";
    	  System.out.println("保单类型：3|互动");
      }
      //4其他待定
      else
      {
    	  buildError("getPrintData", "保单类型传入错误！");
          return false;
      }
      
//    00|全部^01|个险直销^10|中介代理^04|银行代理^07|职团开拓^08|电话销售^11|产代健^13|银代直销^99|其它
      String tSaleChnl = (String) mTransferData.getValueByName("SaleChnl"); 
      System.out.println("销售渠道："+tSaleChnl);
      String sql2 = "";
      if(tSaleChnl.equals("00"))
      {
    	  sql2 = " and a.SaleChnl in ('01','04','10','13','14','15') ";
      }
//      else if(tSaleChnl.equals("99"))
//      {
//    	  sql2 = " and a.SaleChnl not in ('01','03','04','07','08','11','13') ";
//      }
      else
      {
    	  sql2 = " and a.SaleChnl = '" + tSaleChnl + "' ";
      }
      System.out.println(sql2+"---");
//    1|13个月^2|25个月
      String tContinueType = (String) mTransferData.getValueByName("ContinueType"); 
      String sql3 = "";
      if(tContinueType.equals("1"))
      {
    	  sql3 = " and year(PayEndDate)-year(CvaliDate) >= 2 ";
    	  System.out.println("继续率类型： 1|13个月");
      }
      else if(tContinueType.equals("2"))
      {
    	  sql3 = " and year(PayEndDate)-year(CvaliDate) >= 3 ";
    	  System.out.println("继续率类型： 2|25个月");
      }
      else
      {
    	  buildError("getPrintData", "继续率类型传入错误！");
          return false;
      }
      
      String tContNo = (String) mTransferData.getValueByName("ContNo");
      System.out.println("保单号："+tContNo);
      String sql4 = "";
      if(tContNo==null || tContNo.trim().equals(""))
      {
    	  
      }
      else
      {
    	  sql4 = " and ContNo = '" + tContNo + "' ";
      }
      
      //获得EXCEL列信息      
      String tSQL = 
    	  "select (select Name from LDCom where ComCode = a.ManageCom)," 
    	  + "a.ManageCom,(select branchattr from labranchgroup where AgentGroup = a.AgentGroup),(select Name from labranchgroup where AgentGroup = a.AgentGroup), " 
    	  + "ContNo,CValiDate,nvl((select Prem from LPPol where PolNo = a.PolNo order by double(EdorNo) fetch first 1 row only), a.Prem), "
    	  + "CValiDate + " + tContinueType + " years 续期应缴日, "
    	  + "(select max(PayDate) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 收费时间, "
    	  + "(select sum(SumActuPayMoney) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 保单实收保费, "
    	  + "RiskCode, (select AppntName from LCAppnt where AppntNo = a.AppntNo and ContNo = a.ContNo), "
    	  + "(select c.Mobile from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "(select c.Phone from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "(select c.PostalAddress from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "codename('paymode', a.PayMode),getUniteCode(a.agentcode), "
    	  + "(select b.Mobile from laagent b where b.AgentCode = a.AgentCode), "
	      + "(select b.Phone from laagent b where b.AgentCode = a.AgentCode), "
	      + "(select b.Name from laagent b where b.AgentCode = a.AgentCode), "
	      + "a.AgentCom, (select Name from lacom where AgentCom=a.AgentCom), " 
    	  + "a.PayEndDate, "
    	  + "(select max(ConfDate) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 核销日期, "
    	  + "(select codealias from ldcode1 where codetype='salechnl' and code = a.SaleChnl), "
    	  + "(case when	a.SaleChnl='04' then '银保' when a.SaleChnl='13' then '银保' else '个险' end), "
    	  + "(select (select codename('dealstate',DealState) from LJSPayB where GetNoticeno = b.GetNoticeNo) from LJSPayPersonB b where PolNo = a.PolNo " 
    	  + "and LastPayToDate = a.CValiDate + " + tContinueType + " years and CurPayToDate = LastPayToDate + 1 year order by GetNoticeNo desc fetch first 1 row only), "
    	  + "year(PayEndDate)-year(CvaliDate), "
    	  + "(case when exists (select 1 from laascription where contno = a.contno) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else getUniteCode(a.agentcode) end ), " //原代理人代码
	      + "(select name from laagent where agentcode = (case when exists (select 1 from laascription where contno = a.contno ) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else a.agentcode end )), " //原代理人 
    	  + "codename('stateflag',stateflag), "
    	  + "(case when exists (select 1 from LAAscription where ContNo = a.ContNo and AscripState = '3') then '孤儿单' "  
	      + "when exists (select 1 from laagent where agentcode = a.AgentCode and agentstate>='06') then '孤儿单' "
	      + "when (select employdate from laagent where agentcode = a.AgentCode) > a.SignDate then '孤儿单'  "
	      + "else '在职单' end ), "
	      + "PolNo, Cvalidate, PayIntv " //不在列表中显示，取实收保费用
    	  + "from LCPol a where 1=1 " 
    	  + "and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') "
    	  + "and not exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') "
    	  + "and ContType = '1' and AppFlag = '1' and Prem <>0 and PayIntv = 12 " //PayIntv先设置为年缴的 
    	  + sql1 //保单类型
    	  + sql2 //销售渠道
    	  + sql3 //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
    	  + sql4 //保单号
    	  + "and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' "
    	  + "and ManageCom like '" + mManageCom + "%' "
    	  
    	  + " union all "
    	  
    	  + "select (select Name from LDCom where ComCode = a.ManageCom)," 
    	  + "a.ManageCom,(select branchattr from labranchgroup where AgentGroup = a.AgentGroup),(select Name from labranchgroup where AgentGroup = a.AgentGroup), " 
    	  + "ContNo,CValiDate,nvl((select Prem from LPPol where PolNo = a.PolNo order by double(EdorNo) fetch first 1 row only), a.Prem), "
    	  + "CValiDate + " + tContinueType + " years 续期应缴日, "
    	  + "(select max(PayDate) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 收费时间, "
    	  + "(select sum(SumActuPayMoney) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 保单实收保费, "
    	  + "RiskCode, (select AppntName from LCAppnt where AppntNo = a.AppntNo and ContNo = a.ContNo union select AppntName from LBAppnt where AppntNo = a.AppntNo and ContNo = a.ContNo), "
    	  + "(select c.Mobile from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
    	  + " union select c.Mobile from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "(select c.Phone from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
    	  + " union select c.Phone from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "(select c.PostalAddress from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
    	  + " union select c.PostalAddress from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "codename('paymode', a.PayMode),getUniteCode(a.agentcode), "
    	  + "(select b.Mobile from laagent b where b.AgentCode = a.AgentCode), "
	      + "(select b.Phone from laagent b where b.AgentCode = a.AgentCode), "
	      + "(select b.Name from laagent b where b.AgentCode = a.AgentCode), "
	      + "a.AgentCom, (select Name from lacom where AgentCom=a.AgentCom), " 
    	  + "a.PayEndDate, "
    	  + "(select max(ConfDate) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 核销日期, "
    	  + "(select codealias from ldcode1 where codetype='salechnl' and code = a.SaleChnl), "
    	  + "(case when	a.SaleChnl='04' then '银保' when a.SaleChnl='13' then '银保' else '个险' end), "
    	  + "(select (select codename('dealstate',DealState) from LJSPayB where GetNoticeno = b.GetNoticeNo) from LJSPayPersonB b where PolNo = a.PolNo " 
    	  + "and LastPayToDate = a.CValiDate + " + tContinueType + " years and CurPayToDate = LastPayToDate + 1 year order by GetNoticeNo desc fetch first 1 row only), "
    	  + "year(PayEndDate)-year(CvaliDate), "
    	  + "(case when exists (select 1 from laascription where contno = a.contno) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else getUniteCode(a.agentcode) end ), " //原代理人代码
	      + "(select name from laagent where agentcode = (case when exists (select 1 from laascription where contno = a.contno ) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else a.agentcode end )), " //原代理人 
    	  + "'终止', "
    	  + "(case when exists (select 1 from LAAscription where ContNo = a.ContNo and AscripState = '3') then '孤儿单' "  
	      + "when exists (select 1 from laagent where agentcode = a.AgentCode and agentstate>='06') then '孤儿单' "
	      + "when (select employdate from laagent where agentcode = a.AgentCode) > a.SignDate then '孤儿单'  "
	      + "else '在职单' end ), "
	      + "PolNo, Cvalidate, PayIntv " //不在列表中显示，取实收保费用
    	  + "from LBPol a where 1=1 " 
    	  + "and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') "
    	  + "and not exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') "
    	  + "and ContType = '1' and Prem <>0 and PayIntv = 12 " //PayIntv先设置为年缴的
//    	  解约和协议退保的，不包含犹豫期退保
    	  + "and exists (select 1 from LPEdorItem where EdorNo = a.EdorNo and EdorType in ('CT','XT')) "  
    	  + sql1 //保单类型
    	  + sql2 //销售渠道
    	  + sql3 //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
    	  + sql4 //保单号
    	  + "and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' "
    	  + "and ManageCom like '" + mManageCom + "%' with ur "
    	  ;
      
      System.out.println("查询sql：" + tSQL);
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueListBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }

      ArrayList tArrayList = new ArrayList();
      
      String[][] tGetData = null;
      tGetData=tSSRS.getAllData();
      
      if(tGetData==null)
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueListBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }

      for (int j = 0; j < tGetData.length;j++) 
      {
    	  if(tGetData[j][22]== null || tGetData[j][22].equals("") || tGetData[j][22].equals("null"))
    	  {
        	  tGetData[j][6] = "";
        	  tGetData[j][7] = "";
        	  tGetData[j][8] = "";
        	  tGetData[j][22] = "";
    	  }
    	  if(tGetData[j][25] == null || tGetData[j][25].equals("") || tGetData[j][25].equals("null"))
    	  {
        	  tGetData[j][25] = "未抽档";
    	  }
    	  
    	  tArrayList.add(tGetData[j]);
      }
      String[][] tExcelData=new String[tArrayList.size()][]; 
      for(int m=0 ;m<tArrayList.size(); m++)
      {
    	  tExcelData[m]=(String[]) tArrayList.get(m);
      }

      
      if(mCreateExcelList.setData(tExcelData,displayData)==-1)
      {
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

      return true;
  }

  private boolean getULIPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"管理机构", "管理机构代码", "营销单位代码","销售单位名称", "保单号", "保单生效日", "生效基础保费", "生效额外保费", 
    	  "生效保费（全部）", "续期应缴日","实收日期", "实收基础保费", "实收额外金额", "实收保费（全部）", "险种代码", "投保人", "移动电话", 
    	  "联系电话", "投保人联系地址", "收费方式", "代理人编码", "代理人手机", "代理人固话", "代理人姓名", "代理机构编码", 
    	  "代理机构名称", "缴费截止日期",  "核销日期", "销售渠道", "保单类型", "状态", "缴费年期", 
    	  "原代理人代码", "原代理人", "险种状态", "保单服务状态"}};
      
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) 
      {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
//    0|全部^1|个险^2|银保
      String tContType = (String) mTransferData.getValueByName("ContType");
      System.out.println("保单类型："+tContType);
      String sql1 = "";
      if(tContType.equals("0"))
      {
    	  sql1+=" and a.salechnl in ('01','04','10','13','14','15') ";
    	  System.out.println("保单类型：0|全部");  
      }
      else if (tContType.equals("1"))
      {
    	  sql1 = " and a.salechnl in ('01','10') ";
    	  System.out.println("保单类型：1|个险");
      }
      else if (tContType.equals("2"))
      {
    	  sql1 = " and a.salechnl in ('04','13') ";
    	  System.out.println("保单类型：2|银保");
      }
      else if (tContType.equals("3"))
      {
    	  sql1 = " and a.salechnl in ('14','15') ";
    	  System.out.println("保单类型：3|互动");
      }
      //4其他待定
      else
      {
    	  buildError("getPrintData", "保单类型传入错误！");
          return false;
      }
      
//    00|全部^01|个险直销^10|中介代理^04|银行代理^07|职团开拓^08|电话销售^11|产代健^13|银代直销^99|其它
      String tSaleChnl = (String) mTransferData.getValueByName("SaleChnl"); 
      System.out.println("销售渠道："+tSaleChnl);
      String sql2 = "";
      if(tSaleChnl.equals("00"))
      {
    	  sql2 = " and a.SaleChnl in ('01','04','10','13','14','15') ";
      }
//      else if(tSaleChnl.equals("99"))
//      {
//    	  sql2 = " and a.SaleChnl not in ('01','03','04','13','14','15') ";
//      }
      else
      {
    	  sql2 = " and a.SaleChnl = '" + tSaleChnl + "' ";
      }
      
//    1|13个月^2|25个月
      String tContinueType = (String) mTransferData.getValueByName("ContinueType"); 
      String sql3 = "";
      String inputContinueType = "";
      if(tContinueType.equals("1"))
      {
    	  sql3 = " and year(PayEndDate)-year(CvaliDate) >= 2 ";
    	  inputContinueType = "13";
    	  System.out.println("继续率类型： 1|13个月");
      }
      else if(tContinueType.equals("2"))
      {
    	  sql3 = " and year(PayEndDate)-year(CvaliDate) >= 3 ";
    	  inputContinueType = "25";
    	  System.out.println("继续率类型： 2|25个月");
      }
      else
      {
    	  buildError("getPrintData", "继续率类型传入错误！");
          return false;
      }
      
      String tContNo = (String) mTransferData.getValueByName("ContNo");
      System.out.println("保单号："+tContNo);
      String sql4 = "";
      if(tContNo==null || tContNo.trim().equals(""))
      {
    	  
      }
      else
      {
    	  sql4 = " and ContNo = '" + tContNo + "' ";
      }
      
      //获得EXCEL列信息      
      String tSQL = 
    	  "select (select Name from LDCom where ComCode = a.ManageCom)," 
    	  + "a.ManageCom,(select branchattr from labranchgroup where AgentGroup = a.AgentGroup),(select Name from labranchgroup where AgentGroup = a.AgentGroup), ContNo,CValiDate, "
    	  + "GETINITBASEPREM(a.PolNo) 生效基本保费, '生效额外保费', "
    	  + " nvl((select Prem from LPPol where PolNo = a.PolNo order by double(EdorNo) fetch first 1 row only), a.Prem) 生效保费, "
    	  + "CValiDate + " + tContinueType + " years 续期应缴日, "
    	  + "(select max(PayDate) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 实收日期, "
    	  + "GETACTUBASEPREM(a.PolNo,'"+ inputContinueType + "') 实收基础保费, '实收额外金额', "
    	  + "(select sum(SumActuPayMoney) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 保单实收保费, " 
    	  + "RiskCode, "
    	  + "(select AppntName from LCAppnt where AppntNo = a.AppntNo and ContNo = a.ContNo), "
    	  + "(select c.Mobile from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "(select c.Phone from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "(select c.PostalAddress from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "codename('paymode', a.PayMode),getUniteCode(a.agentcode), "
    	  + "(select b.Mobile from laagent b where b.AgentCode = a.AgentCode), "
	      + "(select b.Phone from laagent b where b.AgentCode = a.AgentCode), "
	      + "(select b.Name from laagent b where b.AgentCode = a.AgentCode), "
	      + "a.AgentCom, (select Name from lacom where AgentCom=a.AgentCom), " 
    	  + "a.PayEndDate, "
    	  + "(select max(ConfDate) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 核销日期, "
    	  + "(select codealias from ldcode1 where codetype='salechnl' and code = a.SaleChnl), "
    	  + "(case when	a.SaleChnl='04' then '银保' when a.SaleChnl='13' then '银保' else '个险' end), "
    	  + "(select (select codename('dealstate',DealState) from LJSPayB where GetNoticeno = b.GetNoticeNo) from LJSPayPersonB b where PolNo = a.PolNo " 
    	  + "and LastPayToDate = a.CValiDate + " + tContinueType + " years and CurPayToDate = LastPayToDate + 1 year order by GetNoticeNo desc fetch first 1 row only), "
    	  + "year(PayEndDate)-year(CvaliDate), "
    	  + "(case when exists (select 1 from laascription where contno = a.contno) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else getUniteCode(a.agentcode) end ), " //原代理人代码
	      + "(select name from laagent where agentcode = (case when exists (select 1 from laascription where contno = a.contno ) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else a.agentcode end )), " //原代理人 
    	  + "codename('stateflag',stateflag), "
    	  + "(case when exists (select 1 from LAAscription where ContNo = a.ContNo and AscripState = '3') then '孤儿单' "  
	      + "when exists (select 1 from laagent where agentcode = a.AgentCode and agentstate>='06') then '孤儿单' "
	      + "when (select employdate from laagent where agentcode = a.AgentCode) > a.SignDate then '孤儿单'  "
	      + "else '在职单' end ), "
	      + "PolNo, Cvalidate, PayIntv " //不在列表中显示，取实收保费用
    	  + "from LCPol a where 1=1 " 
    	  + "and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') "
    	  + "and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') "
    	  + "and ContType = '1' and AppFlag = '1' and Prem <>0 and PayIntv = 12 " //PayIntv先设置为年缴的 
    	  + sql1 //保单类型
    	  + sql2 //销售渠道
    	  + sql3 //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
    	  + sql4 //保单号
    	  + "and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' "
    	  + "and ManageCom like '" + mManageCom + "%' "
    	  
    	  + " union all "
    	  
    	  + "select (select Name from LDCom where ComCode = a.ManageCom)," 
    	  + "a.ManageCom,(select branchattr from labranchgroup where AgentGroup = a.AgentGroup),(select Name from labranchgroup where AgentGroup = a.AgentGroup),ContNo,CValiDate, " 
    	  + "GETINITBASEPREM(a.PolNo) 生效基本保费, '生效额外保费', "
    	  + " nvl((select Prem from LPPol where PolNo = a.PolNo order by double(EdorNo) fetch first 1 row only), a.Prem) 生效保费, "
    	  + "CValiDate + " + tContinueType + " years 续期应缴日, "
    	  + "(select max(PayDate) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 实收日期, "
    	  + "GETACTUBASEPREM(a.PolNo,'"+ inputContinueType + "') 实收基础保费, '实收额外金额', "
    	  + "(select sum(SumActuPayMoney) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 保单实收保费, "
    	  + "RiskCode, "
    	  + "(select AppntName from LCAppnt where AppntNo = a.AppntNo and ContNo = a.ContNo union select AppntName from LBAppnt where AppntNo = a.AppntNo and ContNo = a.ContNo), "
    	  + "(select c.Mobile from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
    	  + " union select c.Mobile from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "(select c.Phone from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
    	  + " union select c.Phone from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "(select c.PostalAddress from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
    	  + " union select c.PostalAddress from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno), "
    	  + "codename('paymode', a.PayMode),getUniteCode(a.agentcode), "
    	  + "(select b.Mobile from laagent b where b.AgentCode = a.AgentCode), "
	      + "(select b.Phone from laagent b where b.AgentCode = a.AgentCode), "
	      + "(select b.Name from laagent b where b.AgentCode = a.AgentCode), "
	      + "a.AgentCom, (select Name from lacom where AgentCom=a.AgentCom), " 
    	  + "a.PayEndDate, "
    	  + "(select max(ConfDate) from LJAPayPerson where PolNo = a.PolNo and LastPayToDate = a.CValiDate + " + tContinueType 
    	  + " years and CurPayToDate = LastPayToDate + 1 year  and PayType = 'ZC') 核销日期, "
    	  + "(select codealias from ldcode1 where codetype='salechnl' and code = a.SaleChnl), "
    	  + "(case when	a.SaleChnl='04' then '银保' when a.SaleChnl='13' then '银保' else '个险' end), "
    	  + "(select (select codename('dealstate',DealState) from LJSPayB where GetNoticeno = b.GetNoticeNo) from LJSPayPersonB b where PolNo = a.PolNo " 
    	  + "and LastPayToDate = a.CValiDate + " + tContinueType + " years and CurPayToDate = LastPayToDate + 1 year order by GetNoticeNo desc fetch first 1 row only), "
    	  + "year(PayEndDate)-year(CvaliDate), "
    	  + "(case when exists (select 1 from laascription where contno = a.contno) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else getUniteCode(a.agentcode) end ), " //原代理人代码
	      + "(select name from laagent where agentcode = (case when exists (select 1 from laascription where contno = a.contno ) then (select agentold from laascription where contno = a.contno order by modifydate,modifytime fetch first 1 row only) else a.agentcode end )), " //原代理人 
    	  + "'终止', "
    	  + "(case when exists (select 1 from LAAscription where ContNo = a.ContNo and AscripState = '3') then '孤儿单' "  
	      + "when exists (select 1 from laagent where agentcode = a.AgentCode and agentstate>='06') then '孤儿单' "
	      + "when (select employdate from laagent where agentcode = a.AgentCode) > a.SignDate then '孤儿单'  "
	      + "else '在职单' end ), "
	      + "PolNo, Cvalidate, PayIntv " //不在列表中显示，取实收保费用
    	  + "from LBPol a where 1=1 " 
    	  + "and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') "
    	  + "and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') "
    	  + "and ContType = '1' and Prem <>0 and PayIntv = 12 " //PayIntv先设置为年缴的
//    	  解约和协议退保的，不包含犹豫期退保
    	  + "and exists (select 1 from LPEdorItem where EdorNo = a.EdorNo and EdorType in ('CT','XT')) "  
    	  + sql1 //保单类型
    	  + sql2 //销售渠道
    	  + sql3 //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
    	  + sql4 //保单号
    	  + "and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' "
    	  + "and ManageCom like '" + mManageCom + "%' with ur"
    	  ;
      
      System.out.println("查询sql：" + tSQL);
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueListBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }

      ArrayList tArrayList = new ArrayList();
      
      String[][] tGetData = null;
      tGetData=tSSRS.getAllData();
      
      if(tGetData==null)
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueListBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      System.out.println("拢共这些条：--" +  tGetData.length);

      for (int j = 0; j < tGetData.length;j++) 
      {
    	  System.out.println("第 " + j + " 条：ContNo = " + tGetData[j][3]);
    	  double initPrem = Double.parseDouble(tGetData[j][7]);
    	  double initBasePrem = Double.parseDouble(tGetData[j][5]);
    	  
    	  //初始保费
//    	生效额外保险费=生效保费-生效基本保费
    	  double initOtherPrem = initPrem-initBasePrem; 
    	  tGetData[j][6] = String.valueOf(initOtherPrem);
    	  
    	  if(tGetData[j][26]== null || tGetData[j][26].equals("") || tGetData[j][26].equals("null"))
    	  {
    		  tGetData[j][8] = "";
        	  tGetData[j][9] = "";
        	  tGetData[j][10] = "";
        	  tGetData[j][11] = "";
        	  tGetData[j][12] = "";
        	  tGetData[j][26] = "";
    	  }
    	  else
    	  {
    		  System.out.println(tGetData[j][10]);
    		  double actuPrem = Double.parseDouble(tGetData[j][12]);
        	  double actuBasePrem = Double.parseDouble(tGetData[j][10]);
        	  
        	  //初始保费
//        	生效额外保险费=生效保费-生效基本保费
        	  double actuOtherPrem = actuPrem-actuBasePrem; 
        	  tGetData[j][11] = String.valueOf(actuOtherPrem);
    	  }
    	  

    	  if(tGetData[j][29]== null || tGetData[j][29].equals("") || tGetData[j][29].equals("null"))
    	  {
    		  tGetData[j][29] = "未抽档";
    	  }
    	  
    	  tArrayList.add(tGetData[j]);
      }
      String[][] tExcelData=new String[tArrayList.size()][]; 
      for(int m=0 ;m<tArrayList.size(); m++)
      {
    	  tExcelData[m]=(String[]) tArrayList.get(m);
      }

      
      if(mCreateExcelList.setData(tExcelData,displayData)==-1)
      {
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }
      
      return true;
  }
}

