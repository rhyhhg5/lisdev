package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class DuePayFeeBLS  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 数据操作字符串 */
  private String mOperate;

  public DuePayFeeBLS() {
  }
  public static void main(String[] args) {
    DuePayFeeBLS mDuePayFeeBLS1 = new DuePayFeeBLS();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    boolean tReturn =false;
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    System.out.println("Start DuePayFee BLS Submit...");
    //信息保存
    if(this.mOperate.equals("INSERT"))
    {tReturn=save(cInputData);}

    if (tReturn)
      System.out.println("Save sucessful");
    else
      System.out.println("Save failed") ;

      System.out.println("End DuePayFee BLS Submit...");

    return tReturn;
  }

//保存操作
  private boolean save(VData mInputData)
  {
   boolean tReturn =true;
    System.out.println("Start Save...");
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
    		// @@错误处理
    		CError tError = new CError();
		tError.moduleName = "DuePayFeeBLS";
                tError.functionName = "saveData";
	        tError.errorMessage = "数据库连接失败!";
		this.mErrors .addOneError(tError) ;
 		return false;
    }
    try{
      conn.setAutoCommit(false);

// 应收个人交费表
      System.out.println("Start 个人交费表...");
      LJSPayPersonDBSet tLJSPayPersonDBSet=new LJSPayPersonDBSet(conn);
      tLJSPayPersonDBSet.set((LJSPayPersonSet)mInputData.getObjectByObjectName("LJSPayPersonSet",0));
      if (!tLJSPayPersonDBSet.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJSPayPersonDBSet.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "DuePayFeeBLS";
	        tError.functionName = "saveData";
	        tError.errorMessage = "应收个人交费表数据保存失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }

      /** 应收总表 */

      System.out.println("Start 应收总表...");
      LJSPayDB tLJSPayDB=new LJSPayDB(conn);
      LJSPayBL tLJSPayBL=(LJSPayBL)mInputData.getObjectByObjectName("LJSPayBL",0);
      tLJSPayDB.setSchema(tLJSPayBL);
      System.out.println("Get LJSPay");
      if (!tLJSPayDB.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJSPayDB.mErrors);
    		CError tError = new CError();
		tError.moduleName = "DuePayFeeBLS";
		tError.functionName = "saveData";
		tError.errorMessage = "应收总表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback();
        conn.close();
 		return false;
      }

      LOPRTManagerSchema tLOPRTManagerSchema=(LOPRTManagerSchema)mInputData.getObjectByObjectName("LOPRTManagerSchema",0);
      LOPRTManagerDB tLOPRTManagerDB=new LOPRTManagerDB(conn);
      tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
      if(tLOPRTManagerDB.insert()==false)
      {
          this.mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "DuePayFeeBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "打印数据保存失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback();
          conn.close();
          return false;
      }

      LOPRTManagerSubSchema tLOPRTManagerSubSchema=(LOPRTManagerSubSchema)mInputData.getObjectByObjectName("LOPRTManagerSubSchema",0);
      LOPRTManagerSubDB tLOPRTManagerSubDB=new LOPRTManagerSubDB(conn);
      tLOPRTManagerSubDB.setSchema(tLOPRTManagerSubSchema);
      if(tLOPRTManagerSubDB.insert()==false)
      {
          this.mErrors.copyAllErrors(tLOPRTManagerSubDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "DuePayFeeBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "打印数据子表保存失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback();
          conn.close();
          return false;
      }

      conn.commit() ;
      conn.close();
      System.out.println("commit over");

      TransferData tTransferData=(TransferData)mInputData.getObjectByObjectName("TransferData",0);
      String autoVerifyFlag=(String)tTransferData.getValueByName("autoVerifyFlag");
      if(autoVerifyFlag.equals("Y"))
      {
          //执行自动核销
          VData tVData=new VData();
          tVData.add(tLJSPayBL.getSchema());

          IndiFinUrgeVerifyBLForAuto tIndiFinUrgeVerifyBLForAuto = new IndiFinUrgeVerifyBLForAuto();
          tIndiFinUrgeVerifyBLForAuto.submitData(tVData,"VERIFY");
      }
    }
    catch (Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="DuePayFeeBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString();
      this.mErrors .addOneError(tError);
      try{conn.rollback() ;} catch(Exception e){}
      tReturn=false;
    }


    return tReturn;
  }
}