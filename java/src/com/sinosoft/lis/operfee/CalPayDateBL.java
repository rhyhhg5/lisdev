package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import java.util.HashMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 计算续期续保应收记录的缴至日期
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.3
 */
public class CalPayDateBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    /**起缴日期*/
    public static final String STARTPAYDATE = "STARTPAYDATE";
    /**截止日期*/
    public static final String PAYDATE = "PAYDATE";

    public CalPayDateBL()
    {
    }

    /**
     * 计算团单续期的缴费日期：
     * i.	抽档日期在应收日期之前：交费截止日期为应交日期后第30日。
     * ii.	抽档日期在应收日期之后，应缴日期后第30日之前：交费截止日期为应交日期后第30日。
     * iii.	抽档日期在应缴日期后第30日之后：交费截止设置为抽档日之后第7天。
     * @param tLCGrpPolSet LCGrpPolSet：发生续期的险种
     * @param payMode String：保单缴费方式
     * @return HashMap：若计算成功HashMap存储以StartPayDate和PayDate值，
     * 否则返回null，可从mErrors中的错误情况。
     */
    public HashMap calPayDateG(LCGrpPolSet tLCGrpPolSet, String payMode)
    {
        if(tLCGrpPolSet == null || tLCGrpPolSet.size() == 0)
         {
             mErrors.addOneError("必须传入团单险种");
             return null;
         }

        FDate fDate = new FDate();
        HashMap dateMap = new HashMap();  //存储计算后的缴费日期
        String startPayDate = null;
        String payDate = null;

        String currDate = PubFun.getCurrentDate();   //当前日期
        String tPayToDate = "1000-01-01";

        //得到险种的最大缴至日期
        for(int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            if(tLCGrpPolSet.get(i).getPaytoDate() == null
               || tLCGrpPolSet.get(i).getPaytoDate().equals(""))
            {
                mErrors.addOneError("险种缴至日期为空，请维护后再次进行本操作。");
                return null;
            }

            if(fDate.getDate(tPayToDate)
               .before(fDate.getDate(tLCGrpPolSet.get(i).getPaytoDate())))
            {
                tPayToDate = tLCGrpPolSet.get(i).getPaytoDate();
            }
        }

        //计算抽档日与缴至日的差 currDate - tPayToDate
        int intv = PubFun.calInterval(tPayToDate, currDate, "D");
        if(intv < 0)
        {
            startPayDate = currDate;
            payDate = PubFun.calDate(tPayToDate, 30, "D", null);
        }
        else if(intv >= 0 && intv <= 30)
        {
            startPayDate = currDate;
            payDate = PubFun.calDate(tPayToDate, 30, "D", null);
        }
        else
        {
            startPayDate = currDate;
            payDate = PubFun.calDate(currDate, 7, "D", null);
        }

        dateMap.put(STARTPAYDATE, startPayDate);
        dateMap.put(PAYDATE, payDate);

        return dateMap;
    }

    /**
     * 计算个单续期缴费日期
     * i.	抽档险种有长期险（或含长期险）：
     1.	自交方式：
     1)	抽档时间在应收日之前：交费截止日期为应交日期后第60日。
     2)	若抽档日之后第十五天在应缴日之后第60天的宽限期之内，则交费截止日期仍设为应缴日期后第60天。
     3)	若抽档日后第十五天已超过60天宽限期，则设为抽档当天之后第十五天为交费截止日期。
     2.	转账方式：
     1)	抽档时间在应收日之前：截至日为应收日期之后第15天。
     2)	抽档时间在应收日之后，第45天之前：设置两次转账日期，第一次为抽档日期之后第7天，第二次为应收日期之后第60天。
     3)	抽档时间在应收日期后第45天至第60天之间：设置两次转账日期，第一次为第60天，第二次为第67天。
     4)	抽档时间在应收日期后第60天之后：(仅设一次转账日期，为抽档日当天之后第7天)
        修改为（转账日期为抽档日当天至第7天每天均可生成报盘数据 见CQ5477 和需求说明书）。
     ii.	抽档险种只有短期险：
     1.	自交方式：
     1)	抽档时间在应收日之前：交费截止日期为应交日期后第15日。
     2)	若抽档日在应收日期之后，交费截止日期设为抽档日后第15天。
     2.	转账方式：
     1)	档时间在应收日之前：设置两次转账，第一次为应收日当天，第二次为应收日之后第7天。
     2)	抽档时间在应收日之后，设置两次转账日期，第一次为抽档日之后第7天，第二次为抽档日后第15天。
     * @param tLCPolSet LCPolSet：需要续期续保的个单险种集合
     * @param payMode String：保单交费方式
     * @return HashMap：若计算成功HashMap存储以StartPayDate和PayDate值，
     * 否则返回null，可从mErrors中的错误情况。
     * 
    * 个单续期缴费日期（抽档险种有长期险或含长期险）修改 20090201 zhanggm cbs00023315
      1) 抽档时间在应收日之前：转账开始日期为保单交至日期，转账终止日期为应收日之后第60天。
      2) 抽档时间在应收日之后，第45天之前：转账开始日期为保单抽档日期，转账终止日期为应收日之后第60天。
      3) 抽档时间在应收日期后第45天至第60天之间：转账开始日期为保单抽档日期，转账终止日期为应收日之后第60天。
      4) 抽档时间在应收日期后第60天之后：此时保单正常应该是失效状态了，只有通过特权复效后方能进行抽档，如果抽档成功，则转帐开始日期为保单抽档日，转账终止日期为抽档日后的第3日。
    * 个单续期缴费日期（短）修改
      1) 抽档时间在应收日之前：转账开始日期为保单交至日期，转账终止日期为应收日之后第30天。
      2) 抽档时间在应收日之后，第15天之前：转账开始日期为保单抽档日期，转账终止日期为应收日之后第30天。
      3) 抽档时间在应收日期后第15天至第30天之间：转账开始日期为保单抽档日期，转账终止日期为应收日之后第30天。
      4) 抽档时间在应收日期后第30天之后：此时保单正常应该是失效状态了，只有通过特权复效后方能进行抽档，如果抽档成功，则转帐开始日期为保单抽档日，转账终止日期为抽档日后的第3日。
    *
    */
    
    public HashMap calPayDateP(LCPolSet tLCPolSet, String payMode)
    {
        if(tLCPolSet == null || tLCPolSet.size() == 0
            || payMode == null || payMode.equals(""))
         {
             mErrors.addOneError("必须传入团单险种和缴费方式");
             return null;
         }

        FDate fDate = new FDate();
        HashMap dateMap = new HashMap();  //存储计算后的缴费日期
        String startPayDate = null;
        String payDate = null;

        String currDate = PubFun.getCurrentDate();   //当前日期
        String tPayToDate = "1000-01-01";
        boolean hasLong = false;   //是否有长期险
        boolean hasTPH = false;   //是否有税优险种
        boolean hasYBK = false;   //是否是上海医保卡险种
        //得到险种的最大缴至日期
        String riskCodes = "";
        for(int i = 1; i <= tLCPolSet.size(); i++)
        {
            if(tLCPolSet.get(i).getPaytoDate() == null
               || tLCPolSet.get(i).getPaytoDate().equals(""))
            {
                mErrors.addOneError("险种缴至日期为空，请维护后再次进行本操作。");
                return null;
            }

            riskCodes += "'" + tLCPolSet.get(i).getRiskCode() + "',";

            if(fDate.getDate(tPayToDate)
               .before(fDate.getDate(tLCPolSet.get(i).getPaytoDate())))
            {
                tPayToDate = tLCPolSet.get(i).getPaytoDate();
            }
        }

//        int gracePeriod
//            = getGracePeriod(riskCodes.substring(0, riskCodes.length() - 1));
        hasLong = hasLongRisk(riskCodes.substring(0, riskCodes.length() - 1));
        //计算抽档日与缴至日的差 currDate - tPayToDate
        int intv = PubFun.calInterval(tPayToDate, currDate, "D");
        //税优产品续期续保时，缴费日期改为60天
        hasTPH=hasTPHRisk(riskCodes.substring(0, riskCodes.length() - 1));
        //yukun 2018-2-24新增
        //上海医保卡产品续保时，缴费日期改为60天
        hasYBK=hasYBKRisk(riskCodes.substring(0, riskCodes.length() - 1));

        //进行交费日期计算
        //包含长期险
        if(hasLong || hasTPH || hasYBK)
        {
            //银行转帐
            if(payMode.trim().equals("4") || payMode.trim().equals("8"))
            {
                if(intv < 0)
                {
                    startPayDate = tPayToDate;
//                    payDate = PubFun.calDate(tPayToDate, 45, "D", null);
                    payDate = PubFun.calDate(tPayToDate, 60, "D", null);
                }
                else if(intv < 45)
                {
                    startPayDate = currDate;
//                    payDate = PubFun.calDate(tPayToDate, 45, "D", null);
                    payDate = PubFun.calDate(tPayToDate, 60, "D", null);
                }
                else if(intv >= 45 && intv <= 57)
                {
                    startPayDate = currDate;
                    payDate = PubFun.calDate(tPayToDate, 60, "D", null);
                }
                else
                {
                    //qulq 2007-3-13 modify
//                    startPayDate = PubFun.calDate(currDate, 7, "D", null);
//                    payDate = startPayDate;
                    startPayDate = currDate;
//                    payDate = PubFun.calDate(currDate, 15, "D", null);
                    payDate = PubFun.calDate(currDate, 3, "D", null);
                }
            }
            //其他缴费方式
            else
            {
                if(intv < 0)
                {
                    startPayDate = currDate;
//                    payDate = PubFun.calDate(tPayToDate, 45, "D", null);
                    payDate = PubFun.calDate(tPayToDate, 60, "D", null);
                }
                else if(intv < 45)
                {
                    startPayDate = currDate;
//                    payDate = PubFun.calDate(tPayToDate, 45, "D", null);
                    payDate = PubFun.calDate(tPayToDate, 60, "D", null);
                }
                else if(intv <= 57)
                {
                    startPayDate = currDate;
                    payDate = PubFun.calDate(tPayToDate, 60, "D", null);
                }
                else
                {
                    startPayDate = currDate;
//                    payDate = PubFun.calDate(currDate, 15, "D", null);
                    payDate = PubFun.calDate(currDate, 3, "D", null);
                }
            }
        }
        //只有短期险
        else
        {
            //银行转帐
            if(payMode.trim().equals("4") || payMode.trim().equals("8"))
            {
                if(intv < 0)
                {
                    startPayDate = tPayToDate;
                    payDate = PubFun.calDate(tPayToDate, 30, "D", null);
                }
                else if(intv < 15)
                {
                    startPayDate = currDate;
                    payDate = PubFun.calDate(tPayToDate, 30, "D", null);
                }
                else if(intv <= 27)
                {
                    startPayDate = currDate;
                    payDate = PubFun.calDate(tPayToDate, 30, "D", null);
                }
                else
                {
                    startPayDate = currDate;
                    payDate = PubFun.calDate(currDate, 3, "D", null);
                }
            }
            //其他缴费方式
            else
            {
                if(intv < 0)
                {
                    startPayDate = currDate;
                    payDate = PubFun.calDate(tPayToDate, 30, "D", null);
                }
                else if (intv < 15)
                {
                    startPayDate = currDate;
                    payDate = PubFun.calDate(tPayToDate, 30, "D", null);
                }
                else if(intv <= 27)
                {
                    startPayDate = currDate;
                    payDate = PubFun.calDate(tPayToDate, 30, "D", null);
                }
                else
                {
                    startPayDate = currDate;
                    payDate = PubFun.calDate(currDate, 3, "D", null);
                }
            }

        }

        dateMap.put(STARTPAYDATE, startPayDate);
        dateMap.put(PAYDATE, payDate);

        return dateMap;
    }

    /**
     * 得到险种的宽限期
     * @param riskCodes String:续期的险种
     * @return int：宽限期
     */
    private int getGracePeriod(String riskCodes)
    {
        int maxPeriod = Integer.MIN_VALUE;  //最大宽限期

        String sql = "  select max(b.gracePeriod)  "
                     + "from LMRiskApp a, LMRiskPay b "
                     + "where a.riskCode = b.riskCode "
                     + "   and a.riskCode in(" + riskCodes + ") ";
        String period = new ExeSQL().getOneValue(sql);
        if(period != null && !period.equals("") && !period.equals("null"))
        {
            maxPeriod = Integer.parseInt(period);
        }

        return maxPeriod;
    }
    private boolean hasLongRisk(String riskCodes)
    {
        String sql = " select 1 from LMRiskApp where riskperiod = 'L' "
                    + " and riskcode in ("+riskCodes+") ";
       String period = new ExeSQL().getOneValue(sql);
       if(period != null && !period.equals("") && !period.equals("null"))
       {
           return true;
       }
       return false;
    }
    
    private boolean hasTPHRisk(String riskCodes)
    {
    	
        String tphsql = " select 1 from LMRiskApp where taxoptimal = 'Y' "
                    + " and riskcode in ("+riskCodes+") ";
       String taxoptimal = new ExeSQL().getOneValue(tphsql);
       if(taxoptimal != null && !taxoptimal.equals("") && !taxoptimal.equals("null"))
       {
           return true;
       }
       return false;
    }
    //2018-02-24 于坤 新增
    private boolean hasYBKRisk(String riskCodes)
    {
    	
        String tphsql = " select 1 from ldcode where codetype='ybkriskcode' and code in ("+riskCodes+") with ur";
       String taxoptimal = new ExeSQL().getOneValue(tphsql);
       if(taxoptimal != null && !taxoptimal.equals("") && !taxoptimal.equals("null"))
       {
           return true;
       }
       return false;
    }
    public static void main(String[] args)
    {
        CalPayDateBL bl = new CalPayDateBL();

//        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
//        tLCGrpPolDB.setGrpContNo("0000036901");
//
//        if(bl.calPayDateG(tLCGrpPolDB.query(), "4") == null)
//        {
//            System.out.println(bl.mErrors.getErrContent());
//        }

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo("00000077801");

        if(bl.calPayDateP(tLCPolDB.query(), "4") == null)
        {
            System.out.println(bl.mErrors.getErrContent());
        }

    }
}
