package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LGPhoneHastenDB;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJSPayGrpBDB;
import com.sinosoft.lis.db.LJSPayGrpDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.db.LOPRTManagerSubDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LGPhoneHastenSchema;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.schema.LJSPayGrpBSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.taskservice.NextFeeHastenTask;
import com.sinosoft.lis.vschema.LGPhoneHastenSet;
import com.sinosoft.lis.vschema.LJSPayGrpBSet;
import com.sinosoft.lis.vschema.LJSPayGrpSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.vschema.LOPRTManagerSubSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.task.TaskPhoneFinishBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author HZM
 * @version 1.0
 */

public class GrpLJSPlanCancelBL {
	// 错误处理类，每个需要错误处理的类中都放置该类
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private GlobalInput tGI = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate = "";

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	// 修改抽档表状态
	private String mPlanActuSQL = "";

	private String mPlanActuDetailSQL = "";

	// 银行发盘
	private LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();

	// 暂交费表
	private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();

	// 应收总表
	private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

	// 应收总表集
	private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();

	// 应收团体交费表备份表
	private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();

	private LJSPayGrpBSet tLJSPayGrpBSet = new LJSPayGrpBSet();

	private LJSPayGrpBSchema mLJSPayGrpBSchema = new LJSPayGrpBSchema();

	// 应收总表备份表
	private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();

	// 集体保单表
	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

	private MMap mAppMap = new MMap(); // 存储投保人帐户信息

	// 打印管理表
	private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

	private LOPRTManagerSubSet mLOPRTManagerSubSet = new LOPRTManagerSubSet();

	private MMap map = new MMap();

	public GrpLJSPlanCancelBL() {
	}

	public static void main(String[] args) {
		GrpLJSPlanCancelBL GrpDueFeeBL1 = new GrpLJSPlanCancelBL();

		GlobalInput tGt = new GlobalInput();
		tGt.ComCode = "86";
		tGt.Operator = "001";
		tGt.ManageCom = "86";

		LJSPaySchema tLJSPaySchema = new LJSPaySchema();
		tLJSPaySchema.setGetNoticeNo("31000003486");

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("CancelMode", "1");

		VData tVData = new VData();
		tVData.add(tLJSPaySchema);
		tVData.add(tGt);
		tVData.add(tTransferData);
		GrpDueFeeBL1.submitData(tVData, "INSERT");
	}

	/**
	 * 杨红于2005-07-19添加数据校验，判断页面中查询出的信息是否已被催收
	 * 
	 * @return boolean
	 */
	public boolean checkData() {
		ExeSQL tExeSQL = new ExeSQL();
		String count;
		tExeSQL = new ExeSQL();
		count = tExeSQL.getOneValue("SELECT COUNT(*) FROM LJSPay WHERE BankOnTheWayFlag='1'  AND GetNoticeNo='" + mLJSPaySchema.getGetNoticeNo() + "'");

		if (tExeSQL.mErrors.needDealError()) {
			this.mErrors.addOneError("应收记录:" + mLJSPaySchema.getGetNoticeNo() + "查询银行在途标志时错误:" + tExeSQL.mErrors.getFirstError());

			return false;
		}

		if ((count != null) && !count.equals("0")) {
			CError.buildErr(this, "应收记录:" + mLJSPaySchema.getGetNoticeNo() + "作废错误：银行在途，不能作废");
			return false;
		}
		return true;
	}

	// 传输数据的公共方法
	public boolean submitData(VData cInputData, String cOperate) {
		this.mOperate = cOperate;

		if (!getInputData(cInputData)) {
			return false;
		}
		if (!checkData()) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		if (!prepareData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(mInputData, "") == false) {
			System.out.println("delete fail!");
			return false;
		}

		return true;
	}

	/**
	 * Yangh于2005-07-19创建新的dealData的处理逻辑
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		// 产生流水号

		// 查询发盘数据
		String SqlStr = "select a.* from LYSendToBank a ,LJSPay b where a.PolNo= b.OtherNo and b.tempfeetype=2" + " and b.OtherNoType='1'  and b.OtherNo='" + mLJSPaySchema.getOtherNo() + "'";
		LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
		mLYSendToBankSet = tLYSendToBankDB.executeQuery(SqlStr);
		if (mLYSendToBankSet.size() != 0) {
			CError.buildErr(this, "应收记录:" + mLJSPaySchema.getGetNoticeNo() + "作废错误:已经发盘");
			return false;
		}
		// 保单
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mLJSPaySchema.getOtherNo());
		if (!tLCGrpContDB.getInfo()) {
			CError.buildErr(this, "应收记录:" + mLJSPaySchema.getGetNoticeNo() + "作废错误:保单不存在");
			return false;
		}
		mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());

		// 下面查询出符合条件的LJSPayGrp
		String ljsgrpSqlStr = "select * from LJSPayGrp where GetNoticeNo='" + mLJSPaySchema.getGetNoticeNo() + "'";
		LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
		mLJSPayGrpSet = tLJSPayGrpDB.executeQuery(ljsgrpSqlStr);
		if (mLJSPayGrpSet.size() == 0) {
			CError.buildErr(this, "应收记录:" + mLJSPaySchema.getGetNoticeNo() + "作废错误:应收集体缴费表缺少数据");

			return false;
		}

		// 下面查询出符合条件的LJSPayB
		LJSPayBDB tLJSPayBDB = new LJSPayBDB();
		tLJSPayBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
		if (!tLJSPayBDB.getInfo()) {
			CError.buildErr(this, "应收记录:" + mLJSPaySchema.getGetNoticeNo() + "作废错误:应收总表备份表缺少数据");
			return false;
		}
		mLJSPayBSchema.setSchema(tLJSPayBDB.getSchema());

		// 下面查询出符合条件的LJSPayGrpB
		String ljsgrpSqlB = "select * from LJSPayGrpB where GetNoticeNo='" + mLJSPaySchema.getGetNoticeNo() + "'";
		LJSPayGrpBDB tLJSPayGrpBDB = new LJSPayGrpBDB();
		tLJSPayGrpBSet = tLJSPayGrpBDB.executeQuery(ljsgrpSqlB);
		if (tLJSPayGrpBSet.size() == 0) {
			CError.buildErr(this, "应收记录:" + mLJSPaySchema.getGetNoticeNo() + "作废错误:应收集体缴费表备份表缺少数据");
			return false;
		}

		// 未到帐处理
		String SqlFee = "select a.* from LJTempFee a where a.tempfeeno='" + mLJSPaySchema.getGetNoticeNo() + "'";
		LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
		mLJTempFeeSet = tLJTempFeeDB.executeQuery(SqlFee);
		double moneyToAppAcc = 0;
		for (int i = 1; i <= mLJTempFeeSet.size(); i++) {
			mLJTempFeeSet.get(i).setConfFlag("2"); // 财务暂收放入帐户
			//判断财务暂收数据是否到账确认
			if(mLJTempFeeSet.get(i).getEnterAccDate()!=null&&!"".equals(mLJTempFeeSet.get(i).getEnterAccDate())&&!"null".equals(mLJTempFeeSet.get(i).getEnterAccDate())){
				moneyToAppAcc += mLJTempFeeSet.get(i).getPayMoney();
			}else{
				moneyToAppAcc +=0;
			}
		}
		if (Math.abs(moneyToAppAcc - 0) > 0.000001) {
			LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
			tLCAppAccTraceSchema.setCustomerNo(mLJSPaySchema.getAppntNo());
			tLCAppAccTraceSchema.setOtherNo(mLJSPaySchema.getOtherNo());
			tLCAppAccTraceSchema.setOtherType("1");
			tLCAppAccTraceSchema.setMoney(moneyToAppAcc);
			tLCAppAccTraceSchema.setBakNo(mLJSPaySchema.getGetNoticeNo());
			tLCAppAccTraceSchema.setOperator(tGI.Operator);
			AppAcc tAppAcc = new AppAcc();
			MMap tMMap = tAppAcc.accShiftToXSYIJ(tLCAppAccTraceSchema);
			mAppMap.add(tMMap);
		}
		// 抵扣的帐户余额放入帐户
		String sql = "  select sum(sumActuPayMoney) from LJSPayGrp where getNoticeNo = '" + mLJSPaySchema.getGetNoticeNo() + "'  and payType = 'YEL' ";
		String money = new ExeSQL().getOneValue(sql);
		if (!money.equals("") && !money.equals("null")) {
			String sqlAcc = "update LCAppAcc set accGetMoney = accGetMoney + " + -Double.parseDouble(money) + ", " // 与原操作金额正负相反
					+ "  state = '1' " + "where customerNo = '" + mLJSPaySchema.getAppntNo() + "' ";
			mAppMap.put(sqlAcc, "UPDATE");
			sqlAcc = "  delete from LCAppAccTrace " + "where customerNo = '" + mLJSPaySchema.getAppntNo() + "' " + "   and accType = '0' " + "   and otherNo = '" + mLJSPaySchema.getOtherNo() + "' "
					+ "   and otherType = '1' " + "   and state = '0' " + "   and destSource = '02' ";
			mAppMap.put(sqlAcc, "DELETE");
		}

		mLJSPayBSchema.setDealState("2"); // 应收作废
		mLJSPayBSchema.setCancelReason("1"); // 应收作废原因
		mLJSPayBSchema.setModifyDate(CurrentDate);
		mLJSPayBSchema.setModifyTime(CurrentTime);

		for (int j = 1; j <= tLJSPayGrpBSet.size(); j++) {
			mLJSPayGrpBSchema = tLJSPayGrpBSet.get(j);
			mLJSPayGrpBSchema.setDealState("2"); // 应收作废
			mLJSPayGrpBSchema.setCancelReason("1"); // 应收作废
			mLJSPayGrpBSchema.setModifyDate(CurrentDate);
			mLJSPayGrpBSchema.setModifyTime(CurrentTime);
			mLJSPayGrpBSet.add(mLJSPayGrpBSchema);
		}

		LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
		tLOPRTManagerDB.setOtherNo(mLCGrpContSchema.getGrpContNo());
		tLOPRTManagerDB.setOtherNoType("01");
		tLOPRTManagerDB.setCode(PrintManagerBL.CODE_REPAY);
		tLOPRTManagerDB.setStandbyFlag2(mLJSPaySchema.getGetNoticeNo());
		LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();
		if (tLOPRTManagerSet.size() <= 0) {
			CError.buildErr(this, "没有找到续期打印主表数据，不能撤销！");
			return false;
		}

		mLOPRTManagerSchema = tLOPRTManagerSet.get(1);
		LOPRTManagerSubDB tLOPRTManagerSubDB = new LOPRTManagerSubDB();
		tLOPRTManagerSubDB.setPrtSeq(mLOPRTManagerSchema.getPrtSeq());
		mLOPRTManagerSubSet = tLOPRTManagerSubDB.query();
		if (mLOPRTManagerSubSet.size() <= 0) {
			CError.buildErr(this, "没有找到续期打印子表数据，不能撤销！");
			return false;
		}

		return true;
	}

	public boolean prepareData() {
		// 作废状态
		mPlanActuSQL = "update LCGrpPayActu set state='3',modifydate='"+CurrentDate+"',modifytime='"+CurrentTime+"' where getnoticeno='" + mLJSPaySchema.getGetNoticeNo() + "'";
		mPlanActuDetailSQL = "update LCGrpPayActuDetail set state='3',modifydate='"+CurrentDate+"',modifytime='"+CurrentTime+"' where getnoticeno='" + mLJSPaySchema.getGetNoticeNo() + "'";
		map.put(mPlanActuSQL, "UPDATE");
		map.put(mPlanActuDetailSQL, "UPDATE");
		map.put(mLJTempFeeSet, "UPDATE");
		map.add(mAppMap);
		map.put(mLJSPayGrpSet, "DELETE");
		map.put(mLJSPaySchema, "DELETE");
		map.put(mLJSPayBSchema, "UPDATE");
		map.put(mLJSPayGrpBSet, "UPDATE");
		map.put(mLOPRTManagerSchema, "DELETE");
		map.put(mLOPRTManagerSubSet, "DELETE");
		mInputData.add(map);
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData mInputData) {
		tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
		mLJSPaySchema = ((LJSPaySchema) mInputData.getObjectByObjectName("LJSPaySchema", 0));

		if (tGI == null || mLJSPaySchema == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrpDueFeeBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的数据，请您确认!";
			this.mErrors.addOneError(tError);
			return false;
		}
		LJSPayDB tLJSPayDB = new LJSPayDB();
		tLJSPayDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
		if (!tLJSPayDB.getInfo()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrpDueFeeBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的数据，请您确认!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mLJSPaySchema.setSchema(tLJSPayDB.getSchema());
		return true;
	}

}
