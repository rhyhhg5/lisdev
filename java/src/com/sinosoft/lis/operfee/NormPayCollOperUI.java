package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:应交费用类（界面输入）（暂对个人）
 * 从错误对象处理类继承，用来保存错误对象,在每个类中都存在
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class NormPayCollOperUI {

    //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public NormPayCollOperUI() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {

        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "wuser";
        tGI.ManageCom = "86";
        StringBuffer tSBql = new StringBuffer(128);

        String GrpContNo = "0000029001"; //集体保单号码
        System.out.println(GrpContNo);
        LCGrpContSet tLCGrpContSet = new LCGrpContSet(); ;
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo(GrpContNo);
        tSBql = new StringBuffer(128);
        tSBql.append(
                "UPDATE  LCDuty a  SET  (SumPrem,PaytoDate)=")
                .append(
                        "(SELECT sum(SumActuPayMoney)+a.sumprem,min(CurPayToDate)")
                .append(
                        " FROM LJSPayPerson b WHERE b.PolNo=a.PolNo and b.DutyCode=a.DutyCode  ")
                .append(" and b.grpcontno='")
                .append(tLCGrpContSchema.getGrpContNo())
                .append("' )")
                .append(",ModifyDate='")
                .append(PubFun.getCurrentDate())
                .append("',ModifyTime='")
                .append(PubFun.getCurrentTime())
                .append("' where exists(select distinct PolNo from ljspayperson where inputflag='1' and GrpContNo='")
                .append(tLCGrpContSchema.getGrpContNo())
                .append("' and a.polno=polno)");

        ;

         String mSqlPrem = tSBql.toString();



        NormPayCollOperUI NormPayCollOperUI1 = new NormPayCollOperUI();
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.addElement(tLCGrpContSchema);
        NormPayCollOperUI1.submitData(tVData, "VERIFY");

    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        NormPayCollOperBL tNormPayCollOperBL = new NormPayCollOperBL();
        System.out.println("Start NormPayCollOper UI Submit...");
        tNormPayCollOperBL.submitData(mInputData, cOperate);
        //如果有需要处理的错误，则返回
        if (tNormPayCollOperBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tNormPayCollOperBL.mErrors);
            System.out.println("error num=" + mErrors.getErrorCount());
            return false;
        }
        System.out.println("End NormPayCollOper UI Submit...");
        return true;
    }

    private void jbInit() throws Exception {
    }

}

