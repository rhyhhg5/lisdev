package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.xb.TransferCPDataToBP;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

//程序名称：ULIHuanTask.java
//程序功能：万能暂缓缴费批处理
//创建日期：2009-7-21
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class ULIHuanRun {

    CErrors mErrors = new CErrors();
    
    private String CurrentDate = PubFun.getCurrentDate();
    
    private String CurrentTime = PubFun.getCurrentTime();
    
    private String mOperator = "Server";
    
    ExeSQL mExeSQL = new ExeSQL();
    
    SSRS mSSRS = new SSRS();
    
    public ULIHuanRun() 
    {
    }

    public boolean submitData()
    {
    	if(!getData())
    	{
    		return false;
    	}
    	if(!dealData())
    	{
    		return false;
    	}
    	return true;
    }
    
    private boolean getData()
    {
    	StringBuffer sql = new StringBuffer(128);
    	sql.append("select contno from lcpol a where grpcontno = '00000000000000000000' and grppolno = '00000000000000000000' ")
    	   .append("and mainpolno = polno and AppFlag='1'  and ContType='1' and (standbyflag1 <> '")
    	   .append(BQ.ULI_HUAN).append("' or standbyflag1 is null) and (StateFlag is null or StateFlag = '1') ")
    	   .append("and exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4' and subriskflag='M') ")
    	   .append("and PaytoDate<payEndDate and PayIntv>0 ")
    	   .append("and exists (select riskcode from lmrisk WHERE cPayFlag='Y' and riskcode=a.riskcode) ")
    	   .append("and (StopFlag='0' or StopFlag is null) ")
    	   .append("and not exists (select 1 from ldcode where codetype='ULIDEFER' and code=a.riskcode) ")//add by lzy 20150730新产品不需要原始的缓交
    	   .append("and exists (select 1 from ljspay b where otherno = a.contno and paydate + 30 days < current date) with ur ");
    	
    	System.out.println(sql.toString());
    	mSSRS = mExeSQL.execSQL(sql.toString());
    	System.out.println("拢共需要处理"+mSSRS.MaxRow+"条");
    	if(mSSRS.MaxRow == 0)
    	{
    		System.out.println("没有需要缓交状态恢复的万能保单。");
    		return false;
    	}
    	return true;
    }
    private boolean dealData()
    {
    	for(int i=1;i<=mSSRS.MaxRow;i++)
    	{
    		MMap map = new MMap();
    		String tContNo = mSSRS.GetText(i, 1);
    		String delstate = checkLJSPay(tContNo);
    		if("1".equals(delstate))
    		{
    			map.add(updatePol(tContNo));
    		}
    		else if ("2".equals(delstate))
    		{
    			map.add(updatePol(tContNo));
    			map.add(cancelLJSPay(tContNo));
    		}
    		else
    		{
    			System.out.println("保单"+tContNo+"有应收已发盘或生成电话催缴工单或发盘成功，不能动");
    		}
    		submit(map);
    	}
    	return true;
    }
    //检查能否作废应收 1-无应收，直接修改险种状态；2-有应收没发盘，作废应收修改险种状态；
    //3-有应收已发盘或生成电话催缴工单或发盘成功，不能动
    private String checkLJSPay(String aContNo)
    {
    	String flag = "1";
    	ExeSQL tExeSQL = new ExeSQL();
		LJSPayDB tLJSPayDB = new LJSPayDB();
		tLJSPayDB.setOtherNo(aContNo);
		tLJSPayDB.setOtherNoType("2");
		LJSPaySet tLJSPaySet = new LJSPaySet();
		tLJSPaySet = tLJSPayDB.query();
		if(tLJSPaySet.size()==0)
		{
			System.out.println("保单"+aContNo+"无应收");
			flag = "1";
		}
		else
		{
			for(int j=1;j<=tLJSPaySet.size();j++)
			{
				LJSPaySchema tLJSPaySchema = tLJSPaySet.get(j);
				String tBankOnTheWay = tLJSPaySchema.getBankOnTheWayFlag();
				if(tBankOnTheWay==null || tBankOnTheWay.equals("0") || "".equals(tBankOnTheWay) || "null".equals(tBankOnTheWay))
				{
					System.out.println("保单"+aContNo+"有应收没发盘");
					flag = "2";
				}
				else
				{
					System.out.println("保单"+aContNo+"已经发盘或生成电话催缴工单");
					flag = "3";
					break;
				}
				String sql1 = "select count(1) from ljtempfee where tempfeeno = '" + tLJSPaySchema.getGetNoticeNo() + "' with ur";
				String tCount1 = tExeSQL.getOneValue(sql1);
				if(!"0".equals(tCount1))
				{
					System.out.println("保单"+aContNo+"，应收记录：" + tLJSPaySchema.getGetNoticeNo() + "已经收费");
					flag = "3";
					break;
				}
				String sql2 = "select count(1) from ljspaypersonb where paytype='YEL' and getnoticeno = '" + tLJSPaySchema.getGetNoticeNo() + "' with ur";
				String tCount2 = tExeSQL.getOneValue(sql2);
				if(!"0".equals(tCount2))
				{
					System.out.println("保单"+aContNo+"，应收记录：" + tLJSPaySchema.getGetNoticeNo() + "已经部分收费");
					flag = "3";
					break;
				}
				
			}
			String sql = "select count(1) from LYSendToBank where PolNo = '" + aContNo + "' with ur ";
			String tCount = tExeSQL.getOneValue(sql);
			if(!"0".equals(tCount))
			{
				System.out.println("保单"+aContNo+"已经发盘成功");
				flag = "3";
			}
			
			
		}
		return flag;
    }
    
    //作废应收
    private MMap cancelLJSPay(String aContNo)
    {
    	MMap tMMap = new MMap();
    	String sql = "select getnoticeno from ljspay where otherno = '" + aContNo + "' "
    	           + "and (bankonthewayflag = '0' or bankonthewayflag is null or bankonthewayflag = '') with ur";
    	SSRS tSSRS = mExeSQL.execSQL(sql);
    	for(int i=1;i<=tSSRS.MaxRow;i++)
    	{
    		String aGetNoticeNo = tSSRS.GetText(i, 1);
    		String sql1 = "delete from ljspay where getnoticeno = '" + aGetNoticeNo + "' ";
        	String sql2 = "delete from ljspayperson where getnoticeno = '" + aGetNoticeNo + "' ";
        	String sql3 = "update ljspayb set dealstate = '2',cancelreason='1',operator = '" + mOperator 
        	            + "', modifydate = '" + CurrentDate + "', modifytime = '" + CurrentTime 
        	            + "' where getnoticeno = '" + aGetNoticeNo + "' ";
        	String sql4 = "update ljspaypersonb set dealstate = '2',cancelreason='1',operator = '" + mOperator 
                        + "', modifydate = '" + CurrentDate + "', modifytime = '" + CurrentTime 
                        + "' where getnoticeno = '" + aGetNoticeNo + "' ";
        	tMMap.put(sql1, SysConst.DELETE);
        	tMMap.put(sql2, SysConst.DELETE);
        	tMMap.put(sql3, SysConst.UPDATE);
        	tMMap.put(sql4, SysConst.UPDATE);
    	}
    	return tMMap;
    }
    
    //修改险种
    private MMap updatePol(String aContNo)
    {
    	MMap tMMap = new MMap();
    	StringBuffer updateSql = new StringBuffer(128);
		updateSql.append("update lcpol a set standbyflag1 = '").append(BQ.ULI_HUAN)
				 .append("', operator = '").append(mOperator).append("', modifydate = '")
				 .append(CurrentDate).append("', modifytime = '").append(CurrentTime)
		         .append("' where contno = '").append(aContNo).append("' ");
		tMMap.put(updateSql.toString(), SysConst.UPDATE);
    	return tMMap;
    }
    private void submit(MMap map)
    {
    	VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("提交数据失败");
        }
    }
    
    public static void main(String[] args)
    {
        ULIHuanRun x = new ULIHuanRun();
        x.submitData();
        System.out.println("ULIHuanRun完成！");
    }
}
