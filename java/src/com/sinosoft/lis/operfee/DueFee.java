/**
 * <p>Title: IndiDueFee</p>
 * <p>Description: 自动催付程序---个人，首期，团体</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author guoxiang
 * @version 1.0
 */
package com.sinosoft.lis.operfee;

import java.util.*;
import java.lang.*;
import java.text.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
public class DueFee {
        public static void main(String[] args) {
             String pattern="yyyy-MM-dd";
             SimpleDateFormat df = new SimpleDateFormat(pattern);
             Date today = new Date();
             Date finday;
             finday=PubFun.calDate(today,0,"D",new Date());

             String startDate = df.format(finday);
             String endDate = startDate;

             // 输出参数
             CErrors tError = null;
             String FlagStr = "";
             String Content = "";

             GlobalInput tGI = new GlobalInput(); //repair:
             tGI.Operator ="Admin";
             tGI.ComCode  ="86";
             tGI.ManageCom="001";

             //个人催收-----------------------------------------------------------------------
             System.out.println("个人催收开始--------------------------------------------------");
             LCPolSchema  tLCPolSchema = new LCPolSchema();  // 个人保单表
             tLCPolSchema.setGetStartDate(startDate);//将判断条件设置在起领日期字段中
             tLCPolSchema.setPayEndDate(endDate); ////将判断条件设置在终交日期字段中
             VData tVData = new VData();
             tVData.add(tLCPolSchema);
             tVData.add(tGI);
             IndiDueFeeMultiUI tIndiDueFeeMultiUI = new IndiDueFeeMultiUI();
             tIndiDueFeeMultiUI.submitData(tVData,"INSERT");
             if (tIndiDueFeeMultiUI.mErrors.needDealError()){
                 Content="处理完成：";
                 for(int n=0;n<tIndiDueFeeMultiUI.mErrors.getErrorCount();n++){
                      Content=Content+tIndiDueFeeMultiUI.mErrors.getError(n).errorMessage;
                      Content=Content+"|";
                      System.out.println(Content);
                 }
                 FlagStr="Fail";
            }
            else{
                 Content="处理成功完成！";
                 FlagStr="Succ";
           }

           //首期催收----------------------------------------------------------------------------
           System.out.println("首期催收开始--------------------------------------------------");
           LCPolSchema  firstLCPolSchema = new LCPolSchema();  // 个人保单表
           firstLCPolSchema.setGetStartDate(startDate);//将判断条件设置在起领日期字段中
           firstLCPolSchema.setPayEndDate(endDate); ////将判断条件设置在终交日期字段中
           VData firstVData = new VData();
           firstVData.add(firstLCPolSchema);
           firstVData.add(tGI);
           NewIndiDueFeeMultiUI tNewIndiDueFeeMultiUI = new NewIndiDueFeeMultiUI();
           tNewIndiDueFeeMultiUI.submitData(firstVData,"INSERT");
           if (tNewIndiDueFeeMultiUI.mErrors.needDealError()){
             Content="处理完成：";
             for(int n=0;n<tNewIndiDueFeeMultiUI.mErrors.getErrorCount();n++){
                     Content=Content+tNewIndiDueFeeMultiUI.mErrors.getError(n).errorMessage;
                     Content=Content+"|";
                     System.out.println(Content);
            }
            FlagStr="Fail";
          }
          else{
            Content="处理成功完成！";
            FlagStr="Succ";
          }

          //团体催收----------------------------------------------------------------------------
          System.out.println("团体催收开始--------------------------------------------------");
          LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();    //集体保单表
          tLCGrpPolSchema.setFirstPayDate(startDate);
          tLCGrpPolSchema.setPayEndDate(endDate);
          VData grpVData = new VData();
          grpVData.add(tLCGrpPolSchema);
          grpVData.add(tGI);
          GrpDueFeeMultiUI tGrpDueFeeMultiUI = new GrpDueFeeMultiUI();
          tGrpDueFeeMultiUI.submitData(grpVData,"INSERT");
          if (tGrpDueFeeMultiUI.mErrors.needDealError()){
              Content="处理完成：";
              for(int n=0;n<tGrpDueFeeMultiUI.mErrors.getErrorCount();n++){
                   Content=Content+tGrpDueFeeMultiUI.mErrors.getError(n).errorMessage;
                   Content=Content+"|";
                   System.out.println(Content);
              }
            FlagStr="Fail";
          }
          else
          {
            Content="处理成功完成！";
            FlagStr="Succ";
          }
    }
}
