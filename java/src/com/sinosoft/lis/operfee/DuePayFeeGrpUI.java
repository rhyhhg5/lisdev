package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:应交费用类（界面输入）（暂对个人）
 * 从错误对象处理类继承，用来保存错误对象,在每个类中都存在
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class DuePayFeeGrpUI {

  //业务处理相关变量	
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();
      
  public DuePayFeeGrpUI() {
  }
  public static void main(String[] args) {
    DuePayFeeGrpUI DuePayFeeGrpUI1 = new DuePayFeeGrpUI();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    
    DuePayFeeGrpBL tDuePayFeeGrpBL=new DuePayFeeGrpBL();
    System.out.println("Start DuePayFeeGrp UI Submit...");
    tDuePayFeeGrpBL.submitData(mInputData,cOperate);

    System.out.println("End DuePayFeeGrp UI Submit...");

    //如果有需要处理的错误，则返回
    if (tDuePayFeeGrpBL .mErrors .needDealError() )
        this.mErrors .copyAllErrors(tDuePayFeeGrpBL.mErrors ) ;
System.out.println("error num="+mErrors.getErrorCount());  	
    mInputData=null;
    return true;
  }	

} 




































































