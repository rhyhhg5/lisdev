/**
 * <p>Title: IndiFinUrgeVerify</p>
 * <p>Description: 交费自动核销程序</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author guoxiang
 * @version 1.0
 */
package com.sinosoft.lis.operfee;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.*;
import java.text.*;

public class IndiFinUrgeVerify {
      public static void main(String[] args) {
             // 输出参数
             CErrors tError = null;
             String FlagStr = "";
             String Content = "";
             String TempFeeStatus="";
             int recordCount = 0;
             double tempMoney   = 0;
             double sumDueMoney = 0;

             int succNum=0;
             int failNum=0;

             GlobalInput tG = new GlobalInput();
             tG.ManageCom="86";
             tG.Operator="001";
             //对应暂交费表和暂交费分类表，应收总表中的暂交费收据号，通知书号
             String TempFeeNo = "";

             //暂交费表
             LJTempFeeSchema tLJTempFeeSchema;
             LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();

             //应收总表
             LJSPaySchema tLJSPaySchema;
             LJSPaySet tLJSPaySet = new LJSPaySet();
             String pattern="yyyy-MM-dd";
             SimpleDateFormat df = new SimpleDateFormat(pattern);
             Date today = new Date();
             Date finday;
             finday=PubFun.calDate(today,0,"D",new Date());

             String StartDate = df.format(finday);
             String EndDate = StartDate;

             System.out.println("EndDate="+EndDate);
             LJTempFeeSchema qLJTempFeeSchema = new LJTempFeeSchema();
             qLJTempFeeSchema.setEnterAccDate(StartDate);//到帐日期存放起始日期
             qLJTempFeeSchema.setConfDate(EndDate);      //确认日期存放终止日期
             qLJTempFeeSchema.setTempFeeType("2");       //暂交费收据号类型2 ---续期催收交费，录入催收号
             qLJTempFeeSchema.setOtherNoType("0");       //0---个单交费 ---->个单保单号
             qLJTempFeeSchema.setConfFlag("0");          //核销标志为0
             // 准备传输数据 VData
             VData tVData = new VData();
             tVData.addElement(tG);
             tVData.add(qLJTempFeeSchema);
             tVData.addElement(StartDate);
             tVData.addElement(EndDate);
             tVData.addElement(TempFeeStatus);

             System.out.println(tVData.size());

             TempFeeQueryUI qTempFeeQueryUI = new TempFeeQueryUI();
             if(!qTempFeeQueryUI.submitData(tVData,"QUERY")){
                  Content = " 查询暂交费表失败，原因是: " + qTempFeeQueryUI.mErrors.getError(0).errorMessage;
                  FlagStr = "Fail";
              }
             else{      //批量循环开始
                 System.out.println("批量查询");
                 LJTempFeeSet qLJTempFeeSet = new LJTempFeeSet();//保存查询的结果集
                 tVData.clear();
                 tVData = qTempFeeQueryUI.getResult();
                 qLJTempFeeSet.set((LJTempFeeSet)tVData.getObjectByObjectName("LJTempFeeSet",0));
                 recordCount=qLJTempFeeSet.size();
                 for(int n=1;n<=recordCount;n++){
                       tLJTempFeeSchema=(LJTempFeeSchema)qLJTempFeeSet.get(n);


                       tempMoney=tLJTempFeeSchema.getPayMoney();
                       TempFeeNo=tLJTempFeeSchema.getTempFeeNo();
                       System.out.println("TempFeeNo:"+TempFeeNo);
                       //1-查询应收总表,得到总应收金额
                        tVData.clear();
                        tLJSPaySchema = new LJSPaySchema();
                        tLJSPaySet = new LJSPaySet();
                        VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();
                        tLJSPaySchema.setGetNoticeNo(TempFeeNo);
                        tVData.add(tLJSPaySchema);
                        if(!tVerDuePayFeeQueryUI.submitData(tVData,"QUERY")){

                            failNum=failNum+1;
                            continue;
                       }
                        else{ //第二个条件判断
                            tVData.clear();
                            tVData = tVerDuePayFeeQueryUI.getResult();
                            tLJSPaySet.set((LJSPaySet)tVData.getObjectByObjectName("LJSPaySet",0));
                            tLJSPaySchema=(LJSPaySchema)tLJSPaySet.get(1);
                            sumDueMoney = tLJSPaySchema.getSumDuePayMoney();
                            System.out.println("查询应收总表结束");

                           //2-比较两个金额值，相等则核销
                           if(sumDueMoney!=tempMoney){

                                  failNum=failNum+1;
                                  continue;
                           }
                           else{           //第三个条件判断
                                  tVData.clear();
                                  tVData.add(tLJTempFeeSchema);
                                  tVData.add(tLJSPaySchema);
                                  //执行事务操作
                                  IndiFinUrgeVerifyUI tIndiFinUrgeVerifyUI = new IndiFinUrgeVerifyUI();
                                  tIndiFinUrgeVerifyUI.submitData(tVData,"VERIFY");

                                  if (FlagStr==""){
                                            tError = tIndiFinUrgeVerifyUI.mErrors;
                                            if (!tError.needDealError()){
                                                  Content = " 核销事务成功";
                                                  FlagStr = "Succ";
                                             }
                                             else {
                                                 //    	Content = " 核销事务失败，原因是:" + tError.getFirstError();
                                                 //      FlagStr = "Fail";
                                                 failNum=failNum+1;
                                                  continue;
                                             }
                                   }
                           }//对应第三个
                       }//对应第二个
                   }//for循环结束
             }
      }
  }