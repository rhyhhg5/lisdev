package com.sinosoft.lis.taskservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LAPayOwnChargeSchema;
import com.sinosoft.lis.vschema.LAPayOwnChargeSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;




/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class MixedReadChargeFileTask extends TaskThread {
    private String interFilePath = "";
    private String mLogDate="";
    private String mToday = PubFun.getCurrentDate();
    private String mTime = PubFun.getCurrentTime();
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    private String CrmPath = "";
    private String mFileName="";
    private ExeSQL mExeSQL = new ExeSQL();
    private String mBatchNo = PubFun1.CreateMaxNo("CRMBATCHNO", 20);
    private String mIP = "";
	private String mUserName = "";
	private String mPassword = "";
	private int mPort=21;
	private String mTransMode = "";
	private String mURL = "";
	private LAPayOwnChargeSet mLAPayOwnChargeSet = new LAPayOwnChargeSet(); 
	private String mFileDate = PubFun.getCurrentDate();
    public MixedReadChargeFileTask() { 
    }
    public void run() {
    	if (!getInputData())
        {
            return;
        }
 // 进行业务处理
       try{
    	   
    	   if (!readTextDate()){
    		   return;
    	   }
      }catch(Exception ex){
    	  System.out.println("readTextDate END With Exception...");
    	  ex.printStackTrace();
    	  return;
      }finally {

      }
    }
    
    
    private boolean getInputData()
   {
       System.out.println("初始化文件下载路径");
       String tSql = "select sysvarvalue from ldsysvar where sysvar = 'MixedComGetUrl'";
       CrmPath = mExeSQL.getOneValue(tSql);
       
//       CrmPath = "D:\\CRMDate\\";
       return true;
   }

    private boolean readTextDate() throws java.io.FileNotFoundException,
            java.io.IOException {
    	System.out.println("开始读取FTP数据");
//    	RefreshFileList(CrmPath);
		FtpDownFiles();
		System.out.println("文件下载完毕");
    	File dir = new File(CrmPath);
    	File[] files = dir.listFiles();
    	if(files == null)
    	{
    	  System.out.println("文件查询错误");
    	  return false;	
    	}
    	for (int k = 0; k < files.length; k++) 
    	{
    	   String strFileName = files[k].getName();
    	   System.out.println(strFileName);
   // 判断是否数据库中是否已经存在此文件
    	   String judgefileSql ="select distinct 1 from lapayowncharge where F1 ='"+strFileName+"' ";
    	    ExeSQL tExeSQL = new ExeSQL();
            String FileExist = tExeSQL.getOneValue(judgefileSql);
            if("1".equals(FileExist))
            {
             	System.out.println("文件已经在数据库存中存在！");
             	continue;
            }
		//file
//        String mLogName = FindLogFile(CrmPath);
//        if(mLogName.equals(""))
//            return false;
//        interFilePath = CrmPath + mLogName;
//        String mFileName="";
        interFilePath = CrmPath + strFileName;
        String toDelete ="I[F001]:";
        System.out.println("文件路径--+++++++----"+interFilePath+"#@@@#@#@@#@@#@#");
//        interFilePath="d:\\crmdata\\" + mFileName;
//        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(interFilePath), "GBK"));  
//        FileReader fr = new FileReader(interFilePath);
//        String testFilepath="/wasfs/lis_war/printdata/20120905U210102788793885.xml";
////        String test2FilePath="";
//        FileInputStream tfis1 = new FileInputStream(testFilepath);
//        System.out.println("@test1读取成功："+tfis1.read());
//        InputStreamReader tisr1 = new InputStreamReader(tfis1,"GBK");
//        System.out.println("@test2读取成功："+tisr1.read());
//        BufferedReader reader1 = new BufferedReader(tisr1);
//        System.out.println("@@test3文件成功读取"+reader1.read()+"字符@@");
        
        FileInputStream tfis = new FileInputStream(interFilePath);
//        System.out.println("@1读取成功："+tfis.read());
        InputStreamReader tisr = new InputStreamReader(tfis,"GBK");
//        System.out.println("@2读取成功："+tisr.read());
        BufferedReader reader = new BufferedReader(tisr);
//        System.out.println("@@文件成功读取"+reader.read()+"字符@@");
//        BufferedReader reader = new BufferedReader(fr);
        String Line = reader.readLine() ;
        String SingleData[] = new String[29];
        String SingleFactDate[] = new String[29];
        
        int countNum =0;
        while(Line!=null)
        {
          SingleData = null;
          if("I".equals(Line.substring(0,1)))
          {
        	  SingleData = Line.split("\\,");
        	  
        	  System.out.println("here1:"+Line);
        	  if(SingleData.length==30)
        	  {   
        		  int m = 0;int n=0;
        		  String tSumElement="";
        		  String tSubStr ="'";
        		  for (int i = 1; i <= SingleFactDate.length; i++) 
        		  {   
        			  String tSingleDataElement =SingleData[i];
        			  if(stringsub(tSingleDataElement,tSubStr)!=1)
        			  {
        				  m++;
        				  System.out.println("dfafafafa");
        				  
        			  }else{
        				  
        				  
        				  tSumElement+=tSingleDataElement;
        				  n++;
        			  }
					
				  }
        		  String SingleDatas[] = new String[SingleData.length-n+1];
        		  int c=0;
        		  String tStName="";
        		  for (int i = 0; i < SingleData.length; i++) 
        		  {

        		            if(stringsub(SingleData[i],tSubStr)!=1){ // 假如不等于0

        		            	SingleDatas[c]=SingleData[i]; // 赋值给新的数组
        		                
        		            	c++;
        		            	

        		            }else
        		            {
        		            	tStName+=SingleData[i];
        		            	SingleDatas[c]= tStName;
        		            	if(SingleDatas[c].endsWith("'"))
        		            	{
        		            		c++;
        		            	}
        		            }
//        		            if(l=c){}

				}
        		 
        		  for(int j=0;j<29;j++){
            		  if(SingleDatas[j].endsWith("'")){
            			  SingleFactDate[j] = (String) SingleDatas[j].replaceAll("'", "");
            			  System.out.println(SingleFactDate[j]);
            		  }
            		  else
            		  {
            			  SingleFactDate[j] = (String) SingleDatas[j];
            			  System.out.println(SingleFactDate[j]);
            		  }
            	  }
        	  }else{
        	  
        	  
        	  for(int j=0;j<29;j++){
        		  if(SingleData[j].endsWith("'")){
        			  SingleFactDate[j] = (String) SingleData[j].replaceAll("'", "");
        			  System.out.println(SingleFactDate[j]);
        		  }
        		  else
        		  {
        			  SingleFactDate[j] = (String) SingleData[j];
        			  System.out.println(SingleFactDate[j]);
        		  }
        	  }
        	  }
        	  LAPayOwnChargeSchema mLAPayOwnChargeSchema = new LAPayOwnChargeSchema();
        	  mLAPayOwnChargeSchema.setContNo(SingleFactDate[0].replace(toDelete,""));
        	  mLAPayOwnChargeSchema.setF2(SingleFactDate[1]);
        	  
        	  mLAPayOwnChargeSchema.setCompCode(SingleFactDate[2]);
        	  mLAPayOwnChargeSchema.setCompName(SingleFactDate[3]);
        	  mLAPayOwnChargeSchema.setCValiDate(SingleFactDate[4]);
        	  mLAPayOwnChargeSchema.setSignDate(SingleFactDate[5]);
        	  mLAPayOwnChargeSchema.setRiskCode(SingleFactDate[6]);
        	  mLAPayOwnChargeSchema.setRiskName(SingleFactDate[7]);
        	  mLAPayOwnChargeSchema.setActNo(SingleFactDate[8]);
        	  mLAPayOwnChargeSchema.setTmakeDate(SingleFactDate[9]);
        	  mLAPayOwnChargeSchema.setFeePayTypeCode(SingleFactDate[10]);
        	  mLAPayOwnChargeSchema.setFeePayTypeName(SingleFactDate[11]);
        	  mLAPayOwnChargeSchema.setCharge(SingleFactDate[12]);
        	  mLAPayOwnChargeSchema.setChargeRate(SingleFactDate[13]);
        	  mLAPayOwnChargeSchema.setActCode(SingleFactDate[14]);
        	  mLAPayOwnChargeSchema.setActName(SingleFactDate[15]);
        	  mLAPayOwnChargeSchema.setOrgBelgCode(SingleFactDate[16]);
        	  mLAPayOwnChargeSchema.setOrgBelgName(SingleFactDate[17]);
        	  mLAPayOwnChargeSchema.setOrgCrsCode(SingleFactDate[18]);
        	  mLAPayOwnChargeSchema.setOrgCrsName(SingleFactDate[19]);
        	  mLAPayOwnChargeSchema.setOrgCrsCompCode(SingleFactDate[20]);
        	  mLAPayOwnChargeSchema.setPolSalesCode(SingleFactDate[21]);
        	  mLAPayOwnChargeSchema.setPolSalesName(SingleFactDate[22]);
        	  mLAPayOwnChargeSchema.setCrsSalesCode(SingleFactDate[23]);
        	  mLAPayOwnChargeSchema.setCrsSalesName(SingleFactDate[24]);
        	  mLAPayOwnChargeSchema.setCstName(SingleFactDate[25]);
        	  mLAPayOwnChargeSchema.setInsrTarg(SingleFactDate[26]);
        	  mLAPayOwnChargeSchema.setPrem(SingleFactDate[27]);
        	  mLAPayOwnChargeSchema.setDateSend(SingleFactDate[28]);
        	  mLAPayOwnChargeSchema.setOperator("001");
        	  mLAPayOwnChargeSchema.setMakeDate(this.mToday);
        	  mLAPayOwnChargeSchema.setMakeTime(this.mTime);
        	  mLAPayOwnChargeSchema.setModifyDate(this.mToday);
        	  mLAPayOwnChargeSchema.setModifyTime(this.mTime);
        	  mLAPayOwnChargeSchema.setF1(strFileName);
        	  mLAPayOwnChargeSet.add(mLAPayOwnChargeSchema);
        	  
        	  if(countNum==5000)
        	  {
        		  mMap.put(mLAPayOwnChargeSet, "INSERT");
        	        if (!SubmitMap() )
        	        {
        	        	return false;
        	        }
        	        mLAPayOwnChargeSet.clear();
        	        mMap.keySet().clear();
        	        countNum=0;
        	  }
        	  countNum++;
          }
          Line = reader.readLine();
        }
        mMap.put(mLAPayOwnChargeSet, "INSERT");
        if (!SubmitMap() )
        {
        	return false;
        }
         
        mLAPayOwnChargeSet.clear();
        mMap.keySet().clear();
        RefreshFileList(CrmPath);
//          tfis1.close();
//          tisr1.close();
//          reader1.close();
          tfis.close();
          tisr.close();
          reader.close();
//        fr.close();
//        mLAPayOwnChargeSet= new LAPayOwnChargeSet();
//        for (int i=0;i<intFileNum;i++)
//        {
//            SingleData = null;
//            if(i%2 != 0 ){
//            SingleData = ((String)tResult.get(i)).split("\\,\\'");
//            System.out.println("here1:"+tResult.get(i));
//            for(int j=0;j<28;j++){
//            	if(SingleData[j].endsWith("'")){
//              		SingleFactDate[j] = (String) SingleData[j].replace("'", "");
//              		System.out.println(SingleFactDate[j]);
//              	}
//            }
//            LAPayOwnChargeSchema mLAPayOwnChargeSchema = new LAPayOwnChargeSchema();
//            mLAPayOwnChargeSchema.setContNo(SingleFactDate[0].replace(toDelete,""));
//            mLAPayOwnChargeSchema.setCompCode(SingleFactDate[1]);
//            mLAPayOwnChargeSchema.setCompName(SingleFactDate[2]);
//            mLAPayOwnChargeSchema.setCValiDate(SingleFactDate[3]);
//            mLAPayOwnChargeSchema.setSignDate(SingleFactDate[4]);
//            mLAPayOwnChargeSchema.setRiskCode(SingleFactDate[5]);
//            mLAPayOwnChargeSchema.setRiskName(SingleFactDate[6]);
//            mLAPayOwnChargeSchema.setActNo(SingleFactDate[7]);
//            mLAPayOwnChargeSchema.setTmakeDate(SingleFactDate[8]);
//            mLAPayOwnChargeSchema.setFeePayTypeCode(SingleFactDate[9]);
//            mLAPayOwnChargeSchema.setFeePayTypeName(SingleFactDate[10]);
//            mLAPayOwnChargeSchema.setCharge(SingleFactDate[11]);
//            mLAPayOwnChargeSchema.setChargeRate(SingleFactDate[12]);
//            mLAPayOwnChargeSchema.setActCode(SingleFactDate[13]);
//            mLAPayOwnChargeSchema.setActName(SingleFactDate[14]);
//            mLAPayOwnChargeSchema.setOrgBelgCode(SingleFactDate[15]);
//            mLAPayOwnChargeSchema.setOrgBelgName(SingleFactDate[16]);
//            mLAPayOwnChargeSchema.setOrgCrsCode(SingleFactDate[17]);
//            mLAPayOwnChargeSchema.setOrgCrsName(SingleFactDate[18]);
//            mLAPayOwnChargeSchema.setOrgCrsCompCode(SingleFactDate[19]);
//            mLAPayOwnChargeSchema.setPolSalesCode(SingleFactDate[20]);
//            mLAPayOwnChargeSchema.setPolSalesName(SingleFactDate[21]);
//            mLAPayOwnChargeSchema.setCrsSalesCode(SingleFactDate[22]);
//            mLAPayOwnChargeSchema.setCrsSalesName(SingleFactDate[23]);
//            mLAPayOwnChargeSchema.setCstName(SingleFactDate[24]);
//            mLAPayOwnChargeSchema.setInsrTarg(SingleFactDate[25]);
//            mLAPayOwnChargeSchema.setPrem(SingleFactDate[26]);
//            mLAPayOwnChargeSchema.setDateSend(SingleFactDate[27]);
//            mLAPayOwnChargeSchema.setOperator("001");
//            mLAPayOwnChargeSchema.setMakeDate(this.mToday);
//            mLAPayOwnChargeSchema.setMakeTime(this.mTime);
//            mLAPayOwnChargeSchema.setModifyDate(this.mToday);
//            mLAPayOwnChargeSchema.setModifyTime(this.mTime);
//            mLAPayOwnChargeSet.add(mLAPayOwnChargeSchema);
//          }
//        }
//        mMap = new MMap();
//        mMap.put(mLAPayOwnChargeSet, "DELETE&INSERT");
//        if (!SubmitMap() )
//              return false;
    	}
    	return true;
    }


/*数据处理*/
private boolean SubmitMap()
{
    PubSubmit tPubSubmit = new PubSubmit();
    mResult= new VData();
    mResult.add(mMap);
    if (!tPubSubmit.submitData(mResult, ""))
    {
        System.out.println("提交数据失败！");
        return false;
    }
//    System.gc();
    return true;
}
public String GetBatchNo()
{
    return mBatchNo;
}
private String FindLogFile(String Path)
{    
	
     File  dir = new File(Path);
     Filter aFilter = new Filter("log");
     String filename ="";
     if (dir.list(aFilter).length == 0)
          return filename;
     String  files[] =  dir.list(aFilter);
     if (!StrTool.cTrim(files[0]).equals(""))
            filename = files[0];
     System.out.println("filename "+filename);
     return filename;
}

private boolean FtpDownFiles(){
	
	String tFtpSQL = "select code,codename from ldcode where codetype = 'jtjxftpnl' ";
	SSRS tSSRS = new ExeSQL().execSQL(tFtpSQL);
	if (tSSRS == null || tSSRS.MaxRow < 1) {
		System.out.println("没有找到集团交叉ftp配置信息！");
		return false;
	}
	for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
		if ("IP".equals(tSSRS.GetText(i, 1))) {
			mIP = tSSRS.GetText(i, 2);
		} else if ("UserName".equals(tSSRS.GetText(i, 1))) {
			mUserName = tSSRS.GetText(i, 2);
		} else if ("Password".equals(tSSRS.GetText(i, 1))) {
			mPassword = tSSRS.GetText(i, 2);
		} else if ("Port".equals(tSSRS.GetText(i, 1))) {
			mPort = Integer.parseInt(tSSRS.GetText(i, 2));
		} else if ("TransMode".equals(tSSRS.GetText(i, 1))) {
			mTransMode = tSSRS.GetText(i, 2);
		}
	}

	FTPTool tFTPTool = new FTPTool(mIP, mUserName, mPassword, mPort,
			mTransMode);
	try {
		if (!tFTPTool.loginFTP()) {
			System.out.println(tFTPTool.getErrContent(1));
		} else {
//			String tFileImportPath = "/gather/gather/mqm/sales/rec/mainData/";//ftp上存放文件的目录
//			String tFileImportPath = "D:\\CRMDate\\";//ftp上存放文件的目录
			
			String tBeForDate = getSpecifiedDayBefore(mFileDate).replace("-", "");
			
		    tFTPTool.List("/gather/gather/mqm/sales/rec/mainData/");
			if(tFTPTool.arFiles.size()>0)
			{
				for (int i = 0; i < tFTPTool.arFiles.size(); i++) 
				{
					 String tFileName = (String) tFTPTool.arFiles.get(i);
					 System.out.println("@@@@@@"+tFileName+"$$$$$$$$");
					 if(tFileName.substring(10, 18).equals(tBeForDate))
						{
							tFTPTool.downloadFile("/gather/gather/mqm/sales/rec/mainData/",
									CrmPath,tFileName);
							System.out.println("文件下载成功了！");
						}
				}
			}		
//	        String tFileImportPathBackUp = "/gather/gather/mqm/sales/"+PubFun.getCurrentDate()+"/";//FTP备份目录 
//	        if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
//            	System.out.println("^&&&&&&&&新建目录已存在&&&&&&&@@@@@@@@@");
//            }
//	        System.out.println("OK");
        	
//            String[] tPath = tFTPTool.downloadAllDirectory(tFileImportPath, tFileImportPathBackUp);
//            System.out.println(tPath.length);
//	        tFTPTool.download("/gather/gather/mqm/sales/rec/mainData/",
//					CrmPath);
//			System.out.println("文件下载成功了！");         
			
//					tFTPTool.download("/gather/gather/mqm/sales/rec/mainData/",
//							CrmPath);
//					System.out.println("文件下载成功了！");
					
				
			
		}
	} 
	catch (SocketException ex) {
		ex.printStackTrace();
	}
	catch (IOException ex) {
		ex.printStackTrace();
	} finally 
	{
		tFTPTool.logoutFTP();
	}
       return true;
}

 private boolean  RefreshFileList(String StrPath){
	File dir = new File(StrPath);
	File[] files = dir.listFiles();
	if(files == null)
	{
	  return false;	
	}
	for (int i = 0; i < files.length; i++) {              
        if (files[i].isDirectory()) {                  
            RefreshFileList(files[i].getAbsolutePath());              
        }else {                  
            String strFileName = files[i].getAbsolutePath();  
            if(strFileName.endsWith("000085.txt")){  
                System.out.println("正在删除：" + strFileName);   
                files[i].delete();  
            }  
        }
	}
	return true;
 }

    public static void main(String[] args) throws Exception
    {
    	MixedReadChargeFileTask mMixedReadChargeFileTask = new MixedReadChargeFileTask();
    	mMixedReadChargeFileTask.run();
//    	 String mFileDate = PubFun.getCurrentDate();
////    	String tt = mFileDate.replace("-", "");
//    	String tt = mMixedReadChargeFileTask.getSpecifiedDayBefore(mFileDate);
//    	String CrmPath = "D:\\CRMDate\\";
//    	System.out.println(tt);
//    	mMixedReadChargeFileTask.RefreshFileList(CrmPath);
    	
    
        return;

    }
    /** 
    * 获得指定日期的前一天 
    * @param specifiedDay 
    * @return 
    * @throws Exception 
    */ 
    public static String getSpecifiedDayBefore(String specifiedDay){ 
    //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd"); 
    Calendar c = Calendar.getInstance(); 
    Date date=null; 

    try {
		date = new SimpleDateFormat("yy-MM-dd").parse(specifiedDay);
	} catch (java.text.ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
    
    c.setTime(date); 
    int day=c.get(Calendar.DATE); 
    c.set(Calendar.DATE,day-1); 

    String dayBefore=new SimpleDateFormat("yyyy-MM-dd").format(c.getTime()); 
    return dayBefore; 
    }
    
    public static int stringsub(String str,String substr)
	{
		int index=0;
		int count=0;
		int fromindex=0;
		while((index=str.indexOf(substr,fromindex))!=-1)
		{
			//str=str.substring(index+substr.length());
			fromindex=index+substr.length();
			count++;
		}
		return count;
	}


    
    
}
