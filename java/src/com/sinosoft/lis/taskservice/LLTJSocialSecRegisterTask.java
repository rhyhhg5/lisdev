package com.sinosoft.lis.taskservice;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.encoding.XMLType;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.axis2.AxisFault;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.DOMBuilder;
import org.jdom.output.DOMOutputter;
import org.xml.sax.InputSource;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.yibaotong.DateUtil;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.lis.yibaotong.LLTJSocialSecurityRegister;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *    天津社保批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2014</p>
 *
 * <p>Company: </p>
 *
 * @author zjd
 * @version 1.0
 */
public class LLTJSocialSecRegisterTask extends TaskThread
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData mInputData = new VData();

    // 统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

    private MMap map = new MMap();

    private String mFilePath = ""; //批处理文件的路径

    private String mFileInPath = ""; //批处理输入文件的路径

    private String mTaskDate = ""; //批处理日期

    private String mDtype = "";//保存类型

    private ExeSQL tExeSQL = new ExeSQL();
    
    private Document mResultXml;

    public LLTJSocialSecRegisterTask()
    {
    }

    public static String TaskCode = "";

    private DocumentBuilder cDocbuilder = null;
    {
        DocumentBuilderFactory tDocFactory = DocumentBuilderFactory.newInstance();
        try
        {
            cDocbuilder = tDocFactory.newDocumentBuilder();
        }
        catch (ParserConfigurationException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * @throws  
     * */
    public void run()
    {
        try
        {
            if (runstart())
            {
                return;
            }
        }
        catch (AxisFault e)
        {
            // TODO 自动生成 catch 块
            e.printStackTrace();
        }
    }

    //批处理开始执行
    public boolean runstart() throws AxisFault
    {
        System.out.println("LLTJSocialSecRegisterTask  runstart()---批处理开始执行:" + PubFun.getCurrentDate() + " "
                + PubFun.getCurrentTime());

        //获取配置信息
        String tSql = "select codename,codealias from ldcode where codetype='TJSocialSec' and code='2014TJSS' ";
        SSRS tSSRS = new SSRS();

        tSSRS = tExeSQL.execSQL(tSql);

        if (tSSRS != null && tSSRS.MaxRow > 0)
        {

            mFilePath = tSSRS.GetText(1, 2);
            mTaskDate = tSSRS.GetText(1, 1);
            mDtype = "InNoStd";
            mFileInPath = generateFilePath(mFilePath, mDtype, mTaskDate);
            String[] Files = getFiles(mFileInPath);

            System.out.println(mFileInPath);
            if (Files != null && Files.length > 0)
            {
                for (int i = 0; i < Files.length; i++)
                {
                    System.out.println("报文名称:" + Files[i]);
                    String tFilePath = mFileInPath + Files[i];
                    try
                    {
                        Document tOutXmlDoc;
                        Document tInXmlDoc = JdomUtil.build(new FileInputStream(tFilePath), "GBK");
                        LLTJSocialSecurityRegister tLLTJSocialSecurityRegister = new LLTJSocialSecurityRegister();
                        System.out.println("打印传入报文============");
                        JdomUtil.print(tInXmlDoc.getRootElement());
                        //Document 之间转换
                        tOutXmlDoc = tLLTJSocialSecurityRegister.service(tInXmlDoc);
                        //保存返回的报文
                        save(tOutXmlDoc, Files[i], "OutNoStd", mFilePath, mTaskDate);
                        try
                        {
                            org.w3c.dom.Document tParseOutXml = JDomDoc2Dom(tOutXmlDoc);
                            //调用接口返回报文
                            mResultXml=callService(tParseOutXml);

                            if(mResultXml!=null){
                                save(mResultXml, "Result"+Files[i], "OutNoStd", mFilePath, mTaskDate);
                            }else{
                                System.out.println("天津返回报文为空!");
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            System.out.println("Document之间转化出错!");
                        }

                        System.out.println("打印返回报文============");
                        JdomUtil.print(tOutXmlDoc.getRootElement());

                    }
                    catch (FileNotFoundException e)
                    {
                        e.printStackTrace();
                        System.out.println("文件未找到!");
                        continue;
                    }

                }

            }
            else
            {
                System.out.println("没有要处理的报文!");
                //              更新日期
                if (!updaterundate())
                {
                    System.out.println("更新批处理时间出错!");
                    return false;
                }
                return false;
            }

        }
        else
        {
            System.out.println("获取配置信息出错!");
            return false;
        }

        //更新日期
        if (!updaterundate())
        {
            System.out.println("更新批处理时间出错!");
            return false;
        }

        System.out.println("LLTJSocialSecRegisterTask  runstart()---批处理执行完毕:" + PubFun.getCurrentDate() + " "
                + PubFun.getCurrentTime());

        return true;
    }

    //获取保存路径
    public static String generateFilePath(String FilePath, String pType, String TaskDate)
    {
        StringBuffer sFilePath = new StringBuffer();
        sFilePath.append(FilePath).append('/').append("FileContent").append('/')
                .append(DateUtil.getCurrentDate("yyyy")).append('/').append(DateUtil.getCurrentDate("yyyyMM")).append(
                        '/').append(TaskDate).append('/').append(pType).append('/');
        return sFilePath.toString();
    }

    //获取报文列表
    public String[] getFiles(String mFilePath)
    {
        String[] Files;
        File file = new File(mFilePath);
        Files = file.list();
        return Files;
    }

    /**
     * 保存返回的报文
     * @param string 
     * @param taskDate 
     * @param filePath 
     */
    public void save(Document pXmlDoc, String pName, String pType, String FilePath, String TaskDate)
    {
        StringBuffer mFilePath = new StringBuffer();
        mFilePath.append(generateFilePath(FilePath, pType, TaskDate));
        File mFileDir = new File(mFilePath.toString());
        if (!mFileDir.exists())
        {
            if (!mFileDir.mkdirs())
            {
                System.err.println("创建目录失败！" + mFileDir.getPath());
            }
        }

        try
        {
            FileOutputStream tFos = new FileOutputStream(mFilePath.toString() + pName);
            JdomUtil.output(pXmlDoc, tFos);
            tFos.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 更新批处理日期值
     * @return
     */
    public boolean updaterundate()
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.replace("-", "");
        System.out.println("===" + tDate);
        String tSql = "update ldcode set codename='" + tDate + "' where codetype='TJSocialSec' and code='2014TJSS'";
        boolean tResult = tExeSQL.execUpdateSQL(tSql);
        return tResult;
    }

    /**
     * 1、客户端准备数据;2、调用WebService服务;3、获取服务端处理结果数据
     *
     * @param pReader
     * @param pServiceURL
     * @return org.w3c.dom.Document
     * @throws Exception
     */
    public Document callService(org.w3c.dom.Document mInXmlDoc) throws Exception
    {

        long mOldTimeMillis = System.currentTimeMillis();
        long mCurTimeMillis = mOldTimeMillis;

        SOAPBodyElement[] mSOAPBodyElements = new SOAPBodyElement[1];
        mSOAPBodyElements[0] = new SOAPBodyElement(mInXmlDoc.getDocumentElement());

        String xmlstr = convertStr(mInXmlDoc);

        System.out.println("客户端准备数据完毕！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");

        System.out.println("发送的报文:" + xmlstr);
        Service mService = new Service();
        Call mCall = (Call) mService.createCall();
        //获取地址
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("LLTJSSPath");

        if (tLDSysVarDB.getInfo())
        {
            mCall.setTargetEndpointAddress(tLDSysVarDB.getSchema().getSysVarValue());
        }
        else
        {
            System.out.println("查询接口地址出错!");
        }
        System.out.println("开始调用WebService服务！" + tLDSysVarDB.getSchema().getSysVarValue());

        mCall.setTargetEndpointAddress(tLDSysVarDB.getSchema().getSysVarValue());
        mCall
                .setOperationName(new javax.xml.namespace.QName("http://service.webservice.yinhai.com/",
                        "syncInfomation"));
        mCall.addParameter("xmlstr", XMLType.XSD_STRING, ParameterMode.IN);
        //mCall.setReturnType(XMLType.XSD_STRING);
        String mReElements = (String) mCall.invoke(new Object[] { xmlstr });
        //Vector mReElements = (Vector) mCall.invoke(mSOAPBodyElements);

        System.out.println("调用WebService服务成功！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");

        System.out.println("返回的报文:" + mReElements);
        //SOAPBodyElement mReSOAPBodyElement = (SOAPBodyElement) mReElements.get(0);
        //Document tResultDom=convert(mReSOAPBodyElement.getAsDocument());
        Document tResultDom = convert(convertXml(mReElements));
        return tResultDom;
    }

    /**
     * JDOM2DOM
     * Document之间转换
     */
    public org.w3c.dom.Document JDomDoc2Dom(org.jdom.Document jdomDoc) throws Exception
    {
        DOMOutputter outputter = new DOMOutputter();
        return outputter.output(jdomDoc);
    }

    /**
     * DOM2JDOM
     * @param domDoc
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static org.jdom.Document convert(org.w3c.dom.Document domDoc) throws Exception
    {
        // Create new DOMBuilder, using default parser
        DOMBuilder builder = new DOMBuilder();
        org.jdom.Document jdomDoc = builder.build(domDoc);
        return jdomDoc;
    }

    /**
     * DOM2String
     * @param domDoc
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static String convertStr(org.w3c.dom.Document domDoc) throws Exception
    {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        t.setOutputProperty("encoding", "GBK");//解决中文问题
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        t.transform(new DOMSource(domDoc), new StreamResult(bos));
        String xmlStr = bos.toString();
        return xmlStr;
    }

    /**
     * String2DOM
     * @param domstr
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public org.w3c.dom.Document convertXml(String xmlstr) throws Exception
    {
        StringReader sr = new StringReader(xmlstr);
        InputSource is = new InputSource(sr);
        org.w3c.dom.Document mInXmlDoc = cDocbuilder.parse(is);
        return mInXmlDoc;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO 自动生成方法存根
        LLTJSocialSecRegisterTask tLLTJSocialSecRegisterTask = new LLTJSocialSecRegisterTask();

        // boolean t1=t.updaterundate();
        try
        {
            //boolean t1=t.runstart();
            Document tInXmlDoc = JdomUtil.build(new FileInputStream("E:/TJ0586120000000000011100000555_182536.xml"),
                    "GBK");
//            String xmlstr = convertStr(tLLTJSocialSecRegisterTask.JDomDoc2Dom(tInXmlDoc));
//            org.w3c.dom.Document tInXmlDoc2 = tLLTJSocialSecRegisterTask.convertXml(xmlstr);

            System.out.println("打印传入报文============");
//            JdomUtil.print(convert(tInXmlDoc2).getRootElement());
            JdomUtil.print(tInXmlDoc.getRootElement());
            tLLTJSocialSecRegisterTask.callService(tLLTJSocialSecRegisterTask.JDomDoc2Dom(tInXmlDoc));
        }
        catch (Exception e)
        {
            // TODO 自动生成 catch 块
            e.printStackTrace();
        }

    }
}
