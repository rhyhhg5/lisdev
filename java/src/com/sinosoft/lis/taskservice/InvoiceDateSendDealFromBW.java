package com.sinosoft.lis.taskservice;

import java.util.Set;

import com.sinosoft.lis.db.LYOutPayDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.ygz.InvoicePushBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class InvoiceDateSendDealFromBW extends TaskThread{

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	public InvoiceDateSendDealFromBW(){}

	//private SSRS mSSRS;
	private LYOutPayDetailSet tLYOutPayDetailSet = null;
	private LYOutPayDetailSet mLYOutPayDetailSet = null;
	private GlobalInput mGlobalInput = new GlobalInput();
	private MMap mMap = new MMap();
	private VData mVData = new VData();

	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	private int sysCount = 5000;//默认每五千条数据发送一次请求
	public void run() {
		System.out.println("---InvoiceDateSend开始---");
		submitData();
		System.out.println("---InvoiceDateSend正常结束---");
	}

	private boolean submitData(){

		tLYOutPayDetailSet = getData();

		long startDealData = System.currentTimeMillis();
		if(!dealData()){
			buildError("dealData","数据准备失败");
			return false;
		}
		long endDealData = System.currentTimeMillis();
		System.out.println("========== InvoiceDateSend 数据准备dealData -耗时： "+((endDealData - startDealData)/1000/60)+"分钟");
		if(!submit()){
			return false;
		}

		return true;
	}

	private boolean submit(){

		mVData.add(mMap);
		PubSubmit p = new PubSubmit();
		if (!p.submitData(mVData, SysConst.INSERT)){
			System.out.println("提交数据失败");
			buildError("submitData", "提交数据失败");
			return false;
		}

		return true;

	}

	private boolean dealData(){
		SSRS tSSRS = new SSRS();
		tSSRS = getXmDATA();
		int count=0;
		int count1 = 0 ;
		mLYOutPayDetailSet = new LYOutPayDetailSet();
		for(int i=1; i<=tLYOutPayDetailSet.size(); i++){
			mLYOutPayDetailSet.add(tLYOutPayDetailSet.get(i));
			count ++;
			if(sysCount>mLYOutPayDetailSet.size()){
				if(count==tLYOutPayDetailSet.size()){
					count = 0;
					InvoicePushBL tInvoicePushBL = new InvoicePushBL();
					VData tVData = new VData();
					tVData.add(mLYOutPayDetailSet);
					tVData.add(tSSRS);
					tVData.add(mGlobalInput);
					if(!tInvoicePushBL.getSubmit(tVData, "")){
						buildError("callService", "调用OutPayUploadBL接口失败。");
						return false;
					}
					mMap = tInvoicePushBL.getResult();
					//mVData.add(mMap);
					MMap sMap = new MMap();
					 LYOutPayDetailSet m2OutPayDetailSet = null;
					 Set set = mMap.keySet();
			         for (int j = 0; j < set.size(); j++)
			         {
			             //获取操作对象Schema或Set或SQL
			             //o = iterator.next();
			        	 Object  o = mMap.getOrder().get(String.valueOf(j + 1));
			        	 m2OutPayDetailSet = (LYOutPayDetailSet) o;
			        	 System.out.println(m2OutPayDetailSet.size());
					
			         }
			         
			         for(int k=1;k<=m2OutPayDetailSet.size();k++){
			        	 LYOutPayDetailSchema	 sLYOutPayDetailSchema = m2OutPayDetailSet.get(k); 
			        	 sMap.put("update tmpjszj set querycode='fpts002' where policyno ='"+sLYOutPayDetailSchema.getBusiNo()+"'", "UPDATE");
			         }
			        mVData.add(sMap);
					PubSubmit p = new PubSubmit();
					if (!p.submitData(mVData, SysConst.INSERT)){
						System.out.println("提交数据失败");
						buildError("submitData", "提交数据失败");
						return false;
					}
					mMap = new MMap();
					sMap = new MMap();
					mVData.clear();
					mLYOutPayDetailSet=new LYOutPayDetailSet();

				}

			}else{
				int allCount = mLYOutPayDetailSet.size()/sysCount;
				int reCount = mLYOutPayDetailSet.size()%sysCount;
				if(count1==allCount&&reCount==count){
					count1 = 0;
					count = 0;
					InvoicePushBL tInvoicePushBL = new InvoicePushBL();
					VData tVData = new VData();
					tVData.add(mLYOutPayDetailSet);
					tVData.add(tSSRS);
					tVData.add(mGlobalInput);
					if(!tInvoicePushBL.getSubmit(tVData, "")){
						buildError("callService", "调用OutPayUploadBL接口失败。");
						return false;
					}
					mMap = tInvoicePushBL.getResult();
					MMap sMap = new MMap();
					 LYOutPayDetailSet m2OutPayDetailSet = null;
					 Set set = mMap.keySet();
			         for (int j = 0; j < set.size(); j++)
			         {
			             //获取操作对象Schema或Set或SQL
			             //o = iterator.next();
			        	 Object  o = mMap.getOrder().get(String.valueOf(j + 1));
			        	 m2OutPayDetailSet = (LYOutPayDetailSet) o;
			        	 System.out.println(m2OutPayDetailSet.size());
					
			         }
			         
			         for(int k=1;k<=m2OutPayDetailSet.size();k++){
			        	 LYOutPayDetailSchema	 sLYOutPayDetailSchema = m2OutPayDetailSet.get(k); 
			        	 sMap.put("update tmpjszj set querycode='fpts002' where policyno ='"+sLYOutPayDetailSchema.getBusiNo()+"'", "UPDATE");
			         }

			         mVData.add(sMap);
					//mVData.add(mMap);
					PubSubmit p = new PubSubmit();
					if (!p.submitData(mVData, SysConst.INSERT)){
						System.out.println("提交数据失败");
						buildError("submitData", "提交数据失败");
						return false;
					}
					mMap = new MMap();
					sMap = new MMap();
					mVData.clear();
					mLYOutPayDetailSet=new LYOutPayDetailSet();
				}
				if(count >= sysCount){
					count1 ++;
					count = 0;
					InvoicePushBL tInvoicePushBL = new InvoicePushBL();
					VData tVData = new VData();
					tVData.add(mLYOutPayDetailSet);
					tVData.add(tSSRS);
					tVData.add(mGlobalInput);
					if(!tInvoicePushBL.getSubmit(tVData, "")){
						buildError("callService", "调用OutPayUploadBL接口失败。");
						return false;
					}
					mMap = tInvoicePushBL.getResult();
					MMap sMap = new MMap();
					 LYOutPayDetailSet m2OutPayDetailSet = null;
					 Set set = mMap.keySet();
			         for (int j = 0; j < set.size(); j++)
			         {
			             //获取操作对象Schema或Set或SQL
			             //o = iterator.next();
			        	 Object  o = mMap.getOrder().get(String.valueOf(j + 1));
			        	 m2OutPayDetailSet = (LYOutPayDetailSet) o;
			        	 System.out.println(m2OutPayDetailSet.size());
					
			         }
			         
			         for(int k=1;k<=m2OutPayDetailSet.size();k++){
			        	 LYOutPayDetailSchema	 sLYOutPayDetailSchema = m2OutPayDetailSet.get(k); 
			        	 sMap.put("update tmpjszj set querycode='fpts002' where policyno ='"+sLYOutPayDetailSchema.getBusiNo()+"'", "UPDATE");
			         }
			        // mVData.add(mMap);
			         mVData.add(sMap);
					PubSubmit p = new PubSubmit();
					if (!p.submitData(mVData, SysConst.INSERT)){
						System.out.println("提交数据失败");
						buildError("submitData", "提交数据失败");
						return false;
					}
					mMap = new MMap();
					sMap = new MMap();
					mVData.clear();
					mLYOutPayDetailSet=new LYOutPayDetailSet();
				}

			}

		}
		return true;
	}

	/** 获取需要推送的签单完成的团单 */
	private LYOutPayDetailSet getData(){
		
		LYOutPayDetailSet m1LYOutPayDetailSet = null;
		LYOutPayDetailDB  tLYOutPayDetailDB = new LYOutPayDetailDB();
		m1LYOutPayDetailSet = tLYOutPayDetailDB.executeQuery("select * from LYOutPayDetail where BusiNo in ( select policyno from tmpjszj  where querycode ='fpts001') and tranamt > '0'");

		mGlobalInput.ComCode = "86";
		mGlobalInput.Operator = "001";
		return m1LYOutPayDetailSet;
	}

	private SSRS getXmDATA(){
		String SQL = "select codename,comcode,codealias from ldcode where codetype='fpxmmc'";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(SQL);
		return tSSRS;
	}

	private void buildError(String szFunc, String szErrMsg){
		CError cError = new CError();
		cError.moduleName = "InvoiceDateSend";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {
		InvoiceDateSendDealFromBW instance = new InvoiceDateSendDealFromBW();
		instance.run();
	}
}
