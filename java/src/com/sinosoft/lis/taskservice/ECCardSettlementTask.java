package com.sinosoft.lis.taskservice;

import com.cbsws.xml.ctrl.blogic.CardSettlement;
import com.sinosoft.utility.CErrors;

public class ECCardSettlementTask extends TaskThread{
	/**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    public ECCardSettlementTask()
    {
    }

    /**
     * 批处理程序调用的入口，供批处理程序调用。
     */
    public void run()
    {
    	try{
    		CardSettlement cs = new CardSettlement();
        	cs.settlement();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    }

    public static void main(String[] args)
    {
    	ECCardSettlementTask aa = new ECCardSettlementTask();
    	aa.run();
    }

}
