package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.agentcalculate.DealDataOfWageNoIsNull;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class DealWageNoIsNullTask extends TaskThread {
	
	public void run()
	{   
		DealDataOfWageNoIsNull tDealDataOfWageNoIsNull = new DealDataOfWageNoIsNull();
		long tStartTime = new Date().getTime();
		////根据新需求 ：#2655河北历史数据追加保费不校验回访日期  添加对于薪资月为空的数据中，对于追加保费的重新进行薪资月计算
		String tBranchType = "1" ;
		String tBranchType2 = "01";
		//查询需要对追加保费校验的机构
		String tSQL =" select code from ldcode where codetype='wagehistoryvisit' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(tSQL);
		for (int i = 1; i < tSSRS.getMaxRow(); i++) 
		{
			String tManageCom = tSSRS.GetText(i, 1);
			System.out.println("managecom:"+tManageCom);
			if(!tDealDataOfWageNoIsNull.dealData(tBranchType,tBranchType2,tManageCom))
	            {
	            	System.out.println("DealWageNoIsNullTask:河北追加保费回访日期薪资月计算失败");
	            }
		}
		long tEndTime = new Date().getTime();
		System.out.println("DealWageNoIsNullTask-->批处理执行完成时间"+PubFun.getCurrentTime());
		System.out.println("DealWageNoIsNullTask-->批处理执行时间:" + (tEndTime - tStartTime)
				+ "毫秒，大约" + (tEndTime - tStartTime) / 3600 / 1000 + "小时");
		
	 	
	}
	public static void main(String[] args) {
		DealWageNoIsNullTask tDealWageNoIsNullTask = new DealWageNoIsNullTask();
		tDealWageNoIsNullTask.run();
	}
   
}
