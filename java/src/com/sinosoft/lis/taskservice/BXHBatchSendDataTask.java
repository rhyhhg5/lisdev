package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.operfee.NewIndiDueFeeMultiUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.yinbaotongbank.BXHSendToBankAUI;
import com.sinosoft.lis.yinbaotongbank.BXHWriteToFileAUI;
import com.sinosoft.lis.yinbaotongbank.BXHWriteToFileUI;
import com.sinosoft.lis.yinbaotongbank.SendToBankAUI;
import com.sinosoft.lis.yinbaotongbank.SendToBankUI;
import com.sinosoft.lis.yinbaotongbank.YinBaoTongWriteToFileAUI;
import com.sinosoft.lis.yinbaotongbank.YinBaoTongWriteToFileUI;
import com.sinosoft.lis.yinbaotongbank.BXHSendToBankUI;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BXHBatchSendDataTask extends TaskThread {
	private String startDate = "";

	private String endDate = "";
	
	private String ubank = "";
	
	 private String uagentcode = "";
	
	public void run() {
		System.out.println("---BatchSendDataTask开始---");
		submitData();
		System.out.println("---BatchSendDataTask结束---");
	}

	public boolean submitData() {
		if (!getInputData()) {
			return false;
		}
		System.out.println("---End getInputData---");

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");
		return true;
	}

	private boolean getInputData() {
		try {
			startDate = "2012-1-1"; // 提取数据的起始日期
			endDate = PubFun.getCurrentDate();
		} catch (Exception e) {
			// @@错误处理
			CError.buildErr(this, "接收数据失败");
			return false;
		}
		return true;
	}

	private boolean dealData() {
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL
				.execSQL("select bankcode from ldbank where operator='ybt' and comcode='86' and agentcode ='68' ");
		try {
			// 调用页面数据提取类
			for (int i = 1; i <= tSSRS.MaxRow; i++) {
				System.out.println("---调用数据提取类---" + tSSRS.GetText(i, 1));
				getSendbankData(tSSRS.GetText(i, 1), "1");
				getSendbankData(tSSRS.GetText(i, 1), "2");
			}
		} catch (Exception ex) {
			System.out.println("---数据提取类报错---TT----");
			ex.printStackTrace();
		}

		SSRS tSSRS2 = new SSRS();
		try {
			 for (int i = 1; i <= tSSRS.MaxRow; i++)
	            {
				// 调用页面数据审核类
				System.out.println("---调用数据审核类---" + tSSRS.GetText(i, 1));
				getSendBankConf(tSSRS.GetText(i, 1), "1");//发盘前最后一次对数据进行更新。
				getSendBankConf(tSSRS.GetText(i, 1), "2");
			}
		} catch (Exception ex) {
			System.out.println("---数据审核类报错---TT----");
			ex.printStackTrace();
		}

		return true;
	}
	private boolean getSendbankData(String bankcode, String flag) {
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ManageCom = "86";
		tGlobalInput.ComCode = "86";
		tGlobalInput.Operator = "ybt";

		if ("1".equals(flag)) {
			// 自动催收
			System.out.println("-----------    代收开始执行了     -----------");
			LCPolSchema tLCPolSchema = new LCPolSchema(); // 个人保单表
			System.out.println("StartDate:" + startDate);
			tLCPolSchema.setGetStartDate(startDate); // 将判断条件设置在起领日期字段中
			tLCPolSchema.setPayEndDate(endDate); // 将判断条件设置在终交日期字段中

			// 用TransferData来向后台传送数据 zhanghui 2005.2.18
			TransferData transferData2 = new TransferData();
			transferData2.setNameAndValue("bankCode", bankcode);

			VData tVData2 = new VData();
			tVData2.add(tLCPolSchema);
			tVData2.add(tGlobalInput);
			tVData2.add(transferData2);
			NewIndiDueFeeMultiUI tNewIndiDueFeeMultiUI = new NewIndiDueFeeMultiUI();
			//开始催收
			tNewIndiDueFeeMultiUI.submitData(tVData2, "INSERT");

			if (tNewIndiDueFeeMultiUI.mErrors.needDealError()) {
				System.out.print("催收处理失败，不能生成银行发送数据，原因是:");

				for (int n = 0; n < tNewIndiDueFeeMultiUI.mErrors
						.getErrorCount(); n++) {
					System.out
							.print(tNewIndiDueFeeMultiUI.mErrors.getError(n).errorMessage
									+ "|");
				}
			} else {
				System.out.println("催收处理成功！");
			}

			// 生成银行数据
			System.out.println("\n\n---SendToBankSave Start---");
			BXHSendToBankUI getSendToBankUI1 = new BXHSendToBankUI();

			TransferData transferData1 = new TransferData();
			transferData1.setNameAndValue("startDate", startDate);
			transferData1.setNameAndValue("endDate", endDate);
			transferData1.setNameAndValue("bankCode", bankcode);
			transferData1.setNameAndValue("typeFlag", "");

			VData tVData = new VData();
			tVData.add(transferData1);
			tVData.add(tGlobalInput);

			if (!getSendToBankUI1.submitData(tVData, "GETMONEY")) {
				VData rVData = getSendToBankUI1.getResult();
				System.out.println(" 处理失败，原因是:" + (String) rVData.get(0));
			} else {
				VData rVData = getSendToBankUI1.getResult();
				System.out.println("送银行数据处理成功! " + (String) rVData.get(0));
			}

			System.out.println("\n---SendToBankSave End---\n\n");
		} else if ("2".equals(flag)) {

			System.out.println("-----------    代付开始执行了    -----------");

			// 生成银行数据
			System.out.println("\n\n---SendToBankSave Start---");
			BXHSendToBankAUI getSendToBankUI1 = new BXHSendToBankAUI();

			TransferData transferData1 = new TransferData();
			transferData1.setNameAndValue("startDate", startDate);
			transferData1.setNameAndValue("endDate", endDate);
			transferData1.setNameAndValue("bankCode", bankcode);

			VData tVData = new VData();
			tVData.add(transferData1);
			tVData.add(tGlobalInput);

			if (!getSendToBankUI1.submitData(tVData, "PAYMONEY")) {
				VData rVData = getSendToBankUI1.getResult();
				System.out.println(" 处理失败，原因是:" + (String) rVData.get(0));
			} else {
				VData rVData = getSendToBankUI1.getResult();
				System.out.println("送银行数据处理成功! " + (String) rVData.get(0));
			}

			System.out.println("\n---SendToBankSave End---\n\n");
		}
		return true;
	}

	private boolean getSendBankConf(String bankcode, String flag) {
		GlobalInput tG = new GlobalInput();
		tG.ManageCom = "86";
		tG.ComCode = "86";
		tG.Operator = "ybt";

		String buttonflag = "Yes";
		ExeSQL tExeSQL = new ExeSQL();

		if ("1".equals(flag)) {
			System.out.println("------------   代收开始执行啦！！！   ------------");
			LJSPaySchema tLJSPaySchema;
			LJSPaySet tLJSPaySet = new LJSPaySet();
			SSRS tYSSSRS = new SSRS();
			tYSSSRS = tExeSQL
					.execSQL(" select getnoticeno,BankCode,SumDuePayMoney,BankAccNo,AccName,ManageCom,''  from ljspay a    "
							+ " where ManageCom like '"
							+ tG.ManageCom
							+ "%%'  "
							+ " and cansendbank = '4'  "
							+ " and exists ( select 1 from ldbankunite where bankunitecode='"
							+ bankcode
							+ "' and bankcode=a.bankcode) "
							+ " order by SerialNo desc ");
			if (tYSSSRS.MaxRow > 0) {
				for (int index = 1; index <= tYSSSRS.MaxRow; index++) {
					tLJSPaySchema = new LJSPaySchema();
					tLJSPaySchema.setGetNoticeNo(tYSSSRS.GetText(index, 1));
					tLJSPaySchema.setBankCode(tYSSSRS.GetText(index, 2));
					tLJSPaySchema.setSumDuePayMoney(tYSSSRS.GetText(index, 3));
					tLJSPaySchema.setBankAccNo(tYSSSRS.GetText(index, 4));
					tLJSPaySchema.setAccName(tYSSSRS.GetText(index, 5));
					tLJSPaySchema.setManageCom(tYSSSRS.GetText(index, 6));
					tLJSPaySet.add(tLJSPaySchema);
				}

				VData tVData = new VData();
				tVData.add(tLJSPaySet);
				tVData.add(tG);
				tVData.add(buttonflag);

				BXHWriteToFileUI tBXHWriteToFileUI = new BXHWriteToFileUI();
				if (!tBXHWriteToFileUI.submitData(tVData, "WRITE")) {
					VData rData = tBXHWriteToFileUI.getResult();
					System.out.println("处理失败，原因是:" + (String) rData.get(0));
				} else {
					System.out.println("数据确认成功 ^-^ ");
				}

			}

		}else if ("2".equals(flag)) {
			System.out.println("------------   代付执行啦！！！   ------------");
			LJAGetSchema tLJAGetSchema;
			LJAGetSet tLJAGetSet = new LJAGetSet();

			SSRS tYFSSRS = new SSRS();
			tYFSSRS = tExeSQL
					.execSQL(" select OtherNo,BankCode,SumGetMoney,BankAccNo,AccName,ManageCom,actugetno "
							+ " from ljaget a where ManageCom like '"
							+ tG.ManageCom
							+ "%%' "
							+ " and cansendbank = '4' "
							+ " and exists ( select 1 from ldbankunite where bankunitecode='"
							+ bankcode
							+ "' and bankcode=a.bankcode) "
							+ " order by SerialNo desc ");
			if (tYFSSRS.MaxRow > 0) {
				for (int index = 1; index <= tYFSSRS.MaxRow; index++) {
					tLJAGetSchema = new LJAGetSchema();
					tLJAGetSchema.setActuGetNo(tYFSSRS.GetText(index, 7));
					tLJAGetSchema.setOtherNo(tYFSSRS.GetText(index, 1));
					tLJAGetSchema.setBankCode(tYFSSRS.GetText(index, 2));
					tLJAGetSchema.setSumGetMoney(tYFSSRS.GetText(index, 3));
					tLJAGetSchema.setBankAccNo(tYFSSRS.GetText(index, 4));
					tLJAGetSchema.setAccName(tYFSSRS.GetText(index, 5));
					tLJAGetSchema.setManageCom(tYFSSRS.GetText(index, 6));
					tLJAGetSet.add(tLJAGetSchema);
				}

				VData tVData = new VData();
				tVData.add(tLJAGetSet);
				tVData.add(tG);
				tVData.add(buttonflag);

				BXHWriteToFileAUI tBXHWriteToFileAUI = new BXHWriteToFileAUI();
				if (!tBXHWriteToFileAUI.submitData(tVData, "WRITE")) {
					VData rData = tBXHWriteToFileAUI.getResult();
					System.out.println("处理失败，原因是:" + (String) rData.get(0));
				} else {
					System.out.println("数据确认成功 ^-^ ");
				}
			}

		}

		return true;
	}

	public static void main(String[] args) {
		BXHBatchSendDataTask tBatchSendDataTask = new BXHBatchSendDataTask();
		tBatchSendDataTask.run();
	}
}
