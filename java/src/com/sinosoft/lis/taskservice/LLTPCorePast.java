package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * <p>Title: </p>
 *
 * <p>Description:
 *上海外包案件信息反馈批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author zhang
 * @version 1.1
 */


public class LLTPCorePast extends TaskThread {

	private ExeSQL mExeSQL = new ExeSQL();

	// TransInfo 标识信息
	private Element tPACKET = null;
	
    //TransInfo 标识信息
	private Element tHEAD = null;
	
    //TransInfo 标识信息
	private Element tBODY = null;

	// 案件
	private Element tGrpContInfo = null;

	// 案件
	private Element tContInfos = null;

	// 案件信息SQL
	private String mGrpContInfoSQL = "";

	// 案件信息SSRS
	private SSRS tGrpContInfoSSRS = null;

	// 案件数
	private int GrpContInfolen = 0;

	/** 应用路径 */
	private String mURL = "";

	/** 文件路径 */
	private String mFilePath = "";
	String opr="true";
	
	// 执行任务
	public void run() {//田苗苗
		
		String outSource="select code,codealias from ldcode where codetype  ='thridcompany'  order by othersign ";
    	SSRS outSources=mExeSQL.execSQL(outSource);
		if(outSources.MaxRow<=0){
			return ;
		}
		
		for (int i = 1; i <= outSources.MaxRow; i++) {
			String code=outSources.GetText(i, 1);
			String codealias=outSources.GetText(i, 2);
			if(code==null ||  "".equals(code)  ){
				continue ;
			}
			String mngcoms="Select code,codealias From Ldcode  Where Codetype = '"+code+"ComCode' ";
			
			SSRS mngcom=mExeSQL.execSQL(mngcoms);
			if(mngcom.MaxRow<=0){
				continue ;
			}
			for (int im = 1; im <= mngcom.MaxRow; im++) {
				String mng=mngcom.GetText(im, 1);
				getInputData(code,codealias==null ? "": codealias,mng);
				if(!dealData(code,codealias==null ? "": codealias,mng)){
					 System.out.println("既往理赔出问题了");
		             opr="false";
				}else{
					 System.out.println("既往理赔成功了");
				}
			}
		}
	}
     	
	
	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
	private boolean getInputData(String code,String codealias,String mng) {//田苗苗

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);
		// 本地测试路径
//		mURL = "D:\\picc\\ui\\IMG\\";
		//保存到核心路径
		mURL = mURL + "vtsfile/"+codealias+"TPClaim/"+mng+"/CorePast/"+PubFun.getCurrentDate()+"/";//田苗苗
//		mURL = mURL + "vtsfile\\"+codealias+"TPClaim\\"+mng+"\\CorePast\\"+PubFun.getCurrentDate()+"\\";//田苗苗
		 
		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}
       return true;
	}

	public boolean dealData(String code,String codealias,String mng  ) {

		    String tSQL="Select Code From Ldcode  Where Codetype = '"+code+"' and comcode like '"+mng+"%' and date(codename) =current date - 1 days  ";//田苗苗
			SSRS tGRPcontnoSSRS = mExeSQL.execSQL(tSQL);
			if(tGRPcontnoSSRS.getMaxRow()<=0){
				System.out.println("该外包方（"+code+"）该机构（"+mng+"）下并无保单数据："+tSQL);
				return false;
			}
			int tGRPcontnolen = tGRPcontnoSSRS.getMaxRow();
			for(int x = 1; x <= tGRPcontnolen; x++){
		mGrpContInfoSQL = "select  a.grpcontno,a.caseno,a.rgtno,  " +
		        " (select claimno from llhospcase where caseno=b.caseno), " +  //田苗苗
				" a.getdutykind,(select codename from ldcode where codetype='getdutykind' and code=a.getdutykind fetch first row only), " +
				" a.getdutycode,(select getdutyname from lmdutygetclm where getdutycode=a.getdutycode fetch first row only ), " +
				" b.customerno,a.contno,b.customername,b.idtype,b.idno, " +
				" (select max(accdate) from LLSubReport where subrptno in( select subrptno from llcaserela where caseno=b.caseno))," +
				" a.riskcode,(select riskname from lmriskapp where riskcode=a.riskcode) , " +
				" (select DiseaseCode from llcasecure where caseno =b.caseno fetch first row only), " +
				" (select DiseaseName from llcasecure where caseno =b.caseno fetch first row only), " +
				" sum(a.realpay),a.GiveType,a.GiveTypeDesc,sum(a.OutDutyAmnt),b.endcasedate " +
				" from  llclaimdetail a,llcase b where a.caseno=b.caseno  " +
				" and a.grpcontno ='"+tGRPcontnoSSRS.GetText(x, 1)+"' " +
				" and  b.rgtstate in('11','12') " +
			//	" and  exists  (select 1 from llhospcase where caseno=b.caseno ) " +
//				" and a.rgtno =''" +
				" group by a.grpcontno,a.caseno,a.rgtno,a.getdutykind, a.getdutycode,b.customerno, " +
				" a.contno,b.customername,b.idtype,b.idno,a.GiveType,a.GiveTypeDesc,b.endcasedate,b.caseno,a.riskcode " 
//				+" fetch first 10 rows only" 
				;

		tGrpContInfoSSRS = mExeSQL.execSQL(mGrpContInfoSQL);
		GrpContInfolen = tGrpContInfoSSRS.getMaxRow();

		if (GrpContInfolen > 0) {

		tPACKET = new Element("PACKET");
		Document Doc = new Document(tPACKET);
		tPACKET.addAttribute("type", "REQUEST");
		tPACKET.addAttribute("version", "1.0");
		tHEAD = new Element("HEAD");
		tHEAD.addContent(new Element("REQUEST_TYPE").setText("TP04"));//田苗苗
		tHEAD.addContent(new Element("TRANSACTION_NUM").setText("TP04"+mng+"0000"+PubFun.getCurrentDate2()+"0000000001"));//田苗苗
		tPACKET.addContent(tHEAD);
		tBODY = new Element("BODY");
		tContInfos = new Element("LLCASELIST");
		
		for (int i = 1; i <= GrpContInfolen; i++) {
			
			tGrpContInfo = new Element("LLCASE_DATA");

			tGrpContInfo.addContent(new Element("Grpcontno")
					.setText(tGrpContInfoSSRS.GetText(i, 1)));
			tGrpContInfo.addContent(new Element("Caseno")
					.setText(tGrpContInfoSSRS.GetText(i, 2)));
			tGrpContInfo.addContent(new Element("Rgtno")
					.setText(tGrpContInfoSSRS.GetText(i, 3)));
			tGrpContInfo.addContent(new Element("Claimno")
					.setText(tGrpContInfoSSRS.GetText(i, 4)));//田苗苗
			tGrpContInfo.addContent(new Element("Getdutykind")
					.setText(tGrpContInfoSSRS.GetText(i, 5)));
			tGrpContInfo.addContent(new Element("Getdutykindname")
					.setText(tGrpContInfoSSRS.GetText(i, 6)));
			tGrpContInfo.addContent(new Element("Getdutycde")
					.setText(tGrpContInfoSSRS.GetText(i, 7)));
			tGrpContInfo.addContent(new Element("Getdutycdename")
					.setText(tGrpContInfoSSRS.GetText(i, 8)));
			tGrpContInfo.addContent(new Element("Insuedno")
			.setText(tGrpContInfoSSRS.GetText(i, 9)));
			tGrpContInfo.addContent(new Element("contno")
					.setText(tGrpContInfoSSRS.GetText(i, 10)));
			tGrpContInfo.addContent(new Element("name")
					.setText(tGrpContInfoSSRS.GetText(i, 11)));
			tGrpContInfo.addContent(new Element("idtype")
					.setText(tGrpContInfoSSRS.GetText(i, 12)));
			tGrpContInfo.addContent(new Element("idno").setText(tGrpContInfoSSRS
					.GetText(i, 13)));
			tGrpContInfo.addContent(new Element("accdate").setText(tGrpContInfoSSRS
					.GetText(i, 14)));
			tGrpContInfo.addContent(new Element("riskcode").setText(tGrpContInfoSSRS
					.GetText(i, 15)));
			tGrpContInfo.addContent(new Element("riskname")
					.setText(tGrpContInfoSSRS.GetText(i, 16)));
			tGrpContInfo.addContent(new Element("DISEASECODE")
					.setText(tGrpContInfoSSRS.GetText(i, 17)));
			tGrpContInfo.addContent(new Element("DISEASEname")
					.setText(tGrpContInfoSSRS.GetText(i, 18)));
			tGrpContInfo.addContent(new Element("realpay")
					.setText(tGrpContInfoSSRS.GetText(i, 19)));
			tGrpContInfo.addContent(new Element("givetype")
					.setText(tGrpContInfoSSRS.GetText(i, 20)));
			tGrpContInfo.addContent(new Element("givetypename")
					.setText(tGrpContInfoSSRS.GetText(i, 21)));
			tGrpContInfo.addContent(new Element("OutDutyAmnt")
					.setText(tGrpContInfoSSRS.GetText(i, 22)));
			tGrpContInfo.addContent(new Element("endcasedate")
			.setText(tGrpContInfoSSRS.GetText(i, 23)));
		
			tContInfos.addContent(tGrpContInfo);
			
		}		
			
			tBODY.addContent(tContInfos);
			tPACKET.addContent(tBODY);
			

			mFilePath = mURL +code.substring(0, 2)+"_LPCP_" +tGRPcontnoSSRS.GetText(x, 1)+"_" + PubFun.getCurrentDate2() + PubFun.getCurrentTime2() +".xml";
			XMLOutputter outputter = new XMLOutputter("  ", true, "GBK");

			try {
				outputter.output(Doc, new FileOutputStream(mFilePath));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!sendXML(mFilePath,code,codealias,mng)){//田苗苗
			  System.out.println("既往理赔信息向FTP发送文件失败！");
			  return false;
			}

			}
			}
		return true;
	}

	private boolean sendXML(String cXmlFile,String code,String codealias,String mng){

		String getIPPort = "select codename ,codealias,comcode from ldcode where codetype='"+codealias+"TPClaim'  and code='IP/Port' ";//田苗苗
		String getUserPs = "select codename ,codealias from ldcode where codetype='"+codealias+"TPClaim'  and code='User/Pass' ";//田苗苗
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			System.out.println("FTP服务器IP、用户名、密码、端口都不能为空，否则无法发送xml");
			return false;
		}
		
		String tIP = tIPSSRS.GetText(1, 1);
	    String tPort = tIPSSRS.GetText(1, 2);
	    String tUserName = tUPSSRS.GetText(1, 1);
	    String tPassword = tUPSSRS.GetText(1, 2);
		
	    String thridcompany = tIPSSRS.GetText(1, 3);
	    
		FTPTool tFTPTool = new FTPTool(tIP, tUserName,
				tPassword, Integer.parseInt(tPort));
		
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println("登录ftp服务器失败");
				return false;
			}
		} catch (SocketException ex) {
			System.out.println("无法登录ftp服务器");
			return false;
		} catch (IOException ex) {
			System.out.println("无法读取ftp服务器信息");
			return false;
		}
		try {
			if(!tFTPTool.makeDirectory("/"+thridcompany+"/TPJK/"+mng+"/TPClaimCorePast/"+PubFun.getCurrentDate()+"/")){
				System.out.println(mng+"FTP目录已存在");
			};			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		if (!tFTPTool.upload("/"+thridcompany+"/TPJK/"+mng+"/TPClaimCorePast/"+PubFun.getCurrentDate()+"/", cXmlFile)) {
			System.out.println(thridcompany+"外包"+mng+"案件结果上载文件失败!"+cXmlFile);
			tFTPTool.logoutFTP();
			return false;
		}
		
		tFTPTool.logoutFTP();

		return true;
	}

	public static void main(String[] args) {

		LLTPCorePast t = new LLTPCorePast();
		t.run();

	}

}
