package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.tb.BPODealXMLBL;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 负责调用BPOParseXMLBL.java和BPODealXMLBL.java进行业务逻辑处理。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPODealXMLTask extends TaskThread
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    public BPODealXMLTask()
    {
    }

    /**
     * 批处理程序调用的入口，供批处理程序调用。
     * 方法内调用BPOParseXMLBL.java和BPODealXMLBL.java进行业务逻辑处理。
     */
    public void run()
    {
        GlobalInput mG = new GlobalInput();
        VData mVData = new VData();
        mG.Operator = "001";
        mG.ManageCom = "86";
        mVData.add(mG);


        BPODealXMLBL tBPODealXMLBL = new BPODealXMLBL();
        //发送扫描件
        if(!tBPODealXMLBL.submitData(mVData,""))
        {
            System.out.println(tBPODealXMLBL.mErrors.getErrContent());
        }
    }

    public static void main(String[] args)
    {
        BPODealXMLTask bpodealxmltask = new BPODealXMLTask();
    }
}
