package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAIndirectWageSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAIndirectWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;

public class AgentWageOfLABankManageTask extends TaskThread {

	private MMap mMap = new MMap();
	private LAIndirectWageSet mLAIndirectWageSet = new LAIndirectWageSet();

	public static void main(String[] args) {
		AgentWageOfLABankManageTask tAgentWageOfLABankManageTask = new AgentWageOfLABankManageTask();
		tAgentWageOfLABankManageTask.run();
	}

	public void run() {
		long tStartTime = new Date().getTime();
		System.out.println("AgentWageOfLABankManageTask-->run :开始执行");

		String dateSql = "Select date('" + PubFun.getCurrentDate()
				+ "')-1 day from dual";
		ExeSQL texeSQL = new ExeSQL();
		String tMakeDate = texeSQL.getOneValue(dateSql);
		String sql = "select * from lacommision where branchtype = '3' and branchtype2 = '01' and payyear='0' and (commdire='1'"
				+ "or (commdire='2' and transtype = 'WT'))";
		sql += " and tmakedate='" + tMakeDate + "'";
		// 测试用
		// sql +=
		// " and managecom = '86110000' and tmakedate between '2015-8-1' and '2015-8-30'";
		sql += " order by CommisionSN with ur ";
		System.out.println("：" + sql);

		dealData(sql);
		System.out.println("AgentWageOfCalWageNoOldTask-->批处理执行完成:"
				+ PubFun.getCurrentTime());
		long tEndTime = new Date().getTime();
		System.out.println("新单折标处理的数据处理时间花费" + (tEndTime - tStartTime)
				+ "毫秒，大约" + (tEndTime - tStartTime) / 3600 / 1000 + "小时");
	}

	public boolean dealData(String cSQL) {
		System.out.println("AgentWageOfLABankManageTask-->dealData:开始执行");
		System.out
				.println("AgentWageOfLABankManageTask-->dealData:cSQL" + cSQL);
		LACommisionSet tLACommisionSet = new LACommisionSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLACommisionSet, cSQL);
		do {
			tRSWrapper.getData();
			if (null == tLACommisionSet || 0 == tLACommisionSet.size()) {
				System.out
						.println("AgentWageOfLABankManageTask-->dealData:没有满足新单折标的数据！！");
				return true;
			}
			mMap = new MMap();
			for (int i = 1; i <= tLACommisionSet.size(); i++) {
				LACommisionSchema tLACommisionSchema = new LACommisionSchema();
				LAIndirectWageSchema mLAIndirectWageSchema = new LAIndirectWageSchema();
				tLACommisionSchema = tLACommisionSet.get(i);
				String cCommisionSN = tLACommisionSchema.getCommisionSN();
				String cBranchType = tLACommisionSchema.getBranchType();
				String cBranchType2 = tLACommisionSchema.getBranchType2();
				String cRiskCode = tLACommisionSchema.getRiskCode();
				String cAgentCom = tLACommisionSchema.getAgentCom();
				String cContNo = tLACommisionSchema.getContNo();
				String cGrpContNo = tLACommisionSchema.getGrpContNo();
				String cManageCom = tLACommisionSchema.getManageCom();
				String cPayyears = tLACommisionSchema.getPayYears() + "";
				String cPayyear = tLACommisionSchema.getPayYear() + "";
				String cRenewCount = tLACommisionSchema.getReNewCount() + "";
				String cTransState = tLACommisionSchema.getTransState();
				String cPayIntv = tLACommisionSchema.getPayIntv() + "";
				String cAgentCode = tLACommisionSchema.getAgentCode();
				String cWageNo = tLACommisionSchema.getWageNo();
				String cAgentGroup = tLACommisionSchema.getAgentGroup();

				// 计算团队首期绩效提奖比例
				String strWageRate = this.calCalRateForAll(cBranchType,
						cBranchType2, cRiskCode, cAgentCom, cContNo,
						cGrpContNo, cManageCom, cPayyears, cPayyear,
						cRenewCount, cTransState, cPayIntv, cAgentCode,
						cWageNo, cAgentGroup);
				double wageRateValue = 0;
				wageRateValue = Double.parseDouble(strWageRate);
				System.out.println("主管职级间接提奖比例为：" + wageRateValue);
				double tTransMoney = tLACommisionSchema.getTransMoney();
				double tBankG = tTransMoney * wageRateValue;
				System.out.println("主管职级间接佣金为：" + tBankG);
				// 新单进行折标
				String strStandPrem = this.calCalStandPrem(cCommisionSN,
						cRiskCode, cPayyears, cPayyear, cBranchType,
						cBranchType2);
				double standPrem = 0;
				standPrem = Double.parseDouble(strStandPrem);

				// 存入间接薪资项表
				// System.out.println("明细扎帐号为："+tLACommisionSchema.getCommisionSN());
				mLAIndirectWageSchema.setCommisionSN(tLACommisionSchema
						.getCommisionSN());
				mLAIndirectWageSchema.setBankGrpMoney(tBankG);
				mLAIndirectWageSchema.setGrpRate(wageRateValue);
				// 新单折标保费
				mLAIndirectWageSchema.setF1(standPrem);
				mLAIndirectWageSchema.setBranchType(cBranchType);
				mLAIndirectWageSchema.setBranchType2(cBranchType2);
				mLAIndirectWageSchema.setOperator(tLACommisionSchema
						.getOperator());
				mLAIndirectWageSchema.setTMakeDate(tLACommisionSchema
						.getTMakeDate());
				mLAIndirectWageSchema.setMakeDate(PubFun.getCurrentDate());
				mLAIndirectWageSchema.setMakeTime(PubFun.getCurrentTime());
				mLAIndirectWageSchema.setModifyDate(PubFun.getCurrentDate());
				mLAIndirectWageSchema.setModifyTime(PubFun.getCurrentTime());
				mLAIndirectWageSet.add(mLAIndirectWageSchema);

			}
			mMap.put(this.mLAIndirectWageSet, "INSERT");
			try {
				VData tVData = new VData();
				tVData.add(mMap);
				PubSubmit tPubSubmit = new PubSubmit();
				tPubSubmit.submitData(tVData, "");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} while (tLACommisionSet.size() > 0);
		System.out.println("AgentWageOfLABankManageTask-->dealData:执行结束 ");
		return true;
	}

	// 计算团队首期绩效
	private String calCalRateForAll(String cBranchType, String cBranchType2,
			String cRiskCode, String cAgentCom, String cContNo,
			String cGrpContNo, String cManageCom, String cPayyears,
			String cPayyear, String cRenewCount, String cTransState,
			String cPayIntv, String cAgentCode, String cWageNo,
			String cAgentGroup) {
		String tBranchType = cBranchType == null ? "" : cBranchType;
		String tBranchType2 = cBranchType2 == null ? "" : cBranchType2;
		String tRiskCode = cRiskCode == null ? "" : cRiskCode;
		String tAgentCom = cAgentCom == null ? "" : cAgentCom;
		String tContNo = cContNo == null ? "" : cContNo;
		String tGrpContNo = cGrpContNo == null ? "" : cGrpContNo;
		String tManageCom = cManageCom == null ? "" : cManageCom;
		String tPayyears = cPayyears == null ? "" : cPayyears;
		String tPayyear = cPayyear == null ? "" : cPayyear;
		String tRenewCount = cRenewCount == null ? "" : cRenewCount;// 团险根据收费方式采取不同的佣金计算方式
		String tTransState = cTransState == null ? "" : cTransState;
		String tPayIntv = cPayIntv == null ? "" : cPayIntv;
		String tAgentCode = cAgentCode == null ? "" : cAgentCode;
		String tWageNo = cWageNo == null ? "" : cWageNo;
		String tAgentGroup = cAgentGroup == null ? "" : cAgentGroup;
		// if (tBranchType == "")
		// {
		// return "";
		// }
		// if (tBranchType2 == "") {
		// return "";
		// }
		// //如果是续保、续期，则不需要按照回执回销计算
		// else if (mLACommisionSchema.getPayCount()>1)
		// {
		// tFlag="02";
		// }

		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode("gRate");
		tCalculator.addBasicFactor("BranchType", tBranchType);
		tCalculator.addBasicFactor("BranchType2", tBranchType2);
		tCalculator.addBasicFactor("RiskCode", tRiskCode);
		tCalculator.addBasicFactor("AgentCom", tAgentCom);
		tCalculator.addBasicFactor("ContNo", tContNo);
		tCalculator.addBasicFactor("GrpContNo", tGrpContNo);
		tCalculator.addBasicFactor("ManageCom", tManageCom);
		tCalculator.addBasicFactor("Payyears", tPayyears);
		tCalculator.addBasicFactor("Payyear", tPayyear);
		tCalculator.addBasicFactor("RenewCount", tRenewCount);
		tCalculator.addBasicFactor("TransState", tTransState);
		tCalculator.addBasicFactor("PayIntv", tPayIntv);
		tCalculator.addBasicFactor("AgentCode", tAgentCode);
		tCalculator.addBasicFactor("WageNo", tWageNo);
		tCalculator.addBasicFactor("AgentGroup", tAgentGroup);

		String tResult = tCalculator.calculate();
		return tResult == null ? "" : tResult;
	}

	// 新单进行折标
	private String calCalStandPrem(String cCommisionSN, String cRiskCode,
			String cPayyears, String cPayyear, String cBranchType,
			String cBranchType2) {
		String tCommisionSN = cRiskCode == null ? "" : cCommisionSN;
		String tRiskCode = cRiskCode == null ? "" : cRiskCode;
		String tPayyears = cPayyears == null ? "" : cPayyears;
		String tPayyear = cPayyear == null ? "" : cPayyear;
		String tBranchType = cBranchType == null ? "" : cBranchType;
		String tBranchType2 = cBranchType2 == null ? "" : cBranchType2;

		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode("sPrem");
		tCalculator.addBasicFactor("CommisionSN", tCommisionSN);
		tCalculator.addBasicFactor("RiskCode", tRiskCode);
		tCalculator.addBasicFactor("Payyears", tPayyears);
		tCalculator.addBasicFactor("Payyear", tPayyear);
		tCalculator.addBasicFactor("BranchType", tBranchType);
		tCalculator.addBasicFactor("BranchType2", tBranchType2);

		String tResult = tCalculator.calculate();
		return tResult == null ? "" : tResult;
	}
}
