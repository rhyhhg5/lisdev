package com.sinosoft.lis.taskservice;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.utility.CErrors;

public class LLCaseStateDateTask extends TaskThread {
	public CErrors mErrors = new CErrors();
	public void run(){
		PubFun tPubFun = new PubFun();
		String tStartDate = tPubFun.calDate(tPubFun.getCurrentDate(),-1,"D", null);
		String tEndDate = tPubFun.calDate(tPubFun.getCurrentDate(),-1,"D", null);
		System.out.println("tStartDate: " + tStartDate);
		System.out.println("tEndDate: " + tEndDate);
		//System.out.println(tStartDate);
		LLCaseStateDateBL tLLCaseStateDateBL = new LLCaseStateDateBL();
		tLLCaseStateDateBL.dealData(tStartDate, tEndDate);
	}
	
	public static void main(String args[]) {

        LLCaseStateDateTask a = new LLCaseStateDateTask ();
        a.run();
        if (a.mErrors.needDealError()) {
            System.out.println(a.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }

    }
}