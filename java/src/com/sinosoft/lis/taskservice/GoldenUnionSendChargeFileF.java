package com.sinosoft.lis.taskservice;

import java.io.File;

import com.sinosoft.lis.bank.GoldenUnionSendFile;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

public class GoldenUnionSendChargeFileF extends TaskThread{


	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private String mUrl = null;

    private String mPath = null;
    
    public GoldenUnionSendChargeFileF(){}
    
    public void run(){
    	System.out.println("---GoldenUnionSendChargeFileF开始---");
        submitData();
        System.out.println("---GoldenUnionSendChargeFileF结束---");
    }
    
    public boolean submitData(){

        if (!getUrlName()){
            return false;
        }

        if (!dealData()){
            return false;
        }
        return true;
    }
    
    /**
     * dealData
     * 数据逻辑处理
     * @return boolean
     */
    private boolean dealData(){
        
        System.out.println("文件存放路径" + mUrl);
        GoldenUnionSendFile tGoldenUnionSendFile = new GoldenUnionSendFile();
        //处理
        tGoldenUnionSendFile.submitData(mUrl, "F", "'7707'","");
        return true;
    }
    
    /**
     * newFolder
     * 新建报文存放文件夹，以便对发盘报文查询
     * @return boolean
     */
    public static boolean newFolder(String folderPath){
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try{
            if (myFilePath.isDirectory()){
                System.out.println("目录已存在,文件路径为:"+myFilePath);
                return true;
            }else{
                myFilePath.mkdirs();
                System.out.println("新建目录成功,文件路径为:"+myFilePath);
                return true;
            }
        }catch (Exception e){
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * getUrlName
     * 获取文件存放路径
     * @return boolean
     */
    private boolean getUrlName(){
    	 String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";//发盘报文的存放路径
         String fileFolder = new ExeSQL().getOneValue(sqlurl);

        
         if (fileFolder == null || fileFolder.equals("")){
             buildError("getFileUrlName", "获取文件存放路径出错");
             return false;
         }
         
         mUrl =fileFolder + "JLWJ/" + PubFun.getCurrentDate2();
         if (!newFolder(mUrl)){
        	 return false;
         }
     	/*if(!newFolder(mUrl)){
         	return false;
         }
     	 System.out.println("发盘报文的存放路径:" + mUrl);*/
         
          //TODO
          //测试用
          //mUrl = "E://JLWJ//";
         return true;
    }
	
    private void buildError(String szFunc, String szErrMsg){
        CError cError = new CError();
        cError.moduleName = "BatchSendBankTrans";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String[] arr){
    	GoldenUnionSendChargeFileF tGoldenUnionSendChargeFileF = new GoldenUnionSendChargeFileF();
        tGoldenUnionSendChargeFileF.run();
    }


}
