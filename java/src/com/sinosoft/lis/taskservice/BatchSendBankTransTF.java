package com.sinosoft.lis.taskservice;

import java.io.File;

import com.sinosoft.lis.bank.BatchSendCreateXml;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2011</p>
 * <p>Company: sinosoft</p>
 * @author Zcx
 * @version 1.0
 */

public class BatchSendBankTransTF extends TaskThread
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private String mUrl = null;

    private String mPath = null;

    public BatchSendBankTransTF()
    {

    }

    public void run()
    {
        System.out.println("---BatchSendBankTrans开始---");
        submitData();
        System.out.println("---BatchSendBankTrans正常结束---");

    }

    public boolean submitData()
    {

        if (!getUrlName())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * dealData
     * 数据逻辑处理
     * @return boolean
     */
    private boolean dealData()
    {
        mPath = mUrl + PubFun.getCurrentDate2() ;
        if (!newFolder(mPath))
        {
            mPath = mUrl;
        }
        System.out.println("文件存放路径" + mPath);
        BatchSendCreateXml tBatchSendCreateXml = new BatchSendCreateXml();
//      由于通联银联、代收代付需要不同时间段的批处理进行处理
        tBatchSendCreateXml.submitData(mPath, "F" ,"'7706'");
        return true;
    }

    /**
     * newFolder
     * 新建报文存放文件夹，以便对发盘报文查询
     * @return boolean
     */
    public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }

    /**
     * getUrlName
     * 获取文件存放路径
     * @return boolean
     */
    private boolean getUrlName()
    {
        String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";//发盘报文的存放路径
        mUrl = new ExeSQL().getOneValue(sqlurl);
        System.out.println("发盘报文的存放路径:" + mUrl);//调试用－－－－－
        if (mUrl == null || mUrl.equals(""))
        {
            buildError("getFileUrlName", "获取文件存放路径出错");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BatchSendBankTrans";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] arr)
    {
        BatchSendBankTransTF tBatchSendBankTrans = new BatchSendBankTransTF();
        tBatchSendBankTrans.run();
    }
}
