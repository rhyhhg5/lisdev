package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.operfee.IndiFinVerifyMultUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.tb.BPODealXMLBL;
import com.sinosoft.lis.xb.OmnipDueVerifyBL;
import com.sinosoft.lis.xb.PRnewDueVerifyBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BqXuQiHeXiaoTask extends TaskThread {
	public CErrors mErrors = new CErrors();
	private GlobalInput tGI = new GlobalInput();
	private String EndDate="9999-1-1";
	private String managecom = "86";
	private String mGetNoticeNo=null;
	private String whereParSql="";
	private boolean mFlag=true;
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();
	
	public BqXuQiHeXiaoTask(String tGetNoticeNo,GlobalInput pGI)
	{
		mGetNoticeNo=tGetNoticeNo;
		tGI=pGI;
		managecom=pGI.ManageCom;
	}
	public BqXuQiHeXiaoTask()
	{
		
	}
	
	public void run()
    {       
		dealData();
    }
	public boolean dealData()
	{
		//先准备数据
		getInputData();
		
		//非万能险种核销批处理
		FeiWanNeng();
		
		//万能核销批处理
		wanNeng();
		
		if(!mFlag)
		{
			return false;
		}
		return true;
       
	}
	private boolean getInputData()
	{
		if(mGetNoticeNo!=null&&!mGetNoticeNo.equals(""))
		{
			whereParSql=" and c.GetNoticeNo='"+mGetNoticeNo+"'";
		}
		else
		{
			mFlag=false;
			tGI.ComCode="86";
		    tGI.Operator="Server";
		    tGI.ManageCom="86";	
		}
	    return true;
	}
	private void FeiWanNeng()
	{
	    
        //续期收费
	    String ContSql   = "select c.getnoticeno,c.otherno "
					     + " from LJSPayPerson a ,LCCont  b,LJSPay c "
					     + " where a.LastPayToDate <='" + EndDate  + "' " 
					     + " and a.LastPayToDate <='" + CurrentDate  + "' "
					     + " and c.OtherNo=b.ContNo"
					     + " and a.ContNo=b.ContNo"
//					     + " and a.ContNo='000009809000007'"
					     +   whereParSql
					     + " and a.getnoticeno = c.getnoticeno "
					     + " and b.PayIntv > 0 "
					     + " and c.othernotype='2' "
					     + " and c.manageCom like '" + managecom + "%' "
					     + " and exists(select RiskCode from LMRisk where (CPayFlag='Y') and riskcode=a.RiskCode )"
					     + " and ((select count(1) from LJTempFee where TempFeeNo= c.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
						 + " and not exists(select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.riskcode not in ('332301','334801','340501','340601','340602') and lmriskapp.risktype4='4' and lcpol.Contno = b.ContNo ) "
					     + " group by c.getnoticeno,c.otherno "
					     + " union "
					     //续保收费
					     + " select c.getnoticeno,c.otherno "
					     + " from LJSPayPerson a ,LCCont  b,LJSPay c "
					     + " where a.LastPayToDate <='" + EndDate  + "' "
					     + " and a.LastPayToDate <='" + CurrentDate  + "' "
					     + " and c.othernotype='2' "
					     +   whereParSql
					     + " and a.getnoticeno = c.getnoticeno "
					     + " and c.manageCom like '" + managecom + "%' "
					     + " and ( b.ContNo =(select distinct contNo from LCRnewStateLog where contno=c.otherNo and state > '3')) "
					     + " and exists(select RiskCode from LMRisk where ( RNewFlAg!='N') and riskcode=a.RiskCode ) "
					     + " and a.ContNo=b.ContNo "
//					     + " and a.ContNo='000009809000007'"
					     + " and ((select count(1) from LJTempFee where TempFeeNo= c.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
					     + " and a.riskcode not in('320106','120706') "
						 + " and not exists(select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.riskcode not in ('332301','334801','340501','340601','340602') and lmriskapp.risktype4='4' and lcpol.Contno = b.ContNo ) "
						 + " group by c.getnoticeno,c.otherno  with ur ";
					  
		  
	    System.out.println(ContSql);
	    ExeSQL tEexSQL = new ExeSQL();
	    SSRS tSSRS = tEexSQL.execSQL(ContSql);
	    
	    int succCount=0;
	    String tLimit = PubFun.getNoLimit(managecom);
	    String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

	    for(int i=1;i<=tSSRS.getMaxRow();i++)
	    {
	    	  String tGetNoticeNo =tSSRS.GetText(i,1);
	          String Contno =tSSRS.GetText(i,2);
	          LCContDB tLCcontDB = new LCContDB();
	          LCContSchema tLCContSchema = new LCContSchema();
	          tLCcontDB.setContNo(Contno);
	          if(!tLCcontDB.getInfo())
	          {
	              System.out.println ("查询非万能保单["+Contno+"]失败,因此自动核销失败！");
	              mErrors.addOneError("查询非万能保单["+Contno+"]失败,因此自动核销失败！");
	              mFlag=false;
	              continue;
	          }
		      tLCContSchema =tLCcontDB.getSchema();
		      VData tVData = new VData();
	
		      tVData.add(tLCContSchema);
		      tVData.add(tGI);
		      //这个原来用了一个全局的TransferData，每次放GetNoticeNo的时候会重复所以改成局部的。
		      TransferData  tTransferData = new TransferData();
		      tTransferData.setNameAndValue("GrpSerNo",serNo);
		      tTransferData.setNameAndValue("GetNoticeNo",tGetNoticeNo);
		      tVData.add(tTransferData);
		      PRnewDueVerifyBL tPRnewDueVerifyBL = new PRnewDueVerifyBL();
		      if(!tPRnewDueVerifyBL.submitData(tVData,"VERIFY"))
		      {
		          //如果处理失败
		    	  System.out.println("收费号" + tGetNoticeNo + "核销失败，原因如下："+ tPRnewDueVerifyBL.mErrors.getFirstError());
		    	  mErrors.addOneError("收费号" + tGetNoticeNo + "核销失败，原因如下："+ tPRnewDueVerifyBL.mErrors.getFirstError());
		    	  mFlag=false;
		    	  continue;
		      }
		      else
		      {
		          succCount++;
		      }
	    }
	    if(succCount==tSSRS.getMaxRow())
	    {
	      System.out.println("本次批量核销全部成功！");
	    }
	  
	}
	private void wanNeng()
	{		    
	    String ContSql = "select c.getnoticeno,c.otherno "
	                   + "from LJSPayPerson a ,LCCont b,LJSPay c "
	                   + " where b.conttype = '1' " 
	                   + " and c.OtherNo=b.ContNo"
	                   + " and a.ContNo=b.ContNo"
	                   + " and a.lastpaytodate<='"+ CurrentDate +"'"
//	                   + " and a.ContNo='002103366000002'"
	                   +   whereParSql
	                   + " and a.getnoticeno = c.getnoticeno "
	                   + " and c.othernotype='2' "
	                   + " and b.manageCom like '" + managecom + "%' "	    
	                   + " and exists(select 1 from lcpol ,lmriskapp , lmrisk where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.risktype4='4' and  lmrisk.CPayFlag='Y' and lmrisk.riskcode=lmriskapp.riskcode and lcpol.Contno = b.ContNO )"	    
	                   + " and ((select count(1) from LJTempFee where TempFeeNo= c.GetNoticeNo  and ConfFlag='0' and confmakedate is not null and TempFeetype <>'18') > 0 or c.sumDuePayMoney <= 0) " 
	                   + " group by c.getnoticeno,c.otherno with ur ";
	    
	    System.out.println(ContSql);
	    ExeSQL tEexSQL = new ExeSQL();
	    SSRS tSSRS = tEexSQL.execSQL(ContSql);

	    int succCount=0;
	    String tLimit = PubFun.getNoLimit(managecom);
	    String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

	    for(int i=1;i<=tSSRS.getMaxRow();i++)
	    {
	        String tGetNoticeNo =tSSRS.GetText(i,1);
	        String Contno =tSSRS.GetText(i,2);
	        LCContDB tLCcontDB = new LCContDB();
	        LCContSchema tLCContSchema = new LCContSchema();
	        tLCcontDB.setContNo(Contno);
	        if(!tLCcontDB.getInfo())
	        {
	        	System.out.println("查询万能保单[" + Contno + "]失败,因此自动核销失败！");
	        	mErrors.addOneError("查询万能保单[" + Contno + "]失败,因此自动核销失败！");
	        	mFlag=false;
	            continue ;
	        }
	        tLCContSchema =tLCcontDB.getSchema();
	        VData tVData = new VData();

	        tVData.add(tLCContSchema);
	        tVData.add(tGI);
	        TransferData  tTransferData = new TransferData();
	        tTransferData.setNameAndValue("GrpSerNo",serNo);
	        tTransferData.setNameAndValue("GetNoticeNo",tGetNoticeNo);

	        tVData.add(tTransferData);
	        OmnipDueVerifyBL tOmnipDueVerifyBL = new OmnipDueVerifyBL();
	        if(!tOmnipDueVerifyBL.submitData(tVData,"VERIFY"))
	        {
	            //如果处理失败
	    	    System.out.println("万能收费号" + tGetNoticeNo + "核销失败，原因如下："+ tOmnipDueVerifyBL.mErrors.getFirstError());
	    	    mErrors.addOneError("万能收费号" + tGetNoticeNo + "核销失败，原因如下："+ tOmnipDueVerifyBL.mErrors.getFirstError());
	    	    mFlag=false;
	    	    continue ;
	        }
	        else
	        {
	            succCount++;
	        }
	    }
	    if(succCount==tSSRS.getMaxRow())
	    {
	        System.out.println("本次批量核销全部成功！");
	    }

    }

	
	
	
	 public static void main(String[] args)
	 {
		 BqXuQiHeXiaoTask tBqXuQiHeXiaoTask = new BqXuQiHeXiaoTask();
		 tBqXuQiHeXiaoTask.run();
	 }
}
