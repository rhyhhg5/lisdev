package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.ygz.GetBqPremBillBL;
import com.sinosoft.lis.ygz.GetXqPremBillBL;
import com.sinosoft.lis.ygz.GetXqPremBillSZBL;
import com.sinosoft.lis.ygz.GrpInvoiceDateSend;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YgzSZXQFpTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	
	public YgzSZXQFpTask() {

	}
	
	/**
	 * run方法
	 */
	public void run(){
		System.out.println("======================YgzFpTask Execute  !!!======================");
		
		/**
		 * 往同步保单迁出数据中出入日期的值
		 */
		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		tInputData.add(tTransferData);
		

		long startGetXqPremBillBL = System.currentTimeMillis();
		try {
			/**
			 * 续期发票数据提取
			 */
			GetXqPremBillSZBL tGetXqPremBillSZBL = new GetXqPremBillSZBL(); 
			if(!tGetXqPremBillSZBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetXqPremBillSZBL.mErrors);
				System.out.println("续期发票数据提取报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("续期发票数据提取报错：");
			e.printStackTrace();
		}
		long endGetXqPremBillBL = System.currentTimeMillis();
		System.out.println("========== GetXqPremBillBL续期发票数据提取批处理-耗时： "+((endGetXqPremBillBL -startGetXqPremBillBL)/1000/60)+"分钟");


	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		YgzSZXQFpTask tYgzSZXQFpTask = new YgzSZXQFpTask();
		
		tYgzSZXQFpTask.run();
	}
}
