/*
 * <p>ClassName: AdjustAgentBL </p>
 * <p>Description: AdjustAgentBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.taskservice;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.*;

public class LACredentialsBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private String mOperate;
    private String mStartDate;
    private String mEndDate;
    private String mManageCom;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    // 展业证信息存储
    private LACertificationSet mLACertificationSet = new LACertificationSet();
    private LACertificationBSet mLACertificationBSet = new LACertificationBSet();
    // 资格证信息存储
    private LAQualificationSet mLAQualificationSet = new LAQualificationSet();
    private LAQualificationBSet mLAQualificationBSet = new LAQualificationBSet();
    int temp;
    

    /** 业务处理相关变量 */
    

    private MMap mMap=new MMap();

    public LACredentialsBL()
    {
    }

    public static void main(String[] args)
    {
        String as = "2003-11-21";
        System.out.println(PubFun.calDate(as, -1, "M", null));
        String tDay = as.substring(as.lastIndexOf("-") + 1);
        HuiFangBL tHuiFangBL = new HuiFangBL();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "000"; //系统自签
        tG.ManageCom = "86"; //默认总公司
        VData tVData=new VData();
        tVData.addElement(tG);
        tVData.addElement("2012-11-1");
        tVData.addElement("2012-11-2");
        tVData.addElement("86371400");
        tHuiFangBL.submitData(tVData, "");
    	
    	
    }


    /**
       * 传输数据的公共方法
       * @param: cInputData 输入的数据
       * cOperate 数据操作
       * @return:
       */
      public boolean submitData(VData cInputData, String cOperate)
      {
          //将操作数据拷贝到本类中
          this.mOperate = cOperate;
          //得到外部传入的数据,将数据备份到本类中
          if (!getInputData(cInputData))
          {
              return false;
          }
          if (!dealData())
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "LACredentialsBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败LACredentialsBL!";
              this.mErrors.addOneError(tError);
              return false;
          }
          //准备往后台的数据
          if (!prepareOutputData())
          {
              return false;
          }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LACredentialsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        
          return true;
    }


      /**
      * 从输入数据中得到所有对象
      *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
      */
       private boolean getInputData(VData cInputData)
       {
           mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
           this.mStartDate = (String) cInputData.getObject(1);
           this.mManageCom=(String)cInputData.getObject(2);
           System.out.println("mStartDate"+mStartDate);
           if (mGlobalInput == null)
           {
               // @@错误处理
               CError tError = new CError();
               tError.moduleName = "LACredentialsBL";
               tError.functionName = "getInputData";
               tError.errorMessage = "没有得到足够的信息！";
               this.mErrors.addOneError(tError);
               return false;
           }
           return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {    
    	String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 19);// 一家分公司一个批次号
    	//  查询出目前系统当中展业证有效止期小于等于当前日期 并且状态为有效的数据
    	String sql = "select * from LACertification where 1=1 and validend<'"+this.mStartDate+"' and state = '0' " +
    			"and exists(select '1' from laagent where branchtype = '1' and branchtype2 ='01' and managecom like '"+this.mManageCom+"%' and agentcode =LACertification.agentcode )";
    	LACertificationDB tLACertificationDB = new LACertificationDB();
    	LACertificationSet tLACertificationSet = new LACertificationSet();
    	tLACertificationSet=tLACertificationDB.executeQuery(sql);
    	System.out.println("打印sql如下："+sql);
    	System.out.println("个数如下："+tLACertificationSet.size());
    	if(tLACertificationSet.size()>0)
    	{
    		
    		for(int i = 1;i<=tLACertificationSet.size();i++)
    		{
    			LACertificationSchema tLACertificationSchema = new LACertificationSchema();
    			tLACertificationSchema = tLACertificationSet.get(i).getSchema();
    			LACertificationBSchema tLACertificationBSchema = new LACertificationBSchema();
    			Reflections tReflections = new Reflections();
    			tReflections.transFields(tLACertificationBSchema,tLACertificationSchema);
    			// 备份信息处理
         	    tLACertificationBSchema.setEdorNo(tEdorNo);
        	    tLACertificationBSchema.setOldMakeDate(tLACertificationSchema.getMakeDate());
        	    tLACertificationBSchema.setOldMakeTime(tLACertificationSchema.getMakeTime());
        	    tLACertificationBSchema.setOldModifyDate(tLACertificationSchema.getModifyDate());
        	    tLACertificationBSchema.setOldModifyTime(tLACertificationSchema.getModifyTime());
        	    tLACertificationBSchema.setOldOperator(tLACertificationSchema.getOperator());
        	    tLACertificationBSchema.setMakeDate(currentDate);
        	    tLACertificationBSchema.setMakeTime(currentTime);
        	    tLACertificationBSchema.setModifyDate(currentDate);
        	    tLACertificationBSchema.setModifyTime(currentTime);
        	    tLACertificationBSchema.setOperator(this.mGlobalInput.Operator);
        	    this.mLACertificationBSet.add(tLACertificationBSchema);// 存储到set集合中
        	    
    			tLACertificationSchema.setState("1");// 状态置为失效
    			tLACertificationSchema.setInvalidDate(this.currentDate);
    			tLACertificationSchema.setInvalidRsn("批处理失效处理");
    			tLACertificationSchema.setOperator(this.mGlobalInput.Operator);
    			tLACertificationSchema.setModifyDate(this.currentDate);
    			tLACertificationSchema.setModifyTime(this.currentTime);
    			this.mLACertificationSet.add(tLACertificationSchema);
    			
    		}
    	}
    	mMap.put(mLACertificationBSet, "INSERT");
    	mMap.put(mLACertificationSet, "UPDATE");
    	
        //查询出目前系统当中资格证有效止期小于等于当前日期 并且状态为有效的数据
    	 sql = "select * from LAQualification where 1=1 and validend<'"+this.mStartDate+"' and state = '0' " +
		"and exists(select '1' from laagent where branchtype = '1' and branchtype2 ='01' and managecom like '"+this.mManageCom+"%' and agentcode =LAQualification.agentcode )";

    	 LAQualificationDB tLAQualificationDB = new LAQualificationDB();
    	 LAQualificationSet tLAQualificationSet = new LAQualificationSet();
    	 tLAQualificationSet=tLAQualificationDB.executeQuery(sql);
    	 System.out.println("资格证打印sql 如下:"+sql);
    	 System.out.println("资格证打印sql 如下:"+tLAQualificationSet.size());
    	 if(tLAQualificationSet.size()>0)
    	 {
    		 for(int j = 1;j<=tLAQualificationSet.size();j++)
    		 {
    			 LAQualificationSchema tLAQualificationSchema = new LAQualificationSchema();
    			 tLAQualificationSchema=tLAQualificationSet.get(j).getSchema();
                 // 备份资格证信息
    			 LAQualificationBSchema tLAQualificationBSchema = new LAQualificationBSchema();
    			 Reflections tReflections = new Reflections();
    			 tReflections.transFields(tLAQualificationBSchema, tLAQualificationSchema);
    			 tLAQualificationBSchema.setEdorNo(tEdorNo);// 存储业务号
    			 tLAQualificationBSchema.setMakeDate1(tLAQualificationSchema.getMakeDate());
    			 tLAQualificationBSchema.setMakeTime1(tLAQualificationSchema.getMakeTime());
    			 tLAQualificationBSchema.setOperator1(tLAQualificationSchema.getOperator());
    			 tLAQualificationBSchema.setOperator(mGlobalInput.Operator);
    			 tLAQualificationBSchema.setMakeDate(currentDate);
    			 tLAQualificationBSchema.setMakeTime(currentTime);
    			 tLAQualificationBSchema.setModifyDate(currentDate);
    			 tLAQualificationBSchema.setModifyTime(currentTime);
    			 this.mLAQualificationBSet.add(tLAQualificationBSchema);
    			 
    			 
    			 // 资格证信息调整
    			 tLAQualificationSchema.setState("1");// 状态置为失效
    			 tLAQualificationSchema.setInvalidDate(this.currentDate);
    			 tLAQualificationSchema.setInvalidRsn("批处理失效处理");
    			 tLAQualificationSchema.setOperator(this.mGlobalInput.Operator);
    			 tLAQualificationSchema.setModifyDate(this.currentDate);
    			 tLAQualificationSchema.setModifyTime(this.currentTime);
    			 this.mLAQualificationSet.add(tLAQualificationSchema);		 
    		 }	 
    	 }
     	mMap.put(mLAQualificationBSet, "INSERT");
    	mMap.put(mLAQualificationSet, "UPDATE");
        return true;
    }



    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
          this.mInputData.add(mMap);
          return true;
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACredentialsBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public MMap getMapResult() {
        return mMap;
    }
}