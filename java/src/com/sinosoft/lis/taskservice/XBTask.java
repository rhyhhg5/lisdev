package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.xb.*;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author fuxin
 * @version 1.0
 */

//专门处理续保错误数据和冗余数据
public class XBTask extends TaskThread{

    CErrors mErrors = new CErrors();

    MMap mMap = new MMap();

    GlobalInput mGlobalInput = new GlobalInput();


    public XBTask() 
    {
    }

    public void run()
    {
        System.out.println("续保错误数据批处理开始！");
        XBRun tXBRun = new XBRun();
        if(!tXBRun.submitData())
        {
        	System.out.println("续保错误数据批处理失败！");
        }
        System.out.println("续保错误数据批处理完成！");
    }
    public static void main(String[] args)
    {
        XBTask x = new XBTask();
        x.run();
    }
}
