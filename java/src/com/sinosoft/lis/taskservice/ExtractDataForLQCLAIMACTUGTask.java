package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.db.SolidificationLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.SolidificationLogSchema;
import com.sinosoft.lis.vschema.LQCLAIMACTUGSet;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;

public class ExtractDataForLQCLAIMACTUGTask extends TaskThread 
{
	public String mCurrentDate = PubFun.getCurrentDate();
//	public String mCurrentDate = "2017-8-12";
	private SolidificationLogSchema mSolidificationLogSchema = new SolidificationLogSchema();

	public void run() 
	{
		System.out.println("ExtractDataForLQCLAIMACTUGTask批处理开始执行！");
		String tSolidificationLogSQL = "select * from SolidificationLog where TableName = 'LQCLAIMACTUG' with ur";
		SolidificationLogDB tSolidificationLogDB = new SolidificationLogDB();
		mSolidificationLogSchema = tSolidificationLogDB.executeQuery(tSolidificationLogSQL).get(1);
		String tRealEndDate = mSolidificationLogSchema.getEndDate();
		String tState = mSolidificationLogSchema.getState();
		if("00".equals(tState))
		{
			System.out.println("--需要删除脏数据--");
			if(!deleteDirtyData(tRealEndDate))
			{
				//删除失败
				System.out.println("删除"+tRealEndDate+"天脏数据失败,中断执行");
				return;
			}
		}
		else if ("11".equals(tState)) 
		{
			tRealEndDate = PubFun.calDate(tRealEndDate, 1, "D", "");
		}
		// 日期格式转换类
		FDate tFDate = new FDate();
		Date tStartDate = tFDate.getDate(tRealEndDate);
		Date tEndDate = tFDate.getDate(mCurrentDate);
		while (tStartDate.compareTo(tEndDate) < 0) 
		{
			String tModifyDate = tFDate.getString(tStartDate);
			System.out.println("开始--提取"+tModifyDate+"的数据");
			mSolidificationLogSchema.setEndDate(tModifyDate);;
			if (!insertData(tModifyDate) || !deleteData(tModifyDate)) 
			{
				tSolidificationLogDB.setSchema(mSolidificationLogSchema);
				tSolidificationLogDB.update();
				break;
			}
			mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
			mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
			mSolidificationLogSchema.setState("11");
			mSolidificationLogSchema.setExceptionInfo("处理成功");
			tSolidificationLogDB.setSchema(mSolidificationLogSchema);
			tSolidificationLogDB.update();
			System.out.println("结束---提取"+tModifyDate+"的数据");
			tStartDate = PubFun.calDate(tStartDate, 1, "D", null);
		}

	}

	private boolean insertData(String cModifyDate) 
	{
		String tQuerySQL = "SELECT 'BF' feetype, 'C' type, c.managecom managecom, a.grppolno grppolno, "
				+ " a. riskcode riskcode, c. salechnl salechnl, c.signdate signdate, "
				+ " a.makedate makedate, cast(SUM(a.SumActuPayMoney) as decimal(12, 2)) sumactupaymoney "
				+ " FROM ljapaygrp a, lcgrpcont c "
				+ " WHERE "
				+ " a. endorsementno IS NULL "
				+ " and c.grpcontno = a. grpcontno "
				+ " and a.paytype<>'YF' "
				+ " and c.appflag = '1' "
				+ " and a.makedate  = '" + cModifyDate + "'"
				+ " group by "
				+ " c.managecom, a.grppolno, a.riskcode, c. salechnl, c.signdate, a. makedate "
				+ " union all "
				+ " SELECT 'BF' feetype, 'B' type, c. managecom managecom, a. grppolno grppolno, "
				+ " a.riskcode riskcode, c.salechnl salechnl, c.signdate signdate, a.makedate makedate, "
				+ " cast(SUM(a. SumActuPayMoney) as decimal(12, 2)) sumactupaymoney "
				+ " FROM "
				+ " ljapaygrp a, lbgrpcont c "
				+ " WHERE "
				+ " a.endorsementno IS NULL "
				+ " and c.grpcontno = a. grpcontno "
				+ " and a.paytype<>'YF' "
				+ " and c.appflag = '1' "
				+ " and a.makedate = '" + cModifyDate + "'"
				+ " group by "
				+ " c.managecom, a.grppolno, a. riskcode, c. salechnl, c. signdate, a.makedate "
				+ " union all "
				+ " SELECT 'BQ' feetype, 'C' type, c. managecom managecom, a.grppolno grppolno, "
				+ " a.riskcode riskcode, c.salechnl salechnl, c. signdate signdate, a.makedate makedate, "
				+ " cast(SUM(a.getmoney) as decimal(12, 2)) sumactupaymoney "
				+ " FROM "
				+ " ljagetendorse a, lcgrpcont c "
				+ " WHERE "
				+ " c.grpcontno = a. grpcontno "
				+ " and c.appflag = '1' "
				+ " and a.makedate = '" + cModifyDate + "'"
				+ " group by c.managecom, a.grppolno, a.riskcode, c. salechnl, c.signdate, a.makedate "
				+ " union all "
				+ " SELECT 'BQ' feetype, 'B' type, c. managecom managecom, a.grppolno grppolno, "
				+ " a.riskcode riskcode, c. salechnl salechnl, c.signdate signdate, a.makedate makedate, "
				+ " cast(SUM(a.getmoney) as decimal(12, 2)) sumactupaymoney "
				+ " FROM "
				+ " ljagetendorse a, lbgrpcont c "
				+ " WHERE "
				+ " c. grpcontno = a. grpcontno "
				+ " and c.appflag = '1' "
				+ " and a.makedate = '" + cModifyDate + "'"
				+ " group by "
				+ " c.managecom, a.grppolno, a. riskcode, c. salechnl, c. signdate, a.makedate with ur";
		System.out.println("执行插入时的查询sql:"+tQuerySQL);
		LQCLAIMACTUGSet tLQCLAIMACTUGSet = new LQCLAIMACTUGSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLQCLAIMACTUGSet, tQuerySQL);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		do 
		{
			tRSWrapper.getData();
			if(null==tLQCLAIMACTUGSet||0==tLQCLAIMACTUGSet.size())
			{
				return true;
			}
			tMMap.put(tLQCLAIMACTUGSet, "INSERT");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tVData, "")) 
			{
				System.out.println("ExtractDataForLQCLAIMACTUGTask 批处理执行异常！");
				mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
				mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
				mSolidificationLogSchema.setState("00");
				mSolidificationLogSchema.setExceptionInfo("提取" + cModifyDate
						+ "天数据更新时失败");
				return false;
			}
			tMMap = new MMap();
			tVData.clear();
			//这里的跳出条件与tRSWrapper.getData()有关，这个方法表示一次取出5000条数据，
			//当数据取完时，就会跳出循环
		} while (tLQCLAIMACTUGSet.size() > 0);
		return true;
	}
	
	private boolean deleteData(String cModifyDate)
	{
		System.out.println("执行delete方法");
		String tQuerySQL = "select * from LQCLAIMACTUG where "
				+ " (grppolno in(select b.grppolno grppolno from lbgrpcont a,lbgrppol b where a.grpcontno = b.grpcontno and b.modifydate = '"+ cModifyDate +"') and type = 'C') " 
				+ " or " 
				+ " (grppolno in(select b.grppolno grppolno from lcgrpcont a,lcgrppol b where a.grpcontno = b.grpcontno and b.modifydate = '"+ cModifyDate +"') and type = 'B')"
				+ " with ur";
		System.out.println("执行删除时的查询sql："+tQuerySQL);
		RSWrapper tRSWrapper = new RSWrapper();
		LQCLAIMACTUGSet tDeleteLQCLAIMACTUGSet = new LQCLAIMACTUGSet();
		tRSWrapper.prepareData(tDeleteLQCLAIMACTUGSet, tQuerySQL);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		do
		{
			tRSWrapper.getData();
			if(null==tDeleteLQCLAIMACTUGSet||0==tDeleteLQCLAIMACTUGSet.size())
			{
				return true;
			}
			tMMap.put(tDeleteLQCLAIMACTUGSet, "DELETE");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if(!tPubSubmit.submitData(tVData, ""))
			{
				System.out.println("ExtractDataForLQCLAIMACTUGTask 批处理删除执行异常！");
				mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
				mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
				mSolidificationLogSchema.setState("00");
				mSolidificationLogSchema.setExceptionInfo("删除"+cModifyDate+"天数据时失败");
				return false;
			}
			tMMap = new MMap();
			tVData.clear();
		}while(tDeleteLQCLAIMACTUGSet.size()>0);
		return true;
	}
	private boolean deleteDirtyData(String cRealEndDate)
	{
		String tDeleteDirtySql = "select * from LQCLAIMACTUG where makedate  = '" + cRealEndDate +"' with ur";
		LQCLAIMACTUGSet tDeleteLQCLAIMACTUGSet = new LQCLAIMACTUGSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tDeleteLQCLAIMACTUGSet, tDeleteDirtySql);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		do
		{
			tRSWrapper.getData();
			if(null==tDeleteLQCLAIMACTUGSet||0==tDeleteLQCLAIMACTUGSet.size())
			{
				return true;
			}
			tMMap.put(tDeleteLQCLAIMACTUGSet, "DELETE");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if(!tPubSubmit.submitData(tVData, ""))
			{
				System.out.println("ExtractDataForLQCLAIMACTUGTask 批处理执行异常！");
				mSolidificationLogSchema.setExceptionInfo("删除"+cRealEndDate+"天脏数据时失败");
				return false;
			}
			tMMap = new MMap();
			tVData.clear();
		}while(tDeleteLQCLAIMACTUGSet.size()>0);
		return true;
	}
	public static void main(String[] args) 
	{
		ExtractDataForLQCLAIMACTUGTask tExtractDataForLQCLAIMACTUGTask = 
				new ExtractDataForLQCLAIMACTUGTask();
		tExtractDataForLQCLAIMACTUGTask.run();
	}

}
