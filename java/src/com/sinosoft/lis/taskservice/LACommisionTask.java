package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.agentcalculate.*;

/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: SinoSoft</p>
 * @author xiangchun
 * @version 1.0
 */

public class LACommisionTask extends TaskThread {
    private int strSql;
    private static int intMaxIndex;
    private SSRS mSSRS1 = new SSRS();
    public LACommisionTask() {
        intMaxIndex = 1;
    }


    public void run() {
        System.out.println("");
        System.out.println(
                "＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
        System.out.println("参数个数:" + mParameters.size());
        System.out.println("");
        // Write your code here

        LACommisionTask a = new LACommisionTask();
        SSRS tSSRS=new SSRS();
        tSSRS = a.EasyQuery();
        if(!a.getSaleBranchType())
        {
            return ;
        }
        int SaleCount = a.mSSRS1.getMaxRow();
        //开始批量签单
        for (int i = 1; i <= intMaxIndex; i++)
        {
            //先查询提数的记录集
            String tManageCom = tSSRS.GetText(i, 1) ;
            for(int k = 1;k <= SaleCount; k++)
            {
                String tBranchType = a.mSSRS1.GetText(k, 1);
                String tBranchType2 = a.mSSRS1.GetText(k, 2);
                
                if (!a.getDate(tManageCom , tBranchType, tBranchType2))
                {
                    //如果提数不成功,继续提数下一条;
                    continue;
                }

            }

        }
        System.out.println("*******");
//        DealDataOfWageNoIsNull tDealDataOfWageNoIsNull = new DealDataOfWageNoIsNull();
//        //根据新需求 ：#2655河北历史数据追加保费不校验回访日期  添加对于薪资月为空的数据中，对于追加保费的重新进行薪资月计算
//        String tBranchType ="1";
//        String tBranchType2 ="01";
//        String sql= "select code from ldcode where codetype='wagehistoryvisit'";
//        ExeSQL exeSQL = new ExeSQL();
//        SSRS ssrs = new SSRS();
//        ssrs = exeSQL.execSQL(sql);
//        for(int i=1;i<=ssrs.getMaxRow();i++){
//        	String tManageCom=ssrs.GetText(i, 1);
//        	System.out.println("apple:"+tManageCom);
//        	if(!tDealDataOfWageNoIsNull.dealData(tBranchType,tBranchType2,tManageCom))
//            {
//            	System.out.println("DealDataOfWageNoIsNull:河北追加保费回访日期薪资月计算失败");
//            }
//        }
        //银代新单折标和主管间接绩效
//        LABankManageCommision tLABankManageCommision = new LABankManageCommision();
//        if(!tLABankManageCommision.dealData())
//        {
//        	System.out.println("LABankManageCommision:银代新单折标和主管间接绩效计算失败");
//        }

    }

    public static void main(String[] args) {

        LACommisionTask a = new LACommisionTask();
        SSRS tSSRS=new SSRS();
        tSSRS = a.EasyQuery();
//        if(!a.getSaleBranchType())
//        {
//           return;
//        }
//        int SaleCount=a.mSSRS1.getMaxRow();
//        //开始提数
//        for (int i = 1; i <=intMaxIndex; i++) {
//            //先查询提数的记录集
//            String tManageCom = tSSRS.GetText(i, 1) ;
//            for(int k = 1;k <= SaleCount; k++)
//            {
//                String tBranchType = a.mSSRS1.GetText(k, 1);
//                String tBranchType2 = a.mSSRS1.GetText(k, 2);
//
//                if (!a.getDate(tManageCom , tBranchType, tBranchType2)) {
//                    //如果提数不成功,继续提数下一条;
//                    continue;
//                }
//            }
//        }
        DealDataOfWageNoIsNull tDealDataOfWageNoIsNull = new DealDataOfWageNoIsNull();
        //根据新需求 ：#2655河北历史数据追加保费不校验回访日期  添加对于薪资月为空的数据中，对于追加保费的重新进行薪资月计算
        String tBranchType ="1";
        String tBranchType2 ="01";
        String sql= "select code from ldcode where codetype='wagehistoryvisit'";
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = new SSRS();
        ssrs = exeSQL.execSQL(sql);
        for(int i=1;i<=ssrs.getMaxRow();i++){
        	String tManageCom=ssrs.GetText(i, 1);
        	System.out.println("cindy:"+tManageCom);
        	if(!tDealDataOfWageNoIsNull.dealData(tBranchType,tBranchType2,tManageCom))
            {
            	System.out.println("DealDataOfWageNoIsNull:河北追加保费回访日期薪资月计算失败");
            }
        }
    }
    private boolean getSaleBranchType()
    {
        String tSql = "select distinct branchtype,branchtype2 from labranchlevel and where 1=1 and branchtype2 not in ('04','05')" +
        		" union " +
        		" (select '9','01' from dual) with ur";
        ExeSQL tExeSQL = new ExeSQL();
        mSSRS1 = tExeSQL.execSQL(tSql);
        if(mSSRS1 == null || mSSRS1.getMaxRow() <= 0 )
        {
            return false;
        }
        return true;
    }
    //
    public boolean getDate(String ManageCom,String tBranchType,String tBranchType2) {
        String FlagStr = "";
        TransferData tTransferData = new TransferData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "000"; //系统自签
        tG.ManageCom = "86"; //默认总公司
        //接收信息
        // 投保单列表
        LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
        String dateSql = "Select date('" + PubFun.getCurrentDate() +
                         "')-1 day from dual";
        ExeSQL texeSQL = new ExeSQL();
        String getDate = texeSQL.getOneValue(dateSql);
        tLAWageLogSchema.setManageCom(ManageCom);
        tLAWageLogSchema.setStartDate(getDate);
        tLAWageLogSchema.setEndDate(getDate);
        tLAWageLogSchema.setBranchType(tBranchType);  //0:表示三种展业类型的数据都提
        tLAWageLogSchema.setBranchType2(tBranchType2);  //0:表示三种展业类型的数据都提
        AgentWageCalSaveUI tAgentWageCalSaveUI=new AgentWageCalSaveUI();
        VData tVData=new VData();
        tVData.addElement(tG);
        tVData.addElement(tLAWageLogSchema);
        if (!tAgentWageCalSaveUI.submitData(tVData,""))
        {

            FlagStr="Fail";
            return false;
         }
          else
          {
            AgentWageCalDoUI tAgentWageCalDoUI=new AgentWageCalDoUI();
            if (!tAgentWageCalDoUI.submitData(tVData,""))
            {
                FlagStr="Fail";
                return false;
            }
            else
            {
            	//团险社保业务变更commdire=‘0’
            	if(tBranchType.equals("2")&&tBranchType2.equals("01")){
            		GrpSocialInsuranceBL tGrpSocialInsuranceBL=new GrpSocialInsuranceBL();
            		if (!tGrpSocialInsuranceBL.submitData(tVData,""))
                    {
                        FlagStr="Fail";
                        return false;
                    }
            		else
            		{
            			 System.out.print(" 提数成功! ");
                         return true;	
            		}
            	}
            	else {
                    System.out.print(" 提数成功! ");
                    return true;
            	}
            } // end of if
          }
    }


    public  SSRS EasyQuery() {

        String strError = "";
        Integer intStart = new Integer(String.valueOf("1")); ;
        SSRS tSSRS = new SSRS();

        String sql =
                "select  comcode " +
                "from ldcom where 1=1  and  " +
                "sign='1'  and  length(trim(comcode))=8  order by comcode" ;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        intMaxIndex = tSSRS.getMaxRow();
//        String[] getRowData = tSSRS.getRowData(intStartIndex);
        return tSSRS;
    }
}
