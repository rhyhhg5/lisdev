package com.sinosoft.lis.taskservice;
import com.sinosoft.lis.taskservice.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author ZhangXing
 * @version 1.0
 */

public class UrgerNoticeTask extends TaskThread
{
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;
  private String mOperator;    //操作员

  public UrgerNoticeTask()
  {
  }


  public void run()
  {
    /*
       查询出所有待催办的通知书
       03－体检；04－生调；85－问题件

    */
   String SqlSQL =  "select PrtSeq,OtherNo,ManageCom,ReqOperator,code from loprtmanager where prtseq in "
                  +"("
                  +" select prtseq from loprtmanager where stateflag = '1' and (UrgerFlag <> '1' or UrgerFlag is null) and code in ('03','05','85') and current date >= loprtmanager.formakedate "
                  +") "
                  +" and otherno in (select ContNo from lCCont where appflag='0' and uwflag not in ('1','8','a')) and PrtSeq not in (select doccode from es_doc_main where subtype in('TB15','TB21','TB22') ) order by otherno";
    System.out.println(SqlSQL);

    ExeSQL q_exesql = new ExeSQL();
    SSRS tssrs = new SSRS();
    tssrs = q_exesql.execSQL(SqlSQL);

    int m =  tssrs.getMaxRow();
    System.out.println("参数个数:" + m);
    LOPRTManagerSet tLOPRTManagerSet=new LOPRTManagerSet();

     GlobalInput tG = new GlobalInput();
     tG.Operator = "000";
     tG.ManageCom = "86";
     tG.ComCode = "86";

     for (int i = 1; i<=m; i++)
     {
         if(tssrs.GetText(i,5).equals("07"))
         {
           String tSql = "select distinct 1 from ljtempfee where otherno in (select prtno from lccont where contno = '"+tssrs.GetText(i,2)+"')"
                       +  " union select distinct 1 from lccont where paymode = '4' and contno = '"+tssrs.GetText(i,2)+"'";
           SSRS assrs = new SSRS();
           assrs = q_exesql.execSQL(tSql);
           if(assrs.MaxRow == 0)
           {
             continue;
           }
         }
         LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
         tLOPRTManagerSchema.setPrtSeq(tssrs.GetText(i,1));
         tLOPRTManagerSet.add(tLOPRTManagerSchema);

     }
     VData tVData = new VData();
     tVData.add(tLOPRTManagerSet);
     tVData.add(tG);
     UrgeNoticeBL tUrgeNoticeBL = new UrgeNoticeBL();
     if (tUrgeNoticeBL.submitData(tVData, "UPDATE") == false)
     {
         CError tError = new CError();
         tError.moduleName = "UWBatchManuNormGChkBL";
         tError.functionName = "callPol";
         tError.errorMessage = "调用错误UWManuNormGChkBL 操作员：" + mOperator + "）";
         this.mErrors.addOneError(tError);

     }
     System.out.println("＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");


        // Write your code here

  }
  public static void main(String[] args)
  {
    UrgerNoticeTask tUrgerNoticeTask = new UrgerNoticeTask();
    tUrgerNoticeTask.run();
  }


}
