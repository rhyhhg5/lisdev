package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LPGrpEdorItemDB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.UliInsuAccBala;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 个险万能险保单账户结算 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @version 1.0
 * @CreateDate：2007
 */
public class UliInsuAccBalaTask extends TaskThread
{
    public CErrors mErrors = new CErrors();

    // by gzh 20110104
    private String mGrpContNo = null;

    private GlobalInput mGI = new GlobalInput();

    public UliInsuAccBalaTask()
    {
        mGI.Operator = "001";
        mGI.ComCode = "86";
        mGI.ManageCom = mGI.ComCode;
    }

    public UliInsuAccBalaTask(GlobalInput cGlobalInput)
    {
        mGI = cGlobalInput;
    }

    public void run()
    {
    	int iSuccCount = 0;
        int iFailCount = 0;
//    	by gzh 20110104
    	ExeSQL tExeSQL = new ExeSQL();
    	String GrpContNoSql = "select distinct a.grpcontno from LCInsureAcc a,Interest000001 b "
	        +" where 1=1"
//	        +" and a.InsuAccNo = b.InsuAccNo"
//	        +" and a.BalaDate <= b.startDate"
	        +" and a.riskcode=b.riskcode "//新增团险万能产品了，所以这里要关联下险种
	        +" and a.BalaDate < b.enddate"
	        +" and  b.enddate<='"+PubFun.getCurrentDate()+"' "
	        +" and a.riskcode in (select riskcode from lmriskapp where risktype4='4')"
	        +" and a.ManageCom like '" + mGI.ManageCom + "%' "
	        +" and exists(select 1 from LCGrpPol where GrpPolNo = a.GrpPolNo and StateFlag = '1')";//StateFlag = '1'承保有效
    	if(mGrpContNo != null && !mGrpContNo.equals("")){
    		GrpContNoSql += " and a.grpcontno = '"+mGrpContNo+"'";
		}
    	SSRS GrpContNoSSRS = tExeSQL.execSQL(GrpContNoSql);
    	for(int j=1;j<=GrpContNoSSRS.getMaxRow();j++){
    		mGrpContNo = GrpContNoSSRS.GetText(j, 1);
    		if(!checkEdorItemState(mGrpContNo)){
    			System.out.println("团单"+mGrpContNo+"有未结案的保全，不能进行结算");
                continue;
    		}
    		//校验该团单能否可以进行结算
    		if(!checkClaimState(mGrpContNo))
            {
                System.out.println("团单"+mGrpContNo+"有未结案的理赔，不能进行结算");
                continue;
            }
			VData tVData = new VData();
            tVData.add(mGI);
            UliInsuAccBala tUliInsuAccBala = new UliInsuAccBala();
            try
            {
                if(!tUliInsuAccBala.submitData(tVData, "",mGrpContNo))
                {
                    iFailCount++;
                    String errInfo ="团单:" + mGrpContNo + " 结算失败: ";
                    errInfo += tUliInsuAccBala.mErrors.getErrContent();
                    mErrors.addOneError(errInfo);
                    System.out.println(errInfo);
                }
                else
                {
                    iSuccCount++;
                }
            }
            catch(Exception ex)
            {
            	String errInfo ="团单:" + mGrpContNo + " 结算失败: ";
                errInfo += "结算出现未知异常";
                System.out.println(errInfo);
                ex.printStackTrace();
                mErrors.addOneError(errInfo);
                iFailCount++;
            }
    	}
    }

    /**
     * 结算指定保单
     * @param cContNo String
     */
    public void runOneGrpCont(String cGrpContNo)
    {
        mGrpContNo = cGrpContNo;
        run();
    }

    /**
     * 校验能否进行结算
     * 险种有未完结的理赔不能结算，保单有未完结的保全不能结算
     * @param cLCInsureAccSchema LCInsureAccSchema
     * @return boolean
     */
    private boolean canBalance(LCInsureAccSchema cLCInsureAccSchema)
    {
        String tPolInfo = "保单号: " + cLCInsureAccSchema.getContNo()
                          + ", 被保人号码:" + cLCInsureAccSchema.getInsuredNo()
                          + ", 险种编码" + cLCInsureAccSchema.getRiskCode();

        //判断该账户当天是否已经结算过（防止重复结算插入为0的记录）
        String sql = " select 1 from dual "
                     + "where exists (select 'X' from LCInsureAccTrace "
                     + "             where MoneyType = 'LX' "
                     + "                and PayDate = Current Date "
                     + "                and InsuAccNo ='"
                     + cLCInsureAccSchema.getInsuAccNo() + "'"
                     + "               and PolNo ='"
                     + cLCInsureAccSchema.getPolNo() + "') ";
        String sHasBala = new ExeSQL().getOneValue(sql);
        if(sHasBala != null && sHasBala.equals("1"))
        {
            mErrors.addOneError("当天已经结算过，不需要结算:" + tPolInfo);
            return false;
        }

        if(!LLCaseCommon.checkClaimState(cLCInsureAccSchema.getPolNo()))
        {
            mErrors.addOneError("险种有未结案的理赔，不能进行结算");
            return false;
        }

        LPEdorItemSet set = CommonBL.getEdorItemMayChangePrem(
            cLCInsureAccSchema.getContNo());
        if(set.size() > 0)
        {
            String edorInfo = "";
            for(int i = 1; i <= set.size(); i++)
            {
                edorInfo += set.get(i).getEdorNo() + " "
                    + set.get(i).getEdorType() + ", ";
            }
            mErrors.addOneError("保单有未结案的保全项目:" + edorInfo);
            return false;
        }

        return true;
    }
    
    /**
     * 按照GrpContNo查询是否有理赔案件正在处理
     * @param aPolNo String
     * @return boolean
     */
    public boolean checkClaimState(String aGrpContNo) {
        boolean tClaimFlag = true;
        String tSQL = "SELECT 1 FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno " +
        				"AND b.rgtstate NOT IN ('11','12','14')"  //11	通知状态   12	给付状态 14	撤件状态
                      + " AND a.grpcontno='" + aGrpContNo + "'";
        ExeSQL exesql = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = exesql.execSQL(tSQL);
        if (tSSRS.getMaxRow() > 0) {
            tClaimFlag = false;
        }
        return tClaimFlag;
    }
    
    /**
     * 按照GrpContNo查询是否有保全案件正在处理
     * @param aPolNo String
     * @return boolean
     */
    public boolean checkEdorItemState(String aGrpContNo) {
    	String sql = "select a.* " + "from LPGrpEdorItem a, LPEdorApp b "
        + "where a.EdorAcceptNo = b.EdorAcceptNo "
        + "   and a.grpcontno = '" + aGrpContNo+ "' "
        + "   and b.EdorState != '0' "//批改状态(0-批改确认,1-批改申请录入完成,2-申请确认，3-申请未录入)
        + "   and a.EdorType not in(select Code from LDCode "
        + "               where CodeType = 'edortypechangeprem') ";//保全项目待描述状态
		System.out.println(sql);
		LPGrpEdorItemSet set = new LPGrpEdorItemDB().executeQuery(sql);
		if(set.size() > 0)
        {
            String edorInfo = "";
            for(int i = 1; i <= set.size(); i++)
            {
                edorInfo += set.get(i).getEdorNo() + " "
                    + set.get(i).getEdorType() + ", ";
            }
            mErrors.addOneError("保单有未结案的保全项目:" + edorInfo);
            return false;
        }
		return true;
    }
    // by gzh end
    public static void main(String[] args)
    {
        UliInsuAccBalaTask task = new UliInsuAccBalaTask();
        task.runOneGrpCont("00108203000002");
    }
}
