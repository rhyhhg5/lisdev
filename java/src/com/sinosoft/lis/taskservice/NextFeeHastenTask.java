package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 续期催缴接口，续期催缴分为团单续期催缴和个单续期催缴
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class NextFeeHastenTask extends TaskThread
{
    /**催缴类型， 保全*/
    public static final String HASTEN_TYPE_BQ = "1";
    /**催缴类型， 团单续期*/
    public static final String HASTEN_TYPE_NEXTFEEG = "2";
    /**催缴类型， 个单续期*/
    public static final String HASTEN_TYPE_NEXTFEEP = "3";

    private String message = "";

    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;

    /**
     * 处理业务逻辑的方法
     * @return boolean
     */
    public void run()
    {
        System.out.println("\n\n\n\n\n调用续期电话催缴批处理程序");

        if(mGlobalInput == null)
        {
            mGlobalInput = new GlobalInput();
            mGlobalInput.Operator = "Server";
            mGlobalInput.ComCode = "86";
            mGlobalInput.ManageCom = "86";
        }

        //团单续期催缴
        NextFeeHastenGTask gTask = new NextFeeHastenGTask(mGlobalInput);
        if(!gTask.submit())
        {
            mErrors.copyAllErrors(gTask.mErrors);
            mErrors.addOneError("团单续期催缴失败");
            System.out.println(gTask.mErrors.getErrContent());
        }
        else
        {
            System.out.println("团单续期催缴成功，共生成" + gTask.getHastenedCont()
                    + "个催缴任务");
        }

        //个单续期催缴
        NextFeeHastenPTask pTask = new NextFeeHastenPTask(mGlobalInput);
        pTask.submit();
        if(pTask.mErrors.needDealError())
        {
            mErrors.copyAllErrors(pTask.mErrors);
            System.out.println(pTask.mErrors.getErrContent());
        }

        String info = "成功催缴" + pTask.getSucCount() + "个应收记录，撤销"
                           + pTask.getCancelCount() + "个电话催收任务";
        if(pTask.getCancelCount() != 0 || pTask.getSucCount() != 0)
        {
            this.message = info;
        }
    }

    /**
     * 得到催缴个数成功情况和撤销情况
     * "成功催缴**个应收记录，撤销**"个电话催收任务"
     * @return String
     */
    public String getMessage()
    {
        return this.message;
    }

    /**
     * 由操作人员手动运行
     * @param tGlobalInput GlobalInput
     */
    public void oneCompany(GlobalInput tGlobalInput)
    {
        mGlobalInput = tGlobalInput;
        run();
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "pa0001";
        tGlobalInput.ComCode = "8653";
        NextFeeHastenTask t = new NextFeeHastenTask();
        t.run();
        //t.oneCompany(tGlobalInput);
        System.out.println(t.getMessage());
    }
}
