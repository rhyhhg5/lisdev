package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class YBKClaimMailTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	
	//设置生成日志需要的字段
	/** 发送状态 */
	public String SendFlag;
	
	/** 入机日期 */
	private String MakeDate;
	
	/** 设置操作员 */
	private String Operator = "001";
	
	/**设置流水号*/
	private String SerialNo;
	public String getSendFlag() {
		return SendFlag;
	}

	public void setSendFlag(String sendFlag) {
		SendFlag = sendFlag;
	}

	public String getRemark() {
		return Remark;
	}

	public void setRemark(String remark) {
		Remark = remark;
	}

	/** 入机时间 */
	private String MakeTime;
	
	/** 最后修改日期 */
	private String ModifyDate;
	
	/** 最后修改时间 */
	private String ModifyTime;
	
	/** 备注信息  */
	public String Remark;
	
	private String mWhereSQL ="";
	
	public YBKClaimMailTask(){
	}
	
	public YBKClaimMailTask(String tConsultNo){
		mWhereSQL = " and lc.ConsultNo = '"+tConsultNo+"' ";
	}
	
	
	public void run() {
		// 邮箱发送(用户,密码)
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
		SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS.GetText(1, 2),"picchealth");
		//设置操作员
		Operator = "001";
		//设置入机时间
		MakeDate = PubFun.getCurrentDate();
		MakeTime = PubFun.getCurrentTime();
		
		//获取未发送的且已上报的报案信息
		String sql = "select distinct lct.contno,lc.ConsultNo,lc.CustomerName,ld.idno,lm.mobile,y.AdmissionDiagnosis,y.Hospital from LLConsult lc,ldperson ld,LLMainAsk lm,YbkReport y,lccont lct,YBK_N01_LIS_ResponseInfo ybk"
				    +" where lc.CustomerNo=ld.CustomerNo "
				    +" and lc.LogNo=lm.LogNo "
				    +" and lc.ConsultNo=y.ConsultNo "
				    +" and y.PolicySequenceNo = ybk.PolicySequenceNo "
				    +" and ybk.prtno = lct.prtno "
					+" and not exists(select 1 from YbkDzbdLog where sendflag='1' and PolicyNo=lc.ConsultNo) " //未发送过邮件的
				    +" and exists(select 1 from YBK_C01_LIS_ResponseInfo where caseno=y.ConsultNo and responsecode='1' and  prtno is null) " //报案信息已上报平台的
				    + mWhereSQL
				    +" with ur";
		
		SSRS tSSRSR = new ExeSQL().execSQL(sql);
		
		//设置最后修改时间
		ModifyDate = PubFun.getCurrentDate();
		ModifyTime = PubFun.getCurrentTime();
		
		for(int i = 1;i <= tSSRSR.MaxRow;i++){
			
			String ContNo = tSSRSR.GetText(i, 1);
			String tConsultNo = tSSRSR.GetText(i, 2);
			String tCustomerName = tSSRSR.GetText(i, 3);
			String tIdno = tSSRSR.GetText(i, 4);
			String tMobile = tSSRSR.GetText(i, 5);
			String tADiagnosis = tSSRSR.GetText(i, 6);
			String tHospital = tSSRSR.GetText(i, 7);
			System.out.println("客户姓名为: " + tCustomerName);
			System.out.println("证件号码为: " + tIdno);
			//测试邮箱
			//String tEMail ="li_yunfeng@sinosoft.com.cn";   
			String tEMail ="ybgrlp@picchealth.com";

			
			//标记发送状态
			SendFlag = "1";
			Remark = "邮件已发送成功!";
			
			tMailSender.setSendInf("中国人民健康保险股份有限公司", "您好!\r\n医保卡报案已发送到核心系统中，请尽快处理。\r\n保单号："+ContNo+";\r\n被保险人姓名："+tCustomerName+";\r\n证件号码："+tIdno+";\r\n被保险人电话："+tMobile+";\r\n被保险人因‘"+tADiagnosis+"’原因在‘"+tHospital+"’医院就诊。","");
			tMailSender.setToAddress(tEMail, "", null);
			
			//发送邮件
			if (!tMailSender.sendMail()) {
				//修改标记发送状态
				SendFlag = "2";
				Remark = tMailSender.getErrorMessage();
				System.out.println(tMailSender.getErrorMessage());
			}
			
			System.out.println("发送状态为: " + SendFlag);
			System.out.println("备注信息为: " + Remark);
			
			//设置最后修改时间
			SerialNo = PubFun1.CreateMaxNo("YBKDZBDLOG", 20);
			//生成日志
			String Rsql = "insert into YbkDzbdLog(SerialNo,PolicyNo,Email,SendFlag,Remark,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime) " +
					"values('" + SerialNo + "','" + tConsultNo + "','" + tEMail + "','" + SendFlag + "','" + Remark + "','" + Operator + "','" + MakeDate + "','" + MakeTime + "','" + ModifyDate +"','" + ModifyTime +  
					"')";
			if(!new ExeSQL().execUpdateSQL(Rsql)){
				System.out.println("保存理赔报案"+tConsultNo+"的发送日志失败");
			}
		}
		
	}

	// 主方法 测试用
	public static void main(String[] args) {
		
		YBKClaimMailTask m = new YBKClaimMailTask("T3100170619000001");
		m.run();
	}
	
}