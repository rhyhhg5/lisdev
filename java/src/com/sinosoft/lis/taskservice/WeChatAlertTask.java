package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.operfee.CpayRnewMsgBL;
import com.sinosoft.lis.operfee.WeChatRenewalAlertBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 微信公众号发送续期缴费提醒
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: </p>
 *
 * @author LZJ
 * 
 */
public class WeChatAlertTask extends TaskThread{
	/*错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    String Content = "";
    String Email = "";
    String Mobile = "";
    // 输入数据的容器
    private VData mInputData = new VData();

    private MMap map = new MMap();
    public WeChatAlertTask() {

    }
    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run() {
        if (!sendCustomerMsg()) {
            return;
        }
    }
    private boolean sendCustomerMsg() {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        WeChatRenewalAlertBL tWeChatRenewalAlertBL = new WeChatRenewalAlertBL();
        if (!tWeChatRenewalAlertBL.submitData(mVData, "XQMSG")) {
            System.out.println("微信续期缴费提醒发送失败！");
            CError tError = new CError();
            tError.moduleName = "CpayRnewMsgBL";
            tError.functionName = "submitData";
            tError.errorMessage = "微信续期缴费提醒发送失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    public static void main(String[] args) {
		WeChatAlertTask alertTask = new WeChatAlertTask();
		alertTask.run();
	}
}
