package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.agentcalculate.NoCalWageRenewalData;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 *  对于个险孤儿单数据进行处理：个险中，对于2015-4-1以后的孤儿单数据，不计入到薪资中，即不流转财务
 * </p>
 *
 * <p>Copyright: Copyright (c) 2010</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Pangxy
 * @version 1.0
 */
public class NoCalRenewalTask extends TaskThread
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();
    public NoCalRenewalTask()
    {
    }

    /**
     * 批处理程序调用的入口，供批处理程序调用。
     * 方法内调用ActivationCardModifyBL.java进行业务逻辑处理。
     */
    public void run()
    {
        String sql ="select managecom,wageno from lawagehistory where branchtype='1' and branchtype2='01' and state ='14' and modifydate = current date   ";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sql);
        if(null!=tSSRS&&0<tSSRS.getMaxRow())
        {
        	for(int i =1;i<=tSSRS.getMaxRow();i++)
        	{
        		String tManageCom = tSSRS.GetText(i, 1);
        		String tWageNo = tSSRS.GetText(i, 2);
        		NoCalWageRenewalData tNoCalWageRenewalData = new NoCalWageRenewalData();
        		if(!tNoCalWageRenewalData.submitData(tManageCom, tWageNo))
    			{
        			continue;
    			}
        	}
        }
        //把昨天 标记为孤儿单的数据  中 commdire=‘3’  修改为  commdire='1'
        String tUPSQL ="update lacommision a set a.commdire ='1' where substr(a.operator,1,3)='ged' and a.commdire='3' and a.branchtype ='1' and a.branchtype2='01' and exists( select 1 from lawagehistory where branchtype='1' and branchtype2='01' and state ='14' and modifydate = current date -1 days and  a.managecom = managecom and a.wageno = wageno )";
        tExeSQL.execUpdateSQL(tUPSQL);
    }
   
    public static void main(String[] args)
    {
        NoCalRenewalTask tACardModifyTask = new NoCalRenewalTask();
        tACardModifyTask.run();
    }
}
