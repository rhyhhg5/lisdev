package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.agentcalculate.AgentWageOfCalWageNoData;
import com.sinosoft.lis.pubfun.PubFun;


 /**
  * 批处理优化后的批处理配置
  * @author yangyang  2017-8-28
  *
  */
public class AgentWageOfCalWageNoTask extends TaskThread {

    public void run() 
    {
    	long tStartTime = new Date().getTime();
    	System.out.println("AgentWageOfCalWageNoTask-->run :开始执行");
    	AgentWageOfCalWageNoData tAgentWageOfCalWageNoData = new AgentWageOfCalWageNoData();
    	tAgentWageOfCalWageNoData.setBaseValue("CALALLFYC", "LACOMMISION", "CALALLFYC");
    	String tSQL = "select * from lacommisionnew where  caldate is null and  commdire='1'  with ur";
    	tAgentWageOfCalWageNoData.dealData(tSQL, "sys");
    	System.out.println("AgentWageOfCalWageNoTask-->批处理执行完成:"+PubFun.getCurrentTime());
    	long tEndTime = new Date().getTime();
    	System.out.println("薪资月为空的数据处理时间花费"+(tEndTime-tStartTime)+"毫秒，大约"+(tEndTime-tStartTime)/3600/1000+"小时");
    }
    public static void main(String[] args) {
    	AgentWageOfCalWageNoTask tAgentWageOfCalWageNoTask = new AgentWageOfCalWageNoTask();
    	tAgentWageOfCalWageNoTask.run();
	}
    
}
