package com.sinosoft.lis.taskservice;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.httpclient.inf.NewSYQueryDealBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title:</p>
 * 
 * <P>Description:
 * 税优查询批次信息导入处理
 * </p>
 * 
 * <p>Copyright: Copyright (c) 2016</p>
 * 
 * <p>Company: sinosoft</p>
 * 
 * @author Administrator
 * @version 1.1
 */
public class SYQueryTask  extends TaskThread{

	/**错误容器*/
	public CErrors mErrors = new CErrors();
	
	private VData mVData = new VData();
	
	private String mBeforeDate ;
	
	public SYQueryTask(){
		
	}
	
	public void run(){
		
		//获取系统时间-1;
		SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
		Date beginDate = new Date();
		Calendar today=Calendar.getInstance();
		today.add(Calendar.DAY_OF_MONTH, -1);
		Date endDate= today.getTime();
		mBeforeDate=dft.format(endDate);
		
		System.out.println("获取时间为系统时间前一天"+ mBeforeDate);
		
		NewSYQueryDealBL tNewSYQueryDealBL = new NewSYQueryDealBL();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("CheckDate", mBeforeDate);
		mVData.add(tTransferData);
		tNewSYQueryDealBL.submitData(mVData, "ZBXPT");
		
	}
	

	public static void main(String[] args) {
	
		SYQueryTask tSYQueryTask = new SYQueryTask();
		tSYQueryTask.run();
	}

}
