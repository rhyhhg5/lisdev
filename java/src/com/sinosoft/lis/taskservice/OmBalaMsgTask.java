package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.operfee.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 万能月结客户通知短信
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 * Picchealth
 * <p>Company: </p>
 * Sinosoft
 * @author Zhanggm
 * @version 1.0
 */
public class OmBalaMsgTask extends TaskThread 
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    String Content = "";
    String Email = "";
    String Mobile = "";

    public OmBalaMsgTask() 
    {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run() 
    {
        OmBalaMsgBL tOmBalaMsgBL = new OmBalaMsgBL();
        if (!tOmBalaMsgBL.sendMessages()) 
        {
            System.out.println("万能月结客户通知短信发送失败！");
            CError tError = new CError();
            tError.moduleName = "OmBalaMsgBL";
            tError.functionName = "submitData";
            tError.errorMessage = "万能月结客户通知短信发送失败";
            this.mErrors.addOneError(tError);
        }
    }

    public static void main(String args[]) 
    {
        OmBalaMsgTask a = new OmBalaMsgTask();
        a.run();
    }
}
