package com.sinosoft.lis.taskservice;

import java.util.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.taskservice.HMMsgTask;


/**
 * <p>Title: 任务基类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author ZhangRong
 * @version 1.0
 */

public class Task { //extends Thread
    private Thread mTaskThread;
    private String mThreadName;
    private String mTaskID;
    private LDTaskPlanSchema mLDTaskPlanSchema;
    private LDTaskRunLogSchema mLDTaskRunLogSchema;
    private String mRunTime;
    private String mNextRunTime;
    private long mRunFrequence;
    private int mState;
    private String mTaskResult;
    private static long mSerialNo;
    private int mThreadState; //任务状态，在重载run()时根据状态进行线程的相应处理
    protected HashMap Parameters;
    static String TaskCode;

    private static final int THREAD_READY = 0;
    private static final int THREAD_RUNNING = 1;
    private static final int THREAD_SUSPEND = 2;
    private static final int THREAD_RESUME = 3;
    private static final int THREAD_TERMINATE = 4;

    public Task(String aThreadName) {
        mThreadName = aThreadName;
        mLDTaskRunLogSchema = new LDTaskRunLogSchema();
        Parameters = new HashMap();
        mRunTime = "";
        mRunFrequence = 0;
        mState = 0;
        mTaskResult = "";
        mSerialNo = 0;
        mThreadState = THREAD_READY;
    }

    public String getTaskID() {
        return mTaskID;
    }

    public LDTaskPlanSchema getTaskPlan() {
        return mLDTaskPlanSchema;
    }

    public void SetTaskPlan(LDTaskPlanSchema aLDTaskPlanSchema) {
        mLDTaskPlanSchema = aLDTaskPlanSchema;
        if (mLDTaskPlanSchema != null) {
            mTaskID = mLDTaskPlanSchema.getTaskPlanCode();
            mNextRunTime = mLDTaskPlanSchema.getStartTime();
            if (mNextRunTime == null) {
                mNextRunTime = "";
            }
        }
    }

    public String getNextRunTime() {
        return mNextRunTime;
    }

    public boolean setNextRunTime(String aNextRunTime) {
        mRunTime = mNextRunTime;
        mNextRunTime = aNextRunTime;
        if (mNextRunTime == null) {
            mNextRunTime = "";
        }
        System.out.println("Task " + mTaskID + " Set NextRunTime: " +
                           aNextRunTime);
        return true;
    }

    public long getRunFrequence() {
        return mRunFrequence;
    }

    public void reset() {
        mRunTime = "";
        mRunFrequence = 0;
        mState = 0;
        mTaskResult = "";
        mThreadState = THREAD_READY;
    }

    public void startTask() throws IllegalThreadStateException,
            NoTaskPlanException, TaskLogException, Exception {
        if (mLDTaskPlanSchema == null) {
            System.out.println("Task " + mTaskID +
                               " Error Occur: stopTask NoTaskPlanException");
            throw new NoTaskPlanException();
        }

        mThreadState = THREAD_RUNNING;
        mRunFrequence++;

        if (!logTask("1")) {
            throw new TaskLogException();
        }
        try {

            Class tTaskClass = Class.forName("com.sinosoft.lis.taskservice." +
                                             mThreadName);
            TaskThread tTaskThread = (TaskThread) tTaskClass.newInstance();
            mTaskThread = new Thread(tTaskThread);
            //专为短信后台处理加参数提供
            if (mThreadName.equals("sendMsgEmailTask")) {
                sendMsgEmailTask mTaskThread = new sendMsgEmailTask();
                mTaskThread.setSendType(mLDTaskPlanSchema.getRecycleType());
                mTaskThread.setStartTime(mLDTaskPlanSchema.getStartTime());
                mTaskThread.run();
            } else {
                mTaskThread.start();
            }

            System.out.println("任务计划" + mTaskID + "运行！ 运行次数：" + mRunFrequence +
                               " 运行时间：" + PubFun.getCurrentDate() + " " +
                               PubFun.getCurrentTime());
            TaskCode = mTaskID;

        } catch (IllegalThreadStateException ex) {
            throw new IllegalThreadStateException();
        } catch (Exception ex) {
            throw new Exception();
        }
    }

    public void suspendTask(long aSeconds) throws InterruptedException,
            TaskLogException {
        mThreadState = THREAD_SUSPEND;
        try {
            mTaskThread.sleep(aSeconds * 1000);
        } catch (InterruptedException ex) {
            throw new InterruptedException();
        }
        mThreadState = THREAD_RUNNING;
    }

    public void stopTask() throws SecurityException, TaskLogException {
        mThreadState = THREAD_TERMINATE;
        /*    try
         {
          mTaskThread.sleep(5000);
         }
         catch (InterruptedException ex)
         {
         }
         */
        if (mTaskThread.isAlive()) {
            try {
                mTaskThread.interrupt();
            } catch (SecurityException ex) {
                System.out.println("Task " + mTaskID +
                                   " Exception Occur: stopTask SecurityException");
                throw new SecurityException();
            }
        }
        if (!logTask("4")) {
            throw new TaskLogException();
        }
    }

    public boolean isAlive() {
        return mTaskThread.isAlive();
    }

    private boolean logTask(String aState) {
        VData tData = new VData();
        MMap tMap = new MMap();

        if (mSerialNo == 0) {
            LDTaskRunLogDB tLDTaskRunLogDB = new LDTaskRunLogDB();
            LDTaskRunLogSet tLDTaskRunLogSet = tLDTaskRunLogDB.executeQuery("select * from LDTaskRunLog where SerialNo = (select MAX(SerialNo) from LDTaskRunLog)");
            if (tLDTaskRunLogDB.mErrors.needDealError() ||
                tLDTaskRunLogSet.size() > 1) {
                System.out.println("Task " + mTaskID +
                                   " Error Occur: logTask Fail");
                return false;
            } else if (tLDTaskRunLogSet == null || tLDTaskRunLogSet.size() == 0) {
                mSerialNo = 0;
            } else {
                mSerialNo = new Double(tLDTaskRunLogSet.get(1).getSerialNo()).
                            longValue();
            }
        }

        mLDTaskRunLogSchema.setExecuteState(aState);

        if (aState.equals("1")) { // 1:任务启动
            mSerialNo++;
            mLDTaskRunLogSchema.setSerialNo(mSerialNo);
            mLDTaskRunLogSchema.setTaskCode(mLDTaskPlanSchema.getTaskCode());
            mLDTaskRunLogSchema.setTaskPlanCode(mLDTaskPlanSchema.
                                                getTaskPlanCode());
            mLDTaskRunLogSchema.setExecuteDate(PubFun.getCurrentDate());
            mLDTaskRunLogSchema.setExecuteTime(PubFun.getCurrentTime());
            mLDTaskRunLogSchema.setExecuteFrequence(mRunFrequence);
            tMap.put(mLDTaskRunLogSchema, "INSERT");
        }

        if (aState.equals("2")) { // 2:暂停
            tMap.put(mLDTaskRunLogSchema, "UPDATE");
        }

        if (aState.equals("3") || aState.equals("4") || aState.equals("5")) { // 3:正常终止 4:强行终止 5:异常终止
            mLDTaskRunLogSchema.setFinishDate(PubFun.getCurrentDate());
            mLDTaskRunLogSchema.setFinishTime(PubFun.getCurrentTime());
            tMap.put(mLDTaskRunLogSchema, "UPDATE");
        }

        tData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tData, "")) {
            System.out.println("Task " + mTaskID +
                               " Error Occur: logTask Data Submit Fail");
            return false;
        }
        return true;
    }

    public void addParam(String ParamName, String ParamValue) {
        Parameters.put(ParamName, ParamValue);
    }
}
