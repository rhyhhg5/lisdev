package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.db.LYOutPayDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.ygz.OutPayQueryBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 发票查询批次信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2016</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class OutPayQueryTask  extends TaskThread
{
	  /**错误的容器*/
    public CErrors mErrors = new CErrors();


    String opr = "";
    
    public OutPayQueryTask(){
    	
    }
    
    public void run(){
    	
      OutPayQueryBL  tOutPayQueryBL= new OutPayQueryBL();
      LYOutPayDetailDB tLYOutPayDetailDB=new LYOutPayDetailDB();
  	  LYOutPayDetailSet set=tLYOutPayDetailDB.executeQuery("select * from LYOutPayDetail a where  not exists(select 1 from LYOutPayDetail b where a.busino=b.busino and (b.vstatus='2' or b.printstate='02'))");
  	  VData data=new VData();
  	  data.add(set);
  	  GlobalInput mgl=new GlobalInput();
  	  mgl.Operator="set";
  	  data.add(mgl);
  	  if(!tOutPayQueryBL.getSubmit(data,""))
  	  {
		System.out.println("批次信息导入出问题了");
        System.out.println(mErrors.getErrContent());
        opr ="false";
        return;
	    }else{
	   	 System.out.println("批次信息导入成功了");
	   	 opr ="ture";
	    }
	  	  
	    }
    
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	public static void main(String[] args)
    {

		OutPayQueryTask pOutPayQueryTask = new OutPayQueryTask();
		pOutPayQueryTask.run();

    }
}
