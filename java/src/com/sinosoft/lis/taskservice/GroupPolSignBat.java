package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.workflow.tb.GrpTbWorkFlowUI;

/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author Liuliang
 * @version 1.0
 */

public class GroupPolSignBat extends TaskThread
{
    private int strSql;
    private static int intStartIndex;
	private static int intMaxIndex;

    public GroupPolSignBat()
  {
	  intStartIndex=1;
	  intMaxIndex = 1;
  }


  public void run()
  {
	System.out.println("");
	System.out.println("＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
	System.out.println("参数个数:" + mParameters.size());
	System.out.println("");
	// Write your code here

	GroupPolSignBat a = new GroupPolSignBat();
	  String[] tPram = new String[3];
			   tPram=a.EasyQuery(intStartIndex);

	  //开始批量签单
	  for(int i=1;i<=intMaxIndex;i++)
	  {
		  //先查询待签的记录集
		  tPram=a.EasyQuery(intStartIndex);
		  if(! a.GroupPolSign(tPram[0],tPram[6],tPram[7]))
		  {
			 //如果签单不成功,继续签下一条;
			  intStartIndex++;
		  }
	  }



  }

  public static void main(String[] args)
  {
	  GroupPolSignBat a = new GroupPolSignBat();
	  String[] tPram = new String[3];
	           tPram=a.EasyQuery(intStartIndex);

	  //开始批量签单
	  for(int i=1;i<=intMaxIndex;i++)
	  {
		  //先查询待签的记录集
		  tPram=a.EasyQuery(intStartIndex);
		  if(! a.GroupPolSign(tPram[0],tPram[6],tPram[7]))
		  {
			 //如果签单不成功,继续签下一条;
		      intStartIndex++;
	      }

	  }


  }


  public boolean GroupPolSign(String GrpContNo,String MissionID,String SubMissionID)
  {
	  String FlagStr = "";
	  TransferData tTransferData = new TransferData();
          GlobalInput tG = new GlobalInput();
          tG.Operator="000";//系统自签
          tG.ManageCom="86";//默认总公司
	//接收信息
	// 投保单列表
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	boolean flag = false;
	if( GrpContNo != null )
		{
			System.out.println("GrpContNo:"+":"+GrpContNo);
			tLCGrpContSchema.setGrpContNo( GrpContNo );
			tTransferData.setNameAndValue("MissionID",MissionID );
			tTransferData.setNameAndValue("SubMissionID",SubMissionID );
			flag = true;

		}


	if (flag == true)
	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCGrpContSchema );
		tVData.add( tTransferData );
                tVData.add( tG );

		// 数据传输
		//LCGrpContSignBL tLCGrpContSignBL   = new LCGrpContSignBL();

		//boolean bl = tLCGrpContSignBL.submitData( tVData, "INSERT" );
		//int n = tLCGrpContSignBL.mErrors.getErrorCount();
		GrpTbWorkFlowUI tTbWorkFlowUI = new GrpTbWorkFlowUI();
		boolean bl= tTbWorkFlowUI.submitData( tVData, "0000002006");
		int n = tTbWorkFlowUI.mErrors.getErrorCount();
		if( n == 0 )
		{
			if ( bl )
			{
				System.out.print(" 签单成功! " );
				return true;

			}
			else
			{
				System.out.print(" 集体投保单签单失败! " );
			   return false;

			}
		}
		else
		{

			String strErr = "";
			for (int i = 0; i < n; i++)
			{
				strErr += (i+1) + ": " + tTbWorkFlowUI.mErrors.getError(i).errorMessage + "; ";
				System.out.println(tTbWorkFlowUI.mErrors.getError(i).errorMessage );
			}
			 if ( bl )
			 {

				System.out.print(" 部分签单成功,但是有如下信息: " +strErr);
				return false;
			}
			else
			{
				System.out.print( "集体投保单签单失败，原因是: " + strErr);

				return false;
			}
		} // end of if
	} // end of if
	return false;
  }






  public String[] EasyQuery(int intStartIndex)
  {

	  String strError = "";
	  Integer intStart = new Integer(String.valueOf("1"));;
	  SSRS tSSRS = new SSRS();

	  String sql = "select lwmission.missionprop1,lwmission.missionprop2,lwmission.missionprop9,lwmission.missionprop7,lwmission.missionprop4,lwmission.missionprop5,lwmission.missionid,lwmission.submissionid from lwmission where 1=1"
		   + " and lwmission.processid = '0000000004'"
		   + " and lwmission.activityid = '0000002006' ";
System.out.println("Sql : "+ sql);
		 ExeSQL tExeSQL = new ExeSQL();
		 tSSRS = tExeSQL.execSQL(sql);
		 int MaxCol = tSSRS.getMaxCol();
		 intMaxIndex = tSSRS.getMaxRow();
		 String[] getRowData = new String[MaxCol];

	     for(int i=0;i<MaxCol;i++)
		 {
			 getRowData[i]= tSSRS.GetText(intStartIndex,i+1);
		 }

        System.out.println(getRowData);




	 return getRowData;
  }
}
