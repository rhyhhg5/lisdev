package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.db.FIDataExtractDefDB;
import com.sinosoft.lis.fininterface_v3.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import java.util.*;
/**
 * <p>Title:新财务接口V3 提数、归档、生成凭证 批处理服务类</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class FinDataExtract_V3Task extends TaskThread
{
	public CErrors mErrors = new CErrors();
	private GlobalInput globalInput = new GlobalInput();
	private TransferData mGetCessData = new TransferData();
	private VData mOutputData = new VData();
	
	//管理机构
	private String tMangecom = "";
	//提数日期
	private Date tDate = null;
	//规则类型
	private String[] tRuleDefId = null;
	
	private FIDataExtractDefSchema tFIDataExtractDefSchema ;
	private FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefSet();
	
	public FinDataExtract_V3Task()
	{}
	
	public void run()
    {       
		dealData();
    }
	
	public boolean dealData()
	{
		System.out.println("开始执行财务接口批处理:"+PubFun.getCurrentTime());
		//先准备数据
		getInputData();
		
		//数据提取和归档
		dataExtract();
		
		//生成凭证
		VoucherDataTrans();
		
		System.out.println("财务接口批处理执行完毕:"+PubFun.getCurrentTime());
		
		return true;
	}
	
	public void getInputData()
	{
		//设置管理机构
		tMangecom = "86";
		
		globalInput.ManageCom = tMangecom;
		globalInput.Operator = "cwjk";
		globalInput.ComCode = "86";
		
		//设置提数日期
		FDate chgdate = new FDate();
		tDate = chgdate.getDate(PubFun.getCurrentDate());
		tDate = PubFun.calDate(tDate, -1, "D", null);
		
		//设置数据抽取规则
		FIDataExtractDefDB tFIDataExtractDefDB = new FIDataExtractDefDB();
		tFIDataExtractDefSet = tFIDataExtractDefDB.executeQuery(" select * from FIDataExtractDef a where ruletype = '00' and indexcode <> '00' and rulestate = '1' order by ruledefid with ur ");
		
		System.out.println("提数机构为："+tMangecom+",提数日期为："+tDate);
		
		mGetCessData.setNameAndValue("StartDate",chgdate.getString(tDate));
		mGetCessData.setNameAndValue("EndDate",chgdate.getString(tDate));
		mGetCessData.setNameAndValue("ManageCom",tMangecom);
		mGetCessData.setNameAndValue("CallPointID","00"); //校检节点
		mGetCessData.setNameAndValue("FIExtType","00");
		
		mOutputData.add(globalInput);
		mOutputData.add(mGetCessData);
		mOutputData.add(tFIDataExtractDefSet);
		
	}
	
	public void dataExtract()
	{
		FIDataExtPlanExeUI tFIDataExtPlanExeUI = new FIDataExtPlanExeUI();		
		try
		{
			if(!tFIDataExtPlanExeUI.submitData(mOutputData,"00"))
			{
				System.out.println("操作失败，原因是:" + tFIDataExtPlanExeUI.mErrors.getFirstError());
			}
		}
		catch(Exception ex)
		{
			System.out.println("失败，原因是:" + ex.toString());
		}
		System.out.println("数据提取成功！");
	}
	
	public void VoucherDataTrans()
	{
		VData tVData  = new VData();
		String DataType = "1";
		
		tVData.add(globalInput);
		tVData.add(DataType);
		
		FIDistillCertificateUI tFIDistillCertificateUI = new FIDistillCertificateUI();
		try
		{
			if(!tFIDistillCertificateUI.submitData(tVData,""))
			{
				 System.out.println("凭证转换出错！");
			}    
		}
		catch(Exception ex)
		{
			System.out.println("执行异常，原因是:" + ex.toString());
		}
		
		
	}
	
	public static void main(String args[])
	{
		FinDataExtract_V3Task tFinDataExtract_V3Task = new FinDataExtract_V3Task();
		tFinDataExtract_V3Task.run();
	}

}
