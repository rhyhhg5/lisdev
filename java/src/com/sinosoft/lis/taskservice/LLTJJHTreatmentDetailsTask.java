package com.sinosoft.lis.taskservice;


import com.sinosoft.lis.yibaotong.LLTJJHTreatmentDetailsTransFtpXml;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *天津外包分案信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class LLTJJHTreatmentDetailsTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();


    String opr = "";
    public LLTJJHTreatmentDetailsTask()
    {}

    public void run()
    {
    	LLTJJHTreatmentDetailsTransFtpXml  tLLTJJHTreatmentDetailsTransFtpXml = new LLTJJHTreatmentDetailsTransFtpXml();
    	
    	if(!tLLTJJHTreatmentDetailsTransFtpXml.submitData())
         {
    		 System.out.println("批次信息导入出问题了");
             System.out.println(mErrors.getErrContent());
             opr ="false";
             return;
         }else{
        	 System.out.println("批次信息导入成功了");
        	 opr ="true";
         }

    }

   

   
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	public static void main(String[] args)
    {

        LLTJJHTreatmentDetailsTask tLaHumandeveITask = new LLTJJHTreatmentDetailsTask();
        tLaHumandeveITask.run();
//        tGEdorMJTask.oneCompany(tGlobalInput);
    }
}




