package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.agentcalculate.*;
import com.sinosoft.lis.db.LATreeTempDB;
import com.sinosoft.lis.agentassess.DealLATreeUI;

/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: SinoSoft</p>
 * @author xiangchun
 * @version 1.0
 */

public class LAtreeGrpTask extends TaskThread {
    private int strSql;
    private static int intStartIndex;
    private static int intMaxIndex;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private LATreeTempSet mLATreeTempSet = new LATreeTempSet();
    public LAtreeGrpTask() {
        intStartIndex = 1;
        intMaxIndex = 1;
    }


    public void run() {
        System.out.println(
                "＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
        System.out.println("参数个数:" + mParameters.size());
        System.out.println("");
        LAtreeGrpTask a = new LAtreeGrpTask();
        if(!EasyQuery())
        {
            return ;
        }

        //开始批量处理
        if (mLATreeTempSet!=null && mLATreeTempSet.size()>=1)
        {
            if(!getDate())
            {
                return  ;
            }

        }
    }

    public static void main(String[] args) {

        LAtreeGrpTask a = new LAtreeGrpTask();
        SSRS tSSRS=new SSRS();

       if(!a.EasyQuery())
           {
                return;
           }
    }


    public boolean getDate() {
        String FlagStr = "";
        TransferData tTransferData = new TransferData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "000"; //系统自动做
        tG.ManageCom = "86"; //默认总公司
        DealLATreeUI tDealLATreeUI=new DealLATreeUI();
        VData tVData=new VData();
        tVData.addElement(tG);
        tVData.addElement(mLATreeTempSet);
        if (!tDealLATreeUI.submitData(tVData,""))
        {
            FlagStr="Fail";
            return false;
         }
         return true;
    }


    public  boolean EasyQuery() {

        String strError = "";
        //每月一号计算上月的数据（即当天生效的）
//        String tSql="select date('"+currentDate+"')-1 month  from  dual ";
//        ExeSQL tExeSQL = new ExeSQL();
//        String tdate=tExeSQL.getOneValue(tSql);
        String  tCValiMonth=AgentPubFun.formatDate(currentDate,"yyyyMM");
        LATreeTempDB tLATreeTempDB = new LATreeTempDB();
        String sql = "select *  from  latreetemp  where cvaliflag='1'  and CValiMonth='"+tCValiMonth
                +"' and branchtype='2' and branchtype2='01'  order by  managecom,agentgrade,agentcode" ;
        mLATreeTempSet=tLATreeTempDB.executeQuery(sql);
        return true;
    }
}
