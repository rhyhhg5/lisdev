package com.sinosoft.lis.taskservice;

import java.text.*;
import java.util.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: 任务启动引擎</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author ZhangRong
 * @version 1.0
 */
public class TaskServiceEngine extends TimerTask {
    private static Vector mTaskWaitList; //等待队列
    private static Vector mTaskRunList; //运行队列
    private static Vector mTaskReadyList; //就绪队列
    private static LDTaskPlanDB mLDTaskPlanDB;
    private static LDTaskPlanSet mLDTaskPlanSet;
    private static VData mData;
    private static MMap mMap;
    private static Object lock;
    private static boolean mDataChanged = false;
    private static SimpleDateFormat tSDF = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");
    public static CErrors mErrors = new CErrors();

    public TaskServiceEngine() {
        if (mTaskWaitList == null) {
            mTaskWaitList = new Vector();
        }

        if (mTaskRunList == null) {
            mTaskRunList = new Vector();
        }

        if (mTaskReadyList == null) {
            mTaskReadyList = new Vector();
        }

        mLDTaskPlanDB = new LDTaskPlanDB();
        mData = new VData();
        mMap = new MMap();
        lock = new Object();
    }

//扫描可启动的任务
    private boolean scanTaskPlan() {
        mLDTaskPlanSet = mLDTaskPlanDB.executeQuery(
                "select * from LDTaskPlan where TaskPlanCode <> '000000'");

        if ((mLDTaskPlanSet != null) && (mLDTaskPlanSet.size() > 0)) {
            int n = mLDTaskPlanSet.size();
            LDTaskPlanSchema tLDTaskPlanSchema = null;
//System.out.println("Over 1");
            for (int i = 1; i <= n; i++) {
                tLDTaskPlanSchema = mLDTaskPlanSet.get(i);
//System.out.println("nextTime ["+i+"]: " + tLDTaskPlanSchema.getStartTime());
                if (!isExist(tLDTaskPlanSchema.getTaskPlanCode())) {
                    LDTaskDB tLDTaskDB = new LDTaskDB();
                    tLDTaskDB.setTaskCode(tLDTaskPlanSchema.getTaskCode());

                    LDTaskSet tLDTaskSet = tLDTaskDB.query();

                    if ((tLDTaskSet == null) || (tLDTaskSet.size() <= 0)) {
                        mErrors.addOneError(new CError("任务计划" +
                                tLDTaskPlanSchema.getTaskPlanCode() + "中的任务" +
                                tLDTaskPlanSchema.getTaskCode() + "不存在！"));
                        System.out.println("任务计划" +
                                           tLDTaskPlanSchema.getTaskPlanCode() +
                                           "中的任务" +
                                           tLDTaskPlanSchema.getTaskCode() +
                                           "不存在！");

                        continue;
                    }

                    String tClassName = tLDTaskSet.get(1).getTaskClass().trim();

                    try {
                        Task tTask = new Task(tClassName);
                        tTask.SetTaskPlan(tLDTaskPlanSchema);
//                        System.out.println(tTask.getNextRunTime());
                        if (tTask.getNextRunTime().equals("")) {
                            tTask.setNextRunTime(tSDF.format(Calendar.
                                    getInstance().getTime()));
                        }

                        LDTaskParamDB tLDTaskParamDB = new LDTaskParamDB();
                        tLDTaskParamDB.setTaskCode(tLDTaskPlanSchema.
                                getTaskCode());
                        tLDTaskParamDB.setTaskPlanCode(tLDTaskPlanSchema.
                                getTaskPlanCode());

                        LDTaskParamSet tLDTaskParamSet = tLDTaskParamDB.query();

                        if ((tLDTaskParamSet != null) &&
                            (tLDTaskParamSet.size() > 0)) {
                            LDTaskParamSchema tLDTaskParamSchema = null;
                            int m = tLDTaskParamSet.size();

                            for (int j = 1; j <= m; j++) {
                                tLDTaskParamSchema = tLDTaskParamSet.get(j);
                                tTask.addParam(tLDTaskParamSchema.getParamName(),
                                               tLDTaskParamSchema.getParamValue());
                            }
                        }

                        mTaskWaitList.add(tTask);
                    } catch (Exception ex) {
                        mErrors.addOneError(new CError("创建任务实例失败！异常类型：" +
                                ex.getClass().getName()));
                        System.out.println("创建任务实例失败！异常类型：" +
                                           ex.getClass().getName());
                    }
                }
            }
        }

        return true;
    }

    private boolean searchReadyTask() {
        int n = mTaskWaitList.size();

        String tCurrentTime = PubFun.getCurrentDate() + " " +
                              PubFun.getCurrentTime();
        String tCurrentHM = tCurrentTime.substring(11,16);
        String tCurrentM = tCurrentTime.substring(14,16);
//        System.out.println(tCurrentHM);
//        System.out.println(tCurrentM);
//        System.out.println(tCurrentTime);


        for (int i = 0; i < n; i++) {
            Task tTask = (Task) mTaskWaitList.get(i);
            LDTaskPlanSchema tLDTaskPlanSchema = tTask.getTaskPlan();
//            System.out.println(tTask.getTaskID());
//            System.out.println(tTask.getNextRunTime());
//System.out.println("tLDTaskPlanSchema.starttime["+i+"]"  + tLDTaskPlanSchema.getStartTime());
//System.out.println("tTask.nexttime["+i+"]"  + tTask.getNextRunTime());
            if (!tLDTaskPlanSchema.getRunFlag().equals("1")) {
                continue;
            }

            if (!tLDTaskPlanSchema.getRunState().equals("0")) {
                continue;
            }

            if ((tTask.getNextRunTime() == null) ||
                tTask.getNextRunTime().equals("")) {
                tLDTaskPlanSchema.setRunFlag("0");
                tLDTaskPlanSchema.setRunState("3");
                mMap.put(tLDTaskPlanSchema, "UPDATE");
                mDataChanged = true;
                continue;
            }
            if(tTask.getNextRunTime().length()<19)
            {
                tLDTaskPlanSchema.setRunFlag("0");
                tLDTaskPlanSchema.setRunState("5");
                mMap.put(tLDTaskPlanSchema, "UPDATE");
                mDataChanged = true;
                continue;
            }
//System.out.println("TaskCode : "+ tLDTaskPlanSchema.getTaskPlanCode()+" 启动时间: "+tTask.getNextRunTime());
//System.out.println("启动时间1: "+tTask.getNextRunTime().substring(11,16));
//System.out.println(tCurrentHM);
//            if (tCurrentTime.compareTo(tTask.getNextRunTime()) >= 0 &&
//                tTask.getNextRunTime().substring(11,16).equals(tCurrentHM))
            if (tCurrentTime.compareTo(tTask.getNextRunTime()) >= 0)
            {
                if(tLDTaskPlanSchema.getRecycleType().trim().equals("11")){

                    if ((tLDTaskPlanSchema.getEndTime() != null) &&
                        !tLDTaskPlanSchema.getEndTime().equals("")) {
                        if (tCurrentTime.compareTo(tLDTaskPlanSchema.getEndTime()) <
                            0) {
                            mTaskReadyList.add(tTask);
                        }
                    } else {
                        mTaskReadyList.add(tTask);
                    }
                }
                else if(tLDTaskPlanSchema.getRecycleType().trim().equals("21")&&
                        tTask.getNextRunTime().substring(14,16).equals(tCurrentM)){
                    if ((tLDTaskPlanSchema.getEndTime() != null) &&
                        !tLDTaskPlanSchema.getEndTime().equals("")) {
                        if (tCurrentTime.compareTo(tLDTaskPlanSchema.getEndTime()) <
                            0) {
                            mTaskReadyList.add(tTask);
                        }
                    } else {
                        mTaskReadyList.add(tTask);
                    }

                }else if(tTask.getNextRunTime().substring(11,16).equals(tCurrentHM)){
                    if ((tLDTaskPlanSchema.getEndTime() != null) &&
                        !tLDTaskPlanSchema.getEndTime().equals("")) {
                        if (tCurrentTime.compareTo(tLDTaskPlanSchema.getEndTime()) <
                            0) {
                            mTaskReadyList.add(tTask);
                        }
                    } else {
                        mTaskReadyList.add(tTask);
                    }

                }
            }
        }

        return true;
    }

    private void startTask() {
        int i = 0;
        int n = mTaskRunList.size();

        for (i = n - 1; i >= 0; i--) { //将运行结束的任务线程移出运行队列
            Task tTask = (Task) mTaskRunList.get(i);

            if (!tTask.isAlive()) {
                LDTaskPlanSchema tLDTaskPlanSchema = tTask.getTaskPlan();
                tLDTaskPlanSchema.setRunState("0");
                mMap.put(tLDTaskPlanSchema, "UPDATE");
                mDataChanged = true;
                mTaskRunList.remove(i);
            }
        }

        n = mTaskReadyList.size();

        for (i = 0; i < n; i++) { //运行就绪队列众中的任务并添加倒运行队列中
            Task tTask = (Task) mTaskReadyList.get(i);

            try {
                tTask.startTask();
                mTaskRunList.add(tTask);

                LDTaskPlanSchema tLDTaskPlanSchema = tTask.getTaskPlan();
                tLDTaskPlanSchema.setRunState("1");
                mMap.put(tLDTaskPlanSchema, "UPDATE");
                mDataChanged = true;

                if (!CalculateNextRunTime(tTask)) {
                    mErrors.addOneError(new CError("任务" + tTask.getTaskID() +
                            "启动异常，原因： 执行时间计算异常！"));
                    System.out.println("任务" + tTask.getTaskID() +
                                       "启动异常，原因： 执行时间计算异常！");
                    continue;
                }
            } catch (Exception ex) {
                String tEx = "";

                if (ex.getClass().getName().equals("NoTaskPlanException")) {
                    tEx = "无任务计划!";
                } else if (ex.getClass().getName().equals("TaskLogException")) {
                    tEx = "记录日志失败!";
                } else if (ex.getClass().getName().equals(
                        "IllegalThreadStateException")) {
                    tEx = "任务线程已经启动!";
                } else if (ex.getClass().getName().equals("Exception")) {
                    tEx = "任务线程实例创建异常";
                }

                mErrors.addOneError(new CError("任务" + tTask.getTaskID() +
                                               "启动异常，原因：" + tEx));
                System.out.println("任务" + tTask.getTaskID() + "启动异常，原因：" + tEx);

                LDTaskPlanSchema tLDTaskPlanSchema = tTask.getTaskPlan();
                tLDTaskPlanSchema.setRunFlag("0");
                tLDTaskPlanSchema.setRunState("5");
                mMap.put(tLDTaskPlanSchema, "UPDATE");
                mDataChanged = true;
            }
        }

        mTaskReadyList.removeAllElements();
    }

    private void stopTask() {
        int i = 0;
        int n = mTaskRunList.size();

        for (i = n - 1; i >= 0; i--) { //结束运行队列中正在运行的线程
            Task tTask = (Task) mTaskRunList.get(i);
            LDTaskPlanSchema tLDTaskPlanSchema = tTask.getTaskPlan();

            if (tTask.isAlive()) {
                try {
                    tTask.stopTask();
                    tLDTaskPlanSchema.setRunFlag("0");
                    tLDTaskPlanSchema.setRunState("4");
                    mTaskRunList.remove(i);
                } catch (Exception ex) {
                    String tEx = "";

                    if (ex.getClass().getName().equals("SecurityException")) {
                        tEx = "任务安全性限制强行终止!";
                    } else if (ex.getClass().getName().equals(
                            "TaskLogException")) {
                        tEx = "记录日志失败!";
                    }

                    mErrors.addOneError(new CError("任务" + tTask.getTaskID() +
                            "停止异常，原因：" + tEx));
                    System.out.println("任务" + tTask.getTaskID() + "停止异常，原因：" +
                                       tEx);
                }
            } else {
                tLDTaskPlanSchema.setRunState("3");
                mTaskRunList.remove(i);
            }

            mMap.put(tLDTaskPlanSchema, "UPDATE");
            mDataChanged = true;
        }
    }

    private boolean CalculateNextRunTime(Task aTask) {
        LDTaskPlanSchema tLDTaskPlanSchema = aTask.getTaskPlan();

        if ((tLDTaskPlanSchema.getTimes() > 0) &&
            (aTask.getRunFrequence() >= tLDTaskPlanSchema.getTimes())) {
            aTask.setNextRunTime("");

            return true;
        }

        String tNextTime = "";

        try {
            Date tTime = Calendar.getInstance().getTime();
//            String aa = tSDF.format(tTime);
//            System.out.println("当前时间: " + aa);
            long tInterval = new Double(tLDTaskPlanSchema.getInterval()).
                             longValue();

            if (tLDTaskPlanSchema.getRecycleType().equals("11")) {
                tInterval = 60;
            } else if (tLDTaskPlanSchema.getRecycleType().equals("12")) {
            } else if (tLDTaskPlanSchema.getRecycleType().equals("21")) {
                tInterval = 60 * 60;
            } else if (tLDTaskPlanSchema.getRecycleType().equals("22")) {
            } else if (tLDTaskPlanSchema.getRecycleType().equals("31")) {
                tInterval = 24 * 60 * 60;
                tTime = tSDF.parse(PubFun.getCurrentDate() + " " +
                                   tLDTaskPlanSchema.getStartTime().substring(
                                           11));
//                String aaa = tSDF.format(tTime);
//                System.out.println("数据库中起始日期: " +
//                                   tLDTaskPlanSchema.getStartTime().substring(
//                                           11));
            } else if (tLDTaskPlanSchema.getRecycleType().equals("32")) {
            } else if (tLDTaskPlanSchema.getRecycleType().equals("41")) {
                tInterval = 7 * 24 * 60 * 60;
            } else if (tLDTaskPlanSchema.getRecycleType().equals("42")) {
            } else if (tLDTaskPlanSchema.getRecycleType().equals("51")) {
                tInterval = 12 * 24 * 60 * 60;
            } else if (tLDTaskPlanSchema.getRecycleType().equals("52")) {
            } else if (tLDTaskPlanSchema.getRecycleType().equals("61")) {
                tInterval = 365 * 24 * 60 * 60;
            } else if (tLDTaskPlanSchema.getRecycleType().equals("62")) {
            } else if (tLDTaskPlanSchema.getRecycleType().equals("71")) {
                aTask.setNextRunTime("");

                return true;
            } else if (tLDTaskPlanSchema.getRecycleType().equals("72")) {
            }
            tTime.setTime(tTime.getTime() + tInterval * 1000);
            tNextTime = tSDF.format(tTime);
//            System.out.println(tNextTime);
//            System.out.println("设置时间: " + tNextTime);
            String tEndTime = tLDTaskPlanSchema.getEndTime();

            if (!tNextTime.equals("") &&
                ((tEndTime == null) || tEndTime.equals("") ||
                 ((tEndTime != null) && !tEndTime.equals("") &&
                  (tNextTime.compareTo(tLDTaskPlanSchema.getEndTime()) < 0)))) {
                aTask.setNextRunTime(tNextTime);
            } else {
                aTask.setNextRunTime("");
            }
//            System.out.println(aTask.getNextRunTime());
        } catch (Exception ex) {
            mErrors.addOneError(new CError("计算任务执行时间异常！"));
            System.out.println("计算任务执行时间异常！");
            return false;
        }

        return true;
    }

    private boolean isExist(String tTaskPlanCode) {
        int i = 0;
        int n = mTaskWaitList.size();

        for (i = 0; i < n; i++) {
            if (tTaskPlanCode.equals(((Task) mTaskWaitList.get(i)).getTaskID())) {
                return true;
            }
        }

        return false;
    }

    private int indexOf(String tTaskPlanCode) {
        int i = 0;
        int n = mTaskWaitList.size();

        for (i = 0; i < n; i++) {
            if (tTaskPlanCode.equals(((Task) mTaskWaitList.get(i)).getTaskID())) {
                return i;
            }
        }

        return -1;
    }

//启动引擎
    public boolean startEngine() {
        synchronized (lock) {
            mMap = new MMap();
            mErrors.clearErrors();
            mData.clear();
            scanTaskPlan();
            searchReadyTask();
            startTask();


            if (mDataChanged) {
                mData.clear();
                mData.add(mMap);

                PubSubmit tPubSubmit = new PubSubmit();

                if (!tPubSubmit.submitData(mData, "")) {
                    mErrors.addOneError(new CError("任务计划数据更新失败！"));
                    System.out.println("任务计划数据更新失败！");
                    return false;
                }

                mDataChanged = false;
            }

            return!mErrors.needDealError();
        }
    }

    public boolean stopEngine() {
        synchronized (lock) {
            mMap = new MMap();
            mErrors.clearErrors();
            mData.clear();
            stopTask();
            mTaskRunList.removeAllElements();
            mTaskReadyList.removeAllElements();
            mTaskWaitList.removeAllElements();

            if (mDataChanged) {
                mData.clear();
                mData.add(mMap);

                PubSubmit tPubSubmit = new PubSubmit();

                if (!tPubSubmit.submitData(mData, "")) {
                    mErrors.addOneError(new CError("任务计划数据更新失败！"));
                    System.out.println("任务计划数据更新失败！");

                    return false;
                }

                mDataChanged = false;
            }

            return mErrors.needDealError();
        }
    }

    public boolean addTask(LDTaskPlanSchema tLDTaskPlanSchema,
                           LDTaskParamSet tLDTaskParamSet) {
        synchronized (lock) {
            mMap = new MMap();

            if (isExist(tLDTaskPlanSchema.getTaskPlanCode())) {
                mErrors.addOneError(new CError("任务计划" +
                                               tLDTaskPlanSchema.
                                               getTaskPlanCode() +
                                               "已经存在！"));
                System.out.println("任务计划" + tLDTaskPlanSchema.getTaskPlanCode() +
                                   "已经存在！");

                return false;
            }

            LDTaskDB tLDTaskDB = new LDTaskDB();
            tLDTaskDB.setTaskCode(tLDTaskPlanSchema.getTaskCode());

            LDTaskSet tLDTaskSet = tLDTaskDB.query();

            if ((tLDTaskSet == null) || (tLDTaskSet.size() <= 0)) {
                mErrors.addOneError(new CError("任务计划" +
                                               tLDTaskPlanSchema.
                                               getTaskPlanCode() +
                                               "中的任务" +
                                               tLDTaskPlanSchema.getTaskCode() +
                                               "不存在！"));
                System.out.println("任务计划" + tLDTaskPlanSchema.getTaskPlanCode() +
                                   "中的任务" + tLDTaskPlanSchema.getTaskCode() +
                                   "不存在！");
                return false;
            }

            String tClassName = tLDTaskSet.get(1).getTaskClass().trim();

            try {
                Task tTask = new Task(tClassName);
                tTask.SetTaskPlan(tLDTaskPlanSchema);

                if (tTask.getNextRunTime().equals("")) {
                    tTask.setNextRunTime(tSDF.format(Calendar.getInstance().
                            getTime()));
                }

                if ((tLDTaskParamSet != null) && (tLDTaskParamSet.size() > 0)) {
                    LDTaskParamSchema tLDTaskParamSchema = null;
                    int n = tLDTaskParamSet.size();

                    for (int i = 1; i <= n; i++) {
                        tLDTaskParamSchema = tLDTaskParamSet.get(i);
                        tTask.addParam(tLDTaskParamSchema.getParamName(),
                                       tLDTaskParamSchema.getParamValue());
                    }
                }

                mTaskWaitList.add(tTask);
            } catch (Exception ex) {
                mErrors.addOneError(new CError("创建任务实例失败！异常类型：" +
                                               ex.getClass().getName()));
                System.out.println("创建任务实例失败！异常类型：" + ex.getClass().getName());
            }

            mMap.put(tLDTaskPlanSchema, "INSERT");

            if ((tLDTaskParamSet != null) && (tLDTaskParamSet.size() > 0)) {
                mMap.put(tLDTaskParamSet, "INSERT");
            }

            mData.clear();
            mData.add(mMap);

            PubSubmit tPubSubmit = new PubSubmit();

            return tPubSubmit.submitData(mData, "");
        }
    }

    public boolean removeTask(LDTaskPlanSchema tLDTaskPlanSchema) {
        synchronized (lock) {
            mMap = new MMap();

            int i = indexOf(tLDTaskPlanSchema.getTaskPlanCode());

            if (i < 0) {
                mErrors.addOneError(new CError("任务计划" +
                                               tLDTaskPlanSchema.
                                               getTaskPlanCode() +
                                               "不存在！"));
                System.out.println("任务计划" + tLDTaskPlanSchema.getTaskPlanCode() +
                                   "不存在！");
                return false;
            }

            mTaskWaitList.remove(i);

            mMap.put(tLDTaskPlanSchema, "DELETE");

            LDTaskParamDB tLDTaskParamDB = new LDTaskParamDB();
            tLDTaskParamDB.setTaskCode(tLDTaskPlanSchema.getTaskCode());
            tLDTaskParamDB.setTaskPlanCode(tLDTaskPlanSchema.getTaskPlanCode());

            LDTaskParamSet tLDTaskParamSet = tLDTaskParamDB.query();

            if ((tLDTaskParamSet != null) && (tLDTaskParamSet.size() > 0)) {
                mMap.put(tLDTaskParamSet, "DELETE");
            }

            mData.clear();
            mData.add(mMap);

            PubSubmit tPubSubmit = new PubSubmit();

            return tPubSubmit.submitData(mData, "");
        }
    }

    public boolean activateTask(LDTaskPlanSchema tLDTaskPlanSchema) {
        synchronized (lock) {
            mMap = new MMap();

            int i = indexOf(tLDTaskPlanSchema.getTaskPlanCode());

            if (i < 0) {
                mErrors.addOneError(new CError("任务计划" +
                                               tLDTaskPlanSchema.
                                               getTaskPlanCode() +
                                               "不存在！"));
                System.out.println("任务计划" + tLDTaskPlanSchema.getTaskPlanCode() +
                                   "不存在！");

                return false;
            }

            Task tTask = (Task) mTaskWaitList.get(i);
            tTask.reset();
            tLDTaskPlanSchema = tTask.getTaskPlan();
            tLDTaskPlanSchema.setRunFlag("1");
            tLDTaskPlanSchema.setRunState("0");

            if (tTask.getNextRunTime().equals("")) {
                tTask.setNextRunTime(tSDF.format(Calendar.getInstance().getTime()));
            }

            mMap.put(tLDTaskPlanSchema, "UPDATE");
            mData.clear();
            mData.add(mMap);

            PubSubmit tPubSubmit = new PubSubmit();

            return tPubSubmit.submitData(mData, "");
        }
    }

    public boolean deactivateTask(LDTaskPlanSchema tLDTaskPlanSchema) {
        synchronized (lock) {
            mMap = new MMap();

            int i = indexOf(tLDTaskPlanSchema.getTaskPlanCode());

            if (i < 0) {
                mErrors.addOneError(new CError("任务计划" +
                                               tLDTaskPlanSchema.
                                               getTaskPlanCode() +
                                               "不存在！"));
                System.out.println("任务计划" + tLDTaskPlanSchema.getTaskPlanCode() +
                                   "不存在！");

                return false;
            }

            Task tTask = (Task) mTaskWaitList.get(i);
            tLDTaskPlanSchema = tTask.getTaskPlan();
            tLDTaskPlanSchema.setRunFlag("0");
            tLDTaskPlanSchema.setRunState("3");
            System.out.println(tLDTaskPlanSchema.getRunState());
            mMap.put(tLDTaskPlanSchema, "UPDATE");
            mData.clear();
            mData.add(mMap);

            PubSubmit tPubSubmit = new PubSubmit();

            return tPubSubmit.submitData(mData, "");
        }
    }

    public void run() {
        synchronized (lock) {
            mMap = new MMap();
            mData.clear();
            searchReadyTask();
            startTask();

            if (mDataChanged) {
                mData.clear();
                mData.add(mMap);

                PubSubmit tPubSubmit = new PubSubmit();
                tPubSubmit.submitData(mData, "");
                mDataChanged = false;
            }
        }
    }
}
