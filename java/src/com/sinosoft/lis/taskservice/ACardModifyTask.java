package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.certify.ActivationCardModifyBL;
import java.text.*;
import java.util.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 负责调用ActivationCardModifyBL.java进行业务逻辑处理。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2010</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Pangxy
 * @version 1.0
 */
public class ACardModifyTask extends TaskThread
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    public ACardModifyTask()
    {
    }

    /**
     * 批处理程序调用的入口，供批处理程序调用。
     * 方法内调用ActivationCardModifyBL.java进行业务逻辑处理。
     */
    public void run()
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        tGlobalInput.ManageCom = "86";
        VData mVData = new VData();
        TransferData tTransferData = new TransferData();
        String tWebModifyDate = "";
        Calendar tNow = Calendar.getInstance();
        //获得当前日期的前一天
        tNow.add(Calendar.DATE, -1);
        SimpleDateFormat tSDF = new SimpleDateFormat("yyyy-MM-dd");
        Date tToday = tNow.getTime();
        tWebModifyDate = tSDF.format(tToday);
        tTransferData.setNameAndValue("WebModifyDate", tWebModifyDate);
        mVData.add(tGlobalInput);
        mVData.add(tTransferData);
        ActivationCardModifyBL tActivationCardModifyBL = new ActivationCardModifyBL();
        //批量修改激活卡信息
        if (!tActivationCardModifyBL.submitData(mVData, ""))
        {
            System.out.println(tActivationCardModifyBL.mErrors.getErrContent());
        }
    }

    public static void main(String[] args)
    {
        ACardModifyTask tACardModifyTask = new ACardModifyTask();
        tACardModifyTask.run();
    }
}
