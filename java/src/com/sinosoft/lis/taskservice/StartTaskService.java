package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class StartTaskService implements ServletContextListener {
    public StartTaskService() {
    }

    private String firstName, emailAddress;

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException,
            IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String uri = request.getRequestURI();
        System.out.println("dljfsldjflsjflsdjflsdjf   " + uri);
    }


    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "000";

        TaskService taskService = new TaskService();
//        LDTaskPlanSchema tLDTaskPlanSchema = new LDTaskPlanSchema();
//        LDTaskPlanSet tLDTaskPlanSet = new LDTaskPlanSet();
//        LDTaskParamSchema tLDTaskParamSchema = new LDTaskParamSchema();
//        LDTaskParamSet tLDTaskParamSet = new LDTaskParamSet();
//        LDTaskSchema tLDTaskSchema = new LDTaskSchema();
//        LDTaskSet tLDTaskSet = new LDTaskSet();
//
//        tLDTaskPlanSchema.setTaskPlanCode("000081");
//        tLDTaskPlanSchema.setTaskCode("000026");
//        tLDTaskPlanSchema.setRunFlag("0");
//        tLDTaskPlanSchema.setRecycleType("11");
//        tLDTaskPlanSchema.setStartTime("2006-01-16 14:40:00");
//        tLDTaskPlanSchema.setEndTime("");
//        tLDTaskPlanSchema.setInterval(0);
//        tLDTaskPlanSchema.setTimes(0);
//        tLDTaskPlanSet.add(tLDTaskPlanSchema);
//
//        tLDTaskSchema.setTaskCode("000026");
//        tLDTaskSchema.setTaskDescribe("综合短信管理");
//        tLDTaskSchema.setTaskClass("sendMsgEmailTask");
//        tLDTaskSet.add(tLDTaskSchema);

        VData tData = new VData();
        tData.add(tGI);
//        tData.add(tLDTaskPlanSet);
//        tData.add(tLDTaskSet);
        taskService.submitData(tData, "START");
//        taskService.submitData(tData, "ACTIVATE");
//        taskService.submitData(tData, "STOP");
    }

    public void contextInitialized(ServletContextEvent sce)
	{
		System.out.println("sce.getServletContext().getServerInfo() = " + sce.getServletContext().getServerInfo());
		if (checkHost())
		{
			GlobalInput tGI = new GlobalInput();
			tGI.ManageCom = "86";
			tGI.Operator = "WEB";
			TaskService taskService = new TaskService();
			VData tData = new VData();
			tData.add(tGI);
			taskService.submitData(tData, "START");
		}
	}

    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("sce.getServletContext().getServerInfo() = " +
                           sce.getServletContext().getServerInfo());
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "WEB";
        TaskService taskService = new TaskService();
        VData tData = new VData();
        tData.add(tGI);
        taskService.submitData(tData, "STOP");
    }
    
    /**
     * 指定控制批处理server自动启停主机IP
     * @return
     * false 批处理server不自动启动  
     * true  批处理server自动启动
     * @author filon51
     * @since 2009.03.31
     * @return
     */
    private boolean checkHost()
    {
		String IP = "*.*.*.*";
		try
		{
			IP = InetAddress.getLocalHost().getHostAddress();
			if (IP == null || IP.equals("") || IP.equals("*.*.*.*"))
			{
				System.out.println("get IP Error! null.");
				return false;
			}
		}
		catch (Exception ex)
		{
			System.out.println("Get IP Execption!");
			ex.printStackTrace();
			return false;
		}
		System.out.println("The Server IP Address is:" + IP);

		String tSql = "select count(*) from ldsysvar where sysvar = 'TaskServer' and sysvarvalue = '" + IP
				+ "' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String tFlag = "0";
		try
		{
			tFlag = tExeSQL.getOneValue(tSql);
		}
		catch (Exception ex)
		{
			System.out.println("Query Execption!");
			ex.printStackTrace();
			return false;
		}
		if (tFlag.equals("0"))
		{
			System.out.println("TaskService Not Start At " + IP);
			return false;
		}
		return true;
	}
}
