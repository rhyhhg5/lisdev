package com.sinosoft.lis.taskservice;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.operfee.NewIndiDueFeeMultiUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GoldenUnionFetchDataTask extends TaskThread{

	private String startDate = "";
    private String endDate = "";
    private GlobalInput mGlobalInput = new GlobalInput();
    private LJAGetSet mLJAGetSet = new LJAGetSet();
    //限制提盘的最大条数
    private int limite =  0;
    
    /** 应收总金额 */
    private double totalMoneyS = 0;
    /** 应收总笔数 */
    int sumNumS = 0;
    /** 应收批次号 */
    String serialNoS = "";
    
    /** 应付总金额 */
    private double totalMoneyF = 0;
    /** 应付总笔数 */
    int sumNumF = 0;
    /** 应付批次号 */
    String serialNoF = "";
    
    public void run(){
        System.out.println("---GoldenUnionFetchDataTask开始---");
        submitData();
        System.out.println("---GoldenUnionFetchDataTask结束---");
    }
    
    private boolean submitData(){
        if (!getInputData()){
            return false;
        }
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData()){
            return false;
        }
        System.out.println("---End dealData---");
        return true;
    }
    
    private boolean getInputData(){
        try{
            startDate = "2012-1-1"; //提取数据的起始日期
            endDate = PubFun.getCurrentDate();
            assignGlobalInput();
        }
        catch (Exception e){
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }
        return true;
    }
	
    private boolean dealData(){
    	
    	ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL("select bankcode from ldbank where operator='system' and comcode='86' ");
        try{
        	//1、催收处理
        	for(int i = 1; i <= tSSRS.MaxRow; i++){
        		hastenCharge(tSSRS.GetText(i, 1));
        	}
        	//2、提取应收数据
        	if(!getSendBankData("1")){
        		System.out.println("提取应收数据失败!");
        	}else{
        		System.out.println("提取应收数据成功 ^-^");
        	}
        	//3、提取应付数据
        	if(!getSendBankData("2")){
        		System.out.println("提取应付数据失败!");
        	}else{
        		System.out.println("提取应付数据成功 ^-^");
        	}
        	//4、将提取的数据按前四位机构分别生成批次
        	SSRS tSSRS2 = new ExeSQL().execSQL("select substr(comcode,1,4) from ldcom where sign='1' and comcode not in ('86','86000000')  group by substr(comcode,1,4) ");
        	for(int i = 1; i<=tSSRS2.MaxRow; i++){
        		getSendBankConf("1",tSSRS2.GetText(i, 1));
        		getSendBankConf("2",tSSRS2.GetText(i, 1));
        	}
        }catch(Exception ex){
        	System.out.println("处理出错");
        	 ex.printStackTrace();
        }
    	
    	return true;
    }
    
    /**
     * 催收
     * @param bankcode
     * @return boolean
     */
    private boolean hastenCharge(String bankcode){
        
        LCPolSchema tLCPolSchema = new LCPolSchema();
        System.out.println("StartDate:" + startDate);
        tLCPolSchema.setGetStartDate(startDate); //将判断条件设置在起领日期字段中
        tLCPolSchema.setPayEndDate(endDate); //将判断条件设置在终交日期字段中

        TransferData transferData2 = new TransferData();
        transferData2.setNameAndValue("bankCode", bankcode);
        VData tVData2 = new VData();
        tVData2.add(tLCPolSchema);
        tVData2.add(mGlobalInput);
        tVData2.add(transferData2);
        
        NewIndiDueFeeMultiUI tNewIndiDueFeeMultiUI = new NewIndiDueFeeMultiUI();
        tNewIndiDueFeeMultiUI.submitData(tVData2, "INSERT");

        if(tNewIndiDueFeeMultiUI.mErrors.needDealError()){
            System.out.print("催收处理失败，不能生成银行发送数据，原因是:");
            for (int n = 0; n < tNewIndiDueFeeMultiUI.mErrors.getErrorCount(); n++){
                System.out.print(tNewIndiDueFeeMultiUI.mErrors.getError(n).errorMessage+ "|");
            }
        }else{
            System.out.println("催收处理成功！");
        }
    	
    	return true;
    }
    
    /**
     * 提取发送银行的应收/应付数据
     * @param flag[1:应收;2:应付]
     * @return boolean
     */
    private boolean getSendBankData(String flag){
    	
        if("1".equals(flag)){
        	try{
        		//总应收表处理（获取交费日期在设置的日期区间内的记录；获取银行在途标志为0的记录）
        		LJSPaySet tLJSPaySet = getLJSPay(startDate, endDate);
        		if(tLJSPaySet == null){
        			throw new NullPointerException("总应收表处理失败！");
        		}
        		if(tLJSPaySet.size() == 0){
        			throw new NullPointerException("总应收表无数据！");
        		}
        		System.out.println("---End getLJSPay---");
        		
        		//修改应收表银行在途标志,记录发送银行次数
                tLJSPaySet = modifyBankFlag(tLJSPaySet);
                MMap map = new MMap();
                map.put(tLJSPaySet, "UPDATE");
                VData tVData = new VData();
                tVData.add(map);
                System.out.println("Start PubSubmit BLS Submit...");
                PubSubmit tPubSubmit = new PubSubmit();
                if(!tPubSubmit.submitData(tVData, "UPDATE")){
                	System.out.println("提交数据库出错...");
                	return false;
                }
                System.out.println("End PubSubmit BLS Submit...");
                
        	}catch(Exception e){
        		// @@错误处理
                CError.buildErr(this, "数据处理错误:" + e.getMessage());
                return false;
        	}
        }else if("2".equals(flag)){
        	try{
        		LJAGetSet tLJAGetSet = getLJAGet(startDate, endDate);
                if (tLJAGetSet == null) {
                    throw new NullPointerException("总应付表处理失败！");
                }
                if (tLJAGetSet.size() == 0) {
                    throw new NullPointerException("总应付表无数据！");
                }
                System.out.println("---End getLJAGet---");
                //设置cansendbank状态为9
        		tLJAGetSet = modifyBankFlag(tLJAGetSet);
                mLJAGetSet.clear();
                mLJAGetSet.add(tLJAGetSet);
                MMap map = new MMap();
                map.put(mLJAGetSet, "UPDATE");
                VData tVData = new VData();
                tVData.add(map);
                System.out.println("Start PubSubmit BLS Submit...");
                PubSubmit tPubSubmit = new PubSubmit();
                if(!tPubSubmit.submitData(tVData, "UPDATE")){
                	System.out.println("提交数据库出错...");
                	return false;
                }
        	}catch(Exception e){
        		// @@错误处理
                CError.buildErr(this, "数据处理错误:" + e.getMessage());
                return false;
        	}
        }
        
    	return true;
    }
    
    /**
     * 提取发送银行的应收数据
     * @param 
     * @return 
     */
    private LJSPaySet getLJSPay(String startDate, String endDate){
    	
    	LJSPaySet tLJSPaySet = new LJSPaySet();
    	
    	//获取银行信息，校验是否是金联
    	LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode("7707");
        if(!tLDBankDB.getInfo()){
        	CError.buildErr(this, "获取银行信息（LDBank）失败");
            return null;
        }
    	
       if(tLDBankDB.getBankUniteFlag().equals("1")){

            //unitegroupcode 1代表支持代收 2代表支持代付 3代表支持代收代付
            //String bankSql = "select * from ldbankunite where bankunitecode='7707' and unitegroupcode in ('1','3') ";
            	tLJSPaySet.add(getLJSPayByPaydate(startDate, endDate));
        }
        
    	return tLJSPaySet;
    }
    
private LJSPaySet getLJSPayByPaydate(String startDate, String endDate){
    	
        String tSql = "";
        String tSql2 = "";

        //不指定开始时间
        if (startDate.equals("")){
            //规则：最早应收日期<=结束时间，最晚交费日期>结束时间
            //    银行编码匹配，机构编码向下匹配，不在途，有账号
            //如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
        	tSql = "select * from LJSPay" 
        		 + " where StartPayDate <= '"+endDate+ "'"
                 + " and PayDate >= '"+endDate+"'"
                 + " and BankCode in ( select bankcode from ldbankunite where bankunitecode='7707' and unitegroupcode in ('1','3')) "
                 + " and ManageCom like '"+mGlobalInput.ComCode+"%'"
                 + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                 + " and (CanSendBank = '0' or CanSendBank is null )  and sumduepaymoney<>0"
                 + " and OtherNoType not in ('2','1','3')"
                 + " and (BankAccNo <> '') and (AccName <> '')" 
                 + " and bankcode not in ('489102','999100')" 
                 + " and bankcode not in (select bankcode from ldbankunite a where a.bankunitecode in ('7703','7704','7705','7706') and a.bankcode=bankcode and unitegroupcode in ('1','3')) ";

            //续期划帐,提取所有续期续保数据
            tSql2 = "select * from LJSPay" 
            	  + " where StartPayDate <= '"+endDate+"'"
                  + " and  PayDate >= '"+endDate+"'"
                  + " and BankCode in ( select bankcode from ldbankunite where bankunitecode='7707' and unitegroupcode in ('1','3')) "
                  + " and ManageCom like '"+mGlobalInput.ComCode+"%'"
                  + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                  + " and (CanSendBank = '0' or CanSendBank is null )  and sumduepaymoney<>0"
                  + " and (BankAccNo <> '') and (AccName <> '')" 
                  + " and 0=(select COUNT(enteraccdate) from ljtempfee where  tempfeeno=LJSPay.getnoticeno and tempfeetype='2' ) "
                  + " and OtherNoType='2' and bankcode not in ('489102','999100')" 
                  + " and bankcode not in (select bankcode from ldbankunite a where a.bankunitecode in ('7703','7704','7705','7706') and a.bankcode=bankcode and unitegroupcode in ('1','3')) ";
        }else{//指定开始时间和结束时间
            //规则：最早应收日期>=开始时间，最早应收日期<=结束时间，最晚交费日期>结束时间
            //     银行编码匹配，机构编码向下匹配，不在途，有账号
            //如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
            tSql = "select * from LJSPay" 
            	 + " where StartPayDate >= '"+startDate+"'"
            	 + " and StartPayDate <= '"+endDate+"'"
            	 + " and PayDate >= '"+endDate+"'"
            	 + " and BankCode in ( select bankcode from ldbankunite where bankunitecode='7707' and unitegroupcode in ('1','3')) "
            	 + " and ManageCom like '"+mGlobalInput.ComCode+"%'"
            	 + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
            	 + " and (CanSendBank = '0' or CanSendBank is null ) and sumduepaymoney<>0"
            	 + " and (BankAccNo <> '') and (AccName <> '')"
            	 + " and OtherNoType not in ('2','1','3')" 
            	 + " and bankcode not in ('489102','999100')" 
            	 + " and bankcode not in (select bankcode from ldbankunite a where a.bankunitecode in ('7703','7704','7705','7706') and a.bankcode=bankcode and unitegroupcode in ('1','3')) ";

            tSql2 = "select * from LJSPay" 
            	  + " where StartPayDate <= '"+endDate+"'" 
            	  +	" and StartPayDate >='"+startDate+"'"
                  + " and  PayDate >= '"+endDate+"'"
                  + " and BankCode in ( select bankcode from ldbankunite where bankunitecode='7707' and unitegroupcode in ('1','3') )"
                  + " and ManageCom like '"+mGlobalInput.ComCode+"%'"
                  + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                  + " and (CanSendBank = '0' or CanSendBank is null )  and sumduepaymoney<>0"
                  + " and (BankAccNo <> '') and (AccName <> '')" 
                  + " and 0=(select COUNT(enteraccdate) from ljtempfee where  tempfeeno=LJSPay.getnoticeno and tempfeetype='2' ) "
                  + " and OtherNoType='2' and bankcode not in ('489102','999100')" 
                  + " and bankcode not in (select bankcode from ldbankunite a where a.bankunitecode in ('7703','7704','7705','7706') and a.bankcode=bankcode and unitegroupcode in ('1','3')) ";
        }
        System.out.println(tSql);
        System.out.println(tSql2);

        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(tSql);
        tLJSPaySet.add(tLJSPayDB.executeQuery(tSql2));

        return tLJSPaySet;
    }
    
    /**
     * 提取发送银行的应付数据
     * @param flag[1:应收;2:应付]
     * @return boolean
     */
    private LJAGetSet getLJAGet(String startDate, String endDate){
    	
    	LJAGetSet tLJAGetSet = new LJAGetSet();
    	//获取银行信息，校验是否是金联
    	LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode("7707");
        if(!tLDBankDB.getInfo()){
        	CError.buildErr(this, "获取银行信息（LDBank）失败");
            return null;
        }
    	
        if(tLDBankDB.getBankUniteFlag().equals("1")){
            	tLJAGetSet.add(getLJAGetByPaydate(startDate, endDate));
        }
        return tLJAGetSet;
    }
    
    
private LJAGetSet getLJAGetByPaydate(String startDate, String endDate){

        String tsql = " select * from LJAGet "
				+ " where ShouldDate >= '"+startDate+"' "
				+ " and ShouldDate <= '"+endDate+"' "
				+ " and ManageCom like '"+mGlobalInput.ComCode+"%' "
				+ " and PayMode='4' "
				+ " and BankCode in ( select bankcode from ldbankunite where bankunitecode='7707' and unitegroupcode in ('2','3') )"
				+ " and EnterAccDate is null and ConfDate is null "
				+ " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null) "
				+ " and (CanSendBank is null or  CanSendBank = '0') "
				+ " and (BankAccNo <> '') and (AccName <> '') and sumgetmoney<>0 "
				+ " and bankcode not in ('489102','999100') "
				+ " and bankcode not in (select bankcode from ldbankunite a where a.bankunitecode in ('7703','7704','7705','7706') and a.bankcode=bankcode and unitegroupcode in ('2','3')) "
				+ " and othernotype not in ('3','5','23','24') ";
	
	String tsql2 = " select * from LJAGet "
				 + " where ShouldDate >= '"+startDate+"' "
				 + " and ShouldDate <= '"+endDate+"' "
				 + " and ManageCom like '"+mGlobalInput.ComCode+"%' "
				 + " and PayMode='4' "
				 + " and BankCode in ( select bankcode from ldbankunite where bankunitecode='7707' and unitegroupcode in ('2','3') )"
				 + " and EnterAccDate is null and ConfDate is null "
				 + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null) "
				 + " and (CanSendBank is null or  CanSendBank = '0') "
				 + " and (BankAccNo <> '') and (AccName <> '') and sumgetmoney<>0 "
				 + " and bankcode not in ('489102','999100') "
				 + " and bankcode not in (select bankcode from ldbankunite a where a.bankunitecode in ('7703','7704','7705','7706') and a.bankcode=bankcode and unitegroupcode in ('2','3')) "
				 + " and othernotype = '5' and Exists (Select 1 From Llcase b Where Otherno = b.Caseno or Otherno = b.rgtno) "
				 + " and othernotype not in ('23','24') ";
	
	System.out.println("tsql:"+tsql);	    	 
	System.out.println("tsql2:"+tsql2);
	
	LJAGetDB tLJAGetDB = new LJAGetDB();
	LJAGetSet tLJAGetSet = tLJAGetDB.executeQuery(tsql);
    tLJAGetSet.add(tLJAGetDB.executeQuery(tsql2));

    return tLJAGetSet;

    }
    
    private boolean getSendBankConf(String flag,String managecom){
    	//限制提盘的最大size为limite
    	String sql_limit = "select code1 from ldcode1 where codetype='batchsendlimite' and code='limite'";
        ExeSQL limiteSql = new ExeSQL();
        String limiteCont = limiteSql.getOneValue(sql_limit);
        try {
        	limite = Integer.parseInt(limiteCont);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    	//收费提盘
    	if("1".equals(flag)){
    		VData tVData = new VData();
    		LJSPayDB tLJSPayDB = new LJSPayDB();
    		LJSPaySet sLJSPaySet= null;
            
            String sql1 = "select * from ljspay a where cansendbank = '9' and BankOnTheWayFlag = '1' and  bankcode in "
                    	+ "(select bankcode from ldbankunite where bankunitecode='7707' ) and "
                    	+ "not exists(select 1 from lysendtobank where a.getnoticeno = paycode ) and managecom like '"+managecom+"%'";
            LJSPaySet LJSPaySettol = tLJSPayDB.executeQuery(sql1);
            System.out.println("总数量："+ LJSPaySettol.size());
            int tStartS = 1;
             sLJSPaySet = tLJSPayDB.executeQuery(sql1,tStartS,limite);
            System.out.println("第一条批次的条数："+ sLJSPaySet.size());
            
            
            do {
            	
            	if(sLJSPaySet.size()!=0){
                	// 生成批次号，要在循环外生成
                    StringBuffer mSeriaPrefix = new StringBuffer();
            		String mGetMerchantNo = "select codename from ldcode1 where codetype='GoldenUnionSendBank' and code = 'UserName' " 
            							  + "and code1 = 'S7707'" + " with ur ";
            		ExeSQL mExeSql = new ExeSQL();
            		SSRS tNoSSRS = mExeSql.execSQL(mGetMerchantNo);
            		if(tNoSSRS.getMaxRow() > 0){
            			String xMerchantNo = tNoSSRS.GetText(1, 1);
            			String xLastFourNo = xMerchantNo.substring(xMerchantNo.length()-4, xMerchantNo.length());
            			
            			mSeriaPrefix.append("7707");
            			mSeriaPrefix.append(xLastFourNo);
            		}
            		serialNoS = PubFun1.CreateMaxNo("1", 12);
                    serialNoS = mSeriaPrefix.toString() + serialNoS;
                    System.out.println("生成应收费用：批次号：" + serialNoS);
                }
            	//生成LYBankLog及LYSendToBank信息
            	LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();
                tLYSendToBankSet = getSendToBankS(sLJSPaySet);
                if(tLYSendToBankSet.size() != 0){
                	String serialno = tLYSendToBankSet.get(1).getSerialNo();
                    
                	LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();
                    tLYBankLogSchema = getBankLogS(managecom);
                    tLYBankLogSchema.setSerialNo(serialno);

                    //提交数据
                    MMap map2 = new MMap();
                    map2.put(tLYSendToBankSet, "INSERT");
                    map2.put(tLYBankLogSchema, "INSERT");

                    tVData.clear();
                    tVData.add(map2);
                    
                    PubSubmit tPubSubmit2 = new PubSubmit();
                    tPubSubmit2.submitData(tVData, "");
                    
                   }else{
                    System.out.println("没有金联应收费数据！！！");
                    return false;
                }
                sLJSPaySet = tLJSPayDB.executeQuery(sql1,tStartS,limite);
            }
            while(sLJSPaySet.size()!=0 && sLJSPaySet!=null);
            
    	}else if("2".equals(flag)){
    		//付费提盘
    		VData tVData = new VData();
            System.out.println("End PubSubmit BLS Submit...");
            
            String sql2 = "select * from ljaget a where cansendbank = '9' and BankOnTheWayFlag = '1' "
                    	+ "and bankcode in ( select bankcode from ldbankunite where bankunitecode='7707' ) "
                    	+ "and othernotype <> '24' "
                    	+ "and not exists(select 1 from lysendtobank where a.actugetno = paycode ) and managecom like '"
                    	+ managecom + "%' ";
            LJAGetDB tLJAGetDB = new  LJAGetDB();
            LJAGetSet sLJAGetSettol = tLJAGetDB.executeQuery(sql2);
            
            System.out.println("总长度" +sLJAGetSettol.size());
            int tStartF = 1;
            LJAGetSet tLJAGetSet = new LJAGetSet();
            tLJAGetSet = tLJAGetDB.executeQuery(sql2,tStartF,limite);
            System.out.println("第一个付费批次的数据量" + tLJAGetSet.size());
            
           do {
        	   
        	   if(tLJAGetSet.size()!=0){
              	 //生成批次号，要在循环外生成
                  StringBuffer mSeriaPrefix = new StringBuffer();
          		String mGetMerchantNo = "select codename from ldcode1 where codetype='GoldenUnionSendBank' and code = 'UserName' and code1 = 'F7707'"+ " with ur ";
          		ExeSQL mExeSql = new ExeSQL();
          		SSRS tNoSSRS = mExeSql.execSQL(mGetMerchantNo);
          		if (tNoSSRS.getMaxRow() > 0) {
          			String xMerchantNo = tNoSSRS.GetText(1, 1);
          			String xLastFourNo = xMerchantNo.substring(xMerchantNo.length()-4, xMerchantNo.length());
          			
          			mSeriaPrefix.append("7707");
          			mSeriaPrefix.append(xLastFourNo);
          		}
              	
          		  serialNoF = PubFun1.CreateMaxNo("1", 12);
                  serialNoF = mSeriaPrefix.toString() + serialNoF;
                  System.out.println("生成应付费用：批次号：" + serialNoF);
                  
              }
        	   LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();
               tLYSendToBankSet = getSendToBankF(tLJAGetSet);
              if (tLYSendToBankSet.size() != 0){
                  String serialno = tLYSendToBankSet.get(1).getSerialNo();

                  LYBankLogSchema tLYBankLogSchema =new LYBankLogSchema();
                  tLYBankLogSchema = getBankLogF(managecom);
                  tLYBankLogSchema.setSerialNo(serialno);

                  //提交数据
                  MMap map2 = new MMap();
                  map2.put(tLYSendToBankSet, "INSERT");
                  map2.put(tLYBankLogSchema, "INSERT");

                  tVData.clear();
                  tVData.add(map2);
                  PubSubmit ttPubSubmit2 = new PubSubmit();
                  if (!ttPubSubmit2.submitData(tVData, "")){
                      System.out.println("数据提交失败！");
                      return false;
                  }
              
               
              }else{
                  System.out.println("没有金联代付费数据！！！");
                  return false;
              }
        	   
              tLJAGetSet = tLJAGetDB.executeQuery(sql2,tStartF,limite);
           }
           while(tLJAGetSet!=null && tLJAGetSet.size()!=0);
    	}
    	return true;
    }
    
    /**
     * 生成送银行表应收数据
     * @param tLJSPaySet
     * @return
     */
    public LYSendToBankSet getSendToBankS(LJSPaySet tLJSPaySet){
    	
    	//总金额
        double dTotalMoney = 0;
        sumNumS = 0;
        
        LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();
        
        for (int i = 0; i < tLJSPaySet.size(); i++){
        	
            LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i + 1);

            LYSendToBankDB OldLYSendToBankDB = new LYSendToBankDB();
            OldLYSendToBankDB.setPayCode(tLJSPaySchema.getGetNoticeNo());
            OldLYSendToBankDB.setBankCode(tLJSPaySchema.getBankCode());
            OldLYSendToBankDB.setAccName(tLJSPaySchema.getAccName());
            OldLYSendToBankDB.setAccNo(tLJSPaySchema.getBankAccNo());
            LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
            //设置统一的批次号
            tLYSendToBankSchema.setSerialNo(serialNoS);
            //收费标记
            tLYSendToBankSchema.setDealType("S");
            tLYSendToBankSchema.setPayCode(tLJSPaySchema.getGetNoticeNo());
            tLYSendToBankSchema.setBankCode(tLJSPaySchema.getBankCode());
            tLYSendToBankSchema.setAccName(tLJSPaySchema.getAccName());
            tLYSendToBankSchema.setAccNo(tLJSPaySchema.getBankAccNo());
            tLYSendToBankSchema.setPolNo(tLJSPaySchema.getOtherNo());
            tLYSendToBankSchema.setNoType(tLJSPaySchema.getOtherNoType());
            tLYSendToBankSchema.setComCode(tLJSPaySchema.getManageCom());
            tLYSendToBankSchema.setAgentCode(tLJSPaySchema.getAgentCode());
            tLYSendToBankSchema.setPayMoney(tLJSPaySchema.getSumDuePayMoney());
            tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
            tLYSendToBankSchema.setDoType("1");
            tLYSendToBankSchema.setRemark(mGlobalInput.Operator);
            tLYSendToBankSchema.setModifyDate(PubFun.getCurrentDate());
            tLYSendToBankSchema.setModifyTime(PubFun.getCurrentTime());
            System.out.println("tLJSPaySchema:"+ tLJSPaySchema.getAppntNo());
            SSRS tSSRS = new ExeSQL()
                    .execSQL("select idno,idtype from ldperson where customerno='"
                            + tLJSPaySchema.getAppntNo() + "'");
            if (tSSRS.getMaxRow() > 0){
                tLYSendToBankSchema.setIDNo(tSSRS.GetText(1, 1));
                tLYSendToBankSchema.setIDType(tSSRS.GetText(1, 2));
            }else{
                tLYSendToBankSchema.setIDNo("");
            }
            tLYSendToBankSchema.setRiskCode(tLJSPaySchema.getRiskCode());
            tLYSendToBankSet.add(tLYSendToBankSchema);

            //累加总金额和总数量
            dTotalMoney = dTotalMoney + tLJSPaySchema.getSumDuePayMoney();
            //转换精度
            dTotalMoney = Double.parseDouble((new DecimalFormat("0.00"))
                    .format(dTotalMoney));
            sumNumS = sumNumS + 1;
        }
        totalMoneyS = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));

        return tLYSendToBankSet;
    }
    
    /**
     * 生成银行日志表数据(应收)
     * @return
     */
    public LYBankLogSchema getBankLogS(String managecom)
    {
        LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

        tLYBankLogSchema.setBankCode("7707");
        tLYBankLogSchema.setLogType("S");
        tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setTotalMoney(totalMoneyS);
        tLYBankLogSchema.setTotalNum(sumNumS);
        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        tLYBankLogSchema.setComCode(managecom);

        return tLYBankLogSchema;
    }
    
    /**
     * 生成送银行表应付数据
     * @param tLJSPaySet
     * @return
     */
    public LYSendToBankSet getSendToBankF(LJAGetSet tLJAGetSet){
    	
    	//总金额
        double dTotalMoney = 0;
        sumNumF = 0;
        
        LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();
    
        for (int i = 0; i < tLJAGetSet.size(); i++){
            LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i + 1);

            //生成送银行表数据
            LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
            //设置统一的批次号
            tLYSendToBankSchema.setSerialNo(serialNoF);
            //收费标记
            tLYSendToBankSchema.setDealType("F");
            tLYSendToBankSchema.setPayCode(tLJAGetSchema.getActuGetNo());
            tLYSendToBankSchema.setBankCode(tLJAGetSchema.getBankCode());
            tLYSendToBankSchema.setAccName(tLJAGetSchema.getAccName());
            tLYSendToBankSchema.setAccNo(tLJAGetSchema.getBankAccNo());
            tLYSendToBankSchema.setPolNo(tLJAGetSchema.getOtherNo());
            tLYSendToBankSchema.setNoType(tLJAGetSchema.getOtherNoType());
            if (tLJAGetSchema.getOtherNoType().equals("4")){
                System.out.println(tLJAGetSchema.getOtherNoType()
                        + "-----------------------");
                String tljtempfee = "select * from ljtempfee where tempfeeno ='"
                        + tLJAGetSchema.getOtherNo() + "' ";
                LJTempFeeDB tLjtempfeeDB = new LJTempFeeDB();
                LJTempFeeSet tLJTempFeeSet = tLjtempfeeDB
                        .executeQuery(tljtempfee);
                LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(1);
                tLYSendToBankSchema.setComCode(tLJTempFeeSchema.getPolicyCom());
            }else{
                tLYSendToBankSchema.setComCode(tLJAGetSchema.getManageCom());
            }
            tLYSendToBankSchema.setAgentCode(tLJAGetSchema.getAgentCode());
            tLYSendToBankSchema.setPayMoney(tLJAGetSchema.getSumGetMoney());
            tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
            tLYSendToBankSchema.setDoType("0");
            //因为没有为发送银行盘表设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
            tLYSendToBankSchema.setModifyDate(PubFun.getCurrentDate());
            tLYSendToBankSchema.setModifyTime(PubFun.getCurrentTime());
            if (!StrTool.cTrim(tLJAGetSchema.getDrawerID()).equals("")){
                tLYSendToBankSchema.setIDType("0");
                tLYSendToBankSchema.setIDNo(tLJAGetSchema.getDrawerID());
            }
            if (!StrTool.cTrim(tLJAGetSchema.getDrawer()).equals("")){
                tLYSendToBankSchema.setName(tLJAGetSchema.getDrawer());
            }

            tLYSendToBankSet.add(tLYSendToBankSchema);

            //累加总金额和总数量
            dTotalMoney = dTotalMoney + tLJAGetSchema.getSumGetMoney();
            //转换精度
            dTotalMoney = Double.parseDouble((new DecimalFormat("0.00"))
                    .format(dTotalMoney));
            sumNumF = sumNumF + 1;
        }
        totalMoneyF = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));

        return tLYSendToBankSet;
    }
    
    /**
     * 生成银行日志表数据(应付)
     * @return
     */
    public LYBankLogSchema getBankLogF(String managecom)
    {
        LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

        tLYBankLogSchema.setBankCode("7707");
        tLYBankLogSchema.setLogType("F");
        tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setTotalMoney(totalMoneyF);
        tLYBankLogSchema.setTotalNum(sumNumF);
        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        tLYBankLogSchema.setComCode(managecom);

        return tLYBankLogSchema;
    }
    
    /**
     * 修改应收表银行在途标志,记录发送银行次数
     * @param tLJSPaySet
     * @return
     */
    private LJSPaySet modifyBankFlag(LJSPaySet tLJSPaySet){
    	
    	for (int i = 0; i < tLJSPaySet.size(); i++){
            System.out.println("modifyBankFlag:");
            LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i + 1);

            tLJSPaySchema.setBankOnTheWayFlag("1");
            tLJSPaySchema.setCanSendBank("9");
            tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
            tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
            tLJSPaySchema.setOperator(mGlobalInput.Operator);
            //记录发送银行次数
            tLJSPaySchema.setSendBankCount(tLJSPaySchema.getSendBankCount() + 1);
            
            tLJSPaySet.set(i + 1, tLJSPaySchema);
        }
        return tLJSPaySet;
    }
    
    /**
     * 修改应收表银行在途标志,记录发送银行次数
     * @param tLJSPaySet
     * @return
     */
    private LJAGetSet modifyBankFlag(LJAGetSet tLJAGetSet){
    	
    	for (int i = 0; i < tLJAGetSet.size(); i++){
            System.out.println("modifyBankFlag:");
            LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i + 1);

            tLJAGetSchema.setBankOnTheWayFlag("1");
            tLJAGetSchema.setCanSendBank("9");
            tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
            tLJAGetSchema.setOperator(mGlobalInput.Operator);
            //记录发送银行次数
            tLJAGetSchema.setSendBankCount(tLJAGetSchema.getSendBankCount() + 1);
            
            tLJAGetSet.set(i + 1, tLJAGetSchema);
        }
        return tLJAGetSet;
    }
    
    /**
     * 定义GlobalInput的属性值
     */
    private void assignGlobalInput(){
    	mGlobalInput.ManageCom = "86";
    	mGlobalInput.ComCode = "86";
    	mGlobalInput.Operator = "system";
    }
    
    public static void main(String[] args){
    	GoldenUnionFetchDataTask tGoldenUnionFetchDataTask = new GoldenUnionFetchDataTask();
    	tGoldenUnionFetchDataTask.run();
    }
}
