package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContRemainDB;
import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCInsureAccTraceDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.SustainedBBala;
import com.sinosoft.lis.schema.LCContRemainSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.vschema.LCContRemainSet;
import com.sinosoft.lis.vschema.LCInsureAccClassSet;
import com.sinosoft.lis.vschema.LCInsureAccTraceSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 个险万能险保单账户结算 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @version 1.0
 * @CreateDate：2007
 */
public class RemainEndTask extends TaskThread
{
    public CErrors mErrors = new CErrors();

    private String mContNo = null;

    private GlobalInput mGI = new GlobalInput();
    private MMap map = new MMap();

    public RemainEndTask()
    {
        mGI.Operator = "001";
        mGI.ComCode = "86";
        mGI.ManageCom = mGI.ComCode;
    }

    public RemainEndTask(GlobalInput cGlobalInput)
    {
        mGI = cGlobalInput;
    }
    
    public void runOneContno(String contno){
    	mContNo = contno;
    	run();
    }

    public void run()
    {

    	//查询留存期满的保单
        String sql = " select a.* from lccontremain a "
                     + " where a.remainstate='1' "  //第一次赠送
                     + " and exists (select 1 from lccont where contno=a.contno and (cvalidate +1 years)<=current date)  "
                     + "  with ur ";
        System.out.println(sql);

        LCContRemainDB tLCContRemainDB = new LCContRemainDB();
        LCContRemainSet tLCContRemainSet = new LCContRemainSet();
        tLCContRemainSet = tLCContRemainDB.executeQuery(sql);
        if(tLCContRemainSet==null||tLCContRemainSet.size()<=0){
        	System.out.println("没有需要处理的保单留存信息！");
        	mErrors.addOneError("没有需要处理的保单留存信息！");
        	return ;
        }else{
        	//对每个险种进行持续奖金分配
        	for(int i=1;i<=tLCContRemainSet.size();i++){
        		LCContRemainSchema tLCContRemainSchema = tLCContRemainSet.get(i);

        		LCContDB tLCContDB = new LCContDB();
        		tLCContDB.setContNo(tLCContRemainSchema.getContNo());
        		
        		if(!tLCContDB.getInfo()){
        			System.out.println("查询保单"+tLCContRemainSchema.getContNo()+"信息失败！");
        			mErrors.addOneError("查询保单"+tLCContRemainSchema.getContNo()+"信息失败！");
        			continue;
        		}
                
        		LCContSchema tLCContSchema = tLCContDB.getSchema();
        		String updateSQL = "UPDATE LCContRemain SET REMAINSTATE='2',REMAINENDDATE=date('"+tLCContSchema.getCValiDate()+"')+ 2 years," +
        				" MODIFYDATE=CURRENT DATE,MODIFYTIME=CURRENT TIME WHERE CONTNO= '"+tLCContRemainSchema.getContNo()+"' AND SERIALNO='"+tLCContRemainSchema.getSerialNo()+"' ";
        		
        		map.put(updateSQL, SysConst.UPDATE);
                 
        	}
        }
        submit();
        
    }
    
    /**
     * 提交操作
     */
    private void submit()
    {
        VData tVData = new VData();
        tVData.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(tVData, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            System.out.println(mErrors.getErrContent());
        }
        else
        {
            System.out.println("保单留存信息修改完成。");
        }
    }

    /**
     * 结算指定保单
     * @param cContNo String
     */
    public void runOneCont(String cContNo)
    {
        mContNo = cContNo;
        run();
    }


    public static void main(String[] args)
    {
    	RemainEndTask tRemainEndTask = new RemainEndTask();
    	tRemainEndTask.run();

    }
}
