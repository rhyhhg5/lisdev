package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;


/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: SinoSoft</p>
 * @author xiangchun
 * @version 1.0
 */

public class HuiFangTask extends TaskThread {

    private String currentDate = PubFun.getCurrentDate();
    //因为completetime时间包括分秒 所以没办法 只能用区间去限制
    //private String mStartDate=PubFun.calDate(currentDate,-1,"D",null);
    private String mStartDate = PubFun.getCurrentDate();
    private String mEndDate=PubFun.calDate(currentDate,1,"D",null);
    //private String mEndDate=currentDate;
    private SSRS mSSRS1 = new SSRS();
    public HuiFangTask() {

    }


    public void run() {
        System.out.println("");
        System.out.println(
                "＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
        System.out.println("参数个数:" + mParameters.size());
        System.out.println("");
        // Write your code here
       
        HuiFangTask a = new HuiFangTask();
        this.mSSRS1=a.EasyQuery();
        for (int i = 1; i <= mSSRS1.getMaxRow(); i++)
        {
            //先查询提数的记录集
            String tManageCom = mSSRS1.GetText(i, 1) ;
          if (!a.getDate(mStartDate,mEndDate, tManageCom))
             {
                return;
             }
        }
     }

    public static void main(String[] args) {
    	HuiFangTask a = new HuiFangTask();
    	String currentDate = PubFun.getCurrentDate();
    	String mStartDate=PubFun.calDate(currentDate,-1,"D",null);
    	a.run();
//    	 if (!a.getDate("2012-05-04","2012-05-04" ))
//         {
//            return;
//         }
    }

    //
    public boolean getDate(String tStartDate,String tEndDate,String tManageCom) {
        String FlagStr = "";
        GlobalInput tG = new GlobalInput();
        tG.Operator = "000"; //系统自签
        tG.ManageCom = "86"; //默认总公司
        //接收信息
      

        VData tVData=new VData();
        tVData.addElement(tG);
        tVData.addElement(tStartDate);
        tVData.addElement(tEndDate);
        tVData.addElement(tManageCom);
        
        HuiFangBL tHuiFangBL=new HuiFangBL();
        if (!tHuiFangBL.submitData(tVData,""))
        {
        FlagStr="Fail";
        return false;
        }
        else
        {
              System.out.print(" 成功! ");
              return true;
        }

    }
    // 获取支公司编码
    public  SSRS EasyQuery() {

        String strError = "";
        Integer intStart = new Integer(String.valueOf("1")); ;
        SSRS tSSRS = new SSRS();

//        String sql =
//                "select  comcode " +
//                "from ldcom where 1=1  and  " +
//                "sign='1'  and  length(trim(comcode))=8 and comcode like '8637%' order by comcode" ;
        
      String sql =
      "select  comcode " +
      "from ldcom where 1=1  and  " +
      "sign='1'  and  length(trim(comcode))=8 and substr(comcode,1,4) in " +
//      "('8631','8637','8651','8653','8665') order by comcode" ;
        //把机构 修改为配置，ldcode codetype :hufangcomcode   添加：8613  河北机构
      //其中
      	" (select code from ldcode where codetype ='huifangcomcode') order by comcode ";
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        //intMaxIndex = tSSRS.getMaxRow();
//        String[] getRowData = tSSRS.getRowData(intStartIndex);
        return tSSRS;
    }


}
