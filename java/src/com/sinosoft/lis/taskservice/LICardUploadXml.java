package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.certifybusiness.DentisServiceClient;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICardUploadLogSchema;
import com.sinosoft.lis.vschema.LICardUploadLogSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LICardUploadXml extends TaskThread{
	private GlobalInput mGlobalInput = null;
	private String mWageNo=null;
	private String mBranchtype=null;
	private String mBranchtype2=null;
	private String mManageCom=null;
	private String mOtherSign=null;
	private Element  root=new  Element("PACKET");
	private Document doc = new  Document(root);
	private Document doc1 = null;
	private SSRS tSSRS=null;
	private String mTranscationNum=null;
	/** 往后面传输数据的容器 */
    private VData mInputData;
    private MMap mMap = new MMap();
    private String mFilePath=null;
	private LICardUploadLogSet mLICardUploadLogSet=new LICardUploadLogSet();
	private StringBuffer tSB=new StringBuffer();
    public LICardUploadXml() {
    }

    public void run() {
        System.out.println("");
        System.out.println(
                "＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
        System.out.println("参数个数:" + mParameters.size());
        System.out.println("");
        if(!getInitData()){
        	System.out.println("牙科激活卡初始化数据时失败");
        	return;
        }
        if(!createJDomData()){
        	System.out.println("牙科激活卡取核心数据时失败");
        	return ;
        }
        if(!uploadJDomData()){
        	System.out.println("牙科激活卡上传数据时失败");
        	return ;
        }
        if(!save(doc)){
        	System.out.println("生成xml时失败");
        	return ;
        }
        if(!createLogData()){
        	System.out.println("牙科激活卡上传数据准备日志信息1时失败");
        	return ;
        }
        if(!prepareOutputData()){
        	System.out.println("牙科激活卡上传数据准备日志信息2时失败");
        	return ;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN"))
        {
        	System.out.println("牙科激活卡上传数据提交日志信息时失败");
        	return ;
        }
    }
    
    private boolean createJDomData(){
    	try{
    		root.addAttribute("type", "REQUEST");
    		root.addAttribute("version", "1.0");
    		Element head=new  Element("HEAD");
    		Element REQUEST_TYPE=new  Element("REQUEST_TYPE");
    		Element TRANSACTION_NUM=new  Element("TRANSACTION_NUM");
    		REQUEST_TYPE.addContent("PICC");
    		TRANSACTION_NUM.addContent(this.mTranscationNum);
    		head.addContent(REQUEST_TYPE);
    		head.addContent(TRANSACTION_NUM);
    		root.addContent(head);
    		Element body=new  Element("BODY");
    		Element INSURED_LIST=new  Element("INSURED_LIST");
    		root.addContent(body);
    		body.addContent(INSURED_LIST);
    		Element INSURED_DATA=null;
    		Element CARDNO=null;
    		Element RISKCODE=null;
    		Element PAYCOUNT=null;
    		Element CVALIDATE=null;
    		Element ENDDATE=null;
    		Element PERSON_NAME=null;
    		Element SEX=null;
    		Element ID_TYPE=null;
    		Element ID_NUMBER=null;
    		Element BIRTH_DATE=null;
    		SSRS tempSSRS=new SSRS();
    		tSSRS=new SSRS();
        	ExeSQL tExeSQL=new ExeSQL();
        	String tSQL	="select a.cardno,c.riskcode," +
        			"(select comcode from ldcode where codetype='TeethRiskCode' and code=c.riskcode),a.cvalidate,a.inactivedate," +
        			"a.name,a.sex,a.idtype,a.idno,a.birthday " +
        			"from licardactiveinfolist a,wfcontlist b,lmcardrisk c " +
        			"where a.cardno = b.cardno " +
        			"and b.certifycode=c.certifycode " +
        			"and c.riskcode in (select code from ldcode where codetype='TeethRiskCode') " +
        			"and a.modifydate=current date -1 day ";
        	tSSRS=tExeSQL.execSQL(tSQL);
        	if(tSSRS!=null&&tSSRS.MaxRow>0){
        		for(int i=1;i<=tSSRS.MaxRow;i++){
        			INSURED_DATA=new  Element("INSURED_DATA");
        			CARDNO=new  Element("CARDNO");
        			CARDNO.addContent(tSSRS.GetText(i,1));
        			INSURED_DATA.addContent(CARDNO);
        			RISKCODE=new  Element("RISKCODE");
        			tSQL="select distinct riskcode,calfactorvalue,calfactor from ldriskdutywrap where riskwrapcode='"
        				+tSSRS.GetText(i,2)+"' and calfactor in ('InsuYear','InsuYearFlag') order by calfactor asc";
        			tempSSRS=tExeSQL.execSQL(tSQL);
        			RISKCODE.addContent(tempSSRS.GetText(1,1));
        			INSURED_DATA.addContent(RISKCODE);
        			PAYCOUNT=new  Element("PAYCOUNT");
        			PAYCOUNT.addContent(tSSRS.GetText(i,3));
        			INSURED_DATA.addContent(PAYCOUNT);
        			CVALIDATE=new  Element("CVALIDATE");
        			CVALIDATE.addContent(tSSRS.GetText(i,4));
        			INSURED_DATA.addContent(CVALIDATE);
        			ENDDATE=new  Element("ENDDATE");
        			ENDDATE.addContent(PubFun.calDate(tSSRS.GetText(i,5), Integer.parseInt(tempSSRS.GetText(1, 2)), tempSSRS.GetText(2, 2), null));
        			INSURED_DATA.addContent(ENDDATE);
        			PERSON_NAME=new  Element("PERSON_NAME");
        			PERSON_NAME.addContent(tSSRS.GetText(i,6));
        			INSURED_DATA.addContent(PERSON_NAME);
        			SEX=new  Element("SEX");
        			SEX.addContent(tSSRS.GetText(i,7));
        			INSURED_DATA.addContent(SEX);
        			ID_TYPE=new  Element("ID_TYPE");
        			ID_TYPE.addContent(tSSRS.GetText(i,8));
        			INSURED_DATA.addContent(ID_TYPE);
        			ID_NUMBER=new  Element("ID_NUMBER");
        			ID_NUMBER.addContent(tSSRS.GetText(i,9));
        			INSURED_DATA.addContent(ID_NUMBER);
        			BIRTH_DATE=new  Element("BIRTH_DATE");
        			BIRTH_DATE.addContent(tSSRS.GetText(i,10));
        			INSURED_DATA.addContent(BIRTH_DATE);
        			INSURED_LIST.addContent(INSURED_DATA);
        		}
        	}else{
        		String ttSQL="select max(int(idx)) from licarduploadlog ";
        		int idx=0;
        		ExeSQL ttExeSQL=new ExeSQL();
        		String tRs=ttExeSQL.getOneValue(ttSQL);
        		if(tRs!=null&&!tRs.equals("")){
        			idx=Integer.parseInt(tRs);
        		}
        		LICardUploadLogSchema tLICardUploadLogSchema=null;
        		idx++;
    			tLICardUploadLogSchema=new LICardUploadLogSchema();
    			tLICardUploadLogSchema.setIdx(String.valueOf(idx));
    			tLICardUploadLogSchema.setRequestType("PICC");
    			tLICardUploadLogSchema.setTranscationNum(this.mTranscationNum);
    			tLICardUploadLogSchema.setStartDate(PubFun.getCurrentDate());
    			tLICardUploadLogSchema.setEndDate(PubFun.getCurrentDate());
    			tLICardUploadLogSchema.setCardNo("0");
    			tLICardUploadLogSchema.setRiskCode("0");
    			tLICardUploadLogSchema.setUploadFlag("4");
    			tLICardUploadLogSchema.setUploadInfo("没有查询到需要上传的数据！");
    			tLICardUploadLogSchema.setOperator("server");
    			tLICardUploadLogSchema.setMakeDate(PubFun.getCurrentDate());
    			tLICardUploadLogSchema.setMakeTime(PubFun.getCurrentTime());
    			tLICardUploadLogSchema.setModifyDate(PubFun.getCurrentDate());
    			tLICardUploadLogSchema.setModifyTime(PubFun.getCurrentTime());
    			this.mLICardUploadLogSet.add(tLICardUploadLogSchema);
    			this.mInputData = new VData();
                this.mMap.put(this.mLICardUploadLogSet,"INSERT");
                mInputData.add(mMap);
                PubSubmit tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN"))
                {
                	System.out.println("牙科激活卡上传数据提交日志信息时失败");
                	return false;
                }
        		return false;
        	}
        	return true;
    	}catch(Exception ex){
    		String ttSQL="select max(int(idx)) from licarduploadlog ";
    		int idx=0;
    		ExeSQL ttExeSQL=new ExeSQL();
    		String tRs=ttExeSQL.getOneValue(ttSQL);
    		if(tRs!=null&&!tRs.equals("")){
    			idx=Integer.parseInt(tRs);
    		}
    		LICardUploadLogSchema tLICardUploadLogSchema=null;
    		idx++;
			tLICardUploadLogSchema=new LICardUploadLogSchema();
			tLICardUploadLogSchema.setIdx(String.valueOf(idx));
			tLICardUploadLogSchema.setRequestType("PICC");
			tLICardUploadLogSchema.setTranscationNum(this.mTranscationNum);
			tLICardUploadLogSchema.setStartDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setEndDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setCardNo("0");
			tLICardUploadLogSchema.setRiskCode("0");
			tLICardUploadLogSchema.setUploadFlag("4");
			tLICardUploadLogSchema.setUploadInfo("在生成上传数据jdom时发生异常！");
			tLICardUploadLogSchema.setOperator("server");
			tLICardUploadLogSchema.setMakeDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setMakeTime(PubFun.getCurrentTime());
			tLICardUploadLogSchema.setModifyDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setModifyTime(PubFun.getCurrentTime());
			this.mLICardUploadLogSet.add(tLICardUploadLogSchema);
			this.mInputData = new VData();
            this.mMap.put(this.mLICardUploadLogSet,"INSERT");
            mInputData.add(mMap);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN"))
            {
            	System.out.println("牙科激活卡上传数据提交日志信息时失败");
            	return false;
            }
    		return false;
    	}
    	
    }
    
    public boolean save(Document pXmlDoc) {
    	String pName=this.mTranscationNum+".xml";
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(this.mFilePath);
		File mFileDir = new File(mFilePath.toString());
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
			}
		}
		File tempFile=new File(mFilePath.toString() + pName);
		if(!tempFile.exists()){
			try{
				if(!tempFile.createNewFile()){
					System.err.println("创建文件失败！" + tempFile.getPath());
				}
			}catch(Exception ex){
				System.err.println("创建文件时出现异常！" + tempFile.getPath());
				ex.printStackTrace();
			}
				
		}
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString()+ pName);
			JdomUtil.output(pXmlDoc, tFos);
			tFos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;
			
		}
		return true;
	}
    private boolean getInitData(){
    	ExeSQL tExeSQL=new ExeSQL();
    	String tResult=null;
    	String tSQL="select max(distinct transcationnum) from LICardUploadLog "
    		+" where substr(transcationnum,1,8)=date_format(current date,'yyyymmdd')";
    	tResult=tExeSQL.getOneValue(tSQL);
    	if(tResult!=null&&!tResult.equals("")){
    		String tempStr=tResult.substring(8,11);
    		int tempInt=Integer.parseInt(tempStr);
    		if(tempInt+1>99){
    			tResult=tResult.substring(0,8)+String.valueOf(tempInt+1);
    		}else if(tempInt+1>9){
    			tResult=tResult.substring(0,9)+String.valueOf(tempInt+1);
    		}else{
    			tResult=tResult.substring(0,10)+String.valueOf(tempInt+1);
    		}
    	}else{
    		tSQL="select date_format(current date,'yyyymmdd')||'001' from dual";
    		tResult=tExeSQL.getOneValue(tSQL);
    	}
    	mTranscationNum=tResult;
    	this.mFilePath=this.getClass().getResource("/").getPath();
    	this.mFilePath=this.mFilePath.replaceAll("WEB-INF/classes/", "vtsfile/cardupload/");
//    	this.mFilePath+=mTranscationNum+".xml";
    	System.out.println("...........this.mFilePath"+this.mFilePath);
    	return true;
    }
    
    private boolean uploadJDomData(){
    	DentisServiceClient tDentisServiceClient=new DentisServiceClient();
//    	String tServerAddress="http://121.52.210.101/Service.asmx";
    	String tServerAddress="http://121.52.210.92:8080/BrightSmile/ser/sys/UploadData?";
    	try{
    		doc1=tDentisServiceClient.callUploadXmlService(tServerAddress, doc);
    	}catch(Exception ex){
    		String tSQL="select max(int(idx)) from licarduploadlog ";
    		int idx=0;
    		ExeSQL tExeSQL=new ExeSQL();
    		String tRs=tExeSQL.getOneValue(tSQL);
    		if(tRs!=null&&!tRs.equals("")){
    			idx=Integer.parseInt(tRs);
    		}
    		LICardUploadLogSchema tLICardUploadLogSchema=null;
    		idx++;
			tLICardUploadLogSchema=new LICardUploadLogSchema();
			tLICardUploadLogSchema.setIdx(String.valueOf(idx));
			tLICardUploadLogSchema.setRequestType("PICC");
			tLICardUploadLogSchema.setTranscationNum(this.mTranscationNum);
			tLICardUploadLogSchema.setStartDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setEndDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setCardNo("0");
			tLICardUploadLogSchema.setRiskCode("0");
			tLICardUploadLogSchema.setUploadFlag("4");
			tLICardUploadLogSchema.setUploadInfo("在上传时发生异常！");
			tLICardUploadLogSchema.setOperator("server");
			tLICardUploadLogSchema.setMakeDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setMakeTime(PubFun.getCurrentTime());
			tLICardUploadLogSchema.setModifyDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setModifyTime(PubFun.getCurrentTime());
			this.mLICardUploadLogSet.add(tLICardUploadLogSchema);
			this.mInputData = new VData();
            this.mMap.put(this.mLICardUploadLogSet,"INSERT");
            mInputData.add(mMap);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN"))
            {
            	System.out.println("牙科激活卡上传数据提交日志信息时失败");
            	return false;
            }
    		return false;
    	}
    	return true;
    }
    
    private boolean createLogData()
    {
    	String tSQL="select max(int(idx)) from licarduploadlog ";
		int idx=0;
		SSRS tempSSRS=new SSRS();
		ExeSQL tExeSQL=new ExeSQL();
		String tRs=tExeSQL.getOneValue(tSQL);
		if(tRs!=null&&!tRs.equals("")){
			idx=Integer.parseInt(tRs);
		}
		LICardUploadLogSchema tLICardUploadLogSchema=null;
    	if(doc1!=null){
    		Element root1=doc1.getRootElement();
    		Element HEAD = root1.getChild("HEAD");
    		Element REQUEST_TYPE = HEAD.getChild("REQUEST_TYPE");
    		Element TRANSACTION_NUM = HEAD.getChild("TRANSACTION_NUM");
    		Element RESPONSE_CODE = HEAD.getChild("RESPONSE_CODE");
    		
    		Element ERROR_MESSAGE = HEAD.getChild("ERROR_MESSAGE");
    		
    		for(int i=1;i<=tSSRS.MaxRow;i++){
    			idx++;
    			tLICardUploadLogSchema=new LICardUploadLogSchema();
    			tLICardUploadLogSchema.setIdx(String.valueOf(idx));
    			tLICardUploadLogSchema.setRequestType("PICC");
    			tLICardUploadLogSchema.setTranscationNum(this.mTranscationNum);
    			tLICardUploadLogSchema.setStartDate(PubFun.getCurrentDate());
    			tLICardUploadLogSchema.setEndDate(PubFun.getCurrentDate());
    			tLICardUploadLogSchema.setCardNo(tSSRS.GetText(i,1));
    			tSQL="select distinct riskcode,calfactorvalue,calfactor from ldriskdutywrap where riskwrapcode='"
    				+tSSRS.GetText(i,2)+"' and calfactor in ('InsuYear','InsuYearFlag') order by calfactor asc";
    			tempSSRS=tExeSQL.execSQL(tSQL);
    			tLICardUploadLogSchema.setRiskCode(tempSSRS.GetText(1,1));
    			tLICardUploadLogSchema.setUploadFlag(RESPONSE_CODE.getText());
    			if(ERROR_MESSAGE.getText()!=null&&!ERROR_MESSAGE.getText().equals("")
    					&&RESPONSE_CODE.getText()!=null&&RESPONSE_CODE.getText().equals("0")){
    				tLICardUploadLogSchema.setUploadInfo(ERROR_MESSAGE.getText());
    			}else{
    				tLICardUploadLogSchema.setUploadInfo("上传成功！");
    			}
    			tLICardUploadLogSchema.setOperator("server");
    			tLICardUploadLogSchema.setMakeDate(PubFun.getCurrentDate());
    			tLICardUploadLogSchema.setMakeTime(PubFun.getCurrentTime());
    			tLICardUploadLogSchema.setModifyDate(PubFun.getCurrentDate());
    			tLICardUploadLogSchema.setModifyTime(PubFun.getCurrentTime());
    			this.mLICardUploadLogSet.add(tLICardUploadLogSchema);
    		}
    	}else{
    		idx++;
			tLICardUploadLogSchema=new LICardUploadLogSchema();
			tLICardUploadLogSchema.setIdx(String.valueOf(idx));
			tLICardUploadLogSchema.setRequestType("PICC");
			tLICardUploadLogSchema.setTranscationNum(this.mTranscationNum);
			tLICardUploadLogSchema.setStartDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setEndDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setCardNo("0");
			tLICardUploadLogSchema.setRiskCode("0");
			tLICardUploadLogSchema.setUploadFlag("3");
			tLICardUploadLogSchema.setUploadInfo("没有收到上传返回的信息，不知成功与否！");
			tLICardUploadLogSchema.setOperator("server");
			tLICardUploadLogSchema.setMakeDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setMakeTime(PubFun.getCurrentTime());
			tLICardUploadLogSchema.setModifyDate(PubFun.getCurrentDate());
			tLICardUploadLogSchema.setModifyTime(PubFun.getCurrentTime());
			this.mLICardUploadLogSet.add(tLICardUploadLogSchema);
    	}
    	return true;
    }
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mMap.put(this.mLICardUploadLogSet,"INSERT");
            mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            return false;
        }
        return true;
    }
    public static void main(String[] args){
    	LICardUploadXml tLICardUploadXml = new LICardUploadXml();
    	tLICardUploadXml.run();
    }
}
