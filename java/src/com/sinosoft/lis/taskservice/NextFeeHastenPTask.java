package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.task.Task;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.task.TaskPhoneFinishBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.task.CommonBL;
import com.sinosoft.task.CheckData;
import com.sinosoft.lis.schema.LGPhoneHastenSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LGPhoneHastenDB;
import com.sinosoft.lis.vschema.LGPhoneHastenSet;
import com.sinosoft.lis.operfee.IndiLJSCancelBL;
import com.sinosoft.lis.db.LCContDB;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 个单续期催缴业务处理类
 * 超过续期续保应收费日期或两次转帐不成功后，产生个险电话催缴工单
 * 若生成工单15（可配置）天未处理成功的，则自动进行撤销处理财务应收，并结案工单
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class NextFeeHastenPTask
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private String mWorkNo = null;  //受理号
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    private int mHastenCount = 0;  //处理成功的数据

    private int mCancelCount = 0; //撤销的催缴数

    /**
     * 构造函数
     */
    public NextFeeHastenPTask()
    {
    }

    /**
     * 带参数的构造函数
     * @param tGlobalInput GlobalInput：完整的用户信息
     */
    public NextFeeHastenPTask(GlobalInput tGlobalInput)
    {
        mGlobalInput = tGlobalInput;
    }

    /**
     * 执行个单催缴操作
     * @return boolean：执行成功true，否则false
     */
    public boolean submit()
    {
//        dealHastenData();
//        dealCancel();

        return true;
    }

    /**
     * 生成电话催缴工单
     * 超过续期续保应收费日期或两次转帐不成功后，产生个险电话催缴工单
     * @return boolean
     */
    private boolean dealHastenData()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
            .append("from LJSPay a ")
            .append("where otherNoType = '2' ")
            .append("   and PayDate - 10 days < current date ")
            //已送银行的记录不能生成催收记录1、//生成催收记录的记录会设为暂停状态2
            .append("   and (BankOnTheWayFlag not in('1', '2') or BankOnTheWayFlag is null) ")
            .append("   and SumDuePayMoney > 0 ")   //需要缴费
            .append("   and not exists ")
            .append("       (select 1 from LJTempFee ")  //未财务缴费
            .append("       where tempFeeNo = a.getNoticeNo and ConfFlag='0') ")
            .append("   and getNoticeNo not in ")
            .append("       (select case when getNoticeNo is null then '' else getNoticeNo end from LGPhoneHasten) ")
            .append("order by getNoticeNo ");
        
        System.out.println(sql.toString());
        
        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = null;
        int start = 1;   //开始位置
        int count = 1000;  //一次循环处理数据量

        do
        {
            MMap map = new MMap();
            tLJSPaySet = tLJSPayDB.executeQuery(sql.toString(), start, count);

            int counter = 0;
            for(int i = 1; i <= tLJSPaySet.size(); i++)
           // for(int i = 1; i <= 1 && i <= tLJSPaySet.size(); i++)
            {
                MMap tMMap = dealOne(tLJSPaySet.get(i));
                if(tMMap != null)
                {
                    map.add(tMMap);
                    counter++;
                }
            }

            VData data = new VData();
            data.add(map);

            PubSubmit tPubSubmit = new PubSubmit();
            if(!tPubSubmit.submitData(data, ""))
            {
                mErrors.addOneError("处理第" + start + "到" + (start + count)
                    + "条数据失败。");
            }
            this.mHastenCount += counter;

            start += count;
        }
        while(tLJSPaySet.size() > 0);

        return true;
    }

    /**
     * 生成一条应手记录的催缴任务
     * @param tLJSPaySchema:需要产生电话催缴工单的应手记录
     * @return MMap：处理后的数据集合，若失败反回null
     */
    private MMap dealOne(LJSPaySchema tLJSPaySchema)
    {
        MMap tMMap = new MMap();
        
        //如果是万能险失效保单不生成电话催缴工单记录
        //20090819 zhanggm 万能缓交状态设置有自己的批处理程序，不需要在此进行处理。
        if(com.sinosoft.lis.bq.CommonBL.hasULIRisk(tLJSPaySchema.getOtherNo()))
        {
        	/*
        	MMap cancelMMap = dealOneCancel(tLJSPaySchema);
        	if( cancelMMap !=  null)
        	{	
                tMMap.add(cancelMMap);
        	}
        	else
        	{
        		return null;
        	} 
        	*/  
        	return null;
        }
        else
        {
            MMap taskMMap = createTask(tLJSPaySchema);
            if(taskMMap == null)
            {
                return null;
            }
            tMMap.add(taskMMap);

            MMap hastenMMap = createHasten(tLJSPaySchema);
            if(hastenMMap == null)
            {
                return null;
            }
            tMMap.add(hastenMMap);
        }
        


        return tMMap;
    }

    /**
     * 生成电话催缴记录
     * @param tLJSPaySchema LJSPaySchema
     * @return MMap
     */
    private MMap createHasten(LJSPaySchema tLJSPaySchema)
    {
        LGPhoneHastenSchema tLGPhoneHastenSchema = new LGPhoneHastenSchema();
        tLGPhoneHastenSchema.setWorkNo(this.mWorkNo);
        tLGPhoneHastenSchema.setTimesNo("1");
        tLGPhoneHastenSchema.setEdorAcceptNo(tLJSPaySchema.getOtherNo());
        tLGPhoneHastenSchema.setCustomerNo(tLJSPaySchema.getAppntNo());
        tLGPhoneHastenSchema.setPayReason("个险续期续保缴费");
        tLGPhoneHastenSchema.setOldPayDate(tLJSPaySchema.getPayDate());
        tLGPhoneHastenSchema.setOldBankCode(tLJSPaySchema.getBankCode());
        tLGPhoneHastenSchema.setOldBackAccNo(tLJSPaySchema.getBankAccNo());
        tLGPhoneHastenSchema.setOldPayMode(getPayMode(tLJSPaySchema.getOtherNo()));
        tLGPhoneHastenSchema.setOperator(mGlobalInput.Operator);
        tLGPhoneHastenSchema.setHastenType(NextFeeHastenTask.HASTEN_TYPE_NEXTFEEP);
        tLGPhoneHastenSchema.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());

        PubFun.fillDefaultField(tLGPhoneHastenSchema);

         MMap tMMap = new MMap();
         tMMap.put(tLGPhoneHastenSchema, "INSERT");

         //将应收状态变更为正催收
        tLJSPaySchema.setBankOnTheWayFlag("2");
        tLJSPaySchema.setOperator(mGlobalInput.Operator);
        tLJSPaySchema.setModifyDate(this.mCurDate);
        tLJSPaySchema.setModifyTime(this.mCurTime);
        tMMap.put(tLJSPaySchema, "UPDATE");

         return tMMap;
    }

    /**
     * 得到保单的缴费方式
     * @param contNO String：保单号
     * @return String：缴费方式
     */
    private String getPayMode(String contNo)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(contNo);
        if(!tLCContDB.getInfo() || tLCContDB.getPayMode() == null
            || tLCContDB.getPayMode().equals(""))
        {
            mErrors.addOneError("查询保单" + contNo
                                + "的缴费方式失败，可能是保单数据不完整，请咨询IT");
            return null;
        }
        return tLCContDB.getPayMode();
    }

    /**
     * 生成工单信息
     * @param tLJSPaySchema LJSPaySchema
     * @return MMap
     */
    private MMap createTask(LJSPaySchema tLJSPaySchema)
    {
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(tLJSPaySchema.getAppntNo());
        tLGWorkSchema.setContNo(tLJSPaySchema.getOtherNo());
        tLGWorkSchema.setAcceptWayNo(Task.ACCEPTWAY_BATCH_AUTO);
        tLGWorkSchema.setStatusNo(Task.WORKSTATUS_UNDO);
        tLGWorkSchema.setApplyName("自动");
        tLGWorkSchema.setTypeNo(Task.WORK_TYPE_NEXTFEEP);
        tLGWorkSchema.setRemark("自动批注：个单缴费超期电话催缴。");

        String workBoxNo = CommonBL.getAutodeliverTarget(tLJSPaySchema.getManageCom(),
            CheckData.NEXTFEE_HASTEN_RULEP, null);
        if(workBoxNo == null)
        {
            mErrors.addOneError("没有查询到机构" + tLJSPaySchema.getManageCom()
                + "的转交信箱， 如有疑问请咨询IT。");
            return null;
        }

        VData tVData = new VData();
        tVData.add(tLGWorkSchema);
        tVData.add(mGlobalInput);
        tVData.add(workBoxNo);

        TaskInputBL tTaskInputBL = new TaskInputBL();
        MMap tMMap = tTaskInputBL.getSubmitData(tVData, "");
        if(tMMap == null)
        {
            mErrors.addOneError("需要人工核保，但是生成工单信息失败:\n"
                + tTaskInputBL.mErrors.getFirstError());
            return null;
        }
        this.mWorkNo = tTaskInputBL.getWorkNo();

        return tMMap;
    }

    /**
     * 得到处理生成的催缴任务数
     * @return int:处理生成的催缴任务数
     */
    public int getSucCount()
    {
        return mHastenCount;
    }

    /**
     * 得到撤销催缴任务数
     * @return int:撤销催缴任务数
     */
    public int getCancelCount()
    {
        return mCancelCount;
    }

    /**
     * 撤销超期的催缴工单
     * 规定电话催缴办理时间9天内未结案的催缴工单需要进行撤销处理,并重新设置应收的转账日期 20090302 zhanggm
     * @return boolean
     */
    private boolean dealCancel()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* from LGPhoneHasten a,LGWork b  ")
            .append("where a.WorkNo = b.WorkNo and b.statusno not in ('5','8') and b.typeno = '070016' ")  //未结案
            .append("and HastenType = '" + NextFeeHastenTask.HASTEN_TYPE_NEXTFEEP + "' ")  //个单续期电话催缴
            .append("and (select paydate from ljspayb where getnoticeno = a.getnoticeno) <= current date ")
            .append("with ur");

        LGPhoneHastenDB tLGPhoneHastenDB = new LGPhoneHastenDB();
        LGPhoneHastenSet tLGPhoneHastenSet = null;
        int start = 1;   //开始位置
        int count = 1000;  //一次循环处理数据量

        do
        {
            MMap map = new MMap();
            System.out.println("查询可作废的个险续期电话催缴工单："+sql.toString());
            tLGPhoneHastenSet = tLGPhoneHastenDB.executeQuery(sql.toString(), start, count);

            int counter = 0;
            for(int i = 1; i <= tLGPhoneHastenSet.size(); i++)
            {
                MMap tMMap = dealPhoneHasten(tLGPhoneHastenSet.get(i));
                if(tMMap != null)
                {
                    map.add(tMMap);
                    counter++;
                }
            }

            VData data = new VData();
            data.add(map);

            PubSubmit tPubSubmit = new PubSubmit();
            if(!tPubSubmit.submitData(data, ""))
            {
                mErrors.addOneError("处理第" + start + "到" + (start + count)
                    + "条数据失败。");
            }
            this.mCancelCount += counter;

            start += count;
        }
        while(tLGPhoneHastenSet.size() > 0);
        return true;
    }

    /**
     * 撤销一条应收记录
     * @param tLGPhoneHastenSchema LGPhoneHastenSchema：催缴记录
     * @return MMap：处理后的数据集合
     
    private MMap dealOneCancel(LGPhoneHastenSchema tLGPhoneHastenSchema)
    {
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo(tLGPhoneHastenSchema.getGetNoticeNo());

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("CancelMode", "3"); //撤销
//        tTransferData.setNameAndValue("IsRepeal", "Y");

        VData tVData = new VData();
        tVData.addElement(tLJSPaySchema);
        tVData.addElement(this.mGlobalInput);
        tVData.addElement(tTransferData);

        IndiLJSCancelBL tIndiLJSCancelBL = new IndiLJSCancelBL();
        MMap tMMap = tIndiLJSCancelBL.getSubmitMap(tVData, "INSERT");
        if(tMMap == null)
        {
            mErrors.copyAllErrors(tIndiLJSCancelBL.mCErrors);
            return null;
        }

        return tMMap;
    }
    
    */
    /**
     * 作废一条应收记录
     * @param LJSPaySchema tLJSPaySchema：催缴记录
     * @return MMap：处理后的数据集合
     */
    private MMap dealOneCancel(LJSPaySchema tLJSPaySchema)
    {
        tLJSPaySchema.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("CancelMode", "2"); //自动缓交

        VData tVData = new VData();
        tVData.addElement(tLJSPaySchema);
        tVData.addElement(this.mGlobalInput);
        tVData.addElement(tTransferData);

        IndiLJSCancelBL tIndiLJSCancelBL = new IndiLJSCancelBL();
        MMap tMMap = tIndiLJSCancelBL.getSubmitMap(tVData, "INSERT");
        if(tMMap == null)
        {
            mErrors.copyAllErrors(tIndiLJSCancelBL.mCErrors);
            return null;
        }
        return tMMap;
    }
    
    

    /**
     * 得到需要催缴宽限期
     * @return String
     */
    private String getGracePeriod()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select codeName ")
            .append("from LDCode ")
            .append("where trim(codeType) = 'ljspaygraceperiod' ");
//            .append("   and trim(code) = '");
        String gracePeriod = new ExeSQL().getOneValue(sql.toString());
        if(!gracePeriod.equals(""))
        {
            return gracePeriod;
        }

        return "15";
    }
    
//20090302 zhanggm 对催缴工单的处理
    private MMap dealPhoneHasten(LGPhoneHastenSchema aLGPhoneHastenSchema)
    {
        aLGPhoneHastenSchema.setFinishType("2");
        aLGPhoneHastenSchema.setRemark("应收记录" + aLGPhoneHastenSchema.getGetNoticeNo()
                         + "重新恢复可缴费状态，"+"系统自动作废本电话催缴工单");

        VData data = new VData();
        data.add(aLGPhoneHastenSchema);
        data.add(mGlobalInput);

        TaskPhoneFinishBL bl = new TaskPhoneFinishBL();
        MMap tMMap = bl.getSubmitMap(data, "");
        String sql = "update ljspay set bankonthewayflag = null , "
        	       + "operator = '" + mGlobalInput.Operator + "', " 
        	       + "modifydate = '" + mCurDate + "', modifytime = '" + mCurTime + "' "
        	       + "where getnoticeno = '" + aLGPhoneHastenSchema.getGetNoticeNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        sql = "update lgwork set statusno = '8', "
 	        + "operator = '" + mGlobalInput.Operator + "', " 
 	        + "modifydate = '" + mCurDate + "', modifytime = '" + mCurTime + "' "
 	        + "where workno = '" + aLGPhoneHastenSchema.getWorkNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        return tMMap;
    }

    public static void main(String[] args)
    {
    	NextFeeHastenPTask t = new NextFeeHastenPTask();
    	t.submit();
    }
}
