package com.sinosoft.lis.taskservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.sinosoft.lis.db.LAAccountsDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAInterChargeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LAAccountsSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAInterChargeSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAAccountsSet;
import com.sinosoft.lis.vschema.LAInterChargeSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAChargeJsonReadFromFTPTask extends TaskThread {

	private String interFilePath = "";
	private String mLogDate = "";
	private String mToday = PubFun.getCurrentDate();
	private String mTime = PubFun.getCurrentTime();
	private MMap mMap = new MMap();
	private VData mResult = new VData();
	private String CrmPath = "";
	private String mFileName = "";
	private ExeSQL mExeSQL = new ExeSQL();
	private String mBatchNo = PubFun1.CreateMaxNo("CRMBATCHNO", 20);
	private String mIP = "";
	private String mUserName = "";
	private String mPassword = "";
	private int mPort = 21;
	private String mTransMode = "";
	private String mURL = "";
	private LAInterChargeSet mLAInterChargeSet = new LAInterChargeSet();
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	private String mOperator = "jxxs";
	private String mFileDate = PubFun.getCurrentDate();
	private LJAGetSet mLJAGetSet = new LJAGetSet();
	private MMap mSecondMap = new MMap();
	private String Operator = "jxsh";
	private String mFilePosPoth = "";
	private LAInterChargeSet mUpLAInterChargeSet = new LAInterChargeSet();
	private LAInterChargeSet mQueryLAInterChargeSet = new LAInterChargeSet();

	public LAChargeJsonReadFromFTPTask() {
	}

	public void run() {
		if (!getInputData()) {
			return;
		}
		// 进行业务处理
		try {
			// 读取数据到核心
			if (!readTextDate()) {
				return;
			} else {
				// 生成应付数据
				if (!creatLjaGet()) {
					return;
				}
			}
		} catch (Exception ex) {
			System.out.println("readTextDate END With Exception...");
			ex.printStackTrace();
			return;
		} finally {

		}

	}

	private boolean getInputData() {
		System.out.println("初始化文件下载路径");
		String tSql = "select sysvarvalue from ldsysvar where sysvar = 'JXReadUrl'";
		CrmPath = mExeSQL.getOneValue(tSql);

//		CrmPath = "D:\\CRMDate1\\";
		return true;
	}

	private boolean creatLjaGet() {
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		ExeSQL ttExeSQL = new ExeSQL();
		LAAgentDB tLAAgentDB = new LAAgentDB();
		LAInterChargeDB tLAInterChargeDB = new LAInterChargeDB();
		LAAccountsSet tLAAccountsSet = new LAAccountsSet();
		String tsql = " select sum(PerCharge),agentcode,makedate,F9,PubToPubPaymentNo from laintercharge where makedate ='"
				+ mCurrentDate
				+ "' and f0='C' and  substr(usercomid,1,2) in ('31','44') and  substr(usercomid,1,4) <>'3110'  group by agentcode,makedate,F9,PubToPubPaymentNo  ";
		tSSRS = ttExeSQL.execSQL(tsql);
		String tPaySign = "C";
		if (tSSRS.getMaxRow() > 0) {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				LJAGetSchema tLJAGetSchema = new LJAGetSchema();
				LAAgentSchema tLAAgentSchema = new LAAgentSchema();
				String tLimit = PubFun.getNoLimit(tSSRS.GetText(i, 2));
				String tDealDate = tSSRS.GetText(i, 3).replace("-", "");
				String tActuGetNo = PubFun1.CreateMaxNo("BCGETNO", tLimit);
				tLJAGetSchema.setActuGetNo(tActuGetNo);

				tLJAGetSchema.setOtherNo(tSSRS.GetText(i, 2).trim() + tDealDate
						+ tPaySign);
				tLJAGetSchema.setGetNoticeNo(tSSRS.GetText(i, 2));
				tLJAGetSchema.setPayMode("11");
				tLJAGetSchema.setShouldDate(mFileDate);
				// tLJAGetSchema.setAgentGroup(tLAWageSchema.getAgentGroup());
				tLJAGetSchema.setOtherNoType("25");
				tLJAGetSchema.setManageCom(tSSRS.GetText(i, 4));
				tLJAGetSchema.setAgentCode(tSSRS.GetText(i, 2));
				tLJAGetSchema.setSumGetMoney(tSSRS.GetText(i, 1));
				tLAAgentDB.setAgentCode(tSSRS.GetText(i, 2));
				String tBatchNoSql = "select * from laintercharge where PubToPubPaymentNo='"
						+ tSSRS.GetText(i, 5)
						+ "' and makedate ='"
						+ tSSRS.GetText(i, 3)
						+ "' and agentcode ='"
						+ tSSRS.GetText(i, 2)
						+ "' "
						+ " and f9='"
						+ tSSRS.GetText(i, 4)
						+ "'  and f0='C' and  substr(usercomid,1,2) in ('31','44') and  substr(usercomid,1,4) <>'3110'  ";
				mQueryLAInterChargeSet = tLAInterChargeDB
						.executeQuery(tBatchNoSql);
				if (mQueryLAInterChargeSet.size() > 0) {
					for (int j = 1; j <= mQueryLAInterChargeSet.size(); j++) {
						LAInterChargeSchema tLAInterChargeSchema = new LAInterChargeSchema();
						tLAInterChargeSchema = mQueryLAInterChargeSet.get(j);
						tLAInterChargeSchema.setF11(tActuGetNo);
						tLAInterChargeSchema.setModifyDate(PubFun
								.getCurrentDate());
						tLAInterChargeSchema.setModifyTime(PubFun
								.getCurrentTime());
						mUpLAInterChargeSet.add(tLAInterChargeSchema);
					}
				}
				tLAAgentSchema = tLAAgentDB.getSchema();
				String tSql = "select * from laaccounts where agentcode ='"
						+ tLAAgentSchema.getAgentCode()
						+ "' and state ='0'  with ur ";
				LAAccountsSchema tLAAccountsSchema = new LAAccountsSchema();
				LAAccountsDB tLAAccountsDB = new LAAccountsDB();
				tLAAccountsSet = tLAAccountsDB.executeQuery(tSql);
				if (tLAAccountsSet.size() > 0) {
					tLAAccountsSchema = tLAAccountsDB.executeQuery(tSql).get(1);
					tLJAGetSchema.setBankAccNo(tLAAccountsSchema.getAccount());

					tLJAGetSchema
							.setAccName(tLAAccountsSchema.getAccountName());
					tLJAGetSchema.setBankCode(tLAAccountsSchema.getT1());
					// 添加领取人相关信息
					tLJAGetSchema.setDrawer(tLAAgentSchema.getName());
					// tLJAGetSchema.setDrawerID(tLAAgentSchema.getIDNo());

				}
				tLJAGetSchema.setOperator(Operator);
				tLJAGetSchema.setMakeDate(mCurrentDate);
				tLJAGetSchema.setMakeTime(mCurrentTime);
				tLJAGetSchema.setModifyDate(mCurrentDate);
				tLJAGetSchema.setModifyTime(mCurrentTime);
				this.mLJAGetSet.add(tLJAGetSchema);
			}
		}
		mSecondMap.put(this.mLJAGetSet, "INSERT");
		mSecondMap.put(this.mUpLAInterChargeSet, "UPDATE");
		if (!SubmitMap(mSecondMap)) {
			return false;
		}
		return true;
	}

	private boolean readTextDate() throws java.io.FileNotFoundException,
			java.io.IOException {

		System.out.println("开始读取FTP数据");
		// 删除核心服务期旧文件
		 RefreshFileList(CrmPath);
		// 从财险FTP服务器下载文件到核心服务器
		 FtpDownFiles();
		System.out.println("文件下载完毕");
		// 读取核心服务器文件并插入到核心数据库
		File dir = new File(CrmPath);
		File[] files = dir.listFiles();
		if (files == null) {
			System.out.println("文件查询错误");
			return false;
		}
		for (int k = 0; k < files.length; k++) {
			String strFileName = files[k].getName();
			System.out.println(strFileName);
			// 判断是否数据库中是否已经存在此文件
			String judgefileSql = "select distinct 1 from laintercharge where F1 ='"
					+ strFileName + "' ";
			ExeSQL tExeSQL = new ExeSQL();
			String FileExist = tExeSQL.getOneValue(judgefileSql);
			if ("1".equals(FileExist)) {
				System.out.println("文件已经在数据库存中存在！");
				continue;
			}
			// file
			// String mLogName = FindLogFile(CrmPath);
			// if(mLogName.equals(""))
			// return false;
			// interFilePath = CrmPath + mLogName;
			// String mFileName="";
			interFilePath = CrmPath + strFileName;
			String toDelete = "I[F001]:";
			System.out.println("文件路径--+++++++----" + interFilePath
					+ "#@@@#@#@@#@@#@#");
			// interFilePath="d:\\crmdata\\" + mFileName;
			// BufferedReader reader = new BufferedReader(new
			// InputStreamReader(new FileInputStream(interFilePath), "GBK"));
			// FileReader fr = new FileReader(interFilePath);
			// String
			// testFilepath="/wasfs/lis_war/printdata/20120905U210102788793885.xml";
			// //String test2FilePath="";
			// FileInputStream tfis1 = new FileInputStream(testFilepath);
			// System.out.println("@test1读取成功："+tfis1.read());
			// InputStreamReader tisr1 = new InputStreamReader(tfis1,"GBK");
			// System.out.println("@test2读取成功："+tisr1.read());
			// BufferedReader reader1 = new BufferedReader(tisr1);
			// System.out.println("@@test3文件成功读取"+reader1.read()+"字符@@");

			FileInputStream tfis = new FileInputStream(interFilePath);
			// System.out.println("@1读取成功："+tfis.read());
			InputStreamReader tisr = new InputStreamReader(tfis, "GBK");
			// System.out.println("@2读取成功："+tisr.read());
			BufferedReader reader = new BufferedReader(tisr);
			// System.out.println("@@文件成功读取"+reader.read()+"字符@@");
			// BufferedReader reader = new BufferedReader(fr);
			String Line = reader.readLine();

			LAAgentDB tLAAgentDB = new LAAgentDB();
			int countNum = 0;
			while (Line != null) {

				if (Line.indexOf("policyList") != -1) {
					JSONObject jn = JSONObject.fromObject(Line);
					LAInterChargeSchema tJsonLAInterChargeSchema = new LAInterChargeSchema();

					String tJsonuserName = "";
					String tJsonUserComId = "";
					String tJsonHandler1Code = "";
					String tJsonHandler1ComCode = "";
					String tJsonHandlerCode = "";
					String tJsonHandlerComCode = "";
					String tJsonPubToPubPaymentNo = "";
					String tJsonCommissionNonExtractionDesc = "";
					String tJsonSettleNo = "";
					String tJsonPayNo = "";
					String tJsonCertiNo = "";
					String tJsonSerialNo = "";
					String tJsonCostFeeType = "";

					String tJsonCommissionAmount = jn
							.getString("commissionAmount");
					String tJsonCommissionExtractionStatus = jn
							.getString("commissionExtractionStatus");

					// String tJsonCrossSellAgentCode = jn
					// .getString("crossSellAgentCode");
					// String tJsonCrossSellComCode = jn
					// .getString("crossSellComCode");
					// String tJsonCrossSellUserCode = jn
					// .getString("crossSellUserCode");
					// String tJsonCrossSellStatus =
					// jn.getString("crossSellStatus");
					String tJsonPolicyComId = jn.getString("policyComId");
					String tJsonPolicyNo = jn.getString("policyNo");
					String tJsonPolicyPremium = jn.getString("policyPremium");
					String tJsonUserCode = "";
					if (jn.has("userCode")) {
						tJsonUserCode = jn.getString("userCode");
					}
					if (jn.has("userComId")) {
						tJsonUserComId = jn.getString("userComId");
					}
					if (jn.has("userName")) {
						tJsonuserName = jn.getString("userName");
					}
					if (jn.has("handler1Code")) {
						tJsonHandler1Code = jn.getString("handler1Code").trim();
					}
					if (jn.has("handler1ComCode")) {
						tJsonHandler1ComCode = jn.getString("handler1ComCode");
					}
					if (jn.has("handlerCode")) {
						tJsonHandlerCode = jn.getString("handlerCode").trim();
					}
					if (jn.has("handlerComCode")) {
						tJsonHandlerComCode = jn.getString("handlerComCode")
								.trim();
					}
					if (jn.has("pubToPubPaymentNo")) {
						tJsonPubToPubPaymentNo = jn
								.getString("pubToPubPaymentNo");
					}
					if (jn.has("commissionNonExtractionDesc")) {
						tJsonCommissionNonExtractionDesc = jn
								.getString("commissionNonExtractionDesc");
					}
					if (jn.has("settleNo")) {
						tJsonSettleNo = jn.getString("settleNo");
					}
					if (jn.has("payNo")) {
						tJsonPayNo = jn.getString("payNo");
					}
					if (jn.has("certiNo")) {
						tJsonCertiNo = jn.getString("certiNo");
					}
					if (jn.has("serialNo")) {
						tJsonSerialNo = jn.getString("serialNo");
					}
					if (jn.has("costFeeType")) {
						tJsonCostFeeType = jn.getString("costFeeType");
					}
					String tJsonPolicySystem = jn.getString("policySystem");
					// String tJsonMakeCode = jn.getString("makeCode");
					// String tJsonMakeComCode = jn.getString("makeComCode");
					String tJsonCostFee = jn.getString("costFee");
					String tJsonPaydocFee = jn.getString("payDocFee");
					String tJsonCostRate = jn.getString("costrate");
					// String tJsonReMark = jn.getString("remark");
					String tJsonSignDate = jn.getString("signDate");

					double tPerRate = 0.8;
					double tCharge = 0;
					double tPerCharge = 0;
					double tRemainCharge = 0;
					// LAinterCharge表主键生成方式
					String tLimit = PubFun.getNoLimit(tJsonPolicyNo);
					String tInterActNo = PubFun1.CreateMaxNo("ICGETNO", tLimit);
					tCharge = Double.parseDouble(tJsonPaydocFee);
					tPerCharge = mulrate(tPerRate, tCharge);
					// System.out.println("dfdfdfdfd我tPerCharge11的值："+tPerCharge);
					tPerCharge = Arith.round(tPerCharge, 2);
					// System.out.println("dfdfdfdfd我tPerCharge的值："+tPerCharge);
					// tPerCharge=tPerRate*tCharge;
					tRemainCharge = tCharge - tPerCharge;
					// System.out.println("tRemainCharge的值："+tRemainCharge);
					if (null != tJsonUserCode && !"".equals(tJsonUserCode)) {
						// 业务员转换为内部工号
						tLAAgentDB.setGroupAgentCode(tJsonUserCode);
						String tAgentCode = "";
						String tManageCom = "";

						if (tLAAgentDB.query().size() > 0) {
							tAgentCode = tLAAgentDB.query().get(1)
									.getAgentCode();
							tManageCom = tLAAgentDB.query().get(1)
									.getManageCom();

						}

						tJsonLAInterChargeSchema.setInterActNo(tInterActNo);
						tJsonLAInterChargeSchema
								.setCommissionAmount(tJsonCommissionAmount);
						tJsonLAInterChargeSchema
								.setCommissionExtractionStatus(tJsonCommissionExtractionStatus);
						tJsonLAInterChargeSchema
								.setCommissionNonPaymentDesc(tJsonCommissionNonExtractionDesc);
						// tJsonLAInterChargeSchema
						// .setCrossSellAgentCode(tJsonCrossSellAgentCode);
						// tJsonLAInterChargeSchema
						// .setCrossSellComCode(tJsonCrossSellComCode);
						// tJsonLAInterChargeSchema
						// .setCrossSellUserCode(tJsonCrossSellUserCode);
						tJsonLAInterChargeSchema
								.setPolicyComId(tJsonPolicyComId);
						tJsonLAInterChargeSchema.setPolicyNo(tJsonPolicyNo);
						tJsonLAInterChargeSchema.setPolicyPremium(Double
								.parseDouble(tJsonPolicyPremium));
						tJsonLAInterChargeSchema.setUsercode(tJsonUserCode);
						tJsonLAInterChargeSchema.setUserComId(tJsonUserComId);
						tJsonLAInterChargeSchema.setUserName(tJsonuserName);

						tJsonLAInterChargeSchema
								.setHandlerCode(tJsonHandlerCode);
						tJsonLAInterChargeSchema
								.setHandlerComCode(tJsonHandlerComCode);
						tJsonLAInterChargeSchema
								.setHandler1Code(tJsonHandler1Code);
						tJsonLAInterChargeSchema
								.setHandler1ComCode(tJsonHandler1ComCode);
						// tJsonLAInterChargeSchema.setMakeCode(tJsonMakeCode);
						// tJsonLAInterChargeSchema.setMakeComCode(tJsonMakeComCode);
						tJsonLAInterChargeSchema.setCostFee(tJsonCostFee);
						tJsonLAInterChargeSchema.setCostRate(tJsonCostRate);
						tJsonLAInterChargeSchema.setPaydocFee(tCharge);
						// tJsonLAInterChargeSchema.setRemark(tJsonReMark);
						tJsonLAInterChargeSchema
								.setPubToPubPaymentNo(tJsonPubToPubPaymentNo);
						tJsonLAInterChargeSchema
								.setPolicySystem(tJsonPolicySystem);

						tJsonLAInterChargeSchema.setMakeDate(mCurrentDate);
						tJsonLAInterChargeSchema.setMakeTime(mCurrentTime);
						tJsonLAInterChargeSchema.setModifyDate(mCurrentDate);
						tJsonLAInterChargeSchema.setModifyTime(mCurrentTime);
						tJsonLAInterChargeSchema.setAgentCode(tAgentCode);
						tJsonLAInterChargeSchema.setPerCharge(tPerCharge);
						tJsonLAInterChargeSchema.setReMainCharge(tRemainCharge);
						tJsonLAInterChargeSchema.setF1(strFileName);
						tJsonLAInterChargeSchema.setF0("C");
						// 签单日期
						tJsonLAInterChargeSchema.setF6(tJsonSignDate);
						// 管理机构
						tJsonLAInterChargeSchema.setF9(tManageCom);
						// 结算期次号
						tJsonLAInterChargeSchema.setF2(tJsonPayNo);
						// 保单序号
						tJsonLAInterChargeSchema.setC1(tJsonSerialNo);
						// 佣金类型
						tJsonLAInterChargeSchema.setF3(tJsonCostFeeType);
						// 保批单号
						tJsonLAInterChargeSchema.setF4(tJsonCertiNo);
						// 结算单号
						tJsonLAInterChargeSchema.setF5(tJsonSettleNo);

						mLAInterChargeSet.add(tJsonLAInterChargeSchema);
					}
					// JSONObject tJsonPolicyList =(JSONObject)
					// jn.get("policyList");
					// JSONArray tJsonMainInsuranceList = (JSONArray)
					// tJsonPolicyList.get("mainInsuranceList");

					// if(tJsonMainInsuranceList.size()>0)
					// {
					// for (int i = 0; i < tJsonMainInsuranceList.size(); i++)
					// {
					// JSONObject tSubMainJSONObject = new JSONObject();
					// LAInterChargeSchema tMainLAInterChargeSchema = new
					// LAInterChargeSchema();
					// tSubMainJSONObject =
					// tJsonMainInsuranceList.getJSONObject(i);
					//
					// String tJsonSubMainPremium =
					// tSubMainJSONObject.getString("mainPremium");
					// String tJsonSubMainRiskCode =
					// tSubMainJSONObject.getString("mainRiskCode");
					// String tJsonSubMainRiskName =
					// tSubMainJSONObject.getString("mainRiskName");
					//
					// //LAinterCharge表主键生成方式
					// String tLimit = PubFun.getNoLimit(tJsonPolicyNo);
					// String tInterActNo = PubFun1.CreateMaxNo("ICGETNO",
					// tLimit);
					// tCharge = Double.parseDouble(tJsonCommissionAmount);
					// //业务员转换为内部工号
					// tLAAgentDB.setGroupAgentCode(tJsonUserCode);
					// String tAgentCode="";
					// if(tLAAgentDB.query().size()>0){
					// tAgentCode = tLAAgentDB.query().get(1).getAgentCode();
					// }
					//
					//
					// tMainLAInterChargeSchema.setInterActNo(tInterActNo);
					// tMainLAInterChargeSchema.setCommissionAmount(tCharge);
					// tMainLAInterChargeSchema.setCommissionExtractionStatus(tJsonCommissionExtractionStatus);
					// tMainLAInterChargeSchema.setCommissionNonPaymentDesc(tJsonCommissionNonExtractionDesc);
					// tMainLAInterChargeSchema.setCrossSellAgentCode(tJsonCrossSellAgentCode);
					// tMainLAInterChargeSchema.setCrossSellComCode(tJsonCrossSellComCode);
					// tMainLAInterChargeSchema.setCrossSellUserCode(tJsonCrossSellUserCode);
					// tMainLAInterChargeSchema.setF3(tJsonPolicyComId);
					// tMainLAInterChargeSchema.setPolicyNo(tJsonPolicyNo);
					// tMainLAInterChargeSchema.setPolicyPremium(Double.parseDouble(tJsonPolicyPremium));
					// tMainLAInterChargeSchema.setUsercode(tJsonUserCode);
					// tMainLAInterChargeSchema.setUserComId(tJsonUserComId);
					// tMainLAInterChargeSchema.setUserName(tJsonuserName);
					// tMainLAInterChargeSchema.setMainPremium(Double.parseDouble(tJsonSubMainPremium));
					// tMainLAInterChargeSchema.setMainRiskCode(tJsonSubMainRiskCode);
					// tMainLAInterChargeSchema.setMainRiskName(tJsonSubMainRiskName);
					// tMainLAInterChargeSchema.setMakeDate(mCurrentDate);
					// tMainLAInterChargeSchema.setMakeTime(mCurrentTime);
					// tMainLAInterChargeSchema.setModifyDate(mCurrentDate);
					// tMainLAInterChargeSchema.setModifyTime(mCurrentTime);
					// tMainLAInterChargeSchema.setAgentCode(tAgentCode);
					// tMainLAInterChargeSchema.setPerCharge(tCharge*tPerRate);
					// tMainLAInterChargeSchema.setReMainCharge(tCharge-tCharge*tPerRate);
					// tMainLAInterChargeSchema.set
					// mLAInterChargeSet.add(tMainLAInterChargeSchema);
					// }
					//
					// }
					// JSONArray tJsonSubInsuranceList = (JSONArray)
					// tJsonPolicyList.get("subInsuranceList");
					// if(tJsonSubInsuranceList.size()>0)
					// {
					// for (int i = 0; i < tJsonSubInsuranceList.size(); i++)
					// {
					// JSONObject tSubInsuranceListObject = new JSONObject();
					// LAInterChargeSchema tSubLAInterChargeSchema = new
					// LAInterChargeSchema();
					// tSubInsuranceListObject =
					// tJsonSubInsuranceList.getJSONObject(i);
					// String tJsonSubMainPremium =
					// tSubInsuranceListObject.getString("subPremium");
					// String tJsonSubMainRiskCode =
					// tSubInsuranceListObject.getString("subRiskCode");
					// String tJsonSubMainRiskName =
					// tSubInsuranceListObject.getString("subRiskName");
					//
					// //LAinterCharge表主键生成方式
					// String tLimit = PubFun.getNoLimit(tJsonPolicyNo);
					// String tInterActNo = PubFun1.CreateMaxNo("ICGETNO",
					// tLimit);
					//
					// //业务员转换为内部工号
					// tLAAgentDB.setGroupAgentCode(tJsonUserCode);
					// String tAgentCode="";
					// if(tLAAgentDB.query().size()>0){
					// tAgentCode = tLAAgentDB.query().get(1).getAgentCode();
					// }
					//
					//
					// tSubLAInterChargeSchema.setInterActNo(tInterActNo);
					// tSubLAInterChargeSchema.setCommissionAmount(Double.parseDouble(tJsonCommissionAmount));
					// tSubLAInterChargeSchema.setCommissionExtractionStatus(tJsonCommissionExtractionStatus);
					// tSubLAInterChargeSchema.setCommissionNonPaymentDesc(tJsonCommissionNonExtractionDesc);
					// tSubLAInterChargeSchema.setCrossSellAgentCode(tJsonCrossSellAgentCode);
					// tSubLAInterChargeSchema.setCrossSellComCode(tJsonCrossSellComCode);
					// tSubLAInterChargeSchema.setCrossSellUserCode(tJsonCrossSellUserCode);
					// tSubLAInterChargeSchema.setF3(tJsonPolicyComId);
					// tSubLAInterChargeSchema.setPolicyNo(tJsonPolicyNo);
					// tSubLAInterChargeSchema.setPolicyPremium(Double.parseDouble(tJsonPolicyPremium));
					// tSubLAInterChargeSchema.setUsercode(tJsonUserCode);
					// tSubLAInterChargeSchema.setUserComId(tJsonUserComId);
					// tSubLAInterChargeSchema.setUserName(tJsonuserName);
					// tSubLAInterChargeSchema.setMainPremium(Double.parseDouble(tJsonSubMainPremium));
					// tSubLAInterChargeSchema.setMainRiskCode(tJsonSubMainRiskCode);
					// tSubLAInterChargeSchema.setMainRiskName(tJsonSubMainRiskName);
					// tSubLAInterChargeSchema.setMakeDate(mCurrentDate);
					// tSubLAInterChargeSchema.setMakeTime(mCurrentTime);
					// tSubLAInterChargeSchema.setModifyDate(mCurrentDate);
					// tSubLAInterChargeSchema.setModifyTime(mCurrentTime);
					// tSubLAInterChargeSchema.setAgentCode(tAgentCode);
					// mLAInterChargeSet.add(tSubLAInterChargeSchema);
					// }
					// }

					if (countNum == 5000) {
						mMap.put(mLAInterChargeSet, "INSERT");
						if (!SubmitMap(mMap)) {
							return false;
						}
						mLAInterChargeSet.clear();
						mMap.keySet().clear();
						countNum = 0;
					}

					System.out.println("here1:" + Line);

					countNum++;
				}
				Line = reader.readLine();
			}
			mMap.put(mLAInterChargeSet, "INSERT");
			if (!SubmitMap(mMap)) {
				return false;
			}

			mLAInterChargeSet.clear();
			mMap.keySet().clear();
			// RefreshFileList(CrmPath);
			// tfis1.close();
			// tisr1.close();
			// reader1.close();
			tfis.close();
			tisr.close();
			reader.close();
		}
		return true;
	}

	private boolean RefreshFileList(String StrPath) {
		File dir = new File(StrPath);
		File[] files = dir.listFiles();
		if (files == null) {
			return false;
		}
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				RefreshFileList(files[i].getAbsolutePath());
			} else {
				String strFileName = files[i].getAbsolutePath();
				if (strFileName.endsWith("000085.txt")) {
					System.out.println("正在删除：" + strFileName);
					files[i].delete();
				}
			}
		}
		return true;
	}

	/* 数据处理 */
	private boolean SubmitMap(MMap tmmap) {
		PubSubmit tPubSubmit = new PubSubmit();
		mResult = new VData();
		mResult.add(tmmap);
		if (!tPubSubmit.submitData(mResult, "")) {
			System.out.println("提交数据失败！");
			return false;
		}
		// System.gc();
		return true;
	}

	private boolean FtpDownFiles() {

		String tFtpSQL = "select code,codename from ldcode where codetype = 'jxchargeftp' ";
		SSRS tSSRS = new ExeSQL().execSQL(tFtpSQL);
		if (tSSRS == null || tSSRS.MaxRow < 1) {
			System.out.println("没有找到集团交叉ftp配置信息！");
			return false;
		}

		String tFileSql = "select codename from ldcode where codetype ='caichuanjian' and code ='ccj' ";
		SSRS tSSRS1 = new ExeSQL().execSQL(tFileSql);
		if (tSSRS1 == null || tSSRS1.MaxRow < 1) {
			System.out.println("没有找到财险传递健康险的文件路径！");
			return false;
		}
		mFilePosPoth = tSSRS1.GetText(1, 1);

		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			if ("IP".equals(tSSRS.GetText(i, 1))) {
				mIP = tSSRS.GetText(i, 2);
			} else if ("UserName".equals(tSSRS.GetText(i, 1))) {
				mUserName = tSSRS.GetText(i, 2);
			} else if ("Password".equals(tSSRS.GetText(i, 1))) {
				mPassword = tSSRS.GetText(i, 2);
			} else if ("Port".equals(tSSRS.GetText(i, 1))) {
				mPort = Integer.parseInt(tSSRS.GetText(i, 2));
			} else if ("TransMode".equals(tSSRS.GetText(i, 1))) {
				mTransMode = tSSRS.GetText(i, 2);
			}
		}

		FTPTool tFTPTool = new FTPTool(mIP, mUserName, mPassword, mPort,
				mTransMode);
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println(tFTPTool.getErrContent(1));
			} else {
				// String tFileImportPath =
				// "/gather/gather/mqm/sales/rec/mainData/";//ftp上存放文件的目录
				// String tFileImportPath = "D:\\CRMDate\\";//ftp上存放文件的目录

				String tBeForDate = getSpecifiedDayBefore(mFileDate).replace(
						"-", "");
				System.out.println("日期为：" + tBeForDate);
				System.out.println("核心路径为：" + CrmPath);
				System.out.println("ftp路径为：" + mFilePosPoth);
				tFTPTool.List(mFilePosPoth);
				System.out.println("文件个数为:" + tFTPTool.arFiles.size());
				if (tFTPTool.arFiles.size() > 0) {
					for (int i = 0; i < tFTPTool.arFiles.size(); i++) {
						String tFileName = (String) tFTPTool.arFiles.get(i);
						System.out.println("要下载的文件名为");
						System.out.println("@@@@@@" + tFileName + "$$$$$$$$");

						if (tFileName.substring(10, 18).equals(tBeForDate)) {
							System.out.println("开始下载");
							tFTPTool.downloadFile(mFilePosPoth, CrmPath,
									tFileName);
							System.out.println("文件下载成功了！");
						}
					}
				}
				// String tFileImportPathBackUp =
				// "/gather/gather/mqm/sales/"+PubFun.getCurrentDate()+"/";//FTP备份目录
				// if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
				// System.out.println("^&&&&&&&&新建目录已存在&&&&&&&@@@@@@@@@");
				// }
				// System.out.println("OK");

				// String[] tPath =
				// tFTPTool.downloadAllDirectory(tFileImportPath,
				// tFileImportPathBackUp);
				// System.out.println(tPath.length);
				// tFTPTool.download("/gather/gather/mqm/sales/rec/mainData/",
				// CrmPath);
				// System.out.println("文件下载成功了！");

				// tFTPTool.download("/gather/gather/mqm/sales/rec/mainData/",
				// CrmPath);
				// System.out.println("文件下载成功了！");

			}
		} catch (SocketException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			tFTPTool.logoutFTP();
		}
		return true;
	}

	/**
	 * 获得指定日期的前几天 -0代表获取当天的日期
	 * 
	 * @param specifiedDay
	 * @return
	 * @throws Exception
	 */
	public static String getSpecifiedDayBefore(String specifiedDay) {
		// SimpleDateFormat simpleDateFormat = new
		// SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		Date date = null;

		try {
			date = new SimpleDateFormat("yy-MM-dd").parse(specifiedDay);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		c.setTime(date);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day - 0);

		String dayBefore = new SimpleDateFormat("yyyy-MM-dd").format(c
				.getTime());
		return dayBefore;
	}

	public static void main(String[] args) {
		LAChargeJsonReadFromFTPTask tt = new LAChargeJsonReadFromFTPTask();
		tt.run();
	}

	// double类型的乘法
	public static double mulrate(double v1, double v2) {

		BigDecimal b1 = new BigDecimal(Double.toString(v1));

		BigDecimal b2 = new BigDecimal(Double.toString(v2));

		return b1.multiply(b2).doubleValue();

	}
}
