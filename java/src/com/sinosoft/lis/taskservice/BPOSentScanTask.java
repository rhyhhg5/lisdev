package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.easyscan.BPOSentScanBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 发送扫描件到外包前置机
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class BPOSentScanTask extends TaskThread
{
    public CErrors mErrors = new CErrors();

    public BPOSentScanTask()
    {
    }

    public void run()
    {
        GlobalInput mG = new GlobalInput();
        mG.Operator = "001";
        mG.ManageCom = "86";

        //发送扫描件TB01
        TransferData tf = new TransferData();
        tf.setNameAndValue("SubType", "TB01");

        VData mVData = new VData();
        mVData.add(mG);
        mVData.add(tf);

        BPOSentScanBL mBPOSentScanBLTB01 = new BPOSentScanBL();
        try
        {
            if(!mBPOSentScanBLTB01.submitData(mVData, ""))
            {
                mErrors.addOneError("TB01处理失败:");
                mErrors.copyAllErrors(mBPOSentScanBLTB01.mErrors);
                System.out.println(mBPOSentScanBLTB01.mErrors.getErrContent());
            }
        }
        catch(Exception ex)
        {
            System.out.println("发送前置机失败");
            ex.printStackTrace();
        }
        //发送扫描件TB29
        tf = new TransferData();
        tf.setNameAndValue("SubType", "TB29");

        mVData.clear();
        mVData.add(mG);
        mVData.add(tf);

        BPOSentScanBL mBPOSentScanBLTB29 = new BPOSentScanBL();
        try
        {
            if(!mBPOSentScanBLTB29.submitData(mVData, ""))
            {
                mErrors.addOneError("TB29处理失败:");
                mErrors.copyAllErrors(mBPOSentScanBLTB29.mErrors);
                System.out.println(mBPOSentScanBLTB29.mErrors.getErrContent());
            }
        }
        catch(Exception ex)
        {
            System.out.println("发送前置机失败");
            ex.printStackTrace();
        }

        //发送扫描件TB05
        tf = new TransferData();
        tf.setNameAndValue("SubType", "TB05");

        mVData.clear();
        mVData.add(mG);
        mVData.add(tf);

        BPOSentScanBL mBPOSentScanBLTB05 = new BPOSentScanBL();
        try
        {
            if(!mBPOSentScanBLTB05.submitData(mVData, ""))
            {
                mErrors.addOneError("TB05处理失败:");
                mErrors.copyAllErrors(mBPOSentScanBLTB05.mErrors);
                System.out.println(mBPOSentScanBLTB05.mErrors.getErrContent());
            }
        }
        catch(Exception ex)
        {
            System.out.println("发送前置机失败");
            ex.printStackTrace();
        }
       
    }

    public static void main(String[] args)
    {
        BPOSentScanTask mBPOSentScanTask = new BPOSentScanTask();
        mBPOSentScanTask.run();
    }
}
