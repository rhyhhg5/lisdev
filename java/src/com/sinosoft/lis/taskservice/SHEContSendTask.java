package com.sinosoft.lis.taskservice;

import java.io.File;

import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class SHEContSendTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 设置附件路径 */
	private String path = "";
	private String suffix = ".pdf";

	// 设置生成日志需要的字段
	/** 发送状态 */
	public String SendFlag;

	/** 入机日期 */
	private String MakeDate;

	/** 设置操作员 */
	private String Operator = "001";

	/** 设置流水号 */
	private String SerialNo;

	public String getSendFlag() {
		return SendFlag;
	}

	public void setSendFlag(String sendFlag) {
		SendFlag = sendFlag;
	}

	public String getRemark() {
		return Remark;
	}

	public void setRemark(String remark) {
		Remark = remark;
	}

	/** 入机时间 */
	private String MakeTime;

	/** 最后修改日期 */
	private String ModifyDate;

	/** 最后修改时间 */
	private String ModifyTime;

	/** 备注信息 */
	public String Remark;

	private String mWhereSQL = " and signdate = current date - 1 day ";

	public SHEContSendTask() {
	}

	public SHEContSendTask(String tContNo) {
		mWhereSQL = " and lca.contno = '" + tContNo + "' ";
	}

	public void run() {
		// 邮箱发送(用户,密码)
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
		SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1),
				tSSRS.GetText(1, 2), "picchealth");
		// 设置操作员
		Operator = "001";
		// 设置入机时间
		MakeDate = PubFun.getCurrentDate();
		MakeTime = PubFun.getCurrentTime();

		// 获取客户的合同号和电子邮箱
		String sql = "select distinct lcp.contno,lcad.EMail,lca.appntname,lcp.riskcode,(select riskname from lmrisk where riskcode = lcp.riskcode) "
				+ " from lcappnt lca "
				+ " inner join lcpol lcp on lcp.contno = lca.contno and lcp.renewcount = 0 "
				+ " inner join lcaddress lcad on lca.appntno = lcad.customerno and lca.addressno = lcad.addressno "
				+ " where lcp.riskcode in (select code from ldcode where codetype='SHRiskcode') "
				+ " and lcp.conttype = '1' and lcp.appflag = '1' AND lcp.operator = 'SH_WX'"
				+ mWhereSQL
				+ "with ur";
		SSRS tSSRSR = new ExeSQL().execSQL(sql);

		// 设置最后修改时间
		ModifyDate = PubFun.getCurrentDate();
		ModifyTime = PubFun.getCurrentTime();
		String getPatySQL = "select codename from ldcode where codetype = 'shpdfpath'";
		path = new ExeSQL().getOneValue(getPatySQL);
//					path = "D:\\";
		/**
		 * 未考虑查询不到保单的情况。
		 */
		if (tSSRSR.MaxRow != 0) {
			for (int i = 1; i <= tSSRSR.MaxRow; i++) {
				if (tSSRSR.GetText(i, 2) == null
						|| "".equals(tSSRSR.GetText(i, 2))) {
					String tContNo = tSSRSR.GetText(i, 1);
					System.out.println("合同号为: " + tContNo);

					// 标记发送状态
					SendFlag = "2";
					Remark = "该客户没有预留电子邮箱!";
					// 设置流水号
					System.out.println("该客户没有预留电子邮箱!");
					System.out.println("备注信息为: " + Remark);
				} else {
					String tContNo = tSSRSR.GetText(i, 1);
					String tEmail = tSSRSR.GetText(i, 2);
					System.out.println("合同号为: " + tContNo);
					System.out.println("邮箱为: " + tEmail);

					// 标记发送状态
					SendFlag = "1";
					Remark = "邮件已发送成功!";

					String tFilePath = path + tContNo + suffix;
					System.out.println("===电子保单路径===" + tFilePath);
					File file = new File(tFilePath);
					System.out.println(file.getAbsolutePath());
					if (!file.exists()) {
						Remark = "电子保单信息尚未生成，请确认是否已打印，或稍后再试!";
					} else {
						tMailSender.setSendInf("中国人民健康保险股份有限公司电子保单", "尊敬的"
								+ tSSRSR.GetText(i, 3) + "您好：\r\n附件是您购买"
								+ tSSRSR.GetText(i, 5) + "产品的电子保单，请您查收。",
								tFilePath);
						tMailSender.setToAddress(tEmail, "", null);

						// 发送邮件
						if (!tMailSender.sendMail()) {
							// 修改标记发送状态
							SendFlag = "2";
							Remark = tMailSender.getErrorMessage();
							System.out.println(tMailSender.getErrorMessage());
						}
						file.delete();
					}

					System.out.println("发送状态为: " + SendFlag);
					System.out.println("备注信息为: " + Remark);
				}
			}
		} else {
			System.out.println("未查询到符合条件的保单！");
		}
	}

	public static void main(String[] args) {
		new SHEContSendTask().run();
	}
}