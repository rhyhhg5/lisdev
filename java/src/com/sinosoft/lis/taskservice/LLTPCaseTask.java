package com.sinosoft.lis.taskservice;

 
import com.sinosoft.lis.yibaotong.LLTPTransFtpXmlCase;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class LLTPCaseTask  extends TaskThread {

	/**
	 * 错误的容器
	 */
	public CErrors mErros = new CErrors();
	
	private String opr="true";
	
	public LLTPCaseTask(){
		
	}
	
	public void run(){

		ExeSQL tExeSQL = new ExeSQL();
		
		String outSource="select code,codealias from ldcode where codetype  ='thridcompany'  order by othersign ";
    	SSRS outSources=tExeSQL.execSQL(outSource);
		if(outSources.MaxRow<=0){
			return ;
		}
		
		
			LLTPTransFtpXmlCase tLLTPTransFtpXmlCase = new LLTPTransFtpXmlCase();
			for (int i = 1; i <= outSources.MaxRow; i++) {	
				String code = outSources.GetText(i, 1);
				String codealias = outSources.GetText(i, 2);
				if(!tLLTPTransFtpXmlCase.submitData(code==null ? "":code,codealias==null ? "" : codealias)) {
					System.out.println(code+"批次多事件，理赔案件信息出现问题");
					System.out.println(mErros.getFirstError());
					opr = "false";
				}else {
					System.out.println(code+"批次多事件，理赔案件信息正常===");
				}
			}
	}
	
	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	
	public static void main(String[] args) {
		LLTPCaseTask tLLTPCaseTask = new LLTPCaseTask();
		tLLTPCaseTask.run();
	}

}
