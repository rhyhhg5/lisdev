package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.agentassess.LaHumandeveIBL;
import com.sinosoft.lis.agentcalculate.*;

/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: SinoSoft</p>
 * @author xiangchun
 * @version 1.0
 */

public class LAHumandeveiTask extends TaskThread {
    private int strSql;
    private static int intMaxIndex;
    private SSRS mSSRS1 = new SSRS();
    public LAHumandeveiTask() {
        intMaxIndex = 1;
    }


    public void run() {
        System.out.println("");
        System.out.println(
                "＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
        System.out.println("参数个数:" + mParameters.size());
        System.out.println("");
        // Write your code here

        LAHumandeveiTask a = new LAHumandeveiTask();
        SSRS tSSRS=new SSRS();
        tSSRS = a.EasyQuery();
        if(!a.getSaleBranchType())
        {
            return ;
        }
        int SaleCount = a.mSSRS1.getMaxCol();
        //开始批量签单
        for(int k = 1;k <= SaleCount; k++)
        {
	        for (int i = 1; i <= intMaxIndex; i++)
	        {
	        	String tManageCom = tSSRS.GetText(i, 1) ;
	        	String tWageno = a.mSSRS1.GetText(1, k);
        	  String  sql = "select 1 from lahumandevei where mngcom ='"+tManageCom+"' and remark1='"+tWageno+"' ";
              String result = new ExeSQL().getOneValue(sql);
              if("1".equals(result))
              {
              	continue;
              }
	            //先查询提数的记录集
                if (!a.getDate(tManageCom , tWageno))
                {
                    //如果提数不成功,继续提数下一条;
                    continue;
                }

            }

        }

    }

    public static void main(String[] args) {

        LAHumandeveiTask a = new LAHumandeveiTask();
        a.run();
    }
    private boolean getSaleBranchType()
    {
        String tSql = "select '201501','201502','201503' from dual where 1=1";
        ExeSQL tExeSQL = new ExeSQL();
        mSSRS1 = tExeSQL.execSQL(tSql);
        if(mSSRS1 == null || mSSRS1.getMaxRow() <= 0 )
        {
            return false;
        }
        return true;
    }
    //
    public boolean getDate(String tManageCom,String tWageNo) {
        LaHumandeveIBL tLaHumandeveIBL = new LaHumandeveIBL();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator="001";
        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tManageCom);
        tVData.add(tWageNo);
        tLaHumandeveIBL.submitData(tVData, "INSERT||MAIN");
        return true;
    }


    public  SSRS EasyQuery() {

        SSRS tSSRS = new SSRS();

        String sql =
                "select  code " +
                "from ldcode where codetype ='humandeveidesc' with ur " ;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        intMaxIndex = tSSRS.getMaxRow();
//        String[] getRowData = tSSRS.getRowData(intStartIndex);
        return tSSRS;
    }
}

