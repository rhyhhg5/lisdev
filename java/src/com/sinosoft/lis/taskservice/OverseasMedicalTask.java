package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.schema.LDUserSchema;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.vschema.LDUserSet;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;

public class OverseasMedicalTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */

	//执行任务
	public void run() {
		//判断是否可以执行
		getUser();
	}

	//提取用户
	public boolean getUser() {
		
		Calendar now = Calendar.getInstance();  
		int date = now.get(Calendar.DAY_OF_MONTH); 
		if(date!=3){
			System.out.println("今天不是批处理时间");
			return false;
		}
		
		ExeSQL tEx = new ExeSQL();

		//自动失效
		String tDisDetailAutoSql = "select distinct lcg.signdate, "
				+ "'新契约',"
				+ "'新契约',"
				+ "lcg.prtno,"
				+ "left(lcg.managecom, 4),"
				+ " (select name from ldcom where comcode = left(lcg.managecom, 4)),"
				+ "db2inst1.codename('salechnl', lcg.salechnl),"
				+ "lcg.grpcontno,"
				+ "lcg.signdate,"
				+ "lcg.cvalidate,"
				+ "lcg.cinvalidate,"
				+ "lcg.grpname,"
				+ "'承保有效',"
				+ "lcp.cvalidate,"
				+ "(select lcc.cinvalidate from lccont lcc where grpcontno=lcg.grpcontno and contno=lcp.contno and conttype='2' and appflag='1'),"
				+ "lcp.riskcode,"
				+ "(select riskname from lmriskapp where riskcode = lcp.riskcode),"
				+ "(select distinct db2inst1.codename('plan', calfactorvalue) from lccontplandutyparam "
				+ " where grpcontno = lcg.grpcontno and calfactor = 'Mult' fetch first 1 rows only),"
				+ "lcp.amnt,"
				+ "lcp.prem,"
				+ "(select name from lcinsured where grpcontno = lcg.grpcontno and contno = lcp.contno and RelationToMainInsured = '00'),"
				+ "lci.Name,"
				+ "db2inst1.codename('relation', lci.RelationToMainInsured),"
				+ "db2inst1.codename('idtype', lci.IDType),"
				+ "lci.IDNo,"
				+ "db2inst1.codename('sex', lci.sex),"
				+ "lci.Birthday,"
				+ "lcg.Remark,"
				+ "(select distinct db2inst1.codename('agerange',calfactorvalue) from lccontplandutyparam "
				+ "where grpcontno = lcg.grpcontno and calfactor = 'agerange' and contplancode = lcp.contplancode fetch first 1 rows only) "
				+ "from lcgrpcont lcg, lcpol lcp, lcinsured lci "
				+ "where 1=1 "
				+ "and lci.contplancode = lcp.contplancode "
				+ "and lcg.grpcontno = lcp.grpcontno "
				+ "and lci.grpcontno = lcg.grpcontno "
				+ "and lci.contno = lcp.contno "
				+ "and lcp.conttype = '2' "
				+ "and lcp.appflag = '1' "
				+ "and lcg.appflag = '1' "
				+ "and lcg.stateflag = '1' "
				+ "and lcp.riskcode in ('162601', '162701', '162801', '162901') "
				+ "and lcg.signdate<=current date with ur ";


		SSRS tDisAutoLDUsers = tEx.execSQL(tDisDetailAutoSql);
		int tDisUserAtuoCount = tDisAutoLDUsers.getMaxRow();// 行数

		
/*		Calendar cal = Calendar.getInstance();//可以对每个时间域单独修改
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd"); 
		cal.set(Calendar.DAY_OF_MONTH,0);//设置为0号,当前日期既为上月最后一天
        String tlastDay = fmt.format(cal.getTime());*/
		
	    String mCurDate = PubFun.getCurrentDate();
	    String mCurTime = PubFun.getCurrentTime();
		System.out.println(mCurDate);
		System.out.println(mCurTime);
		
		
		//创建对象
		String[][] tToExcel = new String[tDisUserAtuoCount + 10][29];
		System.out.println("统计时间" + mCurDate);

		tToExcel[0][0] = "契约承保操作完成日期";
		tToExcel[0][1] = "操作类型";
		tToExcel[0][2] = "操作项目";
		tToExcel[0][3] = "操作作业号";
		tToExcel[0][4] = "承保机构";
		tToExcel[0][5] = "承保机构名称";
		tToExcel[0][6] = "销售渠道";
		tToExcel[0][7] = "保单号";
		tToExcel[0][8] = "总单承保日期";
		tToExcel[0][9] = "总单生效日期";
		tToExcel[0][10] = "总单终止日期";
		tToExcel[0][11] = "投保单位名称";
		tToExcel[0][12] = "分单状态";
		tToExcel[0][13] = "分单生效日期";
		tToExcel[0][14] = "分单终止日期";
		tToExcel[0][15] = "产品代码";
		tToExcel[0][16] = "产品名称";
		tToExcel[0][17] = "保障计划";
		tToExcel[0][18] = "保额";
		tToExcel[0][19] = "保费";
		tToExcel[0][20] = "主被保险人姓名";
		tToExcel[0][21] = "被保险人姓名";
		tToExcel[0][22] = "与主被保险人关系";
		tToExcel[0][23] = "证件类型";
		tToExcel[0][24] = "证件号码";
		tToExcel[0][25] = "性别";
		tToExcel[0][26] = "生日";
		tToExcel[0][27] = "总单特别约定";
		tToExcel[0][28] = "年龄组";
		

		int excelRow = 0;

		for (int row = 1; row <= tDisUserAtuoCount; row++) {
			excelRow++;
			tToExcel[excelRow][0] = tDisAutoLDUsers.GetText(row, 1);
			tToExcel[excelRow][1] = tDisAutoLDUsers.GetText(row, 2);
  			tToExcel[excelRow][2] = tDisAutoLDUsers.GetText(row, 3);
  			tToExcel[excelRow][3] = tDisAutoLDUsers.GetText(row, 4);
  			tToExcel[excelRow][4] = tDisAutoLDUsers.GetText(row, 5);
  			tToExcel[excelRow][5] = tDisAutoLDUsers.GetText(row, 6);
  			tToExcel[excelRow][6] = tDisAutoLDUsers.GetText(row, 7);
  			tToExcel[excelRow][7] = tDisAutoLDUsers.GetText(row, 8);
  			tToExcel[excelRow][8] = tDisAutoLDUsers.GetText(row, 9);
  			tToExcel[excelRow][9] = tDisAutoLDUsers.GetText(row, 10);
  			tToExcel[excelRow][10] = tDisAutoLDUsers.GetText(row, 11);
  			tToExcel[excelRow][11] = tDisAutoLDUsers.GetText(row, 12);
  			tToExcel[excelRow][12] = tDisAutoLDUsers.GetText(row, 13);
  			tToExcel[excelRow][13] = tDisAutoLDUsers.GetText(row, 14);
  			tToExcel[excelRow][14] = tDisAutoLDUsers.GetText(row, 15);
  			tToExcel[excelRow][15] = tDisAutoLDUsers.GetText(row, 16);
  			tToExcel[excelRow][16] = tDisAutoLDUsers.GetText(row, 17);
  			tToExcel[excelRow][17] = tDisAutoLDUsers.GetText(row, 18);
  			tToExcel[excelRow][18] = tDisAutoLDUsers.GetText(row, 19);
  			tToExcel[excelRow][19] = tDisAutoLDUsers.GetText(row, 20);
  			tToExcel[excelRow][20] = tDisAutoLDUsers.GetText(row, 21);
  			tToExcel[excelRow][21] = tDisAutoLDUsers.GetText(row, 22);
  			tToExcel[excelRow][22] = tDisAutoLDUsers.GetText(row, 23);
  			tToExcel[excelRow][23] = tDisAutoLDUsers.GetText(row, 24);
  			tToExcel[excelRow][24] = tDisAutoLDUsers.GetText(row, 25);
  			tToExcel[excelRow][25] = tDisAutoLDUsers.GetText(row, 26);
  			tToExcel[excelRow][26] = tDisAutoLDUsers.GetText(row, 27);
  			tToExcel[excelRow][27] = tDisAutoLDUsers.GetText(row, 28);
  			tToExcel[excelRow][28] = tDisAutoLDUsers.GetText(row, 29);
  		}

		
		try {
			String mOutXmlPath = CommonBL.getUIRoot();
			String tPath = "vtsfile/";
			//本地测试
			//mOutXmlPath = "E:/taskperhaps/";
			System.out.println(mOutXmlPath);
			String name = mCurDate + "-"+ "海外医疗产品团险承保数据.xls";
			String tTime = mCurDate +"提数结果";
			
			System.out.println("字符变更前名称："+name);
			name = new String(name.getBytes("GBK"),"iso-8859-1");
			//tTime = new String(tTime.getBytes("GBK"),"iso-8859-1");
			String tAllName = mOutXmlPath + tPath + name;
			System.out.println("文件名称====："+tAllName);
			
			WriteToExcel t = new WriteToExcel(name);
			t.createExcelFile();
			t.setAlignment("CENTER");
			t.setFontName("宋体");
			String[] sheetName = { new String("Data") };
			t.addSheet(sheetName);

			t.setData(0, tToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
			
			//获取文件设置格式
			FileInputStream is = new FileInputStream(new File(mOutXmlPath + name)); //获取文件      
			HSSFWorkbook wb = new HSSFWorkbook(is);
			HSSFSheet sheet = wb.getSheetAt(0); //获取sheet
			

			//设置字体-内容
			HSSFFont tFont = wb.createFont();
			tFont.setFontName("宋体");
			tFont.setFontHeightInPoints((short) 10);//设置字体大小
			
		
			//正文样式
			HSSFCellStyle tContentStyle = wb.createCellStyle(); //获取样式
			tContentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			tContentStyle.setFont(tFont);
			tContentStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

			
			//设置行高 列宽
			for (int i = 0; i < tDisUserAtuoCount + 2; i++) {
				HSSFRow row = sheet.getRow(i);
				for (int j = 0; j < 29; j++) {
					HSSFCell firstCell = row.getCell((short) j);
					sheet.setColumnWidth((short) j, (short) 6000);
//					sheet.setColumnWidth((short) j, (short) (toString().length() * 512));//自动调整列宽
//					sheet.setColumnWidth(i,value.toString().length() * 512);
					firstCell.setCellStyle(tContentStyle);
				}

			}


			
			
			FileOutputStream fileOut = new FileOutputStream(tAllName);
			wb.write(fileOut);
			fileOut.close();
			// 邮箱发送方
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS.GetText(1, 2),"picchealth");
			//标题，内容，附件(多附件可以用|分隔，末尾|可加可不加)
			
			tMailSender.setSendInf("海外医疗产品团险承保数据","您好：\r\n附件是"+tTime,tAllName);
			//收件方
			String tSQLRMailSql = "select codealias,codename from ldcode where codetype = 'hwylemail' ";
//			String tSQLRMailSql = "select codealias,codename from ldcode where codetype = 'yangjian' ";
			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1), tSSRSR.GetText(1, 2), null);
			//发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}

		} catch (Exception ex) {
			this.mErrors.addOneError(new CError("每月用户日志审核时出错",
					"UsersreviewTask", "run"));
			ex.toString();
			ex.printStackTrace();
			return false;
		}

		return true;

	}

	// 主方法 测试用
	public static void main(String[] args) {
		new OverseasMedicalTask().run();

	}
	

}
