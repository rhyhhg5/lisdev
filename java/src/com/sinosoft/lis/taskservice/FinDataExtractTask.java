package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.db.FIDataExtractDefDB;
import com.sinosoft.lis.fininterface_v3.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import java.util.*;
/**
 * <p>Title:新财务接口V3 提数、归档、生成凭证、凭证汇总、凭证导出、旧接口数据同步 批处理服务类</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class FinDataExtractTask extends TaskThread
{
	public CErrors mErrors = new CErrors();
	private GlobalInput globalInput = new GlobalInput();
	private TransferData mGetCessData = new TransferData();
	private VData mOutputData = new VData();
	private TransferData mGetCessDataJT = new TransferData();
	private VData mOutputDataJT = new VData();
	
	//管理机构
	private String tMangecom = "";
	//处理批次
	private String tBatchno = "";
	//提数日期
	private Date tDate = null;
	//规则类型
	private String[] tRuleDefId = null;
	
	private FIDataExtractDefSchema tFIDataExtractDefSchema ;
	private FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefSet();
	
	public FinDataExtractTask()
	{}
        
	public boolean SaveData(VData cInputData){

		run();
		return true;
        }
	public void run()
    {       
		dealData();
//		dealDataJT();
    }
	
	/*
	 * 长险应收计提数据处理类
	 */
	public boolean dealDataJT(){
		if(!isFirstDay()){
			System.out.println();
			return false;
		}
		System.out.println("开始执行财务接口--长险应收计提--批处理:"+PubFun.getCurrentTime());
		//准备数据
		getInputDataJT();
		
		//数据提取和归档
		dataExtractJT();
		
		//生成凭证
		VoucherDataTrans();
		
		//凭证汇总
		VoucherDataGather();
		
		//凭证导出
		MoveDataToInterfacetable();
		
		//旧接口数据同步
		MoveDataToOldTable();
		
		System.out.println("财务接口--长险应收计提--批处理执行完毕:"+PubFun.getCurrentTime());
		
		return true;
	}
	
	/*
	 * 判断当天是否是当月第一天
	 */
	public boolean isFirstDay(){
		String currentdate[] = calFLDate(PubFun.getCurrentDate());
		if(currentdate[0].endsWith(PubFun.getCurrentDate())){
			return true;
		}
		return false;
	}
	
	/*
	 * 长险应收计提数据准备
	 * 除每月1号外，其他时间不进行数据抽取
	 * 抽取日期为：每个月1号
	 */
	public void getInputDataJT()
	{

		globalInput.Operator = "cwjk";
		
		//设置提数日期
		FDate chgdate = new FDate();
		tDate = chgdate.getDate(PubFun.getCurrentDate());
			
		System.out.println("提数日期为："+tDate+",：长线应收计提业务 ；");
		
		mGetCessDataJT.setNameAndValue("StartDate",chgdate.getString(tDate));
		mGetCessDataJT.setNameAndValue("EndDate",chgdate.getString(tDate));
		mGetCessDataJT.setNameAndValue("CallPointID","00"); //校检节点

		
		mOutputDataJT.add(globalInput);
		mOutputDataJT.add(mGetCessDataJT);
		
	}
	
	/*
	 * 每月1号，长险应收计提数据抽取
	 */
	public void dataExtractJT()
	{
		FIDataExtPlanExeJTUI tFIDataExtPlanExeJTUI = new FIDataExtPlanExeJTUI();
		try
		{
			if(!tFIDataExtPlanExeJTUI.submitData(mOutputDataJT,"01"))
			{
				System.out.println("操作失败，原因是:" + tFIDataExtPlanExeJTUI.mErrors.getFirstError());
			}
		}
		catch(Exception ex)
		{
			System.out.println("失败，原因是:" + ex.toString());
		}
		System.out.println("数据提取成功！");
	}
	
	public boolean dealData()
	{
		System.out.println("开始执行财务接口批处理:"+PubFun.getCurrentTime());
		//先准备数据
		getInputData();
		
		//数据提取和归档
		dataExtract();
		
		//生成凭证
		VoucherDataTrans();
		
		//凭证汇总
		VoucherDataGather();
		
		//凭证导出
		MoveDataToInterfacetable();
		
		//旧接口数据同步
		MoveDataToOldTable();
		
		System.out.println("财务接口批处理执行完毕:"+PubFun.getCurrentTime());
		
		return true;
	}
	
	public void getInputData()
	{
		//设置管理机构
		tMangecom = "86";
		
		globalInput.ManageCom = tMangecom;
		globalInput.Operator = "cwjk";
		globalInput.ComCode = "86";
		
		//设置提数日期
		FDate chgdate = new FDate();
		tDate = chgdate.getDate(PubFun.getCurrentDate());
		tDate = PubFun.calDate(tDate, -1, "D", null);
		
		//设置数据抽取规则
		FIDataExtractDefDB tFIDataExtractDefDB = new FIDataExtractDefDB();
		tFIDataExtractDefSet = tFIDataExtractDefDB.executeQuery(" select * from FIDataExtractDef a where ruletype = '00' and indexcode <> '00' and rulestate = '1' and ruledefid not in('0000000011','0000000010') order by ruledefid with ur ");
		
		System.out.println("提数机构为："+tMangecom+",提数日期为："+tDate);
		
		mGetCessData.setNameAndValue("StartDate",chgdate.getString(tDate));
		mGetCessData.setNameAndValue("EndDate",chgdate.getString(tDate));
		mGetCessData.setNameAndValue("ManageCom",tMangecom);
		mGetCessData.setNameAndValue("CallPointID","00"); //校检节点
		mGetCessData.setNameAndValue("FIExtType","00");
		
		mOutputData.add(globalInput);
		mOutputData.add(mGetCessData);
		mOutputData.add(tFIDataExtractDefSet);
		
	}
	
	public void dataExtract()
	{
		FIDataExtPlanExeUI tFIDataExtPlanExeUI = new FIDataExtPlanExeUI();		
		try
		{
			if(!tFIDataExtPlanExeUI.submitData(mOutputData,"00"))
			{
				System.out.println("操作失败，原因是:" + tFIDataExtPlanExeUI.mErrors.getFirstError());
			}
		}
		catch(Exception ex)
		{
			System.out.println("失败，原因是:" + ex.toString());
		}
		System.out.println("数据提取成功！");
	}
	
	public void VoucherDataTrans()
	{
		VData tVData  = new VData();
		String DataType = "1";
		
		tVData.add(globalInput);
		tVData.add(DataType);
		
		FIDistillCertificateUI tFIDistillCertificateUI = new FIDistillCertificateUI();
		try
		{
			if(!tFIDistillCertificateUI.submitData(tVData,""))
			{
				 System.out.println("凭证转换出错！");
			}
//			tBatchno = (String)tFIDistillCertificateUI.getResult().get(0);
			tBatchno = tFIDistillCertificateUI.getBatchno();
			System.out.println("FinDataExtractTask类tBatchno--"+tBatchno);
		}
		catch(Exception ex)
		{
			System.out.println("执行异常，原因是:" + ex.toString());
		}
		
		
	}
	
	/**
	 * 凭证汇总，由FiVoucherDataDetail表汇总至FIVoucherDataGather表
	 *
	 */
	public void VoucherDataGather(){
		VData tVData = new VData();
		
	    TransferData tTransferData= new TransferData();
	    tTransferData.setNameAndValue("BatchNo",tBatchno);
		
		tVData.add(globalInput);
		tVData.add(tTransferData);
		
		FIVoucherDataGatherUI tFIVoucherDataGatherUI = new FIVoucherDataGatherUI();
		
		try{
			
			if(!tFIVoucherDataGatherUI.submitData(tVData, "")){
				System.out.println("凭证汇总出错！");
			}
			
		}catch (Exception ex) {
			System.out.println("执行异常，原因是:" + ex.toString());
		}
		System.out.println("凭证汇总成功");
	}
	
	/**
	 * 凭证导出，由FIVoucherDataGather表导出至Interfacetable表
	 *
	 */
	public void MoveDataToInterfacetable(){
		VData tVData = new VData();
		
		tVData.add(globalInput);
		tVData.add(tBatchno);
		
		FIMoveToInterfacetableUI tFIMoveToInterfacetableUI = new FIMoveToInterfacetableUI();
		
		try{
			
			if(!tFIMoveToInterfacetableUI.submitData(tVData, "")){
				System.out.println("凭证导出出错！");
			}
			
		}catch (Exception ex) {
			System.out.println("执行异常，原因是:" + ex.toString());
		}
		System.out.println("凭证导出成功");
	}
	
	/**
	 * 向旧接口中同步数据，由FIVoucherDataDetail至LIDataTransResult，由FIAbstandardData至LIAboriginalData
	 *
	 */
	public void MoveDataToOldTable(){
		VData tVData = new VData();
		
		tVData.add(globalInput);
		tVData.add(tBatchno);
		
		FIMoveToOldTableUI tFIMoveToOldTable = new FIMoveToOldTableUI();
		
		try{
			
			if(!tFIMoveToOldTable.submitData(tVData, "")){
				System.out.println("旧接口数据同步失败");
			}
			
		}catch (Exception ex) {
			System.out.println("执行异常，原因是："+ex.toString());
		}
		System.out.println("旧接口数据同步成功");
	}
	
	/**
	 * 根据字符型日期，格式如"2014-02-22",返回该日期所在月份的月初与月末日期
	 * @param tDate
	 * @return
	 */
	public String[] calFLDate(String tDate)
    {
        String MonDate[] = new String[2];
        FDate fDate = new FDate();
        Date CurDate = fDate.getDate(tDate);
        GregorianCalendar mCalendar = new GregorianCalendar();
        mCalendar.setTime(CurDate);
        int Years = mCalendar.get(Calendar.YEAR);
        int Months = mCalendar.get(Calendar.MONTH);
        int FirstDay = mCalendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        int LastDay = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        mCalendar.set(Years, Months, FirstDay);
        MonDate[0] = fDate.getString(mCalendar.getTime());
        mCalendar.set(Years, Months, LastDay);
        MonDate[1] = fDate.getString(mCalendar.getTime());
        return MonDate;
    }
	
	public static void main(String args[])
	{
		FinDataExtractTask tFinDataExtract_V3Task = new FinDataExtractTask();
		tFinDataExtract_V3Task.run();
	}

}
