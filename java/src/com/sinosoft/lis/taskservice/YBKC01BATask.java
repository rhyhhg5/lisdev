package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclientybk.inf.C01;
import com.sinosoft.lis.db.YBK_C01_LIS_ResponseInfoDB;
import com.sinosoft.lis.db.YbkReportDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.YbkReportSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *   	医保卡理赔报案上报批处理
 * </p>

 * @version 1.1
 */
public class YBKC01BATask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    String opr = "";
    public YBKC01BATask()
    {}

    public void run()
    {
    	C01 c01 = new C01();
//    	String SQL = "select y.ConsultNo from YbkReport y"
//				    +" where 1=1 "
//				    //报案未上报的
//					+" and not exists(select 1 from YBK_C01_LIS_ResponseInfo where caseno=y.ConsultNo and responsecode='1' and  prtno is null) "
//				    +" with ur";
    	
    	String SQL = " select y.ConsultNo,'WXXS','' "
    			+" from YbkReport y "
    			+" where 1=1 "
    			+" and not exists(select 1 from YBK_C01_LIS_ResponseInfo where caseno=y.ConsultNo and responsecode='1' and  prtno is null) "
    			+" union "
    			+" select distinct ll.caseno,'HXXX',ll.contno "
    			+" from llclaimdetail ll "
    			+" where 1=1 "
    			+" and ll.riskcode in (select code from ldcode where codetype='ybkriskcode') "
    			+" and exists(select 1 from llcase where caseno = ll.caseno and rgtstate in ('01','02','03','04','05','06','11','12')) "
    			+" and not exists(select 1 from ybk_c01_lis_responseinfo where caseno in (select consultno from llcaseext where caseno = ll.caseno) and prtno = '01')" 
    			//本地测试
//    			+" and ll.caseno = 'C3100180730000002' "
    			+" with ur ";
    	
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String flag = tSSRS.GetText(index, 2);
    			if("WXXS".equals(flag)){
	    			String tCaseNo = tSSRS.GetText(index, 1);
	    			VData tVData = new VData();
	    			TransferData tTransferData = new TransferData();
	    			tTransferData.setNameAndValue("PrtNo", tCaseNo);
	    			tVData.add(tTransferData);
	    	    	if(!c01.submitData(tVData, "YBKPT"))
	    	         {
	    	    		 System.out.println("理赔报案信息上传批处理出问题了");
	    	             System.out.println(mErrors.getErrContent());
	    	             opr ="false";
	    	             continue;
	    	         }else{
	    	        	 System.out.println("理赔报案信息上传批处理成功了");
	    	        	 opr ="ture";
	    	         }
    			}else if("HXXX".equals(flag)){
    				YbkReportSchema mYbkReportSchema = new YbkReportSchema();//报案表信息存储
    				String mYesterDay = PubFun.calDate(PubFun.getCurrentDate(), -1, "D", "");
    				String tLimit = PubFun.getNoLimit("86310000");
    				String CNo = PubFun1.CreateMaxNo("NOTICENO", tLimit);
    				String evNo = PubFun1.CreateMaxNo("SERIALNO", "");
    				String contno = tSSRS.GetText(index, 3);
    				String sql = "select distinct "
    						+" lc.insuredname, "
    						+" lca.postaladdress, "
    						+" lca.mobile, "
    						+" llf.HospitalName, "
    						+" ybk.policysequenceno, "
    						+" ll.caseno "
    						+" from lcpol lc,lcaddress lca,ybk_n01_lis_responseinfo ybk,llcasepolicy ll,LLFeeMain llf "
    						+" where lc.prtno = ybk.prtno "
    						+" and ll.contno = lc.contno "
    						+" and ll.caseno = llf.caseno "
    						+" and lc.insuredno = lca.customerno "
    						+" and lc.riskcode in (select code from ldcode where codetype = 'ybkriskcode') "
    						+" and lc.contno = '"+contno+"' "
    						+" with ur ";
    				SSRS tSSRS1=new SSRS();
    		    	ExeSQL tExeSQL1=new ExeSQL();
    		    	tSSRS1 = tExeSQL1.execSQL(sql);
    		        mYbkReportSchema.setConsultNo(CNo);  //咨询通知号，用于关联
    		        mYbkReportSchema.setLogNo(evNo);  //登记号
    		        mYbkReportSchema.setPolicySequenceNo(tSSRS1.GetText(1, 5));  	//保单编码
    		        mYbkReportSchema.setClaimFormerNo(""); 			//理赔流水号
    		        mYbkReportSchema.setAppointName(""); 				//委托人
    		        mYbkReportSchema.setReportName(tSSRS1.GetText(1, 1));   				//报案人
    		        mYbkReportSchema.setRelationShip("00"); 				//报案人与出险人关系
    		        mYbkReportSchema.setReportPhone(tSSRS1.GetText(1, 3)); 				//联系电话
    		        mYbkReportSchema.setReportAddr(tSSRS1.GetText(1, 2)); 					//联系地址
    		        mYbkReportSchema.setHospital(tSSRS1.GetText(1, 4)); 						//治疗医院
    		        mYbkReportSchema.setAdmissionDiagnosis("线下报案"); 	//入院诊断
    		        mYbkReportSchema.setAccidentDate(mYesterDay); 				//出险日期
    		        mYbkReportSchema.setAccidentDes("线下理赔");				//事故说明
    		        mYbkReportSchema.setMakeDate(mYesterDay);
    		        mYbkReportSchema.setMakeTime(PubFun.getCurrentTime());
    		        mYbkReportSchema.setModifyDate(mYesterDay);
    		        mYbkReportSchema.setModifyTime(PubFun.getCurrentTime());
    		        //更新llcaseext表信息
    		        String SQLGX = "update llcaseext set consultno = '"+CNo+"' where caseno = '"+tSSRS1.GetText(1, 6)+"'";
    		        //封装数据
    		        VData tVData = new VData();
    		        MMap tMap = new MMap();
    		        tMap.put(SQLGX, "UPDATE");
    		        tMap.put(mYbkReportSchema, "INSERT");
    		        tVData.add(tMap);
    		        //提交数据库
    		        PubSubmit tPubSubmit = new PubSubmit();
    		        if (!tPubSubmit.submitData(tVData, "")) {
    		        	System.out.println("ybkreport表数据插入错误!" + tPubSubmit.mErrors.getFirstError());
    		        } else {
    		        	System.out.println("ybkreport表插入成功!");
    		        }
    		        tVData.clear();
    		        //调用上报接口
    		        TransferData tTransferData = new TransferData();
    		        tTransferData.setNameAndValue("PrtNo", CNo);
    		        tVData.add(tTransferData);
    		        if(!c01.submitData(tVData, "C01")){
    		        	System.out.println("理赔结案信息上传批处理出问题了");
          	            System.out.println(mErrors.getErrContent());
          	            continue;
    		        }else{
          	        	 System.out.println("理赔结案信息上传批处理成功了");
           	         } 		        
    			}
    		}
    	}

    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
   
    public static void main(String[] args)
    {
    	YBKC01BATask ybkTask = new YBKC01BATask();
    	ybkTask.run();
    }
}




