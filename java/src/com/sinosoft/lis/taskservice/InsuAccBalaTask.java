package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.InsuAccBala;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.vschema.LPEdorItemSet;

/**
 * <p>Title: 个险万能险保单账户结算 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @version 1.0
 * @CreateDate：2007
 */
public class InsuAccBalaTask extends TaskThread
{
    public CErrors mErrors = new CErrors();

    private String mContNo = null;

    private GlobalInput mGI = new GlobalInput();

    public InsuAccBalaTask()
    {
        mGI.Operator = "001";
        mGI.ComCode = "86";
        mGI.ManageCom = mGI.ComCode;
    }

    public InsuAccBalaTask(GlobalInput cGlobalInput)
    {
        mGI = cGlobalInput;
    }

    public void run()
    {
        int iSuccCount = 0;
        int iFailCount = 0;
        int iWaitCount = 0;

        //查询出当前账户最后结算日期比已公布利率日期晚的账户，由于理赔或保全等原因，一个账户可能需要结算多次
        String sql = " select distinct a.* from LCInsureAcc a, LMInsuAccRate b "
                     + "where a.RiskCode = b.RiskCode "
                     + "and a.GrpContNo = '00000000000000000000' "
                     + "   and a.InsuAccNo = b.InsuAccNo "
                     + "   and a.BalaDate < b.BalaDate "
                     + " and  b.BalaDate <=current date "
                     + "   and b.RateType = 'C' "
                     + "   and b.RateIntv = 1 "
                     + "   and b.RateIntvUnit = 'Y' "
                     + "   and a.riskcode in (select RiskCode from LMRiskApp where RiskType4 = '4') "  //万能险
                     + "   and a.ManageCom like '" + mGI.ManageCom + "%' "
//                     + "   and exists(select 1 from LCPol where PolNo = a.PolNo and StateFlag = '1' ) "  //险种有效
                     + " and (a.riskcode in (select riskcode from lmriskapp where  taxoptimal='Y')" 
                     + "       or exists (select 1 from LCPol where PolNo = a.PolNo and StateFlag = '1'  ))"//险种有效+税优失效可以月结 add by hehongwei
                     + (mContNo != null ? (" and a.ContNo = '" + mContNo + "' ") : "")
                     + "order by a.PolNo, a.InsuAccNo with ur";
        System.out.println(sql);

        RSWrapper rsWrapper = new RSWrapper();
        LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
        if (!rsWrapper.prepareData(tLCInsureAccSet, sql))
        {
            System.out.println("万能险保单结算数据准备失败! ");
            return;
        }

        do
        {
            rsWrapper.getData();
            if (tLCInsureAccSet == null || tLCInsureAccSet.size() < 1)
            {
                break;
            }

            iWaitCount += tLCInsureAccSet.size();

            for (int i = 1; i <= tLCInsureAccSet.size(); i++)
            //for (int i = 1; i == 1 && i <= tLCInsureAccSet.size(); i++)
            {
                LCInsureAccSchema tAccSchema = tLCInsureAccSet.get(i);
                if(!canBalance(tAccSchema))
                {
                    iFailCount++;
                    continue;
                }

                VData tVData = new VData();
                tVData.add(mGI);
                tVData.add(tLCInsureAccSet.get(i));

                InsuAccBala tInsuAccBala = new InsuAccBala();

                String errInfo = "保单:" + tAccSchema.getContNo()
                                 + "被保人:" + tAccSchema.getInsuredNo()
                                 + "险种：" + tAccSchema.getRiskCode()
                                 + " 结算失败: ";
                try
                {
                    if(!tInsuAccBala.submitData(tVData, ""))
                    {
                        iFailCount++;
                        errInfo += tInsuAccBala.mErrors.getErrContent();
                        mErrors.addOneError(errInfo);
                        System.out.println(errInfo);
                    }
                    else
                    {
                        iSuccCount++;
                    }
                }
                catch(Exception ex)
                {
                    errInfo += "结算出现未知异常";
                    System.out.println(errInfo);
                    ex.printStackTrace();
                    mErrors.addOneError(errInfo);
                    iFailCount++;
                }
            }
        }
        while (tLCInsureAccSet != null && tLCInsureAccSet.size() > 0);
        rsWrapper.close();
        System.out.println("共计" + iSuccCount + "次账户结算成功，"
                           + iFailCount + "个保单结算失败:"
            + mErrors.getErrContent());

        if(iWaitCount == 0)
        {
            mErrors.addOneError("没有需要处理的保单");
        }
    }

    /**
     * 结算指定保单
     * @param cContNo String
     */
    public void runOneCont(String cContNo)
    {
        mContNo = cContNo;
        run();
    }

    /**
     * 校验能否进行结算
     * 险种有未完结的理赔不能结算，保单有未完结的保全不能结算
     * @param cLCInsureAccSchema LCInsureAccSchema
     * @return boolean
     */
    private boolean canBalance(LCInsureAccSchema cLCInsureAccSchema)
    {
        String tPolInfo = "保单号: " + cLCInsureAccSchema.getContNo()
                          + ", 被保人号码:" + cLCInsureAccSchema.getInsuredNo()
                          + ", 险种编码" + cLCInsureAccSchema.getRiskCode();

        //判断该账户当天是否已经结算过（防止重复结算插入为0的记录）
        String sql = " select 1 from dual "
                     + "where exists (select 'X' from LCInsureAccTrace "
                     + "             where MoneyType = 'LX' "
                     + "                and PayDate = Current Date "
                     + "                and InsuAccNo ='"
                     + cLCInsureAccSchema.getInsuAccNo() + "'"
                     + "               and PolNo ='"
                     + cLCInsureAccSchema.getPolNo() + "') ";
        String sHasBala = new ExeSQL().getOneValue(sql);
        if(sHasBala != null && sHasBala.equals("1"))
        {
            mErrors.addOneError("当天已经结算过，不需要结算:" + tPolInfo);
            return false;
        }

        if(!LLCaseCommon.checkClaimState(cLCInsureAccSchema.getPolNo()))
        {
            mErrors.addOneError("险种有未结案的理赔，不能进行结算");
            return false;
        }

        LPEdorItemSet set = CommonBL.getEdorItemMayChangePrem(
            cLCInsureAccSchema.getContNo());
        if(set.size() > 0)
        {
            String edorInfo = "";
            for(int i = 1; i <= set.size(); i++)
            {
                edorInfo += set.get(i).getEdorNo() + " "
                    + set.get(i).getEdorType() + ", ";
            }
            mErrors.addOneError("保单有未结案的保全项目:" + edorInfo);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        InsuAccBalaTask task = new InsuAccBalaTask();
        task.run();
    }
}
