package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;

import com.sinosoft.lis.operfee.SDMsgBL;

/**
 * <p>Title: 山东烟台短信通知</p>
 *
 * <p>Description:
 * 续期续保发送客户提醒短信
 * </p>
 *
 * <p>Copyright: Copyright (c) 2013-3-20</p>
 *
 * <p>Company: </p>
 *
 * @author LCY
 * @version 1.0
 */
public class SDMsgTask extends TaskThread {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    String Content = "";
    String Email = "";
    String Mobile = "";
    // 输入数据的容器
    public SDMsgTask() {
    }

    /**
     * 实现TaskThread的接口方法run
     * 由TaskThread调用
     * */
    public void run() {
        if (!sendSDYTMsg()) {
            return;
        }
    }
    /**
     * 山东烟台宽限期短信提醒，保单解约后短信提醒，客户资料变更后短息提醒，满期给付后短信提醒
     * @return
     */
    private boolean sendSDYTMsg() {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "863706";//山东烟台
        mVData.add(mGlobalInput);
        SDMsgBL tBSDMsgBL = new SDMsgBL();
        if (!tBSDMsgBL.submitData(mVData, "SDMSG")) {
            System.out.println("短信提醒发送有误！");
            CError tError = new CError();
            tError.moduleName = "SDMsgBL";
            tError.functionName = "submitData";
            tError.errorMessage = "山东烟台分公司短信提醒发送失败";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String args[]) {
        SDMsgTask a = new SDMsgTask();
        a.run();

    }
}
