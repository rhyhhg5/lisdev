package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.yibaotong.LLZBTransFtpXml;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *浙江外包批次信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Zhang
 * @version 1.1
 */

public class LLZWRgtTask extends TaskThread {
		
	/**错误的容器*/
    public CErrors mErrors = new CErrors();

    String opr = "";
    
    public LLZWRgtTask(){}
	
	public void run(){
		LLZBTransFtpXml tLLZBTransFtpXml = new LLZBTransFtpXml();
		if(!tLLZBTransFtpXml.submitData()){
			 System.out.println("批次信息导入出问题了");
             System.out.println(mErrors.getErrContent());
             opr ="false";
             return;
		}else{
			 System.out.println("批次信息导入成功了");
        	 opr ="true";
		}
		
	}

	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
	
	public static void main(String[] args){
		
		LLZWRgtTask tLLZWRgtTask = new LLZWRgtTask();
		tLLZWRgtTask.run();
		
	}
		
}
