package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.cbcheck.ApplyRecallPolUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCApplyRecallPolSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>ClassName:  YbPolicyCancelTask</p>
 * <p>Description: 上海医保卡保单撤销 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2017-5-8 下午02:55:18
 * @author Yu ZhiWei
 */
public class YbPolicyCancelTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	
	public YbPolicyCancelTask() {

	}
	
	/**
	 * run方法
	 */
	@SuppressWarnings("unchecked")
	public void run(){
		System.out.println("======================YbPolicyCancelTask Execute  !!!======================");
		String SQL ="select lcc.prtno, lcc.cardflag, ybe.errorinfo, lcc.managecom " 
					+" from lccont lcc, lcpol lcp, lccontsub lcs, YBK_G03_LIS_ResponseInfo ybg, ljtempfee ljt, YBKErrorList ybe " 
					+" where lcc.prtno = lcp.prtno " 
					+" and lcc.prtno = lcs.prtno "
					+" and lcs.ybkserialno = ybg.PolicyFormerNo " 
					+" and lcc.prtno = ljt.otherno " 
					+" and lcc.prtno = ybe.businessno " 
					+" and lcc.appflag not in ('1') " 
					+" and ljt.enteraccdate is null " 
					+" and ybe.transtype = 'G03' "
					+" and lcp.riskcode in (select code from ldcode where codetype = 'ybkriskcode') " 
					+" and lcs.paymode in ('1')  "
					+" and ybg.MedicalIfSucess = '0' "
					+" and ybg.ActualDecuctPremium <= 0 and (ybg.FailReason <> '08' or ybg.FailReason = '' or ybg.FailReason is null) " 
					+" and exists (select 1 from YBKErrorList where transtype = 'G03' and businessno=lcc.prtno and days(current date) - days(makedate) > 15) "
					+" and not exists (select 1 from YBKErrorList where businessno = lcp.prtno and resultstatus = '1' and transtype = 'T02') "
					+" and not exists (select 1 from LCApplyRecallPol where prtno=lcp.prtno) "
					+" union "
					+" select lcc.prtno, lcc.cardflag, ybe.errorinfo, lcc.managecom " 
					+" from lccont lcc, lcpol lcp, lccontsub lcs, YBK_G03_LIS_ResponseInfo ybg, ljtempfee ljt, YBKErrorList ybe "
					+" where lcc.prtno = lcp.prtno "
					+" and lcc.prtno = lcs.prtno "
					+" and lcs.ybkserialno = ybg.PolicyFormerNo "
					+" and lcc.prtno = ljt.otherno "
					+" and lcc.prtno = ybe.businessno "
					+" and lcc.appflag not in ('1') "
					+" and ybe.transtype = 'G03' " 
					+" and ljt.enteraccdate is null "
					+" and lcp.riskcode in (select code from ldcode where codetype = 'ybkriskcode') " 
					+" and lcs.paymode in ('3') "
					+" and ybg.MedicalIfSucess = '1' "
					+" and ybg.ActualDecuctPremium <= 0 "
					+" and (ybg.FailReason <> '08' or ybg.FailReason = '' or ybg.FailReason is null) "
					+" and exists (select 1 from YBKErrorList where transtype = 'G03' and businessno=lcc.prtno and days(current date) - days(makedate) > 15) "
					+" and not exists (select 1 from YBKErrorList where businessno = lcp.prtno and resultstatus = '1' and transtype = 'T02') "
					+" and not exists (select 1 from LCApplyRecallPol where prtno=lcp.prtno) "
					+" with ur";
		GlobalInput tG = new GlobalInput();
		LCApplyRecallPolSchema tLCApplyRecallPolSchema = new LCApplyRecallPolSchema();
		TransferData tTransferData = new TransferData();
		VData tVData = new VData();
		String tPrtNo;
		String tCardFlag;
		String tErrorInfo;
		String tMagCom;
		SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
		
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++){
    			tPrtNo = tSSRS.GetText(index, 1);
    			tCardFlag = tSSRS.GetText(index, 2);
    			tErrorInfo = tSSRS.GetText(index, 3);
    			tMagCom = tSSRS.GetText(index, 4);
//    			System.out.println("tPrtNo = " + tPrtNo + ",tCardFlag = " + tCardFlag + 
//    					",tErrorInfo = " + tErrorInfo + ",tMagCom = " + tMagCom);
    			tG.ManageCom = tMagCom;
				tG.Operator = "YBK";
    			tTransferData.setNameAndValue("CardFlag",tCardFlag);
    			//补充附加险表			    				
    			tLCApplyRecallPolSchema.setRemark(tErrorInfo);
    			tLCApplyRecallPolSchema.setPrtNo(tPrtNo);	
    			tLCApplyRecallPolSchema.setApplyType("3");
    			tLCApplyRecallPolSchema.setManageCom(tMagCom);
    			tLCApplyRecallPolSchema.setOperator("YBK");
    			// 准备传输数据 VData
    			tVData.add(tLCApplyRecallPolSchema);
    			tVData.add(tTransferData);
    			tVData.add(tG);
    	    	try {
    	    		ApplyRecallPolUI applyRecallPolUI = new ApplyRecallPolUI();
    	    		if(!applyRecallPolUI.submitData(tVData, "")){
    	    			this.mErrors.copyAllErrors(applyRecallPolUI.mErrors);
    	    			System.out.println("上海医保卡核心解锁处理撤单失败："+mErrors.getFirstError());
    	    		}
				} catch (Exception e) {
					System.out.println("上海医保卡核心解锁处理撤单异常");
					e.printStackTrace();
				}
    		}
    	}
    	System.out.println("======================YbPolicyCancelTask End  !!!======================");
	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		YbPolicyCancelTask ybPolicyCancelTask = new YbPolicyCancelTask();
		ybPolicyCancelTask.run();
	}
	
}
