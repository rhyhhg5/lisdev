package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.db.LQCLAIMPOLDB;
import com.sinosoft.lis.db.SolidificationLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LQCLAIMPOLSchema;
import com.sinosoft.lis.schema.SolidificationLogSchema;
import com.sinosoft.lis.vschema.LQCLAIMPOLSet;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;

//解决的两个问题
//------------------------------------------
// 第一为什么老是插入两遍，是因为do--while的判断条件导致会循环两次，之前的例子之所以不会出现set中有缓存的问题
// 是因为RSWrapper的getData方法会自动清除set容器中的缓存，而此类中使用的并不是RSWrapper的容器而是insertSet和UpdateSet容器
// 第二为什么老是查询到是新数据，因为isNewSql中最后少了一个'。
//----------------------------------------------------
public class ExtractDataForLQCLAIMPOLTask extends TaskThread{
	// 获取当前时间
	public String mCurrentTime = PubFun.getCurrentDate();
	// 获取一个日志表Schema
	private SolidificationLogSchema mSolidificationLogSchema = new SolidificationLogSchema();

	// run方法
	public void run() {
		System.out.println("ExtractDataForLQCLAIMPOLTask批处理开始执行！");
		String tSolidificationLogSQL = "select * from SolidificationLog where tablename = 'LQCLAIMPOL' with ur";
		System.out.println("查询日志Sql：" + tSolidificationLogSQL);
		System.out.println("声明一个SolidificationLogDB来执行这个sql");
		SolidificationLogDB tSolidificationLogDB = new SolidificationLogDB();
		System.out.println("用日志表的Schema来接收返回的结果集");
		mSolidificationLogSchema = tSolidificationLogDB.executeQuery(tSolidificationLogSQL)
				.get(1);
		String tEndDate = mSolidificationLogSchema.getEndDate();
		String tState = mSolidificationLogSchema.getState();
		// 判断状态是否为11
		// 状态是00的话endDate就不会变，所以这里不需要进行处理，在sql中进行优化
		if("00".equals(tEndDate))
		{
			System.out.println(tEndDate+"数据状态为00，需要先进行手动处理");
			return;
		}
		else if("11".equals(tState)) 
		{
			// 若是的话就该执行下一天的数据，将EndDate+1
			tEndDate = PubFun.calDate(tEndDate, 1, "D", "");
		}
		// 转换格式类FDate
		FDate tFDate = new FDate();
		Date tStartDate = tFDate.getDate(tEndDate);
		Date tRealEndDate = tFDate.getDate(mCurrentTime);
		// 判断执行到当前日期的前一天
		while (tStartDate.compareTo(tRealEndDate) < 0) {
			String tModifyDate = tFDate.getString(tStartDate);
			mSolidificationLogSchema.setEndDate(tModifyDate);
			// 不论执行插入成功与否，都要将modifydate和modifytime改为这次执行的时间
			// 判断插入的是否成功
			System.out.println("开始操作第 " + tModifyDate + " 天的数据");
			if (!insert(tModifyDate)) {
				tSolidificationLogDB.setSchema(mSolidificationLogSchema);
				tSolidificationLogDB.update();
				break;
			}
			mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
			mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
			mSolidificationLogSchema.setState("11");
			mSolidificationLogSchema.setExceptionInfo("处理成功");
			tSolidificationLogDB.setSchema(mSolidificationLogSchema);
			tSolidificationLogDB.update();
			System.out.println("处理第 " + tModifyDate + " 天数据成功");
			// 将天数加1
			tStartDate = PubFun.calDate(tStartDate, 1, "D", null);
		}
	}

	public boolean insert(String cModifyDate) {
		String tQuerySQL = "select type,RiskCode,sum(Prem) prem,ManageCom,Payintv,Salechnl,cvalidate,enddate,signdate,sum(peoples) as peoples"
				+ " from "
					+ " (select 'C' Type, a.RiskCode RiskCode, cast(sum(a.Prem) as decimal(12, 2)) Prem, "
					+ " a.ManageCom ManageCom, a.Payintv Payintv, a.salechnl Salechnl, "
					+ " a.cvalidate cvalidate, a.enddate enddate, "
					+ " (select signdate from lcgrpcont where a.grpcontno=lcgrpcont.grpcontno and appflag = '1'  union select distinct signdate from lccont where a.contno=lccont.contno and appflag = '1' fetch first 1 rows only) signdate, count(a.insuredno) peoples "
					+ " from "
					+ " lcpol a "
					+ " where 1=1"
					+ " and a.appflag = '1' "
					+ " and exists (select 1 from lcgrpcont where a.grpcontno=lcgrpcont.grpcontno and appflag = '1' "
					+ "	      union select 1 from lccont where a.contno=lccont.contno and appflag = '1')"
					+ " and a.modifydate = '"+ cModifyDate+ "' "
					+ " and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskprop = 'I') "
					+ " group by a.ManageCom, a.Payintv, a.salechnl, a.cvalidate, a.enddate, a.riskcode, a.grpcontno,a.contno "
					+ " union all "
					+ " select 'B' Type, a.RiskCode riskcode, cast(sum(a.Prem) as decimal(12, 2)) Prem, "
					+ " a.ManageCom managecom, a.Payintv payintv, a.salechnl salechnl, "
					+ " a.cvalidate cvalidate, ( "
					+ " case b.edortype when 'WT' then a.cvalidate else b.edorvalidate end) enddate, a.signdate signdate, count(a.insuredno) peoples "
					+ " from "
					+ " lbpol a, lpedoritem b "
					+ " where  a.edorno = b.edorno "
					+ " and a.modifydate = '"+ cModifyDate+ "' "
					+ " and a.contno = b.contno "
					+ " and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskprop = 'I') "
					+ " and b.edortype in ('CT', 'XT', 'WT') "
					+ " and exists (select 1 from lpedorapp where edoracceptno = b.edorno and edorstate = '0') "
					+ " group by a.ManageCom, a.Payintv, a.salechnl, a.cvalidate, b.edorvalidate, "
					+ " a.riskcode, a.signdate, b.edortype "
					+ " union all "
					+ " select 'B' Type, RiskCode, cast(sum(Prem) as decimal(12, 2)) Prem, ManageCom, "
					+ " Payintv, salechnl, cvalidate, enddate, (select signdate from lbgrpcont where a.grpcontno=lbgrpcont.grpcontno and appflag = '1'  union select signdate from lbcont where a.contno=lbcont.contno and appflag = '1' fetch first 1 rows only) signdate, count(insuredno) peoples "
					+ " from "
					+ " lbpol a "
					+ " where 1=1"
					+ " and appflag = '1' "
					+ " and exists (select 1 from lbgrpcont where a.grpcontno=lbgrpcont.grpcontno and appflag = '1' "
					+ "	      union select 1 from lbcont where a.contno=lbcont.contno and appflag = '1')"
					+ " and a.modifydate = '"+ cModifyDate+ "' "
					+ " and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskprop = 'I') "
					+ " and exists (select 1 from lcrnewstatelog where contno = a.contno and newpolno = a.polno) "
					+ " group by ManageCom, Payintv, salechnl, cvalidate, riskcode, grpcontno, grppolno, enddate, contno"
				+ " ) m "
				+ " group by type,RiskCode,ManageCom,Payintv,Salechnl,cvalidate,enddate,signdate with ur";
		System.out.println(tQuerySQL);
		// 将这个sql执行后的结果放到一个结果集中
		// 注意：这个sql需要用RSWrapper来执行，因为LQCLAIMPOLDB只能执行与
		// LQCLAIMPOL表相关的增删改查操作
		RSWrapper tRSWrapper = new RSWrapper();
		// 声明总的Set
		LQCLAIMPOLSet tNewLQCLAIMPOLSet = new LQCLAIMPOLSet();
		tRSWrapper.prepareData(tNewLQCLAIMPOLSet, tQuerySQL);
		// 声明DB来执行sql
		LQCLAIMPOLDB tLQCLAIMPOLDB = new LQCLAIMPOLDB();
		
		// 需要声明一个MMap，一个VData就可以
		MMap tMMap = new MMap();
		VData tVData = new VData();
		System.out.println("开始根据查询到的数据获取每一条数据来判断对它的操作");
		do {
			// getData(方法会一次性从result中取出5000条数据，并且会将存放数据的容器清空)
			tRSWrapper.getData();
			if(null==tNewLQCLAIMPOLSet||0==tNewLQCLAIMPOLSet.size())
			{
				return true;
			}
			// 声明需要直接新增的Set
			LQCLAIMPOLSet tInsertLQCLAIMPOLSet = new LQCLAIMPOLSet();
			// 声明需要更新的Set
			LQCLAIMPOLSet tUpdateLQCLAIMPOLSet = new LQCLAIMPOLSet();
			// 开始循环比较判断是否为新数据
			for (int i = 1; i <= tNewLQCLAIMPOLSet.size(); i++) 
			{
				// 声明接收遍历Set的Schema
				LQCLAIMPOLSchema tLQCLAIMPOLSchema = new LQCLAIMPOLSchema();
				tLQCLAIMPOLSchema = tNewLQCLAIMPOLSet.get(i);
				// 注意peoples的数据类型是int
				String tOldSQL = "select * from LQCLAIMPOL where 1=1"
						+ " and riskcode = '" + tLQCLAIMPOLSchema.getRiskCode() + "' "
						+ " and ManageCom = '" + tLQCLAIMPOLSchema.getManageCom() + "' "
						+ " and PayIntv = '" + tLQCLAIMPOLSchema.getPayIntv()+ "' "
						+ " and Salechnl = '" + tLQCLAIMPOLSchema.getSalechnl() + "' "
						+ " and CValiDate = '" + tLQCLAIMPOLSchema.getCValiDate() + "' "
						+ " and EndDate = '" + tLQCLAIMPOLSchema.getEndDate() + "' "
						+ " and SignDate = '"+ tLQCLAIMPOLSchema.getSignDate() + "' with ur";
				System.out.println("tOldSQL:"+tOldSQL);
				// System.out.println("赋值给新的schema去数据库中查询看是否为新数据");
				// 声明接收DB执行判断是否为新数据查询结果的Set
				LQCLAIMPOLSet tOldLQCLAIMPOLSet = new LQCLAIMPOLSet();
				tOldLQCLAIMPOLSet = tLQCLAIMPOLDB.executeQuery(tOldSQL);
				//查询原来数据，如果没有，则为新提出来的数据
				if (null != tOldLQCLAIMPOLSet && tOldLQCLAIMPOLSet.size() > 0) 
				{
					for (int j = 1; j <= tOldLQCLAIMPOLSet.size(); j++) 
					{
						// 声明接收DB执行判断是否为新数据查询结果的Schema
						LQCLAIMPOLSchema tOldLQCLAIMPOLSchema = new LQCLAIMPOLSchema();
						tOldLQCLAIMPOLSchema = tOldLQCLAIMPOLSet.get(j);
						if (tOldLQCLAIMPOLSchema.getType().equals(tLQCLAIMPOLSchema.getType())) 
						{
							tOldLQCLAIMPOLSchema.setPrem(tOldLQCLAIMPOLSchema.getPrem() + tLQCLAIMPOLSchema.getPrem());
							tOldLQCLAIMPOLSchema.setPeoples(tOldLQCLAIMPOLSchema.getPeoples() + tLQCLAIMPOLSchema.getPeoples());
						} 
						else 
						{
							tOldLQCLAIMPOLSchema.setPrem(tOldLQCLAIMPOLSchema.getPrem() - tLQCLAIMPOLSchema.getPrem());
							tOldLQCLAIMPOLSchema.setPeoples(tOldLQCLAIMPOLSchema.getPeoples() - tLQCLAIMPOLSchema.getPeoples());
									
						}
						tUpdateLQCLAIMPOLSet.add(tOldLQCLAIMPOLSchema);
					}
				} 
				else 
				{
					tInsertLQCLAIMPOLSet.add(tLQCLAIMPOLSchema);
				}
			}
			tMMap.put(tInsertLQCLAIMPOLSet, "INSERT");
			tMMap.put(tUpdateLQCLAIMPOLSet, "UPDATE");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tVData, "")) 
			{
				System.out.println("ExtractDataForLQCLAIMPOLTask 批处理执行异常！");
				mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
				mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
				mSolidificationLogSchema.setState("00");
				mSolidificationLogSchema.setExceptionInfo("提取" + cModifyDate
						+ "天数据更新时失败");
				return false;
			}
			System.out.println("本次数据提交数据库成功");
			tMMap = new MMap();
			tVData.clear();
		} while (tNewLQCLAIMPOLSet.size() > 0);
		return true;
	}

	public static void main(String[] args) {
		ExtractDataForLQCLAIMPOLTask tExtractDataForLQCLAIMPOLTask = new ExtractDataForLQCLAIMPOLTask();
		tExtractDataForLQCLAIMPOLTask.run();
	}
}
