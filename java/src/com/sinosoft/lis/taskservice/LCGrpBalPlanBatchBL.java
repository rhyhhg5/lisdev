package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.bq.*;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.LCGrpBalPlanDB;
import com.sinosoft.lis.vschema.LCGrpBalPlanSet;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.bq.LCGrpBalPlanBL;


/**
 * <p>Title: 定期结算计划表日处理</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LCGrpBalPlanBatchBL extends TaskThread {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = null;

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();

    /** 数据操作字符串 */
    private String mOperate;
    /**
     * 构造函数
     */
    public LCGrpBalPlanBatchBL() {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     */
    public void run() {
        System.out.println("定期结算自动运行开始！");
        dealData();
        System.out.println("定期结算自动运行结束！");
    }

    /**
     * 数据传书方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //数据校验
        if (!checkData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpBalPlanBatchBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败LCGrpBalPlanBatchBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpBalPlanBatchBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LCGrpBalPlanBatchBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 获取从UI传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        this.mInputData = cInputData;
        if (this.mInputData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpBalPlanBatchBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "数据处理失败LCGrpBalPlanBatchBL-->getInputData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData() {
        return true;
    }

    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData() {
        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();

        String stateFlagPart =
            "  and exists "
            + "    (select 1 from LCGrpCont where GrpContNo = a.GrpContNo "
            + "        and (StateFlag is null or StateFlag = '"
            + BQ.STATE_FLAG_SIGN + "')) ";

        String strSql = "";
        if(this.mInputData == null)
        {
            strSql = "select  * from lcgrpbalplan a where NextDate <= '" +
                     this.theCurrentDate +
                     " ' and state = '0' and balintv != 0  and balstartdate <= '" +
                     this.theCurrentDate + " ' " +
                     stateFlagPart;
        } else {
            strSql = "select * from LCGrpBalPlan a where NextDate <='" +
                     this.theCurrentDate + "' and managecom like '" +
                     this.mGlobalInput.ManageCom +
                    "%' and state = '0' and balintv != 0 and balstartdate <= '"
                    + this.theCurrentDate + " ' " +
                     stateFlagPart;
        }
//        对于所有跨自然年度的定期结算保单，在每年的12.31执行批处理提前结算
        if(theCurrentDate.endsWith("12-31")){
            strSql += "union select * from LCGrpBalPlan a where NextDate > '" +
                    this.theCurrentDate + "' and state = '0' and balintv != 0  and balstartdate <= '" +
                    this.theCurrentDate + " ' " +
                     stateFlagPart;
        }
        System.out.println(strSql);

        LCGrpBalPlanSet tLCGrpBalPlanSet = tLCGrpBalPlanDB.executeQuery(strSql);
        if (tLCGrpBalPlanDB.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLCGrpBalPlanDB.mErrors);
            return false;
        }
        for (int i = 1; i <= tLCGrpBalPlanSet.size(); i++) {
            LGWorkSchema tLGWorkSchema = new LGWorkSchema();
            String sql =
                    "select appntno,grpname,managecom from lcgrpcont where grpcontno ='" +
                    tLCGrpBalPlanSet.get(i).getGrpContNo() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(sql);
            if (tSSRS.getMaxRow() == 0) {
                System.out.println("集体合同" +
                                   tLCGrpBalPlanSet.get(i).getGrpContNo() +
                                   "不存在或失效！");
                continue;
            }
            String tCustomerNO = tSSRS.GetText(1, 1);
            String tCustomerName = tSSRS.GetText(1, 2);
            String tManageCom = tSSRS.GetText(1, 3);
            tLGWorkSchema.setCustomerNo(tCustomerNO);
            tLGWorkSchema.setCustomerName(tCustomerName);
            tLGWorkSchema.setStatusNo("3"); //处理状态
            tLGWorkSchema.setTypeNo("0602"); //业务类型
            tLGWorkSchema.setAcceptWayNo("8");
            tLGWorkSchema.setContNo(tLCGrpBalPlanSet.get(i).getGrpContNo());

            VData tVData = new VData();
            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.Operator = "Server";
            tVData.add(tLGWorkSchema);
            tVData.add(tGlobalInput);
            String tsql =
                    "select target from lgautodeliverrule where  sourcecomcode = '" +
                    tManageCom + " ' and goaltype = '6' ";
            tSSRS = tExeSQL.execSQL(tsql);
            if (tSSRS.getMaxRow() == 0) {
                tsql =
                        "select target from lgautodeliverrule where  sourcecomcode like '" +
                        tManageCom.substring(0, 4) + "%' and goaltype = '6' ";
                tSSRS = tExeSQL.execSQL(tsql);
                if (tSSRS.getMaxRow() == 0) {
                    // @@错误处理
//                    CError tError = new CError();
//                    tError.moduleName = "LCGrpBalPlanBatchBL";
//                    tError.functionName = "dealData";
//                    tError.errorMessage =
//                            "数据处理失败LCGrpBalPlanBatchBL-->dealData()找不到自动转发规则。";
//                    this.mErrors.addOneError(tError);
                	tsql =
                        "select workboxno from lgworkbox where ownerno in (select usercode from lduser a,lggroupmember b where a.comcode like '" +
                        tManageCom.substring(0, 4) + "%'  and a.usercode=b.memberno and b.postno='PA04') fetch first 1 row only with ur ";
                	tSSRS = tExeSQL.execSQL(tsql);
                    
                    if (tSSRS.getMaxRow() == 0) {
                    System.out.println("集体合同" +
                                tLCGrpBalPlanSet.get(i).getGrpContNo() +
                                "找不到自动转发规则");	
                    continue;
                    }
                }
            }
            tVData.add(tSSRS.GetText(1, 1));

            LCGrpBalPlanBL tLCGrpBalPlanBL = new LCGrpBalPlanBL();
            tLCGrpBalPlanBL.submitData(tVData, "");

        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    public static void main(String[] args) {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "pa0001";
        tGlobalInput.ManageCom = "8632";

        VData tVData = new VData();
        tVData.add(tGlobalInput);

        LCGrpBalPlanBatchBL tLCGrpBalPlanBatchBL = new LCGrpBalPlanBatchBL();
        tLCGrpBalPlanBatchBL.run();
    }
}
