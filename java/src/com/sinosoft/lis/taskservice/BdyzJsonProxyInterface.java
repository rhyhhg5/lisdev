package com.sinosoft.lis.taskservice;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jdom.output.XMLOutputter;

//import com.sinosoft.services.config.ConfigFactory;
//import com.sinosoft.services.utility.EncryptUtil;
//import com.sinosoft.services.utility.SaveMessage;
//import com.sinosoft.services.utility.XmlTag;
//import com.sinosoft.webservice.iac.VerifyContNewOfIAC;

import net.sf.json.JSONObject;

/**
 * http接口-保单验真二期接口
 * 
 * @author 周小艳 2017-04-17
 * 
 */
public class BdyzJsonProxyInterface  {
	private static final long serialVersionUID = 1L;
	private Logger mLogger = Logger.getLogger(BdyzJsonProxyInterface.class);
//	private final String mPasswordeq = ConfigFactory.getPreConfig().getString(
//			"passwordeq");// 用于解密、加密
//	private final String mToken = ConfigFactory.getPreConfig().getString(
//			"token");// 用于验签
	private boolean mIsWin = true;// 操作系统判定
	
	private String mSerialNo = "";
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		mLogger.info("中保协保单验真验真接口公共http接口Start Servlet");
		// 记录服务启动时间
		long mStartMillis = System.currentTimeMillis();
		/** 将数据转换成UTF-8数据格式 */
		request.setCharacterEncoding("UTF-8");
		OutputStream out = response.getOutputStream();
		String result = "";// 返回结果应加密处理
		response.setContentType("application/xml;charset=UTF-8");
		try 
		{
			// 0、首先判断是何种操作系统,用于加载不同的加密解密方法
			mIsWin = checkOsName();
			// 1、获取验签参数值,进行验签操作
			String tTimestamp = request.getParameter("timestamp");// 时间戳
			String tNonce = request.getParameter("nonce");// 随机数
			String tSignature = request.getParameter("signature");// 签名
//			if (tTimestamp == null || "".equals(tTimestamp) || tNonce == null
//					|| "".equals(tNonce) || tSignature == null
//					|| "".equals(tSignature)) 
//			{
//				result = getSoapError(errorCode01, errorReason01 + ":验签参数不能为空！");
//				out.write(result.getBytes("UTF-8"));
//				return;
//			}
//			if (!EncryptUtil.checkSignature(mToken, tSignature, tTimestamp,
//					tNonce)) 
//			{
//				result = getSoapError(errorCode02, errorReason02);
//				out.write(result.getBytes("UTF-8"));
//				return;
//			}
			InputStream im = request.getInputStream();
			String encrypted = streamToString(im); // 得到请求报文的加密串
			// 3、解密得到请求的报文
//			String decryptedJson = generateDecryptJson(encrypted);
			//以UTF-8编码解析
			encrypted = new String(encrypted.getBytes(),"UTF-8");
			mLogger.info("eci前置机获取的保单验真报文(JSON格式)为：" + encrypted);
			// 4、保存JSON格式的报文
//			saveInJson(decryptedJson);
//			JSONObject jsonObjectIn = JSONObject.fromObject(decryptedJson);
//			// 5、调用核心后置机Servlet，获取标准返回报文
//			result = requestJSON(
//					ConfigFactory.getPreConfig()
//							.getString("PostServletURLJSON"), jsonObjectIn);	
			LAInterMixedSpecialCharge tVerifyContNewOfIAC=new LAInterMixedSpecialCharge();
			JSONObject tJsonObject = new JSONObject();
			result = tVerifyContNewOfIAC.service(tJsonObject).toString();
			// 6、将JSON格式的报文去除空格及换行
			result = result.replaceAll("\r\n", "").replaceAll("\r", "")
					.replaceAll("\n", "");
			mLogger.info("核心返回报文(json格式)为：" + result);
			saveOutJson(result);
			String json = result;
			// 7、获取加密后的JSON字符串
			result = generateEncryptJson(json);
			out.write(result.getBytes("UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
			result = getSoapError(errorCode01, errorReason01);
			out.write(result.getBytes("UTF-8"));
			return;
		} finally {
			out.flush();
			out.close();
		}
		// 记录服务结束时间
		long mDealMillis = System.currentTimeMillis() - mStartMillis;
		mLogger.info("处理总耗时：" + mDealMillis / 1000.0 + "s");
		mLogger.info("End Service");
	}

	private String requestJSON(String request_url, JSONObject jsonObject) {
		String tReturn = "";
		try {
			URL url = new URL(request_url);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			// 设置维持长连接
			connection.setRequestProperty("Connection", "Keep-Alive");
			// 设置文件字符集:
			connection.setRequestProperty("Charset", "UTF-8");
			// 转换为字节数组
			byte[] data = (jsonObject.toString()).getBytes();
			// 设置文件长度
			connection.setRequestProperty("Content-Length",
					String.valueOf(data.length));
			connection.setInstanceFollowRedirects(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.connect();
			DataOutputStream out = new DataOutputStream(
					connection.getOutputStream());
			//设置请求类型，用于区分保单验真接口类型 02-保单验真二期 03-客户证件总数保单验真- 04-客户证件总数保单验真
			jsonObject.put("requestType", "02");
			out.writeBytes(jsonObject.toString());
			out.flush();
			out.close();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String lines;
			StringBuffer sbf = new StringBuffer();
			while ((lines = reader.readLine()) != null) {
				//lines = new String(lines.getBytes(),"UTF-8");
				sbf.append(lines);
			}
			mLogger.info("核心返回的JSON信息："+sbf);
			tReturn += sbf;
			reader.close();
			// 断开连接
			connection.disconnect();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tReturn;
	}

	/**
	 * 封装错误JSON报文（已加密的）
	 * 
	 * @param pResponseCode
	 * @param pErrorMsg
	 * @return
	 * @throws Exception
	 */
	private String getSoapError(String pResponseCode, String pErrorMsg) {
		try 
		{
			String json = "{\"respData\": {\"errorCode\": \""+pResponseCode+"\",\"errorReason\": \""+pErrorMsg+"\"}}";
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("resp", json);//防止乱序
			mLogger.info("BdyzJsonProxyInterface-返回错误报文信息："+jsonObj);
			String jsonStr = jsonObj.get("resp").toString();
			jsonStr=jsonStr.replaceAll("\r\n", "");
			jsonStr=jsonStr.replaceAll("\r", "");
			jsonStr=jsonStr.replaceAll("\n", "");
			return generateEncryptJson(jsonStr);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 加密 重新封装明码JSON报文生成新的密文
	 * 
	 * @param pResponseCode
	 * @param pErrorMsg
	 * @return
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public String generateEncryptJson(String aJsonOutStr) throws Exception {
		mLogger.info("Into BdyzJsonProxyInterface.generateEncryptJson()开始...");
		if (mIsWin) {
			return EncryptUtil.encrypt(aJsonOutStr, mPasswordeq);
		} else {
			return EncryptUtil.LinuxEncrypt(aJsonOutStr, mPasswordeq);
		}

	}

	/**
	 * 解密JSON报文获取明码报文
	 * 
	 * @param aJsonOutStr
	 * @return
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public String generateDecryptJson(String aJsonInStr) throws Exception 
	{
		mLogger.info("Into BdyzJsonProxyInterface.generateDecryptJson()开始...");
		if (mIsWin) 
		{
			return EncryptUtil.decrypt(aJsonInStr, mPasswordeq);
		}else 
		{
			return EncryptUtil.LinuxDecrypt(aJsonInStr, mPasswordeq);
		}

	}

	/**
	 * 判断是何种操作系统
	 * 
	 * @return
	 */
	private boolean checkOsName() 
	{
		Properties prop = System.getProperties();
		String os = prop.getProperty("os.name");
		mLogger.info("当前的操作系统为-os：" + os);
		if (os.toLowerCase().startsWith("win"))
		{
			return true;
		}
		return false;
	}

	public static void prtDocument(org.jdom.Document doc) {
		PrintWriter out = new PrintWriter(System.out);
		XMLOutputter outputter = new XMLOutputter();
		try {
			outputter.output(doc, out);
		} catch (IOException e) {
			e.printStackTrace();
		}

		out.close();

	}

	/**
	 * 原方式streamToString1 会加入换行。导致报文会多一个换行，校验报文会出错。
	 * 
	 * @param im
	 * @return
	 * @throws IOException
	 */
	private String streamToString(InputStream im) throws IOException 
	{
		BufferedReader in = new BufferedReader(new InputStreamReader(im));
		StringBuffer buffer = new StringBuffer();
		String line = "";
		while ((line = in.readLine()) != null) {
			buffer.append(line);
			mLogger.info("streamToString----"+line);
		}
		return buffer.toString();
	}

//	/**
//	 * 保存请求JSON报文
//	 * 
//	 * @param requJson
//	 */
//	private void saveInJson(String requJson) 
//	{
//		JSONObject jsonObj = null;
//		try 
//		{
//			jsonObj = JSONObject.fromObject(requJson);
//			mSerialNo = jsonObj.getJSONObject("reqData").get("serialNo")
//					.toString();
//			String tFunction = "02";//保单验真二期-保单验真
//			mSerialNo += "_" + tFunction + "_";
//			String mFileName = SaveMessage.generateFileName(mSerialNo, "txt");
//			String mInFilePath = SaveMessage.generateFilePath("InNoStd");
//			mLogger.info("保存非标准路径：！" + mInFilePath);
//			SaveMessage.save(requJson.getBytes("UTF-8"), mFileName, "InNoStd");
//		} 
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			mLogger.info("保存非标准报文JSON失败！");
//		}
//	}

	/**
	 * 保存返回JSON报文
	 * 
	 * @param requJson
	 */
	private void saveOutJson(String respJson) {
		try {
			/*String tFunction = "02";
			mSerialNo += "_" + tFunction + "_";*/
			String mFileName = SaveMessage.generateFileName(mSerialNo, "txt");
			String mInFilePath = SaveMessage.generateFilePath("OutStd");
			mLogger.info("保存非标准路径：！" + mInFilePath);
			SaveMessage.save(respJson.getBytes("UTF-8"), mFileName, "OutStd");
		} catch (Exception e) {
			e.printStackTrace();
			mLogger.info("保存非标准报文JSON失败！");
		}
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		mLogger.info("Into BdyzJsonProxyInterface.doGet()...");
		response.getOutputStream()
				.println(
						"Now in BdyzJsonProxyInterface, and BdyzJsonProxyInterface.doGet() work well!");
	}
}

