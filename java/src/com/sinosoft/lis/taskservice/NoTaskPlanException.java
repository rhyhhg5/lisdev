package com.sinosoft.lis.taskservice;

/**
 * <p>Title: 没有任务计划异常</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author ZhangRong
 * @version 1.0
 */

public class NoTaskPlanException extends Exception
{

  public NoTaskPlanException()
  {
  }
}