package com.sinosoft.lis.taskservice;

import java.util.*;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.task.*;

/**
 * <p>Title: 医保通客户信息合并批处理程序</p>
 * <p>Description: 每天由系统定时调用，根据医院编码、客户号、申请日期、已经确认的客户账单进行合并 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class LLHospCustomerConfirmTask  extends TaskThread
{
	private CErrors mErrors = new CErrors(); 
    private VData mResult = new VData(); 
  		
    private LLSecurityReceiptSet mLLSecurityReceiptSet = new LLSecurityReceiptSet();
    private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
    private LLHospAccTraceSet mLLHospAccTraceSet = new LLHospAccTraceSet();
    private LLFeeMainSet mLLFeeMainSet = new LLFeeMainSet();
    
    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
    private LCGetSchema mLCGetSchema = new LCGetSchema();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LLCaseSchema tLLCaseSchema = new LLCaseSchema();
    private LLAffixSet tLLAffixSet = new LLAffixSet();
    private LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
	private LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
	private LLCaseRelaSchema tLLCaseRelaSchema = new LLCaseRelaSchema();
	private LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
	
	private LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
	private LLToClaimDutySchema tLLToClaimDutySchema = new LLToClaimDutySchema();
	private LLClaimSchema tLLClaimSchema = new LLClaimSchema();
	private LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema(); 
	private LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema(); 
	private LJSGetSchema tLJSGetSchema = new LJSGetSchema();
	private LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
	
	private LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
	private LLClaimUWDetailSchema tLLClaimUWDetailSchema = new LLClaimUWDetailSchema();
	private LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();
	private LLClaimUWMDetailSchema tLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();	
	
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mHospitCode = "";
    private String mCustomerNo = "";
    private String mConfirmDate = "";
    private String mRealPay = "";
    private String mHandler = "";
    private String mManageCom = "";
    private String mGrpContNo = "";
    private String mRiskCode = "";
    private String mContNo = "";
    private String mPolNo = "";
    private String mCaseNo = "";
    private String mClmNo = "";
 
    private boolean submitData()throws Exception 
    {
    	if (!getInputData())
        {    		
            return false;
        }
        if (!checkData())
        {
        	System.out.print("校验数据失败");
            return false;
        }

        if (!dealData())
        {
        	System.out.print("处理数据失败!");
            return false;
        }
       
        return true;
    }    
    private boolean dealData()
    {    	  
    	 String LLHospSql="select HospitCode,CustomerNo,ConfirmDate,sum(Realpay),b.riskcode,b.grpcontno "
    		 	+" from LLHospAccTrace a,lcinsureacctrace b "
    		 	+" where a.caseno=b.otherno and b.OtherType='5' and MoneyType='PK' and State='temp'"
    			+" and a.ConfirmState='1' and a.DealState='0' group by HospitCode,CustomerNo,ConfirmDate,b.riskcode,b.grpcontno";
	     ExeSQL LLHospExeSQL = new ExeSQL();
	     SSRS LLHospSSRS = LLHospExeSQL.execSQL(LLHospSql);
	     System.out.println("LLHospSql========"+LLHospSql);
	     System.out.println("账单合并后的案件数===="+LLHospSSRS.getMaxRow());
	     if(LLHospSSRS!= null && LLHospSSRS.getMaxRow()>0)
	     {        	
	      	 for (int i = 1; i <= LLHospSSRS.getMaxRow(); i++)
	         { 	         		 	      		
	      		mHospitCode = LLHospSSRS.GetText(i, 1);
	 	    	mCustomerNo = LLHospSSRS.GetText(i, 2);
	 	    	mConfirmDate = LLHospSSRS.GetText(i, 3);
	 	    	mRealPay = LLHospSSRS.GetText(i, 4);	
	 	    	mRiskCode = LLHospSSRS.GetText(i, 5);	
	 	    	mGrpContNo = LLHospSSRS.GetText(i, 6);
	 	    	
	 	    	String mSQL="select handler,(select comcode from ldcode1 where codetype='ybt_service' and code='11' and code1=a.HospitCode)"
		    		+" from LLHospAccTrace a where HospitCode='"+ mHospitCode +"' and CustomerNo='"+ mCustomerNo +"' and ConfirmDate='"+ mConfirmDate +"'"
		    		+" order by ConfirmDate,ConfirmTime desc fetch first 1 rows only";
		    	ExeSQL tExeSQL = new ExeSQL();
		        SSRS tSSRS = tExeSQL.execSQL(mSQL);
		        if(tSSRS.getMaxRow()>=1)
		        {
		        	mHandler = tSSRS.GetText(1, 1);
		        	mManageCom = tSSRS.GetText(1, 2);        
		        }
		        if(!checkHospCont())
		        {
		        	buildError("dealData()","医院门诊信息在系统中查询失败！");			        	
	 	            return false;
		        }
		        if (mManageCom.length() == 2)
		        	mManageCom = mManageCom + "000000";
		        if (mManageCom.length() == 4)
		        	mManageCom = mManageCom + "0000";
		        System.out.println("管理机构===="+mManageCom );
		        /*生成理赔号*/    	
		        String tLimit = PubFun.getNoLimit(mManageCom);
		        mCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
		        System.out.println("理赔案件号===="+mCaseNo);
		 
		        String limit = PubFun.getNoLimit(this.mManageCom);
		        mClmNo = PubFun1.CreateMaxNo("CLMNO", limit);
		        System.out.println("mClmNo======"+mClmNo);
		        
		        if(!inserCaseData())
		        {
		        	buildError("dealData()","生成受理信息失败！");			        	
	 	            return false;
		        }
		        if(!inserClaimData())
		        {
		        	buildError("dealData()","生成检录理算信息失败！");			        	
	 	            return false;
		        }
		        if(!inseruwData())
		        {
		        	buildError("dealData()","生成审批信息失败！");	
	 	            return false;
		        }
		        if(!updateHospData())
		        {
		        	buildError("dealData()","修改原门诊账单信息失败！");	
	 	            return false;
		        }
		        MMap mMap = new MMap();
		        mMap.put(tLLCaseSchema, "INSERT");
				mMap.put(tLLAffixSet, "INSERT");
				mMap.put(tLLRegisterSchema, "INSERT");
				mMap.put(tLLCaseRelaSchema, "INSERT");
				mMap.put(tLLSubReportSchema, "INSERT");
				mMap.put(tLLAppClaimReasonSchema , "INSERT");
				
		    	mMap.put(tLLCasePolicySchema, "INSERT");
				mMap.put(tLLToClaimDutySchema, "INSERT");
				mMap.put(tLLClaimSchema, "INSERT");
				mMap.put(tLLClaimPolicySchema, "INSERT");
				mMap.put(tLLClaimDetailSchema, "INSERT");
				mMap.put(tLJSGetSchema , "INSERT");
				mMap.put(tLJSGetClaimSchema , "INSERT");
				
				mMap.put(tLLClaimUWMainSchema, "INSERT");
				mMap.put(tLLClaimUWDetailSchema, "INSERT");
				mMap.put(tLLClaimUnderwriteSchema, "INSERT");
				mMap.put(tLLClaimUWMDetailSchema, "INSERT");
				
				mMap.put(mLCInsureAccTraceSet, "UPDATE");
				mMap.put(mLLSecurityReceiptSet, "UPDATE");
				mMap.put(mLLHospAccTraceSet, "UPDATE");
				mMap.put(mLLFeeMainSet, "UPDATE");
				
		        mResult.clear();
				mResult.add(mMap);					
				
		        PubSubmit tPubSubmit = new PubSubmit();
		        if (!tPubSubmit.submitData(mResult, ""))
		        {
		        	buildError("dealData()","提交数据库保存失败！");			            
		            return false;
		        }
	         }   
	      	 
	     } 
        return true;
    }  
    /**
     * 校验客户保单信息
     * @return boolean
     */
    private boolean checkHospCont()
    {    	            	                   	          	
		LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setGrpContNo(mGrpContNo);
        tLCPolBL.setRiskCode(mRiskCode);
        tLCPolBL.setInsuredNo(mCustomerNo);
        if (tLCPolBL.query().size()<=0) {
        	buildError("checkHospCont()","保单险种信息查询失败！");	        	
            return false;
        }
        mLCPolSchema = tLCPolBL.query().get(1).getSchema();
        mContNo = mLCPolSchema.getContNo();
        mPolNo = mLCPolSchema.getPolNo();
        
        LCInsuredDB tLCInsuredDB =new LCInsuredDB();
        LCInsuredSet tLCInsuredSet =new LCInsuredSet();
        tLCInsuredDB.setGrpContNo(mGrpContNo);
        tLCInsuredDB.setInsuredNo(mCustomerNo);
        tLCInsuredSet = tLCInsuredDB.query();
        if(tLCInsuredSet.size()<=0 )
        {
        	LBInsuredDB tLBInsuredDB =new LBInsuredDB();
        	LBInsuredSet tLBInsuredSet =new LBInsuredSet();
        	tLBInsuredDB.setGrpContNo(mGrpContNo);
            tLBInsuredDB.setInsuredNo(mCustomerNo);
            tLBInsuredSet = tLBInsuredDB.query();
            if(tLBInsuredSet.size()<= 0)
            {
            	buildError("checkHospCont()","被保险人信息查询失败！");	              	
                return false;
            }
            Reflections rf = new Reflections();
            rf.transFields(mLCInsuredSchema, tLBInsuredSet.get(1).getSchema());
        }else{
        	mLCInsuredSchema = tLCInsuredSet.get(1).getSchema();
        }
        
        String getDudySql="select a.dutycode,a.getdutycode,b.getdutykind from lcget a,lmdutygetclm b where a.getdutycode=b.getdutycode "
        				+" and a.polno='"+ mLCPolSchema.getPolNo() +"' "
        				+" union select a.dutycode,a.getdutycode,b.getdutykind from lbget a,lmdutygetclm b where a.getdutycode=b.getdutycode "
        				+" and a.polno='"+ mLCPolSchema.getPolNo() +"' ";
        ExeSQL getDudyExeSQL = new ExeSQL();
	    SSRS getDudySSRS = getDudyExeSQL.execSQL(getDudySql);
	    if(getDudySSRS.getMaxRow()<=0)
	    {
	    	buildError("checkHospCont()","保单责任信息查询失败！");	    	    	
            return false;
	    }
	    mLCGetSchema.setDutyCode(getDudySSRS.GetText(1, 1));
	    mLCGetSchema.setGetDutyCode(getDudySSRS.GetText(1, 2));
	    mLCGetSchema.setGetDutyKind(getDudySSRS.GetText(1, 3));
        return true;
    }
    /**
     * 生成受理信息
     * @return boolean
     */
    private boolean inserCaseData()
    {        	
    	tLLCaseSchema.setCaseNo(mCaseNo);
    	tLLCaseSchema.setRgtNo(mCaseNo);
    	tLLCaseSchema.setRgtType("1");
    	tLLCaseSchema.setRgtState("05");
    	tLLCaseSchema.setCustomerNo(mCustomerNo);
    	tLLCaseSchema.setCustomerName(mLCInsuredSchema.getName());
    	tLLCaseSchema.setReceiptFlag("1"); //特需1    	
    	tLLCaseSchema.setSurveyFlag("0");
    	tLLCaseSchema.setRgtDate(mConfirmDate);
    	tLLCaseSchema.setHandleDate(mConfirmDate);
    	tLLCaseSchema.setClaimCalDate(mConfirmDate);
    	tLLCaseSchema.setAffixGetDate(mConfirmDate);    	
    	tLLCaseSchema.setAccidentDate(mConfirmDate);  
    	
    	int tAge = 0;
		try {
			tAge = LLCaseCommon.calAge(mLCInsuredSchema.getBirthday());
		} catch (Exception e) {
			buildError("checkHospCont()","被保险人年龄计算失败！");	    			
	        return false;
		}
    	tLLCaseSchema.setCustBirthday(mLCInsuredSchema.getBirthday());
    	tLLCaseSchema.setCustomerSex(mLCInsuredSchema.getSex());
		tLLCaseSchema.setCustomerAge(tAge);
		tLLCaseSchema.setIDType(mLCInsuredSchema.getIDType());
		tLLCaseSchema.setIDNo(mLCInsuredSchema.getIDNo());
		tLLCaseSchema.setHandler(mHandler);
		tLLCaseSchema.setDealer(mHandler);
		tLLCaseSchema.setMngCom(mManageCom);
		tLLCaseSchema.setOperator(mHandler);
		tLLCaseSchema.setMakeDate(mCurrentDate);
		tLLCaseSchema.setMakeTime(mCurrentTime);
		tLLCaseSchema.setModifyDate(mCurrentDate);
		tLLCaseSchema.setModifyTime(mCurrentTime);
		tLLCaseSchema.setRigister(mHandler);
		tLLCaseSchema.setClaimer(mHandler);
		
		//申请材料流水号
		tLLAffixSet = new LLAffixSet();
		for(int index = 1 ;index <= 3; index ++)
		{
			String tLimit = PubFun.getNoLimit(mManageCom);
			String tAffixNo = PubFun1.CreateMaxNo("rgtaffixno", tLimit);
			System.out.println("tAffixNo=="+tAffixNo);		
			LLAffixSchema tLLAffixSchema = new LLAffixSchema();
			if(index == 1){
				tLLAffixSchema.setAffixType("07");
		    	tLLAffixSchema.setAffixCode("7000"); //7000-被保险人身份证明 3100-门诊医疗费用收据类  3300-门诊医疗费用清单类	03
		    	LLMAffixDB tLLMAffixDB = new LLMAffixDB();
		    	tLLMAffixDB.setAffixCode("7000");
		    	tLLMAffixDB.setAffixTypeCode("07");
		    	tLLMAffixDB.setManageCome("86");
		    	if(tLLMAffixDB.getInfo())
		    	tLLAffixSchema.setAffixName(tLLMAffixDB.getSchema().getAffixName());
			}else if(index == 2)
			{
				tLLAffixSchema.setAffixType("03");
		    	tLLAffixSchema.setAffixCode("3100"); //7000-被保险人身份证明 3100-门诊医疗费用收据类  3300-门诊医疗费用清单类	03
				LLMAffixDB tLLMAffixDB = new LLMAffixDB();
		    	tLLMAffixDB.setAffixCode("3100");
		    	tLLMAffixDB.setAffixTypeCode("03");
		    	tLLMAffixDB.setManageCome("86");
		    	if(tLLMAffixDB.getInfo())
		    	tLLAffixSchema.setAffixName(tLLMAffixDB.getSchema().getAffixName());
			}else if(index == 3){
				tLLAffixSchema.setAffixType("03");
		    	tLLAffixSchema.setAffixCode("3300"); //7000-被保险人身份证明 3100-门诊医疗费用收据类  3300-门诊医疗费用清单类	03
		    	LLMAffixDB tLLMAffixDB = new LLMAffixDB();
		    	tLLMAffixDB.setAffixCode("3300");
		    	tLLMAffixDB.setAffixTypeCode("03");
		    	tLLMAffixDB.setManageCome("86");
		    	if(tLLMAffixDB.getInfo())
		    	tLLAffixSchema.setAffixName(tLLMAffixDB.getSchema().getAffixName());
			}
			
			tLLAffixSchema.setAffixNo(tAffixNo);
			tLLAffixSchema.setSerialNo(index);
			tLLAffixSchema.setCaseNo(mCaseNo);
			tLLAffixSchema.setRgtNo(mCaseNo);
			tLLAffixSchema.setReasonCode("01");
			
	    	tLLAffixSchema.setShortCount("0");
	    	tLLAffixSchema.setCount("1");
	    	tLLAffixSchema.setSupplyDate(mConfirmDate);
	    	tLLAffixSchema.setMngCom(mManageCom);
	    	tLLAffixSchema.setOperator(mHandler);
	    	tLLAffixSchema.setMakeDate(mCurrentDate);
	    	tLLAffixSchema.setMakeTime(mCurrentTime);
	    	tLLAffixSchema.setModifyDate(mCurrentDate);
	    	tLLAffixSchema.setModifyTime(mCurrentTime);	
	    	tLLAffixSet.add(tLLAffixSchema);
		}		
    	
    	tLLRegisterSchema.setRgtNo(mCaseNo);
    	tLLRegisterSchema.setRgtObj("1");
		tLLRegisterSchema.setRgtObjNo(mGrpContNo);		
		tLLRegisterSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
		tLLRegisterSchema.setAgentCode(mLCPolSchema.getAgentCode());
		tLLRegisterSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
		tLLRegisterSchema.setRgtType("1");
		tLLRegisterSchema.setRgtantName(tLLCaseSchema.getCustomerName());
		tLLRegisterSchema.setRelation("00");								
		tLLRegisterSchema.setIDType(mLCInsuredSchema.getIDType());
		tLLRegisterSchema.setIDNo(mLCInsuredSchema.getIDNo());
		tLLRegisterSchema.setRgtState("13");
		tLLRegisterSchema.setRgtClass("0");		
		tLLRegisterSchema.setTogetherFlag("1");
		tLLRegisterSchema.setRgtDate(tLLCaseSchema.getRgtDate());
		tLLRegisterSchema.setHandler(tLLCaseSchema.getHandler());
		tLLRegisterSchema.setOperator(mHandler);
		tLLRegisterSchema.setMngCom(tLLCaseSchema.getMngCom());
		tLLRegisterSchema.setMakeDate(mCurrentDate);
		tLLRegisterSchema.setMakeTime(mCurrentTime);
		tLLRegisterSchema.setModifyDate(mCurrentDate);
		tLLRegisterSchema.setModifyTime(mCurrentTime);
		
		String tLimit = PubFun.getNoLimit(mManageCom);
		String tCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
		String tSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
		System.out.println("tCaseRelaNo=="+tCaseRelaNo);
		System.out.println("tSubRptNo=="+tSubRptNo);
		tLLCaseRelaSchema.setCaseNo(tLLCaseSchema.getCaseNo());
		tLLCaseRelaSchema.setCaseRelaNo(tCaseRelaNo);
		tLLCaseRelaSchema.setSubRptNo(tSubRptNo);
		
		tLLSubReportSchema.setSubRptNo(tSubRptNo);
		tLLSubReportSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
		tLLSubReportSchema.setCustomerName(tLLCaseSchema.getCustomerName());
		tLLSubReportSchema.setAccidentType("1"); //疾病
		tLLSubReportSchema.setAccDate(mConfirmDate);
		tLLSubReportSchema.setOperator(mHandler);
		tLLSubReportSchema.setMngCom(mManageCom);
		tLLSubReportSchema.setMakeDate(mCurrentDate);
		tLLSubReportSchema.setMakeTime(mCurrentTime);
		tLLSubReportSchema.setModifyDate(mCurrentDate);
		tLLSubReportSchema.setModifyTime(mCurrentTime);

		tLLAppClaimReasonSchema.setReasonCode("01");
		tLLAppClaimReasonSchema.setRgtNo(tLLCaseSchema.getRgtNo());
		tLLAppClaimReasonSchema.setCaseNo(tLLCaseSchema.getCaseNo());
		tLLAppClaimReasonSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
		tLLAppClaimReasonSchema.setReasonType("0");
		tLLAppClaimReasonSchema.setOperator(mHandler);
		tLLAppClaimReasonSchema.setMngCom(tLLCaseSchema.getMngCom());
		tLLAppClaimReasonSchema.setMakeDate(mCurrentDate);
		tLLAppClaimReasonSchema.setMakeTime(mCurrentTime);
		tLLAppClaimReasonSchema.setModifyDate(mCurrentDate);
		tLLAppClaimReasonSchema.setModifyTime(mCurrentTime);
		
		
        return true;
    }
    /**
     * 生成检录理算信息
     * @return boolean
     */
    private boolean inserClaimData()
    {    	 
        tLLCasePolicySchema.setCaseNo(mCaseNo);
        tLLCasePolicySchema.setRgtNo(mCaseNo);
        tLLCasePolicySchema.setCasePolType("0");
        tLLCasePolicySchema.setGrpContNo(mLCPolSchema.getGrpContNo());
        tLLCasePolicySchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
        tLLCasePolicySchema.setContNo(mLCPolSchema.getContNo());
        tLLCasePolicySchema.setPolNo(mLCPolSchema.getPolNo());
        tLLCasePolicySchema.setCaseRelaNo(tLLCaseRelaSchema.getCaseRelaNo());
        tLLCasePolicySchema.setKindCode(mLCPolSchema.getKindCode());
        tLLCasePolicySchema.setRiskVer(mLCPolSchema.getRiskVersion());
        tLLCasePolicySchema.setRiskCode(mLCPolSchema.getRiskCode());
        tLLCasePolicySchema.setPolMngCom(mLCPolSchema.getManageCom());
        tLLCasePolicySchema.setSaleChnl(mLCPolSchema.getSaleChnl());
        tLLCasePolicySchema.setAgentCode(mLCPolSchema.getAgentCode());
        tLLCasePolicySchema.setAgentGroup(mLCPolSchema.getAgentGroup());
        tLLCasePolicySchema.setInsuredNo(mLCPolSchema.getInsuredNo());
        tLLCasePolicySchema.setInsuredName(mLCPolSchema.getInsuredName());
        tLLCasePolicySchema.setInsuredSex(mLCPolSchema.getInsuredSex());
        tLLCasePolicySchema.setInsuredBirthday(mLCPolSchema.getInsuredBirthday());
        tLLCasePolicySchema.setAppntNo(mLCPolSchema.getAppntNo());
        tLLCasePolicySchema.setAppntName(mLCPolSchema.getAppntName());
        tLLCasePolicySchema.setCValiDate(mLCPolSchema.getCValiDate());
        tLLCasePolicySchema.setPolType("1");
        tLLCasePolicySchema.setMngCom(mManageCom);
        tLLCasePolicySchema.setOperator(mHandler);
        tLLCasePolicySchema.setMakeDate(mCurrentDate);
        tLLCasePolicySchema.setMakeTime(mCurrentTime);
        tLLCasePolicySchema.setModifyDate(mCurrentDate);
        tLLCasePolicySchema.setModifyTime(mCurrentTime);
        
        tLLToClaimDutySchema.setPolNo(mLCPolSchema.getPolNo());
        tLLToClaimDutySchema.setGetDutyCode(mLCGetSchema.getGetDutyCode());
        tLLToClaimDutySchema.setCaseNo(mCaseNo);
        tLLToClaimDutySchema.setGetDutyKind(mLCGetSchema.getGetDutyKind());
        tLLToClaimDutySchema.setCaseRelaNo(tLLCaseRelaSchema.getCaseRelaNo());
        tLLToClaimDutySchema.setSubRptNo(tLLSubReportSchema.getSubRptNo());
        tLLToClaimDutySchema.setGrpContNo(mLCPolSchema.getGrpContNo());
        tLLToClaimDutySchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
        tLLToClaimDutySchema.setContNo(mLCPolSchema.getContNo());
        tLLToClaimDutySchema.setKindCode(mLCPolSchema.getKindCode());
        tLLToClaimDutySchema.setRiskCode(mLCPolSchema.getRiskCode());
        tLLToClaimDutySchema.setRiskVer(mLCPolSchema.getRiskVersion());
        tLLToClaimDutySchema.setPolMngCom(mLCPolSchema.getManageCom());
        tLLToClaimDutySchema.setClaimCount("0");
        tLLToClaimDutySchema.setEstClaimMoney("0");
        tLLToClaimDutySchema.setDutyCode(mLCGetSchema.getDutyCode());
        
        tLLClaimSchema.setClmNo(mClmNo);
        tLLClaimSchema.setRgtNo(mCaseNo);
        tLLClaimSchema.setCaseNo(mCaseNo);
        tLLClaimSchema.setGetDutyKind(mLCGetSchema.getGetDutyCode());
        tLLClaimSchema.setClmState("2");
        tLLClaimSchema.setStandPay(mRealPay);
        tLLClaimSchema.setRealPay(mRealPay);
        tLLClaimSchema.setGiveType("1");
        tLLClaimSchema.setGiveTypeDesc("正常给付");
        tLLClaimSchema.setClmUWer(mHandler);
        tLLClaimSchema.setCheckType("0");
        tLLClaimSchema.setMngCom(mManageCom);
        tLLClaimSchema.setOperator(mHandler);
        tLLClaimSchema.setMakeDate(mCurrentDate);
        tLLClaimSchema.setMakeTime(mCurrentTime);
        tLLClaimSchema.setModifyDate(mCurrentDate);
        tLLClaimSchema.setModifyTime(mCurrentTime);
        
        tLLClaimPolicySchema.setClmNo(mClmNo);
        tLLClaimPolicySchema.setCaseRelaNo(tLLCaseRelaSchema.getCaseRelaNo());
        tLLClaimPolicySchema.setRgtNo(mCaseNo);
        tLLClaimPolicySchema.setCaseNo(mCaseNo);
        tLLClaimPolicySchema.setGrpContNo(mLCPolSchema.getGrpContNo());
        tLLClaimPolicySchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
        tLLClaimPolicySchema.setContNo(mLCPolSchema.getContNo());
        tLLClaimPolicySchema.setPolNo(mLCPolSchema.getPolNo());
        tLLClaimPolicySchema.setGetDutyKind(mLCGetSchema.getGetDutyCode());
        tLLClaimPolicySchema.setKindCode(mLCPolSchema.getKindCode());
        tLLClaimPolicySchema.setRiskCode(mLCPolSchema.getRiskCode());
        tLLClaimPolicySchema.setRiskVer(mLCPolSchema.getRiskVersion());
        tLLClaimPolicySchema.setPolMngCom(mLCPolSchema.getManageCom());
        tLLClaimPolicySchema.setSaleChnl(mLCPolSchema.getSaleChnl());
        tLLClaimPolicySchema.setAgentCode(mLCPolSchema.getAgentCode());
        tLLClaimPolicySchema.setAgentGroup(mLCPolSchema.getAgentGroup());
        tLLClaimPolicySchema.setInsuredNo(mLCPolSchema.getInsuredNo());
        tLLClaimPolicySchema.setInsuredName(mLCPolSchema.getInsuredName());
        tLLClaimPolicySchema.setAppntNo(mLCPolSchema.getAppntNo());
        tLLClaimPolicySchema.setAppntName(mLCPolSchema.getAppntName());
        tLLClaimPolicySchema.setCValiDate(mLCPolSchema.getCValiDate());
        tLLClaimPolicySchema.setClmState("1");
        tLLClaimPolicySchema.setStandPay(mRealPay);
        tLLClaimPolicySchema.setRealPay(mRealPay);
        tLLClaimPolicySchema.setPreGiveAmnt("0");
        tLLClaimPolicySchema.setSelfGiveAmnt("0");
        tLLClaimPolicySchema.setRefuseAmnt("0");
        tLLClaimPolicySchema.setApproveAmnt("0");
        tLLClaimPolicySchema.setAgreeAmnt("0");
        tLLClaimPolicySchema.setGiveType("1");
        tLLClaimPolicySchema.setGiveTypeDesc("正常给付");
        tLLClaimPolicySchema.setGiveReason("");
        tLLClaimPolicySchema.setGiveReasonDesc("");
        tLLClaimPolicySchema.setClmUWer("mHandler");
        tLLClaimPolicySchema.setMngCom(mManageCom);
        tLLClaimPolicySchema.setOperator(mHandler);
        tLLClaimPolicySchema.setMakeDate(mCurrentDate);
        tLLClaimPolicySchema.setMakeTime(mCurrentTime);
        tLLClaimPolicySchema.setModifyDate(mCurrentDate);
        tLLClaimPolicySchema.setModifyTime(mCurrentTime);
        
        tLLClaimDetailSchema.setRgtNo(mCaseNo);
        tLLClaimDetailSchema.setClmNo(mClmNo);
        tLLClaimDetailSchema.setPolNo(mLCPolSchema.getPolNo());
        tLLClaimDetailSchema.setGetDutyCode(mLCGetSchema.getGetDutyCode());
        tLLClaimDetailSchema.setGetDutyKind(mLCGetSchema.getGetDutyKind());
        tLLClaimDetailSchema.setCaseNo(mCaseNo);
        tLLClaimDetailSchema.setCaseRelaNo(tLLCaseRelaSchema.getCaseRelaNo());
        tLLClaimDetailSchema.setGrpContNo(mLCPolSchema.getGrpContNo());
        tLLClaimDetailSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
        tLLClaimDetailSchema.setContNo(mLCPolSchema.getContNo());
        tLLClaimDetailSchema.setKindCode(mLCPolSchema.getKindCode());
        tLLClaimDetailSchema.setRiskCode(mLCPolSchema.getRiskCode());
        tLLClaimDetailSchema.setRiskVer(mLCPolSchema.getRiskVersion());
        tLLClaimDetailSchema.setPolMngCom(mLCPolSchema.getManageCom());
        tLLClaimDetailSchema.setSaleChnl(mLCPolSchema.getSaleChnl());
        tLLClaimDetailSchema.setAgentCode(mLCPolSchema.getAgentCode());
        tLLClaimDetailSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
        tLLClaimDetailSchema.setTabFeeMoney(mRealPay);
        tLLClaimDetailSchema.setClaimMoney(mRealPay);
        tLLClaimDetailSchema.setDeclineAmnt("0");
        tLLClaimDetailSchema.setOverAmnt("0");
        tLLClaimDetailSchema.setStandPay(mRealPay);
        tLLClaimDetailSchema.setRealPay(mRealPay);
        tLLClaimDetailSchema.setPreGiveAmnt("0");
        tLLClaimDetailSchema.setSelfGiveAmnt("0");
        tLLClaimDetailSchema.setRefuseAmnt("0");
        tLLClaimDetailSchema.setOtherAmnt("0");
        tLLClaimDetailSchema.setOutDutyAmnt("0");
        tLLClaimDetailSchema.setOutDutyRate("1");
        tLLClaimDetailSchema.setApproveAmnt("0");
        tLLClaimDetailSchema.setAgreeAmnt("0");
        tLLClaimDetailSchema.setDutyCode(mLCGetSchema.getDutyCode());
        tLLClaimDetailSchema.setGiveType("1");
        tLLClaimDetailSchema.setGiveTypeDesc("正常给付");
        tLLClaimDetailSchema.setGiveReason("");
        tLLClaimDetailSchema.setGiveReasonDesc("");
        tLLClaimDetailSchema.setMngCom(mManageCom);
        tLLClaimDetailSchema.setOperator(mHandler);
        tLLClaimDetailSchema.setMakeDate(mCurrentDate);
        tLLClaimDetailSchema.setMakeTime(mCurrentTime);
        tLLClaimDetailSchema.setModifyDate(mCurrentDate);
        tLLClaimDetailSchema.setModifyTime(mCurrentTime);
                
        String strLimit = PubFun.getNoLimit(mManageCom);
	    String tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", strLimit);
	    System.out.println("tGetNoticeNo=="+tGetNoticeNo);
        tLJSGetSchema.setGetNoticeNo(tGetNoticeNo);
        tLJSGetSchema.setOtherNo(mCaseNo);
        tLJSGetSchema.setOtherNoType("5");
        tLJSGetSchema.setManageCom(mManageCom);
        tLJSGetSchema.setAgentCom(mLCPolSchema.getAgentCom());
        tLJSGetSchema.setAgentCode(mLCPolSchema.getAgentCode());
        tLJSGetSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
        tLJSGetSchema.setSumGetMoney(mRealPay);
        tLJSGetSchema.setOperator(mHandler);
        tLJSGetSchema.setMakeDate(mCurrentDate);
        tLJSGetSchema.setMakeTime(mCurrentTime);
        tLJSGetSchema.setModifyDate(mCurrentDate);
        tLJSGetSchema.setModifyTime(mCurrentTime);
        
        LMDutyGetClmDB tLMDutyGetClmDB = new LMDutyGetClmDB();
        tLMDutyGetClmDB.setGetDutyCode(mLCGetSchema.getGetDutyCode());
        tLMDutyGetClmDB.setGetDutyKind(mLCGetSchema.getGetDutyKind());
        if(tLMDutyGetClmDB.getInfo())
        {
            tLJSGetClaimSchema.setFeeFinaType(tLMDutyGetClmDB.getSchema().getStatType());
            tLLClaimDetailSchema.setStatType(tLMDutyGetClmDB.getSchema().getStatType());
        }        
        tLJSGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
        tLJSGetClaimSchema.setFeeOperationType(mLCGetSchema.getGetDutyKind());
        tLJSGetClaimSchema.setOtherNo(mCaseNo);
        tLJSGetClaimSchema.setOtherNoType("5");
        tLJSGetClaimSchema.setGetDutyKind(mLCGetSchema.getGetDutyKind());
        tLJSGetClaimSchema.setGrpContNo(mLCPolSchema.getGrpContNo());
        tLJSGetClaimSchema.setContNo(mLCPolSchema.getContNo());
        tLJSGetClaimSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
        tLJSGetClaimSchema.setPolNo(mLCPolSchema.getPolNo());
        tLJSGetClaimSchema.setKindCode(mLCPolSchema.getKindCode());
        tLJSGetClaimSchema.setRiskCode(mLCPolSchema.getRiskCode());
        tLJSGetClaimSchema.setRiskVersion(mLCPolSchema.getRiskVersion());
        tLJSGetClaimSchema.setAgentCode(mLCPolSchema.getAgentCode());
        tLJSGetClaimSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
        tLJSGetClaimSchema.setPay(mRealPay);
        tLJSGetClaimSchema.setManageCom(mLCPolSchema.getManageCom());
        tLJSGetClaimSchema.setAgentCom(mLCPolSchema.getAgentCom());
        tLJSGetClaimSchema.setOperator(mHandler);
        tLJSGetClaimSchema.setMakeDate(mCurrentDate);
        tLJSGetClaimSchema.setMakeTime(mCurrentTime);
        tLJSGetClaimSchema.setModifyDate(mCurrentDate);
        tLJSGetClaimSchema.setModifyTime(mCurrentTime);
               
        return true;
    }
    /**
     * 生成审批信息
     * @return boolean
     */
    private boolean inseruwData()
    {    	 
    	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    	tLLClaimUserDB.setUserCode(mHandler);
    	if(tLLClaimUserDB.getInfo())
    	{
    		tLLClaimUWMainSchema.setClmUWGrade(tLLClaimUserDB.getSchema().getClaimPopedom());
            tLLClaimUWMainSchema.setAppGrade(tLLClaimUserDB.getSchema().getClaimPopedom());           
            tLLClaimUWMDetailSchema.setClmUWGrade(tLLClaimUserDB.getSchema().getClaimPopedom());
    	    tLLClaimUnderwriteSchema.setClmUWGrade(tLLClaimUserDB.getSchema().getClaimPopedom());
    	    tLLClaimUnderwriteSchema.setAppGrade(tLLClaimUserDB.getSchema().getClaimPopedom());
            tLLClaimUWMDetailSchema.setAppGrade(tLLClaimUserDB.getSchema().getClaimPopedom());
    	}
    	tLLClaimUWMainSchema.setClmNo(mClmNo);
        tLLClaimUWMainSchema.setRgtNo(mCaseNo);
        tLLClaimUWMainSchema.setCaseNo(mCaseNo);
        tLLClaimUWMainSchema.setClmUWer(mHandler);
        
        tLLClaimUWMainSchema.setClmDecision("1");

        tLLClaimUWMainSchema.setAppClmUWer(mHandler);
        tLLClaimUWMainSchema.setAppPhase("0");
        tLLClaimUWMainSchema.setRemark1("门诊特需账单");
        tLLClaimUWMainSchema.setRemark2("同意");
        tLLClaimUWMainSchema.setcheckDecision1("1");
        tLLClaimUWMainSchema.setcheckDecision2("2");
        tLLClaimUWMainSchema.setOperator(mHandler);
        tLLClaimUWMainSchema.setMngCom(mManageCom);
        tLLClaimUWMainSchema.setMakeDate(mCurrentDate);
        tLLClaimUWMainSchema.setMakeTime(mCurrentTime);
        tLLClaimUWMainSchema.setModifyDate(mCurrentDate);
        tLLClaimUWMainSchema.setModifyTime(mCurrentTime);
        
    	tLLClaimUWDetailSchema.setClmNo(mClmNo);
        tLLClaimUWDetailSchema.setCaseRelaNo(tLLCaseRelaSchema.getCaseRelaNo());
        tLLClaimUWDetailSchema.setCaseNo(mCaseNo);
        tLLClaimUWDetailSchema.setClmUWNo("1");
        tLLClaimUWDetailSchema.setGrpContNo(mGrpContNo);
        tLLClaimUWDetailSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
        tLLClaimUWDetailSchema.setContNo(mContNo);
        tLLClaimUWDetailSchema.setPolNo(mPolNo);
        tLLClaimUWDetailSchema.setKindCode(mLCPolSchema.getKindCode());        
        tLLClaimUWDetailSchema.setRiskCode(mLCPolSchema.getRiskCode());
        tLLClaimUWDetailSchema.setPolMngCom(mLCPolSchema.getManageCom());
        tLLClaimUWDetailSchema.setSaleChnl(mLCPolSchema.getSaleChnl());
        tLLClaimUWDetailSchema.setAgentCode(mLCPolSchema.getAgentCode());
        tLLClaimUWDetailSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
        tLLClaimUWDetailSchema.setGetDutyCode(mLCGetSchema.getGetDutyCode());
        tLLClaimUWDetailSchema.setGetDutyKind(mLCGetSchema.getGetDutyKind());
        tLLClaimUWDetailSchema.setStandPay(mRealPay);
        tLLClaimUWDetailSchema.setRealPay(mRealPay);
        tLLClaimUWDetailSchema.setCheckType("0");
        tLLClaimUWDetailSchema.setAppClmUWer(mHandler);

        tLLClaimUWDetailSchema.setClmUWer(mHandler);
        tLLClaimUWDetailSchema.setOperator(mHandler);
        tLLClaimUWDetailSchema.setMngCom(mManageCom);
        tLLClaimUWDetailSchema.setMakeDate(mCurrentDate);
        tLLClaimUWDetailSchema.setMakeTime(mCurrentTime);
        tLLClaimUWDetailSchema.setModifyDate(mCurrentDate);
        tLLClaimUWDetailSchema.setModifyTime(mCurrentTime);
        
	 	tLLClaimUnderwriteSchema.setClmNo(mClmNo);
	    tLLClaimUnderwriteSchema.setCaseRelaNo(tLLCaseRelaSchema.getCaseRelaNo());
	    tLLClaimUnderwriteSchema.setRgtNo(mCaseNo);
	    tLLClaimUnderwriteSchema.setCaseNo(mCaseNo);
	    tLLClaimUnderwriteSchema.setGrpContNo(mGrpContNo);
	    tLLClaimUnderwriteSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
	    tLLClaimUnderwriteSchema.setContNo(mContNo);
	    tLLClaimUnderwriteSchema.setPolNo(mPolNo);
	    tLLClaimUnderwriteSchema.setKindCode(mLCPolSchema.getKindCode());
	    tLLClaimUnderwriteSchema.setRiskCode(mLCPolSchema.getRiskCode());	    
	    tLLClaimUnderwriteSchema.setPolMngCom(mLCPolSchema.getManageCom());
	    tLLClaimUnderwriteSchema.setSaleChnl(mLCPolSchema.getSaleChnl());
	    tLLClaimUnderwriteSchema.setAgentCode(mLCPolSchema.getAgentCode());
	    tLLClaimUnderwriteSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
	    tLLClaimUnderwriteSchema.setInsuredNo(mLCPolSchema.getInsuredNo());
	    tLLClaimUnderwriteSchema.setInsuredName(mLCPolSchema.getInsuredName());
	    tLLClaimUnderwriteSchema.setAppntNo(mLCPolSchema.getAppntNo());
	    tLLClaimUnderwriteSchema.setAppntName(mLCPolSchema.getAppntName());
	    tLLClaimUnderwriteSchema.setCValiDate(mLCPolSchema.getCValiDate());
	    tLLClaimUnderwriteSchema.setStandPay(mRealPay);
	    tLLClaimUnderwriteSchema.setRealPay(mRealPay);
	    tLLClaimUnderwriteSchema.setClmUWer(mHandler);

	    tLLClaimUnderwriteSchema.setClmDecision("1");
	    tLLClaimUnderwriteSchema.setClmDepend("");	    
	    tLLClaimUnderwriteSchema.setCheckType("0");
	    tLLClaimUnderwriteSchema.setAppClmUWer(mHandler);
	    tLLClaimUnderwriteSchema.setAppActionType("3");
	    tLLClaimUnderwriteSchema.setOperator(mHandler);
	    tLLClaimUnderwriteSchema.setMngCom(mManageCom);
	    tLLClaimUnderwriteSchema.setMakeDate(mCurrentDate);
	    tLLClaimUnderwriteSchema.setMakeTime(mCurrentTime);
	    tLLClaimUnderwriteSchema.setModifyDate(mCurrentDate);
	    tLLClaimUnderwriteSchema.setModifyTime(mCurrentTime);
    	    
	    tLLClaimUWMDetailSchema.setClmUWNo("1");
        tLLClaimUWMDetailSchema.setClmNo(mClmNo);
        tLLClaimUWMDetailSchema.setRgtNo(mCaseNo);
        tLLClaimUWMDetailSchema.setCaseNo(mCaseNo);
        tLLClaimUWMDetailSchema.setClmUWer(mHandler);
        
        tLLClaimUWMDetailSchema.setAppClmUWer(mHandler);
        tLLClaimUWMDetailSchema.setAppPhase("0");
        tLLClaimUWMDetailSchema.setAppActionType("3");
        tLLClaimUWMDetailSchema.setOperator(mHandler);
        tLLClaimUWMDetailSchema.setMngCom(mManageCom);
        tLLClaimUWMDetailSchema.setMakeDate(mCurrentDate);
        tLLClaimUWMDetailSchema.setMakeTime(mCurrentTime);
        tLLClaimUWMDetailSchema.setModifyDate(mCurrentDate);
        tLLClaimUWMDetailSchema.setModifyTime(mCurrentTime);
  
        return true;
    }
    /**
     * 修改原门诊信息
     * @return boolean
     */
    private boolean updateHospData()
    {    	      
    	LLHospAccTraceDB tLLHospAccTraceDB = new LLHospAccTraceDB();
    	LLHospAccTraceSet tLLHospAccTraceSet = new LLHospAccTraceSet();
    	tLLHospAccTraceDB.setHospitCode(mHospitCode);
    	tLLHospAccTraceDB.setCustomerNo(mCustomerNo);
    	tLLHospAccTraceDB.setConfirmDate(mConfirmDate);
    	tLLHospAccTraceDB.setConfirmState("1");
    	tLLHospAccTraceDB.setDealState("0");
    	tLLHospAccTraceSet = tLLHospAccTraceDB.query();
    	mLCInsureAccTraceSet = new LCInsureAccTraceSet();
    	mLLFeeMainSet = new LLFeeMainSet();
    	mLLSecurityReceiptSet = new LLSecurityReceiptSet();
    	mLLHospAccTraceSet =  new LLHospAccTraceSet();
    	if(tLLHospAccTraceSet.size()>=1)
    	{
    		for ( int index =1 ; index <=tLLHospAccTraceSet.size();index ++)
    		{
    			LLHospAccTraceSchema mLLHospAccTraceSchema = tLLHospAccTraceSet.get(index);
    			String mHosCase = mLLHospAccTraceSchema.getCaseNo();
    			mLLHospAccTraceSchema.setDealState("1");
    			mLLHospAccTraceSchema.setCaseNo(mCaseNo);
    			mLLHospAccTraceSchema.setModifyDate(mCurrentDate);
    			mLLHospAccTraceSchema.setModifyTime(mCurrentTime);
    			mLLHospAccTraceSet.add(mLLHospAccTraceSchema);
    			
    			LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
    			LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
    			tLCInsureAccTraceDB.setOtherNo(mHosCase);
    			tLCInsureAccTraceDB.setMoneyType("PK");
    			tLCInsureAccTraceDB.setState("temp");
    			tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
    			if(tLCInsureAccTraceSet.size()>=1)
    			{
    				LCInsureAccTraceSchema tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(1).getSchema();
    				tLCInsureAccTraceSchema.setState("0");
    				tLCInsureAccTraceSchema.setOtherNo(mCaseNo);
    				tLCInsureAccTraceSchema.setModifyDate(mCurrentDate);
    				tLCInsureAccTraceSchema.setModifyTime(mCurrentTime);    				
    				mLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);  				    			
    			}
    			
    			LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
    			LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
    			tLLFeeMainDB.setCaseNo(mHosCase);    			
    			tLLFeeMainSet = tLLFeeMainDB.query();
    			if(tLLFeeMainSet.size()>=1)
    			{
    				LLFeeMainSchema tLLFeeMainSchema = tLLFeeMainSet.get(1).getSchema();    				
    				tLLFeeMainSchema.setCaseNo(mCaseNo);
    				tLLFeeMainSchema.setModifyDate(mCurrentDate);
    				tLLFeeMainSchema.setModifyTime(mCurrentTime);
    				mLLFeeMainSet.add(tLLFeeMainSchema);    				    			
    			}
    			
    			LLSecurityReceiptDB tLLSecurityReceiptDB = new LLSecurityReceiptDB();
    			LLSecurityReceiptSet tLLSecurityReceiptSet = new LLSecurityReceiptSet();
    			tLLSecurityReceiptDB.setCaseNo(mHosCase);    			
    			tLLSecurityReceiptSet = tLLSecurityReceiptDB.query();
    			if(tLLSecurityReceiptSet.size()>=1)
    			{
    				LLSecurityReceiptSchema tLLSecurityReceiptSchema = tLLSecurityReceiptSet.get(1).getSchema();    				
    				tLLSecurityReceiptSchema.setCaseNo(mCaseNo);
    				tLLSecurityReceiptSchema.setModifyDate(mCurrentDate);
    				tLLSecurityReceiptSchema.setModifyTime(mCurrentTime);
    				mLLSecurityReceiptSet.add(tLLSecurityReceiptSchema);				
    			}    		
    		}    		
    	}    	
        return true;
    }
    
    /**
     * 获取数据
     * @return boolean
     */
    private boolean getInputData()
    {    	            	                   	
        return true;
    }
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHospCustomerConfirm";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()throws Exception
    {    
    	 LLHospAccTraceDB tLLHospAccTraceDB = new LLHospAccTraceDB();
         LLHospAccTraceSet tLLHospAccTraceSet = new LLHospAccTraceSet();
         tLLHospAccTraceDB.setConfirmState("1");
         tLLHospAccTraceDB.setDealState("0");
         tLLHospAccTraceSet = tLLHospAccTraceDB.query();
         System.out.println("需要进行门诊特需账单合并的案件数==="+tLLHospAccTraceSet.size());
         if(tLLHospAccTraceSet.size()>0)
         {               	
        	for(int index = 1; index <= tLLHospAccTraceSet.size(); index++)
         	{        		
        		 LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        		 LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
        		 LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        		 LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
        		 LLSecurityReceiptDB tLLSecurityReceiptDB = new LLSecurityReceiptDB();
        		 LLSecurityReceiptSet tLLSecurityReceiptSet = new LLSecurityReceiptSet();
        		    
        		 LLHospAccTraceSchema tLLHospAccTraceSchema = tLLHospAccTraceSet.get(index).getSchema();
             	 tLCInsureAccTraceDB.setOtherType("5");
             	 tLCInsureAccTraceDB.setMoneyType("PK");
             	 tLCInsureAccTraceDB.setState("temp");
             	 tLCInsureAccTraceDB.setOtherNo(tLLHospAccTraceSchema.getCaseNo());
             	 tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
             	            	 
             	 tLLFeeMainDB.setCaseNo(tLLHospAccTraceSchema.getCaseNo());
             	 tLLFeeMainSet = tLLFeeMainDB.query();   
             	              	
             	 tLLSecurityReceiptDB.setCaseNo(tLLHospAccTraceSchema.getCaseNo());
             	 tLLSecurityReceiptSet = tLLSecurityReceiptDB.query();
             	 if(tLLFeeMainSet.size()<=0 ||tLCInsureAccTraceSet.size()<=0 ||tLLSecurityReceiptSet.size()<0)
             	 {
             		buildError("checkData()","门诊特需账户轨迹信息或账单信息获取失败！");
                    return false;
             	 }             	 
         	} 	        	
         }else{
        	 System.out.println("没有查询到需要进行门诊特需账单合并的案件==="); 
         } 
         
        return true;
    }
  
    /**
     * 主函数，测试用
     * @param arg String[]
     */
    public static void main(String arg[])
    {    	       
    	LLHospCustomerConfirmTask tLLHospCustomerConfirmTask = new LLHospCustomerConfirmTask();
    	System.out.println("门诊特需批处理开始=================");
    	try{
    		tLLHospCustomerConfirmTask.submitData();
    	}catch (Exception e) {
    		System.out.println("系统未知异常错误!");
            e.printStackTrace();   
        }
        System.out.println("门诊特需批处理结束=================");
    }
}
