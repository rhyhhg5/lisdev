package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJTempFeeExtSchema;
import com.sinosoft.lis.vschema.LJTempFeeExtSet;
import com.sinosoft.lis.ygz.PremSeparateBLExt;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: 价税分离批处理
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author zxs
 * @version 1.0
 */

public class PremSeparateTaskExt extends TaskThread {

	public CErrors mErrors = new CErrors();

	private String tFeeNo = null;

	private String getWhereSql = null;

	private GlobalInput mGI = new GlobalInput();

	private int sysCount = 1000;// 默认每一千条数据发送一次请求

	public PremSeparateTaskExt() {
		mGI.Operator = "server";
		mGI.ComCode = "86";
		mGI.ManageCom = mGI.ComCode;
	}

	public PremSeparateTaskExt(GlobalInput cGlobalInput) {
		mGI = cGlobalInput;
	}

	public void run() {
		getWhereSql = "";
		if (null != tFeeNo) {
			getWhereSql = " and (tempfeeno = '" + tFeeNo + "' or chequeno = '" + tFeeNo + "') ";
		}
		// 提取待分离的数据
		String sqlStr = "select * from ljtempfeeExt " + " where (state='01' or state is null or state = '') "
				+ getWhereSql + " with ur ";
		RSWrapper rsWrapper = new RSWrapper();
		LJTempFeeExtSet tLJTempFeeExtSet = new LJTempFeeExtSet();
		if (!rsWrapper.prepareData(tLJTempFeeExtSet, sqlStr)) {
			System.out.println("价税分离明细数据准备失败！");
			return;
		}
		do {
			rsWrapper.getData();

			LJTempFeeExtSet tExtSet = new LJTempFeeExtSet();
			if (null == tLJTempFeeExtSet || tLJTempFeeExtSet.size() < 1) {
				break;
			}
			int count = 0;
			for (int i = 1; i <= tLJTempFeeExtSet.size(); i++) {
				LJTempFeeExtSchema ExtSchema = tLJTempFeeExtSet.get(i).getSchema();
				ExtSchema.setModifyDate(PubFun.getCurrentDate());
				ExtSchema.setModifyTime(PubFun.getCurrentTime());
				tExtSet.add(ExtSchema);
				count++;
				if (count == sysCount || i == tLJTempFeeExtSet.size()) {
					count = 0;
					try {
						VData tVData = new VData();
						tVData.add(mGI);
						tVData.add(tExtSet);
						PremSeparateBLExt tPremSeparateBLExt = new PremSeparateBLExt();
						if (!tPremSeparateBLExt.getSubmit(tVData, "")) {
							this.mErrors = tPremSeparateBLExt.mErrors;
							System.out.println("价税分离发生错误:" + mErrors.getErrContent());
							tExtSet.clear();
						} else {
							tExtSet.clear();

							MMap tMMap = tPremSeparateBLExt.getResult();
							VData vdata = new VData();
							vdata.add(tMMap);
							PubSubmit tPubSubmit = new PubSubmit();
							if (!tPubSubmit.submitData(vdata, "")) {
								CError cError = new CError();
								cError.moduleName = "InsuAccBala";
								cError.functionName = "run";
								cError.errorMessage = "数据提交失败";
								this.mErrors.addOneError(cError);
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}

		} while (null != tLJTempFeeExtSet && tLJTempFeeExtSet.size() > 0);
		rsWrapper.close();

	}

	public void runOne(String feeNo) {
		tFeeNo = feeNo;
		run();
	}

	public static void main(String[] args) {
		PremSeparateTaskExt task = new PremSeparateTaskExt();
		// task.runOne("1100000100519801");
		task.run();
	}

}
