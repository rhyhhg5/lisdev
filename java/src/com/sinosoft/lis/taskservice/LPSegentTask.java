package com.sinosoft.lis.taskservice;

import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.llcase.*;
import com.sinosoft.utility.ExeSQL;

public class LPSegentTask extends TaskThread {
	
	/** 收件人 */
	private String mAddress = "";
	/** 抄送人 */
	private String mCcAddress = "";
	/** 暗抄 */
	private String mBccAddress = "";
	
	public LPSegentTask(){
	}
	
	
    public void run()
    {
    	Calendar c = Calendar.getInstance();
		int datenum=c.get(Calendar.DATE);
		if(datenum == 1 ){
    	 mAddress="wangyinqi@picchealth.com;zhangkeyun@picchealth.com";
    	 mCcAddress="shiyongle@picchealth.com";
    	 mBccAddress = "zhangqiushi@sinosoft.com.cn;liuyachao14270@sinosoft.com.cn";
        sendDateEmail();
        sendInsuranceEmail();
        sendPclaimEmail();
        sendAgentEmail();
        sendCaseEmail();
        sendInfeeEmail();
		}else{
			System.out.println("今天不是1号，这个批处理不启动");
			return;
		}
    }
    
    /**
     * 获取邮件地址
     *
     */
//    private void getEmailAddress() {
//    	String tSQL1 = "select codealias from ldcode where codetype = 'lpemailaddress' and code = 'address' with ur";
//    	mAddress = new ExeSQL().getOneValue(tSQL1);
//    	String tSQL2 = "select codealias from ldcode where codetype = 'lpemailaddress' and code = 'ccaddress' with ur";
//    	mCcAddress = new ExeSQL().getOneValue(tSQL2);
//    	String tSQL3 = "select codealias from ldcode where codetype = 'lpemailaddress' and code = 'bccaddress' with ur";
//    	mBccAddress = new ExeSQL().getOneValue(tSQL3);
//		
//	}


	/**
     * 理赔infee提数
     *
     */
    private void sendInfeeEmail() {
    	try{
        	//理赔infee提数
        	SegentInfeeDataExtraction tSegentInfeeDataExtraction = new SegentInfeeDataExtraction();
        	tSegentInfeeDataExtraction.setMAddress(mAddress);
        	tSegentInfeeDataExtraction.setMCcAddress(mCcAddress);
        	tSegentInfeeDataExtraction.setMBccAddress(mBccAddress);
        	tSegentInfeeDataExtraction.getSagentData();
        }catch(Exception e)
        {
        	System.out.println("infee提数批处理失败！");
        	e.getMessage();
        }
		
	}

    /**
     * 理赔agent提数
     *
     */
	private void sendAgentEmail() {
		 try{
	        	//理赔agent提数
	        	SegentAgentDataExtraction tSegentAgentDataExtraction = new SegentAgentDataExtraction();
	        	tSegentAgentDataExtraction.setMAddress(mAddress);
	        	tSegentAgentDataExtraction.setMCcAddress(mCcAddress);
	        	tSegentAgentDataExtraction.setMBccAddress(mBccAddress);
	        	tSegentAgentDataExtraction.getSagentData();
	        }catch(Exception e)
	        {
	        	System.out.println("agent提数批处理失败！");
	        	e.getMessage();
	        }
		
	}
	
	/**
	 * 理赔case提数
	 *
	 */
	private void sendCaseEmail() {
		try{
         	//理赔case提数
         	SegentCaseDataExtraction tSegentCaseDataExtraction = new SegentCaseDataExtraction();
         	tSegentCaseDataExtraction.setMAddress(mAddress);
         	tSegentCaseDataExtraction.setMCcAddress(mCcAddress);
         	tSegentCaseDataExtraction.setMBccAddress(mBccAddress);
         	tSegentCaseDataExtraction.getSagentData();
         }catch(Exception e)
         {
         	System.out.println("case提数批处理失败！");
         	e.getMessage();
         }
		
	}

	/**
	 * 理赔pclaim提数
	 *
	 */
	private void sendPclaimEmail() {
		try{
        	//理赔pclaim提数
        	SegentPClaimDataExtraction tSegentPClaimDataExtraction = new SegentPClaimDataExtraction();
        	tSegentPClaimDataExtraction.setMAddress(mAddress);
        	tSegentPClaimDataExtraction.setMCcAddress(mCcAddress);
        	tSegentPClaimDataExtraction.setMBccAddress(mBccAddress);
    		tSegentPClaimDataExtraction.getSagentData();
        }catch(Exception e)
        {
        	System.out.println("pclaim提数批处理失败！");
        	e.getMessage();
        }
		
	}

	/**
	 * insurance提数
	 *
	 */
	private void sendInsuranceEmail() {
		try{
        	//insurance提数
        	SegentInsuranceDataExtraction tSegentInsuranceDataExtraction = new SegentInsuranceDataExtraction();
        	tSegentInsuranceDataExtraction.setMAddress(mAddress);
        	tSegentInsuranceDataExtraction.setMCcAddress(mCcAddress);
        	tSegentInsuranceDataExtraction.setMBccAddress(mBccAddress);
    		tSegentInsuranceDataExtraction.getSagentData();
        }catch(Exception e)
        {
        	System.out.println("insurance提数批处理失败！");
        	e.getMessage();
        }
		
	}

	/**
	 * date提数
	 *
	 */
	private void sendDateEmail() {
		try{
        	//date提数
        	SegentDateDataExtraction tSegentDateDataExtraction = new SegentDateDataExtraction();
        	tSegentDateDataExtraction.setMAddress(mAddress);//收件人
        	tSegentDateDataExtraction.setMCcAddress(mCcAddress);//抄送人
        	tSegentDateDataExtraction.setMBccAddress(mBccAddress);//暗抄人
    		tSegentDateDataExtraction.getSagentData();
        }catch(Exception e)
        {
        	System.out.println("理赔date提数批处理失败！");
        	e.getMessage();
        }
		
	}


	public static void main(String[] args)
    {
    	LPSegentTask tLPSegentTask = new LPSegentTask();
    	tLPSegentTask.run();
    	System.out.println("理赔批处理结束");
    }
}
