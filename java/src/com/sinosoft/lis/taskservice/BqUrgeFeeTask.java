package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.EdorCancelForHasten;
import com.sinosoft.lis.db.LGWorkBoxDB;
import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.finfee.PausePayFeeUI;
import com.sinosoft.task.*;
import com.sinosoft.utility.*;
import java.util.*;
import com.sinosoft.lis.db.LGGroupMemberDB;
import com.sinosoft.lis.db.LCContDB;

/**
 * <p>Title: 保全催收费用服务类</p>
 * <p>Description: 对过期未交费的保全任务进行催收</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: SinoSoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class BqUrgeFeeTask extends TaskThread
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();
    /** 输入数据的容器 */
    private VData mInputData;
    /** 统一更新日期 */

    private GlobalInput tGI = new GlobalInput();

    private String mCurrentDate = PubFun.getCurrentDate();
    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    private GlobalInput mGlobalInput = null;

    private MMap map;

    private ExeSQL tExeSQL = new ExeSQL();
    private SSRS tSSRS ;

    public BqUrgeFeeTask()
    {
    }

    /**
     * 得到所有待办的任务并放入List中
     * parm boolean: true,产生催缴任务； false,产生撤销任务
     * @return List
     */
    private List getTaskList(boolean type)
    {
        List list = new ArrayList();
        tSSRS = new SSRS();
        String sql;

        //查询未交费且没有进行催缴过的保全收费记录
        sql = CreateSql.getHastenSql(type, mGlobalInput);  //查询过期缴费记录
        tSSRS = tExeSQL.execSQL(sql);

        for (int i = 0; i < tSSRS.getMaxRow(); i++)
        {
            list.add(tSSRS.getRowData(i + 1));
        }
        return list;
    }

    /**
    * 支持手动执行待办件超时日处理程序
    * 只能操纵本机构及本机构的下级机构的催缴
    * */
   public void oneCompany(GlobalInput g)
   {
       mGlobalInput = g;
       run();
   }


    /**
     * 执行催收任务
     */
    public void run()
    {
        System.out.println("\n\n\n\n\n\n开始运行保全电话催缴批处理");
        String workNo;
        String[] info;
        List taskList = getTaskList(true);   //产生催缴记录

        //生成催缴任务
        if(mGlobalInput != null)
        {
            tGI = mGlobalInput;
        }
        else
        {
            tGI.Operator = "Server";
            tGI.ComCode = "86";
            tGI.ManageCom = "86";
        }

        System.out.println(taskList.size() + "个需要保全需要催缴");
        for (int i = 0; i < taskList.size(); i++)
        {
            mInputData = new VData();
            map = new MMap();
            info = (String[]) taskList.get(i);

            String workBox
                = CommonBL.getAutodeliverTarget(getSourceComCode(info[0]),
                                                CheckData.HASTEN_RULE, null);
            if(workBox == null)
            {
               CError tError = new CError();
               tError.moduleName = "BqUrgeFeeTask";
               tError.functionName = "run";
               tError.errorMessage = "没有查询到催缴任务转交信箱";
               mErrors.addOneError(tError);
               System.out.println(tError.errorMessage);
               return;
            }


            //若信箱是小组信箱且产生当前催缴任务的缴费记录的保全申请的当前经办人在该小组，
            //则将该催缴任务分配给经办人员
            String realWorkBox = getRealWorkBox(info, workBox);
            if(realWorkBox != null)
            {
                workBox = realWorkBox;
            }

            workNo = CommonBL.createWorkNo();

            createLGWorkSchema(workNo, info, workBox);           //生成工单信息
            createLGWorkTraceSchema(workNo, workBox);   //生成作业历史轨迹
            createLGTraceNodeOpSchema(workNo, workBox); //生成历史节点
            createLGPhoneHastenSchema(workNo, info);    //生成催缴具体信息
            mInputData.add(map);

            //提交操作数据
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN"))
            {
                this.mErrors.addOneError(new CError("生成电话催缴任务时出错",
                                                    "BPUrgeFeeTask", "run"));
                System.out.println("生成电话催缴任务时出错");

                return;
            }

            //本条财务缴费暂停
            LJSPaySchema tLJSPaySchema = new LJSPaySchema();
            VData v = new VData();

            tLJSPaySchema.setGetNoticeNo(info[5]);
            tLJSPaySchema.setOtherNoType("3");  //保全
            v.add(tLJSPaySchema);

            PausePayFeeUI tPausePayFeeUI = new PausePayFeeUI();
            if(!tPausePayFeeUI.submitData(v, "Pause"))
            {
                this.mErrors.copyAllErrors(tPausePayFeeUI.mErrors);
                System.out.println("设置财务缴费暂停时出错: "
                                   + tPausePayFeeUI.mErrors.getErrContent());

                return;
            }
        }

        //发生过催缴且过期的相关记录需要要撤销
        List cancelList = getTaskList(false); //产生撤销记录

        for(int i = 0; i < cancelList.size(); i++)
        {
            info = (String[]) cancelList.get(i);

            //删除保全项目, 删除批单, 删除申请
            VData data = new VData();
            data.add(this.tGI);
            data.add(info[0]);

            EdorCancelForHasten cancel = new EdorCancelForHasten();
            if(!cancel.submitData(data, ""))
            {
                return;
            }
        }

    }

    /**
     * 得到受理edorAcceptNo的保单的管理机构，任选一个保单
     * @param edorAcceptNo String
     * @return String
     */
    private String getSourceComCode(String edorAcceptNo)
    {
        String sql = "  select a.manageCom "
                     + "from LCCont a, LPEdorMain b "
                     + "where a.contNo = b.contNo "
                     + "   and b.edorAcceptNo = '" + edorAcceptNo + "' ";
        String manageCom = new ExeSQL().getOneValue(sql);
        return manageCom;
    }

    //若信箱是小组信箱且产生当前催缴任务的缴费记录的保全申请的当前经办人在该小组，
    //则将该催缴任务分配给经办人员
    private String getRealWorkBox(String[] info, String workBox)
    {
        //判断是否是小组信箱
        LGWorkBoxDB db = new LGWorkBoxDB();
        db.setWorkBoxNo(workBox);
        if(db.getInfo())
        {
            //是小组信箱
            if (db.getOwnerTypeNo().equals("1"))
            {
                //判断当前经办人是否在该小组：
                //通过工单获得经办人，然后查询经办人是否在该小组内，若在则查询并返回个人信箱
                LGWorkDB tLGWorkDB = new LGWorkDB();
                tLGWorkDB.setWorkNo(info[0]);
                if(tLGWorkDB.getInfo())
                {
                    String sql = "select workBoxNo "
                                 + "from LGWorkBox "
                                 + "where ownerNo in "
                                 + "    (select groupNo "
                                 + "    from LGGroupMember "
                                 + "    where memberNo = '"
                                 + tLGWorkDB.getOperator() + "' "
                                 + "    )";
                    ExeSQL e = new ExeSQL();
                    SSRS s = e.execSQL(sql);

                    if(s != null && s.getMaxRow() > 0)
                    {
                        //当前经办人是在该小组
                        if(workBox.equals(s.GetText(1, 1)))
                        {
                            LGWorkBoxDB tLGWorkBoxDB = new LGWorkBoxDB();
                            tLGWorkBoxDB.setOwnerNo(tLGWorkDB.getOperator());
                            if(tLGWorkBoxDB.getInfo())
                            {
                                return tLGWorkBoxDB.getWorkBoxNo();
                            }
                        }
                    }
                }  //判断当前经办人是否在该小组
            }  //是小组信箱
        }

        return null;
    }

    //生成催缴工单
    private void createLGWorkSchema(String workNo,
                                    String[] information,
                                    String workBox)
    {
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        String aWorkNo = workNo;
        System.out.println(aWorkNo);
        tLGWorkSchema.setWorkNo(aWorkNo);
        tLGWorkSchema.setAcceptNo(aWorkNo);
        tLGWorkSchema.setNodeNo("0");
        tLGWorkSchema.setStatusNo("2");
        tLGWorkSchema.setDetailWorkNo(aWorkNo);
        tLGWorkSchema.setCustomerNo(information[1]);
        tLGWorkSchema.setCustomerName(CommonBL.getCustomerName(information[1]));
        tLGWorkSchema.setTypeNo("050013");
        tLGWorkSchema.setDateLimit("5");
        tLGWorkSchema.setAcceptCom(getComCode(workBox));
        tLGWorkSchema.setAcceptDate(mCurrentDate);
        tLGWorkSchema.setRemark("日处理程序生成的电话催缴任务！");
        tLGWorkSchema.setOperator(tGI.Operator);
        tLGWorkSchema.setMakeDate(mCurrentDate);
        tLGWorkSchema.setMakeTime(mCurrentTime);
        tLGWorkSchema.setModifyDate(mCurrentDate);
        tLGWorkSchema.setModifyTime(mCurrentTime);
        tLGWorkSchema.setUniting("0");
        tLGWorkSchema.setCurrDoing("0");

        map.put(tLGWorkSchema, "INSERT");
    }

    //生成历史信息
    private void createLGWorkTraceSchema(String workNo,
            String aWorkBoxNo)
    {
        LGWorkTraceSchema tLGWorkTraceSchema = new LGWorkTraceSchema();
        String aWorkNo = workNo;

        tLGWorkTraceSchema.setWorkNo(aWorkNo);
        tLGWorkTraceSchema.setNodeNo("0");
        tLGWorkTraceSchema.setWorkBoxNo(aWorkBoxNo); //信箱编号
        tLGWorkTraceSchema.setInMethodNo("0"); //新单录入
        tLGWorkTraceSchema.setInDate(mCurrentDate);
        tLGWorkTraceSchema.setInTime(mCurrentTime);
        tLGWorkTraceSchema.setMakeDate(mCurrentDate);
        tLGWorkTraceSchema.setMakeTime(mCurrentTime);
        tLGWorkTraceSchema.setModifyDate(mCurrentDate);
        tLGWorkTraceSchema.setModifyTime(mCurrentTime);
        tLGWorkTraceSchema.setOperator(tGI.Operator);

        map.put(tLGWorkTraceSchema, "INSERT");
    }

    private String getComCode(String workBox)
    {
        LGWorkBoxDB tLGWorkBoxDB = new LGWorkBoxDB();
        tLGWorkBoxDB.setWorkBoxNo(workBox);
        tLGWorkBoxDB.getInfo();
        //个人信箱
        if(tLGWorkBoxDB.getOwnerTypeNo().equals("2"))
        {
            LGGroupMemberDB tLGGroupMemberDB = new LGGroupMemberDB();
            tLGGroupMemberDB.setMemberNo(tLGWorkBoxDB.getOwnerNo());
            return tLGGroupMemberDB.query().get(1).getGroupNo();
        }
        else
        {
            return "";
        }
    }

    //生成历史节点
    private void createLGTraceNodeOpSchema(String workNo,
            String aWorkBox)
    {
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
        String aWorkNo = workNo;

        tLGTraceNodeOpSchema.setWorkNo(aWorkNo);
        tLGTraceNodeOpSchema.setNodeNo("0");
        tLGTraceNodeOpSchema.setOperatorNo("0");
        tLGTraceNodeOpSchema.setOperatorType("0");
        tLGTraceNodeOpSchema.setRemark("自动批注：由日处理程序生成催缴任务。");
        tLGTraceNodeOpSchema.setFinishDate(mCurrentDate);
        tLGTraceNodeOpSchema.setFinishTime(mCurrentTime);
        tLGTraceNodeOpSchema.setOperator(tGI.Operator);
        tLGTraceNodeOpSchema.setMakeDate(mCurrentDate);
        tLGTraceNodeOpSchema.setMakeTime(mCurrentTime);
        tLGTraceNodeOpSchema.setModifyDate(mCurrentDate);
        tLGTraceNodeOpSchema.setModifyTime(mCurrentTime);

        map.put(tLGTraceNodeOpSchema, "INSERT");
    }

    //生成催缴任务信息
    private void createLGPhoneHastenSchema(String workNo,
            String[] information)
    {
        LGPhoneHastenSchema tLGPhoneHastenSchema = new LGPhoneHastenSchema();
        String aWorkNo = workNo;

        tLGPhoneHastenSchema.setWorkNo(aWorkNo);
        tLGPhoneHastenSchema.setTimesNo("0");
        tLGPhoneHastenSchema.setEdorAcceptNo(information[0]);
        tLGPhoneHastenSchema.setCustomerNo(information[1]);
        tLGPhoneHastenSchema.setPayReason("保单服务收费");
        tLGPhoneHastenSchema.setOldPayDate(information[4]);
        if (information[3] == null || information[3].equals(""))
        {
            //原缴费方式为现金
            tLGPhoneHastenSchema.setOldPayMode("1");
        }
        else
        {
            //原缴费方式为银行转帐
            tLGPhoneHastenSchema.setOldPayMode("4");
            tLGPhoneHastenSchema.setOldBankCode(information[2]);
            tLGPhoneHastenSchema.setOldBackAccNo(information[3]);

        }
        tLGPhoneHastenSchema.setRemark("日处理程序生成的电话催缴任务！");
        tLGPhoneHastenSchema.setOperator(tGI.Operator);
        tLGPhoneHastenSchema.setMakeDate(this.mCurrentDate);
        tLGPhoneHastenSchema.setMakeTime(this.mCurrentTime);
        tLGPhoneHastenSchema.setModifyDate(this.mCurrentDate);
        tLGPhoneHastenSchema.setModifyTime(this.mCurrentTime);

        map.put(tLGPhoneHastenSchema, "INSERT");
    }

    public static void main(String arg[])
    {
        BqUrgeFeeTask tBqUrgeFeeTask = new BqUrgeFeeTask();
        GlobalInput g = new GlobalInput();
        g.Operator = "endor";
        g.ComCode = "86";

        tBqUrgeFeeTask.run();
        if (tBqUrgeFeeTask.mErrors.needDealError())
        {
            System.out.println("生成催缴任务失败："
                               + tBqUrgeFeeTask.mErrors.getErrContent());
        }
        else
        {
            System.out.println("成功");
        }
    }
}
