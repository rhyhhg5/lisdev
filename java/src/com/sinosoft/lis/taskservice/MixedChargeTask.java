package com.sinosoft.lis.taskservice;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.SocketException;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.digest.DigestUtils;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class MixedChargeTask extends TaskThread {
	public String mCurrentDate = PubFun.getCurrentDate();
	private BufferedWriter mBufferedWriter;
	private String mURL = "";

	private String mIP = "";
	private String mUserName = "";
	private String mPassword = "";
	private int mPort;
	private String mTransMode = "";

	public MixedChargeTask() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void run() {
		System.out.println("开始提取" + this.mCurrentDate + "日的交叉销售数据,开始时间是"
				+ PubFun.getCurrentTime());
		if (!getMixedAgent()) {
			return;
		}
		System.out.println("提取" + this.mCurrentDate + "日的交叉销售数据完成,完成时间是"
				+ PubFun.getCurrentTime());
	}

	private boolean getMixedAgent() {

		String tSQL = "select SysVarValue from LDSYSVAR where Sysvar='JiaoChaURL'";
		this.mURL = new ExeSQL().getOneValue(tSQL);
//		this.mURL = "D:\\xmlbackup1\\";
		if (mURL == null || mURL.equals("")) {
			System.out.println("没有找到集团交叉销售存储文件夹！");
			return false;
		}

		String tFtpSQL = "select code,codename from ldcode where codetype = 'jtjxftpn' ";
		SSRS tSSRS = new ExeSQL().execSQL(tFtpSQL);
		if (tSSRS == null || tSSRS.MaxRow < 1) {
			System.out.println("没有找到集团交叉ftp配置信息！");
			return false;
		}
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			if ("IP".equals(tSSRS.GetText(i, 1))) {
				mIP = tSSRS.GetText(i, 2);
			} else if ("UserName".equals(tSSRS.GetText(i, 1))) {
				mUserName = tSSRS.GetText(i, 2);
			} else if ("Password".equals(tSSRS.GetText(i, 1))) {
				mPassword = tSSRS.GetText(i, 2);
			} else if ("Port".equals(tSSRS.GetText(i, 1))) {
				mPort = Integer.parseInt(tSSRS.GetText(i, 2));
			} else if ("TransMode".equals(tSSRS.GetText(i, 1))) {
				mTransMode = tSSRS.GetText(i, 2);
			}
		}
		// 健康险应付寿险
	     getS001();
		// 健康险应付财产险
		 getS002();
         
			// 健康险实付寿险
		 getP001();
		// 健康险实付财产险
		 getP002();
		  
		 
		 
		 FtpTransferFiles();
		 try
		 {
		 delter(mURL);
		 } catch (IOException ex)
		 {
		 ex.printStackTrace();
		 }




		return true;

	}
	/**
	 * 修改生成的文件夹名称 （添加MD5后的名称）
	 * @param cFilePath  生成的原文件路径
	 * @param cChargeType 手续费类型
	 * @param cToCompare  发给财或寿类型
	 * @return
	 */
	private boolean changeFileName(String cFilePath,String cChargeType,String cToCompare)
	{
		File tOldFile = new File (cFilePath);
		if (!tOldFile.exists()) {
			System.out.println("原文件夹不存在！");
			return false;// 重命名文件不存在
		}
		String tMD5 ="";
		try {
			FileInputStream tFileInputStream = new FileInputStream(cFilePath);
//			tMD5 = DigestUtils.md5Hex(tFileInputStream.toString()).substring(8,
//					24);
				tMD5 =getMD5(tFileInputStream).substring(8,
						24);
			
			tFileInputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			finally {

		}
		File tNewFile = new File(mURL + "000085"+cChargeType
				+ mCurrentDate.replaceAll("-", "") + tMD5 + cToCompare+".txt");
		if (tNewFile.exists())// 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
		{
			System.out.println("新文件已经存在！");
			return false;// 重命名文件不存在
		}
		boolean flag = tOldFile.renameTo(tNewFile);
		return flag;
	}

	private void getS002() {
			String filename = mURL+"000085F001" + mCurrentDate.replaceAll("-", "")
					+ "000002.txt";
			// FileOutputStream tFileOutputStreamd = new
			// FileOutputStream(filename,true);
			// String MD5=DigestUtils.md5Hex(new
			// FileInputStream("D:/xmlBackup/000085F001"+mCurrentDate.replaceAll("-",
			// "")+"000002.txt")
			// .toString()).substring(8, 24);
			// del(filename);
			String tDate =PubFun.getCurrentDate2().substring(0,4)+"-"+PubFun.getCurrentDate2().substring(4,6)+"-01";
			tDate =  PubFun.calDate(tDate, -1, "M", "");
			String tStartdate = PubFun.calDate(tDate, 9, "D", "");
			
//			String tEndDate = PubFun.calDate(tStartdate, 8, "D", "");
			String tEndDate = PubFun.getCurrentDate();
			tEndDate =  PubFun.calDate(tEndDate, -1, "D", "");
//			String tEndDate = PubFun.getCurrentDate();
//			System.out.println("@@@@@@@@@@@寿险&&&&&&&&");
			System.out.println(tEndDate+"----"+tStartdate);
			String tSQL =
			// 个险承保
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate), 'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ " 'QD' 业务活动代码,'签单' 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lccont where contno = b.contno fetch first 1 rows only ) 代理方机构代码 ,''  代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.othernotype ='BC'and b.transtype ='ZC' and d.agentcom = b.agentcom  and d.actype ='06' and b.grpcontno ='00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.TransState <> 03 and  a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"' and a.managecom like '8621%'    union   "
					+
					// 团险承保
					"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate), 'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,b.grppolno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ " 'QD' 业务活动代码,'签单' 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lcgrpcont where grpcontno = b.grpcontno fetch first 1 rows only ) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno ) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称, b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c,lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.othernotype ='BC'and b.transtype ='ZC'and d.agentcom = b.agentcom and d.actype ='06' and  b.grpcontno <> '00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.transstate <> 03  and a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"'  and a.managecom like '8621%'   union "
					+
					// 个险续期
					"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate), 'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select serialno from ljapayperson where polno = b.polno and payno = b.receiptno and dutycode = b.dutycode  and payplancode = b.payplancode)||b.polno 业务单证ID,ts_fmt(date(a.shoulddate), 'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ " 'XQ' 业务活动代码,'续期' 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lccont where contno = c.contno fetch first 1 rows only ) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn  and b.transtype= 'ZC' and d.agentcom = b.agentcom  and d.actype ='06' and b.grpcontno ='00000000000000000000' and b.payyear>0   and b.branchtype ='5' and b.branchtype2='01' and  a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"'  and a.managecom like '8621%'   union "
					+
//					 团险续期
					" select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate), 'yyyymmdd')  业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, (select serialno from ljapaygrp where grppolno = b.grppolno and payno = b.receiptno) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例, "
					+ " 'XQ'  业务活动代码,'续期' 业务活动名称,"
					+ " a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lcgrpcont where grpcontno = c.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno ) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称, b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b, lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and  c.commisionsn = b.commisionsn  and d.agentcom = b.agentcom  and b.transtype ='ZC' and  d.actype ='06' and b.grpcontno <>'00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.payyear>0 and a.shoulddate   between '"+tStartdate+"' and '"+tEndDate+"'  and a.managecom like '8621%'  union  "
					+
					// 个险保全
					"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno||b.polno from ljagetendorse e where e.polno=b.polno and b.receiptno=e.actugetno and e.feeoperationtype=b.transtype and e.makedate=b.tmakedate  fetch first 1 rows only) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ "b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode=b.transtype  fetch first 1 rows only) 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lccont where contno = c.contno fetch first 1 rows only) 代理方机构代码 , '' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom  and d.actype ='06' and b.branchtype ='5' and b.branchtype2='01' and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and b.grpcontno ='00000000000000000000' and   a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"' and a.managecom like '8621%'  union  "
					+
					// 个险保全追加
					"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno||b.polno from ljagetendorse e where e.polno=b.polno and b.receiptno=e.actugetno and e.feeoperationtype='ZB' and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ "b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode='ZB'  fetch first 1 rows only) 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lccont where contno = c.contno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn  and b.transtype='ZC' and b.transstate='03' and d.agentcom = b.agentcom  and d.actype ='06' and b.branchtype ='5' and b.branchtype2='01'  and b.grpcontno ='00000000000000000000' and   a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"'  and a.managecom like '8621%'  union  "
					+
					// 团险保全
					"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno from ljagetendorse e where e.grppolno=b.grppolno  and b.receiptno=e.actugetno and e.feeoperationtype=b.transtype and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ " b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode=b.transtype  fetch first 1 rows only) 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lcgrpcont where grpcontno = c.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode = b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称,b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom   and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and  d.actype ='06'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000' and  a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"' and a.managecom like '8621%'  union "
					+
					// 团险保全追加
					"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno from ljagetendorse e where e.grppolno=b.grppolno and b.receiptno=e.actugetno and e.feeoperationtype='ZB' and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ " b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode='ZB'  fetch first 1 rows only) 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lcgrpcont where grpcontno = c.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode = b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称,b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom   and b.transtype='ZC' and b.transstate='03' and  d.actype ='06'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000'  and a.shoulddate between '"+tStartdate+"' and '"+tEndDate+"' and a.managecom like '8621%'  ";
			System.out.println(tSQL);
			int start = 1;
			int nCount = 200;
			while (true) {
				SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
				if (tSSRS.getMaxRow() <= 0) {
					new File(filename);
					break;
				}
				String tempString = tSSRS.encode();
				String[] tempStringArr = tempString.split("\\^");
				// 默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
				// mFileWriter = new FileWriter("C:\\000085S001" +
				// mCurrentDate.replaceAll("-", "") +
				// ".txt");
				// System.out.println(mFileWriter.getEncoding());
				// mBufferedWriter = new BufferedWriter(mFileWriter);
				// 指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
				FileOutputStream tFileOutputStream =  null;
				OutputStreamWriter tOutputStreamWriter = null;
				try{
					tFileOutputStream = new FileOutputStream(filename, true);
					tOutputStreamWriter = new OutputStreamWriter(
						tFileOutputStream, "GBK");
				mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					mBufferedWriter.write("D[F001]:(1=0)");
					mBufferedWriter.newLine();
					String t = tempStringArr[i].replaceAll("\\|", "','");
					String[] tArr = t.split("\\,");
					mBufferedWriter.write("I[F001]:'");
					for (int j = 0; j < tArr.length - 1; j++) {
						if(j==12||j==13||j==27){
						     mBufferedWriter.write(tArr[j].substring(1,tArr[j].length()-1) + ",");
						}else{
							mBufferedWriter.write(tArr[j] + ",");
							}
					}
					mBufferedWriter.write(tArr[tArr.length - 1] + "'");
					mBufferedWriter.newLine();
					mBufferedWriter.flush();
				}
				mBufferedWriter.close();
				start += nCount;
			} catch (IOException ex2) {
				System.out.println(ex2.getStackTrace());
			}
			finally
			{
				try {
					tFileOutputStream.close();
					tOutputStreamWriter.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		changeFileName(filename,"F001","000002");
	}

	private void getS001() {
			String filename = mURL+"000085F001" + mCurrentDate.replaceAll("-", "")
					+ "000010.txt";
			
			String tDate =PubFun.getCurrentDate2().substring(0,4)+"-"+PubFun.getCurrentDate2().substring(4,6)+"-01";
			tDate =  PubFun.calDate(tDate, -1, "M", "");
			String tStartdate = PubFun.calDate(tDate, 9, "D", "");
			
//			String tEndDate = PubFun.calDate(tStartdate, 8, "D", "");
			String tEndDate = PubFun.getCurrentDate();
			tEndDate =  PubFun.calDate(tEndDate, -1, "D", "");
//			String tEndDate = PubFun.getCurrentDate();
			System.out.println(tEndDate+"----"+tStartdate);
			
			String tSQL =
			// 个险承保
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode ) 险种名称,b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ " 'QD' 业务活动代码,'签单' 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lccont where contno = b.contno fetch first 1 rows only ) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码, getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.othernotype ='BC'and b.transtype ='ZC' and d.agentcom = b.agentcom  and d.actype ='07' and b.grpcontno ='00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.TransState <> '03' and a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"' and a.managecom like '8621%'    union  "
					+
					// 团险承保
					"select b.commisionsn 佣金id, b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,b.grppolno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ " 'QD' 业务活动代码,'签单' 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lcgrpcont where grpcontno = b.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno ) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称, b.p11 承保标的,c.transmoney 保费收入, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c,lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.othernotype ='BC'and b.transtype ='ZC'and d.agentcom = b.agentcom and d.actype ='07' and  b.grpcontno <> '00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.transstate <> '03' and  a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"'  and a.managecom like '8621%'   union  "
					+
					// 个险续期
					"select b.commisionsn 佣金id, b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select serialno from ljapayperson where polno = b.polno and payno = b.receiptno and dutycode = b.dutycode  and payplancode = b.payplancode)||b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ " 'XQ' 业务活动代码,'续期' 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lccont where contno = c.contno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn  and b.transtype= 'ZC' and d.agentcom = b.agentcom  and d.actype ='07' and b.grpcontno ='00000000000000000000' and b.payyear>0   and b.branchtype ='5' and b.branchtype2='01' and a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"' and a.managecom like '8621%'   union "
					+
					// 团险续期
					" select b.commisionsn 佣金id, b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd')  业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, (select serialno from ljapaygrp where grppolno = b.grppolno and payno = b.receiptno) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例, "
					+ " 'XQ'  业务活动代码,'续期' 业务活动名称,"
					+ " a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lcgrpcont where grpcontno = c.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno ) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称, b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b, lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and  c.commisionsn = b.commisionsn  and d.agentcom = b.agentcom  and b.transtype ='ZC' and  d.actype ='07' and b.grpcontno <>'00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.payyear>0 and a.shoulddate between '"+tStartdate+"' and '"+tEndDate+"' and a.managecom like '8621%'  union  "
					+
					// 个险保全
					"select b.commisionsn 佣金id, b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno||b.polno from ljagetendorse e where e.polno=b.polno and b.receiptno=e.actugetno and e.feeoperationtype=b.transtype and e.makedate=b.tmakedate  fetch first 1 rows only) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ "b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode=b.transtype  fetch first 1 rows only) 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lccont where contno = c.contno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom  and d.actype ='07' and b.branchtype ='5' and b.branchtype2='01' and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and b.grpcontno ='00000000000000000000' and a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"' and a.managecom like '8621%'   union  "
					+
					// 个险保全追加
					"select b.commisionsn 佣金id, b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno||b.polno from ljagetendorse e where e.polno=b.polno and b.receiptno=e.actugetno and e.feeoperationtype='ZB' and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ "b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode='ZB'  fetch first 1 rows only) 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select contno from lccont where contno = c.contno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn  and b.transtype='ZC' and b.transstate='03' and d.agentcom = b.agentcom  and d.actype ='07' and b.branchtype ='5' and b.branchtype2='01'  and b.grpcontno ='00000000000000000000' and a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"' and a.managecom like '8621%'  union  "
					+
					// 团险保全
					"select b.commisionsn 佣金id, b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno from ljagetendorse e where e.grppolno=b.grppolno  and b.receiptno=e.actugetno and e.feeoperationtype=b.transtype and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt((a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ " b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode=b.transtype  fetch first 1 rows only) 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lcgrpcont where grpcontno = c.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode = b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称,b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom   and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and  d.actype ='07'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000' and a.shoulddate   between '"+tStartdate+"' and '"+tEndDate+"' and a.managecom like '8621%'   union "
					+
					// 团险保全追加
					"select b.commisionsn 佣金id, b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno from ljagetendorse e where e.grppolno=b.grppolno and b.receiptno=e.actugetno and e.feeoperationtype='ZB' and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
					+ " b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode='ZB'  fetch first 1 rows only) 业务活动名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "(select grpagentcom from lcgrpcont where grpcontno = c.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode = b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称,b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom   and b.transtype='ZC' and b.transstate='03' and  d.actype ='07'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000' and a.shoulddate   between '"+tStartdate+"' and '"+tEndDate+"' and a.managecom like '8621%'  ";

			System.out.println(tSQL);
			int start = 1;
			int nCount = 200;
			while (true) {
				SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
				if (tSSRS.getMaxRow() <= 0) {
					new File(filename);
					break;
				}
				String tempString = tSSRS.encode();
				String[] tempStringArr = tempString.split("\\^");
				// 默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
				// mFileWriter = new FileWriter("C:\\000085S001" +
				// mCurrentDate.replaceAll("-", "") +
				// ".txt");
				// System.out.println(mFileWriter.getEncoding());
				// mBufferedWriter = new BufferedWriter(mFileWriter);
				// 指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
				FileOutputStream tFileOutputStream = null;
				OutputStreamWriter tOutputStreamWriter = null;
				try
				{
					tFileOutputStream = new FileOutputStream(filename, true);
					tOutputStreamWriter = new OutputStreamWriter(
						tFileOutputStream, "GBK");
				mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					mBufferedWriter.write("D[F001]:(1=0)");
					mBufferedWriter.newLine();
					String t = tempStringArr[i].replaceAll("\\|", "','");
					String[] tArr = t.split("\\,");
					mBufferedWriter.write("I[F001]:'");
					for (int j = 0; j < tArr.length - 1; j++) {
						if(j==12||j==13||j==27){
						     mBufferedWriter.write(tArr[j].substring(1,tArr[j].length()-1) + ",");
						}else{
							mBufferedWriter.write(tArr[j] + ",");
							}
					}
					mBufferedWriter.write(tArr[tArr.length - 1] + "'");
					mBufferedWriter.newLine();
					mBufferedWriter.flush();
				}
				mBufferedWriter.close();
				start += nCount;
			} catch (IOException ex2) {
				System.out.println(ex2.getStackTrace());
			}
			finally
			{
				try {
					tFileOutputStream.close();
					tOutputStreamWriter.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		changeFileName(filename,"F001","000010");
	}

	private void getP001() {

			String filename = mURL+"000085F002" + mCurrentDate.replaceAll("-", "")
					+ "000010.txt";
			String tSQL =
			// 个险承保
			"select b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate), 'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "sum(c.charge) 委托方支付佣金 ,ts_fmt((a.confdate),'yyyymmdd')|| a.modifytime 支付时间, a.actugetno 支付单号,'' 银行凭证号 , ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间 ,'' 支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.confdate is not null and a.othernotype ='BC'and b.transtype ='ZC' and d.agentcom = b.agentcom  and d.actype ='07' and b.grpcontno ='00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.TransState <> '03' and  a.shoulddate  between LAST_DAY(current date - 2 months) + 1 days and LAST_DAY(current date - 1 months)   group by b.contno,b.signdate,b.riskcode , b.polno,a.modifytime,a.shoulddate,a.ActuGetNo,a.confdate,c.chargerate,a.managecom   union  "
					+
					// 团险承保
					"select b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,b.grppolno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "sum(c.charge) 支付佣金, ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付时间,a.actugetno 支付单号,''银行凭证号, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间 ,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c,lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.confdate is not null  and a.othernotype ='BC'and b.transtype ='ZC'and d.agentcom = b.agentcom and d.actype ='07' and  b.grpcontno <> '00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.transstate <> '03' and  a.shoulddate  between LAST_DAY(current date - 2 months) + 1 days and LAST_DAY(current date - 1 months)   group by b.grpcontno,b.signdate,b.riskcode ,a.modifytime, b.grppolno,a.shoulddate,a.ActuGetNo,a.confdate, c.chargerate,a.managecom   union  "
					+
					// 个险续期
					"select b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select serialno from ljapayperson where polno = b.polno and payno = b.receiptno and dutycode = b.dutycode  and payplancode = b.payplancode)||b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付时间 ,a.actugetno 支付单号,''银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间 ,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and a.confdate is not null and  c.commisionsn = b.commisionsn  and b.transtype= 'ZC' and d.agentcom = b.agentcom  and d.actype ='07' and b.grpcontno ='00000000000000000000' and b.payyear>0   and b.branchtype ='5' and b.branchtype2='01' and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and LAST_DAY(current date - 1 months)  union "
					+
					// 团险续期
					" select b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd')  业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID, (select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select serialno from ljapaygrp where grppolno = b.grppolno and payno = b.receiptno) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称, "
					+ " a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,''银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间,''支付批次号  "
					+ "from ljaget a ,lacommision b, lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and a.confdate is not null and  c.commisionsn = b.commisionsn  and d.agentcom = b.agentcom  and b.transtype ='ZC' and  d.actype ='07' and b.grpcontno <>'00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.payyear>0 and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and LAST_DAY(current date - 1 months)  union  "
					+
					// 个险保全
					"select b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno||b.polno from ljagetendorse e where e.polno=b.polno and b.receiptno=e.actugetno and e.feeoperationtype=b.transtype and e.makedate=b.tmakedate  fetch first 1 rows only) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom  and d.actype ='07' and b.branchtype ='5' and b.branchtype2='01' and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and b.grpcontno ='00000000000000000000' and  a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and LAST_DAY(current date - 1 months)  union  "
					+
					// 个险保全追加
					"select b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno||b.polno from ljagetendorse e where e.polno=b.polno and b.receiptno=e.actugetno and e.feeoperationtype='ZB' and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,  ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn  and b.transtype='ZC' and b.transstate='03' and d.agentcom = b.agentcom  and d.actype ='07' and b.branchtype ='5' and b.branchtype2='01'  and b.grpcontno ='00000000000000000000' and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and LAST_DAY(current date - 1 months)  union  "
					+
					// 团险保全
					"select b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno from ljagetendorse e where e.grppolno=b.grppolno  and b.receiptno=e.actugetno and e.feeoperationtype=b.transtype and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and a.confdate is not null  and  d.agentcom = b.agentcom   and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and  d.actype ='07'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000' and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and LAST_DAY(current date - 1 months)  union "
					+
					// 团险保全追加
					"select b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno from ljagetendorse e where e.grppolno=b.grppolno and b.receiptno=e.actugetno and e.feeoperationtype='ZB' and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom   and b.transtype='ZC' and b.transstate='03' and  d.actype ='07'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000' and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and LAST_DAY(current date - 1 months)  ";
			System.out.println(tSQL);
			int start = 1;
			int nCount = 200;
			while (true) {
				SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
				if (tSSRS.getMaxRow() <= 0) {
					new File(filename);
					break;
				}
				String tempString = tSSRS.encode();
				String[] tempStringArr = tempString.split("\\^");
				// 默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
				// mFileWriter = new FileWriter("C:\\000085S001" +
				// mCurrentDate.replaceAll("-", "") +
				// ".txt");
				// System.out.println(mFileWriter.getEncoding());
				// mBufferedWriter = new BufferedWriter(mFileWriter);
				// 指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
				FileOutputStream tFileOutputStream = null;
				OutputStreamWriter tOutputStreamWriter = null;
				try
				{
					tFileOutputStream = new FileOutputStream(filename, true);
					tOutputStreamWriter = new OutputStreamWriter(
						tFileOutputStream, "GBK");
				mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					mBufferedWriter.write("D[F001]:(1=0)");
					mBufferedWriter.newLine();
					String t = tempStringArr[i].replaceAll("\\|", "','");
					String[] tArr = t.split("\\,");
					mBufferedWriter.write("I[F001]:'");
					for (int j = 0; j < tArr.length - 1; j++) {
						if(j==14){
						       mBufferedWriter.write(tArr[j].substring(1,tArr[j].length()-1) + ",");
							}else{
							mBufferedWriter.write(tArr[j] + ",");
							}
					}
					mBufferedWriter.write(tArr[tArr.length - 1] + "'");
					mBufferedWriter.newLine();
					mBufferedWriter.flush();
				}
				mBufferedWriter.close();
				start += nCount;
				tFileOutputStream.close();
				//changeFileName(filename,"F002","000010");
				} catch (IOException ex2) {
					System.out.println(ex2.getStackTrace());
				}
				finally
				{
					try {
						tFileOutputStream.close();
						tOutputStreamWriter.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		
		changeFileName(filename,"F002","000010");
	}

	private void getP002() {

			String filename = mURL+"000085F002" + mCurrentDate.replaceAll("-", "")
					+ "000002.txt";
			String tSQL =
			// 个险承保
			"select b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate), 'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "sum(c.charge) 委托方支付佣金 ,ts_fmt((a.confdate),'yyyymmdd')|| a.modifytime 支付时间, a.actugetno 支付单号,'' 银行凭证号 , ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间 ,'' 支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.confdate is not null and a.othernotype ='BC'and b.transtype ='ZC' and d.agentcom = b.agentcom  and d.actype ='06' and b.grpcontno ='00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.TransState <> '03' and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and current date  group by b.contno,b.signdate,b.riskcode ,a.ActuGetNo, b.polno,a.shoulddate, c.chargerate,a.managecom union  "
					+
					// 团险承保
					"select b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,b.grppolno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "sum(c.charge) 支付佣金, ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付时间,a.actugetno 支付单号,''银行凭证号, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间 ,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c,lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.confdate is not null  and a.othernotype ='BC'and b.transtype ='ZC'and d.agentcom = b.agentcom and d.actype ='06' and  b.grpcontno <> '00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.transstate <> '03' and  a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and current date   group by b.grpcontno,b.signdate,b.riskcode,a.ActuGetNo , b.grppolno,a.shoulddate, c.chargerate,a.managecom  union  "
					+
					// 个险续期
					"select b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select serialno from ljapayperson where polno = b.polno and payno = b.receiptno and dutycode = b.dutycode  and payplancode = b.payplancode)||b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付时间 ,a.actugetno 支付单号,''银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间 ,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and a.confdate is not null and  c.commisionsn = b.commisionsn  and b.transtype= 'ZC' and d.agentcom = b.agentcom  and d.actype ='06' and b.grpcontno ='00000000000000000000' and b.payyear>0   and b.branchtype ='5' and b.branchtype2='01' and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and current date  union "
					+
					// 团险续期
					" select b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd')  业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, (select serialno from ljapaygrp where grppolno = b.grppolno and payno = b.receiptno) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称, "
					+ " a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,''银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间,''支付批次号  "
					+ "from ljaget a ,lacommision b, lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and a.confdate is not null and  c.commisionsn = b.commisionsn  and d.agentcom = b.agentcom  and b.transtype ='ZC' and  d.actype ='06' and b.grpcontno <>'00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.payyear>0 and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and current date  union  "
					+
					// 个险保全
					"select b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno||b.polno from ljagetendorse e where e.polno=b.polno and b.receiptno=e.actugetno and e.feeoperationtype=b.transtype and e.makedate=b.tmakedate  fetch first 1 rows only) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom  and d.actype ='06' and b.branchtype ='5' and b.branchtype2='01' and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and b.grpcontno ='00000000000000000000' and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and current date  union  "
					+
					// 个险保全追加
					"select b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno||b.polno from ljagetendorse e where e.polno=b.polno and b.receiptno=e.actugetno and e.feeoperationtype='ZB' and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,  ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn  and b.transtype='ZC' and b.transstate='03' and d.agentcom = b.agentcom  and d.actype ='06' and b.branchtype ='5' and b.branchtype2='01'  and b.grpcontno ='00000000000000000000' and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and current date  union  "
					+
					// 团险保全
					"select b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno from ljagetendorse e where e.grppolno=b.grppolno  and b.receiptno=e.actugetno and e.feeoperationtype=b.transtype and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and a.confdate is not null  and  d.agentcom = b.agentcom   and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and  d.actype ='06'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000' and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and current date  union "
					+
					// 团险保全追加
					"select b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select e.endorsementno from ljagetendorse e where e.grppolno=b.grppolno and b.receiptno=e.actugetno and e.feeoperationtype='ZB' and e.makedate=b.tmakedate  fetch first 1 rows only ) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
					+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
					+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间,''支付批次号  "
					+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom   and b.transtype='ZC' and b.transstate='03' and  d.actype ='06'and b.branchtype ='5' and b.branchtype2 ='01'  and a.shoulddate between  LAST_DAY(current date - 2 months) + 1 days and current date and   b.grpcontno <>'00000000000000000000' ";
			System.out.println(tSQL);
			int start = 1;
			int nCount = 200;
			while (true) {
				SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
				if (tSSRS.getMaxRow() <= 0) {
					new File(filename);
					break;
				}
				String tempString = tSSRS.encode();
				String[] tempStringArr = tempString.split("\\^");
				// 默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
				// mFileWriter = new FileWriter("C:\\000085S001" +
				// mCurrentDate.replaceAll("-", "") +
				// ".txt");
				// System.out.println(mFileWriter.getEncoding());
				// mBufferedWriter = new BufferedWriter(mFileWriter);
				// 指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
				FileOutputStream tFileOutputStream = null;
				OutputStreamWriter tOutputStreamWriter = null;
				try
				{
					tFileOutputStream = new FileOutputStream(filename, true);
					tOutputStreamWriter = new OutputStreamWriter(
						tFileOutputStream, "GBK");
				mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					mBufferedWriter.write("D[F001]:(1=0)");
					mBufferedWriter.newLine();
					String t = tempStringArr[i].replaceAll("\\|", "','");
					String[] tArr = t.split("\\,");
					mBufferedWriter.write("I[F001]:'");
					for (int j = 0; j < tArr.length - 1; j++) {
						if(j==14){
						       mBufferedWriter.write(tArr[j].substring(1,tArr[j].length()-1) + ",");
							}else{
							mBufferedWriter.write(tArr[j] + ",");
							}
					}
					mBufferedWriter.write(tArr[tArr.length - 1] + "'");
					mBufferedWriter.newLine();
					mBufferedWriter.flush();
				}
				mBufferedWriter.close();
				start += nCount;
				tFileOutputStream.close();
				} catch (IOException ex2) {
					System.out.println(ex2.getStackTrace());
				}
				finally
				{
					try {
						tFileOutputStream.close();
						tOutputStreamWriter.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			changeFileName(filename,"F002","000002");
	}

//	/**
//	 * 删除目录下所有文件和子目录，根目录不删除
//	 * 
//	 * @param filepath
//	 *            String
//	 * @throws IOException
//	 */
//	private void del(String filepath) throws IOException {
//		File f = new File(filepath); // 定义文件路径
//		if (f.exists() && f.isDirectory()) { // 判断是文件还是目录
//			if (f.listFiles().length == 0) { // 若目录下没有文件则直接返回，这里不再删除根目录
//				return;
//			} else { // 若有则把文件放进数组，并判断是否有下级目录
//				File delFile[] = f.listFiles();
//				int i = f.listFiles().length;
//				for (int j = 0; j < i; j++) {
//					if (delFile[j].isDirectory()) {
//						del(delFile[j].getAbsolutePath()); // 递归调用del方法并取得子目录路径
//						System.out.println("SHUACHUWENJIAN");
//					}
//					delFile[j].delete(); // 删除文件，或者递归传入的子目录
//				}
//			}
//		}
//	}
	/**
	 * 删除目录下所有文件和子目录，根目录不删除
	 * 
	 * @param filepath
	 *            String
	 * @throws IOException
	 */
	private void delter(String filepath) throws IOException {
		File f = new File(filepath); // 定义文件路径
		if (f.exists() && f.isDirectory()) { // 判断是文件还是目录
			if (f.listFiles().length == 0) { // 若目录下没有文件则直接返回，这里不再删除根目录
				return;
			} else { // 若有则把文件放进数组，并判断是否有下级目录
				File delFile[] = f.listFiles();
				int i = f.listFiles().length;
				for (int j = 0; j < i; j++) {
					if (delFile[j].isDirectory()) {
						delter(delFile[j].getAbsolutePath()); // 递归调用del方法并取得子目录路径
						System.out.println("SHUACHUWENJIAN");
					}
					if(delFile[j].getName().substring(0, 6).equals("000085")){
						delFile[j].delete();
						} // 删除文件，或者递归传入的子目录
				}
			}
		}
	}  
	/**
	 * 将生成文件传送到10.252.1.112的/home/gather/dat/目录下.
	 */
	private void FtpTransferFiles() {
		FTPTool tFTPTool = new FTPTool(mIP, mUserName, mPassword, mPort,
				mTransMode);
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println(tFTPTool.getErrContent(1));
			} else {
				String[] tFileNames = new File(mURL).list();
				if (tFileNames == null) {
					System.out.println("集团交叉销售Error:获取目录下的文件失败，请检查该目录是否存在！");
				} else {
					for (int i = 0; i < tFileNames.length; i++) {
						System.out.println("文件名："+tFileNames[i]);
						if("000085".equals(tFileNames[i].substring(0, 6))){
							// 寿险报送文件夹/gather/gather/mqm/sales/send/mainData/
							tFTPTool.upload("/gather/gather/mqm/sales/send/mainData/",
									mURL + tFileNames[i]);
							System.out.println("文件上传成功了！");
						}
					}
				}
			}
		} catch (SocketException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			tFTPTool.logoutFTP();
		}
	}


	private void jbInit() throws Exception {
	}

	/** */
	/**
	 * 文件重命名
	 * 
	 * @param path
	 *            文件目录
	 * @param oldname
	 *            原来的文件名
	 * @param newname
	 *            新文件名
	 */
	public boolean renameFile(File cOldFIle, File cNewFile) {
		if (!cOldFIle.exists()) {
			System.out.println("原文件夹不存在！");
			return false;// 重命名文件不存在
		}
		if (cNewFile.exists())// 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
		{
			System.out.println("新文件已经存在！");
			return false;// 重命名文件不存在
		}
		boolean flag = cOldFIle.renameTo(cNewFile);
		return flag;
	}

    public void changeFile(String strPath,String chargeType,String changeType) throws Exception {
        File dir = new File(strPath); 
        File[] files = dir.listFiles();
        String currdate = PubFun.getCurrentDate2();
        String strNewFileName="";
        String tMD5="";
           if (files == null) 
               return; 
           for (int i = 0; i < files.length; i++) {
        	   String strFileName = files[i].getAbsolutePath().toLowerCase();
        	   FileInputStream tFileInputStream = new FileInputStream(strFileName);
      			tMD5 = getMD5(tFileInputStream);
        	   strNewFileName= "000085"+chargeType+currdate+tMD5+changeType+".txt";
        	   
           }
    }
    
    /**
     * 使用文件通道的方式复制文件
     * 
     * @param s
     *            源文件
     * @param t
     *            复制到的新文件
     */
     public void fileChannelCopy(File s, File t) {
         FileInputStream fi = null;
         FileOutputStream fo = null;
         FileChannel in = null;
         FileChannel out = null;
         try {
             fi = new FileInputStream(s);
             fo = new FileOutputStream(t);
             in = fi.getChannel();//得到对应的文件通道
             out = fo.getChannel();//得到对应的文件通道
             in.transferTo(0, in.size(), out);//连接两个通道，并且从in通道读取，然后写入out通道
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             try {
                 fi.close();
                 in.close();
                 fo.close();
                 out.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
     }

     public static String getMD5(InputStream is) throws NoSuchAlgorithmException, IOException {
  		StringBuffer md5 = new StringBuffer();
  		MessageDigest md = MessageDigest.getInstance("MD5");
  		byte[] dataBytes = new byte[1024];
  		
  		int nread = 0; 
  		while ((nread = is.read(dataBytes)) != -1) {
  			md.update(dataBytes, 0, nread);
  		};
  		byte[] mdbytes = md.digest();
  		
  		// convert the byte to hex format
  		for (int i = 0; i < mdbytes.length; i++) {
  			md5.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
  		}
  		return md5.toString();
  	}
  	
  	/**
  	 * 获取该文件的MD5值
  	 * 
  	 * @param file
  	 * @return
  	 * @throws NoSuchAlgorithmException
  	 * @throws IOException
  	 */
  	public static String getMD5(File file) throws NoSuchAlgorithmException, IOException {
  		FileInputStream fis = new FileInputStream(file);
  		return getMD5(fis);
  	}

	/* 测试段 { */
	public static void main(String args[]) {
		MixedChargeTask m = new MixedChargeTask();
		String passwd = null;
		String loginpasswd = null;
		passwd = "20150605"; // 密码明文
		String tDate =PubFun.getCurrentDate2().substring(0,4)+"-"+PubFun.getCurrentDate2().substring(4,6)+"-01";
		tDate =  PubFun.calDate(tDate, -1, "M", "");
		String tStartdate = PubFun.calDate(tDate, 9, "D", "");
		
//		String tEndDate = PubFun.calDate(tStartdate, 8, "D", "");
		String tEndDate = PubFun.getCurrentDate();
		tEndDate =  PubFun.calDate(tEndDate, -1, "D", "");
		System.out.println(tStartdate+"----"+tEndDate);
//		try {
//			m.delter("D:\\xmlbackup1\\");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		m.run();        
		
//		FileInputStream tFileInputStream;                                          
//		try {
//			for(int i =1;i<10;i++){
//				File file = new File ("D:\\Download\\000085F00120160128795dc8fe2dcffcf4000002.txt");
//			tFileInputStream = new FileInputStream(file);
//			String md5 =  getMD5(tFileInputStream);
//			System.out.println(md5.substring(8,24));
////			if("81a8a1434cd8f8f8".equalsIgnoreCase(md5)){
////				System.out.println(i);
////				break;
////			  }
//			}
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (NoSuchAlgorithmException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	/* 测试段 } */
}
