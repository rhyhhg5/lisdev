package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.PolicyAbateDealBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PolicyAbateTask extends TaskThread{
    public PolicyAbateTask()
    {
    }
    public void run()
    {
        GlobalInput mG = new GlobalInput();
        LCContSchema mLCContSchema = new LCContSchema();
        VData mVData = new VData();
        mG.Operator = "task";
        mG.ManageCom = "86";
        mVData.add(mG);


        PolicyAbateDealBL mPolicyAbateDealBL = new PolicyAbateDealBL();
        //失效批处理
        mPolicyAbateDealBL.submitData(mVData,"Available");
        //满期终止批处理
//        mPolicyAbateDealBL.submitData(mVData,"Terminate");
    }
    public static void  main(String[] args)
    {
          PolicyAbateTask mpolicyAbateTask = new PolicyAbateTask();
          mpolicyAbateTask.run();
    }
}
