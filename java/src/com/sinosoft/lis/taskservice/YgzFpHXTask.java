package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.ygz.LJAPayGrpInvoiceDataSend;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YgzFpHXTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	
	public YgzFpHXTask() {

	}
	
	/**
	 * run方法
	 */
	public void run(){
		System.out.println("======================YgzFpTask Execute  !!!======================");
		
		/**
		 * 往同步保单迁出数据中出入日期的值
		 */
		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		tInputData.add(tTransferData);
		
		long startLJAPayGrpInvoiceDataSend = System.currentTimeMillis();
		try {
			/**
			 * 契约已签单团单发票数据提取
			 */
			LJAPayGrpInvoiceDataSend tLJAPayGrpInvoiceDataSend = new LJAPayGrpInvoiceDataSend(); 
			tLJAPayGrpInvoiceDataSend.run();
		} catch (Exception e) {
			System.out.println("提取契约已签单团单发票发生错误！");
			e.printStackTrace();
		}
		long endLJAPayGrpInvoiceDataSend = System.currentTimeMillis();
		System.out.println("========== GrpInvoiceDateSend 契约已签单团单发票数据批处理-耗时： "+((endLJAPayGrpInvoiceDataSend -startLJAPayGrpInvoiceDataSend)/1000/60)+"分钟");

	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		JsflGetTask tJsflGetTask = new JsflGetTask();
		
		tJsflGetTask.run();
	}
}
