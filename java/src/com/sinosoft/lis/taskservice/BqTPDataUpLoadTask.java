package com.sinosoft.lis.taskservice;

import com.sinosoft.httpclient.inf.BqDataUpLoad;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BqTPDataUpLoadTask extends TaskThread {
	public CErrors mErrors = new CErrors();
	private GlobalInput tGI = new GlobalInput();
	private String managecom = "86";
	private String mEdorNo=null;
	private String whereParSql="";
	
	public BqTPDataUpLoadTask(String tEdorAcceptNo,GlobalInput pGI)
	{
		mEdorNo=tEdorAcceptNo;
		tGI=pGI;
		managecom=pGI.ManageCom;
	}
	public BqTPDataUpLoadTask()
	{
		
	}
	
	public void run()
    {
		dealData();
    }
	public boolean dealData()
	{
		//先准备数据
		getInputData();
		System.out.println("#######税优保全数据上传批处理开始#######");
		
		BqDataUpload();
		
		System.out.println("#######税优保全数据上传批处理结束#######");
		
		return true;
       
	}
	private boolean getInputData()
	{
		if(mEdorNo!=null&&!mEdorNo.equals(""))
		{
			whereParSql=" and b.EdorNo='"+mEdorNo+"' ";
		}
		else
		{
			tGI.ComCode="86";
		    tGI.Operator="Server";
		    tGI.ManageCom="86";	
		}
	    return true;
	}
	private void BqDataUpload()
	{
		//sql优化
		String riskSql = "select riskcode from lmriskapp where risktype4 = '4' and TaxOptimal='Y' with ur";
        String riskStr = "";
        SSRS riskSSRS= new ExeSQL().execSQL(riskSql);
        if(null!=riskSSRS && riskSSRS.getMaxRow() > 0){
        	for(int i=1; i <= riskSSRS.getMaxRow(); i++){
        		riskStr +="'"+riskSSRS.GetText(i, 1)+"'";
        		if(i < riskSSRS.getMaxRow()){
        			riskStr +=",";
        		}
        	}
        }
        //税优保单向前置机报送结案日期为T-1的工单数据
	    String ContSql = "select b.EdorNo,b.EdorType,b.ContNo "
	    			+ " from lpedorapp a,lpedoritem b,lccont c,lcpol d"
	    			+ " where a.edoracceptno = b.edorno "
	    			+ " and b.contno=c.contno "
	    			+ " and c.conttype='1' "
	    			+ " and c.appflag='1' "
	    			+ " and c.contno = d.contno "
	    			+ " and d.riskcode in ("+ riskStr +") "
	    			+ " and a.edorstate='0' "
	    			+ " and a.confdate <= current date "
	    			+ " and not exists (select 1 from lserrorlist where transtype ='END001' "
			    	+ " and businessno=a.edoracceptno) "//20170621避免上报失败次数太多，在上报失败解决问题后删除上报失败的记录再自动上报
//	    			+ " and b.edorno='20151207000012' "
	    			+ whereParSql
	    			+ " union "
	    			+ " select b.EdorNo,b.EdorType,b.ContNo "
	    			+ " from lpedorapp a,lpedoritem b,lbcont c,lbpol d"
	    			+ " where a.edoracceptno = b.edorno "
	    			+ " and b.contno=c.contno "
	    			+ " and c.conttype='1' "
	    			+ " and c.contno = d.contno "
	    			+ " and d.riskcode in ("+ riskStr +") "
	    			+ " and a.edorstate='0' "
	    			+ " and a.confdate <= current date "
	    			+ " and not exists (select 1 from lserrorlist where transtype ='END001' "
			    	+ " and businessno=a.edoracceptno ) "
//	    			+ " and b.edorno='20151207000012' "
	    			+ whereParSql
	    			+ " with ur";
					  
	    System.out.println(ContSql);
	    ExeSQL tEexSQL = new ExeSQL();
	    SSRS tSSRS = tEexSQL.execSQL(ContSql);
	    
	    int succCount=0;

	    for(int i=1;i<=tSSRS.getMaxRow();i++)
	    {
	    	String tEdorno=tSSRS.GetText(i, 1);
	    	String tEdorType=tSSRS.GetText(i, 2);
	    	String tContNo=tSSRS.GetText(i, 3);
		    VData tVData = new VData();
		    tVData.add(tGI);
		    //为了验证，把做协议退保的数据维护成转出
		    if("XT".equals(tEdorType)){
		    	updateData(tEdorno,tContNo);
		    	tEdorType="ZC";
		    }
		    
		    TransferData  tTransferData = new TransferData();
		    tTransferData.setNameAndValue("EdorNo",tEdorno);
		    tTransferData.setNameAndValue("EdorType",tEdorType);
		    tTransferData.setNameAndValue("ContNo",tContNo);
		    tVData.add(tTransferData);
		    BqDataUpLoad tBqDataUpLoad=new BqDataUpLoad();
		    tBqDataUpLoad.submitData(tVData, "ZBXPT");
		    
	    }
	    if(succCount==tSSRS.getMaxRow())
	    {
	      System.out.println("昨天工单"+succCount+"个全部上传@@@！");
	    }
	  
	}
	
	private void updateData(String edorno,String contno){
		String str="select 1 from lstransoutinfo a,lbcont b where a.contno=b.contno and a.transoutstate='04' and b.contno='"+contno+"' ";
		String exists=new ExeSQL().getOneValue(str);
		if(null !=exists && !"".equals(exists)){
			String update1="update lpedoritem set edortype='ZC',modifydate=current date,modifytime=current time " +
					" where edorno='"+edorno+"' and contno='"+contno+"' and edortype='XT' ";
			String update2="update lbinsureacctrace set moneytype='ZC' "+
				" where edorno='"+edorno+"' and contno='"+contno+"' and moneytype='XT' ";
			MMap mmap=new MMap();
			mmap.put(update1, "UPDATE");
			mmap.put(update2, "UPDATE");
			VData mdata=new VData();
			mdata.add(mmap);
			PubSubmit tPubSubmit = new PubSubmit();
		    if (!tPubSubmit.submitData(mdata, "")) 
		    {
		         System.out.println("提交数据失败！");
		    }
		}
		
	}
	
	 public static void main(String[] args)
	 {
		 String edorno="20170323001682";
		 GlobalInput GI=new GlobalInput();
		 GI.Operator="server";
		 GI.ManageCom="86";
		 BqTPDataUpLoadTask tBqTPDataUpLoadTask = new BqTPDataUpLoadTask(edorno,GI);
		 tBqTPDataUpLoadTask.run();
	 }
}
