/*
 * <p>ClassName: AdjustAgentBL </p>
 * <p>Description: AdjustAgentBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.taskservice;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnSqlServer;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.*;

public class HuiFang1BL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private String mOperate;
    private String mStartDate;
    private String mEndDate;
    private String mManageCom;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private ReturnVisitTableSet mReturnVisitTableSet=new ReturnVisitTableSet();

    int temp;
    

    /** 业务处理相关变量 */
    

    private MMap mMap=new MMap();

    public HuiFang1BL()
    {
    }

    public static void main(String[] args)
    {
        String as = "2003-11-21";
        System.out.println(PubFun.calDate(as, -1, "M", null));
        String tDay = as.substring(as.lastIndexOf("-") + 1);
        HuiFang1BL tHuiFangBL = new HuiFang1BL();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "000"; //系统自签
        tG.ManageCom = "86"; //默认总公司
        VData tVData=new VData();
        tVData.addElement(tG);
        tVData.addElement("2012-11-1");
        tVData.addElement("2012-11-2");
        tVData.addElement("86371400");
        tHuiFangBL.submitData(tVData, "");
    	
    	
    }


    /**
       * 传输数据的公共方法
       * @param: cInputData 输入的数据
       * cOperate 数据操作
       * @return:
       */
      public boolean submitData(VData cInputData, String cOperate)
      {
          //将操作数据拷贝到本类中
          this.mOperate = cOperate;
          //得到外部传入的数据,将数据备份到本类中
          if (!getInputData(cInputData))
          {
              return false;
          }
          if (!dealData())
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "HuiFangBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败HuiFangBL!";
              this.mErrors.addOneError(tError);
              return false;
          }
          //准备往后台的数据
          if (!prepareOutputData())
          {
              return false;
          }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "HuiFangBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
//        if(!returnLogState())
//    	{
//    		return false;
//    	}
        
          return true;
    }


    /**
        * 从输入数据中得到所有对象
        *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
        */
       private boolean getInputData(VData cInputData)
       {
           mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                   "GlobalInput", 0));
           this.mStartDate = (String) cInputData.getObject(1);
           this.mEndDate = (String) cInputData.getObject(2);
           this.mManageCom=(String)cInputData.getObject(3);
           System.out.println(mStartDate+"  "+mEndDate);
           if (mGlobalInput == null)
           {
               // @@错误处理
               CError tError = new CError();
               tError.moduleName = "HuiFangBL";
               tError.functionName = "getInputData";
               tError.errorMessage = "没有得到足够的信息！";
               this.mErrors.addOneError(tError);
               return false;
           }
           return true;
    }




    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {    
    	DBConnSqlServer tDBConnSqlServer = new DBConnSqlServer();
    	JdbcUrl tJdbcUrl = new JdbcUrl();
    	//调用tJdbcUrl的set方法，略
    	tJdbcUrl.setDBType("SQLSERVER");
    	tJdbcUrl.setIP("10.252.11.9");
    	tJdbcUrl.setPort("1433");
    	tJdbcUrl.setDBName("PI4C");
    	tJdbcUrl.setUser("huifang");
    	tJdbcUrl.setPassWord("12345678");
       
    	
    	tDBConnSqlServer.getConnection(tJdbcUrl);
    	//根据SQL语句获取结果集
    	String sql = "select   "
    		+"ACTIVITYSERIALNO,POLICYNO,BATCHSERIALNO,EXECUTEPHASE,LOCKUSERID,LOCKTIME,CLOSEFLAG,"
    		+"COMPLETETIME,VISITUSERID,VISITPHASE,VISITPHASEMEMO,NEXTCONTACTTIME,CALLCOUNT,"
    		+"PROBLEMCOUNT,BELONGORG,REFERREUSER,FEEDBACKUSER,REFERRETIME,FEEDBACKTIME,HIDEFLG,"
    		+"CANCELFLG,PAD1,PAD2,PAD3,PAD4,PAD5,PAD6  "
    		+"from AM_11_WorkOrder where CompleteTime between '"+mStartDate+"'  and '"+mEndDate+"' "
    		//+" and  VisitPhaseMemo like '1%'"
    		+" and BELONGORG ='"+this.mManageCom+"' "
    		+" union select    "
    		+"ACTIVITYSERIALNO,POLICYNO,BATCHSERIALNO,EXECUTEPHASE,LOCKUSERID,LOCKTIME,CLOSEFLAG,"
    		+"COMPLETETIME,VISITUSERID,VISITPHASE,VISITPHASEMEMO,NEXTCONTACTTIME,CALLCOUNT,"
    		+"PROBLEMCOUNT,BELONGORG,REFERREUSER,FEEDBACKUSER,REFERRETIME,FEEDBACKTIME,HIDEFLG,"
    		+"CANCELFLG,PAD1,PAD2,PAD3,PAD4,PAD5,PAD6  "
    		+"from AM_18_WorkOrder where CompleteTime between '"+mStartDate+"'  and '"+mEndDate+"' "
    		//+" and  VisitPhaseMemo like '1%'"
    		+" and BELONGORG ='"+this.mManageCom+"' "
    		+" union select    "
    		+"ACTIVITYSERIALNO,POLICYNO,BATCHSERIALNO,EXECUTEPHASE,LOCKUSERID,LOCKTIME,CLOSEFLAG,"
    		+"COMPLETETIME,VISITUSERID,VISITPHASE,VISITPHASEMEMO,NEXTCONTACTTIME,CALLCOUNT,"
    		+"PROBLEMCOUNT,BELONGORG,REFERREUSER,FEEDBACKUSER,REFERRETIME,FEEDBACKTIME,HIDEFLG,"
    		+"CANCELFLG,PAD1,PAD2,PAD3,PAD4,PAD5,PAD6  "
    		+"from AM_19_WorkOrder where CompleteTime between '"+mStartDate+"'  and '"+mEndDate+"' "
    		//+" and  VisitPhaseMemo like '1%'"
    		+" and BELONGORG ='"+this.mManageCom+"' "
    		;
    	System.out.println(sql);
    	ResultSet tResultSet = tDBConnSqlServer.getResultSet(sql);
    	if(tResultSet==null){
    		CError tError = new CError();
            tError.moduleName = "HuiFangBL";
            tError.functionName = "dealData";
            tError.errorMessage = "获取结果集过程，连接中断或连接为空。";
            this.mErrors.addOneError(tError);
            return true;
    		//添加错误处理,进行阻断
    	}   
    	/**
    	 * 方案二开始，拼接SQL的方式
    	 * 此方法查询插入数据只是针对业务“一旦保单回访为健康件后就不会再改动数据”
    	 */
    	try {
			while(tResultSet.next()){
				
				String col1 = tResultSet.getString(1);
				String col2 = tResultSet.getString(2);
				System.out.println(col1);
				System.out.println(col2);
				ReturnVisitTableDB tReturnVisitTableDB=new ReturnVisitTableDB();
				//如果回访表里有保单回访成功数据,则直接跳出该保单
				tReturnVisitTableDB.setACTIVITYSERIALNO(tResultSet.getString(1));
				tReturnVisitTableDB.setPOLICYNO(tResultSet.getString(2));
				if(tReturnVisitTableDB.getInfo()){
					System.out.println("保单号："+tResultSet.getString(2)+"已曾成功回访，此次提取忽略");
					continue;
				}
				else{
				ReturnVisitTableSchema tReturnVisitTableSchema = new ReturnVisitTableSchema();
				tReturnVisitTableSchema.setACTIVITYSERIALNO(tResultSet.getString(1));// 序列号
				tReturnVisitTableSchema.setPOLICYNO(tResultSet.getString(2));// 保单号				
				tReturnVisitTableSchema.setBATCHSERIALNO(tResultSet.getString(3));
				tReturnVisitTableSchema.setEXECUTEPHASE(tResultSet.getString(4));
				tReturnVisitTableSchema.setLOCKUSERID(tResultSet.getString(5));
				tReturnVisitTableSchema.setLOCKTIME(tResultSet.getString(6));
				tReturnVisitTableSchema.setCLOSEFLAG(tResultSet.getString(7));					
				tReturnVisitTableSchema.setCOMPLETETIME(AgentPubFun.formatDate(tResultSet.getString(8), "yyyy-MM-dd"));
				System.out.println("保单成功今天回访，回访成功日期为:"+tReturnVisitTableSchema.getCOMPLETETIME());
				tReturnVisitTableSchema.setVISITUSERID(tResultSet.getString(9));
				tReturnVisitTableSchema.setVISITPHASE(tResultSet.getString(10));
				//--VISITPHASEMEMO
				tReturnVisitTableSchema.setVISITPHASEMEMO(tResultSet.getString(11));
				tReturnVisitTableSchema.setNEXTCONTACTTIME(tResultSet.getString(12));
				tReturnVisitTableSchema.setCALLCOUNT(tResultSet.getString(13));
				tReturnVisitTableSchema.setPROBLEMCOUNT(tResultSet.getString(14));
				tReturnVisitTableSchema.setBELONGORG(tResultSet.getString(15));
				tReturnVisitTableSchema.setREFERREUSER(tResultSet.getString(16));
				tReturnVisitTableSchema.setFEEDBACKUSER(tResultSet.getString(17));
				tReturnVisitTableSchema.setREFERRETIME(tResultSet.getString(18));
				tReturnVisitTableSchema.setFEEDBACKTIME(tResultSet.getString(19));
				tReturnVisitTableSchema.setHIDEFLG(tResultSet.getString(20));
				tReturnVisitTableSchema.setCANCELFLG(tResultSet.getString(21));
				tReturnVisitTableSchema.setPAD1(tResultSet.getString(22));
				tReturnVisitTableSchema.setPAD2(tResultSet.getString(23));
				tReturnVisitTableSchema.setPAD3(tResultSet.getString(24));
				tReturnVisitTableSchema.setPAD4(tResultSet.getString(25));
				tReturnVisitTableSchema.setPAD5(tResultSet.getString(26));
				tReturnVisitTableSchema.setPAD6(tResultSet.getString(27));
				String temp_Returnvisitflag =tReturnVisitTableSchema.getVISITPHASEMEMO();
				if(temp_Returnvisitflag!=null)
				{
					if(temp_Returnvisitflag.length()<=1)
					{
						tReturnVisitTableSchema.setRETURNVISITFLAG("9");//标识一下这部分数据
					}else{
						String tempVisitFlag = temp_Returnvisitflag.substring(temp_Returnvisitflag.length()-2, temp_Returnvisitflag.length()-1).trim();					
						if("l".equals(tempVisitFlag))
						{
							System.out.println("保单号为"+tReturnVisitTableSchema.getPOLICYNO()+"回访倒数第二个字段为l,不进行回访数据补提");
							continue;
						}
						tReturnVisitTableSchema.setRETURNVISITFLAG(temp_Returnvisitflag.substring(temp_Returnvisitflag.length()-2, temp_Returnvisitflag.length()-1));
					}
					if(!("1").equals(tReturnVisitTableSchema.getRETURNVISITFLAG())&&(!("4").equals(tReturnVisitTableSchema.getRETURNVISITFLAG())))
					{
						continue;
					}
					temp++;
					tReturnVisitTableSchema.setOPERRATOR(mGlobalInput.Operator);
					tReturnVisitTableSchema.setMAKEDATE(currentDate);
					tReturnVisitTableSchema.setMAKETIME(currentTime);
					tReturnVisitTableSchema.setMODIFYDATE(currentDate);
					tReturnVisitTableSchema.setMODIFYTIME(currentTime);
					mReturnVisitTableSet.add(tReturnVisitTableSchema);
					
					}
	
				}
				
				
			}
			System.out.println("机构编码为"+this.mManageCom+","+this.mStartDate+"回访成功总数为------------"+temp);
			mMap.put(mReturnVisitTableSet, "INSERT");	
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	//各种操作
    	tDBConnSqlServer.closeConnection();
    	
    	
        return true;
    }



    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
          //mMap.put(this.mLAAgentBSet ,"INSERT") ;
          this.mInputData.add(mMap);
          return true;
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HuiFangBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public MMap getMapResult() {
        return mMap;
    }
    /*
     *  // 如果是第一次或者次月更新回访轨迹表时,插入数据
     */
    private boolean prepareLogData()
    {
    	String Returnvisitno = AgentPubFun.formatDate(this.mStartDate,"yyyyMM");
    	System.out.println("开始填充回访数据轨迹表");
    	LAReturnVisitLogDB tLAReturnVisitLogDB = new LAReturnVisitLogDB();
    	tLAReturnVisitLogDB.setReturnVisitNo(Returnvisitno);
    	tLAReturnVisitLogDB.setManageCom(this.mManageCom);
    	tLAReturnVisitLogDB.setStartDate(this.mStartDate);//这个地方 注意
    	tLAReturnVisitLogDB.setEndDate(PubFun.calDate(this.mEndDate, -1, "D", null));//这个地方 注意
    	tLAReturnVisitLogDB.setReturnVisitMonth(AgentPubFun.formatDate(this.mStartDate, "MM"));
    	tLAReturnVisitLogDB.setReturnVisitYear(AgentPubFun.formatDate(this.mStartDate, "yyyy"));
    	tLAReturnVisitLogDB.setOperator(mGlobalInput.Operator);
    	tLAReturnVisitLogDB.setState("11");
    	tLAReturnVisitLogDB.setMakeDate(this.currentDate);
    	tLAReturnVisitLogDB.setMakeTime(this.currentTime);
    	tLAReturnVisitLogDB.setModifyDate(this.currentDate);
    	tLAReturnVisitLogDB.setModifyTime(this.currentTime);
    	if(!tLAReturnVisitLogDB.insert())
    	{
    		return false;
    	}
    	return true;
    }

    private boolean checkData()
    {
    	LAReturnVisitLogDB tLAReturnVisitLogDB = new LAReturnVisitLogDB();
    	String tMonth = AgentPubFun.formatDate(this.mStartDate,"yyyyMM");// 得到本月年月编号
        String tLastMonth = AgentPubFun.formatDate(PubFun.calDate(this.mStartDate, -1, "M", null),"yyyyMM").trim();// 得到上个月年月编号
    	String tLastMonthCheck="select * from lareturnvisitlog where managecom = '"+this.mManageCom+"' "+
 	                   " and returnvisitno='"+tLastMonth+"'";
    	// 查找上个月的回访轨迹
 	   LAReturnVisitLogSet tLAReturnVisitLogSet = new LAReturnVisitLogSet();
 	   tLAReturnVisitLogSet=tLAReturnVisitLogDB.executeQuery(tLastMonthCheck);
 	   if(tLAReturnVisitLogSet.size()==0)
 	    {
 		   if(!dealLog(tMonth))
 		   {
 			   return false;
 		   }
 	    }else
 	    {
 		  for(int j=1;j<=tLAReturnVisitLogSet.size();j++)
 		  {
 			 LAReturnVisitLogSchema tLAReturnVisitLogSchema = new LAReturnVisitLogSchema();
 			 tLAReturnVisitLogSchema= tLAReturnVisitLogSet.get(j);
 			 String tLastEnddate = tLAReturnVisitLogSchema.getEndDate();
 			 String tLastDate = PubFun.calDate(AgentPubFun.formatDate(tMonth+"01", "yyyy-MM-dd"), -1, "D", null);
 			 FDate f_d = new FDate();
 			 // 这个地方注意一下。。。
 			 if(f_d.getDate(tLastEnddate).compareTo(f_d.getDate(tLastDate))<0)
 			 {
 				 this.mStartDate=PubFun.calDate(tLastEnddate, 1, "D", null);
 				 LAReturnVisitLogDB ttLAReturnVisitLogDB = new LAReturnVisitLogDB();
 				 ttLAReturnVisitLogDB.setReturnVisitNo(tLastMonth);
 				 ttLAReturnVisitLogDB.setManageCom(this.mManageCom);
 				 ttLAReturnVisitLogDB.getInfo();
 				 ttLAReturnVisitLogDB.setEndDate(tLastDate);
 				 ttLAReturnVisitLogDB.setOperator(mGlobalInput.Operator);
 				 ttLAReturnVisitLogDB.setModifyDate(this.currentDate);
 				 ttLAReturnVisitLogDB.setModifyTime(this.currentTime);
 				 if(!ttLAReturnVisitLogDB.update())
				  {
						 return false;
			      }
 				 // 插入一条新的记录
 				String tempStartdate = AgentPubFun.formatDate(tMonth+"01", "yyyy-MM-dd");
 		    	LAReturnVisitLogDB tempLAReturnVisitLogDB = new LAReturnVisitLogDB();
 		    	tLAReturnVisitLogDB.setReturnVisitNo(tMonth);
 		    	tLAReturnVisitLogDB.setManageCom(this.mManageCom);
 		    	tLAReturnVisitLogDB.setStartDate(tempStartdate);//这个地方 注意
 		    	tLAReturnVisitLogDB.setEndDate(PubFun.calDate(this.mEndDate, -1, "D", null));//这个地方 注意
 		    	tLAReturnVisitLogDB.setReturnVisitMonth(AgentPubFun.formatDate(PubFun.calDate(this.mEndDate, -1, "D", null), "MM"));
 		    	tLAReturnVisitLogDB.setReturnVisitYear(AgentPubFun.formatDate(PubFun.calDate(this.mEndDate, -1, "D", null), "yyyy"));
 		    	tLAReturnVisitLogDB.setOperator(mGlobalInput.Operator);
 		    	tLAReturnVisitLogDB.setState("11");
 		    	tLAReturnVisitLogDB.setMakeDate(this.currentDate);
 		    	tLAReturnVisitLogDB.setMakeTime(this.currentTime);
 		    	tLAReturnVisitLogDB.setModifyDate(this.currentDate);
 		    	tLAReturnVisitLogDB.setModifyTime(this.currentTime);
 		    	if(!tLAReturnVisitLogDB.insert())
 		    	{
 		    		return false;
 		    	}
 			 }else{
 				 if(!dealLog(tMonth))
 		 		   {
 		 			   return false;
 		 		   }
 			 }
 		  }
         }
 	  return true;
}
    private boolean dealLog(String tMonth)
    {//查询本月回访轨迹
    	 LAReturnVisitLogDB tLAReturnVisitLogDB = new LAReturnVisitLogDB();
		   String tMonthCheck="select * from lareturnvisitlog where managecom = '"+this.mManageCom+"' "+
           " and returnvisitno='"+tMonth+"'";
		  LAReturnVisitLogSet t_LAReturnVisitLogSet = new LAReturnVisitLogSet();
		  t_LAReturnVisitLogSet=tLAReturnVisitLogDB.executeQuery(tMonthCheck);// 查询出本月回访结果
		  if(t_LAReturnVisitLogSet.size()==0)
		  {
			  // 第一次进行回访数据提取
			  if(!prepareLogData())
			  {
				  return false;
			  }
		  }else{
			  for(int i =1;i<=t_LAReturnVisitLogSet.size();i++)
			  {
				 LAReturnVisitLogSchema t_LAReturnVisitLogSchema = new LAReturnVisitLogSchema();
				 t_LAReturnVisitLogSchema = t_LAReturnVisitLogSet.get(i);
				 String tEnddate = t_LAReturnVisitLogSchema.getEndDate();
				 FDate fd = new FDate();
				 if(fd.getDate(tEnddate).compareTo(fd.getDate(PubFun.calDate(this.mStartDate, -1, "D", null)))<=0)
				 {
					 this.mStartDate=PubFun.calDate(tEnddate, 1, "D", null);// 提数起期置为上次提数止期+1天
					 LAReturnVisitLogDB t_LAReturnVisitLogDB = new LAReturnVisitLogDB();
					 t_LAReturnVisitLogDB.setReturnVisitNo(tMonth);
					 t_LAReturnVisitLogDB.setManageCom(this.mManageCom);
					 t_LAReturnVisitLogDB.getInfo();
					 t_LAReturnVisitLogDB.setEndDate(PubFun.calDate(this.mEndDate, -1, "D", null));
					 t_LAReturnVisitLogDB.setOperator(mGlobalInput.Operator);
					 t_LAReturnVisitLogDB.setModifyDate(this.currentDate);
					 t_LAReturnVisitLogDB.setModifyTime(this.currentTime);
					 if(!t_LAReturnVisitLogDB.update())
					 {
						 return false;
					 }
				 } 
			  }
		  }
    	
    	return true;
    }
}