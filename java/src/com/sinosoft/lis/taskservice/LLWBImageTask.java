package com.sinosoft.lis.taskservice;


import com.sinosoft.lis.yibaotong.LLWBTransFtpXmlImage;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *外包扫描件信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class LLWBImageTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();


    String opr = "";
    public LLWBImageTask()
    {}

    public void run()
    {
    	LLWBTransFtpXmlImage  tLLWBTransFtpXmlImage = new LLWBTransFtpXmlImage();
    	
    	if(!tLLWBTransFtpXmlImage.submitData())
         {
    		 System.out.println("扫描件信息导入出问题了");
             System.out.println(mErrors.getErrContent());
             opr="false";
             return;
            
         }else{
        	 System.out.println("扫描件信息导入成功了");
        	 opr="true";
         }

    }

   

   
    public static void main(String[] args)
    {

    	LLWBImageTask tLaHumandeveITask = new LLWBImageTask();
        tLaHumandeveITask.run();
//        tGEdorMJTask.oneCompany(tGlobalInput);
    }
}




