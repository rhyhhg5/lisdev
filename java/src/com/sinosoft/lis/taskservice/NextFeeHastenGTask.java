package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.task.*;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.finfee.PausePayFeeBL;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class NextFeeHastenGTask
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private SSRS mSSRS = null;
    private MMap map = new MMap();
    private int hastenedCont = 0;
    private GlobalInput mGlobalInput = null;

    public NextFeeHastenGTask()
    {
    }

    public NextFeeHastenGTask(GlobalInput tGlobalInput)
    {
        mGlobalInput = tGlobalInput;
    }

    public boolean submit()
    {
        if(!dealData())
        {
            return false;
        }

        if(!commit())
        {
            return false;
        }

        return true;
    }

    /**
     * 处理业务逻辑的方法
     * @return boolean
     */
    private boolean dealData()
    {
        //日处理程序自动运行
        if(mGlobalInput == null)
        {
            mSSRS = getFeeInfo(); //查询应催缴的续期收费信息
        }
        //手动运行
        else
        {
            mSSRS = getOneCompanyFeeInfo();
        }

        if(mSSRS == null || mSSRS.getMaxRow() == 0)
        {
            hastenedCont = 0;
            System.out.println("没有需要催缴的团险续期收费信息");
            return true;
        }
        hastenedCont = mSSRS.getMaxRow();

        //生成催缴任务
        for(int i = 1; i <= 0; i++)
        {
            if(!createTask(mSSRS.getRowData(i)))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 查询应催缴的续期收费信息
     * @return SSRS
     */
    private SSRS getOneCompanyFeeInfo()
    {
        String sql =
                //银行转账
                "  select a.getNoticeNo, b.GrpContNo, "
                + "    '', b.appntNo "
                + "from LJSPay a, LCGrpCont b "
                + "where a.otherNo = b.GrpContNo "
                + "    and b.manageCom like '" + mGlobalInput.ComCode + "%' "
                + "    and a.otherNoType in ('1') "  //团单续期收费
                + "    and b.payMode = '4' "   //银行转帐
                + "    and not exists (select 1 from LGPhoneHasten where getNoticeNo = a.getNoticeNo) "
                + "    and (a.sendBankCount > 1 " //若两次及两次以上转帐都不成功，则生成催收任务；第二次转帐中的情况可由本条件和下面一条件确定
                + "         and a.BankOnTheWayFlag != '1' " //已送银行的记录不能生成催收记录1
                + "         and a.BankOnTheWayFlag != '2' " //生成催收记录的记录会社为暂停状态2
                + "         or days(payDate) < days('"      //超过应交日期
                + PubFun.getCurrentDate() + "'))"
                + "group by a.getNoticeNo, b.GrpContNo, b.appntNo "

                //现金
                + "union "
                + "  select a.getNoticeNo, b.GrpContNo, "
                + "    '', b.appntNo "
                + "from LJSPay a, LCGrpCont b "
                + "where a.otherNo = b.GrpContNo "
                + "    and b.manageCom like '" + mGlobalInput.ComCode + "%' "
                + "    and a.otherNoType in ('1') "  //团单续期收费
                + "    and not exists (select 1 from LGPhoneHasten where getNoticeNo = a.getNoticeNo) "
                + "    and b.payMode != '4' "   //非银行转账;
                + "    and (a.BankOnTheWayFlag != '2' or a.BankOnTheWayFlag is null) " //生成催收记录的记录会设为暂停状态2
                + "    and days(payDate) < days('"
                + PubFun.getCurrentDate() + "') "  //现金收费超期及生成任务
                + "group by a.getNoticeNo, b.GrpContNo, b.appntNo ";
                System.out.println(sql);
        ExeSQL tExeSQL = new ExeSQL();
        return tExeSQL.execSQL(sql);
    }

    /**
     * 日处理程序运行
     * @return SSRS
     */
    private SSRS getFeeInfo()
    {
        String sql =
                //银行转账
                "  select a.getNoticeNo, b.GrpContNo, "
                + "    '', b.appntNo "
                + "from LJSPay a, LCGrpCont b "
                + "where a.otherNo = b.GrpContNo "
                + "    and a.otherNoType in ('1') "  //团单续期收费
                + "    and b.payMode = '4' "   //银行转帐
                + "    and (a.sendBankCount > 1 " //若两次及两次以上转帐都不成功，则生成催收任务；第二次转帐中的情况可由本条件和下面一条件确定
                + "         and a.BankOnTheWayFlag != '1' " //已送银行的记录不能生成催收记录1
                + "         and a.BankOnTheWayFlag != '2' " //生成催收记录的记录会社为暂停状态2
                + "         or days(payDate) < days('"      //超过应交日期
                + PubFun.getCurrentDate() + "')) " 
                + "    and not exists (select 1 from ljspayb where getnoticeno=a.getnoticeno and dealstate='4') "
                + "group by a.getNoticeNo, b.GrpContNo, b.appntNo "

                //现金
                + "union "
                + "  select a.getNoticeNo, b.GrpContNo, "
                + "    '', b.appntNo "
                + "from LJSPay a, LCGrpCont b "
                + "where a.otherNo = b.GrpContNo "
                + "    and a.otherNoType in ('1') "  //团单续期收费
                + "    and b.payMode != '4' "   //非银行转账;
                + "    and (a.BankOnTheWayFlag != '2' or a.BankOnTheWayFlag is null) " //生成催收记录的记录会设为暂停状态2
                + "    and days(payDate) < days('"
                + PubFun.getCurrentDate() + "') "  //现金收费超期及生成任务
                + "    and not exists (select 1 from ljspayb where getnoticeno=a.getnoticeno and dealstate='4') "
                + "group by a.getNoticeNo, b.GrpContNo, b.appntNo ";
                System.out.println(sql);

        ExeSQL tExeSQL = new ExeSQL();
        return tExeSQL.execSQL(sql);
    }

    /**
     * 生成催缴任务
     * @param feeInfo String[]
     * @return boolean
     */
    private boolean createTask(String[] feeInfo)
    {
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("getNoticeNo", feeInfo[0]);
        tTransferData.setNameAndValue("grpContNo", feeInfo[1]);
        tTransferData.setNameAndValue("customerNo", feeInfo[3]);

        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(mGlobalInput);

        TaskInput t = new TaskInputNextFeeG();
        MMap m = t.getSubmitMap(tVData, "");
        if(m == null)
        {
            hastenedCont = 0;
            mErrors.copyAllErrors(t.mErrors);
            return false;
        }

        map.add(m);

        return true;
    }

    /**
     * 财务缴费暂停
     * @param getNoticeNo String
     * @return boolean
     */
    private boolean setPayFeePause(String getNoticeNo)
    {
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        VData v = new VData();

        tLJSPaySchema.setGetNoticeNo(getNoticeNo);
        tLJSPaySchema.setOtherNoType("3"); //保全
        v.add(tLJSPaySchema);

        PausePayFeeBL tPausePayFeeBL = new PausePayFeeBL();
        if (!tPausePayFeeBL.submitData(v, "Pause"))
        {
            mErrors.copyAllErrors(tPausePayFeeBL.mErrors);
            System.out.println("设置财务缴费暂停时出错: "
                               + tPausePayFeeBL.mErrors.getErrContent());

            hastenedCont = 0;
            return false;
        }

        return true;
    }

    /**
     * 提交操作生成数据
     * @return boolean
     */
    private boolean commit()
    {
        VData tVData = new VData();
        tVData.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(tVData, "INSRET||MAIN"))
        {
            hastenedCont = 0;
            mErrors.copyAllErrors(p.mErrors);
            return false;
        }

        //财务缴费暂停
        for(int i = 1; i <= mSSRS.getMaxRow(); i++)
        {
            if(!setPayFeePause(mSSRS.GetText(i, 1)))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 得到成功催缴的团单续期催缴数目
     * @return int
     */
    public int getHastenedCont()
    {
        return hastenedCont;
    }

    public static void main(String[] args)
    {
        NextFeeHastenGTask task = new NextFeeHastenGTask();
        task.submit();
    }
}
