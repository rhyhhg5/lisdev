package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.tb.SendManageBL;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class SendManageTask extends TaskThread {

	public void run() {
		System.out.println("--短信发送批处理开始--");
		sendManage();
		System.out.println("--短信发送批处理完毕--");
	}

	/**
	 * 发送短信
	 */
	private void sendManage() {
		
		ExeSQL tExeSQL = new ExeSQL();
		// ldcode配置中的
		// othersign可以标识上午发还是下午发 1-上午发 2-下午发
		// code为短信类型，会通过该类型查找配置文件 SMS_短信类型.xml
		SSRS tSSRS = tExeSQL
				.execSQL("select code from ldcode where codetype='sms_send' and othersign=(case when current time < time('12:00:00') then '1' else '2' end)");
		for (int index = 1; index <= tSSRS.getMaxRow(); index++) {
			try {
				SendManageBL tSendManageBL = new SendManageBL();
				tSendManageBL.submitData(tSSRS.GetText(index, 1));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public static void main(String[] arr) {
		SendManageTask tSendManageTask = new SendManageTask();
		tSendManageTask.run();
	}
}
