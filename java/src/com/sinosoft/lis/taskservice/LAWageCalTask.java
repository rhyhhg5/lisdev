package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.agentcalculate.AgentWageCalDoUI;
import com.sinosoft.lis.agentcalculate.AgentWageCalSaveUI;
import com.sinosoft.lis.agentwages.AgentWageGatherUI;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: SinoSoft</p>
 * @author miaoxiangzheng
 * @version 1.0
 */

public class LAWageCalTask extends TaskThread {
	private GlobalInput mGlobalInput = null;
	private String mWageNo=null;
	private String mBranchtype=null;
	private String mBranchtype2=null;
	private String mManageCom=null;
	private String mOtherSign=null;
	private StringBuffer tSB=new StringBuffer();
    public LAWageCalTask() {
    	
    }

    public void run() {
        System.out.println("");
        System.out.println(
                "＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
        System.out.println("参数个数:" + mParameters.size());
        System.out.println("");
        if(!checkDay()){
        	return ;
        }
        initVariables();
        if(!checkWageCal()){
        	return ;
        }
        if(!doWageCal()){
        	return ;
        }
        returnState();
    }
    
    private boolean checkDay(){
    	String tResult="";
    	ExeSQL tExeSQL=new ExeSQL();
    	tSB.delete(0,tSB.length());
    	tSB.append("select distinct 1 from ldcode where codetype='calwagetask' ")
        	.append("and comcode=substr(date_format(current date,'yyyymmdd'),5,8) and othersign <>'1' ")
        	;
    	tResult=tExeSQL.getOneValue(tSB.toString());
    	if(tResult!=null&&tResult.equals("1")){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    private void initVariables(){
    	LDCodeDB tLDCodeDB=new LDCodeDB();
    	ExeSQL tExeSQL=new ExeSQL();
    	SSRS tSSRS=new SSRS();
    	tSB.delete(0,tSB.length());
    	tSB.append("select code,othersign from ldcode where codetype='calwagetask' ")
        	.append("and comcode=substr(date_format(current date,'yyyymmdd'),5,8) and othersign<>'1' ")
        	.append("order by codealias fetch first 1 rows only");
    	tSSRS=tExeSQL.execSQL(tSB.toString());
        this.mManageCom=tSSRS.GetText(1,1);
        this.mOtherSign=tSSRS.GetText(1,2);
        tLDCodeDB.setCode(this.mManageCom);
        tLDCodeDB.setCodeType("calwagetask");
        tLDCodeDB.getInfo();
        tLDCodeDB.setOtherSign("1");
        tLDCodeDB.update();
    	mGlobalInput = new GlobalInput();
        mGlobalInput.Operator="000";
        mGlobalInput.ManageCom="86";
        tSB.delete(0,tSB.length());
        tSB.append("select date_format(current date - 1 month,'yyyymm') from dual");
        this.mWageNo=tExeSQL.getOneValue(tSB.toString());
        this.mBranchtype="1";
        this.mBranchtype2="01";
    }
    
    private boolean checkWageCal(){
    	ExeSQL tExeSQL=new ExeSQL();
    	String tResult="";
    	tSB.delete(0,tSB.length());
        tSB.append("select distinct 1 from lawagehistory where managecom='")
        	.append(this.mManageCom)
        	.append("' and wageno=char(date_format(current date - 1 month,'yyyymm')) and branchtype='")
        	.append(this.mBranchtype)
        	.append("' and branchtype2='")
        	.append(this.mBranchtype2)
        	.append("' and state<>'11'");
        tResult=tExeSQL.getOneValue(tSB.toString());
        if(tResult!=null&&tResult.equals("1")){
        	return false;
        }
    	return true;
    }
    
    private boolean doWageCal()
    {
    	AgentWageGatherUI tAgentWageGatherUI =new AgentWageGatherUI();
    	VData tVData =new VData();
    	tVData.add(this.mManageCom);
    	tVData.add(this.mBranchtype);
    	tVData.add(this.mBranchtype2);
    	tVData.add(this.mWageNo);
    	tVData.add(this.mGlobalInput);
    	return tAgentWageGatherUI.submitData(tVData);
    }
    
    private void returnState(){
    	ExeSQL tExeSQL=new ExeSQL();
    	String tResult="";
    	tSB.delete(0,tSB.length());
        tSB.append("select substr(date_format(current date + 1 month,'yyyymmdd'),5,8) from dual");
        tResult=tExeSQL.getOneValue(tSB.toString());
    	LDCodeDB tLDCodeDB=new LDCodeDB();
    	tLDCodeDB.setCode(this.mManageCom);
        tLDCodeDB.setCodeType("calwagetask");
        tLDCodeDB.getInfo();
        tLDCodeDB.setComCode(tResult);
        tLDCodeDB.setOtherSign(this.mOtherSign);
        tLDCodeDB.update();
    }
}
