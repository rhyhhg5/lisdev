package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class LLWBCorePast extends TaskThread {

	private ExeSQL mExeSQL = new ExeSQL();

	// TransInfo 标识信息
	private Element tPACKET = null;
	
    //TransInfo 标识信息
	private Element tHEAD = null;
	
    //TransInfo 标识信息
	private Element tBODY = null;

	// 案件
	private Element tGrpContInfo = null;

	// 案件
	private Element tContInfos = null;

	
	// 案件信息SQL
	private String mGrpContInfoSQL = "";



	// 案件信息SSRS
	private SSRS tGrpContInfoSSRS = null;



	// 案件数
	private int GrpContInfolen = 0;


	
	/** 应用路径 */
	private String mURL = "";

	/** 文件路径 */
	private String mFilePath = "";

	// 执行任务
	public void run() {
		
		getInputData();
		
		dealData();
	}
	
	private void getInputData() {

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);
//		mURL = "E:/理赔报文/理赔外包/";
		mURL = mURL + "vtsfile/";

		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}

	}

	public boolean dealData() {

		String tSQL="Select Code From Ldcode  Where Codetype = 'LPOutSou'  ";
			SSRS tGRPcontnoSSRS = mExeSQL.execSQL(tSQL);
			int tGRPcontnolen = tGRPcontnoSSRS.getMaxRow();
			for(int x = 1; x <= tGRPcontnolen; x++){
		mGrpContInfoSQL = "select  a.grpcontno,a.caseno,a.rgtno,a.getdutykind,  " +
				" (select codename from ldcode where codetype='getdutykind' and code=a.getdutykind fetch first row only), " +
				" a.getdutycode,(select getdutyname from lmdutygetclm where getdutycode=a.getdutycode fetch first row only ), " +
				" b.customerno,a.contno,b.customername,b.idtype,b.idno, " +
				" (select max(accdate) from LLSubReport where subrptno in( select subrptno from llcaserela where caseno=b.caseno))," +
				" a.riskcode,(select riskname from lmriskapp where riskcode=a.riskcode) , " +
				" (select DiseaseCode from llcasecure where caseno =b.caseno fetch first row only), " +
				" (select DiseaseName from llcasecure where caseno =b.caseno fetch first row only), " +
				" sum(a.realpay),a.GiveType,a.GiveTypeDesc,sum(a.OutDutyAmnt),b.endcasedate " +
				" from  llclaimdetail a,llcase b where a.caseno=b.caseno  " +
				" and a.grpcontno ='"+tGRPcontnoSSRS.GetText(x, 1)+"' " +
				" and  b.rgtstate in('11','12') " +
//				" and a.rgtno ='P9500171027000001'" +
				" group by a.grpcontno,a.caseno,a.rgtno,a.getdutykind, a.getdutycode,b.customerno, " +
				" a.contno,b.customername,b.idtype,b.idno,a.GiveType,a.GiveTypeDesc,b.endcasedate,b.caseno,a.riskcode " 
//				" fetch first 10 rows only"
				;

		tGrpContInfoSSRS = mExeSQL.execSQL(mGrpContInfoSQL);
		GrpContInfolen = tGrpContInfoSSRS.getMaxRow();

		if (GrpContInfolen > 0) {
		

		tPACKET = new Element("PACKET");
		Document Doc = new Document(tPACKET);
		tPACKET.addAttribute("type", "REQUEST");
		tPACKET.addAttribute("version", "1.0");
		tHEAD = new Element("HEAD");
		tHEAD.addContent(new Element("REQUEST_TYPE").setText("WB04"));
		tHEAD.addContent(new Element("TRANSACTION_NUM").setText("WB0486950000"+PubFun.getCurrentDate2()+"0000000001"));
		tPACKET.addContent(tHEAD);
		tBODY = new Element("BODY");
		tContInfos = new Element("LLCASELIST");
		
		for (int i = 1; i <= GrpContInfolen; i++) {
			
			tGrpContInfo = new Element("LLCASE_DATA");

			tGrpContInfo.addContent(new Element("Grpcontno")
					.setText(tGrpContInfoSSRS.GetText(i, 1)));
			tGrpContInfo.addContent(new Element("Caseno")
					.setText(tGrpContInfoSSRS.GetText(i, 2)));
			tGrpContInfo.addContent(new Element("Rgtno")
					.setText(tGrpContInfoSSRS.GetText(i, 3)));
			tGrpContInfo.addContent(new Element("Getdutykind")
					.setText(tGrpContInfoSSRS.GetText(i, 4)));
			tGrpContInfo.addContent(new Element("Getdutykindname")
					.setText(tGrpContInfoSSRS.GetText(i, 5)));
			tGrpContInfo.addContent(new Element("Getdutycde")
					.setText(tGrpContInfoSSRS.GetText(i, 6)));
			tGrpContInfo.addContent(new Element("Getdutycdename")
					.setText(tGrpContInfoSSRS.GetText(i, 7)));
			tGrpContInfo.addContent(new Element("Insuedno")
			.setText(tGrpContInfoSSRS.GetText(i, 8)));
			tGrpContInfo.addContent(new Element("contno")
					.setText(tGrpContInfoSSRS.GetText(i, 9)));
			tGrpContInfo.addContent(new Element("name")
					.setText(tGrpContInfoSSRS.GetText(i, 10)));
			tGrpContInfo.addContent(new Element("idtype")
					.setText(tGrpContInfoSSRS.GetText(i, 11)));
			tGrpContInfo.addContent(new Element("idno").setText(tGrpContInfoSSRS
					.GetText(i, 12)));
			tGrpContInfo.addContent(new Element("accdate").setText(tGrpContInfoSSRS
					.GetText(i, 13)));
			tGrpContInfo.addContent(new Element("riskcode").setText(tGrpContInfoSSRS
					.GetText(i, 14)));
			tGrpContInfo.addContent(new Element("riskname")
					.setText(tGrpContInfoSSRS.GetText(i, 15)));
			tGrpContInfo.addContent(new Element("DISEASECODE")
					.setText(tGrpContInfoSSRS.GetText(i, 16)));
			tGrpContInfo.addContent(new Element("DISEASEname")
					.setText(tGrpContInfoSSRS.GetText(i, 17)));
			tGrpContInfo.addContent(new Element("realpay")
					.setText(tGrpContInfoSSRS.GetText(i, 18)));
			tGrpContInfo.addContent(new Element("givetype")
					.setText(tGrpContInfoSSRS.GetText(i, 19)));
			tGrpContInfo.addContent(new Element("givetypename")
					.setText(tGrpContInfoSSRS.GetText(i, 20)));
			tGrpContInfo.addContent(new Element("OutDutyAmnt")
					.setText(tGrpContInfoSSRS.GetText(i, 21)));
			tGrpContInfo.addContent(new Element("endcasedate")
			.setText(tGrpContInfoSSRS.GetText(i, 22)));

					
			tContInfos.addContent(tGrpContInfo);
			
		}		
			
			tBODY.addContent(tContInfos);
			tPACKET.addContent(tBODY);
			

			mFilePath = mURL +"PH_LPCP_" +tGRPcontnoSSRS.GetText(x, 1)+"_" + PubFun.getCurrentDate2() + PubFun.getCurrentTime2() +".xml";
			XMLOutputter outputter = new XMLOutputter("  ", true, "GBK");

			try {
				outputter.output(Doc, new FileOutputStream(mFilePath));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!sendXML(mFilePath)){
			  System.out.println("向FTP发送文件失败！");
			}

			}
			}
		return true;
	}

	private boolean sendXML(String cXmlFile){

		String getIPPort = "select codename ,codealias from ldcode where codetype='WBClaim' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='WBClaim' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			System.out.println("FTP服务器IP、用户名、密码、端口都不能为空，否则无法发送xml");
			return false;
		}
		
		String tIP = tIPSSRS.GetText(1, 1);
	    String tPort = tIPSSRS.GetText(1, 2);
	    String tUserName = tUPSSRS.GetText(1, 1);
	    String tPassword = tUPSSRS.GetText(1, 2);
		
//	    tIP = "10.252.130.144";
//	    tPort = "21";
//	    tUserName = "picch";
//	    tPassword = "picch";
	    
		FTPTool tFTPTool = new FTPTool(tIP, tUserName,
				tPassword, Integer.parseInt(tPort));
		
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println("登录ftp服务器失败");
				return false;
			}
		} catch (SocketException ex) {
			System.out.println("无法登录ftp服务器");
			return false;
		} catch (IOException ex) {
			System.out.println("无法读取ftp服务器信息");
			return false;
		}
		try {
			if(!tFTPTool.makeDirectory("./01PH/8695/WBClaimCorePast/")){
				System.out.println("FTP目录已存在");
			};			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		if (!tFTPTool.upload("/01PH/8695/WBClaimCorePast/", cXmlFile)) {
			System.out.println("上载文件失败!");
			return false;
		}
		
		tFTPTool.logoutFTP();

		return true;
	}

	public static void main(String[] args) {

		LLWBCorePast t = new LLWBCorePast();
		t.run();
//		t.sendXML("F:\\PH_TB_20150902171323.xml");

	}

}
