package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.jkxpt.qy.QYACCServiceImp;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QyACCServiceTask extends TaskThread
{
    /**
     * <p>Title: </p>
     *
     * <p>Description:
     * 意外险平台契约模块批跑程序
     * </p>
     *
     * <p>Copyright: Copyright (c) 2005</p>
     *
     * <p>Company: </p>
     *
     * @author zhangyang
     * @version 1.0
     */
    public CErrors mErrors = new CErrors();

    public QyACCServiceTask()
    {

    }

    public void run()
    {
        GlobalInput mG = new GlobalInput();
        mG.Operator = "001";
        mG.ManageCom = "86";

        SSRS tMsgSSRS = getMessage();
        String tPolicyNo = "";

        for (int i = 1; i <= tMsgSSRS.getMaxRow(); i++)
        {
            tPolicyNo = tMsgSSRS.GetText(i, 1);
            QYACCServiceImp tQYACCServiceImp = new QYACCServiceImp();
            
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("DataInfoKey", tPolicyNo);
            
            VData tVData = new VData();
            tVData.add(tTransferData);
            try
            {
                if (!tQYACCServiceImp.callJKXService(tVData, null))
                {
                    mErrors.addOneError("数据发送失败:");
                    mErrors.copyAllErrors(tQYACCServiceImp.mErrors);
                    System.out.println(tQYACCServiceImp.mErrors.getErrContent());
                }
            }
            catch (Exception ex)
            {
                System.out.println("数据发送失败！");
                ex.printStackTrace();
            }
            tVData.clear();
        }
        System.out.println("数据发送成功！");
    }

    private SSRS getMessage()
    {
        String tSql = ""
        	+ " select "
            + " distinct lcc.ContNo "
            + " from LCCont lcc "
            + " inner join LCPol lcp on lcp.ContNo = lcc.ContNo "
            + " where 1 = 1 "
            + " and lcc.conttype = '1' "
            + " and lcc.appflag = '1' "
            + " and lcc.ManageCom like '8611%' "
            + " and lcc.SignDate >= current date - 1 DAYS "
            + " and lcp.RiskCode in (select code from ldcode where codetype = 'bjaccidentrisk') "
            + " and lcp.RiskCode in (select lmra.RiskCode from LMRiskApp lmra where lmra.RiskType = 'A') "
            + " and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in (select code from ldcode where codetype = 'bjaccidentduty')) "
            +"  and not exists (select 1 from DataInfointerface where DataInfoKey=lcc.ContNo and state='1') "
        	+ " union all "
        	+ " select "
        	+ " distinct lic.cardno "
        	+ " from licertify lic "
        	+ " inner join lmcardrisk lcr "
        	+ " on lcr.certifycode = lic.certifycode "
        	+ " inner join ldriskwrap lrw "
        	+ " on lrw.riskwrapcode = lcr.riskcode "
        	+ " where 1=1 "
        	+ " and lic.managecom like '8611%' "
        	+ " and lic.state = '04' "
        	+ " and double(lic.amnt) > 0.00001 "
        	+ " and lrw.riskcode in (select code from ldcode where codetype = 'bjaccidentrisk') "
        	+ " and lic.makedate >= current date - 1 DAYS "
        	+ " and not exists (select 1 from DataInfointerface  where DataInfoKey=lic.cardno and state='1') "
        	+ " and exists (select 1 from licardactiveinfolist where lic.cardno = cardno) "
        	+ " with ur";

        System.out.println("SQL: " + tSql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tMsgSSRS = tExeSQL.execSQL(tSql);

        return tMsgSSRS;
    }

    public static void main(String[] args)
    {
        QyACCServiceTask mBPOQyJKXServiceTask = new QyACCServiceTask();
        mBPOQyJKXServiceTask.run();
    }

}
