package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.agentquery.HRDevelopFinishBL;
import com.sinosoft.lis.agentquery.UpDate2016IncludeManagecom;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;

public class HRDevelopTask extends TaskThread{
	
	CErrors mErrors = new CErrors();
	
	MMap mMap = null;
	
	public HRDevelopTask(){}
	
	public void run(){
		System.out.println("批处理开始");
		//List taskList = getTaskList();
		finish();
		System.out.println("批处理结束");
	}
	/*
	private List getTaskList(){
		List taskList = new ArrayList();
		String sql = "select a.ManageCom,a.agentcode from laagent a,LADimission b "
				   + "where a.agentcode=b.agentcode and a.BranchType='1' and  a.AgentState='02' "
				   + "group by a.agentcode,a.ManageCom "
				   + "having  min(a.employdate-b.DepartDate) not between '0' and '180' "
				   + "union "
				   + "select a.ManageCom,a.agentcode from laagent a "
				   + "where a.BranchType='1' and  a.AgentState='01' "
				   + "with ur";
		System.out.println("sql语句: " + sql);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL. execSQL(sql);
        System.out.println(tSSRS.getMaxRow());
        for(int i=1;i<=tSSRS.getMaxRow();i++){
        	taskList.add(tSSRS.GetText(i, 1));
        	taskList.add(tSSRS.GetText(i, 2));
        }
		return taskList;
	}
	*/
	
	private boolean finish(){
		HRDevelopFinishBL tHRDevelopFinishBL = new HRDevelopFinishBL();
        if(!tHRDevelopFinishBL.submitData())
        {
            mErrors.addOneError("HRDevelopFinishBL不能完成，原因是：" + tHRDevelopFinishBL.mErrors.getFirstError());
            return false;
        }
        UpDate2016IncludeManagecom tUpDate2016IncludeManagecom = new UpDate2016IncludeManagecom();
        if(!tUpDate2016IncludeManagecom.submitData())
        {
            mErrors.addOneError("tUpDate2016IncludeManagecom不能完成，原因是：" + tUpDate2016IncludeManagecom.mErrors.getFirstError());
            return false;
        }
        return true;
	}
	/*
	public static void main(String[] args) {
		HRDevelopTask hrdt = new HRDevelopTask();
		//显示出list中每一个元素的内容
		List list = hrdt.getTaskList();
		for(int i=0;i<list.size();i++){
			System.out.println(list.get(i).toString());
		}
		hrdt.run();
	}
	*/
}
