package com.sinosoft.lis.taskservice;

import java.util.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: 后台任务处理主控模块</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author ZhangRong
 * @version 1.0
 */
public class TaskService {
    private static TaskServiceEngine mTaskServiceEngine;
    private static Timer mTaskServiceTimer;
    private static boolean mTimerRunning;
    private String mOperator;
    private LDTaskPlanSchema mLDTaskPlanSchema;
    private LDTaskParamSet mLDTaskParamSet;
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    public CErrors mErrors = new CErrors();

    public TaskService() {
        /*		if (mTaskServiceEngine == null)
          {
           mTaskServiceEngine = new TaskServiceEngine();
          }

          if (mTaskServiceTimer == null)
          {
           mTaskServiceTimer = new Timer();
          }*/
    }

    public int submitData(VData aInputData, String aOperate) { //return 0:正常 -1:异常且timer未启动 其他:部分异常，timer启动
        int tOperateResult = 0;
        mResult.clear();
        if (aOperate == null) {
            tOperateResult = -1;
        }
        if (aOperate.toUpperCase().equals("START")) {
            tOperateResult = serviceStart();
        } else if (aOperate.toUpperCase().equals("STOP")) {
            tOperateResult = serviceStop();
        } else if (aOperate.toUpperCase().equals("GETSTATE")) {
            tOperateResult = getServiceState();
        } else if (aOperate.toUpperCase().equals("INSERTTASK") ||
                   aOperate.toUpperCase().equals("DELETETASK")) {
            TaskSet tTaskSet = new TaskSet();
            if (!tTaskSet.submitData(aInputData, aOperate)) {
                tOperateResult = -1;
            }
            tOperateResult = 0;
        } else {
            if (!mTimerRunning) {
                mErrors.addOneError(new CError("任务服务引擎尚未启动！"));
                tOperateResult = -1;
            } else {
                boolean tResultFlag = true;

                if (!getInputData(aInputData)) {
                    tOperateResult = -1;
                } else if (aOperate.toUpperCase().equals("INSERT")) {
                    tResultFlag = insertTask();
                } else if (aOperate.toUpperCase().equals("DELETE")) {
                    tResultFlag = deleteTask();
                } else if (aOperate.toUpperCase().equals("ACTIVATE")) {
                    tResultFlag = activateTask();
                } else if (aOperate.toUpperCase().equals("DEACTIVATE")) {
                    tResultFlag = deactivateTask();
                }

                if (!tResultFlag) {
                    tOperateResult = -1;
                }
            }
        }

        System.out.println("return: " + Integer.toString(tOperateResult));

        return tOperateResult;
    }
//启动服务
    private int serviceStart() {
        int tResult = 0;
        boolean cTaskServerState=this.getWebServerState();

        if (mTimerRunning) {
            mErrors.addOneError(new CError("任务引擎已经启动!"));
            //如果服务已经启动，但是WebServer没有启动，则维护之
            if (!cTaskServerState) {
                try {
                    System.out.println("服务已经启动，但是WebServer没有启动,开始启动WebServer");
                    this.StartWebServer();
                } catch (Exception ex) {
                }
            }

            return -2;
        }
        //如果WebServer已经启动，则说明有批处理已经在运行，返回抱错
        if (cTaskServerState) {
            mErrors.addOneError(new CError("已经有任务引擎处于启动状态！"));
            return -2;
        }
        //否则,启动WebServer,锁定引擎.不允许其他引擎启动。
        else{
            try {
                System.out.println("锁定状态，然后开始启动引擎！");
                if (!this.StartWebServer()) {
                    mErrors.addOneError(new CError("启动WebServer引擎失败!"));
                    return -2;
                }
            } catch (Exception ex) {
            }
        }
        LDTaskPlanDB tLDTaskPlanDB = new LDTaskPlanDB();
        LDTaskPlanSet tLDTaskPlanSet = tLDTaskPlanDB.executeQuery(
                "select * from LDTaskPlan where TaskPlanCode = '000000'");

        if ((tLDTaskPlanSet == null) || (tLDTaskPlanSet.size() <= 0)) {
            mErrors.copyAllErrors(tLDTaskPlanDB.mErrors);
            mErrors.addOneError(new CError("无法取得任务引擎信息!"));
            tResult = -1;
        }
//*******************************************************************************
//服务器启动后重置，将运行标志为1-运行，且运行状态不为0-准备，且结束日期为空的任务设置为0-准备。
        String texesql="update  ldtaskplan set runstate='0',runflag='1' where runflag<>'2' and runstate<>'3' and  runstate<>'0' and endtime is null";
        ExeSQL tExeSql=new ExeSQL();
        tExeSql.execUpdateSQL(texesql);
//*********************************************************************************


        LDTaskPlanSchema tLDTaskPlanSchema = tLDTaskPlanSet.get(1);
        try {
            mTaskServiceEngine = new TaskServiceEngine();
            if (!mTaskServiceEngine.startEngine()) {
                mErrors.copyAllErrors(mTaskServiceEngine.mErrors);
                mErrors.addOneError(new CError("任务引擎启动中出现异常，部分任务未能启动！"));
                tResult = 1;
            }
            long l = new Double(tLDTaskPlanSchema.getInterval()).longValue();
            mTaskServiceTimer = new Timer();
            mTaskServiceTimer.schedule(mTaskServiceEngine, l, l);
            mTimerRunning = true;
            System.out.println("后台任务服务引擎启动！");
        } catch (IllegalArgumentException iex) {
            mErrors.addOneError(new CError("间隔时间时间有误!"));
            tResult = -1;
        } catch (Exception ex) {
            mErrors.addOneError(new CError("任务起动异常!"));
            tResult = -1;
        }
        if (tResult == -1) {
            try {
                System.out.println("启动任务失败，停止WebServer！");
                this.StopWebServer();
            } catch (Exception ex) {
            }
        }
        return tResult;
    }

    private boolean getWebServerState() {
        boolean TaskServerState;
        WebServer cWebServer = new WebServer();
        TaskServerState = cWebServer.checkServer();
        return TaskServerState;
    }

    private boolean StartWebServer() throws Exception {
        WebServer cWebServer = new WebServer();
        return cWebServer.startServer();
//        TaskServerState=cWebServer.checkServer();
    }

    private void StopWebServer() throws Exception {
        WebServer cWebServer = new WebServer();
        cWebServer.stopServer();
//        TaskServerState=cWebServer.checkServer();
    }

    private int serviceStop() {
        int tResult = 0;
        if ((mTaskServiceTimer != null && !(mTaskServiceTimer.equals("")))) {
            mTaskServiceTimer.cancel();
        }

        if (!mTimerRunning) {
            mErrors.addOneError(new CError("任务引擎尚未启动!"));

            tResult = -1;
        } else if (!mTaskServiceEngine.stopEngine()) {
            mErrors.copyAllErrors(mTaskServiceEngine.mErrors);
            mErrors.addOneError(new CError("任务引擎终止中出现异常，部分任务未能终止！"));
            tResult = 1;
            mTaskServiceEngine = null;
            mTaskServiceTimer = null;
            mTimerRunning = false;
            try {
                System.out.println("正常停止任务,开始停止WebServer！");
                this.StopWebServer();
            } catch (Exception ex) {

            }
        }

        System.out.println("后台任务服务引擎停止！");

        return tResult;
    }

    private int getServiceState() {
        if (mTimerRunning) {
            mResult.add(new String("RUNNING"));
        } else {
            mResult.add(new String("SLEEPING"));
        }

        return 0;
    }

    private boolean getInputData(VData aInputData) {
        GlobalInput tGI = (GlobalInput) aInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LDTaskPlanSet tLDTaskPlanSet = (LDTaskPlanSet) aInputData.
                                       getObjectByObjectName("LDTaskPlanSet", 0);
        LDTaskParamSet tLDTaskParamSet = (LDTaskParamSet) aInputData.
                                         getObjectByObjectName("LDTaskParamSet",
                0);

        if ((tGI == null) || (tLDTaskPlanSet == null) ||
            (tLDTaskPlanSet.size() <= 0)) {
            mErrors.addOneError(new CError("传入数据不完全！"));

            return false;
        }

        mLDTaskParamSet = tLDTaskParamSet;
        mLDTaskPlanSchema = tLDTaskPlanSet.get(1);
        mOperator = tGI.Operator;

        return true;
    }

    private boolean insertTask() {
        if ((mLDTaskPlanSchema.getTaskCode() == null) ||
            mLDTaskPlanSchema.getTaskCode().equals("")) {
            mErrors.addOneError(new CError("缺少任务信息!"));

            return false;
        }

        int tCodeInt = 0;
        String tCodeString = "000000";
        LDTaskPlanDB tLDTaskPlanDB = new LDTaskPlanDB();
        LDTaskPlanSet tLDTaskPlanSet = tLDTaskPlanDB.executeQuery("select * from LDTaskPlan where TaskPlanCode = (select MAX(TaskPlanCode) from LDTaskPlan)"); //取最大序列号

//		if ((tLDTaskPlanSet != null) && (tLDTaskPlanSet.size() > 0))
//		{
//			tCodeString = tLDTaskPlanSet.get(1).getTaskPlanCode();
//		}
//
//		try
//		{
//			tCodeInt = Integer.parseInt(tCodeString);
//			tCodeInt++;
//			tCodeString = Integer.toString(tCodeInt);
//			tCodeString = "000000".substring(1, 6 - tCodeString.length() + 1) + tCodeString;    //生成新任务计划编码
//		}
//		catch (Exception ex)
//		{
//			return false;
//		}



//		mLDTaskPlanSchema.setTaskPlanCode(tCodeString);
        mLDTaskPlanSchema.setTaskPlanCode(PubFun1.CreateMaxNo("TaskService", 6));

        if ((mLDTaskPlanSchema.getRunFlag() == null) ||
            mLDTaskPlanSchema.getRunFlag().equals("")) {
            mLDTaskPlanSchema.setRunFlag("1"); //未指明是否启动时默认为启动
        }

        mLDTaskPlanSchema.setRunState("0"); //初始状态为等待
        mLDTaskPlanSchema.setOperator(mOperator);
        mLDTaskPlanSchema.setMakeDate(PubFun.getCurrentDate());
        mLDTaskPlanSchema.setMakeTime(PubFun.getCurrentTime());
        mLDTaskPlanSchema.setModifyDate(PubFun.getCurrentDate());
        mLDTaskPlanSchema.setModifyTime(PubFun.getCurrentTime());

        if (mLDTaskParamSet != null) { //如果有参数
            int n = mLDTaskParamSet.size();

            for (int i = 1; i <= n; i++) {
                LDTaskParamSchema tLDTaskParamSchema = mLDTaskParamSet.get(i);

                if ((tLDTaskParamSchema.getParamName() == null) ||
                    tLDTaskParamSchema.getParamName().equals("") ||
                    (tLDTaskParamSchema.getParamValue() == null) ||
                    tLDTaskParamSchema.getParamValue().equals("")) {
                    mErrors.addOneError(new CError("参数信息不完全!"));

                    return false;
                }
                for (int j = 1; (j != i) && (j <= n); j++) {

                    if (tLDTaskParamSchema.getParamName().equals(
                            mLDTaskParamSet.get(j).getParamName())) { //判断参数名是否有重复
                        mErrors.addOneError(new CError("参数名重复!"));

                        return false;
                    }
                }

                tLDTaskParamSchema.setTaskPlanCode(mLDTaskPlanSchema.
                        getTaskPlanCode());
                tLDTaskParamSchema.setTaskCode(mLDTaskPlanSchema.getTaskCode());
                tLDTaskParamSchema.setOperator(mOperator);
                tLDTaskParamSchema.setMakeDate(PubFun.getCurrentDate());
                tLDTaskParamSchema.setMakeTime(PubFun.getCurrentTime());
                tLDTaskParamSchema.setModifyDate(PubFun.getCurrentDate());
                tLDTaskParamSchema.setModifyTime(PubFun.getCurrentTime());
            }
        }

        return mTaskServiceEngine.addTask(mLDTaskPlanSchema, mLDTaskParamSet);
    }

    private boolean deleteTask() {
        LDTaskPlanDB tLDTaskPlanDB = new LDTaskPlanDB();
        tLDTaskPlanDB.setTaskPlanCode(mLDTaskPlanSchema.getTaskPlanCode());

        LDTaskPlanSet tLDTaskPlanSet = tLDTaskPlanDB.query();

        if ((tLDTaskPlanSet == null) || (tLDTaskPlanSet.size() <= 0)) {
            mErrors.addOneError(new CError("未查到相应的任务计划！"));

            return false;
        }

        mLDTaskPlanSchema = tLDTaskPlanSet.get(1);

        return mTaskServiceEngine.removeTask(mLDTaskPlanSchema);
    }

    private boolean activateTask() {
        LDTaskPlanDB tLDTaskPlanDB = new LDTaskPlanDB();
        tLDTaskPlanDB.setTaskPlanCode(mLDTaskPlanSchema.getTaskPlanCode());

        LDTaskPlanSet tLDTaskPlanSet = tLDTaskPlanDB.query();

        if ((tLDTaskPlanSet == null) || (tLDTaskPlanSet.size() <= 0)) {
            mErrors.addOneError(new CError("未查到相应的任务计划！"));

            return false;
        }

        mLDTaskPlanSchema = tLDTaskPlanSet.get(1);
        mLDTaskPlanSchema.setRunFlag("1");

        return mTaskServiceEngine.activateTask(mLDTaskPlanSchema);
    }

    private boolean deactivateTask() {
        LDTaskPlanDB tLDTaskPlanDB = new LDTaskPlanDB();
        tLDTaskPlanDB.setTaskPlanCode(mLDTaskPlanSchema.getTaskPlanCode());

        LDTaskPlanSet tLDTaskPlanSet = tLDTaskPlanDB.query();

        if ((tLDTaskPlanSet == null) || (tLDTaskPlanSet.size() <= 0)) {
            mErrors.addOneError(new CError("未查到相应的任务计划！"));

            return false;
        }

        mLDTaskPlanSchema = tLDTaskPlanSet.get(1);
        mLDTaskPlanSchema.setRunFlag("0");
        mLDTaskPlanSchema.setRunState("3");

        return mTaskServiceEngine.deactivateTask(mLDTaskPlanSchema);
    }

    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "001";

        TaskService taskService = new TaskService();
        LDTaskPlanSchema tLDTaskPlanSchema = new LDTaskPlanSchema();
        LDTaskPlanSet tLDTaskPlanSet = new LDTaskPlanSet();
        LDTaskParamSchema tLDTaskParamSchema = new LDTaskParamSchema();
        LDTaskParamSet tLDTaskParamSet = new LDTaskParamSet();
        LDTaskSchema tLDTaskSchema = new LDTaskSchema();
        LDTaskSet tLDTaskSet = new LDTaskSet();

        tLDTaskPlanSchema.setTaskPlanCode("000092");
        tLDTaskPlanSchema.setTaskCode("000023");
        tLDTaskPlanSchema.setRunFlag("1");
        tLDTaskPlanSchema.setRecycleType("31");
        tLDTaskPlanSchema.setStartTime("2006-01-12 20:00:00");
        tLDTaskPlanSchema.setEndTime("");
        tLDTaskPlanSchema.setInterval(0);
        tLDTaskPlanSchema.setTimes(0);
        tLDTaskPlanSet.add(tLDTaskPlanSchema);

        tLDTaskSchema.setTaskCode("000012");
        tLDTaskSchema.setTaskDescribe("保全满期结算");
        tLDTaskSchema.setTaskClass("GEdorMJTask");
        tLDTaskSet.add(tLDTaskSchema);

        VData tData = new VData();
        tData.add(tGI);
        tData.add(tLDTaskPlanSet);
        tData.add(tLDTaskSet);
        String aaa = "2006-01-12 20:00:00";
        String bbb = aaa.substring(11);
        System.out.println("bbb="+bbb);
        taskService.submitData(tData, "START");
 //       taskService.submitData(tData, "ACTIVATE");
        taskService.submitData(tData, "STOP");
        TaskServiceEngine mTaskServiceEngine = new TaskServiceEngine();
//        mTaskServiceEngine.addTask()
    }
}
