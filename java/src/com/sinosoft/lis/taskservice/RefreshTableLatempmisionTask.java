package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;

public class RefreshTableLatempmisionTask extends TaskThread {

	public void run() {
	  System.out.println("start refresh table latempmision"+PubFun.getCurrentTime());
	  boolean tFlag =true;
	  int tCount =0;
	  while(tFlag)
	  {
		  if(checkTaskState())
		  {
			  tFlag = false;
		  }
		  else
		  {
			  try {
				  Thread.sleep(300000);
				  tCount++;
			  } catch (InterruptedException e) {
				  // TODO Auto-generated catch block
				  e.printStackTrace();
				  tFlag = false;
			  }
			  finally
			  {
				  if(12==tCount)
				  {
					  tFlag = false;
					  ExeSQL tExeSQL = new ExeSQL();
					  tExeSQL.execUpdateSQL("refresh table LATEMPMISION");
				  }
			  }
		  }
	  }
	  System.out.println("end refresh table latempmision"+PubFun.getCurrentTime());
    }
	public boolean checkTaskState()
	{
		ExeSQL tExeSQL = new ExeSQL();
		String tSql ="select count from lawagehistory where branchtype='1' and branchtype2='01' and state ='12' with ur";
		String tCalCount = tExeSQL.getOneValue(tSql);
		if((Integer.parseInt(tCalCount))>0)
		{
			return false;
		}
		tExeSQL.execUpdateSQL("refresh table LATEMPMISION");
		return true;
	}
	public static void main(String[] args) {
		RefreshTableLatempmisionTask tRefreshTableLatempmisionTask = new RefreshTableLatempmisionTask();
		Thread tThread = new Thread(tRefreshTableLatempmisionTask);
//		tRefreshTableLatempmisionTask.run();
		tThread.start();
	} 
}
