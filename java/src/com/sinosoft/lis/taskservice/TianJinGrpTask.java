package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.SysMaxNoPicch;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.schema.LCFileManageSchema;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class TianJinGrpTask extends TaskThread{

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */

	//执行任务
	public void run() {
		//判断是否可以执行
		getUser();
	}

	//提取用户
	public boolean getUser() { 
		
/*		//比较当前时间是否需要批处理
		Date Lastdate = null ;	
		Date CurentDate = null ;
		try {
			String tDateSql = "select makedate from LCFileManage where FileType='9' and FileDetailType='9' order by makedate desc";
			ExeSQL tDateEx = new ExeSQL();
			SSRS tlastDate = tDateEx.execSQL(tDateSql);
			String lastDate = tlastDate.GetText(1, 1);

			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			Calendar c = Calendar.getInstance();
	        //过去七天的日期
	        c.setTime(new Date());
	        c.add(Calendar.DATE, -7);
	        Date d = c.getTime();
	        String day = df.format(d);
	        
			Lastdate = (Date) df.parse(lastDate);	
			CurentDate = (Date) df.parse(day);

			System.out.println("哈哈哈哈1  "+CurentDate.getTime());
			System.out.println("哈哈哈哈2  "+Lastdate.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		if(CurentDate.getTime() != Lastdate.getTime()){
			System.out.println("今天不是批处理时间");
			return false;
		}
		*/
		
		
		
		
		ExeSQL tEx = new ExeSQL();

		//第一个sheet
		String tDisDetailAutoSql = " select distinct lcg.PrtNo, "
									+ "lcg.GrpName, "
									+ "lcg.GrpContNo, "
									+ "lcg.CValiDate, "
									+ "lcg.CInValiDate, "
									+ "lci.Name, "
									+ "db2inst1.codename('sex', lci.sex), "
									+ "lci.IDNo, "
									+ "lci.Birthday, "
									+ "lci.contplancode "
								+ "from lcgrpcont lcg,lcinsured lci,lcpol lcp "
								+ "where 1=1 "
									+ "and lci.contplancode = lcp.contplancode "
									+ "and lcg.grpcontno = lcp.grpcontno "
									+ "and lci.grpcontno = lcg.grpcontno "
									+ "and lci.contno = lcp.contno "
									+ "and lcg.ManageCom like '8612%' "
									+ "and lcp.riskcode ='160306' "
									+ "and lcp.conttype = '2' "
									+ "and lcp.appflag = '1' "
									+ "and lcg.stateflag = '1' "
									+ "and lcg.signdate<current date "
//									+ "and lcg.signdate>=current date - 7 days "
									+ "with ur ";

		SSRS tDisAutoLDUsers = tEx.execSQL(tDisDetailAutoSql);
		int tDisUserAtuoCount = tDisAutoLDUsers.getMaxRow();// 行数

	    String mCurDate = PubFun.getCurrentDate();
	    String mCurTime = PubFun.getCurrentTime();
		System.out.println(mCurDate);
		System.out.println(mCurTime);
		
		//创建对象
		String[][] tToExcel = new String[tDisUserAtuoCount + 10][10];
		System.out.println("统计时间" + mCurDate);
		tToExcel[0][0] = "印刷号";
		tToExcel[0][1] = "投保单位名称";
		tToExcel[0][2] = "保单号";
		tToExcel[0][3] = "生效日期";
		tToExcel[0][4] = "终止日期";
		tToExcel[0][5] = "参保人姓名";
		tToExcel[0][6] = "性别";
		tToExcel[0][7] = "身份证号码";
		tToExcel[0][8] = "出生日期";
		tToExcel[0][9] = "保障计划";
		
		int excelRow = 0;

		for (int row = 1; row <= tDisUserAtuoCount; row++) {
			excelRow++;
			tToExcel[excelRow][0] = tDisAutoLDUsers.GetText(row, 1);
			tToExcel[excelRow][1] = tDisAutoLDUsers.GetText(row, 2);
  			tToExcel[excelRow][2] = tDisAutoLDUsers.GetText(row, 3);
  			tToExcel[excelRow][3] = tDisAutoLDUsers.GetText(row, 4);
  			tToExcel[excelRow][4] = tDisAutoLDUsers.GetText(row, 5);
  			tToExcel[excelRow][5] = tDisAutoLDUsers.GetText(row, 6);
  			tToExcel[excelRow][6] = tDisAutoLDUsers.GetText(row, 7);
  			tToExcel[excelRow][7] = tDisAutoLDUsers.GetText(row, 8);
  			tToExcel[excelRow][8] = tDisAutoLDUsers.GetText(row, 9);
  			tToExcel[excelRow][9] = tDisAutoLDUsers.GetText(row, 10);
  			
  		}

		
		
		//第二个sheet
		String tDisDetailAutoSql1 = /*"//--被保险人信息 "
									+ "//--增人 "
									+ */"Select lcc.PrtNo as 印刷号, "
									+ "lcc.GrpName as 投保单位名称, "
									+ "lco.contno as 保单号, "
									+ "lcc.CValiDate as 生效日期, "
									+ "lcc.CInValiDate as 终止日期, "
									+ "lci.Name as 参保人姓名, "
									+ "lci.Sex as 性别, "
									+ "lci.IDNo as 身份证号码, "
									+ "lci.Birthday as 出生日期, "
									+ "lci.ContPlanCode as 保障计划, "
									+ "nvl (nvl((select EdorValiDate from lcinsuredlist where grpcontno = lcc.grpcontno and edorno = lpg.edorno  and lco.proposalcontno=contno and insuredno=lci.insuredno),(select EdorValiDate from lbinsuredlist where grpcontno = lcc.grpcontno and edorno = lpg.edorno  and lco.proposalcontno=contno and insuredno=lci.insuredno)),lpg.edorvalidate) as 保全生效时间, "
									+ "'增人' as 保全项目类型, "
									+ "lpg.edorno  保全受理号     "
									+ "from lpgrpedoritem lpg,lpedorapp lpp,lcgrpcont lcc,lccont lco,lCinsured lci "
									+ "where lpg.grpcontno=lcc.grpcontno  "
									+ "and lpg.edorno=lpp.edoracceptno "
									+ "and lpp.edorstate='0'  "
									+ "and lcc.grpcontno=lco.grpcontno "
									+ "and lco.contno=lci.contno "
									+ "and lcc.managecom like '8612%'  "
									+ "and lcc.appflag='1' "
									+ "and lco.appflag='1'  "
									+ "and lpg.edortype='NI'  "
									+ "and exists (select 1 from lcpol where lco.contno = contno and riskcode = '160306') "
									+ "and lpp.confdate < current date "
//									+ "and lpp.confdate >= current date - 7 days "
									+ "and exists (select 1 from ljagetendorse  where contno=lco.contno and feeoperationtype='NI') "
									+ "union "
									/*+ "//--被保险人信息 "
									+ "//--增人 "*/
									+ "Select "
									+ "lcc.PrtNo as 印刷号, "
									+ "lcc.GrpName as 投保单位名称, "
									+ "lco.contno as 保单号, "
									+ "lcc.CValiDate as 生效日期, "
									+ "lcc.CInValiDate as 终止日期, "
									+ "lci.Name as 参保人姓名, "
									+ "lci.Sex as 性别, "
									+ "lci.IDNo as 身份证号码, "
									+ "lci.Birthday as 出生日期, "
									+ "lci.ContPlanCode as 保障计划, "
									+ "nvl (nvl((select EdorValiDate from lcinsuredlist where grpcontno = lcc.grpcontno and edorno = lpg.edorno  and lco.proposalcontno=contno and insuredno=lci.insuredno),(select EdorValiDate from lbinsuredlist where grpcontno = lcc.grpcontno and edorno = lpg.edorno  and lco.proposalcontno=contno and insuredno=lci.insuredno)),lpg.edorvalidate) as 保全生效时间, "
									+ "'增人' as 保全项目类型, "
									+ "lpg.edorno  保全受理号     "
									+ "from lpgrpedoritem lpg,lpedorapp lpp,lcgrpcont lcc,lbcont lco,lCinsured lci "
									+ "where lpg.grpcontno=lcc.grpcontno  "
									+ "and lpg.edorno=lpp.edoracceptno "
									+ "and lpp.edorstate='0' "
									+ "and lcc.grpcontno=lco.grpcontno "
									+ "and lco.contno=lci.contno "
									+ "and lcc.managecom like '8612%'  "
									+ "and lcc.appflag='1' "
									+ "and lco.appflag='1' "
									+ "and lpg.edortype='NI' "
									+ "and exists (select 1 from lbpol where lco.contno = contno and riskcode = '160306') "
									+ "and lpp.confdate < current date "
//									+ "and lpp.confdate >= current date - 7 days "
									+ "and exists (select 1 from ljagetendorse  where contno=lco.contno and feeoperationtype='NI') "
									+ "union "
									/*+ "//--减人   "*/
									+ "select "
									+ "lcc.PrtNo as 印刷号, "
									+ "lcc.GrpName as 投保单位名称, "
									+ "lco.contno as 保单号, "
									+ "lcc.CValiDate as 生效日期, "
									+ "lcc.CInValiDate as 终止日期, "
									+ "lci.Name as 参保人姓名, "
									+ "lci.Sex as 性别, "
									+ "lci.IDNo as 身份证号码, "
									+ "lci.Birthday as 出生日期, "
									+ "lci.ContPlanCode as 保障计划, "
									+ "nvl((select EdorValiDate from lpedoritem where edorno = lpg.edorno and grpcontno = lpg.grpcontno and edortype = lpg.edortype and lco.contno=contno),lpg.EdorValiDate) as 保全生效时间,  "
									+ "'减人' as 保全项目类型, "
									+ "lpg.edorno  保全受理号  "
									+ "from lpgrpedoritem lpg,lpedorapp lpp,lcgrpcont lcc,lbcont lco,lbinsured lci "
									+ "where lpg.grpcontno=lcc.grpcontno "
									+ "and lpg.edorno=lpp.edoracceptno "
									+ "and lpp.edorstate='0' "
									+ "and lcc.grpcontno=lco.grpcontno "
									+ "and lco.contno=lci.contno  "
									+ "and lpg.edorno=lci.edorno  "
									+ "and lcc.managecom like '8612%' "
									+ "and lcc.appflag='1'  "
									+ "and lco.appflag='1'  "
									+ "and lpg.edortype='ZT' "
									+ "and exists (select 1 from lbpol where lco.contno = contno and riskcode = '160306')  "
									+ "and lpp.confdate < current date "
//									+ "and lpp.confdate >= current date - 7 days "
									+ "and (exists (select 1 from ljagetendorse  where endorsementno =lpg.edorno and contno=lco.contno and feeoperationtype='ZT')  "
									+ "or exists (select 1 from lpedoritem where edorno=lpg.edorno and contno=lco.contno and edortype=lpg.edortype))  "
									+ "with ur ";
		SSRS tDisAutoLDUsers1 = tEx.execSQL(tDisDetailAutoSql1);
		int tDisUserAtuoCount1 = tDisAutoLDUsers1.getMaxRow();// 行数
		//创建对象
		String[][] tToExcel1 = new String[tDisUserAtuoCount1 + 10][13];
		System.out.println("统计时间1" + mCurDate);
		tToExcel1[0][0] = "印刷号";
		tToExcel1[0][1] = "投保单位名称";
		tToExcel1[0][2] = "保单号";
		tToExcel1[0][3] = "生效日期";
		tToExcel1[0][4] = "终止日期";
		tToExcel1[0][5] = "参保人姓名";
		tToExcel1[0][6] = "性别";
		tToExcel1[0][7] = "身份证号码";
		tToExcel1[0][8] = "出生日期";
		tToExcel1[0][9] = "保障计划";
		tToExcel1[0][10] = "保全生效日期";
		tToExcel1[0][11] = "保全项目名称";
		tToExcel1[0][12] = "保全受理号";
		
		int excelRow1 = 0;

		for (int row = 1; row <= tDisUserAtuoCount1; row++) {
			excelRow1++;
			tToExcel1[excelRow1][0] = tDisAutoLDUsers1.GetText(row, 1);
			tToExcel1[excelRow1][1] = tDisAutoLDUsers1.GetText(row, 2);
  			tToExcel1[excelRow1][2] = tDisAutoLDUsers1.GetText(row, 3);
  			tToExcel1[excelRow1][3] = tDisAutoLDUsers1.GetText(row, 4);
  			tToExcel1[excelRow1][4] = tDisAutoLDUsers1.GetText(row, 5);
  			tToExcel1[excelRow1][5] = tDisAutoLDUsers1.GetText(row, 6);
  			tToExcel1[excelRow1][6] = tDisAutoLDUsers1.GetText(row, 7);
  			tToExcel1[excelRow1][7] = tDisAutoLDUsers1.GetText(row, 8);
  			tToExcel1[excelRow1][8] = tDisAutoLDUsers1.GetText(row, 9);
  			tToExcel1[excelRow1][9] = tDisAutoLDUsers1.GetText(row, 10);
  			tToExcel1[excelRow1][10] = tDisAutoLDUsers1.GetText(row, 11);
  			tToExcel1[excelRow1][11] = tDisAutoLDUsers1.GetText(row, 12);
  			tToExcel1[excelRow1][12] = tDisAutoLDUsers1.GetText(row, 13);
  			
  		}

		
		try {
			String mOutXmlPath = "";
			//TODO
			//本地测试
			mOutXmlPath = "/temp/shebaotishu/";
//			mOutXmlPath = "F:/";
			System.out.println(mOutXmlPath);
			String name = mCurDate + "-"+ "TianJinTiShu.xls";
			String tTime = mCurDate +"提数结果";
			
			System.out.println("字符变更前名称："+name);
			String tAllName =new String( (mOutXmlPath + name).getBytes("GBK"), "iso-8859-1" );
			System.out.println("文件名称====："+tAllName);
			
			//将生成的Excel信息存入LCFileManage表
			LCFileManageSchema tLCFileManageSchema =new LCFileManageSchema();
			//引入流水号
			String fielNo = new SysMaxNoPicch().CreateMaxNo("FILENO", 10);
			tLCFileManageSchema.setFileNo(fielNo);
			
			tLCFileManageSchema.setFileCode("tianjin");
			tLCFileManageSchema.setFileType("9");
			tLCFileManageSchema.setFileDetailType("9");
			tLCFileManageSchema.setDiscription("天津提数");
			tLCFileManageSchema.setManageCom("86");
			tLCFileManageSchema.setOperator("it001");
			tLCFileManageSchema.setOtherNo(null);
			tLCFileManageSchema.setOtherNoType(null);
			
			tLCFileManageSchema.setFileName(name);
			tLCFileManageSchema.setFilePath(mOutXmlPath);
			tLCFileManageSchema.setMakeDate(mCurDate);
			tLCFileManageSchema.setModifyDate(mCurDate);
			tLCFileManageSchema.setMakeTime(mCurTime);
			tLCFileManageSchema.setModifyTime(mCurTime);
			

			//准备传输数据 
			MMap map = new MMap();
			map.put(tLCFileManageSchema,"INSERT");
			VData mVData = new VData();
			mVData.add(map);
			PubSubmit ps = new PubSubmit();
		    if (!ps.submitData(mVData, null)) {
		        this.mErrors.copyAllErrors(ps.mErrors);
		        return false;
		    }
		    //XXX

			String sheetname1 = "承保信息导出";
			String sheetname2 = "保全信息导出";
			
			WriteToExcel t = new WriteToExcel(name);
			t.createExcelFile();
			t.setAlignment("CENTER");
			t.setFontName("宋体");

			String[] sheetName = { sheetname1,sheetname2 };
			t.addSheetGBK(sheetName);
			
			t.setData(0, tToExcel);
			t.setData(1, tToExcel1);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
			
			//获取文件设置格式
			FileInputStream is = new FileInputStream(new File(tAllName)); //获取文件      
			HSSFWorkbook wb = new HSSFWorkbook(is);
			HSSFSheet sheet = wb.getSheetAt(0); //获取sheet
			HSSFSheet sheet1 = wb.getSheetAt(1); //获取sheet
			is.close();
			
			//设置字体-内容
			HSSFFont tFont = wb.createFont();
			tFont.setFontName("宋体");
			tFont.setFontHeightInPoints((short) 10);//设置字体大小
			
		
			//正文样式
			HSSFCellStyle tContentStyle = wb.createCellStyle(); //获取样式
			tContentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			tContentStyle.setFont(tFont);
			tContentStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

			
			//设置sheet行高 列宽
			for (int i = 0; i < tDisUserAtuoCount + 2; i++) {
				HSSFRow row = sheet.getRow(i);
				for (int j = 0; j < 10; j++) {
					HSSFCell firstCell = row.getCell((short) j);
					sheet.setColumnWidth((short) j, (short) 6000);
					firstCell.setCellStyle(tContentStyle);
				}

			}
			//设置sheet1行高 列宽
			for (int i = 0; i < tDisUserAtuoCount1 + 2; i++) {
				HSSFRow row = sheet1.getRow(i);
				for (int j = 0; j < 13; j++) {
					HSSFCell firstCell = row.getCell((short) j);
					sheet1.setColumnWidth((short) j, (short) 6000);
					firstCell.setCellStyle(tContentStyle);
				}
			}

			
			FileOutputStream fileOut = new FileOutputStream(tAllName);
			System.out.println("aaa："+fileOut);
			wb.write(fileOut);
			fileOut.close();
			
			// 邮箱发送方
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS.GetText(1, 2),"picchealth");
			//标题，内容，附件(多附件可以用|分隔，末尾|可加可不加)
			
			tMailSender.setSendInf("天津社保补充业务数据","您好：\r\n附件是"+tTime,tAllName);
			//收件方
			String tSQLRMailSql = "select codealias,codename from ldcode where codetype = 'YJTianJinTiShu' ";
			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1), tSSRSR.GetText(1, 2), null);
			//发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}

		} catch (Exception ex) {
			this.mErrors.addOneError(new CError("每月用户日志审核时出错",
					"UsersreviewTask", "run"));
			ex.toString();
			ex.printStackTrace();
			return false;
		}

		return true;

	}

	// 主方法 测试用
	public static void main(String[] args) {
		new TianJinGrpTask().run();

	}
}
