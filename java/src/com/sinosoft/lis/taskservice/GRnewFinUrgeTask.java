package com.sinosoft.lis.taskservice;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.operfee.NormPayCollMultOperBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.xb.GRnewDueVerifyUI;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GRnewFinUrgeTask extends TaskThread{
	public CErrors mErrors = new CErrors();

    private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();

    private GlobalInput mGI = new GlobalInput();
    private SSRS mSSRS = new SSRS();

    public GRnewFinUrgeTask()
    {
        mGI.Operator = "001";
        mGI.ComCode = "86";
        mGI.ManageCom = mGI.ComCode;
    }

    public GRnewFinUrgeTask(GlobalInput cGlobalInput)
    {
        mGI = cGlobalInput;
    }

    public void run()
    {
    	
    	ExeSQL tExeSQL = new ExeSQL();
    	String  strSql = "select a.GrpContNo,b.GrpName,a.GetNoticeNo,sum(a.SumDuePayMoney), '',value((select sum(sumDuePayMoney) from LJSPayGrp where getNoticeNo = a.getNoticeNo and payType <> 'YEL'), 0),"
    		  + " value((select sum(sumDuePayMoney) from LJSPayGrp where getNoticeNo = a.getNoticeNo and payType = 'YEL'), 0), "
    		  +"(select max(EnterAccDate) from LJTempFee where TempFeeNo= a.GetNoticeNo and ConfFlag='0'),(select codename from ldcode where codetype='paymode' and code=b.PayMode),min(a.CurPayToDate),getUniteCode(a.AgentCode),c.Operator,'',min(a.LastPayToDate) " 
    		  + " from LJSPayGrp a ,LCGrpCont  b,LJSPay c "
    		  + " where   1=1  " //and a.GrpContNo='" + fm.all('GrpContNo').value + "' "
    		  + " and b.PayIntv = 0 and b.AppFlag='1' and (b.StateFlag is null or b.StateFlag = '1') and c.othernotype='1' and c.OtherNo=a.GrpContNo"
    		  + " and exists (select RiskCode from LMRiskPay where  RiskCode in ('162601','162801'))"
    		  + " and b.GrpContNo=a.GrpContNo and b.managecom like '"+mGI.ManageCom+"%%' "
    		  + " and ((select count(*) from LJTempFee where TempFeeNo= a.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
    		  + " group by a.GrpContNo,b.GrpName,a.GetNoticeNo,a.AgentCode,b.GrpContNo,b.PayMode,c.Operator ";

		mSSRS = tExeSQL.execSQL(strSql);
		if (mSSRS.getMaxRow()>=1){
		for(int i=1; i<=mSSRS.getMaxRow(); i++){
			LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
			TransferData tTransferData = new TransferData();
			tLCGrpContSchema.setGrpContNo(mSSRS.GetText(i, 1));
    	
		    String tGetNoticeNo = mSSRS.GetText(i, 3) ; // 应收号 	   
		    tTransferData.setNameAndValue("GetNoticeNo",tGetNoticeNo);
		   GRnewDueVerifyUI tGRnewDueVerifyUI = new GRnewDueVerifyUI();  
	
			VData tVData = new VData();
			tVData.add(mGI);
			tVData.addElement(tLCGrpContSchema);
			tVData.add(tTransferData);
			if (tGRnewDueVerifyUI.submitData(tVData,"VERIFY")){
				System.out.println("核销失败，原因是: " + tGRnewDueVerifyUI.mErrors.getFirstError());
			}
		  }
		}
		}
    public static void main(String[] args) {
       new GRnewFinUrgeTask().run();
	}
}
