package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.f1print.Readhtml;
//个人管理式医疗产品保费留存自动报送功能
public class PersonalManagementMedicalTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */

	//执行任务
	public void run() {
		//判断是否可以执行
		getUser();
	}

	//提取用户
	public boolean getUser() {
		
		Calendar now = Calendar.getInstance();  
		int date = now.get(Calendar.DAY_OF_WEEK); 
		System.out.println(date);
//		if(date!=6){
//			System.out.println("今天不是批处理时间");
//			return false;
//		}
		
		ExeSQL tEx = new ExeSQL();

		//自动失效
		String tDisDetailAutoSql = "select (select name from ldcom where comcode=left(lc.managecom,4)),"
				+ "(select name from ldcom where comcode=lc.managecom),"
				+ "lcp.riskcode,"
				+ "(select riskname from lmriskapp where riskcode = lcp.riskcode),"
				+ "lc.SaleChnl,"
				+ "db2inst1.codename('salechnl', lc.salechnl),"
				+ "(select codename from  ldcode where codetype = 'bankno'  and code = substr(lc.agentcom,1,5)),"
				+ "lcp.prem,"
				+ "lc.contno,"
				+ "lc.appntname,"
				+ "db2inst1.codename('idtype',lc.appntidtype),"
				+ "lc.appntidno,"
				+ "lc.signdate,"
				+ "lcp.CValiDate "
				+ "from lccont lc,lcpol lcp "
				+ "where 1=1 "
				+ "and lc.contno=lcp.contno "
				+ "and lc.conttype='1' "
				+ "and lc.appflag='1' "
				+ "and lc.stateflag='1' "
				+ "and lcp.riskcode in ('333701','532601','333702','532602','334901','533301','335101','533501','335001','533401','334001','532801') "
				+ "and lc.salechnl in ('04','13') "
				+ "order by left(lc.managecom,4) ";


		SSRS tDisAutoLDUsers = tEx.execSQL(tDisDetailAutoSql);
		int tDisUserAtuoCount = tDisAutoLDUsers.getMaxRow();// 行数
		System.out.println(tDisUserAtuoCount);

		
/*		Calendar cal = Calendar.getInstance();//可以对每个时间域单独修改
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd"); 
		cal.set(Calendar.DAY_OF_MONTH,0);//设置为0号,当前日期既为上月最后一天
        String tlastDay = fmt.format(cal.getTime());*/
		
	    String mCurDate = PubFun.getCurrentDate();
	    String mCurTime = PubFun.getCurrentTime();
		System.out.println(mCurDate);
		System.out.println(mCurTime);
		
		
		//创建对象
		String[][] tToExcel = new String[tDisUserAtuoCount + 10][14];
		System.out.println("统计时间" + mCurDate);

		tToExcel[0][0] = "省级分公司名称";
		tToExcel[0][1] = "管理机构名称";
		tToExcel[0][2] = "险种编码";
		tToExcel[0][3] = "险种名称";
		tToExcel[0][4] = "销售渠道";
		tToExcel[0][5] = "销售渠道名称";
		tToExcel[0][6] = "银行渠道";
		tToExcel[0][7] = "保费";
		tToExcel[0][8] = "保单号";
		tToExcel[0][9] = "投保人姓名";
		tToExcel[0][10] = "投保人证件类型";
		tToExcel[0][11] = "投保人证件号码";
		tToExcel[0][12] = "签单日期";
		tToExcel[0][13] = "保单生效日期";
		
		

		int excelRow = 0;

		for (int row = 1; row <= tDisUserAtuoCount; row++) {
			excelRow++;
			tToExcel[excelRow][0] = tDisAutoLDUsers.GetText(row, 1);
			tToExcel[excelRow][1] = tDisAutoLDUsers.GetText(row, 2);
  			tToExcel[excelRow][2] = tDisAutoLDUsers.GetText(row, 3);
  			tToExcel[excelRow][3] = tDisAutoLDUsers.GetText(row, 4);
  			tToExcel[excelRow][4] = tDisAutoLDUsers.GetText(row, 5);
  			tToExcel[excelRow][5] = tDisAutoLDUsers.GetText(row, 6);
  			tToExcel[excelRow][6] = tDisAutoLDUsers.GetText(row, 7);
  			tToExcel[excelRow][7] = tDisAutoLDUsers.GetText(row, 8);
  			tToExcel[excelRow][8] = tDisAutoLDUsers.GetText(row, 9);
  			tToExcel[excelRow][9] = tDisAutoLDUsers.GetText(row, 10);
  			tToExcel[excelRow][10] = tDisAutoLDUsers.GetText(row, 11);
			tToExcel[excelRow][11] = tDisAutoLDUsers.GetText(row, 12);
  			tToExcel[excelRow][12] = tDisAutoLDUsers.GetText(row, 13);
  			tToExcel[excelRow][13] = tDisAutoLDUsers.GetText(row, 14);
  		}

		
		try {
			String mOutXmlPath = CommonBL.getUIRoot();
			String tPath = "vtsfile/";
			//本地测试
//			mOutXmlPath = "E:/";
//			System.out.println(mOutXmlPath);
//			String name = "个人管理式医疗产品有效保单数据" + " "+ mCurDate + ".xls";
			String name = mCurDate + " " + "Tishu" + ".xls";
			String tTime = mCurDate +"提数结果";
			
			System.out.println("字符变更前名称："+name);
//			name = new String(name.getBytes("GBK"),"iso-8859-1");
			//tTime = c
			String tAllName = mOutXmlPath + name;
			System.out.println("文件名称====："+tAllName);
			
			WriteToExcel t = new WriteToExcel(name);
			t.createExcelFile();
			t.setAlignment("CENTER");
			t.setFontName("宋体");
			String[] sheetName = { new String("Data") };
			t.addSheet(sheetName);

			t.setData(0, tToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
			
			//获取文件设置格式
			FileInputStream is = new FileInputStream(new File(mOutXmlPath + name)); //获取文件      
			HSSFWorkbook wb = new HSSFWorkbook(is);
			HSSFSheet sheet = wb.getSheetAt(0); //获取sheet
			

			//设置字体-内容
			HSSFFont tFont = wb.createFont();
			tFont.setFontName("宋体");
			tFont.setFontHeightInPoints((short) 10);//设置字体大小
			
		
			//正文样式
			HSSFCellStyle tContentStyle = wb.createCellStyle(); //获取样式
			tContentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			tContentStyle.setFont(tFont);
			tContentStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

			
			//设置行高 列宽
			for (int i = 0; i < tDisUserAtuoCount + 1; i++) {
				HSSFRow row = sheet.getRow(i);
				for (int j = 0; j < 14; j++) {
					HSSFCell firstCell = row.getCell((short) j);
					sheet.setColumnWidth((short) j, (short) 6000);
//					sheet.setColumnWidth((short) j, (short) (toString().length() * 512));//自动调整列宽
//					sheet.setColumnWidth(i,value.toString().length() * 512);
					firstCell.setCellStyle(tContentStyle);
				}

			}


			FileOutputStream fileOut = new FileOutputStream(tAllName);
			wb.write(fileOut);
			fileOut.close();
			//压缩文件  
			String zips = mOutXmlPath + mCurDate + "-" + "个人管理式医疗产品有效保单数据.zip";
			zips = new String(zips.getBytes("GBK"),"iso-8859-1");
			String[] InputEntry = new String[1];
			InputEntry[0] = tAllName;
			Readhtml rh = new Readhtml();
			System.out.println("zips == " + zips);
			rh.CreateZipFile(InputEntry, zips);
			// 邮箱发送方
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS.GetText(1, 2),"picchealth");
			//标题，内容，附件(多附件可以用|分隔，末尾|可加可不加)
			
			tMailSender.setSendInf("个人管理式医疗产品有效保单数据","您好：\r\n附件是"+tTime,zips);
			//收件方
//			String tSQLRMailSql = "select codealias,codename from ldcode where codetype = 'tangchongemail' ";
//			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
//			tMailSender.setToAddress(
//					tSSRSR.GetText(1, 1), tSSRSR.GetText(1, 2)
//					, null);
			tMailSender.setToAddress("zhaoqintao@sinosoft.com.cn", "mrcao369@163.com;tangchong@sinosoft.com.cn", null);
			//发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}

		} catch (Exception ex) {
			this.mErrors.addOneError(new CError("获取用户信息出错",
					"ExportExcelTask", "run"));
			ex.toString();
			ex.printStackTrace();
			return false;
		}

		return true;

	}

	// 主方法 测试用
	public static void main(String[] args) {
		PersonalManagementMedicalTask q=new PersonalManagementMedicalTask();
		q.run();
		

	}
}
