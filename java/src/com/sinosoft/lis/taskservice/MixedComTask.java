package com.sinosoft.lis.taskservice;

import java.io.*;
import java.util.*;
import java.io.File.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;




/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class MixedComTask extends TaskThread {
    private String interFilePath = "";
    private String mLogDate="";
    private String mToday = PubFun.getCurrentDate();
    private String mTime = PubFun.getCurrentTime();
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    private String CrmPath = "";
    private String mFileName="";
    private ExeSQL mExeSQL = new ExeSQL();
    private String mBatchNo = PubFun1.CreateMaxNo("CRMBATCHNO", 20);

    public MixedComTask() {
    }
    public void run() {
    	if (!getInputData())
        {
            return;
        }
 // 进行业务处理
       try{
    	   if (!readTextDate()){
    		   return;
    	   }
      }catch(Exception ex){
    	  System.out.println("readTextDate END With Exception...");
    	  ex.printStackTrace();
    	  return;
      }
    }
    
    private boolean getInputData()
   {
       String tSql = "select sysvarvalue from ldsysvar where sysvar = 'MixedComGetUrl'";
       CrmPath = mExeSQL.getOneValue(tSql);
       tSql="select DATE_FORMAT(current date, 'yyyymmdd') from dual";
       mFileName="ORG"+mExeSQL.getOneValue(tSql)+".txt";
       
//       CrmPath = "D:\\CRMDate\\";
       return true;
   }

    private boolean readTextDate() throws java.io.FileNotFoundException,
            java.io.IOException {
    	List tResult=new ArrayList();
    	
        String str[]= new String[7];
//        String mLogName = FindLogFile(CrmPath);
//        if(mLogName.equals(""))
//            return false;
//        interFilePath = CrmPath + mLogName;
//        String mFileName="";
        interFilePath=CrmPath + mFileName;
//        interFilePath="d:\\crmdata\\" + mFileName;
        FileReader fr = new FileReader(interFilePath);
        BufferedReader br = new BufferedReader(fr);
        String Line = br.readLine();
        int intFileNum=0;
        String SingleData[] = new String[17];
        LOMixComSet mLOMixComSet = new LOMixComSet();
        while(Line!=null)
       {
          tResult.add(Line);
          System.out.println("第"+intFileNum+"行 :"+Line);
          intFileNum = intFileNum + 1;
          if(intFileNum>5000){
        	  mLOMixComSet= new LOMixComSet();
      	        for (int i=0;i<intFileNum;i++)
              {
                  SingleData = null;
                  SingleData = ((String)tResult.get(i)).split("\\|");
                  System.out.println("here1:"+tResult.get(i));
                  for(int j=0;j<17;j++){
                  	System.out.println(SingleData[j]);
                  }
                  LOMixComSchema mLOMixComSchema = new LOMixComSchema();
                  mLOMixComSchema.setGrpAgentCom(SingleData[0]);
                  mLOMixComSchema.setComp_Cod(SingleData[1]);
                  mLOMixComSchema.setComp_Nam(SingleData[2]);
                  mLOMixComSchema.setProv_Orgcod(SingleData[3]);
                  mLOMixComSchema.setProv_Orgname(SingleData[4]);
                  mLOMixComSchema.setCity_Orgcod(SingleData[5]);
                  mLOMixComSchema.setCity_Orgname(SingleData[6]);
                  mLOMixComSchema.setTown_Orgcod(SingleData[7]);
                  mLOMixComSchema.setTown_Orgname(SingleData[8]);
                  mLOMixComSchema.setUnder_Orgcod(SingleData[9]);
                  mLOMixComSchema.setUnder_Orgname(SingleData[10]);
                  mLOMixComSchema.setOrg_Lvl(SingleData[11]);
                  mLOMixComSchema.setStart_Dat(SingleData[12]);
                  mLOMixComSchema.setEnd_Dat(SingleData[13]);
                  mLOMixComSchema.setStatus_Cod(SingleData[14]);
                  mLOMixComSchema.setStatus_Nam(SingleData[15]);
                  mLOMixComSchema.setDate_Send(SingleData[16]);
                  mLOMixComSchema.setMakeDate(this.mToday);
                  mLOMixComSchema.setMakeTime(this.mTime);
                  mLOMixComSchema.setModifyDate(this.mToday);
                  mLOMixComSchema.setModifyTime(this.mTime);
                  mLOMixComSet.add(mLOMixComSchema);
              }
      	      mMap = new MMap();
              mMap.put(mLOMixComSet, "DELETE&INSERT");
              if (!SubmitMap() )
                    return false;
              intFileNum=0;
              tResult=new ArrayList();
          }
          Line   =   br.readLine();
        }
        br.close();
        fr.close();
        mLOMixComSet= new LOMixComSet();
        for (int i=0;i<intFileNum;i++)
        {
            SingleData = null;
            SingleData = ((String)tResult.get(i)).split("\\|");
            System.out.println("here1:"+tResult.get(i));
            for(int j=0;j<17;j++){
            	System.out.println(SingleData[j]);
            }
            LOMixComSchema mLOMixComSchema = new LOMixComSchema();
            mLOMixComSchema.setGrpAgentCom(SingleData[0]);
            mLOMixComSchema.setComp_Cod(SingleData[1]);
            mLOMixComSchema.setComp_Nam(SingleData[2]);
            mLOMixComSchema.setProv_Orgcod(SingleData[3]);
            mLOMixComSchema.setProv_Orgname(SingleData[4]);
            mLOMixComSchema.setCity_Orgcod(SingleData[5]);
            mLOMixComSchema.setCity_Orgname(SingleData[6]);
            mLOMixComSchema.setTown_Orgcod(SingleData[7]);
            mLOMixComSchema.setTown_Orgname(SingleData[8]);
            mLOMixComSchema.setUnder_Orgcod(SingleData[9]);
            mLOMixComSchema.setUnder_Orgname(SingleData[10]);
            mLOMixComSchema.setOrg_Lvl(SingleData[11]);
            mLOMixComSchema.setStart_Dat(SingleData[12]);
            mLOMixComSchema.setEnd_Dat(SingleData[13]);
            mLOMixComSchema.setStatus_Cod(SingleData[14]);
            mLOMixComSchema.setStatus_Nam(SingleData[15]);
            mLOMixComSchema.setDate_Send(SingleData[16]);
            mLOMixComSchema.setMakeDate(this.mToday);
            mLOMixComSchema.setMakeTime(this.mTime);
            mLOMixComSchema.setModifyDate(this.mToday);
            mLOMixComSchema.setModifyTime(this.mTime);
            mLOMixComSet.add(mLOMixComSchema);
        }
        mMap = new MMap();
        mMap.put(mLOMixComSet, "DELETE&INSERT");
        if (!SubmitMap() )
              return false;
        return true;
    }


    /*数据处理*/
private boolean SubmitMap()
{
    PubSubmit tPubSubmit = new PubSubmit();
    mResult= new VData();
    mResult.add(mMap);
    if (!tPubSubmit.submitData(mResult, ""))
    {
        System.out.println("提交数据失败！");
        return false;
    }
//    System.gc();
    return true;
}
public String GetBatchNo()
{
    return mBatchNo;
}
private String FindLogFile(String Path)
{
     File  dir = new File(Path);
     Filter aFilter = new Filter("log");
     String filename ="";
     if (dir.list(aFilter).length == 0)
          return filename;
     String  files[] =  dir.list(aFilter);
     if (!StrTool.cTrim(files[0]).equals(""))
            filename = files[0];
     System.out.println("filename "+filename);
     return filename;
}

    public static void main(String[] args) throws Exception
    {
        MixedComTask mMixedComTask = new MixedComTask();
        mMixedComTask.run();
        return;

    }

}
class Filter implements FilenameFilter{

    String extent;

    Filter(String extent){

        this.extent=extent;

    }

    public boolean accept(File dir,String name){

        return name.endsWith("."+extent); //返回文件的后缀名

    }

}