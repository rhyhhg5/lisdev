package com.sinosoft.lis.taskservice;

import java.math.BigDecimal;
import java.util.Date;

import utils.system;

import com.sinosoft.lis.agentcalculate.AgentChargeCalSaveBL;
import com.sinosoft.lis.agentcalculate.AgentChargeCalDoBL;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAIndirectWageSchema;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LBGrpContSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAIndirectWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAAutoCalChargeTask extends TaskThread {

	private MMap mMap = new MMap();
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();
	private LAChargeSet mLAChargeSet = new LAChargeSet();
	private AgentChargeCalSaveBL tAgentChargeCalSaveBL = new AgentChargeCalSaveBL();
//	private AgentChargeCalDoBL tAgentChargeCalDoBL = new AgentChargeCalDoBL();
	private VData mInputData = new VData();

	public static void main(String[] args) {
		// LAAutoCalChargeTask tAgentWageOfLABankManageTask = new
		// LAAutoCalChargeTask();
		// tAgentWageOfLABankManageTask.run();
		String CurrentDate = PubFun.getCurrentDate();
		
//		String dateSql = "Select date('" + CurrentDate + "')-1 day from dual";
//		String dateSql = "Select 1 from dual where '2018-1-1' between '2018-8-2' and '2018-8-3' ";
//		ExeSQL texeSQL = new ExeSQL();
//		String tMakeDate = texeSQL.getOneValue(dateSql);
//		String Year = tMakeDate.substring(0, 4);
//		String Month = tMakeDate.substring(5, 7);
//		System.out.println(CurrentDate.substring(8,10));
//		String endDateSql = "select (enddate -6 day),enddate) from lastatsegment where stattype = '1' and wageno = '201812'";
//		 System.out.println(tMakeDate);
		LAAutoCalChargeTask tLAAutoCalChargeTask = new LAAutoCalChargeTask();
		tLAAutoCalChargeTask.run();
	}

	public void run() {
		long tStartTime = new Date().getTime();
		System.out.println("AgentWageOfLABankManageTask-->run :开始执行");
		LAAutoCalChargeTask a = new LAAutoCalChargeTask();
		SSRS tSSRS = new SSRS();
		tSSRS = a.EasyQuery();
		String tBranchType = "5";
		String tBranchType2 = "01";
		String dateSql = "Select date('" + CurrentDate + "')-1 day from dual";
		ExeSQL texeSQL = new ExeSQL();
		String tMakeDate = texeSQL.getOneValue(dateSql);
		SSRS eSSRS = new SSRS();
		String cState = check(tMakeDate);
		if ("1".equals(cState)) {
			System.out.println("月末最后一周不处理数据");
		} else {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				System.out.println("开始处理试点机构"+tSSRS.GetText(i, 1)+"的手续费");
				VData tVData = new VData();
				tVData.add(0, tBranchType);
				tVData.add(1, tBranchType2);
				tVData.add(2, tSSRS.GetText(i, 1));
				tVData.add(3, tMakeDate);
				deal(tVData);
			}
		}
		System.out.println("LAAutoCalChargeTask-->批处理执行完成:"
				+ PubFun.getCurrentTime());
		long tEndTime = new Date().getTime();
		System.out.println("手续费自动处理的数据处理时间花费" + (tEndTime - tStartTime)
				+ "毫秒，大约" + (tEndTime - tStartTime) / 3600 / 1000 + "小时");
	}

	public boolean deal(VData cInputData) {

		if (!tAgentChargeCalSaveBL.submitData(cInputData, "")) {
			System.out.println("手续费计算出错");
			return false;
		}
		// else {
		// if (!tAgentChargeCalDoBL.submitData(cInputData, "")) {
		// System.out.println("手续费结算出错");
		// return false;
		// }
		// }
		return true;
	}

	public static double round(double v, int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");
		}
		BigDecimal b = new BigDecimal(Double.toString(v));
		BigDecimal one = new BigDecimal("1");
		return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	// 查询管理机构
	public SSRS EasyQuery() {

		String strError = "";
		Integer intStart = new Integer(String.valueOf("1"));
		;
		SSRS tSSRS = new SSRS();

		String sql = "select  code "
				+ "from ldcode where 1=1  and codetype = 'jiaochaMng' order by code  ";
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sql);
		return tSSRS;
	}

	public String check(String tMakeDate) {
		SSRS eSSRS = new SSRS();
		String YearMonth = tMakeDate.substring(0, 4)
				+ tMakeDate.substring(5, 7);
		// System.out.println(Year + Month);
		//
		String endDateSql = "select (enddate -6 day),(enddate - 1 day) from lastatsegment where stattype = '1' and yearmonth = '"
				+ YearMonth + "'";
		ExeSQL eExeSQL = new ExeSQL();
		eSSRS = eExeSQL.execSQL(endDateSql);
		String checkSql = "select '1' from dual where '" + tMakeDate
				+ "' between '" + eSSRS.GetText(1, 1) + "' and '"
				+ eSSRS.GetText(1, 2) + "' ";
		String checkState = eExeSQL.getOneValue(checkSql);
		return checkState;
	}
}
