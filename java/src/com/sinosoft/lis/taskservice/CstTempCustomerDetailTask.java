package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.cstreport.CstTempCustomerDetailBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title:BriGrpSignTask.java </p>
 *
 * <p>Description:集团提数生成临时表数据批处理 </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company:sinosoft </p>
 *
 * @author gzh
 * @version 1.0
 */
public class CstTempCustomerDetailTask extends TaskThread
{
    public String mCurrentDate = PubFun.getCurrentDate();
    
    public CstTempCustomerDetailTask()
    {
        try
        {
            jbInit();
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void run()
    {
        System.out.println( this.mCurrentDate + "日生成集团上报数据临时表,开始时间是" + PubFun.getCurrentTime());
        CstTempCustomerDetailBL tCstTempCustomerDetailBL = new CstTempCustomerDetailBL();
    	VData tVData = new VData();
    	
    	GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.Operator = "group";
    	
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("CstDate", "2012-06-30");
    	
    	tVData.add(tGlobalInput);
    	tVData.add(tTransferData);
    	
    	tCstTempCustomerDetailBL.submitData(tVData, "");
        System.out.println(this.mCurrentDate + "日生成集团上报数据临时表完成,完成时间是" +PubFun.getCurrentTime());
    }
    
    public static void main(String args[])
    {
        CstTempCustomerDetailTask tCstTempCustomerDetailTask = new CstTempCustomerDetailTask();
        tCstTempCustomerDetailTask.run();
        return;
    }

    private void jbInit() throws Exception
    {
    }
    
}
