package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.SysMaxNoPicch;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.schema.LCFileManageSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class DigitalRatioTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */

	// 执行任务
	public void run() {
		// 判断是否可以执行
		getUser();
	}

	// 提取用户
	public boolean getUser() {

		ExeSQL tEx = new ExeSQL();

		// 计算提取日期
		String today = PubFun.getCurrentDate();
		Calendar a = Calendar.getInstance();
		int year = a.get(Calendar.YEAR);
		int month = a.get(Calendar.MONTH) + 1;
		int day = a.get(Calendar.DAY_OF_MONTH);
		
		System.out.println(day);
//		if(day!=1){
//			System.out.println("今天不是批处理时间");
//			return false;
//		}
		
		String beginning = year + "-01-01";
		//测试数据
//		today = "2018-04-11";
//		year = 2018;
//		month = 4;
//		day = 11;
//		String testSQL = "select code,code1,codename,codealias from ldcode1 where codetype = 'TestTime' with ur";
//		SSRS ss = new ExeSQL().execSQL(testSQL);
//		today = ss.GetText(1, 4);
//		year = Integer.valueOf(ss.GetText(1,1));
//		month = Integer.valueOf(ss.GetText(1, 2));
//		day = Integer.valueOf(ss.GetText(1,3));
		// 如果当前月份为1月份，则提取去年全年的数据进行汇总。
		if (month == 1) {
			year -= 1;
			today = year + "-12-31";
			beginning = year + "-01-01";
		} else {
			today = PubFun.getLastDate(today + "", -day, "D");
		}
		
		System.out.println("本次将要提取" + beginning + "到" + today + "之间的数据。");
		
		// 第一个sheet
		// Pad出单
		String padSignSQL = "select x.a,sum(x.b) from (select left(cvalidate,7) a,"
				+ "count(*) b from lccont c where conttype='1' and appflag='1' and "
				+ "cvalidate >='"
				+ beginning
				+ "' and cvalidate<='"
				+ today
				+ "' and prtno like "
				+ "'PD%'  and not exists (select 1 from lcpol where prtno=c.prtno and riskcode in  (select riskcode from lmriskapp where  TaxOptimal='Y')) group by left(cvalidate,7) union all select left(cvalidate,7) a,"
				+ "count(*) b from lbcont c where conttype='1' and cvalidate >='"
				+ beginning
				+ "' "
				+ "and cvalidate<='"
				+ today
				+ "' and prtno like 'PD%' and not exists (select 1 from lcpol where prtno=c.prtno and riskcode in  (select riskcode from lmriskapp where  TaxOptimal='Y')) group by "
				+ "left(cvalidate,7)) as x where 1=1 group by x.a with ur";
		SSRS padSign = tEx.execSQL(padSignSQL);

		// 个单简易
		String simpleSingleContSQL = "select x.a,sum(x.b) from (select left(cvalidate,7) a,"
				+ "count(*) b from lccont c where conttype='1' and appflag='1' and "
				+ "cvalidate >='"
				+ beginning
				+ "' and cvalidate<='"
				+ today
				+ "' and cardflag in "
				+ "('1','2','3','5','6','8') and prtno not like 'YBK%' and prtno not like 'YWX%'  and not exists (select 1 from lcpol where prtno=c.prtno and riskcode in  (select riskcode from lmriskapp where  TaxOptimal='Y')) group by left(cvalidate,7) union all select "
				+ "left(cvalidate,7) a,count(*) b from lbcont c where conttype='1' and "
				+ "cvalidate >='"
				+ beginning
				+ "' and cvalidate<='"
				+ today
				+ "' and cardflag "
				+ "in ('1','2','3','5','6','8') and prtno not like 'YBK%' and prtno not like 'YWX%' and not exists (select 1 from lcpol where prtno=c.prtno and riskcode in  (select riskcode from lmriskapp where  TaxOptimal='Y')) group by left(cvalidate,7) ) as x where 1=1"
				+ " group by x.a with ur";
		SSRS simpleSingleCont = tEx.execSQL(simpleSingleContSQL);

		// 个单标准
		String singleContSQL = "select x.a,sum(x.b) from (select left(cvalidate,7) a,"
				+ "count(*) b from lccont c where conttype='1' and appflag='1' and "
				+ "cvalidate >='"
				+ beginning
				+ "' and cvalidate<='"
				+ today
				+ "' and cardflag='0' and prtno not like 'YBK%' and prtno not like 'YWX%' and not exists (select 1 from lcpol where prtno=c.prtno and  riskcode in  (select riskcode from lmriskapp where  TaxOptimal='Y')) "
				+ " group by left(cvalidate,7) union all select left(cvalidate,7) a,"
				+ "count(*) b from lbcont c where conttype='1' and cvalidate >='"
				+ beginning
				+ "'"
				+ " and cvalidate<='"
				+ today
				+ "' and cardflag='0' and prtno not like 'YBK%' and prtno not like 'YWX%' and not exists (select 1 from lcpol where prtno=c.prtno and riskcode in  (select riskcode from lmriskapp where  TaxOptimal='Y')) group by left(cvalidate,7))"
				+ " as x where 1=1 group by x.a with ur";
		SSRS singleCont = tEx.execSQL(singleContSQL);

		String mCurDate = PubFun.getCurrentDate();
		String mCurTime = PubFun.getCurrentTime();
		System.out.println(mCurDate);
		System.out.println(mCurTime);

		// 创建对象
		String[][] tToExcel = new String[12][6];
		System.out.println("统计时间" + mCurDate);
		tToExcel[0][0] = "月份";
		tToExcel[0][1] = "pad出单";
		tToExcel[0][2] = "个单简易";
		tToExcel[0][3] = "个单标准";
		tToExcel[0][4] = "个单出单总量";
		tToExcel[0][5] = "比例";

		String date1 = today.substring(5,7);
		tToExcel[1][0] = year+"."+date1;
		tToExcel[1][1] = padSign.GetText(padSign.getMaxRow(), 2);
		tToExcel[1][2] = simpleSingleCont.GetText(simpleSingleCont.getMaxRow(), 2);
		tToExcel[1][3] = singleCont.GetText(singleCont.getMaxRow(), 2);
		int total = Integer.parseInt(tToExcel[1][2])+Integer.parseInt(tToExcel[1][3]);
		tToExcel[1][4] = String.valueOf(total);
		double rate = Double.parseDouble(tToExcel[1][1])/total;
		tToExcel[1][5] = String.valueOf(rate);

		tToExcel[2][0] = year+".01~"+date1;
		tToExcel[2][1] = String.valueOf(count(padSign));
		tToExcel[2][2] = String.valueOf(count(simpleSingleCont));
		tToExcel[2][3] = String.valueOf(count(singleCont));
		total = count(simpleSingleCont)+count(singleCont);
		tToExcel[2][4] = String.valueOf(total);
		rate = (double)count(padSign)/total;
		tToExcel[2][5] = String.valueOf(rate);

		// 第二个sheet
		// Pad电子
		String PadOLSQL = "select x.a,sum(x.b) from (select left(cvalidate,7) a,count(*) b"
				+ " from lccont where conttype='1' and appflag='1' and cvalidate >="
				+ "'"
				+ beginning
				+ "' and cvalidate<='"
				+ today
				+ "' and prtno like 'PD%' and "
				+ "exists (select 1 from lccontsub where prtno=lccont.prtno "
				+ "and printtype='0') group by left(cvalidate,7) union all "
				+ "select left(cvalidate,7) a,count(*) b from lbcont where "
				+ "conttype='1'  and cvalidate >='"
				+ beginning
				+ "' and "
				+ "cvalidate<='"
				+ today
				+ "' and prtno like 'PD%' and exists "
				+ "(select 1 from lccontsub where prtno=lbcont.prtno "
				+ "and printtype='0') group by left(cvalidate,7) ) as x "
				+ "where 1=1 group by x.a with ur";
		SSRS PadOL = tEx.execSQL(PadOLSQL);

		// 电商
		String dianshangSQL = "select x.a,sum(x.b) from (select left(cvalidate,7) a,"
				+ "count(*) b from lccont where conttype='1' and appflag='1' and "
				+ "cvalidate >='"
				+ beginning
				+ "' and cvalidate<='"
				+ today
				+ "' and cardflag='b'"
				+ " group by left(cvalidate,7) union all select left(cvalidate,7) a,"
				+ "count(*) b from lbcont where conttype='1' and cvalidate >='"
				+ beginning
				+ "'"
				+ " and cvalidate<='"
				+ today
				+ "' and cardflag='b' group by left(cvalidate,7))"
				+ " as x where 1=1 group by x.a with ur";
		SSRS dianshang = tEx.execSQL(dianshangSQL);

		// 银保通
		String yinbaotongSQL = "select x.a,sum(x.b) from (select left(cvalidate,7) a,"
				+ "count(*) b from lccont where salechnl = '04' and cardflag in "
				+ "('c', 'd', 'e', 'f', '9') and cvalidate between '"
				+ beginning
				+ "' and '"
				+ today
				+ "' group by left(cvalidate,7) union all select left(cvalidate,7) a,"
				+ "count(*) b from lccont where agentcom in ('PC4414000714', "
				+ "'PC4407000722') and salechnl = '03' and cardflag = 'a' and cvalidate "
				+ "between '"
				+ beginning
				+ "' and '"
				+ today
				+ "' group by left(cvalidate,7) union "
				+ "all select left(cvalidate,7) a,count(*) b from lbcont where salechnl ="
				+ " '04' and cardflag in ('c', 'd', 'e', 'f', '9') and cvalidate between "
				+ "'"
				+ beginning
				+ "' and '"
				+ today
				+ "' group by left(cvalidate,7) union all "
				+ "select left(cvalidate,7) a,count(*) b from lbcont where "
				+ "agentcom in ('PC4414000714', 'PC4407000722') and salechnl = "
				+ "'03' and cardflag = 'a' and cvalidate between '"
				+ beginning
				+ "' and "
				+ "'"
				+ today
				+ "' group by left(cvalidate,7)) as x where 1=1 "
				+ "group by x.a with ur";
		SSRS yinbaotong = tEx.execSQL(yinbaotongSQL);

		// 个险
		String ContSQL = "select x.a,sum(x.b) from (select left(cvalidate,7) a,"
				+ "count(*) b from lccont where conttype='1' and appflag='1' and "
				+ "cvalidate >='"
				+ beginning
				+ "' and cvalidate<='"
				+ today
				+ "' group by "
				+ "left(cvalidate,7) union all select left(cvalidate,7) a,count(*) b "
				+ "from lbcont where conttype='1' and cvalidate >='"
				+ beginning
				+ "' and "
				+ "cvalidate<='"
				+ today
				+ "' group by left(cvalidate,7)) as x where 1=1 "
				+ "group by x.a with ur";
		SSRS totalCont = tEx.execSQL(ContSQL);		

		// 创建对象
		String[][] tToExcel1 = new String[12][7];
		System.out.println("统计时间1" + mCurDate);
		tToExcel1[0][0] = "月份";
		tToExcel1[0][1] = "pad电子";
		tToExcel1[0][2] = "电商";
		tToExcel1[0][3] = "银保通";
		tToExcel1[0][4] = "电子总数";
		tToExcel1[0][5] = "个险总单";
		tToExcel1[0][6] = "比例";
		
		tToExcel1[1][0] = year+"."+date1;
		tToExcel1[1][1] = PadOL.GetText(PadOL.getMaxRow(), 2);
		tToExcel1[1][2] = dianshang.GetText(dianshang.getMaxRow(), 2);
		tToExcel1[1][3] = yinbaotong.GetText(yinbaotong.getMaxRow(), 2);
		total = Integer.parseInt(tToExcel1[1][1])+Integer.parseInt(tToExcel1[1][2])+
				Integer.parseInt(tToExcel1[1][3]);
		tToExcel1[1][4] = String.valueOf(total);
		tToExcel1[1][5] = totalCont.GetText(totalCont.getMaxRow(), 2);
		rate = (double)total/Integer.parseInt(tToExcel1[1][5]);
		tToExcel1[1][6] = String.valueOf(rate);

		tToExcel1[2][0] = year+".01~"+date1;
		tToExcel1[2][1] = String.valueOf(count(PadOL));
		tToExcel1[2][2] = String.valueOf(count(dianshang));
		tToExcel1[2][3] = String.valueOf(count(yinbaotong));
		total = count(PadOL)+count(dianshang)+count(yinbaotong);
		tToExcel1[2][4] = String.valueOf(total);
		tToExcel1[2][5] = String.valueOf(count(totalCont));
		rate = (double)total/count(totalCont);
		tToExcel1[2][6] = String.valueOf(rate);

		try {
			String mOutXmlPath = "";
			// TODO
			// 本地测试
			String locationSQL = "select sysvarvalue from ldsysvar where sysvar = 'QYLoadPath' with ur";
			mOutXmlPath = tEx.getOneValue(locationSQL)+PubFun.getCurrentDate()+"/";
//			mOutXmlPath = "D:/";
			File fileTest = new File(mOutXmlPath);
			fileTest.mkdirs();
			System.out.println(mOutXmlPath);
			String name = mCurDate + "-" + "DigitalRatio.xls";
			String tTime = mCurDate + "提数结果";

			System.out.println("字符变更前名称：" + name);
			String tAllName = new String((mOutXmlPath + name).getBytes("GBK"),
					"iso-8859-1");
			System.out.println("文件名称====：" + tAllName);

			// 将生成的Excel信息存入LCFileManage表
			LCFileManageSchema tLCFileManageSchema = new LCFileManageSchema();
			// 引入流水号
			String fielNo = new SysMaxNoPicch().CreateMaxNo("FILENO", 10);
			tLCFileManageSchema.setFileNo(fielNo);

			tLCFileManageSchema.setFileCode("DigitalRatio");
			tLCFileManageSchema.setFileType("9");
			tLCFileManageSchema.setFileDetailType("9");
			tLCFileManageSchema.setDiscription("数字化运营指标");
			tLCFileManageSchema.setManageCom("86");
			tLCFileManageSchema.setOperator("it001");
			tLCFileManageSchema.setOtherNo(null);
			tLCFileManageSchema.setOtherNoType(null);

			tLCFileManageSchema.setFileName(name);
			tLCFileManageSchema.setFilePath(mOutXmlPath);
			tLCFileManageSchema.setMakeDate(mCurDate);
			tLCFileManageSchema.setModifyDate(mCurDate);
			tLCFileManageSchema.setMakeTime(mCurTime);
			tLCFileManageSchema.setModifyTime(mCurTime);

			// 准备传输数据
			MMap map = new MMap();
			map.put(tLCFileManageSchema, "INSERT");
			VData mVData = new VData();
			mVData.add(map);
			PubSubmit ps = new PubSubmit();
			if (!ps.submitData(mVData, null)) {
				this.mErrors.copyAllErrors(ps.mErrors);
				return false;
			}
			String sheetname1 = "销售人员移动出单率";
			String sheetname2 = "电子合同签发率";

			WriteToExcel t = new WriteToExcel(name);
			t.createExcelFile();
			t.setAlignment("CENTER");
			t.setFontName("宋体");

			String[] sheetName = { sheetname1, sheetname2 };
			t.addSheetGBK(sheetName);

			t.setData(0, tToExcel);
			t.setData(1, tToExcel1);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");

			// 获取文件设置格式
			FileInputStream is = new FileInputStream(new File(tAllName)); // 获取文件
			HSSFWorkbook wb = new HSSFWorkbook(is);
			HSSFSheet sheet = wb.getSheetAt(0); // 获取sheet
			HSSFSheet sheet1 = wb.getSheetAt(1); // 获取sheet
			is.close();

			// 设置字体-内容
			HSSFFont tFont = wb.createFont();
			tFont.setFontName("宋体");
			tFont.setFontHeightInPoints((short) 10);// 设置字体大小

			// 正文样式
			HSSFCellStyle tContentStyle = wb.createCellStyle(); // 获取样式
			tContentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			tContentStyle.setFont(tFont);
			tContentStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

			// 设置sheet行高 列宽
			for (int i = 0; i < 3; i++) {
				HSSFRow row = sheet.getRow(i);
				for (int j = 0; j < 6; j++) {
					HSSFCell firstCell = row.getCell((short) j);
					sheet.setColumnWidth((short) j, (short) 6000);
					firstCell.setCellStyle(tContentStyle);
				}

			}
			// 设置sheet1行高 列宽
			for (int i = 0; i < 3; i++) {
				HSSFRow row = sheet1.getRow(i);
				for (int j = 0; j < 7; j++) {
					HSSFCell firstCell = row.getCell((short) j);
					sheet1.setColumnWidth((short) j, (short) 6000);
					firstCell.setCellStyle(tContentStyle);
				}
			}

			FileOutputStream fileOut = new FileOutputStream(tAllName);
			System.out.println("呜啦啦啦啦：" + fileOut);
			wb.write(fileOut);
			fileOut.close();

			// 邮箱发送方
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1),
					tSSRS.GetText(1, 2), "picchealth");
			// 标题，内容，附件(多附件可以用|分隔，末尾|可加可不加)

			tMailSender.setSendInf("数字化运营转型关键指标数据", "您好：\r\n附件是" + tTime
					+ ",本邮件设置为每月1日自动发送，请知悉！", tAllName);
			// 收件方
			String tSQLRMailSql = "select codealias,codename from ldcode where codetype = 'DigitalRatio' ";
			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1),
					tSSRSR.GetText(1, 2), null);
			// 发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}
			File delFile = new File(tAllName);
			if(delFile.exists()) {
				delFile.delete();
			}

		} catch (Exception ex) {
			this.mErrors.addOneError(new CError("每月用户日志审核时出错",
					"UsersreviewTask", "run"));
			ex.toString();
			ex.printStackTrace();
			return false;
		}

		return true;

	}
	
	private int count(SSRS padSign){
		int amount = 0;
		for (int i = 1; i <= padSign.getMaxRow(); i++) {
			amount += Integer.parseInt(padSign.GetText(i, 2));
		}
		return amount;
	}

	// 主方法 测试用
	public static void main(String[] args) {
		new DigitalRatioTask().run();
	}
}
