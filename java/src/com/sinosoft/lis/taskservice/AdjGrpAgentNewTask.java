package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.agentbranch.*;

/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: SinoSoft</p>
 * @author xiangchun
 * @version 1.0
 */

public class AdjGrpAgentNewTask extends TaskThread {

    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    //如果其他渠道想要批处理，在LDCode中加描述
    private String tBranchType ="2";
    private String tBranchType2 ="01";
    private String mIndexCalNo =AgentPubFun.formatDate(currentDate,"yyyyMM");


    public AdjGrpAgentNewTask() {

    }


    public void run() {
        System.out.println("");
        System.out.println(
                "＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
        System.out.println("参数个数:" + mParameters.size());
        System.out.println("");
        // Write your code here

        System.out.println(mIndexCalNo);
        AdjGrpAgentNewTask a = new AdjGrpAgentNewTask();
        if (!a.getDate(mIndexCalNo , tBranchType, tBranchType2))
        {
           return;
        }
     }

    public static void main(String[] args) {
     AdjustGrpAgentTempBL a = new AdjustGrpAgentTempBL();
    }

    //
    public boolean getDate(String tIndexCalNo,String tBranchType,String tBranchType2) {
        String FlagStr = "";
        TransferData tTransferData = new TransferData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "000"; //系统自签
        tG.ManageCom = "86"; //默认总公司
        //接收信息
        // 投保单列表
        LABranchChangeTempSchema tLABranchChangeTempSchema = new LABranchChangeTempSchema();

        System.out.println("123:"+tIndexCalNo);
        tLABranchChangeTempSchema.setCValiMonth(tIndexCalNo);
        tLABranchChangeTempSchema.setBranchType(tBranchType);  //0:表示三种展业类型的数据都提
        tLABranchChangeTempSchema.setBranchType2(tBranchType2);  //0:表示三种展业类型的数据都提
        tLABranchChangeTempSchema.setOperator(tG.Operator);

        VData tVData=new VData();
        tVData.addElement(tG);
        tVData.addElement(tLABranchChangeTempSchema);
        System.out.println("1234:"+tLABranchChangeTempSchema.getCValiMonth());

        AdjustGrpAgentTempBL tAdjustGrpAgentTempBL=new AdjustGrpAgentTempBL();
        if (!tAdjustGrpAgentTempBL.submitData(tVData,""))
        {
        FlagStr="Fail";
        return false;
        }
        else
        {
              System.out.print(" 成功! ");
              return true;
        }

    }



}
