package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.SendMsgMail;
import com.sinosoft.lis.taskservice.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun1;
import java.io.IOException;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;
import java.net.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 定时发送短信，并同时将所发短信保存
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author HM
 * @version 1.0
 */
public class LPMsgTask extends TaskThread {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    String Content="";
    String Email="";
    String Mobile="";
    // 输入数据的容器
    private VData mInputData = new VData();

    private MMap map = new MMap();
    public LPMsgTask() {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run() {
        if (!Msg()) {
            return;
        }
    }

    //
    private boolean Msg() {
        String SQLA = "select a.handler h from llcase a "
                      + " where a.rgtstate not in ('07','09','12','11','14','13') "
                      + " and a.receiptflag in('1','3')  group by a.handler "
                      + " union  select c.appclmuwer h from llcase a,llclaimuwmain c "
                      +" where rgtstate in ('05','10') and c.caseno=a.caseno  group by c.appclmuwer "
                      + " union select a.Operator h  from llcase a where "
                      + " a.rgtstate in ('13','07','11') group by a.Operator "
                      + " order by h with ur ";
        System.out.println(SQLA);
        tSSRS = tExeSQL.execSQL(SQLA);

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String handler = tSSRS.GetText(i, 1);
            String count = getCount(2, handler, ">");
            String count3 = getCount(2, handler, "=");
            String count4 = getCount(3, handler, "=");
            String count6 = getCount(5, handler, "=");
            String count12 = getCount(11, handler, ">");
            if (Integer.parseInt(count) > 0) {
                Content = getMsg(handler, count3, count4, count6, count12);
                kSSRS = getEM(handler);
                if (kSSRS.MaxRow > 0) {
                    Email = StrTool.cTrim(kSSRS.GetText(1, 1));
                    Mobile = StrTool.cTrim(kSSRS.GetText(1, 2));
                    TransferData PrintElement = new TransferData();
                    PrintElement.setNameAndValue("mobile", Mobile);
                    PrintElement.setNameAndValue("content", Content);
                    PrintElement.setNameAndValue("strFrom", "service@picchealth.com");
                    PrintElement.setNameAndValue("strTo", Email);
                    PrintElement.setNameAndValue("strTitle", "待处理案件提醒");
                    PrintElement.setNameAndValue("strPassword", "");

                    GlobalInput tGlobalInput = new GlobalInput();
                    tGlobalInput.ComCode = "86";
                    tGlobalInput.ManageCom = "86";
                    tGlobalInput.Operator = "service";

                    VData aVData = new VData();
                    aVData.add(tGlobalInput);
                    aVData.add(PrintElement);

                    SendMsgMail tSendMsgMail = new SendMsgMail();
                    if (!Mobile.equals("")) {
                        try {
                            if (!tSendMsgMail.submitData(aVData, "Message")) {
                                System.out.println("短信发送失败,接受者：" + handler +
                                                   " ；手机号：" + Mobile);

                            }
                        } catch (MalformedURLException ex) {
                        } catch (IOException ex) {
                        }

                    }
                    if (!Email.equals("")) {
                        try {
                            if (!tSendMsgMail.submitData(aVData, "Email")) {
                                System.out.println("邮件发送失败,接受者：" + handler +
                                                   " ；邮箱：" + Email);

                            }
                        } catch (MalformedURLException ex) {
                        } catch (IOException ex) {
                        }

                    }
                } else {
                    System.out.println("没有联系方式,用户：" + handler + "。");
                }
            }else{
                System.out.println("没有2天以上的待处理案件,用户：" + handler + "。");
            }
        }
        return true;
    }

    //得到需要发送的短信/邮件内容
    private String getMsg(String handler,String date1,String date2,String date3,String date4) {
       String SQLName="select username from llclaimuser where usercode='"+handler+"'";
        String handlerName = tExeSQL.getOneValue(SQLName);
        String SMS= handlerName+" 您的个人信箱待处理案件 3天 "+date1+"件，4天 "+date2+"件，6天 "+date3+"件，超过12天 "+date4+"件 ; 请您尽快处理。"
                          +"发信人:中国人保健康";
        return SMS;
    }

    //得到需要发送短信/邮件的收信人信息

    private SSRS getEM(String handler) {
       String SQLEM="select EMail,Mobile from LDPersonnel where usercode='"+handler+"'";
        kSSRS = tExeSQL.execSQL(SQLEM);
        return kSSRS;
    }

    public String  getCount(int tDate,String hand,String Condition) {
        String tCount = "";
      String SQL = "select count(c) from ("
                 +"select caseno c from llcase where days('"+PubFun.getCurrentDate()+"')-days(rgtdate) "+Condition
                 + tDate + " and receiptflag in('1','3')    "
                 + "  and handler='"+hand+"' "
                 +" and rgtstate not in ('07','09','12','11','14','13') "
                 +" union  "
                 +"select a.caseno c from llcase a,llclaimuwmain c where days('" +PubFun.getCurrentDate() + "')-days(a.rgtdate) "+Condition
                 + tDate + " and c.caseno=a.caseno and a.rgtstate in ('05','10')    "
                 + "  and c.appclmuwer='" + hand + "' "
                 +" union  "
                 +"select a.caseno c from llcase a where days('" +PubFun.getCurrentDate() + "')-days(a.rgtdate)"+Condition
                 + tDate + "  and  a.rgtstate in ('13','07','11')     "
                 + "  and a.Operator='" + hand + "' "
                 +") as x  with ur ";
      tCount = tExeSQL.getOneValue(SQL);

        return tCount;
    }



    public static void main(String args[]) {
         LPMsgTask a = new LPMsgTask();
         a.run();


    }
}
