package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.llcase.GrpPremCatchBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.SendMsgMail;
import com.sinosoft.lis.taskservice.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
 
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class SendJGBFEmailTask  extends TaskThread {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public CErrors mErrors = new CErrors();

        // 输入数据的容器
        private VData mInputData = new VData();

        // 统一更新日期
        private String mCurrentDate = GetDate();
        // 统一更新时间
        private String mCurrentTime = PubFun.getCurrentTime();
        private MMap map = new MMap();
        private String mStratTime;
    private String mSendType;
    public SendJGBFEmailTask() {
    }
    public static String TaskCode = "";

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run() {
    	System.out.println("理赔经过保费批处理开始运行");
        if (!SendInfo()) {
            return;
        }
    }

    //执行存档操作
    private boolean SendInfo() {
        SSRS tSSRS_Receive = null; //得到信息接收者信息
//        String tMsgType = ""; //发送类型:"1"表示发短信"2"表示发邮件"3"两者皆发
        String mUser = "";
        String tTopic = ""; //邮件主题
        String tBaseContent = ""; //公用内容
        String tContentFlag = ""; //内容标示:"0"表示自定信息内容"1"信息模板号
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
System.out.println("ttttttttttttttmCurrentDate:"+mCurrentDate);
        boolean YN=false;
     try{
        //发送短消息
        GrpPremCatchBL tGrpPremCatchBL=new GrpPremCatchBL();
         YN=tGrpPremCatchBL.dealData(mCurrentDate, mCurrentDate);
        //boolean YN=false;
     } catch (Exception ex) {
    	 System.out.println("团体保单经过保费历史数据导入错误");
			//			cLogger.error("团体保单经过保费历史数据导入错误", ex);
			ex.printStackTrace();
     }
        if(YN){
        	System.out.println("理赔经过保费提数成功，开始发送邮件");
                String MsgInfoFlag = "ture";
//                tMsgType = "";
               // tTopic = "经过保费提数错误";
                tTopic = "经过保费提数成功";
                tContentFlag = "";
                tSSRS_Receive = getMsgReceiveTask(mUser); //获得发送任务
                String[][] msgReceive = tSSRS_Receive.getAllData();
                String tEmail = "";
                //String tMsgContent = "理赔经过保费"+mCurrentDate+"日数据提数失败，请检查批处理是否正常！";
                String tMsgContent = "理赔经过保费"+mCurrentDate+"日数据提数成功！";
                int mRow_rece = tSSRS_Receive.getMaxRow();
//                if (tMsgType.equals("2")) { //发邮件
                    for (int c_rece = 0; c_rece < mRow_rece; c_rece++) {
                        String mflag = "ture";
                        tEmail = msgReceive[c_rece][0]; //Email地址
                  
                        try {
                        	   TransferData PrintElement = new TransferData();
                               PrintElement.setNameAndValue("content", tMsgContent);
                               PrintElement.setNameAndValue("strFrom",
                                       "itoperation@picchealth.com");
                               PrintElement.setNameAndValue("strTo", tEmail);
                               PrintElement.setNameAndValue("strTitle", tTopic);
                               PrintElement.setNameAndValue("strPassword",
                                       "123456");

                            VData aVData = new VData();
                            aVData.add(tGlobalInput);
                            aVData.add(PrintElement);

                            SendMsgMail tSendMsgMail = new SendMsgMail();
                            if (!tSendMsgMail.submitData(aVData, "Email")) {
                                CError cErrore = new CError();
                                cErrore.errorMessage = "邮件发送失败，邮件批次号：" +
                                        msgReceive[c_rece][0] + "邮件流水号：" +
                                        msgReceive[c_rece][1];
                                mErrors.addOneError(cErrore);
                                System.out.println("邮件发送失败");
                                mflag = "false";
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    }
               }else{System.out.println("理赔经过保费提数失败！");}
            return true;

    }





    //得到需要发送短信的收信人信息
    private SSRS getMsgReceiveTask(String EmailUser) {
        SSRS tSSRS = null;
        ExeSQL tExeSQL = new ExeSQL();
        String sql = "";
        if (EmailUser.equals(""))
        {
            sql = " select CodeName from LDCode where Codetype = 'JGBFEmailAddress' and Othersign = '1' with ur" ;
        }
        else
        {
            sql = " select CodeName from LDCode where Codetype = 'JGBFEmailAddress'" +
                    "and Code = '" + EmailUser + "' with ur";
        }
        tSSRS = tExeSQL.execSQL(sql);
        return tSSRS;
    }


   

//短信发送借口



    public static void main(String args[]) {

        SendJGBFEmailTask a = new SendJGBFEmailTask();
        a.run();
        if (a.mErrors.needDealError()) {
            System.out.println(a.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }

    }
    public boolean setStartTime(String aStartTime) {
        mStratTime = aStartTime;
        System.out.println(" Set NextRunTime: " +
                           mStratTime);
        return true;
    }

    public String getStartTime() {
        return mStratTime;
    }

    public String getSendType() {
        return mSendType;
    }

    public boolean setSendType(String aSendType) {
        mSendType = aSendType;
        System.out.println("Set SendType: " + aSendType);
        return true;
    }
    public String GetDate(){
 		String CurrentDate= PubFun.getCurrentDate();   
 	    String AheadDays="-1";
 	    FDate tD=new FDate();
 	    Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
 	    FDate fdate = new FDate();
 	    String afterdate = fdate.getString( AfterDate );
        System.out.println(afterdate);
        return afterdate;
    }

}
