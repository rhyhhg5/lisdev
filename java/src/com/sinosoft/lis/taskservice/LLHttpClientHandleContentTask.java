package com.sinosoft.lis.taskservice;

 
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *秘钥接口批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author zhang
 * @version 1.1
 */
public class LLHttpClientHandleContentTask extends TaskThread {
	/**
	 * 错误的容器
	 */
	public CErrors mErros = new CErrors();
	
	String opr="";
	
	public LLHttpClientHandleContentTask(){
		
	}
	
	public void run(){
		
		HttpClientHandleContent tHttpClientHandle = new HttpClientHandleContent();
		String result = null;
		try {
			result =tHttpClientHandle.getContent();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(result==null || result.length()==0){
			System.out.println("四川，数联易康诊疗信息接口，获取诊疗信息失败");
			System.out.println(mErros.getFirstError());
			opr="false";
		}else{
			System.out.println("四川，数联易康诊疗信息接口，获取诊疗信息成功");
			opr="true";
		}
		
	}
	
	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	public static void main(String[] args) {
		 
		
	}

}
