package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bank.SYMedicalDealUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class SyybJSTask extends TaskThread {
	
	public void run()
    {
		  GlobalInput tGlobalInput = new GlobalInput(); 
		  tGlobalInput.Operator="SYYB";
		  tGlobalInput.ComCode="86";
		  tGlobalInput.ManageCom="86";
		  
		  //生成银行数据
		  System.out.println("\n\n---GetSendToBankSave Start---");
		  SYMedicalDealUI tSYMedicalDealUI = new SYMedicalDealUI();

		  TransferData transferData1 = new TransferData();
		  transferData1.setNameAndValue("MedicalCode", "88210001");
		  transferData1.setNameAndValue("typeFlag", "");

		  VData tVData = new VData();
		  tVData.add(transferData1);
		  tVData.add(tGlobalInput);

		  //更新LJSPay向LYSendToBank和LYBankLog插入数据
		  if (!tSYMedicalDealUI.submitData(tVData, "GETMONEY")) {
			  VData rVData = tSYMedicalDealUI.getResult();
			  System.out.println(" 处理失败，原因是:" + (String)rVData.get(0));
		  }
		  System.out.println("\n---GetSendToBankSave End---\n\n");
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SyybJSTask tSyybJSTask = new SyybJSTask();
		tSyybJSTask.run();
	}

}
