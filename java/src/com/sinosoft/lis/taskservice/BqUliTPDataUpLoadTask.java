package com.sinosoft.lis.taskservice;

import com.sinosoft.httpclient.inf.WnAccChangeUpLoad;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BqUliTPDataUpLoadTask extends TaskThread {
	public CErrors mErrors = new CErrors();
	private GlobalInput tGI = new GlobalInput();
	private String managecom = "86";
	private String mContNo = null;
	private String whereParSql = "";
	
	/**
	 * 
	 * @param tContNo 上报指定保单时传入,全部上报传null
	 * @param pGI
	 */
	public BqUliTPDataUpLoadTask(String tContNo,GlobalInput pGI)
	{
		mContNo = tContNo;
		tGI = pGI;
		managecom = pGI.ManageCom;
	}
	public BqUliTPDataUpLoadTask()
	{
		
	}
	
	public void run()
    {
		dealData();
    }
	public boolean dealData()
	{
		//先准备数据
		getInputData();
		System.out.println("#######税优保全数据上传批处理开始#######");
		
		WnAccChangeUpLoad();
		
		System.out.println("#######税优保全数据上传批处理结束#######");
		
		return true;
       
	}
	private boolean getInputData()
	{
		if(null!=mContNo && !"".equals(mContNo))
		{
			whereParSql=" and a.ContNo='"+mContNo+"' ";
		}
		else
		{
			tGI.ComCode="86";
		    tGI.Operator="Server";
		    tGI.ManageCom="86";	
		}
	    return true;
	}
	private void WnAccChangeUpLoad()
	{
		//20170613sql效率优化
		String riskSql = "select riskcode from lmriskapp where risktype4 = '4' and TaxOptimal='Y' with ur";
        String riskStr = "";
        SSRS riskSSRS= new ExeSQL().execSQL(riskSql);
        if(null!=riskSSRS && riskSSRS.getMaxRow() > 0){
        	for(int i=1; i <= riskSSRS.getMaxRow(); i++){
        		riskStr +="'"+riskSSRS.GetText(i, 1)+"'";
        		if(i < riskSSRS.getMaxRow()){
        			riskStr +=",";
        		}
        	}
        }
	    
        //向中保信平台报送万能账户信息
	    String ContSql = "select b.SerialNo, b.OtherNo, a.ContNo  "
	    			+ " from lcinsureacc a,lcinsureacctrace b"
	    			+ " where a.contno = b.contno "
	    			+ " and a.insuaccno = b.insuaccno "
	    			+ " and a.GrpContNo = '00000000000000000000' "
	    			+ " and a.riskcode in (" + riskStr + ") "
	    			+ " and not exists (select 1 from lserrorlist where transtype ='ACT001' "
			    	+ " and businessno=b.serialno ) "//20170621上报失败的记录在问题解决后删除失败记录才能再次自动上报
			    	+ " and b.paydate <= current Date "
			    	+ " and exists (select 1 from lccont where contno=a.contno and appflag='1') "
	    			+ whereParSql
	    			+ " union "
	    			+ " select b.SerialNo, b.OtherNo, a.ContNo  "
	    			+ " from lbinsureacc a,lbinsureacctrace b "
	    			+ " where a.contno = b.contno "
	    			+ " and a.insuaccno = b.insuaccno "
	    			+ " and a.GrpContNo = '00000000000000000000' "
	    			+ " and a.riskcode in (" + riskStr + ") "
	    			+ " and exists (select 1 from lpedoritem where edorno=a.edorno and edortype in ('CT','XT','WT','ZC') and edorstate='0') "
	    			+ " and not exists (select 1 from lserrorlist where transtype ='ACT001' "
			    	+ " and businessno=b.serialno ) "
			    	+ " and b.paydate <= current Date "
	    			+ whereParSql
	    			+ " with ur";
					  
	    System.out.println(ContSql);
	    ExeSQL tEexSQL = new ExeSQL();
	    SSRS tSSRS = tEexSQL.execSQL(ContSql);
	    
	    int succCount=0;

	    for(int i=1;i<=tSSRS.getMaxRow();i++)
	    {
	    	String tSerialNo=tSSRS.GetText(i, 1);
	    	String tOtherNo=tSSRS.GetText(i, 2);
	    	String tContNo=tSSRS.GetText(i, 3);
		    VData tVData = new VData();
		    tVData.add(tGI);
		    
		    TransferData  tTransferData = new TransferData();
		    tTransferData.setNameAndValue("SerialNo",tSerialNo);
		    tTransferData.setNameAndValue("OtherNo",tOtherNo);
		    tTransferData.setNameAndValue("ContNo",tContNo);
		    tVData.add(tTransferData);
		    WnAccChangeUpLoad tWnAccChangeUpLoad=new WnAccChangeUpLoad();
		    if(tWnAccChangeUpLoad.submitData(tVData, "ZBXPT")){
		    	succCount++;
		    }
	    }
	    
	    System.out.println("今日税优万能账户记录上传:"+succCount+"条，共"+tSSRS.getMaxRow()+"条！");
	  
	}
	
	public static void main(String[] args)
	 {
		 String contno="033511341000001";
		 GlobalInput GI=new GlobalInput();
		 GI.Operator="server";
		 GI.ManageCom="86";
		 BqUliTPDataUpLoadTask tBqUliTPDataUpLoadTask = new BqUliTPDataUpLoadTask(null,GI);
//		 tBqUliTPDataUpLoadTask.mContNo=contno;
		 tBqUliTPDataUpLoadTask.run();
	 }
}
