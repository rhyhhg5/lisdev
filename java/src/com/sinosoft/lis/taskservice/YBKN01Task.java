package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclientybk.inf.N01;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *保外包案件信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class YBKN01Task extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    String opr = "";
    public YBKN01Task()
    {}

    public void run()
    {
    	N01  cb = new N01();
    	String SQL = " select prtno from lcpol where riskcode in(select code from ldcode where codetype='ybkriskcode') and appflag='1' "
    			+" and not exists(select 1 from  YBKErrorList where businessno=lcpol.prtno and resultstatus='1' and transtype='N01' ) "
//    			+" and exists(select 1 from  YBKErrorList where businessno=lcpol.prtno and resultstatus='1' and transtype='T02' )"
    			+" and exists(select 1 from  YBKErrorList where businessno=lcpol.prtno and responsecode='1' and transtype='G03' ) "
//    			 +" and lcpol.prtno='YWX0000001075'"
                +" union "
                +" select distinct lc.prtno from lcpol lc,lccontsub lcs where lc.riskcode in (select code from ldcode where codetype='ybkriskcode') "
                +" and lc.prtno = lcs.prtno and lcs.ifautopay != '0'and lc.appflag = '1'and lc.renewcount<>0 "
                +" and exists(select 1 from  YBKErrorList where businessno=lc.prtno and responsecode ='1' and transtype='G03' ) "
//                " and exists(select 1 from  YBKErrorList where businessno=lc.prtno and resultstatus='1' and transtype='T02' )"+
                +" and not exists (select 1 from YBK_N01_LIS_ResponseInfo where prtno = lc.prtno and renewcount = char(lc.renewcount)and RequestType = 'N01') "
                +" and exists (select 1 from lcrnewstatelog where prtno = lc.prtno and renewcount = lc.renewcount and state = '6') "
                +" and exists (select 1 from YBK_E03_LIS_ResponseInfo where edorno=lc.contno and prtno=lc.renewcount)"
//                +" and lc.prtno='YWX0000001075'"
                +" with ur ";
 	
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String tCaseNo = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("PrtNo", tCaseNo);
    			tVData.add(tTransferData);
    	    	if(!cb.submitData(tVData, "YBKPT"))
    	         {
    	    		 System.out.println("承保信息上传批处理出问题了");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             continue;
    	         }else{
    	        	 System.out.println("承保信息上传批处理成功了");
    	        	 opr ="ture";
    	         }
    		}
    	}
    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

    public static void main(String[] args)
    {
    	YBKN01Task cbTask = new YBKN01Task();
    	cbTask.run();
    }
}




