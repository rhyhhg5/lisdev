package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.lis.ygz.GetBqTempFeeBL;
import com.sinosoft.lis.ygz.GetTFBL;
import com.sinosoft.lis.ygz.GetTempFeeBL;
import com.sinosoft.lis.ygz.GetXqTempFeeBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: LCContTranfOutTask </p>
 * <p>Description: 保单迁出同步平台数据的批处理 </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class JsflLjtempTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	
	public JsflLjtempTask() {

	}
	
	/**
	 * run方法
	 */
	public void run(){
		System.out.println("======================JsflOneTask Execute  !!!======================");
		
		/**
		 * 往同步保单迁出数据中出入日期的值
		 */
		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		tInputData.add(tTransferData);
		/**
		 * 调用保单迁出同步平台数据的接口
		 */
		long startGetTempFeeBL = System.currentTimeMillis();
		//======提取契约价税分离数据(二期)
		try {
				GetTempFeeBL tGetTempFeeBL = new GetTempFeeBL();
				if(!tGetTempFeeBL.submitData(tInputData,"")){
					this.mErrors.copyAllErrors(tGetTempFeeBL.mErrors);
					System.out.println("从暂收表生成价税分离数据报错："+mErrors.getFirstError());
				  }
			} catch (Exception e) {
				System.out.println("从暂收表生成价税分离数据报错：");
				e.printStackTrace();
			}
		long endGetTempFeeBL = System.currentTimeMillis();
		System.out.println("========== GetTempFeeBL从暂收表生成价税分离数据批处理-耗时： "+((endGetTempFeeBL -startGetTempFeeBL)/1000/60)+"分钟");
		/**
		 * ======提取契约退费价税分离数据
		 * addby liyt 2016-06-22
		 * 
		 */
		long startGetTFSeparateBL = System.currentTimeMillis();
		try{
			GetTFBL tGetTFBL = new GetTFBL();
			if(!tGetTFBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetTFBL.mErrors);
				System.out.println("从暂收表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch(Exception e){
			System.out.println("从暂收表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetTFSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetTFSeparateBL提取契约退费价税分离数据批处理-耗时： "+((endGetTFSeparateBL -startGetTFSeparateBL)/1000/60)+"分钟");
	
		long satrtGetXqTempFeeSeparateBL = System.currentTimeMillis();
		try {
			/**
			 * 续期暂收数据提取
			 */
			GetXqTempFeeBL tGetXqTempFeeBL = new GetXqTempFeeBL();
			if(!tGetXqTempFeeBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetXqTempFeeBL.mErrors);
				System.out.println("续期从暂收表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("续期从暂收表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetXqTempFeeSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetXqTempFeeSeparateBL续期从暂收表生成价税分离数据批处理-耗时： "+((endGetXqTempFeeSeparateBL -satrtGetXqTempFeeSeparateBL)/1000/60)+"分钟");

		long satrtGetBqTempFeeSeparateBL = System.currentTimeMillis();
		try {
			/**
			 * 保全暂收数据提取
			 */
			GetBqTempFeeBL tGetBqTempFeeBL = new GetBqTempFeeBL();
			if(!tGetBqTempFeeBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetBqTempFeeBL.mErrors);
				System.out.println("保全从暂收表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("保全从暂收表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetBqTempFeeSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetBqTempFeeSeparateBL保全从暂收表生成价税分离数据批处理-耗时： "+((endGetBqTempFeeSeparateBL -satrtGetBqTempFeeSeparateBL)/1000/60)+"分钟");
				
		//=====进行价税分离
		long startPremSeparateTask = System.currentTimeMillis();
		try {
			PremSeparateTask tPremSeparateTask = new PremSeparateTask();
			System.out.println("=====开始价税分离=====");
			tPremSeparateTask.run();
			System.out.println("=====价税分离结束=====");
		} catch (Exception e) {
			System.out.println("价税分离发生报错！");
			e.printStackTrace();
		}
		long endPremSeparateTask = System.currentTimeMillis();
		System.out.println("========== PremSeparateTask价税分离批处理-耗时： "+((endPremSeparateTask -startPremSeparateTask)/1000/60)+"分钟");

	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		JsflLjtempTask tJsflLjtempTask = new JsflLjtempTask();
		
		tJsflLjtempTask.run();
	}
	
}
