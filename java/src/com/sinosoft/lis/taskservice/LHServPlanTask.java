package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.FDate;
import java.util.*;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LHServPlanSchema;
import com.sinosoft.lis.schema.LHServItemSchema;
import com.sinosoft.lis.schema.LHServCaseDefSchema;
import com.sinosoft.lis.schema.LHServCaseRelaSchema;

/**
 * <p>Title:LHServPlanTask </p>
 *
 * <p>Description: 健管服务计划的自动流转</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author liuli
 * @version 1.0
 */

public class LHServPlanTask extends TaskThread  {
    public String mCurrentDate = PubFun.getCurrentDate();
    //存储要提取的机构号
    public String mManageCom = null;
    MMap mMap;
    PubSubmit mPubSubmit = null;
    GlobalInput mGlobalInput = new GlobalInput();
    private VData tVData = new VData();

    public LHServPlanTask() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     **/
    public void run() {
//        VData aVData = new VData();
        mGlobalInput.ManageCom="86";
        mGlobalInput.Operator="Server";
//        aVData.addElement(mGlobalInput);
        if (!saveServPlan()) {
            return;
        }
    }


    /**
     * 手动传参数进去调用Run方法来
     * 实现当前机构下时间段的服务计划自动流转
     **/
    public boolean ManualRun(VData tVData){

    	LHServPlanTask mLHServPlanTask = new LHServPlanTask();

        TransferData mTransferData = (TransferData) tVData.
                                     getObjectByObjectName("TransferData", 0);
        String mStartDate = (String) mTransferData.getValueByName("StartDate");
        System.out.println(".................in ManualRun mStartDate"+mStartDate);
        String mManageCom = (String) mTransferData.getValueByName("ManageCom");
        String mEndDate = (String) mTransferData.getValueByName("EndDate");

        mLHServPlanTask.mGlobalInput.ManageCom="86";
        mLHServPlanTask.mGlobalInput.Operator="Server";
      FDate chgdate = new FDate();
      Date dbdate = chgdate.getDate(mStartDate); //录入的起始日期，用FDate处理
      Date dedate = chgdate.getDate(mEndDate); //录入的终止日期，用FDate处理

      while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
    	  System.out.println(".................in ManualRun dbdate"+dbdate);
    	  mLHServPlanTask.mCurrentDate = chgdate.getString(dbdate); //录入的正在统计的日期
    	  System.out.println(".................in ManualRun mCurrentDate"+mLHServPlanTask.mCurrentDate);
    	  mLHServPlanTask.mManageCom = mManageCom;
          if (!mLHServPlanTask.saveServPlan()) {
        	  System.out.println("mCurrentDate:"+mLHServPlanTask.mCurrentDate+"mManageCom:"+mLHServPlanTask.mManageCom+"这天机构中没有需要流转的保单");
          }
          dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
      }
      return true;
    }

    /**
     * 实现对有健管服务险种的保单的自动流转，并找到其对应的服务计划内容并且保存。
     * 服务计划的查找先按保费查找，再按档次查找。
     * @return boolean
     */
    private boolean saveServPlan() {
        System.out.println("come in saveServPlan().....");
        mCurrentDate = PubFun.calDate(mCurrentDate,-1,"D",null);
        String manageCom = "";
        if(mManageCom != null){
            manageCom = " and MissionProp7 like '" + mManageCom + "%' ";
        }

        //查询某一天需要流转的契约新单
        String aSql = " select missionprop1,missionprop8 ,missionid from lwmission where activityid='0000001190' and (missionprop20 is null or missionprop20='')"+
                      " and  makedate = '"+mCurrentDate+"'" + manageCom +"with ur";
        SSRS tSSRS = new ExeSQL().execSQL(aSql);
        if(tSSRS == null || tSSRS.getMaxRow()== 0){
            System.out.print("LHServPlanTask.saveServPlan()－－－－没有需要流转的保单！");
            return false;
        }
        int count = tSSRS.getMaxRow();
        for(int i=1;i<=count;i++){
            String bSql = " select * from lcpol where contno='"+tSSRS.GetText(i,1)+"'"+
                          " and riskcode in (select distinct m.riskcode from lmriskapp m where  m.risktype2 = '5' )"+
                          " order by insuredno,riskcode desc with ur";
            LCPolSet tLCPolSet = new LCPolDB().executeQuery(bSql);
            if(tLCPolSet == null || tLCPolSet.size() <= 0){
                System.out.println("保单"+tSSRS.GetText(i,1)+"不包含健管服务计划！");
            }
            
          //设置初始值是5，如果有健管服务的话就添加详细信息，并将其置为1
            
            String missionprop20 = "5";
            boolean flag = true;
            for(int m = 1;m<=tLCPolSet.size();m++){
                String cSql = " select sum(Prem) from lcpol where  contno = '"+tLCPolSet.get(m).getContNo()
                                +"' and insuredno='"+tLCPolSet.get(m).getInsuredNo()+"' with ur";
                String personFee = new ExeSQL().getOneValue(cSql);//获取该客户在该保单下的保费总和

                String dSql = "select distinct servplanlevel from lhhealthservplan where  plantype = '2' "
                               +" and servplancode = '"+tLCPolSet.get(m).getRiskCode()+"' and comid = '"+tLCPolSet.get(m).getManageCom().substring(0,4)+"' "
                               +" and feeuplimit <= "+Double.parseDouble(personFee)+" and feelowlimit >="+Double.parseDouble(personFee)
                               +" with ur";
                //uplimit(下限) 和 lowlimit(上限)
                String servplanlevel = new ExeSQL().getOneValue(dSql); //按保费取服务计划

                if(servplanlevel == null || servplanlevel.equals("")){
                     servplanlevel = Integer.toString((int)tLCPolSet.get(m).getMult()); //如果查询不到则取该服务计划的LCpol的档次
                }
               String eSql = "select a.servplanname,a.servitemcode,a.servitemsn,a.comid,"
                           +" (select distinct b.ServCaseType as xxx from LHServCaseTypeDef b where b.ContType='1' and b.ComID = '"+tLCPolSet.get(m).getManageCom().substring(0,4)+"' and b.ServItemCode = a.ServItemCode) as def  "
                           +" from LHHealthServPlan a "
                           +" where a.ServPlanCode = '"+tLCPolSet.get(m).getRiskCode()+"'"
                           +" and a.comid = '"+tLCPolSet.get(m).getManageCom().substring(0,4)+"'"
                           +" and a.ServPlanLevel = '"+servplanlevel+"'";
               
      
//             System.out.println("eSql: "+eSql);
              SSRS eSSRS = new ExeSQL().execSQL(eSql);  //该保单所对应的所有服务计划
            
              if(eSSRS.getMaxRow()<=0){
                  System.out.println("保单"+tLCPolSet.get(m).getContNo()+"--"+tLCPolSet.get(m).getRiskCode()+"没有相适应的服务计划！");
                  break;
              }
            //将健康管理的相关操作结束后将missionprop20置为1
             missionprop20 = "1";
             
             LHServPlanSchema tLHServPlanSchema = new LHServPlanSchema();
             tLHServPlanSchema.setGrpServPlanNo("0000000");
             tLHServPlanSchema.setServPlanNo(PubFun1.CreateMaxNo("LHServPlanNo",20));
             tLHServPlanSchema.setContNo(tLCPolSet.get(m).getContNo());
             tLHServPlanSchema.setCustomerNo(tLCPolSet.get(m).getInsuredNo());
             tLHServPlanSchema.setName(tLCPolSet.get(m).getInsuredName());
             tLHServPlanSchema.setServPlanCode(tLCPolSet.get(m).getRiskCode());
             tLHServPlanSchema.setServPlanName(eSSRS.GetText(1,1));  //服务计划名称
             tLHServPlanSchema.setServPlanLevel(servplanlevel);
             //续期续保的保单健管服务计划和新单流转时的区别
             if(tSSRS.GetText(1,2)!=null && tSSRS.GetText(1,2) != ""&& tSSRS.GetText(1,2) != "0"){
                 tLHServPlanSchema.setServPlayType("3"+tSSRS.GetText(1,2));//续保的保单标置：“3”＋续保次数
             }else{
                 tLHServPlanSchema.setServPlayType("1");
             }
             tLHServPlanSchema.setComID(tLCPolSet.get(m).getManageCom().substring(0,4));
             tLHServPlanSchema.setStartDate(tLCPolSet.get(m).getCValiDate());
             if(tLCPolSet.get(m).getManageCom().substring(0,4).equals("8643")&&
                 (tLCPolSet.get(m).getRiskCode().equals("331201")||tLCPolSet.get(m).getRiskCode().equals("331301"))){
                 String aa = PubFun.calDate(tLCPolSet.get(m).getCValiDate(),1, "Y",tLCPolSet.get(m).getCValiDate());  //如果是湖南这两个险种,则服务只提供一年
                 tLHServPlanSchema.setEndDate(aa);
             }else{
                 tLHServPlanSchema.setEndDate(tLCPolSet.get(m).getEndDate());
             }
             tLHServPlanSchema.setServPrem(personFee);
             tLHServPlanSchema.setCaseState("3"); //服务计划状态
             tLHServPlanSchema.setOperator(mGlobalInput.Operator);
             tLHServPlanSchema.setMakeDate(PubFun.getCurrentDate());
             tLHServPlanSchema.setMakeTime(PubFun.getCurrentTime());
             tLHServPlanSchema.setManageCom(mGlobalInput.ManageCom);
             tLHServPlanSchema.setModifyDate(PubFun.getCurrentDate());
             tLHServPlanSchema.setModifyTime(PubFun.getCurrentTime());

             mMap = new MMap();
             String test = " select count(*) from lhservplan where contno='"+tLHServPlanSchema.getContNo()+"' and customerno='"+tLHServPlanSchema.getCustomerNo()+"'"
                       +" and ServPlayType='"+tLHServPlanSchema.getServPlayType()+"' with ur";
             String ttest = new ExeSQL().getOneValue(test);//校验该保单该客户是否已经存在健管服务,ServPlayType字段区分续保的服务。
             if(ttest.equals("0")){
                 mMap.put(tLHServPlanSchema, "INSERT");
             } else{
                 System.out.println("保单"+tLHServPlanSchema.getContNo()+"的被保人："+tLHServPlanSchema.getCustomerNo()+"服务计划已存在，不需要新生成！");
                 flag = false;
                 continue;
             }
             //准备服务项目数据
             for(int n=1;n<=eSSRS.getMaxRow();n++){
                 LHServItemSchema tLHServItemSchema = new LHServItemSchema();

                 tLHServItemSchema.setGrpServPlanNo("0000000");
                 tLHServItemSchema.setServPlanNo(tLHServPlanSchema.getServPlanNo());
                 tLHServItemSchema.setGrpServItemNo("0000000");
                 tLHServItemSchema.setServItemNo(PubFun1.CreateMaxNo("LHServItemNoSet", 20));
                 tLHServItemSchema.setServItemCode(eSSRS.GetText(n,2));
                 tLHServItemSchema.setCustomerNo(tLHServPlanSchema.getCustomerNo());
                 tLHServItemSchema.setName(tLHServPlanSchema.getName());
                 tLHServItemSchema.setContNo(tLHServPlanSchema.getContNo());
                 tLHServItemSchema.setComID(eSSRS.GetText(n,4));
                 tLHServItemSchema.setServItemType(eSSRS.GetText(n,3));
                 tLHServItemSchema.setServPriceCode("0");
                 tLHServItemSchema.setServCaseType(eSSRS.GetText(n,5));
                 tLHServItemSchema.setOperator(mGlobalInput.Operator);
                 tLHServItemSchema.setManageCom(mGlobalInput.ManageCom);
                 tLHServItemSchema.setMakeDate(PubFun.getCurrentDate());
                 tLHServItemSchema.setMakeTime(PubFun.getCurrentTime());
                 tLHServItemSchema.setModifyDate(PubFun.getCurrentDate());
                 tLHServItemSchema.setModifyTime(PubFun.getCurrentTime());

                 mMap.put(tLHServItemSchema, "INSERT");

                 //对个人服务项目自动生成个人服务事件(lhservcasedef)和事件关联信息(lhservcaserela)
                 if(eSSRS.GetText(n,5).equals("1")){
                      LHServCaseDefSchema tLHServCaseDefSchema = new LHServCaseDefSchema();
//                       String MStartDate = PubFun.getCurrentDate().replaceAll("-", "");
//                        String HiddenCaseCode = MStartDate +tLHServItemSchema.getServCaseType();
//
//                        ExeSQL tExeSQL = new ExeSQL();
//                        String sqla =
//                                "select max(substr(ServCaseCode, 10,5)) from LHServCaseDef where makedate='"+ PubFun.getCurrentDate() +"' ";
//                        SSRS aSSRS = tExeSQL.execSQL(sqla);
//                        String result1 = null;
//                        if(aSSRS!=null && !aSSRS.GetText(1,1).equals("")){
//                           int result = Integer.parseInt(aSSRS.GetText(1,1))+1;
//                           result1 = result+"";
//                        }else{
//                            result1 = "1" ;
//                        }
//                        String result4 = "";
//                           for(int x=0;x<5-result1.length();x++)
//                           {
//                               result4 = result4+"0";
//                           }
                       String ServCaseCode = "";
                        ServCaseCode = PubFun1.CreateMaxNo("HEALTHDEF","1",null);//服务事件号码 日期＋事件类型（“1”）＋5位流水号
                        tLHServCaseDefSchema.setServCaseType("1");//事件类型，1为个人
                        tLHServCaseDefSchema.setServCaseCode(ServCaseCode);
                        String sqlb = "select servitemname from lhhealthservitem where servitemcode = '"+tLHServItemSchema.getServItemCode()+"' with ur";
                        String name = new ExeSQL().getOneValue(sqlb);//获取项目的名字。
                        tLHServCaseDefSchema.setServCaseName(name+ "-" +
                                tLHServItemSchema.getName()); //事件名字：项目名称+"-"+被保人姓名
                        tLHServCaseDefSchema.setServCaseState("1");//事件启动状态
                        tLHServCaseDefSchema.setManageCom(tLHServItemSchema.getComID());//机构号码
                        tLHServCaseDefSchema.setOperator(mGlobalInput.Operator);
                        tLHServCaseDefSchema.setMakeDate(PubFun.getCurrentDate());
                        tLHServCaseDefSchema.setMakeTime(PubFun.getCurrentTime());
                        tLHServCaseDefSchema.setModifyDate(PubFun.getCurrentDate());
                        tLHServCaseDefSchema.setModifyTime(PubFun.getCurrentTime());
                        mMap.put(tLHServCaseDefSchema,"INSERT");

                        LHServCaseRelaSchema tLHServCaseRelaSchema = new LHServCaseRelaSchema();
                        tLHServCaseRelaSchema.setServCaseCode(tLHServCaseDefSchema.getServCaseCode());
                        tLHServCaseRelaSchema.setServPlanNo(tLHServPlanSchema.getServPlanNo());
                        tLHServCaseRelaSchema.setServPlanName(tLHServPlanSchema.getServPlanName());
                        tLHServCaseRelaSchema.setComID(tLHServPlanSchema.getComID());
                        tLHServCaseRelaSchema.setServPlanLevel(tLHServPlanSchema.getServPlanLevel());
                        tLHServCaseRelaSchema.setServItemNo(tLHServItemSchema.getServItemNo());
//                        tLHServCaseRelaSchema.setServItemState();
                        tLHServCaseRelaSchema.setServItemCode(tLHServItemSchema.getServItemCode());
                        tLHServCaseRelaSchema.setContNo(tLHServPlanSchema.getContNo());
                        tLHServCaseRelaSchema.setCustomerNo(tLHServPlanSchema.getCustomerNo());
                        tLHServCaseRelaSchema.setCustomerName(tLHServPlanSchema.getName());
                        tLHServCaseRelaSchema.setManageCom(mGlobalInput.ManageCom);
                        tLHServCaseRelaSchema.setOperator(mGlobalInput.Operator);
                        tLHServCaseRelaSchema.setMakeDate(PubFun.getCurrentDate());
                        tLHServCaseRelaSchema.setMakeTime(PubFun.getCurrentTime());
                        tLHServCaseRelaSchema.setModifyDate(PubFun.getCurrentDate());
                        tLHServCaseRelaSchema.setModifyTime(PubFun.getCurrentTime());
                        mMap.put(tLHServCaseRelaSchema,"INSERT");
                        
                 }
             }

             tVData.clear();
             tVData.add(mMap);
             mPubSubmit = new PubSubmit();
             mPubSubmit.submitData(this.tVData, "INSERT");
            }
            //更新工作流表。正常流转的missionprop20＝“1”，不能流转的置为“5”
            if(flag){
		            String mission ="update Lwmission set missionprop20='"+missionprop20+"',lastoperator='" +
		                        mGlobalInput.Operator +
		                        "',modifydate = current date,modifytime  = current time where "
		                        + " missionprop1='" + tSSRS.GetText(i, 1) +"' "
		                        + " and missionid = '"+tSSRS.GetText(i,3)+"'"
		                        + " and activityid='0000001190'";
		            mMap = new MMap();
		            mMap.put(mission,"UPDATE");
		            tVData.clear();
		            tVData.add(mMap);
		            mPubSubmit = new PubSubmit();
		            mPubSubmit.submitData(this.tVData, "UPDATE");
		        }
            }

        return true;
    }

    public boolean submitData(String aStartDate, String aEndDate,String aManageCom) {
        //可以从前台页面置入口来进行操作.
        String  dbdate = aStartDate; //录入起始日期
        String  dedate = aEndDate; //录入终止日期
        mGlobalInput.ManageCom="86";
        mGlobalInput.Operator="Server";
        String tManageCom = aManageCom; //保单机构

        while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            mManageCom = tManageCom;
            mCurrentDate = dbdate;
            if (!saveServPlan()) {
                return false;
            }
            dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
        }
        return true;
    }

    public static void main(String args[]) {
        LHServPlanTask tLHServPlanTask = new LHServPlanTask();
//        下面的程序用于批处理启动失败时的运维
        String tStartDate = "2009-7-15";
        String tEndDate = "2009-7-15";
        String tManageCom = "8643";
        tLHServPlanTask.mGlobalInput.ManageCom="86";
        tLHServPlanTask.mGlobalInput.Operator="Server";
        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(tStartDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(tEndDate); //录入的终止日期，用FDate处理

        while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            tLHServPlanTask.mCurrentDate = chgdate.getString(dbdate); //录入的正在统计的日期
            tLHServPlanTask.mManageCom = tManageCom;
            if (!tLHServPlanTask.saveServPlan()) {
            }
            dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
        }
    }

    private void jbInit() throws Exception {
    }

}
