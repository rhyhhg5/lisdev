package com.sinosoft.lis.taskservice;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class LAChargeJsonWriteFTPTask extends TaskThread {

	private BufferedWriter mBufferedWriter;
	private String mURL = "";
	private ExeSQL mExeSQL = new ExeSQL();
	private String mCurrentDate = PubFun.getCurrentDate();
	public CErrors mErrors = new CErrors();
	private String mIP = "";
	private String mUserName = "";
	private String mTransMode = "";
	private int mPort;
	private String mPassword = "";
	private String mFilePoPoth="";

	public LAChargeJsonWriteFTPTask() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
	}

	public void run() {
		System.out.println("开始提取" + this.mCurrentDate + "日的交叉销售数据,开始时间是"
				+ PubFun.getCurrentTime());
		if (!submitData()) {
			return;
		}
		System.out.println("提取" + this.mCurrentDate + "日的交叉销售数据完成,完成时间是"
				+ PubFun.getCurrentTime());
	}

	public boolean submitData() {
		try {
			// 获取核心文件路径
			if (!getInputData()) {
				return false;
			}

			// 上传文件到核心在上传到财险FTP
			if (!test()) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "InterReportBL";
			cError.functionName = "submit";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		} finally {
			try {
				// dealCatchData();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return true;
	}

	public boolean test() {

		String tFtpSQL = "select code,codename from ldcode where codetype = 'jxchargecxftp' ";
		SSRS tSSRS = new ExeSQL().execSQL(tFtpSQL);
		if (tSSRS == null || tSSRS.MaxRow < 1) {
			System.out.println("没有找到集团交叉ftp配置信息！");
			return false;
		}
		
		String tFileFTPSQL = "select codename from ldcode where codetype = 'jianchuancai' and code ='jcc' ";
		SSRS tSSRS1 = new ExeSQL().execSQL(tFileFTPSQL);
		if (tSSRS1 == null || tSSRS1.MaxRow < 1) {
			System.out.println("没有找到健康险传递财险的文件路径！");
			return false;
		}
		mFilePoPoth = tSSRS1.GetText(1, 1);
		System.out.println("FTP路径："+mFilePoPoth);
		//每次批处理数据之前先删除备份目录数据
		 try
		 {
		 delter(mURL);
		 } catch (IOException ex)
		 {
		 ex.printStackTrace();
		 }
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			if ("IP".equals(tSSRS.GetText(i, 1))) {
				mIP = tSSRS.GetText(i, 2);
			} else if ("UserName".equals(tSSRS.GetText(i, 1))) {
				mUserName = tSSRS.GetText(i, 2);
			} else if ("Password".equals(tSSRS.GetText(i, 1))) {
				mPassword = tSSRS.GetText(i, 2);
			} else if ("Port".equals(tSSRS.GetText(i, 1))) {
				mPort = Integer.parseInt(tSSRS.GetText(i, 2));
			} else if ("TransMode".equals(tSSRS.GetText(i, 1))) {
				mTransMode = tSSRS.GetText(i, 2);
			}
		}

		LAInterMixedSpecialCharge tVerifyContNewOfIAC = new LAInterMixedSpecialCharge();
		ArrayList<String> tJsonList = new ArrayList<String>();
		tJsonList = tVerifyContNewOfIAC.service(tJsonList);
		//
		// String [] aa = new String[]{a,b};

		// this.mURL = "D:\\xmlbackup1\\";
		String filename = mURL + "000085F002"
				+ mCurrentDate.replaceAll("-", "") + "000002.txt";
		System.out.println("上传外测服务器文件:"+filename);
		FileOutputStream tFileOutputStream = null;
		try {
			tFileOutputStream = new FileOutputStream(filename, true);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				tFileOutputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		new File(filename);

		// String[] tempStringArr = a.split("\\^");
		// 默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
		// mFileWriter = new FileWriter("C:\\000085S001" +
		// mCurrentDate.replaceAll("-", "") +
		// ".txt");
		// System.out.println(mFileWriter.getEncoding());
		// mBufferedWriter = new BufferedWriter(mFileWriter);
		// 指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
		OutputStreamWriter tOutputStreamWriter = null;
		try {
			tFileOutputStream = new FileOutputStream(filename, true);
			tOutputStreamWriter = new OutputStreamWriter(tFileOutputStream,
					"GBK");
			mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
			if (tJsonList.size() > 0) {
				for (int k = 0; k < tJsonList.size(); k++) {
					mBufferedWriter.write("D[F001]:(1=0)");
					mBufferedWriter.newLine();
					mBufferedWriter.write(tJsonList.get(k));
					mBufferedWriter.newLine();
					mBufferedWriter.flush();
				}
			}

			mBufferedWriter.close();
			tFileOutputStream.close();
			// changeFileName(filename,"F002","000010");
		} catch (IOException ex2) {
			System.out.println(ex2.getStackTrace());
		} finally {
			try {
				tFileOutputStream.close();
				tOutputStreamWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("文件上传成功");
		if(changeFileName(filename, "F002", "000002"))
		{
			FtpTransferFiles();
		}

//		 try
//		 {
//		 delter(mURL);
//		 } catch (IOException ex)
//		 {
//		 ex.printStackTrace();
//		 }

		return true;

	}

	private boolean getInputData() {
		String tSql = "select sysvarvalue from ldsysvar where sysvar = 'JXWriteUrl'";
		mURL = mExeSQL.getOneValue(tSql);
		System.out.println("核心服务器路径："+mURL);
//		 mURL = "D:\\CRMDate\\";
		return true;
	}

	/**
	 * 删除目录下所有文件和子目录，根目录不删除
	 * 
	 * @param filepath
	 *            String
	 * @throws IOException
	 */
	private void delter(String filepath) throws IOException {
		File f = new File(filepath); // 定义文件路径
		if (f.exists() && f.isDirectory()) { // 判断是文件还是目录
			if (f.listFiles().length == 0) { // 若目录下没有文件则直接返回，这里不再删除根目录
				return;
			} else { // 若有则把文件放进数组，并判断是否有下级目录
				File delFile[] = f.listFiles();
				int i = f.listFiles().length;
				for (int j = 0; j < i; j++) {
					if (delFile[j].isDirectory()) {
						delter(delFile[j].getAbsolutePath()); // 递归调用del方法并取得子目录路径
						System.out.println("SHUACHUWENJIAN");
					}
					if (delFile[j].getName().substring(0, 6).equals("000085")) {
						delFile[j].delete();
					} // 删除文件，或者递归传入的子目录
				}
			}
		}
	}

	public static void main(String[] args) {
		LAChargeJsonWriteFTPTask tt = new LAChargeJsonWriteFTPTask();
		tt.run();

		// String query02 =
		// "{\"reqData\":{\"insuredFlag\":\"03\",\"idType\":\"0101\",\"customerId\":\"370829198812291023\",\"minAmt\":\"1000\",\"lossDay\":\"7\",\"serialNo\":\"999999201702180302110001\"}}";
		// System.out.println();
		// try {
		// JSONObject jsonObject = new JSONObject();
		// JSONObject
		// jsonObect1=jsonObject.fromObject(query02).getJSONObject("reqData");
		// //
		// System.out.println("1:"+jsonObject.fromObject(query02).getJSONObject("reqData"));
		// System.out.println(jsonObect1.getString("insuredFlag"));

		// String aString = jsonObject.getString("reqData");
		// System.out.println(aString);
		// JSONObject jsonObject1 = new JSONObject();
		// System.out.println("2:"+jsonObject1.toString());
		// String idType = jsonObject1.getString("idType");
		// System.out.println(idType);
		// String serialNo = jsonObject1.getString("serialNo");
		// System.out.println(serialNo);
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

	}

	/**
	 * 修改生成的文件夹名称 （添加MD5后的名称）
	 * 
	 * @param cFilePath
	 *            生成的原文件路径
	 * @param cChargeType
	 *            手续费类型
	 * @param cToCompare
	 *            发给财或寿类型
	 * @return
	 */
	private boolean changeFileName(String cFilePath, String cChargeType,
			String cToCompare) {
		File tOldFile = new File(cFilePath);
		if (!tOldFile.exists()) {
			System.out.println("原文件夹不存在！");
			return false;// 重命名文件不存在
		}
		String tMD5 = "";
		try {
			FileInputStream tFileInputStream = new FileInputStream(cFilePath);
			// tMD5 =
			// DigestUtils.md5Hex(tFileInputStream.toString()).substring(8,
			// 24);
			tMD5 = getMD5(tFileInputStream).substring(8, 24);

			tFileInputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}
		File tNewFile = new File(mURL + "000085" + cChargeType
				+ mCurrentDate.replaceAll("-", "") + tMD5 + cToCompare + ".txt");
		if (tNewFile.exists())// 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
		{
			System.out.println("新文件已经存在！");
			return false;// 重命名文件不存在
		}
		boolean flag = tOldFile.renameTo(tNewFile);
		return flag;
	}

	/**
	 * 将生成文件传送到财险的ftp地址10.133.212.81的/home/Weblogic/commissionData/receiveData/healthData目录下.
	 */
	private void FtpTransferFiles() {
		FTPTool tFTPTool = new FTPTool(mIP, mUserName, mPassword, mPort,
				mTransMode);
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println(tFTPTool.getErrContent(1));
			} else {
				String[] tFileNames = new File(mURL).list();
				if (tFileNames == null) {
					System.out.println("集团交叉销售Error:获取目录下的文件失败，请检查该目录是否存在！");
				} else {
					for (int i = 0; i < tFileNames.length; i++) {
						if ("000085".equals(tFileNames[i].substring(0, 6))) {
							// 财险报送文件夹
							tFTPTool.upload(
									mFilePoPoth,
									mURL + tFileNames[i]);
							System.out.println("文件上传成功了！");
						}
					}
				}
			}
		} catch (SocketException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			tFTPTool.logoutFTP();
		}
	}

	public static String getMD5(InputStream is)
			throws NoSuchAlgorithmException, IOException {
		StringBuffer md5 = new StringBuffer();
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] dataBytes = new byte[1024];

		int nread = 0;
		while ((nread = is.read(dataBytes)) != -1) {
			md.update(dataBytes, 0, nread);
		}
		;
		byte[] mdbytes = md.digest();

		// convert the byte to hex format
		for (int i = 0; i < mdbytes.length; i++) {
			md5.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16)
					.substring(1));
		}
		return md5.toString();
	}

}
