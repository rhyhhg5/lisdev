package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title:定额单证核销类</p>
 * <p>Description:若保单已签单，则单证状态更新为正常回销（2）</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: SinoSoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class CertifyDVerifyTask extends TaskThread
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    private MMap map = new MMap();

    public CertifyDVerifyTask()
    {
    }

    /**
    * 支持手动执行待办件超时日处理程序
    * 只能操纵本机构及本机构的下级机构的任务
    * */
   public void oneCompany(GlobalInput g)
   {
       mGI = g;
       run();
   }

    /**
     * 执行催收任务
     */
    public void run()
    {
        getGI();

        if(!dealData())
        {
            return;
        }

        if(!submit())
        {
            return;
        }
    }

    /**
     * 提交数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, ""))
        {
            mErrors.copyAllErrors(p.mErrors);
            System.out.println(p.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "CertifyDVerifyTask";
            tError.functionName = "run";
            tError.errorMessage = "更新单证状态时提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * 更新单证状态
     * @return boolean
     */
    private boolean dealData()
    {
        SSRS tSSRS = getLZCardSetNeedVerify();
        if(tSSRS == null)
        {
            return true;
        }

        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
//        for(int i = 1; i <= 1; i++)
        {
            String sql = "update LZCard "
                         + "set State = '2', "  //正常回销
//                         + "   Operator = '" + mGI.Operator + "', "
                         + "   ModifyDate = '" + mCurDate + "', "
                         + "   ModifyTime = '" + mCurTime + "' "
                         + "where CertifyCode = '" + tSSRS.GetText(i, 1) + "' "
                         + "   and SubCode = '" + tSSRS.GetText(i, 2) + "' "
                         + "   and RiskCode = '" + tSSRS.GetText(i, 3) + "' "
                         + "   and RiskVersion = '" + tSSRS.GetText(i, 4) + "' "
                         + "   and StartNo = '" + tSSRS.GetText(i, 5) + "' "
                         + "   and EndNo = '" + tSSRS.GetText(i, 6) + "' ";
            map.put(sql, SysConst.UPDATE);

            System.out.println();
            for(int j = 1; j <= tSSRS.getRowData(i).length; j++)
            {
                System.out.print(tSSRS.GetText(i, j) + ", ");
            }
        }
        System.out.println();

        return true;
    }

    /**
     * getLZCardSetNeedVerify
     *
     * @return LZCardSet
     */
    private SSRS getLZCardSetNeedVerify()
    {
        String sql = "select t.CertifyCode, t.SubCode, t.RiskCode, "
                     + "   t.RiskVersion, t.StartNo, t.EndNo "
                     + "from ( "
                     + "   select a.*, b.CardNo "
                     + "   from LZCard a left join LZCardNumber b "
                     + "   on a.SubCode = b.CardType "
                     + "      and a.StartNo = b.CardSerNo "
                     + "   where a.State = '12' "  //单证状态为已录入
                     + "      and a.SubCode in(select SubCode from LMCertifyDes "
                     + "                     where CertifyClass = 'D') "  //定额单证
                     + ") t "
                     + "left join LCCont b "
                     + "on t.CardNo = b.PrtNo "
                     + "where b.AppFlag = '1' ";  //保单已签单
        System.out.println(sql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);

        if(tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "CertifyDVerifyTask";
            tError.functionName = "getLZCardSetNeedVerify";
            tError.errorMessage = "查询需修改为正常回校状态的单证时出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if(tSSRS.getMaxRow() == 0)
        {
            System.out.println("没有查询到需要更改状态的单证");
        }

        return tSSRS;
    }

    /**
     * 若没有传入操作人，则默认为服务器
     */
    private void getGI()
    {
        if(mGI == null)
        {
            mGI = new GlobalInput();
            mGI.Operator = "Server";
            mGI.ComCode = "86";
            mGI.ManageCom = "86";
        }
    }

    public static void main(String arg[])
    {
        CertifyDVerifyTask task = new CertifyDVerifyTask();
        GlobalInput g = new GlobalInput();
        g.Operator = "endor";
        g.ComCode = "86";

        task.run();
        if (task.mErrors.needDealError())
        {
            System.out.println("更新单证状态失败："
                               + task.mErrors.getErrContent());
        }
        else
        {
            System.out.println("成功");
        }
    }
}
