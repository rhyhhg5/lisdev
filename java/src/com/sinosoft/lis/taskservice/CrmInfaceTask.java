package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.db.LICrmLogDB;
import com.sinosoft.lis.crminterface.CrmTranOtherDataBL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.crminterface.CrmTranDataBL;
import com.sinosoft.lis.vschema.LICrmLogSet;
import java.io.FileNotFoundException;
import java.io.IOException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.crminterface.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.health.LHQuesKineCRMBL;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CrmInfaceTask extends TaskThread{
        public CrmInfaceTask() {
        }
        public void run()
        {
        	System.out.println(".....................start running in run()!");
            CrmInfaceTask tCrmInfaceTask = new CrmInfaceTask();
            try {
				tCrmInfaceTask.DealDate();
			} catch (FileNotFoundException e) {
				// TODO 自动生成 catch 块
				e.printStackTrace();
			} catch (IOException e) {
				// TODO 自动生成 catch 块
				e.printStackTrace();
			}
			System.out.println(".....................end running!");

        }
        public boolean DealDate() throws FileNotFoundException, IOException {
            //日志表
            CrmTranDataBL mCrmTranDataBL = new CrmTranDataBL();
            if (!mCrmTranDataBL.submitData())
                 return false;
            String aBatch = mCrmTranDataBL.GetBatchNo();
            //中间表
            CrmTranOtherDataBL aCrmTranOtherDataBL = new CrmTranOtherDataBL();
            String tSql="select * from LICrmLog where batchno = '"+aBatch+"'";
            LICrmLogSet aLICrmLogSet = new LICrmLogSet();
            LICrmLogDB aLICrmLogDB = new LICrmLogDB();
            aLICrmLogSet=aLICrmLogDB.executeQuery(tSql);
            VData tVData = new VData();
            tVData.add(aLICrmLogSet);
            aCrmTranOtherDataBL.submitData(tVData);
            //报案回写
            String mSql = "select * from LICrmClaim where batchno = '"+aBatch+"'";
            LICrmClaimSet mLICrmClaimSet = new LICrmClaimSet();
            LICrmClaimDB mLICrmClaimDB = new LICrmClaimDB();
            mLICrmClaimSet = mLICrmClaimDB.executeQuery(mSql);
            VData mVData = new VData();
            if (mLICrmClaimSet.size()>0)
            {
                mVData.add(mLICrmClaimSet);
                LLMainAskBL tLLMainAskBL = new LLMainAskBL();
                tLLMainAskBL.submitData(mVData);
            }
            //保全变更回写
            mSql = "";
            mVData = null;
            mSql = "select * from LICrmPolicyChange where batchno = '"+aBatch+"'";
            LICrmPolicyChangeSet mLICrmPolicyChangeSet = new LICrmPolicyChangeSet();
            LICrmPolicyChangeDB mLICrmPolicyChangeDB = new LICrmPolicyChangeDB();
            mLICrmPolicyChangeSet = mLICrmPolicyChangeDB.executeQuery(mSql);
             VData data = new VData();
            if (mLICrmPolicyChangeSet.size()>0)
           {
               GlobalInput gi = new GlobalInput();
               gi.Operator = "endor0";
               gi.ComCode = "86";
               data.add(mLICrmPolicyChangeSet);
               data.add(gi);
               CrmBQPolicyChangeBL bl = new CrmBQPolicyChangeBL();
               if(!bl.submitData(data, ""))
               {
                   System.out.println(bl.mErrors.getErrContent());
               }
            }
            //保全缴费回写
           mSql = "";
           mVData = null;
           mSql = "select * from LICrmChangePay where batchno = '"+aBatch+"'";
           LICrmChangePaySet mLICrmChangePaySet = new LICrmChangePaySet();
           LICrmChangePayDB mLICrmChangePayDB = new LICrmChangePayDB();
           mLICrmChangePaySet = mLICrmChangePayDB.executeQuery(mSql);
           VData Changdata = new VData();
           if (mLICrmChangePaySet.size()>0)
          {
              GlobalInput gi = new GlobalInput();
              gi.Operator = "crm";
              gi.ComCode = "86";
              Changdata.add(mLICrmChangePaySet);
              Changdata.add(gi);
              BQPayInterfaceBL mBQPayInterfaceBL = new BQPayInterfaceBL();
              mBQPayInterfaceBL.submitData(Changdata, "ChangePayListFeed");
           }
            //续期缴费回写
           mSql = "";
           mVData = null;
           mSql = "select * from LICrmRenewalPay where batchno = '"+aBatch+"'";
           LICrmRenewalPaySet mLICrmRenewalPaySet = new LICrmRenewalPaySet();
           LICrmRenewalPayDB mLICrmRenewalPayDB = new LICrmRenewalPayDB();
           mLICrmRenewalPaySet = mLICrmRenewalPayDB.executeQuery(mSql);
           VData Renewadata = new VData();
           if (mLICrmRenewalPaySet.size()>0)
          {
              GlobalInput gi = new GlobalInput();
              gi.Operator = "crm";
              gi.ComCode = "86";
              Renewadata.add(mLICrmRenewalPaySet);
              Renewadata.add(gi);
              BQPayInterfaceBL mBQPayInterfaceBL = new BQPayInterfaceBL();
              mBQPayInterfaceBL.submitData(Renewadata, "RenewalPayListFeed");
           }
            //健管反馈
             mSql = "";
             mSql = "select * from LICrmLog where batchno = '"+aBatch+"' and filecode ='LHQuesImportMain'";
             LICrmLogSet mLICrmLogSet = new LICrmLogSet();
             LICrmLogDB mLICrmLogDB = new LICrmLogDB();
             mLICrmLogSet = mLICrmLogDB.executeQuery(mSql);
             if (mLICrmLogSet.size()>0)
             {
               LHQuesKineCRMBL tLHQuesKineCRMBL=new LHQuesKineCRMBL();
               tLHQuesKineCRMBL.TaskExecEntry();
             }
            return true;
        }
        public static void  main(String[] args) throws FileNotFoundException,
            IOException {
            CrmInfaceTask mCrmInfaceTask = new CrmInfaceTask();
            mCrmInfaceTask.DealDate();
        }

}
