package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDUserSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 定期处理三个月未登陆用户</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class UserManBL extends TaskThread
{
    public UserManBL()
    {

    }

    public void run()
    {
        System.out.println("---定期处理三个月未登陆用户开始---");
        dealData();
        System.out.println("---定期处理三个月未登陆用户结束---");
    }

    private boolean dealData()
    {

        String sql = "select a.operator "
                + "from lduserlog a,lduser b "
                + "where a.operator=b.usercode and b.userstate<>'1' and b.SuperPopedomFlag<>'1' and b.usercode not in ('it001','001','picch')"
                + "group by a.operator having current_date-max(a.makedate)>300 with ur";
        //String sql = "select usercode from lduser where usercode='qy002' with ur";
        System.out.println(sql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);

        LDUserDB tLDUserDB = new LDUserDB();
        if (tSSRS.getMaxRow() == 0)
        {
            System.out.println("不存在超过3个月未登录的普通用户");
        }
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            System.out.println("正在查询" + tSSRS.GetText(i, 1) + "是否有正在处理的业务");
            if (!checkData(tSSRS.GetText(i, 1)))
            {
                continue;
            }
            tLDUserDB.setUserCode(tSSRS.GetText(i, 1));
            if (tLDUserDB.getInfo())
            {
                tLDUserDB.setUserState("1");
                tLDUserDB.setModifydate(PubFun.getCurrentDate());
                tLDUserDB.setModifytime(PubFun.getCurrentTime());
                tLDUserDB.setModifyOperator("sys");
                tLDUserDB.update();
                //System.out.println("失效！");
            }
            else
            {
                return false;
            }
        }
        return true;
    }

    private boolean checkData(String userCode)
    {
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String sql = "select 1 from lgworktrace a "
                + "where a.workboxno=(select workboxno from lgworkbox where ownerno='"
                + userCode
                + "') "
                + "and exists (select 1 from lgwork b where statusno not in ('5','8') and a.workno=b.workno ) with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理保全");
            return false;
        }

        sql = "select 1 from LGGroupMember where supermanager='" + userCode
                + "' with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户存在业务下级");
            return false;
        }

        sql = "select caseno from llcase "
                + "where handler='"
                + userCode
                + "' and endcasedate is null and rgtstate not in ('09','11','12','14') "
                + "union all "
                + "select a.caseno from llcase a,LLClaimUWMain b where a.caseno=b.caseno "
                + "and a.endcasedate is null and a.rgtstate in ('05','10') and b.AppClmUWer = '"
                + userCode + "' with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理理赔案件");
            return false;
        }

        sql = "select a.surveyno from llInqapply a,llcase b,llsurvey c where a.otherno=b.caseno and b.caseno=c.otherno "
                + "and a.surveyno=c.surveyno and  a.Dipatcher='"
                + userCode
                + "' and c.surveyflag <>'3' and b.rgtstate ='07' "
                + "union all "
                + "select a.surveyno from llsurvey a,llcase b where a.otherno=b.caseno and  a.confer='"
                + userCode
                + "' and a.surveyflag <>'3' "
                + "and b.rgtstate ='07' "
                + "union all "
                + "select a.surveyno from llsurvey a,llinqfee b where a.surveyno = b.surveyno and a.confer='"
                + userCode
                + "' "
                + "and b.uwstate <>'1' "
                + "union all "
                + "select a.surveyno from llsurvey a,llinqfee b,llinqapply c "
                + "where a.surveyno=b.surveyno and a.surveyno=c.surveyno and a.confer<>c.Dipatcher "
                + "and b.uwstate not in ('1','4') and c.Dipatcher='"
                + userCode
                + "' "
                + "union all "
                + "select a.surveyno from llInqapply a,llcase b,llsurvey c where a.otherno=b.caseno and b.caseno=c.otherno "
                + "and a.surveyno=c.surveyno and  a.inqper='"
                + userCode
                + "' and c.surveyflag <>'3' and b.rgtstate ='07' with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理理赔调查案件调查费");
            return false;
        }

        sql = "select 1 from llprepaidclaim a where handler='" + userCode
                + "' and RgtState in ('01','02','03') " + "union all "
                + "select 1 from llprepaidclaim a,LLPrepaidUWMain b "
                + "where a.prepaidno=b.prepaidno "
                + "and a.rgtstate = '03' and b.dealer='" + userCode
                + "' union all "
                + "select 1 from llprepaidclaim a where dealer='" + userCode
                + "' and RgtState ='04' with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理理赔预付赔款");
            return false;
        }
        sql = "select 1 from llcontdeal a,lgwork b where "
                + "a.edorno=b.workno and b.statusno not in ('5','8') and appoperator='"
                + userCode
                + "' "
                + "union all "
                + "select 1 from llcontdeal a,lgwork b where "
                + "a.edorno=b.workno and b.statusno not in ('5','8') and dealer='"
                + userCode + "' with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理理赔合同处理");
            return false;
        }
        sql = "select 1 from llclaimuser where upusercode='" + userCode
                + "' with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户存在业务下级");
            return false;
        }

        sql = "select 1 from lcgrpcont where InputOperator = '"
                + userCode
                + "' and ApproveCode is null and ApproveDate is null and UwOperator is null and UwFlag is null with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理核保保单");
            return false;
        }
        sql = "select 1 from lccont where InputOperator = '"
                + userCode
                + "' and ApproveCode is null and ApproveDate is null and UwOperator is null and UwFlag is null and ContType = '1' with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理核保保单");
            return false;
        }
        sql = "select 1 from lcgrpcont where ApproveCode = '" + userCode
                + "' and UwOperator is null and UwFlag is null with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理核保保单");
            return false;
        }
        sql = "select 1 from lccont where ApproveCode = '"
                + userCode
                + "' and UwOperator is null and UwFlag is null and ContType = '1' with ur";

        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理核保保单");
            return false;
        }
        sql = "select 1 from lcgrpcont where UwOperator = '" + userCode
                + "' and (UwFlag not in ('9','4','a','1')) with ur";

        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理核保保单");
            return false;
        }
        sql = "select 1 from lccont where UwOperator = '"
                + userCode
                + "' and (UwFlag not in ('9','8','4','a','1') or UwFlag is Null) and ContType = '1' with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理核保保单");
            return false;
        }
        sql = "select 1 from lcgrpcont where InputOperator = '"
                + userCode
                + "' and ApproveCode is null and ApproveDate is null and UwOperator is null and UwFlag ='0' "
                + "union all "
                + "select 1 from lccont where Operator = '"
                + userCode
                + "' and ApproveCode is null and ApproveDate is null and UwOperator is null and UwFlag ='0' and ContType = '1'";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户正在处理核保保单");
            return false;
        }
        sql = "select 1 from LDUser a, LDUWUser b "
                + "where a.UserCode = b.UserCode and b.UpUserCode = '"
                + userCode + "' with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户存在下级核保师");
            return false;
        }
        sql = "select 1 from lzcard a where receivecom='B"
                + userCode
                + "' and state  not in ('2','3','4','5','6') "
                + "and exists (select 1 from lmcertifydes where certifycode=a.certifycode and certifyclass='P') with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "该用户手中还有未下发的单证");
            return false;
        }
        sql = "select 1 from lwmission a where  ActivityID='0000001180' and defaultoperator='"
                + userCode
                + "'"
                + " and exists (select 1 from lgwork where workno=a.missionprop1 and statusno not in ('5','8')) with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户存在未结案的核保工单");
            return false;
        }
        sql = "select 1 from lzcard a where receivecom='B"
                + userCode
                + "' and state  not in ('2','4','5','6') "
                + "and exists (select 1 from lmcertifydes where certifycode=a.certifycode and certifyclass='D') with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "该用户手中还有未下发的单证");
            return false;
        }
        sql = "select 1 from LGAutoDeliverRule where target=(select workboxno from lgworkbox where ownerno='"
                + userCode + "') with ur";
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(userCode + "用户在系统中存在目标信息");
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {

        UserManBL tUserManBL = new UserManBL();
        tUserManBL.run();

    }
}
