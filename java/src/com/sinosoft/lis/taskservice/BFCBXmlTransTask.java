package com.sinosoft.lis.taskservice;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class BFCBXmlTransTask extends TaskThread {

	private ExeSQL mExeSQL = new ExeSQL();

	// TransInfo 标识信息
	private Element tPACKET = null;
	
    //TransInfo 标识信息
	private Element tHEAD = null;
	
    //TransInfo 标识信息
	private Element tBODY = null;

	// 保单信息 PolicyInfo
	private Element tItems = null;

	// 分单层数据 InsuredsInfo
	private Element tItem = null;

	// 保单信息SQL
	private String mGrpContInfoSQL = "";

	// 分单层数据SQL
	private String mContInfosSQL = "";

	// 保单信息SSRS
	private SSRS tGrpContInfoSSRS = null;

	// 分单层数据SSRS
	private SSRS tContInfosSSRS = null;

	// 保单数
	private int GrpContInfolen = 0;

	// 分单数
	private int ContInfoslen = 0;

	
	/** 应用路径 */
	private String mURL = "";

	/** 文件路径 */
	private String mFilePath = "";
	
	private String mStartDate="";

	// 执行任务
	public void run() {
		
		getInputData();
		
		dealData();
	}
	
	private void getInputData() {

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);
		//测试使用 本地地址
//		mURL = "E:/";
		mURL = mURL + "vtsfile/";

		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}
		
		//获取提数日期
		tSQL = "select Year(current date - 1 month) from dual where 1=1 ";
		mStartDate = new ExeSQL().getOneValue(tSQL);
		mStartDate = mStartDate + "-1-1";

	}
	
	private boolean getStartRun() {
		String isRun = new ExeSQL()
				.getOneValue("select 1 from ldcode where codetype='BFCBSendDay' and trim(code) = Trim(Char(Day(current date)))");
		if (!"1".equals(isRun)) {
			return false;
		}
		return true;
	}

	public boolean dealData() {
		
		if (!getStartRun()) {
			return false;
		}

		mGrpContInfoSQL =  " select distinct grpcontno  "
			+ " from lcpol "
			+ " where riskcode='160308' "
			+ " and appflag='1' "
			+ " and conttype='2' "
			+ " and managecom like '8611%' "
			+ " and cvalidate >='"+mStartDate+"'"
//			+ " and grpcontno<>'00218104000002' "
			+ " order by grpcontno "
			;

		tGrpContInfoSSRS = mExeSQL.execSQL(mGrpContInfoSQL);
		GrpContInfolen = tGrpContInfoSSRS.getMaxRow();

		if (GrpContInfolen < 1) {
			return true;
		}

		for (int i = 1; i <= GrpContInfolen; i++) {
			tPACKET = new Element("PACKET");
			Document Doc = new Document(tPACKET);
			tPACKET.addAttribute("type", "REQUEST");
			tPACKET.addAttribute("version", "1.0");
			tHEAD = new Element("HEAD");
			tHEAD.addContent(new Element("REQUEST_TYPE").setText("TB"));
			tHEAD.addContent(new Element("TRANSACTION_NUM").setText("TB"));
			tPACKET.addContent(tHEAD);
			tBODY = new Element("BODY");
			
			tItems = new Element("Items");
			
			mContInfosSQL =  " select lcp.appntno,lcp.appntname,lcp.grpcontno,lcp.contno,lcp.riskcode,lcp.cvalidate,lcp.enddate - 1 day,lci.name,lci.idno "
			 + " from lcpol lcp,lcinsured lci "
			 + " where lcp.riskcode='160308' "
			 + " and lcp.appflag='1' "
			 + " and lcp.contno=lci.contno  "
			 + " and lcp.insuredno=lci.insuredno "
			 + " and lcp.grpcontno='"+tGrpContInfoSSRS.GetText(i, 1)+"' ";

			tContInfosSSRS = mExeSQL.execSQL(mContInfosSQL);
			ContInfoslen = tContInfosSSRS.getMaxRow();
			for (int j = 1; j <= ContInfoslen; j++) {
				tItem = new Element("Item");
				tItem.addContent(new Element("AppntNo")
						.setText(tContInfosSSRS.GetText(j, 1)));
				tItem.addContent(new Element("GrpName")
				        .setText(tContInfosSSRS.GetText(j, 2)));
				tItem.addContent(new Element("GrpContNo")
						.setText(tContInfosSSRS.GetText(j, 3)));
				tItem.addContent(new Element("ContNo")
						.setText(tContInfosSSRS.GetText(j, 4)));
				tItem.addContent(new Element("RiskCode")
						.setText(tContInfosSSRS.GetText(j, 5)));
				tItem.addContent(new Element("CValiDate")
						.setText(tContInfosSSRS.GetText(j, 6)));
				tItem.addContent(new Element("CInValiDate")
						.setText(tContInfosSSRS.GetText(j, 7)));
				tItem.addContent(new Element("InsuredName")
						.setText(tContInfosSSRS.GetText(j, 8)));
				tItem.addContent(new Element("IDNo")
						.setText(tContInfosSSRS.GetText(j, 9)));
				tItems.addContent(tItem);
			}
			tBODY.addContent(tItems);
			tPACKET.addContent(tBODY);
			

			mFilePath = mURL + "PH_TB_" + tGrpContInfoSSRS.GetText(i, 1) +"_"+ PubFun.getCurrentDate2() +"_"+ PubFun.getCurrentTime2() +".xml";
			XMLOutputter outputter = new XMLOutputter("  ", true, "GBK");

			try {
				outputter.output(Doc, new FileOutputStream(mFilePath));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//本地测试时需使用
//			mFilePath=mFilePath.replace("/", "\\");
//			System.out.println(mFilePath);
			
			if(!sendXML(mFilePath)){
			  System.out.println("发送文件失败！团单号为："+tGrpContInfoSSRS.GetText(i, 1));
			}
			
			//本地测试时需使用
//			mFilePath=mFilePath.replace("\\", "/");
			File tFile = new File(mFilePath);
			tFile.delete();

		}

		return true;
	}

	private boolean sendXML(String cXmlFile){

		String getIPPort = "select codename ,codealias from ldcode where codetype='BFCBSend' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='BFCBSend' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			System.out.println("FTP服务器IP、用户名、密码、端口都不能为空，否则无法发送xml");
			return false;
		}
		
		String tIP = tIPSSRS.GetText(1, 1);
	    String tPort = tIPSSRS.GetText(1, 2);
	    String tUserName = tUPSSRS.GetText(1, 1);
	    String tPassword = tUPSSRS.GetText(1, 2);
		
//	    String tIP = "1.85.41.115";
//	    String tPort = "21";
//	    String tUserName = "piccftp01";
//	    String tPassword = "*dykj0223!";
	    
		FTPTool tFTPTool = new FTPTool(tIP, tUserName,
				tPassword, Integer.parseInt(tPort));
		
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println("登录ftp服务器失败");
				return false;
			}
		} catch (SocketException ex) {
			System.out.println("无法登录ftp服务器");
			return false;
		} catch (IOException ex) {
			System.out.println("无法读取ftp服务器信息");
			return false;
		}
		
		
		if (!tFTPTool.uploadTMP(".", cXmlFile)) {
			System.out.println("上载文件失败!");
			return false;
		}
		
		tFTPTool.logoutFTP();

		return true;
	}

	public static void main(String[] args) {

		BFCBXmlTransTask t = new BFCBXmlTransTask();
		t.run();
//		String a = "E:\\vtsfile\\PH_TB_0021810400000220160309_160428.xml";
//		t.sendXML(a);
		
//		File tFile = new File(a);
//		tFile.delete();
	
//		File tFile = new File("F:\\PH_TB_20150907204523.xml");
//		boolean e=tFile.exists();
//		System.out.println(e);
		

	}

}
