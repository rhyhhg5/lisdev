package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.agentcalculate.AgentWageInitialize;
import com.sinosoft.lis.agentcalculate.AgentWageOfALLData;
import com.sinosoft.lis.agentcalculate.AgentWageOfCodeDescribe;
import com.sinosoft.lis.db.LAWageNewLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAWageNewLogSchema;
import com.sinosoft.lis.vschema.LAWageNewLogSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

 /**
  * 批处理优化后的批处理配置
  * @author yangyang  2017-8-28
  *
  */
public class LACommisionNewTask extends TaskThread {

	/**今天提取数据的日期，默认为前一天，即昨天 */
	private String mTMakeDate= PubFun.calDate(PubFun.getCurrentDate(), -1, "D", "");
	
	/**昨天提取数据的日期 ，默认为前一天，即前天 */
	private String mTMakeDateBefore= PubFun.calDate(mTMakeDate, -1, "D", "");

	/**今天要提取数据对应的薪资月*/
	private String mWageno = mTMakeDate.replaceAll("-", "").substring(0, 6);
	
    public void run() 
    {
    	System.out.println("LACommisionNewTask-->批处理开始执行:"+PubFun.getCurrentTime());
    	long tStartTime = new Date().getTime();
    	if(!checkLog())
    	{
    		return;
    	}
    	//说明是新的月份，需要向日志表中插入一条数据
    	if(mTMakeDate.endsWith("-01"))
    	{
    		if(!insertLog(mWageno,mTMakeDate))
    		{
    			System.out.println("LACommisionNewTask-->薪资月"+mWageno+"插入(insert)日期表日志表中失败,请查明原因!");
    			return;
    		}
    	}
    	else
    	{
    		if(!updateLog(mWageno,mTMakeDate))
			{
    			System.out.println("LACommisionNewTask-->薪资月"+mWageno+"更新（update）日期表日志表失败,请查明原因!");
    			return;
			}
    	}
    	GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.ManageCom = "86";
    	tGlobalInput.Operator = "sys";
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue(AgentWageOfCodeDescribe.WAGENO_TAMKEDATE, mTMakeDate);
    	VData tVData = new VData();
    	tVData.add(tTransferData);
    	tVData.add(tGlobalInput);
    	AgentWageOfALLData tAgentWageOfALLData = new AgentWageOfALLData();
    	tAgentWageOfALLData.submitData(tVData, "sys");
    	//更新ldmaxno中notype=commisionsnnew的值
    	ExeSQL tExeSQL = new ExeSQL();
    	tExeSQL.execUpdateSQL("update ldmaxno set maxno = maxno+"+AgentWageInitialize.getmUseNo()+" where notype='COMMISIONSNNEW'");
    	long tEndTime = new Date().getTime();
    	System.out.println("LACommisionNewTask-->批处理执行完成:"+PubFun.getCurrentTime());
    	System.out.println("提取"+mTMakeDate+"天的数据花费时间"+(tEndTime-tStartTime)+"毫秒，大约"+(tEndTime-tStartTime)/3600/1000+"小时,共提取"+AgentWageInitialize.getmUseNo()+"条数据");
    }
    
    /**
     * 判断前天的数据提取是否正常
     * @return
     */
    private boolean checkLog()
    {
    	/**判断如果 是提取当月第一天的数据，日志表是没有数据的，此处需要判断上个月最后一天的数据是否提 取 完成*/
    	String tJudgeWageno = mWageno;
    	if(mTMakeDate.endsWith("-01"))
    	{
    		tJudgeWageno = mTMakeDateBefore.replaceAll("-", "").substring(0, 6);
    	}
    	//判断之前的数据是不是提取完成
    	String tJudgeLAWageNewLogSQL = "select * from LAWageNewLog where wageno = '"+tJudgeWageno+"' with ur";
    	LAWageNewLogDB tLAWageNewLogDB = new LAWageNewLogDB();
    	LAWageNewLogSet tLAWageNewLogSet = new LAWageNewLogSet();
    	System.out.println("LACommisionNewTask.java->>tJudgeLAWageNewLogSQL:"+tJudgeLAWageNewLogSQL);
    	tLAWageNewLogSet = tLAWageNewLogDB.executeQuery(tJudgeLAWageNewLogSQL);
    	if(null!=tLAWageNewLogSet&&0<tLAWageNewLogSet.size())
    	{
    		for(int i =1;i<=tLAWageNewLogSet.size();i++)
    		{
    			LAWageNewLogSchema tLAWageNewLogSchema = new LAWageNewLogSchema();
    			tLAWageNewLogSchema = tLAWageNewLogSet.get(i);
    			if("00".equals(tLAWageNewLogSchema.getState())||!mTMakeDateBefore.equals(tLAWageNewLogSchema.getEndDate()))
    			{
    				System.out.println("LACommisionNewTask-->这一天"+mTMakeDateBefore+"的数据没有提取完,请查明原因!");
    				return false;
    			}
    		}
    	}
    	else
    	{
    		System.out.println("LACommisionNewTask-->薪资月"+tJudgeWageno+"在日志表中没有数据,请查明原因!");
    		return false;
    	}
    	return true;
    }
    /**
     * 向日志 表中插入新的日志 数据，以配置表中有效的配置为基础插入
     * @param cWageNo   要插入的薪资月
     * @param cStartDate  开始提取数据的日期，应该为每月1号
     * @return
     */
    private boolean insertLog(String cWageNo,String cStartDate)
    {
    	String tInsertSQL ="insert into lawagenewlog select '"+cWageNo+"',SourceTableName,datatype,"
    				+ " '"+cStartDate+"','"+cStartDate+"','00',"
    				+ " '','sys',current date ,current time ,current date,current time "
    				+ "  from LACommisionConfig where state ='1' ";
    	ExeSQL tExeSQL = new ExeSQL();
    	return tExeSQL.execUpdateSQL(tInsertSQL);
    }
    /**
     * 更新日志 表的状态及提取终止日期
     * @param cWageNo  要更新日志数据对应的薪资月
     * @param cEndDate 当天要提数的数据日期，应为昨天  
     * @return
     */
    private boolean updateLog(String cWageNo,String cEndDate)
    {
    	String tUpdateSQL ="update lawagenewlog set state ='00',"
    					+ " enddate = '"+cEndDate+"' ,modifydate = current date,modifytime= current time "
    					+ " where wageno ='"+cWageNo+"'";
    	ExeSQL tExeSQL = new ExeSQL();
    	return tExeSQL.execUpdateSQL(tUpdateSQL);
    }
    public static void main(String[] args) throws Exception {
    	}
}
