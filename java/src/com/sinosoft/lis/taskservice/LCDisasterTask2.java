package com.sinosoft.lis.taskservice;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: 容灾数据质量监控服务类
 * </p>
 * <p>
 * Description: 容灾数据质量监控
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: SinoSoft
 * </p>
 * 
 * @author zhaolx
 * @version 1.0
 */

public class LCDisasterTask2 extends TaskThread {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */
	private VData mInputData;

	/** 统一更新日期 */


	private String mCurrentDate = PubFun.getCurrentDate();

	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();

	private MMap map;

	public LCDisasterTask2() {
	}

	/**
	 * 执行任务
	 */
	public void run() {
		System.out.println("开始运行容灾数据质量监控批处理");
		mInputData = new VData();
		map = new MMap();
		String tSQL1 = "delete from LCDisasterMonitoringData2 where FlagNo = 'picch' ";
		String tSQL2 = "insert into LCDisasterMonitoringData2 values('picch','"+mCurrentDate+"','"+mCurrentTime+"') ";
		map.put(tSQL1, "DELETE");
		map.put(tSQL2, "INSERT");
		mInputData.add(map);

		// 提交操作数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, "")) {
			System.out.println("出错！");
			return;
		}

	}

	public static void main(String[] args) {
		LCDisasterTask2 task=new LCDisasterTask2();
		task.run();
	}
}
