package com.sinosoft.lis.taskservice;

import com.ecwebservice.services.GetActiveCardDetail;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 1、新建表 LIActiveCardDetail ，字段为着两个报表需要的数据。
   2、新建批处理类 GetActiveCardDetail.java ，从核心数据库中提取需要的数据存储到LIActiveCardDetail 中。
   1）提取全部历史数据，共530万条。 估计要用2到20天的时间。
   2）历史数据取完后，再修改批处理逻辑，以后每天提取前一天产生的增量数据。
   3、修改页面查询逻辑，直接取LIActiveCardDetail 中数据。
 * </p>
 *
 * @author Uncle Zhanggm 20120106
 * @version 1.0
 */
public class ActiveCardDetailTask extends TaskThread
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();

    public ActiveCardDetailTask()
    {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run()
    {
    	new GetActiveCardDetail().submitData();
    }


    public static void main(String args[])
    {
        ActiveCardDetailTask a = new ActiveCardDetailTask();
        a.run();
    }
}
