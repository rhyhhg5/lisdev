package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclientybk.inf.E01;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *   保全批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 
 * @version 1.1
 */
public class YBKE01Task extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    String opr = "";
    public YBKE01Task()
    {}

    public void run()
    {
    	E01 bqTask = new E01();
    	//保全上报逻辑
    	 String SQL = "select b.EdorNo,b.EdorType,b.ContNo "
	    			+ " from lpedorapp a,lpedoritem b,lccont c,lcpol d"
	    			+ " where a.edoracceptno = b.edorno "
	    			+ " and b.contno=c.contno "
	    			+ " and c.conttype='1' "
	    			+ " and c.appflag='1' "
	    			+ " and c.contno = d.contno "
	    			+ " and d.riskcode in (select code from ldcode where codetype='ybkriskcode') "//医保卡项目险种标记
	    			+ " and a.edorstate='0' "
			    	+ " and not exists (select 1 from YBKErrorList where businessno=b.EdorNo and resultstatus='1' and transtype='E01') " //上报成功的不再上报
	    			+ " union "
	    			+ " select b.EdorNo,b.EdorType,b.ContNo "
	    			+ " from lpedorapp a,lpedoritem b,lbcont c,lbpol d"
	    			+ " where a.edoracceptno = b.edorno "
	    			+ " and b.contno=c.contno "
	    			+ " and c.conttype='1' "
	    			+ " and c.contno = d.contno "
	    			+ " and d.riskcode in (select code from ldcode where codetype='ybkriskcode') "//医保卡项目险种标记
	    			+ " and a.edorstate='0' "
			    	+ " and not exists (select 1 from YBKErrorList where businessno=b.EdorNo and resultstatus='1' and transtype='E01') " //上报成功的不再上报
	    			+ " with ur";
    	//维护上报退保数据时用
//    	String SQL = "select b.EdorNo,b.EdorType,b.ContNo "
//    			+ " from lpedorapp a,lpedoritem b,lbcont c,lbpol d"
//    			+ " where a.edoracceptno = b.edorno "
//    			+ " and b.contno=c.contno "
//    			+ " and c.conttype='1' "
//    			+ " and c.contno = d.contno "
//    			+ " and d.riskcode in (java\src\com\sinosoft\lis\taskservice\YBKBYMailTask.java) "//医保卡项目险种标记
//    			+ " and a.edorstate='0' "
//		    	+ " and not exists (select 1 from YBKErrorList where businessno=b.EdorNo and resultstatus='1' and transtype='E01') " //上报成功的不再上报
//    			+ " and c.edorno in ('20180201900001','20180201900002','20180201900003','20180201900004','20180201900005','20180201900006','20180201900007','20180201900008','20180201900009','20180201900010','20180201900011','20180201900012','20180201900013','20180201900014','20180201900015','20180201900016','20180201900017','20180201900018','20180201900019','20180201900020','20180201900021','20180201900022','20180201900023','20180201900024','20180201900025','20180201900026','20180201900027','20180201900028','20180201900029','20180201900030','20180201900031','20180201900032','20180201900033','20180201900034','20180201900035','20180201900036','20180201900037','20180201900038','20180201900039','20180201900040','20180201900041','20180201900042','20180201900043','20180201900044','20180201900045','20180201900046','20180201900047','20180201900048','20180201900049','20180201900050','20180201900051','20180201900052','20180201900053','20180201900054','20180201900055','20180201900056','20180201900057','20180201900058','20180201900059') "
//		    	+ " with ur";
    	
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String tCaseNo = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("PrtNo", tCaseNo);
    			tVData.add(tTransferData);
    	    	if(!bqTask.submitData(tVData, "YBKPT"))
    	         {
    	    		 System.out.println("保全批处理出问题了");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             continue;
    	         }else{
    	        	 System.out.println("保全批处理成功了");
    	        	 opr ="ture";
    	         }
    		}
    	}

    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
   
    public static void main(String[] args)
    {
    	YBKE01Task eTask = new YBKE01Task();
    	eTask.run();
    }
}




