package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCInsureAccTraceDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.SustainedBBala;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.vschema.LCInsureAccClassSet;
import com.sinosoft.lis.vschema.LCInsureAccTraceSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 个险万能险保单账户结算 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @version 1.0
 * @CreateDate：2007
 */
public class SustainedBTask extends TaskThread
{
    public CErrors mErrors = new CErrors();

    private String mContNo = null;

    private GlobalInput mGI = new GlobalInput();

    public SustainedBTask()
    {
        mGI.Operator = "001";
        mGI.ComCode = "86";
        mGI.ManageCom = mGI.ComCode;
    }

    public SustainedBTask(GlobalInput cGlobalInput)
    {
        mGI = cGlobalInput;
    }
    
    public void runOneContno(String contno){
    	mContNo = contno;
    	run();
    }

    public void run()
    {
        int iSuccCount = 0;
        int iFailCount = 0;
        int iWaitCount = 0;

        //查询出当前账户最后结算日期比已公布利率日期晚的账户，由于理赔或保全等原因，一个账户可能需要结算多次
        String sql = " select a.*  from lcpol a,ldcode1 b where stateflag='1' "
                     + " and a.riskcode=b.code and code1='1' "  //第一次赠送
                     + " and b.codetype='SustainedBSendTime' "
                     + " and a.riskcode in (select code from ldcode where  codetype='uli_b') "
                     + " and exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype4='4') "
                     + " and a.cvalidate+to_number(b.codealias) year<=current date "
                     + " and b.othersign='1' "  //只赠送一次持续奖金
                     + (mContNo != null ? (" and a.ContNo = '" + mContNo + "' ") : "")
//                     + " and a.contno='000313582000001' "
                     + " and not exists (select 1 from lcinsureacctrace where contno=a.contno and riskcode=a.riskcode and moneytype='B') "
                     + " union all "
                     + " select a.*  from lcpol a,ldcode1 b where stateflag='1' "
                     + " and a.riskcode=b.code and b.code1=b.othersign "	//最后一次赠送
                     + " and b.codetype='SustainedBSendTime' "
                     + (mContNo != null ? (" and a.ContNo = '" + mContNo + "' ") : "")
//                     + " and a.contno='000313582000001' "
                     + " and a.riskcode in (select distinct code from ldcode1 where  codetype='SustainedBSendTime') "
                     + " and exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype4='4') "
                     + " and (a.cvalidate+to_number(b.codealias) year + 90 days)>=current date "
//加上第一次时间校验                     + "  "
                     + " and b.othersign !='1' "	//分多次赠送持续奖金
                     + " and not exists (select 1 from lcinsureacctrace where contno=a.contno and riskcode=a.riskcode and moneytype='B' "
                     + " and paydate>=a.cvalidate+to_number(b.codealias) year) "
                     + " with ur ";
        System.out.println(sql);

        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolDB.executeQuery(sql);
        if(tLCPolSet==null||tLCPolSet.size()<=0){
        	System.out.println("没有需要分配的持续奖金险种！");
        	return ;
        }else{
        	//对每个险种进行持续奖金分配
        	for(int i=1;i<=tLCPolSet.size();i++){
        		LCPolSchema tLCPolSchema = tLCPolSet.get(i);
        		//校验被保人是否生存或发生过理赔。
        		String checkSQL = "select polno from ljagetclaim a where a.othernotype='5' " +
        		" and exists (select 1 from ljagetclaim where actugetno=a.actugetno and feefinatype='SW') " +
        		" and polno = '"+tLCPolSchema.getPolNo()+"'  group by a.polno having sum(a.pay) >0 with ur ";
        		String tIsDead=new ExeSQL().getOneValue(checkSQL);
                //判断是否已经身故,通过是否发生身故理赔来判断
                if(tIsDead!=null&&(!tIsDead.equals(""))&&(!tIsDead.equals("null"))){
                	continue ;
                }
                //校验是否有未结案的理赔或保全
                if(!canBalance(tLCPolSchema)){
                	continue;
                }
                String errInfo = "保单:" + tLCPolSchema.getContNo()
                + "被保人:" + tLCPolSchema.getInsuredNo()
                + "险种：" + tLCPolSchema.getRiskCode()
                + " 赠送持续奖金失败: ";                
                
                //校验部分领取金额小于账户期缴保费
                LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
                tLCInsureAccClassDB.setContNo(tLCPolSchema.getContNo());
                tLCInsureAccClassDB.setRiskCode(tLCPolSchema.getRiskCode());
                LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
                tLCInsureAccClassSet = tLCInsureAccClassDB.query();
                if(tLCInsureAccClassSet==null||tLCInsureAccClassSet.size()<=0){
                	System.out.println("个人万能账户不存在，无法赠送持续奖金！");
                	continue ;
                }
                LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
                tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1);
                if(tLCInsureAccClassSchema.getInsuAccBala()<=0){
                	System.out.println("部分领取金额大于等于期缴保费，无法赠送持续奖金");
                	continue ;
                }
                //一次发放的持续奖金
                
                String codeQuerySQL = "select * from LDCode1 where CodeType='SustainedBSendTime' and Code='"+tLCPolSchema.getRiskCode()+"' order by int(code1) asc ";
                LDCode1Set tLDCode1Set = new LDCode1Set();
                LDCode1DB tLDCode1DB = new LDCode1DB();
//                tLDCode1DB.setCodeType("SustainedBSendTime");
//                tLDCode1DB.setCode(tLCPolSchema.getRiskCode());
                tLDCode1Set = tLDCode1DB.executeQuery(codeQuerySQL);
                if(tLDCode1Set!=null&&tLDCode1Set.size()==1){
                	LDCode1Schema tLDCode1Schema = tLDCode1Set.get(1);
                	if("1".equals(tLDCode1Schema.getOtherSign())){
                		//调用赠送持续奖金程序处理
                		SustainedBBala tSustainedBBala = new SustainedBBala();
                        VData tVData = new VData();
                        tVData.add(mGI);
                        TransferData tTransferData = new TransferData();
                        tTransferData.setNameAndValue("LDCode1Schema", tLDCode1Schema);
                        tTransferData.setNameAndValue("LCPolSchema", tLCPolSchema);
                        tTransferData.setNameAndValue("mStartDate", tLCPolSchema.getCValiDate());
                        tTransferData.setNameAndValue("mEndDate", PubFun.getLastDate(tLCPolSchema.getCValiDate(), Integer.parseInt(tLDCode1Schema.getCodeAlias()), "Y"));
                        
                        tVData.add(tTransferData);
                		if(tSustainedBBala.submitData(tVData, "")){
                            errInfo += tSustainedBBala.mErrors.getErrContent();
                            mErrors.addOneError(errInfo);
                            System.out.println(errInfo);                			
                		}
                	}else{
                		 mErrors.addOneError("险种："+tLDCode1Schema.getCode()+"配置的持续奖金记录不全错误！");
                		 continue;
                	}
                	
                }else if(tLDCode1Set!=null&&tLDCode1Set.size()>1){
                	//分多次发放的持续奖金
                	for(int m=1;m<=tLDCode1Set.size();m++){
                		LDCode1Schema ttLDCode1Schema = tLDCode1Set.get(m);
                		//到期没有赠送过持续奖金的才会赠送
                		
                		LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
                		LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
//                		tLCInsureAccTraceDB.setContNo(tLCPolSchema.getContNo());
//                		tLCInsureAccTraceDB.setRiskCode(tLCPolSchema.getRiskCode());
//                		tLCInsureAccTraceDB.setMoneyType("B");
//                		tLCInsureAccTraceDB.setPayDate(PubFun.getLastDate(tLCPolSchema.getCValiDate(), Integer.parseInt(ttLDCode1Schema.getCodeAlias()), "Y"));
//                		tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                		
                		
                		String checkBSQL = " select * from  lcinsureacctrace where contno='"+tLCPolSchema.getContNo()+"' " +
                		" and riskcode='"+tLCPolSchema.getRiskCode()+"' and moneytype='B' and paydate>='"+
                		PubFun.getLastDate(tLCPolSchema.getCValiDate(), Integer.parseInt(ttLDCode1Schema.getCodeAlias()), "Y")
                		+"' " ;
                		
                		tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(checkBSQL);
                		
                		if(tLCInsureAccTraceSet!=null&&tLCInsureAccTraceSet.size()>0){
                   		 mErrors.addOneError("险种："+tLCPolSchema.getPolNo()+"已经赠送过持续奖金！");
                		 continue;               			
                		}else{
                			//当前时间大于等于赠送时间才赠送
                			if(CommonBL.stringToDate(PubFun.getCurrentDate()).compareTo(CommonBL.stringToDate(PubFun.getLastDate(tLCPolSchema.getCValiDate(), Integer.parseInt(ttLDCode1Schema.getCodeAlias()), "Y")))>=0){
                				//开始赠送持续奖金
                        		SustainedBBala tSustainedBBala = new SustainedBBala();
                                VData tVData = new VData();
                                tVData.add(mGI);                        		
                                TransferData tTransferData = new TransferData();
                                tTransferData.setNameAndValue("LDCode1Schema", ttLDCode1Schema);
                                tTransferData.setNameAndValue("LCPolSchema", tLCPolSchema); 
                                tTransferData.setNameAndValue("mStartDate", tLCPolSchema.getCValiDate());
                                tTransferData.setNameAndValue("mEndDate", PubFun.getLastDate(tLCPolSchema.getCValiDate(), Integer.parseInt(ttLDCode1Schema.getCodeAlias()), "Y"));                                
                                tVData.add(tTransferData);
                        		if(!tSustainedBBala.submitData(tVData, "")){
                                  errInfo += tSustainedBBala.mErrors.getErrContent();
                                  mErrors.addOneError(errInfo);
                                  System.out.println(errInfo);
                        		}
                			}
                		}
                		
                		
                	}
                }
                
                 
        	}
        }
    }

    /**
     * 结算指定保单
     * @param cContNo String
     */
    public void runOneCont(String cContNo)
    {
        mContNo = cContNo;
        run();
    }

    /**
     * 校验能否进行结算
     * 险种有未完结的理赔不能结算，保单有未完结的保全不能结算
     * @param cLCInsureAccSchema LCInsureAccSchema
     * @return boolean
     */
    private boolean canBalance(LCPolSchema tLCPolSchema)
    {
        if(!LLCaseCommon.checkClaimState(tLCPolSchema.getPolNo()))
        {
            mErrors.addOneError("险种有未结案的理赔，不能进行结算");
            return false;
        }

        LPEdorItemSet set = CommonBL.getEdorItemMayChangePrem(
        		tLCPolSchema.getContNo());
        if(set.size() > 0)
        {
            String edorInfo = "";
            for(int i = 1; i <= set.size(); i++)
            {
                edorInfo += set.get(i).getEdorNo() + " "
                    + set.get(i).getEdorType() + ", ";
            }
            mErrors.addOneError("保单有未结案的保全项目:" + edorInfo);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        SustainedBTask task = new SustainedBTask();
        task.runOneContno("005333169000001");
//    	System.out.println(PubFun.calInterval("2013-11-18", "2013-11-16", "M"));
    }
}
