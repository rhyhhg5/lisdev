package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

import utils.system;
import weblogic.j2ee.Application;

import com.sinosoft.lis.schema.LDUserSchema;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.vschema.LDUserSet;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.WriteToExcel;

public class CustomerCheckTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */
	// 当前时间
	private int tYear;
	private int MONTH;
	private int DATE;
	// 前一天时间
	private int AtYear;
	private int AMONTH;
	private int ADATE;

	private File file;

	// 执行任务
	public void run() {
		// 判断是否可以执行
		getUser();
	}

	// 提取用户
	public boolean getUser() {

		ExeSQL tEx = new ExeSQL();
		// 查询不符合规则的客户号
		String ldpersonSql = "SELECT customerno as 客户号,NAME AS 姓名,SEX AS " + "    性别 " + ",BIRTHDAY AS 生日, IDTYPE AS "
				+ " 证件类型 " + ",IDNO AS 证件号码 ,MAKEDATE AS 入机日期,MODIFYDATE AS 修改日期"
				+ " FROM ldperson A WHERE NOT EXISTS (" + "SELECT customerno as 客户号,NAME AS 姓名,SEX AS " + " 性别 "
				+ ",BIRTHDAY AS 生日, IDTYPE AS " + " 证件类型 " + ",IDNO AS 证件号码 ,MAKEDATE AS 入机日期,MODIFYDATE AS 修改日期"
				+ " FROM ldperson B " + "WHERE idtype IN ('0','5') AND MODIFYDATE =current date - 1 day "
				+ "AND SUBSTR(idno,1,2) IN ('11','12','13','14','15','21','22','23','31','32','33','34','35','36','37','41','42','43','44','45','46','50','51','52','53','54','61','62','63','64','65','71','81','82')"
				+ "AND ((LENGTH(TRIM(idno))=15 "
				+ "AND SUBSTR(TRIM(idno),7,6) = SUBSTR(TO_CHAR(birthday,'YYYYMMDD'),3) "
				+ "AND ((SUBSTR(TRIM(idno),15,1) IN ('1','3','5','7','9') AND sex = '0') OR (SUBSTR(TRIM(idno),15,1) IN ('0','2','4','6','8') AND sex = '1')))"
				+ "OR" + "(LENGTH(TRIM(idno))=18 " + "AND SUBSTR(TRIM(idno),7,8) =TO_CHAR(birthday,'YYYYMMDD')"
				+ "AND ((SUBSTR(TRIM(idno),17,1) IN ('1','3','5','7','9') AND sex = '0') OR (SUBSTR(TRIM(idno),17,1) IN ('0','2','4','6','8') AND sex = '1')) "
				+ "AND SUBSTR(TRIM(idno),18,1) IN ('1','3','5','7','9','0','2','4','6','8','X')) "
				+ " )  AND A.CUSTOMERNO = B.customerno) AND  A.idtype IN ('0','5') AND A.MODIFYDATE = current date - 1 day WITH UR ";

		SSRS ldpersonSqls = tEx.execSQL(ldpersonSql);
		int ldpersoncount = ldpersonSqls.getMaxRow();

		Calendar calendar = Calendar.getInstance();// 可以对每个时间域单独修改
		tYear = calendar.get(Calendar.YEAR);
		MONTH = calendar.get(Calendar.MONTH) + 1;
		DATE = calendar.get(Calendar.DATE);
		updateTime(tYear, MONTH, DATE);
		// 创建对象
		String[][] tToExcel = new String[ldpersoncount + 10][18];// 创建多少行多少列
		System.out.println("统计时间" + tYear + "-" + MONTH + "-" + DATE);
		// 设置头内容
		tToExcel[0][0] = "系统用户状态";
		tToExcel[1][0] = "统计机构：总公司";
		tToExcel[2][0] = "统计时间：" + tYear + "-" + MONTH + "-" + DATE;
		tToExcel[3][0] = "不可用的用户数";
		tToExcel[4][0] = ldpersoncount + "人";
		// 设置主要内容
		tToExcel[6][0] = "每日统计客户新生成的垃圾数据";
		tToExcel[7][0] = "客户号";
		tToExcel[7][1] = "姓名";
		tToExcel[7][2] = "1-女0-男";
		tToExcel[7][3] = "生日";
		tToExcel[7][4] = "0身份证5户口本";
		tToExcel[7][5] = "证件号码";
		tToExcel[7][6] = "入机日期";
		tToExcel[7][7] = "修改日期";
		// 添加主要内容
		int excelRow = 7;
		for (int row = 1; row <= ldpersoncount; row++) {
			excelRow++;
			tToExcel[excelRow][0] = ldpersonSqls.GetText(row, 1);
			tToExcel[excelRow][1] = ldpersonSqls.GetText(row, 2);
			tToExcel[excelRow][2] = ldpersonSqls.GetText(row, 3);
			tToExcel[excelRow][3] = ldpersonSqls.GetText(row, 4);
			tToExcel[excelRow][4] = ldpersonSqls.GetText(row, 5);
			tToExcel[excelRow][5] = ldpersonSqls.GetText(row, 6);
			tToExcel[excelRow][6] = ldpersonSqls.GetText(row, 7);
			tToExcel[excelRow][7] = ldpersonSqls.GetText(row, 8);
		}

		try {
			// 获取系统UI路经
			String mOutXmlPath = CommonBL.getUIRoot();
			// 获取excel表格存放路径
			String tSQL = "select sysvarvalue from ldsysvar where sysvar = 'InvalidUser' ";
			String tPath = new ExeSQL().getOneValue(tSQL);

			//mOutXmlPath = "D:/SourceCode/lisdev/ui/";
			System.out.println(mOutXmlPath);
			// 拼接excel文件名
			String name = AtYear + "-" + AMONTH + "-" + ADATE + "-" + "LDpersonNewJunkData.xls";
			String tAllName = mOutXmlPath + tPath + name;
			System.out.println("文件全路径====：" + tAllName);

			// 创建生成excel表格
			WriteToExcel t = new WriteToExcel(name);
			t.createExcelFile();
			t.setAlignment("CENTER");
			t.setFontName("宋体");
			String[] sheetName = { new String("Data") };
			t.addSheet(sheetName);
			// String[] sheetName = { new String("用户日志审核表") };
			// t.addSheetGBK(sheetName);
			t.setData(0, tToExcel);
			t.write(mOutXmlPath);

			System.out.println("备用文件已生成");

			System.out.println("开始设置文件的样式");
			// //获取文件设置格式
			FileInputStream is = new FileInputStream(new File(mOutXmlPath + name)); // 获取文件
			HSSFWorkbook wb = new HSSFWorkbook(is);
			HSSFSheet sheet = wb.getSheetAt(0); // 获取sheet
			System.out.println("已经获取备用文件数据");
			is.close();
			// 设置字体-标题
			HSSFFont titleFont = wb.createFont();
			titleFont.setFontName("微软雅黑");
			titleFont.setFontHeightInPoints((short) 15);// 设置字体大小
			// titleFont.setColor((short) 1);// 字体颜色白字
			titleFont.setColor((short) 55);
			// 设置字体-内容
			HSSFFont tFont = wb.createFont();
			tFont.setFontName("微软雅黑");
			tFont.setFontHeightInPoints((short) 10);// 设置字体大小

			// 设置样式-标题
			HSSFCellStyle titleStyle = wb.createCellStyle();
			titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			titleStyle.setFillForegroundColor(HSSFColor.BLUE_GREY.index);// 背景色
																			// 蓝色
			titleStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			titleStyle.setFont(titleFont);
			titleStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			titleStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			titleStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			titleStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

			// 第四七行样式
			HSSFCellStyle tFRowStyle = wb.createCellStyle(); // 获取样式
			tFRowStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			// tFRowStyle.setFillForegroundColor((short) 55);// 背景色
			tFRowStyle.setFillForegroundColor(HSSFColor.RED.index);
			tFRowStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			tFRowStyle.setFont(tFont);
			tFRowStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			tFRowStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tFRowStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tFRowStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
			tFRowStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
			// 第八行样式
			HSSFCellStyle BFRowStyle = wb.createCellStyle(); // 获取样式
			BFRowStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			// tFRowStyle.setFillForegroundColor((short) 55);// 背景色
			BFRowStyle.setFillForegroundColor(HSSFColor.GREEN.index);// 背景色 绿色
			BFRowStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			BFRowStyle.setFont(tFont);
			BFRowStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			BFRowStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			BFRowStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);

			//
			// 正文样式
			HSSFCellStyle tContentStyle = wb.createCellStyle(); // 获取样式
			tContentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			tContentStyle.setFont(tFont);
			tContentStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

			// 标题字体
			// HSSFFont font2 = wb.createFont();
			// font2.setFontName("宋体");
			// font2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//粗体显示
			// font2.setFontHeightInPoints((short) 14);
			// hfcStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			// hfcStyle.setFont(font);//选择需要用到的字体格式

			// 设置行高 列宽
			for (int i = 0; i < ldpersoncount + 9; i++) {
				HSSFRow row = sheet.getRow(i);
				row.setHeight((short) 500);
				for (int j = 0; j < 12; j++) {
					HSSFCell firstCell = row.getCell((short) j);
					sheet.setColumnWidth((short) j, (short) 6000);
					// firstCell.setCellStyle(tContentStyle);
				}

			}
			// 合并单元格
			sheet.addMergedRegion(new Region(0, (short) 0, 0, (short) 3));// →第一行从第一列开始合并4个单元格
			sheet.addMergedRegion(new Region(1, (short) 0, 1, (short) 3));
			sheet.addMergedRegion(new Region(2, (short) 0, 2, (short) 3));
			sheet.addMergedRegion(new Region(5, (short) 0, 5, (short) 11));
			sheet.addMergedRegion(new Region(6, (short) 0, 6, (short) 1));
			sheet.addMergedRegion(new Region(ldpersoncount + 8, (short) 0, ldpersoncount + 8, (short) 11));

			HSSFRow tRow0 = sheet.getRow((short) 0);
			for (int i = 0; i < 4; i++) {
				HSSFCell tRow0Cell = tRow0.getCell((short) i);
				tRow0Cell.setCellStyle(titleStyle);
			}

			// HSSFRow tRow1 = sheet.getRow((short) 1);
			// HSSFCell tRow1Cell = tRow1.getCell((short) 0);
			// tRow1Cell.setCellStyle(tFRowStyle);

			HSSFRow tRow3 = sheet.getRow((short) 3);
			for (int i = 0; i < 1; i++) {
				HSSFCell tRow3Cell = tRow3.getCell((short) i);
				tRow3Cell.setCellStyle(tFRowStyle);
			}

			HSSFRow tRow6 = sheet.getRow((short) 6);
			for (int i = 0; i < 2; i++) {
				HSSFCell tRow6Cell = tRow6.getCell((short) i);
				tRow6Cell.setCellStyle(tFRowStyle);
			}
			HSSFRow tRow7 = sheet.getRow((short) 7);
			for (int i = 0; i < 8; i++) {
				HSSFCell tRow7Cell = tRow7.getCell((short) i);
				tRow7Cell.setCellStyle(BFRowStyle);
			}
			System.out.println("设置文件样式已完成");
			FileOutputStream fileOut = new FileOutputStream(tAllName);
			wb.write(fileOut);
			fileOut.close();
			System.out.println("正式文件已生成");
			System.out.println("删除备份文件");
			file = new File(mOutXmlPath + name);
			if (file.exists()) {
				try {
					System.out.println("备份文件已删除");
					file.delete();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("备份文件不存在");
			}
			
			System.out.println("开始设置发送邮箱的信息");
			// 邮箱发送
			// 配置发送邮件
			 String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1),tSSRS.GetText(1, 2), "picchealth");

			// 标题，内容，附件(多附件可以用|分隔，末尾|可加可不加)
			System.out.println("字符变更前所有名称：" + mOutXmlPath + name);
			System.out.println("字符变更后所有名称：" + tAllName);

			tMailSender.setSendInf(AtYear + "年" + AMONTH + "月" + ADATE + "日'" + "客户表新生成的垃圾数据统计",
					"您好：\r\n附件是提取的" + AtYear + "年" + AMONTH + "月" + ADATE + "日'" + "客户表新生成的垃圾数据统计，请审核。", tAllName);
			// 配置接受邮件
			String tSQLRMailSql = "select codealias,codename from ldcode where codetype = 'customcheckmail' ";

			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1), tSSRSR.GetText(1, 2), null);
			// 发送邮件
			
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}
			// 删除正式文件
			file = new File(tAllName);
			if (file.exists()) {
				try {
					file.delete();
					System.out.println("删除正式文件");
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("正式文件不存在");
			}
			
		} catch (Exception ex) {
			this.mErrors.addOneError(new CError("每日用户日志审核时出错", "CustomerCheckTask", "run"));
			ex.toString();
			ex.printStackTrace();
			return false;
		}

		return true;

	}
	//生成前一天的日期
	public void updateTime(int tYear, int MONTH, int DATE) {

		switch (MONTH) {
		case 1:
			if (DATE == 01 || DATE == 1) {
				AtYear = tYear - 1;
				AMONTH = 12;
				ADATE = 31;
			} else {
				ADATE = DATE - 1;
			}
			break;
		case 2:
			if (DATE == 01 || DATE == 1) {
				AMONTH = MONTH - 1;
				ADATE = 31;
			} else {
				ADATE = DATE - 1;
			}
			break;
		case 3:
			if (DATE == 01 || DATE == 1) {
				AMONTH = MONTH - 1;
				ADATE = 28;
			} else {
				ADATE = DATE - 1;
			}
			break;
		case 4:
			if (DATE == 01 || DATE == 1) {
				AMONTH = MONTH - 1;
				ADATE = 31;
			} else {
				ADATE = DATE - 1;
			}
			break;
		case 5:
			if (DATE == 01 || DATE == 1) {
				AMONTH = MONTH - 1;
				ADATE = 30;
			} else {
				ADATE = DATE - 1;
			}
			break;
		case 6:
			if (DATE == 01 || DATE == 1) {
				AMONTH = MONTH - 1;
				ADATE = 31;
			} else {
				ADATE = DATE - 1;
			}
			break;
		case 7:
			if (DATE == 01 || DATE == 1) {
				AMONTH = MONTH - 1;
				ADATE = 30;
			} else {
				ADATE = DATE - 1;
			}
			break;
		case 8:
			if (DATE == 01 || DATE == 1) {
				AMONTH = MONTH - 1;
				ADATE = 31;
			} else {
				ADATE = DATE - 1;
			}
			break;
		case 9:
			if (DATE == 01 || DATE == 1) {
				AMONTH = MONTH - 1;
				ADATE = 31;
			} else {
				ADATE = DATE - 1;
			}
			break;
		case 10:
			if (DATE == 01 || DATE == 1) {
				AMONTH = MONTH - 1;
				ADATE = 30;
			} else {
				ADATE = DATE - 1;
			}
			break;
		case 11:
			if (DATE == 01 || DATE == 1) {
				AMONTH = MONTH - 1;
				ADATE = 31;
			} else {
				ADATE = DATE - 1;
			}
			break;
		case 12:
			if (DATE == 01 || DATE == 1) {
				AMONTH = MONTH - 1;
				ADATE = 30;
			} else {
				ADATE = DATE - 1;
			}
			break;
		}
		if (AMONTH == 0) {
			AMONTH = MONTH;
		}
		if (AtYear == 0) {
			AtYear = tYear;
		}
		System.out.println("上一天的时间-----------" + AtYear + "年" + AMONTH + "月" + ADATE + "日");
	}

	// 主方法 测试用
	public static void main(String[] args) {
		new CustomerCheckTask().run();

	}

	private boolean getStartRun() {
		String isRun = new ExeSQL().getOneValue("select Trim(Char(Day(current date)))  from dual");
		if (!"1".equals(isRun)) {
			return false;
		}
		return true;
	}
}
