package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.certify.AuditCancelBL;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LZCertifyErrorLogSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LZCertifyErrorLogSet;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class AuditCancelTask extends TaskThread {

    CErrors mErrors = new CErrors();

    MMap mMap;
    PubSubmit tPubSubmit = null;
    LZCertifyErrorLogSet setLZCertifyErrorLog = new LZCertifyErrorLogSet();
    GlobalInput mGlobalInput = new GlobalInput();

    /** 当前操作人员的机构 **/
    String manageCom = null;
    String auditFlag = "";
    private String comCode = "";
    private String operator = "";
    private VData tVData = new VData();

    public AuditCancelTask() {
    }

    /**
    * 实现TaskThread的借口方法run
    * 由TaskThread调用
    * */
   public void run()
   {
System.out.println("I come in the run().................................................");
       VData aVData = new VData();
       mGlobalInput.Operator = "AUTO";
       mGlobalInput.ComCode = manageCom;
       aVData.addElement(mGlobalInput);

       if(!auditOperate(aVData,"AUTO"))
       {
           return;
       }
   }


    public boolean auditOperate(VData pVData , String aFlag) //核销操作
    {
        String delSQL = " delete from LZCertifyErrorLog ";
        mMap=new MMap();
        mMap.put(delSQL,"DELETE");
        this.tVData.clear();
        this.tVData.add(mMap);
        tPubSubmit=new PubSubmit();
        tPubSubmit.submitData(this.tVData,"");
        //mMap=null;

        String certifyNo = "";
        String manageCom = "";
        String makeDate = "";
        String makeTime = "";

        String[][] tempdata ;
        String strSQL="";
        ExeSQL exesql = new ExeSQL ();

/**============================已完成================================**/
System.out.println("开始 第 1 种核销..................................................");
        strSQL = "select Doccode,ManageCom,MakeDate,MakeTime from db2inst1.es_doc_main where busstype='TB' and subtype='TB01'"
        +" and Doccode not in (select StartNo from LZCard where certifycode='HG01/05B' and (stateflag='5' or stateflag='6'))"
        //+" and Doccode = '16000000002'"
        ; //家庭健康保险投保书 1
        SSRS audit_HG01 = exesql.execSQL(strSQL);
        tempdata = audit_HG01.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组
        if (tempdata.length>0)
        {

            for (int i = 0; i < audit_HG01.getMaxRow(); i++)
            {
                certifyNo = tempdata[i][0];
                manageCom = tempdata[i][1];
                makeDate = tempdata[i][2];
                makeTime = tempdata[i][3];

                VData aVData = new VData();
                aVData.clear();
                AuditCancelBL auditCancelBL = new AuditCancelBL();
                aVData.addElement("HG01/05B");
                aVData.addElement(certifyNo);

                GlobalInput gInput=new GlobalInput();
                gInput.Operator = "AUTOAA";
                gInput.ComCode = manageCom;
                aVData.addElement(gInput);

                if (!auditCancelBL.autoSubmitData(aVData,"AUTOAUDIT"))
                {
                    LZCertifyErrorLogSchema errorLogSchema = new LZCertifyErrorLogSchema();

                    errorLogSchema.setErrLogNo(PubFun1.CreateMaxNo("SERIALNO", 10));

                    errorLogSchema.setCertifyCode("HG01/05B");
                    errorLogSchema.setCertifyCom(manageCom); //业务处理机构
                    errorLogSchema.setCertifyMakeData(makeDate); //单证进入业务处理日期
                    errorLogSchema.setCertifyMakeTime(makeTime); //单证进入业务处理时间

                    errorLogSchema.setStartNo(certifyNo);
                    errorLogSchema.setEndNo(certifyNo);

                    errorLogSchema.setHandleFlag("0");
                    errorLogSchema.setOperator("AUTO");

                    strSQL = "select Receivecom, havenumber,stateFlag from LZCard a,LMCertifyDes b where a.CertifyCode = b.CertifyCode and "
                      + " StartNo <= '" + certifyNo + "' AND EndNo >= '" + certifyNo
                      + "' AND StartNo <= '" + certifyNo + "' AND EndNo >= '" + certifyNo + "' "
                      + " AND a.CertifyCode = 'HG01/05B' "
                      ;
                    SSRS pSSRS= new SSRS();
                    pSSRS = exesql.execSQL(strSQL);
                    String[][] pCom = pSSRS.getAllData();

                    if (pCom.length<=0)
                    {
                        errorLogSchema.setErrorInfo("1"); //!!!
                        errorLogSchema.setPossessCom("无");
                    }
                    else
                    {
                        errorLogSchema.setPossessCom( pCom[0][0]);
                        errorLogSchema.setState(pCom[0][2]);
                        errorLogSchema.setErrorInfo(AuditCancelBL.ErrorState);
                     }

                    errorLogSchema.setMakedate(PubFun.getCurrentDate());
                    errorLogSchema.setMakeTime(PubFun.getCurrentTime());
                    errorLogSchema.setModifyDate(PubFun.getCurrentDate());
                    errorLogSchema.setModifyTime(PubFun.getCurrentTime());

                    String errorState = AuditCancelBL.ErrorState;
                    System.out.println("error: "+auditCancelBL.mErrors.getFirstError()+ " errorState: " + errorState );

                    mMap=new MMap();
                    mMap.put(errorLogSchema,"INSERT");

                    this.tVData.clear();
                    this.tVData.add(mMap);
                    tPubSubmit=new PubSubmit();
                    tPubSubmit.submitData(this.tVData, "");
                    //mMap=null;

                    continue;
                }
            }
        }

System.out.println("开始 2 种核销..................................................");
        strSQL = "select Doccode,ManageCom,MakeDate,MakeTime from db2inst1.es_doc_main where busstype='TB' and subtype='TB02'"
        +" and Doccode not in (select StartNo from LZCard where certifycode='HT01/05B' and (stateflag='5' or stateflag='6')) "
        //+" and Doccode = '18000000123'"
        ; //团险健康保险投保书 2
        SSRS audit_HT01 = exesql.execSQL(strSQL);
        tempdata=null;
        tempdata = audit_HT01.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组
        if (audit_HT01 != null && audit_HT01.getMaxRow() > 0) {

           for (int i = 0; i < audit_HT01.getMaxRow(); i++)
           {
               certifyNo = tempdata[i][0];
               manageCom = tempdata[i][1];
               makeDate = tempdata[i][2];
               makeTime = tempdata[i][3];

               VData aVData = new VData();
               aVData.clear();
               AuditCancelBL auditCancelBL = new AuditCancelBL();
               aVData.addElement("HT01/05B");
               aVData.addElement(certifyNo);

               GlobalInput gInput=new GlobalInput();
               gInput.Operator = "AUTO";
               gInput.ComCode = manageCom;
               aVData.addElement(gInput);

               if (!auditCancelBL.autoSubmitData(aVData,"AUTOAUDIT"))
               {LZCertifyErrorLogSchema errorLogSchema = new LZCertifyErrorLogSchema();

                    errorLogSchema.setErrLogNo(PubFun1.CreateMaxNo("SERIALNO", 10));

                    errorLogSchema.setCertifyCode("HT01/05B");
                    errorLogSchema.setCertifyCom(manageCom); //业务处理机构
                    errorLogSchema.setCertifyMakeData(makeDate); //单证进入业务处理日期
                    errorLogSchema.setCertifyMakeTime(makeTime); //单证进入业务处理时间

                    errorLogSchema.setStartNo(certifyNo);
                    errorLogSchema.setEndNo(certifyNo);

                    errorLogSchema.setHandleFlag("0");
                    errorLogSchema.setOperator("AUTO");

                    strSQL = "select Receivecom, havenumber,stateFlag from LZCard a,LMCertifyDes b where a.CertifyCode = b.CertifyCode and "
                      + " StartNo <= '" + certifyNo + "' AND EndNo >= '" + certifyNo
                      + "' AND StartNo <= '" + certifyNo + "' AND EndNo >= '" + certifyNo + "' "
                      + " AND a.CertifyCode = 'HT01/05B' "
                      ;
                    SSRS pSSRS= new SSRS();
                    pSSRS = exesql.execSQL(strSQL);
                    String[][] pCom = pSSRS.getAllData();

                    if (pCom.length<=0)
                    {
                        errorLogSchema.setErrorInfo("1"); //!!!
                        errorLogSchema.setPossessCom("无");
                    }
                    else
                    {
                        errorLogSchema.setPossessCom( pCom[0][0]);
                        errorLogSchema.setState(pCom[0][2]);
                        errorLogSchema.setErrorInfo(AuditCancelBL.ErrorState);
                    }

                    errorLogSchema.setMakedate(PubFun.getCurrentDate());
                    errorLogSchema.setMakeTime(PubFun.getCurrentTime());
                    errorLogSchema.setModifyDate(PubFun.getCurrentDate());
                    errorLogSchema.setModifyTime(PubFun.getCurrentTime());

                    String errorState = AuditCancelBL.ErrorState;
                    System.out.println("error: "+auditCancelBL.mErrors.getFirstError()+ " errorState: " + errorState );

                    mMap=new MMap();
                    mMap.put(errorLogSchema,"INSERT");

                    this.tVData.clear();
                    this.tVData.add(mMap);
                    tPubSubmit=new PubSubmit();
                    tPubSubmit.submitData(this.tVData, "");
                    //mMap=null;
                    continue;
                }

           }
       }
System.out.println("开始 3 种核销..................................................");
       strSQL = "select Doccode,ManageCom,MakeDate,MakeTime from db2inst1.es_doc_main where busstype='TB' and subtype='TB04'"
       +" and Doccode not in (select StartNo from LZCard where certifycode='HT03/05B' and (stateflag='5' or stateflag='6')) "
       //+" and Doccode ='xxx'"
       ; //团体询价投保书 3
       SSRS audit_HT03 = exesql.execSQL(strSQL);
       tempdata=null;
       tempdata = audit_HT03.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组

        if (tempdata.length>0) {

            for (int i = 0; i < audit_HT03.getMaxRow(); i++) {
                certifyNo = tempdata[i][0];
                manageCom = tempdata[i][1];
                makeDate = tempdata[i][2];
                makeTime = tempdata[i][3];

                VData aVData = new VData();
                aVData.clear();
                AuditCancelBL auditCancelBL = new AuditCancelBL();
                aVData.addElement("HT03/05B");
                aVData.addElement(certifyNo);

                GlobalInput gInput = new GlobalInput();
                gInput.Operator = "AUTO";
                gInput.ComCode = manageCom;
                System.out.println(
                        "业务处理机构1: ----------------------------------------------------------　"+manageCom);
                aVData.addElement(gInput);

                if (!auditCancelBL.autoSubmitData(aVData, "AUTOAUDIT")) {
                    LZCertifyErrorLogSchema errorLogSchema = new
                            LZCertifyErrorLogSchema();

                    errorLogSchema.setErrLogNo(PubFun1.CreateMaxNo("SERIALNO",
                            10));

                    errorLogSchema.setCertifyCode("HT03/05B");
                    errorLogSchema.setCertifyCom(manageCom); //业务处理机构
                    errorLogSchema.setCertifyMakeData(makeDate); //单证进入业务处理日期
                    errorLogSchema.setCertifyMakeTime(makeTime); //单证进入业务处理时间

                    errorLogSchema.setStartNo(certifyNo);
                    errorLogSchema.setEndNo(certifyNo);

                    errorLogSchema.setHandleFlag("0");
                    errorLogSchema.setOperator("AUTO");

                    strSQL = "select Receivecom, havenumber,stateFlag from LZCard a,LMCertifyDes b where a.CertifyCode = b.CertifyCode and "
                             + " StartNo <= '" + certifyNo + "' AND EndNo >= '" +
                             certifyNo
                             + "' AND StartNo <= '" + certifyNo +
                             "' AND EndNo >= '" + certifyNo + "' "
                             + " AND a.CertifyCode = 'HT03/05B' "
                             ;
                    SSRS pSSRS = new SSRS();
                    pSSRS = exesql.execSQL(strSQL);
                    String[][] pCom = pSSRS.getAllData();

                    if (pCom.length <= 0) {
                        errorLogSchema.setErrorInfo("1");
                        errorLogSchema.setPossessCom("无");
                    } else {

                        errorLogSchema.setPossessCom(pCom[0][0]);
                        errorLogSchema.setState(pCom[0][2]);
                        errorLogSchema.setErrorInfo(AuditCancelBL.ErrorState);
                    }

                    errorLogSchema.setMakedate(PubFun.getCurrentDate());
                    errorLogSchema.setMakeTime(PubFun.getCurrentTime());
                    errorLogSchema.setModifyDate(PubFun.getCurrentDate());
                    errorLogSchema.setModifyTime(PubFun.getCurrentTime());

                    String errorState = AuditCancelBL.ErrorState;
                    System.out.println("error: " +
                                       auditCancelBL.mErrors.getFirstError() +
                                       " errorState: " + errorState);
                    mMap=new MMap();
                    mMap.put(errorLogSchema, "INSERT");

                    this.tVData.clear();
                    this.tVData.add(mMap);
                    tPubSubmit=new PubSubmit();
                    tPubSubmit.submitData(this.tVData, "");
                    //mMap=null;
                    continue;
                }

            }
        }


System.out.println("开始 第 4 种核销..................................................");
       strSQL = "select prtno, managecom, makedate, maketime from lccont where cardflag='1' and conttype ='1' "
       +" and prtno not in (select StartNo from LZCard where certifycode='HG03/05B' and (stateflag='5' or stateflag='6'))"
       //+" and prtno='19000000007'"
       ;//旅行保险投保书 4
       SSRS audit_HG03 = exesql.execSQL(strSQL);
       tempdata=null;
       tempdata = audit_HG03.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组

        if (tempdata.length>0) {

            for (int i = 0; i < audit_HG03.getMaxRow(); i++) {
                certifyNo = tempdata[i][0];
                manageCom = tempdata[i][1];
                makeDate = tempdata[i][2];
                makeTime = tempdata[i][3];

                VData aVData = new VData();
                aVData.clear();
                AuditCancelBL auditCancelBL = new AuditCancelBL();
                aVData.addElement("HG03/05B");
                aVData.addElement(certifyNo);

                GlobalInput gInput = new GlobalInput();
                gInput.Operator = "AUTO";
                gInput.ComCode = manageCom;
                aVData.addElement(gInput);

                if (!auditCancelBL.autoSubmitData(aVData, "AUTOAUDIT")) { //如果没有通过自动核销则向日志中记录
                    LZCertifyErrorLogSchema errorLogSchema = new
                            LZCertifyErrorLogSchema();

                    errorLogSchema.setErrLogNo(PubFun1.CreateMaxNo("SERIALNO",
                            10));

                    errorLogSchema.setCertifyCode("HG03/05B");
                    errorLogSchema.setCertifyCom(manageCom); //业务处理机构
                    errorLogSchema.setCertifyMakeData(makeDate); //单证进入业务处理日期
                    errorLogSchema.setCertifyMakeTime(makeTime); //单证进入业务处理时间

                    errorLogSchema.setStartNo(certifyNo);
                    errorLogSchema.setEndNo(certifyNo);

                    errorLogSchema.setHandleFlag("0");
                    errorLogSchema.setOperator("AUTO");

                    strSQL = "select Receivecom, havenumber,stateFlag from LZCard a,LMCertifyDes b where a.CertifyCode = b.CertifyCode and "
                             + " StartNo <= '" + certifyNo + "' AND EndNo >= '" +
                             certifyNo
                             + "' AND StartNo <= '" + certifyNo +
                             "' AND EndNo >= '" + certifyNo + "' "
                             + " AND a.CertifyCode = 'HG03/05B' "
                             ;
                    SSRS pSSRS = new SSRS();
                    pSSRS = exesql.execSQL(strSQL);
                    String[][] pCom = pSSRS.getAllData();

                    if (pCom.length <= 0) {
                        errorLogSchema.setErrorInfo("1");
                        errorLogSchema.setPossessCom("无");
                    } else {
                        errorLogSchema.setPossessCom(pCom[0][0]);
                        errorLogSchema.setState(pCom[0][2]);
                        errorLogSchema.setErrorInfo(AuditCancelBL.ErrorState);
                    }

                    errorLogSchema.setMakedate(PubFun.getCurrentDate());
                    errorLogSchema.setMakeTime(PubFun.getCurrentTime());
                    errorLogSchema.setModifyDate(PubFun.getCurrentDate());
                    errorLogSchema.setModifyTime(PubFun.getCurrentTime());

                    String errorState = AuditCancelBL.ErrorState;
                    System.out.println("error: " +
                                       auditCancelBL.mErrors.getFirstError() +
                                       " errorState: " + errorState);
                    mMap=new MMap();
                    mMap.put(errorLogSchema, "INSERT");

                    this.tVData.clear();
                    this.tVData.add(mMap);
                    tPubSubmit=new PubSubmit();
                    tPubSubmit.submitData(this.tVData, "");
                    //mMap=null;
                    continue;
                }

            }
       }


System.out.println("开始 第 5 种核销..................................................");
      strSQL = "select prtno, managecom, makedate, maketime from lcgrpcont where cardflag='0'"
       +"and prtno not in (select StartNo from LZCard where certifycode='HT05/05B' and (stateflag='5' or stateflag='6'))"
       //+"and prtno='12000000348'"
       ;//团体旅行保险投保书  5
      SSRS audit_HT05 = exesql.execSQL(strSQL);
      tempdata=null;
              tempdata = audit_HT05.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组

               if (tempdata.length>0) {

                   for (int i = 0; i < audit_HT05.getMaxRow(); i++) {
                       certifyNo = tempdata[i][0];
                       manageCom = tempdata[i][1];
                       makeDate = tempdata[i][2];
                       makeTime = tempdata[i][3];

                       VData aVData = new VData();
                       aVData.clear();
                       AuditCancelBL auditCancelBL = new AuditCancelBL();
                       aVData.addElement("HT05/05B");
                       aVData.addElement(certifyNo);

                       GlobalInput gInput = new GlobalInput();
                       gInput.Operator = "AUTO";
                       gInput.ComCode = manageCom;
                       aVData.addElement(gInput);

                       if (!auditCancelBL.autoSubmitData(aVData, "AUTOAUDIT")) { //如果没有通过自动核销则向日志中记录
                           LZCertifyErrorLogSchema errorLogSchema = new
                                   LZCertifyErrorLogSchema();

                           errorLogSchema.setErrLogNo(PubFun1.CreateMaxNo("SERIALNO",
                                   10));

                           errorLogSchema.setCertifyCode("HT05/05B");
                           errorLogSchema.setCertifyCom(manageCom); //业务处理机构
                           errorLogSchema.setCertifyMakeData(makeDate); //单证进入业务处理日期
                           errorLogSchema.setCertifyMakeTime(makeTime); //单证进入业务处理时间

                           errorLogSchema.setStartNo(certifyNo);
                           errorLogSchema.setEndNo(certifyNo);

                           errorLogSchema.setHandleFlag("0");
                           errorLogSchema.setOperator("AUTO");

                           strSQL = "select Receivecom, havenumber,stateFlag from LZCard a,LMCertifyDes b where a.CertifyCode = b.CertifyCode and "
                                    + " StartNo <= '" + certifyNo + "' AND EndNo >= '" +
                                    certifyNo
                                    + "' AND StartNo <= '" + certifyNo +
                                    "' AND EndNo >= '" + certifyNo + "' "
                                    + " AND a.CertifyCode = 'HT05/05B' "
                                    ;
                           SSRS pSSRS = new SSRS();
                           pSSRS = exesql.execSQL(strSQL);
                           String[][] pCom = pSSRS.getAllData();

                           if (pCom.length <= 0) {
                               errorLogSchema.setErrorInfo("1");
                               errorLogSchema.setPossessCom("无");
                           } else {
                               errorLogSchema.setPossessCom(pCom[0][0]);
                               errorLogSchema.setState(pCom[0][2]);
                               errorLogSchema.setErrorInfo(AuditCancelBL.ErrorState);
                           }

                           errorLogSchema.setMakedate(PubFun.getCurrentDate());
                           errorLogSchema.setMakeTime(PubFun.getCurrentTime());
                           errorLogSchema.setModifyDate(PubFun.getCurrentDate());
                           errorLogSchema.setModifyTime(PubFun.getCurrentTime());

                           String errorState = AuditCancelBL.ErrorState;
                           System.out.println("error: " +
                                              auditCancelBL.mErrors.getFirstError() +
                                              " errorState: " + errorState);
                           mMap=new MMap();
                           mMap.put(errorLogSchema, "INSERT");

                           this.tVData.clear();
                           this.tVData.add(mMap);
                           tPubSubmit=new PubSubmit();
                           tPubSubmit.submitData(this.tVData, "");
                           //mMap=null;

                           continue;
                       }

                   }
        }

      System.out.println("开始 第 6 种核销..................................................");
       strSQL = "select prtno, managecom, makedate, maketime from lccont where cardflag='2' "
       +" and prtno not in (select StartNo from LZCard where certifycode='HG04/06B' and (stateflag='5' or stateflag='6'))" //自动核销stateflag为5强制核销stateflag为6
       //+" and prtno='19000000007'"
       ;//旅行保险投保书 4
       SSRS audit_HG04 = exesql.execSQL(strSQL);
       tempdata=null;
       tempdata = audit_HG04.getAllData(); //SSRS的getAllData()方法得到的是一个二维的字符串数组

       if (tempdata.length>0) {
           for (int i = 0; i < audit_HG04.getMaxRow(); i++) {
               certifyNo = tempdata[i][0];
               manageCom = tempdata[i][1];
               makeDate = tempdata[i][2];
               makeTime = tempdata[i][3];

               VData aVData = new VData();
               aVData.clear();
               AuditCancelBL auditCancelBL = new AuditCancelBL();
               aVData.addElement("HG04/06B"); //
               aVData.addElement(certifyNo);

               GlobalInput gInput = new GlobalInput();
               gInput.Operator = "AUTO";
               gInput.ComCode = manageCom;
               aVData.addElement(gInput);

               if (!auditCancelBL.autoSubmitData(aVData, "AUTOAUDIT")) { //如果没有通过自动核销则向日志中记录
                   LZCertifyErrorLogSchema errorLogSchema = new
                           LZCertifyErrorLogSchema();

                   errorLogSchema.setErrLogNo(PubFun1.CreateMaxNo("SERIALNO",
                           10));

                   errorLogSchema.setCertifyCode("HG04/06B");
                   errorLogSchema.setCertifyCom(manageCom); //业务处理机构
                   errorLogSchema.setCertifyMakeData(makeDate); //单证进入业务处理日期
                   errorLogSchema.setCertifyMakeTime(makeTime); //单证进入业务处理时间

                   errorLogSchema.setStartNo(certifyNo);
                   errorLogSchema.setEndNo(certifyNo);

                   errorLogSchema.setHandleFlag("0");
                   errorLogSchema.setOperator("AUTO");

                   strSQL = "select Receivecom, havenumber,stateFlag from LZCard a,LMCertifyDes b where a.CertifyCode = b.CertifyCode and "
                            + " StartNo <= '" + certifyNo + "' AND EndNo >= '" +
                            certifyNo
                            + "' AND StartNo <= '" + certifyNo +
                            "' AND EndNo >= '" + certifyNo + "' "
                            + " AND a.CertifyCode = 'HG04/06B' "
                            ;
                   SSRS pSSRS = new SSRS();
                   pSSRS = exesql.execSQL(strSQL);
                   String[][] pCom = pSSRS.getAllData();

                   if (pCom.length <= 0) {
                       errorLogSchema.setErrorInfo("1");
                       errorLogSchema.setPossessCom("无");
                   } else {
                       errorLogSchema.setPossessCom(pCom[0][0]);
                       errorLogSchema.setState(pCom[0][2]);
                       errorLogSchema.setErrorInfo(AuditCancelBL.ErrorState);
                   }

                   errorLogSchema.setMakedate(PubFun.getCurrentDate());
                   errorLogSchema.setMakeTime(PubFun.getCurrentTime());
                   errorLogSchema.setModifyDate(PubFun.getCurrentDate());
                   errorLogSchema.setModifyTime(PubFun.getCurrentTime());

                   String errorState = AuditCancelBL.ErrorState;
                   System.out.println("error: " +
                                      auditCancelBL.mErrors.getFirstError() +
                                      " errorState: " + errorState);
                   mMap=new MMap();
                   mMap.put(errorLogSchema, "INSERT");

                   this.tVData.clear();
                   this.tVData.add(mMap);
                   tPubSubmit=new PubSubmit();
                   tPubSubmit.submitData(this.tVData, "");
                   //mMap=null;
                   continue;
               }

           }
       }

     //this.tVData.add(mMap);
     //tPubSubmit.submitData(this.tVData,"");
     return true;
    }

    private boolean getInputData(VData vData)
    {
        if (auditFlag.equals("HANDLE"))
        {
             mGlobalInput= (GlobalInput) vData.getObjectByObjectName(
                    "GlobalInput", 0);
             operator = mGlobalInput.Operator;
             manageCom = mGlobalInput.ComCode;
             return true;
        }
        else
        {
            return false;
        }
    }

    /*
     * add by Javabean, 2006-03-17
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "CertifyFunc";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;

        System.out.println("In CertifyFunc buildError() : " + szErrMsg);
        mErrors.addOneError(cError);
    }



    public static void main( String args[])
    {
        AuditCancelTask tAuditCancel = new AuditCancelTask();
        tAuditCancel.run();
    }

}
