package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 统计监控SQL结果
 * 1、执行时间长的前10条结果
 * 2、查找执行成本较高、最消耗资源的前10条结果
 * 3、查看哪个应用占用了过多的资源
 * 4、查出以上结果，发邮件
 * 
 * @author JC
 *
 */
public class SqlMonitorTask extends TaskThread {
	//声明错误处理对象
	public CErrors mErrors = new CErrors();
	
	//开始线程
	public void run () {
		getSql();
	}
	
	//提取sql
	public boolean getSql() {
		try {
			System.out.println("================开始统计SQL监控结果================");
			
			//1、执行时间大于5分钟的前10条sql语句
			String sql1 = "select substr(stmt_text,1,500) as stmt_text,average_execution_time_s,num_executions  from sysibmadm.TOP_DYNAMIC_SQL where average_execution_time_s >= 600 order by AVERAGE_EXECUTION_TIME_S desc fetch first 5 rows only ";
			//2、查找执行成本较高、最消耗资源的前10条SQL语句
//			String sql2 = "select stmt_text from TABLE(MON_GET_PKG_CACHE_STMT(NULL,NULL,NULL,-2)) as T where rows_read<>0 order by rows_returned/rows_read asc fetch first 5 rows only ";
			String sql2 = "select QUERY_COST_ESTIMATE,stmt_text from TABLE(MON_GET_PKG_CACHE_STMT(NULL,NULL,NULL,-2)) as T where 1=1 order by QUERY_COST_ESTIMATE desc fetch first 5 rows only ";
			//3、查看哪个应用占用了过多的资源
			String sql3 = "select application_handle,application_name,total_cpu_time from table(mon_get_connection(null,null)) as t where 1=1 order by total_cpu_time desc fetch first 5 rows only ";
			//创建sql执行对象
			ExeSQL tEx = new ExeSQL();
			//取查询结果集
			
			SSRS result1 = tEx.execSQL(sql1);
			SSRS result2 = tEx.execSQL(sql2);
			SSRS result3 = tEx.execSQL(sql3);
			
			String content = "";
			if(result1.getMaxRow() > 0) {
				content += "一、执行时间大于5分钟的前5条sql相关信息：<br/>字段：  stmt_text &nbsp; average_execution_time_s &nbsp; num_executions";
				for(int i = 1; i <= result1.MaxRow; i++) {
					content += "<br/>"+result1.GetText(i, 1)+" ;&nbsp; "+result1.GetText(i, 2)+" &nbsp; "+result1.GetText(i, 3);
				}
			}else {
				content += "一、未查询到执行时间大于5分钟的SQL";
			}
			if(result2.getMaxRow() > 0) {
				content += "<br/>二、查找执行成本较高、最消耗资源的前5条SQL相关信息：<br/>字段：  stmt_text";
				for(int i = 1; i <= result2.MaxRow; i++) {
					System.out.println("====="+result2.GetText(i, 1));
					content += "<br/>QUERY_COST_ESTIMATE:"+result2.GetText(i, 1)+":";
					content += "<br/>"+result2.GetText(i, 2)+" ;";
				}
			}else {
				content += "<br/>二、未查询到查找执行成本较高、最消耗资源的SQL";
			}
			if(result3.getMaxRow() > 0) {
				content += "<br/>三、查看哪个应用占用了过多的资源：<br/>字段：application_handle &nbsp; application_name &nbsp; total_cpu_time";
				for(int i = 1; i <= result3.MaxRow; i++) {
					content += "<br/>"+result3.GetText(i, 1)+" &nbsp; "+result3.GetText(i, 2)+" &nbsp; "+result3.GetText(i, 3);
				}
			}else {
				content += "<br/>三、未查询到占用过多资源的应用";
			}
			//邮件处理
			//获取发送邮箱的账号密码
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			//创建邮件发送类
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS.GetText(1, 2),"picchealth");
			tMailSender.setSendInf("监控结果", content, "");//没有附件，置空
			//codealias:收件人  codename：抄送
			String tSQLRMailSql = "select codealias,codename from ldcode where codetype = 'sqlmonitormail' ";
			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1), tSSRSR.GetText(1, 2), null);
			// 发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}
			System.out.println("================SQL监控统计结束================");
		}catch(Exception e) {
			this.mErrors.addOneError(new CError("SQL监控出错","SqlMonitorTask","run"));
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	//测试main方法
	public static void main (String args[]) {
		System.out.println("开始测试....");
		SqlMonitorTask task = new SqlMonitorTask();
		task.run();
		System.out.println("测试结束....");
	}
}
