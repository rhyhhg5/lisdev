package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.VData;

public class BJHealthTask extends TaskThread {
	
	public BJHealthTask(){
	}
	
	
    public void run()
    {
        try{
        	//契约承保
        	QyJKXServiceTask tQyJKXServiceTask = new QyJKXServiceTask();
        	tQyJKXServiceTask.run();
        }catch(Exception e)
        {
        	System.out.println("契约承保上报数据异常！");
        	e.getMessage();
        }
       
        try{
        //保全
        	BqJKXPingTaiBaoWenTask tBqJKXPingTaiBaoWenTask = new BqJKXPingTaiBaoWenTask();
        	tBqJKXPingTaiBaoWenTask.run();
        }catch(Exception e)
        {
        	System.out.println("保全上报数据异常！");
        	e.getMessage();
        }
        
        
        try{
        //理赔
       	LPJKXServiceTask tLPJKXServiceTask = new LPJKXServiceTask();
        	tLPJKXServiceTask.run();
        }catch(Exception e)
       {
        	System.out.println("理赔上报数据异常！");
        	e.getMessage();
        }
        
    }
    
    public static void main(String[] args)
    {
    	BJHealthTask tBJHealthTask = new BJHealthTask();
    	tBJHealthTask.run();
    }
}
