package com.sinosoft.lis.taskservice;

import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.jdom.Document;
import org.jdom.Element;
import org.w3c.dom.Node;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.namespace.QName;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * <p>
 * Title: 自动立案理算
 * </p>
 * 
 * <p>
 * Description: 生成案件至理算状态
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * 
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class LLCaseInforFeedback extends TaskThread {
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mFilePath = null;

	public LLCaseInforFeedback() {
	}

	/**
	 * 解析报文主要部分
	 * 
	 * @param cInXml
	 *            Document
	 * @return boolean
	 */
	public Document createDomData() {
		Document tDocument = null;
		try {
			System.out
					.println("LLCaseInforFeedback--createDomData--开始查找待反馈的给付信息");

			System.out.println("LLCaseInforFeedback--createJDomData");
			String tSQL = "select distinct(b.caseno) "
					+ " from LLHospCase a,llcase b,ljagetclaim c "
					+ " where a.caseno = b.caseno  "
					+ " and b.caseno = c.otherno   "
					+ " and a.hospitcode = '02'    "
					+ " and a.caseno like 'C12%'   "
					+ " and b.rgtstate = '12'      "
					+ " and c.confdate = current date - 1 day ";
			System.out.println("查询" + mCurrentDate + "前一天数据========" + tSQL);
			SSRS tSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			tSSRS = tExeSQL.execSQL(tSQL);

			if (tSSRS != null && tSSRS.MaxRow > 0) {
				Element tRootData = null;
				for (int index = 1; index <= tSSRS.getMaxRow(); index++) {

					// XML 信息描述部分
					tRootData = new Element("PACKET");
					tRootData.addAttribute("type", "REQUEST");
					tRootData.addAttribute("version", "1.0");

					// XML Head部分
					Element tHeadData = new Element("HEAD");

					Element tRequestType = new Element("REQUEST_TYPE");
					tRequestType.setText("TJ05"); // 这个地方仍需调整
					tHeadData.addContent(tRequestType);

					String getName = "Select AppTranNo from llhospcase where caseno ='"
							+ tSSRS.GetText(index, 1) + "' ";
					String tName = tExeSQL.getOneValue(getName);

					Element tTransactionNum = new Element("TRANSACTION_NUM");
					tTransactionNum.setText(tName);
					tHeadData.addContent(tTransactionNum);

					tRootData.addContent(tHeadData);

					// XML Body部分
					Element tBodyData = new Element("BODY");
					// XML BASE_PART部分
					String SQL = "";
					SSRS t_SSRS = new SSRS(); // 临时数据存取
					SQL = "select lhc.claimno,lc.CustomerName,cpat.grpContNo,lhc.HospitCode,'',lc.CustomerSex,lc.CustBirthday,lc.IDType,lc.IDNo,pi.InsuredStat,"
							+ "lc.AccidentDate,sr.AccidentType,acr.ReasonCode,sr.DieDate,ci.Code,lla.Code,llr.RgtantName,llr.Relation,lc.IDNo,lc.IDType,"
							+ "llr.RgtantPhone,llr.RgtantMobile,llr.RgtantAddress,llr.PostCode"
							+ " from LLHospCase lhc left join LLCase lc on lhc.caseno=lc.caseno"
							+ " left join LLCasePolicy cpat on cpat.caseno=lhc.caseno"
							+ " left join LPInsured pi on cpat.contno=pi.contno"
							+ " left join LLAppClaimReason acr on lhc.caseno=acr.caseno"
							+ " left join LLCaseRela cr on cr.caseno=lhc.caseno"
							+ " left join LLSubReport sr on sr.SubRptNo=cr.SubRptNo"
							+ " left join LLCaseInfo ci on ci.caseno=lhc.caseno"
							+ " left join LLAccident lla on lla.caseno=lhc.caseno"
							+ " left join LLRegister llr on llr.RgtNo=lc.RgtNo where lhc.CaseNo = '"
							+ tSSRS.GetText(index, 1) + "'";
					t_SSRS = tExeSQL.execSQL(SQL);
					if (t_SSRS != null && t_SSRS.MaxRow > 0) {
						Element tBasePart = new Element("BASE_PART");
						tBodyData.addContent(tBasePart); // 插入 BASE_PART 节点

						// 向BASE_PART 节点插入CLAIM_NO 节点
						Element ClaimNO = new Element("CLAIM_NO");
						ClaimNO.setText(t_SSRS.GetText(1, 1));
						tBasePart.addContent(ClaimNO);

						// 向BASE_PART 节点插入INSURED_NAME 节点
						Element InsuredName = new Element("INSURED_NAME");
						InsuredName.setText(t_SSRS.GetText(1, 2));
						tBasePart.addContent(InsuredName);

						// 向BASE_PART 节点插入CONT_NO 节点
						Element ContNO = new Element("CONT_NO");
						ContNO.setText(t_SSRS.GetText(1, 3));
						tBasePart.addContent(ContNO);

						// 向BASE_PART 节点插入CLAIM_TYPE 节点
						Element ClaimType = new Element("CLAIM_TYPE");
						ClaimType.setText(t_SSRS.GetText(1, 4));
						tBasePart.addContent(ClaimType);

						// 向BASE_PART 节点插入CLAIM_YEAR 节点
						Element ClaimYear = new Element("CLAIM_YEAR");
						ClaimYear.setText(t_SSRS.GetText(1, 5));
						tBasePart.addContent(ClaimYear);

						// 向BASE_PART 节点插入INSURED_SEX 节点
						Element InsuredSex = new Element("INSURED_SEX");
						InsuredSex.setText(t_SSRS.GetText(1, 6));
						tBasePart.addContent(InsuredSex);

						// 向BASE_PART 节点插入INSURED_BIRTHDAY 节点
						Element InsuredBirthday = new Element(
								"INSURED_BIRTHDAY");
						InsuredBirthday.setText(t_SSRS.GetText(1, 7));
						tBasePart.addContent(InsuredBirthday);

						// 向BASE_PART 节点插入ID_TYPE 节点
						Element IDType = new Element("ID_TYPE");
						IDType.setText(t_SSRS.GetText(1, 8));
						tBasePart.addContent(IDType);

						// 向BASE_PART 节点插入ID_NO 节点
						Element IDNo = new Element("ID_NO");
						IDNo.setText(t_SSRS.GetText(1, 9));
						tBasePart.addContent(IDNo);

						// 向BASE_PART 节点插入INSURED_STAT 节点
						Element InsuredStat = new Element("INSURED_STAT");
						InsuredStat.setText(t_SSRS.GetText(1, 10));
						tBasePart.addContent(InsuredStat);

						// 向BASE_PART 节点插入ACCDATE 节点
						Element Accdate = new Element("ACCDATE");
						Accdate.setText(t_SSRS.GetText(1, 11));
						tBasePart.addContent(Accdate);

						// 向BASE_PART 节点插入ACCIDENTTYPE 节点
						Element Accidenttype = new Element("ACCIDENTTYPE");
						Accidenttype.setText(t_SSRS.GetText(1, 12));
						tBasePart.addContent(Accidenttype);

						// 向BASE_PART 节点插入CLAIMREASON 节点
						Element ClaimReason = new Element("CLAIMREASON");
						ClaimReason.setText(t_SSRS.GetText(1, 13));
						tBasePart.addContent(ClaimReason);

						// 向BASE_PART 节点插入DEATHDATE 节点
						Element Deathdate = new Element("DEATHDATE");
						Deathdate.setText(t_SSRS.GetText(1, 14));
						tBasePart.addContent(Deathdate);

						// 向BASE_PART 节点插入DEFORMITYCODE 节点
						Element Deformitycode = new Element("DEFORMITYCODE");
						Deformitycode.setText(t_SSRS.GetText(1, 15));
						tBasePart.addContent(Deformitycode);

						// 向BASE_PART 节点插入ACCIDENTNO 节点
						Element AccidentNo = new Element("ACCIDENTNO");
						AccidentNo.setText(t_SSRS.GetText(1, 16));
						tBasePart.addContent(AccidentNo);

						// 向BASE_PART 节点插入RGTANTNAME 节点
						Element Rgtantname = new Element("RGTANTNAME");
						Rgtantname.setText(t_SSRS.GetText(1, 17));
						tBasePart.addContent(Rgtantname);

						// 向BASE_PART 节点插入RGTANTRELATIOIN 节点
						Element Rgtantrelatioin = new Element("RGTANTRELATIOIN");
						Rgtantrelatioin.setText(t_SSRS.GetText(1, 18));
						tBasePart.addContent(Rgtantrelatioin);

						// 向BASE_PART 节点插入RGTANTIDNO 节点
						Element Rgtantidno = new Element("RGTANTIDNO");
						Rgtantidno.setText(t_SSRS.GetText(1, 19));
						tBasePart.addContent(Rgtantidno);

						// 向BASE_PART 节点插入RGTANTIDTYPE 节点
						Element Rgtantidtype = new Element("RGTANTIDTYPE");
						Rgtantidtype.setText(t_SSRS.GetText(1, 20));
						tBasePart.addContent(Rgtantidtype);

						// 向BASE_PART 节点插入RGTANTPHONE 节点
						Element Rgtantphone = new Element("RGTANTPHONE");
						Rgtantphone.setText(t_SSRS.GetText(1, 21));
						tBasePart.addContent(Rgtantphone);

						// 向BASE_PART 节点插入RGTMOBILE 节点
						Element Rgtmobile = new Element("RGTMOBILE");
						Rgtmobile.setText(t_SSRS.GetText(1, 22));
						tBasePart.addContent(Rgtmobile);

						// 向BASE_PART 节点插入RGTANTADDRESS 节点
						Element Rgtantaddress = new Element("RGTANTADDRESS");
						Rgtantaddress.setText(t_SSRS.GetText(1, 23));
						tBasePart.addContent(Rgtantaddress);

						// 向BASE_PART 节点插入RGTANTPOSTCODE 节点
						Element Rgtantpostcode = new Element("RGTANTPOSTCODE");
						Rgtantpostcode.setText(t_SSRS.GetText(1, 24));
						tBasePart.addContent(Rgtantpostcode);
					}

					// XML RECEIPTLIST部分
					SQL = "";
					t_SSRS = new SSRS(); // 临时数据存取
					SQL = "select fm.FeeType,fm.FeeAtti,fm.ReceiptNo,fm.HospitalName,fm.FeeDate,fm.HospStartDate,fm.HospEndDate,fm.SumFee,sr.GetLimit,"
							+ "sr.PlanFee,sr.SupInHosFee,sr.SelfAmnt,sr.SelfPay2"
							+ " from LLFeeMain fm left join LLSecurityReceipt sr on fm.caseno=sr.caseno where fm.CaseNo = '"
							+ tSSRS.GetText(index, 1) + "'";
					t_SSRS = tExeSQL.execSQL(SQL);
					if (t_SSRS != null && t_SSRS.MaxRow > 0) {
						Element tReceiptlist = new Element("RECEIPTLIST");
						tBodyData.addContent(tReceiptlist); // 插入 RECEIPTLIST 节点
						Element tReceiptData = new Element("RECEIPTDATA");
						tReceiptlist.addContent(tReceiptData); // 向 RECEIPTLIST
																// 节点插入
																// RECEIPTDATA
																// 节点

						for (int i = 1; i <= t_SSRS.getMaxRow(); i++) {

							// 向RECEIPTDATA节点插入FEETYPE 节点
							Element tFeeType = new Element("FEETYPE");
							tFeeType.setText(t_SSRS.GetText(i, 1));
							tReceiptData.addContent(tFeeType);

							// 向RECEIPTDATA节点插入FEETYPE 节点
							Element tFeeAtti = new Element("FEEATTI");
							tFeeAtti.setText(t_SSRS.GetText(i, 2));
							tReceiptData.addContent(tFeeAtti);

							// 向RECEIPTDATA节点插入MAINFEENO 节点
							Element tMainFeeNo = new Element("MAINFEENO");
							tMainFeeNo.setText(t_SSRS.GetText(i, 3));
							tReceiptData.addContent(tMainFeeNo);

							// 向RECEIPTDATA节点插入HOSIPITALNAME 节点
							Element tHospitalName = new Element("HOSIPITALNAME");
							tHospitalName.setText(t_SSRS.GetText(i, 4));
							tReceiptData.addContent(tHospitalName);

							// 向RECEIPTDATA节点插入FEEDATE 节点
							Element tFeeDate = new Element("FEEDATE");
							tFeeDate.setText(t_SSRS.GetText(i, 5));
							tReceiptData.addContent(tFeeDate);

							// 向RECEIPTDATA节点插入HOSPSTARTDATE 节点
							Element tHospstartDate = new Element(
									"HOSPSTARTDATE");
							tHospstartDate.setText(t_SSRS.GetText(i, 6));
							tReceiptData.addContent(tHospstartDate);

							// 向RECEIPTDATA节点插入HOSPENDDATE 节点
							Element tHospendDate = new Element("HOSPENDDATE");
							tHospendDate.setText(t_SSRS.GetText(i, 7));
							tReceiptData.addContent(tHospendDate);

							// 向RECEIPTDATA节点插入SUMFEE 节点
							Element tSumFee = new Element("SUMFEE");
							tSumFee.setText(t_SSRS.GetText(i, 8));
							tReceiptData.addContent(tSumFee);

							// 向RECEIPTDATA节点插入GETLIMIT 节点
							Element tGetLimit = new Element("GETLIMIT");
							tGetLimit.setText(t_SSRS.GetText(i, 9));
							tReceiptData.addContent(tGetLimit);

							// 向RECEIPTDATA节点插入PLANFEE 节点
							Element tPlanFee = new Element("PLANFEE");
							tPlanFee.setText(t_SSRS.GetText(i, 10));
							tReceiptData.addContent(tPlanFee);

							// 向RECEIPTDATA节点插入SUPINHOSFEE 节点
							Element tSupinhosFee = new Element("SUPINHOSFEE");
							tSupinhosFee.setText(t_SSRS.GetText(i, 11));
							tReceiptData.addContent(tSupinhosFee);

							// 向RECEIPTDATA节点插入SELFAMNT 节点
							Element tSelfAmnt = new Element("SELFAMNT");
							tSelfAmnt.setText(t_SSRS.GetText(i, 12));
							tReceiptData.addContent(tSelfAmnt);

							// 向RECEIPTDATA节点插入SELFPAY2 节点
							Element tSelfPay2 = new Element("SELFPAY2");
							tSelfPay2.setText(t_SSRS.GetText(i, 13));
							tReceiptData.addContent(tSelfPay2);

							// 向RECEIPTDATA节点插入账单分类信息FEEITEMLIST部分
							SQL = "";
							t_SSRS = new SSRS(); // 临时数据存取
							SQL = "Select (select code1 from ldcode1 where codetype = 'llfeeitemtype' and codealias = LLCaseReceipt.FeeItemCode fetch first 1 rows only),Fee,SelfAmnt,PreAmnt,RefuseAmnt From LLCaseReceipt where CaseNo = '"
									+ tSSRS.GetText(index, 1) + "'";
							t_SSRS = tExeSQL.execSQL(SQL);
							if (t_SSRS != null && t_SSRS.MaxRow > 0) {
								Element tFeeitemList = new Element(
										"FEEITEMLIST");
								tReceiptData.addContent(tFeeitemList); // 向RECEIPTDATA节点插入
																		// FEEITEMLIST
																		// 节点

								for (int j = 1; j <= t_SSRS.getMaxRow(); j++) {
									Element tFeeitemData = new Element(
											"FEEITEMDATA");
									tFeeitemList.addContent(tFeeitemData); // 插入
									// FEEITEMDATA
									// 节点

									// 插入FEEITEM_CODE 节点
									Element tFeeitemCode = new Element(
											"FEEITEM_CODE");
									tFeeitemCode.setText(t_SSRS.GetText(j, 1));
									tFeeitemData.addContent(tFeeitemCode);

									// 插入SUMFEE 节点
									Element tSumFee2 = new Element("SUMFEE");
									tSumFee2.setText(t_SSRS.GetText(j, 2));
									tFeeitemData.addContent(tSumFee2);

									// 插入SELFAMNT 节点
									Element tSelfAmnt2 = new Element("SELFAMNT");
									tSelfAmnt2.setText(t_SSRS.GetText(j, 3));
									tFeeitemData.addContent(tSelfAmnt2);

									// 插入PREAMNT 节点
									Element tPreAmnt = new Element("PREAMNT");
									tPreAmnt.setText(t_SSRS.GetText(j, 4));
									tFeeitemData.addContent(tPreAmnt);

									// 插入REFUSEAMNT 节点
									Element tRefuseAmnt = new Element(
											"REFUSEAMNT");
									tRefuseAmnt.setText(t_SSRS.GetText(j, 5));
									tFeeitemData.addContent(tRefuseAmnt);

								}

							}

							// 单据明细信息 DOCUMNENTATIONLIST 该部分先不处理
							SQL = "select ReceiptNo,ReceiptNum,ServiceDate,ProductCode,ProductName,ProductType,DrugCode,PayService,DrugName,UnitPrice,Number"
									+ ",Specifications,Dosage,Sumprice,ProfessionalCode,DoctorName,DepartmentCode,DepartmentName,Usage,DrugType,Consumption "
									+ ",Frequency,DrugDay,Receiptfeeinsecu  from LLReceipt where CaseNo = '"
									+ tSSRS.GetText(index, 1) + "'";
							t_SSRS = tExeSQL.execSQL(SQL);
							if (t_SSRS != null && t_SSRS.MaxRow > 0) {
								Element tDocumnentationList = new Element(
										"DOCUMNENTATIONLIST");
								tReceiptData.addContent(tDocumnentationList); // 插入
								// DOCUMNENTATIONLIST
								// 节点
								for (int k = 1; k <= t_SSRS.getMaxRow(); k++) {
									Element tDocumnentationData = new Element(
											"DOCUMNENTATIONDATA");
									tDocumnentationList
											.addContent(tDocumnentationData); // 插入
									// DOCUMNENTATIONDATA
									// 节点

									// 插入RECEIPT_NO 节点
									Element tReceiptNo = new Element(
											"RECEIPT_NO");
									tReceiptNo.setText(t_SSRS.GetText(k, 1));
									tDocumnentationData.addContent(tReceiptNo);

									// 插入RECEIPT_NUM 节点
									Element tReceiptNum = new Element(
											"RECEIPT_NUM");
									tReceiptNum.setText(t_SSRS.GetText(k, 2));
									tDocumnentationData.addContent(tReceiptNum);

									// 插入SERVICEDATE 节点
									Element tServiceDate = new Element(
											"SERVICEDATE");
									tServiceDate.setText(t_SSRS.GetText(k, 3));
									tDocumnentationData
											.addContent(tServiceDate);

									// 插入PRODUCTCODE 节点
									Element tProductCode = new Element(
											"PRODUCTCODE");
									tProductCode.setText(t_SSRS.GetText(k, 4));
									tDocumnentationData
											.addContent(tProductCode);

									// 插入PRODUCTNAME 节点
									Element tProductName = new Element(
											"PRODUCTNAME");
									tProductName.setText(t_SSRS.GetText(k, 5));
									tDocumnentationData
											.addContent(tProductName);

									// 插入PRODUCTTYPE 节点
									Element tProductType = new Element(
											"PRODUCTTYPE");
									tProductType.setText(t_SSRS.GetText(k, 6));
									tDocumnentationData
											.addContent(tProductType);

									// 插入PRODUCTTYPE 节点
									Element tDrugCode = new Element("DRUGCODE");
									tDrugCode.setText(t_SSRS.GetText(k, 7));
									tDocumnentationData.addContent(tDrugCode);

									// 插入PAYSERVICE 节点
									Element tPayService = new Element(
											"PAYSERVICE");
									tPayService.setText(t_SSRS.GetText(k, 8));
									tDocumnentationData.addContent(tPayService);

									// 插入DRUGNAME 节点
									Element tDrugName = new Element("DRUGNAME");
									tDrugName.setText(t_SSRS.GetText(k, 9));
									tDocumnentationData.addContent(tDrugName);

									// 插入UNITPRICE 节点
									Element tUnitPrice = new Element(
											"UNITPRICE");
									tUnitPrice.setText(t_SSRS.GetText(k, 10));
									tDocumnentationData.addContent(tUnitPrice);

									// 插入NUMBER 节点
									Element tNumber = new Element("NUMBER");
									tNumber.setText(t_SSRS.GetText(k, 11));
									tDocumnentationData.addContent(tNumber);

									// 插入SPECIFICATIONS 节点
									Element tSpecifications = new Element(
											"SPECIFICATIONS");
									tSpecifications.setText(t_SSRS.GetText(k,
											12));
									tDocumnentationData
											.addContent(tSpecifications);

									// 插入DOSAGE 节点
									Element tDosage = new Element("DOSAGE");
									tDosage.setText(t_SSRS.GetText(k, 13));
									tDocumnentationData.addContent(tDosage);

									// 插入SUMPRICE 节点
									Element tSumprice = new Element("SUMPRICE");
									tSumprice.setText(t_SSRS.GetText(k, 14));
									tDocumnentationData.addContent(tSumprice);

									// 插入PROFESSIONALCODE 节点
									Element tProfessionalcode = new Element(
											"PROFESSIONALCODE");
									tProfessionalcode.setText(t_SSRS.GetText(k,
											15));
									tDocumnentationData
											.addContent(tProfessionalcode);

									// 插入DOCTORNAME 节点
									Element tDoctorName = new Element(
											"DOCTORNAME");
									tDoctorName.setText(t_SSRS.GetText(k, 16));
									tDocumnentationData.addContent(tDoctorName);

									// 插入DEPARTMENTCODE 节点
									Element tDepartmentCode = new Element(
											"DEPARTMENTCODE");
									tDepartmentCode.setText(t_SSRS.GetText(k,
											17));
									tDocumnentationData
											.addContent(tDepartmentCode);

									// 插入DEPARTMENTNAME 节点
									Element tDepartmentName = new Element(
											"DEPARTMENTNAME");
									tDepartmentName.setText(t_SSRS.GetText(k,
											18));
									tDocumnentationData
											.addContent(tDepartmentName);

									// 插入USAGE 节点
									Element tUsage = new Element("USAGE");
									tUsage.setText(t_SSRS.GetText(k, 19));
									tDocumnentationData.addContent(tUsage);

									// 插入DRUGTYPE 节点
									Element tDrugType = new Element("DRUGTYPE");
									tDrugType.setText(t_SSRS.GetText(k, 20));
									tDocumnentationData.addContent(tDrugType);

									// 插入CONSUMPTION 节点
									Element tConsumption = new Element(
											"CONSUMPTION");
									tConsumption.setText(t_SSRS.GetText(k, 21));
									tDocumnentationData
											.addContent(tConsumption);

									// 插入FREQUENCY 节点
									Element tFrequency = new Element(
											"FREQUENCY");
									tFrequency.setText(t_SSRS.GetText(k, 22));
									tDocumnentationData.addContent(tFrequency);

									// 插入DRUGDAY 节点
									Element tDrugDay = new Element("DRUGDAY");
									tDrugDay.setText(t_SSRS.GetText(k, 23));
									tDocumnentationData.addContent(tDrugDay);

									// 插入RECEIPTFEEINSECU 节点
									Element tReceiptfeeinsecu = new Element(
											"RECEIPTFEEINSECU");
									tReceiptfeeinsecu.setText(t_SSRS.GetText(k,
											24));
									tDocumnentationData
											.addContent(tReceiptfeeinsecu);

								}

							}
						}
					}

					// 社保补充账单分类信息 该部分先不处理
					SQL = "Select (Select FeeType from LLFeeMain where caseno =LLSecurityReceipt.caseno ),(Select FeeAtti from LLFeeMain where caseno =LLSecurityReceipt.caseno ),(Select ReceiptNo from LLFeeMain where caseno =LLSecurityReceipt.caseno ),ApplyAmnt,PlanFee,SupInHosFee,OfficialSubsidy,SelfAmnt "
							+ ",SelfPay1,SelfPay2,GetLimit,MidAmnt,HighAmnt1,HighAmnt2,SuperAmnt "
							+ ",RetireAddFee,SmallDoorPay,HighDoorAmnt,FeeInSecu,YearSupInHosFee From LLSecurityReceipt where CaseNo = '"
							+ tSSRS.GetText(index, 1) + "' ";
					t_SSRS = tExeSQL.execSQL(SQL);
					if (t_SSRS != null && t_SSRS.MaxRow > 0) {
						Element tSecurityList = new Element("SECURITYLIST");
						tBodyData.addContent(tSecurityList); // 插入 SECURITYLIST
																// 节点
						for (int i = 1; i <= t_SSRS.getMaxRow(); i++) {
							Element tSecurityData = new Element("SECURITYDATA");
							tSecurityList.addContent(tSecurityData); // 插入
																		// SECURITYDATA
																		// 节点

							// 插入SBBC_FEETYPE 节点
							Element tSbbcfeetype = new Element("SBBC_FEETYPE");
							tSbbcfeetype.setText(t_SSRS.GetText(i, 1));
							tSecurityData.addContent(tSbbcfeetype);

							// 插入FEEATTI 节点
							Element tFeeatti = new Element("FEEATTI");
							tFeeatti.setText(t_SSRS.GetText(i, 2));
							tSecurityData.addContent(tFeeatti);

							// 插入SBBC_MAINFEENO 节点
							Element tSbbcMainfeeno = new Element(
									"SBBC_MAINFEENO");
							tSbbcMainfeeno.setText(t_SSRS.GetText(i, 3));
							tSecurityData.addContent(tSbbcMainfeeno);

							// 插入APPLYAMNT 节点
							Element tApplyAmnt = new Element("APPLYAMNT");
							tApplyAmnt.setText(t_SSRS.GetText(i, 4));
							tSecurityData.addContent(tApplyAmnt);

							// 插入PLANFEE 节点
							Element tPlanfee = new Element("PLANFEE");
							tPlanfee.setText(t_SSRS.GetText(i, 5));
							tSecurityData.addContent(tPlanfee);

							// 插入SUPINHOSFEE 节点
							Element tSupinhosFee = new Element("SUPINHOSFEE");
							tSupinhosFee.setText(t_SSRS.GetText(i, 6));
							tSecurityData.addContent(tSupinhosFee);

							// 插入OFFICIALSUBSIDY 节点
							Element tOfficialsubsidy = new Element(
									"OFFICIALSUBSIDY");
							tOfficialsubsidy.setText(t_SSRS.GetText(i, 7));
							tSecurityData.addContent(tOfficialsubsidy);

							// 插入SELFAMNT 节点
							Element tSelfamnt = new Element("SELFAMNT");
							tSelfamnt.setText(t_SSRS.GetText(i, 8));
							tSecurityData.addContent(tSelfamnt);

							// 插入SELFPAY1 节点
							Element tSelfpay1 = new Element("SELFPAY1");
							tSelfpay1.setText(t_SSRS.GetText(i, 9));
							tSecurityData.addContent(tSelfpay1);

							// 插入SELFPAY2 节点
							Element tSelfpay2 = new Element("SELFPAY2");
							tSelfpay2.setText(t_SSRS.GetText(i, 10));
							tSecurityData.addContent(tSelfpay2);

							// 插入GETLIMIT 节点
							Element tGetlimit = new Element("GETLIMIT");
							tGetlimit.setText(t_SSRS.GetText(i, 11));
							tSecurityData.addContent(tGetlimit);

							// 插入MIDAMNT 节点
							Element tMidamnt = new Element("MIDAMNT");
							tMidamnt.setText(t_SSRS.GetText(i, 12));
							tSecurityData.addContent(tMidamnt);

							// 插入HIGHAMNT1 节点
							Element tHighamnt1 = new Element("HIGHAMNT1");
							tHighamnt1.setText(t_SSRS.GetText(i, 13));
							tSecurityData.addContent(tHighamnt1);

							// 插入HIGHAMNT2 节点
							Element tHighamnt2 = new Element("HIGHAMNT2");
							tHighamnt2.setText(t_SSRS.GetText(i, 14));
							tSecurityData.addContent(tHighamnt2);

							// 插入SUPERAMNT 节点
							Element tSuperamnt = new Element("SUPERAMNT");
							tSuperamnt.setText(t_SSRS.GetText(i, 15));
							tSecurityData.addContent(tSuperamnt);

							// 插入RETIREADDFEE 节点
							Element tRetireaddfee = new Element("RETIREADDFEE");
							tRetireaddfee.setText(t_SSRS.GetText(i, 16));
							tSecurityData.addContent(tRetireaddfee);

							// 插入SMALLDOORPAY 节点
							Element tSmalldoorpay = new Element("SMALLDOORPAY");
							tSmalldoorpay.setText(t_SSRS.GetText(i, 17));
							tSecurityData.addContent(tSmalldoorpay);

							// 插入HIGHDOORAMNT 节点
							Element tHighdooramnt = new Element("HIGHDOORAMNT");
							tHighdooramnt.setText(t_SSRS.GetText(i, 18));
							tSecurityData.addContent(tHighdooramnt);

							// 插入FEEINSECU 节点
							Element tFeeinsecu = new Element("FEEINSECU");
							tFeeinsecu.setText(t_SSRS.GetText(i, 19));
							tSecurityData.addContent(tFeeinsecu);

							// 插入YEARSUPINHOSFEE 节点
							Element tYearsupinhosfee = new Element(
									"YEARSUPINHOSFEE");
							tYearsupinhosfee.setText(t_SSRS.GetText(i, 20));
							tSecurityData.addContent(tYearsupinhosfee);

						}
					}

					// 受益人信息
					SQL = "select CustomerNo,Name,Sex,Birthday,IDType,IDNo,RelationToInsured,BnfNo,BnfLot,getmoney,BankCode,BankAccNo ,AccName from LLBnf where CaseNo = '"
							+ tSSRS.GetText(index, 1) + "'";
					t_SSRS = tExeSQL.execSQL(SQL);
					if (t_SSRS != null && t_SSRS.MaxRow > 0) {
						Element tBnfList = new Element("BNFLIST");
						tBodyData.addContent(tBnfList); // 插入 BNFLIST 节点

						for (int i = 1; i <= t_SSRS.getMaxRow(); i++) {
							Element tBnfData = new Element("BNFDATA");
							tBnfList.addContent(tBnfData); // 插入 BNFDATA 节点

							// 插入BNFNO 节点
							Element tBnfNo = new Element("BNFNO");
							tBnfNo.setText(t_SSRS.GetText(i, 1));
							tBnfData.addContent(tBnfNo);

							// 插入BNFNAME 节点
							Element tBnfName = new Element("BNFNAME");
							tBnfName.setText(t_SSRS.GetText(i, 2));
							tBnfData.addContent(tBnfName);

							// 插入BNFSEX 节点
							Element tBnfSex = new Element("BNFSEX");
							tBnfSex.setText(t_SSRS.GetText(i, 3));
							tBnfData.addContent(tBnfSex);

							// 插入BNFBIRTHDAY 节点
							Element tBnfBirthday = new Element("BNFBIRTHDAY");
							tBnfBirthday.setText(t_SSRS.GetText(i, 4));
							tBnfData.addContent(tBnfBirthday);

							// 插入BNFIDTYPE 节点
							Element tBnfidType = new Element("BNFIDTYPE");
							tBnfidType.setText(t_SSRS.GetText(i, 5));
							tBnfData.addContent(tBnfidType);

							// 插入BNFIDNO 节点
							Element tBnfidNo = new Element("BNFIDNO");
							tBnfidNo.setText(t_SSRS.GetText(i, 6));
							tBnfData.addContent(tBnfidNo);

							// 插入BNFRELATION 节点
							Element tBnfrelation = new Element("BNFRELATION");
							tBnfrelation.setText(t_SSRS.GetText(i, 7));
							tBnfData.addContent(tBnfrelation);

							// 插入BNFGRADE 节点
							Element tBnfGrade = new Element("BNFGRADE");
							tBnfGrade.setText(t_SSRS.GetText(i, 8));
							tBnfData.addContent(tBnfGrade);

							// 插入BNFRATE 节点
							Element tBnfRate = new Element("BNFRATE");
							tBnfRate.setText(t_SSRS.GetText(i, 9));
							tBnfData.addContent(tBnfRate);

							// 插入SUMPAY 节点
							Element tSumPay = new Element("SUMPAY");
							tSumPay.setText(t_SSRS.GetText(i, 10));
							tBnfData.addContent(tSumPay);

							// 插入BANKCODE 节点
							Element tBankCode = new Element("BANKCODE");
							tBankCode.setText(t_SSRS.GetText(i, 11));
							tBnfData.addContent(tBankCode);

							// 插入ACCNO 节点
							Element tAccNo = new Element("ACCNO");
							tAccNo.setText(t_SSRS.GetText(i, 12));
							tBnfData.addContent(tAccNo);

							// 插入ACCNAME 节点
							Element tAccName = new Element("ACCNAME");
							tAccName.setText(t_SSRS.GetText(i, 13));
							tBnfData.addContent(tAccName);

							SQL = "select 'SBBC',REALPAY,GIVETYPE,GIVEREASON from LLClaimDetail  where CaseNo = '"
									+ tSSRS.GetText(index, 1) + "'";
							SSRS t_SSRS1 = new SSRS(); // 临时数据存取
							t_SSRS1 = tExeSQL.execSQL(SQL);
							if (t_SSRS1 != null && t_SSRS1.MaxRow > 0) {
								Element tPayList = new Element("PAYLIST");
								tBnfData.addContent(tPayList); // 插入 PAYLIST 节点

								for (int j = 1; j <= t_SSRS1.getMaxRow(); j++) {
									Element tPayData = new Element("PAYDATA");
									tPayList.addContent(tPayData); // 插入 PAYDATA
																	// 节点

									// 插入GETDUTYCODE 节点
									Element tGetdutyCode = new Element(
											"GETDUTYCODE");
									tGetdutyCode.setText(t_SSRS1.GetText(j, 1));
									tPayData.addContent(tGetdutyCode);

									// 插入REALPAY 节点
									Element tRealPay = new Element("REALPAY");
									tRealPay.setText(t_SSRS1.GetText(j, 2));
									tPayData.addContent(tRealPay);

									// 插入GIVETYPE 节点
									Element tGiveType = new Element("GIVETYPE");
									tGiveType.setText(t_SSRS1.GetText(j, 3));
									tPayData.addContent(tGiveType);

									// 插入GIVEREASON 节点
									Element tGiveReason = new Element(
											"GIVEREASON");
									tGiveReason.setText(t_SSRS1.GetText(j, 4));
									tPayData.addContent(tGiveReason);

								}
							}
						}
					}
					// 疾病信息
					SQL = "select DiseaseCode,DiseaseName from LLCaseCure  where CaseNo = '"
							+ tSSRS.GetText(index, 1) + "'";
					t_SSRS = tExeSQL.execSQL(SQL);
					if (t_SSRS != null && t_SSRS.MaxRow > 0) {
						Element tDiseaseList = new Element("DISEASELIST");
						tBodyData.addContent(tDiseaseList); // 插入 DISEASELIST 节点
						for (int i = 1; i <= t_SSRS.getMaxRow(); i++) {
							Element tDiseaseData = new Element("DISEASEDATA");
							tDiseaseList.addContent(tDiseaseData); // 插入
																	// DISEASEDATA
																	// 节点

							// 插入DISEASECODE 节点
							Element tDiseaseCode = new Element("DISEASECODE");
							tDiseaseCode.setText(t_SSRS.GetText(i, 1));
							tDiseaseData.addContent(tDiseaseCode);

							// 插入DISEASENAME 节点
							Element tDiseaseName = new Element("DISEASENAME");
							tDiseaseName.setText(t_SSRS.GetText(i, 2));
							tDiseaseData.addContent(tDiseaseName);
						}
					}

					tRootData.addContent(tBodyData);
					tDocument = new Document(tRootData);

					if (tDocument == null) {
						System.err.println("天津社保补充XML报文信息生成有误!");
						return null;
					}

					 LDSysVarDB tLDSysVarDB = new LDSysVarDB();
					 tLDSysVarDB.setSysVar("LLTJSBUploadXml");
					 LDSysVarSet tLDSysVarSet = tLDSysVarDB.query();
					 if(tLDSysVarSet.size()< 1)
					 {
					 System.err.println("文件保存路径查询失败！");
					 return null;
					 }
					 this.mFilePath = tLDSysVarSet.get(1).getSysVarValue();

					if (!saveInput(tDocument, tName, index)) {
						System.out.println("保存传入xml时失败");
						return null;
					}

					// String tDoc = JdomUtil.outputToString(tDocument);
					// System.out.println(tDoc);
					//
					// String tOutStr = callService(tDoc);
					// //
					// Document tOutXmlDoc =
					// JdomUtil.buildFromXMLString(tOutStr);
					// if (tOutXmlDoc == null) {
					// System.err.println("天津城乡居民意外返回报文为空!");
					// return null;
					// }else{
					// if(!saveOutput(tOutXmlDoc,tSSRS.GetText(index,
					// 10),index)){
					// System.out.println("保存传出xml时失败");
					// return null;
					// }
					// }
				}
			} else {
				System.out.println("天津社保补充没有信息反馈");
				return null;
			}

		} catch (Exception ex) {
			System.err.println("天津社保补充没有信息反馈获取失败！");
			ex.printStackTrace();
			return null;
		}
		return tDocument;
	}

	public boolean saveInput(Document pXmlDoc, String mTranscationNum, int index) {
		String strLimit = PubFun.getNoLimit("86120000");
		String pName = mTranscationNum + "_IN"
				+ PubFun1.CreateMaxNo("LLTJ", strLimit) + ".xml";
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(this.mFilePath);
		System.out.println(mFilePath);

		File mFileDir = new File(mFilePath.toString());

		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
			}
		}
		File tempFile=new File(mFilePath.toString() + pName);
		System.out.println("文件地址:" + tempFile);
		if (!tempFile.exists()) {
			try {
				if (!tempFile.createNewFile()) {
					System.err.println("创建文件失败！" + tempFile.getPath());
				}
			} catch (Exception ex) {
				System.err.println("创建文件时出现异常！" + tempFile.getPath());
				ex.printStackTrace();
			}

		}
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString()+ pName);
			JdomUtil.output(pXmlDoc, tFos);
			tFos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;

		}
		return true;
	}

	public boolean saveOutput(Document pXmlDoc, String mTranscationNum,
			int index) {
		String strLimit = PubFun.getNoLimit("86120000");
		String pName = mTranscationNum + "_OUT"
				+ PubFun1.CreateMaxNo("LLTJ", strLimit) + ".xml";
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(this.mFilePath);
		File mFileDir = new File(mFilePath.toString());
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
			}
		}
		File tempFile = new File(mFilePath.toString() + pName);
		if (!tempFile.exists()) {
			try {
				if (!tempFile.createNewFile()) {
					System.err.println("创建文件失败！" + tempFile.getPath());
				}
			} catch (Exception ex) {
				System.err.println("创建文件时出现异常！" + tempFile.getPath());
				ex.printStackTrace();
			}

		}
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString()
					+ pName);
			JdomUtil.output(pXmlDoc, tFos);
			tFos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;

		}
		return true;
	}

	/**
	 * 1、客户端准备数据;2、调用WebService服务;3、获取服务端处理结果数据
	 * 
	 * @param pReader
	 * @param pServiceURL
	 * @return org.w3c.dom.Document
	 * @throws AxisFault
	 * @throws Exception
	 */
	public String callService(String tDoc) throws AxisFault {
		RPCServiceClient serviceClient = new RPCServiceClient();

		Options options = serviceClient.getOptions();
		LDSysVarDB tLDSysVarDB = new LDSysVarDB();
		tLDSysVarDB.setSysVar("lLLTJPath");
		if (!tLDSysVarDB.getInfo()) {
		}
		EndpointReference targetEPR = new EndpointReference(tLDSysVarDB
				.getSchema().getSysVarValue());
		options.setTo(targetEPR);
		options.setProperty(HTTPConstants.CHUNKED, Boolean.FALSE);
		QName opAddEntry = new QName("http://webservice.yinhai.com/",
				"call_YH_Hp");

		Object[] opAddEntryArgs = new Object[] { tDoc };

		Class[] returnTypes = new Class[] { String.class };
		Object[] objects = serviceClient.invokeBlocking(opAddEntry,
				opAddEntryArgs, returnTypes);
		for (int i = 0; i < objects.length; i++) {
			System.out.println(objects[i]);
		}
		String responseXML = (String) objects[0];
		return responseXML;
	}

	public void run() {
		if (createDomData() == null) {
			System.out.println("天津意外平台给付信息反馈失败");
		}
	}

	/**
	 * 将org.w3c.dom.Document转化为文件输出流
	 * 
	 * @param pNode
	 * @param pOutputStream
	 * @throws Exception
	 */
	public void outputDOM(Node pNode, OutputStream pOutputStream)
			throws Exception {
		DOMSource mDOMSource = new DOMSource(pNode);

		StreamResult mStreamResult = new StreamResult(pOutputStream);

		Transformer mTransformer = cTransformerFactory.newTransformer();
		mTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
		mTransformer.setOutputProperty(OutputKeys.ENCODING, "GB2312");

		mTransformer.transform(mDOMSource, mStreamResult);
	}

	private TransformerFactory cTransformerFactory = TransformerFactory
			.newInstance();

	public static void main(String[] args) {
		try {
			// LLTJUploadXml tBusinessDeal = new LLTJUploadXml();
			// tBusinessDeal.run();
			LLCaseInforFeedback tLLCaseInforFeedback = new LLCaseInforFeedback();
			tLLCaseInforFeedback.createDomData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
