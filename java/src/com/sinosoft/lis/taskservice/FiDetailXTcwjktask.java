package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.f1print.FiDetailXTcwjkBL;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class FiDetailXTcwjktask extends TaskThread {
	
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	//提数日期
	private Date tDate = null;
	
	public FiDetailXTcwjktask()
	{}
	
	public boolean SaveData(VData cInputData){

		run();
		return true;
        }
		
	public void run()
    {      		
		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		
		//设置提数日期
		FDate chgdate = new FDate();
		tDate = chgdate.getDate(PubFun.getCurrentDate());
		tDate = PubFun.calDate(tDate, -1, "D", null);
		
		tTransferData.setNameAndValue("StartDate",chgdate.getString(tDate));
		tTransferData.setNameAndValue("EndDate",chgdate.getString(tDate));
		
		tInputData.add(tTransferData);
		/**
		 * 调用保单迁出同步平台数据的接口
		 */
		long startFiDetailXTcwjkBL = System.currentTimeMillis();
		
		System.out.println("批处理开始提取协议退保管理费数据进行价税分离:"+PubFun.getCurrentTime());
		//准备提取数据
		try {
			FiDetailXTcwjkBL tFiDetailXTcwjkBL = new FiDetailXTcwjkBL(); 
			if(!tFiDetailXTcwjkBL.submitData(tInputData,"")){
				this.mErrors.copyAllErrors(tFiDetailXTcwjkBL.mErrors);
				System.out.println("协议退保管理费价税分离数据提取报错："+mErrors.getFirstError());
			  }
		} catch (Exception e) {
			System.out.println("协议退保管理费价税分离数据提取报错：");
			e.printStackTrace();
		}		
		System.out.println("协议退管理费保价税分离数据提取完毕:"+PubFun.getCurrentTime());
		
		//=====进行价税分离
//		long startPremSeparateTask = System.currentTimeMillis();
//		try {
//			PremSeparateTask tPremSeparateTask = new PremSeparateTask();
//			System.out.println("=====开始价税分离=====");
//			tPremSeparateTask.run();
//			System.out.println("=====价税分离结束=====");
//		} catch (Exception e) {
//			System.out.println("价税分离发生报错！");
//			e.printStackTrace();
//		}
//		long endPremSeparateTask = System.currentTimeMillis();
//		System.out.println("========== PremSeparateTask价税分离批处理-耗时： "+((endPremSeparateTask -startPremSeparateTask)/1000/60)+"分钟");

    }
	
	
	

	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		FiDetailXTcwjktask tFiDetailXTcwjktask = new FiDetailXTcwjktask();
		
		tFiDetailXTcwjktask.run();
	}
}
