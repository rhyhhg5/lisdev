package com.sinosoft.lis.taskservice;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * <p>Title: </p>
 *
 * <p>Description:
 *浙江外包案件承保信息批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author zhang
 * @version 1.1
 */
public class ZBCBXmlTrans extends TaskThread {

	private ExeSQL mExeSQL = new ExeSQL();

	// TransInfo 标识信息
	private Element tPACKET = null;
	
    //TransInfo 标识信息
	private Element tHEAD = null;
	
    //TransInfo 标识信息
	private Element tBODY = null;

	// 保单信息 PolicyInfo
	private Element tGrpContInfo = null;

	// 分单层数据 InsuredsInfo
	private Element tContInfos = null;

	// 分单险种层数据 Policys
	private Element tPolInfos = null;

	// 险种责任层数据 Benefits
	private Element tDutyInfos = null;

	// 保单信息SQL
	private String mGrpContInfoSQL = "";

	// 分单层数据SQL
	private String mContInfosSQL = "";

	// 分单险种层数据SQL
	private String mPolInfosSQL = "";

	// 险种责任层数据 SQL
	private String mDutyInfosSQL = "";

	// 保单信息SSRS
	private SSRS tGrpContInfoSSRS = null;

	// 分单层数据SSRS
	private SSRS tContInfosSSRS = null;

	// 分单险种层数据SSRS
	private SSRS tPolInfosSSRS = null;

	// 险种责任层数据SSRS
	private SSRS tDutyInfosSSRS = null;

	// 保单数
	private int GrpContInfolen = 0;

	// 分单数
	private int ContInfoslen = 0;

	// 险种数
	private int PolInfoslen = 0;

	// 责任数
	private int DutyInfoslen = 0;
	
	/** 应用路径 */
	private String mURL = "";

	/** 文件路径 */
	private String mFilePath = "";
	String opr="";
	// 执行任务
	public void run() {
		
		getInputData();
		
		if(!dealData()){
			 System.out.println("承保上报出问题了");
            opr="false";
		}else{
			 System.out.println("承保上报成功了");
       	 opr="true";
		}
	}
	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
	private void getInputData() {

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);
		//本地测试
//		mURL = "D:\\picc\\ui\\";
		//核心存储路径
		mURL = mURL + "vtsfile/WBClaim/8633/ZNewMissions/CHENGBAO/"+PubFun.getCurrentDate()+"/";
		//本地测试路径
//		mURL = mURL + "vtsfile\\WBClaim\\8633\\ZNewMissions\\承保\\"+PubFun.getCurrentDate()+"\\";
		
		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}

	}

	public boolean dealData() {

		mGrpContInfoSQL = " Select Lcg.Grpcontno As GrpContNo, "
				+ "        'TB' As OperateType, '' EdorType,'' EdorName,"
				+ "        '' As EdorValiDate, "
				+ "        Lcg.Cvalidate As ValidDate, "
				+ "        Lcg.Cinvalidate As EndData, "
				+ "        Lcg.Managecom As ManageCom, "
				+ "        Lcg.Stateflag As StateFlag, "
				+ "        Lcg.Appntno As AppntNo, "
				+ "        Lcg.Grpname As GrpName, "
				+ "        (Select Postaladdress "
				+ "         From Lcgrpappnt "
				+ "         Where Grpcontno = Lcg.Grpcontno) As PostalAddress, "
				+ "        Lcg.Peoples2 As Peoples2, "
				+ "        Lcg.Phone As Phone, "
				+ "        Lcg.Fax As Fax, "
				+ "        Lcg.Email As EMail, "
				+ "        Lca.ClaimBankCode As ClaimBankCode, "
				+ "        (Select Bankname "
				+ "         From Ldbank "
				+ "         Where Bankcode = Lca.ClaimBankCode Fetch First 1 Rows Only) As ClaimBankName, "
				+ "        Lca.ClaimBankAccNo As ClaimBankAccNo, "
				+ "        Lca.ClaimAccName As ClaimAccName, "
				+ "        Lcg.Paymode As PayMode, "
				+ "        Lcg.Prem As Premium, "
				+ "        Lcg.Remark As Remark " + " From Lcgrpcont Lcg,lcgrpappnt lca "
				+ " Where 1 = 1 " + " And Lcg.Appflag = '1' and lca.grpcontno=lcg.grpcontno "
				 + " And Lcg.Grpcontno In (Select a.Code "
				 + " From Ldcode a ,ldcode b "
				 + " where 1=1 and a.codetype = b.code and b.codetype = 'thridcompany' and a.comcode like '8633%' "
				 + " And a.Codename = char(current date - 1 day )) With Ur " //前一天
				;

		tGrpContInfoSSRS = mExeSQL.execSQL(mGrpContInfoSQL);
		GrpContInfolen = tGrpContInfoSSRS.getMaxRow();

		if (GrpContInfolen < 1) {
			System.out.println("无报送保单数据");
			return true;
		}

		for (int i = 1; i <= GrpContInfolen; i++) {
			tPACKET = new Element("PACKET");
			Document Doc = new Document(tPACKET);
			tPACKET.addAttribute("type", "REQUEST");
			tPACKET.addAttribute("version", "1.0");
			tHEAD = new Element("HEAD");
			tHEAD.addContent(new Element("REQUEST_TYPE").setText("TB"));
			tHEAD.addContent(new Element("TRANSACTION_NUM").setText("TB"));
			tPACKET.addContent(tHEAD);
			tBODY = new Element("BODY");

			tGrpContInfo = new Element("GrpContInfo");
			tContInfos = new Element("ContInfos");

			tGrpContInfo.addContent(new Element("GrpContNo")
					.setText(tGrpContInfoSSRS.GetText(i, 1)));
			tGrpContInfo.addContent(new Element("OperateType")
					.setText(tGrpContInfoSSRS.GetText(i, 2)));
			tGrpContInfo.addContent(new Element("EdorType")
					.setText(tGrpContInfoSSRS.GetText(i, 3)));
			tGrpContInfo.addContent(new Element("EdorName")
					.setText(tGrpContInfoSSRS.GetText(i, 4)));
			tGrpContInfo.addContent(new Element("EdorValiDate")
					.setText(tGrpContInfoSSRS.GetText(i, 5)));
			tGrpContInfo.addContent(new Element("CValiDate")
					.setText(tGrpContInfoSSRS.GetText(i, 6)));
			tGrpContInfo.addContent(new Element("CInValiDate")
					.setText(tGrpContInfoSSRS.GetText(i, 7)));
			tGrpContInfo.addContent(new Element("ManageCom")
					.setText(tGrpContInfoSSRS.GetText(i, 8)));
			tGrpContInfo.addContent(new Element("StateFlag")
					.setText(tGrpContInfoSSRS.GetText(i, 9)));
			tGrpContInfo.addContent(new Element("AppntNo")
					.setText(tGrpContInfoSSRS.GetText(i, 10)));
			tGrpContInfo.addContent(new Element("GrpName")
					.setText(tGrpContInfoSSRS.GetText(i, 11)));
			tGrpContInfo.addContent(new Element("PostalAddress").setText(tGrpContInfoSSRS
					.GetText(i, 12)));
			tGrpContInfo.addContent(new Element("Peoples2").setText(tGrpContInfoSSRS
					.GetText(i, 13)));
			tGrpContInfo.addContent(new Element("Phone").setText(tGrpContInfoSSRS
					.GetText(i, 14)));
			tGrpContInfo.addContent(new Element("Fax")
					.setText(tGrpContInfoSSRS.GetText(i, 15)));
			tGrpContInfo.addContent(new Element("EMail")
					.setText(tGrpContInfoSSRS.GetText(i, 16)));
			tGrpContInfo.addContent(new Element("ClaimBankCode")
					.setText(tGrpContInfoSSRS.GetText(i, 17)));
			tGrpContInfo.addContent(new Element("ClaimBankName")
					.setText(tGrpContInfoSSRS.GetText(i, 18)));
			tGrpContInfo.addContent(new Element("ClaimBankAccNo")
					.setText(tGrpContInfoSSRS.GetText(i, 19)));
			tGrpContInfo.addContent(new Element("ClaimAccName")
					.setText(tGrpContInfoSSRS.GetText(i, 20)));
			tGrpContInfo.addContent(new Element("PayMode")
					.setText(tGrpContInfoSSRS.GetText(i, 21)));
			tGrpContInfo.addContent(new Element("Prem")
			.setText(tGrpContInfoSSRS.GetText(i, 21)));
			tGrpContInfo.addContent(new Element("Remark")
			.setText(tGrpContInfoSSRS.GetText(i, 21)));
			mContInfosSQL = " Select Lci.Contno As ContNo, "
					+ "        Lci.Insuredno As InsuredNo, "
					+ "        '' As EdorType, "
					+ "        '' As EdorName, "
					+ "        '' As EdorValidate, "
					+ "        Lci.Name As InsuredName, "
					+ "        Lci.Sex As InsuredSex, "
					+ "        Lci.Birthday As InsuredBirthday, "
					+ "        Lci.Idtype As InsuredIDType, "
					+ "        Lci.Idno As InsuredIDNo, "
					+ "        Lci.Relationtomaininsured As RelationToMainInsured, "
					+ "        Lco.CValiDate As CValiDate, "
					+ "        Lco.Cinvalidate As CInValiDate, "
					+ "        Lco.Prem As Prem, "
					+ "        Lco.Stateflag As StateFlag, "
					+ "        Lci.Bankcode As BankCode, "
					+ "        (Select Bankname "
					+ "         From Ldbank "
					+ "         Where Bankcode = Lci.Bankcode Fetch First 1 Rows Only) As BankName, "
					+ "        Lci.Bankaccno As BankAccNo, "
					+ "        Lci.Accname As AccName, "
					+ "        Lci.Insuredstat As InsuredStat, "
					+ "        Lci.Grpinsuredphone As Phone, "
					+ "        Lco.Remark As Remark, "
					+ "        Lci.Contplancode As ContPlanCode, "
					+ "        (Select Contplanname "
					+ "         From Lccontplandutyparam "
					+ "         Where Grpcontno = Lcc.Grpcontno "
					+ "         And Contplancode = Lci.Contplancode Fetch First 1 Rows Only) As ContPlanName "
					+ " From Lcgrpcont Lcc, " + "      Lccont Lco, "
					+ "      Lcinsured Lci " + " Where 1=1 "
					+ " And Lcc.Grpcontno = Lco.Grpcontno "
					+ " And Lco.Contno = Lci.Contno "
					+ " And Lcc.Appflag = '1' " + " And Lco.Appflag = '1' "
					+ " And lco.poltype<>'2' " + " and lcc.grpcontno='"
					+ tGrpContInfoSSRS.GetText(i, 1) + "' "
					+ " And not Exists (Select 1 "
					+ "        From Ljagetendorse "
					+ "        Where Contno = Lco.Contno "
					+ "        And Feeoperationtype = 'NI')"
			;

			tContInfosSSRS = mExeSQL.execSQL(mContInfosSQL);
			ContInfoslen = tContInfosSSRS.getMaxRow();
			for (int j = 1; j <= ContInfoslen; j++) {
				Element tContInfo = new Element("ContInfo");
				tContInfo.addContent(new Element("ContNo")
						.setText(tContInfosSSRS.GetText(j, 1)));
				tContInfo.addContent(new Element("InsuredNo")
						.setText(tContInfosSSRS.GetText(j, 2)));
				tContInfo.addContent(new Element("EdorType")
						.setText(tContInfosSSRS.GetText(j, 3)));
				tContInfo.addContent(new Element("EdorName")
						.setText(tContInfosSSRS.GetText(j, 4)));
				tContInfo.addContent(new Element("EdorValidate")
						.setText(tContInfosSSRS.GetText(j, 5)));
				tContInfo.addContent(new Element("InsuredName").setText(tContInfosSSRS
						.GetText(j, 6)));
				tContInfo.addContent(new Element("InsuredSex").setText(tContInfosSSRS
						.GetText(j, 7)));
				tContInfo.addContent(new Element("InsuredBirthday")
						.setText(tContInfosSSRS.GetText(j, 8)));
				tContInfo.addContent(new Element("InsuredIDType").setText(tContInfosSSRS
						.GetText(j, 9)));
				tContInfo.addContent(new Element("InsuredIDNo").setText(tContInfosSSRS
						.GetText(j, 10)));
				tContInfo.addContent(new Element("RelationToMainInsured")
						.setText(tContInfosSSRS.GetText(j, 11)));
				tContInfo.addContent(new Element("CValiDate")
						.setText(tContInfosSSRS.GetText(j, 12)));
				tContInfo.addContent(new Element("CInValiDate")
						.setText(tContInfosSSRS.GetText(j, 13)));
				tContInfo.addContent(new Element("Prem")
						.setText(tContInfosSSRS.GetText(j, 14)));
				tContInfo.addContent(new Element("StateFlag")
						.setText(tContInfosSSRS.GetText(j, 15)));
				tContInfo.addContent(new Element("BankCode")
						.setText(tContInfosSSRS.GetText(j, 16)));
				tContInfo.addContent(new Element("BankName").setText(tContInfosSSRS
						.GetText(j, 17)));
				tContInfo.addContent(new Element("BankAccNo")
						.setText(tContInfosSSRS.GetText(j, 18)));
				tContInfo.addContent(new Element("AccName")
						.setText(tContInfosSSRS.GetText(j, 19)));
				tContInfo.addContent(new Element("InsuredStat")
						.setText(tContInfosSSRS.GetText(j, 20)));
				tContInfo.addContent(new Element("Phone")
						.setText(tContInfosSSRS.GetText(j, 21)));
				tContInfo.addContent(new Element("Remark")
						.setText(tContInfosSSRS.GetText(j, 22)));
				tContInfo.addContent(new Element("ContPlanCode")
						.setText(tContInfosSSRS.GetText(j, 23)));
				tContInfo.addContent(new Element("ContPlanName")
						.setText(tContInfosSSRS.GetText(j, 24)));
				
				mPolInfosSQL = " Select Lcp.Riskcode As RisCode, "
						+ "        (Select Riskname "
						+ "         From Lmriskapp "
						+ "         Where Riskcode = Lcp.Riskcode) As RiskName, "
						+ "        Lcp.Cvalidate As CValiDate, "
						+ "        Lcp.Enddate As EndDate, "
						+ "        Lcp.Amnt As Amnt, "
						+ "        Lcp.Stateflag As StateFlag, "
						+ "        Lcp.Polno As Polno " + " From Lccont Lco, "
						+ "      Lcpol Lcp, " + "      Lcinsured Lci "
						+ " Where Lco.Contno = Lcp.Contno "
						+ " And Lcp.Contno = Lci.Contno "
						+ " And Lcp.Insuredno = Lci.Insuredno "
						+ " And Lco.Appflag = '1' " + " And Lcp.Appflag = '1' "
						+ " And Lco.Grpcontno = '"
						+ tGrpContInfoSSRS.GetText(i, 1) + "' "
						+ " And Lcp.Contno = '" + tContInfosSSRS.GetText(j, 1)
						+ "' " + " And Lci.Insuredno = '"
						+ tContInfosSSRS.GetText(j, 2) + "' ";
				tPolInfosSSRS = mExeSQL.execSQL(mPolInfosSQL);
				PolInfoslen = tPolInfosSSRS.getMaxRow();
				tPolInfos = new Element("PolInfos");
				for (int k = 1; k <= PolInfoslen; k++) {
					Element tPolInfo = new Element("PolInfo");
					tPolInfo.addContent(new Element("RisCode")
							.setText(tPolInfosSSRS.GetText(k, 1)));
					tPolInfo.addContent(new Element("RiskName")
							.setText(tPolInfosSSRS.GetText(k, 2)));
					tPolInfo.addContent(new Element("CValiDate")
							.setText(tPolInfosSSRS.GetText(k, 3)));
					tPolInfo.addContent(new Element("EndDate")
							.setText(tPolInfosSSRS.GetText(k, 4)));
					tPolInfo.addContent(new Element("Amnt")
							.setText(tPolInfosSSRS.GetText(k, 5)));
					tPolInfo.addContent(new Element("StateFlag")
							.setText(tPolInfosSSRS.GetText(k, 6)));

					mDutyInfosSQL = " Select Lcd.Dutycode As DutyCode, "
							+ "        (Select Dutyname "
							+ "         From Lmduty "
							+ "         Where Dutycode = Lcd.Dutycode) As DutyName, "
							+ "        Lcd.Amnt As Amnt, "
							+ "        Lcd.Getlimit As GetLimit, "
							+ "        Lcd.Getrate As GetRate, "
							+ "        ' ' As BRemark " + " From Lcduty Lcd "
							+ " Where 1 = 1 " + " And Lcd.Polno = '"
							+ tPolInfosSSRS.GetText(k, 7) + "'";
					tDutyInfosSSRS = mExeSQL.execSQL(mDutyInfosSQL);
					tDutyInfos = new Element("DutyInfos");
					DutyInfoslen = tDutyInfosSSRS.getMaxRow();
					for (int m = 1; m <= DutyInfoslen; m++) {
						Element tDutyInfo = new Element("DutyInfo");
						tDutyInfo.addContent(new Element("DutyCode")
								.setText(tDutyInfosSSRS.GetText(m, 1)));
						tDutyInfo.addContent(new Element("DutyName")
								.setText(tDutyInfosSSRS.GetText(m, 2)));
						tDutyInfo.addContent(new Element("Amnt")
								.setText(tDutyInfosSSRS.GetText(m, 3)));
						tDutyInfo.addContent(new Element("GetLimit")
								.setText(tDutyInfosSSRS.GetText(m, 4)));
						tDutyInfo.addContent(new Element("GetRate")
								.setText(tDutyInfosSSRS.GetText(m, 5)));
						tDutyInfos.addContent((Element) tDutyInfo.clone());
					}
					tPolInfo.addContent((Element) tDutyInfos.clone());
					tPolInfos.addContent((Element) tPolInfo.clone());
				}

				tContInfo.addContent((Element) tPolInfos.clone());
				tContInfos.addContent(tContInfo);
			}

			tGrpContInfo.addContent(tContInfos);
			tBODY.addContent(tGrpContInfo);
			tPACKET.addContent(tBODY);
			

			mFilePath = mURL + "PH_TB_" + tGrpContInfoSSRS.GetText(i, 1)+"_" + PubFun.getCurrentDate2() +"_"+ PubFun.getCurrentTime2() +".xml";
			XMLOutputter outputter = new XMLOutputter("  ", true, "GBK");

			try {
				outputter.output(Doc, new FileOutputStream(mFilePath));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!sendXML(mFilePath)){
			  System.out.println("发送浙江承保文件失败！团单号为："+tGrpContInfoSSRS.GetText(i, 1));
			}
			
//			File tFile = new File(mFilePath);
//			tFile.delete();

		}

		return true;
	}

	private boolean sendXML(String cXmlFile){

		String getIPPort = "select codename ,codealias from ldcode where codetype='ZBClaim' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='ZBClaim' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			System.out.println("FTP服务器IP、用户名、密码、端口都不能为空，否则无法发送xml");
			return false;
		}
		
		String tIP = tIPSSRS.GetText(1, 1);
	    String tPort = tIPSSRS.GetText(1, 2);
	    String tUserName = tUPSSRS.GetText(1, 1);
	    String tPassword = tUPSSRS.GetText(1, 2);
		
		FTPTool tFTPTool = new FTPTool(tIP, tUserName,
				tPassword, Integer.parseInt(tPort));
		
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println("登录ftp服务器失败");
				return false;
			}
		} catch (SocketException ex) {
			System.out.println("无法登录ftp服务器");
			return false;
		} catch (IOException ex) {
			System.out.println("无法读取ftp服务器信息");
			return false;
		}
		try {
			tFTPTool.makeDirectory("./01PH/8633/NewMissions/");			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		if (!tFTPTool.upload("/01PH/8633/NewMissions", cXmlFile)) {
			System.out.println("浙江外包承保上载文件失败!");
			return false;
		}
		
		tFTPTool.logoutFTP();

		return true;
	}

	public static void main(String[] args) {

		ZBCBXmlTrans t = new ZBCBXmlTrans();
		t.run();

	}

}
