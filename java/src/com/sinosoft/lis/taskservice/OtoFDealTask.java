package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.otof.*;
import java.io.*;
import org.xml.sax.InputSource;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import java.net.*;
import org.apache.xml.serialize.*;
import java.util.*;
import org.xml.sax.InputSource;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.otof.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.xpath.*;
import com.sinosoft.lis.xpath.UniteXML;

/**
 * <p>Title: OtoFDealTask</p>
 *
 * <p>Description: 财务数据提交日处理 </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author zhangjun
 * @version 1.0
 */
public class OtoFDealTask extends TaskThread {
    public String bdate = ""; //request.getParameter("Bdate");
    public String edate = ""; //request.getParameter("Edate");
    public GlobalInput tG = new GlobalInput();
    private String flag = "true";
    public String tFlag = "0"; //手工提取
    private CErrors tError = null;
    private String Content = "";
    private String FlagStr = "";
    private String tdate = "";
    private String tname = "";
    private String pname = "";
    private String allpath = "";
    public String Path1 = "";
    public String Path = "";
    private BillXmlOutput tBillXmlOutput = new BillXmlOutput();
    private String transportmessage = "";
    public String danzhenghao = "";
    public String yearmonth = "";
    private String URLpath = "";
    private String tURL = ""; //request.getRequestURL().toString();
    private String ttURL = ""; //HttpUtils.getRequestURL(request).toString();
    private String ttAddress = ""; //request.getRemoteAddr();
    private String allname = "";
    private String xml2name = "";
    private int entrycount;
    private int num;


    public OtoFDealTask() {
    }

    public void run() {
        OtoFDealTask mOtoFDealTask = new OtoFDealTask();
        for (int i = 1; i <= 26; i++) {
            System.out.println("财务提数项目" + i);
            if (i==10 || i==11) continue;
            if (mOtoFDealTask.initDeal()) {
                System.out.println("initDeal() begin.....");
                Integer I = new Integer(i);
                String danzheng = I.toString();
                mOtoFDealTask.Setdanzhenghao(danzheng);
                mOtoFDealTask.GetPath();
                System.out.println("initDeal() end.....");
            }
            try {
                mOtoFDealTask.bussDeal();
                System.out.println("bussDeal() end.....");
            } catch (FileNotFoundException ex) {
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

        }
    }

    public static void main(String[] args) throws FileNotFoundException,
            Exception {
        OtoFDealTask mOtoFDealTask = new OtoFDealTask();
        for (int i = 1; i <= 9; i++) {
            if (mOtoFDealTask.initDeal()) {
                Integer I = new Integer(i);
                String danzheng = I.toString();
                mOtoFDealTask.Setdanzhenghao(danzheng);
                mOtoFDealTask.GetPath();
            }
            mOtoFDealTask.bussDeal();

        }
    }

    private boolean initDeal() {
        OtoFDealTask tOtoFDealTask = new OtoFDealTask();
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        String sql = "Select * from LDSysVar where SysVar = 'ServerRoot'";
        LDSysVarSet tLDSysVarSet = tLDSysVarDB.executeQuery(sql);
        LDSysVarSchema tLDSysVarSchema = tLDSysVarSet.get(1);
        Path = tLDSysVarSchema.getSysVarValue() + "/temp";
        tOtoFDealTask.SetPath(Path);
        //tOtoFDealTask.Setbdate(PubFun.getCurrentDate());
        //tOtoFDealTask.Setedate(PubFun.getCurrentDate());
        String dateSql = "Select date('" + PubFun.getCurrentDate() +
                         "')-1 day from dual";
        ExeSQL texeSQL = new ExeSQL();
        String getDate = texeSQL.getOneValue(dateSql);
        bdate = getDate;
        edate = getDate;
        System.out.println(Path);
        if (Path.endsWith("\\") || Path.endsWith("/")) {
            Path = Path.substring(0, Path.length() - 1);
        }
        System.out.println(Path);

        Path1 = tLDSysVarSchema.getSysVarValue() + "/";
        System.out.println(Path1);
        System.out.println(
                "------------------------------------------------------");
        if (!(Path1.endsWith("\\") || Path1.endsWith("/"))) {
            Path1 = Path1 + "/";
        }
        System.out.println(Path1);

        //tG=(GlobalInput)session.getValue("GI");
        String urlSql = "Select * from LDSysVar where SysVar = 'ServerURL'";
        tLDSysVarSet = tLDSysVarDB.executeQuery(urlSql);
        tLDSysVarSchema = tLDSysVarSet.get(1);
        String tempURL = tLDSysVarSchema.getSysVarValue();
        pname = Path + "/";
        URLpath = "ufin/addVoucherReturnXML.jsp";
        ttURL = tempURL + "otof/OtoF.jsp";
        tURL = ttURL.toString();
        System.out.println("aaa==" + ttURL);
        System.out.println("bbb==" + tURL);
        System.out.println("ccc==" + ttAddress);
        int tBeginidx = tURL.indexOf("otof/OtoF.jsp");
        String addVoucerUrl = tURL.substring(0, tBeginidx);
        if (addVoucerUrl.endsWith("/")) {
            addVoucerUrl = addVoucerUrl + URLpath;
        } else {
            addVoucerUrl = addVoucerUrl + "/" + URLpath;
        }
        System.out.println("aaa==" + addVoucerUrl);
        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(bdate);
        Date dedate = chgdate.getDate(edate);

        if (dbdate.compareTo(dedate) > 0) {
            flag = "false";
            Content = "日期录入错误1!";
            System.out.println(Content);

        }
        System.out.println("开始测试");
        return true;
    }

    public boolean bussDeal() throws FileNotFoundException, Exception {
        String strLDComSQL="";
        System.out.println(flag);
        if (flag.equals("true")) {
//     for (int i=1;i<=7;i++)
//     {
//        System.out.println("次数-------------------------"+i);
           LDComSchema mLDComSchema = new LDComSchema();
           LDComDB mLDComDB = new LDComDB();
           LDComSet mLDComSet = new LDComSet();
           strLDComSQL = "Select * from LDCom where sign = '1' and comcode not in ('86','860000') and length(trim(comcode)) = 4 order by comcode";
           mLDComSet = mLDComDB.executeQuery(strLDComSQL);
           System.out.println(strLDComSQL);
           for (int j=1;j<=mLDComSet.size();j++)
           {
               mLDComSchema = mLDComSet.get(j);
               VData vData = new VData();
               tG.Operator = "cwad";
               tG.ManageCom = mLDComSchema.getComCode();
               System.out.println("tG.ManageCom = " +  mLDComSchema.getComCode());
               vData.addElement(tG);
               vData.addElement(bdate);
               vData.addElement(edate);
               Integer itemp = new Integer(danzhenghao);
               vData.addElement(itemp);
               vData.addElement(tFlag);
               String realPath = Path + "/";
               vData.addElement(realPath);
               vData.addElement(yearmonth);
               System.out.println("-----------------------------------------");
               System.out.println("realPath : " + realPath);
               System.out.println("-----------------------------------------");
               System.out.println("加载完成");
               OtoFUI tOtoFUI = new OtoFUI();
               if (!tOtoFUI.submitData(vData, "PRINT")) {
                   tError = tOtoFUI.mErrors;
                   System.out.println("fail to get print data");
                   flag = "false";
                   Content = "提取数据出错，原因是：" + tError.getFirstError();
                   FlagStr = "Fail";

               } else {
                   System.out.println("nook");
                   int i = 0;
                   int entrynumber = 0;
                   num = tOtoFUI.getFilenumUI();
                   vData = tOtoFUI.getResult();
                   CErrors mErrors = new CErrors();
                   System.out.println("--------------ok!------------------");
                   OtoFExport tOtoFExport = (OtoFExport) vData.
                                            getObjectByObjectName("OtoFExport",
                           0);
                   tdate = PubFun.getCurrentDate();
                   tname = "bill" + itemp + "-" + tdate;
                   System.out.println(
                           "------Before do outputDocumentToFileOtoF------");
//                if (!danzhenghao.equals("3")) {
                   tOtoFExport.outputDocumentToFileOtoF(pname, tname);
//                }
                   System.out.println("outputtoaddVoucherUrl start");
//      		System.out.println("addVoucerUrl:"+addVoucerUrl);
//      		tOtoFExport.outputDocumentToFileOtoF(addVoucerUrl);
//      		System.out.println("outputtoaddVoucherUrl end");


                   allpath = Path1 + "temp/";
//                if (danzhenghao.equals("3")) {
//                    tname = tname + "-1.xml";
//                } else {
                   tname = tname + ".xml";
//                }
                   System.out.println(allpath + tname);
                   System.out.println("------Before do BillXmlOutput------");
                   if (!tBillXmlOutput.findfile(allpath, tname)) {
                       Content = "生成第一个xml出错";
                       flag = "false";
                       FlagStr = "Fail";
                   } else {
                       System.out.println("toFinxml中的文件已经生成完毕");
//                    if (danzhenghao.equals("3")) {
//                        System.out.println("生成PiccFinxml中的文件");
//                        for (int n = 1; n <= num; n++) {
//                            System.out.println(allpath + "bill" + itemp +
//                                               "-" + tdate + "-" + n + ".xml");
//                            FileInputStream fis1 = new FileInputStream(
//                                    allpath + "bill" + itemp + "-" + tdate +
//                                    "-" + n + ".xml");
//                            UFFecade uff = new UFFecade(fis1);
//                            uff.read();
//                            uff.setFilecount(n);
//                            uff.setFilenum(num);
//                            uff.convert(Path1);
//                            uff.writeReturnXML(Path1);
//                        }
//                    } else {
                       FileInputStream fis1 = new FileInputStream(allpath +
                               tname);

                       UFFecade uff = new UFFecade(fis1);
                       uff.read();
                       uff.convert(Path1);
                       uff.writeReturnXML(Path1);
                       mErrors = uff.geterrors();
                       entrynumber = uff.getentrycount();
                       transportmessage = PubFun.changForHTML(uff.
                               gettransportmessage());
//                    }
                   }

                   if (flag.equals("true")) {
                       if (mErrors.getErrorCount() > 0) {
                           Content = transportmessage + " 有部分错误，请查看错误日志";
                           FlagStr = "Succ";
                       } else if (entrynumber == 0) {
                           Content = " 没有需要提交的数据";
                           FlagStr = "Succ";
                       } else {
                           Content = " 保存成功" + transportmessage;
                           FlagStr = "Succ";
                       }
                       System.out.println("开始执行日志表的操作");
                       LITranInfoSchema yLITranInfoSchema = new
                               LITranInfoSchema();
                       LITranInfoSet yLITranInfoSet = new LITranInfoSet();
                       yLITranInfoSet = (LITranInfoSet) vData.
                                        getObjectByObjectName("LITranInfoSet",
                               0);
                       LITranLogSchema tLITranLogSchema = new LITranLogSchema();
                       if (yLITranInfoSet.get(1) != null) {
                           yLITranInfoSchema.setSchema(yLITranInfoSet.get(1));
                           System.out.println("批次号是:" +
                                              yLITranInfoSchema.getBatchNo());
                           tLITranLogSchema.setBatchNo(yLITranInfoSchema.
                                   getBatchNo());
                           tLITranLogSchema.setFileName(tname);
                           tLITranLogSchema.setFilePath(pname);
                           tLITranLogSchema.setStartDate(bdate);
                           tLITranLogSchema.setEndDate(edate);
                           tLITranLogSchema.setFlag(FlagStr);
                           LITranLogUI tLITranLogUI = new LITranLogUI();
                           vData.clear();
                           vData.addElement(tLITranLogSchema);
                           vData.addElement(tG);
                           if (!tLITranLogUI.submitData(vData, "INSERT")) {
                               Content = " 日志表保存失败";
                               FlagStr = "Fail";
//               break;
                           }
                       }
                       System.out.println("执行日志表的操作结束");

                   }

               }
               try {} catch (Exception e) {
                   e.printStackTrace();
               }
           }
           
//  } //end for
        } //end if
        flag="true";
        return true;
    }

    private boolean CommisionDeal() throws FileNotFoundException, Exception {

        if (flag.equals("true")) {
            VData vData = new VData();
            vData.addElement(tG);
            vData.addElement(bdate);
            vData.addElement(edate);
            Integer itemp = new Integer(10);
            vData.addElement(itemp);
            vData.addElement(tFlag);
            System.out.println("加载完成");
            OtoFUI tOtoFUI = new OtoFUI();
            if (!tOtoFUI.submitData(vData, "PRINT")) {
                tError = tOtoFUI.mErrors;
                System.out.println("fail to get print data");
                flag = "false";
                Content = "提取数据出错，原因是：" + tError.getFirstError();
                FlagStr = "Fail";

            } else {
                int i = 0;
                int entrynumber = 0;
                vData = tOtoFUI.getResult();
                CErrors mErrors = new CErrors();
                System.out.println("ok!");
                OtoFExport tOtoFExport = (OtoFExport) vData.
                                         getObjectByObjectName("OtoFExport",
                        0);
                tdate = PubFun.getCurrentDate();
                tname = "bill" + itemp + "-" + tdate;
                tOtoFExport.outputDocumentToFileOtoF(pname, tname);
                System.out.println("outputtoaddVoucherUrl start");

                allpath = Path1 + "temp/";
                tname = tname + ".xml";
                System.out.println(allpath + tname);
                if (!tBillXmlOutput.findfile(allpath, tname)) {
                    Content = "生成第一个xml出错";
                    flag = "false";
                    FlagStr = "Fail";
                } else {
                    FileInputStream fis1 = new FileInputStream(allpath +
                            tname);
                    System.out.println("temp1");
                    UFFecade uff = new UFFecade(fis1);
                    System.out.println("temp2");
                    uff.read();
                    System.out.println("temp3");
                    uff.convert(Path1);
                    System.out.println("temp4");
                    uff.writeReturnXML(Path1);
                    mErrors = uff.geterrors();
                    entrynumber = uff.getentrycount();
                    transportmessage = PubFun.changForHTML(uff.
                            gettransportmessage());

                }

                if (flag.equals("true")) {
                    if (mErrors.getErrorCount() > 0) {
                        Content = transportmessage + " 有部分错误，请查看错误日志";
                        FlagStr = "Succ";
                    } else if (entrynumber == 0) {
                        Content = " 没有需要提交的数据";
                        FlagStr = "Succ";
                    } else {
                        Content = " 保存成功" + transportmessage;
                        FlagStr = "Succ";
                    }
                    System.out.println("开始执行日志表的操作");
                    LITranInfoSchema yLITranInfoSchema = new
                            LITranInfoSchema();
                    LITranInfoSet yLITranInfoSet = new LITranInfoSet();
                    yLITranInfoSet = (LITranInfoSet) vData.
                                     getObjectByObjectName("LITranInfoSet",
                            0);
                    LITranLogSchema tLITranLogSchema = new LITranLogSchema();
                    if (yLITranInfoSet.get(1) != null) {
                        yLITranInfoSchema.setSchema(yLITranInfoSet.get(1));
                        System.out.println("批次号是:" +
                                           yLITranInfoSchema.getBatchNo());
                        tLITranLogSchema.setBatchNo(yLITranInfoSchema.
                                getBatchNo());
                        tLITranLogSchema.setFileName(tname);
                        tLITranLogSchema.setFilePath(pname);
                        tLITranLogSchema.setStartDate(bdate);
                        tLITranLogSchema.setEndDate(edate);
                        tLITranLogSchema.setFlag(FlagStr);
                        LITranLogUI tLITranLogUI = new LITranLogUI();
                        vData.clear();
                        vData.addElement(tLITranLogSchema);
                        vData.addElement(tG);
                        if (!tLITranLogUI.submitData(vData, "INSERT")) {
                            Content = " 日志表保存失败";
                            FlagStr = "Fail";
                        }
                    }
                    System.out.println("执行日志表的操作结束");

                }
            }
        }

        return true;
    }

    private String convert(String in) throws Exception {
        StringTokenizer st = new StringTokenizer(in, "||");
        String result = "";

        while (st.hasMoreTokens()) {
            String content = st.nextToken().trim();
            if (content.indexOf("<?") < 0) {
                continue;
            }
            content.substring(content.indexOf("<?"));

            System.out.println(content);

            StringReader stringReader = new StringReader(content);

            InputSource inputS = new InputSource(stringReader);

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(inputS);
            Node root = doc.getDocumentElement();

            String company = XPathUsage.findValue(root, "//company");
            String retCode = XPathUsage.findValue(root, "//resultcode");
            String bdocid = XPathUsage.findValue(root, "//bdocid");

            retCode = retCode.replace((char) 13, ' ').replace((char) 10, ' ').
                      trim();
            bdocid = bdocid.replace((char) 13, ' ').replace((char) 10, ' ').
                     trim();

            String oneRes;

            if ("1".equals(retCode)) {
                oneRes = bdocid + "交易成功";
            } else {
                oneRes = bdocid + "交易失败! 原因:" +
                         XPathUsage.findValue(root, "//errMsg") + "\t返回码:" +
                         retCode;
            }

            result = result + oneRes + "\n";
        }

        return result;
    }

    public boolean Setbdate(String ndate) {
        bdate = ndate;
        return true;
    }

    public String Getbdate() {
        return bdate;
    }

    public boolean Setedate(String mdate) {
        edate = mdate;
        return true;
    }

    public String Getedate() {
        return edate;
    }

    public boolean Setdanzhenghao(String tDanzhanghao) {
        danzhenghao = tDanzhanghao;
        return true;
    }

    public String Getdanzhenghao() {
        return danzhenghao;
    }

    public boolean SettFlag(String aFlag) {
        tFlag = aFlag;
        return true;
    }

    public String GettFlag() {
        return tFlag;
    }

    public boolean SetPath(String nPath) {
        Path = nPath;
        return true;
    }

    public String GetPath() {
        return Path;
    }
    public boolean SetYearMonth(String nYearmonth) {
        yearmonth = nYearmonth;
        return true;
    }

    public String GetYearMonth() {
        return yearmonth;
    }
}
