package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.io.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import java.net.SocketException;

/**
 * <p>Title:MixedAgentTask.java </p>
 *
 * <p>Description:集团交叉销售批处理 </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company:sinosoft </p>
 *
 * @author huxl
 * @version 1.0
 */
public class MixedAgentNewTask extends TaskThread
{
    public String mCurrentDate = PubFun.getCurrentDate();
    private FileWriter mFileWriter;
    private BufferedWriter mBufferedWriter;
    private String mURL = "";

    public MixedAgentNewTask()
    {
        try
        {
            jbInit();
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void run()
    {
        System.out.println("开始提取" + this.mCurrentDate + "日的交叉销售数据,开始时间是" +
                           PubFun.getCurrentTime());
        if (!getMixedAgent())
        {
            return;
        }
        System.out.println("提取" + this.mCurrentDate + "日的交叉销售数据完成,完成时间是" +
                           PubFun.getCurrentTime());
    }

    private boolean getMixedAgent()
    {
        String tSQL =
                "select SysVarValue from LDSYSVAR where Sysvar='JiaoChaURL'";
//        this.mURL = new ExeSQL().getOneValue(tSQL);
        this.mURL = "D:\\xmlBackup\\";
        if (mURL == null || mURL.equals(""))
        {
            System.out.println("没有找到集团交叉销售存储文件夹！");
            return false;
        }

        getS001();
        getS002();
        getD001();
        getD002();
        getP001();
        getP002();
//        FtpTransferFiles();
//        try
//        {
//            del(mURL);
//        } catch (IOException ex)
//        {
//            ex.printStackTrace();
//        }
        return true;
    }

    /**
     * 将生成文件传送到10.252.1.112的/home/gather/dat/目录下.
     */
    private void FtpTransferFiles()
    {
        FTPTool tFTPTool = new FTPTool("10.252.0.12", "gather", "gather12", 21,
                                       "ActiveMode");
        try
        {
            if (!tFTPTool.loginFTP())
            {
                System.out.println(tFTPTool.getErrContent(1));
            } else
            {
                String[] tFileNames = new File(mURL).list();
                if (tFileNames == null)
                {
                    System.out.println("集团交叉销售Error:获取目录下的文件失败，请检查该目录是否存在！");
                } else
                {
                    for (int i = 0; i < tFileNames.length; i++)
                    {
                        tFTPTool.upload("/home/gather/dat/",
                                        mURL + tFileNames[i]);
                    }
                }
            }
        } catch (SocketException ex)
        {
            ex.printStackTrace();
        } catch (IOException ex)
        {
            ex.printStackTrace();
        } finally
        {
            tFTPTool.logoutFTP();
        }
    }

    /**
     * 营销员信息表
     */
    private void getS001()
    {
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "000085S001" + mCurrentDate.replaceAll("-", "") + ".txt", true);
            String tSQL =
                    "select agentcode  营销员工号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                    "0 当事方编码,'' 当事方角色代码,name 姓名,ts_fmt(date(birthday),'yyyymmdd') 出生日期,CodeName('crs_cst_linker_sex',sex) 性别,managecom 销售机构代码," +
                    "(select name from ldcom where comcode=laagent.managecom ) 销售机构名称," +
                    "'11' 证件类型代码,'居民身份证' 证件类型名称,idno 证件号码,phone 联系电话," +
                    "mobile 联系手机,email 电子邮件,case when agentstate<='05' then '1' else  '0' end  在职状态代码," +
                    "case when agentstate<='05' then '在职' else  '离职' end 在职状态名称,'2'  营销员类型代码," +
                    "'个人代理' 营销员类型名称,'I' 数据变更类型,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' 时间戳 " +
                    "from laagent " +
                    "where branchtype2 not in ('04','05') and ModifyDate = current date - 1 day with ur";
            int start = 1;
            int nCount = 200;
            while (true)
            {
                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }
                String tempString = tSSRS.encode();
                String[] tempStringArr = tempString.split("\\^");
                //默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
//            mFileWriter = new FileWriter("C:\\000085S001" +
//                                         mCurrentDate.replaceAll("-", "") +
//                                         ".txt");
//            System.out.println(mFileWriter.getEncoding());
//            mBufferedWriter = new BufferedWriter(mFileWriter);
//          指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
                tFileOutputStream = new FileOutputStream(mURL + "000085S001" +
                        mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[S001]:Sales_Cod = '" +
                                          tSSRS.GetText(i, 1) +
                                          "' AND Comp_Cod = '" +
                                          tSSRS.GetText(i, 2) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");
                    tArr[3] = tArr[3].substring(1, tArr[3].length() - 1);
                    mBufferedWriter.write("I[S001]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
        } catch (IOException ex2)
        {
            System.out.println(ex2.getStackTrace());
        }
    }

    /**
     * 中介机构信息表
     */
    private void getS002()
    {
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "000085S002" + mCurrentDate.replaceAll("-", "") + ".txt", true);
            String tSQL = "select agentcom 中介机构ID,name 中介机构名称," +
                          "'000085' 子公司代码," +
                          "'中国人民健康保险股份有限公司' 子公司名称," +
                          "'310' 企业性质,ts_fmt(date(ChiefBusiness),'yyyymmdd') 成为中介日期,managecom 管理机构代码,(select name from ldcom where comcode=lacom.managecom ) 管理机构名称," +
                          "LicenseNo 企业注册证号,case when sellflag='Y' then '1' else  '9' end 状态," +
                          "Corporation  联系人姓名,'' 联系人性别,'' 联系人电话,'' 联系人手机,'' 联系人电子邮件,'I' 数据变更类型," +
                          "ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' 时间戳 " +
                          " from lacom where  branchtype='2' and branchtype2 not in ('04','05') and ModifyDate = current date - 1 day with ur";
            int start = 1;
            int nCount = 200;
            while (true)
            {
                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }
                String tempString = tSSRS.encode();
                String[] tempStringArr = tempString.split("\\^");

                tFileOutputStream = new FileOutputStream(mURL + "000085S002" +
                        mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[S002]:Brok_Cod = '" +
                                          tSSRS.GetText(i, 1) +
                                          "' AND Comp_Cod = '" +
                                          tSSRS.GetText(i, 3) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    mBufferedWriter.write("I[S002]:'" + t + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
        } catch (IOException ex2)
        {
        }
    }

    /**
     * 机构维度表
     */
    private void getD001()
    {
        String tSQL = "select a.comcode,'000085','中国人民健康保险股份有限公司'," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                      "(select codealias from ldcode where codetype='managearea' and code=substr(a.comcode,1,4))," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                      "(select codename from ldcode where codetype='managearea' and code=substr(a.comcode,1,4))," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                      "substr(a.comcode,1,4),(select name from ldcom where comcode=substr(a.comcode,1,4))," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                      "comcode,(select name from ldcom where comcode=substr(a.comcode,1,8))," + 
                      "comcode,(select name from ldcom where comcode=substr(a.comcode,1,8))," +
                      "comcode,(select name from ldcom where comcode=substr(a.comcode,1,8))," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                      "case when substr(a.comcode,5,4) = '0000' then '1' when substr(a.comcode,7,2) = '00' then '2' else '3' end,'','',a.sign,case when a.sign='1' then '正常' else '终止' end," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                      "(select codealias from ldcode where codetype='managecode' and code=substr(a.comcode,1,4))," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                      "(select codename from ldcode where codetype='managecode' and code=substr(a.comcode,1,4))," + 
//                按修改要求添加市一级的归集机构,县一级的暂时为空
                      "(select codealias from ldcode where codetype='managecode' and code=a.comcode)," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                      "(select codename from ldcode where codetype='managecode' and code=a.comcode)," + 
                      "''," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                      "''," + 
                      "'I',db2inst1.ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss'),''" +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                      "from ldcom a where ComGrade = '03' and ComCode <> '86000000'  and ModifyDate = current date - 1 day order by a.comcode with ur";

        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(tSQL);

        String tempString = tSSRS.encode();
        String[] tempStringArr = tempString.split("\\^");
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "000085D001" + mCurrentDate.replaceAll("-", "") +
                    ".txt");
            OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                    tFileOutputStream, "GBK");
            mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                mBufferedWriter.write("D[D001]:Orgcod = '" +
                                      tSSRS.GetText(i, 1) +
                                      "' AND Comp_Cod = '" +
                                      tSSRS.GetText(i, 2) + "'");
                mBufferedWriter.newLine();
                String t = tempStringArr[i].replaceAll("\\|", "','");
                mBufferedWriter.write("I[D001]:'" + t + "'");
                mBufferedWriter.newLine();
                mBufferedWriter.flush();
            }
            mBufferedWriter.close();
        } catch (IOException ex2)
        {
        }
    }

    /**
     * 险种维度表
     */
    private void getD002()
    {
        String tSQL =
                "select a.riskcode,a.riskname,a.riskshortname,'000085','中国人民健康保险股份有限公司'," +
                "a.riskver,'',(case when b.subriskflag='M' then '1' else '2' end),''," +
                "ts_fmt(date(b.startdate), 'yyyymmdd'),ts_fmt(date(b.enddate), 'yyyymmdd')," +
                "'','','','','','','',''," +
                "'','','','','','','',''," +
                "(case when b.riskperiod='L' then '2' else '1' end)," +
                "(case when c.riskgrpflag='2' then '0' when c.riskgrpflag='1' then '1' end)," +
                "(case when c.healthtype='6' or c.healthtype='7' then '1' when c.healthtype='4' then '3' when c.healthtype='5' then '4' else '2' end)," +
                "(case when c.healthtype='3' then '1' when c.healthtype='6' then '2' when c.healthtype='7' then '3' else '9' end)," +
                "'','','',''," +
                "'I',ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss'),'' " +
                "from lmrisk a,lmriskapp b,lfrisk c " +
                "where a.riskcode=b.riskcode and a.riskcode=c.riskcode " +
                "order by a.riskcode with ur";

        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(tSQL);

        String tempString = tSSRS.encode();
        String[] tempStringArr = tempString.split("\\^");
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "000085D002" + mCurrentDate.replaceAll("-", "") +
                    ".txt");
            OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                    tFileOutputStream, "GBK");
            mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                mBufferedWriter.write("D[D002]:Risk_Cod = '" +
                                      tSSRS.GetText(i, 1) +
                                      "' AND Comp_Cod = '" +
                                      tSSRS.GetText(i, 4) + "'");
                mBufferedWriter.newLine();
                String t = tempStringArr[i].replaceAll("\\|", "','");
                mBufferedWriter.write("I[D002]:'" + t + "'");
                mBufferedWriter.newLine();
                mBufferedWriter.flush();
            }
            mBufferedWriter.close();
        } catch (IOException ex2)
        {
        }
    }

    /**
     * 保单信息表
     */
    private void getP001()
    {
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "000085P001" + mCurrentDate.replaceAll("-", "") +
                    ".txt", true);
            String tSQL = "select " +
                          "lcc.ContNo pol_no, " +
                          "(select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo) pol_no_orig, " +
                          "'000085' comp_cod, " +
                          "'中国人民健康保险股份有限公司' comp_nam, " +
                          "ts_fmt(lcc.SignDate, 'yyyymmdd') date_sign, " +
                          "ts_fmt(lcc.CValiDate, 'yyyymmdd') date_begin, " +
                          "ts_fmt(lcc.CInValiDate, 'yyyymmdd') date_end, " +
                          "ts_fmt(lcc.FirstPayDate, 'yyyymmdd') date_fee," +
                          "(select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lcc.ContNo) pay_yrs, " +
                          "CodeName('crs_pay_mod', char(lcc.PayIntv)) pay_mod_cod, " +
                          "CodeAlias('crs_pay_mod', char(lcc.PayIntv)) pay_mod_nam, " +
                          "'1' grp_psn_flg, " +
                          "'99' chl_typ_cod, " +
                          "'其它' chl_typ_nam, " +
                          "'' chl_cod, " +
                          "'' chl_nam, " +
                          "lcc.AgentCom brok_cod, " +
                          "(select lac.Name from LACom lac where lac.AgentCom = lcc.AgentCom) brok_nam, " +
                          "CodeName('crs_salechnl_typ', lcc.crs_salechnl) crs_typ_cod, " +
                          "CodeAlias('crs_salechnl_typ', lcc.crs_salechnl) crs_typ_nam, " +
                          "lcc.AgentCode sales_cod," +
                          "(select Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) sales_nam," +
                          "lcc.GrpAgentCode crs_sales_cod, " +
                          "lcc.GrpAgentName crs_sales_nam, " +
                          "lcc.ManageCom org_sales_cod, " +
                          "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_sales_nam, " +
                          "lcc.ManageCom org_belg_cod, " +
                          "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_belg_nam, " +
                          "lcc.GrpAgentCom org_crs_cod, " +
                          "'' org_crs_nam, " +
                          "lcc.AppntNo cst_cod, " +
                          "lcc.AppntName cst_nam, " +
                          "'P' cst_typ, " +
                          "(select lcad.PostalAddress from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_addr, " +
                          "(select lcad.ZipCode from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_post," +
                          "CodeName('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_cod, " +
                          "CodeAlias('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_nam," +
                          "lcc.AppntIdNo cst_idno," +
                          "lcc.AppntName cst_linker_nam, " +
                          "CodeName('crs_cst_linker_sex', lcc.AppntSex) cst_linker_sex," +
                          "(select lcad.Phone from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_tel," +
                          "(select lcad.Mobile from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mob," +
                          "(select lcad.EMail from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mail," +
                          "(select value(lci.Name, '') || '' || value(lcad.PostalAddress, '') || '' || value(lcad.Phone, '') from LCInsured lci inner join LCAddress lcad on lcad.CustomerNo = lci.Insuredno and lcad.AddressNo = lci.AddressNo and lci.ContNo = lcc.ContNo and lci.Insuredno = lcc.Insuredno) insd_info," +
                          "(select lcb.Name from LCBnf lcb where lcb.ContNo = lcc.ContNo fetch first 1 rows only) beft_info," +
                          "lcc.InsuredIdNo insr_targ, " +
                          "lcc.InsuredName insr_targ_info," +
                          "lcc.Prem prem," +
                          "0 prem_sign," +
                          "0 prem_pol, " +
                          "lcc.Amnt amount, " +
                          "'1' percnt," +
                          "CodeName('crs_busstype_typ', lcc.Crs_BussType) bak1," +
                          "CodeAlias('crs_busstype_typ', lcc.Crs_BussType) bak2," +
                          "'' bak3," +
                          "'' bak4," +
                          "'' bak5," +
                          "'' bak6," +
                          "'' bak7," +
                          "'' bak8," +
                          "'I' data_upd_typ," +
                          "ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send," +
                          "'' date_update " +
                          "From LCCont lcc " +
                          "where 1 = 1 " +
                          "and lcc.ContType = '1' " +
                          "and lcc.AppFlag = '1' " +
                          "and lcc.UWFlag in ('4', '9') " +
                          "and lcc.Crs_SaleChnl in ('01', '02') " +
                          "and lcc.SignDate = current date - 1 day " +
                          " union all " +
                          "select " +
                          "	lgc.GrpContNo pol_no," +
                          "	'' pol_no_orig, " +
                          "	'000085' comp_cod, " +
                          "	'中国人民健康保险股份有限公司' comp_nam," +
                          "	ts_fmt(lgc.SignDate, 'yyyymmdd') date_sign," +
                          "	ts_fmt(lgc.CValiDate, 'yyyymmdd') date_begin," +
                          "	ts_fmt(lgc.CInValiDate, 'yyyymmdd') date_end," +
                          "	ts_fmt((select ljtf.PayDate from LJTempFee ljtf where ljtf.otherno = lgc.GrpContNo and ljtf.othernotype = '7' fetch first 1 rows only), 'yyyymmdd') date_fee," +
                          "	(select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lgc.GrpContNo) pay_yrs," +
                          "	CodeName('crs_pay_mod', char(lgc.PayIntv)) pay_mod_cod, " +
                          "	CodeAlias('crs_pay_mod', char(lgc.PayIntv)) pay_mod_nam," +
                          "	'0' grp_psn_flg, " +
                          "	'99' chl_typ_cod," +
                          "	'其它' chl_typ_nam," +
                          "	'' chl_cod," +
                          "	'' chl_nam," +
                          "	lgc.AgentCom brok_cod," +
                          "	(select lac.Name from LACom lac where lac.AgentCom = lgc.AgentCom) brok_nam," +
                          "	CodeName('crs_salechnl_typ', lgc.crs_salechnl) crs_typ_cod," +
                          "	CodeAlias('crs_salechnl_typ', lgc.crs_salechnl) crs_typ_nam," +
                          "	lgc.AgentCode sales_cod," +
                          "	(select Name from LAAgent laa where laa.AgentCode = lgc.AgentCode) sales_nam," +
                          "	lgc.GrpAgentCode crs_sales_cod," +
                          "	lgc.GrpAgentName crs_sales_nam," +
                          "	lgc.ManageCom org_sales_cod," +
                          "	(select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_sales_nam," +
                          "	lgc.ManageCom org_belg_cod," +
                          "	(select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_belg_nam," +
                          "	lgc.GrpAgentCom org_crs_cod," +
                          "	'' org_crs_nam," +
                          "	lgc.AppntNo cst_cod," +
                          "	lgc.GrpName cst_nam," +
                          "	'G' cst_typ," +
                          "	(select lcad.GrpAddress from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_addr," +
                          "	(select lcad.GrpZipCode from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_post," +
                          "	'' cst_idtyp_cod," +
                          "	'' cst_idtyp_nam," +
                          "	'' cst_idno," +
                          "	(select lcad.LinkMan1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_linker_nam," +
                          "	'' cst_linker_sex," +
                          "	(select lcad.Phone1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_tel," +
                          "	(select lcad.Mobile1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_mob," +
                          "	(select lcad.E_Mail1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_mail," +
                          "	(select lcc.InsuredName || '等' from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insd_info," +
                          "	'' beft_info," +
                          "	(select lcc.InsuredNo from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insr_targ," +
                          "	(select lcc.InsuredName from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insr_targ_info," +
                          "	lgc.Prem prem," +
                          "	0 prem_sign," +
                          "	0 prem_pol," +
                          "	lgc.Amnt amount," +
                          "	'1' percnt," +
                          "CodeName('crs_busstype_typ', lgc.Crs_BussType) bak1," +
                          "CodeAlias('crs_busstype_typ', lgc.Crs_BussType) bak2," +
                          "	'' bak3, " +
                          "	'' bak4, " +
                          "	'' bak5, " +
                          "	'' bak6, " +
                          "	'' bak7, " +
                          "	'' bak8, " +
                          "	'I' data_upd_typ," +
                          "	ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send," +
                          "	'' date_update " +
                          "From LCGrpCont lgc " +
                          " where 1 = 1 " +
                          "	and lgc.AppFlag = '1' " +
                          "	and lgc.UWFlag in ('4', '9') " +
                          "	and lgc.crs_salechnl in ('01', '02') " +
                          " and lgc.SignDate = current date - 1 day " +
                          " union all " +
                          "select " +
                          "lcc.ContNo pol_no, " +
                          "(select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo) pol_no_orig, " +
                          "'000085' comp_cod, " +
                          "'中国人民健康保险股份有限公司' comp_nam, " +
                          "ts_fmt(lcc.SignDate, 'yyyymmdd') date_sign, " +
                          "ts_fmt(lcc.CValiDate, 'yyyymmdd') date_begin, " +
                          "ts_fmt(lcc.CInValiDate, 'yyyymmdd') date_end, " +
                          "ts_fmt(lcc.FirstPayDate, 'yyyymmdd') date_fee," +
                          "(select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lcc.ContNo) pay_yrs, " +
                          "CodeName('crs_pay_mod', char(lcc.PayIntv)) pay_mod_cod, " +
                          "CodeAlias('crs_pay_mod', char(lcc.PayIntv)) pay_mod_nam, " +
                          "'1' grp_psn_flg, " +
                          "'99' chl_typ_cod, " +
                          "'其它' chl_typ_nam, " +
                          "'' chl_cod, " +
                          "'' chl_nam, " +
                          "lcc.AgentCom brok_cod, " +
                          "(select lac.Name from LACom lac where lac.AgentCom = lcc.AgentCom) brok_nam, " +
                          "CodeName('crs_salechnl_typ', lom.SaleChnl) crs_typ_cod, " +
                          "CodeAlias('crs_salechnl_typ', lom.SaleChnl) crs_typ_nam, " +
                          "lcc.AgentCode sales_cod," +
                          "(select Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) sales_nam," +
                          "lcc.GrpAgentCode crs_sales_cod, " +
                          "lcc.GrpAgentName crs_sales_nam, " +
                          "lcc.ManageCom org_sales_cod, " +
                          "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_sales_nam, " +
                          "lcc.ManageCom org_belg_cod, " +
                          "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_belg_nam, " +
                          "lcc.GrpAgentCom org_crs_cod, " +
                          "'' org_crs_nam, " +
                          "lcc.AppntNo cst_cod, " +
                          "lcc.AppntName cst_nam, " +
                          "'P' cst_typ, " +
                          "(select lcad.PostalAddress from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_addr, " +
                          "(select lcad.ZipCode from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_post," +
                          "CodeName('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_cod, " +
                          "CodeAlias('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_nam," +
                          "lcc.AppntIdNo cst_idno," +
                          "lcc.AppntName cst_linker_nam, " +
                          "CodeName('crs_cst_linker_sex', lcc.AppntSex) cst_linker_sex," +
                          "(select lcad.Phone from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_tel," +
                          "(select lcad.Mobile from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mob," +
                          "(select lcad.EMail from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mail," +
                          "(select value(lci.Name, '') || '' || value(lcad.PostalAddress, '') || '' || value(lcad.Phone, '') from LCInsured lci inner join LCAddress lcad on lcad.CustomerNo = lci.Insuredno and lcad.AddressNo = lci.AddressNo and lci.ContNo = lcc.ContNo and lci.Insuredno = lcc.Insuredno) insd_info," +
                          "(select lcb.Name from LCBnf lcb where lcb.ContNo = lcc.ContNo fetch first 1 rows only) beft_info," +
                          "lcc.InsuredIdNo insr_targ, " +
                          "lcc.InsuredName insr_targ_info," +
                          "lcc.Prem prem," +
                          "0 prem_sign," +
                          "0 prem_pol, " +
                          "lcc.Amnt amount, " +
                          "'1' percnt," +
                          "CodeName('crs_busstype_typ', lom.StandByFlag1) bak1," +
                          "CodeAlias('crs_busstype_typ', lom.StandByFlag1) bak2," +
                          "'' bak3," +
                          "'' bak4," +
                          "'' bak5," +
                          "'' bak6," +
                          "'' bak7," +
                          "'' bak8," +
                          "'I' data_upd_typ," +
                          "ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send," +
                          "'' date_update " +
                          "From LCCont lcc , LOMixAgent lom " +
                          "where 1 = 1 " +
                          "and lcc.ContNo=lom.ContNo " +
                          "and lcc.ContType = '1' " +
                          "and lom.ContType = '1' " +
                          "and lcc.AppFlag = '1' " +
                          "and lcc.UWFlag in ('4', '9') " +
                          "and lom.SaleChnl in ('01', '02') " +
                          "and lom.MakeDate = current date - 1 day " +
                          " union all " +
                          "select " +
                          "	lgc.GrpContNo pol_no," +
                          "	'' pol_no_orig, " +
                          "	'000085' comp_cod, " +
                          "	'中国人民健康保险股份有限公司' comp_nam," +
                          "	ts_fmt(lgc.SignDate, 'yyyymmdd') date_sign," +
                          "	ts_fmt(lgc.CValiDate, 'yyyymmdd') date_begin," +
                          "	ts_fmt(lgc.CInValiDate, 'yyyymmdd') date_end," +
                          "	ts_fmt((select ljtf.PayDate from LJTempFee ljtf where ljtf.otherno = lgc.GrpContNo and ljtf.othernotype = '7' fetch first 1 rows only), 'yyyymmdd') date_fee," +
                          "	(select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lgc.GrpContNo) pay_yrs," +
                          "	CodeName('crs_pay_mod', char(lgc.PayIntv)) pay_mod_cod, " +
                          "	CodeAlias('crs_pay_mod', char(lgc.PayIntv)) pay_mod_nam," +
                          "	'0' grp_psn_flg, " +
                          "	'99' chl_typ_cod," +
                          "	'其它' chl_typ_nam," +
                          "	'' chl_cod," +
                          "	'' chl_nam," +
                          "	lgc.AgentCom brok_cod," +
                          "	(select lac.Name from LACom lac where lac.AgentCom = lgc.AgentCom) brok_nam," +
                          "	CodeName('crs_salechnl_typ', lom.SaleChnl) crs_typ_cod," +
                          "	CodeAlias('crs_salechnl_typ', lom.SaleChnl) crs_typ_nam," +
                          "	lgc.AgentCode sales_cod," +
                          "	(select Name from LAAgent laa where laa.AgentCode = lgc.AgentCode) sales_nam," +
                          "	lgc.GrpAgentCode crs_sales_cod," +
                          "	lgc.GrpAgentName crs_sales_nam," +
                          "	lgc.ManageCom org_sales_cod," +
                          "	(select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_sales_nam," +
                          "	lgc.ManageCom org_belg_cod," +
                          "	(select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_belg_nam," +
                          "	lgc.GrpAgentCom org_crs_cod," +
                          "	'' org_crs_nam," +
                          "	lgc.AppntNo cst_cod," +
                          "	lgc.GrpName cst_nam," +
                          "	'G' cst_typ," +
                          "	(select lcad.GrpAddress from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_addr," +
                          "	(select lcad.GrpZipCode from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_post," +
                          "	'' cst_idtyp_cod," +
                          "	'' cst_idtyp_nam," +
                          "	'' cst_idno," +
                          "	(select lcad.LinkMan1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_linker_nam," +
                          "	'' cst_linker_sex," +
                          "	(select lcad.Phone1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_tel," +
                          "	(select lcad.Mobile1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_mob," +
                          "	(select lcad.E_Mail1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_mail," +
                          "	(select lcc.InsuredName || '等' from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insd_info," +
                          "	'' beft_info," +
                          "	(select lcc.InsuredNo from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insr_targ," +
                          "	(select lcc.InsuredName from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insr_targ_info," +
                          "	lgc.Prem prem," +
                          "	0 prem_sign," +
                          "	0 prem_pol," +
                          "	lgc.Amnt amount," +
                          "	'1' percnt," +
                          "CodeName('crs_busstype_typ', lom.StandByFlag1) bak1," +
                          "CodeAlias('crs_busstype_typ', lom.StandByFlag1) bak2," +
                          "	'' bak3, " +
                          "	'' bak4, " +
                          "	'' bak5, " +
                          "	'' bak6, " +
                          "	'' bak7, " +
                          "	'' bak8, " +
                          "	'I' data_upd_typ," +
                          "	ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send," +
                          "	'' date_update " +
                          "From LCGrpCont lgc  , LOMixAgent lom " +
                          " where 1 = 1 " +
                          "	and lgc.AppFlag = '1' " +
                          " and lom.ContType = '2' " +
                          " and lom.GrpContNo = lgc.GrpContNo " +
                          "	and lgc.UWFlag in ('4', '9') " +
                          "	and lom.SaleChnl in ('01', '02') " +
                          " and lom.MakeDate = current date - 1 day " +
                          "with ur ";
            int start = 1;
            int nCount = 200;
            while (true)
            {
                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }

                String tempString = tSSRS.encode();
                String[] tempStringArr = tempString.split("\\^");
                tFileOutputStream = new FileOutputStream(mURL +
                        "000085P001" + mCurrentDate.replaceAll("-", "") +
                        ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new
                        OutputStreamWriter(
                                tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[P001]:Pol_No = '" +
                                          tSSRS.GetText(i, 1) +
                                          "' AND Comp_Cod = '" +
                                          tSSRS.GetText(i, 3) +
                                          "' AND Date_Sign = '" +
                                          tSSRS.GetText(i, 5) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");
                    tArr[8] = tArr[8].substring(1, tArr[8].length() - 1);
                    for (int k = 47; k < 52; k++)
                    {
                        tArr[k] = tArr[k].substring(1, tArr[k].length() - 1);
                    }
                    mBufferedWriter.write("I[P001]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
        } catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 业务信息表
     */
    private void getP002()
    {
        String tSQL[] = new String[10];
        //承保个险
        getP002SQL0(tSQL);
        //承保团险
        getP002SQL1(tSQL);
        //理赔个险
        getP002SQL2(tSQL);
        //理赔团险
        getP002SQL3(tSQL);
        //个险续期
        getP002SQL4(tSQL);
        //团险续期
        getP002SQL5(tSQL);
        //个险满期
        getP002SQL6(tSQL);
        //团险满期
        getP002SQL7(tSQL);
        //团险保全
        getP002SQL8(tSQL);
        //个险保全
        getP002SQL9(tSQL);
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(
                    mURL + "000085P002" +
                    mCurrentDate.replaceAll("-", "") + ".txt", true);
            for (int m = 0; m < tSQL.length; m++)
            {
                int start = 1;
                int nCount = 200;
                while (true)
                {
                    SSRS tSSRS = new ExeSQL().execSQL(tSQL[m], start, nCount);
                    if (tSSRS.getMaxRow() <= 0)
                    {
                        break;
                    }
                    String tempString = tSSRS.encode();
                    String[] tempStringArr = tempString.split("\\^");

                    tFileOutputStream = new FileOutputStream(
                            mURL + "000085P002" +
                            mCurrentDate.replaceAll("-", "") + ".txt", true);
                    OutputStreamWriter tOutputStreamWriter = new
                            OutputStreamWriter(
                                    tFileOutputStream, "GBK");
                    mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                    for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                    {
                        mBufferedWriter.write("D[P002]:Pol_No = '" +
                                              tSSRS.GetText(i, 1) +
                                              "' AND Comp_Cod = '" +
                                              tSSRS.GetText(i, 2) +
                                              "' AND Date_Sign = '" +
                                              tSSRS.GetText(i, 4) +
                                              "' AND Act_No = '" +
                                              tSSRS.GetText(i, 5) +
                                              "' AND Date_Act = '" +
                                              tSSRS.GetText(i, 6) +
                                              "' AND Risk_Cod = '" +
                                              tSSRS.GetText(i, 21) + "'");
                        mBufferedWriter.newLine();
                        String t = tempStringArr[i].replaceAll("\\|", "','");
                        String[] tArr = t.split("\\,");
                        tArr[51] = tArr[51].substring(1, tArr[51].length() - 1);
                        for (int k = 22; k < 49; k++)
                        {
                            tArr[k] = tArr[k].substring(1, tArr[k].length() - 1);
                        }
                        mBufferedWriter.write("I[P002]:'");
                        for (int j = 0; j < tArr.length - 1; j++)
                        {
                            mBufferedWriter.write(tArr[j] + ",");
                        }
                        mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                        mBufferedWriter.newLine();
                        mBufferedWriter.flush();
                    }
                    mBufferedWriter.close();
                    start += nCount;
                }
            }
        } catch (IOException ex2)
        {
            ex2.printStackTrace();
        }
    }

    private void getP002SQL9(String[] tSQL)
    {
        //个险保全('CT','XT','WT')
        tSQL[9] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, X.EndorseMentNo  业务单证ID, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='I') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lbcont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,a.Amnt*(-1) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, " +
                "0 年金给付,X.money*(-1) 退保金, " +
                "-1 承保人次,a.Amnt*(-1) 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "CodeName('crs_busstype_typ',(select crs_busstype from lbcont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lbcont where contno=a.contno))," +
                "'','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                "from lbpol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.PolNo,b.MakeDate,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('CT','XT','WT') " +
                "and b.grpcontno = '00000000000000000000' and b.MakeDate = current date -1 day " +
                "group by EndorsementNo,FeeOperationType,polno,makedate) as X " +
                "where exists (select 1 from lbcont where contno=a.contno and crs_salechnl in ('01','02')) and a.PolNo = X.PolNo  " +
                //个险保全('FX','RB','CM','BF','TB')
                "union all " +
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, X.EndorseMentNo  业务单证ID, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='I') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "X.money 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "X.money 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, " +
                "0 年金给付,0 退保金, " +
                "(case FeeOperationType when 'RB' then 1 else 0 end) 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "'','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                "from lcpol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.PolNo,b.MakeDate,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('FX','RB','CM','BF','TB') " +
                "and b.grpcontno = '00000000000000000000' and b.MakeDate = current date -1 day " +
                "group by EndorsementNo,FeeOperationType,polno,makedate) as X " +
                "where exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) and a.PolNo = X.PolNo " +
                "with ur ";
    }

    private void getP002SQL8(String[] tSQL)
    {
        //团险保全('LQ','CM','ZB','ZT'),增人这里会契约统计冲突，所以这里不再提取
        tSQL[8] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='G') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "X.money 保费收入,0 签单保费,0 保单保费,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, " +
                "0 退保金, " +
                "(case FeeOperationType when 'ZT' then X.Peoples*(-1) else 0 end) 承保人次,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保险金额, " +
                "0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "'','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                "from lcgrppol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*) peoples,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('LQ','CM','ZB','ZT') " +
                "and b.grpcontno <> '00000000000000000000' and b.MakeDate = current date -1 day " +
                "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X " +
                "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) and a.GrpPolNo = X.GrpPolNo  " +
                //团险保全('XT','WT','CT')
                "union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, " +
                "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='G') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LBGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费, " +
                "a.Amnt*(-1) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, " +
                "X.money*(-1) 退保金, " +
                "-a.Peoples2 承保人次,a.Amnt*(-1) 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "CodeName('crs_busstype_typ',(select crs_busstype from lbgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lbgrpcont where grpcontno=a.grpcontno))," +
                "'','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                "from lbgrppol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*)*(-1) peoples,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('XT','WT','CT') " +
                "and b.grpcontno <> '00000000000000000000' and b.MakeDate = current date -1 day " +
                "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X " +
                "where exists (select 1 from lbgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) and a.GrpPolNo = X.GrpPolNo  with ur ";
    }

    private void getP002SQL7(String[] tSQL)
    {
        tSQL[7] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, b.EndorseMentNo 业务单证ID, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'MJ' 业务活动代码,'满期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,sum(b.GetMoney)*(-1) 满期给付,0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "'','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                "from lcgrppol a,ljagetendorse b " +
                "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) and b.Makedate = current date -1 day  " +
                "and a.GrpPolNo = b.GrpPolNo and b.FeeOperationtype = 'MJ' " +
                "group by a.GrpContno,b.EndorseMentNo,a.ManageCom,a.Riskcode,a.CustomerNo,a.GrpName, " +
                "a.GrpPolNo,a.payintv,a.SaleChnl,b.FeeOperationtype with ur ";
    }

    private void getP002SQL6(String[] tSQL)
    {
        tSQL[6] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, b.SerialNo 业务单证ID, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'MJ' 业务活动代码,'满期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,sum(b.GetMoney) 满期给付, " +
                "0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "'','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                "from lcpol a,ljsgetdraw b,ljaget c " +
                "where a.Conttype = '1' and exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) and c.ConfDate = current date -1 day  " +
                "and a.PolNo = b.PolNo and b.getnoticeno = c.actugetno " +
                "group by a.contno,b.SerialNo,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName, " +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate with ur ";
    }

    private void getP002SQL5(String[] tSQL)
    {
        tSQL[5] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, b.SerialNo 业务单证ID, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'XQ' 业务活动代码,'续期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "sum(b.SumActuPayMoney) 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,sum(b.SumActuPayMoney) 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "b.Paycount 缴费期次, " +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "'','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                "from lcgrppol a,ljapaygrp b " +
                "where a.SaleChnl in ('11','12') and b.Confdate = current date -1 day  " +
                "and a.GrpPolNo = b.GrpPolNo and b.Paycount > 1 " +
                "group by a.GrpContno,b.SerialNo,a.ManageCom,a.Riskcode,a.CustomerNo,a.GrpName, " +
                "a.GrpPolNo,a.payintv,a.SaleChnl,b.PayCount with ur ";
    }

    private void getP002SQL4(String[] tSQL)
    {
        tSQL[4] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, b.SerialNo 业务单证ID, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'XQ' 业务活动代码,'续期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "sum(b.SumActuPayMoney) 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,sum(b.SumActuPayMoney) 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, " +
                "0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "b.PayCount 缴费期次, " +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "'','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                "from lcpol a,ljapayperson b " +
                "where a.Conttype = '1' and exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) and b.Confdate = current date -1 day  " +
                "and a.PolNo = b.PolNo and b.Paycount > 1 " +
                "group by a.contno,b.SerialNo,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName, " +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,b.PayCount with ur ";
    }

    private void getP002SQL3(String[] tSQL)
    {
        tSQL[3] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司'子公司名称," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno),'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno),'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称," +
                "'LP' 业务活动代码,'理赔' 业务活动名称," +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称," +
                "a.CustomerNo 客户号,a.GrpName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述," +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称," +
                "0 保费收入,0 签单保费,0 保单保费,0 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "0 承保人次,0 保险金额," +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次," +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额," +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额," +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码," +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "'','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                "from lcgrppol a,LLClaimDetail b,LLCase c " +
                "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) and b.grppolno = a.grppolno " +
                "and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo " +
                "group by a.grpcontno,b.CaseNo,a.ManageCom,a.riskcode,a.CustomerNo,a.GrpName," +
                "a.GrpPolNo,a.payintv,a.SaleChnl,a.amnt,c.RgtState " +
                "union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司'子公司名称," +
                "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno),'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno),'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称, " +
                "'LP' 业务活动代码,'理赔' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LBGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno union select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno union select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出, " +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额, " +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次, " +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次, " +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额, " +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码, " +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次, " +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno union select crs_busstype from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno union select crs_busstype from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only))," +
                "'','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                "from lbgrppol a,LLClaimDetail b,LLCase c " +
                "where 1=1 and (exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) or " +
                "exists (select 1 from lbgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02'))) " +
                " and b.grppolno = a.grppolno  " +
                "and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo  " +
                "group by a.grpcontno,b.CaseNo,a.ManageCom,a.riskcode,a.CustomerNo,a.GrpName, " +
                "a.GrpPolNo,a.payintv,a.SaleChnl,a.amnt,c.RgtState with ur ";
    }

    private void getP002SQL2(String[] tSQL)
    {
        tSQL[2] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称," +
                "'LP' 业务活动代码,'理赔' 业务活动名称," +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称," +
                "a.AppntNo 客户号,a.AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,a.InsuredName 承保标的描述," +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称," +
                "0 保费收入,0 签单保费,0 保单保费,0 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "0 承保人次,0 保险金额," +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次," +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额," +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额," +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码," +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "'','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                "from lcpol a,LLClaimDetail b,LLCase c " +
                "where a.Conttype = '1' and exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) " +
                "and b.polno=a.polno and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo " +
                "group by a.contno,b.CaseNo,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName," +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,c.RgtState " +
                "union all " +
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称," +
                "'LP' 业务活动代码,'理赔' 业务活动名称," +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lbcont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno union select crs_salechnl from lbcont where contno=a.contno fetch first 1 row only)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno union select crs_salechnl from lbcont where contno=a.contno fetch first 1 row only)) 交叉销售类型名称," +
                "a.AppntNo 客户号,a.AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,a.InsuredName 承保标的描述," +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称," +
                "0 保费收入,0 签单保费,0 保单保费,0 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "0 承保人次,0 保险金额," +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次," +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额," +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额," +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码," +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno union select crs_busstype from lbcont where contno=a.contno fetch first 1 row only))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno union select crs_busstype from lbcont where contno=a.contno fetch first 1 row only))," +
                "'','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                "from lbpol a,LLClaimDetail b,LLCase c " +
                "where a.Conttype = '1' and (exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) or " +
                "exists (select 1 from lbcont where contno=a.contno and crs_salechnl in ('01','02')))" +
                "and b.polno=a.polno and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo " +
                "group by a.contno,b.CaseNo,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName," +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,c.RgtState with ur ";
    }

    private void getP002SQL1(String[] tSQL)
    {
        tSQL[1] =
                "select GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, prtno 业务单证ID," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称," +
                "CustomerNo 客户号,GrpName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "peoples2 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "'','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                "from lcgrppol a " +
                "where  exists (select 1 from lcgrpcont where grpcontno = a.grpcontno " +
                " and crs_salechnl in ('01','02') " +
                " and signdate = current date -1 day) " +
                " union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, prtno 业务单证ID," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',lom.SaleChnl) 交叉销售类型ID,CodeAlias('crs_salechnl_typ',lom.SaleChnl) 交叉销售类型名称," +
                "CustomerNo 客户号,GrpName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "peoples2 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "CodeName('crs_busstype_typ', lom.standbyflag1)," +
                "CodeAlias('crs_busstype_typ', lom.standbyflag1)," +
                "'','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                "from lcgrppol a ,LOMixAgent lom " +
                "where lom.SaleChnl in ('01','02') and lom.grpcontno = a.grpcontno and lom.ContType='2'" +
                "and lom.Makedate = current date -1 day " +
                "with ur ";
    }

    private void getP002SQL0(String[] tSQL)
    {
        tSQL[0] =
                "select contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(signdate), 'yyyymmdd') 签单日期, prtno 业务单证ID," +
                "ts_fmt(date(signdate), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称," +
                "AppntNo 客户号,AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "1 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "'','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                "from lcpol a " +
                "where Conttype = '1' " +
                " and exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) " +
                " and signdate = current date -1 day " +
                " union all " +
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(signdate), 'yyyymmdd') 签单日期, prtno 业务单证ID," +
                "ts_fmt(date(signdate), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ', lom.salechnl) 交叉销售类型ID,CodeAlias('crs_salechnl_typ', lom.salechnl) 交叉销售类型名称," +
                "AppntNo 客户号,AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "1 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "CodeName('crs_busstype_typ', lom.standbyflag1)," +
                "CodeAlias('crs_busstype_typ', lom.standbyflag1)," +
                "'','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                "from lcpol a ,LOMixAgent lom " +
                "where a.Conttype = '1' " +
                " and lom.ContType = '1' " +
                " and a.ContNo = lom.ContNo " +
                "and lom.SaleChnl in ('01','02') and lom.Makedate = current date -1 day " +
                "with ur ";
    }

    public static void main(String args[])
    {
        MixedAgentNewTask tMixedAgentTask = new MixedAgentNewTask();
        tMixedAgentTask.run();
        return;
    }

    /**
     * 删除目录下所有文件和子目录，根目录不删除
     * @param filepath String
     * @throws IOException
     */
    private void del(String filepath) throws IOException
    {
        File f = new File(filepath); //定义文件路径
        if (f.exists() && f.isDirectory())
        { //判断是文件还是目录
            if (f.listFiles().length == 0)
            { //若目录下没有文件则直接返回，这里不再删除根目录
                return;
            } else
            { //若有则把文件放进数组，并判断是否有下级目录
                File delFile[] = f.listFiles();
                int i = f.listFiles().length;
                for (int j = 0; j < i; j++)
                {
                    if (delFile[j].isDirectory())
                    {
                        del(delFile[j].getAbsolutePath()); //递归调用del方法并取得子目录路径
                    }
                    delFile[j].delete(); //删除文件，或者递归传入的子目录
                }
            }
        }
    }

    private void jbInit() throws Exception
    {
    }
    
}
