package com.sinosoft.lis.taskservice;

import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.task.*;

/**
 * <p>Title: 理赔自动结案批处理程序</p>
 * <p>Description: 每天由系统定时调用，把可以结案的理赔自动结案，用于理赔交费项目 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class LLClaimFinishTask  extends TaskThread
{
    CErrors mErrors = new CErrors();

    MMap mMap = null;

    GlobalInput mGlobalInput = new GlobalInput();

    /** 当前操作人员的机构 */
    String manageCom = null;

    public LLClaimFinishTask()
    {
    }

    /**
     * 当前机构
     * @param manageCom String
     */
    public LLClaimFinishTask(String manageCom)
    {
        this.manageCom = manageCom;
    }

    /**
     * 自动结案
     */
    public void run()
    {
        System.out.println("理赔自动结案运行开始！");
        List taskList = getTaskList();
        for (int i = 0; i < taskList.size(); i++)
        {
            mMap = new MMap();
            finish((String) taskList.get(i));
        }       

        System.out.println("理赔自动结案运行结束！");
    }

    /**
     * 得到待结案的任务列表
     * @return List
     */
    private List getTaskList()
    {
        List taskList = new ArrayList();
        String sql = "select b.PrepaidNo from LJTempFee a, llprepaidclaim b" 
                   +" where a.OtherNo = b.PrepaidNo " 
                   +" and b.RgtState ='05' and b.RgtType='2'"  
                   +" and a.EnterAccDate is not null ";
        if (manageCom != null) //只对本机构的数据进行操作
        {
            sql += "and b.ManageCom like '" + subComCode(manageCom) + "%' ";
        }       
        System.out.println(sql);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL. execSQL(sql);
        System.out.println(tSSRS.getMaxRow());
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            taskList.add(tSSRS.GetText(i, 1));
        }
        return taskList;
    }
    /**
     * 截取有效机构编码
     *
     */
     private String subComCode(String managecom)
     {
     	 int len = managecom.length();
     	 for(;len>1;len--)
     	 {
                  if(managecom.charAt(len-1)!='0')
                  {
                      break;
                  }
     	 }
         return managecom.substring(0,len);
     }

    /**
     * 理赔结案，工单也要结案
     * @param edorAcceptNo String
     * @return boolean
     */
    private boolean finish(String PrepaidNo)
    {
        //得到经办人和机构
        getOperatorInfo(PrepaidNo);
        LLClaimFinishBL tLLClaimFinishBL = new LLClaimFinishBL(mGlobalInput, PrepaidNo);
        if(!tLLClaimFinishBL.submitData())
        {
            mErrors.addOneError("理赔确认未通过！原因是：" + tLLClaimFinishBL.mErrors.getFirstError());
            return false;
        }
        return true;
    }
   
    /**
     * 得到操作人员信息
     * @param edorAcceptNo String
     */
    private void getOperatorInfo(String aPrepaidNo)
    {
        String sql = "select a.Register, a.ManageCom " +
                "from LLPrepaidClaim a, LDUser b " +
                "where a.Register = b.UserCode " +
                "and a.PrepaidNo = '" + aPrepaidNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            System.out.println("未找到理赔申请人信息!");
            return;
        }
        mGlobalInput.Operator = tSSRS.GetText(1, 1);
        mGlobalInput.ManageCom = tSSRS.GetText(1, 2);
        mGlobalInput.ComCode = mGlobalInput.ManageCom;
    }  

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 主函数，测试用
     * @param arg String[]
     */
    public static void main(String arg[])
    {
        LLClaimFinishTask tLLClaimFinishTask = new LLClaimFinishTask("8611");
        tLLClaimFinishTask.run();
    }
}
