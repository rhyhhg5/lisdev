package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.sinosoft.lis.f1print.Readhtml;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.SysMaxNoPicch;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.pubfun.ZipAndUnzip;
import com.sinosoft.lis.schema.LCFileManageSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgResHead;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

/**
 * 通过邮件自动发送一带一路承保及保全清单功能，并将扫描件传到邮件附件中。
 * 
 * @author CLH
 * @since 2018-05
 *
 */
public class LCGrpContRoadTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	protected MsgCollection mResMsgCols = null;
	protected Logger mLogger = Logger.getLogger(getClass().getName());

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	private int STATE;
	private String str = "";
	private FTPTool tFTPTool;
	private String tFileName;
	private String[][] data1;
	private String serverpath;
	public int x = 1;
	private String outputFileName = "";
	private String location = "";
	private String rootPath = "";
	private String[] inputEntry1;
	private String[] inputEntry2;
	private Readhtml rh = new Readhtml();

	/** 输入数据的容器 */

	// 执行任务
	public void run() {
		getUser();
	}

	// 提取用户
	public boolean getUser() {

		//设置文档在外测的存放目录
		String scanSQL = "";
		ExeSQL tEx = new ExeSQL();
		String tSQL = "select sysvarvalue from ldsysvar where sysvar = 'QYLoadPath'";
		SSRS tSSRS = new ExeSQL().execSQL(tSQL);
		location = tSSRS.GetText(1, 1)+PubFun.getCurrentDate()+"/";
//		location = "D:/jske/jjjs/";
		System.out.println(location);
		File fileTest = new File(location);
		fileTest.mkdirs();
		//查询扫描件的根目录
		rootPath = "select sysvarvalue from ldsysvar where sysvar = 'ServerRoot'";
		tSSRS = new ExeSQL().execSQL(rootPath);
		rootPath = tSSRS.GetText(1, 1);
		
		// 第一个sheet
		String tDisDetailAutoSql = "select "
				+ " distinct lcg.signdate,"
				+ " '新契约',"
				+ " '新契约',"
				+ " '',"
				+ " left(lcg.managecom, 4),"
				+ " (select name"
				+ " from ldcom"
				+ " where comcode ="
				+ " left(lcg.managecom, 4)),"
				+ " '',"
				+ " lcg.grpcontno,"
				+ " lcg.signdate,"
				+ " lcg.cvalidate,"
				+ " lcg.cinvalidate,"
				+ " lcg.grpname,"
				+ " '承保有效',"
				+ " lcp.contno,"
				+ " lcp.signdate,"
				+ " lcp.cvalidate,"
				+ " (select lcc.cinvalidate"
				+ " from lccont lcc"
				+ " where grpcontno = lcg.grpcontno"
				+ " and contno = lcp.contno"
				+ " and conttype = '2'"
				+ " and appflag = '1'),"
				+ " lcp.riskcode,"
				+ " (select name"
				+ " from lcinsured"
				+ " where grpcontno = lcg.grpcontno"
				+ " and contno = lcp.contno"
				+ " and RelationToMainInsured = '00'),"
				+ " lci.Name,"
				+ " db2inst1.codename('relation', lci.RelationToMainInsured),"
				+ " db2inst1.codename('idtype', lci.IDType),"
				+ " lci.IDNo,"
				+ " db2inst1.codename('sex', lci.sex),"
				+ " lci.Birthday,"
				+ " lcg.Remark,"
				+ " lcp.contplancode,"
				+ " (select country from db2inst1.lcgrpcontroad where prtno=lcg.prtno),"
				+ " (select case when EngineeringFlag='1' then '是' else '否' end from db2inst1.lcgrpcontroad where prtno=lcg.prtno),"
				+ " (select EngineeringCategory from db2inst1.lcgrpcontroad where prtno=lcg.prtno)"
				+ " from lcgrpcont lcg, lcpol lcp, lcinsured lci"
				+ " where 1 = 1"
				+ " and lci.contplancode = lcp.contplancode"
				+ " and lcg.grpcontno = lcp.grpcontno"
				+ " and lci.grpcontno = lcg.grpcontno"
				+ " and lci.contno = lcp.contno"
				+ " and lcp.conttype = '2' and lcp.appflag = '1'"
				+ " and lcg.appflag = '1' and lcg.stateflag = '1'"
				+ " and lcg.signdate = '"+PubFun.getLastDate(PubFun.getCurrentDate(), -1, "D")+"'"
				+ " and lcp.riskcode in ('163001', '561301','163002','561302') with ur";

		SSRS tDisAutoLDUsers = tEx.execSQL(tDisDetailAutoSql);
		int tDisUserAutoCount = tDisAutoLDUsers.getMaxRow();// 行数

		String mCurDate = PubFun.getCurrentDate();
		String mCurTime = PubFun.getCurrentTime();
		System.out.println(mCurDate);
		System.out.println(mCurTime);

		// 创建对象
		String[][] tToExcel = new String[tDisUserAutoCount + 10][30];
		System.out.println("统计时间" + mCurDate);
		tToExcel[0][0] = "契约承保操作完成日期";
		tToExcel[0][1] = "操作类型";
		tToExcel[0][2] = "操作项目";
		tToExcel[0][3] = "操作作业号";
		tToExcel[0][4] = "承保机构";
		tToExcel[0][5] = "承保机构名称";
		tToExcel[0][6] = "项目名称";
		tToExcel[0][7] = "保单号";
		tToExcel[0][8] = "总单承保日期";
		tToExcel[0][9] = "总单生效日期";
		tToExcel[0][10] = "总单终止日期";
		tToExcel[0][11] = "投保单位名称";
		tToExcel[0][12] = "分单状态";
		tToExcel[0][13] = "分单号";
		tToExcel[0][14] = "分单承保日期";
		tToExcel[0][15] = "分单生效日期";
		tToExcel[0][16] = "分单终止日期";
		tToExcel[0][17] = "产品代码";
		tToExcel[0][18] = "主被保险人姓名";
		tToExcel[0][19] = "被保险人姓名";
		tToExcel[0][20] = "与主被保险人关系";
		tToExcel[0][21] = "证件类型";
		tToExcel[0][22] = "证件号码";
		tToExcel[0][23] = "性别";
		tToExcel[0][24] = "生日";
		tToExcel[0][25] = "总单特别约定";
		tToExcel[0][26] = "保障计划类型";
		tToExcel[0][27] = "前往国家/地区";
		tToExcel[0][28] = "是否属于建筑工程";
		tToExcel[0][29] = "工程分类";

		Set<String> contnoset = new HashSet<String>();

		int excelRow = 0;

		for (int row = 1; row <= tDisUserAutoCount; row++) {
			excelRow++;
			tToExcel[excelRow][0] = tDisAutoLDUsers.GetText(row, 1);
			tToExcel[excelRow][1] = tDisAutoLDUsers.GetText(row, 2);
			tToExcel[excelRow][2] = tDisAutoLDUsers.GetText(row, 3);
			tToExcel[excelRow][3] = tDisAutoLDUsers.GetText(row, 4);
			tToExcel[excelRow][4] = tDisAutoLDUsers.GetText(row, 5);
			tToExcel[excelRow][5] = tDisAutoLDUsers.GetText(row, 6);
			tToExcel[excelRow][6] = tDisAutoLDUsers.GetText(row, 7);
			tToExcel[excelRow][7] = tDisAutoLDUsers.GetText(row, 8);
			tToExcel[excelRow][8] = tDisAutoLDUsers.GetText(row, 9);
			tToExcel[excelRow][9] = tDisAutoLDUsers.GetText(row, 10);
			tToExcel[excelRow][10] = tDisAutoLDUsers.GetText(row, 11);
			tToExcel[excelRow][11] = tDisAutoLDUsers.GetText(row, 12);
			tToExcel[excelRow][12] = tDisAutoLDUsers.GetText(row, 13);
			tToExcel[excelRow][13] = tDisAutoLDUsers.GetText(row, 14);
			tToExcel[excelRow][14] = tDisAutoLDUsers.GetText(row, 15);
			tToExcel[excelRow][15] = tDisAutoLDUsers.GetText(row, 16);
			tToExcel[excelRow][16] = tDisAutoLDUsers.GetText(row, 17);
			tToExcel[excelRow][17] = tDisAutoLDUsers.GetText(row, 18);
			tToExcel[excelRow][18] = tDisAutoLDUsers.GetText(row, 19);
			tToExcel[excelRow][19] = tDisAutoLDUsers.GetText(row, 20);
			tToExcel[excelRow][20] = tDisAutoLDUsers.GetText(row, 21);
			tToExcel[excelRow][21] = tDisAutoLDUsers.GetText(row, 22);
			tToExcel[excelRow][22] = tDisAutoLDUsers.GetText(row, 23);
			tToExcel[excelRow][23] = tDisAutoLDUsers.GetText(row, 24);
			tToExcel[excelRow][24] = tDisAutoLDUsers.GetText(row, 25);
			tToExcel[excelRow][25] = tDisAutoLDUsers.GetText(row, 26);
			tToExcel[excelRow][26] = tDisAutoLDUsers.GetText(row, 27);
			tToExcel[excelRow][27] = tDisAutoLDUsers.GetText(row, 28);
			tToExcel[excelRow][28] = tDisAutoLDUsers.GetText(row, 29);
			tToExcel[excelRow][29] = tDisAutoLDUsers.GetText(row, 30);

			contnoset.add(tToExcel[excelRow][7]);
		}

		SSRS tSSRS1 = null;
		///wasfs/wasappserv/profiles/AppSrv01/installedApps/p55a1Node01Cell/lis_war.ear/taobaoPort.war/
		///wasfs/lis_war/
		inputEntry2 = new String[contnoset.size()];
		for (String str1 : contnoset) {
			System.out.println(str1);
			scanSQL = "select picpath,pagename from es_doc_pages where docid in (select docid from es_doc_main "
//					+ "where doccode in (select prtno from lccont where grpcontno='00000686000002"
					 +"where doccode in (select prtno from lccont where grpcontno='"+str1
					+ "' and appflag='1'))";
			str = str1;
			tSSRS1 = new ExeSQL().execSQL(scanSQL);
			if (tSSRS1.getMaxRow() <= 0) {
//				errLog("团单号为" + str + "的保单查询出错！");
				continue;
			}
			// TransferData transferData1 = new TransferData();
			data1 = tSSRS1.getAllData();
			if (!getFile()) {
				errLog(mErrors.getFirstError());
				return false;
			}
		}
//		zipp = new ZipAndUnzip();
		if(contnoset.size()>0){
			outputFileName = location + PubFun.getCurrentDate() + ".zip";
			rh.CreateZipFile(inputEntry2, outputFileName);
		}
		String replace = location + PubFun.getCurrentDate() + ".zip";
//		outputFileName = "D:\\tempPath\\扫描件"+PubFun.getCurrentDate()+".zip";
//		zipp.zip(zinputFileList, zzipFileList, outputFileName);
		// 第二个sheet
		String tDisDetailAutoSql1 = "select"
				+ " distinct lpp.confdate,"
				+ " '保全',"
				+ " 'NI',"
				+ " lpg.edorno,"
				+ " left(lcg.managecom, 4),"
				+ " (select name"
				+ " from ldcom"
				+ " where comcode ="
				+ " left(lcg.managecom, 4)),"
				+ " '增加被保人',"
				+ " lcg.grpcontno,"
				+ " lcg.signdate,"
				+ " lcg.cvalidate,"
				+ " lcg.cinvalidate,"
				+ " lcg.grpname,"
				+ " (select codename from ldcode where codetype = 'stateflag' and code =lcp.stateflag) , "
				+ " lcp.contno,"
				+ " lcp.signdate,"
				+ " lcp.cvalidate,"
				+ " (select lcc.cinvalidate"
				+ " from lccont lcc"
				+ " where grpcontno = lcg.grpcontno"
				+ " and contno = lcp.contno"
				+ " and conttype = '2'"
				+ " and appflag = '1'),"
				+ " lcp.riskcode,"
				+ " (select name"
				+ " from lcinsured"
				+ " where grpcontno = lcg.grpcontno"
				+ " and contno = lcp.contno"
				+ " and RelationToMainInsured = '00'),"
				+ " lci.Name,"
				+ " db2inst1.codename('relation', lci.RelationToMainInsured),"
				+ " db2inst1.codename('idtype', lci.IDType),"
				+ " lci.IDNo,"
				+ " db2inst1.codename('sex', lci.sex),"
				+ " lci.Birthday,"
				+ " lcg.Remark,"
				+ " lcp.contplancode,"
				+ " (select country from db2inst1.lcgrpcontroad where prtno=lcg.prtno),"
				+ " (select case when EngineeringFlag='1' then '是' else '否' end from db2inst1.lcgrpcontroad where prtno=lcg.prtno),"
				+ " (select EngineeringCategory from db2inst1.lcgrpcontroad where prtno=lcg.prtno)"
				+ " from lcgrpcont lcg, lcpol lcp, lcinsured lci,lpgrpedoritem lpg,lpedorapp lpp"
				+ " where 1 = 1"
				+ " and lci.contplancode = lcp.contplancode"
				+ " and lcg.grpcontno = lcp.grpcontno"
				+ " and lci.grpcontno = lcg.grpcontno"
				+ " and lci.contno = lcp.contno"
				+ "	and lpg.grpcontno=lcg.grpcontno"
				+ " and lpg.edorno=lpp.edoracceptno"
				+ " and lcp.insuredno=lci.insuredno"
				+ " and lpp.edorstate='0'"
				+ " and lcp.conttype = '2'"
				+ " and lcp.appflag = '1'"
				+ " and lcg.appflag = '1'"
				+ " and lpp.confdate = '"+PubFun.getLastDate(PubFun.getCurrentDate(), -1, "D")+"'"
				+ " and exists (select 1 from ljagetendorse where contno=lcp.contno and feeoperationtype='NI' and endorsementno=lpg.edorno) "
				+ " and lcp.riskcode in ('163001', '561301','163002','561302')"
				+ " union" // 减连带被保人
				+ " select"
				+ " distinct lpp.confdate,"
				+ "'保全',"
				+ " 'ZT',"
				+ " lpg.edorno,"
				+ " left(lcg.managecom, 4),"
				+ " (select name"
				+ " from ldcom"
				+ " where comcode ="
				+ " left(lcg.managecom, 4)),"
				+ " '减少被保人',"
				+ " lcg.grpcontno,"
				+ " lcg.signdate,"
				+ " lcg.cvalidate,"
				+ " lcg.cinvalidate,"
				+ " lcg.grpname,"
				+ " (select codename from ldcode where codetype = 'stateflag' and code =lcp.stateflag) ,"
				+ " lcp.contno,"
				+ " lcp.signdate,"
				+ " lcp.cvalidate,"
				+ " (select lcc.cinvalidate"
				+ " from lccont lcc"
				+ " where grpcontno = lcg.grpcontno"
				+ " and contno = lcp.contno"
				+ " and conttype = '2'"
				+ " and appflag = '1'),"
				+ " lcp.riskcode,"
				+ " (select name"
				+ " from lcinsured"
				+ " where grpcontno = lcg.grpcontno"
				+ " and contno = lcp.contno"
				+ " and RelationToMainInsured = '00'),"
				+ " lci.Name,"
				+ " db2inst1.codename('relation', lci.RelationToMainInsured),"
				+ " db2inst1.codename('idtype', lci.IDType),"
				+ " lci.IDNo,"
				+ " db2inst1.codename('sex', lci.sex),"
				+ " lci.Birthday,"
				+ " lcg.Remark,"
				+ " lcp.contplancode,"
				+ " (select country from db2inst1.lcgrpcontroad where prtno=lcg.prtno),"
				+ " (select case when EngineeringFlag='1' then '是' else '否' end from db2inst1.lcgrpcontroad where prtno=lcg.prtno),"
				+ " (select EngineeringCategory from db2inst1.lcgrpcontroad where prtno=lcg.prtno)"
				+ " from lcgrpcont lcg, lcpol lcp, lbinsured lci,lpgrpedoritem lpg,lpedorapp lpp"
				+ " where 1 = 1"
				+ " and lci.contplancode = lcp.contplancode"
				+ " and lcg.grpcontno = lcp.grpcontno"
				+ " and lci.grpcontno = lcg.grpcontno"
				+ " and lci.contno = lcp.contno"
				+ "	and lpg.grpcontno=lcg.grpcontno"
				+ " and lcp.insuredno=lci.insuredno"
				+ " and lpg.edorno=lpp.edoracceptno"
				+ " and lpp.edorstate='0'"
				+ " and lcp.conttype = '2'"
				+ " and lcp.appflag = '1'"
				+ " and lcg.appflag = '1'"
				+ " and lpp.confdate = '"+PubFun.getLastDate(PubFun.getCurrentDate(), -1, "D")+"'"
				+ " and ( exists (select 1 from ljagetendorse where contno=lcp.contno and feeoperationtype='ZT' and endorsementno=lpg.edorno) )"
				+ " and lcp.riskcode in ('163001', '561301','163002','561302') "
				+ " union all"
				+ " select "
				+ " distinct lpp.confdate,"
				+ " '保全',"
				+ " 'ZT',"
				+ " lpg.edorno,"
				+ " left(lcg.managecom, 4),"
				+ " (select name"
				+ " from ldcom"
				+ " where comcode ="
				+ " left(lcg.managecom, 4)),"
				+ " '减少被保人',"
				+ " lcg.grpcontno,"
				+ " lcg.signdate,"
				+ " lcg.cvalidate,"
				+ " lcg.cinvalidate,"
				+ " lcg.grpname,"
				+ " (select codename from ldcode where codetype = 'stateflag' and code =lcp.stateflag) ,"
				+ " lcp.contno,"
				+ " lcp.signdate,"
				+ " lcp.cvalidate,"
				+ " (select lcc.cinvalidate"
				+ " from lbcont lcc "
				+ " where grpcontno = lcg.grpcontno"
				+ " and contno = lcp.contno"
				+ " and conttype = '2'"
				+ " and appflag = '1'),"
				+ " lcp.riskcode,"
				+ " (select name"
				+ " from lbinsured"
				+ " where grpcontno = lcg.grpcontno"
				+ " and contno = lcp.contno"
				+ " and RelationToMainInsured = '00'),"
				+ " lci.Name,"
				+ " db2inst1.codename('relation', lci.RelationToMainInsured),"
				+ " db2inst1.codename('idtype', lci.IDType),"
				+ " lci.IDNo,"
				+ " db2inst1.codename('sex', lci.sex),"
				+ " lci.Birthday,"
				+ " lcg.Remark,"
				+ " lcp.contplancode,"
				+ " (select country from db2inst1.lcgrpcontroad where prtno=lcg.prtno),"
				+ " (select case when EngineeringFlag='1' then '是' else '否' end from db2inst1.lcgrpcontroad where prtno=lcg.prtno), "
				+ " (select EngineeringCategory from db2inst1.lcgrpcontroad where prtno=lcg.prtno)"
				+ " from lcgrpcont lcg, lbpol lcp, lbinsured lci,lpgrpedoritem lpg,lpedorapp lpp"
				+ " where 1 = 1"
				+ " and lci.contplancode = lcp.contplancode"
				+ " and lcg.grpcontno = lcp.grpcontno"
				+ " and lci.grpcontno = lcg.grpcontno"
				+ " and lci.contno = lcp.contno"
				+ "	and lpg.grpcontno=lcg.grpcontno"
				+ " and lcp.insuredno=lci.insuredno"
				+ " and lpg.edorno=lpp.edoracceptno"
				+ " and lpp.edorstate='0'"
				+ " and lcp.conttype = '2'"
				+ " and lcp.appflag = '1'"
				+ " and lcg.appflag = '1'"
				+ " and lpp.confdate = '"+PubFun.getLastDate(PubFun.getCurrentDate(), -1, "D")+"'"
				+ " and exists (select 1 from ljagetendorse where contno=lcp.contno and feeoperationtype='ZT' and endorsementno=lpg.edorno)"
				+ " and lcp.riskcode in ('163001', '561301','163002','561302')"
				+ " union"
				+ " select "
				+ " distinct lpp.confdate,"
				+ " '保全',"
				+ " lpg.edortype,"
				+ " lpg.edorno,"
				+ " left(lcg.managecom, 4),"
				+ " (select name"
				+ " from ldcom"
				+ " where comcode ="
				+ " left(lcg.managecom, 4)),"
				+ " (select edorname from lmedoritem where edorcode=lpg.edortype and appobj='G'),"
				+ " lcg.grpcontno,"
				+ " lcg.signdate,"
				+ " lcg.cvalidate,"
				+ " lcg.cinvalidate,"
				+ " lcg.grpname,"
				+ " (select codename from ldcode where codetype = 'stateflag' and code =lcp.stateflag) ,"
				+ " lcp.contno,"
				+ " lcp.signdate,"
				+ " lcp.cvalidate,"
				+ " (select lcc.cinvalidate"
				+ " from lbcont lcc"
				+ " where grpcontno = lcg.grpcontno"
				+ " and contno = lcp.contno"
				+ " and conttype = '2'"
				+ " and appflag = '1'),"
				+ " lcp.riskcode,"
				+ " (select name"
				+ " from lbinsured"
				+ " where grpcontno = lcg.grpcontno"
				+ " and contno = lcp.contno"
				+ " and RelationToMainInsured = '00'),"
				+ " lci.Name,"
				+ " db2inst1.codename('relation', lci.RelationToMainInsured),"
				+ " db2inst1.codename('idtype', lci.IDType),"
				+ " lci.IDNo,"
				+ " db2inst1.codename('sex', lci.sex),"
				+ " lci.Birthday,"
				+ " lcg.Remark,"
				+ " lcp.contplancode,"
				+ " (select country from db2inst1.lcgrpcontroad where prtno=lcg.prtno),"
				+ " (select case when EngineeringFlag='1' then '是' else '否' end from db2inst1.lcgrpcontroad where prtno=lcg.prtno),"
				+ " (select EngineeringCategory from db2inst1.lcgrpcontroad where prtno=lcg.prtno)"
				+ " from lbgrpcont lcg, lbpol lcp, lbinsured lci,lpgrpedoritem lpg,lpedorapp lpp"
				+ " where 1 = 1"
				+ " and lci.contplancode = lcp.contplancode"
				+ " and lcg.grpcontno = lcp.grpcontno"
				+ " and lci.grpcontno = lcg.grpcontno"
				+ " and lci.contno = lcp.contno"
				+ " and lcp.insuredno=lci.insuredno"
				+ "	and lpg.grpcontno=lcg.grpcontno"
				+ " and lpg.edorno=lpp.edoracceptno"
				+ " and lpp.edorstate='0'"
				+ " and lcp.conttype = '2'"
				+ " and lpp.confdate = '"+PubFun.getLastDate(PubFun.getCurrentDate(), -1, "D")+"'"
				+ " and lpg.edortype in ('CT','XT','WT','NI','ZT')"
				+ " and lcp.riskcode in ('163001', '561301','163002','561302')"
				+ " with ur";
		SSRS tDisAutoLDUsers1 = tEx.execSQL(tDisDetailAutoSql1);
		int tDisUserAutoCount1 = tDisAutoLDUsers1.getMaxRow();// 行数

		/**
		 * 当查不到数据时，STATE为1，不发送附件。
		 */
		if (tDisUserAutoCount == 0 && tDisUserAutoCount1 == 0) {
			STATE = 1;
		}
		if (STATE != 1) {
			// 创建对象
			String[][] tToExcel1 = new String[tDisUserAutoCount1 + 10][30];
			System.out.println("统计时间1" + mCurDate);
			tToExcel1[0][0] = "保全确认日期";
			tToExcel1[0][1] = "操作类型";
			tToExcel1[0][2] = "操作项目";
			tToExcel1[0][3] = "操作作业号";
			tToExcel1[0][4] = "承保机构代码";
			tToExcel1[0][5] = "承保机构名称";
			tToExcel1[0][6] = "项目名称";
			tToExcel1[0][7] = "保单号";
			tToExcel1[0][8] = "总单承保日期";
			tToExcel1[0][9] = "总单生效日期";
			tToExcel1[0][10] = "总单终止日期";
			tToExcel1[0][11] = "投保单位名称";
			tToExcel1[0][12] = "分单状态";
			tToExcel1[0][13] = "分单号";
			tToExcel1[0][14] = "分单承保日期";
			tToExcel1[0][15] = "分单生效日期";
			tToExcel1[0][16] = "分单终止日期";
			tToExcel1[0][17] = "产品代码";
			tToExcel1[0][18] = "主被保险人姓名";
			tToExcel1[0][19] = "被保险人姓名";
			tToExcel1[0][20] = "与主被保险人关系";
			tToExcel1[0][21] = "证件类型";
			tToExcel1[0][22] = "证件号码";
			tToExcel1[0][23] = "性别";
			tToExcel1[0][24] = "生日";
			tToExcel1[0][25] = "总单特别约定";
			tToExcel1[0][26] = "保障计划类型";
			tToExcel1[0][27] = "前往国家/地区";
			tToExcel1[0][28] = "是否属于建筑工程";
			tToExcel1[0][29] = "工程分类";

			int excelRow1 = 0;

			for (int row = 1; row <= tDisUserAutoCount1; row++) {
				excelRow1++;
				tToExcel1[excelRow1][0] = tDisAutoLDUsers1.GetText(row, 1);
				tToExcel1[excelRow1][1] = tDisAutoLDUsers1.GetText(row, 2);
				tToExcel1[excelRow1][2] = tDisAutoLDUsers1.GetText(row, 3);
				tToExcel1[excelRow1][3] = tDisAutoLDUsers1.GetText(row, 4);
				tToExcel1[excelRow1][4] = tDisAutoLDUsers1.GetText(row, 5);
				tToExcel1[excelRow1][5] = tDisAutoLDUsers1.GetText(row, 6);
				tToExcel1[excelRow1][6] = tDisAutoLDUsers1.GetText(row, 7);
				tToExcel1[excelRow1][7] = tDisAutoLDUsers1.GetText(row, 8);
				tToExcel1[excelRow1][8] = tDisAutoLDUsers1.GetText(row, 9);
				tToExcel1[excelRow1][9] = tDisAutoLDUsers1.GetText(row, 10);
				tToExcel1[excelRow1][10] = tDisAutoLDUsers1.GetText(row, 11);
				tToExcel1[excelRow1][11] = tDisAutoLDUsers1.GetText(row, 12);
				tToExcel1[excelRow1][12] = tDisAutoLDUsers1.GetText(row, 13);
				tToExcel1[excelRow1][13] = tDisAutoLDUsers1.GetText(row, 14);
				tToExcel1[excelRow1][14] = tDisAutoLDUsers1.GetText(row, 15);
				tToExcel1[excelRow1][15] = tDisAutoLDUsers1.GetText(row, 16);
				tToExcel1[excelRow1][16] = tDisAutoLDUsers1.GetText(row, 17);
				tToExcel1[excelRow1][17] = tDisAutoLDUsers1.GetText(row, 18);
				tToExcel1[excelRow1][18] = tDisAutoLDUsers1.GetText(row, 19);
				tToExcel1[excelRow1][19] = tDisAutoLDUsers1.GetText(row, 20);
				tToExcel1[excelRow1][20] = tDisAutoLDUsers1.GetText(row, 21);
				tToExcel1[excelRow1][21] = tDisAutoLDUsers1.GetText(row, 22);
				tToExcel1[excelRow1][22] = tDisAutoLDUsers1.GetText(row, 23);
				tToExcel1[excelRow1][23] = tDisAutoLDUsers1.GetText(row, 24);
				tToExcel1[excelRow1][24] = tDisAutoLDUsers1.GetText(row, 25);
				tToExcel1[excelRow1][25] = tDisAutoLDUsers1.GetText(row, 26);
				tToExcel1[excelRow1][26] = tDisAutoLDUsers1.GetText(row, 27);
				tToExcel1[excelRow1][27] = tDisAutoLDUsers1.GetText(row, 28);
				tToExcel1[excelRow1][28] = tDisAutoLDUsers1.GetText(row, 29);
				tToExcel1[excelRow1][29] = tDisAutoLDUsers1.GetText(row, 30);

				Set<String> edornoset = new HashSet<String>();
				edornoset.add(tToExcel1[excelRow1][3]);

			}

			try {
				String name = mCurDate + "-" + "YiDaiYiLu.xls";
				String tTime = mCurDate + "承保和保全清单";

				System.out.println("字符变更前名称：" + name);
				String tAllName = new String(
						(location + name).getBytes("GBK"), "iso-8859-1");
				System.out.println("文件名称====：" + tAllName);

				// 将生成的Excel信息存入LCFileManage表
				LCFileManageSchema tLCFileManageSchema = new LCFileManageSchema();
				// 引入流水号
				String fielNo = new SysMaxNoPicch().CreateMaxNo("FILENO", 10);
				tLCFileManageSchema.setFileNo(fielNo);

				tLCFileManageSchema.setFileCode("YiDaiYiLu");
				tLCFileManageSchema.setFileType("9");
				tLCFileManageSchema.setFileDetailType("9");
				tLCFileManageSchema.setDiscription("一带一路提数");
				tLCFileManageSchema.setManageCom("86");
				tLCFileManageSchema.setOperator("it001");
				tLCFileManageSchema.setOtherNo(null);
				tLCFileManageSchema.setOtherNoType(null);

				tLCFileManageSchema.setFileName(name);
				tLCFileManageSchema.setFilePath(location);
				tLCFileManageSchema.setMakeDate(mCurDate);
				tLCFileManageSchema.setModifyDate(mCurDate);
				tLCFileManageSchema.setMakeTime(mCurTime);
				tLCFileManageSchema.setModifyTime(mCurTime);

				// 准备传输数据
				MMap map = new MMap();
				map.put(tLCFileManageSchema, "INSERT");
				VData mVData = new VData();
				mVData.add(map);
				PubSubmit ps = new PubSubmit();
				if (!ps.submitData(mVData, null)) {
					this.mErrors.copyAllErrors(ps.mErrors);
					return false;
				}

				String sheetname1 = "契约数据报表";
				String sheetname2 = "保全数据报表";

				WriteToExcel t = new WriteToExcel(name);
				t.createExcelFile();
				t.setAlignment("CENTER");
				t.setFontName("宋体");

				String[] sheetName = { sheetname1, sheetname2 };
				t.addSheetGBK(sheetName);

				t.setData(0, tToExcel);
				t.setData(1, tToExcel1);
				t.write(location);
				System.out.println("生成文件完成");

				// 获取文件设置格式
				FileInputStream is = new FileInputStream(new File(tAllName)); // 获取文件
				HSSFWorkbook wb = new HSSFWorkbook(is);
				HSSFSheet sheet = wb.getSheetAt(0); // 获取sheet
				HSSFSheet sheet1 = wb.getSheetAt(1); // 获取sheet
				is.close();

				// 设置字体-内容
				HSSFFont tFont = wb.createFont();
				tFont.setFontName("宋体");
				tFont.setFontHeightInPoints((short) 10);// 设置字体大小

				// 正文样式
				HSSFCellStyle tContentStyle = wb.createCellStyle(); // 获取样式
				tContentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
				tContentStyle.setFont(tFont);
				tContentStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				tContentStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				tContentStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
				tContentStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

				// 设置sheet行高 列宽
				for (int i = 0; i < tDisUserAutoCount + 2; i++) {
					HSSFRow row = sheet.getRow(i);
					for (int j = 0; j < 30; j++) {
						HSSFCell firstCell = row.getCell((short) j);
						sheet.setColumnWidth((short) j, (short) 6000);
						firstCell.setCellStyle(tContentStyle);
					}

				}
				// 设置sheet1行高 列宽
				for (int i = 0; i < tDisUserAutoCount1 + 2; i++) {
					HSSFRow row = sheet1.getRow(i);
					for (int j = 0; j < 30; j++) {
						HSSFCell firstCell = row.getCell((short) j);
						sheet1.setColumnWidth((short) j, (short) 6000);
						firstCell.setCellStyle(tContentStyle);
					}
				}

				FileOutputStream fileOut = new FileOutputStream(tAllName);
				System.out.println("aaa：" + fileOut);
				wb.write(fileOut);

				File file1 = new File(tAllName);
				System.out.print(file1.length());
				File file2 = new File(replace);
				System.out.print(file2.length());
				long fileSize = file1.length() + file2.length();
				
				String tSQLNumSql = "select codename,codealias from ldcode where codetype = 'adjustablenumber' ";
				SSRS tSSRS5 = new ExeSQL().execSQL(tSQLNumSql);
				int maxNum = Integer.parseInt(tSSRS5.GetText(1, 1));
				if (fileSize >= maxNum) {
					STATE = 3;
				} else if(contnoset.size()>0){
					STATE = 2;
				} else{
					STATE = 4;//只有保全
				}
				maxNum = Integer.parseInt(tSSRS5.GetText(1,2));
				// replace
				if(STATE == 2){
					sendMail("附件是"+tTime, tAllName + "|" + replace);
				}else if(STATE == 4){
					sendMail("附件是"+tTime, tAllName);
				}else{
					String path = location + PubFun.getCurrentDate() + "split.";
					ZipFile zipFile = new ZipFile(path+"zip");
					ArrayList<File> filesToAdd = new ArrayList<File>(); 
					File fffile = new File(replace);
					filesToAdd.add(fffile);
					ZipParameters parameters = new ZipParameters();
					parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
					parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
					zipFile.createZipFile(filesToAdd, parameters, true, maxNum);//14680064
					int n = 1;
					File split;
					sendMail("附件是"+tTime+"，由于附件过大，采用分卷压缩的方法，此为第1封邮件，请勿遗漏！", tAllName + "|" + path+"zip");
					split = new File(path+"zip");
					//发完就删，解决存在重复文件时的报错问题。
					split.delete();
					/**
					 * 根据实际情况，10个压缩包基本能够满足要求，所以排除n>=10的情况
					 */
					while(true){
						split = new File(path+"z0"+ n);
						if(split.exists()){
							try{Thread.sleep(30000);}catch(Exception e){}
							sendMail("附件是"+tTime+"，由于附件过大，采用分卷压缩的方法，此为第"+(n+1)+"封邮件，请勿遗漏！", tAllName + "|" + split);
							n++;
							split.delete();
						}else{
							break;
						}
					}
				}

			} catch (Exception ex) {
				this.mErrors.addOneError(new CError("每月用户日志审核时出错",
						"UsersreviewTask", "run"));
				ex.toString();
				ex.printStackTrace();
				return false;
			}
		}else{
			sendMail("今日无契约/保全!","");
		}
		return true;

	}

	/**
	 * 发送邮件
	 * @param tTime 邮件内容
	 * @param tAllName 邮件附件(地址)
	 */
	public void sendMail(String tTime, String tAllName) {
		// 邮箱发送方
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
		SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1),
				tSSRS.GetText(1, 2), "picchealth");

		// 标题，内容，附件(多附件可以用|分隔，末尾|可加可不加)
		tMailSender.setSendInf("承保和保全清单", "您好：\r\n" + tTime,
				tAllName);
		// 收件方
		String tSQLRMailSql = "select codealias,codename from ldcode where codetype = 'roadmailreceiver'";
		SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
		String tSQLRMailSql2 = "select codealias,codename from ldcode where codetype = 'roadmailreceiver1'";
		SSRS tSSRS6 = new ExeSQL().execSQL(tSQLRMailSql2);
		tMailSender.setToAddress(tSSRS6.GetText(1, 1)+tSSRSR.GetText(1, 1),
				tSSRS6.GetText(1, 2)+tSSRSR.GetText(1, 2), null);
		// 发送邮件
		if (!tMailSender.sendMail()) {
			System.out.println(tMailSender.getErrorMessage());
		}
	}
	
	

	protected final void errLog(String cErrInfo) {
		mLogger.error("ErrInfo:" + cErrInfo);

		String tErrCode = "99999999";
		MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
		tResMsgHead.setState("01");
		tResMsgHead.setErrCode(tErrCode);
		tResMsgHead.setErrInfo(cErrInfo);
	}

	public boolean downloadScan() {

		if (!getFile()) {
			try {
				tFTPTool.logout();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}

		return true;
	}

	/**
	 * 从FTP服务器下载扫描件
	 * 
	 * @return boolean
	 */
	private boolean getFile() {
		boolean downflag = true;
		System.out.println(data1.length);
		inputEntry1 = new String[data1.length];
		for (int i = 0; i < data1.length; i++) {
			serverpath = rootPath + data1[i][0];
			tFileName = data1[i][1] + ".tif";
//			serverpath = "D:/tempPath/";
//			tFileName = "F19033969.tif";
			System.out.println(serverpath+tFileName);
			inputEntry1[i] = serverpath + tFileName;
			System.out.println("第" + x + "个团单号下的第" + (i+1) + "个扫描件添加成功");
		}	
		outputFileName = location + str + ".zip";
		try {
			outputFileName = new String(outputFileName.getBytes("GBK"),"iso-8859-1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("保单"+str+"的zip存放目录为"+outputFileName);
		rh.CreateZipFile(inputEntry1, outputFileName);
		System.out.println("第" + x + "个团单号下的所有扫描件压缩成功！");
		inputEntry2[x-1] = outputFileName;
		x++;
		if (!downflag) {
			CError tError = new CError();
			tError.moduleName = "LCGrpContRoadTask";
			tError.functionName = "getFile";
			tError.errorMessage = tFTPTool.mErrors.getFirstError();
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	// 主方法 测试用
	public static void main(String[] args) {
		new LCGrpContRoadTask().run();

	}
}
