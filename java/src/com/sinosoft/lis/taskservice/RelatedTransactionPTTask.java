package com.sinosoft.lis.taskservice;

import java.util.Calendar;

import com.sinosoft.lis.operfee.NewIndiDueFeeMultiUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.tb.RelatedTransactionPTUI;
import com.sinosoft.lis.tb.RelatedTransactionUI;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.yinbaotongbank.SendToBankAUI;
import com.sinosoft.lis.yinbaotongbank.SendToBankUI;
import com.sinosoft.lis.yinbaotongbank.YinBaoTongWriteToFileAUI;
import com.sinosoft.lis.yinbaotongbank.YinBaoTongWriteToFileUI;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class RelatedTransactionPTTask extends TaskThread {
	private String startDate = "";

	private String endDate = "";

	public void run() {
		System.out.println("---RelatedTransactionPTTask开始---");
		submitData();
		System.out.println("---RelatedTransactionPTTask结束---");
	}

	private boolean submitData() {
		if (!getInputData()) {
			return false;
		}
		System.out.println("---End getInputData---");

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");
		return true;
	}

	private boolean getInputData() {
		try {
			Calendar now = Calendar.getInstance();  
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH)+1;
			int day = now.get(Calendar.DAY_OF_MONTH);
			if(month%3!=1 || day !=9 ){
				System.out.println("今天不是批处理时间");
				return false;
			}
			int syear = 0;
			int smonth = 0;
			int sday = 0;
			int eyear = 0;
			int emonth = 0;
			int eday = 0;
			switch (month) {
			case 1:
				syear = year - 1 ;
				smonth = 10;
				sday = 1;
				eyear = year - 1 ;
				emonth = 12;
				eday = 31;
				break;
			case 4:
				syear = year;
				smonth = 1;
				sday = 1;
				eyear = year;
				emonth = 3;
				eday = 31;
				break;
			case 7:
				syear = year;
				smonth = 4;
				sday = 1;
				eyear = year;
				emonth = 6;
				eday = 30;
				break;
			case 10:
				syear = year;
				smonth = 7;
				sday = 1;
				eyear = year;
				emonth =9;
				eday = 30;
				break;
			}
			
			System.out.println(syear+"-"+smonth+"-"+sday);
			System.out.println(eyear+"-"+emonth+"-"+eday);
			startDate = syear+"-"+smonth+"-"+sday; // 打标签的开始日期
			endDate = eyear+"-"+emonth+"-"+eday; //打标签的结束日期
		} catch (Exception e) {
			// @@错误处理
			CError.buildErr(this, "接收数据失败");
			return false;
		}
		return true;
	}

	private boolean dealData() {
		VData tVData = new VData();
		GlobalInput tGlobalInput = new GlobalInput();
		tVData.add(tGlobalInput);
		tVData.add(startDate);
		tVData.add(endDate);
		RelatedTransactionPTUI tRelatedTransactionPTUI = new RelatedTransactionPTUI();
		if(!tRelatedTransactionPTUI.submitData(tVData)){
			System.out.println("处理失败，原因是:" + tRelatedTransactionPTUI.mErrors.getFirstError());
  		}else{
  			System.out.println("处理成功");
  		}
		return true;
	}

	public static void main(String[] args) {
		RelatedTransactionPTTask tRelatedTransactionPTTash = new RelatedTransactionPTTask();
		tRelatedTransactionPTTash.run();
	}
}
