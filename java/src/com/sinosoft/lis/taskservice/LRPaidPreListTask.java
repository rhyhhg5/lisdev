package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LRPaidCessListSchema;
import com.sinosoft.lis.vschema.LRPaidCessListSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 实收保费的批处理,目前只有一个合同SWR2014M04需要算实收保费
 * @author 杨阳 2014-8-12
 *	
 */
public class LRPaidPreListTask extends TaskThread {

	private ExeSQL mExeSQL = new ExeSQL();
	
    private PubFun mPubFun = new PubFun();
    
	private FDate mFDate = new FDate();
	
	public void run() {
		//得到再保日志表lrgetdatalog中已经提取完毕的日期
		String startDate = mExeSQL.getOneValue("select max(startdate) from LRGetDataLog where operator = 'CH' with ur");
		//得到已经重新计算过实收保费的日期
		String getDataDate = mExeSQL.getOneValue("select max(getdatadate)+1 days from db2inst1.LRPaidCessList with ur");
		//如果没有重新计算过的保费，则置时间为1900-1-1
		if("".equals(getDataDate)||null==getDataDate)
		{
			//再保中合同SWR2014M04最早出现分保的日期
			getDataDate = "2014-01-01";
		}
		Date maxDate = mFDate.getDate(startDate);
		Date minDate = mFDate .getDate(getDataDate);
		if(minDate.compareTo(maxDate) >  0)
		{
			System.out.println("没有要计算的实收保费数据！");
		}
		else 
		{
			while(minDate.compareTo(maxDate)<=  0)
			{
				getDataDate=mFDate.getString(minDate);
				
				getList(getDataDate,"SWR2014M04","合同分保");
				minDate=PubFun.calDate(minDate, 1, "D", null);
			}
//			String recontSql ="select code,	CODEALIAS from ldcode where codetype ='paidpre' with ur";
//			SSRS contSSRS = mExeSQL.execSQL(recontSql);
//			for(int i=1;i<=contSSRS.MaxRow;i++)
//			{
//				String recontcode = contSSRS.GetText(i, 1);
//				String tempCessFlag = contSSRS.GetText(i, 2);
				
//			}
		}
		System.out.println("LRPaidPreListTask实收保费计算完成！");
	}

	/**
	 * 对于在些区间内的数据进行实收保费计算
	 * @param startDate  
	 * @param getDataDate
	 * @return
	 */
	private boolean getList(String getDataDate,String recontcode,String tempCessFlag)
	{
		LRPaidCessListSet mLRPaidCessListSet  = new LRPaidCessListSet();
		String sql ="select distinct ManageCom 机构代码,      "
					+"       RiskCode 产品代码,        "
					+"       (case grpcontno when '00000000000000000000' then contno else grpcontno end) 保单号,         "
					+"       CValidate 生效日期,       "
					+"       enddate 失效日期,         "
					+"       EdorValidate 增减人日期,  "
					+"       SignDate 签单日期,        "
					+"       GetDataDate 清单时间      "
					+"  from lrcesslist where 1=1 " 
					+"  and recontcode = '"+recontcode+"'"
					+"  and getdatadate   ='"+getDataDate+"' with ur ";
		System.out.println(sql);
		SSRS ssrs = mExeSQL.execSQL(sql);
		System.out.println(ssrs.MaxRow);
		if(0<ssrs.MaxRow)
		{
			for(int i =1;i<=ssrs.getMaxRow();i++)
			{
				String managecom = ssrs.GetText(i, 1);
				String riskcode = ssrs.GetText(i, 2);
				String grpcontno = ssrs.GetText(i, 3);
				String cvalidate = ssrs.GetText(i, 4);
				String enddate = ssrs.GetText(i, 5);
				String edorvalidate = ssrs.GetText(i, 6);
				String getdatadate = ssrs.GetText(i, 8);
				String signdate = ssrs.GetText(i, 7);
				if("".equals(edorvalidate)||null ==edorvalidate)
				{
					edorvalidate = "1900-1-1";
				}
				String paidSql="";
				paidSql = "select sum(case finitemtype  when 'C' then  summoney else -summoney  end)" +
				" from fivoucherdatadetail where accountcode = '6031000000'" +
				" and checkflag = '00' " +
				" and certificateid not in (select distinct versionno from ficodetrans where codetype = 'ClassTypeNew' "+
				" and versionno like '%JT%')" +
				" and classtype in ('X-02','X-02-Y','X-03', 'X-03-Y','X-04', 'X-04-Y','N-03','N-03-Y','N-05', 'N-05-Y','B-02','B-02-Y','B-03', 'B-04','B-03-Y', 'B-04-Y') " +
				" and contno = '"+grpcontno+"' " +
				" and riskcode = '"+riskcode+"' " +
				" and accountdate = '"+getdatadate+"' " +
				"with ur";
				System.out.println(paidSql);
				//实收保费
				String premStr = mExeSQL.getOneValue(paidSql);
				if("".equals(premStr)||null==premStr)
				{
					continue;
				}
				double prem = Double.parseDouble(premStr);
				//分保比例,根据合同和险种从要素表中查询   目前只有一个合同，不用此方法
//			String cessionrateSql ="select factrovalue from lrcalfactorvalue where 1=1" +
//							 " and factorcode ='CessionRate'  " +
//							 " and recontcode ='"+recontcode+"' " +
//					 		 " and riskcode = '"+riskcode+"' with ur";
				//SWR2014M04合同的分保比例为0.5
				double cessionrate =0.5;
				//手续费率=再保手续费率+特殊佣金比例，根据合同和险种从要素表中查询 目前只有一个合同，不用此方法
//			String reinprocfeerateSql = "select sum(factorvalue)  from lrcalfactorvalue where 1=1" +
//									 " and recontcode ='"+recontcode+"'  " +
//									 " and riskcode ='"+riskcode+"' " +
//									 " and factorcode in ('ReinProcFeeRate','SpeComRate')  with ur;";
				//SWR2014M04合同的手续费率为0.22
				double reinprocfeerate =0.22;
				//实收保费的分出保费=实收保费*分保比例
				double cessprem = prem* cessionrate;
				//实收保费的手续费用= 实收保费的分出保费*手续费率
				double reprocfee  = cessprem * reinprocfeerate;
				
				LRPaidCessListSchema mLRPaidCessListSchema = new LRPaidCessListSchema();
				
				mLRPaidCessListSchema.setManageCom(managecom);
				mLRPaidCessListSchema.setRiskCode(riskcode);
				mLRPaidCessListSchema.setReContCode(recontcode);
				mLRPaidCessListSchema.setGrpContNo(grpcontno);
				mLRPaidCessListSchema.setCValiDate(cvalidate);
				mLRPaidCessListSchema.setEndDate(enddate);
				//如果没有增减人日期，则设置为默认值1900-01-01
				mLRPaidCessListSchema.setEdorValidate(edorvalidate);
				mLRPaidCessListSchema.setSignDate(signdate);
				//保单层的保额没有办法 设置，全写为0
				mLRPaidCessListSchema.setAmnt(0);
				mLRPaidCessListSchema.setPrem(prem);
				//目前只有SWR2014M04合同，属于合同分保
				mLRPaidCessListSchema.setTempCessFlag(tempCessFlag);
				mLRPaidCessListSchema.setCessionRate(cessionrate);
				mLRPaidCessListSchema.setCessPrem(cessprem);
				mLRPaidCessListSchema.setReProcFee(reprocfee);
				mLRPaidCessListSchema.setGetDataDate(getdatadate);
				mLRPaidCessListSchema.setOprater("Server");
				mLRPaidCessListSchema.setMakeDate(mPubFun.getCurrentDate());
				mLRPaidCessListSchema.setMakeTime(mPubFun.getCurrentTime());
				mLRPaidCessListSchema.setModifyDate(mPubFun.getCurrentDate());
				mLRPaidCessListSchema.setModifyTime(mPubFun.getCurrentTime());
				mLRPaidCessListSet.add(mLRPaidCessListSchema);
			}
		}
		sql ="select distinct managecom,riskcode,grpcontno,cvalidate,enddate from db2inst1.LRPaidCessList a where getdatadate <='"+getDataDate+"' and not exists(" 
		+" select 1  from lrcesslist where 1=1 " 
		+"  and recontcode = '"+recontcode+"'"
		+"  and getdatadate   ='"+getDataDate+"'" +
		" and a.riskcode = riskcode and " +
		"((grpcontno='00000000000000000000' and a.grpcontno =contno) or (grpcontno<>'00000000000000000000' and a.grpcontno =grpcontno))" 
		+")with ur";
		SSRS tSSRS = new SSRS();
		tSSRS = mExeSQL.execSQL(sql);
		String paidSql ="";
		if(0<tSSRS.MaxRow)
		{
			for(int i =1;i<=tSSRS.getMaxRow();i++)
			{
				System.out.println(i);
				String managecom = tSSRS.GetText(i, 1);
				String riskcode = tSSRS.GetText(i, 2);
				String grpcontno = tSSRS.GetText(i, 3);
				String cvalidate = tSSRS.GetText(i, 4);
				String enddate = tSSRS.GetText(i, 5);
				paidSql = "select sum(case finitemtype  when 'C' then  summoney else -summoney  end)" +
				" from fivoucherdatadetail where accountcode = '6031000000'" +
				" and checkflag = '00' " +
				" and certificateid not in (select distinct versionno from ficodetrans where codetype = 'ClassTypeNew' "+
				" and versionno like '%JT%')" +
				" and classtype in ('X-02','X-02-Y','X-03', 'X-03-Y','X-04', 'X-04-Y','N-03','N-03-Y','N-05', 'N-05-Y','B-02','B-02-Y','B-03', 'B-04','B-03-Y', 'B-04-Y') " +
				" and contno = '"+grpcontno+"' " +
				" and riskcode = '"+riskcode+"' " +
				" and accountdate = '"+getDataDate+"' " +
				"with ur";
				System.out.println(paidSql);
				//实收保费
				String premStr = mExeSQL.getOneValue(paidSql);
				if("".equals(premStr)||null==premStr)
				{
					continue;
				}
				double prem = Double.parseDouble(premStr);
				//分保比例,根据合同和险种从要素表中查询   目前只有一个合同，不用此方法
//		String cessionrateSql ="select factrovalue from lrcalfactorvalue where 1=1" +
//						 " and factorcode ='CessionRate'  " +
//						 " and recontcode ='"+recontcode+"' " +
//				 		 " and riskcode = '"+riskcode+"' with ur";
				//SWR2014M04合同的分保比例为0.5
				double cessionrate =0.5;
				//手续费率=再保手续费率+特殊佣金比例，根据合同和险种从要素表中查询 目前只有一个合同，不用此方法
//		String reinprocfeerateSql = "select sum(factorvalue)  from lrcalfactorvalue where 1=1" +
//								 " and recontcode ='"+recontcode+"'  " +
//								 " and riskcode ='"+riskcode+"' " +
//								 " and factorcode in ('ReinProcFeeRate','SpeComRate')  with ur;";
				//SWR2014M04合同的手续费率为0.22
				double reinprocfeerate =0.22;
				//实收保费的分出保费=实收保费*分保比例
				double cessprem = prem* cessionrate;
				//实收保费的手续费用= 实收保费的分出保费*手续费率
				double reprocfee  = cessprem * reinprocfeerate;
				
				LRPaidCessListSchema mLRPaidCessListSchema = new LRPaidCessListSchema();
				
				mLRPaidCessListSchema.setManageCom(managecom);
				mLRPaidCessListSchema.setRiskCode(riskcode);
				mLRPaidCessListSchema.setReContCode(recontcode);
				mLRPaidCessListSchema.setGrpContNo(grpcontno);
				mLRPaidCessListSchema.setCValiDate(cvalidate);
				mLRPaidCessListSchema.setEndDate(enddate);
				//如果没有增减人日期，则设置为默认值1900-01-01
				mLRPaidCessListSchema.setEdorValidate("1900-01-01");
				mLRPaidCessListSchema.setSignDate("1900-01-01");
				//保单层的保额没有办法 设置，全写为0
				mLRPaidCessListSchema.setAmnt(0);
				mLRPaidCessListSchema.setPrem(prem);
				//目前只有SWR2014M04合同，属于合同分保
				mLRPaidCessListSchema.setTempCessFlag(tempCessFlag);
				mLRPaidCessListSchema.setCessionRate(cessionrate);
				mLRPaidCessListSchema.setCessPrem(cessprem);
				mLRPaidCessListSchema.setReProcFee(reprocfee);
				mLRPaidCessListSchema.setGetDataDate(getDataDate);
				mLRPaidCessListSchema.setOprater("Server");
				mLRPaidCessListSchema.setMakeDate(mPubFun.getCurrentDate());
				mLRPaidCessListSchema.setMakeTime(mPubFun.getCurrentTime());
				mLRPaidCessListSchema.setModifyDate(mPubFun.getCurrentDate());
				mLRPaidCessListSchema.setModifyTime(mPubFun.getCurrentTime());
				mLRPaidCessListSet.add(mLRPaidCessListSchema);
			}
		}
		MMap mMap = new MMap();
		VData vData = new  VData();
		mMap.put(mLRPaidCessListSet, "INSERT");
		vData.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(vData, "")) 
		{
			System.out.print("批处理失败");
			return false;
		}
		System.out.print("批处理成功");
		return true;
	}
	public static void main(String[] args) {
		LRPaidPreListTask tLRPaidPreListTask = new LRPaidPreListTask();
		tLRPaidPreListTask.getList("2014-5-6", "SCR2014M01", "合同分保");
//		tLRPaidPreListTask.run();
	}
	
}
	