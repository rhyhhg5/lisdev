package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclientybk.inf.C01;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *   	医保卡理赔立案上报批处理
 * </p>

 * @version 1.1
 */
public class YBKC01LATask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    String opr = "";
    public YBKC01LATask()
    {}

    public void run()
    {
    	C01 c01 = new C01();
    	String SQL = "select distinct y.ConsultNo "
    			+" from YbkReport y,llcase lca,LLClaimdetail llc,lccont lc,YBK_N01_LIS_ResponseInfo ybk "
    			+" where y.PolicySequenceNo = ybk.PolicySequenceNo "
    			+" and ybk.prtno = lc.prtno " 
		    	+" and lc.contno = llc.contno "
		    	+" and llc.caseno = lca.caseno "
		    	+" and y.makedate <= lca.makedate "
		    	+" and lca.rgtstate in ('04','05','06','11','12') "
		    	+" and exists(select 1 from YBK_C01_LIS_ResponseInfo where caseno = y.ConsultNo and responsecode = '1' and prtno = '01' and requesttype = 'C01') "
		    	+" and not exists(select 1 from YBK_C01_LIS_ResponseInfo where caseno = y.ConsultNo and responsecode = '1' and  prtno in ('02','03') and requesttype = 'C01') " 
		    	//本地测试
//		    	+" and lca.caseno = 'C3100180730000001' "
		    	+" union "
		    	+" select distinct y.ConsultNo "
		    	+" from YbkReport y,llcase lca,LLClaimdetail llc,lbcont lc,YBK_N01_LIS_ResponseInfo ybk "
		    	+" where y.PolicySequenceNo = ybk.PolicySequenceNo "
		    	+" and ybk.prtno = lc.prtno "
		    	+" and lc.contno = llc.contno " 
		    	+" and llc.caseno = lca.caseno "
		    	+" and y.makedate <= lca.makedate "
		    	+" and lca.rgtstate in ('04','05','06','11','12') "
		    	+" and exists(select 1 from YBK_C01_LIS_ResponseInfo where caseno = y.ConsultNo and responsecode = '1' and prtno = '01' and requesttype = 'C01') "
		    	+" and not exists(select 1 from YBK_C01_LIS_ResponseInfo where caseno = y.ConsultNo and responsecode = '1' and  prtno in ('02','03') and requesttype = 'C01') "
		    	//本地测试
//		    	+" and lca.caseno = 'C3100180730000001' "
		    	+" with ur ";
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String tCaseNo = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("PrtNo", tCaseNo);
    			tVData.add(tTransferData);
    	    	if(!c01.submitData(tVData, "YBKPT"))
    	         {
    	    		 System.out.println("理赔立案信息上传批处理出问题了");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             continue;
    	         }else{
    	        	 System.out.println("理赔立案信息上传批处理成功了");
    	        	 opr ="ture";
    	         }
    		}
    	}

    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
   
    public static void main(String[] args)
    {
    	YBKC01LATask ybkTask = new YBKC01LATask();
    	ybkTask.run();
    }
}




