package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.ES_DOC_RELATIONBDB;
import com.sinosoft.lis.vschema.ES_DOC_RELATIONBSet;
import com.sinosoft.lis.schema.ES_DOC_RELATIONBSchema;
import com.sinosoft.lis.pubfun.EasyScanQueryUI;
import com.sinosoft.lis.pubfun.EasyScanArchiveBL;
import com.sinosoft.utility.TransferData;
import java.io.IOException;
import com.sinosoft.utility.*;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 * 本批处理对系统中的扫描件进行归档。
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author huxl
 * @version 1.0
 */
public class EasyScanBatchArchive extends TaskThread {
    CErrors mErrors = new CErrors();
    public String mCurrentDate = PubFun.getCurrentDate();
    GlobalInput mGlobalInput = new GlobalInput();
    private static String mlogic;
    /** 当前操作人员的机构 **/
    String manageCom = null;
    private FileWriter mFileWriter;
    private BufferedWriter mBufferedWriter;

    public EasyScanBatchArchive() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     **/
    public void run() {
        mlogic ="000001";
        if (!getEasyScanURL()) {
            return;
        }
    }

    /**
     * 置合同分保标记
     * @return boolean
     */
    private boolean getEasyScanURL() {
        System.out.println("come in EasyScanBatchArchive...");
//       String dir = "E:\\workspace\\ui\\printdata\\archive\\";
        String strSql = "select SysVarValue from LDSYSVAR where Sysvar='ArchiveUrl'";
        ExeSQL tExeSQL = new ExeSQL();
        String dir = tExeSQL.getOneValue(strSql);




        String tsql = "SELECT b.* FROM ES_Doc_Main a,ES_Doc_Relation b ,llcase c "
                           +" WHERE a.DocID = b.DocID and c.caseno=a.doccode and a.ModifyDate + 1 days ='" + mCurrentDate + "' AND b.SubType ='LP01'"
                                +" UNION  SELECT b.* FROM ES_Doc_Main a,ES_Doc_Relation b ,llregister c "
                                +" WHERE a.DocID = b.DocID and c.rgtno=a.doccode and a.ModifyDate + 1 days = '" + mCurrentDate + "' AND b.SubType ='LP02'"
        			+ " UNION SELECT b.* FROM LCCont a, ES_Doc_Relation b "
        			+ " WHERE SignDate + 1 days = '" + mCurrentDate + "' AND ContType = '1' AND a.PrtNo = b.BussNo "
        			+ " UNION SELECT b.* FROM LCGrpCont a, ES_Doc_Relation b "
        			+ " WHERE SignDate + 1 days = '" + mCurrentDate + "' AND a.PrtNo = b.BussNo "
                                + " UNION "
                                + " SELECT b.* FROM LPEDORITEM a , ES_Doc_Relation b ,   lccont c where a.edorappdate + 1 days='" + mCurrentDate + "' and a.contno=c.contno and c.conttype='1' and a.edoracceptno=bussno "
                                + " UNION "
                                + " SELECT b.* FROM LPGRPEDORITEM a , ES_Doc_Relation b , lccont c where a.edorappdate + 1 days='"+ mCurrentDate + "' and a.GRPCONTNO=c.GRPCONTNO and c.conttype='1' and a.edoracceptno=bussno with ur "
                                ;
        System.out.println("xxxxx:::"+ tsql);
        ES_DOC_RELATIONBSet tES_DOC_RELATIONBSet = new ES_DOC_RELATIONBDB().executeQuery(tsql);

        try {
            mFileWriter = new FileWriter(dir + "mapping" +
                                          mCurrentDate.replaceAll("-","") +
                                         ".txt");        //新建MAPPING文件
            mBufferedWriter = new BufferedWriter(mFileWriter);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        //下面对每个影印件单独处理
        for (int i = 1; i <=  tES_DOC_RELATIONBSet.size(); i++) {
        	ES_DOC_RELATIONBSchema tES_DOC_RELATIONBSchema = tES_DOC_RELATIONBSet.get(i);

        	String subType = tES_DOC_RELATIONBSchema.getSubType();
        	String detailType = "";

        	if(subType.equals("LP01"))
	        	detailType = "LP1001";
        	else if(subType.equals("LP02"))
	        	detailType = "LP1002";
        	else if(subType.equals("TB01"))
	        	detailType = "TB1001";
        	else if(subType.equals("TB02"))
	        	detailType = "TB1002";
        	else if(subType.equals("TB15"))
	        	detailType = "TB1015";
        	else if(subType.equals("TB16"))
	        	detailType = "TB1016";
        	else if(subType.equals("TB21"))
	        	detailType = "TB21";
        	else if(subType.equals("TB22"))
	        	detailType = "TB1022";
                else if(subType.equals("BQ01"))
                        detailType = "BQ01" ;
        	else
        		continue;

        	String[] arrPic = this.easyScanQueryKernel(tES_DOC_RELATIONBSchema.
                    getBussNo(), tES_DOC_RELATIONBSchema.getBussNoType(),
                    tES_DOC_RELATIONBSchema.getBussType(), detailType, "");
            if (arrPic != null) {
                TransferData data = new TransferData();
                data.setNameAndValue("arrPic", arrPic);
                data.setNameAndValue("prtNo", tES_DOC_RELATIONBSchema.getBussNo());
                data.setNameAndValue("SubType", subType);

                VData tVData = new VData();
                tVData.add(data);

                EasyScanArchiveBL tEasyScanArchiveBL = new EasyScanArchiveBL();
                try {
                    if (!tEasyScanArchiveBL.submitData(tVData, "")) {
                        System.out.println(tEasyScanArchiveBL.mErrors.
                                           getErrContent());
                    }else{
                        if(tEasyScanArchiveBL.mfilename[0]!= null){
                            for (int j = 0;j < tEasyScanArchiveBL.mfilename.length;j++) {
                                if(tEasyScanArchiveBL.mfilename[j]== null){
                                    break;
                                }
                                //写mapping文件
                                try {
                                    mBufferedWriter.write(tEasyScanArchiveBL.
                                            mfilename[j]+".tif" + "|" + mlogic +
                                            "|" + (j+1));
                                    mBufferedWriter.newLine();
                                    mBufferedWriter.flush();
                                } catch (IOException ex1) {
                                }
                             }
                            if (tEasyScanArchiveBL.mfilename.length != 0) {
                                mlogic = ""+(Integer.parseInt(mlogic)+1);     //逻辑文件名加1
                                int p = mlogic.length();   //获取mlogic的长度
                                for(int q=0;q < 6-p;q++){
                                    mlogic="0"+mlogic;    //逻辑文件号格式化
                                }
                            }
                        }
                    }

                } catch (IOException e) {
                    System.out.println(e.toString());
                }
            }
        }
        try {
            mBufferedWriter.close();
        } catch (IOException ex2) {
        }

        File  aFile = new File(dir);
        String bFile[] = aFile.list();
        for(int m=0;m<bFile.length;m++){
           // System.out.println("Filename:"+dir+bFile[m]);
            if(sendZip(dir+bFile[m])){
                 File cFile = new File(dir+bFile[m]);
                 cFile.delete();//删除上传完的文件
            }
        }
        return true;
    }

    public boolean submitData(String  aStartDate, String aEndDate) {
        String tStartDate = aStartDate;
        String tEndDate = aEndDate;
        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(tStartDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(tEndDate); //录入的终止日期，用FDate处理

        while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            mCurrentDate = chgdate.getString(dbdate); //录入的正在统计的日期
            if (!getEasyScanURL()) {
                return false;
            }
            dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
        }
        return true;
    }

    /**
     * 向指定地址上传文件
     * @param cZipFilePath String
     * @return boolean
     */
    private boolean sendZip(String cZipFilePath)
    {
        //登入前置机
        String ip = "";
        String user = "";
        String pwd = "";
        //String port = "";

        String sql = "select code,codename from LDCODE where codetype='ScanURL'";
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS != null){
            for(int m=1;m<=tSSRS.getMaxRow();m++){
                if(tSSRS.GetText(m,1).equals("http")){
                    ip = tSSRS.GetText(m,2);
                }else if(tSSRS.GetText(m,1).equals("user")){
                    user = tSSRS.GetText(m,2);
                }else if(tSSRS.GetText(m,1).equals("pwd")){
                    pwd = tSSRS.GetText(m,2);
                }
            }
        }
        FTPTool tFTPTool = new FTPTool(ip, user, pwd);
        try
        {
            if (!tFTPTool.loginFTP())
            {
                System.out.println(tFTPTool.getErrContent(1));
                return false;
            }
            else
            {
                if (!tFTPTool.upload(cZipFilePath))
                {
                    System.out.println("上载文件失败!");
                    CError tError = new CError();
                    tError.moduleName = "BPOSentScanBL";
                    tError.functionName = "sendZip";
                    tError.errorMessage = tFTPTool
                            .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                    return false;
                }
                tFTPTool.logoutFTP();
            }
        }
        catch (SocketException ex)
        {
            ex.printStackTrace();

            System.out.println("上载文件失败，可能是网络异常。");
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "sendZip";
            tError.errorMessage = tFTPTool
                    .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();

            System.out.println("上载文件失败，可能是无法写入文件");
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "sendZip";
            tError.errorMessage = tFTPTool
                    .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    // 调用EasyScanQueryUI进行单证印刷号PrtNo提交和数据库查找，返回结果字符串strResult
    private String[] easyScanQueryKernel(String ptrNo, String BussNoType,
                                         String BussType, String SubType,
                                         String clientUrl) {
        String[] arrResult;
        if (ptrNo == null || ptrNo.equals("") || ptrNo.equals("undefined")) {
            System.out.println("EasyScanQuery don't accept ptrNo!");
            return null;
        }

        VData tVData = new VData();
        VData tVData1 = new VData();
        tVData.add(ptrNo);
        tVData.add(BussNoType);
        tVData.add(BussType);
        tVData.add(SubType);
        tVData.add(clientUrl);
        EasyScanQueryUI tEasyScanQueryUI = new EasyScanQueryUI();
        tEasyScanQueryUI.submitData(tVData, "QUERY||MAIN");

        if (tEasyScanQueryUI.mErrors.needDealError()) {
            System.out.println("EasyScanQueryUI throw errors:" +
                               tEasyScanQueryUI.mErrors.getFirstError());
            return null;
        } else {
            tVData.clear();
            tVData = tEasyScanQueryUI.getResult();
            tVData1 = (VData) tVData.get(0);
            arrResult = new String[tVData1.size()];
            for (int i = 0; i < tVData1.size(); i++) {
                arrResult[i] = (String) tVData1.get(i);
            }
        }

        return arrResult;
    }

    public static void main(String args[]) {
        EasyScanBatchArchive tEasyScanBatchArchive = new EasyScanBatchArchive();
        tEasyScanBatchArchive.mCurrentDate = args[0];
        //EasyScanArchiveBL aEasyScanArchiveBL = new EasyScanArchiveBL();
        tEasyScanBatchArchive.run();
        return;
    }

    private void jbInit() throws Exception {
    }
}
