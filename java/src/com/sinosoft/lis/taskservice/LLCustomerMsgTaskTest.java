package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.llcase.LLCustomerMsgNewBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 发送客户理赔短信测试类
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: </p>
 *
 * @author 
 * @version 1.0
 */
public class LLCustomerMsgTaskTest {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    String Content = "";
    String Email = "";
    String Mobile = "";
    String opr="";
    // 输入数据的容器
    private VData mInputData = new VData();

    private MMap map = new MMap();
    public LLCustomerMsgTaskTest() {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run() {
        if (!sendCustomerMsg()) {
            return;
        }
    }

    private boolean sendCustomerMsg() {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
//        LLCustomerMsgBL tLLCustomerMsgBL = new LLCustomerMsgBL();
//        if (!tLLCustomerMsgBL.submitData(mVData, "LPMSG")) {
//            System.out.println("客户理赔短信发送失败！");
//            CError tError = new CError();
//            tError.moduleName = "LLCustomerMsgBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "客户理赔短信发送失败";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        LLCustomerMsgNewBL tLLCustomerMsgNewBL = new LLCustomerMsgNewBL();
        if (!tLLCustomerMsgNewBL.submitData(mVData, "LPMSG")) {
            System.out.println("客户理赔短信发送失败！");
            CError tError = new CError();
            tError.moduleName = "LLCustomerMsgNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "客户理赔短信发送失败";
            this.mErrors.addOneError(tError);
            opr="false";
            return false;
            
        }else{
        	System.out.println("客户理赔短信发送成功！");
        	opr="true";
        }
        return true;
    }
    
	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
    
    public static void main(String args[]) {
        LLCustomerMsgTaskTest a = new LLCustomerMsgTaskTest();
        a.run();

    }
    
}
