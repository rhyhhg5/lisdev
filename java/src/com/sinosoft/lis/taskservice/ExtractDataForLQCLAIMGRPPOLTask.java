package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.db.LQCLAIMGRPPOLDB;
import com.sinosoft.lis.db.SolidificationLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LQCLAIMGRPPOLSchema;
import com.sinosoft.lis.schema.SolidificationLogSchema;
import com.sinosoft.lis.vschema.LQCLAIMGRPPOLSet;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;

public class ExtractDataForLQCLAIMGRPPOLTask extends TaskThread
{
	public String mCurrentDate = PubFun.getCurrentDate();
//	public String mCurrentDate = "2017-8-11";
	private SolidificationLogSchema mSolidificationLogSchema = new SolidificationLogSchema();

	public void run() 
	{
		System.out.println("ExtractDataForLQCLAIMGRPPOLTask批处理开始执行！");
		String tSolidificationLogSQL = "select * from SolidificationLog where TableName = 'LQCLAIMGRPPOL' with ur";
		System.out.println("tSolidificationLogSQL:"+tSolidificationLogSQL);
		SolidificationLogDB tSolidificationLogDB = new SolidificationLogDB();
		mSolidificationLogSchema = tSolidificationLogDB.executeQuery(
				tSolidificationLogSQL).get(1);
		String tRealEndDate = mSolidificationLogSchema.getEndDate();
		String tState = mSolidificationLogSchema.getState();
		if("00".equals(tState))
		{
			System.out.println(tRealEndDate+"数据状态为00，需要先进行手动处理");
			return;
		}
		else if("11".equals(tState)) 
		{
			System.out.println("state是11");
			tRealEndDate = PubFun.calDate(tRealEndDate, 1, "D", "");
		}
		FDate tFDate = new FDate();
		Date tStartDate = tFDate.getDate(tRealEndDate);
		Date tEndDate = tFDate.getDate(mCurrentDate);
		while (tStartDate.compareTo(tEndDate) < 0) 
		{
			String tModifyDate = tFDate.getString(tStartDate);
			System.out.println("开始--提取"+tModifyDate+"的数据");
			mSolidificationLogSchema.setEndDate(tModifyDate);
			if (!insertData(tModifyDate)) 
			{
				tSolidificationLogDB.setSchema(mSolidificationLogSchema);
				tSolidificationLogDB.update();
				break;
			}
			mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
			mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
			mSolidificationLogSchema.setState("11");
			mSolidificationLogSchema.setExceptionInfo("处理成功");
			tSolidificationLogDB.setSchema(mSolidificationLogSchema);
			tSolidificationLogDB.update();
			System.out.println("结束---提取"+tModifyDate+"的数据");
			tStartDate = PubFun.calDate(tStartDate, 1, "D", null);
		}

	}

	private boolean insertData(String cModifyDate) 
	{
		String tInsertSQL = "select type,grpcontno,grppolno,RiskCode,sum(Prem) as Prem,ManageCom, Payintv, salechnl, cvalidate, enddate, signdate "
				+ " from "
				+ " (select 'C' Type, grpcontno, grppolno, RiskCode, cast(sum(Prem) as decimal(12, 2)) Prem, "
				+ " ManageCom, Payintv, salechnl, cvalidate, enddate, (select signdate from lcgrpcont where lcpol.grpcontno=lcgrpcont.grpcontno and appflag = '1') signdate "
				+ " from "
				+ " lcpol "
				+ " where 1=1 "
				+ " and grpcontno <> '00000000000000000000' "
				+ " and conttype = '2' " 
				+ " and exists (select 1 from lcgrpcont where lcpol.grpcontno=lcgrpcont.grpcontno and appflag = '1')"
				+ " and appflag = '1' "
				+ " and modifydate = '" + cModifyDate + "'"
				+ " group by "
				+ " ManageCom, Payintv, salechnl, cvalidate, riskcode, grpcontno, "
				+ " grppolno, enddate, signdate "
				+ " union all "
				+ " select 'B' Type, a.grpcontno grpcontno, a.grppolno grppolno, a.RiskCode riskcode, "
				+ " cast(sum(a.Prem) as decimal(12, 2)) Prem, a.ManageCom managecom, "
				+ " a.Payintv payintv, a.salechnl salechnl, a.cvalidate cvalidate, "
				+ " (case b.edortype "
				+ " when 'WT' then a.cvalidate else b.edorvalidate end) enddate, a.signdate signdate "
				+ " from "
				+ " lbpol a, lpedoritem b "
				+ " where "
				+ " a.edorno = b.edorno "
				+ " and a.contno = b.contno "
				+ " and a.grpcontno <> '00000000000000000000' "
				+ " and a.conttype = '2' "
				+ " and b.edortype in ('CT', 'XT', 'ZT', 'WT') "
				+ " and exists ( "
				+ " select 1 "
				+ " from "
				+ " lpedorapp "
				+ " where "
				+ " edoracceptno = b.edorno "
				+ " and edorstate = '0') "
				+ " and a.modifydate = '" + cModifyDate + "'"
				+ " group by "
				+ " a.ManageCom, a.Payintv, a.salechnl, a.cvalidate, a.riskcode, a.grpcontno, a.grppolno, b.edorvalidate, a.signdate, b.edortype "
				+ " union all "
				+ " select 'B' Type, a.grpcontno grpcontno, a.grppolno grppolno, a.RiskCode riskcode, "
				+ " cast(sum(a.Prem) as decimal(12, 2)) Prem, a.ManageCom managecom, a.Payintv payintv, a.salechnl salechnl, a.cvalidate cvalidate, "
				+ " (case b.edortype when 'WT' then a.cvalidate "
				+ " else b.edorvalidate end) enddate, a.signdate signdate "
				+ " from "
				+ " lbpol a, lpgrpedoritem b "
				+ " where "
				+ " a.edorno = b.edorno "
				+ " and a.grpcontno = b.grpcontno "
				+ " and a.grpcontno <> '00000000000000000000' "
				+ " and a.conttype = '2' "
				+ " and b.edortype in ('XT', 'WT') "
				+ " and exists ( "
				+ " select 1 "
				+ " from "
				+ " lpedorapp "
				+ " where "
				+ " edoracceptno = b.edorno "
				+ " and edorstate = '0' "
				+ " and othernotype = '2') "
				+ " and a.modifydate = '" + cModifyDate + "'"
				+ " group by "
				+ " a.ManageCom, a.Payintv, a.salechnl, a.cvalidate, a.riskcode, "
				+ " a.grpcontno, a.grppolno, b.edorvalidate, a.signdate, b.edortype "
				+ " union all "
				+ " select 'B' Type, grpcontno, grppolno, RiskCode, cast(sum(Prem) as decimal(12, 2)) Prem, "
				+ " ManageCom, Payintv, salechnl, cvalidate, enddate, (select signdate from lbgrpcont where a.grpcontno=lbgrpcont.grpcontno and appflag = '1') signdate "
				+ " from "
				+ " lbpol a "
				+ " where 1=1 "
				+ " and appflag = '1' "
				+ " and grpcontno <> '00000000000000000000' "
				+ " and exists (select 1 from lbgrpcont where a.grpcontno=lbgrpcont.grpcontno and appflag = '1')"
				+ " and exists ( "
				+ " select 1 "
				+ " from "
				+ " lcrnewstatelog "
				+ " where "
				+ " grpcontno = a.grpcontno "
				+ " and newpolno = a.polno ) "
				+ " and a.modifydate = '" + cModifyDate + "'"
				+ " group by ManageCom, Payintv, salechnl, cvalidate, riskcode, grpcontno, "
				+ " grppolno, enddate, signdate) m "
				+ " where not exists (select 1"
				+ " from"
				+ " LQCLAIMGRPPOL"
				+ " where "
				+ " grpcontno = m.grpcontno "
				+ " and grppolno = m.grppolno"
				+ " and RiskCode = m.RiskCode"
				+ " and ManageCom = m.ManageCom "
				+ " and Payintv = m.Payintv "
				+ " and salechnl = m.salechnl "
			    + " and cvalidate = m.cvalidate "
				+ " and enddate = m.enddate "
				+ " and signdate = m.signdate) "
				+ " group by type,ManageCom, Payintv, salechnl, cvalidate, riskcode, grpcontno, "
				+ " grppolno, enddate, signdate with ur";
		System.out.println("1");
		System.out.println("INSERTSQL:"+tInsertSQL);
		LQCLAIMGRPPOLSet tLQCLAIMGRPPOLSet = new LQCLAIMGRPPOLSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLQCLAIMGRPPOLSet, tInsertSQL);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		do
		{
			tRSWrapper.getData();
			if(null==tLQCLAIMGRPPOLSet||0==tLQCLAIMGRPPOLSet.size())
			{
				return true;
			}
			//声明接收新数据的set
			LQCLAIMGRPPOLSet tInsertLQCLAIMGRPPOLSet = new LQCLAIMGRPPOLSet();
			//声明接收旧数据的set
			LQCLAIMGRPPOLSet tUpdateLQCLAIMGRPPOLSet = new LQCLAIMGRPPOLSet();
			System.out.println("开始根据查询到需要入库的数据判断是插入还是更新");
			for(int i = 1;i<=tLQCLAIMGRPPOLSet.size();i++)
			{
				LQCLAIMGRPPOLSchema tLQCLAIMGRPPOLSchema = new LQCLAIMGRPPOLSchema();
				tLQCLAIMGRPPOLSchema = tLQCLAIMGRPPOLSet.get(i);
				String tNewOrOldSql = "select * from LQCLAIMGRPPOL where 1=1"
									+ " and grpcontno = '" + tLQCLAIMGRPPOLSchema.getGrpContNo() + "'"
									+ " and grppolno = '" + tLQCLAIMGRPPOLSchema.getGrpPolNo() + "'"
									+ " and riskcode = '" + tLQCLAIMGRPPOLSchema.getRiskCode() + "'"
									+ " and managecom = '" + tLQCLAIMGRPPOLSchema.getManageCom() + "'"
									+ " and Payintv = '" + tLQCLAIMGRPPOLSchema.getPayIntv() + "'"
									+ " and salechnl = '" + tLQCLAIMGRPPOLSchema.getSalechnl() + "'"
									+ " and cvalidate = '" + tLQCLAIMGRPPOLSchema.getCValiDate() + "'"
									+ " and enddate = '" + tLQCLAIMGRPPOLSchema.getEndDate() + "'"
									+ " and signdate = '" + tLQCLAIMGRPPOLSchema.getSignDate() + "' with ur";
				System.out.println("tNewOrOldSql:" + tNewOrOldSql);
				//声明一个DB来执行这个sql
				LQCLAIMGRPPOLDB tLQCLAIMGRPPOLDB = new LQCLAIMGRPPOLDB();
				//声明一个set接收db执行sql返回的结果
				LQCLAIMGRPPOLSet tOldLQCLAIMGRPPOLSet = new LQCLAIMGRPPOLSet();
				tOldLQCLAIMGRPPOLSet = tLQCLAIMGRPPOLDB.executeQuery(tNewOrOldSql);
				if(null != tOldLQCLAIMGRPPOLSet && tOldLQCLAIMGRPPOLSet.size()>0)
				{
					for(int j = 1;j<=tOldLQCLAIMGRPPOLSet.size();j++)
					{
						//声明接收旧数据的Schema
						LQCLAIMGRPPOLSchema tOldLQCLAIMGRPPOLSchema = new LQCLAIMGRPPOLSchema();
						tOldLQCLAIMGRPPOLSchema = tOldLQCLAIMGRPPOLSet.get(j);
						if(tOldLQCLAIMGRPPOLSchema.getType().equals(tLQCLAIMGRPPOLSchema.getType()))
						{
							tOldLQCLAIMGRPPOLSchema.setPrem(tOldLQCLAIMGRPPOLSchema.getPrem()+tLQCLAIMGRPPOLSchema.getPrem());
						}else
						{
							tOldLQCLAIMGRPPOLSchema.setPrem(tOldLQCLAIMGRPPOLSchema.getPrem()-tLQCLAIMGRPPOLSchema.getPrem());
						}
						tUpdateLQCLAIMGRPPOLSet.add(tOldLQCLAIMGRPPOLSchema);
					}
				}else
				{
					System.out.println("是新数据");
					tInsertLQCLAIMGRPPOLSet.add(tLQCLAIMGRPPOLSchema);
				}
			}
			tMMap.put(tInsertLQCLAIMGRPPOLSet, "INSERT");
			tMMap.put(tUpdateLQCLAIMGRPPOLSet, "UPDATE");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if(!tPubSubmit.submitData(tVData, ""))
			{
				System.out.println("ExtractDataForLQCLAIMGRPPOLTask 批处理执行异常！");
				mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
				mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
				mSolidificationLogSchema.setState("00");
				mSolidificationLogSchema.setExceptionInfo("提取" + cModifyDate + "天数据更新时失败");
				return false;
			}
			System.out.println("本次数据提交数据库成功");
			tMMap = new MMap();
			tVData.clear();
		}while(tLQCLAIMGRPPOLSet.size()>0);
		return true;
	}
	public static void main(String[] args) 
	{
		ExtractDataForLQCLAIMGRPPOLTask tExtractDataForLQCLAIMGRPPOLTask = 
				new ExtractDataForLQCLAIMGRPPOLTask();
		tExtractDataForLQCLAIMGRPPOLTask.run();
	}
}
