package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.db.LGLetterDB;
import com.sinosoft.lis.vschema.LGLetterSet;
import com.sinosoft.lis.db.LPEdorAppDB;
import com.sinosoft.lis.vschema.LPEdorAppSet;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 函件超时日处理类,当函件到达预定的回销日期时，工单信息的处理状态为“函件已超时”
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class LetterOverTimeTask extends TaskThread
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData mInputData = new VData();

    private GlobalInput mGlobalInput = null;

    // 统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

    private MMap map = new MMap();

    public LetterOverTimeTask()
    {
    }

    /**
     * 执行函件超时处理
     * */
    public void run()
    {
        if (!getOverTimeLetter())
        {
            return;
        }

        if (map.size() > 0)
        {
            mInputData.add(map);

            //提交操作数据
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN"))
            {
                mErrors.copyAllErrors(tPubSubmit.mErrors);
                mErrors.addOneError("提交函件超时处理时出错");
                System.out.println("生成电话催缴任务时出错");

                return;
            }

        }
    }

    /**
     * 支持手动执行待办件超时日处理程序
     * 只能操纵本机构及本机构的下级机构的待办件
     * */
    public void oneCompany(GlobalInput g)
    {
        mGlobalInput = g;

        run();
    }

    //处理函件已超时的工单状态
    private boolean getOverTimeLetter()
    {
        String sql;

        if(mGlobalInput == null)
        {
            sql = "select distinct edorAcceptNo "
                  + "from LGLetter "
                  + "where backFlag = '1' " //需回销
                  + "    and state = '1' " //且已下发的
                  + "    and days(backDate) < days('" + mCurrentDate + "') ";
        }
        //只能操纵本机构及本机构的下级机构的超市函件
        else
        {
            sql = "select distinct edorAcceptNo "
                  + "from LGLetter a, LDUser b "
                  + "where backFlag = '1' " //需回销
                  + "    and state = '1' " //且已下发的
                  + "    and days(backDate) < days('" + mCurrentDate + "') "
                  + "    and a.operator = b.userCode "
                  + "    and b.comCode like '" + mGlobalInput.ComCode + "%'";
        }

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS;
        try
        {
            tSSRS = tExeSQL.execSQL(sql);
        }
        catch(Exception e)
        {
            mErrors.addOneError("查询超时函件出错" + e.toString());
            System.out.println("查询超时函件出错" + e.toString());

            return false;
        }

        if(tSSRS != null && tSSRS.getMaxRow() != 0)
        {
            if(!changeAppState(tSSRS))
            {
                return false;
            }
            if(!changeLetterState(tSSRS))
            {
                return false;
            }
        }

        return true;
    }

    //改变保全业务状态
    private boolean changeAppState(SSRS tSSRS)
    {
        LPEdorAppSet tLPEdorAppSet = new LPEdorAppSet();

        for(int i = 1; i < tSSRS.getMaxRow() + 1; i++)
        {
            //改变工单保全状态为函件超时
            LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();

            tLPEdorAppDB.setEdorAcceptNo(tSSRS.GetText(i, 1));
            if(tLPEdorAppDB.getInfo())
            {
                tLPEdorAppDB.setEdorState("8");
                tLPEdorAppDB.setModifyDate(mCurrentDate);
                tLPEdorAppDB.setModifyTime(mCurrentTime);

                tLPEdorAppSet.add(tLPEdorAppDB.getSchema());
            }
        }
        if(tLPEdorAppSet.size() > 0)
        {
            map.put(tLPEdorAppSet, "DELETE&INSERT");
        }

        return true;
    }

    //改变保全业务状态
    private boolean changeLetterState(SSRS tSSRS)
    {
        LGLetterSet tLGLetterSet = new LGLetterSet();

        for(int i = 1; i < tSSRS.getMaxRow() + 1; i++)
        {
            //查询函件数
            LGLetterDB db = new LGLetterDB();
            db.setEdorAcceptNo(tSSRS.GetText(i, 1));
            LGLetterSet set = db.query();

            LGLetterDB tLGLetterDB = new LGLetterDB();
            tLGLetterDB.setEdorAcceptNo(tSSRS.GetText(i, 1));

            //所需函件序号 = 函件数, 因为下发函件是串行的
            tLGLetterDB.setSerialNumber(String.valueOf(set.size()));
            if(tLGLetterDB.getInfo())
            {
                //改变函件状态为函件超时
                tLGLetterDB.setState("6");
                tLGLetterDB.setModifyDate(mCurrentDate);
                tLGLetterDB.setModifyTime(mCurrentTime);

                tLGLetterSet.add(tLGLetterDB.getSchema());
            }
        }
        if(tLGLetterSet.size() > 0)
        {
            map.put(tLGLetterSet, "DELETE&INSERT");
        }

        return true;
    }


    public static void main(String args[])
    {
        LetterOverTimeTask lot = new LetterOverTimeTask();
        GlobalInput g = new GlobalInput();
        g.Operator = "endor5";
        g.ComCode = "86";

        lot.oneCompany(g);

        if (lot.mErrors.needDealError())
        {
            System.out.println(lot.mErrors.getErrContent());
        }
        else
        {
            System.out.println("成功");
        }

    }
}



