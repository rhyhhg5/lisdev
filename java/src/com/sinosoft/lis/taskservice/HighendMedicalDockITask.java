package com.sinosoft.lis.taskservice;

import java.io.File;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.f1print.Readhtml;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;

public class HighendMedicalDockITask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = new GlobalInput();

	/** 文件路径 */
	private String tOutXmlPath = "";

	/** 应用路径 */
	private String mURL = "";

	/** 应用路径 */
	private String mURL1 = "";

	/** 压缩文件的路径 */
	private String mZipFilePath;

	private String fileName = "";

	String[][] mToExcel = null;

	String[][] mToExcel2 = null;

	SSRS tSSRS = new SSRS();

	private String mSql = null;

	private boolean haveCB = false;

	private boolean haveBQ = false;

	private boolean haveScan = false;

	private String mScanFilePath = "";

	private static final int BUFFER = 2048;

	// 执行任务
	public void run() {
		dealData();
	}

	public boolean dealData() {
		System.out.println("开始执行批处理:" + PubFun.getCurrentTime());
		// 先准备数据
		getInputData();

		// 打印报表
		getReprot();

		// 得到扫描件
		getScan();

		// 报表文件压缩并发送邮件
		getReportZipFile();

		System.out.println("批处理执行完毕:" + PubFun.getCurrentTime());

		return true;
	}

	/**
	 * ] 判断文件是否存在
	 * 
	 * @param filePath
	 * @return
	 */
	private boolean FileExists(String filePath) {
		File reportFile = new File(filePath);
		if (!reportFile.exists()) {
			return false;
		}
		return true;
	}

	/**
	 * 将给定目录下的文件打包为压缩包
	 * 
	 */
	private void getZipFile() {

		String tDocFileName = mURL + fileName; // 文件绝对路径 + 文件名
		mZipFilePath = mURL + fileName.replace("xls", "zip");
		System.out.println("压缩路径名：" + mZipFilePath);
		compressIntoZip(mZipFilePath, tDocFileName);
	}

	/**
	 * 添加到压缩文件
	 * 
	 * @param zipfilepath
	 *            String
	 * @param out
	 *            ZipOutputStream
	 * @param tZipDocFileName
	 *            String：zip文件名
	 * @param tDocFileName
	 *            String：待压缩的文件路径名
	 * @return int：1：成功、0：失败，但不中断整个程序、-1：失败，中断程序
	 */
	private int compressIntoZip(String zipfilepath, String tDocFileName) {
		System.out.println("zipfilepath:" + zipfilepath);
		System.out.println("tDocFileName待压缩文件压缩路径:" + tDocFileName);
		Readhtml rh = new Readhtml();
		String[] tInputEntry = new String[1];
		tInputEntry[0] = tDocFileName;
		rh.CreateZipFile(tInputEntry, zipfilepath);
		return 1;
	}

	private int compressIntoZip(String zipfilepath, String[] tDocFileName) {
		System.out.println("zipfilepath:" + zipfilepath);
		System.out.println("tDocFileName待压缩文件压缩路径:" + tDocFileName);
		Readhtml rh = new Readhtml();
		String[] tInputEntry = tDocFileName;
		rh.CreateZipFile(tInputEntry, zipfilepath);
		return 1;
	}

	/**
	 * 将报表查询文件压缩包发送邮件
	 * 
	 */
	private boolean sendSagentMail() {
		// 压缩文件生成成功，开始发送邮件
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
		SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
				.GetText(1, 2), "picchealth");
		// 设置邮件发送信息
		String sendInf = "个险高端医疗产品报送数据见附件！";
		String sendInfs = "";
		if (haveCB && haveBQ) {
			sendInfs = "";
		} else if (haveCB || haveBQ) {
			sendInfs = haveCB ? "其中保全无数据。" : "其中契约无数据。";
		}
		String Fj = mZipFilePath;
		if (haveScan) {
			Fj += "|" + mScanFilePath;
		}
		tMailSender.setSendInf("个险高端医疗产品报送数据", sendInf + sendInfs, Fj);
		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
		tMailSender.setToAddress("highendmedicalI");
		if (!tMailSender.sendMail()) {
			tMailSender.getErrorMessage();
		}
		deleteFile(tOutXmlPath);
		deleteFile(mZipFilePath);
		if (haveScan) {
			deleteFile(mScanFilePath);
		}

		return true;

	}

	/**
	 * 删除生成的ZIP文件和XML文件
	 * 
	 * @param cFilePath
	 *            String：完整文件名
	 */
	private boolean deleteFile(String cFilePath) {
		File f = new File(cFilePath);
		if (!f.exists()) {
			CError tError = new CError();
			tError.moduleName = "MonthDataCatch";
			tError.functionName = "deleteFile";
			tError.errorMessage = "没有找到需要删除的文件";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		f.delete();
		return true;
	}

	private void sendMessage() {

		// 邮箱发送
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
		SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
				.GetText(1, 2), "picchealth");
		// 设置邮件发送信息
		String sendInf = "个险高端医疗产品报送数据中：契约、保全均无数据，请知悉~~";
		tMailSender.setSendInf("个险高端医疗产品报送数据", sendInf, null);
		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
		tMailSender.setToAddress("highendmedicalI");
		if (!tMailSender.sendMail()) {
			tMailSender.getErrorMessage();
		}

	}

	private boolean getReportZipFile() {
		String name = tOutXmlPath;
		if (FileExists(name)) {
			System.out.println("打印文件存在");
			getZipFile();
			sendSagentMail();
		} else {
			sendMessage();
			System.out.println("没有符合打印条件的数据");
		}

		return true;
	}

	private boolean getReprot() {

		fileName = "High_EndMedicalIReport.xls";
		tOutXmlPath = mURL + fileName;
		System.out.println("文件名称====：" + tOutXmlPath);
		WriteToExcel t = new WriteToExcel(fileName);
		t.createExcelFile();
		t.setAlignment("CENTER");
		t.setFontName("宋体");
		String[] sheetName = { new String("契约数据报表"), new String("保全数据报表") };
		t.addSheetGBK(sheetName);
		if (!getData1()) {
			mToExcel = getDefalutData();
		}
		t.setData(0, mToExcel);

		if (!getData2()) {
			mToExcel2 = getDefalutData();
		}
		t.setData(1, mToExcel2);

		if (haveBQ || haveCB) {
			try {
				t.write(mURL);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		System.out.println("生成文件完成");

		return true;

	}

	private void getInputData() {
		// 设置管理机构
		mGI.ManageCom = "86";
		mGI.Operator = "001";
		mGI.ComCode = "86";

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL1 = new ExeSQL().getOneValue(tSQL);
//		mURL1 = "F:/";
		mURL = mURL1 + "vtsfile/";

		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}

	}

	private String[][] getDefalutData() {
		String[][] tToExcel = new String[2][30];
		tToExcel[0][0] = "无数据";
		return tToExcel;
	}

	public boolean getData1() {
		System.out.println("BL->dealDate()");

		ExeSQL tExeSQL = new ExeSQL();
		mSql = " Select Lcp.Signdate 契约承保操作完成日期, "
				+ "        '新契约' 操作类型, "
				+ "        '新契约' 操作项目, "
				+ "        Lcc.Prtno 操作作业号, "
				+ "        Substr(Lcc.Managecom, 1, 4) 承保机构, "
				+ "        (Select Name "
				+ "         From Ldcom "
				+ "         Where Comcode = Substr(Lcc.Managecom, 1, 4)) 承保机构名称, "
				+ "        lcc.contno 保单号,"
				+ "        Lcp.Signdate 承保日期, "
				+ "        Lcp.Cvalidate 生效日, "
				+ "        Lcp.enddate 终止日期, "
				+ "        lcp.riskcode 产品代码, "
				+ "        Lcp.Insuredname 被保险人姓名, "
				+ "        Codename('relation', Lci.Relationtomaininsured) 与主被保险人关系, "
				+ "        Codename('idtype', Lci.Idtype) 证件类型, "
				+ "        Lci.Idno 证件号码, "
				+ "        Codename('sex', Lci.Sex) 性别, "
				+ "        Lci.Birthday 生日, "
				+ "        (select codename from ldcode1 where codetype='highendriskI' and code=lcp.riskcode and code1=lcp.mult)  保障计划类型, "
				+ "        (select codealias from ldcode1 where codetype='highendriskI' and code=lcp.riskcode and code1=lcp.mult)  承保区域, "
				+ " Lcp.Prem ,lcp.Amnt "
				+ " From Lccont Lcc, "
				+ "      Lcpol Lcp, "
				+ "      Lcinsured Lci "
				+ " Where Lcc.Contno = Lcp.Contno "
				+ " And Lcp.Contno = Lci.Contno "
				+ " And Lcp.Insuredno = Lci.Insuredno "
				+ " And Lcc.ContType = '1' "
				+ " And Lcc.Appflag = '1'  "
				 + " And Lcp.SignDate= Current date - 1 day "
				+ " And lcp.riskcode in (select distinct code from ldcode1 where codetype='highendriskI') ";

		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);

		int count = tSSRS.getMaxRow();
		mToExcel = new String[count + 4][30];
		mToExcel[0][0] = "契约承保操作完成日期";
		mToExcel[0][1] = "操作类型";
		mToExcel[0][2] = "操作项目";
		mToExcel[0][3] = "操作作业号";
		mToExcel[0][4] = "承保机构";
		mToExcel[0][5] = "承保机构名称";
		mToExcel[0][6] = "保单号";
		mToExcel[0][7] = "承保日期";
		mToExcel[0][8] = "生效日期";
		mToExcel[0][9] = "终止日期";
		mToExcel[0][10] = "产品代码";
		mToExcel[0][11] = "被保险人姓名";
		mToExcel[0][12] = "与主被保险人关";
		mToExcel[0][13] = "证件类型";
		mToExcel[0][14] = "证件号码";
		mToExcel[0][15] = "性别";
		mToExcel[0][16] = "生日";
		mToExcel[0][17] = "保障计划类型";
		mToExcel[0][18] = "承保区域";
		mToExcel[0][19] = "保费";
		mToExcel[0][20] = "保额";

		if (count > 0) {
			haveCB = true;
		}

		for (int i = 1; i <= count; i++) {
			mToExcel[i][0] = tSSRS.GetText(i, 1);
			mToExcel[i][1] = tSSRS.GetText(i, 2);
			mToExcel[i][2] = tSSRS.GetText(i, 3);
			mToExcel[i][3] = tSSRS.GetText(i, 4);
			mToExcel[i][4] = tSSRS.GetText(i, 5);
			mToExcel[i][5] = tSSRS.GetText(i, 6);
			mToExcel[i][6] = tSSRS.GetText(i, 7);
			mToExcel[i][7] = tSSRS.GetText(i, 8);
			mToExcel[i][8] = tSSRS.GetText(i, 9);
			mToExcel[i][9] = tSSRS.GetText(i, 10);
			mToExcel[i][10] = tSSRS.GetText(i, 11);
			mToExcel[i][11] = tSSRS.GetText(i, 12);
			mToExcel[i][12] = tSSRS.GetText(i, 13);
			mToExcel[i][13] = tSSRS.GetText(i, 14);
			mToExcel[i][14] = tSSRS.GetText(i, 15);
			mToExcel[i][15] = tSSRS.GetText(i, 16);
			mToExcel[i][16] = tSSRS.GetText(i, 17);
			mToExcel[i][17] = tSSRS.GetText(i, 18);
			mToExcel[i][18] = tSSRS.GetText(i, 19);
			mToExcel[i][19] = tSSRS.GetText(i, 20);
			mToExcel[i][20] = tSSRS.GetText(i, 21);
		}

		return true;
	}

	public boolean getData2() {
		System.out.println("BL->dealDate()");

		ExeSQL tExeSQL = new ExeSQL();
		mSql = " Select lpp.confdate 保全确认日期, "
				+ "        '保全' 操作类型, "
				+ "        (select edorname from lmedoritem where edorcode=lpe.edortype fetch first 1 rows only) 操作项目, "
				+ "        lpe.edorno 操作作业号, "
				+ "        Substr(Lcc.Managecom, 1, 4) 承保机构, "
				+ "        (Select Name "
				+ "         From Ldcom "
				+ "         Where Comcode = Substr(Lcc.Managecom, 1, 4)) 承保机构名称, "
				+ "        Lcc.Contno 保单号, "
				+ "        Lcp.Signdate 承保日期, "
				+ "        Lcp.Cvalidate 生效日, "
				+ "        Lcp.enddate 终止日期, "
				+ "        Lcp.Riskcode 产品代码, "
				+ "        Lcp.Insuredname 被保险人姓名, "
				+ "        Codename('relation', Lci.Relationtomaininsured) 与主被保险人关系, "
				+ "        Codename('idtype', Lci.Idtype) 证件类型, "
				+ "        Lci.Idno 证件号码, "
				+ "        Codename('sex', Lci.Sex) 性别, "
				+ "        Lci.Birthday 生日, "
				+ "        (Select Codename "
				+ "         From Ldcode1 "
				+ "         Where Codetype = 'highendriskI' "
				+ "         And Code = Lcp.Riskcode "
				+ "         And Code1 = Lcp.Mult) 保障计划类型, "
				+ "        (Select Codealias "
				+ "         From Ldcode1 "
				+ "         Where Codetype = 'highendriskI' "
				+ "         And Code = Lcp.Riskcode "
				+ "         And Code1 = Lcp.Mult) 承保区域 "
				+ " From Lccont Lcc, "
				+ "      Lcpol Lcp, "
				+ "      Lcinsured Lci, "
				+ "      lpedoritem lpe, "
				+ "      lpedorapp lpp "
				+ " Where Lcc.Contno = Lcp.Contno "
				+ " And Lcp.Contno = Lci.Contno "
				+ " And Lcp.Insuredno = Lci.Insuredno "
				+ " And lcc.contno=lpe.contno "
				+ " And lpe.edorno=lpp.edoracceptno "
				+ " And lpe.edortype ='BF' "
				+ " And lpe.edorstate='0' "
				+ " And Lcc.Conttype = '1' "
				+ " And Lcc.Appflag = '1' "
				+ " And lpp.confdate= Current date - 1 day "
				+ " And exists (select 1 from lppol where edorno=lpe.edorno and polno=lcp.polno and edortype=lpe.edortype) "
				+ " And Lcp.Riskcode In (Select Distinct Code "
				+ "                     From Ldcode1 "
				+ "                     Where Codetype = 'highendriskI') "
				+ " union "
				+ " Select lpp.confdate 保全确认日期, "
				+ "        '保全' 操作类型, "
				+ "        (select edorname from lmedoritem where edorcode=lpe.edortype fetch first 1 rows only) 操作项目, "
				+ "        lpe.edorno 操作作业号, "
				+ "        Substr(Lcc.Managecom, 1, 4) 承保机构, "
				+ "        (Select Name "
				+ "         From Ldcom "
				+ "         Where Comcode = Substr(Lcc.Managecom, 1, 4)) 承保机构名称, "
				+ "        Lcc.Contno 保单号, "
				+ "        Lcp.Signdate 承保日期, "
				+ "        Lcp.Cvalidate 生效日, "
				+ "        Lcp.enddate 终止日期, "
				+ "        Lcp.Riskcode 产品代码, "
				+ "        Lcp.Insuredname 被保险人姓名, "
				+ "        Codename('relation', Lci.Relationtomaininsured) 与主被保险人关系, "
				+ "        Codename('idtype', Lci.Idtype) 证件类型, "
				+ "        Lci.Idno 证件号码, "
				+ "        Codename('sex', Lci.Sex) 性别, "
				+ "        Lci.Birthday 生日, "
				+ "        (Select Codename "
				+ "         From Ldcode1 "
				+ "         Where Codetype = 'highendriskI' "
				+ "         And Code = Lcp.Riskcode "
				+ "         And Code1 = Lcp.Mult) 保障计划类型, "
				+ "        (Select Codealias "
				+ "         From Ldcode1 "
				+ "         Where Codetype = 'highendriskI' "
				+ "         And Code = Lcp.Riskcode "
				+ "         And Code1 = Lcp.Mult) 承保区域 "
				+ " From Lbcont Lcc, "
				+ "      Lbpol Lcp, "
				+ "      Lbinsured Lci, "
				+ "      lpedoritem lpe, "
				+ "      lpedorapp lpp "
				+ " Where Lcc.Contno = Lcp.Contno "
				+ " And Lcp.Contno = Lci.Contno "
				+ " And Lcp.Insuredno = Lci.Insuredno "
				+ " And lcc.contno=lpe.contno "
				+ " And lpe.edorno=lpp.edoracceptno "
				+ " And lpe.edortype in ('BF','CT','WT','XT') "
				+ " And lpe.edorstate='0' "
				+ " And Lcc.Conttype = '1' "
				+ " And lpp.confdate= Current date - 1 day "
				+ " And exists (select 1 from lppol where edorno=lpe.edorno and polno=lcp.polno and edortype=lpe.edortype) "
				+ " And Lcp.Riskcode In (Select Distinct Code "
				+ "                     From Ldcode1 "
				+ "                     Where Codetype = 'highendriskI') "
				+ " union "
				+ " Select lpp.confdate 保全确认日期, "
				+ "        '保全' 操作类型, "
				+ "        (select edorname from lmedoritem where edorcode=lpe.edortype fetch first 1 rows only) 操作项目, "
				+ "        lpe.edorno 操作作业号, "
				+ "        Substr(Lcc.Managecom, 1, 4) 承保机构, "
				+ "        (Select Name "
				+ "         From Ldcom "
				+ "         Where Comcode = Substr(Lcc.Managecom, 1, 4)) 承保机构名称, "
				+ "        Lcc.Contno 保单号, "
				+ "        Lcp.Signdate 承保日期, "
				+ "        Lcp.Cvalidate 生效日, "
				+ "        Lcp.enddate 终止日期, "
				+ "        Lcp.Riskcode 产品代码, "
				+ "        Lcp.Insuredname 被保险人姓名, "
				+ "        Codename('relation', Lci.Relationtomaininsured) 与主被保险人关系, "
				+ "        Codename('idtype', Lci.Idtype) 证件类型, "
				+ "        Lci.Idno 证件号码, "
				+ "        Codename('sex', Lci.Sex) 性别, "
				+ "        Lci.Birthday 生日, "
				+ "        (Select Codename "
				+ "         From Ldcode1 "
				+ "         Where Codetype = 'highendriskI' "
				+ "         And Code = Lcp.Riskcode "
				+ "         And Code1 = Lcp.Mult) 保障计划类型, "
				+ "        (Select Codealias "
				+ "         From Ldcode1 "
				+ "         Where Codetype = 'highendriskI' "
				+ "         And Code = Lcp.Riskcode "
				+ "         And Code1 = Lcp.Mult) 承保区域 "
				+ " From Lccont Lcc, "
				+ "      Lbpol Lcp, "
				+ "      Lbinsured Lci, "
				+ "      lpedoritem lpe, "
				+ "      lpedorapp lpp "
				+ " Where Lcc.Contno = Lcp.Contno "
				+ " And Lcp.Contno = Lci.Contno "
				+ " And Lcp.Insuredno = Lci.Insuredno "
				+ " And lcc.contno=lpe.contno "
				+ " And Lcp.edorno=lpe.edorno "
				+ " And lpe.edorno=lpp.edoracceptno "
				+ " And lpe.edortype in ('CT','WT','XT') "
				+ " And lpe.edorstate='0' "
				+ " And Lcc.Conttype = '1' "
				+ " And lpp.confdate= Current date - 1 day "
				+ " And exists (select 1 from lppol where edorno=lpe.edorno and polno=lcp.polno and edortype=lpe.edortype) "
				+ " And Lcp.Riskcode In (Select Distinct Code "
				+ "                     From Ldcode1 "
				+ "                     Where Codetype = 'highendriskI') "
				+ " with ur ";
		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);

		int count = tSSRS.getMaxRow();
		mToExcel2 = new String[count + 4][30];
		mToExcel2[0][0] = "保全确认日期";
		mToExcel2[0][1] = "操作类型";
		mToExcel2[0][2] = "操作项目";
		mToExcel2[0][3] = "操作作业号";
		mToExcel2[0][4] = "承保机构";
		mToExcel2[0][5] = "承保机构名称";
		mToExcel2[0][6] = "保单号";
		mToExcel2[0][7] = "承保日期";
		mToExcel2[0][8] = "生效日期";
		mToExcel2[0][9] = "终止日期";
		mToExcel2[0][10] = "产品代码";
		mToExcel2[0][11] = "被保险人姓名";
		mToExcel2[0][12] = "与主被保险人关";
		mToExcel2[0][13] = "证件类型";
		mToExcel2[0][14] = "证件号码";
		mToExcel2[0][15] = "性别";
		mToExcel2[0][16] = "生日";
		mToExcel2[0][17] = "保障计划类型";
		mToExcel2[0][18] = "承保区域";

		if (count > 0) {
			haveBQ = true;
		}
		for (int i = 1; i <= count; i++) {
			mToExcel2[i][0] = tSSRS.GetText(i, 1);
			mToExcel2[i][1] = tSSRS.GetText(i, 2);
			mToExcel2[i][2] = tSSRS.GetText(i, 3);
			mToExcel2[i][3] = tSSRS.GetText(i, 4);
			mToExcel2[i][4] = tSSRS.GetText(i, 5);
			mToExcel2[i][5] = tSSRS.GetText(i, 6);
			mToExcel2[i][6] = tSSRS.GetText(i, 7);
			mToExcel2[i][7] = tSSRS.GetText(i, 8);
			mToExcel2[i][8] = tSSRS.GetText(i, 9);
			mToExcel2[i][9] = tSSRS.GetText(i, 10);
			mToExcel2[i][10] = tSSRS.GetText(i, 11);
			mToExcel2[i][11] = tSSRS.GetText(i, 12);
			mToExcel2[i][12] = tSSRS.GetText(i, 13);
			mToExcel2[i][13] = tSSRS.GetText(i, 14);
			mToExcel2[i][14] = tSSRS.GetText(i, 15);
			mToExcel2[i][15] = tSSRS.GetText(i, 16);
			mToExcel2[i][16] = tSSRS.GetText(i, 17);
			mToExcel2[i][17] = tSSRS.GetText(i, 18);
			mToExcel2[i][18] = tSSRS.GetText(i, 19);

		}

		return true;
	}

	private boolean getScan() {
		ExeSQL tExeSQL = new ExeSQL();
		mSql = " Select distinct Lcc.Prtno,lcc.contno  "
				+ " From Lccont Lcc, "
				+ "      Lcpol Lcp "
				+ " Where Lcc.contno = Lcp.contno "
				+ " And lcp.riskcode in (select distinct code from ldcode1 where codetype='highendriskI') "
				+ " And lcc.SignDate= Current date - 1 day "
				+ " And lcc.conttype='1' "
				+ " And exists ( select 1 from es_Doc_main where doccode=lcc.prtno and subtype='TB01' ) "
				+ " And Lcc.Appflag = '1'  ";
		System.out.println(mSql);
		String zipfilepath = "";
		tSSRS = tExeSQL.execSQL(mSql);
		int zipsize = 0;
		int count = tSSRS.getMaxRow();
		if (count == 0) {
			System.out.println("无扫描件");
		} else {
			mScanFilePath = mURL + "ScanC.zip";
			String[] sumzip = new String[count];
			String tPrtNo = "";
			String tGrpContNo = "";
			for (int i = 1; i <= count; i++) {
				tPrtNo = tSSRS.GetText(i, 1);
				tGrpContNo = tSSRS.GetText(i, 2);
				zipfilepath = mURL + tGrpContNo + ".zip";
				sumzip[i - 1] = zipfilepath;
				mSql = "select esp.PicPath||esp.PageName||'.tif',esp.PicPath||esp.PageName||'.jpg' from es_doc_main esd,es_doc_pages esp where esd.docid=esp.docid and esd.subtype='TB01' and esd.doccode='"
						+ tPrtNo + "' ";
				SSRS tSSRS2 = tExeSQL.execSQL(mSql);
				int size = tSSRS2.getMaxRow();
				if (size > 0) {
					zipsize = zipsize + 1;
					String[] zipsource = new String[size];
					for (int j = 1; j <= tSSRS2.getMaxRow(); j++) {
						zipsource[j - 1] = mURL1 + tSSRS2.GetText(j, 1);
						File mFile = new File(zipsource[j - 1]);
						if (!mFile.exists()) {
							System.out.println("文件不存在");
							zipsource[j - 1] = mURL1 + tSSRS2.GetText(j, 2);
						}
					}
					compressIntoZip(zipfilepath, zipsource);
				} else {
					continue;
				}

			}
			compressIntoZip(mScanFilePath, sumzip);
			haveScan = true;
		}

		return true;

	}

	// 主方法 测试用
	public static void main(String[] args) {
		new HighendMedicalDockITask().run();

	}
}
