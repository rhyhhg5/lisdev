package com.sinosoft.lis.taskservice;


import com.sinosoft.lis.llcase.YKTForward;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *外包批次信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class LLClaimOneCardTask extends TaskThread
{
    /**错误的容器*/

    String opr = "";
    public LLClaimOneCardTask()
    {}

    public void run()
    {
    	 YKTForward tYKTForward = new YKTForward();
    	if(!tYKTForward.runSer())
         {
    		 System.out.println("调用一卡通远程service接口批处理过程异常");
    		 opr="false";
    		 return;
         }else{
        	 System.out.println("调用一卡通远程service接口批处理任务完成");
        	 opr="true";
         }
    }

   

    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

 

	public static void main(String[] args)
    {

//        LLClaimOneCardTask tLaHumandeveITask = new LLClaimOneCardTask();
//        tLaHumandeveITask.run();
        
		LLClaimOneCardTask tYKTForward = new LLClaimOneCardTask();
		tYKTForward.run(); 
		
//        tGEdorMJTask.oneCompany(tGlobalInput);
    }
}




