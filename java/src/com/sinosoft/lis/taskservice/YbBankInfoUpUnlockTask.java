package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclientybk.T02.YbBankInfoUp;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>ClassName:  YbBankInfoUpUnlockTask</p>
 * <p>Description: 上海医保平台解锁批处理 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2017-3-21 下午05:10:17
 * @author Yu ZhiWei
 */
public class YbBankInfoUpUnlockTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    String opr = "";
    public YbBankInfoUpUnlockTask()
    {}

    public void run(){
    	YbBankInfoUp  ybkInfoUp = new YbBankInfoUp();
    	/*
    	String SQL = "select lcc.prtno from lccont lcc, lcpol lcp, lccontsub lcs, YBK_G03_LIS_ResponseInfo ybg" + 
    	" where lcc.prtno = lcp.prtno and lcc.prtno = lcs.prtno and lcs.ybkserialno = ybg.PolicyFormerNo and lcc.appflag not in ('1')" + 
    	" and lcp.riskcode in (select code from ldcode where codetype = 'ybkriskcode') and lcc.prtno in (select businessno from YBKErrorList where transtype = 'G03')" + 
    	" and ybg.MedicalIfSucess = '0' and ybg.FailReason not in ('08') and not exists (select 1 from YBKErrorList where businessno = lcp.prtno and resultstatus = '1' and transtype = 'T02')" + 
    	" union select lcc.prtno from lccont lcc, lcpol lcp, lccontsub lcs, YBK_G03_LIS_ResponseInfo ybg where lcc.prtno = lcp.prtno and lcc.prtno = lcs.prtno and lcs.ybkserialno = ybg.PolicyFormerNo" + 
    	" and lcp.riskcode in (select code from ldcode where codetype = 'ybkriskcode') and lcc.appflag not in ('1') and lcs.paymode = '2' and ybg.MedicalIfSucess = '1' and ybg.ActualDecuctPremium <= 0" + 
    	" and ybg.FailReason not in ('08') and not exists (select 1 from YBKErrorList where businessno = lcp.prtno and resultstatus = '1' and transtype = 'T02')" + 
    	" union select lcc.prtno from lccont lcc, lcpol lcp, lccontsub lcs, YBK_G03_LIS_ResponseInfo ybg, ljtempfee ljt where lcc.prtno = lcp.prtno and lcc.prtno = lcs.prtno and lcs.ybkserialno = ybg.PolicyFormerNo" + 
    	" and lcc.prtno = ljt.otherno and lcc.appflag not in ('1') and lcp.riskcode in (select code from ldcode where codetype = 'ybkriskcode') and lcs.paymode = '1' and ybg.MedicalIfSucess = '1' and ybg.ActualDecuctPremium <= 0" + 
    	" and ybg.FailReason not in ('08') and lcc.prtno in (select businessno from YBKErrorList where transtype = 'G03' and days(current date) - days(makedate) > 15) and ljt.enteraccdate is null and not exists (select 1 from YBKErrorList where businessno = lcp.prtno and resultstatus = '1' and transtype = 'T02')" + 
    	" union select lcc.prtno from lccont lcc, lcpol lcp, lccontsub lcs, ljtempfee ljt, YBK_G03_LIS_ResponseInfo ybg  where lcc.prtno = lcp.prtno and lcc.prtno = lcs.prtno and lcc.prtno = ljt.otherno and lcs.ybkserialno = ybg.PolicyFormerNo and lcc.appflag not in ('1')" + 
    	" and lcp.riskcode in (select code from ldcode where codetype = 'ybkriskcode') and lcs.paymode = '3' and ybg.MedicalIfSucess = '1' and ybg.ActualDecuctPremium <= 0 and ybg.FailReason not in ('08') and lcc.prtno in (select businessno from YBKErrorList" + 
    	" where transtype = 'G03' and days(current date) - days(makedate) > 15) and ljt.enteraccdate is null and not exists (select 1 from YBKErrorList where businessno = lcp.prtno and resultstatus = '1' and transtype = 'T02')";
    	*/
    	String SQL = "select lcc.prtno "
	    			+" from lccont lcc, lcpol lcp, lccontsub lcs, YBK_G03_LIS_ResponseInfo ybg, ljtempfee ljt "
	    			+" where lcc.prtno = lcp.prtno "
	    			+" and lcc.prtno = lcs.prtno "
	    			+" and lcs.ybkserialno = ybg.PolicyFormerNo "
	    			+" and lcc.prtno = ljt.otherno "
	    			+" and lcc.appflag not in ('1') " 
	    			+" and ljt.enteraccdate is null "
	    			+" and lcp.riskcode in (select code from ldcode where codetype = 'ybkriskcode') "
	    			+" and lcs.paymode in ('1') "
	    			+" and ybg.MedicalIfSucess = '0' "
	    			+" and ybg.ActualDecuctPremium <= 0 "
	    			+" and (ybg.FailReason <> '08' or ybg.FailReason = '' or ybg.FailReason is null) "
	    			+" and exists (select 1 from YBKErrorList where transtype = 'G03' and businessno=lcc.prtno and days(current date) - days(makedate) > 15) "
	    			+" and not exists (select 1 from YBKErrorList where businessno = lcp.prtno and resultstatus = '1' and transtype = 'T02') "
	    			+" union "
	    			+" select lcc.prtno "
	    			+" from lccont lcc, lcpol lcp, lccontsub lcs, YBK_G03_LIS_ResponseInfo ybg, ljtempfee ljt "
	    			+" where lcc.prtno = lcp.prtno "
	    			+" and lcc.prtno = lcs.prtno "
	    			+" and lcs.ybkserialno = ybg.PolicyFormerNo "
	    			+" and lcc.prtno = ljt.otherno "
	    			+" and lcc.appflag not in ('1') "
	    			+" and ljt.enteraccdate is null "
	    			+" and lcp.riskcode in (select code from ldcode where codetype = 'ybkriskcode') "
	    			+" and lcs.paymode in ('3') "
	    			+" and ybg.MedicalIfSucess = '1' "
	    			+" and ybg.ActualDecuctPremium <= 0 "
	    			+" and (ybg.FailReason <> '08' or ybg.FailReason = '' or ybg.FailReason is null) "
	    			+" and exists (select 1 from YBKErrorList where transtype = 'G03' and businessno=lcc.prtno and days(current date) - days(makedate) > 15) "
	    			+" and not exists (select 1 from YBKErrorList where businessno = lcp.prtno and resultstatus = '1' and transtype = 'T02') "
	    			+" with ur";
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++){
    			String tCaseNo = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("PrtNo", tCaseNo);
    			tVData.add(tTransferData);
    	    	if(!ybkInfoUp.submitData(tVData, "YBKPT")){
    	    		 System.out.println("银行卡缴费信息上传批处理出问题了-->解锁失败");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             continue;
    	         }else{
    	        	 System.out.println("银行卡缴费信息上传批处理成功了-->解锁成功");
    	        	 opr ="ture";
    	         }
    		}
    	}

    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
   
    public static void main(String[] args){
    	YbBankInfoUpUnlockTask bankInfoUpUnlockTask = new YbBankInfoUpUnlockTask();
    	bankInfoUpUnlockTask.run();
    	
    	/*String SQL = "select lcc.prtno from lccont lcc, lcpol lcp, lccontsub lcs, YBK_G03_LIS_ResponseInfo ybg, ljtempfee ljt where lcc.prtno = lcp.prtno and lcc.prtno = lcs.prtno" + 
    	" and lcs.ybkserialno = ybg.PolicyFormerNo and lcc.prtno = ljt.otherno and lcc.appflag not in ('1') and ljt.enteraccdate is null" + 
    	" and lcp.riskcode in (select code from ldcode where codetype = 'ybkriskcode') and lcs.paymode in ('1') and ybg.MedicalIfSucess = '0'" + 
    	" and ybg.ActualDecuctPremium <= 0 and (ybg.FailReason <> '08' or ybg.FailReason = '' or ybg.FailReason is null) and lcc.prtno in (select businessno from YBKErrorList where transtype = 'G03' and days(current date) - days(makedate) > 15)" + 
    	" and not exists (select 1 from YBKErrorList where businessno = lcp.prtno and resultstatus = '1' and transtype = 'T02')" + 
    	" union select lcc.prtno from lccont lcc, lcpol lcp, lccontsub lcs, YBK_G03_LIS_ResponseInfo ybg, ljtempfee ljt where lcc.prtno = lcp.prtno" + 
    	" and lcc.prtno = lcs.prtno and lcs.ybkserialno = ybg.PolicyFormerNo and lcc.prtno = ljt.otherno and lcc.appflag not in ('1')" + 
    	" and ljt.enteraccdate is null and lcp.riskcode in (select code from ldcode where codetype = 'ybkriskcode') and lcs.paymode in ('3') and ybg.MedicalIfSucess = '1' and ybg.ActualDecuctPremium <= 0" + 
    	" and (ybg.FailReason <> '08' or ybg.FailReason = '' or ybg.FailReason is null) and lcc.prtno in (select businessno from YBKErrorList where transtype = 'G03' and days(current date) - days(makedate) > 15)" + 
    	" and not exists (select 1 from YBKErrorList where businessno = lcp.prtno and resultstatus = '1' and transtype = 'T02')";
    	System.out.println(SQL);
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	int maxRow = tSSRS.getMaxRow();
    	System.out.println(maxRow);*/
    }
}




