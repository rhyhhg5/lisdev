package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.lis.ygz.GetBqGetMoneySeparateBL;
import com.sinosoft.lis.ygz.GetBqMoneyNoBL;
import com.sinosoft.lis.ygz.GetBqPremBillBL;
import com.sinosoft.lis.ygz.GetBqTempFeeSeparateBL;
import com.sinosoft.lis.ygz.GetPayNoBL;
import com.sinosoft.lis.ygz.GetTempFeeSeparateBL;
import com.sinosoft.lis.ygz.GetXqPremBillBL;
import com.sinosoft.lis.ygz.GetXqTempFeeSeparateBL;
import com.sinosoft.lis.ygz.GetYjGLMoneyBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: LCContTranfOutTask </p>
 * <p>Description: 保单迁出同步平台数据的批处理 </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class YjGLGetTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	
	public YjGLGetTask() {

	}
	
	/**
	 * run方法
	 */
	public void run(){
		System.out.println("======================YjBqGetTask Execute  !!!======================");

		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		tInputData.add(tTransferData);

		try {
			/**
			 * 月结、保全管理费数据提取
			 */
			GetYjGLMoneyBL tGetYjGLMoneyBL = new GetYjGLMoneyBL();
			if(!tGetYjGLMoneyBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetYjGLMoneyBL.mErrors);
				System.out.println("月结和保全管理费生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("月结和保全管理费生成价税分离数据报错：");
			e.printStackTrace();
		}
		
	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		YjGLGetTask tYjGLGetTask = new YjGLGetTask();
		
		tYjGLGetTask.run();
	}
	
}
