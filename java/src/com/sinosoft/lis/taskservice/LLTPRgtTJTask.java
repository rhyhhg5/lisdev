package com.sinosoft.lis.taskservice;


import com.sinosoft.lis.yibaotong.LLTJTransFtpXml;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *天津大病批次信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Li JinKe
 * @version 1.1
 */
public class LLTPRgtTJTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

	private ExeSQL mExeSQL = new ExeSQL();
    String opr = "ture";
    public LLTPRgtTJTask()
    {}

    public void run()
    {
		LLTJTransFtpXml  tLLTJTransFtpXml = new LLTJTransFtpXml();
	
		if(!tLLTJTransFtpXml.submitData())
         {
    		 System.out.println("批次信息导入出问题了");
             opr ="false";
         }else{
        	 System.out.println("批次信息导入成功了");
        	 opr="true";
         }
	}
   
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	public static void main(String[] args)
    {

        LLTPRgtTJTask tLLTPRgtTJTask = new LLTPRgtTJTask();
        tLLTPRgtTJTask.run();
//        tGEdorMJTask.oneCompany(tGlobalInput);
    }
}




