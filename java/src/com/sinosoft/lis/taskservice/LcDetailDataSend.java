package com.sinosoft.lis.taskservice;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class LcDetailDataSend extends TaskThread{

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = new GlobalInput();

	/** 文件路径 */
	private String tOutXmlPath = "";

	/** 应用路径 */
	private String mURL = "";

	/** 应用路径 */
	private String mURL1 = "";

	/** 压缩文件的路径 */
	//private String mZipFilePath;

	private String fileName = "";

	String[][] mToExcel = null;

	String[][] mToExcel2 = null;
	
	String[][] mToExcel3 = null;
	
	SSRS tSSRS = new SSRS();

	private String mSql = null;

	private boolean haveCB = false;

	private boolean haveBQ = false;

	private static final int BUFFER = 2048;
	


	// 执行任务
	public void run() {
		dealData();
	}

	public boolean dealData() {
		System.out.println("开始执行批处理:" + PubFun.getCurrentTime());
		// 先准备数据
		getInputData();

		// 打印报表

		getReprot();
		
		// 发送邮件
		SendEmail();

		System.out.println("批处理执行完毕:" + PubFun.getCurrentTime());

		return true;
	}

	/**
	 * ] 判断文件是否存在
	 * 
	 * @param filePath
	 * @return
	 */
	private boolean FileExists(String filePath) {
		File reportFile = new File(filePath);
		if (!reportFile.exists()) {
			return false;
		}
		return true;
	}

	/**
	 * 将报表查询文件发送邮件
	 * 
	 */
	private boolean sendSagentMail() {
		// 压缩文件生成成功，开始发送邮件
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
		SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
				.GetText(1, 2), "picchealth");
		// 设置邮件发送信息
		String sendInf = "契约承保明细数据和汇总数据见附件！";
		String sendInfs = "";
		if (haveCB && haveBQ) {
			sendInfs = "";
		} else  {
			sendInfs =  "承保无数据";
		}
		String Fj = tOutXmlPath;

		tMailSender.setSendInf("福利双全报送数据", sendInf + sendInfs, Fj);
		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
		tMailSender.setToAddress("DetailData");
		if (!tMailSender.sendMail()) {
			tMailSender.getErrorMessage();
		}
	     deleteFile(tOutXmlPath);
		//deleteFile(mZipFilePath);

		return true;

	}

	/**
	 * 删除生成的XML文件
	 * 
	 * @param cFilePath
	 *            String：完整文件名
	 */
	private boolean deleteFile(String cFilePath) {
		File f = new File(cFilePath);
		if (!f.exists()) {
			CError tError = new CError();
			tError.moduleName = "MonthDataCatch";
			tError.functionName = "deleteFile";
			tError.errorMessage = "没有找到需要删除的文件";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		f.delete();
		return true;
	}

	private void sendMessage() {

		// 邮箱发送
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
		SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
				.GetText(1, 2), "picchealth");
		// 设置邮件发送信息
		String sendInf = "无数据，请知悉~~";
		tMailSender.setSendInf("核心提取福利双全保单数据", sendInf, null);
		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
		tMailSender.setToAddress("DetailData");
		if (!tMailSender.sendMail()) {
			tMailSender.getErrorMessage();
		}

	}

	private boolean SendEmail() {
		String name = tOutXmlPath;
		if (FileExists(name)) {
			System.out.println("打印文件存在");
			//getZipFile();
			tOutXmlPath = mURL + fileName;//.replace("xls", "zip");
			sendSagentMail();
		} else {
			sendMessage();
			System.out.println("没有符合打印条件的数据");
		}

		return true;
	}

	private boolean getReprot() {
		Calendar   c   =   Calendar.getInstance();   
		c.add(Calendar.DAY_OF_MONTH, -1);  
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		String mDateTime=formatter.format(c.getTime());
		String strStart=mDateTime.substring(0, 10);

 		fileName = "LcDetailData.xls";
		tOutXmlPath = mURL + fileName;
		System.out.println("文件名称====：" + tOutXmlPath);
		WriteToExcel t = new WriteToExcel(fileName);
		t.createExcelFile();
		t.setAlignment("CENTER");
		t.setFontName("宋体");
		String[] sheetName = { new String("2016-1-1到"+strStart+"承保明细数据"), new String(strStart+"汇总数据") , new String("2016-1-1到"+strStart+"承保汇总数据") };
		t.addSheetGBK(sheetName);
		if (!getData1()) {
			mToExcel = getDefalutData();
		}
		t.setData(0, mToExcel);

		if (!getData2()) {
			mToExcel2 = getDefalutData();
		}
	      t.setData(1, mToExcel2);
	      
	  	if (!getData3()) {
			mToExcel3 = getDefalutData();
		}
	      t.setData(2, mToExcel3);

		if (haveBQ || haveCB) {
			try {
				t.write(mURL);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		System.out.println("生成文件完成");

		return true;

	}

	private boolean getData3() {
		System.out.println("BL->dealDate()");

		ExeSQL tExeSQL = new ExeSQL();
		mSql = " Select substr(lc.managecom,1, 4 )省级机构代码, "
			+ "       (select name from ldcom where comcode=substr(lc.managecom,1,4)) 省级机构名称, "
			+ "        count ( DISTINCT lc.contno  )  保单数, "
			+ "        count(DISTINCT  lc.appntno) 投保人数量   "
			+ " From Lccont Lc"
			+ "     inner join lcpol lp on lc.contno=lp.contno "
			+ " Where  1 = 1 "
			+ " And lc.signdate between '2016-1-1' and  current date - 1 days"// '2016-01-25'  
			+ " And lc.conttype='1' "
			+ " And lc.appflag='1' "
			+ " And lp.riskcode in ('333701','334001','333702')"
			+ "group by substr(lc.managecom,1, 4 ) "
			+ " with ur";
		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);
		int count = tSSRS.getMaxRow();
		mToExcel3 = new String[count + 4][30];
		mToExcel3[0][0] = "省级机构代码";
		mToExcel3[0][1] = "省级机构名称";
		mToExcel3[0][2] = "保单数";
		mToExcel3[0][3] = "投保人数量";
		if (count > 0) {
			haveBQ = true;
		}
		for (int i = 1; i <= count; i++) {
			mToExcel3[i][0] = tSSRS.GetText(i, 1);
			mToExcel3[i][1] = tSSRS.GetText(i, 2);
			mToExcel3[i][2] = tSSRS.GetText(i, 3);
			mToExcel3[i][3] = tSSRS.GetText(i, 4);
		}

		return true;
	}

	private void getInputData() {
		// 设置管理机构
		mGI.ManageCom = "86";
		mGI.Operator = "001";
		mGI.ComCode = "86";

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL1 = new ExeSQL().getOneValue(tSQL);
	//mURL1 = "D:/";
		mURL = mURL1 + "DetailData/";

		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}

	}

	private String[][] getDefalutData() {
		String[][] tToExcel = new String[2][30];
		tToExcel[0][0] = "无数据";
		return tToExcel;
	}

	public boolean getData1() {
		System.out.println("BL->dealDate()");

		ExeSQL tExeSQL = new ExeSQL();
	   	mSql = "Select substr(lc.managecom,1, 4 )省级机构代码, "
	   		    + " (select name from ldcom where comcode=substr(lc.managecom,1,4)) 省级机构名称,           "
			    + "        lc.managecom 机构代码,  "
				+ "        (select name from ldcom where comcode=lc.managecom) 机构名称, "
				+ "        db2inst1.codename('lcsalechnl',lc.salechnl) 渠道, "
				+ "        lc.contno 保单号, "
				+ "        lp.riskcode 险种代码, "
				+ "        (select riskname from lmriskapp where riskcode=lp.riskcode) 险种名称, "
				+ "        lc.amnt 保额,"
				+ "        lc.prem 保费, "
				+ "        lc.cvalidate 保单生效日, "
				+ "        lc.signdate 保单签单日, "
				+ "        lca.appntno 投保人客户号, "
				+ "        lca.appntname 投保人姓名, "
				+ "        db2inst1.codename('sex',lca.appntsex) 投保人性别, "
				+ "        db2inst1.codename('idtype',lca.idtype) 投保人证件类型, "
				+ "        lca.idno 投保人证件号, "
				+ "        (select mobile from lcaddress where customerno=lca.appntno and addressno=lca.addressno) 联系电话, "
				+ "        lci.name 被保人姓名, "
				+ "        db2inst1.codename('idtype',lci.idtype) 被保人证件类型, "
				+ "        lci.idno 被保人证件号, "
				+ "       (select branchattr from labranchgroup where agentgroup=lc.agentgroup) 营业单位代码, "
				+ "        (select groupagentcode from laagent where agentcode=lc.agentcode) 代理人代码,"
				+ "        (select name from laagent where agentcode=lc.agentcode) 代理人姓名, "
				+ "       (select mobile from laagent where agentcode=lc.agentcode) 代理人电话"
				+ "        from lccont lc "
				+ "        inner join lcappnt lca on lc.contno=lca.contno "
				+ "        inner join lcpol lp on lc.contno=lp.contno "
				+ "      inner join lcinsured lci on lp.contno = lci.contno and lp.insuredno = lci.insuredno"
				+ " where 1=1"
				+ " and lc.signdate between '2016-1-1' and  current date - 1 days"// ='2016-01-26' 
				+ " and lc.conttype='1' "
				+ " and lc.appflag='1'"
				+ "and lp.riskcode in ('333701','334001','333702')"
				+ " order by lc.managecom,lc.salechnl,lc.signdate"
				 
				+ " with ur ";

		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);

		int count = tSSRS.getMaxRow();
		mToExcel = new String[count + 4][30];
		    mToExcel[0][0] = "省级机构代码";
			mToExcel[0][1] = "省级机构名称";
		    mToExcel[0][2] = "机构代码";
			mToExcel[0][3] = "机构名称";
			mToExcel[0][4] = "渠道";
			mToExcel[0][5] = "保单号";
			mToExcel[0][6] = "险种代码";
			mToExcel[0][7] = "险种名称"; 
			mToExcel[0][8] = "保额";
			mToExcel[0][9] = "保费";
			mToExcel[0][10]= "保单生效日";
			mToExcel[0][11] = "保单签单日";
			mToExcel[0][12] = "投保人客户号";
			mToExcel[0][13] = "投保人姓名";
			mToExcel[0][14] = "投保人性别";
			mToExcel[0][15] = "投保人证件类型";
			mToExcel[0][16] = "投保人证件号";
			mToExcel[0][17] = "联系电话";
			mToExcel[0][18] = "被保人姓名";
			mToExcel[0][19] = "被保人证件类型";
			mToExcel[0][20] = "被保人证件号码";
			mToExcel[0][21] = "营业单位代码";
			mToExcel[0][22] = "代理人代码";
			mToExcel[0][23] = "代理人姓名";
			mToExcel[0][24] = "代理人电话";
		if (count > 0) {
			haveCB = true;
		}

		for (int i = 1; i <= count; i++) {
			mToExcel[i][0] = tSSRS.GetText(i, 1);
			mToExcel[i][1] = tSSRS.GetText(i, 2);
			mToExcel[i][2] = tSSRS.GetText(i, 3);
			mToExcel[i][3] = tSSRS.GetText(i, 4);
			mToExcel[i][4] = tSSRS.GetText(i, 5);
			mToExcel[i][5] = tSSRS.GetText(i, 6);
			mToExcel[i][6] = tSSRS.GetText(i, 7);
			mToExcel[i][7] = tSSRS.GetText(i, 8);
			mToExcel[i][8] = tSSRS.GetText(i, 9);
			mToExcel[i][9] = tSSRS.GetText(i, 10);
			mToExcel[i][10] = tSSRS.GetText(i, 11);
			mToExcel[i][11] = tSSRS.GetText(i, 12);
			mToExcel[i][12] = tSSRS.GetText(i, 13);
			mToExcel[i][13] = tSSRS.GetText(i, 14);
			mToExcel[i][14] = tSSRS.GetText(i, 15);
			mToExcel[i][15] = tSSRS.GetText(i, 16);
			mToExcel[i][16] = tSSRS.GetText(i, 17);
			mToExcel[i][17] = tSSRS.GetText(i, 18);
			mToExcel[i][18] = tSSRS.GetText(i, 19);
			mToExcel[i][19] = tSSRS.GetText(i, 20);
			mToExcel[i][20] = tSSRS.GetText(i, 21);
			mToExcel[i][21] = tSSRS.GetText(i, 22);
			mToExcel[i][22] = tSSRS.GetText(i, 23);
			mToExcel[i][23] = tSSRS.GetText(i, 24);
		}

		return true;
	}

public boolean getData2() {
		System.out.println("BL->dealDate()");

		ExeSQL tExeSQL = new ExeSQL();
		mSql = " Select substr(lc.managecom,1, 4 )省级机构代码, "
				+ "       (select name from ldcom where comcode=substr(lc.managecom,1,4)) 省级机构名称, "
				+ "        count ( DISTINCT lc.contno  )  保单数, "
				+ "        count(DISTINCT  lc.appntno) 投保人数量   "
				+ " From Lccont Lc"
				+ "     inner join lcpol lp on lc.contno=lp.contno "
				+ " Where  1 = 1 "
				+ " And lc.signdate =current date - 1 days"// '2016-01-25'  
				+ " And lc.conttype='1' "
				+ " And lc.appflag='1' "
	//+ " And lc.salechnl in ('01','10','04','13','14','15','17')"
				+ " And lp.riskcode in ('333701','334001','333702')"
				+ "group by substr(lc.managecom,1, 4 ) "
				+ " with ur";
		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);

		int count = tSSRS.getMaxRow();
		mToExcel2 = new String[count + 4][30];
		mToExcel2[0][0] = "省级机构代码";
		mToExcel2[0][1] = "省级机构名称";
		mToExcel2[0][2] = "保单数";
		mToExcel2[0][3] = "投保人数量";
		if (count > 0) {
			haveBQ = true;
		}
		for (int i = 1; i <= count; i++) {
			mToExcel2[i][0] = tSSRS.GetText(i, 1);
			mToExcel2[i][1] = tSSRS.GetText(i, 2);
			mToExcel2[i][2] = tSSRS.GetText(i, 3);
			mToExcel2[i][3] = tSSRS.GetText(i, 4);
		}

		return true;
	}
	// 主方法 测试用
	public static void main(String[] args) {
		new LcDetailDataSend().run();

	}
}
