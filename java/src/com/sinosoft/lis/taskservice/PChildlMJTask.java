package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.operfee.ExpirBenefitBatchUI;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.operfee.IndiLJSCancelBL;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.operfee.NewPChildMJDealBL;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PChildlMJTask extends TaskThread {

    private static final int STEP_DAYS = 15;

    public CErrors mErrors = new CErrors();

    private String mCurrentDate = PubFun.getCurrentDate();

    private GlobalInput mGlobalInput = null;

    private VData mVData = new VData();

    String Content ="" ;

    public PChildlMJTask() {
        mGlobalInput = new GlobalInput();
        mGlobalInput.Operator = "Server";
        mGlobalInput.ComCode = "86";
        mGlobalInput.ManageCom = "86";
    }

    public void run()
    {

        NewPChildMJDealBL tNewPChildMJDealBL = new NewPChildMJDealBL();
        mVData.add(mGlobalInput);
        tNewPChildMJDealBL.submitData(mVData,"");
    }

    public static void main(String[] args)
    {
        PChildlMJTask tPChildlMJTask = new PChildlMJTask();
        tPChildlMJTask.run();
    }
}
