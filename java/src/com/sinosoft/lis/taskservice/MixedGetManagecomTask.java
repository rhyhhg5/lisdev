package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.io.*;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import java.net.SocketException;

/**
 * <p>
 * Title:MixedAgentTask.java
 * </p>
 * 
 * <p>
 * Description:集团交叉销售批处理
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company:sinosoft
 * </p>
 * 
 * @author huxl
 * @version 1.0
 */
public class MixedGetManagecomTask {
	public String mCurrentDate = PubFun.getCurrentDate();

	private FileWriter mFileWriter;

	private BufferedWriter mBufferedWriter;

	private String mURL = "";

	public MixedGetManagecomTask() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void run(String managecom) {
		System.out.println("开始提取" + this.mCurrentDate + "日的交叉销售数据,开始时间是"
				+ PubFun.getCurrentTime());
		if (!getMixedManageCom(managecom)) {
			return;
		}
		System.out.println("提取" + this.mCurrentDate + "日的交叉销售数据完成,完成时间是"
				+ PubFun.getCurrentTime());
	}

	private boolean getMixedManageCom(String managecom) {
		getS001(managecom);

		return true;
	}

	/**
	 * 营销员信息表
	 * 
	 * @return
	 */
	private String getS001(String managecom) {
		String strCont = "";
		String strContent = "";
		try {
			String tSQL = "select 'P','I','000002',comcode,'D001', comcode, "
					+ " '"+PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+"' 子公司报送时间,'' 时间戳 "
					+ "from ldcom "
					+ "where comcode = '"
					+ managecom
					+ "' with ur";

			SSRS tSSRS = new ExeSQL().execSQL(tSQL);
			if (tSSRS.getMaxRow() <= 0) {
				return null;
			}
			String tempString = tSSRS.encode();
			String[] tempStringArr = tempString.split("\\^");
			System.out.println(tempStringArr);

			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				String t = tempStringArr[i].replaceAll("\\|", "\\-");
				t = t.substring(0, t.length() - 1);
				strCont = t;
			}
			
			String mSQL = "select comcode 管理机构ID,'000085' 子公司代码, '中国人民健康保险股份有限公司' 子公司名称, "
				+ " (select codealias from ldcode where codetype='managearea' and code=substr(a.comcode,1,4))区域代码," 
				+ "(select codename from ldcode where codetype='managearea' and code=substr(a.comcode,1,4))区域名称, "
				+ " substr(a.comcode,1,4)省级分公司代码,(select name from ldcom where comcode=substr(a.comcode,1,4))省级分公司名称,"
				+ " comcode 地市级分公司代码,(select name from ldcom where comcode=substr(a.comcode,1,8)) 地市级分公司名称," 
                + " comcode 县支级分公司代码,(select name from ldcom where comcode=substr(a.comcode,1,8)) 县支级分公司名称," 
                + " comcode 代理机构代码,(select name from ldcom where comcode=substr(a.comcode,1,8)) 代理机构名称," 
                + " case when substr(a.comcode,5,4) = '0000' then '1' when substr(a.comcode,7,2) = '00' then '2' else '3' end 机构等级,"
                + " '' 起始日期,'' 终止日期,a.sign 状态,case when a.sign='1' then '正常' else '终止' end 状态说明,"
                + " (select codealias from ldcode where codetype='managecode' and code=substr(a.comcode,1,4)) 省归集代码,"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                + " (select codename from ldcode where codetype='managecode' and code=substr(a.comcode,1,4)) 省归集名称,"  
                //按修改要求添加市一级的归集机构,县一级的暂时为空
                + " (select codealias from ldcode where codetype='managecode' and code=a.comcode) 市级归集代码,"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                + " (select codename from ldcode where codetype='managecode' and code=a.comcode) 市级归集名称,"  
                + " '' 县级归集代码,"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                + " '' 县级归集名称,"  
                + " 'I' 数据变更类型,db2inst1.ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间 ,'' 时间戳," 
                + " ''地域代码,"
                + " '' 第五级机构代码,"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                + " '' 第五级机构名称,'000085'交叉销售权限,'0'校验标志,current date 校验时间" 
                + " from ldcom a where ComGrade = '03' and ComCode = '"+managecom+"'  " 
                //+ " and ModifyDate = current date - 1 day " 
                + " order by a.comcode with ur";

		SSRS mSSRS = new ExeSQL().execSQL(mSQL);
		if (mSSRS.getMaxRow() <= 0) {
			return null;
		}
		String tempStringCont = mSSRS.encode();
		String[] tempStringContArr = tempStringCont.split("\\^");
		System.out.println(tempStringContArr);

		for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
			String m = tempStringContArr[i].replaceAll("\\|", "','");
			String[] mArr = m.split("\\,");
			mArr[3] = mArr[3].substring(1, mArr[3].length() - 1);
			System.out.println(tempStringContArr[i]);
			strContent += tempStringContArr[i];
		}
		} catch (Exception ex2) {
			System.out.println(ex2.getStackTrace());
		}
		String strManagecomCont = strCont + ":" + strContent;
		System.out.println(strManagecomCont);
		return strManagecomCont;
	}

	public static void main(String args[]) {
		MixedGetManagecomTask tMixedAgentTask = new MixedGetManagecomTask();
		tMixedAgentTask.run("86110000");
		return;
	}

	private void jbInit() throws Exception {
	}

}
