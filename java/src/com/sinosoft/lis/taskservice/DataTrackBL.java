package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.DataInfointerfaceSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class DataTrackBL {
	


		    /** 错误处理类，每个需要错误处理的类中都放置该类 */
		    public CErrors mErrors = new CErrors();

		    /** 前台传入的公共变量 */
		    private GlobalInput globalInput = new GlobalInput();


		    /** 往后面传输数据的容器 */
//		    private VData mInputData = new VData();

		    /** 数据操作字符串 */
		    private String mDataInfoKey = "";
		    private String mDataType = "";
		    private String mState="";

		    private TransferData mGetCessData = new TransferData();

		    private MMap tmap = new MMap();
		    private VData mOutputData = new VData();



		    //业务处理相关变量
		    /** 全局数据 */

		    public DataTrackBL() {
		    }

		   /**
		    * 
		    * @param mDataInfoKey
		    * @param mDataType
		    * @param mState
		    * @return
		    */
		    public boolean submitData(String mDataInfoKey, String mDataType,String mState) {
		        System.out.println("Begin DataTrackBL.Submit..............");
		      
		        if (!getInputData(mDataInfoKey,mDataType,mState)) {
		            return false;
		        }
		        if (!dealData()) {
		            return false;
		        }
		        //准备往后台的数据
		        if (!prepareOutputData())
		        {
		            return false;
		        }

		        PubSubmit tPubSubmit = new PubSubmit();
		        tPubSubmit.submitData(mOutputData, "");
		        //如果有需要处理的错误，则返回
		        if (tPubSubmit.mErrors.needDealError())
		        {
		            // @@错误处理
		            CError tError = new CError();
		            tError.moduleName = "DataTrackBL";
		            tError.functionName = "submitDat";
		            tError.errorMessage = "数据提交失败!";
		            this.mErrors.addOneError(tError);
		            return false;
		        }
		        
		        return true;
		    }


		    /**
		     * 根据前面的输入数据，进行UI逻辑处理
		     * 如果在处理过程中出错，则返回false,否则返回true
		     */
		    private boolean dealData() {

		        
		            if (!insertPolData()) {
		                return false;
		            }
		        
		        return true;
		    }

		   
		    
		     /**
		     * 准备后台的数据
		     * @return boolean
		     */
		    private boolean prepareOutputData()
		    {
		        try
		        {

		        	this.mOutputData.clear();
		            this.mOutputData.add(tmap);
		        }
		        catch (Exception ex)
		        {
		            // @@错误处理
		            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
		            return false;
		        }
		        return true;
		    }
		    //执行SQL,并将取出的数据插入到:
		    private boolean insertPolData(){
		    	String currentdate=PubFun.getCurrentDate();
		    	String currenttime=PubFun.getCurrentTime();
//		    	String sql[] = new String[6];
		    	String sql = null;

	    		
		    	try{
		             	tmap = new MMap();
		     			if (mDataInfoKey != null || !"".equals(mDataInfoKey)) {

		        	
		     			DataInfointerfaceSchema tDataInfointerfaceSchema = new DataInfointerfaceSchema();
		     			String tSerialNo=PubFun1.CreateMaxNo("DataTrack", 20);
		     			tDataInfointerfaceSchema.setSerialNo(tSerialNo);
		     			tDataInfointerfaceSchema.setDataInfoKey(mDataInfoKey);
		     			tDataInfointerfaceSchema.setDate(currentdate); 
		     			tDataInfointerfaceSchema.setState(mState);
		     			tDataInfointerfaceSchema.setDataType(mDataType);
		     			tDataInfointerfaceSchema.setManageCom("8611"); 
		       			tDataInfointerfaceSchema.setMakeDate(currentdate); 
		     			tDataInfointerfaceSchema.setMakeTime(currenttime);  
		     			tDataInfointerfaceSchema.setModifyDate(currentdate);  
		     			tDataInfointerfaceSchema.setModifyTime(currenttime);  
		     			tDataInfointerfaceSchema.setOprater("Server");  
		        		
		        		
		        		tmap.put(tDataInfointerfaceSchema, "DELETE&INSERT");

		        	 if (!prepareOutputData()) {
		                    return false;
		                }
		        	 PubSubmit tPubSubmit = new PubSubmit();
				     tPubSubmit.submitData(mOutputData, "");
				     //如果有需要处理的错误，则返回
				     if (tPubSubmit.mErrors.needDealError())
				       {
				            // @@错误处理
				            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				            CError tError = new CError();
				            tError.moduleName = "DataTrackBL";
				            tError.functionName = "submitDat";
				            tError.errorMessage = "数据提交失败!";
				            this.mErrors.addOneError(tError);
				            return false;
				        }
		     			}
		     			
		     		
		    		 
		        	
		    	}catch(Exception ex){
		    		ex.printStackTrace();
		    		return false;
		    	}
//	    		}
		    	
		    	
		    	return true;
		    }
		    
		    //错误信息
		    private void buildError(String szFunc, String szErrMsg) {
		        CError cError = new CError();
		        cError.moduleName = "DataTrackBL";
		        cError.functionName = szFunc;
		        cError.errorMessage = szErrMsg;
		        this.mErrors.addOneError(cError);
		    }

		    
		    private boolean getInputData(String mDataInfoKey, String mDataType,String mState) {
		       
		       this.mDataInfoKey = mDataInfoKey;
		       this.mDataType = mDataType;
		       this.mState = mState;
		       
		        
		        return true;
		    }
		    /**
		     * 从输入数据中得到所有对象
		     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
		     */

		    public String getResult() {
		        return "没有传数据";
		    }
		    

		    public static void main(String[] args) {
		    	DataTrackBL mDataTrackBL=new DataTrackBL();
		    	mDataTrackBL.submitData("00129876", "AC008", "8611");
		    }
}
