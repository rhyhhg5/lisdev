package com.sinosoft.lis.taskservice;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.operfee.GrpDueFeeMultiBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.xb.GRnewDueFeeUI;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GRnewDueFeeBatchTask extends TaskThread {

	public CErrors mErrors = new CErrors();

	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	private String end ="";

	private GlobalInput mGI = new GlobalInput();
    private SSRS mSSRS = new SSRS();
	public GRnewDueFeeBatchTask() {
		mGI.Operator = "001";
		mGI.ComCode = "86";
		mGI.ManageCom = mGI.ComCode;
	}

	public GRnewDueFeeBatchTask(GlobalInput cGlobalInput) {
		mGI = cGlobalInput;
	}

	public void run() {

		Date date = new Date();
		SimpleDateFormat s = new SimpleDateFormat("yyyy'-'MM'-'dd");
		// System.out.println(s.format(date));
		String str1 = s.format(date);// 当前的时间

		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 60);// 计算30天后的时间
		end = s.format(c.getTime());
		ExeSQL tExeSQL = new ExeSQL();
		String strSQL = "select grpcontno,prtno,grpname,CValiDate,nvl(SumPrem,0),"
				+ "nvl((select AccGetMoney from LCAppAcc where CustomerNo=LCGrpCont.appntno),0),"
				+ "nvl((select sum(prem) from lccont where LCGrpCont.grpcontno = grpcontno),0),"
				+ "(SELECT min(paytodate) from lcgrppol a where a.grpcontno=LCGrpCont.grpcontno), "
				+ "(select codename from ldcode where codetype='paymode' and code=LCGrpCont.PayMode), "
				+ "db2inst1.ShowManageName(LCGrpCont.ManageCom), "
				+ "db2inst1.getUniteCode(LCGrpCont.AgentCode) "
				+ "from "
				+ "lcgrpcont "
				+ "where appflag = '1' "
				+ "and (StateFlag is null or StateFlag = '1') "
				+ "and not exists (select 1 from ljspayb where otherno = grpcontno and dealstate = '2' and CancelReason = '0') "
				+ "and grpcontno in (select grpcontno from lcgrppol where riskcode in ('162601','162801') "
				+ "	and grpcontno=lcgrpcont.grpcontno and ManageCom like '"
				+ mGI.ManageCom
				+ "%'   and paytodate >'2018-10-01' "
//				between '"
//				+ mCurrentDate
//				+ "' and  '"
//				+ end
//				+ "' "
				+ " and GrpPolNo not in (select GrpPolNo from LJSPayGrp where GrpContNo = LCGrpCont.GrpContNo) )";
		mSSRS = tExeSQL.execSQL(strSQL);
		if (mSSRS.getMaxRow()>=1){
		for(int i=1; i<=mSSRS.getMaxRow(); i++){
		
		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	    tLCGrpContSchema.setGrpContNo(mSSRS.GetText(i, 1));
	    tLCGrpContSchema.setPrtNo(mSSRS.GetText(i, 2));//Yangh于2005-07-19将以前的注销
	    TransferData tempTransferData=new TransferData();
	    tempTransferData.setNameAndValue("StartDate","2018-10-01");
	    tempTransferData.setNameAndValue("EndDate","2018-12-01");
		tempTransferData.setNameAndValue("ManageCom",mGI.ManageCom);
		  
	    VData tVData = new VData(); 
	    tVData.add(tLCGrpContSchema);
	    tVData.add(mGI);
	    tVData.add(tempTransferData);

	    GRnewDueFeeUI tGRnewDueFeeUI = new GRnewDueFeeUI(); 
	    if (!tGRnewDueFeeUI.submitData(tVData,"INSERT")){
	    	System.out.println("催收失败");
	       }
		 }
		}
	}

	public static void main(String[] args) {
		new GRnewDueFeeBatchTask().run();
	}
}
