package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.rmi.*;
import javax.xml.rpc.*;
import com.sinosoft.lis.message.*;

import java.util.*;

//程序名称：NewBalaMsgTask.java
//程序功能：新产品月结客户通知短信
//创建日期：2013-10-12
//创建人  ：jiaos
//更新记录：  更新人    更新日期     更新原因/内容
public class NewBalaMsgTask extends TaskThread 
{
	
	private MMap map = new MMap();
	private LCCSpecSchema tLCCSpecSchema = null;
	private VData mResult = new VData();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private ExeSQL tExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public NewBalaMsgTask() {}

    public void run() {
        if (!sendMessages()) {
            return;
        }
    }
    
    public boolean sendMessages() 
    {
        System.out.println("月结通知短信批处理开始......");
        SmsServiceSoapBindingStub binding = null;
        try 
        {
            binding = (SmsServiceSoapBindingStub)new SmsServiceServiceLocator().getSmsService(); //创建binding对象
        } 
        catch(javax.xml.rpc.ServiceException jre) 
        {
            jre.printStackTrace();
        }

        binding.setTimeout(60000);
        Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。
        SmsMessages msgs = new SmsMessages(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
        msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
        msgs.setExtension("024"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
        msgs.setServiceType("xuqi"); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
        msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
        msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
        msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
        msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素

        Vector vec = new Vector();
        vec = getMessages();

        if (!vec.isEmpty()) 
        {
            msgs.setMessages((SmsMessage[]) vec.toArray(new SmsMessage[vec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
            try 
            {
                value = binding.sendSMS("Admin", "Admin", msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
                System.out.println(value.getStatus());
                System.out.println(value.getMessage());
                
                mResult.clear();                        //将发送的短信添加到LCCSpec表防止重复发送
                mResult.add(map);
                PubSubmit tSubmit = new PubSubmit();

                if(tSubmit.submitData(mResult, "")){
                	
                	System.out.println("添加到短信轨迹表成功!!!");
                }
            } 
            catch (RemoteException ex) 
            {
                ex.printStackTrace();
            }
        } 
        else 
        {
            System.out.print("本月月结短信已通知过，无需再次通知！");
        }
        return true;
    }

    private Vector getMessages() 
    {
        Vector tVector = new Vector();
        StringBuffer sql = new StringBuffer(128);
        /*
         * 通过lmriskapp表starttime查询90天以内发布的新险种险码
         * 通过返回的险码查询lcinsureacctrace是否有返回值来判断该新产品是否月结过
         * 通过ldcode返回要发送的手机号
         * 通过lccspec判断该手机号是否已经发送过信息，防止重复发送
         * 
         * 
         * 其中ldcode添加手机号时，codetype=‘monbalamsgnum’----mon="月",bala="结",msg="短信",num="号"
         * 						code='要添加的手机号'	
         * 						codename='新万能产品上线以后第一次月结的短信通知功能'
         * lccspec定义：
         * 						spectype='xp1'，月结短信记录标志
         * 						Serialno=‘险种码’，
         * 						Contno=‘手机号’
         * 						SpecContent=‘短信内容’
         * 						
         * 
         * 查询返回值如下：
         * 
         * riskcode      rickname                starttime  
         *  333201	惠享连年个人护理保险（万能型）	2013-9-25	
			333201	惠享连年个人护理保险（万能型）	2013-9-25	
         * */
        sql.append("select distinct a.riskcode,riskname,b.startdate "+
					"from lcinsureacctrace a,lmriskapp b "+ 
					"where a.riskcode=b.riskcode and "+ 
					"b.startdate > current date - 90 day "+ 
					"and b.risktype4='4' " +
					"and a.othertype='6' "+ 
					"with ur");
        System.out.println(sql.toString());
        SSRS getRisk =tExeSQL.execSQL(sql.toString());
        System.out.println("共："+getRisk.MaxRow+"新险种");
        
        sql = new StringBuffer(128);
        //查询要发送的手机号,返回：code="手机号"
        sql.append("select code from ldcode where codetype='monbalamsgnum' with ur");
        SSRS getPhoneNum =tExeSQL.execSQL(sql.toString());
        System.out.println("共："+getPhoneNum.MaxRow+"要送的手机号");
        
        for (int i = 1; i <= getRisk.MaxRow; i++) 
        {
        	
        	for(int j = 1;j<=getPhoneNum.MaxRow;j++){
        		
        		sql = new StringBuffer(128);
        		//判断是否已经发送过，防止重复发送
        		sql.append("select 1 from lccspec where spectype='xp1' and Serialno='"+getRisk.GetText(i, 1)+"' and Contno='"+getPhoneNum.GetText(j,1)+"' with ur");
        		String isHave= new ExeSQL().getOneValue(sql.toString());
        		if(isHave!= ""){
        			continue;
        		}
        		
        		
        	String tRiskCode = getRisk.GetText(i, 1);
        	String tRiskName = getRisk.GetText(i, 2);
        	String tMobile = getPhoneNum.GetText(j, 1);
        	
            if (tMobile == null || "".equals(tMobile) ||"null".equals(tMobile)) 
            {
                continue;
            }

            
            //发送给客户的短信内容            
            SmsMessage tSmsMessage = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
            StringBuffer content = new StringBuffer(128);
            
            //尊敬的中科软工作人员，××险种已经月结，请留意查看，该险码为××。日期：××时间：××
            content.append("尊敬的中科软工作人员,").append(tRiskName).append("险种已经月结").append("，请留意查看")
            	.append(".\n该险种险码为").append(tRiskCode).append("\n日期:").append(mCurrentDate).append("\n时间").append(mCurrentTime).append("\n");
            System.out.println(content.toString());
           
            tSmsMessage.setReceiver(tMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
            tSmsMessage.setContents(content.toString()); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            tSmsMessage.setOrgCode("86530000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素

            //添加SmsMessage对象
            tVector.add(tSmsMessage);
            
            
            //将发送的短信添加到LCCSpec表防止重复发送
            
            tLCCSpecSchema = new LCCSpecSchema();
            tLCCSpecSchema.setGrpContNo("00000000000000000000");
            tLCCSpecSchema.setContNo(tMobile);
            tLCCSpecSchema.setProposalContNo("00000000000000000000");
            tLCCSpecSchema.setSerialNo(tRiskCode);
            tLCCSpecSchema.setSpecType("xp1");
            tLCCSpecSchema.setSpecContent(content.toString());
            tLCCSpecSchema.setOperator("001");
            tLCCSpecSchema.setMakeDate(mCurrentDate);
            tLCCSpecSchema.setMakeTime(mCurrentTime);
            tLCCSpecSchema.setModifyDate(mCurrentDate);
            tLCCSpecSchema.setModifyTime(mCurrentTime);
            
            map.put(tLCCSpecSchema, "INSERT");
            
        	}
        }

        return tVector;
    }
    
    
    //测试
    public static void main(String[] args) 
    {
    	NewBalaMsgTask tNewBalaMsgTask = new NewBalaMsgTask();
        tNewBalaMsgTask.run();
    }
}
