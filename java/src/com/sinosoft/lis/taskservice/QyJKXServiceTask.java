package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.jkxpt.qy.QYJKXTwoGrpServiceImp;
import com.sinosoft.lis.jkxpt.qy.QYJKXTwoServiceImp;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QyJKXServiceTask extends TaskThread
{
    /**
     * <p>Title: </p>
     *
     * <p>Description:
     * 健康险平台契约模块批跑程序
     * </p>
     *
     * <p>Copyright: Copyright (c) 2005</p>
     *
     * <p>Company: </p>
     *
     * @author zhangyang
     * @version 1.0
     */
    public CErrors mErrors = new CErrors();

    public QyJKXServiceTask()
    {

    }

    public void run()
    {
        GlobalInput mG = new GlobalInput();
        mG.Operator = "001";
        mG.ManageCom = "86";

        SSRS tOneMsgSSRS = getMessageOne();
        String tOnePolicyNo = "";
    
        for (int i = 1; i <= tOneMsgSSRS.getMaxRow(); i++)
        {
        	tOnePolicyNo = tOneMsgSSRS.GetText(i, 1);
        	QYJKXTwoServiceImp tBusLogic = new QYJKXTwoServiceImp();
            
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("DataInfoKey", tOnePolicyNo);
            
            VData tVData = new VData();
            tVData.add(tTransferData);
            try
            {
                if (!tBusLogic.callJKXService(tVData, null))
                {
                    mErrors.addOneError("数据发送失败:");
                    mErrors.copyAllErrors(tBusLogic.mErrors);
                    System.out.println(tBusLogic.mErrors.getErrContent());
                }
            }
            catch (Exception ex)
            {
                System.out.println("数据发送失败！");
                ex.printStackTrace();
            }
            tVData.clear();
        }
        SSRS tGrpMsgSSRS = getMessageGrp();
        String tGrpPolicyNo = "";
        
        for (int i = 1; i <= tGrpMsgSSRS.getMaxRow(); i++)
        {
        	tGrpPolicyNo = tGrpMsgSSRS.GetText(i, 1);
        	QYJKXTwoGrpServiceImp tBusLogic = new QYJKXTwoGrpServiceImp();
        	
        	TransferData tTransferData = new TransferData();
        	tTransferData.setNameAndValue("DataInfoKey", tGrpPolicyNo);
        	
        	VData tVData = new VData();
        	tVData.add(tTransferData);
        	try
        	{
        		if (!tBusLogic.callJKXService(tVData, null))
        		{
        			mErrors.addOneError("数据发送失败:");
        			mErrors.copyAllErrors(tBusLogic.mErrors);
        			System.out.println(tBusLogic.mErrors.getErrContent());
        		}
        	}
        	catch (Exception ex)
        	{
        		System.out.println("数据发送失败！");
        		ex.printStackTrace();
        	}
        	tVData.clear();
        }
        System.out.println("数据发送成功！");
    }

    private SSRS getMessageOne()
    {
    	//#3309北京健康险平台数据报送优化 start
    	String strSQL1 = "select code from ldcode where codetype='bjjkrisk' and codealias = 'I'";
    	String strSQL2 = "select code from ldcode where codetype='bjjkduty' and codealias = 'I'";
    	String strQuery1 = LLCaseCommon.codeQuery(strSQL1);
    	String strQuery2 = LLCaseCommon.codeQuery(strSQL2);
    	//#3309北京健康险平台数据报送优化 end
    	
    	String tSql = ""
    		+ " select "
    		+ " distinct lcc.ContNo "
    		+ " from LCCont lcc "
    		+ " inner join LCPol lcp on lcp.ContNo = lcc.ContNo "
    		+ " where 1 = 1 "
    		+ " and lcc.conttype = '1' "
    		+ " and lcc.ManageCom like '8611%' "
    		+ " and lcc.SignDate = current date - 1 day "
    		+ " and lcp.RiskCode in ("+ strQuery1 +") "
    		+ " and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ("+strQuery2+"))"
    		+ " union all "
    		+ " select "
    		+ " distinct lcc.ContNo "
    		+ " from LCCont lcc "
    		+ " inner join LBPol lcp on lcp.ContNo = lcc.ContNo "
    		+ " where 1 = 1 "
    		+ " and lcc.conttype = '1' "
    		+ " and lcc.ManageCom like '8611%' "
    		+ " and lcc.SignDate = current date - 1 day "
    		+ " and lcp.RiskCode in ("+strQuery1+") "
    		+ " and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ("+strQuery2+")) "
    		+ " union all "
    		+ " select "
    		+ " distinct lcc.ContNo "
    		+ " from LBCont lcc "
    		+ " inner join LBPol lcp on lcp.ContNo = lcc.ContNo "
    		+ " where 1 = 1 "
    		+ " and lcc.conttype = '1' "
    		+ " and lcc.ManageCom like '8611%' "
    		+ " and lcc.SignDate = current date - 1 day "
    		+ " and lcp.RiskCode in ("+strQuery1+") "
    		+ " and exists (select 1 from LBDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ("+strQuery2+"))"; 

        System.out.println("SQL: " + tSql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tMsgSSRS = tExeSQL.execSQL(tSql);

        return tMsgSSRS;
    }
    
    private SSRS getMessageGrp()
    {
    	//#3309北京健康险平台数据报送优化 start
    	String strSQL1 = "select code from ldcode where codetype='bjjkrisk' and codealias = 'G'";
//    	String strSQL2 = "select lmra.RiskCode from LMRiskApp lmra where lmra.RiskType1 = '1'";
    	String strSQL3 = "select code from ldcode where codetype='bjjkduty' and codealias = 'G'";
    	String strQuery1 = LLCaseCommon.codeQuery(strSQL1);
//    	String strQuery2 = LLCaseCommon.codeQuery(strSQL2);
    	String strQuery3 = LLCaseCommon.codeQuery(strSQL3);
    	//#3309北京健康险平台数据报送优化 end
    	
    	String tSql = ""
    		+ " select "
    		+ " distinct lgc.GrpContNo "
    		+ " from LCGrpCont lgc "
    		+ " inner join LCPol lcp on lcp.GrpContNo = lgc.GrpContNo "
    		+ " where 1 = 1 "
    		+ " and lgc.CardFlag is null "
    		+ " and lgc.ManageCom like '8611%' "
    		+ " and lgc.GrpContNo != '00072191000001' "
    		+ " and lgc.SignDate = current date - 1 day "
    		+ " and lgc.Peoples2 <= 50 "
    		+ " and lcp.RiskCode in ("+strQuery1+") "
//    		+ " and lcp.RiskCode in ("+strQuery2+") "
    		+ " and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ("+strQuery3+")) "
    		+ " and not exists (select 1 from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo and lcc.PolType in ('1', '2')) "
    		+ " union all "
    		+ " select "
    		+ " distinct lgc.GrpContNo "
    		+ " from LCGrpCont lgc "
    		+ " inner join LBPol lcp on lcp.GrpContNo = lgc.GrpContNo "
    		+ " where 1 = 1 "
    		+ " and lgc.CardFlag is null "
    		+ " and lgc.ManageCom like '8611%' "
    		+ " and lgc.GrpContNo != '00072191000001' "
    		+ " and lgc.SignDate = current date - 1 day "
    		+ " and lgc.Peoples2 <= 50 "
    		+ " and lcp.RiskCode in ("+strQuery1+") "
//    		+ " and lcp.RiskCode in ("+strQuery2+") "
    		+ " and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ("+strQuery3+")) "
    		+ " and not exists (select 1 from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo and lcc.PolType in ('1', '2')) "
    		+ " union all "
    		+ " select "
    		+ " distinct lgc.GrpContNo "
    		+ " from LBGrpCont lgc "
    		+ " inner join LBPol lcp on lcp.GrpContNo = lgc.GrpContNo "
    		+ " where 1 = 1 "
    		+ " and lgc.CardFlag is null "
    		+ " and lgc.ManageCom like '8611%' "
    		+ " and lgc.GrpContNo != '00072191000001' "
    		+ " and lgc.SignDate = current date - 1 day "
    		+ " and lgc.Peoples2 <= 50 "
    		+ " and lcp.RiskCode in ("+strQuery1+") "
//    		+ " and lcp.RiskCode in ("+strQuery2+") "
    		+ " and exists (select 1 from LBDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ("+strQuery3+")) "
    		+ " and not exists (select 1 from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo and lcc.PolType in ('1', '2')) ";

        System.out.println("SQL: " + tSql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tMsgSSRS = tExeSQL.execSQL(tSql);

        return tMsgSSRS;
    }

    public static void main(String[] args)
    {
        QyJKXServiceTask mBPOQyJKXServiceTask = new QyJKXServiceTask();
        mBPOQyJKXServiceTask.run();
    }

}
