package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.jkxpt.qy.QYJKXTwoServiceImp;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QyJKXServiceTwoTask extends TaskThread
{
    /**
     * <p>Title: </p>
     *
     * <p>Description:
     * 健康险平台契约模块批跑程序
     * </p>
     *
     * <p>Copyright: Copyright (c) 2005</p>
     *
     * <p>Company: </p>
     *
     * @author zhangyang
     * @version 1.0
     */
    public CErrors mErrors = new CErrors();

    public QyJKXServiceTwoTask()
    {

    }

    public void run()
    {
        GlobalInput mG = new GlobalInput();
        mG.Operator = "001";
        mG.ManageCom = "86";

        SSRS tMsgSSRS = getMessage();
        String tPolicyNo = "";

        for (int i = 1; i <= tMsgSSRS.getMaxRow(); i++)
        {
            tPolicyNo = tMsgSSRS.GetText(i, 1);
            QYJKXTwoServiceImp tBusLogic = new QYJKXTwoServiceImp();
            
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("DataInfoKey", tPolicyNo);
            
            VData tVData = new VData();
            tVData.add(tTransferData);
            try
            {
                if (!tBusLogic.callJKXService(tVData, null))
                {
                    mErrors.addOneError("数据发送失败:");
                    mErrors.copyAllErrors(tBusLogic.mErrors);
                    System.out.println(tBusLogic.mErrors.getErrContent());
                }
            }
            catch (Exception ex)
            {
                System.out.println("数据发送失败！");
                ex.printStackTrace();
            }
            tVData.clear();
        }
        System.out.println("数据发送成功！");
    }

    private SSRS getMessage()
    {
        String tSql = ""
	        	+ " select "
	            + " distinct lcc.ContNo "
	            + " from LCCont lcc "
	            + " inner join LCPol lcp on lcp.ContNo = lcc.ContNo "
	            + " where 1 = 1 "
	            + " and lcc.conttype = '1' "
	            + " and lcc.ManageCom like '8611%' "
	            +" and lcc.SignDate <= current date - 1 day"
	            + " and lcp.RiskCode in ('1201','1202','1206','120706','121001','130101','230201','230301','230501','230601','230701','230801','230901','231001','240201','240301','240501','320106','330401','330601','330901','331001','331401','331601','331701','331801','332101','332401','332601','340101','340201','5201','531001','531201','531501','531801','540101','730101') "
	            + " and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ('207001','201001','212001','253001','243001','265001','256001','258001','270001','275001','284001','296001','297001','200001','257001','285001','239001','252001','259001','264001','274001','271001','290001','298001','262001','241001','208001','312001','315001','294001','240001','203001','299001','242001','217001','316001','295001','311001')) "
	            + "union all "
                + " select "
                + " distinct lgc.GrpContNo "
                + " from LCGrpCont lgc "
                + " inner join LCPol lcp on lcp.GrpContNo = lgc.GrpContNo "
                + " where 1 = 1 "
                + " and lgc.CardFlag is null "
                + " and lgc.ManageCom like '8611%' "
                + " and lgc.GrpContNo != '00072191000001' "
                +" and lgc.SignDate <= current date - 1 day"
                + " and lgc.Peoples2 <= 50 "
                + " and lcp.RiskCode in ('1601','1602','160308','1604','1607','160806','260301','260401','5503','5601') "
                + " and lcp.RiskCode in (select lmra.RiskCode from LMRiskApp lmra where lmra.RiskType1 = '1') "
                + " and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ('601001','601006','601005','601004','601003','601002','602001','647001','647019','647018','647017','647016','647015','647014','647013','647012','647011','647010','647009','647008','647007','647006','647005','647004','647003','647002','603001','614001','614002','621001','619003','619005','619004','619002','619001','606001','606002')) "
                + " and not exists (select 1 from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo and lcc.PolType in ('1', '2')) ";

        System.out.println("SQL: " + tSql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tMsgSSRS = tExeSQL.execSQL(tSql);

        return tMsgSSRS;
    }
    
    public static void main(String[] args)
    {
        QyJKXServiceTwoTask mBPOQyJKXServiceTask = new QyJKXServiceTwoTask();
        mBPOQyJKXServiceTask.run();
    }

}
