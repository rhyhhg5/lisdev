package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.operfee.IndiDueFeeMultiBL;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class OMDueFeeBatchTask  extends TaskThread {

    public OMDueFeeBatchTask() {
        mGlobalInput = new GlobalInput();
        mGlobalInput.Operator = "Server";
        mGlobalInput.ComCode = "86";
        mGlobalInput.ManageCom = "86";
    }

    //查找范围
    private static final int STEP_DAYS = 60;
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    // 得到当前时间
    private String mCurrentDate = PubFun.getCurrentDate();
    // 输入数据的容器
    private VData tVData = new VData();
    private TransferData tTransferData = new TransferData();
    private String startDate = "2005-01-01", endDate = null;
    private GlobalInput mGlobalInput = null;

    private IndiDueFeeMultiBL tIndiDueFeeMultiBL = new IndiDueFeeMultiBL();


    /**
     * 实现TaskThread的接口方法run
     * 由TaskThread调用
     * */
    public void run() {
        initData();
        //万能催收批处理
        try {
            tIndiDueFeeMultiBL.submitData(tVData, "INSERT");
        } catch (Exception ex) {
            this.mErrors = tIndiDueFeeMultiBL.mErrors;
            ex.getMessage();
        }
    }

    public void initData() {
        endDate = addDate(mCurrentDate, STEP_DAYS);
        System.out.print(endDate);
        tTransferData.setNameAndValue("StartDate", startDate);
        tTransferData.setNameAndValue("EndDate", endDate);
        tTransferData.setNameAndValue("ManageCom", mGlobalInput.ManageCom);
        tTransferData.setNameAndValue("BusinessFlag", "omnip"); //万能险标识
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);
    }

    private String addDate(String date, int days) {
        return PubFun.calDate(date, days - 1, "D", "1999-12-31");
    }
    public static void main(String[] args) {
        OMDueFeeBatchTask tOMDueFeeBatchTask = new OMDueFeeBatchTask();
        tOMDueFeeBatchTask.run();
    }
}
