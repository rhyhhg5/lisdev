package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.SendMsgMail;
import com.sinosoft.lis.taskservice.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.PubFun1;
import java.io.IOException;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class sendTSEmailTask {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public CErrors mErrors = new CErrors();

        // 输入数据的容器
        private VData mInputData = new VData();

        // 统一更新日期
        private String mCurrentDate = PubFun.getCurrentDate();

        // 统一更新时间
        private String mCurrentTime = PubFun.getCurrentTime();
        private MMap map = new MMap();
        private String mStratTime;
    private String mSendType;
    public sendTSEmailTask() {
    }
    public static String TaskCode = "";

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run() {
        if (!SendInfo()) {
            return;
        }
    }

    //执行存档操作
    private boolean SendInfo() {
        SSRS tSSRS_Receive = null; //得到信息接收者信息
//        String tMsgType = ""; //发送类型:"1"表示发短信"2"表示发邮件"3"两者皆发
        String mUser = "";
        String tTopic = ""; //邮件主题
        String tBaseContent = ""; //公用内容
        String tContentFlag = ""; //内容标示:"0"表示自定信息内容"1"信息模板号
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";

        //发送短消息
                String MsgInfoFlag = "ture";
//                tMsgType = "";
                tTopic = "任务管理状态";
                tContentFlag = "";
                tSSRS_Receive = getMsgReceiveTask(mUser); //获得发送任务
                String[][] msgReceive = tSSRS_Receive.getAllData();
                String tEmail = "";
                String tMsgContent = "";
                int mRow_rece = tSSRS_Receive.getMaxRow();
//                if (tMsgType.equals("2")) { //发邮件
                    for (int c_rece = 0; c_rece < mRow_rece; c_rece++) {
                        String mflag = "ture";
                        tEmail = msgReceive[c_rece][0]; //Email地址
                        tMsgContent = getMsgContent(mCurrentDate);

                        try {
                            TransferData PrintElement = new TransferData();
                            PrintElement.setNameAndValue("content", tMsgContent);
                            PrintElement.setNameAndValue("strFrom",
                                    "operation@picchealth.com");
                            PrintElement.setNameAndValue("strTo", tEmail);
                            PrintElement.setNameAndValue("strTitle", tTopic);
                            PrintElement.setNameAndValue("strPassword",
                                    "123456");

                            VData aVData = new VData();
                            aVData.add(tGlobalInput);
                            aVData.add(PrintElement);

                            SendMsgMail tSendMsgMail = new SendMsgMail();
                            if (!tSendMsgMail.submitData(aVData, "Email")) {
                                CError cErrore = new CError();
                                cErrore.errorMessage = "邮件发送失败，邮件批次号：" +
                                        msgReceive[c_rece][0] + "邮件流水号：" +
                                        msgReceive[c_rece][1];
                                mErrors.addOneError(cErrore);
                                System.out.println("邮件发送失败");
                                mflag = "false";
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    }

            return true;

    }





    //得到需要发送短信的收信人信息
    private SSRS getMsgReceiveTask(String EmailUser) {
        SSRS tSSRS = null;
        ExeSQL tExeSQL = new ExeSQL();
        String sql = "";
        if (EmailUser.equals(""))
        {
            sql = " select CodeName from LDCode where Codetype = 'EmailAddress' and Othersign = '1' with ur" ;
        }
        else
        {
            sql = " select CodeName from LDCode where Codetype = 'EmailAddress'" +
                    "and Code = '" + EmailUser + "' with ur";
        }
        tSSRS = tExeSQL.execSQL(sql);
        return tSSRS;
    }


    //合并要发送的信内容
    private String getMsgContent(String strTaskDate) {
        String msgContent = "";
        String Content[] = null;
        ExeSQL mExeSQL = new ExeSQL();
        SSRS mSSRS = new SSRS();
        int ContentLine = 0;
        String strSQL="select taskcode,taskdescribe,taskplancode,Starttime,executedate,executetime,  ";
               strSQL+="(case  when executestate = '1' then '执行正常' when executestate is null then '没有执行' when executestate = '4' then '终止执行' end ),  ";
               strSQL+="(case runstate when '0' then '正常等待状态' when '1' then '启动状态' when '2' then '暂停状态' when '3' then '正常终止状态' when '4' then '强行终止状态' when '5' then '异常终止状态' end)  ";
               strSQL+="from (   ";
               strSQL+="select a.taskcode,(select taskdescribe from ldtask where taskcode = a.taskcode),  ";
               strSQL+="a.taskplancode,substr(Starttime,12,8) Starttime,b.executedate, b.executetime, b.executestate,runstate,  ";
               strSQL+="substr(Starttime,1,10) BeginDate,substr(Endtime,1,10) EndDate   ";
               strSQL+="from ldtaskplan a, ldtaskrunlog b  ";
               strSQL+="where a.taskcode <> '000000'  ";
               strSQL+="and b.taskcode = a.taskcode   ";
               strSQL+="and b.taskplancode = a.taskplancode   ";
               strSQL+="and b.executedate = '"+strTaskDate+"'  ";
               strSQL+="and a.endtime is null ) as aa  ";
               strSQL+="order by taskcode  ";
               strSQL+="with ur ";
        mSSRS = mExeSQL.execSQL(strSQL);
        ContentLine = mSSRS.getMaxRow();
        for (int i=1;i<=ContentLine;i++)
        {
              Content =  mSSRS.getRowData(i);
              int aa = Content.length;
              String TempContent = "";
              for (int j=0;j<aa;j++)
              {
                  TempContent = TempContent + Content[j] + " ";
              }
              msgContent = msgContent +   TempContent + "\r\n ";
        }
        return msgContent;
    }

//短信发送借口



    public static void main(String args[]) {

        sendTSEmailTask a = new sendTSEmailTask();
        a.run();
        if (a.mErrors.needDealError()) {
            System.out.println(a.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }

    }
    public boolean setStartTime(String aStartTime) {
        mStratTime = aStartTime;
        System.out.println(" Set NextRunTime: " +
                           mStratTime);
        return true;
    }

    public String getStartTime() {
        return mStratTime;
    }

    public String getSendType() {
        return mSendType;
    }

    public boolean setSendType(String aSendType) {
        mSendType = aSendType;
        System.out.println("Set SendType: " + aSendType);
        return true;
    }

}
