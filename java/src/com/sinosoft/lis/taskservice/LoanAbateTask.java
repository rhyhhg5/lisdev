package com.sinosoft.lis.taskservice;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.EdorCalZTTestBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LOLoanDB;
import com.sinosoft.lis.pubfun.AccountManage;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContStateSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LOLoanSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: 还款保全自动撤销批处理程序
 * </p>
 * <p>
 * Description: 每天由系统定时调用，7天后撤销还款还未收费的保全项目
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author QiuYang
 * @version 1.0
 */

public class LoanAbateTask extends TaskThread {
	CErrors mErrors = new CErrors();

	MMap mMap = new MMap();

	GlobalInput mGlobalInput = new GlobalInput();

	/** 当前操作人员的机构 */
	String manageCom = null;

	private String currDate = PubFun.getCurrentDate();

	private String currTime = PubFun.getCurrentTime();

	public LoanAbateTask() {
		mGlobalInput.ManageCom = "86";
		mGlobalInput.Operator = "001";
	}

	/**
	 * 当前机构
	 * 
	 * @param manageCom
	 *            String
	 */
	public LoanAbateTask(String manageCom) {
		this.manageCom = manageCom;
		mGlobalInput.ManageCom = "86";
		mGlobalInput.Operator = "001";
	}

	/**
	 * 自动结案
	 */
	public void run() {
		System.out.println("贷款失效运行开始！");
		List taskList = getTaskList();
		for (int i = 0; i < taskList.size(); i++) {
			mMap = new MMap();
			if (!Abate((String) taskList.get(i))) {
				System.out.println("工单号为:" + (String) taskList.get(i) + " 的贷款无需失效!");
				;
			}
		}
		System.out.println("贷款失效运行结束！");
	}

	/**
	 * 得到待结案的任务列表
	 * 
	 * @return List
	 */
	private List getTaskList() {
		List taskList = new ArrayList();
		String sql = " select edorno from loloan a where  exists (select 1 from lcpol where polno=a.polno and stateflag in ('1','2')) and payoffflag='0' and not exists (select 1 from lccontstate where polno=a.polno and otherno=a.edorno and enddate is null ) with ur";
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		System.out.println(tSSRS.getMaxRow());
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			taskList.add(tSSRS.GetText(i, 1));
		}
		return taskList;
	}

	/**
	 * 保全撤销
	 * 
	 * @param edorAcceptNo
	 *            String
	 * @return boolean
	 */
	private boolean Abate(String edorAcceptNo) {
		System.out.println("撤销工单开始:" + edorAcceptNo);
		String tContNo = new ExeSQL().getOneValue("select contno from lpedoritem where edorno='" + edorAcceptNo + "'");
		if (tContNo == null || tContNo.equals("")) {
			System.out.println("保单号获取失败:" + edorAcceptNo);
			return false;
		}
		if (!check(tContNo)) {
			return false;
		}
		if (!checkMoney(tContNo, edorAcceptNo)) {
			return false;
		}
		if (!getsubmit(edorAcceptNo, tContNo)) {
			return false;
		}
		if (!submit()) {
			return false;
		}
		System.out.println("失效处理结束:" + edorAcceptNo);
		return true;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @param map
	 *            MMap
	 * @return boolean
	 */
	private boolean submit() {
		VData data = new VData();
		data.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}
//
//	/**
//	 * 通过团单项目信息生成个单项目公共信息，不包括具体的个保单或被保人或险种信息
//	 * 
//	 * @param polNo
//	 *            String
//	 * @return LPEdorItemSchema
//	 */
//	private LPEdorItemSchema createCmnLPEdorItemInfo(String contno) {
//		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
//		tLPEdorItemSchema.setEdorAcceptNo("000000"); // 这么写是为了数据的完整性，试算过程中不EdorAcceptNo没有实际意义
//		tLPEdorItemSchema.setEdorAppNo(tLPEdorItemSchema.getEdorAcceptNo());
//		tLPEdorItemSchema.setEdorNo(tLPEdorItemSchema.getEdorAcceptNo());
//		tLPEdorItemSchema.setEdorType(BQ.EDORTYPE_CT);
//		tLPEdorItemSchema.setEdorAppDate(currDate);
//		tLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
//		tLPEdorItemSchema.setEdorValiDate(currDate);
//		tLPEdorItemSchema.setContNo(contno);
//		return tLPEdorItemSchema;
//	}

	/**
	 * 校验本息合计和现金价值大小
	 * 
	 * @param map
	 *            MMap
	 * @return boolean
	 */
	private boolean checkMoney(String contno, String edorno) {
		double tCashValue = 0;

		LCPolDB tLCPolDB = new LCPolDB();
		LCPolSet tLCPolSet = tLCPolDB.executeQuery("select * from lcpol where contno='"+contno+"' and appflag='1' ");
		if(tLCPolSet.size()<=0){
			// @@错误处理
			System.out.println("LoanAbateTask+checkMoney++--");
			CError tError = new CError();
			tError.moduleName = "LoanAbateTask";
			tError.functionName = "checkMoney";
			tError.errorMessage = "获取险种信息失败!";
			mErrors.addOneError(tError);
			return false;
		}
		for (int i = 1; i <= tLCPolSet.size(); i++) {
			EdorCalZTTestBL tEdorCalZTTestBL = new EdorCalZTTestBL();
			tEdorCalZTTestBL.setEdorValiDate(currDate);
			tEdorCalZTTestBL.setOperator(mGlobalInput.Operator);
			tCashValue += -tEdorCalZTTestBL.budgetOnePol(tLCPolSet.get(i).getPolNo()).getGetMoney();
		}
		
//		EdorCalZTTestBL tEdorCalZTTestBL = new EdorCalZTTestBL();
//		tEdorCalZTTestBL.setEdorValiDate(currDate);
//		tEdorCalZTTestBL.setOperator(mGlobalInput.Operator);
//		LPEdorItemSchema tLPEdorItemSchema = createCmnLPEdorItemInfo(contno);
//
//		EdorCalZT tEdorCalZT = new EdorCalZT("1");
//		try {
//			tEdorCalZT.mGetDataFromC=true;
//			tEdorCalZT.calZTData(tLPEdorItemSchema);
//		} catch (Exception ex) {
//			// @@错误处理
//			System.out.println("LoanAbateTask+checkMoney++--");
//			CError tError = new CError();
//			tError.moduleName = "LoanAbateTask";
//			tError.functionName = "checkMoney";
//			tError.errorMessage = "计算现金价值失败";
//			mErrors.addOneError(tError);
//			return false;
//		}
//
//		for (int i = 1; i <= tEdorCalZT.mLJSGetEndorseSet.size(); i++) {
//			tCashValue += -tEdorCalZT.mLJSGetEndorseSet.get(i).getGetMoney();
//		}

		LOLoanDB tLOLoanDB = new LOLoanDB();
		LOLoanSet tLOLoanSet = tLOLoanDB.executeQuery("select * from loloan where polno in (select polno from lcpol where contno='" + contno + "') and edorno='" + edorno
				+ "' and loantype='0' and payoffflag='0' with ur");
		if (tLOLoanSet != null && tLOLoanSet.size() != 1) {
			// @@错误处理
			System.out.println("LoanAbateTask+checkData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorRFAppConfirmBL";
			tError.functionName = "checkMoney";
			tError.errorMessage = "获取贷款表失败!";
			mErrors.addOneError(tError);
			return false;
		}
		LCContDB tLCContDB = new LCContDB();
		LCContSchema tLCContSchema = new LCContSchema();
		tLCContDB.setContNo(contno);
		if(!tLCContDB.getInfo()){
			// @@错误处理
			System.out.println("LoanAbateTask+checkData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorRFAppConfirmBL";
			tError.functionName = "checkMoney";
			tError.errorMessage = "获取保单表失败!";
			mErrors.addOneError(tError);
			return false;			
		}
		tLCContSchema = tLCContDB.getSchema();
		String riskcode = new ExeSQL().getOneValue("select riskcode from lcpol where polno='" + tLOLoanSet.get(1).getPolNo() + "'");
		AccountManage tAccountManage = new AccountManage();
		double sumloan = tAccountManage.getLoanInterest(tLOLoanSet.get(1).getLoanDate(), tLOLoanSet.get(1).getSumMoney(),tLCContSchema.getManageCom(),tLCContSchema.getPrem()+"", PubFun.calDate(currDate, 1, "D", null), riskcode);
		if (sumloan == -1) {
			// @@错误处理
			System.out.println("LoanAbateTask+checkMoney++--");
			CError tError = new CError();
			tError.moduleName = "LoanAbateTask";
			tError.functionName = "checkMoney";
			tError.errorMessage = "计算利息失败";
			mErrors.addOneError(tError);
			return false;
		}
		sumloan += tLOLoanSet.get(1).getSumMoney();
		if (sumloan < tCashValue) {
			System.out.println("现金价值大于本息合计,不会失效:" + contno);
			return false;
		}
		return true;
	}
	
	/**
	 * 校验此保全能否撤销
	 * 
	 * @param map
	 *            MMap
	 * @return boolean
	 */
	private boolean check(String contno) {
		// 续期交费未核销
		if (!booIsGetMoney("OperFee", contno)) {
			System.out.println(contno + "续期交费未核销");
			return false;
		}
		// 保全交费未核销
		if (!booIsGetMoney("Endorse", contno)) {
			System.out.println(contno + "保全缴费未核销");
			return false;
		}
		/* 保全状态 */
		if (edorseState(contno, "1")) {
			System.out.println(contno + "在做保全项目");
			return false;
		}
		/* 理赔状态 */
		ExeSQL e = new ExeSQL();
		StringBuffer sql = new StringBuffer();
		sql.append("select 1 from LCInsured a ").append("where exists (select 1 from LLCase  ").append(" where CustomerNo = a.InsuredNo  ").append(
				" and rgtstate not in ('11', '12', '14')) and ContNo = '" + contno + "' ");
		SSRS tSSRS = e.execSQL(sql.toString());
		if (tSSRS != null && tSSRS.getMaxRow() > 0) {
			return false;
		}
		/* 保单状态 */
		if (contState(contno, "1")) {
			System.out.println(contno + "已满期");
			return false;
		}
		return true;
	}

	/* 保全状态 */
	public boolean edorseState(String aContNo, String aContType) {
		String tableName = "";
		String contType = "";
		// 个单
		if ("1".equals(aContType)) {
			tableName = " LPEdorMain ";
			contType = " contNo ";
		} else {
			tableName = " LPGrpEdorMain ";
			contType = " grpContNo ";
		}

		StringBuffer sql = new StringBuffer();
		sql.append("select a.edorAcceptNo ").append("from LPEdorApp a, ").append(tableName).append(" b ").append("where a.edorAcceptNo = b.edorAcceptNo ").append("    and b.").append(contType)
				.append(" = '").append(aContNo).append("' ").append("    and a.edorState != '").append(BQ.EDORSTATE_CONFIRM).append("' ").append("   and not exists(select 1 from LGwork ").append(
						"           where WorkNo = a.EdorAcceptNo and StatusNo = '8') "); // 撤销
		System.out.println(sql.toString());
		ExeSQL e = new ExeSQL();
		SSRS tSSRS = e.execSQL(sql.toString());
		if (tSSRS != null && tSSRS.getMaxRow() > 0) {
			return true;
		}

		return false;
	}
	/* 保单状态 */
	public boolean contState(String aContNo, String aContType) {
		String sql = "";
		FDate tFDate=new FDate();
		// 个单
		if ("1".equals(aContType)) {
			sql=" select cinvalidate from lccont where contno='"+aContNo+"'  with ur";
		} else {
			sql=" select cinvalidate from lcgrpcont where grpcontno='"+aContNo+"' with ur";
		}
		
		String cinvalidate = new ExeSQL().getOneValue(sql);
		System.out.println(sql);
		//过了满期日（含）之后，保单不再贷款失效
        if(tFDate.getDate(PubFun.getCurrentDate()).compareTo(tFDate.getDate(cinvalidate))>=0)
        {
        	mErrors.addOneError("保单已满期");
            return true;
        }

		return false;
	}

	/**
	 * 是否已交费 已缴费false，否则true
	 */
	private boolean booIsGetMoney(String aFlag, String aContNo) {
		// 如果已经交费尚未核销，不能终止
		String sql = "";
		if (aFlag.equals("OperFee")) {
			sql = " select 1 from ljtempfee where tempfeeno = " + " (select getnoticeno from ljspay where otherno = '" + aContNo + "') ";
		}
		if (aFlag.equals("Endorse")) {
			String EndorseSql = "select distinct endorsementno from ljsgetendorse where grpcontno = '" + aContNo + "' and endorsementno like '20%' ";
			sql = " select 1 from ljtempfee where tempfeeno = " + " (select getnoticeno from ljspay where otherno in (" + EndorseSql + ")) ";
		}
		ExeSQL tExeSQL = new ExeSQL();
		String sHasPayFlag = tExeSQL.getOneValue(sql);
		if (tExeSQL.mErrors.needDealError()) {
			// CError.buildErr(this, "暂交费查询失败!");
			mErrors.addOneError(new CError("暂交费查询失败!"));
			return false;
		}
		if (sHasPayFlag != null && sHasPayFlag.trim().equals("1")) {
			mErrors.addOneError(new CError("已经有暂交费!"));
			return false; // 已经有暂交费
		}
		sql = " select bankonthewayflag from ljspay " + " where otherno = '" + aContNo + "' ";
		SSRS tssrs = tExeSQL.execSQL(sql);
		if (tssrs != null && tssrs.getMaxRow() >= 1) {
			String tBankFlag = tssrs.GetText(1, 1);
			if (tBankFlag != null && !tBankFlag.trim().equals("") && tBankFlag.trim().equals("1")) {
				mErrors.addOneError(new CError("处于银行划款期间"));
				return false;
			}
		}
		return true;
	}

	/**
	 * 获取提交数据
	 * 
	 * @param map
	 *            MMap
	 * @return boolean
	 */
	private boolean getsubmit(String edorno, String contno) {
		/*
		 * LCContStateDB tLCContStateDB = new LCContStateDB(); LCContStateSet
		 * tLCContStateSet = new LCContStateSet(); LCContStateSchema
		 * oldLCContStateSchema = new LCContStateSchema(); String strSql = "
		 * select * from lccontstate where statetype = 'Available' " + " and
		 * enddate is null and contno = '" + contno + "' and polno = '000000'";
		 * tLCContStateSet = tLCContStateDB.executeQuery(strSql); if
		 * (!tLCContStateDB.mErrors.needDealError()) { if (tLCContStateSet !=
		 * null && tLCContStateSet.size() > 0) { oldLCContStateSchema =
		 * tLCContStateSet.get(1).getSchema();
		 * oldLCContStateSchema.setEndDate(currDate);
		 * oldLCContStateSchema.setModifyDate(currDate);
		 * oldLCContStateSchema.setModifyTime(currTime);
		 * mMap.put(oldLCContStateSchema, "DELETE&INSERT"); }else{
		 * System.out.println("保单状态查询失败"); return false; } }
		 */
		
        String updateStr = "update LCContState set State = '0' ,EndDate='"+currDate+"' "+ "where contno ='"+ contno + "'" 
        			+ " and StateType ='Available' and State = '1' and enddate is null ";
        mMap.put(updateStr, SysConst.UPDATE);
		
		LCPolDB tLCPolDB = new LCPolDB(); 
		LCPolSet tLCPolSet = new LCPolSet();
		tLCPolSet=tLCPolDB.executeQuery("select * from lcpol where contno='"+contno+"' and stateflag in ('1','2')");
		for (int i = 1; i <= tLCPolSet.size(); i++) {
			LCContStateSchema tLCContStateSchema = new LCContStateSchema();
			tLCContStateSchema.setOtherNoType("10");
			tLCContStateSchema.setOtherNo(edorno);
			tLCContStateSchema.setGrpContNo("000000");
			tLCContStateSchema.setContNo(contno);
			tLCContStateSchema.setInsuredNo(tLCPolSet.get(i).getInsuredNo());
			tLCContStateSchema.setPolNo(tLCPolSet.get(i).getPolNo());
			tLCContStateSchema.setStateType("Available");
			tLCContStateSchema.setStateReason("LN");
			tLCContStateSchema.setState("1");
			tLCContStateSchema.setOperator("000");
			tLCContStateSchema.setStartDate(currDate);
			tLCContStateSchema.setMakeDate(currDate);
			tLCContStateSchema.setMakeTime(currTime);
			tLCContStateSchema.setModifyDate(currDate);
			tLCContStateSchema.setModifyTime(currTime);
			mMap.put(tLCContStateSchema, "INSERT");
		}

		if(tLCPolSet.size()>0){
		LCContStateSchema tLCContStateSchema = new LCContStateSchema();
		tLCContStateSchema.setOtherNoType("10");
		tLCContStateSchema.setOtherNo(edorno);
		tLCContStateSchema.setGrpContNo("000000");
		tLCContStateSchema.setContNo(contno);
		tLCContStateSchema.setInsuredNo("000000");
		tLCContStateSchema.setPolNo("000000");
		tLCContStateSchema.setStateType("Available");
		tLCContStateSchema.setStateReason("LN");
		tLCContStateSchema.setState("1");
		tLCContStateSchema.setOperator("000");
		tLCContStateSchema.setStartDate(currDate);
		tLCContStateSchema.setMakeDate(currDate);
		tLCContStateSchema.setMakeTime(currTime);
		tLCContStateSchema.setModifyDate(currDate);
		tLCContStateSchema.setModifyTime(currTime);
		mMap.put(tLCContStateSchema, "INSERT");
		}
		String updatepol = " update LCPol Set StateFlag = '2',ModifyDate = '" + currDate + "',ModifyTime = '" + currTime + "' where ContNo = '" + contno + "' and stateflag in ('1','2')";
		mMap.put(updatepol, "UPDATE");
		String updatecont = "update LCCont Set StateFlag = '2',ModifyDate = '" + currDate + "',ModifyTime = '" + currTime + "' where Contno = '" + contno + "' and stateflag in ('1','2') ";
		mMap.put(updatecont, "UPDATE");
		return true;
	}

	/**
	 * 主函数，测试用
	 * 
	 * @param arg
	 *            String[]
	 */
	public static void main(String arg[]) {

		LoanAbateTask tBqFinishTask = new LoanAbateTask();
		System.out.println("保全自动结案运行开始！");
		List taskList = tBqFinishTask.getTaskList();
		for (int i = 0; i < taskList.size(); i++) {
			tBqFinishTask.mMap = new MMap();
			tBqFinishTask.Abate((String) taskList.get(i));
		}
		System.out.println("保全自动结案运行结束！");
		// tBqFinishTask.run();
		// tBqFinishTask.delete("20101202000037");
		// tBqFinishTask.getsubmit("20111122000003");
		// tBqFinishTask.submit();
	}
}
