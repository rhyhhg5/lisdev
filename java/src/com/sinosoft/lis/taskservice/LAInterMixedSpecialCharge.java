package com.sinosoft.lis.taskservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class LAInterMixedSpecialCharge {

	private static Logger mLogger = Logger
			.getLogger(LAInterMixedSpecialCharge.class);

	private ExeSQL mExeSQL = new ExeSQL();

	private ArrayList mpInList;

	private long inTime = 0;

	private SSRS mSSRS = new SSRS();

	private SSRS mSSRSGRP = new SSRS();

	private String mCurrentDate = PubFun.getCurrentDate();

	private int mBatchNoCount = 0;

	private String mOthertBatchNo = "";

	// 错误编码设置
	private String mErrorCode = "";

	private static Map<String, String> mErrorInfo = new HashMap<String, String>();// 存储错误信息

	static {
		mErrorInfo.put("00", "成功");
		mErrorInfo.put("01", "接口处理过程中程序发生异常");
	}

	public ArrayList<String> service(ArrayList<String> pInlist) {

		// 将操作数据拷贝到本类中
		this.mpInList = pInlist;

		// if (!getInputData())
		// {
		//
		// return getSoapError(mErrorCode);
		// }

		// if (!checkData())
		// {
		// return getSoapError(mErrorCode);
		// }
		// 进行业务处理
		if (!dealData()) {
			return pInlist;
		}
		return pInlist;

	}

	// private boolean getInputData()
	// {
	// inTime = System.currentTimeMillis();
	// // mLogger.info("1 VerifyContNewOfIAC 获取到JSON报文时间-----" + inTime +
	// "--------");
	// try
	// {
	// JSONObject reqJSONObject = mpInJSONDoc.getJSONObject("reqData");
	// // 得到解密后的参数信息
	// // mInsuredFlag = reqJSONObject.getString("insuredFlag");
	// // mIdType_req = reqJSONObject.getString("idType");
	// // mIdNo = reqJSONObject.getString("customerId");
	// // mMinAmnt = reqJSONObject.getString("minAmt");
	// // mLossDay = Integer.parseInt(reqJSONObject.getString("lossDay"));
	// // mSerialNo = reqJSONObject.getString("serialNo");
	// }
	// catch (Exception e)
	// {
	// e.printStackTrace();
	// mLogger.info("1 VerifyContNewOfIAC 报文JSON解析异常-----" + e.getMessage() +
	// "--------");
	// mErrorCode = "01";
	// return false;
	// }
	// return true;
	// }

	/**
	 * 进行业务处理(此处不区分团险、个险，根据证件信息分开返回团、个险的信息)
	 * 
	 * @return
	 */

	private boolean dealData() {
		// 团险保单验真
		if (!verifyLAInterCharge()) {
			return false;
		}
		// 个险保单验真
		// if (!verifyCont())
		// {
		// return false;
		// }
		// 查询结果为空
		if ((mSSRS == null || mSSRS.getMaxRow() < 1)) {
			mErrorCode = "03";
			mLogger.info("dealdata() 01----个险保单验真信息无符合查询条件的保单数据");
			return false;
		}
		return true;
	}

	/**
	 * 健康险实付财险
	 * 
	 * @param pResponseCode
	 *            错误编码
	 * @param pErrorMsg
	 *            错误原因
	 * @return
	 */

	public boolean verifyLAInterCharge() {
		String tCalDate = PubFun.getCurrentDate();
		tCalDate = PubFun.calDate(tCalDate, -1, "D", "");
//		tCalDate = "2019-01-24";
		SSRS tSSRS = new SSRS();
		SSRS tSSRS11 = new SSRS();
		// 查询个单
		String tSQL = " select  d.contno,d.endorsementno,d.tmakedate,d.managecom,a.othernotype  from ljaget a,lacharge b,lacom c, lacommision d  where a.getnoticeno = b.commisionsn and b.tcommisionsn = d.commisionsn and a.otherno = b.receiptno "
				+ "and a.othernotype in ('BC','MC')   and b.branchtype ='5' and b.branchtype2='01' and d.grpcontno ='00000000000000000000' "
				+ "and c.actype='06' and c.agentcom = b.agentcom  and a.confdate ='"
				+ tCalDate
				+ "'  group by d.endorsementno,d.tmakedate,d.contno,d.managecom,a.othernotype  ";
		System.out.println("@@@@@@@@@**********&&&&&&&查询个单sql:---" + tSQL);
		tSSRS = mExeSQL.execSQL(tSQL);
		// 查询团单
		String tSQL2 = " select  d.grpcontno,d.endorsementno,d.tmakedate,d.managecom,a.othernotype   from ljaget a,lacharge b,lacom c, lacommision d  where a.getnoticeno = b.commisionsn and b.tcommisionsn = d.commisionsn and a.otherno = b.receiptno "
				+ "and a.othernotype in ('BC','MC')   and b.branchtype ='5' and b.branchtype2='01' and d.grpcontno <>'00000000000000000000' "
				+ "and c.actype='06' and c.agentcom = b.agentcom and   a.confdate ='"
				+ tCalDate
				+ "'  group by d.endorsementno,d.tmakedate,d.grpcontno,d.managecom,a.othernotype  ";
		System.out.println("<<<<<<^^^^^^^>>>>>>查询团单sql:-----" + tSQL2);
		tSSRS11 = mExeSQL.execSQL(tSQL2);

		if ((tSSRS == null || tSSRS.getMaxRow() < 1)
				&& (tSSRS11 == null || tSSRS11.getMaxRow() < 1)) {
			return false;
		}
		String tEndorseSQL = "";
		// 处理个单
		if ((tSSRS != null && tSSRS.getMaxRow() >= 1)) {
			for (int i = 1; i <= tSSRS.MaxRow; i++) {
				if (tSSRS.GetText(i, 2).equals("")
						|| null == tSSRS.GetText(i, 2)) {
					tEndorseSQL = " is null";
				} else {
					tEndorseSQL = "='" + tSSRS.GetText(i, 2) + "'";
				}
				StringBuffer tDetailSql = new StringBuffer();

				if (tSSRS.GetText(i, 4).substring(0, 4).equals("8695")) {
					tDetailSql
							.append(" select distinct  (case when (select grpagentcode from lccont where contno = b.contno) is null then (select grpagentcode from lbcont where contno = b.contno) else (select grpagentcode from lccont where contno = b.contno) end) 代理方人员代码,");
					tDetailSql
							.append("(case when (select grpagentname from lccont where contno = b.contno) is null then (select grpagentname from lbcont where contno = b.contno) else (select grpagentname from lccont where contno = b.contno) end) 代理方人员姓名 ,");
					tDetailSql
							.append("b.contno 保单号,substr(b.managecom,1,4) 归属机构,b.managecom  管理机构,b.branchtype 渠道,(select codename from ldcode where codetype='cardflag1' and code =(select cardflag from lccont where contno = b.contno)) 出单平台 ,");
					tDetailSql
							.append("(select agentstate from laagent where agentcode = d.agentcode) 在职状态,getUniteCode(d.agentcode) 互动专员工号,d.signdate 签单日期,");
					tDetailSql
							.append("(select sum(e.charge) from lacharge e,ljaget,lacommision   where e.tcommisionsn = lacommision.commisionsn and e.commisionsn= ljaget.getnoticeno   and ljaget.confdate ='"
									+ tCalDate
									+ "' and e.contno ='"
									+ tSSRS.GetText(i, 1)
									+ "' and lacommision.endorsementno "
									+ tEndorseSQL
									+ " and e.tmakedate ='"
									+ tSSRS.GetText(i, 3)
									+ "' and substr(e.managecom,1,4) ='8695' and ljaget.othernotype ='"+tSSRS.GetText(i, 5)+"'  )  应付业务佣金合计 ,d.endorsementno 保全工单号, a.othernotype 佣金类型");
					tDetailSql
							.append(" from ljaget a,lacharge b,lacom c, lacommision d  where a.getnoticeno = b.commisionsn and b.tcommisionsn = d.commisionsn and a.otherno = b.receiptno    and b.branchtype ='5' and b.branchtype2='01'    ");
					tDetailSql
							.append(" and c.actype='06' and c.agentcom = b.agentcom  and a.confdate ='"
									+ tCalDate
									+ "' and d.contno  ='"
									+ tSSRS.GetText(i, 1)
									+ "' and d.endorsementno "
									+ tEndorseSQL
									+ "  and d.tmakedate ='"
									+ tSSRS.GetText(i, 3)
									+ "' and substr(b.managecom,1,4) ='8695' and a.othernotype='"+tSSRS.GetText(i, 5)+"' ");
//					tDetailSql.append(" union select distinct  (case when (select grpagentcode from lccont where contno = b.contno) is null then (select grpagentcode from lbcont where contno = b.contno) else (select grpagentcode from lccont where contno = b.contno) end) 代理方人员代码, ");
//					tDetailSql
//							.append("(case when (select grpagentname from lccont where contno = b.contno) is null then (select grpagentname from lbcont where contno = b.contno) else (select grpagentname from lccont where contno = b.contno) end) 代理方人员姓名 ,");
//					tDetailSql
//							.append("b.contno 保单号,substr(b.managecom,1,4) 归属机构,b.managecom  管理机构,b.branchtype 渠道,(select codename from ldcode where codetype='cardflag1' and code =(select cardflag from lccont where contno = b.contno)) 出单平台 ,");
//					tDetailSql
//							.append("(select agentstate from laagent where agentcode = d.agentcode) 在职状态,getUniteCode(d.agentcode) 互动专员工号,d.signdate 签单日期,");
//					tDetailSql
//							.append("(select sum(e.charge) from lacharge e,ljaget,lacommision   where e.tcommisionsn = lacommision.commisionsn and e.commisionsn= ljaget.getnoticeno and ljaget.othernotype ='MC'  and ljaget.confdate ='"
//									+ tCalDate
//									+ "' and e.contno ='"
//									+ tSSRS.GetText(i, 1)
//									+ "' and lacommision.endorsementno "
//									+ tEndorseSQL
//									+ " and e.tmakedate ='"
//									+ tSSRS.GetText(i, 3)
//									+ "' and substr(e.managecom,1,4) ='8695'  )  应付业务佣金合计 ,d.endorsementno 保全工单号,'GL' 管理佣金");
//					tDetailSql
//							.append(" from ljaget a,lacharge b,lacom c, lacommision d  where a.getnoticeno = b.commisionsn and b.tcommisionsn = d.commisionsn and a.otherno = b.receiptno and a.othernotype = 'MC'   and b.branchtype ='5' and b.branchtype2='01'    ");
//					tDetailSql
//							.append(" and c.actype='06' and c.agentcom = b.agentcom  and a.confdate ='"
//									+ tCalDate
//									+ "' and d.contno  ='"
//									+ tSSRS.GetText(i, 1)
//									+ "' and d.endorsementno "
//									+ tEndorseSQL
//									+ "  and d.tmakedate ='"
//									+ tSSRS.GetText(i, 3)
//									+ "' and substr(b.managecom,1,4) ='8695' ");
				} else {
					tDetailSql
							.append("select distinct  (case when (select grpagentcode from lccont where contno = b.contno) is null then (select grpagentcode from lbcont where contno = b.contno) else (select grpagentcode from lccont where contno = b.contno) end) 代理方人员代码, ");
					tDetailSql
							.append("(case when (select grpagentname from lccont where contno = b.contno) is null then (select grpagentname from lbcont where contno = b.contno) else (select grpagentname from lccont where contno = b.contno) end) 代理方人员姓名 ,");
					tDetailSql
							.append("b.contno 保单号,substr(b.managecom,1,4) 归属机构,b.managecom  管理机构,b.branchtype 渠道,(select codename from ldcode where codetype='cardflag1' and code =(select cardflag from lccont where contno = b.contno)) 出单平台 ,");
					tDetailSql
							.append("(select agentstate from laagent where agentcode = d.agentcode) 在职状态,getUniteCode(d.agentcode) 互动专员工号,d.signdate 签单日期,");
					tDetailSql
							.append("(select sum(e.charge) from lacharge e,ljaget,lacommision   where e.tcommisionsn = lacommision.commisionsn and e.commisionsn= ljaget.getnoticeno and ljaget.othernotype ='BC' and substr(e.managecom,1,4) <> '8695'  and ljaget.confdate ='"
									+ tCalDate
									+ "' and e.contno ='"
									+ tSSRS.GetText(i, 1)
									+ "' and lacommision.endorsementno "
									+ tEndorseSQL
									+ " and e.tmakedate ='"
									+ tSSRS.GetText(i, 3)
									+ "' and substr(e.managecom,1,4)<>'8695' and ljaget.othernotype='"+tSSRS.GetText(i, 5)+"'  )    应付业务佣金合计,d.endorsementno 保全工单号,a.othernotype 佣金类型  ");
					tDetailSql
							.append(" from ljaget a,lacharge b,lacom c, lacommision d  where a.getnoticeno = b.commisionsn and b.tcommisionsn = d.commisionsn and a.otherno = b.receiptno and a.othernotype ='BC'   and b.branchtype ='5' and b.branchtype2='01' and substr(b.managecom,1,4) <> '8695'   ");
					tDetailSql
							.append(" and c.actype='06' and c.agentcom = b.agentcom  and a.confdate ='"
									+ tCalDate
									+ "' and d.contno  ='"
									+ tSSRS.GetText(i, 1)
									+ "' and d.endorsementno "
									+ tEndorseSQL
									+ "  and d.tmakedate ='"
									+ tSSRS.GetText(i, 3) + "' and othernotype='"+tSSRS.GetText(i, 5)+"'  ");
				}

				mLogger.info("dealdata()处理个单数据@@@@@@ 01----tDetailSql"
						+ tDetailSql);
				mLogger.info("*************开始执行个单sql时间："
						+ PubFun.getCurrentTime());
				mSSRS = mExeSQL.execSQL(tDetailSql.toString());
				mLogger.info("#################开始执行个单sql时间："
						+ PubFun.getCurrentTime());

				String tCharge = getSoapRecordPerson().toString();
				mpInList.add(tCharge);

			}
		}
		// 处理团单
		if ((tSSRS11 != null && tSSRS11.getMaxRow() >= 1)) {
			for (int i = 1; i <= tSSRS11.MaxRow; i++) {
				if (tSSRS11.GetText(i, 2).equals("")
						|| null == tSSRS11.GetText(i, 2)) {
					tEndorseSQL = " is null";
				} else {
					tEndorseSQL = "='" + tSSRS11.GetText(i, 2) + "'";
				}
				StringBuffer tDetailSql = new StringBuffer();
				if (tSSRS11.GetText(i, 4).substring(0, 4).equals("8695")) {
					tDetailSql
							.append("  select distinct  (case when (select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) is null then (select grpagentcode from lbgrpcont where grpcontno = b.grpcontno) else (select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) end) 代理方人员代码,");
					tDetailSql
							.append("(case when (select grpagentname from lcgrpcont where grpcontno = b.grpcontno) is null then (select grpagentname from lbgrpcont where grpcontno = b.grpcontno) else (select grpagentname from lcgrpcont where grpcontno = b.grpcontno) end) 代理方人员姓名 , ");
					tDetailSql
							.append("b.contno 保单号,substr(b.managecom,1,4) 归属机构,b.managecom  管理机构,b.branchtype 渠道,(select codename from ldcode where codetype='cardflag1' and code =(select cardflag from lcgrpcont where grpcontno = b.grpcontno)) 出单平台 ,");
					tDetailSql
							.append("(select agentstate from laagent where agentcode = d.agentcode) 在职状态,getUniteCode(d.agentcode) 互动专员工号,d.signdate 签单日期,");
					tDetailSql
							.append("(select sum(e.charge) from lacharge e,ljaget,lacommision   where e.tcommisionsn = lacommision.commisionsn and  e.commisionsn= ljaget.getnoticeno and ljaget.othernotype='"+tSSRS11.GetText(i, 5)+"' and  ljaget.confdate ='"
									+ tCalDate
									+ "' and e.grpcontno ='"
									+ tSSRS11.GetText(i, 1)
									+ "' and lacommision.endorsementno "
									+ tEndorseSQL
									+ "  and e.tmakedate ='"
									+ tSSRS11.GetText(i, 3)
									+ "' and substr(e.managecom,1,4)='8695' )   应付业务佣金合计,d.endorsementno 保全工单号,'DL' 业务佣金  ");
					tDetailSql
							.append(" from ljaget a,lacharge b,lacom c, lacommision d  where a.getnoticeno = b.commisionsn and b.tcommisionsn = d.commisionsn and a.otherno = b.receiptno   and b.branchtype ='5' and b.branchtype2='01'  and d.endorsementno "
									+ tEndorseSQL
									+ " and d.tmakedate ='"
									+ tSSRS11.GetText(i, 3) + "'    ");
					tDetailSql
							.append(" and c.actype='06' and c.agentcom = b.agentcom and   a.confdate ='"
									+ tCalDate
									+ "' and b.grpcontno ='"
									+ tSSRS11.GetText(i, 1)
									+ "' and substr(b.managecom,1,4) ='8695' and a.othernotype='"+tSSRS11.GetText(i, 5)+"'  ");
//					tDetailSql
//							.append("  union   select distinct  (case when (select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) is null then (select grpagentcode from lbgrpcont where grpcontno = b.grpcontno) else (select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) end) 代理方人员代码,");
//					tDetailSql
//							.append("(case when (select grpagentname from lcgrpcont where grpcontno = b.grpcontno) is null then (select grpagentname from lbgrpcont where grpcontno = b.grpcontno) else (select grpagentname from lcgrpcont where grpcontno = b.grpcontno) end) 代理方人员姓名 , ");
//					tDetailSql
//							.append("b.contno 保单号,substr(b.managecom,1,4) 归属机构,b.managecom  管理机构,b.branchtype 渠道,(select codename from ldcode where codetype='cardflag1' and code =(select cardflag from lcgrpcont where grpcontno = b.grpcontno)) 出单平台 ,");
//					tDetailSql
//							.append("(select agentstate from laagent where agentcode = d.agentcode) 在职状态,getUniteCode(d.agentcode) 互动专员工号,d.signdate 签单日期,");
//					tDetailSql
//							.append("(select sum(e.charge) from lacharge e,ljaget,lacommision   where e.tcommisionsn = lacommision.commisionsn and  e.commisionsn= ljaget.getnoticeno and ljaget.othernotype='MC'  and ljaget.confdate ='"
//									+ tCalDate
//									+ "' and e.grpcontno ='"
//									+ tSSRS11.GetText(i, 1)
//									+ "' and lacommision.endorsementno "
//									+ tEndorseSQL
//									+ "  and e.tmakedate ='"
//									+ tSSRS11.GetText(i, 3)
//									+ "' and substr(e.managecom,1,4)='8695' )   应付业务佣金合计,d.endorsementno 保全工单号 ,'GL' 管理佣金 ");
//					tDetailSql
//							.append(" from ljaget a,lacharge b,lacom c, lacommision d  where a.getnoticeno = b.commisionsn and b.tcommisionsn = d.commisionsn and a.otherno = b.receiptno and a.othernotype = 'MC'  and b.branchtype ='5' and b.branchtype2='01'  and d.endorsementno "
//									+ tEndorseSQL
//									+ " and d.tmakedate ='"
//									+ tSSRS11.GetText(i, 3) + "'    ");
//					tDetailSql
//							.append(" and c.actype='06' and c.agentcom = b.agentcom and   a.confdate ='"
//									+ tCalDate
//									+ "' and b.grpcontno ='"
//									+ tSSRS11.GetText(i, 1)
//									+ "' and substr(b.managecom,1,4) ='8695'");
				} else {
					tDetailSql
							.append("select distinct  (case when (select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) is null then (select grpagentcode from lbgrpcont where grpcontno = b.grpcontno) else (select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) end) 代理方人员代码, ");
					tDetailSql
							.append("(case when (select grpagentname from lcgrpcont where grpcontno = b.grpcontno) is null then (select grpagentname from lbgrpcont where grpcontno = b.grpcontno) else (select grpagentname from lcgrpcont where grpcontno = b.grpcontno) end) 代理方人员姓名 ,");
					tDetailSql
							.append("b.contno 保单号,substr(b.managecom,1,4) 归属机构,b.managecom  管理机构,b.branchtype 渠道,(select codename from ldcode where codetype='cardflag1' and code =(select cardflag from lcgrpcont where grpcontno = b.grpcontno)) 出单平台 ,");
					tDetailSql
							.append("(select agentstate from laagent where agentcode = d.agentcode) 在职状态,getUniteCode(d.agentcode) 互动专员工号,d.signdate 签单日期,");
					tDetailSql
							.append("(select sum(e.charge) from lacharge e,ljaget,lacommision   where e.tcommisionsn = lacommision.commisionsn and  e.commisionsn= ljaget.getnoticeno and ljaget.othernotype='BC' and substr(e.managecom,1,4) <>'8695' and ljaget.confdate ='"
									+ tCalDate
									+ "' and e.grpcontno ='"
									+ tSSRS11.GetText(i, 1)
									+ "' and lacommision.endorsementno "
									+ tEndorseSQL
									+ "  and e.tmakedate ='"
									+ tSSRS11.GetText(i, 3)
									+ "' and ljaget.othernotype='"+tSSRS11.GetText(i, 5)+"'  )   应付业务佣金合计 , d.endorsementno 保全工单号,a.othernotype 佣金类型  ");
					tDetailSql
							.append(" from ljaget a,lacharge b,lacom c, lacommision d  where a.getnoticeno = b.commisionsn and b.tcommisionsn = d.commisionsn and a.otherno = b.receiptno and a.othernotype ='BC'  and b.branchtype ='5' and b.branchtype2='01'  and substr(b.managecom,1,4) <>'8695'  and d.endorsementno "
									+ tEndorseSQL
									+ " and d.tmakedate ='"
									+ tSSRS11.GetText(i, 3) + "'    ");
					tDetailSql
							.append(" and c.actype='06' and c.agentcom = b.agentcom and   a.confdate ='"
									+ tCalDate
									+ "' and b.grpcontno ='"
									+ tSSRS11.GetText(i, 1) + "' and a.othernotype='"+tSSRS11.GetText(i, 5)+"' ");
				}

				mLogger
						.info("~~~~~~~~~~~~~~~~~~dealdata()处理团单数据@@@@ 02----tDetailSql"
								+ tDetailSql);
				mLogger.info("开始执行团单sql时间：" + PubFun.getCurrentTime());
				mSSRSGRP = mExeSQL.execSQL(tDetailSql.toString());
				mLogger.info("开始执行团单sql时间：" + PubFun.getCurrentTime());

				String tGrpCharge = getGrpSoapRecordPerson().toString();
				mpInList.add(tGrpCharge);
			}
		}

		return true;
	}

	/**
	 * 生成错误信息报文
	 * 
	 * @param pResponseCode
	 *            错误编码
	 * @param pErrorMsg
	 *            错误原因
	 * @return
	 */
	public JSONObject getSoapError(String pResponseCode) {
		// mLogger.info("Into VerifyContNewOfIAC.getSoapError()...");
		try {
			// String json =
			// "{\"respData\": {\"errorCode\": \""+pResponseCode+"\",\"errorReason\": \""+mErrorInfo.get(pResponseCode)+"\"}}";
			String json = "{\"errorCode\": \"" + pResponseCode
					+ "\",\"errorReason\": \"" + mErrorInfo.get(pResponseCode)
					+ "\"}";
			JSONObject tNoStdJSON = new JSONObject();
			// tNoStdJSON.put("resp", json);//防止乱序
			tNoStdJSON.put("respData", json);// 防止乱序
			// mLogger.info("VerifyContNewOfIAC-返回错误报文信息："+tNoStdJSON);
			return tNoStdJSON;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private JSONObject getSoapRecordPerson() {
		JSONObject policyLevelJson = new JSONObject();
		try {
			mLogger.info("开始封装报文");
			mLogger.info("开始封装报文时间：" + PubFun.getCurrentTime());
			mErrorCode = "00";// 成功
			String tContNo = "";// 当前保单号
			String tInsuredNo = "";// 当前被保险人客户号
			int tPolicyCount = 0;// 保单个数
			String tAgentState = "";
			String tCostFeeType="";

			// resqJson.put("serialNo", mSerialNo);
			 JSONObject policyListJson = new JSONObject();
			 // 处理多被保人
			 JSONArray iMainJSONObject = new JSONArray();
			 JSONArray iSubJSONObject = new JSONArray();
			 JSONArray iMaiPolArray = new JSONArray();

			// 个单封装返回
			for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
				 if (!tContNo.equals(mSSRS.GetText(i, 3))) {
				// riskLevelJson = new JSONObject();
//				JSONObject policyLevelJson = new JSONObject();
//				JSONObject policyListJson = new JSONObject();
//				// 处理多被保人
//				JSONArray iMainJSONObject = new JSONArray();
//				JSONArray iSubJSONObject = new JSONArray();
//				JSONArray iMaiPolArray = new JSONArray();
				tContNo = mSSRS.GetText(i, 3);
				tAgentState = mSSRS.GetText(i, 8);
				tCostFeeType = mSSRS.GetText(i, 13);
				if (("06").equals(tAgentState)) {
					policyLevelJson.put("crossSellStatus", "0");
				} else {
					policyLevelJson.put("crossSellStatus", "1");
				}
                if("BC".equals(tCostFeeType))
                {
    				policyLevelJson.put("costFeeType","DL");
                }else{
                	policyLevelJson.put("costFeeType","GL");
                } 
				policyLevelJson.put("policyNo", tContNo);
				policyLevelJson.put("userCode", mSSRS.GetText(i, 1));

				policyLevelJson.put("userName", mSSRS.GetText(i, 2));
				policyLevelJson.put("policyComId", mSSRS.GetText(i, 4));
				policyLevelJson.put("crossSellComCode", mSSRS.GetText(i, 5));
				policyLevelJson.put("crossSellAgentCode", mSSRS.GetText(i, 6));
				policyLevelJson.put("crossSellUserCode", mSSRS.GetText(i, 9));
				policyLevelJson.put("commissionAmount", mSSRS.GetText(i, 11));
				policyLevelJson.put("policyList", policyListJson);
				policyLevelJson.put("costfee", mSSRS.GetText(i, 11));
				policyLevelJson.put("paydocfee", mSSRS.GetText(i, 11));
				policyLevelJson.put("costrate", "1");
				policyLevelJson.put("policySystem", mSSRS.GetText(i, 7));
				policyLevelJson.put("costrate", "1");
				policyLevelJson.put("signdate", mSSRS.GetText(i, 10));
				policyLevelJson.put("endorsementNo", mSSRS.GetText(i, 12));
				policyLevelJson.put("batchNo", creatBatchNo());
				

				String tPolSQL = "SELECT a.riskcode,sum(a.prem),b.riskname  from lcpol a,lmriskapp b  where a.contno ='"
						+ tContNo
						+ "' and  a.mainpolno= a.polno and a.riskcode= b.riskcode group by a.riskcode,b.riskname union  "
						+ " SELECT a.riskcode,sum(a.prem),b.riskname  from lbpol a,lmriskapp b  where a.contno ='"
						+ tContNo
						+ "' and  a.mainpolno= a.polno and a.riskcode= b.riskcode group by a.riskcode,b.riskname";
				String tSubPolSQL = " ";
				SSRS tSSRSmain = new SSRS();
				SSRS tSSRSSubMain = new SSRS();
				SSRS tSubPol = new SSRS();

				tSSRSmain = mExeSQL.execSQL(tPolSQL);
				System.out.println("……&&…………&……&……&……&……查询个单中主险的险种，保费及险种名称的sql"
						+ tPolSQL);

				double tSumPrem = 0.0;
				for (int j = 1; j <= tSSRSmain.getMaxRow(); j++) {
					JSONObject iMainPol = new JSONObject();
					double tSumPremMain = Double.parseDouble(tSSRSmain.GetText(
							j, 2));
					iMainPol.put("mainRiskCode", tSSRSmain.GetText(j, 1));
					iMainPol.put("mainRiskName", tSSRSmain.GetText(j, 3));
					iMainPol.put("mainPremium", tSSRSmain.GetText(j, 2));
					tSumPrem += tSumPremMain;
					iMainJSONObject.add(iMainPol);

					String tMainRiskCode = tSSRSmain.GetText(i, 1);
					String tMainPolSQL = "select mainpolno from lcpol where contno ='"
							+ tContNo
							+ "' and riskcode ='"
							+ tMainRiskCode
							+ "' union select mainpolno from lbpol where contno = '"
							+ tContNo
							+ "' and riskcode='"
							+ tMainRiskCode
							+ "' ";
					tSSRSSubMain = mExeSQL.execSQL(tMainPolSQL);
					if (tSSRSSubMain.MaxRow > 0) {
						tSubPolSQL = "select a.riskcode,sum(a.prem),b.riskname from lcpol a,lmriskapp b  where a.contno ='"
								+ tContNo
								+ "' and a.mainpolno ='"
								+ tSSRSSubMain.GetText(i, 1)
								+ "' and a.mainpolno<> a.polno and a.riskcode= b.riskcode group by a.riskcode,b.riskname union select a.riskcode,sum(a.prem),b.riskname from lbpol a,lmriskapp b  where a.contno = '"
								+ tContNo
								+ "'  and a.mainpolno ='"
								+ tSSRSSubMain.GetText(i, 1)
								+ "' and a.mainpolno<> a.polno and a.riskcode= b.riskcode group by a.riskcode,b.riskname ";
						tSubPol = mExeSQL.execSQL(tSubPolSQL);
						for (int k = 1; k <= tSubPol.getMaxRow(); k++) {
							JSONObject iSunPol = new JSONObject();
							double tSumPremSub = Double.parseDouble(tSubPol
									.GetText(k, 2));
							iSunPol.put("subPremium", tSubPol.GetText(k, 2));
							iSunPol.put("subRiskCode", tSubPol.GetText(k, 1));
							iSunPol.put("subRiskName", tSubPol.GetText(k, 3));
							iSunPol.put("relateMainRiskCode", tMainRiskCode);
							tSumPrem += tSumPremSub;
							iSubJSONObject.add(iSunPol);

						}

					}

				}

				policyListJson.put("mainInsuranceList", iMainJSONObject);
				policyListJson.put("subInsuranceList", iSubJSONObject);
				policyLevelJson.put("policyPremium", tSumPrem);
				policyLevelJson.put("commissionExtractionStatus", "1");
				policyLevelJson.put("policyList", policyListJson);

				 }

			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		mLogger.info("封装报文结束时间：" + PubFun.getCurrentTime());
		return policyLevelJson;
	}

	// 处理团单的封装json
	private JSONObject getGrpSoapRecordPerson() {
		JSONObject policyLevelJson = new JSONObject();
		try {
			mLogger.info("开始封装报文");
			mLogger.info("开始封装报文时间：" + PubFun.getCurrentTime());
			mErrorCode = "00";// 成功
			String tContNo = "";// 当前保单号
			String tInsuredNo = "";// 当前被保险人客户号
			int tPolicyCount = 0;// 保单个数
			String tAgentState = "";
			String tCostFeeType="";

			// resqJson.put("serialNo", mSerialNo);
			JSONObject riskLevelJson = new JSONObject();
			JSONArray riskLevelArray = new JSONArray();
			JSONArray policyLevelArray = new JSONArray();
			JSONObject policyListJson = new JSONObject();
			// 处理多被保人
			JSONObject insuredLevelJson = new JSONObject();
			JSONArray iMainJSONObject = new JSONArray();
			JSONArray iSubJSONObject = new JSONArray();
			JSONArray iMaiPolArray = new JSONArray();

			// 团单封装返回
			double tSumPrem = 0.0;
			for (int i = 1; i <= mSSRSGRP.getMaxRow(); i++) {
				if (!tContNo.equals(mSSRSGRP.GetText(i, 3))) {
					// riskLevelJson = new JSONObject();
					tContNo = mSSRSGRP.GetText(i, 3);
					tAgentState = mSSRSGRP.GetText(i, 8);
					tCostFeeType = mSSRSGRP.GetText(i, 13);
					if (("06").equals(tAgentState)) {
						policyLevelJson.put("crossSellStatus", "0");
					} else {
						policyLevelJson.put("crossSellStatus", "1");
					}
					if("BC".equals(tCostFeeType))
	                {
	    				policyLevelJson.put("costFeeType","DL");
	                }else{
	                	policyLevelJson.put("costFeeType","GL");
	                }
					policyLevelJson.put("policyNo", tContNo);
					policyLevelJson.put("userCode", mSSRSGRP.GetText(i, 1));

					policyLevelJson.put("userName", mSSRSGRP.GetText(i, 2));
					policyLevelJson.put("policyComId", mSSRSGRP.GetText(i, 4));
					policyLevelJson
							.put("crossSellComCode", mSSRSGRP.GetText(i, 5));
					policyLevelJson.put("crossSellAgentCode", mSSRSGRP.GetText(i,
							6));
					policyLevelJson.put("policySystem", mSSRSGRP.GetText(i, 7));
					policyLevelJson.put("crossSellUserCode", mSSRSGRP
							.GetText(i, 9));
					policyLevelJson.put("commissionAmount", mSSRSGRP
							.GetText(i, 11));
					policyLevelJson.put("policyList", policyListJson);
					policyLevelJson.put("costfee", mSSRSGRP.GetText(i, 11));
					policyLevelJson.put("paydocfee", mSSRSGRP.GetText(i, 11));
					policyLevelJson.put("costrate", "1");
					policyLevelJson.put("signdate", mSSRSGRP.GetText(i, 10));
					policyLevelJson.put("endorsementNo", mSSRSGRP.GetText(i, 12));
					policyLevelJson.put("batchNo", creatBatchNo());

					String tPolSQL = "SELECT a.riskcode,sum(a.prem),b.riskname  from lcpol a,lmriskapp b  where a.grpcontno ='"
							+ tContNo
							+ "' and  a.mainpolno= a.polno and a.riskcode= b.riskcode group by a.riskcode,b.riskname union SELECT a.riskcode,sum(a.prem),b.riskname  from lbpol a,lmriskapp b  where a.grpcontno = '"
							+ tContNo
							+ "' and  a.mainpolno= a.polno and a.riskcode= b.riskcode group by a.riskcode,b.riskname ";
					String tSubPolSQL = "";
					SSRS tSSRSmain = new SSRS();
					SSRS tSSRSSubMain = new SSRS();
					SSRS tSubPol = new SSRS();

					tSSRSmain = mExeSQL.execSQL(tPolSQL);
					System.out.println("***************查询团单的主险中的险种，险种名称，保费"
							+ tPolSQL);

					for (int j = 1; j <= tSSRSmain.getMaxRow(); j++) {
						JSONObject iMainPol = new JSONObject();
						iMainPol.put("mainRiskCode", tSSRSmain.GetText(j, 1));
						iMainPol.put("mainRiskName", tSSRSmain.GetText(j, 3));
						iMainPol.put("mainPremium", tSSRSmain.GetText(j, 2));
						iMainJSONObject.add(iMainPol);

						double tSumPremMain = Double.parseDouble(tSSRSmain
								.GetText(j, 2));
						tSumPrem += tSumPremMain;

						String tMainRiskCode = tSSRSmain.GetText(i, 1);
						String tMainPolSQL = "select mainpolno from lcpol where grpcontno ='"
								+ tContNo
								+ "' and riskcode ='"
								+ tMainRiskCode
								+ "' union select mainpolno from lbpol where grpcontno ='"
								+ tContNo
								+ "' and riskcode='"
								+ tMainRiskCode
								+ "' ";
						tSSRSSubMain = mExeSQL.execSQL(tMainPolSQL);
						if (tSSRSSubMain.MaxRow > 0) {
							tSubPolSQL = "select a.riskcode,sum(a.prem),b.riskname from lcpol a,lmriskapp b  where a.grpcontno ='"
									+ tContNo
									+ "' and a.mainpolno ='"
									+ tSSRSSubMain.GetText(i, 1)
									+ "' and a.mainpolno<> a.polno and a.riskcode= b.riskcode group by a.riskcode,b.riskname union select a.riskcode,sum(a.prem),b.riskname from lbpol a,lmriskapp b  where a.grpcontno ='"
									+ tContNo
									+ "'  and a.mainpolno ='"
									+ tSSRSSubMain.GetText(i, 1)
									+ "' and a.mainpolno<> a.polno and a.riskcode= b.riskcode group by a.riskcode,b.riskname ";
							tSubPol = mExeSQL.execSQL(tSubPolSQL);
							for (int k = 1; k <= tSubPol.getMaxRow(); k++) {
								JSONObject iSunPol = new JSONObject();
								double tSumPremSub = Double.parseDouble(tSubPol
										.GetText(k, 2));
								iSunPol
										.put("subPremium", tSubPol
												.GetText(k, 2));
								iSunPol.put("subRiskCode", tSubPol
										.GetText(k, 1));
								iSunPol.put("subRiskName", tSubPol
										.GetText(k, 3));
								iSunPol
										.put("relateMainRiskCode",
												tMainRiskCode);
								tSumPrem += tSumPremSub;
								iSubJSONObject.add(iSunPol);

							}

						}
					}
					policyListJson.put("mainInsuranceList", iMainJSONObject);
					policyListJson.put("subInsuranceList", iSubJSONObject);
					policyLevelJson.put("policyPremium", tSumPrem);
					policyLevelJson.put("policyList", policyListJson);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		mLogger.info("封装报文结束时间：" + PubFun.getCurrentTime());
		return policyLevelJson;
	}

	public static void main(String[] args) {
		String name = null;
		name = "";
		name = "张华那打瞌睡";
		ArrayList<String> mpInJSONDoc11 = new ArrayList<String>();
		LAInterMixedSpecialCharge a = new LAInterMixedSpecialCharge();
		a.service(mpInJSONDoc11);
		System.out.println("工作处理完成----------------");
		for (int i = 0; i < mpInJSONDoc11.size(); i++) {
			String tEXM = mpInJSONDoc11.get(i);
			System.out.println(tEXM);
		}
		// String tt = a.creatBatchNo();
		// System.out.println("tt的序列号"+tt);

	}

	public String creatBatchNo() {

		String tCurrentDate = PubFun.getCurrentDate().replace("-", "");
		String tt = "";
		String tBatchNo = "";
		// String tOthertBatchNo="";

		if (mBatchNoCount == 0) {
			tt = PubFun1.CreateMaxNo("JXXS", "86");
			tBatchNo = tCurrentDate + tt;
			mBatchNoCount++;
			mOthertBatchNo = tBatchNo;

		}

		return mOthertBatchNo;
	}
}
