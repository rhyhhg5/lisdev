package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclientybk.inf.C12;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *   	医保卡理赔注销上报批处理
 *    Time:
 *    	2017-06-05
 * </p>

 * @version 1.0
 */
public class YBKC12CancleTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    String opr = "";
    public YBKC12CancleTask()
    {}

    public void run()
    {
    	C12 c12 = new C12();
    	String SQL = "select lce.consultno from LLCaseExt lce,llcase lc"
    				+" where lce.caseno=lc.caseno"
    				+" and lc.rgtstate='14'"
    				+" and lce.consultno is not null"
    				+" and exists(select 1 from YBK_C01_LIS_ResponseInfo where caseno=lce.consultno and responsecode='1' and requesttype='C01')"
    				+" and not exists(select 1 from YBK_C12_LIS_ResponseInfo where caseno=lce.consultno and responsecode='1' and requesttype='C12')" 
    				+" with ur";
    	
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String tCaseNo = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("PrtNo", tCaseNo);
    			tVData.add(tTransferData);
    	    	if(!c12.submitData(tVData, "YBKPT"))
    	         {
    	    		 System.out.println("理赔注销批处理出问题了");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             continue;
    	         }else{
    	        	 System.out.println("理赔注销批处理成功了");
    	        	 opr ="ture";
    	         }
    		}
    	}

    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
   
    public static void main(String[] args)
    {
    	YBKC12CancleTask ybkTask = new YBKC12CancleTask();
    	ybkTask.run();
    }
}
