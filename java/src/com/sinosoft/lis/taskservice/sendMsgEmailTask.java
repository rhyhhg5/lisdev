package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.SendMsgMail;
import com.sinosoft.lis.taskservice.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.PubFun1;
import java.io.IOException;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class sendMsgEmailTask extends TaskThread {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData mInputData = new VData();

    // 统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();
    private MMap map = new MMap();
    private String mStratTime;
    private String mSendType;

    public sendMsgEmailTask() {
    }

    public static String TaskCode = "";

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run() {
        if (!SendInfo()) {
            return;
        }
    }

    //执行存档操作
    private boolean SendInfo() {
        SSRS tSSRS = getSendDate();
        SSRS tSSRS_Receive = null; //得到信息接收者信息
        int mRow = tSSRS.getMaxRow();
        int mCol = tSSRS.getMaxCol();
        String[][] msgInfo = tSSRS.getAllData();
        String tMsgType = ""; //发送类型:"1"表示发短信"2"表示发邮件"3"两者皆发
        String tTopic = ""; //邮件主题
        String tBaseContent = ""; //公用内容
        String tContentFlag = ""; //内容标示:"0"表示自定信息内容"1"信息模板号
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        //发送短消息
        if (tSSRS != null && mRow > 0) {
            for (int msgNum = 0; msgNum < mRow; msgNum++) {
                String MsgInfoFlag = "ture";
                tMsgType = msgInfo[msgNum][2];
                tTopic = msgInfo[msgNum][5];
                tContentFlag = msgInfo[msgNum][19];
                tBaseContent = getBaseContent(msgInfo[msgNum][19],
                                              msgInfo[msgNum][1],
                                              msgInfo[msgNum][6]);
                tSSRS_Receive = getMsgReceiveTask(msgInfo[msgNum][0]); //获得发送任务
                String[][] msgReceive = tSSRS_Receive.getAllData();
                String mobile = "";
                String tEmail = "";
                String tSeqNo = "";
                String tCustomerNo = "";
                String tMsgContent = "";
                int mRow_rece = tSSRS_Receive.getMaxRow();
                //发短信
                if (tMsgType.equals("1")) {
                    for (int c_rece = 0; c_rece < mRow_rece; c_rece++) {
                        String mflag = "ture";
                        mobile = msgReceive[c_rece][6]; //手机号码
                        tMsgContent = getMsgContent(msgInfo[msgNum][6],
                                msgInfo[msgNum][1],
                                msgReceive[c_rece][1],
                                msgReceive[c_rece][2],
                                msgReceive[c_rece][3],
                                msgReceive[c_rece][17],
                                tBaseContent); //发送内容

                        try {
                            TransferData PrintElement = new TransferData();
                            PrintElement.setNameAndValue("mobile", mobile);
                            PrintElement.setNameAndValue("content", tMsgContent);

                            VData aVData = new VData();
                            aVData.add(tGlobalInput);
                            aVData.add(PrintElement);

                            SendMsgMail tSendMsgMail = new SendMsgMail();
                            if (!tSendMsgMail.submitData(aVData, "Message")) {
                                CError cErrore = new CError();
                                cErrore.errorMessage = "短信发送失败，短信批次号：" +
                                        msgReceive[c_rece][0] + "短信流水号：" +
                                        msgReceive[c_rece][1];
                                mErrors.addOneError(cErrore);
                                System.out.println("短信发送失败");
                                mflag = "false";
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        //写发送成功标记及时间
                        this.setReceiveFlag(msgInfo[msgNum][0],
                                            msgReceive[c_rece][1],
                                            msgReceive[c_rece][17], mflag);
                    }
                } else if (tMsgType.equals("2")) { //发邮件
                    for (int c_rece = 0; c_rece < mRow_rece; c_rece++) {
                        String mflag = "ture";
                        tEmail = msgReceive[c_rece][7]; //Email地址
                        tMsgContent = getMsgContent(msgInfo[msgNum][6],
                                msgInfo[msgNum][1],
                                msgReceive[c_rece][1],
                                msgReceive[c_rece][2],
                                msgReceive[c_rece][3],
                                msgReceive[c_rece][17],
                                tBaseContent);

                        try {
                            TransferData PrintElement = new TransferData();
                            PrintElement.setNameAndValue("content", tMsgContent);
                            PrintElement.setNameAndValue("strFrom",
                                    "operation@picchealth.com");
                            PrintElement.setNameAndValue("strTo", tEmail);
                            PrintElement.setNameAndValue("strTitle", tTopic);
                            PrintElement.setNameAndValue("strPassword",
                                    "123456");

                            VData aVData = new VData();
                            aVData.add(tGlobalInput);
                            aVData.add(PrintElement);

                            SendMsgMail tSendMsgMail = new SendMsgMail();
                            if (!tSendMsgMail.submitData(aVData, "Email")) {
                                CError cErrore = new CError();
                                cErrore.errorMessage = "邮件发送失败，邮件批次号：" +
                                        msgReceive[c_rece][0] + "邮件流水号：" +
                                        msgReceive[c_rece][1];
                                mErrors.addOneError(cErrore);
                                System.out.println("邮件发送失败");
                                mflag = "false";
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    }
                } else if (tMsgType.equals("3")) { //发短信及邮件
                    for (int c_rece = 0; c_rece < mRow_rece; c_rece++) {
                        mobile = msgReceive[c_rece][6];
                        tEmail = msgReceive[c_rece][7];
                        tMsgContent = getMsgContent(msgInfo[msgNum][6],
                                msgInfo[msgNum][1],
                                msgReceive[c_rece][1],
                                msgReceive[c_rece][2],
                                msgReceive[c_rece][3],
                                msgReceive[c_rece][17],
                                tBaseContent);

                        try {
                            TransferData PrintElement = new TransferData();
                            PrintElement.setNameAndValue("mobile", mobile);
                            PrintElement.setNameAndValue("content", tMsgContent);
                            PrintElement.setNameAndValue("strFrom",
                                    "operation@picchealth.com");
                            PrintElement.setNameAndValue("strTo", tEmail);
                            PrintElement.setNameAndValue("strTitle", tTopic);
                            PrintElement.setNameAndValue("strPassword",
                                    "654321");

                            VData aVData = new VData();
                            aVData.add(tGlobalInput);
                            aVData.add(PrintElement);

                            SendMsgMail tSendMsgMail = new SendMsgMail();

                            if (!tSendMsgMail.submitData(aVData, "Message")) {
                                CError cErrore = new CError();
                                cErrore.errorMessage = "短信发送失败，短信批次号：" +
                                        msgReceive[c_rece][0] + "短信流水号：" +
                                        msgReceive[c_rece][1];
                                mErrors.addOneError(cErrore);
                                System.out.println("短信发送失败");
                            }
                            if (!tSendMsgMail.submitData(aVData, "Email")) {
                                CError cErrore = new CError();
                                cErrore.errorMessage = "邮件发送失败，邮件批次号：" +
                                        msgReceive[c_rece][0] + "邮件流水号：" +
                                        msgReceive[c_rece][1];
                                mErrors.addOneError(cErrore);
                                System.out.println("邮件发送失败");
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                this.setMsgInfoFlag(msgInfo[msgNum][0], MsgInfoFlag);
            }
            //提交数据
            if (map.size() > 0) {
                mInputData.add(map);

                PubSubmit tPubSubmit = new PubSubmit();

                if (!tPubSubmit.submitData(mInputData, "UPDATE||MAIN")) {
                    mErrors.copyAllErrors(tPubSubmit.mErrors);
                    mErrors.addOneError("已发短信置标记失败");
                    System.out.println("已发短信置标记失败");

                    return false;
                }

                return true;
            }
            return true;
        } else {
            return false;
        }
    }

    //得到需要发送的短信信息批次
    private SSRS getSendDate() {
        String mDate = "";
        String mTime = "";
        SSRS tSSRS = null;
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("运行时间： " + this.getStartTime());
        mDate = this.getStartTime().substring(0, 10);
        mTime = this.getStartTime().substring(11);
//        String sendType = "11";
//        String strDateTime = "2006-01-13 15:00:00";
//        mDate = strDateTime.substring(0, 10);
//        mTime = strDateTime.substring(11);
        SSRS tTaskDate = null;
        String task_sql = " select * from LOMsgInfo where presendtime = '" +
                          mTime + "' and sendType = '" +
                          this.getSendType() + "' with ur";
//                          sendType + "'";
        tTaskDate = tExeSQL.execSQL(task_sql);
        if (tTaskDate != null) {
            String sql = " select * from LOMsgInfo where presendtime = '" +
                         mTime + "' and sendType = '" +
                         this.getSendType() + "' with ur";
//                         sendType + "'";
            tSSRS = tExeSQL.execSQL(sql);
        }

        return tSSRS;
    }

    //得到需要发送短信的收信人信息
    private SSRS getMsgReceiveTask(String MsgNo) {
        SSRS tSSRS = null;
        ExeSQL tExeSQL = new ExeSQL();
        String sql = " select * from LOMsgReceive where MsgNo = '" + MsgNo +
                     "'and  (MsgSend = '0' Or MsgSend is null) with ur";
        tSSRS = tExeSQL.execSQL(sql);
        return tSSRS;
    }

    //获得基本信息内容
    private String getBaseContent(String aflag, String aDepartment,
                                  String aContent) {
        String baseContent = "";
        if (aflag.equals("0")) {
            baseContent = aContent;
        } else if (aflag.equals("1")) {
            ExeSQL tExeSQL = new ExeSQL();
            String tSql =
                    "select TemplateContent from LOMsgTemplate where Department ='"
                    + aDepartment + "' and TemplateCode = '" + aContent + "' with ur";
            baseContent = tExeSQL.getOneValue(tSql);
        }
        return baseContent;
    }

    //合并要发送的信内容
    private String getMsgContent(String aTemplatecode, String aDepartment,
                                 String aSeqno, String aCustomerno,
                                 String aCustomername, String aAddressno,
                                 String aContent) {
        String msgContent = "";
        String Content[] = aContent.split("\\^");
        ExeSQL tExeSQL = new ExeSQL();
        String addressSql = "";
        if (aCustomerno.length() == 9) {
            addressSql =
                    "select Postaladdress from LCAddress where Customerno = '"
                    + aCustomerno + "' and Addressno = '" + aAddressno + "' with ur";
        } else if (aCustomerno.length() == 8) {
            addressSql =
                    "select Grpaddress from LCGrpAddress where Customerno = '"
                    + aCustomerno + "' and Addressno = '" + aAddressno + "' with ur";
        }
        if (aDepartment.equals("BQ")) {
            if (aTemplatecode.equals("HJ")) {
                String dateSql =
                        "select edorappdate from LPEdorApp where edoracceptno='"
                        + aSeqno + "' with ur";
                String address = tExeSQL.getOneValue(addressSql);
                String edorAppdate = tExeSQL.getOneValue(dateSql);
                msgContent = Content[0] + aCustomername + Content[1] +
                             edorAppdate + Content[2] + address + Content[3];
            }
        }
        return msgContent;
    }

//短信发送借口



    public static void main(String args[]) {

        sendMsgEmailTask a = new sendMsgEmailTask();
        a.run();
        if (a.mErrors.needDealError()) {
            System.out.println(a.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }

    }

    private boolean setReceiveFlag(String aBatch, String aSeqno,
                                   String aOtherno, String aflag) {
        String Succsql = "update LOMSGRECEIVE set msgsend = '1',senddate = '" +
                         PubFun.getCurrentDate() +
                         "',sendtime='" + PubFun.getCurrentTime() +
                         "'where msgno = '" + aBatch +
                         "' and seqno = '" +
                         aSeqno + "' and addressno='" + aOtherno + "'";
        String Failsql = "update LOMSGRECEIVE set msgsend = '0',senddate = '" +
                         PubFun.getCurrentDate() +
                         "',sendtime='" + PubFun.getCurrentTime() +
                         "'where msgno = '" + aBatch +
                         "' and seqno = '" +
                         aSeqno + "' and addressno='" + aOtherno + "'";
        if (aflag.equals("ture")) {
            map.put(Succsql, "UPDATE");
        } else {
            map.put(Failsql, "UPDATE");
        }
        return true;
    }

    private boolean setMsgInfoFlag(String aBatch, String aflag) {
        String Succsql = "update LOMsgInfo set msgsend = '1',senddate = '" +
                         PubFun.getCurrentDate() +
                         "',sendtime='" + PubFun.getCurrentTime() +
                         "'where msgno = '" + aBatch + "'";
        String Failsql = "update LOMSGRECEIVE set msgsend = '0',senddate = '" +
                         PubFun.getCurrentDate() +
                         "',sendtime='" + PubFun.getCurrentTime() +
                         "'where msgno = '" + aBatch + "'";
        if (aflag.equals("ture")) {
            map.put(Succsql, "UPDATE");
        } else {
            map.put(Failsql, "UPDATE");
        }
        return true;
    }

    public boolean setStartTime(String aStartTime) {
        mStratTime = aStartTime;
        System.out.println(" Set NextRunTime: " +
                           mStratTime);
        return true;
    }

    public String getStartTime() {
        return mStratTime;
    }

    public String getSendType() {
        return mSendType;
    }

    public boolean setSendType(String aSendType) {
        mSendType = aSendType;
        System.out.println("Set SendType: " + aSendType);
        return true;
    }
}
