package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.lis.ygz.PremSeparateBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: 营改增，价税分离批处理
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author lzy
 * @version 1.0
 */

public class PremSeparateTask extends TaskThread {

	public CErrors mErrors = new CErrors();

	private String tFeeNo = null;

	private String getWhereSql = null;

	private GlobalInput mGI = new GlobalInput();

	private int sysCount = 1000;//默认每一千条数据发送一次请求

	public PremSeparateTask() {
		mGI.Operator = "server";
		mGI.ComCode = "86";
		mGI.ManageCom = mGI.ComCode;
	}

	public PremSeparateTask(GlobalInput cGlobalInput) {
		mGI = cGlobalInput;
	}

	public void run() {
		getWhereSql = "";
		if (null != tFeeNo) {
			getWhereSql = " and (tempfeeno = '" + tFeeNo + "' or moneyNo = '"
					+ tFeeNo + "') ";
		}
		//提取待分离的数据
		String sqlStr = "select * from LYPremSeparateDetail "
				+ " where state='01' and paymoney <> '0' and  paymoney <> '0.00' and  otherstate <> '04'  " 
				+ getWhereSql
				+ " with ur ";
		RSWrapper rsWrapper = new RSWrapper();
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		if (!rsWrapper.prepareData(tLYPremSeparateDetailSet, sqlStr)) {
			System.out.println("价税分离明细数据准备失败！");
			return;
		}
		do {
			rsWrapper.getData();

			LYPremSeparateDetailSet tDetailSet = new LYPremSeparateDetailSet();
			if (null == tLYPremSeparateDetailSet
					|| tLYPremSeparateDetailSet.size() < 1) {
				break;
			}
			int count = 0;
			for (int i = 1; i <= tLYPremSeparateDetailSet.size(); i++) {
				LYPremSeparateDetailSchema detailSchema=tLYPremSeparateDetailSet.get(i).getSchema();
				detailSchema.setModifyDate(PubFun.getCurrentDate());
				detailSchema.setModifyTime(PubFun.getCurrentTime());
				tDetailSet.add(detailSchema);
				count ++;
				if (count == sysCount || i == tLYPremSeparateDetailSet.size()) {
					count = 0;
					try {
						VData tVData = new VData();
						tVData.add(mGI);
						tVData.add(tDetailSet);
						PremSeparateBL tPremSeparateBL = new PremSeparateBL();
						if (!tPremSeparateBL.getSubmit(tVData, "")) {
							this.mErrors = tPremSeparateBL.mErrors;
							System.out.println("价税分离发生错误:"
									+ mErrors.getErrContent());
							tDetailSet.clear();
						} else {
							tDetailSet.clear();

							MMap tMMap = tPremSeparateBL.getResult();
							VData vdata = new VData();
							vdata.add(tMMap);
							PubSubmit tPubSubmit = new PubSubmit();
							if (!tPubSubmit.submitData(vdata, "")) {
								CError cError = new CError();
								cError.moduleName = "InsuAccBala";
								cError.functionName = "run";
								cError.errorMessage = "数据提交失败";
								this.mErrors.addOneError(cError);
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}

		} while (null != tLYPremSeparateDetailSet
				&& tLYPremSeparateDetailSet.size() > 0);
		rsWrapper.close();

	}

	public void runOne(String feeNo) {
		tFeeNo = feeNo;
		run();
	}

	public static void main(String[] args) {
		PremSeparateTask task = new PremSeparateTask();
//		task.runOne("31000620515");
		task.run();
	}

}
