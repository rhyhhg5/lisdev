package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.workflow.tb.GrpTbWorkFlowUI;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.schema.LCGrpContSchema;
import org.jdom.Element;
import org.jdom.Document;
/**
 * <p>Title: bodycheckNotice</p>
 *
 * <p>Description: 体检通知书批量打印任务管理</p>
 *
 * <p>Copyright:  Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft </p>
 *
 * @author zhuangjun
 * @version 1.0
 */
public class BodyCheckPrintTask extends TaskThread{
    private int strSql;
    private static int intStartIndex;
    private static int intMaxIndex;

    public BodyCheckPrintTask() {
        intStartIndex = 1;
        intMaxIndex = 1;
    }
    public  void run(){
        System.out.println("I come in the BodyCheckPrintTask");
        BodyCheckPrintTask BodyCheckPT = new BodyCheckPrintTask() ;
       boolean tPramamer = false;
        tPramamer = BodyCheckPT.BodyCheckQuery(intStartIndex);
        //开始批量打印
        if (tPramamer)
               BodyCheckPT.BodyCheckPB(intMaxIndex);

    }
     public static void main(String[] args){
           BodyCheckPrintTask BodyCheckPT = new BodyCheckPrintTask() ;
           boolean tPramamer = false;
           tPramamer = BodyCheckPT.BodyCheckQuery(intStartIndex);
           //开始批量打印
           if (tPramamer)
               BodyCheckPT.BodyCheckPB(intMaxIndex);



    }
    public boolean BodyCheckQuery(int intStartIndex){

        String strError = "";
        Integer intStart = new Integer(String.valueOf("1"));;
        SSRS tSSRS = new SSRS();

        String sql = "SELECT * FROM LOPRTManager,LAAgent,LABranchGroup,LWMission WHERE LOPRTManager.Code = '03' "  //LOPRTManager.Code='03' 单证类型为核保
                   +"and LWMission.ActivityID = '0000001106' "
                   +"and LWMission.ProcessID = '0000000003' "
                   +"and LWMission.Missionprop3 = LOPRTManager.Prtseq "
                   + "and LAAgent.AgentCode = LOPRTManager.AgentCode "
                   + "and LABranchGroup.AgentGroup = LAAgent.AgentGroup "
                   //+ "AND LOPRTManager.ManageCom LIKE" + "'86%25'"//ManageCom
                   + " AND LOPRTManager.ManageCom LIKE " + "'86%%'" //comcode + "%%" //登陆机构权限控制
                   + " AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'";

               ExeSQL tExeSQL = new ExeSQL();
               tSSRS = tExeSQL.execSQL(sql);
               int MaxCol = tSSRS.getMaxCol();
               intMaxIndex = tSSRS.getMaxRow();
              if (intMaxIndex==0)
                  return false;
              return true;
}

    public void BodyCheckPB(int intRecordNum){
            //JBook mbook = new JBook();
//System.out.println(mbook);
         String strLogs="";
         LOPRTManagerSchema tLOPRTManagerSchema;
         GlobalInput tG = new GlobalInput();
         tG.Operator="001";//系统自签
         tG.ManageCom="86";//默认总公司
//输出参数

         boolean operFlag=true;
         String FlagStr = "";
         String Content = "";
         String strOperation = "";
         strOperation = "PRINT";
         String strPrtSeqsql = "SELECT * FROM LOPRTManager,LAAgent,LABranchGroup,LWMission WHERE LOPRTManager.Code = '03' "  //LOPRTManager.Code='03' 单证类型为核保
                   +"and LWMission.ActivityID = '0000001106' "
                   +"and LWMission.ProcessID = '0000000003' "
                   +"and LWMission.Missionprop3 = LOPRTManager.Prtseq "
                   + "and LAAgent.AgentCode = LOPRTManager.AgentCode "
                   + "and LABranchGroup.AgentGroup = LAAgent.AgentGroup "
                   //+ "AND LOPRTManager.ManageCom LIKE" + "'86%25'"//ManageCom
                   + " AND LOPRTManager.ManageCom LIKE " + "'86%%'" //comcode + "%%" //登陆机构权限控制
                   + " AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'";




         int iFlag= 0;
         ExeSQL mPrtSeq = new ExeSQL();
         SSRS mssrs = mPrtSeq.execSQL(strPrtSeqsql);

         String mGetNowDate = PubFun.getCurrentDate();
         String mGetNowTime = PubFun.getCurrentTime();
         String mGetTime = StrTool.replace(mGetNowTime,":","-");
         String mxmlFileName = mGetNowDate+ " " + mGetTime;

         String PrtSeqArr[][]= mssrs.getAllData();
         String fileName = "ui";//StrTool.unicodeToGBK("ui");


         VData tVData;
         VData mResult = new VData();
         PrintManagerBL tPrintManagerBL = new PrintManagerBL();

         try
         {
                 XmlExport txmlExportAll = new XmlExport();
                 txmlExportAll.createDocuments("printer",tG);
                 for(int i= 0;i< intMaxIndex;i++)
                 {
                         //System.out.println("--------------I come in the For---------------"+ i);
                         tLOPRTManagerSchema = new LOPRTManagerSchema();
                         tVData = new VData();
                         tPrintManagerBL = new PrintManagerBL();
                         tLOPRTManagerSchema.setPrtSeq(PrtSeqArr[i][0]);


                         tVData.addElement(tLOPRTManagerSchema);
                         //System.out.println("--------------I come in the tVData---------------");
                         tLOPRTManagerSchema = null;
                         tVData.addElement(tG);
                         if(!tPrintManagerBL.submitData(tVData,strOperation))
                         {
                          // System.out.println("--------------I come in the if-One---------------");
                                 FlagStr = "Fail";
                                 Content=" 印刷号"+PrtSeqArr[i][0]+"："+tPrintManagerBL.mErrors.getFirstError().toString();
                                 strLogs=strLogs+Content;
                                 continue;
                         }

                           mResult = tPrintManagerBL.getResult();
                           XmlExport txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
                         if (txmlExport==null)
                         {
                         //System.out.println("--------------I come in the if-Two---------------");
                                 FlagStr = "Fail";
                                 Content="印刷号"+PrtSeqArr[i][0]+"没有得到要显示的数据文件！";
                                 strLogs=strLogs+Content;
                                 continue;
                         }
                         Element telement = txmlExport.getDocument().getRootElement();

                         if(iFlag==0)
                         {
                          // System.out.println("--------------I come in the if-Three---------------");
                                 txmlExportAll.setTemplateName(txmlExportAll.getDocument().getRootElement(),telement);
                                 iFlag = 1;
                         }
                         txmlExportAll.addDataSet(txmlExportAll.getDocument().getRootElement(),telement);

                         //生成单个数据文件
                         //if (operFlag==true)
                   //txmlExport.outputDocumentToFile("e:\\"+ fileName + "\\",PrtSeqArr[i][0]);

                         tPrintManagerBL = null;
                         tVData = null;
                 }
                 if(operFlag==true)
                 {
                         txmlExportAll.outputDocumentToFile("e:\\"+ fileName + "\\",mxmlFileName);
                         FlagStr = "Success";
                         if (strLogs.equals("")){
                         Content = "生成批量打印文件成功！";
                         }
                   else{
                   Content = strLogs;
                   }
                         //System.out.println("strLogs-->"+strLogs);
                 }
         }
         catch(Exception ex)
         {
                 operFlag=false;
                 FlagStr = "Fail";
                 Content = strOperation+"失败，原因是:" + tPrintManagerBL.mErrors.getFirstError().toString();
         }


}

}
