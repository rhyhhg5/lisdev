package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.jkxpt.qy.QYACCGrpServiceImp;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QyACCGrpServiceTask extends TaskThread
{
    /**
     * <p>Title: </p>
     *
     * <p>Description:
     * 意外险平台契约模块批跑程序
     * </p>
     *
     * <p>Copyright: Copyright (c) 2005</p>
     *
     * <p>Company: </p>
     *
     * @author zhangyang
     * @version 1.0
     */
    public CErrors mErrors = new CErrors();

    public QyACCGrpServiceTask()
    {

    }

    public void run()
    {
        GlobalInput mG = new GlobalInput();
        mG.Operator = "001";
        mG.ManageCom = "86";

        SSRS tMsgSSRS = getMessage();
        String tPolicyNo = "";

        for (int i = 1; i <= tMsgSSRS.getMaxRow(); i++)
        {
            tPolicyNo = tMsgSSRS.GetText(i, 1);
            QYACCGrpServiceImp tQYACCGrpServiceImp = new QYACCGrpServiceImp();
            
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("DataInfoKey", tPolicyNo);
            
            VData tVData = new VData();
            tVData.add(tTransferData);
            try
            {
                if (!tQYACCGrpServiceImp.callJKXService(tVData, null))
                {
                    mErrors.addOneError("数据发送失败:");
                    mErrors.copyAllErrors(tQYACCGrpServiceImp.mErrors);
                    System.out.println(tQYACCGrpServiceImp.mErrors.getErrContent());
                }
            }
            catch (Exception ex)
            {
                System.out.println("数据发送失败！");
                ex.printStackTrace();
            }
            tVData.clear();
        }
        System.out.println("数据发送成功！");
    }

    private SSRS getMessage()
    {
        String tSql = ""
                + " select "
                + " distinct lgc.GrpContNo "
                + " from LCGrpCont lgc "
                + " inner join LCPol lcp on lcp.GrpContNo = lgc.GrpContNo "
                + " where 1 = 1 "
                + " and lgc.CardFlag is null "
                + " and lgc.ManageCom like '8611%' "
                +"  and  lgc.SignDate >= current date - 1 DAYS "
                + " and lgc.Peoples2 <= 50 "
                + " and lcp.RiskCode in (select code from ldcode where codetype = 'bjaccidentrisk' ) "
                + " and lcp.RiskCode in (select lmra.RiskCode from LMRiskApp lmra where lmra.RiskType = 'A') "
                + " and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in (select code from ldcode where codetype = 'bjaccidentduty')) "
                + " and not exists (select 1 from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo and lcc.PolType in ('1', '2')) "
                +"  and not exists (select 1 from DataInfointerface where DataInfoKey=lgc.GrpContNo and state='1') ";

        System.out.println("SQL: " + tSql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tMsgSSRS = tExeSQL.execSQL(tSql);

        return tMsgSSRS;
    }

    public static void main(String[] args)
    {
        QyACCGrpServiceTask mBPOQyJKXServiceTask = new QyACCGrpServiceTask();
        mBPOQyJKXServiceTask.run();
    }

}
