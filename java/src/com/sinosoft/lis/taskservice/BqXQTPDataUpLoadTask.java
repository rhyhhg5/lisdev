package com.sinosoft.lis.taskservice;

import com.sinosoft.httpclient.inf.BqDataUpLoad;
import com.sinosoft.httpclient.inf.XbDataUpLoad;
import com.sinosoft.httpclient.inf.XqPremUpLoad;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 税优产品续期续保数据上报批处理
 * @author abc
 *
 */
public class BqXQTPDataUpLoadTask extends TaskThread {
	public CErrors mErrors = new CErrors();
	private GlobalInput tGI = new GlobalInput();
	private String managecom = "86";
	private String mPayNo=null;
	private String whereParSql="";
	
	public BqXQTPDataUpLoadTask(String tPayNo,GlobalInput pGI)
	{
		mPayNo=tPayNo;
		tGI=pGI;
	}
	public BqXQTPDataUpLoadTask()
	{
		
	}
	
	public void run()
    {
		dealData();
    }
	public boolean dealData()
	{
		//先准备数据
		getInputData();
		System.out.println("#######税优保全数据上传批处理开始#######");
		
		XqDataUpload();
		
		System.out.println("#######税优保全数据上传批处理结束#######");
		
		return true;
       
	}
	private boolean getInputData()
	{
		if(mPayNo!=null&&!mPayNo.equals(""))
		{
			whereParSql=" and b.PayNo='"+mPayNo+"' ";
		}
		else
		{
			tGI.ComCode="86";
		    tGI.Operator="Server";
		    tGI.ManageCom="86";	
		}
		if(null!=tGI){
			this.managecom=tGI.ManageCom;
		}
	    return true;
	}
	@SuppressWarnings("unchecked")
	private void XqDataUpload()
	{
		//sql效率优化
        String riskSql = "select riskcode from lmriskapp where risktype4 = '4' and TaxOptimal='Y' with ur";
        String riskStr = "";
        SSRS riskSSRS= new ExeSQL().execSQL(riskSql);
        if(null!=riskSSRS && riskSSRS.getMaxRow() > 0){
        	for(int i=1; i <= riskSSRS.getMaxRow(); i++){
        		riskStr +="'"+riskSSRS.GetText(i, 1)+"'";
        		if(i < riskSSRS.getMaxRow()){
        			riskStr +=",";
        		}
        	}
        }
        
        //向中保信平台报送税优产品续期续保信息
	    String ContSql = "select temp.* from (select a.payno,a.incomeno,a.confdate as confdate from ljapay a,ljapayperson b,lcpol c "
				    	+" where a.payno=b.payno "
				    	+" and a.incomeno=b.contno "
				    	+" and b.contno=c.contno "
				    	+" and c.conttype='1' "
				    	+" and c.appflag='1' "
				    	+" and a.incometype='2' "
				    	+" and a.duefeetype='1' "//排除首期
				    	+" and c.riskcode in ("+ riskStr +") "
				    	+" and not exists (select 1 from lserrorlist where transtype in('PRM001','RNW001') "//过滤已经上报成功过的
				    	+" and businessno=a.payno and resultstatus='00') "
				    	+ whereParSql
				    	+" union "
				    	+" select a.payno,a.incomeno,a.confdate as confdate from ljapay a,ljapayperson b,lbpol c "
				    	+" where a.payno=b.payno "
				    	+" and a.incomeno=b.contno "
				    	+" and b.contno=c.contno "
				    	+" and c.conttype='1' "
				    	+" and a.incometype='2' "
				    	+" and a.duefeetype='1' "//排除首期
				    	+" and c.edorno not like 'xb%' "
				    	+" and c.riskcode in ("+ riskStr +") "
				    	+" and not exists (select 1 from lserrorlist where transtype in('PRM001','RNW001') "
				    	+" and businessno=a.payno and resultstatus='00') "
				    	+ whereParSql
				    	+" ) temp "
				    	+" order by temp.confdate "
				    	+" with ur";
					  
	    System.out.println(ContSql);
	    ExeSQL tEexSQL = new ExeSQL();
	    SSRS tSSRS = tEexSQL.execSQL(ContSql);
	    int succCount=1;
	    System.out.println("续期续保共需要上报"+tSSRS.getMaxRow()+"笔业务");
	    for(int i=1;i<=tSSRS.getMaxRow();i++)
	    {
	    	String tPayNo=tSSRS.GetText(i, 1);
	    	String tContNo=tSSRS.GetText(i, 2);
		    VData tVData = new VData();
		    tVData.add(tGI);
		    TransferData  tTransferData = new TransferData();
		    tTransferData.setNameAndValue("PayNo",tPayNo);
		    tTransferData.setNameAndValue("ContNo",tContNo);
		    tVData.add(tTransferData);
		    /**
		     * 判断是续期还是续保业务，通过不同接口进行上报
		     */
		    String str="select 1 from ljapayperson a,lcrnewstatelog b "
				    	+" where a.contno=b.contno "
				    	+" and a.lastpaytodate=b.paytodate "
				    	+" and a.payno='"+ tPayNo +"' "
				    	+" and a.contno='"+tContNo+"' "
				    	+" with ur";
		    String xbFlag=tEexSQL.getOneValue(str);
		    if(null!=xbFlag && !"".equals(xbFlag)){
		    	//续保
		    	XbDataUpLoad tXbDataUpLoad=new XbDataUpLoad();
		    	tXbDataUpLoad.submitData(tVData, "ZBXPT");
		    }else{
		    	//续期
		    	XqPremUpLoad tXqPremUpLoad=new XqPremUpLoad();
		    	tXqPremUpLoad.submitData(tVData, "ZBXPT");
		    }
		    
	    }
	    if(succCount==tSSRS.getMaxRow())
	    {
	      System.out.println("昨天工单"+succCount+"个全部上传@@@！");
	    }
	  
	}
	
	 public static void main(String[] args)
	 {
		 String edorno="32055847828";
		 GlobalInput GI=new GlobalInput();
		 GI.Operator="server";
		 GI.ManageCom="86";
		 BqXQTPDataUpLoadTask tBqTPDataUpLoadTask = new BqXQTPDataUpLoadTask(edorno,GI);
		 tBqTPDataUpLoadTask.run();
	 }
}
