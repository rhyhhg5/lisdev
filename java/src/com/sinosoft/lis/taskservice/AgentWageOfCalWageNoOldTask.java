package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LBGrpContSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AgentWageOfCalWageNoOldTask extends TaskThread{		
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private String  AFlag = "0";//0表示收费的情况,1表示付费情况
    private MMap mMMap = new MMap();
    private static int intMaxIndex;
    
    public AgentWageOfCalWageNoOldTask() {
        intMaxIndex = 1;
    }
    
    public static void main(String[] args)
    {
    	AgentWageOfCalWageNoOldTask tAgentWageOfCalWageNoOldTask = new AgentWageOfCalWageNoOldTask();
    	tAgentWageOfCalWageNoOldTask.run();
    }
    public void run() 
    {   
    	AgentWageOfCalWageNoOldTask a = new AgentWageOfCalWageNoOldTask();
    	SSRS tSSRS=new SSRS();
        tSSRS = a.EasyQuery();
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
			String tManageCom = tSSRS.GetText(i, 1);
			long tStartTime = new Date().getTime();
	    	System.out.println("AgentWageOfCalWageNoOldTask-->run :开始执行");
	    	String tSQL = "select * from lacommision where  caldate is null and  commdire='1' "
	    				//个险直销的，只查询展业证有效的业务员的保单 	
	    				+ " and branchtype ='1' and branchtype2='01' "
	    				+ " and exists (select '1' from LACERTIFICATION where agentcode = lacommision.agentcode and state = '0') and managecom like '"+tManageCom+"%'   "
	    				+ " union"
	    				+ " select * from lacommision where  caldate is null and  commdire='1'"
	    				+ " and ((branchtype ='1' and branchtype2!='01') or branchtype!='1') and managecom like '"+tManageCom+"%' with ur";
	    	dealData(tSQL);
	    	System.out.println("AgentWageOfCalWageNoOldTask-->批处理执行完成:"+PubFun.getCurrentTime());
	    	long tEndTime = new Date().getTime();
	    	System.out.println("薪资月为空的数据处理时间花费"+(tEndTime-tStartTime)+"毫秒，大约"+(tEndTime-tStartTime)/3600/1000+"小时");
		}
//    	long tStartTime = new Date().getTime();
//    	System.out.println("AgentWageOfCalWageNoOldTask-->run :开始执行");
//    	String tSQL = "select * from lacommision where  caldate is null and  commdire='1' "
//    				//个险直销的，只查询展业证有效的业务员的保单 	
//    				+ " and branchtype ='1' and branchtype2='01' "
//    				+ " and exists (select '1' from LACERTIFICATION where agentcode = lacommision.agentcode and state = '0')  "
//    				+ " union"
//    				+ " select * from lacommision where  caldate is null and  commdire='1'"
//    				+ " and ((branchtype ='1' and branchtype2!='01') or branchtype!='1') with ur";
//    	dealData(tSQL);
//    	System.out.println("AgentWageOfCalWageNoOldTask-->批处理执行完成:"+PubFun.getCurrentTime());
//    	long tEndTime = new Date().getTime();
//    	System.out.println("薪资月为空的数据处理时间花费"+(tEndTime-tStartTime)+"毫秒，大约"+(tEndTime-tStartTime)/3600/1000+"小时");
    }
    public boolean dealData(String cSQL)
	{
		System.out.println("AgentWageOfCalWageNoOldTask-->dealData:开始执行");
		System.out.println("AgentWageOfCalWageNoOldTask-->dealData:cSQL"+cSQL);
		LACommisionSet tLACommisionSet = new LACommisionSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLACommisionSet, cSQL);
		do
		{
			tRSWrapper.getData();
			if(null==tLACommisionSet||0==tLACommisionSet.size())
			{
				System.out.println("AgentWageOfCalWageNoOldTask-->dealData:没有薪资月为空的数据！！");
				return true;
			}
		    mMMap = new MMap();
		    ExeSQL tExeSQL = new ExeSQL();
			LACommisionSet tUpdateLACommisionSet = new LACommisionSet();
			for(int i =1;i<=tLACommisionSet.size();i++)
			{
		        LCGrpContSchema tLCGrpContSchema;
		        LCContSchema tLCContSchema;
		        String tContNo = "";
		        String tGrpContNo = "";
		        String getPolDate = "";
		        String tWageNo = "";
		        String custPolDate = "";
		        String tTransType = "";
		        String tMakeDate = "";//---songgh因为一个保单可能会做多个保全项目,所以在进行查找的时候按照提数当天发生的保全项目进行搜索
		        LACommisionSchema tLACommisionSchema = new LACommisionSchema();
		        tLACommisionSchema = tLACommisionSet.get(i);
	            String tBranchType = tLACommisionSchema.getBranchType();
	            String tBranchType2 = tLACommisionSchema.getBranchType2();
	            tContNo = tLACommisionSchema.getContNo();
	            tGrpContNo = tLACommisionSchema.getGrpContNo();
	            tTransType = tLACommisionSchema.getTransType();
	            tMakeDate = tLACommisionSchema.getTMakeDate();
	            String tGrpPolNo = tLACommisionSchema.getGrpPolNo();
	            //如果是团单，查询团单信息
	            if (!tGrpPolNo.equals("00000000000000000000")) {
	                tLCGrpContSchema = new LCGrpContSchema();
	                //查询团险的保单信息
	                tLCGrpContSchema = this.queryLCGrpCont(tGrpContNo);
	                if (tLCGrpContSchema == null) {
	                    continue;
	                }
	                getPolDate = tLCGrpContSchema.getGetPolDate();
	                custPolDate = tLCGrpContSchema.getCustomGetPolDate();
	              //团险保全项目根据财务的确认日期计算佣金月---modifydate:20080329-songgh
	                String tConfDate = "";
	                tConfDate = this.queryConfDate(tGrpContNo,tTransType,tMakeDate,
	                		                         tLACommisionSchema.getTConfDate(),tLACommisionSchema.getReceiptNo());
	                //if (getPolDate != null && !getPolDate.equals("")) {
	                    tLACommisionSchema.setCustomGetPolDate(custPolDate);
	                    tLACommisionSchema.setGetPolDate(getPolDate);
//		                    chargeDate = queryChargeDate(mLACommisionSchema.getGrpPolNo(),
//		                                                 mLACommisionSchema.getAgentCom(),
//		                                                 mLACommisionSchema.
//		                                                 getReceiptNo());

	                    //=== 增加访后付费 日期校验--针对山东分公司需求
	                    String tRiskCode = tLACommisionSchema.getRiskCode(); // 查询出险种信息
	                    String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
	                    String tTransState = tLACommisionSchema.getTransState();
	                    String tVisitTime = this.queryReturnVisit(tGrpContNo);//--查询回访日期
	                    // add new 
	                    String tAgentcode =tLACommisionSchema.getAgentCode() ;

	                    String caldate = this.calCalDateForAll(tBranchType,
	                            tBranchType2,
	                            tLACommisionSchema.getSignDate(),
	                            getPolDate,
	                            tLACommisionSchema.getPayCount(),
	                            tLACommisionSchema.getTMakeDate(), custPolDate,
	                            tLACommisionSchema.getCValiDate(),"02",
	                            tConfDate,AFlag,tLACommisionSchema.getManageCom(),tVisitTime,tRiskFlag,tTransState,tAgentcode,tGrpPolNo,tRiskCode);
	                    if (caldate != null && !caldate.equals("")) {
	                        String sql =
	                                "select yearmonth from LAStatSegment where startdate<='" +
	                                caldate + "' and enddate>='" + caldate +
	                                "' and stattype='91' "; //团险薪资月为91,2005-11-23修改
	                        tExeSQL = new ExeSQL();
	                        tWageNo = tExeSQL.getOneValue(sql);
	                        tLACommisionSchema.setWageNo(tWageNo);
	                        tLACommisionSchema.setCalDate(caldate);
	                        tLACommisionSchema.setTConfDate(tConfDate);//财务确认日期
	                        tLACommisionSchema.setModifyDate(PubFun.getCurrentDate());
	                        tLACommisionSchema.setModifyTime(PubFun.getCurrentTime());
	                        tUpdateLACommisionSet.add(tLACommisionSchema);
	                    }
	            } // 团单结束
	            else { //如果是个单
	                tLCContSchema = new LCContSchema();
	                tLCContSchema = this.queryLCCont(tContNo);
	                if (tLCContSchema == null) {
	                   System.out.println("渠道检验出错!");
	                	continue;
	                }
	                System.out.println("after get LAContSchema!");
	                getPolDate = tLCContSchema.getGetPolDate();
	                custPolDate = tLCContSchema.getCustomGetPolDate();
	                if (getPolDate != null && !getPolDate.equals("")) {
	                    tLACommisionSchema.setCustomGetPolDate(custPolDate);
	                    tLACommisionSchema.setGetPolDate(getPolDate);
	                    String tFlag="01";
	                    if(tLACommisionSchema.getReNewCount()>0)
	                    {
	                        tFlag="02";
	                    }
	                    //简易险
	                    if(tLCContSchema.getCardFlag() != null && !tLCContSchema.getCardFlag().equals("0"))
	                    {
	                        tFlag="02";
	                    }
	                    String tTMakeDate=tLACommisionSchema.getTMakeDate();
	                    //犹豫期退保需要修改tTMakeDate
	                    if(tLACommisionSchema.getTransType().equals("WT"))
	                    {
	                        String tSQL1 =
	                                "select tMakeDate from LACommision where transtype='ZC' and PolNo = '" +
	                                tLACommisionSchema.getPolNo() +
	                                "' and SignDate='" +
	                                tLACommisionSchema.getSignDate() +
	                                "' order by commisionsn with ur ";
	                        tExeSQL = new ExeSQL();
	                        tTMakeDate=tExeSQL.getOneValue(tSQL1);

	                    }

	                    String tConfDate = "";
	                    tConfDate = this.queryConfDate(tGrpContNo,tTransType,tTMakeDate,tLACommisionSchema.getTConfDate(),tLACommisionSchema.getReceiptNo());
	                    //=== 增加访后付费 日期校验--针对山东分公司需求
	                    String tRiskCode = tLACommisionSchema.getRiskCode(); // 查询出险种信息
	                    String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
	                    String tTransState = tLACommisionSchema.getTransState();
	                    String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
	                    // add new 
	                    String tAgentcode =tLACommisionSchema.getAgentCode() ;
	                    String caldate = this.calCalDateForAll(tBranchType,
	                           tBranchType2,
	                           tLACommisionSchema.getSignDate(),
	                           getPolDate,
	                           tLACommisionSchema.getPayCount(),
	                           tTMakeDate, custPolDate,
	                           tLACommisionSchema.getCValiDate(),tFlag,
	                            tConfDate, AFlag,tLACommisionSchema.getManageCom(),tVisitTime,tRiskFlag,tTransState,tAgentcode,tLCContSchema.getGrpContNo(),tRiskCode);
	                   if (!caldate.equals("") && !caldate.equals("0")) {

	                        String sql =
	                                "select yearmonth from LAStatSegment where startdate<='" +
	                                caldate + "' and enddate>='" + caldate +
	                                "' and stattype='5' ";
	                        tExeSQL = new ExeSQL();
	                        tWageNo = tExeSQL.getOneValue(sql);
	                        tLACommisionSchema.setWageNo(tWageNo);
	                        tLACommisionSchema.setCalDate(caldate);
	                        tLACommisionSchema.setTConfDate(tConfDate);
	                        tLACommisionSchema.setModifyDate(PubFun.getCurrentDate());
	                        tLACommisionSchema.setModifyTime(PubFun.getCurrentTime());
	                        tUpdateLACommisionSet.add(tLACommisionSchema);
	                    }
	                }
	            }
	        }
			//提交数据库，防止 内存溢出
			mMMap.put(tUpdateLACommisionSet,"UPDATE");
	        try{
	        	VData tVData = new VData();
	        	tVData.add(mMMap);
	    		PubSubmit tPubSubmit = new PubSubmit();
	    		tPubSubmit.submitData(tVData, "");
	        }catch(Exception ex)
	        {
	        	ex.printStackTrace();
	        }
		}
		while(tLACommisionSet.size()>0);
		System.out.println("AgentWageOfCalWageNoOldTask-->dealData:执行结束 ");
		return true;
	}
	/**
	 * caigang
	 * 查询集体保单表
	 * 输出：如果查询时发生错误则返回null,否则返回Schema
	 */
	private LCGrpContSchema queryLCGrpCont(String tGrpContNo) 
	{
	    LCGrpContDB tLCGrpContDB = new LCGrpContDB();
	    tLCGrpContDB.setGrpContNo(tGrpContNo);
	    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	    if (!tLCGrpContDB.getInfo()) {
	        LBGrpContDB tLBGrpContDB = new LBGrpContDB();
	        tLBGrpContDB.setGrpContNo(tGrpContNo);
	        if (!tLBGrpContDB.getInfo()) {
	            if (tLBGrpContDB.mErrors.needDealError() == true) {
	                // @@错误处理
	                this.mErrors.copyAllErrors(tLBGrpContDB.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "AgentWageCalSaveNewBL";
	                tError.functionName = "queryLCGrpCont";
	                tError.errorMessage = "集体保单备份表查询失败!";
	                this.mErrors.addOneError(tError);
	                return null;
	            }
	            return null;
	        }
	        Reflections tReflections = new Reflections();
	        LBGrpContSchema tLBGrpContSchema = new LBGrpContSchema();
	        tLBGrpContSchema = tLBGrpContDB.getSchema();
	        tReflections.transFields(tLCGrpContSchema, tLBGrpContSchema);
	    } else {
	        tLCGrpContSchema = tLCGrpContDB.getSchema();
	    }
		return tLCGrpContSchema;
	}		
	/**
     * songguohui
     * 查询收费的财务日期
     * 如果是保全收费则取实收表的财务确认日期，如果是保全付费则是付费表的makedate
     * 定期结算的时间是按照最终结算时的财务日期
     * 输出：如果查询时发生错误则返回null,否则返回tconfdate
     */
    private String  queryConfDate(String tGrpContNo,String tTransType,String tMakeDate,String tConfDate,String tPayNo) {
        String mconfdate = tConfDate;
        AFlag = "1";//表示付费
        ExeSQL tExeSQL = new ExeSQL();
        if(tGrpContNo!=null&&!tGrpContNo.equals("00000000000000000000")
        		&&!tGrpContNo.equals(""))//个单不做处理
        {
	        if(tTransType.equals("ZC")||tTransType.equals("ZT"))
	        {
	        	//首先判断是不是保全付费的情况
	        	String getSQL="select distinct paymode from ljaget where actugetno in ("
	        		+"select actugetno from ljagetendorse where grpcontno='"+tGrpContNo
	        		+"' and actugetno='"+tPayNo+"' and endorsementno is not null) with ur";
	        	String tPayMode = tExeSQL.getOneValue(getSQL);
           		if(tPayMode!=null&&!tPayMode.equals("")){
           			if("B".equals(tPayMode))//定期结算中的白条
            		{
            			 mconfdate = "";
            			 AFlag = "0";
            		}
            		else if("J".equals(tPayMode))//已经定期结算
            		{
            			String ttSQL="select actuno,makedate from LJAEdorBalDetail where btactuno='" +tPayNo+"' with ur";
            			SSRS tSSRS=new SSRS();
            			tSSRS=tExeSQL.execSQL(ttSQL);
            			if(tSSRS.getMaxRow()>0){
                    			mconfdate=tSSRS.GetText(1,2);
                				System.out.println("财务确认日期"+mconfdate);
            			}else{
            					mconfdate="";
            			}
//            			String SQL = "select confdate,MAX(INComeno) from ljapay where getnoticeno in " +
//    	        		" (select distinct actuno from ljaedorbaldetail where btactuno in " +
//    	        		" (select distinct actugetno from ljagetendorse where grpcontno in" +
//    	        		" (select grpcontno from LCGrpBalPlan where GrpContno = '"+tGrpContNo+"'))) " +
//    	        		" GROUP BY ConfDate,INComeno " +
//    	        		" order by confdate desc with ur ";
//    			          mconfdate = tExeSQL.getOneValue(SQL);
    			          AFlag = "0";
            		}
           		}else{
//           		首先判断是契约收费还是保全收费
                	String endorseSQL = "select distinct endorsementno from ljapaygrp where grpcontno='"+tGrpContNo+"'" +
                			" and makedate='"+tMakeDate+"' and payno = '"+tPayNo+"' with ur";
                	String endorsementno = tExeSQL.getOneValue(endorseSQL);
                	if(endorsementno!=null&&!endorsementno.equals(""))//为空的时候表示是契约收费,不为空表示是保全的工单号
                	{
                		String incometypeSQL = "select distinct incometype from ljapay where incomeno='"+endorsementno+"'" +
                				" with ur ";
                		String   tIncometype = tExeSQL.getOneValue(incometypeSQL);
                		if(tIncometype!=null&&!tIncometype.equals("")){
                			if("B".equals(tIncometype))//定期结算中的白条
                    		{
                    			 mconfdate = "";
                    			 AFlag = "0";
                    		}
                    		else if("J".equals(tIncometype))//已经定期结算
                    		{
                    			String tSQL="select incometype,confdate from ljapay where getnoticeno in (" +
                    					"select actuno from LJAEdorBalDetail where btactuno='" +tPayNo+"') with ur";
                    			SSRS tSSRS=new SSRS();
                    			tSSRS=tExeSQL.execSQL(tSQL);
                    			if(tSSRS.getMaxRow()>0){
                    				if(tSSRS.GetText(1,1).equals("13")){
                    					mconfdate=tSSRS.GetText(1,2);
                        				System.out.println("财务确认日期"+mconfdate);
                    				}else{
                    					mconfdate="";
                    				}

                    			}else{
                    				tSQL="select othernotype,confdate from ljaget where actugetno in (" +
                					"select actuno from LJAEdorBalDetail where btactuno='" +tPayNo+"') with ur";
                    				tSSRS=tExeSQL.execSQL(tSQL);
                    				if(tSSRS.getMaxRow()>0&&tSSRS.GetText(1,1).equals("13")
                    						&&tSSRS.GetText(1,2)!=null
                    						&&!tSSRS.GetText(1,2).equals("")){
                        				mconfdate=tSSRS.GetText(1,2);
                        				System.out.println("财务确认日期"+mconfdate);
                        			}else{
                        				mconfdate = "";
                        			}

                    			}
//                    			String SQL = "select confdate,MAX(INComeno) from ljapay where getnoticeno in " +
//            	        		" (select distinct actuno from ljaedorbaldetail where btactuno in " +
//            	        		" (select distinct actugetno from ljagetendorse where grpcontno in" +
//            	        		" (select grpcontno from LCGrpBalPlan where GrpContno = '"+tGrpContNo+"'))) " +
//            	        		" GROUP BY ConfDate,INComeno " +
//            	        		" order by confdate desc with ur ";
//            			          mconfdate = tExeSQL.getOneValue(SQL);
            			          AFlag = "0";

                    		}
                		}
                   	 }
           		}
	        }
	        else
	        {
	        	   String AddSQL =  "select max(GETCONFIRMDATE) from ljagetendorse where  " +
	           		" FeeFinaType in (select code from ldcode where codetype='feefinatype2')" +
	           		" and grpcontno = '"+tGrpContNo+"' and feeoperationtype='"+tTransType+"' and  makedate='"+tMakeDate+"' with ur";
	        	   String maxConfdate = tExeSQL.getOneValue(AddSQL);
		           if(maxConfdate!=null&&!maxConfdate.equals(""))//表示是保全加费
		           {
		        	   mconfdate = maxConfdate;
		        	   AFlag = "0";
		           }
		           else //付费
		           {
		        	   AFlag = "1";
		           }
	        }
        }
        return mconfdate;

    }
    /**
     * 查询保单回访信息
     * 目前山东访后付费只作用于 个险保障期间一年期以上产品 及 保证续保产品
     * @param 传入对应的险种号信息
     * @return String
     */
    private String queryRiskFlag(String tRiskCode){
   	 ExeSQL tExeSQL = new ExeSQL();
   	 String tFlag = "";
   	 String sql = "select 'Y' from lmriskapp where riskprop='I'  and  RiskPeriod = 'L' and riskcode = '"+tRiskCode+"' "
                    +" union "
                    +" select 'Y' from lmrisk a ,lmriskapp b where a.RnewFlag='B' and a.riskcode=b.riskcode and b.riskprop='I' and b.riskcode ='"+tRiskCode+"'";
   	 tFlag=tExeSQL.getOneValue(sql);
   	 if(tFlag==null||("").equals(tFlag))
   	 {
   		 tFlag="N";
   	 }
   	 return tFlag;
   	 
    }
    /**
     * 查询保单回访信息
     *
     * @param 
     * @return String
     */
    private String queryReturnVisit(String tContno)
    {
   	 String tVisitTime = "";
   	 String sql = "select CompleteTime from ReturnVisitTable where RETURNVISITFLAG in ('1','4') and POLICYNO = '"+tContno+"' order by makedate desc fetch first 1 rows only with ur";
   	 ExeSQL tExeSQL = new ExeSQL();
   	 tVisitTime = tExeSQL.getOneValue(sql);
   	 if (tExeSQL.mErrors.needDealError() == true) 
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tExeSQL.mErrors);
          CError tError = new CError();
          tError.moduleName = "AgentWageCalSaveNewBL";
          tError.functionName = "queryReturnVisit";
          tError.errorMessage = "回访数据表查询失败!";
          this.mErrors.addOneError(tError);
          return null;
        }
   	 
   	 return tVisitTime;
    }
    /*
    tFlag=01 按照回执回销计算
    tFlag=02 按照实收表的makedate计算
   */
   private String calCalDateForAll(String cBranchType, String cBranchType2,
                                   String cSignDate, String cGetPolDate,
                                   int cPayCount, String cTMakeDate,
                                   String cCustomGetPolDate, String cValidDate,String cFlag,
                                   String cConfDate,
                                   String cAFlag,String cManageCom,String cVisistTime,String cRiskFlag,String cTransState,String cAgentCode,String cGrpcontno,String cRiskCode)
   {
       String tGetPolDate = cGetPolDate == null ? "" : cGetPolDate;
       String tSignDate = cSignDate == null ? "" : cSignDate;
       String tTMakeDate = cTMakeDate == null ? "" : cTMakeDate;
       String tCustomGetPolDate = cCustomGetPolDate == null ? "" : cCustomGetPolDate;
       String tBranchType = cBranchType == null ? "" : cBranchType;
       String tBranchType2 = cBranchType2 == null ? "" : cBranchType2;
       String tPayCount = cPayCount+"";
       String tConfDate = cConfDate ==null? "": cConfDate;
       String tFlag = cFlag== null ? "" : cFlag ;
       String tAFlag = cAFlag== null ? "" : cAFlag ;//团险根据收费方式采取不同的佣金计算方式
       String tManageCom=cManageCom==null ? "" : cManageCom ;
       String tVisistTime = cVisistTime==null ? "" : cVisistTime ;
       String tRiskFlag = cRiskFlag==null ? "" : cRiskFlag ;
       String tTransState = cTransState==null ? "" : cTransState ;
       String tAgentCode =cAgentCode ==null ? "" :cAgentCode;
       String tGrpcontno =cGrpcontno ==null ? "" :cGrpcontno;
       String tRiskCode =cRiskCode ==null ? "" :cRiskCode;
       if (tBranchType == "") 
       {
           return "";
       }
       if (tBranchType2 == "") {
           return "";
       }
       //如果是续保、续期，则不需要按照回执回销计算
       else if (cPayCount>1)
       {
           tFlag="02";
       }

       Calculator tCalculator = new Calculator();
       tCalculator.setCalCode("wageno");
       tCalculator.addBasicFactor("GetPolDate", tGetPolDate);
       tCalculator.addBasicFactor("SignDate", tSignDate);
       tCalculator.addBasicFactor("TMakeDate", tTMakeDate);
       tCalculator.addBasicFactor("CustomGetPolDate", tCustomGetPolDate);
       tCalculator.addBasicFactor("BranchType", tBranchType);
       tCalculator.addBasicFactor("BranchType2", tBranchType2);
       tCalculator.addBasicFactor("ValiDate", cValidDate);
       tCalculator.addBasicFactor("PayCount", tPayCount);
       tCalculator.addBasicFactor("Flag", tFlag);
       tCalculator.addBasicFactor("ConfDate", tConfDate);
       tCalculator.addBasicFactor("AFlag", tAFlag);
       tCalculator.addBasicFactor("ManageCom", tManageCom);
       tCalculator.addBasicFactor("VisistTime", tVisistTime);
       tCalculator.addBasicFactor("RiskFlag", tRiskFlag);
       tCalculator.addBasicFactor("TransState", tTransState);
       tCalculator.addBasicFactor("AgentCode", tAgentCode);
       tCalculator.addBasicFactor("GroupAgentCode", tGrpcontno);
       tCalculator.addBasicFactor("RiskCode", tRiskCode);
       tCalculator.addBasicFactor("VisitFlag", "Y");
       //用来兼容校验回访日期中：河北追加保费的不用校验回访日期
       
//       if("8634".equals(cManageCom.substring(0,4))&&"03".equals(cTransState)&&"1".equals(cBranchType)&&"01".equals(cBranchType2)){
//       	tCalculator.addBasicFactor("VisitFlag", "N");
//       }
//       else{
//       	tCalculator.addBasicFactor("VisitFlag", "Y");
//       }
       
       String tResult = tCalculator.calculate();
       return tResult == null ? "" : tResult;

   }
   /**
    * caigang
    * 查询集体保单表
    * 输出：如果查询时发生错误则返回null,否则返回Schema
    */
   private LCContSchema queryLCCont(String tContNo) {
       LCContDB tLCContDB = new LCContDB();
       tLCContDB.setContNo(tContNo);
       LCContSchema tLCContSchema = new LCContSchema();
       if (!tLCContDB.getInfo()) {
           LBContDB tLBContDB = new LBContDB();
           tLBContDB.setContNo(tContNo);
           if (!tLBContDB.getInfo()) {
               if (tLBContDB.mErrors.needDealError() == true) {
                   // @@错误处理
                   this.mErrors.copyAllErrors(tLBContDB.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "AgentWageCalSaveNewBL";
                   tError.functionName = "queryLCCont";
                   tError.errorMessage = "集体保单备份表查询失败!";
                   this.mErrors.addOneError(tError);
                   return null;
               }
               return null;
           }
           Reflections tReflections = new Reflections();
           LBContSchema tLBContSchema = new LBContSchema();
           tLBContSchema = tLBContDB.getSchema();
           tReflections.transFields(tLCContSchema, tLBContSchema);
       } else {
           tLCContSchema = tLCContDB.getSchema();
       }
       return tLCContSchema;
   }
   public  SSRS EasyQuery() {

       String strError = "";
       Integer intStart = new Integer(String.valueOf("1")); ;
       SSRS tSSRS = new SSRS();

       String sql =
               "select  comcode " +
               "from ldcom where 1=1  and  " +
               "sign='1'  and  length(trim(comcode))=8  order by comcode" ;
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(sql);
       intMaxIndex = tSSRS.getMaxRow();
//       String[] getRowData = tSSRS.getRowData(intStartIndex);
       return tSSRS;
   }
}
