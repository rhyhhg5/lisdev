package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.BonusGetBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LOBonusAssignErrLogSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 分红险分红批处理 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company: sinosoft</p>
 * @author XP
 * @version 1.0
 * @CreateDate：2012-08-02
 */

public class BonusGetBLTask extends TaskThread {
	
    public CErrors mErrors = new CErrors();

    private String mContNo = null;
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    private GlobalInput mGI = new GlobalInput();

    public BonusGetBLTask()
    {
        mGI.Operator = "001";
        mGI.ComCode = "86";
        mGI.ManageCom = mGI.ComCode;
    }

    public BonusGetBLTask(GlobalInput cGlobalInput)
    {
        mGI = cGlobalInput;
    }

    /**
     * 结算指定保单
     * @param cContNo String
     */
    public void runOneCont(String cContNo)
    {
        mContNo = cContNo;
        run();
    }
    
    public void run()
    {
    	int iSuccCount = 0;
        int iFailCount = 0;

        //查询出当前账户最后结算日期比已公布利率日期晚的账户，由于理赔或保全等原因，一个账户可能需要结算多次
        String sql = " select distinct a.polno,b.bonusyear from LCPol a, lmriskbonusrate b "
                     + " where a.RiskCode = b.RiskCode "
                     + " and a.GrpContNo = '00000000000000000000' "
                     + " and a.riskcode in (select riskcode from lmriskapp where risktype4='2') " //分红险
                     + " and a.ManageCom like '" + mGI.ManageCom + "%' "
                     + " and a.appflag='1' "
                     + " and a.stateflag in ('1','2')"
//                     + " and contno='013936348000001' " //TODO:测试用更新记得注掉
//                     是否过了该保单周年日
                     + " and ((month(a.cvalidate)<=6 and year(a.cvalidate)<=int(b.bonusyear) and current date>= (a.cvalidate+ (int(b.bonusyear)-year(a.cvalidate)+1) year)) " 
                     + " or " 
                     + " (month(a.cvalidate)>6 and year(a.cvalidate)<int(b.bonusyear) and current date>= (a.cvalidate+ (int(b.bonusyear)-year(a.cvalidate)) year)))" 
                     + " and not exists (select 1 from LOBonusPol where PolNo = a.PolNo  and FiscalYear=int(b.bonusyear)) "  //险种有效
                     //排除分红险满期红利，分红险满期的上一个年度的红利分配的话，就不在此分配，在分红险满期批处理处理
//                     + " and not exists (select 1 from lobonuspol where polno=a.polno and year(a.enddate-1 year)<=int(FiscalYear) )"
                     + "and ((month(a.cvalidate)<=6 and year(a.cvalidate)<=int(b.bonusyear) and int(b.bonusyear)< ( year(a.Enddate) - 1 )) " 
                     + " or  (month(a.cvalidate)>6 and year(a.cvalidate)<int(b.bonusyear) and int(b.bonusyear)< year(a.Enddate)) ) "
                     + (mContNo != null ? (" and a.ContNo = '" + mContNo + "' ") : "")
                     + " order by a.PolNo, b.bonusyear with ur";
        System.out.println(sql);

        RSWrapper rsWrapper = new RSWrapper();
        if (!rsWrapper.prepareData(null, sql))
        {
            System.out.println("分红险保单分红数据准备失败! ");
            return;
        }
        SSRS tSSRS = rsWrapper.getSSRS();
    	if (tSSRS.getMaxRow() > 0)
        {
	    	for (int i = 1 ; i<= tSSRS.getMaxRow(); i++)
	    	{
	    		String PolNo=tSSRS.GetText(i, 1);
	    		String FisCalYear=tSSRS.GetText(i,2);
                VData tVData = new VData();
                tVData.add(mGI);
                tVData.add(PolNo);
                tVData.add(FisCalYear);

                BonusGetBL tBonusGetBL = new BonusGetBL();

                String errInfo = "险种号:" + tSSRS.GetText(i, 1)
                                 + "年度：" + tSSRS.GetText(i, 2)
                                 + " 结算失败: ";
                try
                {
                    if(!tBonusGetBL.submitData(tVData, ""))
                    {
                        iFailCount++;
                        errInfo += tBonusGetBL.mErrors.getErrContent();
                        mErrors.addOneError(errInfo);
                        System.out.println(errInfo);
                        submiterror(errInfo,PolNo,FisCalYear);
                    }
                    else
                    {
                        iSuccCount++;
                    }
                }
                catch(Exception ex)
                {
                    errInfo += "结算出现未知异常";
                    System.out.println(errInfo);
                    ex.printStackTrace();
                    mErrors.addOneError(errInfo);
                    submiterror(errInfo,PolNo,FisCalYear);
                    iFailCount++;
                }
	    	}
        }
    	rsWrapper.close();

        System.out.println("共计" + iSuccCount + "次账户结算成功，"
                + iFailCount + "个保单结算失败:"
                + mErrors.getErrContent());
    }
    
    public void submiterror(String errInfo,String PolNo,String FisCalYear){
    	errInfo= "结算年度: "+ FisCalYear+ errInfo;
        LOBonusAssignErrLogSchema tLOBonusAssignErrLogSchema =new LOBonusAssignErrLogSchema();
        tLOBonusAssignErrLogSchema.setPolNo(PolNo);
        tLOBonusAssignErrLogSchema.setSerialNo(PubFun1.CreateMaxNo("SERIALNO",mGI.ManageCom));
        tLOBonusAssignErrLogSchema.seterrMsg(errInfo);
        tLOBonusAssignErrLogSchema.setmakedate(mCurrentDate);
        tLOBonusAssignErrLogSchema.setmaketime(mCurrentTime);
        MMap tMMap=new MMap();
        tMMap.put(tLOBonusAssignErrLogSchema, SysConst.INSERT);
        VData data = new VData();
        data.add(tMMap);
        PubSubmit tSubmit = new PubSubmit();
        try{
        	tSubmit.submitData(data, "");
        }catch(Exception e){
        }
    }
    
    public static void main(String[] args) {
    	BonusGetBLTask tBonusGetBLTask =new  BonusGetBLTask();
//    	tBonusGetBLTask.run();
    	tBonusGetBLTask.runOneCont("001017951000001");
    	
	}
    
}
