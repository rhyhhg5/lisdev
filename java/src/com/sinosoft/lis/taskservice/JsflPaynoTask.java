package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.lis.ygz.GetBqMoneyNoBL;
import com.sinosoft.lis.ygz.GetPayNoBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: LCContTranfOutTask </p>
 * <p>Description: 保单迁出同步平台数据的批处理 </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class JsflPaynoTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	
	public JsflPaynoTask() {

	}
	
	/**
	 * run方法
	 */
	public void run(){
		System.out.println("======================JsflPaynoTask Execute  !!!======================");
		
		/**
		 * 往同步保单迁出数据中出入日期的值
		 */
		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		tInputData.add(tTransferData);
		/**
		 * 调用保单迁出同步平台数据的接口
		 */
		
		//=====为价税分离数据补充实收号码
		long satrtGetPayNoBL = System.currentTimeMillis();
		try {
			GetPayNoBL tGetPayNoBL = new GetPayNoBL();
			if(!tGetPayNoBL.submitData(tInputData,"")){
				   this.mErrors.copyAllErrors(tGetPayNoBL.mErrors);
				   System.out.println("从实收表获取payno回写价税分离表报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从实收表获取payno回写价税分离表报错");
			e.printStackTrace();
		}
		long endGetPayNoBL = System.currentTimeMillis();
		System.out.println("========== GetPayNoBL从实收表获取payno回写价税分离批处理-耗时： "+((endGetPayNoBL -satrtGetPayNoBL)/1000/60)+"分钟");


		long startGetBqMoneyNoBL = System.currentTimeMillis();
		try {
			/**
			 * 续期、保全回写实收号码
			 */
			GetBqMoneyNoBL tGetBqMoneyNoBL = new GetBqMoneyNoBL(); 
			if(!tGetBqMoneyNoBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetBqMoneyNoBL.mErrors);
				System.out.println("续期、保全回写实收号报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("续期、保全回写实收号报错");
			e.printStackTrace();
		}
		long endGetBqMoneyNoBL = System.currentTimeMillis();
		System.out.println("========== GetBqMoneyNoBL续期、保全回写实收号批处理-耗时： "+((endGetBqMoneyNoBL -startGetBqMoneyNoBL)/1000/60)+"分钟");

	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		JsflPaynoTask tJsflPaynoTask = new JsflPaynoTask();
		
		tJsflPaynoTask.run();
	}
	
}
