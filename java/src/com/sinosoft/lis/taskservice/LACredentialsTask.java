package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;


/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: SinoSoft</p>
 * @author xiangchun
 * @version 1.0
 */

public class LACredentialsTask extends TaskThread {

    private String currentDate = PubFun.getCurrentDate();
    private SSRS mSSRS1 = new SSRS();
    public LACredentialsTask() {

    }


    public void run() {
        System.out.println("参数个数:" + mParameters.size());
        System.out.println("");
        // Write your code here
       
        LACredentialsTask a = new LACredentialsTask();
        this.mSSRS1=a.EasyQuery();
        for (int i = 1; i <= mSSRS1.getMaxRow(); i++)
        {
            //先查询提数的记录集
            String tManageCom = mSSRS1.GetText(i, 1) ;
          if (!a.getDate(currentDate, tManageCom))
             {
                return;
             }
        }
     }

    public static void main(String[] args) {
    }

    //
    public boolean getDate(String tStartDate,String tManageCom) {
        String FlagStr = "";
        GlobalInput tG = new GlobalInput();
        tG.Operator = "000"; //系统自签
        tG.ManageCom = "86"; //默认总公司
        //接收信息
      

        VData tVData=new VData();
        tVData.addElement(tG);
        tVData.addElement(tStartDate);
        tVData.addElement(tManageCom);
        
        LACredentialsBL tLACredentialsBL = new LACredentialsBL();
        if (!tLACredentialsBL.submitData(tVData,""))
        {
        FlagStr="Fail";
        return false;
        }
        else
        {
              System.out.print(" 成功! ");
              return true;
        }

    }
    // 获取分公司编码
    public  SSRS EasyQuery() {    	
        SSRS tSSRS = new SSRS();
        String sql =
                "select  comcode " +
                "from ldcom where 1=1  and  " +
                "sign='1'  and  length(trim(comcode))=4  order by comcode" ;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        return tSSRS;
    }


}
