package com.sinosoft.lis.taskservice;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

public class YJBatchSendData extends TaskThread{

	private String startDate = "";
    private String endDate = "";
    private GlobalInput mGlobalInput = new GlobalInput();
    private LJAGetSet mLJAGetSet = new LJAGetSet();
    
    
    /** 应收总金额 */
    private double totalMoneyF = 0;
    /** 应收总笔数 */
    int sumNumF = 0;
    /** 应收批次号 */
    String serialNoF = "";
    //限制提盘的最大条数
    private  int limite =  0;
    
    public void run(){
        System.out.println("---YJBatchSendDataTask开始---");
        submitData();
        System.out.println("---YJBatchSendDataTask结束---");
    }
    
    private boolean submitData(){
        if (!getInputData()){
            return false;
        }
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData()){
            return false;
        }
        System.out.println("---End dealData---");
        return true;
    }
    
    private boolean getInputData(){
        try{
//          startDate = "2012-1-1"; //提取数据的起始日期
//          endDate = PubFun.getCurrentDate();
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        	Date today = new Date();
        	endDate = sdf.format(today);//当前日期
        	System.out.println("当前日期："+endDate);
            Calendar theCa = Calendar.getInstance();
            theCa.setTime(today);
            theCa.add(Calendar.DATE, -30);//最后一个数字30可改，30天的意思
            Date start = theCa.getTime();
            startDate = sdf.format(start);
            System.out.println("30天前日期："+startDate);
            assignGlobalInput();
        }
        catch (Exception e){
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }
        return true;
    }
	
    private boolean dealData(){
    	ExeSQL tExeSQL = new ExeSQL();
    	SSRS tSSRS2 = new SSRS();
        try{
        	//1、提取佣金应付数据
        	if(!getSendBankData("2")){
        		System.out.println("提取应付数据失败!");
        		return false;
        	}else{
        		System.out.println("提取应付数据成功 ^-^");
        	}
        	//4、将提取的数据按机构分别生成批次
        	//将提取的数据不分批次
        	 tSSRS2 = tExeSQL.execSQL("select distinct PubToPubPaymentNo from laintercharge where f11 in "
        	 			+ "	(select actugetno from ljaget a where cansendbank = '9' and BankOnTheWayFlag = '1' "
                    	+ "	and bankcode in ( select bankcode from ldbankunite where bankunitecode='7702' ) "
                    	+ "	and othernotype = '25' "
                    	+ "	and not exists(select 1 from lysendtobank where a.actugetno = paycode )) ");
                //按机构分别生成批次
                for (int j = 1; j <= tSSRS2.MaxRow; j++)
                {
                    getSendBankConf( tSSRS2.GetText(j,1), "2");
                }
         	
        }catch(Exception ex){
        	System.out.println("---数据审核类报错---TT----");
        	 ex.printStackTrace();
        }
    	
    	return true;
    }
    
    /**
     * 提取发送银行的应收/应付数据
     * @param flag[1:应收;2:应付]
     * @return boolean
     */
    private boolean getSendBankData(String flag){
    	
       if("2".equals(flag)){
        	try{
        		LJAGetSet tLJAGetSet = getLJAGet(startDate, endDate);
                if (tLJAGetSet == null) {
                    throw new NullPointerException("佣金类总应付表处理失败！");
                }
                if (tLJAGetSet.size() == 0) {
                    throw new NullPointerException("佣金类总应付表无数据！");
                }
                System.out.println("---End getLJAGetByPaydate---");
              //查询出cansendbank状态为9的数据
        		tLJAGetSet = modifyBankFlag(tLJAGetSet);
                mLJAGetSet.clear();
                mLJAGetSet.add(tLJAGetSet);
                MMap map = new MMap();
                map.put(mLJAGetSet, "UPDATE");
                VData tVData = new VData();
                tVData.add(map);
                System.out.println("Start PubSubmit BLS Submit...");
                PubSubmit tPubSubmit = new PubSubmit();
                if(!tPubSubmit.submitData(tVData, "UPDATE")){
                	System.out.println("提交数据库出错...");
                	return false;
                }
        	}catch(Exception e){
        		// @@错误处理
                CError.buildErr(this, "数据处理错误:" + e.getMessage());
                return false;
        	}
        }
        
    	return true;
    }
    
    /**
     * 提取发送银行的应收数据
     * @param 
     * @return 
     */
    
    
    /**
     * 提取发送银行的应付数据
     * @param flag[1:应收;2:应付]
     * @return boolean
     */
   
    private LJAGetSet getLJAGet(String startDate, String endDate){
    	
    	LJAGetSet tLJAGetSet = new LJAGetSet();
    	
    	//获取银行信息，校验是否是金联
    	LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode("7702");
        if(!tLDBankDB.getInfo()){
        	CError.buildErr(this, "获取银行信息（LDBank）失败");
            return null;
        }
    	
         if(tLDBankDB.getBankUniteFlag().equals("2")){
        	LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();

            //unitegroupcode 1代表支持代收 2代表支持代付 3代表支持代收代付
            //String bankSql = "select * from ldbankunite where bankunitecode='7707' and unitegroupcode in ('2','3') ";
            	tLJAGetSet.add(getLJAGetByPaydate(startDate, endDate));
        }
        
    	return tLJAGetSet;
    }
    
private LJAGetSet getLJAGetByPaydate(String startDate, String endDate){
    	
        String tsql = " select * from LJAGet "
				+ " where ShouldDate >= '"+startDate+"' "
				+ " and ShouldDate <= '"+endDate+"' "
				+ " and ManageCom like '"+mGlobalInput.ComCode+"%' "
				+ " and PayMode='11' "
				+ " and BankCode in ( select bankcode from ldbankunite where bankunitecode='7702' and unitegroupcode in ('2','3') )"
				+ " and EnterAccDate is null and ConfDate is null "
				+ " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null) "
				+ " and (CanSendBank is null or  CanSendBank = '0') "
				+ " and (BankAccNo <> '') and (AccName <> '') and sumgetmoney<>0 "
				+ " and bankcode not in ('489102','999100') "
				+ " and bankcode not in (select bankcode from ldbankunite a where a.bankunitecode in ('7701','7703','7704','7705','7706','7709') and a.bankcode=bankcode) "
				+ " and othernotype = '25' with ur";

	
	System.out.println("tsql:"+tsql);	    	 
	
	LJAGetDB tLJAGetDB = new LJAGetDB();
	LJAGetSet tLJAGetSet = tLJAGetDB.executeQuery(tsql);
	return tLJAGetSet;

    }
    
    private boolean getSendBankConf(String pubtopubpaymentNo,String flag){
    	
    	String sql_limit = "select code1 from ldcode1 where codetype='batchsendlimite' and code='limite'";
        ExeSQL limiteSql = new ExeSQL();
        String limiteCont = limiteSql.getOneValue(sql_limit);
        try {
        	limite = Integer.parseInt(limiteCont);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    	
    	
    	 if("2".equals(flag)){
    		VData tVData = new VData();
    		LJAGetDB tLJAGetDBtol = new LJAGetDB();
    		LJAGetDB tLJAGetDB = new LJAGetDB();
    		LYBankLogDB  tLYBankLogDB =new LYBankLogDB();
            System.out.println("End PubSubmit BLS Submit...");

            String sql2=" select *  from ljaget a where actugetno in (select  f11 from laintercharge  where pubtopubpaymentNo = '"+pubtopubpaymentNo+"' )"
            		+ " and bankonthewayflag= '1' and cansendbank='9'"
            		+ "	and bankcode in ( select bankcode from ldbankunite where bankunitecode='7702' )"
            		+ "	and othernotype = '25' "
            		+ "	and not exists(select 1 from lysendtobank where a.actugetno = paycode )";
                    	
            LJAGetSet sLJAGetSettol = tLJAGetDBtol.executeQuery(sql2);
            System.out.println("佣金总条数" + sLJAGetSettol.size());

            int tStartF = 1;
            LJAGetSet tLJAGetSet = new LJAGetSet();
            tLJAGetSet = tLJAGetDB.executeQuery(sql2,tStartF,limite);
            LYBankLogSet tLYBankLogSet=new LYBankLogSet();
            System.out.println("第一个付费批次的数据量" + tLJAGetSet.size());
            StringBuffer mSeriaPrefix = new StringBuffer();
            do{
                if(tLJAGetSet.size()!=0){
               	 //生成批次号，要在循环外生成
                	int i=0;
                	serialNoF = pubtopubpaymentNo;
                	String sql3="select * from lybanklog where serialno ='"+serialNoF+"'";
                	tLYBankLogSet=tLYBankLogDB.executeQuery(sql3);
                	do{
                		i++;
                		if(tLYBankLogSet!=null&&tLYBankLogSet.size()>0)
                    	{
                			if(i>=2)
                			{
                				serialNoF=serialNoF.substring(0, serialNoF.lastIndexOf("C")+1)+i;
                			}
                			else
                			{
                				serialNoF = serialNoF+"C"+i;
                			}
                		}
                	String sql4="select * from lybanklog where serialno ='"+serialNoF+"'";
                	
                		tLYBankLogSet=tLYBankLogDB.executeQuery(sql4);
                	}
                	while(tLYBankLogSet!=null && tLYBankLogSet.size()!=0);
                	
                }  
                   
                   System.out.println("生成应付费用：批次号：" + serialNoF);
                  LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();
                tLYSendToBankSet = getSendToBankF(tLJAGetSet);
               if (tLYSendToBankSet.size() != 0){
                   String serialno = tLYSendToBankSet.get(1).getSerialNo();
                   LYBankLogSchema tLYBankLogSchema= new LYBankLogSchema();
                   tLYBankLogSchema = getBankLogF("86000000");
                   tLYBankLogSchema.setSerialNo(serialno);
                   tLYBankLogSchema.setOutFile(serialno+".xls");
                   //提交数据
                   MMap map2 = new MMap();
                   map2.put(tLYSendToBankSet, "INSERT");
                   map2.put(tLYBankLogSchema, "INSERT");

                   tVData.clear();
                   tVData.add(map2);
                   PubSubmit ttPubSubmit2 = new PubSubmit();
                   if (!ttPubSubmit2.submitData(tVData, "")){
                       System.out.println("数据提交失败！");
                       return false;
                   }
               
                
               }else{
                   System.out.println("没有佣金代付费数据！！！");
                   return false;
               }
            	
               tLJAGetSet = tLJAGetDB.executeQuery(sql2,tStartF,limite);
            }
            while(tLJAGetSet!=null && tLJAGetSet.size()!=0);

    	}
    	return true;
    }
    

    /**
     * 生成送银行表应付数据
     * @param LJAGetSet
     * @return
     */
    public LYSendToBankSet getSendToBankF(LJAGetSet tLJAGetSet){
    	
    	//总金额
        double dTotalMoney = 0;
        sumNumF = 0;
        
        LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();
    
        for (int i = 0; i < tLJAGetSet.size(); i++){
            LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i + 1);

            //生成送银行表数据
            LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
            //设置统一的批次号
            tLYSendToBankSchema.setSerialNo(serialNoF);
            //收费标记
            tLYSendToBankSchema.setDealType("F");
            tLYSendToBankSchema.setPayCode(tLJAGetSchema.getActuGetNo());
            tLYSendToBankSchema.setBankCode(tLJAGetSchema.getBankCode());
            tLYSendToBankSchema.setAccName(tLJAGetSchema.getAccName());
            tLYSendToBankSchema.setAccNo(tLJAGetSchema.getBankAccNo());
            tLYSendToBankSchema.setPolNo(tLJAGetSchema.getOtherNo());
            tLYSendToBankSchema.setNoType(tLJAGetSchema.getOtherNoType());
           
            tLYSendToBankSchema.setComCode(tLJAGetSchema.getManageCom());
            tLYSendToBankSchema.setAgentCode(tLJAGetSchema.getAgentCode());
            tLYSendToBankSchema.setPayMoney(tLJAGetSchema.getSumGetMoney());
            tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
            tLYSendToBankSchema.setDoType("0");
            //因为没有为发送银行盘表设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
            tLYSendToBankSchema.setModifyDate(PubFun.getCurrentDate());
            tLYSendToBankSchema.setModifyTime(PubFun.getCurrentTime());
            if (!StrTool.cTrim(tLJAGetSchema.getDrawerID()).equals("")){
                tLYSendToBankSchema.setIDType("0");
                tLYSendToBankSchema.setIDNo(tLJAGetSchema.getDrawerID());
            }
            if (!StrTool.cTrim(tLJAGetSchema.getDrawer()).equals("")){
                tLYSendToBankSchema.setName(tLJAGetSchema.getDrawer());
            }

            tLYSendToBankSet.add(tLYSendToBankSchema);

            //累加总金额和总数量
            dTotalMoney = dTotalMoney + tLJAGetSchema.getSumGetMoney();
            //转换精度
            dTotalMoney = Double.parseDouble((new DecimalFormat("0.00"))
                    .format(dTotalMoney));
            sumNumF = sumNumF + 1;
        }
        totalMoneyF = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));

        return tLYSendToBankSet;
    }
    
    /**
     * 生成银行日志表数据(应付)
     * @return
     */
    public LYBankLogSchema getBankLogF(String managecom)
    {
        LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

        tLYBankLogSchema.setBankCode("7702");
        tLYBankLogSchema.setLogType("F");
        tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setSendDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setTotalMoney(totalMoneyF);
        tLYBankLogSchema.setTotalNum(sumNumF);
        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        tLYBankLogSchema.setComCode(managecom);

        return tLYBankLogSchema;
    }
    
    
    /**
     * 修改应收表银行在途标志,记录发送银行次数
     * @param tLJSPaySet
     * @return
     */
    private LJAGetSet modifyBankFlag(LJAGetSet tLJAGetSet){
    	
    	for (int i = 0; i < tLJAGetSet.size(); i++){
            System.out.println("modifyBankFlag:");
            LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i + 1);

            tLJAGetSchema.setBankOnTheWayFlag("1");
            tLJAGetSchema.setCanSendBank("9");
            tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
            tLJAGetSchema.setOperator(mGlobalInput.Operator);
            //记录发送银行次数
            tLJAGetSchema.setSendBankCount(tLJAGetSchema.getSendBankCount() + 1);
            
            tLJAGetSet.set(i + 1, tLJAGetSchema);
        }
        return tLJAGetSet;
    }
    
    /**
     * 定义GlobalInput的属性值
     */
    private void assignGlobalInput(){
    	mGlobalInput.ManageCom = "86";
    	mGlobalInput.ComCode = "86";
    	mGlobalInput.Operator = "sys";
    }
    

}
