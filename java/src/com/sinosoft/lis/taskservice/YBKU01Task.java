package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclientybk.inf.U01;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *     核保信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class YBKU01Task extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    String opr = "";
    
    public YBKU01Task()
    {}

    public void run()
    {
    	U01  hb = new U01();
    	String SQL = "select prtno from lcpol where riskcode in(select code from ldcode where codetype='ybkriskcode') and uwflag is not null and uwdate is not null and  "+ 
    	"not exists(select 1 from  YBKErrorList where businessno=lcpol.prtno and resultstatus='1' and transtype='U01' )";
    	
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String tCaseNo = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("PrtNo", tCaseNo);
    			tVData.add(tTransferData);
    	    	if(!hb.submitData(tVData, "YBKPT"))
    	         {
    	    		 System.out.println("核保信息上传批处理出问题了");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             continue;
    	         }else{
    	        	 System.out.println("核保信息上传批处理成功了");
    	        	 opr ="ture";
    	         }
    		}
    	}
    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

    public static void main(String[] args)
    {

    	YBKU01Task hbTask = new YBKU01Task();
    	hbTask.run();
    }
}




