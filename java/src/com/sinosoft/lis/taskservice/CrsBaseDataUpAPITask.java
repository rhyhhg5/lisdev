package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.crssale.up.CrsBaseDataUpAPI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;

public class CrsBaseDataUpAPITask extends TaskThread {
	
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private MMap map = null;

    private GlobalInput mGlobalInput = null;
    
    public CrsBaseDataUpAPITask(){
    	
    }
    
    
    public void run()
    {
    	CrsBaseDataUpAPI tCrsBaseDataUpAPI = new CrsBaseDataUpAPI();
    	if(tCrsBaseDataUpAPI.run())
    	{
    		System.out.println("集团提数批处理正常完成！");
    	}
    	else
    	{
    		System.out.println("集团提数批处理异常终止，请检查！");
    	}
    }
    
    public static void main(String[] args)
    {
    	CrsBaseDataUpAPITask tCrsBaseDataUpAPITask = new CrsBaseDataUpAPITask();
    	tCrsBaseDataUpAPITask.run();
    }
    
}
