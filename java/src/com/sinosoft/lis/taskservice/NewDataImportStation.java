package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.NewDataImport;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;

public class NewDataImportStation {

	/**
	 * @param args
	 * 
	 * <p>Title: GuangDong Province list</p>
	 *
	 * <p>Description: It is a auto program to creat the table of be named "LCCONTACT"</p>
	 *
	 * <p>Copyright: Copyright (c) 2011</p>
	 *
	 * <p>Company: sinosoft&&PICC(H)</p>
	 *
	 * @author Ryan Tianzheng Yang
	 * 
	 * @version 1.0
	 */
	
	CErrors mErrors = new CErrors();

    MMap mMap = new MMap();

    GlobalInput mGlobalInput = new GlobalInput();
    
	public NewDataImportStation() 
    {
    }

    public void run()
    {
     //TODO English info for maintaining 
    System.out.println("GuangDong Province the customer-truth" +
    		" verifying list supportion beginning");
    NewDataImport tNewDataImport = new NewDataImport();
    try 
    {
    	tNewDataImport.dealData();
	} 
    catch (Exception e) 
    {
		e.printStackTrace();
	}
    //TODO English info for maintaining and end it 
    System.out.println("GuangDong Province the customer-truth " +
    		"verifying list supportion endding");
}
    public static void main(String[] args)
	{
		// TODO Auto-generated method stub
        // TODO Do what you want to do
    	NewDataImportStation tmain_NewDataImportStation = new NewDataImportStation();
    	
    	tmain_NewDataImportStation.run();
	}
}