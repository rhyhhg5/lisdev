package com.sinosoft.lis.taskservice;

import com.sinosoft.httpclientybk.deal.YbkXBTB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbkXBTask extends TaskThread {
	
	//查找范围
    private static final int STEP_DAYS=60;
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();
    // 得到当前时间
    private String mCurrentDate = PubFun.getCurrentDate();
    // 区分页面抽档标志，false为批处理
    private Boolean flag = false;
    
    // 输入数据的容器
    private VData tVData = new VData();
    
    private String StartDate="2005-01-01";
    private String EndDate="";
    private GlobalInput mGlobalInput = null;

    public YbkXBTask()
    {
        mGlobalInput = new GlobalInput();
        mGlobalInput.Operator="YBK";
        mGlobalInput.ComCode="86";
        mGlobalInput.ManageCom="86310000";
    }


    /**
     * 实现TaskThread的接口方法run
     * 由TaskThread调用
     * */
    public void run()
    {   
    	EndDate=addDate(mCurrentDate,STEP_DAYS);
    	//EndDate = "2018-12-30";
    	System.out.println(EndDate);
    	dealDueFee();
    }
    
    //普通险种
    private void dealDueFee()
    {
    	YbkXBTB tYbkXBTB = new YbkXBTB();
    	initData();
        try
        {
            //普通险种续期、续保催收
        	tYbkXBTB.submitData(tVData, "INSERT");
        }
        catch(Exception ex)
        {
            this.mErrors = tYbkXBTB.mErrors;
            ex.getMessage();
        }
    }
    
    public void initData()
    {
    	tVData.clear();
        System.out.println(EndDate);
        TransferData tTransferData=new TransferData();
        tTransferData.setNameAndValue("StartDate",StartDate);
        tTransferData.setNameAndValue("EndDate",EndDate);
        tTransferData.setNameAndValue("ManageCom","86310000");
        tTransferData.setNameAndValue("BusinessFlag","2"); //万能险标识，这处理普通险种所以为空。
        tTransferData.setNameAndValue("flag",flag);
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);
    }
    
    private String addDate(String date,int days)
    {
        return PubFun.calDate(date,days-1,"D","1999-12-31");
    }
	
	public static void main(String[] args) {
		YbkXBTask tYbkXBTask = new YbkXBTask();
		tYbkXBTask.run();
	}

}
