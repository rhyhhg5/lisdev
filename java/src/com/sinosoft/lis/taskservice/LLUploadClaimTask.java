package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclient.inf.UploadClaim;
import com.sinosoft.lis.yibaotong.LLWBTransFtpXmlCase;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *外包案件信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class LLUploadClaimTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();


    String opr = "";
    public LLUploadClaimTask()
    {}

    public void run()
    {
    	UploadClaim  tUploadClaim = new UploadClaim();
    	String SQL = " SELECT distinct otherno " +
    			" from ljagetclaim a" +
    			" where  not exists(select 1 from  lserrorlist where a.otherno=businessno and transtype='CLM001' )  " +
    			" and a.riskcode in(select riskcode from lmriskapp where taxoptimal='Y') and a.othernotype='5' and a.getdutykind in('100','200')  ";
    	
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String tCaseNo = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("ClaimNo", tCaseNo);
    			tVData.add(tTransferData);
    	    	if(!tUploadClaim.submitData(tVData, "ZBXPT"))
    	         {
    	    		 System.out.println("案件信息上报出问题了");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             continue;
    	         }else{
    	        	 System.out.println("案件信息上报成功了");
    	        	 opr ="ture";
    	         }
    		}
    	}
    	

    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
   

   
    public static void main(String[] args)
    {

    	LLUploadClaimTask tLaHumandeveITask = new LLUploadClaimTask();
        tLaHumandeveITask.run();
//        tGEdorMJTask.oneCompany(tGlobalInput);
    }
}




