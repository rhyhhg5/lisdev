package com.sinosoft.lis.taskservice;

import com.sinosoft.httpclient.inf.BusinessCheck;
import com.sinosoft.httpclient.inf.PolicyTransOutRegisterQuery;
import com.sinosoft.httpclient.inf.PolicyTransformRemainderQuery;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: LCContTranfOutTask </p>
 * <p>Description: 保单迁出同步平台数据的批处理 </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class End009Task extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储传入的容器*/
	private VData mInputData = new VData();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	/**存储操作数据类型*/
	private String mOperate = "";
	/**
	 * run方法
	 */
	public void run(){
		System.out.println("");
		System.out.println("======================-End010-CHK001-End009Task Execute  !!!======================");
		System.out.println("参数个数："+mParameters.size());
		System.out.println("                  ");
		/**
		 * 往同步保单迁出数据中出入日期的值
		 */
		String tCurrentDate = PubFun.getCurrentDate();//当前日期
		/**
		 * 将同步的起止日期都置为当前日期，因为是一天一跑批处理
		 */
		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("QueryStartDate", tCurrentDate);
		tTransferData.setNameAndValue("QueryEndDate", tCurrentDate);
		tInputData.add(tTransferData);
		/**
		 * 调用保单迁出同步平台数据的接口-End009
		 */
		PolicyTransOutRegisterQuery tPolicyTransOutRegisterQuery = new PolicyTransOutRegisterQuery();
		
		if(!tPolicyTransOutRegisterQuery.submitData(tInputData,"ZBXPT")){
		   this.mErrors.copyAllErrors(tPolicyTransOutRegisterQuery.mErrors);
	       mResult.clear();
	       mResult.add(mErrors.getFirstError());
		}
		
		/**
		 * 调用保单转入余额信息查询数据接口 - End010
		 */
		PolicyTransformRemainderQuery tPolicyTransformRemainderQuery = new PolicyTransformRemainderQuery();
		
		if(!tPolicyTransformRemainderQuery.submitData(tInputData,"ZBXPT")){
		   this.mErrors.copyAllErrors(tPolicyTransformRemainderQuery.mErrors);
	       mResult.clear();
	       mResult.add(mErrors.getFirstError());
		}
		
		/**
		 * 调用交易核对查询接口 - CHK001
		 */
		BusinessCheck tBusinessCheck = new BusinessCheck();
		
		if(!tBusinessCheck.submitData(tInputData,"ZBXPT")){
		   this.mErrors.copyAllErrors(tBusinessCheck.mErrors);
	       mResult.clear();
	       mResult.add(mErrors.getFirstError());
		}
	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		End009Task tLCContTranfOutTask = new End009Task();
		tLCContTranfOutTask.run();
	}
	
}
