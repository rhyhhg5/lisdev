package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author zhang
 * @version 1.1
 */

public class LLTPCroeResultTask extends TaskThread {

	private ExeSQL mExeSQL = new ExeSQL();

	// TransInfo 标识信息
	private Element tPACKET = null;
	
    //TransInfo 标识信息
	private Element tHEAD = null;
	
    //TransInfo 标识信息
	private Element tBODY = null;

	// 案件
	private Element tGrpContInfo = null;

	// 案件
	private Element tContInfos = null;

	// 案件信息SQL
	private String mGrpContInfoSQL = "";

	// 案件信息SSRS
	private SSRS tGrpContInfoSSRS = null;

	// 案件数
	private int GrpContInfolen = 0;

	/** 应用路径 */
	private String mURL = "";

	/** 文件路径 */
	private String mFilePath = "";
	String opr="true";
	// 执行任务
	public void run() {
		
		String outSource="select code,codealias from ldcode where codetype  ='thridcompany'  order by othersign ";
    	SSRS outSources=mExeSQL.execSQL(outSource);
		if(outSources.MaxRow<=0){
			return ;
		}
		for (int i = 1; i <= outSources.MaxRow; i++) {
			String code=outSources.GetText(i, 1);
			String codealias=outSources.GetText(i, 2);
			if(code==null ||  "".equals(code)  ){
				continue ;
			}
		String mngcoms="Select code,codealias From Ldcode  Where Codetype = '"+code+"ComCode'";
		
		SSRS mngcom=mExeSQL.execSQL(mngcoms);
		if(mngcom.MaxRow<=0){
			continue ;
		}
		for (int im = 1; im <= mngcom.MaxRow; im++) {
			String mng=mngcom.GetText(im, 1);
			String mngtype=mngcom.GetText(im, 2);
			if(mng==null || mngtype==null || "".equals(mng) || "".equals(mngtype)){
				continue ;
			}
			getInputData(code,codealias==null ? "": codealias,mng);
			
			if(!dealData(code,codealias==null ? "": codealias,mng,mngtype)){
				 System.out.println("理赔反馈出问题了");
	             opr="false";
			}else{
				 System.out.println("理赔反馈成功了");
			}
		}
	}
	}
	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
	private void getInputData(String code,String codealias,String mng ) {

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);

		mURL = mURL + "temp_lp/"+codealias+"TPClaim/"+mng+"/CoreResult/"+PubFun.getCurrentDate()+"/";
		//本地测试 存储核心路径
//		mURL = "D:\\temp_lp\\TPClaim\\"+mng+"\\CoreResult\\"+PubFun.getCurrentDate()+"\\";
		//本地测试
		
		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}

	}

	public boolean dealData(String code,String codealias,String mng ,String mngtype) {
		
		mGrpContInfoSQL = "select a.caseno,b.grpcontno,a.idno,a.endcasedate," +
				" (select claimno from llhospcase where caseno=a.caseno fetch first row only),sum(b.pay)," +
				" (select sum(OutDutyAmnt) from llclaimdetail where caseno=a.caseno)" +
				" from llcase a,ljagetclaim b where a.caseno=b.otherno and b.othernotype='5' " +
				" and exists(select 1 from llhospcase where llhospcase.caseno=a.caseno and llhospcase.casetype='17' and llhospcase.HCNO='"+mngtype+"'   )" + //外包批次多事件
				" and a.rgtstate in('11','12') and b.makedate = Current Date - 1 Day" +  //前一天
				" group by a.caseno,b.grpcontno,a.idno,a.endcasedate "
				;

		tGrpContInfoSSRS = mExeSQL.execSQL(mGrpContInfoSQL);
		GrpContInfolen = tGrpContInfoSSRS.getMaxRow();

		if (GrpContInfolen < 1) {
			System.out.println("理赔完成SQL未查询到数据："+mGrpContInfoSQL);
			return true;
		}

		tPACKET = new Element("PACKET");
		Document Doc = new Document(tPACKET);
		tPACKET.addAttribute("type", "REQUEST");
		tPACKET.addAttribute("version", "1.0");
		tHEAD = new Element("HEAD");
		tHEAD.addContent(new Element("REQUEST_TYPE").setText("TP03"));
		tHEAD.addContent(new Element("TRANSACTION_NUM").setText("TP03"+mng+"0000"+PubFun.getCurrentDate2()+"0000000001"));
		tPACKET.addContent(tHEAD);
		tBODY = new Element("BODY");
		tContInfos = new Element("LLCASELIST");
		
		for (int i = 1; i <= GrpContInfolen; i++) {
			
			tGrpContInfo = new Element("LLCASE_DATA");

			tGrpContInfo.addContent(new Element("CASENO")
					.setText(tGrpContInfoSSRS.GetText(i, 1)));
			tGrpContInfo.addContent(new Element("GrpcontNo")
					.setText(tGrpContInfoSSRS.GetText(i, 2)));
			tGrpContInfo.addContent(new Element("IdNo")
					.setText(tGrpContInfoSSRS.GetText(i, 3)));
			tGrpContInfo.addContent(new Element("EndCaseDate")
					.setText(tGrpContInfoSSRS.GetText(i, 4)));
			tGrpContInfo.addContent(new Element("CLAIMNO")
					.setText(tGrpContInfoSSRS.GetText(i, 5)));
			tGrpContInfo.addContent(new Element("RealPay")
					.setText(tGrpContInfoSSRS.GetText(i, 6)));
			tGrpContInfo.addContent(new Element("OutDutyAmnt")
					.setText(tGrpContInfoSSRS.GetText(i, 7)));
			tContInfos.addContent(tGrpContInfo);
			
		}		
			
			tBODY.addContent(tContInfos);
			tPACKET.addContent(tBODY);
			

			mFilePath = mURL +code.substring(0, 2)+ "_LPCR_" + PubFun.getCurrentDate2() + PubFun.getCurrentTime2() +".xml";
			XMLOutputter outputter = new XMLOutputter("  ", true, "GBK");

			try {
				outputter.output(Doc, new FileOutputStream(mFilePath));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!sendXML(code,codealias,mFilePath,mng)){
			  System.out.println("理赔完成反馈数据向FTP发送文件失败！");
			}

		

		return true;
	}

	private boolean sendXML(String code,String codealias,String cXmlFile,String mng){

		String getIPPort = "select codename ,codealias,comcode from ldcode where codetype='"+codealias+"TPClaim' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='"+codealias+"TPClaim' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			System.out.println("FTP服务器IP、用户名、密码、端口都不能为空，否则无法发送xml");
			return false;
		}
		
		String tIP = tIPSSRS.GetText(1, 1);
	    String tPort = tIPSSRS.GetText(1, 2);
	    String tUserName = tUPSSRS.GetText(1, 1);
	    String tPassword = tUPSSRS.GetText(1, 2);
		
	    String thridcompany = tIPSSRS.GetText(1, 3);
	    
		FTPTool tFTPTool = new FTPTool(tIP, tUserName,
				tPassword, Integer.parseInt(tPort));
		
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println("登录ftp服务器失败");
				return false;
			}
		} catch (SocketException ex) {
			System.out.println("无法登录ftp服务器");
			return false;
		} catch (IOException ex) {
			System.out.println("无法读取ftp服务器信息");
			return false;
		}
		try {
			if(!tFTPTool.makeDirectory("/"+thridcompany+"/TPJK/"+mng+"/TPClaimCoreResult/")){
				System.out.println("FTP目录已存在");
			};			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		if (!tFTPTool.upload("/"+thridcompany+"/TPJK/"+mng+"/TPClaimCoreResult/", cXmlFile)) {
			System.out.println("外包方"+thridcompany+"机构"+mng+"，批次多事件外包案件结果上载文件失败!");
			tFTPTool.logoutFTP();
			return false;
		}
		
		tFTPTool.logoutFTP();

		return true;
	}

	public static void main(String[] args) {

		LLTPCroeResultTask t = new LLTPCroeResultTask();
		t.run();

	}

}
