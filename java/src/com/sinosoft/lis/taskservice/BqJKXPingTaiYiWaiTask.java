package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.jkxpt.BQJKXServiceImp;
import com.sinosoft.lis.jkxpt.BQJYXGRPJKXServiceImp;
import com.sinosoft.lis.jkxpt.BQJYXPJKXServiceImp;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.tb.BPODealXMLBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BqJKXPingTaiYiWaiTask extends TaskThread {
	
	public void run()
    {
		tiQu();
    }
	private void tiQu()
	{

		
		//个单批处理
		SSRS tSSRS = getShuJuGe();
        int mRow = tSSRS.getMaxRow();
        int mCol = tSSRS.getMaxCol();
        String[][] msgInfo = tSSRS.getAllData();
        System.out.println(msgInfo.length+"////////////");
        if (tSSRS != null && mRow > 0) {
        	 for (int bqNum = 0; bqNum < mRow; bqNum++) {
                String edorno=msgInfo[bqNum][0];
                String edortype=msgInfo[bqNum][1];
                String iOrG=msgInfo[bqNum][2];
                String tDataInfoKey=edorno+"&"+edortype+"&"+iOrG;
               
                
                TransferData tTransferData = new TransferData();
            	tTransferData.setNameAndValue("DataInfoKey", tDataInfoKey);
            	VData tVData = new VData();
            	tVData.add(tTransferData);
            	new BQJYXPJKXServiceImp().callJKXService(tVData, "");
            	
              }
        }
        
        //团单批处理
		SSRS tGRPSSRS = getShuJuTuan();
        int mGRPRow = tGRPSSRS.getMaxRow();
        int mGRPCol = tGRPSSRS.getMaxCol();
        String[][] msgGRPInfo = tGRPSSRS.getAllData();
        System.out.println(msgGRPInfo.length+"////////////");
        if (tGRPSSRS != null && mGRPRow > 0) {
        	 for (int bqNum = 0; bqNum < mGRPRow; bqNum++) {
                String edorno=msgGRPInfo[bqNum][0];
                String edortype=msgGRPInfo[bqNum][1];
                String iOrG=msgGRPInfo[bqNum][2];
                String tDataInfoKey=edorno+"&"+edortype+"&"+iOrG;
               
                
                TransferData tTransferData = new TransferData();
            	tTransferData.setNameAndValue("DataInfoKey", tDataInfoKey);
            	VData tVData = new VData();
            	tVData.add(tTransferData);
            	new BQJYXGRPJKXServiceImp().callJKXService(tVData, "");
            	
              }
        }
	}
	
	
	private SSRS getShuJuGe()
	{
		SSRS tSSRS=new SSRS();
		String tStrSql ="  select edorno 工单号 ,edortype 类型,'I' 标志 from lpedoritem a  " 
			           +"  where exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate='0') "
		               +"  and  exists (select 1 from LCCont lcc where lcc.contno = a.contno and lcc.conttype ='1' and ManageCom like '8611%') "
		               +"  and exists (select 1 from lpedorapp where edoracceptno=a.edorno and (  confdate = current date)) "
		               +"  and exists (select 1 from lcpol where contno=a.contno and riskcode in (select riskcode from lmriskapp where risktype = 'A')) "
		               + " and exists (select 1 from lcpol lcp where contno=a.contno and RiskCode in ('510901','510901','510901','510901','5201','520401','520401','5501','5502','5503','5503','5503','5503','5503','5601','5601')  "
		               +"  and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ('244001','244002','244003','244004','203001','656004','656002','610001','617001','619001','619002','619003','619004','619005','606002','606001','601001','601002','601003','601004','601005','601006','614001','614002','611001','613001'))) "
		               +"  and exists (select 1 from lccont where contno=a.contno and signdate >= '2011-1-1')"
		               +"  and edortype in ('CT','WT','BC','CM','CC','TB') " 
		               +"  and not exists (select 1 from DataInfointerface " 
		               +"  where DataInfoKey=(trim(edorno)||'&'||edortype||'&I') and state='0') "
		               ;
	    tSSRS = new ExeSQL().execSQL(tStrSql);
	    
	    return tSSRS;
	}
	
	private SSRS getShuJuTuan()
	{
		SSRS tSSRS=new SSRS();
		String tStrSql ="  select edorno 工单号 ,edortype 类型,'G' 标志 from lpgrpedoritem a " 
		               +"  where exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate='0') "
		               +"  and exists (select 1 from lpedorapp where edoracceptno=a.edorno and ( confdate = current date)) "
		               +"  and  exists (select 1 from lcgrppol where grpcontno=a.grpcontno and  ManageCom like '8611%' and riskcode in (select riskcode from lmriskapp where risktype = 'A'))"
		               + " and exists (select 1 from lcpol lcp where grpcontno=a.grpcontno and RiskCode in ('510901','510901','510901','510901','5201','520401','520401','5501','5502','5503','5503','5503','5503','5503','5601','5601')  "
		               +"  and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ('244001','244002','244003','244004','203001','656004','656002','610001','617001','619001','619002','619003','619004','619005','606002','606001','601001','601002','601003','601004','601005','601006','614001','614002','611001','613001'))) "           
		               +"  and exists (select 1 from LCCont lcc where lcc.GrpContNo = a.GrpContNo and lcc.conttype ='2') "
		               +"  and exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and signdate >= '2011-1-1')"
		               +"  and (select count(1) from lcinsured where grpcontno =a.grpcontno)<=50"
		               +"  and edortype in ('CM','AC','NI','ZT','CT','WT')"
		               +"  and not exists (select 1 from DataInfointerface " 
		               +"  where DataInfoKey=(trim(edorno)||'&'||edortype||'&G') and state='0') "
		               ;
	    tSSRS = new ExeSQL().execSQL(tStrSql);
	    
	    return tSSRS;
	}
	
	
	
	
	
	 public static void main(String[] args)
	 {
		 BqJKXPingTaiYiWaiTask bqBqJKXPingTaiYiWaiTask = new BqJKXPingTaiYiWaiTask();
		 bqBqJKXPingTaiYiWaiTask.run();
	 }
}
