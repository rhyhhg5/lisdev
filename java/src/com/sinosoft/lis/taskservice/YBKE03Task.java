package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclientybk.inf.E03;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *    续保退保批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 
 * @version 1.1
 */
public class YBKE03Task extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    String opr = "";
    public YBKE03Task()
    {}

    public void run()
    {
    	E03 xbtbTask = new E03();
    	//续保退保批处理逻辑
    	String SQL =  "select b.EdorNo "
	    			+ " from lpedorapp a,lpedoritem b,lbcont c,lbpol d"
	    			+ " where a.edoracceptno = b.edorno "
	    			+ " and b.contno=c.contno "
	    			+ " and c.conttype='1' "
	    			+ " and c.contno = d.contno "
	    			+ " and d.riskcode in (select code from ldcode where codetype='ybkriskcode') " //医保卡险种标记
	    			+ " and a.edorstate='0' "
	    			+ " and b.EdorType in('CT','WT') "    //新增保全类型
	    			+ " and exists (select 1 from YBKErrorList where businessno=b.EdorNo and resultstatus='1' and transtype='E01') " //保全上报成功的
	    			+ " and not exists (select 1 from YBKErrorList where businessno=b.EdorNo and resultstatus='1' and transtype='E03') " //上报成功的不再上报
	    			+ " union select distinct(a.contno) from LJSPayperson a,ljspay b where a.riskcode  in (select code from ldcode where codetype='ybkriskcode') and a.contno=b.otherno and b.startpaydate<= current date "
	    		    + " and not exists (select 1 from YBK_E03_LIS_ResponseInfo where edorno=a.contno and prtno=(select renewcount from lcpol where prtno=(select prtno from lcpol where contno=a.contno) order by makedate desc fetch first 1 rows only))"
//	    			+ " and a.contno='000014899000001' with ur " //测试 lijia
	    			;
    	
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String tCaseNo = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("PrtNo", tCaseNo);
    			tVData.add(tTransferData);
    	    	if(!xbtbTask.submitData(tVData, "YBKPT"))
    	         {
    	    		 System.out.println("续保退保批处理出问题了");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             continue;
    	         }else{
    	        	 System.out.println("续保退保批处理成功了");
    	        	 opr ="ture";
    	         }
    		}
    	}

    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
   
    public static void main(String[] args)
    {
    	YBKE03Task eTask = new YBKE03Task();
    	eTask.run();
    }
}




