package com.sinosoft.lis.taskservice;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.sinosoft.lis.certifybusiness.CertActBatchImportBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title:BriGrpSignTask.java </p>
 *
 * <p>Description:大连工伤险签单批处理 </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company:sinosoft </p>
 *
 * @author gzh
 * @version 1.0
 */
public class CardActiveBatchTask extends TaskThread
{
    public String mCurrentDate = PubFun.getCurrentDate();
    
    public CardActiveBatchTask()
    {
        try
        {
            jbInit();
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void run()
    {
        System.out.println( this.mCurrentDate + "日的批量卡激活处理,开始时间是" +PubFun.getCurrentTime());
        
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
		tGI.Operator = "001";
		Calendar calendar = Calendar.getInstance();//此时打印它获取的是系统当前时间
        calendar.add(Calendar.DATE, -1); 
        String tActiveDate = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());;
    	System.out.println("激活时间："+tActiveDate);
    	VData tVData = new VData();
	    
	    TransferData tTransferData = new TransferData();
	    tTransferData.setNameAndValue("ActiveDate", tActiveDate);
	    
	    tVData.add(tTransferData);
	    tVData.add(tGI);
	    
	    CertActBatchImportBL tCertActBatchImportBL = new CertActBatchImportBL();
	    if(!tCertActBatchImportBL.submitData(tVData, "ActSyncBatch"))
	    {
	        System.out.println( " 卡激活数据处理失败，原因是: " + tCertActBatchImportBL.mErrors.getFirstError());
	    }
	    else
	    {
	        System.out.print( " 卡激活数据处理成功！");
	    }
        System.out.println( this.mCurrentDate + "日的批量卡激活处理,完成时间是" +PubFun.getCurrentTime());
    }

    
    
    public static void main(String args[])
    {
        CardActiveBatchTask tCardActiveBatchTask = new CardActiveBatchTask();
        tCardActiveBatchTask.run();
        return;
    }

    private void jbInit() throws Exception
    {
    }
    
}
