package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.f1print.LILRRecePremPrtUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.VData;

public class LILRRecePremPrtTask extends TaskThread
{

    /**
     * @param args
     */
    public LILRRecePremPrtTask(){
    }
    public void run()
    {  
        GlobalInput mG = new GlobalInput();

        VData mVData = new VData();
        mG.Operator = "server";
        mG.ManageCom = "86";
        String Startdate=PubFun.getCurrentDate();
        mVData.add(mG);
        mVData.add(Startdate);
       
        LILRRecePremPrtUI tLILRRecePremPrtUI = new LILRRecePremPrtUI();
        tLILRRecePremPrtUI.submitData(mVData, "PRINT");
        
    }
    public static void main(String args[])
    {
        LILRRecePremPrtTask a = new LILRRecePremPrtTask();
        a.run();

    }

}
