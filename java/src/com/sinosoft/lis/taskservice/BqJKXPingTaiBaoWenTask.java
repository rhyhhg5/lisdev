package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.jkxpt.BQJKXTwoServiceImp;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BqJKXPingTaiBaoWenTask extends TaskThread {
	
	public void run()
    {
		tiQu();
    }
	private void tiQu()
	{
		SSRS tSSRS = getShuJu();
        int mRow = tSSRS.getMaxRow();
        int mCol = tSSRS.getMaxCol();
        String[][] msgInfo = tSSRS.getAllData();
        System.out.println(msgInfo.length+"////////////");
        if (tSSRS != null && mRow > 0) {
        	 for (int bqNum = 0; bqNum < mRow; bqNum++) {
                String edorno=msgInfo[bqNum][0];
                String edortype=msgInfo[bqNum][1];
                String iOrG=msgInfo[bqNum][2];
                String contractNo=msgInfo[bqNum][3];
                String edorAppDate=msgInfo[bqNum][4];
                String edorValiDate=msgInfo[bqNum][5];
                String tDataInfoKey=edorno+"&"+edortype+"&"+iOrG+"&"+contractNo+"&"+edorAppDate+"&"+edorValiDate;
               
                
                TransferData tTransferData = new TransferData();
            	tTransferData.setNameAndValue("DataInfoKey", tDataInfoKey);
            	VData tVData = new VData();
            	tVData.add(tTransferData);
            	new BQJKXTwoServiceImp().callJKXService(tVData, "");
                                
              }
        }
	}
	
	
	private SSRS getShuJu()
	{
		//#3309北京健康险平台数据报送优化 start
		String strSQL1 = "select code from ldcode where codetype='bjjkrisk' and codealias='I'";
    	String strSQL2 = "select code from ldcode where codetype='bjjkduty' and codealias='I'";
    	String strSQL3 = "select code from ldcode where codetype='bjjkrisk' and codealias='G'";
    	String strSQL4 = "select code from ldcode where codetype='bjjkduty' and codealias='G'";
    	String strQuery1 = LLCaseCommon.codeQuery(strSQL1);
    	String strQuery2 = LLCaseCommon.codeQuery(strSQL2);
    	String strQuery3 = LLCaseCommon.codeQuery(strSQL3);
    	String strQuery4 = LLCaseCommon.codeQuery(strSQL4);
    	//#3309北京健康险平台数据报送优化 end
    	
		SSRS tSSRS=new SSRS();
		String tStrSql="select edorno 工单号,edortype 类型,'I' 标志,a.contno 合同号,a.edorappdate 受理日期, a.edorvalidate 生效日期 from lpedoritem a " 
	 		+ " where exists (select 1  from lpedorapp  where edoracceptno = a.edorno and edorstate ='0')" 
	 		+ " and exists (select 1  from lcpol lcp where contno = a.contno   and RiskCode in ("+strQuery1+")   " 
	 		+ " and exists (select 1  from LCDuty lcd  where lcd.PolNo = lcp.PolNo  and lcd.DutyCode in ("+strQuery2+")) " 
	 		+ " union all select 1  from lbpol lbp  where contno = a.contno  and RiskCode in ("+strQuery1+")  " 
	 		+ " and exists (select 1  from LBDuty lbd  where lbd.PolNo = lbp.PolNo and lbd.DutyCode in ("+strQuery2+"))) " 
	 		+ " and not exists (select 1  from LCCont lcc  where lcc.contno = a.contno  and lcc.PolType in ('1', '2'))  " 
	 		+ " and not exists (select 1  from LBCont lbc where lbc.contno = a.contno   and lbc.PolType in ('1', '2'))   " 
	 		+ " and exists (select 1  from lpedorapp where edoracceptno = a.edorno  and confdate = (current date - 1 day))  " 
	 		+ " and exists (select 1  from lccont where contno = a.contno   and signdate >= '2011-1-1' " 
	 		+ " union all select 1  from lbcont where contno = a.contno   and signdate >= '2011-1-1')  and a.ManageCom like '8611%' " 
	 		+ " and edortype in ('BC', 'NS', 'BF', 'XT', 'CT', 'WT') " 
	 		+ " union all "  
	 		+ " select edorno 工单号, edortype 类型,'G' 标志, a.grpcontno 合同号,a.edorappdate 受理日期,a.edorvalidate 生效日期  from lpgrpedoritem a " 
	 		+ " where exists (select 1  from lpedorapp  where edoracceptno = a.edorno  and edorstate = '0') " 
	 		+ " and exists (select 1  from lpedorapp  where edoracceptno = a.edorno  and confdate = (current date - 1 day)) " 
	 		+ " and a.ManageCom like '8611%'   and exists  (select 1  from lcpol lcp  where grpcontno = a.grpcontno  and RiskCode in ("+strQuery3+") " 
	 		+ " and exists (select 1 from LCDuty lcd  where lcd.PolNo = lcp.PolNo   and lcd.DutyCode in ("+strQuery4+"))" 
	 		+ " union all select 1  from lbpol lbp where grpcontno = a.grpcontno  and RiskCode in ("+strQuery3+")  " 
	 		+ " and exists (select 1  from LBDuty lbd where lbd.PolNo = lbp.PolNo  and lbd.DutyCode in ("+strQuery4+"))) " 
	 		+ " and not exists (select 1  from LCCont lcc where lcc.GrpContNo = a.GrpContNo   and lcc.PolType in ('1', '2')) " 
	 		+ " and not exists (select 1  from LBCont lbc  where lbc.GrpContNo = a.GrpContNo  and lbc.PolType in ('1', '2')) " 
	 		+ " and exists  (select 1  from lcgrpcont  where grpcontno = a.grpcontno  and signdate >= '2011-1-1' " 
	 		+ " union all  select 1  from lbgrpcont where grpcontno = a.grpcontno and signdate >= '2011-1-1') " 
	 		+ " and (select count(1) from lcinsured where grpcontno = a.grpcontno) <= 50  " 
	 		+ " and edortype in ('AC', 'NI', 'ZT', 'RR', 'RS', 'XT', 'CT', 'WT')"
	 		;
	     tSSRS = new ExeSQL().execSQL(tStrSql);
	    return tSSRS;
	}
	
	
	
	
	
	 public static void main(String[] args)
	 {
		 BqJKXPingTaiBaoWenTask bqJKXPingTaiBaoWenTask = new BqJKXPingTaiBaoWenTask();
		 bqJKXPingTaiBaoWenTask.run();
	 }
}
