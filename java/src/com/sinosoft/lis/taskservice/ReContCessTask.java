package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.reinsure.CalRiskAmntBL;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.schema.LCPolSchema;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.lis.vschema.LRPolCessAmntSet;
import com.sinosoft.lis.schema.LRPolCessAmntSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 * 本批处理对满足合同分保的保单置合同分保标记
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author huxl
 * @version 1.0
 */
public class ReContCessTask extends TaskThread {
    CErrors mErrors = new CErrors();
    public String mCurrentDate = PubFun.getCurrentDate();
    MMap mMap;
    PubSubmit mPubSubmit = null;
    GlobalInput mGlobalInput = new GlobalInput();

    /** 当前操作人员的机构 **/
    String manageCom = null;
    private VData tVData = new VData();

    public ReContCessTask() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     **/
    public void run() {
        VData aVData = new VData();
        mGlobalInput.ComCode = manageCom;
        aVData.addElement(mGlobalInput);

        if (!signCessFlag()) {
            return;
        }
    }

    /**
     * 置合同分保标记
     * @return boolean
     */
    private boolean signCessFlag() {
        System.out.println("come in signCessFlag()..");
        mCurrentDate = PubFun.calDate(mCurrentDate,-1,"D",null);
        CalRiskAmntBL tCalRiskAmntBL = new CalRiskAmntBL(mCurrentDate);

        //得到保单生效日和第一次交费核销日孰后日(等于签单日期)的保单及保单周年日的保单
        //2007-10-16日修改:1结合型市场的团体保单不进行分保
        //                2保单周年日的长险保单必须进行了本保单年度的首次缴费.考虑到目前的缴费方式一般为趸缴,年缴,
        //                所以这里判断若趸缴：缴至日期 > 当前日期，取保单周年日。否则取本保单年度第一次保单年度的缴费核销日
        String tSql = "select * from LCPol a where a.appflag='1' and ((cvalidate < signdate and signdate = '" +
                      mCurrentDate + "') or (cvalidate >= signdate and cvalidate = '" +mCurrentDate + "')) " +
                      "and riskcode in (select distinct riskcode from LRCalFactorValue) " +
                      "and (reinsureflag not in ('2','3') or reinsureflag is null) and stateflag !='2' and a.riskcode not in ('1601','1602','1603','1604','160306','160307','160308') " + //3:临分,5:临分未实现
//                      " and  not exists (select 1 from lcgrpcont where GrpContNo = a.GrpContNo and MarketType in ('2','3','5','7','8','9'))" +
                      " union "+
                      "select * from LCPol a where a.appflag='1' "+
                      " and ((cvalidate < signdate and signdate = '" +
                      mCurrentDate + "') or (cvalidate >= signdate and cvalidate = '" +
                      mCurrentDate + "')) " +
                      "and riskcode in (select distinct riskcode from LRCalFactorValue) " +
                      "and (reinsureflag not in ('2','3') or reinsureflag is null) and stateflag !='2' and a.riskcode in ('1601','1602','1603','1604','160306','160307','160308') " + //3:临分,5:临分未实现
                      " and (a.conttype='1' or (a.conttype='2' and exists (select 1 from lcgrpcont where GrpContNo = a.GrpContNo and (MarketType ='1' or MarketType is null))))" +//修改场市型为1,9的分保.其它的不分保.个单不限制//090608修改市场类型为1的分保//090610改为1和9分保//090615改为空也加入分保
                      " union all " +
                      "select * from lcpol a where Month(CValidate) = Month('" +
                      mCurrentDate + "') and Day(CValidate)=Day('" +
                      mCurrentDate + "') and Year(CValidate) < Year('" +
                      mCurrentDate + "') and PayToDate > '" + mCurrentDate +
                      "' and PayIntv = 0 and appflag='1' and exists (select 1 from LRCalFactorValue b,lmriskapp c where b.RiskCode = c.RiskCode and c.RiskPeriod = 'L' and b.RiskCode = a.RiskCode) " +
                      " and (reinsureflag not in ('2','3','5') or reinsureflag is null) and (stateFlag in ('0','1') or StateFlag is null) " +
                      " union all "+
                      "select * from lcpol a where exists ( select 1 from ljapayperson where db2inst1.trim(PolNo) = a.PolNo and ConfDate = '" +
                      mCurrentDate +
                      "' and LastPayToDate = a.Cvalidate + year('" +
                      mCurrentDate + "' - a.Cvalidate) years) and PayIntv <> 0 and appflag='1' and exists (select 1 from LRCalFactorValue b,lmriskapp c where b.RiskCode = c.RiskCode and c.RiskPeriod = 'L' and b.RiskCode = a.RiskCode) " +
                      " and (reinsureflag not in ('2','3','5') or reinsureflag is null) and (stateFlag in ('0','1') or StateFlag is null) " +
                      "order by Cvalidate,makedate,maketime with ur ";
        //由于查询结果太多,因此采用游标方法,一次处理五千条数据
        RSWrapper rswrapper = new RSWrapper();
        LCPolSet tLCPolSet = new LCPolSet();
        rswrapper.prepareData(tLCPolSet, tSql);

        try {
            do {
                rswrapper.getData();

                double lowAmnt = 0; //最低再保保额
                double cessionAmnt = 0; //分保保额
                double sumCessAmnt = 0; //累计分出保额
                double retainAmnt = 0; //自留额

                //对新单和保单周年日的保单进行循环
                for (int i = 1; i <= tLCPolSet.size(); i++) {
                    LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                    //得到所有该险种的合同号和生效日期
                    tSql = "select Rvalidate,RecontCode,CessionMode,CessionFeeMode from LRContInfo where Recontcode in " +
                           " (select distinct recontcode from LRcalfactorvalue where riskcode='" +
                           tLCPolSchema.getRiskCode() +
                           "') and ((Rinvalidate is not null and '" +
                           mCurrentDate +
                           "' between Rvalidate and Rinvalidate) or ( Rinvalidate is null and '" +
                           mCurrentDate + "' >= Rvalidate)) " +
                           "order by Rvalidate desc with ur"; //因为长期险续期不能根据签单日期计算

                    SSRS tSSRS = new ExeSQL().execSQL(tSql);
                    LCPolSet uLCPolSet = new LCPolSet();
                    LRPolCessAmntSet uLRPolCessAmntSet = new LRPolCessAmntSet();

                    if (tSSRS.getMaxRow() <= 0) {
                        System.out.println(tLCPolSchema.getPolNo() +
                                           "没有对应的再保合同");
                        tLCPolSchema.setReinsureFlag("6"); //标记为非合同分保
                        uLCPolSet.add(tLCPolSchema);
                        mMap = new MMap();
                        mMap.put(uLCPolSet, "UPDATE");
                        tVData.clear();
                        tVData.add(mMap);
                        mPubSubmit = new PubSubmit();
                        mPubSubmit.submitData(this.tVData, "UPDATE");
                        continue;
                    }
                    String strRecontCode = tSSRS.GetText(1, 2);
                    String strCesssionMode = tSSRS.GetText(1, 3);
                    String strCessionFeeMode = tSSRS.GetText(1, 4);

                    tSql =
                            "select FactorValue from LRcalfactorvalue where RecontCode='" +
                            strRecontCode + "' " +
                            " and FactorCode='LowReAmnt' and RiskCode='" +
                            tLCPolSchema.getRiskCode() + "' with ur";
                    tSSRS = new ExeSQL().execSQL(tSql);
                    if (tSSRS.getMaxRow() <= 0 || tSSRS.GetText(1, 1) == null ||
                        tSSRS.GetText(1, 1).equals("")) {
                        lowAmnt = 0; //如果没有最低再保保额，则为0
                    } else {
                        lowAmnt = Double.parseDouble(tSSRS.GetText(1, 1));
                    }

                    //如果是成数分保则直接置合同分保标志
                    if (strCesssionMode.equals("1")) { //成数分保
                        tLCPolSchema.setReinsureFlag("4");
                        uLCPolSet.add(tLCPolSchema);
                    } else if (strCesssionMode.equals("2")) { //溢额分保
                        tSql =
                                "select distinct factorvalue from LRCalFactorValue where recontcode='" +
                                strRecontCode +
                                "' and FactorCode='RetainAmnt' ";
                        tSSRS = new ExeSQL().execSQL(tSql);
                        if (tSSRS.getMaxRow() <= 0) {
                            System.out.println("没有得到自留额");
                            continue;
                        }
                        String[][] tRetainAmnt = tSSRS.getAllData();
                        if (tRetainAmnt[0][0] == null ||
                            tRetainAmnt[0][0].equals("")) {
                            tRetainAmnt[0][0] = "0";
                        }
                        retainAmnt = Double.parseDouble(tRetainAmnt[0][0]);
                        //得到累计分出保额
                        sumCessAmnt = tCalRiskAmntBL.sumCessAmnt(tLCPolSchema,
                                strRecontCode);
                        LRPolCessAmntSchema tLRPolCessAmntSchema = new
                                LRPolCessAmntSchema();
                        tLRPolCessAmntSchema.setPolNo(tLCPolSchema.getPolNo());
                        tLRPolCessAmntSchema.setReNewCount(tLCPolSchema.
                                getRenewCount());
                        tLRPolCessAmntSchema.setPayCount(0); //长险保单暂时不记录轨迹，实现起来难度太大
                        tLRPolCessAmntSchema.setOperator("Server");
                        tLRPolCessAmntSchema.setMakeDate(PubFun.getCurrentDate());
                        tLRPolCessAmntSchema.setMakeTime(PubFun.getCurrentTime());
                        tLRPolCessAmntSchema.setModifyDate(PubFun.
                                getCurrentDate());
                        tLRPolCessAmntSchema.setModifyTime(PubFun.
                                getCurrentTime());

                        if (strCessionFeeMode.equals("1")) { //风险保费方式
                            double accAmnt = tCalRiskAmntBL.accAmnt(
                                    tLCPolSchema, strRecontCode);
                            double riskAmnt = tCalRiskAmntBL.accRiskAmnt(
                                    tLCPolSchema, strRecontCode);
                            //得到分保保额, 分出保额=本年有效累计风险保额—自留额—本年累计分出保额
                            if(tLCPolSchema.getRiskCode().equals("530301")){
                                cessionAmnt = 2*riskAmnt-retainAmnt;
                                tLRPolCessAmntSchema.setSumAmnt(2*accAmnt);
                                tLRPolCessAmntSchema.setSumRiskAmnt(2*riskAmnt);
                                tLRPolCessAmntSchema.setSumReserve(accAmnt -
                                   riskAmnt);//530301的风险准备金暂时为0
                                tLRPolCessAmntSchema.setSumCessAmnt(sumCessAmnt);
                                tLRPolCessAmntSchema.setRemainAmnt(retainAmnt);

                            }else{
                                cessionAmnt = riskAmnt - retainAmnt - sumCessAmnt;
                                tLRPolCessAmntSchema.setSumAmnt(accAmnt);
                                tLRPolCessAmntSchema.setSumRiskAmnt(riskAmnt);
                                tLRPolCessAmntSchema.setSumReserve(accAmnt -
                                                                   riskAmnt);
                                tLRPolCessAmntSchema.setSumCessAmnt(sumCessAmnt);
                                tLRPolCessAmntSchema.setRemainAmnt(retainAmnt);
                            }
                        } else if (strCessionFeeMode.equals("2")) { //原始保费方式
                            double accAmnt = tCalRiskAmntBL.accAmnt(
                                    tLCPolSchema, strRecontCode);
                            //得到分保保额, 分出保额=本年有效累计风险保额—自留额—本年累计分出保额
                            cessionAmnt = accAmnt - retainAmnt - sumCessAmnt;
                            tLRPolCessAmntSchema.setSumAmnt(accAmnt);
                            tLRPolCessAmntSchema.setSumRiskAmnt(accAmnt);
                            tLRPolCessAmntSchema.setSumReserve(0);
                            tLRPolCessAmntSchema.setSumCessAmnt(sumCessAmnt);
                            tLRPolCessAmntSchema.setRemainAmnt(retainAmnt);
                        }
                        if (cessionAmnt > lowAmnt) {
                            tLCPolSchema.setReinsureFlag("4");
                            //标记为合同分保,这里考虑到同时签单的保单，可能分保保额大于保单保额，所以进行如下处理
                            if (cessionAmnt <= tLCPolSchema.getAmnt()) {
                                tLCPolSchema.setCessAmnt(cessionAmnt);
                                tLRPolCessAmntSchema.setCessAmnt(cessionAmnt);
                            } else {
                                if(tLCPolSchema.getRiskCode().equals("530301")){
                                    tLCPolSchema.setCessAmnt(tLCPolSchema.getAmnt()*2-retainAmnt);
                                    tLRPolCessAmntSchema.setCessAmnt(tLCPolSchema.getAmnt()*2-retainAmnt);
                                }else{
                                    tLCPolSchema.setCessAmnt(tLCPolSchema.getAmnt());
                                    tLRPolCessAmntSchema.setCessAmnt(tLCPolSchema.
                                                                     getAmnt());
                                }
                            }
                            uLCPolSet.add(tLCPolSchema);
                            uLRPolCessAmntSet.add(tLRPolCessAmntSchema);
                        } else {
                            tLCPolSchema.setReinsureFlag("6"); //标记为非合同分保.
                            tLCPolSchema.setCessAmnt(0);
                            uLCPolSet.add(tLCPolSchema);
                        }
                    }
                    mMap = new MMap();
                    mMap.put(uLCPolSet, "UPDATE");
                    mMap.put(uLRPolCessAmntSet, "DELETE&INSERT");
                    tVData.clear();
                    tVData.add(mMap);
                    mPubSubmit = new PubSubmit();
                    mPubSubmit.submitData(this.tVData, "UPDATE");
                }
            } while (tLCPolSet.size() > 0);
            rswrapper.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            rswrapper.close();
        }
        return true;
    }

    public boolean submitData(String aStartDate, String aEndDate) {
        String tStartDate = aStartDate;
        String tEndDate = aEndDate;
        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(tStartDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(tEndDate); //录入的终止日期，用FDate处理

        while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            mCurrentDate = chgdate.getString(dbdate); //录入的正在统计的日期
            if (!signCessFlag()) {
                return false;
            }
            dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
        }
        return true;
    }

    public static void main(String args[]) {
        ReContCessTask tReContCessTask = new ReContCessTask();
//        下面的程序用于批处理启动失败时的运维
        String tStartDate = "2010-1-01";
        String tEndDate = "2010-2-25";

        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(tStartDate); //录入的起始日期，用FDate处理
        Date dedate = chgdate.getDate(tEndDate); //录入的终止日期，用FDate处理

        while (dbdate.compareTo(dedate) <= 0) { //compareTo() 如果前面的早于后面的为-1，等于时为0，从mStartDate到mEndDate循环
            tReContCessTask.mCurrentDate = chgdate.getString(dbdate); //录入的正在统计的日期
            if (!tReContCessTask.signCessFlag()) {
            }
            dbdate = PubFun.calDate(dbdate, 1, "D", null); //将其日期+1天
        }
    }

    private void jbInit() throws Exception {
    }
}
