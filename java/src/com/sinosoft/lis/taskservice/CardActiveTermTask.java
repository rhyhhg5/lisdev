package com.sinosoft.lis.taskservice;

import com.ecwebservice.services.GetActiveTerm;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 
 * </p>
 *
 * @author Uncle zhangyige 2012-04-19
 * @version 1.0
 */
public class CardActiveTermTask extends TaskThread
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();

    public CardActiveTermTask()
    {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run()
    {
    	new GetActiveTerm().submitData();
    }


    public static void main(String args[])
    {
        CardActiveTermTask a = new CardActiveTermTask();
        a.run();
    }
}
