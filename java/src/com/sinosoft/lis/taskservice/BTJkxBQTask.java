package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.jkxpt.BQJKXTwoServiceImp;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BTJkxBQTask extends TaskThread {
	
	public void run()
    {
		tiQu();
    }
	private void tiQu()
	{
		SSRS tSSRS = getShuJu();
        int mRow = tSSRS.getMaxRow();
        int mCol = tSSRS.getMaxCol();
        String[][] msgInfo = tSSRS.getAllData();
        System.out.println(msgInfo.length+"////////////");
        if (tSSRS != null && mRow > 0) {
        	 for (int bqNum = 0; bqNum < mRow; bqNum++) {
                String edorno=msgInfo[bqNum][0];
                String edortype=msgInfo[bqNum][1];
                String iOrG=msgInfo[bqNum][2];
                String contractNo=msgInfo[bqNum][3];
                String edorAppDate=msgInfo[bqNum][4];
                String edorValiDate=msgInfo[bqNum][5];
                String tDataInfoKey=edorno+"&"+edortype+"&"+iOrG+"&"+contractNo+"&"+edorAppDate+"&"+edorValiDate;
               
                
                TransferData tTransferData = new TransferData();
            	tTransferData.setNameAndValue("DataInfoKey", tDataInfoKey);
            	VData tVData = new VData();
            	tVData.add(tTransferData);
            	new BQJKXTwoServiceImp().callJKXService(tVData, "");
                                
              }
        }
	}
	
	
	private SSRS getShuJu()
	{
		SSRS tSSRS=new SSRS();
		String tStrSql = " "
			+ " select edorno 工单号 ,edortype 类型,'I' 标志,a.contno 合同号,a.edorappdate 受理日期,a.edorvalidate 生效日期  from lpedoritem a    "
			+ " where exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate='0')  "
			+ " and exists (select 1 from lcpol lcp where contno=a.contno and RiskCode in ('1201','1202','1206','120706','121001','130101','230201','230301','230501','230601','230701','230801','230901','231001','240201','240301','240501','320106','330401','330601','330901','331001','331401','331601','331701','331801','332101','332401','332601','340101','340201','5201','531001','531201','531501','531801','540101','730101')   "
			+ " and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ('207001','201001','212001','253001','243001','265001','256001','258001','270001','275001','284001','296001','297001','200001','257001','285001','239001','252001','259001','264001','274001','271001','290001','298001','262001','241001','208001','312001','315001','294001','240001','203001','299001','242001','217001','316001','295001','311001')))  "
			+ " and not exists (select 1 from LCCont lcc where lcc.contno = a.contno and lcc.PolType in ('1', '2'))  "
			+ " and exists (select 1 from lpedorapp where edoracceptno=a.edorno and confdate >='2013-02-06')  "
			+ " and  exists (select 1 from lcpol where contno=a.contno and riskcode in (select riskcode from lmriskapp where risktype1 = '1'))  "
			+ " and exists (select 1 from lccont where contno=a.contno and signdate >= '2011-1-1') "
			+ " and  a.ManageCom like '8611%'  "
			+ " and edortype in ('BC','NS','BF','XT','CT','WT') "
			+ " union all  "
			+ " select edorno 工单号 ,edortype 类型,'G' 标志,a.grpcontno 合同号,a.edorappdate 受理日期,a.edorvalidate 生效日期 from lpgrpedoritem a   "
			+ " where exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate='0')  "
			+ " and exists (select 1 from lpedorapp where edoracceptno=a.edorno and confdate >='2013-02-06')  "
			+ " and  exists (select 1 from lcgrppol where grpcontno=a.grpcontno and riskcode in (select riskcode from lmriskapp where risktype1 = '1')) "
			+ " and  a.ManageCom like '8611%'  "
			+ " and exists (select 1 from lcpol lcp where grpcontno=a.grpcontno and RiskCode in ('1601','1602','1603','160306','160307','160308','1604','1607','160806','260301','260401','5503','5601')   "
			+ " and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ('601001','601006','601005','601004','601003','601002','602001','604001','604002','604003','604004','604005','604006','604007','604008','604009','604010','604011','604012','604013','604014','604015','604016','604017','604018','604019','604020','615001','615002','615003','615004','615005','615006','615007','615008','615009','615010','615011','615012','615013','615014','615015','615016','615017','615018','615019','615020','615021','615022','615023','615024','629001','629002','629003','629004','629005','629006','629007','629008','629009','629010','629011','629012','629013','629014','629015','629016','629017','629018','629019','629020','629021','647001','647019','647018','647017','647016','647015','647014','647013','647012','647011','647010','647009','647008','647007','647006','647005','647004','647003','647002','603001','614001','614002','621001','634001','634002','633001','619003','619005','619004','619002','619001','606001','606002')))  "
			+ " and not exists (select 1 from LCCont lcc where lcc.GrpContNo = a.GrpContNo and lcc.PolType in ('1', '2'))  "
			+ " and exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and signdate >= '2011-1-1') "
			+ " and (select count(1) from lcinsured where grpcontno =a.grpcontno)<=50 "
			+ " and edortype in ('AC','NI','ZT','RR','RS','XT','CT','WT') ";
	    tSSRS = new ExeSQL().execSQL(tStrSql);
	    
	    return tSSRS;
	}
	
	
	
	
	
	 public static void main(String[] args)
	 {
		 BTJkxBQTask bqJKXPingTaiBaoWenTask = new BTJkxBQTask();
		 bqJKXPingTaiBaoWenTask.run();
	 }
}
