package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.f1print.GetFeePrintUI;
import com.sinosoft.lis.llcase.SagentMail;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.*;
/**
 * <p>Title:新财务接口V3 应收保费报表--团险期缴 批处理服务类</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2014</p>
 *
 * <p>Company: </p>
 *
 * @author 
 * @version 3.0
 */
public class FinLcgrpDataReportTask extends TaskThread
{
	public CErrors mErrors = new CErrors();
	private GlobalInput globalInput = new GlobalInput();
	private TransferData mGetCessData = new TransferData();
	private VData mOutputData = new VData();
	
	/**管理机构*/
	private String tMangecom = "";
	/**提数日期*/
	private String [] date;
	/**文件路径*/
	private String tOutXmlPath = "";
	/**文件名称*/
	private String fileName = "";
	/**应用路径*/
	private String mURL = "";
	/** 邮件收件人 */
    private String mAddress = "";
    /** 邮件抄送人 */
    private String mCcAddress = "";
    /** 邮件暗抄人 */
    private String mBccAddress = "";
	
	private static final int BUFFER = 2048;
	
	/**对提数结果压缩成功标志：1-成功;0-失败;-1-失败终止*/
    private int mFlag;
    
    /**压缩文件的路径*/
    private String mZipFilePath;
	
	public FinLcgrpDataReportTask()
	{}
	
	public void run()
    {       
		dealData();
    }
	
	public boolean dealData()
	{
		System.out.println("开始执行财接--应收保费报表(团险期缴)批处理:"+PubFun.getCurrentTime());
		//先准备数据
		getInputData();
		
		//开始报表打印
		dataReportPrint();
		
		//报表文件压缩并发送邮件
		getReportZipFile();
		
		System.out.println("财接--应收保费报表(团险期缴)批处理执行完毕:"+PubFun.getCurrentTime());
		
		return true;
	}
	
	
	/**
	 * 数据准备，提数日期--date，提数机构--86，生成文件路径--tOutXmlPath
	 *
	 */
	public void getInputData()
	{
		//设置管理机构
		globalInput.ManageCom = "86";
		globalInput.Operator = "lcgrp";
		globalInput.ComCode = "86";
		
		//设置提数日期
		String predate = new ExeSQL().getOneValue("select current date -1 MONTHS from dual"); 
		date = PubFun.calFLDate(predate);
		System.out.println("提数机构为："+globalInput.ManageCom+",提数日期为："+date[0]+"至"+date[1]);
		
		//设置文件名称与文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
    	mURL = new ExeSQL().getOneValue(tSQL);
//    	mURL = "F:/YSFiles/";
    	mURL+="vtsfile/";
    	
    	
    	File mFileDir = new File(mURL);
        if (!mFileDir.exists())
        {
            if (!mFileDir.mkdirs())
            {
                System.err.println("创建目录[" + mURL.toString() + "]失败！" + mFileDir.getPath());
            }
        }
    	
    	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		String tDate = dateFormat.format(new Date());
		String tTime = timeFormat.format(new Date());
		fileName = tDate.replace("-", "")+tTime.replace(":", "")+"-lcgrp.xls";
		tOutXmlPath = mURL +fileName;
		
		mGetCessData.setNameAndValue("StartDate",date[0]);  //打印起期
		mGetCessData.setNameAndValue("EndDate",date[1]);	//打印止期
		mGetCessData.setNameAndValue("OutXmlPath",tOutXmlPath);//文件路径
		
		mOutputData.add(globalInput);
		mOutputData.add(mGetCessData);
		
	}
	
	
	
	/**
	 * 报表打印提交
	 *
	 */
	public void dataReportPrint()
	{
		GetFeePrintUI tGetFeePrintUI = new GetFeePrintUI();		
		try
		{
			if(!tGetFeePrintUI.submitData(mOutputData,""))
			{
				System.out.println("报表打印失败，原因是:" + tGetFeePrintUI.mErrors.getFirstError());
			}
		}
		catch(Exception ex)
		{
			System.out.println("失败，原因是:" + ex.toString());
		}
	}
	
	/**]
	 * 判断文件是否存在
	 * @param filePath
	 * @return
	 */
	private boolean FileExists(String filePath){
		File reportFile = new File(filePath);
		if(!reportFile.exists()){
			return false;
		}
		return true;
	}
	
	private void getReportZipFile(){
		String name = mURL + fileName;
		if(FileExists(name)){
			getZipFile();
			sendSagentMail();
			System.out.println("打印文件存在");
		}else{
			sendMessage();
			System.out.println("没有符合打印条件的数据");
		}
	}
	
    /**
     * 将给定目录下的文件打包为压缩包
     *
     */
    private void getZipFile()
    {
    	 
        FileOutputStream f = null;
        String tDocFileName = mURL + fileName; //文件绝对路径 + 文件名
        mZipFilePath = mURL+fileName.replace("xls", "zip");
    	System.out.println("压缩路径名：" + mZipFilePath);
        ZipOutputStream out = null;
        
        try
        {
            f = new FileOutputStream(mZipFilePath);
            System.out.println("f：" + f);
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("无法创建zip文件");
            System.out.println(mErrors.getLastError());
            return ;
        }
        out = new ZipOutputStream(new BufferedOutputStream(f));
        System.out.println("out：" + out);
        mFlag = compressIntoZip(mZipFilePath, out, fileName,tDocFileName);
		   
        try {
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		    mErrors.addOneError("流关闭异常");
		    System.out.println(mErrors.getLastError());
		    return ;
		} 	
    }
    
    /**
     * 添加到压缩文件
     * @param zipfilepath String
     * @param out ZipOutputStream
     * @param tZipDocFileName String：zip文件名
     * @param tDocFileName String：待压缩的文件路径名
     * @return int：1：成功、0：失败，但不中断整个程序、-1：失败，中断程序
     */
    private  int compressIntoZip(String zipfilepath, ZipOutputStream out,
            String tZipDocFileName, String tDocFileName)
    {
    System.out.println("zipfilepath:" + zipfilepath);
    System.out.println("out:" + out);
    System.out.println("tZipDocFileName:" + tZipDocFileName);
    System.out.println("tDocFileName待压缩文件压缩路径:" + tDocFileName);

        try
        {
            FileInputStream fi = new FileInputStream(tDocFileName);
            BufferedInputStream origin = new BufferedInputStream(fi, BUFFER);

            ZipEntry entry = new ZipEntry(tZipDocFileName);
            out.putNextEntry(entry);

            int count;
            byte data[] = new byte[BUFFER];
            while ((count = origin.read(data, 0, BUFFER)) != -1)
            {
                out.write(data, 0, count);
            }
            origin.close();
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
            this.mErrors.addOneError("找不到待压缩文件压缩路径" + tDocFileName);
            System.out.println("找不到待压缩文件压缩路径" + tDocFileName);

            //某些页找不到可不用单独处理
            return 0;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "无法生成压缩包";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            deleteFile(zipfilepath);
            return -1;
        }
        return 1;
    }
	
    /**
     * 删除生成的ZIP文件和XML文件
     * @param cFilePath String：完整文件名
     */
    private boolean deleteFile(String cFilePath)
    {
        File f = new File(cFilePath);
        if (!f.exists())
        {
            CError tError = new CError();
            tError.moduleName = "MonthDataCatch";
            tError.functionName = "deleteFile";
            tError.errorMessage = "没有找到需要删除的文件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        f.delete();
        return true;
    }
    
	/**
	 * 得到配置的收件人邮箱地址
	 *
	 */
	private void getEmailAddress(){
		String mAddressSQL = "select codealias from ldcode where codetype = 'MailADDRESS' and code = 'mAddress' with ur";
    	mAddress = new ExeSQL().getOneValue(mAddressSQL);
    	String mCcAddresstSQL = "select codealias from ldcode where codetype = 'MailADDRESS' and code = 'mCcAddress' with ur";
    	mCcAddress = new ExeSQL().getOneValue(mCcAddresstSQL);
    	String mBccAddressSQL = "select codealias from ldcode where codetype = 'MailADDRESS' and code = 'mBccAddressSQL' with ur";
    	mBccAddress = new ExeSQL().getOneValue(mBccAddressSQL);
	}
    
    /**
     * 将报表查询文件压缩包发送邮件
     *
     */
    private void sendSagentMail(){
    	//压缩文件生成成功，开始发送邮件
    	if(mFlag == 1){
    		getEmailAddress();
    		SagentMail tSendMail = new SagentMail();
        	try {
    			tSendMail.SendMail();
    			if(!"".equals(mAddress)){
    				tSendMail.setToAddress(mAddress);
    			}
    			if(!"".equals(mCcAddress)){
    				tSendMail.setCcAddress(mCcAddress);
    			}
    			if(!"".equals(mBccAddress)){
    				tSendMail.setBccAddress(mBccAddress);
    			}
    		} catch (Exception ex) {
    			System.out.println("邮件配置失败："+ex.toString());
    		}
    		try {
    			
				tSendMail.setFile(mZipFilePath);
    			tSendMail.send("<b>您好："+date[0]+"至"+date[1]+"期间内，应收保费报表(团险期缴)报表打印文件详情请见附件，谢谢</b>", "应收保费报表(团险期缴)-报表打印");
    			
    		} catch (Exception ex) {
    			System.out.println("邮件发送失败："+ex.toString());
    			ex.printStackTrace();
    		}
    	}
    	
    }
    
    private void sendMessage(){
    	//压缩文件生成成功，开始发送邮件
    	if(mFlag != 1){
    		getEmailAddress();
    		SagentMail tSendMail = new SagentMail();
        	try {
    			tSendMail.SendMail();
    			if(!"".equals(mAddress)){
    				tSendMail.setToAddress(mAddress);
    			}
    			if(!"".equals(mCcAddress)){
    				tSendMail.setCcAddress(mCcAddress);
    			}
    			if(!"".equals(mBccAddress)){
    				tSendMail.setBccAddress(mBccAddress);
    			}
    		} catch (Exception ex) {
    			System.out.println("邮件配置失败："+ex.toString());
    		}
    		try {
    			
    			tSendMail.send("<b>您好："+date[0]+"至"+date[1]+"期间内，没有符合条件的数据,谢谢</b>", "应收保费报表(团险期缴)-报表打印");
    			
    		} catch (Exception ex) {
    			System.out.println("邮件发送失败："+ex.toString());
    			ex.printStackTrace();
    		}
    	}
    	
    }
    
	public static void main(String args[])
	{
		FinLcgrpDataReportTask tFinLcgrpDataReportTask = new FinLcgrpDataReportTask();
		tFinLcgrpDataReportTask.run();
		System.out.println("========应收报表批处理结束=========");
	}

}
