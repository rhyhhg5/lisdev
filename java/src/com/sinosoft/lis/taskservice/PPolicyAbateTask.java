package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.PolicyAbateDealBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.PPolicyAbateDealBL;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PPolicyAbateTask extends TaskThread{
    public PPolicyAbateTask()
    {
    }
    public void run()
    {
        GlobalInput mG = new GlobalInput();
        LCContSchema mLCContSchema = new LCContSchema();
        VData mVData = new VData();
        mG.Operator = "task";
        mG.ManageCom = "86";
        mVData.add(mG);


        PPolicyAbateDealBL mPPolicyAbateDealBL = new PPolicyAbateDealBL();
        //失效批处理
        mPPolicyAbateDealBL.submitData(mVData,"Available");
        //满期终止批处理
        mPPolicyAbateDealBL.submitData(mVData,"Terminate");
    }

}
