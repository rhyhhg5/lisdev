package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.SendMsgMail;
import com.sinosoft.lis.taskservice.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun1;
import java.io.IOException;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;
import java.net.*;
import com.sinosoft.lis.hospitalmanage.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 建立病例档案
 * </p>
 *
 * <p>Copyright: Copyright (c) 2010</p>
 *
 * <p>Company: </p>
 *
 * @author MN
 * @version 1.0
 */
public class HMCustAddDisDocTask extends TaskThread {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    String Content = "";
    String Email = "";
    String Mobile = "";
    // 输入数据的容器
    private VData mInputData = new VData();

    private MMap map = new MMap();
    public HMCustAddDisDocTask() {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run() {
        if (!addDisDoc()) {
            return;
        }
    }

    private boolean addDisDoc() {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        HMCustomerAddDisDocTaskBL tHMCustomerAddDisDocBL = new HMCustomerAddDisDocTaskBL();
        if (!tHMCustomerAddDisDocBL.submitData(mVData, "")) {
            System.out.println("客户病例档案建立失败！");
            CError tError = new CError();
            tError.moduleName = "tHMCustomerAddDisDocBL";
            tError.functionName = "submitData";
            tError.errorMessage = "客户病例档案建立失败";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String args[]) {
    	HMCustAddDisDocTask a = new HMCustAddDisDocTask();
        a.run();

    }
}
