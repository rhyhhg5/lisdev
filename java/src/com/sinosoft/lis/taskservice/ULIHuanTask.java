package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.*;
import com.sinosoft.lis.operfee.*;

//程序名称：ULIHuanTask.java
//程序功能：万能暂缓缴费批处理
//创建日期：2009-7-21
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class ULIHuanTask extends TaskThread{

    CErrors mErrors = new CErrors();

    public ULIHuanTask()
    {
    }

    public void run()
    {
        System.out.println("万能暂缓缴费批处理开始！");
        ULIHuanRun tULIHuanRun = new ULIHuanRun();
        if(!tULIHuanRun.submitData())
        {
        	System.out.println("万能暂缓缴费批处理完成-没有需要缓交状态恢复的万能保单！");
        }
        System.out.println("万能暂缓缴费批处理完成！");
    }
    public static void main(String[] args)
    {
        ULIHuanTask x = new ULIHuanTask();
        x.run();
    }
}
