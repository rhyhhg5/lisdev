package com.sinosoft.lis.taskservice;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.utility.CErrors;

public class LLCoreInvokeTask extends TaskThread { 
	public CErrors mErrors = new CErrors();
	public void run(){
		//System.out.println(tStartDate);
		LLCoreInvokeBL tLLCoreInvokeBL = new LLCoreInvokeBL();
		tLLCoreInvokeBL.dealData();
	}
	
	public static void main(String args[]) {

		LLCoreInvokeTask a = new LLCoreInvokeTask ();
        a.run();
        if (a.mErrors.needDealError()) {
            System.out.println(a.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }

    }
}