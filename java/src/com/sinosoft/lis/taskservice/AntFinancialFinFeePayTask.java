package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJFIGetDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
/*
 * 蚂蚁金服付费批量实付类（WT、CT、XT）
 * Made at 18-3-15  JL
 */
public class AntFinancialFinFeePayTask extends TaskThread
{
	String  mErrors= null;
	//实付信息
	LJFIGetDB aLJFIGetDB =  new LJFIGetDB();
	LJFIGetSet mLJFIGetSet =  new LJFIGetSet();
	LJFIGetSet mnewLJFIGetSet =  new LJFIGetSet();
	//保全批改补退费表
	LJAGetEndorseSet newLJAGetEndorseSet = new LJAGetEndorseSet();
	//应付表
	LJAGetSet mLJAGetSet =  new LJAGetSet();
	LJAGetDB aLJAGetDB =  new LJAGetDB();
	LJAGetSet mnewLJAGetSet = new LJAGetSet();
	//数据集合
	private VData mInputData =  new VData();
	//数据集合
	private MMap map = new MMap();
	//系统化发当前时间
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();

	@Override
	public void run()
	{

		try{
			//正式查询SQL
			//查询蚂蚁金服犹豫期退保、解约、协议解约数据
			String SQL ="select distinct(b.actugetno) from ljagetendorse a ,ljaget  b ,lbcont c "
					+ "where a.actugetno=b.actugetno "
					+ "and a.contno=c.contno "
					+ "and c.agentcode='9510000003' "//电商专用中介
					+ "and a.endorsementno=b.otherno "
					+ "and b.othernotype='10' "
					+ "and c.ManageCom='86950000' "
					//+ "and a.riskcode in (select code from ldcode where codetype='AntPinancialRisk') "
					//+ "and c.SaleChnl='21' "
					//+ "and exists (select 1 from WXCONTRELAINFO where batchno like 'ANTWX%' and prtno=c.prtno) "
					+ "and a.feeoperationtype in ('CT','WT','XT') "
					+ "and b.enteraccdate is null "
					+ "and b.confdate is null " 
					+ "and b.paymode<>'4' "  
					+ "and not exists (select 1 from ljfiget where actugetno= b.actugetno) fetch first 2000 rows only";
			//应付信息
			ExeSQL mexExeSQL = new ExeSQL();
			System.out.println("查询SQL："+SQL);
			SSRS mSSRS = mexExeSQL.execSQL(SQL);
			/*LJAGetDB aLJAGetDB =  new LJAGetDB();
			System.out.println("查询SQL："+SQL);
			mLJAGetSet= aLJAGetDB.executeQuery(SQL);*/
			if(mSSRS.MaxRow>0){
				for(int i =0 ; i< mSSRS.MaxRow;i++){
					LJAGetSchema mLJAGetSchema =  new LJAGetSchema();
					LJFIGetSchema mLJFIGetSchema =  new LJFIGetSchema();
					//mLJAGetSchema
					String Actugetno = mSSRS.GetText(i+1, 1);
					if(QyeryLJFIGet(Actugetno)) {
						System.out.println("实付号："+Actugetno+"非正常数据");
						continue;
						
					}else{
						aLJAGetDB.setActuGetNo(Actugetno);
						mLJAGetSet = aLJAGetDB.query();
						mLJAGetSchema= mLJAGetSet.get(1);

						//1-处理财务给付表
						mLJFIGetSchema.setActuGetNo(mLJAGetSchema.getActuGetNo());
						mLJFIGetSchema.setPayMode(mLJAGetSchema.getPayMode());
						mLJFIGetSchema.setOtherNo(mLJAGetSchema.getOtherNo());
					    mLJFIGetSchema.setOtherNoType(mLJAGetSchema.getOtherNoType());
					    mLJFIGetSchema.setGetMoney(mLJAGetSchema.getSumGetMoney());
					    mLJFIGetSchema.setShouldDate(mLJAGetSchema.getShouldDate());
					    mLJFIGetSchema.setEnterAccDate(CurrentDate);
					    mLJFIGetSchema.setConfDate(CurrentDate);
					    mLJFIGetSchema.setBankCode(mLJAGetSchema.getBankCode());
					    mLJFIGetSchema.setChequeNo(mLJAGetSchema.getChequeNo()); 
					    mLJFIGetSchema.setConfMakeDate(CurrentDate);
						mLJFIGetSchema.setConfMakeTime(CurrentTime);
						mLJFIGetSchema.setPolicyCom(mLJAGetSchema.getManageCom());
						mLJFIGetSchema.setManageCom("86950000");
						mLJFIGetSchema.setSaleChnl(mLJAGetSchema.getSaleChnl());
						mLJFIGetSchema.setAgentCom(mLJAGetSchema.getAgentCom());
					    mLJFIGetSchema.setAgentType(mLJAGetSchema.getAgentType());
					    mLJFIGetSchema.setAgentGroup(mLJAGetSchema.getAgentGroup());
						mLJFIGetSchema.setAgentCode(mLJAGetSchema.getAgentCode());
						mLJFIGetSchema.setSerialNo(mLJAGetSchema.getSerialNo());
						mLJFIGetSchema.setDrawer(mLJAGetSchema.getDrawer());
						mLJFIGetSchema.setDrawerID(mLJAGetSchema.getDrawerID());
						mLJFIGetSchema.setOperator("SYS");
						mLJFIGetSchema.setMakeDate(CurrentDate);//入机日期
						mLJFIGetSchema.setMakeTime(CurrentTime);//入机时间
						mLJFIGetSchema.setModifyDate(CurrentDate);//最后一次修改日期
						mLJFIGetSchema.setModifyTime(CurrentTime);//最后一次修改时间
						mLJFIGetSchema.setBankAccNo(mLJAGetSchema.getBankAccNo());
					    mLJFIGetSchema.setAccName(mLJAGetSchema.getAccName());
					    
						String SqlBankInfo = "select code,code1 from ldcode1 where codetype='AntPinancialRisk'";
						ExeSQL meExeSQL =  new ExeSQL();
						SSRS mSSRSBank  = meExeSQL.execSQL(SqlBankInfo);
						if(mSSRSBank.MaxRow<0){
							mErrors ="保险公司本方归集账户信息查询错误";
						}
						//更新应付表信息
						mLJAGetSchema.setInsBankCode(mSSRSBank.GetText(1, 1));
						mLJAGetSchema.setInsBankAccNo(mSSRSBank.GetText(1, 2));
						mLJAGetSchema.setModifyDate(CurrentDate);
						mLJAGetSchema.setModifyTime(CurrentTime);
						mLJAGetSchema.setConfDate(CurrentDate);
						mLJAGetSchema.setEnterAccDate(CurrentDate);

	                   //更新保全业务批改补退费表
					    System.out.println("更新 批改补退费表(实收/实付)");
					    updateLJAGetEndorse(mLJAGetSchema);
					    mnewLJAGetSet.add(mLJAGetSchema);
					    mnewLJFIGetSet.add(mLJFIGetSchema);
					}
						
				}
				
			}else {
				mErrors = "没有需要操作实付的数据";
				return;
			}
			
			//提交数据集
			  map.put(mnewLJAGetSet, "UPDATE");
		      map.put(mnewLJFIGetSet, "INSERT");
		      map.put(newLJAGetEndorseSet, "UPDATE");

		      mInputData.clear();
		      mInputData.add(map);
			
			  PubSubmit tPubSubmit = new PubSubmit();
		      if (!tPubSubmit.submitData(mInputData, "")) {
		        // @@错误处理
		    	  mErrors = tPubSubmit.mErrors.getContent();
		      }
		
			
			
		}catch( Exception e){
			
		}finally{
			if(!mErrors.equals(null)){
				System.out.println(mErrors);
			}
			
		}
		
		
	}
	
	 private boolean QyeryLJFIGet(String  ActuGetNo)
	  {

	    if(ActuGetNo==null)
	    {
	      mErrors = "实付号码不能为空!";
	      return true;
	    }
	    String sqlStr="select * from LJFIGet where ActuGetNo='"+ActuGetNo+"'";
	    System.out.println("查询财务给付表:"+sqlStr);
	    mLJFIGetSet = aLJFIGetDB.executeQuery(sqlStr);
	    if (aLJFIGetDB.mErrors.needDealError() == true)
	     {
	      // @@错误处理
	      mErrors=aLJFIGetDB.mErrors.getErrContent();
	      return true;
	     }
	     if (mLJFIGetSet.size() > 0)
	     {
	    	 mErrors="实付号："+ActuGetNo+"已经操作过实付";
	      return true;
	      }

	    return false;
	  }
	 
	 /*
	  * 更新保全批改补退费表
	  */
	 private LJAGetEndorseSet updateLJAGetEndorse(LJAGetSchema mLJAGetSchema)
	   {
	    String sqlStr="select * from LJAGetEndorse where ActuGetNo='"+mLJAGetSchema.getActuGetNo()+"'";
	    System.out.println("查询给付表(生存领取_实付):"+sqlStr);
	    LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
	    LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
	    LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
	    tLJAGetEndorseSet = tLJAGetEndorseDB.executeQuery(sqlStr);
	    if (tLJAGetEndorseDB.mErrors.needDealError() == true)
	     {
	      mErrors="批改补退费表(实收/实付)查询失败!";
	      return null;
	     }
	     
	     for(int n=1;n<=tLJAGetEndorseSet.size();n++)
	     {
	       tLJAGetEndorseSchema = new LJAGetEndorseSchema();
	       tLJAGetEndorseSchema = tLJAGetEndorseSet.get(n);
	       tLJAGetEndorseSchema.setGetConfirmDate(CurrentDate);
	       tLJAGetEndorseSchema.setEnterAccDate(CurrentDate);
	       tLJAGetEndorseSchema.setModifyDate(CurrentDate);
	       tLJAGetEndorseSchema.setModifyTime(CurrentTime);
	       newLJAGetEndorseSet.add(tLJAGetEndorseSchema);
	       /*//数据量过大时可能出现问题更改  dongjian
	       String Sql="update ljagetendorse set GetConfirmDate='"+CurrentDate+"',"
	       +" EnterAccDate = '"+CurrentDate+"',"
	       +" ModifyDate = '"+CurrentDate+"',"
	       +" ModifyTime = '"+CurrentTime+"'"
	       +" where actugetno = '"+tLJAGetEndorseSchema.getActuGetNo()+"'";
	       System.out.println("mUpdateLJAGetEndorse is "+Sql);*/
	     }
	     if (newLJAGetEndorseSet.size()==0)
	     {
	      mErrors="没有需要更新的批改补退费表";
	      return null;
	      }
	     return newLJAGetEndorseSet;

	   }
	 public static void main (String args[]){
		 new AntFinancialFinFeePayTask().run();
	 }
	 

}
