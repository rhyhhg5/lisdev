package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.jkxpt.BQJKXServiceImp;
import com.sinosoft.lis.jkxpt.BQJKXTwoServiceImp;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.tb.BPODealXMLBL;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BqJKXTwoTask extends TaskThread {
	
	public void run()
    {
		System.out.println("健康险平台二期个单保全报数开始……");
		try {
			reportBQData();
		} catch (Exception e) {
			System.out.println("健康险平台二期个单保全报数异常……");
			e.printStackTrace();
		}
		System.out.println("健康险平台二期个单保全报数结束……");
		System.out.println("健康险平台二期团单保全报数开始……");
		try {
			reportGrpBQData();
		} catch (Exception e) {
			System.out.println("健康险平台二期个单保全报数异常……");
			e.printStackTrace();
		}
		System.out.println("健康险平台二期团单保全报数结束……");
    }
	private void reportBQData()
	{
		SSRS tSSRS = new SSRS();
		StringBuffer tStrSql = new StringBuffer();
		tStrSql.append(" select distinct lpe.EdorAcceptNo 工单号 ,lpe.edortype 类型,'I' 标志,lpe.ContNo 保单号,lpe.EdorAppDate,lpe.EdorValiDate ");
		tStrSql.append(" from LCCont lcc ");
		tStrSql.append(" inner join LCPol lcp on lcp.ContNo = lcc.ContNo ");
		tStrSql.append(" inner join LPEdorItem lpe on lpe.ContNo = lcc.ContNo ");
		tStrSql.append(" where 1 = 1 ");
		tStrSql.append(" and lcc.conttype = '1' ");
		tStrSql.append(" and lcc.ManageCom like '8611%' ");
		tStrSql.append(" and lcc.SignDate >= '2011-1-1' ");
		tStrSql.append(" and lcp.RiskCode in ('1201','1202','1206','120706','121001','130101','230201','230301','230501','230601','230701','230801','230901','231001','240201','240301','240501','320106','330401','330601','330901','331001','331401','331601','331701','331801','332101','332401','332601','340101','340201','5201','531001','531201','531501','531801','540101','730101') ");
		tStrSql.append(" and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ('207001','201001','212001','253001','243001','265001','256001','258001','270001','275001','284001','296001','297001','200001','257001','285001','239001','252001','259001','264001','274001','271001','290001','298001','262001','241001','208001','312001','315001','294001','240001','203001','299001','242001','217001','316001','295001','311001')) ");
		tStrSql.append(" and exists (select 1 from LPEdorApp where EdorState = '0' and ConfDate = current date - 1 day and EdorAcceptNo = lpe.EdorAcceptNo)");
		tStrSql.append(" and lpe.edortype in ('BC','NS','BF','XT','CT','WT') ");
		tStrSql.append(" union all ");
		tStrSql.append(" select distinct lpe.EdorAcceptNo 工单号 ,lpe.edortype 类型,'I' 标志,lpe.ContNo 保单号,lpe.EdorAppDate,lpe.EdorValiDate " );
		tStrSql.append(" from LBCont lcc ");
		tStrSql.append(" inner join LBPol lcp on lcp.ContNo = lcc.ContNo ");
		tStrSql.append(" inner join LPEdorItem lpe on lpe.ContNo = lcc.ContNo ");
		tStrSql.append(" where 1 = 1 ");
		tStrSql.append(" and lcc.conttype = '1' ");
		tStrSql.append(" and lcc.ManageCom like '8611%' ");
		tStrSql.append(" and lcc.SignDate >= '2011-1-1' ");
		tStrSql.append(" and lcp.RiskCode in ('1201','1202','1206','120706','121001','130101','230201','230301','230501','230601','230701','230801','230901','231001','240201','240301','240501','320106','330401','330601','330901','331001','331401','331601','331701','331801','332101','332401','332601','340101','340201','5201','531001','531201','531501','531801','540101','730101') ");
		tStrSql.append(" and exists (select 1 from LBDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ('207001','201001','212001','253001','243001','265001','256001','258001','270001','275001','284001','296001','297001','200001','257001','285001','239001','252001','259001','264001','274001','271001','290001','298001','262001','241001','208001','312001','315001','294001','240001','203001','299001','242001','217001','316001','295001','311001')) ");
		tStrSql.append(" and exists (select 1 from LPEdorApp where EdorState = '0' and ConfDate = current date - 1 day and EdorAcceptNo = lpe.EdorAcceptNo)");
		tStrSql.append(" and lpe.edortype in ('BC','NS','BF','XT','CT','WT')");
		
		tSSRS = new ExeSQL().execSQL(tStrSql.toString());

        int mRow = tSSRS.getMaxRow();
        String[][] msgInfo = tSSRS.getAllData();
        if (tSSRS != null && mRow > 0) 
        {
        	 for (int bqNum = 0; bqNum < mRow; bqNum++) 
        	 {
                String edorno=msgInfo[bqNum][0];
                String edortype=msgInfo[bqNum][1];
                String iOrG=msgInfo[bqNum][2];
                String contno=msgInfo[bqNum][3];
                String EdorAppDate=msgInfo[bqNum][4];
                String EdorValiDate=msgInfo[bqNum][5];
                
                String tDataInfoKey=edorno+"&"+edortype+"&"+iOrG+"&"+contno+"&"+EdorAppDate+"&"+EdorValiDate;
                
                TransferData tTransferData = new TransferData();
            	tTransferData.setNameAndValue("DataInfoKey", tDataInfoKey);
            	VData tVData = new VData();
            	tVData.add(tTransferData);
            	new BQJKXTwoServiceImp().callJKXService(tVData, "");
            }
        }
	}
	
	private void reportGrpBQData()
	{
		SSRS tSSRS = new SSRS();
		
		StringBuffer tStrSql = new StringBuffer();
		tStrSql.append(" select distinct lpe.EdorAcceptNo 工单号 ,lpe.edortype 类型,'G' 标志,lpe.GrpContNo 保单号,lpe.EdorAppDate,lpe.EdorValiDate ");
		tStrSql.append(" from LCGrpCont lgc ");
		tStrSql.append(" inner join LCPol lcp on lcp.GrpContNo = lgc.GrpContNo ");
		tStrSql.append(" inner join LPGrpEdorItem lpe on lpe.GrpContNo = lgc.GrpContNo ");
		tStrSql.append(" where 1 = 1 ");
		tStrSql.append(" and lgc.CardFlag is null ");
		tStrSql.append(" and lgc.ManageCom like '8611%' ");
		tStrSql.append(" and lgc.SignDate >= '2011-1-1'");
		tStrSql.append(" and lgc.Peoples2 <= 50 ");
		tStrSql.append(" and lcp.RiskCode in ('1601','1602','160308','1604','1607','160806','260301','260401','5503','5601') ");
		tStrSql.append(" and lcp.RiskCode in (select lmra.RiskCode from LMRiskApp lmra where lmra.RiskType1 = '1') ");
		tStrSql.append(" and exists (select 1 from LCDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ('601001','601006','601005','601004','601003','601002','602001','647001','647019','647018','647017','647016','647015','647014','647013','647012','647011','647010','647009','647008','647007','647006','647005','647004','647003','647002','603001','614001','614002','621001','619003','619005','619004','619002','619001','606001','606002')) ");
		tStrSql.append(" and exists (select 1 from LPEdorApp where EdorState = '0' and ConfDate = current date - 1 day and EdorAcceptNo = lpe.EdorAcceptNo)" );
		tStrSql.append(" and lpe.edortype in ('AC','NI','ZT','RR','RS','XT','CT','WT') ");
		tStrSql.append(" union all ");
		tStrSql.append(" select distinct lpe.EdorAcceptNo 工单号 ,lpe.edortype 类型,'G' 标志,lpe.GrpContNo 保单号,lpe.EdorAppDate,lpe.EdorValiDate ");
		tStrSql.append(" from LBGrpCont lgc ");
		tStrSql.append(" inner join LBPol lcp on lcp.GrpContNo = lgc.GrpContNo ");
		tStrSql.append(" inner join LPGrpEdorItem lpe on lpe.GrpContNo = lgc.GrpContNo ");
		tStrSql.append(" where 1 = 1 ");
		tStrSql.append(" and lgc.CardFlag is null ");
		tStrSql.append(" and lgc.ManageCom like '8611%' ");
		tStrSql.append(" and lgc.SignDate >= '2011-1-1'");
		tStrSql.append(" and lgc.Peoples2 <= 50 ");
		tStrSql.append(" and lcp.RiskCode in ('1601','1602','160308','1604','1607','160806','260301','260401','5503','5601') ");
		tStrSql.append(" and lcp.RiskCode in (select lmra.RiskCode from LMRiskApp lmra where lmra.RiskType1 = '1') ");
		tStrSql.append(" and exists (select 1 from LBDuty lcd where lcd.PolNo = lcp.PolNo and lcd.DutyCode in ('601001','601006','601005','601004','601003','601002','602001','647001','647019','647018','647017','647016','647015','647014','647013','647012','647011','647010','647009','647008','647007','647006','647005','647004','647003','647002','603001','614001','614002','621001','619003','619005','619004','619002','619001','606001','606002')) ");
		tStrSql.append(" and exists (select 1 from LPEdorApp where EdorState = '0' and ConfDate = current date - 1 day and EdorAcceptNo = lpe.EdorAcceptNo)" );
		tStrSql.append(" and lpe.edortype in ('AC','NI','ZT','RR','RS','XT','CT','WT')");

		tSSRS = new ExeSQL().execSQL(tStrSql.toString());
        int mRow = tSSRS.getMaxRow();
        String[][] msgInfo = tSSRS.getAllData();
        if (tSSRS != null && mRow > 0) 
        {
        	 for (int bqNum = 0; bqNum < mRow; bqNum++) 
        	 {
                String edorno=msgInfo[bqNum][0];
                String edortype=msgInfo[bqNum][1];
                String iOrG=msgInfo[bqNum][2];
                String contno=msgInfo[bqNum][3];
                String EdorAppDate=msgInfo[bqNum][4];
                String EdorValiDate=msgInfo[bqNum][5];
                
                String tDataInfoKey=edorno+"&"+edortype+"&"+iOrG+"&"+contno+"&"+EdorAppDate+"&"+EdorValiDate;
                
                TransferData tTransferData = new TransferData();
            	tTransferData.setNameAndValue("DataInfoKey", tDataInfoKey);
            	VData tVData = new VData();
            	tVData.add(tTransferData);
            	new BQJKXTwoServiceImp().callJKXService(tVData, "");
            }
        }
	}
	
	 public static void main(String[] args)
	 {
		 BqJKXTwoTask t = new BqJKXTwoTask();
		 t.run();
	 }
}
