package com.sinosoft.lis.taskservice;

import com.sinosoft.httpclientybk.inf.U01;
import com.sinosoft.lis.cbcheck.ApplyRecallPolUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCApplyRecallPolSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>ClassName:  YbkUnderWritingCancleTask</p>
 * <p>Description: 上海医保卡人工核保系统强撤功能 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2017-7-11
 * @author lyf
 */
public class YbkUnderWritingCancleTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	private String mWhereSQL = "";
	
	public YbkUnderWritingCancleTask() {

	}
	
	public YbkUnderWritingCancleTask(String tPrtNo){
		mWhereSQL = " and lc.prtno = '"+tPrtNo+"' ";
	}
	/**
	 * run方法
	 */
	@SuppressWarnings("unchecked")
	public void run(){
		System.out.println("======================YbkUnderWritingCancleTask Execute  !!!======================");
		String SQL ="select lc.prtno,lc.cardflag,lc.managecom "
					+" from lccont lc,LCIssuePol lcp,lcpol lp "
					+" where lc.prtno=lp.prtno "
					+" and lc.contno=lcp.contno "
					+" and lc.prtno like 'Y%' " 
					+" and lp.riskcode in(select code from ldcode where codetype='ybkriskcode') "
					+" and lc.uwflag='5' "
					+" and not exists(select 1 from LCApplyRecallPol where prtno=lc.prtno) "
					+" and days(current date) - days(lcp.makedate) > 15 "
					+ mWhereSQL
					+" with ur";
		GlobalInput tG = new GlobalInput();
		LCApplyRecallPolSchema tLCApplyRecallPolSchema = new LCApplyRecallPolSchema();
		TransferData tTransferData = new TransferData();
		VData tVData = new VData();
		String tPrtNo;
		String tCardFlag;
		String tErrorInfo;
		String tMagCom;
		SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
		
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++){
    			tPrtNo = tSSRS.GetText(index, 1);
    			tCardFlag = tSSRS.GetText(index, 2);
    			tMagCom = tSSRS.GetText(index, 3);
    			tG.ManageCom = tMagCom;
				tG.Operator = "YBK";
    			tTransferData.setNameAndValue("CardFlag",tCardFlag);
    			//补充附加险表			    				
    			tLCApplyRecallPolSchema.setRemark("人工核保下发问题件超过15天了");
    			tLCApplyRecallPolSchema.setPrtNo(tPrtNo);	
    			tLCApplyRecallPolSchema.setApplyType("3");
    			tLCApplyRecallPolSchema.setManageCom(tMagCom);
    			tLCApplyRecallPolSchema.setOperator("YBK");
    			// 准备传输数据 VData
    			tVData.add(tLCApplyRecallPolSchema);
    			tVData.add(tTransferData);
    			tVData.add(tG);
    	    	try {
    	    		ApplyRecallPolUI applyRecallPolUI = new ApplyRecallPolUI();
    	    		if(applyRecallPolUI.submitData(tVData, "")){
    	    			System.out.println("上海医保卡人工核保系统强撤撤单成功,继续执行,上传撤单结论");
    	    			U01 u01 = new U01();
        	    		VData tVData1 = new VData();
        	    		TransferData tTransferData1 = new TransferData();
        	    		tTransferData1.setNameAndValue("PrtNo", tPrtNo);
        	    		tVData1.add(tTransferData1);
        	    		if(!(u01.submitData(tVData1, "YBKPT"))){
        	    			System.out.println("------撤单结论------上传给平台失败,印刷号======"+tPrtNo);
        	    		}
    	    		}else{
    	    			this.mErrors.copyAllErrors(applyRecallPolUI.mErrors);
    	    			System.out.println("上海医保卡人工核保系统强撤撤单失败："+mErrors.getFirstError()+"---印刷号---"+tPrtNo); 
    	    		}
    	    		
				} catch (Exception e) {
					System.out.println("上海医保卡人工核保系统强撤撤单异常");
					e.printStackTrace();
				}
    		}
    	}
    	System.out.println("======================YbkUnderWritingCancleTask End  !!!======================");
	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		YbkUnderWritingCancleTask ybkCancleTask = new YbkUnderWritingCancleTask();
		ybkCancleTask.run();
	}
}
