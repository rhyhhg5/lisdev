package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.httpclient.inf.GetTaxCode;
import com.sinosoft.lis.pubfun.PubFun;

public class GetContTaxCodeTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private SSRS tSSRS = new SSRS();

	private String mSql = null;
	
	private String mPrtNo = "";
	
	private String mContNo = "";

	private ExeSQL mExeSQL = new ExeSQL();

	// 执行任务
	public void run() {
		dealData();
	}

	public boolean dealData() {
		System.out.println("开始执行批处理:" + PubFun.getCurrentTime());
		
		mSql =" select distinct  Lcp.Contno,  Lcp.Prtno "
             +" from  Lccontsub Lcs,  lcpol Lcp "
             +" where lcs.prtno=lcp.prtno "
             +" and lcp.conttype='1' "
             +" and lcp.appflag='1' "
             +" and exists  (Select 1 From lmriskapp Where riskcode = Lcp.riskcode And TaxOptimal = 'Y')  "
             +" and (Lcs.Succflag Is Null Or Lcs.Succflag <> '00')  "
//             +" and lcc.contno='014059968000002' "
             +" With Ur";
		
		tSSRS = mExeSQL.execSQL(mSql);
		for(int i=1;i<=tSSRS.MaxRow;i++){
			mPrtNo = tSSRS.GetText(i, 2);
			mContNo = tSSRS.GetText(i, 1);
			VData tVData = new VData();
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("ContNo", mContNo);
			tVData.add(tTransferData);
			GetTaxCode tGetTaxCode = new GetTaxCode();
			if(!tGetTaxCode.submitData(tVData, "ZBXPT")){
				System.out.println("保单："+mContNo +"获取税优识别码失败！");
				String updLCCont = "update lccontsub set succflag='99', errorinfo = '"+tGetTaxCode.mErrors.getError(0).errorMessage+"',modifydate=current date,modifytime=current time where prtno = '"+mPrtNo+"'";
			    mExeSQL.execUpdateSQL(updLCCont);
			}else{
				String updLCCont = "update lccontsub set succflag='00', errorinfo =null ,taxcode = '"+tGetTaxCode.getTaxCode()+"',modifydate=current date,modifytime=current time where prtno = '"+mPrtNo+"'";
			    mExeSQL.execUpdateSQL(updLCCont);
			}
			
		}		
		System.out.println("批处理执行完毕:" + PubFun.getCurrentTime());

		return true;
	}


	// 主方法 测试用
	public static void main(String[] args) {
		new GetContTaxCodeTask().run();

	}
}
