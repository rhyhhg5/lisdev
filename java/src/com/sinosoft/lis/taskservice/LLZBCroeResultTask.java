package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *浙江外包既往理赔信息批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author zhang
 * @version 1.1
 */

public class LLZBCroeResultTask extends TaskThread {

	private ExeSQL mExeSQL = new ExeSQL();

	// TransInfo 标识信息
	private Element tPACKET = null;
	
    //TransInfo 标识信息
	private Element tHEAD = null;
	
    //TransInfo 标识信息
	private Element tBODY = null;

	// 案件
	private Element tGrpContInfo = null;

	// 案件
	private Element tContInfos = null;

	// 案件信息SQL
	private String mGrpContInfoSQL = "";

	// 案件信息SSRS
	private SSRS tGrpContInfoSSRS = null;

	// 案件数
	private int GrpContInfolen = 0;

	/** 应用路径 */
	private String mURL = "";

	/** 文件路径 */
	private String mFilePath = "";
	String opr="";
	// 执行任务
	public void run() {
		
		getInputData();
		
		if(!dealData()){
			 System.out.println("理赔反馈出问题了");
             opr="false";
		}else{
			 System.out.println("理赔反馈成功了");
        	 opr="true";
		}
	}
	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
	private void getInputData() {

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);
		//本地测试
//		mURL = "D:\\picc\\ui\\";
		mURL = mURL + "temp_lp/WBClaim/8633/CoreResult/"+PubFun.getCurrentDate()+"/";
		//本地测试 存储核心路径
//		mURL = mURL + "temp_lp\\WBClaim\\8633\\CoreResult\\"+PubFun.getCurrentDate()+"\\";
		
		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}

	}

	public boolean dealData() {

		mGrpContInfoSQL = "select a.caseno,b.grpcontno,a.idno,a.endcasedate," +
				" (select claimno from llhospcase where caseno=a.caseno fetch first row only),sum(b.pay)," +
				" (select sum(OutDutyAmnt) from llclaimdetail where caseno=a.caseno)" +
				" from llcase a,ljagetclaim b where a.caseno=b.otherno and b.othernotype='5' " +
				" and exists(select 1 from llhospcase where llhospcase.caseno=a.caseno and llhospcase.casetype='12')" + //浙江外包的理赔案件
				" and a.rgtstate in('11','12') and b.makedate = Current Date - 1 Day" +  //前一天
				" group by a.caseno,b.grpcontno,a.idno,a.endcasedate "
				;

		tGrpContInfoSSRS = mExeSQL.execSQL(mGrpContInfoSQL);
		GrpContInfolen = tGrpContInfoSSRS.getMaxRow();

		if (GrpContInfolen < 1) {
			return true;
		}

		tPACKET = new Element("PACKET");
		Document Doc = new Document(tPACKET);
		tPACKET.addAttribute("type", "REQUEST");
		tPACKET.addAttribute("version", "1.0");
		tHEAD = new Element("HEAD");
		tHEAD.addContent(new Element("REQUEST_TYPE").setText("ZW03"));
		tHEAD.addContent(new Element("TRANSACTION_NUM").setText("ZW0386330000"+PubFun.getCurrentDate2()+"0000000001"));
		tPACKET.addContent(tHEAD);
		tBODY = new Element("BODY");
		tContInfos = new Element("LLCASELIST");
		
		for (int i = 1; i <= GrpContInfolen; i++) {
			
			tGrpContInfo = new Element("LLCASE_DATA");

			tGrpContInfo.addContent(new Element("CASENO")
					.setText(tGrpContInfoSSRS.GetText(i, 1)));
			tGrpContInfo.addContent(new Element("GrpcontNo")
					.setText(tGrpContInfoSSRS.GetText(i, 2)));
			tGrpContInfo.addContent(new Element("IdNo")
					.setText(tGrpContInfoSSRS.GetText(i, 3)));
			tGrpContInfo.addContent(new Element("EndCaseDate")
					.setText(tGrpContInfoSSRS.GetText(i, 4)));
			tGrpContInfo.addContent(new Element("CLAIMNO")
					.setText(tGrpContInfoSSRS.GetText(i, 5)));
			tGrpContInfo.addContent(new Element("RealPay")
					.setText(tGrpContInfoSSRS.GetText(i, 6)));
			tGrpContInfo.addContent(new Element("OutDutyAmnt")
					.setText(tGrpContInfoSSRS.GetText(i, 7)));
			tContInfos.addContent(tGrpContInfo);
			
		}		
			
			tBODY.addContent(tContInfos);
			tPACKET.addContent(tBODY);
			

			mFilePath = mURL + "PH_LPCR_" + PubFun.getCurrentDate2() + PubFun.getCurrentTime2() +".xml";
			XMLOutputter outputter = new XMLOutputter("  ", true, "GBK");

			try {
				outputter.output(Doc, new FileOutputStream(mFilePath));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!sendXML(mFilePath)){
			  System.out.println("浙江理赔数据向FTP发送文件失败！");
			}

		

		return true;
	}

	private boolean sendXML(String cXmlFile){

		String getIPPort = "select codename ,codealias from ldcode where codetype='ZBClaim' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='ZBClaim' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			System.out.println("FTP服务器IP、用户名、密码、端口都不能为空，否则无法发送xml");
			return false;
		}
		
		String tIP = tIPSSRS.GetText(1, 1);
	    String tPort = tIPSSRS.GetText(1, 2);
	    String tUserName = tUPSSRS.GetText(1, 1);
	    String tPassword = tUPSSRS.GetText(1, 2);
		
	    
		FTPTool tFTPTool = new FTPTool(tIP, tUserName,
				tPassword, Integer.parseInt(tPort));
		
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println("登录ftp服务器失败");
				return false;
			}
		} catch (SocketException ex) {
			System.out.println("无法登录ftp服务器");
			return false;
		} catch (IOException ex) {
			System.out.println("无法读取ftp服务器信息");
			return false;
		}
		try {
			if(!tFTPTool.makeDirectory("./01PH/8633/ZWBClaimCoreResult/")){
				System.out.println("FTP目录已存在");
			};			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		if (!tFTPTool.upload("/01PH/8633/ZWBClaimCoreResult/", cXmlFile)) {
			System.out.println("浙江外包案件结果上载文件失败!");
			return false;
		}
		
		tFTPTool.logoutFTP();

		return true;
	}

	public static void main(String[] args) {

		LLZBCroeResultTask t = new LLZBCroeResultTask();
		t.run();

	}

}
