package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.ygz.GetBqPremBillBL;
import com.sinosoft.lis.ygz.GetXqPremBillBL;
import com.sinosoft.lis.ygz.GrpInvoiceDateSend;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YgzFpTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	
	public YgzFpTask() {

	}
	
	/**
	 * run方法
	 */
	public void run(){
		System.out.println("======================YgzFpTask Execute  !!!======================");
		
		/**
		 * 往同步保单迁出数据中出入日期的值
		 */
		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		tInputData.add(tTransferData);
		
		long startGrpInvoiceDateSend = System.currentTimeMillis();
		try {
			/**
			 * 契约已签单团单发票数据提取
			 */
			GrpInvoiceDateSend tGrpInvoiceDateSend = new GrpInvoiceDateSend(); 
			tGrpInvoiceDateSend.run();
		} catch (Exception e) {
			System.out.println("提取契约已签单团单发票发生错误！");
			e.printStackTrace();
		}
		long endGrpInvoiceDateSend = System.currentTimeMillis();
		System.out.println("========== GrpInvoiceDateSend 契约已签单团单发票数据批处理-耗时： "+((endGrpInvoiceDateSend -startGrpInvoiceDateSend)/1000/60)+"分钟");

//		long startInvoiceDateSend = System.currentTimeMillis();
//		try {
//			/**
//			 * 契约已签单个单发票数据提取
//			 */
//			InvoiceDateSend tInvoiceDateSend = new InvoiceDateSend(); 
//			tInvoiceDateSend.run();
//		} catch (Exception e) {
//			System.out.println("提取契约已签单个单发票发生错误！");
//			e.printStackTrace();
//		}
//		long endInvoiceDateSend = System.currentTimeMillis();
//		System.out.println("========== InvoiceDateSend 契约已签单个单发票数据批处理-耗时： "+((endInvoiceDateSend -startInvoiceDateSend)/1000/60)+"分钟");

		long startGetXqPremBillBL = System.currentTimeMillis();
		try {
			/**
			 * 续期发票数据提取
			 */
			GetXqPremBillBL tGetXqPremBillBL = new GetXqPremBillBL(); 
			if(!tGetXqPremBillBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetXqPremBillBL.mErrors);
				System.out.println("续期发票数据提取报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("续期发票数据提取报错：");
			e.printStackTrace();
		}
		long endGetXqPremBillBL = System.currentTimeMillis();
		System.out.println("========== GetXqPremBillBL续期发票数据提取批处理-耗时： "+((endGetXqPremBillBL -startGetXqPremBillBL)/1000/60)+"分钟");

		long startGetBqPremBillBL = System.currentTimeMillis();
		try {
			/**
			 * 保全发票数据提取
			 */
			GetBqPremBillBL tGetBqPremBillBL = new GetBqPremBillBL(); 
			if(!tGetBqPremBillBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetBqPremBillBL.mErrors);
				System.out.println("保全发票数据提取报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("保全发票数据提取报错：");
			e.printStackTrace();
		}
		long endGetBqPremBillBL = System.currentTimeMillis();
		System.out.println("========== GetBqPremBillBL保全发票数据提取批处理-耗时： "+((endGetBqPremBillBL -startGetBqPremBillBL)/1000/60)+"分钟");

	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		JsflGetTask tJsflGetTask = new JsflGetTask();
		
		tJsflGetTask.run();
	}
}
