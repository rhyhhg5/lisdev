package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.VData;

public class BJYWTask extends TaskThread {
	
	public BJYWTask(){
	}
	
	
    public void run()
    {
        try{
        	//契约承保团单
        	QyACCGrpServiceTask tQyACCGrpServiceTask = new QyACCGrpServiceTask();
        	tQyACCGrpServiceTask.run();
        }catch(Exception e)
        {
        	System.out.println("契约承保团单上报数据异常！");
        	e.getMessage();
        }
        
        try{
        	//契约承保个单
        	QyACCServiceTask tQyACCServiceTask = new QyACCServiceTask();
        	tQyACCServiceTask.run();
        }catch(Exception e)
        {
        	System.out.println("契约承保个单上报数据异常！");
        	e.getMessage();
        }
     
        try{
        	//保全
        	BqJKXPingTaiYiWaiTask tBqJKXPingTaiYiWaiTask = new BqJKXPingTaiYiWaiTask();
        	tBqJKXPingTaiYiWaiTask.run();
        }catch(Exception e)
        {
        	System.out.println("保全上报数据异常！");
        	e.getMessage();
        }
       
        
//        try{
//        //理赔
//        	LPJKXServiceTask tLPJKXServiceTask = new LPJKXServiceTask();
//        	tLPJKXServiceTask.run();
//        }catch(Exception e)
//        {
//        	System.out.println("理赔上报数据异常！");
//        	e.getMessage();
//        }
        
    }
    
    public static void main(String[] args)
    {
    	BJYWTask tBJYWTask = new BJYWTask();
    	tBJYWTask.run();
    }
}
