package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.wiitb.BriGrpSignBL;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title:BriGrpSignTask.java </p>
 *
 * <p>Description:大连工伤险签单批处理 </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company:sinosoft </p>
 *
 * @author gzh
 * @version 1.0
 */
public class BriGrpSignTask extends TaskThread
{
    public String mCurrentDate = PubFun.getCurrentDate();
    
    private String mGrpContNo = null;

    public BriGrpSignTask()
    {
        try
        {
            jbInit();
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void run()
    {
        System.out.println( this.mCurrentDate + "日的大连工伤险签单数据,开始时间是" +
                           PubFun.getCurrentTime());
        SSRS GrpSSRS = getSignGrpContInfo();
        if(GrpSSRS != null && GrpSSRS.MaxRow >0){
        	
        	for(int i=1;i<=GrpSSRS.MaxRow;i++){
        		BriGrpSignBL tBriGrpSignBL = new BriGrpSignBL();
        		GlobalInput tGlobalInput = new GlobalInput();
            	tGlobalInput.Operator = "ly";
            	tGlobalInput.ManageCom = "86910000";
            	
            	String tPrtNo = GrpSSRS.GetText(i, 1);
            	if(tPrtNo == null || "".equals(tPrtNo)){
            		System.out.print("印刷号为空！");
            		continue;
            	}
            	
            	LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            	String tSql = "select * from lcgrpcont where prtno = '"+tPrtNo+"'";
            	LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(tSql);
            	
            	if(tLCGrpContSet ==null || tLCGrpContSet.size() != 1){
            		System.out.print("印刷号为【"+GrpSSRS.GetText(i, 1)+"】的保单获取保单数据失败！");
            		continue;
            	}
            	String tGrpContNo = tLCGrpContSet.get(1).getGrpContNo();
            	
            	if(tGrpContNo ==null || "".equals(tGrpContNo)){
            		System.out.print("印刷号为【"+GrpSSRS.GetText(i, 1)+"】的保单获取保单号码失败！");
            		continue;
            	}
            	
            	TransferData tTransferData = new TransferData();
            	tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
            	
            	VData tVData = new VData();
            	tVData.add(tGlobalInput);
            	tVData.add(tTransferData);
            	tVData.add(tLCGrpContSet);
            	
        		if(!tBriGrpSignBL.submitData(tVData, "")){
        			System.out.println("保单【"+tGrpContNo+"】签单失败，原因为："+tBriGrpSignBL.getErrors().getLastError());
        		}else{
        			System.out.println("保单【"+tGrpContNo+"】签单成功。");
        		}
        	}
        }else{
        	System.out.println(this.mCurrentDate + "日的大连工伤险签单数据完成没有待签单数据");
        }
        System.out.println(this.mCurrentDate + "日的大连工伤险签单数据完成,完成时间是" +
                           PubFun.getCurrentTime());
    }

    private SSRS getSignGrpContInfo()
    {
        StringBuffer tSQLBuffer = new StringBuffer();
        tSQLBuffer.append("select lgc.prtno from lcgrpcont lgc ");
        tSQLBuffer.append("where lgc.appflag !='1' and lgc.prtno in ");
        tSQLBuffer.append("( ");
        tSQLBuffer.append("select distinct bussno from lcbrigrpimportdetail lbgid ");
        tSQLBuffer.append("where lbgid.enableflag = '01' ");//有效保单
        tSQLBuffer.append("and lbgid.importstate = '02' ");//导入成功
        tSQLBuffer.append("and lbgid.dealflag = '00' ");//未承保，即未签单
        tSQLBuffer.append("and exists (select 1 from ljtempfee where otherno = lbgid.bussno and confmakedate is not null and confdate is null) ");//已缴费且不是暂收退费
        tSQLBuffer.append(" ) ");//未承保，即未签单
        if(mGrpContNo != null && !"".equals(mGrpContNo)){
        	tSQLBuffer.append("and lgc.grpcontno = '"+mGrpContNo+"' ");//手动签单
		}
        
        SSRS tSSRS = new ExeSQL().execSQL(tSQLBuffer.toString());

   		return tSSRS;
    }
    
    /**
     * 签发指定保单
     * @param  String mPrtNo
     */
    public void runOneGrpCont(String cGrpContNo)
    {
        mGrpContNo = cGrpContNo;
        run();
    }
    
    public static void main(String args[])
    {
        BriGrpSignTask tBriGrpSignTask = new BriGrpSignTask();
//        tBriGrpSignTask.run();
        tBriGrpSignTask.runOneGrpCont("1400008488");
        return;
    }

    private void jbInit() throws Exception
    {
    }
    
}
