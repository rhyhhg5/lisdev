
package com.sinosoft.lis.taskservice;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import net.sf.json.JSONObject;
//import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.yibaotong.DateUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
  
/**
**
**秘钥接口
***
***
*/

//    2post请求,参数json格式
public class HttpClientHandle {

private static String Method_Get = "GET"; // get方式

private static String Method_Post = "POST"; // post方式

public  String call_rsa_key_value = "";  
public  String private_key = "";  
String file_path="";// key的存储地址？
/**
 * httpURLConnection
 * @return String 返回结果
 * @param param
 *            请求参数
 * @param serverUrl
 *            服务端地址
 * @param method
 *            请求方式
 * @return
 */
public static String sendHttpReq(String param, String sign, String serverUrl, String method) {
    BufferedReader br = null;
    DataOutputStream out = null;
    try {
    	 
    	URL realUrl = new URL(serverUrl);
        // 打开和URL之间的连接
        URLConnection urlcon = realUrl.openConnection();
        // 设置通用的请求属性
        urlcon.setRequestProperty("accept", "*/*");
        urlcon.setRequestProperty("connection", "Keep-Alive");
        urlcon.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        // 发送POST请求必须设置如下两行
        urlcon.setDoOutput(true);
        urlcon.setDoInput(true);
        // 2、发送消息
        if(Method_Post.equals(method) && !(param==null || "".equals(param))) {
            out = new DataOutputStream(urlcon.getOutputStream());
            out.write(param.getBytes("UTF-8"));
            out.flush();
            System.out.println("发送完成");
        }
        // 3、接收消息
        br = new BufferedReader(new InputStreamReader(urlcon.getInputStream(), "UTF-8"));
        String line = br.readLine();
        StringBuffer sb = new StringBuffer();
        while (null != line) {
            sb.append(line);
            line = br.readLine();
        }
        System.out.println("接收完成");
        System.out.println("响应结果：" + sb.toString());
        return sb.toString();
    } catch (IOException e) {
    	 System.out.println(e);
        return null;
    } catch (Exception e) {
    	 System.out.println(e);
        return null;
    } finally {
        if(br != null) {
            try {
                br.close();
                br = null;
            } catch (IOException e) {
            	 System.out.println(e);
            }
        }
        if(out != null) {
            try {
                out.close();
                out = null;
            } catch (IOException e) {
            	 System.out.println(e);
            }
        }
    }
}
 
/**
 * POST方式请求
 * 
 * @param xmlmsg
 * @param ServerUrl
 * @return
 */
public static String post(String param, String serverUrl) {
    return sendHttpReq(param, "", serverUrl, Method_Post);
}

/**
 * GET方式请求
 * 
 * @param xmlmsg
 * @param ServerUrl
 * @return
 */
public static String get(String param, String serverUrl) {
    return sendHttpReq(param, "", serverUrl, Method_Get);
}

/**
 * 发送带签名的post请求
 * @return String
 * @param serverUrl
 * @param param
 * @param sign
 * @return
 */
public static String post(String serverUrl, String param, String sign) {
    return sendHttpReq(param, sign, serverUrl, Method_Post);
}
 
public static void main(String args[]) throws Exception {
	HttpClientHandle httpch=new HttpClientHandle();
	System.out.println(httpch.getAesKey());
}
/**取秘密
 * @throws Exception 
*
*
*/
public String getAesKey() throws Exception{
  RSA RSA= new RSA();
  String filename=DateUtil.getCurrentDate("yyyyMMdd")+""+PubFun1.CreateMaxNo("SCSL_JSON", 10)+".json";//核心使用
  System.out.println("生成报文名称："+filename);
  
  String fileKey="KeyValue.json";
  System.out.println("key的文件名："+fileKey);
  
  String plaintext_json="明文报文内容"; //明文内容
  String call_id_value="";  //标识id
  String get_key="";// key接口

  String se="select  codetype,code,codename,codealias,riskwrapplanname  from LDCode1 where CodeType='SCSL_JSON' with ur";
  SSRS ssrs = new SSRS();
  ExeSQL ex= new ExeSQL();
  ssrs=ex.execSQL(se);
  
	 if(ssrs.MaxRow>0){ // 获取状态名称
		 call_id_value =ssrs.GetText(1, 2);//标识id
		 get_key =ssrs.GetText(1, 3);//获取key接口
		 file_path=ssrs.GetText(1, 5);//获取报文存储地址
	 }
	  if(call_id_value==null || call_id_value =="" || file_path==null || file_path==""){
		  System.out.println("未获取到标识id或报文存储地址信息");
		  return "";
	  }
  //供应方公钥
  String base64="";

  //获得key路径
  String Supply_key_file_path=file_path+"interface1_getKey/SupplyKey.json";
//  key_file_path="E:\\wdx\\SCSLJSON\\interface2_getData\\201805\\response\\plaintext\\201805150000000019.json";
  String Supply_key_file=getKeyFile(Supply_key_file_path);
  if("".equals(Supply_key_file) || Supply_key_file==null){
	  System.out.println("获取的供应方公钥的报文为空："+Supply_key_file_path);
	  return "";
  }
  
  JSONObject Supply_jno =  JSONObject.fromObject(Supply_key_file);
  if(Supply_jno.size()>0){
	  base64 = Supply_jno.getString("supplyKey");
  }
  
  if("".equals(base64)||"".equals(base64) ){
	  System.out.println("获取的供应方公钥为空");
	  return "";
  } 
  
  
  // 生成公钥私钥  
  Map<String, Object> map = RSA.init();  
  call_rsa_key_value = RSA.getPublicKey(map);  
  private_key = RSA.getPrivateKey(map);
  System.out.println("平台方公钥："+call_rsa_key_value);
  System.out.println("平台方私钥："+private_key);
  System.out.println("供应方公钥："+base64);
  System.out.println("标识ID: "+call_id_value);
  System.out.println("获取key接口地址："+get_key);
  System.out.println("报文存储地址："+file_path);
  
  
  //json
  JSONObject json =new JSONObject();                                //创建jsonobject对象

  Map<String,String> map1 = new HashMap();                       //创建map对象
  Map<String,String> map2 = new HashMap();

  map1.put("call_id",call_id_value);
  map2.put("call_rsa_key",call_rsa_key_value);

  json.put("body",map2);
  json.put("header",map1);

  System.out.println(json.toString());
  plaintext_json=json.toString();
  startReq(filename,plaintext_json,"plaintext","request"); //存明文，请求

  String word = call_id_value;  
  byte[] encWord = RSA.encryptByPublicKey(word.getBytes(), base64);  
  String id= RSA.encryptBase64(encWord);
  String key= call_rsa_key_value;
  System.out.println(id);
  System.out.println(key);
  String ciphertext_json="密文内容";//密文
  System.out.println("请求报文明文内容："+plaintext_json);
  JSONObject jsonM =new JSONObject();                                //创建jsonobject对象
  Map<String,String> mapM = new HashMap();                       //创建map对象
  Map<String,String> map2M = new HashMap();
  mapM.put("call_id",id);
  map2M.put("call_rsa_key",key);
  jsonM.put("header",mapM);
  jsonM.put("body",map2M);
//  ObjectMapper objectMapper = new ObjectMapper();
//  ciphertext_json= objectMapper.writeValueAsString(jsonM);
  ciphertext_json=jsonM.toString();
  System.out.println("请求报文密文内容："+ciphertext_json);
  startReq(filename,ciphertext_json,"ciphertext","request"); //存密文,请求
  
  //路径先写在这里，需要配置到配置表中
  String str1 = RSA.encryptBase64(ciphertext_json.getBytes());  
  System.out.println("全文转码："+str1);
    String str = HttpClientHandle.post("content=" +str1, get_key );
    //解密
    //私钥
    byte [] resultvalue =RSA.decryptBase64(str);
    System.out.println("Base64解码完成");
    String sss= new String(resultvalue);
    System.out.println("转换为string类型:"+sss);
    startReq(filename,sss,"ciphertext","response"); //存密文,返回
    System.out.println("返回报文，密文内容："+sss);
    JSONObject jn =  JSONObject.fromObject(sss);
    if(jn.size()>0){
        JSONObject jo = (JSONObject) jn.getJSONObject("body");
    	    String code = jo.getString("code");
    	    String aes_key = jo.getString("aes_key");
    	    System.out.println("code:"+code);
    	    System.out.println("aes_key:"+aes_key);
    	    if("10001".equals(code)){
    	    	byte [] key_value =RSA.decryptBase64(aes_key.toString());
    	        String decWord = new String(RSA.decryptByPrivateKey(key_value, private_key));  
    	        System.out.println("私钥解密后："+decWord);
    	        
    	        if(decWord!=null && !"".equals(decWord)){
    	        	//返回报文明文内容
    	        	Map<String,Object> map11 = new HashMap();                       //创建map对象
    	        	JSONObject obj = new JSONObject();
    	        	map11.put("header","");
    	        	map11.put("body",obj);
    	        	obj.put("code",code);
    	        	obj.put("aes_key",decWord);
					String jsonstr_CC =map11.toString();
	    			System.out.println("返回报文，明文内容："+jsonstr_CC);
    	    		startReq(filename,jsonstr_CC,"plaintext","response"); //存明文，返回
    	        }
    	        JSONObject map11 =new JSONObject(); 
    	        map11.put("getKey",decWord);
    	        map11.put("publicKey",call_rsa_key_value);
    	        map11.put("privateKey",private_key);
    	        //秘钥，公钥，私钥存入网盘
    	        startReq(fileKey,map11.toString());
    	    	System.out.println("成功");
    	    	
    	    	return decWord;
    	    }else if("10002".equals(code)){
    	    	System.out.println("参数错误");
    	    }else if("10003".equals(code)){
    	    	System.out.println("系统异常");
    	    }else if("10004".equals(code)){
    	    	System.out.println("用户验证失败");
    	    }else if("10005".equals(code)){
    	    	System.out.println("数字签名验证(signature)失败");
    	    }else if("10006".equals(code)){
    	    	System.out.println("数据查询超时");
    	    }
    }
    System.out.println("数据大小："+jn.size());
    return "";
}
public void startReq(String filename,String reqxml){
 	
   	//存储报文
 	String  rootPath_input=file_path+"interface1_getKey/";
 	File fileDir = new File(rootPath_input );
     if(!fileDir.exists()){
         fileDir.mkdirs();
     }
     String rootPathbat=rootPath_input+filename;
 	try {
 		writeXml2File(rootPathbat, reqxml);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
 }
public void startReq(String filename,String reqxml,String type,String req_res){
 	
   	//存储报文
 	String  rootPath_input=file_path+"interface1_getKey/"+DateUtil.getCurrentDate("yyyyMM")+"/"+req_res+"/"+type+"/";
 	File fileDir = new File(rootPath_input );
     if(!fileDir.exists()){
         fileDir.mkdirs();
     }
    
     String rootPathbat=rootPath_input+filename;
 	
 	try {
 		writeXml2File(rootPathbat, reqxml);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
 }
public static void writeXml2File(String filePath, String xmlString) throws IOException 
{
	File inputXmlFile = new File(filePath);
    PrintWriter pw = new PrintWriter(new FileWriter(inputXmlFile));
    try{
    	pw.write(xmlString);
    	pw.flush();
    }finally{
    	pw.close();
    }
}
public String getKeyFile(String key_file_patch){
	
	File file=new File(key_file_patch);
    String content = null;
	try {
		content = FileUtils.readFileToString(file,"UTF-8");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	System.out.println("读取的文件内容："+content);
	return content;
}
}