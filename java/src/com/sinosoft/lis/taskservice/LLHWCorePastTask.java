package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class LLHWCorePastTask extends TaskThread {
	
	private ExeSQL mExeSQL = new ExeSQL();
	//TransInfo  標識信息
	private Element tPACKET = null;
	//TransInfo 標識信息
	private Element tHEAD = null;
	//TransInfo 標識信息
	private Element tBODY = null;
	//案件
	private Element tGrpContInfo = null;
	//案件
	private Element tContInfos = null;
	//案件信息SQL
	private String mGrpContInfoSQL = "";
	//案件信息SSRS
	private SSRS tGrpContInfoSSRS = null;
	//案件數
	private int GrpContInfolen = 0;
	/**應用路勁*/
	private String mURL = "";
	/**文件路勁*/
	private String mFilePath = "";
	
	String opr = "";
	public String getOpr() {
		return opr;
	}
	public void setOpr(String opr) {
		this.opr = opr;
	}
	
	//執行任務
	public void run() {
		getInputData();
		if(!dealData()) {
			System.out.println("结案数据接口出问题了");
			opr = "false";
		}else {
			System.out.println("结案数据接口成功了");
			opr = "true";
		}
	}
	
	private void getInputData() {
		//設置文件的路勁
		
		//先獲取服務器的路徑
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);
		//保存到核心路徑
		mURL = mURL + "vtsfile/WBClaim/8631/Cont/ResultRevert/" + PubFun.getCurrentDate() + "/";
//		mURL="C:\\Users\\Administrator\\Desktop\\HW08ZWBClaimCorePast\\" + PubFun.getCurrentDate() + "\\";
		//本地測試
		File mFileDir = new File(mURL);
		if(!mFileDir.exists()) {
			if(!mFileDir.mkdirs()) {
				System.out.println("创建目录[" + mURL.toString() + "]失败！" + mFileDir.getPath());
			}
		}
	}
	
	public boolean dealData() {
//		String tSQL = "select contno from ljsgetclaim where othernotype = '5'";
//		SSRS tGRPcontnoSSRS = mExeSQL.execSQL(tSQL);
//		int tGRPcontnolen = tGRPcontnoSSRS.getMaxRow();
//		for(int x=1; x<=tGRPcontnolen; x++) {
			mGrpContInfoSQL = "select a.caseno ,b.contno ,a.idno,a.endcasedate, " +
					"(select claimno from llhospcase where caseno=a.caseno fetch first 1 row only), " +
					"sum(b.realpay), " + 
					"sum(b.OutDutyAmnt) " + 
					"from llcase a,llclaimdetail b " +  
					"where a.caseno=b.caseno " +  
					"and a.rgtstate in('09','11','12') " + 
					"and exists(select 1 from llhospcase c where c.caseno=a.caseno "
					+ "and c.casetype='14'"
					+ ""
					+ " ) " + 
					"and (a.endcasedate = current date - 1 day or a.endcasedate  in "
					+ "(select codename  from ldcode where  1=1   "
					+ "and codetype='HWSocialSec' and code='2014HWSS' "
					+ "and comcode='HW08'  and codealias = current date - 1 day )) " + 
					"group by a.caseno,b.contno,a.idno,a.endcasedate " + 
//					" fetch first 2 row only "+
					"with ur " ;
			tGrpContInfoSSRS = mExeSQL.execSQL(mGrpContInfoSQL);
			GrpContInfolen = tGrpContInfoSSRS.getMaxRow();
			if(GrpContInfolen > 0) {
				tPACKET = new Element("PACKET");
				Document Doc = new Document(tPACKET);
				tPACKET.addAttribute("type", "REQUEST");
				tPACKET.addAttribute("version", "1.0");
				tHEAD = new Element("HEAD");
				tHEAD.addContent(new Element("REQUEST_TYPE").setText("HW08"));
				tHEAD.addContent(new Element("TRANSACTION_NUM").setText("HW0886310000"+PubFun.getCurrentDate2()+""+PubFun1.CreateMaxNo("HW08_NUM", 10)));
				tPACKET.addContent(tHEAD);
				tBODY = new Element("BODY");
				tContInfos = new Element("LLCASELIST");
				for(int i = 1 ; i <= GrpContInfolen; i++) {
					tGrpContInfo = new Element("LLCASE_DATA");
					tGrpContInfo.addContent(new Element("CASENO").setText(tGrpContInfoSSRS.GetText(i, 1)));
					tGrpContInfo.addContent(new Element("ContNo").setText(tGrpContInfoSSRS.GetText(i, 2)));
					tGrpContInfo.addContent(new Element("IdNo").setText(tGrpContInfoSSRS.GetText(i, 3)));
					tGrpContInfo.addContent(new Element("EndCaseDate").setText(tGrpContInfoSSRS.GetText(i, 4)));
					tGrpContInfo.addContent(new Element("CLAIMNO").setText(tGrpContInfoSSRS.GetText(i, 5)));
					tGrpContInfo.addContent(new Element("OutDutyAmnt").setText(tGrpContInfoSSRS.GetText(i, 7)));
					tGrpContInfo.addContent(new Element("RealPay").setText(tGrpContInfoSSRS.GetText(i, 6)));
					
					tContInfos.addContent(tGrpContInfo);
				}
				tBODY.addContent(tContInfos);
				tPACKET.addContent(tBODY);
				
				mFilePath = mURL + "PH_LPCP_" + "LLHWCorePast" + "_" + PubFun.getCurrentDate2() + PubFun.getCurrentTime2() +".xml";
				XMLOutputter outputter = new XMLOutputter("  ", true, "GBK");
				try {
					outputter.output(Doc, new FileOutputStream(mFilePath));
				}catch(FileNotFoundException e) {
					e.printStackTrace();
				}catch(IOException e) {
					e.printStackTrace();
				}
				
				if(!sendXML(mFilePath)) {
					System.out.println("上海外包结案数据接口向FTP发送文件失败！");
				}
			}
//		}
		return true;
	}
	
	private boolean sendXML(String cXmlFile) {
		String getIPPort = "select codename ,codealias from ldcode where codetype='ZBClaim' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='ZBClaim' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			System.out.println("FTP服务器IP、用户名、密码、端口都不能为空，否则无法发送xml");
			return false;
		}
		String tIP = tIPSSRS.GetText(1, 1);
	    String tPort = tIPSSRS.GetText(1, 2);
	    String tUserName = tUPSSRS.GetText(1, 1);
	    String tPassword = tUPSSRS.GetText(1, 2);
	    
	    FTPTool tFTPTool = new FTPTool(tIP, tUserName,tPassword, Integer.parseInt(tPort));
	    
	    try {
			if (!tFTPTool.loginFTP()) {
				System.out.println("登录ftp服务器失败");
				return false;
			}
		} catch (SocketException ex) {
			System.out.println("无法登录ftp服务器");
			return false;
		} catch (IOException ex) {
			System.out.println("无法读取ftp服务器信息");
			return false;
		}
	    
	    try {
	    	//ftp://10.252.4.88/8631/Cont/ResultRevert/
			if(!tFTPTool.makeDirectory("./01PH/8631/Cont/ResultRevert/")){
				System.out.println("FTP目录已存在");
			};			
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	    if (!tFTPTool.upload("/01PH/8631/Cont/ResultRevert/", cXmlFile)) {
			System.out.println("上海外包结案数据接口上载文件失败!");
			return false;
		}
	    
	    tFTPTool.logoutFTP();
	    
		return true;
	}
	
	public static void main(String[] args) {
		LLHWCorePastTask a = new LLHWCorePastTask();
		a.run();
	}
	
}
