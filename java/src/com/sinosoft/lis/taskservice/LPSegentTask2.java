package com.sinosoft.lis.taskservice;

import java.util.Calendar;

import com.sinosoft.lis.llcase.*;
import com.sinosoft.utility.ExeSQL;

public class LPSegentTask2 extends TaskThread {
	
	/** 收件人 */
	private String mAddress = "";
	/** 抄送人 */
	private String mCcAddress = "";
	/** 暗抄 */
	private String mBccAddress = "";
	
	public LPSegentTask2(){
	}
	
	
    public void run()
    {
    	Calendar c = Calendar.getInstance();
		int datenum=c.get(Calendar.DATE);
		if(datenum == 2 ){			
    	 mAddress = "zhanglingling@picchealth.com;haolingye@picchealth.com";
         mCcAddress = "jingze@picchealth.com;zhangkeyun@picchealth.com;shiyongle@picchealth.com";
         mBccAddress = "zhangqiushi@sinosoft.com.cn;liuyachao14270@sinosoft.com.cn";
    	sendFunctionEmail();
    	sendRegionEmail();
    	sendChannelsEmail();  
		}else{
			System.out.println("今天不是2号，这个批处理不启动");
			return;
		}
    }
    
    /**
     * 理赔channels提数
     *
     */
    private void sendChannelsEmail() {
    	 try{
         	//理赔channels提数
         	SegentChannelsDataExtraction tSegentDateDataExtraction = new SegentChannelsDataExtraction();
         	tSegentDateDataExtraction.setMAddress(mAddress);
         	tSegentDateDataExtraction.setMCcAddress(mCcAddress);
         	tSegentDateDataExtraction.setMBccAddress(mBccAddress);
         	tSegentDateDataExtraction.getSagentData();
         }catch(Exception e)
         {
         	System.out.println("channels提数批处理失败！");
         	e.getMessage();
         }
		
	}

    /**
     * region提数
     *
     */
	private void sendRegionEmail() {
		try{
        	//region提数
        	SegentRegionDataExtraction tSegentDateDataExtraction = new SegentRegionDataExtraction();
        	tSegentDateDataExtraction.setMAddress(mAddress);
        	tSegentDateDataExtraction.setMCcAddress(mCcAddress);
        	tSegentDateDataExtraction.setMBccAddress(mBccAddress);
        	tSegentDateDataExtraction.getSagentData();
        }catch(Exception e)
        {
        	System.out.println("region提数批处理失败！");
        	e.getMessage();
        }
		
	}

	/**
	 * function提数
	 *
	 */
	private void sendFunctionEmail() {
		try{
        	//function提数
    		SegentFunctionDataExtraction tSegentDateDataExtraction = new SegentFunctionDataExtraction();
        	tSegentDateDataExtraction.setMAddress(mAddress);//收件人
        	tSegentDateDataExtraction.setMCcAddress(mCcAddress);//抄送人
        	tSegentDateDataExtraction.setMBccAddress(mBccAddress);//暗抄人
    		tSegentDateDataExtraction.getSagentData();
        }catch(Exception e)
        {
        	System.out.println("理赔function提数批处理失败！");
        	e.getMessage();
        }
		
	}


	/**
     * 获取邮件地址
     *
     */
    private void getEmailAddress() {
    	String tSQL1 = "select codealias from ldcode where codetype = 'lpemailaddress2' and code = 'address' with ur";
    	mAddress = new ExeSQL().getOneValue(tSQL1);
    	String tSQL2 = "select codealias from ldcode where codetype = 'lpemailaddress2' and code = 'ccaddress' with ur";
    	mCcAddress = new ExeSQL().getOneValue(tSQL2);
    	String tSQL3 = "select codealias from ldcode where codetype = 'lpemailaddress2' and code = 'bccaddress' with ur";
    	mBccAddress = new ExeSQL().getOneValue(tSQL3);
		
	}
    
    public static void main(String[] args)
    {
    	LPSegentTask2 tLPSegentTask = new LPSegentTask2();
    	tLPSegentTask.run();
    	System.out.println("理赔批处理结束");
    }
}
