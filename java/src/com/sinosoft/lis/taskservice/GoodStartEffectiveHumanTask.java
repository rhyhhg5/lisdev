package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class GoodStartEffectiveHumanTask extends TaskThread{
	
	public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    public GoodStartEffectiveHumanTask(){}
    
    public void run()
    {
    	submitData();
    }
    public boolean submitData(){
    	if(!dealDate()){
    		return false;
    	}
    	if(!submit()){
    		return false;
    	}
    	return true;
    }
    
    /**
     * 处理业务数据
     * @return boolean
     */
    public boolean dealDate(){
    	String insertSql = "";
    	String tDate = PubFun.getCurrentDate2();
    	if(!"20170331".equals(tDate))
    	{
    		return false;
    	}
    	//2017开门红  只在2017-3-31晚上进行数据提取
    	String querySql = "select a.ManageCom,a.Agentcode from laagent a,LADimission b "
				   		+ "where a.agentcode=b.agentcode and a.BranchType='1' and a.branchtype2='01' and  a.AgentState='02' and a.employdate between '2016-12-1' and '2017-3-31' "
				   		+ "group by a.agentcode,a.ManageCom "
				   		+ "having  min(days(a.employdate)-days(b.DepartDate)) >= '181' "
				   		+ "union "
				   		+ "select a.ManageCom,a.Agentcode from laagent a "
				   		+ "where a.BranchType='1' and  a.AgentState='01' and a.branchtype2='01' and a.employdate between '2016-12-1' and '2017-3-31' "
				   		+ "with ur";
    	ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL. execSQL(querySql);
		
		System.out.println("开门红本次插入数据开始"+querySql);
		for(int i=1;i<=tSSRS.getMaxRow();i++){
			String Mngcom = tSSRS.GetText(i, 1);
			String Agentcode = tSSRS.GetText(i, 2);
			insertSql = "insert into LAhumanDesc(Mngcom,Year,Month,Agentcode,Operator,Makedate,Maketime,Modifydate,Modifytime) "
					  + "values('"+Mngcom+"','"+tDate+"','"+tDate+"','"+Agentcode+"', " 
					  + "'001',current date,current time,current date,current time)";
			mMap.put(insertSql,"INSERT");
		}
		System.out.println("本次插入数据结束");
    	return true;
    }
    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit(){
    	System.out.println("本次提交数据开始");
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")){
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("本次提交数据结束");
        return true;
    }
    
    public static void main(String[] args) {
    	GoodStartEffectiveHumanTask hrbl = new GoodStartEffectiveHumanTask();
		hrbl.run();
	}
}
