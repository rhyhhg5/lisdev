package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.cbcheck.ApplyRecallPolUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCApplyRecallPolSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *   	沈阳医保当日未支付撤单批处理
 * </p>

 * @version 1.0
 */
public class SYPolicyCancleTask extends TaskThread{

	/**错误的容器*/
    public CErrors mErrors = new CErrors();
    public SYPolicyCancleTask(){
    	
    }

    public void run()
    {
    	System.out.println("======================沈阳医保当日未支付撤单批处理开始执行!!!======================");
    	String SQL = "select lcc.prtno,lcc.cardflag,lcc.managecom "
    			   +" from lccont lcc,LJTempFee lj "
		           +" where lcc.prtno=lj.otherno"
		           + "and lcc.appflag in ('0','9') "
		           +" and lcc.conttype = '1' "
		           +" and (lcc.UWFlag is null or lcc.UWFlag not in ('a','1','8')) "
		           +" and lcc.prtno like 'SYW%' "
		           +" and lj.EnterAccDate is null "
		           +" and not exists (select 1 from InterfaceDealLogInfo where businessno=lcc.prtno and transtype='SY03' and responsecode='1') "
		           +" and not exists (select 1 from LCApplyRecallPol where prtno=lcc.prtno) "
		           +" and lcc.makedate=current date "
		           +" with ur";
		GlobalInput tG = new GlobalInput();
		LCApplyRecallPolSchema tLCApplyRecallPolSchema = new LCApplyRecallPolSchema();
		TransferData tTransferData = new TransferData();
		VData tVData = new VData();
		String tPrtNo;
		String tCardFlag;
		String tMagCom;
		SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
		
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++){
    			tPrtNo = tSSRS.GetText(index, 1);
    			tCardFlag = tSSRS.GetText(index, 2);
    			tMagCom = tSSRS.GetText(index, 3);
    			
    			tG.ManageCom = tMagCom;
				tG.Operator = "YBK";
    			tTransferData.setNameAndValue("CardFlag",tCardFlag);
    			//补充附加险表			    				
    			tLCApplyRecallPolSchema.setRemark("沈阳医保当日未支付撤单批处理");
    			tLCApplyRecallPolSchema.setPrtNo(tPrtNo);	
    			tLCApplyRecallPolSchema.setApplyType("3");
    			tLCApplyRecallPolSchema.setManageCom(tMagCom);
    			tLCApplyRecallPolSchema.setOperator("SYW");
    			// 准备传输数据 VData
    			tVData.add(tLCApplyRecallPolSchema);
    			tVData.add(tTransferData);
    			tVData.add(tG);
    	    	try {
    	    		ApplyRecallPolUI applyRecallPolUI = new ApplyRecallPolUI();
    	    		if(!applyRecallPolUI.submitData(tVData, "")){
    	    			this.mErrors.copyAllErrors(applyRecallPolUI.mErrors);
    	    			System.out.println("处理撤单失败："+mErrors.getFirstError());
    	    		}
				} catch (Exception e) {
					System.out.println("上海医保卡核心解锁处理撤单异常");
					e.printStackTrace();
				}
    		}
    	}
    	System.out.println("======================沈阳医保当日未支付撤单批处理执行完毕!!!======================");

    }
   
    public static void main(String[] args)
    {
    	SYPolicyCancleTask syTask = new SYPolicyCancleTask();
    	syTask.run();
    }
}

