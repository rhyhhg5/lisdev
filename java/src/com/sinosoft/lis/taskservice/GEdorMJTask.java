package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LGWorkTypeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LGTraceNodeOpSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LGWorkTraceSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.task.CheckData;
import com.sinosoft.task.CommonBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 特需险种满期结算日处理程序，在险种到期的最后一天生成结算任务
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GEdorMJTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private MMap map = null;

    private GlobalInput mGlobalInput = null;

    private int mTaskCount = 0;  //生成的结算任务数

    public GEdorMJTask()
    {}

    public void run()
    {
        getSubmitMap();
        if(mErrors.needDealError())
        {
            System.out.println(mErrors.getErrContent());
            return;
        }

        submit();
    }

    /**
     * 提交操作
     */
    private void submit()
    {
        VData tVData = new VData();
        tVData.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(tVData, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            System.out.println(mErrors.getErrContent());
        }
        else
        {
            System.out.println("共生成" + mTaskCount + "个满期结算任务。");
        }
    }

    /**
     * 得到生成的任务
     * @return MMap
     */
    public MMap getSubmitMap()
    {
        if(mGlobalInput == null)
        {
            mGlobalInput = new GlobalInput();
            mGlobalInput.Operator = "Server";
            mGlobalInput.ComCode = "86";
            mGlobalInput.ManageCom = "86";
        }

        createTask();

        return map;
    }

    /**
     * 由业务人员手动运行，只生成本机构及下级机构的保单的满期结算任务
     * @param tGlobalInput GlobalInput
     */
    public void oneCompany(GlobalInput tGlobalInput)
    {
        mGlobalInput = tGlobalInput;
        run();
    }

    /**
     * 得到生成满期结算任务的个数
     * @return int
     */
    public int getTaskCount()
    {
        return mTaskCount;
    }

    /**
     * 生成满期结算任务
     */
    private void createTask()
    {
        LCGrpContSet tLCGrpContSet = getEndTimeGrpCont();
        if(tLCGrpContSet.size() == 0)
        {
            return;
        }

        map = new MMap();
        for(int i = 1; i <= tLCGrpContSet.size(); i++)
        {
        	
        	//处理并发操作 同一个团单只能操作一次
        	MMap tCekMap = null;
        	tCekMap = lockLGWORK(tLCGrpContSet.get(i).getSchema());
            if (tCekMap == null)
            {
                return ;
            }
            map.add(tCekMap);
        	
            String workNo = com.sinosoft.task.CommonBL.createWorkNo();
            if(!cerateLGWorkInfo(workNo, tLCGrpContSet.get(i)))
            {
                return;
            }
            if(!createLGWorkTraceInfo(workNo,
                                      tLCGrpContSet.get(i).getManageCom()))
            {
                return ;
            }
            if(!createOpInfo(workNo))
            {
                return;
            }
            if(!setContState(tLCGrpContSet.get(i)))
            {
                return;
            }
            if(!createMJState(workNo))
            {
                return;
            }

            mTaskCount++;
        }
    }

    /**
     * 满期结算任务设置为未处理
     * @return boolean
     */
    private boolean createMJState(String workNo)
    {
        LPEdorEspecialDataSchema specialData = new LPEdorEspecialDataSchema();
        specialData.setEdorAcceptNo(workNo);
        specialData.setEdorNo(specialData.getEdorAcceptNo());
        specialData.setEdorType(BQ.EDORTYPE_MJ);
        specialData.setPolNo(BQ.FILLDATA);
        specialData.setDetailType(BQ.DETAILTYPE_MJSTATE);
        specialData.setEdorValue(BQ.MJSTATE_INIT);

        map.put(specialData, "INSERT");
        return true;
    }
    
    /**
     * 锁定动作
     * @param cLCContSchema
     * @return
     */
    private MMap lockLGWORK(LCGrpContSchema cLCGRPContSchema)
    {
        MMap tMMap = null;
        /**满期给付锁定标志"MJ"*/
        String tLockNoType = "MJ";
        /**锁定时间*/
        String tAIS = "3600";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cLCGRPContSchema.getGrpContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }    
    

    /**
     * 生成工单主表信息
     * @return boolean
     */
    private boolean cerateLGWorkInfo(String workNo,
                                     LCGrpContSchema tLCGrpContSchema)
    {
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();

        tLGWorkSchema.setWorkNo(workNo);
        tLGWorkSchema.setAcceptNo(workNo);
        tLGWorkSchema.setNodeNo("0");
        tLGWorkSchema.setTypeNo(com.sinosoft.task.Task.WORK_TYPE_MJ);
        tLGWorkSchema.setStatusNo(com.sinosoft.task.Task.WORK_STATE_INIT);
        tLGWorkSchema.setDetailWorkNo(workNo);
        tLGWorkSchema.setCustomerNo(tLCGrpContSchema.getAppntNo());
        tLGWorkSchema.setCustomerName(tLCGrpContSchema.getGrpName());
        tLGWorkSchema.setContNo(tLCGrpContSchema.getGrpContNo());
        tLGWorkSchema.setApplyTypeNo(com.sinosoft.task.Task.APPLYTYPE_INNER);
        tLGWorkSchema.setApplyName("Server");
        tLGWorkSchema.setAcceptWayNo(com.sinosoft.task.Task.ACCEPTWAY_SERVER);
        tLGWorkSchema.setAcceptCom(mGlobalInput.ComCode);
        tLGWorkSchema.setAcceptDate(PubFun.getCurrentDate());
        tLGWorkSchema.setDateLimit(getDateLimit());
        tLGWorkSchema.setRemark("自动批注：生成新的工单");
        tLGWorkSchema.setOperator(mGlobalInput.Operator);
        tLGWorkSchema.setMakeDate(PubFun.getCurrentDate());
        tLGWorkSchema.setMakeTime(PubFun.getCurrentTime());
        tLGWorkSchema.setModifyDate(PubFun.getCurrentDate());
        tLGWorkSchema.setModifyTime(PubFun.getCurrentTime());
        tLGWorkSchema.setUniting("0");
        tLGWorkSchema.setCurrDoing("0");
        tLGWorkSchema.setScanFlag("2");

        map.put(tLGWorkSchema, "INSERT");

        return true;
    }

    /**
     * 生成历史轨迹
     * @param workNo String
     * @return boolean
     */
    private boolean createLGWorkTraceInfo(String workNo, String manageCom)
    {
        LGWorkTraceSchema tLGWorkTraceSchema = new LGWorkTraceSchema();

        String workBoxNo = getWorkBoxNo(manageCom);
        if(workBoxNo == null)
        {
            return false;
        }
        tLGWorkTraceSchema.setWorkNo(workNo);
        tLGWorkTraceSchema.setNodeNo("0");
        tLGWorkTraceSchema.setWorkBoxNo(workBoxNo); //信箱编号
        tLGWorkTraceSchema.setInMethodNo(
            com.sinosoft.task.Task.HISTORY_TYPE_INIT); //新单录入
        tLGWorkTraceSchema.setInDate(PubFun.getCurrentDate());
        tLGWorkTraceSchema.setInTime(PubFun.getCurrentTime());
        tLGWorkTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLGWorkTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLGWorkTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLGWorkTraceSchema.setModifyTime(PubFun.getCurrentTime());
        tLGWorkTraceSchema.setOperator(mGlobalInput.Operator);

        map.put(tLGWorkTraceSchema, "INSERT");
        return true;
    }

    /**
    * 查询当前用户的信箱号
    * @return String
    */
   private String getWorkBoxNo(String manageCom)
   {
       String boxNo = CommonBL.getAutodeliverTarget(manageCom,
                                                    CheckData.MJ_RULE, null);
       if(boxNo == null)
       {
           mErrors.addOneError("没有查询到机构"
                               + mGlobalInput.ComCode + "的续期催缴信箱。");
           return null;
       }

       return boxNo;
   }

   /**
     * 生成历史轨迹操作信息
     * @param tWorkNo String
     */
    private boolean createOpInfo(String tWorkNo)
    {
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();

        tLGTraceNodeOpSchema.setWorkNo(tWorkNo);
        tLGTraceNodeOpSchema.setNodeNo("0");
        tLGTraceNodeOpSchema.setOperatorNo("0");
        tLGTraceNodeOpSchema.setOperatorType(
            com.sinosoft.task.Task.HISTORY_TYPE_INIT);
        tLGTraceNodeOpSchema.setRemark("自动批注：由日处理程序生成特需满期结算任务。");
        tLGTraceNodeOpSchema.setFinishDate(PubFun.getCurrentDate());
        tLGTraceNodeOpSchema.setFinishTime(PubFun.getCurrentTime());
        tLGTraceNodeOpSchema.setOperator(mGlobalInput.Operator);
        tLGTraceNodeOpSchema.setMakeDate(PubFun.getCurrentDate());
        tLGTraceNodeOpSchema.setMakeTime(PubFun.getCurrentTime());
        tLGTraceNodeOpSchema.setModifyDate(PubFun.getCurrentDate());
        tLGTraceNodeOpSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(tLGTraceNodeOpSchema, "INSERT");
        return true;
    }


    //查询时间限制
    private String getDateLimit()
    {
        LGWorkTypeDB tLGWorkTypeDB = new LGWorkTypeDB();
        tLGWorkTypeDB.setWorkTypeNo(com.sinosoft.task.Task.WORK_TYPE_MJ);
        if(!tLGWorkTypeDB.getInfo())
        {
            return "30";
        }
        return tLGWorkTypeDB.getDateLimit();
    }

    /**
     * 设置保单状态
     * @param tLCGrpContSchema LCGrpContSchema
     * @return boolean
     */
    private boolean setContState(LCGrpContSchema tLCGrpContSchema)
    {
        //设置团单状态为正在满期结算
        tLCGrpContSchema.setState(BQ.CONTSTATE_MJING);
        map.put(tLCGrpContSchema, "UPDATE");

        //设置团单险种状态为正在满期结算
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LCGrpPol a, LMRiskApp b ")
            .append("where a.riskCode = b.riskCode ")
            .append("   and grpContNo = '")
            .append(tLCGrpContSchema.getGrpContNo())
            .append("'  and b.riskType3 = '")
            .append(BQ.RISKTYPE3_SPECIAL)
            .append("' and a.riskCode != '170101' ");
        System.out.println(sql.toString());
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.executeQuery(sql.toString());
        for(int i = 1; i <=tLCGrpPolSet.size(); i++)
        {
            tLCGrpPolSet.get(i).setState(BQ.CONTSTATE_MJING);
        }
        map.put(tLCGrpPolSet, "UPDATE");

        return true;
    }

    /**
     * 得到需要满期结算的保单
     * @return LCGrpPolSet
     */
    private LCGrpContSet getEndTimeGrpCont()
    {
        StringBuffer sql = new StringBuffer();
        if(mGlobalInput == null)
        {
            sql.append("select a.* ")
                .append("from LCGrpCont a, LCGrpPol b, LMRiskApp c ")
                .append("where a.grpContNo = b.grpContNo ")
                .append("   and b.riskCode = c.riskCode ")
                .append("   and (b.StateFlag is null or b.StateFlag = '1') ")
                .append("   and (a.state is null or a.state not in ('")
                .append(BQ.CONTSTATE_MJING)
                .append("', '")
                .append(BQ.CONTSTATE_MJED)
                .append("'))   and c.riskType3 = '")
                .append(BQ.RISKTYPE3_SPECIAL)
                .append("' and b.riskCode != '170101' and days(cInValiDate) <= days(current Date) ")
                //.append("'  and days(cInValiDate) >= days('2006-12-30')")
                .append("   and a.appFlag = '1'");
        }
        else
        {
            sql.append("select a.* ")
                .append("from LCGrpCont a, LCGrpPol b, LMRiskApp c ")
                .append("where a.grpContNo = b.grpContNo ")
                .append("   and b.riskCode = c.riskCode ")
                .append("   and (b.StateFlag is null or b.StateFlag = '1') ")
                .append("   and (a.state is null or a.state not in ('")
                .append(BQ.CONTSTATE_MJING)
                .append("', '")
                .append(BQ.CONTSTATE_MJED)
                .append("'))   and a.ManageCom like '")
                .append(mGlobalInput.ComCode)
                .append("%'   and c.riskType3 = '")
                .append(BQ.RISKTYPE3_SPECIAL)
                .append("' and b.riskCode != '170101'  and days(cInValiDate) <= days(current Date) ")
                //.append("'  and days(cInValiDate) >= days('2006-10-30')")
                .append("   and a.appFlag = '1'");
        }

        System.out.println(sql.toString());
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(sql.toString());

        return tLCGrpContSet;
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "endor0";
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = tGlobalInput.ComCode;

        GEdorMJTask tGEdorMJTask = new GEdorMJTask();
        tGEdorMJTask.run();
//        tGEdorMJTask.oneCompany(tGlobalInput);
    }
}




