package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.yibaotong.LLTJTransFtpXmlDiseasesDetails;
import com.sinosoft.utility.CErrors;

public class LLTPCaseDiseasesTJTask {

	/**
	 * 错误的容器
	 */
	public CErrors mErros = new CErrors();
	
	private String opr="true";
	
	public LLTPCaseDiseasesTJTask(){
		
	}
	
	public void run(){

		LLTJTransFtpXmlDiseasesDetails  tLLTJTransFtpXmlDiseasesDetails = new LLTJTransFtpXmlDiseasesDetails();
		if(!tLLTJTransFtpXmlDiseasesDetails.submitData()){
			System.out.println("天津大病理赔案件信息出现问题====");
			System.out.println(mErros.getFirstError());
			opr="false";
		}else{
			System.out.println("天津大病理赔案件信息正常====");
			opr="true";
		}	
	}
	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	
	public static void main(String[] args) {
		LLTPCaseDiseasesTJTask tLLTPCaseTask = new LLTPCaseDiseasesTJTask();
		tLLTPCaseTask.run();
	}

}
