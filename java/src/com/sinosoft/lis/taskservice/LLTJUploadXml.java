package com.sinosoft.lis.taskservice;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.jdom.Document;
import org.jdom.Element;
import org.w3c.dom.Node;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.namespace.QName;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * <p>Title: 自动立案理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLTJUploadXml extends TaskThread{       
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mFilePath=null;
    public LLTJUploadXml() {
    }
    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    public Document createDomData(){ 
    	Document tDocument = null ;
    try{
    	System.out.println("LLTJUploadXml--createDomData--开始查找待反馈的给付信息");   	   
      
        System.out.println("LLTJUploadXml--createJDomData");
        String tSQL	="select claimno,bnfno,drawer,sum(money),bankcode,bankaccno,accname,banksuccflag,confdate,AppTranNo,FalseReason from "
        	+" (select nvl((select claimno from llhospcase where caseno=b.otherno),b.otherno) claimno,"
        	+" nvl((select BnfNo from llhospcase where caseno=b.otherno),(select BnfNo from LLCasePayAdvancedTrace where caseno=b.otherno and actugetno=b.actugetno)) bnfno,"
        	+" drawer,sum(sumgetmoney) money,bankcode,bankaccno,accname,"
        	+" (case when b.confdate is null then '2' else '1' end) banksuccflag,confdate,"
        	+" nvl((select AppTranNo from llhospcase where caseno=b.otherno),(select AppTranNo from LLCasePayAdvancedTrace where caseno=b.otherno and actugetno=b.actugetno)) AppTranNo,"
        	+" (case when b.confdate is null then nvl((select codename from ldcode1 m,lyreturnfrombankb n where m.code=n.bankcode and m.code1=n.banksuccflag and n.paycode=b.actugetno and m.codetype='bankerror' order by n.modifydate,n.modifytime desc fetch first 1 rows only),'失败未知') else '' end) FalseReason"
    		+" from ljaget b" 
    		+" where b.othernotype in ('5','D')"
    		+" and b.modifydate =current date -1 day "    		
    		+" and ((b.cansendbank ='1' and b.confdate is null and (bankonthewayflag = '0' or bankonthewayflag is null)) or b.confdate is not null)"  
    		+" and (exists (select 1 from llhospcase where caseno=b.otherno and (hospitcode='01' or hospitcode='00000000' )) or exists (select 1 from LLCasePayAdvancedTrace where ClaimNo=b.otherno))"
        	+" group by b.otherno,b.drawer,b.bankcode,b.bankaccno,b.accname,b.confdate,b.actugetno) as temp "
//        	+" where not exists (select 1 from ljaget where otherno=temp.claimno and othernotype='D' and accname=temp.accname and confdate is null)"
//    		+" and not exists (select 1 from llhospcase llh,ljaget lja  where llh.caseno=lja.otherno and lja.othernotype='5' and lja.accname=temp.accname and llh.claimno=temp.claimno and confdate is null)"
        	+" group by claimno,bnfno,drawer,bankcode,bankaccno,accname,banksuccflag,confdate,AppTranNo,FalseReason";
        System.out.println("查询"+mCurrentDate+"前一天数据========"+tSQL);
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(tSQL);

    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		Element tRootData= null ;
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			 String mCaseSql ="select caseno from LLHospCase where claimno='"+tSSRS.GetText(index, 1) +"' and caseno like 'C%'";
    			 ExeSQL tCaseExeSQL = new ExeSQL();
    			 String mCaseNo = tCaseExeSQL.getOneValue(mCaseSql);
                 
                 //add by zjd 增加对案件发生日期的校验.必须是2013年以后发生的案件
                 String mStrsql="select accdate from LLSubReport where subrptno in ( select subrptno from llcaserela where caseno='"+mCaseNo+"')";
                 String maccdate=tCaseExeSQL.getOneValue(mStrsql);
                 if (maccdate.substring(0, 4).compareTo("2013") < 0) {
                     continue;
                 }

    			 tRootData = new Element("PACKET");
   		     	 tRootData.addAttribute("type", "REQUEST");
	   		     tRootData.addAttribute("version", "1.0");
	
	   		     //Head部分
	   		     Element tHeadData = new Element("HEAD");
	   		     Element tRequestType = new Element("REQUEST_TYPE");
	   		     tRequestType.setText("TJ02");
	   		     tHeadData.addContent(tRequestType);                
	   		     tRootData.addContent(tHeadData);
	
	   		     //Body部分
	   		     Element tBodyData = new Element("BODY");    		        
	   		     Element tTransactionNum = new Element("TRANSACTION_NUM");
	   	         tTransactionNum.setText(tSSRS.GetText(index, 10));
	   	         tHeadData.addContent(tTransactionNum);
	   	        
	   			 Element tClaimNo = new Element("CLAIM_NO");
	   	   	     tClaimNo.setText(tSSRS.GetText(index, 1));
	   	   	     tBodyData.addContent(tClaimNo);
	   	   	     
	   	   	     Element tCaseNo = new Element("CASENO");
	   	   	     tCaseNo.setText(mCaseNo);
	   	   	     tBodyData.addContent(tCaseNo);
	   	
	   	   	     Element tBnfNo = new Element("BNFNO");
	   	   	     tBnfNo.setText(tSSRS.GetText(index, 2));
	   	   	     tBodyData.addContent(tBnfNo);
	   	   	     
	   	   	     Element tBnfName = new Element("BNFNAME");
	   	   	     tBnfName.setText(tSSRS.GetText(index,3));
	   	   	     tBodyData.addContent(tBnfName);
	   	   	     
	   	   	     Element tSumPay = new Element("SUMPAY");
	   	   	     tSumPay.setText(tSSRS.GetText(index, 4));
	   	   	     tBodyData.addContent(tSumPay);
	   	   	     
	   	   	     Element tBankCode = new Element("BANKCODE");
	   	   	     tBankCode.setText(tSSRS.GetText(index, 5));
	   	   	     tBodyData.addContent(tBankCode);
	   	   	     
	   	   	     Element tAccNo = new Element("ACCNO");
	   	   	     tAccNo.setText(tSSRS.GetText(index, 6));
	   	   	     tBodyData.addContent(tAccNo);
	   	   	     
	   	   	     Element tAccName = new Element("ACCNAME");
	   	   	     tAccName.setText(tSSRS.GetText(index, 7));
	   	   	     tBodyData.addContent(tAccName);
	   	   	     
	   	   	     Element tDealFlag = new Element("DEALFLAG");
	   	   	     tDealFlag.setText(tSSRS.GetText(index, 8));
	   	   	     tBodyData.addContent(tDealFlag);
	   	   	     
	   	   	     Element tConfDate = new Element("CONFDATE");
	   	   	     tConfDate.setText(tSSRS.GetText(index, 9));
	   	   	     tBodyData.addContent(tConfDate);
	   	   	     
	   	   	     Element tFalseReason = new Element("FALSEREASON");
	   	   	     tFalseReason.setText(tSSRS.GetText(index, 11));
	   	   	     tBodyData.addContent(tFalseReason);
	   	   	     
	   	   	     tRootData.addContent(tBodyData);	
	   	   	     tDocument = new Document(tRootData); 
	   	   	     
		   	   	if(tDocument == null)
	            {
	            	System.err.println("天津城乡居民意外没有待给付反馈信息!");
	            	return null;
	            } 
		   	   
		   	   	LDSysVarDB tLDSysVarDB = new LDSysVarDB();
	            tLDSysVarDB.setSysVar("LLTJUploadXml");
	            LDSysVarSet tLDSysVarSet = tLDSysVarDB.query();
	            if(tLDSysVarSet.size()< 1)
	            {
	            	System.err.println("文件保存路径查询失败！");
	            	return null;
	            }
	            this.mFilePath = tLDSysVarSet.get(1).getSysVarValue();
		    	if(!saveInput(tDocument,tSSRS.GetText(index, 10),index)){
		         	System.out.println("保存传入xml时失败");
		         	return null;
		         }
		    	
	            String tDoc = JdomUtil.outputToString(tDocument);
	            System.out.println(tDoc);
	            	           	            
	            String tOutStr = callService(tDoc,mCaseNo);
	            
	            Document tOutXmlDoc = JdomUtil.buildFromXMLString(tOutStr);
	        	if (tOutXmlDoc == null) {
					System.err.println("天津城乡居民意外返回报文为空!");
					return null;
				}else{
					if(!saveOutput(tOutXmlDoc,tSSRS.GetText(index, 10),index)){
			         	System.out.println("保存传出xml时失败");
			         	return null;
			         }
				}		        	
	    	}    	    		
    	}else{    
    		System.out.println("天津城乡居民意外没有给付信息反馈");
    		return null;
    	}
    	
	}catch(Exception ex){
		System.err.println("天津城乡居民意外给付信息反馈获取失败！");
		ex.printStackTrace();
		return null;
	}	
     return tDocument;
    }
    
    public boolean saveInput(Document pXmlDoc,String mTranscationNum,int index) {   
    	String strLimit = PubFun.getNoLimit("86120000");
    	String pName=mTranscationNum+"_IN"+PubFun1.CreateMaxNo("LLTJ",strLimit)+".xml";
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(this.mFilePath);
		File mFileDir = new File(mFilePath.toString());
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
			}
		}
		File tempFile=new File(mFilePath.toString() + pName);
		if(!tempFile.exists()){
			try{
				if(!tempFile.createNewFile()){
					System.err.println("创建文件失败！" + tempFile.getPath());
				}
			}catch(Exception ex){
				System.err.println("创建文件时出现异常！" + tempFile.getPath());
				ex.printStackTrace();
			}
				
		}
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString()+ pName);
			JdomUtil.output(pXmlDoc, tFos);
			tFos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;
			
		}
		return true;
	}
    
    public boolean saveOutput(Document pXmlDoc,String mTranscationNum,int index) {
    	String strLimit = PubFun.getNoLimit("86120000");
    	String pName=mTranscationNum+"_OUT"+PubFun1.CreateMaxNo("LLTJ",strLimit)+".xml";
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(this.mFilePath);
		File mFileDir = new File(mFilePath.toString());
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
			}
		}
		File tempFile=new File(mFilePath.toString() + pName);
		if(!tempFile.exists()){
			try{
				if(!tempFile.createNewFile()){
					System.err.println("创建文件失败！" + tempFile.getPath());
				}
			}catch(Exception ex){
				System.err.println("创建文件时出现异常！" + tempFile.getPath());
				ex.printStackTrace();
			}
				
		}
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString()+ pName);
			JdomUtil.output(pXmlDoc, tFos);
			tFos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;
			
		}
		return true;
	}
    /**
	 * 1、客户端准备数据;2、调用WebService服务;3、获取服务端处理结果数据
	 *
	 * @param pReader
	 * @param pServiceURL
	 * @return org.w3c.dom.Document
     * @throws AxisFault 
	 * @throws Exception
	 */
	public String callService(String tDoc) throws AxisFault
	{
		RPCServiceClient serviceClient = new RPCServiceClient();
	
		Options options = serviceClient.getOptions();
		LDSysVarDB tLDSysVarDB = new LDSysVarDB();
		tLDSysVarDB.setSysVar("lLLTJPath");		
		if(!tLDSysVarDB.getInfo())
		{}						
		EndpointReference targetEPR = new EndpointReference(tLDSysVarDB.getSchema().getSysVarValue());
		options.setTo(targetEPR); 
		options.setProperty(HTTPConstants.CHUNKED, Boolean.FALSE);
		QName opAddEntry = new QName("http://webservice.yinhai.com/", "call_YH_Hp");
	
		Object[] opAddEntryArgs = new Object[] { tDoc };
	
		Class[] returnTypes = new Class[] { String.class };
		Object[] objects = serviceClient.invokeBlocking(opAddEntry,
				opAddEntryArgs, returnTypes);
		for (int i = 0; i < objects.length; i++) {
			System.out.println(objects[i]);
		}
		String responseXML = (String)objects[0];
		return responseXML;
	}
	
	public String callService(String tDoc, String mCaseNo) throws AxisFault {
		RPCServiceClient serviceClient = new RPCServiceClient();

		Options options = serviceClient.getOptions();
		LDSysVarDB tLDSysVarDB = new LDSysVarDB();

		String accdateSql = "select accdate from LLSubReport where subrptno in ( select subrptno from llcaserela where caseno='"+ mCaseNo + "')";
		ExeSQL tCaseExeSQL = new ExeSQL();
		String accdate = tCaseExeSQL.getOneValue(accdateSql);
		if (null == accdate || "".equals(accdate)) {
			System.out.println("查询LLSubReport后的accdate没有数据");
		}else if (accdate.substring(0, 4).compareTo("2013") < 0) {
			tLDSysVarDB.setSysVar("lLLTJPath");
		}else{
			tLDSysVarDB.setSysVar("lLLTJPathNew");
		}

		if (!tLDSysVarDB.getInfo()) {
		}
		EndpointReference targetEPR = new EndpointReference(tLDSysVarDB.getSchema().getSysVarValue());
		options.setTo(targetEPR);
		options.setProperty(HTTPConstants.CHUNKED, Boolean.FALSE);
		//QName opAddEntry = new QName("http://webservice.yinhai.com/","call_YH_Hp");
        QName opAddEntry = new QName("http://util.common.yinhai.com/","call_YH_Hp");
		Object[] opAddEntryArgs = new Object[] { tDoc };

		Class[] returnTypes = new Class[] { String.class };
		Object[] objects = serviceClient.invokeBlocking(opAddEntry,opAddEntryArgs, returnTypes);
		for (int i = 0; i < objects.length; i++) {
			System.out.println(objects[i]);
		}
		String responseXML = (String) objects[0];
		return responseXML;
	}
	
	public void run(){
		if(createDomData()== null)
		{
			System.out.println("天津意外平台给付信息反馈失败");
		}
	}	
    /**
	 * 将org.w3c.dom.Document转化为文件输出流
	 *
	 * @param pNode
	 * @param pOutputStream
	 * @throws Exception
	 */
	public void outputDOM(Node pNode, OutputStream pOutputStream)
			throws Exception {
		DOMSource mDOMSource = new DOMSource(pNode);

		StreamResult mStreamResult = new StreamResult(pOutputStream);

		Transformer mTransformer = cTransformerFactory.newTransformer();
		mTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
		mTransformer.setOutputProperty(OutputKeys.ENCODING, "GB2312");

		mTransformer.transform(mDOMSource, mStreamResult);
	}    
	private TransformerFactory cTransformerFactory = TransformerFactory.newInstance();
	
    public static void main(String[] args) {
        try {                   	
            LLTJUploadXml tBusinessDeal = new LLTJUploadXml();            
            tBusinessDeal.run();        	       
        } catch (Exception e) {
            e.printStackTrace();
        }
      } 
    }
