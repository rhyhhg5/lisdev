package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.SendMsgMail;
import com.sinosoft.lis.taskservice.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun1;
import java.io.IOException;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;
import java.net.*;

import com.sinosoft.lis.operfee.CpayRnewMsgBL;
import com.sinosoft.lis.llcase.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 续期续保发送客户提醒短信
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author MN
 * @version 1.0
 */
public class CpayRnewMsgTask extends TaskThread {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    String Content = "";
    String Email = "";
    String Mobile = "";
    // 输入数据的容器
    private VData mInputData = new VData();

    private MMap map = new MMap();
    public CpayRnewMsgTask() {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run() {
        if (!sendCustomerMsg()) {
            return;
        }
    }

    private boolean sendCustomerMsg() {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        CpayRnewMsgBL tCpayRnewMsgBL = new CpayRnewMsgBL();
        if (!tCpayRnewMsgBL.submitData(mVData, "XQMSG")) {
            System.out.println("续期续保短信发送失败！");
            CError tError = new CError();
            tError.moduleName = "CpayRnewMsgBL";
            tError.functionName = "submitData";
            tError.errorMessage = "续期续保短信发送失败";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String args[]) {
        CpayRnewMsgTask a = new CpayRnewMsgTask();
        a.run();

    }
}
