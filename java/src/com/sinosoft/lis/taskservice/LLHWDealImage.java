package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.yibaotong.LLHWBGetScanImageBL;
import com.sinosoft.utility.CErrors;

public class LLHWDealImage {

	/**错误的容器*/
    public CErrors mErrors = new CErrors();


    String opr = "";
    public LLHWDealImage()
    {}
    
    public void run() {
    	LLHWBGetScanImageBL tLLHWBGetScanImageBL = new LLHWBGetScanImageBL();
    	if(!tLLHWBGetScanImageBL.submitData()) {
    		System.out.println("影像件处理有问题");
            System.out.println(mErrors.getErrContent());
            opr ="false";
            return;
    	}else {
    		System.out.println("影像件处理成功");
       	 	opr ="ture";
    	}
    }
    
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
}
