package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LZCardJourUsedSchema;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LZCardJourUsedSet;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CardJourUsed extends TaskThread
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData inputData = new VData();

    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput globalInput = new GlobalInput();
    //统一更新日期，时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();


    public CardJourUsed()
    {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run()
    {
        if(!submit())
        {
            return;
        }
    }

    //公共的处理方法
    public boolean submit()
    {
        //String sql = getSql();
        if(!setInputDate())
        {
            return false;
        }

        PubSubmit pubSubmit = new PubSubmit();
        if(pubSubmit.submitData(inputData, "INSERT||MAIN"))
        {
            return true;
        }
        else
        {
            mErrors.copyAllErrors(pubSubmit.mErrors);
            mErrors.addOneError("单证日结出错");
            System.out.println("单证日结出错");
            return false;
        }
    }

    //设置日结值
    public boolean setInputDate()
    {
        String ownerSql = "select sendoutcom from lzcard where modifydate = '" + currentDate + "' union select receivecom from lzcardtrack where modifydate = '" + currentDate + "'";
        boolean isFirst = ifFirst();
        if(isFirst) ownerSql = "select sendoutcom from lzcard union select receivecom from lzcardtrack";
        ExeSQL exeSQL = new ExeSQL();
        SSRS ownerSSRS = exeSQL.execSQL(ownerSql);
        //String certifyTypeSql = "select distinct p.certifycode, d.certifyname from LZCardPrint p, lmcertifydes d where p.certifycode = d.certifycode and d.certifyclass = 'D'";
        String certifyTypeSql = "select distinct t.certifycode, d.certifyname, d.certifyclass from lzcardtrack t, lmcertifydes d "
                                + "where t.certifycode = d.certifycode and modifydate = '" + currentDate + "'";
        if(isFirst) certifyTypeSql = "select distinct t.certifycode, d.certifyname, d.certifyclass from lzcardtrack t, lmcertifydes d "
        							+ "where t.certifycode = d.certifycode";
        SSRS certifyTypeSSRS = exeSQL.execSQL(certifyTypeSql);
        String owner = "";
        String certifyCode = "";
        String certifyName = "";
        String sql = "";
        LZCardJourUsedSet tLZCardJourUsedSet = new LZCardJourUsedSet();

        //System.out.println("单证拥有者数量：" + ownerSSRS.getMaxRow());
        if (ownerSSRS.getMaxRow() > 0)
        {
            for(int j = 1; j <= certifyTypeSSRS.getMaxRow(); j++)
            {
                certifyCode = certifyTypeSSRS.GetText(j, 1);
                certifyName = certifyTypeSSRS.GetText(j, 2);System.out.println(certifyCode);
                sql = "select (select int(sum(sumcount)) from lzcard where modifydate - 1 day < '" +
                    currentDate + "' and receivecom = 'A86' and certifycode = '" +
                    certifyCode +  "' and (state in ('0', '3') or state is null)) kucun, "
                    + "(select int(sum(sumcount)) from lzcardtrack where modifydate = '" +
                    currentDate + "' and receivecom = 'A86' and certifycode = '" +
                    certifyCode + "' and state = '0') jieshou, "
                    + "(select int(sum(sumcount)) from lzcardtrack where modifydate = '" +
                    currentDate + "' and receivecom = 'A86' and certifycode = '" +
                    certifyCode + "' and (state = '3' or state is null)) huishou, "
                    + "(select int(sum(sumcount)) from lzcardtrack where modifydate = '" +
                    currentDate + "' and sendoutcom = 'A86' and certifycode = '" +
                    certifyCode + "' and state = '8') chuku, "
                    + "(select int(sum(sumcount)) from lzcardtrack where modifydate = '" +
                    currentDate + "' and sendoutcom = 'A86' and certifycode = '" +
                    certifyCode + "' and state = '6') zuofei, "
                    + "(select int(sum(sumcount)) from lzcardtrack where modifydate = '" +
                    currentDate + "' and sendoutcom = 'A86' and certifycode = '" +
                    certifyCode + "' and state = '5') sunhui, "
                    + "(select int(sum(sumcount)) from lzcardtrack where modifydate = '" +
                    currentDate + "' and sendoutcom = 'A86' and certifycode = '" +
                    certifyCode + "' and state = '4') yishi, "
                    + "(select int(sum(sumcount)) from lzcardtrack where modifydate = '" +
                    currentDate + "' and receivecom = 'A86' and certifycode = '" +
                    certifyCode + "' and state = '3') kongbaihuixiao, "
                    + "(select sum(certifyprice*sumcount)/sum(sumcount) from LZCardPrint where certifycode = '" +
                    certifyCode + "') danjia, "
                    + "'', "
                    + "'' "
                    + " from dual";
                System.out.println("sql : " + sql);
                SSRS cardJourUsedSSRS = exeSQL.execSQL(sql);
                map.put(setLZCardJourUsedSchema(cardJourUsedSSRS, certifyCode, certifyName, "A86"), "INSERT");
            }
        }

        for(int i = 1; i <= ownerSSRS.getMaxRow(); i++)
        {
            owner = ownerSSRS.GetText(i, 1);
            sql = "";
            for(int j = 1; j <= certifyTypeSSRS.getMaxRow(); j++)
            {
                certifyCode = certifyTypeSSRS.GetText(j, 1);
                certifyName = certifyTypeSSRS.GetText(j, 2);
                if(certifyTypeSSRS.GetText(j, 3).equals("D"))
                {
                    if (owner.startsWith("A"))
                    {
                        if (owner.length() == 3)
                        {
                            continue;
                        }
                        else if (owner.length() == 5)
                        {
                            sql = "select (select sum(sumcount) from lzcard where modifydate - 1 day < '" +
                                    currentDate + "' and receivecom = '" +
                                    owner + "' and certifycode = '" +
                                    certifyCode +
                                    "' and (state in ('0', '8', '3') or state is null)) kucun, "
                                    +
                                    "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                    currentDate + "' and receivecom = '" +
                                    owner + "' and certifycode = '" +
                                    certifyCode +
                                    "' and state in ('0', '8')) jieshou, "
                                    +
                                    "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                    currentDate + "' and receivecom = '" +
                                    owner + "' and certifycode = '" +
                                    certifyCode +
                                    "' and (state = '3' or state is null)) huishou, "
                                    +
                                    "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                    currentDate + "' and sendoutcom = '" +
                                    owner + "' and certifycode = '" +
                                    certifyCode + "' and state = '9') chuku, "
                                    +
                                    "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                    currentDate + "' and sendoutcom = '" +
                                    owner + "' and certifycode = '" +
                                    certifyCode + "' and state = '6') zuofei, "
                                    +
                                    "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                    currentDate + "' and sendoutcom = '" +
                                    owner + "' and certifycode = '" +
                                    certifyCode + "' and state = '5') sunhui, "
                                    +
                                    "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                    currentDate + "' and sendoutcom = '" +
                                    owner + "' and certifycode = '" +
                                    certifyCode + "' and state = '4') yishi, "
                                    +
                                    "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                    currentDate + "' and receivecom = '" +
                                    owner + "' and certifycode = '" +
                                    certifyCode +
                                    "' and state = '3') kongbaihuixiao, "
                                    + "(select sum(certifyprice*sumcount)/sum(sumcount) from LZCardPrint where certifycode = '" +
                                    certifyCode + "') danjia, "
                                    + "'', "
                                    + "'' "
                                    + " from dual";

                        }
                    }
                    else if (owner.startsWith("B"))
                    {
                        sql = "select (select sum(sumcount) from lzcard where modifydate - 1 day < '" +
                                currentDate + "' and receivecom = '" + owner +
                                "' and certifycode = '" + certifyCode +
                                "' and (state in ('0', '9', '3') or state is null)) kucun, "
                                +
                                "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                currentDate + "' and receivecom = '" + owner +
                                "' and certifycode = '" + certifyCode +
                                "' and state in ('0', '9')) jieshou, "
                                +
                                "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                currentDate + "' and receivecom = '" + owner +
                                "' and certifycode = '" + certifyCode +
                                "' and (state = '3' or state is null)) huishou, "
                                +
                                "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                currentDate + "' and sendoutcom = '" + owner +
                                "' and certifycode = '" + certifyCode +
                                "' and state in ('10', '11')) chuku, "
                                +
                                "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                currentDate + "' and sendoutcom = '" + owner +
                                "' and certifycode = '" + certifyCode +
                                "' and state = '6') zuofei, "
                                +
                                "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                currentDate + "' and sendoutcom = '" + owner +
                                "' and certifycode = '" + certifyCode +
                                "' and state = '5') sunhui, "
                                +
                                "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                currentDate + "' and sendoutcom = '" + owner +
                                "' and certifycode = '" + certifyCode +
                                "' and state = '4') yishi, "
                                +
                                "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                currentDate + "' and receivecom = '" + owner +
                                "' and certifycode = '" + certifyCode +
                                "' and state = '3') kongbaihuixiao, "
                                + "(select sum(certifyprice*sumcount)/sum(sumcount) from LZCardPrint where certifycode = '" +
                                certifyCode + "') danjia, "
                                +
                                "(select sum(sumcount) from lzcard where modifydate - 1 day < '" +
                                currentDate + "' and receivecom = '" + owner +
                                "' and certifycode = '" + certifyCode +
                                "' and (state in ('0', '9', '3') or state is null)) weilingyong, "
                                +
                                "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                                currentDate + "' and sendoutcom = '" + owner +
                                "' and certifycode = '" + certifyCode +
                                "' and state in ('2', '12')) zhengchangshiyong "
                                + " from dual";
                          addCompanyInfo(owner, certifyCode, certifyName, ownerSSRS);
                    }
                    else
                    {
                        continue;
                    }

                }
                else if(certifyTypeSSRS.GetText(j, 3).equals("P"))
                {
                    if (owner.startsWith("A") || owner.startsWith("B"))
                    {
                    	if (owner.startsWith("A") && owner.length() == 3)  continue;
                    	
                        sql = "select (select sum(sumcount) from lzcard where modifydate - 1 day < '" +
                            currentDate + "' and receivecom = '" + owner +
                            "' and certifycode = '" + certifyCode +
                            "' and stateflag = '0') kucun, "
                            +
                            "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                            currentDate + "' and receivecom = '" + owner +
                            "' and certifycode = '" + certifyCode +
                            "' and stateflag = '0' and operateflag = '0') jieshou, "
                            +
                            "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                            currentDate + "' and receivecom = '" + owner +
                            "' and certifycode = '" + certifyCode +
                            "' and stateflag = '0' and operateflag = '2') huishou, "
                            +
                            "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                            currentDate + "' and sendoutcom = '" + owner +
                            "' and certifycode = '" + certifyCode +
                            "' and stateflag = '0') chuku, "
                            +
                            "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                            currentDate + "' and receivecom = '" + owner +
                            "' and certifycode = '" + certifyCode +
                            "' and stateflag = '2') zuofei, "
                            +
                            "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                            currentDate + "' and receivecom = '" + owner +
                            "' and certifycode = '" + certifyCode +
                            "' and stateflag = '4') sunhui, "
                            +
                            "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                            currentDate + "' and receivecom = '" + owner +
                            "' and certifycode = '" + certifyCode +
                            "' and stateflag = '3') yishi, "
                            +
                            "0 kongbaihuixiao, "
                            + "(select sum(certifyprice*sumcount)/sum(sumcount) from LZCardPrint where certifycode = '" +
                            certifyCode + "') danjia, "
                            +
                            "(select sum(sumcount) from lzcard where modifydate - 1 day < '" +
                            currentDate + "' and receivecom = '" + owner +
                            "' and certifycode = '" + certifyCode +
                            "' and stateflag = '0') weilingyong, "
                            +
                            "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                            currentDate + "' and certifycode = '" + certifyCode +
                            "' and ((stateflag = '1' and receivecom = '" + owner + "') or (stateflag = '5' and sendoutcom = '" + owner + "'))) zhengchangshiyong "
                            + " from dual";
                        
                        addCompanyInfo(owner, certifyCode, certifyName, ownerSSRS);
                    }
                    else
                    {
                        continue;
                    }
                }

                System.out.println("sql : " + sql);
                if(sql.equals("")) continue;
                SSRS cardJourUsedSSRS = exeSQL.execSQL(sql);
                map.put(setLZCardJourUsedSchema(cardJourUsedSSRS, certifyCode, certifyName, owner), "INSERT");
            }
        }

        inputData.add(map);
        return true;
    }
    
    //
    public boolean ifFirst()
    {
    	ExeSQL exeSQL = new ExeSQL();
        SSRS ifFirstSqlSSRS = exeSQL.execSQL("SELECT COUNT(1) FROM LZCardJourUsed");
        System.out.println(ifFirstSqlSSRS.GetText(1, 1).equals("22"));
        if(ifFirstSqlSSRS.getMaxCol() > 0 && ifFirstSqlSSRS.GetText(1, 1).equals("0"))
        {
        	return true;
        }
        return false;
    }

    //
    public void addCompanyInfo(String owner, String certifyCode, String certifyName, SSRS ownerSSRS)
    {
        String sendOutSql = "SELECT SendOutCom FROM LZAccess WHERE ReceiveCom = '" + owner +"'";
        ExeSQL exeSQL = new ExeSQL();
        SSRS sendOutSqlSSRS = exeSQL.execSQL(sendOutSql);
        for(int i = 1; i <= sendOutSqlSSRS.getMaxRow(); i++)
        {
            String sendOut = sendOutSqlSSRS.GetText(i, 1);
            boolean flag = true;
            for(int j = 1; j <= ownerSSRS.getMaxRow(); j++)
            {
                if(sendOut.equals(ownerSSRS.GetText(j, 1)))
                {
                    flag = false;
                    break;
                }
            }
            if(flag)
            {
                String sql = "select (select sum(sumcount) from lzcard where modifydate - 1 day < '" +
                    currentDate + "' and receivecom = '" + sendOut +
                    "' and certifycode = '" +
                    certifyCode + "' and (state in ('0', '8', '3') or state is null)) kucun, "
                    + "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                    currentDate + "' and receivecom = '" + sendOut +
                    "' and certifycode = '" + certifyCode + "' and state in ('0', '8')) jieshou, "
                    + "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                    currentDate + "' and receivecom = '" + sendOut +
                    "' and certifycode = '" +
                    certifyCode + "' and (state = '3' or state is null)) huishou, "
                    + "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                    currentDate + "' and sendoutcom = '" + sendOut +
                    "' and certifycode = '" + certifyCode + "' and state = '9') chuku, "
                    + "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                    currentDate + "' and sendoutcom = '" + sendOut +
                    "' and certifycode = '" + certifyCode + "' and state = '6') zuofei, "
                    + "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                    currentDate + "' and sendoutcom = '" + sendOut +
                    "' and certifycode = '" + certifyCode + "' and state = '5') sunhui, "
                    + "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                    currentDate + "' and sendoutcom = '" + sendOut +
                    "' and certifycode = '" + certifyCode + "' and state = '4') yishi, "
                    + "(select sum(sumcount) from lzcardtrack where modifydate = '" +
                    currentDate + "' and receivecom = '" + sendOut +
                    "' and certifycode = '" + certifyCode + "' and state = '3') kongbaihuixiao, "
                    + "(select sum(certifyprice*sumcount)/sum(sumcount) from LZCardPrint where certifycode = '" +
                    certifyCode + "') danjia, "
                    + "'', "
                    + "'' "
                    + " from dual";
                System.out.println("sql : " + sql);
                SSRS cardJourUsedSSRS = exeSQL.execSQL(sql);
                map.put(setLZCardJourUsedSchema(cardJourUsedSSRS, certifyCode, certifyName, owner), "INSERT");
            }
        }
    }

    //
    public LZCardJourUsedSchema setLZCardJourUsedSchema(SSRS cardJourUsedSSRS, String certifyCode, String certifyName, String owner)
    {
        LZCardJourUsedSchema tLZCardJourUsedSchema = new LZCardJourUsedSchema();
        tLZCardJourUsedSchema.setCertifyCode(certifyCode);//单证编码
        tLZCardJourUsedSchema.setCertifyName(certifyName);//单证名称
        tLZCardJourUsedSchema.setOwner(owner.substring(1));//所有者
        tLZCardJourUsedSchema.setPrtNo("0");//印刷号

        if(cardJourUsedSSRS == null || cardJourUsedSSRS.getMaxRow() == 0)
        {
            tLZCardJourUsedSchema.setCertifyPrice(0); //单证价格
            tLZCardJourUsedSchema.setStockpileNumber(0); //库存量
            tLZCardJourUsedSchema.setUnemployedNumber(0); //未用量
            tLZCardJourUsedSchema.setReceiveNumber(0); //接收入库量
            tLZCardJourUsedSchema.setCallBackNumber(0); //回收入库量
            tLZCardJourUsedSchema.setSendOutNumber(0); //出库量
            tLZCardJourUsedSchema.setNaturalNumber(0); //正常使用量
            tLZCardJourUsedSchema.setBlankOutNumber(0); //作废量
            tLZCardJourUsedSchema.setMarNumber(0); //损毁量
            tLZCardJourUsedSchema.setLostNumber(0); //遗失量
            tLZCardJourUsedSchema.setRetourNumber(0); //空白回销量
        }
        else
        {
            tLZCardJourUsedSchema.setCertifyPrice(cardJourUsedSSRS.GetText(1, 9)); //单证价格
            tLZCardJourUsedSchema.setStockpileNumber(cardJourUsedSSRS.GetText(1, 1)); //库存量
            tLZCardJourUsedSchema.setUnemployedNumber(cardJourUsedSSRS.GetText(1, 10)); //未用量
            tLZCardJourUsedSchema.setReceiveNumber(cardJourUsedSSRS.GetText(1, 2)); //接收入库量
            tLZCardJourUsedSchema.setCallBackNumber(cardJourUsedSSRS.GetText(1, 3)); //回收入库量
            tLZCardJourUsedSchema.setSendOutNumber(cardJourUsedSSRS.GetText(1, 4)); //出库量
            tLZCardJourUsedSchema.setNaturalNumber(cardJourUsedSSRS.GetText(1, 11)); //正常使用量
            tLZCardJourUsedSchema.setBlankOutNumber(cardJourUsedSSRS.GetText(1, 5)); //作废量
            tLZCardJourUsedSchema.setMarNumber(cardJourUsedSSRS.GetText(1, 6)); //损毁量
            tLZCardJourUsedSchema.setLostNumber(cardJourUsedSSRS.GetText(1, 7)); //遗失量
            tLZCardJourUsedSchema.setRetourNumber(cardJourUsedSSRS.GetText(1, 8)); //空白回销量
        }
        tLZCardJourUsedSchema.setJourDate(currentDate);
        tLZCardJourUsedSchema.setFlag("");
        tLZCardJourUsedSchema.setOperator("system");
        tLZCardJourUsedSchema.setMakeDate(currentDate);
        tLZCardJourUsedSchema.setMakeTime(currentTime);
        tLZCardJourUsedSchema.setModifyDate(currentDate);
        tLZCardJourUsedSchema.setModifyTime(currentTime);

        return tLZCardJourUsedSchema;
    }

    //自定义脚本
    public String getSql()
    {

        String sql = "INSERT INTO LZCardJourUsed ()";
        return sql;
    }


    public static void main(String[] args)
    {
        CardJourUsed cardjourused = new CardJourUsed();
        cardjourused.run();
    }
}
