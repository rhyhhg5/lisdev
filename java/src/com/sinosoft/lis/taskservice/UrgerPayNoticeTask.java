package com.sinosoft.lis.taskservice;
import com.sinosoft.lis.taskservice.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.finfee.FirstHastenUI;
/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author ZhangXing
 * @version 1.0
 */

public class UrgerPayNoticeTask extends TaskThread
{
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;
  private String mOperator;    //操作员

  public UrgerPayNoticeTask()
  {
  }


  public void run()
  {
    /*
       查询出所有待催办的通知书
       15－催缴

    */
   String SqlSQL ="select prtno from ljspay a,lccont b where a.otherno=b.prtno and a. OtherNoType='9' "
                  +"and a.BankOnTheWayFlag='0' and a.SendBankCount>=1 and BankSuccFlag in('0','2') "
                  +"and b.paymode='4' and current date >=StartPayDate and conttype='1' "
                  +"and appflag<>'1' and approveflag='9' and uwflag not in('a','8','1')  "
                  +"union "
                  +"select prtno from lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.paymode='1' "
                  +"and b.code='07' and conttype='1' and appflag<>'1' and approveflag='9' and uwflag not in('a','8','1') and paymode='1' "
                  +"and prtno not in (select otherno from ljtempfee where enteraccdate is not null) and b.ForMakeDate=current date ";
    System.out.println(SqlSQL);

    ExeSQL q_exesql = new ExeSQL();
    SSRS tssrs = new SSRS();
    tssrs = q_exesql.execSQL(SqlSQL);

    int m =  tssrs.getMaxRow();
    System.out.println("参数个数:" + m);

     GlobalInput tG = new GlobalInput();
     tG.Operator = "000";
     tG.ManageCom = "86";
     tG.ComCode = "86";

     for (int i = 1; i<=m; i++)
     {
         TransferData tTransferData = new TransferData();
         tTransferData.setNameAndValue("PRTNO",tssrs.GetText(i,1));
         System.out.println("PRTNO"+tssrs.GetText(i,1));
         VData tVData = new VData();
         tVData.add(tTransferData);
         tVData.add(tG);
          FirstHastenUI tFirstHastenUI = new FirstHastenUI();
         if (tFirstHastenUI.submitData(tVData, "INSERT") == false)
         {
             CError tError = new CError();
             tError.moduleName = "UWBatchManuNormGChkBL";
             tError.functionName = "callPol";
             tError.errorMessage = "调用错误UWManuNormGChkBL 操作员：" + mOperator + "）";
             this.mErrors.addOneError(tError);

         }
         System.out.println("＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");


        // Write your code here
     }


  }
  public static void main(String[] args)
  {
    UrgerPayNoticeTask tUrgerNoticeTask = new UrgerPayNoticeTask();
    tUrgerNoticeTask.run();
  }


}
