package com.sinosoft.lis.taskservice;

import java.io.File;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class OffshoreRescue {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = new GlobalInput();

	/** 文件路径 */
	private String tOutXmlPath = "";

	/** 应用路径 */
	private String mURL = "";
	
	private String fileName = "";
	
	String[][] mToExcel = null;
	
	SSRS tSSRS = new SSRS();
	
	private String mSql = null;
	
	//执行任务
	public void run(){
		dealData();
	}
	
	public boolean dealData(){
		
		System.out.println("开始执行批处理:" + PubFun.getCurrentTime());
		
		//准备数据
		getInputData();
		
		//生成报表
		if(!getReport()){
			System.out.println("生成报表数据错误！");
		}
		
		//发送邮件
		if(!send()){
			System.out.println("发送邮件过程中发生错误！");
		}
		
		//将报送的数据置为已报送状态，避免重复报送
		changePoliciesState();
		
		System.out.println("批处理执行完毕:" + PubFun.getCurrentTime());
		
		return true;
	}
	
	private void getInputData(){
		//设置管理机构
		mGI.ManageCom = "86";
		mGI.Operator = "001";
		mGI.ComCode = "86";
		
		//设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL) + "ExcelFile/";//或者换一个地方存放
//		mURL = "F:/ExcelFile/";
		File mFileDir = new File(mURL);
		if(!mFileDir.exists()){
			if(!mFileDir.mkdir()){
				System.out.println("创建目录[" + mURL.toString() + "]失败！" + mFileDir.getPath());
			}
		}
	}
	
	private boolean getReport(){
		
		fileName = "OffshoreRescue.xls";
		tOutXmlPath = mURL + fileName;
		System.out.println("文件全称:" + tOutXmlPath);
		WriteToExcel t = new WriteToExcel(fileName);
		t.createExcelFile();
		t.setAlignment("CENTER");
		t.setFontName("宋体");
		String[] sheetName = {new String("境外救援数据报表")};
		t.addSheetGBK(sheetName);
		if(!getData()){
			mToExcel = getDefalutData();
		}
		t.setData(0,mToExcel);
		
		try{
			t.write(mURL);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println("生成文件完成！");
		
		return true;
	}
	
	private boolean send(){
		//开始发送邮件
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
		SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
			.GetText(1, 2), "picchealth");
		//设置邮件发送信息
		String sendInf = "";
		String Fj = "";
		if(tSSRS.MaxRow <= 0){
			sendInf = "无境外救援承保数据！";
		}else{
			sendInf = "境外救援报送数据见附件！";
			Fj = tOutXmlPath;
		}
		
		tMailSender.setSendInf("境外救援报送数据",sendInf,Fj);
		//通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
		tMailSender.setToAddress("testtoczwnn");
		if(!tMailSender.sendMail()){
			tMailSender.getErrorMessage();
		}
		deleteFile(tOutXmlPath);
		
		return true;
	}
	
	public void changePoliciesState(){
		MMap map = new MMap();
		String update_SQL = "update lcgrpcont set standbyflag1='1' where cardflag='0' and appflag='1' and standbyflag1 is null and exists (select 1 from lcpol where grpcontno=lcgrpcont.grpcontno and riskcode in ('1501','1502'))";
		map.put(update_SQL,"UPDATE");
		VData mResult = new VData();
		mResult.add(map);
		if(!(new PubSubmit().submitData(mResult, ""))){
			System.out.println("数据提交错误：报送过的保单状态改变时发生异常！");
		}
	}
	
	public boolean getData(){
		System.out.println("查询数据...");
		
		ExeSQL tExeSQL = new ExeSQL();
		mSql = " select a.grpcontno,a.signdate,a.signcom,a.cvalidate,a.CInValiDate,b.riskcode,b.mult," +
		          " ( select d1.standmoney from lcinsured a1, lcgrpcont b1, lcpol c1, lcget d1 where a1.grpcontNo=a.grpcontNo and a1.insuredno=e.insuredno and b1.grpcontNo=a1.grpcontNo and c1.grpcontNo=a1.grpcontNo and c1.insuredno=a1.insuredno and c1.riskcode='1502' and d1.grpcontNo=a1.grpcontNo and d1.polno=c1.polno and d1.getdutycode='613207' )," +
		          " (select d2.standmoney from lcinsured a2, lcgrpcont b2, lcpol c2, lcget d2 where a2.grpcontNo=a.grpcontNo and a2.insuredno=e.insuredno and b2.grpcontNo=a2.grpcontNo and c2.grpcontNo=a2.grpcontNo and c2.insuredno=a2.insuredno and c2.riskcode='1502' and d2.grpcontNo=a2.grpcontNo and d2.polno=c2.polno and d2.getdutycode='613208')," +
		          " (select d3.standmoney from lcinsured a3, lcgrpcont b3, lcpol c3, lcget d3 where a3.grpcontNo=a.grpcontNo and a3.insuredno=e.insuredno and b3.grpcontNo=a3.grpcontNo and c3.grpcontNo=a3.grpcontNo and c3.insuredno=a3.insuredno and c3.riskcode='1502'and d3.grpcontNo=a3.grpcontNo and d3.polno=c3.polno and d3.getdutycode='613209' )," +
		          " b.insuredname,b.insuredno,e.EnglishName,e.Birthday,e.OthIDNo," +
		          " d.linkmanaddress1,d.linkmanzipcode1,d.linkman1,d.phone1 from lcgrpcont a, lcpol b,lcgrpappnt c, lcgrpaddress d, lcinsured e " +
		          " where a.standbyflag1 is null and a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno and c.CustomerNo=d.CustomerNo and c.AddressNo=d.AddressNo" +
		          " and a.grpcontno=e.grpcontno and a.cardflag='0' and b.riskcode ='1502' and b.insuredno=e.insuredno and a.appflag='1' and a.grpcontno=e.grpcontno and b.contno=e.contno" +
		          " union all " +
		          " select a.grpcontno,a.signdate,a.signcom,a.cvalidate,a.CInValiDate,b.riskcode,b.mult," +
		          " ( select d1.standmoney from lcinsured a1, lcgrpcont b1, lcpol c1, lcget d1 where a1.grpcontNo=a.grpcontNo and a1.insuredno=e.insuredno and b1.grpcontNo=a1.grpcontNo and c1.grpcontNo=a1.grpcontNo and c1.insuredno=a1.insuredno and c1.riskcode='1501' and d1.grpcontNo=a1.grpcontNo and d1.polno=c1.polno and d1.getdutycode='611207' )," +
		          " 0," +
		          " 0," +
		          " b.insuredname,b.insuredno,e.EnglishName,e.Birthday,e.OthIDNo," +
		          " d.linkmanaddress1,d.linkmanzipcode1,d.linkman1,d.phone1 from lcgrpcont a, lcpol b,lcgrpappnt c, lcgrpaddress d, lcinsured e " +
		          " where a.standbyflag1 is null and a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno and c.CustomerNo=d.CustomerNo and c.AddressNo=d.AddressNo" +
		          " and a.grpcontno=e.grpcontno and a.cardflag='0' and b.riskcode='1501' and b.insuredno=e.insuredno and a.appflag='1' and a.grpcontno=e.grpcontno and b.contno=e.contno";
		
		System.out.println("查询sql:" + mSql);
		tSSRS = tExeSQL.execSQL(mSql);
		
		int count = tSSRS.getMaxRow();
		mToExcel = new String[count+3][20];
		mToExcel[0][0] = "Policy No";
		mToExcel[0][1] = "Undersign Date";
		mToExcel[0][2] = "Undersign Branch";
		mToExcel[0][3] = "Valid from";
		mToExcel[0][4] = "Valid to";
		mToExcel[0][5] = "Product code";
		mToExcel[0][6] = "Product Level";
		mToExcel[0][7] = "Sum of Inpatient Treatment";
		mToExcel[0][8] = "Sum of  Emergecy Outpatient Treatment";
		mToExcel[0][9] = "Sum of  Emergecy Dental Treatment";
		mToExcel[0][10] = "Insured Name";
		mToExcel[0][11] = "Insured No.";
		mToExcel[0][12] = "Insured  Chinese Alphabetics";
		mToExcel[0][13] = "Birth date";
		mToExcel[0][14] = "Passport or ID No.";
		mToExcel[0][15] = "address";
		mToExcel[0][16] = "Postal code";
		mToExcel[0][17] = "Name of Contact Person In China";
		mToExcel[0][18] = "Tel. No. of Contact Person In China";
		
		if(count <= 0){
			System.out.println("没有查询到数据");
		}
		
		for(int i=1; i<=count; i++){
			mToExcel[i][0] = tSSRS.GetText(i, 1);
			mToExcel[i][1] = tSSRS.GetText(i, 2);
			mToExcel[i][2] = tSSRS.GetText(i, 3);
			mToExcel[i][3] = tSSRS.GetText(i, 4);
			mToExcel[i][4] = tSSRS.GetText(i, 5);
			mToExcel[i][5] = tSSRS.GetText(i, 6);
			mToExcel[i][6] = tSSRS.GetText(i, 7);
			mToExcel[i][7] = tSSRS.GetText(i, 8);
			mToExcel[i][8] = tSSRS.GetText(i, 9);
			mToExcel[i][9] = tSSRS.GetText(i, 10);
			mToExcel[i][10] = tSSRS.GetText(i, 11);
			mToExcel[i][11] = tSSRS.GetText(i, 12);
			mToExcel[i][12] = tSSRS.GetText(i, 13);
			mToExcel[i][13] = tSSRS.GetText(i, 14);
			mToExcel[i][14] = tSSRS.GetText(i, 15);
			mToExcel[i][15] = tSSRS.GetText(i, 16);
			mToExcel[i][16] = tSSRS.GetText(i, 17);
			mToExcel[i][17] = tSSRS.GetText(i, 18);
			mToExcel[i][18] = tSSRS.GetText(i, 19);
		}
		
		System.out.println("数据准备完成！");
		
		return true;
	}
	
	private String[][] getDefalutData(){
		String[][] tToExcel = new String[2][30];
		tToExcel[0][0] = "无数据";
		return tToExcel;
	}
	
	private boolean deleteFile(String cFilePath){
		File f = new File(cFilePath);
		if (!f.exists()) {
			CError tError = new CError();
			tError.moduleName = "MonthDataCatch";
			tError.functionName = "deleteFile";
			tError.errorMessage = "没有找到需要删除的文件";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		f.delete();
		return true;
	}
	
	public static void main(String[] args) {
		new OffshoreRescue().run();
	}
}
