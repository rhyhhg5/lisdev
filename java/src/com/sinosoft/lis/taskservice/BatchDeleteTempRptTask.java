package com.sinosoft.lis.taskservice;

import java.io.File;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 定时对3个月前的报表进行删除
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author 
 * @version 1.0
 */
public class BatchDeleteTempRptTask extends TaskThread
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData mInputData = new VData();

    // 统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

    private MMap map = new MMap();

    public BatchDeleteTempRptTask()
    {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run()
    {
        if(!deleteRpt())
        {
            return;
        }
    }

    //执行批量删除
    private boolean deleteRpt()
    {
		  String tDate = PubFun.calDate(mCurrentDate, 0, "M", "");
		  //目标日期 2015-02-13
		  String[] tDateArray = tDate.split("-");
		  //目标日期  年2015
		  String tYear = tDateArray[0];
		  System.out.println("目标年份:"+tYear);
		  //目标日期  月201502
		  String tMonth = tDateArray[0]+tDateArray[1];
		  System.out.println("目标月份:"+tMonth);
		  //目标日期   日20150213
		  String tDay = tDateArray[0]+tDateArray[1]+tDateArray[2];
		  System.out.println("目标日期:"+tDay);
		  //当前年份
		  String nowYear = mCurrentDate.substring(0, 4);
		  System.out.println("当前年份:"+nowYear);
		  //工程路径
		  LDSysVarDB tLDSysVarDB = new LDSysVarDB();
	      tLDSysVarDB.setSysVar("LPCSVREPORT");

	      if (!tLDSysVarDB.getInfo()) {
	    	  return false;
	      }
	      //原路径
		  String tPath = tLDSysVarDB.getSysVarValue(); 
		  //如果三个月前是去年 删除去年文件夹
		  if(Integer.parseInt(nowYear)>Integer.parseInt(tYear))
		  {
			  delFiles(tPath+tYear);
		  }
		  else
		  {
			  int intMonth = Integer.parseInt(tDateArray[1]);
			  //循环月份删除
			  for (int i = intMonth; i <= intMonth&&i>0; i--) 
			  {
				  String tMonStr = String.valueOf(i);
				  if(tMonStr.length()==1)
				  {
					  tMonStr="0"+tMonStr;
				  }
				  delFiles(tPath+tYear+"/"+tYear+tMonStr);
				  System.out.println("删除目录为:"+tYear+"/"+tYear+tMonStr);
			  }
		  }
		  //如果去年还有 删掉
		  delFiles(tPath+String.valueOf(Integer.parseInt(tYear)-1));
	//      String sql = "Delete from LDRInfor where  ";
	//      map.put(sql, "DELETE"); //修改

        //提交数据
//        if(map.size() > 0)
//        {
//            mInputData.add(map);
//
//            PubSubmit tPubSubmit = new PubSubmit();
//
//            if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN"))
//            {
//                mErrors.copyAllErrors(tPubSubmit.mErrors);
//                mErrors.addOneError("删除出错");
//                System.out.println("删除出错");
//
//                return false;
//            }
//
//            return true;
//        }


        return true;
    }

  
  //删除文件夹以及文件夹下所有文件
	public void delFiles(String path) 
	{   
       // 首先根据path路径创建一个File对象   
       File file = new File(path);   
  
       // 判断所给的路径是否是一个目录是否存在，如果都成立再继续操作   
       if (file.exists() && file.isDirectory()) 
       {   
  
           // 如果目录下的没有文件则直接删除，否则继续进行后面的操作   
           if (file.listFiles().length == 0) 
           {   
              file.delete();   
           } 
           else 
           {   
			  // 如果目录下还有其他文件或目录，对这个目录下的所有文件对象进行遍历   
			  File[] delFile = file.listFiles();   
			  int i = delFile.length;   
			  for (int j=0; j<i; j++) 
			  {   
			       // 每遍历一个文件对象，判断其如果是目录，则   
				   // 调用自身方法进行递归操作，不是目录则直接删除   
				   if (delFile[j].isDirectory())
				   {   
					   delFiles(delFile[j].getAbsolutePath());   
				   }   
				   delFile[j].delete();   
			   }   
           }   
       }
	    //最后删除该文件夹
       if (file.exists() && file.isDirectory()) 
       { 
    	   file.delete();
       }
	} 
    public static void main(String args[])
    {
        BatchDeleteTempRptTask a = new BatchDeleteTempRptTask();
        a.run();

        if(a.mErrors.needDealError())
        {
            System.out.println(a.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
//    	//当前日期
//    	String[] tCurrentDate = PubFun.getCurrentDate().split("-");
//    	//当前日期  年
//    	String tYear = tCurrentDate[0];
//    	//当前日期  月
//    	String tMonth = tCurrentDate[0]+tCurrentDate[1];
//    	//当前日期   日
//    	String tDay = tCurrentDate[0]+tCurrentDate[1]+tCurrentDate[2];
//    	//原路径
//    	String tFilePath = "G:\\picch\\ui\\vtsfile\\";
//    	//年文件
//    	File yFile = new File(tFilePath+tYear);
//    	//月文件
//    	File mFile = new File(tFilePath+tYear+"/"+tMonth);
//    	//日文件
//    	File dFile = new File(tFilePath+tYear+"/"+tMonth+"/"+tDay); 
//
//    	if(!yFile.exists()||!mFile.exists()||!dFile.exists())
//    	{
//    		  dFile.mkdirs();
//    	}
    }

}
