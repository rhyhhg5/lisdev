package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class TYLpOutSendBQSolveData extends TaskThread{

	
	private ExeSQL mExeSQL = new ExeSQL();

	// TransInfo 标识信息
	private Element tPACKET = null;
	
    //TransInfo 标识信息
	private Element tHEAD = null;
	
    //TransInfo 标识信息
	private Element tBODY = null;

	// 保单信息 PolicyInfo
	private Element tGrpContInfo = null;

	// 分单层数据 InsuredsInfo
	private Element tContInfos = null;

	// 分单险种层数据 Policys
	private Element tPolInfos = null;

	// 险种责任层数据 Benefits
	private Element tDutyInfos = null;

	// 保单信息SQL
	private String mGrpContInfoSQL = "";

	// 分单层数据SQL
	private String mContInfosSQL = "";

	// 分单险种层数据SQL
	private String mPolInfosSQL = "";

	// 险种责任层数据 SQL
	private String mDutyInfosSQL = "";

	// 保单信息SSRS
	private SSRS tGrpContInfoSSRS = null;

	// 分单层数据SSRS
	private SSRS tContInfosSSRS = null;

	// 分单险种层数据SSRS
	private SSRS tPolInfosSSRS = null;

	// 险种责任层数据SSRS
	private SSRS tDutyInfosSSRS = null;

	// 保单数
	private int GrpContInfolen = 0;

	// 分单数
	private int ContInfoslen = 0;

	// 险种数
	private int PolInfoslen = 0;

	// 责任数
	private int DutyInfoslen = 0;
	//外包公司
	private SSRS WBTypeSSRS = null;
	
	/** 应用路径 */
	private String mURL = "";

	/** 文件路径 */
	private String mFilePath = "";
	//成功标志
	String opr = "";
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
	public void run(){
		System.out.println("-----$$$$理赔外包保全数据报送开始$$$$-----");
		//getInputData();
		getWBType();
//		createXml();
		System.out.println("-----$$$$理赔外包保全数据报送结束$$$$-----");
	}
	
	private void getWBType(){
		String tWBSQL = "select a.codetype from ldcode a,ldcode b where a.codetype = b.code and b.codetype = 'thridcompany' group by a.codetype with ur ";
		WBTypeSSRS =  mExeSQL.execSQL(tWBSQL);
		if(WBTypeSSRS.getMaxNumber()<1){
			System.out.println("无数据报送");
		}else{
			for(int i=1;i <= WBTypeSSRS.getMaxRow();i++){
				String tWBType =  WBTypeSSRS.GetText(i, 1);
				getInputData(tWBType);
				if(!dealData(tWBType)) {
					System.out.println("统一保全上报出问题了");
					opr = "false";
				}else {
					System.out.println("统一保全上报成功了");
					opr = "true";
				}
			}
		}
		
	}
	private void getInputData(String type) {

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);
//		mURL = "E:\\wdx\\统一保全\\";
		mURL = mURL + "vtsfile/WBClaim/"+type+"/"+PubFun.getCurrentDate()+"/";

		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}

	}
	
	public boolean dealData(String type) {
		mGrpContInfoSQL = " select lpg.edorno,"
                + " lcg.grpcontno as GrpContNo,"
                + " 'BQ' as OperateType,"
                + " lpg.edortype as EdorType,"
                + " (select edorname from lmedoritem where edorcode=lpg.edortype fetch first 1 rows only) as EdorName,"
                + " lpg.edorvalidate as EdorValiDate,"                                                                                 
                + " lcg.cvalidate as CValidDate,"
                + " lcg.cinvalidate as CInValiDate,"                                                           
                + " lcg.managecom as ManageCom,"
                + " lcg.stateFlag as StateFlag,"                                                          
                + " lcg.appntno as AppntNo,"
                + " lcg.grpname as GrpName,"                                                          
                + " (select grpaddress from lcgrpaddress where customerno=lcg.appntno and addressno =lcg.addressno) as PostalAddress,"
                + " lcg.peoples2 as Peoples2,"                                                                                       
                + " (select linkman1 from LCGrpAddress where customerno=Lcg.appntno and addressno=Lcg.addressno fetch first 1 rows only) As Name, "
				+ " (Select idtype From Lcgrpappnt Where Grpcontno = Lcg.Grpcontno) As IDType,  "
				+ " (Select idno From Lcgrpappnt Where Grpcontno = Lcg.Grpcontno) As IDNo,  "
				+ " (Select idstartdate From Lcgrpappnt Where Grpcontno = Lcg.Grpcontno) As IDStartDate,  "
				+ " (Select idenddate From Lcgrpappnt Where Grpcontno = Lcg.Grpcontno) As IDEndDate,  "
                + " lcg.Phone as Phone,"
                + " lcg.Fax as Fax,"
                + " lcg.EMail as EMail,"
                + " lcg.BankCode as ClaimBankCode,"                                                                                         
                + " (select bankname from ldbank where bankcode=lcg.BankCode fetch first 1 rows only) as ClaimBankName,"                
                + " lcg.BankAccNo as ClaimBankAccNo,"                                                                                       
                + " lcg.AccName as ClaimAccName,"                                                                                       
                + " lcg.PayMode as PayMode,"                                                                                           
                + " lcg.prem as Prem,"                                                                                              
                + " lcg.Remark as Remark"                                                                                              
                + " from lcgrpcont lcg,lpgrpedoritem lpg,lpedorapp lpe"                                                                
                + " where lpg.edorno=lpe.edoracceptno"                                                                                 
                + " and lcg.grpcontno=lpg.grpcontno"                                                                                   
                + " and lcg.appflag='1'"                                                                                               
                + " and lpe.edorstate='0'"                                                                                             
                + " and lpg.edortype in (select code from ldcode where codetype='LPOutSou_BQTYPE')" 
                + " and lcg.grpcontno in (select a.code from ldcode a,ldcode b where 1=1 and a.codetype = b.code and b.codetype = 'thridcompany' and a.codetype='"+type+"' )" 
                //ldcode.othersign 作为补提标示
                + " and (lpe.confdate=current date - 1 days "
                + " or exists (select a.code from ldcode a,ldcode b where 1=1 and a.codetype = b.code and b.codetype = 'thridcompany' and a.codetype='"+type+"' and a.code=lcg.grpcontno and a.othersign is not null and a.othersign='1' and lpe.confdate>=date(a.codealias) )"
                + " or exists (select a.code from ldcode a,ldcode b where 1=1 and a.codetype = b.code and b.codetype = 'thridcompany' and a.codetype='"+type+"' and a.code=lcg.grpcontno and current date - 1 days=date(a.codename)) ) "//当天配置的保单提取全部历史保全数据
                + " union "
                + " select lpg.edorno,"
                + " lcg.grpcontno as GrpContNo,"
                + " 'BQ' as OperateType,"
                + " lpg.edortype as EdorType,"
                + " (select edorname from lmedoritem where edorcode=lpg.edortype fetch first 1 rows only) as EdorName,"
                + " lpg.edorvalidate as EdorValiDate,"                                                                            
                + " lcg.cvalidate as CValidDate,"
                + " lcg.cinvalidate as CInValiDate,"                                                           
                + " lcg.managecom as ManageCom,"
                + " lcg.stateFlag as StateFlag,"                                                          
                + " lcg.appntno as AppntNo,"
                + " lcg.grpname as GrpName,"                                                          
                + " (select grpaddress from lcgrpaddress where customerno=lcg.appntno and addressno =lcg.addressno) as Peoples2,"
                + " lcg.peoples2 as Peoples2,"  
                + " (select linkman1 from LCGrpAddress where customerno=Lcg.appntno and addressno=Lcg.addressno fetch first 1 rows only) As Name, "
				+ " (Select idtype From Lcgrpappnt Where Grpcontno = Lcg.Grpcontno) As IDType,  "
				+ " (Select idno From Lcgrpappnt Where Grpcontno = Lcg.Grpcontno) As IDNo,  "
				+ " (Select idstartdate From Lcgrpappnt Where Grpcontno = Lcg.Grpcontno) As IDStartDate,  "
				+ " (Select idenddate From Lcgrpappnt Where Grpcontno = Lcg.Grpcontno) As IDEndDate,  "
                + " lcg.Phone as Phone,"                                                                                               
                + " lcg.Fax as Fax,"                                                                                                   
                + " lcg.EMail as EMail,"                                                                                               
                + " lcg.BankCode as ClaimBankCode,"                                                                                         
                + " (select bankname from ldbank where bankcode=lcg.BankCode fetch first 1 rows only) as ClaimBankName,"                
                + " lcg.BankAccNo as ClaimBankAccNo,"                                                                                       
                + " lcg.AccName as ClaimAccName,"                                                                                       
                + " lcg.PayMode as PayMode,"                                                                                           
                + " lcg.prem as Prem,"                                                                                              
                + " lcg.Remark as Remark"                                                                                              
                + " from lbgrpcont lcg,lpgrpedoritem lpg,lpedorapp lpe"                                                                
                + " where lpg.edorno=lpe.edoracceptno"                                                                                 
                + " and lcg.grpcontno=lpg.grpcontno"        
                + " and lpe.edorstate='0'"
                + " and lpg.edortype in (select code from ldcode where codetype='LPOutSou_BQTYPE')"
                + " and lcg.grpcontno in (select a.code from ldcode a,ldcode b where 1=1 and a.codetype = b.code and b.codetype = 'thridcompany' and a.codetype='"+type+"' )"
                //ldcode.othersign 作为补提标示
                + " and (lpe.confdate=current date - 1 days "
                + " or exists (select a.code from ldcode a,ldcode b where 1=1 and a.codetype = b.code and b.codetype = 'thridcompany' and a.codetype='"+type+"'  and a.code=lcg.grpcontno and a.othersign is not null and a.othersign='1' and lpe.confdate>=date(a.codealias) )"
                + " or exists (select a.code from ldcode a,ldcode b where 1=1 and a.codetype = b.code and b.codetype = 'thridcompany' and a.codetype='"+type+"'  and a.code=lcg.grpcontno and current date - 1 days=date(a.codename)) ) "//当天配置的保单提取全部历史保全数据
                + " with ur";
			
				tGrpContInfoSSRS = mExeSQL.execSQL(mGrpContInfoSQL);
				GrpContInfolen = tGrpContInfoSSRS.getMaxRow();
				
				if (GrpContInfolen < 1) {
					return true;
				}
				for (int i = 1; i <= GrpContInfolen; i++) {
					
					try {
					tPACKET = new Element("PACKET");
					Document Doc = new Document(tPACKET);
					tPACKET.addAttribute("type", "REQUEST");
					tPACKET.addAttribute("version", "1.0");
					tHEAD = new Element("HEAD");
					tHEAD.addContent(new Element("REQUEST_TYPE").setText("BQ"));
					tHEAD.addContent(new Element("TRANSACTION_NUM").setText("BQ"));
					tPACKET.addContent(tHEAD);
					tBODY = new Element("BODY");

					tGrpContInfo = new Element("GrpContInfo");
					tContInfos = new Element("ContInfos");
				
					tGrpContInfo.addContent(new Element("GrpContNo")
					        .setText(tGrpContInfoSSRS.GetText(i, 2)));
					tGrpContInfo.addContent(new Element("OperateType")
			        		.setText(tGrpContInfoSSRS.GetText(i, 3)));
					tGrpContInfo.addContent(new Element("EdorType")
	        		        .setText(tGrpContInfoSSRS.GetText(i, 4)));
					tGrpContInfo.addContent(new Element("EdorName")
	        		        .setText(tGrpContInfoSSRS.GetText(i, 5)));
			        tGrpContInfo.addContent(new Element("EdorValiDate")
			        		.setText(tGrpContInfoSSRS.GetText(i, 6)));
			        tGrpContInfo.addContent(new Element("CValiDate")
			        		.setText(tGrpContInfoSSRS.GetText(i, 7)));
			        tGrpContInfo.addContent(new Element("CInValiDate")
			        		.setText(tGrpContInfoSSRS.GetText(i, 8)));
			        tGrpContInfo.addContent(new Element("ManageCom")
			        		.setText(tGrpContInfoSSRS.GetText(i, 9)));
			        tGrpContInfo.addContent(new Element("StateFlag")
			        		.setText(tGrpContInfoSSRS.GetText(i, 10)));
			        tGrpContInfo.addContent(new Element("AppntNo")
			        		.setText(tGrpContInfoSSRS.GetText(i, 11)));
			        tGrpContInfo.addContent(new Element("GrpName")
			        		.setText(tGrpContInfoSSRS.GetText(i, 12)));
			        tGrpContInfo.addContent(new Element("PostalAddress")
			        		.setText(tGrpContInfoSSRS.GetText(i, 13)));
			        tGrpContInfo.addContent(new Element("Peoples2")
			        		.setText(tGrpContInfoSSRS.GetText(i, 14)));
			        tGrpContInfo.addContent(new Element("Name").setText(tGrpContInfoSSRS
							.GetText(i, 15)));
					tGrpContInfo.addContent(new Element("IDType").setText(tGrpContInfoSSRS
							.GetText(i, 16)));
					tGrpContInfo.addContent(new Element("IDNo").setText(tGrpContInfoSSRS
							.GetText(i, 17)));
					tGrpContInfo.addContent(new Element("IDStartDate").setText(tGrpContInfoSSRS
							.GetText(i, 18)));
					tGrpContInfo.addContent(new Element("IDEndDate").setText(tGrpContInfoSSRS
							.GetText(i, 19)));
			        tGrpContInfo.addContent(new Element("Phone").setText(tGrpContInfoSSRS
			        		.GetText(i, 20)));
			        tGrpContInfo.addContent(new Element("Fax").setText(tGrpContInfoSSRS
			        		.GetText(i, 21)));
			        tGrpContInfo.addContent(new Element("EMail").setText(tGrpContInfoSSRS
			        		.GetText(i, 22)));
			        tGrpContInfo.addContent(new Element("ClaimBankCode")
			        		.setText(tGrpContInfoSSRS.GetText(i, 23)));
			        tGrpContInfo.addContent(new Element("ClaimBankName")
			        		.setText(tGrpContInfoSSRS.GetText(i, 24)));
			        tGrpContInfo.addContent(new Element("ClaimBankAccNo")
			        		.setText(tGrpContInfoSSRS.GetText(i, 25)));
			        tGrpContInfo.addContent(new Element("ClaimAccName")
			        		.setText(tGrpContInfoSSRS.GetText(i, 26)));
			        tGrpContInfo.addContent(new Element("PayMode")
			        		.setText(tGrpContInfoSSRS.GetText(i, 27)));
			        tGrpContInfo.addContent(new Element("Prem")
			        		.setText(tGrpContInfoSSRS.GetText(i, 28)));
			        tGrpContInfo.addContent(new Element("Remark")
			        		.setText(tGrpContInfoSSRS.GetText(i, 29)));
					
					
					if( tGrpContInfoSSRS.GetText(i, 4).equals("AC") )
					{
						System.out.println("团体信息变更，个人信息不显示！！！");
					}
					else{
					//ContInfos
					if(tGrpContInfoSSRS.GetText(i, 4).equals("NI")){
				        //--被保险人信息
				        //--增人
				        mContInfosSQL = " select" 
					        	+ " lci.contno as ContNo,"   
		                        + " lci.insuredno as InsuredNo,"                                                                                                                                                                                                                                           
		                        + " lpg.edortype as EdorType,"
		                        + " (select edorname from lmedoritem where edorcode=lpg.edortype fetch first 1 rows only) as EdorName,"
		                        + " lpg.edorvalidate as EdorValiDate,"
		                        + " lci.name as InsuredName,"                                                                                                                               
		                        + " lci.sex as InsuredSex,"                                                                                                                                
		                        + " lci.Birthday as InsuredBirthday,"                                                                                                                         
		                        + " lci.IDType as InsuredIDType,"                                                                                                                             
		                        + " lci.IDNo as InsuredIDNo,"                                                                                                                                 
		                        + " lci.RelationToMainInsured as RelationToMainInsured,"                                                                                                      
		                        + " lco.cvalidate as CValiDate,"                                                                                                                        
		                        + " lco.cinvalidate as CInValiDate,"                                                                                                                       
		                        + " lco.prem as Prem,"
		                        + " lco.stateflag as StateFlag,"
		                        + " lci.bankcode as BankCode,"                                                                                                                        
		                        + " (select bankname from ldbank where bankcode=lci.BankCode fetch first 1 rows only) as BankName,"                                                    
		                        + " lci.bankAccNo as BankAccNo,"                                                                                                                       
		                        + " lci.AccName as AccName,"                                                                                                                           
		                        + " lci.Insuredstat as InsuredStat,"                                                                                                             
		                        + " lci.GrpInsuredPhone as Phone,"                                                                                                              
		                        + " lco.remark as Remark,"                                                                                                                      
		                        + " lci.contPlanCode as ContPlanCode,"                                                                                                                     
		                        + " (select contplanName from lccontplandutyparam where grpcontno=lcc.grpcontno and ContPlanCode=lci.ContPlanCode fetch first 1 rows only) as ContPlanName"
		                        + " from lpgrpedoritem lpg,lpedorapp lpp,lcgrpcont lcc,lccont lco,lcinsured lci"                                                                       
		                        + " where lpg.grpcontno=lcc.grpcontno"                                                                                                                 
		                        + " and lpg.edorno=lpp.edoracceptno"                                                                                                                
		                        + " and lpp.edorstate='0'"                                                                                                                             
		                        + " and lcc.grpcontno=lco.grpcontno"
		                        + " and lco.contno=lci.contno"
		                        + " and lcc.appflag='1'"
		                        + " and lco.appflag='1'"                                                                                                                             
		                        + " and lpg.grpcontno='" + tGrpContInfoSSRS.GetText(i, 2)+ "'"
		                        + " and lpg.edorno='" + tGrpContInfoSSRS.GetText(i, 1)+ "'"                                                                                                                                
				                + " and lpg.edortype='NI'"                                                                                                                              
		                        + " and ( exists (select 1 from ljagetendorse  where endorsementno=lpg.edorno and contno=lco.contno and feeoperationtype=lpg.edortype) "
		                        + " or exists (select 1 from lpedoritem where edorno=lpg.edorno and contno=lco.contno and edortype=lpg.edortype))"               
		                        + " union "
		                        //--连带被保人减人--  
		                        + " select "                                                                                                                                        
		                        + " lci.contno as ContNo,"   
		                        + " lci.insuredno as InsuredNo,"                                                                                                                                                                                                                                           
		                        + " lpg.edortype as EdorType,"
		                        + " (select edorname from lmedoritem where edorcode=lpg.edortype fetch first 1 rows only) as EdorName,"
		                        + " lpg.edorvalidate as EdorValiDate,"
		                        + " lci.name as InsuredName,"                                                                                                                               
		                        + " lci.sex as InsuredSex,"                                                                                                                                
		                        + " lci.Birthday as InsuredBirthday,"                                                                                                                         
		                        + " lci.IDType as InsuredIDType,"                                                                                                                             
		                        + " lci.IDNo as InsuredIDNo,"                                                                                                                                 
		                        + " lci.RelationToMainInsured as RelationToMainInsured,"                                                                                                      
		                        + " lco.cvalidate as CValiDate,"                                                                                                                        
		                        + " lco.cinvalidate as CInValiDate,"                                                                                                                       
		                        + " lco.prem as Prem,"                                                                                                                                 
		                        + " lco.stateflag as StateFlag,"                                                                                                                         
		                        + " lci.bankcode as BankCode,"                                                                                                                        
		                        + " (select bankname from ldbank where bankcode=lci.BankCode fetch first 1 rows only) as BankName,"                                                    
		                        + " lci.bankAccNo as BankAccNo,"                                                                                                                       
		                        + " lci.AccName as AccName,"                                                                                                                           
		                        + " lci.Insuredstat as InsuredStat,"                                                                                                             
		                        + " lci.GrpInsuredPhone as Phone,"                                                                                                              
		                        + " lco.remark as Remark,"                                                                                                                      
		                        + " lci.contPlanCode as ContPlanCode,"                                                                                                                     
		                        + " (select contplanName from lccontplandutyparam where grpcontno=lcc.grpcontno and ContPlanCode=lci.ContPlanCode fetch first 1 rows only) as ContPlanName"
		                        + " from lpgrpedoritem lpg,lpedorapp lpp,lcgrpcont lcc,lccont lco,lpinsured lci"                                                                       
		                        + " where lpg.grpcontno=lcc.grpcontno"                                                                                                                 
		                        + " and lpg.edorno=lpp.edoracceptno"
		                        + " and lpp.edorstate='0'"
		                        + " and lcc.grpcontno=lco.grpcontno"
		                        + " and lco.contno=lci.contno"
		                        + " and lcc.appflag='1'"
		                        + " and lco.appflag='1'"                                                                                                                             
		                        + " and lpg.grpcontno='" + tGrpContInfoSSRS.GetText(i, 2)+ "'"
		                        + " and lpg.edorno='" + tGrpContInfoSSRS.GetText(i, 1)+ "'"                                                                                                                                
		                        + " and lpg.edortype='NI'"                                                                                                                              
		                        + " and ( exists (select 1 from ljagetendorse  where endorsementno=lpg.edorno and contno=lco.contno and feeoperationtype=lpg.edortype) "
		                        + " or exists (select 1 from lpedoritem where edorno=lpg.edorno and contno=lco.contno and edortype=lpg.edortype))" 
		                        + " union "
		                      //被保人当天增人并且减人
		                        + " select " 
					        	+ " lci.contno as ContNo,"   
		                        + " lci.insuredno as InsuredNo,"                                                                                                                                                                                                                                           
		                        + " lpg.edortype as EdorType,"
		                        + " (select edorname from lmedoritem where edorcode=lpg.edortype fetch first 1 rows only) as EdorName,"
		                        + " lpg.edorvalidate as EdorValiDate,"
		                        + " lci.name as InsuredName,"                                                                                                                               
		                        + " lci.sex as InsuredSex,"                                                                                                                                
		                        + " lci.Birthday as InsuredBirthday,"                                                                                                                         
		                        + " lci.IDType as InsuredIDType,"                                                                                                                             
		                        + " lci.IDNo as InsuredIDNo,"                                                                                                                                 
		                        + " lci.RelationToMainInsured as RelationToMainInsured,"                                                                                                      
		                        + " lco.cvalidate as CValiDate,"                                                                                                                        
		                        + " lco.cinvalidate as CInValiDate,"                                                                                                                       
		                        + " lco.prem as Prem,"                                                                                                                                 
		                        + " lco.stateflag as StateFlag,"                                                                                                                         
		                        + " lci.bankcode as BankCode,"                                                                                                                        
		                        + " (select bankname from ldbank where bankcode=lci.BankCode fetch first 1 rows only) as BankName,"                                                    
		                        + " lci.bankAccNo as BankAccNo,"                                                                                                                       
		                        + " lci.AccName as AccName,"                                                                                                                           
		                        + " lci.Insuredstat as InsuredStat,"                                                                                                             
		                        + " lci.GrpInsuredPhone as Phone,"                                                                                                              
		                        + " lco.remark as Remark,"                                                                                                                      
		                        + " lci.contPlanCode as ContPlanCode,"                                                                                                                     
		                        + " (select contplanName from lccontplandutyparam where grpcontno=lcc.grpcontno and ContPlanCode=lci.ContPlanCode fetch first 1 rows only) as ContPlanName"
		                        + " from lpgrpedoritem lpg,lpedorapp lpp,lcgrpcont lcc,lbcont lco,lpinsured lci"                                                                       
		                        + " where lpg.grpcontno=lcc.grpcontno"                                                                                                                 
		                        + " and lpg.edorno=lpp.edoracceptno"                                                                                                                
		                        + " and lpp.edorstate='0'"                                                                                                                             
		                        + " and lcc.grpcontno=lco.grpcontno"
		                        + " and lco.contno=lci.contno"
		                        + " and lcc.appflag='1'"
		                        + " and lco.appflag='1'"                                                                                                                             
		                        + " and lpg.grpcontno='" + tGrpContInfoSSRS.GetText(i, 2)+ "'"
		                        + " and lpg.edorno='" + tGrpContInfoSSRS.GetText(i, 1)+ "'"                                                                                                                                
		                        + " and lpg.edortype='NI'"                                                                                                                              
		                        + " and ( exists (select 1 from ljagetendorse  where endorsementno=lpg.edorno and contno=lco.contno and feeoperationtype=lpg.edortype) "
		                        + " or exists (select 1 from lpedoritem where edorno=lpg.edorno and contno=lco.contno and edortype=lpg.edortype))" 
		                        + " with ur";
					}else if(tGrpContInfoSSRS.GetText(i, 4).equals("ZT")){
 //--连带被保人减人--  
						mContInfosSQL =" select"                                                                                                                                        
                        + " lci.contno as ContNo,"   
                        + " lci.insuredno as InsuredNo,"                                                                                                                                                                                                                                           
                        + " lpg.edortype as EdorType,"
                        + " (select edorname from lmedoritem where edorcode=lpg.edortype fetch first 1 rows only) as EdorName,"
                        + " lpg.edorvalidate as EdorValiDate,"
                        + " lci.name as InsuredName,"                                                                                                                               
                        + " lci.sex as InsuredSex,"                                                                                                                                
                        + " lci.Birthday as InsuredBirthday,"                                                                                                                         
                        + " lci.IDType as InsuredIDType,"                                                                                                                             
                        + " lci.IDNo as InsuredIDNo,"                                                                                                                                 
                        + " lci.RelationToMainInsured as RelationToMainInsured,"                                                                                                      
                        + " lco.cvalidate as CValiDate,"                                                                                                                        
                        + " (select (case when max(lpdiskimport.EdorValiDate) is null then  max(lp.EdorValiDate) else max(lpdiskimport.EdorValiDate)   end) from LPGrpEdorItem lp, lpdiskimport where lpdiskimport.EdorNo = lpg.EdorNo and lp.EdorType = lpg.EdorType and lp.EdorNo = lpdiskimport.EdorNo and lpdiskimport.insuredno = lci.insuredno) as CInValiDate,"                                                                                                                       
                        + " lco.prem as Prem,"                                                                                                                                 
                        + " lco.stateflag as StateFlag,"                                                                                                                         
                        + " lci.bankcode as BankCode,"                                                                                                                        
                        + " (select bankname from ldbank where bankcode=lci.BankCode fetch first 1 rows only) as BankName,"                                                    
                        + " lci.bankAccNo as BankAccNo,"                                                                                                                       
                        + " lci.AccName as AccName,"                                                                                                                           
                        + " lci.Insuredstat as InsuredStat,"                                                                                                             
                        + " lci.GrpInsuredPhone as Phone,"                                                                                                              
                        + " lco.remark as Remark,"                                                                                                                      
                        + " lci.contPlanCode as ContPlanCode,"                                                                                                                     
                        + " (select contplanName from lccontplandutyparam where grpcontno=lcc.grpcontno and ContPlanCode=lci.ContPlanCode fetch first 1 rows only) as ContPlanName"
                        + " from lpgrpedoritem lpg,lpedorapp lpp,lcgrpcont lcc,lccont lco,lbinsured lci"                                                                       
                        + " where lpg.grpcontno=lcc.grpcontno"                                                                                                                 
                        + " and lpg.edorno=lpp.edoracceptno"
                        + " and lpp.edorstate='0'"
                        + " and lcc.grpcontno=lco.grpcontno"
                        + " and lco.contno=lci.contno"       
                        + " and lpg.edorno=lci.edorno"       
                        + " and lcc.appflag='1'"             
                        + " and lco.appflag='1'"             
                        + " and lpg.grpcontno='"+ tGrpContInfoSSRS.GetText(i, 2)+ "'"
                        + " and lpg.edorno='"+ tGrpContInfoSSRS.GetText(i, 1)+ "'"
                        + " and lpg.edortype='ZT'"
                        + " and exists (select 1 from ljagetendorse  where endorsementno =lpg.edorno and contno=lco.contno and feeoperationtype=lpg.edortype)"
                        + " union "                       
                        //--减人                            
                        + " select"                      
                        + " lci.contno as ContNo,"   
                        + " lci.insuredno as InsuredNo,"                                                                                                                                                                                                                                           
                        + " lpg.edortype as EdorType,"
                        + " (select edorname from lmedoritem where edorcode=lpg.edortype fetch first 1 rows only) as EdorName,"
                        + " lpg.edorvalidate as EdorValiDate,"
                        + " lci.name as InsuredName,"                                                                                                                               
                        + " lci.sex as InsuredSex,"                                                                                                                                
                        + " lci.Birthday as InsuredBirthday,"                                                                                                                         
                        + " lci.IDType as InsuredIDType,"                                                                                                                             
                        + " lci.IDNo as InsuredIDNo,"                                                                                                                                 
                        + " lci.RelationToMainInsured as RelationToMainInsured,"                                                                                                      
                        + " lco.cvalidate as CValiDate,"                                                                                                                        
                        + " (select (case when max(lpdiskimport.EdorValiDate) is null then  max(lp.EdorValiDate) else max(lpdiskimport.EdorValiDate)   end) from LPGrpEdorItem lp, lpdiskimport where lpdiskimport.EdorNo = lpg.EdorNo and lp.EdorType = lpg.EdorType and lp.EdorNo = lpdiskimport.EdorNo and lpdiskimport.insuredno = lci.insuredno) as CInValiDate,"                                                                                                                       
                        + " lco.prem as Prem,"                                                                                                                                 
                        + " lco.stateflag as StateFlag,"                                                                                                                         
                        + " lci.bankcode as BankCode,"                                                                                                                        
                        + " (select bankname from ldbank where bankcode=lci.BankCode fetch first 1 rows only) as BankName,"                                                    
                        + " lci.bankAccNo as BankAccNo,"                                                                                                                       
                        + " lci.AccName as AccName,"                                                                                                                           
                        + " lci.Insuredstat as InsuredStat,"                                                                                                             
                        + " lci.GrpInsuredPhone as Phone,"                                                                                                              
                        + " lco.remark as Remark,"                                                                                                                      
                        + " lci.contPlanCode as ContPlanCode,"                                                                                                                     
                        + " (select contplanName from lccontplandutyparam where grpcontno=lcc.grpcontno and ContPlanCode=lci.ContPlanCode fetch first 1 rows only) as ContPlanName"
                        + " from lpgrpedoritem lpg,lpedorapp lpp,lcgrpcont lcc,lbcont lco,lbinsured lci"                                                                       
                        + " where lpg.grpcontno=lcc.grpcontno"
                        + " and lpg.edorno=lpp.edoracceptno"
                        + " and lpp.edorstate='0'"
                        + " and lcc.grpcontno=lco.grpcontno"
                        + " and lco.contno=lci.contno"       
                        + " and lpg.edorno=lci.edorno"       
                        + " and lcc.appflag='1'"             
                        + " and lco.appflag='1'"             
                        + " and lpg.grpcontno='"+ tGrpContInfoSSRS.GetText(i, 2)+ "'"
                        + " and lpg.edorno='"+ tGrpContInfoSSRS.GetText(i, 1)+ "'"
                        + " and lpg.edortype='ZT'"
                        + " and exists (select 1 from ljagetendorse  where endorsementno =lpg.edorno and contno=lco.contno and feeoperationtype=lpg.edortype)"                         
                        + " with ur";
					}else if(tGrpContInfoSSRS.GetText(i, 4).equals("CM")){
 
						mContInfosSQL =" select"                                                                                                                                        
                        + " lci.contno as ContNo,"   
                        + " lci.insuredno as InsuredNo,"                                                                                                                                                                                                                                           
                        + " lpg.edortype as EdorType,"
                        + " (select edorname from lmedoritem where edorcode=lpg.edortype fetch first 1 rows only) as EdorName,"
                        + " lpg.edorvalidate as EdorValiDate,"
                        + " lci.name as InsuredName,"//变更后姓名                                                                                                                               
                        + " lci.sex as InsuredSex,"                                                                                                                                
                        + " lci.Birthday as InsuredBirthday,"                                                                                                                         
                        + " lci.IDType as InsuredIDType,"//变更后证件类型                                                                                                                      
                        + " lci.IDNo as InsuredIDNo," //变更后证件号码                                                                                                                                
                        + " lci.RelationToMainInsured as RelationToMainInsured,"                                                                                                      
                        + " lco.cvalidate as CValiDate,"                                                                                                                        
//                        + " (select (case when max(lpdiskimport.EdorValiDate) is null then  max(lp.EdorValiDate) else max(lpdiskimport.EdorValiDate)   end) from LPGrpEdorItem lp, lpdiskimport where lpdiskimport.EdorNo = lpg.EdorNo and lp.EdorType = lpg.EdorType and lp.EdorNo = lpdiskimport.EdorNo and lpdiskimport.insuredno = lci.insuredno) as CInValiDate,"                                                                                                                       
						+ " lco.CInValiDate,"
						+ " lco.prem as Prem,"                                                                                                                                 
                        + " lco.stateflag as StateFlag,"                                                                                                                         
                        + " lci.bankcode as BankCode,"                                                                                                                        
                        + " (select bankname from ldbank where bankcode=lci.BankCode fetch first 1 rows only) as BankName,"                                                    
                        + " lci.bankAccNo as BankAccNo,"                                                                                                                       
                        + " lci.AccName as AccName,"                                                                                                                           
                        + " lci.Insuredstat as InsuredStat,"                                                                                                             
                        + " lci.GrpInsuredPhone as Phone,"                                                                                                              
                        + " lco.remark as Remark,"                                                                                                                      
                        + " lci.contPlanCode as ContPlanCode,"                                                                                                                     
                        + " (select contplanName from lccontplandutyparam where grpcontno=lcc.grpcontno and ContPlanCode=lci.ContPlanCode fetch first 1 rows only) as ContPlanName,"
                        + " lpi.name as InsuredName,"//变更前姓名
                        + " lpi.IDType as InsuredIDType,"//变更前证件类型
                        + " lpi.IDNo as InsuredIDNo"//变更前证件号码
                        + " from lpgrpedoritem lpg,lpedorapp lpp,lcgrpcont lcc,lccont lco,lcinsured lci,lpinsured lpi"                                                                       
                        + " where lpg.grpcontno=lcc.grpcontno"                                                                                                                 
                        + " and lpg.edorno=lpp.edoracceptno"
                        + " and lpp.edorstate='0'"
                        + " and lcc.grpcontno=lco.grpcontno"
                        + " and lco.contno=lpi.contno"       
                        + " and lpg.edorno=lpi.edorno"       
                        + " and lci.contno = lpi.contno"
                        + " and lci.insuredno = lpi.insuredno"
                        + " and lcc.appflag='1'"             
                        + " and lco.appflag='1'"             
                        + " and lpg.grpcontno='"+ tGrpContInfoSSRS.GetText(i, 2)+ "'"
                        + " and lpg.edorno='"+ tGrpContInfoSSRS.GetText(i, 1)+ "'"
                        + " and lpg.edortype='CM'"
//                        + " and exists (select 1 from ljagetendorse  where endorsementno =lpg.edorno and contno=lco.contno and feeoperationtype=lpg.edortype)"
                        + " with ur";
					}
					
			        tContInfosSSRS = mExeSQL.execSQL(mContInfosSQL);
					ContInfoslen = tContInfosSSRS.getMaxRow();
					for (int j = 1; j <= ContInfoslen; j++) {
						Element tContInfo = new Element("ContInfo");
						tContInfo.addContent(new Element("ContNo")
								.setText(tContInfosSSRS.GetText(j, 1)));
						tContInfo.addContent(new Element("InsuredNo")
								.setText(tContInfosSSRS.GetText(j, 2)));
						tContInfo.addContent(new Element("EdorType")
								.setText(tContInfosSSRS.GetText(j, 3)));
						tContInfo.addContent(new Element("EdorName")
								.setText(tContInfosSSRS.GetText(j, 4)));
						tContInfo.addContent(new Element("EdorValidate")
								.setText(tContInfosSSRS.GetText(j, 5)));
						tContInfo.addContent(new Element("InsuredName")
						        .setText(tContInfosSSRS.GetText(j, 6)));
						tContInfo.addContent(new Element("InsuredSex")
						        .setText(tContInfosSSRS.GetText(j, 7)));
						tContInfo.addContent(new Element("InsuredBirthday")
								.setText(tContInfosSSRS.GetText(j, 8)));
						tContInfo.addContent(new Element("InsuredIDType")
						        .setText(tContInfosSSRS.GetText(j, 9)));
						tContInfo.addContent(new Element("InsuredIDNo")
						        .setText(tContInfosSSRS.GetText(j, 10)));
						if("CM".equals(tContInfosSSRS.GetText(j, 3))){
							tContInfo.addContent(new Element("OldInsuredName")
					        	.setText(tContInfosSSRS.GetText(j, 25)));
							tContInfo.addContent(new Element("OldInsuredIDType")
					        	.setText(tContInfosSSRS.GetText(j, 26)));
							tContInfo.addContent(new Element("OldInsuredIDNo")
					        	.setText(tContInfosSSRS.GetText(j, 27)));
						}
						tContInfo.addContent(new Element("RelationToMainInsured")
								.setText(tContInfosSSRS.GetText(j, 11)));
						tContInfo.addContent(new Element("CValiDate")
								.setText(tContInfosSSRS.GetText(j, 12)));
						tContInfo.addContent(new Element("CInValiDate")
								.setText(tContInfosSSRS.GetText(j, 13)));
						tContInfo.addContent(new Element("Prem")
								.setText(tContInfosSSRS.GetText(j, 14)));
						tContInfo.addContent(new Element("StateFlag")
								.setText(tContInfosSSRS.GetText(j, 15)));
						tContInfo.addContent(new Element("BankCode")
								.setText(tContInfosSSRS.GetText(j, 16)));
						tContInfo.addContent(new Element("BankName")
						        .setText(tContInfosSSRS.GetText(j, 17)));
						tContInfo.addContent(new Element("BankAccNo")
								.setText(tContInfosSSRS.GetText(j, 18)));
						tContInfo.addContent(new Element("AccName")
								.setText(tContInfosSSRS.GetText(j, 19)));
						tContInfo.addContent(new Element("InsuredStat")
								.setText(tContInfosSSRS.GetText(j, 20)));
						tContInfo.addContent(new Element("Phone")
								.setText(tContInfosSSRS.GetText(j, 21)));
						tContInfo.addContent(new Element("Remark")
								.setText(tContInfosSSRS.GetText(j, 22)));
						tContInfo.addContent(new Element("ContPlanCode")
								.setText(tContInfosSSRS.GetText(j, 23)));
						tContInfo.addContent(new Element("ContPlanName")
								.setText(tContInfosSSRS.GetText(j, 24)));
					
				mPolInfosSQL = " select "
                        + " lcp.polno as polNo,"
                        + " lcp.riskcode as RisCode,"
                        + " (select riskname from lmriskapp where riskcode =lcp.riskcode ) as RiskName,"     
                        + " lcp.cvalidate as CValiDate,"
                        + " lcp.enddate as EndDate,"
                        + " lcp.amnt as Amnt,"
                        + " lcp.stateflag as StateFlag"
                        + " from lccont lco,lcpol lcp,lcinsured lci"
                        + " where lco.contno=lcp.contno"
                        + " and lcp.contno=lci.contno"
                        + " and lcp.insuredno=lci.insuredno"
                        + " and lco.appflag='1'"
                        + " and lco.grpcontno='" 
                        + tGrpContInfoSSRS.GetText(i, 2)
                        + "'"
                        + " and lcp.contno='" 
                        + tContInfosSSRS.GetText(j, 1)
                        + "'"
                        + " and lci.insuredno='" 
                        + tContInfosSSRS.GetText(j, 2)
                        + "'" 
                        + " union "
                        //--减人                                                            
                        + " select"                                                                           
                        + " lcp.polno as polNo," 
                        + " lcp.riskcode as RisCode," 
                        + " (select riskname from lmriskapp where riskcode =lcp.riskcode ) as RiskName," 
                        + " lcp.cvalidate as CValiDate,"                                                      
                        + " lcp.enddate as EndDate,"                                                          
                        + " lcp.amnt as Amnt,"  
                        + " lcp.stateflag as StateFlag"  
                        + " from lbcont lco,lbpol lcp,lbinsured lci"  
                        + " where lco.contno=lcp.contno"  
                        + " and lcp.contno=lci.contno"
                        + " and lcp.insuredno=lci.insuredno" 
                        + " and lco.appflag='1'"
                        + " and lco.grpcontno='"
                        + tGrpContInfoSSRS.GetText(i, 2)
                        + "'"                                                                                  
                        + " and lcp.contno='" 
                        + tContInfosSSRS.GetText(j, 1)
                        + "'"
                        + " and lci.insuredno='" 
                        + tContInfosSSRS.GetText(j, 2)
                        + "'"
                        + " union "
                        //--只减连带被保人                                                            
                        + " select"                                                                           
                        + " lcp.polno as polNo," 
                        + " lcp.riskcode as RisCode," 
                        + " (select riskname from lmriskapp where riskcode =lcp.riskcode ) as RiskName," 
                        + " lcp.cvalidate as CValiDate,"                                                      
                        + " lcp.enddate as EndDate,"                                                          
                        + " lcp.amnt as Amnt,"  
                        + " lcp.stateflag as StateFlag"  
                        + " from lccont lco,lbpol lcp,lbinsured lci"  
                        + " where lco.contno=lcp.contno"  
                        + " and lcp.contno=lci.contno"
                        + " and lcp.insuredno=lci.insuredno" 
                        + " and lco.appflag='1'"
                        + " and lco.grpcontno='"
                        + tGrpContInfoSSRS.GetText(i, 2)
                        + "'"                                                                                  
                        + " and lcp.contno='" 
                        + tContInfosSSRS.GetText(j, 1)
                        + "'"
                        + " and lci.insuredno='" 
                        + tContInfosSSRS.GetText(j, 2)
                        + "'"
                        + " with ur"; 
				tPolInfosSSRS = mExeSQL.execSQL(mPolInfosSQL);
				PolInfoslen = tPolInfosSSRS.getMaxRow();
				tPolInfos = new Element("PolInfos");
				for (int k = 1; k <= PolInfoslen; k++) {
					Element tPolInfo = new Element("PolInfo");
					tPolInfo.addContent(new Element("RisCode")
							.setText(tPolInfosSSRS.GetText(k, 2)));
					tPolInfo.addContent(new Element("RiskName")
							.setText(tPolInfosSSRS.GetText(k, 3)));
					tPolInfo.addContent(new Element("CValiDate")
							.setText(tPolInfosSSRS.GetText(k, 4)));
					tPolInfo.addContent(new Element("EndDate")
							.setText(tPolInfosSSRS.GetText(k, 5)));
					tPolInfo.addContent(new Element("Amnt")
							.setText(tPolInfosSSRS.GetText(k, 6)));
					tPolInfo.addContent(new Element("StateFlag")
							.setText(tPolInfosSSRS.GetText(k, 7)));
					
					mDutyInfosSQL = " select"
                        + " lcd.dutycode as DutyCode,"
                        + " (select dutyname from lmduty where dutycode=lcd.dutycode) as Dutyname,"
                        + " lcd.Amnt as Amnt,"  
                        + " lcd.getlimit as GetLimit,"
                        + " lcd.getrate as GetRate,"
                        + " ' ' as BRemark"
                        + " from lccont lco,lcpol lcp,lcinsured lci,lcduty lcd"
                        + " where lco.contno=lcp.contno"
                        + " and lcp.contno=lci.contno"
                        + " and lcp.insuredno=lci.insuredno"
                        + " and lcp.polno=lcd.polno"
                        + " and lco.appflag='1'"
                        + " and lco.grpcontno='"
                        + tGrpContInfoSSRS.GetText(i, 2)
                        + "'"                                                                           
                        + " and lcp.contno='" 
                        + tContInfosSSRS.GetText(j, 1)
                        + "'"                                                                           
                        + " and lci.insuredno='" 
                        + tContInfosSSRS.GetText(j, 2)
                        + "'"                                                                          
                        + " and lcp.polno='" 
                        + tPolInfosSSRS.GetText(k, 1)
                        + "'"                                                                          
                        + " union "                                                                  
                        //--减人
                        + " select"                                                                     
                        + " lcd.dutycode as DutyCode,"
                        + " (select dutyname from lmduty where dutycode=lcd.dutycode) as Dutyname,"
                        + " lcd.Amnt as Amnt,"  
                        + " lcd.getlimit as GetLimit,"
                        + " lcd.getrate as GetRate,"
                        + " ' ' as BRemark"                                 
                        + " from lbcont lco,lbpol lcp,lbinsured lci,lbduty lcd"
                        + " where lco.contno=lcp.contno"                       
                        + " and lcp.contno=lci.contno"                         
                        + " and lcp.insuredno=lci.insuredno"                   
                        + " and lcp.polno=lcd.polno"                           
                        + " and lco.appflag='1'"
                        + " and lco.grpcontno='" 
                        + tGrpContInfoSSRS.GetText(i, 2)
                        + "'"                                                  
                        + " and lcp.contno='" 
                        + tContInfosSSRS.GetText(j, 1)
                        + "'"                                                  
                        + " and lci.insuredno='" 
                        + tContInfosSSRS.GetText(j, 2)
                        + "'"                                                  
                        + " and lcp.polno='" 
                        + tPolInfosSSRS.GetText(k, 1)
                        + "'"                                                  
                        + " with ur";	
			
					tDutyInfosSSRS = mExeSQL.execSQL(mDutyInfosSQL);
					tDutyInfos = new Element("DutyInfos");
					DutyInfoslen = tDutyInfosSSRS.getMaxRow();
					for (int m = 1; m <= DutyInfoslen; m++) {
						Element tDutyInfo = new Element("DutyInfo");
						tDutyInfo.addContent(new Element("DutyCode")
								.setText(tDutyInfosSSRS.GetText(m, 1)));
						tDutyInfo.addContent(new Element("DutyName")
								.setText(tDutyInfosSSRS.GetText(m, 2)));
						tDutyInfo.addContent(new Element("Amnt")
								.setText(tDutyInfosSSRS.GetText(m, 3)));
						tDutyInfo.addContent(new Element("GetLimit")
								.setText(tDutyInfosSSRS.GetText(m, 4)));
						tDutyInfo.addContent(new Element("GetRate")
								.setText(tDutyInfosSSRS.GetText(m, 5)));
						tDutyInfos.addContent((Element) tDutyInfo.clone());
					}
					tPolInfo.addContent((Element) tDutyInfos.clone());
					tPolInfos.addContent((Element) tPolInfo.clone());
				}
				
				tContInfo.addContent((Element) tPolInfos.clone());
				tContInfos.addContent(tContInfo);
			}
			}//EdorType=AC时，个人信息不显示		
			tGrpContInfo.addContent(tContInfos);
			tBODY.addContent(tGrpContInfo);
			tPACKET.addContent(tBODY);		
			//机构，，团单号，工单号，，保全类型
			mFilePath = mURL +type.substring(0, 2)+"_BQ_" +tGrpContInfoSSRS.GetText(i, 9).substring(0, 4)+"_"+ tGrpContInfoSSRS.GetText(i, 2)+"_"+ tGrpContInfoSSRS.GetText(i, 1) +""+ tGrpContInfoSSRS.GetText(i, 4) +".xml";
			XMLOutputter outputter = new XMLOutputter("  ", true, "GBK");

			try {
				outputter.output(Doc, new FileOutputStream(mFilePath));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!sendXML(mFilePath,type)){
			  System.out.println("发送文件失败！工单号为："+tGrpContInfoSSRS.GetText(i, 1));
			}
//			File mfile=new File(mFilePath);
//			mfile.delete();
			} catch (Exception e) {
				System.out.println("理赔外包保全数据上报发生错误！！！"+e.getMessage());
				System.out.println("报错工单:"+tGrpContInfoSSRS.GetText(i, 1));
			}
		}
		
		return true;
	}

	private boolean sendXML(String cXmlFile,String type){
		
		String getIPPort = "select codename ,codealias,comcode from ldcode where codetype=(select codealias from ldcode where codetype='thridcompany' and code='"+type+"')||('WBClaim') and code='IP/Port'  ";
		String getUserPs = "select codename ,codealias from ldcode where codetype=(select codealias from ldcode where codetype='thridcompany' and code='"+type+"')||('WBClaim') and code='User/Pass'  ";
		String getCbBq="select codename,codealias from ldcode where codetype=(select codealias from ldcode where codetype='thridcompany' and code='"+type+"')||('WBClaim') and code='CB/BQ' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		SSRS tCBSSRS = new ExeSQL().execSQL(getCbBq);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1 || tCBSSRS.getMaxRow() < 1) {
			System.out.println("FTP服务器IP、用户名、密码、端口都不能为空，否则无法发送xml");
			return false;
		}
		
		String tIP = tIPSSRS.GetText(1, 1);
	    String tPort = tIPSSRS.GetText(1, 2);
	    String tUserName = tUPSSRS.GetText(1, 1);
	    String tPassword = tUPSSRS.GetText(1, 2);
	    String tCBBQtype = tCBSSRS.GetText(1, 1);
	    String tCBBQxml = tCBSSRS.GetText(1, 2);
	    
	    String thridcompany = tIPSSRS.GetText(1, 3);
	    String FTPUrl = "/"+thridcompany+"/"+tCBBQtype+tCBBQxml+"/"+PubFun.getCurrentDate()+"/";
		FTPTool tFTPTool = new FTPTool(tIP, tUserName,
				tPassword, Integer.parseInt(tPort));
		
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println("登录ftp服务器失败");
				return false;
			}
		} catch (SocketException ex) {
			System.out.println("无法登录ftp服务器");
			return false;
		} catch (IOException ex) {
			System.out.println("无法读取ftp服务器信息");
			return false;
		}
		try {
			tFTPTool.makeDirectory(FTPUrl);			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		if (!tFTPTool.upload(FTPUrl, cXmlFile)) {
			System.out.println("上载文件失败!");
			return false;
		}
		
		
		
		tFTPTool.logoutFTP();

		return true;
	}

	
	
	public static void main(String[] args) {
		TYLpOutSendBQSolveData tt=new TYLpOutSendBQSolveData();
		tt.run();
	}
	



}
