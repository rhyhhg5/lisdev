package com.sinosoft.lis.taskservice;

import java.io.*;
import java.net.*;
import java.util.*;
import weblogic.xml.util.StringInputStream;

public class WebServer {
    private static boolean ServerState;

    public static void test(String[] args) throws Exception {
        String requestMessageLine;
        String fileName;
        ServerSocket listenSocket = new ServerSocket(80);
        Socket connectSocket = listenSocket.accept();
        BufferedReader inFormClient = new BufferedReader(new
                InputStreamReader(connectSocket.getInputStream()));
        DataOutputStream outToClient = new
                                       DataOutputStream(connectSocket.
                getOutputStream());
        requestMessageLine = inFormClient.readLine();
        StringTokenizer tokenizedLine = new StringTokenizer(requestMessageLine);
        if (tokenizedLine.nextToken().equals("GET")) {
            fileName = tokenizedLine.nextToken();
            if (fileName.startsWith("/") == true) {
                fileName = fileName.substring(1);
            }
            String filePath = new String("D:\\ddd\\");
            File file = new File(filePath + fileName);
            int numOfBytes = (int) file.length();
            FileInputStream inFile = new FileInputStream(filePath + "1.jpg");
            byte[] fileInBytes = new byte[numOfBytes];
            inFile.read(fileInBytes);
            outToClient.writeBytes("HTTP/1.0 200 Document Follows\r\n");
            if (fileName.endsWith(".jpg")) {
                outToClient.writeBytes("Conten-Type: image/jpeg\r\n");
            } else if (fileName.endsWith(".gif")) {
                outToClient.writeBytes("Conten-Type: image/gif\r\n");
            }
            outToClient.writeBytes("Content-Length: " + numOfBytes + "\r\n");
            outToClient.writeBytes("\r\n");
            outToClient.write(fileInBytes, 0, numOfBytes);
            connectSocket.close();

        } else {
            System.out.println("No support for other request except GET!");
        }

    }

    public static void main(String[] args) throws Exception {
        WebServer cWebServer = new WebServer();
        if (!cWebServer.checkServer()) {
            cWebServer.startServer();
        }
    }


    public boolean checkServer() {
        try {
            Socket server1 = new Socket(InetAddress.getLocalHost(), 5678);
            server1.close();
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public boolean checkServer2() {
        try {
            Socket server1 = new Socket(InetAddress.getLocalHost(), 8765);
            server1.close();
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public boolean startServer() {
        System.out.println("开始启动WebServer！");
        WebServerStart cWebServerStart = new WebServerStart();
        try {
            if (!cWebServerStart.newServer()) {
                System.out.println("启动端口失败");
                return false;
            }
            Thread t1 = new Thread(cWebServerStart);
            t1.start();
            System.out.println("启动WebServer完毕！");
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public void stopServer() {
        System.out.println("开始停止WebServer！");
        WebServerStop cWebServerStop = new WebServerStop();
        try {
            Thread t1 = new Thread(cWebServerStop);
            t1.start();
            int i = 0;
            while (this.checkServer()) {
                System.out.println("checekserver");
                this.checkServer();
                i++;
                if (i > 1000) {
                    break;
                }
            }
        } catch (Exception ex) {
        }
    }
}

