package com.sinosoft.lis.taskservice;

import java.text.DecimalFormat;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl.StandSecondaryRule;
import com.sinosoft.lis.fininterface_v3.tools.sequence.PExpirBenefitPayNo;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.FIDataExtractDefDB;
import com.sinosoft.lis.db.LJSGetDrawJtDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import java.util.*;

import org.apache.xerces.validators.common.InsertableElementsInfo;

import com.sinosoft.lis.operfee.PExpirBenefitGetBL;

public class PExpirBenefitPayBL extends TaskThread{
	
	 private FIOperationLog tLogDeal = null;
	 private String mBatchNo=null;
	 private GlobalInput tGI = new GlobalInput();
	 public CErrors mErrors =null;
	 private VData tVData = null;
	 private VData mVData = null;
	 private TransferData tTransferData = new TransferData();
	 private LJSGetDrawJtSchema  tLJSGetDrawJtSchema=null;
	 private LJSGetDrawJtSet tLJSGetDrawJtSet=null;
	 private LCContSchema mLCContSchema=null;
	 private LJSGetDrawSet tLJSGetDrawSet=null;
	 private LJSGetDrawSchema tLJSGetDrawSchema=null;
	 private LJSGetDrawJtDB  tLJSGetDrawJtDB=null;
	 private MMap mmap=null;
	 PubSubmit tPubSubmit=null;
	 
	 //准备需要插入到表中的数据
	public void run(){

		PExpirBenefitGetBL PEBL=new PExpirBenefitGetBL();
		//获取当前日期
		FDate chgdate = new FDate();
		Date tdate = chgdate.getDate(PubFun.getCurrentDate());
		String today=chgdate.getString(tdate);
		//生成查询SQL
		String tsql="select distinct a.contno from lcpol a, lcget b, lmdutygetalive c where  a.conttype = '1' and a.appflag = '1' and a.polno = b.polno and b.getdutycode = c.getdutycode and c.discntflag != '9' and c.getdutykind = '0' and not exists (select 1 from ljsgetdraw where a.contno = b.contno and getdutykind = '0' and a.polno = polno) and ((a.paytodate >= a.payenddate) or (a.paytodate < a.payenddate and b.gettodate <= a.paytodate)) and a.riskcode not in ('330501', '530301') and not exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') and not exists (select 1 from ldcode1 where code = a.riskcode and codetype = 'mainsubriskrela') and b.gettodate ='"+today+"'  and b.managecom like '86%' union select distinct a.contno from lcpol a, lcget b, lmdutygetalive c where a.conttype = '1' and a.appflag = '1' and a.polno = b.polno and b.getdutycode = c.getdutycode and c.discntflag != '9' and c.getdutykind = '0' and not exists (select 1 from ljsgetdraw where a.contno = b.contno and getdutykind = '0' and a.polno = polno) and ((a.paytodate >= a.payenddate) or (a.paytodate < a.payenddate and b.gettodate <= a.paytodate)) and a.riskcode not in ('330501', '530301') and not exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') and exists (select 1 from ldcode1 where code = a.riskcode and codetype = 'mainsubriskrela') and b.gettodate ='"+today+"' and b.managecom like '86%' union select distinct a.contno from lcpol a, lcget b, lmdutygetalive c where a.conttype = '1' and a.appflag = '1' and a.polno = b.polno and b.getdutycode = c.getdutycode and c.discntflag != '9' and c.getdutykind = '12' and ((a.paytodate >= a.payenddate) or (a.paytodate < a.payenddate and b.gettodate <= a.paytodate)) and not exists (select 1 from ljsgetdraw where a.contno = b.contno and getdutykind = '0' and a.polno = polno) and a.riskcode not in ('330501', '530301') and not exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') and not exists (select 1 from ldcode1 where code = a.riskcode and codetype = 'mainsubriskrela') and b.gettodate ='"+today+"' and b.managecom like '86%' and '"+today+"' >= (case (select count(1) From ljsgetdraw b where a.contno = b.contno and b.polno = a.polno) when 0 then date('2008-1-1') else (select max(getDate) From ljsgetdraw b where a.contno = b.contno and b.polno = a.polno) end) union select distinct a.contno from lcpol a, lcget b, lmdutygetalive c  where a.conttype = '1' and a.appflag = '1' and a.polno = b.polno and b.getdutycode = c.getdutycode and c.discntflag != '9' and c.getdutykind = '36' and ((a.paytodate >= a.payenddate) or (a.paytodate < a.payenddate and b.gettodate <= a.paytodate)) and not exists (select 1 from ljsgetdraw where a.contno = b.contno  and getdutykind = '0'  and a.polno = polno) and a.riskcode not in ('330501', '530301') and not exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') and not exists (select 1  from ldcode1  where code = a.riskcode  and codetype = 'mainsubriskrela') and b.gettodate ='"+today+"' and ((b.getdutykind = '0' and b.gettodate <= b.getenddate and not exists (select 1  from ljsgetdraw where contno = b.contno and getdutycode = b.getdutycode  and insuredno = b.insuredno)) or (b.getdutykind != '0' and ((b.gettodate < b.getenddate) or ((select max(Curgettodate) from ljsgetdraw  where contno = b.contno  and getdutycode = b.getdutycode and insuredno = b.insuredno) <= b.getenddate)))) and b.managecom like '86%' and '"+today+"' >= (case (select count(1) From ljsgetdraw b where a.contno = b.contno and b.polno = a.polno) when 0 then date('2012-5-15') else (select max(getDate) From ljsgetdraw b where a.contno = b.contno and b.polno = a.polno) end) union select distinct a.contno from lcpol a, lcget b, lmdutygetalive c where a.conttype = '1' and a.appflag = '1' and a.polno = b.polno and c.discntflag = '9' and b.getdutycode = c.getdutycode and ((a.paytodate >= a.payenddate) or (a.paytodate < a.payenddate and b.gettodate <= a.paytodate)) and not exists (select 1 from ljsgetdraw where a.contno = b.contno and getdutykind = '0' and a.polno = polno) and a.riskcode not in ('330501', '530301') and not exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') and not exists (select 1 from ldcode1 where code = a.riskcode and codetype = 'mainsubriskrela') and b.gettodate ='"+today+"' and b.managecom like '86%' and '"+today+"' >= (case (select count(1) From ljsgetdraw b where a.contno = b.contno and b.polno = a.polno) when 0 then date('2008-1-1') else (select max(getDate) From ljsgetdraw b where a.contno = b.contno and b.polno = a.polno) end) fetch first 2000 rows only with ur";
		System.out.println(tsql);
		SSRS mSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		mSSRS = tExeSQL.execSQL(tsql);
		//设置全球变量
		tGI.Operator="cwjk";
		tGI.ManageCom="86";
		tGI.ManageCom="总公司";
		//循环调取保全模块接口
		for (int row = 1; row <= mSSRS.getMaxRow(); row++){
			tVData = new VData();
			mLCContSchema=new LCContSchema();
			tLJSGetDrawJtSet=new LJSGetDrawJtSet();
			tLJSGetDrawSet=new LJSGetDrawSet();
			String contno = mSSRS.GetText(row, 1);
			//获取批次
			mBatchNo = "37"+PExpirBenefitPayNo.getMBatchNo();
			System.out.println(contno);
			System.out.println(mBatchNo);
			//调用接口数据准备
			mLCContSchema.setContNo(contno);
			tVData.add(tGI);
			tVData.add(mLCContSchema);
			System.out.println("1111111");
			//此处调用的方法有问题-------------------------------------------------------------
			tLJSGetDrawSet=PEBL.getSubmitData(tVData);
			if(tLJSGetDrawSet==null){
				continue;
			}
			for(int ss=1;ss<=tLJSGetDrawSet.size();ss++){
			tLJSGetDrawSchema=new LJSGetDrawSchema();
			tLJSGetDrawJtSchema=new LJSGetDrawJtSchema();
			mmap=new MMap();
			mVData=new VData();
			tLJSGetDrawSchema.setSchema(tLJSGetDrawSet.get(ss));
			System.out.println(tLJSGetDrawSet.size());
			if(tLJSGetDrawSchema.getGetMoney()==0){
				continue;	
			}
			tLJSGetDrawJtSchema.setBatchno(mBatchNo);
			tLJSGetDrawJtSchema.setPolNo(tLJSGetDrawSchema.getPolNo());
			tLJSGetDrawJtSchema.setEnterAccDate(tLJSGetDrawSchema.getEnterAccDate());
			tLJSGetDrawJtSchema.setDutyCode(tLJSGetDrawSchema.getDutyCode());
			tLJSGetDrawJtSchema.setGetDutyKind(tLJSGetDrawSchema.getGetDutyKind());
			tLJSGetDrawJtSchema.setGetDutyCode(tLJSGetDrawSchema.getGetDutyCode());
			tLJSGetDrawJtSchema.setGrpPolNo(tLJSGetDrawSchema.getGrpPolNo());
			tLJSGetDrawJtSchema.setGrpContNo(tLJSGetDrawSchema.getGrpContNo());
			tLJSGetDrawJtSchema.setContNo(tLJSGetDrawSchema.getContNo());
			tLJSGetDrawJtSchema.setAppntNo(tLJSGetDrawSchema.getAppntNo());
			tLJSGetDrawJtSchema.setInsuredNo(tLJSGetDrawSchema.getInsuredNo());
			tLJSGetDrawJtSchema.setGetMoney(tLJSGetDrawSchema.getGetMoney());
			tLJSGetDrawJtSchema.setConfDate(tLJSGetDrawSchema.getConfDate());
			tLJSGetDrawJtSchema.setFeeOperationType(tLJSGetDrawSchema.getFeeOperationType());
			tLJSGetDrawJtSchema.setFeeFinaType(tLJSGetDrawSchema.getFeeFinaType());
			tLJSGetDrawJtSchema.setKindCode(tLJSGetDrawSchema.getKindCode());
			tLJSGetDrawJtSchema.setRiskCode(tLJSGetDrawSchema.getRiskCode());
			tLJSGetDrawJtSchema.setManageCom(tLJSGetDrawSchema.getManageCom());
			tLJSGetDrawJtSchema.setAgentCom(tLJSGetDrawSchema.getAgentCom());
			tLJSGetDrawJtSchema.setAgentType(tLJSGetDrawSchema.getAgentType());
			tLJSGetDrawJtSchema.setAgentCode(tLJSGetDrawSchema.getAgentCode());
			tLJSGetDrawJtSchema.setAgentGroup(tLJSGetDrawSchema.getAgentGroup());
			tLJSGetDrawJtSchema.setPolType(tLJSGetDrawSchema.getPolType());
			tLJSGetDrawJtSchema.setMakeDate(tLJSGetDrawSchema.getMakeDate());
			tLJSGetDrawJtSchema.setMakeTime(tLJSGetDrawSchema.getMakeTime());
			tLJSGetDrawJtSchema.setModifyDate(tLJSGetDrawSchema.getModifyDate());
			tLJSGetDrawJtSchema.setModifyTime(tLJSGetDrawSchema.getModifyTime());
			tLJSGetDrawJtSchema.setBankCode(tLJSGetDrawSchema.getBankCode());
			tLJSGetDrawJtSchema.setBankAccNo(tLJSGetDrawSchema.getBankAccNo());
			tLJSGetDrawJtSchema.setAccName(tLJSGetDrawSchema.getAccName());
			tLJSGetDrawJtSchema.setLastGettoDate(tLJSGetDrawSchema.getLastGettoDate());
			tLJSGetDrawJtSchema.setCurGetToDate(tLJSGetDrawSchema.getCurGetToDate());
			mmap.put(tLJSGetDrawJtSchema, "INSERT");
			mVData.add(mmap);
			tPubSubmit = new PubSubmit();
			try 
			{
			if(!tPubSubmit.submitData(mVData, ""))
			{
	    		buildError("PExpirBenefitPayBL","SaveData","插入数据失败");
	    		continue;
			}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("循环总次数为"+tLJSGetDrawSet.size()+"，当前循环数"+ss+"，错误保单号码为"+tLJSGetDrawJtSchema.getContNo());
				continue;	
			}
			}
		}
		
	}
	
	 private void buildError(String szModuleName, String szFunc, String szErrMsg)
	    {
	        CError cError = new CError();
	        cError.moduleName = szModuleName;
	        cError.functionName = szFunc;
	        cError.errorMessage = szErrMsg;
	        this.mErrors.addOneError(cError);
	        System.out.print(szErrMsg);
	    }

	public static void main(String[] args) {
		PExpirBenefitPayBL st= new PExpirBenefitPayBL();
		st.run();
	}
	
	
}
