package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.db.SolidificationLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.SolidificationLogSchema;
import com.sinosoft.lis.vschema.LQCLAIMREALPAYSet;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;

public class ExtractDataForLQCLAIMREALPAYTask extends TaskThread 
{
	public String mCurrentDate = PubFun.getCurrentDate();
	private SolidificationLogSchema mSolidificationLogSchema = 
			new SolidificationLogSchema();
	public void run()
	{
		System.out.println("ExtractDataForLQCLAIMREALPAYTask批处理开始执行！");
		String tSolidificationLogSQL = 
				"select * from SolidificationLog where TableName = 'LQCLAIMREALPAY' with ur";
		System.out.println("tSolidificationLogSQL:"+tSolidificationLogSQL);
		SolidificationLogDB tSolidificationLogDB = 
				new SolidificationLogDB();
		mSolidificationLogSchema = tSolidificationLogDB.executeQuery(tSolidificationLogSQL).get(1);
		String tRealEndDate = mSolidificationLogSchema.getEndDate();
		String tState = mSolidificationLogSchema.getState();
		System.out.println(tState);
		if("00".equals(tState))
		{
			System.out.println("--删除故障数据--");
			if(!deleteDirtyData(tRealEndDate))
			{
				//删除失败
				System.out.println("删除"+tRealEndDate+"天脏数据失败,中断执行");
				return;
			}
		}
		else if("11".equals(tState))
		{
			tRealEndDate = PubFun.calDate(tRealEndDate, 1, "D", "");
		}
		FDate tFDate = new FDate();
		Date tStartDate = tFDate.getDate(tRealEndDate);
		Date tEndDate = tFDate.getDate(mCurrentDate);
		while(tStartDate.compareTo(tEndDate)<0)
		{
			String tModifyDate = tFDate.getString(tStartDate);
			System.out.println("开始--提取"+tModifyDate+"的数据");
			mSolidificationLogSchema.setEndDate(tModifyDate);
			if(!insertData(tModifyDate)){
				tSolidificationLogDB.setSchema(mSolidificationLogSchema);
				tSolidificationLogDB.update();
				break;
			}
			
			mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
			mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
			mSolidificationLogSchema.setState("11");
			mSolidificationLogSchema.setExceptionInfo("处理成功");
			tSolidificationLogDB.setSchema(mSolidificationLogSchema);
			tSolidificationLogDB.update();
			System.out.println("结束---提取"+tModifyDate+"的数据");
			tStartDate = PubFun.calDate(tStartDate, 1, "D", null);
		}
				
	}
	
	/*
	 * 写插入的方法
	 * 返回boolean值
	 */
	private boolean insertData(String cModifyDate)
	{
		String tQuerySQL = "SELECT 'PC' type,c.managecom managecom,a.riskcode riskcode,a.salechnl salechnl,"
				    +" b.rgtstate rgtstate,b.endcasedate endcasedate,cast(sum(a.realpay) as "
				    +" decimal(12,2)) realpay "
				    +" from llclaimdetail a,llcase b ,lccont c "
				    +" where b.caseno=a.caseno "
				    +" and c.contno=a.contno "
				    +" and c.conttype='1' "
				    +" and c.grpcontno='00000000000000000000' "
				    +" and a.realpay<>0 "
				    +" and b.rgtstate in( '09','11','12') "
				    +" and b.endcasedate = '" + cModifyDate + "'"
				    +" group by c.managecom,a.riskcode,a.salechnl,b.endcasedate,b.rgtstate "
				    +" union all "
				    +" SELECT 'PB' type,c.managecom managecom,a.riskcode riskcode,a.salechnl salechnl, " 
				    +" b.rgtstate rgtstate,b.endcasedate endcasedate,cast(sum(a.realpay) as decimal(12,2)) realpay "
				    +" from llclaimdetail a,llcase b,lbcont c "
				    +" where b.caseno=a.caseno "
				    +" and c.contno=a.contno "
				    +" and conttype='1' "
				    +" and c.grpcontno='00000000000000000000' "
				    +" and a.realpay<>0 "
				    +" and b.rgtstate in('09','11','12') "
				    +" and b.endcasedate = '"+ cModifyDate + "'"
				    +" group by c.managecom,a.riskcode,a.salechnl,b.endcasedate,b.rgtstate "
				    +" union all "
				    +" SELECT 'GC' type,c.managecom managecom,a.riskcode riskcode,a.salechnl salechnl, "
				    +" b.rgtstate rgtstate,b.endcasedate endcasedate,cast(sum(a.realpay) as decimal(12,2)) realpay "
				    +" from llclaimdetail a,llcase b ,lcgrpcont c "
				    +" where b.caseno=a.caseno "
				    +" and c.grpcontno=a.grpcontno "
				    +" and a.realpay<>0 "
				    +" and b.rgtstate in( '09','11','12') "
				    +" and b.endcasedate = '"+ cModifyDate + "'"
				    +" group by c.managecom,a.riskcode,a.salechnl,b.endcasedate,b.rgtstate "
				    +" union all "
				    +" SELECT 'GB' type,c.managecom managecom,a.riskcode riskcode,a.salechnl salechnl, "
				    +" b.rgtstate rgtstate,b.endcasedate endcasedate,cast(sum(a.realpay) as decimal(12,2)) realpay "
				    +" from llclaimdetail a,llcase b,lbgrpcont c "
				    +" where b.caseno=a.caseno "
				    +" and c.grpcontno=a.grpcontno "
				    +" and a.realpay<>0 "
				    +" and b.rgtstate in('09','11','12') "
				    +" and b.endcasedate = '" + cModifyDate + "'"
				    +" group by c.managecom,a.riskcode,a.salechnl,b.endcasedate,b.rgtstate with ur";
		System.out.println("tQuerySQL:"+tQuerySQL);
		LQCLAIMREALPAYSet tLQCLAIMREALPAYSet = new LQCLAIMREALPAYSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLQCLAIMREALPAYSet, tQuerySQL);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		do
		  {
			tRSWrapper.getData();
			if(null==tLQCLAIMREALPAYSet||0==tLQCLAIMREALPAYSet.size())
			{
				return true;
			}
			tMMap.put(tLQCLAIMREALPAYSet, "INSERT");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if(!tPubSubmit.submitData(tVData, "")){
				System.out.println("ExtractDataForLQCLAIMREALPAYTask 批处理执行异常！");
				mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
				mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
				mSolidificationLogSchema.setState("00");
				mSolidificationLogSchema.setExceptionInfo("提取"+cModifyDate+"天数据更新时失败");
				return false;
			}
			tMMap = new MMap();
			tVData.clear();
		}
		while(tLQCLAIMREALPAYSet.size()>0);
		return true;
	}
	private boolean deleteDirtyData(String cRealEndDate)
	{
		String tDeleteDirtySql = "select * from LQCLAIMREALPAY where endcasedate = '" + cRealEndDate +"' with ur";
		RSWrapper tRSWrapper = new RSWrapper();
		LQCLAIMREALPAYSet tDeleteLQCLAIMREALPAYSet = new LQCLAIMREALPAYSet();
		tRSWrapper.prepareData(tDeleteLQCLAIMREALPAYSet, tDeleteDirtySql);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		do
		{
			tRSWrapper.getData();
			if(null==tDeleteLQCLAIMREALPAYSet||0==tDeleteLQCLAIMREALPAYSet.size())
			{
				return true;
			}
			tMMap.put(tDeleteLQCLAIMREALPAYSet, "DELETE");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if(!tPubSubmit.submitData(tVData, ""))
			{
				System.out.println("ExtractDataForLQCLAIMREALPAYTask 批处理执行异常！");
				mSolidificationLogSchema.setExceptionInfo("删除"+cRealEndDate+"天脏数据时失败");
				return false;
			}
			tMMap = new MMap();
			tVData.clear();
		}while(tDeleteLQCLAIMREALPAYSet.size()>0);
		return true;
	}
	public static void main(String[] args) 
	{
		ExtractDataForLQCLAIMREALPAYTask tExtractDataForLQCLAIMREALPAYTask = new ExtractDataForLQCLAIMREALPAYTask();
		tExtractDataForLQCLAIMREALPAYTask.run();
	}
}
