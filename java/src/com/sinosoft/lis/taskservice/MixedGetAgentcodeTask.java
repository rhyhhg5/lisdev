package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.io.*;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import java.net.SocketException;

/**
 * <p>
 * Title:MixedAgentTask.java
 * </p>
 * 
 * <p>
 * Description:集团交叉销售批处理
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company:sinosoft
 * </p>
 * 
 * @author huxl
 * @version 1.0
 */
public class MixedGetAgentcodeTask {
	public String mCurrentDate = PubFun.getCurrentDate();

	private FileWriter mFileWriter;

	private BufferedWriter mBufferedWriter;

	private String mURL = "";

	public MixedGetAgentcodeTask() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void run(String agentcode) {
		System.out.println("开始提取" + this.mCurrentDate + "日的交叉销售数据,开始时间是"
				+ PubFun.getCurrentTime());
		if (!getMixedAgentcode(agentcode)) {
			return;
		}
		System.out.println("提取" + this.mCurrentDate + "日的交叉销售数据完成,完成时间是"
				+ PubFun.getCurrentTime());
	}

	private boolean getMixedAgentcode(String agentcode) {
		getS001(agentcode);

		return true;
	}

	/**
	 * 营销员信息表
	 * 
	 * @return
	 */
	private String getS001(String agentcode) {
		String strCont = "";
		String strContent = "";
		try {
			String tSQL = "select 'P','I','000002',agentcode,'S001', agentcode, "
					+ " '"+PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+"' 子公司报送时间,'' 时间戳 "
					+ "from laagent "
					+ "where agentcode = '"
					+ agentcode
					+ "' with ur";

			SSRS tSSRS = new ExeSQL().execSQL(tSQL);
			if (tSSRS.getMaxRow() <= 0) {
				return null;
			}
			String tempString = tSSRS.encode();
			String[] tempStringArr = tempString.split("\\^");
			System.out.println(tempStringArr);

			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                String t = tempStringArr[i].replaceAll("\\|", "\\-");
				t = t.substring(0, t.length() - 1);
				strCont = t;
			}
			
			String mSQL =  "select agentcode  营销员工号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," 
				    +" 0 当事方编码,'' 当事方角色代码,name 姓名,ts_fmt(date(birthday),'yyyymmdd') 出生日期," 
				    +" CodeName('crs_cst_linker_sex',sex) 性别,managecom 销售机构代码," 
				    +" (select name from ldcom where comcode=laagent.managecom ) 销售机构名称," 
				    +" '11' 证件类型代码,'居民身份证' 证件类型名称,idno 证件号码,phone 联系电话," 
				    +" mobile 联系手机,email 电子邮件,case when agentstate<='05' then '1' else  '0' end  在职状态代码," 
				    +" case when agentstate<='05' then '在职' else  '离职' end 在职状态名称,'2'  营销员类型代码," 
				    +" '个人代理' 营销员类型名称,'I' 数据变更类型,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' 时间戳, " 
				    +" branchtype 销售渠道代码,"
				    +" case branchtype when '1' then '个人营销' when '2' then '团险营销' when '3' then '银行代理' when '4' then '电话销售' end 销售渠道名称,"
				    +" employdate 入职时间,outworkdate 离司时间,'2' 集团校验标志,''校验时间,'000085' 交叉销售权限"
				    +" from laagent " 
				    +" where agentcode = '"+agentcode+"' with ur";

		SSRS mSSRS = new ExeSQL().execSQL(mSQL);
		if (mSSRS.getMaxRow() <= 0) {
			return null;
		}
		String tempStringCont = mSSRS.encode();
		String[] tempStringContArr = tempStringCont.split("\\^");
		System.out.println(tempStringContArr);

		for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
			String m = tempStringContArr[i].replaceAll("\\|", "','");
			String[] mArr = m.split("\\,");
			mArr[3] = mArr[3].substring(1, mArr[3].length() - 1);
			System.out.println(tempStringContArr[i]);
			strContent += tempStringContArr[i];
		}
		} catch (Exception ex2) {
			System.out.println(ex2.getStackTrace());
		}
		String strManagecomCont = strCont + ":" + strContent;
		System.out.println(strManagecomCont);
		return strManagecomCont;
	}

	public static void main(String args[]) {
		MixedGetAgentcodeTask tMixedAgentTask = new MixedGetAgentcodeTask();
		tMixedAgentTask.run("1101000012");
		return;
	}

	private void jbInit() throws Exception {
	}

}
