package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.schema.LZCardSynchroSchema;
import com.sinosoft.lis.schema.LZCertifyErrorLogSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.lis.vschema.LZCardSynchroSet;
import com.sinosoft.lis.vschema.LZCertifyErrorLogSet;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LZCardSynchroTask extends TaskThread {

	CErrors mErrors = new CErrors();
	private VData tVData = new VData();

	PubSubmit tPubSubmit = new PubSubmit();

	public LZCardSynchroTask() {
	}

	/**
	 * 实现TaskThread的借口方法run
	 * 由TaskThread调用
	 * */
	public void run() {
		System.out.println("开始向LZCardSynchro同步单证");

		if (!LZCardSynchro()) {
			System.out.println("向LZCardSynchro同步单证失败！");
			return;
		}
		System.out.println("向LZCardSynchro同步单证完成");
	}

	public boolean LZCardSynchro() //核销操作
	{
		String tSQL = "select lzcn.cardno CardNo,lzcn.CardPassword2 CardPassword2,lzcn.CardPassword CardPassword,"
					+ "lzc.certifycode CertifyCode,"
					+ "(select riskcode from lmcardrisk where certifycode = lzc.certifycode and risktype = 'W') RiskWrapCode," 
					+ "lzc.state State,'001' Operator,"
					+ "current date MakeDate,current time MakeTime,current date ModifyDate,current time ModifyTime "
					+ "from lzcardnumber lzcn inner join lzcard lzc " 
					+ "on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
					+ "where lzc.state in ('10','11') "
					+ "and not exists (select 1 from licardactiveinfolist where cardno = lzcn.cardno) "
					+ "and lzc.receivecom in (select code from ldcode where codetype = 'lzcardsynchro' ) "
					+ "and not exists (select 1 from LZCardSynchro where cardno = lzcn.cardno) ";
		LZCardSynchroSet tLZCardSynchroSet = new LZCardSynchroSet();
		RSWrapper rswrapper = new RSWrapper();
		rswrapper.prepareData(tLZCardSynchroSet, tSQL);
		try
        {
            do
            {
                rswrapper.getData();
                MMap tMap = new MMap();
                tMap.put(tLZCardSynchroSet, SysConst.INSERT);
                tVData.clear();
                tVData.add(tMap);
                tPubSubmit.submitData(tVData, "");
                
            }
            while (tLZCardSynchroSet.size() > 0);
            rswrapper.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            rswrapper.close();
        }
		return true;
	}

	public static void main(String args[]) {
		LZCardSynchroTask tAuditCancel = new LZCardSynchroTask();
		tAuditCancel.run();
	}

}
