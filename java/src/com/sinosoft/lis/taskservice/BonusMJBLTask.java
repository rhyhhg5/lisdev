package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.BonusGetBL;
import com.sinosoft.lis.bq.BonusMJBL;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LOBonusAssignErrLogSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 分红险满期批处理 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: sinosoft</p>
 * @author 
 * @version 1.0
 * @CreateDate：2016-08-22
 */

public class BonusMJBLTask extends TaskThread {
	
    public CErrors mErrors = new CErrors();

    private String mContNo = null;
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    
    private GlobalInput mGI = new GlobalInput();
    
    private ExeSQL mExeSQL=new ExeSQL();
    public BonusMJBLTask()
    {
        mGI.Operator = "001";
        mGI.ComCode = "86";
        mGI.ManageCom = mGI.ComCode;
    }

    public BonusMJBLTask(GlobalInput cGlobalInput)
    {
        mGI = cGlobalInput;
    }

    /**
     * 结算指定保单
     * @param cContNo String
     */
    public void runOneCont(String cContNo)
    {
        mContNo = cContNo;
        run();
    }
    
    public void run()
    {
    	int iSuccCount = 0;
        int iFailCount = 0;

        //查询出当前账户最后结算日期比已公布利率日期晚的账户，由于理赔或保全等原因，一个账户可能需要结算多次
        String sql = " select distinct a.contno from LCPol a "
        			 + " where a.conttype='1' "
                     + " and a.appflag='1' "
                     + " and a.stateflag in ( '1','3')"
                     + " and a.riskcode in (select riskcode from lmriskapp where risktype4='2') " //分红险
                     + " and a.ManageCom like '" + mGI.ManageCom + "%' "
                     + "  and (a.polstate not like '%03%' or a.polstate is null or a.polstate='') "
                     + " and a.payenddate<=a.paytodate"
                     + " and a.enddate<='" + mCurrentDate + "' "
                     + (mContNo != null ? (" and a.ContNo = '" + mContNo + "' ") : "")
                     + " order by a.ContNo with ur";
        System.out.println(sql);

        RSWrapper rsWrapper = new RSWrapper();
        if (!rsWrapper.prepareData(null, sql))
        {
            System.out.println("分红险保单满期数据准备失败! ");
            return;
        }
        SSRS tSSRS = rsWrapper.getSSRS();
    	if (tSSRS.getMaxRow() > 0)
        {	
	    	for (int i = 1 ; i<= tSSRS.getMaxRow(); i++)
	    	{	
	    		BonusMJBL tBonusMJBL=new BonusMJBL();
	    		String ContNo=tSSRS.GetText(i, 1);
	            String sql2="select polno,cvalidate from lcpol where contno='"+ContNo+"'  and riskcode in (select riskcode from lmriskapp where risktype4='2') with ur";
	            SSRS tSSRS2 = new ExeSQL().execSQL(sql2);
	            if(tSSRS2.getMaxRow()>0){
	            	String PolNo=tSSRS2.GetText(1, 1);
            		String Cvalidate=tSSRS2.GetText(1, 2);
            		VData tVData = new VData();
            		tVData.add(mGI);
            		tVData.add(PolNo);
            		tVData.add(Cvalidate);
            		
                    String errInfo = "险种号:" + tSSRS.GetText(i, 1)+ " 满期结算失败: ";
                    try
                    { 
            		if(!tBonusMJBL.submitData(tVData, "")){
            			iFailCount++;
                        errInfo += tBonusMJBL.mErrors.getErrContent();
                        mErrors.addOneError(errInfo);
                        System.out.println(errInfo);
                        submiterror(errInfo,PolNo);
            		}else{
            			iSuccCount++;
            		}
	              } catch(Exception ex)
                    {
                    	errInfo += "结算出现未知异常";
                    	System.out.println(errInfo);
                    	ex.printStackTrace();
                    	mErrors.addOneError(errInfo);
                    	submiterror(errInfo,PolNo);
                  	  	iFailCount++;
                    }
                }       	
	    	}
        }
    	rsWrapper.close();

        System.out.println("共计" + iSuccCount + "个保单结算成功，"
                + iFailCount + "个保单满期结算失败:"
                + mErrors.getErrContent());
    }
    
    public void submiterror(String errInfo,String PolNo){
    	errInfo= "满期计算失败";
        LOBonusAssignErrLogSchema tLOBonusAssignErrLogSchema =new LOBonusAssignErrLogSchema();
        tLOBonusAssignErrLogSchema.setPolNo(PolNo);
        tLOBonusAssignErrLogSchema.setSerialNo(PubFun1.CreateMaxNo("SERIALNO",mGI.ManageCom));
        tLOBonusAssignErrLogSchema.seterrMsg(errInfo);
        tLOBonusAssignErrLogSchema.setmakedate(mCurrentDate);
        tLOBonusAssignErrLogSchema.setmaketime(mCurrentTime);
        MMap tMMap=new MMap();
        tMMap.put(tLOBonusAssignErrLogSchema, SysConst.INSERT);
        VData data = new VData();
        data.add(tMMap);
        PubSubmit tSubmit = new PubSubmit();
        try{
        	tSubmit.submitData(data, "");
        }catch(Exception e){
        }
    }
    
    public static void main(String[] args) {
    	BonusMJBLTask tBonusMJBLTask =new  BonusMJBLTask();
//    	tBonusMJBLTask.run();
    	tBonusMJBLTask.runOneCont("016107884000001");
    	
	}
    
}
