package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.SendMsgMail;
import com.sinosoft.lis.taskservice.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun1;
import java.io.IOException;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 定时发送短信，并同时将所发短信保存
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author HM
 * @version 1.0
 */
public class HMMsgTask extends TaskThread {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData mInputData = new VData();

    // 统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();
    private MMap map = new MMap();
    public HMMsgTask() {
    }
    public static String TaskCode = "";

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run() {
        if (!Msg()) {
            return;
        }
    }

    //执行存档操作
    private boolean Msg() {
        SSRS tSSRS = getMsgTask();
        SSRS tSSRS_Receive = null; //得到信息接收者信息
        int mRow = tSSRS.getMaxRow();
        int mCol = tSSRS.getMaxCol();
        String[][] msgInfo = tSSRS.getAllData();

        //发送短消息
        if (tSSRS != null && mRow > 0) {
            for (int msgNum = 0; msgNum < mRow; msgNum++) {
                tSSRS_Receive = getMsgReceiveTask(msgInfo[msgNum][0]);
                String[][] msgReceive = tSSRS_Receive.getAllData();
                String mobile = "";
                int mRow_rece = tSSRS_Receive.getMaxRow();
                for (int c_rece = 0; c_rece < mRow_rece; c_rece++) {
                    if (c_rece != (mRow_rece - 1)) {
                        mobile = mobile + msgReceive[c_rece][6] + ",";
                    } else {
                        mobile = mobile + msgReceive[c_rece][6];
                    }
                }

                try {
                         TransferData PrintElement = new TransferData();
                         PrintElement.setNameAndValue("mobile", mobile);
                         PrintElement.setNameAndValue("content", msgInfo[msgNum][6]);
                         PrintElement.setNameAndValue("strFrom", "service@picchealth.com");
                         PrintElement.setNameAndValue("strTo", "zhangjun@sinosoft.com.cn");
                         PrintElement.setNameAndValue("strTitle", "无");
                         PrintElement.setNameAndValue("strPassword", "");

                         GlobalInput tGlobalInput = new GlobalInput();
                         tGlobalInput.ComCode = "86";
                         tGlobalInput.ManageCom = "86";
                         tGlobalInput.Operator = "lands";

                         VData aVData = new VData();
                         aVData.add(tGlobalInput);
                         aVData.add(PrintElement);

                         SendMsgMail tSendMsgMail = new SendMsgMail();


                    if (! tSendMsgMail.submitData(aVData, "Message"))
                    {

                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                    mErrors.addOneError("短信发送失败，短信内容："+msgInfo[msgNum][7]);
                    System.out.println("短信发送失败");
                }
            }


        //将所要发送信息在信息表新增一条记录


            for (int i = 0; i < mRow; i++) {
                String temp_MsgNo = PubFun1.CreateMaxNo("HMMSG", 12);
                String sql = " INSERT INTO LOMSGINFO ( MsgNo, Department, MsgType, SendEmail, MsgTopic, MsgContent, MsgSend, ManageCom, Operator, MakeDate, MakeTime, ModifyDate, ModifyTime  ) "
                             + " VALUES ('" + temp_MsgNo +
                             "', 'HM', '1', '"
                             + msgInfo[i][3] + "', '" + msgInfo[i][5] + "', '" +
                             msgInfo[i][6] + "', '"
                             + "1" + "', '" + msgInfo[i][13] + "','" +
                             msgInfo[i][14] + "','"
                             + mCurrentDate + "','" + mCurrentTime + "','"
                             + mCurrentDate + "','" + mCurrentTime + "')"
                             ;
                map.put(sql, "INSERT"); //修改

                tSSRS_Receive = getMsgReceiveTask(msgInfo[i][0]);
                String[][] msgReceive = tSSRS_Receive.getAllData();
                int mRow_rece = tSSRS_Receive.getMaxRow();
                for (int c_rece = 0; c_rece < mRow_rece; c_rece++) {
                    String sql_rece = " INSERT INTO LOMSGRECEIVE ( MsgNo, Seqno, Customerno, Mobileno, ManageCom, Operator, MakeDate, MakeTime, ModifyDate, ModifyTime  ) "
                                      + " VALUES ( '" + temp_MsgNo + "', '" +
                                      PubFun1.CreateMaxNo("MSGSEQ", 12) +
                                      "', '" + msgReceive[c_rece][2] + "', '" +
                                      msgReceive[c_rece][6]
                                      + "', '" + msgReceive[c_rece][11] +
                                      "', '" + msgReceive[c_rece][12] + "','"
                                      + mCurrentDate + "','" + mCurrentTime +
                                      "','"
                                      + mCurrentDate + "','" + mCurrentTime +
                                      "')"
                                      ;
                    map.put(sql_rece, "INSERT");
                }
            }


        //提交数据
        if (map.size() > 0) {
            mInputData.add(map);

            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN")) {
                mErrors.copyAllErrors(tPubSubmit.mErrors);
                mErrors.addOneError("已发短信存储失败");
                System.out.println("已发短信存储失败");

                return false;
            }

            return true;
        }

        return true;
    }
    else{return false;}
    }

    //得到需要发送的短信信息
    private SSRS getMsgTask() {
        SSRS tSSRS = null;
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("------TaskCode: "+Task.TaskCode+"--------");
        if(!Task.TaskCode.equals(""))
        {
            SSRS tTask = null;
            String task_sql = " select * from LOMsgInfo where password ='"+Task.TaskCode+"'";
            tTask = tExeSQL.execSQL(task_sql);
            if(tTask != null)
            {
                String sql = " select * from LOMsgInfo where password ='"+Task.TaskCode+"' ";
                tSSRS = tExeSQL.execSQL(sql);
            }
        }

        return tSSRS;
    }

    //得到需要发送短信的收信人信息
    private SSRS getMsgReceiveTask(String MsgNo) {
        SSRS tSSRS = null;
        ExeSQL tExeSQL = new ExeSQL();
        String sql = " select * from LOMsgReceive where MsgNo = '" + MsgNo +
                     "'  ";
        tSSRS = tExeSQL.execSQL(sql);
        return tSSRS;
    }


//短信发送借口



    public static void main(String args[]) {
//        Task.TaskCode = "000083";
        if(!Task.TaskCode.equals(""))
        {
            System.out.println(Task.TaskCode);
            HMMsgTask a = new HMMsgTask();
            a.run();
            if (a.mErrors.needDealError()) {
                System.out.println(a.mErrors.getErrContent());
            } else {
                System.out.println("OK");
            }
        }
    }
}
