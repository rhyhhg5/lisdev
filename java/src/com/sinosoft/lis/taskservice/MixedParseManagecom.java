package com.sinosoft.lis.taskservice;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.File.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class MixedParseManagecom extends TaskThread {
    private String interFilePath = "";
    private String mLogDate="";
    private String mToday = PubFun.getCurrentDate();
    private String mTime = PubFun.getCurrentTime();
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    private String CrmPath = "";
    private String mFileName="";
    private ExeSQL mExeSQL = new ExeSQL();
    private String mBatchNo = PubFun1.CreateMaxNo("CRMBATCHNO", 20);

    public MixedParseManagecom() {
    }
    public void run(String cMsgInfo) {
       try{
    	   if (!parseMsg(cMsgInfo)){
    		   return;
    	   }
      }catch(Exception ex){
    	  System.out.println("readTextDate END With Exception...");
    	  ex.printStackTrace();
    	  return;
      }
    } 
    

    private boolean parseMsg(String cMsgInfo) throws java.io.FileNotFoundException,
            java.io.IOException {
    	LOMixComSchema tCrsChkMsg = new LOMixComSchema();

        String tPatMsgHeadFormat = "[A-Z]-[A-Z]-\\d+-[A-Za-z]\\d+-\\d+-\\d{14}:";
        String tPatMsgContextFormat = ".*(\\|(.*))+";

        StringBuffer tPatMsgFormat = new StringBuffer();
        tPatMsgFormat.append("^");
        tPatMsgFormat.append(tPatMsgHeadFormat + tPatMsgContextFormat);
        tPatMsgFormat.append("$");

        Pattern pattern = Pattern.compile(tPatMsgFormat.toString());

        Matcher tMatcher = null;

        // 校验消息
        tMatcher = pattern.matcher(cMsgInfo);
        if (!tMatcher.matches())
        {
        	CError tError = new CError();
            tError.moduleName = "groupContBL";
            tError.functionName = "createAddressNo";
            tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!";
            this.mErrors.addOneError(tError);
        }
        // --------------------

        // 拆分消息
        int idx = cMsgInfo.indexOf(":");
        if (idx == -1)
            return false;
        String tContext = cMsgInfo.substring(idx + 1, cMsgInfo.length());
        String[] tSubContext = tContext.split("\\|");

        tCrsChkMsg.setGrpAgentCom(tSubContext[0]);          //管理机构

        tCrsChkMsg.setComp_Cod(tSubContext[1]);             //子公司代码 
        tCrsChkMsg.setComp_Nam(tSubContext[2]);             //子公司名称
        
        tCrsChkMsg.setProv_Orgcod(tSubContext[5]);          //省级分公司代码
        tCrsChkMsg.setProv_Orgname(tSubContext[6]);         //省级分公司名称
        tCrsChkMsg.setCity_Orgcod(tSubContext[7]);          //地市级分公司代码
        tCrsChkMsg.setCity_Orgname(tSubContext[8]);         //地市级分公司名称
        tCrsChkMsg.setTown_Orgcod(tSubContext[9]);          //县支级分公司代码
        tCrsChkMsg.setTown_Orgname(tSubContext[10]);        //县支级分公司名称
        tCrsChkMsg.setUnder_Orgcod(tSubContext[11]);        //代理机构代码
        tCrsChkMsg.setUnder_Orgname(tSubContext[12]);       //代理机构名称
        tCrsChkMsg.setOrg_Lvl(tSubContext[13]);             //机构等级
        tCrsChkMsg.setStart_Dat(tSubContext[14]);           //起始日期
        tCrsChkMsg.setEnd_Dat("");                          //终止日期
        tCrsChkMsg.setStatus_Cod(tSubContext[16]);          //状态
        tCrsChkMsg.setStatus_Nam(tSubContext[17]);          //状态说明
        tCrsChkMsg.setDate_Send(tSubContext[25]);           //子公司报送时间
        tCrsChkMsg.setStandByFlag1(tSubContext[27]);        //第五级机构代码
        tCrsChkMsg.setStandByFlag2(tSubContext[28]);        //第五级机构名称
        

        tCrsChkMsg.setMakeDate(PubFun.getCurrentDate());
        tCrsChkMsg.setMakeTime(PubFun.getCurrentTime());
        tCrsChkMsg.setModifyDate(PubFun.getCurrentDate());
        tCrsChkMsg.setModifyTime(PubFun.getCurrentTime());
        
        mMap = new MMap();
        mMap.put(tCrsChkMsg, "DELETE&INSERT");
        if (!SubmitMap())
        {
        	return false;
        }	
        return true;
    }


    /*数据处理*/
	private boolean SubmitMap()
	{
	    PubSubmit tPubSubmit = new PubSubmit();
	    mResult= new VData();
	    mResult.add(mMap);
	    if (!tPubSubmit.submitData(mResult, ""))
	    {
	        System.out.println("提交数据失败！");
	        return false;
	    }
	    return true;
	}

	private void buildError(String szFunc, String szErrMsg)
	{
	    CError cError = new CError();
	    cError.moduleName = "GroupContBL";
	    cError.functionName = szFunc;
	    cError.errorMessage = szErrMsg;
	    this.mErrors.addOneError(cError);
	    System.out.println("程序报错：" + cError.errorMessage);
	}


	public static void main(String[] args) throws Exception
	{
	        MixedParseManagecom mMixedComTask = new MixedParseManagecom();
	        mMixedComTask.run("R-I-000002-11000000-D001-11010000-20100527123430: 1940306|000100|中国人保寿险有限公司|1|东部|1940000|深圳市分公司|1940300|深圳分公司本部|1940306|深圳宝安区支公司|1940306|深圳宝安区支公司|3|20070930||1|正常|440300|深圳市|440300|深圳市|||I|20091208 04:12:42|20091208 04:12:42|11010102|机构名称");
	        return;
	
	}
}