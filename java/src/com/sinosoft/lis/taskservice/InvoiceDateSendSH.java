package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.ygz.OutPayUploadBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class InvoiceDateSendSH extends TaskThread{
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public InvoiceDateSendSH(){}
    
    private SSRS mSSRS;
    
    private LYOutPayDetailSet mLYOutPayDetailSet = null;
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap = new MMap();
    private VData mVData = new VData();
    
    private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	private int sysCount = 5000;//默认每一千条数据发送一次请求

    public void run() {
        System.out.println("---InvoiceDateSend开始---");
        submitData();
        System.out.println("---InvoiceDateSend正常结束---");
    }
    
    private boolean submitData(){
    	
    	mSSRS = getData();
    	
    	if(mSSRS==null){
    		buildError("getData","没有获得需要推送的数据");
    		return false;
    	}
    	
    	long startDealData = System.currentTimeMillis();
    	if(!dealData()){
    		buildError("dealData","数据准备失败");
    		return false;
    	}
    	long endDealData = System.currentTimeMillis();
    	System.out.println("========== InvoiceDateSend 数据准备dealData -耗时： "+((endDealData - startDealData)/1000/60)+"分钟");

    	long startCallService = System.currentTimeMillis();
    	if(!callService()){
    		return false;
    	}
    	long endCallService = System.currentTimeMillis();
    	System.out.println("========== InvoiceDateSend callService调用发票接口  -耗时： "+((endCallService -startCallService)/1000/60)+"分钟");

    	if(!submit()){
    		return false;
    	}
    	
        return true;
    }
    
    private boolean callService(){
    	
    	OutPayUploadBL tOutPayUploadBL = new OutPayUploadBL();
    	VData tVData = new VData();
    	tVData.add(mLYOutPayDetailSet);
    	tVData.add(mGlobalInput);
    	if(!tOutPayUploadBL.getSubmit(tVData, "")){
    		buildError("callService", "调用OutPayUploadBL接口失败。");
    		return false;
    	}
    	mMap = new MMap();
    	mMap = tOutPayUploadBL.getResult();
    	    	
    	return true;
    }
    
    private boolean submit(){
    	
    	mVData.add(mMap);
    	PubSubmit p = new PubSubmit();
    	if (!p.submitData(mVData, SysConst.INSERT)){
    		System.out.println("提交数据失败");
    		buildError("submitData", "提交数据失败");
    		return false;
    	}

        return true;
    	
    }
    
    private boolean dealData(){
    	int count=0;
    	mLYOutPayDetailSet = new LYOutPayDetailSet();
    	for(int i=1; i<=mSSRS.getMaxRow(); i++){
    		
    		LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
    		
    		// 是否更新客户信息
    		tLYOutPayDetailSchema.setisupdate("Y");
    		// 数据日期 
    		tLYOutPayDetailSchema.setvdata(mCurrentDate);
    		// 集团  传"00"，,00是集团编码
    		tLYOutPayDetailSchema.setpkgroup("00");
    		tLYOutPayDetailSchema.setgroup("00");
    		// 组织
    		tLYOutPayDetailSchema.setpkorg(mSSRS.GetText(i, 10));
    		tLYOutPayDetailSchema.setorg(mSSRS.GetText(i, 10));
    		tLYOutPayDetailSchema.setorgv(mSSRS.GetText(i, 10));
    		// 税率 
    		tLYOutPayDetailSchema.settaxrate(mSSRS.GetText(i, 1));
    		// 不含税金额（原币） 
    		tLYOutPayDetailSchema.setoriamt(mSSRS.GetText(i, 2));
    		// 税额（原币） 
    		tLYOutPayDetailSchema.setoritax(mSSRS.GetText(i, 3));
    		// 不含税金额（本币） 
    		tLYOutPayDetailSchema.setlocalamt(mSSRS.GetText(i, 2));
    		// 税额（本币）
    		tLYOutPayDetailSchema.setlocaltax(mSSRS.GetText(i, 3));
    		// 客户号
    		tLYOutPayDetailSchema.setcustcode(mSSRS.GetText(i, 4));
    		tLYOutPayDetailSchema.setcode(mSSRS.GetText(i, 4));
    		// 客户的名称 
    		tLYOutPayDetailSchema.setcustname(mSSRS.GetText(i, 14));
    		tLYOutPayDetailSchema.setname(mSSRS.GetText(i, 14));
    		// 客户的类型 团体客户为"1"
    		// 需求调整为 客户类型统一为"2"
    		tLYOutPayDetailSchema.setcusttype("2");
    		tLYOutPayDetailSchema.setcustomertype("2");
    		// 价税分离项目  传"01"用来匹配商品服务档案
    		tLYOutPayDetailSchema.setptsitem("01");
    		// 申报类型  “0”表示正常申报
    		tLYOutPayDetailSchema.setdectype("0");
    		// 来源的系统  传“01”，代表核心系统
			tLYOutPayDetailSchema.setsrcsystem("01");
			// 金额是否含税  “0”表示含税，“1”表示不含税
			tLYOutPayDetailSchema.settaxtype("0");
			// 收付标志  “0”表示收入，“1”表示支出
			tLYOutPayDetailSchema.setpaymentflag("0");
			// 境内外标示  “0”表示境内，“1”表示境外
			tLYOutPayDetailSchema.setoverseasflag("0");
			// 是否开票   传“Y/N”，必须传
			tLYOutPayDetailSchema.setisbill("Y");
			// 汇总地类型  “0”表示汇总，“1”表示属地，后续纳税
			tLYOutPayDetailSchema.setareatype("0");
    		// 凭证id -- 保单号
    		tLYOutPayDetailSchema.setvoucherid(mSSRS.GetText(i, 5));
    		// 交易流水号 
    		tLYOutPayDetailSchema.settranserial(mSSRS.GetText(i, 6));
    		// 交易日期--确认核销日期:取保单的生效与签单日期孰后日期
    		tLYOutPayDetailSchema.settrandate(mSSRS.GetText(i, 7));
    		// 产品代码--险种编码前四位
    		tLYOutPayDetailSchema.setprocode(mSSRS.GetText(i, 8));
    		// 交易币种
    		tLYOutPayDetailSchema.settrancurrency("CNY");
    		// 交易金额 
    		tLYOutPayDetailSchema.settranamt(mSSRS.GetText(i, 9));
    		// 备注 -- 保单号+险种号
    		tLYOutPayDetailSchema.setvdef1("保单号:"+mSSRS.GetText(i, 5)+";险种:"+mSSRS.GetText(i, 15));
    		// vdef2 -- 保单号
    		tLYOutPayDetailSchema.setvdef2(mSSRS.GetText(i, 5));
    		// 部门 -- 管理机构
    		tLYOutPayDetailSchema.setdeptdoc(mSSRS.GetText(i, 10));
    		// 主键 --流水号
    		tLYOutPayDetailSchema.setBusiNo(mSSRS.GetText(i, 6));
    		// 业务项目编码--价税分离表的流水号
    		tLYOutPayDetailSchema.setbusipk(mSSRS.GetText(i, 6));
    		// 保单生效日期
    		tLYOutPayDetailSchema.setbilleffectivedate(mSSRS.GetText(i, 11));
    		// 投保单号
    		tLYOutPayDetailSchema.setinsureno(mSSRS.GetText(i, 12));
    		// 预打发票标识 : 01--预打 02--非预打
    		tLYOutPayDetailSchema.setprebilltype("02");
    		// 收付类型 ： 01--收费  02--付费
    		tLYOutPayDetailSchema.setmoneytype("01");
    		// 收付费号  
    		tLYOutPayDetailSchema.setmoneyno(mSSRS.GetText(i, 13));
    		// 业务类型代码  01--保费收入
    		tLYOutPayDetailSchema.setbusitype("01");
    		// makedate,maketime,modifydate,modifytime
    		tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
			tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
			tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
			tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
			//业务员编码
			tLYOutPayDetailSchema.setvdef8(mSSRS.GetText(i, 16));
			// 邮件说明要新添字段
			tLYOutPayDetailSchema.setinvtype("0"); // 默认为0
			tLYOutPayDetailSchema.setvaddress("0"); // 默认为0
			tLYOutPayDetailSchema.setvsrcsystem("01"); // 默认为01
			tLYOutPayDetailSchema.settaxpayertype("01"); // 默认为01
			tLYOutPayDetailSchema.setenablestate("2"); // 默认为2
    	
			mLYOutPayDetailSet.add(tLYOutPayDetailSchema);
			count ++;
			if(count >= sysCount){
				count = 0;
				OutPayUploadBL tOutPayUploadBL = new OutPayUploadBL();
		    	VData tVData = new VData();
		    	tVData.add(mLYOutPayDetailSet);
		    	tVData.add(mGlobalInput);
		    	if(!tOutPayUploadBL.getSubmit(tVData, "")){
		    		buildError("callService", "调用OutPayUploadBL接口失败。");
		    		return false;
		    	}
		    	mMap = tOutPayUploadBL.getResult();
		    	mVData.add(mMap);
		    	PubSubmit p = new PubSubmit();
		    	if (!p.submitData(mVData, SysConst.INSERT)){
		    		System.out.println("提交数据失败");
		    		buildError("submitData", "提交数据失败");
		    		return false;
		    	}
				mMap = new MMap();
		    	mVData.clear();
				mLYOutPayDetailSet=new LYOutPayDetailSet();
			}
    	}
    	
    	return true;
    }
    
    /** 获取需要推送的签单完成的团单 */
    private SSRS getData(){
    	
    	mGlobalInput.ComCode = "86";
    	mGlobalInput.Operator = "001";
    	
    	long startSSRSgetData = System.currentTimeMillis();
    	SSRS tSSRS = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
    	String tSQL = "select codename from ldcode where codetype='shgdfptstime'";
    	tSSRS = tExeSQL.execSQL(tSQL);
    	String time=null;
    	if(tSSRS.getMaxRow()>0){
    		time="and lcg.signdate between "+tSSRS.GetText(1, 1)+" ";
    		System.out.println(time);
    	}
    	String SQL = "select lyp.taxrate ,lyp.moneynotax, lyp.moneytax, lcg.appntno, lcg.contno, lyp.busino "
    			   + ",(case when lcg.cvalidate>=lcg.signdate then lcg.cvalidate else lcg.signdate end) "
    			   + ",substr(lyp.riskcode,1,4),lyp.paymoney,lcg.managecom,lcg.cvalidate,lcg.prtno "
    			   + ",lyp.moneyno,lcg.appntname "
    			   + ",(select riskname from lmriskapp where riskcode=lyp.riskcode) "
    			   + ",(select groupagentcode from laagent where agentcode=lcg.agentcode) "
    			   + "from lccont lcg "
    			   + "inner join lypremseparatedetail lyp "
    			   + "on lcg.prtno = lyp.prtno "
    			   + "where lcg.appflag='1' "
    			   + "and lcg.conttype='1' "
    			   + time
    			   + "and lcg.managecom like '8631%' "
    			   + "and lyp.otherstate not in ('03','04','05') "
    			   + "and lyp.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
    			   + "and not exists (select 1 from LYOutPayDetail where busino=lyp.busino and state != '03') "
    			   + "and not exists (select 1 from LYOutPayDetail where busino in (select busino from lypremseparatedetail where prtno = lcg.prtno and otherstate = '03') ) "
    			   + "and lyp.state='02' ";
    	tSSRS = tExeSQL.execSQL(SQL);
    	long endSSRSgetData = System.currentTimeMillis();
    	System.out.println("========== InvoiceDateSend获取需要推送的签单完成的个单-耗时： "+((endSSRSgetData -startSSRSgetData)/1000/60)+"分钟");
    	return tSSRS;
    }
    
    private void buildError(String szFunc, String szErrMsg){
        CError cError = new CError();
        cError.moduleName = "InvoiceDateSend";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String[] args) {
    	InvoiceDateSendSH instance = new InvoiceDateSendSH();
    	instance.run();
	}
}
