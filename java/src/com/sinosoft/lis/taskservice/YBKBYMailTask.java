package com.sinosoft.lis.taskservice;

import java.io.File;

import oracle.sql.DATE;

import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class YBKBYMailTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 设置附件路径 */
	private String path = "";
	private String suffix = ".pdf";
	
	//设置生成日志需要的字段
	/** 发送状态 */
	public String SendFlag;
	
	/** 入机日期 */
	private String MakeDate;
	
	/** 设置操作员 */
	private String Operator = "001";
	
	/**设置流水号*/
	private String SerialNo;
	public String getSendFlag() {
		return SendFlag;
	}

	public void setSendFlag(String sendFlag) {
		SendFlag = sendFlag;
	}

	public String getRemark() {
		return Remark;
	}

	public void setRemark(String remark) {
		Remark = remark;
	}

	/** 入机时间 */
	private String MakeTime;
	
	/** 最后修改日期 */
	private String ModifyDate;
	
	/** 最后修改时间 */
	private String ModifyTime;
	
	/** 备注信息  */
	public String Remark;
	
	private String mWhereSQL = " and signdate < current date - 1 day ";
	
	public YBKBYMailTask(){
	}
	
	public YBKBYMailTask(String startDate,String endDate){
		mWhereSQL = "and signdate between '"+startDate+"' and '"+endDate+"' ";
	}
	
	
	public void run() {
		// 邮箱发送(用户,密码)
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
		SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS.GetText(1, 2),"picchealth");
		//设置操作员
		Operator = "001";
		//设置入机时间
		MakeDate = PubFun.getCurrentDate();
		MakeTime = PubFun.getCurrentTime();
		
		//获取客户的合同号和电子邮箱
		String sql = "select distinct lcp.contno,lcad.EMail,lca.appntname,lcp.riskcode,(select riskname from lmrisk where riskcode = lcp.riskcode) " +
		" from lcappnt lca " +
		" inner join lcpol lcp on lcp.contno = lca.contno " +
		" inner join lcaddress lcad on lca.appntno = lcad.customerno and lca.addressno = lcad.addressno " +
		" where lcp.riskcode in (select code from ldcode where codetype='ybkriskcode') " +
		" and lcp.conttype = '1' and lcp.appflag = '1' " 
		+ mWhereSQL
		+ "with ur";
		
		SSRS tSSRSR = new ExeSQL().execSQL(sql);
		
		//设置最后修改时间
		ModifyDate = PubFun.getCurrentDate();
		ModifyTime = PubFun.getCurrentTime();
		
		for(int i = 1;i <= tSSRSR.MaxRow;i++){
			if(tSSRSR.GetText(i, 2) == null || "".equals(tSSRSR.GetText(i, 2))){
				String tContNo = tSSRSR.GetText(i, 1);
				System.out.println("合同号为: " + tContNo);
				
				//标记发送状态
				SendFlag = "2";
				Remark = "该客户没有预留电子邮箱!";
				//设置流水号
				SerialNo = PubFun1.CreateMaxNo("YBKDZBDLOG", 20);
				System.out.println("该客户没有预留电子邮箱!");
				//生成日志
				String Rsql = "insert into YbkDzbdLog(SerialNo,PolicyNo,Email,SendFlag,Remark,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime)" +
				"values('" + SerialNo + "','" + tContNo + "','" + "" + "','" + SendFlag + "','" + Remark + "','" + Operator + "','" + MakeDate + "','" + MakeTime + "','" + ModifyDate +"','" + ModifyTime +  
				"')";
				if(!new ExeSQL().execUpdateSQL(Rsql)){
					Remark = "该客户没有预留电子邮箱!插入数据失败!";
				}else{
					System.out.println("数据插入成功!");
				}
				System.out.println("备注信息为: " + Remark);
			}else{
				String tContNo = tSSRSR.GetText(i, 1);
				String tEmail = tSSRSR.GetText(i, 2);
				System.out.println("合同号为: " + tContNo);
				System.out.println("邮箱为: " + tEmail);
				
				//标记发送状态
				SendFlag = "1";
				Remark = "邮件已发送成功!";
				
				String getPatySQL = "select codename from ldcode where codetype = 'ybkpdfpath' ";
				path = new ExeSQL().getOneValue(getPatySQL);
//				path = "D:\\gnkf\\";
				String tFilePath = path + tContNo + suffix;
				System.out.println("===电子保单路径==="+tFilePath);
				File file=new File(tFilePath);
				if(!file.exists()){
					SendFlag = "2";
					Remark = "电子保单信息尚未生成，请确认是否已打印，或稍后再试!";
				}else{
					tMailSender.setSendInf("中国人民健康保险股份有限公司电子保单", "尊敬的"+tSSRSR.GetText(i, 3)+"您好：\r\n附件是您购买"+tSSRSR.GetText(i, 5)+"产品的电子保单，请您查收。",tFilePath);
					tMailSender.setToAddress(tEmail, "", null);
					
//					 发送邮件
					if (!tMailSender.sendMail()) {
						//修改标记发送状态
						SendFlag = "2";
						Remark = tMailSender.getErrorMessage();
						System.out.println(tMailSender.getErrorMessage());
					}
				}
				
				System.out.println("发送状态为: " + SendFlag);
				System.out.println("备注信息为: " + Remark);
				
				//设置最后修改时间
				SerialNo = PubFun1.CreateMaxNo("YBKDZBDLOG", 20);
				//生成日志
				String Rsql = "insert into YbkDzbdLog(SerialNo,PolicyNo,Email,SendFlag,Remark,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime) " +
						"values('" + SerialNo + "','" + tContNo + "','" + tEmail + "','" + SendFlag + "','" + Remark + "','" + Operator + "','" + MakeDate + "','" + MakeTime + "','" + ModifyDate +"','" + ModifyTime +  
						"')";
				if(!new ExeSQL().execUpdateSQL(Rsql)){
					System.out.println("保存保单"+tContNo+"的发送电子保单日志失败");
				}
			}
		}
		
	}

	// 主方法 测试用
	public static void main(String[] args) {
		
		YBKBYMailTask m = new YBKBYMailTask("2017-02-21","2017-02-21");
		m.run();
	}
	
}