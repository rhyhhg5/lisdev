package com.sinosoft.lis.taskservice;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.SocketException;

import com.sinosoft.lis.db.t_policy_mainDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.t_policy_mainSchema;
import com.sinosoft.lis.vschema.t_policy_mainSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SysConst;

/**
 * <p>Title:MixedAgentTask.java </p>
 *
 * <p>Description:集团交叉销售批处理 </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company:sinosoft </p>
 *
 * @author huxl
 * @version 1.0
 */
public class MixedContNUTTask extends TaskThread
{
    public String mCurrentDate = PubFun.getCurrentDate();
    private FileWriter mFileWriter;
    private BufferedWriter mBufferedWriter;
    private String mURL = "";
    private MMap tMMap = new MMap();
    private String mCurrrentDate = PubFun.getCurrentDate();
    
    private String mIP = "";
    private String mUserName = "";
    private String mPassword = "";
    private int mPort;
    private String mTransMode = "";

    public MixedContNUTTask()
    {
        try
        {
            jbInit();
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void run()
    {
        System.out.println("开始提取" + this.mCurrentDate + "日的交叉销售数据,开始时间是" +
                           PubFun.getCurrentTime());
        if (!getMixedAgent())
        {
            return;
        }
        System.out.println("提取" + this.mCurrentDate + "日的交叉销售数据完成,完成时间是" +
                           PubFun.getCurrentTime());
    }

    private boolean getMixedAgent()
    {
        String tSQL =
                "select SysVarValue from LDSYSVAR where Sysvar='JiaoChaURLNT'";//保存文件爱你的路径
        this.mURL = new ExeSQL().getOneValue(tSQL);
        //this.mURL = "D:\\xmlBackup3\\";
        if (mURL == null || mURL.equals(""))
        {
            System.out.println("没有找到集团交叉销售存储文件夹！");
            return false;
        }
        
        String tFtpSQL = "select code,codename from ldcode where codetype = 'jtjxftpn' ";
        SSRS tSSRS = new ExeSQL().execSQL(tFtpSQL);
        if(tSSRS == null || tSSRS.MaxRow<1){
        	System.out.println("没有找到集团交叉ftp配置信息！");
            return false;
        }
        for(int i=1;i<=tSSRS.getMaxRow();i++){
        	if("IP".equals(tSSRS.GetText(i, 1))){
        		mIP = tSSRS.GetText(i, 2);
        	}else if("UserName".equals(tSSRS.GetText(i, 1))){
        		mUserName = tSSRS.GetText(i, 2);
        	}else if("Password".equals(tSSRS.GetText(i, 1))){
        		mPassword = tSSRS.GetText(i, 2);
        	}else if("Port".equals(tSSRS.GetText(i, 1))){
        		mPort = Integer.parseInt(tSSRS.GetText(i, 2));
        	}else if("TransMode".equals(tSSRS.GetText(i, 1))){
        		mTransMode = tSSRS.GetText(i, 2);
        	}
        }

//        getS002();
//        getD002();
        getP001();
        getP002();
//        getS004();
       // getS005();
        
        FtpTransferFiles();
        try
        {
            del(mURL);
        } catch (IOException ex)
        {
            ex.printStackTrace();
        }
//        getS001();
//        getD001();
      
     // 客户整合提数
//        getCP01();
//        getCP02();
//        getCC01();
//        getCC02();
//        getCC03();
//        getCC04();
//        getCC05();
//        // ---------------------
//        FtpTransferFilesCstData();
//        try
//        {
//            del(mURL);
//        } catch (IOException ex)
//        {
//            ex.printStackTrace();
//        }
   		return true;
    }
    
    /**
     * 将生成文件传送到10.252.1.112的/home/gather/dat/目录下.
     */
    private void FtpTransferFiles()
    {
    	FTPTool tFTPTool = new FTPTool(mIP, mUserName, mPassword, mPort,mTransMode);
        try
        {
            if (!tFTPTool.loginFTP())
            {
                System.out.println(tFTPTool.getErrContent(1));
            } else
            {
                String[] tFileNames = new File(mURL).list();
                if (tFileNames == null)
                {
                    System.out.println("集团交叉销售Error:获取目录下的文件失败，请检查该目录是否存在！");
                } else
                {
                    for (int i = 0; i < tFileNames.length; i++)
                    {
                        tFTPTool.upload("/gather/gather/mqm/send/mainData/",
                                        mURL + tFileNames[i]);
                    }
                }
            }
        } catch (SocketException ex)
        {
            ex.printStackTrace();
        } catch (IOException ex)
        {
            ex.printStackTrace();
        } finally
        {
            tFTPTool.logoutFTP();
        }
    }
    
    /**
     * 将生成文件传送到10.252.1.112的/home/gather/dat/目录下.
     */
    private void FtpTransferFilesCstData()
    {
    	FTPTool tFTPTool = new FTPTool(mIP, mUserName, mPassword, mPort,mTransMode);
        try
        {
            if (!tFTPTool.loginFTP())
            {
                System.out.println(tFTPTool.getErrContent(1));
            } else
            {
                String[] tFileNames = new File(mURL).list();
                if (tFileNames == null)
                {
                    System.out.println("集团交叉销售Error:获取目录下的文件失败，请检查该目录是否存在！");
                } else
                {
                    for (int i = 0; i < tFileNames.length; i++)
                    {
                        tFTPTool.upload("/gather/gather/mqm/send/cstData/",
                                        mURL + tFileNames[i]);
                    }
                }
            }
        } catch (SocketException ex)
        {
            ex.printStackTrace();
        } catch (IOException ex)
        {
            ex.printStackTrace();
        } finally
        {
            tFTPTool.logoutFTP();
        }
    }

    /**
     * 营销员信息表
     */
    private void getS001()
    {
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "test000085S001" + mCurrentDate.replaceAll("-", "") + ".txt", true);
            String tSQL =
                    "select agentcode  营销员工号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                    "0 当事方编码,'' 当事方角色代码,name 姓名,ts_fmt(date(birthday),'yyyymmdd') 出生日期,CodeName('crs_cst_linker_sex',sex) 性别,managecom 销售机构代码," +
                    "(select name from ldcom where comcode=laagent.managecom ) 销售机构名称," +
                    "'11' 证件类型代码,'居民身份证' 证件类型名称,idno 证件号码,phone 联系电话," +
                    "mobile 联系手机,email 电子邮件,case when agentstate<='05' then '1' else  '0' end  在职状态代码," +
                    "case when agentstate<='05' then '在职' else  '离职' end 在职状态名称,'2'  营销员类型代码," +
                    "'个人代理' 营销员类型名称,'I' 数据变更类型,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' 时间戳 " +
                    "from laagent " +
                    "where branchtype2 not in ('04','05') and ModifyDate = current date - 1 day with ur";
            int start = 1;
            int nCount = 200;
            while (true)
            {
                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }
                String tempString = tSSRS.encode();
                String[] tempStringArr = tempString.split("\\^");
                //默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
//            mFileWriter = new FileWriter("C:\\000085S001" +
//                                         mCurrentDate.replaceAll("-", "") +
//                                         ".txt");
//            System.out.println(mFileWriter.getEncoding());
//            mBufferedWriter = new BufferedWriter(mFileWriter);
//          指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
                tFileOutputStream = new FileOutputStream(mURL + "test000085S001" +
                        mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[S001]:Sales_Cod = '" +
                                          tSSRS.GetText(i, 1) +
                                          "' AND Comp_Cod = '" +
                                          tSSRS.GetText(i, 2) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");
                    tArr[3] = tArr[3].substring(1, tArr[3].length() - 1);
                    mBufferedWriter.write("I[S001]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
        } catch (IOException ex2)
        {
            System.out.println(ex2.getStackTrace());
        }
    }

    /**
     * 中介机构信息表
     */
    private void getS002()
    {
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "test000085S002" + mCurrentDate.replaceAll("-", "") + ".txt", true);
            String tSQL = "select agentcom 中介机构ID,name 中介机构名称," +
                          "'000085' 子公司代码," +
                          "'中国人民健康保险股份有限公司' 子公司名称," +
                          "'310' 企业性质,ts_fmt(date(ChiefBusiness),'yyyymmdd') 成为中介日期,managecom 管理机构代码,(select name from ldcom where comcode=lacom.managecom ) 管理机构名称," +
                          "LicenseNo 企业注册证号,case when sellflag='Y' then '1' else  '9' end 状态," +
                          "Corporation  联系人姓名,'' 联系人性别,'' 联系人电话,'' 联系人手机,'' 联系人电子邮件,'I' 数据变更类型," +
                          "ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' 时间戳 " +
                          " from lacom where  branchtype='2' and branchtype2 not in ('04','05') and ModifyDate = current date - 1 day with ur";
            int start = 1;
            int nCount = 200;
            while (true)
            {
                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }
                String tempString = tSSRS.encode();
                String[] tempStringArr = tempString.split("\\^");

                tFileOutputStream = new FileOutputStream(mURL + "test000085S002" +
                        mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[S002]:Brok_Cod = '" +
                                          tSSRS.GetText(i, 1) +
                                          "' AND Comp_Cod = '" +
                                          tSSRS.GetText(i, 3) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    mBufferedWriter.write("I[S002]:'" + t + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
        } catch (IOException ex2)
        {
        }
    }

    /**
     * 机构维度表
     */
    private void getD001()
    {
        String tSQL = "select a.comcode orgcod,'000085','中国人民健康保险股份有限公司'," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                      "(select codealias from ldcode where codetype='managearea' and code=substr(a.comcode,1,4))," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                      "(select codename from ldcode where codetype='managearea' and code=substr(a.comcode,1,4))," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                      "substr(a.comcode,1,4),(select name from ldcom where comcode=substr(a.comcode,1,4))," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                      "comcode,(select name from ldcom where comcode=substr(a.comcode,1,8))," + 
                      "comcode,(select name from ldcom where comcode=substr(a.comcode,1,8))," +
                      "comcode,(select name from ldcom where comcode=substr(a.comcode,1,8))," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                      "case when substr(a.comcode,5,4) = '0000' then '1' when substr(a.comcode,7,2) = '00' then '2' else '3' end,'','',a.sign,case when a.sign='1' then '正常' else '终止' end," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                      "(select codealias from ldcode where codetype='managecode' and code=substr(a.comcode,1,4))," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                      "(select codename from ldcode where codetype='managecode' and code=substr(a.comcode,1,4))," + 
//                按修改要求添加市一级的归集机构,县一级的暂时为空
                      "(select codealias from ldcode where codetype='managecode' and code=a.comcode)," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                      "(select codename from ldcode where codetype='managecode' and code=a.comcode)," + 
                      "''," +                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                      "''," + 
                      "'I',db2inst1.ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss'),'' " +   
                      //",'','' dep_cod,'' dep_name,'000085' coll_type,''check_status ,'' check_date , Name orgname "+
                      "from ldcom a where ComGrade = '03' and ComCode <> '86000000'  and ModifyDate = current date - 1 day order by a.comcode with ur";
                      //" from ldcom a where ComGrade = '03' and ComCode <> '86000000'  and ModifyDate between '2010-6-1' and '2010-6-1' order by a.comcode with ur";
                      

        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(tSQL);

        String tempString = tSSRS.encode();
        String[] tempStringArr = tempString.split("\\^");
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "test000085D001" + mCurrentDate.replaceAll("-", "") +
                    ".txt");
            OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                    tFileOutputStream, "GBK");
            mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                mBufferedWriter.write("D[D001]:Orgcod = '" +
                                      tSSRS.GetText(i, 1) +
                                      "' AND Comp_Cod = '" +
                                      tSSRS.GetText(i, 2) + "'");
                mBufferedWriter.newLine();
                String t = tempStringArr[i].replaceAll("\\|", "','");
                mBufferedWriter.write("I[D001]:'" + t + "'");
                mBufferedWriter.newLine();
                mBufferedWriter.flush();
            }
            mBufferedWriter.close();
        } catch (IOException ex2)
        {
        }
    }

    /**
     * 险种维度表
     */
    private void getD002()
    {
        String tSQL =
                "select a.riskcode,a.riskname,a.riskshortname,'000085','中国人民健康保险股份有限公司'," +
                "a.riskver,'',(case when b.subriskflag='M' then '1' else '2' end),''," +
                "ts_fmt(date(b.startdate), 'yyyymmdd'),ts_fmt(date(b.enddate), 'yyyymmdd')," +
                "'','','','','','','',''," +
                "'','','','','','','',''," +
                "(case when b.riskperiod='L' then '2' else '1' end)," +
                "(case when c.riskgrpflag='2' then '0' when c.riskgrpflag='1' then '1' end)," +
                "(case when c.healthtype='6' or c.healthtype='7' then '1' when c.healthtype='4' then '3' when c.healthtype='5' then '4' else '2' end)," +
                "(case when c.healthtype='3' then '1' when c.healthtype='6' then '2' when c.healthtype='7' then '3' else '9' end)," +
                "'','','',''," +
                "'I',ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss'),'' " +
                "from lmrisk a,lmriskapp b,lfrisk c " +
                "where a.riskcode=b.riskcode and a.riskcode=c.riskcode " +
                "order by a.riskcode with ur";

        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(tSQL);

        String tempString = tSSRS.encode();
        String[] tempStringArr = tempString.split("\\^");
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "test000085D002" + mCurrentDate.replaceAll("-", "") +
                    ".txt");
            OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                    tFileOutputStream, "GBK");
            mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                mBufferedWriter.write("D[D002]:Risk_Cod = '" +
                                      tSSRS.GetText(i, 1) +
                                      "' AND Comp_Cod = '" +
                                      tSSRS.GetText(i, 4) + "'");
                mBufferedWriter.newLine();
                String t = tempStringArr[i].replaceAll("\\|", "','");
                mBufferedWriter.write("I[D002]:'" + t + "'");
                mBufferedWriter.newLine();
                mBufferedWriter.flush();
            }
            mBufferedWriter.close();
        } catch (IOException ex2)
        {
        }
    }

    /**
     * 保单信息表
     */
    private void getP001()
    {
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "test000085P001" + mCurrentDate.replaceAll("-", "") +
                    ".txt", true);
            String tSQL = "select " +
            "lcc.ContNo pol_no, " +
            "(select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo) pol_no_orig, " +
            "'000085' comp_cod, " +
            "'中国人民健康保险股份有限公司' comp_nam, " +
            "ts_fmt(lcc.SignDate, 'yyyymmdd') date_sign, " +
            "ts_fmt(lcc.CValiDate, 'yyyymmdd') date_begin, " +
            "ts_fmt(lcc.CInValiDate, 'yyyymmdd') date_end, " +
            "ts_fmt(lcc.FirstPayDate, 'yyyymmdd') date_fee," +
            "(select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lcc.ContNo) pay_yrs, " +
            "CodeName('crs_pay_mod', char(lcc.PayIntv)) pay_mod_cod, " +
            "CodeAlias('crs_pay_mod', char(lcc.PayIntv)) pay_mod_nam, " +
            "'1' grp_psn_flg, " +
            "'99' chl_typ_cod, " +
            "'其它' chl_typ_nam, " +
            "'' chl_cod, " +
            "'' chl_nam, " +
            "lcc.AgentCom brok_cod, " +
            "(select lac.Name from LACom lac where lac.AgentCom = lcc.AgentCom) brok_nam, " +
            "CodeName('crs_salechnl_typ', lcc.crs_salechnl) crs_typ_cod, " +
            "CodeAlias('crs_salechnl_typ', lcc.crs_salechnl) crs_typ_nam, " +
            "(select groupagentcode from laagent where agentcode = lcc.agentcode) sales_cod," +
            "(select Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) sales_nam," +
            "lcc.GrpAgentCode crs_sales_cod, " +
            "lcc.GrpAgentName crs_sales_nam, " +
            "lcc.ManageCom org_sales_cod, " +
            "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_sales_nam, " +
            "lcc.ManageCom org_belg_cod, " +
            "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_belg_nam, " +
            "lcc.GrpAgentCom org_crs_cod, " +
            "'' org_crs_nam, " +
            "lcc.AppntNo cst_cod, " +
            "lcc.AppntName cst_nam, " +
            "'P' cst_typ, " +
            "(select lcad.PostalAddress from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_addr, " +
            "(select lcad.ZipCode from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_post," +
            "CodeName('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_cod, " +
            "CodeAlias('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_nam," +
            "lcc.AppntIdNo cst_idno," +
            "lcc.AppntName cst_linker_nam, " +
            "CodeName('crs_cst_linker_sex', lcc.AppntSex) cst_linker_sex," +
            "(select lcad.Phone from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_tel," +
            "(select lcad.Mobile from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mob," +
            "(select lcad.EMail from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mail," +
            "replace((select value(lci.Name, '') || '' || value(lcad.PostalAddress, '') || '' || value(lcad.Phone, '') from LCInsured lci inner join LCAddress lcad on lcad.CustomerNo = lci.Insuredno and lcad.AddressNo = lci.AddressNo and lci.ContNo = lcc.ContNo and lci.Insuredno = lcc.Insuredno),',','，') insd_info," +
            "(select lcb.Name from LCBnf lcb where lcb.ContNo = lcc.ContNo fetch first 1 rows only) beft_info," +
            "lcc.InsuredIdNo insr_targ, " +
            "lcc.InsuredName insr_targ_info," +
            "lcc.Prem prem," +
            "0 prem_sign," +
            "0 prem_pol, " +
            "lcc.Amnt amount, " +
            "'1' percnt," +
            "'' bak1," +
            "'' bak2," +
            "'' bak3," +
            "'' bak4," +
            "CodeName('crs_busstype_typ', lcc.Crs_BussType) bak5," +
            "CodeAlias('crs_busstype_typ', lcc.Crs_BussType) bak6," +
            "lcc.grpagentidno bak7," +
            "(ts_fmt(lcc.CValiDate, 'yyyymmdd')||'_'||lcc.crs_busstype||'_'||'085'||'_'||lcc.managecom||'_'||lcc.contno) bak8," +
            "'I' data_upd_typ," +
            "ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send," +
            "'' date_update " +
            " ,ts_fmt(lcc.inputdate, 'yyyymmdd') inputdate " + //录单时间
            " ,lcc.signdate hold_date "+ //投保日期
            " ,lcc.ProposalContNo proposal_no " +//投保单号
            " ,'1' policy_conti_type " +//业务类型区分
            "From LCCont lcc " +
            "where 1 = 1 " +
            "and lcc.ContType = '1' " +
            "and lcc.AppFlag = '1' " +
            "and lcc.UWFlag in ('4', '9') " +
            "and lcc.SaleChnl not in ('04', '13') " +
            "and lcc.Crs_SaleChnl in ('01', '02') " +
            "and lcc.SignDate = current date - 1 day " +
            //******我是分割线******//
            " union all " +
            "select " +
            "lcc.ContNo pol_no, " +
            "(select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo) pol_no_orig, " +
            "'000085' comp_cod, " +
            "'中国人民健康保险股份有限公司' comp_nam, " +
            "ts_fmt(lcc.SignDate, 'yyyymmdd') date_sign, " +
            "ts_fmt(lcc.CValiDate, 'yyyymmdd') date_begin, " +
            "ts_fmt(lcc.CInValiDate, 'yyyymmdd') date_end, " +
            "ts_fmt(lcc.FirstPayDate, 'yyyymmdd') date_fee," +
            "(select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lcc.ContNo) pay_yrs, " +
            "CodeName('crs_pay_mod', char(lcc.PayIntv)) pay_mod_cod, " +
            "CodeAlias('crs_pay_mod', char(lcc.PayIntv)) pay_mod_nam, " +
            "'2' grp_psn_flg, " +
            "'99' chl_typ_cod, " +
            "'其它' chl_typ_nam, " +
            "'' chl_cod, " +
            "'' chl_nam, " +
            "lcc.AgentCom brok_cod, " +
            "(select lac.Name from LACom lac where lac.AgentCom = lcc.AgentCom) brok_nam, " +
            "CodeName('crs_salechnl_typ', lcc.crs_salechnl) crs_typ_cod, " +
            "CodeAlias('crs_salechnl_typ', lcc.crs_salechnl) crs_typ_nam, " +
            "(select groupagentcode from laagent where agentcode = lcc.agentcode) sales_cod," +
            "(select Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) sales_nam," +
            "lcc.GrpAgentCode crs_sales_cod, " +
            "lcc.GrpAgentName crs_sales_nam, " +
            "lcc.ManageCom org_sales_cod, " +
            "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_sales_nam, " +
            "lcc.ManageCom org_belg_cod, " +
            "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_belg_nam, " +
            "lcc.GrpAgentCom org_crs_cod, " +
            "'' org_crs_nam, " +
            "lcc.AppntNo cst_cod, " +
            "lcc.AppntName cst_nam, " +
            "'P' cst_typ, " +
            "(select lcad.PostalAddress from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_addr, " +
            "(select lcad.ZipCode from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_post," +
            "CodeName('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_cod, " +
            "CodeAlias('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_nam," +
            "lcc.AppntIdNo cst_idno," +
            "lcc.AppntName cst_linker_nam, " +
            "CodeName('crs_cst_linker_sex', lcc.AppntSex) cst_linker_sex," +
            "(select lcad.Phone from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_tel," +
            "(select lcad.Mobile from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mob," +
            "(select lcad.EMail from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mail," +
            "replace((select value(lci.Name, '') || '' || value(lcad.PostalAddress, '') || '' || value(lcad.Phone, '') from LCInsured lci inner join LCAddress lcad on lcad.CustomerNo = lci.Insuredno and lcad.AddressNo = lci.AddressNo and lci.ContNo = lcc.ContNo and lci.Insuredno = lcc.Insuredno),',','，') insd_info," +
            "(select lcb.Name from LCBnf lcb where lcb.ContNo = lcc.ContNo fetch first 1 rows only) beft_info," +
            "lcc.InsuredIdNo insr_targ, " +
            "lcc.InsuredName insr_targ_info," +
            "lcc.Prem prem," +
            "0 prem_sign," +
            "0 prem_pol, " +
            "lcc.Amnt amount, " +
            "'1' percnt," +
            "'' bak1," +
            "'' bak2," +
            "'' bak3," +
            "'' bak4," +
            "CodeName('crs_busstype_typ', lcc.Crs_BussType) bak5," +
            "CodeAlias('crs_busstype_typ', lcc.Crs_BussType) bak6," +
            "lcc.grpagentidno bak7," +
            "(ts_fmt(lcc.CValiDate, 'yyyymmdd')||'_'||lcc.crs_busstype||'_'||'085'||'_'||lcc.managecom||'_'||lcc.contno) bak8," +
            "'I' data_upd_typ," +
            "ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send," +
            "'' date_update " +
            " ,ts_fmt(lcc.inputdate, 'yyyymmdd')   inputdate " + //录单时间
            " ,lcc.signdate hold_date "+ //投保日期
            " ,lcc.ProposalContNo proposal_no " +//投保单号
            " ,'1' policy_conti_type " +//业务类型区分
            "From LCCont lcc " +
            "where 1 = 1 " +
            "and lcc.ContType = '1' " +
            "and lcc.AppFlag = '1' " +
            "and lcc.UWFlag in ('4', '9') " +
            "and lcc.SaleChnl in ('04', '13') " +
            "and lcc.Crs_SaleChnl in ('01', '02') " +
            "and lcc.SignDate = current date - 1 day " +
            //******我是分割线******//
            " union all " +
            "select " +
            "	lgc.GrpContNo pol_no," +
            "	'' pol_no_orig, " +
            "	'000085' comp_cod, " +
            "	'中国人民健康保险股份有限公司' comp_nam," +
            "	ts_fmt(lgc.SignDate, 'yyyymmdd') date_sign," +
            "	ts_fmt(lgc.CValiDate, 'yyyymmdd') date_begin," +
            "	ts_fmt(lgc.CInValiDate, 'yyyymmdd') date_end," +
            "	ts_fmt((select ljtf.PayDate from LJTempFee ljtf where ljtf.otherno = lgc.GrpContNo and ljtf.othernotype = '7' fetch first 1 rows only), 'yyyymmdd') date_fee," +
            "	(select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lgc.GrpContNo) pay_yrs," +
            "	CodeName('crs_pay_mod', char(lgc.PayIntv)) pay_mod_cod, " +
            "	CodeAlias('crs_pay_mod', char(lgc.PayIntv)) pay_mod_nam," +
            "	'0' grp_psn_flg, " +
            "	'99' chl_typ_cod," +
            "	'其它' chl_typ_nam," +
            "	'' chl_cod," +
            "	'' chl_nam," +
            "	lgc.AgentCom brok_cod," +
            "	(select lac.Name from LACom lac where lac.AgentCom = lgc.AgentCom) brok_nam," +
            "	CodeName('crs_salechnl_typ', lgc.crs_salechnl) crs_typ_cod," +
            "	CodeAlias('crs_salechnl_typ', lgc.crs_salechnl) crs_typ_nam," +
            "	lgc.AgentCode sales_cod," +
            "	(select Name from LAAgent laa where laa.AgentCode = lgc.AgentCode) sales_nam," +
            "	lgc.GrpAgentCode crs_sales_cod," +
            "	lgc.GrpAgentName crs_sales_nam," +
            "	lgc.ManageCom org_sales_cod," +
            "	(select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_sales_nam," +
            "	lgc.ManageCom org_belg_cod," +
            "	(select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_belg_nam," +
            "	lgc.GrpAgentCom org_crs_cod," +
            "	'' org_crs_nam," +
            "	lgc.AppntNo cst_cod," +
            "	lgc.GrpName cst_nam," +
            "	'G' cst_typ," +
            "	(select lcad.GrpAddress from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_addr," +
            "	(select lcad.GrpZipCode from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_post," +
            "	'' cst_idtyp_cod," +
            "	'' cst_idtyp_nam," +
            "	'' cst_idno," +
            "	(select lcad.LinkMan1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_linker_nam," +
            "	'' cst_linker_sex," +
            "	(select lcad.Phone1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_tel," +
            "	(select lcad.Mobile1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_mob," +
            "	(select lcad.E_Mail1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_mail," +
            "	(select lcc.InsuredName || '等' from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insd_info," +
            "	'' beft_info," +
            "	(select lcc.InsuredNo from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insr_targ," +
            "	(select lcc.InsuredName from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insr_targ_info," +
            "	lgc.Prem prem," +
            "	0 prem_sign," +
            "	0 prem_pol," +
            "	lgc.Amnt amount," +
            "	'1' percnt," +
            "'' bak1," +
            "'' bak2," +
            "'' bak3," +
            "'' bak4," +
            "CodeName('crs_busstype_typ', lgc.Crs_BussType) bak5," +
            "CodeAlias('crs_busstype_typ', lgc.Crs_BussType) bak6," +
            "	lgc.grpagentidno bak7, " +
            "	(ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) bak8, " +
            "	'I' data_upd_typ," +
            "	ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send," +
            "	'' date_update " +
            " ,ts_fmt(lgc.inputdate, 'yyyymmdd') inputdate" + //录单时间
            " ,lgc.signdate hold_date"+ //投保日期
            " ,lgc.ProposalGrpContNo proposal_no " +//投保单号
            " ,'1' policy_conti_type " +//业务类型区分
            "From LCGrpCont lgc " +
            " where 1 = 1 " +
            "	and lgc.AppFlag = '1' " +
            "	and lgc.UWFlag in ('4', '9') " +
            "	and lgc.crs_salechnl in ('01', '02') " +
            "   and ((lgc.cardflag!='2') or (lgc.cardflag is null))" +
            " and lgc.SignDate = current date - 1 day " +
            //******我是分割线******//	
            " union all " +
            "select " +
            "lcc.ContNo pol_no, " +
            "(select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo) pol_no_orig, " +
            "'000085' comp_cod, " +
            "'中国人民健康保险股份有限公司' comp_nam, " +
            "ts_fmt(lcc.SignDate, 'yyyymmdd') date_sign, " +
            "ts_fmt(lcc.CValiDate, 'yyyymmdd') date_begin, " +
            "ts_fmt(lcc.CInValiDate, 'yyyymmdd') date_end, " +
            "ts_fmt(lcc.FirstPayDate, 'yyyymmdd') date_fee," +
            "(select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lcc.ContNo) pay_yrs, " +
            "CodeName('crs_pay_mod', char(lcc.PayIntv)) pay_mod_cod, " +
            "CodeAlias('crs_pay_mod', char(lcc.PayIntv)) pay_mod_nam, " +
            "'1' grp_psn_flg, " +
            "'99' chl_typ_cod, " +
            "'其它' chl_typ_nam, " +
            "'' chl_cod, " +
            "'' chl_nam, " +
            "lcc.AgentCom brok_cod, " +
            "(select lac.Name from LACom lac where lac.AgentCom = lcc.AgentCom) brok_nam, " +
            "CodeName('crs_salechnl_typ', lcc.crs_salechnl) crs_typ_cod, " +
            "CodeAlias('crs_salechnl_typ', lcc.crs_salechnl) crs_typ_nam, " +
            "(select groupagentcode from laagent where agentcode = lcc.agentcode) sales_cod," +
            "(select Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) sales_nam," +
            "lcc.GrpAgentCode crs_sales_cod, " +
            "lcc.GrpAgentName crs_sales_nam, " +
            "lcc.ManageCom org_sales_cod, " +
            "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_sales_nam, " +
            "lcc.ManageCom org_belg_cod, " +
            "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_belg_nam, " +
            "lcc.GrpAgentCom org_crs_cod, " +
            "'' org_crs_nam, " +
            "lcc.AppntNo cst_cod, " +
            "lcc.AppntName cst_nam, " +
            "'P' cst_typ, " +
            "(select lcad.PostalAddress from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_addr, " +
            "(select lcad.ZipCode from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_post," +
            "CodeName('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_cod, " +
            "CodeAlias('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_nam," +
            "lcc.AppntIdNo cst_idno," +
            "lcc.AppntName cst_linker_nam, " +
            "CodeName('crs_cst_linker_sex', lcc.AppntSex) cst_linker_sex," +
            "(select lcad.Phone from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_tel," +
            "(select lcad.Mobile from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mob," +
            "(select lcad.EMail from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mail," +
            "replace((select value(lci.Name, '') || '' || value(lcad.PostalAddress, '') || '' || value(lcad.Phone, '') from LCInsured lci inner join LCAddress lcad on lcad.CustomerNo = lci.Insuredno and lcad.AddressNo = lci.AddressNo and lci.ContNo = lcc.ContNo and lci.Insuredno = lcc.Insuredno),',','，') insd_info," +
            "(select lcb.Name from LCBnf lcb where lcb.ContNo = lcc.ContNo fetch first 1 rows only) beft_info," +
            "lcc.InsuredIdNo insr_targ, " +
            "lcc.InsuredName insr_targ_info," +
            "lcc.Prem prem," +
            "0 prem_sign," +
            "0 prem_pol, " +
            "lcc.Amnt amount, " +
            "'1' percnt," +
            "'' bak1," +
            "'' bak2," +
            "'' bak3," +
            "'' bak4," +
            "CodeName('crs_busstype_typ', lcc.Crs_BussType) bak5," +
            "CodeAlias('crs_busstype_typ', lcc.Crs_BussType) bak6," +
            "lcc.grpagentidno bak7," +
            "(ts_fmt(lcc.CValiDate, 'yyyymmdd')||'_'||lcc.crs_busstype||'_'||'085'||'_'||lcc.managecom||'_'||lcc.contno) bak8," +
            "'I' data_upd_typ," +
            "ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send," +
            "'' date_update " +
            " ,ts_fmt(lcc.inputdate, 'yyyymmdd') inputdate" + //录单时间
            " ,lcc.signdate hold_date"+ //投保日期
            " ,lcc.ProposalContNo proposal_no " +//投保单号
            " ,'1' policy_conti_type " +//业务类型区分
            "From LCCont lcc , LOMixAgent lom " +
            "where 1 = 1 " +
            "and lcc.ContNo=lom.ContNo " +
            "and lcc.ContType = '1' " +
            "and lom.ContType = '1' " +
            "and lcc.AppFlag = '1' " +
            "and lcc.UWFlag in ('4', '9') " +
            "and lcc.SaleChnl not in ('04', '13') " +
            "and lom.SaleChnl in ('01', '02') and lom.standbyflag2 is null  " +
            "and lom.MakeDate = current date - 1 day " +
            //******我是分割线******//
            " union all " +
            "select " +
            "lcc.ContNo pol_no, " +
            "(select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo) pol_no_orig, " +
            "'000085' comp_cod, " +
            "'中国人民健康保险股份有限公司' comp_nam, " +
            "ts_fmt(lcc.SignDate, 'yyyymmdd') date_sign, " +
            "ts_fmt(lcc.CValiDate, 'yyyymmdd') date_begin, " +
            "ts_fmt(lcc.CInValiDate, 'yyyymmdd') date_end, " +
            "ts_fmt(lcc.FirstPayDate, 'yyyymmdd') date_fee," +
            "(select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lcc.ContNo) pay_yrs, " +
            "CodeName('crs_pay_mod', char(lcc.PayIntv)) pay_mod_cod, " +
            "CodeAlias('crs_pay_mod', char(lcc.PayIntv)) pay_mod_nam, " +
            "'2' grp_psn_flg, " +
            "'99' chl_typ_cod, " +
            "'其它' chl_typ_nam, " +
            "'' chl_cod, " +
            "'' chl_nam, " +
            "lcc.AgentCom brok_cod, " +
            "(select lac.Name from LACom lac where lac.AgentCom = lcc.AgentCom) brok_nam, " +
            "CodeName('crs_salechnl_typ', lcc.crs_salechnl) crs_typ_cod, " +
            "CodeAlias('crs_salechnl_typ', lcc.crs_salechnl) crs_typ_nam, " +
            "(select groupagentcode from laagent where agentcode = lcc.agentcode) sales_cod," +
            "(select Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) sales_nam," +
            "lcc.GrpAgentCode crs_sales_cod, " +
            "lcc.GrpAgentName crs_sales_nam, " +
            "lcc.ManageCom org_sales_cod, " +
            "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_sales_nam, " +
            "lcc.ManageCom org_belg_cod, " +
            "(select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_belg_nam, " +
            "lcc.GrpAgentCom org_crs_cod, " +
            "'' org_crs_nam, " +
            "lcc.AppntNo cst_cod, " +
            "lcc.AppntName cst_nam, " +
            "'P' cst_typ, " +
            "(select lcad.PostalAddress from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_addr, " +
            "(select lcad.ZipCode from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_post," +
            "CodeName('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_cod, " +
            "CodeAlias('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_nam," +
            "lcc.AppntIdNo cst_idno," +
            "lcc.AppntName cst_linker_nam, " +
            "CodeName('crs_cst_linker_sex', lcc.AppntSex) cst_linker_sex," +
            "(select lcad.Phone from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_tel," +
            "(select lcad.Mobile from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mob," +
            "(select lcad.EMail from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_mail," +
            "replace((select value(lci.Name, '') || '' || value(lcad.PostalAddress, '') || '' || value(lcad.Phone, '') from LCInsured lci inner join LCAddress lcad on lcad.CustomerNo = lci.Insuredno and lcad.AddressNo = lci.AddressNo and lci.ContNo = lcc.ContNo and lci.Insuredno = lcc.Insuredno),',','，') insd_info," +
            "(select lcb.Name from LCBnf lcb where lcb.ContNo = lcc.ContNo fetch first 1 rows only) beft_info," +
            "lcc.InsuredIdNo insr_targ, " +
            "lcc.InsuredName insr_targ_info," +
            "lcc.Prem prem," +
            "0 prem_sign," +
            "0 prem_pol, " +
            "lcc.Amnt amount, " +
            "'1' percnt," +
            "'' bak1," +
            "'' bak2," +
            "'' bak3," +
            "'' bak4," +
            "CodeName('crs_busstype_typ', lcc.Crs_BussType) bak5," +
            "CodeAlias('crs_busstype_typ', lcc.Crs_BussType) bak6," +
            "lcc.grpagentidno bak7," +
            "(ts_fmt(lcc.CValiDate, 'yyyymmdd')||'_'||lcc.crs_busstype||'_'||'085'||'_'||lcc.managecom||'_'||lcc.contno) bak8," +
            "'I' data_upd_typ," +
            "ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send," +
            "'' date_update " +
            " ,ts_fmt(lcc.inputdate, 'yyyymmdd')  inputdate" + //录单时间
            " ,lcc.signdate hold_date"+ //投保日期
            " ,lcc.ProposalContNo proposal_no " +//投保单号
            " ,'1' policy_conti_type " +//业务类型区分
            "From LCCont lcc , LOMixAgent lom " +
            "where 1 = 1 " +
            "and lcc.ContNo=lom.ContNo " +
            "and lcc.ContType = '1' " +
            "and lom.ContType = '1' " +
            "and lcc.AppFlag = '1' " +
            "and lcc.UWFlag in ('4', '9') " +
            "and lcc.SaleChnl in ('04', '13') " +
            "and lom.SaleChnl in ('01', '02') and lom.standbyflag2 is null  " +
            "and lom.MakeDate = current date - 1 day " +
            //******我是分割线******//
            " union all " +
            "select " +
            "	lgc.GrpContNo pol_no," +
            "	'' pol_no_orig, " +
            "	'000085' comp_cod, " +
            "	'中国人民健康保险股份有限公司' comp_nam," +
            "	ts_fmt(lgc.SignDate, 'yyyymmdd') date_sign," +
            "	ts_fmt(lgc.CValiDate, 'yyyymmdd') date_begin," +
            "	ts_fmt(lgc.CInValiDate, 'yyyymmdd') date_end," +
            "	ts_fmt((select ljtf.PayDate from LJTempFee ljtf where ljtf.otherno = lgc.GrpContNo and ljtf.othernotype = '7' fetch first 1 rows only), 'yyyymmdd') date_fee," +
            "	(select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lgc.GrpContNo) pay_yrs," +
            "	CodeName('crs_pay_mod', char(lgc.PayIntv)) pay_mod_cod, " +
            "	CodeAlias('crs_pay_mod', char(lgc.PayIntv)) pay_mod_nam," +
            "	'0' grp_psn_flg, " +
            "	'99' chl_typ_cod," +
            "	'其它' chl_typ_nam," +
            "	'' chl_cod," +
            "	'' chl_nam," +
            "	lgc.AgentCom brok_cod," +
            "	(select lac.Name from LACom lac where lac.AgentCom = lgc.AgentCom) brok_nam," +
            "	CodeName('crs_salechnl_typ', lgc.crs_salechnl) crs_typ_cod," +
            "	CodeAlias('crs_salechnl_typ', lgc.crs_salechnl) crs_typ_nam," +
            "	lgc.AgentCode sales_cod," +
            "	(select Name from LAAgent laa where laa.AgentCode = lgc.AgentCode) sales_nam," +
            "	lgc.GrpAgentCode crs_sales_cod," +
            "	lgc.GrpAgentName crs_sales_nam," +
            "	lgc.ManageCom org_sales_cod," +
            "	(select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_sales_nam," +
            "	lgc.ManageCom org_belg_cod," +
            "	(select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_belg_nam," +
            "	lgc.GrpAgentCom org_crs_cod," +
            "	'' org_crs_nam," +
            "	lgc.AppntNo cst_cod," +
            "	lgc.GrpName cst_nam," +
            "	'G' cst_typ," +
            "	(select lcad.GrpAddress from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_addr," +
            "	(select lcad.GrpZipCode from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_post," +
            "	'' cst_idtyp_cod," +
            "	'' cst_idtyp_nam," +
            "	'' cst_idno," +
            "	(select lcad.LinkMan1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_linker_nam," +
            "	'' cst_linker_sex," +
            "	(select lcad.Phone1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_tel," +
            "	(select lcad.Mobile1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_mob," +
            "	(select lcad.E_Mail1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1') cst_mail," +
            "	(select lcc.InsuredName || '等' from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insd_info," +
            "	'' beft_info," +
            "	(select lcc.InsuredNo from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insr_targ," +
            "	(select lcc.InsuredName from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insr_targ_info," +
            "	lgc.Prem prem," +
            "	0 prem_sign," +
            "	0 prem_pol," +
            "	lgc.Amnt amount," +
            "	'1' percnt," +
            "'' bak1," +
            "'' bak2," +
            "'' bak3," +
            "'' bak4," +
            "CodeName('crs_busstype_typ', lgc.crs_busstype) bak5," +
            "CodeAlias('crs_busstype_typ', lgc.crs_busstype) bak6," +
            "	lgc.grpagentidno bak7, " +
            "	(ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) bak8, " +
            "	'I' data_upd_typ," +
            "	ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send," +
            "	'' date_update " +
            " ,ts_fmt(lgc.inputdate, 'yyyymmdd')   inputdate" + //录单时间
            " ,lgc.signdate hold_date"+ //投保日期
            " ,lgc.ProposalGrpContNo proposal_no " +//投保单号
            " ,'1' policy_conti_type " +//业务类型区分
            "From LCGrpCont lgc  , LOMixAgent lom " +
            " where 1 = 1 " +
            "	and lgc.AppFlag = '1' " +
            " and lom.ContType = '2' " +
            " and lom.GrpContNo = lgc.GrpContNo " +
            "	and lgc.UWFlag in ('4', '9') and lom.standbyflag2 is null  " +
            "	and lom.SaleChnl in ('01', '02') " +
            "	and ((lgc.cardflag!='2') or (lgc.cardflag is null))" +
            " and lom.MakeDate = current date - 1 day " +
            //******我是分割线******//
            //卡单类型的交叉销售
            " union all " +
            "select " +
            "	'Hcd'||(select cardno from licertify where prtno=lgc.prtno fetch first 1 row only) pol_no," +
            "	'' pol_no_orig, " +
            "	'000085' comp_cod, " +
            "	'中国人民健康保险股份有限公司' comp_nam," +
            "	ts_fmt(lgc.SignDate, 'yyyymmdd') date_sign," +			//销售日期
            "	'' date_begin," +
            "	'' date_end," +
            "	'' date_fee," +
            "	0 pay_yrs," +
            "	'1' pay_mod_cod, " +
            "	'趸缴' pay_mod_nam," +
            "	'0' grp_psn_flg, " +
            "	lgc.crs_salechnl chl_typ_cod," +			
            "	(select codename from ldcode where codetype='crs_salechnl' and code=lgc.crs_salechnl) chl_typ_nam," +			
            "	'' chl_cod," +
            "	'' chl_nam," +
            "	'' brok_cod," +
            "	'' brok_nam," +
            "	CodeName('crs_salechnl_typ', lgc.crs_salechnl) crs_typ_cod," +
            "	CodeAlias('crs_salechnl_typ', lgc.crs_salechnl) crs_typ_nam," +
            "	value(lgc.AgentCode,'') sales_cod," +
            "	value((select Name from LAAgent laa where laa.AgentCode = lgc.AgentCode),'') sales_nam," +
            "	value(lgc.GrpAgentCode,'') crs_sales_cod," +
            "	value(lgc.GrpAgentName,'') crs_sales_nam," +
            	//下面的机构都为实际的出单机构和实际出单机构的归属机构
            "	lgc.ManageCom org_sales_cod," +
            "	(select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_sales_nam," +
            "	lgc.ManageCom org_belg_cod," +
            "	(select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_belg_nam," +
            
            "	value(lgc.GrpAgentCom,'') org_crs_cod," +
            "	'' org_crs_nam," +
            "	value(lgc.AppntNo,'') cst_cod," +
            "	value(lgc.GrpName,'') cst_nam," +
            "	'G' cst_typ," +
            "	value((select lcad.GrpAddress from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1'),'') cst_addr," +
            "	value((select lcad.GrpZipCode from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1'),'') cst_post," +
            "	'' cst_idtyp_cod," +
            "	'' cst_idtyp_nam," +
            "	'' cst_idno," +
            "	value((select lcad.LinkMan1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1'),'') cst_linker_nam," +
            "	'' cst_linker_sex," +
            "	value((select lcad.Phone1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1'),'') cst_tel," +
            "	value((select lcad.Mobile1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1'),'') cst_mob," +
            "	value((select lcad.E_Mail1 from LCGrpAddress lcad where lcad.CustomerNo = lgc.AppntNo and lcad.AddressNo = '1'),'') cst_mail," +
            "	value((select lcc.InsuredName || '等' from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only),'') insd_info," +
            "	'' beft_info," +
            "	value((select lcc.InsuredNo from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only),'') insr_targ," +
            "	value((select lcc.InsuredName from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only),'') insr_targ_info," +
            "	lgc.Prem prem," +
            "	lgc.Prem prem_sign," +
            "	lgc.Prem prem_pol," +
            "	lgc.Amnt amount," +
            "	'1' percnt," +
            "	'卡单' bak1, " +
            "	'' bak2, " +
            "	'' bak3, " +
            "	'' bak4, " +
            "CodeName('crs_busstype_typ', lgc.Crs_BussType) bak5," +
            "CodeAlias('crs_busstype_typ', lgc.Crs_BussType) bak6," +
            "	lgc.grpagentidno bak7, " +
            "	(ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) bak8, " +
            "	'I' data_upd_typ," +
            "	ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send," +
            "	'' date_update " +
            " ,ts_fmt(lgc.inputdate, 'yyyymmdd')  inputdate" + //录单时间
            " ,lgc.signdate hold_date"+ //投保日期
            " ,lgc.ProposalGrpContNo proposal_no " +//投保单号
            " ,'1' policy_conti_type " +//业务类型区分
            "From LCGrpCont lgc " +
            " where 1 = 1 " +
            " and lgc.CardFlag = '2' " +
            "	and lgc.AppFlag = '1' " +
            "	and lgc.UWFlag in ('4', '9') " +
            "	and lgc.crs_salechnl in ('01', '02') " +
            " and lgc.SignDate = current date - 1 day " +
            "with ur ";
            int start = 1;
            int nCount = 200;
            while (true)
            {
                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }

                String tempString = tSSRS.encode();
                String[] tempStringArr = tempString.split("\\^");
                tFileOutputStream = new FileOutputStream(mURL +
                        "test000085P001" + mCurrentDate.replaceAll("-", "") +
                        ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new
                        OutputStreamWriter(
                                tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[P001]:Pol_No = '" +
                                          tSSRS.GetText(i, 1) +
                                          "' AND Comp_Cod = '" +
                                          tSSRS.GetText(i, 3) +
                                          "' AND Date_Sign = '" +
                                          tSSRS.GetText(i, 5) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");
                    tArr[8] = tArr[8].substring(1, tArr[8].length() - 1);
                    for (int k = 47; k < 52; k++)
                    {
                        tArr[k] = tArr[k].substring(1, tArr[k].length() - 1);
                    }
                    mBufferedWriter.write("I[P001]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
        } catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
    /**
     * lcc.agentcode
     */

    /**
     * 业务信息表
     */
    private void getP002()
    {
        String tSQL[] = new String[21];
        //承保个险
        getP002SQL0(tSQL);
        //承保团险
        getP002SQL1(tSQL);
        //理赔个险
        getP002SQL2(tSQL);
        //理赔团险
        getP002SQL3(tSQL);
        //个险续期
        getP002SQL4(tSQL);
        //团险续期
        getP002SQL5(tSQL);
        //个险满期
        getP002SQL6(tSQL);
        //团险满期
        getP002SQL7(tSQL);
        //团险保全
        getP002SQL8(tSQL);
        //个险保全
        getP002SQL9(tSQL);
        //老接口保单活动数据
        //承保个险
        getP002SQL10(tSQL);
        //承保团险
        getP002SQL11(tSQL);
        //理赔个险
        getP002SQL12(tSQL);
        //理赔团险
        getP002SQL13(tSQL);
        //个险续期
        getP002SQL14(tSQL);
        //团险续期
        getP002SQL15(tSQL);
        //个险满期
        getP002SQL16(tSQL);
        //团险满期
        getP002SQL17(tSQL);
        //团险保全
        getP002SQL18(tSQL);
        //个险保全
        getP002SQL19(tSQL);
//      卡单类交叉销售新单
        getP002SQL20(tSQL);
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(
                    mURL + "test000085P002" +
                    mCurrentDate.replaceAll("-", "") + ".txt", true);
            for (int m = 0; m < tSQL.length; m++)
            {
                int start = 1;
                int nCount = 200;
                while (true)
                {
                    SSRS tSSRS = new ExeSQL().execSQL(tSQL[m], start, nCount);
                    if (tSSRS.getMaxRow() <= 0)
                    {
                        break;
                    }
                    String tempString = tSSRS.encode();
                    String[] tempStringArr = tempString.split("\\^");

                    tFileOutputStream = new FileOutputStream(
                            mURL + "test000085P002" +
                            mCurrentDate.replaceAll("-", "") + ".txt", true);
                    OutputStreamWriter tOutputStreamWriter = new
                            OutputStreamWriter(
                                    tFileOutputStream, "GBK");
                    mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                    for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                    {
                        mBufferedWriter.write("D[P002]:Pol_No = '" +
                                              tSSRS.GetText(i, 1) +
                                              "' AND Comp_Cod = '" +
                                              tSSRS.GetText(i, 2) +
                                              "' AND Date_Sign = '" +
                                              tSSRS.GetText(i, 4) +
                                              "' AND Act_No = '" +
                                              tSSRS.GetText(i, 5) +
                                              "' AND Date_Act = '" +
                                              tSSRS.GetText(i, 6) +
                                              "' AND Risk_Cod = '" +
                                              tSSRS.GetText(i, 21) + "'");
                        mBufferedWriter.newLine();
                        String t = tempStringArr[i].replaceAll("\\|", "','");
                        String[] tArr = t.split("\\,");
                        tArr[51] = tArr[51].substring(1, tArr[51].length() - 1);
                        for (int k = 22; k < 49; k++)
                        {
                            tArr[k] = tArr[k].substring(1, tArr[k].length() - 1);
                        }
                        mBufferedWriter.write("I[P002]:'");
                        for (int j = 0; j < tArr.length - 1; j++)
                        {
                            mBufferedWriter.write(tArr[j] + ",");
                        }
                        mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                        mBufferedWriter.newLine();
                        mBufferedWriter.flush();
                    }
                    mBufferedWriter.close();
                    start += nCount;
                }
            }
        } catch (IOException ex2)
        {
            ex2.printStackTrace();
        }
    }

    private void getP002SQL9(String[] tSQL)
    {
        //个险保全('CT','XT')
        tSQL[9] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, X.EndorseMentNo||X.PolNo 业务单证ID, " +
                "ts_fmt(date(X.MakeDate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='I') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lbcont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,a.Amnt*(-1) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, " +
                "0 年金给付,X.money*(-1) 退保金, " +
                "-1 承保人次,a.Amnt*(-1) 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lbcont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lbcont where contno=a.contno))," +
                "(select grpagentidno from lccont where contno=a.contno union select grpagentidno from lbcont where contno=a.contno fetch first 1 row only)," +
                "(select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lccont where contno=a.contno union select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lbcont where contno=a.contno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbpol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.PolNo,b.MakeDate,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('CT','XT') " +
                "and b.grpcontno = '00000000000000000000' and b.MakeDate = current date - 1 day " +
                "group by EndorsementNo,FeeOperationType,polno,makedate) as X " +
                "where exists (select 1 from lbcont where contno=a.contno and crs_salechnl in ('01','02')) and a.PolNo = X.PolNo  " +
//               因WT需冲减保费收入，因此需按照如下逻辑处理。原WT逻辑需取消。by gzh 20141126
                "union all " +
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, X.EndorseMentNo||X.PolNo  业务单证ID, " +
                "ts_fmt(date(X.MakeDate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='I') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lbcont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "X.money 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "X.money 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, " +
                "0 年金给付,0 退保金, " +
                "(case FeeOperationType when 'RB' then 1 else 0 end) 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lbcont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lbcont where contno=a.contno))," +
                "(select grpagentidno from lccont where contno=a.contno union select grpagentidno from lbcont where contno=a.contno fetch first 1 row only)," +
                "(select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lccont where contno=a.contno union select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lbcont where contno=a.contno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbpol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.PolNo,b.MakeDate,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('WT') " +
                "and b.grpcontno = '00000000000000000000' and b.MakeDate = current date - 1 day " +
                "group by EndorsementNo,FeeOperationType,polno,makedate) as X " +
                "where exists (select 1 from lbcont where contno=a.contno and crs_salechnl in ('01','02')) and a.PolNo = X.PolNo " +
                //个险保全('FX','RB','CM','BF','TB')
                "union all " +
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, X.EndorseMentNo||X.PolNo  业务单证ID, " +
                "ts_fmt(date(X.MakeDate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='I') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "X.money 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "X.money 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, " +
                "0 年金给付,0 退保金, " +
                "(case FeeOperationType when 'RB' then 1 else 0 end) 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "(select grpagentidno from lccont where contno=a.contno union select grpagentidno from lbcont where contno=a.contno fetch first 1 row only)," +
                "(select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lccont where contno=a.contno union select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lbcont where contno=a.contno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcpol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.PolNo,b.MakeDate,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('FX','RB','CM','BF','TB') " +
                "and b.grpcontno = '00000000000000000000' and b.MakeDate = current date - 1 day " +
                "group by EndorsementNo,FeeOperationType,polno,makedate) as X " +
                "where exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) and a.PolNo = X.PolNo " +
                "with ur ";
    }

    private void getP002SQL8(String[] tSQL)
    {
        //团险保全('LQ','CM','ZB','ZT','WZ','WJ'),增人这里会契约统计冲突，所以这里不再提取
        tSQL[8] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, " +
                "ts_fmt(X.MakeDate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='G') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "X.money 保费收入,0 签单保费,0 保单保费,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, " +
                "0 退保金, " +
                "(case FeeOperationType when 'ZT' then X.Peoples*(-1) else 0 end) 承保人次,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保险金额, " +
                "0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno union select grpagentidno from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)," +
                "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno union select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lbgrpcont lgc where lgc.grpcontno=a.grpcontno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcgrppol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*) peoples,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('LQ','CM','ZB','ZT','WZ','WJ','NI') " +
                "and b.grpcontno <> '00000000000000000000' and b.MakeDate = current date - 1 day " +
                "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X " +
                "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) and a.GrpPolNo = X.GrpPolNo  " +
//              因WT需冲减保费收入，因此需按照如下逻辑处理。原WT逻辑需取消。by gzh 20141126
                "union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, " +
                "ts_fmt(X.MakeDate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='G') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LbGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "X.money 保费收入,0 签单保费,0 保单保费,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, " +
                "0 退保金, " +
                "(case FeeOperationType when 'ZT' then X.Peoples*(-1) else 0 end) 承保人次,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保险金额, " +
                "0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lbgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lbgrpcont where grpcontno=a.grpcontno))," +
                "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno union select grpagentidno from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)," +
                "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno union select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lbgrpcont lgc where lgc.grpcontno=a.grpcontno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbgrppol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*) peoples,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('WT') " +
                "and b.grpcontno <> '00000000000000000000' and (b.MakeDate = current date - 1 day or (exists (select 1 from LOMixAgent where grpcontno = b.grpcontno and makedate =  current date - 1 day))) " +
                "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X " +
                "where exists (select 1 from lbgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) and a.GrpPolNo = X.GrpPolNo  " +
                //团险保全('XT','CT')
                "union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, " +
                "ts_fmt(X.MakeDate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='G') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LBGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费, " +
                "a.Amnt*(-1) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, " +
                "X.money*(-1) 退保金, " +
                "-a.Peoples2 承保人次,a.Amnt*(-1) 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lbgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lbgrpcont where grpcontno=a.grpcontno))," +
                "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno union select grpagentidno from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)," +
                "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno union select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lbgrpcont lgc where lgc.grpcontno=a.grpcontno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbgrppol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*)*(-1) peoples,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('XT','CT') " +
                "and b.grpcontno <> '00000000000000000000' and (b.MakeDate = current date - 1 day or (exists (select 1 from LOMixAgent where grpcontno = b.grpcontno and makedate =  current date - 1 day))) " +
                "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X " +
                "where exists (select 1 from lbgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) and a.GrpPolNo = X.GrpPolNo  "
                //补提数SQL
                +"union all "+
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, " +
                "ts_fmt(X.MakeDate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='G') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "X.money 保费收入,0 签单保费,0 保单保费,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, " +
                "0 退保金, " +
                "(case FeeOperationType when 'ZT' then X.Peoples*(-1) else 0 end) 承保人次,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保险金额, " +
                "0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno union select grpagentidno from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)," +
                "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno union select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lbgrpcont lgc where lgc.grpcontno=a.grpcontno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcgrppol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*) peoples,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('LQ','CM','ZB','ZT','WZ','WJ','NI') " +
                "and b.grpcontno <> '00000000000000000000'  " +
                "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X,LOMixAgent lom " +
                "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) and a.GrpPolNo = X.GrpPolNo  "+ 
                "and x.EndorsementNo = lom.grpcontno and lom.makedate =  current date - 1 day "
                ;
    }

    private void getP002SQL7(String[] tSQL)
    {
        tSQL[7] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, b.EndorseMentNo 业务单证ID, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'MJ' 业务活动代码,'满期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,sum(b.GetMoney)*(-1) 满期给付,0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno union select grpagentidno from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)," +
                "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno union select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lbgrpcont lgc where lgc.grpcontno=a.grpcontno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcgrppol a,ljagetendorse b " +
                "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) and b.Makedate = current date - 1 day  " +
                "and a.GrpPolNo = b.GrpPolNo and b.FeeOperationtype = 'MJ' " +
                "group by a.GrpContno,b.EndorseMentNo,a.ManageCom,a.Riskcode,a.CustomerNo,a.GrpName, " +
                "a.GrpPolNo,a.payintv,a.SaleChnl,b.FeeOperationtype with ur ";
    }

    private void getP002SQL6(String[] tSQL)
    {
        tSQL[6] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, b.SerialNo||b.polno 业务单证ID, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'MJ' 业务活动代码,'满期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,sum(b.GetMoney) 满期给付, " +
                "0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "(select grpagentidno from lccont where contno=a.contno union select grpagentidno from lbcont where contno=a.contno fetch first 1 row only)," +
                "(select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lccont where contno=a.contno union select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lbcont where contno=a.contno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcpol a,ljsgetdraw b,ljaget c " +
                "where a.Conttype = '1' and exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) and c.ConfDate = current date - 1 day  " +
                "and a.PolNo = b.PolNo and b.getnoticeno = c.actugetno " +
                "group by a.contno,b.SerialNo||b.polno,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName, " +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate with ur ";
    }

    private void getP002SQL5(String[] tSQL)
    {
        tSQL[5] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, b.SerialNo 业务单证ID, " +
                "ts_fmt(confdate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'XQ' 业务活动代码,'续期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "sum(b.SumActuPayMoney) 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,sum(b.SumActuPayMoney) 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "b.Paycount 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno union select grpagentidno from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)," +
                "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno union select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lbgrpcont lgc where lgc.grpcontno=a.grpcontno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'2' policy_conti_type " +//业务类型区分
                "from lcgrppol a,ljapaygrp b " +
                "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) and b.Confdate = current date - 1 day  " +
                "and a.GrpPolNo = b.GrpPolNo and b.Paycount > 1 " +
                "group by a.GrpContno,b.SerialNo,a.ManageCom,a.Riskcode,a.CustomerNo,a.GrpName, " +
                "a.GrpPolNo,a.payintv,a.SaleChnl,b.PayCount,b.confdate " +
                " union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, b.SerialNo 业务单证ID, " +
                "ts_fmt(confdate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'XQ' 业务活动代码,'续期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "sum(b.SumActuPayMoney) 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,sum(b.SumActuPayMoney) 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "b.Paycount 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno union select grpagentidno from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)," +
                "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno union select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lbgrpcont lgc where lgc.grpcontno=a.grpcontno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'2' policy_conti_type " +//业务类型区分
                "from lcgrppol a,ljapaygrp b,LOMixAgent lom " +
                "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno " +
                " and lom.SaleChnl in ('01','02') and lom.grpcontno = a.grpcontno and lom.ContType='2'" +
                " and crs_salechnl in ('01','02'))   " +
                " and  lom.Makedate = current date - 1 day " +
                "and a.GrpPolNo = b.GrpPolNo and b.Paycount > 1 " +
                "group by a.GrpContno,b.SerialNo,a.ManageCom,a.Riskcode,a.CustomerNo,a.GrpName, " +
                "a.GrpPolNo,a.payintv,a.SaleChnl,b.PayCount,b.confdate " +
                		"with ur ";
    }

    private void getP002SQL4(String[] tSQL)
    {
        tSQL[4] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, b.SerialNo||b.polno 业务单证ID, " +
                "ts_fmt(b.confdate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'XQ' 业务活动代码,'续期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "sum(b.SumActuPayMoney) 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,sum(b.SumActuPayMoney) 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, " +
                "0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "b.PayCount 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "(select grpagentidno from lccont where contno=a.contno union select grpagentidno from lbcont where contno=a.contno fetch first 1 row only)," +
                "(select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lccont where contno=a.contno union select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lbcont where contno=a.contno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'2' policy_conti_type " +//业务类型区分
                "from lcpol a,ljapayperson b,ljapay c " + //只提取续期 ljapay中duefeetype = '1' by gzh 20110317
                "where a.Conttype = '1' and exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) and b.Confdate = current date - 1 day  " +
                "and a.PolNo = b.PolNo and b.Paycount > 1 " +
                "and b.payno = c.payno and c.duefeetype = '1' " +  //by gzh 20110317 只提取续期
                "group by a.contno,b.SerialNo||b.polno,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName, " +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,b.PayCount,b.confdate with ur ";
    }

    private void getP002SQL3(String[] tSQL)
    {
        tSQL[3] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司'子公司名称," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno),'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt(b.Modifydate,'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称," +
                "'LP' 业务活动代码,'理赔' 业务活动名称," +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称," +
                "a.CustomerNo 客户号,a.GrpName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述," +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称," +
                "0 保费收入,0 签单保费,0 保单保费,0 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "0 承保人次,0 保险金额," +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次," +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额," +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额," +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码," +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次," +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno union select grpagentidno from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)," +
                "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno union select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lbgrpcont lgc where lgc.grpcontno=a.grpcontno fetch first 1 row only)," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcgrppol a,LLClaimDetail b,LLCase c " +
                "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) and b.grppolno = a.grppolno " +
                "and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo " +
                "group by a.grpcontno,b.CaseNo,a.ManageCom,a.riskcode,a.CustomerNo,a.GrpName," +
                "a.GrpPolNo,a.payintv,a.SaleChnl,a.amnt,c.RgtState,b.Modifydate " +
                "union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司'子公司名称," +
                "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno),'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt(b.Modifydate,'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称, " +
                "'LP' 业务活动代码,'理赔' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LBGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno union select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno union select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出, " +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额, " +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次, " +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次, " +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额, " +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码, " +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次, " +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno union select crs_busstype from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno union select crs_busstype from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only))," +
                "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno union select grpagentidno from lbgrpcont where grpcontno=a.grpcontno fetch first 1 row only)," +
                "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno union select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lbgrpcont lgc where lgc.grpcontno=a.grpcontno fetch first 1 row only), " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbgrppol a,LLClaimDetail b,LLCase c " +
                "where 1=1 and (exists (select 1 from lcgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02')) or " +
                "exists (select 1 from lbgrpcont where grpcontno=a.grpcontno and crs_salechnl in ('01','02'))) " +
                " and b.grppolno = a.grppolno  " +
                "and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo  " +
                "group by a.grpcontno,b.CaseNo,a.ManageCom,a.riskcode,a.CustomerNo,a.GrpName, " +
                "a.GrpPolNo,a.payintv,a.SaleChnl,a.amnt,c.RgtState,b.Modifydate with ur ";
    }

    private void getP002SQL2(String[] tSQL)
    {
        tSQL[2] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt(date(b.Modifydate), 'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称," +
                "'LP' 业务活动代码,'理赔' 业务活动名称," +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称," +
                "a.AppntNo 客户号,a.AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,a.InsuredName 承保标的描述," +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称," +
                "0 保费收入,0 签单保费,0 保单保费,0 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "0 承保人次,0 保险金额," +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次," +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额," +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额," +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码," +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次," +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "(select grpagentidno from lccont where contno=a.contno union select grpagentidno from lbcont where contno=a.contno fetch first 1 row only)," +
                "(select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lccont where contno=a.contno union select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lbcont where contno=a.contno fetch first 1 row only)," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcpol a,LLClaimDetail b,LLCase c " +
                "where a.Conttype = '1' and exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) " +
                "and b.polno=a.polno and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo " +
                "group by a.contno,b.CaseNo,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName," +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,c.RgtState,b.Modifydate " +
                "union all " +
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt(date(b.Modifydate), 'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称," +
                "'LP' 业务活动代码,'理赔' 业务活动名称," +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lbcont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno union select crs_salechnl from lbcont where contno=a.contno fetch first 1 row only)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno union select crs_salechnl from lbcont where contno=a.contno fetch first 1 row only)) 交叉销售类型名称," +
                "a.AppntNo 客户号,a.AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,a.InsuredName 承保标的描述," +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称," +
                "0 保费收入,0 签单保费,0 保单保费,0 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "0 承保人次,0 保险金额," +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次," +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额," +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额," +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码," +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次," +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno union select crs_busstype from lbcont where contno=a.contno fetch first 1 row only))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno union select crs_busstype from lbcont where contno=a.contno fetch first 1 row only))," +
                "(select grpagentidno from lccont where contno=a.contno union select grpagentidno from lbcont where contno=a.contno fetch first 1 row only)," +
                "(select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lccont where contno=a.contno union select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lbcont where contno=a.contno fetch first 1 row only)," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbpol a,LLClaimDetail b,LLCase c " +
                "where a.Conttype = '1' and (exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) or " +
                "exists (select 1 from lbcont where contno=a.contno and crs_salechnl in ('01','02')))" +
                "and b.polno=a.polno and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo " +
                "group by a.contno,b.CaseNo,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName," +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,c.RgtState,b.Modifydate with ur ";
    }

    private void getP002SQL1(String[] tSQL)
    {
        tSQL[1] =
                "select GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, grppolno 业务单证ID," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称," +
                "CustomerNo 客户号,GrpName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "peoples2 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno)," +
                "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno)," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'1' policy_conti_type " +//业务类型区分
                "from lcgrppol a " +
                "where  exists (select 1 from lcgrpcont where grpcontno = a.grpcontno " +
                " and crs_salechnl in ('01','02') " +
                " and signdate = current date - 1 day" +
                " and ((cardflag!='2') or (cardflag is null))) " +
                " union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, grppolno 业务单证ID," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID,CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称," +
                "CustomerNo 客户号,GrpName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "peoples2 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "'','','',''," +
                "CodeName('crs_busstype_typ', (select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "CodeAlias('crs_busstype_typ', (select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
                "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno)," +
                "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno)," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'1' policy_conti_type " +//业务类型区分
                "from lcgrppol a ,LOMixAgent lom " +
                "where lom.SaleChnl in ('01','02') and lom.grpcontno = a.grpcontno and lom.ContType='2'" +
                "and lom.Makedate = current date - 1 day and lom.standbyflag2 is null  " +
                "and (((select cardflag from lcgrpcont where grpcontno=a.grpcontno)<>'2') or ((select cardflag from lcgrpcont where grpcontno=a.grpcontno) is null))" +
                "with ur ";
    }

    private void getP002SQL0(String[] tSQL)
    {
        tSQL[0] =
                "select contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(signdate), 'yyyymmdd') 签单日期, polno 业务单证ID," +
                "ts_fmt(date(signdate), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," +
                "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称," +
                "AppntNo 客户号,AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem+supplementaryprem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "1 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "(select grpagentidno from lccont where contno=a.contno)," +
                "(select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lccont where contno=a.contno)," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'1' policy_conti_type " +//业务类型区分
                "from lcpol a " +
                "where Conttype = '1' " +
                " and exists (select 1 from lccont where contno=a.contno and crs_salechnl in ('01','02')) " +
                " and signdate = current date - 1 day " +
                " union all " +
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(signdate), 'yyyymmdd') 签单日期, polno 业务单证ID," +
                "ts_fmt(date(signdate), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID,CodeAlias('crs_salechnl_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称," +
                "AppntNo 客户号,AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem+supplementaryprem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "1 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "'','','',''," +
                "CodeName('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "CodeAlias('crs_busstype_typ',(select crs_busstype from lccont where contno=a.contno))," +
                "(select grpagentidno from lccont where contno=a.contno)," +
                "(select (ts_fmt(CValiDate, 'yyyymmdd')||'_'||crs_busstype||'_'||'085'||'_'||managecom||'_'||contno) from lccont where contno=a.contno)," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'1' policy_conti_type " +//业务类型区分
                "from lcpol a ,LOMixAgent lom " +
                "where a.Conttype = '1' " +
                " and lom.ContType = '1' " +
                " and a.ContNo = lom.ContNo and lom.standbyflag2 is null  " +
                "and lom.SaleChnl in ('01','02') and lom.Makedate = current date - 1 day " +
                "with ur ";
    }
    
    private void getP002SQL20(String[] tSQL)
    {
        tSQL[20] =
        	"select 'Hcd'||(select cardno from licertify where prtno=a.prtno fetch first 1 row only) 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
            "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期," +//卡单销售日期
            " 'Hcd'||(select cardno from licertify where prtno=a.prtno  fetch first 1 row only) 业务单证ID," +
            "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期," +//卡单销售日期
            "'1' 业务类型代码,'承保' 业务类型名称," +
            "'cb' 业务活动代码,'承保' 业务活动名称," +
            "a.ManageCom 归属机构代码,(select Name from LDCom ldc where ldc.ComCode = a.ManageCom) 归属机构名称," +
            "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称," +
            "CodeName('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
            "CodeAlias('crs_salechnl_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称," +
            "CustomerNo 客户号,GrpName 客户名称," +
            "'' 承保标的,'' 承保标的描述," +
            "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
            "prem 保费收入,prem 签单保费,prem 保单保费,amnt 保额," +
            "0,0,0,0,0,0,0,0,0,0," +
            "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
            "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
            "'1' 缴费方式代码,'趸交' 缴费方式名称,1 缴费期次," +
            "'1'," +
            "'','卡单',''," +
            "CodeName('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
            "CodeAlias('crs_busstype_typ',(select crs_busstype from lcgrpcont where grpcontno=a.grpcontno))," +
            "(select grpagentidno from lcgrpcont where grpcontno=a.grpcontno)," +
            "(select (ts_fmt(lgc.CValiDate, 'yyyymmdd')||'_'||lgc.crs_busstype||'_'||'085'||'_'||lgc.managecom||'_'||lgc.grpcontno) from lcgrpcont lgc where lgc.grpcontno=a.grpcontno)," +
            "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
            " ,'4' policy_conti_type " +//业务类型区分
            "from lcgrppol a " +
            "where  exists (select 1 from lcgrpcont where grpcontno = a.grpcontno " +
            " and crs_salechnl in ('01','02') " +
            " and signdate = current date - 1 day" +
            " and cardflag='2') " +
                "with ur ";
    }
    
    private void getP002SQL19(String[] tSQL)
    {
        //个险保全('CT','XT','WT')
        tSQL[19] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, X.EndorseMentNo||X.PolNo 业务单证ID, " +
                "ts_fmt(date(X.MakeDate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='I') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lbcont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,a.Amnt*(-1) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, " +
                "0 年金给付,X.money*(-1) 退保金, " +
                "-1 承保人次,a.Amnt*(-1) 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbpol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.PolNo,b.MakeDate,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('CT','XT') " +
                "and b.grpcontno = '00000000000000000000' and b.MakeDate = current date -1 day " +
                "group by EndorsementNo,FeeOperationType,polno,makedate) as X " +
                "where a.SaleChnl in ('11','12') and a.PolNo = X.PolNo  " +
//              因WT需冲减保费收入，因此需按照如下逻辑处理。原WT逻辑需取消。by gzh 20141126
                "union all " +
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, X.EndorseMentNo||X.PolNo  业务单证ID, " +
                "ts_fmt(date(X.MakeDate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='I') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lbcont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "X.money 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "X.money 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, " +
                "0 年金给付,0 退保金, " +
                "(case FeeOperationType when 'RB' then 1 else 0 end) 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbpol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.PolNo,b.MakeDate,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('WT') " +
                "and b.grpcontno = '00000000000000000000' and b.MakeDate = current date -1 day " +
                "group by EndorsementNo,FeeOperationType,polno,makedate) as X " +
                "where a.SaleChnl in ('11','12') and a.PolNo = X.PolNo " +
                //个险保全('FX','RB','CM','BF','TB')
                "union all " +
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, X.EndorseMentNo||X.PolNo  业务单证ID, " +
                "ts_fmt(date(X.MakeDate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='I') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "X.money 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "X.money 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, " +
                "0 年金给付,0 退保金, " +
                "(case FeeOperationType when 'RB' then 1 else 0 end) 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcpol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.PolNo,b.MakeDate,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('FX','RB','CM','BF','TB','ZB') " +
                "and b.grpcontno = '00000000000000000000' and b.MakeDate = current date -1 day " +
                "group by EndorsementNo,FeeOperationType,polno,makedate) as X " +
                "where a.SaleChnl in ('11','12') and a.PolNo = X.PolNo " +
                "with ur ";
    }

    private void getP002SQL18(String[] tSQL)
    {
        //团险保全('LQ','CM','ZB','ZT'),增人这里会契约统计冲突，所以这里不再提取
        tSQL[18] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, " +
                "ts_fmt(X.MakeDate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='G') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "X.money 保费收入,0 签单保费,0 保单保费,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, " +
                "0 退保金, " +
                "(case FeeOperationType when 'ZT' then X.Peoples*(-1) else 0 end) 承保人次,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保险金额, " +
                "0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcgrppol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*) peoples,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('LQ','CM','ZB','ZT','NI') " +
                "and b.grpcontno <> '00000000000000000000' and b.MakeDate = current date -1 day " +
                "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X " +
                "where a.SaleChnl in ('11','12') and a.GrpPolNo = X.GrpPolNo  " +
//              因WT需冲减保费收入，因此需按照如下逻辑处理。原WT逻辑需取消。by gzh 20141126
                "union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, " +
                "ts_fmt(X.MakeDate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='G') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LbGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "X.money 保费收入,0 签单保费,0 保单保费,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, " +
                "0 退保金, " +
                "(case FeeOperationType when 'ZT' then X.Peoples*(-1) else 0 end) 承保人次,(case FeeOperationType when 'ZT' then  " +
                "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) else 0 end) 保险金额, " +
                "0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbgrppol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*) peoples,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('WT') " +
                "and b.grpcontno <> '00000000000000000000' and b.MakeDate = current date -1 day " +
                "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X " +
                "where a.SaleChnl in ('11','12') and a.GrpPolNo = X.GrpPolNo  " +
                //团险保全('XT','WT','CT')
                "union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, " +
                "ts_fmt(X.MakeDate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType " +
                "and AppObj ='G') 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LBGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费, " +
                "a.Amnt*(-1) 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, " +
                "X.money*(-1) 退保金, " +
                "-a.Peoples2 承保人次,a.Amnt*(-1) 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbgrppol a, " +
                "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*)*(-1) peoples,sum(getmoney) money " +
                "from ljagetendorse b  " +
                "where b.feeoperationtype in ('XT','CT') " +
                "and b.grpcontno <> '00000000000000000000' and b.MakeDate = current date -1 day " +
                "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X " +
                "where a.SaleChnl in ('11','12') and a.GrpPolNo = X.GrpPolNo  with ur ";
    }

    private void getP002SQL17(String[] tSQL)
    {
        tSQL[17] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, b.EndorseMentNo 业务单证ID, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'MJ' 业务活动代码,'满期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,sum(b.GetMoney)*(-1) 满期给付,0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcgrppol a,ljagetendorse b " +
                "where a.SaleChnl in ('11','12') and b.Makedate = current date -1 day  " +
                "and a.GrpPolNo = b.GrpPolNo and b.FeeOperationtype = 'MJ' " +
                "group by a.GrpContno,b.EndorseMentNo,a.ManageCom,a.Riskcode,a.CustomerNo,a.GrpName, " +
                "a.GrpPolNo,a.payintv,a.SaleChnl,b.FeeOperationtype with ur ";
    }

    private void getP002SQL16(String[] tSQL)
    {
        tSQL[16] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, b.SerialNo||b.polno 业务单证ID, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'MJ' 业务活动代码,'满期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,sum(b.GetMoney) 满期给付, " +
                "0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "0 缴费期次, " +
                "'','','','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcpol a,ljsgetdraw b,ljaget c " +
                "where a.Conttype = '1' and a.SaleChnl in ('11','12') and c.ConfDate = current date -1 day  " +
                "and a.PolNo = b.PolNo and b.getnoticeno = c.actugetno " +
                "group by a.contno,b.SerialNo||b.polno,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName, " +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate with ur ";
    }

    private void getP002SQL15(String[] tSQL)
    {
        tSQL[15] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, b.SerialNo 业务单证ID, " +
                "ts_fmt(b.Confdate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'XQ' 业务活动代码,'续期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "sum(b.SumActuPayMoney) 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,sum(b.SumActuPayMoney) 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "b.Paycount 缴费期次, " +
                "'1','','','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'2' policy_conti_type " +//业务类型区分
                "from lcgrppol a,ljapaygrp b " +
                "where a.SaleChnl in ('11','12') and b.Confdate = current date -1 day  " +
                "and a.GrpPolNo = b.GrpPolNo and b.Paycount > 1 " +
                "group by a.GrpContno,b.SerialNo,a.ManageCom,a.Riskcode,a.CustomerNo,a.GrpName, " +
                "a.GrpPolNo,a.payintv,a.SaleChnl,b.PayCount,b.Confdate with ur ";
    }

    private void getP002SQL14(String[] tSQL)
    {
        tSQL[14] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, b.SerialNo||b.polno 业务单证ID, " +
                "ts_fmt(date(b.Confdate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, " +
                "'XQ' 业务活动代码,'续期' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称, " +
                "a.AppntNo 客户号,a.AppntName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, " +
                "sum(b.SumActuPayMoney) 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,sum(b.SumActuPayMoney) 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, " +
                "0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, " +
                "b.PayCount 缴费期次, " +
                "'1','','','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'2' policy_conti_type " +//业务类型区分
                "from lcpol a,ljapayperson b " +
                "where a.Conttype = '1' and a.SaleChnl in ('11','12') and b.Confdate = current date -1 day  " +
                "and a.PolNo = b.PolNo and b.Paycount > 1 " +
                "group by a.contno,b.SerialNo||b.polno,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName, " +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,b.PayCount,b.Confdate with ur ";
    }

    private void getP002SQL13(String[] tSQL)
    {
        tSQL[13] =
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司'子公司名称," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno),'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt(b.Modifydate,'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称," +
                "'LP' 业务活动代码,'理赔' 业务活动名称," +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称," +
                "a.CustomerNo 客户号,a.GrpName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述," +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称," +
                "0 保费收入,0 签单保费,0 保单保费,0 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "0 承保人次,0 保险金额," +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次," +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额," +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额," +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码," +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次," +
                "'','','','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcgrppol a,LLClaimDetail b,LLCase c " +
                "where a.SaleChnl in ('11','12') and b.grppolno = a.grppolno " +
                "and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo " +
                "group by a.grpcontno,b.CaseNo,a.ManageCom,a.riskcode,a.CustomerNo,a.GrpName," +
                "a.GrpPolNo,a.payintv,a.SaleChnl,a.amnt,c.RgtState,b.Modifydate " +
                "union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司'子公司名称," +
                "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno),'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt(b.Modifydate,'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称, " +
                "'LP' 业务活动代码,'理赔' 业务活动名称, " +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, " +
                "(select GrpAgentCom from LBGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, " +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称, " +
                "a.CustomerNo 客户号,a.GrpName 客户名称, " +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, " +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称, " +
                "0 保费收入,0 签单保费,0 保单保费,0 保额, " +
                "0,0,0,0,0,0,0,0,0,0, " +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出, " +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金, " +
                "0 承保人次,0 保险金额, " +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次, " +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次, " +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额, " +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额, " +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码, " +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次, " +
                "'','','','','','','','', " +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbgrppol a,LLClaimDetail b,LLCase c " +
                "where a.SaleChnl in ('11','12') and b.grppolno = a.grppolno  " +
                "and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo  " +
                "group by a.grpcontno,b.CaseNo,a.ManageCom,a.riskcode,a.CustomerNo,a.GrpName, " +
                "a.GrpPolNo,a.payintv,a.SaleChnl,a.amnt,c.RgtState,b.Modifydate with ur ";
    }

    private void getP002SQL12(String[] tSQL)
    {
        tSQL[12] =
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt(date(b.Modifydate), 'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称," +
                "'LP' 业务活动代码,'理赔' 业务活动名称," +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称," +
                "a.AppntNo 客户号,a.AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,a.InsuredName 承保标的描述," +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称," +
                "0 保费收入,0 签单保费,0 保单保费,0 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "0 承保人次,0 保险金额," +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次," +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额," +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额," +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码," +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次," +
                "'','','','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lcpol a,LLClaimDetail b,LLCase c " +
                "where a.Conttype = '1' and a.SaleChnl in ('11','12') " +
                "and b.polno=a.polno and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo " +
                "group by a.contno,b.CaseNo,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName," +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,c.RgtState,b.Modifydate " +
                "union all " +
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期," +
                "b.CaseNo 业务单证ID," +
                "ts_fmt(date(b.Modifydate), 'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称," +
                "'LP' 业务活动代码,'理赔' 业务活动名称," +
                "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lbcont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_typ',a.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',a.SaleChnl) 交叉销售类型名称," +
                "a.AppntNo 客户号,a.AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,a.InsuredName 承保标的描述," +
                "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称," +
                "0 保费收入,0 签单保费,0 保单保费,0 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "0 承保人次,0 保险金额," +
                "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次," +
                "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次," +
                "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额," +
                "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额," +
                "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码," +
                "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次," +
                "'','','','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'4' policy_conti_type " +//业务类型区分
                "from lbpol a,LLClaimDetail b,LLCase c " +
                "where a.Conttype = '1' and a.SaleChnl in ('11','12') " +
                "and b.polno=a.polno and b.Modifydate = current date - 1 day and c.CaseNo = b.CaseNo " +
                "group by a.contno,b.CaseNo,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName," +
                "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,c.RgtState,b.Modifydate with ur ";
    }

    private void getP002SQL11(String[] tSQL)
    {
        tSQL[11] =
                "select GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, grppolno 业务单证ID," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_typ',SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',SaleChnl) 交叉销售类型名称," +
                "CustomerNo 客户号,GrpName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "peoples2 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "'1','','','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'1' policy_conti_type " +//业务类型区分
                "from lcgrppol a " +
                "where SaleChnl in ('11','12') and exists (select 1 from lcgrpcont where grpcontno = a.grpcontno " +
                "and signdate = current date -1 day) " +
                " union all " +
                "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, grppolno 业务单证ID," +
                "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_typ',lom.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',lom.SaleChnl) 交叉销售类型名称," +
                "CustomerNo 客户号,GrpName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = (select InsuredNo " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName " +
                "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "peoples2 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "'1','','','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'1' policy_conti_type " +//业务类型区分
                "from lcgrppol a ,LOMixAgent lom " +
                "where lom.SaleChnl in ('11','12') and lom.standbyflag2 is null  and lom.grpcontno = a.grpcontno and lom.ContType='2'" +
                "and lom.Makedate = current date -1 day " +
                "with ur ";
    }

    private void getP002SQL10(String[] tSQL)
    {
        tSQL[10] =
                "select contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(signdate), 'yyyymmdd') 签单日期, polno 业务单证ID," +
                "ts_fmt(date(signdate), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_typ',SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',SaleChnl) 交叉销售类型名称," +
                "AppntNo 客户号,AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "1 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "'1','','','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''" +
                " ,'1' policy_conti_type " +//业务类型区分
                "from lcpol a " +
                "where Conttype = '1' " +
                "and SaleChnl in ('11','12') and signdate = current date -1 day " +
                " union all " +
                "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, " +
                "ts_fmt(date(signdate), 'yyyymmdd') 签单日期, polno 业务单证ID," +
                "ts_fmt(date(signdate), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称," +
                "'QD' 业务活动代码,'签单' 业务活动名称," +
                "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称," +
                "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称," +
                "CodeName('crs_typ',lom.SaleChnl) 交叉销售类型ID,CodeAlias('crs_typ',lom.SaleChnl) 交叉销售类型名称," +
                "AppntNo 客户号,AppntName 客户名称," +
                "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述," +
                "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称," +
                "prem 保费收入,0 签单保费,0 保单保费,amnt 保额," +
                "0,0,0,0,0,0,0,0,0,0," +
                "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金," +
                "1 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额," +
                "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次," +
                "'1','','','','','','',''," +
                "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' " +
                " ,'1' policy_conti_type " +//业务类型区分
                "from lcpol a ,LOMixAgent lom " +
                "where a.Conttype = '1' " +
                " and lom.ContType = '1' " +
                " and a.ContNo = lom.ContNo and lom.standbyflag2 is null  " +
                "and lom.SaleChnl in ('11','12') and lom.Makedate = current date -1 day " +
                "with ur ";
    }
    /**
     * 保单法人投保人表
     */
    private void getCC01()
    {
        String tSQL = " select "
                + " lgc.GrpContNo pol_no, "
                + " (select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.GrpContNo = lgc.GrpContNo) pol_no_orig, "
                + " '000085' comp_cod, "
                + " '中国人民健康保险股份有限公司' comp_cod, "
                + " ts_fmt(lgc.SignDate, 'yyyymmdd') date_sign, "
                + " lga.CustomerNo cst_id, "
                + " lgc.ManageCom blng_org_cod, "
                + " (select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) blng_org_nam, "
                + " lga.Name cst_grpfullcnam, "
                + " lga.Name cst_grpsimpcnam, "
                + " (select ldg.GrpEnglishName from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_grpenam, "
                + " (select ldg.OrganComCode from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_orgcomcod, "
                + " (select ldg.SociRegCode from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_sociregcod, "
                + " '' cst_tax_code, "
                + " '' cst_reg_cod, "
                + " (select CodeName('crs_cst_bsnestyp_cod', ldg.BusinessType) from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_bsnestyp_cod, "
                + " (select CodeAlias('crs_cst_bsnestyp_cod', ldg.BusinessType) from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_bsnestyp_nam, "
                + " (select CodeName('crs_cst_grpnatr_cod', ldg.GrpNature) from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_grpnatr_cod, "
                + " (select CodeAlias('crs_cst_grpnatr_cod', ldg.GrpNature) from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_grpnatr_nam, "
                + " (select replace(lcgad.GrpAddress,',','-') from LCGrpAddress lcgad where lcgad.CustomerNo = lga.CustomerNo and lcgad.AddressNo = lga.AddressNo) cst_addr, "
                + " (select lcgad.GrpZipCode from LCGrpAddress lcgad where lcgad.CustomerNo = lga.CustomerNo and lcgad.AddressNo = lga.AddressNo) cst_zip, "
                + " (select ldg.RgtMoney from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_rgtmony, "
                + " (select ldg.Asset from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_asset, "
                + " (select ldg.NetProfitRate from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_netrat, "
                + " (select ldg.MainBussiness from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_mainbus, "
                + " (select ldg.ComAera from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_comaera, "
                + " (select ldg.Fax from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_fax, "
                + " (select ldg.Phone from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_phon, "
                + " (select ldg.EMail from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_email, "
                + " (select ts_fmt(ldg.FoundDate, 'yyyymmdd') from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_fnddat, "
                + " (select ldg.BankCode from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_claim_bnkcod, "
                + " (select ldg.BankCode from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_claim_bnknam, "
                + " (select ldg.BankAccNo from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_claim_cnt, "
                + " (select ldg.SubCompanyFlag from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_subflag, "
                + " (select ldg.SupCustoemrNo from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_father_cstid, "
                + " (select ldg.LevelCode from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_lvl_discb, "
                + " (select ldg.Peoples from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_peoplenum, "
                + " (select ldg.OnWorkPeoples from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_onwrknum, "
                + " (select ldg.OffWorkPeoples from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_offwrknum, "
                + " (select ldg.OtherPeoples from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) cst_othernum, "
                + " '' cst_url, "
                + " (select ldg.Remark from LDGrp ldg where ldg.CustomerNo = lga.CustomerNo) comments, "
                + " '' bak1, "
                + " '' bak2, "
                + " '' bak3, "
                + " '' bak4, "
                + " '' bak5, "
                + " '' bak6, "
                + " '' bak7, "
                + " '' bak8, "
                + " ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send, "
                + " '' date_update "
                + "from LCGrpAppnt lga "
                + "inner join LCGrpCont lgc on lga.GrpContNo = lgc.GrpContNo "
                + "where 1 = 1 "
                + " and lgc.AppFlag = '1' "
                + " and lgc.UWFlag in ('4', '9') "
//                + " and lgc.Crs_SaleChnl in ('01', '02') "
                + " and exists( select 1 From t_policy_main where Pol_No = lgc.grpcontno)  " // add by fuxin
                + " and ( lga.ModifyDate  = current date -1 day  or lgc.SignDate  = current date -1 day ) "
//                + " and lga.ModifyDate  between '2010-6-1' and current date  " 
                + "   with ur  ";

        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL
                    + "test000085CC01" + mCurrentDate.replaceAll("-", "") + ".txt",
                    true);
            int start = 1;
            int nCount = 200;

            RSWrapper rswrapper = new RSWrapper();
            rswrapper.prepareData(null, tSQL);
            SSRS tSSRS = null;

            do
            //            while (true)
            {
                //                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                tSSRS = rswrapper.getSSRS();

                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }

                //                String tempString = tSSRS.encode();
                String tempString = rswrapper.encode();

                String[] tempStringArr = tempString.split("\\^");

                tFileOutputStream = new FileOutputStream(mURL + "test000085CC01"
                        + mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[CC01]:pol_no = '"
                            + tSSRS.GetText(i, 1) + "' AND comp_cod = '"
                            + tSSRS.GetText(i, 3) + "' AND date_sign = '"
                            + tSSRS.GetText(i, 5) + "' AND cst_id = '"
                            + tSSRS.GetText(i, 6) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");
                    for (int k = 21; k < 24; k++)
                    {
                        tArr[k] = tArr[k].substring(1, tArr[k].length() - 1);
                    }
                    for (int k = 36; k < 40; k++)
                    {
                        tArr[k] = tArr[k].substring(1, tArr[k].length() - 1);
                    }
                    mBufferedWriter.write("I[CC01]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                tOutputStreamWriter.close();
                start += nCount;
            }
            while (tSSRS.getMaxRow() > 0);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 保单个人投保人表
     */
    private void getCC02()
    {
        String tSQL = " select "
                + " lcc.ContNo pol_no, "
                + " (select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo) pol_no_orig, "
                + " '000085' comp_cod, "
                + " '中国人民健康保险股份有限公司' comp_cod, "
                + " ts_fmt(lcc.SignDate, 'yyyymmdd') date_sign, "
                + " lca.AppntNo cst_id, "
                + " lcc.ManageCom blng_org_cod, "
                + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) blng_org_nam, "
                + " lca.AppntName cst_cnam, "
                + " lca.EnglishName cst_enam, "
                + " CodeName('crs_cst_linker_sex', lca.AppntSex) cst_sex_cod, "
                + " CodeAlias('crs_cst_linker_sex', lca.AppntSex) cst_sex_nam, "
                + " ts_fmt(lca.AppntBirthday, 'yyyymmdd') cst_birthdy, "
                + " CodeName('crs_cst_idtyp', lca.IDType) cst_idtyp_cod, "
                + " CodeAlias('crs_cst_idtyp', lca.IDType) cst_idtyp_nam, "
                + " lca.IDNo cst_idno, "
                + " CodeName('crs_marrystat_cod', lca.Marriage) marrystat_cod, "
                + " CodeAlias('crs_marrystat_cod', lca.Marriage) marrystat_nam, "
                + " ts_fmt(lca.MarriageDate, 'yyyymmdd') marry_dat, "
                + " '' spouse_nam, "
                + " CodeName('crs_edu_cod', lca.Degree) edu_cod, "
                + " CodeAlias('crs_edu_cod', lca.Degree) edu_nam, "
                + " lca.Nationality nation_cod, "
                + " CodeName('nationality', lca.Nationality) nation_nam, "
                + " CodeName('nativeplace', lca.NativePlace) nationality, "
                + " CodeName('crs_cst_ocpation_cod', lca.OccupationCode) ocpation_cod, "
                + " CodeAlias('crs_cst_ocpation_cod', lca.OccupationCode) ocpation_nam, "
                + " lca.SmokeFlag smk_flg, "
                + " (select replace(lcadd.HomeAddress,',','-') from LCAddress lcadd where lcadd.CustomerNo = lca.AppntNo and lcadd.AddressNo = lca.AddressNo) hom_caddr, "
                + " '' hom_eaddr, "
                + " (select lcadd.HomeZipCode from LCAddress lcadd where lcadd.CustomerNo = lca.AppntNo and lcadd.AddressNo = lca.AddressNo) hom_zip, "
                + " (select lcadd.HomePhone from LCAddress lcadd where lcadd.CustomerNo = lca.AppntNo and lcadd.AddressNo = lca.AddressNo) hom_tel, "
                + " (select lcadd.GrpName from LCAddress lcadd where lcadd.CustomerNo = lca.AppntNo and lcadd.AddressNo = lca.AddressNo) wrk_nam, "
                + " (select replace(lcadd.CompanyAddress,',','-') from LCAddress lcadd where lcadd.CustomerNo = lca.AppntNo and lcadd.AddressNo = lca.AddressNo) wrk_addr, "
                + " (select lcadd.CompanyZipCode from LCAddress lcadd where lcadd.CustomerNo = lca.AppntNo and lcadd.AddressNo = lca.AddressNo) wrk_zip, "
                + " (select lcadd.CompanyPhone from LCAddress lcadd where lcadd.CustomerNo = lca.AppntNo and lcadd.AddressNo = lca.AddressNo) wrk_tel, "
                + " (select lcadd.CompanyFax from LCAddress lcadd where lcadd.CustomerNo = lca.AppntNo and lcadd.AddressNo = lca.AddressNo) cst_fax, "
                + " (select lcadd.Mobile from LCAddress lcadd where lcadd.CustomerNo = lca.AppntNo and lcadd.AddressNo = lca.AddressNo) cst_mob, "
                + " (select lcadd.EMail from LCAddress lcadd where lcadd.CustomerNo = lca.AppntNo and lcadd.AddressNo = lca.AddressNo) cst_email, "
                + " '' bak1, "
                + " '' bak2, "
                + " '' bak3, "
                + " '' bak4, "
                + " '' bak5, "
                + " '' bak6, "
                + " '' bak7, "
                + " '' bak8, "
                + " ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send, "
                + " '' date_update "
                + " from LCAppnt lca "
                + " inner join LCCont lcc on lca.ContNo = lcc.ContNo "
                + " where 1 = 1 "
                + " and lcc.ContType = '1' "
                + " and lcc.AppFlag = '1' "
                + " and lcc.UWFlag in ('4', '9') "
//                + " and lcc.Crs_SaleChnl in ('01', '02') "
                + " and exists( select 1 From t_policy_main where Pol_No = lcc.contno)  " // add by fuxin
                + " and (lca.ModifyDate  = current date -1 day or lcc.SignDate  = current date -1 day ) " 
//                + " and lca.ModifyDate  between '2010-6-1' and current date  "
                + "   with ur  ";

        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL
                    + "test000085CC02" + mCurrentDate.replaceAll("-", "") + ".txt",
                    true);
            int start = 1;
            int nCount = 200;

            RSWrapper rswrapper = new RSWrapper();
            rswrapper.prepareData(null, tSQL);
            SSRS tSSRS = null;

            do
            //            while (true)
            {
                //                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                tSSRS = rswrapper.getSSRS();

                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }

                //                String tempString = tSSRS.encode();
                String tempString = rswrapper.encode();

                String[] tempStringArr = tempString.split("\\^");

                tFileOutputStream = new FileOutputStream(mURL + "test000085CC02"
                        + mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[CC02]:pol_no = '"
                            + tSSRS.GetText(i, 1) + "' AND comp_cod = '"
                            + tSSRS.GetText(i, 3) + "' AND date_sign = '"
                            + tSSRS.GetText(i, 5) + "' AND cst_id = '"
                            + tSSRS.GetText(i, 6) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");
                    mBufferedWriter.write("I[CC02]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
            while (tSSRS.getMaxRow() > 0);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 保单标的表
     */
    private void getCC03()
    {
        String tSQL = " select "
                + " lcc.ContNo pol_no, "
                + " (select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo) pol_no_orig, "
                + " '000085' comp_cod, "
                + " '中国人民健康保险股份有限公司' comp_cod, "
                + " ts_fmt(lcc.SignDate, 'yyyymmdd') date_sign, "
                + " lcc.InsuredNo obj_no, "
                + " (lcc.InsuredName || '等' || trim(char(lcc.Peoples)) || '人') obj_nam, "
                + " '3' obj_typ, "
                + " '被保人' obj_typnam, "
                + " lcc.InsuredIDType obj_id_typ, "
                + " lcc.InsuredIDNo obj_id, "
                + " '' use_atr_nam, "
                +"'' bak1," 
                +"'' bak2," 
                +"'' bak3," 
                +"'' bak4," 
                +"'' bak5," 
                +"'' bak6," 
                +"'' bak7," 
                +"'' bak8," 
                + " ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send, "
                + " '' date_update "
                + " from LCCont lcc "
                + " where 1 = 1 "
                + " and lcc.ContType = '1' "
                + " and lcc.AppFlag = '1' "
                + " and lcc.UWFlag in ('4', '9') "
//                + " and lcc.Crs_SaleChnl in ('11', '12') "
                + " and lcc.ModifyDate = current date - 1 day "
//                + " and lcc.ModifyDate between '2010-6-1' and current date "
                + " and exists( select 1 From t_policy_main where Pol_No = lcc.contno)  " // add by fuxin
                + " union all "
                + " select "
                + " lgc.GrpContNo pol_no, "
                + " (select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.GrpContNo = lgc.GrpContNo) pol_no_orig, "
                + " '000085' comp_cod, "
                + " '中国人民健康保险股份有限公司' comp_cod, "
                + " ts_fmt(lgc.SignDate, 'yyyymmdd') date_sign, "
                + " (select insuredno from lcpol where grpcontno=lgc.GrpContNo fetch first 1 row only) obj_no, "
                + " (select lcc.InsuredName || '等' || trim(char(lgc.Peoples2)) || '人' from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) obj_nam, "
                + " '3' obj_typ, "
                + " '被保人' obj_typnam, "
                + " (select lcc.InsuredIDType from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) obj_id_typ, "
                + " (select lcc.InsuredIDNo from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) obj_id, "
                + " '' use_atr_nam, "
                + " '' bak1, "
                + " '' bak2, "
                + " '' bak3, "
                + " '' bak4, "
                + " '' bak5, "
                + " '' bak6, "
                + " '' bak7, "
                + " '' bak8, "
                + " ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send, "
                + " '' date_update "
                + " from LCGrpCont lgc "
                + " where 1 = 1 "
                + " and lgc.AppFlag = '1' "
                + " and lgc.UWFlag in ('4', '9') "
//                + " and lgc.Crs_SaleChnl in ('01', '02') "
                + " and exists( select 1 From t_policy_main where Pol_No = lgc.grpcontno)  " // add by fuxin
                + " and lgc.ModifyDate  = current date -1 day  " 
//                + " and lgc.ModifyDate between '2010-6-1' and current date  "
                + "   with ur  ";
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL
                    + "test000085CC03" + mCurrentDate.replaceAll("-", "") + ".txt",
                    true);
            int start = 1;
            int nCount = 200;

            RSWrapper rswrapper = new RSWrapper();
            rswrapper.prepareData(null, tSQL);
            SSRS tSSRS = null;

            do
            //            while (true)
            {
                //                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                tSSRS = rswrapper.getSSRS();

                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }

                //                String tempString = tSSRS.encode();
                String tempString = rswrapper.encode();

                String[] tempStringArr = tempString.split("\\^");

                tFileOutputStream = new FileOutputStream(mURL + "test000085CC03"
                        + mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[CC03]:pol_no = '"
                            + tSSRS.GetText(i, 1) + "' AND comp_cod = '"
                            + tSSRS.GetText(i, 3) + "' AND date_sign = '"
                            + tSSRS.GetText(i, 5) + "' AND obj_no = '"
                            + tSSRS.GetText(i, 6) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");
                    mBufferedWriter.write("I[CC03]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
            while (tSSRS.getMaxRow() > 0);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 保单联系人表
     */
    private void getCC04()
    {
        String tSQL = 
        	//modify by fuxin 个单没有联系人，根据集团要求不取个单的联系人
//        		" select "
//                + " lcc.ContNo pol_no, "
//                + " (select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo) pol_no_orig, "
//                + " '000085' comp_cod, "
//                + " '中国人民健康保险股份有限公司' comp_cod, "
//                + " ts_fmt(lcc.SignDate, 'yyyymmdd') date_sign, "
//                + " lcc.ManageCom blng_org_cod, "
//                + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) blng_org_nam, "
//                + " lci.InsuredNo linker_no, "
//                + " '' Act_id, "
//                + " lci.InsuredNo cst_id, "
//                + " lci.Name psn_cnam, "
//                + " lci.EnglishName psn_enam, "
//                + " '01' rltion_cod, "
//                + " '被保人' rltion_nam, "
//                + " CodeName('relation', lci.relationtoappnt) rltion_rol, "
//                + " CodeName('crs_cst_linker_sex', lci.Sex) psn_sex_cod, "
//                + " CodeAlias('crs_cst_linker_sex', lci.Sex) psn_sex_nam, "
//                + " ts_fmt(lci.Birthday, 'yyyymmdd') psn_birthdy, "
//                + " CodeName('crs_cst_idtyp', lci.IDType) psn_idtyp_cod, "
//                + " CodeAlias('crs_cst_idtyp', lci.IDType) psn_idtyp_nam, "
//                + " lci.IDNo psn_idno, "
//                + " CodeName('crs_marrystat_cod', lci.Marriage) marrystat_cod, "
//                + " CodeAlias('crs_marrystat_cod', lci.Marriage) marrystat_nam, "
//                + " ts_fmt(lci.MarriageDate, 'yyyymmdd') marry_dat, "
//                + " '' spouse_nam, "
//                + " CodeName('crs_edu_cod', lci.Degree) edu_cod, "
//                + " CodeAlias('crs_edu_cod', lci.Degree) edu_nam, "
//                + " lci.Nationality nation_cod, "
//                + " CodeName('nationality', lci.Nationality) nation_nam, "
//                + " CodeName('nativeplace', lci.NativePlace) nationality, "
//                + " CodeName('crs_cst_ocpation_cod', lci.OccupationCode) ocpation_cod, "
//                + " CodeAlias('crs_cst_ocpation_cod', lci.OccupationCode) ocpation_nam, "
//                + " lci.SmokeFlag smk_flg, "
//                + " (select replace(lcadd.HomeAddress,',','-') from LCAddress lcadd where lcadd.CustomerNo = lci.InsuredNo and lcadd.AddressNo = lci.AddressNo) hom_caddr, "
//                + " '' hom_eaddr, "
//                + " (select lcadd.HomeZipCode from LCAddress lcadd where lcadd.CustomerNo = lci.InsuredNo and lcadd.AddressNo = lci.AddressNo) hom_zip, "
//                + " (select lcadd.HomePhone from LCAddress lcadd where lcadd.CustomerNo = lci.InsuredNo and lcadd.AddressNo = lci.AddressNo) hom_tel, "
//                + " (select lcadd.GrpName from LCAddress lcadd where lcadd.CustomerNo = lci.InsuredNo and lcadd.AddressNo = lci.AddressNo) wrk_nam, "
//                + " (select replace(lcadd.CompanyAddress,',','-') from LCAddress lcadd where lcadd.CustomerNo = lci.InsuredNo and lcadd.AddressNo = lci.AddressNo) wrk_addr, "
//                + " (select lcadd.CompanyZipCode from LCAddress lcadd where lcadd.CustomerNo = lci.InsuredNo and lcadd.AddressNo = lci.AddressNo) wrk_zip, "
//                + " (select lcadd.CompanyPhone from LCAddress lcadd where lcadd.CustomerNo = lci.InsuredNo and lcadd.AddressNo = lci.AddressNo) wrk_tel, "
//                + " (select lcadd.CompanyFax from LCAddress lcadd where lcadd.CustomerNo = lci.InsuredNo and lcadd.AddressNo = lci.AddressNo) cst_fax, "
//                + " (select lcadd.Mobile from LCAddress lcadd where lcadd.CustomerNo = lci.InsuredNo and lcadd.AddressNo = lci.AddressNo) cst_mob, "
//                + " (select lcadd.EMail from LCAddress lcadd where lcadd.CustomerNo = lci.InsuredNo and lcadd.AddressNo = lci.AddressNo) cst_email, "
//                + " '' bak1, "
//                + " '' bak2, "
//                + " '' bak3, "
//                + " '' bak4, "
//                + " '' bak5, "
//                + " '' bak6, "
//                + " '' bak7, "
//                + " '' bak8, "
//                + " ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send, "
//                + " '' date_update "
//                + " from LCInsured lci "
//                + " inner join LCCont lcc on lci.ContNo = lcc.ContNo "
//                + " where 1 = 1 "
//                + " and lcc.ContType = '1' "
//                + " and lcc.AppFlag = '1' "
//                + " and lcc.UWFlag in ('4', '9') "
//                + " and exists( select 1 From t_policy_main where Pol_No = lcc.contno)  " // add by fuxin
////                + " and lcc.Crs_SaleChnl in ('01', '02') "
////                + " and lcc.ModifyDate  = current date -1 day  "
//                + " and lcc.ModifyDate  between '2010-6-1' and current date  "
//                + " union all "
                 " select "
                + " lgc.GrpContNo pol_no, "
                + " (select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.GrpContNo = lgc.GrpContNo) pol_no_orig, "
                + " '000085' comp_cod, "
                + " '中国人民健康保险股份有限公司' comp_cod, "
                + " ts_fmt(lgc.SignDate, 'yyyymmdd') date_sign, "
                + " lgc.ManageCom blng_org_cod, "
                + " (select ldc.Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) blng_org_nam, "
                + " lgc.AppntNo linker_no, "
                + " '' Act_id, "
                + " lgc.AppntNo cst_id, "
                + " (select lcg.LinkMan1 from LCGrpAddress lcg where lcg.CustomerNo = lgc.AppntNo and lcg.AddressNo = lgc.AddressNo ) psn_cnam, "
                + " '' psn_enam, "
                + " '02' rltion_cod, "
                + " '联系人' rltion_nam, "
                + " '' rltion_rol, "
                + " '' psn_sex_cod, "
                + " '' psn_sex_nam, "
                + " '' psn_birthdy, "
                + " '' psn_idtyp_cod, "
                + " '' psn_idtyp_nam, "
                + " '' psn_idno, "
                + " '' marrystat_cod, "
                + " '' marrystat_nam, "
                + " '' marry_dat, "
                + " '' spouse_nam, "
                + " '' edu_cod, "
                + " '' edu_nam, "
                + " '' nation_cod, "
                + " '' nation_nam, "
                + " '' nationality, "
                + " '' ocpation_cod, "
                + " '' ocpation_nam, "
                + " '' smk_flg, "
                + " '' hom_caddr, "
                + " '' hom_eaddr, "
                + " '' hom_zip, "
                + " '' hom_tel, "
                + " lgc.GrpName wrk_nam, "
                + " replace(lga.PostalAddress,',','-') wrk_addr, "
                + " lga.ZipCode wrk_zip, "
                + " lgc.Phone wrk_tel, "
                + " lgc.Fax cst_fax, "
                + " '' cst_mob, "
                + " lgc.EMail cst_email, "
                + " '' bak1, "
                + " '' bak2, "
                + " '' bak3, "
                + " '' bak4, "
                + " '' bak5, "
                + " '' bak6, "
                + " '' bak7, "
                + " '' bak8, "
                + " ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send, "
                + " '' date_update "
                + " from LCGrpAppnt lga "
                + " inner join LCGrpCont lgc on lga.GrpContNo = lgc.GrpContNo"
                + " where 1 = 1 "
                + " and lgc.AppFlag = '1' "
                + " and lgc.UWFlag in ('4', '9') "
//                + " and lgc.Crs_SaleChnl in ('01', '02') "
                + " and exists( select 1 From t_policy_main where Pol_No = lgc.grpcontno)  " // add by fuxin
                + " and lgc.ModifyDate  = current date -1 day "
//                + " and lgc.ModifyDate  between '2010-6-1' and current date" 
                + "   with ur  ";

        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL
                    + "test000085CC04" + mCurrentDate.replaceAll("-", "") + ".txt",
                    true);
            int start = 1;
            int nCount = 200;

            RSWrapper rswrapper = new RSWrapper();
            rswrapper.prepareData(null, tSQL);
            SSRS tSSRS = null;

            do
            //            while (true)
            {
                //                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                tSSRS = rswrapper.getSSRS();

                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }

                //                String tempString = tSSRS.encode();
                String tempString = rswrapper.encode();

                String[] tempStringArr = tempString.split("\\^");

                tFileOutputStream = new FileOutputStream(mURL + "test000085CC04"
                        + mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[CC04]:pol_no = '"
                            + tSSRS.GetText(i, 1) + "' AND comp_cod = '"
                            + tSSRS.GetText(i, 3) + "' AND date_sign = '"
                            + tSSRS.GetText(i, 5) + "' AND linker_no = "
                            + tSSRS.GetText(i, 8) + " AND cst_id = '"
                            + tSSRS.GetText(i, 10) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");

                    tArr[7] = tArr[7].substring(1, tArr[7].length() - 1);

                    mBufferedWriter.write("I[CC04]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
            while (tSSRS.getMaxRow() > 0);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 保单个人被保人表
     */
    private void getCC05()
    {
        String tSQL =
        		" select Lcc.ContNo pol_no, "
        		+" (select max(lcrnsl.NewContNo) "
	        	+"    from LCRNewStateLog lcrnsl "
	        	+"   where lcrnsl.ContNo = Lcc.ContNo) pol_no_orig, "
	        	+" lci.insuredno pol_subno, "
	        	+" '000085' comp_cod, "
	        	+" '中国人民健康保险股份有限公司' comp_cod, "
	        	+" ts_fmt(Lcc.SignDate, 'yyyymmdd') date_sign, "
	        	+" Lcc.AppntNo cst_id, "
	        	+" Lcc.ManageCom blng_org_cod,"
	        	+" (select ldc.Name from LDCom ldc where ldc.ComCode = Lcc.ManageCom) blng_org_nam, "
	        	+" lci.Name cst_cnam, "
	        	+" lci.EnglishName cst_enam, "
	        	+" CodeName('crs_cst_linker_sex', lci.Sex) cst_sex_cod, "
	        	+" CodeAlias('crs_cst_linker_sex', lci.Sex) cst_sex_nam, "
	        	+" ts_fmt(lci.Birthday, 'yyyymmdd') cst_birthdy, "
	        	+" CodeName('crs_cst_idtyp', lci.IDType) cst_idtyp_cod, "
	        	+" CodeAlias('crs_cst_idtyp', lci.IDType) cst_idtyp_nam, "
	        	+" lci.IDNo cst_idno, "
	        	+" CodeName('crs_marrystat_cod', lci.Marriage) marrystat_cod, "
	        	+" CodeAlias('crs_marrystat_cod', lci.Marriage) marrystat_nam, "
	        	+" ts_fmt(lci.MarriageDate, 'yyyymmdd') marry_dat, "
	        	+" '' spouse_nam, "
	        	+" CodeName('crs_edu_cod', lci.Degree) edu_cod, "
	        	+" CodeAlias('crs_edu_cod', lci.Degree) edu_nam, "
	        	+" lci.Nationality nation_cod, "
	        	+" CodeName('nationality', lci.Nationality) nation_nam, "
	        	+" CodeName('nativeplace', lci.NativePlace) nationality, "
	        	+" CodeName('crs_cst_ocpation_cod', lci.OccupationCode) ocpation_cod, "
	        	+" CodeAlias('crs_cst_ocpation_cod', lci.OccupationCode) ocpation_nam, "
	        	+" lci.SmokeFlag smk_flg, "
	        	+" replace(lcadd.HomeAddress, ',', '-') hom_caddr, "
	        	+" '' hom_eaddr, "
	        	+" (lcadd.HomeZipCode) hom_zip, "
	        	+" (lcadd.HomePhone) hom_tel, "
	        	+" (lcadd.GrpName) wrk_nam, "
	        	+" (replace(lcadd.CompanyAddress, ',', '-')) wrk_addr, "
	        	+" (lcadd.CompanyZipCode) wrk_zip, "
	        	+" (lcadd.CompanyPhone) wrk_tel, "
	        	+" (lcadd.CompanyFax) cst_fax, "
	        	+" (lcadd.Mobile) cst_mob, "
	        	+" (lcadd.EMail) cst_email, "
	        	+" '' bak1, "
	        	+" '' bak2, "
	        	+" '' bak3, "
	        	+" '' bak4, "
	        	+" '' bak5, "
	        	+" '' bak6, "
	        	+" '' bak7, "
	        	+" '' bak8, "
	        	+" ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send, "
	        	+" '' date_update "
	        	+" from LCInsured lci "
	        	+" inner join LCCont Lcc on lci.ContNo = Lcc.ContNo "
	        	+" inner join LCAddress lcadd on lcadd.addressno = lci.addressno "
	        	+" and lcadd.customerno = lci.insuredno "
	        	+" where 1 = 1 "
	        	+" and ContType ='1' "
	        	+" and Lcc.AppFlag = '1' "
	        	+" and Lcc.UWFlag in ('4', '9') "
	        	+" and Lcc.managecom = lci.managecom "
	        	+" and exists (select 1 From t_policy_main where Pol_No = Lcc.ContNo) "
//	        	+" and lci.ModifyDate between '2010-6-1' and current date  "
	        	+" and lci.ModifyDate = current date -1 day  "
	        	+" union all "
	        	+ " select lgc.GrpContNo pol_no, "
                + "       (select max(lcrnsl.NewContNo) "
                + "          from LCRNewStateLog lcrnsl "
                + "         where lcrnsl.GrpContNo = lgc.GrpContNo) pol_no_orig, "
                + "       lci.ContNo pol_subno, "
                + "       '000085' comp_cod, "
                + "       '中国人民健康保险股份有限公司' comp_cod, "
                + "       ts_fmt(lgc.SignDate, 'yyyymmdd') date_sign, "
                + "       lgc.AppntNo cst_id, "
                + "       lgc.ManageCom blng_org_cod, "
                + "       (select ldc.Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) blng_org_nam, "
                + "       lci.Name cst_cnam, "
                + "       lci.EnglishName cst_enam, "
                + "       CodeName('crs_cst_linker_sex', lci.Sex) cst_sex_cod, "
                + "       CodeAlias('crs_cst_linker_sex', lci.Sex) cst_sex_nam, "
                + "       ts_fmt(lci.Birthday, 'yyyymmdd') cst_birthdy, "
                + "       CodeName('crs_cst_idtyp', lci.IDType) cst_idtyp_cod, "
                + "       CodeAlias('crs_cst_idtyp', lci.IDType) cst_idtyp_nam, "
                + "       lci.IDNo cst_idno, "
                + "       CodeName('crs_marrystat_cod', lci.Marriage) marrystat_cod, "
                + "       CodeAlias('crs_marrystat_cod', lci.Marriage) marrystat_nam, "
                + "       ts_fmt(lci.MarriageDate, 'yyyymmdd') marry_dat, "
                + "       '' spouse_nam, "
                + "       CodeName('crs_edu_cod', lci.Degree) edu_cod, "
                + "       CodeAlias('crs_edu_cod', lci.Degree) edu_nam, "
                + "       lci.Nationality nation_cod, "
                + "       CodeName('nationality', lci.Nationality) nation_nam, "
                + "       CodeName('nativeplace', lci.NativePlace) nationality, "
                + "       CodeName('crs_cst_ocpation_cod', lci.OccupationCode) ocpation_cod, "
                + "       CodeAlias('crs_cst_ocpation_cod', lci.OccupationCode) ocpation_nam, "
                + "       lci.SmokeFlag smk_flg, "
                + "       replace(lcadd.HomeAddress,',','-') hom_caddr, "
                + "       '' hom_eaddr, "
                + "       (lcadd.HomeZipCode) hom_zip, "
                + "       (lcadd.HomePhone) hom_tel, "
                + "       (lcadd.GrpName) wrk_nam, "
                + "       ( replace(lcadd.CompanyAddress,',','-')) wrk_addr, "
                + "       (lcadd.CompanyZipCode) wrk_zip, "
                + "       (lcadd.CompanyPhone) wrk_tel, "
                + "       (lcadd.CompanyFax) cst_fax, "
                + "       ( lcadd.Mobile "
                + "           ) cst_mob, "
                + "       ( lcadd.EMail) cst_email, "
                + "       '' bak1, "
                + "       '' bak2, "
                + "       '' bak3, "
                + "       '' bak4, "
                + "       '' bak5, "
                + "       '' bak6, "
                + "       '' bak7, "
                + "       '' bak8, "
                + "       ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send, "
                + "       '' date_update "
                + "  from LCInsured lci "
                + " inner join LCGrpCont lgc on lci.GrpContNo = lgc.GrpContNo "
                + " inner join LCAddress lcadd on lcadd.addressno= lci.addressno and lcadd.customerno= lci.insuredno "
                + " where 1 = 1 "
                + "   and lgc.AppFlag = '1' "
                + "   and lgc.UWFlag in ('4', '9') and lci.relationtomaininsured ='00'"
//                + "   and lgc.Crs_SaleChnl in "
//                + "       ('01', '02') "
                + "   and lgc.managecom=lci.managecom "
                + " and exists( select 1 From t_policy_main where Pol_No = lgc.grpcontno)  " // add by fuxin
                + "   and lci.ModifyDate  = current date -1 day  "
//                + "   and lci.ModifyDate  between '2010-6-1' and current date  " 
                + " with ur ";
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL
                    + "test000085CC05" + mCurrentDate.replaceAll("-", "") + ".txt",
                    true);
            int start = 1;
            int nCount = 200;

            RSWrapper rswrapper = new RSWrapper();
            rswrapper.prepareData(null, tSQL);
            SSRS tSSRS = null;

            do
            //            while (true)
            {
                //                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                tSSRS = rswrapper.getSSRS();

                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }

                //                String tempString = tSSRS.encode();
                String tempString = rswrapper.encode();

                String[] tempStringArr = tempString.split("\\^");

                tFileOutputStream = new FileOutputStream(mURL + "test000085CC05"
                        + mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[CC05]:pol_no = '"
                            + tSSRS.GetText(i, 1) + "' AND pol_subno = '"
                            + tSSRS.GetText(i, 3) + "' AND comp_cod = '"
                            + tSSRS.GetText(i, 4) + "' AND date_sign = '"
                            + tSSRS.GetText(i, 6) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");
                    mBufferedWriter.write("I[CC05]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
            while (tSSRS.getMaxRow() > 0);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 保单信息表
     */
    private void getCP01()
    {
    	
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL
                    + "test000085CP01" + mCurrentDate.replaceAll("-", "") + ".txt",
                    true);
            String tSQL = " select  "
                    + " lcc.ContNo pol_no,  "
                    + " (select max(lcrnsl.NewContNo) from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo) pol_no_orig,  "
                    + " '000085' comp_cod,  "
                    + " '中国人民健康保险股份有限公司' comp_nam,  "
                    + " ts_fmt(lcc.SignDate, 'yyyymmdd') date_sign,  "
                    + " ts_fmt(lcc.CValiDate, 'yyyymmdd') date_begin,  "
                    + " ts_fmt(lcc.CInValiDate, 'yyyymmdd') date_end,  "
                    + " ts_fmt(lcc.FirstPayDate, 'yyyymmdd') date_fee, "
                    + " (select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lcc.ContNo) pay_yrs,  "
                    + " CodeName('crs_pay_mod', char(lcc.PayIntv)) pay_mod_cod,  "
                    + " CodeAlias('crs_pay_mod', char(lcc.PayIntv)) pay_mod_nam,  "
                    + " '1' grp_psn_flg,  "
                    + " '99' chl_typ_cod,  "
                    + " '其它' chl_typ_nam,  "
                    + " '' chl_cod,  "
                    + " '' chl_nam,  "
                    + " lcc.AgentCom brok_cod,  "
                    + " (select lac.Name from LACom lac where lac.AgentCom = lcc.AgentCom) brok_nam,  "
                    + " CodeName('crs_typ', lcc.Crs_SaleChnl) crs_typ_cod,  "
                    + " CodeAlias('crs_typ', lcc.Crs_SaleChnl) crs_typ_nam,  "
                    + " (select groupagentcode from laagent where agentcode = lcc.agentcode) sales_cod, "
                    + " (select Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) sales_nam, "
                    + " lcc.GrpAgentCode crs_sales_cod,  "
                    + " lcc.GrpAgentName crs_sales_nam,  "
                    + " lcc.ManageCom org_sales_cod,  "
                    + " (select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_sales_nam,  "
                    + " lcc.ManageCom org_belg_cod,  "
                    + " (select Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) org_belg_nam,  "
                    + " lcc.GrpAgentCom org_crs_cod,  "
                    + " '' org_crs_nam,  "
                    + " lcc.AppntNo cst_cod,  "
                    + " lcc.AppntName cst_nam,  "
                    + " 'P' cst_typ,  "
                    + " (select replace(lcad.PostalAddress,',','-') from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_addr,  "
                    + " (select lcad.ZipCode from LCAppnt lca inner join LCAddress lcad on lcad.CustomerNo = lca.appntno and lcad.AddressNo = lca.AddressNo and lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo) cst_post, "
                    + " CodeName('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_cod,  "
                    + " CodeAlias('crs_cst_idtyp', lcc.AppntIdType) cst_idtyp_nam, "
                    + " lcc.AppntIdNo cst_idno, "
                    + " lcc.AppntName cst_linker_nam,  "
                    + " CodeName('crs_cst_linker_sex', lcc.AppntSex) cst_linker_sex, "
                    + " lcad.Phone cst_tel, "
                    + " lcad.Mobile  cst_mob, "
                    + " lcad.EMail  cst_mail, "
                    + " replace((select value(lci.Name, '') || '' || value(replace(lcad.PostalAddress,',','-'), '') || '' || value(lcad.Phone, '') from LCInsured lci inner join LCAddress lcad on lcad.CustomerNo = lci.Insuredno and lcad.AddressNo = lci.AddressNo and lci.ContNo = lcc.ContNo and lci.Insuredno = lcc.Insuredno),',','-') insd_info, "
                    + " (select lcb.Name from LCBnf lcb where lcb.ContNo = lcc.ContNo fetch first 1 rows only) beft_info, "
                    + " lcc.InsuredIdNo insr_targ,  "
                    + " lcc.InsuredName insr_targ_info, "
                    + " lcc.Prem prem, "
                    + " 0 prem_sign, "
                    + " 0 prem_pol,  "
                    + " lcc.Amnt amount,  "
                    + " '1' percnt, "
                    + " '' bak1, "
                    + " '' bak2, "
                    + " '' bak3, "
                    + " '' bak4, "
                    + " '' bak5, "
                    + " '' bak6, "
                    + " '' bak7, "
                    + " '' bak8, "
                    + " 'I' data_upd_typ, "
                    + " ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send, "
                    + " '' date_update  "
                    + " From LCCont lcc  "
                    + " inner join LCAppnt lca on lca.ContNo = lcc.ContNo  "
                    + " inner join LCAddress lcad on lca.AppntNo = lcad.CustomerNo and lcad.AddressNo = lca.AddressNo "
                    + " where 1 = 1  "
                    + " and lcc.ContType = '1'  "
                    + " and lcc.GrpContNo = '00000000000000000000'  "
                    + " and lcc.AppFlag = '1'  "
                    + " and lcc.UWFlag in ('4', '9')  "
//                    + " and lcc.Crs_SaleChnl in ('01', '02')  "
                    + " and lcc.SignDate = current date -1 day  "
//                    +" and lcc.CValiDate = current date - 1 day "  // modify by fuxin 集团调整取数规则
//                    +" and lcc.CValiDate between '2010-6-1' and current date "
                    + "  union  all "
                    + " ( select  "
                    + "  lgc.GrpContNo pol_no, "
                    + "  '' pol_no_orig,  "
                    + "  '000085' comp_cod,  "
                    + "  '中国人民健康保险股份有限公司' comp_nam, "
                    + "  ts_fmt(lgc.SignDate, 'yyyymmdd') date_sign, "
                    + "  ts_fmt(lgc.CValiDate, 'yyyymmdd') date_begin, "
                    + "  ts_fmt(lgc.CInValiDate, 'yyyymmdd') date_end, "
                    + "  ts_fmt((select ljtf.PayDate from LJTempFee ljtf where ljtf.otherno = lgc.GrpContNo and ljtf.othernotype = '7' fetch first 1 rows only), 'yyyymmdd') date_fee, "
                    + "  (select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lgc.GrpContNo) pay_yrs, "
                    + "  CodeName('crs_pay_mod', char(lgc.PayIntv)) pay_mod_cod,  "
                    + "  CodeAlias('crs_pay_mod', char(lgc.PayIntv)) pay_mod_nam, "
                    + "  '0' grp_psn_flg,  "
                    + "  '99' chl_typ_cod, "
                    + "  '其它' chl_typ_nam, "
                    + "  '' chl_cod, "
                    + "  '' chl_nam, "
                    + "  lgc.AgentCom brok_cod, "
                    + "  (select lac.Name from LACom lac where lac.AgentCom = lgc.AgentCom) brok_nam, "
                    + "  CodeName('crs_typ', lgc.Crs_SaleChnl) crs_typ_cod, "
                    + "  CodeAlias('crs_typ', lgc.Crs_SaleChnl) crs_typ_nam, "
                    + "  lgc.AgentCode sales_cod, "
                    + "  (select Name from LAAgent laa where laa.AgentCode = lgc.AgentCode) sales_nam, "
                    + "  lgc.GrpAgentCode crs_sales_cod, "
                    + "  lgc.GrpAgentName crs_sales_nam, "
                    + "  lgc.ManageCom org_sales_cod, "
                    + "  (select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_sales_nam, "
                    + "  lgc.ManageCom org_belg_cod, "
                    + "  (select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_belg_nam, "
                    + "  lgc.GrpAgentCom org_crs_cod, "
                    + "  '' org_crs_nam, "
                    + "  lgc.AppntNo cst_cod, "
                    + "  lgc.GrpName cst_nam, "
                    + "  'G' cst_typ, "
                    + "  replace(lcad.GrpAddress,',','-') cst_addr, "
                    + "  lcad.GrpZipCode  cst_post, "
                    + "  '' cst_idtyp_cod, "
                    + "  '' cst_idtyp_nam, "
                    + "  '' cst_idno, "
                    + "  lcad.LinkMan1 cst_linker_nam, "
                    + "  '' cst_linker_sex, "
                    + "  lcad.Phone1 cst_tel, "
                    + "  lcad.Mobile1 cst_mob, "
                    + "  lcad.E_Mail1 cst_mail, "
                    + "  (select lcc.InsuredName || '等' from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insd_info, "
                    + "  '' beft_info, "
                    + "  (select lcc.InsuredNo from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insr_targ, "
                    + "  replace((select lcc.InsuredName from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only),',','-') insr_targ_info, "
                    + "  lgc.Prem prem, "
                    + "  0 prem_sign, "
                    + "  0 prem_pol, "
                    + "  lgc.Amnt amount, "
                    + "  '1' percnt, "
                    + "  '' bak1,  "
                    + "  '' bak2,  "
                    + "  '' bak3,  "
                    + "  '' bak4,  "
                    + "  '' bak5,  "
                    + "  '' bak6,  "
                    + "  '' bak7,  "
                    + "  '' bak8,  "
                    + "  'I' data_upd_typ, "
                    + "  ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send, "
                    + "  '' date_update  "
                    + " From LCGrpCont lgc  "
                    + " inner join LCGrpAppnt lga on lgc.GrpContNo = lga.GrpContNo  "
                    + " inner join LCGrpAddress lcad on lcad.CustomerNo = lga.CustomerNo and lcad.AddressNo = lga.AddressNo  "
                    + "  where 1 = 1  "
                    + "    and lgc.AppFlag = '1'  "
                    + "  and lgc.UWFlag in ('4', '9')  "
//                    + "  and lgc.Crs_SaleChnl in ('01', '02')  "
                    + "  and lgc.SignDate  = current date -1 day  " 
//                    +" and lgc.CValiDate = current date - 1 day " // modify by fuxin 集团调整取数规则
//                    +" and lgc.CValiDate between '2010-6-1' and current date "
                    // 到签单数据
//                    + "  union   "
//                    + " select  "
//                    + "  lgc.GrpContNo pol_no, "
//                    + "  '' pol_no_orig,  "
//                    + "  '000085' comp_cod,  "
//                    + "  '中国人民健康保险股份有限公司' comp_nam, "
//                    + "  ts_fmt(lgc.SignDate, 'yyyymmdd') date_sign, "
//                    + "  ts_fmt(lgc.CValiDate, 'yyyymmdd') date_begin, "
//                    + "  ts_fmt(lgc.CInValiDate, 'yyyymmdd') date_end, "
//                    + "  ts_fmt((select ljtf.PayDate from LJTempFee ljtf where ljtf.otherno = lgc.GrpContNo and ljtf.othernotype = '7' fetch first 1 rows only), 'yyyymmdd') date_fee, "
//                    + "  (select max(lcp.PayYears) from LCPol lcp where lcp.ContNo = lgc.GrpContNo) pay_yrs, "
//                    + "  CodeName('crs_pay_mod', char(lgc.PayIntv)) pay_mod_cod,  "
//                    + "  CodeAlias('crs_pay_mod', char(lgc.PayIntv)) pay_mod_nam, "
//                    + "  '0' grp_psn_flg,  "
//                    + "  '99' chl_typ_cod, "
//                    + "  '其它' chl_typ_nam, "
//                    + "  '' chl_cod, "
//                    + "  '' chl_nam, "
//                    + "  lgc.AgentCom brok_cod, "
//                    + "  (select lac.Name from LACom lac where lac.AgentCom = lgc.AgentCom) brok_nam, "
//                    + "  CodeName('crs_typ', lgc.Crs_SaleChnl) crs_typ_cod, "
//                    + "  CodeAlias('crs_typ', lgc.Crs_SaleChnl) crs_typ_nam, "
//                    + "  lgc.AgentCode sales_cod, "
//                    + "  (select Name from LAAgent laa where laa.AgentCode = lgc.AgentCode) sales_nam, "
//                    + "  lgc.GrpAgentCode crs_sales_cod, "
//                    + "  lgc.GrpAgentName crs_sales_nam, "
//                    + "  lgc.ManageCom org_sales_cod, "
//                    + "  (select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_sales_nam, "
//                    + "  lgc.ManageCom org_belg_cod, "
//                    + "  (select Name from LDCom ldc where ldc.ComCode = lgc.ManageCom) org_belg_nam, "
//                    + "  lgc.GrpAgentCom org_crs_cod, "
//                    + "  '' org_crs_nam, "
//                    + "  lgc.AppntNo cst_cod, "
//                    + "  lgc.GrpName cst_nam, "
//                    + "  'G' cst_typ, "
//                    + "  replace(lcad.GrpAddress,',','-') cst_addr, "
//                    + "  lcad.GrpZipCode  cst_post, "
//                    + "  '' cst_idtyp_cod, "
//                    + "  '' cst_idtyp_nam, "
//                    + "  '' cst_idno, "
//                    + "  lcad.LinkMan1 cst_linker_nam, "
//                    + "  '' cst_linker_sex, "
//                    + "  lcad.Phone1 cst_tel, "
//                    + "  lcad.Mobile1 cst_mob, "
//                    + "  lcad.E_Mail1 cst_mail, "
//                    + "  (select lcc.InsuredName || '等' from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insd_info, "
//                    + "  '' beft_info, "
//                    + "  (select lcc.InsuredNo from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only) insr_targ, "
//                    + "  replace((select lcc.InsuredName from LCCont lcc where lcc.GrpContNo = lgc.GrpContNo fetch first 1 rows only),',','-') insr_targ_info, "
//                    + "  lgc.Prem prem, "
//                    + "  0 prem_sign, "
//                    + "  0 prem_pol, "
//                    + "  lgc.Amnt amount, "
//                    + "  '1' percnt, "
//                    + "  '' bak1,  "
//                    + "  '' bak2,  "
//                    + "  '' bak3,  "
//                    + "  '' bak4,  "
//                    + "  '' bak5,  "
//                    + "  '' bak6,  "
//                    + "  '' bak7,  "
//                    + "  '' bak8,  "
//                    + "  'I' data_upd_typ, "
//                    + "  ts_fmt(current timestamp, 'yyyymmdd hh:mi:ss') date_send, "
//                    + "  '' date_update  "
//                    + " From LCGrpCont lgc  "
//                    + " inner join LCGrpAppnt lga on lgc.GrpContNo = lga.GrpContNo  "
//                    + " inner join LCGrpAddress lcad on lcad.CustomerNo = lga.CustomerNo and lcad.AddressNo = lga.AddressNo  "
//                    + "  where 1 = 1  "
//                    + "    and lgc.AppFlag = '1'  "
//                    + "  and lgc.UWFlag in ('4', '9')  "
//////                    + "  and lgc.Crs_SaleChnl in ('01', '02')  "
//                    + "  and lgc.SignDate < '2010-6-1' " 
//////                    +" and lgc.CValiDate = current date - 1 day " // modify by fuxin 集团调整取数规则
//                    +" and lgc.CValiDate between '2010-6-1' and current date) "
                    + ")   with ur "
                    ;
    
            
            int start = 1;
            int nCount = 1000;

            RSWrapper rswrapper = new RSWrapper();
            rswrapper.prepareData(null, tSQL);
            SSRS tSSRS = null;

            do
            //while (true)
            {
            	
                //                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                tSSRS = rswrapper.getSSRS();
                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }

                //                String tempString = tSSRS.encode();
                String tempString = rswrapper.encode();

                String[] tempStringArr = tempString.split("\\^");
                tFileOutputStream = new FileOutputStream(mURL + "test000085CP01"
                        + mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                t_policy_mainSet tt_policy_mainSet = new t_policy_mainSet();
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                	t_policy_mainSchema tt_policy_mainSchema = new t_policy_mainSchema();
                	tt_policy_mainSchema.setpol_no(tSSRS.GetText(i, 1));
                	tt_policy_mainSchema.setcomp_cod(tSSRS.GetText(i, 3));
                	tt_policy_mainSchema.setdate_sign(tSSRS.GetText(i, 5));
                	tt_policy_mainSchema.setdate_update(mCurrrentDate);
                	tt_policy_mainSet.add(tt_policy_mainSchema);
                	
                	tMMap.put(tt_policy_mainSet, SysConst.DELETE_AND_INSERT);
                    VData data = new VData();
                    data.add(tMMap);
                    PubSubmit tPubSubmit = new PubSubmit();
                    tPubSubmit.submitData(data, "");
                    tt_policy_mainSet.clear();
                    
                    mBufferedWriter.write("D[CP01]:Pol_No = '"
                            + tSSRS.GetText(i, 1) + "' AND Comp_Cod = '"
                            + tSSRS.GetText(i, 3) + "' AND Date_Sign = '"
                            + tSSRS.GetText(i, 5) + "'");
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");
                    tArr[8] = tArr[8].substring(1, tArr[8].length() - 1);
                    for (int k = 47; k < 52; k++)
                    {
                        tArr[k] = tArr[k].substring(1, tArr[k].length() - 1);
                    }
                    mBufferedWriter.write("I[CP01]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
            while (tSSRS.getMaxRow() > 0);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
    /**
     * lcc.agentcode 
     */

    /**
     * 业务信息表
     */
    private void getCP02()
    {
        String tSQL[] = new String[10];
        //承保个险
        getCP02SQL0(tSQL);
        //承保团险
        getCP02SQL1(tSQL);
        //理赔个险
        getCP02SQL2(tSQL);
        //理赔团险
        getCP02SQL3(tSQL);
        //个险续期
        getCP02SQL4(tSQL);
        //团险续期
        getCP02SQL5(tSQL);
        //个险满期
        getCP02SQL6(tSQL);
        //团险满期
        getCP02SQL7(tSQL);
        //团险保全
        getCP02SQL8(tSQL);
        //个险保全
        getCP02SQL9(tSQL);
        try
        {
            FileOutputStream tFileOutputStream = new FileOutputStream(mURL
                    + "test000085CP02" + mCurrentDate.replaceAll("-", "") + ".txt",
                    true);
            for (int m = 0; m < tSQL.length; m++)
            {
                int start = 1;
                int nCount = 200;

                RSWrapper rswrapper = new RSWrapper();
                rswrapper.prepareData(null, tSQL[m]);
                SSRS tSSRS = null;

                do
                //                while (true)
                {
                    //                    SSRS tSSRS = new ExeSQL().execSQL(tSQL[m], start, nCount);
                    tSSRS = rswrapper.getSSRS();

                    if (tSSRS.getMaxRow() <= 0)
                    {
                        break;
                    }
                    //                    String tempString = tSSRS.encode();
                    String tempString = rswrapper.encode();

                    String[] tempStringArr = tempString.split("\\^");

                    tFileOutputStream = new FileOutputStream(mURL
                            + "test000085CP02" + mCurrentDate.replaceAll("-", "")
                            + ".txt", true);
                    OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                            tFileOutputStream, "GBK");
                    mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                    for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                    {
                        mBufferedWriter.write("D[CP02]:Pol_No = '"
                                + tSSRS.GetText(i, 1) + "' AND Comp_Cod = '"
                                + tSSRS.GetText(i, 2) + "' AND Date_Sign = '"
                                + tSSRS.GetText(i, 4) + "' AND Act_No = '"
                                + tSSRS.GetText(i, 5) + "' AND Date_Act = '"
                                + tSSRS.GetText(i, 6) + "' AND Risk_Cod = '"
                                + tSSRS.GetText(i, 21) + "'");
                        mBufferedWriter.newLine();
                        String t = tempStringArr[i].replaceAll("\\|", "','");
                        String[] tArr = t.split("\\,");
                        tArr[51] = tArr[51].substring(1, tArr[51].length() - 1);
                        for (int k = 22; k < 49; k++)
                        {
                            tArr[k] = tArr[k]
                                    .substring(1, tArr[k].length() - 1);
                        }
                        mBufferedWriter.write("I[CP02]:'");
                        for (int j = 0; j < tArr.length - 1; j++)
                        {
                            mBufferedWriter.write(tArr[j] + ",");
                        }
                        mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                        mBufferedWriter.newLine();
                        mBufferedWriter.flush();
                    }
                    mBufferedWriter.close();
                    start += nCount;
                }
                while (tSSRS.getMaxRow() > 0);
            }
        }
        catch (IOException ex2)
        {
            ex2.printStackTrace();
        }
    }

    private void getCP02SQL9(String[] tSQL)
    {
        //个险保全('CT','XT','WT')
        tSQL[9] = "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, "
                + "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, X.EndorseMentNo||X.PolNo 业务单证ID, "
                + "ts_fmt(X.MakeDate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, "
                + "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType "
                + "and AppObj ='I') 业务活动名称, "
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, "
                + "(select GrpAgentCom from lbcont where contno = a.contno) 销售机构代码,'' 销售机构名称, "
                + "CodeName('crs_typ',(select crs_salechnl from lccont where contno=a.contno union select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型ID,CodeAlias('crs_typ',(select crs_salechnl from lccont where contno=a.contno union select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型名称, "
                + "a.AppntNo 客户号,a.AppntName 客户名称, "
                + " '' 承保标的,max(InsuredName) 承保标的描述, "
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, "
                + "0 保费收入,0 签单保费,0 保单保费,-sum(a.Amnt) 保额, "
                + "0,0,0,0,0,0,0,0,0,0, "
                + "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, "
                + "0 年金给付,-sum(X.money) 退保金, "
                + "-1 承保人次,-sum(a.Amnt) 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, "
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, "
                + "0 缴费期次, "
                + "'','','','','','','','', "
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' "
                + "from lbpol a, "
                + "(select b.EndorseMentNo,b.FeeOperationType,b.PolNo,b.MakeDate,sum(getmoney) money "
                + "from ljagetendorse b  "
                + "where b.feeoperationtype in ('CT','XT') "
                + "and b.grpcontno = '00000000000000000000'   "
                + "and b.MakeDate  = current date -1 day  "
//              + " and b.MakeDate  between '2010-6-1' and current date "
                + "group by EndorsementNo,FeeOperationType,polno,makedate) as X "
                + "where (exists (select 1 from lccont where contno=a.contno ) " +
                		"or exists (select 1 from lbcont where contno=a.contno )) and a.PolNo = X.PolNo  "
                + " and exists( select 1 From t_policy_main where Pol_No = a.contno)  " // add by fuxin
                +" group by a.contno,a.signdate,X.FeeOperationType,a.ManageCom,a.AppntNo, a.AppntName,a.RiskCode,a.payintv,x.EndorseMentNo||X.PolNo,X.MakeDate "
//              因WT需冲减保费收入，因此需按照如下逻辑处理。原WT逻辑需取消。by gzh 20141126
                +"union all "
                + "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, "
                + "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, X.EndorseMentNo||X.PolNo 业务单证ID, "
                + "ts_fmt(X.MakeDate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, "
                + "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType "
                + "and AppObj ='I') 业务活动名称, "
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, "
                + "(select GrpAgentCom from lbcont where contno = a.contno) 销售机构代码,'' 销售机构名称, "
                + "CodeName('crs_typ',(select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型ID,CodeAlias('crs_typ',(select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型名称, "
                + "a.AppntNo 客户号,a.AppntName 客户名称, "
                + "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, "
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, "
                + "X.money 保费收入,0 签单保费,0 保单保费,0 保额, "
                + "0,0,0,0,0,0,0,0,0,0, "
                + "X.money 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, "
                + "0 年金给付,0 退保金, "
                + "(case FeeOperationType when 'RB' then 1 else 0 end) 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, "
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, "
                + "0 缴费期次, "
                + "'','','','','','','','', "
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' "
                + "from lbpol a, "
                + "(select b.EndorseMentNo,b.FeeOperationType,b.PolNo,b.MakeDate,sum(getmoney) money "
                + "from ljagetendorse b  "
                + "where b.feeoperationtype in ('WT') "
                + "and b.grpcontno = '00000000000000000000'   "
                + "and b.MakeDate = current date -1 day "
//              + " and b.MakeDate between '2010-6-1' and current date "
                + "group by EndorsementNo,FeeOperationType,polno,makedate) as X "
                + "where exists (select 1 from lbcont where contno=a.contno ) and a.PolNo = X.PolNo "
                + " and exists( select 1 From t_policy_main where Pol_No = a.contno)  " // add by fuxin
                //个险保全('FX','RB','CM','BF','TB')
                +"union all "
                + "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, "
                + "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, X.EndorseMentNo||X.PolNo 业务单证ID, "
                + "ts_fmt(X.MakeDate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, "
                + "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType "
                + "and AppObj ='I') 业务活动名称, "
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, "
                + "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, "
                + "CodeName('crs_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID,CodeAlias('crs_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称, "
                + "a.AppntNo 客户号,a.AppntName 客户名称, "
                + "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, "
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, "
                + "X.money 保费收入,0 签单保费,0 保单保费,0 保额, "
                + "0,0,0,0,0,0,0,0,0,0, "
                + "X.money 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, "
                + "0 年金给付,0 退保金, "
                + "(case FeeOperationType when 'RB' then 1 else 0 end) 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, "
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, "
                + "0 缴费期次, "
                + "'','','','','','','','', "
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' "
                + "from lcpol a, "
                + "(select b.EndorseMentNo,b.FeeOperationType,b.PolNo,b.MakeDate,sum(getmoney) money "
                + "from ljagetendorse b  "
                + "where b.feeoperationtype in ('FX','RB','CM','BF','TB') "
                + "and b.grpcontno = '00000000000000000000'   "
                + "and b.MakeDate = current date -1 day "
//              + " and b.MakeDate between '2010-6-1' and current date "
                + "group by EndorsementNo,FeeOperationType,polno,makedate) as X "
                + "where exists (select 1 from lccont where contno=a.contno ) and a.PolNo = X.PolNo "
                + " and exists( select 1 From t_policy_main where Pol_No = a.contno)  " // add by fuxin
                + "  with ur  ";
    }

    private void getCP02SQL8(String[] tSQL)
    {
        //团险保全('LQ','CM','ZB','ZT'),增人这里会契约统计冲突，所以这里不再提取
        tSQL[8] = "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, "
                + "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, "
                + "ts_fmt(max(X.makedate) , 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, "
                + "'ZT' 业务活动代码, "
                + " '减人' 业务活动名称, "
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, "
                + "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, "
                + "CodeName('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID,CodeAlias('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, "
                + "a.CustomerNo 客户号,a.GrpName 客户名称, "
                + "(select Idno from LDPerson where CustomerNo = (select InsuredNo "
                + "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName "
                + "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, "
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, "
                + "sum(X.money) 保费收入,0 签单保费,0 保单保费, "
                + "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) 保额, "
                + "0,0,0,0,0,0,0,0,0,0, "
                + "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, "
                + "0 退保金, "
                + "Peoples2 承保人次, "
                + "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) 保险金额, "
                + "0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, "
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, "
                + "0 缴费期次, "
                + "'','','','','','','','', "
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' "
                + "from lcgrppol a, "
                + "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*) peoples,sum(getmoney) money "
                + "from ljagetendorse b  "
                + "where b.feeoperationtype in ('LQ','CM','ZB','ZT','NI') "
//                + "and b.grpcontno <> '00000000000000000000' and b.MakeDate = current date -1 day  "
                + "and b.grpcontno <> '00000000000000000000' "
//                + "  and b.MakeDate between '2010-6-1' and current date  "
                + "and b.MakeDate = current date -1 day  "
                + "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X "
                + "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno ) and a.GrpPolNo = X.GrpPolNo  "
                + " and exists( select 1 From t_policy_main where Pol_No = a.grpcontno )  " // add by fuxin
                + " group by  a.GrpContNo,a.ManageCom,a.CustomerNo,a.GrpName,a.GrpPolNo,a.RiskCode,X.EndorseMentNo,a.payintv,Peoples2,X.makedate "
//              因WT需冲减保费收入，因此需按照如下逻辑处理。原WT逻辑需取消。by gzh 20141126
                + "union all "
                + "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, "
                + "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, "
                + "ts_fmt(max(X.makedate) , 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, "
                + "'WT' 业务活动代码, "
                + "'犹豫期退保' 业务活动名称, "
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, "
                + "(select GrpAgentCom from LbGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, "
                + "CodeName('crs_typ',(select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID,CodeAlias('crs_typ',(select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, "
                + "a.CustomerNo 客户号,a.GrpName 客户名称, "
                + "(select Idno from LDPerson where CustomerNo = (select InsuredNo "
                + "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName "
                + "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, "
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, "
                + "sum(X.money) 保费收入,0 签单保费,0 保单保费, "
                + "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) 保额, "
                + "0,0,0,0,0,0,0,0,0,0, "
                + "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, "
                + "0 退保金, "
                + "Peoples2 承保人次, "
                + "(select sum(Amnt) from lbpol where Edorno = X.EndorseMentNo and GrpPolNo = a.GrpPolNo) 保险金额, "
                + "0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, "
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, "
                + "0 缴费期次, "
                + "'','','','','','','','', "
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' "
                + "from lbgrppol a, "
                + "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*) peoples,sum(getmoney) money "
                + "from ljagetendorse b  "
                + "where b.feeoperationtype in ('WT') "
//                + "and b.grpcontno <> '00000000000000000000' and b.MakeDate = current date -1 day  "
                + "and b.grpcontno <> '00000000000000000000' "
//                + "  and b.MakeDate between '2010-6-1' and current date  "
                + "and (b.MakeDate = current date -1 day or (exists (select 1 from LOMixAgent where grpcontno = b.grpcontno and makedate =  current date - 1 day))) "
                + "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X "
                + "where exists (select 1 from lbgrpcont where grpcontno=a.grpcontno ) and a.GrpPolNo = X.GrpPolNo  "
                + " and exists( select 1 From t_policy_main where Pol_No = a.grpcontno )  " // add by fuxin
                + " group by  a.GrpContNo,a.ManageCom,a.CustomerNo,a.GrpName,a.GrpPolNo,a.RiskCode,X.EndorseMentNo,a.payintv,Peoples2,X.makedate "
                +
                //团险保全('XT','WT','CT')
                "union all "
                + "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, "
                + "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, X.EndorseMentNo 业务单证ID, "
                + "ts_fmt((X.makedate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, "
                + "X.FeeOperationType 业务活动代码,(select EdorName from lmedoritem where EdorCode = X.FeeOperationType "
                + "and AppObj ='G') 业务活动名称, "
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, "
                + "(select GrpAgentCom from LBGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, "
                + "CodeName('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno " +
                		"union select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," +
                		"CodeAlias('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno union select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, "
                + "a.CustomerNo 客户号,a.GrpName 客户名称, "
                + "(select Idno from LDPerson where CustomerNo = (select InsuredNo "
                + "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName "
                + "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, "
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, "
                + "0 保费收入,0 签单保费,0 保单保费, "
                + "a.Amnt*(-1) 保额, "
                + "0,0,0,0,0,0,0,0,0,0, "
                + "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付, "
                + "X.money*(-1) 退保金, "
                + "-a.Peoples2 承保人次,a.Amnt*(-1) 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, "
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, "
                + "0 缴费期次, "
                + "'','','','','','','','', "
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' "
                + "from lbgrppol a, "
                + "(select b.EndorseMentNo,b.FeeOperationType,b.GrpPolNo,b.MakeDate,count(*)*(-1) peoples,sum(getmoney) money "
                + "from ljagetendorse b  "
                + "where b.feeoperationtype in ('XT','CT') "
//                + "and b.grpcontno <> '00000000000000000000' and b.MakeDate  = current date -1 day  "
                + "and b.grpcontno <> '00000000000000000000' "
//                + " and b.MakeDate  between '2010-6-1' and current date  "
                + " and b.MakeDate  = current date -1 day "
                + "group by EndorsementNo,FeeOperationType,grppolno,makedate) as X "
                + "where (exists (select 1 from lcgrpcont where grpcontno=a.grpcontno ) or exists (select 1 from lbgrpcont where grpcontno=a.grpcontno ) )" 
                + " and exists( select 1 From t_policy_main where Pol_No = a.grpcontno  )  " // add by fuxin		
                +"   and a.GrpPolNo = X.GrpPolNo    with ur  ";
    }

    private void getCP02SQL7(String[] tSQL)
    {
        tSQL[7] = "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, "
                + "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, b.EndorseMentNo 业务单证ID, "
                + "ts_fmt(b.MakeDate, 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, "
                + "'MJ' 业务活动代码,'满期' 业务活动名称, "
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, "
                + "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, "
                + "CodeName('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," 
                +"CodeAlias('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, "
                + "a.CustomerNo 客户号,a.GrpName 客户名称, "
                + "(select Idno from LDPerson where CustomerNo = (select InsuredNo "
                + "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName "
                + "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, "
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, "
                + "0 保费收入,0 签单保费,0 保单保费,0 保额, "
                + "0,0,0,0,0,0,0,0,0,0, "
                + "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,sum(b.GetMoney)*(-1) 满期给付,0 年金给付,0 退保金, "
                + "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, "
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, "
                + "0 缴费期次, "
                + "'','','','','','','','', "
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' "
                + "from lcgrppol a,ljagetendorse b "
//                + "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno ) and b.MakeDate = current date -1 day  "
                + "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno )  "
//                + " and b.MakeDate between '2010-6-1' and current date  "
                + " and b.MakeDate = current date -1 day  "
                + "and a.GrpPolNo = b.GrpPolNo and b.FeeOperationtype = 'MJ' "
                + " and exists( select 1 From t_policy_main where Pol_No = a.grpcontno)  " // add by fuxin
                + "group by a.GrpContno,b.EndorseMentNo,a.ManageCom,a.Riskcode,a.CustomerNo,a.GrpName, "
                + "a.GrpPolNo,a.payintv,a.SaleChnl,b.FeeOperationtype,b.MakeDate   with ur  ";
    }

    private void getCP02SQL6(String[] tSQL)
    {
        tSQL[6] = "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, "
                + "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, b.SerialNo||b.polno 业务单证ID, "
                + "ts_fmt(date(c.ConfDate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, "
                + "'MJ' 业务活动代码,'满期' 业务活动名称, "
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, "
                + "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, "
                + "CodeName('crs_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID,CodeAlias('crs_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称, "
                + "a.AppntNo 客户号,a.AppntName 客户名称, "
                + "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,InsuredName 承保标的描述, "
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, "
                + "0 保费收入,0 签单保费,0 保单保费,0 保额, "
                + "0,0,0,0,0,0,0,0,0,0, "
                + "0 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,sum(b.GetMoney) 满期给付, "
                + "0 年金给付,0 退保金, "
                + "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, "
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, "
                + "0 缴费期次, "
                + "'','','','','','','','', "
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' "
                + "from lcpol a,ljsgetdraw b,ljaget c "
//                + "where a.Conttype = '1' and exists (select 1 from lccont where contno=a.contno ) and c.ConfDate = current date -1 day   "
                + "where a.Conttype = '1' and exists (select 1 from lccont where contno=a.contno )    "
//                + " and c.ConfDate between '2010-6-1' and current date "
                + "and a.PolNo = b.PolNo and b.getnoticeno = c.actugetno "
                + " and exists( select 1 From t_policy_main where Pol_No = a.contno)  " // add by fuxin
                + " and c.ConfDate = current date -1 day  "
                + "group by a.contno,b.SerialNo||b.polno,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName, "
                + "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,c.ConfDate   with ur  ";
    }

    private void getCP02SQL5(String[] tSQL)
    {
        tSQL[5] = "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, "
                + "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, b.SerialNo 业务单证ID, "
                + "ts_fmt((select b.confdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, "
                + "'XQ' 业务活动代码,'续期' 业务活动名称, "
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, "
                + "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, "
                + "CodeName('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," 
                +"CodeAlias('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, "
                + "a.CustomerNo 客户号,a.GrpName 客户名称, "
                + "(select Idno from LDPerson where CustomerNo = (select InsuredNo "
                + "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName "
                + "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, "
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, "
                + "sum(b.SumActuPayMoney) 保费收入,0 签单保费,0 保单保费,0 保额, "
                + "0,0,0,0,0,0,0,0,0,0, "
                + "0 新单保费,sum(b.SumActuPayMoney) 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金, "
                + "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, "
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, "
                + "b.Paycount 缴费期次, "
                + "'1','','','','','','','', "
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' "
                + "from lcgrppol a,ljapaygrp b "
//                + "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno ) and b.Confdate = current date -1 day    "
                + "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno )   "
//                + "  and b.Confdate between '2010-6-1' and current date  "
                + "  and b.Confdate = current date -1 day "
                + "and a.GrpPolNo = b.GrpPolNo and b.getnoticeno like '31%' "
                + " and exists( select 1 From t_policy_main where Pol_No = a.grpcontno)  " // add by fuxin
                + "group by a.GrpContno,b.SerialNo,a.ManageCom,a.Riskcode,a.CustomerNo,a.GrpName, "
                + "a.GrpPolNo,a.payintv,a.SaleChnl,b.PayCount,b.confdate with ur  ";
    }

    private void getCP02SQL4(String[] tSQL)
    {
        tSQL[4] = "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, "
                + "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期, b.SerialNo||b.polno 业务单证ID, "
                + "ts_fmt(date(b.confdate), 'yyyymmdd') 业务日期,'2' 业务类型代码,'保全' 业务类型名称, "
                + "'XQ' 业务活动代码,'续期' 业务活动名称, "
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, "
                + "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称, "
                + "CodeName('crs_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," 
                +"CodeAlias('crs_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称, "
                + "a.AppntNo 客户号,a.AppntName 客户名称, "
                + " max(c.idno) 承保标的,max(InsuredName) 承保标的描述, "
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称, "
                + "sum(b.SumActuPayMoney) 保费收入,0 签单保费,0 保单保费,0 保额, "
                + "0,0,0,0,0,0,0,0,0,0, "
                + "0 新单保费,sum(b.SumActuPayMoney) 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付, "
                + "0 年金给付,0 退保金, "
                + "0 承保人次,0 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额, "
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称, "
                + "b.PayCount 缴费期次, "
                + "'1','','','','','','','', "
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' "
                + "from lcpol a,ljapayperson b ,lcinsured c "
//                + "where a.Conttype = '1' and exists (select 1 from lccont where contno=a.contno ) and b.Confdate = current date -1 day   "
                + "where a.Conttype = '1' and exists (select 1 from lccont where contno=a.contno )  "
                + " and a.PolNo = b.PolNo and b.Paytypeflag is not null and a.insuredno = c.insuredno "
                + " and exists( select 1 From t_policy_main where Pol_No = a.contno)  " // add by fuxin
                + " and b.getnoticeno like '31%'"
                + " and b.Confdate = current date -1 day   "
                + " group by a.contno,b.SerialNo||b.polno,a.ManageCom,a.riskcode, "
                + " a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,b.PayCount,b.confdate   with ur  ";
    }

    private void getCP02SQL3(String[] tSQL)
    {
        tSQL[3] = "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司'子公司名称,"
                + "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno),'yyyymmdd') 签单日期,"
                + "b.CaseNo 业务单证ID,"
                + "ts_fmt(d.MakeDate,'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称,"
                + "'LP' 业务活动代码,'理赔' 业务活动名称,"
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称,"
                + "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称,"
                + "CodeName('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," 
                +"CodeAlias('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称,"
                + "a.CustomerNo 客户号,a.GrpName 客户名称,"
                + "(select Idno from LDPerson where CustomerNo = (select InsuredNo from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName "
                + "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述,"
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称,"
                + "0 保费收入,0 签单保费,0 保单保费,0 保额,"
                + "0,0,0,0,0,0,0,0,0,0,"
                + "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出,"
                + "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金,"
                + "0 承保人次,0 保险金额,"
                + "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次,"
                + "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次,"
                + "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额,"
                + "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额,"
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,"
                + "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次,"
                + "'','','','','','','','',"
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''"
                + "from lcgrppol a,LLClaimDetail b,LLCase c,ljagetclaim d "
                + "where exists (select 1 from lcgrpcont where grpcontno=a.grpcontno ) and b.grppolno = a.grppolno "
                + " and exists( select 1 From t_policy_main where Pol_No = a.grpcontno)  " // add by fuxin
                + "  and c.CaseNo = b.CaseNo "
                + " and c.CaseNo = d.otherno and d.othernotype = '5'" //by gzh 20110609 关联ljagetclaim表获取结案时间makedate
                + " and d.MakeDate = current date -1 day "  //by gzh 20110609 根据ljagetclaim结案时间提数
//                + " and d.MakeDate between '2010-6-1' and current date  "//提取时间段内的数据
                + "group by a.grpcontno,b.CaseNo,a.ManageCom,a.riskcode,a.CustomerNo,a.GrpName,"
                + "a.GrpPolNo,a.payintv,a.SaleChnl,a.amnt,c.RgtState,d.MakeDate "
                + "union all "
                + "select a.GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司'子公司名称,"
                + "ts_fmt((select signdate from lbgrpcont where grpcontno = a.grpcontno),'yyyymmdd') 签单日期,"
                + "b.CaseNo 业务单证ID,"
                + "ts_fmt(d.MakeDate,'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称, "
                + "'LP' 业务活动代码,'理赔' 业务活动名称, "
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称, "
                + "(select GrpAgentCom from LBGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称, "
                + "CodeName('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno union select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," 
                +"CodeAlias('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno union select crs_salechnl from lbgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称, "
                + "a.CustomerNo 客户号,a.GrpName 客户名称, "
                + "(select Idno from LDPerson where CustomerNo = (select InsuredNo from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName "
                + "from lbpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述, "
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称, "
                + "0 保费收入,0 签单保费,0 保单保费,0 保额, "
                + "0,0,0,0,0,0,0,0,0,0, "
                + "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出, "
                + "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金, "
                + "0 承保人次,0 保险金额, "
                + "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次, "
                + "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次, "
                + "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额, "
                + "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额, "
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码, "
                + "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次, "
                + "'','','','','','','','', "
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' "
                + "from lbgrppol a,LLClaimDetail b,LLCase c,ljagetclaim d "
                + "where (exists (select 1 from lcgrpcont where grpcontno=a.grpcontno ) or exists (select 1 from lbgrpcont where grpcontno=a.grpcontno ) )" +
                		" and b.grppolno = a.grppolno  "
                + " and exists( select 1 From t_policy_main where Pol_No = a.grpcontno)  " // add by fuxin
                + " and c.CaseNo = b.CaseNo  "
                + " and c.CaseNo = d.otherno and d.othernotype = '5'" //by gzh 20110609 关联ljagetclaim表获取结案时间makedate
                + " and d.MakeDate = current date -1 day "  //by gzh 20110609 根据ljagetclaim结案时间提数
//                + " and d.MakeDate  between '2010-6-1' and current date "//提取时间段的数据
                + "group by a.grpcontno,b.CaseNo,a.ManageCom,a.riskcode,a.CustomerNo,a.GrpName, "
                + "a.GrpPolNo,a.payintv,a.SaleChnl,a.amnt,c.RgtState,d.MakeDate   with ur  ";
    }

    private void getCP02SQL2(String[] tSQL)
    {
        tSQL[2] = "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,"
                + "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期,"
                + "b.CaseNo 业务单证ID,"
                + "ts_fmt(date(d.MakeDate), 'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称,"
                + "'LP' 业务活动代码,'理赔' 业务活动名称,"
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称,"
                + "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称,"
                + "CodeName('crs_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," 
                +"CodeAlias('crs_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称,"
                + "a.AppntNo 客户号,a.AppntName 客户名称,"
                + "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,a.InsuredName 承保标的描述,"
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称,"
                + "0 保费收入,0 签单保费,0 保单保费,0 保额,"
                + "0,0,0,0,0,0,0,0,0,0,"
                + "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出,"
                + "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金,"
                + "0 承保人次,0 保险金额,"
                + "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次,"
                + "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次,"
                + "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额,"
                + "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额,"
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,"
                + "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次,"
                + "'','','','','','','','',"
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''"
                + "from lcpol a,LLClaimDetail b,LLCase c,ljagetclaim d "
                + "where a.Conttype = '1' and exists (select 1 from lccont where contno=a.contno ) "
                + " and exists( select 1 From t_policy_main where Pol_No = a.contno)  " // add by fuxin
                + " and b.polno=a.polno   and c.CaseNo = b.CaseNo " 
                + " and c.CaseNo = d.otherno and d.othernotype = '5'" //by gzh 20110609 关联ljagetclaim表获取结案时间makedate
                + " and d.MakeDate = current date -1 day "  //by gzh 20110609 根据ljagetclaim结案时间提数
//                + " and d.MakeDate  between '2010-6-1' and current date "//根据时间段提数
                + "group by a.contno,b.CaseNo,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName,"
                + "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,c.RgtState,d.MakeDate "
                + "union all "
                + "select a.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,"
                + "ts_fmt(date(a.signdate), 'yyyymmdd') 签单日期,"
                + "b.CaseNo 业务单证ID,"
                + "ts_fmt(date(d.MakeDate), 'yyyymmdd') 业务日期, '3' 业务类型代码,'理赔' 业务类型名称,"
                + "'LP' 业务活动代码,'理赔' 业务活动名称,"
                + "a.ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称,"
                + "(select GrpAgentCom from lbcont where contno = a.contno) 销售机构代码,'' 销售机构名称,"
                + "CodeName('crs_typ',(select crs_salechnl from lccont where contno=a.contno union select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型ID," 
                +"CodeAlias('crs_typ',(select crs_salechnl from lccont where contno=a.contno union select crs_salechnl from lbcont where contno=a.contno)) 交叉销售类型名称,"
                + "a.AppntNo 客户号,a.AppntName 客户名称,"
                + "(select Idno from LDPerson where CustomerNo = a.InsuredNo) 承保标的,a.InsuredName 承保标的描述,"
                + "a.RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode)险种名称,"
                + "0 保费收入,0 签单保费,0 保单保费,0 保额,"
                + "0,0,0,0,0,0,0,0,0,0,"
                + "0 新单保费,0 续期保费,case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 寿险赔款支出,"
                + "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 死伤医疗给付,0 满期给付,0 年金给付,0 退保金,"
                + "0 承保人次,0 保险金额,"
                + "case when c.RgtState in ('11','12') then 1 else 0 end 已决赔付人次,"
                + "case when c.RgtState in ('11','12') then 0 else 1 end 未决赔付人次,"
                + "case when c.RgtState in ('11','12') then sum(b.Realpay) else 0 end 已决赔付金额,"
                + "case when c.RgtState in ('11','12') then 0 else sum(b.Realpay) end 未决赔付金额,"
                + "CodeName('crs_pay_mod',char(a.payintv)) 缴费方式代码,"
                + "CodeAlias('crs_pay_mod',char(a.payintv)) 缴费方式名称,1 缴费期次,"
                + "'','','','','','','','',"
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''"
                + "from lbpol a,LLClaimDetail b,LLCase c,ljagetclaim d  "
                + "where a.Conttype = '1' and (exists (select 1 from lccont where contno=a.contno ) or " 
                +" exists (select 1 from lbcont where contno=a.contno )) "
                + " and exists( select 1 From t_policy_main where Pol_No = a.contno)  "
                + "	and b.polno=a.polno  and c.CaseNo = b.CaseNo "
                + " and c.CaseNo = d.otherno and d.othernotype = '5'" //by gzh 20110609 关联ljagetclaim表获取结案时间makedate
                + " and d.MakeDate = current date -1 day "  //by gzh 20110609 根据ljagetclaim结案时间提数
//              + " and d.MakeDate  between '2010-6-1' and current date "//根据时间段提数
                + "group by a.contno,b.CaseNo,a.ManageCom,a.riskcode,a.InsuredNo,a.InsuredName,"
                + "a.AppntNo,a.AppntName,a.payintv,a.SaleChnl,a.signdate,c.RgtState,d.MakeDate   with ur  ";
    }

    private void getCP02SQL1(String[] tSQL)
    {
        tSQL[1] = "select GrpContNo 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,"
                + "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 签单日期, grppolno 业务单证ID,"
                + "ts_fmt((select signdate from lcgrpcont where grpcontno = a.grpcontno), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称,"
                + "'QD' 业务活动代码,'签单' 业务活动名称,"
                + "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称,"
                + "(select GrpAgentCom from LCGrpCont where GrpContNo = a.GrpContNo) 销售机构代码,'' 销售机构名称,"
                + "CodeName('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型ID," 
                +"CodeAlias('crs_typ',(select crs_salechnl from lcgrpcont where grpcontno=a.grpcontno)) 交叉销售类型名称,"
                + "CustomerNo 客户号,GrpName 客户名称,"
                + "(select Idno from LDPerson where CustomerNo = (select InsuredNo "
                + "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only)) 承保标的,(select InsuredName "
                + "from lcpol where GrpPolNo = a.GrpPolNo fetch first 1 rows only) 承保标的描述,"
                + "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称,"
                + "prem 保费收入,0 签单保费,0 保单保费,amnt 保额,"
                + "0,0,0,0,0,0,0,0,0,0,"
                + "prem 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金,"
                + "peoples2 承保人次,amnt 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额,"
                + "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次,"
                + "'1','','','','','','','',"
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''"
                + "from lcgrppol a "
                + "where  exists (select 1 from lcgrpcont where grpcontno=a.grpcontno ) and exists (select 1 from lcgrpcont where grpcontno = a.grpcontno "
                + " and exists( select 1 From t_policy_main where Pol_No = a.grpcontno)  "
                + " and signdate = current date -1 day "
//                + " and cvalidate between '2010-6-1' and current date " 
                + " )   with ur  ";
    }

    private void getCP02SQL0(String[] tSQL)
    {
        tSQL[0] = "select contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称, "
                + "ts_fmt(date(signdate), 'yyyymmdd') 签单日期, polno 业务单证ID,"
                + "ts_fmt(date(signdate), 'yyyymmdd') 业务日期,'1' 业务类型代码,'承保' 业务类型名称,"
                + "'QD' 业务活动代码,'签单' 业务活动名称,"
                + "ManageCom 归属机构代码,(select name from ldcom where comcode=a.ManageCom) 归属机构名称,"
                + "(select GrpAgentCom from lccont where contno = a.contno) 销售机构代码,'' 销售机构名称,"
                + "CodeName('crs_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型ID," 
                +"CodeAlias('crs_typ',(select crs_salechnl from lccont where contno=a.contno)) 交叉销售类型名称,"
                + "AppntNo 客户号,AppntName 客户名称,"
                + "max(Idno) 承保标的,max(InsuredName) 承保标的描述,"
                + "RiskCode 险种ID,(select RiskName from LMRiskApp where RiskCode = a.RiskCode) 险种名称,"
                + "sum(prem) 保费收入,0 签单保费,0 保单保费,sum(amnt) 保额,"
                + "0,0,0,0,0,0,0,0,0,0,"
                + "sum(prem) 新单保费,0 续期保费,0 寿险赔款支出,0 死伤医疗给付,0 满期给付,0 年金给付,0 退保金,"
                + "1 承保人次,sum(amnt) 保险金额,0 已决赔付人次,0 未决赔付人次,0 已决赔付金额,0 未决赔付金额,"
                + "CodeName('crs_pay_mod',char(payintv)) 缴费方式代码,CodeAlias('crs_pay_mod',char(payintv)) 缴费方式名称,1 缴费期次,"
                + "'1','','','','','','','',"
                + "'I' 数据变更类型, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,''"
                + "from lcpol a ,ldperson b "
                + "where Conttype = '1' and exists (select 1 from lccont where contno=a.contno ) "
                + " and a.insuredno = b.customerno "
                + " and not exists (select 1 from ljspayb where a.contno = otherno and dealstate ='1' ) " //不提续保
                + " and exists( select 1 From t_policy_main where Pol_No = a.contno)  "
//                + " and cvalidate between '2010-6-1' and current date "
//                + " and cvalidate = current date -1 day "  // by gzh 20110527
                + " and signdate = current date -1 day "
                + " group by polno,a.contno,signdate,ManageCom,AppntNo,AppntName,RiskCode,payintv "
                +" with ur  ";
//                + " and signdate = current date -1 day   with ur  ";
    }
    
    /**
     * 销售团队  date 20101027 add by gzh
     */
    private void getS004()
    {
        try
        {
        	FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "test000085S004" + mCurrentDate.replaceAll("-", "") + ".txt", true);
            String tSQL =
                    "select BranchAttr  销售团队ID,trim(branchtype)||trim(branchtype2) 渠道类型代码," +
                    "'000085' 子公司代码,ManageCom 归属销售机构代码,BranchManager 销售团队经理工号," +
                    "case when UpBranch ='' then BranchAttr else UpBranch  end 上级销售团队ID,Name 销售团队名称," +
                    "ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,case when endflag='Y' then 1 else 0 end 有效状态," +
                    "'' 备用字段1,'' 备用字段2,'' 备用字段3,'' 时间戳 "+
                    "from labranchgroup where modifydate = current date -1 day  with  ur";
            int start = 1;
            int nCount = 200;
            while (true)
            {
                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }
                String tempString = tSSRS.encode();
                String[] tempStringArr = tempString.split("\\^");
                //默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
//            mFileWriter = new FileWriter("C:\\000085S001" +
//                                         mCurrentDate.replaceAll("-", "") +
//                                         ".txt");
//            System.out.println(mFileWriter.getEncoding());
//            mBufferedWriter = new BufferedWriter(mFileWriter);
//          指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
                tFileOutputStream = new FileOutputStream(mURL + "test000085S004" +
                        mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[S004]:teamid = '" +
                                          tSSRS.GetText(i, 1) +
                                          "' AND teamChannel = '" +
                                          tSSRS.GetText(i, 2) + 
                                          "' AND comp_cod = '"+
                                          tSSRS.GetText(i, 3) + 
                                          "' and man_org_cod ='"+
                                          tSSRS.GetText(i, 4)+"'"
                                          );
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");
                    mBufferedWriter.write("I[S004]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                    	mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
        } catch (IOException ex2)
        {
            System.out.println(ex2.getStackTrace());
        }
    }
    
    /**
     * 销售团队人员上报  date 20101103 add by gzh
     
    private void getS005()
    {
        try
        {
        	FileOutputStream tFileOutputStream = new FileOutputStream(mURL +
                    "000085S005" + mCurrentDate.replaceAll("-", "") + ".txt", true);
            String tSQL =
                    "select lab.BranchAttr  销售团队ID,trim(laa.branchtype)||trim(laa.branchtype2) 渠道类型代码," +
                    "'000085' 子公司代码,laa.ManageCom 归属销售机构代码,laa.agentcode 销售员工号," +                    
                    "ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间, " +
                    "case when laa.agentstate ='06' then 0 else 1  end 有效状态," +
                    "'' 备用字段1,'' 备用字段2,'' 备用字段3,'' 时间戳 "+
                    "from laagent laa left join labranchgroup lab on laa.agentgroup = lab.agentgroup where laa.modifydate = current date -1 day  with  ur";
//            	 " select lab.BranchAttr 销售团队ID, "
//                +"(select trim(branchtype) || trim(branchtype2) from laagent where agentcode =lab.branchmanager) 渠道类型代码,"
//                +"'000085' 子公司代码, "
//                +" lab.ManageCom 归属销售机构代码, "
//                +" branchmanager 团队主管,"
//                +" ts_fmt(current TIMESTAMP, 'yyyymmdd hh:mi:ss') 子公司报送时间,"
//                +"case when  (select laa.agentstate  from laagent laa where agentcode =lab.branchmanager) ='06' then 0 else 1 end ,"	
//                +" '' 备用字段1,'' 备用字段2,'' 备用字段3,'' 时间戳 "
//                +" from  labranchgroup lab " 
//                +" where lab.modifydate = current date -1 day "  
//                +" order by lab.BranchAttr "
//                +"with ur ";
            int start = 1;
            int nCount = 200;
            while (true)
            {
                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }
                String tempString = tSSRS.encode();
                String[] tempStringArr = tempString.split("\\^");
                //默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
//            mFileWriter = new FileWriter("C:\\000085S001" +
//                                         mCurrentDate.replaceAll("-", "") +
//                                         ".txt");
//            System.out.println(mFileWriter.getEncoding());
//            mBufferedWriter = new BufferedWriter(mFileWriter);
//          指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
                tFileOutputStream = new FileOutputStream(mURL + "000085S005" +
                        mCurrentDate.replaceAll("-", "") + ".txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    mBufferedWriter.write("D[S005]:teamid = '" +
                                          tSSRS.GetText(i, 1) +
                                          "' AND teamChannel = '" +
                                          tSSRS.GetText(i, 2) + 
                                          "' AND comp_cod = '"+
                                          tSSRS.GetText(i, 3) + 
                                          "' and man_org_cod ='"+
                                          tSSRS.GetText(i, 4)+
                                          "' and sales_cod ='"+
                                          tSSRS.GetText(i, 5)+"'"
                                          );
                    mBufferedWriter.newLine();
                    String t = tempStringArr[i].replaceAll("\\|", "','");
                    String[] tArr = t.split("\\,");
                    mBufferedWriter.write("I[S005]:'");
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] + "'");
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
        } catch (IOException ex2)
        {
            System.out.println(ex2.getStackTrace());
        }
    }
*/
    public static void main(String args[])
    {
        MixedContNUTTask tMixedAgentTask = new MixedContNUTTask();
        tMixedAgentTask.run();
        return;
    }

    /**
     * 删除目录下所有文件和子目录，根目录不删除
     * @param filepath String
     * @throws IOException
     */
    private void del(String filepath) throws IOException
    {
        File f = new File(filepath); //定义文件路径
        if (f.exists() && f.isDirectory())
        { //判断是文件还是目录
            if (f.listFiles().length == 0)
            { //若目录下没有文件则直接返回，这里不再删除根目录
                return;
            } else
            { //若有则把文件放进数组，并判断是否有下级目录
                File delFile[] = f.listFiles();
                int i = f.listFiles().length;
                for (int j = 0; j < i; j++)
                {
                    if (delFile[j].isDirectory())
                    {
                        del(delFile[j].getAbsolutePath()); //递归调用del方法并取得子目录路径
                    }
                    delFile[j].delete(); //删除文件，或者递归传入的子目录
                }
            }
        }
    }

    private void jbInit() throws Exception
    {
    }
    
}
