package com.sinosoft.lis.taskservice;

import com.sinosoft.httpclient.inf.BqStatusUpLoad;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BqSCTPDataUpLoadTask extends TaskThread {
	public CErrors mErrors = new CErrors();
	private GlobalInput tGI = new GlobalInput();
	private String managecom = "86";
	private String mContNo=null;
	private String whereParSql="";
	
	public BqSCTPDataUpLoadTask(String tContNo,GlobalInput pGI)
	{
		mContNo = tContNo;
		tGI = pGI;
		managecom = pGI.ManageCom;
	}
	public BqSCTPDataUpLoadTask()
	{
		
	}
	
	public void run()
    {
		dealData();
    }
	public boolean dealData()
	{
		//先准备数据
		getInputData();
		System.out.println("#######税优保全数据上传批处理开始#######");
		
		BqDataUpload();
		
		System.out.println("#######税优保全数据上传批处理结束#######");
		
		return true;
       
	}
	private boolean getInputData()
	{
		if(mContNo!=null&&!mContNo.equals(""))
		{
			whereParSql=" and c.ContNo='"+mContNo+"' ";
		}
		else
		{
			tGI.ComCode="86";
		    tGI.Operator="Server";
		    tGI.ManageCom="86";	
		}
	    return true;
	}
	private void BqDataUpload()
	{
		String riskSql = "select riskcode from lmriskapp where risktype4 = '4' and TaxOptimal='Y' with ur";
        String riskStr = "";
        SSRS riskSSRS= new ExeSQL().execSQL(riskSql);
        if(null!=riskSSRS && riskSSRS.getMaxRow() > 0){
        	for(int i=1; i <= riskSSRS.getMaxRow(); i++){
        		riskStr +="'"+riskSSRS.GetText(i, 1)+"'";
        		if(i < riskSSRS.getMaxRow()){
        			riskStr +=",";
        		}
        	}
        }
        //向前置机报送最近日期的保单状态修改数据
	    String ContSql = "select c.startDate, a.contno "
	    			+ " from lccont a, lcpol b, lccontstate c"
	    			+ " where a.contno = b.contno "
	    			+ " and b.contno = c.contno "
	    			+ " and b.polno = c.polno "
	    			+ " and a.conttype = '1' "
	    			+ " and a.appflag = '1' "
	    			+ " and b.riskcode in ("+ riskStr +") "//税优产品标记
	    			+ " and not exists (select 1 from lserrorlist where transtype ='END003' "
			    	//+ " and businessno = (trim(c.contno)||' '||year(c.startdate)||'-'||month(c.startdate)||'-'||day(c.startdate)) and resultstatus='00') "
	    			+ " and businessno = c.contno "
	    			+ " and transno = c.startdate and resultstatus = '00' ) "
	    			//+ " and c.contno='014026496000001' "
	    			+ whereParSql
	    			+ " with ur";
					  
	    System.out.println(ContSql);
	    ExeSQL tEexSQL = new ExeSQL();
	    SSRS tSSRS = tEexSQL.execSQL(ContSql);
	    
	    int succCount=0;

	    for(int i=1;i<=tSSRS.getMaxRow();i++)
	    {
	    	String tStartDate=tSSRS.GetText(i, 1);
	    	String tContNo=tSSRS.GetText(i, 2);
		    VData tVData = new VData();
		    tVData.add(tGI);
		    
		    TransferData  tTransferData = new TransferData();
		    tTransferData.setNameAndValue("StartDate",tStartDate);
		    tTransferData.setNameAndValue("ContNo",tContNo);
		    tVData.add(tTransferData);
		    BqStatusUpLoad tBqStatusUpLoad=new BqStatusUpLoad();
		    tBqStatusUpLoad.submitData(tVData, "ZBXPT");
		    
	    }
	    if(succCount==tSSRS.getMaxRow())
	    {
	      System.out.println("昨天工单"+succCount+"个全部上传@@@！");
	    }
	  
	}
	
	 public static void main(String[] args)
	 {
		 String contno="012091051000004";
		 GlobalInput GI=new GlobalInput();
		 GI.Operator="server";
		 GI.ManageCom="86";
//		 BqSCTPDataUpLoadTask tBqSCTPDataUpLoadTask = new BqSCTPDataUpLoadTask(contno,GI);
		 BqSCTPDataUpLoadTask tBqSCTPDataUpLoadTask = new BqSCTPDataUpLoadTask();
		 tBqSCTPDataUpLoadTask.run();
	 }
}
