package com.sinosoft.lis.taskservice;

import java.io.File;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.GetFeePrintBL;
import com.sinosoft.lis.f1print.NewPreRecPrintBL;
import com.sinosoft.lis.f1print.Readhtml;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;

public class PremReceivableRptM extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = new GlobalInput();

	/** 提数日期 */
	private String[] date;

	/** 文件路径 */
	private String tOutXmlPath = "";

	/** 应用路径 */
	private String mURL = "";

	/** 对提数结果压缩成功标志：1-成功;0-失败;-1-失败终止 */
	private int mFlag;

	/** 压缩文件的路径 */
	private String mZipFilePath;

	private String StartDate = "";

	private String EndDate = "";

	private String type = "";
	
	private String toMail = "";

	private String fileName = "";

	String[][] mToExcel = null;

	String[][] mToExcel2 = null;

	SSRS tSSRS = new SSRS();

	private VData cInputData = new VData();

	// 执行任务
	public void run() {
		dealData();
	}

	public boolean dealData() {
		System.out.println("开始执行批处理:" + PubFun.getCurrentTime());
		
		if (!getStartRun()) {
			return false;
		}
		
		// 先准备数据
		getInputData();

		// 打印报表
		getReprot();

		// 报表文件压缩并发送邮件
		getReportZipFile();

		System.out.println("批处理执行完毕:" + PubFun.getCurrentTime());

		return true;
	}
	
	private boolean getStartRun() {
		String isRun = new ExeSQL()
				.getOneValue("select Trim(Char(Day(current date)))  from dual");
		if (!"1".equals(isRun)) {
			return false;
		}
		return true;
	}	

	/**
	 * ] 判断文件是否存在
	 * 
	 * @param filePath
	 * @return
	 */
	private boolean FileExists(String filePath) {
		File reportFile = new File(filePath);
		if (!reportFile.exists()) {
			return false;
		}
		return true;
	}

	/**
	 * 将给定目录下的文件打包为压缩包
	 * 
	 */
	private void getZipFile() {

		String tDocFileName = mURL + fileName; // 文件绝对路径 + 文件名
		mZipFilePath = mURL + fileName.replace("xls", "zip");
		System.out.println("压缩路径名：" + mZipFilePath);
		mFlag = compressIntoZip(mZipFilePath, tDocFileName);
	}

	/**
	 * 添加到压缩文件
	 * 
	 * @param zipfilepath
	 *            String
	 * @param out
	 *            ZipOutputStream
	 * @param tZipDocFileName
	 *            String：zip文件名
	 * @param tDocFileName
	 *            String：待压缩的文件路径名
	 * @return int：1：成功、0：失败，但不中断整个程序、-1：失败，中断程序
	 */
	private int compressIntoZip(String zipfilepath, String tDocFileName) {
		System.out.println("zipfilepath:" + zipfilepath);
		System.out.println("tDocFileName待压缩文件压缩路径:" + tDocFileName);
		Readhtml rh = new Readhtml();
		String[] tInputEntry = new String[1];
		tInputEntry[0] = tDocFileName;
		rh.CreateZipFile(tInputEntry, zipfilepath);
		return 1;
	}

	/**
	 * 将报表查询文件压缩包发送邮件
	 * 
	 */
	private boolean sendSagentMail() {
		// 压缩文件生成成功，开始发送邮件
		if (mFlag == 1) {
			// 邮箱发送
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS
					.GetText(1, 2), "picchealth");
			tMailSender.setSendInf("社保业务应收保费统计月报报表", "您好：\r\n    保单生效日期在  " + StartDate + " 至 "
					+ EndDate + " 期间内，社保业务应收保费报表见附件，谢谢</b>", mZipFilePath);
			String tSQLRMailSql = "select codename,codealias from ldcode where codetype = 'premreceivablermail' ";
			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1),
					tSSRSR.GetText(1, 2), null);
			// 发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}
			deleteFile(tOutXmlPath);
			deleteFile(mZipFilePath);
		}
		return true;

	}

	/**
	 * 删除生成的ZIP文件和XML文件
	 * 
	 * @param cFilePath
	 *            String：完整文件名
	 */
	private boolean deleteFile(String cFilePath) {
		File f = new File(cFilePath);
		if (!f.exists()) {
			CError tError = new CError();
			tError.moduleName = "MonthDataCatch";
			tError.functionName = "deleteFile";
			tError.errorMessage = "没有找到需要删除的文件";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		f.delete();
		return true;
	}

	private void sendMessage() {
		// 压缩文件生成成功，开始发送邮件
		if (mFlag != 1) {
			// 邮箱发送
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS
					.GetText(1, 2), "picchealth");
			tMailSender.setSendInf("社保业务应收保费统计月报报表", "您好：\r\n    保单生效日期在  " + StartDate + " 至 "
					+ EndDate + " 期间内，没有符合条件的数据,谢谢</b>", null);
			String tSQLRMailSql = "select codename,codealias from ldcode where codetype = 'premreceivablermail' ";
			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1),
					tSSRSR.GetText(1, 2), null);
			// 发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}

		}

	}

	private boolean getReportZipFile() {
		String name = tOutXmlPath;
		if (FileExists(name)) {
			System.out.println("打印文件存在");
			getZipFile();
			sendSagentMail();
		} else {
			sendMessage();
			System.out.println("没有符合打印条件的数据");
		}

		return true;
	}

	private boolean getReprot() {

		fileName = "PremReceivableReport.xls";
		tOutXmlPath = mURL + fileName;
		System.out.println("文件名称====：" + tOutXmlPath);
		WriteToExcel t = new WriteToExcel(fileName);
		t.createExcelFile();
		t.setAlignment("CENTER");
		t.setFontName("宋体");
		String[] sheetName = { new String("约定缴费报表"), new String("期缴保费报表") };
		t.addSheetGBK(sheetName);
		NewPreRecPrintBL tNewPreRecPrintBL = new NewPreRecPrintBL();
		if(!tNewPreRecPrintBL.getInputData(cInputData)){
			return false;
		}
		if(tNewPreRecPrintBL.dealData()){
			mToExcel = tNewPreRecPrintBL.getMToExcel();	
		}else{
			mToExcel = getDefalutData();
		}	
		t.setData(0, mToExcel);
		GetFeePrintBL tGetFeePrintBL = new GetFeePrintBL();
		if(!tGetFeePrintBL.getInputData(cInputData)){
			return false;
		}
		if(tGetFeePrintBL.dealData()){
			mToExcel2 = tGetFeePrintBL.getMToExcel();
		}else{
			mToExcel2 = getDefalutData();
		}			
		t.setData(1, mToExcel2);
		
		try {
			t.write(mURL);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("生成文件完成");

		return true;

	}

	private void getInputData() {
		// 设置管理机构
		mGI.ManageCom = "86";
		mGI.Operator = "001";
		mGI.ComCode = "86";
		StartDate = "2013-01-01";
		toMail = "auto";

		// 设置提数日期
		String predate = new ExeSQL()
				.getOneValue("select current date -1 MONTHS from dual");
		date = PubFun.calFLDate(predate);
		System.out.println("提数机构为：" + mGI.ManageCom + ",提数日期为："+StartDate+" 至"
				+ date[1]);

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);
//		mURL = "F:/";
		mURL+="vtsfile/";

		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}

		type = "";
		EndDate = date[1];

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("OutXmlPath", tOutXmlPath);
		tTransferData.setNameAndValue("StartDate", StartDate);
		tTransferData.setNameAndValue("EndDate", EndDate);
		tTransferData.setNameAndValue("type", type);
		tTransferData.setNameAndValue("toMail", toMail);
		cInputData.add(mGI);
		cInputData.add(tTransferData);

	}

	private String[][] getDefalutData() {
		String[][] tToExcel = new String[10000][30];
		tToExcel[0][0] = "中国人民健康保险股份有限公司";
		tToExcel[1][0] = EndDate.substring(0, EndDate.indexOf('-'))
				+ "年"
				+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate
						.lastIndexOf('-')) + "月应收保费统计表";
		tToExcel[3][0] = "统计期内无数据" ;

		return tToExcel;
	}



	// 主方法 测试用
	public static void main(String[] args) {
		new PremReceivableRptM().run();

	}
}
