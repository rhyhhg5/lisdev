package com.sinosoft.lis.taskservice;


import com.sinosoft.lis.yibaotong.LLTPTransFtpXml;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *外包批次信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class LLTPRgtTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

	private ExeSQL mExeSQL = new ExeSQL();
    String opr = "ture";
    public LLTPRgtTask()
    {}

    public void run()
    {
    	
    	String outSource="select code,codealias from ldcode where codetype  ='thridcompany'  order by othersign ";
    	SSRS outSources=mExeSQL.execSQL(outSource);
		if(outSources.MaxRow<=0){
			return ;
		}
    	
			//循环处理每家外包。
			for (int i = 1; i <= outSources.MaxRow; i++) {
				String code=outSources.GetText(i, 1);
				String codealias=outSources.GetText(i, 2);
		
				LLTPTransFtpXml  tLLTPTransFtpXml = new LLTPTransFtpXml();
	    	
				if(!tLLTPTransFtpXml.submitData(code==null ? "" : code,codealias==null ? "" : codealias))
		         {
		    		 System.out.println("批次信息导入出问题了");
		             opr ="false";
		         }else{
		        	 System.out.println("批次信息导入成功了");
		         }
		}
    }

   

   
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	public static void main(String[] args)
    {

        LLTPRgtTask tLaHumandeveITask = new LLTPRgtTask();
        tLaHumandeveITask.run();
//        tGEdorMJTask.oneCompany(tGlobalInput);
    }
}




