package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LATrainerWageHistoryDB;

/**
 * <p>
 * Title: 任务实例
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2006
 * </p>
 * <p>
 * Company: SinoSoft
 * </p>
 * 
 * @author xiangchun
 * @version 1.0
 */

public class LACommisionTransMoneyTask extends TaskThread {
	private String currentDate = PubFun.getCurrentDate();
	private String currentTime = PubFun.getCurrentTime();
	private LAIndirectWageSet mLAIndirectWageSchemaSet = new LAIndirectWageSet();
	private LATrainerWageHistorySet mLaTrainerWageHistorySet = new LATrainerWageHistorySet();
	private MMap mMap = new MMap();
	/** 全局数据 */
	private String mOperate;
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	public LACommisionTransMoneyTask() {
	}

	public void run() {
		System.out
				.println("＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  开始执行销售承保保费折标计算批处理 LACommisionTransMoneyTask.java!!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
		System.out.println("参数个数:" + mParameters.size());
		// 计算营销服务部月新单期缴标准保费
		if (!dealData()) {
			System.out.println("LACommisionTransMoneyTask:新单折标期缴标准保费计算失败");
		}

	}

	// 处理数据
	public boolean dealData() {
		
		// 查询所有八位管理机构
		String manageComSql = "select comcode from ldcom where char(length(trim(comcode)))='8' and sign='1'  order by comcode ";
		SSRS tSSRS = new SSRS();
		ExeSQL quExeSQLSQL = new ExeSQL();
		tSSRS = quExeSQLSQL.execSQL(manageComSql);
        // 处理前两天的保单对新单进行间接绩效的计算 T+2
		System.out
				.println("*****************查询lacommsion表中前两天的数据***************");
		String dateSql = "Select date('" + PubFun.getCurrentDate()
				+ "')-2 day from dual";
		ExeSQL texeSQL = new ExeSQL();
		String tMakeDate = texeSQL.getOneValue(dateSql);
		String wageNo[]=tMakeDate.split("\\-");
		String tWageNo=wageNo[0]+wageNo[1];
		
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			String tManageCom = tSSRS.GetText(i, 1);
			
			String sql = "select CommisionSN from lacommision where branchtype = '1' and branchtype2 = '01' and payyear='0'";
			sql += " and tmakedate='"
					+ tMakeDate
					+ "' and managecom ='"
					+ tManageCom
					+ "' and (commdire='1' or (commdire='2' and transtype = 'WT')) ";
			sql += " order by CommisionSN with ur ";
			
			System.out.println("个险组训新单折标批处理lacommision提数sql：" + sql);

			LACommisionSet tLaCommisionSet = new LACommisionSet();
			RSWrapper tRSWrapper = new RSWrapper();
			tRSWrapper.prepareData(tLaCommisionSet, sql);
			boolean tQueryFlag = false;
			do {
				tRSWrapper.getData();
				if (!tQueryFlag) {
					if (tLaCommisionSet == null || tLaCommisionSet.size() == 0) {
						System.out.println("LACommisionTransMoneyTask-->dealData没有满足条件 的数据");
						break;
					}
				}
				dealSet(tLaCommisionSet, tManageCom);

			} while (tLaCommisionSet.size() > 0);
			InsertHistory(tManageCom,tWageNo);
		}
		System.out.println("个险营业部组训新单期缴标准保费--->批处理结束！");
		return true;
	}

	private void dealSet(LACommisionSet laCommisionSet, String tManagecom) {
		for (int i = 1; i <= laCommisionSet.size(); i++) {
			LACommisionSchema tLACommisionSchema = new LACommisionSchema();
			LAIndirectWageSchema mLAIndirectWageSchema = new LAIndirectWageSchema();
			LACommisionDB tLACommisionDB = new LACommisionDB();
			tLACommisionDB.setCommisionSN(laCommisionSet.get(i)
					.getCommisionSN());
			tLACommisionDB.getInfo();
			tLACommisionSchema = tLACommisionDB.getSchema();
			String cCommisionSN = tLACommisionSchema.getCommisionSN();
			String cRiskCode = tLACommisionSchema.getRiskCode();
			String cTmakeDate = tLACommisionSchema.getTMakeDate();
			String cTransState = tLACommisionSchema.getTransState();
			String cPayIntv = String.valueOf(tLACommisionSchema.getPayIntv());
			String cPayYears = String.valueOf(tLACommisionSchema.getPayYears());
			// 计算团队首期绩效提奖比例
			String strWageRate = this.calCalRateForAll(cRiskCode, cPayIntv,
					cTransState,cPayYears);
			double wageRateValue = 0;
			wageRateValue = Double.parseDouble(strWageRate);
			System.out.println("承保保费折标系数为：" + wageRateValue);
			double tTransMoney = tLACommisionSchema.getTransMoney();
			double tBankG = tTransMoney * wageRateValue;
			System.out.println("折标后的标准承保保费为：" + tBankG);
			// 存入间接薪资项表
			mLAIndirectWageSchema.setCommisionSN(cCommisionSN);
			mLAIndirectWageSchema.setGrpRate(wageRateValue);
			// 新单折标保费
			mLAIndirectWageSchema.setF1(tBankG);
			mLAIndirectWageSchema.setBranchType("1");
			mLAIndirectWageSchema.setBranchType2("01");
			mLAIndirectWageSchema.setOperator("it001");
			mLAIndirectWageSchema.setTMakeDate(cTmakeDate);
			mLAIndirectWageSchema.setMakeDate(currentDate);
			mLAIndirectWageSchema.setMakeTime(currentTime);
			mLAIndirectWageSchema.setModifyDate(currentDate);
			mLAIndirectWageSchema.setModifyTime(currentTime);
			mLAIndirectWageSchemaSet.add(mLAIndirectWageSchema);
		}
		if (mLAIndirectWageSchemaSet.size() == 0) {
			System.out.println("管理机构:" + tManagecom
					+ "LAIndirectWageSchemaSet没有要插入的数据");
			return;
		}
		mMap.keySet().clear();
		mMap.put(this.mLAIndirectWageSchemaSet, "INSERT");
		this.mInputData.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start AgentWageCalSaveNewBL Submit...");
		try {
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				System.out.println("组训新单保费折标计算，管理机构" + tManagecom + "插入失败！");
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "AgentWageCalSaveNewBL";
				tError.functionName = "submitData";
				tError.errorMessage = "管理机构：" + tManagecom + "数据提交失败!";
				this.mErrors.addOneError(tError);
				return;
			}
			mLAIndirectWageSchemaSet.clear();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	private void InsertHistory(String tManagecom ,String tWageNo) {
		// 插入日志表
		String flag = "INSERT";
		LATrainerWageHistoryDB tLATrainerWageHistoryDB = new LATrainerWageHistoryDB();
		LATrainerWageHistorySchema tLATrainerWageHistorySchema = new LATrainerWageHistorySchema();
		tLATrainerWageHistoryDB.setManageCom(tManagecom);
		tLATrainerWageHistoryDB.setAClass("03");
		tLATrainerWageHistoryDB.setBranchType("1");
		tLATrainerWageHistoryDB.setBranchType2("01");
		tLATrainerWageHistoryDB.setWageNo(tWageNo);

		if (tLATrainerWageHistoryDB.getInfo()) {
			tLATrainerWageHistorySchema = tLATrainerWageHistoryDB.getSchema();
			flag = "UPDATE";
		} else {

			tLATrainerWageHistorySchema.setWageNo(tWageNo);
			tLATrainerWageHistorySchema.setAClass("03");
			tLATrainerWageHistorySchema.setManageCom(tManagecom);
			tLATrainerWageHistorySchema.setBranchType("1");
			tLATrainerWageHistorySchema.setBranchType2("01");
			tLATrainerWageHistorySchema.setMakeDate(currentDate);
			tLATrainerWageHistorySchema.setMakeTime(currentTime);
			flag = "INSERT";
		}
		tLATrainerWageHistorySchema.setState("00");
		tLATrainerWageHistorySchema.setModifyDate(currentDate);
		tLATrainerWageHistorySchema.setModifyTime(currentTime);
		tLATrainerWageHistorySchema.setOperator("it001");
		mLaTrainerWageHistorySet.clear();
		mLaTrainerWageHistorySet.add(tLATrainerWageHistorySchema);
		mMap.keySet().clear();
		mMap.put(this.mLaTrainerWageHistorySet, flag);
		this.mInputData.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start AgentWageCalSaveNewBL Submit...");
		try {
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				System.out.println("组训新单保费折标计算，管理机构" + tManagecom + "插入失败！");
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "AgentWageCalSaveNewBL";
				tError.functionName = "submitData";
				tError.errorMessage = "管理机构：" + tManagecom + "数据提交失败!";
				this.mErrors.addOneError(tError);
				return;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// 新单期缴标准保费折标系数提取函数
	private String calCalRateForAll(String cRiskCode, String cPayIntv,
			String cTransState,String cPayYears) {
		String tRiskCode = cRiskCode == null ? "" : cRiskCode;
		String tPayIntv = cPayIntv == null ? "" : cPayIntv;
		String tTransState = cTransState == null ? "" : cTransState;
		String tPayYears = cPayYears == null ? "" : cPayYears;
		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode("TMRate");
		tCalculator.addBasicFactor("RiskCode", tRiskCode);
		tCalculator.addBasicFactor("PayIntv", tPayIntv);
		tCalculator.addBasicFactor("TransState", tTransState);
		tCalculator.addBasicFactor("PayYears", tPayYears);

		
		String tResult = tCalculator.calculate();
		return tResult == null ? "" : tResult;
	}

	public static void main(String[] args) {
		LACommisionTransMoneyTask mlaCommisionTransMoneyTask = new LACommisionTransMoneyTask();
		mlaCommisionTransMoneyTask.run();
		System.out.println("批处理执行完毕！！！");
	}

}
