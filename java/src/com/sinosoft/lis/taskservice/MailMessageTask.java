package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.CommonMMImpl;
import com.sinosoft.lis.bq.ContLosted;
import com.sinosoft.lis.bq.MMControlImpl;
import com.sinosoft.lis.bq.mm.face.ICommonMM;
import com.sinosoft.lis.bq.mm.face.IMMControl;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;

public class MailMessageTask extends TaskThread {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();
    private ICommonMM common = new CommonMMImpl();
    private GlobalInput cGlobalInput = new GlobalInput();
    //这个保单失效通知
    private ContLosted lost = new ContLosted() ;
    //这个是判断是否打开了失效保单的通知功能
    private IMMControl control = new MMControlImpl() ;
    //这个是随便设的 因为是批处理自己跑
    public MailMessageTask(){
    	cGlobalInput.AgentCom="86";
    	cGlobalInput.ClientIP="127.0.0.1";
    	cGlobalInput.ComCode="86" ;
    	cGlobalInput.ManageCom="86";
    	cGlobalInput.Operator="server";
    	cGlobalInput.ServerIP="127.0.0.1";
    }
    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run()
    {
    	common.sendMM(cGlobalInput) ;
    	if(control.isOpen("C", "BQ")){
    	      lost.sendMessageForLosted() ;
    	}
    }
}
