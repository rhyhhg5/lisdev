package com.sinosoft.lis.taskservice;

import com.sinosoft.httpclient.inf.LCContTranferOut;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: LCContTranfOutTask </p>
 * <p>Description: 保单迁出同步平台数据的批处理 </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class LCContTranfOutTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储传入的容器*/
	private VData mInputData = new VData();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	/**存储操作数据类型*/
	private String mOperate = "";
	
	private String mStartDate=PubFun.getCurrentDate();//当前日期
	
	private String mEndDate=PubFun.getCurrentDate();//当前日期
	
	public LCContTranfOutTask() {

	}
	
	public LCContTranfOutTask(String startDate,String endDate) {
		if(null!=startDate && !"".equals(startDate)){
			mStartDate=startDate;
		}
		if(null!=endDate && !"".equals(endDate)){
			mEndDate=endDate;
		}
	}
	
	/**
	 * run方法
	 */
	public void run(){
		System.out.println("");
		System.out.println("======================TransferAgentDataTask Execute  !!!======================");
		System.out.println("                  ");
		/**
		 * 往同步保单迁出数据中出入日期的值
		 */
		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("QueryStartDate", mStartDate);
		tTransferData.setNameAndValue("QueryEndDate", mEndDate);
		tInputData.add(tTransferData);
		/**
		 * 调用保单迁出同步平台数据的接口
		 */
		LCContTranferOut tLCContTranferOut = new LCContTranferOut();
		
		if(!tLCContTranferOut.submitData(tInputData,"ZBXPT")){
		   this.mErrors.copyAllErrors(tLCContTranferOut.mErrors);
	       mResult.clear();
	       mResult.add(mErrors.getFirstError());
		}
	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		LCContTranfOutTask tLCContTranfOutTask = new LCContTranfOutTask();
		tLCContTranfOutTask.run();
	}
	
}
