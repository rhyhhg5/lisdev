package com.sinosoft.lis.taskservice;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;
import com.sinosoft.lis.message.Response;
import com.sinosoft.lis.message.SmsMessage;
import com.sinosoft.lis.message.SmsMessages;
import com.sinosoft.lis.message.SmsServiceServiceLocator;
import com.sinosoft.lis.message.SmsServiceSoapBindingStub;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.message.SMSClient;    
import com.sinosoft.lis.message.SmsMessageNew;
import com.sinosoft.lis.message.SmsMessagesNew;



public class PremIcomeBL extends TaskThread{

//	private static String host = "10.252.8.100";//发送方邮箱所在的smtp主机
//	private static String sender = "eshop@picchealth.com";//发送方邮箱 
//	private static String password = "pass@word1";//密码
    private String mCurrentDate = PubFun.getCurrentDate();
    private ExeSQL mExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public void run() {
        System.out.println("短信通知批处理开始......。。。。");
//        SmsServiceSoapBindingStub binding = null;
//        try {
//            binding = (SmsServiceSoapBindingStub)new SmsServiceServiceLocator().
//                      getSmsService(); //创建binding对象
//        } catch (javax.xml.rpc.ServiceException jre) {
//            jre.printStackTrace();
//        }

//        binding.setTimeout(60000);
//        Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。
        
        SMSClient smsClient=new SMSClient();
        Vector vec = new Vector();
        vec = getMessage();
        Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数
        //拆分Vector
        System.out.println("---------vec="+vec.size());

        if (!vec.isEmpty()) 
        {
        	for(int i=0;i<vec.size();i++)
            {
        		tempVec.clear();
            	tempVec.add(vec.get(i));
            	
            	SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
                msgs.setOrganizationId("86"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                msgs.setServiceType(SMSClient.mes_serviceType1);//设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                msgs.setStartDate(PubFun.getCurrentDate()); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                msgs.setStartTime("5:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                msgs.setEndDate(PubFun.getCurrentDate()); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                msgs.setTaskValue(SMSClient.mes_serviceType1);
                msgs.setMessages((SmsMessageNew []) tempVec.toArray(new SmsMessageNew[tempVec.size()]));//设置该批短信的每一条短信，一批短信可以包含多条短信
               
                smsClient.sendSMS(msgs);
//                try 
//                {
//                    value = binding.sendSMS("Admin", "Admin", msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
//                    System.out.println(value.getStatus());
//                    System.out.println(value.getMessage());
//                } 
//                catch (RemoteException ex) 
//                {
//                    ex.printStackTrace();
//               }
            }
        } 
        else 
        {
            System.out.print("无符合条件的短信！");
        }
        System.out.println("客户短信通知批处理正常结束......");
    }
 
    private Vector getMessage() {
    	System.out.println("进入获取信息 getMessage()------------------------");
        Vector tVector = new Vector();
        Calendar c =Calendar.getInstance();   
        c.add(Calendar.DAY_OF_MONTH, -1);  
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        String mDateTime=formatter.format(c.getTime());  
        String date=mDateTime.substring(0, 10);
        String year=mDateTime.substring(0, 4);
        System.out.println(date);
        System.out.println(year);
//        年度总保费
        String StrSQL1="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate between '"+year+"-01-01' and '"+date+"' and Accountcode in ('6031000000','7031000000') and checkflag='00'";
//        个险
        String StrSQL2="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate between '"+year+"-01-01' and '"+date+"' and Accountcode in ('6031000000','7031000000') and (salechnl in ('01','06') or salechnl like '10%') and checkflag='00'";
//        团险
        String StrSQL3="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate between '"+year+"-01-01' and '"+date+"' and Accountcode in ('6031000000','7031000000') and (salechnl in ('02','07','11','12') or salechnl like '03%') and checkflag='00'";
//        银代
        String StrSQL4="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate between '"+year+"-01-01' and '"+date+"' and Accountcode in ('6031000000','7031000000') and salechnl in ('04','13') and checkflag='00'";
//        互动
        String StrSQL5="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate between '"+year+"-01-01' and '"+date+"' and Accountcode in ('6031000000','7031000000') and (salechnl='14' or salechnl like '15%') and checkflag='00'";
//        社保
        String StrSQL6="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate between '"+year+"-01-01' and '"+date+"' and Accountcode in ('6031000000','7031000000') and salechnl='16' and checkflag='00'";
//        健管
        String StrSQL7="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate between '"+year+"-01-01' and '"+date+"' and Accountcode in ('6031000000','7031000000') and salechnl ='17' and checkflag='00'";
     
        //        当日总保费
        String StrSQL8="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate='"+date+"' and Accountcode in ('6031000000','7031000000') and checkflag='00'";
//      个险
        String StrSQL9="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate='"+date+"' and Accountcode in ('6031000000','7031000000') and (salechnl in ('01','06') or salechnl like '10%') and checkflag='00'";
//      团险
        String StrSQL10="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate='"+date+"' and Accountcode in ('6031000000','7031000000') and (salechnl in('02','07','11','12') or salechnl like '03%') and checkflag='00'";
//      银代
        String StrSQL11="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate='"+date+"' and Accountcode in ('6031000000','7031000000') and salechnl in ('04','13') and checkflag='00'";
//      互动
        String StrSQL12="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate='"+date+"' and Accountcode in ('6031000000','7031000000') and (salechnl='14' or salechnl like '15%') and checkflag='00'";
//      社保
        String StrSQL13="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate='"+date+"' and Accountcode in ('6031000000','7031000000') and salechnl='16' and checkflag='00'";
//      健管
        String StrSQL14="select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from Fivoucherdatadetail where Accountdate='"+date+"' and Accountcode in ('6031000000','7031000000') and salechnl ='17' and checkflag='00'";
       
        SSRS tSSRS1=mExeSQL.execSQL(StrSQL1);
        SSRS tSSRS2=mExeSQL.execSQL(StrSQL2);
        SSRS tSSRS3=mExeSQL.execSQL(StrSQL3);
        SSRS tSSRS4=mExeSQL.execSQL(StrSQL4);
        SSRS tSSRS5=mExeSQL.execSQL(StrSQL5);
        SSRS tSSRS6=mExeSQL.execSQL(StrSQL6);
        SSRS tSSRS7=mExeSQL.execSQL(StrSQL7);
        SSRS tSSRS8=mExeSQL.execSQL(StrSQL8);
        SSRS tSSRS9=mExeSQL.execSQL(StrSQL9);
        SSRS tSSRS10=mExeSQL.execSQL(StrSQL10);
        SSRS tSSRS11=mExeSQL.execSQL(StrSQL11);
        SSRS tSSRS12=mExeSQL.execSQL(StrSQL12);
        SSRS tSSRS13=mExeSQL.execSQL(StrSQL13);
        SSRS tSSRS14=mExeSQL.execSQL(StrSQL14);

        String baofeij1=tSSRS1.GetText(1, 1);
        String baofeij2=tSSRS2.GetText(1, 1);
        String baofeij3=tSSRS3.GetText(1, 1);
        String baofeij4=tSSRS4.GetText(1, 1);
        String baofeij5=tSSRS5.GetText(1, 1);
        String baofeij6=tSSRS6.GetText(1, 1);
        String baofeij7=tSSRS7.GetText(1, 1);
        String baofeij8=tSSRS8.GetText(1, 1);
        String baofeij9=tSSRS9.GetText(1, 1);
        String baofeij10=tSSRS10.GetText(1, 1);
        String baofeij11=tSSRS11.GetText(1, 1);
        String baofeij12=tSSRS12.GetText(1, 1);
        String baofeij13=tSSRS13.GetText(1, 1);
        String baofeij14=tSSRS14.GetText(1, 1); 

        double baofei1 = Double.parseDouble(baofeij1)/10000.00;
        double baofei2 = Double.parseDouble(baofeij2)/10000.00;
        double baofei3 = Double.parseDouble(baofeij3)/10000.00;
        double baofei4 = Double.parseDouble(baofeij4)/10000.00;
        double baofei5 = Double.parseDouble(baofeij5)/10000.00;
        double baofei6 = Double.parseDouble(baofeij6)/10000.00;
        double baofei7 = Double.parseDouble(baofeij7)/10000.00;
        double baofei8 = Double.parseDouble(baofeij8)/10000.00;
        double baofei9 = Double.parseDouble(baofeij9)/10000.00;
        double baofei10 = Double.parseDouble(baofeij10)/10000.00;
        double baofei11 = Double.parseDouble(baofeij11)/10000.00;
        double baofei12 = Double.parseDouble(baofeij12)/10000.00;
        double baofei13 = Double.parseDouble(baofeij13)/10000.00;
        double baofei14 = Double.parseDouble(baofeij14)/10000.00; 
        
        BigDecimal bf1 = new BigDecimal(baofei1);
        BigDecimal bf2 = new BigDecimal(baofei2);
        BigDecimal bf3 = new BigDecimal(baofei3);
        BigDecimal bf4 = new BigDecimal(baofei4);
        BigDecimal bf5 = new BigDecimal(baofei5);
        BigDecimal bf6 = new BigDecimal(baofei6);
        BigDecimal bf7 = new BigDecimal(baofei7);
        BigDecimal bf8 = new BigDecimal(baofei8);
        BigDecimal bf9 = new BigDecimal(baofei9);
        BigDecimal bf10 = new BigDecimal(baofei10);
        BigDecimal bf11 = new BigDecimal(baofei11);
        BigDecimal bf12 = new BigDecimal(baofei12);
        BigDecimal bf13 = new BigDecimal(baofei13);
        BigDecimal bf14 = new BigDecimal(baofei14);
        
        double bfi1 = bf1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi2 = bf2.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi3 = bf3.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi4 = bf4.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi5 = bf5.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi6 = bf6.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi7 = bf7.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi8 = bf8.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi9 = bf9.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi10 = bf10.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi11 = bf11.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi12 = bf12.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi13 = bf13.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        double bfi14 = bf14.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        
        	//短信内容
        	String tMSGContents=""; 
        	SmsMessageNew tKXMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
//        	SmsMessageNew mKXMsg = new SmsMessageNew();
//        	SmsMessageNew xKXMsg = new SmsMessageNew();
//        	SmsMessageNew sKXMsg = new SmsMessageNew();
//        	SmsMessageNew hKXMsg = new SmsMessageNew();
        	SmsMessageNew dKXMsg= new SmsMessageNew();
        	
//        	tMSGContents = "各位领导好：\n财务接口批处理已结束，记账日期为"+date+"日，截至当前年度总保费收入为"+baofei1+"元，其中：\n个险渠道"+baofei2+"元\n团险渠道"+baofei3+"元\n银代渠道"+baofei4+"元\n互动渠道"+baofei5+"元\n社保渠道"+baofei6+"元\n健管渠道"+baofei7+"元。\n"+date+"日（昨天）当日总保费"+baofei8+"元,其中：\n个险渠道"+baofei9+"元\n团险渠道"+baofei10+"元\n银代渠道"+baofei11+"元\n互动渠道"+baofei12+"元\n社保渠道"+baofei13+"元\n健管渠道"+baofei14+"元。";
        	tMSGContents = "截至"+date+"日年度保费收入为"+bfi1+"万元，其中：\n个险"+bfi2+"万元；团险"+bfi3+"万元；银代"+bfi4+"万元；互动"+bfi5+"万元；社保"+bfi6+"万元；健管"+bfi7+"万元。\n"+date+"日当日总保费"+bfi8+"万元,其中：\n个险"+bfi9+"万元；团险"+bfi10+"万元；银代"+bfi11+"万元；互动"+bfi12+"万元；社保"+bfi13+"万元；健管"+bfi14+"万元。";
        	System.out.println(tMSGContents);
//        	tKXMsg.setReceiver("15801335864");
//        	tKXMsg.setContents(tMSGContents);
//         	mKXMsg.setReceiver("15810663099");
//       	  mKXMsg.setContents(tMSGContents);
//       	  xKXMsg.setReceiver("13718439169");
//      	  xKXMsg.setContents(tMSGContents);
//      	  sKXMsg.setReceiver("18611746311");
//      	  sKXMsg.setContents(tMSGContents);
//      	  hKXMsg.setReceiver("18910898865");
//      	  hKXMsg.setContents(tMSGContents);

					dKXMsg.setReceiver("18701025305");
        	dKXMsg.setContents(tMSGContents);
        	dKXMsg.setOrgCode("86000000");
//        	tVector.add(tKXMsg);
//        	tVector.add(mKXMsg);
//        	tVector.add(xKXMsg);
//        	tVector.add(sKXMsg);
//          tVector.add(hKXMsg);
						tVector.add(dKXMsg);
          System.out.println("都结束了----------------------------------------------------------------"+"\n"+dKXMsg.toString()
        		+"\n"+dKXMsg.getOrgCode());
        return tVector;
    }
    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
//        VData mVData = new VData();
//        mGlobalInput.Operator = "001";
//        mGlobalInput.ManageCom = "86";
//        mVData.add(mGlobalInput);
        PremIcomeBL tPremIcomeBL = new PremIcomeBL();
        tPremIcomeBL.run();
    }
}
