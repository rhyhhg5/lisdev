package com.sinosoft.lis.taskservice;

import java.io.File;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.f1print.Readhtml;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;

public class HighendMedicalDockTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = new GlobalInput();

	/** 文件路径 */
	private String tOutXmlPath = "";

	/** 应用路径 */
	private String mURL = "";

	/** 应用路径 */
	private String mURL1 = "";

	/** 压缩文件的路径 */
	private String mZipFilePath;

	private String fileName = "";

	String[][] mToExcel = null;

	String[][] mToExcel2 = null;

	SSRS tSSRS = new SSRS();

	private String mSql = null;

	private boolean haveCB = false;

	private boolean haveBQ = false;

	private boolean haveScan = false;

	private String mScanFilePath = "";

	private static final int BUFFER = 2048;

	// 执行任务
	public void run() {
		dealData();
	}

	public boolean dealData() {
		System.out.println("开始执行批处理:" + PubFun.getCurrentTime());
		// 先准备数据
		getInputData();

		// 打印报表
		getReprot();

		// 得到扫描件
		getScan();

		// 报表文件压缩并发送邮件
		getReportZipFile();

		System.out.println("批处理执行完毕:" + PubFun.getCurrentTime());

		return true;
	}

	/**
	 * ] 判断文件是否存在
	 * 
	 * @param filePath
	 * @return
	 */
	private boolean FileExists(String filePath) {
		File reportFile = new File(filePath);
		if (!reportFile.exists()) {
			return false;
		}
		return true;
	}

	/**
	 * 将给定目录下的文件打包为压缩包
	 * 
	 */
	private void getZipFile() {

		String tDocFileName = mURL + fileName; // 文件绝对路径 + 文件名
		mZipFilePath = mURL + fileName.replace("xls", "zip");
		System.out.println("压缩路径名：" + mZipFilePath);
		compressIntoZip(mZipFilePath, tDocFileName);
	}

	/**
	 * 添加到压缩文件
	 * 
	 * @param zipfilepath
	 *            String
	 * @param out
	 *            ZipOutputStream
	 * @param tZipDocFileName
	 *            String：zip文件名
	 * @param tDocFileName
	 *            String：待压缩的文件路径名
	 * @return int：1：成功、0：失败，但不中断整个程序、-1：失败，中断程序
	 */
	private int compressIntoZip(String zipfilepath, String tDocFileName) {
		System.out.println("zipfilepath:" + zipfilepath);
		System.out.println("tDocFileName待压缩文件压缩路径:" + tDocFileName);
		Readhtml rh = new Readhtml();
		String[] tInputEntry = new String[1];
		tInputEntry[0] = tDocFileName;
		rh.CreateZipFile(tInputEntry, zipfilepath);
		return 1;
	}

	private int compressIntoZip(String zipfilepath, String[] tDocFileName) {
		System.out.println("zipfilepath:" + zipfilepath);
		System.out.println("tDocFileName待压缩文件压缩路径:" + tDocFileName);
		Readhtml rh = new Readhtml();
		String[] tInputEntry = tDocFileName;
		rh.CreateZipFile(tInputEntry, zipfilepath);
		return 1;
	}

	/**
	 * 将报表查询文件压缩包发送邮件
	 * 
	 */
	private boolean sendSagentMail() {
		// 压缩文件生成成功，开始发送邮件
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
		SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
				.GetText(1, 2), "picchealth");
		// 设置邮件发送信息
		String sendInf = "高端医疗产品报送数据见附件！";
		String sendInfs = "";
		if(haveCB&&haveBQ){
			sendInfs = "";
		}else if(haveCB||haveBQ){
			sendInfs = haveCB?"其中保全无数据。":"其中契约无数据。";
		}
		String Fj = mZipFilePath;
		if(haveScan){
			Fj +="|"+mScanFilePath;
		}
		tMailSender.setSendInf("高端医疗产品报送数据", sendInf+sendInfs,Fj);
		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
		tMailSender.setToAddress("highendmedical");
		if (!tMailSender.sendMail()) {
			tMailSender.getErrorMessage();
		}
		deleteFile(tOutXmlPath);
		deleteFile(mZipFilePath);
		if(haveScan){
			deleteFile(mScanFilePath);
		}

		return true;

	}

	/**
	 * 删除生成的ZIP文件和XML文件
	 * 
	 * @param cFilePath
	 *            String：完整文件名
	 */
	private boolean deleteFile(String cFilePath) {
		File f = new File(cFilePath);
		if (!f.exists()) {
			CError tError = new CError();
			tError.moduleName = "MonthDataCatch";
			tError.functionName = "deleteFile";
			tError.errorMessage = "没有找到需要删除的文件";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		f.delete();
		return true;
	}

	private void sendMessage() {

		// 邮箱发送
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
		SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
				.GetText(1, 2), "picchealth");
		// 设置邮件发送信息
		String sendInf = "高端医疗产品报送数据中：契约、保全均无数据，请知悉~~";
		tMailSender.setSendInf("高端医疗产品报送数据", sendInf, null);
		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
		tMailSender.setToAddress("highendmedical");
		if (!tMailSender.sendMail()) {
			tMailSender.getErrorMessage();
		}

	}

	private boolean getReportZipFile() {
		String name = tOutXmlPath;
		if (FileExists(name)) {
			System.out.println("打印文件存在");
			getZipFile();
			sendSagentMail();
		} else {
			sendMessage();
			System.out.println("没有符合打印条件的数据");
		}

		return true;
	}

	private boolean getReprot() {

		fileName = "High_EndMedicalReport.xls";
		tOutXmlPath = mURL + fileName;
		System.out.println("文件名称====：" + tOutXmlPath);
		WriteToExcel t = new WriteToExcel(fileName);
		t.createExcelFile();
		t.setAlignment("CENTER");
		t.setFontName("宋体");
		String[] sheetName = { new String("契约数据报表"), new String("保全数据报表") };
		t.addSheetGBK(sheetName);
		if (!getData1()) {
			mToExcel = getDefalutData();
		}
		t.setData(0, mToExcel);
		if (!getData2()) {
			mToExcel2 = getDefalutData();
		}
		t.setData(1, mToExcel2);

		if (haveBQ || haveCB) {
			try {
				t.write(mURL);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		System.out.println("生成文件完成");

		return true;

	}

	private void getInputData() {
		// 设置管理机构
		mGI.ManageCom = "86";
		mGI.Operator = "001";
		mGI.ComCode = "86";

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL1 = new ExeSQL().getOneValue(tSQL);
//		mURL1 = "F:/";
		mURL = mURL1 + "vtsfile/";

		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}

	}

	private String[][] getDefalutData() {
		String[][] tToExcel = new String[2][30];
		tToExcel[0][0] = "无数据";
		return tToExcel;
	}

	public boolean getData1() {
		System.out.println("BL->dealDate()");

		ExeSQL tExeSQL = new ExeSQL();
		mSql =  " Select Lcc.Signdate 契约承保操作完成日期, "
				+ "        '新契约' 操作类型, "
				+ "        '新契约' 操作项目, "
				+ "        Lcc.Prtno 操作作业号, "
				+ "        Substr(Lcc.Managecom, 1, 4) 承保机构, "
				+ "        (Select Name "
				+ "         From Ldcom "
				+ "         Where Comcode = Substr(Lcc.Managecom, 1, 4)) 承保机构名称, "
				+ "        (Select Bigprojectname "
				+ "         From Lcbigprojectinfo Lcb,Lcbigprojectcont Lci "
				+ "         Where Lcb.Bigprojectno = Lci.Bigprojectno And Lci.Prtno =Lcc.Prtno fetch first 1 rows only) 项目名称, "
				+ "        Lcc.Grpcontno 保单号, "
				+ "        Lcc.Signdate 总单承保日期, "
				+ "        Lcc.Cvalidate 总单生效日期, "
				+ "        Lcc.Cinvalidate 总单终止日期, "
				+ "        Lcc.Grpname 投保单位名称, "
				+ "        Codename('stateflag', Lco.Stateflag) 分单状态, "
				+ "        lco.contno 分单号,"
				+ "        Lco.Signdate 分单承保日期, "
				+ "        Lco.Cvalidate 分单生效日, "
				+ "        Lco.Cinvalidate 分单终止日期, "
				+ "        '162001' 产品代码, "
				+ "        (Select Name "
				+ "         From Lcinsured "
				+ "         Where Contno = Lco.Contno "
				+ "         And Relationtomaininsured = '00') 主被保险人姓名, "
				+ "        Lcp.Insuredname 被保险人姓名, "
				+ "        Codename('relation', Lci.Relationtomaininsured) 与主被保险人关系, "
				+ "        Codename('idtype', Lci.Idtype) 证件类型, "
				+ "        Lci.Idno 证件号码, "
				+ "        Codename('sex', Lci.Sex) 性别, "
				+ "        Lci.Birthday 生日, "
				+ "        Lcc.Remark 总单特别约定, "
				+ "        (Select Calfactorvalue "
				+ "         From Lccontplandutyparam "
				+ "         Where Grpcontno = Lcc.Grpcontno "
				+ "         And Calfactor = 'InsuPlanCode' "
				+ "         And Contplancode = Lci.Contplancode "
				+ "         And Riskcode = '162001' Fetch First 1 Rows Only) 保障计划类型, "
				+ "        (Select Calfactorvalue "
				+ "         From Lccontplandutyparam "
				+ "         Where Grpcontno = Lcc.Grpcontno "
				+ "         And Calfactor = 'InsuArea' "
				+ "         And Contplancode = Lci.Contplancode "
				+ "         And Riskcode = '162001' Fetch First 1 Rows Only) 承保区域 "
				+ " From Lcgrpcont Lcc, " + "      Lcgrppol Lcg, "
				+ "      Lccont Lco, " + "      Lcpol Lcp, "
				+ "      Lcinsured Lci "
				+ " Where Lcc.Grpcontno = Lcg.Grpcontno "
				+ " And Lcg.Riskcode = '162001' "
				+ " And Lcp.Grppolno = Lcg.Grppolno "
				+ " And Lcc.Grpcontno = Lco.Grpcontno "
				+ " And Lco.Contno = Lcp.Contno "
				+ " And Lcp.Contno = Lci.Contno "
				+ " And lcc.SignDate= Current date - 1 day "
				+ " And Substr(Lcc.Managecom, 1, 4) Not In (Select Code From Ldcode Where Codetype = 'HighendMedicalCom') "
				+ " And Lcp.Insuredno = Lci.Insuredno "
				+ " And Lcc.Appflag = '1'  " + " And Lco.Appflag = '1'  "
				+ " And Not Exists (Select 1 " + "        From Ljagetendorse "
				+ "        Where Contno = Lco.Contno "
				+ "        And Feeoperationtype = 'NI') ";

		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);

		int count = tSSRS.getMaxRow();
		mToExcel = new String[count + 4][30];
		mToExcel[0][0] = "契约承保操作完成日期";
		mToExcel[0][1] = "操作类型";
		mToExcel[0][2] = "操作项目";
		mToExcel[0][3] = "操作作业号";
		mToExcel[0][4] = "承保机构";
		mToExcel[0][5] = "承保机构名称";
		mToExcel[0][6] = "项目名称";
		mToExcel[0][7] = "保单号";
		mToExcel[0][8] = "总单承保日期";
		mToExcel[0][9] = "总单生效日期";
		mToExcel[0][10] = "总单终止日期";
		mToExcel[0][11] = "投保单位名称";
		mToExcel[0][12] = "分单状态";
		mToExcel[0][13] = "分单号";
		mToExcel[0][14] = "分单承保日期";
		mToExcel[0][15] = "分单生效日期";
		mToExcel[0][16] = "分单终止日期";
		mToExcel[0][17] = "产品代码";
		mToExcel[0][18] = "主被保险人姓名";
		mToExcel[0][19] = "被保险人姓名";
		mToExcel[0][20] = "与主被保险人关系";
		mToExcel[0][21] = "证件类型";
		mToExcel[0][22] = "证件号码";
		mToExcel[0][23] = "性别";
		mToExcel[0][24] = "生日";
		mToExcel[0][25] = "总单特别约定";
		mToExcel[0][26] = "保障计划类型";
		mToExcel[0][27] = "承保区域";

		if (count > 0) {
			haveCB = true;
		}

		for (int i = 1; i <= count; i++) {
			mToExcel[i][0] = tSSRS.GetText(i, 1);
			mToExcel[i][1] = tSSRS.GetText(i, 2);
			mToExcel[i][2] = tSSRS.GetText(i, 3);
			mToExcel[i][3] = tSSRS.GetText(i, 4);
			mToExcel[i][4] = tSSRS.GetText(i, 5);
			mToExcel[i][5] = tSSRS.GetText(i, 6);
			mToExcel[i][6] = tSSRS.GetText(i, 7);
			mToExcel[i][7] = tSSRS.GetText(i, 8);
			mToExcel[i][8] = tSSRS.GetText(i, 9);
			mToExcel[i][9] = tSSRS.GetText(i, 10);
			mToExcel[i][10] = tSSRS.GetText(i, 11);
			mToExcel[i][11] = tSSRS.GetText(i, 12);
			mToExcel[i][12] = tSSRS.GetText(i, 13);
			mToExcel[i][13] = tSSRS.GetText(i, 14);
			mToExcel[i][14] = tSSRS.GetText(i, 15);
			mToExcel[i][15] = tSSRS.GetText(i, 16);
			mToExcel[i][16] = tSSRS.GetText(i, 17);
			mToExcel[i][17] = tSSRS.GetText(i, 18);
			mToExcel[i][18] = tSSRS.GetText(i, 19);
			mToExcel[i][19] = tSSRS.GetText(i, 20);
			mToExcel[i][20] = tSSRS.GetText(i, 21);
			mToExcel[i][21] = tSSRS.GetText(i, 22);
			mToExcel[i][22] = tSSRS.GetText(i, 23);
			mToExcel[i][23] = tSSRS.GetText(i, 24);
			mToExcel[i][24] = tSSRS.GetText(i, 25);
			mToExcel[i][25] = tSSRS.GetText(i, 26);
			mToExcel[i][26] = tSSRS.GetText(i, 27);
			mToExcel[i][27] = tSSRS.GetText(i, 28);
		}

		return true;
	}

	public boolean getData2() {
		System.out.println("BL->dealDate()");

		ExeSQL tExeSQL = new ExeSQL();
		mSql = " select lpp.confdate ,'保全',(select edorname from lmedoritem where edorcode=lpg.edortype),lpp.edoracceptno,substr(lcc.managecom,1,4), "
				+ " (select name from ldcom where comcode=substr(lcc.managecom,1,4)), "
				+ " (Select Bigprojectname From Lcbigprojectinfo Lcb,Lcbigprojectcont Lci Where Lcb.Bigprojectno = Lci.Bigprojectno And Lci.Prtno = Lcc.PrtNo), "
				+ " lcc.grpcontno,lcc.signdate,lcc.cvalidate,lcc.cinvalidate, "
				+ " lcc.grpname,codename('stateflag',lco.stateflag), "
				+ " lco.contno,lco.signdate,lco.cvalidate,lco.cinvalidate,'162001', "
				+ " (select name from lcinsured where contno=lco.contno and RelationToMainInsured='00'), "
				+ " lcp.insuredname,codename('relation',lci.RelationToMainInsured), "
				+ " codename('idtype',lci.idtype), "
				+ " lci.idno,codename('sex',lci.sex),lci.Birthday,lcc.remark, "
				+ " (select CalFactorValue from lccontplandutyparam where grpcontno=lcc.grpcontno and calfactor='InsuPlanCode' and ContPlanCode=lci.ContPlanCode and riskcode='162001' fetch first 1 rows only), "
				+ " (select CalFactorValue from lccontplandutyparam where grpcontno=lcc.grpcontno and calfactor='InsuArea' and ContPlanCode=lci.ContPlanCode and riskcode='162001' fetch first 1 rows only) "
				+ " from lpgrpedoritem lpg,lpedorapp lpp,lcgrpcont lcc,lcgrppol lcg,lccont lco,lcpol lcp,lcinsured lci "
				+ " where lpg.grpcontno=lcg.grpcontno "
				+ " and lpg.edorno=lpp.edoracceptno "
				+ " and lpg.edortype ='NI' "
				+ " and lpp.edorstate='0' "
				+ " and lcg.riskcode='162001' "
				+ " and lcp.grppolno=lcg.grppolno "
				+ " and lcc.grpcontno=lco.grpcontno "
				+ " and lco.contno=lcp.contno "
				+ " and lcp.contno=lci.contno "
				+ " and lcp.insuredno=lci.insuredno "
				+ " and lcc.appflag='1' "
				+ " and lco.appflag='1' "
				+ " and lpp.confdate=current date - 1 day "
				+ " And Substr(Lcc.Managecom, 1, 4) Not In (Select Code From Ldcode Where Codetype = 'HighendMedicalCom') "
				+ " and exists (select 1 from ljagetendorse  where contno=lco.contno and feeoperationtype='NI') "
				+ " union all "
				+ " select lpp.confdate ,'保全',(select edorname from lmedoritem where edorcode=lpg.edortype),lpp.edoracceptno,substr(lcc.managecom,1,4), "
				+ " (select name from ldcom where comcode=substr(lcc.managecom,1,4)), "
				+ " (Select Bigprojectname From Lcbigprojectinfo Lcb,Lcbigprojectcont Lci Where Lcb.Bigprojectno = Lci.Bigprojectno And Lci.Prtno = Lcc.PrtNo), "
				+ " lcc.grpcontno,lcc.signdate,lcc.cvalidate,lcc.cinvalidate, "
				+ " lcc.grpname,codename('stateflag',lco.stateflag), "
				+ " lco.contno,lco.signdate,lco.cvalidate,lco.cinvalidate,'162001', "
				+ " (select name from lbinsured where contno=lco.contno and RelationToMainInsured='00'), "
				+ " lcp.insuredname,codename('relation',lci.RelationToMainInsured), "
				+ " codename('idtype',lci.idtype), "
				+ " lci.idno,codename('sex',lci.sex),lci.Birthday,lcc.remark, "
				+ " (select CalFactorValue from lccontplandutyparam where grpcontno=lcc.grpcontno and calfactor='InsuPlanCode' and ContPlanCode=lci.ContPlanCode and riskcode='162001' fetch first 1 rows only), "
				+ " (select CalFactorValue from lccontplandutyparam where grpcontno=lcc.grpcontno and calfactor='InsuArea' and ContPlanCode=lci.ContPlanCode and riskcode='162001' fetch first 1 rows only) "
				+ " from lpgrpedoritem lpg,lpedorapp lpp,lcgrpcont lcc,lcgrppol lcg,lbcont lco,lbpol lcp,lbinsured lci "
				+ " where lpg.grpcontno=lcg.grpcontno "
				+ " and lpg.edorno=lpp.edoracceptno "
				+ " and lpg.edortype ='ZT' "
				+ " and lpp.edorstate='0' "
				+ " and lcg.riskcode='162001' "
				+ " and lcp.grppolno=lcg.grppolno "
				+ " and lcc.grpcontno=lco.grpcontno "
				+ " and lco.contno=lcp.contno "
				+ " and lcp.contno=lci.contno "
				+ " and lcp.insuredno=lci.insuredno "
				+ " and lcc.appflag='1' "
				+ " and lco.appflag='1' "
				+ " And Substr(Lcc.Managecom, 1, 4) Not In (Select Code From Ldcode Where Codetype = 'HighendMedicalCom') "
				+ " and lpp.confdate=current date - 1 day "
				+ " and exists (select 1 from ljagetendorse  where contno=lco.contno and feeoperationtype='ZT') ";

		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);

		int count = tSSRS.getMaxRow();
		mToExcel2 = new String[count + 4][30];
		mToExcel2[0][0] = "保全确认日期";
		mToExcel2[0][1] = "操作类型";
		mToExcel2[0][2] = "操作项目";
		mToExcel2[0][3] = "操作作业号";
		mToExcel2[0][4] = "承保机构代码";
		mToExcel2[0][5] = "承保机构名称";
		mToExcel2[0][6] = "项目名称";
		mToExcel2[0][7] = "保单号";
		mToExcel2[0][8] = "总单承保日期";
		mToExcel2[0][9] = "总单生效日期";
		mToExcel2[0][10] = "总单终止日期";
		mToExcel2[0][11] = "投保单位名称";
		mToExcel2[0][12] = "分单状态";
		mToExcel2[0][13] = "分单号";
		mToExcel2[0][14] = "分单承保日期";
		mToExcel2[0][15] = "分单生效日期";
		mToExcel2[0][16] = "分单终止日期";
		mToExcel2[0][17] = "产品代码";
		mToExcel2[0][18] = "主被保险人姓名";
		mToExcel2[0][19] = "被保险人姓名";
		mToExcel2[0][20] = "与主被保险人关系";
		mToExcel2[0][21] = "证件类型";
		mToExcel2[0][22] = "证件号码";
		mToExcel2[0][23] = "性别";
		mToExcel2[0][24] = "生日";
		mToExcel2[0][25] = "总单特别约定";
		mToExcel2[0][26] = "保障计划类型";
		mToExcel2[0][27] = "承保区域";

		if (count > 0) {
			haveBQ = true;
		}
		for (int i = 1; i <= count; i++) {
			mToExcel2[i][0] = tSSRS.GetText(i, 1);
			mToExcel2[i][1] = tSSRS.GetText(i, 2);
			mToExcel2[i][2] = tSSRS.GetText(i, 3);
			mToExcel2[i][3] = tSSRS.GetText(i, 4);
			mToExcel2[i][4] = tSSRS.GetText(i, 5);
			mToExcel2[i][5] = tSSRS.GetText(i, 6);
			mToExcel2[i][6] = tSSRS.GetText(i, 7);
			mToExcel2[i][7] = tSSRS.GetText(i, 8);
			mToExcel2[i][8] = tSSRS.GetText(i, 9);
			mToExcel2[i][9] = tSSRS.GetText(i, 10);
			mToExcel2[i][10] = tSSRS.GetText(i, 11);
			mToExcel2[i][11] = tSSRS.GetText(i, 12);
			mToExcel2[i][12] = tSSRS.GetText(i, 13);
			mToExcel2[i][13] = tSSRS.GetText(i, 14);
			mToExcel2[i][14] = tSSRS.GetText(i, 15);
			mToExcel2[i][15] = tSSRS.GetText(i, 16);
			mToExcel2[i][16] = tSSRS.GetText(i, 17);
			mToExcel2[i][17] = tSSRS.GetText(i, 18);
			mToExcel2[i][18] = tSSRS.GetText(i, 19);
			mToExcel2[i][19] = tSSRS.GetText(i, 20);
			mToExcel2[i][20] = tSSRS.GetText(i, 21);
			mToExcel2[i][21] = tSSRS.GetText(i, 22);
			mToExcel2[i][22] = tSSRS.GetText(i, 23);
			mToExcel2[i][23] = tSSRS.GetText(i, 24);
			mToExcel2[i][24] = tSSRS.GetText(i, 25);
			mToExcel2[i][25] = tSSRS.GetText(i, 26);
			mToExcel2[i][26] = tSSRS.GetText(i, 27);
			mToExcel2[i][27] = tSSRS.GetText(i, 28);
		}

		return true;
	}

	private boolean getScan() {
		ExeSQL tExeSQL = new ExeSQL();
		mSql = " Select distinct Lcc.Prtno,lcc.grpcontno  "
				+ " From Lcgrpcont Lcc, "
				+ "      Lcgrppol Lcg "
				+ " Where Lcc.Grpcontno = Lcg.Grpcontno "
				+ " And Lcg.Riskcode = '162001' "
				+ " And lcc.SignDate= Current date - 1 day "
				+ " And Substr(Lcc.Managecom, 1, 4) Not In (Select Code From Ldcode Where Codetype = 'HighendMedicalCom') "
				+ " And exists ( select 1 from es_Doc_main where doccode=lcc.prtno and subtype='TB02' ) "
				+ " And Lcc.Appflag = '1'  ";
		System.out.println(mSql);
		String zipfilepath = "";
		tSSRS = tExeSQL.execSQL(mSql);
		int zipsize = 0;
		int count = tSSRS.getMaxRow();
		if (count == 0) {
			System.out.println("无扫描件");
		} else {
			mScanFilePath = mURL + "Scan.zip";
			String[] sumzip = new String[count];
			String tPrtNo = "";
			String tGrpContNo = "";
			for (int i = 1; i <= count; i++) {
				tPrtNo = tSSRS.GetText(i, 1);
				tGrpContNo = tSSRS.GetText(i, 2);
				zipfilepath = mURL + tGrpContNo + ".zip";
				sumzip[i - 1] = zipfilepath;
				mSql = "select esp.PicPath||esp.PageName||'.tif',esp.PicPath||esp.PageName||'.jpg' from es_doc_main esd,es_doc_pages esp where esd.docid=esp.docid and esd.subtype='TB02' and esd.doccode='"
						+ tPrtNo + "' ";
				SSRS tSSRS2 = tExeSQL.execSQL(mSql);
				int size = tSSRS2.getMaxRow();
				if (size > 0) {
					zipsize = zipsize + 1;
					String[] zipsource = new String[size];
					for (int j = 1; j <= tSSRS2.getMaxRow(); j++) {
						zipsource[j - 1] = mURL1 + tSSRS2.GetText(j, 1);
						File mFile = new File(zipsource[j - 1]);
						if (!mFile.exists()) {
							System.out.println("文件不存在");
							zipsource[j - 1] = mURL1 + tSSRS2.GetText(j, 2);
						}
					}
					compressIntoZip(zipfilepath, zipsource);
				} else {
					continue;
				}

			}
			compressIntoZip(mScanFilePath, sumzip);
			haveScan = true;
		}

		return true;

	}

	// 主方法 测试用
	public static void main(String[] args) {
		new HighendMedicalDockTask().run();

	}
}
