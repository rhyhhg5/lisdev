package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.operfee.IndiDueFeeMultiBL;
import com.sinosoft.utility.*;
import com.sinosoft.lis.operfee.IndiDueFeeMultiForChildBL;


/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 个险续期续保批处理程序
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author fuxin
 * @version 1.0
 */
public class IndiDueFeeBatchTask extends TaskThread
{
    //查找范围
    private static final int STEP_DAYS=60;
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();
    // 得到当前时间
    private String mCurrentDate = PubFun.getCurrentDate();
    
    // 输入数据的容器
    private VData tVData = new VData();
    
    private IndiDueFeeMultiForChildBL tIndiDueFeeMultiForChildBL = new IndiDueFeeMultiForChildBL();
    private String startDate="2005-01-01",endDate=null;
    private GlobalInput mGlobalInput = null;

    public IndiDueFeeBatchTask()
    {
        mGlobalInput = new GlobalInput();
        mGlobalInput.Operator="Server";
        mGlobalInput.ComCode="86";
        mGlobalInput.ManageCom="86";
    }


    /**
     * 实现TaskThread的接口方法run
     * 由TaskThread调用
     * */
    public void run()
    {   
    	endDate=addDate(mCurrentDate,STEP_DAYS);
    	dealDueFee();
    	dealDSDueFee();
        dealULIDueFee();
    }
    public void runSomeCont(String mStartDate,String mEndDate){
    	startDate=mStartDate;
    	endDate=mEndDate;
    	dealDueFee();
    	dealDSDueFee();
        dealULIDueFee();
    }
    public void initData()
    {
    	tVData.clear();
//        endDate=addDate(mCurrentDate,STEP_DAYS);
        System.out.print(endDate);
        TransferData tTransferData=new TransferData();
        tTransferData.setNameAndValue("StartDate",startDate);
        tTransferData.setNameAndValue("EndDate",endDate);
        tTransferData.setNameAndValue("ManageCom",mGlobalInput.ManageCom);
        tTransferData.setNameAndValue("BusinessFlag",""); //万能险标识，这处理普通险种所以为空。
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);
       
    }
    public void initULIData()
    {
    	tVData.clear();
 //      endDate=addDate(mCurrentDate,STEP_DAYS);
       System.out.print(endDate);
       TransferData tTransferData=new TransferData();
       tTransferData.setNameAndValue("StartDate",startDate);
       tTransferData.setNameAndValue("EndDate",endDate);
       tTransferData.setNameAndValue("ManageCom",mGlobalInput.ManageCom);
       tTransferData.setNameAndValue("BusinessFlag","omnip"); //万能险标识。
       tVData.add(mGlobalInput);
       tVData.add(tTransferData);
    }
    private String addDate(String date,int days)
    {
        return PubFun.calDate(date,days-1,"D","1999-12-31");
    }
    
    //普通险种 无电商
    private void dealDueFee()
    {
    	IndiDueFeeMultiBL I1 = new IndiDueFeeMultiBL();
    	initData();
        try
        {
            //普通险种续期、续保催收
        	I1.submitData(tVData, "INSERT");
        }
        catch(Exception ex)
        {
            this.mErrors = I1.mErrors;
            ex.getMessage();
        }
    }
    //普通险种 电商
    private void dealDSDueFee()
    {
    	IndiDueFeeMultiBL I1 = new IndiDueFeeMultiBL();
    	initData();
        try
        {
            //普通险种续期、续保催收
        	I1.submitData(tVData, "DSINSERT");
        }
        catch(Exception ex)
        {
            this.mErrors = I1.mErrors;
            ex.getMessage();
        }
    }
    
    
//  万能险种
    private void dealULIDueFee()
    {
    	IndiDueFeeMultiBL I2 = new IndiDueFeeMultiBL();
    	initULIData();
        try
        {
        	I2.submitData(tVData, "INSERT");
        }
        catch(Exception ex)
        {
            this.mErrors = I2.mErrors;
            ex.getMessage();
        }
    }
    public static void main(String args[])
    {  
        IndiDueFeeBatchTask tIndi = new IndiDueFeeBatchTask();
        tIndi.runSomeCont("2019-1-2","2019-1-2");
        if(tIndi.mErrors.needDealError())
        {
           System.out.println(tIndi.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}
