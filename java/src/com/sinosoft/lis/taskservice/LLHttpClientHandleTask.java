package com.sinosoft.lis.taskservice;

 
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *秘钥接口批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author zhang
 * @version 1.1
 */
public class LLHttpClientHandleTask extends TaskThread {
	/**
	 * 错误的容器
	 */
	public CErrors mErros = new CErrors();
	
	String opr="";
	
	public LLHttpClientHandleTask(){
		
	}
	
	public void run(){
		
		HttpClientHandle tHttpClientHandle = new HttpClientHandle();
		String result = null;
		try {
			result = tHttpClientHandle.getAesKey();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(result==null || result.length()==0){
			System.out.println("四川，数联易康秘钥接口，获取秘钥失败");
			System.out.println(mErros.getFirstError());
			opr="false";
		}else{
			System.out.println("四川，数联易康秘钥接口，获取秘钥成功"+result);
			opr="true";
		}
		
	}
	
	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	public static void main(String[] args) {
		 
		
	}

}
