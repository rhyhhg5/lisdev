package com.sinosoft.lis.taskservice;

import com.ecwebservice.subinterface.DelUnSignContBL;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: 复杂网销未承保垃圾数据删除</p>
 * <p>Copyright: Copyright (c) 2014-1-21</p>
 *
 * @author licaiyan
 * @version 1.0
 */
public class WxUnSignContDelTask extends TaskThread {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
   
    // 输入数据的容器
    public WxUnSignContDelTask() {
    }

    /**
     * 实现TaskThread的接口方法run
     * 由TaskThread调用
     * */
    public void run() {
        if (!deleteWxUnSign()) {
            return;
        }
    }
    /**
     * 复杂网销未承保垃圾数据删除
     * @return
     */
    private boolean deleteWxUnSign() {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "EC_WX";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        DelUnSignContBL tUnSignBL = new DelUnSignContBL();
        if (!tUnSignBL.submitData(mVData, "WXDEL")) {
            System.out.println("短信提醒发送有误！");
            CError tError = new CError();
            tError.moduleName = "DelUnSignContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "网销新单删除批处理失败！！！！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String args[]) {
        //WxUnSignContDelTask a = new WxUnSignContDelTask();
       // a.run();

    }
}
