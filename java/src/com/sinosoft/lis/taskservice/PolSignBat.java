package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.workflow.tb.TbWorkFlowUI;
import com.sinosoft.workflow.brieftb.BriefTbWorkFlowBL;
import com.sinosoft.workflow.brieftb.BriefTbWorkFlowUI;

/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author Liuliang
 * @version 1.0
 */

public class PolSignBat extends TaskThread
{
    private int strSql;

    private static int intStartIndex;

    private static int intMaxIndex;

    public PolSignBat()
    {
        intStartIndex = 1;
        intMaxIndex = 1;
    }

    public void run()
    {
        System.out.println("");
        System.out
                .println("＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
        System.out.println("参数个数:" + mParameters.size());
        System.out.println("");
        // Write your code here

        PolSignBat a = new PolSignBat();
        String[] tPram = new String[4];
        tPram = a.EasyQuery(intStartIndex);
        String valiRiskSQL="";
        String valiSQL="";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS RiskResult = new SSRS();
        SSRS arrResult = new SSRS();

        //开始批量签单
        for (int i = 1; i <= intMaxIndex; i++)
        {
            //先查询待签的记录集
            tPram = a.EasyQuery(intStartIndex);
            if (tPram == null)
            {
                return;
            }
            
            valiRiskSQL = "select 1 from lcriskdutywrap where prtno = '"+tPram[1]+"' and riskwrapcode in ( select code from ldcode where codetype = 'valiscancopy' and othersign = '1' )"
                        + " with ur";
        	RiskResult = tExeSQL.execSQL(valiRiskSQL);
        	if(RiskResult!=null && RiskResult.getMaxRow()>0){
        		valiSQL = "select 1 from es_doc_main where doccode = '"+ tPram[1] +"' with ur";
        		arrResult = tExeSQL.execSQL(valiSQL);
        	     if(arrResult == null || arrResult.getMaxRow() == 0){ 
                    System.out.println("保单["+tPram[1]+"]，该产品并未上传扫描件，不能签单！");
                    intStartIndex++;
        	    	continue;
        	     }
        	}
        	
        	 valiRiskSQL =" select riskcode "
                          + " from ( "
                          + " select pol.riskcode riskcode,pol.prem+pol.SupplementaryPrem prem, "
                          + " GETBASPREM(pol.riskcode,pol.insuredappage,pol.prem,pol.amnt,pol.payintv)*double(ld.codename) basfee, "
                          + " GETEXPREM(pol.riskcode,pol.insuredappage,pol.prem,pol.amnt,pol.payintv)*double(ld.codealias) exfee, "
                          + " pol.SupplementaryPrem*double(ld.comcode) supfee,pol.amnt amnt,double(ld.othersign) rate "
                          + " from lcpol pol,ldcode ld "
                          + " where pol.prtno='"+tPram[1]+"' "
                          + " and ld.codetype='checkaccprem' "
                          + " and ld.code=pol.riskcode " 
                          + " ) temp "
                          + " where (prem-basfee-exfee-supfee)*rate>amnt ";
        	 RiskResult = tExeSQL.execSQL(valiRiskSQL);
        	 if(RiskResult!=null && RiskResult.getMaxRow()>0){
         		System.out.println("保单["+tPram[1]+"]，账户价值也保额关系不符合规则，不能签单！");
                intStartIndex++;
         	    continue;
         	}        	
            
            if (!a.PolSign(tPram[0], tPram[1], tPram[2], tPram[3], tPram[4]))
            {
                //如果签单不成功,继续签下一条;
                intStartIndex++;
            }
        }

    }

    public static void main(String[] args)
    {
        PolSignBat a = new PolSignBat();
        a.run();
        //        String[] tPram = new String[3];
        ////        tPram = a.EasyQuery(intStartIndex);
        //
        //        //开始批量签单
        //        for (int i = 1; i < intMaxIndex; i++) {
        //            //先查询待签的记录集
        //            tPram = a.EasyQuery(intStartIndex);
        //            if (!a.PolSign(tPram[0], tPram[1], tPram[2], tPram[3], tPram[4])) {
        //                //如果签单不成功,继续签下一条;
        //                intStartIndex++;
        //            }
        //        }
    }

    public boolean PolSign(String ContNo, String PtrNo, String MissionID,
            String SubMissionID, String tActivityID)
    {
        String FlagStr = "";
        TransferData tTransferData = new TransferData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "000"; //系统自签
        tG.ManageCom = "86"; //默认总公司
        //接收信息
        // 投保单列表
        LCContSchema tLCContSchema = new LCContSchema();
        LCContSet tLCContSet = new LCContSet();
        boolean flag = false;
        if (ContNo != null)
        {
            System.out.println("ContNo:" + ":" + ContNo);
            tLCContSchema.setContNo(ContNo);
            tLCContSchema.setPrtNo(PtrNo);
            tLCContSet.add(tLCContSchema);
            tTransferData.setNameAndValue("MissionID", MissionID);
            tTransferData.setNameAndValue("SubMissionID", SubMissionID);
            flag = true;

        }

        if (flag == true)
        {
            // 准备传输数据 VData
            VData tVData = new VData();

            tVData.add(tLCContSet);
            tVData.add(tTransferData);
            tVData.add(tG);
            tVData.add(tLCContSchema);
            // 数据传输
            //LCGrpContSignBL tLCGrpContSignBL   = new LCGrpContSignBL();

            //boolean bl = tLCGrpContSignBL.submitData( tVData, "INSERT" );
            //int n = tLCGrpContSignBL.mErrors.getErrorCount();
            if (tActivityID.equals("0000001150")
                    || tActivityID.equals("0000009004"))
            {
                TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
                boolean bl = tTbWorkFlowUI.submitData(tVData, tActivityID);
                int n = tTbWorkFlowUI.mErrors.getErrorCount();
                if (n == 0)
                {
                    if (bl)
                    {
                        System.out.print(" 签单成功! ");
                        return true;

                    }
                    else
                    {
                        System.out.print(" 集体投保单签单失败! ");
                        return false;

                    }
                }
                else
                {

                    String strErr = "";
                    for (int i = 0; i < n; i++)
                    {
                        strErr += (i + 1)
                                + ": "
                                + tTbWorkFlowUI.mErrors.getError(i).errorMessage
                                + "; ";
                        System.out
                                .println(tTbWorkFlowUI.mErrors.getError(i).errorMessage);
                    }
                    if (bl)
                    {

                        System.out.print(" 部分签单成功,但是有如下信息: " + strErr);
                        return false;
                    }
                    else
                    {
                        System.out.print("集体投保单签单失败，原因是: " + strErr);

                        return false;
                    }
                } // end of if
            }
            else if (tActivityID.equals("0000007002")||tActivityID.equals("0000013002"))
            {
                BriefTbWorkFlowUI tTbWorkFlowUI = new BriefTbWorkFlowUI();
                boolean bl = tTbWorkFlowUI.submitData(tVData, tActivityID);
                int n = tTbWorkFlowUI.mErrors.getErrorCount();
                if (n == 0)
                {
                    if (bl)
                    {
                        System.out.print(" 签单成功! ");
                        return true;

                    }
                    else
                    {
                        System.out.print(" 集体投保单签单失败! ");
                        return false;

                    }
                }
                else
                {

                    String strErr = "";
                    for (int i = 0; i < n; i++)
                    {
                        strErr += (i + 1)
                                + ": "
                                + tTbWorkFlowUI.mErrors.getError(i).errorMessage
                                + "; ";
                        System.out
                                .println(tTbWorkFlowUI.mErrors.getError(i).errorMessage);
                    }
                    if (bl)
                    {

                        System.out.print(" 部分签单成功,但是有如下信息: " + strErr);
                        return false;
                    }
                    else
                    {
                        System.out.print("集体投保单签单失败，原因是: " + strErr);
                        return false;
                    }
                } // end of if
            }
        } // end of if
        return false;
    }

    public String[] EasyQuery(int intStartIndex)
    {
        SSRS tSSRS = new SSRS();

        String sql = "select  lwm.missionprop1, lwm.missionprop2, lwm.missionid, lwm.submissionid, lwm.activityid "
                + "from lwmission lwm "
                + " inner join LCCont lcc on lwm.MissionProp2 = lcc.PrtNo  "
                //+ " and lcc.prtno='168201209261' "
                + " where 1=1 "
                + "  and (processid = '0000000003' and activityid = '0000001150' "
                + "    or processid = '0000000009' and activityid = '0000009004')"
                //                + " and missionprop2 in "
                //                + "(select otherno  from ljtempfee where othernotype = '4' "
                //                + "and confflag='0' and enteraccdate is not null)"
                + " and lcc.UWFlag in ('4', '9') "
                + " and lcc.Prem != 0 "
                + " and "
                + " ("
                + "     select nvl(sum(ljtf.PayMoney), 0)"
                + "     from LJTempFee ljtf"
                + "     where 1 = 1"
                + "     and ljtf.ConfFlag = '0'"
                + "     and ljtf.EnterAccDate is not null"
                + "     and ljtf.OtherNoType = '4'"
                + "     and ljtf.OtherNo = lcc.PrtNo "
                + " ) != 0 "
                + " and "
                + " round(("
                + "     ("
                + "         select "
                + "         nvl("
                + "         ("
                + "             select nvl(sum(ljtf.PayMoney), 0)"
                + "             from LJTempFee ljtf"
                + "             where 1 = 1"
                + "             and ljtf.ConfFlag = '0'"
                + "             and ljtf.EnterAccDate is not null"
                + "             and ljtf.OtherNoType = '4'"
                + "             and ljtf.OtherNo = lcc.PrtNo "
                + "         )"
                + "         -"
                + "         ("
                + "         select nvl(sum(nvl(lcp.Prem, 0) + nvl(lcp.SupplementaryPrem, 0)), 0)"
                + "         from LCPol lcp"
                + "         where 1 = 1"
                + "         and lcp.UWFlag in ('4', '9')"
                + "         and lcp.ContNo = lcc.ContNo"
                + "         ), 0) "
                + "         from Dual"
                + "     )"
                + " ),6) >= 0 "
                + " union "
                + "select  lwm.missionprop1, lwm.missionprop2, lwm.missionid, lwm.submissionid, lwm.activityid "
                + "from lwmission lwm "
                + " inner join LCCont lcc on lwm.MissionProp2 = lcc.PrtNo  "
                //+ " and lcc.prtno='168201209261' "
                + " where 1=1 and processid = '0000000007' and "
                + "activityid = '0000007002' "
                //                + " and missionprop2 in "
                //                + "(select otherno  from ljtempfee where othernotype = '4' "
                //                + "and confflag='0' and enteraccdate is not null) "
                + " and lcc.UWFlag in ('4', '9') "
                + " and lcc.Prem != 0 "
                + " and "
                + " ("
                + "     select nvl(sum(ljtf.PayMoney), 0)"
                + "     from LJTempFee ljtf"
                + "     where 1 = 1"
                + "     and ljtf.ConfFlag = '0'"
                + "     and ljtf.EnterAccDate is not null"
                + "     and ljtf.OtherNoType = '4'"
                + "     and ljtf.OtherNo = lcc.PrtNo "
                + " ) != 0 "
                + " and "
                + " round(("
                + "     ("
                + "         select "
                + "         nvl("
                + "         ("
                + "             select nvl(sum(ljtf.PayMoney), 0)"
                + "             from LJTempFee ljtf"
                + "             where 1 = 1"
                + "             and ljtf.ConfFlag = '0'"
                + "             and ljtf.EnterAccDate is not null"
                + "             and ljtf.OtherNoType = '4'"
                + "             and ljtf.OtherNo = lcc.PrtNo "
                + "         )"
                + "         -"
                + "         ("
                + "         select nvl(sum(nvl(lcp.Prem, 0) + nvl(lcp.SupplementaryPrem, 0)), 0)"
                + "         from LCPol lcp"
                + "         where 1 = 1"
                + "         and lcp.UWFlag in ('4', '9')"
                + "         and lcp.ContNo = lcc.ContNo"
                + "         ), 0) "
                + "         from Dual"
                + "     )"
                + " ),6) >= 0 "
                //2014-8-7 新增对于家庭单自动签单处理
                + " union "
                + "select  lwm.missionprop1, lwm.missionprop2, lwm.missionid, lwm.submissionid, lwm.activityid "
                + "from lwmission lwm "
                + " inner join LCCont lcc on lwm.MissionProp2 = lcc.PrtNo  "
                //+ " and lcc.prtno='168201209261' "
                + " where 1=1 and processid = '0000000013' and "
                + "activityid = '0000013002' "
                //                + " and missionprop2 in "
                //                + "(select otherno  from ljtempfee where othernotype = '4' "
                //                + "and confflag='0' and enteraccdate is not null) "
                + " and lcc.UWFlag in ('4', '9') "
                + " and lcc.Prem != 0 "
                + " and "
                + " ("
                + "     select nvl(sum(ljtf.PayMoney), 0)"
                + "     from LJTempFee ljtf"
                + "     where 1 = 1"
                + "     and ljtf.ConfFlag = '0'"
                + "     and ljtf.EnterAccDate is not null"
                + "     and ljtf.OtherNoType = '4'"
                + "     and ljtf.OtherNo = lcc.PrtNo "
                + " ) != 0 "
                + " and "
                + " round(("
                + "     ("
                + "         select "
                + "         nvl("
                + "         ("
                + "             select nvl(sum(ljtf.PayMoney), 0)"
                + "             from LJTempFee ljtf"
                + "             where 1 = 1"
                + "             and ljtf.ConfFlag = '0'"
                + "             and ljtf.EnterAccDate is not null"
                + "             and ljtf.OtherNoType = '4'"
                + "             and ljtf.OtherNo = lcc.PrtNo "
                + "         )"
                + "         -"
                + "         ("
                + "         select nvl(sum(nvl(lcp.Prem, 0) + nvl(lcp.SupplementaryPrem, 0)), 0)"
                + "         from LCPol lcp"
                + "         where 1 = 1"
                + "         and lcp.UWFlag in ('4', '9')"
                + "         and lcp.ContNo = lcc.ContNo"
                + "         ), 0) "
                + "         from Dual"
                + "     )"
                + " ),6) >= 0 "
                + "order by missionid desc";
        System.out.println("\n" + sql);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        intMaxIndex = tSSRS.getMaxRow();
        if (tSSRS.getMaxRow() < intStartIndex)
        {
            return null;
        }
        String[] getRowData = tSSRS.getRowData(intStartIndex);
        return getRowData;
    }
    
    public static void main(){
    	PolSignBat tPolSignBat = new PolSignBat();
    	tPolSignBat.run();
    }
}
