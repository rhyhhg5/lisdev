package com.sinosoft.lis.taskservice;

//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//
//import org.jdom.Document;
//import org.jdom.Element;
//
//import com.sinosoft.lis.certifybusiness.DentisServiceClient;
//import com.sinosoft.lis.pubfun.GlobalInput;
//import com.sinosoft.lis.pubfun.MMap;
//import com.sinosoft.lis.pubfun.PubFun;
//import com.sinosoft.lis.pubfun.PubSubmit;
//import com.sinosoft.lis.schema.LICardUploadLogSchema;
//import com.sinosoft.lis.vschema.LICardUploadLogSet;
//import com.sinosoft.lis.yibaotong.JdomUtil;
//import com.sinosoft.utility.CError;
import com.sinosoft.lis.jkxpt.LPJKXTwoServiceImp;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.ReportPubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LPJKXServiceTaskadd extends TaskThread
{
    //	private String[] mCaseNos;
    private SSRS mSSRS;//存储符合条件的案件号
    private String mStartDate = "";
    private String mEndDate = "";
    private TransferData mTransferData = new TransferData();

    public LPJKXServiceTaskadd()
    {
    }

    public void run(VData cInputData)
    {
        System.out.println("＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  LPJKXServiceTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
        System.out.println("");
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
        	return;
        }
        if (!getInitData())
        {
            System.out.println("...........LPJKXServiceTask没有查询到符合条件的案件号............");
            return;
        }
        LPJKXTwoServiceImp tLPJKXServiceImp = new LPJKXTwoServiceImp();
        for (int i = 1; i <= mSSRS.MaxRow; i++)
        {
            if (mSSRS.GetText(i, 1) == null || mSSRS.GetText(i, 1).equals(""))
            {
                continue;
            }
            System.out.println("开始发送：案件号" + mSSRS.GetText(i, 1));
            VData tVData = new VData();
            TransferData mTransferData = new TransferData();
            mTransferData.setNameAndValue("DataInfoKey", mSSRS.GetText(i, 1));
            tVData.add(mTransferData);
            if (!tLPJKXServiceImp.callJKXService(tVData, null))
                System.out.println("案件号" + mSSRS.GetText(i, 1) + "发送失败");
            else
                System.out.println("案件号" + mSSRS.GetText(i, 1) + "发送成功");
        }
        //        if(!prepareOutputData()){
        //        	System.out.println("牙科激活卡上传数据准备日志信息2时失败");
        //        	return ;
        //        }
        //        PubSubmit tPubSubmit = new PubSubmit();
        //        if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN"))
        //        {
        //        	System.out.println("牙科激活卡上传数据提交日志信息时失败");
        //        	return ;
        //        }
    }

    /**
     * 取得传入的数据
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
    	
        mTransferData = (TransferData) pmInputData.getObjectByObjectName(
                "TransferData", 0);
      
        this.mStartDate = (String) mTransferData
                          .getValueByName("tStartDate");
        this.mEndDate = (String) mTransferData.getValueByName("tEndDate");

        return true;
    }

    private boolean getInitData()
    {
    	//#3309北京健康险平台数据报送优化 start
    	String strSQL1 = "select code from ldcode where codetype='bjjkduty' and othersign='LP'";
    	String strSQL2 = "select code from ldcode where codetype='bjjkrisk' and othersign='LP'";
    	String strQuery1 = LLCaseCommon.codeQuery(strSQL1);
    	String strQuery2 = LLCaseCommon.codeQuery(strSQL2);
    	//#3309北京健康险平台数据报送优化 end
    	
        ExeSQL tExeSQL = new ExeSQL();
        String tSQL = ""
        	+ " select distinct a.otherno  "
        	+ " from ljagetclaim a,lmriskapp b,llclaimdetail c  "
        	+ " where a.riskcode=b.riskcode and a.otherno=c.caseno  "
        	+ " and a.riskcode=c.riskcode and a.getdutykind=c.getdutykind  "
        	+ " and a.feeoperationtype <> 'FC' "//FC为反冲数据，此提数派出了反冲数据避免干扰
        	+ " and a.OPConfirmDate between '"+mStartDate+"' and '"+mEndDate+"' "//业务确认日期  
        	+ " and a.othernotype='5' "//othernotype = '5'即为理赔数据
//        	+ " and b.risktype1 = '1' "//risktype1 = '1'为医疗保险
        	+ " and a.managecom like '8611%' "//机构为北京 
        	+ " and exists (select 1 from lccont where contno = c.contno and signdate>='2011-1-1' )"      //签单日为2011-1-1以后的，排除了之前的历史数据问题 
        	+ " and c.DutyCode in ("+strQuery1+")"
        	+ " and a.RiskCode in ("+strQuery2+")"
//        	+ " and not exists (select 1 from lmriskapp where riskcode = c.riskcode and RiskProp = 'G')  "//排除了团险险种 
        	+ " and exists (select 1 from llcasecure where caseno=c.caseno and caserelano=c.caserelano)  "//此为账单表，需要赔案有账单 
        	+ " union "
        	+ " select distinct a.otherno  "
        	+ " from ljagetclaim a,lmriskapp b,llclaimdetail c  "
        	+ " where a.riskcode=b.riskcode and a.otherno=c.caseno  "
        	+ " and a.riskcode=c.riskcode and a.getdutykind=c.getdutykind  "
        	+ " and a.feeoperationtype <> 'FC'  "
        	+ " and a.OPConfirmDate between '"+mStartDate+"' and '"+mEndDate+"' "  //
        	+ " and a.othernotype='5' "
//        	+ " and b.risktype1 = '1' "
        	+ " and a.managecom like '8611%'  "
        	+ " and exists (select 1 from lbcont where contno = c.contno and signdate>='2011-1-1' )  "
        	+ " and c.DutyCode in ("+strQuery1+")"
        	+ " and a.RiskCode in ("+strQuery2+")"
//        	+ " and not exists (select 1 from lmriskapp where riskcode = c.riskcode and RiskProp = 'G')  "
        	+ " and exists (select 1 from llcasecure where caseno=c.caseno and caserelano=c.caserelano)  "
        	+ " with ur ";
        mSSRS = tExeSQL.execSQL(tSQL);
        if (mSSRS.MaxRow <= 0)
        {
            //    		System.out.println("...........LPJKXServiceTask没有查询到符合条件的案件号............");
            return false;
        }
        return true;
    }

    //    private boolean prepareOutputData()
    //    {
    //        try
    //        {
    //            this.mInputData = new VData();
    //            this.mMap.put(this.mLICardUploadLogSet,"INSERT");
    //            mInputData.add(mMap);
    //        }
    //        catch (Exception ex)
    //        {
    //            return false;
    //        }
    //        return true;
    //    }
    /**
     * 测试主程序
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        LPJKXServiceTask tLPJKXServiceTask = new LPJKXServiceTask();
        tLPJKXServiceTask.run();
    }
}
