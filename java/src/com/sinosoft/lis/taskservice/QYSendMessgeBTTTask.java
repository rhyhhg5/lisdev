package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.tb.QySendMessgeBTTBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QYSendMessgeBTTTask extends TaskThread {
	/**
	 * <p>Title: </p>
	 *
	 * <p>Description:
	 * 签单发送短信
	 * </p>
	 *
	 * <p>Copyright: Copyright (c) 2005</p>
	 *
	 * <p>Company: </p>
	 *
	 * @author not attributable
	 * @version 1.0
	 */
	 public CErrors mErrors = new CErrors();

	    public QYSendMessgeBTTTask()
	    {
	    }

	    public void run()
	    {
	        GlobalInput mG = new GlobalInput();
	        mG.Operator = "001";
	        mG.ManageCom = "86";

	        TransferData tf = null;
	        VData mVData = new VData();
	        
	        for(int i = 1; i <= 2; i++)
	        {
	        	tf = new TransferData();
	        	tf.setNameAndValue("ContTypeFlag", i);
	        	
	 	        mVData.add(mG);
	 	        mVData.add(tf);
	        	QySendMessgeBTTBL mQySendMessgeBL = new QySendMessgeBTTBL();
	        	try
		        {
		            if(!mQySendMessgeBL.submitData(mVData, "QYMSG"))
		            {
		                mErrors.addOneError("短信发送失败:");
		                mErrors.copyAllErrors(mQySendMessgeBL.mErrors);
		                System.out.println(mQySendMessgeBL.mErrors.getErrContent());
		            }
		        }
		        catch(Exception ex)
		        {
		            System.out.println("发送失败！");
		            ex.printStackTrace();
		        }
		        mVData.clear();
	        }
	        System.out.println("发送成功！");
	    }

	    public static void main(String[] args)
	    {
	    	QYSendMessgeTask mBPOQYSendMessgeTask = new QYSendMessgeTask();
	    	mBPOQYSendMessgeTask.run();
	    }

}
