package com.sinosoft.lis.taskservice;

import java.io.File;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.Readhtml;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;

public class PremReceivableRptD extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = new GlobalInput();

	/** 提数日期 */
	private String[] date;

	/** 文件路径 */
	private String tOutXmlPath = "";

	/** 应用路径 */
	private String mURL = "";

	/** 对提数结果压缩成功标志：1-成功;0-失败;-1-失败终止 */
	private int mFlag;

	/** 压缩文件的路径 */
	private String mZipFilePath;

	private String StartDate = "";

	private String EndDate = "";

	private String type = "";

	private String fileName = "";

	String[][] mToExcel = null;

	String[][] mToExcel2 = null;

	SSRS tSSRS = new SSRS();

	private String mSql = null;
	private SSRS ttSSRS;

	private String mCurDate = PubFun.getCurrentDate();

	private VData cInputData = new VData();

	// 执行任务
	public void run() {
		dealData();
	}

	public boolean dealData() {
		System.out.println("开始执行批处理:" + PubFun.getCurrentTime());
		// 先准备数据
		getInputData();

		// 打印报表
		getReprot();

		// 报表文件压缩并发送邮件
		getReportZipFile();

		System.out.println("批处理执行完毕:" + PubFun.getCurrentTime());

		return true;
	}

	/**
	 * ] 判断文件是否存在
	 * 
	 * @param filePath
	 * @return
	 */
	private boolean FileExists(String filePath) {
		File reportFile = new File(filePath);
		if (!reportFile.exists()) {
			return false;
		}
		return true;
	}

	/**
	 * 将给定目录下的文件打包为压缩包
	 * 
	 */
	private void getZipFile() {

		String tDocFileName = mURL + fileName; // 文件绝对路径 + 文件名
		mZipFilePath = mURL + fileName.replace("xls", "zip");
		System.out.println("压缩路径名：" + mZipFilePath);
		mFlag = compressIntoZip(mZipFilePath, tDocFileName);
	}

	/**
	 * 添加到压缩文件
	 * 
	 * @param zipfilepath
	 *            String
	 * @param out
	 *            ZipOutputStream
	 * @param tZipDocFileName
	 *            String：zip文件名
	 * @param tDocFileName
	 *            String：待压缩的文件路径名
	 * @return int：1：成功、0：失败，但不中断整个程序、-1：失败，中断程序
	 */
	private int compressIntoZip(String zipfilepath, String tDocFileName) {
		System.out.println("zipfilepath:" + zipfilepath);
		System.out.println("tDocFileName待压缩文件压缩路径:" + tDocFileName);
		Readhtml rh = new Readhtml();
		String[] tInputEntry = new String[1];
		tInputEntry[0] = tDocFileName;
		rh.CreateZipFile(tInputEntry, zipfilepath);
		return 1;
	}

	/**
	 * 将报表查询文件压缩包发送邮件
	 * 
	 */
	private boolean sendSagentMail() {
		// 压缩文件生成成功，开始发送邮件
		if (mFlag == 1) {
			// 邮箱发送
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS
					.GetText(1, 2), "picchealth");
			tMailSender.setSendInf("社保业务应收保费统计日报报表", "您好：\r\n    保单生效日期在  " + StartDate + " 至 "
					+ EndDate + " 期间内，社保业务应收保费报表见附件，谢谢</b>", mZipFilePath);
			String tSQLRMailSql = "select codename,codealias from ldcode where codetype = 'premreceivablermail' ";

			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1),tSSRSR.GetText(1, 2), null);
		     //	测试使用"lifuxiu@sinosoft.com.cn","zhumanfei@sinosoft.com.cn", 
//			tMailSender.setToAddress("yaojianzhen@sinosoft.com.cn",null,null);
			// 发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}
			 deleteFile(tOutXmlPath);
			 deleteFile(mZipFilePath);
		}
		return true;

	}

	/**
	 * 删除生成的ZIP文件和XML文件
	 * 
	 * @param cFilePath
	 *            String：完整文件名
	 */
	private boolean deleteFile(String cFilePath) {
		File f = new File(cFilePath);
		if (!f.exists()) {
			CError tError = new CError();
			tError.moduleName = "MonthDataCatch";
			tError.functionName = "deleteFile";
			tError.errorMessage = "没有找到需要删除的文件";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		f.delete();
		return true;
	}

	private void sendMessage() {
		// 压缩文件生成成功，开始发送邮件
		if (mFlag != 1) {
			// 邮箱发送
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS
					.GetText(1, 2), "picchealth");
			tMailSender.setSendInf("社保业务应收保费统计日报报表", "您好：\r\n    保单生效日期在  " + StartDate + " 至 "
					+ EndDate + " 期间内，没有符合条件的数据,谢谢</b>", null);
			String tSQLRMailSql = "select codename,codealias from ldcode where codetype = 'premreceivablermail' ";

			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1),tSSRSR.GetText(1, 2), null);
			//测试使用"lifuxiu@sinosoft.com.cn","zhumanfei@sinosoft.com.cn",
//			tMailSender.setToAddress("yaojianzhen@sinosoft.com.cn",null,null);
			
			// 发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}

		}

	}

	private boolean getReportZipFile() {
		String name = tOutXmlPath;
		if (FileExists(name)) {
			System.out.println("打印文件存在");
			getZipFile();
			sendSagentMail();
		} else {
			sendMessage();
			System.out.println("没有符合打印条件的数据");
		}

		return true;
	}

	private boolean getReprot() {

		fileName = "PremReceivableDayReport.xls";
		tOutXmlPath = mURL + fileName;
		System.out.println("文件名称====：" + tOutXmlPath);
		WriteToExcel t = new WriteToExcel(fileName);
		t.createExcelFile();
		t.setAlignment("CENTER");
		t.setFontName("宋体");
		String[] sheetName = { new String("约定缴费报表"), new String("期缴保费报表") };
		t.addSheetGBK(sheetName);
		if (!getData1()) {
			mToExcel = getDefalutData();
		}
		t.setData(0, mToExcel);
		if (!getData2()) {
			mToExcel2 = getDefalutData();
		}
		t.setData(1, mToExcel2);

		try {
			t.write(mURL);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("生成文件完成");

		return true;

	}

	private void getInputData() {
		// 设置管理机构
		mGI.ManageCom = "86";
		mGI.Operator = "001";
		mGI.ComCode = "86";
		StartDate = "2013-01-01";
		//测试使用
//		StartDate = "2016-01-01";

		// 设置提数日期
		String predate = new ExeSQL()
				.getOneValue("select current date -1 day from dual");
		System.out.println("提数机构为：" + mGI.ManageCom + ",提数日期为：" + StartDate
				+ " 至" + predate);
		// 提取管理机构
		 String mmsql="select comcode,name from ldcom where length(trim(comcode)) =4 and name like '%分公司' order by comcode with ur";
		 ttSSRS = new ExeSQL().execSQL(mmsql);

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);
		mURL+="vtsfile/";
		//测试使用
//		mURL = "F:/";


		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}

		type = "";
		EndDate = predate;
		
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("OutXmlPath", tOutXmlPath);
		tTransferData.setNameAndValue("StartDate", StartDate);
		tTransferData.setNameAndValue("EndDate", EndDate);
		tTransferData.setNameAndValue("type", type);
		cInputData.add(mGI);
		cInputData.add(tTransferData);

	}

	private String[][] getDefalutData() {
		String[][] tToExcel = new String[10000][30];
		tToExcel[0][0] = "中国人民健康保险股份有限公司";
		tToExcel[1][0] = EndDate.substring(0, EndDate.indexOf('-'))
				+ "年"
				+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate
						.lastIndexOf('-')) + "月应收保费统计表";
		tToExcel[3][0] = "统计期内无数据";

		return tToExcel;
	}

	public boolean getData1() {
		System.out.println("BL->dealDate()");

		int printNum = 3;
		double fShouMoney = 0;
		double fMoney = 0;

		ExeSQL tExeSQL = new ExeSQL();
		mSql = " select mng,mngcom,nvl(sum(Totalshouldpay*gbbl),0),nvl(sum(shouldpay*gbbl),0),nvl(sum(Totalshouldpay*gbbl),0)-nvl(sum(shouldpay*gbbl),0) "
				+ " from( "
				+ " Select "
				+ "        Substr(Gc.Managecom, 1, 4) mng, "
				+ "        (Select Name "
				+ "         From Ldcom "
				+ "         Where Comcode = Substr(Gc.Managecom, 1, 4)) mngcom, "
							//共保比例
				+ "		    (select 1-nvl(sum(rate),0) From lccoinsuranceparam cp  where cp.Grpcontno=Gc.Grpcontno) as gbbl, "
							//应收	
				+ "         (case when (select count(1) from lcgrppayplandetail where prtno = gc.prtno  and state='2')=0 then 0 else ( (select sum(prem) from db2inst1.LcGrpPayDue p where p.prtno = gc.prtno and confstate = '02'))end ) as totalShouldPay,"
							//未逾期
				+ "       (case when (select count(1) from lcgrppayplandetail where prtno = gc.prtno  and state='2')=0 then 0 else ( (case when current date-(select min(paytodate) from db2inst1.LcGrpPayDue p where p.prtno = gc.prtno and confstate = '02') > 0 "
				+ "			then (select sum(prem) from db2inst1.LcGrpPayDue p where p.prtno = gc.prtno and confstate = '02' and  paytodate >=current date ) "
				+ "			else (select sum(prem) from db2inst1.LcGrpPayDue p where p.prtno = gc.prtno and confstate = '02')  end))end ) as shouldpay "
				+ " From Lcgrpcont Gc "
				+ " Where Gc.Payintv = -1 "
				+ " And Gc.Signdate Is Not Null "
				+ " And Exists (Select 1 "
				+ " From Lcgrppayplan Gpp "
				+ " Where Gpp.Prtno = Gc.Prtno) "
				+ " And Gc.Managecom Like '86%'  "
				+ " and Gc.Markettype not in ('1','9') "
				+ " and Gc.cvalidate <='"
				+ EndDate
				+ "'  "
				+ " and Gc.cvalidate >='"
				+ StartDate
				+ "'  )tmep "
				+ " group by mng,mngcom  "
				+ " having nvl(sum(Totalshouldpay),0)>0  with ur";
		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);	   
		if (tSSRS.getMaxRow() < 1) {
			return false;
		} else {
			int count = ttSSRS.getMaxRow();
			mToExcel = new String[count+printNum+4][30];
			mToExcel[0][0] = "中国人民健康保险股份有限公司";
			mToExcel[1][0] = EndDate.substring(0, EndDate.indexOf('-'))
					+ "年"
					+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate
							.lastIndexOf('-')) + "月应收保费统计表";
			mToExcel[2][0] = "编制单位：" + getName();
			mToExcel[2][4] = "金额单位：元";

			mToExcel[3][0] = "序号";
			mToExcel[3][1] = "机构代码";
			mToExcel[3][2] = "机构名称";
			mToExcel[3][3] = "应收保费金额";
			mToExcel[3][4] = "逾期应收保费余额";
			for(int j = 1; j <= count; j++){
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {					
						if(ttSSRS.GetText(j, 1).equals(tSSRS.GetText(i, 1))){
							String tempfMoney=PubFun.format(Double.parseDouble(tSSRS.GetText(i, 3)));
							String tempMoney=PubFun.format(Double.parseDouble(tSSRS.GetText(i, 5)));
							mToExcel[printNum + j][0] = "" + j;
							mToExcel[printNum + j][1] = tSSRS.GetText(i, 1);
							mToExcel[printNum + j][2] = tSSRS.GetText(i, 2);
							mToExcel[printNum + j][3] = tempfMoney;
							mToExcel[printNum + j][4] = tempMoney;
							fShouMoney = fShouMoney+ Double.parseDouble(tempfMoney);
							fMoney = fMoney + Double.parseDouble(tempMoney);	
							break;
						}else{
							mToExcel[printNum + j][0] = "" + j;
							mToExcel[printNum + j][1] = ttSSRS.GetText(j, 1);
							mToExcel[printNum + j][2] = ttSSRS.GetText(j, 2);
							mToExcel[printNum + j][3] = "0.00";
							mToExcel[printNum + j][4] = "0.00";
						}
					}
				}
				mToExcel[printNum + count + 1][1] = "合计";
				mToExcel[printNum + count + 1][3] = PubFun.format(fShouMoney);
				mToExcel[printNum + count + 1][4] = PubFun.format(fMoney);
	
				mToExcel[printNum + count + 3][0] = "制表";
				mToExcel[printNum + count + 3][4] = "日期：" + mCurDate;			
		}

		return true;

	}

	public boolean getData2() {
		System.out.println("BL->dealDate()");

		int printNum = 3;
		double fShouMoney = 0;
		double fMoney = 0;

		ExeSQL tExeSQL = new ExeSQL();
		mSql = " select DepCode,(select name from ldcom where comcode=DepCode),sum(YS*gbbl),sum(YQYS*gbbl) from "
				+ " ( "
				+ " select "
				+ " substr(Gc.managecom,1,4) DepCode, "
				+ " DB2INST1.LF_PAYMONEY(Gp.grppolno,Gp.paytodate,Gp.payenddate,Gp.payintv) YS, "
				+ " DB2INST1.LF_PAYMONEY(Gp.grppolno,Gp.paytodate,Gp.payenddate,Gp.payintv)-DB2INST1.LF_GETSHOUTMONEY(Gp.grppolno,Gp.payenddate,Gp.paytodate,Gp.payintv,'0',Current Date) YQYS, "
				+ " nvl((select 1-sum(rate) From lccoinsuranceparam cp  where cp.Grpcontno=Gc.Grpcontno),1) as gbbl "
				+ " From Lcgrpcont Gc Inner Join Lcgrppol Gp On Gc.Grpcontno = Gp.Grpcontno "
				+ " Where Gc.Payintv not in(0,12,-1) "
				+ " And Gc.Managecom Like '86%'  "
				+ " And Gc.Signdate Is Not Null "
				+ " and Gc.Markettype not in ('1','9') "
				+ " and Gc.cvalidate <='"
				+ EndDate
				+ "'  "
				+ " and Gc.cvalidate >='"
				+ StartDate
				+ "' "
				+ " And exists (select 1 from lmriskapp where riskcode = Gp.riskcode And riskperiod in ('S', 'M')) "
				+ " And Gc.Signdate Is Not Null "
				+ " And Exists (Select 1 From lidatatransresult Gpp Where contno =Gc.grpcontno and classid in('O-NC-000003','00000116','00000125') and riskcode=Gp.riskcode) "
				+ " And DB2INST1.LF_PAYMONEY(Gp.grppolno,Gp.paytodate,Gp.payenddate,Gp.payintv)<>0 "
				+ " and not exists(select 1 from lidatatransresult a where a.contno=Gc.grpcontno and classid in('Y-BQ-FC-000003','BDZF0001','00000143')) "
				+ " ) as cc " + " group by depcode " + " with ur ";

		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);

		if (tSSRS.getMaxRow() < 1) {
			return false;
		} else {
			int count = ttSSRS.getMaxRow();
			mToExcel2 = new String[count+printNum+4][30];
			mToExcel2[0][0] = "中国人民健康保险股份有限公司";
			mToExcel2[1][0] = EndDate.substring(0, EndDate.indexOf('-'))
					+ "年"
					+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate
							.lastIndexOf('-')) + "月应收保费统计表";
			mToExcel2[2][0] = "编制单位：" + getName();
			mToExcel2[2][4] = "金额单位：元";

			mToExcel2[3][0] = "序号";
			mToExcel2[3][1] = "机构代码";
			mToExcel2[3][2] = "机构名称";
			mToExcel2[3][3] = "应收保费金额";
			mToExcel2[3][4] = "逾期应收保费余额";
			for(int j = 1; j <= count; j++){
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {	
						if(ttSSRS.GetText(j, 1).equals(tSSRS.GetText(i, 1))){
							String tempfMoney=PubFun.format(Double.parseDouble(tSSRS.GetText(i, 3)));
							String tempMoney=PubFun.format(Double.parseDouble(tSSRS.GetText(i, 4)));
							mToExcel2[printNum + j][0] = "" + j;
							mToExcel2[printNum + j][1] = tSSRS.GetText(i, 1);
							mToExcel2[printNum + j][2] = tSSRS.GetText(i, 2);
							mToExcel2[printNum + j][3] = tempfMoney;
							mToExcel2[printNum + j][4] = tempMoney;
							fShouMoney = fShouMoney+ Double.parseDouble(tempfMoney);
							fMoney = fMoney + Double.parseDouble(tempMoney);
							break;
						}else{	
							mToExcel2[printNum + j][0] = "" + j;
							mToExcel2[printNum + j][1] = ttSSRS.GetText(j, 1);
							mToExcel2[printNum + j][2] = ttSSRS.GetText(j, 2);
							mToExcel2[printNum + j][3] = "0.00";
							mToExcel2[printNum + j][4] = "0.00";
						}
				}
			}
			mToExcel2[printNum + count + 1][1] = "合计";
			mToExcel2[printNum + count + 1][3] = PubFun.format(fShouMoney);
			mToExcel2[printNum + count + 1][4] = PubFun.format(fMoney);
			mToExcel2[printNum + count + 3][0] = "制表";
			mToExcel2[printNum + count + 3][4] = "日期：" + mCurDate;

		}
		return true;
	}

	private String getName() {
		String tSQL = "";
		String tRtValue = "";
		tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSQL);
		if (tSSRS.getMaxRow() == 0) {
			tRtValue = "";
		} else
			tRtValue = tSSRS.GetText(1, 1);
		return tRtValue;
	}

	// 主方法 测试用
	public static void main(String[] args) {
		new PremReceivableRptD().run();

	}
}
