package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclient.inf.UploadClaim;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *    银行卡缴费信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2016</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @version 1.1
 */
public class nLLUploadBfTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    /**携带数据的类*/
    private TransferData tempTransferData = new TransferData();
    /** 往后面传输数据的容器 */
	private VData mInputData = new VData();
    String opr = "";
    
    public nLLUploadBfTask()
    {}

    
    public void run()
    {
    	UploadClaim  tUploadClaim = new UploadClaim();
    	String SQL = "select prtno from lcpol where riskcode in(select code from ldcode where codetype='riskcode') and appflag='1' and "+
                     "exists(select 1 from  YBKErrorList where businessno=lcpol.prtno and resultstatus='1' and transtype='N01' )"+
                     "and  not exists(select 1 from  YBKErrorList where businessno=lcpol.contno and resultstatus='1' and transtype='T01' )";
    	
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String cardNumber = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("cardNumber", cardNumber);
    			tVData.add(tTransferData);
    	    	if(!tUploadClaim.submitData(tVData, "ZBXPT"))
    	         {
    	    		 System.out.println("保费信息上传出问题了");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             continue;
    	         }else{
    	        	 System.out.println("保费信息上传成功了");
    	        	 opr ="true";
    	         }
    		}
    	}else{
    		 System.out.println("保费信息查询异常");
             System.out.println(mErrors.getErrContent());
             opr ="false";
    	}
    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
	
	
    public static void main(String[] args)
    {
    	nLLUploadBfTask myTest = new nLLUploadBfTask();
    	myTest.run();
    }
}




