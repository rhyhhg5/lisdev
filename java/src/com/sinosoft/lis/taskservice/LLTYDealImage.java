package com.sinosoft.lis.taskservice;

import org.apache.log4j.Logger;

import com.sinosoft.lis.yibaotong.LLTYGetScanImageBL;
import com.sinosoft.utility.CErrors;

public class LLTYDealImage extends TaskThread{

	/**错误的容器*/
    public CErrors mErrors = new CErrors();
    private Logger cLogger = Logger.getLogger(getClass());

    public String opr = "";
    public String consume = "";
    public LLTYDealImage()
    {}
    
    public void run() {
    	long mOldTimeMillis = System.currentTimeMillis();
    	LLTYGetScanImageBL tLLTYGetScanImageBL = new LLTYGetScanImageBL();
    	if(!tLLTYGetScanImageBL.submitData()) {
    		System.out.println("影像件处理有问题");
            System.out.println(mErrors.getErrContent());
            opr ="false";
            return;
    	}else {
    		System.out.println("影像件处理成功");
       	 	opr ="true";
    	}
    	long mCurTimeMillis = System.currentTimeMillis();
    	consume = (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s";
    	cLogger.info("影像件处理完毕！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");
    }
    
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
	public String getConsume() {
		return consume;
	}

	public void setConsume(String consume) {
		this.consume = consume;
	}
}
