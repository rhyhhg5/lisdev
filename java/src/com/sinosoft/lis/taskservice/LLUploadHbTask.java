package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclient.inf.UploadClaim;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *    核保信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2016</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @version 1.1
 */
public class LLUploadHbTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    /**携带数据的类*/
    private TransferData tempTransferData = new TransferData();
    /** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/**存储接收的印刷号  */
    private String pNumber;
    String opr = "";
    
    public LLUploadHbTask()
    {}

    
    public boolean run(VData cInputData,String cOperate)
    {
    	//将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    if(!getInputData()){
    		return false;
    	}
   		if(!dealData()){
   			return false;
   		}
    	return true;
    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
	
	/**
	 * 获取传入的方法
	 */
	public boolean getInputData(){
		
		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
	    pNumber = (String) tempTransferData.getValueByName("pNumber");
   		System.out.println("接收的pNumber:"+pNumber);
   		if(pNumber==null&&pNumber.equals("")){
   			CError tCError = new CError();
   			tCError.moduleName = "LLUploadClaimTask";
   			tCError.functionName = "getMessage";
   			tCError.errorMessage = "传入的印刷号为空！";
   			this.mErrors.addOneError(tCError);
		}
   		return true;
	}
	
	public boolean dealData(){
		UploadClaim  tUploadClaim = new UploadClaim();
    	String SQL = " SELECT distinct otherno " +
    			" from ljagetclaim a" +
    			" where  not exists(select 1 from  lserrorlist where a.otherno=businessno and transtype='CLM001' and resultstatus='00')  " +
    			" and a.riskcode in(select riskcode from lmriskapp where taxoptimal='Y') and a.othernotype='5' " +
    			" and a.getdutykind in('100','200') and otherno='"+ pNumber +"' ";
    	
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String pNumber = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("pNumber", pNumber);
    			tVData.add(tTransferData);
    	    	if(!tUploadClaim.submitData(tVData, "ZBXPT"))
    	         {
    	    		 System.out.println("核保信息上传出问题了");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             return false;
    	         }else{
    	        	 System.out.println("核保信息上传成功了");
    	        	 opr ="true";
    	         }
    		}
    	}else{
    		 System.out.println("核保信息查询异常");
             System.out.println(mErrors.getErrContent());
             opr ="false";
             return false;
    	}
    	return true;
	}
	
    public static void main(String[] args)
    {
    	LLUploadHbTask myTest = new LLUploadHbTask();
    	myTest.run();
    }
}




