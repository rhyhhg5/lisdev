package com.sinosoft.lis.taskservice;

import java.io.File;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.GetFeePrintBL;
import com.sinosoft.lis.f1print.NewPreRecPrintBL;
import com.sinosoft.lis.f1print.Readhtml;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;

public class LLCaseContRptHN extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = new GlobalInput();

	/** 提数日期 */
	private String[] date;

	/** 文件路径 */
	private String tOutXmlPath = "";

	/** 应用路径 */
	private String mURL = "";

	/** 对提数结果压缩成功标志：1-成功;0-失败;-1-失败终止 */
	private int mFlag;

	/** 压缩文件的路径 */
	private String mZipFilePath;

	private String StartDate = "";

	private String EndDate = "";

	private String type = "";

	private String fileName = "";

	String[][] mToExcel = null;

	String[][] mToExcel2 = null;
	
	String[][] mToExcel3 = null;

	SSRS tSSRS = new SSRS();

	private String mSql = null;

	private String mCurDate = PubFun.getCurrentDate();

	private VData cInputData = new VData();

	// 执行任务
	public void run() {
		dealData();
	}

	public boolean dealData() {
		System.out.println("开始执行批处理:" + PubFun.getCurrentTime());
		// 先准备数据
		getInputData();

		// 打印报表
		getReprot();

		// 报表文件压缩并发送邮件
		getReportZipFile();

		System.out.println("批处理执行完毕:" + PubFun.getCurrentTime());

		return true;
	}

	/**
	 * ] 判断文件是否存在
	 * 
	 * @param filePath
	 * @return
	 */
	private boolean FileExists(String filePath) {
		File reportFile = new File(filePath);
		if (!reportFile.exists()) {
			return false;
		}
		return true;
	}

	/**
	 * 将给定目录下的文件打包为压缩包
	 * 
	 */
	private void getZipFile() {

		String tDocFileName = mURL + fileName; // 文件绝对路径 + 文件名
		mZipFilePath = mURL + fileName.replace("xls", "zip");
		System.out.println("压缩路径名：" + mZipFilePath);
		mFlag = compressIntoZip(mZipFilePath, tDocFileName);
	}

	/**
	 * 添加到压缩文件
	 * 
	 * @param zipfilepath
	 *            String
	 * @param out
	 *            ZipOutputStream
	 * @param tZipDocFileName
	 *            String：zip文件名
	 * @param tDocFileName
	 *            String：待压缩的文件路径名
	 * @return int：1：成功、0：失败，但不中断整个程序、-1：失败，中断程序
	 */
	private int compressIntoZip(String zipfilepath, String tDocFileName) {
		System.out.println("zipfilepath:" + zipfilepath);
		System.out.println("tDocFileName待压缩文件压缩路径:" + tDocFileName);
		Readhtml rh = new Readhtml();
		String[] tInputEntry = new String[1];
		tInputEntry[0] = tDocFileName;
		rh.CreateZipFile(tInputEntry, zipfilepath);
		return 1;
	}

	/**
	 * 将报表查询文件压缩包发送邮件
	 * 
	 */
	private boolean sendSagentMail() {
		// 压缩文件生成成功，开始发送邮件
		if (mFlag == 1) {
			// 邮箱发送
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender("itoperation","11111111", "picchealth");
			tMailSender.setSendInf("关于报送河南省保险消费者权益保护中心服务网络系统基础信息数据", "胡涌哲,您好：\r\n   相关提数详见附件 </b>", mZipFilePath);
			String tSQLRMailSql = "select codename,codealias from ldcode where codetype = 'LLCaseContRptHNmail' ";
			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress("liuyachao14270@sinosoft.com.cn",
					null, null);
			// 发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}
//			 deleteFile(tOutXmlPath);
//			 deleteFile(mZipFilePath);
		}
		return true;

	}

	/**
	 * 删除生成的ZIP文件和XML文件
	 * 
	 * @param cFilePath
	 *            String：完整文件名
	 */
	private boolean deleteFile(String cFilePath) {
		File f = new File(cFilePath);
		if (!f.exists()) {
			CError tError = new CError();
			tError.moduleName = "MonthDataCatch";
			tError.functionName = "deleteFile";
			tError.errorMessage = "没有找到需要删除的文件";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		f.delete();
		return true;
	}

	private void sendMessage() {
		// 压缩文件生成成功，开始发送邮件
		if (mFlag != 1) {
			// 邮箱发送
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender("itoperation","11111111", "picchealth");
			tMailSender.setSendInf("社保业务应收保费统计日报报表", "您好：\r\n    保单生效日期在  " + StartDate + " 至 "
					+ EndDate + " 期间内，没有符合条件的数据,谢谢</b>", null);
			String tSQLRMailSql = "select codename,codealias from ldcode where codetype = 'LLCaseContRptHNmail'  ";
			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1),
					tSSRSR.GetText(1, 2), null);
			// 发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}

		}

	}

	private boolean getReportZipFile() {
		String name = tOutXmlPath;
		if (FileExists(name)) {
			System.out.println("打印文件存在");
			getZipFile();
			sendSagentMail();
		} else {
			sendMessage();
			System.out.println("没有符合打印条件的数据");
		}

		return true;
	}

	private boolean getReprot() {

		fileName = "LLCaseContDayReport.xls";
		tOutXmlPath = mURL + fileName;
		System.out.println("文件名称====：" + tOutXmlPath);
		WriteToExcel t = new WriteToExcel(fileName);
		t.createExcelFile();
		t.setAlignment("CENTER");
		t.setFontName("宋体");
		String[] sheetName = { new String("承保"), new String("立案"), new String("结案") };
		t.addSheetGBK(sheetName);
		if (!getData1()) {
			mToExcel = getDefalutData();
		}
		t.setData(0, mToExcel);
		if (!getData2()) {
			mToExcel2 = getDefalutData();
		}
		t.setData(1, mToExcel2);
		if (!getData3()) {
			mToExcel3 = getDefalutData();
		}
		t.setData(2, mToExcel3);

		try {
			t.write(mURL);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("生成文件完成");

		return true;

	}

	private void getInputData() {
		// 设置管理机构
		mGI.ManageCom = "86";
		mGI.Operator = "001";
		mGI.ComCode = "86";
		StartDate = "2015-04-12";

		// 设置提数日期
		String predate = new ExeSQL()
				.getOneValue("select current date -1 day from dual");
		System.out.println("提数机构为：" + mGI.ManageCom + ",提数日期为：" + StartDate
				+ " 至" + predate);

		// 设置文件路径
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		mURL = new ExeSQL().getOneValue(tSQL);
		mURL = "E:/";
		mURL+="vtsfile/";

		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}

		type = "";
		EndDate = predate;

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("OutXmlPath", tOutXmlPath);
		tTransferData.setNameAndValue("StartDate", StartDate);
		tTransferData.setNameAndValue("EndDate", EndDate);
		tTransferData.setNameAndValue("type", type);
		cInputData.add(mGI);
		cInputData.add(tTransferData);

	}

	private String[][] getDefalutData() {
		String[][] tToExcel = new String[10000][30];
		tToExcel[0][0] = "中国人民健康保险股份有限公司";
		tToExcel[1][0] = EndDate.substring(0, EndDate.indexOf('-'))
				+ "年"
				+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate
						.lastIndexOf('-')) + "月应收保费统计表";
		tToExcel[3][0] = "统计期内无数据";

		return tToExcel;
	}

	public boolean getData1() {
		System.out.println("BL->dealDate()");

		int printNum = 0;

		ExeSQL tExeSQL = new ExeSQL();
		mSql = " select contno 保单号, "
			+ "       trim(risktype2) 险种, "
			+ "       appntname 投保人姓名, "
			+ "       idno 投保人证件号码,  "
			+ "       case                  "
			+ "           when appmobile is not null then "
			+ "            appmobile        "
			+ "           else              "
			+ "            appphone         "
			+ "       end 投保人移动电话,   "
			+ "       insuname 被保人姓名,  "
			+ "       insuidno 被保人证件号码,            "
			+ "       case                  "
			+ "           when insumobile is not null then  "
			+ "            insumobile       "
			+ "           else              "
			+ "            insuphone        "
			+ "       end 被保人移动电话,   "
			+ "       sum(amnt) 保额,       "
			+ "       cvalidate 生效日期,   "
			+ "       salechnl 销售渠道     "
			+ "  from (select lcc.contno contno,          "
			+ "               (select case  "
			+ "                           when riskcode in  "
			+ "                                ('730101', '332401', '730201', '332501') then      "
			+ "                            '分红'         "
			+ "                           else            "
			+ "                            case           "
			+ "                           when risktype = 'A' then    "
			+ "                            '意外'         "
			+ "                           when risktype = 'H' then "
			+ "                            '健康'         "
			+ "                           else            "
			+ "                            '其他'         "
			+ "                       end end             "
			+ "                  from lmriskapp           "
			+ "                 where riskcode = lcp.riskcode) risktype2,  "
			+ "               lcc.appntname appntname,    "
			+ "               lcc.appntidno idno,         "
			+ "               (select mobile              "
			+ "                  from lcaddress           "
			+ "                 where customerno = lca.appntno     "
			+ "                   and addressno = lca.addressno) appmobile,   "
			+ "               (select phone               "
			+ "                  from lcaddress           "
			+ "                 where customerno = lca.appntno      "
			+ "                   and addressno = lca.addressno) appphone,  "
			+ "               lci.name insuname,          "
			+ "               lci.idno insuidno,          "
			+ "               (select mobile              "
			+ "                  from lcaddress           "
			+ "                 where customerno = lci.insuredno   "
			+ "                   and addressno = lci.addressno) insumobile,    "
			+ "               (select phone               "
			+ "                  from lcaddress           "
			+ "                 where customerno = lci.insuredno   "
			+ "                   and addressno = lci.addressno) insuphone,    "
			+ "               lcp.amnt amnt,              "
			+ "               lcp.cvalidate cvalidate,    "
			+ "               case          "
			+ "                   when lcc.salechnl in ('01', '10') then "
			+ "                    '个险'   "
			+ "                   else      "
			+ "                    '银保'   "
			+ "               end salechnl  "
			+ "          from lccont lcc, lcpol lcp, lcappnt lca, lcinsured lci    "
			+ "         where 1=1 and lcc.conttype = '1'  "
			+ "           and lcc.managecom like '8641%'  "
			+ "           and lcc.appflag = '1'           "
			+ "           and lcc.cvalidate >= current date - 1   "
			+ "         DAYS                "
			+ "           and lcc.cvalidate < current     "
			+ "         date                "
			+ "           and lcc.contno = lcp.contno     "
			+ "           and lcc.contno = lca.contno     "
			+ "           and lcp.contno = lci.contno     "
			+ "           and lcp.insuredno = lci.insuredno    "
			+ "           and lcc.salechnl in ('01', '10', '04', '13')) temp  "
			+ " group by contno,            "
			+ "          risktype2,         "
			+ "          appntname,         "
			+ "          idno,              "
			+ "          appmobile,         "
			+ "          appphone,          "
			+ "          insuname,          "
			+ "          insuidno,          "
			+ "          insumobile,        "
			+ "          insuphone,         "
			+ "          cvalidate,         "
			+ "          salechnl with ur   ";
		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);

		if (tSSRS.getMaxRow() < 1) {
			return false;
		} else {
			int count = tSSRS.getMaxRow();
			mToExcel = new String[count+printNum+4][30];

			mToExcel[0][0] = "序号";
			mToExcel[0][1] = "机构代码";
			mToExcel[0][2] = "机构名称";
			mToExcel[0][3] = "应收保费金额";
			mToExcel[0][4] = "逾期应收保费余额";
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				mToExcel[printNum + i][0] = "" + i;
				mToExcel[printNum + i][1] = tSSRS.GetText(i, 1);
				mToExcel[printNum + i][2] = tSSRS.GetText(i, 2);
				mToExcel[printNum + i][3] = tSSRS.GetText(i, 3);
				mToExcel[printNum + i][4] = tSSRS.GetText(i, 5);


			}


			mToExcel[printNum + count + 3][0] = "制表";
			mToExcel[printNum + count + 3][4] = "日期：" + mCurDate;
		}

		return true;

	}

	public boolean getData2() {
		System.out.println("BL->dealDate()");

		int printNum = 0;


		ExeSQL tExeSQL = new ExeSQL();
		mSql = " select  a.caseno 案件号,b.RgtantName 申请人姓名,b.idno 申请人身份证号,b.RgtantMobile 申请人手机号,a.rgtdate 立案时间, " +
				" db2inst1.CodeName('llrgtreason',(select ReasonCode from LLAppClaimReason where caseno=a.caseno fetch first row only )) 出险类型, " +
				" a.customername 被保险人姓名,a.mobilephone 被保险人手机号  " +
				" from llcase a,llregister b  " +
				" where a.rgtno=b.rgtno and a.mngcom like '8641%'  " +
				" and a.rgtdate  >= current date - 1 DAYS and a.rgtdate  < current date  with ur";

		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);

		if (tSSRS.getMaxRow() < 1) {
			return false;
		} else {
			int count = tSSRS.getMaxRow();
			mToExcel2 = new String[count+printNum+4][30];

			mToExcel2[0][0] = "序号";
			mToExcel2[0][1] = "机构代码";
			mToExcel2[0][2] = "机构名称";
			mToExcel2[0][3] = "应收保费金额";
			mToExcel2[0][4] = "逾期应收保费余额";
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				mToExcel2[printNum + i][0] = "" + i;
				mToExcel2[printNum + i][1] = tSSRS.GetText(i, 1);
				mToExcel2[printNum + i][2] = tSSRS.GetText(i, 2);
				mToExcel2[printNum + i][3] = tSSRS.GetText(i, 3);
				mToExcel2[printNum + i][4] = tSSRS.GetText(i, 4);

			}

			mToExcel2[printNum + count + 3][0] = "制表";
			mToExcel2[printNum + count + 3][4] = "日期：" + mCurDate;

		}

		return true;
	}
	
	public boolean getData3(){
		System.out.println("BL->dealDate()");

		int printNum = 3;
		double fShouMoney = 0;
		double fMoney = 0;

		ExeSQL tExeSQL = new ExeSQL();
		mSql = "select  a.caseno 案件号,b.RgtantName 申请人姓名,b.RgtantMobile 申请人手机号,a.customername 被保险人姓名, " +
				" b.idno 被保险人身份证号,c.GiveTypeDesc 理赔结果,sum(c.realpay) 赔付金额,a.endcasedate 结案时间, " +
				" (select  HospitalName from LLCaseCure where caseno=a.caseno fetch first rows only) 出险医院 " +
				" from llcase a,llregister b,llclaimpolicy c where a.rgtno=b.rgtno and a.caseno=c.caseno and " +
				" a.rgtstate in('09','11','12') and a.mngcom like '8641%' and a.endcasedate  >= current date - 1 DAYS " +
				" and a.endcasedate  < current date  " +
				" group by a.caseno,b.RgtantName,b.RgtantMobile, a.customername,b.idno,c.GiveTypeDesc,a.endcasedate with ur ";

		System.out.println(mSql);
		tSSRS = tExeSQL.execSQL(mSql);

		if (tSSRS.getMaxRow() < 1) {
			return false;
		} else {
			int count = tSSRS.getMaxRow();
			mToExcel3 = new String[count+printNum+4][30];
			mToExcel3[0][0] = "中国人民健康保险股份有限公司";
			mToExcel3[1][0] = EndDate.substring(0, EndDate.indexOf('-'))
					+ "年"
					+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate
							.lastIndexOf('-')) + "月应收保费统计表";
			mToExcel3[2][0] = "编制单位：" + getName();
			mToExcel3[2][4] = "金额单位：元";

			mToExcel3[3][0] = "序号";
			mToExcel3[3][1] = "机构代码";
			mToExcel3[3][2] = "机构名称";
			mToExcel3[3][3] = "应收保费金额";
			mToExcel3[3][4] = "逾期应收保费余额";
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				mToExcel3[printNum + i][0] = "" + i;
				mToExcel3[printNum + i][1] = tSSRS.GetText(i, 1);
				mToExcel3[printNum + i][2] = tSSRS.GetText(i, 2);
				mToExcel3[printNum + i][3] = tSSRS.GetText(i, 3);
				mToExcel3[printNum + i][4] = tSSRS.GetText(i, 4);
//				fShouMoney = fShouMoney
//						+ Double.parseDouble(tSSRS.GetText(i, 3));
//				fMoney = fMoney + Double.parseDouble(tSSRS.GetText(i, 4));
			}
			mToExcel3[printNum + count + 1][1] = "合计";
			mToExcel3[printNum + count + 1][3] = fShouMoney + "";
			mToExcel3[printNum + count + 1][4] = fMoney + "";
			mToExcel3[printNum + count + 3][0] = "制表";
			mToExcel3[printNum + count + 3][4] = "日期：" + mCurDate;

		}

		return true;
	}

	private String getName() {
		String tSQL = "";
		String tRtValue = "";
		tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSQL);
		if (tSSRS.getMaxRow() == 0) {
			tRtValue = "";
		} else
			tRtValue = tSSRS.GetText(1, 1);
		return tRtValue;
	}

	// 主方法 测试用
	public static void main(String[] args) {
		new LLCaseContRptHN().run();

	}
}
