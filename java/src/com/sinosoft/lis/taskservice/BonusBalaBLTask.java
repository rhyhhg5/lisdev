package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.BonusBalaBL;
import com.sinosoft.lis.bq.BonusGetBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.InsuAccBala;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LOBonusAssignErrLogSchema;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 分红险累积生息批处理 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company: sinosoft</p>
 * @author XP
 * @version 1.0
 * @CreateDate：2012-08-02
 */

public class BonusBalaBLTask extends TaskThread {
	
    public CErrors mErrors = new CErrors();

    private String mContNo = null;
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    private GlobalInput mGI = new GlobalInput();

    public BonusBalaBLTask()
    {
        mGI.Operator = "001";
        mGI.ComCode = "86";
        mGI.ManageCom = mGI.ComCode;
    }

    public BonusBalaBLTask(GlobalInput cGlobalInput)
    {
        mGI = cGlobalInput;
    }

    /**
     * 结算指定保单
     * @param cContNo String
     */
    public void runOneCont(String cContNo)
    {
        mContNo = cContNo;
        run();
    }
    
    public void run()
    {
    	int iSuccCount = 0;
        int iFailCount = 0;
        int iWaitCount = 0;

        //查询出当前账户最后结算日期比已公布利率日期晚的账户，由于理赔或保全等原因，一个账户可能需要结算多次
        String sql = " select distinct a.* from lcinsureacc a, lcpol b "
                     + " where a.polno = b.polno "
                     + " and a.GrpContNo = '00000000000000000000' "
                     + " and b.bonusgetmode= '2' "
                     + " and a.riskcode in (select riskcode from lmriskapp where risktype4='2') " //分红险
                     + " and a.ManageCom like '" + mGI.ManageCom + "%' "
                     + " and b.appflag='1' "
                     + " and (a.baladate + 1 month)<= b.enddate "
                     + " and (a.makedate - (day(a.makedate)-1) day + 1 month)<=current date "
                     + " and exists (select 1 from lcinsureacctrace where polno=a.polno and money>0)"
//                     初始化lcinsureaccbalance调用,同时结算利息
                     + " and not exists (select 1 from lcinsureaccbalance where polno=a.polno) "
                     + (mContNo != null ? (" and a.ContNo = '" + mContNo + "' ") : "")
                     + " union all " 
                     + " select distinct a.* from lcinsureacc a, lcpol b "
                     + " where a.polno = b.polno "
                     + " and a.GrpContNo = '00000000000000000000' "
//                     存在这个表表示baladate准的了
                     + " and exists (select 1 from lcinsureaccbalance where polno=a.polno) "
                     + " and b.bonusgetmode= '2' "
                     + " and a.riskcode in (select riskcode from lmriskapp where risktype4='2') " //分红险
                     + " and a.ManageCom like '" + mGI.ManageCom + "%' "
                     + " and b.appflag='1' "
                     + " and (a.baladate + 1 month)<= b.enddate "
//                     是否已过结算日下个月1号
                     + " and (a.baladate - (day(a.baladate)-1) day + 1 month)<=current date "
                     + (mContNo != null ? (" and a.ContNo = '" + mContNo + "' ") : "")
                     + "  with ur";
        System.out.println(sql);

        RSWrapper rsWrapper = new RSWrapper();
        LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
        if (!rsWrapper.prepareData(tLCInsureAccSet, sql))
        {
            System.out.println("万能险保单结算数据准备失败! ");
            return;
        }

        do
        {
            rsWrapper.getData();
            if (tLCInsureAccSet == null || tLCInsureAccSet.size() < 1)
            {
                break;
            }

            iWaitCount += tLCInsureAccSet.size();

            for (int i = 1; i <= tLCInsureAccSet.size(); i++)
            {
                LCInsureAccSchema tAccSchema = tLCInsureAccSet.get(i);

                VData tVData = new VData();
                tVData.add(mGI);
                tVData.add(tLCInsureAccSet.get(i));

                BonusBalaBL tBonusBalaBL = new BonusBalaBL();

                String errInfo = "保单:" + tAccSchema.getContNo()
                                 + "被保人:" + tAccSchema.getInsuredNo()
                                 + "险种：" + tAccSchema.getRiskCode()
                                 + " 结算失败: ";
                try
                {
                    if(!tBonusBalaBL.submitData(tVData, ""))
                    {
                        iFailCount++;
                        errInfo += tBonusBalaBL.mErrors.getErrContent();
                        mErrors.addOneError(errInfo);
                        System.out.println(errInfo);
                    }
                    else
                    {
                        iSuccCount++;
                    }
                }
                catch(Exception ex)
                {
                    errInfo += "结算出现未知异常";
                    System.out.println(errInfo);
                    ex.printStackTrace();
                    mErrors.addOneError(errInfo);
                    iFailCount++;
                }
            }
        }
        while (tLCInsureAccSet != null && tLCInsureAccSet.size() > 0);
        rsWrapper.close();
        System.out.println("共计" + iSuccCount + "次账户结算成功，"
                           + iFailCount + "个保单结算失败:"
            + mErrors.getErrContent());

        if(iWaitCount == 0)
        {
            mErrors.addOneError("没有需要处理的保单");
        }
    }

    
    public static void main(String[] args) {
    	BonusBalaBLTask tBonusGetBLTask =new  BonusBalaBLTask();
    	tBonusGetBLTask.runOneCont("014079301000001");
	}
    
}
