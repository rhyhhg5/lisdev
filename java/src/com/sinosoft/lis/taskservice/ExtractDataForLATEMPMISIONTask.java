package com.sinosoft.lis.taskservice;

import java.util.Date;

import com.sinosoft.lis.db.SolidificationLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.SolidificationLogSchema;
import com.sinosoft.lis.vschema.LATEMPMISIONSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;

public class ExtractDataForLATEMPMISIONTask extends TaskThread 
{
	public String mCurrentDate = PubFun.getCurrentDate();
	//public String mCurrentDate = "2017-8-18";
	public SolidificationLogSchema mSolidificationLogSchema = 
			new SolidificationLogSchema();
	public void run()
	{
		System.out.println("ExtractDataForLATEMPMISIONTask-->run 开始执行");
		if (!(checkLAWageLog(this.mCurrentDate)))
	    {
	      return;
	    }
		//查詢sql获取当前日志表中Enddate和state
		String tSql = "select * from solidificationlog where tablename = 'LATEMPMISION' with ur";
		System.out.println("tSql:"+tSql);
		//声明Db执行这个Sql
		SolidificationLogDB tSolidificationLogDB = new SolidificationLogDB();
		mSolidificationLogSchema = tSolidificationLogDB.executeQuery(tSql).get(1);
		String tState = mSolidificationLogSchema.getState();
		String tEndDate = mSolidificationLogSchema.getEndDate();
		if("11".equals(tState))
		{
			//上一天数据没有问题开始提取下一天数据
			tEndDate = PubFun.calDate(tEndDate, 1, "D", "");
		}
		//声明日期字符串转换类，因为日期比较只能使用日期格式
		FDate tFDate = new FDate();
		Date tStartDate = tFDate.getDate(tEndDate);
		Date tNowDate = tFDate.getDate(mCurrentDate);
		while(tStartDate.compareTo(tNowDate) < 0)
		{
			String tModifyDate = tFDate.getString(tStartDate);
			mSolidificationLogSchema.setEndDate(tModifyDate);
			System.out.println("开始--提取第"+tModifyDate+"天数据");
			if(!insertData(tModifyDate) || !updateData(tModifyDate) || !deleteData())
			{
				tSolidificationLogDB.setSchema(mSolidificationLogSchema);
				tSolidificationLogDB.update();
				break;
			}
			//获取当前时间
			mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
			mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
			mSolidificationLogSchema.setState("11");
			mSolidificationLogSchema.setExceptionInfo("处理成功");
			tSolidificationLogDB.setSchema(mSolidificationLogSchema);
			tSolidificationLogDB.update();
			System.out.println("结束---提取"+tModifyDate+"的数据");
			tStartDate = PubFun.calDate(tStartDate, 1, "D", null);
		}
	}
	
	public boolean insertData(String cModifyDate)
	{
		//新数据sql
		String tQueryNewDataSql = "SELECT COMMISIONSN,SUBSTR(BRANCHSERIES,1,12) DEPARTMENT, SUBSTR(BRANCHSERIES,14,12) SECTION, SUBSTR(BRANCHSERIES,27,12) BRANCH, WAGENO WAGENO, TMAKEDATE TMAKEDATE "
							+ " FROM "
							+ " LACOMMISION "
							+ " WHERE 1=1"
							+ " and BRANCHTYPE='1' "
							+ " AND TRIM(WAGENO)>= TO_CHAR(CURRENT DATE -12 MONTHS ,'YYYYMM') "
							+ " and modifydate = '" + cModifyDate + "'"
							+ " and not exists(select 1 from LATEMPMISION where LATEMPMISION.COMMISIONSN = LACOMMISION.COMMISIONSN)";
		System.out.println("tQueryNewDataSql："+ tQueryNewDataSql);
		//声明Set来接收DB返回的结果集，插入
		LATEMPMISIONSet tInsertLATEMPMISIONSet = new LATEMPMISIONSet();
		RSWrapper tInsertRSWrapper = new RSWrapper();
		tInsertRSWrapper.prepareData(tInsertLATEMPMISIONSet, tQueryNewDataSql);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		do
		{
			tInsertRSWrapper.getData();
			tMMap.put(tInsertLATEMPMISIONSet, "INSERT");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if(!tPubSubmit.submitData(tVData, ""))
			{
				System.out.println("ExtractDataForLATEMPMISIONTask 批处理执行异常！");
				//获取当前时间
				mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
				mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
				mSolidificationLogSchema.setState("00");
				mSolidificationLogSchema.setExceptionInfo("提取(插入新数据)第"+ cModifyDate +"天数据时失败");
				return false;
			}
			tMMap = new MMap();
			tVData.clear();
		}while(tInsertLATEMPMISIONSet.size()>0);
		return true;
	}
	public boolean updateData(String cModifyDate)
	{
		//旧数据sql
		String tQueryOldDataSql = "SELECT COMMISIONSN,SUBSTR(BRANCHSERIES,1,12) DEPARTMENT, SUBSTR(BRANCHSERIES,14,12) SECTION, SUBSTR(BRANCHSERIES,27,12) BRANCH, WAGENO WAGENO, TMAKEDATE TMAKEDATE "
							+ " FROM "
							+ " LACOMMISION "
							+ " WHERE 1=1"
							+ " and BRANCHTYPE='1' "
							+ " AND TRIM(WAGENO)>= TO_CHAR(CURRENT DATE -12 MONTHS ,'YYYYMM') "
							+ " and modifydate = '" + cModifyDate + "'"
							+ " and exists(select 1 from LATEMPMISION where LATEMPMISION.COMMISIONSN = LACOMMISION.COMMISIONSN)";
		System.out.println("tQueryOldDataSql："+ tQueryOldDataSql);
		//声明Set来接收DB返回的结果集，更新
		LATEMPMISIONSet tUpdateLATEMPMISIONSet = new LATEMPMISIONSet();
		RSWrapper tUpdateRSWrapper = new RSWrapper();
		tUpdateRSWrapper.prepareData(tUpdateLATEMPMISIONSet, tQueryOldDataSql);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		do
		{
			tUpdateRSWrapper.getData();
			tMMap.put(tUpdateLATEMPMISIONSet, "UPDATE");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if(!tPubSubmit.submitData(tVData, ""))
			{
				System.out.println("ExtractDataForLATEMPMISIONTask 批处理执行异常！");
				//获取当前时间
				mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
				mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
				mSolidificationLogSchema.setState("00");
				mSolidificationLogSchema.setExceptionInfo("提取(更新旧数据)第"+ cModifyDate +"天数据时失败");
				return false;
			}
			tMMap = new MMap();
			tVData.clear();
		}while(tUpdateLATEMPMISIONSet.size()>0);
		return true;
	}
	public boolean deleteData()
	{
		String tDeleteSql = "select * from LATEMPMISION where TRIM(WAGENO)< TO_CHAR(CURRENT DATE -12 MONTHS ,'YYYYMM')";
		System.out.println("tDeleteSql:"+ tDeleteSql);
		//声明Set来接收需要删除的数据
		LATEMPMISIONSet tLATEMPMISIONSet = new LATEMPMISIONSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLATEMPMISIONSet, tDeleteSql);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		do
		{
			tRSWrapper.getData();
			tMMap.put(tLATEMPMISIONSet, "DELETE");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if(!tPubSubmit.submitData(tVData, ""))
			{
				System.out.println("ExtractDataForLATEMPMISIONTask 批处理执行异常！");
				//获取当前时间
				mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
				mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
				mSolidificationLogSchema.setState("00");
				mSolidificationLogSchema.setExceptionInfo("删除旧数据时失败");
				return false;
			}
			tMMap = new MMap();
			tVData.clear();
		}while(tLATEMPMISIONSet.size()>0);
		return true;
	}
	
	private boolean checkLAWageLog(String cCurrentDate)
	{
		String tCheckSQL = "select count from lawagelog where wageno =substr(db2inst1.date_format((current date - 1 DAY), 'yyyymm'), 1, 6) "
						+ " and (state ='00' or enddate <> current date -1 days) with ur";
		if (cCurrentDate.endsWith("-02"))
	    {
			tCheckSQL = "select count from lawagelog a where wageno =substr(db2inst1.date_format((current date - 2 DAY), 'yyyymm'), 1, 6) "
					+ " and managecom  in (select comcode from ldcom where sign='1'  and  length(trim(comcode))=8) "
					+ " and not exists (select 1 from lawagelog b where b.wageno =substr(db2inst1.date_format((current date - 1 DAY), 'yyyymm'), 1, 6) "
					+ " and a.branchtype= b.branchtype and a.branchtype2= b.branchtype2 "
					+ " and a.managecom = b.managecom and b.state ='11') with ur";
	    }
		System.out.println("ExtractDataForLATEMPMISIONTask-->checkLAWageLog-->tCheckSQL:"+tCheckSQL);
		ExeSQL tExeSQL = new ExeSQL();
	    String tCount = tExeSQL.getOneValue(tCheckSQL);
	    if (!"0".equals(tCount))
	    {
	      System.out.println("ExtractDataForLATEMPMISIONTask-->checkLAWageLog,扎账提数没有提完");
	      return false;
	    }
	    return true; 
	}
	public static void main(String[] args) {
		ExtractDataForLATEMPMISIONTask tExtractDataForLATEMPMISIONTask = 
				new ExtractDataForLATEMPMISIONTask();
		tExtractDataForLATEMPMISIONTask.run();
		
	}
}
