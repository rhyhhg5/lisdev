package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAHundredNewlyBuildTask extends TaskThread {
	private String currentDate = PubFun.getCurrentDate();
	private String mEndDate = PubFun.calDate(currentDate,1,"D",null);
	
	private SSRS mSSRS1 = new SSRS();
	
	public LAHundredNewlyBuildTask(){
		
	}
	
	
	public void run(){
		System.out.println("");
        System.out.println(
                "＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝ START LAHundredNewlyBuildTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
        System.out.println("参数个数:" + mParameters.size());
        System.out.println("");
//        // Write your code here
//       
        LAHundredNewlyBuildTask a = new LAHundredNewlyBuildTask();
        this.mSSRS1=a.EasyQuery();
        for (int i = 1; i <= mSSRS1.getMaxRow(); i++)
        {
            //先查询提数的记录集
            String tAgentGroup = mSSRS1.GetText(i, 1) ;
            String tStartDate = mSSRS1.GetText(i, 2);
            String tManageCom = mSSRS1.GetText(i, 3);
          if (!a.getDate(tStartDate,mEndDate, tAgentGroup,tManageCom))
             {
                return;
             }
        }
//		
	}
    //查询参与百部新建的团队与起始日期
	public SSRS EasyQuery(){
		SSRS tSSRS = new SSRS();
		String tSql ="";
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSql);
		return tSSRS;
	}
	//处理类
	public boolean getData(String tStartDate,String tEndDate,String tAgentGroup,String tManageCom){
		
		GlobalInput tG = new GlobalInput();
		tG.Operator="000";
		tG.ManageCom="86";
		
		VData tVData = new VData();
		tVData.addElement(tG);
		tVData.addElement(tAgentGroup);
		tVData.addElement(tStartDate);
		tVData.addElement(tEndDate);
		tVData.addElement(tManageCom);
		
		LAHundredNewlyBuildBL tLAHundredNewlyBuildBL = new LAHundredNewlyBuildBL();
		if(!tLAHundredNewlyBuildBL.submitData(tVData, "")){
			
		}
		return true;
	}
	
}
