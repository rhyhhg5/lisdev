package com.sinosoft.lis.taskservice;

import com.sinosoft.httpclient.inf.LCContTranferOutReg;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LSTransOutInfoDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LSTransOutInfoSchema;
import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.lis.vschema.LSTransOutInfoSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: LCContTranfOutTask </p>
 * <p>Description: 保单转出登记批处理 </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class LCContTranfOutRegTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储传入的容器*/
	private VData mInputData = new VData();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	/**存储操作数据类型*/
	private String mOperate = "";
	
	private MMap mMap = new MMap();
	
	private ExeSQL exesql = new ExeSQL();
	
	private String mCurrDate=PubFun.getCurrentDate();
	
	private String mCurrTime=PubFun.getCurrentTime();
	
	private String mContNo ;
	
	private GlobalInput tGI = new GlobalInput();
	
	private String managecom = "86";
	
	private String whereParSql ="";
	
	
	
	public LCContTranfOutRegTask(){
		
	}
	public LCContTranfOutRegTask(String tContNo,GlobalInput pGI){
		mContNo = tContNo;
		tGI = pGI;
		managecom = pGI.ManageCom;
	}
	private boolean getInputData()
	{
		if(mContNo!=null&&!mContNo.equals(""))
		{
			whereParSql=" and a.ContNo='"+mContNo+"' ";
		}
		else
		{
			tGI.ComCode="86";
		    tGI.Operator="Server";
		    tGI.ManageCom="86";	
		}
	    return true;
	}

	/**
	 * run方法
	 */
	public void run(){
		System.out.println("");
		System.out.println("======================LCContTranfOutRegTask Execute  !!!======================");
		System.out.println("                  ");
		
		getInputData();
		
		deal();
		
	}
	
	private void deal(){
		/**transOutState保单转出状态
		 * 01-申请转出
		 * 02-拒绝转出
		 * 03-待转出状态
		 */
		String query="select a.* from LSTransOutInfo a,Lccont b " 
					+ " where a.transOutState in ('01','02') "//转出申请待登记
					+ " and a.contno=b.contno "
					+ " and b.conttype='1' "
					+ " and b.appflag='1' "
					+ whereParSql
					+ " with ur ";
		
		LSTransOutInfoSet tLSTransOutInfoSet=new LSTransOutInfoSet();
		LSTransOutInfoSchema tLSTransOutInfoSchema=null;
		tLSTransOutInfoSet=new LSTransOutInfoDB().executeQuery(query);
		System.out.println("待保单转出登记的保单共计"+tLSTransOutInfoSet.size());
		for(int i=1; i<=tLSTransOutInfoSet.size(); i++){
			boolean RejFlag=false;//拒绝转出登记
			tLSTransOutInfoSchema=tLSTransOutInfoSet.get(i);
			String mContNo=tLSTransOutInfoSchema.getContNo();
			String oldState=tLSTransOutInfoSchema.getTransOutState();
			String rejRe="";
			/**正在处理的保全校验**/
			if(!checkEdor(mContNo)){
				rejRe="保单有正在处理的保全，";
				RejFlag=true;
			}
			/**正在处理的理赔校验**/
			if(!checkClaimState(mContNo)){
				rejRe=rejRe+"保单有正在处理的理赔，";
				RejFlag=true;
			}
			/**保单是否已满期的校验**/
//			if(!checkExpirDate(mContNo)){
//				rejRe=rejRe+"保单未满期。";
//				RejFlag=true;
//			}
			String strEnd="select cinvalidate from lccont where contno='"+mContNo+"' with ur";
			String cinvalidate=exesql.getOneValue(strEnd);
			tLSTransOutInfoSchema.setRejectReason(rejRe);
			if(RejFlag){
				tLSTransOutInfoSchema.setTransOutState("02");//拒绝转出
			}else{
				tLSTransOutInfoSchema.setExEndDate(cinvalidate);//预计终止日期为保单的满期日
				tLSTransOutInfoSchema.setRegDate(mCurrDate);
			}
			String contactInfo="select b.name,b.phone,b.email from lccont a,laagent b "
								+ " where a.agentcode=b.agentcode and a.conttype='1' "
								+ " and a.contno='"+ mContNo +"' with ur ";
			SSRS tSSRS = new SSRS();
	        tSSRS =exesql.execSQL(contactInfo);
	        
	        tLSTransOutInfoSchema.setContactName(tSSRS.GetText(1, 1));
	        tLSTransOutInfoSchema.setContactTele(tSSRS.GetText(1, 2));
	        tLSTransOutInfoSchema.setContactEmail(tSSRS.GetText(1, 3));
			tLSTransOutInfoSchema.setRegDate(mCurrDate+" "+mCurrTime);
			tLSTransOutInfoSchema.setModifyDate(mCurrDate);
			tLSTransOutInfoSchema.setModifyTime(mCurrTime);
			
			mMap.put(tLSTransOutInfoSchema, "DELETE&INSERT");
			
			/**提交更新的数据**/
			if(!SubmitMap()){
				continue;
			}
			
			//原本就拒绝转出的本次就不再登记上报了
			if(RejFlag && "02".equals(oldState)){
				continue;
			}
			
			/**转出登记**/
			LSTransOutInfoSet set=new LSTransOutInfoSet();
			set.add(tLSTransOutInfoSchema);
			VData tInputData = new VData();
			tInputData.add(set);
			/**
			 * 调用保单迁出同步平台数据的接口
			 */
			LCContTranferOutReg tLCContTranferOutReg = new LCContTranferOutReg();
			
			if(!tLCContTranferOutReg.submitData(tInputData,"ZBXPT")){
			   this.mErrors.copyAllErrors(tLCContTranferOutReg.mErrors);
		       mResult.clear();
		       mResult.add(mErrors.getFirstError());
			}
		}
	}
	
	private boolean checkEdor(String aContNo) {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.edorAcceptNo ")
                .append("from LPEdorApp a, LPEdorMain  b  ")
                .append("where a.edorAcceptNo = b.edorAcceptNo ")
                .append("    and b.contNo = '")
                .append(aContNo).append("' ")
                .append("    and a.edorState != '")
                .append(BQ.EDORSTATE_CONFIRM).append("' ")
                .append("   and not exists(select 1 from LGwork ")
                .append("           where WorkNo = a.EdorAcceptNo and StatusNo = '8') "); //撤销
        System.out.println(sql.toString());
        SSRS tSSRS = exesql.execSQL(sql.toString());
        if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            return false;
        }
        return true;
    }
	
	/**
     * 按照contno查询是否有理赔案件正在处理
     * @param aContNo String
     * @return boolean
     */
    private boolean checkClaimState(String aContNo) {
        boolean tClaimFlag = true;
        String tSQL = "SELECT 1 FROM llcasepolicy a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate NOT IN ('11','12','14')"
                      + " AND a.contno='" + aContNo + "'";
        SSRS tSSRS = new SSRS();
        tSSRS = exesql.execSQL(tSQL);
        if (tSSRS.getMaxRow() > 0) {
            tClaimFlag = false;
        }
        return tClaimFlag;
    }
    
    private boolean checkExpirDate(String aContNo){
    	String str="select 1 from lcpol where appflag='1' and conttype='1' "
    			+ " and contno='"+aContNo+"' "
    			+ " and paytodate<payenddate "
    			+ " with ur";
    	String isExpir=exesql.getOneValue(str);
    	if(null !=isExpir && !"".equals(isExpir)){
    		return false;
    	}
    	return true;
    }
    
    /**数据处理**/
    private boolean SubmitMap() {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult.clear();
        mResult.add(mMap);
        if (!tPubSubmit.submitData(mResult, "")) 
        {
            System.out.println("提交数据失败！");
            return false;
        }
        this.mMap = new MMap();
        return true;
    }
	
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		LCContTranfOutRegTask tLCContTranfOutRegTask=new LCContTranfOutRegTask();
		tLCContTranfOutRegTask.run();
	}
	
}
