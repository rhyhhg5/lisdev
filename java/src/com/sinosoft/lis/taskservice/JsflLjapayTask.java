package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.lis.ygz.GetBqGetMoneySeparateBL;
import com.sinosoft.lis.ygz.GetFCSeparateBL;
import com.sinosoft.lis.ygz.GetULIInitPremSeparateBL;
import com.sinosoft.lis.ygz.GetYjGLMoneyBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: LCContTranfOutTask </p>
 * <p>Description: 保单迁出同步平台数据的批处理 </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class JsflLjapayTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	
	public JsflLjapayTask() {

	}
	
	/**
	 * run方法
	 */
	public void run(){
		System.out.println("======================JsflLjapayTask Execute  !!!======================");
		
		/**
		 * 往同步保单迁出数据中出入日期的值
		 */
		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		tInputData.add(tTransferData);
		/**
		 * 调用保单迁出同步平台数据的接口
		 */
	
//		long startGetFCSeparateBL= System.currentTimeMillis();
//		try{
//			/**
//			 * add by liyt 2016-06-24 反冲数据提取
//			 */
//			GetFCSeparateBL tGetFCSeparateBL = new GetFCSeparateBL();
//			if(!tGetFCSeparateBL.submitData(tInputData, "")){
//				this.mErrors.copyAllErrors(tGetFCSeparateBL.mErrors);
//				System.out.println("反冲数据提取提取报错："+mErrors.getFirstError());
//			}
//		} catch (Exception e) {
//			System.out.println("反冲数据提取报错：");
//			e.printStackTrace();
//		}
//		long endGetFCSeparateBL = System.currentTimeMillis();
//		System.out.println("========== GetFCSeparateBL反冲数据提取批处理-耗时： "+((endGetFCSeparateBL -startGetFCSeparateBL)/1000/60)+"分钟");
		
		long satrtGetBqGetMoneySeparateBL = System.currentTimeMillis();
		try {
			/**
			 * 保全退费数据提取
			 */
			GetBqGetMoneySeparateBL tGetBqGetMoneySeparateBL = new GetBqGetMoneySeparateBL();
			if(!tGetBqGetMoneySeparateBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetBqGetMoneySeparateBL.mErrors);
				System.out.println("保全退费数据提取报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("保全退费数据提取报错：");
			e.printStackTrace();
		}
		long endGetBqGetMoneySeparateBL = System.currentTimeMillis();
		System.out.println("========== GetBqGetMoneySeparateBL保全退费数据提取批处理-耗时： "+((endGetBqGetMoneySeparateBL -satrtGetBqGetMoneySeparateBL)/1000/60)+"分钟");

		long startGetULIInitPremSeparateBL = System.currentTimeMillis();
		try {
			/**
			 * 初始扣费数据提取
			 */
			GetULIInitPremSeparateBL tGetULIInitPremSeparateBL = new GetULIInitPremSeparateBL(); 
			if(!tGetULIInitPremSeparateBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetULIInitPremSeparateBL.mErrors);
				System.out.println("初始扣费数据提取报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("初始扣费数据提取报错：");
			e.printStackTrace();
		}
		long endGetULIInitPremSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetULIInitPremSeparateBL初始扣费数据提取批处理-耗时： "+((endGetULIInitPremSeparateBL -startGetULIInitPremSeparateBL)/1000/60)+"分钟");

		long startGetYjGLMoneyBL = System.currentTimeMillis();
		try {
		/**
		 * 月结、保全管理费数据提取
		 */
				GetYjGLMoneyBL tGetYjGLMoneyBL = new GetYjGLMoneyBL();
				if(!tGetYjGLMoneyBL.submitData(tInputData, "")){
					this.mErrors.copyAllErrors(tGetYjGLMoneyBL.mErrors);
					System.out.println("月结和保全管理费生成价税分离数据报错："+mErrors.getFirstError());
				}
			} catch (Exception e) {
				System.out.println("月结和保全管理费生成价税分离数据报错：");
				e.printStackTrace();
			}
		long endGetYjGLMoneyBL = System.currentTimeMillis();
		System.out.println("========== GetYjGLMoneyBL月结和保全管理费生成价税分离数据批处理-耗时： "+((endGetYjGLMoneyBL -startGetYjGLMoneyBL)/1000/60)+"分钟");

	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		JsflLjapayTask tJsflLjapayTask = new JsflLjapayTask();
		
		tJsflLjapayTask.run();
	}
	
}
