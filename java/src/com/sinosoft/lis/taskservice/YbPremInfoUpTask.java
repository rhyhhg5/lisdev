package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclientybk.T01.YbPremInfoUp;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *外包案件信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class YbPremInfoUpTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    String opr = "";
    
    public YbPremInfoUpTask()
    {}

    public void run()
    {
    	YbPremInfoUp  bf = new YbPremInfoUp();
    	String SQL = " select prtno from lcpol where riskcode in(select code from ldcode where codetype='ybkriskcode') and appflag='1' "
    			+" and exists(select 1 from YBKErrorList where businessno=lcpol.prtno and resultstatus='1' and transtype='N01') "
    			+" and not exists(select 1 from YBKErrorList where businessno=lcpol.contno and resultstatus='1' and transtype='T01') "
//    			+"and lcpol.prtno='YWX0000001075' "
    			+" union "
    			+" select prtno from lbpol a,lpedoritem b,ljagetendorse c,ljaget d where a.riskcode in (select code from ldcode where codetype='ybkriskcode')  "
    			+" and a.conttype = '1' and a.contno = b.contno and b.edorno = c.endorsementno and c.actugetno=d.actugetno and b.edortype in ('CT','WT') and d.enteraccdate is not null"
    			+" and exists (select 1 from YBKErrorList where businessno=b.edorno and resultstatus='1' and transtype='E03') "
    			+" and exists (select 1 from ybk_e03_lis_responseinfo where edorno = b.edorno and policyformerno is null) "
    			+" and not exists (select 1 from YBK_T_BankPremInfoResult where otherno = b.edorno )  "
//    			+"and a.prtno='YWX0000001075' "
    			+" union "
    			+" select lc.prtno from lcpol lc,lccontsub lcs where lc.riskcode in (select code from ldcode where codetype='ybkriskcode') "
    			+" and lc.prtno = lcs.prtno and lcs.ifautopay != '0'and lc.appflag = '1'and lc.renewcount<>0 "
    			+" and exists(select 1 from YBKErrorList where businessno=lc.prtno and resultstatus='1' and transtype='N01') "
    			+" and not exists (select 1 from YBK_T_BankPremInfoResult where otherno = lc.contno and renewcount = char(lc.renewcount)and RequestType = 'T01') "
    			+" and exists (select 1 from YBK_N01_LIS_ResponseInfo where prtno=lc.prtno and renewcount=lc.renewcount)"
//    			+"and lc.prtno='YWX0000001075' "
    			+" with ur ";

    				
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String tCaseNo = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("PrtNo", tCaseNo);
    			tVData.add(tTransferData);
    	    	if(!bf.submitData(tVData, "YBKPT"))
    	         {
    	    		 System.out.println("保费信息上传批处理出问题了");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             continue;
    	         }else{
    	        	 System.out.println("保费信息上传批处理成功了");
    	        	 opr ="ture";
    	         }
    		}
    	}
    	

    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

   
    public static void main(String[] args)
    {
    	YbPremInfoUpTask bfTask = new YbPremInfoUpTask();
    	bfTask.run();
    }
}




