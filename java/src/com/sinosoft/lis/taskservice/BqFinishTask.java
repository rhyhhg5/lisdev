package com.sinosoft.lis.taskservice;

import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.task.*;

/**
 * <p>Title: 保全自动结案批处理程序</p>
 * <p>Description: 每天由系统定时调用，把可以结案的保全自动结案，用于保全交费项目 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class BqFinishTask  extends TaskThread
{
    CErrors mErrors = new CErrors();

    MMap mMap = null;

    GlobalInput mGlobalInput = new GlobalInput();

    /** 当前操作人员的机构 */
    String manageCom = null;
    String WorkNo = null;

    public BqFinishTask()
    {
    }

    /**
     * 当前机构
     * @param manageCom String
     */
    public BqFinishTask(String manageCom)
    {
        this.manageCom = manageCom;
    }
    
    /**
     * 当前机构
     * @param manageCom String
     */
    public BqFinishTask(String manageCom,String WorkNo)
    {
        this.manageCom = manageCom;
        this.WorkNo = WorkNo;
    }

    /**
     * 自动结案
     */
    public void run()
    {
        System.out.println("保全自动结案运行开始！");
        List taskList = getTaskList();
        for (int i = 0; i < taskList.size(); i++)
        {
            mMap = new MMap();
            finish((String) taskList.get(i));
        }
        GrpBalancePayConfirmBL tGrpBalancePayConfirmBL = new GrpBalancePayConfirmBL();
        tGrpBalancePayConfirmBL.submitData(new VData(),"");

        System.out.println("保全自动结案运行结束！");
    }

    /**
     * 得到待结案的任务列表
     * @return List
     */
    private List getTaskList()
    {
        List taskList = new ArrayList();
        String sql = "select b.EdorAcceptNo from LJTempFee a, LPEdorApp b, lgwork c " +
                     "where a.OtherNo = b.EdorAcceptNo and b.EdorAcceptNo = c.workno " +
                     "and b.EdorState in ('2', '9') and c.typeno not like '06%'" + //9是待收费状态
                     "and a.ConfFlag = '0' " +
                     "and a.EnterAccDate is not null ";
        if (manageCom != null) //只对本机构的数据进行操作
        {
        //qulq 2007-2-27 解决财务操作使用8位机构无法收费问题。
            sql += "and b.ManageCom like '" + subComCode(manageCom) + "%' ";
        }
//      20140120 处理传入工单号的情况
        if (WorkNo != null) 
        {
            sql += "and b.edoracceptno = '" + WorkNo + "' with ur ";
        }else{
        //huxl 2008-7-17 增加万能延期结案的工单的批处理
        sql += "union all " +
               "select distinct n.EdorAcceptNo from LPEdorItem m, LPEdorApp n " +
               "where m.EdorAcceptNo = n.EdorAcceptNo " + 
               "and n.EdorState = '12' and m.Edorvalidate <= Current Date with ur";
        }
        System.out.println(sql);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL. execSQL(sql);
        System.out.println(tSSRS.getMaxRow());
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            taskList.add(tSSRS.GetText(i, 1));
        }
        return taskList;
    }
    /**
     * 截取有效机构编码
     *
     */
     private String subComCode(String managecom)
     {
     	 int len = managecom.length();
     	 for(;len>1;len--)
     	 {
                  if(managecom.charAt(len-1)!='0')
                  {
                      break;
                  }
     	 }
         return managecom.substring(0,len);
     }

    /**
     * 保全结案，工单也要结案
     * @param edorAcceptNo String
     * @return boolean
     */
    private boolean finish(String edorAcceptNo)
    {
        //得到经办人和机构
        getOperatorInfo(edorAcceptNo);

        String contType;

        //判断是团单还是个单
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(edorAcceptNo);
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
        //个单
        if (tLPGrpEdorMainSet.size() == 0)
        {
            contType = BQ.CONTTYPE_P;
//            if (!personalFinish(edorAcceptNo))
//            {
//                return false;
//            }
        }
        //团单
        else
        {
            contType = BQ.CONTTYPE_G;
//            if (!groupFinish(tLPGrpEdorMainSet.get(1)))
//            {
//                return false;
//            }
        }

        EdorFinishBL tEdorFinishBL = new EdorFinishBL(mGlobalInput, edorAcceptNo);
        if(!tEdorFinishBL.submitData(contType))
        {
            mErrors.addOneError("保全确认未通过！原因是：" +
                                tEdorFinishBL.mErrors.getFirstError());
            return false;
        }

        createRemark(edorAcceptNo);

//        if (!taskFinish(edorAcceptNo))
//        {
//            return false;
//        }
//        if (!submit())
//        {
//            return false;
//        }
        return true;
    }

    /**
     * 生成作业历史信息出错
     * @param edorAcceptNo String
     * @return boolean
     */
    private boolean createRemark(String edorAcceptNo)
    {
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
        tLGTraceNodeOpSchema.setWorkNo(edorAcceptNo);
        tLGTraceNodeOpSchema.setOperatorType(com.sinosoft.task.Task.HISTORY_TYPE_CONFIRM);
//        String edorState = CommonBL.getEdorState(edorAcceptNo);
        String edorState = com.sinosoft.lis.bq.CommonBL.getEdorState(edorAcceptNo);
        if (edorState == null)
        {
            System.out.println("保全自动结案批处理程序：没有生成作业历史信息！原因是：未找到保全状态！");
            return false;
        }
        if (edorState.equals(BQ.EDORSTATE_WAITPAY))
        { //如果是待收费状态则确认成功
            tLGTraceNodeOpSchema.setRemark("自动批注：待收费。");
        }
        else if(edorState.equals(BQ.EDORSTATE_CONFIRM))
        {
            tLGTraceNodeOpSchema.setRemark("自动批注：保全确认成功。");

        }
        else
        {
            tLGTraceNodeOpSchema.setRemark("自动批注：保全确认失败！");
        }

        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLGTraceNodeOpSchema);
        TaskTraceNodeOpBL bl = new TaskTraceNodeOpBL();
        if (!bl.submitData(data, ""))
        {
            System.out.println("保全自动结案批处理程序：生成作业历史信息出错");
            return false;
        }
        return true;
    }

    /**
     * 得到操作人员信息
     * @param edorAcceptNo String
     */
    private void getOperatorInfo(String edorAcceptNo)
    {
        String sql = "select a.Operator, b.ComCode " +
                "from LGWork a, LDUser b " +
                "where a.Operator = b.UserCode " +
                "and a.DetailWorkNo = '" + edorAcceptNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            System.out.println("未找到工单经办人信息!");
            return;
        }
        mGlobalInput.Operator = tSSRS.GetText(1, 1);
        mGlobalInput.ManageCom = tSSRS.GetText(1, 2);
        mGlobalInput.ComCode = mGlobalInput.ManageCom;
    }

    /**
     * 个单结案
     * @return boolean
     */
    private boolean personalFinish(String edorAcceptNo)
    {
        PEdorConfirmBL tPEdorConfirmBL =
                new PEdorConfirmBL(mGlobalInput, edorAcceptNo);
        MMap map = tPEdorConfirmBL.getSubmitData();
        if (map == null)
        {
            mErrors.copyAllErrors(tPEdorConfirmBL.mErrors);
            return false;
        }
        mMap.add(map);
        return true;
    }

    /**
     * 团单结案
     * @return boolean
     */
    private boolean groupFinish(LPGrpEdorMainSchema cLPGrpEdorMainSchema)
    {
        String strTemplatePath = "";
        VData data = new VData();
        data.addElement(strTemplatePath);
        data.addElement(cLPGrpEdorMainSchema);
        data.addElement(mGlobalInput);

        PGrpEdorConfirmBL tPGrpEdorConfirmBL = new PGrpEdorConfirmBL();
        MMap map = tPGrpEdorConfirmBL.getSubmitData(data, "INSERT||GRPEDORCONFIRM");
        if (map == null)
        {
            mErrors.copyAllErrors(tPGrpEdorConfirmBL.mErrors);
            return false;
        }
        mMap.add(map);
        return true;
    }

    /**
     * 工单结案
     * @param edorAcceptNo String
     * @return boolean
     */
    private boolean taskFinish(String edorAcceptNo)
    {
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setDetailWorkNo(edorAcceptNo);
        tLGWorkSchema.setTypeNo("03");  //结案状态

        VData tTaskVData = new VData();
        tTaskVData.add(mGlobalInput);
        tTaskVData.add(tLGWorkSchema);
        TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
        MMap map = tTaskAutoFinishBL.getSubmitData(tTaskVData, "");
        if (map == null)
        {
            mErrors.copyAllErrors(tTaskAutoFinishBL.mErrors);
            return false;
        }
        mMap.add(map);
        return true;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 主函数，测试用
     * @param arg String[]
     */
    public static void main(String arg[])
    {
    	BqFinishTask tBqFinishTask1 = new BqFinishTask();
        BqFinishTask tBqFinishTask = new BqFinishTask("86940000");
        BqFinishTask tBqFinishTask2 = new BqFinishTask("86940000","1111");
//        tBqFinishTask.run();
//        tBqFinishTask1.run();
        tBqFinishTask2.run();
    }
}
