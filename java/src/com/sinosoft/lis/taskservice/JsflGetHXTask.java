package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.ygz.GetLBInsureAccCSeparateBL;
import com.sinosoft.lis.ygz.GetLBInsureAccFeeTSeparateBL;
import com.sinosoft.lis.ygz.GetLBInsureAccTSeparateBL;
import com.sinosoft.lis.ygz.GetLCGrpPayActuDSeparateBL;
import com.sinosoft.lis.ygz.GetLCGrpPayDueDSeparateBL;
import com.sinosoft.lis.ygz.GetLCGrpPayPlanDSeparateBL;
import com.sinosoft.lis.ygz.GetLCInsureAccCSeparateBL;
import com.sinosoft.lis.ygz.GetLCInsureAccFeeTSeparateBL;
import com.sinosoft.lis.ygz.GetLCInsureAccTSeparateBL;
import com.sinosoft.lis.ygz.GetLjaGetESeparateBL;
import com.sinosoft.lis.ygz.GetLjaPayGSeparateBL;
import com.sinosoft.lis.ygz.GetLjaPayPSeparateBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class JsflGetHXTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mInputData = new VData();	
	
	private GlobalInput mGI = new GlobalInput();
	private TransferData mTransferData = new TransferData();
	public JsflGetHXTask() {
		mGI.Operator = "server";
		mGI.ComCode = "86";
		mGI.ManageCom = mGI.ComCode;
		mInputData.add(mGI);
	}
	
	public JsflGetHXTask(GlobalInput cGlobalInput) {
		mGI = cGlobalInput;
		mInputData.add(mGI);
	}
	public JsflGetHXTask(VData tInputData) {
		mInputData = tInputData;
	}
	/**
	 * run方法
	 */
	public void run(){
		System.out.println("======================JsflGetHXTask Execute  Start !!!======================");		

//		mInputData.add(mTransferData);
//		mInputData.add(mGI);
		
		/**
		 * 提取实收个人交费表价税分离数据
		 */
		long startGetLjaPayPSeparateBL = System.currentTimeMillis();
		try {
			GetLjaPayPSeparateBL tGetLjaPayPSeparateBL = new GetLjaPayPSeparateBL();
			if(!tGetLjaPayPSeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(tGetLjaPayPSeparateBL.mErrors);
			   System.out.println("从实收个人交费表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从实收个人交费表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLjaPayPSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLjaPayPSeparateBL从实收个人交费表生成价税分离数据批处理-耗时： "+((endGetLjaPayPSeparateBL - startGetLjaPayPSeparateBL)/1000/60)+"分钟");
		
		/**
		 * 提取实收集体交费表价税分离数据
		 */
		long startGetLjaPayGSeparateBL = System.currentTimeMillis();
		try {
			GetLjaPayGSeparateBL tGetLjaPayGSeparateBL = new GetLjaPayGSeparateBL();
			if(!tGetLjaPayGSeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(tGetLjaPayGSeparateBL.mErrors);
			   System.out.println("从实收集体交费表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从实收集体交费表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLjaPayGSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLjaPayGSeparateBL从提取实收集体交费表价税分离数据批处理-耗时： "+((endGetLjaPayGSeparateBL - startGetLjaPayGSeparateBL)/1000/60)+"分钟");
		
		/**
		 * 提取批改补退费表价税分离数据
		 */
		long startGetLjaGetESeparateBL = System.currentTimeMillis();
		try {
			GetLjaGetESeparateBL tGetLjaGetESeparateBL = new GetLjaGetESeparateBL();
			if(!tGetLjaGetESeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(tGetLjaGetESeparateBL.mErrors);
			   System.out.println("从批改补退费表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从批改补退费表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLjaGetESeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLjaGetESeparateBL从批改补退费表生成价税分离数据批处理-耗时： "+((endGetLjaGetESeparateBL - startGetLjaGetESeparateBL)/1000/60)+"分钟");
		
		/**
		 * 提取保险账户管理履历表价税分离数据
		 */
		long startGetLCInsureAccFeeTSeparateBL = System.currentTimeMillis();
		try {
			GetLCInsureAccFeeTSeparateBL tGetLCInsureAccFeeTSeparateBL = new GetLCInsureAccFeeTSeparateBL();
			if(!tGetLCInsureAccFeeTSeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(tGetLCInsureAccFeeTSeparateBL.mErrors);
			   System.out.println("从保险账户管理履历表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从保险账户管理履历表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLCInsureAccFeeTSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLCInsureAccFeeTSeparateBL从保险账户管理履历表生成价税分离数据批处理-耗时： "+((endGetLCInsureAccFeeTSeparateBL - startGetLCInsureAccFeeTSeparateBL)/1000/60)+"分钟");
		
		/**
		 * 提取保险账户管理履历备份表价税分离数据
		 */
		long startGetLBInsureAccFeeTSeparateBL = System.currentTimeMillis();
		try {
			GetLBInsureAccFeeTSeparateBL tGetLBInsureAccFeeTSeparateBL = new GetLBInsureAccFeeTSeparateBL();
			if(!tGetLBInsureAccFeeTSeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(tGetLBInsureAccFeeTSeparateBL.mErrors);
			   System.out.println("从保险账户管理履历备份表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从保险账户管理履历备份表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLBInsureAccFeeTSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLBInsureAccFeeTSeparateBL从保险账户管理履历备份表生成价税分离数据批处理-耗时： "+((endGetLBInsureAccFeeTSeparateBL - startGetLBInsureAccFeeTSeparateBL)/1000/60)+"分钟");
		
		/**
		 * 提取缴费计划实收明细表价税分离数据
		 */
		long startGetLCGrpPayActuDSeparateBL = System.currentTimeMillis();
		try {
			GetLCGrpPayActuDSeparateBL GetLCGrpPayActuDSeparateBL = new GetLCGrpPayActuDSeparateBL();
			if(!GetLCGrpPayActuDSeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(GetLCGrpPayActuDSeparateBL.mErrors);
			   System.out.println("从缴费计划实收明细表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从缴费计划实收明细表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLCGrpPayActuDSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLCGrpPayActuDSeparateBL从缴费计划实收明细表生成价税分离数据批处理-耗时： "+((endGetLCGrpPayActuDSeparateBL - startGetLCGrpPayActuDSeparateBL)/1000/60)+"分钟");
		
		/**
		 * 提取缴费计划对应明细表价税分离数据
		 */
		long startGetLCGrpPayDueDSeparateBL = System.currentTimeMillis();
		try {
			GetLCGrpPayDueDSeparateBL tGetLCGrpPayDueDSeparateBL = new GetLCGrpPayDueDSeparateBL();
			if(!tGetLCGrpPayDueDSeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(tGetLCGrpPayDueDSeparateBL.mErrors);
			   System.out.println("从缴费计划对应明细表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从缴费计划对应明细表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLCGrpPayDueDSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLCGrpPayDueDSeparateBL从缴费计划对应明细表生成价税分离数据批处理-耗时： "+((endGetLCGrpPayDueDSeparateBL - startGetLCGrpPayDueDSeparateBL)/1000/60)+"分钟");
		
		/**
		 * 提取缴费计划明细表价税分离数据
		 */
		long startGetLCGrpPayPlanDSeparateBL = System.currentTimeMillis();
		try {
			GetLCGrpPayPlanDSeparateBL tGetLCGrpPayPlanDSeparateBL = new GetLCGrpPayPlanDSeparateBL();
			if(!tGetLCGrpPayPlanDSeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(tGetLCGrpPayPlanDSeparateBL.mErrors);
			   System.out.println("从缴费计划明细表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从缴费计划明细表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLCGrpPayPlanDSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLCGrpPayPlanDSeparateBL从缴费计划明细表生成价税分离数据批处理-耗时： "+((endGetLCGrpPayPlanDSeparateBL - startGetLCGrpPayPlanDSeparateBL)/1000/60)+"分钟");
		
		/**
		 * 提取保险帐户表记价履历备份表价税分离数据
		 */
		long startGetLBInsureAccTSeparateBL = System.currentTimeMillis();
		try {
			GetLBInsureAccTSeparateBL tGetLBInsureAccTSeparateBL = new GetLBInsureAccTSeparateBL();
			if(!tGetLBInsureAccTSeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(tGetLBInsureAccTSeparateBL.mErrors);
			   System.out.println("从保险帐户表记价履历备份表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从保险帐户表记价履历备份表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLBInsureAccTSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLBInsureAccTSeparateBL从保险帐户表记价履历备份表生成价税分离数据批处理-耗时： "+((endGetLBInsureAccTSeparateBL - startGetLBInsureAccTSeparateBL)/1000/60)+"分钟");
		
		/**
		 * 提取保险帐户表记价履历表价税分离数据
		 */
		long startGetLCInsureAccTSeparateBL = System.currentTimeMillis();
		try {
			GetLCInsureAccTSeparateBL tGetLCInsureAccTSeparateBL = new GetLCInsureAccTSeparateBL();
			if(!tGetLCInsureAccTSeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(tGetLCInsureAccTSeparateBL.mErrors);
			   System.out.println("从保险帐户表记价履历表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从保险帐户表记价履历表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLCInsureAccTSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLCInsureAccTSeparateBL从保险帐户表记价履历表生成价税分离数据批处理-耗时： "+((endGetLCInsureAccTSeparateBL - startGetLCInsureAccTSeparateBL)/1000/60)+"分钟");
		
		/**
		 * 提取保险账户管理费分类表价税分离数据
		 */
		long startGetLCInsureAccCSeparateBL = System.currentTimeMillis();
		try {
			GetLCInsureAccCSeparateBL tGetLCInsureAccCSeparateBL = new GetLCInsureAccCSeparateBL();
			if(!tGetLCInsureAccCSeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(tGetLCInsureAccCSeparateBL.mErrors);
			   System.out.println("从保险账户管理费分类表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从保险账户管理费分类表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLCInsureAccCSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLCInsureAccCSeparateBL从保险账户管理费分类表生成价税分离数据批处理-耗时： "+((endGetLCInsureAccCSeparateBL - startGetLCInsureAccCSeparateBL)/1000/60)+"分钟");
		
		/**
		 * 提取保险账户管理费分类备份表价税分离数据
		 */
		long startGetLBInsureAccCSeparateBL = System.currentTimeMillis();
		try {
			GetLBInsureAccCSeparateBL tGetLBInsureAccCSeparateBL = new GetLBInsureAccCSeparateBL();
			if(!tGetLBInsureAccCSeparateBL.submitData(mInputData,"")){
			   this.mErrors.copyAllErrors(tGetLBInsureAccCSeparateBL.mErrors);
			   System.out.println("从保险账户管理费分类备份表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从保险账户管理费分类备份表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetLBInsureAccCSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetLBInsureAccCSeparateBL从保险账户管理费分类备份表生成价税分离数据批处理-耗时： "+((endGetLBInsureAccCSeparateBL - startGetLBInsureAccCSeparateBL)/1000/60)+"分钟");
		
//		/**
//		 * 提取保险账户管理费分类备份表价税分离数据
//		 */
//		long startGetLPInsureAccCSeparateBL = System.currentTimeMillis();
//		try {
//			GetLPInsureAccCSeparateBL tGetLPInsureAccCSeparateBL = new GetLPInsureAccCSeparateBL();
//			if(!tGetLPInsureAccCSeparateBL.submitData(mInputData,"")){
//			   this.mErrors.copyAllErrors(tGetLPInsureAccCSeparateBL.mErrors);
//			   System.out.println("从保全保险账户管理费分类备份表生成价税分离数据报错："+mErrors.getFirstError());
//			}
//		} catch (Exception e) {
//			System.out.println("从保全保险账户管理费分类备份表生成价税分离数据报错：");
//			e.printStackTrace();
//		}
//		long endGetLPInsureAccCSeparateBL = System.currentTimeMillis();
//		System.out.println("========== GetLPInsureAccCSeparateBL从保全保险账户管理费分类备份表生成价税分离数据批处理-耗时： "+((endGetLPInsureAccCSeparateBL - startGetLPInsureAccCSeparateBL)/1000/60)+"分钟");
//		
//		/**
//		 * 提取保全保险帐户表记价履历备份表价税分离数据
//		 */
//		long startGetLPInsureAccTSeparateBL = System.currentTimeMillis();
//		try {
//			GetLPInsureAccTSeparateBL tGetLPInsureAccTSeparateBL = new GetLPInsureAccTSeparateBL();
//			if(!tGetLPInsureAccTSeparateBL.submitData(mInputData,"")){
//			   this.mErrors.copyAllErrors(tGetLPInsureAccTSeparateBL.mErrors);
//			   System.out.println("从保全保险帐户表记价履历备份表生成价税分离数据报错："+mErrors.getFirstError());
//			}
//		} catch (Exception e) {
//			System.out.println("从保全保险帐户表记价履历备份表生成价税分离数据报错：");
//			e.printStackTrace();
//		}
//		long endGetLPInsureAccTSeparateBL = System.currentTimeMillis();
//		System.out.println("========== GetLPInsureAccTSeparateBL从保全保险帐户表记价履历备份表生成价税分离数据批处理-耗时： "+((endGetLPInsureAccTSeparateBL - startGetLPInsureAccTSeparateBL)/1000/60)+"分钟");
//		
//		/**
//		 * 提取保全保险账户管理履历表价税分离数据
//		 */
//		long startGetLPInsureAccFeeTSeparateBL = System.currentTimeMillis();
//		try {
//			GetLPInsureAccFeeTSeparateBL tGetLPInsureAccFeeTSeparateBL = new GetLPInsureAccFeeTSeparateBL();
//			if(!tGetLPInsureAccFeeTSeparateBL.submitData(mInputData,"")){
//			   this.mErrors.copyAllErrors(tGetLPInsureAccFeeTSeparateBL.mErrors);
//			   System.out.println("从保全保险账户管理履历表生成价税分离数据报错："+mErrors.getFirstError());
//			}
//		} catch (Exception e) {
//			System.out.println("从保全保险账户管理履历表生成价税分离数据报错：");
//			e.printStackTrace();
//		}
//		long endGetLPInsureAccFeeTSeparateBL = System.currentTimeMillis();
//		System.out.println("========== GetLPInsureAccFeeTSeparateBL从保全保险账户管理履历表生成价税分离数据批处理-耗时： "+((endGetLPInsureAccFeeTSeparateBL - startGetLPInsureAccFeeTSeparateBL)/1000/60)+"分钟");
		
		System.out.println("======================JsflGetHXTask Execute  End !!!======================");
	}
	
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mInputData;
	}
	
	public static void main(String[] args) {
		JsflGetHXTask tJsflGetHXTask = new JsflGetHXTask();
		tJsflGetHXTask.run();
	}
}
