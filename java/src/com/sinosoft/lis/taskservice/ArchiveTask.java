package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 工单结案90天后自动转为存档，有日处理程序运行
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class ArchiveTask extends TaskThread
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData mInputData = new VData();

    // 统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

    private MMap map = new MMap();

    public ArchiveTask()
    {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run()
    {
        if(!archive())
        {
            return;
        }
    }

    //执行存档操作
    private boolean archive()
    {
        SSRS tSSRS = getNeedArchiveTask();

        //将各工单状态转为存档
        int count = tSSRS.getMaxRow();
        if (tSSRS != null && count > 0)
        {
            for (int i = 1; i <= count; i++)
            {
                String sql = "update LGWork set "
                             + "statusNo = '6', " //转为存档
                             + "modifyDate = '" + mCurrentDate + "', "
                             + "modifyTime = '" + mCurrentTime + "' "
                             + "where  workNo = '" + tSSRS.GetText(i, 1) + "' ";
                map.put(sql, "UPDATE"); //修改
            }
        }

        //提交数据
        if(map.size() > 0)
        {
            mInputData.add(map);

            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN"))
            {
                mErrors.copyAllErrors(tPubSubmit.mErrors);
                mErrors.addOneError("日处理转为存档时出错");
                System.out.println("日处理转为存档时出错");

                return false;
            }

            return true;
        }


        return true;
    }

    //得到需要转为待办的工单好
    private SSRS getNeedArchiveTask()
    {
        SSRS tSSRS = null;
        ExeSQL tExeSQL = new ExeSQL();
        String sql = "select workNo "
                     + "from LGWork "
                     + "where StatusNo='5' and "
                     + "(days('" + mCurrentDate + "') - days(modifyDate)) > 90";

        tSSRS = tExeSQL.execSQL(sql);

        return tSSRS;
    }

    public static void main(String args[])
    {
        ArchiveTask a = new ArchiveTask();
        a.run();

        if(a.mErrors.needDealError())
        {
            System.out.println(a.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }

}
