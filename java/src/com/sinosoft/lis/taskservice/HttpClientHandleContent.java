
package com.sinosoft.lis.taskservice;

import java.math.BigInteger;

import java.io.*;
import java.net.*;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.yibaotong.DateUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


  
/**
**
**秘钥接口
***
***
*/

//    2post请求,参数json格式
public class HttpClientHandleContent {

private static String Method_Get = "GET"; // get方式

private static String Method_Post = "POST"; // post方式

public String file_path="";// 获取报文存储地址

/**
 * httpURLConnection
 * @return String 返回结果
 * @param param
 *            请求参数
 * @param serverUrl
 *            服务端地址
 * @param method
 *            请求方式
 * @return
 */
public static String sendHttpReq(String param, String sign, String serverUrl, String method) {
    BufferedReader br = null;
    DataOutputStream out = null;
    try {
    	 
    	URL realUrl = new URL(serverUrl);
        // 打开和URL之间的连接
        URLConnection urlcon = realUrl.openConnection();
        // 设置通用的请求属性
        urlcon.setRequestProperty("accept", "*/*");
        urlcon.setRequestProperty("connection", "Keep-Alive");
        urlcon.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        // 发送POST请求必须设置如下两行
        urlcon.setDoOutput(true);
        urlcon.setDoInput(true);
        // 2、发送消息
        if(Method_Post.equals(method) && ("".equals(param)||param==null)) {
            out = new DataOutputStream(urlcon.getOutputStream());
            out.write(param.getBytes("UTF-8"));
            out.flush();
            System.out.println("发送完成");
        }
        // 3、接收消息
        br = new BufferedReader(new InputStreamReader(urlcon.getInputStream(), "UTF-8"));
        String line = br.readLine();
        StringBuffer sb = new StringBuffer();
        while (null != line) {
            sb.append(line);
            line = br.readLine();
        }
        System.out.println("接收完成");
        System.out.println("响应结果：" + sb.toString());
        return sb.toString();
    } catch (IOException e) {
    	 System.out.println(e);
        return null;
    } catch (Exception e) {
    	 System.out.println(e);
        return null;
    } finally {
        if(br != null) {
            try {
                br.close();
                br = null;
            } catch (IOException e) {
            	 System.out.println(e);
            }
        }
        if(out != null) {
            try {
                out.close();
                out = null;
            } catch (IOException e) {
            	 System.out.println(e);
            }
        }
    }
}
 
/**
 * POST方式请求
 * 
 * @param xmlmsg
 * @param ServerUrl
 * @return
 */
public static String post(String param, String serverUrl) {
    return sendHttpReq(param, "", serverUrl, Method_Post);
}

/**
 * GET方式请求
 * 
 * @param xmlmsg
 * @param ServerUrl
 * @return
 */
public static String get(String param, String serverUrl) {
    return sendHttpReq(param, "", serverUrl, Method_Get);
}

/**
 * 发送带签名的post请求
 * @return String
 * @param serverUrl
 * @param param
 * @param sign
 * @return
 */
public static String post(String serverUrl, String param, String sign) {
    return sendHttpReq(param, sign, serverUrl, Method_Post);
}
 
public static void main(String args[]) throws Exception {
	HttpClientHandleContent httpch=new HttpClientHandleContent();
	System.out.println(httpch.getContent());
}
/**取秘密
 * @throws Exception 
*
*
*/
public String getContent() throws Exception{
	String id_no="";
	String n_name="";
  RSA RSA= new RSA();
  String order_id=DateUtil.getCurrentDate("yyyyMMdd")+""+PubFun1.CreateMaxNo("SCSL_JSON", 10);
  String filename=order_id+".json";//核心使用
  System.out.println("生成报文名称："+filename);
  
  String call_id="";  //标识id
  String getData="";// getData接口
  String se="select  codetype,code,codename,codealias,riskwrapplanname  from LDCode1 where CodeType='SCSL_JSON' ";
  SSRS ssrs = new SSRS();
  ExeSQL ex= new ExeSQL();
  ssrs=ex.execSQL(se);
	 if(ssrs.MaxRow>0){ // 获取状态名称
		 call_id =ssrs.GetText(1, 2);//标识id
		 getData =ssrs.GetText(1, 4);//获取Data接口
		 file_path=ssrs.GetText(1, 5);// 存储地址
	 }
	 
  //供应方公钥
  String base64="";
  
  //获得key路径
  String Supply_key_file_path=file_path+"interface1_getKey/SupplyKey.json";
//  key_file_path="E:\\wdx\\SCSLJSON\\interface2_getData\\201805\\response\\plaintext\\201805150000000019.json";
  String Supply_key_file=getKeyFile(Supply_key_file_path);
  if("".equals(Supply_key_file) || Supply_key_file==null){
	  System.out.println("获取的供应方公钥的报文为空："+Supply_key_file_path);
	  return "";
  }
  
  JSONObject Supply_jno =  JSONObject.fromObject(Supply_key_file);
  if(Supply_jno.size()>0){
	  base64 = Supply_jno.getString("supplyKey");
  }
  
  if("".equals(base64)||"".equals(base64) ){
	  System.out.println("获取的供应方公钥为空");
	  return "";
  } 
  
  
  String public_key = "";
  String private_key = "";
  String AES_Key="";
//  public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCg3Q+XgY0ptFUY4QPnpCPCowg1UR3KOLgFsRI4h4Y84ynnt3wCBw6BBz3phJCLm1BUAmN6Ug75LyKE5uOHJeJUo1XYZRdjj4p4lAxne+oty9wXh4Suz4P5q3yUeJqv17R8zHVcU0sTfVtpeUcXZck4wn6bLCMbESLwER/pLzzezQIDAQAB";  
//  private_key = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKDdD5eBjSm0VRjhA+ekI8KjCDVRHco4uAWxEjiHhjzjKee3fAIHDoEHPemEkIubUFQCY3pSDvkvIoTm44cl4lSjVdhlF2OPiniUDGd76i3L3BeHhK7Pg/mrfJR4mq/XtHzMdVxTSxN9W2l5RxdlyTjCfpssIxsRIvARH+kvPN7NAgMBAAECgYEAn3WjEBePHEkzld7wJP5TqIw+BcQsGJqS6Kl3m2vUiKTSlG0RU101ZngMSmBrfMhwH2biv416ZE7yE/Zgk0bdjF/oSsFHnI37yYd33dwI0M51jcmPt/q4xq2BNxIn4DZsnoeozDUpRgmkzWzO6SHSi5Z9xWVgxDWpxusv+HZyyW0CQQDX45blBpCCp8XWSTwksxH2fOe0Dw2zUkItRmv7vP+xDECXenbdIU9CXaQfGhE0j7uozrt/9SPPcHIf+rkf/aJ3AkEAvsBF2jOAkE58pdpx7cI3REk3k6HB/xsUmpycbhnO4OTMVi2duTHL8UaChoKTEbaGlMwbww7VOl4oQQ6J5DH12wJAXSv/53/ZxkmZ/E1HA5+01Og7J16HzyhlHx0RZWfAAi0Dg1x7CBxkFIRNXI7ndgPCPaxbB3JIrjIdTufqFYBlKQJBAKCzAyYstWbdeI2RVuHi+QSCWlv5rqHogpcu0WrAO1Wj67G+hF384b/GkzgW0SffCcpZmhneaSzfHjKQuQKSRHkCQAQ7qrnfOvPCqoY25KCLBKO9cMt+bQJ+EGvtGyEk5ZdYyjmBl4Y8VjezelQ2JLT/TF/oevcV3UbuEbRcXtnN+Co=";  
//  AES_Key="fakeyhjayiihtfamnlnl";
  
  //获得key路径
  String key_file_path=file_path+"interface1_getKey/KeyValue.json";
//  key_file_path="E:\\wdx\\SCSLJSON\\interface2_getData\\201805\\response\\plaintext\\201805150000000019.json";
  String jsonxml=getKeyFile(key_file_path);
  
  
  /**
   *返回报文存入网盘，
   *读取存入网盘的报文，
   *验证能否获得值，方便以后读取报文中的值
   */
//  JSONObject jn =  JSONObject.fromObject(jsonxml);
//  if(jn.size()>0){
//	    System.out.println("开始解析返回报文头");
//      JSONObject headers = (JSONObject) jn.getJSONObject("header");
//      String aaa=headers.getString("code");
//      System.out.println(aaa);
//  }
  
  System.out.println("密钥报文keyValue.json: "+jsonxml);
  if(jsonxml==null||"".equals(jsonxml)){
	  System.out.println("未获得密钥报文keyValue.json");
 	  return  "";
  }
  JSONObject jno =  JSONObject.fromObject(jsonxml);
  if(jno.size()>0){
	  //key
	  AES_Key = jno.getString("getKey");
	  public_key = jno.getString("publicKey");
	  private_key = jno.getString("privateKey");
  }
  
  if("".equals(AES_Key)||"".equals(public_key)||"".equals(private_key)){
	  System.out.println("未获得密钥，或者公钥私钥为空");
	  return "";
  } 
	  System.out.println("平台公钥（由核心生成）："+public_key);
	  System.out.println("平台私钥（由核心生成）："+private_key);
	  System.out.println("AES_Key（调用秘钥接口后，对方返回）："+AES_Key);
	  System.out.println("供应方公钥（由对方提供）："+base64);
 
	  //时间戳
	  String seq = new Long(new Date().getTime()).toString();
		
	  //可以新增一个SQL查询证件号
	  //暂时先配置到 ldcode,
	  //证件号，名字，开始时间，结束时间,投保单号，平台内部客户统一标识
		String idnos="";
		String sc_zl="select riskwrapplanname from ldcode1 where 1=1 and codetype='SCSLJSON_ZL' ";
		idnos=ex.getOneValue(sc_zl);
		if("".equals(idnos)|| idnos==null){
			System.out.println("获取诊疗信息接口提数sql为空");
			return "";
		}
		SSRS s=new SSRS();
		s=ex.execSQL(idnos);
		if(s.MaxRow>0){
			
		}else{
			System.out.println("未查询到证件号和姓名");
			return "";
		}
		
		String   code_req_content="";
		try {
			//平台封装报文 -start
			HashMap<String, Object> content = new HashMap<String, Object>();//报文全文
			HashMap<String, String> header = new HashMap<String, String>();//报文header属性
			HashMap<String, Object> body = new HashMap<String, Object>();//报文body属性
			//填充报文正文body信息
			content.put("header", header);
			content.put("body", body);
			body.put("order_id", order_id);
			body.put("batch_id", order_id);
			body.put("req_time", seq);
			body.put("req_nums", s.MaxRow);
			List<HashMap<String, String>> infos = new ArrayList<HashMap<String,String>>();
			
			for (int i = 1; i <= s.MaxRow; i++) {
				String idno=s.GetText(i, 1);
				String name=s.GetText(i, 2);
				 id_no=idno;
				 n_name=name;
				String starttime=s.GetText(i, 3);
				String endtime=s.GetText(i, 4);
				String bdno=s.GetText(i, 5);
				String uc_id=s.GetText(i, 6);
				System.out.println("id_no:"+idno);
				System.out.println("name:"+name);
				System.out.println("start_time:"+starttime);
				System.out.println("end_time:"+endtime);
				System.out.println("bd_no:"+bdno);
				System.out.println("ucid:"+uc_id);
				HashMap<String, String> info = new HashMap<String, String>();
				info.put("name", getMD5(name));
				info.put("bd_no", bdno);
				info.put("id_no", getMD5(idno));
				info.put("ucid", uc_id);
				info.put("start_time", starttime);
				info.put("end_time", endtime);
				infos.add(info);
			}

			body.put("infos", infos);
			JSONObject json=new JSONObject();
			JSONObject jsonbody  =json.fromObject(body);
			String jsonbodystring = jsonbody.toString();
			System.out.println("jsonbodystring"+ jsonbodystring);//String body
			
			String aes_body = AES.AESEncode(AES_Key, jsonbodystring);//加密后的body内容体内容
			System.out.println("body加密后："+aes_body);
			String sign = RSA.sign(aes_body.getBytes(), private_key);//对加密后的body内容体内容进行签名
			//组装报文全文
			content = new HashMap<String, Object>();
			header = new HashMap<String, String>();
			body = new HashMap<String, Object>();
			content.put("header", header);
			content.put("body", aes_body);
			//使用供应方公钥对call_id进行加密，加入header内容体
			header.put("call_id", RSA.encryptBase64(RSA.encryptByPublicKey(call_id.getBytes(), base64)));
			header.put("signature", sign);
			
			if(jsonbodystring!=null && !"".equals(jsonbodystring)){
				//组装报文全文
				HashMap<String, Object> content_C = new HashMap<String, Object>();
				HashMap<String, String> header_C = new HashMap<String, String>();
				content_C.put("header", header_C);
				content_C.put("body", jsonbodystring);
				//使用供应方公钥对call_id进行加密，加入header内容体
				header_C.put("call_id", call_id);
				header_C.put("signature", sign);
				System.out.println("请求报文明文：" + content_C);
				startReq(filename,content_C.toString(),"plaintext","request"); //存明文，请求
			}else{
				System.out.println("组装的BODY为空");
				return "";
			}
			System.out.println("请求报文base64编码前：" + content);
			JSONObject jsonobj = new JSONObject();
			JSONObject jsoncontent = jsonobj.fromObject(content);
			String jsonstr =jsoncontent.toString();
			code_req_content = RSA.encryptBase64(jsonstr.getBytes());//编码后的报文全文
			System.out.println("请求报文base64编码后：" + code_req_content);
			if(jsonstr!=null && !"".equals(jsonstr)){
				startReq(filename,jsonstr,"ciphertext","request"); //存密文,请求
			}else{
				System.out.println("请求报文，密文为空");
				return "";
			}
			//平台封装报文 -end
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
    String str = HttpClientHandle.post("content=" +code_req_content, getData);
    //解密
    //私钥
    System.out.println("返回报文:"+str);
    byte [] resultvalue =RSA.decryptBase64(str);
    String sss= new String(resultvalue,"UTF-8");
    System.out.println("返回报文Base64解码后:"+sss);
    
    startReq(filename,sss,"ciphertext","response"); //存密文,返回
    
    JSONObject jn =  JSONObject.fromObject(sss);
    if(jn.size()>0){
	    System.out.println("开始解析返回报文头");
        JSONObject headers = (JSONObject) jn.getJSONObject("header");
    	    String code = headers.getString("code");//("code").toString;
    	    String message = headers.getString("message");
    	    String signature = headers.getString("signature");
    	    String bodys = (String) jn.get("body");
    	    System.out.println("输出header信息");
    	    System.out.println("code:"+code);
    	    System.out.println("message:"+message);
    	    System.out.println("signature:"+signature);
   
    	    boolean status = RSA.verify(bodys.getBytes(), base64, signature);  
    	    if(status){ //验签成功
    	    	System.out.println("验签成功");
    	    	HashMap<String, Object> body_C = new HashMap<String, Object>();//报文body属性
    	    	HashMap<String, Object> content_CC = new HashMap<String, Object>();
    	    	HashMap<String, String> header_C = new HashMap<String, String>();
    	    	
    	    	header_C.put("code",code);
    	    	header_C.put("message",message);
    	    	header_C.put("signature",signature);
    	    	
				content_CC.put("header", header_C);
				
    	    	if("10001".equals(code)){

    	    		String bodys_String = AES.AESDncode(AES_Key, bodys);
    	    		System.out.println("body解密后："+bodys_String);
    	    	    JSONObject bodys_Json =  JSONObject.fromObject(bodys_String);
    				List<HashMap<String, String>> infos_C = new ArrayList<HashMap<String,String>>();
    	    	    if(bodys_Json.size()>0){//解析body信息
	    	    	    
//    	    	        JSONObject headers = (JSONObject) bodys_Json.getJSONObject("header");
        	    		System.out.println("开始解析body中信息");
    	    	    	    String order_id1 = bodys_Json.getString("order_id");
    	    	    	    String batch_id = bodys_Json.getString("batch_id");
    	    	    	    String req_nums = bodys_Json.getString("req_nums");
    	    	    	    String rep_nums = bodys_Json.getString("rep_nums");
    	    	    	    String req_time = bodys_Json.getString("req_time");
    	    	    	    String rep_time = bodys_Json.getString("rep_time");
    	    	    	    JSONArray infos_JSONArray = bodys_Json.getJSONArray("infos");
    	    	    	    
    	    	    	    body_C.put("order_id", order_id1);
    	    	    	    body_C.put("batch_id", batch_id);
    	    	    	    body_C.put("req_nums", req_nums);
    	    	    	    body_C.put("rep_nums", rep_nums);
    	    	    	    body_C.put("req_time", req_time);
    	    	    	    body_C.put("rep_time", rep_time);
    	    	    	    body_C.put("infos", infos_C);

    	    	    	    System.out.println("rep_time:"+rep_time);
    	    	    	    System.out.println("infos:"+infos_JSONArray);
    	    	    	    
    	    	    	    
    	    	    	    if(infos_JSONArray.size()>0){ //循环解析info信息
    	    	    	    	for (int i = 0; i < infos_JSONArray.size(); i++) {
    	    	    	    		JSONObject info_JSONs=infos_JSONArray.getJSONObject(i);
    	    	    	    		
	    	    	    			
    	    	    	    		if(info_JSONs.size()>0){
    	    	    	    	    	System.out.println("开始解析info中信息");
    	    	    	    			String result=info_JSONs.getString("result");                                
    	    	    	    			String start_time=info_JSONs.getString("start_time");                            
    	    	    	    			String end_time=info_JSONs.getString("end_time");                              
    	    	    	    			String data_update_time=info_JSONs.getString("data_update_time");                      
    	    	    	    			String name1=info_JSONs.getString("name");                                  
    	    	    	    			String id_no1=info_JSONs.getString("id_no");                                 
    	    	    	    			String ucid=info_JSONs.getString("ucid");                                  
    	    	    	    			String bd_no=info_JSONs.getString("bd_no");                                 
    	    	    	    			String potential_hospital1=info_JSONs.getString("potential_hospital1");                   
    	    	    	    			String potential_hospital1_rate=info_JSONs.getString("potential_hospital1_rate");              
    	    	    	    			String potential_hospital2=info_JSONs.getString("potential_hospital2");                   
    	    	    	    			String potential_hospital2_rate=info_JSONs.getString("potential_hospital2_rate");              
    	    	    	    			String potential_hospital3=info_JSONs.getString("potential_hospital3");                   
    	    	    	    			String potential_hospital3_rate=info_JSONs.getString("potential_hospital3_rate");              
    	    	    	    			String potential_code1=info_JSONs.getString("potential_code1");                       
    	    	    	    			String potential_code2=info_JSONs.getString("potential_code2");                       
    	    	    	    			String potential_code3=info_JSONs.getString("potential_code3");                       
    	    	    	    			String potential_level=info_JSONs.getString("potential_level");        
    	    	    	    			String potential_code1_rate=info_JSONs.getString("potential_code1_rate");        
    	    	    	    			String potential_code2_rate=info_JSONs.getString("potential_code2_rate");  
    	    	    	    			String potential_code3_rate=info_JSONs.getString("potential_code3_rate");  
    	    	    	    			///////
    	    	    	    			 
    	    	    	    				HashMap<String, String> info_C = new HashMap<String, String>();
    	    	    	    				info_C.put("result", result);
    	    	    	    				info_C.put("name", n_name);
    	    	    	    				info_C.put("id_no", id_no);
    	    	    	    				info_C.put("ucid", ucid);
    	    	    	    				
    	    	    	    				info_C.put("potential_hospital1", potential_hospital1);
    	    	    	    				info_C.put("potential_hospital1_rate", potential_hospital1_rate);
    	    	    	    				info_C.put("potential_hospital2", potential_hospital2);
    	    	    	    				info_C.put("potential_hospital2_rate", potential_hospital2_rate);
    	    	    	    				info_C.put("potential_hospital3", potential_hospital3);
    	    	    	    				info_C.put("potential_hospital3_rate", potential_hospital3_rate);
    	    	    	    				
    	    	    	    				info_C.put("potential_level", potential_level);
    	    	    	    				 
    	    	    	    				info_C.put("potential_code1", potential_code1);
    	    	    	    				info_C.put("potential_code1_rate", potential_code1_rate);
    	    	    	    				info_C.put("potential_code2", potential_code2);
    	    	    	    				info_C.put("potential_code2_rate", potential_code2_rate);
    	    	    	    				info_C.put("potential_code3", potential_code3);
    	    	    	    				info_C.put("potential_code3_rate", potential_code3_rate);
    	    	    	    				
    	    	    	    				info_C.put("data_update_time", data_update_time);
    	    	    	    				infos_C.add(info_C);
    	    	    	    			
    	    	    	    			
    	    	    	    			////////
    	    	    	    			
    	    	    	    			System.out.println("result："+result);                             
    	    	    	    			System.out.println("start_time："+start_time);                          
    	    	    	    			System.out.println("end_time："+end_time);                            
    	    	    	    			System.out.println("data_update_time："+data_update_time);                    
    	    	    	    			System.out.println("name："+name1);                                
    	    	    	    			System.out.println("id_no："+id_no1);                               
    	    	    	    			System.out.println("ucid："+ucid);                                
    	    	    	    			System.out.println("bd_no："+bd_no);                               
    	    	    	    			System.out.println("potential_hospital1："+potential_hospital1);                 
    	    	    	    			System.out.println("potential_hospital1_rate："+potential_hospital1_rate);            
    	    	    	    			System.out.println("potential_hospital2："+potential_hospital2);                 
    	    	    	    			System.out.println("potential_hospital2_rate："+potential_hospital2_rate);            
    	    	    	    			System.out.println("potential_hospital3："+potential_hospital3);                 
    	    	    	    			System.out.println("potential_hospital3_rate："+potential_hospital3_rate);            
    	    	    	    			System.out.println("potential_code1："+potential_code1);                     
    	    	    	    			System.out.println("potential_code2："+potential_code2);                     
    	    	    	    			System.out.println("potential_code3："+potential_code3);                     
    	    	    	    		}
								}
    	    	    	    }
    	    	    	    
    	    	    }
    	    	    content_CC.put("body", body_C);
        	    	System.out.println("成功");
        	    	JSONObject jsonobj = new JSONObject();
        			JSONObject jsoncontent = jsonobj.fromObject(content_CC);
        			String jsonstr =jsoncontent.toString();
	    			System.out.println("返回报文，明文内容："+jsonstr);
	    			
    	    		startReq(filename,jsonstr,"plaintext","response"); //存明文，返回
        	    	
        	    	return "OK";
        	    }else if("10002".equals(code)){
        	    	System.out.println("参数错误");
        	    }else if("10003".equals(code)){
        	    	System.out.println("系统异常");
        	    }else if("10004".equals(code)){
        	    	System.out.println("用户验证失败");
        	    }else if("10005".equals(code)){
        	    	System.out.println("数字签名验证(signature)失败");
        	    }else if("10006".equals(code)){
        	    	System.out.println("数据查询超时");
        	    }
    	    }else{
    	    	System.out.println("验签失败，需要再次发送请求，暂不实现该处理");
    	    }
    }
    System.out.println("数据大小："+jn.size());
	return "";
}
public void startReq(String filename,String reqxml,String type,String req_res){
 	
   	//存储报文
 	String  rootPath_input=file_path+"interface2_getData/"+DateUtil.getCurrentDate("yyyyMM")+"/"+req_res+"/"+type+"/";
 	File fileDir = new File(rootPath_input );
     if(!fileDir.exists()){
         fileDir.mkdirs();
     }
    
     String rootPathbat=rootPath_input+filename;
 	
 	try {
 		writeXml2File(rootPathbat, reqxml);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
 }
public static void writeXml2File(String filePath, String xmlString) throws IOException 
{
	File inputXmlFile = new File(filePath);
    PrintWriter pw = new PrintWriter(new FileWriter(inputXmlFile));
    try{
    	pw.write(xmlString);
    	pw.flush();
    }finally{
    	pw.close();
    }
}
	public static String getMD5(String str) {  
	try { 
		// 生成一个MD5加密计算摘要 
		MessageDigest md = MessageDigest.getInstance("MD5");  
		// 计算md5函数  
		md.update(str.getBytes());  
		// digest()最后确定返回md5 hash值，返回值为8为字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符  
		// BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值  
		 
		return new BigInteger(1, md.digest()).toString(16);  
		} catch (Exception e) { 
		System.out.println("MD5加密出现错误"); 
		return "";
		}

	}
	public String getKeyFile(String key_file_patch){
		
		File file=new File(key_file_patch);
        String content = null;
		try {
			content = FileUtils.readFileToString(file,"UTF-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("读取的文件内容："+content);
		return content;
	}
 
}