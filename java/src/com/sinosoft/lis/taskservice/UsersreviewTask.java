package com.sinosoft.lis.taskservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.schema.LDUserSchema;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.vschema.LDUserSet;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.WriteToExcel;

public class UsersreviewTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */

	//执行任务
	public void run() {
		//判断是否可以执行
		getUser();
	}

	//提取用户
	public boolean getUser() {
		
		if (!getStartRun()) {
			System.out.println("既往客户提取：非月初1号，不用提取数据！");
			return false;
		}
		
		ExeSQL tEx = new ExeSQL();

//		当前总用户数
		String tUserCount = tEx.getOneValue("Select  Count(1) From Lduser"); 
//		当前总停用用户数
		String tDisAllUserCount = tEx.getOneValue("Select Count(1) From Lduser Where Userstate = '1'"); 
//		当月失效数
//		String tDisUserMouCount = tEx.getOneValue("Select  Count(1) From Lduser Where Userstate = '1' And Modifydate Between date(Trim(Char(Year(Current date))) || '-' || Trim(Char(Month(Current date))) || '-1') -1 month And date(Trim(Char(Year(Current date))) || '-' || Trim(Char(Month(Current date))) || '-1') -1 day");
		//自动失效数
//		String tDisUserAtuoCount = tEx.getOneValue("Select  Count(1) From Lduser Where Userstate = '1' And Modifydate Between date(Trim(Char(Year(Current date))) || '-' || Trim(Char(Month(Current date))) || '-1') -1 month And date(Trim(Char(Year(Current date))) || '-' || Trim(Char(Month(Current date))) || '-1') -1 day And Modifyoperator = 'sys' ");
		//当月停用
		String tDisDetailSql = "Select Usercode, Username, Comcode, Userdescription, Operator, Validstartdate, Makedate, Modifydate, Modifyoperator,(Select Max(Makedate) From Lduserlog a Where a.Operator = Usercode), '失效', case Modifyoperator when 'sys' then '批处理停用' else '正常停用' end 内审结论 From Lduser Where Userstate = '1' And Modifydate Between date(Trim(Char(Year(Current date))) || '-' || Trim(Char(Month(Current date))) || '-1') -1 month And date(Trim(Char(Year(Current date))) || '-' || Trim(Char(Month(Current date))) || '-1') -1 day With Ur ";
		//自动失效
		String tDisDetailAutoSql = "Select Usercode, Username, Comcode, Userdescription, Operator, Validstartdate, Makedate, Modifydate, '自动失效',(Select Max(Makedate) From Lduserlog a Where a.Operator = Usercode), '失效', case Modifyoperator when 'sys' then '批处理停用' else '正常停用' end 内审结论 From Lduser Where Userstate = '1' And Modifydate Between date(Trim(Char(Year(Current date))) || '-' || Trim(Char(Month(Current date))) || '-1') -1 month And date(Trim(Char(Year(Current date))) || '-' || Trim(Char(Month(Current date))) || '-1') -1 day And Modifyoperator = 'sys' With Ur "; 

		SSRS tDisLDUsers = tEx.execSQL(tDisDetailSql);
		int tDisUserMouCount = tDisLDUsers.getMaxRow();
		SSRS tDisAutoLDUsers = tEx.execSQL(tDisDetailAutoSql);
		int tDisUserAtuoCount = tDisAutoLDUsers.getMaxRow();

		int count = tDisUserMouCount + tDisUserAtuoCount; // 行数
		
		Calendar calendar = Calendar.getInstance();//可以对每个时间域单独修改
		int tYear = calendar.get(Calendar.YEAR);
		int tMonth = calendar.get(Calendar.MONTH);
		
		//创建对象
		String[][] tToExcel = new String[count + 10][12];
		System.out.println("统计时间" + tYear + "-" + tMonth);

		tToExcel[0][0] = "系统用户状态";
		tToExcel[1][0] = "统计机构：总公司";
		tToExcel[2][0] = "统计时间：" + tYear + "-" + tMonth;
		tToExcel[3][0] = "当前总用户数";
		tToExcel[3][1] = "当前总停用用户数";
		tToExcel[3][2] = "当月停用用户数";
		tToExcel[3][3] = "自动失效用户数";

		tToExcel[4][0] = tUserCount;
		tToExcel[4][1] = tDisAllUserCount;
		tToExcel[4][2] = String.valueOf(tDisUserMouCount);
		tToExcel[4][3] = String.valueOf(tDisUserAtuoCount);

		//当月停用用户清单
		tToExcel[5][0] = "当月停用用户清单";
		
		tToExcel[6][0] = "用户编码";
		tToExcel[6][1] = "用户名称";
		tToExcel[6][2] = "用户管理机构";
		tToExcel[6][3] = "用户描述";
		tToExcel[6][4] = "用户上级";
		tToExcel[6][5] = "有效开始日期";
		tToExcel[6][6] = "录入时间";
		tToExcel[6][7] = "停用日期";
		tToExcel[6][8] = "停用操作人";
		tToExcel[6][9] = "最后一次登陆日期";
		tToExcel[6][10] = "状态";
		tToExcel[6][11] = "内审结论";

		int excelRow = 6;

		for (int row = 1; row <= tDisUserMouCount; row++) {
			excelRow++;
			tToExcel[excelRow][0] = tDisLDUsers.GetText(row, 1);
			tToExcel[excelRow][1] = tDisLDUsers.GetText(row, 2);
			tToExcel[excelRow][2] = tDisLDUsers.GetText(row, 3);
			tToExcel[excelRow][3] = tDisLDUsers.GetText(row, 4);
			tToExcel[excelRow][4] = tDisLDUsers.GetText(row, 5);
			tToExcel[excelRow][5] = tDisLDUsers.GetText(row, 6);
			tToExcel[excelRow][6] = tDisLDUsers.GetText(row, 7);
			tToExcel[excelRow][7] = tDisLDUsers.GetText(row, 8);
			tToExcel[excelRow][8] = tDisLDUsers.GetText(row, 9);
			tToExcel[excelRow][9] = tDisLDUsers.GetText(row, 10);
			tToExcel[excelRow][10] = tDisLDUsers.GetText(row, 11);
			tToExcel[excelRow][11] = tDisLDUsers.GetText(row, 12);

		}
		
		int excelRow1 = tDisUserMouCount + 9;
		int excelRow2 = tDisUserMouCount + 8;
		//自动失效用户清单
		tToExcel[excelRow2][0] = "自动失效用户清单";
		tToExcel[excelRow1][0] = "用户编码";
		tToExcel[excelRow1][1] = "用户名称";
		tToExcel[excelRow1][2] = "用户管理机构";
		tToExcel[excelRow1][3] = "用户描述";
		tToExcel[excelRow1][4] = "用户上级";
		tToExcel[excelRow1][5] = "有效开始日期";
		tToExcel[excelRow1][6] = "录入时间";
		tToExcel[excelRow1][7] = "停用日期";
		tToExcel[excelRow1][8] = "停用操作人";
		tToExcel[excelRow1][9] = "最后一次登陆日期";
		tToExcel[excelRow1][10] = "状态";
		tToExcel[excelRow1][11] = "内审结论";

		for (int row1 = 1; row1 <= tDisUserAtuoCount; row1++) {
			excelRow1++;
			tToExcel[excelRow1][0] = tDisAutoLDUsers.GetText(row1, 1);
			tToExcel[excelRow1][1] = tDisAutoLDUsers.GetText(row1, 2);
			tToExcel[excelRow1][2] = tDisAutoLDUsers.GetText(row1, 3);
			tToExcel[excelRow1][3] = tDisAutoLDUsers.GetText(row1, 4);
			tToExcel[excelRow1][4] = tDisAutoLDUsers.GetText(row1, 5);
			tToExcel[excelRow1][5] = tDisAutoLDUsers.GetText(row1, 6);
			tToExcel[excelRow1][6] = tDisAutoLDUsers.GetText(row1, 7);
			tToExcel[excelRow1][7] = tDisAutoLDUsers.GetText(row1, 8);
			tToExcel[excelRow1][8] = tDisAutoLDUsers.GetText(row1, 9);
			tToExcel[excelRow1][9] = tDisAutoLDUsers.GetText(row1, 10);
			tToExcel[excelRow][10] = tDisAutoLDUsers.GetText(row1, 11);
			tToExcel[excelRow][11] = tDisAutoLDUsers.GetText(row1, 12);
		}

		try {
			String mOutXmlPath = CommonBL.getUIRoot();
			String tSQL = "select sysvarvalue from ldsysvar where sysvar = 'InvalidUser' ";
			String tPath = new ExeSQL().getOneValue(tSQL);
			
			System.out.println(mOutXmlPath);
//			String name = tYear + "年" + tMonth + "月用户日志审核报告.xls";
			String name = tYear + "-" + tMonth + "-" + "UsersOfTheAuditReport.xls";
			System.out.println("字符变更前名称："+name);
//			name = new String(name.getBytes("GBK"),"iso-8859-1");
			System.out.println("字符变更后名称："+name);
			String tAllName = mOutXmlPath + tPath + name;
			System.out.println("文件名称====："+tAllName);
			
			WriteToExcel t = new WriteToExcel(name);
			t.createExcelFile();
			t.setAlignment("CENTER");
			t.setFontName("宋体");
			String[] sheetName = { new String("Data") };
			t.addSheet(sheetName);
//			String[] sheetName = { new String("用户日志审核表") };
//			t.addSheetGBK(sheetName);
			t.setData(0, tToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
			
//			//获取文件设置格式
			FileInputStream is = new FileInputStream(new File(mOutXmlPath + name)); //获取文件      
			HSSFWorkbook wb = new HSSFWorkbook(is);
			HSSFSheet sheet = wb.getSheetAt(0); //获取sheet
			
//			设置字体-标题
			HSSFFont titleFont = wb.createFont();
			titleFont.setFontName("宋体");
			titleFont.setFontHeightInPoints((short) 12);//设置字体大小
			titleFont.setColor((short)1);//字体颜色白字
			
//			设置字体-内容
			HSSFFont tFont = wb.createFont();
			tFont.setFontName("宋体");
			tFont.setFontHeightInPoints((short) 10);//设置字体大小
			
//			设置样式-标题
			HSSFCellStyle titleStyle = wb.createCellStyle();
			titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			titleStyle.setFillForegroundColor(HSSFColor.BLUE_GREY.index);//背景色
			titleStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			titleStyle.setFont(titleFont);
			titleStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			titleStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			titleStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			titleStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
			
//			第一行样式
			HSSFCellStyle tFRowStyle = wb.createCellStyle(); //获取样式
			tFRowStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			tFRowStyle.setFillForegroundColor((short)22);//背景色
			tFRowStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			tFRowStyle.setFont(tFont);
			tFRowStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			tFRowStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tFRowStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tFRowStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
//			
//			正文样式
			HSSFCellStyle tContentStyle = wb.createCellStyle(); //获取样式
			tContentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			tContentStyle.setFont(tFont);
			tContentStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tContentStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

			
			//标题字体
//			HSSFFont font2 = wb.createFont();
//			font2.setFontName("宋体");
//			font2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//粗体显示
//			font2.setFontHeightInPoints((short) 14);
//			hfcStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
//			hfcStyle.setFont(font);//选择需要用到的字体格式
			//设置行高 列宽
			for (int i = 0; i < count + 10; i++) {
				HSSFRow row = sheet.getRow(i);
				row.setHeight((short) 500);
				for (int j = 0; j < 12; j++) {
					HSSFCell firstCell = row.getCell((short) j);
					sheet.setColumnWidth((short) j, (short) 6000);
//					firstCell.setCellStyle(tContentStyle);
				}

			}
			//合并单元格
			sheet.addMergedRegion(new Region(0, (short) 0, 0, (short) 3));
			sheet.addMergedRegion(new Region(1, (short) 0, 1, (short) 3));
			sheet.addMergedRegion(new Region(2, (short) 0, 2, (short) 3));
			sheet.addMergedRegion(new Region(5, (short) 0, 5, (short) 11));
			sheet.addMergedRegion(new Region(tDisUserMouCount + 8, (short) 0, tDisUserMouCount + 8, (short) 11));

			

			HSSFRow tRow0 = sheet.getRow((short) 0);
			for(int i=0;i<4;i++){
				HSSFCell tRow0Cell = tRow0.getCell((short) i);
				tRow0Cell.setCellStyle(titleStyle);
			}
			
//			HSSFRow tRow1 = sheet.getRow((short) 1);
//			HSSFCell tRow1Cell = tRow1.getCell((short) 0);
//			tRow1Cell.setCellStyle(tFRowStyle);
			
			HSSFRow tRow3 = sheet.getRow((short) 3);
			for(int i=0;i<4;i++){
				HSSFCell tRow3Cell = tRow3.getCell((short) i);
				tRow3Cell.setCellStyle(titleStyle);
			}
			
			HSSFRow tRow5 = sheet.getRow((short) 5);
			for(int i=0;i<12;i++){
				HSSFCell tRow5Cell = tRow5.getCell((short) i);
				tRow5Cell.setCellStyle(titleStyle);
			}
			
			HSSFRow tRow6 = sheet.getRow((short) 6);
			for(int i=0;i<12;i++){
				HSSFCell tRow6Cell = tRow6.getCell((short) i);
				tRow6Cell.setCellStyle(titleStyle);
			}
			
			HSSFRow tAutoRow1 = sheet.getRow((short)tDisUserMouCount + 8);
			for(int i=0;i<12;i++){
				HSSFCell tAutoCell1 = tAutoRow1.getCell((short) i);
				tAutoCell1.setCellStyle(titleStyle);
			}
			
			HSSFRow tAutoRow9 = sheet.getRow((short)tDisUserMouCount + 9);
			for(int i=0;i<12;i++){
				HSSFCell tAutoCell9 = tAutoRow9.getCell((short) i);
				tAutoCell9.setCellStyle(titleStyle);
			}
			
			FileOutputStream fileOut = new FileOutputStream(tAllName);
			wb.write(fileOut);
			fileOut.close();
			// 邮箱发送
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS.GetText(1, 2),"picchealth");
			//			 标题，内容，附件(多附件可以用|分隔，末尾|可加可不加)
			
			System.out.println("字符变更前所有名称："+tAllName);
//			tAllName = new String(tAllName.getBytes("iso-8859-1"),"GBK");
			System.out.println("字符变更后所有名称："+tAllName);
			
			tMailSender.setSendInf(tYear+"年"+tMonth+"月份既往失效用户审核", "您好：\r\n附件是提取的"+tYear+"年"+tMonth+"月份用户审核报告，请审核。",tAllName);
			
			String tSQLRMailSql = "select codealias,codename from ldcode where codetype = 'invaliduserrmail' ";
			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1), tSSRSR.GetText(1, 2), null);
			//				 发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}

		} catch (Exception ex) {
			this.mErrors.addOneError(new CError("每月用户日志审核时出错",
					"UsersreviewTask", "run"));
			ex.toString();
			ex.printStackTrace();
			return false;
		}

		return true;

	}

	// 主方法 测试用
	public static void main(String[] args) {
		new UsersreviewTask().run();

	}
	
	private boolean getStartRun() {
		String isRun = new ExeSQL()
				.getOneValue("select Trim(Char(Day(current date)))  from dual");
		if (!"1".equals(isRun)) {
			return false;
		}
		return true;
	}
}
