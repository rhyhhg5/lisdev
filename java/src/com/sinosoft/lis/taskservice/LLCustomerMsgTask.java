package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.SendMsgMail;
import com.sinosoft.lis.taskservice.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun1;

import java.io.IOException;

import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;

import java.net.*;

import com.sinosoft.lis.llcase.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 发送客户理赔短信
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author MN
 * @version 1.0
 */
public class LLCustomerMsgTask extends TaskThread {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    String Content = "";
    String Email = "";
    String Mobile = "";
    // 输入数据的容器
    private VData mInputData = new VData();
    public String opr = "";

    private MMap map = new MMap();
    public LLCustomerMsgTask() {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run() {
        if (!sendCustomerMsg()) {
        	System.out.println("理赔短信处理有问题");
            System.out.println(mErrors.getErrContent());
            opr ="false";
            return;
        }else {
        	System.out.println("理赔短信处理成功");
       	 	opr ="true";
        }
    }

    private boolean sendCustomerMsg() {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
//        LLCustomerMsgBL tLLCustomerMsgBL = new LLCustomerMsgBL();
//        if (!tLLCustomerMsgBL.submitData(mVData, "LPMSG")) {
//            System.out.println("客户理赔短信发送失败！");
//            CError tError = new CError();
//            tError.moduleName = "LLCustomerMsgBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "客户理赔短信发送失败";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        LLCustomerMsgNewBL tLLCustomerMsgNewBL = new LLCustomerMsgNewBL();
        if (!tLLCustomerMsgNewBL.submitData(mVData, "LPMSG")) {
            System.out.println("客户理赔短信发送失败！");
            CError tError = new CError();
            tError.moduleName = "LLCustomerMsgNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "客户理赔短信发送失败";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

    public static void main(String args[]) {
        LLCustomerMsgTask a = new LLCustomerMsgTask();
        a.run();

    }
}
