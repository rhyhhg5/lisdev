package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.yibaotong.LLHWTransFtpXmlImage;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *  浙江外包扫描件信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Zhang
 * @version 1.1
 */

public class LLHWImageTask extends TaskThread {
	
	/***
	 * 错误的容器
	 */
	public CErrors tErrors =new CErrors();
	String opr="";
	
	public LLHWImageTask(){
		
	}
	
	public void  run(){
		
		LLHWTransFtpXmlImage tLLHWTransFtpXmlImage = new LLHWTransFtpXmlImage();
		
		if(!tLLHWTransFtpXmlImage.submitData()){
			 System.out.println("扫描件信息导入出问题了");
             System.out.println(tErrors.getErrContent());
             opr="false";
             return;
			
		}else{
			 System.out.println("扫描件信息导入成功了");
        	 opr="true";
		}
	}
	
	
	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	public static void main(String[] args) {
		LLHWImageTask tLLHWImageTask = new LLHWImageTask();
		tLLHWImageTask.run();
	}

}
