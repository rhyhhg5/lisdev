package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.IAC_LDPERSONSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.vschema.IAC_LDPERSONSet;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.taskservice.TaskThread;

public class BDYZExtractPersonTask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	private CErrors mErrors = new CErrors();

	private MMap mMMap = new MMap();

	private VData mVData = new VData();

	/**默认分组时是以150分一组*/
	private int mMaxNumCount = 150;

	/**提取数据的日期 */
	public String mYesterDay = PubFun.calDate(PubFun.getCurrentDate(), -1, "D", "");
	/** 设置附件路径 */
	public BDYZExtractPersonTask() {
	}
	
	public void run() {
		String tBatchNo = mYesterDay.replace("-", "") + "001";// 批次号
		IAC_LDPERSONSet tOtherIAC_LDPERSONSet = new IAC_LDPERSONSet();
		IAC_LDPERSONSet tInsertIAC_LDPERSONSet = new IAC_LDPERSONSet();
		LDPersonSet tLDPersonSet = new LDPersonSet();
		String tCurrentDate = PubFun.calDate(mYesterDay, 1, "D", "");
		String tWhereSQL = "  = '"+mYesterDay+"'";
		String tFirstDate = (PubFun.calDate(PubFun.getCurrentDate(), -1, "M", "")).substring(0, 8)+"01";
		if("01".equals(tCurrentDate.substring(8,10)))
		{
			tWhereSQL = " between '"+tFirstDate+"' and '"+mYesterDay+"'";
		}
//		String tSelectSql = "SELECT * FROM ldperson ld WHERE modifydate "+tWhereSQL+" AND ld.idtype = '0' AND ( idtype IS NOT NULL and length(TRIM(idtype)) != 0 ) AND ( idno IS NOT NULL and  length(TRIM(idno)) != 0 ) AND NOT EXISTS ( SELECT 1 FROM iac_ldperson ild WHERE ild.idno = ld.idno AND ild.idtype = ld.idtype ) order by idtype,idno WITH ur";
		String tSelectSql = "select * from ldperson ld " 
				+" where (ld.idtype IS NOT NULL and ld.idtype<>'') AND (ld.idno IS NOT NULL and ld.idno <>'') and ld.idtype = '0'"
				+" and exists (select 1 from lcpol where appflag='1' and signdate "+tWhereSQL+" and (insuredno=ld.customerno or appntno=ld.customerno)) "
				+" and not exists (select 1 from iac_ldperson where idno = ld.idno and idtype = ld.idtype) "
				+" union "
				+" SELECT * FROM ldperson ld "
				+" WHERE ld.modifydate "+tWhereSQL+" " 
				+" AND ld.idtype = '0' "
				+" AND ( ld.idtype IS NOT NULL and ld.idtype <>'' ) "
				+" AND ( ld.idno IS NOT NULL and ld.idno <>'' ) "
				+" AND NOT EXISTS ( SELECT 1 FROM iac_ldperson ild WHERE ild.idno = ld.idno AND ild.idtype = ld.idtype ) "
				+" order by idtype,idno "
				+" WITH ur ";
		//String tSelectSql = "SELECT * FROM ldperson ld WHERE modifydate='2017-08-30' AND ld.idtype = '0' AND ( idtype IS NOT NULL and length(TRIM(idtype)) != 0 ) AND ( idno IS NOT NULL and  length(TRIM(idno)) != 0 ) AND NOT EXISTS ( SELECT 1 FROM iac_ldperson ild WHERE ild.idno = ld.idno AND ild.idtype = ld.idtype ) order by idtype,idno WITH ur";

		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLDPersonSet, tSelectSql);
		int tSumCount = 0;
		int tGroupNo = 1;
		String tPIdNo = "";
		String tPIdType = "";
		do{
			tRSWrapper.getData();
			int tCustomerCount = tLDPersonSet.size();
			tSumCount += tCustomerCount;
		    if (tCustomerCount > 0) 
		    {
				for (int i = 1;i<=tLDPersonSet.size();i++)
				{
					LDPersonSchema tLDPersonSchema=tLDPersonSet.get(i);
					String tIDNo=tLDPersonSchema.getIDNo();
					String tIDType=tLDPersonSchema.getIDType();
					String tCustomerNo = tLDPersonSchema.getCustomerNo();
					if(tIDNo.equals(tPIdNo)&&tIDType.equals(tPIdType))
					{
						tSumCount--;
						continue;
					}
					else if(!tIDNo.equals(tPIdNo)||!tIDType.equals(tPIdType))
					{
						tPIdType = tIDType;
						tPIdNo = tIDNo;
					}
					IAC_LDPERSONSchema nIAC_LDPERSONSchema = new IAC_LDPERSONSchema();
					nIAC_LDPERSONSchema.setBatchNo(tBatchNo);
					// tIAC_LDPERSONSchema.setBatchNo("20170526001");
					nIAC_LDPERSONSchema.setGroupNo(tGroupNo);
					nIAC_LDPERSONSchema.setIdType(tIDType);
					nIAC_LDPERSONSchema.setIdNo(tIDNo);
					nIAC_LDPERSONSchema.setState("0");
					nIAC_LDPERSONSchema.setCustomerNo(tCustomerNo);
					nIAC_LDPERSONSchema.setMakeDate(PubFun.getCurrentDate());
					nIAC_LDPERSONSchema.setMakeTime(PubFun.getCurrentTime());
					nIAC_LDPERSONSchema.setModifyDate(PubFun.getCurrentDate());
					nIAC_LDPERSONSchema.setModifyTime(PubFun.getCurrentTime());
					tOtherIAC_LDPERSONSet.add(nIAC_LDPERSONSchema);
					if(tOtherIAC_LDPERSONSet.size()==mMaxNumCount)
					{
						tInsertIAC_LDPERSONSet.add(tOtherIAC_LDPERSONSet);
						tGroupNo+=1;
						tOtherIAC_LDPERSONSet = new IAC_LDPERSONSet();
					}
				}
				mMMap.put(tInsertIAC_LDPERSONSet, "INSERT");
				mVData.add(mMMap);
				PubSubmit tPubSubmit = new PubSubmit();
				tPubSubmit.submitData(mVData, "");
				mVData.clear();
				mMMap = new MMap();
				tInsertIAC_LDPERSONSet = new IAC_LDPERSONSet();
			}
		}while(tLDPersonSet.size()>0);	
		String tInsertSql = "INSERT INTO iac_person_index "
				+ " ( BatchNo, UploadDate, ReportCount, CustomerCount, GroupCount, Reportstate, Operator, MakeDate, MakeTime, ModifyDate, ModifyTime ) "
				+ " VALUES " + " ('" + tBatchNo + "', '" + PubFun.calDate(PubFun.getCurrentDate(), -1, "D", "") + "', '0', '" + tSumCount + "', '"+tGroupNo+"', '0', '000', '"
				// + " VALUES " + " ('20170526001', '" + PubFun.calDate(PubFun.getCurrentDate(), -1, "D", "") + "', '0', '" + tCustomerCount + "', '0', '000', '"
				+ PubFun.getCurrentDate() + "', '"
				+ PubFun.getCurrentTime() + "', '"
				+ PubFun.getCurrentDate() + "', '"
				+ PubFun.getCurrentTime() + "')";
		mMMap.put(tOtherIAC_LDPERSONSet, "INSERT");
		mMMap.put(tInsertSql, "INSERT");
		mVData.add(mMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		tPubSubmit.submitData(mVData, "");
	}

	public static void main(String[] args) {
		BDYZExtractPersonTask tBDYZExtractPersonTask = new BDYZExtractPersonTask();
		tBDYZExtractPersonTask.run();
//		System.out.println(tBDYZExtractPersonTask.getBeforeFirstMonthdate());
//		System.out.println(PubFun.calDate(tBDYZExtractPersonTask.mYesterDay, 1, "D", ""));
//		System.out.println(tBDYZExtractPersonTask.mYesterDay);
//		System.out.println((PubFun.calDate(PubFun.getCurrentDate(), -1, "M", "")).substring(0, 8)+"01");
//		String tCurrentDate = PubFun.calDate(PubFun.getCurrentDate(), 1, "D", "");
//		System.out.println(tCurrentDate);
//		System.out.println(tCurrentDate.substring(8,10));
	}

}