package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.SubInsuAccSupplBala;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.operfee.ExpirBenefitAutoGetBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LGErrorLogSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 含有附加万能险种的保单满期金自动给付
 * @author lzy
 *
 */
public class ExpirBenefitAutoGetTask extends TaskThread {
	public CErrors mErrors = new CErrors();
	private GlobalInput tGI = new GlobalInput();
	private String managecom = "86";
	private String mContNo = null;
	private String whereParSql = "";
	private String mCurrDate = PubFun.getCurrentDate();
	
	public ExpirBenefitAutoGetTask(String tContno,GlobalInput pGI)
	{
		mContNo=tContno;
		tGI=pGI;
		managecom=pGI.ManageCom;
	}
	public ExpirBenefitAutoGetTask()
	{
		
	}
	
	private boolean getInputData()
	{
		if(mContNo!=null&&!mContNo.equals(""))
		{
			whereParSql=" and a.ContNo='"+mContNo+"'";
		}
		else
		{
			tGI.ComCode="86";
		    tGI.Operator="Server";
		    tGI.ManageCom="86";	
		}
	    return true;
	}
	
	public void run()
    {
		getInputData();
		dealData();
    }
	public boolean dealData()
	{
		System.out.println("附加万能自动追加保费开始@############################@");
		String str = "select trim(riskcode) from lmriskapp where risktype4='4' and subriskflag='S' and riskprop='I' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS uliSSRS = tExeSQL.execSQL(str);
		String uliSqlWhere ="";
		if(null != uliSSRS && uliSSRS.MaxRow>0){
			for(int i=1;i<=uliSSRS.getMaxRow(); i++){
				uliSqlWhere += "'"+uliSSRS.GetText(i, 1)+"'";
				if(i<uliSSRS.getMaxRow()){
					uliSqlWhere += ",";
				}
			}
		}else{
			System.out.println("附加万能险种信息查询失败。。。。");
			return false;
		}
		
		String querySql="select distinct a.contno,a.polno " 
					 + " from lcpol a,lcget b ,lmdutygetalive c "
					 + " where a.conttype ='1' and a.appflag ='1' "
					 + " and a.polno = b.polno "
					 + " and b.getdutycode = c.getdutycode "
					 + " and c.getdutykind in ('12','24','36') " //生存给付金
					 + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno) " 
					 + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate)) "
					 + " and ((b.getdutykind ='0' and (b.gettodate<=b.getenddate or b.getenddate is null)) or( b.getdutykind !='0' and"
                     + " ((b.gettodate<b.getenddate) or ((select nvl(max(Curgettodate),'2000-01-01') from ljsgetdraw where contno=b.contno and getdutycode=b.getdutycode and insuredno=b.insuredno)<=b.getenddate))))"
					 + " and exists (select 1 from lcpol pol where pol.contno=a.contno and pol.cvalidate<='"+ mCurrDate +"' "
					 + " and pol.appflag='1' and pol.stateflag='1' and pol.insuredno=a.insuredno  "
					 //换个处理方式，直接在本sql中写子查询效率太低
					 + " and pol.riskcode in ("+uliSqlWhere+",'340501','340601','340602') )"//必须是含有已生效的附加万能险
					 + " and b.gettodate <='" + mCurrDate + "' "
					 + " and b.managecom like '" + managecom + "%' "
					 + " and a.riskcode not in ('330501','530301','320106','120706') "//过滤常无忧B、少儿险
					 + whereParSql
					 + " with ur";
		
		System.out.println("自动满期查询sql："+querySql);
		SSRS tSSRS=tExeSQL.execSQL(querySql);
		if (tSSRS == null || tSSRS.getMaxRow() == 0) {
            CError.buildErr(this, "没有查到需要自动满期给付的保单");
            return false;
        }
		System.out.println("共需处理 "+tSSRS.getMaxRow()+"条保单");
		
		//生成满期给付批次号
        String serNo = PubFun1.CreateMaxNo("SERIALNO", "");
        int errCount=0;
		for(int i=1; i<=tSSRS.MaxRow; i++){
			LCContSchema tLCContSchema = null ;
			LCPolSchema tLCPolSchema = null ;
			try {
				tLCContSchema = queryCont(tSSRS.GetText(i, 1));
				if(null == tLCContSchema ){
					System.out.println("获取保单"+tSSRS.GetText(i, 1)+"数据失败");
					continue;
				}
				tLCPolSchema = queryPol(tSSRS.GetText(i, 2));
				if(null == tLCPolSchema ){
					System.out.println("获取险种"+tSSRS.GetText(i, 2)+"数据失败");
					continue;
				}
				String errInfo="自动满期给付处理失败，";
				VData tVData = new VData();
			    tVData.add(tLCContSchema);
			    tVData.add(tLCPolSchema);
			    tVData.add(tGI);
		        tVData.add(serNo);
			    
				ExpirBenefitAutoGetBL tExpirBenefitAutoGetBL=new ExpirBenefitAutoGetBL();
				if(!tExpirBenefitAutoGetBL.submitData(tVData, "")){
					errInfo +=tExpirBenefitAutoGetBL.mErrors.getFirstError();
					dealErrorLog(tLCPolSchema,errInfo);
					errCount ++;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
				errCount ++;
				continue;
			}
			
		}
		System.out.println("自动满期处理完毕，共计 "+tSSRS.getMaxRow()+" 单，处理失败 "+errCount+" 单");
		
		System.out.println("-------开始对附加万能账户补结");
		//调用账户结算
    	SubInsuAccSupplBala tSubInsuAccSupplBala = new SubInsuAccSupplBala();
    	VData cInputData = new VData();
    	cInputData.add(tGI);
        if(!tSubInsuAccSupplBala.submitData(cInputData, "")){
        	System.out.println("附加万能账户结算失败："+tSubInsuAccSupplBala.mErrors.getFirstError());
//        	dealErrorLog(mLCPolSchema,"附加万能账户补结失败："+tSubInsuAccSupplBala.mErrors.getFirstError());
        	return true;
        }
		
		return true;
		
	}
	
	private LCContSchema queryCont(String mContNo){
		String str="select * from LCCont where Contno='"+mContNo+"' ";
		LCContDB tLCContDB=new LCContDB();
		LCContSet tLCContSet=tLCContDB.executeQuery(str);
		if(tLCContSet.size() > 0 ){
			return tLCContSet.get(1).getSchema();
		}else{
			return null;
		}
	}
	
	private LCPolSchema queryPol(String mPolNo){
		String str="select * from LCPol where polno='"+mPolNo+"' ";
		LCPolDB tLCPolDB=new LCPolDB();
		LCPolSet tLCPolSet=tLCPolDB.executeQuery(str);
		if(tLCPolSet.size() > 0 ){
			return tLCPolSet.get(1).getSchema();
		}else{
			return null;
		}
	}
	
	/**
     * 加入到日志表数据,记录因为各种原因抽档失败的单子
     * @param
     * @return boolean
     */
 private boolean dealErrorLog(LCPolSchema tLCPolSchema, String Error) {
        LGErrorLogSchema tLGErrorLogSchema = new LGErrorLogSchema();
        String mserNo = PubFun.getCurrentDate2() + PubFun.getCurrentTime2();
        tLGErrorLogSchema.setSerialNo(mserNo);
        tLGErrorLogSchema.setErrorType("0003");//附加万能满期
        tLGErrorLogSchema.setErrorTypeSub("03");
        tLGErrorLogSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
        tLGErrorLogSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
        tLGErrorLogSchema.setContNo(tLCPolSchema.getContNo());
        tLGErrorLogSchema.setPolNo(tLCPolSchema.getPolNo());
        tLGErrorLogSchema.setOperator(tGI.Operator);
        tLGErrorLogSchema.setMakeDate(mCurrDate);
        tLGErrorLogSchema.setMakeTime(PubFun.getCurrentTime());
        tLGErrorLogSchema.setModifyDate(mCurrDate);
        tLGErrorLogSchema.setModifyTime(PubFun.getCurrentTime());
        tLGErrorLogSchema.setDescribe(Error);

        MMap tMap = new MMap();
        tMap.put(tLGErrorLogSchema, "DELETE&INSERT");
        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LGErrorLog 表失败!");
            return false;
        }
        return true;
 }
	
	
	 public static void main(String[] args)
	 {
		 String mContNo="013969959000001";
		 GlobalInput tGI=new GlobalInput();
		 tGI.Operator="Server";
		 tGI.ManageCom="86";
		 ExpirBenefitAutoGetTask t=new ExpirBenefitAutoGetTask(mContNo,tGI);
//		 ExpirBenefitAutoGetTask t=new ExpirBenefitAutoGetTask();
		 t.run();
	 }
}
