package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.llcase.LLSZExcelTransFtp;
import com.sinosoft.utility.CErrors;

/**
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description: FTP提数批处理
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2017
 * </p>
 * 
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @author Gao Bo
 * @version 1.0
 */
public class LLSZTransFtpTask extends TaskThread {
	/** 错误的容器 */
	public CErrors mErrors = new CErrors();

	String opr = "";

	public LLSZTransFtpTask() {
	}

	public void run() {
		LLSZExcelTransFtp tLLSZExcelTransFtp = new LLSZExcelTransFtp();
		if (!tLLSZExcelTransFtp.dealData()) {
			System.out.println("执行错误");
			System.out.println(mErrors.getErrContent());
			opr = "false";
			return;
		} else {
			System.out.println("执行成功");
			opr = "true";
		}

	}

	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

}
