package com.sinosoft.lis.taskservice;


import com.sinosoft.httpclientybk.T02.YbBankInfoUp;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *   外包案件信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class YbBankInfoUpTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    String opr = "";
    public YbBankInfoUpTask()
    {}

    public void run()
    {
    	YbBankInfoUp  ybkInfoUp = new YbBankInfoUp();
    	String SQL = " select prtno from lcpol where riskcode in(select code from ldcode where codetype='ybkriskcode') and appflag='1' "
      +" and exists(select 1 from LJTempFee a, LJTempFeeClass b where a.tempfeeno=b.tempfeeno  and b.paymode in ('4','11') and a.EnterAccDate is not null and a.otherno=lcpol.contno ) "
      +" and not exists(select 1 from  YBKErrorList where businessno=lcpol.prtno and resultstatus='1' and transtype='T02' ) "
//      " and prtno='YWX0000001075'"+ // lijia
      +" union "
      +" select lc.prtno from lcpol lc,lccontsub lcs where lc.riskcode in (select code from ldcode where codetype='ybkriskcode') and lc.prtno = lcs.prtno and lcs.ifautopay != '0'and lc.appflag = '1' "
      +" and exists(select 1 from LJTempFee a, LJTempFeeClass b where a.tempfeeno=b.tempfeeno  and b.paymode in ('4','11') and a.EnterAccDate is not null and a.otherno=lc.contno ) "
      +" and not exists (select 1 from YBK_T_BankPremInfoResult where otherno = lc.prtno and renewcount = char(lc.renewcount)and RequestType = 'T02') and lc.renewcount<>0 "
      +" and exists (select 1 from YBK_E03_LIS_ResponseInfo where edorno=lc.contno and prtno=lc.renewcount)"
      +" and exists (select 1 from lcrnewstatelog where prtno = lc.prtno and renewcount = lc.renewcount and state = '6') "
//     +" and lc.prtno='YWX0000001075'" // lijia
      +" with ur ";
    	
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(SQL);
    	
    	if(tSSRS!=null && tSSRS.MaxRow>0){
    		for ( int index = 1; index <=  tSSRS.getMaxRow(); index ++)
    		{
    			String tCaseNo = tSSRS.GetText(index, 1);
    			VData tVData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("PrtNo", tCaseNo);
    			tVData.add(tTransferData);
    	    	if(!ybkInfoUp.submitData(tVData, "YBKPT"))
    	         {
    	    		 System.out.println("银行卡缴费信息上传批处理出问题了");
    	             System.out.println(mErrors.getErrContent());
    	             opr ="false";
    	             continue;
    	         }else{
    	        	 System.out.println("银行卡缴费信息上传批处理成功了");
    	        	 opr ="ture";
    	         }
    		}
    	}

    }
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
   
    public static void main(String[] args){
    	YbBankInfoUpTask ybkTask = new YbBankInfoUpTask();
    	ybkTask.run();
    }
}




