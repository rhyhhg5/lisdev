package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.io.*;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import java.net.SocketException;

/**
 * <p>
 * Title:MixedAgentTask.java
 * </p>
 * 
 * <p>
 * Description:集团交叉销售批处理
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company:sinosoft
 * </p>
 * 
 * @author huxl
 * @version 1.0
 */
public class MixedGetAgentcom {
	public String mCurrentDate = PubFun.getCurrentDate();

	private FileWriter mFileWriter;

	private BufferedWriter mBufferedWriter;

	private String mURL = "";

	public MixedGetAgentcom() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void run(String agentcom) {
		System.out.println("开始提取" + this.mCurrentDate + "日的交叉销售数据,开始时间是"
				+ PubFun.getCurrentTime());
		if (!getMixedAgentcom(agentcom)) {
			return;
		}
		System.out.println("提取" + this.mCurrentDate + "日的交叉销售数据完成,完成时间是"
				+ PubFun.getCurrentTime());
	}

	private boolean getMixedAgentcom(String agentcom) {
		getS001(agentcom);

		return true;
	}

	/**
	 * 营销员信息表
	 * 
	 * @return
	 */
	private String getS001(String agentcom) {
		String strCont = "";
		String strContent = "";
		try {
			String tSQL = "select 'P','I','000002',agentcom,'S001', agentcom, "
					+ " '"+PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+"' 子公司报送时间,'' 时间戳 "
					+ "from lacom "
					+ "where agentcom = '"
					+ agentcom
					+ "' with ur";

			SSRS tSSRS = new ExeSQL().execSQL(tSQL);
			if (tSSRS.getMaxRow() <= 0) {
				return null;
			}
			String tempString = tSSRS.encode();
			String[] tempStringArr = tempString.split("\\^");
			System.out.println(tempStringArr);

			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				String t = tempStringArr[i].replaceAll("\\|", "\\-");
				t = t.substring(0, t.length() - 1);
				strCont = t;
			}
			
			String mSQL =  "select agentcom 中介机构ID,name 中介机构名称," 
				    +" '000085' 子公司代码," 
				    +" '中国人民健康保险股份有限公司' 子公司名称," 
				    +" '310' 企业性质,ts_fmt(date(ChiefBusiness),'yyyymmdd') 成为中介日期,managecom 管理机构代码,(select name from ldcom where comcode=lacom.managecom ) 管理机构名称," 
				    +" LicenseNo 企业注册证号,case when sellflag='Y' then '1' else  '9' end 状态," 
				    +" Corporation  联系人姓名,'' 联系人性别,'' 联系人电话,'' 联系人手机,'' 联系人电子邮件,'I' 数据变更类型," 
				    +" ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 子公司报送时间,'' 时间戳 " 
				    +" from lacom where  agentcom = '"+agentcom+"' with ur";

		SSRS mSSRS = new ExeSQL().execSQL(mSQL);
		if (mSSRS.getMaxRow() <= 0) {
			return null;
		}
		String tempStringCont = mSSRS.encode();
		String[] tempStringContArr = tempStringCont.split("\\^");
		System.out.println(tempStringContArr);

		for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
			String m = tempStringContArr[i].replaceAll("\\|", "','");
			String[] mArr = m.split("\\,");
			mArr[3] = mArr[3].substring(1, mArr[3].length() - 1);
			System.out.println(tempStringContArr[i]);
			strContent += tempStringContArr[i];
		}
		} catch (Exception ex2) {
			System.out.println(ex2.getStackTrace());
		}
		String strManagecomCont = strCont + ":" + strContent;
		System.out.println(strManagecomCont);
		return strManagecomCont;
	}

	public static void main(String args[]) {
		MixedGetAgentcom tMixedAgentTask = new MixedGetAgentcom();
		tMixedAgentTask.run("PC1100000008");
		return;
	}

	private void jbInit() throws Exception {
	}

}
