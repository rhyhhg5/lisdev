package com.sinosoft.lis.taskservice;

import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class WebServerStop implements Runnable {
    public WebServerStop() {
    }

    public void run() {
        try {
            if (!this.checkServer2()) {
                ServerSocket server = new ServerSocket(8765);
                System.out.println("Server:8765:started");
                Socket client = server.accept();
                System.out.println("Server:8765:accepted");
                client.close();
                server.close();
                System.out.println("Server:8765:stoped");
            }
        } catch (Exception ex) {
        }
    }

    public boolean checkServer() {
        try {
            Socket server1 = new Socket(InetAddress.getLocalHost(), 5678);
            server1.close();
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public boolean checkServer2() {
        try {
            Socket server1 = new Socket(InetAddress.getLocalHost(), 8765);
            server1.close();
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        WebServerStop tWebServerStop = new WebServerStop();
        Thread t1 = new Thread(tWebServerStop);
        t1.start();
    }
}
