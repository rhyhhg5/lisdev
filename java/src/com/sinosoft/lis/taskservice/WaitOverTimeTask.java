package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 工单待办超时处理, 转为待办的工单超过待办期限的,需要自动转为原状态
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class WaitOverTimeTask extends TaskThread
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();

    // 输入数据的容器
    private VData mInputData = new VData();

    // 统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

    private GlobalInput mGlobalInput = null;

    private MMap map = new MMap();

    public WaitOverTimeTask()
    {
    }

    /**
     * 实现TaskThread的借口方法run
     * 由TaskThread调用
     * */
    public void run()
    {
        if(!unWait())
        {
            return;
        }
    }

    /**
     * 支持手动执行待办件超时日处理程序
     * 只能操纵本机构及本机构的下级机构的待办件
     * */
    public void oneCompany(GlobalInput g)
    {
        mGlobalInput = g;

        run();
    }

    //工单取消待办操作
    private boolean unWait()
    {
        SSRS tSSRS = getOverTimeTask();
        int count = tSSRS.getMaxRow();

        if (tSSRS != null && count > 0)
        {
            for (int i = 1; i <= count; i++)
            {
                String sql = "update LGWork set "
                             + "statusNo = pauseNo, " //待办天数pauseNo功能已取消，在转为待办时pauseNo暂存原工单状态
                             + "modifyDate = '" + mCurrentDate + "', "
                             + "modifyTime = '" + mCurrentTime + "' "
                             + "where  workNo = '" + tSSRS.GetText(i, 1) + "'  ";
                map.put(sql, "UPDATE");
            }
        }

        if(map.size() > 0)
        {
            mInputData.add(map);

            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN"))
            {
                mErrors.copyAllErrors(tPubSubmit.mErrors);
                mErrors.addOneError("日处理取消待办时出错");
                System.out.println("日处理取消待办时出错");

                return false;
            }

            return true;
        }

        return true;
    }

    //获得超时的工单号
    //返回值：SSRS， 存储了工单号
    private SSRS getOverTimeTask()
    {
        SSRS tSSRS;
        ExeSQL tExeSQL = new ExeSQL();

        String sql;
        if (mGlobalInput == null)
        {
            sql = "Select WorkNo "
                  + "from LGWork "
                  + "Where StatusNo = '0' " //定期代办
                  + "  and days(wakeDate) < days('" + mCurrentDate + "') "
                  + "     or (days(wakeDate) = days('" + mCurrentDate + "') "
                  + "         and wakeTime < current time) "; //超过期限
        }
        else
        {
            //只能操纵本机构及本机构的下级机构的待办件
            sql = "Select workNo "
                  + "from LGWork a, LDUser b "
                  + "where StatusNo = '0' " //定期代办
                  + "  and (days(wakeDate) < days('" + mCurrentDate + "') "
                  + "    or (days(wakeDate) = days('" + mCurrentDate + "'))) "
                  + "        and (wakeTime < current time) " //超过期限
                  + "  and a.operator = b.userCode "
                  + "  and b.comCode like '" + mGlobalInput.ComCode + "%'";

        }
        tSSRS = tExeSQL.execSQL(sql);

        return tSSRS;
    }

    public static void main(String args[])
    {
        WaitOverTimeTask w = new WaitOverTimeTask();
        GlobalInput g = new GlobalInput();
        g.ComCode = "86";
        g.Operator = "endor5";

        //w.oneCompany(g);
        w.run();
        if(w.mErrors.needDealError())
        {
            System.out.println(w.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}



