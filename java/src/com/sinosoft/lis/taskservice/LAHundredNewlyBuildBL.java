package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LAHundredNewlyBuildBL {
	
	/**错误处理类*/
	public CErrors mErrors = new CErrors();
	/**传输数据的容器*/
	private VData mInputData = new VData();
	private String mOperate;
	private String mStartDate;
	private String mEndDate;
	private String mAgentGroup;
	private String mManageCom;
	private GlobalInput mGlobalInput = new GlobalInput();
	
	
	
	public LAHundredNewlyBuildBL(){
		
	}
	
	public boolean getInputData(VData tVData){
		
		mGlobalInput.setSchema((GlobalInput) tVData.getObjectByObjectName("GlobalInput", 0)); 
		this.mStartDate=(String) tVData.getObject(1);
		this.mEndDate =(String) tVData.getObject(2);
		this.mAgentGroup =(String) tVData.getObject(3);
		this.mManageCom= (String) tVData.getObject(4);
		System.out.println(mStartDate+","+mEndDate+","+mAgentGroup+","+mManageCom);
		
		return true;
	}
	
	public boolean checkData(){
		//先判断mEndDate是否满足考核计算时间
		ExeSQL tExeSQL = new ExeSQL();
		String tsql = "select date('" + mStartDate + "')+3 month from dual";
		String tYearMonth = tExeSQL.getOneValue(tsql);
		if(tYearMonth.compareTo(mEndDate)<0){
			return false;
		}
		//判断业务员的考核日期若计算日期小于薪资审核发放日期不进行计算
		String tVerifyDate = PubFun.calDate(tYearMonth,-1,"M",null);
		tVerifyDate = AgentPubFun.formatDate(tYearMonth, "yyyyMM");
		LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
		LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
		String tWageSql = " select *  from lawagehistory where state ='14' and managecom ='"+mManageCom+"' and " +
				" branchtype ='1' and branchtype2='01' and aclass ='03' and wageno ='"+tVerifyDate+"'  ";
		tLAWageHistorySet = tLAWageHistoryDB.executeQuery(tWageSql);
		if(tLAWageHistorySet.size()<= 0){
			return false;
		}
		String tUpDate = tLAWageHistorySet.get(1).getModifyDate();
		if(mEndDate.compareTo(tUpDate)<0){
			return false;
		}
		
		return true;
	}
	
	public boolean dealData(){
		return true;
	}
	
	public boolean prepareOutputData(){
		return true;
	}
	
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
      	  return false;
        }
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HuiFangBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败HuiFangBL!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, "")) {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "HuiFangBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
      mInputData = null;
//      if(!returnLogState())
//  	{
//  		return false;
//  	}
      
        return true;
  }
     public static void main(String[] args) {
    	 String tYearMonth = "2017-08-01" ;
    	 String tVerifyDate = PubFun.calDate(tYearMonth,-1,"M",null);
    	 System.out.println(tVerifyDate);
    	 System.out.println("2017-12-30".compareTo("2017-12-30"));
    	 
	} 
}
