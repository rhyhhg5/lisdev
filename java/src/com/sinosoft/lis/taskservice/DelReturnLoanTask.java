package com.sinosoft.lis.taskservice;

import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.task.*;

/**
 * <p>
 * Title: 还款保全自动撤销批处理程序
 * </p>
 * <p>
 * Description: 每天由系统定时调用，7天后撤销还款还未收费的保全项目
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author QiuYang
 * @version 1.0
 */

public class DelReturnLoanTask extends TaskThread {
	CErrors mErrors = new CErrors();

	MMap mMap = new MMap();

	GlobalInput mGlobalInput = new GlobalInput();

	/** 当前操作人员的机构 */
	String manageCom = null;

	public DelReturnLoanTask() {
		mGlobalInput.ManageCom="86";
		mGlobalInput.Operator="001";
	}

	/**
	 * 当前机构
	 * 
	 * @param manageCom
	 *            String
	 */
	public DelReturnLoanTask(String manageCom) {
		this.manageCom = manageCom;
		mGlobalInput.ManageCom="86";
		mGlobalInput.Operator="001";
	}

	/**
	 * 自动结案
	 */
	public void run() {
		System.out.println("保全自动结案运行开始！");
		List taskList = getTaskList();
		for (int i = 0; i < taskList.size(); i++) {
			mMap = new MMap();
			delete((String) taskList.get(i));
		}
		System.out.println("保全自动结案运行结束！");
	}

	/**
	 * 得到待结案的任务列表
	 * 
	 * @return List
	 */
	private List getTaskList() {
		List taskList = new ArrayList();
		String sql = " select b.EdorAcceptNo from LPReturnLoan a, LPEdorApp b, lpedoritem c " + " where  b.EdorAcceptNo = c.edorno and a.edorno=c.edorno "
				+ " and c.edortype='RF' and b.EdorState in ('2', '9') " + " and not exists (select 1 from LJTempFee where otherno=b.EdorAcceptNo and EnterAccDate is not null) "
				+ " and (a.payoffdate + 7 days) <= Current Date with ur";
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		System.out.println(tSSRS.getMaxRow());
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			taskList.add(tSSRS.GetText(i, 1));
		}
		return taskList;
	}

	/**
	 * 保全撤销
	 * 
	 * @param edorAcceptNo
	 *            String
	 * @return boolean
	 */
	private boolean delete(String edorAcceptNo) {
		System.out.println("撤销工单开始:"+edorAcceptNo);

		if(!check(edorAcceptNo)){
			return false;
		}
		if(!getsubmit(edorAcceptNo)){
			return false;
		}
		if (!submit()) {
			return false;
		}
		System.out.println("撤销工单结束:"+edorAcceptNo);
		return true;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @param map
	 *            MMap
	 * @return boolean
	 */
	private boolean submit() {
		VData data = new VData();
		data.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}
	
	/**
	 * 校验此保全能否撤销
	 * 
	 * @param map
	 *            MMap
	 * @return boolean
	 */
	private boolean check(String edorno) {
		String flag=new ExeSQL().getOneValue("select 1 from ljspay where bankonthewayflag='1' and otherno='"+edorno+"' and othernotype='10'");
		if(flag.equals("1")){
			// @@错误处理
			System.out.println("存在正在发盘的应收,无法撤销该保全:"+edorno);
			CError tError = new CError();
			tError.moduleName = "DelReturnLoanTask";
			tError.functionName = "check";
			tError.errorMessage = "存在正在发盘的应收,无法撤销该保全:"+edorno;
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	/**
	 * 获取提交数据
	 * 
	 * @param map
	 *            MMap
	 * @return boolean
	 */
	private boolean getsubmit(String edorno) {
		LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
		LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
		tLPEdorAppDB.setEdorAcceptNo(edorno);
		if(!tLPEdorAppDB.getInfo()){
			// @@错误处理
			System.out.println("DelReturnLoanTask+getsubmit++--");
			CError tError = new CError();
			tError.moduleName = "DelReturnLoanTask";
			tError.functionName = "getsubmit";
			tError.errorMessage = "获取保全表失败!";
			mErrors.addOneError(tError);
			return false;
		}
		System.out.println("");
		tLPEdorAppSchema=tLPEdorAppDB.getSchema();
		
		LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
		LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
		tLPEdorMainDB.setEdorAcceptNo(edorno);
		tLPEdorMainDB.setEdorNo(edorno);
		if(tLPEdorMainDB.query().size()!=1){
			// @@错误处理
			System.out.println("DelReturnLoanTask+getsubmit++--");
			CError tError = new CError();
			tError.moduleName = "DelReturnLoanTask";
			tError.functionName = "getsubmit";
			tError.errorMessage = "获取保全明细表失败!";
			mErrors.addOneError(tError);
			return false;
		}
		tLPEdorMainSchema=tLPEdorMainDB.query().get(1);
		
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("DelReason", "保单还款超期未缴费撤销!");
		tTransferData.setNameAndValue("ReasonCode", "081");
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.addElement(mGlobalInput);
		tVData.addElement(tLPEdorMainSchema);
		tVData.addElement(tLPEdorAppSchema);
		tVData.addElement(tTransferData);
		// 撤销项目
		PEdorMainCancelBL tPEdorMainCancelBL = new PEdorMainCancelBL();
		if (!tPEdorMainCancelBL.submitData(tVData, "EDORMAIN")) {
			this.mErrors.copyAllErrors(tPEdorMainCancelBL.mErrors);
			return false;
		} else {
			mMap.add(tPEdorMainCancelBL.getMap());
		}
		System.out.println("mEdorNov:" + edorno);
		// 重复理算
		PEdorReCalBL tPEdorReCalBL = new PEdorReCalBL(edorno);
		MMap map = tPEdorReCalBL.getSubmitData();
		if (map == null) {
			mErrors.addOneError("重复理算失败！");
			return false;
		}
		mMap.add(map);
		// 催缴工单结案
		String reason = "待催缴工单" + edorno + "撤销。";
		FinishPhoneHastenBL tFinishPhoneHastenBL = new FinishPhoneHastenBL(mGlobalInput, edorno, reason);
		MMap phoneMap = tFinishPhoneHastenBL.getSubmitData();
		if (map == null) {
			mErrors.copyAllErrors(tFinishPhoneHastenBL.mErrors);
			return false;
		}
		mMap.add(phoneMap);

		// 撤销工单
		PEdorAppCancelBL tPEdorAppCancelBL = new PEdorAppCancelBL();
		if (!tPEdorAppCancelBL.submitData(tVData, "EDORAPP")) {
			this.mErrors.copyAllErrors(tPEdorAppCancelBL.mErrors);
			return false;
		} else {
			mMap.add(tPEdorAppCancelBL.getMap());
		}
		return true;
	}

	/**
	 * 主函数，测试用
	 * 
	 * @param arg
	 *            String[]
	 */
	public static void main(String arg[]) {

		DelReturnLoanTask tBqFinishTask = new DelReturnLoanTask();
		System.out.println("保全自动结案运行开始！");
		List taskList = tBqFinishTask.getTaskList();
		for (int i = 0; i < taskList.size(); i++) {
			tBqFinishTask.mMap = new MMap();
			tBqFinishTask.delete((String) taskList.get(i));
		}
		System.out.println("保全自动结案运行结束！");
		// tBqFinishTask.run();
//		tBqFinishTask.delete("20101202000037");
//		tBqFinishTask.getsubmit("20111122000003");
//		tBqFinishTask.submit();
	}
}
