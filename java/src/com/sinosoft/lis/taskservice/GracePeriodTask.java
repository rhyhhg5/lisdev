package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.PolInGracePeriodBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: </p>
 * @author 张彦梅 2008-6-20
 * @version 1.0
 * 万能险个险进入宽限期批处理
 */

public class GracePeriodTask extends TaskThread
{
    //构造函数
    public GracePeriodTask()
    {
    }

    //从TaskThread中继承的方法，必须实现
    public void run()
    {
        GlobalInput mG = new GlobalInput();

        VData mVData = new VData();
        mG.Operator = "task";
        mG.ManageCom = "86";
        mVData.add(mG);

        PolInGracePeriodBL tPolInGracePeriodBL = new PolInGracePeriodBL();
        tPolInGracePeriodBL.submitData(mVData, "GracePeriod");
    }
}
