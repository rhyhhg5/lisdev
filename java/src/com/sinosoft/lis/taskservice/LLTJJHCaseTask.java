package com.sinosoft.lis.taskservice;


import com.sinosoft.lis.yibaotong.LLTJJHCaseTransFtpXml;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *天津外包分案信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class LLTJJHCaseTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();


    String opr = "";
    public LLTJJHCaseTask()
    {}

    public void run()
    {
    	LLTJJHCaseTransFtpXml  tLLTJJHCaseTransFtpXml = new LLTJJHCaseTransFtpXml();
    	
    	if(!tLLTJJHCaseTransFtpXml.submitData())
         {
    		 System.out.println("批次信息导入出问题了");
             System.out.println(mErrors.getErrContent());
             opr ="false";
             return;
         }else{
        	 System.out.println("批次信息导入成功了");
        	 opr ="true";
         }

    }

   

   
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	public static void main(String[] args)
    {

        LLTJJHCaseTask tLaHumandeveITask = new LLTJJHCaseTask();
        tLaHumandeveITask.run();
//        tGEdorMJTask.oneCompany(tGlobalInput);
    }
}




