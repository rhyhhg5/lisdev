package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LCInsureAccBalanceDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJSGetDrawDB;
import com.sinosoft.lis.db.LOBonusPolDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.AnalyzeMSG;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MsgSendBase;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.vschema.LCInsureAccBalanceSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSGetDrawSet;
import com.sinosoft.lis.vschema.LOBonusPolSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: 个险万能险保单账户结算 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @version 1.0
 * @CreateDate：2007
 */
public class GetMoneyMSGTask extends TaskThread
{
    public CErrors mErrors = new CErrors();

    private String mContNo = null;

    private GlobalInput mGI = new GlobalInput();

    public GetMoneyMSGTask()
    {
        mGI.Operator = "001";
        mGI.ComCode = "86";
        mGI.ManageCom = mGI.ComCode;
    }

    public GetMoneyMSGTask(GlobalInput cGlobalInput)
    {
        mGI = cGlobalInput;
    }
    
    public void runOneContno(String contno){
    	mContNo = contno;
    	run();
    }

    public void run()
    {

        //个单保全的应付信息短信通知
        	sendBQGetMsg();
        	
        //个单保全的批改信息短信通知
        	sendBQMsg();
        
        //个单满期给付信息短信通知
        	sendEBMsg();
        
        //保单第十二个月月结完成后短信通知
        	sendUliMsg();
        
        //保单分红后的短信通知
        	sendBonusMsg();
        
    }
    
    private void sendBQGetMsg(){
    	//查询sql
    	String BQGetSQL = " select * from ljaget where othernotype='10' " +
    					  (mContNo != null ? (" and otherno = '" + mContNo + "' ") : "") +
    			     	  " and exists (select 1 from lpedorapp where ljaget.otherno=edoracceptno and confdate=current date and edorstate='0') " +
    			     	  " and not exists (select 1 from lgwork where  workno=ljaget.otherno and applytypeno='3') " +
    			     	  " and makedate=current date " +
    			     	  " and managecom like '8644%' with ur  ";
    	LJAGetDB tLJAGetDB = new LJAGetDB();
    	LJAGetSet tLJAGetSet = new LJAGetSet();
    	tLJAGetSet = tLJAGetDB.executeQuery(BQGetSQL);
    	
    	//对每个实收单独发送短信
    	for(int i=1;i<=tLJAGetSet.size();i++){
    		//查询短信发送基础信息
    		String queryAppntInfo = "select a.contno, "   //保单号
//        	+" (select mobile from lcaddress x, lcappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
    		+" mobile(a.contno), "	
        	+" a.appntname,a.managecom, "       //被保人姓名
			+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end), "
			+" (select edorname from lmedoritem where edorcode in (select edortype from lpedoritem where edoracceptno='"+tLJAGetSet.get(i).getOtherNo()+"') fetch first 1 rows only), "
			+" (select codename from ldcode where codetype='applytypeno' and code=(select applytypeno from lgwork where  workno='"+tLJAGetSet.get(i).getOtherNo()+"' )), "
			+" (select acceptdate from lgwork where  workno='"+tLJAGetSet.get(i).getOtherNo()+"' ) "
        	+" from lccont a "
            +" where a.contno = (select contno from lpedoritem where edoracceptno='"+tLJAGetSet.get(i).getOtherNo()+"' fetch first 1 rows only) " 
            +" and not exists (select 1 from LCCSpec  where contno=('BQ001'||a.contno) and EndorsementNo ='"+tLJAGetSet.get(i).getOtherNo()+"' )  "
            +" union "
            +"select a.contno, "   //保单号
//        	+" (select mobile  from lcaddress x, lbappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
            +" mobile(a.contno), "
        	+" a.appntname,a.managecom, "       //被保人姓名
			+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end), "
			+" (select edorname from lmedoritem where edorcode in (select edortype from lpedoritem where edoracceptno='"+tLJAGetSet.get(i).getOtherNo()+"') fetch first 1 rows only), "
			+" (select codename from ldcode where codetype='applytypeno' and code=(select applytypeno from lgwork where  workno='"+tLJAGetSet.get(i).getOtherNo()+"' )), "
			+" (select acceptdate from lgwork where  workno='"+tLJAGetSet.get(i).getOtherNo()+"' ) "
			+" from lbcont a "
            +" where a.contno = (select contno from lpedoritem where edoracceptno='"+tLJAGetSet.get(i).getOtherNo()+"' fetch first 1 rows only) " 
            +" and not exists (select 1 from LCCSpec  where contno=('BQ001'||a.contno) and EndorsementNo ='"+tLJAGetSet.get(i).getOtherNo()+"' )  "            
            +" with ur";
    		
    		ExeSQL tExeSQL = new ExeSQL();
    		SSRS tMsgSuccSSRS =tExeSQL.execSQL(queryAppntInfo);
    		if(tMsgSuccSSRS!=null&&tMsgSuccSSRS.getMaxRow()>0){
    			MsgSendBase tMsgSendBase = new MsgSendBase();
    			tMsgSendBase.setContno(tMsgSuccSSRS.GetText(1, 1));
    			tMsgSendBase.setAppntName(tMsgSuccSSRS.GetText(1, 3));
    			tMsgSendBase.setAppntSex(tMsgSuccSSRS.GetText(1, 5));
    			tMsgSendBase.setPrem(tLJAGetSet.get(i).getSumGetMoney()+"");
    			tMsgSendBase.setApplyDate(tMsgSuccSSRS.GetText(1, 8));
    			tMsgSendBase.setEdorName(tMsgSuccSSRS.GetText(1, 6));
    			tMsgSendBase.setApplyNoType(tMsgSuccSSRS.GetText(1, 7));
    			tMsgSendBase.setEndorsementNo(tLJAGetSet.get(i).getOtherNo());
    			
    			if(tMsgSuccSSRS.GetText(1, 2)!=null&&!"".equals(tMsgSuccSSRS.GetText(1, 2))){
    				tMsgSendBase.setMobilPhone(tMsgSuccSSRS.GetText(1, 2));
    			}else{
    				System.out.println("客户电话号码为空，无法发送短信");
    				continue;
    			}
//    			tMsgSendBase.setMobilPhone("18610913667");
    			tMsgSendBase.setOperator("001");
    			tMsgSendBase.setManageCom(tMsgSuccSSRS.GetText(1, 4));
    			AnalyzeMSG tAnalyzeMSG = new AnalyzeMSG();
    			tAnalyzeMSG.setmMsgSendBase(tMsgSendBase);
    			tAnalyzeMSG.applySend("BQ001");
    		}else{
    			System.out.println("工单号"+tLJAGetSet.get(i).getOtherNo()+"查询客户信息失败，无法发送短信！！");
    		}
    		
    	}
    }
    
    
    public void sendBQMsg(){
    	//查询sql
    	String BQSQL = " select * from lpedoritem where exists (  " +
    			" select 1 from lpedorapp where edoracceptno=lpedoritem.edoracceptno and confdate=current date and edorstate='0' " +
    			" and othernotype='1') " +
    			(mContNo != null ? (" and edoracceptno = '" + mContNo + "' ") : "") +
    			" and not exists (select 1 from lgwork where  workno=lpedoritem.edoracceptno and applytypeno='3') " +
    			" and getmoney>=0" +
    			" and not exists(select 1 from ljaget where otherno=lpedoritem.edoracceptno)" +
    			" and managecom like '8644%' and edortype!='TF' "  +
    			" with ur ";
    	LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
    	LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
    	tLPEdorItemSet = tLPEdorItemDB.executeQuery(BQSQL);
    	//对每个个单批改类发送短信
    	for(int i=1;i<=tLPEdorItemSet.size();i++){
    		//查询短信发送基础信息
    		String queryAppntInfo = "select a.contno, "   //保单号
//        	+" (select mobile from lcaddress x, lcappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
    		+" mobile(a.contno), "	
        	+" a.appntname,a.managecom, "       //被保人姓名
			+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end) ,"
			+" (select edorname from lmedoritem where edorcode='"+tLPEdorItemSet.get(i).getEdorType()+"' fetch first 1 rows only), "
			+" (select confdate from lpedorapp where edoracceptno='"+tLPEdorItemSet.get(i).getEdorAcceptNo()+"') "
        	+" from lccont a "
            +" where a.contno = (select contno from lpedoritem where edoracceptno='"+tLPEdorItemSet.get(i).getEdorAcceptNo()+"' fetch first 1 rows only) " 
            +" and not exists (select 1 from LCCSpec  where contno=('BQ004'||a.contno) and EndorsementNo ='"+tLPEdorItemSet.get(i).getEdorAcceptNo()+"' )  "
            +" union "
            +"select a.contno, "   //保单号
//        	+" (select  mobile from lcaddress x, lbappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
            +" mobile(a.contno), "
        	+" a.appntname,a.managecom, "       //被保人姓名
			+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end), "
			+" (select edorname from lmedoritem where edorcode='"+tLPEdorItemSet.get(i).getEdorType()+"' fetch first 1 rows only), "
			+" (select confdate from lpedorapp where edoracceptno='"+tLPEdorItemSet.get(i).getEdorAcceptNo()+"') "
        	+" from lbcont a "
            +" where a.contno = (select contno from lpedoritem where edoracceptno='"+tLPEdorItemSet.get(i).getEdorAcceptNo()+"' fetch first 1 rows only) " 
            +" and not exists (select 1 from LCCSpec  where contno=('BQ004'||a.contno) and EndorsementNo ='"+tLPEdorItemSet.get(i).getEdorAcceptNo()+"' )  "            
            +" with ur";
    		ExeSQL tExeSQL = new ExeSQL();
    		SSRS tMsgSuccSSRS =tExeSQL.execSQL(queryAppntInfo);
    		if(tMsgSuccSSRS!=null&&tMsgSuccSSRS.getMaxRow()>0){
    			MsgSendBase tMsgSendBase = new MsgSendBase();
    			tMsgSendBase.setContno(tMsgSuccSSRS.GetText(1, 1));
    			tMsgSendBase.setAppntName(tMsgSuccSSRS.GetText(1, 3));
    			tMsgSendBase.setApplyDate(tMsgSuccSSRS.GetText(1, 7));
    			tMsgSendBase.setEdorName(tMsgSuccSSRS.GetText(1, 6));
    			tMsgSendBase.setAppntSex(tMsgSuccSSRS.GetText(1, 5));
    			tMsgSendBase.setPrem(tLPEdorItemSet.get(i).getGetMoney()+"");
    			tMsgSendBase.setEndorsementNo(tLPEdorItemSet.get(i).getEdorAcceptNo());
    			
    			if(tMsgSuccSSRS.GetText(1, 2)!=null&&!"".equals(tMsgSuccSSRS.GetText(1, 2))){
    				tMsgSendBase.setMobilPhone(tMsgSuccSSRS.GetText(1, 2));
    			}else{
    				System.out.println("客户电话号码为空，无法发送短信");
    				continue;
    			}
    			
//    			tMsgSendBase.setMobilPhone("18610913667");
    			tMsgSendBase.setOperator("001");
    			tMsgSendBase.setManageCom(tMsgSuccSSRS.GetText(1, 4));
    			AnalyzeMSG tAnalyzeMSG = new AnalyzeMSG();
    			tAnalyzeMSG.setmMsgSendBase(tMsgSendBase);
    			tAnalyzeMSG.applySend("BQ004");
    		}else{
    			System.out.println("工单号"+tLPEdorItemSet.get(i).getEdorAcceptNo()+"查询客户信息失败，无法发送短信！！");
    		}
    		
    	}
    }
    
    public void sendEBMsg(){
    	//查询sql
    	String EBGetSQL = " select * from ljaget where OtherNoType='20' " +
    			     	  "	and exists (select 1 from ljsgetdraw where getnoticeno=ljaget.actugetno and FeeOperationType='EB' and handler!='1' " +
    			     	  (mContNo != null ? (" and contno = '" + mContNo + "' ") : "") +
    			     	  " ) " +
    			     	  " and makedate=current date " +
    			     	  " and managecom like '8644%' with ur " ;
    	LJAGetDB tLJAGetDB = new LJAGetDB();
    	LJAGetSet tLJAGetSet = new LJAGetSet();
    	tLJAGetSet = tLJAGetDB.executeQuery(EBGetSQL);
    	for(int i=1;i<=tLJAGetSet.size();i++){
    		String queryAppntInfo = "";
    		//查询给付对象
    		LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
    		LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();
    		tLJSGetDrawSet = tLJSGetDrawDB.executeQuery("select * from ljsgetdraw where getnoticeno='"+tLJAGetSet.get(i).getActuGetNo()+"'");
    		if(tLJSGetDrawSet!=null&&tLJSGetDrawSet.size()>0){
    			LJSGetDrawSchema tLJSGetDrawSchema = tLJSGetDrawSet.get(1).getSchema();
    			if(tLJSGetDrawSchema.getHandler()!=null&&"0".equals(tLJSGetDrawSchema.getHandler())){//发送给投保人
    				//查询短信发送基础信息
    				queryAppntInfo = "select a.contno, "   //保单号
//    					+" (select mobile from lcaddress x, lcappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
    					+" mobile(a.contno), "
    					+" a.appntname,a.managecom, "       //被保人姓名
    					+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end) "
    					+" from lccont a "
    					+" where a.contno = '"+tLJSGetDrawSchema.getContNo()+"'  " 
    					+" and not exists (select 1 from LCCSpec  where contno=('BQ002'||a.contno) and EndorsementNo ='"+tLJAGetSet.get(i).getActuGetNo()+"'  )  "
    					+" union "
    					+"select a.contno, "   //保单号
//    					+" (select  mobile from lcaddress x, lbappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
    					+" mobile(a.contno), "
    					+" a.appntname,a.managecom, "       //被保人姓名
    					+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end) "
    					+" from lbcont a "
    					+" where a.contno = '"+tLJSGetDrawSchema.getContNo()+"'  " 
    					+" and not exists (select 1 from LCCSpec  where contno=('BQ002'||a.contno) and EndorsementNo ='"+tLJAGetSet.get(i).getActuGetNo()+"'  )  "            
    					+" with ur";
    			}else if(tLJSGetDrawSchema.getHandler()!=null&&"2".equals(tLJSGetDrawSchema.getHandler())){//发送给被保险人
    				//查询短信发送基础信息
    				queryAppntInfo = "select a.contno, "   //保单号
//    					+" (select mobile from lcaddress x, lcinsured y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
    					+" mobile(a.contno), "
    					+" a.appntname,a.managecom, "       //被保人姓名
    					+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end) "
    					+" from lccont a "
    					+" where a.contno = '"+tLJSGetDrawSchema.getContNo()+"'  " 
    					+" and not exists (select 1 from LCCSpec  where contno=('BQ002'||a.contno)  and EndorsementNo ='"+tLJAGetSet.get(i).getActuGetNo()+"' )  "
    					+" union "
    					+"select a.contno, "   //保单号
//    					+" (select mobile from lcaddress x, lbinsured y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
    					+" mobile(a.contno), "
    					+" a.appntname,a.managecom, "       //被保人姓名
    					+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end) "
    					+" from lbcont a "
    					+" where a.contno = '"+tLJSGetDrawSchema.getContNo()+"'  " 
    					+" and not exists (select 1 from LCCSpec  where contno=('BQ002'||a.contno)  and EndorsementNo ='"+tLJAGetSet.get(i).getActuGetNo()+"' )  "            
    					+" with ur";
    			}else{
    				System.out.println("其他申请人暂时不支持发送短信");
    				continue;
    			}
    		}else{
    			System.out.println("查询满期抽挡表失败，无法发送短信！");
    			continue;
    		}
    		
    		ExeSQL tExeSQL = new ExeSQL();
    		SSRS tMsgSuccSSRS =tExeSQL.execSQL(queryAppntInfo);
    		if(tMsgSuccSSRS!=null&&tMsgSuccSSRS.getMaxRow()>0){
    			MsgSendBase tMsgSendBase = new MsgSendBase();
    			tMsgSendBase.setContno(tMsgSuccSSRS.GetText(1, 1));
    			tMsgSendBase.setAppntName(tMsgSuccSSRS.GetText(1, 3));
    			tMsgSendBase.setAppntSex(tMsgSuccSSRS.GetText(1, 5));
    			tMsgSendBase.setPrem(tLJAGetSet.get(i).getSumGetMoney()+"");
    			tMsgSendBase.setEndorsementNo(tLJAGetSet.get(i).getActuGetNo());
    			if(tMsgSuccSSRS.GetText(1, 2)!=null&&!"".equals(tMsgSuccSSRS.GetText(1, 2))){
    				tMsgSendBase.setMobilPhone(tMsgSuccSSRS.GetText(1, 2));
    			}else{
    				System.out.println("客户电话号码为空，无法发送短信");
    				continue;
    			}
//    			tMsgSendBase.setMobilPhone("18610913667");
    			tMsgSendBase.setOperator("001");
    			tMsgSendBase.setManageCom(tMsgSuccSSRS.GetText(1, 4));
    			AnalyzeMSG tAnalyzeMSG = new AnalyzeMSG();
    			tAnalyzeMSG.setmMsgSendBase(tMsgSendBase);
    			tAnalyzeMSG.applySend("BQ002");
    		}else{
    			System.out.println("工单号"+tLJAGetSet.get(i).getOtherNo()+"查询客户信息失败，无法发送短信！！");
    		}
    	}
    	
    }
    
    public void sendUliMsg(){
    	//查询sql
    	String UliSQL = " select * from lcinsureaccbalance a where  " +
    			"  exists (select 1 from lcpol where polno=a.polno and grpcontno='00000000000000000000' and managecom like  '8644%' " +
    			"  and exists ( select 1 from lmriskapp where riskcode=lcpol.riskcode and riskprop!='G' and risktype4='4' " +
    			"  )) and polmonth='12' " +
    			(mContNo != null ? (" and contno = '" + mContNo + "' ") : "") +
    			"  and makedate<=current date and makedate> (current date - 7 days) " ;
    	LCInsureAccBalanceDB tLCInsureAccBalanceDB = new LCInsureAccBalanceDB();
    	LCInsureAccBalanceSet tLCInsureAccBalanceSet = new LCInsureAccBalanceSet();
    	tLCInsureAccBalanceSet = tLCInsureAccBalanceDB.executeQuery(UliSQL);
    	
    	for(int i=1;i<=tLCInsureAccBalanceSet.size();i++){


    		//查询短信发送基础信息
    		String queryAppntInfo = "select a.contno, "   //保单号
//        	+" (select mobile from lcaddress x, lcappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
    		+" mobile(a.contno), "
        	+" a.appntname,a.managecom, "       //被保人姓名
			+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end), "
			+" year(date('"+tLCInsureAccBalanceSet.get(i).getDueBalaDate()+"') - 1 days) "
        	+" from lccont a "
            +" where a.contno = '"+tLCInsureAccBalanceSet.get(i).getContNo()+"'  " 
            +" and not exists (select 1 from LCCSpec  where contno=('BQ003'||a.contno)  and EndorsementNo ='"+tLCInsureAccBalanceSet.get(i).getSequenceNo()+"' )  "
            +" union "
            +"select a.contno, "   //保单号
//        	+" (select mobile from lcaddress x, lbappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
            +" mobile(a.contno), "
        	+" a.appntname,a.managecom, "       //被保人姓名
			+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end), "
			+" year(date('"+tLCInsureAccBalanceSet.get(i).getDueBalaDate()+"') - 1 days) "
        	+" from lbcont a "
            +" where a.contno = '"+tLCInsureAccBalanceSet.get(i).getContNo()+"' " 
            +" and not exists (select 1 from LCCSpec  where contno=('BQ003'||a.contno)  and EndorsementNo ='"+tLCInsureAccBalanceSet.get(i).getSequenceNo()+"' )  "            
            +" with ur";
    		
    		ExeSQL tExeSQL = new ExeSQL();
    		SSRS tMsgSuccSSRS =tExeSQL.execSQL(queryAppntInfo);
    		if(tMsgSuccSSRS!=null&&tMsgSuccSSRS.getMaxRow()>0){
    			MsgSendBase tMsgSendBase = new MsgSendBase();
    			tMsgSendBase.setContno(tMsgSuccSSRS.GetText(1, 1));
    			tMsgSendBase.setAppntName(tMsgSuccSSRS.GetText(1, 3));
    			tMsgSendBase.setAppntSex(tMsgSuccSSRS.GetText(1, 5));
    			tMsgSendBase.setPolYear(tMsgSuccSSRS.GetText(1, 6));
    			tMsgSendBase.setEndorsementNo(tLCInsureAccBalanceSet.get(i).getSequenceNo());
//    			tMsgSendBase.setPrem(tLJAGetSet.get(i).getSumGetMoney()+"");
//    			tMsgSendBase.setMobilPhone(tMsgSuccSSRS.GetText(1, 2));
    			if(tMsgSuccSSRS.GetText(1, 2)!=null&&!"".equals(tMsgSuccSSRS.GetText(1, 2))){
    				tMsgSendBase.setMobilPhone(tMsgSuccSSRS.GetText(1, 2));
    			}else{
    				System.out.println("客户电话号码为空，无法发送短信");
    				continue;
    			}    			
//    			tMsgSendBase.setMobilPhone("18610913667");
    			tMsgSendBase.setOperator("001");
    			tMsgSendBase.setManageCom(tMsgSuccSSRS.GetText(1, 4));
    			AnalyzeMSG tAnalyzeMSG = new AnalyzeMSG();
    			tAnalyzeMSG.setmMsgSendBase(tMsgSendBase);
    			tAnalyzeMSG.applySend("BQ003");
    		}else{
    			System.out.println("工单号"+tLCInsureAccBalanceSet.get(i).getContNo() +"查询客户信息失败，无法发送短信！！");
    		}
    	
    	}
    }
    
    public void sendBonusMsg(){
    	String BonusSQL=" select  * from LOBonusPol where " +
    			" exists (select 1 from lcpol where polno=LOBonusPol.polno and managecom like '8644%'  ) " +
    			(mContNo != null ? (" and contno = '" + mContNo + "' ") : "") +
    			" and bonusmakedate <=current date and bonusmakedate> (current date - 7 days) with ur  ";
    	LOBonusPolDB tLOBonusPolDB = new LOBonusPolDB();
    	LOBonusPolSet tLOBonusPolSet = new LOBonusPolSet();
    	tLOBonusPolSet = tLOBonusPolDB.executeQuery(BonusSQL);
    	for(int i=1;i<=tLOBonusPolSet.size();i++){

    		//查询短信发送基础信息
    		String queryAppntInfo = "select a.contno, "   //保单号
//        	+" (select mobile from lcaddress x, lcappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
    		+" mobile(a.contno), "
        	+" a.appntname,a.managecom, "       //被保人姓名
			+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end) "
        	+" from lccont a "
            +" where a.contno = '"+tLOBonusPolSet.get(i).getContNo()+"'  " 
            +" and not exists (select 1 from LCCSpec  where contno=('BQ005'||a.contno)  and EndorsementNo ='"+tLOBonusPolSet.get(i).getPolNo()+""+tLOBonusPolSet.get(i).getFiscalYear()+"' )  "
            +" union "
            +"select a.contno, "   //保单号
//        	+" (select mobile from lcaddress x, lbappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
            +" mobile(a.contno), "
        	+" a.appntname,a.managecom, "       //被保人姓名
			+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end) "
        	+" from lbcont a "
            +" where a.contno = '"+tLOBonusPolSet.get(i).getContNo()+"' " 
            +" and not exists (select 1 from LCCSpec  where contno=('BQ005'||a.contno)  and EndorsementNo ='"+tLOBonusPolSet.get(i).getPolNo()+""+tLOBonusPolSet.get(i).getFiscalYear()+"' )  "            
            +" with ur";
    		
    		ExeSQL tExeSQL = new ExeSQL();
    		SSRS tMsgSuccSSRS =tExeSQL.execSQL(queryAppntInfo);
    		if(tMsgSuccSSRS!=null&&tMsgSuccSSRS.getMaxRow()>0){
    			MsgSendBase tMsgSendBase = new MsgSendBase();
    			tMsgSendBase.setContno(tMsgSuccSSRS.GetText(1, 1));
    			tMsgSendBase.setAppntName(tMsgSuccSSRS.GetText(1, 3));
    			tMsgSendBase.setAppntSex(tMsgSuccSSRS.GetText(1, 5));
    			tMsgSendBase.setPolYear(tLOBonusPolSet.get(i).getFiscalYear()+"");
    			tMsgSendBase.setEndorsementNo(tLOBonusPolSet.get(i).getPolNo()+""+tLOBonusPolSet.get(i).getFiscalYear());
//    			tMsgSendBase.setPrem(tLJAGetSet.get(i).getSumGetMoney()+"");
//    			tMsgSendBase.setMobilPhone(tMsgSuccSSRS.GetText(1, 2));
//    			tMsgSendBase.setMobilPhone("18610913667");
    			if(tMsgSuccSSRS.GetText(1, 2)!=null&&!"".equals(tMsgSuccSSRS.GetText(1, 2))){
    				tMsgSendBase.setMobilPhone(tMsgSuccSSRS.GetText(1, 2));
    			}else{
    				System.out.println("客户电话号码为空，无法发送短信");
    				continue;
    			}
    			
    			tMsgSendBase.setOperator("001");
    			tMsgSendBase.setManageCom(tMsgSuccSSRS.GetText(1, 4));
    			AnalyzeMSG tAnalyzeMSG = new AnalyzeMSG();
    			tAnalyzeMSG.setmMsgSendBase(tMsgSendBase);
    			tAnalyzeMSG.applySend("BQ005");
    		}else{
    			System.out.println("工单号"+tLOBonusPolSet.get(i).getContNo() +"查询客户信息失败，无法发送短信！！");
    		}
    	
    	
    	}
    }
    

    /**
     * 结算指定保单
     * @param cContNo String
     */
    public void runOneCont(String cContNo,String queryFlag)
    
    {
    	if(queryFlag!=null&&"01".equals(queryFlag)){//保全给付类
    		mContNo = cContNo;
    		sendBQGetMsg();
    	}else if(queryFlag!=null&&"02".equals(queryFlag)){//批改类
    		mContNo = cContNo;
    		sendBQMsg();
    	}else if(queryFlag!=null&&"03".equals(queryFlag)){//满期给付类
    		mContNo = cContNo;
    		sendEBMsg();
    	}else if(queryFlag!=null&&"04".equals(queryFlag)){//万能年报类
    		mContNo = cContNo;
    		sendUliMsg();
    	}else if(queryFlag!=null&&"05".equals(queryFlag)){//分红通知书类
    		mContNo = cContNo;
    		sendBonusMsg();
    	}else{
    		mContNo = null;
    		System.out.println("====================================发送短信失败");
    	}
    }

    /**
     * 校验能否进行结算
     * 险种有未完结的理赔不能结算，保单有未完结的保全不能结算
     * @param cLCInsureAccSchema LCInsureAccSchema
     * @return boolean
     */
    private boolean canBalance(LCPolSchema tLCPolSchema)
    {
        if(!LLCaseCommon.checkClaimState(tLCPolSchema.getPolNo()))
        {
            mErrors.addOneError("险种有未结案的理赔，不能进行结算");
            return false;
        }

        LPEdorItemSet set = CommonBL.getEdorItemMayChangePrem(
        		tLCPolSchema.getContNo());
        if(set.size() > 0)
        {
            String edorInfo = "";
            for(int i = 1; i <= set.size(); i++)
            {
                edorInfo += set.get(i).getEdorNo() + " "
                    + set.get(i).getEdorType() + ", ";
            }
            mErrors.addOneError("保单有未结案的保全项目:" + edorInfo);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        GetMoneyMSGTask task = new GetMoneyMSGTask();
        task.run();
//    	System.out.println(PubFun.calInterval("2013-11-18", "2013-11-16", "M"));
    }
}
