package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.f1print.YBKPrintBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.schema.LCContReceiveSchema;
import com.sinosoft.lis.tb.LCContGetPolUI;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YBKPrintBPBL extends TaskThread{
	
	private String BPSql;
	private String prtno;
	private String risk;
			
	public void run() {
		BPSql = "select lc.prtno,lc.riskcode from lcpol lc where lc.riskcode in(select code from ldcode where codetype='ybkriskcode') " 
				+" and lc.appflag='1' and lc.stateflag='1' and lc.signdate = current date - 1 day "
				+" and exists (select 1 from ybk_n01_lis_responseinfo where prtno = lc.prtno) "
				+" union "
				+" select distinct lc.prtno,lc.riskcode from lcpol lc,ybk_n01_lis_responseinfo ybk "
				+" where lc.riskcode in(select code from ldcode where codetype='ybkriskcode') and lc.appflag='1' and lc.stateflag='1' "
				+" and ybk.renewcount = lc.renewcount "
				+" and lc.signdate = current date - 1 day " 
				+" with ur ";
		//测试
//		BPSql = "select prtno,riskcode from lcpol where riskcode in(select code from ldcode where codetype='ybkriskcode') "
//				+" and appflag='1' and stateflag='1' and contno = '057261666000001'"
//				+" with ur ";
		SSRS tBPSql = new ExeSQL().execSQL(BPSql);
		for (int i = 1; i <= tBPSql.MaxRow; i++) {
			prtno = tBPSql.GetText(i, 1);
			System.out.println("tPrtNo[0] = " + prtno);
			risk = tBPSql.GetText(i, 2);
			System.out.println("risk = " + risk);
			
			//封装数据,向后台提交数据
			VData tVData = new VData();
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("risk",risk);
			tTransferData.setNameAndValue("tPrtNo[0]",prtno);
			tVData.add(tTransferData);
			
			YBKPrintBL tYBKPrintBL = new YBKPrintBL();
			if (!tYBKPrintBL.submitData(tVData)) {
		        System.out.println("向后台传输数据失败!");
		        System.out.println("=======测试失败===========");
		        continue;
		    }
		}
	}
	
	public static void main(String[] args) {
		YBKPrintBPBL tYBKPrintBPBL = new YBKPrintBPBL();
		tYBKPrintBPBL.run();
	}
	
}
