package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.agentcalculate.*;
import com.sinosoft.lis.db.LATreeTempDB;
import com.sinosoft.lis.agentassess.DealLATreeUI;
import com.sinosoft.lis.agentbranch.AdjustGrpAgentTemptTask;
import com.sinosoft.lis.db.LABranchChangeTempDB;
import com.sinosoft.lis.agentbranch.LABranchManagerTask;

/**
 * <p>Title: 任务实例</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: SinoSoft</p>
 * @author xiangchun
 * @version 1.0
 */

public class LABranchTempTask extends TaskThread {
    private int strSql;
    private static int intStartIndex;
    private static int intMaxIndex;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private LABranchChangeTempSet mLABranchChangeTempSet = new LABranchChangeTempSet();
    private LABranchChangeTempSet mManLABranchChangeTempSet = new LABranchChangeTempSet();
    private SSRS mSSRS = new SSRS();

    public LABranchTempTask() {
        intStartIndex = 1;
        intMaxIndex = 1;
    }


    public void run() {
        System.out.println(
                "＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  ExampleTask Execute !!! ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝");
        System.out.println("参数个数:" + mParameters.size());
        System.out.println("");
        LABranchTempTask a = new LABranchTempTask();
        //开始批量处理
        //处理人员调动_-------------------------------------------
        if(!EasyQuery())
        {
        }

        if (mLABranchChangeTempSet != null && mLABranchChangeTempSet.size() >= 1)
        {
            for (int i = 1; i <= mLABranchChangeTempSet.size(); i++)
            {//循环处理每一条人员调动数据
                LABranchChangeTempSet tLABranchChangeTempSet = new LABranchChangeTempSet();
                LABranchChangeTempSchema tLABranchChangeTempSchema = new LABranchChangeTempSchema();
                tLABranchChangeTempSchema=mLABranchChangeTempSet.get(i);
                tLABranchChangeTempSet.add(tLABranchChangeTempSchema);
                if (!getDate(tLABranchChangeTempSet)) {
                    continue; //处理下个团队
                }
            }
        }
        //人员调动处理完成,开始处理主管任命-----------------------------
        if(!EasyQuerymanager())
        {
         return ;
        }
        if (mManLABranchChangeTempSet != null && mManLABranchChangeTempSet.size() >= 1)
        {
            for (int i = 1; i <= mManLABranchChangeTempSet.size(); i++)
            {//循环处理每一条人员调动数据
                LABranchChangeTempSet tLABranchChangeTempSet = new LABranchChangeTempSet();
                LABranchChangeTempSchema tLABranchChangeTempSchema = new LABranchChangeTempSchema();
                tLABranchChangeTempSchema=mManLABranchChangeTempSet.get(i);
                tLABranchChangeTempSet.add(tLABranchChangeTempSchema);
                if (!getmanagerDate(tLABranchChangeTempSet)) {
                    continue; //处理下个团队
                }
            }
        }
    }

    public static void main(String[] args) {

        LABranchTempTask a = new LABranchTempTask();
        SSRS tSSRS=new SSRS();

//       if(!a.EasyQuery())
//           {
//                return;
//           }
    }

   //处理人员调动
    public boolean getDate(LABranchChangeTempSet tLABranchChangeTempSet) {
        String FlagStr = "";
        TransferData tTransferData = new TransferData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "000"; //系统自动做
        tG.ManageCom = "86"; //默认总公司
        AdjustGrpAgentTemptTask tAdjustGrpAgentTemptTask=new AdjustGrpAgentTemptTask();
        VData tVData=new VData();
        tVData.addElement(tG);
        tVData.addElement(tLABranchChangeTempSet);
        if (!tAdjustGrpAgentTemptTask.submitData(tVData,"INSERT||MAIN"))
        {
            FlagStr="Fail";
            return false;
         }
         return true;
    }
    //处理主管任命
     public boolean getmanagerDate(LABranchChangeTempSet tLABranchChangeTempSet) {
         String FlagStr = "";
         TransferData tTransferData = new TransferData();
         GlobalInput tG = new GlobalInput();
         tG.Operator = "000"; //系统自动做
         tG.ManageCom = "86"; //默认总公司
         LABranchManagerTask tLABranchManagerTask=new LABranchManagerTask();
         VData tVData=new VData();
         tVData.addElement(tG);
         tVData.addElement(tLABranchChangeTempSet);
         if (!tLABranchManagerTask.submitData(tVData,"INSERT||MAIN"))
         {
             FlagStr="Fail";
             return false;
          }
          return true;
    }
   // 查询人员调动
    public  boolean EasyQuery() {

        String strError = "";
        //每月一号计算上月的数据（即当天生效的）

        String tCValiMonth = AgentPubFun.formatDate(currentDate, "yyyyMM");
        LABranchChangeTempDB tLABranchChangeTempDB = new LABranchChangeTempDB();
        String sql =
                "select *  from  LABranchChangeTemp  where cvaliflag='1'  and CValiMonth='" +
                tCValiMonth
                + "' and branchtype='2' and branchtype2='01' and BranchChangeType='03' "
                +" order by  managecom,agentgroup,agentcode";
        // 缺少类型BranchChangeType（人员调动，主管任命。。。）
        mLABranchChangeTempSet = new LABranchChangeTempSet();
        mLABranchChangeTempSet = tLABranchChangeTempDB.executeQuery(sql);


        return true;
    }

    // 查询人员调动
     public  boolean EasyQuerymanager() {

         String strError = "";
         //每月一号计算上月的数据（即当天生效的）

         String tCValiMonth = AgentPubFun.formatDate(currentDate, "yyyyMM");
         LABranchChangeTempDB tLABranchChangeTempDB = new LABranchChangeTempDB();
         String sql =
                 "select *  from  LABranchChangeTemp  where cvaliflag='1'  and CValiMonth='" +
                 tCValiMonth
                 + "' and branchtype='2' and branchtype2='01' and BranchChangeType='04' "
                 +" order by  managecom,agentgroup,agentcode";
         // 缺少类型BranchChangeType（人员调动，主管任命。。。）
         mManLABranchChangeTempSet = new LABranchChangeTempSet();
         mManLABranchChangeTempSet = tLABranchChangeTempDB.executeQuery(sql);


         return true;
    }
}
