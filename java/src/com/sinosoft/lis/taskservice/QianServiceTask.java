package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.jkxpt.qy.QYACCGrpServiceImp;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QianServiceTask extends TaskThread
{
   
    private MMap map = new MMap();
    
    private VData mInputData;

	private String mOperate;

    public QianServiceTask()
    {

    }

    public void run()
    {
    	String sql = "Update statsqlconfig set valiflag =  '1'";
		
		map.put(sql, "UPDATE"); // 修改
		
    	try {
    		mInputData = new VData();

			mInputData.add(map);

			
		} catch (Exception ex) {
			System.out.println("批处理出错了");
		}
		
		mOperate="UPDATE";
		PubSubmit tPubSubmit = new PubSubmit();
		tPubSubmit.submitData(mInputData, mOperate);
    }

    public static void main(String[] args)
    {
    	QianServiceTask qqq = new QianServiceTask();
        qqq.run();
    }

}
