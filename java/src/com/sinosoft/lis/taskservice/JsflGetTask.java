package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.lis.ygz.GetBqGetMoneySeparateBL;
import com.sinosoft.lis.ygz.GetBqMoneyNoBL;
import com.sinosoft.lis.ygz.GetBqTempFeeSeparateBL;
import com.sinosoft.lis.ygz.GetFCSeparateBL;
import com.sinosoft.lis.ygz.GetPayNoBL;
import com.sinosoft.lis.ygz.GetTFBL;
import com.sinosoft.lis.ygz.GetTempFeeBL;
import com.sinosoft.lis.ygz.GetULIInitPremSeparateBL;
import com.sinosoft.lis.ygz.GetXqTempFeeSeparateBL;
import com.sinosoft.lis.ygz.GetYjGLMoneyBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: LCContTranfOutTask </p>
 * <p>Description: 保单迁出同步平台数据的批处理 </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class JsflGetTask extends TaskThread{
	/**错误处理信息类*/
	public CErrors mErrors = new CErrors();
	/**存储返回的结果集*/
	private VData mResult = new VData();
	
	public JsflGetTask() {

	}
	
	/**
	 * run方法
	 */
	public void run(){
		System.out.println("======================TransferAgentDataTask Execute  !!!======================");
		
		/**
		 * 往同步保单迁出数据中出入日期的值
		 */
		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		tInputData.add(tTransferData);
		/**
		 * 调用保单迁出同步平台数据的接口
		 */
		long startGetTempFeeBL = System.currentTimeMillis();
		//======提取契约价税分离数据(二期)
		try {
				GetTempFeeBL tGetTempFeeBL = new GetTempFeeBL();
				if(!tGetTempFeeBL.submitData(tInputData,"")){
					this.mErrors.copyAllErrors(tGetTempFeeBL.mErrors);
					System.out.println("从暂收表生成价税分离数据报错："+mErrors.getFirstError());
				  }
			} catch (Exception e) {
				System.out.println("从暂收表生成价税分离数据报错：");
				e.printStackTrace();
			}
		long endGetTempFeeBL = System.currentTimeMillis();
		System.out.println("========== GetTempFeeBL从暂收表生成价税分离数据批处理-耗时： "+((endGetTempFeeBL -startGetTempFeeBL)/1000/60)+"分钟");
		/**
		 * ======提取契约退费价税分离数据
		 * addby liyt 2016-06-22
		 * 
		 */
		long startGetTFSeparateBL = System.currentTimeMillis();
		try{
			GetTFBL tGetTFBL = new GetTFBL();
			if(!tGetTFBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetTFBL.mErrors);
				System.out.println("从暂收表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch(Exception e){
			System.out.println("从暂收表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetTFSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetTFSeparateBL提取契约退费价税分离数据批处理-耗时： "+((endGetTFSeparateBL -startGetTFSeparateBL)/1000/60)+"分钟");
	
		long startGetFCSeparateBL= System.currentTimeMillis();
		try{
			/**
			 * add by liyt 2016-06-24 反冲数据提取
			 */
			GetFCSeparateBL tGetFCSeparateBL = new GetFCSeparateBL();
			if(!tGetFCSeparateBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetFCSeparateBL.mErrors);
				System.out.println("反冲数据提取提取报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("反冲数据提取报错：");
			e.printStackTrace();
		}
		long endGetFCSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetFCSeparateBL反冲数据提取批处理-耗时： "+((endGetFCSeparateBL -startGetFCSeparateBL)/1000/60)+"分钟");
		
		//=====为价税分离数据补充实收号码
		long satrtGetPayNoBL = System.currentTimeMillis();
		try {
			GetPayNoBL tGetPayNoBL = new GetPayNoBL();
			if(!tGetPayNoBL.submitData(tInputData,"")){
				   this.mErrors.copyAllErrors(tGetPayNoBL.mErrors);
				   System.out.println("从实收表获取payno回写价税分离表报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("从实收表获取payno回写价税分离表报错");
			e.printStackTrace();
		}
		long endGetPayNoBL = System.currentTimeMillis();
		System.out.println("========== GetPayNoBL从实收表获取payno回写价税分离批处理-耗时： "+((endGetPayNoBL -satrtGetPayNoBL)/1000/60)+"分钟");

		long satrtGetXqTempFeeSeparateBL = System.currentTimeMillis();
		try {
			/**
			 * 续期暂收数据提取
			 */
			GetXqTempFeeSeparateBL tGetXqTempFeeSeparateBL = new GetXqTempFeeSeparateBL();
			if(!tGetXqTempFeeSeparateBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetXqTempFeeSeparateBL.mErrors);
				System.out.println("续期从暂收表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("续期从暂收表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetXqTempFeeSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetXqTempFeeSeparateBL续期从暂收表生成价税分离数据批处理-耗时： "+((endGetXqTempFeeSeparateBL -satrtGetXqTempFeeSeparateBL)/1000/60)+"分钟");

		long satrtGetBqTempFeeSeparateBL = System.currentTimeMillis();
		try {
			/**
			 * 保全暂收数据提取
			 */
			GetBqTempFeeSeparateBL tGetBqTempFeeSeparateBL = new GetBqTempFeeSeparateBL();
			if(!tGetBqTempFeeSeparateBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetBqTempFeeSeparateBL.mErrors);
				System.out.println("保全从暂收表生成价税分离数据报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("保全从暂收表生成价税分离数据报错：");
			e.printStackTrace();
		}
		long endGetBqTempFeeSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetBqTempFeeSeparateBL保全从暂收表生成价税分离数据批处理-耗时： "+((endGetBqTempFeeSeparateBL -satrtGetBqTempFeeSeparateBL)/1000/60)+"分钟");
		
		long satrtGetBqGetMoneySeparateBL = System.currentTimeMillis();
		try {
			/**
			 * 保全退费数据提取
			 */
			GetBqGetMoneySeparateBL tGetBqGetMoneySeparateBL = new GetBqGetMoneySeparateBL();
			if(!tGetBqGetMoneySeparateBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetBqGetMoneySeparateBL.mErrors);
				System.out.println("保全退费数据提取报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("保全退费数据提取报错：");
			e.printStackTrace();
		}
		long endGetBqGetMoneySeparateBL = System.currentTimeMillis();
		System.out.println("========== GetBqGetMoneySeparateBL保全退费数据提取批处理-耗时： "+((endGetBqGetMoneySeparateBL -satrtGetBqGetMoneySeparateBL)/1000/60)+"分钟");

		long startGetULIInitPremSeparateBL = System.currentTimeMillis();
		try {
			/**
			 * 初始扣费数据提取
			 */
			GetULIInitPremSeparateBL tGetULIInitPremSeparateBL = new GetULIInitPremSeparateBL(); 
			if(!tGetULIInitPremSeparateBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetULIInitPremSeparateBL.mErrors);
				System.out.println("初始扣费数据提取报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("初始扣费数据提取报错：");
			e.printStackTrace();
		}
		long endGetULIInitPremSeparateBL = System.currentTimeMillis();
		System.out.println("========== GetULIInitPremSeparateBL初始扣费数据提取批处理-耗时： "+((endGetULIInitPremSeparateBL -startGetULIInitPremSeparateBL)/1000/60)+"分钟");

		long startGetYjGLMoneyBL = System.currentTimeMillis();
		try {
		/**
		 * 月结、保全管理费数据提取
		 */
				GetYjGLMoneyBL tGetYjGLMoneyBL = new GetYjGLMoneyBL();
				if(!tGetYjGLMoneyBL.submitData(tInputData, "")){
					this.mErrors.copyAllErrors(tGetYjGLMoneyBL.mErrors);
					System.out.println("月结和保全管理费生成价税分离数据报错："+mErrors.getFirstError());
				}
			} catch (Exception e) {
				System.out.println("月结和保全管理费生成价税分离数据报错：");
				e.printStackTrace();
			}
		long endGetYjGLMoneyBL = System.currentTimeMillis();
		System.out.println("========== GetYjGLMoneyBL月结和保全管理费生成价税分离数据批处理-耗时： "+((endGetYjGLMoneyBL -startGetYjGLMoneyBL)/1000/60)+"分钟");

		long startGetBqMoneyNoBL = System.currentTimeMillis();
		try {
			/**
			 * 续期、保全回写实收号码
			 */
			GetBqMoneyNoBL tGetBqMoneyNoBL = new GetBqMoneyNoBL(); 
			if(!tGetBqMoneyNoBL.submitData(tInputData, "")){
				this.mErrors.copyAllErrors(tGetBqMoneyNoBL.mErrors);
				System.out.println("续期、保全回写实收号报错："+mErrors.getFirstError());
			}
		} catch (Exception e) {
			System.out.println("续期、保全回写实收号报错");
			e.printStackTrace();
		}
		long endGetBqMoneyNoBL = System.currentTimeMillis();
		System.out.println("========== GetBqMoneyNoBL续期、保全回写实收号批处理-耗时： "+((endGetBqMoneyNoBL -startGetBqMoneyNoBL)/1000/60)+"分钟");

		
		//=====进行价税分离
		long startPremSeparateTask = System.currentTimeMillis();
		try {
			PremSeparateTask tPremSeparateTask = new PremSeparateTask();
			System.out.println("=====开始价税分离=====");
			tPremSeparateTask.run();
			System.out.println("=====价税分离结束=====");
		} catch (Exception e) {
			System.out.println("价税分离发生报错！");
			e.printStackTrace();
		}
		long endPremSeparateTask = System.currentTimeMillis();
		System.out.println("========== PremSeparateTask价税分离批处理-耗时： "+((endPremSeparateTask -startPremSeparateTask)/1000/60)+"分钟");

	}
	/**
	 * 返回结果集的方法
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 */
	public static void main(String[] args){
		JsflGetTask tJsflGetTask = new JsflGetTask();
		
		tJsflGetTask.run();
	}
	
}
