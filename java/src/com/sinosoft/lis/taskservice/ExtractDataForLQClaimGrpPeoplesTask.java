package com.sinosoft.lis.taskservice;

import java.util.Date;
//问题：缺失三个类文件，不需要DB吗？
import com.sinosoft.lis.db.SolidificationLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.SolidificationLogSchema;
import com.sinosoft.lis.vschema.LQClaimGrpPeoplesSet;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;
//这个类名的命名规范：ExtractDataFor +  表名 + Task 并且必须继承TaskThread
public class ExtractDataForLQClaimGrpPeoplesTask extends TaskThread {
	
	public String mCurrentDate = PubFun.getCurrentDate();
	private SolidificationLogSchema mSolidificationLogSchema = new SolidificationLogSchema();
	
	public void run() 
	{
		System.out.println("ExtractDataForLQClaimGrpPeoplesTask批处理开始执行！");
		//这里是查询日志表的sql，要查出来endDate:最后一次执行的时间
		//state：endDate的状态。00表示未完成，11表示已完成
		//需要注意的是查询哪个表的这条数据sql中的表名需要更改一下
		String tSolidificationLogSQL = "select * from db2inst1.SolidificationLog where TableName = 'LQCLAIMGRPPEOPLES' with ur";
		System.out.println("tSolidificationLogSQL:"+tSolidificationLogSQL);
		//new一个日志表DB
		SolidificationLogDB tSolidificationLogDB = new SolidificationLogDB();
		//日志表DB执行sql并且将返回值存入日志表Schema，该Schema在上边已经声明，
		//因为只有一条数据但是excute返回的是数组所以直接.get(1)
		mSolidificationLogSchema = tSolidificationLogDB.executeQuery(tSolidificationLogSQL).get(1);
		//取得EndDate和State的值
		String tRealEndDate = mSolidificationLogSchema.getEndDate();
		String tState = mSolidificationLogSchema.getState();
		
		if("11".equals(tState))
		{
			//如果Endate的状态是11说明enddate的数据执行成功，执行后一天的数据，把Endate+1
			//问题：如果不成功呢？
			tRealEndDate = PubFun.calDate(tRealEndDate, 1, "D", "");
		}
		//转换成日期格式
		FDate tFDate = new FDate();
		//把+1之后的日期给变量StartDate
		Date tStartDate = tFDate.getDate(tRealEndDate);
		//当前日期
		Date tEndDate = tFDate.getDate(mCurrentDate);
		//将未执行的日期与当前日期作比较
		//下边循环的逻辑是是执行到当前日期的前一天
		while(tStartDate.compareTo(tEndDate)<0)
		{
			//其实就是EndDate+1
			String tModifyDate = tFDate.getString(tStartDate);
			System.out.println("开始--提取"+tModifyDate+"的数据");
			//其实就是EndDate+1
			mSolidificationLogSchema.setEndDate(tModifyDate);
			
			//如果插入和删除有一个不成功就break
			if(!insertData(tModifyDate)||!deleteData(tModifyDate))
			{
				//问题：如果失败为什么没有把状态更新成00，是因为原来就是00吗
				tSolidificationLogDB.setSchema(mSolidificationLogSchema);
				tSolidificationLogDB.update();
				break;
			}
			//获取当前时间
			mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
			mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
			mSolidificationLogSchema.setState("11");
			mSolidificationLogSchema.setExceptionInfo("处理成功");
			tSolidificationLogDB.setSchema(mSolidificationLogSchema);
			tSolidificationLogDB.update();
			System.out.println("结束---提取"+tModifyDate+"的数据");
			tStartDate = PubFun.calDate(tStartDate, 1, "D", null);
		}
		
		
	}
	
	/**
	 * 向表中插入数据
	 * @return
	 */
	private boolean insertData(String cModifyDate)
	{
		String tInsertSQL ="select grpcontno,grppolno,RiskCode,ManageCom,salechnl, "
				+ "(select signdate from lcgrpcont where grpcontno= lcgrppol .grpcontno) signdate, "
				+ "peoples2 from lcgrppol where appflag='1' "
				+ " and not exists(select 1 from LQClaimGrpPeoples where lcgrppol.grppolno = LQClaimGrpPeoples.grppolno ) "
				+ " and modifydate ='"+cModifyDate+"'";
		System.out.println("INSERTSQL:"+tInsertSQL);
		LQClaimGrpPeoplesSet tLQClaimGrpPeoplesSet = new LQClaimGrpPeoplesSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLQClaimGrpPeoplesSet, tInsertSQL);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		do
		{
			tRSWrapper.getData();
			if(null==tLQClaimGrpPeoplesSet||0==tLQClaimGrpPeoplesSet.size())
			{
				return true;
			}
			tMMap.put(tLQClaimGrpPeoplesSet, "INSERT");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			//这个插入数据是pubsubmit映射到DB来执行
			if(!tPubSubmit.submitData(tVData, ""))
			{
				System.out.println("ExtractDataForLQClaimGrpPeoplesTask 批处理执行异常！");
				//获取当前时间
				mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
				mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
				mSolidificationLogSchema.setState("00");
				mSolidificationLogSchema.setExceptionInfo("提取"+cModifyDate+"天数据更新时失败");
				return false;
			}
			tMMap = new MMap();
			tVData.clear();
		}while(tLQClaimGrpPeoplesSet.size()>0);
		return true;
	}
	/**
	 * 删除表中的数据
	 * @return
	 */
	private boolean deleteData(String cModifyDate)
	{
		String tDeleteSQL ="select * from LQClaimGrpPeoples where 1=1 "
				+ "and exists(select 1 from lbgrppol where LQClaimGrpPeoples.grppolno =lbgrppol.grppolno "
				+ " and  modifydate ='"+cModifyDate+"')";
		System.out.println("DELETESQL:"+tDeleteSQL);
		LQClaimGrpPeoplesSet tLQClaimGrpPeoplesSet = new LQClaimGrpPeoplesSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLQClaimGrpPeoplesSet, tDeleteSQL);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		do
		{
			tRSWrapper.getData();
			if(null==tLQClaimGrpPeoplesSet||0==tLQClaimGrpPeoplesSet.size())
			{
				return true;
			}
			tMMap.put(tLQClaimGrpPeoplesSet, "DELETE");
			tVData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if(!tPubSubmit.submitData(tVData, ""))
			{
				System.out.println("ExtractDataForLQClaimGrpPeoplesTask 批处理执行异常！");
				//获取当前时间
				mSolidificationLogSchema.setModifyDate(PubFun.getCurrentDate());
				mSolidificationLogSchema.setModifyTime(PubFun.getCurrentTime());
				mSolidificationLogSchema.setState("00");
				mSolidificationLogSchema.setExceptionInfo("删除"+cModifyDate+"天数据更新时失败");
				return false;
			}
			tMMap = new MMap();
			tVData.clear();
		}while(tLQClaimGrpPeoplesSet.size()>0);
		return true;
	}
	public static void main(String[] args) {
		ExtractDataForLQClaimGrpPeoplesTask tExtractDataForLQClaimGrpPeoplesTask = new ExtractDataForLQClaimGrpPeoplesTask();
		tExtractDataForLQClaimGrpPeoplesTask.run();
	}
}
