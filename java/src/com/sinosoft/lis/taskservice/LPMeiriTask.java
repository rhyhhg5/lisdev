package com.sinosoft.lis.taskservice;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.llcase.SegentLiAnExtraction;
import com.sinosoft.utility.ExeSQL;

public class LPMeiriTask extends TaskThread {

	
	/** 收件人 */
	private String mAddress = "";
	/** 抄送人 */
	private String mCcAddress = "";
	/** 暗抄 */
	private String mBccAddress = "";
	
	String opr = "";
	
	public LPMeiriTask(){
	}
	
	  public static String getWeek(Date date){   
	        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");  
	        String week = sdf.format(date);  
	        return week;  
	    }  

    public void run()
    {
    	Date date=new Date();
		if(!"星期六".equals(getWeek(date)) || !"星期日".equals(getWeek(date)) ){	
         getEmailAddress();
    	sendEmail();
		}else{
			System.out.println("今天是周末，这个批处理不启动");
			return;
		}
    }
    
    /**
     * 理赔channels提数
     *
     */
    private void sendEmail() {
    	 try{
         	//理赔channels提数
    		 SegentLiAnExtraction tSegentDateDataExtraction = new SegentLiAnExtraction();
         	tSegentDateDataExtraction.setMAddress(mAddress);
         	tSegentDateDataExtraction.setMCcAddress(mCcAddress);
         	tSegentDateDataExtraction.setMBccAddress(mBccAddress);
         	tSegentDateDataExtraction.getSagentData();
         	opr = "ture";
         }catch(Exception e)
         {
         	System.out.println("channels提数批处理失败！");
         	e.getMessage();
         }
		
	}
	/**
     * 获取邮件地址
     *
     */
    private void getEmailAddress() {
    	String tSQL1 = "select codealias from ldcode where codetype = 'lpemeirimail' and code = 'address' with ur";
    	mAddress = new ExeSQL().getOneValue(tSQL1);
    	String tSQL2 = "select codealias from ldcode where codetype = 'lpemeirimail' and code = 'ccaddress' with ur";
    	mCcAddress = new ExeSQL().getOneValue(tSQL2);
    	String tSQL3 = "select codealias from ldcode where codetype = 'lpemeirimail' and code = 'bccaddress' with ur";
    	mBccAddress = new ExeSQL().getOneValue(tSQL3);
		
	}
    
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}
    
    public static void main(String[] args)
    {
    	
    	LPMeiriTask tLPSegentTask = new LPMeiriTask();
    	tLPSegentTask.run();
    	System.out.println("理赔批处理结束");
    }

}
