package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.yibaotong.LLZBTransFtpXmlCase;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *浙江外包案件信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author zhang
 * @version 1.1
 */
public class LLZBCaseTask extends TaskThread {
	/**
	 * 错误的容器
	 */
	public CErrors mErros = new CErrors();
	
	String opr="";
	
	public LLZBCaseTask(){
		
	}
	
	public void run(){
		
		LLZBTransFtpXmlCase  tLLZBTransFtpXmlCase = new LLZBTransFtpXmlCase();
		if(!tLLZBTransFtpXmlCase.submitData()){
			System.out.println("浙江理赔案件信息出现问题====");
			System.out.println(mErros.getFirstError());
			opr="false";
		}else{
			System.out.println("浙江理赔案件信息正常====");
			opr="true";
		}
		
	}
	
	public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	public static void main(String[] args) {
		LLZBCaseTask tLLZBCaseTask = new LLZBCaseTask();
		tLLZBCaseTask.run();
		
	}

}
