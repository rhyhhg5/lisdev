package com.sinosoft.lis.taskservice;


import com.sinosoft.lis.yibaotong.LLHWTransFtpXml;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 *外包批次信息导入批处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Liu YaChao
 * @version 1.1
 */
public class LLHWRgtTask extends TaskThread
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();


    String opr = "";
    public LLHWRgtTask()
    {}

    public void run()
    {
    	LLHWTransFtpXml  tLLHWTransFtpXml = new LLHWTransFtpXml();
    	
    	if(!tLLHWTransFtpXml.submitData())
         {
    		 System.out.println("批次信息导入出问题了");
             System.out.println(mErrors.getErrContent());
             opr ="false";
             return;
         }else{
        	 System.out.println("批次信息导入成功了");
        	 opr ="ture";
         }

    }

   

   
    public String getOpr() {
		return opr;
	}

	public void setOpr(String opr) {
		this.opr = opr;
	}

	public static void main(String[] args)
    {

        LLHWRgtTask tLaHumandeveITask = new LLHWRgtTask();
        tLaHumandeveITask.run();
//        tGEdorMJTask.oneCompany(tGlobalInput);
    }
}




