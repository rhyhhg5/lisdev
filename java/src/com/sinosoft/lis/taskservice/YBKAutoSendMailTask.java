package com.sinosoft.lis.taskservice;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class YBKAutoSendMailTask extends TaskThread{
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 设置附件路径 */
	
	public YBKAutoSendMailTask(){
	}
	
	public void run() {
		String sql = "select distinct y.policyno from YbkDzbdLog y where " 
				+" exists(select 1 from lccont lc where y.policyno=lc.contno and prtno like 'YBK%' and appflag='1' and conttype='1' and stateflag = '1') " 
				+" and not exists(select 1 from YbkDzbdLog where policyno=y.policyno and sendflag='1') "
				+" with ur ";
		SSRS tSSRSR = new ExeSQL().execSQL(sql);
		for (int i = 1; i <= tSSRSR.MaxRow; i++) {
			String tContNo = tSSRSR.GetText(i, 1);
			System.out.println("合同号为: " + tContNo);
			YBKMailTask tYBKMailTask = new YBKMailTask(tContNo);
			tYBKMailTask.run();
		}
	}
	
	public static void main(String[] args) {	
		YBKAutoSendMailTask m = new YBKAutoSendMailTask();
		m.run();
	}

}
