package com.sinosoft.lis.taskservice;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.utility.*;
import com.sinosoft.lis.midplat.util.Data;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.fininterface.*;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.schema.LDComSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class FinIntfTask extends TaskThread {
    public FinIntfTask() {
    }
    private String mStrateDate = "";
    private String mEndDate = "";
    private String mClassType = "";
    private String cGetType = "";
    private GlobalInput mG = new GlobalInput();
    private VData vData = new VData();
    
    public String GetDate(){
        Date date=new Date();
        date.setDate(date.getDate()-1);
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        String tString = df.format(date);
        System.out.println(tString);
        return tString;
    }
    public VData InitInfo() {
        Date date=new Date();
        System.out.println(date);
        //mStrateDate =PubFun.getCurrentDate();
        mStrateDate=GetDate();
       // mStrateDate = "2008-05-15";
        mEndDate = mStrateDate;
        mG.ManageCom = "86";
        mG.Operator = "server";
        if (!mStrateDate.substring(9,10).equals("01"))
        {
        	String strSQL = "select code from licodetrans where codetype='ClassType' and code not in ('A-01','A-02','J-01','J-02') with ur";
        	SSRS tSSRS = new SSRS();
        	ExeSQL rExeSQL = new ExeSQL();
        	tSSRS = rExeSQL.execSQL(strSQL);
        	String classtype = "" ;
       // 	System.out.println(tSSRS.MaxRow);
            for(int i=1;i<=tSSRS.MaxRow;i++){
            //	System.out.println(tSSRS.GetText(i, 1));
            	classtype += tSSRS.GetText(i, 1)+",";
            }
             mClassType = classtype.substring(0, classtype.length()-1);
             System.out.println(mClassType);
         //  mClassType = "B-01,B-02,B-03,B-04,B-05,B-06,G-01,G-02,L-01,L-03,L-04,N-01,N-02,N-03,N-04,N-05,N-07,N-08,X-01,X-02,X-03,X-04,Y-01,Y-02";
         //  mClassType = "J-01,J-02";
            cGetType = "2";
        }
        else
        {
            mClassType = "";
            cGetType = "1";
        }
        vData.add(mG);
        vData.add(mStrateDate);
        vData.add(mEndDate);
        vData.add(mClassType);
        vData.add(cGetType);

        return vData;
    }

    public static void main(String[] args) {
        FinIntfTask fin=new FinIntfTask();
        fin.run();
    //	fin.InitInfo();
    }
    public void runFinDealEngine(){
        VData vData = new VData();
        /*初始化，给提数的程序赋值*/
        //FinIntfTask tFinIntfTask = new FinIntfTask();
        //vData = tFinIntfTask.InitInfo();
          vData = InitInfo();
         /*提业务数据到财务接口表*/
        FInDealEngine tFInDealEngine = new FInDealEngine();
        if (tFInDealEngine.DealDate(vData))
        {
            /*记录提数起始时间*/
            long sTime1 = Calendar.getInstance().getTimeInMillis();
            System.out.println("/****************/开始提数时间 :"
                               + Calendar.getInstance().getTimeInMillis());
            /*开始提数*/
            if (!tFInDealEngine.dealProcess(vData)) {
                System.out.println(tFInDealEngine.mErrors.getFirstError());
            }
            /*记录提数日志*/
            if (!tFInDealEngine.updateLIDistillLog()) {
                System.out.println(tFInDealEngine.mErrors.getFirstError());
                //System.out.println("afdafasdasdfasdf");
            }
            /*记录提数结束日期*/
            System.out.println("/****************/结束提数时间 :"
                               + Calendar.getInstance().getTimeInMillis());
            long sTime2 = Calendar.getInstance().getTimeInMillis();
            /*记录提数耗时*/
            System.out.println("/****************/开始提数时间 :" + sTime1);
            System.out.println("/****************/时间间隔 :"
                               + String.valueOf((sTime2 - sTime1) / 1000) +
                               "秒");
        }
    
//    String strMoveSQL="select a.batchno,  char(a.makedate) || ' ' || char(a.maketime), char(a.startdate) || '至' || char(a.enddate),a.operator, "
//                      +"nvl(sum(b.sumactumoney),0) as moneyinfo "
//                      +"from lidistilllog a,liaboriginaldata b "
//                      +"where a.batchno = b.batchno "
//                      +"and a.batchno not in (select batchno from litranlog where flag = '3') and a.batchno <> 'FinInterFaceService' "
//                      +"and a.managecom like '86%' "
//                      +"group by a.batchno,a.makedate,a.maketime,a.startdate,a.enddate,a.operator,a.managecom "
//                      +"order by batchno";
//    ExeSQL mExeSQL = new ExeSQL();
//    SSRS mSSRS = new SSRS();
//    mSSRS = mExeSQL.execSQL(strMoveSQL);
//    for (int i=1;i<=mSSRS.getMaxRow();i++)
//    {
//        /*往SAP导数*/
//        System.out.println("开始导数");
//        String mBatchNo = mSSRS.GetText(i,1);
//        String Content = "";
//        try {
//            MoveDataFromGRPToPRO oMoveDataFromGRPToPRO = new
//                    MoveDataFromGRPToPRO();
//            if (!oMoveDataFromGRPToPRO.moveData(mBatchNo)) {
//                Content = "提取数据失败，原因是：" +
//                          oMoveDataFromGRPToPRO.mErrors.getFirstError();
//                System.out.println("操作失败" + Content);
//            } else {
//                Content = "提数成功";
//                System.out.println("操作成功");
//            }
//        } catch (Exception ex) {
//            Content = "提数失败，原因是: " + ex.getMessage();
//            System.out.println(Content);
//        }
//        System.out.println("导数结束");
//    }

    }
    public boolean checkData(VData tVData ){
        boolean treturn=false;
        FinDataCheckBL tFinDataCheckBL = new FinDataCheckBL();
        boolean tcheck=tFinDataCheckBL.submitData(tVData,"");
        if(tcheck==true){
            String tresult="select 1 from LIFinDataCheck where othersign <> '0' and othersign<>'' and othersign is not null and codetype='FinDataCheck' with ur";
            String tresult2="select code from ldcode where codetype='FinInt' with ur";
            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            SSRS tSSRS = new SSRS();
            mSSRS = mExeSQL.execSQL(tresult);
            if(mSSRS.getMaxRow()==0){
                tSSRS= mExeSQL.execSQL(tresult2);      
                if(tSSRS.getMaxRow()!=0&&tSSRS.GetText(1, 1).equals("00")){
                   System.out.println("是否进行财务导数标记："+tSSRS.MaxRow);
                treturn=true;
                }
            }
        }
        return treturn;
    }
    public void run()
    {   
        System.out.println("start");
        String Startdate=GetDate();    
        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        
        tTransferData.setNameAndValue("ManageCom", "86");
        tTransferData.setNameAndValue("StartDate", Startdate);
        tTransferData.setNameAndValue("EndDate", Startdate);
        
        tVData.add(tTransferData);
      //  tFinDataCheckBL.ReCheck();
       // checkData(tVData);
 //       if(checkData(tVData)==true){
        if(true){
          runFinDealEngine();
        }
        else{
          System.out.println("没有进行批处理提数！");  
        }
    }           
}
