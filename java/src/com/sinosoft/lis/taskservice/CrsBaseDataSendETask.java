package com.sinosoft.lis.taskservice;

import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * 集团报送每日验证发送验证邮件
 * @author 杨阳
 *
 */
public class CrsBaseDataSendETask extends TaskThread {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 输入数据的容器 */

	//执行任务
	public void run() {
		//判断是否可以执行
		getCheckFlag();
	}

	//提取用户
	public boolean getCheckFlag() {
		ExeSQL tEx = new ExeSQL();
		//邮件标题
		String subject="集团报送每日验证";
		//邮件内容
		String content ="";

//		昨天是否跑批处理验证
		String flag = tEx.getOneValue("select 1 from ldtaskrunlog " +
				"where taskcode = '000050' and finishdate = current date - 1 day " +
				"with ur");
		//在ldcode中配置信息：codetype ='CrsBaseDataSendETask',code:0--失败    1--成功，具体的描述在codealias
		String code ="";
		if("".equals(flag)||null == flag)
		{
			code="0";
		}
		else
		{
			code ="1";
		}
		String sql ="select codealias from ldcode where codetype ='CrsBaseDataSendETask' and code ='"+code+"' with ur";
//		System.out.println(sql);
		content = tEx.getOneValue(sql);
		try {
			
			// 邮箱发送
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'invalidusersmail' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS.GetText(1, 2),"picchealth");
			//			 标题，内容，附件(多附件可以用|分隔，末尾|可加可不加)
			tMailSender.setSendInf(subject, content,null);
			//在ldcode里面配置，codetype='invaliduserymail'
			String tSQLRMailSql = "select codealias,codename from ldcode where codetype = 'invaliduserymail' ";
			SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
			tMailSender.setToAddress(tSSRSR.GetText(1, 1), tSSRSR.GetText(1, 2), null);
			//				 发送邮件
			if (!tMailSender.sendMail()) {
				System.out.println(tMailSender.getErrorMessage());
			}
		} catch (Exception ex) {
			this.mErrors.addOneError(new CError("集团报送每日验证出错",
					"CrsBaseDataSendETask", "run"));
			ex.toString();
			ex.printStackTrace();
			return false;
		}

		return true;

	}

	// 主方法 测试用
	public static void main(String[] args) {
		new CrsBaseDataSendETask().run();
	}
}
