package com.sinosoft.lis.operation;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * 核心业务处理类
 * @author Administrator
 *
 */
public class SalechnlBranchtypeBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	/** 数据操作字符串 */
	private String mOperate;
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	
	//准备接受数据
	String code = "" ;
	String code1 = "" ;
	String codeName = "" ;
	
	String SalesChannelsValue = "" ;
	String AcquisitionTypesValue = "" ;
	String ChannelsTypeValue = "" ;
	
	
	//存放相关sql语句
	private MMap mMap = new MMap();
	
	//无参方法
	public SalechnlBranchtypeBL() {
	}
	
	//业务处理
	public boolean submitData(VData cInputData, String cOperate) {
		
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		
		
		//进行业务处理之错误处理时
		if (!dealData()) {
			CError tError = new CError();
			tError.moduleName = "SalechnlBranchtypeBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败SalechnlBranchtypeBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据之错误时
		if (!prepareOutputData()) {
			return false;
		}
		
		//实施数据真正的处理
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start SalechnlBranchtypeBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "SalechnlBranchtypeBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
		
	}

	
	/**
	 * 取出从前台传来的数据
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		//分操作取数据核对
		if("DELETE".equals((String)tTransferData.getValueByName("operatortype"))){
			
				code = (String)tTransferData.getValueByName("code");
				if (code == null || "".equals(code)){
					CError tError = new CError();
					tError.moduleName = "SalechnlBranchtypeBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "销售渠道编号为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				
				code1 = (String)tTransferData.getValueByName("code1");
				if (code1 == null || "".equals(code1)){
					CError tError = new CError();
					tError.moduleName = "SalechnlBranchtypeBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "展业类型编号为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				
				codeName = (String)tTransferData.getValueByName("codeName");
				if (code == null || "".equals(code)){
					CError tError = new CError();
					tError.moduleName = "SalechnlBranchtypeBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "渠道类型编号为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
			
		}else if("UPDATE".equals((String)tTransferData.getValueByName("operatortype"))){
				
				code = (String)tTransferData.getValueByName("code");
				if (code == null || "".equals(code)){
					CError tError = new CError();
					tError.moduleName = "SalechnlBranchtypeBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "销售渠道编号为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				
				code1 = (String)tTransferData.getValueByName("code1");
				if (code1 == null || "".equals(code1)){
					CError tError = new CError();
					tError.moduleName = "SalechnlBranchtypeBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "展业类型编号为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				
				codeName = (String)tTransferData.getValueByName("codeName");
				if (code == null || "".equals(code)){
					CError tError = new CError();
					tError.moduleName = "SalechnlBranchtypeBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "渠道类型编号为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				
				SalesChannelsValue = (String)tTransferData.getValueByName("SalesChannelsValue");
				if (SalesChannelsValue == null || "".equals(SalesChannelsValue)){
					CError tError = new CError();
					tError.moduleName = "SalechnlBranchtypeBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "销售渠道编号为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				
				AcquisitionTypesValue = (String)tTransferData.getValueByName("AcquisitionTypesValue");
				if (AcquisitionTypesValue == null || "".equals(AcquisitionTypesValue)){
					CError tError = new CError();
					tError.moduleName = "SalechnlBranchtypeBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "展业类型编号为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				
				ChannelsTypeValue = (String)tTransferData.getValueByName("ChannelsTypeValue");
				if (ChannelsTypeValue == null || "".equals(ChannelsTypeValue)){
					CError tError = new CError();
					tError.moduleName = "SalechnlBranchtypeBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "渠道类型编号为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				
		}else if("INSERT".equals((String)tTransferData.getValueByName("operatortype"))){
			
			SalesChannelsValue = (String)tTransferData.getValueByName("SalesChannelsValue");
			if (SalesChannelsValue == null || "".equals(SalesChannelsValue)){
				CError tError = new CError();
				tError.moduleName = "SalechnlBranchtypeBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "销售渠道编号为空！";
				this.mErrors.addOneError(tError);
				return false;
			}
			
			AcquisitionTypesValue = (String)tTransferData.getValueByName("AcquisitionTypesValue");
			if (AcquisitionTypesValue == null || "".equals(AcquisitionTypesValue)){
				CError tError = new CError();
				tError.moduleName = "SalechnlBranchtypeBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "展业类型编号为空！";
				this.mErrors.addOneError(tError);
				return false;
			}
			
			ChannelsTypeValue = (String)tTransferData.getValueByName("ChannelsTypeValue");
			if (ChannelsTypeValue == null || "".equals(ChannelsTypeValue)){
				CError tError = new CError();
				tError.moduleName = "SalechnlBranchtypeBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "渠道类型编号为空！";
				this.mErrors.addOneError(tError);
				return false;
			}
			
		}
		return true;
	}

	//处理数据
	private boolean dealData(){
		
		//操作分支
		if("DELETE".equals(mOperate)){
			
			//书写相关业务sql语句
			String sqlDelete = "DELETE FROM Ldcode1 where 1=1 AND codetype = 'salechnl'"
								+" AND code='"+code
								+"' AND code1='"+code1
								+"' AND codename='"+codeName
								+"' WITH UR";
			mMap.put(sqlDelete, "DELETE");
			System.out.println("刪除語句已走"+code+code1+codeName);
			
		}else if("UPDATE".equals(mOperate)){
			
			//书写相关业务sql语句
			String sqlDelete = "UPDATE Ldcode1 SET code='"+SalesChannelsValue
								+"' , code1='"+AcquisitionTypesValue
								+"' , codename='"+ChannelsTypeValue
								+ "' where 1=1 AND codetype = 'salechnl'"
								+"  AND code='"+code
								+"' AND code1='"+code1
								+"' AND codename='"+codeName
								+"' WITH UR";
			mMap.put(sqlDelete, "UPDATE");
			System.out.println("更新語句已走！"+AcquisitionTypesValue+AcquisitionTypesValue+ChannelsTypeValue);
			
		}else if("INSERT".equals(mOperate)){
			
			//书写相关业务sql语句
			String sqlDelete = "INSERT INTO Ldcode1(codetype,code,code1,codename,codealias,comcode,othersign)"
								+ " values('salechnl','"+SalesChannelsValue+"','"+AcquisitionTypesValue+"','"+ChannelsTypeValue+"','','','') "
								+ " with ur";
			mMap.put(sqlDelete, "INSERT");
			
			System.out.println("新增語句已走！"+SalesChannelsValue+AcquisitionTypesValue+ChannelsTypeValue);
			
		}else{
			CError tError = new CError();
			tError.moduleName = "SalechnlBranchtypeBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单类型失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		return true;
	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	private boolean prepareOutputData(){
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "SalechnlBranchtypeBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
