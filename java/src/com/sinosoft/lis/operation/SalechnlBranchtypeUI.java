package com.sinosoft.lis.operation;

import com.sinosoft.lis.actuary.CalLoreserveBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;


/**
 * 业务UI类
 */
public class SalechnlBranchtypeUI {
	
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
	 public  CErrors mErrors = new CErrors();
	 /** 往后面传输数据的容器 */
     private VData mInputData = new VData();
     /** 往界面传输数据的容器 */
	 private VData mResult = new VData();
	 /** 数据操作字符串 */
	 private String mOperate;

	 //无参构造
	 public SalechnlBranchtypeUI() {
	 }
	
	 /**
	 *传输数据的公共方法
	 */
	 public boolean submitData(VData cInputData,String cOperate)
	 {
	    //将操作数据拷贝到本类中
	    this.mOperate = cOperate;
	    
	    //实例化BL
	    SalechnlBranchtypeBL salechnlBranchtypeBL = new SalechnlBranchtypeBL();

	    //执行BL业务处理
	    if (salechnlBranchtypeBL.submitData(cInputData,mOperate))
	    {
			// @@错误处理
			this.mErrors.copyAllErrors(salechnlBranchtypeBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "SalechnlBranchtypeUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提取失败!";
			this.mErrors .addOneError(tError) ;
			mResult.clear();
			return false;
	    }
	    return true;
	  }

	  //获取处理后的数据集
	  public VData getResult()
	  {
	  	return mResult;
	  }

}
