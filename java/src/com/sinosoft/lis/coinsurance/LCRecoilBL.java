package com.sinosoft.lis.coinsurance;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.vschema.LCISPayGetSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LCISPayGetSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LCISPayGetDB;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.utility.SysConst;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.vschema.LJAPaySet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author fuxin
 * @version 1.0
 */
public class LCRecoilBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCISPayGetSet mLCISPayGetSet = new LCISPayGetSet();
    private LCISPayGetSchema mLCISPayGetSchema = new LCISPayGetSchema();
    private LCISPayGetDB mLCISPayGetDB = new LCISPayGetDB();
    private MMap mMap = new MMap();
    private String mOperate = "";
    private String mBatchNo = "";
    private String mType = "";
    private String mMoney ="" ; //任务对应的合计金额
    private String mPayMode = "";
    private String mBankCode = "";
    private String mBankAccNo = "";
    private TransferData mTransferData = new TransferData();
//    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();
//    private LJAPaySet mLJAPaySet = new LJAPaySet();
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
//    private LJAPayGrpSchema mLJAPayGrpSchema = new LJAPayGrpSchema();
    private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();
    private SSRS mSSRS = new SSRS();
    private ExeSQL mExeSQL = new ExeSQL();
    private Reflections ref = new Reflections();

    public LCRecoilBL() {
    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        // 进行数据处理
        if (!dealData()) {
            return false;
        }
        //准备给后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("after prepareOutputData");

        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCRecoilBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    /**
     * 传输数据的公共方法
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);

        mMoney =(String) mTransferData.getValueByName("Claimmoney");
        mType =(String) mTransferData.getValueByName("Type");
        mBatchNo =(String)mTransferData.getValueByName("BatchNo");
        mPayMode =(String)mTransferData.getValueByName("PayMode") ;
        mBankCode =(String)mTransferData.getValueByName("BankCode") ;
        mBankAccNo =(String)mTransferData.getValueByName("BankAccNo") ;

        if ( mMoney== null || mMoney.equals("")
             &&  mType==null || mType.equals("")
             && mBatchNo == null || mBatchNo.equals("")) {
            CError.buildErr(this, "没有得到足够的数据，请选择抽档类型!");
            return false;
        }
        return true;
    }

    private boolean dealData() {
        System.out.println("***Start dealData***");
        if(mType.equals("TB"))
        {
            //提取金额大于0的，反冲实收表
            //否则反冲实付表。
            if(Double.parseDouble(mMoney) >= 0)
            {
                LJAPaySchema mLJAPaySchema = new LJAPaySchema();
                LJAPaySet mLJAPaySet = new LJAPaySet();

                //得到批次对应保单
                String sql =" select batchno,grpcontno From LCISPayGet where  othernotype ='"+mType+"' and state='1' "
                           +" and batchno ='"+mBatchNo+"' "
                           +" group by batchno,grpcontno "
                           ;
               mSSRS = mExeSQL.execSQL(sql);
               if (mSSRS.MaxRow==0)
               {
                   CError.buildErr(this, "该纪录尚未被抽档确认，不能反冲！");
                   return false ;
               }
               for(int i = 1 ;  i<= mSSRS.getMaxRow() ; i++ )
               {
                   String tPayNo = createPayNoticeNo();
                   String tGrpContNo = mSSRS.GetText(i,2);
                   //根据保单得到收费
                   String SQL = " select GRPpolno,riskcode,getnoticeno,sum(claimmoney) From LCISPayGet where  othernotype ='"+mType+"'"
                              + " and batchno ='"+mBatchNo+"'"
                              + " and grpcontno ='"+tGrpContNo+"'"
                              + "group by GRPpolno,riskcode,getnoticeno "
                              ;
                  SSRS tSSRS = new ExeSQL().execSQL(SQL);
                  for ( int j = 1 ; j <= tSSRS.getMaxRow() ; j++ )
                  {
                      //根据保单反冲明细
                      LJAPayGrpSchema mLJAPayGrpSchema = new LJAPayGrpSchema();
                      LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();
                      LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
                      if(mType.equals("TB"))
                      {
                          tLJAPayGrpDB.setPayNo(tSSRS.GetText(j,3));
                      }else{
                          tLJAPayGrpDB.setGetNoticeNo(tSSRS.GetText(j, 3));
                      }

                      tLJAPayGrpDB.setGrpPolNo(tSSRS.GetText(j, 1));
                      tLJAPayGrpDB.setPayType("ZC");
                      tLJAPayGrpSet = tLJAPayGrpDB.query();
                      if (tLJAPayGrpSet.size() == 0 )
                      {
                          CError.buildErr(this, "获得实收记录失败！");
                          return false ;
                      }
                      LJAPayGrpSchema tLJAPayGrpSchema = tLJAPayGrpSet.get(1).getSchema();
                      ref.transFields(mLJAPayGrpSchema,tLJAPayGrpSchema);
                      mLJAPayGrpSchema.setPayNo(tPayNo);
                      if(mType.equals("TB"))
                      {
                          String TBsql =" select * From ljapaygrp where paycount = 1 and grpcontno ='"+tGrpContNo+"'"
                                       +" and riskcode ='"+tSSRS.GetText(j,2)+"' and paytype ='ZC' "
                                       ;
                         LJAPayGrpDB xLJAPayGrpDB = new LJAPayGrpDB();
                         LJAPayGrpSchema xLJAPayGrpSchema = new LJAPayGrpSchema();
                         LJAPayGrpSet  xLJAPayGrpSet = xLJAPayGrpDB.executeQuery(TBsql);
                         xLJAPayGrpSchema = xLJAPayGrpSet.get(1);
                         mLJAPayGrpSchema.setCurPayToDate(xLJAPayGrpSchema.getCurPayToDate());
                         mLJAPayGrpSchema.setLastPayToDate(xLJAPayGrpSchema.getLastPayToDate());
                      }else
                      {
                          String XQsql =" select * From ljapaygrp where paycount > 1 and grpcontno ='" +tGrpContNo + "'"
                                       + " and riskcode ='" + tSSRS.GetText(j, 2) +
                                       "' and paytype ='ZC' order by paycount desc"
                                       ;
                          LJAPayGrpDB xLJAPayGrpDB = new LJAPayGrpDB();
                          LJAPayGrpSchema xLJAPayGrpSchema = new LJAPayGrpSchema();
                          LJAPayGrpSet xLJAPayGrpSet = xLJAPayGrpDB.executeQuery(XQsql);
                          xLJAPayGrpSchema = xLJAPayGrpSet.get(1);
                          mLJAPayGrpSchema.setCurPayToDate(xLJAPayGrpSchema.getCurPayToDate());
                          mLJAPayGrpSchema.setLastPayToDate(xLJAPayGrpSchema.getLastPayToDate());
                      }
                      mLJAPayGrpSchema.setSumActuPayMoney(-Double.parseDouble(tSSRS.GetText(j,4)));
                      mLJAPayGrpSchema.setSumDuePayMoney(-Double.parseDouble(tSSRS.GetText(j,4)));
                      mLJAPayGrpSchema.setOperator(mGlobalInput.Operator);
                      mLJAPayGrpSchema.setPayType("GB");//共保反冲
                      mLJAPayGrpSchema.setGetNoticeNo(mBatchNo);
                      mLJAPayGrpSchema.setPayDate(PubFun.getCurrentDate());
                      mLJAPayGrpSchema.setConfDate(PubFun.getCurrentDate());
                      mLJAPayGrpSchema.setEnterAccDate(PubFun.getCurrentDate());
                      mLJAPayGrpSchema.setMakeDate(PubFun.getCurrentDate());
                      mLJAPayGrpSchema.setMakeTime(PubFun.getCurrentTime());
                      mLJAPayGrpSchema.setModifyDate(PubFun.getCurrentDate());
                      mLJAPayGrpSchema.setModifyTime(PubFun.getCurrentTime());
                      mLJAPayGrpSet.add(mLJAPayGrpSchema);
                  }
                  mMap.put(mLJAPayGrpSet,SysConst.INSERT);
                  LJAPayDB tLJAPayDB = new LJAPayDB();
                  if(mType.equals("TB"))
                  {
                      tLJAPayDB.setPayNo(tSSRS.GetText(i, 3));
                  }else
                  {
                      tLJAPayDB.setGetNoticeNo(tSSRS.GetText(i, 3));
                  }
                  LJAPaySet tLJAPaySet = tLJAPayDB.query();
                  if (tLJAPaySet.size() == 0) {
                      mErrors.addOneError("没有查询都实收记录");
                      return false;
                  }
                  LJAPaySchema tLJAPaySchema = tLJAPaySet.get(1).getSchema();
                  ref.transFields(mLJAPaySchema,tLJAPaySchema);
                  mLJAPaySchema.setPayNo(tPayNo);
                  mLJAPaySchema.setGetNoticeNo(mBatchNo);
                  mLJAPaySchema.setSumActuPayMoney(-Double.parseDouble(mMoney));
                  mLJAPaySchema.setSumActuPayMoney(-Double.parseDouble(mMoney));
                  mLJAPaySchema.setBankAccNo(mBankAccNo);
                  mLJAPaySchema.setBankCode(mBankCode);
                  mLJAPaySchema.setDueFeeType("3");
                  mLJAPaySchema.setOperator(mGlobalInput.Operator);
                  mLJAPaySchema.setConfDate(PubFun.getCurrentDate());
                  mLJAPaySchema.setEnterAccDate(PubFun.getCurrentDate());
                  mLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
                  mLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
                  mLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
                  mLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
                  mLJAPaySet.add(mLJAPaySchema);
                  mMap.put(mLJAPaySet,SysConst.INSERT);
               }
            }
        }
        else if (mType.equals("XQ")){

            //提取金额大于0的，反冲实收表
            //否则反冲实付表。
            if(Double.parseDouble(mMoney) >= 0)
            {
            	String tPayNo = "";
            	String mPayNo = "";
                LJAPaySchema mLJAPaySchema = new LJAPaySchema();
                LJAPaySet mLJAPaySet = new LJAPaySet();

                //得到批次对应保单
                String sql =" select batchno,grpcontno From LCISPayGet where  othernotype ='"+mType+"' and state='1' "
                           +" and batchno ='"+mBatchNo+"' "
                           +" group by batchno,grpcontno "
                           ;
               mSSRS = mExeSQL.execSQL(sql);
               if (mSSRS.MaxRow==0)
               {
                   CError.buildErr(this, "该纪录尚未被抽档确认，不能反冲！");
                   return false ;
               }
               for(int i = 1 ;  i<= mSSRS.getMaxRow() ; i++ )
               {
                   String tGrpContNo = mSSRS.GetText(i,2);
                   //根据保单得到收费
                   String SQL = " select GRPpolno,riskcode,sum(claimmoney) From LCISPayGet where  othernotype ='"+mType+"'"
                              + " and batchno ='"+mBatchNo+"'"
                              + " and grpcontno ='"+tGrpContNo+"'"
                              + "group by GRPpolno,riskcode"
                              ;
                  SSRS tSSRS = new ExeSQL().execSQL(SQL);
                  tPayNo = createPayNoticeNo();
                  for ( int j = 1 ; j <= tSSRS.getMaxRow() ; j++ )
                  {
//                	  tPayNo = createPayNoticeNo();
                      //根据保单反冲明细
                      LJAPayGrpSchema mLJAPayGrpSchema = new LJAPayGrpSchema();
                      
                      String XQsql =" select * From ljapaygrp where paycount > 1 and grpcontno ='" +tGrpContNo + "'"
                                   + " and riskcode ='" + tSSRS.GetText(j, 2) +
                                     "' and paytype ='ZC' order by paycount desc";
                      LJAPayGrpDB xLJAPayGrpDB = new LJAPayGrpDB();
                      LJAPayGrpSchema xLJAPayGrpSchema = new LJAPayGrpSchema();
                      LJAPayGrpSet xLJAPayGrpSet = xLJAPayGrpDB.executeQuery(XQsql);
                      if (xLJAPayGrpSet.size() == 0 )
                      {
                          CError.buildErr(this, "获得实收记录失败！");
                          return false ;
                      }
                      xLJAPayGrpSchema = xLJAPayGrpSet.get(1);
                      mPayNo = xLJAPayGrpSchema.getPayNo();
                      ref.transFields(mLJAPayGrpSchema,xLJAPayGrpSchema);
                      mLJAPayGrpSchema.setPayNo(tPayNo);
                      mLJAPayGrpSchema.setSumActuPayMoney(-Double.parseDouble(tSSRS.GetText(j,3)));
                      mLJAPayGrpSchema.setSumDuePayMoney(-Double.parseDouble(tSSRS.GetText(j,3)));
                      mLJAPayGrpSchema.setOperator(mGlobalInput.Operator);
                      mLJAPayGrpSchema.setPayType("GB");//共保反冲
                      mLJAPayGrpSchema.setGetNoticeNo(mBatchNo);
                      mLJAPayGrpSchema.setPayDate(PubFun.getCurrentDate());
                      mLJAPayGrpSchema.setConfDate(PubFun.getCurrentDate());
                      mLJAPayGrpSchema.setEnterAccDate(PubFun.getCurrentDate());
                      mLJAPayGrpSchema.setMakeDate(PubFun.getCurrentDate());
                      mLJAPayGrpSchema.setMakeTime(PubFun.getCurrentTime());
                      mLJAPayGrpSchema.setModifyDate(PubFun.getCurrentDate());
                      mLJAPayGrpSchema.setModifyTime(PubFun.getCurrentTime());
                      mLJAPayGrpSet.add(mLJAPayGrpSchema);
                  }
                  mMap.put(mLJAPayGrpSet,SysConst.INSERT);

                  LJAPayDB tLJAPayDB = new LJAPayDB();
                  tLJAPayDB.setIncomeNo(tGrpContNo);

                  LJAPaySet tLJAPaySet = tLJAPayDB.query();
                  if (tLJAPaySet.size() == 0) {
                      mErrors.addOneError("没有查询都实收记录");
                      return false;
                  }
                  LJAPaySchema tLJAPaySchema = tLJAPaySet.get(1).getSchema();
                  ref.transFields(mLJAPaySchema,tLJAPaySchema);
                  mLJAPaySchema.setPayNo(tPayNo);
                  mLJAPaySchema.setGetNoticeNo(mBatchNo);
                  mLJAPaySchema.setSumActuPayMoney(-Double.parseDouble(mMoney));
                  mLJAPaySchema.setSumActuPayMoney(-Double.parseDouble(mMoney));
                  mLJAPaySchema.setBankAccNo(mBankAccNo);
                  mLJAPaySchema.setBankCode(mBankCode);
                  mLJAPaySchema.setDueFeeType("3");
                  mLJAPaySchema.setOperator(mGlobalInput.Operator);
                  mLJAPaySchema.setConfDate(PubFun.getCurrentDate());
                  mLJAPaySchema.setEnterAccDate(PubFun.getCurrentDate());
                  mLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
                  mLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
                  mLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
                  mLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
                  mLJAPaySet.add(mLJAPaySchema);
                  mMap.put(mLJAPaySet,SysConst.INSERT);
               }
            }
        }
        else if(mType.equals("BQ"))
        {
            if(Double.parseDouble(mMoney) >= 0)
            {
                String BQPayNo = createPayNoticeNo();
                LJAPayGrpSchema mLJAPayGrpSchema = new LJAPayGrpSchema();
                LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
                LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
                String sql = " select batchno,grpcontno From LCISPayGet where  othernotype ='" +mType + "' and state='1' "
                           + " and batchno ='" + mBatchNo + "' "
                           + " group by batchno,grpcontno "
                           ;
                mSSRS = mExeSQL.execSQL(sql);
                if (mSSRS.MaxRow == 0) {
                    CError.buildErr(this, "该纪录尚未被抽档确认，不能反冲！");
                    return false;
                }
                for ( int i = 1 ; i <= mSSRS.getMaxRow() ; i++)
                {
                    String tGrpContNo = mSSRS.GetText(i,2);
                    String BQpaySQL = " select grppolno,riskcode,sum(claimmoney) From LCISPayGet where  othernotype ='" +mType + "' and state='1' "
                               + " and batchno ='" + mBatchNo + "'"
                               + " and grpContNo='"+tGrpContNo+"'"
                               + " group by grppolno,riskcode "
                               ;
                    SSRS xSSRS = new ExeSQL().execSQL(BQpaySQL);
                    for ( int j = 1 ; j <= xSSRS.getMaxRow() ; j++ )
                    {
                        LJAPayGrpSchema xLJAPayGrpSchema = new LJAPayGrpSchema();
                        String dateSql = "";
                        if("000000".equals(xSSRS.GetText(j,2))){
                        	dateSql = " select  *  from ljapaygrp where grpcontno ='"+tGrpContNo+"'"
                                    + " and endorsementno is null "
                                    + " order by paycount desc "
                                    + " with ur  "; 
                    	}else{
                    		dateSql = " select  *  from ljapaygrp where grpcontno ='"+tGrpContNo+"'"
                                    + " and riskcode ='"+xSSRS.GetText(j,2)+"'"
                                    + " and endorsementno is null "
                                    + " order by paycount desc "
                                    + " with ur  ";                    		
                    	}

                        LJAPayGrpSet tLJAPayGrpSet = tLJAPayGrpDB.executeQuery(dateSql);
                        tLJAPayGrpSchema = tLJAPayGrpSet.get(1); //取得到最后一条实收数据。
                        tLJAPayGrpSchema.setRiskCode(xSSRS.GetText(j,2));

                        ref.transFields(xLJAPayGrpSchema,tLJAPayGrpSchema);
                        xLJAPayGrpSchema.setPayNo(BQPayNo);
                        xLJAPayGrpSchema.setGetNoticeNo(mBatchNo);
                        xLJAPayGrpSchema.setGrpPolNo(xSSRS.GetText(j,1));
                        xLJAPayGrpSchema.setRiskCode(xSSRS.GetText(j,2));
                        xLJAPayGrpSchema.setSumActuPayMoney(-Double.parseDouble(xSSRS.GetText(j,3)));
                        xLJAPayGrpSchema.setSumDuePayMoney(-Double.parseDouble(xSSRS.GetText(j,3)));
                        xLJAPayGrpSchema.setMakeDate(PubFun.getCurrentDate());
                        xLJAPayGrpSchema.setMakeTime(PubFun.getCurrentTime());
                        xLJAPayGrpSchema.setModifyDate(PubFun.getCurrentDate());
                        xLJAPayGrpSchema.setModifyTime(PubFun.getCurrentTime());
                        xLJAPayGrpSchema.setConfDate(PubFun.getCurrentDate());
                        xLJAPayGrpSchema.setEnterAccDate(PubFun.getCurrentDate());
                        xLJAPayGrpSchema.setPayType("GB");
                        xLJAPayGrpSchema.setOperator(mGlobalInput.Operator);
                        System.out.println("我是超人: i am superman!");
                        mLJAPayGrpSet.add(xLJAPayGrpSchema);
                    }
                    mMap.put(mLJAPayGrpSet,SysConst.INSERT);
                    LJAPaySchema mLJAPaySchema = new LJAPaySchema();
                    LJAPayDB tLJAPayDB = new LJAPayDB();
                    tLJAPayDB.setPayNo(tLJAPayGrpSchema.getPayNo());
                    if (!tLJAPayDB.getInfo())
                    {
                        CError.buildErr(this, "获得实收纪录失败！");
                        return false;
                    }
                    LJAPaySchema tLJAPaySchema = tLJAPayDB.getSchema();
                    LJAPaySet mLJAPaySet = new LJAPaySet();
                    ref.transFields(mLJAPaySchema,tLJAPaySchema);
                    mLJAPaySchema.setSumActuPayMoney(mMoney);
                    mLJAPaySchema.setPayNo(BQPayNo);
                    mLJAPaySchema.setGetNoticeNo(mBatchNo);
                    mLJAPaySchema.setSumActuPayMoney( -Double.parseDouble(mMoney));
                    mLJAPaySchema.setSumActuPayMoney( -Double.parseDouble(mMoney));
                    mLJAPaySchema.setBankAccNo(mBankAccNo);
                    mLJAPaySchema.setBankCode(mBankCode);
                    mLJAPaySchema.setDueFeeType("3");
                    mLJAPaySchema.setOperator(mGlobalInput.Operator);
                    mLJAPaySchema.setConfDate(PubFun.getCurrentDate());
                    mLJAPaySchema.setEnterAccDate(PubFun.getCurrentDate());
                    mLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
                    mLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
                    mLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
                    mLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
                    mLJAPaySet.add(mLJAPaySchema);
                    mMap.put(mLJAPaySet, SysConst.INSERT);
                }
            }else
            {
                String BQActuGetNo = createGetNoticeNo();
                String sql   = " select batchno,grpcontno From LCISPayGet where  othernotype ='" +mType + "' and state='1' "
                             + " and batchno ='" + mBatchNo + "' "
                             + " group by batchno,grpcontno "
                             ;
                mSSRS = mExeSQL.execSQL(sql);
                if (mSSRS.getMaxRow()== 0){
                    CError.buildErr(this,"获得反冲信息失败！");
                    return false ;
                }
                for ( int i = 1 ; i <= mSSRS.getMaxRow() ; i++ )
                {
                    String tGrpContNo = mSSRS.GetText(i,2);
                    String SQL   =   " select sum(claimmoney) From LCISPayGet where  othernotype ='" +mType + "'"
                                 + " and batchno ='" + mBatchNo + "'"
                                 + " and grpcontno ='" + tGrpContNo + "'"
                                 ;
                   String getMoney = new ExeSQL().getOneValue(SQL);
                   String otherSql = " select managecom,agentcom,agenttype,agentcode,agentgroup,salechnl From lcgrpcont where grpcontno ='"+tGrpContNo+"'"
                                   + " union "
                                   + " select managecom,agentcom,agenttype,agentcode,agentgroup,salechnl From lbgrpcont where grpcontno ='"+tGrpContNo+"'"
                                   ;
                   SSRS tSSRS = new ExeSQL().execSQL(otherSql);
                   String tManagecom = tSSRS.GetText(1, 1);
                   String tAgentcom = tSSRS.GetText(1, 2);
                   String tAgenttype = tSSRS.GetText(1, 3);
                   String tAgentcode = tSSRS.GetText(1, 4);
                   String tAgentgroup = tSSRS.GetText(1, 5);
                   String tSalechnl = tSSRS.GetText(1,6);
                   mLJAGetSchema.setActuGetNo(BQActuGetNo);
                   mLJAGetSchema.setOtherNo(mBatchNo);
                   mLJAGetSchema.setOtherNoType("GB");
                   mLJAGetSchema.setPayMode(mPayMode); //付费方式
                   mLJAGetSchema.setInsBankAccNo(mBankAccNo);
                   mLJAGetSchema.setInsBankCode(mBankCode);
                   mLJAGetSchema.setManageCom(tManagecom);
                   mLJAGetSchema.setSaleChnl(tSalechnl);
                   mLJAGetSchema.setAgentCom(tAgentcom);
                   mLJAGetSchema.setAgentType(tAgenttype);
                   mLJAGetSchema.setAgentCode(tAgentcode);
                   mLJAGetSchema.setAgentGroup(tAgentgroup);
                   mLJAGetSchema.setSumGetMoney(getMoney);
                   mLJAGetSchema.setEnterAccDate(PubFun.getCurrentDate());
                   mLJAGetSchema.setConfDate(PubFun.getCurrentDate());
                   mLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
                   mLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
                   mLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
                   mLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
                   mLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
                   mLJAGetSchema.setOperator(mGlobalInput.Operator);
                   mMap.put(mLJAGetSchema,SysConst.INSERT);
                }
            }
        }
        else{
            String LPActuGetNo = createGetNoticeNo();
            String sql   = " select batchno,grpcontno From LCISPayGet where  state='1' "
                         + " and batchno ='" + mBatchNo + "' "
                         + " group by batchno,grpcontno "
                         ;
               mSSRS = mExeSQL.execSQL(sql);
               if (mSSRS.getMaxRow() == 0) {
                   CError.buildErr(this, "获得反冲信息失败！");
                   return false;
               }
               for ( int i = 1 ; i <= mSSRS.getMaxRow() ; i ++)
               {
                   String tGrpContNo = mSSRS.GetText(i,2);
                   String SQL   =   " select sum(claimmoney) From LCISPayGet where "
                                    + "  batchno ='" + mBatchNo + "'"
                                    + " and grpcontno ='" + tGrpContNo + "'"
                                    ;
                   String getMoney = new ExeSQL().getOneValue(SQL);
                   String otherSql = " select managecom,agentcom,agenttype,agentcode,agentgroup,Salechnl From lcgrpcont where grpcontno ='"+tGrpContNo+"'"
                                     + " union "
                                     + " select managecom,agentcom,agenttype,agentcode,agentgroup,Salechnl From lbgrpcont where grpcontno ='"+tGrpContNo+"'"
                                     ;
                   SSRS tSSRS = new ExeSQL().execSQL(otherSql);
                   String tManagecom = tSSRS.GetText(1, 1);
                   String tAgentcom = tSSRS.GetText(1, 2);
                   String tAgenttype = tSSRS.GetText(1, 3);
                   String tAgentcode = tSSRS.GetText(1, 4);
                   String tAgentgroup = tSSRS.GetText(1, 5);
                   String tSalechnl = tSSRS.GetText(1,6);
                   mLJAGetSchema.setActuGetNo(LPActuGetNo);
                   mLJAGetSchema.setOtherNo(mBatchNo);
                   mLJAGetSchema.setOtherNoType("GB");
                   mLJAGetSchema.setPayMode(mPayMode); //付费方式
                   mLJAGetSchema.setInsBankAccNo(mBankAccNo);
                   mLJAGetSchema.setInsBankCode(mBankCode);
                   mLJAGetSchema.setManageCom(tManagecom);
                   mLJAGetSchema.setAgentCom(tAgentcom);
                   mLJAGetSchema.setAgentType(tAgenttype);
                   mLJAGetSchema.setSaleChnl(tSalechnl);
                   mLJAGetSchema.setAgentCode(tAgentcode);
                   mLJAGetSchema.setAgentGroup(tAgentgroup);
                   mLJAGetSchema.setSumGetMoney(getMoney);
                   mLJAGetSchema.setEnterAccDate(PubFun.getCurrentDate());
                   mLJAGetSchema.setConfDate(PubFun.getCurrentDate());
                   mLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
                   mLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
                   mLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
                   mLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
                   mLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
                   mLJAGetSchema.setOperator(mGlobalInput.Operator);
                   mMap.put(mLJAGetSchema,SysConst.INSERT);
               }
        }
        changeState(mBatchNo);
        return true;
    }

    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCISPullBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错:" +
                                  ex.toString();
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 产生交费记录号，如果原来LJSPay表中没有数据时才产生新的号
     * @return String
     */
    private String createPayNoticeNo() {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(mBatchNo);
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() > 0) {
            return tLJSPaySet.get(1).getGetNoticeNo();
        } else {
            return PubFun1.CreateMaxNo("PAYNO", null);
        }
    }

    /**
     * 产生退费记录号，如果原来LJAGet表中没有数据时才产生新的号
     * @return String
     */
    private String createGetNoticeNo() {
        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setOtherNo(mBatchNo);
        LJAGetSet tLJAGetSet = tLJAGetDB.query();
        if (tLJAGetSet.size() > 0) {
            return tLJAGetSet.get(1).getActuGetNo();
        } else {
            return PubFun1.CreateMaxNo("GETNO", null);
        }
    }

    /**
     * 修改LCISPayGet中的状态 State ='2' 已经反冲。
     */
    private void changeState(String BatchaNo)
    {
        String updateSQL =" update LCISPayGet set state ='2' ,modifydate = current date,modifytime = current time "
                         +" ,recoildate = current date "
                         +" where batchno='"+BatchaNo+"'";
        mMap.put(updateSQL,SysConst.UPDATE);
    }
}
