package com.sinosoft.lis.coinsurance;

/**
 * <p>Title: LCISPullBL</p>
 * <p>Description: 共保抽档</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LCISPullBL {
    public LCISPullBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCISPayGetSet mLCISPayGetSet = new LCISPayGetSet();
    private LCISPayGetSchema mLCISPayGetSchema = new LCISPayGetSchema();
    private MMap mMap = new MMap();
    private String mOperate = "";
    private String mDate = "";
    private String mTime = "";

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        // 进行数据处理
        if (!dealData()) {
            return false;
        }
        
        mInputData = null;

        return true;
    }

    /**
     * 取得传入的数据
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLCISPayGetSet = (LCISPayGetSet) pmInputData.getObjectByObjectName(
                "LCISPayGetSet", 0);
        if (mLCISPayGetSet == null) {
            CError.buildErr(this, "没有得到足够的数据，请选择抽档类型!");
            return false;
        }

        if ("05".equals(mLCISPayGetSet.get(1).getOtherNoType())) {
            LCISPayGetSchema aLCISPayGetSchema = new LCISPayGetSchema();
            aLCISPayGetSchema.setAgentCom(mLCISPayGetSet.get(1).getAgentCom());
            aLCISPayGetSchema.setMakeDate(mLCISPayGetSet.get(1).getMakeDate());
            aLCISPayGetSchema.setOtherNoType("06");
            mLCISPayGetSet.add(aLCISPayGetSchema);
        }

        mDate = PubFun.getCurrentDate();
        mTime = PubFun.getCurrentTime();

        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData() {
        if ("INSERT".equals(mOperate)) {
            if (!pullData()) {
                return false;
            }
        }

        if ("CONFIRM".equals(mOperate)) {
            if (!confirm()) {
                return false;
            }
            
            //准备给后台的数据
            if (!prepareOutputData()) {
                return false;
            }

            System.out.println("after prepareOutputData");

            //数据提交
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "ICaseCureBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        return true;
    }

    private boolean confirm() {
        System.out.println("抽档确认");

        for (int i = 1; i <= mLCISPayGetSet.size(); i++) {
            String tBatchNo = mLCISPayGetSet.get(i).getBatchNo();
            System.out.println("抽档号：" + tBatchNo);
            
            if ("".equals(tBatchNo)||tBatchNo==null) {
            	continue;
            }

            String tSQL = "select * from lcispayget where batchno='" + tBatchNo +
                          "' fetch first 1 rows only ";
            LCISPayGetDB tLCISPayGetDB = new LCISPayGetDB();
            LCISPayGetSet aLCISPayGetSet = tLCISPayGetDB.executeQuery(tSQL);
            LCISPayGetSet tLCISPayGetSet = new LCISPayGetSet();
            for (int j = 1; j <= aLCISPayGetSet.size(); j++) {
                LCISPayGetSchema tLCISPayGetSchema = aLCISPayGetSet.get(j);
                //抽档确认
                if ("1".equals(tLCISPayGetSchema.getState())) {
                    CError.buildErr(this, "该数据已确认！");
                    return false;
                }

//                tLCISPayGetSchema.setState("1");
//                tLCISPayGetSchema.setConfirmDate(mDate);
//                tLCISPayGetSchema.setConfirmOperator(mGlobalInput.Operator);
//                tLCISPayGetSchema.setOperator(mGlobalInput.Operator);
//                tLCISPayGetSchema.setManageCom(mGlobalInput.ManageCom);
//                tLCISPayGetSchema.setModifyDate(mDate);
//                tLCISPayGetSchema.setModifyTime(mTime);
//                tLCISPayGetSet.add(tLCISPayGetSchema);
            }
//            mMap.put(tLCISPayGetSet, "UPDATE");
            String updateSQL =" update LCISPayGet set state ='1' ,modifydate = current date,modifytime = current time "
                +" ,ConfirmDate = current date,ConfirmOperator='"+mGlobalInput.Operator+"' "
                +" where batchno='"+tBatchNo+"'";
            mMap.put(updateSQL,SysConst.UPDATE);
        }
        return true;
    }

    private boolean pullData() {
    	lockData();
        System.out.println("开始抽档");

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();

//        //查询共保机构
//        String tAgentSQL = "select distinct b.agentcom from "
//                           + " LCIGrpCont a,LCCoInsuranceParam b,LCGrpCont c "
//                           +
//                           " where a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno "
//                           + " and c.appflag='1' and a.state='1'"
//                           + " union select distinct b.agentcom from "
//                           + " LCIGrpCont a,LCCoInsuranceParam b,LBGrpCont c "
//                           +
//                           " where a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno "
//                           + " and c.appflag='1' and a.state='1'";
//        SSRS tAgentSSRS = tExeSQL.execSQL(tAgentSQL);
//
//        for (int a = 1; a <= tAgentSSRS.getMaxRow(); a++) {
//            String tAgentCom = tAgentSSRS.GetText(a, 1);
//            System.out.println("共保机构：" + tAgentCom);

        //提数类型
        for (int i = 1; i <= mLCISPayGetSet.size(); i++) {
            mLCISPayGetSchema = mLCISPayGetSet.get(i);
            String tOtherNoType = mLCISPayGetSchema.getOtherNoType();
            System.out.println("抽档类型：" + tOtherNoType);
            String tAgentCom = mLCISPayGetSchema.getAgentCom();
            System.out.println("共保机构：" + tAgentCom);
            String tEndDate = mLCISPayGetSchema.getMakeDate();

            //共保保单
            String tGrpContSQL =
                    "select distinct a.grpcontno,b.rate from "
                    +
                    " LCIGrpCont a,LCCoInsuranceParam b,LCGrpCont c"
                    +
                    " where a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno"
                    +
                    " and c.appflag='1' and a.state='1' and b.agentcom='" +
                    tAgentCom + "' union "
                    +
                    "select distinct a.grpcontno,b.rate from "
                    +
                    " LCIGrpCont a,LCCoInsuranceParam b,LBGrpCont c"
                    +
                    " where a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno"
                    +
                    " and c.appflag='1' and a.state='1' and b.agentcom='" +
                    tAgentCom + "'";
            SSRS tGrpContSSRS = tExeSQL.execSQL(tGrpContSQL);

            for (int m = 1; m <= tGrpContSSRS.getMaxRow(); m++) {
                //抽档号
                String tBatchNo = PubFun1.CreateMaxNo("CIACCOUNT", "");
                System.out.println("抽档号" + tBatchNo);

                String tGrpContNo = tGrpContSSRS.GetText(m, 1);
                double tRate = Double.parseDouble(tGrpContSSRS.GetText(m, 2));

                String tSQL = "";
                String tTBSQL =
                        "select b.prtno,'TB',a.payno,"
                        +
                        //处理白条的情况，从实际收了的钱里面推算共保金额，按ljapaygrp中险种金额的比例
                        " round(sum(X.PAYMONEY)*sum(a.sumactupaymoney)/c.sumactupaymoney,2),"
                        + " a.grppolno,a.riskcode "
                        + " from ljapaygrp a,LCIGrpCont b,ljapay c, "
                        + " (select d.otherno OTHERNO,sum(d.paymoney) PAYMONEY from ljtempfee d,ljtempfeeclass f "
                        + " where d.tempfeeno=f.tempfeeno "
                        +
                        " and not exists(select 1 from ljspayb where f.tempfeeno = ljspayb.getnoticeno "
                        + " and dealstate ='1') group by OTHERNO) AS X "
                        + " where a.grpcontno=b.grpcontno and a.grpcontno='"
                        + tGrpContNo
                        + "' and X.OTHERNO=a.grpcontno "
                        + " and c.payno = a.payno and a.Sumduepaymoney<>0 " +
                        " and c.incometype='1' and c.DueFeeType='0' " +
                        " and a.paycount=1 and a.paytype='ZC' and a.confdate<='" +
                        tEndDate +
                        "' and not exists (select 1 from lcispayget where " +
                        " grpcontno = a.grpcontno and getnoticeno=a.payno and agentcom='" +
                        tAgentCom +
                        "' and state<>'0') group by b.prtno,a.payno,a.grppolno,a.riskcode,c.sumactupaymoney ";

                String tXQSQL =
                        "select a.payno,'XQ',a.getnoticeno,sum(a.sumactupaymoney),a.grppolno,a.riskcode "
                        +
                        " from ljapaygrp a,ljapay b where a.payno = b.payno "
                        + " and a.paytype='ZC' and a.confdate<='" + tEndDate +
                        "' and a.sumduepaymoney<>0 and b.incometype='1' and b.DueFeeType='1' "
//                        +
//                        " and not exists (select 1 from ljtempfee d,ljtempfeeclass f,ljspayb s "
//                        +
//                        " where d.tempfeeno=f.tempfeeno and f.paymode='10' "
//                        + " and f.tempfeeno=s.getnoticeno and s.dealstate='1' and b.getnoticeno=d.tempfeeno) "
                        + " and a.grpcontno='"
                        + tGrpContNo +
                        "' and not exists (select 1 from lcispayget where "
                        +
                        " otherno=a.payno and getnoticeno=a.getnoticeno and grpcontno=a.grpcontno "
                        + " and agentcom='" + tAgentCom +
                        "' and state<>'0' )"
                        +
                        " group by a.payno,a.getnoticeno,a.grppolno,a.riskcode ";

                String tBQSQL =
                        "select a.endorsementno,'BQ',a.actugetno,sum(a.getmoney),a.grppolno,a.riskcode "
                        + " from ljagetendorse a where a.grpcontno='"
                        + tGrpContNo +
                        "' and a.othernotype='3' and a.makedate<='" + tEndDate +
                        "' and not exists (select 1 from ljapay p,ljtempfee d,ljtempfeeclass f where payno=a.actugetno and incomeno=a.endorsementno "
                        + " and d.tempfeeno=f.tempfeeno and d.otherno=a.endorsementno and f.paymode='10') " +
                        " and not exists (select 1 from ljaget where actugetno=a.actugetno and otherno=a.endorsementno and paymode='B') " +
                        " and not exists (select 1 from lcispayget where "
                        +
                        " otherno=a.endorsementno and getnoticeno=a.actugetno and grpcontno=a.grpcontno "
                        + " and agentcom='" + tAgentCom +
                        "' and state<>'0' )"
                        +
                        " group by a.endorsementno,a.actugetno,a.grppolno,a.riskcode ";

                String tLPSQL =
                        "select a.otherno,a.othernotype,a.actugetno,-sum(a.pay),a.grppolno,a.riskcode "
                        + " from ljagetclaim a where a.grpcontno='" +
                        tGrpContNo
                        +
                        "' and a.othernotype = '5' and a.makedate<='" +
                        tEndDate +
                        "' and not exists (select 1 from lcispayget where "
                        +
                        " otherno=a.otherno and othernotype=a.othernotype "
                        + " and getnoticeno=a.actugetno and agentcom='" +
                        tAgentCom + "' and state<>'0' and grpcontno=a.grpcontno ) "
                        +
                        " group by a.otherno, a.othernotype,a.actugetno,a.grppolno,a.riskcode ";
                
                String tLPCSQL =
                    "select a.otherno,a.othernotype,a.actugetno,-sum(a.pay),a.grppolno,a.riskcode "
                    + " from ljagetclaim a where a.grpcontno='" +
                    tGrpContNo
                    +
                    "' and a.othernotype = 'C' and a.makedate<='" +
                    tEndDate +
                    "' and not exists (select 1 from lcispayget where "
                    +
                    " otherno=a.otherno and othernotype=a.othernotype "
                    + " and getnoticeno=a.actugetno and agentcom='" +
                    tAgentCom + "' and state<>'0'  and grpcontno=a.grpcontno ) "
                    +
                    " group by a.otherno, a.othernotype,a.actugetno,a.grppolno,a.riskcode ";

                String tLPFSQL =
                        "select a.otherno,a.othernotype,a.actugetno,-sum(a.pay),a.grppolno,a.riskcode "
                        + " from ljagetclaim a where a.grpcontno='" +
                        tGrpContNo
                        +
                        "' and a.othernotype = 'F' and a.makedate<='" +
                        tEndDate +
                        "' and not exists (select 1 from lcispayget where "
                        +
                        " otherno=a.otherno and othernotype=a.othernotype "
                        + " and getnoticeno=a.actugetno and agentcom='" +
                        tAgentCom + "' and state<>'0'  and grpcontno=a.grpcontno ) "
                        +
                        " group by a.otherno, a.othernotype,a.actugetno,a.grppolno,a.riskcode ";

                String tType = "";

                if ("01".equals(tOtherNoType)) {
                    tSQL = tTBSQL;
                    tType = "TB";
                } else if ("02".equals(tOtherNoType)) {
                    tSQL = tXQSQL;
                    tType = "XQ";
                } else if ("03".equals(tOtherNoType)) {
                    tSQL = tBQSQL;
                    tType = "BQ";
                } else if ("04".equals(tOtherNoType)) {
                    tSQL = tLPSQL;
                    tType = "5";
                } else if ("05".equals(tOtherNoType)) {
                    tSQL = tLPCSQL;
                    tType = "C";
                } else if ("06".equals(tOtherNoType)) {
                    tSQL = tLPFSQL;
                    tType = "F";
                } else {
                    CError.buildErr(this, "抽档类型错误!");
                    return false;
                }

                String tDelSQL =
                        "delete from lcispayget where state='0' and grpcontno='" +
                        tGrpContNo + "' and agentcom='" +
                        tAgentCom + "' and othernotype ='" + tType + "'";
                MMap tMap = new MMap();
                tMap.put(tDelSQL, "DELETE");
                mInputData.clear();
                mInputData.add(tMap);
                //数据提交
                PubSubmit tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, mOperate)) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ICaseCureBL";
                    tError.functionName = "submitData";
                    tError.errorMessage = "数据提交失败!";
                    delLock();
                    this.mErrors.addOneError(tError);
                    return false;
                }

                System.out.println("提数SQL:" + tSQL);

//                tSSRS = tExeSQL.execSQL(tSQL);
                RSWrapper tRSWrapper = new RSWrapper();
                tRSWrapper.prepareData(null, tSQL);
                try
                {
                    do
                    {
                    	tSSRS = tRSWrapper.getSSRS();
                        if(tSSRS!=null && tSSRS.getMaxRow()>0){
                        	LCISPayGetSet tLCISPayGetSet = new LCISPayGetSet();

                            for (int j = 1; j <= tSSRS.getMaxRow(); j++) {
                                LCISPayGetSchema aLCISPayGetSchema = new
                                        LCISPayGetSchema();
                                aLCISPayGetSchema.setBatchNo(tBatchNo);
                                aLCISPayGetSchema.setAgentCom(tAgentCom);
                                aLCISPayGetSchema.setOtherNo(tSSRS.GetText(j, 1));
                                aLCISPayGetSchema.setOtherNoType(tSSRS.GetText(j, 2));
                                aLCISPayGetSchema.setGetNoticeNo(tSSRS.GetText(j, 3));
                                double tSumMoney = Arith.round(Double.parseDouble(tSSRS.
                                        GetText(j, 4)), 2);
                                aLCISPayGetSchema.setSumActuMoney(tSumMoney);
                                aLCISPayGetSchema.setRate(tRate);
                                double tClaimMoney = Arith.round(tSumMoney * tRate, 2);
                                aLCISPayGetSchema.setClaimMoney(tClaimMoney);
                                aLCISPayGetSchema.setGrpContNo(tGrpContNo);
                                aLCISPayGetSchema.setGrpPolNo(tSSRS.GetText(j, 5));
                                aLCISPayGetSchema.setRiskCode(tSSRS.GetText(j, 6));
                                //状态：待确认
                                aLCISPayGetSchema.setState("0");
                                aLCISPayGetSchema.setPullDate(mDate);
                                aLCISPayGetSchema.setPullOperator(mGlobalInput.Operator);
                                aLCISPayGetSchema.setOperator(mGlobalInput.Operator);
                                aLCISPayGetSchema.setManageCom(mGlobalInput.ManageCom);
                                aLCISPayGetSchema.setMakeDate(mDate);
                                aLCISPayGetSchema.setMakeTime(mTime);
                                aLCISPayGetSchema.setModifyDate(mDate);
                                aLCISPayGetSchema.setModifyTime(mTime);
                                tLCISPayGetSet.add(aLCISPayGetSchema);
                                
                            }
                            tMap = new MMap();
                            tMap.put(tLCISPayGetSet, "INSERT");
                            mInputData.clear();
                            mInputData.add(tMap);
                            System.out.println("after prepareOutputData ------提交保单:"+tGrpContNo+"结束-------");

                            //数据提交
                            tPubSubmit = new PubSubmit();
                            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                                CError tError = new CError();
                                tError.moduleName = "ICaseCureBL";
                                tError.functionName = "submitData";
                                tError.errorMessage = "数据提交失败!";
                                delLock();
                                this.mErrors.addOneError(tError);
                                return false;
                            }
                        }
                    }
                    while (tSSRS!=null && tSSRS.getMaxRow() > 0);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    tRSWrapper.close();
                }
                try
                {
                    tRSWrapper.close();
                }
                catch (Exception ex)
                {
                }
                
            }
            delLock();
        }
//        }
        return true;
    }

/***
 * lock  zai locktable 表中添加抽档记录
 * @return
 */
	private boolean lockData() {
    	
    	String tAgentCom = mLCISPayGetSet.get(1).getAgentCom();
    	
        LockTableSet tLockTableSet = new LockTableSet();
        LockTableSchema tLockTableSchema = new LockTableSchema();
        tLockTableSchema.setNoType("PL");
        tLockTableSchema.setNoLimit(tAgentCom);
        tLockTableSchema.setMakeDate(mDate);
        tLockTableSchema.setMakeTime(mTime);
        tLockTableSet.add(tLockTableSchema);
        MMap tMap = new MMap();
        tMap.put(tLockTableSet,"INSERT"); 
        mInputData.clear();
        mInputData.add(tMap);
        System.out.println("添加锁表记录sql");
        
        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
		return false;
	}
    
    /***
     *删除抽档记录，保证可以重复抽档 
     */
	 private boolean delLock() {
	    	String tAgentCom = mLCISPayGetSet.get(1).getAgentCom();
	    	 String tDelSQL =
	                 "delete from locktable where notype = 'PL' and nolimit='" +
	                		 tAgentCom + "'";
	         MMap tMap = new MMap();
	         tMap.put(tDelSQL, "DELETE");
	         mInputData.clear();
	         mInputData.add(tMap);
	         //数据提交  
	         PubSubmit tPubSubmit = new PubSubmit();
	         if (!tPubSubmit.submitData(mInputData, mOperate)) {
	             // @@错误处理
	             this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	             CError tError = new CError();
	             tError.moduleName = "ICaseCureBL";
	             tError.functionName = "submitData";
	             tError.errorMessage = "数据删除失败!";
	             this.mErrors.addOneError(tError);
	             return false;
	         }
	    	return false;
		}


    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCISPullBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错:" +
                                  ex.toString();
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }
}
