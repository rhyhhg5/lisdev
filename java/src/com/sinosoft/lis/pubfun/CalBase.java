/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;


/**
 * <p>ClassName: CalBase </p>
 * <p>Description: 计算基础要素类文件 </p>
 * <p>Description: 所有方法均是为内部成员变量存取值，set开头为存，get开头为取 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: HST
 * @CreateDate：2002-07-30
 */
public class CalBase {
    // @Field
    /** 保费 */
    private double Prem;

    /** 保额 */
    private double Get;

    /** 份数 */
    private double Mult;

    /** 缴费间隔 */
    private int PayIntv;

    /** 领取间隔 */
    private int GetIntv;

    /** 缴费终止年期或年龄 */
    private int PayEndYear;

    /** 缴费终止年期或年龄标记 */
    private String PayEndYearFlag;

    /** 领取开始年期或年龄 */
    private int GetYear;

    /** 领取开始年期或年龄标记 */
    private String GetYearFlag;

    /** 责任领取类型 */
    private String GetDutyKind;

    /** 起领日期计算参照 */
    private String StartDateCalRef;

    /** 保险期间 */
    private int Years;

    /** 保险期间 */
    private int InsuYear;

    /** 保险期间标记 */
    private String InsuYearFlag;

    /** 被保人投保年龄 */
    private int AppAge;

    /** 被保人性别 */
    private String Sex;

    /** 被保人工种 */
    private String Job;

    /** 责任给付编码 */
    private String GDuty;

    /** 投保人数 */
    private int Count;

    /** 续保次数 */
    private int RnewFlag;

    /** 递增率 */
    private double AddRate;

    /** 个单合同号 */
    private String ContNo;

    /** 保单号 */
    private String PolNo;

    /** 原保额 */
    private double Amnt;

    /** 浮动费率 */
    private double FloatRate;

    /**险种编码*/
    private String RiskCode;

    /**个人保单表中用来做传递计算变量的公用元素--团体商业补充保险中传递责任选择的组合代码*/
    private String StandbyFlag1;

    /**待用*/
    private String StandbyFlag2;

    /**团体商业补充保险中传递在职或退休字段*/
    private String StandbyFlag3;

    /**集体合同号*/
    private String GrpContNo;

    /**集体保单号*/
    private String GrpPolNo;

    private String EdorNo;

    /**计算类型*/
    private String CalType;

    /**起付线**/
    private double GetLimit;

    /**赔付比例**/
    private double GetRate;

    /**社保标记**/
    private String SSFlag;

    /**封顶线**/
    private double PeakLine;

    /**保单生效日**/
    private String CValiDate;

    /**保单失效日**/
    private String CInValiDate;

    /**保单失效日**/
    private String CalRule;

    /**原交至日期*/
    private String LastPayToDate;

    /**现交至日期*/
    private String PayToDate;

    /**保全增人非一年期险保费还原标志*/
    private String RevertType;

    /**
     * 保全生效日期
     * 此处存储不一定是保全生效日期,在PICC中用保全申请时期
     * 作为保全增人的保险期间的起始日期
     */
    private String EdorValiDate;

    /** ComCode */
    private String ComCode;
    /** 行业大类 */
    private String Industry;
    /** 在职退休比例 */
    private String RetireRate;
    /** 平均年龄 */
    private String AvgAge;
    /** 参保规模 */
    private String InsuredNum;
    /** 份数 */
    private double Copys;
    /** 在职/退休  1在职 2退休 */
    private String InsuredState;
    /** 万能上一个月月末的账户价值*/
    private String LastBala ;
    /** 税优折扣因子 */
    private String Totaldiscountfactor;
    /** 国家系数 */
    private String CountryFactor;
    /** 工程系数 */
    private String EngineeringFactor;
    /** 规模系数 */
	private String ScaleFactor;
	/** 平均年龄 */
	private String Avage;
	/** 保险区间 */
	private String Insureyear;
    
    
    public String getCountryFactor() {
		return CountryFactor;
	}


	public void setCountryFactor(String countryFactor) {
		CountryFactor = countryFactor;
	}


	public String getEngineeringFactor() {
		return EngineeringFactor;
	}


	public void setEngineeringFactor(String engineeringFactor) {
		EngineeringFactor = engineeringFactor;
	}


	public String getScaleFactor() {
		return ScaleFactor;
	}


	public void setScaleFactor(String scaleFactor) {
		ScaleFactor = scaleFactor;
	}


	public String getAvage() {
		return Avage;
	}


	public void setAvage(String avage) {
		Avage = avage;
	}


	public String getInsureyear() {
		return Insureyear;
	}


	public void setInsureyear(String insureyear) {
		Insureyear = insureyear;
	}


	public String getTotaldiscountfactor() {
		return Totaldiscountfactor;
	}


	public void setTotaldiscountfactor(String totaldiscountfactor) {
		Totaldiscountfactor = totaldiscountfactor;
	}

	//保障计划编码
    private String ContPlanCode ;
    public String getContPlanCode() {
		return ContPlanCode;
	}


	public void setContPlanCode(String contPlanCode) {
		ContPlanCode = contPlanCode;
	}


	// @Constructor
    public CalBase() {}


    // @Method
    public void setContNo(String tContNo) {
        ContNo = tContNo;
    }

    public void setGrpContNo(String tGrpContNo) {
        GrpContNo = tGrpContNo;
    }

    public void setPrem(double tPrem) {
        Prem = tPrem;
    }

    public String getPrem() {
        return String.valueOf(Prem);
    }

    public void setGet(double tGet) {
        Get = tGet;
    }

    public String getGet() {
        return String.valueOf(Get);
    }

    public void setAmnt(double tAmnt) {
        Amnt = tAmnt;
    }

    public String getAmnt() {
        return String.valueOf(Amnt);
    }

    public void setMult(double tMult) {
        Mult = tMult;
    }

    public String getMult() {
        return String.valueOf(Mult);
    }

    public void setFloatRate(double tFloatRate) {
        FloatRate = tFloatRate;
    }

    public String getFloatRate() {
        return String.valueOf(FloatRate);
    }

    public void setAddRate(double tAddRate) {
        AddRate = tAddRate;
    }

    public String getContNo() {
        return ContNo;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public String getAddRate() {
        return String.valueOf(AddRate);
    }

    public void setPayIntv(int tPayIntv) {
        PayIntv = tPayIntv;
    }

    public String getPayIntv() {
        return String.valueOf(PayIntv);
    }

    public void setGetIntv(int tGetIntv) {
        GetIntv = tGetIntv;
    }

    public String getGetIntv() {
        return String.valueOf(GetIntv);
    }

    public void setPayEndYear(int tPayEndYear) {
        PayEndYear = tPayEndYear;
    }

    public String getPayEndYear() {
        return String.valueOf(PayEndYear);
    }

    public void setGetYear(int tGetYear) {
        GetYear = tGetYear;
    }

    public String getGetYear() {
        return String.valueOf(GetYear);
    }

    public void setYears(int tYears) {
        Years = tYears;
    }

    public String getYears() {
        return String.valueOf(Years);
    }

    public void setInsuYear(int tInsuYear) {
        InsuYear = tInsuYear;
    }

    public String getInsuYear() {
        return String.valueOf(InsuYear);
    }

    public void setAppAge(int tAppAge) {
        AppAge = tAppAge;
    }

    public String getAppAge() {
        return String.valueOf(AppAge);
    }

    public void setCount(int tCount) {
        Count = tCount;
    }

    public String getCount() {
        return String.valueOf(Count);
    }

    public void setRnewFlag(int tRnewFlag) {
        RnewFlag = tRnewFlag;
    }

    public String getRnewFlag() {
        return String.valueOf(RnewFlag);
    }

    public void setSex(String tSex) {
        Sex = tSex;
    }

    public String getSex() {
        return Sex;
    }

    public void setInsuYearFlag(String tInsuYearFlag) {
        InsuYearFlag = tInsuYearFlag;
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }

    public void setPayEndYearFlag(String tPayEndYearFlag) {
        PayEndYearFlag = tPayEndYearFlag;
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }

    public void setGetYearFlag(String tGetYearFlag) {
        GetYearFlag = tGetYearFlag;
    }

    public String getGetYearFlag() {
        return GetYearFlag;
    }

    public void setGetDutyKind(String tGetDutyKind) {
        GetDutyKind = tGetDutyKind;
    }

    public String getGetDutyKind() {
        return GetDutyKind;
    }

    public void setStartDateCalRef(String tStartDateCalRef) {
        StartDateCalRef = tStartDateCalRef;
    }

    public String getStartDateCalRef() {
        return StartDateCalRef;
    }

    public void setJob(String tJob) {
        Job = tJob;
    }

    public String getJob() {
        return Job;
    }

    public void setGDuty(String tGDuty) {
        GDuty = tGDuty;
    }

    public String getGDuty() {
        return GDuty;
    }

    public void setPolNo(String tPolNo) {
        PolNo = tPolNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setRiskCode(String tRiskCode) {
        RiskCode = tRiskCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setStandbyFlag1(String tStandbyFlag1) {
        StandbyFlag1 = tStandbyFlag1;
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }

    public void setStandbyFlag2(String tStandbyFlag2) {
        StandbyFlag2 = tStandbyFlag2;
    }

    public String getStandbyFlag2() {
        return StandbyFlag2;
    }

    public void setStandbyFlag3(String tStandbyFlag3) {
        StandbyFlag3 = tStandbyFlag3;
    }

    public String getStandbyFlag3() {
        return StandbyFlag3;
    }

    public void setGrpPolNo(String tGrpPolNo) {
        GrpPolNo = tGrpPolNo;
    }

    public String getGrpPolNo() {
        return GrpPolNo;
    }

    public void setEdorNo(String tEdorNo) {
        EdorNo = tEdorNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setCalType(String tCalType) {
        CalType = tCalType;
    }

    public String getCalType() {
        return CalType;
    }

    public void setGetLimit(double tGetLimit) {
        GetLimit = tGetLimit;
    }

    public String getGetLimit() {
        return String.valueOf(GetLimit);
    }

    public void setGetRate(double tGetRate) {
        GetRate = tGetRate;
    }

    public String getGetRate() {
        return String.valueOf(GetRate);
    }

    public void setSSFlag(String tSSFlag) {
        SSFlag = tSSFlag;
    }

    public String getSSFlag() {
        return SSFlag;
    }

    public void setPeakLine(double tPeakLine) {
        PeakLine = tPeakLine;
    }

    public String getPeakLine() {
        return String.valueOf(PeakLine);
    }

    public void setCValiDate(String tCValiDate) {
        CValiDate = tCValiDate;
    }

    public String getCValiDate() {
        return CValiDate;
    }

    public void setCInValiDate(String tCInValiDate) {
        CInValiDate = tCInValiDate;
    }

    public String getCInValiDate() {
        return CInValiDate;
    }

    public void setEdorValiDate(String tEdorValiDate) {
        EdorValiDate = tEdorValiDate;
    }

    public String getEdorValiDate() {
        return EdorValiDate;
    }

    public void setCalRule(String tCalRule) {
        CalRule = tCalRule;
    }

    public String getCalRule() {
        return CalRule;
    }

    public void setLastPayToDate(String tLastPayToDate) {
        LastPayToDate = tLastPayToDate;
    }

    public String getLastPayToDate() {
        return LastPayToDate;
    }

    public void setPayToDate(String tPayToDate) {
        PayToDate = tPayToDate;
    }

    public String getPayToDate() {
        return PayToDate;
    }

    public void setRevertType(String tRevertType) {
        RevertType = tRevertType;
    }

    public String getRevertType() {
        return RevertType;
    }

    public void setComCode(String ComCode) {
        this.ComCode = ComCode;
    }

    public String getComCode() {
        return ComCode;
    }

    public void setIndustry(String Industry) {
        this.Industry = Industry;
    }

    public String getIndustry() {
        return Industry;
    }

    public void setInsuredNum(String InsuredNum) {
        this.InsuredNum = InsuredNum;
    }

    public String getInsuredNum() {
        return InsuredNum;
    }

    public void setAvgAge(String AvgAge) {
        this.AvgAge = AvgAge;
    }

    public String getAvgAge() {
        return AvgAge;
    }

    public void setRetireRate(String RetireRate) {
        this.RetireRate = RetireRate;
    }

    public String getRetireRate() {
        return RetireRate;
    }

    public void setCopys(double Copys) {
        this.Copys = Copys;
    }

    public String getCopys() {
        return String.valueOf(Copys);
    }

    public void setInsuredState(String InsuredState) {
        this.InsuredState = InsuredState;
    }

    public String getInsuredState() {
        return InsuredState;
    }

    public void setLastBala(String LastBala)
    {
        this.LastBala = LastBala;
    }

    public String getLastBala(){
        return LastBala;
    }
}
