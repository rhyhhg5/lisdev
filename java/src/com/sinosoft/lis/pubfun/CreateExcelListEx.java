package com.sinosoft.lis.pubfun;

import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;

/**
 * 生成Excel报表拓展类
 * 
 * @author zhangcx
 *
 */
public class CreateExcelListEx extends CreateExcelList {

	public CreateExcelListEx(String fileName) {
		super(fileName);
	}

	/**
	 * 通过sql打印清单，自动增加序号列
	 * 
	 * @param title
	 *            表头
	 * @param aQuerySql
	 *            String 指定查询SQL
	 * @param aColAttribute
	 *            int[] 指定对应列是否显示在清单中，0不显示,其它值为显示列。第一列为1
	 * @return int 成功返回当前excel行数，否则为-1
	 */
	public int setDataAddNo(String[] title, String aQuerySql,
			int[] aColAttribute) {
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(aQuerySql);
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			return -1;
		}
		String[][] tData = tSSRS.getAllData();
		return setDataAddNo(title, tData, aColAttribute);
	}

	/**
	 * 通过数据生成Excel清单，自动添加序号列
	 * 
	 * @param title
	 *            表头
	 * @param RowColData
	 *            String[][] 指定数据
	 * @param aColAttribute
	 *            int[] 指定对应列是否显示在清单中，0不显示,其它值为显示列。第一列为1
	 * @return int 成功返回当前excel行数，否则为-1
	 */
	public int setDataAddNo(String[] title, String[][] aRowColData,
			int[] aColAttribute) {
		// 添加表头
		String[][] tTitle = new String[1][];
		tTitle[0] = title;
		String[][] tTitleData = dataFetch(tTitle, aColAttribute);
		// 自动添加序号列
		setData(0, 0, 0, "序号");
		// 添加表头
		for (int j = 0; j < tTitleData[0].length; j++) {
			setData(0, 0, j + 1, tTitleData[0][j]);
		}
		// 设置偏移量
		int mRowOffset = 1;
		
		String[][] tData = dataFetch(aRowColData, aColAttribute);
		
		// 添加序号
		for (int i = 0; i < tData.length; i++) {
			setData(0, mRowOffset + i, 0, (i + 1) + "");
		}
		// 添加内容
		for (int i = 0; i < tData.length; i++) {
			for (int j = 0; j < tData[i].length; j++) {
				setData(0, mRowOffset + i, j + 1, tData[i][j]);
			}
		}
		mRowOffset += tData.length;
		return mRowOffset;
	}

	private String[][] dataFetch(String[][] aOriginalData, int[] aColAttribute) {
		int length = aColAttribute.length;
		for (int i = 0; i < aColAttribute.length; i++) {
			if (aColAttribute[i] == 0) {
				length--;
			}
		}
		String[][] t = new String[aOriginalData.length][length];
		for (int i = 0; i < aOriginalData.length; i++) {
			for (int j = 0; j < aColAttribute.length; j++) {
				if (aColAttribute[j] == 0)
					continue;
				else
					t[i][aColAttribute[j] - 1] = StrTool
							.cTrim(aOriginalData[i][j]);
			}
		}
		return t;
	}

	public static void main(String[] args) {
		int[] displayData = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		String querySql = "select trim(prtno)||'801',prtno,'新单交费',(case paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转账支票' when '5' then '内部转账' when '11' then '银行汇款' when '12' then '银行代收' end),prem,bankcode,bankaccno,accname,appntname,AppntNo from lccont  where uwflag in ('4','9') and Conttype = '1'  and GrpContNo = '00000000000000000000' and CardFlag = '0' and appflag in ('0', '9') and prem<>0 and signdate is null and (select count(1) from lcrnewstatelog where prtno=lccont.prtno)=0 and paymode  in ('1','2','3','5','6','11','12')   and managecom like '86%' and makedate>='2012-08-01' and makedate<='2012-08-21' and not exists (select 1 from ljtempfee where otherno=lccont.prtno and othernotype = '4' ) union all select trim(prtno)||'801',prtno,'新单交费',(case paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转账支票' when '5' then '内部转账' when '11' then '银行汇款' when '12' then '银行代收' end),prem, '', '', '',grpname,AppntNo from lcgrpcont  where uwflag in ('4','9') and appflag in ('0', '9') and (cardflag is null or cardflag='4') and prem<>0 and signdate is null and (select count(1) from lcrnewstatelog where prtno=lcgrpcont.prtno)=0 and paymode in ('1','2','3','5','6','11','12') and managecom like '86%' and makedate>='2012-08-01' and makedate<='2012-08-21' and not exists (select 1 from ljtempfee where otherno=lcgrpcont.prtno and othernotype = '5') with ur";
		String[] tTitle = new String[]{ "暂收费号", "关联号码", "号码类型",
				"收费方式", "交费金额", "银行编码", "帐号", "账户名", "投保人", "投保人编号" };

		CreateExcelListEx createexcellist = new CreateExcelListEx("");// 指定文件名
		createexcellist.createExcelFile();
		String[] sheetName = { "list" };
		createexcellist.addSheet(sheetName);
		createexcellist.setDataAddNo(tTitle,querySql, displayData);
		try {
			createexcellist.write("f:\\zzz.xls");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
