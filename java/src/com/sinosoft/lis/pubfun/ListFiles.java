package com.sinosoft.lis.pubfun;

import java.util.List;
import java.util.ArrayList;
import java.io.File;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ListFiles {
    private File mFile = null;
    public CErrors mErrors = new CErrors();
    List mFilePaths = new ArrayList();
    public ListFiles() {
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "ListDirs";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public boolean submitData(String DirPath, String Type) {
        boolean treturn = true;
        mFile = new File(DirPath);
        if (!mFile.exists()) {
            treturn = false;
        }
        if (Type.equals("RootFiles")) {
            return ListRootFiles(DirPath);
        }
        if (Type.equals("AllFiles")) {
            return ListAllFiles(DirPath);
        }
        if (!Type.equals("AllFiles") || !Type.equals("AllFiles")) {
            treturn = false;
            System.out.println("选择类型应为根目录文件（RootFiles）或者全部文件（AllFiles）");
        }

        return treturn;
    }

    public boolean ListRootFiles(String DirPath) {
        boolean treturn = true;
        String strPath = DirPath;
        try {
            File f = new File(strPath);
            if (f.isDirectory()) {
                File[] fList = f.listFiles();
                for (int j = 0; j < fList.length; j++) {
                    if (fList[j].isFile()) {
                        mFilePaths.add(fList[j].getPath());
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error： " + e);
        }
        return treturn;
    }

    public boolean ListAllFiles(String DirPath) {
        boolean treturn = true;
        String strPath = DirPath;
        try {
            File f = new File(strPath);
            if (f.isDirectory()) {
                File[] fList = f.listFiles();
                for (int j = 0; j < fList.length; j++) {
                    if (fList[j].isDirectory()) {
                        ListAllFiles(fList[j].getPath()); //在getDir函数里面又调用了getDir函数本身
                    }
                }
                for (int j = 0; j < fList.length; j++) {

                    if (fList[j].isFile()) {
                        mFilePaths.add(fList[j].getPath());
                    }

                }
            }
        } catch (Exception e) {
            System.out.println("Error： " + e);
        }
        return treturn;
    }

    public String[] getFilePaths() {
        return (String[]) mFilePaths.toArray(new String[0]);
    }

    public static void main(String[] args) {
        String strPath = "d:\\10.10";
        ListFiles tListDirsOrFiles = new ListFiles();
        if (tListDirsOrFiles.submitData(strPath, "AllFiles")) {
            String[] mfiles = tListDirsOrFiles.getFilePaths();
            for (int i = 0; i < mfiles.length; i++) {
                System.out.println(mfiles[i]);
            }
        }

    }

}
