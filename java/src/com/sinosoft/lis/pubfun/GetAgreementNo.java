package com.sinosoft.lis.pubfun;

import com.sinosoft.utility.*;

public class GetAgreementNo
{
    public GetAgreementNo()
    {
    }
    
    public static String getAgreementNo(String paycode)
    {
        ExeSQL exeSql = new ExeSQL();
        SSRS testSSRS = new SSRS();
        String sql = "select c.OrganComCode,c.OtherCertificates from ljtempfee a,lcgrpcont b,lcgrpappnt c " +
                "where a.otherno=b.prtno and b.grpcontno=c.grpcontno and a.tempfeeno='"+ paycode.trim() + "' " +
                "union " +
                "select c.OrganComCode,c.OtherCertificates from ljtempfee a,ljsgetendorse b,lcgrpappnt c " +
                "where a.otherno=b.endorsementno and b.grpcontno=c.grpcontno and a.tempfeeno='"+ paycode.trim() + "'";
        testSSRS = exeSql.execSQL(sql);
        String agreementNo = testSSRS.GetText(1, 2);
        if (agreementNo == null || agreementNo.length() != 8) {
            agreementNo="00000000";
        }
        agreementNo = "22200010" + agreementNo;
        return agreementNo;
    }
    
    
    /**
     * 处理特殊字符·占两位的问题
     * @param value String
     * @return String
     */
    public static String getChinaLenOth(String value)
    {
        char[] arr = value.toCharArray();
        int result = 0;

        for (int i = 0; i < arr.length; i++)
        {
            if (arr[i] > 511)
            {
                System.out.println(arr[i]);
                result++;
            } else if (arr[i] == '·'){
                System.out.println(arr[i]);
                result++;
            }
        }

        return result + "";
    }


    public static void main(String[] str) {
        GetAgreementNo tGetAgreementNo = new GetAgreementNo();
        String s = tGetAgreementNo.getAgreementNo("99001300007801");
        System.out.println("s : "+s + " " +s.length());
    }
}
