package com.sinosoft.lis.pubfun;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.vschema.LDCode1Set;

/**
 * 邮件发送类，使用方式见main函数
 * 
 * @author 张成轩
 */
public class MailSender extends Authenticator {
	// 发送邮件的服务器的IP和端口
	private String mailServerHost;

	private String mailServerPort = "25";

	// 邮件发送者的地址
	private String fromAddress;

	// 邮件接收者的地址
	private String toAddresses;

	// 邮件抄送的地址
	private String ccAddresses;

	// 邮件暗送的地址
	private String bccAddresses;

	// 登陆邮件发送服务器的用户名和密码
	private String userName;
	private String password;

	// 邮件主题
	private String subject;

	// 邮件的文本内容
	private String content;

	// 邮件附件的文件名
	private String attachFilePaths;

	// 报错信息
	private String errorMessage = "";

	// 邮箱格式校验正则表达式
	private final String mailRegex = "^(([\\w.-])+@(([\\w-])+\\.)+\\w+;)*(([\\w.-])+@(([\\w-])+\\.)+\\w+;?)$";

	/**
	 * @return 报错信息
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * 构造方法，可以指定邮箱
	 * 
	 * @param userName
	 *            用户名
	 * @param password
	 *            密码
	 * @param mailType
	 *            邮箱类型
	 */
	public MailSender(String userName, String password, String mailType) {
		this.userName = userName;
		this.password = password;
		this.setMailType(mailType);
	}

	/**
	 * 构造方法，使用公司邮箱
	 * 
	 * @param userName
	 *            用户名
	 * @param password
	 *            密码
	 */
	public MailSender(String userName, String password) {
		this.userName = userName;
		this.password = password;
		this.setMailType(null);
	}

	/**
	 * 设置邮箱服务器，程序内部使用
	 * 
	 * @param mailType
	 *            邮箱类型
	 */
	private boolean setMailType(String mailType) {
		// 若有需要，可以改为数据库配置的方式
		if ("163".equals(mailType)) {
			System.out.println("--163邮箱--");
			// 163邮箱
			mailServerHost = "smtp.163.com";
			mailServerPort = "25";
			fromAddress = userName + "@163.com";
		} else if("picchealth".equals(mailType)){
			System.out.println("--PICCHEALTH邮箱--");
			mailServerHost = "10.252.36.9";
			mailServerPort = "25";
			fromAddress = userName + "@picchealth.com";
		}
		else {
			System.out.println("--公司邮箱--");
			// 公司邮箱
			mailServerHost = "mail.sinosoft.com.cn";
			mailServerPort = "25";
			fromAddress = userName + "@sinosoft.com.cn";
		}
		return true;
	}

	/**
	 * 设置邮件发送地址，通过数据库配置的方式 需要在ldcode1表中进行相关配置，参数为code的值，用于区分不同的功能使用
	 * 
	 * 数据库配置： ldcode1表 codetype=sendmail code自己配置 codename为邮箱地址
	 * othersign为发送(to)、抄送(cc)、暗送标记(bcc)，othersign若为空则为发送
	 * 
	 * @param code
	 *            提取地址标识
	 */
	public void setToAddress(String code) {
		if (code != null && !"".equals(code)) {
			// 从数据库中提取发送邮箱地址
			LDCode1DB tLDCode1DB = new LDCode1DB();
			tLDCode1DB.setCodeType("sendmail");
			tLDCode1DB.setCode(code);
			LDCode1Set tLDCode1Set = tLDCode1DB.query();
			// 发送地址
			String to = "";
			// 抄送地址
			String cc = "";
			// 暗送地址
			String bcc = "";

			for (int row = 1; row <= tLDCode1Set.size(); row++) {
				LDCode1Schema tLDCode1 = tLDCode1Set.get(row);
				if ("cc".equals(tLDCode1.getOtherSign())) {
					cc += tLDCode1.getCodeName() + ";";
				} else if ("bcc".equals(tLDCode1.getOtherSign())) {
					bcc += tLDCode1.getCodeName() + ";";
				} else {
					to += tLDCode1.getCodeName() + ";";
				}
			}
			setToAddress(to, cc, bcc);
		}
	}

	/**
	 * 设置邮箱地址
	 * 
	 * @param toAddresses
	 *            发送地址，多地址可以用;分隔
	 * @param ccAddresses
	 *            抄送地址，多地址可以用;分隔
	 * @param bccAddresses
	 *            暗送地址，多地址可以用;分隔
	 */
	public boolean setToAddress(String toAddresses, String ccAddresses,
			String bccAddresses) {
		if (!"".equals(toAddresses)) {
			this.toAddresses = toAddresses;
		} else {
			this.toAddresses = null;
		}
		if (!"".equals(ccAddresses)) {
			this.ccAddresses = ccAddresses;
		} else {
			this.ccAddresses = null;
		}
		if (!"".equals(bccAddresses)) {
			this.bccAddresses = bccAddresses;
		} else {
			this.bccAddresses = null;
		}
		return true;
	}

	/**
	 * 设置邮箱地址
	 * 
	 * @param subject
	 *            邮件标题
	 * @param content
	 *            邮件内容
	 * @param attachFilePaths
	 *            附件，多附件可以用|分隔
	 */
	public boolean setSendInf(String subject, String content,
			String attachFilePaths) {
		if (subject == null || subject.trim().equals("")) {
			this.subject = "未设置标题";
		} else {
			this.subject = subject;
		}
		if (content == null || content.trim().equals("")) {
			this.content = "未设置邮件内容";
		} else {
			this.content = content;
		}
		this.attachFilePaths = attachFilePaths;
		return true;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(userName, password);
	}

	/**
	 * 以HTML格式发送邮件
	 * 
	 * @param mailInfo
	 *            待发送的邮件信息
	 */
	public boolean sendMail() {
		System.out.println("^0^ -----开始发送邮件-----");
		if (!checkMailInfo()) {
			System.out.println("T_T -----邮件发送失败了-----" + errorMessage);
			return false;
		}
		Properties pro = getProperties();
		Session sendMailSession = Session.getDefaultInstance(pro, this);
		try {
			Message mailMessage = new MimeMessage(sendMailSession);
			Address from = new InternetAddress(this.fromAddress);
			mailMessage.setFrom(from);

			// 设置发送地址 to-发送 cc-抄送 bcc-暗送
			setAddresses(mailMessage, this.toAddresses, "to");
			setAddresses(mailMessage, this.ccAddresses, "cc");
			setAddresses(mailMessage, this.bccAddresses, "bcc");

			mailMessage.setSubject(this.subject);
			mailMessage.setSentDate(new Date());
			Multipart mainPart = new MimeMultipart();

			// 增加邮件内容
			BodyPart html = new MimeBodyPart();
			html.setContent(this.content, "text/html; charset=GBK");
			mainPart.addBodyPart(html);

			// 若存在附件，添加到mainPart中
			if (this.attachFilePaths != null
					&& !this.attachFilePaths.trim().equals("")) {
				String attachFilePaths[] = this.attachFilePaths.split("[|]");
				for (int num = 0; num < attachFilePaths.length; num++) {
					String filePath = attachFilePaths[num];
					if (!new File(filePath).exists()) {
						errorMessage = "文件" + filePath + "不存在";
						return false;
					}
					FileDataSource fds = new FileDataSource(filePath);
					BodyPart fileBodyPart = new MimeBodyPart();
					fileBodyPart.setDataHandler(new DataHandler(fds));

					String fileName = filePath.substring(Math.max(
							filePath.lastIndexOf("/"),
							filePath.lastIndexOf("\\")) + 1);

					fileBodyPart.setFileName(fileName);
					mainPart.addBodyPart(fileBodyPart);
				}
			}

			mailMessage.setContent(mainPart);
			Transport.send(mailMessage);
			System.out.println("^0^ -----邮件发送完毕-----");
			return true;
		} catch (AuthenticationFailedException afex) {
			errorMessage = "用户名密码错误";
			System.out.println("T_T -----邮件发送失败了-----" + errorMessage);
			return false;
		} catch (Exception ex) {
			if ("".equals(errorMessage)) {
				errorMessage = "发送邮件失败";
				System.out.println("邮件发送失败："+ex.toString());
    			ex.printStackTrace();
			}
			System.out.println("T_T -----邮件发送失败了-----" + errorMessage);
			return false;
		}

	}

	/**
	 * 校验邮件信息，程序内部使用
	 * 
	 * @param mailInfo
	 *            待发送的邮件信息
	 */
	private boolean checkMailInfo() {

		if (this.toAddresses == null && this.ccAddresses == null
				&& this.bccAddresses == null) {
			errorMessage = "发送、抄送、暗送地址不能同时为空";
			return false;
		}

		if (this.toAddresses != null) {
			if (!this.toAddresses.matches(mailRegex)) {
				errorMessage = "发送地址格式不正确";
				return false;
			}
		}

		if (this.ccAddresses != null) {
			if (!this.ccAddresses.matches(mailRegex)) {
				errorMessage = "抄送地址格式不正确";
				return false;
			}
		}

		if (this.bccAddresses != null) {
			if (!this.bccAddresses.matches(mailRegex)) {
				errorMessage = "暗送地址格式不正确";
				return false;
			}
		}

		if (this.subject == null || this.content == null) {
			errorMessage = "邮件标题、内容不能为空";
			return false;
		}

		if (this.mailServerHost == null || this.mailServerPort == null
				|| this.fromAddress == null) {
			errorMessage = "邮箱服务器配置异常";
			return false;
		}
		return true;
	}

	/**
	 * 添加邮件发送地址，程序内部使用
	 * 
	 * @param mailMessage
	 *            邮件发送信息
	 * @param addresses
	 *            发送地址字符串
	 * @param recipientType
	 *            发送类型 to-发送 cc-抄送 bcc-暗送
	 */
	private void setAddresses(Message mailMessage, String addresses,
			String recipientType) throws Exception {
		if (addresses != null && !addresses.trim().equals("")) {
			String toAddress[] = addresses.split("[;]");
			Address recipient[] = new Address[toAddress.length];
			for (int num = 0; num < toAddress.length; num++) {
				Address address = new InternetAddress(toAddress[num]);
				recipient[num] = address;
			}
			if (recipientType.equals("to")) {
				// 添加发送地址
				mailMessage.setRecipients(Message.RecipientType.TO, recipient);
			} else if (recipientType.equals("cc")) {
				// 添加抄送地址
				mailMessage.setRecipients(Message.RecipientType.CC, recipient);
			} else if (recipientType.equals("bcc")) {
				// 添加暗送地址
				mailMessage.setRecipients(Message.RecipientType.BCC, recipient);
			} else {
				errorMessage = "地址发送类型错误";
				Exception ex = new Exception("地址发送类型错误");
				throw ex;
			}

		}
	}

	/**
	 * 获得邮件会话属性，程序内部使用
	 */
	private Properties getProperties() {
		Properties p = new Properties();
		p.put("mail.smtp.host", this.mailServerHost);
		p.put("mail.smtp.port", this.mailServerPort);
		p.put("mail.smtp.auth", "true");
		return p;
	}

	/**
	 * 使用方式演示
	 */
	public static void main(String[] main) {
		// 邮箱体
		MailSender tMailSender = new MailSender("zhangchengxuan", "zhangcx3");
		// 标题，内容，附件(多附件可以用|分隔，末尾|可加可不加)
		tMailSender.setSendInf("集中代收付异常批次", "1111111\n/n111111",
				"E:\\loong\\a.xml|E:\\loong\\书API\\jdom.doc|");

		// 添加发送地址方式1
		// 发送，抄送，暗送。多收件人可以用;分隔，末尾;可加可不加
		tMailSender
				.setToAddress(
						"zhangchengxuan@sinosoft.com.cn;zhangchenxuan@sinosoft.com.cn;",
						null, null);

		// 添加发送地址方式2
		// 通过从数据库提取的方式
		// 需要在ldcode1表中进行相关配置，参数为code的值，用于区分不同的功能使用
		// 配置：codetype=sendmail code自己配置 codename为邮箱地址
		// othersign为发送、抄送、暗送标记，若为空则直接发送
		tMailSender.setToAddress("7705/7706");

		// 发送邮件
		if (!tMailSender.sendMail()) {
			System.out.println(tMailSender.getErrorMessage());
		}
	}
}