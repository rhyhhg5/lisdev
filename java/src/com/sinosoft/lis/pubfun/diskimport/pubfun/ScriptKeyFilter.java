/*
 * @(#)ScriptKeyFilter.java 1.0 2011/01/28
 *
 * Copyright (c) 2011 SinoSoft
 * All rights reserved.
 *
 */
package com.sinosoft.lis.pubfun;

/**
 * 对 关键词 做过滤  公共方法
 * 防止      跨站脚本/SQL注入    攻击
 * 
 * @version 1.0 
 * @since 2011.01.28
 * @author filon51
 */
public class ScriptKeyFilter {
	
	/**
	 * 过滤跨站脚本关键字
	 * 
	 * @param str
	 * @return
	 */
	public static String CrsSitScptFilter(String str) {
		if (str == null)
			return "";
		str = str.replaceAll("<", "＜");
		str = str.replaceAll(">", "＞");
		str = str.replaceAll("'", "＇");
		str = str.replaceAll("&", "＆");
		str = str.replaceAll("#", "＃");
		str = str.replaceAll("%", "％");
		str = str.replaceAll("\"", "＼");
		// str = str.replaceAll(",","");
		// str = str.replaceAll(".","");
		// str = str.replaceAll("[tab]","");
		// str = str.replaceAll("[space]","");
		// str = str.replaceAll("(","");
		// str = str.replaceAll(")","");

		return str;
	}

	/**
	 * 前台传入参数不能含有SQL关键词
	 * 防止SQL注入漏洞
	 * 
	 * @param aPara
	 * @return
	 */
	public static boolean checkValidation(String aPara) {
		String tPara = aPara.toUpperCase();
		boolean vFlag = true;

		if (tPara.indexOf("SELECT") > -1 && tPara.indexOf("FROM") > -1) {
			vFlag = false;
		}
		if (tPara.indexOf('<') != -1) {
			vFlag = false;
		} else if (tPara.indexOf('=') != -1) {
			vFlag = false;
		} else if (tPara.indexOf('>') != -1) {
			vFlag = false;
		} else if (tPara.indexOf('%') != -1) {
			vFlag = false;
		} else if (tPara.indexOf('\'') != -1) {
			vFlag = false;
		} else if (tPara.indexOf('"') != -1) {
			vFlag = false;
		} else if (tPara.indexOf(';') != -1) {
			vFlag = false;
		} else if (tPara.indexOf('(') != -1) {
			vFlag = false;
		} else if (tPara.indexOf(')') != -1) {
			vFlag = false;
		} else if (tPara.indexOf('+') != -1) {
			vFlag = false;
		} else if (tPara.indexOf('\'') != -1) {
			vFlag = false;
		}

		// System.out.println("aPara:"+aPara+" is "+vFlag) ;
		return vFlag;
	}
	
	/**
	 * 前台查询SQL语句不能同时含有以下关键词：
	 * 1 FINAL 和 UPDATE
	 * 2 FINAL 和 INSERT
	 * 3 OLD 和 DELETE
	 * 
	 * 防止SELECT SQL语句注入DML操作
	 * @param tSql
	 * @return
	 */
	public static boolean checkSelect(String tSql){
		String upperSql = tSql.toUpperCase();
		if (upperSql.indexOf(" FINAL ") > -1 && upperSql.indexOf("UPDATE ") > -1) {
			return false;
		}
		if (upperSql.indexOf(" FINAL ") > -1 && upperSql.indexOf("INSERT ") > -1) {
			return false;
		}
		if (upperSql.indexOf(" OLD ") > -1 && upperSql.indexOf("DELETE ") > -1) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * 测试主函数
	 * @param args
	 */
	public static void main(String[] args){
		if(!ScriptKeyFilter.checkSelect("select s_companyid from old  table (delete from test_filon);")){
			System.out.println("含有不合法的DML操作关键词");
		}else{
			System.out.println("校验通过！");
		}
		if(!ScriptKeyFilter.checkSelect("select s_companyid from final table (insert into test_filon (S_COMPANYID,S_PCOMPANYID,S_LEVEL,S_COMPCODE) values ('ts','ts','te','1'));")){
			System.out.println("含有不合法的DML操作关键词");
		}else{
			System.out.println("校验通过！");
		}
		if(!ScriptKeyFilter.checkSelect("select s_companyid from final table (update test_filon set S_COMPANYID = 'tu' where S_COMPCODE = '1');")){
			System.out.println("含有不合法的DML操作关键词");
		}else{
			System.out.println("校验通过！");
		}
		return;	
	}
}
