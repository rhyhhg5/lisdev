/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;

import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LDUserSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 *
 * @author Minim
 * @version 1.0
 */

public class CodeQueryBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    // private String mOperate;
    /** 存储查询语句 */
    private String mSQL = "";

    private StringBuffer mSBql = new StringBuffer();

    /** 存储全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 存储查询条件 */
    private String mCodeCondition = "";

    private String mConditionField = "";

    /** 业务处理相关变量 */
    private LDCodeSchema mLDCodeSchema = new LDCodeSchema();

    // private LDCodeSet mLDCodeSet = new LDCodeSet();
    private ExeSQL mExeSQL = new ExeSQL();

    /** 返回的数据 */
    private String mResultStr = "";

    public CodeQueryBL()
    {
        try
        {
            jbInit();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 传输数据的公共方法, 本处理没有后续的BLS层，故该方法无用
     *
     * @param cInputData
     *            VData
     * @param cOperate
     *            String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将操作数据拷贝到本类中
        // this.mOperate = cOperate;

        // 得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 进行业务处理
        if (!queryData())
        {
            return false;
        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     *
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    public String getSql()
    {
        return mSQL;
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData
     *            VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        // 代码查询条件
        try
        {
            mLDCodeSchema.setSchema((LDCodeSchema) cInputData
                    .getObjectByObjectName("LDCodeSchema", 0));

            try
            {
                mGlobalInput.setSchema((GlobalInput) cInputData
                        .getObjectByObjectName("GlobalInput", 0));
            }
            catch (Exception e)
            {
                mGlobalInput.ComCode = "";
                mGlobalInput.ManageCom = "";
                mGlobalInput.Operator = "";
            }

            TransferData tTransferData = (TransferData) cInputData
                    .getObjectByObjectName("TransferData", 0);
            // 暂时默认为字符串类型
            // mCodeCondition = "'" +
            // (String)tTransferData.getValueByName("codeCondition") + "'";
            mCodeCondition = (String) tTransferData
                    .getValueByName("codeCondition");
            if (mCodeCondition.indexOf('#') == -1)
            {
                mCodeCondition = "'" + mCodeCondition + "'";
            }
            else
            {
                mCodeCondition = mCodeCondition.replace('#', '\'');
            }
            mConditionField = (String) tTransferData
                    .getValueByName("conditionField");

            if (mConditionField.equals(""))
            {
                mCodeCondition = "1";
                mConditionField = "1";
            }
        }
        catch (Exception e)
        {
            System.out
                    .println("\nCodeQueryBL throw Errors at getInputData: can not get GlobalInput!\n");
            mCodeCondition = "1";
            mConditionField = "1";
        }
        return true;
    }

    public void setGlobalInput(GlobalInput cGlobalInput)
    {
        mGlobalInput.setSchema(cGlobalInput);
    }

    /**
     * 查询符合条件的保单 输出：如果准备数据时发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean queryData()
    {
        mSQL = "";
        int executeType = 0;

        System.out.println("aa");
        if (mGlobalInput.ManageCom == null
                || mGlobalInput.ManageCom.trim().equals(""))
        {
            mGlobalInput.ManageCom = "86";
        }
//===============================财务接口========================================

        //		 财务接口depcode	
      		if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("comcode2") == 0)
      		{
      			mSBql.append("select bb.codealias,(select codename from licodetrans a where a.codealias=bb.codealias order by code fetch first 1 rows only)" 
      					+" from (select distinct codealias from licodetrans where codetype='ManageCom' )  as bb order by bb.codealias");
      			mSQL = mSBql.toString();

      			executeType = 1; // add by fx
      		}
        
        //		 财务接口数据源		
		if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("fidatasource") == 0)
		{
			mSBql.append("select InterfaceCode, InterfaceName from FIDataBaseLink a order by DBType ");
			mSQL = mSBql.toString();

			executeType = 1; // add by fx
		}
		//科目查询
	    if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("fiaccountcode") == 0)
	    {
	            mSBql.append("select itemmaincode, finitemname from fifinitemdef where 1=1 and finitemtype = ");
	            mSBql.append(mCodeCondition);
	            mSBql.append(" order by itemmaincode");
	            mSQL = mSBql.toString();
	            executeType = 1;
	    }
        //专项表字段
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("ficolumnid") == 0)
		{
		    mSQL = " select attno,attname from FIMetadataAttDef where  metadatano=(select metadatano from FIMetadataDef where object = 'FIVoucherDataDetail') order by attno";
		}
        //上游字段
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("fisourcecolumnid") == 0)
		{
		    mSQL = " select attno,attname from FIMetadataAttDef where  metadatano=(select metadatano from FIMetadataDef where object = 'FIAbStandardData') order by attno";
		}
        //业务属性
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "fibusinfo") == 0)
		{
		    mSQL = " select attno,attname from FIMetadataAttDef where  metadatano=(select metadatano from FIMetadataDef where object = 'FIAboriginalGenAtt') order by attno";
		}
		//业务属性值域
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("fibusvalues") == 0)
		{
		    mSQL = " select mainvalues,valuemeaning from FIMetadataAttValues where  metadatano=(select metadatano from FIMetadataDef where object = 'FIAboriginalGenAtt') and "
		    	+ mConditionField
		    	+ " = "
		    	+ mCodeCondition
		    	+" order by attno,mainvalues ";
		}
        //业务编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("fibntype") == 0)
		{
		    mSQL = " select businessid,businessname from fibntypedef where 1=1 and state = '1' order by businessid";
		}
        //科目编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("finitem") == 0)
		{
		    mSQL = " select FinItemID,FinItemName from FIFinItemDef where 1=1 and "
		    	+ mConditionField
		    	+ " = "
		    	+ mCodeCondition
		    	+" order by FinItemID";
		}  
        //索引编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "detailindexid") == 0)
		{
		    mSQL = " select code,codename from ldcode where 1=1 and codetype = 'detailindexid' "
		    	+" order by code";
		}
        
        //专项查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "associatedid") == 0)
		{
		    mSQL = " select AssociatedID,AssociatedName from FIAssociatedItemDef where 1=1 and "
		    	+ mConditionField
		    	+ " = "
		    	+ mCodeCondition
		    	+" order by AssociatedID";
		}
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("nextjudgementno") == 0)
        {
    	   	mSBql.append(" select 'END','结束' from dual  union all  ");
        	 mSBql.append(
             "select JudgementNo ,'判断条件'||JudgementNo from FIDetailFinItemDef where 1 = 1");
        	 //mSBql.append(" VersionNo = ");
//        	 mSBql.append(mCodeCondition);
        	 mSBql.append(" AND ");
        	 mSBql.append("  FirstMark !='Y'");
        	 mSQL = mSBql.toString();
        }
      //add by zjd 补充工伤失能B款
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("defcontplan") == 0) {
            mSQL = "select ContPlanCode,ContPlanName from LCContPlan where '1426672209000'='1426672209000' and  1=1 and " +
                   mConditionField
                    + " = "
                    + mCodeCondition+
                    " and ContPlanCode not in ('00','11')  union  select ContPlanCode,ContPlanName  from LbContPlan where 1=1 " +
                    "and " +
                    mConditionField
                    + " = "
                    + mCodeCondition+ 
                    "and ContPlanCode not in ('00','11')  union  select ContPlanCode,ContPlanName from LobContPlan where 1=1 and " +
                    mConditionField
                    + " = "
                    + mCodeCondition+" and ContPlanCode not in ('00','11') order by ContPlanCode  with ur";
          } 
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("defcontduty") == 0) {
            mSQL = "Select Distinct a.Dutycode,c.dutyname From Lmduty c,Lccontplandutyparam a Where c.dutycode=a.dutycode And " +
            		mConditionField
                    + " = "
                    + mCodeCondition+
            	   "  with ur";
          } 
        /**
         * 一家亲被保人序号
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "insurednum") == 0)
        {
            System.out.println("====="+mCodeCondition + "------------"+mConditionField);
            mSQL = "select Sequenceno,name from lcinsured where  "+mConditionField+"= " + mCodeCondition+" order by Sequenceno ";
        }
        
//======================================================================
        /**
         * 理赔身故受益人 by Xuxin at 20051119
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llbnfrela") == 0)
        {
            mSQL = "select code, codeName from LDcode"
                    + " where codetype='relation' and code<>'00' and "
                    + mConditionField + " = " + mCodeCondition;
        }
        
        /**
         * 所有银行编码统一 by Minim at 20051119 modification by zhangjun at20060628
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "bankcom") == 0)
        {
            mSQL = "select BankCode, BankName from LDBank"
                    + " where (BankUniteFlag is null or BankUniteFlag<>'1') and "
                    + mConditionField + " = " + mCodeCondition
                    + " order by BankCode";
        }
        /**
         * 银行编码按机构分类 by zhangjun at 20060628
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "bank") == 0)
        {
            String MsgCom = "";
            if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
            {
                MsgCom = mGlobalInput.ManageCom.substring(0, 4);
            }
            else
            {
                MsgCom = mGlobalInput.ManageCom;
            }
            mSQL = "select BankCode, BankName from LDBank"
                    + " where (BankUniteFlag is null or BankUniteFlag<>'1') and "
                    + " Comcode like '" + MsgCom + "%' and " + mConditionField
                    + " = " + mCodeCondition + " order by BankCode";
            executeType = 1;
        }

         /**
         * 返回银行编码和是否签约银行
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "llbank") == 0){
                String MsgCom = "";
                if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4){
                    MsgCom = mGlobalInput.ManageCom.substring(0, 4);
                } else {
                    MsgCom = mGlobalInput.ManageCom;
                }
                mSQL = "select BankCode, BankName,case when cansendflag='1' then '是' else '否' end from LDBank"
                     + " where (BankUniteFlag is null or BankUniteFlag<>'1') and "
                     + " Comcode like '" + MsgCom + "%' and " + mConditionField
                     + " = " + mCodeCondition + " order by BankCode";
                //add by chen xiang wei 20081015
                executeType = 1;
        }
        /**
         * 返回银行编码和是否签约银行，模糊查询
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "lllbank") == 0){
                String MsgCom = "";
                if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4){
                    MsgCom = mGlobalInput.ManageCom.substring(0, 4);
                } else {
                    MsgCom = mGlobalInput.ManageCom;
                }
//                String aaaaaa="";
//                try{
//                	aaaaaa=new String(mCodeCondition
//                            .substring(1, (mCodeCondition.length() - 1)).trim().getBytes("ISO8859_1"),"GBK");
//                }
//                catch(Exception ex)
//                {
//                	System.out.println(ex.toString());
//                }
                mSQL = "select BankCode, BankName,case when cansendflag='1' then '是' else '否' end from LDBank"
                     + " where (BankUniteFlag is null or BankUniteFlag<>'1') and "
                     + " Comcode like '" + MsgCom + "%' and " + mConditionField
                     + " like '" + mCodeCondition
                     .substring(1, (mCodeCondition.length() - 1)).trim() + "%' order by BankCode";
                //add by chen xiang wei 20081015
                executeType = 1;
        }
        /**
         * 查询指定机构的银行 by yangyalin 20080306
         */
        else if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                .compareTo("bankbranchcom") == 0)
        {
            String MsgCom = StrTool.cTrim(mCodeCondition);
            if (MsgCom.length() >= 6)
            {
                MsgCom = MsgCom.substring(1, 5);
            }
            else
            {
                MsgCom = "";
            }
            mSQL = "select BankCode, BankName from LDBank "
                    + "where (BankUniteFlag is null or BankUniteFlag<>'1') "
                    //+ "  and (Comcode = '" + MsgCom + "' "
                    //+ "    or ComCode = '" + MsgCom + "0000') order by BankCode";
                    + "  and Comcode like '" + MsgCom + "%' order by BankCode";
        }
        
         /**
         * 查询银行转账的银行 by guzg 20090701
         */
        else if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                .compareTo("simiplebankbranchcom") == 0)
        {
        	String MsgCom = "";
            if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
            {
                MsgCom = mGlobalInput.ManageCom.substring(0, 4);
            }
            else
            {
                MsgCom = mGlobalInput.ManageCom;
            }
        	
            String PayMode = StrTool.cTrim(mCodeCondition);
            if("'8'".equals(PayMode)){
            	mSQL = "select MedicalComCode,MedicalComName from LDMedicalCom where CanSendFlag='1' and  comcode like '" +
				MsgCom+
				"%' order by MedicalComCode with ur";
            }else if("8695".equals(MsgCom)||!"'4'".equals(PayMode))
            {
            	mSQL = "select BankCode, BankName from LDBank "
                    + "where (BankUniteFlag is null or BankUniteFlag<>'1') "
                    //+ "  and CanSendFlag = '1' "
                    //+ "    or ComCode = '" + MsgCom + "0000') order by BankCode";
                    + "  and Comcode like '" + MsgCom + "%' order by BankCode";
            }
            else	            
            {
            	 mSQL = "select BankCode, BankName from LDBank "           	
	                    + "where (BankUniteFlag is null or BankUniteFlag<>'1') "
	                    + "  and CanSendFlag = '1' "
	                    //+ "    or ComCode = '" + MsgCom + "0000') order by BankCode";
	                    + "  and Comcode like '" + MsgCom + "%' order by BankCode";
            }
        }
        
           /**
         * 天津外包录入银行分类  by  liujian 2013-12-25
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "osbank") == 0)
        {
        	String MsgCom = "";
        	if (StrTool.cTrim(mConditionField).length() >= 4)
            {
                MsgCom = mConditionField.substring(0, 4);
            }
        	 mSQL = "select * from ldbank a where exists (select 1 from ldbankunite where bankcode=a.bankcode " +
        	 		" and bankunitecode in ('7705','7706') and  unitegroupcode in ('2','3'))  " +
        	 		"  and Comcode like '" + MsgCom + "%' order by BankCodewith ur";
        	
           
        }
        
		//贷款利率页面选择录入险种 by xp 20120319
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("loanriskcode") == 0)
        {
            mSBql.append("select b.riskcode,a.riskname from LMRisk a,lmloan b where a.riskcode=b.riskcode with ur");
            mSQL = mSBql.toString();
        }

        //分红险种 by xp 20121024
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("bonusriskcode") == 0)
        {
            mSBql.append("select riskcode,riskname from LMRiskapp where risktype4='2' with ur");
            mSQL = mSBql.toString();
        }
        
        //所有险种分类
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("riskcodetype") == 0)
        {
            mSBql.append("select riskcode,riskname from LMRiskapp where risktype4 in('1','2','3','4','5')with ur");
            mSQL = mSBql.toString();
        }
        /**
         * 银行接口银行编码统一 by Minim at 20051119 modification by zhangjun at20060628
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "sendbankcom") == 0)
        {
            mSQL = "select BankCode, BankName from LDBank"
                    + " where CanSendFlag='1' and " + mConditionField + " = "
                    + mCodeCondition + " order by BankCode";
        }
        /**
         * 银行编码按机构和发送银行分类 by zhangjun at 20060628
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "sendbank") == 0)
        {
            String MsgCom = "";
            if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
            {
                MsgCom = mGlobalInput.ManageCom.substring(0, 4);
            }
            else
            {
                MsgCom = mGlobalInput.ManageCom;
            }
            mSQL = "select BankCode, BankName from LDBank"
                    + " where CanSendFlag='1' and " + " Comcode like '"
                    + MsgCom + "%' and " + mConditionField + " = "
                    + mCodeCondition + " order by BankCode";
        }
        
        /**
         * 金联万佳指定的银行编码 by wyd 20160713
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "sendbankjlwj") == 0)
        {
            String MsgCom = "";
            if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
            {
                MsgCom = mGlobalInput.ManageCom.substring(0, 4);
            }
            else
            {
                MsgCom = mGlobalInput.ManageCom;
            }
//            mSQL = "select BankCode, BankName from LDBank"
//                    + " where CanSendFlag='1' and " + " Comcode like '"
//                    + MsgCom + "%' and " + mConditionField + " = "
//                    + mCodeCondition + " order by BankCode";
            
            mSQL = "select BankCode, BankName from LDBank"
                    + " where Comcode like '"
                    + MsgCom + "%'  and exists (select 1 from ldbankunite where bankunitecode='7707' and bankcode = LDBank.bankcode ) order by BankCode";
        }
        
        /**
         * 交叉销售银行编码 by wrx 任务号#4142 #4143 #4147
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "sendbankjxxs") == 0)
        {
            String MsgCom = "";
            if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
            {
                MsgCom = mGlobalInput.ManageCom.substring(0, 4);
            }
            else
            {
                MsgCom = mGlobalInput.ManageCom;
            }            
            mSQL = "select BankCode, BankName from LDBank"
                    + " where Comcode like '"
                    + MsgCom + "%'  and exists (select 1 from ldbankunite where bankunitecode='7702' and bankcode = LDBank.bankcode ) order by BankCode";
        }
        /**
         * 医保机构发送银行分类 by  ws 2013-9-4
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "medicalcode") == 0)
        {
            String MsgCom = "";
            if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
            {
                MsgCom = mGlobalInput.ManageCom.substring(0, 4);
            }
            else
            {
                MsgCom = mGlobalInput.ManageCom;
            }
            mSQL = "select MedicalComCode, MedicalComName from LDMedicalCom"
                    + " where CanSendFlag='1' and " + " ComCode like '"
                    + MsgCom + "%' and " + mConditionField + " = "
                    + mCodeCondition + " order by MedicalComCode";
        }
        /**
         * 医保机构发送银行分类 by  ws 2013-9-11
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "bankcode") == 0)
        {
        	 String MsgCom = "";
        	 if (StrTool.cTrim(mConditionField).length() >= 4)
             {
                 MsgCom = mConditionField.substring(0, 4);
             }
        	 
        	if("'8'".equals(mCodeCondition)){
        		mSQL = "select MedicalComCode,MedicalComName from LDMedicalCom where CanSendFlag='1' and  comcode like '" +
        				MsgCom+
        				"%' order by MedicalComCode with ur";
        	}else{
        		mSQL = "select BankCode, BankName from LDBank "
                        + "where (BankUniteFlag is null or BankUniteFlag<>'1') "
                        //+ "  and (Comcode = '" + MsgCom + "' "
                        //+ "    or ComCode = '" + MsgCom + "0000') order by BankCode";
                        + "  and Comcode like '" + MsgCom + "%' order by BankCode with ur";
        	}
            
           
        }
        /**
         * 总公司集中代收付
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "sendbank4") == 0)
                {
                        String MsgCom4 = "";
                    if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
                    {
                        MsgCom4 = mGlobalInput.ManageCom.substring(0, 4);
                    }
                     else
                    {
                     MsgCom4 = mGlobalInput.ManageCom;
                    }
                      mSQL = "select BankCode, BankName from LDBank"
                              + " where CanSendFlag='1' and operator='sys' and " + mConditionField + " = "
                              + mCodeCondition + " order by BankCode";
                }
        
        /**
         * 总公司集中代收付银联通联金联用
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "sendbank4ytj") == 0)
                {
                        String MsgCom4 = "";
                    if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
                    {
                        MsgCom4 = mGlobalInput.ManageCom.substring(0, 4);
                    }
                     else
                    {
                     MsgCom4 = mGlobalInput.ManageCom;
                    }
                      mSQL = "select BankCode, BankName from LDBank"
                              + " where CanSendFlag='1' and operator in ('sys','system','baoRong') and " + mConditionField + " = "
                              + mCodeCondition + " order by BankCode";
	                }
	      /**
	      * 总公司集中代收付保融用
	      */
	     if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
	     		"sendbank4br") == 0)
	     {
	     	String MsgCom4 = "";
	     	if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
	     	{
	     		MsgCom4 = mGlobalInput.ManageCom.substring(0, 4);
	     	}
	     	else
	     	{
	     		MsgCom4 = mGlobalInput.ManageCom;
	     	}
	     	mSQL="select codealias,codename from ldcode1 where codetype='BatchBank' and code='7701' and " + mConditionField + " = "
	     			+ mCodeCondition + " order by codealias";
	     	
	     }
        /**
         * 总公司集中代收付
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "sendbank5") == 0)
                {
                        String MsgCom5 = mGlobalInput.ManageCom;
                      mSQL = "select BankCode, BankName from LDBank"
                              + " where (BankUniteFlag<>'1' or BankUniteFlag is null) and "
                              + " comcode like '" + MsgCom5 
                              + "%' and " + mConditionField + " = "
                              + mCodeCondition + " order by BankCode";
                }
        /**
         * 总公司集中代收付
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "sendbank6") == 0)
                {
                      mSQL = "select code1, codename from LDCode1 where codetype='BatchBank' and "
                              + mConditionField + " = "
                              + mCodeCondition + " order by code1";
                }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("wrapplancode") == 0)
        {
            mSQL = "select distinct plancode,(select distinct planname from LDWrapPlan where plancode=LDWrapPlanToCom.plancode) from LDWrapPlanToCom"
                    + " where managecom = (select distinct managecom from lcgrpcont where prtno='"
                    + mConditionField
                    + "') order by plancode";
        }
        
        /**
         * 银行编码按机构和银保通标记分类 by yanjing at 20100726
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "sendbank2") == 0)
{
    String MsgCom = "";
    if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
    {
        MsgCom = mGlobalInput.ManageCom.substring(0, 4);
    }
    else
    {
        MsgCom = mGlobalInput.ManageCom;
    }
    mSQL = "select BankCode, BankName from LDBank"
            + " where CanSendFlag='1' and operator='ybt' and " + mConditionField + " = "
            + mCodeCondition + " order by BankCode";
}
        /**
         * 银行编码按机构和发送银行分类,增加邮保通判断 by dongjian at 20060628
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "sendbank3") == 0)
        {
            String MsgCom = "";
            if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
            {
                MsgCom = mGlobalInput.ManageCom.substring(0, 4);
            }
            else
            {
                MsgCom = mGlobalInput.ManageCom;
            }
            if(mCodeCondition.equals("'7703'"))
            {
            	mSQL = "select BankCode, BankName from LDBank"
                    + " where Comcode like '"
                    + MsgCom + "%' and bankcode like '16%' order by BankCode";
            }
            else
            {
            	mSQL = "select BankCode, BankName from LDBank"
                + " where CanSendFlag='1' and " + " Comcode like '"
                + MsgCom + "%' order by BankCode";
            }
            
        }

 /**
         * 银行编码按机构和银保通标记分类 by yuml at 20100726
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "unitebankcode1") == 0)
{
    String MsgCom = "";
    if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
    {
        MsgCom = mGlobalInput.ManageCom.substring(0, 4);
    }
    else
    {
        MsgCom = mGlobalInput.ManageCom;
    }
    mSQL = "select distinct bankunitecode,bankunitename  from ldbankunite  order by bankunitecode ";
}


        /**
         * 银行编码按机构和发送银行分类（深圳除外,财务收费的本方银行使用） by zhangjun at 20070820
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "otofsendbank") == 0)
        {
            String MsgCom = "";
            if (StrTool.cTrim(mGlobalInput.ManageCom).length() >= 4)
            {
                MsgCom = mGlobalInput.ManageCom.substring(0, 4);
            }
            else
            {
                MsgCom = mGlobalInput.ManageCom;
            }
            if (!MsgCom.equals("86"))
            {
                if (!MsgCom.equals("8695"))
                {
                    mSQL = "select BankCode, BankName from LDBank"
                            + " where CanSendFlag='1' and " + " Comcode like '"
                            + MsgCom + "%' and " + mConditionField + " = "
                            + mCodeCondition + " and bankuniteflag is null"
                            + " order by BankCode";
                }
                else if (MsgCom.equals("8695"))
                {
                    mSQL = "select BankCode,BankName from LDBank"
                            + " where CanSendFlag='0' and"
                            + " Comcode like '8695%' and " + mConditionField
                            + " = " + mCodeCondition + " order by BankCode";
                }
            }
            else
            {
                mSQL = "select BankCode, BankName from LDBank"
                        + " where CanSendFlag='1' and " + " Comcode like '"
                        + MsgCom + "%' and " + mConditionField + " = "
                        + mCodeCondition + " and bankuniteflag is null"
                        + " Union " + "select BankCode,BankName from LDBank"
                        + " where CanSendFlag='0' and"
                        + " Comcode like '8695%' and " + mConditionField
                        + " = " + mCodeCondition + " order by BankCode";
            }
        }
        /**
         * 财务本方收费银行编码提取 by yanjing at 20100129
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "getbankcode1") == 0)
        {
            String MsgCom = mGlobalInput.ManageCom;
            mSQL = "select distinct BankCode||'-'||BankName, BankCode from LDFinBank"
                    + " where FinFlag='S' and " + " ManageCom like '"
                    + MsgCom + "%' and " + mConditionField + " = "
                    + mCodeCondition + " order by BankCode";
        }
        /**
         * 财务本方收费银行编码提取 by yanjing at 20100803
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "getbankcode2") == 0)
        {
            String MsgCom = mGlobalInput.ManageCom;
            mSQL = "select distinct BankCode||'-'||BankName, BankCode from LDFinBank"
                    + " where FinFlag='F' and " + " ManageCom like '"
                    + MsgCom + "%' and " + mConditionField + " = "
                    + mCodeCondition + " order by BankCode";
        }
        /**
         * 财务本方收费银行编码提取 by zhangjun at 20080626
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "getbankcode") == 0)
        {
            String MsgCom = mGlobalInput.ManageCom;
            mSQL = "select distinct BankCode, BankName from LDFinBank"
                    + " where FinFlag='S' and " + " ManageCom like '"
                    + MsgCom + "%' and " + mConditionField + " = "
                    + mCodeCondition + " order by BankCode";
        }
        /**
        * 财务本方收费银行帐号提取 by zhangjun at 20080626
        */
         if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
               "getbankaccno3") == 0)
       {
           String MsgCom = mGlobalInput.ManageCom;
           System.out.println("mCodeCondition==="+mCodeCondition);
           System.out.println("mConditionField==="+mConditionField);
           mSQL = "select BankAccNo,'' from LDFinBank"
                   + " where FinFlag='S'   and " 
                   + mCodeCondition + " order by BankCode";
      }
       if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
               "getbankaccno") == 0)
       {
           String MsgCom = mGlobalInput.ManageCom;
           mSQL = "select BankAccNo,'' from LDFinBank"
                   + " where FinFlag='S' and " + " ManageCom like '"
                   + MsgCom + "%' and " + mConditionField + " = "
                   + mCodeCondition + " order by BankCode";
      }
       /**
       * 财务本方付费银行编码提取 by zhangjun at 20080626
       */
      if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
              "paybankcode") == 0)
      {
          String MsgCom = mGlobalInput.ManageCom;
          mSQL = "select BankCode, BankName from LDFinBank"
                  + " where FinFlag='F' and " + " ManageCom like '"
                  + MsgCom + "%' and " + mConditionField + " = "
                  + mCodeCondition + " order by BankCode";
      }
       /**
       * 财务本方付费银行帐号提取 by zhangjun at 20080626
       */
      if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
              "paybankaccno") == 0)
      {
          String MsgCom = mGlobalInput.ManageCom;
          mSQL = "select BankAccNo, '' from LDFinBank"
                  + " where FinFlag='F' and " + " ManageCom like '"
                  + MsgCom + "%' and " + mConditionField + " = "
                  + mCodeCondition + " order by BankCode";
                 
      }

        /**
         * 批量打印单证名称选择 zhangjun
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "chnlcardname") == 0)
        {

            mSQL = "select code,codename from ldbatchprint" + " where "
                    + mConditionField + " = " + mCodeCondition;
        }
        /**
         * 代理机构引用AgentCom  yangming
         * 增加允许录单的营业机构的校验  zhangjianbao  2008-03-14
         * #3557新增中介机构类型11-保险专业代理   wrx 2017-10-20
         */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcombank") == 0)
        {
            mSBql
                    .append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
            mSBql.append("ManageCom like'");
            mSBql.append(mCodeCondition.substring(1,
                    (mCodeCondition.length() - 1)).trim());
            mSBql.append("%'");
            mSBql
                    .append(" and ACType in ('01','11') and (EndFlag='N' OR EndFlag is NULL) and AgentCom like 'PY%'");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom");

            mSQL = mSBql.toString();

            executeType = 1;
        }
        // 代理机构引用AgentCom
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcombrief") == 0)
        {
            // mSQL =
            // "select AgentCom, Name, UpAgentCom, AreaType, ChannelType from
            // LACom where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom + "%' order by
            // AgentCom";

            mSBql
                    .append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
            mSBql.append("ManageCom like'");
            mSBql.append(mCodeCondition.substring(1,
                    (mCodeCondition.length() - 1)).trim());
            mSBql.append("%'");
            mSBql.append(" and ACType <> '01'");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom");

            mSQL = mSBql.toString();

            executeType = 1;
        }
        // 代理机构引用AgentCom
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcomphone") == 0)
        {
            mSBql
                    .append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
            mSBql.append("ManageCom like'");
            mSBql.append(mCodeCondition.substring(1,
                    (mCodeCondition.length() - 1)).trim());
            mSBql.append("%'");
            mSBql.append(" and BranchType = '4'");
            mSBql.append(" and BranchType2 = '01'");
            mSBql.append(" and ACType <> '01'");
            mSBql.append(" and (EndFlag='N' OR EndFlag is NULL)");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom");

            mSQL = mSBql.toString();

            executeType = 1;
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "insuredno") == 0)
        {
            System.out.println("mCodeCondition :" + mCodeCondition);
            System.out.println("mConditionField :" + mConditionField);
            mSQL = "select insuredno,name from lcinsured where " + " 1=1 "
                    + " and ContNo=" + mCodeCondition
                    + " order by int(SequenceNo)";
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "diseasecode") == 0)
        {
            mSQL = "select distinct DiseaseCode,DisText,ICDCode from LDUWAddFee where "
                    + " 1=1 " + " order by DiseaseCode";
            System.out.println("mCodeCondition ：" + mCodeCondition);
            if (mCodeCondition != null && !mCodeCondition.equals("''"))
            {
                mSQL = "select distinct DiseaseCode,DisText,ICDCode from LDUWAddFee where "
                        + " 1=1 "
                        + " and DisText like '%"
                        + mCodeCondition.substring(1,
                                (mCodeCondition.length() - 1)).trim()
                        + "%' order by DiseaseCode";

            }
            System.out.println("使用Mulline CodeQuery查询 ：" + mSQL);
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "airrisk") == 0)
        {
            mSQL = "select riskcode,riskname from lmriskapp where " + " 1=1 "
                    + " and risktype8='2' "
                    + " and RiskProp in ('G','A','B','D')"
                    + " order by riskcode";
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "issuepoltype") == 0)
        {
            mSQL = "select code,codename from LDCODE where 1=1  and codetype='issuepoltype' order by code";
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "ldnation") == 0)
        {
            mSQL = "select  nationno,chinesename from ldnation where 1=1  and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' and travelinsuranceflag='Y' order by nationno";
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "scanoperator") == 0)
        {
            mSQL = "select  distinct a.ScanOperator,b.username from  ES_DOC_MAIN a,lduser b where 1=1 and a.ScanOperator=b.usercode";
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "briefrisk") == 0)
        {
            mSQL = "select riskcode,riskname from lmriskapp where " + " 1=1 "
                    + " and risktype8='1' "
                    + " and RiskProp in ('G','A','B','D')"
                    + " order by riskcode";
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "bankriskcode") == 0)
        {
        	mSQL = "select code,codename from ldcode where " + " 1=1 "
                    + " and codetype='bankriskcode' and  "
                    + mConditionField
                    + "="
                    + mCodeCondition  + " order by code";
        	
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        		"allbankriskcode") == 0)
        {
        	mSQL = "select code,codename from ldcode where " +
        	"codetype='bankriskcode'  order by code";
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "firsttrialoperator") == 0)
        {
            mSQL = "select UserCode,UserName from LDUser where " + " 1=1 "
                    + " and OtherPopedom='1' and UserState <>'1' " + " order by UserCode";
            executeType=1;            
        }
        // 团体问题件
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lcgrpissue") == 0)
        {
            mSQL = "select distinct integer(issuecode) ascCol, issuename from LDISSUE where IssueType='2' "
                    + " order by ascCol";
        }
        // 团体问题件规则代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "issuerulecode") == 0)
        {
            mSQL = "select substr(issuerulecode,1,1) errtype,issuerulecode,backobjtype,backobj,issuecont from LDISSUE where"
                    + " IssueType='2' and issuecode="
                    + mCodeCondition
                    + " order by issuerulecode";
        }
        /**
         * 健康管理相关查询
         */
        // 查询健身项目名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhgymitem") == 0)
        {
            mSQL = "select  codename,code from ldcode where microsecond(current timestamp)= microsecond(current timestamp) and codetype = 'gymitem' and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by codename";
        }

        // 查询合同状态名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhcontrastate") == 0)
        {
            mSQL = "select  code,codename from ldcode where microsecond(current timestamp)= microsecond(current timestamp) and codetype = 'contrastate' ";
        }

        //查询集团交叉对方机构代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "grpagentcom") == 0)
        {
        	System.out.println("mCodeCondition :" + mCodeCondition);
            mSQL = "select grpagentcom,Under_Orgname  from lomixcom " + " where "
            	+ mConditionField + " = " + mCodeCondition;
        }
        // 查询责任项目名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhdutyitemname") == 0)
        {
            mSQL = "select  DutyItemCode,DutyItemName from DutyItem where microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by DutyItemName";
        }
        // 查询检查项目名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhmedicaitemname") == 0)
        {
            mSQL = "select  MedicaItemName,MedicaItemCode,StandardMeasureUnit from LHCountrMedicaItem where microsecond(current timestamp)= microsecond(current timestamp) and  "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by MedicaItemName";
        }

        // 查询亲属
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "familycode") == 0)
        {
            mSQL = "select  CodeName,Code from LDCode where microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + "CodeType = 'familycode'";
        }

        // 查询家族疾病名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhdisease") == 0)
        {
            mSQL = "select  ICDName,ICDCode from LDDisease where microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by ICDName";
        }
        // 查询医院名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhhospitname") == 0)
        {
            String mlen = "";
            // mSQL = "select HospitName,HospitCode from ldhospital where " +
            // mConditionField + " like " + mCodeCondition+" order by
            // HospitName";
            if (mGlobalInput.ManageCom.length() == 8)
            {
                mlen = mGlobalInput.ManageCom.substring(0, 4);
            }
            else
            {
                mlen = mGlobalInput.ManageCom;
            }
            System.out.println(mlen);
            mSQL = "select  HospitName,HospitCode from LDHospital where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' and Managecom like '"
                    + mlen
                    + "%' order by  HospitName";

        }
        // 查询医师名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhdoctname") == 0)
        {
            mSQL = "select  DoctName,DoctNo from LDDoctor where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by DoctName";
        }
        //查询批处理任务编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "taskcode") == 0)
        {
            mSQL = "select  taskcode,taskdescribe from ldtask "
                    + " order by taskcode";
        }

        // 查询是否为主要诊疗标志
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "ismaindiagno") == 0)
        {
            mSQL = "select  CodeName,Code from LDCode where Codetype = 'ismaindiagno' ";
            // and microsecond(current timestamp)= microsecond(current
            // timestamp) and "
            // + mConditionField + " like '%" +
            // mCodeCondition.substring(1, (mCodeCondition.length() - 1)).
            // trim()
            // + "%' order by DoctName";
        }

        // 查询就诊方式
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhinhospitmode") == 0)
        {
            mSQL = "select  codename,code from LDcode"
                    + " where microsecond(current timestamp)= microsecond(current timestamp) and codetype='inhospitmode' and to_number(code)<30 ";
            // + mConditionField + " like '%" +
            // mCodeCondition.substring(1, (mCodeCondition.length() - 1)).
            // trim()
            // + "%' order by DoctName";
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "comtoagent") == 0)
//        	modify lyc 统一工号 2014-11-24
        {
            mSQL = "select getUniteCode(laagent.agentcode),laagent.name,labranchgroup.branchattr,labranchgroup.name from laagent,labranchgroup "
                    + " where  "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and laagent.branchcode=labranchgroup.agentgroup ";
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "markrsn") == 0)
        {
            mSQL = "select rtrim(Itemcode),rtrim(Itemcontext) from laqualityitemdef "
                    + " where Itemcode like 'Q%' and  "
                    + mConditionField
                    + "="
                    + mCodeCondition + "";

            System.out.println("--------------------------------5");
            System.out.println(mSQL);
        }

        // 查询手术名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhicdopsname") == 0)
        {

            mSQL = "select  ICDOPSName,ICDOPSCode from LDICDOPS where microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by ICDOPSName";

        }
        // 查询套餐对应医疗项目
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhselectedmediaitem") == 0)
        {

            mSQL = "select distinct MedicaItemCode,MedicaItemName from LHCountrMedicaItem,LHCustomTest where microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + "LHCountrMedicaItem.MedicaItemCode=LHCustomTest.MedicaItemCode and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by MedicaItemName";

            // mSQL = "select "+mConditionField+" distinct from
            // LHMedicalItemGroup order by "+mConditionField;

        }
        // 查询体检项目名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhtestname") == 0)
        {

            mSQL = "select  MedicaItemName,MedicaItemCode,StandardMeasureUnit from LHCountrMedicaItem where microsecond(current timestamp)= microsecond(current timestamp) and  "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by MedicaItemName";

        }
        // 查询体检方式
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhtestmode") == 0)
        {

            mSQL = "select  codename,code from ldcode where microsecond(current timestamp)= microsecond(current timestamp) and  "
                    + " codetype ='inhospitmode' and code like '3%'";
        }
        // 查询展业类型
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "BranchType") == 0)
        {

            mSQL = "select distinct BranchType from labranchLevel ";

        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "BranchType2") == 0)
        {

            mSQL = "select distinct BranchType2 from labranchLevel where"
                    + mConditionField + "=" + mCodeCondition;

        }

        // 查询对应机构下的用户
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "usercode") == 0)
        {
            mSQL = "select  Usercode,Username from LDUser where "
                    + mConditionField + " = " + mCodeCondition;
        }

        // 咨询专家
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "familydoctorno") == 0)
        {
            mSQL = "select  DoctNo,DoctName from LDDoctor where 1=1 and CExportFlag='1' and "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by DoctNo";
        }

        // 认证级别
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "ldattestleve") == 0)
        {
            mSQL = "select  AttestLevelCode,AttestLevel from LDAttestLeve where 1=1 and "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by AttestLevelCode";
        }
        // 卫生机构类别查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "ldhealthorganclass") == 0)
        {
            mSQL = "select  HealthOrganClass,HealthOrganClassName from LDHealthOrganClass where 1=1 and "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by HealthOrganClass";
        }
        // 单位隶属关系查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "ldorganisubjec") == 0)
        {
            mSQL = "select  SubjecCode,SubjecName from LDOrganiSubjec where 1=1 and "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by SubjecCode";
        }
        // 业务类型代码查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "ldbusitype") == 0)
        {
            mSQL = "select  BusiTypeCode,BusiType from LDBusiType where 1=1 and "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by BusiTypeCode";
        }
        // 主要治疗方式
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "maincuremode") == 0)
        {
            mSQL = "select  codename,code from ldcode where microsecond(current timestamp)= microsecond(current timestamp) and  "
                    + " codetype ='maincuremode' ";
        }
        // 咨询作业状态
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmreplystate") == 0)
        {
            mSQL = "select  codename,code from ldcode where microsecond(current timestamp)= microsecond(current timestamp) and  "
                    + " codetype ='hmreplystate' ";
        }
        // 咨询方式
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmaskmode") == 0)
        {
            mSQL = "select  codename,code from ldcode where microsecond(current timestamp)= microsecond(current timestamp) and  "
                    + " codetype ='hmaskmode' ";
        }
        // 就诊费用查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhfeeitemtype") == 0)
        {
            mSQL = "select  codename,code from ldcode where microsecond(current timestamp)= microsecond(current timestamp) and  "
                    + mCodeCondition + " codetype ='llfeeitemtype' ";
        }

       // 综合查询--客户号码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmldperson") == 0)
        {
            mSQL = "select  CustomerNo,Name from ldperson where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " like '"
                    + (mCodeCondition.substring(1, (mCodeCondition.length() - 1)).trim().equals("")?"A":mCodeCondition.substring(1, (mCodeCondition.length() - 1)).trim())
                    + "%' order by CustomerNo ";
        }

        // 综合查询--客户姓名
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmldpersonname") == 0)
        {
            mSQL = "select  Name,CustomerNo from ldperson where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " like '"
                    + (mCodeCondition.substring(1, (mCodeCondition.length() - 1)).trim().equals("")?"A":mCodeCondition.substring(1, (mCodeCondition.length() - 1)).trim())
                    + "%' order by Name ";
        }

        // 疾病名称对照
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lddiseasename") == 0)
        {
            mSQL = "select  ICDCode,ICDName from lddisease where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by ICDCode ";
        }

        // 手术名称对照
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "ldopsname") == 0)
        {
            mSQL = "select  ICDOPSCode,ICDOPSName from ldicdops where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by ICDOPSCode ";
        }

        // 客户满意度
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmsatisfy") == 0)
        {
            mSQL = "select  codename,code from ldcode where microsecond(current timestamp)= microsecond(current timestamp) and  "
                    + " codetype ='hmsatisfy' ";
        }

        // 医疗服务项目(按代码查询)
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhmedicalitem") == 0)
        {
            mSQL = "select  MedicaItemCode,MedicaItemName from LHCountrMedicaItem where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by MedicaItemCode ";
        }

        // 医疗服务项目(按中文查询)
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhmedicalitem2") == 0)
        {
            mSQL = "select  MedicaItemName,MedicaItemCode from LHCountrMedicaItem where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by MedicaItemCode ";
        }

        // 套餐管理--套餐对应组合项
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhmedicalitem3") == 0)
        {
            mSQL = " select distinct testgrpcode, testgrpname  from ldtestgrpmgt "
                    + " where  testgrptype = '3' and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by testgrpcode ";

        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhmedicalitem4") == 0)
        {
            mSQL = " select distinct testgrpname, testgrpcode  from ldtestgrpmgt "
                    + " where  testgrptype = '3' and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by testgrpcode ";
        }

        // 职称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "techpost") == 0)
        {
            mSQL = "select  Codename,Code from LDCode where codetype ='TechPostCode' "
                    + "and code like '"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }

        // 合同管理--责任项目
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmdutyitemdef") == 0)
        {
            mSQL = "select  a.DutyItemName,a.DutyItemCode from LDContraItemDuty a ,ldcode b "
                    + " where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + " a.DutyItemName like '%%' and a.Dutyitemtye=b.code and  b.codetype = 'hmdeftype' "
                    + " and a.Dutyitemtye like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }

        // 费用结算管理下的合同编号
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmgrpcontrano") == 0)
        {
            mSQL = "select distinct  a.ContraNo,a.ContraName from LHGroupCont a  "
                    + " where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + " a.ContraName like '%%'  "
                    + " and a.HospitCode like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }
        // 费用结算管理下的合同下的责任代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmgrpdutyitemno") == 0)
        {
            mSQL = "select  distinct a.DutyItemCode,a.DutyItemName ,b.Contraitemno from LDContraItemDuty a ,LHContItem b  "
                    + " where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + " a.DutyItemCode=b.DutyItemCode   and  b.ContraNo like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }

        // 体检价格管理--医院选择
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhhospitnameht") == 0)
        {
            mSQL = "select c.hospitname ,a.hospitcode from lhgroupcont a,lhcontitem b,ldhospital c "
                    + " where microsecond(current timestamp)= microsecond(current timestamp) "
                    + " and a.Contrano = b.Contrano and   b.DutyItemCode = 'HB0001' "
                    + " and   Dutystate = '1' and   c.HospitCode = a.hospitcode and c."

                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhhospitnamehtuw") == 0)
        {
            String managecomname = this.mExeSQL
                    .getOneValue("Select ManageCom From LCCont Where ContNo="
                            + mCodeCondition);

            mSQL = "select a.hospitcode,c.hospitname from lhgroupcont a,lhcontitem b,ldhospital c "
                    + " where microsecond(current timestamp)= microsecond(current timestamp) "
                    + " and a.Contrano = b.Contrano and   b.DutyItemCode = 'HB0001' "
                    + " and   Dutystate = '1' and   c.HospitCode = a.hospitcode and c."

                    + mConditionField
                    + " like '%"
                    + managecomname.substring(0, 4) + "%'";
            // System.out.println(this.mSQL);
        }

        // 客户体检信息--选择套餐
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmchoosetest") == 0)
        {

            if (mCodeCondition.substring(1, 3).trim().equals("31"))
            {
                mSQL = "select distinct Testgrpname,Testgrpcode from ldtestgrpmgt where  testgrptype='1'";
            }
            else if (mCodeCondition.substring(1, 3).trim().equals("32"))
            {
                mSQL = "select distinct Testgrpname,Testgrpcode from ldtestgrpmgt where  testgrptype='2' and explain = '"
                        + mCodeCondition.substring(3,
                                (mCodeCondition.length() - 1)).trim() + "' ";
            }
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmchoosetest2") == 0)
        {
            mSQL = " select distinct Testgrpname,Testgrpcode from ldtestgrpmgt where  testgrptype='1' ";
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmchoosetestuw") == 0)
        {
            mSQL = " select distinct testgrpcode,testgrpname,othersign from ldtestgrpmgt where Testgrpcode like 'ZH%%' and testgrptype = '3'";
        }
        // if
        // (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        // "testgrpname") == 0) {
        // mSQL = " select distinct TestGrpName,TestGrpCode from LDGrpTest ";
        // }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "testgrpname") == 0)
        {
            mSQL = " select distinct testgrpcode, testgrpname from ldtestgrpmgt where testgrptype = '4' ";
        }

        // 体检价格管理--检查项目单价(根据名称查询)
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmtestgrpquery") == 0)
        {
            mSQL = " select a.testgrpcode as Ho,a.testgrpname from ldtestgrpmgt a "
                    + " where a.testgrpcode like 'ZH%' "
                    + " and   a.testgrp"
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' "
                    + "  union "
                    + " select b.medicaitemcode as Ho,b.medicaitemname from lhcountrmedicaitem b "
                    + " where b.medicaitem"
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'" + " order by Ho DESC ";
        }

        // 药品信息
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmdrug") == 0)
        {
            mSQL = "select  DrugName,DrugCode from LDDrug where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by DrugCode ";
        }

        // 地区名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmareaname") == 0)
        {
            mSQL = "select  CodeName,Code from LDCode where codetype = 'hmareacode' and microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }

        // 医疗机构管理机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmhospitalmng") == 0)
        {
            mSQL = " select showname, comcode from ldcom where  sign = '1' and length(trim(comcode)) = 4";
        }

        // 通过客户号查体检扫描件流水号
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhtestprtno") == 0)
        {
            mSQL = "select prtseq, pedate from lcpenotice where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " = '"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "'";
        }
        // 团体客户
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhgetgrpcontno") == 0)
        {
            mSQL = "select grpcontno,grpname  from lcgrpcont where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }

        // 短信模版分类查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "msgclassone") == 0)
        {
            mSQL = " select distinct SendEmail, '' from LOMsgInfo where msgtype = '9'";
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "msgclasstwo") == 0)
        {
            mSQL = " select distinct MsgTopic, '' from LOMsgInfo  where msgtype = '9' and "
                    + mConditionField
                    + " = '"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "'";
        }

        // 健管短信通过客户姓名查保单
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhmsggetcontinfo") == 0)
        {
            mSQL = "select grpcontno, contno  from lccont where microsecond(current timestamp)= microsecond(current timestamp) and "
                    + mConditionField
                    + " = '"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "' or appntno = '"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "'"

            ;
        }

        // 结算方式
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmbalancemode") == 0)
        {
            mSQL = "select  CodeName,Code from LDCode where microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + "CodeType = 'balancemode'";
        }

        // 机构代码和个人代码共有

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhgroupunit") == 0)
        {
            mSQL = "select hospitname, hospitcode from ldhospital  "
                    + "where  hospitname like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' union "
                    + "select doctname, doctno from lddoctor "
                    + "where  doctname like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'"

            ;
        }
          // 查询归属规则
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).equalsIgnoreCase(
            "ascriptionrulecode")) {
            mSBql
                .append("select distinct AscriptionRuleCode,AscriptionRuleName from LCAscriptionRuleFactory where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSQL = mSBql.toString();
            executeType = 1;
        }
        
        
        //      date 20101209 by gzh
        // 团单险种归属规则查询RiskAscriptionRuleFactoryType，Type编码默认为000006
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).equalsIgnoreCase(
            "riskascriptionrulefactorytype")) {
            mSBql
                .append("select distinct a.FactoryType,b.FactoryTypeName,trim(a.FactoryType)||trim(a.RiskCode),'' from LMFactoryMode a,LMFactoryType b where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql
                .append(" and a.FactoryType = b.FactoryType and a.FactoryType = '000006'");
            mSQL = mSBql.toString();
            //break SelectCode;
        }
        
//      团单险种归属规则查询RiskRuleFactoryNo
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).equalsIgnoreCase(
            "riskascriptionrulefactoryno")) {
            if (mCodeCondition.substring(1, 7).equals("000006")) {
                // 没有加入任何限制条件，以后扩展
                mSBql
                    .append("select PayPlanCode,PayPlanName,'' from LMDutyPay where AccPayClass in('4','7','8')"
                            + " and payplancode in (select payplancode from lmdutypayrela where dutycode"
                            + " in (select dutycode from lmriskduty where riskcode = '");
                //针对险种是4位编码的情况。 20070528
                System.out.println(mCodeCondition+"==========4577");
//                if (mCodeCondition.length() == 12) {
//                    mSBql.append(mCodeCondition.substring(7, 11));
//                } else {
                    mSBql.append(mCodeCondition.substring(7,(mCodeCondition.length()-1)));
//                }
                mSBql.append("'))");
                mSQL = mSBql.toString();
                //System.out.println("riskascriptionrulefactoryno="+mSQL);
            }
            //break SelectCode;
        }
//      团单险种归属规则查询RiskRuleFactory
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).equalsIgnoreCase(
            "riskascriptionrulefactory")) {
            mSBql
                .append("select FactoryCode||to_char(FactorySubCode),CalRemark,Params,FactoryName from LMFactoryMode where FactoryType = '");
            mSBql.append(mCodeCondition.substring(1, 7));
            mSBql.append("' and RiskCode='");
            mSBql.append(mCodeCondition.substring(7));
            mSBql.append(" order by FactoryCode,FactorySubCode ");
            mSQL = mSBql.toString();
            //break SelectCode;
        }
        //------gzh 增加完毕-----
        
        
              if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "uliriskgrp") == 0)
        {
            String mcurdate = PubFun.getCurrentDate();
            StringBuffer sql = new StringBuffer(255);
            sql.append("select RiskCode a, RiskName,'Risk' from LMRiskApp where ");
            sql.append(mConditionField);
            sql.append(" = ");
            sql.append(mCodeCondition);
            sql.append(" and RiskProp in ('G','A','B','D') ");
            sql.append(" and risktype4 = '4' ");
            sql.append(" and riskcode != '370301' ");
            sql.append(" and (EndDate is null or EndDate>'");
            sql.append(mcurdate);
            sql.append("') order by riskcode");
            mSQL = sql.toString();
        }
        
        //add by lzy 2015-7-6 团险万能产品停售之后仍需要录入结算利率
         if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
              "uliriskgrpbq") == 0)
         {
	          StringBuffer sql = new StringBuffer(255);
	          sql.append("select RiskCode a, RiskName,'Risk' from LMRiskApp where ");
	          sql.append(mConditionField);
	          sql.append(" = ");
	          sql.append(mCodeCondition);
	          sql.append(" and RiskProp ='G' ");
	          sql.append(" and risktype4 = '4' ");
	          sql.append(" order by riskcode");
	          mSQL = sql.toString();
         }
        
               // 工种代码引用PositionCode add by chenjg
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "positioncode") == 0)
        {
        	System.out.println(mConditionField+""+mConditionField);
            mSQL = "select Gradecode,Gradename from lcgrpposition where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by Gradecode";

            executeType = 1;
        }

        // 标准服务项目查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmservitemcode") == 0)
        {
            mSQL = "select   ServItemCode,ServItemName from LHHealthServItem where microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by ServItemCode";
        }

        // 服务计划档次
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmservplanlevel") == 0)
        {
            mSQL = "select  CodeName,Code from LDCode where codetype = 'servplanlevel' and microsecond(current timestamp)= microsecond(current timestamp) and "
                    + "CodeType = 'servplanlevel'";
        }
        // 查询服务计划代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmriskcode") == 0)
        {
            mSQL = "select  Riskcode,Riskname from lmriskapp where  microsecond(current timestamp)= microsecond(current timestamp) and  "
                    + " risktype2 ='5' ";
        }
        // 个人服务计划名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhservplanname") == 0)
        {
            mSQL = "select distinct ServPlanName,ServPlanLevel,ServPlanCode,ComID,ServPlayType from LHHealthServPlan where  microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }
        // 定价方式代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhserveritemcode") == 0)
        {
            mSQL = "select servpricecode,servpricename from LHServerItemPrice where  microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + mConditionField
                    + " like '"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }

        // 定价方式名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhserveritemname") == 0)
        {
            mSQL = "select servpricecode,servpricename from LHServerItemPrice where  microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }
        // 服务事件代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhservcasecode") == 0)
        {
            mSQL = "select ServCaseCode,ServCaseName from LHServCaseDef where  microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }
        // 服务任务代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhservtaskcode") == 0)
        {
            mSQL = "select ServTaskCode,ServTaskName from LHServTaskDef where  microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }
        // 服务任务编号
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhservtaskno") == 0)
        {
            mSQL = "select a.ServTaskNo , (select d.ServTaskName from LHServTaskDef d  where d.ServTaskCode=a.ServTaskCode),a.ServTaskCode from LHCaseTaskRela  a where  microsecond(current timestamp)= microsecond(current timestamp) and  "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'"

            ;
        }
        // 健康通讯代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhmessagecode") == 0)
        {
            mSQL = "select HmMessCode,HmMessName from LHMessManage where  microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }

        // 操作员代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhoperator") == 0)
        {
            mSQL = "select Usercode,Username from lduser where  microsecond(current timestamp)= microsecond(current timestamp)  and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%'";
        }

        // 第三方成本中MulLine的合同选项
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmcontraincost") == 0)
        {
            mSQL = " select aa, bb, cc, dd from "
                    + " ( select a.contraname as aa, "
                    + " ( select b.hospitname from ldhospital b where b.hospitcode = a.hospitcode) as bb, "
                    + " a.contrano as cc, a.hospitcode as dd from lhgroupcont a"
                    + " union select c.contraname as aa, "
                    + " ( select b.doctname from lddoctor b where b.doctno = c.doctno ) as bb, c.contrano as cc, c.doctno  as dd "
                    + " from lhunitcontra c)  as x "
            // + mConditionField + " like '%" +
            // mCodeCondition.substring(1, (mCodeCondition.length() - 1)).
            // trim()
            // + "%'"
            ;
        }

        // 第三方成本中MulLine的合同下责任
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "vvvvvvvv") == 0)
        {
            mSQL = " select a.dutyitemcode, (select b.dutyitemname from ldcontraitemduty b where b.dutyitemcode = a.dutyitemcode), a.contraitemno from lhcontitem a where  contrano = '"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "' ";
        }

        // 合同管理--责任项目名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hmdutyitemnamedef") == 0)
        {
            mSQL = "select distinct DutyItemCode,DutyItemName from LDContraItemDuty "
                    + " where microsecond(current timestamp)= microsecond(current timestamp) and dutyitemtye = '"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "'";
            System.out.println(mSQL);
        }

        // 四级疾病代码分类

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "ldiseasecodeclassify") == 0)
        {
            // System.out.println("123456789");
            String resu = mCodeCondition.substring(1, 2);
            // System.out.println(resu);
            if (resu.equals("1") || resu.equals("2"))
            {
                mSQL = "select icdbegincode||'-'||icdendcode, diseastype from LDicddiseastype where diseastypelevel='"
                        + mCodeCondition.substring(1, 2)
                        + "' and "
                        + " diseastype like '%"
                        + mCodeCondition.substring(2,
                                (mCodeCondition.length() - 1)).trim() + "%'";
                // System.out.println(mSQL);
            }
            else
            {
                mSQL = "select icdname,icdcode from lddisease a where a.frequencyflag = "
                        + mCodeCondition.substring(1, 2)
                        + " and "
                        + " icdname like '%"
                        + mCodeCondition.substring(2,
                                (mCodeCondition.length() - 1)).trim() + "%'";
            }
            System.out.println(mSQL);
        }

        // 医院专科信息模糊查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhspecname") == 0)
        {
            mSQL = " select Code, CodeName from ldcode where codetype = 'lhspecname' and codename like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' ";
        }

        // 事件实施管理，服务任务名称与服务项目进行匹配。
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lhservitemtaskcode") == 0)
        {
            mSQL = " select a.servtaskcode, (select distinct t.servtaskname from lhservtaskdef t where t.servtaskcode = a.servtaskcode) "
                    + " from  lhtaskservitemrela a where a.servitemcode = '"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "' ";
        }

        /** *******************理赔相关查询 */
        // 出险疾病查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lldiseas") == 0)
        {
            mSQL = "select  ICDName,ICDCode,tumflag,coalesce(RiskLevel,''),coalesce(RiskContinueFlag,'') from LDDisease where 1=1 and "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by ICDName";
        }

        //扫描修改单证类型查询--公共扫描修改
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "cqapplytype") == 0)
        {
            mSQL = "select code,codename from ldcode where 1=1 "
            	+" and codetype='esapplytype' and code not like 'LP%'";
        }
        
        //扫描修改单证类型查询--承保处理下的扫描修改
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "es_subtype") == 0)
        {
            mSQL = "select code,codename from ldcode where 1=1 "
            	+" and codetype='subtypedetail' and code not like 'LP%'";
        }
        
        // 出险疾病查询--非轻症
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llseriousdiseas") == 0)
        {
            mSQL = "select distinct code,name,description from llmserialsdiease where 1=1 and dieasetype!='6' and "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by code";
        }
        
        // 出险疾病查询--轻症
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llmdseriousdiseas") == 0)
        {
            mSQL = "select distinct code,name,description from llmserialsdiease where 1=1 and dieasetype='6' and "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by code";
        }

        // 理赔问题件处理人选择
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llissueop") == 0)
        {
            //
            System.out.print("mCodeCondition==" + mCodeCondition);

            if (StrTool.cTrim(mCodeCondition).equals("'02'"))
            {
                mSQL = "select usercode,username from lduser where "
                        + "claimpopedom = '2' and usercode ='"
                        + mConditionField + "'" + " order by claimpopedom desc";

            }
            else
            {
                mSQL = "select usercode,username,claimpopedom from llclaimuser where   "
                        // + mConditionField + " = " + mCodeCondition
                        + " upusercode = '"
                        + mGlobalInput.Operator
                        + "' order by claimpopedom desc";
            }

        }
        // 理赔材料名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llmaffix") == 0)
        {
            String tempCondtion = "";
            if (!StrTool.cTrim(mCodeCondition).equals("")
                    && !StrTool.cTrim(mCodeCondition).equals("''"))
            {
                tempCondtion = " and " + mConditionField + "=" + mCodeCondition;
            }
            mSQL = "select  affixcode,affixname,affixtypecode,affixtypename from llmaffix where 1=1  "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + tempCondtion + " order by affixcode";
        }
        //

        // 理赔材料类型
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llmaffixtype") == 0)
        {
            mSQL = "select distinct affixtypecode,affixtypename from llmaffix where 1=1 and "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by affixtypecode";
        }
        SelectCode: try
        { // 理赔合同查询
            if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                    .compareTo("llcont") == 0)
            {
                // mSQL = "select distinct contno,'' from lccont where " +
                // mConditionField + " = " + mCodeCondition +
                // " ";
                mSBql.append("select distinct contno,'' from lcinsured where ");
                mSBql.append(mConditionField);
                mSBql.append(" = ");
                mSBql.append(mCodeCondition);
                mSBql.append(" union select distinct contno,'' from lbinsured where ");
                mSBql.append(mConditionField);
                mSBql.append(" = ");
                mSBql.append(mCodeCondition);
                mSQL = mSBql.toString();
                break SelectCode;
            }
            // 理赔险种查询
            if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                    .compareTo("llrisk") == 0)
            {
                // mSQL =
                // "select distinct a.riskcode,b.riskname,a.polno from lcpol a,
                // lmrisk b where a.contno = "
                // + mCodeCondition + " and b.riskcode=a.riskcode";
            	 mSBql.append("select distinct a.riskcode,b.riskname,a.polno from lcpol a, lmrisk b where a.contno ");
                 mSBql.append(" = ");
                 mSBql.append(mCodeCondition);
                 mSBql.append(" and b.riskcode=a.riskcode");
                 mSBql.append(" union select distinct a.riskcode,b.riskname,a.polno from lbpol a, lmrisk b where a.contno ");
                 mSBql.append(" = ");
                 mSBql.append(mCodeCondition);
                 mSBql.append(" and b.riskcode=a.riskcode");
                mSQL = mSBql.toString();
                break SelectCode;
            }
        }
        catch (Exception ex)
        {
            System.out.println("原则上不会进入这个地方！");
        }

        // 理赔调查号查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llsurvey") == 0)
        {
            mSQL = "select substr(surveyno,18),'直接调查费',surveyno from llsurvey where  "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by surveyno";
        }

        // 理赔客户查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llinsured") == 0)
        {
            mSQL = "select distinct name,insuredno,idno,othidno,birthday"
                    + " from lcinsured where 1=1 and name =" + mCodeCondition
                    // +"'"+ mConditionField
                    + "  order by insuredno";

        }
        //理赔案件状态查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "rgtstate") == 0)
        {
        	mSQL = "select code,codename from ldcode where codetype='llrgtstate' order by code";

        }
        // 处理人上级查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llupusercode") == 0)
        {
            mSQL = "select distinct upusercode,(select username from llclaimuser b where b.usercode=a.upusercode fetch first 1 rows only) from llclaimuser a " 
            		+" where StateFlag<>'0' and usercode<>'claim'  and upusercode is not null and "
                    + mConditionField
                    + " like '"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by upusercode asc ";
        }
        // 理赔保单查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llclaimpolicy") == 0)
        {
            //mSQL = "select  a.contno,'' from llcasepolicy a where 1=1 and "
             mSQL = "select distinct(a.contno),'','','','','' from llcasepolicy a where 1=1 and "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField + " = " + mCodeCondition
                    + " order by a.contno";
        }
        // 理赔被保险人保单查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llinsucont") == 0)
        {
            System.out.println("mConditionField:" + mConditionField
                    + "...mCodeCondition:" + mCodeCondition);
            mSQL = "select distinct contno,appntname from lcpol where 1=1 and "
                    + mConditionField + " = " + mCodeCondition
                    + " order by contno";
        }

        // 险种查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llclaimrisk") == 0)
        {
            mSQL = "select distinct a.riskcode,b.riskname,a.polno from llcasepolicy a,lmrisk b where 1=1 and a.contno="
                    + mCodeCondition
                    + " and a.caseno='"
                    + mConditionField
                    + "' and b.riskcode=a.riskcode order by a.riskcode";
        }
        // 责任给付类别

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llgetdutykind") == 0)
        {
            mSQL = "select distinct a.GetDutyKind,b.codeName from LMDutyGetClm a , ldcode b where getdutycode in (select getdutycode from lmdutygetrela where dutycode in (select dutycode from lmriskduty where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " )) and b.codetype='getdutykind' and code=a.getdutykind order by getdutykind";
        }
        // 给付类型
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llclaimdecision_1") == 0)
        {
            mSQL = "select a.Code, a.CodeName, a.CodeAlias, a.ComCode, a.OtherSign from ldcode a where  trim(a.codetype)=(select trim(b.codeaLias) from ldcode b where b.codetype='llclaimdecision' and b.code="
                    + mCodeCondition + ") order by a.code ";
        }
        //客户风险等级原因
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskclassreason") == 0)
        {
            mSQL = "select a.Code, a.CodeName, a.CodeAlias, a.ComCode, a.OtherSign from ldcode a where  trim(a.codetype)=(select trim(b.codeaLias) from ldcode b where b.codetype='riskclass' and b.code="
                    + mCodeCondition + ")";
        }
        // 理赔费用项目查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llfeeitemtype") == 0)
        {
            mSQL = "select  code,codename,codealias from ldcode where substr(code,1,1) ="
                    + mCodeCondition
                    + " and codetype ='llfeeitemtype' order by code ";
        }
        // if
        // (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        // "llfeetypes") == 0) {
        // String FeeAtti = mConditionField.substring(0, 1);
        // String Comcode = mConditionField.substring(1);
        // if (Comcode.length() < 4 || Comcode.equals("8631")) {
        // Comcode = "8611";
        // }
        // if (FeeAtti.equals("1")) {
        // mSQL =
        // "select code,codename,codealias from ldcode where substr(code,1,1) ="
        // + mCodeCondition +
        // " and codetype ='llfeeitemtype' union "
        // +
        // "select code,codename,codealias from ldcode where
        // codetype='llfeeitemtype'"
        // + " and comcode='" + Comcode + "' order by code ";
        // } else {
        // mSQL =
        // "select code,codename,codealias from ldcode where substr(code,1,1) ="
        // + mCodeCondition
        // + "and codetype ='llfeeitemtype' order by code "
        // ;
        // }
        // }

        // 手术
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lloperation") == 0)
        {
            mSQL = "select  IcdOpsName,ICDopsCode,char(FrequencyFlag) from LDICDOps where  "
                    // + " and a.caseno='" + mCodeCondition +"'"
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by ICDOpsName";

        }
        // 其它录入要素类型
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llfactor") == 0)
        {
            mSQL = "select a.codename, a.code from ldcode a where a.codetype =( select CODEALIAS from ldcode where codetype='llotherfactor' and code="
                    + mCodeCondition + ") order by a.code";
        }

        // 核保师编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "uwusercode") == 0)
        {
        	mSQL = "select UserCode, trim(UserName) from LDUser "
                    + "where  UWPopedom is not null  and userstate='0' order by UserCode";
        }

        // 团单客户编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "supcustomerno") == 0)
        {
            mSQL = "select CustomerNo, trim(GrpName) from  LDGrp order by CustomerNo";
        }

        // 健康险要素目标编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "healthfactoryno") == 0)
        {

            if (mCodeCondition.substring(1, 7).equals("000000"))
            { // 基于保单的计算
                mSQL = "select '__','请录入保单号' from dual";
            }
            else if (mCodeCondition.substring(1, 7).equals("000001"))
            { // 基于保单的计算
                mSQL = "select DutyCode,DutyName from LMDuty where DutyCode in(select DutyCode from LMRiskDuty "
                        + " where RiskCode='"
                        + mCodeCondition.substring(7)
                        + ") order by DutyCode";
            }
            else if (mCodeCondition.substring(1, 7).equals("000002"))
            { // 基于给付的计算
                mSQL = "select getdutycode,getdutyname from lmdutygetrela where dutycode in (select dutycode from lmriskduty where riskcode ='"
                        + mCodeCondition.substring(7)
                        + ") order by getdutycode";
            }
            else if (mCodeCondition.substring(1, 7).equals("000003"))
            { // 基于账户的计算
                mSQL = "select insuaccno,insuaccname from LMRiskToAcc where RiskCode='"
                        + mCodeCondition.substring(7) + " order by insuaccno";
            }
            else if (mCodeCondition.substring(1, 7).equals("000004"))
            { // 基于理赔责任的计算
                mSQL = "select getdutycode,getdutyname from lmdutygetrela where dutycode in (select dutycode from lmriskduty where riskcode ='"
                        + mCodeCondition.substring(7)
                        + ") order by getdutycode";
            }
        }

        // 疾病代码查询ICDCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "icdcode") == 0)
        {
            mSQL = "select a.icdcode, a.icdname from  lduwaddfee a where "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by a.icdcode";
        }

        // 疾病代码查询ICDCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "diseascode") == 0)
        {
            mSQL = "select icdcode, icdname from lddisease order by a.icdcode";
        }

        // 疾病代码查询ICDCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "diseasname") == 0)
        {
            mSQL = "select icdname,icdcode from lddisease order by icdname";
        }

        // 医院模糊查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llhospiquery") == 0)
        {
            // mSQL = "select a.hospitcode,a.hospitname,b.codename from
            // LDHospital a ,ldcode b where b.codetype='llhospiflag' and
            // b.code=a.fixflag order by a.hospitcode";
            mSQL = "select a.hospitcode, a.hospitname,a.associateclass,a.levelcode,"
                    + "urbanoption,securityno from  ldhospital a where "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by a.hospitcode";

        }
        
        //医保通医院查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llhosp") == 0)
        {
            mSQL = "select distinct a.hospitcode, a.hospitname "
                    + " from  ldhospital a,llhospcomright b where "
                    + " a.managecom like '"+mGlobalInput.ManageCom
                    +"%' and a.hospitcode=b.hospitcode and "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by a.hospitcode";

        }
        
        // 社保医院查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llsechosquery") == 0)
        {
            mSQL = "select a.securityno,a.hospitname,a.hospitcode "
                    + " from  ldhospital a where "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by a.hospitcode";

        }
        // 医院查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "hospital") == 0)
        {
            mSQL = "select a.hospitcode,a.hospitname,b.codename from  LDHospital a ,ldcode b where b.codetype='llhospiflag' and b.code=a.associateclass order by a.hospitcode";

        }
        // 意外代码查询ICDCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llacci") == 0)
        {
            mSQL = "select a.accidentno, a.accname from  llaccidenttype a where "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by a.accidentno";
        }
        // 理赔师权限查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llclaimpopedom") == 0)
        {
            mSQL = "select distinct claimpopedom, popedomname from  llclaimpopedom where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by claimpopedom";
        }

        // 医生代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "doctor") == 0)
        {
            mSQL = "select a.doctname,a.doctno from  lddoctor a where  "
                    + mConditionField
                    + " like '%"
                    + mCodeCondition
                            .substring(1, (mCodeCondition.length() - 1)).trim()
                    + "%' order by a.doctname";
        }

        // 健康险计算要素编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "healthfactory") == 0)
        {
            mSQL = "select FactoryCode||to_char(FactorySubCode),CalRemark,Params from LMFactoryMode where FactoryType = '"
                    + mCodeCondition.substring(1, 7)
                    + "' and RiskCode='"
                    + mCodeCondition.substring(7)
                    + " order by FactoryCode,FactorySubCode ";
        }

        /**
        * 加款描述 by xiongxin at 20091020
        */
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                      "rewardtype") == 0)
        {
         mSQL = "select code2,description from LDCodeRela"
              + " where relatype='rewardtype' and code3='86' and " + mConditionField + " = "
              + mCodeCondition + "   order by code2";
        }

        /**
         * 扣款描述  by xiongxin at 20091020
         */

         if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                       "punishtype") == 0)
         {
          mSQL = "select code2,description from LDCodeRela"
               + " where relatype='punishtype'and code3='86'   and " + mConditionField + " = "
               + mCodeCondition + " order by code2";
         }
         /**
          * 加款描述 by xiongxin at 20081020
          */
          if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                                      "addproject") == 0)
          {
           mSQL = "select code1,description from LDCodeRela"
                  + " where relatype='addproject' and " + mConditionField + " = "
                  + mCodeCondition + "   order by code1";
           }

           /**
            * 扣款描述  by xiongxin at 20081020
            */
            if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                          "purproject") == 0)
            {
                  mSQL = "select code1,description from LDCodeRela"
                     + " where relatype='purproject' and " + mConditionField + " = "
                     + mCodeCondition + " order by code1";
                }

           /**
            * 扣款描述  by xiongxin at 20081020
            */
             if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                         "approject") == 0)
             {
                 mSQL = "select code1,description from LDCodeRela"
                 + " where relatype='approject' and " + mConditionField + " = "
                 + mCodeCondition + " order by code1";
             }

             /**
             * 扣款描述  by xiongxin at 20081020
             */
            if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                           "reputype") == 0)
            {
             mSQL = "select code2,description from LDCodeRela"
                    + " where relatype='reputype' and code3='86'   and " + mConditionField + " = "
                   + mCodeCondition + " order by code2";
            }


            if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                       "payyearsbkc") == 0)
            {
                mSQL = "select code2,description from LDCodeRela"
                   + " where relatype='payyearsbkc' and " + mConditionField + " = "
                   + mCodeCondition + "   order by code1";
           }
        // 保监会Excel表数据信息导入配置模板相对文件名
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "circreport_config") == 0)
        {
            mSQL = "select code,codeName,codealias from ldcode where codetype='circreport_config' order by code";
        }

        // 保监会管理机构信息
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "itemcode") == 0)
        {
            mSQL = "select itemcode,trim(ItemName) from lfItemRela order by itemcode";
            executeType = 1;
        }
        // 保监会管理机构信息
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "outitemcode") == 0)
        {
            mSQL = "select outitemcode,trim(ItemName) from lfItemRela order by outitemcode";
            executeType = 1;
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "stati") == 0)
        {
            mSQL = "select Code, CodeName, CodeAlias, ComCode, OtherSign from ldcode where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and codetype = 'station' order by code";
        }
        // 理赔人查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "optname") == 0)
        {
            mSQL = "select usercode,username,claimpopedom from llclaimuser where stateflag='1' and "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by usercode asc ";
        }
        // 理赔账单录入人员查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llinputer") == 0)
        {
            mSQL = "select usercode,username from lduser where "
                    + "claimpopedom = '2' "
                    // + mConditionField + " = " + mCodeCondition
                    + " order by claimpopedom desc";
        }

        // 操作员代码--GN
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "optnameunclass") == 0)
        {
            mSQL = "select usercode,username from llclaimuser where StateFlag<>'0' and usercode<>'claim' order by usercode";
        }
        // 理赔查询中客户查询-GN
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "findcus") == 0)
        {
            mSQL = " select Name, CustomerNo, IDNo from LDPerson where 1=1 and "
                    + mConditionField + " like '%" + mCodeCondition + "%'";

        }

        // 保监会管理机构信息
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "comcodeisc") == 0)
        {
            mSQL = "select comcodeisc,trim(name) from LFComISC order by comcodeisc";
            executeType = 1;
        }

        // 保单状态导致原因PolState
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "polstate2") == 0)
        {
            mSQL = "select code,codename,codealias from ldcode where codetype = 'polstate' order by code";
        }

        // 保单状态导致原因PolStateReason
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "polstatereason") == 0)
        {
            mSQL = "select code,codename,codealias from ldcode where "
                    + mConditionField + " = " + mCodeCondition
                    + " and codetype = 'polstatereason' order by code";
        }

        // 责任领取类型DutyKind
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "dutykind") == 0)
        {
            mSQL = "select GetDutyKind, GetDutyName from LMDutyGetAlive where "
                    + mConditionField + " = " + mCodeCondition
                    // + " and GetDutyCode in ( select GetDutyCode from
                    // LMDutyGetRela where dutycode in "
                    // + " ( select dutycode from LMRiskDuty where
                    // riskcode='212401' ) ) "
                    + " order by GetDutyKind";
        }

        // 保全项目EdorType
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "edortype") == 0)
        {
            mSQL = "select distinct EdorCode, EdorName from LMRiskEdoritem where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by EdorCode";
        }

        // 操作岗位OperateType
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "operatetype") == 0)
        {
            mSQL = "select distinct OperateType,Remark from LDRiskComOperate where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by OperateType";
        }

        // 核保上报级别UWPopedomCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "uwpopedomcode") == 0)
        {
            mSQL = "select usercode, username from lduser where "
                    + mConditionField + " = " + mCodeCondition
                    + " order by usercode";
        }

        // 核保上报级别UWPopedomCode1
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "uwpopedomcode1") == 0)
        {
            mSQL = "select usercode, username from lduser where usercode = (select UpUserCode from LDUWUser where usercode = '"
                    + mGlobalInput.Operator.trim() + "') order by usercode";
        }

        // 银行分行渠道channel
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "channel") == 0)
        {
            mSQL = "select agentcom,name from lacom where " + mConditionField
                    + " = " + mCodeCondition
                    + "and  banktype ='01' order by agentcom";
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "upagentcode") == 0)
        {
            mSQL = "select agentcom,name from lacom where " + mConditionField
                    + " = " + mCodeCondition
                    + " and AcType='01' order by agentcom";
        }

        // 工种代码引用StaticGroup
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "staticgroup") == 0)
        {
            // mSQL = "select comcode,shortname from ldcom where "
            // + mConditionField + " = " + mCodeCondition
            // + "and comcode like '"
            // + mGlobalInput.ManageCom +
            // "%' union select branchattr,name from labranchgroup where
            // ManageCom like '"
            // + mGlobalInput.ManageCom +
            // "%' and branchlevel='03' and branchtype='1'";
            mSBql.append("select comcode,shortname from ldcom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and comcode like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql
                    .append("%' union select branchattr,name from labranchgroup where ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' and branchlevel='03' and branchtype='1'");

            mSQL = mSBql.toString();

            executeType = 1;
        }

        // 工种代码引用Depart
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "depart") == 0)
        {
            // mSQL = "select branchattr,name from labranchgroup where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '"
            // + mGlobalInput.ManageCom +
            // "%'and branchlevel>='02' and branchtype='1' order by branchattr";
            mSBql.append("select branchattr,name from labranchgroup where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql
                    .append("%'and branchlevel>='02' and branchtype='1' order by branchattr");

            mSQL = mSBql.toString();
            executeType = 1;
        }

        // 工种代码引用Occupation
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "occupation") == 0)
        {
            mSQL = "select code,codename from ldcode where " + mConditionField
                    + " = " + mCodeCondition + " and codetype='occupation'";
        }

        // 引用BranchAttr
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "branchattr") == 0)
        {
            // mSQL = "select BranchAttr, Name from LABranchGroup where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '"
            // + mGlobalInput.ManageCom + "%' order by BranchAttr";
            mSBql.append("select BranchAttr, Name from LABranchGroup where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by BranchAttr");

            mSQL = mSBql.toString();
            executeType = 1;
        }


    // 按照页面上的管理机构（like）选择个险的branchattr
              if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                      "branchattr2") == 0)
              {
                  // mSQL = "select AgentGroup, Name from LABranchGroup where "
                  // + mConditionField + " = " + mCodeCondition
                  // + " and BranchLevel = '01' and ManageCom like '"
                  // + mGlobalInput.ManageCom + "%' order by AgentGroup";

                  mSBql.append("select BranchAttr, Name,AgentGroup from LABranchGroup where ");
                  mSBql.append(mConditionField);
                  mSBql.append(" like '");
                  mSBql.append(mCodeCondition.substring(1,
                          (mCodeCondition.length() - 1)).trim());
                  mSBql.append("%' and branchtype='2' and branchtype2='01'");
                  mSBql.append(" order by BranchAttr");
                  mSQL = mSBql.toString();
        }
        
        // 营销机构和代理人机构查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "branchattrandagentgroup") == 0)
        {
            System.out.println("code condition : " + mCodeCondition
                    + "  and condition field : " + mConditionField);
            if (mCodeCondition != null && !mCodeCondition.equals(""))
            {
                String branchType = "";
                String branchType2 = "";
                if (mCodeCondition.equals("'01'"))
                {
                    branchType = "1";
                    branchType2 = "01";
                }
                else if (mCodeCondition.equals("'02'"))
                {
                    branchType = "2";
                    branchType2 = "01";
                }
                else if (mCodeCondition.equals("'03'"))
                {
                    branchType = "2";
                    branchType2 = "02";
                }
                else if (mCodeCondition.equals("'04'"))
                {
                    branchType = "3";
                    branchType2 = "01";
                }
                else if (mCodeCondition.equals("'05'"))
                {
                    branchType = "";
                    branchType2 = "";
                }
                else if (mCodeCondition.equals("'06'"))
                {
                    branchType = "1";
                    branchType2 = "03";
                }
                else if (mCodeCondition.equals("'07'"))
                {
                    branchType = "2";
                    branchType2 = "01";
                }
                else if (mCodeCondition.equals("'08'"))
                {
                    branchType = "9";
                    branchType2 = "01";
                }else if (mCodeCondition.equals("'24'"))
                {
                    branchType = "1";
                    branchType2 = "06";
                }

                mSQL = "select branchattr, name, agentgroup from LABranchGroup "
                        + "where ManageCom like '"
                        + mGlobalInput.ManageCom
                        + "%' and branchType = '"
                        + branchType
                        + "' and branchType2 = '"
                        + branchType2
                        + "' order by branchattr";
            }
        }

        // 代理人组别引用BranchCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "branchcode") == 0)
        {
            mSQL = "select agentgroup, name from labranchgroup where "
                    + mConditionField + " = " + mCodeCondition
                    + " and ManageCom like '" + mGlobalInput.ManageCom
                    + "%' order by branchattr";
            executeType = 1;
        }
        // 考核建议职级
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "assessgrade1") == 0)
        {

            if (mCodeCondition.equals("'A08'")
                    || mCodeCondition.equals("'B04'"))
            {
                mSQL = "select  gradecode,gradename from  laagentgrade where  branchtype='1' and branchtype2='01'  and  "
                        + "  (select b.gradeid from laagentgrade b where b.gradecode="
                        + mCodeCondition + ") in (gradeid,gradeid+1) "

                        + "  order by gradecode";

            }
            else if (mCodeCondition.equals("'B01'"))
            {
                mSQL = "select  gradecode,gradename from  laagentgrade where  branchtype='1' and branchtype2='01'  and  "
                        + "  (select b.gradeid from laagentgrade b where b.gradecode="
                        + mCodeCondition + ") in (gradeid,gradeid-1)"

                        + "  order by gradecode";

            }
            else
            {
                mSQL = "select  gradecode,gradename from  laagentgrade where  branchtype='1' and branchtype2='01'  and  "
                        + " abs(gradeid -(select b.gradeid from laagentgrade b where b.gradecode="
                        + mCodeCondition + "))<=1 "

                        + "  order by gradecode";
            }
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "assessgrade2") == 0)
        {

            mSQL = "select  gradecode,gradename  from  laagentgrade where 1=1  and "
                    + mConditionField
                    + " ="
                    + mCodeCondition
                    + " order by gradecode ";

        }
        // 代理人组别引用BranchCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "allbranch") == 0)
        {
            // mSQL = "select comcode,name from ldcom where "
            // + mConditionField + " = " + mCodeCondition
            // + "and comcode like '" + mGlobalInput.ManageCom + "%' union
            // select branchattr,name from labranchgroup where branchtype='1'
            // and (branchlevel='03' or branchlevel='02') and managecom like '"
            // +
            // mGlobalInput.ManageCom +
            // "%' and (state<>1 or state is null) order by comcode";

            mSBql.append("select comcode,name from ldcom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append("and comcode like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql
                    .append("%' union select branchattr,name from labranchgroup where branchtype='1' and (branchlevel='03' or branchlevel='02') and managecom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' and (state<>1 or state is null) order by comcode");

            mSQL = mSBql.toString();

            executeType = 1;
        }

        // 员工属性引用BranchCodeType
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "branchcodetype") == 0)
        {
            mSQL = "select gradecode, gradename from laagentgrade where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and branchtype='1' and gradeproperty6='1' order by gradecode";
        }

        // 代理人组别引用HealthCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "healthcode") == 0)
        {
            mSQL = "select distinct HealthCode, HealthName from LDHealth where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by HealthCode";
        }

        // 个险契调引用RReportCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "rreportcode1") == 0)
        {
            mSQL = "select rreportcode, RReportName from LDRReport where "
                    + mConditionField + " = " + mCodeCondition
                    + " and rreportclass = '1' order by rreportcode";
        }

        // 团险契调引用RReportCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "rreportcode2") == 0)
        {
            mSQL = "select rreportcode, RReportName from LDRReport where "
                    + mConditionField + " = " + mCodeCondition
                    + " and rreportclass = '2' order by rreportcode";
        }

        // 代理人组别引用AgentGroup
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentgroup") == 0)
        {
            // mSQL = "select AgentGroup, Name from LABranchGroup where "
            // + mConditionField + " = " + mCodeCondition
            // + " and BranchLevel = '01' and ManageCom like '"
            // + mGlobalInput.ManageCom + "%' order by AgentGroup";

            mSBql.append("select AgentGroup, Name from LABranchGroup where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and BranchLevel = '01' and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentGroup");

            mSQL = mSBql.toString();
        }

        // qulq 061212 通过参数判定是使用那一级代理机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentgroupbq") == 0)
        {
            mSBql.append("select AgentGroup, Name from LABranchGroup where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentGroup");

            mSQL = mSBql.toString();
        }
        
        //取个险部及银保部级下拉语句
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentgroupbq3") == 0)
        {
            mSBql.append("select AgentGroup, Name from LABranchGroup where ");
            mSBql.append(mConditionField);
            mSBql.append(" in ");
            mSBql.append(" ('03','43') ");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentGroup");

            mSQL = mSBql.toString();
        }
        
        // 按照页面上的管理机构（like）选择个险的agentgroup
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentgroup1") == 0)
        {
            // mSQL = "select AgentGroup, Name from LABranchGroup where "
            // + mConditionField + " = " + mCodeCondition
            // + " and BranchLevel = '01' and ManageCom like '"
            // + mGlobalInput.ManageCom + "%' order by AgentGroup";

            mSBql.append("select AgentGroup, Name from LABranchGroup where ");
            mSBql.append(mConditionField);
            mSBql.append(" like '");
            mSBql.append(mCodeCondition.substring(1,
                    (mCodeCondition.length() - 1)).trim());
            mSBql.append("%' and branchtype='1'");
            mSBql.append(" order by AgentGroup");

            mSQL = mSBql.toString();
        }
        // 按照页面上的管理机构（like）选择团险的agentgroup
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentgroup2") == 0)
        {
            // mSQL = "select AgentGroup, Name from LABranchGroup where "
            // + mConditionField + " = " + mCodeCondition
            // + " and BranchLevel = '01' and ManageCom like '"
            // + mGlobalInput.ManageCom + "%' order by AgentGroup";

            mSBql.append("select AgentGroup, Name from LABranchGroup where ");
            mSBql.append(mConditionField);
            mSBql.append(" like '");
            mSBql.append(mCodeCondition.substring(1,
                    (mCodeCondition.length() - 1)).trim());
            mSBql.append("%' and branchtype='2'");
            mSBql.append(" order by AgentGroup");

            mSQL = mSBql.toString();
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "edortypecode") == 0)
        {
            mSBql.append("select edorcode,edorname from lmedoritem");

            mSQL = mSBql.toString();
        }
        // 退保类型引用EdorCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "edorcode") == 0)
        {
            String riskCode;
            String dealWMD = " ";
            StringBuffer sql = new StringBuffer();
            if(!mCodeCondition.equals("1")){
            // 修改为利用保单号查找，先查个单表再查团单表，找到就置
            // 个单，mCodeCondition= "'" + contNo + "'"
            String flag = "";
            String querySql = "select 1 from lccont where contno ="
                    + mCodeCondition;
            SSRS temp = mExeSQL.execSQL(querySql);
            if (temp.getMaxRow() > 0)
            {
                flag = "P"; // 个人
            }
            querySql = "select 1 from lcgrpcont where grpcontno ="
                    + mCodeCondition;
            temp = mExeSQL.execSQL(querySql);
            if (temp.getMaxRow() > 0)
            {
                flag = "G"; // 团单
            }

            if (flag.equals("P"))
            {
                riskCode = " (select riskCode from LCPol " + "where contNo = "
                        + mCodeCondition + ") ";
            }
            else if (flag.equals("G"))
            {
				String wrapsql = " (select 1 from lccontplanrisk where grpcontno = " + mCodeCondition + " and riskwrapcode is not null)";
				SSRS w = mExeSQL.execSQL(wrapsql);
				if (w.getMaxRow() > 0) {
					riskCode = " (select riskwrapcode from lccontplanrisk where grpcontno = " + mCodeCondition + ")";
					String edorSql = "select 1 from lmriskedoritem where riskcode in " + riskCode + "";
					String edor = mExeSQL.getOneValue(edorSql);
					if (!"1".equals(edor)) {
						riskCode = " (select riskCode from LCGrpPol " + "where grpContNo = " + mCodeCondition + ") ";
						// 若能查出数据，则保单为无名单
						sql.append("select 1 ").append("from LCCont ")
								.append("where polType = '1' ").append(
										"   and grpContNo = ").append(
										mCodeCondition);
						String t = new ExeSQL().getOneValue(sql.toString());

						if (t.equals("1")) {
							dealWMD = " and (b.edorTypeFlag != 'N' or b.edorTypeFlag is null)";
						} else {
							dealWMD = " and (b.edorTypeFlag != 'W' or b.edorTypeFlag is null)";
						}
					}
				} else {
					riskCode = " (select riskCode from LCGrpPol "
							+ "where grpContNo = " + mCodeCondition + ") ";
					// 若能查出数据，则保单为无名单
					sql.append("select 1 ").append("from LCCont ").append(
							"where polType = '1' ").append(
							"   and grpContNo = ").append(mCodeCondition);
					String t = new ExeSQL().getOneValue(sql.toString());

					if (t.equals("1")) {
						dealWMD = " and (b.edorTypeFlag != 'N' or b.edorTypeFlag is null)";
					} else {
						dealWMD = " and (b.edorTypeFlag != 'W' or b.edorTypeFlag is null)";
					}
				}
			}
            else
            {
                riskCode = " (select riskCode from LMRisk) ";
            }
            }
            else{
            	riskCode = " (select riskCode from LMRisk) ";
            }

            sql.delete(0, sql.length());
            sql.append("select distinct b.EdorCode, b.EdorName ").append(
                    "from LMRiskEdoritem  a, LMEdorItem b ").append(
                    "where a.edorCode = b.edorCode and b.edorcode != 'XB'")
                    .append("   and a.riskCode in ").append(riskCode).append(
                            dealWMD).append("order by EdorCode ");
            mSQL = sql.toString();
        }

        // 代理机构引用AgentCom
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcom") == 0)
        {
            // mSQL =
            // "select AgentCom, Name, UpAgentCom, AreaType, ChannelType from
            // LACom where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom + "%' order by
            // AgentCom";

            mSBql
                    .append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom");

            mSQL = mSBql.toString();

            executeType = 1;
        }
        //单证批量发放管理  接收者配置
        if(StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("dzwd")==0){
       	 mSQL ="select code,codename from ldcode where codetype='dzwd'";
        }


   //单证设置1.16
   if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
   "dzwdcode") == 0){
   	
   	mSQL="select code,codename from ldcode where codetype='dzwdcode'";
   	
   }
        
        //      银行代理机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcom1") == 0)
        {
            // mSQL =
            // "select AgentCom, Name, UpAgentCom, AreaType, ChannelType from
            // LACom where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom + "%' order by
            // AgentCom";

            mSBql
                    .append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and branchtype='3'");
            mSBql.append(" and branchtype2='01'");
            //  mSBql.append(" and banktype<>'01'");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom");

            mSQL = mSBql.toString();

            executeType = 1;
        }
        
        
                // 套餐编码引用WrapCode by 20091223
      if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
              .toLowerCase().compareTo("wrapcode") == 0)
      {
           mSBql.append("select RiskWrapCode,WrapName from LDWrap where  ");
           mSBql.append(mConditionField);
           mSBql.append(" = ");
           mSBql.append(mCodeCondition);
           mSBql.append(" order by RiskWrapCode");

           mSQL = mSBql.toString();
           executeType = 1;
      }
      
      // 简易平台套餐编码引用WrapCode by 20160509
      if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
              .toLowerCase().compareTo("wrapcode1") == 0)
      {
           mSBql.append("select RiskWrapCode,WrapName from LDWrap where  ");
           mSBql.append(mConditionField);
           mSBql.append(" = ");
           mSBql.append(mCodeCondition);
           mSBql.append(" and wraptype='2'");
           mSBql.append(" order by RiskWrapCode");

           mSQL = mSBql.toString();

      }
      
//      银行代理机构 by alsa
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcom2") == 0)
        {
            // mSQL =
            // "select AgentCom, Name, UpAgentCom, AreaType, ChannelType from
            // LACom where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom + "%' order by
            // AgentCom";

            
        	mSBql.append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and branchtype='2'");
            mSBql.append(" and branchtype2='02'");
            //  mSBql.append(" and banktype<>'01'");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom");

            mSQL = mSBql.toString();

            executeType = 1;
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcom3") == 0)
        {
            // mSQL =
            // "select AgentCom, Name, UpAgentCom, AreaType, ChannelType from
            // LACom where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom + "%' order by
            // AgentCom";

            
        	mSBql.append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and branchtype='1'");
            mSBql.append(" and branchtype2='02'");
            //  mSBql.append(" and banktype<>'01'");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom");

            mSQL = mSBql.toString();

            executeType = 1;
        }
      //互动中介代理结构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcom4") == 0)
        {
            // mSQL =
            // "select AgentCom, Name, UpAgentCom, AreaType, ChannelType from
            // LACom where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom + "%' order by
            // AgentCom";

            
        	mSBql.append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and branchtype='5'");
            mSBql.append(" and branchtype2='01'");
            //  mSBql.append(" and banktype<>'01'");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom");

            mSQL = mSBql.toString();

            executeType = 1;
        }
        // 肿瘤形态学代码查询//tmm
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "tumour") == 0)
        {
            mSQL = "select  LDName,LDCode from LDTumour where 1=1 and "
                + mConditionField
                + " like '%"
                + mCodeCondition
                        .substring(1, (mCodeCondition.length() - 1)).trim()
                + "%' order by LDName";

        }
		
		
//电商中介代理机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcom7") == 0)
        {
            // mSQL =
            // "select AgentCom, Name, UpAgentCom, AreaType, ChannelType from
            // LACom where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom + "%' order by
            // AgentCom";

            
        	mSBql.append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and branchtype='8'");
            mSBql.append(" and branchtype2='02'");
            //  mSBql.append(" and banktype<>'01'");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom");

            mSQL = mSBql.toString();

            executeType = 1;
        }
      //健康管理中介代理机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcom5") == 0)
        {
            // mSQL =
            // "select AgentCom, Name, UpAgentCom, AreaType, ChannelType from
            // LACom where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom + "%' order by
            // AgentCom";

            
        	mSBql.append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and branchtype='7'");
            mSBql.append(" and branchtype2='01'");
            //  mSBql.append(" and banktype<>'01'");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom");

            mSQL = mSBql.toString();

            executeType = 1;
        }		
		
 //保单投保项目编码和项目名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        		"projectaskmode") == 0) {
        	String com = mGlobalInput.ManageCom;
                	mSQL = "select projectno,projectname from LCProjectInfo where 1=1 and "
                			+ mConditionField
                            + " like '%"
                            + mCodeCondition.substring(1, (mCodeCondition.length() - 1)).trim()
                            + "%' with ur"
                            ;
                	
                }	
        
      //社保综拓中介代理结构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcom6") == 0)
        {
        	mSBql.append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and branchtype='6'");
            mSBql.append(" and branchtype2='02'");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom");

            mSQL = mSBql.toString();

            executeType = 1;
        }
        
      
        // 银行分行渠道BankCharge
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "BankCharge") == 0)
        {
            mSQL = "select Code, CodeName, CodeAlias, ComCode, OtherSign from ldcode where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + "and codetype = 'bankcharge' order by Code";
        }
        
        // 外包
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "bjwbbl") == 0)
        {
            mSQL = "select Code, CodeName, CodeAlias, ComCode, OtherSign from ldcode where "
                    + " codetype = 'bjwbbl'"
                    + " union "
                    + " select '', '', '','',''  from dual "
                    + " order by code";
        }
        
        // 理赔附件材料查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "llsurveyaffix") == 0)
        {
            mSQL = "select Affixcode, Affixname from LLMAffix where "
                    + mConditionField + " = " + mCodeCondition;
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "edorcodeforview") == 0)
        {
            mSQL = "select distinct EdorCode, EdorName " + "from LMEdorItem "
                    + "order by EdorCode";
        }

        // 险种编码引用RiskCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                .toLowerCase().compareTo("riskcode") == 0)
        {
            mSQL = "select RiskCode,RiskName from LMRisk where "
                    + mConditionField + " = " + mCodeCondition
                    + " order by RiskCode";
            
        }
        
     // 再保临分团险险种查询  add by lh 2010-4-26
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                .toLowerCase().compareTo("riskcode2") == 0)
        {
            mSQL = "select RiskCode,RiskName from LMRisk where "
                    + mConditionField + " in " + mCodeCondition
                    + " order by RiskCode";
        }
        
        //      险种编码引用RiskCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                .toLowerCase().compareTo("gwriskcode") == 0)
        {
            String tempStr=null;
            if(mConditionField.equals("1")){
            	tempStr="0";
            }else{
            	tempStr="1";
            }
        	mSQL = "select distinct Codename,(select RiskName from lmrisk where " +
            		"riskcode=a.codename) from LDcode a where "
                    +"codetype='GWRISKCODE' and ((comcode= " + mCodeCondition
                    + " and othersign='"+mConditionField+"') or " 
                    +"(comcode='86' and othersign='"+mConditionField+"' " 
                    +"and not exists(select 1 from ldcode where codetype='GWRISKCODE' and comcode="
                    +mCodeCondition+" and othersign='"+tempStr+"' and codename=a.codename))) order by Codename";
        }
        
//      再保险种编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "criskcode") == 0)
        {
        	String ReComCode = "";
        	String RecontCode = "";
        	System.out.println("mCodeCondition-----------:" + mCodeCondition);
        
        	if(mCodeCondition != null && !mCodeCondition.trim().equals("") && !mCodeCondition.trim().equals("'|'")){
        		mCodeCondition = mCodeCondition.substring(1, mCodeCondition.length()-1);
        		String[] ReArray =  mCodeCondition.replace('|', '@').split("@");
        		
        		ReComCode = ReArray[0];
        		if(ReArray.length>1)
        		RecontCode = ReArray[1];
        	}
        	
        	System.out.println("ReComCode:" + ReComCode + " RecontCode:" + RecontCode);
        	
        	//既有公司又有合同号
        	if(!ReComCode.trim().equals("") && !RecontCode.trim().equals("")){
        		mSQL = " select c.Riskcode, c.Riskname from LMRisk c,"
        			   + " (select distinct b.Riskcode Riskcode from lrcontinfo a, LRCalFactorValue b"
        			   + " where a.recontcode=b.recontcode and a.Recomcode='" + ReComCode + "' AND b.Recontcode='" + RecontCode + "') d"
        			   + " where c.Riskcode = d.Riskcode with ur";
        	}else if(!ReComCode.trim().equals("")){//只有公司
        		mSQL = " select distinct a.Riskcode, a.Riskname from LMRisk a,LRCalFactorValue b,"
        			 + " (select Recontcode from lrcontinfo where Recomcode='" + ReComCode + "') c"
        			 + " where b.Recontcode = c.Recontcode AND a.Riskcode=b.Riskcode with ur";
        	}else if(!RecontCode.trim().equals("")){
        		mSQL = " select distinct a.Riskcode, a.Riskname from LMRisk a, LRCalFactorValue b "
        			 + " where a.Riskcode=b.Riskcode AND b.Recontcode='" + RecontCode + "' with ur";
        	}else{
        		mSQL = " select distinct Riskcode, Riskname from LMRisk with ur ";
        	}
        	
        	System.out.println("mSQL:" + mSQL);
        }
        
        // 险种编码引用RiskCode1
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                .toLowerCase().compareTo("riskcode1") == 0)
        {
            mSQL = "select a.RiskCode, a.RiskName,b.SubRiskFlag,b.SubRiskFlag from LMRisk a,LMRiskApp b where a.RiskCode=b.RiskCode order by a.RiskCode";
        }

        // 险种版本引用RiskVersion
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskversion") == 0)
        {
            mSQL = "select RiskVer from LMRisk where " + mConditionField
                    + " = " + mCodeCondition + " order by RiskVer";
        }
        //险种给付责任查询。引用getDutyCode
       if(StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
              "getdutycode") == 0)
         {
             mSQL = "select getdutycode,getdutyname from lmdutyget where getdutycode in (select getdutycode from lmdutygetrela where dutycode in "+
                  "(select dutycode from lmriskduty where "+mConditionField+"="+mCodeCondition+")) with ur";
         }


        // 机构编码引用ComCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "comcode") == 0)
        {
            // mSQL =
            // "select ComCode, Name, ShortName, Address, Sign from ldcom where
            // "
            // + mConditionField + " = " + mCodeCondition
            // + " and comcode like '"
            // + mGlobalInput.ManageCom + "%' order by comcode";
            mSBql
                    .append("select ComCode, Name, ShortName, Address from ldcom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and Sign='1' and comcode like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by comcode");

            mSQL = mSBql.toString();
        }
        // 机构编码引用ComCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "comcodejx") == 0)
        {
            // mSQL =
            // "select ComCode, Name, ShortName, Address, Sign from ldcom where
            // "
            // + mConditionField + " = " + mCodeCondition
            // + " and comcode like '"
            // + mGlobalInput.ManageCom + "%' order by comcode";
            mSBql
                    .append("select ComCode, Name,ComCode from ldcom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and Sign='1' and comcode like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by comcode");

            mSQL = mSBql.toString();
        }
        
 // 机构编码引用ComCode 只等查询8位的分公司，且不为营业部
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "comcode1") == 0)
        {
            // mSQL =
            // "select ComCode, Name, ShortName, Address, Sign from ldcom where
            // "
            // + mConditionField + " = " + mCodeCondition
            // + " and comcode like '"
            // + mGlobalInput.ManageCom + "%' order by comcode";
            mSBql
                    .append("select ComCode, Name, ShortName, Address from ldcom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and Sign='1'   and comcode like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by comcode");

            mSQL = mSBql.toString();
        }
        // 机构编码引用ComCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "comcode0") == 0)
        {
            // mSQL =
            // "select ComCode, Name, ShortName, Address, Sign from ldcom where
            // "
            // + mConditionField + " = " + mCodeCondition
            // + " and comcode like '"
            // + mGlobalInput.ManageCom + "%' order by comcode";
            mSBql
                    .append("select ComCode, Name, ShortName, Address from ldcom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and comcode like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by comcode");

            mSQL = mSBql.toString();
        }
        // 机构编码引用ComCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "comcode8") == 0)
        {
            // mSQL =
            // "select ComCode, Name, ShortName, Address, Sign from ldcom where
            // "
            // + mConditionField + " = " + mCodeCondition
            // + " and comcode like '"
            // + mGlobalInput.ManageCom + "%' order by comcode";
            mSBql
                    .append("select ComCode, Name, ShortName, Address from ldcom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql
                    .append(" and ComGrade = '03' and Sign='1' and comcode like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by comcode");

            mSQL = mSBql.toString();
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "comcode4") == 0)
        {
            // mSQL =
            // "select ComCode, Name, ShortName, Address, Sign from ldcom where
            // "
            // + mConditionField + " = " + mCodeCondition
            // + " and comcode like '"
            // + mGlobalInput.ManageCom + "%' and length(trim(comcode))=4 order
            // by comcode";

            mSBql
                    .append("select ComCode, Name, ShortName, Address, Sign from ldcom where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and comcode like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' and length(trim(comcode))=4 order by comcode");

            mSQL = mSBql.toString();
        }
        

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "comcode10") == 0)
        {
        	// mSQL =
        	// "select ComCode, Name, ShortName, Address, Sign from ldcom where
        	// "
        	// + mConditionField + " = " + mCodeCondition
        	// + " and comcode like '"
        	// + mGlobalInput.ManageCom + "%' and length(trim(comcode))=4 order
        	// by comcode";
        	
        	mSBql
        	.append("select ComCode, Name, ShortName, Address, Sign from ldcom where ");
        	mSBql.append(mConditionField);
        	mSBql.append(" = ");
        	mSBql.append(mCodeCondition);
        	mSBql.append(" and comcode like '");
        	mSBql.append(mGlobalInput.ManageCom);
        	mSBql.append("%' and (length(trim(comcode))=4 or trim(comcode)='86') order by comcode");
        	
        	mSQL = mSBql.toString();
        }        

        // 机构编码引用ComCodeAll
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "comcodeall") == 0)
        {
            mSQL = "select ComCode, Name, ShortName, Address, Sign from ldcom where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by comcode";
        }
        
//      机构编码引用ComCodeAllSign
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "comcodeallsign") == 0)
        {
            mSQL = "select ComCode, Name, ShortName, Address, Sign from ldcom where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and Sign = '1' order by comcode";
        }
        
        //单证查询接收者
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "receivecodesearch") == 0)
        {
            if (mGlobalInput.ComCode.length() == 2)
            {
                mSQL = "select 'A'||ComCode, Name from ldcom where 1=1 and Sign='1' and "
                        + " length(trim(comcode))=4 order by comcode ";
            }
            else if (mGlobalInput.ComCode.length() == 4)
            {
                mSQL = "select 'A'||Comcode,name from ldcom where comcode="+mCodeCondition+" union "
                        + " (select 'B'||usercode,username from lzcertifyuser "
                        + " where 'B'||usercode in (select receivecom from lzaccess where sendoutcom = 'A'||"
                        + mCodeCondition + ") order by usercode) ";
            }
        }

        //单证查询发放者
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "sendoutcodesearch") == 0)
        {
            if (mGlobalInput.ComCode.length() == 2)
            {
                mSQL = "select 'A'||Comcode, Name from ldcom where comcode='86'";
            }
            else if (mGlobalInput.ComCode.length() == 4)
            {
                mSQL = "select 'A'||Comcode, Name from ldcom where 1=1 and Sign='1' and "
                        + " comcode='" + mGlobalInput.ComCode + "' ";
            }
            else if (mGlobalInput.ComCode.length() == 8)
            {
                mSQL = "select 'B'||usercode,username from LDUser where usercode='"
                        + mGlobalInput.Operator + "'";
            }
        }

        // 单证发放机构代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "comcodeso") == 0)
        {
            mSQL = "select ComCode, Name from ldcom where " + mConditionField
                    + " = " + mCodeCondition
                    + " and Sign='1' and comcode like '"
                    + mGlobalInput.ManageCom + "%'"
                    + " and length(trim(comcode)) <= 4 order by comcode";

        }

        // 银行险编码引用Riskbank
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskbank") == 0)
        {
            if (mCodeCondition.trim().equals("'1' and branchtype=2"))
            {
                mCodeCondition = "1";
                mSQL = "select RiskCode, RiskName from LMRiskApp where "
                        + mConditionField
                        + " = "
                        + mCodeCondition
                        + " and RiskProp in ('A','B','G','D') order by RiskCode";
            }
            else
            {
                mCodeCondition = "1";
                mSQL = "select RiskCode, RiskName from LMRiskApp where "
                        + mConditionField
                        + " = "
                        + mCodeCondition
                        + " and RiskProp in ('Y','B','C','D') order by RiskCode";
            }
        }

        // 团险编码引用RiskGrp
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskgrp") == 0)
        {
            String mcurdate = PubFun.getCurrentDate();
            StringBuffer sql = new StringBuffer(255);
            if("'dd'".equals(mCodeCondition)){
            	sql.append("select code,codename,codealias from ldcode where codetype = 'bigillrisk' order by code ");
            }else{
            	sql.append("select * from (");
                sql
                        .append("select RiskCode a, RiskName,'Risk' from LMRiskApp where 1=1 ");
//                sql.append(mConditionField);
//                sql.append(" = ");
//                sql.append(mCodeCondition);
                sql.append(" and RiskProp in ('G','A','B','D') ");
                sql.append("and (EndDate is null or EndDate>'");
                sql.append(mcurdate);
                sql.append("')");
                sql.append(" union ");
                sql
                        .append("select distinct RiskWrapCode a,RiskWrapName,'Warp' From LDRiskWrap where ");
                sql.append("RiskWrapType = 'G' and");
                sql.append(" (EndDate is null or EndDate>'");
                sql.append(mcurdate);
                sql.append("')");
                sql.append(" union ");
                sql.append("select code,codename,'Risk' from ldcode where codetype = 'grphjrisk' and  ");
                sql.append("othersign = '1' ");
                sql.append(") as x order by a");
            }
            
            mSQL = sql.toString();
        }

        // 汇缴件险种编码引用。RiskColl
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskcoll") == 0)
        {
            String mcurdate = PubFun.getCurrentDate();

            StringBuffer sql = new StringBuffer(255);
            sql.append(" select RiskCode, RiskName, 'Risk' from LMRiskApp ");
            sql.append(" where ");
            sql.append(mConditionField);
            sql.append(" = ");
            sql.append(mCodeCondition);
            sql.append(" and RiskProp in ('I','A','C','D') ");
            sql.append(" and RiskTypeDetail = 'H' ");
            sql.append(" and (EndDate is null or EndDate > '");
            sql.append(mcurdate);
            sql.append("') ");
            mSQL = sql.toString();
        }

        // 个险编码引用RiskInd
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskind") == 0)
        {
            String mcurdate = PubFun.getCurrentDate();
            mSQL = "select RiskCode, RiskName from LMRiskApp where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and RiskProp in ('I','A','C','D') and (EndDate is null or EndDate>'"
                    + mcurdate + "') order by RiskCode";
        }
  //个险万能险编码引用RiskName
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "riskname") == 0)
{
    String mcurdate = PubFun.getCurrentDate();
    mSQL = "select RiskCode, RiskName from LMRiskApp where "
    	+ mConditionField
        + " = "
        + mCodeCondition
            + " and risktype4='4'";
}
      //互动提取普通险种
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskacname") == 0)
        {
            String mcurdate = PubFun.getCurrentDate();
            mSQL = "select RiskCode, RiskName from LMRiskApp where "
            	+ mConditionField
                + " = "
                + mCodeCondition
                    + " and risktype4 <>'4'";
        } 
        
        // 普通单证编码引用CertifyCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "certifycodedes") == 0)
        {
        	if (mGlobalInput.ManageCom != null && mGlobalInput.ManageCom.length() > 3){
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    // + mConditionField + " = " + mCodeCondition
                    + " State = '0' and CertifyClass  in  ('P','D') and " 
                    + " managecom in ('86','A','" + mGlobalInput.ManageCom.substring(0,4) + "') " 
                    + "order by CertifyCode";
        	} else {
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    // + mConditionField + " = " + mCodeCondition
                    + " State = '0' and CertifyClass  in  ('P','D') order by CertifyCode";
        	}
            
        }

        // 普通单证编码引用CertifyCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "certifycode") == 0)
        {
        	if (mGlobalInput.ManageCom != null && mGlobalInput.ManageCom.length() > 3){
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    // + mConditionField + " = " + mCodeCondition
        			+ " managecom in ('86','A','" + mGlobalInput.ManageCom.substring(0,4) + "') and " 
        			+ " 1=1 and CertifyClass = 'P'  AND State = '0' order by CertifyCode";
        	} else {
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    // + mConditionField + " = " + mCodeCondition
        			+ " 1=1 and CertifyClass = 'P'  AND State = '0' order by CertifyCode";
        	}
        }

        // 普通单证编码引用CertifyCode1
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "certifycode1") == 0)
        {
        	if (mGlobalInput.ManageCom != null && mGlobalInput.ManageCom.length() > 3){
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    // + mConditionField + " = " + mCodeCondition
        			+ " managecom in ('86','A','" + mGlobalInput.ManageCom.substring(0,4) + "') and " 
                    + " 1=1 and CertifyClass = 'P'  order by CertifyCode";
        	} else {
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    // + mConditionField + " = " + mCodeCondition
                    + " 1=1 and CertifyClass = 'P'  order by CertifyCode";
        	}
        }
        // 定额单证编码引用CertifyCoded
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "certifycoded") == 0)
        {
        	if (mGlobalInput.ManageCom != null && mGlobalInput.ManageCom.length() > 3){
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and managecom in ('86','A','" + mGlobalInput.ManageCom.substring(0,4) + "') "
                    + "  and  CertifyClass = 'D' order by CertifyCode";
        	} else {
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + "  and  CertifyClass = 'D' order by CertifyCode";
        	}
        }

        // 定额单证编码引用CardCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "cardcode") == 0)
        {
        	if (mGlobalInput.ManageCom != null && mGlobalInput.ManageCom.length() > 3){
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and managecom in ('86','A','" + mGlobalInput.ManageCom.substring(0,4) + "') "
                    + " and CertifyClass = 'D' AND State = '0' order by CertifyCode";
        	} else {
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and CertifyClass = 'D' AND State = '0' order by CertifyCode";
        	}
        }
        
        //----------------------------------------------
        // 单证新增 普通单证 发放管理、核销管理、增领单证
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "certifycodesend") == 0)
        {
        	if (mGlobalInput.ManageCom != null && mGlobalInput.ManageCom.length() < 5){
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    // + mConditionField + " = " + mCodeCondition
        			+ " 1=1 and CertifyClass = 'P' AND State = '0' " 
        			+ " and exists (select 1 from lzcard where " 
        			+ "    certifycode=lmcertifydes.certifycode " 
        			+ "    And Receivecom = 'A" + mGlobalInput.ManageCom + "' and StateFlag in ('0','7','8','9')) " 
        			+ "order by CertifyCode";
        	} else {
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    // + mConditionField + " = " + mCodeCondition
        			+ " 1=1 and CertifyClass = 'P' AND State = '0' " 
        			+ " and exists (select 1 from lzcard where " 
        			+ "    certifycode=lmcertifydes.certifycode " 
        			+ "    And Receivecom = 'B" + mGlobalInput.Operator + "' and StateFlag in ('0','7','8','9')) " 
        			+ " order by CertifyCode";
        	}
        }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "certifycodesend1") == 0)
        {
        	if (mGlobalInput.ManageCom != null && mGlobalInput.ManageCom.length() < 5){
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
        			// + mConditionField + " = " + mCodeCondition
        			+ " 1=1 and CertifyClass = 'P' " 
        			+ " and exists (select 1 from lzcard where " 
        			+ "    certifycode=lmcertifydes.certifycode " 
				+ "    And Receivecom = 'A" + mGlobalInput.ManageCom + "' and StateFlag in ('0','7','8','9')) " 
				+ "order by CertifyCode";
        	} else {
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
        			// + mConditionField + " = " + mCodeCondition
        			+ " 1=1 and CertifyClass = 'P' " 
        			+ " and exists (select 1 from lzcard where " 
        			+ "    certifycode=lmcertifydes.certifycode " 
        			+ "    And Receivecom = 'B" + mGlobalInput.Operator + "' and StateFlag in ('0','7','8','9')) " 
        			+ " order by CertifyCode";
        	}
        }
        
        // 单证新增 定额单证 发放管理、增领单证
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "cardcodesend") == 0)
        {
        	if (mGlobalInput.ManageCom != null && mGlobalInput.ManageCom.length() < 5){
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and CertifyClass = 'D' AND State = '0' " 
                    + " and exists (select 1 from lzcard where " 
        			+ "    certifycode=lmcertifydes.certifycode " 
        			+ "    And Receivecom = 'A" + mGlobalInput.ManageCom + "' and State in ('0','3','8','9')) " 
        			+ "order by CertifyCode";
        	} else {
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and CertifyClass = 'D' AND State = '0' " 
                    + " and exists (select 1 from lzcard where " 
        			+ "    certifycode=lmcertifydes.certifycode " 
        			+ "    And Receivecom = 'B" + mGlobalInput.Operator + "' and State in ('0','3','8','9')) " 
        			+ " order by CertifyCode";
        	}
        }
        
        //普通单证 单证发放回退、空白回销
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        		"cardcodereceive") == 0)
        {
        	if (mGlobalInput.ManageCom != null && mGlobalInput.ManageCom.length() < 5){
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
        			+ mConditionField
        			+ " = "
        			+ mCodeCondition
        			+ " and CertifyClass = 'P' AND State = '0' " 
        			+ " and exists (select 1 from lzcard where " 
        			+ "    certifycode=lmcertifydes.certifycode " 
        			+ "    And Receivecom in (select ReceiveCom from LZAccess where SendOutCom = 'A" 
                    + mGlobalInput.ManageCom  + "') and StateFlag in ('0','7','8','9')) " 
        			+ "order by CertifyCode";
        	} else {
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
        			+ mConditionField
        			+ " = "
        			+ mCodeCondition
					+ " and CertifyClass = 'P' AND State = '0' " 
					+ " and exists (select 1 from lzcard where " 
					+ "    certifycode=lmcertifydes.certifycode " 
					+ "    And SendOutCom = 'B" + mGlobalInput.Operator + "' and StateFlag in ('0','7','8','9')" 
					+ "and (ReceiveCom like 'D%' or ReceiveCom like 'E%')) " 
					+ " order by CertifyCode";
        		}
        }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
				"certifycodereceive1") == 0)
        {
        	if (mGlobalInput.ManageCom != null && mGlobalInput.ManageCom.length() < 5){
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
        			+ mConditionField
        			+ " = "
        			+ mCodeCondition
        			+ " and CertifyClass = 'P' " 
        			+ " and exists (select 1 from lzcard where " 
        			+ "    certifycode=lmcertifydes.certifycode " 
        			+ "    And Receivecom in (select ReceiveCom from LZAccess where SendOutCom = 'A" 
        			+ mGlobalInput.ManageCom  + "') and StateFlag in ('0','7','8','9')) " 
        			+ "order by CertifyCode";
        	} else {
        		mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
        			+ mConditionField
        			+ " = "
        			+ mCodeCondition
        			+ " and CertifyClass = 'P' " 
        			+ " and exists (select 1 from lzcard where " 
        			+ "    certifycode=lmcertifydes.certifycode " 
        			+ "    And SendOutCom = 'B" + mGlobalInput.Operator + "' and StateFlag in ('0','7','8','9')" 
        			+ "and (ReceiveCom like 'D%' or ReceiveCom like 'E%')) " 
        			+ " order by CertifyCode";
        	}
        }
        //----------------------------------------------
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
			"certifymanagesend") == 0) {
    	
        	String com = mGlobalInput.ManageCom;
    	
    			mSQL = "select comcode,name from ldcom where comcode like '" + com + "%' "
    				+ "and length(trim(comcode))=(select int(codealias) from ldcode where codetype='certifycom' and code='" + com.length() + "')  and sign='1' "
    				+ "order by comcode";
    	
        }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
		"managestf") == 0) {
	
    	String com = mGlobalInput.ManageCom;
	
			mSQL = "select comcode,name from ldcom where comcode like '" + com + "%' "
				+ "and length(trim(comcode))<=4 "
				+ "order by comcode";
	
    }
    
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
			"certifymanageall") == 0) {
	
        	String com = mGlobalInput.ManageCom;
	
        	mSQL = "select comcode,name from ldcom where comcode like '" + com + "%' and sign='1' "
					+ "order by comcode";
	
    	}
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
			"certifycomname") == 0) {
        	
        	mSQL = "select distinct comcode,(select name from ldcom where comcode=LZSendOutList.comcode) from LZSendOutList where batchno='" + mConditionField + "' order by comcode";
        }
        
        //保单投保项目编码和项目名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        		"projectaskmode") == 0) {
        	String com = mGlobalInput.ManageCom;
                	mSQL = "select projectno,projectname from LCProjectInfo where 1=1 and "
                			+ mConditionField
                            + " like '%"
                            + mCodeCondition.substring(1, (mCodeCondition.length() - 1)).trim()
                            + "%' with ur"
                            ;
                	
                }
        
      //下发问题件
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
		"questionlower") == 0) {

        	String com = mGlobalInput.ManageCom;
        	
        	mSQL = "select  code,Codename from ldcode where 1=1 "
        			+ "and codetype='ImageDeal' "
        			+ "order by code with ur";
        }
        	
      //外包第三方
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
		"thridcompany2") == 0) {

        	String com = mGlobalInput.ManageCom;
        	
        	mSQL = "select  Othersign,Codename from ldcode where 1=1 "
        			+ "and codetype='thridcompany' "
        			+ "order by Othersign with ur";
        }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
		"thridcompany1") == 0) {

        	String com = mGlobalInput.ManageCom;
//        	 
//        	mSQL =" select   a.comcode , (select  b.name from ldcom b where 1=1 and a.comcode=b.comcode) comname   from ldcode a    where 1=1   and a.codetype like 'LP%'  and a.comcode like  '86%' " +
//        	" and a.comcode like '"+com+"%' " +
//        			" group by a.comcode order by a.comcode ";
        	mSQL =" select  "
            	+"	b.managecom  ,"
            	+"	( select name from ldcom where 1=1  and comcode=b.managecom ) name"
            	+"	from ldcode a ,lcgrpcont b   where 1=1 and a.code=b.grpcontno  "
            	+"	and a.codetype like 'LP%'  and a.comcode like  '86%'  "
            	+"	and b.managecom like '"+com+"%' " 
            	+"	group by b.managecom order by b.managecom " ;

        }
        
    	//----------------------------------------------
        
        // 定额单证编码引用,网销ECCardCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "eccardcode") == 0)
        {
            mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and CertifyClass = 'D' AND State = '0' AND operatetype='4' order by CertifyCode";
        }
        
     // 网销下发人员
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "ecagentcode") == 0)
        {
        	mSQL = "select agentcode,name from laagent where agentcode in (select agentcode from laagent where branchtype = '2' and branchtype2 = '01' and insideflag='0') and " +
    		"managecom='"+this.mGlobalInput.ComCode+"' order by agentcode";
        }

        // 系统单证编码引用SysCertCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "syscertcode") == 0)
        {
            mSQL = "SELECT CertifyCode, CertifyName, SubCode FROM LMCertifyDes WHERE "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and CertifyClass = 'S' AND State = '0' order by CertifyCode";
        }

        // 告知编码引用ImpartCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "impartcode") == 0)
        {
            mSQL = "SELECT ImpartCode, ImpartContent FROM LDImpart WHERE "
                    + mConditionField + " = " + mCodeCondition
                    + " order by ImpartCode";
        }

        // 管理机构编码引用Station，已不再使用
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "station2") == 0)
        {
            mSQL = "select Code, CodeName, CodeAlias, ComCode, OtherSign from ldcode where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and codetype = 'station' and code like '"
                    + mGlobalInput.ManageCom + "%' order by code";
        }

        // 工种代码引用OccupationCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "occupationcode") == 0)
        {
            mSQL = "select OccupationCode, trim(OccupationName)||'-'||workname, OccupationType from LDOccupation where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by OccupationCode";

            executeType = 1;
        }

        // 交费方式代码引用PayYears
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "payyears") == 0)
        {
            mSQL = "select trim(PayEndYearFlag)||PayEndYear||'*'||PayIntv,ShowInfo from LMPayMode where "
                    + mConditionField + " = " + mCodeCondition;

            executeType = 1;
        }

        // 团单险种查询GrpRisk
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "grprisk") == 0)
        {
            mSQL = "select a.RiskCode, a.RiskName,b.GrpPolNo,a.SubRiskFlag from LMRiskApp a,LCGrpPol b where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and a.RiskCode = b.RiskCode and b.riskwrapflag='N' "
                    + " union "
                    +"select a.RiskCode, a.RiskName,b.GrpPolNo,a.SubRiskFlag from LMRiskApp a,LBGrpPol b where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and a.RiskCode = b.RiskCode and b.riskwrapflag='N' "
                    + " union "
                    + " select distinct b.RiskWrapCode,a.RiskWrapName,'','Wrap' from  "
                    + " lcriskwrap b,ldriskwrap a where a.riskwrapcode=b.riskwrapCode and "
                    + mConditionField + " = " + mCodeCondition;
        }

        // 团单险种查询GrpRiskB(LBGrpPol 表)
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "grpriskb") == 0)
        {
            mSQL = "select a.RiskCode, a.RiskName,b.GrpPolNo,a.SubRiskFlag from LMRiskApp a,LBGrpPol b where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and a.RiskCode = b.RiskCode order by a.RiskCode";
        }

        // 团单险种查询GrpMainRisk主险
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "grpmainrisk") == 0)
        {
            mSQL = "select a.RiskCode, a.RiskName,b.GrpPolNo from LMRiskApp a,LCGrpPol b where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and a.RiskCode = b.RiskCode and a.SubRiskFlag <> 'S' order by a.RiskCode";
        }

        // 团单险种查询GrpMainRisk主险
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "grpmainrisk1") == 0)
        {
            mSQL = "select a.RiskCode, a.RiskName from LMRiskApp a where a.SubRiskFlag <> 'S' order by a.RiskCode";
        }

        // 保险套餐RiskPlan
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskplan") == 0)
        {
            mSQL = "select ContPlanCode,ContPlanName from LDPlan order by ContPlanCode";
        }

        // 团单险种缴费规则查询RiskRuleFactoryType，Type编码默认为000005
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskrulefactorytype") == 0)
        {
            mSQL = "select distinct a.FactoryType,b.FactoryTypeName,trim(a.FactoryType)||trim(a.RiskCode) from LMFactoryMode a,LMFactoryType b where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and a.FactoryType = b.FactoryType and a.FactoryType = '000005'";
        }

        // 团单险种缴费规则查询RiskRuleFactoryNo
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskrulefactoryno") == 0)
        {
            if (mCodeCondition.substring(1, 7).equals("000005"))
            {
                // 没有加入任何限制条件，以后扩展
                mSQL = "select PayPlanCode,PayPlanName from LMDutyPay where payplancode in (select payplancode from lmdutypayrela where dutycode in (select dutycode from lmriskduty where riskcode = '"
                        + mCodeCondition.substring(7, 13) + "'))";
            }
        }

        // 团单险种缴费规则查询RiskRuleFactory
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskrulefactory") == 0)
        {
            mSQL = "select FactoryCode||to_char(FactorySubCode),CalRemark,Params,FactoryName from LMFactoryMode  "
                    + " where FactoryType = '"
                    + mCodeCondition.substring(1, 7)
                    + "' and RiskCode='"
                    + mCodeCondition.substring(7)
                    + " order by FactoryCode,FactorySubCode ";
        }

        // 团单保险计划下险种查询ImpRiskCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "impriskcode") == 0)
        {
            String a = mCodeCondition.substring(0, 2); // 保险计划编码
            String b = mCodeCondition.substring(2); // 合同号

            mSQL = "select a.RiskCode, a.RiskName, a.RiskVer,b.MainRiskCode,b.MainRiskVersion from LMRiskApp a,LCContPlanRisk b where "
                    + mConditionField
                    + " = "
                    + a
                    + "' and GrpContNo = '"
                    + b
                    + " and a.RiskCode = b.RiskCode order by a.RiskCode";
        }

        // 工单用户
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "memberingroup") == 0)
        {
            mSQL = "  select userCode, userName " + "from  LDUser "
                    + "where userCode in " + "   (select memberNo "
                    + "   from LGGroupMember)";
        }

        // 工单中保全用户
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "pamemberingroup") == 0)
        {
            mSQL = "  select userCode, userName " + "from  LDUser "
                    + "where userCode in " + "   (select memberNo "
                    + "   from LGGroupMember" + "   where postNo is not null)";
        }

        // 团单保险计划下险种对应要素类别ImpFactoryType
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "impfactorytype") == 0)
        {
            mSQL = "select distinct a.FactoryType,b.FactoryTypeName,trim(a.FactoryType)||trim(a.RiskCode) from LMFactoryMode a,LMFactoryType b where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and a.FactoryType = b.FactoryType and a.FactoryType < '000005'";
        }
        // 团单保险计划下险种对应要素目标编码ImHealthFactoryNo
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "imhealthfactoryno") == 0)
        {
            if (mCodeCondition.substring(1, 7).equals("000000"))
            { // 基于保单的计算
                mSQL = "select '__','请录入保单号' from dual";
            }
            else if (mCodeCondition.substring(1, 7).equals("000001"))
            { // 基于保单的计算
                mSQL = "select DutyCode,DutyName from LMDuty where DutyCode in(select DutyCode from LMRiskDuty where RiskCode='"
                        + mCodeCondition.substring(7) + ") order by DutyCode";
            }
            else if (mCodeCondition.substring(1, 7).equals("000002"))
            {
                // 基于给付的计算
                mSQL = "select getdutycode,getdutyname from lmdutygetrela where dutycode in (select dutycode from lmriskduty where riskcode ='"
                        + mCodeCondition.substring(7)
                        + ") order by getdutycode";
            }
            else if (mCodeCondition.substring(1, 7).equals("000003"))
            {
                // 基于账户的计算
                mSQL = "select insuaccno,insuaccname from LMRiskToAcc where RiskCode='"
                        + mCodeCondition.substring(7) + " order by insuaccno";
            }
            else if (mCodeCondition.substring(1, 7).equals("000004"))
            {
                // 基于理赔责任的计算
                mSQL = "select getdutycode,getdutyname from lmdutygetrela where dutycode in ( select dutycode from lmriskduty where riskcode ='"
                        + mCodeCondition.substring(7)
                        + ") order by getdutycode";
            }
        }

        // 团单保险计划下险种对应要素计算编码ImHealthFactory
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "imhealthfactory") == 0)
        {
            mSQL = "select FactoryCode||to_char(FactorySubCode),CalRemark,Params,FactoryName from LMFactoryMode where FactoryType = '"
                    + mCodeCondition.substring(1, 7)
                    + "' and RiskCode='"
                    + mCodeCondition.substring(7)
                    + ""
                    + " order by FactoryCode,FactorySubCode ";
        }

        // 代理人编码引用AgentCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcode") == 0)
        {
            ExeSQL tExeSQL = new ExeSQL();
            String agent = tExeSQL
                    .getEncodedResult(
                            "select Sysvarvalue from ldsysvar where Sysvar = 'LAAgent'",
                            1);
            agent = agent.substring(agent.indexOf("^") + 1);

            // mSQL = "select AgentCode, Name, BranchCode from LAAgent where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom +
            // "%' and " +
            // agent + " order by AgentCode";
            mSBql
                    .append("select getUniteCode(AgentCode), Name, BranchCode from LAAgent where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' and ");
            mSBql.append(agent);
            mSBql.append(" and groupagentcode is not null ");
            mSBql.append("  order by AgentCode");

            mSQL = mSBql.toString();

            executeType = 1;
            // mResultStr = mExeSQL.getEncodedResult(mSQL);
        }
        // 银行代理人编码引用AgentCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "bankagentcode") == 0)
        {
            ExeSQL tExeSQL = new ExeSQL();
            String agent = tExeSQL
                    .getEncodedResult(
                            "select Sysvarvalue from ldsysvar where Sysvar = 'LAAgent'",
                            1);
            agent = agent.substring(agent.indexOf("^") + 1);

            // mSQL = "select AgentCode, Name, BranchCode from LAAgent where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom +
            // "%' and " +
            // agent + " order by AgentCode";
            mSBql
                    .append("select getUniteCode(AgentCode), Name, ManageCom from LAAgent where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%'  and branchtype='3' and branchtype2='01' and ");
            mSBql.append(agent);
            mSBql.append(" and groupagentcode is not null ");
            mSBql.append("  order by AgentCode");

            mSQL = mSBql.toString();

            executeType = 1;
            // mResultStr = mExeSQL.getEncodedResult(mSQL);
        }
        // 团险直销代理人编码引用AgentCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "grpdiragentcode") == 0)
        {
            ExeSQL tExeSQL = new ExeSQL();
            String agent = tExeSQL
                    .getEncodedResult(
                            "select Sysvarvalue from ldsysvar where Sysvar = 'LAAgent'",
                            1);
            agent = agent.substring(agent.indexOf("^") + 1);

            // mSQL = "select AgentCode, Name, BranchCode from LAAgent where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom +
            // "%' and " +
            // agent + " order by AgentCode";
            mSBql
                    .append("select getUniteCode(AgentCode), Name, ManageCom from LAAgent where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%'  and branchtype='2' and branchtype2='01' and ");
            mSBql.append(agent);
            mSBql.append(" and groupagentcode is not null ");
            mSBql.append("  order by AgentCode");

            mSQL = mSBql.toString();

            executeType = 1;
            // mResultStr = mExeSQL.getEncodedResult(mSQL);
        }
     // 团险中介代理人编码引用AgentCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "grpinsagentcode") == 0)
        {
            ExeSQL tExeSQL = new ExeSQL();
            String agent = tExeSQL
                    .getEncodedResult(
                            "select Sysvarvalue from ldsysvar where Sysvar = 'LAAgent'",
                            1);
            agent = agent.substring(agent.indexOf("^") + 1);

            // mSQL = "select AgentCode, Name, BranchCode from LAAgent where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom +
            // "%' and " +
            // agent + " order by AgentCode";
            mSBql
                    .append("select getUniteCode(AgentCode), Name, ManageCom from LAAgent where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%'  and branchtype='2' and branchtype2='02' and ");
            mSBql.append(agent);
            mSBql.append(" and groupagentcode is not null ");
            mSBql.append("  order by AgentCode");

            mSQL = mSBql.toString();

            executeType = 1;
            // mResultStr = mExeSQL.getEncodedResult(mSQL);
        }
        // 代理人编码引用AgentName
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentname") == 0)
        {
            ExeSQL tExeSQL = new ExeSQL();
            String agent = tExeSQL
                    .getEncodedResult(
                            "select Sysvarvalue from ldsysvar where Sysvar = 'LAAgent'",
                            1);
            agent = agent.substring(agent.indexOf("^") + 1);

            // mSQL = "select AgentCode, Name, BranchCode from LAAgent where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom +
            // "%' and " +
            // agent + " order by AgentCode";
            mSBql
                    .append("select  Name,groupagentcode,BranchCode,agentcode from LAAgent where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' and ");
            mSBql.append(agent);
            mSBql.append("  order by AgentCode");

            mSQL = mSBql.toString();

            executeType = 1;
            // mResultStr = mExeSQL.getEncodedResult(mSQL);
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "laagentname") == 0)
        {
            ExeSQL tExeSQL = new ExeSQL();
            String agent = tExeSQL
                    .getEncodedResult(
                            "select Sysvarvalue from ldsysvar where Sysvar = 'LAAgent'",
                            1);
            agent = agent.substring(agent.indexOf("^") + 1);
            //张飞飞 2014-11-14 集团统一工号
            mSBql.append("select  groupAgentCode,name,BranchCode from LAAgent where ");
            mSBql.append(mConditionField);
            mSBql.append(" like '%");
            mSBql.append(mCodeCondition.substring(1,
                    (mCodeCondition.length() - 1)).trim()
                    + "%'");
            mSBql.append(" and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' and ");
            mSBql.append(agent);
            mSBql.append("  order by AgentCode");
            mSQL = mSBql.toString();
            executeType = 1;
        }
        // 代理人编码引用AgentCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcodet") == 0)
        {
            ExeSQL tExeSQL = new ExeSQL();
            String agent = tExeSQL
                    .getEncodedResult(
                            "select Sysvarvalue from ldsysvar where Sysvar = 'LAAgent'",
                            1);
            agent = agent.substring(agent.indexOf("^") + 1);

            // mSQL = "select AgentCode, Name, BranchCode from LAAgent where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom +
            // "%' and " +
            // agent + " order by AgentCode";
            mSBql
                    .append("select getUniteCode(a.AgentCode), a.Name, a.BranchCode,a.agentgroup,b.Name from LAAgent a,LABranchGroup b where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and a.agentgroup=b.agentgroup ");
            mSBql.append(" and a.ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' and ");
            mSBql.append(agent);
            mSBql.append(" and a.branchtype='2' order by a.AgentCode");

            mSQL = mSBql.toString();

            executeType = 1;
            // mResultStr = mExeSQL.getEncodedResult(mSQL);
        }
        // 按照页面上的管理机构（like）选择个险的代理人编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcodet1") == 0)
        {
            ExeSQL tExeSQL = new ExeSQL();
            String agent = tExeSQL
                    .getEncodedResult(
                            "select Sysvarvalue from ldsysvar where Sysvar = 'LAAgent'",
                            1);
            agent = agent.substring(agent.indexOf("^") + 1);

            // mSQL = "select AgentCode, Name, BranchCode from LAAgent where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom +
            // "%' and " +
            // agent + " order by AgentCode";
            mSBql
                    .append("select a.GroupAgentCode, a.Name, a.BranchCode,a.agentgroup,b.Name from LAAgent a,LABranchGroup b where ");
            mSBql.append("a." + mConditionField);
            mSBql.append(" like '");
            mSBql.append(mCodeCondition.substring(1,
                    (mCodeCondition.length() - 1)).trim());
            mSBql.append("%'  and a.agentgroup=b.agentgroup and ");
            mSBql.append(agent);
            mSBql.append(" and groupagentcode is not null ");
            mSBql.append(" and a.branchtype='1' order by a.AgentCode");

            mSQL = mSBql.toString();

            executeType = 1;
            // mResultStr = mExeSQL.getEncodedResult(mSQL);
        }
        // 按照页面上的管理机构（like）选择团险的代理人编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcodet2") == 0)
        {
            ExeSQL tExeSQL = new ExeSQL();
            String agent = tExeSQL
                    .getEncodedResult(
                            "select Sysvarvalue from ldsysvar where Sysvar = 'LAAgent'",
                            1);
            agent = agent.substring(agent.indexOf("^") + 1);

            // mSQL = "select AgentCode, Name, BranchCode from LAAgent where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom +
            // "%' and " +
            // agent + " order by AgentCode";
            mSBql
                    .append("select a.groupAgentCode, a.Name, a.BranchCode,a.agentgroup,b.Name from LAAgent a,LABranchGroup b where ");
            mSBql.append("a." + mConditionField);
            mSBql.append(" like '");
            mSBql.append(mCodeCondition.substring(1,
                    (mCodeCondition.length() - 1)).trim());
            mSBql.append("%'  and a.agentgroup=b.agentgroup and ");
            mSBql.append(agent);
            mSBql.append(" and a.branchtype='2' order by a.AgentCode");

            mSQL = mSBql.toString();

            executeType = 1;
            // mResultStr = mExeSQL.getEncodedResult(mSQL);
        }
//      按照页面上的管理机构（like）选择银代的代理人编码
        // 代理人编码引用AgentCode
         if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                 "agentcodet3") == 0)
         {
             ExeSQL tExeSQL = new ExeSQL();
             String agent = tExeSQL
                     .getEncodedResult(
                             "select Sysvarvalue from ldsysvar where Sysvar = 'LAAgent'",
                             1);
             agent = agent.substring(agent.indexOf("^") + 1);

             // mSQL = "select AgentCode, Name, BranchCode from LAAgent where "
             // + mConditionField + " = " + mCodeCondition
             // + " and ManageCom like '" + mGlobalInput.ManageCom +
             // "%' and " +
             // agent + " order by AgentCode";
             mSBql.append("select groupAgentCode, Name, BranchCode from LAAgent where ");
             mSBql.append(mConditionField);
             mSBql.append(" = ");
             mSBql.append(mCodeCondition);
             mSBql.append(" and  branchtype='3' and agentstate<='02'  and  ManageCom like '");
             mSBql.append(mGlobalInput.ManageCom);
             mSBql.append("%' and ");
             mSBql.append(agent);
             mSBql.append("  order by AgentCode");

             mSQL = mSBql.toString();

             executeType = 1;
             // mResultStr = mExeSQL.getEncodedResult(mSQL);
         }
        // 代理人编码引用AgentCode2 --liujw
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcode2") == 0)
        {
            mSQL = "select getUniteCode(b.AgentCode),(select Name from laagent where agentcode = b.agentcode) name,b.AgentGroup,(select trim(BranchAttr) from laBranchGroup where agentgroup = (select branchcode from laagent where agentcode = b.agentcode)) branchattr,b.AgentSeries,b.AgentGrade from latree b where 1=1 and b.AgentCode in (select AgentCode from LAAgent Where AgentState in ('01','02') "
                    // +"And Branchtype = '1') And "
                    + mCodeCondition
                    // +mConditionField + " = " + mCodeCondition
                    + " ORDER BY b.AgentCode";
            executeType = 1;
        }

        //按照页面上的管理机构（like）选择个险的代理人编码
        //保全查询个单代理人:2007-8-28 by fuxin
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcodetbq") == 0)
        {
            ExeSQL tExeSQL = new ExeSQL();
            String agent = tExeSQL
                    .getEncodedResult(
                            "select Sysvarvalue from ldsysvar where Sysvar = 'LAAgent'",
                            1);
            agent = agent.substring(agent.indexOf("^") + 1);
            String st = null;

            if (mCodeCondition.substring(1, (mCodeCondition.length() - 1))
                    .equals("")
                    || mCodeCondition.substring(1,
                            (mCodeCondition.length() - 1)) == null)
            {
                st = " in (select agentgroup from LAAgent  where managecom LIKE '"
                        + mGlobalInput.ManageCom + "%')";
            }
            else
            {
                st = " in (select  agentgroup from LABranchGroup where branchseries like'"
                        + mCodeCondition.substring(1,
                                (mCodeCondition.length() - 1)) + "%')".trim();
            }
            //            mSQL = "select AgentCode, Name, BranchCode from LAAgent where "
            //                   + mConditionField + " = " + mCodeCondition
            //                   + " and ManageCom like '" + mGlobalInput.ManageCom +
            //                   "%' and " +
            //                   agent + " order by AgentCode";
            mSBql
                    .append("select a.GroupAgentCode, a.Name, a.BranchCode,a.agentgroup,b.Name from LAAgent a,LABranchGroup b where ");
            mSBql.append("a." + mConditionField);
            //                   mSBql.append(" like '");
            //                   mSBql.append(mCodeCondition.substring(1,
            //                                                          (mCodeCondition.length() - 1)).
            //                                 trim());
            mSBql.append(st);
            mSBql.append("  and a.agentgroup=b.agentgroup and ");
            mSBql.append(agent);
            mSBql.append(" and a.branchtype='1' order by a.AgentCode");

            mSQL = mSBql.toString();

            executeType = 1;
            //      mResultStr = mExeSQL.getEncodedResult(mSQL);
        }

        // 员工制待遇级别查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "employeegrade") == 0)
        {
            mSQL = "select gradecode,gradename from laagentgrade where GradeProperty6 = '1' order by gradecode";
        }

        // 员工制待遇级别查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "employeegrade2") == 0)
        {
            mSQL = "select lawelfareradix.agentgrade,ldcode.codename,100 from lawelfareradix ,ldcode,laagentgrade where  ldcode.codetype = 'employeeaclass' and ldcode.code = lawelfareradix.aclass and lawelfareradix.branchtype = '1' and laagentgrade.gradecode = lawelfareradix.agentgrade and laagentgrade.gradeproperty6 = '1' and lawelfareradix.aclass = "
                    + mCodeCondition + " order by lawelfareradix.agentgrade";
        }
        // 个单合同无扫描件录入账号查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "accnum") == 0)
        {
            mSQL = "select BankAccNo,AccName from LCAccount where "
                    + mConditionField + " = " + mCodeCondition;
        }
        // 用户地址代码条件查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "getaddressno") == 0)
        {
            mSQL = "select AddressNo,PostalAddress from LCAddress where "
                    + mConditionField + " = " + mCodeCondition;
        }
        // 团体用户地址代码条件查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "getgrpaddressno") == 0)
        {
            mSQL = "select AddressNo,GrpAddress from LCGrpAddress where "
                    + mConditionField + " = " + mCodeCondition;
        }
        // 团单险种查询交费间隔payintv
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskpayintv") == 0)
        {
            mSQL = "select a.PayIntv, b.CodeName from LMRiskPayIntv a,LDCode b where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and a.ChooseFlag = '1'  and b.CodeType = 'payintv'  and a.PayIntv = b.Code order by a.PayIntv";
        }
        // 查询险种代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "dutycode") == 0)
        {
            mSQL = "select DutyCode,DutyName from LMDuty where  dutycode in (select dutycode from lmriskduty where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + ") order by DutyCode";
        }

        // 查询给付类型
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "getdutykind") == 0)
        {
            mSQL = "select GetDutyKind,GetDutyName from LMDutyGetAlive where getdutycode in (select getdutycode from lmdutygetrela where dutycode in (select dutycode from lmriskduty where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " )) order by getdutykind";
        }

        // xjh Add,2005/02/18
        // 机构级别 branchlevel
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "branchlevel") == 0)
        {
            mSQL = "select BranchLevelCode,BranchLevelName from LABranchLevel where "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " order by BranchLevelID desc";
        }
				//miaoxiangzheng add below 2008/06/12
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "oldagentgrade") == 0)
        {
        	mSQL = "select Code,codeName from ldcode where "
            + mConditionField + " = "
            + mCodeCondition
            // + " and trim(gradecode) > '00'"
            + "and codetype='oldagentgrade'  order by code";
        }
        // xjh Modify 2005/3/22
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentgrade") == 0)
        {
            mSQL = "select GradeCode,GradeName from LAAgentGrade where "
                    + mConditionField + " = "
                    + mCodeCondition
                    // + " and trim(gradecode) > '00'"
                    + " and substr(rtrim(gradecode),length(rtrim(gradecode))-1) >'00' order by GradeID";
        }

        // 工单管理
        // 小组机构信息编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "acceptcom") == 0)
        {
            mSQL = "select GroupNo, GroupName from LGGroup order by GroupNo";
        }
				
//      财务收付费-->财务付费-->手续费付费-->手续费批量给付-->支付对象
        if(StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("shpmanagecom") == 0){
            mSQL = "select agentcom,name " 
                    + " from lacom "
                    + " where managecom like '"+mGlobalInput.ManageCom+"%'" 
                    + " and (endflag ='N' or endflag is null) " 
                    + " order by bankcode with ur " ;
        }
        
        // 业务分类编号
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "tasktoptypeno") == 0)
        { // 顶级分类
            mSQL = "select WorkTypeNo, WorkTypeName from LGWorkType where SuperTypeNo = '00' order by WorkTypeNo ";
        }

        // 业务分类编号
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "tasktypeno") == 0)
        { // 子分类
            String[] conditions = null;
            try
            {
                // 若是无条件查询，则mCodeCondition = "1"
                mCodeCondition = mCodeCondition.substring(1, mCodeCondition
                        .length() - 1);
                conditions = mCodeCondition.split(",");
            }
            catch (Exception e)
            {
                conditions = new String[1];
                mConditionField = "'" + mConditionField + "'";
                conditions[0] = mCodeCondition;
            }
            mSQL = "select WorkTypeNo, WorkTypeName from LGWorkType "
                    + "where " + mConditionField + " = '" + conditions[0]
                    + "' ";
            if (conditions.length == 2 && conditions[0].equals("03"))
            {
                if (conditions[1].length() == 9)
                {
                    mSQL += "  and workTypeNo like '030%' ";
                }
                else if (conditions[1].length() == 8)
                {
                    mSQL += "  and workTypeNo like '031%' ";
                }
            }
            mSQL += " order by WorkTypeNo ";
        }

        // 人员编码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "taskmemberno") == 0)
        {
            mSQL = "select UserCode, UserName from LDUser "
                    + " order by UserCode ";
        }

        // 受益人类型
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "BnfType") == 0)
        {
            mSQL = "select code,codename from ldcode where codetype='bnftype' and code='"
                    + mLDCodeSchema.getCode() + "'";
        }

        // 受益人顺序
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "bnfgrade") == 0)
        {
            mSQL = "select code,codename from ldcode where codetype='bnfgrade' and code='"
                    + mLDCodeSchema.getCode() + "'";
        }

        // 受益人顺序
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "bnfgrade2") == 0)
        {
            mSQL = "select code,codename from ldcode where codetype='bnfgrade'";
        }

        // 受理人
        // if
        // (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        // "usercode") == 0)
        // {
        // mSQL = "select usercode,username from lduser order by usercode";
        // }

        // 机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "organcode") == 0)
        {
            mSQL = "select comcode,name from ldcom order by comcode";
        }
        // 查小组成员
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "groupmember") == 0)
        {
            mSQL = "select usercode,username from lduser where usercode in (select memberno from lggroupmember where "
                    + mConditionField + "=" + mCodeCondition + ")";
        }
        // xjh Add,2005/02/24
        // 特殊险种 SpecRisk
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "specrisk") == 0)
        {
            mSQL = "select riskcode,riskname from lmriskapp ";
        }
        // 团单客户服务需求
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "serverinfotype") == 0)
        {
            mSQL = "select ServKind,ServKindRemark from LDServKindInfo order by servkind";
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "serverinfocode") == 0)
        {
            mSQL = "select ServDetail,ServDetailRemark,trim(servkind)||'-'||trim(servdetail) from LDServDetailInfo where "
                    + mConditionField
                    + "="
                    + mCodeCondition
                    + "order by ServDetail";
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "serverinfochoosecode") == 0)
        {
            int c = mCodeCondition.indexOf("-");
            String a = mCodeCondition.substring(0, c); // 保险计划编码
            String b = mCodeCondition.substring(c + 1); // 合同号
            mSQL = "select ServChoose,ServChooseRemark from LDServChooseInfo where "
                    + mConditionField
                    + "= "
                    + a
                    + "' and  servdetail= '"
                    + b
                    + " order by ServChoose";

        }
        // 查找契调专员
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "commissioner") == 0)
        {

            mSQL = "select UserCode,UserName from LDUser where " + " 1=1 "
                    + " and OtherPopedom='2' " + " order by UserCode";

        }

        // 查询团体险种号险种名zb
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "grpriskcode") == 0)
        {
            mSQL = "select Riskcode, Riskname from lmriskapp where riskprop='G' order by Riskcode asc";
        }

        // 查询用户下级
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "underlingname") == 0)
        {
            mSQL = "select usercode,username from lduser where "
                    + mConditionField + " =" + mCodeCondition;
        }
        // 配置单证发放机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "certifysendoutcode") == 0)
        {
            if (mGlobalInput.ComCode.length() == 2)
            {
                mSQL = "select ComCode, Name,'' from ldcom where comcode='"
                        + mGlobalInput.ComCode + "'";
            }
            else if (mGlobalInput.ComCode.length() == 4)
            {
                mSQL = "select ComCode, Name,'' from ldcom where 1=1 and Sign='1' and "
                        + " comcode='" + mGlobalInput.ComCode + "'";
            }
        }

        // 配置单证接受机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "certifyreceivecode") == 0)
        {
            if (mGlobalInput.ComCode.length() == 2)
            {
                mSQL = "select ComCode, Name,'' from ldcom where 1=1 and Sign='1' and "
                        + " length(trim(comcode))=4 ";
            }
            else if (mGlobalInput.ComCode.length() == 4)
            {
                mSQL = "select usercode,username from LZCertifyUser where ComCode like '"
                        + mGlobalInput.ComCode
                        + "%' "
                        + " and 'A'||usercode not in (select receivecom from lzaccess)";
            }
            else if (mGlobalInput.ComCode.length() == 8)
            {
                mSQL = "select usercode,username from LZCertifyUser where ComCode='"
                        + mGlobalInput.ComCode
                        + "' "
                        + " and 'A'||usercode not in (select receivecom from lzaccess)";
            }
        }

        // 单证发放机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "cardsendoutcode") == 0)
        {
            if (mGlobalInput.ComCode.length() == 2)
            {
                mSQL = "select a, b from (select ComCode a, Name b from ldcom where Sign='1' and "
                        + "(length(trim(comcode))=4 or length(trim(comcode))=2) union "
                        + "select usercode a,username b from LZCertifyUser where ComCode like '86%') as x order by a";
            }
            else if (mGlobalInput.ComCode.length() == 4)
            {
                mSQL = "select a, b from (select usercode a,username b from LZCertifyUser where ComCode like '"
                        + mGlobalInput.ComCode
                        + "%' and 'A'||usercode not in (select receivecom from lzaccess)"
                        + "union select comcode a, name b from ldcom where comcode = '8611') as x order by a";
            }
            else if (mGlobalInput.ComCode.length() == 8)
            {
                mSQL = "select usercode,username from LZCertifyUser where usercode = '"
                        + mGlobalInput.Operator + "' ";
            }
        }

        // 查询单证发放机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "searchsendoutcode") == 0)
        {
            if (mGlobalInput.ComCode.length() == 2)
            {
                mSQL = "select ComCode A, Name from ldcom where 1=1 and "
                        + "'A'||comcode in (select sendoutcom from Lzaccess where sendoutcom like 'A'||'86%')";
            }
            else if (mGlobalInput.ComCode.length() == 4)
            {
                mSQL = "select comcode A,name from ldcom "
                        + " where 'A'||comcode in (select sendoutcom from Lzaccess where sendoutcom like 'A'||"
                        + mCodeCondition + "||'%')";
            }
        }

        // 查询单证接受机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "searchreceivecode") == 0)
        {
            if (mGlobalInput.ComCode.length() == 2)
            {
                if (!mCodeCondition.equals("''"))
                {
                    mSQL = "(select comcode A,name from ldcom "
                            + " where 'A'||comcode in (select receivecom from Lzaccess where sendoutcom='A'||"
                            + mCodeCondition
                            + ")"
                            + " union "
                            + "select usercode A,username from lzcertifyuser where comcode like "
                            + mCodeCondition + "||'%') order by A";
                }
                else
                {
                    mSQL = "select comcode A,name from ldcom "
                            + " where 'A'||comcode in (select receivecom from Lzaccess )"
                            + " union "
                            + "select usercode A,username from lzcertifyuser where comcode like '"
                            + mGlobalInput.ComCode + "%' order by A";

                }
            }
            else if (mGlobalInput.ComCode.length() == 4)
            {
                mSQL = "select usercode A,username from lzcertifyuser where comcode like '"
                        + mGlobalInput.ComCode + "%' ";
            }
        }

        // 单证发放机构，
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "sendoutcode") == 0)
        {
            if (mGlobalInput.ComCode.length() == 2)
            {
                mSQL = "select ComCode, Name,'' from ldcom where comcode='86'";
            }
            else if (mGlobalInput.ComCode.length() == 4)
            {
                mSQL = "select ComCode, Name,'' from ldcom where 1=1 and Sign='1' and "
                        + " comcode='" + mGlobalInput.ComCode + "' ";
            }
            else if (mGlobalInput.ComCode.length() == 8)
            {
                mSQL = "select usercode,username from LDUser where usercode='"
                        + mGlobalInput.Operator + "'";
            }
        }

        // 单证接收机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "receivecode") == 0)
        {
            if (mGlobalInput.ComCode.length() == 2)
            {
                mSQL = "select ComCode, Name,'' from ldcom where 1=1 and Sign='1' and "
                        + " length(trim(comcode))=4 ";
            }
            else if (mGlobalInput.ComCode.length() == 4)
            {
                mSQL = "select usercode,username from lzcertifyuser "
                        + " where 'B'||usercode in (select receivecom from lzaccess where sendoutcom = 'A'||"
                        + mCodeCondition + ")";
            }
        }
        // 配置定额单证接受代码 wanglong
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "cardreceivecode") == 0)
        {
            mSQL = "select code,codename from ldcode where codetype='cardreceivecode' ";

        }
        // 单证错误类型
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "certifyerrortype") == 0)
        {
            mSQL = "select code,codename from ldcode where codetype='certifyerror'";
        }

        // 征订季度查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "orderquarter") == 0)
        {
            mSQL = " select distinct cast(year(attachdate) as char(4))||cast(((month(attachdate))-1)/3+1 as char(1)) b, "
                    + " cast(year(attachdate) as char(4))||'年'||cast(((month(attachdate))-1)/3+1 as char(1))||'季度  ' a  "
                    + " from  LZOrder ";
        }

        // 再保公司代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "reinsurecomcode") == 0)
        {
            mSQL = "select recomcode,recomname From lrrecominfo";
        }

        // 再保临分被保人查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "lrinsuredno") == 0)
        {
            mSQL = "select a.insuredno,a.insuredname,a.riskcode,a.amnt,a.prem,a.riskamnt,a.ProposalNo,'' "
                    + " from LCPol a where  Reinsureflag in ('3','4','5') and prtno='"
                    + mConditionField + "'";
        }
        // 再保合同代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "recontcode") == 0)
        {
            mSQL = "select RecontCode,Recontname from lrcontinfo where "
                    + mConditionField + "=" + this.mCodeCondition;
        }
        // 再保险种代码
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "reriskcode") == 0)
        {
            mSQL = "select distinct a.RiskCode,b.RiskName from LRCalFactorValue a,LMRisk b where a.RiskCode = b.RiskCode and "
                    + mConditionField + "=" + this.mCodeCondition;
        }

        // 根据客户号查询客户地址
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "addressno") == 0)
        {
            if (mCodeCondition.length() == 9 + 2)
            {
                mSQL = "select b.addressNo, b.postalAddress "
                        + "from LCAppnt a, LCAddress b, LCCont c "
                        + "where a.addressNo = b.addressNo "
                        + "    and a.appntNo = b.customerNo "
                        + "    and a.contNo = c.contNo "
                        + "    and c.appFlag = '1' " + "    and a.appntNo = "
                        + mCodeCondition + " order by int(addressNo)";
            }
            else if (mCodeCondition.length() == 8 + 2)
            {
                mSQL = "  select b.addressNo, b.grpAddress "
                        + "from LCGrpAppnt a, LCGrpAddress b, LCGrpCont c "
                        + "where a.addressNo = b.addressNo "
                        + "    and a.customerNo = b.customerNo "
                        + "    and a.grpContNo = c.grpContNo "
                        + "    and c.appFlag = '1' "
                        + "    and a.customerNo = " + mCodeCondition
                        + " order by int(addressNo)";
            }
        }

        // 查询信箱拥有者的名称: 可能为个人或团体
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "ownername") == 0)
        {
            // 小组
            mSQL = "  select groupNo as code, groupName " + "from LGGroup "
                    + "union " + "select userCode as code, userName "
                    + "from LDUser order by code";
        }

        // 查询信箱拥有者的名称: 可能为个人或团体
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "scanbusstype") == 0)
        {
            // 小组
            mSQL = "  select distinct BussType, BussTypeName "
                    + "from Es_Doc_Def " + "union " + "select '', '' "
                    + "from dual ";
        }
        else if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                .compareTo("scansubtype") == 0)
        {
            // 小组
            mSQL = " select distinct SubType SubType, SubTypeName SubTypeName from Es_Doc_Def "
                    + " union "
                    + " select '' SubType, '' SubTypeName from dual "
                    + " order by SubType ";
        }
        else if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                .compareTo("edortypep") == 0)
        {
            mSBql.append("select EdorCode, EdorName from LMEdorItem ").append(
                    "where AppObj = 'I' ").append("order by EdorCode ");
            mSQL = mSBql.toString();
        }
        else if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                .compareTo("edortypeg") == 0)
        {
            mSBql.append("select EdorCode, EdorName from LMEdorItem ").append(
                    "where AppObj = 'G' ").append("order by EdorCode ");
            mSQL = mSBql.toString();
        }
        else if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                .compareTo("salechnlall") == 0)
        {
        	//zff 2015-01-14 将code = ‘03’ 去掉 添加团险中介的下拉选项
            mSBql.append("select distinct Code, CodeName, ").append(
                    "   CodeAlias, ComCode, OtherSign ").append("from LDCode ")
                    .append("where (CodeType = 'lcsalechnl' ").append(
                            "      or codetype = 'salechnl') ").append(
                            "   and CodeName not like '%其他%' and code not in ('06','16') ").append(
                            "order by Code");
            mSQL = mSBql.toString();
            
        }

        //业务性质编码引用StatusKind
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "statuskind") == 0)
        {

            mSQL = "select distinct code,codename from ldcode "
                    + "where codetype in ('salechnl','lcsalechnl') order by code";
        }

        //文件详细类型
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "filetypedetail") == 0)
        {

            mSQL = "SELECT Code1, CodeName, CodeAlias FROM LDCode1 WHERE CodeType = 'filetypedetail' AND Code = "
                    + mCodeCondition + " ORDER BY Code1";
        }

        //存在保险帐户险种编码引用RiskCode
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
                .toLowerCase().compareTo("riskcodebyacc") == 0)
        {
            mSQL = "select distinct a.RiskCode , a.RiskName "
                    + "from LMRiskApp a ,LMRiskToAcc b "
                    + "where a.RiskCode = b.RiskCode "
                    + "   and a.RiskType4 = '4' " + "order by a.RiskCode ";
        }

        //险种编码_保险帐户号码_名称
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "insuaccno") == 0)
        {
            mSQL = "SELECT InsuAccNo, InsuAccName FROM LMRiskToAcc WHERE RiskCode = "
                    + mCodeCondition + " ORDER BY InsuAccNo ";
        }

        //显示团个险险种
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskprop") == 0)
        {
            String riskprop = "";
            if (mCodeCondition.equals("'1'"))
            {
                riskprop = "I";
            }
            else if (mCodeCondition.equals("'2'"))
            {
                riskprop = "G";
            }
            mSQL = "select Riskcode, Riskname from lmriskapp where "
                    + mConditionField + " = '" + riskprop
                    + "' order by riskcode ";
        }
        
        //显示团个险险种
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "riskprop2") == 0)
        {
            String riskprop = "";
            if (mCodeCondition.equals("'1'"))
            {
                riskprop = "I";
            }
            else if (mCodeCondition.equals("'2'"))
            {
                riskprop = "G";
            }
            else if(mCodeCondition.equals("''")||mCodeCondition==null)
            {
                riskprop = "I','G";
            }
            mSQL = "select Riskcode, Riskname from lmriskapp where "
                    + mConditionField + " in ('" + riskprop
                    + "') order by riskcode ";
        }
        
        //续期缴费方式  add by lh
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "expaymode") == 0)
                {
                    mSQL = "SELECT code,codename FROM ldcode WHERE codetype='expaymode'   ORDER BY int(code) ";
                    System.out.println(mSQL);
                }
        
        //银保通地区信息查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "zoneno") == 0)
        {
            mSQL = "SELECT zoneno,'' FROM LKCodemapping WHERE bankcode = "
                    + mCodeCondition + " ORDER BY zoneno ";
        }
        //      银保通网点信息查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "banknode") == 0)
        {
            mSQL = "SELECT banknode,'' FROM LKCodemapping WHERE zoneno = "
                    + mCodeCondition + " ORDER BY banknode ";
        }

        //代理机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcominput") == 0)
        {
            int branchIndex = mCodeCondition.indexOf("^");
            int manageComIndex = mCodeCondition.lastIndexOf("^");
            String tBranchType = mCodeCondition.substring(1, branchIndex);
            String tBranchType2 = mCodeCondition.substring(branchIndex + 1, manageComIndex);
            String tManageCom = mCodeCondition.substring(manageComIndex + 1, mCodeCondition.length() - 1);

            mSBql.append(" select lac.AgentCom, lac.Name, ");
            mSBql.append(" lac.UpAgentCom, lac.AreaType, lac.Channeltype ");
            mSBql.append(" from LACom lac ");
            mSBql.append(" where lac.ManageCom = '" + tManageCom + "' ");
            mSBql.append(" and lac.BranchType = '" + tBranchType + "' ");
            mSBql.append(" and lac.BranchType2 in ('" + tBranchType2 + "') ");
            mSBql.append(" and lac.AcType != '05' ");
            mSBql.append(" and (lac.EndFlag='N' or lac.EndFlag is null) ");
            mSBql.append(" order by lac.AgentCom ");

            mSQL = mSBql.toString();
        }

        // 共保机构查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "coinsurancecom") == 0)
        {
            String tManageCom = mCodeCondition;

            mSBql.append(" select lac.AgentCom, lac.Name, ");
            mSBql.append(" lac.UpAgentCom, lac.AreaType, lac.Channeltype ");
            mSBql.append(" from LACom lac ");
            mSBql.append(" where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and lac.ManageCom like '" + mGlobalInput.ManageCom + "%'");
            //共保公司录入时，可查询出互动渠道与团险中介渠道的 共保机构。
            mSBql.append(" and ((lac.BranchType = '2' and lac.BranchType2 = '02') ");
            mSBql.append(" or lac.BranchType = '5') ");
//            mSBql.append(" and lac.BranchType = '2' ");
//            mSBql.append(" and lac.BranchType2 = '02' ");
            mSBql.append(" and lac.AcType = '05' ");
            mSBql.append(" and lac.SellFlag = 'Y' ");
            mSBql.append(" and (lac.EndFlag='N' or lac.EndFlag is null) ");
            mSBql.append(" order by lac.AgentCom ");

            mSQL = mSBql.toString();
        }
        // --------------------------------

        //套餐信息查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
            "riskwrapcode") == 0)
        {
            mSBql.append("select RiskWrapCode, WrapName, WrapType, 'W' from LDWrap where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" order by RiskWrapCode");

            mSQL = mSBql.toString();
        }
        
         // 套餐编码引用团险卡折 by 20100707
      if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
              .toLowerCase().compareTo("grpwrapcode") == 0)
      {
           mSBql.append("select RiskWrapCode,WrapName from LDWrap ");
           mSBql.append("where riskwrapcode in ( select distinct b.riskcode from lmcertifydes a,lmcardrisk b " +
           		" where a.certifyclass='D'  and a.certifycode=b.certifycode  and b.risktype='W' ");
           mSBql.append("and a.managecom ");
           mSBql.append(" like '");
        if(!mCodeCondition.equals("")&&mCodeCondition.length()>=2){   
           mSBql.append(mCodeCondition.substring(1,
                   (mCodeCondition.length() - 1)).trim());
        }
           mSBql.append("%') order by RiskWrapCode");

           mSQL = mSBql.toString();
           System.out.println(mSQL);

      }
      
//    特殊产品定义时，选择可打印医疗机构的险种SpecRiskCode by gzh 20110718
      if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase()
              .toLowerCase().compareTo("specriskcode") == 0)
      { 
          mSQL = "select code,codename from ldcode where codetype = 'printriskcode'  order by Code";
      }
      
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "paymode1") == 0)
        {
            mSQL = "SELECT code,codename FROM ldcode WHERE codetype='paymode' and code not in ('4','9','14','15','10','8','21','22','20','23')  ORDER BY int(code) ";
        }

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "paymode2") == 0)
        {
            mSQL = "SELECT code,codename FROM ldcode WHERE codetype='paymode' and code not in ('4','6','13','10','9','14','15','16','17','8','19','21','22','20','23')  ORDER BY int(code) ";
            System.out.println(mSQL);
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "paymode3") == 0)
        {
        	mSQL = "SELECT code,codename FROM ldcode WHERE codetype='paymode' and code not in ('6','13','10','9','14','15','16','17')  ORDER BY int(code) ";
        	System.out.println(mSQL);
        }
         if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
         "bankfinish") == 0)
        {
            mSQL = "select code,codename from ldcode where 1=1 and "
                   +mConditionField+"="+ mCodeCondition + " and codetype='bankfinish' order by code";
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("bankmoneyk") == 0)
        {
        	//mSQL = "SELECT code,codename FROM ldcode WHERE codetype='bankmoney' and othersign = '1' order by int(code) with ur ";
            //System.out.println(mSQL);
        	mSBql.append("SELECT code,codename FROM ldcode WHERE codetype='bankmoney' and othersign = '1' and ");
        	mSBql.append(mConditionField);
        	mSBql.append(" = ");
        	mSBql.append(mCodeCondition);
        	mSBql.append(" order by int(code) ");
        	mSQL = mSBql.toString();
        }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("bankmoneyj") == 0)
        {
        	//mSQL = "SELECT code,codename FROM ldcode WHERE codetype='bankmoney' and othersign = '0' order by int(code) with ur";
            //System.out.println(mSQL);
        	mSBql.append("SELECT code,codename FROM ldcode WHERE codetype='bankmoney' and othersign = '0' and ");
        	mSBql.append(mConditionField);
        	mSBql.append(" = ");
        	mSBql.append(mCodeCondition);
        	mSBql.append(" order by int(code) ");
        	mSQL = mSBql.toString();
        }
        //互动加扣款种类
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("activebankmoneyk") == 0)
        {
        	//mSQL = "SELECT code,codename FROM ldcode WHERE codetype='bankmoney' and othersign = '1' order by int(code) with ur ";
            //System.out.println(mSQL);
        	mSBql.append("SELECT code,codename FROM ldcode WHERE codetype='activebankmoney' and othersign = '1' and ");
        	mSBql.append(mConditionField);
        	mSBql.append(" = ");
        	mSBql.append(mCodeCondition);
        	mSBql.append(" order by int(code) ");
        	mSQL = mSBql.toString();
        }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("activebankmoneyj") == 0)
        {
        	//mSQL = "SELECT code,codename FROM ldcode WHERE codetype='bankmoney' and othersign = '0' order by int(code) with ur";
            //System.out.println(mSQL);
        	mSBql.append("SELECT code,codename FROM ldcode WHERE codetype='activebankmoney' and othersign = '0' and ");
        	mSBql.append(mConditionField);
        	mSBql.append(" = ");
        	mSBql.append(mCodeCondition);
        	mSBql.append(" order by int(code) ");
        	mSQL = mSBql.toString();
        }
        //健康管理加扣款种类
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("healthmoneyaddj") == 0)
        {
        	//mSQL = "SELECT code,codename FROM ldcode WHERE codetype='bankmoney' and othersign = '0' order by int(code) with ur";
            //System.out.println(mSQL);
        	mSBql.append("SELECT code,codename FROM ldcode WHERE codetype='healthmoneyadd' and othersign='0' and  ");
        	mSBql.append(mConditionField);
        	mSBql.append(" = ");
        	mSBql.append(mCodeCondition);
        	mSBql.append(" order by int(code) ");
        	mSQL = mSBql.toString();
        }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("healthmoneyaddk") == 0)
        {
        	//mSQL = "SELECT code,codename FROM ldcode WHERE codetype='bankmoney' and othersign = '0' order by int(code) with ur";
            //System.out.println(mSQL);
        	mSBql.append("SELECT code,codename FROM ldcode WHERE codetype='healthmoneyadd' and othersign = '1' and ");
        	mSBql.append(mConditionField);
        	mSBql.append(" = ");
        	mSBql.append(mCodeCondition);
        	mSBql.append(" order by int(code) ");
        	mSQL = mSBql.toString();
        }
        
//      万能主险编码引用omrisk
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("omrisk") == 0)
        {
        	mSQL = "select riskcode,riskname from lmriskapp where risktype4 = '4' order by riskcode with ur ";
        }
        
        //理赔重点业务查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("llgrptypes") == 0) {
            mSQL = "select '0','全部' from dual where 1=1 union"
                 + " SELECT Code, CodeName FROM LDCode WHERE CodeType = 'llgrptype' ";
        }
        //      理赔疾病风险等级查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("risklevel") == 0) {
            mSQL = " SELECT Code,codename FROM LDCode WHERE CodeType = 'Risklevel' ";
        }
//      理赔疾病风险存续期查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("riskcontinueflag") == 0) {
            mSQL = " SELECT Code,codename FROM LDCode WHERE CodeType = 'Riskcontinueflag' ";
        }
        //生存给付的个险险种，不包括万能险
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("cangetrisk") == 0)
        {
        	mSBql.append("select RiskCode, RiskName from LMRiskApp a where RiskProp = 'I' and risktype4 <> '4' ");
            mSBql.append("and exists (select 1 from LMRisk where RiskCode = a.RiskCode and GetFlag = 'Y') ");
            //20141010附加险存在给付责任
//          mSBql.append("and not exists (select 1 from ldcode1 where code = a.riskcode and codetype = 'checkappendrisk') with ur ");
            mSBql.append(" order by RiskCode with ur ");

            mSQL = mSBql.toString();
        }

        
//      取8位代理机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("agentcom8") == 0)
        {
            mSBql.append("select AgentCom,Name from LACom where 1 = 1 and length(trim(AgentCom)) = 8 and ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by AgentCom ");

            mSQL = mSBql.toString();
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("rptname") == 0)
        {
            mSQL = "SELECT Remark1,ReportName FROM LDDLLoc  ORDER BY int(Remark1) ";
        }
        //财接报表名称查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("cjreportname") == 0)
        {
            mSQL = "SELECT Remark1,ReportName FROM LDDLLoc where remark2='CJ' ORDER BY int(Remark1) ";
        }
        
        //财接用户名查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("cjusercode") == 0)
        {    
          mSQL ="select usercode,username from lduser where comcode ="+mCodeCondition+" ORDER BY usercode ";
          
        }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("llusercode") == 0)
        {   //String manageCom=mCodeCondition;
        	//System.out.println("**ManageCom*** "+manageCom);
            mSQL = "SELECT usercode,username FROM llclaimuser where "
            	   + mConditionField + " = " + mCodeCondition + " ORDER BY usercode ";
        
        } 
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("llsocialcode") == 0)
        {   
            mSQL = "SELECT usercode,username FROM LLSocialClaimUser where "
            	   + mConditionField + " = " + mCodeCondition + " ORDER BY usercode ";
        
        } 
        //分别取理赔岗用户和非理赔岗用户
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("claimusercode") == 0)
        {    
      	  System.out.println("**mCodeCondition*** "+mCodeCondition);
          int manageComIndex = mCodeCondition.indexOf("^");
          String claimuser = mCodeCondition.substring(1, manageComIndex);
          String tManageCom = mCodeCondition.substring(manageComIndex + 1, mCodeCondition.length() - 1);
           System.out.println("**claimuser*** "+claimuser+"**tManageCom**"+tManageCom);
        	if(claimuser.equals("1")){
        		mSQL ="SELECT usercode,username FROM llclaimuser where comcode like  '"+tManageCom+"%' ORDER BY usercode ";
        	}else{
        		mSQL ="select usercode,username from lduser where comcode like  '"+tManageCom+"%' and not exists (select 1 from llclaimuser where usercode=lduser.usercode) ORDER BY usercode ";
        	}  
        }
        
        //理赔伤残	add by Houyd #1777 新伤残评定标准及代码系统定义 响应mulline中的双击事件
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("desc_invalidism") == 0)
        {   
            mSQL = "SELECT codename,codealias,code,comcode,othersign FROM ldcode where "
            	   + mConditionField + " like '%" + mCodeCondition.substring(1, (mCodeCondition.length() - 1)).trim() + "%' and codetype='desc_invalidism' ORDER BY code with ur ";
        
        } 
        
//      by gzh 20110402 个单受益人速填
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).equalsIgnoreCase(
            "CustomertypeCodeQueryBL")) {
        	String[] WherePH = mCodeCondition.split(",");
            mSBql.append("select 'A-投保人',appntname,idtype,idno,'' from lcappnt where "+WherePH[0]);
            mSBql.append(" union select 'I-被保人',name,idtype,idno,relationtomaininsured from lcinsured where "+WherePH[0]+" and "+ WherePH[1]);
//            mSBql.append(mCodeCondition);
//            mSBql.append("'union select 'I-被保人',");
//            mSBql.append(mCodeCondition.substring(11));
//            mSBql.append(" order by FactoryCode,FactorySubCode ");
            mSQL = mSBql.toString();
            //break SelectCode;
        }
        //2015-4-28 by yangyang
     // 代理人营业组别引用AgentGroup2
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "aagentgroup2") == 0)
        {
            // mSQL = "select AgentGroup, Name from LABranchGroup where "
            // + mConditionField + " = " + mCodeCondition
            // + " and BranchLevel = '01' and ManageCom like '"
            // + mGlobalInput.ManageCom + "%' order by AgentGroup";

            mSBql.append("select replace(a.AgentGroup2,a.AgentGroup,trim(b.BranchAttr)) ,(select name from laagent where a.agentcode = agentcode) from latree a ,labranchgroup b where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and a.agentgrade ='B01' and trim(a.AgentGroup2)<>'' and a.AgentGroup2 is not null  and a.agentgroup = b.agentgroup and a.ManageCom like '");
            mSBql.append(mGlobalInput.ManageCom);
            mSBql.append("%' order by a.AgentGroup");

            mSQL = mSBql.toString();
        }
//        by gzh 20120607 
//        项目制管理增加省市县选择
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("province") == 0)
        {
            mSBql.append("select Code,CodeName from LDCode1 where codetype = 'province' and ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" order by code ");

            mSQL = mSBql.toString();
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("city") == 0)
        {
            mSBql.append("select Code,CodeName from LDCode1 where codetype = 'city' and ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" order by code ");

            mSQL = mSBql.toString();
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("county") == 0)
        {
            mSBql.append("select Code,CodeName from LDCode1 where codetype = 'county' and ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" order by code ");

            mSQL = mSBql.toString();
        }
        //增加新的省市县查询
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("province1") == 0)
        {
            mSBql.append("select Code,CodeName from LDCode1 where codetype = 'province1' and ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" order by code ");

            mSQL = mSBql.toString();
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("city1") == 0)
        {
            mSBql.append("select Code,CodeName from LDCode1 where codetype = 'city1' and ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" order by code ");

            mSQL = mSBql.toString();
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("county1") == 0)
        {
            mSBql.append("select Code,CodeName from LDCode1 where codetype = 'county1' and ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" order by code ");

            mSQL = mSBql.toString();
        }
        //添加税优算费
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("discountmode") == 0)
        {
            mSBql.append("select Code,CodeName from LDCode1 where codetype = 'discountmode' and ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" order by code ");

            mSQL = mSBql.toString();
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("projectyear1") == 0)
        {
            mSBql.append("select ProjectYear,(select codename from ldcode where codetype = 'projectyear' and code = projectyear) from LCProjectYearInfo where 1=1 and ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" order by ProjectYear ");

            mSQL = mSBql.toString();
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("projecttype1") == 0)
        {
            mSBql.append("select code,codename from ldcode where codetype = 'projecttype1' and ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" order by int(code) ");

            mSQL = mSBql.toString();
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("projectmonth") == 0)
        {
            mSBql.append("select code,codename from ldcode where codetype = 'projectmonth' and ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" order by int(code) ");

            mSQL = mSBql.toString();
        }
//        by gzh 20120607 end
//      by gzh 20121031 健享全球方案编码，原直接在ldcode中获取，现表定费率和约定费率方案编码不一致，调整为在此提取。
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("insuplancode") == 0)
        {
        	String tBWhere = "";//表定费率条件
        	if ("'1'".equals(mCodeCondition))
            {
        		tBWhere = " and Othersign = '0' ";
            }
        	mSBql.append("select Code,Codename from LDCode where codetype = 'insuplancode' ");
            mSBql.append(tBWhere);
            mSBql.append(" order by int(comcode) ");

            mSQL = mSBql.toString();
        }
        
//      卡折结算CertifyCodeJS，考虑停售产品问题
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "certifycodejs") == 0)
        {
            mSQL = "SELECT distinct lcd.CertifyCode, lcd.CertifyName, lcd.SubCode FROM LMCertifyDes lcd "
            	    + "inner join LMCardRisk lcr on lcr.CertifyCode = lcd.CertifyCode "
            	    + "inner join LDRiskWrap lrw on lrw.RiskWrapCode = lcr.RiskCode "
            	    + "WHERE "
                    + mConditionField
                    + " = "
                    + mCodeCondition
                    + " and  lcd.CertifyClass = 'D' "
                    + " and (lrw.EndDate is null or lrw.EndDate >= '"+PubFun.getCurrentDate()+"')"
                    + " order by CertifyCode";
        }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("position1") == 0)
        {   //String manageCom=mCodeCondition;
        	//System.out.println("**ManageCom*** "+manageCom);
            mSQL = "SELECT gradecode,gradename FROM lcgrpposition where " + mConditionField + " " +
            		" = " + mCodeCondition + "  ";
        
        } 

        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
				"invoicecertify") == 0) {
			if (mGlobalInput.ManageCom != null
					&& mGlobalInput.ManageCom.length() > 3) {
				mSQL = "select distinct memo,(select certifyname from lmcertifydes where certifycode=memo) from LOPRTInvoiceManager where comcode like '"
						+ mGlobalInput.ManageCom.substring(0, 4)
						+ "%' and memo<>''";
			} else {
				mSQL = "select 1 from dual where 1=2";
			}
		}
        
        // 万能险缓缴业务查询报表界面使用  万能险险种  wdk于2013年5月9日添加
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("umriskcode") == 0) {
            mSQL = "select riskcode,riskname from lmriskapp a where risktype4 = '4' and  kindcode='U'  and riskprop='I' and exists " +
            		"(select 1 from lmriskpayintv where riskcode=a.riskcode and payintv!='0') order by riskcode with ur ";
        }
        

        // 单证征订印刷厂配置界面  wdk于2013年5月9日添加
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("printcode") == 0) {
            mSQL = "select printcode,printname from LMPrintCom  where EffectiveState='0' with ur " ;
        }
        
         //      单证征订 印刷单证配置界面
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("certifycomcode") == 0) {
        	String com = mGlobalInput.ManageCom;
        	 mSQL = "select ComCode, Name,'' from ldcom where 1=1 and Sign='1' and comcode like '"+com+"%'and "
                 + " length(trim(comcode))<=4   order by comcode  with  ur";
        }
        // 查询险种 取前4位 去重
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("coverage") == 0) {
        	mSQL="select distinct left(riskcode,4),'' from lmrisk with ur" ;
        }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
        "sendcomcode") == 0)
        {
        	if (mGlobalInput.ComCode.length() == 2)
        	{
        		mSQL = "select ComCode, Name,'' from ldcom where 1=1 and Sign='1' and "
        			 + " length(trim(comcode))=4 and comcode like '"+ mGlobalInput.ComCode+"%' order by comcode";
        	}
        	else if (mGlobalInput.ComCode.length() == 4)
        	{
        		mSQL = "select ComCode, Name,'' from ldcom where 1=1 and Sign='1' and "
                    + " length(trim(comcode))=8 and comcode like '"+ mGlobalInput.ComCode+"%'order by comcode";
        	}
        }
        
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("newcertifycode") == 0) {
       	 mSQL = "select certifycode, certifyname,'' from LMCardDescription where 1=1 order by certifycode  with  ur";
       }
        
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("certifycodejhk") == 0) {
          	 mSQL = "select certifycode,certifyname from LMCertifyDes where length(trim(managecom))>=4  and operatetype = '0' order by certifycode with ur";
        }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("branchtypecode") == 0) {
            mSQL = "select replace(a.code||b.code, ' ','') ,a.codename||b.codename from ldcode a,ldcode b where a.codetype ='branchtype' and b.codetype ='branchtype2' and (b.code='01' or (a.code='1' and b.code in('02','03','04')) or (a.code='2' and b.code in('02','04')))";
          }
//      add lyc 2015-2-10 销售个险人力发展
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("humandeveimngcom") == 0) {
            mSQL = "select a.code,(select name from ldcom where comcode=a.code) from ldcode a where a.codetype='2016humandeveidesc' and code like '"+mGlobalInput.ManageCom+"%' order by a.code with ur";
          }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("humandeveimonth") == 0) {
            mSQL = "select codename,'考核月' from ldcode1 where 1 = 1 and codetype = 'humandeveimonth' order by Codename  with ur";
          }
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("comcode123") == 0) {
            mSQL = "select comcode,name from ldcom where 1 = 1 and (comgrade in('01','02','03')or comcode in(select code1 from ldcode1 where codetype = '2016includemanagecom') ) and sign='1' and comcode like '"+mGlobalInput.ManageCom+"%' order by comcode  with ur";
          } 
        //2015-6-16添加 江苏中介手续费支付
        //菜单：财务收付费-->财务付费-->手续费付费【按钮】
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("jsbatchno") == 0) {
            mSBql.append("select BatchNo,BatchNo from LAChargePayBatchLog a where (RegisteRequestType is null or(RegisteErrorCode <>'0000'  and RegisteRequestType is not null)) and BatchNo in( select batchno from lacharge where  ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" ) order by BatchNo ");

            mSQL = mSBql.toString();
          } 
        //再保账单增加 2015-10-23   yangyang 分保管理(新)-->账单管理-->账单管理结算-->账单结算
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("accountrecontcode") == 0) {
        	String tReComCode="";
        	String tRecontType="";
        	if(mCodeCondition != null && !mCodeCondition.trim().equals("") && !mCodeCondition.trim().equals("'|'")){
        		mCodeCondition = mCodeCondition.substring(1, mCodeCondition.length()-1);
        		String[] ReArray =  mCodeCondition.replace('|', '@').split("@");
        		
        		tReComCode = ReArray[0];
        		if(ReArray.length>1)
        			tRecontType = ReArray[1];
        	}
        	
        	//既有公司又有合同号
        	if(!tReComCode.trim().equals("") && !tRecontType.trim().equals("")){
        		if("C".equals(tRecontType))
            	{
        			mSQL = "select recontcode,recontname from lrcontinfo  c where recomcode='"+tReComCode+"'  ";
            	}
            	else if("T".equals(tRecontType))
            	{
            		mSQL = "select  tempcontcode,tempcontname  from lrtempcesscont c where  comcode='"+tReComCode+"' ";
            	}
        	}else if(!tReComCode.trim().equals("")){//只有公司
        		mSQL = "select recontcode,recontname from lrcontinfo  where  recomcode='"+tReComCode+"' " +
        				"union select  tempcontcode,tempcontname  from lrtempcesscont  where comcode='"+tReComCode+"' ";
        	}else if(!tRecontType.trim().equals("")){
        		if("C".equals(tRecontType))
            	{
        			mSQL = "select recontcode,recontname from lrcontinfo  c ";
            	}
            	else if("T".equals(tRecontType))
            	{
            		mSQL = "select  tempcontcode,tempcontname  from lrtempcesscont c";
            	}
        	}else{
        		mSQL = " select recontcode,recontname from lrcontinfo  c union select  tempcontcode,tempcontname  from lrtempcesscont c ";
        	}
        	
          }
//      add lyc 2015-10-15 #2668 账单录入添加收据类型  
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("receipttype") == 0) {
            mSQL = "select code,codename from ldcode where 1 = 1 and codetype='receipttype' order by othersign  with ur";
          } 
//      add lyc 2015-11-6 #2668 账单录入分割单收据单位
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("receipttype") == 0) {
            mSQL = "select code,codename from ldcode where 1 = 1 and codetype='receipttype' order by othersign  with ur";
          } 
        // 远程出单增加银代代理机构
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
                "agentcomyccd") == 0)
        {
            // mSQL =
            // "select AgentCom, Name, UpAgentCom, AreaType, ChannelType from
            // LACom where "
            // + mConditionField + " = " + mCodeCondition
            // + " and ManageCom like '" + mGlobalInput.ManageCom + "%' order by
            // AgentCom";

        	mSBql
            .append("select AgentCom, Name, UpAgentCom, AreaType, ChannelType from LACom where ");
		    mSBql.append("ManageCom like'");
		    mSBql.append(mCodeCondition.substring(1,
		            (mCodeCondition.length() - 1)).trim());
		    mSBql.append("%'");
		    mSBql.append(" and ManageCom like '");
		    mSBql.append(mGlobalInput.ManageCom);
		    mSBql.append("%' order by AgentCom");
		
		    mSQL = mSBql.toString();
		
		    executeType = 1;
        }
        
        //2017-10-10 新销售批处理维护功能
        //菜单：业务数据准备-->新提数试算  提取业务类型下拉
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("fetchbusstype") == 0) {
            mSBql.append("select extractdatatype,orderno||'('||describe||')' from lacommisionconfig where state ='1' and  ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append("  order by orderno");

            mSQL = mSBql.toString();
          } 
        
      //保全结案添加扫描件配置
        if(StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("conttypeedorcode") == 0){
        	mSBql.append("select edorcode,edorname from lmedoritem where appobj ");
        	if("'1'".equals(mCodeCondition)){
        		mSBql.append("= 'I'");
        	}else if("'2'".equals(mCodeCondition)){
        		mSBql.append("= 'G'");
        	}else{
        		mSBql.append("in ('I','G')");
        	}
        	mSBql.append(" order by edorcode");
        	mSQL = mSBql.toString();
        }
        
        //保全查询销售渠道
        if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("bqsalechnl") == 0){
        	mSBql.append("select distinct code,codename from ldcode where codetype = 'bqsalechnl'");
        	if(!"".equals(mConditionField)){
        		mSBql.append(" and ").append(mConditionField).append(" != ").append(mCodeCondition);
        	}
        	mSBql.append(" order by code");
        	mSQL = mSBql.toString();
        }
        
        
        
        //销售渠道为互动中介时，只可选择相互代理；销售渠道为互动直销时，只可选择联合展业、渠道共享、互动部、农网共建。
		if(StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo("crs_busstype") == 0){
			String tWhereSQL  = "othersign in('1','2')";
			if("'15'".equals(mCodeCondition)){
				tWhereSQL = "othersign in('1')";
			}
			if("'14'".equals(mCodeCondition)){
				tWhereSQL = "othersign in('3','2')";
			}
			
			mSQL = "select code,codename from ldcode where 1=1 and codetype = 'crs_busstype' and  " + tWhereSQL + "with ur";
			
		}
        
		
		//银保通险种查询 add by GaoJinfu 20180321
		if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
				"ybtriskcode") == 0) {

			mSQL = "select RiskCode,RiskName from LMRisk union select riskwrapcode,riskwrapname from ldriskwrap fetch first 3000 rows only with ur";
			executeType = 1;
		}
		//查询组训人员工号
		 if (StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase().compareTo(
	                "trainercode") == 0)
	        {
			 mSBql .append( "select a.trainercode,a.trainername from latrainer a where ");
			 mSBql.append(mConditionField);
			 mSBql.append(" = ");
	         mSBql.append(mCodeCondition);
	         mSBql.append(" order by trainercode");
	         mSQL=mSBql.toString();
	         executeType=1;
	        }
        
        // 其他LDCODE表中定义的引用
        if (mSQL.equals(""))
        {
            // mSQL =
            // "select Code, CodeName, CodeAlias, ComCode, OtherSign from ldcode
            // where "
            // + mConditionField + " = " + mCodeCondition
            // + " and codetype = '"
            // + StrTool.cTrim(mLDCodeSchema.getCodeType()).toLowerCase() +
            // "' order by Code";
            mSBql
                    .append("select Code, CodeName, CodeAlias, ComCode, OtherSign from ldcode where ");
            mSBql.append(mConditionField);
            mSBql.append(" = ");
            mSBql.append(mCodeCondition);
            mSBql.append(" and codetype = '");
            mSBql.append(StrTool.cTrim(mLDCodeSchema.getCodeType())
                    .toLowerCase());
            mSBql.append("' order by Code");

            mSQL = mSBql.toString();
            // TO_NUMBER(Code)
        }
        System.out.println("CodeQueryBL sql:" + mSQL);
        
        if (executeType == 0)
        {
            mResultStr = mExeSQL.getEncodedResult(mSQL, 1, 1000);
            // System.out.println(mResultStr);
        }
        else
        {
            // else if (executeType == 1) 增加查询出的条数，陈祥伟修改于20081015
            mResultStr = mExeSQL.getEncodedResult(mSQL, 1, 2000);
            // System.out.println(mResultStr);
        }

        if (mExeSQL.mErrors.needDealError())
        {
            // @@错误处理,在ExeSQL中已进行错误处理，这里直接返回即可。
            this.mErrors.copyAllErrors(mExeSQL.mErrors);
            // 如果sql执行错误，则返回sql描述
            System.out.println("Code Query Error Sql:" + mSQL);
        }
        mResult.clear();
        mResult.add(mResultStr);
        return true;
    }

    /**
     * 测试函数
     *
     * @param args
     *            String[]
     */
    public static void main(String[] args)
    {
        CodeQueryBL codeQueryBL1 = new CodeQueryBL();

        LDCodeSchema tLDCodeSchema = new LDCodeSchema();
        tLDCodeSchema.setCodeType("sendbankjlwj");

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("conditionField", "managecom");
        tTransferData.setNameAndValue("codeCondition", "86110000");

        VData tVData = codeQueryBL1.getResult();
        tVData.add(tTransferData);
        tVData.add(tLDCodeSchema);

        codeQueryBL1.submitData(tVData, "");
    }

    private void jbInit() throws Exception
    {
    }
}
