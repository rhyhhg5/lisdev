package com.sinosoft.lis.pubfun;

import java.text.DecimalFormat;

import com.sinosoft.utility.*;

public class GetTotalSum
{
    public GetTotalSum()
    {
    }

    public static String getTotalPay(String serialno)
    {
        ExeSQL exeSql = new ExeSQL();
        SSRS testSSRS = new SSRS();

        String sql =
            "select Totalmoney, Totalnum from lybanklog where SerialNo = '"
            + serialno.trim() + "'";
        testSSRS = exeSql.execSQL(sql);
        return testSSRS.GetText(1, 1);
    }

    public static String getTotalPic(String serialno)
    {
        ExeSQL exeSql = new ExeSQL();
        SSRS testSSRS = new SSRS();

        String sql =
            "select Totalmoney, Totalnum from lybanklog where SerialNo = '"
            + serialno.trim() + "'";
        testSSRS = exeSql.execSQL(sql);

        return testSSRS.GetText(1, 2);
    }
    
    public static String getPercentPay(String PayMoney)
    {
        String result="0";
        try{
            result=(String)(new DecimalFormat("0").format(Double.parseDouble(PayMoney)*100));
        }catch(Exception e){
            result="0";
        }
        return result;
    }

    public static void main(String[] str) {
        GetTotalSum tGetTotalSum = new GetTotalSum();
        String s = tGetTotalSum.getTotalPay("00000000000000000018");
        String w = tGetTotalSum.getTotalPic("00000000000000000018");
        System.out.println("s : "+s);
        System.out.println("w : "+w);
    }
}
