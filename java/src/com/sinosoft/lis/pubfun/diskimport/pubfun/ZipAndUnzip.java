/**
 * 
 */
package com.sinosoft.lis.pubfun;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

/**
 * @author Administrator
 *
 */
public class ZipAndUnzip
{
    private int BUFFER = 1024;
    
    private Log log = LogFactory.getLog(this.getClass().getName());//日志
    
    /**
     * 压缩--待压缩文件为本地文件
     * 
     * @param inputFileList 待压缩文件列表
     * @param zipFileList zip中文件列表
     * @param outputFileName 压缩后文件存放路径+文件名
     * @return
     */
    public boolean zip(List inputFileList, List zipFileList, String outputFileName)
    {
        try
        {
            //校验文件列表长度是否一致
            if(inputFileList == null || zipFileList == null || inputFileList.size() != zipFileList.size())
            {
                log.info("校验文件列表长度不一致！");
                return false;
            }
            
            //如果压缩文件存放目录不存在，创建目录
            outputFileName = outputFileName.replace('/', '\\');
            File file = new File(outputFileName);
            File parent = file.getParentFile();
            if(parent != null && (!parent.exists())){
                parent.mkdirs();
            }

            BufferedInputStream bufferedInputStream = null;
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(fileOutputStream));
            byte data[] = new byte[BUFFER];
            for(int i = 0; i < inputFileList.size(); i++)
            {
                String fileName = (String) inputFileList.get(i);
                FileInputStream fileInputStream = new FileInputStream(fileName);
                bufferedInputStream = new BufferedInputStream(fileInputStream, BUFFER);
                //zip中储存的文件
                ZipEntry zipEntry = new ZipEntry((String) zipFileList.get(i));
                zipOutputStream.putNextEntry(zipEntry);
                int count = 0;
                while((count = bufferedInputStream.read(data, 0, BUFFER)) != -1)
                {
                    zipOutputStream.write(data, 0, count);
                }
                bufferedInputStream.close();
                log.info("文件 " + fileName + " 压缩成功！");
            }
            zipOutputStream.close();
            return true;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * 压缩--待压缩文件须通过HTML下载
     * 
     * @param inputFileList 待压缩文件列表
     * @param zipFileList zip中文件列表
     * @param outputFileName 压缩后文件存放路径+文件名
     * @return
     */
    public boolean htmlZip(List inputFileList, List zipFileList, String outputFileName)
    {
        try
        {
            //校验文件列表长度是否一致
            if(inputFileList == null || zipFileList == null || inputFileList.size() != zipFileList.size())
            {
                log.info("校验文件列表长度不一致！");
                return false;
            }
            
            //如果压缩文件存放目录不存在，创建目录
            outputFileName = outputFileName.replace('\\', '/');
            File file = new File(outputFileName);
            File parent = file.getParentFile();
            if(parent != null && (!parent.exists())){
                if(!parent.mkdirs())
                {
                    log.info("创建文件夹失败：" + parent.getName());
                    return false;
                }
            }

            BufferedInputStream bufferedInputStream = null;
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(fileOutputStream));
            byte data[] = new byte[BUFFER];
            for(int i = 0; i < inputFileList.size(); i++)
            {
                String fileName = (String) inputFileList.get(i);
                URL url = new URL(fileName);
                URLConnection uc = url.openConnection();
                bufferedInputStream = new BufferedInputStream(uc.getInputStream(), BUFFER);
                //zip中储存的文件
                ZipEntry zipEntry = new ZipEntry((String) zipFileList.get(i));
                zipOutputStream.putNextEntry(zipEntry);
                int count = 0;
                while((count = bufferedInputStream.read(data, 0, BUFFER)) != -1)
                {
                    zipOutputStream.write(data, 0, count);
                }
                bufferedInputStream.close();
                log.info("文件 " + fileName + " 压缩成功！");
            }
            zipOutputStream.close();
            return true;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * 解压
     * @param zipFileName 需要解压zip文件名
     * @param outputDirectory 解压后文件存放路径
     * @return
     */
    public boolean unzip(String zipFileName, String outputDirectory)
    {
        try
        {
            ZipFile zipFile = new ZipFile(zipFileName);
            Enumeration enumeration = zipFile.getEntries();
            ZipEntry zipEntry = null;
            while(enumeration.hasMoreElements())
            {
                zipEntry = (ZipEntry) enumeration.nextElement();
                //创建输出目录
                File file = new File(outputDirectory + File.separator);
                file.mkdirs();
                //文件名
                String fileName = zipEntry.getName();
                
                if (zipEntry.isDirectory())
                {
                    fileName = fileName.substring(0, fileName.length() - 1);
                    //创建压缩文件中的文件夹目录
                    file = new File(outputDirectory + File.separator + fileName);
                    file.mkdirs();
                    log.info("创建目录：" + outputDirectory + File.separator + fileName);
                }
                else
                {
                    //在指定文件夹下创建文件
                    file = new File(outputDirectory + File.separator + fileName);
                    //zipfile读取文件是随机读取的，这就造成可能先读取一个文件
                    File parent = file.getParentFile();
                        if(parent != null && (!parent.exists())){
                            parent.mkdirs();
                    }
                    file.createNewFile();
                    InputStream inputStream = zipFile.getInputStream(zipEntry);
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    byte[] data = new byte[BUFFER];
                    int count = 0;
                    while ((count = inputStream.read(data)) != -1)
                    {
                        fileOutputStream.write(data, 0, count);
                    }
                    fileOutputStream.close();
                    inputStream.close();
                    log.info("解压文件 " + fileName + " 成功！");
                }
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO Auto-generated method stub
        ZipAndUnzip zip = new ZipAndUnzip();
        List inputFileList = new ArrayList();
        inputFileList.add("E:\\project\\新系统模型.pdm");
        inputFileList.add("E:\\project\\服务器环境管理.xls");
        inputFileList.add("E:\\project\\定额单证流程.doc");
        List zipFileList = new ArrayList();
        zipFileList.add("宝宝\\新系统模型.pdm");
        zipFileList.add("宝宝\\服务器环境管理.xls");
        zipFileList.add("宝宝\\定额单证流程.doc");
        String outputFileName = "E:\\project\\zip\\第一个压缩文件.zip";
        zip.zip(inputFileList, zipFileList, outputFileName);
        zip.unzip(outputFileName, "E:\\project\\unzip\\");
    }

}
