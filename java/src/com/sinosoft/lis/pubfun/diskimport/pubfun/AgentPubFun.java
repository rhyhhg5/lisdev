package com.sinosoft.lis.pubfun;


/**
 * <p>Title:销售管理系统</p>
 * <p>Description:销售管理的公共业务处理函数
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author
 * @version 1.0
 */
import java.text.*;
import java.util.*;

import com.sinosoft.utility.*;
import java.math.BigInteger;
import com.sinosoft.lis.db.LABranchLevelDB;
import com.sinosoft.lis.schema.LABranchLevelSchema;
import com.sinosoft.lis.db.LATreeDB;

public class AgentPubFun
{

    public AgentPubFun()
    {
    }

  

    /**
     * 转换业务系统的销售渠道为销售系统的展业类型的函数 author: liujw
     * @param cSalChnl 业务系统的销售渠道
     * @return String 销售系统的展业类型(BranchType)
     */
    public static String switchSalChnltoBranchType(String cSalChnl)
    {
        String tBranchType = "";

        cSalChnl = cSalChnl.trim();
        if (cSalChnl.equals("01"))
        { //团险
            tBranchType = "2";
        }
        else if (cSalChnl.equals("02"))
        { //个险
            tBranchType = "1";
        }
        else if (cSalChnl.equals("03") || cSalChnl.equals("04")
                 || cSalChnl.equals("05") || cSalChnl.equals("06"))
        //银行保险 兼业代理 专业代理 经纪公司
        {
            tBranchType = "3";
        }

        return tBranchType;
    }

    /**
     * parseTime
     *
     * @param endTime String
     * @param reportType String
     * @param reportCycle String
     * @return String[]
     */
    public static String[] parseTime(String endTime, int reportType,
                                     int reportCycle)
    {
        //当前时间形式,yyyy-MM-dd
//      String sourcePattern=getSessionPool().getConfig().getDefaultDateFormat();

//    int reportCycle=1;
        String sourcePattern = "yyyy-MM-dd";
        String targetPattern = "yyyy-MM-dd";

        SimpleDateFormat sdf = new SimpleDateFormat(sourcePattern); //用户定义形式
        SimpleDateFormat targetDF = new SimpleDateFormat(targetPattern); //数据库要求形式

        String reportStartTime = endTime;
        String reportEndTime = endTime;

        String settedEnd = endTime;

        Date endDate;
        Date startDate;
        try
        {
            endDate = sdf.parse(settedEnd);

            //报表类型
            int type = reportType;
            //报表周期
            int cycle = reportCycle;

            Calendar cal = Calendar.getInstance();
            cal.setTime(endDate);
            switch (type)
            {
                case 1: //日报
                    reportEndTime = targetDF.format(endDate);
                    cal.add(Calendar.DATE, ( -1) * cycle + 1); //开始时间=结束时间-周期+1
                    reportStartTime = targetDF.format(cal.getTime());
                    break;
                case 2: //月报
                    cal.set(Calendar.DAY_OF_MONTH, 1); //月初
                    cal.add(Calendar.MONTH, ( -1) * cycle + 1); //到周期的月初
                    reportStartTime = targetDF.format(cal.getTime()); //开始时间
                    cal.add(Calendar.MONTH, cycle); //周期末的下月初
                    cal.add(Calendar.DAY_OF_MONTH, -1); //周期末
                    reportEndTime = targetDF.format(cal.getTime());
                    break;
                case 3: //季报

                    // int month=cal.get(Calendar.MONTH)+1;  //月度,需要+1
                    int month = cal.get(Calendar.MONTH);
                    int quarter = (int) month / 3; //所在季度,0-spring;1-summer
                    cal.set(Calendar.MONTH,
                            quarter * 3 + (( -1) * cycle + 1) * 3); //季初-周期持续时间
                    cal.set(Calendar.DATE, 1); //设到1号
                    reportStartTime = targetDF.format(cal.getTime());

                    ////////////////////end time
                    cal.add(Calendar.MONTH, cycle * 3); //多了一天
                    cal.add(Calendar.DATE, -1);
                    reportEndTime = targetDF.format(cal.getTime());
                    break;
                case 4: //年报
                    cal.add(Calendar.YEAR, ( -1) * cycle + 1);
                    cal.set(Calendar.MONTH, Calendar.JANUARY);
                    cal.set(Calendar.DATE, 1); //设到年初
                    reportStartTime = targetDF.format(cal.getTime());

                    /////////
                    cal.add(Calendar.YEAR, cycle); //下一年元旦,多了一天
                    cal.add(Calendar.DATE, -1); //年末
                    reportEndTime = targetDF.format(cal.getTime());
                    break;
                default:
                    break;
            }
        }
        catch (ParseException ex)
        {

        }

        String[] result = new String[2];
        result[0] = reportStartTime;
        result[1] = reportEndTime;
        return result;
    }

    /**
     * 将日期格式化成“YYYY年MM月DD天”的格式，输入格式为“YYYY/MM/DD”，“YYYY-MM-DD”
     */
    public static String formatDatex(String cDate)
    {
        if (cDate.indexOf("/") != -1)
        {
            cDate = cDate.substring(0, cDate.indexOf("/")) + "年"
                    +
                    cDate.substring(cDate.indexOf("/") + 1,
                                    cDate.lastIndexOf("/")) +
                    "月"
                    + cDate.substring(cDate.lastIndexOf("/") + 1) + "日";
        }
        else if (cDate.indexOf("-") != -1)
        {
            cDate = cDate.substring(0, cDate.indexOf("-")) + "年"
                    +
                    cDate.substring(cDate.indexOf("-") + 1,
                                    cDate.lastIndexOf("-")) +
                    "月"
                    + cDate.substring(cDate.lastIndexOf("-") + 1) + "日";
        }
        return cDate;
    }

    /**
         * 转换业务系统的销售渠道为销售系统的展业类型的函数 author: liujw
         * @param cSalChnl 销售系统的展业类型(BranchType)
         * @return String  业务系统的销售渠道
         */
        //xjh Add 2005/04/13
        //展业类型到销售渠道的转化
        public static String[] switchBranchTypetoSalChnl(String cBranchType,
                                                  String cBranchType2)
        {
            String[] result = new String[3];
            //BranchType 1 个险,2 团险,3 银代 ，9 其他
            //BranchType 1 个险,2 团险,3 银代 ，9 其他
            //BranchType
             //SaleChnl ：
             //  个单 01  直销
             //  团单 02  直销
             //      03   中介, 银代
             //      07   职团开拓
             //      08    其他
             //SaleChnlDetail：
             //   目前新契约不用，系统默认为 01

            if(cBranchType == null || cBranchType.equals(""))
            {
                result[0] = "01,02,03,07,08";
                 result[2] = "01";
            }
            else if (cBranchType.equals("2") && cBranchType2.equals("01"))
            {
                result[0] = "02,07";
                result[2] = "01";
            }
            else if (cBranchType.equals("1") && cBranchType2.equals("01"))
            {
                result[0] = "01";
                result[2] = "01";
            }
            else if (cBranchType.equals("1") && cBranchType2.equals("02"))//
            {
                result[0] = "10";
                result[2] = "01";
            }
            else if(cBranchType.equals("1")&&cBranchType2.equals("03"))//jiaocha
            {
            	result[0] = "06";
                result[2] = "01";
            }
            else if (cBranchType.equals("2") && cBranchType2.equals("02"))
            {
                result[0] = "03";
                result[2] = "02";
            }
            else if (cBranchType.equals("3") && cBranchType2.equals("01"))
            {
               result[0] = "04,13";
               result[2] = "01";
            }
            else if (cBranchType.equals("9") && cBranchType2.equals("01"))
            {
               result[0] = "99";
               result[2] = "01";
            }

            else if(cBranchType.equals("4")&&cBranchType2.equals("01"))//jiaocha
            {
            	result[0] = "08";
                result[2] = "01";
            }
            //对于互动渠道的修改
            //2014-12-3 modify yangyang
            else if(cBranchType.equals("5")&&cBranchType2.equals("01"))//add by 2014-6-30
            {
            	result[0] = "14,15";
                result[2] = "01,02,03,13,14";
            }
            //2016-12-14新增渠道 salechnl=18 modify zhangyingying
            else if(cBranchType.equals("6")&&cBranchType2.equals("01"))//add by 2014-6-30
            {
            	result[0] = "16,18";
                result[2] = "01";
            }
            /**
             * 健管渠道
             */
            else if(cBranchType.equals("7")&&cBranchType2.equals("01"))//add by 2014-6-30
            {
            	result[0] = "17,23";
                result[2] = "01";
            }
            /**
             * 社保中介
             */
            else if(cBranchType.equals("6")&&cBranchType2.equals("02"))//add by 2014-6-30
            {
            	result[0] = "20";
                result[2] = "01";
            }
            /**
             * 电商
             */
            else if(cBranchType.equals("8")&&cBranchType2.equals("02"))//add by 2014-6-30
            {
            	result[0] = "21,22";
                result[2] = "01";
            }
             result[1] = "01";


            //BranchType2 01 直销,02 中介,03 交叉,04 银代
            //SaleChnlDetail 01 直销团队,02 经纪公司,03 代理公司,04 兼业代理公司,05 交叉销售，06 银行代理
//            if(cBranchType2 == null || cBranchType2.equals(""))
//            {
//                result[1] = "01,02,03,04,05,06";
//            }
//            else if (cBranchType2.equals("01"))
//            {
//                result[1] = "01";
//            }
//            else if (cBranchType2.equals("02"))
//            {
//                result[1] = "02,03,04";
//            }
//            else if (cBranchType2.equals("03"))
//            {
//                result[1] = "05";
//            }
//            else if (cBranchType2.equals("04"))
//            {
//                result[1] = "06";
//            }
            return result;
        }

        //xjh Add 2005/04/14
          //销售渠道到展业类型的转化
          public static String[] switchSalChnl(String cSaleChnl,String cSaleChnlDetail,String tAcType)
          {
        	  //为了支持互动渠道下所有的业务员都可以卖直销和互动两个渠道下的保单，增加数组长度
        	  //2014-12-3  modifyby yangyang
              String[] result = new String[3];
              //BranchType 1 个险,2 团险,3 银代
              //SaleChnl ：
              //  个单 01  直销
              //  团单 02  直销
              //      03   中介
              //      07   职团开拓
              //      08    其他
              //SaleChnlDetail：
              //   目前新契约不用，系统默认为 01

              if(cSaleChnl == null || cSaleChnl.equals(""))
              {
                  result[0] = "";
                  result[1] = "";
              }
              else if (cSaleChnl.equals("01"))
              {
                  result[0] = "1";
                  result[1] = "01";
              }
              else if (cSaleChnl.equals("02"))
              {
                  result[0] = "2";
                  result[1] = "01";
              }
              else if (cSaleChnl.equals("03"))
              {
                      result[0] = "2";
                      result[1] = "02";
              }
/*modified by miaoxz,2009-1-13
 * salechnl='04',银代,只能通过中介机构出单
 * salechnl='13',银代直销，即银代业务员不通过中介机构直接出单
 */
              else if (cSaleChnl.equals("04")
            		  || cSaleChnl.equals("13")) //this line is new added by miaoxz
              {
                      result[0] = "3";
                      result[1] = "01";
              }
/*
 * here end-miaoxz
 */

              /**
               *  modify by fant 2014-6-26
               *  salechnl = '14' 互动直销渠道 不通过中介机构直接出单
               *  salechnl = '15' 互动中介渠道 通过中介机构出单
               */
//              else if (cSaleChnl.equals("14")
//            		  || cSaleChnl.equals("15")) //this line is new added by miaoxz
//              {
//                      result[0] = "5";
//                      result[1] = "01";
//              }
              //根据新需求，契约录入时，销售渠道为14-互动直销，未勾选交叉销售的业务
              //契约录入时，销售渠道为15-互动中介，勾选了交叉销售,交叉销售类型为：01-相互代理、02-联合展业、03-渠道共享、13-互动部(销售人身险业务)、14-农网共建（互动业务）
              //2014-12-3  modifyby yangyang
              else if (cSaleChnl.equals("14")&&(null==tAcType || tAcType=="")) //this line is new added by miaoxz
              {
                      result[0] = "5";
                      result[1] = "01";
                      result[2] = "1";
              }
              else if (cSaleChnl.equals("15")&&(null==tAcType || tAcType=="")) //this line is new added by miaoxz
              {
                      result[0] = "5";
                      result[1] = "01";
                      result[2] = "2";
              }
              //互动中介
              else if (cSaleChnl.equals("15")&&tAcType!=null&&!"".equals(tAcType)) //this line is new added by miaoxz
              {
                      result[0] = "5";
                      result[1] = "01";
                      result[2] = tAcType;
              }
              /**
               *  modify by fant 2014-6-26
               *  salechnl = '16' 社保直销渠道
               */
              /*else if (cSaleChnl.equals("16"))//个销团交叉
              {
            	  result[0] = "2";
            	  result[1] = "03";
              }*/
              /**
               * modify by zhuxt 2014-7-28
               * salechnl = '16' 社保直销  branchtype = '6' ,branchtype2 = '01'
               */
              else if (cSaleChnl.equals("16"))//个销团交叉
              {
            	  result[0] = "6";
            	  result[1] = "01";
              }
              //新增渠道2016-12-14 zyy
              else if (cSaleChnl.equals("20"))//社保综拓中介
              {
            	  result[0] = "6";
            	  result[1] = "02";
              }
              else if (cSaleChnl.equals("22"))//电商中介
              {
            	  result[0] = "8";
            	  result[1] = "02";
              }
              else if (cSaleChnl.equals("21"))//电商直销
              {
            	  result[0] = "8";
            	  result[1] = "02";
              }
              else if (cSaleChnl.equals("18"))//社保综拓渠道
              {
            	  result[0] = "6";
            	  result[1] = "01";
              }
              /**
               * modify by yangy  2014-12-31
               * salechnl ='17' 健管渠道   branchtype='7'  ,branchtype2='01'
               */
              else if (cSaleChnl.equals("17")
            		  || cSaleChnl.equals("23"))//健管渠道 
              {
            	  result[0] = "7";
            	  result[1] = "01";
              }
              else if (cSaleChnl.equals("06"))//个销团交叉
              {
            	  result[0] = "1";
            	  result[1] = "03";
              }
              else if (cSaleChnl.equals("07")) //团险职团开拓
              {
                      result[0] = "2";
                      result[1] = "01";
              }
              else if (cSaleChnl.equals("08")) //电话销售
              {
                      result[0] = "4";
                      result[1] = "01";
              }
              else if (cSaleChnl.equals("10")) //
              {
                      result[0] = "1";
                      result[1] = "02";
              }
              else if(cSaleChnl.equals("99"))   //其他
              {
                  result[0] = "9";
                  result[1] = "01";
              }
//              //BranchType2 01 直销,02 中介,03 交叉,04 银代
//              //SaleChnlDetail 01 直销团队,02 经纪公司,03 代理公司,04 兼业代理公司,05 交叉销售，06 银行代理
//              if(cSaleChnlDetail == null || cSaleChnlDetail.equals(""))
//              {
//                  result[1] = "";
//              }
//              else if (cSaleChnlDetail.equals("01"))
//              {
//                  result[1] = "01";
//              }
//              else if (cSaleChnlDetail.equals("02"))
//              {
//                  result[1] = "02";
//              }
//              else if (cSaleChnlDetail.equals("03"))
//              {
//                  result[1] = "02";
//              }
//              else if (cSaleChnlDetail.equals("04"))
//              {
//                  result[1] = "02";
//              }
//              else if (cSaleChnlDetail.equals("05"))
//              {
//                  result[1] = "03";
//              }
//              else if (cSaleChnlDetail.equals("06"))
//              {
//                  result[1] = "04";
//              }
              return result;
    }


//    /**
//     * 转换业务系统的销售渠道为销售系统的展业类型的函数 author: liujw
//     * @param cSalChnl 销售系统的展业类型(BranchType)
//     * @return String  业务系统的销售渠道
//     */
//    //xjh Add 2005/04/13
//    //展业类型到销售渠道的转化
//    public static String[] switchBranchTypetoSalChnl(String cBranchType,
//                                              String cBranchType2)
//    {
//        String[] result = new String[2];
//        //BranchType 1 个险,2 团险,3 银代
//        //SaleChnl 01 团险销售,02 个人销售,03 银行代理
//        if(cBranchType == null || cBranchType.equals(""))
//        {
//            result[0] = "01,02,03";
//        }
//        else if (cBranchType.equals("2"))
//        {
//            result[0] = "01";
//        }
//        else if (cBranchType.equals("1"))
//        {
//            result[0] = "02";
//        }
//        else if (cBranchType.equals("3"))
//        {
//            result[0] = "03";
//        }
//        //BranchType2 01 直销,02 中介,03 交叉,04 银代
//        //SaleChnlDetail 01 直销团队,02 经纪公司,03 代理公司,04 兼业代理公司,05 交叉销售，06 银行代理
//        if(cBranchType2 == null || cBranchType2.equals(""))
//        {
//            result[1] = "01,02,03,04,05,06";
//        }
//        else if (cBranchType2.equals("01"))
//        {
//            result[1] = "01";
//        }
//        else if (cBranchType2.equals("02"))
//        {
//            result[1] = "02,03,04";
//        }
//        else if (cBranchType2.equals("03"))
//        {
//            result[1] = "05";
//        }
//        else if (cBranchType2.equals("04"))
//        {
//            result[1] = "06";
//        }
//        return result;
//    }

//    //xjh Add 2005/04/14
//    //销售渠道到展业类型的转化
//    public static String[] switchSalChnl(String cSaleChnl,String cSaleChnlDetail)
//    {
//        String[] result = new String[2];
//        //BranchType 1 个险,2 团险,3 银代
//        //SaleChnl 01 团险销售,02 个人销售,03 银行代理
//        if(cSaleChnl == null || cSaleChnl.equals(""))
//        {
//            result[0] = "";
//        }
//        else if (cSaleChnl.equals("01"))
//        {
//            result[0] = "2";
//        }
//        else if (cSaleChnl.equals("02"))
//        {
//            result[0] = "1";
//        }
//        else if (cSaleChnl.equals("03"))
//        {
//            result[0] = "3";
//        }
//        //BranchType2 01 直销,02 中介,03 交叉,04 银代
//        //SaleChnlDetail 01 直销团队,02 经纪公司,03 代理公司,04 兼业代理公司,05 交叉销售，06 银行代理
//        if(cSaleChnlDetail == null || cSaleChnlDetail.equals(""))
//        {
//            result[1] = "";
//        }
//        else if (cSaleChnlDetail.equals("01"))
//        {
//            result[1] = "01";
//        }
//        else if (cSaleChnlDetail.equals("02"))
//        {
//            result[1] = "02";
//        }
//        else if (cSaleChnlDetail.equals("03"))
//        {
//            result[1] = "02";
//        }
//        else if (cSaleChnlDetail.equals("04"))
//        {
//            result[1] = "02";
//        }
//        else if (cSaleChnlDetail.equals("05"))
//        {
//            result[1] = "03";
//        }
//        else if (cSaleChnlDetail.equals("06"))
//        {
//            result[1] = "04";
//        }
//        return result;
//    }



    /**
     * 根据代理人职级查询代理人系列（只适用个险）
     * @param cAgentGrade 个险的代理人职级
     * @return String 职级对应的代理人系列
     */
    public static String getAgentSeries(String cAgentGrade)
    {
        if (cAgentGrade == null || cAgentGrade.equals(""))
            return null;
        String tAgentSeries = "";
        //xjh Modify 2005/3/22
//        String tSQL =
//                "select Trim(code2) from ldcodeRela where relaType = 'gradeserieslevel' "
//                + "and code1 = '" + cAgentGrade + "' ";
        String tSQL =
                "Select GradeProperty2 from LAAgentGrade where GradeCode = '" +
                cAgentGrade + "'";
        ExeSQL tExeSQL = new ExeSQL();
        tAgentSeries = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError())
            return null;
        return tAgentSeries;
    }


    /**
     * 根据代理人编码查询代理人入司职级
     * @param cAgentCode 代理人编码
     * @return String 代理人入司日期
     */
    public static String getAgentGrade(String cAgentCode)
    {
        if (cAgentCode == null || cAgentCode.equals(""))
            return null;
        String tAgentGrade = "";
        String tSQL = "select agentgrade from latreeb where agentcode='" +
                      cAgentCode + "' order by makedate,maketime";
        ExeSQL tExeSQL = new ExeSQL();
        tAgentGrade = tExeSQL.getOneValue(tSQL);
        if (tAgentGrade == null || tAgentGrade.equals(""))
        {
            String aSQL = "select agentgrade from latree where agentcode='" +
                          cAgentCode + "' ";
            ExeSQL aExeSQL = new ExeSQL();
            tAgentGrade = aExeSQL.getOneValue(aSQL);
        }
        return tAgentGrade;
    }

    public static String AdjustCommCheck(String tAgentCode, String tStartDate)
    {
        if (!tStartDate.endsWith("01")) //如果开始时间不是从1号开始，则返回错误
            return "调整日期必须是从一号开始";
        String sql = "select max(indexcalno) from lawage where AgentCode='" +
                     tAgentCode + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String maxIndexCalNo = tExeSQL.getOneValue(sql);
        if (maxIndexCalNo != null && !maxIndexCalNo.equals(""))
        {
            String lastDate = PubFun.calDate(tStartDate, -1, "M", null);
            lastDate = AgentPubFun.formatDate(lastDate, "yyyyMM");
            if (maxIndexCalNo.trim().compareTo(lastDate.trim()) > 0)
            {
                return ("上次发工资是" + maxIndexCalNo.substring(0, 4) + "年" +
                        maxIndexCalNo.substring(4).trim() +
                        "月，因次调整日期必须从这个月的下一个月1号");
            }
        }
        return "00";

    }


    /**
     * 查询代理人所在组的外部编码
     * @param cAgentGrade 代理人代码
     * @return String 代理人所在组的外部编码
     */
    public static String getAgentBranchAttr(String cAgentCode)
    {
        String tBranchAttr = "";
        if (cAgentCode == null || cAgentCode.equals(""))
            return null;
        String tSQL = "Select Trim(BranchAttr) From LABranchGroup Where AgentGroup = ("
                + "Select BranchCode From LAAgent Where AgentCode = '" +
                cAgentCode +
                "') ";
        ExeSQL tExeSQL = new ExeSQL();
        tBranchAttr = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError())
            return null;
        return tBranchAttr;
    }


    /**
     * 按cFormat的格式 格式化日期类型
     * 年月日的表示字母为 y M d
     */
    public static String formatDate(String cDate, String cFormat)
    {
        String FormatDate = "";
        Date tDate;
        SimpleDateFormat sfd = new SimpleDateFormat(cFormat);
        FDate fDate = new FDate();
        tDate = fDate.getDate(cDate.trim());
        FormatDate = sfd.format(tDate);
//       System.out.println("[--formatedate--]:"+FormatDate);
        return FormatDate;
    }


//    /**
//     * 从LDCodeRela表中查出ManageCom对应的地区类型
//     */
//    public static String getAreaType(String cManageCom)
//    {
//        //AreaType
//        String tSql = "Select trim(code2) From LDCodeRela Where RelaType = 'comtoareatype' and trim(code1) = '" +
//                      cManageCom + "'    ";
//        ExeSQL tExeSQL = new ExeSQL();
//        String tAreaType = tExeSQL.getOneValue(tSql);
//        if (tExeSQL.mErrors.needDealError())
//            return null;
//        if (tAreaType == null || tAreaType.equals(""))
//            return null;
//        return tAreaType;
//    }
    /**
     * 团险机构地区类别
     * 从LDCodeRela表中查出cObject对应的地区类型
     * 团险cObject=AgentGroup
     */
      public static String getAreaType(String cObject)
      {
          //AreaType
          String tAreaType = "";
          String tcomtoareatype =  "comtoareatype2";
          String tSql =
                  "select BranchStyle,ComStyle,managecom  from labranchgroup where agentgroup='" +
                  cObject + "' ";
          ExeSQL tExeSQL = new ExeSQL();
          SSRS tSSRS = new SSRS();
          tSSRS = tExeSQL.execSQL(tSql);
          System.out.println("AAA"+tSql);
          if (tExeSQL.mErrors.needDealError())
              return null;
          if (tSSRS.getMaxRow() == 1) {
              String tBranchStyle = tSSRS.GetText(1, 1);
              String tComStyle = tSSRS.GetText(1, 2);
              String tManageCom = tSSRS.GetText(1, 3);
              //二级机构营业部或三级机构营业部
              if (tBranchStyle.equals("01")  ||tBranchStyle.equals("")) {
                 //先计算支公司地区类型
//                  tSql =
//                          "Select trim(code2) From LDCodeRela Where RelaType = '" +
//                          tcomtoareatype + "' and trim(code1) = '" +
//                          tManageCom + "'";
//                  tAreaType = tExeSQL.getOneValue(tSql);
//                  if (tExeSQL.mErrors.needDealError())
//                      return null;
                  //支公司地区类型没有录入，则计算分公司类型，按分公司计算（不需要了）
                  if(tAreaType.equals(""))
                  {
                      tManageCom = tManageCom.substring(0, 4);
                      tSql =
                              "Select trim(code2) From LDCodeRela Where RelaType = '" +
                              tcomtoareatype + "' and trim(code1) = '" +
                              tManageCom + "'";
                      tAreaType = tExeSQL.getOneValue(tSql);
                      if (tExeSQL.mErrors.needDealError())
                          return null;
                  }
              } else if (tBranchStyle.equals("02") ){
                  //B类（或C类）三级机构营业部
                  tAreaType = tComStyle;
              }
          }
          if (tAreaType == null || tAreaType.equals(""))
              return null;
          return tAreaType;
      }

      /**
          * 从LDCodeRela表中查出ManageCom对应的地区类型


          */
       public static String getAreaType(String cManageCom,String cType)
       {
              //AreaType

              String tcomtoareatype="";
              if ("01".equals(cType))   tcomtoareatype="comtoareatype1";
              if ("02".equals(cType))   tcomtoareatype="comtoareatype2";
              if ("03".equals(cType))   tcomtoareatype="comtoareatype3";
              String tSql = "Select trim(code2) From LDCodeRela Where RelaType = '"+tcomtoareatype+"' and trim(code1) = '" +
                           cManageCom + "'";
              ExeSQL tExeSQL = new ExeSQL();
              String tAreaType = tExeSQL.getOneValue(tSql);
              if (tExeSQL.mErrors.needDealError())
                  return null;

              if (tAreaType == null || tAreaType.equals(""))
                  return null;
              return tAreaType;
      }
    /**
         * 计算本月所在季度的开始时间
         * @Return :格式为YYYY-MM-DD
         * cDate 为YYYYMM
         */
        public static String getQuarterStart(String cDate)
        {
            String tStartDate = "";
            ExeSQL tExeSQL = new ExeSQL();
            String tSql ="select startdate from LAStatSegment  where ("+cDate+"+1=yearmonth or "
            +" "+cDate+"+2=yearmonth or "+cDate+"=yearmonth) and stattype='2' ";
            tStartDate = tExeSQL.getOneValue(tSql);
            if (tExeSQL.mErrors.needDealError())
                return null;
            if (tStartDate == null || tStartDate.equals(""))
                return null;
            return tStartDate;
        }
        /*
        * 计算本月所在季度的开始时间
        * @Return :格式为YYYY-MM-DD
        * cDate 为YYYYMM
        */
       public static String getQuarterEnd(String cDate)
       {
           String tEndDate = "";
           ExeSQL tExeSQL = new ExeSQL();
           String tSql ="select Enddate from LAStatSegment  where ("+cDate+"+1=yearmonth or "
           +" "+cDate+"+2=yearmonth or "+cDate+"=yearmonth) and stattype='2' ";
           tEndDate = tExeSQL.getOneValue(tSql);
           if (tExeSQL.mErrors.needDealError())
               return null;
           if (tEndDate == null || tEndDate.equals(""))
               return null;
           return tEndDate;
        }
    /**
     * 转换规则：上月26号---本月25号算作本月
     * @Return :格式为YYYYMM
     */
    public static String ConverttoYM(String cDate)
    {
        String sYearMonth = "";
        ExeSQL tExeSQL = new ExeSQL();
        String tSql =
                "Select YearMonth From LAStatSegment Where StatType = '5' "
                + "And StartDate <= '" + cDate + "' And EndDate >= '" + cDate +
                "'";
        sYearMonth = tExeSQL.getOneValue(tSql);
        if (tExeSQL.mErrors.needDealError())
            return null;
        if (sYearMonth == null || sYearMonth.equals(""))
            return null;
        return sYearMonth;
    }

    /**
     * getManagecom
     * 从LABranchGroup 表中由AgentGroup取得其管理机构
     * @param AgentGroup String
     * @return String
     */
    public static String getManagecom(String cAgentGroup)
    {
        if (cAgentGroup == null || cAgentGroup.equals(""))
            return null;
        String tSQL =
                "Select Trim(Managecom) From LABranchGroup Where AgentGroup = '" +
                cAgentGroup + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String tManagecom = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError())
            return null;
        return tManagecom;
    }


    /**
     * 转换规则：上月26号---本月25号算作本月
     * @Return :格式为YYYYMMDD
     */
    public static String ConverttoYMD(String cDate)
    {
        String sYearMonth = "";
        sYearMonth = ConverttoYM(cDate);
        if (sYearMonth != null && !sYearMonth.equals(""))
        {
            sYearMonth = sYearMonth.substring(0, 4) + "-" +
                         sYearMonth.substring(4) +
                         "-01";
        }
        return sYearMonth;
    }
    /**
        * 得到代理人某一个月的职级
        * @param cAgentCode 人员代码
        * @param cIndexCalNo 年月
        * @return String agentgrade
        * 如果职级缓存表为空，则没有做过职级变动
     */
    public static String getMonthAgentGrade(String cAgentCode,String cIndexCalNo)
    {

//        String tSQL = "select agentgrade from latreetemp  where  agentcode='"+cAgentCode
//                          +"'  and cvalimonth<='"+cIndexCalNo+"' and cvaliflag='1' "
//                         +" order by cvalimonth  desc  fetch first 1 rows only"; //取最近一次有效变更
//        ExeSQL tExeSQL=new ExeSQL();
//        String tAgentGrade=tExeSQL.getOneValue(tSQL);
//        if (tAgentGrade==null || tAgentGrade.equals(""))
//        {
//            tSQL="select agentgrade from latree where agentcode='"+cAgentCode
//                 +"'  and date_format(startdate,'yyyymm')<='"+cIndexCalNo+"' ";
//            tExeSQL=new ExeSQL();
//            tAgentGrade=tExeSQL.getOneValue(tSQL);
//            if (tAgentGrade==null || tAgentGrade.equals(""))
//            {
//                tSQL = "select agentlastgrade from latree where agentcode='" +
//                        cAgentCode
//                        + "'  and date_format(oldstartdate,'yyyymm')<='" +
//                        cIndexCalNo
//                        + "'  and date_format(oldenddate,'yyyymm')>='" +
//                        cIndexCalNo + "' ";
//                tExeSQL = new ExeSQL();
//                tAgentGrade = tExeSQL.getOneValue(tSQL);
//
//                if (tAgentGrade == null || tAgentGrade.equals("")) {
//                    tSQL =
//                            "select agentlastgrade from latreeb where agentcode='" +
//                            cAgentCode
//                            + "'  and date_format(oldstartdate,'yyyymm')<='" +
//                            cIndexCalNo
//                            + "'  and date_format(oldenddate,'yyyymm')>='" +
//                            cIndexCalNo
//                            + "'  and removetype in ('31','01')  order by makedate desc,maketime desc order by fetch first 1 rows only ";
//                    tExeSQL = new ExeSQL();
//                    tAgentGrade = tExeSQL.getOneValue(tSQL);
//                }
//            }
//        }


        String tSQL="select agentgrade from latree where agentcode='"+cAgentCode
             +"'  and date_format(startdate,'yyyymm')<='"+cIndexCalNo+"' ";
        ExeSQL tExeSQL=new ExeSQL();
        String tAgentGrade=tExeSQL.getOneValue(tSQL);
        if (tAgentGrade==null || tAgentGrade.equals(""))
        {
            tSQL = "select agentlastgrade from latree where agentcode='" +
                    cAgentCode
                    + "'  and date_format(oldstartdate,'yyyymm')<='" +
                    cIndexCalNo
                    + "'  and date_format(oldenddate,'yyyymm')>='" +
                    cIndexCalNo + "' ";
            tExeSQL = new ExeSQL();
            tAgentGrade = tExeSQL.getOneValue(tSQL);

            if (tAgentGrade == null || tAgentGrade.equals("")) {
                tSQL =
                        "select agentlastgrade from latreeb where agentcode='" +
                        cAgentCode
                        + "'  and date_format(oldstartdate,'yyyymm')<='" +
                        cIndexCalNo
                        + "'  and date_format(oldenddate,'yyyymm')>='" +
                        cIndexCalNo
                        + "'  and removetype in ('31','01','06')  order by makedate desc,maketime desc  fetch first 1 rows only ";
                tExeSQL = new ExeSQL();
                tAgentGrade = tExeSQL.getOneValue(tSQL);
            }
            }
        return tAgentGrade;
    }
    /**
       * 得到代理人某一个月的职级
       * @param cAgentCode 人员代码
       * @param cIndexCalNo 年月
       * @return String agentgrade
       * 如果职级缓存表为空，则没有做过职级变动
    */
   public static String getMonthAgentGradeBK(String cAgentCode,String cIndexCalNo)
   {

       String tSQL="select agentgrade from latree where agentcode='"+cAgentCode
            +"'  and date_format(startdate,'yyyymm')<='"+cIndexCalNo+"' ";
       ExeSQL tExeSQL=new ExeSQL();
       String tAgentGrade=tExeSQL.getOneValue(tSQL);
       if (tAgentGrade==null || tAgentGrade.equals(""))
       {
           tSQL = "select agentlastgrade from latree where agentcode='" +
                   cAgentCode
                   + "'  and date_format(oldstartdate,'yyyymm')<='" +
                   cIndexCalNo
                   + "'  and date_format(oldenddate,'yyyymm')>='" +
                   cIndexCalNo + "' ";
           tExeSQL = new ExeSQL();
           tAgentGrade = tExeSQL.getOneValue(tSQL);

           if (tAgentGrade == null || tAgentGrade.equals("")) {
               tSQL =
                       "select agentlastgrade from latreeb where agentcode='" +
                       cAgentCode
                       + "'  and date_format(oldstartdate,'yyyymm')<='" +
                       cIndexCalNo
                       + "'  and date_format(oldenddate,'yyyymm')>='" +
                       cIndexCalNo
                       + "'  and removetype in ('06','01')  order by edorno desc,makedate desc,maketime desc  fetch first 1 rows only ";
               tExeSQL = new ExeSQL();
               tAgentGrade = tExeSQL.getOneValue(tSQL);
           }
           }
       return tAgentGrade;
   }

    /**
        * 得到代理人某一个月的agentgroup
        * @param cAgentCode 人员代码
        * @param cIndexCalNo 年月
        * @return String agentgrade
        * 如果团队缓存表为空，则没有做过团队变动变动
     */
   public static String getMonthAgentGroup(String cAgentCode,String cIndexCalNo)
   {
       String tAgentGroup="";
       String tSQL="select AgentGroup  from  labranchchangetemp where  agentcode='"+cAgentCode
                   +"' and cvalimonth<='"+cIndexCalNo+"'   and cvaliflag='1' "
                   +" order by cvalimonth  desc  fetch first 1 rows only"; //取最近一次有效变更
       ExeSQL tExeSQL=new ExeSQL();
       tAgentGroup=tExeSQL.getOneValue(tSQL);
       //没有做过变更
       if(tAgentGroup==null ||   tAgentGroup.equals(""))
       {
           tSQL="select agentgroup from  latree where agentcode='"+cAgentCode+"'";
           tExeSQL=new ExeSQL();
           tAgentGroup=tExeSQL.getOneValue(tSQL);
       }

       return tAgentGroup;
   }
   /**
     * 得到代理人某一个月的Branchattr
     * @param cAgentCode 人员代码
     * @param cIndexCalNo 年月
     * @return String agentgrade
     * 如果团队缓存表为空，则没有做过团队变动变动
  */
   public static String getMonthBranchAttr(String cAgentGroup)
   {
       String tBranchAttr = "";

       String tSQL ="Select branchattr From LABranchGroup Where AgentGroup = '" +
                      cAgentGroup + "'";
       ExeSQL tExeSQL = new ExeSQL();
       tBranchAttr = tExeSQL.getOneValue(tSQL);

       return tBranchAttr;
   }
   public static String getMonthBranchSeries(String cAgentGroup)
  {
      String tBranchSeries = "";

      String tSQL ="Select BranchSeries From LABranchGroup Where AgentGroup = '" +
                     cAgentGroup + "'";
      ExeSQL tExeSQL = new ExeSQL();
      tBranchSeries = tExeSQL.getOneValue(tSQL);

      return tBranchSeries;
  }

    /**
     * 查询机构系列号
     * @param cAgentGroup 机构内部编码
     * @return String 机构系列号
     */
    public static String getBranchSeries(String cAgentGroup)
    {
        String tBranchSeries = cAgentGroup;
        if (cAgentGroup == null || cAgentGroup.equals(""))
            return "";
        String tSQL =
                "Select Trim(UpBranch) From LABranchGroup Where AgentGroup = '" +
                cAgentGroup + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String tUpBranch = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError())
            return "";
        if (tUpBranch == null || tUpBranch.compareTo("") == 0)
        {
            System.out.println("到达出口");
        }
        else
        {
            //如果发现该机构存在上级机构，则查询上级机构信息
            //xjh Modify ， 2005/02/17，机构间使用“：”分隔符
//            tBranchSeries = getBranchSeries(tUpBranch) + tBranchSeries;
            tBranchSeries = getBranchSeries(tUpBranch) + ":" + tBranchSeries;

//          System.out.println("机构序列" + tBranchSeries);
        }
        return tBranchSeries;
    }

    /**
   * 自动生成销售机构外部号BranchAttr
   * @param cUpBranchAttr 上级机构的外部编码
   *        cBranchLevel 被生成机构的级别
   *        cBranchType  渠道
   *        cBranchType2 渠道类型2
   * @return String 机构系列号
   */
  public static String CreateBranchAttr(String cUpBranchAttr,
                                        String cBranchLevel,
                                        String cBranchType,
                                        String cBranchType2)
  {
      System.out.println("CreateBranchAttr...");
      LABranchLevelDB tLABranchLevelDB = new LABranchLevelDB();
      tLABranchLevelDB.setBranchLevelCode(cBranchLevel);
      tLABranchLevelDB.setBranchType(cBranchType);
      tLABranchLevelDB.setBranchType2(cBranchType2);
      if (!tLABranchLevelDB.getInfo())
      {
          return null;
      }
      LABranchLevelSchema tLABranchLevelSchema = new LABranchLevelSchema();
      tLABranchLevelSchema = tLABranchLevelDB.getSchema();

      String tmaxBranchAttr = "";
      ExeSQL tExeSQL = new ExeSQL();
      String tmaxBranchAttr1 = "";
      String tmaxBranchAttr2 = "";
      String sql = "";

      sql = "select char(max(bigint(branchattr))) from labranchgroup where BranchType='" +
            cBranchType + "' and BranchType2='"+cBranchType2+"' and  BranchLevel = '" +
            cBranchLevel + "' "
            +
            (cUpBranchAttr.equals("") ? "" :
             " and branchattr like '" + cUpBranchAttr + "%' ");
      tmaxBranchAttr1 = tExeSQL.getOneValue(sql);
      if (tmaxBranchAttr1 == null || tmaxBranchAttr1.equals(""))
      {
          tmaxBranchAttr1 = cUpBranchAttr+
                            "000000000000000".substring(0,
                  Integer.parseInt(tLABranchLevelSchema.
                                   getBranchLevelProperty2()) -
                                   cUpBranchAttr.length());
      }
      tmaxBranchAttr1=tmaxBranchAttr1.substring( cUpBranchAttr.length());
      BigInteger tBigInteger = new BigInteger("10" + tmaxBranchAttr1);
      System.out.println("tmaxBranchAttr1: "+tmaxBranchAttr1 +" "+ tBigInteger);

      sql = "select char(max(bigint(branchattr))) from labranchgroupb where BranchType='" +
            cBranchType + "' and BranchType2='" + cBranchType2 +
            "' and  BranchLevel = '" + cBranchLevel + "'"
            +
            (cUpBranchAttr.equals("") ? "" :
             " and branchattr like '" + cUpBranchAttr + "%'  ");
      tmaxBranchAttr2 = tExeSQL.getOneValue(sql);
      if (tmaxBranchAttr2 == null || tmaxBranchAttr2.equals(""))
      {
          tmaxBranchAttr2 = cUpBranchAttr+
                            "000000000000000".substring(0,
                  Integer.parseInt(tLABranchLevelSchema.
                                   getBranchLevelProperty2()) -
                                   cUpBranchAttr.length());
      }
      tmaxBranchAttr2=tmaxBranchAttr2.substring(cUpBranchAttr.length());
      BigInteger tBigInteger1 = new BigInteger("10" + tmaxBranchAttr2);
      System.out.println("tmaxBranchAttr2: "+tmaxBranchAttr2 +" "+ tBigInteger1);
      
      
      
      BigInteger tBig = new BigInteger("1100");
      if (tBigInteger1.compareTo(tBigInteger) >= 0)
      {
          BigInteger tAdd = new BigInteger("1");
          tBigInteger1 = tBigInteger1.add(tAdd);
          
          if(tBigInteger1.compareTo(tBig)==0){
        	  tmaxBranchAttr = tBigInteger1.toString().substring(1);  
          }
          else
          tmaxBranchAttr = tBigInteger1.toString().substring(2);
      }
      else
      {
          BigInteger tAdd = new BigInteger("1");
          tBigInteger = tBigInteger.add(tAdd);
          if(tBigInteger.compareTo(tBig)==0){
        	  tmaxBranchAttr = tBigInteger.toString().substring(1);  
          }
          else
          tmaxBranchAttr = tBigInteger.toString().substring(2);
      }
      return cUpBranchAttr +tmaxBranchAttr;
  }
  public static void main(String args[])
  {
      String Agentcode = "000000000473";
      System.out.println(getBranchSeries(Agentcode));
      String tSQL =
              "Select GradeProperty2 from LAAgentGrade where GradeCode = 'a'";
      ExeSQL tExeSQL = new ExeSQL();
      String tAreaType=getAreaType("000000001776");
      String tmanx=CreateBranchAttr("863503000101","01","1","01");
      System.out.println(tmanx);
  }

  /**
       * 自动生成销售机构外部号BranchAttr
       * @param cUpBranchAttr 上级机构的外部编码
       *        cBranchLevel 被生成机构的级别
       *        cBranchType  渠道
       *        cBranchType2 渠道类型2
       * @return String 机构系列号
       */
      public static String getMaxBranchAttr(String cUpBranchAttr,
                                            String cMaxBranchAttr)
      {
          System.out.println("CreateBranchAttr...");

          int iLength = cUpBranchAttr.length();
          String tMaxBranchAttr = cMaxBranchAttr.substring(iLength);

          BigInteger tBigInteger = new BigInteger("1" + tMaxBranchAttr);

          BigInteger tAdd = new BigInteger("1");
          tBigInteger = tBigInteger.add(tAdd);
          tMaxBranchAttr = cUpBranchAttr + tBigInteger.toString().substring(1);

          return tMaxBranchAttr;
      }

      /**
       * 查询考核临界日期
       * @return String
       */
      public static String getAssessDay()
     {
         String sql = "";
         String assessDay = "";
         ExeSQL tExeSQL = new ExeSQL();
         sql = "select code2 from ldcoderela where 1=1 and relatype='assessday'";
         try
         {
             assessDay = tExeSQL.getOneValue(sql);
         }
         catch (Exception ex)
         {
             ex.printStackTrace();
         }
         return assessDay;
     }

     /**
      * 取得代理人当前职级的起聘日期
      */
     public static String getPostBeginDate(String cAgentCode)
     {
         String tStartDate = "";
         LATreeDB tLATreeDB = new LATreeDB();
         tLATreeDB.setAgentCode(cAgentCode);
         if (!tLATreeDB.getInfo())
         {
             return "";
         }
         tStartDate = tLATreeDB.getStartDate().trim();
         if (tStartDate == null || tStartDate.equals(""))
         {
             return "";
         }
         return tStartDate;
     }
}
