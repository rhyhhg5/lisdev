/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;

import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.db.LMRiskPayDB;
import com.sinosoft.lis.message.Response;
import com.sinosoft.lis.message.SMSClient;
import com.sinosoft.lis.message.SmsMessage;
import com.sinosoft.lis.message.SmsMessageNew;
import com.sinosoft.lis.message.SmsMessages;
import com.sinosoft.lis.message.SmsMessagesNew;
import com.sinosoft.lis.message.SmsServiceServiceLocator;
import com.sinosoft.lis.message.SmsServiceSoapBindingStub;
import com.sinosoft.lis.vschema.LMRiskPaySet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:业务系统的公共业务处理函数
 * 该类包含所有业务处理中的公共函数，和以前系统中的funpub.4gl
 * 文件相对应。在这个类中，所有的函数都采用Static的类型，所有需要的数据都是
 * 通过参数传入的，在本类中不采用通过属性传递数据的方法。 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author YT
 * @version 1.0
 */
public class PubFun
{
    private final static int MAX_YEAR = 9999;

    private final static int MIN_YEAR = 1800;

    private final static int MAX_MONTH = 12;

    private final static int MIN_MONTH = 1;

    private final static int MAX_DAY = 31;

    private final static int MIN_DAY = 1;

    private final static String DATE_LIST = "0123456789-";

    private final static String[] IDNOPROVINCES =  {"11","12","13","14","15",
		"21","22","23","31","32",
		"33","34","35","36","37",
		"41","42","43","44","45",
		"46","50","51","52","53",
		"54","61","62","63","64",
		"65","71","81","82"};
    
    public PubFun()
    {
    }
    
    /** 支持期缴月交paytodate的计算
     * 20170511原逻辑有问题，使用lis其他版本的calOFDate()的部分逻辑
	 * 仅参考此版本中计算单位为M的逻辑
	 */
    public static Date calOFDate(Date baseDate, int interval, String unit, Date compareDate)
    {
        Date returnDate = null;

        GregorianCalendar mCalendar = new GregorianCalendar();
        mCalendar.setTime(baseDate);
        if (unit.equals("Y"))
        {
            mCalendar.add(Calendar.YEAR, interval);
        }
        if (unit.equals("M"))
        {
            mCalendar.add(Calendar.MONTH, interval);
        }
        if (unit.equals("D"))
        {
            mCalendar.add(Calendar.DATE, interval);
        }
        if (compareDate != null)
        {
            GregorianCalendar cCalendar = new GregorianCalendar();
            cCalendar.setTime(compareDate);

            int mYears = mCalendar.get(Calendar.YEAR);
            int cMonths = cCalendar.get(Calendar.MONTH);
            int cDays = cCalendar.get(Calendar.DATE);

            if (unit.equals("Y"))
            {
                cCalendar.set(mYears, cMonths, cDays);
                if (cCalendar.before(mCalendar))
                {
                    mCalendar.set(mYears + 1, cMonths, cDays);
                    returnDate = mCalendar.getTime();
                }
                else
                {
                    returnDate = cCalendar.getTime();
                }
            }
            if (unit.equals("M"))
            {
            	/* 20170511原逻辑有问题，使用lis其他版本的calOFDate()的部分逻辑
	        	 * 仅参考此版本中计算单位为M的逻辑
	        	 * 日取原日期中的日與目標月的最大值之間的較小者，換言之不會超過目標月的最後一天
	        	 * 例如保單生效日是2007-01-30日，第一個交費後的paytodate為2008-02-28，第二次計算
	        	 * 第二次計算paytodate時
	        	 * 中間的目標日期為2008-02-28+1個月=2008-03-28，3月的最後一天為31，而保單生效日為30，
	        	 * 故此時的paytodate為2008-30-30。
	        	 */
            	if (mCalendar.get(Calendar.DATE) < cCalendar.get(Calendar.DATE)) {
		              mCalendar.set(Calendar.DATE,mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH) > cCalendar
		                               .get(Calendar.DATE) ? cCalendar.get(Calendar.DATE): mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		           }
		           returnDate = mCalendar.getTime();
            }
            if (unit.equals("D"))
            {
                returnDate = mCalendar.getTime();
            }
        }
        else
        {
            returnDate = mCalendar.getTime();
        }

        return returnDate;
    }
    
    /**
     * 支持期缴月交paytodate的计算
     * @param baseDate String
     * @param interval int
     * @param unit String
     * @param compareDate String
     * @return String
     */
    public static String calOFDate(String baseDate, int interval, String unit, String compareDate)
    {
        try
        {
            FDate tFDate = new FDate();
            Date bDate = tFDate.getDate(baseDate);
            Date cDate = tFDate.getDate(compareDate);
            return tFDate.getString(calOFDate(bDate, interval, unit, cDate));
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * 计算日期的函数 author: HST
     * 参照日期指当按照年月进行日期的计算的时候，参考的日期，如下例，结果返回2002-03-31
     * <p><b>Example: </b><p>
     * <p>FDate tD=new FDate();<p>
     * <p>Date baseDate =new Date();<p>
     * <p>baseDate=tD.getDate("2000-02-29");<p>
     * <p>Date comDate =new Date();<p>
     * <p>comDate=tD.getDate("1999-12-31");<p>
     * <p>int inteval=1;<p>
     * <p>String tUnit="M";<p>
     * <p>Date tDate =new Date();<p>
     * <p>tDate=PubFun.calDate(baseDate,inteval,tUnit,comDate);<p>
     * <p>System.out.println(tDate.toString());<p>
     * @param baseDate 起始日期
     * @param interval 时间间隔
     * @param unit 时间间隔单位
     * @param compareDate 参照日期
     * @return Date类型变量
     */
    public static Date calDate(Date baseDate, int interval, String unit, Date compareDate)
    {
        Date returnDate = null;

        GregorianCalendar mCalendar = new GregorianCalendar();
        mCalendar.setTime(baseDate);
        if (unit.equals("Y"))
        {
            mCalendar.add(Calendar.YEAR, interval);
        }
        if (unit.equals("M"))
        {
            mCalendar.add(Calendar.MONTH, interval);
        }
        if (unit.equals("D"))
        {
            mCalendar.add(Calendar.DATE, interval);
        }

        if (compareDate != null)
        {
            GregorianCalendar cCalendar = new GregorianCalendar();
            cCalendar.setTime(compareDate);

            int mYears = mCalendar.get(Calendar.YEAR);
            int mMonths = mCalendar.get(Calendar.MONTH);
            int cMonths = cCalendar.get(Calendar.MONTH);
            int cDays = cCalendar.get(Calendar.DATE);

            if (unit.equals("Y"))
            {
                cCalendar.set(mYears, cMonths, cDays);
                if (cCalendar.before(mCalendar))
                {
                    mCalendar.set(mYears + 1, cMonths, cDays);
                    returnDate = mCalendar.getTime();
                }
                else
                {
                    returnDate = cCalendar.getTime();
                }
            }
            if (unit.equals("M"))
            {
                cCalendar.set(mYears, mMonths, cDays);
                if (cCalendar.before(mCalendar))
                {
                    mCalendar.set(mYears, mMonths + 1, cDays);
                    returnDate = mCalendar.getTime();
                }
                else
                {
                    returnDate = cCalendar.getTime();
                }
            }
            if (unit.equals("D"))
            {
                returnDate = mCalendar.getTime();
            }
        }
        else
        {
            returnDate = mCalendar.getTime();
        }

        return returnDate;
    }

    /**
     * 重载计算日期，参数见楼上，add by Minim
     * @param baseDate String
     * @param interval int
     * @param unit String
     * @param compareDate String
     * @return String
     */
    public static String calDate(String baseDate, int interval, String unit, String compareDate)
    {
        try
        {
            FDate tFDate = new FDate();
            Date bDate = tFDate.getDate(baseDate);
            Date cDate = tFDate.getDate(compareDate);
            return tFDate.getString(calDate(bDate, interval, unit, cDate));
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * 获取传入日期所属的标准交费期间，返回EdorValiDate所在交费期间的起期和止期,如果是非整期保单,止期比EndDate大; CValiDate<=EdorValiDate<EndDate
     * @param CValiDate String 团体保单生效日期
     * @param EndDate String 团体保单终止日期
     * @param EdorValiDate String 个人生效日期   
     * @param PayIntv int 团体保单交费间隔 0,1,3,6         
     * @return String[]
     */
    public static String[] getIntervalDate(String CValiDate, String EndDate, String EdorValiDate, int PayIntv)
    {
        String MonDate[] = new String[2];
        FDate tD = new FDate();
        //PayIntv = 0 趸交
        if (tD.getDate(EndDate).compareTo(tD.getDate(CValiDate)) > 0
                && tD.getDate(EdorValiDate).compareTo(tD.getDate(CValiDate)) >= 0
                && tD.getDate(EndDate).compareTo(tD.getDate(EdorValiDate)) > 0 && PayIntv == 0)
        {
            Date tCValiDate = tD.getDate(CValiDate);
            MonDate[0] = tD.getString(tCValiDate);
            Date tEndDate = tD.getDate(EndDate);
            MonDate[1] = tD.getString(tEndDate);
        }
        //PayIntv > 0 (1,3,6)期交
        else if (tD.getDate(EndDate).compareTo(tD.getDate(CValiDate)) > 0
                && tD.getDate(EdorValiDate).compareTo(tD.getDate(CValiDate)) >= 0
                && tD.getDate(EndDate).compareTo(tD.getDate(EdorValiDate)) > 0 && PayIntv > 0)
        {
            int monthUnit = PubFun.calInterval2(CValiDate, EndDate, "M");//算出两个日期之间月份间隔
            int num = monthUnit / PayIntv;
            if (Arith.div(monthUnit, PayIntv) > num)
            {
                num += 1;
            }
            Date newBaseDate = tD.getDate(CValiDate);
            MonDate[0] = tD.getString(newBaseDate);
            for (int i = 1; i <= num; i++)
            {
                Date PayToDate = PubFun.calDate(newBaseDate, PayIntv * i, "M", null);
                if (PayToDate.compareTo(tD.getDate(EdorValiDate)) > 0)
                {
                    MonDate[1] = tD.getString(PayToDate);
                    break;
                }
                else
                {
                    MonDate[0] = tD.getString(PayToDate);
                }
            }
        }
        return MonDate;
    }

    /**
     * 通过起始日期和终止日期计算以时间间隔单位为计量标准的时间间隔 author: HST
     * <p><b>Example: </b><p>
     * <p>参照calInterval(String  cstartDate, String  cendDate, String unit)，前两个变量改为日期型即可<p>
     * @param startDate 起始日期，Date变量
     * @param endDate 终止日期，Date变量
     * @param unit 时间间隔单位，可用值("Y"--年 "M"--月 "D"--日)
     * @return 时间间隔,整形变量int
     */
    public static int calInterval(Date startDate, Date endDate, String unit)
    {
        int interval = 0;

        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        int sYears = sCalendar.get(Calendar.YEAR);
        int sMonths = sCalendar.get(Calendar.MONTH);
        int sDays = sCalendar.get(Calendar.DAY_OF_MONTH);
        int sDaysOfYear = sCalendar.get(Calendar.DAY_OF_YEAR);

        GregorianCalendar eCalendar = new GregorianCalendar();
        eCalendar.setTime(endDate);
        int eYears = eCalendar.get(Calendar.YEAR);
        int eMonths = eCalendar.get(Calendar.MONTH);
        int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
        int eDaysOfYear = eCalendar.get(Calendar.DAY_OF_YEAR);

        if (unit.equals("Y"))
        {
            interval = eYears - sYears;
            if (eMonths < sMonths)
            {
                interval--;
            }
            else
            {
                if (eMonths == sMonths && eDays < sDays)
                {
                    interval--;
                    if (eMonths == 1)
                    { //如果同是2月，校验润年问题
                        if ((sYears % 4) == 0 && (eYears % 4) != 0)
                        { //如果起始年是润年，终止年不是润年
                            if (eDays == 28)
                            { //如果终止年不是润年，且2月的最后一天28日，那么补一
                                interval++;
                            }
                        }
                    }
                }
            }
        }
        if (unit.equals("M"))
        {
            interval = eYears - sYears;
            interval = interval * 12;

            interval = eMonths - sMonths + interval;
            if (eDays < sDays)
            {
                interval--;
                //eDays如果是月末，则认为是满一个月
                int maxDate = eCalendar.getActualMaximum(Calendar.DATE);
                if (eDays == maxDate)
                {
                    interval++;
                }
            }
        }
        if (unit.equals("D"))
        {
            interval = eYears - sYears;
            interval = interval * 365;
            interval = eDaysOfYear - sDaysOfYear + interval;

            // 处理润年
            int n = 0;
            eYears--;
            if (eYears > sYears)
            {
                int i = sYears % 4;
                if (i == 0)
                {
                    sYears++;
                    n++;
                }
                int j = (eYears) % 4;
                if (j == 0)
                {
                    eYears--;
                    n++;
                }
                n += (eYears - sYears) / 4;
            }
            if (eYears == sYears)
            {
                int i = sYears % 4;
                if (i == 0)
                {
                    n++;
                }
            }
            interval += n;
        }
        return interval;
    }

    /**
     * 通过起始日期和终止日期计算以时间间隔单位为计量标准的时间间隔，舍弃法 author: HST
     * 起始日期，(String,格式："YYYY-MM-DD")
     * @param cstartDate String
     * 终止日期，(String,格式："YYYY-MM-DD")
     * @param cendDate String
     * 时间间隔单位，可用值("Y"--年 "M"--月 "D"--日)
     * @param unit String
     * 时间间隔,整形变量int
     * @return int
     */
    public static int calInterval(String cstartDate, String cendDate, String unit)
    {
        FDate fDate = new FDate();
        Date startDate = fDate.getDate(cstartDate);
        Date endDate = fDate.getDate(cendDate);
        if (fDate.mErrors.needDealError())
        {
            return 0;
        }

        int interval = 0;

        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        int sYears = sCalendar.get(Calendar.YEAR);
        int sMonths = sCalendar.get(Calendar.MONTH);
        int sDays = sCalendar.get(Calendar.DAY_OF_MONTH);
        int sDaysOfYear = sCalendar.get(Calendar.DAY_OF_YEAR);

        GregorianCalendar eCalendar = new GregorianCalendar();
        eCalendar.setTime(endDate);
        int eYears = eCalendar.get(Calendar.YEAR);
        int eMonths = eCalendar.get(Calendar.MONTH);
        int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
        int eDaysOfYear = eCalendar.get(Calendar.DAY_OF_YEAR);

        if (StrTool.cTrim(unit).equals("Y"))
        {
            interval = eYears - sYears;

            if (eMonths < sMonths)
            {
                interval--;
            }
            else
            {
                if (eMonths == sMonths && eDays < sDays)
                {
                    interval--;
                    if (eMonths == 1)
                    { //如果同是2月，校验润年问题
                        if ((sYears % 4) == 0 && (eYears % 4) != 0)
                        { //如果起始年是润年，终止年不是润年
                            if (eDays == 28)
                            { //如果终止年不是润年，且2月的最后一天28日，那么补一
                                interval++;
                            }
                        }
                    }
                }
            }
        }
        if (StrTool.cTrim(unit).equals("M"))
        {
            interval = eYears - sYears;
            interval = interval * 12;
            interval = eMonths - sMonths + interval;

            if (eDays < sDays)
            {
                interval--;
                //eDays如果是月末，则认为是满一个月
                int maxDate = eCalendar.getActualMaximum(Calendar.DATE);
                if (eDays == maxDate)
                {
                    interval++;
                    if (sDays == 1)
                    {
                        interval++;
                    }

                }
            }
        }
        if (StrTool.cTrim(unit).equals("D"))
        {
            interval = eYears - sYears;
            interval = interval * 365;

            interval = eDaysOfYear - sDaysOfYear + interval;

            // 处理润年
            int n = 0;
            eYears--;
            if (eYears > sYears)
            {
                int i = sYears % 4;
                if (i == 0)
                {
                    sYears++;
                    n++;
                }
                int j = (eYears) % 4;
                if (j == 0)
                {
                    eYears--;
                    n++;
                }
                n += (eYears - sYears) / 4;
            }
            if (eYears == sYears)
            {
                int i = sYears % 4;
                if (i == 0)
                {
                    n++;
                }
            }
            interval += n;
        }
        return interval;
    }

    /**
     * 通过起始日期和终止日期计算以时间间隔单位为计量标准的时间间隔，约进法 author: YangZhao，Minim
     * 起始日期，(String,格式："YYYY-MM-DD")
     * @param cstartDate String
     * 终止日期，(String,格式："YYYY-MM-DD")
     * @param cendDate String
     * 时间间隔单位，可用值("Y"--年 "M"--月 "D"--日)
     * @param unit String
     * 时间间隔,整形变量int
     * @return int
     */
    public static int calInterval2(String cstartDate, String cendDate, String unit)
    {
        FDate fDate = new FDate();
        Date startDate = fDate.getDate(cstartDate);
        Date endDate = fDate.getDate(cendDate);
        if (fDate.mErrors.needDealError())
        {
            return 0;
        }

        int interval = 0;

        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        int sYears = sCalendar.get(Calendar.YEAR);
        int sMonths = sCalendar.get(Calendar.MONTH);
        int sDays = sCalendar.get(Calendar.DAY_OF_MONTH);
        int sDaysOfYear = sCalendar.get(Calendar.DAY_OF_YEAR);

        GregorianCalendar eCalendar = new GregorianCalendar();
        eCalendar.setTime(endDate);
        int eYears = eCalendar.get(Calendar.YEAR);
        int eMonths = eCalendar.get(Calendar.MONTH);
        int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
        int eDaysOfYear = eCalendar.get(Calendar.DAY_OF_YEAR);

        if (StrTool.cTrim(unit).equals("Y"))
        {
            interval = eYears - sYears;

            if (eMonths > sMonths)
            {
                interval++;
            }
            else
            {
                if (eMonths == sMonths && eDays > sDays)
                {
                    interval++;
                    if (eMonths == 1)
                    { //如果同是2月，校验润年问题
                        if ((sYears % 4) == 0 && (eYears % 4) != 0)
                        { //如果起始年是润年，终止年不是润年
                            if (eDays == 28)
                            { //如果终止年不是润年，且2月的最后一天28日，那么减一
                                interval--;
                            }
                        }
                    }
                }
            }
        }
        if (StrTool.cTrim(unit).equals("M"))
        {
            interval = eYears - sYears;
            interval = interval * 12;
            interval = eMonths - sMonths + interval;

            if (eDays > sDays)
            {
                interval++;
                //sDays如果是月末，则认为是满一个月 //eDays，改为sDays
                int maxDate = sCalendar.getActualMaximum(Calendar.DATE); //eCalendar，应该是sCalendar
                if (sDays == maxDate)
                {//eDays，应该是sDays
                    interval--;
                    if (sDays == 1)
                    {
                        interval++;
                    }
                }
            }
        }
        if (StrTool.cTrim(unit).equals("D"))
        {
            interval = eYears - sYears;
            interval = interval * 365;

            interval = eDaysOfYear - sDaysOfYear + interval;

            // 处理润年
            int n = 0;
            eYears--;
            if (eYears > sYears)
            {
                int i = sYears % 4;
                if (i == 0)
                {
                    sYears++;
                    n++;
                }
                int j = (eYears) % 4;
                if (j == 0)
                {
                    eYears--;
                    n++;
                }
                n += (eYears - sYears) / 4;
            }
            if (eYears == sYears)
            {
                int i = sYears % 4;
                if (i == 0)
                {
                    n++;
                }
            }
            interval += n;
        }
        return interval;
    }

    /**
     * 通过起始日期和终止日期计算以时间间隔单位为计量标准的时间间隔，约进法 author: YangZhao，Minim
     * 起始日期，(String,格式："YYYY-MM-DD")
     * @param cstartDate String
     * 终止日期，(String,格式："YYYY-MM-DD")
     * @param cendDate String
     * 时间间隔单位，可用值("Y"--年 "M"--月 "D"--日)
     * @param unit String
     * 时间间隔,整形变量int
     * @return int
     */
    public static int calInterval3(String cstartDate, String cendDate, String unit)
    {
        FDate fDate = new FDate();
        Date startDate = fDate.getDate(cstartDate);
        Date endDate = fDate.getDate(cendDate);
        if (fDate.mErrors.needDealError())
        {
            return 0;
        }

        int interval = 0;

        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        int sYears = sCalendar.get(Calendar.YEAR);
        int sMonths = sCalendar.get(Calendar.MONTH);
        int sDays = sCalendar.get(Calendar.DAY_OF_MONTH);
        int sDaysOfYear = sCalendar.get(Calendar.DAY_OF_YEAR);

        GregorianCalendar eCalendar = new GregorianCalendar();
        eCalendar.setTime(endDate);
        int eYears = eCalendar.get(Calendar.YEAR);
        int eMonths = eCalendar.get(Calendar.MONTH);
        int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
        int eDaysOfYear = eCalendar.get(Calendar.DAY_OF_YEAR);

        if (StrTool.cTrim(unit).equals("Y"))
        {
            interval = eYears - sYears;

            if (eMonths > sMonths)
            {
                interval++;
            }
            else
            {
                if (eMonths == sMonths && eDays > sDays)
                {
                    interval++;
                    if (eMonths == 1)
                    { //如果同是2月，校验润年问题
                        if ((sYears % 4) == 0 && (eYears % 4) != 0)
                        { //如果起始年是润年，终止年不是润年
                            if (eDays == 28)
                            { //如果终止年不是润年，且2月的最后一天28日，那么减一
                                interval--;
                            }
                        }
                    }
                }
            }
        }
        if (StrTool.cTrim(unit).equals("M"))
        {
            interval = eYears - sYears;
            interval = interval * 12;
            interval = eMonths - sMonths + interval;

            if (eDays > sDays)
            {
                interval++;
                //eDays如果是月末，则认为是满一个月
                int maxDate = eCalendar.getActualMaximum(Calendar.DATE);
                if (eDays == maxDate)
                {
                    interval--;
                    if (sDays == 1)
                    {
                        interval++;
                    }
                }
            }
        }
        if (StrTool.cTrim(unit).equals("D"))
        {
            interval = eYears - sYears;
            interval = interval * 365;

            interval = eDaysOfYear - sDaysOfYear + interval + 1;

            // 处理润年
            int n = 0;
            eYears--;
            if (eYears > sYears)
            {
                int i = sYears % 4;
                if (i == 0)
                {
                    sYears++;
                    n++;
                }
                int j = (eYears) % 4;
                if (j == 0)
                {
                    eYears--;
                    n++;
                }
                n += (eYears - sYears) / 4;
            }
            if (eYears == sYears)
            {
                int i = sYears % 4;
                if (i == 0)
                {
                    n++;
                }
            }
            interval += n;
        }
        return interval;
    }

    /**
     * 通过传入的日期可以得到所在月的第一天和最后一天的日期 author: LH
     * 日期，(String,格式："YYYY-MM-DD")
     * @param tDate String
     * 本月开始和结束日期，返回String[2]
     * @return String[]
     */
    public static String[] calFLDate(String tDate)
    {
        String MonDate[] = new String[2];
        FDate fDate = new FDate();
        Date CurDate = fDate.getDate(tDate);
        GregorianCalendar mCalendar = new GregorianCalendar();
        mCalendar.setTime(CurDate);
        int Years = mCalendar.get(Calendar.YEAR);
        int Months = mCalendar.get(Calendar.MONTH);
        int FirstDay = mCalendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        int LastDay = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        mCalendar.set(Years, Months, FirstDay);
        MonDate[0] = fDate.getString(mCalendar.getTime());
        mCalendar.set(Years, Months, LastDay);
        MonDate[1] = fDate.getString(mCalendar.getTime());
        return MonDate;
    }

    /**
     * 通过传入的日期可以得到宽限期止期(包括延长宽限期)
     * 起始日期，(String,格式："YYYY-MM-DD")
     * @param startDate String
     * @param strRiskCode String
     * @return String
     */
    public static String calLapseDate(String startDate, String strRiskCode)
    {
        String returnDate = "";
        //        Date tLapseDate = null;
        FDate tFDate = new FDate();
        int nDates;
        int nExtendLapseDates;

        //检查输入数据完整性
        if ((startDate == null) || startDate.trim().equals(""))
        {
            System.out.println("没有起始日期,计算宽限止期失败!");
            return returnDate;
        }

        //获取险种交费失效描述
        LMRiskPayDB tLMRiskPayDB = new LMRiskPayDB();
        tLMRiskPayDB.setRiskCode(strRiskCode);
        LMRiskPaySet tLMRiskPaySet = tLMRiskPayDB.query();

        if (tLMRiskPaySet.size() > 0)
        {
            if ((tLMRiskPaySet.get(1).getGracePeriodUnit() == null)
                    || tLMRiskPaySet.get(1).getGracePeriodUnit().equals(""))
            {
                //设置宽限期为默认值
                System.out.println("缺少险种交费失效描述!按默认值计算");
                nDates = 60;
                returnDate = calDate(startDate, nDates, "D", null);
            }
            else
            {
                //取得指定宽限期
                nDates = tLMRiskPaySet.get(1).getGracePeriod();
                returnDate = calDate(startDate, nDates, tLMRiskPaySet.get(1).getGracePeriodUnit(), null);
                //jdk1.4自带的方法，根据－拆分字符串到数组
                //                String[] tDate = returnDate.split("-");
                //按月进位，舍弃日精度
                if (tLMRiskPaySet.get(1).getGraceDateCalMode().equals("1"))
                {
                    //                    tLapseDate = tFDate.getDate(returnDate);
                    //                    tLapseDate.setMonth(tLapseDate.getMonth() + 1);
                    //                    tLapseDate.setDate(1);
                    //                    returnDate = tFDate.getString(tLapseDate);

                    //对日期的操作，最好使用Calendar方法
                    GregorianCalendar tCalendar = new GregorianCalendar();
                    tCalendar.setTime(tFDate.getDate(returnDate));
                    //月份进位，舍弃日精度
                    tCalendar.set(tCalendar.get(Calendar.YEAR), tCalendar.get(Calendar.MONTH) + 1, 1);
                    returnDate = tFDate.getString(tCalendar.getTime());
                }

                //按年进位，只舍弃了日精度，不舍弃月精度
                if (tLMRiskPaySet.get(1).getGraceDateCalMode().equals("2"))
                {
                    //                    tLapseDate = tFDate.getDate(returnDate);
                    //                    tLapseDate.setYear(tLapseDate.getYear() + 1);
                    //                    tLapseDate.setDate(1);
                    //                    returnDate = tFDate.getString(tLapseDate);

                    //对日期的操作，最好使用Calendar方法
                    GregorianCalendar tCalendar = new GregorianCalendar();
                    tCalendar.setTime(tFDate.getDate(returnDate));
                    //年份进位，舍弃日精度，不舍弃月精度
                    tCalendar.set(tCalendar.get(Calendar.YEAR) + 1, tCalendar.get(Calendar.MONTH), 1);
                    returnDate = tFDate.getString(tCalendar.getTime());
                }
            }
        }
        else
        {
            //设置宽限期为默认值
            System.out.println("没有险种交费失效描述!按默认值计算");
            nDates = 60;
            returnDate = calDate(startDate, nDates, "D", null);
        }

        //取得宽限期延长期
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("ExtendLapseDates");
        if (!tLDSysVarDB.getInfo())
        {
            nExtendLapseDates = 0;
        }
        else
        {
            nExtendLapseDates = Integer.parseInt(tLDSysVarDB.getSchema().getSysVarValue());
            returnDate = calDate(returnDate, nExtendLapseDates, "D", null);
        }

        return returnDate;
    }

    /**
     * 得到默认的JDBCUrl
     * @return JDBCUrl
     */
    public static JdbcUrl getDefaultUrl()
    {
        JdbcUrl tUrl = new JdbcUrl();
        return tUrl;
    }

    /**
     * 将字符串补数,将sourString的<br>后面</br>用cChar补足cLen长度的字符串,如果字符串超长，则不做处理
     * <p><b>Example: </b><p>
     * <p>RCh("Minim", "0", 10) returns "Minim00000"<p>
     * @param sourString 源字符串
     * @param cChar 补数用的字符
     * @param cLen 字符串的目标长度
     * @return 字符串
     */
    public static String RCh(String sourString, String cChar, int cLen)
    {
        int tLen = sourString.length();
        int i, j, iMax;
        String tReturn = "";
        if (tLen >= cLen)
        {
            return sourString;
        }
        iMax = cLen - tLen;
        for (i = 0; i < iMax; i++)
        {
            tReturn = tReturn + cChar;
        }
        tReturn = sourString.trim() + tReturn.trim();
        return tReturn;
    }

    /**
     * 将字符串补数,将sourString的<br>前面</br>用cChar补足cLen长度的字符串,如果字符串超长，则不做处理
     * <p><b>Example: </b><p>
     * <p>LCh("Minim", "0", 10) returns "00000Minim"<p>
     * @param sourString 源字符串
     * @param cChar 补数用的字符
     * @param cLen 字符串的目标长度
     * @return 字符串
     */
    public static String LCh(String sourString, String cChar, int cLen)
    {
        int tLen = sourString.length();
        int i, j, iMax;
        String tReturn = "";
        if (tLen >= cLen)
        {
            return sourString;
        }
        iMax = cLen - tLen;
        for (i = 0; i < iMax; i++)
        {
            tReturn = cChar + tReturn;
        }
        tReturn = tReturn.trim() + sourString.trim();
        return tReturn;
    }

    /**
     * 比较获取两天中较后的一天
     * @param date1 String
     * @param date2 String
     * @return String
     */
    public static String getLaterDate(String date1, String date2)
    {
        try
        {
            date1 = StrTool.cTrim(date1);
            date2 = StrTool.cTrim(date2);
            if (date1.equals(""))
            {
                return date2;
            }
            if (date2.equals(""))
            {
                return date1;
            }
            FDate fd = new FDate();
            Date d1 = fd.getDate(date1);
            Date d2 = fd.getDate(date2);
            if (d1.after(d2))
            {
                return date1;
            }
            return date2;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }

    }

    /**
     * 比较获取两天中较早的一天
     * @param date1 String
     * @param date2 String
     * @return String
     */
    public static String getBeforeDate(String date1, String date2)
    {
        try
        {
            date1 = StrTool.cTrim(date1);
            date2 = StrTool.cTrim(date2);
            if (date1.equals(""))
            {
                return date2;
            }
            if (date2.equals(""))
            {
                return date1;
            }
            FDate fd = new FDate();
            Date d1 = fd.getDate(date1);
            Date d2 = fd.getDate(date2);
            if (d1.before(d2))
            {
                return date1;
            }
            return date2;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }

    }

    /**
     * 得到当前系统日期 author: YT
     * @return 当前日期的格式字符串,日期格式为"yyyy-MM-dd"
     */
    public static String getCurrentDate()
    {
        //        String pattern = "yyyy-MM-dd";
        //        SimpleDateFormat df = new SimpleDateFormat(pattern);
        //        Date today = new Date();
        //        String tString = df.format(today);
        //        return tString;

        //优化执行效率，陈祥伟修改于2008-11-24
        GregorianCalendar tGCalendar = new GregorianCalendar();
        StringBuffer tStringBuffer = new StringBuffer(10);
        int sYears = tGCalendar.get(Calendar.YEAR);
        tStringBuffer.append(sYears);
        tStringBuffer.append('-');
        int sMonths = tGCalendar.get(Calendar.MONTH) + 1;
        if (sMonths < 10)
        {
            tStringBuffer.append('0');
        }
        tStringBuffer.append(sMonths);
        tStringBuffer.append('-');
        int sDays = tGCalendar.get(Calendar.DAY_OF_MONTH);
        if (sDays < 10)
        {
            tStringBuffer.append('0');
        }
        tStringBuffer.append(sDays);
        String tString = tStringBuffer.toString();
        return tString;
    }

    public static String getCurrentDate2()
    {
        String pattern = "yyyyMMdd";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        Date today = new Date();
        String tString = df.format(today);
        return tString;
    }

    /**
     * 得到当前系统时间 author: YT
     * @return 当前时间的格式字符串，时间格式为"HH:mm:ss"
     */
    public static String getCurrentTime()
    {
        //String pattern = "HH:mm:ss";
        //SimpleDateFormat df = new SimpleDateFormat(pattern);
        //Date today = new Date();
        //String tString = df.format(today);
        //return tString;

        //优化执行效率 陈祥伟修改于2008-11-24
        GregorianCalendar tGCalendar = new GregorianCalendar();
        StringBuffer tStringBuffer = new StringBuffer(8);
        int sHOUR = tGCalendar.get(Calendar.HOUR_OF_DAY);
        if (sHOUR < 10)
        {
            tStringBuffer.append('0');
        }
        tStringBuffer.append(sHOUR);
        tStringBuffer.append(':');
        int sMINUTE = tGCalendar.get(Calendar.MINUTE);
        if (sMINUTE < 10)
        {
            tStringBuffer.append('0');
        }
        tStringBuffer.append(sMINUTE);
        tStringBuffer.append(':');
        int sSECOND = tGCalendar.get(Calendar.SECOND);
        if (sSECOND < 10)
        {
            tStringBuffer.append('0');
        }
        tStringBuffer.append(sSECOND);
        String tString = tStringBuffer.toString();
        return tString;
    }

    /**
     * 得到当前系统时间
     * @return 当前时间的格式字符串，时间格式为"HHmmss"
     */
    public static String getCurrentTime2()
    {
        String pattern = "HHmmss";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        Date today = new Date();
        String tString = df.format(today);
        return tString;
    }

    /**
     * 得到当前系统时间毫秒级
     * @return 当前时间的格式字符串，时间格式为"HHmmss毫秒"
     */
    public static String getCurrentTime3()
    {
        GregorianCalendar tGCalendar = new GregorianCalendar();
        StringBuffer tStringBuffer = new StringBuffer(8);
        int sHOUR = tGCalendar.get(Calendar.HOUR_OF_DAY);
        if (sHOUR < 10)
        {
            tStringBuffer.append('0');
        }
        tStringBuffer.append(sHOUR);
        tStringBuffer.append(':');
        int sMINUTE = tGCalendar.get(Calendar.MINUTE);
        if (sMINUTE < 10)
        {
            tStringBuffer.append('0');
        }
        tStringBuffer.append(sMINUTE);
        tStringBuffer.append(':');
        int sSECOND = tGCalendar.get(Calendar.SECOND);
        if (sSECOND < 10)
        {
            tStringBuffer.append('0');
        }
        tStringBuffer.append(sSECOND);
        tStringBuffer.append(':');
        int milliSECOND = tGCalendar.get(Calendar.MILLISECOND);//毫秒
        tStringBuffer.append(milliSECOND);
        String tString = tStringBuffer.toString();
        return tString;
    }

    /**
     * 得到流水号前导 author: YT
     * @param comCode 机构代码
     * @return 流水号的前导字符串
     */
    public static String getNoLimit(String comCode)
    {
        comCode = comCode.trim();
        int tLen = comCode.length();
        if (tLen > 6)
        {
            comCode = comCode.substring(0, 6);
        }
        if (tLen < 6)
        {
            comCode = RCh(comCode, "0", 6);
        }
        String tString = "";
        tString = comCode + getCurrentDate().substring(0, 4);
        return tString;
    }

    /**
     * picc获取管理机构，截取管理代码的第3-6位（二级机构+三级机构）
     * 再加上日期编码的两位年两位月日日 052203
     * @param comCode String
     * @return String
     */
    public static String getPiccNoLimit(String comCode)
    {
        comCode = comCode.trim();
        System.out.println("comCode :" + comCode);
        int tLen = comCode.length();
        if (tLen == 8)
        {
            comCode = comCode.substring(2, 6);
        }
        if (tLen == 4)
        {
            comCode = comCode.substring(2, 4) + "00";
        }
        System.out.println("SubComCode :" + comCode);
        String tString = "";
        tString = comCode + getCurrentDate().substring(2, 4) + getCurrentDate().substring(5, 7)
                + getCurrentDate().substring(8, 10);
        System.out.println("PubFun getPiccNoLimit : " + tString);
        return tString;
    }

    /**
     * 该函数得到c_Str中的第c_i个以c_Split分割的字符串
     * @param c_Str 目标字符串
     * @param c_i 位置
     * @param c_Split   分割符
     * @return 如果发生异常，则返回空
     */
    public static String getStr(String c_Str, int c_i, String c_Split)
    {
        String t_Str1 = "", t_Str2 = "", t_strOld = "";
        int i = 0, i_Start = 0;
        //        int j_End = 0;
        t_Str1 = c_Str;
        t_Str2 = c_Split;
        i = 0;
        try
        {
            while (i < c_i)
            {
                i_Start = t_Str1.indexOf(t_Str2, 0);
                if (i_Start >= 0)
                {
                    i = i + 1;
                    t_strOld = t_Str1;
                    t_Str1 = t_Str1.substring(i_Start + t_Str2.length(), t_Str1.length());
                }
                else
                {
                    if (i != c_i - 1)
                    {
                        t_Str1 = "";
                    }
                    break;
                }
            }

            if (i_Start >= 0)
            {
                t_Str1 = t_strOld.substring(0, i_Start);
            }
        }
        catch (Exception ex)
        {
            t_Str1 = "";
        }
        return t_Str1;
    }

    /**
     * 把数字金额转换为中文大写金额 author: HST
     * @param money 数字金额(double)
     * @return 中文大写金额(String)
     */
    public static String getChnMoney(double money)
    {
        String ChnMoney = "";
        String s0 = "";

        // 在原来版本的程序中，getChnMoney(585.30)得到的数据是585.29。

        if (money == 0.0)
        {
            ChnMoney = "零元整";
            return ChnMoney;
        }

        if (money < 0)
        {
            s0 = "负";
            money = money * (-1);
        }

        String sMoney = new DecimalFormat("0").format(money * 100);

        int nLen = sMoney.length();
        String sInteger;
        String sDot;
        if (nLen < 2)
        {
            //add by JL at 2004-9-14
            sInteger = "";
            if (nLen == 1)
            {
                sDot = "0" + sMoney.substring(nLen - 1, nLen);
            }
            else
            {
                sDot = "0";
            }
        }
        else
        {
            sInteger = sMoney.substring(0, nLen - 2);
            sDot = sMoney.substring(nLen - 2, nLen);
        }

        String sFormatStr = PubFun.formatStr(sInteger);

        String s1 = PubFun.getChnM(sFormatStr.substring(0, 4), "亿");

        String s2 = PubFun.getChnM(sFormatStr.substring(4, 8), "万");

        String s3 = PubFun.getChnM(sFormatStr.substring(8, 12), "");

        String s4 = PubFun.getDotM(sDot);

        if (s1.length() > 0 && s1.substring(0, 1).equals("0"))
        {
            s1 = s1.substring(1, s1.length());
        }
        if (s1.length() > 0 && s1.substring(s1.length() - 1, s1.length()).equals("0") && s2.length() > 0
                && s2.substring(0, 1).equals("0"))
        {
            s1 = s1.substring(0, s1.length() - 1);
        }
        if (s2.length() > 0 && s2.substring(s2.length() - 1, s2.length()).equals("0") && s3.length() > 0
                && s3.substring(0, 1).equals("0"))
        {
            s2 = s2.substring(0, s2.length() - 1);
        }
        if (s4.equals("00"))
        {
            s4 = "";
            if (s3.length() > 0 && s3.substring(s3.length() - 1, s3.length()).equals("0"))
            {
                s3 = s3.substring(0, s3.length() - 1);
            }
        }
        if (s3.length() > 0 && s3.substring(s3.length() - 1, s3.length()).equals("0") && s4.length() > 0
                && s4.substring(0, 1).equals("0"))
        {
            s3 = s3.substring(0, s3.length() - 1);
        }
        if (s4.length() > 0 && s4.substring(s4.length() - 1, s4.length()).equals("0"))
        {
            s4 = s4.substring(0, s4.length() - 1);
        }
        if (s3.equals("0"))
        {
            s3 = "";
            s4 = "0" + s4;
        }

        ChnMoney = s0 + s1 + s2 + s3 + "元" + s4;
        if (ChnMoney.substring(0, 1).equals("0"))
        {
            ChnMoney = ChnMoney.substring(1, ChnMoney.length());
        }
        for (int i = 0; i < ChnMoney.length(); i++)
        {
            if (ChnMoney.substring(i, i + 1).equals("0"))
            {
                ChnMoney = ChnMoney.substring(0, i) + "零" + ChnMoney.substring(i + 1, ChnMoney.length());
            }
        }

        if (sDot.substring(1, 2).equals("0"))
        {
            ChnMoney += "整";
        }

        return ChnMoney;
    }

    /**
     * 得到money的角分信息
     * @param sIn String
     * @return String
     */
    private static String getDotM(String sIn)
    {
        String sMoney = "";
        if (!sIn.substring(0, 1).equals("0"))
        {
            sMoney += getNum(sIn.substring(0, 1)) + "角";
        }
        else
        {
            sMoney += "0";
        }
        if (!sIn.substring(1, 2).equals("0"))
        {
            sMoney += getNum(sIn.substring(1, 2)) + "分";
        }
        else
        {
            sMoney += "0";
        }

        return sMoney;
    }

    /**
     * 添加仟、佰、拾等单位信息
     * @param strUnit String
     * @param digit String
     * @return String
     */
    private static String getChnM(String strUnit, String digit)
    {
        String sMoney = "";
        boolean flag = false;

        if (strUnit.equals("0000"))
        {
            sMoney += "0";
            return sMoney;
        }
        if (!strUnit.substring(0, 1).equals("0"))
        {
            sMoney += getNum(strUnit.substring(0, 1)) + "仟";
        }
        else
        {
            sMoney += "0";
            flag = true;
        }
        if (!strUnit.substring(1, 2).equals("0"))
        {
            sMoney += getNum(strUnit.substring(1, 2)) + "佰";
            flag = false;
        }
        else
        {
            if (flag == false)
            {
                sMoney += "0";
                flag = true;
            }
        }
        if (!strUnit.substring(2, 3).equals("0"))
        {
            sMoney += getNum(strUnit.substring(2, 3)) + "拾";
            flag = false;
        }
        else
        {
            if (flag == false)
            {
                sMoney += "0";
                flag = true;
            }
        }
        if (!strUnit.substring(3, 4).equals("0"))
        {
            sMoney += getNum(strUnit.substring(3, 4));
        }
        else
        {
            if (flag == false)
            {
                sMoney += "0";
                flag = true;
            }
        }

        if (sMoney.substring(sMoney.length() - 1, sMoney.length()).equals("0"))
        {
            sMoney = sMoney.substring(0, sMoney.length() - 1) + digit.trim() + "0";
        }
        else
        {
            sMoney += digit.trim();
        }
        return sMoney;
    }

    /**
     * 格式化字符
     * @param sIn String
     * @return String
     */
    private static String formatStr(String sIn)
    {
        int n = sIn.length();
        String sOut = sIn;
        //        int i = n % 4;

        for (int k = 1; k <= 12 - n; k++)
        {
            sOut = "0" + sOut;
        }
        return sOut;
    }

    /**
     * 获取阿拉伯数字和中文数字的对应关系
     * @param value String
     * @return String
     */
    private static String getNum(String value)
    {
        String sNum = "";
        Integer I = new Integer(value);
        int iValue = I.intValue();
        switch (iValue)
        {
            case 0:
                sNum = "零";
                break;
            case 1:
                sNum = "壹";
                break;
            case 2:
                sNum = "贰";
                break;
            case 3:
                sNum = "叁";
                break;
            case 4:
                sNum = "肆";
                break;
            case 5:
                sNum = "伍";
                break;
            case 6:
                sNum = "陆";
                break;
            case 7:
                sNum = "柒";
                break;
            case 8:
                sNum = "捌";
                break;
            case 9:
                sNum = "玖";
                break;
        }
        return sNum;
    }

    /**
     * 如果一个字符串数字中小数点后全为零，则去掉小数点及零
     * @param Value String
     * @return String
     */
    public static String getInt(String Value)
    {
        if (Value == null)
        {
            return null;
        }
        String result = "";
        boolean mflag = true;
        int m = 0;
        m = Value.lastIndexOf(".");
        if (m == -1)
        {
            result = Value;
        }
        else
        {
            for (int i = m + 1; i <= Value.length() - 1; i++)
            {
                if (Value.charAt(i) != '0')
                {
                    result = Value;
                    mflag = false;
                    break;
                }
            }
            if (mflag == true)
            {
                result = Value.substring(0, m);
            }
        }
        return result;
    }

    /**
     * 得到近似值
     * @param aValue double
     * @return double
     */
    public static double getApproximation(double aValue)
    {
        if (java.lang.Math.abs(aValue) <= 0.01)
        {
            aValue = 0;
        }
        return aValue;
    }

    /**
     * 根据输入标记为间隔符号，拆分字符串
     * @param strMain String
     * @param strDelimiters String
     * 失败返回NULL
     * @return String[]
     */
    public static String[] split(String strMain, String strDelimiters)
    {
        int i;
        int intIndex = 0; //记录分隔符位置，以取出子串
        Vector vResult = new Vector(); //存储子串的数组
        String strSub = ""; //存放子串的中间变量

        strMain = strMain.trim();

        //若主字符串比分隔符串还要短的话,则返回空字符串
        if (strMain.length() <= strDelimiters.length())
        {
            System.out.println("分隔符串长度大于等于主字符串长度，不能进行拆分！");
            return null;
        }

        //取出第一个分隔符在主串中的位置
        intIndex = strMain.indexOf(strDelimiters);

        //在主串中找不到分隔符
        if (intIndex == -1)
        {
            String[] arrResult = { strMain };
            return arrResult;
        }

        //分割主串到数组中
        while (intIndex != -1)
        {
            strSub = strMain.substring(0, intIndex);
            if (intIndex != 0)
            {
                vResult.add(strSub);
            }
            else
            {
                //break;
                vResult.add("");
            }

            strMain = strMain.substring(intIndex + strDelimiters.length()).trim();
            intIndex = strMain.indexOf(strDelimiters);
        }

        //如果最末不是分隔符，取最后的字符串
        if (!strMain.equals("") && strMain != null)
        {
            vResult.add(strMain);
        }

        String[] arrResult = new String[vResult.size()];
        for (i = 0; i < vResult.size(); i++)
        {
            arrResult[i] = (String) vResult.get(i);
        }

        return arrResult;
    }

    /**
     * 设置数字精度
     * 需要格式化的数据
     * @param value float
     * 精度描述（0.00表示精确到小数点后两位）
     * @param precision String
     * @return double
     */
    public static double setPrecision(float value, String precision)
    {
        return Float.parseFloat(new DecimalFormat(precision).format(value));
    }

    /**
     * 设置数字精度
     * 需要格式化的数据
     * @param value double
     * 精度描述（0.00表示精确到小数点后两位）
     * @param precision String
     * @return double
     */
    public static double setPrecision(double value, String precision)
    {
        int idx = precision.indexOf(".");
        //        if (idx) {
        //
        //        }
        return Double.parseDouble(new DecimalFormat(precision).format(value));
    }

    /**
     * 把schemaset对象拷贝一份返回
     * @param srcSet SchemaSet
     * @return SchemaSet
     */
    public static SchemaSet copySchemaSet(SchemaSet srcSet)
    {
        Reflections reflect = new Reflections();
        try
        {
            if (srcSet != null && srcSet.size() > 0)
            {
                if (srcSet.getObj(1) == null)
                {
                    return null;
                }
                Class cls = srcSet.getClass();
                Schema schema = (Schema) srcSet.getObj(1).getClass().newInstance();
                SchemaSet obj = (SchemaSet) cls.newInstance();
                obj.add(schema);
                reflect.transFields(obj, srcSet);
                return (SchemaSet) obj;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * 互换LP和LC表数据
     * @param source Schema
     * @param object Schema
     * @return boolean
     */
    public static boolean exchangeSchema(Schema source, Schema object)
    {
        try
        {
            //把LP表的数据传递到LC表
            Reflections tReflections = new Reflections();
            tReflections.transFields(object, source);

            //获取一个数据库连接DB
            Method m = object.getClass().getMethod("getDB", null);
            Schema schemaDB = (Schema) m.invoke(object, null);
            //因为LP表与LC表只有EdorNo和EdorType两个关键字的差别，所以可以唯一获取LC表对应记录
            m = schemaDB.getClass().getMethod("getInfo", null);
            m.invoke(schemaDB, null);
            m = schemaDB.getClass().getMethod("getSchema", null);
            object = (Schema) m.invoke(schemaDB, null);

            //把LC表数据备份到临时表
            m = object.getClass().getMethod("getSchema", null);
            Schema tSchema = (Schema) m.invoke(object, null);

            //互换LP和LC表数据
            tReflections.transFields(object, source);
            tReflections.transFields(source, tSchema);

            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * 生成更新的sql列表
     * @param tables String[]
     * @param condition String
     * @param wherepart String
     * @return Vector
     */
    public static Vector formUpdateSql(String[] tables, String condition, String wherepart)
    {
        Vector sqlVec = new Vector();
        for (int i = 0; i < tables.length; i++)
        {
            sqlVec.add("update " + tables[i] + " set " + condition + " where " + wherepart);
        }
        return sqlVec;
    }

    /**
     * 将账号前的0去掉
     * @param sIn String
     * @return String
     */
    public static String DeleteZero(String sIn)
    {
        int n = sIn.length();
        String sOut = sIn;
        int k = 0;

        while (sOut.substring(0, 1).equals("0") && n > 1)
        {
            sOut = sOut.substring(1, n);
            n = sOut.length();
            System.out.println(sOut);
        }

        if (sOut.equals("0"))
        {
            return "";
        }
        else
        {
            return sOut;
        }
    }

    /**
     * 转换JavaScript解析不了的特殊字符
     * @param s String
     * @return String
     */
    public static String changForJavaScript(String s)
    {
        char[] arr = s.toCharArray();
        s = "";
        for (int i = 0; i < arr.length; i++)
        {
            if (arr[i] == '"' || arr[i] == '\'' || arr[i] == '\n')
            {
                s = s + "\\";
            }

            s = s + arr[i];
        }

        return s;
    }

    /**
     * 转换JavaScript解析不了的特殊字符
     * @param s String
     * @return String
     */
    public static String changForHTML(String s)
    {
        char[] arr = s.toCharArray();
        s = "";

        for (int i = 0; i < arr.length; i++)
        {
            if (arr[i] == '"' || arr[i] == '\'')
            {
                s = s + "\\";
            }

            if (arr[i] == '\n')
            {
                s = s + "<br>";
                continue;
            }

            s = s + arr[i];
        }

        return s;
    }

    public static String getClassFileName(Object o)
    {
        String fileName = o.getClass().getName();
        fileName = fileName.substring(fileName.lastIndexOf(".") + 1);
        return fileName;
    }

    public static void out(Object o, String s)
    {
        System.out.println(PubFun.getClassFileName(o) + " : " + s);
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        try
        {
            /*String s = "三大件福连锁店福连锁店福连锁店连锁店福连锁店福连锁店福连锁店";

            //            PubFun.sendMessage("13581737078", StrTool.unicodeToGBK(s));
            //            System.out.println(PubFun.LRemoveString("0000sdfsdfsd","0"));
            System.out.println(PubFun.calInterval2("2010-2-28", "2010-3-31", "M"));
            System.out.println(PubFun.calInterval2("2010-2-28", "2010-3-30", "M"));
            System.out.println(PubFun.calInterval2("2010-2-28", "2010-3-29", "M"));
            System.out.println(PubFun.calInterval2("2010-5-1", "2010-5-31", "M"));*/
        	boolean aaa = isDoBQ("00000034000003", "2011-08-07");
        	System.out.println(aaa);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 计算保单年度
     * @param cstartDate String
     * @param cendDate String
     * @return int
     */
    public static int calPolYear(String cstartDate, String cendDate)
    {
        FDate fDate = new FDate();
        Date startDate = fDate.getDate(cstartDate);
        Date endDate = fDate.getDate(cendDate);
        if (fDate.mErrors.needDealError())
        {
            return 0;
        }

        int interval = 0;

        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        int sYears = sCalendar.get(Calendar.YEAR);
        //        int sMonths = sCalendar.get(Calendar.MONTH);
        //        int sDays = sCalendar.get(Calendar.DAY_OF_MONTH);
        int sDaysOfYear = sCalendar.get(Calendar.DAY_OF_YEAR);

        GregorianCalendar eCalendar = new GregorianCalendar();
        eCalendar.setTime(endDate);
        int eYears = eCalendar.get(Calendar.YEAR);
        //        int eMonths = eCalendar.get(Calendar.MONTH);
        //        int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
        int eDaysOfYear = eCalendar.get(Calendar.DAY_OF_YEAR);

        interval = eYears - sYears;
        interval = interval * 365;
        interval = eDaysOfYear - sDaysOfYear + interval;

        // 处理润年
        int n = 0;
        eYears--;
        if (eYears > sYears)
        {
            int i = sYears % 4;
            if (i == 0)
            {
                sYears++;
                n++;
            }
            int j = (eYears) % 4;
            if (j == 0)
            {
                eYears--;
                n++;
            }
            n += (eYears - sYears) / 4;
        }
        if (eYears == sYears)
        {
            int i = sYears % 4;
            if (i == 0)
            {
                n++;
            }
        }
        interval += n;

        int x = 365;
        int PolYear = 1;
        while (x < interval)
        {
            x = x + 365;
            PolYear = PolYear + 1;
        }

        return PolYear;
    }

    /**
     * 通过身份证号号获取生日日期
     * @param IdNo String
     * @return String
     */
    public static String getBirthdayFromId(String IdNo)
    {
        String tIdNo = StrTool.cTrim(IdNo);
        String birthday = "";
        if (tIdNo.length() != 15 && tIdNo.length() != 18)
        {
            return "";
        }
        if (tIdNo.length() == 18)
        {
            birthday = tIdNo.substring(6, 14);
            birthday = birthday.substring(0, 4) + "-" + birthday.substring(4, 6) + "-" + birthday.substring(6);
        }
        if (tIdNo.length() == 15)
        {
            birthday = tIdNo.substring(6, 12);
            birthday = birthday.substring(0, 2) + "-" + birthday.substring(2, 4) + "-" + birthday.substring(4);
            birthday = "19" + birthday;
        }
        return birthday;

    }

    /**
     * 通过身份证号获取性别
     * @param IdNo String
     * @return String
     */
    public static String getSexFromId(String IdNo)
    {
        String tIdNo = StrTool.cTrim(IdNo);
        if (tIdNo.length() != 15 && tIdNo.length() != 18)
        {
            return "";
        }
        String sex = "";
        if (tIdNo.length() == 15)
        {
            sex = tIdNo.substring(14, 15);
        }
        else
        {
            sex = tIdNo.substring(16, 17);
        }
        try
        {
            int iSex = Integer.parseInt(sex);
            iSex = iSex % 2;
            if (iSex == 0)
            {
                return "1";
            }
            if (iSex == 1)
            {
                return "0";
            }
        }
        catch (Exception ex)
        {
            return "";
        }
        return "";
    }

    /**
     * 将Schema中的MakeDate,MakeTime等默认信息填充
     * @param o Object
     */
    public static void fillDefaultField(Object o)
    {
        Class[] c = new Class[1];
        Method m = null;
        String[] date = new String[1];
        date[0] = PubFun.getCurrentDate();
        String[] time = new String[1];
        time[0] = PubFun.getCurrentTime();
        try
        {
            c[0] = Class.forName("java.lang.String");
        }
        catch (Exception ex)
        {
        }
        try
        {
            m = o.getClass().getMethod("setMakeDate", c);
        }
        catch (Exception ex)
        {
        }
        try
        {
            m.invoke(o, date);
        }
        catch (Exception ex)
        {
        }
        try
        {
            m = o.getClass().getMethod("setModifyDate", c);
        }
        catch (Exception ex)
        {
        }
        try
        {
            m.invoke(o, date);
        }
        catch (Exception ex)
        {
        }
        try
        {
            m = o.getClass().getMethod("setMakeTime", c);
        }
        catch (Exception ex)
        {
        }
        try
        {
            m.invoke(o, time);
        }
        catch (Exception ex)
        {
        }
        try
        {
            m = o.getClass().getMethod("setModifyTime", c);
        }
        catch (Exception ex)
        {
        }
        try
        {
            m.invoke(o, time);
        }
        catch (Exception ex)
        {
        }
    }

    /**
     * 重载,使Set也可以填充默认字段
     * @param set SchemaSet
     */
    public static void fillDefaultField(SchemaSet set)
    {
        for (int i = 1; i <= set.size(); i++)
        {
            fillDefaultField(set.getObj(i));
        }
    }

    /**
     * 生日是birthday 到date 的年龄
     * @param date String
     * @param birthday String
     * @return int
     */
    public static int getInsuredAppAge(String date, String birthday)
    {
        String in_year = birthday.split("-")[0];
        String in_month = birthday.split("-")[1];
        String in_day = birthday.split("-")[2];

        String year = date.split("-")[0];
        String month = date.split("-")[1];
        String day = date.split("-")[2];
        int age = 0;
        if (Integer.parseInt(year) > Integer.parseInt(in_year))
        {
            age = Integer.parseInt(year) - Integer.parseInt(in_year);
        }
        if (Integer.parseInt(month) < Integer.parseInt(in_month))
        {
            age--;
        }
        if (Integer.parseInt(month) == Integer.parseInt(in_month))
        {
            if (Integer.parseInt(day) < Integer.parseInt(in_day))
            {
                age--;
            }
        }
        return age;
    }

    /**
     * 可以执行dos命令
     *
     * @param Com String
     * @throws Exception
     */
    public static void sendMessage(String tPhone, String tMessage) throws Exception
    {
        if (tPhone == null || tPhone.equals(""))
        {
            throw new Exception("没有录入电话号码！");
        }
        if (tMessage == null || tMessage.equals(""))
        {
            throw new Exception("没有录入短信内容！");
        }
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("mobile", tPhone);
        tTransferData.setNameAndValue("content", tMessage);
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "comp";
        tGlobalInput.ManageCom = "86";
        tGlobalInput.ComCode = "86";
        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(tGlobalInput);
        SendMsgMail msg = new SendMsgMail();
        if (!msg.submitData(tVData, "Message"))
        {
            throw new Exception("发送短信失败：" + msg.mErrors.getErrContent());
        }
    }

    /**
     *
     * @param mobile 移动电话号码
     * @param content 短信内容
     * @param orgCode 发送机构
     *
     * @return
     */
    public static String sendSMS(String mobile, String content, String orgCode)
    {
        if (mobile == null || mobile.equals(""))
            return "移动电话号码为空！";
        if (content == null || content.equals(""))
            return "短信内容为空！";

//        SmsServiceSoapBindingStub binding = null;
//        try
//        {
//            binding = (SmsServiceSoapBindingStub) new SmsServiceServiceLocator().getSmsService(); //创建binding对象
//        }
//        catch (Exception ex)
//        {
//            ex.printStackTrace();
//        }

        Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。

 //       binding.setTimeout(60000);
        SMSClient smsClient=new SMSClient();
        // 创建单条短信发送设置
        Vector vector = new Vector();
        SmsMessageNew message = new SmsMessageNew();
        message.setReceiver(mobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素        
        message.setContents(content);//设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
//        if (orgCode != null && orgCode.length() == 8)
//        {
//            orgCode = orgCode.substring(0, 4) + "0000";
//        }
        message.setOrgCode(orgCode);//设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
        vector.add(message);
        // --------------------

        // 创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
        SmsMessagesNew msgs = new SmsMessagesNew();
        msgs.setOrganizationId("86"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
        msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
        msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
        msgs.setStartDate(PubFun.getCurrentDate()); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
        msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
        msgs.setEndDate(PubFun.getCurrentDate()); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
        msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
        msgs.setMessages((SmsMessageNew[]) vector.toArray(new SmsMessageNew[vector.size()]));
        msgs.setTaskValue(SMSClient.mes_taskVlaue15);
        
        smsClient.sendSMS(msgs);
        // --------------------

//        // 发送短信
//        try
//        {
//           value = binding.sendSMS("Admin", "Admin", msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
//            System.out.println(value.getStatus());
//            System.out.println(value.getMessage());
//        }
//        catch (RemoteException ex)
//        {
//            ex.printStackTrace();
//        }
        // --------------------

        return null;
    }

    /**
     * Trim函数
     * 
     */
    public static String TrimStr(String str)
    {
        String str1 = str.trim();
        return str1;
    }

    /**
     *判断是否为续期应收，add by wanglong
     * @param pmContNo String
     * @param pmStatDate String
     * @param pmEndDate String
     * @return boolean
     */
    public static boolean isDueFee(String pmContNo, String pmStatDate, String pmEndDate)
    {
        try
        {
            ExeSQL tExeSQL = new ExeSQL();
            String count = null;
            StringBuffer tSBql = new StringBuffer(128);
            tSBql.append("SELECT COUNT(*) FROM LJSPay a WHERE a.otherNo='").append(pmContNo).append("'").append(
                    " and (select count(PolNo) from LJSPayPerson where ").append(" LastPayToDate>='")
                    .append(pmStatDate).append("' and LastPayToDate<='").append(pmEndDate).append(
                            "' and GetNoticeNo=a.GetNoticeNo").append(" and payCount>1 )>0");
            String strSql = tSBql.toString();
            count = tExeSQL.getOneValue(strSql);
            if ((count == null) || count.equals("0"))
            {
                return false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();

        }
        return true;
    }

    /**
     * 将字符串补数,将sourString的<br>前面</br>用cChar补足cLen长度的字符串,如果字符串超长，则不做处理,允许填充空格
     * <p><b>Example: </b><p>
     * <p>LCh("Minim", "0", 10) returns "00000Minim"<p>
     * @param sourString 源字符串
     * @param cChar 补数用的字符
     * @param cLen 字符串的目标长度
     * @return 字符串
     */
    public static String LFillCh(String sourString, String cChar, int cLen)
    {
        int tLen = sourString.length();
        int i, j, iMax;
        String tReturn = "";
        if (tLen >= cLen)
        {
            return sourString;
        }
        iMax = cLen - tLen;
        for (i = 0; i < iMax; i++)
        {
            tReturn = cChar + tReturn;
        }
        tReturn = tReturn + sourString.trim();
        return tReturn;
    }

    public static String LRemoveString(String sourString, String cChar)
    {
        int tLen = sourString.length();
        String result = sourString;
        for (int i = 0; i < tLen; i++)
        {
            if (sourString.substring(i, i + 1).trim().toLowerCase().equals(cChar))
            {
                result = sourString.substring(i + 1);
            }
            else
            {
                break;
            }
        }
        return result;
    }

    /**
     * 替换字符中的字符
     */
    public static String RePlaceString(String sourString, String oldChar, String newChar)
    {
        String str = StrTool.unicodeToGBK(StrTool.cTrim(sourString));
        str = str.replaceAll(StrTool.unicodeToGBK(oldChar), StrTool.unicodeToGBK(newChar));
        return str;
    }

    /**
     * 设置数字精度
     * 需要格式化的数据
     * @param value double
     * 精度描述（0.00表示精确到小数点后两位）
     * @param precision String
     * @return double
     * add by fuxin
     */
    public static String setPrecision1(double value, String precision)
    {
        DecimalFormat df = new DecimalFormat(precision);
        return df.format(value);
    }

    /**
     * 将".00"格式化为"0.00"
     * 需要格式化的数据
     * @param value double or String
     * 精度描述（0.00表示精确到小数点后两位）
     * @return String
     * add by zhanggm
     */
    public static String setPrecision2(double value)
    {
        DecimalFormat df = new DecimalFormat("#.00");
        String value1 = df.format(value);
        return ".".equals(value1.substring(0, 1)) ? "0" + value1 : value1;
    }

    public static String setPrecision2(String value)
    {
        double value1 = Double.parseDouble(value);
        return setPrecision2(value1);
    }

    /**
     * 校验日期是否为YYYY-MM-DD
     * @param aDate String
     * @return boolean
     */
    public static boolean checkDateForm(String aDate)
    {
        SimpleDateFormat tDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        tDateFormat.setLenient(false);
        try
        {
            tDateFormat.parse(aDate);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    /**
     * 严格校验身份证号
     * @param aIDNo String
     * @return boolean
     */
    public static boolean checkIDNo(String aIDNo)
    {
        if (aIDNo == null || "".equals(aIDNo))
        {
            //不能为空
            return false;
        }

        int tIDLength = aIDNo.length();
        if (tIDLength != 15 && tIDLength != 18)
        {
            //长度错误
            return false;
        }

        if (tIDLength == 15)
        {
            for (int i = 0; i < tIDLength; i++)
            {
                if (!Character.isDigit(aIDNo.charAt(i)))
                {
                    //15位身份证没有字符
                    return false;
                }
            }

            int tYear = Integer.parseInt("19" + aIDNo.substring(6, 8));
            int tMonth = Integer.parseInt(aIDNo.substring(8, 10));
            int tDay = Integer.parseInt(aIDNo.substring(10, 12));

            String tDate = tYear + "-" + tMonth + "-" + tDay;
            if (!checkDateForm(tDate))
            {
                //日期错误
                return false;
            }

        }
        else
        {
            for (int i = 0; i < tIDLength - 1; i++)
            {
                if (!Character.isDigit(aIDNo.charAt(i)))
                {
                    //前17位身份证没有字符
                    return false;
                }
            }

            if (!Character.isDigit(aIDNo.charAt(17)) && !"X".equals("" + aIDNo.charAt(17))
                    && !"x".equals("" + aIDNo.charAt(17)))
            {
                //最后一位错误
                return false;
            }

            int tYear = Integer.parseInt(aIDNo.substring(6, 10));
            int tMonth = Integer.parseInt(aIDNo.substring(10, 12));
            int tDay = Integer.parseInt(aIDNo.substring(12, 14));

            String tDate = tYear + "-" + tMonth + "-" + tDay;
            if (!checkDateForm(tDate))
            {
                //日期错误
                return false;
            }

            if (!GetVerifyBit(aIDNo).equals(("" + aIDNo.charAt(17)).toUpperCase()))
            {
                //校验位错误
                return false;
            }
        }
        return true;
    }

    /**
     * 获取18位身份证的最后一位
     * @param aIDNo String
     * @return String
     */
    public static String GetVerifyBit(String aIDNo)
    {
        //原理:
        //∑(a[i]*W[i]) mod 11 ( i = 2, 3, ..., 18 )(1)
        //"*" 表示乘号
        //i--------表示身份证号码每一位的序号，从右至左，最左侧为18，最右侧为1。
        //a[i]-----表示身份证号码第 i 位上的号码
        //W[i]-----表示第 i 位上的权值 W[i] = 2^(i-1) mod 11
        //计算公式 (1) 令结果为 R
        //根据下表找出 R 对应的校验码即为要求身份证号码的校验码C。
        //R 0 1 2 3 4 5 6 7 8 9 10
        //C 1 0 X 9 8 7 6 5 4 3 2// X 就是 10，罗马数字中的 10 就是 X
        //15位转18位中,计算校验位即最后一位

        if (aIDNo.length() != 18)
        {
            return "";
        }
        String tResult = "";
        int tID0 = Integer.parseInt("" + aIDNo.charAt(0));
        int tID1 = Integer.parseInt("" + aIDNo.charAt(1));
        int tID2 = Integer.parseInt("" + aIDNo.charAt(2));
        int tID3 = Integer.parseInt("" + aIDNo.charAt(3));
        int tID4 = Integer.parseInt("" + aIDNo.charAt(4));
        int tID5 = Integer.parseInt("" + aIDNo.charAt(5));
        int tID6 = Integer.parseInt("" + aIDNo.charAt(6));
        int tID7 = Integer.parseInt("" + aIDNo.charAt(7));
        int tID8 = Integer.parseInt("" + aIDNo.charAt(8));
        int tID9 = Integer.parseInt("" + aIDNo.charAt(9));
        int tID10 = Integer.parseInt("" + aIDNo.charAt(10));
        int tID11 = Integer.parseInt("" + aIDNo.charAt(11));
        int tID12 = Integer.parseInt("" + aIDNo.charAt(12));
        int tID13 = Integer.parseInt("" + aIDNo.charAt(13));
        int tID14 = Integer.parseInt("" + aIDNo.charAt(14));
        int tID15 = Integer.parseInt("" + aIDNo.charAt(15));
        int tID16 = Integer.parseInt("" + aIDNo.charAt(16));

        int nNum = tID0 * 7 + tID1 * 9 + tID2 * 10 + tID3 * 5 + tID4 * 8 + tID5 * 4 + tID6 * 2 + tID7 * 1 + tID8 * 6
                + tID9 * 3 + tID10 * 7 + tID11 * 9 + tID12 * 10 + tID13 * 5 + tID14 * 8 + tID15 * 4 + tID16 * 2;
        nNum = nNum % 11;
        switch (nNum)
        {
            case 0:
                tResult = "1";
                break;
            case 1:
                tResult = "0";
                break;
            case 2:
                tResult = "X";
                break;
            case 3:
                tResult = "9";
                break;
            case 4:
                tResult = "8";
                break;
            case 5:
                tResult = "7";
                break;
            case 6:
                tResult = "6";
                break;
            case 7:
                tResult = "5";
                break;
            case 8:
                tResult = "4";
                break;
            case 9:
                tResult = "3";
                break;
            case 10:
                tResult = "2";
                break;
        }
        return tResult;
    }

    //  date 20101207 by gzh
    /**
     * <p>
     * 由15位身份证计算18位.
     * </p>
     * 
     * @param id
     *            String,证件号码
     * @param type
     *            String,类型0：15->18类型1：18->15
     * @return boolean
     */
    public static final String getNewId(String id, String type)
    {
        String newid = "";
        if (type.equals("0"))
        {
            final int[] W = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1 };
            // 加权因子
            final String[] A = { "1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2" };
            // 校验码
            int i, j, s = 0;
            newid = id;
            newid = newid.substring(0, 6) + "19" + newid.substring(6, id.length());
            for (i = 0; i < newid.length(); i++)
            {

                j = Integer.parseInt(newid.substring(i, i + 1)) * W[i];
                s += j;
            }
            s %= 11;
            newid += A[s];
        }
        if (type.equals("1"))
        {
            newid = id.substring(0, 6) + id.substring(8, 17);
        }
        return newid;
    }

    /************************
     * 判断传入的字符串是否为数字或字母
     * 如果是数字，则返回true;否则返回false
     * 只要能Double.parseDouble(str)的都支持，比如:0000.89=0.89
     ************************/
    public static boolean isNumLetter(String str)
    {
        Pattern pattern = Pattern.compile("^[A-Za-z0-9]+$");
        Matcher isNum = pattern.matcher(str);

        if (!isNum.matches())
        {
            return false;
        }
        return true;
    }

    /**
     * <p>
     * 比较两个身份证
     * </p>
     * 
     * @param id1
     *            String,证件号码1
     * @param id2
     *            String,证件号码2
     * @return boolean
     */
    public static boolean CompareId(String id1, String id2)
    {
        if (id1.length() == id2.length())
        {
            if (id1.equals(id2))
            {
                return true;
            }
        }
        else if (id1.length() == 15 && id2.length() == 18)
        {
            if (PubFun.getNewId(id1, "0").equals(id2))
            {
                return true;
            }
        }
        else if (id1.length() == 18 && id2.length() == 15)
        {
            if (PubFun.getNewId(id1, "1").equals(id2))
            {
                return true;
            }
        }
        return false;
    }

    /************************
     * 判断传入的字符串是否为数字
     * 如果是数字，则返回true;否则返回false
     * 只要能Double.parseDouble(str)的都支持，比如:0000.89=0.89
     ************************/
    public static boolean isNumeric(String str)
    {
        Pattern pattern = Pattern.compile("([\\+\\-])?([0-9])+(.[0-9])?([0-9])*");
        Matcher isNum = pattern.matcher(str);

        if (!isNum.matches())
        {
            return false;
        }

        return true;
    }

    /**
     * <p>
     * 计算输入日期格式校验, 并且这个日期不能晚于今天.
     * </p>
     * 
     * @param strDate
     *            String
     * @return boolean
     */
    public static boolean validateDate(String strDate)
    {
        int yyyy = 0000;
        int mm = 00;
        int dd = 00;
        // 校验是否有非法字符
        if (!validateNumber(strDate))
        {
            return false;
        }

        if (strDate.indexOf("-") >= 0)
        {
            StringTokenizer token = new StringTokenizer(strDate, "-");
            int i = 0;
            while (token.hasMoreElements())
            {
                if (i == 0)
                {
                    yyyy = Integer.parseInt(token.nextToken());
                }
                if (i == 1)
                {
                    mm = Integer.parseInt(token.nextToken());
                }
                if (i == 2)
                {
                    dd = Integer.parseInt(token.nextToken());
                }
                i++;
            }
        }
        else
        {
            if (strDate.length() != 8)
            {
                return false;
            }
            yyyy = Integer.parseInt(strDate.substring(0, 4));
            mm = Integer.parseInt(strDate.substring(4, 6));
            dd = Integer.parseInt(strDate.substring(6, 8));
        }
        if (yyyy > MAX_YEAR || yyyy < MIN_YEAR)
        {
            return false;
        }
        if (mm > MAX_MONTH || mm < MIN_MONTH)
        {
            return false;
        }
        if (dd > MAX_DAY || dd < MIN_DAY)
        {
            return false;
        }
        if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && (dd == 31))
        {
            return false;
        }
        if (mm == 2)
        {
            // 校验闰年的情况下__日期格式
            boolean leap = (yyyy % 4 == 0 && (yyyy % 100 != 0 || yyyy % 400 == 0));
            if (dd > 29 || (dd == 29 && !leap))
            {
                return false;
            }
        }
        // 日期不能超过机器日期
        FDate myFDate = new FDate();
        Date validateDate = myFDate.getDate(strDate);
        Date now = new Date();
        if (calInterval(validateDate, now, "D") < 0)
        {
            return false;
        }

        return true;
    }

    /**
     * <p>
     * 校验是否存在非法字符
     * </p>
     * 
     * @param str
     *            String
     * @return boolean
     */
    public static boolean validateNumber(String str)
    {
        String tmp = null;
        for (int i = 0; i < str.length(); i++)
        {
            tmp = str.substring(i, i + 1);
            if (DATE_LIST.indexOf(tmp) == -1)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 格式化浮点型数据
     * 
     * @param dValue
     *            double
     * @return String
     */
    public static String format(double dValue)
    {
        // 格式化的四舍五入不准确
        // 准确的四舍五入
        double tValue = Arith.round(dValue, 2);
        return new DecimalFormat("0.00").format(tValue);
    }

    /**
     * 获取传入日期所属的标准交费期间之后至paytodate之前还有几期标准交费期间
     * @param CValiDate String 团体保单生效日期
     * @param EndDate String 团体保单终止日期
     * @param EdorValiDate String 个人生效日期   
     * @param PayIntv int 团体保单交费间隔 0,1,3,6   
     * @param PayToDate String 团体保单交至日期       
     * @return String[]
     */
    public static int getPayIntvNum(String CValiDate, String EndDate, String EdorValiDate, int PayIntv, String PayToDate)
    {
        int result = 0;
        if (PayIntv == 0) //趸交
        {
            result = 0;
        }
        else if (PayIntv > 0) //1,3,6期交
        {
            String MonDate[] = new String[2];
            MonDate = PubFun.getIntervalDate(CValiDate, EndDate, EdorValiDate, PayIntv);
            FDate tD = new FDate();
            if (MonDate[1] != null && !MonDate[1].equals("")
                    && tD.getDate(PayToDate).compareTo(tD.getDate(EndDate)) <= 0
                    && tD.getDate(PayToDate).compareTo(tD.getDate(MonDate[1])) >= 0)
            {
                int monthUnit = PubFun.calInterval2(MonDate[1], PayToDate, "M");//算出两个日期之间月份间隔
                int num = monthUnit / PayIntv;
                if (Arith.div(monthUnit, PayIntv) > num)
                {
                    num += 1;
                }
                result = num;
            }
        }
        return result;
    }

    //by gzh 20101224
    /**
     * 获取传入日期的上月信息，返回上月的第一天和最后一天
     * 
     * @param cDate
     *            String
     * @return String[]
     */
    public static String[] getPrevMonth(String cDate)
    {
        // 返回日期的格式化样式
        String tFormatDate = "yyyy-MM-dd";
        // 创建格式化对象
        SimpleDateFormat tSimpleDateFormat = new SimpleDateFormat(tFormatDate);
        FDate tFDate = new FDate();
        Date CurDate = tFDate.getDate(cDate);
        GregorianCalendar tCalendar = new GregorianCalendar();
        // 将传入的日期置入
        tCalendar.setTime(CurDate);
        // 获取月份信息
        int tMonth = tCalendar.get(Calendar.MONTH);
        // 将月份减一，并置回
        tCalendar.set(Calendar.MONTH, tMonth - 1);
        return PubFun.calFLDate(tSimpleDateFormat.format(tCalendar.getTime()));
    }

    /**
     * 获取传入日期后的度量纬日期
     * 
     * @param cDate
     *            String 传入的日期
     * @param cInterval
     *            int 日期的间隔
     * @param cUnit
     *            String 日期计算的纬度
     * @return String
     */
    public static String getLastDate(String cDate, int cInterval, String cUnit)
    {
        // 返回日期的格式化样式
        String tFormatDate = "yyyy-MM-dd";
        // 创建格式化对象
        SimpleDateFormat tSimpleDateFormat = new SimpleDateFormat(tFormatDate);
        // 转换日期字符串为日期类型
        FDate tFDate = new FDate();
        Date CurDate = tFDate.getDate(cDate);

        GregorianCalendar tCalendar = new GregorianCalendar();
        // 将传入的日期置入
        tCalendar.setTime(CurDate);

        if (cUnit.equals("D"))
        {
            // 取出日信息，需要同时取日月
            int tDay = tCalendar.get(Calendar.DAY_OF_MONTH);
            // 将日加上间隔，置回
            tCalendar.set(Calendar.DAY_OF_MONTH, tDay + cInterval);
            // 返回
            return tSimpleDateFormat.format(tCalendar.getTime());
        }
        else if (cUnit.equals("M"))
        {
            // 获取月份信息
            int tMonth = tCalendar.get(Calendar.MONTH);
            // 将月份加上间隔，置回
            tCalendar.set(Calendar.MONTH, tMonth + cInterval);
            // 返回
            return tSimpleDateFormat.format(tCalendar.getTime());
        }
        else if (cUnit.equals("Y"))
        {
            // 获取年信息
            int tYear = tCalendar.get(Calendar.YEAR);
            // 将年加上间隔，置回
            tCalendar.set(Calendar.YEAR, tYear + cInterval);
            // 返回
            return tSimpleDateFormat.format(tCalendar.getTime());
        }
        else
        {
            return "";
        }
    }

    // by gzh 20110106
    /**
     * 获取传入日期所在自然月的天数 add by zhangtao 2007-04-28
     * @param sDate 传入日期
     * @return
     */
    public static int monthLength(String sDate)
    {
        FDate fDate = new FDate();
        Date startDate = fDate.getDate(sDate);
        if (fDate.mErrors.needDealError())
        {
            return 0;
        }

        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        int sYears = sCalendar.get(Calendar.YEAR);
        int sMonths = sCalendar.get(Calendar.MONTH);

        return monthLength(sYears, sMonths + 1);
    }

    /**
     * 获取传入年份、月份的自然月天数 add by zhangtao 2007-04-28
     * @param year 年份
     * @param month 月份 1-12
     * @return
     */
    public static int monthLength(int year, int month)
    {
        int LEAP_MONTH_LENGTH[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        int MONTH_LENGTH[] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        return isLeapYear(year) ? MONTH_LENGTH[month - 1] : LEAP_MONTH_LENGTH[month - 1];
    }

    /**
     * 判断是否为闰年
     * XinYQ added on 2006-09-25
     */
    public static boolean isLeapYear(int nYear)
    {
        boolean ResultLeap = false;
        ResultLeap = (nYear % 400 == 0) | (nYear % 100 != 0) & (nYear % 4 == 0);
        return ResultLeap;
    }

    /**
     * 判断是否为闰年
     * frost added on 2007-10-08
     */
    public static boolean isLeapYear(String sDate)
    {
        FDate fDate = new FDate();
        Date startDate = fDate.getDate(sDate);
        if (fDate.mErrors.needDealError())
        {
            return false;
        }

        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        int sYears = sCalendar.get(Calendar.YEAR);
        return isLeapYear(sYears);
    }
    
    /**
     * 
     * @param grpcontno 团单号
     * @param edorvalidate 保全生效日
     * @return  
     */
    public static boolean isDoBQ(String grpcontno,String edorvalidate){
    	boolean flag = false;
    	//找到该单最小的paydate
    	String payDateString = "select min(paydate) from lcinsureacctrace where grpcontno='"+grpcontno+"'";
    	ExeSQL tExeSQL = new ExeSQL();
        String dateString = tExeSQL.getOneValue(payDateString);
        
    	FDate fDate = new FDate();
    	//最小paydate时间
        Date startDate = fDate.getDate(dateString);
        //生效日时间
        Date endDate = fDate.getDate(edorvalidate);

        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        int sYear = sCalendar.get(Calendar.YEAR);
        int sMonth = sCalendar.get(Calendar.MONTH);
        
        GregorianCalendar eCalendar = new GregorianCalendar();
        eCalendar.setTime(endDate);
        int eYear = eCalendar.get(Calendar.YEAR);
        int eMonth = eCalendar.get(Calendar.MONTH);
        
        if(sYear==eYear && sMonth==eMonth){
        	//新契约，不需要检验，直接可以申请保全。
        	flag = true;
        }else {
        	//校验上月必须月结 ，才可以建立保全
            String mEdorValidate = edorvalidate.substring(0, 7) + "-01";
            String sql = "select count(*) from lcinsureacctrace a  where grpcontno='"+grpcontno+"' and exists (select 1 from lcinsureacc where grpcontno=a.grpcontno and polno=a.polno and acctype='001' and insuaccno=a.insuaccno ) and paydate =date('"+mEdorValidate+"')  and MoneyType='MF' and othertype='6' ";
                
            ExeSQL eExeSQL = new ExeSQL();
            String resultString = eExeSQL.getOneValue(sql);
            if(resultString.equals("0")){
               flag = false;
            }else {
               flag = true;
            }
        }
        return flag;
    }
    /**
     * 通过所传证件类型、证件号、出生日期、性别校验。
     * @param IdNo String
     * @return String
     */
    public static String CheckIDNo(String IDType,String IDNo,String birthday,String sex)
    {
    	String strReturn="";
    	//身份证或户口本进行校验
    	if(!"0".equals(IDType)&&!"5".equals(IDType)&&!"a".equals(IDType)&&!"b".equals(IDType)){
    		return strReturn;
    	}
    	if("0".equals(IDType)||"5".equals(IDType)){
    	//身份证长度校验
    	if(IDNo.length()!=15&&IDNo.length()!=18){
    		strReturn = "身份证号位数错误";
    	    return strReturn;
    	}
    	//18位身份证除最后一位可以为字母，其余只能是数字，且字母只能为大写英文X
    	if(IDNo.length()==18){
//    		String tSql = "select 1 from ldcode where codetype='idnolast' and code='"+IDNo.substring(17,18)+"'";
//    		ExeSQL eExeSQL = new ExeSQL();
//            String resultString = eExeSQL.getOneValue(tSql);
//            if(resultString==null||"".equals(resultString)){
//            	strReturn = "18位身份证号最后一位错误，只能为数字或大写字母X";
//        		return strReturn;
//            }
    		if(!(IDNo.substring(17,18)).matches("^[\\d|X]$")){
    		  	strReturn = "18位身份证号最后一位错误，只能为数字或大写字母X";
    		    return strReturn;	

            }
            if(!(IDNo.substring(0,IDNo.length()-1)).matches("[0-9]{1,}")){
    		  	strReturn = "18位身份证号除最后一位其他必须为数字";
    		    return strReturn;	

            }
    	}
    	//15位身份证必须只能为数字
    	if(IDNo.length()==15){
    		  if(!(IDNo.substring(0,IDNo.length())).matches("[0-9]{1,}")){
    			  	strReturn = "15位身份证号必须为数字";
    			    return strReturn;	
    		  }
    	}
    	//校验前身份证前两位是否为有效省份代码
//    	String tSql1 = "select 1 from ldcode where codetype='idnoprovince' and code='"+IDNo.substring(0,2)+"'";
//		ExeSQL eExeSQL = new ExeSQL();
//        String resultString = eExeSQL.getOneValue(tSql1);
//        if(resultString==null||"".equals(resultString)){
//        	strReturn = "身份证号前两位不是有效的省份代码";
//       	  	return strReturn;
//        }
    	if(!Arrays.asList(IDNOPROVINCES).contains(IDNo.substring(0,2))){
    		strReturn = "身份证号前两位不是有效的省份代码";
   	  		return strReturn;
    	}
    	}
    	if("a".equals(IDType)||"b".equals(IDType)){
    		//港澳台居民居住证长度校验
        	if(IDNo.length()!=18||!IDNo.matches("^[\\d|[A-Z]]*$")){
        		strReturn = "证件号码必须为18位并且只能包含数字和大写字母";
        	    return strReturn;
        	}
        	if("a".equals(IDType)){
        		if(!"810000".equals(IDNo.substring(0,6))&&!"820000".equals(IDNo.substring(0,6))){
        			strReturn = "证件号码前6位不是有效的地区代码";
            	    return strReturn;
        		}
        	}else if("b".equals(IDType)){
        		if(!"830000".equals(IDNo.substring(0,6))){
        			strReturn = "证件号码前6位不是有效的地区代码";
            	    return strReturn;
        		}
        	}
    	}
    	
        //不传出生日期字段获取出生日期
        if(birthday==null||"".equals(birthday)){
        	birthday=getBirthdayFromId(IDNo);
        	System.out.println(birthday);
        }
        //传出生日是否为正确的日期YYYY-MM-DD格式
        if(!checkDateForm(birthday)){
        	strReturn = "出生日期格式错误";
        	return strReturn;	
        }
        //校验生日与身份证是否一致
        String tbirthday=getBirthdayFromId(IDNo);
        if(!tbirthday.equals(birthday)){
        	if("0".equals(IDType)||"5".equals(IDType)){
        		strReturn = "出生日期与身份证号的信息不一致";
        	}
        	if("a".equals(IDType)||"b".equals(IDType)){
        		strReturn = "出生日期与证件号码的信息不一致";
        	}
        	
        	return strReturn;
        }
        //传性别为空字段获取性别
        if(sex==null||"".equals(sex)){
        	sex = getSexFromId(IDNo);
        	System.out.println(sex);
        }
        //校验所传性别
        if("0".equals(sex)||"1".equals(sex)){
        	String tsex = getSexFromId(IDNo);
        	if(!tsex.equals(sex)){
        		if("0".equals(IDType)||"5".equals(IDType)){
        			strReturn = "性别与身份证号的信息不一致";
        		}
        		if("a".equals(IDType)||"b".equals(IDType)){
        			strReturn = "性别与证件号码的信息不一致";
        		}
        		return strReturn;
        	}
        }else{
        	strReturn = "性别不明确";
        	return strReturn;
        }
    	return strReturn;
    }
    /**
     * 通过传固定电话校验
     * @param fixPhone
     * @return
     */
        public static String CheckFixPhone(String fixPhone){
        	String strReturn="";
        	if(!(fixPhone.matches("[0-9\\-\\(\\)]*"))){
    		  	strReturn = "固定电话仅允许包含数字、英文小括号和“-”";
    		    return strReturn;	
    	  }
    		return strReturn;
        	
        }
     /**
      * 通过传手机号码校验
      * @param Phone
      * @return
      */
        public static String CheckPhone(String Phone){
        	String strReturn="";
               
        	if(!(Phone.matches("[0-9]{11}"))){
    		  	strReturn = "手机号码只能是11位数字";
    		    return strReturn;	
    	  }
    		return strReturn;
        	
        }
        /**
         * 通过传邮箱校验
         * @param Phone
         * @return
         */
        public static String CheckEmail(String email){
        	String strReturn="";
            String reg1="@";
            String reg2=".";
            //@的下标
            Integer index1=email.indexOf(reg1);
            //.的下标
            Integer index2=email.indexOf(reg2);
            System.out.println(index1);
        	if(index1==-1||index2==-1||index1>index2){
    		  	strReturn = "邮箱中必须包含@和.且.在@之后";
    		    return strReturn;	
    	  }
    		return strReturn;
        	
        }
        /**
         * 通过传印刷号校验
         * @param Phone
         * @return
         */
        public static String CheckPrtno(String email){
        	String strReturn="";
            
        	if(!(email.matches("[A-Za-z0-9]*"))){
    		  	strReturn = "印刷号必须为字母或数字";
    		    return strReturn;	
    	  }
    		return strReturn;
        }
}
