package com.sinosoft.lis.pubfun;

import java.util.*;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
//import com.sinosoft.lis.message.SendMessage;
import com.sinosoft.lis.bl.LPInsureAccClassBL;
import com.sinosoft.lis.bl.LPInsureAccClassFeeBL;

/**
 * <p>Title: 万能险保单账户结算 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author YangYalin
 * @version 1.3
 * @CreateDate：2007-04-28
 */
public class InsuAccBala
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGI = null;
    private LCInsureAccSchema mLCInsureAccSchema = null;
    private LPInsureAccSchema mLPInsureAccSchema = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mBalanceSeq = null;
    private String mStartDate = null;
    private String mEndDate = null;
    private int mPolYear = 1; //保单年度
    private int mPolMonth = 1; //保单月度
    

    private Reflections ref = new Reflections();
    private FDate tFDate = new FDate();
    private String mRateType = "";

    public MMap map = new MMap();
    
    private LCPolSchema mLCPolSchema = null;

    public InsuAccBala()
    {
    }

    /**
     * submitData
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * submitData
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitEdorCTData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealEdorCTData())
        {
            return false;
        }

        return true;
    }

    /**
     * 业务处理不需要考虑保证利率，业务提供的利率不小于保证利率
     * @return boolean
     */
    public boolean dealData()
    {
        //查询出当前账户最后结算日期比已公布利率日期晚的账户，由于理赔或保全等原因，一个账户可能需要结算多次
        LMInsuAccRateSet tLMInsuAccRateSet
                = getInsuAccRateUnBalance(mLCInsureAccSchema);
        if (tLMInsuAccRateSet == null)
        {
            return false;
        }
        System.out.println(tLMInsuAccRateSet.size());
        for (int m = 1; m <= tLMInsuAccRateSet.size(); m++)
        {
            mLCInsureAccSchema = getCurrentAcc();
            if (mLCInsureAccSchema == null)
            {
                return false;
            }

            System.out.println("处理被保人" + mLCInsureAccSchema.getInsuredNo()
                               + ", 险种" + mLCInsureAccSchema.getRiskCode()
                               + ", 结算结算起始日期"
                               + tLMInsuAccRateSet.get(m).getBalaDate() + "的结算");
            mBalanceSeq = null; //由于月度不同，需要重新生成结算序号

            //查询帐户子表信息
            LCInsureAccClassSet tLCInsureAccClassSet = getAccClass(
                    mLCInsureAccSchema);
            if (tLCInsureAccClassSet == null)
            {
                return false;
            }

            double tAccValue = 0;
            double tAccValueGurat = 0; //保证账户收益
            LCInsureAccClassSchema schemaAV = null; //处理后的账户分类价值
            LCInsureAccTraceSchema schemaTraceOne = null; //本次产生的任一个轨迹

            MMap tMMap = new MMap();
            
//        	20091217 zhanggm 避免同一万能保单在一次月结程序中结算2遍，插入控制字段--险种号+结算流水号
            String sql = "select max(BalaCount + 1) from LCInsureAccBalance "
                + "where PolNo = '"
                + mLCInsureAccSchema.getPolNo() + "' "
                + "   and InsuAccNo = '"
                + mLCInsureAccSchema.getInsuAccNo() + "' ";
            String maxBalaCount = new ExeSQL().getOneValue(sql);
        	MMap tCekMap = null;
        	tCekMap = lockBalaCount(mLCInsureAccSchema.getPolNo()+maxBalaCount);
            if (tCekMap == null)
            {
                return false;
            }
            tMMap.add(tCekMap);
//    --------------------------
            
            for (int n = 1; n <= tLCInsureAccClassSet.size(); n++)
            {
                LCInsureAccClassSchema tLCInsureAccClassSchema
                        = tLCInsureAccClassSet.get(n);
                
                mStartDate = tLCInsureAccClassSchema.getBalaDate();
                mEndDate = tLMInsuAccRateSet.get(m).getBalaDate();
                System.out.println(mStartDate);
                System.out.println(mEndDate);
                if (!calPolYearMonth())
                {
                    return false;
                }

                System.out.println("计算账户价值");
                MMap tMMapAV = dealOneAccClass(tLCInsureAccClassSchema,
                                               "1", "1");
                if (tMMapAV == null)
                {
                    return false;
                }
                tMMap.add(tMMapAV);
                schemaAV = (LCInsureAccClassSchema) tMMapAV
                           .getObjectByObjectName("LCInsureAccClassSchema", 0);
                schemaTraceOne = (LCInsureAccTraceSchema) tMMapAV
                                 .getObjectByObjectName(
                                         "LCInsureAccTraceSchema", 0);

                if (schemaAV == null)
                {
                    mErrors.addOneError("没有计算出账户价值");
                    return false;
                }
                tAccValue += schemaAV.getInsuAccBala();

                System.out.println("计算账户保证价值");
                MMap tMMapGurat = dealOneAccClass(tLCInsureAccClassSchema,
                                                  "2", "2");
                if (tMMapGurat == null)
                {
                    mErrors.addOneError("计算保证收益出错");
                    return false;
                }
                LCInsureAccClassSchema schema
                        = (LCInsureAccClassSchema) tMMapGurat
                          .getObjectByObjectName("LCInsureAccClassSchema", 0);
                if (schema == null)
                {
                    mErrors.addOneError("没有计算出保证账户价值");
                    return false;
                }
                tAccValueGurat += schema.getInsuAccBala();
            }

            //生成结算信息
            LCInsureAccBalanceSchema tLCInsureAccBalanceSchema
                    = getLCInsureAccBalance(tLMInsuAccRateSet.get(m),
                                            tAccValue, tAccValueGurat);
            if (tLCInsureAccBalanceSchema == null)
            {
                return false;
            }
            tMMap.put(tLCInsureAccBalanceSchema, SysConst.INSERT);

            //处理年终保证收益补差
            MMap tMMapVD = dealAccountValueDif(tLCInsureAccBalanceSchema,
                                               schemaTraceOne);
            System.out.println(tLCInsureAccBalanceSchema.getInsuAccBalaAfter());
            if(tLCInsureAccBalanceSchema.getInsuAccBalaAfter()<0)
            {
            	return false;
            }
            
            if (tMMapVD == null)
            {
                return false;
            }
            tMMap.add(tMMapVD);

            tMMap.add(updateAccInfo());

            VData data = new VData();
            data.add(tMMap);
            PubSubmit tSubmit = new PubSubmit();
            if (!tSubmit.submitData(data, ""))
            {
                buildError("InsureAccSubmit", "数据提交失败");
                return false;
            }

//            sendMessage(tLCInsureAccBalanceSchema);
        }

        return true;
    }

    /**
     *
     * @return boolean
     */
    public boolean dealEdorCTData()
    {
        if (this.mEndDate == null || mEndDate.equals(""))
        {
            buildError("dealEdorCTData", "万能解约没有传送结算终止日期");
            return false;
        }
        if (mLPInsureAccSchema == null)
        {
            buildError("dealEdorCTData", "万能解约结算没有传送帐户信息");
            return false;
        }

        System.out.println("处理被保人" + mLPInsureAccSchema.getInsuredNo()
                           + ", 险种" + mLPInsureAccSchema.getRiskCode()
                           + ", 结算结算起始日期"
                           + this.mEndDate + "的结算");
//        mBalanceSeq = null; //万能解约退保时存储保全工单号 modified by huxl @20080411

        //查询帐户子表信息
        LPInsureAccClassBL tLPInsureAccClassBL = new LPInsureAccClassBL();
        LPInsureAccClassSet tLPInsureAccClassSet = tLPInsureAccClassBL.
                queryAllLPInsureAccClass(mLPEdorItemSchema);
        if (tLPInsureAccClassSet == null)
        {
            return false;
        }

        double tAccValue = 0;
        LPInsureAccClassSchema schemaAV = null; //处理后的账户分类价值

        MMap tMMap = new MMap();
        for (int n = 1; n <= tLPInsureAccClassSet.size(); n++)
        {
            LPInsureAccClassSchema tLPInsureAccClassSchema
                    = tLPInsureAccClassSet.get(n);

            mStartDate = tLPInsureAccClassSchema.getBalaDate();
            if (!calPolYearMonthP())
            {
                return false;
            }

            System.out.println("按照保证利率结算账户价值");
            MMap tMMapAV = null;
            
            if(("332601".equals(mLPInsureAccSchema.getRiskCode())||"332901".equals(mLPInsureAccSchema.getRiskCode())
            				||"333001".equals(mLPInsureAccSchema.getRiskCode())||"333201".equals(mLPInsureAccSchema.getRiskCode())))
            {
            	LCPolSchema tLCPolSchema = null;
            	if(mLPInsureAccSchema.getPolNo()!= null && !"".equals(mLPInsureAccSchema.getPolNo()))
                {
                	LCPolDB tLCPolDB = new LCPolDB();
                    tLCPolDB.setPolNo(mLPInsureAccSchema.getPolNo());
                    if(tLCPolDB.getInfo())
                    {
                    	tLCPolSchema = tLCPolDB.getSchema();
                    }
                }
            	String checkDate = PubFun.calDate(tLCPolSchema.getCValiDate(), 1, "Y", null);
            	if((tFDate.getDate(checkDate).
 						compareTo(tFDate.getDate(PubFun.getCurrentDate()))<=0)
                		){
            		
            		mRateType = "3";
                	tMMapAV = dealOneAccClassP(tLPInsureAccClassSchema,
                            "1", "3");
                	
            		
            	}
            	 else{
                 	
                 	tMMapAV = dealOneAccClassP(tLPInsureAccClassSchema,
                             "1", "2");
                 }
            	
            }else if("334601".equals(mLPInsureAccSchema.getRiskCode())){//康利B款生效月之后均使用上一期公布的利率结算
            	String strMonth="select 1 from lcpol where polno='"+mLPInsureAccSchema.getPolNo()+"' "
            			+ " and year(cvalidate)=year(date('"+PubFun.getCurrentDate()+"')) "
            			+ " and month(cvalidate)=month(date('"+PubFun.getCurrentDate()+"')) ";
            	String cMonth = new ExeSQL().getOneValue(strMonth);
            	if(null!=cMonth && !"".equals(cMonth)){
            		tMMapAV = dealOneAccClassP(tLPInsureAccClassSchema,"1", "2");
            	}else{
            		mRateType = "3";
                	tMMapAV = dealOneAccClassP(tLPInsureAccClassSchema,"1", "3");
            	}
            }else{
            	
            	tMMapAV = dealOneAccClassP(tLPInsureAccClassSchema,
                        "1", "2");
            }
            
            if (tMMapAV == null)
            {
                return false;
            }
            tMMap.add(tMMapAV);
            schemaAV = (LPInsureAccClassSchema) tMMapAV
                       .getObjectByObjectName("LPInsureAccClassSchema", 0);
            if (schemaAV == null)
            {
                mErrors.addOneError("没有计算出保证账户价值");
                return false;
            }
            tAccValue += schemaAV.getInsuAccBala();
            System.out.println(tAccValue);
            /**add by hhw 20170401处理税优录入的税优优惠额度*/
            String str="select 1 from lmriskapp where riskcode='" +tLPInsureAccClassSchema.getRiskCode()+ "' and taxoptimal='Y'";
            String sy=new ExeSQL().getOneValue(str);
            if(null != sy && !"".equals(sy)){
                String tSql="select edorvalue from LPEdorEspecialData where edorno='" +mLPEdorItemSchema.getEdorNo()+ "' and detailtype='TAXMONEY'";
                System.out.println(tSql);
                String bTaxMoney=new ExeSQL().getOneValue(tSql); 
                System.out.println(bTaxMoney);
                double taxMoney=Double.parseDouble(bTaxMoney);
                if(tAccValue < taxMoney){
                    mErrors.addOneError("补交税收优惠额度大于账户余额与未满期净风险保费之和，请重新录入补交税收优惠额度。");
                    return false;
                }
            }
            //20160408 by hehongwei 个险万能产品解约，账户价值为负时提示阻断
            if (tAccValue < 0)
            {
                mErrors.addOneError("保单账户余额不足以抵扣风险保费，请将保全生效日期选为最后一次月结日期。");
                return false;
            }
            
        }

        //生成结算信息，这里由于不再计算保证帐户价值，所以直接置InsuAccBalaGurat ＝ InsuAccBalaAfter,
        //这里由理算时直接插入记录，所以重复理算时一定要删除本条记录。 modified by huxl @20080411
        LMInsuAccRateSchema tLMInsuAccRateSchema = new LMInsuAccRateSchema();
        tLMInsuAccRateSchema.setBalaDate(this.mEndDate);
        LCInsureAccBalanceSchema tLCInsureAccBalanceSchema
                = getLCInsureAccBalanceP(tLMInsuAccRateSchema,
                                         tAccValue, tAccValue);
        if (tLCInsureAccBalanceSchema == null)
        {
            return false;
        }
        tMMap.put(tLCInsureAccBalanceSchema, SysConst.INSERT);
        tMMap.add(updateAccInfoP());

        VData data = new VData();
        data.add(tMMap);
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(data, ""))
        {
            buildError("InsureAccSubmit", "数据提交失败");
            return false;
        }

        return true;
    }

    /**
     * sendMessage
     *
     * @param cLCInsureAccSchema LCInsureAccSchema
     */
//    private void sendMessage(LCInsureAccBalanceSchema cLCInsureAccBalanceSchema)
//    {
//        String sql = "select b.AppntName, b.AppntSex, a.Mobile "
//                     + "from LCAddress a, LCAppnt b "
//                     + "where a.CustomerNo = b.AppntNo "
//                     + "   and a.AddressNo = b.AddressNo "
//                     + "   and b.ContNo = '"
//                     + mLCInsureAccSchema.getContNo() + "' ";
//        SSRS tSSRS = new ExeSQL().execSQL(sql);
//        if (tSSRS.getMaxRow() == 0)
//        {
//            System.out.println("查询投保人信息出错");
//            mErrors.addOneError("查询投保人信息出错");
//            return;
//        }
//
//        String info = tSSRS.GetText(1, 1) + "先生/女士：已将您的"
//                      + mLCInsureAccSchema.getContNo() + "保单（万能险）";
//
//        String tStartDate = PubFun.calDate(cLCInsureAccBalanceSchema.
//                                           getDueBalaDate(),
//                                           -1, "D", null);
//        String[] dateArr = tStartDate.split("-");
//        info += dateArr[0] + "年" + dateArr[1] + "月利息";
//
//        sql =
//                "select MoneyType, nvl(abs(sum(Money)), 0) from LCInsureAccTrace "
//                + "where OtherType = '6' "
//                + "   and OtherNo = '"
//                + cLCInsureAccBalanceSchema.getSequenceNo() + "' "
//                + "   and ContNo = '" + mLCInsureAccSchema.getContNo() + "' "
//                + "   and MoneyType in('LX', 'MF', 'RP') "
//                + "group by MoneyType ";
//        tSSRS = new ExeSQL().execSQL(sql);
//        if (tSSRS.getMaxRow() == 0)
//        {
//            System.out.println("月结算成功，但查询结算信息失败");
//            mErrors.addOneError("月结算成功，但查询结算信息失败");
//            return;
//        }
//        try
//        {
//
//            info += tSSRS.GetText(1, 2) + "元计入保单账户，扣除保单管理费"
//                    + tSSRS.GetText(2, 2) + "元、风险保费" + tSSRS.GetText(3, 2)
//                    + "元，账户当前价值"
//                    + CommonBL.bigDoubleToCommonString(
//                            cLCInsureAccBalanceSchema.getInsuAccBalaAfter(),
//                            "0.00")
//                    + "元，尽请留意。人保健康敬致，服务热线95591。";
////            PubFun.sendMessage(tSSRS.GetText(1, 3), info);
//            String[] Receivers = new String[1];
//            Receivers[0] = tSSRS.GetText(1, 3);
//            TransferData tTransferData = new TransferData();
//            tTransferData.setNameAndValue("ServiceType", "cbs");
//            tTransferData.setNameAndValue("Receiver", Receivers);
//            tTransferData.setNameAndValue("Contents", info);
//            String tCurrentDate = PubFun.getCurrentDate();
//            String tTomorrow = PubFun.calDate(tCurrentDate, 1, "D", null);
//            tTransferData.setNameAndValue("StartDate", tTomorrow); //从第二天早上九点开始发送
//            tTransferData.setNameAndValue("StartTime", "9:00");
//            tTransferData.setNameAndValue("EndDate", tTomorrow);
//            tTransferData.setNameAndValue("EndTime", "21:00");
//            tTransferData.setNameAndValue("Organization", "86000000");
//            VData tVData = new VData();
//            tVData.add(tTransferData);
//            SendMessage tSendMessage = new SendMessage();
//            tSendMessage.submitData(tVData);
//
//        } catch (Exception ex)
//        {
//            System.out.println("月结算成功，但发送短信失败");
//            mErrors.addOneError("月结算成功，但发送短信失败");
//            ex.printStackTrace();
//        }
//    }
    
    
    private boolean checkCont(String tPolNo)
    {
    	LCPolSchema tLCPolSchema = null;
    	if(tPolNo != null&&!"".equals(tPolNo))
        {
        	LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tPolNo);
            if(tLCPolDB.getInfo())
            {
            	tLCPolSchema = tLCPolDB.getSchema();
            }
        }
    	else{
    		
    		mErrors.addOneError("校验利率使用规则时传入的信息不全");
            return false;
    	}
    	String checkDate = PubFun.calDate(tLCPolSchema.getCValiDate(), 1, "Y", null);
    	if((!"332601".equals(tLCPolSchema.getRiskCode())&&!"332901".equals(tLCPolSchema.getRiskCode())
 				&&!"333001".equals(tLCPolSchema.getRiskCode())&&!"333201".equals(tLCPolSchema.getRiskCode())
 				&&!"334601".equals(tLCPolSchema.getRiskCode()))){//康利B款
    		
    		mErrors.addOneError("非万能F/G/H险种，不能使用非保证利率进行退保利息计算");
            return false;
    		
    	}
    	else{
    		if("334601".equals(tLCPolSchema.getRiskCode())){
    			String strSql="select 1 from dual where year(date('"+PubFun.getCurrentDate()+"'))=year(date('"+tLCPolSchema.getCValiDate()+"')) "
    					+ " and month(date('"+PubFun.getCurrentDate()+"'))=month(date('"+tLCPolSchema.getCValiDate()+"')) ";
    			String chDate=new ExeSQL().getOneValue(strSql);
    			if(null!=chDate && !"".equals(chDate)){
    				mErrors.addOneError("康利万能B险种，当前日期还处在生效当月，不能使用非保证利率进行退保利息计算");
        			return false;
    			}
    		}else{
    			if(tFDate.getDate(checkDate).
				compareTo(tFDate.getDate(PubFun.getCurrentDate()))>0){
    				mErrors.addOneError("万能F/G/H险种，当前日期还处在第一保单年度，不能使用非保证利率进行退保利息计算");
        			return false;
    			}
    		}
    		
    	}

        return true;
    }

    /**
     * calPolYearMonth
     */
    private boolean calPolYearMonth()
    {
    	//针对税优产品计算保单年度，生效日应取首期生效日期
    	ExeSQL tExeSQL = new ExeSQL();
    	String getYearMonthSql = "";
    	String hasResult = tExeSQL.getOneValue("select 1 from lmriskapp where riskcode = '"+mLCInsureAccSchema.getRiskCode()+"' and taxoptimal = 'Y'");
    	if("1".equals(hasResult)){
    		// 首期承保生效日
    		String CValiDateSQL = "select min(temp.cvalidate) "
    			+ "from (select cvalidate cvalidate "
    			+ "from lccont "
    			+ "where conttype='1' and appflag='1' and contno='"
    			+ mLCInsureAccSchema.getContNo()
    			+ "' "
    			+ "union all select cvalidate cvalidate "
    			+ "from lbcont where conttype='1' and appflag='1' and edorno like 'xb%' and contno "
    			+ "in (select newcontno from lcrnewstatelog where contno='"
    			+ mLCInsureAccSchema.getContNo() + "' and state='6')) temp "
    			+ "with ur";
    		String CValiDate = tExeSQL.getOneValue(CValiDateSQL);
    		if (null == CValiDate || "".equals(CValiDate)) {
    			mErrors.addOneError("获取税优保单生效日期出错");
    			return false;
    		}
    		getYearMonthSql = "select year(date('"+mStartDate+"') - date('"+CValiDate+"')) + 1,month(date('"+mStartDate+"') - date('"+CValiDate+"')) + 1 from dual where 1=1";
    		
    	}else{
    		getYearMonthSql = "select year(date('" + mStartDate +
    		"') - CValidate) + 1, "
    		+ "   month(date('" + mStartDate + "') - CValidate) + 1 "
    		+ "from LCPol "
    		+ "where PolNo = '" + mLCInsureAccSchema.getPolNo() + "' ";
    	}
        System.out.println(getYearMonthSql);
        SSRS tSSRS = tExeSQL.execSQL(getYearMonthSql);
        if (tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        mPolYear = Integer.parseInt(tSSRS.GetText(1, 1));
        mPolMonth = Integer.parseInt(tSSRS.GetText(1, 2));

        return true;
    }

    /**
     * calPolYearMonth
     */
    private boolean calPolYearMonthP()
    {
        String sql = "select year(date('" + mStartDate +
                     "') - CValidate) + 1, "
                     + "   month(date('" + mStartDate + "') - CValidate) + 1 "
                     + "from LCPol "
                     + "where PolNo = '" + mLPInsureAccSchema.getPolNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        mPolYear = Integer.parseInt(tSSRS.GetText(1, 1));
        mPolMonth = Integer.parseInt(tSSRS.GetText(1, 2));

        return true;
    }

    /**
     * calPolYearMonth
     */
    private boolean calContinuedbonusTime()
    {
        String sql = "select year(date('" + mStartDate +
                     "') - CValidate) + 1, "
                     + "   month(date('" + mStartDate + "') - CValidate) + 1 "
                     + "from LCPol "
                     + "where PolNo = '" + mLPInsureAccSchema.getPolNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        mPolYear = Integer.parseInt(tSSRS.GetText(1, 1));
        mPolMonth = Integer.parseInt(tSSRS.GetText(1, 2));

        return true;
    }

    /**
     * dealAccountValueDif
     * 进行保证账户价值补差，按年补差
     * @return MMap
     */
    private MMap dealAccountValueDif(LCInsureAccBalanceSchema schema,
                                     LCInsureAccTraceSchema
                                     cLCInsureAccTraceSchema)
    {
        MMap tMMap = new MMap();

        if (12 != schema.getPolMonth())
        {
            return tMMap;
        }

//        String sql = "select sum(InsuAccBalaAfter - InsuAccBalaGurat) "
//                     + "from LCInsureAccBalance "
//                     + "where PolNo = '" + schema.getPolNo() + "' "
//                     + "   and InsuAccNo = '" + schema.getInsuAccNo() + "' "
//                     + "   and PolYear = " + schema.getPolYear() + " ";
//        String difStr = new ExeSQL().getOneValue(sql);
//        if(difStr == null || difStr.equals("") || difStr.equals("null"))
//        {
//            mErrors.addOneError("计算保证收益差出错");
//            return null;
//        }
//
//        double dif = Double.parseDouble(difStr);
        double dif = schema.getInsuAccBalaAfter() - schema.getInsuAccBalaGurat();
        if (dif >= 0)
        {
            return tMMap;
        }

        dif = Math.abs(PubFun.setPrecision(dif, "0.00"));
        LCInsureAccTraceSchema accTrace = new LCInsureAccTraceSchema();
        ref.transFields(accTrace, cLCInsureAccTraceSchema);
        accTrace.setMoney(dif);
        accTrace.setMoneyType("VD"); //年度收益补差

        //结算信息也做相应的调整：保证收益、账户余额
        schema.setInsuAccBalaAfter(schema.getInsuAccBalaAfter() + dif);
        schema.setInsuAccBalaGurat(schema.getInsuAccBalaAfter());

        String tLimit = PubFun.getNoLimit(accTrace.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        accTrace.setSerialNo(serNo);
        tMMap.put(accTrace, SysConst.INSERT);

        String sql = "update LCInsureAccClass a "
                     + "set InsuAccBala = InsuAccBala + " + dif + ", "
                     + "    LastAccBala = InsuAccBala + " + dif + " "
                     + "where PolNo = '" + accTrace.getPolNo() + "' "
                     + "   and InsuAccNo = '" + accTrace.getInsuAccNo() + "' "
                     + "   and PayPlanCode = '" + accTrace.getPayPlanCode() +
                     "' ";
        tMMap.put(sql, SysConst.UPDATE);

        return tMMap;
    }

    /**
     * updateAccInfo
     * 更新总账户信息
     * @return MMap
     */
    private MMap updateAccInfo()
    {
        MMap tMMap = new MMap();

        String sql = "update LCInsureAccClass a "
                     + "set (InsuAccBala, LastAccBala) "
                     + "   =(select sum(Money), sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo) "
                     + "where PolNo = '"
                     + mLCInsureAccSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        //更新账户信息
        sql = "update LCInsureAcc a "
              + "set (InsuAccBala, LastAccBala, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(InsuAccBala), sum(LastAccBala), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LCInsureAccClass where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + mLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        
        //按轨迹更新账户管理费信息
        sql = "update LCInsureAccClassFee a " +
              "set Fee =(select sum(Fee) from LCInsureAccFeeTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo ) "
              + "where PolNo = '"
              + mLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        //更新账户管理费信息
        sql = "update LCInsureAccFee a " +
              "set (Fee, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(Fee), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LCInsureAccClassFee where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + mLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        return tMMap;
    }

    /**
     * updateAccInfo
     * 更新总账户信息
     * @return MMap
     */
    private MMap updateAccInfoP()
    {
        MMap tMMap = new MMap();
        tMMap.put(mLPInsureAccSchema, SysConst.INSERT);

        LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
        tLCInsureAccFeeDB.setPolNo(mLPInsureAccSchema.getPolNo());
        tLCInsureAccFeeDB.setInsuAccNo(mLPInsureAccSchema.getInsuAccNo());
        tLCInsureAccFeeDB.getInfo();

        Reflections tReflections = new Reflections();
        LPInsureAccFeeSchema tLPInsureAccFeeSchema = new LPInsureAccFeeSchema();
        tReflections.transFields(tLPInsureAccFeeSchema,
                                 tLCInsureAccFeeDB.getSchema());
        tLPInsureAccFeeSchema.setEdorNo(mLPInsureAccSchema.getEdorNo());
        tLPInsureAccFeeSchema.setEdorType(mLPInsureAccSchema.getEdorType());
        tMMap.put(tLPInsureAccFeeSchema, SysConst.INSERT);

        String sql = "update LPInsureAccClass a "
                     + "set (InsuAccBala, LastAccBala) "
                     + "   =(select sum(Money)+" +
                     mLPInsureAccSchema.getInsuAccBala() + ", sum(Money)+" +
                     mLPInsureAccSchema.getInsuAccBala()
                     + " from LPInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and EdorNo = a.EdorNo) "
                     + "where PolNo = '"
                     + mLPInsureAccSchema.getPolNo() + "' "
                     + " and InsuAccNo = '"
                     + mLPInsureAccSchema.getInsuAccNo() + "' and EdorNo = '"
                     + mLPInsureAccSchema.getEdorNo() + "'";
        tMMap.put(sql, SysConst.UPDATE);
        //更新账户信息
        sql = "update LPInsureAcc a "
              + "set (InsuAccBala, LastAccBala, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(InsuAccBala), sum(LastAccBala), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime "
              + " from LPInsureAccClass where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and EdorNo = a.EdorNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + " where PolNo = '"
              + mLPInsureAccSchema.getPolNo() + "' "
              + " and InsuAccNo = '"
              + mLPInsureAccSchema.getInsuAccNo() + "' and EdorNo = '"
              + mLPInsureAccSchema.getEdorNo() + "'";
        tMMap.put(sql, SysConst.UPDATE);

        //更新账户管理费信息
        sql = "update LPInsureAccFee a " +
              "set (Fee, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + "   =(select sum(Fee), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime "
              + " from LPInsureAccClassFee where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and EdorNo = a.EdorNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + mLPInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + mLPInsureAccSchema.getInsuAccNo() + "' and EdorNo = '"
              + mLPInsureAccSchema.getEdorNo() + "'";
        tMMap.put(sql, SysConst.UPDATE);

        return tMMap;
    }


    /**
     * getCurrentAcc
     *
     * @return LCInsureAccSchema
     */
    private LCInsureAccSchema getCurrentAcc()
    {
        LCInsureAccDB db = mLCInsureAccSchema.getDB();
        if (!db.getInfo())
        {
            mErrors.addOneError("查询账户信息出错：客户号" + db.getInsuredNo()
                                + ", 险种号" + db.getRiskCode());
            return null;
        }

        return db.getSchema();
    }

    /**
     * getLCInsureAccBalance
     * 生成结算轨迹
     * @param cAccValue double
     * @param cAccValueGurat double
     * @return Object
     */
    private LCInsureAccBalanceSchema getLCInsureAccBalance(LMInsuAccRateSchema
            cLMInsuAccRateSchema,
            double cAccValue,
            double cAccValueGurat)
    {
        LCInsureAccBalanceSchema schema = new LCInsureAccBalanceSchema();
        ref.transFields(schema, mLCInsureAccSchema);

        schema.setSequenceNo(getBalanceSequenceNo());
        schema.setInsuAccBalaBefore(mLCInsureAccSchema.getInsuAccBala());
        schema.setInsuAccBalaAfter(cAccValue);
        schema.setInsuAccBalaGurat(cAccValueGurat);
        schema.setDueBalaDate(cLMInsuAccRateSchema.getBalaDate());
        schema.setDueBalaTime("00:00:00");
        schema.setRunDate(PubFun.getCurrentDate());
        schema.setRunTime(PubFun.getCurrentTime());
        schema.setPolYear(mPolYear);
        schema.setPolMonth(mPolMonth);
        schema.setPrintCount(0);
        schema.setOperator(mGI.Operator);
        PubFun.fillDefaultField(schema);

        String sql = "select max(BalaCount + 1) from LCInsureAccBalance "
                     + "where PolNo = '"
                     + mLCInsureAccSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + mLCInsureAccSchema.getInsuAccNo() + "' ";
        String maxBalaCount = new ExeSQL().getOneValue(sql);
        if (maxBalaCount == null || maxBalaCount.equals("") ||
            maxBalaCount.equals("null"))
        {
            maxBalaCount = "1";
        }
        schema.setBalaCount(maxBalaCount);

        return schema;
    }

    private LCInsureAccBalanceSchema getLCInsureAccBalanceP(LMInsuAccRateSchema
            cLMInsuAccRateSchema,
            double cAccValue,
            double cAccValueGurat)
    {
        LCInsureAccBalanceSchema schema = new LCInsureAccBalanceSchema();
        ref.transFields(schema, mLPInsureAccSchema);

        schema.setSequenceNo(getBalanceSequenceNo());
        schema.setInsuAccBalaBefore(mLPInsureAccSchema.getInsuAccBala());
        schema.setInsuAccBalaAfter(cAccValue);
        schema.setInsuAccBalaGurat(cAccValueGurat);
        schema.setDueBalaDate(cLMInsuAccRateSchema.getBalaDate());
        schema.setDueBalaTime("00:00:00");
        schema.setRunDate(PubFun.getCurrentDate());
        schema.setRunTime(PubFun.getCurrentTime());
        schema.setPolYear(mPolYear);
        schema.setPolMonth(mPolMonth);
        schema.setPrintCount(0);
        schema.setOperator(mGI.Operator);
        PubFun.fillDefaultField(schema);

        String sql = "select max(BalaCount + 1) from LCInsureAccBalance "
                     + "where PolNo = '"
                     + mLPInsureAccSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + mLPInsureAccSchema.getInsuAccNo() + "' ";
        String maxBalaCount = new ExeSQL().getOneValue(sql);
        if (maxBalaCount == null || maxBalaCount.equals("") ||
            maxBalaCount.equals("null"))
        {
            maxBalaCount = "1";
        }
        schema.setBalaCount(maxBalaCount);

        return schema;
    }


    private MMap dealOneAccClass(LCInsureAccClassSchema tLCInsureAccClassSchema,
                                 String cBalaType, String cRatetype)
    {
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("StartDate", mStartDate);
        tTransferData.setNameAndValue("EndDate", mEndDate);
        tTransferData.setNameAndValue("RiskCode",
                                      tLCInsureAccClassSchema.getRiskCode());
        tTransferData.setNameAndValue("BalaType", cBalaType); //1：账户价值结算，2：保障价值计算
        tTransferData.setNameAndValue("RateType", cRatetype); //1：取公布利率，2：取保证利率
        tTransferData.setNameAndValue("PolYear", String.valueOf(mPolYear));
        tTransferData.setNameAndValue("PolMonth", String.valueOf(mPolMonth));

        VData data = new VData();
        data.add(mGI);
        data.add(tLCInsureAccClassSchema);
        data.add(tTransferData);

        MMap tMMapAV = balanceOneAccRate(data);
        return tMMapAV;
    }

    private MMap dealOneAccClassP(LPInsureAccClassSchema
                                  tLPInsureAccClassSchema,
                                  String cBalaType, String cRatetype)
    {
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("StartDate", mStartDate);
        tTransferData.setNameAndValue("EndDate", mEndDate);
        tTransferData.setNameAndValue("RiskCode",
                                      tLPInsureAccClassSchema.getRiskCode());
        tTransferData.setNameAndValue("BalaType", cBalaType); //1：账户价值结算，2：保障价值计算
        tTransferData.setNameAndValue("RateType", cRatetype); //1：取公布利率，2：取保证利率
        tTransferData.setNameAndValue("PolYear", String.valueOf(mPolYear));
        tTransferData.setNameAndValue("PolMonth", String.valueOf(mPolMonth));

        VData data = new VData();
        data.add(mGI);
        data.add(tLPInsureAccClassSchema);
        data.add(tTransferData);

        MMap tMMapAV = balanceOneAccRateP(data);
        return tMMapAV;
    }

    /**
     * balanceOneAccRate
     * 对子账户进行一个月的收益结算
     * @param cInputDate VData
     * @return MMap
     */
    public MMap balanceOneAccRate(VData cInputDate)
    {
        TransferData tTransferData = (TransferData) cInputDate
                                     .getObjectByObjectName("TransferData", 0);
        mGI = (GlobalInput) cInputDate
              .getObjectByObjectName("GlobalInput", 0);
        LCInsureAccClassSchema tLCInsureAccClassSchema
                = (LCInsureAccClassSchema) cInputDate
                  .getObjectByObjectName("LCInsureAccClassSchema", 0);
        if (tTransferData == null || mGI == null
            || tLCInsureAccClassSchema == null)
        {
            mErrors.addOneError("传入的数据不完整");
            return null;
        }

        tTransferData.setNameAndValue("LCInsureAccClass",
                                      tLCInsureAccClassSchema);
        tTransferData.setNameAndValue("GI", mGI);

        //查询上一次结算信息
        LCInsureAccBalanceDB tLCInsureAccBalanceDB = new LCInsureAccBalanceDB();
        tLCInsureAccBalanceDB.setPolNo(tLCInsureAccClassSchema.getPolNo());
        tLCInsureAccBalanceDB.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
        tLCInsureAccBalanceDB.setDueBalaDate(tLCInsureAccClassSchema.
                                             getBalaDate());
        LCInsureAccBalanceSet tLCInsureAccBalanceSet
                = tLCInsureAccBalanceDB.query();
        if (tLCInsureAccBalanceSet.size() == 0)
        {
            mErrors.addOneError("没有查询到上一次结算信息");
            return null;
        }
        double tLastBala = tLCInsureAccBalanceSet.get(1).getInsuAccBalaAfter();
        double tLastBalaGurat = tLCInsureAccBalanceSet.get(1).
                                getInsuAccBalaGurat();
        tLastBalaGurat = (mPolYear != 1 && mPolMonth == 1 ? tLastBala :
                          tLastBalaGurat); //计算第一保单年度的保证收益时已上一年度末账户价值为基础
        tTransferData.setNameAndValue("LastBala", "" + tLastBala);
        tTransferData.setNameAndValue("LastBalaGurat", "" + tLastBalaGurat);

        //计算账户收益
        AccountManage tAccountManage = new AccountManage(mLCPolSchema);
        
        MMap tMMapLX = tAccountManage.getBalaAccInterest(tTransferData);
        if (tMMapLX == null)
        {
            mErrors.copyAllErrors(tAccountManage.mErrors);
            return null;
        }

        LCInsureAccClassSchema accClassDelt
                = (LCInsureAccClassSchema) tMMapLX
                  .getObjectByObjectName("LCInsureAccClassSchema", 0);
        accClassDelt.setInsuAccBala(PubFun.setPrecision(accClassDelt.
                getInsuAccBala(), "0.00"));
        LCInsureAccTraceSchema accTraceDelt
                = (LCInsureAccTraceSchema) tMMapLX
                  .getObjectByObjectName("LCInsureAccTraceSchema", 0);
        accTraceDelt.setOtherType("6"); //月结算
        accTraceDelt.setOtherNo(getBalanceSequenceNo());
        accTraceDelt.setMoney(PubFun.setPrecision(accTraceDelt.getMoney(),
                                                  "0.00"));

        //计算管理费
        MMap tMMapFee = balanceOneAccManageFee(accClassDelt, tTransferData);
        if (tMMapFee == null)
        {
            return null;
        }

        MMap tMMapAll = new MMap();
        tMMapAll.add(tMMapLX);
        tMMapAll.add(tMMapFee);

        return tMMapAll;
    }

    public MMap balanceOneAccRateP(VData cInputDate)
    {
        TransferData tTransferData = (TransferData) cInputDate
                                     .getObjectByObjectName("TransferData", 0);
        mGI = (GlobalInput) cInputDate
              .getObjectByObjectName("GlobalInput", 0);
        LPInsureAccClassSchema tLPInsureAccClassSchema
                = (LPInsureAccClassSchema) cInputDate
                  .getObjectByObjectName("LPInsureAccClassSchema", 0);
        if (tTransferData == null || mGI == null
            || tLPInsureAccClassSchema == null)
        {
            mErrors.addOneError("传入的数据不完整");
            return null;
        }

        tTransferData.setNameAndValue("LPInsureAccClass",
                                      tLPInsureAccClassSchema);
        tTransferData.setNameAndValue("GI", mGI);

        //查询上一次结算信息
        LCInsureAccBalanceDB tLCInsureAccBalanceDB = new LCInsureAccBalanceDB();
        tLCInsureAccBalanceDB.setPolNo(tLPInsureAccClassSchema.getPolNo());
        tLCInsureAccBalanceDB.setInsuAccNo(tLPInsureAccClassSchema.getInsuAccNo());
        tLCInsureAccBalanceDB.setDueBalaDate(tLPInsureAccClassSchema.
                                             getBalaDate());
        LCInsureAccBalanceSet tLCInsureAccBalanceSet
                = tLCInsureAccBalanceDB.query();
        if (tLCInsureAccBalanceSet.size() == 0)
        {
            mErrors.addOneError("没有查询到上一次结算信息");
            return null;
        }
        double tLastBala = tLCInsureAccBalanceSet.get(1).getInsuAccBalaAfter();
        double tLastBalaGurat = tLCInsureAccBalanceSet.get(1).
                                getInsuAccBalaGurat();
        tLastBalaGurat = (mPolYear != 1 && mPolMonth == 1 ? tLastBala :
                          tLastBalaGurat); //计算第一保单年度的保证收益时已上一年度末账户价值为基础
        tTransferData.setNameAndValue("LastBala", "" + tLastBala);
        tTransferData.setNameAndValue("LastBalaGurat", "" + tLastBalaGurat);
        
        

        //计算账户收益
        AccountManage tAccountManage = new AccountManage();
        MMap tMMapLX = null;
        if("3".equals(mRateType)){
        	if(!checkCont(tLCInsureAccBalanceSet.get(1).getPolNo())){
        		
        		System.out.println("保单不符合使用非保证利率结算的标准");
        		return null;
        		
        	}
        	tMMapLX = tAccountManage.getBalaAccInterestUP(tTransferData);
        }
        else
        	tMMapLX = tAccountManage.getBalaAccInterestP(tTransferData);
        if (tMMapLX == null)
        {
            mErrors.copyAllErrors(tAccountManage.mErrors);
            return null;
        }

        LPInsureAccClassSchema accClassDeltP
                = (LPInsureAccClassSchema) tMMapLX
                  .getObjectByObjectName("LPInsureAccClassSchema", 0);
        accClassDeltP.setInsuAccBala(PubFun.setPrecision(accClassDeltP.
                getInsuAccBala(), "0.00"));
        LPInsureAccTraceSchema accTraceDeltP
                = (LPInsureAccTraceSchema) tMMapLX
                  .getObjectByObjectName("LPInsureAccTraceSchema", 0);
        accTraceDeltP.setOtherType("10"); //保全解约结算
        accTraceDeltP.setOtherNo(getBalanceSequenceNo());
        accTraceDeltP.setMoney(PubFun.setPrecision(accTraceDeltP.getMoney(),
                "0.00"));

        //计算管理费
        MMap tMMapFee = balanceOneAccManageFeeP(accClassDeltP, tTransferData);
        if (tMMapFee == null)
        {
            return null;
        }

        MMap tMMapAll = new MMap();
        tMMapAll.add(tMMapLX);
        tMMapAll.add(tMMapFee);

        return tMMapAll;
    }


    /**
     * 计算子帐户管理费，生成管理费轨迹和账户轨迹。
     * 取LMRiskFee对应账户的FeeTakePlace=5的管理费描述进行计算。
     * @param LCInsureAccClassSchema LCInsureAccClassSchema
     * @return MMap
     */
    private MMap balanceOneAccManageFee(LCInsureAccClassSchema
                                        cLCInsureAccClassSchema,
                                        TransferData tTransferData)
    {
    	String tEndDate = (String) tTransferData.getValueByName("EndDate");
    	String tStartDate = (String) tTransferData.getValueByName("StartDate");
    	String tBalaType = (String) tTransferData.getValueByName("BalaType");
    	/**
         * 20150715新万能产品账户在保单缓交区间使用保证利率结算且不收取风险保费
         * 使用新的方法结算
         * 月结中保证账户价值的结算按原来正常处理
         */
        if(AccountManage.hasULIDefer(cLCInsureAccClassSchema,tStartDate,tEndDate)){
        	MMap pMap= balanceOneAccManageFeeNew(cLCInsureAccClassSchema,tTransferData);
        	return pMap;
        }
    	/**
         * 20160926附加万能产品账户在保单失效区间不结息
         * 使用新的方法结算
         */
        if(AccountManage.hasSUBULI(cLCInsureAccClassSchema,tStartDate,tEndDate)){
        	MMap pMap= balanceOneAccManageFeeSub(cLCInsureAccClassSchema,tTransferData);
        	return pMap;
        }
        
        //计算管理费
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tLMRiskFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        if (tLMRiskFeeDB.mErrors.needDealError())
        {
            System.out.println(tLMRiskFeeDB.mErrors.getErrContent());
            mErrors.addOneError("查询管理费时出现异常");
            return null;
        }

//        tLMRiskFeeSet.add(getLMRiskFeeSetB(cLCInsureAccClassSchema,
//                                           tTransferData));

        if (tEndDate == null)
        {
            mErrors.addOneError("请传入截至日期");
            return null;
        }

        LCInsureAccClassFeeDB tAccClassFeeDB = new LCInsureAccClassFeeDB();
        tAccClassFeeDB.setPolNo(cLCInsureAccClassSchema.getPolNo());
        tAccClassFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tAccClassFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tAccClassFeeDB.setOtherNo(cLCInsureAccClassSchema.getPolNo());
        LCInsureAccClassFeeSet set = tAccClassFeeDB.query();
        if (set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户管理费分类信息");
            return null;
        }

        MMap tMMap = new MMap();

        //计算管理费
        for (int i = 1; i <= tLMRiskFeeSet.size(); i++)
        {
            Calculator tCalculator = new Calculator();
            tCalculator.setCalCode(tLMRiskFeeSet.get(i).getFeeCalCode());
            tCalculator.addBasicFactor("ContNo",
                                       cLCInsureAccClassSchema.getContNo());
            tCalculator.addBasicFactor("PolNo",
                                       cLCInsureAccClassSchema.getPolNo());
            tCalculator.addBasicFactor("InsuredNo",
                                       cLCInsureAccClassSchema.getInsuredNo());
            tCalculator.addBasicFactor("InsuAccNo",
                                       cLCInsureAccClassSchema.getInsuAccNo());
            tCalculator.addBasicFactor("PayPlanCode",
                                       cLCInsureAccClassSchema.getPayPlanCode());

//            LCPolDB tLCPolDB = new LCPolDB();
//            tLCPolDB.setPolNo(cLCInsureAccClassSchema.getPolNo());
//            tLCPolDB.getInfo();
            //新增变量Prem，支持常无忧万能的持续奖励计算。 added by huxl @20080616
            tCalculator.addBasicFactor("Prem", String.valueOf(mLCPolSchema.getPrem()));
            //新增变量Sex，保证计算风险保费的时候能够兼容保全的重算。 added by huxl @20080619
            tCalculator.addBasicFactor("Sex", mLCPolSchema.getInsuredSex());
            //新增变量Amnt，保证计算各险万能风险保费。 added by huxl @20080619
            tCalculator.addBasicFactor("Amnt", String.valueOf(mLCPolSchema.getAmnt()));
            
            tCalculator.addBasicFactor("CValiDate", String.valueOf(mLCPolSchema.getCValiDate()));
            
            //20120130 新增变量 持续奖金计算终止日期
            tCalculator.addBasicFactor("CalEndDate", tEndDate);
//          20101221 zhanggm 新增变量，附加险保额SubAmnt,计算附加重疾风险保费；如果存在风险加费比例，传入参数RiskRate
            String subRiskCode = ""; 
            //subRiskCode ,附加险险种代码，存储在附加险月结风险保费trace的PayPlanCode字段
			if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
				subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
				LCPolDB subLCPolDB = new LCPolDB();
				subLCPolDB.setContNo(mLCPolSchema.getContNo());
				subLCPolDB.setMainPolNo(mLCPolSchema.getPolNo());
				subLCPolDB.setRiskCode(subRiskCode);
				LCPolSet subLCPolSet = subLCPolDB.query();
				if(subLCPolSet==null || subLCPolSet.size()==0)
				{
					continue;
				}
				else
				{
					LCPolSchema subLCPolSchema = subLCPolSet.get(1).getSchema();
					if(("231001".equals(subLCPolSchema.getRiskCode()) || "231201".equals(subLCPolSchema.getRiskCode())|| "231801".equals(subLCPolSchema.getRiskCode()))
							&& !"1".equals(subLCPolSchema.getStateFlag()))
					{
						continue;
					}
					String tSubAmnt = String.valueOf(subLCPolSchema.getAmnt());
					tCalculator.addBasicFactor("SubAmnt", tSubAmnt);
					
					String sql = "select Rate from LCPrem where PolNo = '" + subLCPolSchema.getPolNo() 
						+ "' and PayPlanCode like '00000%' order by PayPlanCode desc " ;
					String tRiskRate = new ExeSQL().getOneValue(sql);
					if(tRiskRate==null || tRiskRate.equals(""))
					{
						tCalculator.addBasicFactor("RiskRate", "0");
					}
					else
					{
						tCalculator.addBasicFactor("RiskRate", tRiskRate);
					}
				}
			}
			
	        //健康一生保险系列计算风险保费，需传入风险保额riskAmnt,
			//是否为健康一生险种 add by fengzhihang			
			String isJKYSSql = "select 1 from ldcode where codetype = 'jkys' and code = '" +cLCInsureAccClassSchema.getRiskCode() + "'";
	        String jkysResult = new ExeSQL().getOneValue(isJKYSSql);
			if(jkysResult != null && jkysResult.equals("1")){
					String sql1 = " select a.birthday,b.cvalidate from lcinsured a,lcpol b where a.contno=b.contno and  a.contno='" + cLCInsureAccClassSchema.getContNo()
					+ "' and a.insuredno='"+cLCInsureAccClassSchema.getInsuredNo()+ "' and b.riskcode ='"+cLCInsureAccClassSchema.getRiskCode()+ "' ";
//				String tBirthday = new ExeSQL().getOneValue(sql1);
				SSRS mSSRS1 = new ExeSQL().execSQL(sql1);
				if(mSSRS1.MaxRow == 0){
					mErrors.addOneError("获取被保人生日信息失败！");
				}
				else 
				{
					FDate tFDate= new FDate();
					String getMidDate=PubFun.calDate(mSSRS1.GetText(1, 1), 61, "Y", mSSRS1.GetText(1, 2));

					String tSql="select a.duebaladate,a.insuaccbalaafter from LCInsureAccBalance a,lcinsureaccclass b where a.polno=b.polno and  a.polno='"+cLCInsureAccClassSchema.getPolNo()+"'" 
						    	+" and a.duebaladate=b.baladate ";
					SSRS accSSRS = new ExeSQL().execSQL(tSql);
					System.out.println(tSql);
					if(accSSRS.MaxRow == 0){
						mErrors.addOneError("获取账户月结信息失败！");
		                return null;
					}
					
					String mAccSql="select sum(money) from lcinsureacctrace where contno='"+cLCInsureAccClassSchema.getContNo()+"'   and paydate<'"+tEndDate+"'  ";
					String mBFSql="select sum(money) from lcinsureacctrace where contno='"+cLCInsureAccClassSchema.getContNo()+"' and moneytype in ('ZF','BF','LQ')  and paydate<'"+tEndDate+"'  ";
					String mGLSql="select sum(fee) from lcinsureaccfeetrace where contno='"+cLCInsureAccClassSchema.getContNo()+"' and moneytype='GL' and othertype='1'";
					//缴纳的保费和追加保费-部分领取的金额 (实收的保费，所以要算上管理费)
					double BFMoney = PubFun.setPrecision(
							Double.valueOf(new ExeSQL().getOneValue(mBFSql)) 
									+ Double.valueOf(new ExeSQL().getOneValue(mGLSql)),"0.00");
					double accMoney = PubFun.setPrecision(Double.valueOf(new ExeSQL().getOneValue(mAccSql)),"0.00");//个人账户余额
					//保险金额：     61岁的保单周年日（不含）之前  
					//1)	160%×（交纳的保险费-累计已领取的“部分领取”金额）；
					//2)	个人账户价值。
					// 61岁的保单周年日（含 ）之后
					//1)	120%×（交纳的保险费-累计已领取的“部分领取”金额）；
					//2)	个人账户价值。
					//风险保额是指本合同的保险金额与个人账户价值的差额，差额为零时不收取风险保险费
					double minMoney=0;//差
					double riskAmnt=0;//风险保额
					if(tFDate.getDate(mStartDate).compareTo(tFDate.getDate(getMidDate))==-1){
						BFMoney=BFMoney*1.6;
						minMoney=PubFun.setPrecision(BFMoney - accMoney, "0.00");
					}
					else 
					{
						BFMoney=BFMoney*1.2;
						minMoney=PubFun.setPrecision(BFMoney - accMoney, "0.00");
					}
					if(minMoney < 0){
						riskAmnt = 0;
					}else{
						riskAmnt=Math.abs(minMoney);
					}
					tCalculator.addBasicFactor("RiskAmnt", String.valueOf(riskAmnt));
					
				}
	        	
	        }

            setCalculatorFactor(tTransferData, tCalculator);

            AccountManage tAccountManage = new AccountManage(mLCPolSchema);
            String tInsuredAppAge = tAccountManage
                                    .getInsuredAppAge(mStartDate,
                    cLCInsureAccClassSchema);
            if (tInsuredAppAge == null)
            {
                mErrors.copyAllErrors(tAccountManage.mErrors);
                return null;
            }
            tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);

            double fee = Math.abs(PubFun.setPrecision(
                    Double.parseDouble(tCalculator.calculate()), "0.00"));
            if (tCalculator.mErrors.needDealError())
            {
                System.out.println(tCalculator.mErrors.getErrContent());
                mErrors.addOneError("计算管理费出错");
                return null;
            }

            //管理费轨迹
            LCInsureAccFeeTraceSchema schema = new LCInsureAccFeeTraceSchema();
            ref.transFields(schema, cLCInsureAccClassSchema);

            String tLimit = PubFun.getNoLimit(schema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
            
            if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
            	schema.setPayPlanCode(subRiskCode);
			}
            schema.setSerialNo(serNo);
            schema.setFeeCode(tLMRiskFeeSet.get(i).getFeeCode());
            schema.setFee(fee);
            schema.setOtherNo(this.getBalanceSequenceNo());
            schema.setOtherType("6"); //月结算
            schema.setFeeRate(tLMRiskFeeSet.get(i).getFeeValue());
            schema.setPayDate(tEndDate);
            schema.setState("0");
            PubFun.fillDefaultField(schema);

            String moneyType = CommonBL.getCodeName("accmanagefee",
                    schema.getFeeCode());
            if (moneyType == null)
            {
                mErrors.addOneError("请先描述管理费的财务类型");
                return null;
            }
            schema.setMoneyType(moneyType);
            //持续奖金不进入管理费表。
            if (!moneyType.equals("B"))
            {
                tMMap.put(schema, SysConst.INSERT);
                //更新ClassFee
                set.get(1).setFee(set.get(1).getFee() + fee);
                set.get(1).setBalaDate(schema.getPayDate());
                set.get(1).setOperator(mGI.Operator);
                set.get(1).setModifyDate(PubFun.getCurrentDate());
                set.get(1).setModifyTime(PubFun.getCurrentTime());
            }
            //管理费的账户轨迹
            LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
            ref.transFields(traceSchema, schema);

            String tLimit2 = PubFun.getNoLimit(traceSchema.getManageCom());
            String serNo2 = PubFun1.CreateMaxNo("SERIALNO", tLimit2);
            traceSchema.setSerialNo(serNo2);
            //只有持续奖金大于零的时候才进账户轨迹表。
            if (fee > 0 && moneyType.equals("B"))
            {
                traceSchema.setMoney(Math.abs(schema.getFee()));
                tMMap.put(traceSchema, SysConst.INSERT);
            } else if (!moneyType.equals("B"))
            {
                traceSchema.setMoney( -Math.abs(schema.getFee()));
                tMMap.put(traceSchema, SysConst.INSERT);
            }
            //更新账户分类轨迹
            cLCInsureAccClassSchema.setInsuAccBala(cLCInsureAccClassSchema.
                    getInsuAccBala()
                    + traceSchema.getMoney());
            cLCInsureAccClassSchema.setLastAccBala(cLCInsureAccClassSchema.
                    getInsuAccBala());
        }

        tMMap.put(set.get(1), SysConst.UPDATE);

        return tMMap;
    }

    /**
     * 计算子帐户管理费，生成管理费轨迹和账户轨迹。
     * 取LMRiskFee对应账户的FeeTakePlace=5的管理费描述进行计算。
     * @param LCInsureAccClassSchema LCInsureAccClassSchema
     * @return MMap
     */
    private MMap balanceOneAccManageFeeNew(LCInsureAccClassSchema
                                        cLCInsureAccClassSchema,
                                        TransferData tTransferData)
    {
    	String tEndDate = (String) tTransferData.getValueByName("EndDate");
    	String tStartDate = (String) tTransferData.getValueByName("StartDate");
    	String tBalaType = (String) tTransferData.getValueByName("BalaType");
    	//获取结算区间
        LMInsuAccRateSet tLMInsuAccRateSet=AccountManage.getBalanceRate(cLCInsureAccClassSchema,
        											tStartDate,tEndDate,tBalaType);
        if(tLMInsuAccRateSet.size()==0){
        	mErrors.addOneError("账户结算区间拆分失败！");
            return null;
        }
        //计算管理费
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tLMRiskFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
        tLMRiskFeeDB.setFeeKind("03"); 
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        if (tLMRiskFeeDB.mErrors.needDealError())
        {
            System.out.println(tLMRiskFeeDB.mErrors.getErrContent());
            mErrors.addOneError("查询管理费时出现异常");
            return null;
        }

        if (tEndDate == null)
        {
            mErrors.addOneError("请传入截至日期");
            return null;
        }

        LCInsureAccClassFeeDB tAccClassFeeDB = new LCInsureAccClassFeeDB();
        tAccClassFeeDB.setPolNo(cLCInsureAccClassSchema.getPolNo());
        tAccClassFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tAccClassFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tAccClassFeeDB.setOtherNo(cLCInsureAccClassSchema.getPolNo());
        LCInsureAccClassFeeSet set = tAccClassFeeDB.query();
        if (set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户管理费分类信息");
            return null;
        }

        MMap tMMap = new MMap();

        //计算管理费
        for (int i = 1; i <= tLMRiskFeeSet.size(); i++)
        {
            double fee=0;
            String subRiskCode = ""; 
            //对每一笔管理费分结算区间处理
            for(int j=1;j<=tLMInsuAccRateSet.size();j++){
            	LMInsuAccRateSchema tRate=tLMInsuAccRateSet.get(j);
            	tTransferData.removeByName("StartDate");
            	tTransferData.removeByName("EndDate");
            	System.out.println(tRate.getStartBalaDate()+","+tRate.getBalaDate());
            	tTransferData.setNameAndValue("StartDate", tRate.getStartBalaDate());
            	tTransferData.setNameAndValue("EndDate", tRate.getBalaDate());
            	//缓交区间不收主附险的风险保费
            	String str=tLMRiskFeeSet.get(i).getFeeName();
            	int isRP=str.indexOf("风险");//没有字段能准确 标识，只能用模糊的方式来查是否为风险保费了，尴尬。。。
            	if("2".equals(tRate.getRateType()) 
            		&& isRP>=0){
            		subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
            		continue;
            	}
            	
            	Calculator tCalculator = new Calculator();
                tCalculator.setCalCode(tLMRiskFeeSet.get(i).getFeeCalCode());
                tCalculator.addBasicFactor("ContNo",
                                           cLCInsureAccClassSchema.getContNo());
                tCalculator.addBasicFactor("PolNo",
                                           cLCInsureAccClassSchema.getPolNo());
                tCalculator.addBasicFactor("InsuredNo",
                                           cLCInsureAccClassSchema.getInsuredNo());
                tCalculator.addBasicFactor("InsuAccNo",
                                           cLCInsureAccClassSchema.getInsuAccNo());
                tCalculator.addBasicFactor("PayPlanCode",
                                           cLCInsureAccClassSchema.getPayPlanCode());

                //新增变量Prem，支持常无忧万能的持续奖励计算。 added by huxl @20080616
                tCalculator.addBasicFactor("Prem", String.valueOf(mLCPolSchema.getPrem()));
                //新增变量Sex，保证计算风险保费的时候能够兼容保全的重算。 added by huxl @20080619
                tCalculator.addBasicFactor("Sex", mLCPolSchema.getInsuredSex());
                //新增变量Amnt，保证计算各险万能风险保费。 added by huxl @20080619
                tCalculator.addBasicFactor("Amnt", String.valueOf(mLCPolSchema.getAmnt()));
                
                tCalculator.addBasicFactor("CValiDate", String.valueOf(mLCPolSchema.getCValiDate()));
                //20120130 新增变量 持续奖金计算终止日期
                tCalculator.addBasicFactor("CalEndDate", tEndDate);
                //subRiskCode ,附加险险种代码，存储在附加险月结风险保费trace的PayPlanCode字段
    			if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
    			{
    				subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
    				LCPolDB subLCPolDB = new LCPolDB();
    				subLCPolDB.setContNo(mLCPolSchema.getContNo());
    				subLCPolDB.setMainPolNo(mLCPolSchema.getPolNo());
    				subLCPolDB.setRiskCode(subRiskCode);
    				LCPolSet subLCPolSet = subLCPolDB.query();
    				if(subLCPolSet==null || subLCPolSet.size()==0)
    				{
    					continue;
    				}
    				else
    				{
    					LCPolSchema subLCPolSchema = subLCPolSet.get(1).getSchema();
    					if(("231001".equals(subLCPolSchema.getRiskCode()) || "231201".equals(subLCPolSchema.getRiskCode())|| "231801".equals(subLCPolSchema.getRiskCode()))
    							&& !"1".equals(subLCPolSchema.getStateFlag()))
    					{
    						continue;
    					}
    					String tSubAmnt = String.valueOf(subLCPolSchema.getAmnt());
    					tCalculator.addBasicFactor("SubAmnt", tSubAmnt);
    					
    					String sql = "select Rate from LCPrem where PolNo = '" + subLCPolSchema.getPolNo() 
    						+ "' and PayPlanCode like '00000%' order by PayPlanCode desc " ;
    					String tRiskRate = new ExeSQL().getOneValue(sql);
    					if(tRiskRate==null || tRiskRate.equals(""))
    					{
    						tCalculator.addBasicFactor("RiskRate", "0");
    					}
    					else
    					{
    						tCalculator.addBasicFactor("RiskRate", tRiskRate);
    					}
    				}
    			}
                AccountManage tAccountManage = new AccountManage(mLCPolSchema);
                String tInsuredAppAge = tAccountManage
                                        .getInsuredAppAge(mStartDate,
                        cLCInsureAccClassSchema);
                if (tInsuredAppAge == null)
                {
                    mErrors.copyAllErrors(tAccountManage.mErrors);
                    return null;
                }
                tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);
            	
	            setCalculatorFactor(tTransferData, tCalculator);
	
	            fee += Math.abs(PubFun.setPrecision(
                    Double.parseDouble(tCalculator.calculate()), "0.0000"));
	            System.out.println(tLMRiskFeeSet.get(i).getFeeName()+":在"+tRate.getStartBalaDate()+" 至 "
        				+ tRate.getBalaDate()+"期间，收取管理费："+fee);
	            if (tCalculator.mErrors.needDealError())
	            {
	                System.out.println(tCalculator.mErrors.getErrContent());
	                mErrors.addOneError("计算管理费出错");
	                return null;
	            }
            }
            fee =PubFun.setPrecision(fee,"0.00");
            //管理费轨迹
            LCInsureAccFeeTraceSchema schema = new LCInsureAccFeeTraceSchema();
            ref.transFields(schema, cLCInsureAccClassSchema);

            String tLimit = PubFun.getNoLimit(schema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
            
            if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
            	schema.setPayPlanCode(subRiskCode);
			}
            
            schema.setSerialNo(serNo);
            schema.setFeeCode(tLMRiskFeeSet.get(i).getFeeCode());
            schema.setFee(fee);
            schema.setOtherNo(this.getBalanceSequenceNo());
            schema.setOtherType("6"); //月结算
            schema.setFeeRate(tLMRiskFeeSet.get(i).getFeeValue());
            schema.setPayDate(tEndDate);
            schema.setState("0");
            PubFun.fillDefaultField(schema);

            String moneyType = CommonBL.getCodeName("accmanagefee",
                    schema.getFeeCode());
            if (moneyType == null)
            {
                mErrors.addOneError("请先描述管理费的财务类型");
                return null;
            }
            schema.setMoneyType(moneyType);
            tMMap.put(schema, SysConst.INSERT);
            //更新ClassFee
            set.get(1).setFee(set.get(1).getFee() + fee);
            set.get(1).setBalaDate(schema.getPayDate());
            set.get(1).setOperator(mGI.Operator);
            set.get(1).setModifyDate(PubFun.getCurrentDate());
            set.get(1).setModifyTime(PubFun.getCurrentTime());
            
            //管理费的账户轨迹
            LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
            ref.transFields(traceSchema, schema);

            String tLimit2 = PubFun.getNoLimit(traceSchema.getManageCom());
            String serNo2 = PubFun1.CreateMaxNo("SERIALNO", tLimit2);
            traceSchema.setSerialNo(serNo2);
            traceSchema.setMoney( -Math.abs(schema.getFee()));
            tMMap.put(traceSchema, SysConst.INSERT);
            //更新账户分类轨迹
            cLCInsureAccClassSchema.setInsuAccBala(cLCInsureAccClassSchema.
                    getInsuAccBala()
                    + traceSchema.getMoney());
            cLCInsureAccClassSchema.setLastAccBala(cLCInsureAccClassSchema.
                    getInsuAccBala());
        }

        tMMap.put(set.get(1), SysConst.UPDATE);

        return tMMap;
    }
    
    /**
     * 计算子帐户管理费，生成管理费轨迹和账户轨迹。
     * 附加万能账户没有管理费（20161020）加上对管理费计算部分，以防以后支持此类情况
     * 取LMRiskFee对应账户的FeeTakePlace=5的管理费描述进行计算。
     * @param LCInsureAccClassSchema LCInsureAccClassSchema
     * @return MMap
     */
    private MMap balanceOneAccManageFeeSub(LCInsureAccClassSchema
                                        cLCInsureAccClassSchema,
                                        TransferData tTransferData)
    {
    	String tEndDate = (String) tTransferData.getValueByName("EndDate");
    	String tStartDate = (String) tTransferData.getValueByName("StartDate");
    	String tBalaType = (String) tTransferData.getValueByName("BalaType");
    	//获取结算区间
        LMInsuAccRateSet tLMInsuAccRateSet=AccountManage.getBalanceRate1(cLCInsureAccClassSchema,
        											tStartDate,tEndDate,tBalaType);
        if(tLMInsuAccRateSet.size()==0){
        	mErrors.addOneError("账户结算区间拆分失败！");
            return null;
        }
        //计算管理费
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tLMRiskFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
        tLMRiskFeeDB.setFeeKind("03"); 
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        if (tLMRiskFeeDB.mErrors.needDealError())
        {
            System.out.println(tLMRiskFeeDB.mErrors.getErrContent());
            mErrors.addOneError("查询管理费时出现异常");
            return null;
        }

        if (tEndDate == null)
        {
            mErrors.addOneError("请传入截至日期");
            return null;
        }

        LCInsureAccClassFeeDB tAccClassFeeDB = new LCInsureAccClassFeeDB();
        tAccClassFeeDB.setPolNo(cLCInsureAccClassSchema.getPolNo());
        tAccClassFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tAccClassFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tAccClassFeeDB.setOtherNo(cLCInsureAccClassSchema.getPolNo());
        LCInsureAccClassFeeSet set = tAccClassFeeDB.query();
        if (set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户管理费分类信息");
            return null;
        }

        MMap tMMap = new MMap();

        //计算管理费
        for (int i = 1; i <= tLMRiskFeeSet.size(); i++)
        {
            double fee=0;
            String subRiskCode = ""; 
            //对每一笔管理费分结算区间处理
            for(int j=1;j<=tLMInsuAccRateSet.size();j++){
				LMInsuAccRateSchema tRate = tLMInsuAccRateSet.get(j);
				tTransferData.removeByName("StartDate");
				tTransferData.removeByName("EndDate");
				System.out.println(tRate.getStartBalaDate() + ","
						+ tRate.getBalaDate());
				tTransferData.setNameAndValue("StartDate", tRate
						.getStartBalaDate());
				tTransferData.setNameAndValue("EndDate", tRate.getBalaDate());
				// 失效区间不收附加万能的风险保费
				String str = tLMRiskFeeSet.get(i).getFeeName();
				int isRP = str.indexOf("风险");// 没有字段能准确
												// 标识，只能用模糊的方式来查是否为风险保费了，尴尬。。。
				if ("2".equals(tRate.getRateType()) && isRP >= 0) {
					subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
					continue;
				}

				Calculator tCalculator = new Calculator();
				tCalculator.setCalCode(tLMRiskFeeSet.get(i).getFeeCalCode());
				tCalculator.addBasicFactor("ContNo", cLCInsureAccClassSchema
						.getContNo());
				tCalculator.addBasicFactor("PolNo", cLCInsureAccClassSchema
						.getPolNo());
				tCalculator.addBasicFactor("InsuredNo", cLCInsureAccClassSchema
						.getInsuredNo());
				tCalculator.addBasicFactor("InsuAccNo", cLCInsureAccClassSchema
						.getInsuAccNo());
				tCalculator.addBasicFactor("PayPlanCode",
						cLCInsureAccClassSchema.getPayPlanCode());

				// 新增变量Prem，支持常无忧万能的持续奖励计算。 added by huxl @20080616
				tCalculator.addBasicFactor("Prem", String.valueOf(mLCPolSchema
						.getPrem()));
				// 新增变量Sex，保证计算风险保费的时候能够兼容保全的重算。 added by huxl @20080619
				tCalculator.addBasicFactor("Sex", mLCPolSchema.getInsuredSex());
				// 新增变量Amnt，保证计算各险万能风险保费。 added by huxl @20080619
				tCalculator.addBasicFactor("Amnt", String.valueOf(mLCPolSchema
						.getAmnt()));

				tCalculator.addBasicFactor("CValiDate", String
						.valueOf(mLCPolSchema.getCValiDate()));
				// 20120130 新增变量 持续奖金计算终止日期
				tCalculator.addBasicFactor("CalEndDate", tEndDate);
				// subRiskCode ,附加险险种代码，存储在附加险月结风险保费trace的PayPlanCode字段
				if (tLMRiskFeeSet.get(i).getFeeItemType().equals("07")) {
					subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
					LCPolDB subLCPolDB = new LCPolDB();
					subLCPolDB.setContNo(mLCPolSchema.getContNo());
					subLCPolDB.setMainPolNo(mLCPolSchema.getPolNo());
					subLCPolDB.setRiskCode(subRiskCode);
					LCPolSet subLCPolSet = subLCPolDB.query();
					if (subLCPolSet == null || subLCPolSet.size() == 0) {
						continue;
					} else {
						LCPolSchema subLCPolSchema = subLCPolSet.get(1)
								.getSchema();
						if (("231001".equals(subLCPolSchema.getRiskCode())
								|| "231201"
										.equals(subLCPolSchema.getRiskCode()) || "231801"
								.equals(subLCPolSchema.getRiskCode()))
								&& !"1".equals(subLCPolSchema.getStateFlag())) {
							continue;
						}
						String tSubAmnt = String.valueOf(subLCPolSchema
								.getAmnt());
						tCalculator.addBasicFactor("SubAmnt", tSubAmnt);

						String sql = "select Rate from LCPrem where PolNo = '"
								+ subLCPolSchema.getPolNo()
								+ "' and PayPlanCode like '00000%' order by PayPlanCode desc ";
						String tRiskRate = new ExeSQL().getOneValue(sql);
						if (tRiskRate == null || tRiskRate.equals("")) {
							tCalculator.addBasicFactor("RiskRate", "0");
						} else {
							tCalculator.addBasicFactor("RiskRate", tRiskRate);
						}
					}
				}
				AccountManage tAccountManage = new AccountManage(mLCPolSchema);
				String tInsuredAppAge = tAccountManage.getInsuredAppAge(
						mStartDate, cLCInsureAccClassSchema);
				if (tInsuredAppAge == null) {
					mErrors.copyAllErrors(tAccountManage.mErrors);
					return null;
				}
				tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);

				setCalculatorFactor(tTransferData, tCalculator);

				fee += Math.abs(PubFun.setPrecision(Double
						.parseDouble(tCalculator.calculate()), "0.0000"));
				System.out.println(tLMRiskFeeSet.get(i).getFeeName() + ":在"
						+ tRate.getStartBalaDate() + " 至 "
						+ tRate.getBalaDate() + "期间，收取管理费：" + fee);
				if (tCalculator.mErrors.needDealError()) {
					System.out.println(tCalculator.mErrors.getErrContent());
					mErrors.addOneError("计算管理费出错");
					return null;
				}

			}
            fee =PubFun.setPrecision(fee,"0.00");
            //管理费轨迹
            LCInsureAccFeeTraceSchema schema = new LCInsureAccFeeTraceSchema();
            ref.transFields(schema, cLCInsureAccClassSchema);

            String tLimit = PubFun.getNoLimit(schema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
            
            if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
            	schema.setPayPlanCode(subRiskCode);
			}
            
            schema.setSerialNo(serNo);
            schema.setFeeCode(tLMRiskFeeSet.get(i).getFeeCode());
            schema.setFee(fee);
            schema.setOtherNo(this.getBalanceSequenceNo());
            schema.setOtherType("6"); //月结算
            schema.setFeeRate(tLMRiskFeeSet.get(i).getFeeValue());
            schema.setPayDate(tEndDate);
            schema.setState("0");
            PubFun.fillDefaultField(schema);

            String moneyType = CommonBL.getCodeName("accmanagefee",
                    schema.getFeeCode());
            if (moneyType == null)
            {
                mErrors.addOneError("请先描述管理费的财务类型");
                return null;
            }
            schema.setMoneyType(moneyType);
            tMMap.put(schema, SysConst.INSERT);
            //更新ClassFee
            set.get(1).setFee(set.get(1).getFee() + fee);
            set.get(1).setBalaDate(schema.getPayDate());
            set.get(1).setOperator(mGI.Operator);
            set.get(1).setModifyDate(PubFun.getCurrentDate());
            set.get(1).setModifyTime(PubFun.getCurrentTime());
            
            //管理费的账户轨迹
            LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
            ref.transFields(traceSchema, schema);

            String tLimit2 = PubFun.getNoLimit(traceSchema.getManageCom());
            String serNo2 = PubFun1.CreateMaxNo("SERIALNO", tLimit2);
            traceSchema.setSerialNo(serNo2);
            traceSchema.setMoney( -Math.abs(schema.getFee()));
            tMMap.put(traceSchema, SysConst.INSERT);
            //更新账户分类轨迹
            cLCInsureAccClassSchema.setInsuAccBala(cLCInsureAccClassSchema.
                    getInsuAccBala()
                    + traceSchema.getMoney());
            cLCInsureAccClassSchema.setLastAccBala(cLCInsureAccClassSchema.
                    getInsuAccBala());
        }

        tMMap.put(set.get(1), SysConst.UPDATE);

        return tMMap;
    }
    
    /**
     * 计算子帐户管理费，生成管理费轨迹和账户轨迹。
     * 取LMRiskFee对应账户的FeeTakePlace=5的管理费描述进行计算。
     * @param LCInsureAccClassSchema LCInsureAccClassSchema
     * @return MMap
     */
    private MMap balanceOneAccManageFeeP(LPInsureAccClassSchema
                                         cLPInsureAccClassSchema,
                                         TransferData tTransferData)
    {
        //计算管理费
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(cLPInsureAccClassSchema.getInsuAccNo());
        tLMRiskFeeDB.setPayPlanCode(cLPInsureAccClassSchema.getPayPlanCode());
        tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        if (tLMRiskFeeDB.mErrors.needDealError())
        {
            System.out.println(tLMRiskFeeDB.mErrors.getErrContent());
            mErrors.addOneError("查询管理费时出现异常");
            return null;
        }

//        tLMRiskFeeSet.add(getLMRiskFeeSetBP(cLPInsureAccClassSchema,
//                                            tTransferData));

        String tEndDate = (String) tTransferData.getValueByName("EndDate");
        if (tEndDate == null)
        {
            mErrors.addOneError("请传入截至日期");
            return null;
        }
        String tStartDate = (String) tTransferData.getValueByName("StartDate");
        LCInsureAccClassSchema tempAccSchema=new LCInsureAccClassSchema();
        ref.transFields(tempAccSchema, cLPInsureAccClassSchema);
        /**
         * 20150715新万能产品账户在保单缓交区间使用保证利率结算且不收取风险保费
         * 使用新的方法结算
         */
        if(AccountManage.hasULIDefer(tempAccSchema,tStartDate,tEndDate)){
        	MMap pMap= balanceOneAccManageFeePNew(cLPInsureAccClassSchema,tTransferData);
        	return pMap;
        }
    	/**
         * 20160926附加万能产品账户在保单失效区间不结息
         * 使用新的方法结算
         */
        if(AccountManage.hasSUBULI(tempAccSchema,tStartDate,tEndDate)){
        	MMap pMap= balanceOneAccManageFeePSub(cLPInsureAccClassSchema,tTransferData);
        	return pMap;
        }
        
        /**add by lzy 20151208处理税优未满期风险保费*/
        String str="select 1 from lmriskapp where riskcode='" +cLPInsureAccClassSchema.getRiskCode()+ "' and taxoptimal='Y'";
        String sy=new ExeSQL().getOneValue(str);
        if(null != sy && !"".equals(sy)){
        	LMRiskFeeDB tmLMRiskFeeDB = new LMRiskFeeDB();
        	tmLMRiskFeeDB.setInsuAccNo(cLPInsureAccClassSchema.getInsuAccNo());
        	tmLMRiskFeeDB.setPayPlanCode(cLPInsureAccClassSchema.getPayPlanCode());
        	tmLMRiskFeeDB.setFeeCode("T00001");
        	tmLMRiskFeeDB.setFeeKind("03");
        	tmLMRiskFeeDB.setFeeTakePlace("07");
        	tmLMRiskFeeDB.setFeeItemType("05");
        	tLMRiskFeeSet.add(tmLMRiskFeeDB.query());
        }

        LPInsureAccClassFeeBL tLPInsureAccClassFeeBL = new
                LPInsureAccClassFeeBL();
        LPInsureAccClassFeeSet set = tLPInsureAccClassFeeBL.
                                     queryAllLPInsureAccClassFee(
                                             mLPEdorItemSchema);
        if (set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户管理费分类信息");
            return null;
        }

        MMap tMMap = new MMap();

        //计算管理费
        for (int i = 1; i <= tLMRiskFeeSet.size(); i++)
        {
            Calculator tCalculator = new Calculator();
            tCalculator.setCalCode(tLMRiskFeeSet.get(i).getFeeCalCode());
            tCalculator.addBasicFactor("ContNo",
                                       cLPInsureAccClassSchema.getContNo());
            tCalculator.addBasicFactor("PolNo",
                                       cLPInsureAccClassSchema.getPolNo());
            tCalculator.addBasicFactor("InsuredNo",
                                       cLPInsureAccClassSchema.getInsuredNo());
            tCalculator.addBasicFactor("InsuAccNo",
                                       cLPInsureAccClassSchema.getInsuAccNo());
            tCalculator.addBasicFactor("PayPlanCode",
                                       cLPInsureAccClassSchema.getPayPlanCode());
            //新增变量Prem，支持常无忧万能的持续奖励计算。 added by huxl @20080616
            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setPolNo(cLPInsureAccClassSchema.getPolNo());
            tLPPolDB.setEdorNo(cLPInsureAccClassSchema.getEdorNo());
            tLPPolDB.setEdorType(cLPInsureAccClassSchema.getEdorType());
            tLPPolDB.getInfo();
            
            tCalculator.addBasicFactor("Prem", String.valueOf(tLPPolDB.getPrem()));
            tCalculator.addBasicFactor("Sex", tLPPolDB.getInsuredSex());
            tCalculator.addBasicFactor("Amnt", String.valueOf(tLPPolDB.getAmnt()));
            tCalculator.addBasicFactor("CValiDate", String.valueOf(tLPPolDB.getCValiDate()));
            
	        //addby fengzhihang 健康一生系列需要传入风险保额riskAmnt
            String isJKYSSql = "select 1 from ldcode where codetype = 'jkys' and code = '" +cLPInsureAccClassSchema.getRiskCode() + "'";
	        String jkysResult = new ExeSQL().getOneValue(isJKYSSql);
			if(jkysResult != null && jkysResult.equals("1")){
					String sql1 = " select a.birthday,b.cvalidate from lcinsured a,lcpol b where a.contno=b.contno and  a.contno='" + cLPInsureAccClassSchema.getContNo()
					+ "' and a.insuredno='"+cLPInsureAccClassSchema.getInsuredNo()+ "' and b.riskcode ='"+cLPInsureAccClassSchema.getRiskCode()+ "' ";
//				String tBirthday = new ExeSQL().getOneValue(sql1);
				SSRS mSSRS1 = new ExeSQL().execSQL(sql1);
				if(mSSRS1.MaxRow == 0){
					mErrors.addOneError("获取被保人生日信息失败！");
				}
				else 
				{
					FDate tFDate= new FDate();
					String getMidDate=PubFun.calDate(mSSRS1.GetText(1, 1), 61, "Y", mSSRS1.GetText(1, 2));

					String tSql="select a.duebaladate,a.insuaccbalaafter from LCInsureAccBalance a,lcinsureaccclass b where a.contno=b.contno and  b.polno='"+cLPInsureAccClassSchema.getPolNo()+"'" 
						    	+" and duebaladate=b.baladate  ";
//					String insuaccbalaafter = new ExeSQL().getOneValue(tSql);
					SSRS accSSRS = new ExeSQL().execSQL(tSql);
					System.out.println(tSql);
					if(accSSRS.MaxRow == 0){
						mErrors.addOneError("获取账户月结信息失败！");
		                return null;
					}

//					double LXMoney=Double.valueOf(mLXMoney);//批处理算出来的本月的收益
					String mAccSql="select sum(money) from lcinsureacctrace where contno='"+cLPInsureAccClassSchema.getContNo()+"'   and paydate<'"+tEndDate+"'  ";
					String mBFSql="select sum(money) from lcinsureacctrace where contno='"+cLPInsureAccClassSchema.getContNo()+"' and moneytype in ('ZF','BF','LQ')  and paydate<'"+tEndDate+"'  ";
					String mGLSql="select sum(fee) from lcinsureaccfeetrace where contno='"+cLPInsureAccClassSchema.getContNo()+"' and moneytype='GL' and othertype='1'";
					//缴纳的保费和追加保费-部分领取的金额  (实收的保费，算上管理费)
					double BFMoney = PubFun.setPrecision(
							Double.valueOf(new ExeSQL().getOneValue(mBFSql)) 
									+ Double.valueOf(new ExeSQL().getOneValue(mGLSql)),"0.00");
					double accMoney = PubFun.setPrecision(Double.valueOf(new ExeSQL().getOneValue(mAccSql)),"0.00");//个人账户余额
					//保险金额： 61岁的保单周年日（不含）之前 
					//1)	160%×（交纳的保险费-累计已领取的“部分领取”金额）；
					//2)	个人账户价值。
					// 61岁的保单周年日（含）之后
					//1)	120%×（交纳的保险费-累计已领取的“部分领取”金额）；
					//2)	个人账户价值。
					//风险保额是指本合同的保险金额与个人账户价值的差额，差额为零时不收取风险保险费

					double minMoney=0;//差
					double riskAmnt=0;//风险保额
					if(tFDate.getDate(mStartDate).compareTo(tFDate.getDate(getMidDate))==-1){
						BFMoney=BFMoney*1.6;
						minMoney=PubFun.setPrecision(BFMoney - accMoney, "0.00");
					}
					else 
					{
						BFMoney=BFMoney*1.2;
						minMoney=PubFun.setPrecision(BFMoney - accMoney, "0.00");
					}
					if(minMoney < 0){
						riskAmnt = 0;
					}else{
						riskAmnt=Math.abs(minMoney);
					}
					
					tCalculator.addBasicFactor("RiskAmnt", String.valueOf(riskAmnt));
					
				}
	        	
	        }

            //20120130 新增变量 持续奖金计算终止日期
            tCalculator.addBasicFactor("CalEndDate", tEndDate);
//          20101221 zhanggm 新增变量，附加险保额SubAmnt,计算附加重疾风险保费；如果存在风险加费比例，传入参数RiskRate
            String subRiskCode = ""; 
            //subRiskCode ,附加险险种代码，存储在附加险月结风险保费trace的PayPlanCode字段
			if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
				subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
				LCPolDB subLCPolDB = new LCPolDB();
				subLCPolDB.setContNo(cLPInsureAccClassSchema.getContNo());
				subLCPolDB.setMainPolNo(cLPInsureAccClassSchema.getPolNo());
				subLCPolDB.setRiskCode(subRiskCode);
				LCPolSet subLCPolSet = subLCPolDB.query();
				if(subLCPolSet==null || subLCPolSet.size()==0)
				{
					continue;
				}
				else
				{
					LCPolSchema subLCPolSchema = subLCPolSet.get(1).getSchema();
					if(("231001".equals(subLCPolSchema.getRiskCode()) || "231201".equals(subLCPolSchema.getRiskCode())|| "231801".equals(subLCPolSchema.getRiskCode()))
							&& !"1".equals(subLCPolSchema.getStateFlag()))
					{
						continue;
					}
					String tSubAmnt = String.valueOf(subLCPolSchema.getAmnt());
					tCalculator.addBasicFactor("SubAmnt", tSubAmnt);
					
					String sql = "select Rate from LCPrem where PolNo = '" + subLCPolSchema.getPolNo() 
						+ "' and PayPlanCode like '00000%' order by PayPlanCode desc " ;
					String tRiskRate = new ExeSQL().getOneValue(sql);
					if(tRiskRate==null || tRiskRate.equals(""))
					{
						tCalculator.addBasicFactor("RiskRate", "0");
					}
					else
					{
						tCalculator.addBasicFactor("RiskRate", tRiskRate);
					}
				}
			}	
			
			//20151209税优增加未满器净风险保费的计算
			if(null != sy && !"".equals(sy)){
				String strSQL="select a.payintv,a.insuredappage,b.totaldiscountfactor from lcpol a,lccontsub b where a.prtno=b.prtno and a.polno='"+ cLPInsureAccClassSchema.getPolNo() +"' ";
				SSRS tSSRS = new ExeSQL().execSQL(strSQL);
				if(tSSRS.MaxRow == 0){
					mErrors.addOneError("获取保单详细信息失败！");
	                return null;
				}
				tCalculator.addBasicFactor("PayIntv", tSSRS.GetText(1, 1));
				tCalculator.addBasicFactor("AppAge", tSSRS.GetText(1, 2));
				//增加折扣因子
				if(null != tSSRS.GetText(1, 3) && !"".equals(tSSRS.GetText(1, 3))){
					tCalculator.addBasicFactor("Totaldiscountfactor", tSSRS.GetText(1, 3));
				}else
				{
					tCalculator.addBasicFactor("Totaldiscountfactor", "1");
				}
//				String disCountSQL="select b.totaldiscountfactor from lcpol a,lccontsub b where a.prtno=b.prtno and a.polno='"+ cLPInsureAccClassSchema.getPolNo() +"' ";
				
        	}
			 //2012-03-14 增加给付持续奖金特殊情况的校验
            System.out.println(tLMRiskFeeSet.get(i).getFeeCode());
           if(tLMRiskFeeSet.get(i).getFeeCode().equals("000004"))
            {
//            	  tTransferData.setNameAndValue("PolMonth", "12");
            	  tCalculator.addBasicFactor("PolMonth", "12");
            }
            else
            {
//            	  tTransferData.setNameAndValue("PolMonth", String.valueOf(mPolMonth));
            	  tCalculator.addBasicFactor("PolMonth", String.valueOf(mPolMonth));
            }
            //*********************************************

            setCalculatorFactor(tTransferData, tCalculator);

            
            AccountManage tAccountManage = new AccountManage();
            String tInsuredAppAge = tAccountManage
                                    .getInsuredAppAgeP(mStartDate,
                    cLPInsureAccClassSchema);
            if (tInsuredAppAge == null)
            {
                mErrors.copyAllErrors(tAccountManage.mErrors);
                return null;
            }
            tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);
            double fee = Math.abs(PubFun.setPrecision(
                    Double.parseDouble(tCalculator.calculate()), "0.00"));
            if (tCalculator.mErrors.needDealError())
            {
                System.out.println(tCalculator.mErrors.getErrContent());
                mErrors.addOneError("计算管理费出错");
                return null;
            }
            //20151209税优未满期净风险保费处理（已发生保险金给付的，未满期净风险保险费为零）
			if(null != sy && !"".equals(sy) && tLMRiskFeeSet.get(i).getFeeCode().equals("T00001")){
				GCheckClaimBL check = new GCheckClaimBL();
				if(check.checkClaimed(cLPInsureAccClassSchema.getPolNo())){
					fee=0;
				}else{
					fee= -Math.abs(fee);//这里是补费不是扣费。
				}
			}
            //管理费轨迹
            LPInsureAccFeeTraceSchema schema = new LPInsureAccFeeTraceSchema();
            ref.transFields(schema, cLPInsureAccClassSchema);
            
            if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
            	schema.setPayPlanCode(subRiskCode);
			}

            String tLimit = PubFun.getNoLimit(schema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

            schema.setSerialNo(serNo);
            schema.setFeeCode(tLMRiskFeeSet.get(i).getFeeCode());
            schema.setFee(fee);
            schema.setOtherNo(this.getBalanceSequenceNo());
            schema.setOtherType("10"); //保全解约结算
            schema.setFeeRate(tLMRiskFeeSet.get(i).getFeeValue());
            schema.setPayDate(tEndDate);
            schema.setState("0");

            String moneyType = CommonBL.getCodeName("accmanagefee",
                    schema.getFeeCode());
            if (moneyType == null)
            {
                mErrors.addOneError("请先描述管理费的财务类型");
                return null;
            }
            schema.setMoneyType(moneyType);
            if (fee > 0 && !moneyType.equals("B"))
            {
                tMMap.put(schema, SysConst.INSERT);
            }
            //更新ClassFee
            set.get(1).setFee(set.get(1).getFee() + fee);
            set.get(1).setBalaDate(schema.getPayDate());
            set.get(1).setOperator(mGI.Operator);
            set.get(1).setModifyDate(PubFun.getCurrentDate());
            set.get(1).setModifyTime(PubFun.getCurrentTime());

            //管理费的账户轨迹
            LPInsureAccTraceSchema traceSchemaP = new LPInsureAccTraceSchema();
            ref.transFields(traceSchemaP, schema);
            //只有持续奖金大于零的时候才进账户轨迹表。
            if (fee > 0 && moneyType.equals("B"))
            {
                traceSchemaP.setMoney(Math.abs(schema.getFee()));
            } else if (!moneyType.equals("B"))
            {
                traceSchemaP.setMoney( -Math.abs(schema.getFee()));
                //税优退未满期净风险保费
                if(null != sy && !"".equals(sy) && tLMRiskFeeSet.get(i).getFeeCode().equals("T00001")){
                	traceSchemaP.setMoney(Math.abs(schema.getFee()));
                }
            }

            String tLimit2 = PubFun.getNoLimit(traceSchemaP.getManageCom());
            String serNo2 = PubFun1.CreateMaxNo("SERIALNO", tLimit2);

            traceSchemaP.setSerialNo(serNo2);
            tMMap.put(traceSchemaP, SysConst.INSERT);

            //更新账户分类轨迹
            cLPInsureAccClassSchema.setInsuAccBala(cLPInsureAccClassSchema.
                    getInsuAccBala()
                    + traceSchemaP.getMoney());
            cLPInsureAccClassSchema.setLastAccBala(cLPInsureAccClassSchema.
                    getInsuAccBala());
        }

        tMMap.put(set.get(1), SysConst.INSERT);

        return tMMap;
    }
    
    /**
     * 计算子帐户管理费子方法，供有缓交记录的保单结算使用
     * 取LMRiskFee对应账户的FeeTakePlace=5的管理费描述进行计算。
     * @param LCInsureAccClassSchema LCInsureAccClassSchema
     * @return MMap
     */
    private MMap balanceOneAccManageFeePNew(LPInsureAccClassSchema
                                         cLPInsureAccClassSchema,
                                         TransferData tTransferData)
    {
        //计算管理费
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(cLPInsureAccClassSchema.getInsuAccNo());
        tLMRiskFeeDB.setPayPlanCode(cLPInsureAccClassSchema.getPayPlanCode());
        tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        if (tLMRiskFeeDB.mErrors.needDealError())
        {
            System.out.println(tLMRiskFeeDB.mErrors.getErrContent());
            mErrors.addOneError("查询管理费时出现异常");
            return null;
        }

//        tLMRiskFeeSet.add(getLMRiskFeeSetBP(cLPInsureAccClassSchema,
//                                            tTransferData));

        String tEndDate = (String) tTransferData.getValueByName("EndDate");
        if (tEndDate == null)
        {
            mErrors.addOneError("请传入截至日期");
            return null;
        }
        String tStartDate = (String) tTransferData.getValueByName("StartDate");
        LCInsureAccClassSchema tempAccSchema=new LCInsureAccClassSchema();
        ref.transFields(tempAccSchema, cLPInsureAccClassSchema);
        //获取结算区间
        LMInsuAccRateSet tLMInsuAccRateSet=AccountManage.getBalanceRate(tempAccSchema,
        											tStartDate,tEndDate,"2");
        if(tLMInsuAccRateSet.size()==0){
        	mErrors.addOneError("账户结算区间拆分失败！");
            return null;
        }

        LPInsureAccClassFeeBL tLPInsureAccClassFeeBL = new
                LPInsureAccClassFeeBL();
        LPInsureAccClassFeeSet set = tLPInsureAccClassFeeBL.
                                     queryAllLPInsureAccClassFee(
                                             mLPEdorItemSchema);
        if (set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户管理费分类信息");
            return null;
        }

        MMap tMMap = new MMap();

        //计算管理费
        for (int i = 1; i <= tLMRiskFeeSet.size(); i++)
        {
        	double fee=0;
            String subRiskCode = ""; 
            //对每一笔管理费分结算区间处理
        	for(int j=1;j<=tLMInsuAccRateSet.size();j++){
        		LMInsuAccRateSchema tRate=tLMInsuAccRateSet.get(j);
            	tTransferData.removeByName("StartDate");
            	tTransferData.removeByName("EndDate");
            	System.out.println(tRate.getStartBalaDate()+","+tRate.getBalaDate());
            	tTransferData.setNameAndValue("StartDate", tRate.getStartBalaDate());
            	tTransferData.setNameAndValue("EndDate", tRate.getBalaDate());
            	//缓交区间不收主附险的风险保费
            	String str=tLMRiskFeeSet.get(i).getFeeName();
            	int isRP=str.indexOf("风险");//没有字段能准确 标识，只能用模糊的方式来查是否为风险保费了，尴尬。。。
            	if("2".equals(tRate.getRateType()) 
            		&& isRP>=0){
            		subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
            		continue;
            	}
        		
	            Calculator tCalculator = new Calculator();
	            tCalculator.setCalCode(tLMRiskFeeSet.get(i).getFeeCalCode());
	            tCalculator.addBasicFactor("ContNo",
	                                       cLPInsureAccClassSchema.getContNo());
	            tCalculator.addBasicFactor("PolNo",
	                                       cLPInsureAccClassSchema.getPolNo());
	            tCalculator.addBasicFactor("InsuredNo",
	                                       cLPInsureAccClassSchema.getInsuredNo());
	            tCalculator.addBasicFactor("InsuAccNo",
	                                       cLPInsureAccClassSchema.getInsuAccNo());
	            tCalculator.addBasicFactor("PayPlanCode",
	                                       cLPInsureAccClassSchema.getPayPlanCode());
	            //新增变量Prem，支持常无忧万能的持续奖励计算。 added by huxl @20080616
	            LPPolDB tLPPolDB = new LPPolDB();
	            tLPPolDB.setPolNo(cLPInsureAccClassSchema.getPolNo());
	            tLPPolDB.setEdorNo(cLPInsureAccClassSchema.getEdorNo());
	            tLPPolDB.setEdorType(cLPInsureAccClassSchema.getEdorType());
	            tLPPolDB.getInfo();
	            tCalculator.addBasicFactor("Prem", String.valueOf(tLPPolDB.getPrem()));
	            tCalculator.addBasicFactor("Sex", tLPPolDB.getInsuredSex());
	            tCalculator.addBasicFactor("Amnt", String.valueOf(tLPPolDB.getAmnt()));
	            tCalculator.addBasicFactor("CValiDate", String.valueOf(tLPPolDB.getCValiDate()));
	            
	            //20120130 新增变量 持续奖金计算终止日期
	            tCalculator.addBasicFactor("CalEndDate", tEndDate);
	            //subRiskCode ,附加险险种代码，存储在附加险月结风险保费trace的PayPlanCode字段
				if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
				{
					subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
					LCPolDB subLCPolDB = new LCPolDB();
					subLCPolDB.setContNo(cLPInsureAccClassSchema.getContNo());
					subLCPolDB.setMainPolNo(cLPInsureAccClassSchema.getPolNo());
					subLCPolDB.setRiskCode(subRiskCode);
					LCPolSet subLCPolSet = subLCPolDB.query();
					if(subLCPolSet==null || subLCPolSet.size()==0)
					{
						continue;
					}
					else
					{
						LCPolSchema subLCPolSchema = subLCPolSet.get(1).getSchema();
						if(("231001".equals(subLCPolSchema.getRiskCode()) || "231201".equals(subLCPolSchema.getRiskCode())|| "231801".equals(subLCPolSchema.getRiskCode()))
								&& !"1".equals(subLCPolSchema.getStateFlag()))
						{
							continue;
						}
						String tSubAmnt = String.valueOf(subLCPolSchema.getAmnt());
						tCalculator.addBasicFactor("SubAmnt", tSubAmnt);
						
						String sql = "select Rate from LCPrem where PolNo = '" + subLCPolSchema.getPolNo() 
							+ "' and PayPlanCode like '00000%' order by PayPlanCode desc " ;
						String tRiskRate = new ExeSQL().getOneValue(sql);
						if(tRiskRate==null || tRiskRate.equals(""))
						{
							tCalculator.addBasicFactor("RiskRate", "0");
						}
						else
						{
							tCalculator.addBasicFactor("RiskRate", tRiskRate);
						}
					}
				}
				 //2012-03-14 增加给付持续奖金特殊情况的校验
	            System.out.println(tLMRiskFeeSet.get(i).getFeeCode());
	           if(tLMRiskFeeSet.get(i).getFeeCode().equals("000004"))
	            {
	            	  tCalculator.addBasicFactor("PolMonth", "12");
	            }
	            else
	            {
	            	  tCalculator.addBasicFactor("PolMonth", String.valueOf(mPolMonth));
	            }
	            //*********************************************
	
	            setCalculatorFactor(tTransferData, tCalculator);
	
	            
	            AccountManage tAccountManage = new AccountManage();
	            String tInsuredAppAge = tAccountManage
	                                    .getInsuredAppAgeP(mStartDate,
	                    cLPInsureAccClassSchema);
	            if (tInsuredAppAge == null)
	            {
	                mErrors.copyAllErrors(tAccountManage.mErrors);
	                return null;
	            }
	            tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);
	            fee += Math.abs(PubFun.setPrecision(
	                    Double.parseDouble(tCalculator.calculate()), "0.0000"));
	            if (tCalculator.mErrors.needDealError())
	            {
	                System.out.println(tCalculator.mErrors.getErrContent());
	                mErrors.addOneError("计算管理费出错");
	                return null;
	            }
        	}
        	fee =PubFun.setPrecision(fee,"0.00");
            //管理费轨迹
            LPInsureAccFeeTraceSchema schema = new LPInsureAccFeeTraceSchema();
            ref.transFields(schema, cLPInsureAccClassSchema);
            
            if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
            	schema.setPayPlanCode(subRiskCode);
			}

            String tLimit = PubFun.getNoLimit(schema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

            schema.setSerialNo(serNo);
            schema.setFeeCode(tLMRiskFeeSet.get(i).getFeeCode());
            schema.setFee(fee);
            schema.setOtherNo(this.getBalanceSequenceNo());
            schema.setOtherType("10"); //保全解约结算
            schema.setFeeRate(tLMRiskFeeSet.get(i).getFeeValue());
            schema.setPayDate(tEndDate);
            schema.setState("0");

            String moneyType = CommonBL.getCodeName("accmanagefee",
                    schema.getFeeCode());
            if (moneyType == null)
            {
                mErrors.addOneError("请先描述管理费的财务类型");
                return null;
            }
            schema.setMoneyType(moneyType);
            if (fee > 0 && !moneyType.equals("B"))
            {
                tMMap.put(schema, SysConst.INSERT);
            }
            //更新ClassFee
            set.get(1).setFee(set.get(1).getFee() + fee);
            set.get(1).setBalaDate(schema.getPayDate());
            set.get(1).setOperator(mGI.Operator);
            set.get(1).setModifyDate(PubFun.getCurrentDate());
            set.get(1).setModifyTime(PubFun.getCurrentTime());

            //管理费的账户轨迹
            LPInsureAccTraceSchema traceSchemaP = new LPInsureAccTraceSchema();
            ref.transFields(traceSchemaP, schema);
            //只有持续奖金大于零的时候才进账户轨迹表。
            if (fee > 0 && moneyType.equals("B"))
            {
                traceSchemaP.setMoney(Math.abs(schema.getFee()));
            } else if (!moneyType.equals("B"))
            {
                traceSchemaP.setMoney( -Math.abs(schema.getFee()));
            }

            String tLimit2 = PubFun.getNoLimit(traceSchemaP.getManageCom());
            String serNo2 = PubFun1.CreateMaxNo("SERIALNO", tLimit2);

            traceSchemaP.setSerialNo(serNo2);
            tMMap.put(traceSchemaP, SysConst.INSERT);

            //更新账户分类轨迹
            cLPInsureAccClassSchema.setInsuAccBala(cLPInsureAccClassSchema.
                    getInsuAccBala()
                    + traceSchemaP.getMoney());
            cLPInsureAccClassSchema.setLastAccBala(cLPInsureAccClassSchema.
                    getInsuAccBala());
        }

        tMMap.put(set.get(1), SysConst.INSERT);

        return tMMap;
    }
    
    /**
     * 计算子帐户管理费子方法，供有失效记录的保单结算使用
     * 取LMRiskFee对应账户的FeeTakePlace=5的管理费描述进行计算。
     * @param LCInsureAccClassSchema LCInsureAccClassSchema
     * @return MMap
     */
    private MMap balanceOneAccManageFeePSub(LPInsureAccClassSchema
                                         cLPInsureAccClassSchema,
                                         TransferData tTransferData)
    {
        //计算管理费
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(cLPInsureAccClassSchema.getInsuAccNo());
        tLMRiskFeeDB.setPayPlanCode(cLPInsureAccClassSchema.getPayPlanCode());
        tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        if (tLMRiskFeeDB.mErrors.needDealError())
        {
            System.out.println(tLMRiskFeeDB.mErrors.getErrContent());
            mErrors.addOneError("查询管理费时出现异常");
            return null;
        }

//        tLMRiskFeeSet.add(getLMRiskFeeSetBP(cLPInsureAccClassSchema,
//                                            tTransferData));

        String tEndDate = (String) tTransferData.getValueByName("EndDate");
        if (tEndDate == null)
        {
            mErrors.addOneError("请传入截至日期");
            return null;
        }
        String tStartDate = (String) tTransferData.getValueByName("StartDate");
        LCInsureAccClassSchema tempAccSchema=new LCInsureAccClassSchema();
        ref.transFields(tempAccSchema, cLPInsureAccClassSchema);
        //获取结算区间
        LMInsuAccRateSet tLMInsuAccRateSet=AccountManage.getBalanceRate1(tempAccSchema,
        											tStartDate,tEndDate,"2");
        if(tLMInsuAccRateSet.size()==0){
        	mErrors.addOneError("账户结算区间拆分失败！");
            return null;
        }

        LPInsureAccClassFeeBL tLPInsureAccClassFeeBL = new
                LPInsureAccClassFeeBL();
        LPInsureAccClassFeeSet set = tLPInsureAccClassFeeBL.
                                     queryAllLPInsureAccClassFee(
                                             mLPEdorItemSchema);
        if (set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户管理费分类信息");
            return null;
        }

        MMap tMMap = new MMap();

        //计算管理费
        for (int i = 1; i <= tLMRiskFeeSet.size(); i++)
        {
        	double fee=0;
            String subRiskCode = ""; 
            //对每一笔管理费分结算区间处理
        	for(int j=1;j<=tLMInsuAccRateSet.size();j++){
				LMInsuAccRateSchema tRate = tLMInsuAccRateSet.get(j);
				tTransferData.removeByName("StartDate");
				tTransferData.removeByName("EndDate");
				System.out.println(tRate.getStartBalaDate() + ","
						+ tRate.getBalaDate());
				tTransferData.setNameAndValue("StartDate", tRate
						.getStartBalaDate());
				tTransferData.setNameAndValue("EndDate", tRate.getBalaDate());
				// 缓交区间不收主附险的风险保费
				String str = tLMRiskFeeSet.get(i).getFeeName();
				int isRP = str.indexOf("风险");// 没有字段能准确
												// 标识，只能用模糊的方式来查是否为风险保费了，尴尬。。。
				if ("2".equals(tRate.getRateType()) && isRP >= 0) {
					subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
					continue;
				}

				Calculator tCalculator = new Calculator();
				tCalculator.setCalCode(tLMRiskFeeSet.get(i).getFeeCalCode());
				tCalculator.addBasicFactor("ContNo", cLPInsureAccClassSchema
						.getContNo());
				tCalculator.addBasicFactor("PolNo", cLPInsureAccClassSchema
						.getPolNo());
				tCalculator.addBasicFactor("InsuredNo", cLPInsureAccClassSchema
						.getInsuredNo());
				tCalculator.addBasicFactor("InsuAccNo", cLPInsureAccClassSchema
						.getInsuAccNo());
				tCalculator.addBasicFactor("PayPlanCode",
						cLPInsureAccClassSchema.getPayPlanCode());
				// 新增变量Prem，支持常无忧万能的持续奖励计算。 added by huxl @20080616
				LPPolDB tLPPolDB = new LPPolDB();
				tLPPolDB.setPolNo(cLPInsureAccClassSchema.getPolNo());
				tLPPolDB.setEdorNo(cLPInsureAccClassSchema.getEdorNo());
				tLPPolDB.setEdorType(cLPInsureAccClassSchema.getEdorType());
				tLPPolDB.getInfo();
				tCalculator.addBasicFactor("Prem", String.valueOf(tLPPolDB
						.getPrem()));
				tCalculator.addBasicFactor("Sex", tLPPolDB.getInsuredSex());
				tCalculator.addBasicFactor("Amnt", String.valueOf(tLPPolDB
						.getAmnt()));
				tCalculator.addBasicFactor("CValiDate", String.valueOf(tLPPolDB
						.getCValiDate()));

				// 20120130 新增变量 持续奖金计算终止日期
				tCalculator.addBasicFactor("CalEndDate", tEndDate);
				// subRiskCode ,附加险险种代码，存储在附加险月结风险保费trace的PayPlanCode字段
				if (tLMRiskFeeSet.get(i).getFeeItemType().equals("07")) 
				{
					subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
					LCPolDB subLCPolDB = new LCPolDB();
					subLCPolDB.setContNo(cLPInsureAccClassSchema.getContNo());
					subLCPolDB.setMainPolNo(cLPInsureAccClassSchema.getPolNo());
					subLCPolDB.setRiskCode(subRiskCode);
					LCPolSet subLCPolSet = subLCPolDB.query();
					if (subLCPolSet == null || subLCPolSet.size() == 0) 
					{
						continue;
					} 
					else 
					{
						LCPolSchema subLCPolSchema = subLCPolSet.get(1).getSchema();
						if (("231001".equals(subLCPolSchema.getRiskCode()) || "231201".equals(subLCPolSchema.getRiskCode()) || "231801".equals(subLCPolSchema.getRiskCode()))
								&& !"1".equals(subLCPolSchema.getStateFlag())) 
						{
							continue;
						}
						String tSubAmnt = String.valueOf(subLCPolSchema
								.getAmnt());
						tCalculator.addBasicFactor("SubAmnt", tSubAmnt);

						String sql = "select Rate from LCPrem where PolNo = '"+ subLCPolSchema.getPolNo()
								+ "' and PayPlanCode like '00000%' order by PayPlanCode desc ";
						String tRiskRate = new ExeSQL().getOneValue(sql);
						if (tRiskRate == null || tRiskRate.equals("")) 
						{
							tCalculator.addBasicFactor("RiskRate", "0");
						} 
						else 
						{
							tCalculator.addBasicFactor("RiskRate", tRiskRate);
						}
					}
				}
				// 2012-03-14 增加给付持续奖金特殊情况的校验
				System.out.println(tLMRiskFeeSet.get(i).getFeeCode());
				if (tLMRiskFeeSet.get(i).getFeeCode().equals("000004")) 
				{
					tCalculator.addBasicFactor("PolMonth", "12");
				} 
				else 
				{
					tCalculator.addBasicFactor("PolMonth", String
							.valueOf(mPolMonth));
				}
				// *********************************************

				setCalculatorFactor(tTransferData, tCalculator);

				AccountManage tAccountManage = new AccountManage();
            	String tInsuredAppAge = tAccountManage
            							.getInsuredAppAgeP(mStartDate,
            							cLPInsureAccClassSchema);
				if (tInsuredAppAge == null) 
				{
					mErrors.copyAllErrors(tAccountManage.mErrors);
					return null;
				}
				tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);
				fee += Math.abs(PubFun.setPrecision(Double
						.parseDouble(tCalculator.calculate()), "0.0000"));
				if (tCalculator.mErrors.needDealError()) 
				{
					System.out.println(tCalculator.mErrors.getErrContent());
					mErrors.addOneError("计算管理费出错");
					return null;
				}
			}
        	fee =PubFun.setPrecision(fee,"0.00");
            //管理费轨迹
            LPInsureAccFeeTraceSchema schema = new LPInsureAccFeeTraceSchema();
            ref.transFields(schema, cLPInsureAccClassSchema);
            
            if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
            	schema.setPayPlanCode(subRiskCode);
			}

            String tLimit = PubFun.getNoLimit(schema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

            schema.setSerialNo(serNo);
            schema.setFeeCode(tLMRiskFeeSet.get(i).getFeeCode());
            schema.setFee(fee);
            schema.setOtherNo(this.getBalanceSequenceNo());
            schema.setOtherType("10"); //保全解约结算
            schema.setFeeRate(tLMRiskFeeSet.get(i).getFeeValue());
            schema.setPayDate(tEndDate);
            schema.setState("0");

            String moneyType = CommonBL.getCodeName("accmanagefee",
                    schema.getFeeCode());
            if (moneyType == null)
            {
                mErrors.addOneError("请先描述管理费的财务类型");
                return null;
            }
            schema.setMoneyType(moneyType);
            if (fee > 0 && !moneyType.equals("B"))
            {
                tMMap.put(schema, SysConst.INSERT);
            }
            //更新ClassFee
            set.get(1).setFee(set.get(1).getFee() + fee);
            set.get(1).setBalaDate(schema.getPayDate());
            set.get(1).setOperator(mGI.Operator);
            set.get(1).setModifyDate(PubFun.getCurrentDate());
            set.get(1).setModifyTime(PubFun.getCurrentTime());

            //管理费的账户轨迹
            LPInsureAccTraceSchema traceSchemaP = new LPInsureAccTraceSchema();
            ref.transFields(traceSchemaP, schema);
            //只有持续奖金大于零的时候才进账户轨迹表。
            if (fee > 0 && moneyType.equals("B"))
            {
                traceSchemaP.setMoney(Math.abs(schema.getFee()));
            } else if (!moneyType.equals("B"))
            {
                traceSchemaP.setMoney( -Math.abs(schema.getFee()));
            }

            String tLimit2 = PubFun.getNoLimit(traceSchemaP.getManageCom());
            String serNo2 = PubFun1.CreateMaxNo("SERIALNO", tLimit2);

            traceSchemaP.setSerialNo(serNo2);
            tMMap.put(traceSchemaP, SysConst.INSERT);

            //更新账户分类轨迹
            cLPInsureAccClassSchema.setInsuAccBala(cLPInsureAccClassSchema.
                    getInsuAccBala()
                    + traceSchemaP.getMoney());
            cLPInsureAccClassSchema.setLastAccBala(cLPInsureAccClassSchema.
                    getInsuAccBala());
        }

        tMMap.put(set.get(1), SysConst.INSERT);

        return tMMap;
    }

//    /**
//     * getLMRiskFeeSetB
//     *
//     * @param cLCInsureAccClassSchema LCInsureAccClassSchema
//     * @return Schema
//     */
//    private LMRiskFeeSet getLMRiskFeeSetB(LCInsureAccClassSchema
//                                          cLCInsureAccClassSchema,
//                                          TransferData tTransferData)
//    {
////        String tYear = (String) tTransferData.getValueByName("PolYear");
//        String tMonth = (String) tTransferData.getValueByName("PolMonth");
//        //满保单年度时才处理持续奖金.
//        if (!"12".equals(tMonth))
//        {
//        	return new LMRiskFeeSet();
//        }
//        
//        //由于通过产品描述来判断较为复杂,从ldcode中描述中判断是否支持险种的持续奖金配置
//        ExeSQL texesql = new ExeSQL();
//		String tIsUliB = texesql
//				.getOneValue("select 1 from ldcode where  codetype='uli_b' and code='"+cLCInsureAccClassSchema.getRiskCode()+"' ");
//		if ((tIsUliB == null) || (tIsUliB.equals(""))) {
//			return new LMRiskFeeSet();
//		}
//        
///*        //由于通过产品描述来判断较为复杂,暂时写死,先开发331201的持续奖金,后续调整为ldcode中描述
//        if(!cLCInsureAccClassSchema.getRiskCode().equals("331201")){
//        	return new LMRiskFeeSet();
//        }*/
//		
//        String tIsDead=new ExeSQL().getOneValue("select polno from ljagetclaim a where a.othernotype='5' " +
//        								" and exists (select 1 from ljagetclaim where actugetno=a.actugetno and feefinatype='SW') " +
//        								" and polno = '"+cLCInsureAccClassSchema.getPolNo()+"'  group by a.polno having sum(a.pay) >0 with ur ");
//        //判断是否已经身故,通过是否发生身故理赔来判断
//        if(tIsDead!=null&&(!tIsDead.equals(""))&&(!tIsDead.equals("null"))){
//        	return new LMRiskFeeSet();
//        }
//        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
//        tLMRiskFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
//        tLMRiskFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
//        tLMRiskFeeDB.setFeeTakePlace("07"); //每月扣除
//        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
//        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
//
//        return tLMRiskFeeSet;
//    }

//    private LMRiskFeeSet getLMRiskFeeSetBP(LPInsureAccClassSchema
//                                           cLPInsureAccClassSchema,
//                                           TransferData tTransferData)
//    {
////        String tYear = (String) tTransferData.getValueByName("PolYear");
//        String tMonth = (String) tTransferData.getValueByName("PolMonth");
//
//        String exSQLduebaladate="select polyear,polmonth from LCInsureAccBalance a where contno = '"+cLPInsureAccClassSchema.getContNo()+"' and duebaladate='"+mStartDate+"'";
//        System.out.println(exSQLduebaladate);
//        SSRS tSSRS1 = new ExeSQL().execSQL(exSQLduebaladate);
//        int tPolYear = Integer.parseInt(tSSRS1.GetText(1, 1));
//        int tPolMonth = Integer.parseInt(tSSRS1.GetText(1, 2));
//        
//        String exSQLcount="select year(edorvalidate-date('" + mStartDate +"')),month(edorvalidate-date('" + mStartDate +"')) from lpedoritem a " +
//		" where contno ='"+cLPInsureAccClassSchema.getContNo()+"'" +
//	    " and edorno='"+cLPInsureAccClassSchema.getEdorNo()+"' with ur ";
//        System.out.println(exSQLcount);
//        SSRS tSSRS2 = new ExeSQL().execSQL(exSQLcount);
//        int S_count_year = Integer.parseInt(tSSRS2.GetText(1, 1));
//        int S_count_month = Integer.parseInt(tSSRS2.GetText(1, 2));
//        
//        int tCompare=Integer.parseInt(tMonth)+S_count_year*12+S_count_month;
//        if(tCompare>=12)
//        {
//        	tMonth="12";
//        }
////        String exSQLlastpaytodate="select max(lastpaytodate) from ljapayperson a " +
////        		" where contno ='"+cLPInsureAccClassSchema.getContNo()+"'" +
////        	    " and exists (select 1 from ljspayb where getnoticeno=a.getnoticeno and dealstate='1')  with ur ";        
////        String tMAXlastpaytodate = new ExeSQL().getOneValue(exSQLlastpaytodate);
//     
//        //满保单年度时才处理持续奖金.
//        if (!"12".equals(tMonth))
//        {
//        	return new LMRiskFeeSet();
//        }
//
//      //由于通过产品描述来判断较为复杂,从ldcode中描述中判断是否支持险种的持续奖金配置
//        ExeSQL texesql = new ExeSQL();
//		String tIsUliB = texesql
//				.getOneValue("select 1 from ldcode where  codetype='uli_b' and code='"+cLPInsureAccClassSchema.getRiskCode()+"' ");
//		if ((tIsUliB == null) || (tIsUliB.equals(""))) {
//			return new LMRiskFeeSet();
//		}
//        
//        String tIsDead=new ExeSQL().getOneValue("select polno from ljagetclaim a where a.othernotype='5' " +
//				" and exists (select 1 from ljagetclaim where actugetno=a.actugetno and feefinatype='SW') " +
//				" and polno = '"+cLPInsureAccClassSchema.getPolNo()+"'  group by a.polno having sum(a.pay) >0 with ur ");
//        //判断是否已经身故,通过是否发生身故理赔来判断
//        if(tIsDead!=null&&(!tIsDead.equals(""))&&(!tIsDead.equals("null"))){
//        	return new LMRiskFeeSet();
//        }
//        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
//        tLMRiskFeeDB.setInsuAccNo(cLPInsureAccClassSchema.getInsuAccNo());
//        tLMRiskFeeDB.setPayPlanCode(cLPInsureAccClassSchema.getPayPlanCode());
//        tLMRiskFeeDB.setFeeTakePlace("07"); //每月扣除
//        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
//        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
//
//        return tLMRiskFeeSet;
//    }


    private void setCalculatorFactor(TransferData tTransferData,
                                     Calculator tCalculator)
    {
        Vector tVector = tTransferData.getValueNames();
        for (int i = 0; i < tVector.size(); i++)
        {
            Object tNameObject = null;
            Object tValueObject = null;
            try
            {
                tNameObject = tVector.get(i);
                tValueObject = tTransferData.getValueByName(tNameObject);
                tCalculator.addBasicFactor((String) tNameObject,
                                           (String) tValueObject);
            } catch (Exception ex)
            {
                //对象无法转换为字符串，不需要做处理
            }
        }
    }

    /**
     * 得到结算序号
     * @return String
     */
    public String getBalanceSequenceNo()
    {
        if (mBalanceSeq == null)
        {
            String[] dateArr = PubFun.getCurrentDate().split("-");
            String date = dateArr[0] + dateArr[1] + dateArr[2];
            mBalanceSeq = date
                          + PubFun1.CreateMaxNo("BalaSeq" + date, 10);
        }

        return mBalanceSeq;
    }

    /**
     * getInsuAccRateUnBalance
     * 查询未结算的月收益利率
     * @param cLCInsureAccClassSchema LCInsureAccClassSchema，子账户
     * @return LMInsuAccRateSet，月收益率
     */
    private LMInsuAccRateSet getInsuAccRateUnBalance(LCInsureAccSchema
            cLCInsureAccSchema)
    {
        String sql = " select a.* from LMInsuAccRate a "
                     + "where 1 = 1 "
                     + "   and RateType = 'C' "
                     + "   and RateIntv = 1 "
                     + "   and RateIntvUnit = 'Y' "
                     + "   and a.BalaDate <= current date "
                     + "   and a.BalaDate > '"
                     + cLCInsureAccSchema.getBalaDate() + "' "
                     + "   and RiskCode = '"
                     + cLCInsureAccSchema.getRiskCode() + "' "
                     + "   and InsuAccNo = '"
                     + cLCInsureAccSchema.getInsuAccNo() + "' "
                     + "order by a.BalaDate ";
        System.out.println(sql);
        LMInsuAccRateDB tLMInsuAccRateDB = new LMInsuAccRateDB();
        LMInsuAccRateSet tLMInsuAccRateSet = tLMInsuAccRateDB.executeQuery(sql);
        if (tLMInsuAccRateDB.mErrors.needDealError())
        {
            System.out.println(tLMInsuAccRateDB.mErrors.getErrContent());
            mErrors.addOneError("查询符合该账户的月收益率发生异常");
            return null;
        }
        if (tLMInsuAccRateSet.size() == 0)
        {
            mErrors.addOneError("没有查询到符合该账户的月收益率，不能进行结算");
            return null;
        }

        return tLMInsuAccRateSet;
    }

    private LCInsureAccClassSet getAccClass(LCInsureAccSchema
                                            tLCInsureAccSchema)
    {
        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
        tLCInsureAccClassDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
        LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
        if (tLCInsureAccClassDB.mErrors.needDealError())
        {
            CError.buildErr(this, "子帐户信息查询失败");
            return null;
        }
        if (tLCInsureAccClassSet == null || tLCInsureAccClassSet.size() < 1)
        {
            CError.buildErr(this, "没有子帐户信息");
            return null;
        }
        return tLCInsureAccClassSet;
    }

    /**
     * 传递参数
     * 准备账户号码和账户结算日期
     * @param cInputData VData
     * @return boolean
     */
    public boolean getInputData(VData cInputData)
    {
        mGI = (GlobalInput) cInputData
              .getObjectByObjectName("GlobalInput", 0);
        mLCInsureAccSchema = (LCInsureAccSchema) cInputData
                             .getObjectByObjectName("LCInsureAccSchema", 0);
        mLPInsureAccSchema = (LPInsureAccSchema) cInputData
                             .getObjectByObjectName("LPInsureAccSchema", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData
                            .getObjectByObjectName("LPEdorItemSchema", 0);
        TransferData tf = (TransferData) cInputData
                          .getObjectByObjectName("TransferData", 0);
        if (tf != null)
        {
            mEndDate = (String) tf.getValueByName("EndDate");
            mBalanceSeq = (String) tf.getValueByName("EdorNo");
        }

        if (mGI == null)
        {
            buildError("getInputData", "请传入操作员信息");
            return false;
        }
//        if (mLCInsureAccSchema == null)
//        {
//            buildError("getInputData", "请传入账户信息");
//            return false;
//        }
        if(mLCInsureAccSchema != null)
        {
        	LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(mLCInsureAccSchema.getPolNo());
            if(tLCPolDB.getInfo())
            {
            	mLCPolSchema = tLCPolDB.getSchema();
            }
        }
//        else if(mLPInsureAccSchema != null)
//        {
//        	LCPolDB tLCPolDB = new LCPolDB();
//            tLCPolDB.setPolNo(mLPInsureAccSchema.getPolNo());
//            if(tLCPolDB.getInfo())
//            {
//            	mLCPolSchema = tLCPolDB.getSchema();
//            }
//        }
        
        
        return true;
    }

    /**
     * 错误构建方法
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "InsuAccBala";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * 锁定动作
     * @param polBalaCount
     * @return MMap
     */
    private MMap lockBalaCount(String polBalaCount)
    {
        MMap tMMap = null;
        /**万能险月结算"UJ"*/
        String tLockNoType = "UJ";
        /**锁定有效时间（秒）*/
        String tAIS = "600";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", polBalaCount);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGI);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }

    public static void main(String arg[])
    {

		System.out.println(PubFun.calInterval("2011-8-18","2012-8-17", "Y"));
		System.out.println(PubFun.calInterval("2011-8-18","2012-8-18", "Y"));
		System.out.println(PubFun.calInterval("2011-8-18","2012-8-19", "Y"));
    }
}
