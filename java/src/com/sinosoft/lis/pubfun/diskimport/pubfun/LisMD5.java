/**
 * 2008-12-19
 */
package com.sinosoft.lis.pubfun;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5加密类
 * 
 * @author LY
 *
 */
public class LisMD5
{
    /**
     * MD5加密，返回byte[]。
     * @param cPlainStr
     * @return
     */
    public static final byte[] encrypt(String cPlainStr)
    {
        byte[] tBEncryptDatas = null;

        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(cPlainStr.getBytes());
            tBEncryptDatas = md.digest();
        }
        catch (NoSuchAlgorithmException e)
        {
            tBEncryptDatas = null;
            e.printStackTrace();
        }

        return tBEncryptDatas;
    }

    /**
     * MD5加密，返回十六进制字符串。
     * @param cPlainStr
     * @return
     */
    public static final String encryptString(String cPlainStr)
    {
        byte[] tBEncryptDatas = null;

        tBEncryptDatas = LisMD5.encrypt(cPlainStr);

        if (tBEncryptDatas == null)
            return null;

        return LisMD5.bytesHEX(tBEncryptDatas);
    }

    /**
     * 把byte转换为对应十六进制字符。
     * @param b
     * @return
     */
    public static String byteHEX(byte b)
    {
        char[] Digit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
                'B', 'C', 'D', 'E', 'F' };
        char[] ob = new char[2];
        ob[0] = Digit[(b >>> 4) & 0X0F];
        ob[1] = Digit[b & 0X0F];
        String s = new String(ob);
        return s;
    }

    /**
     * 把byte[]转换为对应十六进制字符串。
     * @param b
     * @return
     */
    public static String bytesHEX(byte[] b)
    {
        StringBuffer tStr = new StringBuffer();
        for (int i = 0; i < b.length; i++)
        {
            tStr.append(LisMD5.byteHEX(b[i]));
        }
        return tStr.toString();
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        String tStr = "S3000001820" + "123456";//MD5: DDC5AB710ACE219DE28EEBE35040B8E8
        System.out.println(LisMD5.encryptString(tStr));
    }
}
