package com.sinosoft.lis.pubfun;

import bsh.This;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 万能险保单账户结算 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author chenjianguo
 * @version 1.3
 * @CreateDate：2011-03-16
 */
public class UliBQInsuAccBala
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mInputData = null;
    /** 全局变量 */
    private GlobalInput mGI = new GlobalInput();
    private LCInsureAccSchema mLCInsureAccSchema = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;
    private String mBalanceSeq = null;
    private String mStartDate = null;
    private String mEndDate = null;
    private int mPolYear = 1; //保单年度
    private int mPolMonth = 1; //保单月度
    
    public String glFeeMode=""	;
    
    private String mPolno = null;
    private String mEdorNo=null;
    private String mEdorType=null;
    private String mInsuredNo=null;
    LDPromiseRateSet tLDPromiseRateSet=null;
    LDPromiseRateSchema tLDPromiseRateSchema=null;
    
    private Reflections ref = new Reflections();
	public MMap tPerMMap = new MMap();
    //保存公共账户金额
    private String GGSequenceno = "";//公共账户是否已处理完存库，""为未存库，非""已存库。
    private String GGBalaCoutn = "";
    //记录结息后的子账户信息
    public LCInsureAccClassSet mLCInsureAccClassSet =  new LCInsureAccClassSet();//记录子账户结息后的信息
    
    public LCInsureAccTraceSet mLCInsureAccTraceSet=  new LCInsureAccTraceSet();//记录子账户结息后的信息
    
    public LPInsureAccTraceSet mLPInsureAccTraceSet = new LPInsureAccTraceSet();//记录轨迹信息表
    
    public LPInsureAccClassSet mLPInsureAccClassSet = new LPInsureAccClassSet();//记录保全账户分类表
    
    //下面几个用来存公共账户的信息表（分类表,管理费表）
    LCInsureAccSchema mGGLCInsureAccSchema = new LCInsureAccSchema();//公共账户信息
    
    LCInsureAccClassSchema mGGLCInsureAccClassSchema = new LCInsureAccClassSchema();//公共账户信息
    
    LPInsureAccClassSchema mGGLPInsureAccClassSchema = new LPInsureAccClassSchema();//公共账户信息
    
    LPInsureAccClassFeeSchema  mLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();//公共账户信息
    
    LPInsureAccFeeSchema mLPInsureAccFeeSchema = new LPInsureAccFeeSchema();//公共账户信息
    
    LPInsureAccSchema mLPInsureAccSchema = new LPInsureAccSchema();//公共账户信息
    LCInsureAccClassSet mGGLCInsureAccClassSet = new LCInsureAccClassSet() ;  
    LCInsureAccSet mGGLCInsureAccSet = new LCInsureAccSet() ;  
    private String mGrpContNo = "";//记录保单号
//    private LCInsureAccSchema mGGLCInsureAccSchema = new LCInsureAccSchema();//公共账户信息
//    private LCInsureAccClassSet mGGLCInsureAccClassSet = new LCInsureAccClassSet() ;

    public UliBQInsuAccBala()
    {
    }

    /**
     * submitData
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate,String grpcontno)
    {
        if (!getInputData(cInputData,grpcontno))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }
    
    /**
     * 传递参数
     * 准备账户号码和账户结算日期
     * @param cInputData VData
     * @return boolean
     */
    public boolean getInputData(VData cInputData,String grpcontno)
    {
    	
    	mInputData = (VData) cInputData.clone();
    	mLPEdorItemSchema = (LPEdorItemSchema) mInputData.
        getObjectByObjectName("LPEdorItemSchema", 0);
    	System.out.println("mLPEdorItemSchema"+mLPEdorItemSchema.getEdorNo());
    	mGI=((GlobalInput) mInputData.
           getObjectByObjectName("GlobalInput", 0));
        mGrpContNo = grpcontno;
        if(mGrpContNo == null || "".equals(mGrpContNo)){
        	buildError("getInputData", "获取团单号码失败！");
            return false;
        }   
        if (mLPEdorItemSchema!=null){
        mEndDate = mLPEdorItemSchema.getEdorValiDate();//初始化个人生效日期 
        System.out.println(mEndDate+"########################### 初始化个人生效日期   #############################");
        mEdorNo=mLPEdorItemSchema.getEdorNo(); //初始化保全受理号
        mEdorType=mLPEdorItemSchema.getEdorType();//初始化保全类型
        mInsuredNo=mLPEdorItemSchema.getInsuredNo();//初始化个人号
        }else{
        	buildError("getInputData", "获保全项目失败！");
            return false;
        }
        System.out.println("mEdorNo"+mEdorNo);
        //获取polno 因为PICC这边LPEdorItem 没有存polno ,默认为‘0000’;
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolDB  tLCPolDB=new LCPolDB();
        tLCPolDB.setGrpContNo(mGrpContNo);
        tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
        tLCPolDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        tLCPolSet=tLCPolDB.query();
        if (tLCPolSet.size()==0)
        {
        	buildError("getInputData", "查询待领取险种时失败！");
            return false;
        }
        mPolno=tLCPolSet.get(1).getPolNo();
        if ("".equals(mPolno))
        {
            buildError("getInputData", "查询到的险种号位空");
            return false;
        }
    	System.out.println("获取到的mPolno为"+mPolno);
    	
        if (mGI == null)
        {
            buildError("getInputData", "请传入操作员信息");
            return false;
        }
        return true;
    }


    /**
     * 业务处理这里需要保证利率来结息.
     * @return boolean
     */
    public boolean dealData()
    {
        //查询出该保单的管理费收取方式及金额
    	String FeeSql = "select feevalue,riskfeemode,feecode from lcgrpfee where grpcontno = '"+mGrpContNo+"' and insuaccno = '000000'";
    	SSRS FeeSSRS = new ExeSQL().execSQL(FeeSql);
    	if(FeeSSRS == null || FeeSSRS.getMaxRow()<=0 || FeeSSRS.getMaxRow()>1){
    		mErrors.addOneError("获取管理费收取方式及金额失败！");
            return false;
    	}
    	double glFee = Double.parseDouble(FeeSSRS.GetText(1, 1));//管理费金额
    	this.glFeeMode = FeeSSRS.GetText(1, 2);
    	String FeeCode = FeeSSRS.GetText(1, 3);
    	String Accsql ="";//查询保单最小的结算日期
    	Accsql = "select a.* from LCInsureAcc a where a.grpcontno = '"+mGrpContNo+"' and InsuredNo='"+mLPEdorItemSchema.getInsuredNo()+"' order by baladate";
    	System.out.println("Accsql"+Accsql);
    	LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
    	LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.executeQuery(Accsql);   	
    	if(tLCInsureAccSet.size()==0){
    		mErrors.addOneError("获取被保险的个人账户为空");
            return false;
    	}
    	//新增农民工险种根据印刷号查询保证利率
    	String prtNo = new ExeSQL().getOneValue("select prtno from lcgrpcont where grpcontno = '"+mGrpContNo+"'");
    	tLDPromiseRateSet = getLDPromiseRate(tLCInsureAccSet.get(1),prtNo);
        if(tLDPromiseRateSet==null){
        	return false;
        }
        tLDPromiseRateSchema=tLDPromiseRateSet.get(1);
       double GGAccValue = 0;
       GGSequenceno = "";//公共账户是否已处理完存库，""为未存库，非""已存库。
       GGBalaCoutn = "";
       String tRate =  String.valueOf(tLDPromiseRateSchema.getRate());
       String tRateIntv = tLDPromiseRateSchema.getRateIntv();
       String tRateType = tLDPromiseRateSchema.getRateType();
	   long t0 = System.currentTimeMillis();	
	   System.out.println("size"+tLCInsureAccSet.size());
       for(int a=1;a<=tLCInsureAccSet.size();a++){
		   mLCInsureAccSchema = tLCInsureAccSet.get(a);
    		if (mLCInsureAccSchema == null)
            {	
                return false;
            }
    		System.out.println("处理被保人" + mLCInsureAccSchema.getInsuredNo()
                    + ", 险种" + mLCInsureAccSchema.getRiskCode()+"开始结算"+mLCInsureAccSchema.getInsuAccNo()
                    + ", 结算起始日期"
                    + mLCInsureAccSchema.getBalaDate() + "的结算");
    		System.out.println("处理被保人" + mLCInsureAccSchema.getInsuredNo()
                    + ", 险种" + mLCInsureAccSchema.getRiskCode()
                    + ", 结算结算日期"
                    + mEndDate + "的结算");
    		mBalanceSeq = null; //由于月度不同，需要重新生成结算序号
          // 查询帐户子表信息
    		LCInsureAccClassSet tLCInsureAccClassSet = getAccClass(
    				mLCInsureAccSchema);
            if (tLCInsureAccClassSet == null)
            {
                return false;
            }
            LCInsureAccClassSchema schemaAV = null; //处理后的账户分类价值
            LCInsureAccTraceSchema traceschemaAV = null; //处理后的账户分类价值
            MMap tMMap1 = new MMap();
            for (int n = 1; n <= tLCInsureAccClassSet.size(); n++)
            {
                LCInsureAccClassSchema tLCInsureAccClassSchema
                        = tLCInsureAccClassSet.get(n);
                mStartDate = tLCInsureAccClassSchema.getBalaDate();
                System.out.println("mStartDate"+mStartDate);
                if (!calPolYearMonth())
                {
                    return false;
                }
                System.out.println("计算账户价值");
                MMap tMMapAV = dealOneAccClass(tLCInsureAccClassSchema,
                		tRateIntv, tRateType,tRate);
                if (tMMapAV == null)
                {
                    return false;
                }
                schemaAV = (LCInsureAccClassSchema) tMMapAV
                           .getObjectByObjectName("LCInsureAccClassSchema", 0);
                
                traceschemaAV = (LCInsureAccTraceSchema) tMMapAV
                .getObjectByObjectName("LCInsureAccTraceSchema", 0);
                if (schemaAV == null)
                {
                    mErrors.addOneError("没有计算出账户价值");
                    return false;
                }
                if (traceschemaAV == null)
                {
                    mErrors.addOneError("没有算出轨迹信息");
                    return false;
                }
              
                mLCInsureAccClassSet.add(schemaAV);//将结息后的子账户信息存入全局变量
                // 下面方法用来保存P表的记录 主要生成lpinsureaccclass，lpinsureaccclassfee                         
               if(!setPerACC(schemaAV,traceschemaAV,tMMap1)){
            	   return false;
               }
            }
            //下面方法用来执行P表ACC的记录 //主要生成ACC的记录
             if(!setACC(mLCInsureAccSchema,tMMap1)){
            	 return false;
             }
            tPerMMap.add(tMMap1);
	    	}//对个人的所有账户处理完毕
	    	System.out.println("个人结息处理完成，用时："+(System.currentTimeMillis() - t0));
	    	long t1 = System.currentTimeMillis();
	    	System.out.println("以下开始收取管理费");
	         //保全管理费按天数进行计算.
	    	int monthLength = PubFun.monthLength(PubFun.calDate(mEndDate, -1, "D", null));//获取结算月的天数 对于每月最后一天结算时需要减一天
	    	int tIntv = PubFun.calInterval(mStartDate, mEndDate, "D");
	    	System.out.println("扣管理费的天数"+tIntv);
	    	glFee=Arith.round(Arith.mul(glFee, Arith.div(tIntv, monthLength)), 2);
	   /* 	if(glFee>=0){
	    		System.out.println("天数一共有"+monthLength+"管理费一共收"+glFee);
	    	}else{
	    		 mErrors.addOneError("计算管理费错误，不能为负数");
	    		System.out.println("天数一共有"+monthLength+"管理费一共收"+glFee);
	    	}*/
	    	System.out.println("天数一共有"+tIntv+"管理费一共收"+glFee);
	    	System.out.println("管理费收取方式为"+glFeeMode);
            if("2".equals(glFeeMode)){// 从公共账户收取
    			System.out.println("公共账户的钱从库里面取出来的，公共账户不结息");
    			String GGAccSql = "select * from lcinsureacc where grpcontno = '"+mGrpContNo+"' and acctype = '001'";
    			LCInsureAccDB tGGLCInsureAccDB = new LCInsureAccDB();
    			mGGLCInsureAccSet = tGGLCInsureAccDB.executeQuery(GGAccSql);
    			if(mGGLCInsureAccSet.size()==0){
    				mErrors.addOneError("没有获取到公共账户信息！");
                    return false;
    			}
    			mGGLCInsureAccSchema = mGGLCInsureAccSet.get(1);
//    			//如果是公共账户的话，以为公共账户要扣除费用啊，所以这里要保存相关的信息
    			if(!"CT".equals(mLPEdorItemSchema.getEdorType()) && !"TG".equals(mLPEdorItemSchema.getEdorType())){
    			setPubACC();
    			}
         		mGGLCInsureAccClassSet= getAccClass(mGGLCInsureAccSchema);
         		if(mGGLCInsureAccClassSet==null){
         			return false;
         		}
         		//GGAccValue算出公共账户的总金额
    			GGAccValue = getAccBalaByAccClass(mGGLCInsureAccClassSet);
        		if(glFee>GGAccValue){
        			System.out.println("公共账户在扣除管理费时余额不足！");
        			glFeeMode = "1";
        		}
        	}
        	double temp = glFee;
        	boolean flag = true;
    		if("2".equals(glFeeMode)){//从公共账户扣除
    	    	for(int j=1;j<=mGGLCInsureAccClassSet.size();j++){
    	    		LCInsureAccClassSchema tGGLCInsureAccClassSchema = mGGLCInsureAccClassSet.get(j);//公共账户只有一个，子账户也有一个
        			MMap MMapTrace = getTrace(tGGLCInsureAccClassSchema,glFee,mPolno,FeeCode);//生成管理费轨迹			
        			tPerMMap.add(MMapTrace);
    	    	}
    	    	double tAccValue=0;
    			tPerMMap.add(updateAccInfo(mGGLCInsureAccSchema));
    			for(int i=1;i<=tLCInsureAccSet.size();i++){
    				mBalanceSeq = null; //由于月度不同，需要重新生成结算序号
            		LCInsureAccSchema glLCInsureAccSchema = tLCInsureAccSet.get(i);
    				LCInsureAccClassSet grLCInsureAccClassSet = getAccClassSetFromM(tLCInsureAccSet.get(i));
    				tAccValue = getAccBalaByAccClass(grLCInsureAccClassSet);
        			MMap MMapBalance1 = getBalance
        			(glLCInsureAccSchema,tLDPromiseRateSchema,tAccValue,"","0");
        			tPerMMap.add(MMapBalance1);
        			tPerMMap.add(updateAccInfo(glLCInsureAccSchema));
    			}
    		}else{//从个人账户收取
    			double accbala = getAccBalaByAccClass(mLCInsureAccClassSet);//一个人所有账户的总额
    			System.out.println("个人账户accbala"+accbala);
    			if(glFee>accbala){
    				System.out.println("处理被保人客户号码为"+mInsuredNo+"的数据时余额不足！");
            	}
    			for(int i=1;i<=tLCInsureAccSet.size();i++){
        			mBalanceSeq = null; //由于月度不同，需要重新生成结算序号
                	LCInsureAccSchema glLCInsureAccSchema = tLCInsureAccSet.get(i);
        			LCInsureAccClassSet tLCInsureAccClassSet = getAccClassSetFromM(tLCInsureAccSet.get(i));//从全局变量中获取当前账户的子账户
        			if(flag){
	        			double accbalacur = getAccBalaByAccClass(tLCInsureAccClassSet);//当前账户的余额
	        			System.out.println("当前第"+i+"账户的余额"+accbalacur);
	        			if(accbalacur>temp){
	        				double accTemp = temp;
	        				for(int j=1;j<=tLCInsureAccClassSet.size();j++){
	        					double accTemp1 = accTemp;//本次扣除费用
	                			accTemp = Arith.sub(tLCInsureAccClassSet.get(j).getLastAccBala(), accTemp);
	                			System.out.println("当前第"+i+"账户的剩余余额"+accTemp);
	                			if(accTemp>=0){
	                				MMap MMapTrace = getTrace(tLCInsureAccClassSet.get(j),accTemp1,"",FeeCode);//生成管理费轨迹
	                				tLCInsureAccClassSet.get(j).setInsuAccBala(accTemp);//本账户余额够，将剩余钱放入账户
	                				tPerMMap.add(MMapTrace);
	                				flag = false;
	                			}else{
	                				System.out.println("=========================== entry  ======================");
	                				MMap MMapTrace = getTrace(tLCInsureAccClassSet.get(j),tLCInsureAccClassSet.get(j).getLastAccBala(),"",FeeCode);//生成管理费轨迹
	                				tLCInsureAccClassSet.get(j).setInsuAccBala(0);//本账户不够 全部扣除
	                				tPerMMap.add(MMapTrace);
	                				accTemp = Arith.sub(accTemp,tLCInsureAccClassSet.get(j).getLastAccBala());
	                			}
	        				}
	        			}else{//当前账户余额不够扣除管理费
	            			for(int j=1;j<=tLCInsureAccClassSet.size();j++){
	            				MMap MMapTrace = getTrace(tLCInsureAccClassSet.get(j),tLCInsureAccClassSet.get(j).getLastAccBala(),"",FeeCode);//生成管理费轨迹
	            				temp = Arith.sub(temp, tLCInsureAccClassSet.get(j).getLastAccBala());
	            				tLCInsureAccClassSet.get(j).setInsuAccBala(0);
	            				tPerMMap.add(MMapTrace);
	                		}
	        			}
	        			double tAccValue = getAccBalaByAccClass(tLCInsureAccClassSet);
	        			MMap MMapBalance = getBalance(glLCInsureAccSchema,tLDPromiseRateSchema,tAccValue,"","0");
	        			tPerMMap.add(MMapBalance);
        			}else{
	        			double tAccValue = getAccBalaByAccClass(tLCInsureAccClassSet);
	        			MMap MMapBalance = getBalance(glLCInsureAccSchema,tLDPromiseRateSchema,tAccValue,"","0");
	        			tPerMMap.add(MMapBalance);
        			}
        			tPerMMap.add(updateAccInfo(glLCInsureAccSchema));
    			}
                    }
    		VData data = new VData();
            data.add(tPerMMap);
            PubSubmit tSubmit = new PubSubmit();
            if (!tSubmit.submitData(data, ""))
            {
                buildError("UliInsureAccSubmit", "数据提交失败");
                return false;
            }
            System.out.println("个人管理费处理完成，用时："+(System.currentTimeMillis() - t1));
            System.out.println("个人处理完成，用时："+(System.currentTimeMillis() - t0));
           return true;
      }
    
    

          public  MMap getResult(){
        	   
        	   return tPerMMap;
           }
    /**
     * calPolYearMonth
     */
    private boolean calPolYearMonth()
    {
        String sql = "select year(date('" + mStartDate +
                     "') - CValidate) + 1, "
                     + "   month(date('" + mStartDate + "') - CValidate) + 1 "
                     + "from LCPol "
                     + "where PolNo = '" + mLCInsureAccSchema.getPolNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        mPolYear = Integer.parseInt(tSSRS.GetText(1, 1));
        mPolMonth = Integer.parseInt(tSSRS.GetText(1, 2));

        return true;
    }
    private MMap dealOneAccClass(LCInsureAccClassSchema tLCInsureAccClassSchema,
                                 String cRateIntv, String cRatetype,String tRate )
    {
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("StartDate", mStartDate);
        tTransferData.setNameAndValue("EndDate", mEndDate);
        tTransferData.setNameAndValue("RiskCode",
                                      tLCInsureAccClassSchema.getRiskCode());
        tTransferData.setNameAndValue("RateIntv", cRateIntv); //利率间隔
        tTransferData.setNameAndValue("RateType", cRatetype); //利率类型
        tTransferData.setNameAndValue("PolYear", String.valueOf(mPolYear));
        tTransferData.setNameAndValue("PolMonth", String.valueOf(mPolMonth));
        
        tTransferData.setNameAndValue("Rate", tRate);//利率  根据cBalaType，利率对应结算利率和保证利率
        

        VData data = new VData();
        data.add(mGI);
        data.add(tLCInsureAccClassSchema);
        data.add(tTransferData);

        MMap tMMapAV = balanceOneAccRate(data);
        return tMMapAV;
    }
    /**
     * balanceOneAccRate
     * 对子账户进行一个月的收益结算
     * @param cInputDate VData
     * @return MMap
     */
    public MMap balanceOneAccRate(VData cInputDate)
    {
        TransferData tTransferData = (TransferData) cInputDate
                                     .getObjectByObjectName("TransferData", 0);
        mGI = (GlobalInput) cInputDate
              .getObjectByObjectName("GlobalInput", 0);
        LCInsureAccClassSchema tLCInsureAccClassSchema
                = (LCInsureAccClassSchema) cInputDate
                  .getObjectByObjectName("LCInsureAccClassSchema", 0);
        if (tTransferData == null || mGI == null
            || tLCInsureAccClassSchema == null)
        {
            mErrors.addOneError("传入的数据不完整");
            return null;
        }
        tTransferData.setNameAndValue("LCInsureAccClass",
                                      tLCInsureAccClassSchema);
        tTransferData.setNameAndValue("GI", mGI);

        //查询上一次结算信息
        LCInsureAccBalanceDB tLCInsureAccBalanceDB = new LCInsureAccBalanceDB();
        tLCInsureAccBalanceDB.setPolNo(tLCInsureAccClassSchema.getPolNo());
        tLCInsureAccBalanceDB.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
        tLCInsureAccBalanceDB.setDueBalaDate(tLCInsureAccClassSchema.
                                             getBalaDate());
        LCInsureAccBalanceSet tLCInsureAccBalanceSet
                = tLCInsureAccBalanceDB.query();
        if (tLCInsureAccBalanceSet.size() == 0)
        {
            mErrors.addOneError("没有查询到上一次结算信息");
            return null;
        }
        double tLastBala = tLCInsureAccBalanceSet.get(1).getInsuAccBalaAfter();
        System.out.println("上次结息后的金额为"+tLastBala);
        tTransferData.setNameAndValue("LastBala", "" + tLastBala);
        //计算账户收益
        AccountManage tAccountManage = new AccountManage();
        MMap tMMapLX = tAccountManage.getUliBalaAccInterest(tTransferData);
        if (tMMapLX == null)
        {
            mErrors.copyAllErrors(tAccountManage.mErrors);
            return null;
        }
        LCInsureAccClassSchema accClassDelt
                = (LCInsureAccClassSchema) tMMapLX
                  .getObjectByObjectName("LCInsureAccClassSchema", 0);
        accClassDelt.setInsuAccBala(PubFun.setPrecision(accClassDelt.
                getInsuAccBala(), "0.00"));
        LCInsureAccTraceSchema accTraceDelt
                = (LCInsureAccTraceSchema) tMMapLX
                  .getObjectByObjectName("LCInsureAccTraceSchema", 0);
        Reflections ref = new Reflections();
        LPInsureAccTraceSchema traceSchema = new LPInsureAccTraceSchema();
        ref.transFields(traceSchema, accTraceDelt);
        LPInsureAccClassSchema classSchema = new LPInsureAccClassSchema();
        ref.transFields(classSchema, accClassDelt);
        accTraceDelt.setOtherType("3"); //保全结算
        accTraceDelt.setOtherNo(getBalanceSequenceNo());
        accTraceDelt.setMoney(PubFun.setPrecision(accTraceDelt.getMoney(),
                                                  "0.00"));
        MMap tMMapAll = new MMap();
        tMMapAll.add(tMMapLX);

        return tMMapAll;
    }
    /**
     * 得到结算序号
     * @return String
     */
    public String getBalanceSequenceNo()
    {
        if (mBalanceSeq == null)
        {
            String[] dateArr = PubFun.getCurrentDate().split("-");
            String date = dateArr[0] + dateArr[1] + dateArr[2];
            mBalanceSeq = date
                          + PubFun1.CreateMaxNo("BalaSeq" + date, 10);
        }

        return mBalanceSeq;
    }
    private LCInsureAccClassSet getAccClass(LCInsureAccSchema
                                            tLCInsureAccSchema)
    {
        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
        tLCInsureAccClassDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
        LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
        if (tLCInsureAccClassDB.mErrors.needDealError())
        {
            CError.buildErr(this, "子帐户信息查询失败");
            return null;
        }
        if (tLCInsureAccClassSet == null || tLCInsureAccClassSet.size() < 1)
        {
            CError.buildErr(this, "没有子帐户信息");
            return null;
        }
        return tLCInsureAccClassSet;
    }
    /**
     * 错误构建方法
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "UliBQInsuAccBala";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
   
    /**
     * getInsuAccRateUnBalance
     * 查询未结算的保障利率
     * @param cLCInsureAccClassSchema LCInsureAccClassSchema，子账户
     * @return LMInsuAccRateSet，月收益率
     */
    private LDPromiseRateSet getLDPromiseRate(LCInsureAccSchema
            cLCInsureAccSchema,String prtNo)
    {
    	String subSQL = "";
    	if("370301".equals(cLCInsureAccSchema.getRiskCode())){
    		if(prtNo!=null && !"".equals(prtNo)){
    			subSQL = " and a.prtno = '"+prtNo+"'";
    		}
    	}else{
    		subSQL = " and a.prtno = '0000'";
    	}
        String sql = "SELECT * from ldpromiserate a where date('"+mEndDate+"')>=a.startdate "
        		+ " and date('"+mEndDate+"')<a.enddate "
        		+ " and exists (select riskcode from lmriskapp where risktype4='4'and Riskprop='G') "
        		+ " and riskcode='"+cLCInsureAccSchema.getRiskCode()+"'"	//modify by lzy 201506新增团险万能产品， 关联一下险种
        		+ subSQL
        		+ " order by startdate ";
        LDPromiseRateDB tLDPromiseRateDB = new LDPromiseRateDB();
        LDPromiseRateSet tLDPromiseRateSet = tLDPromiseRateDB.executeQuery(sql);
        if (tLDPromiseRateSet.mErrors.needDealError())
        {
            System.out.println(tLDPromiseRateDB.mErrors.getErrContent());
            mErrors.addOneError("查询符合该账户的保证利率发生异常");
            return null;
        }
        if (tLDPromiseRateSet.size() == 0)
        {
            mErrors.addOneError("没有查询到符合该账户的保证利率，不能进行结算");
            return null;
        }

        return tLDPromiseRateSet;
    }
    
    
  

    /**
     * getLCInsureAccBalance
     * 生成结算轨迹
     * @param cAccValue double
     * @param cAccValueGurat double
     * @return Object
     */
    private LPInsureAccBalanceSchema getLCInsureAccBalance1(LCInsureAccSchema glLCInsureAccSchema,LDPromiseRateSchema
            cInterest000001Schema,
            double cAccValue,String flag,String isgg)
    {
        LPInsureAccBalanceSchema schema = new LPInsureAccBalanceSchema();
        ref.transFields(schema, glLCInsureAccSchema);

//        schema.setSequenceNo(getBalanceSequenceNo());
        if("1".equals(isgg) ){//isgg 1 为公共账户
        	if(!"".equals(GGSequenceno)){
        		schema.setSequenceNo(GGSequenceno);
        	}else{
        		schema.setSequenceNo(getBalanceSequenceNo());
        		GGSequenceno = schema.getSequenceNo();
        	}
        }else{
        	schema.setSequenceNo(getBalanceSequenceNo());
        }
        schema.setEdorNo(mEdorNo);
        schema.setEdorType(mEdorType);
        schema.setInsuAccBalaBefore(glLCInsureAccSchema.getInsuAccBala());
        schema.setInsuAccBalaAfter(cAccValue);
        //这里由于不再计算保证帐户价值，所以直接置InsuAccBalaGurat ＝ InsuAccBalaAfter,
        if("370101".equals(glLCInsureAccSchema.getRiskCode())){
        	schema.setInsuAccBalaGurat("");
        }else{
        	schema.setInsuAccBalaGurat(cAccValue);
        }
        schema.setDueBalaDate(cInterest000001Schema.getEndDate());
        schema.setDueBalaTime("00:00:00");
        schema.setRunDate(PubFun.getCurrentDate());
        schema.setRunTime(PubFun.getCurrentTime());
        schema.setPolYear(mPolYear);
        schema.setPolMonth(mPolMonth);
        schema.setPrintCount(0);
        schema.setOperator(mGI.Operator);
        PubFun.fillDefaultField(schema);
        if("".equals(flag)){
        	String sql = "select max(BalaCount + 1) from LCInsureAccBalance "
                + "where PolNo = '"
                + glLCInsureAccSchema.getPolNo() + "' "
                + "   and InsuAccNo = '"
                + glLCInsureAccSchema.getInsuAccNo() + "' ";
        	String maxBalaCount = new ExeSQL().getOneValue(sql);
        	if (maxBalaCount == null || maxBalaCount.equals("") ||
        			maxBalaCount.equals("null"))
        	{
        		maxBalaCount = "1";
        	}
        	schema.setBalaCount(maxBalaCount);
        	if("1".equals(isgg)){
        		GGBalaCoutn = maxBalaCount;
            }
        }else{
        	schema.setBalaCount(GGBalaCoutn);
        }
        return schema;
    }
    public boolean setLPInsureAcc(){
        
		return true;
    }
    //管理费收取轨迹
    public MMap getTrace(LCInsureAccClassSchema tLCInsureAccClassSchema,double fee,String aAIPolNo,String FeeCode){
    	MMap tMMap = new MMap();
//		管理费轨迹
//		String RiskFeeSql = "select feecode,feevalue from LMRiskFee where insuaccno = '"+tLCInsureAccClassSchema.getInsuAccNo()+"' and payplancode = '"+tLCInsureAccClassSchema.getPayPlanCode()+"'";
//		SSRS RiskFeeSSRS = new ExeSQL().execSQL(RiskFeeSql);
		LPInsureAccFeeTraceSchema schema = new LPInsureAccFeeTraceSchema();
		ref.transFields(schema, tLCInsureAccClassSchema);
        String tLimit = PubFun.getNoLimit(schema.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        schema.setEdorNo(mEdorNo);
        schema.setEdorType(mEdorType);
        schema.setPayPlanCode(tLCInsureAccClassSchema.getPayPlanCode());
        schema.setSerialNo(serNo);
        schema.setOtherNo(mEdorNo);
        schema.setFeeCode(FeeCode);
        schema.setFee(fee);
        schema.setOtherNo(mEdorNo);
        schema.setOtherType("14"); //保全结算 
        schema.setFeeRate("0");
        schema.setPayDate(mEndDate);
        schema.setState("0");
        PubFun.fillDefaultField(schema);
        String moneyType = "MF";
        schema.setMoneyType(moneyType);
        tMMap.put(schema, SysConst.INSERT);
//      管理费的账户轨迹
        LPInsureAccTraceSchema traceSchema = new LPInsureAccTraceSchema();
        ref.transFields(traceSchema, schema);
        traceSchema.setEdorNo(mEdorNo);
        traceSchema.setEdorType(mEdorType);
        traceSchema.setSerialNo(serNo);
        traceSchema.setAIPolNo(aAIPolNo);
        traceSchema.setOtherNo(mEdorNo);
        traceSchema.setOtherType("14");
        traceSchema.setMoney( -Math.abs(schema.getFee()));
        tMMap.put(traceSchema, SysConst.INSERT);
        return tMMap;
    }
    //生成结算信息
    public MMap getBalance(LCInsureAccSchema glLCInsureAccSchema,LDPromiseRateSchema tLDPromiseRateSchema,double tAccValue,String flag,String isGG){
    	MMap BaMMap = new MMap();
    	LPInsureAccBalanceSchema tLPInsureAccBalanceSchema
    	  = getLCInsureAccBalance1(glLCInsureAccSchema,tLDPromiseRateSchema,
    	                          tAccValue,flag,isGG);
    	    if (tLPInsureAccBalanceSchema == null)
    	    {
    	        return null;
    	    }
    	    if("".equals(flag)){// 公共账户未存库及所有个人账户处理
    	    	BaMMap.put(tLPInsureAccBalanceSchema, SysConst.INSERT);
    	    }else{
    	    	tLPInsureAccBalanceSchema.setSequenceNo(flag);
    	    	BaMMap.put(tLPInsureAccBalanceSchema, SysConst.UPDATE);
    	    }
    	    return BaMMap;
    }
    //根据子账户获取账户总金额
    public double getAccBalaByAccClass(LCInsureAccClassSet tLCInsureAccClassSet){
    	double accBala = 0;
    	for(int i=1;i<=tLCInsureAccClassSet.size();i++){
    		accBala = Arith.add(accBala, tLCInsureAccClassSet.get(i).getInsuAccBala());
    	}
    	return accBala;
    }
    
    //根据账户在全局变量中获取子账户
    public LCInsureAccClassSet getAccClassSetFromM(LCInsureAccSchema tLCInsureAccSchema){
    	LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
    		for(int i=1;i<=mLCInsureAccClassSet.size();i++){
        		if(mLCInsureAccClassSet.get(i).getPolNo().equals(tLCInsureAccSchema.getPolNo()) 
        				&& mLCInsureAccClassSet.get(i).getInsuAccNo().equals(tLCInsureAccSchema.getInsuAccNo())){
        			tLCInsureAccClassSet.add(mLCInsureAccClassSet.get(i));
        		}
        	}
    	return tLCInsureAccClassSet;
    }
    /**
     * updateAccInfo
     * 更新总账户信息
     * @return MMap
     */
    private MMap updateAccInfo(LCInsureAccSchema aLCInsureAccSchema)
    {
        MMap tMMap = new MMap();
        String sql = "update LPInsureAccClass a "
            + "set (InsuAccBala, LastAccBala) "
            + "   =((select nvl(sum(Money),0)from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo)+(select nvl(sum(Money),0)from LpInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and edorno=a.edorno and edortype=a.edortype),(select nvl(sum(Money),0)from LcInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo)+(select nvl(sum(Money),0)from LpInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and edorno=a.edorno and edortype=a.edortype)) "  
                     + "where PolNo = '"
                     + aLCInsureAccSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + aLCInsureAccSchema.getInsuAccNo() + "' "
                     +"   and edorno = '"
                     + mEdorNo + "' "
                     +"   and edortype = '"
                     + mEdorType + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        //更新账户信息
        sql = "update LpInsureAcc a "
              + "set (InsuAccBala, LastAccBala, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(InsuAccBala), sum(LastAccBala), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LpInsureAccClass where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and edorno=a.edorno and edortype=a.edortype "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + aLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + aLCInsureAccSchema.getInsuAccNo() + "' "
              +"   and edorno = '"
              + mEdorNo + "' "
              +"   and edortype = '"
              + mEdorType + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        //按轨迹更新账户管理费信息
        sql = "update LpInsureAccClassFee a " +
        "set Fee =((select nvl(sum(Fee),0) from LCInsureAccFeeTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo)+(select nvl(sum(Fee),0) from LpInsureAccFeeTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and edorno=a.edorno and edortype=a.edortype )) "
              + "where PolNo = '"
              + aLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + aLCInsureAccSchema.getInsuAccNo() + "' "
              +"   and edorno = '"
              + mEdorNo + "' "
              +"   and edortype = '"
              + mEdorType + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        //更新账户管理费信息
        sql = "update LpInsureAccFee a " +
              "set (Fee, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(fee), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LpInsureAccClassFee where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and edorno=a.edorno and edortype=a.edortype"
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + aLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + aLCInsureAccSchema.getInsuAccNo() + "' "
              +"   and edorno = '"
              + mEdorNo + "' "
              +"   and edortype = '"
              + mEdorType + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        return tMMap;
    }
    public boolean deletePtable(){
    	System.out.println("开始执行删除P表");
        String AccClasssql = "Delete from LPInsureAccClass a where a.grpcontno='"+mGrpContNo+"' and a.polno='"
            + mPolno+ "' and a.insuredno='"+mInsuredNo+"'"
            + "   and a.EdorNo = '"
            + mLPEdorItemSchema.getEdorNo()+ "' and a.edortype='"+mLPEdorItemSchema.getEdorType()+"'";
        tPerMMap.put(AccClasssql, SysConst.DELETE);
        
        String Accsql = "Delete from LPInsureAcc a where a.contno='"
        	  + mLPEdorItemSchema.getContNo() + "' and a.insuredno='"+mInsuredNo+"'"
            + "   and a.EdorNo = '"
            + mLPEdorItemSchema.getEdorNo()+ "' and a.edortype='"+mLPEdorItemSchema.getEdorType()+"'";
        tPerMMap.put(Accsql, SysConst.DELETE);
        
        String AccTracesql = "Delete from LPInsureAccTrace a where a.polno='"
        	+ mPolno + "'"
            + "   and a.EdorNo = '"
            + mLPEdorItemSchema.getEdorNo()+ "' and a.edortype='"+mLPEdorItemSchema.getEdorType()+"'";
        tPerMMap.put(AccTracesql, SysConst.DELETE);
        
        String AccBalancesql = "Delete from LPInsureAccBalance a where a.polno='"
        	+ mPolno + "'"
            + "   and a.EdorNo = '"
            + mLPEdorItemSchema.getEdorNo()+ "' and a.edortype='"+mLPEdorItemSchema.getEdorType()+"'";
        tPerMMap.put(AccBalancesql, SysConst.DELETE);
        String InsureAccClassFeesql = "Delete from LPInsureAccClassFee a where a.contno='"
        	+ mLPEdorItemSchema.getContNo() + "' and a.insuredno='"+mInsuredNo+"'"
            + "   and a.EdorNo = '"
            + mLPEdorItemSchema.getEdorNo()+ "' and a.edortype='"+mLPEdorItemSchema.getEdorType()+"'";
        tPerMMap.put(InsureAccClassFeesql, SysConst.DELETE);
        
        String InsureAccFeesql = "Delete from LPInsureAccFee a where a.contno='"
        	+ mLPEdorItemSchema.getContNo() + "' and a.insuredno='"+mInsuredNo+"'"
            + "   and a.EdorNo = '"
            + mLPEdorItemSchema.getEdorNo()+ "' and a.edortype='"+mLPEdorItemSchema.getEdorType()+"'";
        tPerMMap.put(InsureAccFeesql, SysConst.DELETE);
    	return true;
    }
    //设置个人账户ACC除外的相关的表信息
   private boolean setPerACC(LCInsureAccClassSchema schemaAV,LCInsureAccTraceSchema traceschemaAV,MMap tMMap1){
	   Reflections ref = new Reflections();
       //增加LPmLPInsureAccClassSet和LPInsureAcctraceSet
       LPInsureAccClassSchema classSchema = new LPInsureAccClassSchema();
       ref.transFields(classSchema, schemaAV);
       classSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
       classSchema.setEdorType(mLPEdorItemSchema.getEdorType());
       mLPInsureAccClassSet.add(classSchema);
       tMMap1.put(classSchema, SysConst.INSERT);
       
       LPInsureAccTraceSchema traceSchema = new LPInsureAccTraceSchema();
       ref.transFields(traceSchema, traceschemaAV);
       traceSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
       traceSchema.setEdorType(mLPEdorItemSchema.getEdorType());
       traceSchema.setOtherNo(mLPEdorItemSchema.getEdorNo());//这边保存otherno 主要为了区别开市保全结息轨迹还是月结轨迹
       mLPInsureAccTraceSet.add(traceSchema);
       tMMap1.put(traceSchema, SysConst.INSERT);

       //对每个人生成LCInsureAccClassFee表记录
       LCInsureAccClassFeeDB mLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
       mLCInsureAccClassFeeDB.setGrpContNo(mGrpContNo);
       mLCInsureAccClassFeeDB.setContNo(mLPEdorItemSchema.getContNo());
       mLCInsureAccClassFeeDB.setPolNo(mPolno);                    
       mLCInsureAccClassFeeDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
       mLCInsureAccClassFeeDB.setInsuAccNo(schemaAV.getInsuAccNo());
       LCInsureAccClassFeeSet mLCInsureAccClassFeeSet = mLCInsureAccClassFeeDB.query();
		if(mLCInsureAccClassFeeSet.size()==0)
       {
		   mErrors.addOneError("没有查询到被保人"+mInsuredNo+"的LCInsureAccClassFee记录");
           return false;
       }
		LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = mLCInsureAccClassFeeSet.get(1).getSchema();
		LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
		ref.transFields(tLPInsureAccClassFeeSchema, tLCInsureAccClassFeeSchema);
		tLPInsureAccClassFeeSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPInsureAccClassFeeSchema.setEdorType(mLPEdorItemSchema.getEdorType()); 
		tMMap1.put(tLPInsureAccClassFeeSchema, SysConst.INSERT);
 		  return true;
    }
   //设置个人账户ACC的表信息
    private boolean setACC(LCInsureAccSchema mLCInsureAccSchema,MMap tMMap1){
    	
    	 //对每个人生成LCInsureAccFee表记录
        LCInsureAccFeeDB mLCInsureAccFeeDB = new LCInsureAccFeeDB();
        mLCInsureAccFeeDB.setGrpContNo(mGrpContNo);
        mLCInsureAccFeeDB.setContNo(mLPEdorItemSchema.getContNo());
        mLCInsureAccFeeDB.setPolNo(mPolno);                    
        mLCInsureAccFeeDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        mLCInsureAccFeeDB.setInsuAccNo(mLCInsureAccSchema.getInsuAccNo());
        LCInsureAccFeeSet mLCInsureAccFeeSet = mLCInsureAccFeeDB.query();
		if(mLCInsureAccFeeSet == null || mLCInsureAccFeeSet.size()==0)
        {
			 mErrors.addOneError("没有查询到被保人"+mInsuredNo+"的LCInsureAccFee记录");
	         return false;
        }
		LCInsureAccFeeSchema tLCInsureAccFeeSchema = mLCInsureAccFeeSet.get(1).getSchema();
		LPInsureAccFeeSchema LPInsureAccFeeSchema = new LPInsureAccFeeSchema();
		ref.transFields(LPInsureAccFeeSchema, tLCInsureAccFeeSchema);
		LPInsureAccFeeSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		LPInsureAccFeeSchema.setEdorType(mLPEdorItemSchema.getEdorType()); 
		tMMap1.put(LPInsureAccFeeSchema, SysConst.INSERT);
		
    	LCInsureAccDB mLCInsureAccDB = new LCInsureAccDB();
        mLCInsureAccDB.setGrpContNo(mGrpContNo);
        mLCInsureAccDB.setContNo(mLPEdorItemSchema.getContNo());
        mLCInsureAccDB.setPolNo(mPolno);
        mLCInsureAccDB.setInsuAccNo(mLCInsureAccSchema.getInsuAccNo());
        mLCInsureAccDB.setInsuredNo(mLCInsureAccSchema.getInsuredNo());
		LCInsureAccSet mLCInsureAccSet = mLCInsureAccDB.query();
		if(mLCInsureAccSet == null || mLCInsureAccSet.size()==0)
        {
			mErrors.addOneError("查询账户信息失败Acc!");
            return false;
        }
		LCInsureAccSchema tLCInsureAccSchema = mLCInsureAccSet.get(1).getSchema();
		LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
		ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
		tLPInsureAccSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPInsureAccSchema.setEdorType(mLPEdorItemSchema.getEdorType()); 
		 tMMap1.put(tLPInsureAccSchema, SysConst.INSERT);
       return true;
   }
    
    private boolean setPubACC(){
    	String GGAccClassSql = "select * from lcinsureaccclass where grpcontno = '"+mGrpContNo+"' and acctype = '001'";
		LCInsureAccClassDB tGGLCInsureAccClassDB = new LCInsureAccClassDB();
		mGGLCInsureAccClassSchema = tGGLCInsureAccClassDB.executeQuery(GGAccClassSql).get(1);
		if(mGGLCInsureAccClassSchema==null){
			mErrors.addOneError("没有获取到公共账户信息！");
            return false;
		}
		//取出是否已存在P表中，如存在，则取出P表的数据来.对其进行更新.
		String BQGGAccClassSql = "select * from lpinsureaccclass where grpcontno = '"+mGrpContNo+"' and edorno ='"+mEdorNo+"' and edortype='"+mEdorType+"'and acctype = '001'";
		LPInsureAccClassDB tGGLPInsureAccClassDB = new LPInsureAccClassDB();
		// = tGGLPInsureAccClassDB.executeQuery(BQGGAccClassSql).get(1);
		if(tGGLPInsureAccClassDB.executeQuery(BQGGAccClassSql).get(1)==null){
      		ref.transFields(mGGLPInsureAccClassSchema, mGGLCInsureAccClassSchema);
      		mGGLPInsureAccClassSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
      		mGGLPInsureAccClassSchema.setEdorType(mLPEdorItemSchema.getEdorType()); 
			tPerMMap.put(mGGLPInsureAccClassSchema, SysConst.INSERT);
		}else{
			tPerMMap.put(tGGLPInsureAccClassDB.executeQuery(BQGGAccClassSql).get(1), SysConst.UPDATE);
		}
  		//对每个人生成LCInsureAccClassFee表记录
        LCInsureAccClassFeeDB mLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
        mLCInsureAccClassFeeDB.setGrpContNo(mGrpContNo);
        mLCInsureAccClassFeeDB.setContNo(mGGLCInsureAccClassSchema.getContNo());
        mLCInsureAccClassFeeDB.setPolNo(mGGLCInsureAccClassSchema.getPolNo());                    
        mLCInsureAccClassFeeDB.setInsuredNo(mGGLCInsureAccClassSchema.getInsuredNo());
        mLCInsureAccClassFeeDB.setInsuAccNo(mGGLCInsureAccClassSchema.getInsuAccNo());
        LCInsureAccClassFeeSet mLCInsureAccClassFeeSet = mLCInsureAccClassFeeDB.query();
		if(mLCInsureAccClassFeeSet == null || mLCInsureAccClassFeeSet.size()==0)
        {
			mErrors.addOneError("查询LCInsureAccClassFee失败");
            return false;
        }
		String BQGGAccClassFeeSql = "select * from lpinsureaccclassfee where grpcontno = '"+mGrpContNo+"' and edorno ='"+mEdorNo+"' and edortype='"+mEdorType+"' and acctype = '001'";
		LPInsureAccClassFeeDB tLPInsureAccClassFeeDB = new LPInsureAccClassFeeDB();
		//mLPInsureAccClassFeeSchema = tLPInsureAccClassFeeDB.executeQuery(BQGGAccClassFeeSql).get(1);
		LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = mLCInsureAccClassFeeSet.get(1).getSchema();
		if(tLPInsureAccClassFeeDB.executeQuery(BQGGAccClassFeeSql).get(1)==null){
	        		ref.transFields(mLPInsureAccClassFeeSchema, tLCInsureAccClassFeeSchema);
	        	System.out.println("mEdorNo"+mEdorNo);
      		mLPInsureAccClassFeeSchema.setEdorNo(mEdorNo);
      		mLPInsureAccClassFeeSchema.setEdorType(mEdorType); 
			tPerMMap.put(mLPInsureAccClassFeeSchema, SysConst.INSERT);
		}else{
			tPerMMap.put(tLPInsureAccClassFeeDB.executeQuery(BQGGAccClassFeeSql).get(1), SysConst.UPDATE);
		}
//		对每个人生成LCInsureAccFee表记录
          LCInsureAccFeeDB mLCInsureAccFeeDB = new LCInsureAccFeeDB();
          mLCInsureAccFeeDB.setGrpContNo(mGrpContNo);
          mLCInsureAccFeeDB.setContNo(mGGLCInsureAccClassSchema.getContNo());
          mLCInsureAccFeeDB.setPolNo(mGGLCInsureAccClassSchema.getPolNo());                    
          mLCInsureAccFeeDB.setInsuredNo(mGGLCInsureAccClassSchema.getInsuredNo());
          mLCInsureAccFeeDB.setInsuAccNo(mGGLCInsureAccClassSchema.getInsuAccNo());
          LCInsureAccFeeSet mLCInsureAccFeeSet = mLCInsureAccFeeDB.query();
  		if(mLCInsureAccFeeSet == null || mLCInsureAccFeeSet.size()==0)
          {
  			mErrors.addOneError("查询LCInsureAccFee失败!");
            return false;
          }
  		LCInsureAccFeeSchema tLCInsureAccFeeSchema = mLCInsureAccFeeSet.get(1).getSchema();
  		
  		String BQGGAccFeeql = "select * from lpinsureaccfee where grpcontno = '"+mGrpContNo+"'and edorno ='"+mEdorNo+"' and edortype='"+mEdorType+"' and acctype = '001'";
		LPInsureAccFeeDB tLPInsureAccFeeDB = new LPInsureAccFeeDB();
		//mLPInsureAccFeeSchema = tLPInsureAccFeeDB.executeQuery(BQGGAccFeeql).get(1);
		
		if(tLPInsureAccFeeDB.executeQuery(BQGGAccFeeql).get(1)==null){
			ref.transFields(mLPInsureAccFeeSchema, tLCInsureAccFeeSchema);
			mLPInsureAccFeeSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
			mLPInsureAccFeeSchema.setEdorType(mLPEdorItemSchema.getEdorType()); 
      		tPerMMap.put(mLPInsureAccFeeSchema, SysConst.INSERT);
		}else{
			tPerMMap.put(tLPInsureAccFeeDB.executeQuery(BQGGAccFeeql).get(1), SysConst.UPDATE);
		}
  		 LCInsureAccDB mLCInsureAccDB = new LCInsureAccDB();
         mLCInsureAccDB.setGrpContNo(mGrpContNo);
         mLCInsureAccDB.setContNo(mGGLCInsureAccClassSchema.getContNo());
         mLCInsureAccDB.setPolNo(mGGLCInsureAccClassSchema.getPolNo());
         mLCInsureAccDB.setInsuAccNo(mGGLCInsureAccClassSchema.getInsuAccNo());
         mLCInsureAccDB.setInsuredNo(mGGLCInsureAccClassSchema.getInsuredNo());
 		LCInsureAccSet mLCInsureAccSet = mLCInsureAccDB.query();
 		if(mLCInsureAccSet == null || mLCInsureAccSet.size()==0)
         {
 			mErrors.addOneError("查询账户信息失败Acc!");
	         return false;
         }
 		LCInsureAccSchema tLCInsureAccSchema = mLCInsureAccSet.get(1).getSchema();
 		
 		String BQGGAccql = "select * from lpinsureacc where grpcontno = '"+mGrpContNo+"'and edorno ='"+mEdorNo+"' and edortype='"+mEdorType+"' and acctype = '001'";
		LPInsureAccDB tLPInsureAccDB = new LPInsureAccDB();
		//mLPInsureAccSchema = tLPInsureAccDB.executeQuery(BQGGAccql).get(1);		
		if(tLPInsureAccDB.executeQuery(BQGGAccql).get(1)==null){
			ref.transFields(mLPInsureAccSchema, tLCInsureAccSchema);
			mLPInsureAccSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
			mLPInsureAccSchema.setEdorType(mLPEdorItemSchema.getEdorType()); 
      		tPerMMap.put(mLPInsureAccSchema, SysConst.INSERT);
		}else{
			tPerMMap.put(tLPInsureAccDB.executeQuery(BQGGAccql).get(1), SysConst.UPDATE);
		}        
		return true;
    }
    
    public String getGlFeeMode() {
		return glFeeMode;
	}

	public void setGlFeeMode(String glFeeMode) {
		this.glFeeMode = glFeeMode;
	}

	public static void main(String arg[])
    {
		String sql = "delete from lpedoritem where edorno='11111'";
		MMap tMMap = new MMap();
		tMMap.put(sql, "DELETE");
		VData data = new VData();
        data.add(tMMap);
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(data, ""))
        {
//            buildError("UliInsureAccSubmit", "数据提交失败");
        	System.out.println("1111");
//            return false;
        }
    }
}
