package com.sinosoft.lis.pubfun;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 扫描件处理类</p>
 * <p>Description: UI功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 * @date 2002-11-06
 */

public class EasyScanQueryMultilpUI
{
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public EasyScanQueryMultilpUI()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"QUERY||MAIN"和"QUERY||DETAIL"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        EasyScanQueryMultilpBL EasyScanQueryMultilpBL = new EasyScanQueryMultilpBL();

        if (EasyScanQueryMultilpBL.submitData(cInputData, cOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(EasyScanQueryMultilpBL.mErrors);
            mResult.clear();
            return false;
        }
        else
        {
            mResult = EasyScanQueryMultilpBL.getResult();
        }
        System.out.println("---EasyScanQueryMultilpBL END---");

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args)
    {
//        EasyScanQueryMultilpUI EasyScanQueryMultilpUI = new EasyScanQueryMultilpUI();
//        EasyScanQueryBL easyScanQueryBL1 = new EasyScanQueryBL();
//        VData tVData = new VData();
//        tVData.add("5");
//        EasyScanQueryMultilpUI.submitData(tVData, "QUERY||MAIN");
//        easyScanQueryBL1.submitData(tVData, "QUERY||MAIN");
//        VData tVDataResult = new VData();
//        tVDataResult = easyScanQueryBL1.getResult();
//        for (int i = 0; i < tVDataResult.size(); i++)
//        {
//            System.out.println(tVDataResult.get(i));
//        }
    }
}
