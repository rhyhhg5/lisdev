/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;

import com.sinosoft.lis.db.LDSpotTrackDB;
import com.sinosoft.lis.vschema.LDSpotTrackSet;

/**
 * <p>
 * Title:
 * </p>
 * 【理赔#3279】 抽取准备类
 * <p>
 * Description:
 * </p>
 * 准备要抽取的数据记录和抽取方式
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: SINOSOFT
 * </p>
 * 
 * @author GAOBO
 * @version 1.0
 */
public class SpotPrepareClaim {
	public SpotPrepareClaim() {
	}

	// 返回数据集
	public LDSpotTrackSet mLDSpotTrackSet = new LDSpotTrackSet();

	/**
	 * 随机抽取比例的数据准备
	 * 
	 * @param cOtherNo
	 *            其他类型No
	 * @param cOtherType
	 *            类型，"CaseNo"，用于抽检轨迹表LDSpotTrack
	 * @param cFlag
	 *            判断是否重复抽检，false为不重复抽检
	 * @param cSign
	 *            抽检比例标志，1为10%，2为20%
	 */
	public boolean PrepareData(String cOtherNo, String cOtherType, boolean cFlag, String cSign) {
		// 判断是否已经抽检，如果轨迹表中存在，证明已经抽检过
		if (!cFlag) {
			LDSpotTrackDB tLDSpotTrackDB = new LDSpotTrackDB();
			tLDSpotTrackDB.setOtherNo(cOtherNo);
			tLDSpotTrackDB.setOtherType(cOtherType);
			if (tLDSpotTrackDB.query().size() > 0) {
				return false;
			}
		}
		// 抽取比例
		int tPercent;
		// 抽取状态，表示是否被选中
		boolean tFlag;
		// 抽取计算类
		SpotCheck tspotcheck = new SpotCheck();
		// 判断抽取比例
		if ("1".equals(cSign)) {
			tPercent = 10;
		} else {
			tPercent = 20;
		}
		// 由于改用返回数据集的方式，因此在抽取的时候不会出现错误
		tFlag = tspotcheck.RandomRate(cOtherNo, cOtherType, tPercent);
		// 无论抽中与否，都需要取得返回的数据集
		mLDSpotTrackSet.set(tspotcheck.getLDSpotTrackSet());
		return tFlag;
	}

	/**
	 * 返回数据集
	 * 
	 * @return LDSpotTrackDBSet
	 */
	public LDSpotTrackSet getLDSpotTrackSet() {
		return mLDSpotTrackSet;
	}

}
