/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 全局变量区</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author YT
 * @version 1.0
 */

public class GlobalInput {
    /** 当前操作员 */
    public String Operator;
    /** 当前管理机构 */
    public String ManageCom;
    /** 当前登陆机构 */
    public String ComCode;
//  /** 当前险种 */
//  public String RiskCode;
//  /** 当前险种版本 */
//  public String RiskVersion;
    /*登陆客户端IP地址*/
    public String ClientIP;
    /*服务器IP地址*/
    public String ServerIP;
    /** 新增代理机构代码 */
    public String AgentCom;

    public GlobalInput() {
    }

    /**
     * 调试函数
     * @param args String[]
     */
    public static void main(String[] args) {
//        GlobalInput globalInput1 = new GlobalInput();
        GlobalInput tG = new GlobalInput();
        System.out.println("IP : " + tG.GetServerIP());
    }

    /**
     * 两个GlobalInput对象之间的直接复制
     * @param cGlobalInput 包含有具体值的GlobalInput对象
     */
    public void setSchema(GlobalInput cGlobalInput) {
        //获取登陆用户基础信息：用户编码、管理机构等
        this.Operator = cGlobalInput.Operator;
        this.ComCode = cGlobalInput.ComCode;
        this.ManageCom = cGlobalInput.ManageCom;
        this.ClientIP = cGlobalInput.ClientIP;
        this.ServerIP = cGlobalInput.ServerIP;
        this.AgentCom = cGlobalInput.AgentCom;
    }

    public String GetServerIP() {
        //获取服务器IP地址
        InetAddress address = null;
        try {
            address = InetAddress.getLocalHost(); //getByName("localhost");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        String addStr = address.getHostAddress();
        return addStr;
    }
}
