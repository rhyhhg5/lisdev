package com.sinosoft.lis.pubfun;

import java.io.*;
import com.sinosoft.utility.*;
/**
 * <p>Title: 通用excel清单下载</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author qulqss
 * @version 1.0
 */
public class CreateExcelList extends WriteToExcel {
    private int mRowOffset = 0;
    private int mColOffset = 0;
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();
    public CreateExcelList() {
        super();
    }

    public CreateExcelList(String fileName) {
        super(fileName);
    }

    public static void main(String[] args) {
        CreateExcelList createexcellist = new CreateExcelList("");
        String sql = "select usercode,username,comcode,password,userstate from lduser "
                     + "where usercode ='fuxin' "
                     ;
        int t[] = {1,2,3,0,4};
        createexcellist.createExcelFile();
        String[] sheetName ={PubFun.getCurrentDate()};
        createexcellist.addSheet(sheetName);
        createexcellist.setRowColOffset(0,2);
        String [][]tt ={{"用户名","名字","机构","","状态"}};
        createexcellist.setData(tt,t);
        createexcellist.setRowColOffset(createexcellist.setData(tt,t)+2,2);
        createexcellist.setData(sql,t);
        try{
            createexcellist.write("d:\\ffff..xls");
        }catch(Exception e)
        {
        }

    }
    /**
     *
     * @param aQuerySql String 指定查询SQL
     * @param aColAttribute int[] 指定对应列是否显示在清单中，0不显示,其它值为显示列。第一列为1
     * @return int 成功返回当前excel行数，否则为-1
     */
    public int setData(String aQuerySql,int []aColAttribute)
    {
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(aQuerySql);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "CreateExcelList";
            tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            return -1;
        }
        String [][]tData = tSSRS.getAllData();
        return setData(tData,aColAttribute);
    }
    /**
     *
     * @param RowColData String[][] 指定数据
     * @param aColAttribute int[] 指定对应列是否显示在清单中，0不显示,其它值为显示列。第一列为1
     * @return int 成功返回当前excel行数，否则为-1
     */
    public int setData(String [][]aRowColData,int []aColAttribute)
    {
        String [][]tData = dataFetch(aRowColData,aColAttribute);
        for(int i=0;i<tData.length;i++)
        {
            for (int j = 0; j < tData[i].length; j++) {
                setData(0, mRowOffset+i, mColOffset+j, tData[i][j]);
            }
        }
        mRowOffset+=tData.length;
        return mRowOffset;
    }

    public void setRowColOffset(int aRow,int aCol)
    {
        mRowOffset = aRow;
        mColOffset = aCol;
    }

    private String[][] dataFetch(String [][]aOriginalData,int []aColAttribute)
    {
        int length = aColAttribute.length;
        for(int i=0;i<aColAttribute.length;i++)
        {
            if(aColAttribute[i]==0)
            {
                length--;
            }
        }
        String [][]t=new String[aOriginalData.length][length];
        for(int i=0;i<aOriginalData.length;i++)
        {
            for(int j=0;j<aColAttribute.length;j++)
            {
               if(aColAttribute[j]==0)
                    continue;
                else
                    t[i][aColAttribute[j]-1]=StrTool.cTrim(aOriginalData[i][j]);
            }
        }
        return t;
    }
}
