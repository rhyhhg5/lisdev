/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.util.Vector;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LMSendMSGDB;
import com.sinosoft.lis.message.Response;
import com.sinosoft.lis.message.SMSClient;
import com.sinosoft.lis.message.SmsMessage;
import com.sinosoft.lis.message.SmsMessageNew;
import com.sinosoft.lis.message.SmsMessages;
import com.sinosoft.lis.message.SmsMessagesNew;
import com.sinosoft.lis.message.SmsServiceServiceLocator;
import com.sinosoft.lis.message.SmsServiceSoapBindingStub;
import com.sinosoft.lis.schema.LCCSpecSchema;
import com.sinosoft.lis.schema.LMCalFactorSchema;
import com.sinosoft.lis.schema.LMSendMSGSchema;
import com.sinosoft.lis.vschema.LMCalFactorSet;
import com.sinosoft.lis.vschema.LMSendMSGSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;

/*
 * <p>Title: 短信发送公共类 </p>
 * <p>Description: 通过传入的保单信息和责任信息构建出保费信息和领取信息 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author HST
 * @version 1.0
 * @date 2002-07-01
 */
public class AnalyzeMSG
{
    public static void main(String[] args)
    {
    	String aTaskCode="LP001";
    	MsgSendBase  mMsgSendBase =new MsgSendBase();
    	mMsgSendBase.setPrem("1000");
    	mMsgSendBase.setAppntName("二胡");
    	mMsgSendBase.setAppntSex("男");
    	mMsgSendBase.setManageCom("86110000");
    	mMsgSendBase.setMobilPhone("18510745015");
    	AnalyzeMSG  tAnalyzeMSG= new AnalyzeMSG();
    	tAnalyzeMSG.setmMsgSendBase(mMsgSendBase);
    	tAnalyzeMSG.applySend(aTaskCode);
    }

    private boolean mFlag = false;

    private Connection conn = null;

    public AnalyzeMSG() {
    }

    public AnalyzeMSG(Connection conn) {
        mFlag = true;
        this.conn = conn;
    }


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 计算需要用到的保单号码 */
    public String PolNo;

    // @Field
    //计算编码
    private String mTaskCode = "";
    //算法对应SQL语句所在表结构
    private LMSendMSGSchema mLMSendMSGSchema = new LMSendMSGSchema();

    private MsgSendBase mMsgSendBase = new MsgSendBase();
    
    /** 各种要素存放的琏表
     *  1--基本要素、和常量要素相同，但是优先级最低
     *  2--扩展要素，根据SQL语句从新计算
     *  3--常量要素（只取默认值）
     */
    private LMCalFactorSet mCalFactors1 = new LMCalFactorSet(); //存放基本要素

    public LMCalFactorSet mCalFactors = new LMCalFactorSet();    

    /**
     * 增加基本要素
     * @param cFactorCode 要素的编码
     * @param cFactorValue  要素的数据值
     */
    public void addBasicFactor(String cFactorCode, String cFactorValue)
    {
        LMCalFactorSchema tS = new LMCalFactorSchema();
        tS.setFactorCode(cFactorCode);
        tS.setFactorDefault(cFactorValue);
        tS.setFactorType("1");
        mCalFactors1.add(tS);
    }

    public MsgSendBase getmMsgSendBase() {
		return mMsgSendBase;
	}

	public void setmMsgSendBase(MsgSendBase mMsgSendBase) {
		this.mMsgSendBase = mMsgSendBase;
	}

	// @Method
    public void setTaskCode(String tTaskCode)
    {
    	mTaskCode = tTaskCode;
    }
    
    public boolean applySend(String tTaskCode){
    	mTaskCode = tTaskCode;
        this.addBasicFactor("Contno", mMsgSendBase.getContno());
        this.addBasicFactor("AppntNo", mMsgSendBase.getAppntNo());
        this.addBasicFactor("AppntName", mMsgSendBase.getAppntName());
        this.addBasicFactor("InsuredNo", mMsgSendBase.getInsuredNo());
        this.addBasicFactor("InsuredName", mMsgSendBase.getInsuredName());
        this.addBasicFactor("InsuredSex", mMsgSendBase.getInsuredSex());
        this.addBasicFactor("InsuredBirthday", mMsgSendBase.getInsuredBirthday());
        this.addBasicFactor("InsuredAppAge", mMsgSendBase.getInsuredAppAge());
        this.addBasicFactor("AppntSex", mMsgSendBase.getAppntSex());
        this.addBasicFactor("AppntBirthDay", mMsgSendBase.getAppntBirthDay());
        this.addBasicFactor("AppntAppAge", mMsgSendBase.getAppntAppAge());
        this.addBasicFactor("RiskCode", mMsgSendBase.getRiskCode());
        this.addBasicFactor("RiskName", mMsgSendBase.getRiskName());
        this.addBasicFactor("ApplyDate", mMsgSendBase.getApplyDate());
        this.addBasicFactor("ApplyNoType", mMsgSendBase.getApplyNoType());
        this.addBasicFactor("BusinessType", mMsgSendBase.getBusinessType());
        this.addBasicFactor("MobilPhone", mMsgSendBase.getMobilPhone());
        this.addBasicFactor("ManageCom", mMsgSendBase.getManageCom());
        this.addBasicFactor("PolYear", mMsgSendBase.getPolYear());
        this.addBasicFactor("Operator", mMsgSendBase.getOperator());
        this.addBasicFactor("EdorName", mMsgSendBase.getEdorName());
        this.addBasicFactor("EndorsementNo", mMsgSendBase.getEndorsementNo());
        //短信内容：“金额”
        this.addBasicFactor("Prem", mMsgSendBase.getPrem());
        //短信内容：“类型”
        this.addBasicFactor("SpecType", mMsgSendBase.getSpecType());
        //短信内容：“银行账户号码”
        this.addBasicFactor("BankAccNo", mMsgSendBase.getBankAccNo());
        
        this.addBasicFactor("IDNo", mMsgSendBase.getIDNo());
        this.addBasicFactor("HospitalName", mMsgSendBase.getHospitalName());
        this.addBasicFactor("SumFee", mMsgSendBase.getSumFee());
        this.addBasicFactor("SelfAmnt", mMsgSendBase.getSelfAmnt());
        this.addBasicFactor("BankName", mMsgSendBase.getBankName());

        if(!sendMSG()){
        	System.out.println("任务号=="+tTaskCode+"发送短信失败");
        	return false;
        }
    	return true;
    }
    

    /**
     * 公式计算函数
     * @return: String 计算的结果，只能是单值的数据（数字型的转换成字符型）
     * @author: wujun
     **/
    public String doAnalyze()
    {

        System.out.println("start calculate++++++++++++++");
        if (!checkAnalyzeMSG())
        {
            return null;
        }
        //取得数据库中短信模版
        LMSendMSGDB tLMSendMSGDB = new LMSendMSGDB();
        tLMSendMSGDB.setTaskcode(mTaskCode);
        LMSendMSGSet tLMSendMSGSet = tLMSendMSGDB.query();
        if(tLMSendMSGSet!=null&&tLMSendMSGSet.size()>0){
        	mLMSendMSGSchema = tLMSendMSGSet.get(1);
        }else{
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AnalyzeMSG";
            tError.functionName = "doAnalyze";
            tError.errorMessage = "查询短信模版时出错。";
            this.mErrors.addOneError(tError);
            return null;
        }
        
        
        
        String massage = mLMSendMSGSchema.getMSGContents();

        //解释SQL语句中的变量
        if (!interpretFactorInSQL())
        {
            return "0";
        }
        //执行SQL语句
        System.out.println("发送短险的短信内容是....."+massage);
        return mLMSendMSGSchema.getMSGContents();
    }

    /**
     * 执行SQL语句
     * @return
     */
    private  Vector getMessage()
    {
    	System.out.println("进入获取信息 getMessage()------------------------");
    	 Vector tVector = new Vector();
		   //短信内容
         String tMSGContents=""; 
          
          SmsMessageNew tKXMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的MessageNew元素
          	
          	 tMSGContents = doAnalyze();
          if(tMSGContents==null||"".equals(tMSGContents)){
        	  return tVector;
          }
          // #3302 暂时就这样吧。修改8位管理机构，任务号 后建
          String nManageCom=null;
          String mManageCom=mMsgSendBase.getManageCom();
          if (mManageCom.trim().length() == 2){
        	  nManageCom = mManageCom.trim() + "000000";
          }else if (mManageCom.trim().length() == 4){
        	  nManageCom = mManageCom.trim() + "0000";
          }else if (mManageCom.trim().length() == 6){
        	  nManageCom =mManageCom.trim() + "00";
          }else{
        	  nManageCom=mManageCom;
          }
          System.out.println("oldManageCom======="+mManageCom);
          System.out.println("newManageCom======="+nManageCom);
          tKXMsg.setOrgCode(nManageCom); // 新增短信8位管理机构
          System.out.println("最后一测manageCom====="+tKXMsg.getOrgCode());
          // # 3302 塞个值就好。具体干嘛我也不知道
          
          tKXMsg.setReceiver(mMsgSendBase.getMobilPhone()); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
          tKXMsg.setContents(tMSGContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
          tVector.add(tKXMsg);
      		
                                       
          // 在LCCSpec表中插入值  ********************************************************  
      	LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
          tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
          tLCCSpecSchema.setContNo(mLMSendMSGSchema.getTaskcode()+mMsgSendBase.getContno());
          tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);      
          tLCCSpecSchema.setPrtSeq(null);
          //放 ljspayb   SerialNo
          String tLimit = PubFun.getNoLimit(mMsgSendBase.getManageCom());
	      String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
	      tLCCSpecSchema.setSerialNo(serNo); //流水号                
          tLCCSpecSchema.setEndorsementNo(mMsgSendBase.getEndorsementNo());
          tLCCSpecSchema.setSpecType(mMsgSendBase.getSpecType()); //kxD-短险宽限期发送短息
          tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
          tLCCSpecSchema.setSpecContent(tMSGContents+"客户手机号："+ mMsgSendBase.getMobilPhone());
          tLCCSpecSchema.setOperator(mMsgSendBase.getOperator());
          tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
          tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
          tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
          tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
          tLCCSpecSchema.getDB().insert();
             
             //到此处
      System.out.println("都结束了----------------------------------------------------------------");    	 

        return tVector;
    }

    /**
     * 解释SQL语句中的变量
     * @return
     */
    private boolean interpretFactorInSQL()
    {
        String Massage, tMassage = "", tMassage1 = "";
        Massage = mLMSendMSGSchema.getMSGContents();

        try
        {
            while (true)
            {
            	tMassage = PubFun.getStr(Massage, 2, "?");
                if (tMassage.equals(""))
                {
                    break;
                }
                
                tMassage1 = "?" + tMassage.trim() + "?";
                //替换变量
                Massage = StrTool.replaceEx(Massage, tMassage1, getValueByName(tMassage));
//        tSql=tSql.replace(tStr,getValueByName(tStr));
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "Calculator";
            tError.functionName = "interpretFactorInSQL";
            tError.errorMessage = "解释" + Massage + "的变量:" + tMassage1 + "时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLMSendMSGSchema.setMSGContents(Massage);
        return true;
    }


    //将信息整理发送短信
    public boolean sendMSG(){
    	 System.out.println("发送短信服务开始=========");
//         SmsServiceSoapBindingStub binding = null;
         SMSClient tSMSClient = new SMSClient();
//         try {
//             binding = (SmsServiceSoapBindingStub)new SmsServiceServiceLocator().
//                       getSmsService(); //创建binding对象
//         } catch (javax.xml.rpc.ServiceException jre) {
//             jre.printStackTrace();
//             return false;
//         }
//         binding.setTimeout(60000);
         Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。
         
         Vector vec = new Vector();
         vec = getMessage();
         Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数
         //拆分Vector
         System.out.println("---------vec="+vec.size());

         if (!vec.isEmpty()) 
         {
         	for(int i=0;i<vec.size();i++)
             {
         		tempVec.clear();
             	tempVec.add(vec.get(i));
             	
             	SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的MessagesNew元素
                 msgs.setOrganizationId("86"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                 msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                 msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                 msgs.setStartDate(PubFun.getCurrentDate()); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                 msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                 msgs.setEndDate(PubFun.getCurrentDate()); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                 msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                 msgs.setTaskValue(SMSClient.mes_taskVlaue15);
                 
             	msgs.setMessages((SmsMessageNew[]) tempVec.toArray(new SmsMessageNew[tempVec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
//                 try 
//                 {
//                     value = binding.sendSMS("Admin", "Admin", msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
                    tSMSClient.sendSMS(msgs);
//                	 System.out.println(value.getStatus());
//                     System.out.println(value.getMessage());
//                 } 
//                 catch (RemoteException ex) 
//                 {
//                     ex.printStackTrace();
//                     return false;
//                 }
             }
         } 
         else 
         {
             System.out.print("没有需要发送的短信");
             return false;
             
         }
         System.out.println("短信发送服务结束===========");

    	
    	
    	return true;
    }
    


    /**
     * 校验计算的输入是否足够
     * @return boolean 如果不正确返回false
     */
    private boolean checkAnalyzeMSG()
    {
        if (mTaskCode == null || mTaskCode.equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AnalyzeMSG";
            tError.functionName = "checkAnalyzeMSG";
            tError.errorMessage = "解析短信时必须编码。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据变量名得到变量的值
     * @return String 如果不正确返回"",否则返回变量值
     */
    private String getValueByName(String cVarName)
    {
        cVarName = cVarName.toLowerCase();
        int i, iMax;
        String tReturn = "";
        LMCalFactorSchema tC = new LMCalFactorSchema();
        iMax = mCalFactors1.size();
        for (i = 1; i <= iMax; i++)
        {
            tC = mCalFactors1.get(i);
            if (tC.getFactorCode().toLowerCase().equals(cVarName))
            {
                tReturn = tC.getFactorDefault();
                break;
            }
        }
        return tReturn;
    }


    public LMCalFactorSet getCalFactors1()
    {
    	return mCalFactors1;
    }
}
