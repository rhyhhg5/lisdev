package com.sinosoft.lis.pubfun.diskimport;

import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SchemaSet;

/**
 * <p>Title: 保全磁盘导入类</p>
 * <p>Description: 保全只会从一个sheet中导入，导入数据都会放入一个临时表</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class BqDiskImporter
{
    public CErrors mErrrors = new CErrors();

    /** 保全磁盘导入临时表的schema */
    private static final String schemaClassName =
            "com.sinosoft.lis.schema.LPDiskImportSchema";

    /** 保全磁盘导入临时表的set */
    private static final String schemaSetClassName =
            "com.sinosoft.lis.vschema.LPDiskImportSet";

    /** 保全使用默认的导入方式 */
    private DefaultDiskImporter importer = null;

    /**
     * 构造函数
     * @param fileName String
     * @param configFileName String
     * @param sheetName String
     */
    public BqDiskImporter(String fileName, String configFileName,
            String sheetName)
    {
        importer = new DefaultDiskImporter(fileName, configFileName, sheetName);
    }

    /**
     * 执行导入
     * @return boolean
     */
    public boolean doImport()
    {
        importer.setSchemaClassName(schemaClassName);
        importer.setSchemaSetClassName(schemaSetClassName);
        if (!importer.doImport())
        {
            mErrrors.copyAllErrors(importer.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 得到导入结果
     * @return SchemaSet
     */
    public SchemaSet getSchemaSet()
    {
        return importer.getSchemaSet();
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        String fileName = "C:/Documents and Settings/qiuyang/桌面/磁盘导入/团单磁盘投保模板文件/18000620008.xls";
        String configFileName = "F:/PROJECT/lis_coding/ui/temp/BqImport.xml";
        String sheetName = "GA";
        BqDiskImporter importer = new BqDiskImporter(fileName, configFileName,
                sheetName);
        importer.doImport();
        LPDiskImportSet set = (LPDiskImportSet) importer.getSchemaSet();
        System.out.println(set.toString());
    }
}
