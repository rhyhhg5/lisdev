package com.sinosoft.lis.pubfun.diskimport.copy;

import java.util.List;
import com.f1j.ss.BookModelImpl;
import com.f1j.util.F1Exception;

/**
 * <p>Title: excel文件的工作簿类</p>
 * <p>Description: 封装了BookModelImpl，对应excel的工作簿</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class Sheet {
    /** 一个book对应一个excel文件 */
    private BookModelImpl book = null;
    //public int size = book.getLastRow();

    /** sheet编号 */
    private int sheetId = 0;

    /** 表头列表 */
    private List headList = null;

    /** 在xls文件中用于版本号定义的列数，这些列没有数据，在计算Sheet总数时要减去*/
    private static final int NODATAROWNUMBER = 1;

    /**
     * 构造函数
     * @param book BookModelImpl
     * @param sheetId int
     * @param headList List
     */
    public Sheet(BookModelImpl book, int sheetId, List headList) {
        this.book = book;
        this.sheetId = sheetId;
        this.headList = headList;
    }

    /**
     * 得到sheet的名称
     * @return String
     */
    public String getName() {
        try {
            return book.getSheetName(sheetId);
        } catch (F1Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 根据列数得到相应的表头
     * @param col int
     * @return String
     */
    public String getHead(int col) {
        return (String) headList.get(col);
    }

    /**
     * 根据行数和列数得到sheet的数据
     * @param row int
     * @param col int
     * @return String
     */
    public String getText(int row, int col) {
        try {
            return book.getText(sheetId, row + 1, col);
        } catch (F1Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 根据行数和列名得到sheet的数据
     * @param row int
     * @param colName String
     * @return String
     */
    public String getText(int row, String colName) {
        try {
            for (int col = 0; col < headList.size(); col++) {
                String head = (String) headList.get(col);
                if (head.equalsIgnoreCase(colName)) {
                    return book.getText(sheetId, row + 1, col);
                }
            }
        } catch (F1Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 根据行数和列数得到sheet的数据
     * @param row int
     * @param col int
     * @return String
     */
    public String getText(int row, int col, int startRow) {
        try {
            return book.getText(sheetId, row + startRow, col);
        } catch (F1Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 根据行数和列名得到sheet的数据
     * @param row int
     * @param colName String
     * @return String
     */
    public String getText(int row, String colName, int startRow) {
        try {
            for (int col = 0; col < headList.size(); col++) {
                String head = (String) headList.get(col);
                if (head.equalsIgnoreCase(colName)) {
                    return book.getText(sheetId, row + startRow, col);
                }
            }
        } catch (F1Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 得到加入了版本号的Sheet总行数
     * @return int
     */
    public int getRowSizeOfVersionSheet(int sheet) {
        try {
            book.setSheet(sheet);
             
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            for (int i = 0; i <= book.getLastRow(); i++) {
                String id = book.getText(i, 0);
                /**
                 * 这一段代码的意义在于，当Sheet中间有一行没有序号的时候，
                 * 就只读取从Sheet第一行到没有序号这一行之间的有效数据，
                 * 后面的数据不再读取，由于这个类没有CError成员，
                 * 所以不报错，在后台报告。
                 */
                if ((id == null) || (id.equals(""))) {
                    System.out.println("Sheet 中间出现空白行，读入空白行之前的数据。");
                    return (i - 1  );
                }
            }
            return book.getLastRow() ;
        } catch (F1Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    
    /**
     * 得到加入了版本号的Sheet总行数
     * @return int
     */
    public int getRowSizeOfVersionSheet1(int sheet) {
        try {
            book.setSheet(sheet);
           
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            for (int i = 0; i < book.getLastRow(); i++) {
                String id = book.getText(i, 0);
                /**
                 * 这一段代码的意义在于，当Sheet中间有一行没有序号的时候，
                 * 就只读取从Sheet第一行到没有序号这一行之间的有效数据，
                 * 后面的数据不再读取，由于这个类没有CError成员，
                 * 所以不报错，在后台报告。
                 */
                if ((id == null) || (id.equals(""))) {
                    System.out.println("Sheet 中间出现空白行，读入空白行之前的数据。");
                    return (i - 1  );
                }
            }
            return book.getLastRow()  ;
        } catch (F1Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    
    /**
     * 得到总行数
     * @return int
     */
    public int getRowSize() {
        try {
            for (int i = 0; i < book.getLastRow(); i++) {
                String id = book.getText(i, 0);
                if ((id == null) || (id.equals(""))) {
                    return (i - 1);
                }
            }
            return book.getLastRow();
        } catch (F1Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 得到总列数
     * @return int
     */
    public int getColSize() {
        return headList.size();
    }
}
