package com.sinosoft.lis.pubfun.diskimport.copy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.StrTool;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 1.0
 */
public class MultiSheetImporter extends MultiDiskImporter {
    /** Sheet的名字 */
    private String[] mSheetNames;
    /** 对应Sheet所导入的表名 */
    private String[] mTableNames;
    /** 对应Set */
    private HashMap mSheetAndSet = new HashMap();
    /** 对应Scheam */
    private HashMap mSheetAndSchema = new HashMap();
    /** 是否填入版本号 */
    private boolean isInit = false;
    /** 最终数据 */
    private HashMap mData = new HashMap();
    /** 读取文件开始行 */
    private int mStartRows = 1;

    /** xls文件名*/
    private String fileName = null;

    /** xml文件名*/
    private String configFileName = null;
    /**
     * 处理多个sheet中的一个
     *
     * @param sheet Sheet
     * @return boolean
     * @todo Implement this com.sinosoft.lis.pubfun.diskimport.MultiDiskImporter
     *   method
     */
    protected boolean dealOneSheet(Sheet sheet) {
        if (!initSchemaSet()) {
            return false;
        }
        String sheetName = sheet.getName();
        /**
         * 导致sheet.size出错的所在
         */
        int size = sheet.getRowSizeOfVersionSheet(0);
        SchemaSet schemaSet = null;
        if (!sheetName.equals("Ver")) {
            try {
                schemaSet = (SchemaSet) Class.forName((String) mSheetAndSet.get(
                        sheetName)).newInstance();
            } catch (Exception ex) {
                String str = "生成Set失败!";
                buildError("dealOneSheet", str);
                System.out.println(
                        "在程序MultiSheetImporter.dealOneSheet() - 57 : " +
                        sheet.getName() + str);
                return false;
            }
            for (int i = 0; i < size; i++) {
                Schema schema = createSchema(sheet, i);
                if (schema == null) {
                    return false;
                }
                schemaSet.add(schema);
            }
            mData.put(sheetName, schemaSet);
        }
        /** 获取版本号 **/
        if (sheetName.equals("Ver")) {
            String ver = getVersion(sheet);
            System.out.println("版本号为：" + ver);
            mData.put("Ver", ver);
        }
        return true;
    }

    /**
     * 执行磁盘导入
     * @return boolean
     */
    public boolean doImport() {
      System.out.println(" Into MulteSheetImporter doImport");
        XlsImporter importer = new XlsImporter(fileName, configFileName,
                                               mSheetNames);
        importer.ImportMutilSheet();
        Sheet[] sheets = importer.getSheets();
        if (!dealSheets(sheets)) {
            return false;
        }
//        deleteFile();
        return true;
    }

    /**
     * Deal with more than one Sheet, get correct rownumber of them
     * @param sheet Sheet
     * @param numberOfSheet int
     * @return boolean
     */
    protected boolean dealOneSheet(Sheet sheet, int numberOfSheet) {
        if (!initSchemaSet()) {
            return false;
        }
        String sheetName = sheet.getName();
        /**
         * 导致sheet.size出错的所在
         */
        int size = sheet.getRowSizeOfVersionSheet(numberOfSheet);
        SchemaSet schemaSet = null;
        if (!sheetName.equals("Ver")) {
            try {
                schemaSet = (SchemaSet) Class.forName((String) mSheetAndSet.get(
                        sheetName)).newInstance();
            } catch (Exception ex) {
                String str = "生成Set失败!";
                buildError("dealOneSheet", str);
                System.out.println(
                        "在程序MultiSheetImporter.dealOneSheet() - 57 : " +
                        sheet.getName() + str);
                return false;
            }
            for (int i = 0; i < size; i++) {
                Schema schema = createSchema(sheet, i);
                if (schema == null) {
                    return false;
                }
                schemaSet.add(schema);
            }
            mData.put(sheetName, schemaSet);
        }
        /** 获取版本号 **/
        if (sheetName.equals("Ver")) {
            String ver = getVersion(sheet);
            System.out.println("版本号为：" + ver);
            mData.put("Ver", ver);
        }
        return true;
    }

    /**
     * 返回版本号
     *
     * @param sheet Sheet
     * @return String
     */
    private String getVersion(Sheet sheet) {
        String tSheetName = sheet.getName();
        String ver = "";
        int col = sheet.getColSize();
        String head = sheet.getHead(0);
        if (!(head == null) && !(head.equals(""))) {
            ver = sheet.getText(0, 0);
        }
        if (ver != null && !ver.equals("")) {
            return ver;
        }
        return null;
    }

    /**
     * createSchema
     *
     *
     * @param sheet Sheet
     * @param i int
     * @return Schema
     */
    private Schema createSchema(Sheet sheet, int row) {
        try {
            String tSheetName = sheet.getName();
            if (!tSheetName.equals("Ver")) {
                String tSchemaClassName = (String) mSheetAndSchema.get(
                        tSheetName);
                Class schemaClass = Class.forName(tSchemaClassName);
                Schema schema = (Schema) schemaClass.newInstance();
                int col = sheet.getColSize();
                for (int i = 0; i < col; i++) {
                    String head = sheet.getHead(i);
                    if ((head == null) || (head.equals(""))) {
                        continue;
                    }
                    String value = sheet.getText(row, i, mStartRows);
                    String methodName = getSetMethodName(schemaClass, head);
                    if (methodName == null) {
                        String str = "Schema中没有" + head + "对应方法的描述!";
                        buildError("createSchema", str);
                        System.out.println(
                                "在程序MultiSheetImporter.createSchema() - 78 : " +
                                str);
                        return null;
                    }
                    Class[] paramType = new Class[1];
                    paramType[0] = Class.forName("java.lang.String");
                    Method method = null;
                    method = schemaClass.getMethod(methodName, paramType);
                    Object[] args = new Object[1];
                    args[0] = StrTool.cTrim(value);
                    method.invoke(schema, args);
                }
                return schema;
            }
        } catch (ClassNotFoundException ex) {
            String str = "没有到对应Schema!";
            buildError("createSchema", str);
            System.out.println("在程序MultiSheetImporter.createSchema() - 61 : " +
                               str);
            return null;
        } catch (IllegalAccessException ex) {
            String str = "类型描述的不是Schema!";
            buildError("createSchema", str);
            System.out.println("在程序MultiSheetImporter.createSchema() - 66 : " +
                               str);
            return null;
        } catch (InstantiationException ex) {
            String str = "声明类失败!";
            buildError("createSchema", str);
            System.out.println("在程序MultiSheetImporter.createSchema() - 71 : " +
                               str);
            return null;
        } catch (InvocationTargetException ex) {
            String str = "执行set方法失败!";
            buildError("createSchema", str);
            System.out.println("在程序MultiSheetImporter.createSchema() - 103 : " +
                               str);
            return null;
        } catch (IllegalArgumentException ex) {
            String str = "执行set方法失败!";
            buildError("createSchema", str);
            System.out.println("在程序MultiSheetImporter.createSchema() - 103 : " +
                               str);
            return null;
        } catch (SecurityException ex) {
            String str = "没有找到对应方法!";
            buildError("createSchema", str);
            System.out.println("在程序MultiSheetImporter.createSchema() - 114 : " +
                               str);
            return null;
        } catch (NoSuchMethodException ex) {
            String str = "没有找到对应方法!";
            buildError("createSchema", str);
            System.out.println("在程序MultiSheetImporter.createSchema() - 114 : " +
                               str);
            return null;
        }
        return null;
    }

    /**
     * 得到shema中的所有和sheet的head相匹配的set方法
     * @param schemaClass Class
     * @param head String
     * @return String
     */
    private String getSetMethodName(Class schemaClass, String head) {
        Method[] methods = schemaClass.getMethods();
        for (int i = 0; i < methods.length; i++) {
            String methodName = methods[i].getName();
            if ((methodName.length() > 3) &&
                (methodName.substring(0, 3).equals("set"))) {
                String subName = methodName.substring(3);
                if (subName.equalsIgnoreCase(head)) {
                    return methodName;
                }
            }
        }
        return null;
    }

    /**
     * initSchemaSet
     *
     * @return boolean
     */
    private boolean initSchemaSet() {
        if (isInit) {
            return true;
        }
        /** 首先判断sheet与tablename是否对应 */
        if (mSheetNames == null || mSheetNames.length <= 0) {
            String str = "没有定义SheetName!";
            buildError("initSchemaSet", str);
            System.out.println("在程序MultiSheetImporter.initSchemaSet() - 44 : " +
                               str);
            return false;
        }
        if (mTableNames == null || mTableNames.length <= 0) {
            String str = "没有定义对应Sheet所导入的表名!";
            buildError("initSchemaSet", str);
            System.out.println("在程序MultiSheetImporter.initSchemaSet() - 52 : " +
                               str);
            return false;
        }
        if (mSheetNames.length != mTableNames.length) {
            String str = "SheetName 与 TableName不对应!";
            buildError("initSchemaSet", str);
            System.out.println("在程序MultiSheetImporter.initSchemaSet() - 58 : " +
                               str);
            return false;
        }

        for (int i = 0; i < mSheetNames.length; i++) {
            /** 判断是否是定义版本的Sheet，版本Sheet是保留Sheet */
            if (!mSheetNames[i].equals("Ver")) {
                String SetName = "com.sinosoft.lis.vschema." + mTableNames[i] +
                                 "Set";
                String SchemaName = "com.sinosoft.lis.schema." + mTableNames[i] +
                                    "Schema";
                mSheetAndSchema.put(mSheetNames[i], SchemaName);
                mSheetAndSet.put(mSheetNames[i], SetName);
            }
        }
        isInit = true;
//    if (mSheetAndSchema.size() != mSheetNames.length) {
//      String str = "有重复SheetName!";
//      buildError("initSchemaSet", str);
//      System.out.println("在程序MultiSheetImporter.initSchemaSet() - 77 : " + str);
//      return false;
//    }

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "MultiSheetImporter";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 用于调试的主函数
     * @param args String[]
     */
    public static void main(String[] args) {
        String file = "D:/wuligang/workspace/testData/16000002222.xls";
        String conf = "D:/wuligang/workspace/testData/GrpDiskImport.xml";
        String[] sheet = {"Sheet1", "Sheet2", "Ver"};
        MultiSheetImporter m = new MultiSheetImporter(file, conf, sheet);
        System.out.println("MultiSheetImporter(file, conf, sheet) finished");
        m.setTableName(new String[] {"LCInsuredList", "LCInsuredList", "Ver"});
        System.out.println(
                "setTableName(new String[] {'LCInsuredList', 'Ver'}) finished");
        if (!m.doImport()) {
            System.out.println("m : " + m.mErrors.getContent());
        }
    }

    /**
     * 导入多Sheet
     * @param fileName String
     * @param configFileName String
     * @param sheetNames String[]
     */
    public MultiSheetImporter(String fileName, String configFileName,
                              String[] sheetNames) {
        super(fileName, configFileName, sheetNames);
        mSheetNames = sheetNames;
        this.fileName = fileName;
        this.configFileName = configFileName;
    }

    public void setTableName(String[] tTables) {
        mTableNames = tTables;
    }

    /**
     * 得到返回结果
     * @return SchemaSet
     */
    public HashMap getResult() {
        return mData;
    }

    public void setMStartRows(int mStartRows) {
        this.mStartRows = mStartRows;
    }

    public int getMStartRows() {
        return mStartRows;
    }

    /**
     * 处理所有导入的sheet
     * @param sheets Sheet[]
     * @return boolean
     */
    protected boolean dealSheets(Sheet[] sheets) {
        for (int i = 0; i < sheets.length; i++) {
            if (!dealOneSheet(sheets[i], i)) {
                return false;
            }
        }
        return true;
    }

}
