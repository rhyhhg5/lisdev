package com.sinosoft.lis.pubfun.ftp;

import java.io.*;
import java.net.SocketException;
import java.util.HashMap;
import org.apache.commons.net.ftp.*;
import java.util.ArrayList;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 本类使用org.apache.commons.net.ftp包实现FTP上下载功能
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class FTPTool extends FTPClient
{
    //服务器名，可以是IP，也可以是域名
    private String mServer = null;

    //用户名，默认为匿名
    private String mUserName = "anonymous";

    //密码，为支持匿名登录，增加了初始值
    private String mPassword = "yangyalin@sinosoft.com.cn";

    //ftp连接模式,此处默认的构造函数为被动模式,如果需要采用主动模式,传入参数:ActiveMode
    private String mTransMode = "PassiveMode";

    //端口，默认为21
    private int mPort = 21;

    //错误代码列表
    private ArrayList mErrorsList = new ArrayList();

    private FTPReplyCodeName mFTPReplyCodeName = new FTPReplyCodeName();
    
    public ArrayList<String> arFiles = new  ArrayList<String>();

    public FTPTool()
    {
    }

    /**
     * 需要传入服务器的构造函数，使用匿名登录
     * @param cServer String：见全局变量mServer
     */
    public FTPTool(String cServer)
    {
        mServer = cServer;
    }

    /**
     * 需要传入服务器的构造函数，使用匿名登录
     * @param cServer String：见全局变量mServer
     */
    public FTPTool(String cServer, int cPort)
    {
        mServer = cServer;
        mPort = cPort;
    }

    /**
     * 需要传入服务器、用户名、密码的构造函数
     * @param cServer String：见全局变量mServer
     * @param cUserName String：见全局变量mUserName
     * @param cPassword String：见全局变量mPassword
     */
    public FTPTool(String cServer, String cUserName, String cPassword)
    {
       this(cServer);
        mUserName = cUserName;
        mPassword = cPassword;
    }
    /**
     * 需要传入服务器、用户名、密码的构造函数
     * @param cServer String：见全局变量mServer
     * @param cUserName String：见全局变量mUserName
     * @param cPassword String：见全局变量mPassword
     * @param cPort String：见全局变量mPort
     */
    public FTPTool(String cServer,
                   String cUserName,
                   String cPassword,
                   int cPort)
    {
        this(cServer, cUserName, cPassword);
        mPort = cPort;
    }
    /**
     * 需要传入服务器、用户名、密码的构造函数
     * @param cServer String：见全局变量mServer
     * @param cUserName String：见全局变量mUserName
     * @param cPassword String：见全局变量mPassword
     * @param cPort String：见全局变量mPort
     * @param cTransMode String：见全局变量mTransMode
     */
    public FTPTool(String cServer, String cUserName, String cPassword,int cPort,String cTransMode)
    {
       this(cServer,cUserName,cPassword,cPort);
       this.mTransMode = cTransMode;
    }

    /**
     * 登录FTP服务器
     * @return boolean：登录成功true，否则false
     */
    public boolean loginFTP() throws SocketException, IOException
    {
        if(this.mServer == null || this.mServer.equals(""))
        {
            mErrorsList.add(String.valueOf(FTPReplyCodeName.MCODE_600));
            System.out.println(mFTPReplyCodeName
                               .getCodeName(FTPReplyCodeName.MCODE_600,
                                            mFTPReplyCodeName.LANGUSGE_CHINESE));
            return false;
        }

        setDefaultPort(this.mPort); //设置端口
        connect(this.mServer); //ip地址
        int replyCode = getReplyCode(); //得到连接ftp服务器操作的返回代码

        if(!FTPReply.isPositiveCompletion(replyCode)) //若操作失败
        {
            mErrorsList.add(String.valueOf(getReplyCode()));
            System.out.println(mFTPReplyCodeName
                               .getCodeName(getReplyCode(),
                                            mFTPReplyCodeName.LANGUSGE_CHINESE));
            return false;
        }

        if(!login(mUserName, mPassword))
        {
            mErrorsList.add(String.valueOf(getReplyCode()));
            System.out.println(mFTPReplyCodeName
                               .getCodeName(getReplyCode(),
                                            mFTPReplyCodeName.LANGUSGE_CHINESE));
            disconnect();

            return false;
        }

        return true;
    }
    /**
     * 上载文件
     * @param filePath String：文件路径
     * @return boolean：成功与否标志
     * @throws SocketException
     * @throws IOException
     */
    public boolean upload(String filePath)
    {
        return upload(".", filePath);
    }
    /**
     * 上载文件
     * @param directory:要上传的目录服务器
     * @param filePath String：文件路径
     * @return boolean：成功与否标志
     * @throws SocketException
     * @throws IOException
     */
    public boolean upload(String directory, String filePath)
    {
        try
        {
            changeWorkingDirectory(directory);
            setFileType(FTP.BINARY_FILE_TYPE);
            if("ActiveMode".equals(this.mTransMode)){
                this.enterLocalActiveMode();
            }else{
                this.enterLocalPassiveMode();
            }
            String fileName = filePath.substring(filePath.lastIndexOf(File.separator) + 1);  //FTP服务器显示的文件名
            FileInputStream fis = new FileInputStream(filePath);  //打开文件
            if(!storeFile(fileName, fis))  //FTP上传文件
            {
                this.mErrorsList.add(String.valueOf(getReplyCode()));
                fis.close();
                return false;
            }
            fis.close();
        }
        catch(SocketException ex)
        {
            ex.printStackTrace();
            mErrorsList.add(String.valueOf(mFTPReplyCodeName.MCODE_602));
            return false;
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
            mErrorsList.add(String.valueOf(mFTPReplyCodeName.MCODE_601));
            return false;
        }

        return true;
    }

    /**
     * 下载文件夹内所有文件
     * @param cDirectory String：服务器下载路径
     * @param cLocalDirectory：本地文件保存路径
     * @return boolean：下载成功true，否则false
     * @throws IOException
     */
    public String[] downloadOneDirectory(String cDirectory,
                            String cLocalDirectory) throws IOException
    {
        changeWorkingDirectory(cDirectory);
        this.enterLocalPassiveMode();

        FTPFile[] files = listFiles();
        String[] fileName = new String[files.length];

        for(int i = 0; i < files.length; i++)
        {
            if(files[i].isDirectory())
            {
                    continue;
            }

            if(!downloadFile(cDirectory, cLocalDirectory, files[i].getName()))
            {
                return null;
            }
            fileName[i] = files[i].getName();
        }

        return fileName;
    }

    /**
     * 下载文件夹内所有文件
     * @param cDirectory String：服务器下载路径
     * @param cLocalDirectory：本地文件保存路径
     * @return boolean：下载成功true，否则false
     * @throws IOException
     */
    public boolean download(String cDirectory,
                            String cLocalDirectory) throws IOException
    {
        changeWorkingDirectory(cDirectory);
        this.enterLocalPassiveMode();

        FTPFile[] files = listFiles();
        System.out.println("\nCount in dir " + cDirectory
                           + ": " + (files.length - 2));

        for(int i = 0; i < files.length; i++)
        {
            if(files[i].isDirectory())
            {
                //下载下级目录文件
                if(files[i].getName().equals(".")
                   || files[i].getName().equals(".."))
                {
                    continue;
                }

                File file = new File(cLocalDirectory + File.separator
                                     + files[i].getName());
                file.mkdir();
                if(!download(files[i].getName(),
                    cLocalDirectory + File.separator + files[i].getName()))
                {
                    return false;
                }
            }
            else
            {   
            	
                downloadFile(cDirectory, cLocalDirectory, files[i].getName());
            }
        }

        return true;
    }

    /**
     * 下载指定文件夹的指定文件
     * @param cDirectory String：服务器下载路径
     * @param cLocalDirectory：本地文件保存路径
     * @param cFileName：要下载的服务器文件，若为null，则全部下载
     * @return boolean：下载成功true，否则false
     * @throws IOException
     */
    public boolean downloadFile(String cDirectory,
                            String cLocalDirectory,
                            String cFileName)
    {
        System.out.print(cFileName);

        try
        {
            changeWorkingDirectory(cDirectory);
            File file = new File(cLocalDirectory + File.separator + cFileName);
            FileOutputStream fos = new FileOutputStream(file);
            retrieveFile(cFileName, fos);
            fos.close();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
            mErrorsList.add(String.valueOf(mFTPReplyCodeName.MCODE_601));
            System.out.println(": Fail");
            return false;
        }

        System.out.println(": Succ");

        return true;
    }

    /**
     * 上载文件 by lyc
     * @param directory:要上传的目录服务器
     * @param filePath String：文件路径
     * @return boolean：成功与否标志
     * @throws SocketException
     * @throws IOException
     */
    public boolean uploadAll(String directory, String filePath)
    {
        try
        {
            changeWorkingDirectory(directory);
            setFileType(FTP.BINARY_FILE_TYPE);
            if("ActiveMode".equals(this.mTransMode)){
                this.enterLocalActiveMode();
            }else{
                this.enterLocalPassiveMode();
            }
            File file=new File(filePath);  
        	String[] tFileNames = file.list();
        	for(int i=0; i<tFileNames.length; i++){
//            String fileName = filePath.substring(filePath.lastIndexOf(File.separator) + 1);  //FTP服务器显示的文件名
            FileInputStream fis = new FileInputStream(filePath+tFileNames[i]);  //打开文件
            if(!storeFile(tFileNames[i], fis))  //FTP上传文件
            {
                this.mErrorsList.add(String.valueOf(getReplyCode()));
                fis.close();
                return false;
            }
            fis.close();
        }
        }
        catch(SocketException ex)
        {
            ex.printStackTrace();
            mErrorsList.add(String.valueOf(mFTPReplyCodeName.MCODE_602));
            return false;
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
            mErrorsList.add(String.valueOf(mFTPReplyCodeName.MCODE_601));
            return false;
        }

        return true;
    }

 /**
     * 下载文件夹内所有文件 by lyc
     * @param cDirectory String：服务器下载路径
     * @param cLocalDirectory：本地文件保存路径
     * @return boolean：下载成功true，否则false
     * @throws IOException
     */
    public String[] downloadAllDirectory(String cDirectory,
                            String cLocalDirectory) throws IOException
    {
        changeWorkingDirectory(cDirectory);
        this.enterLocalPassiveMode();
        String[] filename = this.listNames();


        for(int i = 0; i < filename.length; i++)
        {

            if(!downloadFile(cDirectory, cLocalDirectory, filename[i]))
            {
                return null;
            }

        }

        return filename;
    }
    
    /**
     * 退出ftp服务器
     * @return boolean
     */
    public boolean logoutFTP()
    {
        try
        {
            logout();
            disconnect();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();


            return false;
        }

        return true;
    }

    /**
     * 得到中文出错信息
     * @return String：出错信息
     */
    public String getErrContent(int cLanguage)
    {
        StringBuffer err = new StringBuffer();

        for(int i = 0; i < this.mErrorsList.size(); i++)
        {
            String tCode = mErrorsList.get(i).toString();
            String codeName = mFTPReplyCodeName.getCodeName(Integer.parseInt(tCode),
                       cLanguage);
            err.append(codeName)
                .append("\n");
        }

        if(err.length() == 0)
        {
            return null;
        }

        return err.toString();
    }

    public static void main(String[] args)
    {
        FTPTool tFTPTool = new FTPTool("10.252.3.8", "sino", "sino", 21);
        try
        {
            if(!tFTPTool.loginFTP())
            {
                System.out.println(tFTPTool.getErrContent(1));
            }
            else
            {
//                tFTPTool.upload("D:/PICCH.rar");
                tFTPTool.downloadFile("/temp/BPO/ScanBackup/", "D:/ftp/xml", "ScanListSendOut_20070930_101044.xml");
            }
        }
        catch(SocketException ex)
        {
            ex.printStackTrace();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
    /** 
     * 递归遍历出目录下面所有文件 
     * @param pathName 需要遍历的目录，必须以"/"开始和结束 
     * @throws IOException 
     */  
    public void List(String pathName) throws IOException{  
        if(pathName.startsWith("/")&&pathName.endsWith("/")){  
            String directory = pathName;  
            //更换目录到当前目录  
            changeWorkingDirectory(directory);  
            FTPFile[] files = listFiles();  
            for(FTPFile file:files){  
                if(file.isFile()){  
                    arFiles.add(file.getName());  
                }else if(file.isDirectory()){  
                    List(directory+file.getName()+"/");  
                }  
            }  
        }  
    } 
    /**
	 * 移动文件.
	 * date 2019-01-16
	 * @param sourceFileName
	 * @param targetFile
	 * @throws IOException
	 */
	public void cutFile(String sourceFileName, String sourceDir, String targetDir) throws IOException {
		ByteArrayInputStream in = null;
		ByteArrayOutputStream fos = new ByteArrayOutputStream();
		try {
			// 变更工作路径
			this.changeWorkingDirectory(sourceDir);
			// 设置以二进制流的方式传输
			this.setFileType(FTP.BINARY_FILE_TYPE);
			// 将文件读到内存中
			this.retrieveFile(new String(sourceFileName.getBytes("GBK"), "iso-8859-1"), fos);
			in = new ByteArrayInputStream(fos.toByteArray());
			if (in != null) {
				this.changeWorkingDirectory(targetDir);
				this.storeFile(new String(sourceFileName.getBytes("GBK"), "iso-8859-1"), in);
				this.changeWorkingDirectory(sourceDir);
				this.deleteFile(sourceFileName);
			}
		} finally {
			// 关闭流
			if (in != null) {
				in.close();
			}
			if (fos != null) {
				fos.close();
			}
		}
	}
	
	/**
	 * 移动文件夹.
	 * date 2019-01-16
	 * @param sourceDir
	 * @param targetDir
	 * @throws IOException
	 */
	public void cutDirectiory(String sourceDir, String targetDir) throws IOException {
		// 新建目标目录
		// 获取源文件夹当前下的文件或目录
		FTPFile[] ftpFiles = listFiles(sourceDir);
		for (int i = 0; i < ftpFiles.length; i++) {
			if (ftpFiles[i].isFile()) {
				cutFile(ftpFiles[i].getName(), sourceDir, targetDir);
			} else if (ftpFiles[i].isDirectory()) {
				cutDirectiory(sourceDir + "/" + ftpFiles[i].getName(), targetDir + "/" + ftpFiles[i].getName());
			}
		}
	}
 
}
