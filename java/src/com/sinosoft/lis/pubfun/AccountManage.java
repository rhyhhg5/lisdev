/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;

import java.sql.*;
import java.util.*;
import java.util.Date;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Life Information System</p>
 * <p>Description:帐户处理类 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */
public class AccountManage
{

    public CErrors mErrors = new CErrors(); // 错误信息
    LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();
    LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
    private static HashMap mInsuAccNoHashMap = new HashMap(0);
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    
//  存放利息的缓存  by gzh 20101223
	private static Map rateCacheMap;// = new HashMap();

    public AccountManage()
    {
    	//by gzh 20101223
    	if (rateCacheMap == null)
			rateCacheMap = new HashMap();
    }


    public AccountManage(LCPolSchema cLCPolSchema) 
    {
    	mLCPolSchema.setSchema(cLCPolSchema);
	}
    
    //by gzh 20101223
    public static void deleteRateCash(){
        if (rateCacheMap != null)
            rateCacheMap.clear();
    }
    
//  by xp 20111116
//    public double getLoanRate(String aDate,String aRiskCode){
//    	SSRS tSSRS= new ExeSQL().execSQL("select rate from lminterestrate where riskcode='"+aRiskCode+"' and busstype='L' and startdate<='"+aDate+"' and enddate>='"+aDate+"' and ratetype='C' and rateintv=1 and rateintvunit='Y'");
//    	if(tSSRS.MaxRow!=1){
//    		System.out.println("利率获取失败");
//			return -1;
//    	}
//    	return Double.parseDouble(tSSRS.GetText(1, 1));
//    }
    
//  by wujun 20111116
    public double getLoanRate(String aDate,String manageCom,String prem,String aRiskCode){
    	 ExeSQL eq = new ExeSQL();
    	 String queryRateSQL = "";
    	 SSRS tSSRS = new SSRS();
        //查询保单机构是否有特殊配置
        String checkSql = "select 1 from ldcode where codetype = 'loancom' and code =  " +
        		" (case when '"+manageCom+"' = '86' then '"+manageCom+"' else subStr('"+manageCom+"',1,4) end ) with ur  " ; 
    	if(eq.getOneValue(checkSql)!=null&&!"".equals(eq.getOneValue(checkSql))){
    		queryRateSQL = "select rate from lminterestrate where riskcode='"+aRiskCode+"' " +
			"  and busstype='L' and startdate<='"+aDate+"' and enddate>='"+aDate+"' " +
			"  and comcode = (case when '"+manageCom+"' = '86' then '"+manageCom+"' else subStr('"+manageCom+"',1,4) end ) " +
			"  and int(minpremlimit)<="+prem+" " +
			"  and (int(nvl(maxpremlimit,0))>"+prem+" or maxpremlimit is null or maxpremlimit='') " +
			"and ratetype='C' and rateintv=1 and rateintvunit='Y'";
    	}else{
    		queryRateSQL = "select rate from lminterestrate where riskcode='"+aRiskCode+"' " +
    		"  and busstype='L' and startdate<='"+aDate+"' and enddate>='"+aDate+"' " +
    		"  and comcode = '86'" +
    		"  and int(minpremlimit)<="+prem+" " +
    		"  and (int(nvl(maxpremlimit,0))>"+prem+" or maxpremlimit is null or maxpremlimit='') " +
    		"and ratetype='C' and rateintv=1 and rateintvunit='Y'";
    	}
    	 tSSRS= eq.execSQL(queryRateSQL);
    	if(tSSRS.MaxRow!=1){
    		System.out.println("利率获取失败");
    		return -1;
    	}
    	return Double.parseDouble(tSSRS.GetText(1, 1));
    }
    
//  by xp 20111116
//  直接将参数代入公式计算
    public double getInterest(String startDate, double aBalance,String endDate,double aRate,String aRiskCode){
    	if(PubFun.calInterval(startDate,endDate, "D")<0){
    		System.out.println(PubFun.calInterval(startDate,endDate, "D"));
    		System.out.println("贷款起止日期错误");
			return -1;
    	}
    	LMLoanDB tLMLoanDB=new LMLoanDB();
    	tLMLoanDB.setRiskCode(aRiskCode);
    	if(!tLMLoanDB.getInfo()){
    		System.out.println("贷款定义表获取失败");
			return -1;
    	}
    	String tCalCode=tLMLoanDB.getRateCalCode();
    	if(tCalCode==null||tCalCode.equals("")||tCalCode.equals("null")){
    		System.out.println("贷款计算代码获取失败");
			return -1;
    	}
        Calculator tCalculator = new Calculator();
        tCalculator.setCalCode(tCalCode);
        tCalculator.addBasicFactor("LoanMoney", ""+aBalance);
        tCalculator.addBasicFactor("Rate",  ""+aRate);
        tCalculator.addBasicFactor("DateInterval", ""+ PubFun.calInterval(startDate,endDate, "D"));
        double tInterest = Double.parseDouble(tCalculator.calculate());
        if (tCalculator.mErrors.needDealError()||tInterest<0)
        {
            System.out.println(tCalculator.mErrors.getErrContent());
            mErrors.addOneError("计算利息出错");
            return -1;
        }
    	return tInterest;
    }
    
	/**
	 * 利息计算方法:该方法按6个月分段，分段后交给getInterest处理
	 *
	 * @param startDate
	 *            String 计息开始日期
	 * @param aBalance
	 *            double 本金
	 * @param endDate
	 *            String 止息日期
	 * @param aRate
	 *            double 利率
	 * @return double -1为错误
	 * @author xp 2011/11/15
	 */
//	public double getLoanInterest(String startDate, double aBalance,String endDate,String aRiskCode)
//	{
//		double oldMoney = aBalance;
//		double tInterest = 0.0;
//		// 间隔月份,舍弃法
//		int intvMonths = PubFun.calInterval(startDate, endDate, "M");
//		int intvCount = intvMonths/6;
//		if(intvCount<0){
//			// @@错误处理
//			System.out.println("AccountManage+getLoanInterest++--");
//			CError tError = new CError();
//			tError.moduleName = "AccountManage";
//			tError.functionName = "getLoanInterest";
//			tError.errorMessage = "计算起始日期大于终止日期!";
//			mErrors.addOneError(tError);
//			return -1;
//		}
//		// 按照半年累计利息
//		// 如果借款期限小于6个月,按一次利息计算，如果借款区间大于6个月，每6个月计算一次
//		// 同时考虑在某个计息区间内存在多个率的变动，这时按照不同利率分段计算
//			for (int j = 0; j <= intvCount; j++) {
//				if(j>0&&j==intvCount){
//					String startDate_i = PubFun.calDate(startDate, 6*j, "M", null) ;
//					String endDate_i = endDate;
//					double aRate=getLoanRate(startDate_i,aRiskCode);
//					if(aRate==-1){
//						// @@错误处理
//						System.out.println("AccountManage+getLoanInterest++--");
//						CError tError = new CError();
//						tError.moduleName = "AccountManage";
//						tError.functionName = "getLoanInterest";
//						tError.errorMessage = "获取利率失败1!";
//						mErrors.addOneError(tError);
//						return -1;
//					}
//					// 计算利息,起始日期算终止日期不算
//					tInterest = getInterest(startDate_i, aBalance,	endDate_i, aRate,aRiskCode);
//					if(tInterest==-1){
////						 @@错误处理
//						System.out.println("AccountManage+getLoanInterest++--");
//						CError tError = new CError();
//						tError.moduleName = "AccountManage";
//						tError.functionName = "getLoanInterest";
//						tError.errorMessage = "计算利息失败1!";
//						mErrors.addOneError(tError);
//						return -1;
//					}
//					aBalance += tInterest;
//				}
//				else if (j==0&&j==intvCount){
//					String startDate_i = PubFun.calDate(startDate, 6*j, "M", null) ;
//					String endDate_i = endDate;
//					double aRate=getLoanRate(PubFun.calDate(startDate_i,-3,"D",null),aRiskCode);
//					if(aRate==-1){
//						// @@错误处理
//						System.out.println("AccountManage+getLoanInterest++--");
//						CError tError = new CError();
//						tError.moduleName = "AccountManage";
//						tError.functionName = "getLoanInterest";
//						tError.errorMessage = "获取利率失败2!";
//						mErrors.addOneError(tError);
//						return -1;
//					}
//					// 计算利息,起始日期算终止日期不算
//					tInterest = getInterest(startDate_i, aBalance,	endDate_i, aRate,aRiskCode);
//					if(tInterest==-1){
////						 @@错误处理
//						System.out.println("AccountManage+getLoanInterest++--");
//						CError tError = new CError();
//						tError.moduleName = "AccountManage";
//						tError.functionName = "getLoanInterest";
//						tError.errorMessage = "计算利息失败2!";
//						mErrors.addOneError(tError);
//						return -1;
//					}
//					aBalance += tInterest;
//				}else if (j==0&&j!=intvCount){
//					String startDate_i = PubFun.calDate(startDate, 6*j, "M", null) ;
//					String endDate_i = PubFun.calDate(startDate, 6*(j+1), "M", null) ;
//					double aRate=getLoanRate(PubFun.calDate(startDate_i,-3,"D",null),aRiskCode);
//					if(aRate==-1){
//						// @@错误处理
//						System.out.println("AccountManage+getLoanInterest++--");
//						CError tError = new CError();
//						tError.moduleName = "AccountManage";
//						tError.functionName = "getLoanInterest";
//						tError.errorMessage = "获取利率失败2!";
//						mErrors.addOneError(tError);
//						return -1;
//					}
//					// 计算利息,起始日期算终止日期不算
//					tInterest = getInterest(startDate_i, aBalance,	endDate_i, aRate,aRiskCode);
//					if(tInterest==-1){
////						 @@错误处理
//						System.out.println("AccountManage+getLoanInterest++--");
//						CError tError = new CError();
//						tError.moduleName = "AccountManage";
//						tError.functionName = "getLoanInterest";
//						tError.errorMessage = "计算利息失败2!";
//						mErrors.addOneError(tError);
//						return -1;
//					}
//					aBalance += tInterest;
//				}
//				else if(j>0&&j!=intvCount){
//					String startDate_i = PubFun.calDate(startDate, 6*j, "M", null) ;
//					String endDate_i = PubFun.calDate(startDate, 6*(j+1), "M", null) ;
//					double aRate=getLoanRate(startDate_i,aRiskCode);
//					if(aRate==-1){
//						// @@错误处理
//						System.out.println("AccountManage+getLoanInterest++--");
//						CError tError = new CError();
//						tError.moduleName = "AccountManage";
//						tError.functionName = "getLoanInterest";
//						tError.errorMessage = "获取利率失败3!";
//						mErrors.addOneError(tError);
//						return -1;
//					}
//					// 计算利息,起始日期算终止日期不算
//					tInterest = getInterest(startDate_i, aBalance,endDate_i, aRate,aRiskCode);
//					if(tInterest==-1){
////						 @@错误处理
//						System.out.println("AccountManage+getLoanInterest++--");
//						CError tError = new CError();
//						tError.moduleName = "AccountManage";
//						tError.functionName = "getLoanInterest";
//						tError.errorMessage = "计算利息失败3!";
//						mErrors.addOneError(tError);
//						return -1;
//					}
//					aBalance += tInterest;
//				}else{
//					// @@错误处理
//					System.out.println("AccountManage+getLoanInterest++--");
//					CError tError = new CError();
//					tError.moduleName = "AccountManage";
//					tError.functionName = "getLoanInterest";
//					tError.errorMessage = "计算失败!";
//					mErrors.addOneError(tError);
//					return -1;
//				}
//			}
//
//		// 最终的利息
//		tInterest = aBalance - oldMoney;
//		return tInterest;
//	}

	/**
	 * 利息计算方法:该方法按6个月分段，分段后交给getInterest处理
	 *
	 * @param startDate
	 *            String 计息开始日期
	 * @param aBalance
	 *            double 本金
	 * @param endDate
	 *            String 止息日期
	 * @param aRate
	 *            double 利率
	 * @return double -1为错误
	 * @author xp 2011/11/15
	 */
	public double getLoanInterest(String startDate, double aBalance,String manageCom,String prem,String endDate,String aRiskCode)
	{
		double oldMoney = aBalance;
		double tInterest = 0.0;
		// 间隔月份,舍弃法
		int intvMonths = PubFun.calInterval(startDate, endDate, "M");
		int intvCount = intvMonths/6;
		if(intvCount<0){
			// @@错误处理
			System.out.println("AccountManage+getLoanInterest++--");
			CError tError = new CError();
			tError.moduleName = "AccountManage";
			tError.functionName = "getLoanInterest";
			tError.errorMessage = "计算起始日期大于终止日期!";
			mErrors.addOneError(tError);
			return -1;
		}
		// 按照半年累计利息
		// 如果借款期限小于6个月,按一次利息计算，如果借款区间大于6个月，每6个月计算一次
		// 同时考虑在某个计息区间内存在多个率的变动，这时按照不同利率分段计算
		for (int j = 0; j <= intvCount; j++) {
			if(j>0&&j==intvCount){
				String startDate_i = PubFun.calDate(startDate, 6*j, "M", null) ;
				String endDate_i = endDate;
				double aRate=getLoanRate(startDate_i,manageCom,prem,aRiskCode);
				if(aRate==-1){
					// @@错误处理
					System.out.println("AccountManage+getLoanInterest++--");
					CError tError = new CError();
					tError.moduleName = "AccountManage";
					tError.functionName = "getLoanInterest";
					tError.errorMessage = "获取利率失败1!";
					mErrors.addOneError(tError);
					return -1;
				}
				// 计算利息,起始日期算终止日期不算
				tInterest = getInterest(startDate_i, aBalance,	endDate_i, aRate,aRiskCode);
				if(tInterest==-1){
//						 @@错误处理
					System.out.println("AccountManage+getLoanInterest++--");
					CError tError = new CError();
					tError.moduleName = "AccountManage";
					tError.functionName = "getLoanInterest";
					tError.errorMessage = "计算利息失败1!";
					mErrors.addOneError(tError);
					return -1;
				}
				aBalance += tInterest;
			}
			else if (j==0&&j==intvCount){
				String startDate_i = PubFun.calDate(startDate, 6*j, "M", null) ;
				String endDate_i = endDate;
				double aRate=getLoanRate(PubFun.calDate(startDate_i,-3,"D",null),manageCom,prem,aRiskCode);
				if(aRate==-1){
					// @@错误处理
					System.out.println("AccountManage+getLoanInterest++--");
					CError tError = new CError();
					tError.moduleName = "AccountManage";
					tError.functionName = "getLoanInterest";
					tError.errorMessage = "获取利率失败2!";
					mErrors.addOneError(tError);
					return -1;
				}
				// 计算利息,起始日期算终止日期不算
				tInterest = getInterest(startDate_i, aBalance,	endDate_i, aRate,aRiskCode);
				if(tInterest==-1){
//						 @@错误处理
					System.out.println("AccountManage+getLoanInterest++--");
					CError tError = new CError();
					tError.moduleName = "AccountManage";
					tError.functionName = "getLoanInterest";
					tError.errorMessage = "计算利息失败2!";
					mErrors.addOneError(tError);
					return -1;
				}
				aBalance += tInterest;
			}else if (j==0&&j!=intvCount){
				String startDate_i = PubFun.calDate(startDate, 6*j, "M", null) ;
				String endDate_i = PubFun.calDate(startDate, 6*(j+1), "M", null) ;
				double aRate=getLoanRate(PubFun.calDate(startDate_i,-3,"D",null),manageCom,prem,aRiskCode);
				if(aRate==-1){
					// @@错误处理
					System.out.println("AccountManage+getLoanInterest++--");
					CError tError = new CError();
					tError.moduleName = "AccountManage";
					tError.functionName = "getLoanInterest";
					tError.errorMessage = "获取利率失败2!";
					mErrors.addOneError(tError);
					return -1;
				}
				// 计算利息,起始日期算终止日期不算
				tInterest = getInterest(startDate_i, aBalance,	endDate_i, aRate,aRiskCode);
				if(tInterest==-1){
//						 @@错误处理
					System.out.println("AccountManage+getLoanInterest++--");
					CError tError = new CError();
					tError.moduleName = "AccountManage";
					tError.functionName = "getLoanInterest";
					tError.errorMessage = "计算利息失败2!";
					mErrors.addOneError(tError);
					return -1;
				}
				aBalance += tInterest;
			}
			else if(j>0&&j!=intvCount){
				String startDate_i = PubFun.calDate(startDate, 6*j, "M", null) ;
				String endDate_i = PubFun.calDate(startDate, 6*(j+1), "M", null) ;
				double aRate=getLoanRate(startDate_i,manageCom,prem,aRiskCode);
				if(aRate==-1){
					// @@错误处理
					System.out.println("AccountManage+getLoanInterest++--");
					CError tError = new CError();
					tError.moduleName = "AccountManage";
					tError.functionName = "getLoanInterest";
					tError.errorMessage = "获取利率失败3!";
					mErrors.addOneError(tError);
					return -1;
				}
				// 计算利息,起始日期算终止日期不算
				tInterest = getInterest(startDate_i, aBalance,endDate_i, aRate,aRiskCode);
				if(tInterest==-1){
//						 @@错误处理
					System.out.println("AccountManage+getLoanInterest++--");
					CError tError = new CError();
					tError.moduleName = "AccountManage";
					tError.functionName = "getLoanInterest";
					tError.errorMessage = "计算利息失败3!";
					mErrors.addOneError(tError);
					return -1;
				}
				aBalance += tInterest;
			}else{
				// @@错误处理
				System.out.println("AccountManage+getLoanInterest++--");
				CError tError = new CError();
				tError.moduleName = "AccountManage";
				tError.functionName = "getLoanInterest";
				tError.errorMessage = "计算失败!";
				mErrors.addOneError(tError);
				return -1;
			}
		}
		
		// 最终的利息
		tInterest = aBalance - oldMoney;
		return tInterest;
	}

	/**
     * 按照保单得到帐户子帐户的余额
     * @param aOtherNo
     * @param aInsuAccNo
     * @param aPolNo
     * @param BalanceDate 结息日期
     * @param aRateType 取得描述帐户利率的时间单位。
     * @param aIntervalType 结息的时间单位。
     * @return
     */
    public double getAccBalance(String aOtherNo, String aInsuAccNo,
                                String aPolNo, String aBalanceDate,
                                String aRateType, String aIntervalType)
    {
        double aAccBalance = 0.0;
        double tAccRate, tCalAccRate;
        int tInterval = 0;
        String tSql = "";
        String iSql = "";
        double tAccInterest = 0.0;

        mLCInsureAccSet.clear();
        mLCInsureAccTraceSet.clear();

        //得到描述表中帐户利率
        LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
        tLMRiskInsuAccDB.setInsuAccNo(aInsuAccNo);
        if (!tLMRiskInsuAccDB.getInfo())
            return aAccBalance;

        switch (Integer.parseInt(tLMRiskInsuAccDB.getAccComputeFlag()))
        {
            //不计息
            case 0:
                aAccBalance = 0.0;
                break;
                //按照固定利率单利生息
            case 1:
                tAccRate = tLMRiskInsuAccDB.getAccRate();
                tCalAccRate = TransAccRate(tAccRate, aRateType, "S",
                                           aIntervalType);
                //tSql = "select * from lcInsureAcc where PolNo='"+aPolNo+"' and InsuAccNo='"+aInsuAccNo+"'";
                LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                tLCInsureAccDB.setPolNo(aPolNo);
                tLCInsureAccDB.setInsuAccNo(aInsuAccNo);
//        tLCInsureAccDB.setOtherNo(aOtherNo);
                mLCInsureAccSet = tLCInsureAccDB.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aPolNo);
                    tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        //得到时间间隔
                        tInterval = PubFun.calInterval(mLCInsureAccSet.get(i).
                                getBalaDate(), aBalanceDate, aIntervalType);
                        double tInterest = mLCInsureAccTraceSet.get(j).getMoney() +
                                           mLCInsureAccTraceSet.get(j).getMoney() *
                                           getIntvRate(tInterval, tCalAccRate,
                                "S");
                        mLCInsureAccTraceSet.get(j).setMoney((double) tInterest);
                        tAccInterest = tAccInterest + tInterest;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }
                break;
                //按照固定利率复利生息
            case 2:
                tAccRate = tLMRiskInsuAccDB.getAccRate();
                tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                           aIntervalType);
                //tSql = "select * from lcInsureAcc where PolNo='"+aPolNo+"' and InsuAccNo='"+aInsuAccNo+"'";
                LCInsureAccDB tLCInsureAccDB2 = new LCInsureAccDB();
                tLCInsureAccDB2.setPolNo(aPolNo);
                tLCInsureAccDB2.setInsuAccNo(aInsuAccNo);
//        tLCInsureAccDB2.setOtherNo(aOtherNo);
                mLCInsureAccSet = tLCInsureAccDB2.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aPolNo);
                    tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        //得到时间间隔
                        tInterval = PubFun.calInterval(mLCInsureAccSet.get(i).
                                getBalaDate(), aBalanceDate, aIntervalType);
                        double tInterest = mLCInsureAccTraceSet.get(j).getMoney() +
                                           mLCInsureAccTraceSet.get(j).getMoney() *
                                           getIntvRate(tInterval, tCalAccRate,
                                "C");
                        mLCInsureAccTraceSet.get(j).setMoney((double) tInterest);
                        tAccInterest = tAccInterest + tInterest;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }

                break;
                //按照利率表单利生息
            case 3:

                //tAccRate = this.getAccRate(tLMRiskInsuAccDB.getSchema());
                //tCalAccRate = TransAccRate(tAccRate,aRateType,"S",aIntervalType);
                //tSql = "select * from lcInsureAcc where PolNo='"+aPolNo+"' and InsuAccNo='"+aInsuAccNo+"'";
                LCInsureAccDB tLCInsureAccDB3 = new LCInsureAccDB();
                tLCInsureAccDB3.setPolNo(aPolNo);
                tLCInsureAccDB3.setInsuAccNo(aInsuAccNo);
//        tLCInsureAccDB3.setOtherNo(aOtherNo);
                mLCInsureAccSet = tLCInsureAccDB3.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aPolNo);
                    tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        double tInterest = 0;
                        //得到分段的单利计算
                        String[] ResultRate = getMultiAccRate(aInsuAccNo,
                                tLMRiskInsuAccDB.getSchema(),
                                              mLCInsureAccSet.get(i).
                                              getBalaDate(), aBalanceDate,
                                              aRateType, "S", aIntervalType);
                        for (int m = 0; m < ResultRate.length; m++)
                        {

                            tCalAccRate = Double.parseDouble(ResultRate[m]);
                            double tSubInterest = mLCInsureAccTraceSet.get(j).
                                                  getMoney() * tCalAccRate;
                            tInterest = tInterest + tSubInterest;

                        }
                        double tBalance = mLCInsureAccTraceSet.get(j).getMoney() +
                                          tInterest;
                        mLCInsureAccTraceSet.get(j).setMoney((double) tBalance);
                        tAccInterest = tAccInterest + tBalance;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }

                break;
                //按照利率表复利生息
            case 4:
                tAccRate = this.getAccRate(tLMRiskInsuAccDB.getSchema());
                tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                           aIntervalType);
                //tSql = "select * from lcInsureAcc where PolNo='"+aPolNo+"' and InsuAccNo='"+aInsuAccNo+"'";
                LCInsureAccDB tLCInsureAccDB4 = new LCInsureAccDB();
                tLCInsureAccDB4.setPolNo(aPolNo);
                tLCInsureAccDB4.setInsuAccNo(aInsuAccNo);
//        tLCInsureAccDB4.setOtherNo(aOtherNo);
                mLCInsureAccSet = tLCInsureAccDB4.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aPolNo);
                    tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        double tInterest = 0;
                        //得到分段的单利计算
                        String[] ResultRate = getMultiAccRate(aInsuAccNo,
                                tLMRiskInsuAccDB.getSchema(),
                                              mLCInsureAccSet.get(i).
                                              getBalaDate(), aBalanceDate,
                                              aRateType, "C", aIntervalType);
                        for (int m = 0; m < ResultRate.length; m++)
                        {
                            tCalAccRate = Double.parseDouble(ResultRate[m]);
                            double tSubInterest = mLCInsureAccTraceSet.get(j).
                                                  getMoney() * tCalAccRate;
                            tInterest = tInterest + tSubInterest;
                        }
                        double tBalance = mLCInsureAccTraceSet.get(j).getMoney() +
                                          tInterest;
                        mLCInsureAccTraceSet.get(j).setMoney((double) tBalance);
                        tAccInterest = tAccInterest + tBalance;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }
                break;
            default:
                aAccBalance = 0.0;
                break;
        }
        aAccBalance = tAccInterest;
        return aAccBalance;
    }


    /**
     * 按照保单得到帐户子帐户的余额
     * @param aOtherNo
     * @param aInsuAccNo
     * @param aPolNo
     * @param BalanceDate 结息日期
     * @param aRateType 取得描述帐户利率的时间单位。
     * @param aIntervalType 结息的时间单位。
     * @return
     */
    public double getAccBalance(LCInsureAccSchema aLCInsureAccSchema,
                                String aBalanceDate, String aRateType,
                                String aIntervalType)
    {
        double aAccBalance = 0.0;
        double tAccRate, tCalAccRate;
        int tInterval = 0;
        String tSql = "";
        String iSql = "";
        double tAccInterest = 0.0;

        mLCInsureAccSet.clear();
        mLCInsureAccTraceSet.clear();

        //得到描述表中帐户利率
        LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
        tLMRiskInsuAccDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        if (!tLMRiskInsuAccDB.getInfo())
            return aAccBalance;

        switch (Integer.parseInt(tLMRiskInsuAccDB.getAccComputeFlag()))
        {
            //不计息
            case 0:
                aAccBalance = 0.0;
                break;
                //按照固定利率单利生息
            case 1:
                tAccRate = tLMRiskInsuAccDB.getAccRate();
                tCalAccRate = TransAccRate(tAccRate, aRateType, "S",
                                           aIntervalType);

                LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                tLCInsureAccDB.setPolNo(aLCInsureAccSchema.getPolNo());
                tLCInsureAccDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
//        tLCInsureAccDB.setOtherNo(aLCInsureAccSchema.getOtherNo());
                mLCInsureAccSet = tLCInsureAccDB.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
                    tLCInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.
                            getInsuAccNo());
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        //得到时间间隔
                        tInterval = PubFun.calInterval(mLCInsureAccSet.get(i).
                                getBalaDate(), aLCInsureAccSchema.getBalaDate(),
                                aIntervalType);
                        double tInterest = mLCInsureAccTraceSet.get(j).getMoney() +
                                           mLCInsureAccTraceSet.get(j).getMoney() *
                                           getIntvRate(tInterval, tCalAccRate,
                                "S");
                        mLCInsureAccTraceSet.get(j).setMoney((double) tInterest);
                        tAccInterest = tAccInterest + tInterest;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }
                break;
                //按照固定利率复利生息
            case 2:
                tAccRate = tLMRiskInsuAccDB.getAccRate();
                tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                           aIntervalType);
                //tSql = "select * from lcInsureAcc where PolNo='"+aPolNo+"' and InsuAccNo='"+aInsuAccNo+"'";
                LCInsureAccDB tLCInsureAccDB2 = new LCInsureAccDB();
                tLCInsureAccDB2.setPolNo(aLCInsureAccSchema.getPolNo());
                tLCInsureAccDB2.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
//        tLCInsureAccDB2.setOtherNo(aLCInsureAccSchema.getOtherNo());
                mLCInsureAccSet = tLCInsureAccDB2.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
                    tLCInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.
                            getInsuAccNo());
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        //得到时间间隔
                        tInterval = PubFun.calInterval(mLCInsureAccSet.get(i).
                                getBalaDate(), aBalanceDate, aIntervalType);
                        double tInterest = mLCInsureAccTraceSet.get(j).getMoney() +
                                           mLCInsureAccTraceSet.get(j).getMoney() *
                                           getIntvRate(tInterval, tCalAccRate,
                                "C");
                        mLCInsureAccTraceSet.get(j).setMoney((double) tInterest);
                        tAccInterest = tAccInterest + tInterest;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }

                break;
                //按照利率表单利生息
            case 3:
                tAccRate = this.getAccRate(tLMRiskInsuAccDB.getSchema());
                tCalAccRate = TransAccRate(tAccRate, aRateType, "S",
                                           aIntervalType);
                //tSql = "select * from lcInsureAcc where PolNo='"+aPolNo+"' and InsuAccNo='"+aInsuAccNo+"'";
                LCInsureAccDB tLCInsureAccDB3 = new LCInsureAccDB();
                tLCInsureAccDB3.setPolNo(aLCInsureAccSchema.getPolNo());
                tLCInsureAccDB3.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
//        tLCInsureAccDB3.setOtherNo(aLCInsureAccSchema.getOtherNo());
                mLCInsureAccSet = tLCInsureAccDB3.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
                    tLCInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.
                            getInsuAccNo());
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        double tInterest = 0;
                        //得到分段的单利计算
                        String[] ResultRate = getMultiAccRate(
                                aLCInsureAccSchema.getInsuAccNo(),
                                tLMRiskInsuAccDB.getSchema(),
                                mLCInsureAccSet.get(i).getBalaDate(),
                                aBalanceDate, aRateType, "S", aIntervalType);
                        for (int m = 0; m < ResultRate.length; m++)
                        {
                            tCalAccRate = Double.parseDouble(ResultRate[m]);
                            double tSubInterest = mLCInsureAccTraceSet.get(j).
                                                  getMoney() * tCalAccRate;
                            tInterest = tInterest + tSubInterest;
                        }
                        double tBalance = mLCInsureAccTraceSet.get(j).getMoney() +
                                          tInterest;
                        mLCInsureAccTraceSet.get(j).setMoney((double) tBalance);
                        tAccInterest = tAccInterest + tBalance;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }
                break;
                //按照利率表复利生息
            case 4:
                tAccRate = this.getAccRate(tLMRiskInsuAccDB.getSchema());
                tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                           aIntervalType);
                //tSql = "select * from lcInsureAcc where PolNo='"+aPolNo+"' and InsuAccNo='"+aInsuAccNo+"'";
                LCInsureAccDB tLCInsureAccDB4 = new LCInsureAccDB();
                tLCInsureAccDB4.setPolNo(aLCInsureAccSchema.getPolNo());
                tLCInsureAccDB4.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
//        tLCInsureAccDB4.setOtherNo(aLCInsureAccSchema.getOtherNo());
                mLCInsureAccSet = tLCInsureAccDB4.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
                    tLCInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.
                            getInsuAccNo());
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        double tInterest = 0;
                        //得到分段的单利计算
                        String[] ResultRate = getMultiAccRate(
                                aLCInsureAccSchema.getInsuAccNo(),
                                tLMRiskInsuAccDB.getSchema(),
                                mLCInsureAccSet.get(i).getBalaDate(),
                                aBalanceDate, aRateType, "C", aIntervalType);
                        for (int m = 0; m < ResultRate.length; m++)
                        {
                            if (!(ResultRate[m] == null ||
                                  ResultRate[m].equals("")))
                            {
                                tCalAccRate = Double.parseDouble(ResultRate[m]);
                                System.out.println("当m的值是" + m + "时，值是" +
                                        tCalAccRate);
                                double tSubInterest = mLCInsureAccTraceSet.get(
                                        j).getMoney() * tCalAccRate;
                                System.out.println("当m的值是" + m + "时，利息是" +
                                        tSubInterest);
                                tInterest = tInterest + tSubInterest;
                            }
                        }
                        double tBalance = mLCInsureAccTraceSet.get(j).getMoney() +
                                          tInterest;
                        mLCInsureAccTraceSet.get(j).setMoney((double) tBalance);
                        tAccInterest = tAccInterest + tBalance;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }
                break;
            default:
                aAccBalance = 0.0;
                break;
        }
        aAccBalance = tAccInterest;
        return aAccBalance;
    }


    /**
     * 按照保单得到某个帐户的余额
     * @param aInsuAccNo
     * @param aPolNo
     * @param BalanceDate 结息日期
     * @param aRateType 取得描述帐户利率的时间单位。
     * @param aIntervalType 结息的时间单位。
     * @return
     */
    public double getAccBalance(String aInsuAccNo, String aPolNo,
                                String aBalanceDate, String aRateType,
                                String aIntervalType)
    {
        double aAccBalance = 0.0;
        double tAccRate, tCalAccRate;
        int tInterval = 0;
        String tSql = "";
        String iSql = "";
        double tAccInterest = 0.0;
        mLCInsureAccSet.clear();
        mLCInsureAccTraceSet.clear();

        //得到描述表中帐户利率
        LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
        tLMRiskInsuAccDB.setInsuAccNo(aInsuAccNo);
        if (!tLMRiskInsuAccDB.getInfo())
            return aAccBalance;

        switch (Integer.parseInt(tLMRiskInsuAccDB.getAccComputeFlag()))
        {
            //不计息
            case 0:
                aAccBalance = 0.0;
                break;
                //按照固定利率单利生息
            case 1:
                tAccRate = tLMRiskInsuAccDB.getAccRate();
                tCalAccRate = TransAccRate(tAccRate, aRateType, "S",
                                           aIntervalType);
                //tSql = "select * from lcInsureAcc where PolNo='"+aPolNo+"' and InsuAccNo='"+aInsuAccNo+"'";
                LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                tLCInsureAccDB.setPolNo(aPolNo);
                tLCInsureAccDB.setInsuAccNo(aInsuAccNo);
                mLCInsureAccSet = tLCInsureAccDB.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aPolNo);
                    tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        //得到时间间隔
                        tInterval = PubFun.calInterval(mLCInsureAccSet.get(i).
                                getBalaDate(), aBalanceDate, aIntervalType);
                        double tInterest = mLCInsureAccTraceSet.get(j).getMoney() +
                                           mLCInsureAccTraceSet.get(j).getMoney() *
                                           getIntvRate(tInterval, tCalAccRate,
                                "S");
                        mLCInsureAccTraceSet.get(j).setMoney((double) tInterest);
                        tAccInterest = tAccInterest + tInterest;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }
                break;
                //按照固定利率复利生息
            case 2:
                tAccRate = tLMRiskInsuAccDB.getAccRate();
                tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                           aIntervalType);
                //tSql = "select * from lcInsureAcc where PolNo='"+aPolNo+"' and InsuAccNo='"+aInsuAccNo+"'";
                LCInsureAccDB tLCInsureAccDB2 = new LCInsureAccDB();
                tLCInsureAccDB2.setPolNo(aPolNo);
                tLCInsureAccDB2.setInsuAccNo(aInsuAccNo);
                mLCInsureAccSet = tLCInsureAccDB2.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aPolNo);
                    tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        //得到时间间隔
                        tInterval = PubFun.calInterval(mLCInsureAccSet.get(i).
                                getBalaDate(), aBalanceDate, aIntervalType);
                        double tInterest = mLCInsureAccTraceSet.get(j).getMoney() +
                                           mLCInsureAccTraceSet.get(j).getMoney() *
                                           getIntvRate(tInterval, tCalAccRate,
                                "C");
                        mLCInsureAccTraceSet.get(j).setMoney((double) tInterest);
                        tAccInterest = tAccInterest + tInterest;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }

                break;
                //按照利率表单利生息
            case 3:
                tAccRate = this.getAccRate(tLMRiskInsuAccDB.getSchema());
                tCalAccRate = TransAccRate(tAccRate, aRateType, "S",
                                           aIntervalType);
                //tSql = "select * from lcInsureAcc where PolNo='"+aPolNo+"' and InsuAccNo='"+aInsuAccNo+"'";
                LCInsureAccDB tLCInsureAccDB3 = new LCInsureAccDB();
                tLCInsureAccDB3.setPolNo(aPolNo);
                tLCInsureAccDB3.setInsuAccNo(aInsuAccNo);
                mLCInsureAccSet = tLCInsureAccDB3.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aPolNo);
                    tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        double tInterest = 0;
                        //得到分段的单利计算
                        String[] ResultRate = getMultiAccRate(aInsuAccNo,
                                tLMRiskInsuAccDB.getSchema(),
                                              mLCInsureAccSet.get(i).
                                              getBalaDate(), aBalanceDate,
                                              aRateType, "S", aIntervalType);
                        for (int m = 0; m < ResultRate.length; m++)
                        {
                            tCalAccRate = Double.parseDouble(ResultRate[m]);
                            double tSubInterest = mLCInsureAccTraceSet.get(j).
                                                  getMoney() * tCalAccRate;
                            tInterest = tInterest + tSubInterest;
                        }
                        double tBalance = mLCInsureAccTraceSet.get(j).getMoney() +
                                          tInterest;
                        mLCInsureAccTraceSet.get(j).setMoney((double) tBalance);
                        tAccInterest = tAccInterest + tBalance;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }

                break;
                //按照利率表复利生息
            case 4:
                tAccRate = this.getAccRate(tLMRiskInsuAccDB.getSchema());
                tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                           aIntervalType);
                //tSql = "select * from lcInsureAcc where PolNo='"+aPolNo+"' and InsuAccNo='"+aInsuAccNo+"'";
                LCInsureAccDB tLCInsureAccDB4 = new LCInsureAccDB();
                tLCInsureAccDB4.setPolNo(aPolNo);
                tLCInsureAccDB4.setInsuAccNo(aInsuAccNo);
                mLCInsureAccSet = tLCInsureAccDB4.query();
                for (int i = 1; i <= mLCInsureAccSet.size(); i++)
                {
                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    tLCInsureAccTraceDB.setPolNo(aPolNo);
                    tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    mLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++)
                    {
                        double tInterest = 0;
                        //得到分段的单利计算
                        String[] ResultRate = getMultiAccRate(aInsuAccNo,
                                tLMRiskInsuAccDB.getSchema(),
                                              mLCInsureAccSet.get(i).
                                              getBalaDate(), aBalanceDate,
                                              aRateType, "C", aIntervalType); //modify by sxy 2004-05-19
                        //String[] ResultRate  = getMultiAccRate(aInsuAccNo,tLMRiskInsuAccDB.getSchema(),mLCInsureAccTraceSet.get(j).getMakeDate(),aBalanceDate,aRateType,"C",aIntervalType);

                        for (int m = 0; m < ResultRate.length; m++)
                        {
                            if (ResultRate[m] != null &&
                                !ResultRate[m].trim().equals(""))
                            {
                                tCalAccRate = Double.parseDouble(ResultRate[m]);
                                double tSubInterest = mLCInsureAccTraceSet.get(
                                        j).getMoney() * tCalAccRate;
                                tInterest = tInterest + tSubInterest;
                            }
                        }
                        double tBalance = mLCInsureAccTraceSet.get(j).getMoney() +
                                          tInterest;
                        mLCInsureAccTraceSet.get(j).setMoney((double) tBalance);
                        tAccInterest = tAccInterest + tBalance;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                }
                break;
            default:
                aAccBalance = 0.0;
                break;
        }
        aAccBalance = tAccInterest;
        return aAccBalance;
    }


    /**
     * 按照保单得到所有帐户的余额
     * @param aInsuAccNo
     * @param aPolNo
     * @param BalanceDate 结息日期
     * @param aRateType 取得描述帐户利率的时间单位。
     * @param aIntervalType 结息的时间单位。
     * @return
     */
    public double getAccBalance(String aPolNo, String aBalanceDate,
                                String aRateType, String aIntervalType)
    {
        double aAccBalance = 0.0;
        double tAccRate, tCalAccRate;
        String aInsuAccNo = "";
        int tInterval = 0;
        String tSql = "";
        String iSql = "";
        double tAccInterest = 0.0;
        mLCInsureAccSet.clear();
        mLCInsureAccTraceSet.clear();

        tSql = "select * from LCInsureAcc where PolNo='" + aPolNo +
               "' order by InsuAccNo";
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        mLCInsureAccSet = tLCInsureAccDB.executeQuery(tSql);

        for (int i = 1; i <= mLCInsureAccSet.size(); i++)
        {
            //得到描述表中帐户利率
            LMRiskInsuAccSchema tLMRiskInsuAccSchema = new LMRiskInsuAccSchema();
            if (i == 1 || mLCInsureAccSet.get(i).getInsuAccNo() != aInsuAccNo)
            {
                LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
                tLMRiskInsuAccDB.setInsuAccNo(mLCInsureAccSet.get(i).
                                              getInsuAccNo());
                if (!tLMRiskInsuAccDB.getInfo())
                    return aAccBalance;
                tLMRiskInsuAccSchema = tLMRiskInsuAccDB.getSchema();
                aInsuAccNo = mLCInsureAccSet.get(i).getInsuAccNo();
            }

            switch (Integer.parseInt(tLMRiskInsuAccSchema.getAccComputeFlag()))
            {
                //不计息
                case 0:
                    aAccBalance = 0.0;
                    break;
                    //按照固定利率单利生息
                case 1:
                    tAccRate = tLMRiskInsuAccSchema.getAccRate();
                    tCalAccRate = TransAccRate(tAccRate, aRateType, "S",
                                               aIntervalType);

                    LCInsureAccTraceDB tLCInsureAccTraceDB = new
                            LCInsureAccTraceDB();
                    LCInsureAccTraceSet tLCInsureAccTraceSet = new
                            LCInsureAccTraceSet();
                    tLCInsureAccTraceDB.setPolNo(aPolNo);
                    tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB.setState("0");
//          tLCInsureAccTraceDB.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                    for (int j = 1; j <= tLCInsureAccTraceSet.size(); j++)
                    {
                        //得到时间间隔
                        tInterval = PubFun.calInterval(mLCInsureAccSet.get(i).
                                getBalaDate(), aBalanceDate, aIntervalType);
                        double tInterest = tLCInsureAccTraceSet.get(j).getMoney() +
                                           tLCInsureAccTraceSet.get(j).getMoney() *
                                           getIntvRate(tInterval, tCalAccRate,
                                "S");
                        tLCInsureAccTraceSet.get(j).setMoney((double) tInterest);
                        mLCInsureAccTraceSet.add(tLCInsureAccTraceSet.get(j));
                        tAccInterest = tAccInterest + tInterest;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                    break;
                    //按照固定利率复利生息
                case 2:
                    tAccRate = tLMRiskInsuAccSchema.getAccRate();
                    tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                               aIntervalType);

                    LCInsureAccTraceDB tLCInsureAccTraceDB2 = new
                            LCInsureAccTraceDB();
                    LCInsureAccTraceSet tLCInsureAccTraceSet2 = new
                            LCInsureAccTraceSet();
                    tLCInsureAccTraceDB2.setPolNo(aPolNo);
                    tLCInsureAccTraceDB2.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB2.setState("0");
//          tLCInsureAccTraceDB2.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    tLCInsureAccTraceSet2 = tLCInsureAccTraceDB2.query();
                    for (int j = 1; j <= tLCInsureAccTraceSet2.size(); j++)
                    {
                        //得到时间间隔
                        tInterval = PubFun.calInterval(mLCInsureAccSet.get(i).
                                getBalaDate(), aBalanceDate, aIntervalType);
                        double tInterest = tLCInsureAccTraceSet2.get(j).
                                           getMoney() +
                                           tLCInsureAccTraceSet2.get(j).
                                           getMoney() *
                                           getIntvRate(tInterval, tCalAccRate,
                                "C");
                        tLCInsureAccTraceSet2.get(j).setMoney((double)
                                tInterest);
                        mLCInsureAccTraceSet.add(tLCInsureAccTraceSet2.get(j));
                        tAccInterest = tAccInterest + tInterest;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());

                    break;
                    //按照利率表单利生息
                case 3:
                    tAccRate = this.getAccRate(tLMRiskInsuAccSchema.getSchema());
                    tCalAccRate = TransAccRate(tAccRate, aRateType, "S",
                                               aIntervalType);

                    LCInsureAccTraceDB tLCInsureAccTraceDB3 = new
                            LCInsureAccTraceDB();
                    LCInsureAccTraceSet tLCInsureAccTraceSet3 = new
                            LCInsureAccTraceSet();
                    tLCInsureAccTraceDB3.setPolNo(aPolNo);
                    tLCInsureAccTraceDB3.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB3.setState("0");
//          tLCInsureAccTraceDB3.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    tLCInsureAccTraceSet3 = tLCInsureAccTraceDB3.query();
                    for (int j = 1; j <= tLCInsureAccTraceSet3.size(); j++)
                    {
                        double tInterest = 0;
                        //得到分段的单利计算
                        String[] ResultRate = getMultiAccRate(aInsuAccNo,
                                tLMRiskInsuAccSchema.getSchema(),
                                mLCInsureAccSet.get(i).getBalaDate(),
                                aBalanceDate, aRateType, "S", aIntervalType);
                        for (int m = 0; m < ResultRate.length; m++)
                        {
                            tCalAccRate = Double.parseDouble(ResultRate[m]);
                            double tSubInterest = tLCInsureAccTraceSet3.get(j).
                                                  getMoney() * tCalAccRate;
                            tInterest = tInterest + tSubInterest;
                        }
                        double tBalance = tLCInsureAccTraceSet3.get(j).getMoney() +
                                          tInterest;
                        mLCInsureAccTraceSet.get(j).setMoney((double) tBalance);
                        tAccInterest = tAccInterest + tBalance;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());

                    break;
                    //按照利率表复利生息
                case 4:
                    tAccRate = this.getAccRate(tLMRiskInsuAccSchema.getSchema());
                    tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                               aIntervalType);

                    LCInsureAccTraceDB tLCInsureAccTraceDB4 = new
                            LCInsureAccTraceDB();
                    LCInsureAccTraceSet tLCInsureAccTraceSet4 = new
                            LCInsureAccTraceSet();

                    tLCInsureAccTraceDB4.setPolNo(aPolNo);
                    tLCInsureAccTraceDB4.setInsuAccNo(aInsuAccNo);
                    tLCInsureAccTraceDB4.setState("0");
//          tLCInsureAccTraceDB4.setOtherNo(mLCInsureAccSet.get(i).getOtherNo());
                    tLCInsureAccTraceSet4 = tLCInsureAccTraceDB4.query();
                    for (int j = 1; j <= tLCInsureAccTraceSet4.size(); j++)
                    {
                        double tInterest = 0;
                        //得到分段的复利计算
                        String[] ResultRate = getMultiAccRate(aInsuAccNo,
                                tLMRiskInsuAccSchema.getSchema(),
                                mLCInsureAccSet.get(i).getBalaDate(),
                                aBalanceDate, aRateType, "C", aIntervalType);
                        for (int m = 0; m < ResultRate.length; m++)
                        {
                            tCalAccRate = Double.parseDouble(ResultRate[m]);
                            double tSubInterest = tLCInsureAccTraceSet4.get(j).
                                                  getMoney() * tCalAccRate;
                            tInterest = tInterest + tSubInterest;
                        }
                        double tBalance = tLCInsureAccTraceSet4.get(j).getMoney() +
                                          tInterest;
                        mLCInsureAccTraceSet.get(j).setMoney((double) tBalance);
                        tAccInterest = tAccInterest + tBalance;
                    }
                    mLCInsureAccSet.get(i).setInsuAccBala((double) tAccInterest);
                    mLCInsureAccSet.get(i).setBalaDate(aBalanceDate);
                    mLCInsureAccSet.get(i).setBalaTime(PubFun.getCurrentTime());
                    break;
                default:
                    aAccBalance = 0.0;
                    break;
            }
            aAccBalance = aAccBalance + tAccInterest;
        }

        return aAccBalance;
    }

    /**
     * 计算账户收益
     * @param cTransferData，包含：
     A.	GlobalInput对象，完整的登陆用户信息，参数名GI
     B.	一个子账户信息，参数名LCInsureAccClass
     C.	开始日期，参数名StartDate，结算周期起始的第一日
     D.	结束日期，参数名EndDate，结算周期末的次日
     E.	结算类型，参数名BalaType，1：月结算，2：保证收益结算
     F.	保单年度，参数名PolYear，月结算可不传
     G.	保单月度，参数名PolMonth，月结算可不传
     * @return
     */
    public MMap getBalaAccInterest(TransferData cTransferData)
    {
        LCInsureAccClassSchema tAccClassSchema
            = (LCInsureAccClassSchema)cTransferData.getValueByName("LCInsureAccClass");
        GlobalInput tGI = (GlobalInput)cTransferData.getValueByName("GI");
        String tStartDate = (String)cTransferData.getValueByName("StartDate");
        String tEndDate = (String)cTransferData.getValueByName("EndDate");
        String tBalaType = (String)cTransferData.getValueByName("BalaType");
        String tLastBalaStr = (String)cTransferData.getValueByName("LastBala");
        String tLastBalaGuratStr = (String)cTransferData.getValueByName("LastBalaGurat");
        
        /**
         * 20150715新万能产品账户在保单缓交区间使用保证利率结算且不收取风险保费
         * 使用新的方法结算
         * 月结中保证账户价值的结算按原来正常处理
         */
        if(hasULIDefer(tAccClassSchema,tStartDate,tEndDate) && !"2".equals(tBalaType)){
        	//引用类型的对象，避免临时数据对后边计算管理费有影响，所以这里copy一份
        	TransferData tempLXTransferData=copyData(cTransferData);
        	MMap pMap= getBalaAccInterestNew(tempLXTransferData);
        	return pMap;
        }
        
    	/**
         * 20160926附加万能产品账户在保单失效区间不结息
         * 使用新的方法结算
         */
        if(AccountManage.hasSUBULI(tAccClassSchema,tStartDate,tEndDate)){
        	TransferData tempLXTransferData=copyData(cTransferData);
        	MMap pMap= getBalaAccInterestSub(tempLXTransferData);
        	return pMap;
        }

        String oldMoneyStr = "1".equals(tBalaType) ? tLastBalaStr : tLastBalaGuratStr;
        if(oldMoneyStr == null || oldMoneyStr.equals(""))
        {
            mErrors.addOneError("请传入上一次结算金额");
            return null;
        }
        double oldMoney = Double.parseDouble(oldMoneyStr);

        //计算LastBala产生的计算金额
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setRiskCode((String)cTransferData.getValueByName("RiskCode"));
        tLMCalModeDB.setType("J");
        LMCalModeSet set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }

        Calculator tCalculator = createCalculator(cTransferData, tAccClassSchema);
        if(tCalculator == null)
        {
            return null;
        }

        tCalculator.setCalCode(set.get(1).getCalCode());
        String money = tCalculator.calculate();
        if(tCalculator.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tCalculator.mErrors);
            return null;
        }
        double resultMoney = Double.parseDouble(money);

        //计算后轨迹本金及利息
        tLMCalModeDB.setType("K");
        set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }
        tCalculator.setCalCode(set.get(1).getCalCode());

        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
        String sql = "select * from LCInsureAccTrace "
                     + "where PayDate >= '" + tStartDate + "' "
                     + "   and paydate < '" + tEndDate + "' "
                     + "   and PolNo = '" + tAccClassSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + tAccClassSchema.getInsuAccNo() + "' "
                     + "   and ((PayPlanCode = '"
                     + tAccClassSchema.getPayPlanCode() + "') or (PayPlanCode in ('231201','231001','231801')))"
                     + "   and ((OtherType not in('1', '6')) or (OtherType='6' and moneytype='B')) with ur";  //契约轨迹和月结轨迹不计算利息
        System.out.println("查询sql==="+sql);
        tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(sql);

        for(int i = 1; i <= tLCInsureAccTraceSet.size(); i++)
        {
            tCalculator = createCalculator(cTransferData, tAccClassSchema);
            tCalculator.setCalCode(set.get(1).getCalCode());
            tCalculator.addBasicFactor("TraceStartDate",
                                       tLCInsureAccTraceSet.get(i).getPayDate()); //轨迹计算区间为产生轨迹日期到EndDate
            tCalculator.addBasicFactor("TraceMoney",
                                       "" + tLCInsureAccTraceSet.get(i).getMoney());

            oldMoney += tLCInsureAccTraceSet.get(i).getMoney();
            money = tCalculator.calculate();
            if(tCalculator.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tCalculator.mErrors);
                return null;
            }
            resultMoney += Double.parseDouble(money);
        }
        resultMoney = PubFun.setPrecision(resultMoney, "0.00");
        double tLX = resultMoney - oldMoney;  //收益

        System.out.println("上月价值及本周期轨迹计算后总价值(未扣除相关费用):" + resultMoney);

        //更新轨迹信息
        LCInsureAccClassSchema tAccClassSchemaDelt = tAccClassSchema.getSchema();
        tAccClassSchemaDelt.setBalaDate(tEndDate);
        tAccClassSchemaDelt.setInsuAccBala(resultMoney);
        tAccClassSchemaDelt.setLastAccBala(tAccClassSchemaDelt.getInsuAccBala());
        tAccClassSchemaDelt.setOperator(tGI.Operator);
        tAccClassSchemaDelt.setModifyDate(PubFun.getCurrentDate());
        tAccClassSchemaDelt.setModifyTime(PubFun.getCurrentTime());

        Reflections ref = new Reflections();
        LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
        ref.transFields(traceSchema, tAccClassSchema);

        String tLimit = PubFun.getNoLimit(tAccClassSchemaDelt.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        traceSchema.setSerialNo(serNo);
        traceSchema.setMoneyType("LX");
        traceSchema.setMoney(PubFun.setPrecision(tLX, "0.00"));
        traceSchema.setPayDate(tEndDate);
        PubFun.fillDefaultField(traceSchema);

        MMap tMMap = new MMap();
        tMMap.put(tAccClassSchemaDelt, SysConst.UPDATE);
        tMMap.put(traceSchema, SysConst.INSERT);

        return tMMap;
    }
    public MMap getBalaAccInterestP(TransferData cTransferData)
    {
        LPInsureAccClassSchema tPAccClassSchema
            = (LPInsureAccClassSchema)cTransferData.getValueByName("LPInsureAccClass");
        GlobalInput tGI = (GlobalInput)cTransferData.getValueByName("GI");
        String tStartDate = (String)cTransferData.getValueByName("StartDate");
        String tEndDate = (String)cTransferData.getValueByName("EndDate");
        String tBalaType = (String)cTransferData.getValueByName("BalaType");
        String tLastBalaStr = (String)cTransferData.getValueByName("LastBala");
        String tLastBalaGuratStr = (String)cTransferData.getValueByName("LastBalaGurat");
        
        Reflections ref1 = new Reflections();
        LCInsureAccClassSchema tempAccSchema=new LCInsureAccClassSchema();
        ref1.transFields(tempAccSchema, tPAccClassSchema);
    	/**
         * 20160926附加万能产品账户在保单失效区间不结息
         * 使用新的方法结算
         */
        if(AccountManage.hasSUBULI(tempAccSchema,tStartDate,tEndDate)){
        	MMap pMap= getBalaAccInterestPSub(cTransferData);
        	return pMap;
        }

        String oldMoneyStr = "1".equals(tBalaType) ? tLastBalaStr : tLastBalaGuratStr;
        if(oldMoneyStr == null || oldMoneyStr.equals(""))
        {
            mErrors.addOneError("请传入上一次结算金额");
            return null;
        }
        double oldMoney = Double.parseDouble(oldMoneyStr);
        double resultMoney=0.0;
        //计算LastBala产生的计算金额
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setRiskCode((String)cTransferData.getValueByName("RiskCode"));
        tLMCalModeDB.setType("J");
        LMCalModeSet set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }

        Calculator tCalculator = createCalculatorP(cTransferData, tPAccClassSchema);
        if(tCalculator == null)
        {
            return null;
        }

        tCalculator.setCalCode(set.get(1).getCalCode());
        String money = tCalculator.calculate();
        if(tCalculator.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tCalculator.mErrors);
            return null;
        }
        resultMoney += Double.parseDouble(money);

        //计算后轨迹本金及利息
        tLMCalModeDB.setType("K");
        set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }
        tCalculator.setCalCode(set.get(1).getCalCode());

        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
        String sql = "select * from LCInsureAccTrace "
                     + "where PayDate >= '" + tStartDate + "' "
                     + "   and paydate < '" + tEndDate + "' "
                     + "   and PolNo = '" + tPAccClassSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + tPAccClassSchema.getInsuAccNo() + "' "
                     + "   and ((PayPlanCode = '"
                     + tPAccClassSchema.getPayPlanCode() + "') or (PayPlanCode in ('231201','231001','231801'))) "
                     + "   and ((OtherType not in('1', '6')) or (OtherType='6' and moneytype='B')) with ur ";  //契约轨迹和月结轨迹不计算利息
        tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(sql);

        for(int i = 1; i <= tLCInsureAccTraceSet.size(); i++)
        {
            tCalculator = createCalculatorP(cTransferData, tPAccClassSchema);
            tCalculator.setCalCode(set.get(1).getCalCode());
            tCalculator.addBasicFactor("TraceStartDate",
                                       tLCInsureAccTraceSet.get(i).getPayDate()); //轨迹计算区间为产生轨迹日期到EndDate
            tCalculator.addBasicFactor("TraceMoney",
                                       "" + tLCInsureAccTraceSet.get(i).getMoney());

            oldMoney += tLCInsureAccTraceSet.get(i).getMoney();
            money = tCalculator.calculate();
            if(tCalculator.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tCalculator.mErrors);
                return null;
            }
            resultMoney += Double.parseDouble(money);
        }
        resultMoney = PubFun.setPrecision(resultMoney, "0.00");
        double tLX = resultMoney - oldMoney;  //收益

        System.out.println("上月价值及本周期轨迹计算后总价值(未扣除相关费用):" + resultMoney);

        //更新轨迹信息
        LPInsureAccClassSchema tPAccClassSchemaDelt = tPAccClassSchema.getSchema();
        tPAccClassSchemaDelt.setBalaDate(tEndDate);
        tPAccClassSchemaDelt.setInsuAccBala(resultMoney);
        tPAccClassSchemaDelt.setLastAccBala(tPAccClassSchemaDelt.getInsuAccBala());
        tPAccClassSchemaDelt.setOperator(tGI.Operator);
        tPAccClassSchemaDelt.setModifyDate(PubFun.getCurrentDate());
        tPAccClassSchemaDelt.setModifyTime(PubFun.getCurrentTime());

        Reflections ref = new Reflections();
        LPInsureAccTraceSchema traceSchemaP = new LPInsureAccTraceSchema();
        ref.transFields(traceSchemaP, tPAccClassSchema);

        String tLimit = PubFun.getNoLimit(tPAccClassSchemaDelt.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        traceSchemaP.setSerialNo(serNo);
        traceSchemaP.setMoneyType("LX");
        traceSchemaP.setMoney(PubFun.setPrecision(tLX, "0.00"));
        traceSchemaP.setPayDate(tEndDate);
        PubFun.fillDefaultField(traceSchemaP);

        MMap tMMap = new MMap();
        tMMap.put(tPAccClassSchemaDelt, SysConst.INSERT); //这里需要插入轨迹到P表。
        tMMap.put(traceSchemaP, SysConst.INSERT);

        return tMMap;
    }
    
    /**
     * add by lzy 20150715
     * 含有缓交区间的账户收益计算
     * 如账户结算期间内有缓交记录，则对缓交区间内使用保证利率结算，不收取风险保费
     * 缓交区间外正常结算
     * @return
     */
    public MMap getBalaAccInterestNew(TransferData cTransferData)
    {
    	//新万能产品账户在保单缓交区间使用保证利率结算且不收取风险保费
        LCInsureAccClassSchema tAccClassSchema
            = (LCInsureAccClassSchema)cTransferData.getValueByName("LCInsureAccClass");
        GlobalInput tGI = (GlobalInput)cTransferData.getValueByName("GI");
        String tStartDate = (String)cTransferData.getValueByName("StartDate");
        String tEndDate = (String)cTransferData.getValueByName("EndDate");
        String tBalaType = (String)cTransferData.getValueByName("BalaType");
        String tLastBalaStr = (String)cTransferData.getValueByName("LastBala");
        String tLastBalaGuratStr = (String)cTransferData.getValueByName("LastBalaGurat");
        
        String oldMoneyStr = "1".equals(tBalaType) ? tLastBalaStr : tLastBalaGuratStr;
        if(oldMoneyStr == null || oldMoneyStr.equals(""))
        {
            mErrors.addOneError("请传入上一次结算金额");
            return null;
        }
        double oldMoney = Double.parseDouble(oldMoneyStr);
        double tLX=0;
        //获取结算区间
        LMInsuAccRateSet tLMInsuAccRateSet=getBalanceRate(tAccClassSchema,tStartDate,tEndDate,tBalaType);
        if(tLMInsuAccRateSet.size()==0){
        	mErrors.addOneError("账户结算区间拆分失败！");
            return null;
        }

        //计算LastBala产生的计算金额的算法编码
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setRiskCode((String)cTransferData.getValueByName("RiskCode"));
        tLMCalModeDB.setType("J");
        LMCalModeSet set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }
        String accCalCode=set.get(1).getCalCode();
        
        //计算后轨迹本金及利息的算法编码
        tLMCalModeDB.setType("K");
        set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }
        String lxCalCode=set.get(1).getCalCode();
        
        double resultMoney=0;
        System.out.println("本次结算区间共拆分了"+tLMInsuAccRateSet.size()+"段");
        for(int j=1;j<=tLMInsuAccRateSet.size();j++){
        	LMInsuAccRateSchema tRate=tLMInsuAccRateSet.get(j);
        	cTransferData.removeByName("StartDate");
        	cTransferData.removeByName("EndDate");
        	cTransferData.removeByName("RateType");
        	cTransferData.setNameAndValue("StartDate", tRate.getStartBalaDate());
        	cTransferData.setNameAndValue("EndDate", tRate.getBalaDate());
        	cTransferData.setNameAndValue("RateType", tRate.getRateType());
//        	tStartDate=(String)cTransferData.getValueByName("StartDate");
//        	tEndDate=(String)cTransferData.getValueByName("EndDate");;
        	
	        Calculator tCalculator = createCalculator(cTransferData, tAccClassSchema);
	        if(tCalculator == null)
	        {
	            return null;
	        }
	
//	        tCalculator.setCalCode(set.get(1).getCalCode());
	        tCalculator.setCalCode(accCalCode);
	        String money = tCalculator.calculate();
	        if(tCalculator.mErrors.needDealError())
	        {
	            mErrors.copyAllErrors(tCalculator.mErrors);
	            return null;
	        }
	        resultMoney = Double.parseDouble(money);
	        tLX +=resultMoney-oldMoney;
	        System.out.println("第"+j+"段：起始日期："+tRate.getStartBalaDate()+",截止日："+tRate.getBalaDate()
	        		+",结算类型："+tRate.getRateType()+",结算金额："+resultMoney);
	        
	        
	        tCalculator.setCalCode(lxCalCode);
	
	        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
	        LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
	        String sql = "select * from LCInsureAccTrace "
	                     + "where PayDate >= '" + tStartDate + "' "
	                     + "   and paydate < '" + tEndDate + "' "
	                     + "   and PolNo = '" + tAccClassSchema.getPolNo() + "' "
	                     + "   and InsuAccNo = '"
	                     + tAccClassSchema.getInsuAccNo() + "' "
	                     + "   and ((PayPlanCode = '"
	                     + tAccClassSchema.getPayPlanCode() + "') or (PayPlanCode in ('231201','231001','231801')))"
	                     + "   and ((OtherType not in('1', '6')) or (OtherType='6' and moneytype='B')) with ur";  //契约轨迹和月结轨迹不计算利息
	        System.out.println("查询sql==="+sql);
	        tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(sql);
	        FDate tFDate=new FDate();
	        for(int i = 1; i <= tLCInsureAccTraceSet.size(); i++)
	        {
	            tCalculator = createCalculator(cTransferData, tAccClassSchema);
	            tCalculator.setCalCode(set.get(1).getCalCode());
	            /* 迹的金额也要分区间算，起始日用轨迹生成日和区间起始日较晚的一个
	             * 若轨迹的发生时间早于本次结算区间的不进行结算。
	             */
	            String traceStartDate=tLCInsureAccTraceSet.get(i).getPayDate();
	            if(tFDate.getDate(traceStartDate).compareTo(tFDate.getDate(tRate.getBalaDate()))<0){
		            if(tFDate.getDate(traceStartDate).compareTo(tFDate.getDate(tRate.getStartBalaDate()))<=0){
		            	traceStartDate=tRate.getStartBalaDate();
		            }
	            }else{
	            	continue;
	            }
	            
	            
	            tCalculator.addBasicFactor("TraceStartDate",traceStartDate); //轨迹计算区间为产生轨迹日期到EndDate
	            tCalculator.addBasicFactor("TraceMoney",
	                                       "" + tLCInsureAccTraceSet.get(i).getMoney());
	            if(j==tLMInsuAccRateSet.size()){
	            	oldMoney += tLCInsureAccTraceSet.get(i).getMoney();
	            }
	            money = tCalculator.calculate();
	            if(tCalculator.mErrors.needDealError())
	            {
	                mErrors.copyAllErrors(tCalculator.mErrors);
	                return null;
	            }
	            tLX +=Double.parseDouble(money)-tLCInsureAccTraceSet.get(i).getMoney();
	            System.out.println("tracemoney="+tLCInsureAccTraceSet.get(i).getMoney()+",paydate="+traceStartDate+",结算金额="+money+",LX="+tLX);
	        }
        }
        resultMoney = PubFun.setPrecision(oldMoney+tLX, "0.00");
//        double tLX = resultMoney - oldMoney;  //收益

        System.out.println("上月价值及本周期轨迹计算后利息共计：" + tLX+",本息共计："+resultMoney);

        //更新轨迹信息
        LCInsureAccClassSchema tAccClassSchemaDelt = tAccClassSchema.getSchema();
        tAccClassSchemaDelt.setBalaDate(tEndDate);
        tAccClassSchemaDelt.setInsuAccBala(resultMoney);
        tAccClassSchemaDelt.setLastAccBala(tAccClassSchemaDelt.getInsuAccBala());
        tAccClassSchemaDelt.setOperator(tGI.Operator);
        tAccClassSchemaDelt.setModifyDate(PubFun.getCurrentDate());
        tAccClassSchemaDelt.setModifyTime(PubFun.getCurrentTime());

        Reflections ref = new Reflections();
        LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
        ref.transFields(traceSchema, tAccClassSchema);

        String tLimit = PubFun.getNoLimit(tAccClassSchemaDelt.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        traceSchema.setSerialNo(serNo);
        traceSchema.setMoneyType("LX");
        traceSchema.setMoney(PubFun.setPrecision(tLX, "0.00"));
        traceSchema.setPayDate(tEndDate);
        PubFun.fillDefaultField(traceSchema);

        MMap tMMap = new MMap();
        tMMap.put(tAccClassSchemaDelt, SysConst.UPDATE);
        tMMap.put(traceSchema, SysConst.INSERT);

        return tMMap;
    }
    
    /**
     * add by hhw 20160928
     * 有失效记录的附加万能账户收益计算
     * 如账户结算期间内有失效记录，则失效区间内不计息
     * 失效区间外正常结算
     * @return
     */
    public MMap getBalaAccInterestSub(TransferData cTransferData)
    {

        LCInsureAccClassSchema tAccClassSchema
            = (LCInsureAccClassSchema)cTransferData.getValueByName("LCInsureAccClass");
        GlobalInput tGI = (GlobalInput)cTransferData.getValueByName("GI");
        String tStartDate = (String)cTransferData.getValueByName("StartDate");
        String tEndDate = (String)cTransferData.getValueByName("EndDate");
        String tBalaType = (String)cTransferData.getValueByName("BalaType");
        String tLastBalaStr = (String)cTransferData.getValueByName("LastBala");
        String tLastBalaGuratStr = (String)cTransferData.getValueByName("LastBalaGurat");
        
        String oldMoneyStr = "1".equals(tBalaType) ? tLastBalaStr : tLastBalaGuratStr;
        if(oldMoneyStr == null || oldMoneyStr.equals(""))
        {
            mErrors.addOneError("请传入上一次结算金额");
            return null;
        }
        double oldMoney = Double.parseDouble(oldMoneyStr);
        double tLX=0;
        //获取结算区间
        LMInsuAccRateSet tLMInsuAccRateSet=getBalanceRate1(tAccClassSchema,tStartDate,tEndDate,tBalaType);
        if(tLMInsuAccRateSet.size()==0){
        	mErrors.addOneError("账户结算区间拆分失败！");
            return null;
        }

        //计算LastBala产生的计算金额的算法编码
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setRiskCode((String)cTransferData.getValueByName("RiskCode"));
        tLMCalModeDB.setType("J");
        LMCalModeSet set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }
        String accCalCode=set.get(1).getCalCode();
        
        //计算后轨迹本金及利息的算法编码
        tLMCalModeDB.setType("K");
        set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }
        String lxCalCode=set.get(1).getCalCode();
        
        double resultMoney=0;
        System.out.println("本次结算区间共拆分了"+tLMInsuAccRateSet.size()+"段");
        for(int j=1;j<=tLMInsuAccRateSet.size();j++){
			LMInsuAccRateSchema tRate = tLMInsuAccRateSet.get(j);

			cTransferData.removeByName("StartDate");
			cTransferData.removeByName("EndDate");
			cTransferData.removeByName("RateType");
			cTransferData
					.setNameAndValue("StartDate", tRate.getStartBalaDate());
			cTransferData.setNameAndValue("EndDate", tRate.getBalaDate());
			cTransferData.setNameAndValue("RateType", tRate.getRateType());
			// tStartDate=(String)cTransferData.getValueByName("StartDate");
			// tEndDate=(String)cTransferData.getValueByName("EndDate");;

			Calculator tCalculator = createCalculator(cTransferData,
					tAccClassSchema);
			if (tCalculator == null) {
				return null;
			}

			// tCalculator.setCalCode(set.get(1).getCalCode());
			tCalculator.setCalCode(accCalCode);
			String money = tCalculator.calculate();
			if (tCalculator.mErrors.needDealError()) {
				mErrors.copyAllErrors(tCalculator.mErrors);
				return null;
			}
			resultMoney = Double.parseDouble(money);
			tLX += resultMoney - oldMoney;
			System.out.println("第" + j + "段：起始日期：" + tRate.getStartBalaDate()
					+ ",截止日：" + tRate.getBalaDate() + ",结算类型："
					+ tRate.getRateType() + ",结算金额：" + resultMoney);

			tCalculator.setCalCode(lxCalCode);

			LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
			LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
			String sql = "select * from LCInsureAccTrace "
					+ "where PayDate >= '"+ tStartDate+ "' "
					+ "   and paydate < '"+ tEndDate+ "' "
					+ "   and PolNo = '"+ tAccClassSchema.getPolNo()+ "' "
					+ "   and InsuAccNo = '"+ tAccClassSchema.getInsuAccNo()+ "' "
					+ "   and ((PayPlanCode = '"+ tAccClassSchema.getPayPlanCode()
					+ "') or (PayPlanCode in ('231201','231001','231801')))"
					+ "   and ((OtherType not in('1', '6')) or (OtherType='6' and moneytype='B')) with ur"; // 契约轨迹和月结轨迹不计算利息
			System.out.println("查询sql===" + sql);
			tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(sql);
			FDate tFDate = new FDate();
			for (int i = 1; i <= tLCInsureAccTraceSet.size(); i++) {
				tCalculator = createCalculator(cTransferData, tAccClassSchema);
				tCalculator.setCalCode(set.get(1).getCalCode());
				/*
				 * 迹的金额也要分区间算，起始日用轨迹生成日和区间起始日较晚的一个 若轨迹的发生时间早于本次结算区间的不进行结算。
				 */
				String traceStartDate = tLCInsureAccTraceSet.get(i)
						.getPayDate();
				if (tFDate.getDate(traceStartDate).compareTo(
						tFDate.getDate(tRate.getBalaDate())) < 0) {
					if (tFDate.getDate(traceStartDate).compareTo(
							tFDate.getDate(tRate.getStartBalaDate())) <= 0) {
						traceStartDate = tRate.getStartBalaDate();
					}
				} else {
					continue;
				}

				tCalculator.addBasicFactor("TraceStartDate", traceStartDate); // 轨迹计算区间为产生轨迹日期到EndDate
				tCalculator.addBasicFactor("TraceMoney", ""
						+ tLCInsureAccTraceSet.get(i).getMoney());
				if (j == tLMInsuAccRateSet.size()) {
					oldMoney += tLCInsureAccTraceSet.get(i).getMoney();
				}
				money = tCalculator.calculate();
				if (tCalculator.mErrors.needDealError()) {
					mErrors.copyAllErrors(tCalculator.mErrors);
					return null;
				}
				tLX += Double.parseDouble(money)
						- tLCInsureAccTraceSet.get(i).getMoney();
				System.out.println("tracemoney="
						+ tLCInsureAccTraceSet.get(i).getMoney() + ",paydate="
						+ traceStartDate + ",结算金额=" + money + ",LX=" + tLX);
			}

        }
        resultMoney = PubFun.setPrecision(oldMoney+tLX, "0.00");
//        double tLX = resultMoney - oldMoney;  //收益

        System.out.println("上月价值及本周期轨迹计算后利息共计：" + tLX+",本息共计："+resultMoney);

        //更新轨迹信息
        LCInsureAccClassSchema tAccClassSchemaDelt = tAccClassSchema.getSchema();
        tAccClassSchemaDelt.setBalaDate(tEndDate);
        tAccClassSchemaDelt.setInsuAccBala(resultMoney);
        tAccClassSchemaDelt.setLastAccBala(tAccClassSchemaDelt.getInsuAccBala());
        tAccClassSchemaDelt.setOperator(tGI.Operator);
        tAccClassSchemaDelt.setModifyDate(PubFun.getCurrentDate());
        tAccClassSchemaDelt.setModifyTime(PubFun.getCurrentTime());

        Reflections ref = new Reflections();
        LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
        ref.transFields(traceSchema, tAccClassSchema);

        String tLimit = PubFun.getNoLimit(tAccClassSchemaDelt.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        traceSchema.setSerialNo(serNo);
        traceSchema.setMoneyType("LX");
        traceSchema.setMoney(PubFun.setPrecision(tLX, "0.00"));
        traceSchema.setPayDate(tEndDate);
        PubFun.fillDefaultField(traceSchema);

        MMap tMMap = new MMap();
        tMMap.put(tAccClassSchemaDelt, SysConst.UPDATE);
        tMMap.put(traceSchema, SysConst.INSERT);

        return tMMap;
    }
    
    public MMap getBalaAccInterestPSub(TransferData cTransferData)
    {
        LPInsureAccClassSchema tPAccClassSchema
            = (LPInsureAccClassSchema)cTransferData.getValueByName("LPInsureAccClass");
        GlobalInput tGI = (GlobalInput)cTransferData.getValueByName("GI");
        String tStartDate = (String)cTransferData.getValueByName("StartDate");
        String tEndDate = (String)cTransferData.getValueByName("EndDate");
        String tBalaType = (String)cTransferData.getValueByName("BalaType");
        String tRateType = (String)cTransferData.getValueByName("RateType");
        String tLastBalaStr = (String)cTransferData.getValueByName("LastBala");
        String tLastBalaGuratStr = (String)cTransferData.getValueByName("LastBalaGurat");
        
        Reflections ref1 = new Reflections();
        LCInsureAccClassSchema tempAccSchema=new LCInsureAccClassSchema();
        ref1.transFields(tempAccSchema, tPAccClassSchema);

        //获取结算区间
        LMInsuAccRateSet tLMInsuAccRateSet=getBalanceRate1(tempAccSchema,tStartDate,tEndDate,tRateType);
        if(tLMInsuAccRateSet.size()==0){
        	mErrors.addOneError("账户结算区间拆分失败！");
            return null;
        }
        
        String oldMoneyStr = "1".equals(tBalaType) ? tLastBalaStr : tLastBalaGuratStr;
        if(oldMoneyStr == null || oldMoneyStr.equals(""))
        {
            mErrors.addOneError("请传入上一次结算金额");
            return null;
        }
        

        double oldMoney = Double.parseDouble(oldMoneyStr);
        double resultMoney=0.0;
        double tLX=0;
        //计算LastBala产生的计算金额
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setRiskCode((String)cTransferData.getValueByName("RiskCode"));
        tLMCalModeDB.setType("J");
        LMCalModeSet set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }
        String accCalCode=set.get(1).getCalCode();


        //计算后轨迹本金及利息
        tLMCalModeDB.setType("K");
        set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }
        String lxCalCode=set.get(1).getCalCode();
        System.out.println("本次结算区间共拆分了"+tLMInsuAccRateSet.size()+"段");
		for (int j = 1; j <= tLMInsuAccRateSet.size(); j++) {
			LMInsuAccRateSchema tRate = tLMInsuAccRateSet.get(j);

			cTransferData.removeByName("StartDate");
			cTransferData.removeByName("EndDate");
			cTransferData.removeByName("RateType");
			cTransferData
					.setNameAndValue("StartDate", tRate.getStartBalaDate());
			cTransferData.setNameAndValue("EndDate", tRate.getBalaDate());
			cTransferData.setNameAndValue("RateType", tRate.getRateType());
//			tStartDate = (String) cTransferData.getValueByName("StartDate");
//			tEndDate = (String) cTransferData.getValueByName("EndDate");

			Calculator tCalculator = createCalculatorP(cTransferData,
					tPAccClassSchema);
			if (tCalculator == null) {
				return null;
			}
			tCalculator.setCalCode(accCalCode);
			String money = tCalculator.calculate();
			if (tCalculator.mErrors.needDealError()) {
				mErrors.copyAllErrors(tCalculator.mErrors);
				return null;
			}
			resultMoney = Double.parseDouble(money);
			tLX += resultMoney - oldMoney;
			System.out.println("第" + j + "段：起始日期：" + tRate.getStartBalaDate()
					+ ",截止日：" + tRate.getBalaDate() + ",结算类型："
					+ tRate.getRateType() + ",结算金额：" + resultMoney);

			tCalculator.setCalCode(lxCalCode);
			LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
			LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
			String sql = "select * from LCInsureAccTrace "
				+ "where PayDate >= '" + tStartDate + "' "
				+ "   and paydate < '" + tEndDate + "' "
				+ "   and PolNo = '" + tPAccClassSchema.getPolNo() + "' "
				+ "   and InsuAccNo = '"
				+ tPAccClassSchema.getInsuAccNo() + "' "
				+ "   and ((PayPlanCode = '"
				+ tPAccClassSchema.getPayPlanCode() + "') or (PayPlanCode in ('231201','231001','231801')))"
				+ "   and ((OtherType not in('1', '6')) or (OtherType='6' and moneytype='B')) with ur";  //契约轨迹和月结轨迹不计算利息
			System.out.println("查询sql==="+sql);
			tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(sql);
			FDate tFDate = new FDate();
			for (int i = 1; i <= tLCInsureAccTraceSet.size(); i++) {
				tCalculator = createCalculatorP(cTransferData, tPAccClassSchema);
				tCalculator.setCalCode(set.get(1).getCalCode());
				/*
				 * 迹的金额也要分区间算，起始日用轨迹生成日和区间起始日较晚的一个 若轨迹的发生时间早于本次结算区间的不进行结算。
				 */
				String traceStartDate = tLCInsureAccTraceSet.get(i)
						.getPayDate();
				System.out.println(tRate.getBalaDate());
				if (tFDate.getDate(traceStartDate).compareTo(
						tFDate.getDate(tRate.getBalaDate())) < 0) {
					if (tFDate.getDate(traceStartDate).compareTo(
							tFDate.getDate(tRate.getStartBalaDate())) <= 0) {
						traceStartDate = tRate.getStartBalaDate();
					}
				} else {
					continue;
				}
				tCalculator.addBasicFactor("TraceStartDate", traceStartDate); // 轨迹计算区间为产生轨迹日期到EndDate
				tCalculator.addBasicFactor("TraceMoney", ""
						+ tLCInsureAccTraceSet.get(i).getMoney());
				if (j == tLMInsuAccRateSet.size()) {
					oldMoney += tLCInsureAccTraceSet.get(i).getMoney();
				}

				money = tCalculator.calculate();
				if (tCalculator.mErrors.needDealError()) {
					mErrors.copyAllErrors(tCalculator.mErrors);
					return null;
				}
				tLX += Double.parseDouble(money)
						- tLCInsureAccTraceSet.get(i).getMoney();
				System.out.println("tracemoney="
						+ tLCInsureAccTraceSet.get(i).getMoney() + ",paydate="
						+ traceStartDate + ",结算金额=" + money + ",LX=" + tLX);

//				 resultMoney += Double.parseDouble(money);
			}

		}
        resultMoney = PubFun.setPrecision(resultMoney, "0.00");
//        tLX = resultMoney - oldMoney;  //收益

        System.out.println("上月价值及本周期轨迹计算后总价值(未扣除相关费用):" + resultMoney);

        //更新轨迹信息
        LPInsureAccClassSchema tPAccClassSchemaDelt = tPAccClassSchema.getSchema();
        tPAccClassSchemaDelt.setBalaDate(tEndDate);
        tPAccClassSchemaDelt.setInsuAccBala(resultMoney);
        tPAccClassSchemaDelt.setLastAccBala(tPAccClassSchemaDelt.getInsuAccBala());
        tPAccClassSchemaDelt.setOperator(tGI.Operator);
        tPAccClassSchemaDelt.setModifyDate(PubFun.getCurrentDate());
        tPAccClassSchemaDelt.setModifyTime(PubFun.getCurrentTime());

        Reflections ref = new Reflections();
        LPInsureAccTraceSchema traceSchemaP = new LPInsureAccTraceSchema();
        ref.transFields(traceSchemaP, tPAccClassSchema);

        String tLimit = PubFun.getNoLimit(tPAccClassSchemaDelt.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        traceSchemaP.setSerialNo(serNo);
        traceSchemaP.setMoneyType("LX");
        traceSchemaP.setMoney(PubFun.setPrecision(tLX, "0.00"));
        traceSchemaP.setPayDate(tEndDate);
        PubFun.fillDefaultField(traceSchemaP);

        MMap tMMap = new MMap();
        tMMap.put(tPAccClassSchemaDelt, SysConst.INSERT); //这里需要插入轨迹到P表。
        tMMap.put(traceSchemaP, SysConst.INSERT);

        return tMMap;
    }

    public MMap getBalaAccInterestUP(TransferData cTransferData)
    {
        LPInsureAccClassSchema tPAccClassSchema
            = (LPInsureAccClassSchema)cTransferData.getValueByName("LPInsureAccClass");
        GlobalInput tGI = (GlobalInput)cTransferData.getValueByName("GI");
        String tStartDate = (String)cTransferData.getValueByName("StartDate");
        String tEndDate = (String)cTransferData.getValueByName("EndDate");
        String tBalaType = (String)cTransferData.getValueByName("BalaType");
        String tLastBalaStr = (String)cTransferData.getValueByName("LastBala");
        String tLastBalaGuratStr = (String)cTransferData.getValueByName("LastBalaGurat");

        String oldMoneyStr = "1".equals(tBalaType) ? tLastBalaStr : tLastBalaGuratStr;
        if(oldMoneyStr == null || oldMoneyStr.equals(""))
        {
            mErrors.addOneError("请传入上一次结算金额");
            return null;
        }
        double oldMoney = Double.parseDouble(oldMoneyStr);

        //计算LastBala产生的计算金额
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setRiskCode((String)cTransferData.getValueByName("RiskCode"));
        tLMCalModeDB.setType("N");
        LMCalModeSet set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }

        Calculator tCalculator = createCalculatorP(cTransferData, tPAccClassSchema);
        if(tCalculator == null)
        {
            return null;
        }

        tCalculator.setCalCode(set.get(1).getCalCode());
        String money = tCalculator.calculate();
        if(tCalculator.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tCalculator.mErrors);
            return null;
        }
        double resultMoney = Double.parseDouble(money);

        //计算后轨迹本金及利息
        tLMCalModeDB.setType("R");
        set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return null;
        }
        tCalculator.setCalCode(set.get(1).getCalCode());

        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
        String sql = "select * from LCInsureAccTrace "
                     + "where PayDate >= '" + tStartDate + "' "
                     + "   and paydate < '" + tEndDate + "' "
                     + "   and PolNo = '" + tPAccClassSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + tPAccClassSchema.getInsuAccNo() + "' "
                     + "   and ((PayPlanCode = '"
                     + tPAccClassSchema.getPayPlanCode() + "') or (PayPlanCode in ('231201','231001','231801'))) "
                     + "   and ((OtherType not in('1', '6')) or (OtherType='6' and moneytype='B')) with ur ";  //契约轨迹和月结轨迹不计算利息
        tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(sql);

        for(int i = 1; i <= tLCInsureAccTraceSet.size(); i++)
        {
            tCalculator = createCalculatorP(cTransferData, tPAccClassSchema);
            tCalculator.setCalCode(set.get(1).getCalCode());
            tCalculator.addBasicFactor("TraceStartDate",
                                       tLCInsureAccTraceSet.get(i).getPayDate()); //轨迹计算区间为产生轨迹日期到EndDate
            tCalculator.addBasicFactor("TraceMoney",
                                       "" + tLCInsureAccTraceSet.get(i).getMoney());

            oldMoney += tLCInsureAccTraceSet.get(i).getMoney();
            money = tCalculator.calculate();
            if(tCalculator.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tCalculator.mErrors);
                return null;
            }
            resultMoney += Double.parseDouble(money);
        }
        resultMoney = PubFun.setPrecision(resultMoney, "0.00");
        double tLX = resultMoney - oldMoney;  //收益

        System.out.println("上月价值及本周期轨迹计算后总价值(未扣除相关费用):" + resultMoney);

        //更新轨迹信息
        LPInsureAccClassSchema tPAccClassSchemaDelt = tPAccClassSchema.getSchema();
        tPAccClassSchemaDelt.setBalaDate(tEndDate);
        tPAccClassSchemaDelt.setInsuAccBala(resultMoney);
        tPAccClassSchemaDelt.setLastAccBala(tPAccClassSchemaDelt.getInsuAccBala());
        tPAccClassSchemaDelt.setOperator(tGI.Operator);
        tPAccClassSchemaDelt.setModifyDate(PubFun.getCurrentDate());
        tPAccClassSchemaDelt.setModifyTime(PubFun.getCurrentTime());

        Reflections ref = new Reflections();
        LPInsureAccTraceSchema traceSchemaP = new LPInsureAccTraceSchema();
        ref.transFields(traceSchemaP, tPAccClassSchema);

        String tLimit = PubFun.getNoLimit(tPAccClassSchemaDelt.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        traceSchemaP.setSerialNo(serNo);
        traceSchemaP.setMoneyType("LX");
        traceSchemaP.setMoney(PubFun.setPrecision(tLX, "0.00"));
        traceSchemaP.setPayDate(tEndDate);
        PubFun.fillDefaultField(traceSchemaP);

        MMap tMMap = new MMap();
        tMMap.put(tPAccClassSchemaDelt, SysConst.INSERT); //这里需要插入轨迹到P表。
        tMMap.put(traceSchemaP, SysConst.INSERT);

        return tMMap;
    }
    
    
    /**
     * 补充团体医疗产品保额计算
     * @param cTransferData
     * @return
     */
    public MMap getTDBCBalaAccP(TransferData cTransferData){
    	GlobalInput tGI = (GlobalInput)cTransferData.getValueByName("GI");
    	LPInsureAccClassSchema tPInsureAccSchema
        = (LPInsureAccClassSchema)cTransferData.getValueByName("LPInsureAccClassSchema");
    	String startDate=(String)cTransferData.getValueByName("StartDate");
    	String balaDate=(String)cTransferData.getValueByName("BalaDate");
    	String riskcode=tPInsureAccSchema.getRiskCode();
    	
    	double oldMoney=0 ;	//账户中的保额
    	double resultMoney=0; //结算后的保额
    	
    	LMCalModeDB tLMCalModeDB=new LMCalModeDB();
    	tLMCalModeDB.setRiskCode(riskcode);
    	tLMCalModeDB.setType("J");
    	LMCalModeSet set = tLMCalModeDB.query();
    	if(set.size() == 0)
        {
            mErrors.addOneError("获取保额计算公式失败！");
            return null;
        }
    	String mCalCode=set.get(1).getCalCode();
    	
    	LCInsureAccTraceDB mLCInsureAccTraceDB=new LCInsureAccTraceDB();
    	String traceStr="select * from lcinsureacctrace "
    			+ " where contno='"+tPInsureAccSchema.getContNo()+"' "
    			+ " and polno='"+tPInsureAccSchema.getPolNo()+"' "
    			+ " and insuaccno='"+tPInsureAccSchema.getInsuAccNo()+"' "
    			+ " with ur";
    	LCInsureAccTraceSet mLCInsureAccTraceSet=mLCInsureAccTraceDB.executeQuery(traceStr);
    	for(int i = 1; i <= mLCInsureAccTraceSet.size(); i++){
    		FDate fdate=new FDate();
    		String mPayDate=mLCInsureAccTraceSet.get(i).getPayDate();
    		//对于费用发生日期晚于结算日的将其置为结算日
    		if(fdate.getDate(mPayDate).compareTo(fdate.getDate(balaDate))>0){
    			mPayDate=balaDate;
    		}
    		Calculator tCalculator = new Calculator();
        	tCalculator.setCalCode(mCalCode);
        	tCalculator.addBasicFactor("StartDate", mPayDate);
        	tCalculator.addBasicFactor("EndDate", balaDate);
        	tCalculator.addBasicFactor("Money", String.valueOf(mLCInsureAccTraceSet.get(i).getMoney()));
        	String money = tCalculator.calculate();
        	if(tCalculator.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tCalculator.mErrors);
                return null;
            }
        	if(null != money && !"".equals(money)){
        		resultMoney += Double.parseDouble(money);
        	}else{
        		mErrors.addOneError("保额记录"+mLCInsureAccTraceSet.get(i).getSerialNo()+"计算失败！");
                return null;
        	}
        	oldMoney += mLCInsureAccTraceSet.get(i).getMoney();
    	}
    	double mLX=resultMoney - oldMoney;//结算后增加的额度
    	
    	//更新轨迹信息
    	LPInsureAccClassSchema tPAccSchemaDelt = tPInsureAccSchema.getSchema();
        tPAccSchemaDelt.setBalaDate(balaDate);
        tPAccSchemaDelt.setInsuAccBala(resultMoney);
        tPAccSchemaDelt.setLastAccBala(tPInsureAccSchema.getInsuAccBala());
        tPAccSchemaDelt.setOperator(tGI.Operator);
        tPAccSchemaDelt.setModifyDate(PubFun.getCurrentDate());
        tPAccSchemaDelt.setModifyTime(PubFun.getCurrentTime());

        Reflections ref = new Reflections();
        LPInsureAccTraceSchema traceSchemaP = new LPInsureAccTraceSchema();
        ref.transFields(traceSchemaP, tPAccSchemaDelt);

        String tLimit = PubFun.getNoLimit(tPAccSchemaDelt.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        traceSchemaP.setSerialNo(serNo);
        traceSchemaP.setMoneyType("LX");
        traceSchemaP.setMoney(PubFun.setPrecision(mLX, "0.00"));
        traceSchemaP.setPayDate(balaDate);
        PubFun.fillDefaultField(traceSchemaP);
        
        //没有管理费，为后续处理，直接拿C表的过来
        Reflections tReflections = new Reflections();
        LPInsureAccClassFeeSchema tPAccClassFeeSchemaDelt =new LPInsureAccClassFeeSchema();
        LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
        tLCInsureAccClassFeeDB.setPolNo(tPInsureAccSchema.getPolNo());
        LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
        tReflections.transFields(tPAccClassFeeSchemaDelt, tLCInsureAccClassFeeSet.get(1));
        tPAccClassFeeSchemaDelt.setEdorNo(tPInsureAccSchema.getEdorNo());
        tPAccClassFeeSchemaDelt.setEdorType(tPInsureAccSchema.getEdorType());

        MMap tMMap = new MMap();
        tMMap.put(tPAccSchemaDelt, SysConst.INSERT); //这里需要插入轨迹到P表。
        tMMap.put(traceSchemaP, SysConst.INSERT);
        tMMap.put(tPAccClassFeeSchemaDelt, SysConst.INSERT);
    	
    	return tMMap;
    }
    
    /**
     * createCalculator
     *
     * @param cTransferData TransferData
     * @param cAccClassSchema LCInsureAccClassSchema
     * @return Calculator
     */
    private Calculator createCalculator(TransferData cTransferData,
                                        LCInsureAccClassSchema cAccClassSchema)
    {
        Calculator tCalculator = new Calculator();

        Vector tVector = cTransferData.getValueNames();
        for(int i = 0; i < tVector.size(); i++)
        {
            Object tNameObject = null;
            Object tValueObject = null;
            try
            {
                tNameObject = tVector.get(i);
                tValueObject = cTransferData.getValueByName(tNameObject);
                tCalculator.addBasicFactor((String)tNameObject, (String) tValueObject);
            }
            catch(Exception ex)
            {
                //对象无法转换为字符串，不需要做处理
            }
        }

        tCalculator.addBasicFactor("ContNo", cAccClassSchema.getContNo());
        tCalculator.addBasicFactor("PolNo", cAccClassSchema.getPolNo());
        tCalculator.addBasicFactor("InsuredNo", cAccClassSchema.getInsuredNo());
        tCalculator.addBasicFactor("InsuAccNo", cAccClassSchema.getInsuAccNo());
        tCalculator.addBasicFactor("PayPlanCode", cAccClassSchema.getPayPlanCode());
        tCalculator.addBasicFactor("RiskCode", cAccClassSchema.getRiskCode());
        tCalculator.addBasicFactor("InsuAccNo", cAccClassSchema.getInsuAccNo());

        String tStartDate = (String)cTransferData.getValueByName("StartDate");
        if(tStartDate == null)
        {
            mErrors.addOneError("请传入结算起始日期");
            return null;
        }

        String tInsuredAppAge = getInsuredAppAge(tStartDate, cAccClassSchema);
        if(tInsuredAppAge == null)
        {
            return null;
        }
        tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);

        return tCalculator;
    }
    private Calculator createCalculatorP(TransferData cTransferData,
                                       LPInsureAccClassSchema cPAccClassSchema)
   {
       Calculator tCalculator = new Calculator();

       Vector tVector = cTransferData.getValueNames();
       for(int i = 0; i < tVector.size(); i++)
       {
           Object tNameObject = null;
           Object tValueObject = null;
           try
           {
               tNameObject = tVector.get(i);
               tValueObject = cTransferData.getValueByName(tNameObject);
               tCalculator.addBasicFactor((String)tNameObject, (String) tValueObject);
           }
           catch(Exception ex)
           {
               //对象无法转换为字符串，不需要做处理
           }
       }

       tCalculator.addBasicFactor("ContNo", cPAccClassSchema.getContNo());
       tCalculator.addBasicFactor("PolNo", cPAccClassSchema.getPolNo());
       tCalculator.addBasicFactor("InsuredNo", cPAccClassSchema.getInsuredNo());
       tCalculator.addBasicFactor("InsuAccNo", cPAccClassSchema.getInsuAccNo());
       tCalculator.addBasicFactor("PayPlanCode", cPAccClassSchema.getPayPlanCode());
       tCalculator.addBasicFactor("RiskCode", cPAccClassSchema.getRiskCode());
       tCalculator.addBasicFactor("InsuAccNo", cPAccClassSchema.getInsuAccNo());

       String tStartDate = (String)cTransferData.getValueByName("StartDate");
       if(tStartDate == null)
       {
           mErrors.addOneError("请传入结算起始日期");
           return null;
       }

       String tInsuredAppAge = getInsuredAppAgeP(tStartDate, cPAccClassSchema);
       if(tInsuredAppAge == null)
       {
           return null;
       }
       tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);

       return tCalculator;
   }


    /**
     * getInsuredAppAge
     *
     * @param cAccClassSchema LCInsureAccClassSchema
     * @return String
     */
    public String getInsuredAppAge(String cStartDate,
                                   LCInsureAccClassSchema cAccClassSchema)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        
        String tPolNo = mLCPolSchema.getPolNo();
    	if(!"".equals(tPolNo) && tPolNo != null)
    	{
    		tLCPolDB = mLCPolSchema.getDB();
    	}
    	else
    	{
    		tLCPolDB.setPolNo(cAccClassSchema.getPolNo());
            if(!tLCPolDB.getInfo())
            {
                mErrors.addOneError("没有查询得到被保人险种信息："
                                    + cAccClassSchema.getInsuredNo());
                return null;
            }
    	}

        int tAge = PubFun.getInsuredAppAge(cStartDate,
                                           tLCPolDB.getInsuredBirthday());
        if(tAge < 0)
        {
            mErrors.addOneError("被保人投保年龄小于0："
                                + cAccClassSchema.getInsuredNo());
            return null;
        }

        return "" + tAge;
    }
    public String getInsuredAppAgeP(String cStartDate,
                                 LPInsureAccClassSchema cPAccClassSchema)
  {
      LCPolDB tLCPolDB = new LCPolDB();
      tLCPolDB.setPolNo(cPAccClassSchema.getPolNo());
      if(!tLCPolDB.getInfo())
      {
          mErrors.addOneError("没有查询得到被保人险种信息："
                              + cPAccClassSchema.getInsuredNo());
          return null;
      }

      int tAge = PubFun.getInsuredAppAge(cStartDate,
                                         tLCPolDB.getInsuredBirthday());
      if(tAge < 0)
      {
          mErrors.addOneError("被保人投保年龄小于0："
                              + cPAccClassSchema.getInsuredNo());
          return null;
      }

      return "" + tAge;
  }

    /**
     * 已知帐户余额得到帐户的利息（原描述帐户利率默认为年）
     * @param aInsuAccNo
     * @param aOriginAccBalance
     * @param aStartDate
     * @param aEndDate
     * @param aIntervalType
     * @return
     */
    public double getAccInterest(String aInsuAccNo, double aOriginAccBalance,
                                 String aStartDate, String aEndDate,
                                 String aIntervalType)
    {
        double aAccInterest = 0.0;
        double tAccRate, tCalAccRate;
        int tInterval = 0;
        String aRateType = "Y";
        //得到时间间隔
        tInterval = PubFun.calInterval(aStartDate, aEndDate, aIntervalType);
        //得到描述表中帐户利率
        LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
        tLMRiskInsuAccDB.setInsuAccNo(aInsuAccNo);
        if (!tLMRiskInsuAccDB.getInfo())
            return aAccInterest;

        switch (Integer.parseInt(tLMRiskInsuAccDB.getAccComputeFlag()))
        {
            //不计息
            case 0:
                aAccInterest = 0.0;
                break;
                //按照固定利率单利生息
            case 1:
                tAccRate = tLMRiskInsuAccDB.getAccRate();
                tCalAccRate = TransAccRate(tAccRate, aRateType, "S",
                                           aIntervalType);
                aAccInterest = aOriginAccBalance * tCalAccRate * tInterval;
                break;
                //按照固定利率复利生息
            case 2:
                tAccRate = tLMRiskInsuAccDB.getAccRate();
                tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                           aIntervalType);
                aAccInterest = aOriginAccBalance * tCalAccRate * tInterval;
                break;
                //按照利率表单利生息
            case 3:
                tAccRate = this.getAccRate(tLMRiskInsuAccDB.getSchema());
                tCalAccRate = TransAccRate(tAccRate, aRateType, "S",
                                           aIntervalType);
                aAccInterest = aOriginAccBalance * tCalAccRate * tInterval;
                break;
                //按照利率表复利生息
            case 4:
                tAccRate = this.getAccRate(tLMRiskInsuAccDB.getSchema());
                tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                           aIntervalType);
                aAccInterest = aOriginAccBalance * tCalAccRate * tInterval;
                break;
            default:
                aAccInterest = 0.0;
                break;
        }
        return aAccInterest;
    }


    /**
     * 已知帐户余额得到帐户的利息（按照描述帐户利率）
     * @param aInsuAccNo
     * @param aOriginAccBalance
     * @param aStartDate
     * @param aEndDate
     * @param aIntervalType
     * @return
     */
    public double getAccInterest(String aInsuAccNo, String aRateType,
                                 double aOriginAccBalance, String aStartDate,
                                 String aEndDate, String aIntervalType)
    {
        double aAccInterest = 0.0;
        double tAccRate, tCalAccRate;
        int tInterval = 0;

        //得到时间间隔
        tInterval = PubFun.calInterval(aStartDate, aEndDate, aIntervalType);
        //得到描述表中帐户利率
        LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
        tLMRiskInsuAccDB.setInsuAccNo(aInsuAccNo);
        if (!tLMRiskInsuAccDB.getInfo())
            return aAccInterest;

        switch (Integer.parseInt(tLMRiskInsuAccDB.getAccComputeFlag()))
        {
            //不计息
            case 0:
                aAccInterest = 0.0;
                break;
                //按照固定利率单利生息
            case 1:
                tAccRate = tLMRiskInsuAccDB.getAccRate();
                tCalAccRate = TransAccRate(tAccRate, aRateType, "S",
                                           aIntervalType);
                aAccInterest = aOriginAccBalance * tCalAccRate * tInterval;
                break;
                //按照固定利率复利生息
            case 2:
                tAccRate = tLMRiskInsuAccDB.getAccRate();
                tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                           aIntervalType);
                aAccInterest = aOriginAccBalance * tCalAccRate * tInterval;
                break;
                //按照利率表单利生息
            case 3:
                tAccRate = this.getAccRate(tLMRiskInsuAccDB.getSchema(),
                                           aRateType);
                tCalAccRate = TransAccRate(tAccRate, aRateType, "S",
                                           aIntervalType);
                aAccInterest = aOriginAccBalance * tCalAccRate * tInterval;
                break;
                //按照利率表复利生息
            case 4:
                tAccRate = this.getAccRate(tLMRiskInsuAccDB.getSchema(),
                                           aRateType);
                tCalAccRate = TransAccRate(tAccRate, aRateType, "C",
                                           aIntervalType);
                aAccInterest = aOriginAccBalance * tCalAccRate * tInterval;
                break;
            default:
                aAccInterest = 0.0;
                break;
        }
        return aAccInterest;
    }


    /**
     * 得到描述表中的利率
     * @param InsuAccNo
     * @return
     */
    private double getAccRate(LMRiskInsuAccSchema aLMRiskInsuAccSchema)
    {
        double aAccRate = 0.0;
        LMRiskInsuAccSchema tLMRiskInsuAccSchema = new LMRiskInsuAccSchema();
        Calculator tCalculator = new Calculator();
        tCalculator.addBasicFactor("InsuAccNo",
                                   tLMRiskInsuAccSchema.getInsuAccNo());
        //默认为年利率
        tCalculator.addBasicFactor("RateType", "Y");
        //待填加其它条件
        //tCalculator.addBasicFactor("","");
        tCalculator.setCalCode(tLMRiskInsuAccSchema.getAccCancelCode());
        String tStr = "";
        tStr = tCalculator.calculate();
        System.out.println("---str" + tStr);
        if (tStr != null && !tStr.trim().equals(""))
            aAccRate = Double.parseDouble(tStr);

        return aAccRate;
    }


    /**
     * 按传入类型得到描述表中的利率
     * @param InsuAccNo
     * @return
     */
    private double getAccRate(LMRiskInsuAccSchema aLMRiskInsuAccSchema,
                              String aRateType)
    {
        double aAccRate = 0.0;
        LMRiskInsuAccSchema tLMRiskInsuAccSchema = new LMRiskInsuAccSchema();
        Calculator tCalculator = new Calculator();
        tCalculator.addBasicFactor("InsuAccNo",
                                   tLMRiskInsuAccSchema.getInsuAccNo());
        tCalculator.addBasicFactor("RateType", aRateType);
        //待填加其它条件
        //tCalculator.addBasicFactor("","");
        tCalculator.setCalCode(tLMRiskInsuAccSchema.getAccCancelCode());
        String tStr = "";
        tStr = tCalculator.calculate();
        System.out.println("---str" + tStr);
        if (tStr != null && !tStr.trim().equals(""))
            aAccRate = Double.parseDouble(tStr);

        return aAccRate;
    }


    /**
     * 利率转换函数
     * @param OriginRate(原始利率）
     * @param OriginRateType（原始利率类型：年利率（"Y")，月利率("M")，日利率("D")）
     * @param TransType（复利转换("C")compound，单利转换("S")simple）
     * @param DestRateType（年利率，月利率,日利率）
     * @return
     */
    public static double TransAccRate(double OriginRate, String OriginRateType,
                                      String TransType, String DestRateType)
    {
        double DestRate = 0;
        double aPower;

        //Add by Minim for RF of BQ
        if (TransType.equals("1")) TransType = "S";
        if (TransType.equals("2")) TransType = "C";

        if (OriginRateType.equals("1")) OriginRateType = "Y";
        if (OriginRateType.equals("2")) OriginRateType = "M";
        if (OriginRateType.equals("3")) OriginRateType = "D";
        //End add by Minim

        //复利处理
        if (TransType.equals("C"))
        {
            //年复利转换
            if (OriginRateType.equals("Y"))
            {
                //translate to year
                if (DestRateType.equals("Y"))
                    DestRate = OriginRate;
                    //translate to month
                else if (DestRateType.equals("M"))
                {
                    aPower = 1.0 / 12.0;
                    DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
                }
                //translate to day
                else if (DestRateType.equals("D"))
                {
//          aPower = 1.0 / 365.0;
                    aPower = 1.0 / Double.parseDouble(SysConst.DAYSOFYEAR);
                    DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
                }
                else
                {
                    System.out.println("-----CY no this DestRateType----");
                }
            }
            //月复利转换
            else if (OriginRateType.equals("M"))
            {
                //translate to month
                if (DestRateType.equals("M"))
                    DestRate = OriginRate;
                    //translate to year
                else if (DestRateType.equals("Y"))
                {
                    aPower = 12;
                    DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
                }
                //translate to day
                else if (DestRateType.equals("D"))
                {
                    aPower = 1.0 / 30.0;
                    DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
                }
                else
                {
                    System.out.println("-----CM no this DestRateType----");
                }
            }
            //日复利转换
            else if (OriginRateType.equals("D"))
            {
                //translate to day
                if (DestRateType.equals("D"))
                    DestRate = OriginRate;
                    //translate to month
                else if (DestRateType.equals("M"))
                {
                    aPower = 30;
                    DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
                }
                //translate to year
                else if (DestRateType.equals("Y"))
                {
                    aPower = 365;
                    DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
                }
                else
                {
                    System.out.println("-----CD no this DestRateType----");
                }
            }
            else
            {
                System.out.println("------C no this OriginRateType------");
            }
        }
        //单利处理
        else if (TransType.equals("S"))
        {
            //年单利转换
            if (OriginRateType.equals("Y"))
            {
                //translate to year
                if (DestRateType.equals("Y"))
                    DestRate = OriginRate;
                    //translate to month
                else if (DestRateType.equals("M"))
                {
                    DestRate = OriginRate / 12;
                }
                //translate to day
                else if (DestRateType.equals("D"))
                {
                    DestRate = OriginRate / 365;
                }
                else
                {
                    System.out.println("-----SY no this DestRateType----");
                }
            }
            //月单利转换
            else if (OriginRateType.equals("M"))
            {
                //translate to month
                if (DestRateType.equals("M"))
                    DestRate = OriginRate;
                    //translate to year
                else if (DestRateType.equals("Y"))
                {
                    DestRate = OriginRate * 12;
                }
                //translate to day
                else if (DestRateType.equals("D"))
                {
                    DestRate = OriginRate / 30;
                }
                else
                {
                    System.out.println("-----SM no this DestRateType----");
                }
            }
            //日单利转换
            else if (OriginRateType.equals("D"))
            {
                //translate to day
                if (DestRateType.equals("D"))
                    DestRate = OriginRate;
                    //translate to month
                else if (DestRateType.equals("M"))
                {
                    DestRate = OriginRate * 30;
                }
                //translate to year
                else if (DestRateType.equals("Y"))
                {
                    DestRate = OriginRate * 365;
                }
                else
                {
                    System.out.println("-----SD no this DestRateType----");
                }
            }
            else
            {
                System.out.println("------S no this OriginRateType------");
            }
        }
        else
        {
            System.out.println("-------have not this TransType------");
        }
        return DestRate;
    }


    /**
     * 得到结息后的帐户记录
     * @return
     */
    public LCInsureAccSet getInsureAcc()
    {
        return mLCInsureAccSet;
    }


    /**
     * 得到分段结息计算参数。(未测试)
     * @param aLCInsureAccSchema
     * @param aLMRiskInsuAccSchema
     * @param aBalanceDate
     * @param aRateType
     * @param aIntervalType
     * @return
     */

    private String[] getMultiAccRate1(LCInsureAccSchema aLCInsureAccSchema,
                                      LMRiskInsuAccSchema aLMRiskInsuAccSchema,
                                      String aBalanceDate, String aTransType,
                                      String aIntervalType)
    {
        String tSql = "";
        String ResultArray[] = new String[100];
        Calculator tCalculator = new Calculator();
        String TableName = aLMRiskInsuAccSchema.getAccRateTable();
        if (aLMRiskInsuAccSchema.getAccCancelCode() != null &&
            !aLMRiskInsuAccSchema.getAccCancelCode().trim().equals(""))
        {
            try
            {
                tSql = PubFun1.getSQL(aLMRiskInsuAccSchema.getAccCancelCode(),
                                      tCalculator);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AccountManage";
                tError.functionName = "getMultiAccInterest";
                tError.errorMessage = e.toString();
                this.mErrors.addOneError(tError);
            }
        }
        else
        {
            TableName = aLMRiskInsuAccSchema.getAccRateTable();
            tSql = "select * from '" + TableName + "' where InsuAccNo='" +
                   aLCInsureAccSchema.getInsuAccNo() + "' and EndDate>='" +
                   aLCInsureAccSchema.getBalaDate() + "' and EndDate<='" +
                   aBalanceDate + "' order by EndDate";
        }

        Statement stmt = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        double aValue = 0.0;
        double aResult = 0.0;
        String aDate = "";
        String tStartDate = "";

        DBOper db = new DBOper("");
        Connection con = db.getConnection();

        System.out.println(tSql.trim());
        try
        {
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                       ResultSet.CONCUR_READ_ONLY);
            //得到其中所有的时间段信息
            rs = stmt.executeQuery(StrTool.GBKToUnicode(tSql));
            int i = 0;
            while (rs.next())
            {
                i++;
                if (i == 1)
                {
                    tStartDate = aLCInsureAccSchema.getBalaDate();
                }
                aDate = rs.getString("EndDate");
                aValue = rs.getDouble("Rate");
                String aRateIntv = rs.getString("RateIntv");
                double tCalAccRate = TransAccRate(aValue, aRateIntv, aTransType,
                                                  aIntervalType);
                int tInterval = PubFun.calInterval(tStartDate, aDate,
                        aIntervalType);
                if (tInterval > 0)
                {
                    aResult = getIntvRate(tInterval, tCalAccRate, aTransType);
                    ResultArray[i - 1] = String.valueOf(aResult);
                }
                tStartDate = aDate;
            }
            rs.close();

            //得到结息日在利率临界点后
            if (ResultArray.length > 0)
            {
                int tLength = ResultArray.length - 1;
                tSql = "select * from '" + TableName + "' where InsuAccNo='" +
                       aLCInsureAccSchema.getInsuAccNo() + "' and StartDate<'" +
                       aBalanceDate + "' and EndDate>'" + aBalanceDate +
                       "' order by EndDate";
                rs1 = stmt.executeQuery(StrTool.GBKToUnicode(tSql));
                i = 0;
                while (rs1.next())
                {
                    i++;
                    aDate = rs1.getString("StartDate");
                    aValue = rs1.getDouble("Rate");
                    String aRateIntv = rs.getString("RateIntv");
                    double tCalAccRate = TransAccRate(aValue, aRateIntv,
                            aTransType, aIntervalType);
                    int tInterval = PubFun.calInterval(aDate, aBalanceDate,
                            aIntervalType);
                    aResult = getIntvRate(tInterval, tCalAccRate, aTransType);
                    ResultArray[tLength++] = String.valueOf(aResult);
                }
                rs1.close();
            }
            else
            {
                tSql = "select * from '" + TableName + "' where InsuAccNo='" +
                       aLCInsureAccSchema.getInsuAccNo() + "' and StartDate<'" +
                       aLCInsureAccSchema.getBalaDate() + "' and EndDate>'" +
                       aBalanceDate + "' order by EndDate";
                rs2 = stmt.executeQuery(StrTool.GBKToUnicode(tSql));
                i = 0;
                while (rs2.next())
                {
                    i++;
                    aDate = rs2.getString("StartDate");
                    aValue = rs2.getDouble("Rate");
                    String aRateIntv = rs.getString("RateIntv");
                    double tCalAccRate = TransAccRate(aValue, aRateIntv,
                            aTransType, aIntervalType);
                    int tInterval = PubFun.calInterval(aDate,
                            rs2.getString("EndDate"), aIntervalType);
                    aResult = getIntvRate(tInterval, tCalAccRate, aTransType);
                    ResultArray[i - 1] = String.valueOf(aResult);
                }
                rs1.close();
            }
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AccountManage";
            tError.functionName = "getMultiAccInterest";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

        }
        return ResultArray;
    }


    /**
     * 得到分段结息的参数（推荐）
     * @param aLCInsureAccSchema
     * @param aLMRiskInsuAccSchema
     * @param aBalanceDate
     * @param aRateType
     * @param aTransType
     * @param aIntervalType
     * @return
     */
    private String[] getMultiAccRate(LCInsureAccSchema aLCInsureAccSchema,
                                     LMRiskInsuAccSchema aLMRiskInsuAccSchema,
                                     String aBalanceDate, String aRateType,
                                     String aTransType, String aIntervalType)
    {
        String tSql = "";
        String[] ResultArray = new String[100];
        Calculator tCalculator = new Calculator();
        String TableName = aLMRiskInsuAccSchema.getAccRateTable();
        if (aLMRiskInsuAccSchema.getAccCancelCode() != null &&
            !aLMRiskInsuAccSchema.getAccCancelCode().trim().equals(""))
        {
            try
            {
                tSql = PubFun1.getSQL(aLMRiskInsuAccSchema.getAccCancelCode(),
                                      tCalculator);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AccountManage";
                tError.functionName = "getMultiAccInterest";
                tError.errorMessage = e.toString();
                this.mErrors.addOneError(tError);
            }
        }
        else
        {
            TableName = aLMRiskInsuAccSchema.getAccRateTable();
            tSql = "select * from '" + TableName + "' where InsuAccNo='" +
                   aLCInsureAccSchema.getInsuAccNo() + "' and StartDate<='" +
                   aBalanceDate + "' and EndDate>='" +
                   aLCInsureAccSchema.getBalaDate() + "' order by EndDate";
        }

        Statement stmt = null;
        ResultSet rs = null;
        double aValue = 0.0;
        double aResult = 0.0;
        String aDate = "";
        String tStartDate = "";

        DBOper db = new DBOper("");
        Connection con = db.getConnection();

        System.out.println(tSql.trim());
        try
        {
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                       ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(StrTool.GBKToUnicode(tSql));
            int i = 0;
            while (rs.next())
            {
                i++;
                aDate = rs.getString("EndDate");
                aValue = rs.getDouble("Rate");
                String aRateIntv = rs.getString("RateIntv");
                double tCalAccRate = TransAccRate(aValue, aRateIntv, aTransType,
                                                  aIntervalType);
                if (i == 1)
                {
                    tStartDate = aLCInsureAccSchema.getBalaDate();
                }
                //结息点超过某个利率临界点
                if (PubFun.calInterval(aDate, aBalanceDate, aIntervalType) > 0)
                {
                    int tInterval = PubFun.calInterval(tStartDate, aDate,
                            aIntervalType);
                    if (tInterval > 0)
                    {
                        aResult = getIntvRate(tInterval, tCalAccRate,
                                              aTransType);
                        ResultArray[i - 1] = String.valueOf(aResult);
                    }
                    tStartDate = aDate;
                }
                else
                {
                    int tInterval = PubFun.calInterval(tStartDate, aBalanceDate,
                            aIntervalType);
                    if (tInterval > 0)
                    {
                        aResult = getIntvRate(tInterval, tCalAccRate,
                                              aTransType);
                        ResultArray[i - 1] = String.valueOf(aResult);
                    }
                    break;
                }
            }
            rs.close();
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AccountManage";
            tError.functionName = "getMultiAccInterest";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

        }
        return ResultArray;
    }


    /**
     ** 得到分段结息的参数（推荐）
     * @param aInsuAccNo
     * @param aLMRiskInsuAccSchema
     * @param aOriginBalaDate
     * @param aBalanceDate
     * @param aRateType 原描述利率的类型
     * @param aTransType 单利复利
     * @param aIntervalType 转换成利率类型
     * @return
     */
    private String[] getMultiAccRate(String aInsuAccNo,
                                     LMRiskInsuAccSchema aLMRiskInsuAccSchema,
                                     String aOriginBalaDate,
                                     String aBalanceDate, String aRateType,
                                     String aTransType, String aIntervalType)
    {
        String tSql = "";
        String[] ResultArray = new String[100];
        Calculator tCalculator = new Calculator();
        String TableName = aLMRiskInsuAccSchema.getAccRateTable();
        if (aLMRiskInsuAccSchema.getAccCancelCode() != null &&
            !aLMRiskInsuAccSchema.getAccCancelCode().trim().equals(""))
        {
            try
            {
                tSql = PubFun1.getSQL(aLMRiskInsuAccSchema.getAccCancelCode(),
                                      tCalculator);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AccountManage";
                tError.functionName = "getMultiAccInterest";
                tError.errorMessage = e.toString();
                this.mErrors.addOneError(tError);
            }
        }
        else
        {
            TableName = aLMRiskInsuAccSchema.getAccRateTable();
            tSql = "select * from " + TableName + " where InsuAccNo='" +
                   aInsuAccNo + "' and StartDate<='" + aBalanceDate +
                   "' and EndDate>='" + aOriginBalaDate + "' order by EndDate";
        }

        Statement stmt = null;
        ResultSet rs = null;
        double aValue = 0.0;
        double aResult = 0.0;
        String aDate = "";
        String tStartDate = "";

        DBOper db = new DBOper("");
        Connection con = db.getConnection();

        System.out.println(tSql.trim());
        try
        {
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                       ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(StrTool.GBKToUnicode(tSql));
            int i = 0;
            while (rs.next())
            {
                i++;
                aDate = rs.getString("EndDate");
                aValue = rs.getDouble("Rate");
                String aRateIntv = rs.getString("RateIntv");
                double tCalAccRate = TransAccRate(aValue, aRateType, aTransType,
                                                  aIntervalType);
                if (i == 1)
                {
                    tStartDate = aOriginBalaDate;
                }
                //结息点超过某个利率临界点
                if (PubFun.calInterval(aDate, aBalanceDate, aIntervalType) > 0)
                {
                    int tInterval = PubFun.calInterval(tStartDate, aDate,
                            aIntervalType);
                    if (tInterval > 0)
                    {
                        aResult = getIntvRate(tInterval, tCalAccRate,
                                              aTransType);
                        ResultArray[i - 1] = String.valueOf(aResult);
                    }
                    tStartDate = aDate;
                }
                else
                {
                    int tInterval = PubFun.calInterval(tStartDate, aBalanceDate,
                            aIntervalType);
                    if (tInterval > 0)
                    {
                        aResult = getIntvRate(tInterval, tCalAccRate,
                                              aTransType);
                        ResultArray[i - 1] = String.valueOf(aResult);
                    }
                    break;
                }
            }
            rs.close();
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AccountManage";
            tError.functionName = "getMultiAccInterest";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
        }
        try
        {
            if (!con.isClosed())
            {
                con.close();
            }
        }
        catch (Exception e)
        {
            CError tError = new CError();
            tError.moduleName = "AccountManage";
            tError.functionName = "getMultiAccInterest";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
        }

        return ResultArray;
    }


    /**
     ** 得到帐户描述分段结息的参数（推荐）
     * @param aInsuAccNo
     * @param aLMRiskInsuAccSchema
     * @param aOriginBalaDate
     * @param aBalanceDate
     * @param aRateType 原描述利率的类型
     * @param aTransType 单利复利
     * @param aIntervalType 转换成利率类型
     * @return
     */
    public double getMultiAccInterest(String aInsuAccNo,
                                      LMRiskInsuAccSchema aLMRiskInsuAccSchema,
                                      double aBalance, String aOriginBalaDate,
                                      String aBalanceDate, String aTransType,
                                      String aIntervalType)
    {
        String tSql = "";
        double tInterest = 0.0;
        String[] ResultArray = new String[100];
        Calculator tCalculator = new Calculator();
        String TableName = aLMRiskInsuAccSchema.getAccRateTable();
        if (aLMRiskInsuAccSchema.getAccCancelCode() != null &&
            !aLMRiskInsuAccSchema.getAccCancelCode().trim().equals(""))
        {
            try
            {
                tSql = PubFun1.getSQL(aLMRiskInsuAccSchema.getAccCancelCode(),
                                      tCalculator);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AccountManage";
                tError.functionName = "getMultiAccInterest";
                tError.errorMessage = e.toString();
                this.mErrors.addOneError(tError);
            }
            ExeSQL tExeSQL = new ExeSQL();
            String tStr = tExeSQL.getOneValue(tSql);
            if (tStr == null || tStr.trim().equals(""))
                tStr = "0";
            tInterest = Double.parseDouble(tStr);
        }
        else
        {
            TableName = aLMRiskInsuAccSchema.getAccRateTable();
            tSql = "select * from " + TableName + " where InsuAccNo='" +
                   aInsuAccNo + "' and StartDate<='" + aBalanceDate +
                   "' and EndDate>='" + aOriginBalaDate + "' order by EndDate";

            Statement stmt = null;
            ResultSet rs = null;
            double aValue = 0.0;
            double aResult = 0.0;
            String aDate = "";
            String tStartDate = "";

            DBOper db = new DBOper("");
            Connection con = db.getConnection();

            System.out.println(tSql.trim());
            try
            {
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                           ResultSet.CONCUR_READ_ONLY);
                rs = stmt.executeQuery(StrTool.GBKToUnicode(tSql));
                int i = 0;
                while (rs.next())
                {
                    i++;
                    aDate = rs.getString("EndDate");
                    aValue = rs.getDouble("Rate");
                    String aRateIntv = rs.getString("RateIntv");
                    double tCalAccRate = TransAccRate(aValue, aRateIntv,
                            aTransType, aIntervalType);

                    if (i == 1)
                    {
                        tStartDate = aOriginBalaDate;
                    }
                    //结息点超过某个利率临界点
                    if (PubFun.calInterval(aDate, aBalanceDate, aIntervalType) >
                        0)
                    {
                        int tInterval = PubFun.calInterval(tStartDate, aDate,
                                aIntervalType);
                        if (tInterval > 0)
                        {
                            aResult = getIntvRate(tInterval, tCalAccRate,
                                                  aTransType);
                            tInterest = tInterest + aBalance * aResult;
                        }
                        tStartDate = aDate;
                    }
                    else
                    {
                        int tInterval = PubFun.calInterval(tStartDate,
                                aBalanceDate, aIntervalType);
                        if (tInterval > 0)
                        {
                            aResult = getIntvRate(tInterval, tCalAccRate,
                                                  aTransType);
                            tInterest = tInterest + aBalance * aResult;
                        }
                        break;
                    }
                }
                rs.close();
                stmt.close();
                con.close();
            }
            catch (Exception e)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AccountManage";
                tError.functionName = "getMultiAccInterest";
                tError.errorMessage = e.toString();
                this.mErrors.addOneError(tError);
                try
                {
                    rs.close();
                    stmt.close();
                    con.close();
                }
                catch (Exception ex)
                {}
            }
        }
        return tInterest;
    }


    /**
     ** 得到借款描述分段结息的参数（推荐）
     * @param aInsuAccNo
     * @param aLMRiskInsuAccSchema
     * @param aOriginBalaDate
     * @param aBalanceDate
     * @param aRateType 原描述利率的类型
     * @param aTransType 单利复利
     * @param aIntervalType 转换成利率类型
     * @return
     */
    private double getMultiAccInterest(LOLoanSchema aLOLoanSchema,
                                       double aBalance, String aOriginBalaDate,
                                       String aBalanceDate, String aTransType,
                                       String aIntervalType)
    {
        String tSql = "";
        double tInterest = 0.0;
        String[] ResultArray = new String[100];
        Calculator tCalculator = new Calculator();
        if (aLOLoanSchema.getRateCalType() != null &&
            aLOLoanSchema.getRateCalType().trim().equals("2"))
        {
            try
            {
                tSql = PubFun1.getSQL(aLOLoanSchema.getRateCalCode(),
                                      tCalculator);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AccountManage";
                tError.functionName = "getMultiAccInterest";
                tError.errorMessage = e.toString();
                this.mErrors.addOneError(tError);
            }
            ExeSQL tExeSQL = new ExeSQL();
            String tStr = tExeSQL.getOneValue(tSql);
            if (tStr == null || tStr.trim().equals(""))
                tStr = "0";
            tInterest = Double.parseDouble(tStr);
        }
        else
        {
            String TableName = aLOLoanSchema.getRateCalCode();
            tSql = "select * from '" + TableName + "' where  and StartDate<='" +
                   aBalanceDate + "' and EndDate>='" + aOriginBalaDate +
                   "' order by EndDate";

            Statement stmt = null;
            ResultSet rs = null;
            double aValue = 0.0;
            double aResult = 0.0;
            String aDate = "";
            String tStartDate = "";

            DBOper db = new DBOper("");
            Connection con = db.getConnection();

            System.out.println(tSql.trim());
            try
            {
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                           ResultSet.CONCUR_READ_ONLY);
                rs = stmt.executeQuery(StrTool.GBKToUnicode(tSql));
                int i = 0;
                while (rs.next())
                {
                    i++;
                    aDate = rs.getString("EndDate");
                    aValue = rs.getDouble("Rate");
                    String aRateIntv = rs.getString("RateIntv");
                    double tCalAccRate = TransAccRate(aValue, aRateIntv,
                            aTransType, aIntervalType);

                    if (i == 1)
                    {
                        tStartDate = aOriginBalaDate;
                    }
                    //结息点超过某个利率临界点
                    if (PubFun.calInterval(aDate, aBalanceDate, aIntervalType) >
                        0)
                    {
                        int tInterval = PubFun.calInterval(tStartDate, aDate,
                                aIntervalType);
                        if (tInterval > 0)
                        {
                            aResult = getIntvRate(tInterval, tCalAccRate,
                                                  aTransType);
                            tInterest = tInterest + aBalance * aResult;
                        }
                        tStartDate = aDate;
                    }
                    else
                    {
                        int tInterval = PubFun.calInterval(tStartDate,
                                aBalanceDate, aIntervalType);
                        if (tInterval > 0)
                        {
                            aResult = getIntvRate(tInterval, tCalAccRate,
                                                  aTransType);
                            tInterest = tInterest + aBalance * aResult;
                        }
                        break;
                    }
                }
                rs.close();
                stmt.close();
                con.close();
            }
            catch (Exception e)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AccountManage";
                tError.functionName = "getMultiAccInterest";
                tError.errorMessage = e.toString();
                this.mErrors.addOneError(tError);
                try
                {
                    rs.close();
                    stmt.close();
                    con.close();
                }
                catch (Exception ex)
                {}
            }
        }
        return tInterest;
    }


    /**
     * 得到按照银行利率结息分段结息的参数（推荐）
     * @param aRateKind 活期,一年定期,二年定期
     * @param aBalance 原余额
     * @param aOriginBalaDate 利息起期
     * @param aBalanceDate 利息止期
     * @param aTransType  单利复利
     * @param aIntervalType 转换成利率类型
     * @return
     */
    public static double getMultiAccInterest(String aRateType, double aBalance,
                                             String aOriginBalaDate,
                                             String aBalanceDate,
                                             String aTransType,
                                             String aIntervalType)
    {
        String tSql = "";
        FDate tFDate = new FDate();
        String[] ResultArray = new String[100];
        //tSql = "select to_char(StartDate,'"+"yyyy-mm-dd"+"') as StartDate,to_char(EndDate,'"+"yyyy-mm-dd"+"') as EndDate,RateType,RateIntv,Rate from LDBankRate where RateType='"+aRateType+"' and StartDate<='"+aBalanceDate+"' and EndDate>='"+aOriginBalaDate+"' order by EndDate";
        tSql = "select * from LDBankRate where RateType='" + aRateType +
               "' and StartDate<='" + aBalanceDate + "' and EndDate>='" +
               aOriginBalaDate + "' order by EndDate";
        Statement stmt = null;
        ResultSet rs = null;
        double aValue = 0.0;
        double aResult = 0.0;
        String aDate = "";
        String tStartDate = "";

        double tInterest = 0.0;

        DBOper db = new DBOper("");
        Connection con = db.getConnection();

        System.out.println(tSql.trim());
        try
        {
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                       ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(StrTool.GBKToUnicode(tSql));
            int i = 0;
            while (rs.next())
            {
                i++;
                aDate = tFDate.getString(rs.getDate("EndDate"));
                aValue = rs.getDouble("Rate");
                String aRateIntv = rs.getString("RateIntv");
                double tCalAccRate = TransAccRate(aValue, aRateIntv, aTransType,
                                                  aIntervalType);
                if (i == 1)
                {
                    tStartDate = aOriginBalaDate;
                }
                //结息点超过某个利率临界点
                if (PubFun.calInterval(aDate, aBalanceDate, aIntervalType) > 0)
                {
                    int tInterval = PubFun.calInterval(tStartDate, aDate,
                            aIntervalType);
                    if (tInterval > 0)
                    {
                        aResult = getIntvRate(tInterval, tCalAccRate,
                                              aTransType);
                        tInterest = tInterest + aBalance * aResult;
                    }
                    tStartDate = aDate;
                }
                else
                {
                    int tInterval = PubFun.calInterval(tStartDate, aBalanceDate,
                            aIntervalType);
                    if (tInterval > 0)
                    {
                        aResult = getIntvRate(tInterval, tCalAccRate,
                                              aTransType);
                        tInterest = tInterest + aBalance * aResult;
                    }
                    break;
                }
            }
            rs.close();
            stmt.close();
            con.close();
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AccountManage";
            tError.functionName = "getMultiAccInterest";
            tError.errorMessage = e.toString();
            try
            {
                rs.close();
                stmt.close();
                con.close();
            }
            catch (Exception ex)
            {}
        }

        return tInterest;
    }


    /**
     *
     * @return
     */
    public boolean updAccBalance()
    {

        Connection conn = null;
        conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AccountManage";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        try
        {
            conn.setAutoCommit(false);
            if (mLCInsureAccTraceSet.size() > 0)
            {
                LCInsureAccTraceDBSet tLCInsureAccTraceDBSet = new
                        LCInsureAccTraceDBSet(conn);
                tLCInsureAccTraceDBSet.set(mLCInsureAccTraceSet);
                if (!tLCInsureAccTraceDBSet.update())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "AccountManage";
                    tError.functionName = "saveData";
                    tError.errorMessage = "子帐户更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            if (mLCInsureAccSet.size() > 0)
            {
                LCInsureAccDBSet tLCInsureAccDBSet = new LCInsureAccDBSet(conn);
                tLCInsureAccDBSet.set(mLCInsureAccSet);
                if (!tLCInsureAccDBSet.update())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "AccountManage";
                    tError.functionName = "saveData";
                    tError.errorMessage = "帐户余额更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AccountManage";
            tError.functionName = "updAccBalance";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }


    /**
     * 得到结息后的帐户轨迹
     * @return
     */
    public LCInsureAccTraceSet getInsureAccTrace()
    {
        return mLCInsureAccTraceSet;
    }


    /**
     *得到固定利率利息
     * @param OriginBalance 原始金额
     * @param aStartDate 起始日期
     * @param aEndDate   终止日期
     * @param OriginRate(原始利率）
     * @param OriginRateType（原始利率类型：年利率（"Y")，月利率("M")，日利率("D")）
     * @param TransType（复利转换("C")compound，单利转换("S")simple）
     * @param DestRateType（年利率，月利率,日利率）
     * @return
     */
    public double getInterest(double OriginBalance, String aStartDate,
                              String aEndDate, double OriginRate,
                              String OriginRateType, String TransType,
                              String DestRateType)
    {
        double aInterest = 0.0;
        double tCalRate = 0.0;
        tCalRate = TransAccRate(OriginRate, OriginRateType, TransType,
                                DestRateType);
        int tInterval = PubFun.calInterval(aStartDate, aEndDate, DestRateType);
        aInterest = OriginBalance * getIntvRate(tInterval, tCalRate, TransType);
        return aInterest;
    }


    /**
     * 取得利息
     * @param aCalType
     * @param aBalance
     * @param aStartDate
     * @param aEndDate
     * @return
     */
    public double getMultiInterest(String aCalType, VData aVData,
                                   double aBalance, String aStartDate,
                                   String aEndDate)
    {
        double aInterest = 0.0;
        if (aCalType.equals("0"))
        {

        }
        else if (aCalType.equals("1"))
        {
            LOLoanSchema tLOLoanSchema = new LOLoanSchema();
            tLOLoanSchema = (LOLoanSchema) aVData.getObjectByObjectName(
                    "LOLoanSchema", 0);
            if (tLOLoanSchema.getSpecifyRate().equals("1"))
            {
                aInterest = getInterest(aBalance, aStartDate, aEndDate,
                                        tLOLoanSchema.getInterestRate(),
                                        tLOLoanSchema.getInterestMode(),
                                        tLOLoanSchema.getInterestType(), "D");
            }
            else if (tLOLoanSchema.getSpecifyRate().equals("2"))
            {
                aInterest = getMultiAccInterest(tLOLoanSchema, aBalance,
                                                aStartDate, aEndDate,
                                                tLOLoanSchema.getInterestType(),
                                                "D");
            }
            else
            {
                System.out.println("-----no this type----");
            }
        }
        else
        {

        }
        return aInterest;
    }


    /**
     * 得到计算时间间隔的比率
     * @param aInterval
     * @param aRate
     * @param aTransType
     * @return
     */
    private static double getIntvRate(int aInterval, double aRate,
                                      String aTransType)
    {
        double aIntvRate = 0.0;

        //Add by Minim for RF of BQ
        if (aTransType.equals("1")) aTransType = "S";
        if (aTransType.equals("2")) aTransType = "C";
        //End add by Minim;

        if (aTransType.equals("S"))
        {
            aIntvRate = aRate * aInterval;
        }
        else if (aTransType.equals("C"))
        {
            aIntvRate = java.lang.Math.pow(1 + aRate, aInterval) - 1;
        }
        else
        {

        }
        return aIntvRate;
    }

    /**
     * 计算账户余额利息
     * @param accBalance double
     * @param polYearType int：险种类型
     * @param accTypeFlag String：账户类型
     * @return double
     */
    public double getAccInterest(String grpPolNo,
                                        int payIntv,
                                        String accType,
                                        double rate)
    {
        //if(payIntv == 12)
        //{
            return getAccInterestYear(grpPolNo, accType, rate);
        //}
        //return 0;
    }

    /**
     * 计算一年期特需医疗账户余额利息
     * @param accBalance double
     * @param polYearType int：险种类型
     *A：一年期特需医疗账户保险责任到期：
         账户利息=保险期内账户平均余额×约定年利息率
         其中：保险期内账户平均余额=每日账户余额之和/365
      B：一年期特需医疗账户保险责任客户提前退保：
         账户利息=保险期内账户平均余额×约定年利息率×实际承保天数/365
         其中：保险期内账户平均余额=每日账户余额之和/实际承保天数
      C：不按照帐户余额计息
         实行定额计息，业务人员在财务人员复核后，在帐户到期或退保时直接将利息金额写入。
     * @return double
     */
    public double getAccInterestYear(String grpPolNo,
                                            String accType,
                                            double rate)
    {
        double averageAccBalanceMoney = 0;
        LCPolSet tLCPolSet = new LCPolSet();
        if (accType.equals(BQ.ACCTYPE_FIXED)
            || accType.equals(BQ.ACCTYPE_GROUP))
        {
            StringBuffer sql = new StringBuffer();
            sql.append("select * ")
                .append("from LCPol ")
                .append("where grpPolNo = '").append(grpPolNo).append("' ")
                .append("   and polTypeFlag = '2' ");
            System.out.println(sql.toString());
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolSet = tLCPolDB.executeQuery(sql.toString());
            if(tLCPolSet.size() == 0)
            {
                System.out.println("险种号为" + grpPolNo + "的险种没有公共账户信息");
                return 0;
            }
        }
        else if (accType.equals(BQ.ACCTYPE_INSURED))
        {
            StringBuffer sql = new StringBuffer();
            sql.append("select * ")
                .append("from LCPol ")
                .append("where grpPolNo = '").append(grpPolNo).append("' ")
                .append("   and polTypeFlag = '")
                .append(BQ.POLTYPEFLAG_INSURED)
                .append("'  and appFlag = '")
                .append(BQ.APPFLAG_SIGN)
                .append("' ");
            System.out.println(sql.toString());
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolSet = tLCPolDB.executeQuery(sql.toString());
            if(tLCPolSet.size() == 0)
            {
                System.out.println("没险种号为" + grpPolNo + "的险种没有个人账户信息");
                return 0;
            }
        }

        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            averageAccBalanceMoney += getGrpAccBalaDayByDayYear(
                tLCPolSet.get(i).getPolNo(), accType);
        }
        return averageAccBalanceMoney * rate;
    }

    /**
     * 计算一年期险种账户余额利息：险种号为polNo的险种余额的利息
     * @param String grpPolNo
     * @return double
     */
    public double getGrpAccBalaDayByDayYear(String polNo,
                                               String accType)
    {
        int days = 365;
        //根据账户轨迹计算平均余额
        String sql = "  select * "
                     + "from LCInsureAccTrace "
                     + "where polNo = '" + polNo + "' "
                     + "    and insuAccNo = '"
                     + getInsuAccNo(accType) + "' "
                     + "order by days(makeDate)";
        System.out.println(sql);
        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB
                                                   .executeQuery(sql);

        //去除状态为temp的数据
        for(int i = 1; i <= tLCInsureAccTraceSet.size(); i++)
        {
            if(tLCInsureAccTraceSet.get(i).getState().equals("temp"))
            {
                tLCInsureAccTraceSet.removeRange(i, i);
                i--;
            }
        }
        double moneyDayByDay = 0;
        if(tLCInsureAccTraceSet.size() == 1)
        {
            moneyDayByDay += tLCInsureAccTraceSet.get(1).getMoney() * 365;
        }
        else if(tLCInsureAccTraceSet.size() > 1)
        {
            int intv;
            double totalMoney = tLCInsureAccTraceSet.get(1).getMoney();
            for (int i = 1; i < tLCInsureAccTraceSet.size(); i++)
            {
                //某期间账户余额 * 余额保持期间
                intv = PubFun.calInterval(
                        tLCInsureAccTraceSet.get(i).getMakeDate(),
                        tLCInsureAccTraceSet.get(i + 1).getMakeDate(),
                        "D");

                moneyDayByDay += totalMoney * intv;
                totalMoney += tLCInsureAccTraceSet.get(i + 1).getMoney();
            }
            intv = PubFun.calInterval(
                tLCInsureAccTraceSet.get(tLCInsureAccTraceSet.size()).
                getMakeDate(),
                getPayEndDate(polNo),
                "D");
            moneyDayByDay += totalMoney * intv;
        }
        return moneyDayByDay / days;
    }

    /**
     * 得到账户号码
     * @param accType String
     * @return String
     */
    private String getInsuAccNo(String accType)
    {
        String insuAccNo = (String) mInsuAccNoHashMap.get(accType);
        if(insuAccNo == null)
        {
            insuAccNo = CommonBL.getInsuAccNo(accType);
            mInsuAccNoHashMap.put(accType, insuAccNo);
        }

        return insuAccNo;
    }

    /**
     * 计算某帐户分类信息的利息
     * 一年期特需医疗账户保险责任到期：
     * 账户利息=保险期内账户平均余额×约定年利息率
     * 其中：保险期内账户平均余额=每日账户余额之和/365
     * @param schema LCInsureAccClassSchema
     * @param rate double：利率
     * @param endDate String：算费终止日期
     * @param days int，利率计算的间隔
     * @return double，利率
     */
    public double getOneAccClassInterest(LCInsureAccClassSchema schema,
                                         double rate, String endDate, int days)
    {
        double interest = 0;  //利息

        //得到该帐户分类的轨迹信息
        LCInsureAccTraceDB db = new LCInsureAccTraceDB();
        db.setPolNo(schema.getPolNo());
        db.setInsuAccNo(schema.getInsuAccNo());
        //因为在帐户轨迹表中理赔和保全操作没有录入PAYPLANCODE
//        db.setPayPlanCode(schema.getPayPlanCode());
        db.setState("0");
        LCInsureAccTraceSet set = db.query();
        if(set.size() == 0)
        {
            return 0;
        }

        double moneyDayByDay = 0;
        int intv;
        double totalMoney = set.get(1).getMoney();//账户在set.get(i).getMakeData()时的余额
        for (int i = 1; i < set.size(); i++)
        {
            //某期间账户余额 * 余额保持期间
            intv = PubFun.calInterval(
                set.get(i).getMakeDate(),
                set.get(i + 1).getMakeDate(),
                "D");

            moneyDayByDay += totalMoney * intv;
            totalMoney += set.get(i + 1).getMoney();
        }
        intv = PubFun.calInterval(
            set.get(set.size()).
            getMakeDate(),
            endDate,
            "D");
        moneyDayByDay += totalMoney * intv;
        interest = rate * moneyDayByDay / days;

        return interest;
    }

    /**
     * 查询保单失效日期
     * @param grpPolNo String
     * @return String
     */
    private String getPayEndDate(String polNo)
    {
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(polNo);
        LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
        tLCInsureAccSet = tLCInsureAccDB.query();
        if(tLCInsureAccSet.size() == 0)
        {
            System.out.println("没有查询到险种号为" + polNo + "的险种");
            return null;
        }

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(tLCInsureAccSet.get(1).getGrpContNo());
        if(!tLCGrpContDB.getInfo())
        {
            System.out.println("没有查询到保单号为" + polNo + "的保单");
            return null;
        }
        return tLCGrpContDB.getCInValiDate();
    }

    public static void main(String args[])
    {
        LCInsureAccClassSchema schema = new LCInsureAccClassSchema();
        schema.setPolNo("21000003063");
        schema.setInsuAccNo("100000");
        schema.setPayPlanCode("605101");

        AccountManage tAccountManage = new AccountManage();
        System.out.println(tAccountManage
                           .getOneAccClassInterest(schema, 0.1, "2006-1-1", 365));

//        AccountManage aAccountManage = new AccountManage();
//        LCInsureAccSchema aLCInsureAccSchema = new LCInsureAccSchema();
//        aLCInsureAccSchema.setPolNo("86110020040210000722");
//        aLCInsureAccSchema.setInsuAccNo("000006");
//        aLCInsureAccSchema.setRiskCode("211701");
//        aLCInsureAccSchema.setAccType("003");
//    aLCInsureAccSchema.setOtherNo("86110020040210000722");
//    aLCInsureAccSchema.setOtherType("1");
//        aLCInsureAccSchema.setContNo("00000000000000000000");
//        aLCInsureAccSchema.setGrpPolNo("86110020040220000042");
//        aLCInsureAccSchema.setInsuredNo("0000001881");
//        aLCInsureAccSchema.setAppntName("北京捷通投资咨询管理有限公司");
//        aLCInsureAccSchema.setSumPay("93000");
//        aLCInsureAccSchema.setInsuAccBala("111800");
//        aLCInsureAccSchema.setUnitCount("0");
//        aLCInsureAccSchema.setInsuAccGetMoney("0");
//        aLCInsureAccSchema.setSumPaym("0");
//        aLCInsureAccSchema.setFrozenMoney("0");
//        aLCInsureAccSchema.setBalaDate("2004-9-1");
//        aLCInsureAccSchema.setAccComputeFlag("4");
//        aLCInsureAccSchema.setManageCom("86110000");
//        aLCInsureAccSchema.setOperator("001");
//        aLCInsureAccSchema.setMakeDate("2004-10-10");
//        aLCInsureAccSchema.setModifyTime("2004-10-10");
//        double a = aAccountManage.getAccBalance(aLCInsureAccSchema,
//                                                "2004-10-11", "Y", "D");
//    (String aInsuAccNo,double aOriginAccBalance,String aStartDate,String aEndDate,String aIntervalType)


        //   double interest =  aAccountManage.getInterest("000006",118000,"2004-9-1","2004-10-11","1");
        //    double a = 0.72;
        //    double b = 0;
        //    double d ;
        //    double c ;
        //    b = 1.0/365.0;
        //    d = 1/365;
        //    LMLoanSchema tLMLoanSchema = new LMLoanSchema();
        //    LMLoanDB tLMLoanDB = new LMLoanDB();
        //    tLMLoanDB.setRiskCode("111301");
        //    tLMLoanDB.getInfo();
        //    tLMLoanSchema.setSchema(tLMLoanDB.getSchema());
        //    LOLoanSchema tLOLoanSchema = new LOLoanSchema();
        //    LOLoanDB tLOLoanDB = new LOLoanDB();
        //    tLOLoanDB.setEdorNo("86110020030420000338");
        //    tLOLoanDB.setPolNo("86110020030210000009");
        //    tLOLoanDB.getInfo();
        //    tLOLoanSchema.setSchema(tLOLoanDB.getSchema());
        //    VData tv = new VData();
        //    tv.add(tLOLoanSchema);
        //
        //
        //    AccountManage tAccountManage = new AccountManage();
        //    double tt=tAccountManage.getMultiInterest("1",tv,1000,"2000-01-01","2003-09-01");
        //    // b = tAccountManage.TransAccRate(a,"Y","C","D");
        //   // d = tAccountManage.getAccBalance("86110020020210000032","2003-05-01","Y","D");
        //   d = tAccountManage.getInterest(1000.0,"2002-10-10","2003-01-01",0.0072,"Y","S","D");
        //    LOLoanSchema tL = new LOLoanSchema();
        //    tL.set
        //    System.out.println("---d:"+d);
    }
    
    //by gzh 20110106
    /**
     * 万能险月结计算账户收益
     * @param cTransferData，包含：
     A.	GlobalInput对象，完整的登陆用户信息，参数名GI
     B.	一个子账户信息，参数名LCInsureAccClass
     C.	开始日期，参数名StartDate，结算周期起始的第一日
     D.	结束日期，参数名EndDate，结算周期末的次日
     E.	结算类型，参数名BalaType，1：月结算，2：保证收益结算
     F.	保单年度，参数名PolYear，月结算可不传
     G.	保单月度，参数名PolMonth，月结算可不传
     * @return
     */
    public MMap getUliBalaAccInterest(TransferData cTransferData)
    {
        LCInsureAccClassSchema tAccClassSchema
            = (LCInsureAccClassSchema)cTransferData.getValueByName("LCInsureAccClass");
        GlobalInput tGI = (GlobalInput)cTransferData.getValueByName("GI");
        String tStartDate = (String)cTransferData.getValueByName("StartDate");
        String tEndDate = (String)cTransferData.getValueByName("EndDate");
//        String tBalaType = (String)cTransferData.getValueByName("BalaType");
        String tLastBalaStr = (String)cTransferData.getValueByName("LastBala");
//        String tLastBalaGuratStr = (String)cTransferData.getValueByName("LastBalaGurat");
        

//        String oldMoneyStr = "1".equals(tBalaType) ? tLastBalaStr : tLastBalaGuratStr;
        String oldMoneyStr = tLastBalaStr ;
        if(oldMoneyStr == null || oldMoneyStr.equals(""))
        {
            mErrors.addOneError("请传入上一次结算金额");
            return null;
        }
        double oldMoney = Double.parseDouble(oldMoneyStr);

        double tRate = Double.parseDouble((String)cTransferData.getValueByName("Rate"));
        String tRateIntv = (String)cTransferData.getValueByName("RateIntv");
//        OriginRateType String 原始利率类型：年利率（"1")，月利率("2")，日利率("3")
        String OriginRateIntv = "1";
        if("M".equals(tRateIntv)){
        	OriginRateIntv = "2";
        }else if("D".equals(tRateIntv)){
        	OriginRateIntv = "3";
        }
        String tRateType = (String)cTransferData.getValueByName("RateType");
        //转换为日复利
        int monthLength = PubFun.monthLength(tStartDate);//获取结算月的天数
//		int yearLength = PubFun.isLeapYear(tStartDate) ? 366 : 365;
        int yearLength = 365;
        double tCalAccClassRate = TransAccRate(tRate, OriginRateIntv,
        		tRateType, "D", monthLength, yearLength);
        int tIntv = PubFun.calInterval(tStartDate, tEndDate, "D");
        double tmpIntrest = 1;
        for(int i=1;i<=tIntv;i++){
        	tmpIntrest = Arith.mul(tmpIntrest, Arith.add(tCalAccClassRate, 1));
        }
        double aAccClassInterest = 0.0;
        aAccClassInterest = Arith.add(Arith.mul(oldMoney, Arith.sub(
				tmpIntrest, 1)), aAccClassInterest);
        double resultMoney = Arith.add(oldMoney, aAccClassInterest);


        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
        String sql = "select * from LCInsureAccTrace "
                     + "where PayDate >= '" + tStartDate + "' "
                     + "   and paydate < '" + tEndDate + "' "
                     + "   and PolNo = '" + tAccClassSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + tAccClassSchema.getInsuAccNo() + "' "
                     + "   and PayPlanCode = '"
                     + tAccClassSchema.getPayPlanCode() + "' "
                     + "   and OtherType not in('1', '6') ";  //契约轨迹和月结轨迹不计算利息
        tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(sql);

        for(int i = 1; i <= tLCInsureAccTraceSet.size(); i++)
        {
            oldMoney += tLCInsureAccTraceSet.get(i).getMoney();
            tIntv = PubFun.calInterval(tLCInsureAccTraceSet.get(i).getPayDate(), tEndDate, "D");
            tmpIntrest = 1;
            for(int j=1;j<=tIntv;j++){
            	tmpIntrest = Arith.mul(tmpIntrest, Arith.add(tCalAccClassRate, 1));
            }
            aAccClassInterest = 0.0;
            aAccClassInterest = Arith.add(Arith.mul(tLCInsureAccTraceSet.get(i).getMoney(), Arith.sub(
    				tmpIntrest, 1)), aAccClassInterest);
            resultMoney = Arith.add(resultMoney, Arith.add(tLCInsureAccTraceSet.get(i).getMoney(), aAccClassInterest));
        }
        resultMoney = Arith.round(resultMoney, 2);
        double tLX = Arith.round(Arith.sub(resultMoney, oldMoney), 2);  //收益

        System.out.println("上月价值及本周期轨迹计算后总价值(未扣除相关费用):" + resultMoney);

        //更新轨迹信息
        LCInsureAccClassSchema tAccClassSchemaDelt = tAccClassSchema.getSchema();
        tAccClassSchemaDelt.setBalaDate(tEndDate);
        tAccClassSchemaDelt.setInsuAccBala(resultMoney);
        tAccClassSchemaDelt.setLastAccBala(tAccClassSchemaDelt.getInsuAccBala());
        tAccClassSchemaDelt.setOperator(tGI.Operator);
        tAccClassSchemaDelt.setModifyDate(PubFun.getCurrentDate());
        tAccClassSchemaDelt.setModifyTime(PubFun.getCurrentTime());

        Reflections ref = new Reflections();
        LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
        ref.transFields(traceSchema, tAccClassSchema);

        String tLimit = PubFun.getNoLimit(tAccClassSchemaDelt.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        traceSchema.setSerialNo(serNo);
        traceSchema.setMoneyType("LX");
        traceSchema.setMoney(tLX);
        traceSchema.setPayDate(tEndDate);
        PubFun.fillDefaultField(traceSchema);

        MMap tMMap = new MMap();
        tMMap.put(tAccClassSchemaDelt, SysConst.UPDATE);
        tMMap.put(traceSchema, SysConst.INSERT);

        return tMMap;
    }
    //获取利率
    public double queryRateFromInterest000001(String aTableName,
			String insuAccNo, String rateType, String intvType, String rateDate) {
		synchronized (AccountManage.class) {
			String key = aTableName + "#" + insuAccNo + "#" + rateType + "#"
					+ intvType;
			Vector specRateList = (Vector) rateCacheMap.get(key);
			if (specRateList == null || specRateList.size() == 0) {
				StringBuffer tSBql = new StringBuffer(128);
				tSBql.append("select startdate,enddate,rate from " + aTableName
						+ " where insuaccno = '");
				tSBql.append(insuAccNo);
				tSBql.append("' and RateType = '" + rateType
						+ "' and RateIntv = '" + intvType + "'");
				SSRS resultSSRS = new ExeSQL().execSQL(tSBql.toString());
				if (resultSSRS != null && resultSSRS.getMaxRow() != 0) {
					specRateList = new Vector();
					for (int i = 1; i <= resultSSRS.MaxRow; i++) {
						String resultArray[] = new String[3];
						resultArray[0] = resultSSRS.GetText(i, 1);
						resultArray[1] = resultSSRS.GetText(i, 2);
						resultArray[2] = resultSSRS.GetText(i, 3);
						
						specRateList.add(resultArray);
					}
				}
				rateCacheMap.put(key, specRateList);
			}
			if (specRateList != null) {
				for (int i = 0; i < specRateList.size(); i++) {
					String resultArray[] = (String[]) specRateList.get(i);
					String startDate = resultArray[0];
					String endDate = resultArray[1];
					String rateStr = resultArray[2];
					if (startDate.compareTo(rateDate) <= 0
							&& endDate.compareTo(rateDate) >= 0) {
						return Double.parseDouble(rateStr);
					}
				}
			}
			return 0;
		}
	}
    
    /**
     * 校验个险万能账户在结算期间内是否存在缓交区间
     * @param mLCInsureAccClassSchema
     * @param mStartDate
     * @param mEndDate
     * @return
     */
    public static boolean hasULIDefer(LCInsureAccClassSchema schema,String mStartDate,String mEndDate){
    	ExeSQL tExeSQL=new ExeSQL();
    	String strSQL="select 1 from LCContState where polno='"+schema.getPolNo()+"' and StateType='"+BQ.STATETYPE_ULIDEFER+"' "
    					+" and ((EndDate is null and StartDate<'"+mEndDate+"') "
    					+" or (StartDate>='"+mStartDate+"' and StartDate<'"+mEndDate+"')"
    					+" or (EndDate is not null and EndDate>='"+mStartDate+"' and EndDate<'"+mEndDate+"') "
    					+" or (StartDate<'"+mStartDate+"' and EndDate is not null and EndDate>='"+mEndDate+"') "
    					+")";
    	String polState=tExeSQL.getOneValue(strSQL);
    	if(null!=polState && !"".equals(polState)){
    		return true;
    	}
    	return false;
    }
    
    /**
     * 对账户的结算期间区拆分缓交区间和非缓交区间
     * 拆分到LMInsuAccRate，并借用RateType标记当前区间使用的结算类型：1-公布利率、2-保证利率
     * @param schema
     * 
     * @return
     */
    public static LMInsuAccRateSet getBalanceRate(LCInsureAccClassSchema schema,String mStartDate,
    		String mEndDate,String mBalaType){
    	LMInsuAccRateSet mLMInsuAccRateSet=new LMInsuAccRateSet();
    	String strSQL="select StartDate,EndDate,State from LCContState where polno='"+schema.getPolNo()+"' and StateType='"+BQ.STATETYPE_ULIDEFER+"' "
					+" and ((EndDate is null and StartDate<'"+mEndDate+"') "
					+" or (StartDate>='"+mStartDate+"' and StartDate<'"+mEndDate+"')"
					+" or (EndDate is not null and EndDate>='"+mStartDate+"' and EndDate<'"+mEndDate+"') "
					+" or (StartDate<'"+mStartDate+"' and EndDate is not null and EndDate>='"+mEndDate+"') "
					+") order by makedate desc fetch first 1 rows only";
    	SSRS mSSRS=new ExeSQL().execSQL(strSQL);
    	if(mSSRS.MaxRow==0){
    		System.out.println("获取保单缓交区间失败！获取sql："+strSQL);
    		return null;
    	}
    	String deStart=mSSRS.GetText(1, 1);//缓交的起始日期
    	String deEnd=mSSRS.GetText(1, 2);//缓交的截止日期
    	if(null==deEnd || "".equals(deEnd)){//没有截止日期就置一个超大日期，以便后续处理
    		deEnd="9999-1-1";
    	}
    	FDate tFDate=new FDate();
    	//1、缓交区间在结算区间之外，没有交集 （以上sql查出来不会有此情况）
    	if(tFDate.getDate(deEnd).before(tFDate.getDate(mStartDate)) 
    			|| tFDate.getDate(deStart).after(tFDate.getDate(deEnd))){
    		LMInsuAccRateSchema mLMInsuAccRateSchema=new LMInsuAccRateSchema();
    		mLMInsuAccRateSchema.setStartBalaDate(mStartDate);
    		mLMInsuAccRateSchema.setBalaDate(mEndDate);
    		mLMInsuAccRateSchema.setRateType("1");
    		mLMInsuAccRateSet.add(mLMInsuAccRateSchema);
    	}
    	String tempStart="";//每个结算区间的起止日期
		String tempEnd="";
		String tempBalaType="";
    	/*2、含部分交集--缓交起期或止期 在结算期间内 ，则拆分为至多3个结算区间
    	 */
    	if((tFDate.getDate(deStart).compareTo(tFDate.getDate(mStartDate))>=0
    			&& tFDate.getDate(deStart).compareTo(tFDate.getDate(mEndDate))<=0)
    		|| (tFDate.getDate(deEnd).compareTo(tFDate.getDate(mStartDate))>=0 
    			&& tFDate.getDate(deEnd).compareTo(tFDate.getDate(mEndDate))<=0)
    		){
    		//a)缓交起始日早于结算起始日-->缓交区间：结算起日至缓交终止日
    		if(tFDate.getDate(deStart).compareTo(tFDate.getDate(mStartDate))<0){
    			tempBalaType="2";
    			tempStart=mStartDate;
    			tempEnd=PubFun.calDate(deEnd, 1, "D", "");//因为账户结算时不包括截止日当天所以这里要+1天
//    			tempEnd=deEnd;
    			LMInsuAccRateSchema mLMInsuAccRateSchema1=new LMInsuAccRateSchema();
        		mLMInsuAccRateSchema1.setStartBalaDate(tempStart);
        		mLMInsuAccRateSchema1.setBalaDate(tempEnd);
        		mLMInsuAccRateSchema1.setRateType(tempBalaType);
        		mLMInsuAccRateSet.add(mLMInsuAccRateSchema1);
        		//a-2)缓交终止日早于结算截止日-->非缓交区间：缓交终止日 至 结算截止日
        		if(tFDate.getDate(tempEnd).compareTo(tFDate.getDate(mEndDate))<0){
        			tempBalaType="1";
        			tempStart=PubFun.calDate(deEnd, 1, "D", "");
//        			tempStart=deEnd;
        			tempEnd=mEndDate;
        			LMInsuAccRateSchema mLMInsuAccRateSchema2=new LMInsuAccRateSchema();
            		mLMInsuAccRateSchema2.setStartBalaDate(tempStart);
            		mLMInsuAccRateSchema2.setBalaDate(tempEnd);
            		mLMInsuAccRateSchema2.setRateType(tempBalaType);
            		mLMInsuAccRateSet.add(mLMInsuAccRateSchema2);
        		}
    		}//b)缓交起始日晚于或等于结算起始日
    		else if(tFDate.getDate(deStart).compareTo(tFDate.getDate(mStartDate))>=0){
    			//b-1)缓交日等于结算起始日
    			if(tFDate.getDate(deStart).compareTo(tFDate.getDate(mStartDate))==0){
    				//b-1-1)缓交截止日早于结算截止日
    				tempEnd=PubFun.calDate(deEnd, 1, "D", "");
    				if(tFDate.getDate(tempEnd).compareTo(tFDate.getDate(mEndDate))<0){
    					tempBalaType="2";
            			tempStart=deStart;
            			tempEnd=PubFun.calDate(deEnd, 1, "D", "");
            			LMInsuAccRateSchema mLMInsuAccRateSchema3=new LMInsuAccRateSchema();
                		mLMInsuAccRateSchema3.setStartBalaDate(tempStart);
                		mLMInsuAccRateSchema3.setBalaDate(tempEnd);
                		mLMInsuAccRateSchema3.setRateType(tempBalaType);
                		mLMInsuAccRateSet.add(mLMInsuAccRateSchema3);
                		
                		tempBalaType="1";
//            			tempStart=PubFun.calDate(deEnd, 1, "D", "");
                		tempStart=tempEnd;
            			tempEnd=mEndDate;
            			LMInsuAccRateSchema mLMInsuAccRateSchema4=new LMInsuAccRateSchema();
                		mLMInsuAccRateSchema4.setStartBalaDate(tempStart);
                		mLMInsuAccRateSchema4.setBalaDate(tempEnd);
                		mLMInsuAccRateSchema4.setRateType(tempBalaType);
                		mLMInsuAccRateSet.add(mLMInsuAccRateSchema4);
    				}//b-1-2)结算期间完全处于缓交状态
    				else if(tFDate.getDate(tempEnd).compareTo(tFDate.getDate(mEndDate))>=0){
    					tempBalaType="2";
            			tempStart=mStartDate;
            			tempEnd=mEndDate;
            			LMInsuAccRateSchema mLMInsuAccRateSchema4=new LMInsuAccRateSchema();
                		mLMInsuAccRateSchema4.setStartBalaDate(tempStart);
                		mLMInsuAccRateSchema4.setBalaDate(tempEnd);
                		mLMInsuAccRateSchema4.setRateType(tempBalaType);
                		mLMInsuAccRateSet.add(mLMInsuAccRateSchema4);
    				}
    				//b-2)缓交日晚于结算起始日
    			}else{
    				//第一段：前非缓交区间
					tempBalaType="1";
        			tempStart=mStartDate;
        			tempEnd=deStart;
        			LMInsuAccRateSchema mLMInsuAccRateSchema5=new LMInsuAccRateSchema();
            		mLMInsuAccRateSchema5.setStartBalaDate(tempStart);
            		mLMInsuAccRateSchema5.setBalaDate(tempEnd);
            		mLMInsuAccRateSchema5.setRateType(tempBalaType);
            		mLMInsuAccRateSet.add(mLMInsuAccRateSchema5);
            		//第二段：缓交区间
            		tempBalaType="2";
        			tempStart=deStart;
        			//日期较早的一个
    				if(tFDate.getDate(deEnd).compareTo(tFDate.getDate(mEndDate))<0){
    					tempEnd=PubFun.calDate(deEnd, 1, "D", "");//结算时不包括最后一天（例如结算7月1日至8月1日时，8月1日当天是不会结算的），所以这里要加1天。
    				}else{
    					tempEnd=mEndDate;
    				}
        			LMInsuAccRateSchema mLMInsuAccRateSchema6=new LMInsuAccRateSchema();
            		mLMInsuAccRateSchema6.setStartBalaDate(tempStart);
            		mLMInsuAccRateSchema6.setBalaDate(tempEnd);
            		mLMInsuAccRateSchema6.setRateType(tempBalaType);
            		mLMInsuAccRateSet.add(mLMInsuAccRateSchema6);
            		//第三段：后非缓交区间
            		if(tFDate.getDate(tempEnd).compareTo(tFDate.getDate(mEndDate))<0){
	            		tempBalaType="1";
	        			tempStart=PubFun.calDate(deEnd, 1, "D", "");
	        			tempEnd=mEndDate;
	        			LMInsuAccRateSchema mLMInsuAccRateSchema7=new LMInsuAccRateSchema();
	            		mLMInsuAccRateSchema7.setStartBalaDate(tempStart);
	            		mLMInsuAccRateSchema7.setBalaDate(tempEnd);
	            		mLMInsuAccRateSchema7.setRateType(tempBalaType);
	            		mLMInsuAccRateSet.add(mLMInsuAccRateSchema7);
            		}
    			}
    		}
    	}
    	//结算期间完全处理缓交状态
    	if(tFDate.getDate(deStart).compareTo(tFDate.getDate(mStartDate))<0
    		&&tFDate.getDate(deEnd).compareTo(tFDate.getDate(mEndDate))>0){
    		tempBalaType="2";
			tempStart=mStartDate;
			tempEnd=mEndDate;
			LMInsuAccRateSchema mLMInsuAccRateSchema8=new LMInsuAccRateSchema();
    		mLMInsuAccRateSchema8.setStartBalaDate(tempStart);
    		mLMInsuAccRateSchema8.setBalaDate(tempEnd);
    		mLMInsuAccRateSchema8.setRateType(tempBalaType);
    		mLMInsuAccRateSet.add(mLMInsuAccRateSchema8);
    	}
    	
    	
    	return mLMInsuAccRateSet;
    }
    
    
    /**
     * 校验个险附加万能账户在结算期间内是否存在失效区间
     * @param mLCInsureAccClassSchema
     * @param mStartDate
     * @param mEndDate
     * @return
     */
    public static boolean hasSUBULI(LCInsureAccClassSchema schema,String mStartDate,String mEndDate){
    	ExeSQL tExeSQL=new ExeSQL();
    	String strSQL=" select 1 from LCContState a, LCInsureAccClass b where a.polno=b.polno " 
    					+" and b.riskcode in (select riskcode from lmriskapp where risktype4='4' and subriskflag='S')"
    					+" and ((EndDate is null and StartDate<'"+mEndDate+"') "
    					+" or (StartDate>='"+mStartDate+"' and StartDate<'"+mEndDate+"')"
    					+" or (EndDate is not null and EndDate>='"+mStartDate+"' and EndDate<'"+mEndDate+"') "
    					+" or (StartDate<'"+mStartDate+"' and EndDate is not null and EndDate>='"+mEndDate+"') "
    					+" ) " 
    					+" and a.polno='"+schema.getPolNo()+"' with ur";
    	String polState=tExeSQL.getOneValue(strSQL);
    	if(null!=polState && !"".equals(polState)){
    		return true;
    	}
    	return false;
    }
    
    /**
     * 对账户的结算期间区拆分失效区间和有效区间
     * 拆分到LMInsuAccRate，并借用RateType标记当前区间使用的结算类型：1-公布利率、2-保证利率、3-0
     * @param schema
     * 
     * @return
     */
    public static LMInsuAccRateSet getBalanceRate1(LCInsureAccClassSchema schema,String mStartDate,
    		String mEndDate,String mBalaType){
    	LMInsuAccRateSet mLMInsuAccRateSet=new LMInsuAccRateSet();
    	String strSQL="select StartDate,EndDate,a.State from LCContState a, LCInsureAccClass b where a.polno=b.polno "
    				+" and b.riskcode in (select riskcode from lmriskapp where risktype4='4' and subriskflag='S')"
					+" and ((EndDate is null and StartDate<'"+mEndDate+"') "
					+" or (StartDate>='"+mStartDate+"' and StartDate<'"+mEndDate+"')"
					+" or (EndDate is not null and EndDate>='"+mStartDate+"' and EndDate<'"+mEndDate+"') "
					+" or (StartDate<'"+mStartDate+"' and EndDate is not null and EndDate>='"+mEndDate+"') "
					+") " 
					+" and a.polno='"+schema.getPolNo()+"' "
					+" order by a.makedate desc fetch first 1 rows only";
    	SSRS mSSRS=new ExeSQL().execSQL(strSQL);
    	if(mSSRS.MaxRow==0){
    		System.out.println("获取保单失效区间失败！获取sql："+strSQL);
    		return null;
    	}
    	String deStart=mSSRS.GetText(1, 1);//失效的起始日期
    	String deEnd=mSSRS.GetText(1, 2);//失效的截止日期
    	if(null==deEnd || "".equals(deEnd)){//没有截止日期就置一个超大日期，以便后续处理
    		deEnd="9999-1-1";
    	}
    	FDate tFDate=new FDate();
    	//1、失效区间在结算区间之外，没有交集 （以上sql查出来不会有此情况）
    	if(tFDate.getDate(deEnd).before(tFDate.getDate(mStartDate)) 
    			|| tFDate.getDate(deStart).after(tFDate.getDate(deEnd))){
    		LMInsuAccRateSchema mLMInsuAccRateSchema=new LMInsuAccRateSchema();
    		mLMInsuAccRateSchema.setStartBalaDate(mStartDate);
    		mLMInsuAccRateSchema.setBalaDate(mEndDate);
    		mLMInsuAccRateSchema.setRateType(mBalaType);
    		mLMInsuAccRateSet.add(mLMInsuAccRateSchema);
    	}
    	String tempStart="";//每个结算区间的起止日期
		String tempEnd="";
		String tempBalaType="";
    	/*2、含部分交集--失效起期或止期 在结算期间内 ，则拆分为至多3个结算区间
    	 */
    	if((tFDate.getDate(deStart).compareTo(tFDate.getDate(mStartDate))>=0
    			&& tFDate.getDate(deStart).compareTo(tFDate.getDate(mEndDate))<=0)
    		|| (tFDate.getDate(deEnd).compareTo(tFDate.getDate(mStartDate))>=0 
    			&& tFDate.getDate(deEnd).compareTo(tFDate.getDate(mEndDate))<=0)
    		){
    		//a)失效起始日早于结算起始日-->失效区间：结算起日至失效终止日
    		if(tFDate.getDate(deStart).compareTo(tFDate.getDate(mStartDate))<0){
    			tempBalaType="3";
    			tempStart=mStartDate;
    			tempEnd=PubFun.calDate(deEnd, 1, "D", "");//因为账户结算时不包括截止日当天所以这里要+1天
//    			tempEnd=deEnd;
    			LMInsuAccRateSchema mLMInsuAccRateSchema1=new LMInsuAccRateSchema();
        		mLMInsuAccRateSchema1.setStartBalaDate(tempStart);
        		mLMInsuAccRateSchema1.setBalaDate(tempEnd);
        		mLMInsuAccRateSchema1.setRateType(tempBalaType);
        		mLMInsuAccRateSet.add(mLMInsuAccRateSchema1);
        		//a-2)失效终止日早于结算截止日-->有效区间：失效终止日 至 结算截止日
        		if(tFDate.getDate(tempEnd).compareTo(tFDate.getDate(mEndDate))<0){
        			tempBalaType=mBalaType;
        			tempStart=PubFun.calDate(deEnd, 1, "D", "");
//        			tempStart=deEnd;
        			tempEnd=mEndDate;
        			LMInsuAccRateSchema mLMInsuAccRateSchema2=new LMInsuAccRateSchema();
            		mLMInsuAccRateSchema2.setStartBalaDate(tempStart);
            		mLMInsuAccRateSchema2.setBalaDate(tempEnd);
            		mLMInsuAccRateSchema2.setRateType(tempBalaType);
            		mLMInsuAccRateSet.add(mLMInsuAccRateSchema2);
        		}
    		}//b)失效起始日晚于或等于结算起始日
    		else if(tFDate.getDate(deStart).compareTo(tFDate.getDate(mStartDate))>=0){
    			//b-1)失效日等于结算起始日
    			if(tFDate.getDate(deStart).compareTo(tFDate.getDate(mStartDate))==0){
    				//b-1-1)失效截止日早于结算截止日
    				tempEnd=PubFun.calDate(deEnd, 1, "D", "");
    				if(tFDate.getDate(tempEnd).compareTo(tFDate.getDate(mEndDate))<0){
    					tempBalaType="3";
            			tempStart=deStart;
            			tempEnd=PubFun.calDate(deEnd, 1, "D", "");
            			LMInsuAccRateSchema mLMInsuAccRateSchema3=new LMInsuAccRateSchema();
                		mLMInsuAccRateSchema3.setStartBalaDate(tempStart);
                		mLMInsuAccRateSchema3.setBalaDate(tempEnd);
                		mLMInsuAccRateSchema3.setRateType(tempBalaType);
                		mLMInsuAccRateSet.add(mLMInsuAccRateSchema3);
                		
                		tempBalaType=mBalaType;
//            			tempStart=PubFun.calDate(deEnd, 1, "D", "");
                		tempStart=tempEnd;
            			tempEnd=mEndDate;
            			LMInsuAccRateSchema mLMInsuAccRateSchema4=new LMInsuAccRateSchema();
                		mLMInsuAccRateSchema4.setStartBalaDate(tempStart);
                		mLMInsuAccRateSchema4.setBalaDate(tempEnd);
                		mLMInsuAccRateSchema4.setRateType(tempBalaType);
                		mLMInsuAccRateSet.add(mLMInsuAccRateSchema4);
    				}//b-1-2)结算期间完全处于失效状态
    				else if(tFDate.getDate(tempEnd).compareTo(tFDate.getDate(mEndDate))>=0){
    					tempBalaType="3";
            			tempStart=mStartDate;
            			tempEnd=mEndDate;
            			LMInsuAccRateSchema mLMInsuAccRateSchema4=new LMInsuAccRateSchema();
                		mLMInsuAccRateSchema4.setStartBalaDate(tempStart);
                		mLMInsuAccRateSchema4.setBalaDate(tempEnd);
                		mLMInsuAccRateSchema4.setRateType(tempBalaType);
                		mLMInsuAccRateSet.add(mLMInsuAccRateSchema4);
    				}
    				//b-2)失效日晚于结算起始日
    			}else{
    				//第一段：前有效区间
					tempBalaType=mBalaType;
        			tempStart=mStartDate;
        			tempEnd=deStart;
        			LMInsuAccRateSchema mLMInsuAccRateSchema5=new LMInsuAccRateSchema();
            		mLMInsuAccRateSchema5.setStartBalaDate(tempStart);
            		mLMInsuAccRateSchema5.setBalaDate(tempEnd);
            		mLMInsuAccRateSchema5.setRateType(tempBalaType);
            		mLMInsuAccRateSet.add(mLMInsuAccRateSchema5);
            		//第二段：失效区间
            		tempBalaType="3";
        			tempStart=deStart;
        			//日期较早的一个
    				if(tFDate.getDate(deEnd).compareTo(tFDate.getDate(mEndDate))<0){
    					tempEnd=PubFun.calDate(deEnd, 1, "D", "");//结算时不包括最后一天（例如结算7月1日至8月1日时，8月1日当天是不会结算的），所以这里要加1天。
    				}else{
    					tempEnd=mEndDate;
    				}
        			LMInsuAccRateSchema mLMInsuAccRateSchema6=new LMInsuAccRateSchema();
            		mLMInsuAccRateSchema6.setStartBalaDate(tempStart);
            		mLMInsuAccRateSchema6.setBalaDate(tempEnd);
            		mLMInsuAccRateSchema6.setRateType(tempBalaType);
            		mLMInsuAccRateSet.add(mLMInsuAccRateSchema6);
            		//第三段：后有效区间
            		if(tFDate.getDate(tempEnd).compareTo(tFDate.getDate(mEndDate))<0){
	            		tempBalaType=mBalaType;
	        			tempStart=PubFun.calDate(deEnd, 1, "D", "");
	        			tempEnd=mEndDate;
	        			LMInsuAccRateSchema mLMInsuAccRateSchema7=new LMInsuAccRateSchema();
	            		mLMInsuAccRateSchema7.setStartBalaDate(tempStart);
	            		mLMInsuAccRateSchema7.setBalaDate(tempEnd);
	            		mLMInsuAccRateSchema7.setRateType(tempBalaType);
	            		mLMInsuAccRateSet.add(mLMInsuAccRateSchema7);
            		}
    			}
    		}
    	}
    	//结算期间完全处理失效状态
    	if(tFDate.getDate(deStart).compareTo(tFDate.getDate(mStartDate))<0
    		&&tFDate.getDate(deEnd).compareTo(tFDate.getDate(mEndDate))>0){
    		tempBalaType="3";
			tempStart=mStartDate;
			tempEnd=mEndDate;
			LMInsuAccRateSchema mLMInsuAccRateSchema8=new LMInsuAccRateSchema();
    		mLMInsuAccRateSchema8.setStartBalaDate(tempStart);
    		mLMInsuAccRateSchema8.setBalaDate(tempEnd);
    		mLMInsuAccRateSchema8.setRateType(tempBalaType);
    		mLMInsuAccRateSet.add(mLMInsuAccRateSchema8);
    	}
    	
    	
    	return mLMInsuAccRateSet;
    }
    
    /**
     * 复制一个TransferData对象的的数据
     * @param data
     * @return
     */
    private TransferData copyData(TransferData data){
    	TransferData tTransferData=new TransferData();
    	 Vector tVector = data.getValueNames();
         for(int i = 0; i < tVector.size(); i++)
         {
             Object tNameObject = null;
             Object tValueObject = null;
             try
             {
                 tNameObject = tVector.get(i);
                 tValueObject = data.getValueByName(tNameObject);
                 tTransferData.setNameAndValue(tNameObject, tValueObject);
             }
             catch(Exception ex)
             {
                 //对象无法转换为字符串，不需要做处理
             }
         }
         return tTransferData;
    }
    
    /**
	 * 利率转换函数 add by frost 2007-10-09 不能按每月30天，每年365天计算，而应该按照实际的天数转换
	 *
	 * @param OriginRate double 原始利率
	 * @param OriginRateType String 原始利率类型：年利率（"1")，月利率("2")，日利率("3")
	 * @param TransType String 复利转换("C")compound，单利转换("S")simple
	 * @param DestRateType String 转换后的利率类型 年利率（"Y")，月利率("M")，日利率("D")
	 * @param MonthLength int 交易日期所在的月份天数
	 * @param YearLength int 交易日期所在的年数天数
	 * @return double 例子：TransAccRate(0.48,"1","C","D",30,365) 将0.48的年复利，转换为日复利
	 */
	public static double TransAccRate(double OriginRate, String OriginRateType,
			String TransType, String DestRateType, int MonthLength, int YearLength) {
		double DestRate = 0;
		double aPower;

		// 判定OriginRateType，决定原始利率类型（年、月、日）
		if (OriginRateType.equals("1")) {
			OriginRateType = "Y";
		}
		if (OriginRateType.equals("2")) {
			OriginRateType = "M";
		}
		if (OriginRateType.equals("3")) {
			OriginRateType = "D";
		}
		// End add by Minim

		// 复利处理
		if (TransType.equals("C")) {
			// 年复利转换
			if (OriginRateType.equals("Y")) {
				// translate to year
				if (DestRateType.equals("Y")) {
					DestRate = OriginRate;
				}
				// translate to month
				else if (DestRateType.equals("M")) {
					aPower = 1.0 / 12.0;
					DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
				}
				// translate to day
				else if (DestRateType.equals("D")) {
					aPower = 1.0 / ((double)(YearLength));
					DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
				} else {
					System.out.println("-----CY no this DestRateType----");
				}
			}
			// 月复利转换
			else if (OriginRateType.equals("M")) {
				// translate to month
				if (DestRateType.equals("M")) {
					DestRate = OriginRate;
				}
				// translate to year
				else if (DestRateType.equals("Y")) {
					aPower = 12;
					DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
				}
				// translate to day
				else if (DestRateType.equals("D")) {
					aPower = 1.0 / ((double)(MonthLength));
					DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
				} else {
					System.out.println("-----CM no this DestRateType----");
				}
			}
			// 日复利转换
			else if (OriginRateType.equals("D")) {
				// translate to day
				if (DestRateType.equals("D")) {
					DestRate = OriginRate;
				}
				// translate to month
				else if (DestRateType.equals("M")) {
					aPower = (double)(MonthLength);
					DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
				}
				// translate to year
				else if (DestRateType.equals("Y")) {
					aPower = (double)(YearLength);
					DestRate = java.lang.Math.pow(1 + OriginRate, aPower) - 1;
				} else {
					System.out.println("-----CD no this DestRateType----");
				}
			} else {
				System.out.println("------C no this OriginRateType------");
			}
		}
		// 单利处理
		else if (TransType.equals("S")) {
			// 年单利转换
			if (OriginRateType.equals("Y")) {
				// translate to year
				if (DestRateType.equals("Y")) {
					DestRate = OriginRate;
				}
				// translate to month
				else if (DestRateType.equals("M")) {
					DestRate = OriginRate / 12;
				}
				// translate to day
				else if (DestRateType.equals("D")) {
					DestRate = OriginRate / ((double)(YearLength));
				} else {
					System.out.println("-----SY no this DestRateType----");
				}
			}
			// 月单利转换
			else if (OriginRateType.equals("M")) {
				// translate to month
				if (DestRateType.equals("M")) {
					DestRate = OriginRate;
				}
				// translate to year
				else if (DestRateType.equals("Y")) {
					DestRate = OriginRate * 12;
				}
				// translate to day
				else if (DestRateType.equals("D")) {
					DestRate = OriginRate / ((double)(MonthLength));
				} else {
					System.out.println("-----SM no this DestRateType----");
				}
			}
			// 日单利转换
			else if (OriginRateType.equals("D")) {
				// translate to day
				if (DestRateType.equals("D")) {
					DestRate = OriginRate;
				}
				// translate to month
				else if (DestRateType.equals("M")) {
					DestRate = OriginRate * ((double)(MonthLength));
				}
				// translate to year
				else if (DestRateType.equals("Y")) {
					DestRate = OriginRate * ((double)(YearLength));
				} else {
					System.out.println("-----SD no this DestRateType----");
				}
			} else {
				System.out.println("------S no this OriginRateType------");
			}
		} else {
			System.out.println("-------have not this TransType------");
		}
		// 返回转换后的实际利率
		return DestRate;
	}
//by gzh end
}
