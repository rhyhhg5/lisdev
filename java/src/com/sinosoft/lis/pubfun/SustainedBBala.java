package com.sinosoft.lis.pubfun;

import java.util.Vector;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMRiskFeeDB;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCInsureAccFeeTraceSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCInsureAccTraceSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPInsureAccSchema;
import com.sinosoft.lis.vschema.LCInsureAccClassSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMRiskFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 万能险保单账户结算 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author YangYalin
 * @version 1.3
 * @CreateDate：2007-04-28
 */
public class SustainedBBala
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGI = null;
    private LCInsureAccSchema mLCInsureAccSchema = null;
    private LPInsureAccSchema mLPInsureAccSchema = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mBalanceSeq = null;
    private String mStartDate = null;
    private String mEndDate = null;
    private int mPolYear = 1; //保单年度
    private int mPolMonth = 1; //保单月度

    private Reflections ref = new Reflections();
    private FDate tFDate = new FDate();
    private String mRateType = "";

    public MMap map = new MMap();
    
    private LCPolSchema mLCPolSchema = null;
    
    private LDCode1Schema mLDCode1Schema = new LDCode1Schema();

    public SustainedBBala()
    {
    }

    /**
     * submitData
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 业务处理不需要考虑保证利率，业务提供的利率不小于保证利率
     * @return boolean
     */
    public boolean dealData()
    {
                System.out.println("计算账户价值");
                MMap tMMap = new MMap();
          LCInsureAccClassSchema  tLCInsureAccClassSchema = new LCInsureAccClassSchema();
          LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
          tLCInsureAccClassDB.setContNo(mLCPolSchema.getContNo());
          tLCInsureAccClassDB.setPolNo(mLCPolSchema.getPolNo());
          tLCInsureAccClassDB.setRiskCode(mLCPolSchema.getRiskCode());
          LCInsureAccClassSet tLCInsureAccClassSet= new LCInsureAccClassSet();
          tLCInsureAccClassSet = tLCInsureAccClassDB.query();
          if(tLCInsureAccClassSet==null||tLCInsureAccClassSet.size()<=0){
              mErrors.addOneError("查询保单账户表出错");
              return false;        	  
          }
          if (!calPolYearMonth())
          {
              return false;
          }
          tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1);
                MMap tMMapAV = dealOneAccClass(tLCInsureAccClassSchema,
                                               "1", "1");
                if (tMMapAV == null)
                {
                    return false;
                }
                tMMap.add(tMMapAV);
                
            tMMap.add(updateAccInfo(tLCInsureAccClassSchema));

            VData data = new VData();
            data.add(tMMap);
            PubSubmit tSubmit = new PubSubmit();
            if (!tSubmit.submitData(data, ""))
            {
                buildError("InsureAccSubmit", "数据提交失败");
                return false;
            }

        return true;
    }

    /**
     * calPolYearMonth
     */
    private boolean calPolYearMonth()
    {
        String sql = "select year(date('" + mEndDate +
                     "') - CValidate), "
                     + "   month(date('" + mEndDate + "') - CValidate) + 1 "
                     + "from LCPol "
                     + "where PolNo = '" + mLCPolSchema.getPolNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        mPolYear = Integer.parseInt(tSSRS.GetText(1, 1));
        mPolMonth = 12;

        return true;
    }

    /**
     * calPolYearMonth
     */
    private boolean calContinuedbonusTime()
    {
        String sql = "select year(date('" + mStartDate +
                     "') - CValidate) + 1, "
                     + "   month(date('" + mStartDate + "') - CValidate) + 1 "
                     + "from LCPol "
                     + "where PolNo = '" + mLPInsureAccSchema.getPolNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        mPolYear = Integer.parseInt(tSSRS.GetText(1, 1));
        mPolMonth = 12;

        return true;
    }

    /**
     * updateAccInfo
     * 更新总账户信息
     * @return MMap
     */
    private MMap updateAccInfo(LCInsureAccClassSchema tLCInsureAccClassSchema)
    {
        MMap tMMap = new MMap();

        String sql = "update LCInsureAccClass a "
                     + "set (InsuAccBala, LastAccBala) "
                     + "   =(select sum(Money), sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo) "
                     + "where PolNo = '"
                     + tLCInsureAccClassSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + tLCInsureAccClassSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        //更新账户信息
        sql = "update LCInsureAcc a "
              + "set (InsuAccBala, LastAccBala, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(InsuAccBala), sum(LastAccBala), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LCInsureAccClass where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + tLCInsureAccClassSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + tLCInsureAccClassSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        
        //按轨迹更新账户管理费信息
        sql = "update LCInsureAccClassFee a " +
              "set Fee =(select sum(Fee) from LCInsureAccFeeTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo ) "
              + "where PolNo = '"
              + tLCInsureAccClassSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + tLCInsureAccClassSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        //更新账户管理费信息
        sql = "update LCInsureAccFee a " +
              "set (Fee, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(Fee), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LCInsureAccClassFee where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + tLCInsureAccClassSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + tLCInsureAccClassSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        return tMMap;
    }


    private MMap dealOneAccClass(LCInsureAccClassSchema tLCInsureAccClassSchema,
                                 String cBalaType, String cRatetype)
    {
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("StartDate", mStartDate);
        tTransferData.setNameAndValue("EndDate", mEndDate);
        tTransferData.setNameAndValue("RiskCode",
                                      tLCInsureAccClassSchema.getRiskCode());
        tTransferData.setNameAndValue("BalaType", cBalaType); //1：账户价值结算，2：保障价值计算
        tTransferData.setNameAndValue("RateType", cRatetype); //1：取公布利率，2：取保证利率
        tTransferData.setNameAndValue("PolYear", String.valueOf(mPolYear));
        tTransferData.setNameAndValue("PolMonth", String.valueOf(mPolMonth));

        VData data = new VData();
        data.add(mGI);
        data.add(tLCInsureAccClassSchema);
        data.add(tTransferData);

        MMap tMMapAV = balanceOneAccRate(data);
        return tMMapAV;
    }

    /**
     * balanceOneAccRate
     * 对子账户进行一个月的收益结算
     * @param cInputDate VData
     * @return MMap
     */
    public MMap balanceOneAccRate(VData cInputDate)
    {
        TransferData tTransferData = (TransferData) cInputDate
                                     .getObjectByObjectName("TransferData", 0);
        mGI = (GlobalInput) cInputDate
              .getObjectByObjectName("GlobalInput", 0);
        LCInsureAccClassSchema tLCInsureAccClassSchema
                = (LCInsureAccClassSchema) cInputDate
                  .getObjectByObjectName("LCInsureAccClassSchema", 0);
        if (tTransferData == null || mGI == null
            || tLCInsureAccClassSchema == null)
        {
            mErrors.addOneError("传入的数据不完整");
            return null;
        }

        tTransferData.setNameAndValue("LCInsureAccClass",
                                      tLCInsureAccClassSchema);
        tTransferData.setNameAndValue("GI", mGI);

        //计算管理费
        MMap tMMapFee = balanceOneAccManageFee(tLCInsureAccClassSchema, tTransferData);
        if (tMMapFee == null)
        {
            return null;
        }

        MMap tMMapAll = new MMap();
        tMMapAll.add(tMMapFee);

        return tMMapAll;
    }


    /**
     * 计算子帐户管理费，生成管理费轨迹和账户轨迹。
     * 取LMRiskFee对应账户的FeeTakePlace=5的管理费描述进行计算。
     * @param LCInsureAccClassSchema LCInsureAccClassSchema
     * @return MMap
     */
    private MMap balanceOneAccManageFee(LCInsureAccClassSchema
                                        cLCInsureAccClassSchema,
                                        TransferData tTransferData)
    {
        //计算持续奖金
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tLMRiskFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tLMRiskFeeDB.setFeeTakePlace("07"); //每月扣除
        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        if (tLMRiskFeeDB.mErrors.needDealError())
        {
            System.out.println(tLMRiskFeeDB.mErrors.getErrContent());
            mErrors.addOneError("查询持续奖金时出现异常");
            return null;
        }

        String tEndDate = (String) tTransferData.getValueByName("EndDate");
        if (tEndDate == null)
        {
            mErrors.addOneError("请传入截至日期");
            return null;
        }

        MMap tMMap = new MMap();

        //计算管理费
        for (int i = 1; i <= tLMRiskFeeSet.size(); i++)
        {
            Calculator tCalculator = new Calculator();
            tCalculator.setCalCode(tLMRiskFeeSet.get(i).getFeeCalCode());
            tCalculator.addBasicFactor("ContNo",
                                       cLCInsureAccClassSchema.getContNo());
            tCalculator.addBasicFactor("PolNo",
                                       cLCInsureAccClassSchema.getPolNo());
            tCalculator.addBasicFactor("InsuredNo",
                                       cLCInsureAccClassSchema.getInsuredNo());
            tCalculator.addBasicFactor("InsuAccNo",
                                       cLCInsureAccClassSchema.getInsuAccNo());
            tCalculator.addBasicFactor("PayPlanCode",
                                       cLCInsureAccClassSchema.getPayPlanCode());

//            LCPolDB tLCPolDB = new LCPolDB();
//            tLCPolDB.setPolNo(cLCInsureAccClassSchema.getPolNo());
//            tLCPolDB.getInfo();
            //新增变量Prem，支持常无忧万能的持续奖励计算。 added by huxl @20080616
            tCalculator.addBasicFactor("Prem", String.valueOf(mLCPolSchema.getPrem()));
            //新增变量Sex，保证计算风险保费的时候能够兼容保全的重算。 added by huxl @20080619
            tCalculator.addBasicFactor("Sex", mLCPolSchema.getInsuredSex());
            //新增变量Amnt，保证计算各险万能风险保费。 added by huxl @20080619
            tCalculator.addBasicFactor("Amnt", String.valueOf(mLCPolSchema.getAmnt()));
            
            tCalculator.addBasicFactor("CValiDate", String.valueOf(mLCPolSchema.getCValiDate()));
            
            //20120130 新增变量 持续奖金计算终止日期
            tCalculator.addBasicFactor("CalEndDate", tEndDate);
//          20101221 zhanggm 新增变量，附加险保额SubAmnt,计算附加重疾风险保费；如果存在风险加费比例，传入参数RiskRate
            String subRiskCode = ""; 
				subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
				LCPolDB subLCPolDB = new LCPolDB();
				subLCPolDB.setContNo(mLCPolSchema.getContNo());
				subLCPolDB.setMainPolNo(mLCPolSchema.getPolNo());
				subLCPolDB.setRiskCode(subRiskCode);
				LCPolSet subLCPolSet = subLCPolDB.query();
				if(subLCPolSet==null || subLCPolSet.size()==0)
				{
					continue;
				}
				else
				{
					LCPolSchema subLCPolSchema = subLCPolSet.get(1).getSchema();
					String tSubAmnt = String.valueOf(subLCPolSchema.getAmnt());
					tCalculator.addBasicFactor("SubAmnt", tSubAmnt);
					
					String sql = "select Rate from LCPrem where PolNo = '" + subLCPolSchema.getPolNo() 
						+ "' and PayPlanCode like '00000%' order by PayPlanCode desc " ;
					String tRiskRate = new ExeSQL().getOneValue(sql);
					if(tRiskRate==null || tRiskRate.equals(""))
					{
						tCalculator.addBasicFactor("RiskRate", "0");
					}
					else
					{
						tCalculator.addBasicFactor("RiskRate", tRiskRate);
					}
				}

            setCalculatorFactor(tTransferData, tCalculator);

            AccountManage tAccountManage = new AccountManage(mLCPolSchema);
            String tInsuredAppAge = tAccountManage
                                    .getInsuredAppAge(mStartDate,
                    cLCInsureAccClassSchema);
            if (tInsuredAppAge == null)
            {
                mErrors.copyAllErrors(tAccountManage.mErrors);
                return null;
            }
            tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);

            double fee = Math.abs(PubFun.setPrecision(
                    Double.parseDouble(tCalculator.calculate()), "0.00"));
            if (tCalculator.mErrors.needDealError())
            {
                System.out.println(tCalculator.mErrors.getErrContent());
                mErrors.addOneError("持续奖金计算出错");
                return null;
            }

            //管理费轨迹
            LCInsureAccFeeTraceSchema schema = new LCInsureAccFeeTraceSchema();
            ref.transFields(schema, cLCInsureAccClassSchema);

            String tLimit = PubFun.getNoLimit(schema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
            
            if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
            	schema.setPayPlanCode(subRiskCode);
			}
            schema.setSerialNo(serNo);
            schema.setFeeCode(tLMRiskFeeSet.get(i).getFeeCode());
            schema.setFee(fee);
            schema.setOtherNo(this.getBalanceSequenceNo());
            schema.setOtherType("6"); //月结算
            schema.setFeeRate(tLMRiskFeeSet.get(i).getFeeValue());
            schema.setPayDate(tEndDate);
            schema.setState("0");
            PubFun.fillDefaultField(schema);

            String moneyType = CommonBL.getCodeName("accmanagefee",
                    schema.getFeeCode());
            if (moneyType == null)
            {
                mErrors.addOneError("请先描述管理费的财务类型");
                return null;
            }
            schema.setMoneyType(moneyType);
            //管理费的账户轨迹
            LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
            ref.transFields(traceSchema, schema);

            String tLimit2 = PubFun.getNoLimit(traceSchema.getManageCom());
            String serNo2 = PubFun1.CreateMaxNo("SERIALNO", tLimit2);
            traceSchema.setSerialNo(serNo2);
            //只有持续奖金大于零的时候才进账户轨迹表。
            if (fee > 0 && moneyType.equals("B"))
            {
                traceSchema.setMoney(Math.abs(schema.getFee()));
                tMMap.put(traceSchema, SysConst.INSERT);
            } 
            //更新账户分类轨迹
            cLCInsureAccClassSchema.setInsuAccBala(cLCInsureAccClassSchema.
                    getInsuAccBala()
                    + traceSchema.getMoney());
            cLCInsureAccClassSchema.setLastAccBala(cLCInsureAccClassSchema.
                    getInsuAccBala());
        }

        return tMMap;
    }


    private void setCalculatorFactor(TransferData tTransferData,
                                     Calculator tCalculator)
    {
        Vector tVector = tTransferData.getValueNames();
        for (int i = 0; i < tVector.size(); i++)
        {
            Object tNameObject = null;
            Object tValueObject = null;
            try
            {
                tNameObject = tVector.get(i);
                tValueObject = tTransferData.getValueByName(tNameObject);
                tCalculator.addBasicFactor((String) tNameObject,
                                           (String) tValueObject);
            } catch (Exception ex)
            {
                //对象无法转换为字符串，不需要做处理
            }
        }
    }

    /**
     * 得到结算序号
     * @return String
     */
    public String getBalanceSequenceNo()
    {
        if (mBalanceSeq == null)
        {
            String[] dateArr = PubFun.getCurrentDate().split("-");
            String date = dateArr[0] + dateArr[1] + dateArr[2];
            mBalanceSeq = date
                          + PubFun1.CreateMaxNo("BalaSeq" + date, 10);
        }

        return mBalanceSeq;
    }

    /**
     * 传递参数
     * 准备账户号码和账户结算日期
     * @param cInputData VData
     * @return boolean
     */
    public boolean getInputData(VData cInputData)
    {
        mGI = (GlobalInput) cInputData
              .getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) cInputData
                          .getObjectByObjectName("TransferData", 0);
        if (tf != null)
        {
        	mLCPolSchema = (LCPolSchema) tf.getValueByName("LCPolSchema");
        	mLDCode1Schema = (LDCode1Schema) tf.getValueByName("LDCode1Schema");
        	mStartDate = (String)tf.getValueByName("mStartDate");
        	mEndDate = (String)tf.getValueByName("mEndDate");
        }

        if (mGI == null)
        {
            buildError("getInputData", "请传入操作员信息");
            return false;
        }       
        return true;
    }

    /**
     * 错误构建方法
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "InsuAccBala";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * 锁定动作
     * @param polBalaCount
     * @return MMap
     */
    private MMap lockBalaCount(String polBalaCount)
    {
        MMap tMMap = null;
        /**万能险月结算"UJ"*/
        String tLockNoType = "UJ";
        /**锁定有效时间（秒）*/
        String tAIS = "600";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", polBalaCount);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGI);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }

    public static void main(String arg[])
    {

		System.out.println(PubFun.calInterval("2011-8-18","2012-8-17", "Y"));
		System.out.println(PubFun.calInterval("2011-8-18","2012-8-18", "Y"));
		System.out.println(PubFun.calInterval("2011-8-18","2012-8-19", "Y"));
    }
}
