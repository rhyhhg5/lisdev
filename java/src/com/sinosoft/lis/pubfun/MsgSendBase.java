/*
 * <p>ClassName: BqCalBase </p>
 * <p>Description: 保全计算基础要素类文件 </p>
 * <p>Description: 所有方法均是为内部成员变量存取值，set开头为存，get开头为取,方式源于CalBase,要素描述属于保全 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: TJJ
 * @CreateDate：2002-10-02
 */
package com.sinosoft.lis.pubfun;

public class MsgSendBase
{
    // @Field
    /** 保单号 */
    private String Contno;
    /** 投保人号 */
    private String AppntNo;
	/** 投保人姓名 */
    private String AppntName;
    /** 被保人号 */
    private String InsuredNo;
    /** 被保险人姓名 */
    private String InsuredName;
    /** 被保险人性别 */
    private String InsuredSex;
    /**被保险人生日*/
    private String InsuredBirthday;
    /**被保险人年龄*/
    private String InsuredAppAge;
    /** 投保人性别 */
    private String AppntSex;
    /** 投保人生日 */
    private String AppntBirthDay;
    /** 投保人年龄 */
    private String AppntAppAge;
    /** 险种号 */
    private String RiskCode;
    /** 险种名称 */
    private String RiskName;
    /** 申请日期 */
    private String ApplyDate;
    /** 申请人类型 */
    private String ApplyNoType;
    /** 业务类型 */
    private String BusinessType ;
    /** 电话号码  */
    private String MobilPhone;
    /** 机构代码  */
    private String ManageCom;
    /** 年度 */
    private String PolYear;
    /** 操作者 */
    private String Operator;
    
    private String EdorName;
    
	/** 金额 **/
    private String Prem;
    /** 银行账户号码 */
    private String BankAccNo;
    /** 特约类型 ：为了支持所有的短信操作，均需要将本模块的操作标示符存于本字段*/
    private String SpecType;
    
    private String EndorsementNo;
    
    /**身份证号**/
    private String IDNo;
    /**医院名称**/
    private String HospitalName;
    /**费用总额**/
    private String SumFee;
    /**个人自付**/
    private String SelfAmnt;
    /**银行名称**/
    private String BankName;
    
    
    
    public String getIDNo() {
		return IDNo;
	}
	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}
	public String getHospitalName() {
		return HospitalName;
	}
	public void setHospitalName(String hospitalName) {
		HospitalName = hospitalName;
	}
	public String getSumFee() {
		return SumFee;
	}
	public void setSumFee(String sumFee) {
		SumFee = sumFee;
	}
	public String getSelfAmnt() {
		return SelfAmnt;
	}
	public void setSelfAmnt(String selfAmnt) {
		SelfAmnt = selfAmnt;
	}
	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	public String getEndorsementNo() {
		return EndorsementNo;
	}
	public void setEndorsementNo(String endorsementNo) {
		EndorsementNo = endorsementNo;
	}
	public String getEdorName() {
    	return EdorName;
    }
    public void setEdorName(String edorName) {
    	EdorName = edorName;
    }
    public String getBankAccNo() {
		return BankAccNo;
	}
	public void setBankAccNo(String bankAccNo) {
		BankAccNo = bankAccNo;
	}
	public String getSpecType() {
		return SpecType;
	}
	public void setSpecType(String specType) {
		SpecType = specType;
	}
	public String getPrem() {
		return Prem;
	}
	public void setPrem(String prem) {
		Prem = prem;
	}
	public String getAppntNo() {
    	return AppntNo;
    }
    public void setAppntNo(String appntNo) {
    	AppntNo = appntNo;
    }

	public String getOperator() {
		return Operator;
	}
	public void setOperator(String operator) {
		Operator = operator;
	}
	public String getManageCom() {
		return ManageCom;
	}
	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getMobilPhone() {
		return MobilPhone;
	}
	public void setMobilPhone(String mobilPhone) {
		MobilPhone = mobilPhone;
	}
	public String getContno() {
		return Contno;
	}
	public void setContno(String contno) {
		Contno = contno;
	}
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getInsuredNo() {
		return InsuredNo;
	}
	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}
	public String getInsuredName() {
		return InsuredName;
	}
	public void setInsuredName(String insuredName) {
		InsuredName = insuredName;
	}
	public String getInsuredSex() {
		return InsuredSex;
	}
	public void setInsuredSex(String insuredSex) {
		InsuredSex = insuredSex;
	}
	public String getInsuredBirthday() {
		return InsuredBirthday;
	}
	public void setInsuredBirthday(String insuredBirthday) {
		InsuredBirthday = insuredBirthday;
	}
	public String getInsuredAppAge() {
		return InsuredAppAge;
	}
	public void setInsuredAppAge(String insuredAppAge) {
		InsuredAppAge = insuredAppAge;
	}
	public String getAppntSex() {
		return AppntSex;
	}
	public void setAppntSex(String appntSex) {
		AppntSex = appntSex;
	}

	public String getAppntBirthDay() {
		return AppntBirthDay;
	}
	public void setAppntBirthDay(String appntBirthDay) {
		AppntBirthDay = appntBirthDay;
	}
	public String getAppntAppAge() {
		return AppntAppAge;
	}
	public void setAppntAppAge(String appntAppAge) {
		AppntAppAge = appntAppAge;
	}
	public String getRiskCode() {
		return RiskCode;
	}
	public void setRiskCode(String riskCode) {
		RiskCode = riskCode;
	}
	public String getRiskName() {
		return RiskName;
	}
	public void setRiskName(String riskName) {
		RiskName = riskName;
	}
	public String getApplyDate() {
		return ApplyDate;
	}
	public void setApplyDate(String applyDate) {
		ApplyDate = applyDate;
	}
	public String getApplyNoType() {
		return ApplyNoType;
	}
	public void setApplyNoType(String applyNoType) {
		ApplyNoType = applyNoType;
	}
	public String getBusinessType() {
		return BusinessType;
	}
	public void setBusinessType(String businessType) {
		BusinessType = businessType;
	}
	public String getPolYear() {
		return PolYear;
	}
	public void setPolYear(String polYear) {
		PolYear = polYear;
	}
  
}
