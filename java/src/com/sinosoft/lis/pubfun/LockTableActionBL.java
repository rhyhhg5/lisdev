/**
 * 2008-9-9
 */
package com.sinosoft.lis.pubfun;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.sinosoft.lis.db.LockTableDB;
import com.sinosoft.lis.schema.LockTableSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class LockTableActionBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    /** 锁定关键字 */
    private String mLockNoKey = null;

    /** 锁定关键字类型 */
    private String mLockNoType = null;

    /** 与上次锁定有效时间间隔，单位：秒 */
    private long mAvailabilityIntervalSecond = 0;
    
    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    public LockTableActionBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mLockNoKey = (String) mTransferData.getValueByName("LockNoKey");
        if (mLockNoKey == null)
        {
            buildError("getInputData", "参数[LockNoKey]不存在。");
            return false;
        }

        mLockNoType = (String) mTransferData.getValueByName("LockNoType");
        if (mLockNoType == null)
        {
            buildError("getInputData", "参数[mLockNoType]不存在。");
            return false;
        }

        String tAIS = (String) mTransferData
                .getValueByName("AvailabilityIntervalSecond");
        if (tAIS != null)
        {
            try
            {
                mAvailabilityIntervalSecond = Long.parseLong(tAIS);
            }
            catch (Exception e)
            {
                buildError("getInputData",
                        "参数[AvailabilityIntervalSecond]应该为整数。");
                return false;
            }
        }

        return true;
    }

    private boolean dealData()
    {
        MMap tMMap = null;

        // 进行锁定
        tMMap = lockAction();
        if (tMMap == null)
        {
            return false;
        }
        mMap.add(tMMap);
        // ---------------------

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    /**
     * 根据关键字，关键字类型，记录锁定轨迹。
     * @return
     */
    private MMap lockAction()
    {
        MMap tMMap = new MMap();

        // 获取上次锁定轨迹
//        LockTableSchema tLockTableSchema = getLastLockTrace();
        // -----------------------------

        MMap tTmpMap = new MMap();
        
/* 之前的先查后删的方式会有时间差,同样会导致并发,修改为新的并发锁逻辑:删除大于并发时间的记录,通过INSERT来控制并发. add by xp 2010-5-13  
	注掉之前的相关代码
        if (tLockTableSchema != null)
        {
            if (!checkLockAvailability(tLockTableSchema))
            {
                // 删除锁定轨迹
                tTmpMap = delLockTrace(tLockTableSchema);
                if (tTmpMap == null)
                {
                    return null;
                }
                tMMap.add(tTmpMap);
                // ----------------------
            }
            else
            {
                buildError("lockAction", "为了避免重复提交，现控制同一数据处理在"
                        + mAvailabilityIntervalSecond + "秒不得重复操作。");
                return null;
            }
        }*/
        
        //添加删除记录  add by xp 2010-5-13  
        String DelSQL="Delete From LockTable Where Notype='"+mLockNoType+"' And Nolimit='"+mLockNoKey
                     +"' And timestampdiff(2, char( timestamp('" + mCurrentDate + "'||' '||'" + mCurrentTime 
                     + "' )	- 	timestamp(char(makedate)||' '||maketime)))>"+mAvailabilityIntervalSecond;        
        tTmpMap.put(DelSQL,"DELETE");

        //生成锁定记录
        tTmpMap.add(createLockTrace());
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        // -------------------------

        return tMMap;
    }

    /**
     * 获取该关键字最近一次锁定轨迹。
     * @return 无锁定轨迹，则返回 null
     */
    private LockTableSchema getLastLockTrace()
    {
        LockTableDB tLockTableDB = new LockTableDB();
        tLockTableDB.setNoType(mLockNoType);
        tLockTableDB.setNoLimit(mLockNoKey);

        if (!tLockTableDB.getInfo())
        {
            return null;
        }

        return tLockTableDB.getSchema();
    }

    /**
     * 校验锁定是否有效。
     * @param cLockTableSchema
     * @return 有效，返回：true；无效，返回：false；
     */
    private boolean checkLockAvailability(LockTableSchema cLockTableSchema)
    {
        String date = cLockTableSchema.getMakeDate();
        String time = cLockTableSchema.getMakeTime();

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.SIMPLIFIED_CHINESE);

        Date bef = null;
        Date now = new Date();

        try
        {
            bef = df.parse(date + " " + time);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        long between = (now.getTime() - bef.getTime()) / 1000;// 除以1000是为了转换成秒

        return (0 == mAvailabilityIntervalSecond || between < mAvailabilityIntervalSecond);
    }

    /**
     * 删除锁定轨迹。
     * @param cLockTableSchema
     * @return
     */
    private MMap delLockTrace(LockTableSchema cLockTableSchema)
    {
        MMap tMMap = new MMap();

        tMMap.put(cLockTableSchema, SysConst.DELETE);

        return tMMap;
    }

    /**
     * 创建锁定轨迹
     * @return
     */
    private MMap createLockTrace()
    {
        MMap tMMap = new MMap();

        LockTableSchema tLockTableSchema = new LockTableSchema();

        tLockTableSchema.setNoType(mLockNoType);
        tLockTableSchema.setNoLimit(mLockNoKey);

        tLockTableSchema.setMakeDate(PubFun.getCurrentDate());
        tLockTableSchema.setMakeTime(PubFun.getCurrentTime());

        tMMap.put(tLockTableSchema, SysConst.INSERT);

        return tMMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LockTableActionBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
