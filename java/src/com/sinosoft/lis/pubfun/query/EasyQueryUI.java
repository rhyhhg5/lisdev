package com.sinosoft.lis.pubfun.query;

import java.util.Map;

/**
 * <p>Title: 简易查询</p>
 * <p>Description: 会把编码自动转为汉字，仅用于页面查询</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class EasyQueryUI
{
    /** 封装了EasyQueryBL类 */
    private EasyQueryBL mEasyQueryBL = null;

    /**
     * 构造函数
     * @param sql String
     * @param codeTypeParams CodeTypeParams
     */
    public EasyQueryUI(String sql, String start, String pageRowNum, Map paramMap)
    {
        mEasyQueryBL = new EasyQueryBL(sql, start, pageRowNum, paramMap);
    }

    /**
     * 执行查询操作
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mEasyQueryBL.query())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回查询结果
     * @return String
     */
    public String getResult()
    {
        return mEasyQueryBL.getResult();
    }

    public static void main(String[] args)
    {
        String sql = "select '','','', case when a.msgsend = '1' then '　已发送' else '　未发送' end, a.sendtype, a.senddate, a.msgtopic, a.MsgNo, '', char(rownumber() over())  from LOMsgInfo a where 1=1 order by MsgNo";
        String codeType = "0|0|station^";
        Map params = ParseParams.parseCodeType(codeType);
        EasyQueryUI tEasyQueryUI = new EasyQueryUI(sql, "1", "10", params);

        long start = System.currentTimeMillis();
        if (tEasyQueryUI.submitData())
        {
            System.out.println(tEasyQueryUI.getResult());
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
}
