package com.sinosoft.lis.pubfun.query;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 缓存大小计算类</p>
 * <p>Description: 得到不同数据类型占用内存空间大小</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class CodeQuery
{
    /** 代码类型 */
    private String codeType = null;

    /** 代码类型缓存 */
    private CodeCache codeCache = CodeCache.getInstance();

    /**
     * 根据代码得到代码名称，缓存中找不到才去库中找
     * @param code String
     * @return String
     */
    public String getCodeName(String codeType, String code)
    {
        this.codeType = codeType;
        String codeName = null;
        if (codeCache.containsCodeNode(codeType))
        {
            codeName = getCodeNameFromCache(code);
        }
        else
        {
            codeName = getCodeNameFromDb(code);
        }
        if (codeName == null)
        {
            return code;
        }
        return codeName;
    }

    /**
     * 从缓存中得到代码名称
     * @param code String
     * @return String
     */
    private String getCodeNameFromCache(String code)
    {
        CodeNode codeNode = codeCache.get(codeType);
        if (codeNode == null)
        {
            return null;
        }
        return codeNode.getCodeName(code);
    }

    /**
     * 从数据库中得到代码名称
     * @param code String
     * @return String
     */
    private String getCodeNameFromDb(String code)
    {
        if (!query())
        {
            return null;
        }
        return getCodeNameFromCache(code);
    }

    /**
     * 查询代码名称
     * @return boolean
     */
    private boolean query()
    {
        CodeNode node = new CodeNode(codeType);
        String sql = getSql();
        Connection conn = DBConnPool.getConnection();
        try
        {
            PreparedStatement pstmt = conn.prepareStatement(sql,
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                String code = StrTool.cTrim(rs.getString(1));
                String codeName = StrTool.cTrim(rs.getString(2));
                node.add(code, codeName);
            }
            rs.close();
            pstmt.close();
            conn.close();
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
            return false;
        }
        codeCache.add(node);
        return true;
    }

    /**
     * 得到查询代码的SQL
     * @return String
     */
    private String getSql()
    {
        LDCodeSchema tLDCodeSchema = new LDCodeSchema();
        tLDCodeSchema.setCodeType(codeType);

        //这里用"'1'"和"1"是为了兼容CodeQueryBL中写的mCodeCondition.substring(1, (mCodeCondition.length() - 1)).trim()
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("conditionField", "'1'");
        tTransferData.setNameAndValue("codeCondition", "1");

        VData tData = new VData();
        tData.add(tLDCodeSchema);
        tData.add(new GlobalInput());
        tData.add(tTransferData);

        CodeQueryBL codeQueryBL = new CodeQueryBL();
        codeQueryBL.submitData(tData, "");
        return codeQueryBL.getSql();
    }
}
