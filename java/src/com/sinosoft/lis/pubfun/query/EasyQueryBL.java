package com.sinosoft.lis.pubfun.query;

import java.util.Map;
import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 简易查询</p>
 * <p>Description: 会把编码自动转为汉字，仅用于页面查询</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class EasyQueryBL
{
    /** 每次最多查询的记录数 */
    private int maxRowNum;

    /** 执行查询的SQL */
    private String sql = null;

    /** 查询记录的起始位置 */
    private int start = 0;

    /** 代码类型参数 */
    private Map paramMap = null;

    /** 存放查询结果 */
    private StringBuffer buffer = new StringBuffer();

    private CodeQuery codeQuery = new CodeQuery();

    /**
     * 构造函数
     * @param sql String
     * @param start int
     * @param pageRowNum int
     * @param paramMap Map
     */
    public EasyQueryBL(String sql, int start, int pageRowNum, Map paramMap)
    {
        this.maxRowNum = pageRowNum * SysConst.MAXMEMORYPAGES;
        this.sql = StrTool.GBKToUnicode(sql);
        this.start = start - 1; //以0开始的索引
        this.paramMap = paramMap;
    }

    /**
     * 构造函数
     * @param sql String
     * @param start String
     * @param pageRowNum String
     * @param paramMap Map
     */
    public EasyQueryBL(String sql, String start, String pageRowNum, Map paramMap)
    {
        this(sql, Integer.parseInt(start), Integer.parseInt(pageRowNum), paramMap);
    }

    /**
     * 执行查询操作
     * @return boolean
     */
    public boolean query()
    {
        Connection conn = DBConnPool.getConnection();
        try
        {
            //System.out.println("EasyQuery: " + sql);
            PreparedStatement pstmt = conn.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = pstmt.executeQuery();
            ResultSetMetaData rsmd = pstmt.getMetaData();
            rs.last();
            int rowCount = rs.getRow();
            int rowNum = 0;
            rs.beforeFirst();
            while ((rowNum < start) && rs.next()) //把游标移动到起始位置
            {
                rowNum ++;
            }
            int end = start + maxRowNum;  //当前查询最后记录的位置
            while ((rowNum < end) && rs.next())
            {
                buffer.append(SysConst.RECORDSPLITER);
                dealOneRow(rsmd, rs);
                rowNum ++;
            }
            if (rowNum > 0)
            {
                buffer.insert(0, "0" + SysConst.PACKAGESPILTER + rowCount);
            }
            else
            {
                buffer.append("100|未查询到相关数据！");
            }
            rs.close();
            pstmt.close();
            conn.close();
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
            System.out.println("查询语句错误!");
            return false;
        }
        return true;
    }

    /**
     * 返回查询结果
     * @return String
     */
    public String getResult()
    {
        return buffer.toString();
    }

    /**
     * 处理一条记录
     * @param rsmd ResultSetMetaData
     * @param rs ResultSet
     * @throws SQLException
     */
    private void dealOneRow(ResultSetMetaData rsmd, ResultSet rs)
            throws SQLException
    {
        for (int i = 1; i <= rsmd.getColumnCount(); i++)
        {
            int columnType = rsmd.getColumnType(i);
            String value = getValue(i, columnType, rs);
            dealOneCol(i, value);
            if (i < rsmd.getColumnCount())
            {
                buffer.append(SysConst.PACKAGESPILTER);
            }
        }
    }

    /**
     * 处理一个字段
     * @param col int
     * @param value String
     */
    private void dealOneCol(int col, String value)
    {
        String colValue = value;
        CodeTypeParam param = (CodeTypeParam) paramMap.get(String.valueOf(col));
        if (param != null)
        {
            String colType = param.getColType();
            if ((colType != null) &&
                    (colType.equals("0") || colType.equals("3")))
            {
                colValue = codeQuery.getCodeName(param.getCodeType(), colValue);
            }
        }
        buffer.append(colValue);
    }

    /**
     * 把所有查询结果都转为String类型的
     * @param col int
     * @param columnType int
     * @param rs ResultSet
     * @throws SQLException
     * @return String
     */
    private String getValue(int col, int columnType, ResultSet rs)
            throws SQLException
    {
        if ((columnType == Types.CHAR) || (columnType == Types.VARCHAR))
        {
            return StrTool.cTrim(StrTool.unicodeToGBK(rs.getString(col)));
        }

        if ((columnType == Types.TIMESTAMP) || (columnType == Types.DATE))
        {
            return FDate.toString(rs.getDate(col));
        }

        if ((columnType == Types.DECIMAL) || (columnType == Types.FLOAT))
        {
            return PubFun.getInt(String.valueOf(rs.getBigDecimal(col)));
        }

        if ((columnType == Types.INTEGER) || (columnType == Types.SMALLINT))
        {
            return PubFun.getInt(String.valueOf(rs.getInt(col)));
        }

        if (columnType == Types.NUMERIC)
        {
            return PubFun.getInt(String.valueOf(rs.getBigDecimal(col)));
        }
        return "";
    }
}
