package com.sinosoft.lis.pubfun.query;

import java.util.Map;
import java.util.HashMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.SysConst;

public class ParseParams
{
    private ParseParams() {}

    public static Map parseCodeType(String params)
    {
        Map map = new HashMap();
        try
        {
            String[] codeTypes = PubFun.split(params, SysConst.RECORDSPLITER);
            for (int i = 0; i < codeTypes.length; i++)
            {
                String[] temp = PubFun.split(codeTypes[i],
                        SysConst.PACKAGESPILTER);
                CodeTypeParam param = new CodeTypeParam();
                param.setCol(temp[0]);
                param.setColType(temp[1]);
                param.setCodeType(temp[2]);
                map.put(temp[0], param);
            }
        }
        catch (NullPointerException ex)
        {
            ex.printStackTrace();
        }
        catch (NumberFormatException ex)
        {
            ex.printStackTrace();
        }
        catch (ArrayIndexOutOfBoundsException ex)
        {
            ex.printStackTrace();
        }
        return map;
    }
}
