package com.sinosoft.lis.pubfun.query;

import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

/**
 * <p>Title: 代码名称缓存</p>
 * <p>Description: 用缓存来提高查询效率</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class CodeCache
{
    /** 使用单例模式，使缓存唯一 */
    private static CodeCache instance = null;

    /** 用映射来查找缓存中的数据 */
    private Map codeNodeMap = new HashMap();

    /** 用链表来维持缓存的周期 */
    private LinkedList codeNodeList = new LinkedList();

    /** 缓存最大空间 */
    private int maxSize = 1024 * 1024;

    /** 缓存使用97%时要释放空间 */
    private int fullSize = (int) (maxSize * 0.97);

    /** 缓存释放空间时释放10% */
    private int desiredSize = (int) (maxSize * 0.90);

    /** 当前缓存使用大小 */
    private int size = 0;

    /**
     * 得到CodeCache的实例
     * @return CodeCache
     */
    public static synchronized CodeCache getInstance()
    {
        if (instance == null)
        {
            instance = new CodeCache();
        }
        return instance;
    }

    /**
     * 添加一个code
     * @param codeNode CodeNode
     */
    public void add(CodeNode codeNode)
    {
        int nodeSize = codeNode.getSize();
        if (nodeSize > maxSize * 0.9) //如果CodeNode太大则不用缓存
        {
            return;
        }
        size += nodeSize;
        codeNodeMap.put(codeNode.getCodeType(), codeNode);
        codeNodeList.addFirst(codeNode);
        cullCache(); //如果缓存过满则释放空间
    }

    /**
     * 根据codeType得到code，并把当前访问的code移到链表的最前面
     * @param codeType String
     * @return CodeNode
     */
    public CodeNode get(String codeType)
    {
        CodeNode codeNode = (CodeNode) codeNodeMap.get(codeType);
        codeNodeList.remove(codeNode);
        codeNodeList.addFirst(codeNode);
        return codeNode;
    }

    /**
     * 判断缓存中是否有与codeType相对应的CodeNode
     * @param codeType String
     * @return boolean
     */
    public boolean containsCodeNode(String codeType)
    {
        return codeNodeMap.containsKey(codeType);
    }

    /**
     * 删除Code
     * @param codeNode CodeNode
     */
    public void remove(CodeNode codeNode)
    {
        codeNodeMap.remove(codeNode);
        codeNodeList.remove(codeNode);
        size -= codeNode.getSize();
    }

    /**
     * 清空缓存
     */
    public void clear()
    {
        codeNodeMap.clear();
        codeNodeList.clear();
        size = 0;
    }

    /**
     * 如果缓存过满则释放空间
     */
    private void cullCache()
    {
        if (size >= fullSize)
        {
            while (size > desiredSize)
            {
                remove((CodeNode) codeNodeList.getLast());
            }
        }
    }
}
