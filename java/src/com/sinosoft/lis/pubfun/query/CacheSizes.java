package com.sinosoft.lis.pubfun.query;

import java.util.*;

/**
 * <p>Title: 缓存大小计算类</p>
 * <p>Description: 得到不同数据类型占用内存空间大小</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class CacheSizes
{
    public static int sizeOfObject()
    {
        return 4;
    }

    public static int sizeOfString(String string)
    {
        if (string == null)
        {
            return 0;
        }
        return 4 + string.length() * 2;
    }

    public static int sizeOfInt()
    {
        return 4;
    }

    public static int sizeOfChar()
    {
        return 2;
    }

    public static int sizeOfBoolean()
    {
        return 1;
    }

    public static int sizeOfLong()
    {
        return 8;
    }

    public static int sizeOfDouble()
    {
        return 8;
    }

    public static int sizeOfDate()
    {
        return 12;
    }

    public static int sizeOfMap(Map map)
    {
        if (map == null)
        {
            return 0;
        }

        int size = 36;
        Iterator iter = map.values().iterator();
        while (iter.hasNext())
        {
            String value = (String) iter.next();
            size += sizeOfString(value);
        }

        iter = map.keySet().iterator();
        while (iter.hasNext())
        {
            String key = (String) iter.next();
            size += sizeOfString(key);
        }
        return size;
    }
}
