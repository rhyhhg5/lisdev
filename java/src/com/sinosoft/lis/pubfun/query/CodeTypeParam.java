package com.sinosoft.lis.pubfun.query;

class CodeTypeParam
{
    private String col;

    private String colType = null;

    private String codeType = null;

    public String getCol()
    {
        return col;
    }

    public void setCol(String col)
    {
        this.col = col;
    }

    public String getColType()
    {
        return colType;
    }

    public void setColType(String colType)
    {
        this.colType = colType;
    }

    public String getCodeType()
    {
        return codeType;
    }

    public void setCodeType(String codeType)
    {
        this.codeType = codeType;
    }
}
