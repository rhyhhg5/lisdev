package com.sinosoft.lis.pubfun.query;

import java.util.Map;
import java.util.HashMap;

/**
 * <p>Title: 代码结点类</p>
 * <p>Description: 存放某一类型的代码</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class CodeNode
{
    /** 代码类型 */
    private String codeType = null;

    /** 代码和代码名称的映射 */
    private Map codeMap = new HashMap();

    /**
     * 构造函数
     * @param codeType String
     */
    public CodeNode(String codeType)
    {
        this.codeType = codeType;
    }

    /**
     * 得到code类型
     * @return String
     */
    public String getCodeType()
    {
        return codeType;
    }

    /**
     * 添加一个Code
     * @param code String
     * @param codeName String
     */
    public void add(String code, String codeName)
    {
        codeMap.put(code, codeName);
    }

    /**
     * 根据code代码得到code名称
     * @param code String
     * @return String
     */
    public String getCodeName(String code)
    {
        return (String) codeMap.get(code);
    }

    /**
     * 得到CodeNode对象的大小
     * @return int
     */
    public int getSize()
    {
        int size = CacheSizes.sizeOfObject()
                + CacheSizes.sizeOfString(codeType)
                + CacheSizes.sizeOfMap(codeMap);
        return size;
    }
}
