/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 扫描件处理类</p>
 * <p>Description: UI功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 * @date 2002-11-06
 */

public class EasyScanQueryUI
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public EasyScanQueryUI()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"QUERY||MAIN"和"QUERY||DETAIL"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 数据操作字符串拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        System.out.println("---EasyScanQuery BL BEGIN---");
        EasyScanQueryBL tEasyScanQueryBL = new EasyScanQueryBL();

        if (tEasyScanQueryBL.submitData(cInputData, mOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tEasyScanQueryBL.mErrors);
            mResult.clear();
            return false;
        }
        else
        {
            mResult = tEasyScanQueryBL.getResult();
        }
        System.out.println("---EasyScanQuery BL END---");

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args)
    {
//        EasyScanQueryUI easyScanQueryUI1 = new EasyScanQueryUI();
//        EasyScanQueryBL easyScanQueryBL1 = new EasyScanQueryBL();
//        VData tVData = new VData();
//        tVData.add("5");
//        easyScanQueryUI1.submitData(tVData, "QUERY||MAIN");
//        easyScanQueryBL1.submitData(tVData, "QUERY||MAIN");
//        VData tVDataResult = new VData();
//        tVDataResult = easyScanQueryBL1.getResult();
//        for (int i = 0; i < tVDataResult.size(); i++)
//        {
//            System.out.println(tVDataResult.get(i));
//        }
    }
}
