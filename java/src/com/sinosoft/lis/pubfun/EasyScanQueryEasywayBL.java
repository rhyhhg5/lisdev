/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.ES_DOC_PAGESDB;
import com.sinosoft.lis.db.ES_SERVER_INFODB;
import com.sinosoft.lis.easyscan.EasyScanConfig;
import com.sinosoft.lis.easyscan.RelationConfig;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_SERVER_INFOSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_SERVER_INFOSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 扫描件处理类</p>
 * <p>Description: BL层业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 * @date 2002-11-06
 */

public class EasyScanQueryEasywayBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 业务处理相关变量 */
    private String DocID="";
    private String DocCode = "";
    private String BussType = "";
    private String SubType = "";
    private String mSQL = ""; //查询语句
    private String mClientUrl = ""; //浏览器访问的URL

    /** 扫描件表 */
    private ES_DOC_MAINSet mES_DOC_MAINSet = new ES_DOC_MAINSet();
    private ES_DOC_MAINSchema mES_DOC_MAINSchema = new ES_DOC_MAINSchema();

    private ES_DOC_PAGESSet mES_DOC_PAGESSet = new ES_DOC_PAGESSet();
    private ES_DOC_PAGESSchema mES_DOC_PAGESSchema = new ES_DOC_PAGESSchema();

    private ES_SERVER_INFOSet mES_SERVER_INFOSet = new ES_SERVER_INFOSet();
    private ES_SERVER_INFOSchema mES_SERVER_INFOSchema = new
            ES_SERVER_INFOSchema();

    public EasyScanQueryEasywayBL()
    {
    }

    /**
     * 数据处理
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (this.getInputData() == false)
            return false;
        System.out.println("---End getInputData---");

        // 查询数据,查询的分支可以根据业务要求放到不同的调用级别中
        if (mOperate.equals("QUERY||MAIN"))
        {
            if (this.queryData() == false)
                return false;
            System.out.println("---End queryData---");
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @return boolean
     */
    private boolean getInputData()
    {
        DocID=(String) mInputData.getObject(0);
        System.out.println("DocID is "+DocID);
        DocCode = (String) mInputData.getObject(1);
        BussType = (String) mInputData.getObject(2);
        SubType = (String) mInputData.getObject(3);
        if (mClientUrl == null)
        {
            mClientUrl =
                    "ClientURL is null aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        }

        if (DocCode == null || DocCode.trim().equals(""))
        {
            // @@错误处理
            this.mErrors.addOneError(throwErr("EasyScanQueryEasywayBL", "getInputData",
                                              "没有传入mPrtNo印刷号!"));
            return false;
        }

        return true;
    }

    /**
     * 主要信息查询
     * @return boolean
     */
    private boolean queryData()
    {
        ES_DOC_MAINSet tES_DOC_MAINSet = new ES_DOC_MAINSet();
        ES_DOC_MAINSchema tES_DOC_MAINSchema = new ES_DOC_MAINSchema();
        VData tVDataResult = new VData();
        int i, j;
//
//        RelationConfig mConfig = RelationConfig.getInstance();
//        if (mConfig.getrelation(SubType).equals(""))
//        {
//            this.mErrors.addOneError(throwErr("EasyScanQueryBL", "queryData",
//                                              "读取配置信息出错！"));
//            mES_DOC_MAINSet.clear();
//            return false;
//        }
//        else
//        {
//            SubType = mConfig.getrelation(SubType);
//        }
        StringBuffer sb = new StringBuffer();
        //ES_DOC_MAIN Dealing*******************************************************
        //根据印刷号获取DOC_ID、NUM_PAGES和DOC_FLAGE，要校验DOC_FLAGE是否为1，不为1则进行扫描文件出错处理

        sb.append("select * from ES_DOC_MAIN where ");
        if (DocID != null && !DocID.equals(""))
        {
            sb.append("docid= " + DocID + " and ");
        }
        if (DocCode != null && !DocCode.equals(""))
        {
            sb.append("DocCode = '" + DocCode + "' and ");
        }
        if (BussType != null && !BussType.equals(""))
        {
            sb.append("BUSSTYPE = '" + BussType + "' and ");
        }
        if (SubType != null && !SubType.equals(""))
        {
            sb.append("SubType = '" + SubType + "' and ");
        }
        sb.append("1=1");
        mSQL = sb.toString();
        System.out.println("ANT" + mSQL);
        ES_DOC_MAINDB tES_DOC_MAINDB = mES_DOC_MAINSchema.getDB();
        mES_DOC_MAINSet = tES_DOC_MAINDB.executeQuery(mSQL);

        if (tES_DOC_MAINDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tES_DOC_MAINDB.mErrors);
            this.mErrors.addOneError(throwErr("EasyScanQueryEasywayBL", "queryData",
                                              "ES_DOC_MAIN查询失败!"));
            mES_DOC_MAINSet.clear();
            return false;
        }

        if (mES_DOC_MAINSet.size() == 0)
        {
            // @@错误处理
            this.mErrors.addOneError(throwErr("EasyScanQueryEasywayBL", "queryData",
                                              "未找到ES_DOC_MAIN相关数据!"));
            mES_DOC_MAINSet.clear();
            return false;
        }

        //当一个印刷号对应多个记录的时候，没有该业务情况，未考虑
//        if (mES_DOC_MAINSet.size() > 1)
//        {
//            System.out.println("该印刷号对应的单证号记录多于1个，未处理");
//            this.mErrors.addOneError(throwErr("EasyScanQueryBL", "queryData",
//                                              "该单证号记录多于1个，未处理!"));
//            mES_DOC_MAINSet.clear();
//            return false;
//        }

        //假设一个单证只有一条记录



        //判断扫描件错误，DOC_FLAGE为扫描件处理标记
//    if (!mES_DOC_MAINSchema.getDocFlag().equals("1")) {
//      this.mErrors.addOneError(throwErr("EasyScanQueryBL", "queryData", "DOC_FLAGE不为1，扫描件有误!"));
//      mES_DOC_MAINSet.clear();
//      return false;
//    }

        //ES_SERVER_INFO Dealing****************************************************
        mSQL = "select * from ES_SERVER_INFO";
        ES_SERVER_INFODB tES_SERVER_INFODB = mES_SERVER_INFOSchema.getDB();
        mES_SERVER_INFOSet = tES_SERVER_INFODB.executeQuery(mSQL);

        if (tES_SERVER_INFODB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tES_SERVER_INFODB.mErrors);
            this.mErrors.addOneError(throwErr("EasyScanQueryEasywayBL", "queryData",
                                              "ES_SERVER_INFO查询失败!"));
            mES_SERVER_INFOSet.clear();
            return false;
        }

        if (mES_SERVER_INFOSet.size() == 0)
        {
            // @@错误处理
            this.mErrors.addOneError(throwErr("EasyScanQueryBL", "queryData",
                                              "未找到ES_SERVER_INFO相关数据!"));
            mES_SERVER_INFOSet.clear();
            return false;
        }
        //mResult.add(mES_SERVER_INFOSet.encode());
        //取出所有服务器的信息，每个服务器取服务器名、IP和WEB根路径

        String[][] arrServerInfo = new String[mES_SERVER_INFOSet.size()][3];
        for (i = 1; i <= mES_SERVER_INFOSet.size(); i++) {
            ES_SERVER_INFOSchema tES_SERVER_INFOSchema = mES_SERVER_INFOSet.get(
                    i);
            arrServerInfo[i - 1][0] = tES_SERVER_INFOSchema.getHostName();
            arrServerInfo[i - 1][1] = tES_SERVER_INFOSchema.getServerPort();
            arrServerInfo[i - 1][2] = tES_SERVER_INFOSchema.getPicPath();
        }

        //ES_DOC_PAGES Dealing******************************************************
        //获取PAGE_NAME、PAGE_PATH和SERVER_HOST，要校验PAGE_FLAGE是否为1，不为1则进行扫描文件出错处理（PAGE_FLAGE放到前台进行校验了）
       mResult.clear();
       VData Url = new VData();
       VData Pages = new VData();
        for (int k = 0; k < mES_DOC_MAINSet.size(); k++) {
          mES_DOC_MAINSchema = mES_DOC_MAINSet.get(k + 1);
          mSQL = "select * from ES_DOC_PAGES where DOCID = " +
              mES_DOC_MAINSchema.getDocID() + " order by PAGECODE";
          System.out.println(mSQL);
          ES_DOC_PAGESDB tES_DOC_PAGESDB = mES_DOC_PAGESSchema.getDB();
          mES_DOC_PAGESSet = tES_DOC_PAGESDB.executeQuery(mSQL);

          if (tES_DOC_PAGESDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tES_DOC_PAGESDB.mErrors);
            this.mErrors.addOneError(throwErr("EasyScanQueryEasywayBL", "queryData",
                                              "ES_DOC_PAGES查询失败!"));
            mES_DOC_MAINSet.clear();
            return false;
          }

          if (mES_DOC_PAGESSet.size() == 0) {
            // @@错误处理
            this.mErrors.addOneError(throwErr("EasyScanQueryEasywayBL", "queryData",
                                              "未找到ES_DOC_PAGES相关数据!"));
            mES_DOC_PAGESSet.clear();
            return false;
          }

          System.out.println(mES_DOC_PAGESSet.encode());

          //拼URL，使VECTOR的每一个元素是一个URL
          System.out.println("--mClientURL=" + mClientUrl + "--");
            EasyScanConfig tConfig = EasyScanConfig.getInstance();
          StringBuffer serverUrlBuf = new StringBuffer();
          for (i = 1; i <= mES_DOC_PAGESSet.size(); i++) {
            ES_DOC_PAGESSchema tES_DOC_PAGESSchema = mES_DOC_PAGESSet.get(i);
            for (j = 0; j < arrServerInfo.length; j++) {
              if (arrServerInfo[j][0].equals(tES_DOC_PAGESSchema.getHostName())) {
                String strUrl = "http://" + arrServerInfo[j][1] + "/";
                serverUrlBuf.delete(0, serverUrlBuf.length());
                serverUrlBuf.append(strUrl);
                      tConfig.isForward(mClientUrl, serverUrlBuf);

                                                            //Edited by wellhi 2005.09.18
                                                            //如果数据库中保存的文件后缀是TIF,浏览器中就使用的GIF格式的图片;
                                                            //如果数据库中保存的文件后缀是JPG,浏览器中就使用的JPG格式的图片;
                                                            String strSuffix = tES_DOC_PAGESSchema.getPageSuffix();
                                                            if (strSuffix==null || strSuffix.equals("")) {
                                                              strSuffix = ".gif";
                                                            }
                                                            else {
                                                              if (strSuffix.equalsIgnoreCase(".TIF")) {
                                                                strSuffix = ".gif";
                                                              }
                                                            }
                                                            Url.add(serverUrlBuf
                                                                    //+ arrServerInfo[j][2]
                                                                    + tES_DOC_PAGESSchema.getPicPath()
                                                                    + tES_DOC_PAGESSchema.getPageName()
                                                                    + strSuffix);
                                                //                                + ".gif");
                        Pages.add(Integer.toString(mES_DOC_PAGESSet.size()) +
                                  "_" +
                                  Integer.toString(i));
                        break;
              }
            }
          }
          mResult.add(Url);
          mResult.add(Pages);
        }
        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 简化错误处理类付值
     * @param cModuleName String
     * @param cFunctionName String
     * @param cErrorMessage String
     * @return CError
     */
    private CError throwErr(String cModuleName, String cFunctionName,
                            String cErrorMessage)
    {
        CError tError = new CError();
        tError.moduleName = cModuleName;
        tError.functionName = cFunctionName;
        tError.errorMessage = cErrorMessage;
        return tError;
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args)
    {

        EasyScanQueryEasywayBL easyScanQueryBL1 = new EasyScanQueryEasywayBL();
        VData mVData = new VData();


        mVData.add("1921212121");
        mVData.add("11");
        mVData.add("TB");
        mVData.add("TB01");
        easyScanQueryBL1.submitData(mVData,"QUERY||MAIN");
    }

}
