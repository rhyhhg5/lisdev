package com.sinosoft.lis.pubfun;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class BankFileInfo {
	
    public static String getBankName(String paycode)
    {
        ExeSQL exeSql = new ExeSQL();
        SSRS testSSRS = new SSRS();

        String sql =
        	"select bankname from ldbank where exists (select 1 from  lysendtobank where paycode='"+paycode.trim() + "' and bankcode=ldbank.bankcode)";
        testSSRS = exeSql.execSQL(sql);
        return testSSRS.GetText(1, 1);
    }

}
