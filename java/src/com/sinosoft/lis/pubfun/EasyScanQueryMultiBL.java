/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.pubfun;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.ES_DOC_PAGESDB;
import com.sinosoft.lis.db.ES_SERVER_INFODB;
import com.sinosoft.lis.easyscan.EasyScanConfig;
import com.sinosoft.lis.easyscan.RelationConfig;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_SERVER_INFOSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_SERVER_INFOSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;

/**
 * <p>Title: 扫描件处理类</p>
 * <p>Description: BL层业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 * @date 2002-11-06
 */

public class EasyScanQueryMultiBL
{
    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 业务处理相关变量 */
    private String BussNo = null;  //单证条码号
    private String[] BussNoType = null;  //单证条码类型
    private String[] BussType = null;  //单证类型
    private String[] SubType = null;  //单证子类型
    private String mClientUrl = null; //浏览器访问的URL
    private String Order = null;  //查询到的单证扫描页的排序方式

    /** 扫描件表 */
    private ES_DOC_MAINSet mES_DOC_MAINSet = new ES_DOC_MAINSet();

    public EasyScanQueryMultiBL()
    {
    }

    /**
     * 数据处理
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 查询数据,查询的分支可以根据业务要求放到不同的调用级别中
        if (cOperate.equals("QUERY||MAIN"))
        {
            if (!queryData())
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        TransferData data = (TransferData)cInputData.
                         getObjectByObjectName("TransferData", 0);

        BussNo = (String) data.getValueByName("BussNo");
        BussNoType = (String[]) data.getValueByName("BussNoType");
        BussType = (String[]) data.getValueByName("BussType");
        SubType = (String[]) data.getValueByName("SubType");
        Order = (String) data.getValueByName("Order");

        if (mClientUrl == null)
        {
            mClientUrl = "ClientURL is null ";
        }
        if (BussNo == null || BussNo.trim().equals(""))
        {
            // @@错误处理
            this.mErrors.addOneError(throwErr("EasyScanQueryBL", "getInputData",
                                              "没有传入mPrtNo印刷号!"));
            return false;
        }
        if (Order == null || Order.equals("")
            || Order.toLowerCase().equals("asc"))
        {
            Order = " asc ";
        }
        else
        {
            Order = " desc ";
        }

        return true;
    }

    /**
     * 主要信息查询
     * @return boolean
     */
    private boolean queryData()
    {
        RelationConfig mConfig = RelationConfig.getInstance();
        for(int i = 0; i < SubType.length; i++)
        {
            String tempSubType = mConfig.getrelation(SubType[i]);
            if (tempSubType.equals(""))
            {
                this.mErrors.addOneError(throwErr("EasyScanQueryBL",
                                                  "queryData",
                                                  "读取配置信息出错！"));
                mES_DOC_MAINSet.clear();
                return false;
            }
            else
            {
                SubType[i] = tempSubType;
            }
        }

        //根据印刷号获取DOC_ID、NUM_PAGES和DOC_FLAGE
        mES_DOC_MAINSet = queryMainSet();
        if(mES_DOC_MAINSet == null || mES_DOC_MAINSet.size() == 0)
        {
            return false;
        }

        //ES_SERVER_INFO Dealing****************************************************
        ES_SERVER_INFOSet tES_SERVER_INFOSet = queryERVER_INFO();
        if(tES_SERVER_INFOSet == null || tES_SERVER_INFOSet.size() == 0)
        {
            return false;
        }

        //mResult.add(mES_SERVER_INFOSet.encode());
        //取出所有服务器的信息，每个服务器取服务器名、IP和WEB根路径
        String[][] arrServerInfo = new String[tES_SERVER_INFOSet.size()][3];
        for (int i = 1; i <= tES_SERVER_INFOSet.size(); i++)
        {
            ES_SERVER_INFOSchema tES_SERVER_INFOSchema =
                    tES_SERVER_INFOSet.get(i);
            arrServerInfo[i - 1][0] = tES_SERVER_INFOSchema.getHostName();
            arrServerInfo[i - 1][1] = tES_SERVER_INFOSchema.getServerPort();
            arrServerInfo[i - 1][2] = tES_SERVER_INFOSchema.getPicPath();
        }

        if(!queryPages(arrServerInfo))
        {
            return false;
        }

        return true;
    }

    /**
     * 组合成查询扫描件主表的SQL语句
     * @return String
     */
    private String createSql()
    {
        StringBuffer sb = new StringBuffer();

        sb.append("select a.* from ES_DOC_MAIN a,ES_DOC_RELATION b where ");
        if (BussNo != null && !BussNo.equals(""))
        {
            sb.append("b.BUSSNO = '" + BussNo + "' ");
        }
        if (BussNoType != null && BussNoType.length > 0)
        {
            String temp = "";
            for(int i = 0; i < BussNoType.length; i++)
            {
                temp += "b.BUSSNOTYPE = '" + BussNoType[i] + "' or ";
            }
            sb.append("and (" + temp.substring(
                    0, temp.lastIndexOf("or")) + ") ");
        }
        if (BussType != null && BussType.length > 0)
        {
            String temp = "";
            for(int i = 0; i < BussType.length; i++)
            {
                temp += "b.BUSSTYPE = '" + BussType[i] + "' or ";
            }
            sb.append("and (" + temp.substring(
                    0, temp.lastIndexOf("or")) + ") ");
        }
        if (SubType != null && SubType.length > 0)
        {
            String temp = "";
            for (int i = 0; i < SubType.length; i++)
            {
                temp += "b.SubType = '" + SubType[i] + "' or ";
            }
            sb.append("and (" + temp.substring(
                    0, temp.lastIndexOf("or")) + ") ");
        }
        sb.append("and a.docid = b.docid ");
        sb.append("order by a.docID ");

        System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     * 根据印刷号获取DOC_ID、NUM_PAGES和DOC_FLAGE
     * @return ES_DOC_MAINSet
     */
    private ES_DOC_MAINSet queryMainSet()
    {
        //根据印刷号获取DOC_ID、NUM_PAGES和DOC_FLAGE，要校验DOC_FLAGE是否为1，不为1则进行扫描文件出错处理
        String sql = createSql();
        ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
        ES_DOC_MAINSet tES_DOC_MAINSet = tES_DOC_MAINDB.executeQuery(sql);

        if (tES_DOC_MAINSet == null || tES_DOC_MAINDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tES_DOC_MAINDB.mErrors);
            this.mErrors.addOneError(throwErr("EasyScanQueryBL", "queryData",
                                              "ES_DOC_MAIN查询失败!"));
            tES_DOC_MAINSet.clear();
        }

        if (tES_DOC_MAINSet.size() == 0)
        {
            // @@错误处理
            this.mErrors.addOneError(throwErr("EasyScanQueryBL", "queryData",
                                              "未找到ES_DOC_MAIN相关数据!"));
            tES_DOC_MAINSet.clear();
        }

        return tES_DOC_MAINSet;
    }

    /**
     * 查询服务器信息
     * @return ES_SERVER_INFOSet
     */
    private ES_SERVER_INFOSet queryERVER_INFO()
    {
        //ES_SERVER_INFO Dealing****************************************************
        String sql = "select * from ES_SERVER_INFO";
        ES_SERVER_INFODB tES_SERVER_INFODB = new ES_SERVER_INFODB();
        ES_SERVER_INFOSet tES_SERVER_INFOSet = tES_SERVER_INFODB.executeQuery(
                sql);

        if (tES_SERVER_INFODB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tES_SERVER_INFODB.mErrors);
            this.mErrors.addOneError(throwErr("EasyScanQueryBL", "queryData",
                                              "ES_SERVER_INFO查询失败!"));
            tES_SERVER_INFOSet.clear();
        }

        if (tES_SERVER_INFOSet.size() == 0)
        {
            // @@错误处理
            this.mErrors.addOneError(throwErr("EasyScanQueryBL", "queryData",
                                              "未找到ES_SERVER_INFO相关数据!"));
            tES_SERVER_INFOSet.clear();
        }
        return tES_SERVER_INFOSet;
    }

    /**
     * 获取扫描页信息
     * @param arrServerInfo String[][]
     * @return boolean
     */
    private boolean queryPages(String[][] arrServerInfo)
    {
        //获取PAGE_NAME、PAGE_PATH和SERVER_HOST，要校验PAGE_FLAGE是否为1，不为1则进行扫描文件出错处理（PAGE_FLAGE放到前台进行校验了）
        mResult.clear();
        VData Url = new VData();
        VData Pages = new VData();

        for (int k = 0; k < mES_DOC_MAINSet.size(); k++)
        {
            ES_DOC_MAINSchema tES_DOC_MAINSchema = mES_DOC_MAINSet.get(k + 1);

            //查询 tES_DOC_MAINSchema 对应的扫描页
            ES_DOC_PAGESSet tES_DOC_PAGESSet = queryPAGESSet(tES_DOC_MAINSchema);
            if(tES_DOC_PAGESSet == null || tES_DOC_PAGESSet.size() == 0)
            {
                return false;
            }

            //拼URL，使VECTOR的每一个元素是一个URL
            System.out.println("--mClientURL=" + mClientUrl + "--");
            EasyScanConfig tConfig = EasyScanConfig.getInstance();
            StringBuffer serverUrlBuf = new StringBuffer();
            for (int i = 1; i <= tES_DOC_PAGESSet.size(); i++)
            {
                ES_DOC_PAGESSchema tES_DOC_PAGESSchema = tES_DOC_PAGESSet.get(i);
                for (int j = 0; j < arrServerInfo.length; j++)
                {
                    if (arrServerInfo[j][0].equals(tES_DOC_PAGESSchema.
                            getHostName()))
                    {
                        String strUrl = "http://" + arrServerInfo[j][1] + "/";
                        serverUrlBuf.delete(0, serverUrlBuf.length());
                        serverUrlBuf.append(strUrl);
                        tConfig.isForward(mClientUrl, serverUrlBuf);

                        Url.add(serverUrlBuf
                                + tES_DOC_PAGESSchema.getPicPath()
                                + tES_DOC_PAGESSchema.getPageName() + ".gif");
                        Pages.add(Integer.toString(tES_DOC_PAGESSet.size())
                                  + "_" + Integer.toString(i));
                        break;
                    }
                }
            }
            mResult.add(Url);
            mResult.add(Pages);
        }

        return true;
    }

    /**
     * 查询 tES_DOC_MAINSchema 对应的扫描页
     * @param tES_DOC_MAINSchema ES_DOC_MAINSchema
     * @return ES_DOC_PAGESSet
     */
    private ES_DOC_PAGESSet queryPAGESSet(ES_DOC_MAINSchema tES_DOC_MAINSchema)
    {
        String sql = "select * from ES_DOC_PAGES where DOCID = "
                     + tES_DOC_MAINSchema.getDocID() + " order by PAGECODE";
        ES_DOC_PAGESDB tES_DOC_PAGESDB = new ES_DOC_PAGESDB();
        ES_DOC_PAGESSet tES_DOC_PAGESSet = tES_DOC_PAGESDB.executeQuery(sql);

        if (tES_DOC_PAGESDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tES_DOC_PAGESDB.mErrors);
            this.mErrors.addOneError(throwErr("EasyScanQueryBL",
                                              "queryData",
                                              "ES_DOC_PAGES查询失败!"));
            tES_DOC_PAGESSet.clear();
        }

        if (tES_DOC_PAGESSet.size() == 0)
        {
            // @@错误处理
            this.mErrors.addOneError(throwErr("EasyScanQueryBL",
                                              "queryData",
                                              "未找到ES_DOC_PAGES相关数据!"));
            tES_DOC_PAGESSet.clear();
        }

        System.out.println(tES_DOC_PAGESSet.encode());
        return tES_DOC_PAGESSet;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 简化错误处理类付值
     * @param cModuleName String
     * @param cFunctionName String
     * @param cErrorMessage String
     * @return CError
     */
    private CError throwErr(String cModuleName, String cFunctionName,
                            String cErrorMessage)
    {
        CError tError = new CError();
        tError.moduleName = cModuleName;
        tError.functionName = cFunctionName;
        tError.errorMessage = cErrorMessage;
        return tError;
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args)
    {

        EasyScanQueryMultiBL easyScanQueryMultiBL = new EasyScanQueryMultiBL();
        VData mVData = new VData();

        String[] BussNoType = {"91", "91"};
        String[] BussType = {"BQ", "BQ"};
        String[] SubType = {"BQ01", "BQ02"};

        TransferData data = new TransferData();
        data.setNameAndValue("BussNo", "20050928000047");
        data.setNameAndValue("BussNoType", BussNoType);
        data.setNameAndValue("BussType", BussType);
        data.setNameAndValue("SubType", SubType);

        mVData.add(data);
        easyScanQueryMultiBL.submitData(mVData, "QUERY||MAIN");
        if(easyScanQueryMultiBL.mErrors.needDealError())
        {
            System.out.println(easyScanQueryMultiBL.mErrors.getErrContent());
        }
        else
        {
            VData tVData = new VData();
            tVData = easyScanQueryMultiBL.getResult();
            tVData = (VData) tVData.get(0);
            String[] arrResult = new String[tVData.size()];

            for (int i = 0; i < tVData.size(); i++)
            {
                arrResult[i] = (String) tVData.get(i);
            }
        }
    }
}
