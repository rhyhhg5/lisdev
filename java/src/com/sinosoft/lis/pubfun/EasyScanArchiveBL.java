package com.sinosoft.lis.pubfun;

/**
 * <p>Title: EasyScanArchiveBL.java</p>
 * <p>Description:通用扫描件归档 </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: Sinosoft</p>
 * @author Huxl
 * @version 1.0
 */
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.io.IOException;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.BufferedOutputStream;
import java.io.BufferedInputStream;
import java.io.*;
import java.net.URL;
import java.util.Date;
import java.text.SimpleDateFormat;

public class EasyScanArchiveBL {
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 业务处理相关变量 */
    private String[] arrPic = null; //扫描件地址
    private String prtNo = "";
    private String SubType = "";

    //获取当前系统的前一天日期
    private String mCurrentDate = new PubFun().calDate(new PubFun().getCurrentDate(),-1,"D",null).replaceAll("-","");
    private String mCurrentTime = new PubFun().getCurrentTime2();
    public String mfilename[];

    public EasyScanArchiveBL() {
    }

    /**
     * 数据处理
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) throws
            IOException {
        // 将传入的数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return false;
        }
        System.out.println("---End getInputData---");
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @return boolean
     */
    private boolean getInputData() {
        TransferData data = (TransferData) mInputData.
                            getObjectByObjectName("TransferData", 0);

        arrPic = (String[]) data.getValueByName("arrPic");
        prtNo = (String) data.getValueByName("prtNo");
        SubType = (String) data.getValueByName("SubType");

        if (arrPic.length == 0) {
            // @@错误处理
            this.mErrors.addOneError("没有传入扫描件地址!");
            return false;
        }

        return true;
    }

    private synchronized boolean dealData() throws IOException {
        String strSql =
                "select SysVarValue from LDSYSVAR where Sysvar='ArchiveUrl'";
        String dir = new ExeSQL().getOneValue(strSql);
//          String dir = "E:\\workspace\\ui\\printdata\\archive\\";
        mfilename = new String[arrPic.length];
        int m=0;
        for (int i = 0; i < arrPic.length; i++) {
            String tPageSuffix = ".tif";
            if (arrPic[i].substring(arrPic[i].lastIndexOf(".")).toLowerCase().
                equals(".jpg")) {
                tPageSuffix = ".jpg";
            }
            String filePath = dir.substring(0,dir.lastIndexOf(".")+4)+arrPic[i].substring(arrPic[i].indexOf("/",arrPic[i].lastIndexOf(":")), arrPic[i].lastIndexOf(
                    ".")) + tPageSuffix;
            System.out.println("扫描件URL地址:" + filePath);

            String fileName = arrPic[i].substring(arrPic[i].lastIndexOf("/") +
                                                  1);
            fileName = fileName.substring(0, fileName.lastIndexOf("."));
            System.out.println("解析后获取扫描件文件名:" + fileName);

            byte[] buffer = new byte[4096];
            BufferedOutputStream output = null;
            BufferedInputStream input = null;
            File tFile = null ;
            try {
                tFile = new File(dir + fileName + tPageSuffix);
                output = new BufferedOutputStream((OutputStream) (new
                        FileOutputStream(tFile)));
                input = new BufferedInputStream((InputStream)(new FileInputStream(new File(filePath))));
                int n = -1;
                while ((n = input.read(buffer, 0, 4096)) > -1) {
                    output.write(buffer, 0, n);
                }
                output.flush();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if(input == null){
                    if(output != null){
                        output.close();
                    }
                    tFile.delete();
                    break;
                }
                if (input != null) {
                    input.close();
                }
                if (output != null) {
                    output.close();
                }
            }

            if( tPageSuffix != ".jpg"){
              mfilename[m] = fileName;//传回文件名,往mapping里面写
//              System.out.println(" mfilename["+m+"]:"+mfilename[m]);
              m++;
            }
            FileWriter tFileWriter = new FileWriter(dir +
                    tPageSuffix.substring(1) + "_" +
                    fileName + ".jrn");
            BufferedWriter tBufferedWriter = new BufferedWriter(tFileWriter);
            //如果是健管团体合同扫描件
            if (SubType.equals("HM01")) {
                String strSQL = "select a.contraNo,a.ContraName,a.HospitCode,(select  HospitName from LDHospital " +
                                "where LDHospital.HospitCode=a.HospitCode) from lhgroupcont a where contrano = '" +
                                prtNo + "' with ur";
                SSRS tSSRS = new ExeSQL().execSQL(strSQL);
                if(tSSRS != null && tSSRS.getMaxNumber() > 0){
                    String contraNo = tSSRS.GetText(1, 1); //合同代码
                    String congtraName = tSSRS.GetText(1, 2); //合同名称
                    String HospitCode = tSSRS.GetText(1, 3); //医院代码
                    String HospitName = tSSRS.GetText(1, 4); //医院名称

                    tBufferedWriter.write("J|" + fileName + "|" + mCurrentDate +
                                          mCurrentTime);
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("A|HospitalID|" + HospitCode);
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("A|ContractName|" + congtraName);
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("A|Page|" + (i + 1));
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("D|" + fileName + "|" + mCurrentDate +
                                          "|1|" +
                                          HospitName + "|");
                    tBufferedWriter.newLine();
                }else{
                    tFile.delete();
                    tBufferedWriter.flush();
                    tBufferedWriter.close();
                    File mFile = new File(dir + tPageSuffix.substring(1) + "_" +fileName + ".jrn");
                    mFile.delete();
                    if(mfilename[m-1].equals(fileName)){
                        mfilename[m-1]=null;
                        m--;
                    }
                }
            } else if (SubType.equals("LP01")) { //处理
                String strSQL =
                        "select customerno,customername,mngcom from llcase where caseno = '" +
                        prtNo + "'  with ur";
                SSRS tSSRS = new ExeSQL().execSQL(strSQL);
                System.out.println("strSQL");
                if(tSSRS!=null && tSSRS.getMaxNumber() > 0){
                    String ProposalContNo = "abc"; //投保单号
                    String tCustomerNo = tSSRS.GetText(1, 1); //出险人号
                    String tCustomerName = tSSRS.GetText(1, 2); //出险人姓名
                    String tManageCom = tSSRS.GetText(1, 3); //管理机构
                    tBufferedWriter.write("A|DOCID|" + prtNo);
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("A|CUSID|" + tCustomerNo);
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("D|" + ProposalContNo + "|" +
                                          mCurrentDate + "_" + SubType + "_" +
                                          prtNo + "_" + tCustomerNo + "_" +
                                          tCustomerName +
                                          "_" + (i + 1) + "_" + tManageCom +
                                          "|1|" +
                                          tCustomerName + "|");
                    tBufferedWriter.newLine();
                }else{
                    tFile.delete();
                    tBufferedWriter.flush();
                    tBufferedWriter.close();
                    File mFile = new File(dir + tPageSuffix.substring(1) + "_" +
                                          fileName + ".jrn");
                    mFile.delete();
                    if (mfilename[m - 1].equals(fileName)) {
                        mfilename[m - 1] = null;
                        m--;
                    }
                }
            } else if (SubType.equals("LP02")) { //处理
                String strSQL =
                        "select customerno,grpname,mngcom,rgtobjno from llregister where rgtno = '" +
                        prtNo + "'  with ur";
                SSRS tSSRS = new ExeSQL().execSQL(strSQL);
                if(tSSRS!=null && tSSRS.getMaxNumber() > 0){
                String ProposalContNo = tSSRS.GetText(1, 4); //投保单号
                String tCustomerNo = tSSRS.GetText(1, 1); //出险人号
                String tCustomerName = tSSRS.GetText(1, 2); //出险人姓名
                String tManageCom = tSSRS.GetText(1, 3); //管理机构
                tBufferedWriter.write("A|DOCID|" + prtNo);
                tBufferedWriter.newLine();
                tBufferedWriter.write("A|CUSID|" + tCustomerNo);
                tBufferedWriter.newLine();
                tBufferedWriter.write("D|" + ProposalContNo + "|" +
                                      mCurrentDate + "_" + SubType + "_" +
                                      prtNo + "_" + tCustomerNo + "_" +
                                      tCustomerName +
                                      "_" + (i + 1) + "_" + tManageCom + "|1|" +
                                      tCustomerName + "|");
                tBufferedWriter.newLine();
                }else{
                    tFile.delete();
                    tBufferedWriter.flush();
                    tBufferedWriter.close();
                    File mFile = new File(dir + tPageSuffix.substring(1) + "_" +
                                          fileName + ".jrn");
                    mFile.delete();
                    if (mfilename[m - 1].equals(fileName)) {
                        mfilename[m - 1] = null;
                        m--;
                    }
                }
            } else if (SubType.equals("TB01") || SubType.equals("TB15") ||
                       SubType.equals("TB16") || SubType.equals("TB21") ||
                       SubType.equals("TB22")) { //处理个险投保单
                String strSQL = "SELECT ContNo, AppntNo, AppntName, ManageCom "
                                +
                        "FROM LCCont WHERE ContType = '1' AND PrtNo = '" +
                                prtNo + "' with ur";
                System.out.println(strSQL);
                SSRS tSSRS = new ExeSQL().execSQL(strSQL);
                if(tSSRS!=null && tSSRS.getMaxNumber() > 0){
                    String contNo = tSSRS.GetText(1, 1); //投保单号
                    String customerNo = tSSRS.GetText(1, 2); //出险人号
                    String customerName = tSSRS.GetText(1, 3); //出险人姓名
                    String manageCom = tSSRS.GetText(1, 4); //管理机构
                    tBufferedWriter.write("A|DOCID|" + prtNo);
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("A|CUSID|" + customerNo);
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("D|" + contNo + "|" + mCurrentDate +
                                          "_" +
                                          SubType + "_" +
                                          prtNo + "_" + customerNo + "_" +
                                          customerName +
                                          "_" + (i + 1) + "_" + manageCom +
                                          "|1|" +
                                          customerName + "|");
                    tBufferedWriter.newLine();
                }else{
                    tFile.delete();
                    tBufferedWriter.flush();
                    tBufferedWriter.close();
                    File mFile = new File(dir + tPageSuffix.substring(1) + "_" +
                                          fileName + ".jrn");
                    mFile.delete();
                    if (mfilename[m - 1].equals(fileName)) {
                        mfilename[m - 1] = null;
                        m--;
                    }
                }
            } else if (SubType.equals("TB02")) { //处理团险投保单
                String strSQL =
                        "SELECT PrtNo, GrpContNo, AppntNo, GrpName, ManageCom "
                        + "FROM LCGrpCont WHERE PrtNo = '" + prtNo +
                        "' with ur";
                System.out.println(strSQL);
                SSRS tSSRS = new ExeSQL().execSQL(strSQL);

                if(tSSRS!=null && tSSRS.getMaxNumber() > 0){
                    String contNo = tSSRS.GetText(1, 1); //投保单号
                    String customerNo = tSSRS.GetText(1, 2); //出险人号
                    String customerName = tSSRS.GetText(1, 3); //出险人姓名
                    String manageCom = tSSRS.GetText(1, 4); //管理机构
                    tBufferedWriter.write("A|DOCID|" + prtNo);
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("A|CUSID|" + customerNo);
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("D|" + contNo + "|" + mCurrentDate + "_" +
                                          SubType + "_" +
                                          prtNo + "_" + customerNo + "_" +
                                          customerName +
                                          "_" + (i + 1) + "_" + manageCom + "|1|" +
                                          customerName + "|");
                    tBufferedWriter.newLine();
                }else{
                    tFile.delete();
                    tBufferedWriter.flush();
                    tBufferedWriter.close();
                   File mFile = new File(dir + tPageSuffix.substring(1) + "_" +
                                         fileName + ".jrn");
                   mFile.delete();
                   if (mfilename[m - 1].equals(fileName)) {
                       mfilename[m - 1] = null;
                       m--;
                   }
                }
            } else  if (SubType.equals("BQ01")) {
                String strSQL = "select edorno,b.contno,b.appntno,appntname,b.managecom From lpedoritem a,lccont " +
                                "b where  edoracceptno='"
                                + prtNo + "'" +
                                "and a.contno = b.contno with ur";
                System.out.println(strSQL);
                SSRS tSSRS = (new ExeSQL()).execSQL(strSQL);
                if (tSSRS != null && tSSRS.getMaxNumber() > 0) {
                    String contNo = tSSRS.GetText(1, 2);
                    String edorNo = tSSRS.GetText(1, 1);
                    String appntNo = tSSRS.GetText(1, 3);
                    String appntName = tSSRS.GetText(1, 4);
                    String manageCom = tSSRS.GetText(1, 5);
                    tBufferedWriter.write("A|DOCID|" + prtNo);
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("A|CUSID|" + appntNo);
                    tBufferedWriter.newLine();
                    tBufferedWriter.write("D|" + contNo + "|" + mCurrentDate +
                                          "_" +
                                          SubType + "_" + prtNo + "_" + appntNo +
                                          "_" + appntName + "_" + (i + 1) + "_" +
                                          manageCom + "|1|" + appntName + "|");
                    tBufferedWriter.newLine();
                } else {
                    tFile.delete();
                    tBufferedWriter.flush();
                    tBufferedWriter.close();
                    File mFile = new File(dir + tPageSuffix.substring(1) + "_" +
                                          fileName + ".jrn");
                    mFile.delete();
                    if (mfilename[m - 1].equals(fileName)) {
                        mfilename[m - 1] = null;
                        m--;
                    }
                }
            }
            tBufferedWriter.flush();
            tBufferedWriter.close();

        }
        //下面调用ftp脚本将索引文件,扫描件文件传送到Group One 的服务器中.
//        String strCmd = "cmd /c D:\\62\\ui\\printdata\\archive\\123.bat";
//        CallDoscomm(strCmd);
//        strCmd =
//                "cmd /c ftp -n -s:D:\\62\\ui\\printdata\\archive\\putPDF.ftp";
       // CallDoscomm(strCmd, dir);
        return true;
    }

    public boolean CallDoscomm(String strCmd) {
        CallDoscomm(strCmd, null);
        return true;
    }

    /*指定路径下执行DOS命令程序*/
    public boolean CallDoscomm(String strCmd, String dir) {
        try {
            System.out.println("i come the CallDoscomm!" + strCmd);
            String ls_1;
            Process process = null;
            System.out.println("Str Cmd " + strCmd);
            if (dir != null) {
                process = Runtime.getRuntime().exec(strCmd, null, new File(dir));
            } else {
                process = Runtime.getRuntime().exec(strCmd);
            }
            System.out.println("111");

            BufferedReader tBufferedReader = new BufferedReader(new
                    InputStreamReader(process.getInputStream()));
            System.out.println("222");

            while ((ls_1 = tBufferedReader.readLine()) != null) {
                System.out.println(ls_1);
            }
            System.out.println("333");
            process.waitFor();
            System.out.println("getRuntime() : " +
                               Runtime.getRuntime().exec(strCmd));
        } catch (Exception e) {
            System.err.println("IO error: " + e.toString());
            return false;
        }
        return true;
    }

    public static void main(String[] args) throws IOException {
//        String  path = new File("/").getAbsolutePath();
//            EasyScanArchiveBL.class.getResource("/");
//        System.out.println(Thread.currentThread().getContextClassLoader().
//                           getResource(""));
//        System.out.println(EasyScanArchiveBL.class.getClassLoader().getResource(""));
//        System.out.println(ClassLoader.getSystemResource(""));
//        System.out.println(EasyScanArchiveBL.class.getResource(""));
//        System.out.println(EasyScanArchiveBL.class.getResource("/")); //Class文件所在路径
//        System.out.println(new File("/").getAbsolutePath());
//        System.out.println(System.getProperty("user.dir"));
//
//        System.out.println(EasyScanArchiveBL.class.getResource("/"));
//        String filename = "http:\\10.252.130.157:900/picch/ui/F63.jpg";
//        filename = filename.substring(filename.lastIndexOf("/") + 1);
//        filename = filename.substring(0, filename.lastIndexOf("."));
//        FileWriter tFileWriter = new FileWriter("C:\\" + filename + ".jrn");
//        BufferedWriter tBufferedWriter = new BufferedWriter(tFileWriter);
//        tBufferedWriter.write("J|F176534|20060302101010");
//        tBufferedWriter.newLine();
//        tBufferedWriter.write("J|F176534|20060302101010");
//        tBufferedWriter.newLine();
//        tBufferedWriter.write("J|F176534|20060302101010");
//        tBufferedWriter.newLine();
//        tBufferedWriter.flush();
//        tBufferedWriter.close();
        EasyScanArchiveBL tEasyScanArchiveBL=new EasyScanArchiveBL();
//        System.out.println(tEasyScanArchiveBL.mCurrentDate);
//        tEasyScanArchiveBL.getInputData();
//        tEasyScanArchiveBL.dealData();
    }
}
