package com.sinosoft.lis.pubfun.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class MailAuthenticator extends Authenticator {
    public MailAuthenticator() {
    }

    /**  用户名 */
    private String mUserName;
    /** 密码 */
    private String mPassword;

    public void setUserName(String username) {
        mUserName = username;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public MailAuthenticator(String username, String password) {
        super();
        setUserName(username);
        setPassword(password);
    }

    /**
     * 添加用户名密码设置
     * @return PasswordAuthentication
     */
    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(mUserName, mPassword);
    }
}
