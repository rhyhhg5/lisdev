package com.sinosoft.lis.pubfun.mail;

import java.util.Properties;

import javax.mail.Session;
import javax.mail.Transport;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import javax.mail.*;
import javax.mail.*;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class MailServerConfig {
    /** Smtp服务器配置 */
    private String MAIL_PASSWORD;
    private String MAIL_USER;
    private String MAIL_SERVER;
    private String MAIL_SMTP_FROM_NAME;
    private String MAIL_SMTP_FROM_MAIL;
    /** 声明连接配置信息 */
    private Properties props;
    /** 用户信息 */
    private MailAuthenticator mMailAuthenticator;
    /** 会话信息(邮件中的会话可以认为是访问邮件服务器) */
    private Session session;
    /** 报错信息 */
    public CErrors mErrors = new CErrors();
    /** 调试信息 */
    private boolean mDebug = true;
    /** 发送邮件配置信息 */
    private Transport mTransport;
    /**
     *  添加邮件服务器配置
     */
    public MailServerConfig() throws Exception {
        /** 首先取出系统描述 */
        if (!initServer()) {
            buildError("MailServerConfig", "初始化服务器失败！");
            throw new Exception("初始化服务器失败");
        }
        /** 初始化会话相关 */
        if (!initMail()) {
            buildError("MailServerConfig", "初始化邮件信息失败！");
            throw new Exception("初始化邮件信息失败");
        }
        /** 初始化传输方案 */
        if (!initTransport()) {
            buildError("MailServerConfig", "初始化传输服务失败！");
            throw new Exception("初始化传输服务失败");
        }
    }

    /**
     * Transport
     *
     * @return boolean
     */
    private boolean initTransport() {
        try {
            mTransport = session.getTransport("smtp");
            mTransport.connect(this.MAIL_SERVER, this.MAIL_USER,
                               this.MAIL_PASSWORD);
        } catch (NoSuchProviderException ex) {
            buildError("initTransport", "设置邮件传输协议错误！" + ex.getMessage());
            return false;
        } catch (MessagingException ex) {
            buildError("initTransport", "连接邮件服务器失败！" + ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * initMail
     *
     * @return boolean
     */
    private boolean initMail() {
        mMailAuthenticator = new MailAuthenticator(MAIL_USER, MAIL_PASSWORD);
        System.out.println("开始设置邮件服务器,目前仅支持SMTP邮件服务器服务器地址为" + MAIL_SERVER);
        props = new Properties();
        try {
            props.put("mail.smtp.host", MAIL_SERVER);
            props.put("mail.stmp.auth", "true");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        session = Session.getDefaultInstance(props, mMailAuthenticator);
        /** 将会把发送邮件的后台信息输出 */
        session.setDebug(mDebug);
        return true;
    }

    /**
     * initServer
     *
     * @return boolean
     */
    private boolean initServer() {
        StringBuffer sql = new StringBuffer(255);
        sql.append(
                "SELECT SYSVARVALUE FROM LDSYSVAR WHERE SYSVAR='MAIL_SMTP_PASSWORD'");
        MAIL_PASSWORD = (new ExeSQL()).getOneValue(sql.toString());
        sql = new StringBuffer(255);
        sql.append(
                "SELECT SYSVARVALUE FROM LDSYSVAR WHERE SYSVAR='MAIL_SMTP_USER'");
        MAIL_SERVER = (new ExeSQL()).getOneValue(sql.toString());
        sql = new StringBuffer(255);
        sql.append(
                "SELECT SYSVARVALUE FROM LDSYSVAR WHERE SYSVAR='MAIL_SMTP_SERVER'");
        MAIL_SERVER = (new ExeSQL()).getOneValue(sql.toString());
        sql = new StringBuffer(255);
        sql.append(
                "SELECT SYSVARVALUE FROM LDSYSVAR WHERE SYSVAR='MAIL_SMTP_FROM_MAIL'");
        MAIL_SMTP_FROM_MAIL = (new ExeSQL()).getOneValue(sql.toString());
        sql = new StringBuffer(255);
        sql.append(
                "SELECT SYSVARVALUE FROM LDSYSVAR WHERE SYSVAR='MAIL_SMTP_FROM_NAME'");
        MAIL_SMTP_FROM_NAME = (new ExeSQL()).getOneValue(sql.toString());

        if (MAIL_PASSWORD == null || MAIL_PASSWORD == null || MAIL_PASSWORD == null ||
            MAIL_SMTP_FROM_MAIL == null || MAIL_SMTP_FROM_NAME == null) {
            buildError("initServer", "邮件服务信息配置错误,请检查是否已经配置有mail服务信息！");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
//        MailServerConfig mailserverconfig = new MailServerConfig();
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "MailServerConfig";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    public Transport getMTransport() {
        return mTransport;
    }

    public String getFromName() {
        return MAIL_SMTP_FROM_NAME;
    }

    public String getFromMail() {
        return MAIL_SMTP_FROM_MAIL;
    }
}
