package com.sinosoft.lis.pubfun;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

import com.sinosoft.utility.*;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Yangming
 * @version 6.0
 */
public class WriteToExcel {
	/** Excel对象 **/
	private HSSFWorkbook outputExecleFile = null;
	/** Sheet对象 **/
	private HSSFSheet[] sheet = null;
	/** 文件名 */
	private String fileName;
	/** 样式（字体） **/
	private HSSFFont font = null;
	/** Style 是针对全局单元格的样式 **/
	private HSSFCellStyle style = null;

	private String mSheetName = null;

	public WriteToExcel() {
	}

	public WriteToExcel(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * 如果没有指定某一格单元格的字体,将使用此字体, 如果没添加此字体,则默认使用系统中的第一个字体 建议添加一个中文字体,"宋体",否则有可能出现乱
	 * 码问题
	 * 
	 * @param fontName
	 *            String
	 */
	public void setFontName(String fontName) {
		font = outputExecleFile.createFont();
		font.setFontName(fontName);
		if (style == null) {
			style = outputExecleFile.createCellStyle();
		}
		style.setFont(font);
	}

	/**
	 * 调节单元格
	 * 
	 * @param tWrap
	 *            boolean
	 */
	public void setWrapText(boolean tWrap) {
		if (style == null) {
			style = outputExecleFile.createCellStyle();
		}
		style.setWrapText(tWrap);
	}

	/**
	 * 全局单元格水平对齐方式
	 * 
	 * @param tAlignment
	 *            String
	 */
	public void setAlignment(String tAlignment) {
		if (style == null) {
			style = outputExecleFile.createCellStyle();
		}
		short alignement = style.ALIGN_GENERAL;
		if (tAlignment.toUpperCase().equals("RIGHT")) {
			alignement = style.ALIGN_RIGHT;
		} else if (tAlignment.toUpperCase().equals("LEFT")) {
			alignement = style.ALIGN_LEFT;
		} else if (tAlignment.toUpperCase().equals("CENTER")) {
			alignement = style.ALIGN_CENTER;
		} else if (tAlignment.toUpperCase().equals("FILL")) {
			alignement = style.ALIGN_FILL;
		}
		style.setAlignment(alignement);
	}

	/**
	 * 全局单元格垂直对齐方式
	 * 
	 * @param tVertical
	 *            String
	 */
	public void setVerticalAlignment(String tVertical) {
		if (style == null) {
			style = outputExecleFile.createCellStyle();
		}
		short vertical = style.VERTICAL_BOTTOM;
		if (tVertical.toUpperCase().equals("BOTTON")) {
			vertical = style.ALIGN_RIGHT;
		} else if (tVertical.toUpperCase().equals("TOP")) {
			vertical = style.ALIGN_LEFT;
		} else if (tVertical.toUpperCase().equals("CENTER")) {
			vertical = style.ALIGN_CENTER;
		} else if (tVertical.toUpperCase().equals("JUSTIFY")) {
			vertical = style.ALIGN_FILL;
		}
		style.setAlignment(vertical);
	}

	/**
	 * 是否锁定单元格
	 * 
	 * @param lock
	 *            boolean
	 */
	public void setLocked(boolean lock) {
		if (style == null) {
			style = outputExecleFile.createCellStyle();
		}
		style.setLocked(lock);
	}

	/**
	 * 生成Sheet
	 * 
	 * @param sheetName
	 *            String[]
	 * @return HSSFSheet[]
	 */
	public HSSFSheet[] addSheet(String[] sheetName) {
		sheet = new HSSFSheet[sheetName.length];
		for (int i = 0; i < sheetName.length; i++) {
			if (this.mSheetName == null) {
				mSheetName = sheetName[i];
			}
			sheet[i] = outputExecleFile.createSheet(sheetName[i]);
		}
		return sheet;
	}

	/**
	 * 保存 title
	 * 
	 * @param sheetNum
	 *            int
	 * @param title
	 *            String[]
	 */
	public void setTitle(int sheetNum, String[] title) {
		HSSFRow rowTitle = sheet[sheetNum].createRow(0);
		for (int i = 0; i < title.length; i++) {
			HSSFCell cell = rowTitle.createCell((short) i);
			cell.setEncoding(HSSFCell.ENCODING_UTF_16);
			cell.setCellValue(title[i]);
			if (style != null) {
				cell.setCellStyle(style);
			}
		}
	}

	/**
	 * 将文本要添加的文本添加到Excel中指定位置
	 * 
	 * @param sheetNum
	 *            int 将文本添加到的sheet
	 * @param row
	 *            int 添加到第几行
	 * @param col
	 *            int 添加到第几列
	 * @param data
	 *            String 添加的内容
	 */
	public void setData(int sheetNum, int row, int col, String data) {
		if (sheet.length < sheetNum) {
			HSSFSheet[] tmpSheet = new HSSFSheet[sheetNum];
			for (int i = 0; i < sheet.length; i++) {
				tmpSheet[i] = sheet[i];
			}
			tmpSheet[sheetNum] = outputExecleFile.createSheet(mSheetName + "_"
					+ sheetNum);

			sheet = tmpSheet;
		}
		HSSFRow cellData = sheet[sheetNum].createRow(row);
		HSSFCell cell = cellData.createCell((short) col);
		cell.setEncoding(HSSFCell.ENCODING_UTF_16);
		
		if("null".equals(data)){
			data="";
		}
		cell.setCellValue(data);
		if (style != null) {
			cell.setCellStyle(style);
		}
	}

	/**
	 * 将文本的指定格式添加的文本添加到Excel中指定位置
	 * 
	 * @param sheetNum
	 *            int 将文本添加到的sheet
	 * @param row
	 *            int 添加到第几行
	 * @param col
	 *            int 添加到第几列
	 * @param data
	 *            String 添加的内容
	 * @param style
	 *            HSSFCellStyle 控制单元格的样式,包括是否隐藏,字体,位置
	 */
	public void setData(int sheetNum, int row, int col, String data,
			HSSFCellStyle style) {
		HSSFRow cellData = sheet[sheetNum].createRow(row);
		HSSFCell cell = cellData.createCell((short) col);
		cell.setEncoding(HSSFCell.ENCODING_UTF_16);
		cell.setCellValue(data);
		if (style != null) {
			cell.setCellStyle(style);
		}
	}

	/**
	 * 将一行数据添加到Excel中的指定行中
	 * 
	 * @param sheetNum
	 *            int 添加sheet数
	 * @param row
	 *            int 添加到那一行
	 * @param rowData
	 *            String[] 一行内容
	 */
	public void setRowData(int sheetNum, int row, String[] rowData) {
		for (int i = 0; i < rowData.length; i++) {
			this.setData(sheetNum, row, i, rowData[i]);
		}
	}

	/**
	 * 指定格式的添加
	 * 
	 * @param sheetNum
	 *            int
	 * @param row
	 *            int
	 * @param rowData
	 *            String[]
	 * @param style
	 *            HSSFCellStyle[]
	 */
	public void setRowData(int sheetNum, int row, String[] rowData,
			HSSFCellStyle[] style) {
		for (int i = 0; i < rowData.length; i++) {
			this.setData(sheetNum, row, i, rowData[i], style[i]);
		}
	}

	/**
	 * 将数据添加到指定列中
	 * 
	 * @param sheetNum
	 *            int 添加sheet数
	 * @param col
	 *            int 添加的列数
	 * @param colData
	 *            String[] 某一列的数据
	 */
	public void setColData(int sheetNum, int col, String[] colData) {
		for (int i = 0; i < colData.length; i++) {
			this.setData(sheetNum, i + 1, col, colData[i]);
		}
	}

	/**
	 * 指定格式添加数据
	 * 
	 * @param sheetNum
	 *            int
	 * @param col
	 *            int
	 * @param colData
	 *            String[]
	 * @param style
	 *            HSSFCellStyle[]
	 */
	public void setColData(int sheetNum, int col, String[] colData,
			HSSFCellStyle[] style) {
		for (int i = 0; i < colData.length; i++) {
			this.setData(sheetNum, i + 1, col, colData[i], style[i]);
		}
	}

	/**
	 * 将sql查询的结果显示出来
	 * 
	 * @param sheetNum
	 *            int
	 * @param Sql
	 *            String
	 */
	public void setData(int sheetNum, String Sql) {
		// 执行sql，将执行的sql显示在execl
		ExeSQL tExeSQL = new ExeSQL();
		SSRS ssrs = tExeSQL.execSQL(Sql);
		for (int i = 0; i < ssrs.getMaxRow(); i++) {
			setRowData(sheetNum, i + 1, ssrs.getRowData(i + 1));
		}
	}

	/**
	 * 将sql查询的结果显示出来
	 * 
	 * @param Sql
	 *            String
	 */
	public void setData(String sql) {
		// 执行sql，将执行的sql显示在execl
		// setData(0, Sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;
		DBConn con = null;

		System.out.println("WriteToExcel : " + sql);
		con = DBConnPool.getConnection();

		try {
			pstmt = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery();
			rsmd = rs.getMetaData();

			int n = rsmd.getColumnCount();
			int row = 1;
			int sheet = 0;
			// 取得总记录数
			while (rs.next()) {
				System.out.println("Row:" + row);
				for (int j = 1; j <= n; j++) {
					if (row > 60000) {
						sheet++;
					}
					setData(sheet, row, j, getDataValue(rsmd, rs, j));
				}
				row++;
			}

			try {
				rs.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			try {
				pstmt.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			try {
				con.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} catch (SQLException e) {
			System.out
					.println("### Error WriteToExcel at setData(String sql): "
							+ sql);
			e.printStackTrace();
			CError.buildErr(this, e.toString());
			try {
				if (rs != null) {
					rs.close();
				}
				if (pstmt != null) {
					// 由于描述的问题，导致执行的sql错误百出，因此pstmt的关闭需要特殊处理
					try {
						pstmt.close();
					} catch (SQLException ex) {
						ex.printStackTrace();
					} finally {
						pstmt.close();
					}
				}
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * 将二维数据写入Excel文件
	 * 
	 * @param sheetNum
	 *            int
	 * @param data
	 *            String[][]
	 */
	public void setData(int sheetNum, String[][] data) {
		for (int i = 0; i < data.length; i++) {
			setRowData(sheetNum, i, data[i]);
		}
	}

	/**
	 * 写入指定位置
	 * 
	 * @param path
	 *            String
	 * @throws IOException
	 */
	public void write(String path) throws Exception {
		FileOutputStream fileOut = new FileOutputStream(path + fileName);
		try {
			outputExecleFile.write(fileOut);
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new FileNotFoundException("文件路径读取错误");
		} catch (IOException e) {
			e.printStackTrace();
			throw new IOException("文件正在读取中，写入失败");
		}finally{
			if(fileOut != null) fileOut.close();
		}
	}

	public FileOutputStream getFileOutputStream(String path) throws Exception {
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(path + fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new FileNotFoundException("文件路径读取错误");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception("生成流错误 ！");
		}
		return fileOut;
	}

	public void createExcelFile() {
		// 每new一个WorkBook对象 就生生一个新的Excel
		outputExecleFile = new HSSFWorkbook();
	}

	/**
	 * 调试用
	 * 
	 * @param args
	 *            String[]
	 */
	public static void main(String[] args) {
		// POIFSFileSystem fs;
		// HSSFWorkbook wb;
		WriteToExcel tWriteToExcel = new WriteToExcel();
		String[] sheetName = { "sheet" };
		tWriteToExcel.createExcelFile();
		tWriteToExcel.addSheet(sheetName);
		tWriteToExcel.setTitle(0, new String[] { "1", "2", "3", "4" });
		tWriteToExcel
				.setData("select * from lcpol fetch first 100 rows only with ur");
		try {
			tWriteToExcel.write("c:\\xx.xls");
		} catch (Exception e) {
			e.printStackTrace();
		}
		// try {
		// WriteToExcel t = new WriteToExcel("ceshiyong.xls");
		// t.createExcelFile();
		// String[] sheetName = {
		// "24_Hour_Policy"};
		// t.addSheet(sheetName);
		// String[] title = {
		// "Policy No", "Undersign Date", "Undersign Branch",
		// "Valid from"
		// , "Valid to", "Product code", "Product Level",
		// "Sum of Inpatient Treatment",
		// "Sum of  Emergecy Outpatient Treatment",
		// "Sum of  Emergecy Dental Treatment"
		// , "Insured Name", "Insured No.", "Insured  Chinese Alphabetics",
		// "Birth date"
		// , "Passport or ID No.", "address", "Postal code",
		// "Name of Contact Person In China",
		// "Tel. No. of Contact Person In China"};
		// t.setTitle(0, title);
		// String sql =
		// " select a.grpcontno,a.signdate,a.signcom,a.cvalidate,a.CInValiDate,b.riskcode,b.mult,"
		// +
		// " ( select d1.standmoney from lcinsured a1, lcgrpcont b1, lcpol c1, lcget d1 where a1.grpcontNo=a.grpcontNo and a1.insuredno=e.insuredno and b1.grpcontNo=a1.grpcontNo and c1.grpcontNo=a1.grpcontNo and c1.insuredno=a1.insuredno and c1.riskcode='1502' and d1.grpcontNo=a1.grpcontNo and d1.polno=c1.polno and d1.getdutycode='613207' ),"
		// +
		// " (select d2.standmoney from lcinsured a2, lcgrpcont b2, lcpol c2, lcget d2 where a2.grpcontNo=a.grpcontNo and a2.insuredno=e.insuredno and b2.grpcontNo=a2.grpcontNo and c2.grpcontNo=a2.grpcontNo and c2.insuredno=a2.insuredno and c2.riskcode='1502' and d2.grpcontNo=a2.grpcontNo and d2.polno=c2.polno and d2.getdutycode='613208'),"
		// +
		// " (select d3.standmoney from lcinsured a3, lcgrpcont b3, lcpol c3, lcget d3 where a3.grpcontNo=a.grpcontNo and a3.insuredno=e.insuredno and b3.grpcontNo=a3.grpcontNo and c3.grpcontNo=a3.grpcontNo and c3.insuredno=a3.insuredno and c3.riskcode='1502'and d3.grpcontNo=a3.grpcontNo and d3.polno=c3.polno and d3.getdutycode='613209' ),"
		// +
		// " b.insuredname,b.insuredno,e.EnglishName,e.Birthday,e.OthIDNo," +
		// " d.linkmanaddress1,c.zipcode,d.phone1 from lcgrpcont a, lcpol b,lcgrpappnt c, lcgrpaddress d, lcinsured e "
		// +
		// " where a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno and c.CustomerNo=d.CustomerNo and c.AddressNo=d.AddressNo"
		// +
		// " and a.grpcontno=e.grpcontno and a.cardflag='0' and b.riskcode ='1502' and b.insuredno=e.insuredno and a.appflag='1' and a.grpcontno=e.grpcontno and b.contno=e.contno"
		// +
		// " union all " +
		// " select a.grpcontno,a.signdate,a.signcom,a.cvalidate,a.CInValiDate,b.riskcode,b.mult,"
		// +
		// " ( select d1.standmoney from lcinsured a1, lcgrpcont b1, lcpol c1, lcget d1 where a1.grpcontNo=a.grpcontNo and a1.insuredno=e.insuredno and b1.grpcontNo=a1.grpcontNo and c1.grpcontNo=a1.grpcontNo and c1.insuredno=a1.insuredno and c1.riskcode='1501' and d1.grpcontNo=a1.grpcontNo and d1.polno=c1.polno and d1.getdutycode='611207' ),"
		// +
		// " 0," +
		// " 0," +
		// " b.insuredname,b.insuredno,e.EnglishName,e.Birthday,e.OthIDNo," +
		// " d.linkmanaddress1,c.zipcode,d.phone1 from lcgrpcont a, lcpol b,lcgrpappnt c, lcgrpaddress d, lcinsured e "
		// +
		// " where a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno and c.CustomerNo=d.CustomerNo and c.AddressNo=d.AddressNo"
		// +
		// " and a.grpcontno=e.grpcontno and a.cardflag='0' and b.riskcode='1501' and b.insuredno=e.insuredno and a.appflag='1' and a.grpcontno=e.grpcontno and b.contno=e.contno"
		// ;
		//
		// t.setData(sql);
		// t.write("c:\\");
		// }
		// catch (Exception ex) {
		// ex.printStackTrace();
		// }
		// try {
		// fs = new POIFSFileSystem(new FileInputStream("c:\\Book2.xls"));
		// wb = new HSSFWorkbook(fs);
		// for (int sheetNum = 0; sheetNum < 10; sheetNum++) {
		// HSSFSheet sheet = wb.getSheetAt(sheetNum);
		// for (int row = 0; row < sheet.getLastRowNum(); row++) {
		// HSSFRow hssRow = sheet.getRow(row);
		// if (hssRow != null) {
		// for (int col = 0; col < hssRow.getLastCellNum(); col++) {
		// HSSFCell cell = hssRow.getCell((short) col);
		// if (cell != null) {
		// if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
		//
		// String value = cell.getStringCellValue();
		// if (value != null && !value.equals("")) {
		// if (value.startsWith("$")
		// && value.endsWith("/$")) {
		// String condition = value.substring(
		// 1, value.length() - 2);
		// cell.setCellValue(999999999.99);
		// }
		// }
		// }
		// }
		// }
		// }
		// }
		// }
		// short i = 0;
		// FileOutputStream fileOut = new FileOutputStream("c:\\Creat.xls");
		// wb.write(fileOut);
		// fileOut.flush();
		// fileOut.close();
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }
		// WriteToExcel t = new WriteToExcel("ceshiyong.xls");
		// t.createExcelFile();
		// String[] sheetName = {"ceshi", "000", "234"};
		// t.addSheet(sheetName);
		// // t.setFontName("宋体");
		// String[] title = {"1", "2", "3", "4"};
		// t.setTitle(0, title);
		// String[] rowData = {"测试汉字", "nvnvbnvnvbn", "sfsdsfsdsdfsdf",
		// "sdfsfsdfsdfsdf", "sdfsdfsdfsdfsdf"};
		// // t.setData(0, 1, 0, "；滤色镜分散科技飞速经费；随即放松；龙卷风");
		// // t.setData(0, 1, 1, "；滤龙卷风");
		// t.setData("select ");
		// t.setColData(0, 1, rowData);
		// try {
		// t.write("c:\\");
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }
	}

	/**
	 * 把ResultSet中取出的数据转换为相应的数据值字符串
	 * 输出：如果成功执行，返回True，否则返回False，并且在Error中设置错误的详细信息
	 * 
	 * @param rsmd
	 *            ResultSetMetaData
	 * @param rs
	 *            ResultSet
	 * @param i
	 *            int
	 * @return String
	 */
	public String getDataValue(ResultSetMetaData rsmd, ResultSet rs, int i) {
		String strValue = "";
		FDate fDate = new FDate();
		try {
			int dataType = rsmd.getColumnType(i);
			int dataScale = rsmd.getScale(i);
			int dataPrecision = rsmd.getPrecision(i);
			// 数据类型为字符
			if ((dataType == Types.CHAR) || (dataType == Types.VARCHAR)) {
				// 由于存入数据库的数据是GBK模式，因此没有必要做一次unicodeToGBK
				// strValue = StrTool.unicodeToGBK(rs.getString(i));
				strValue = rs.getString(i);
			}
			// 数据类型为日期、时间
			else if ((dataType == Types.TIMESTAMP) || (dataType == Types.DATE)) {
				strValue = fDate.getString(rs.getDate(i));
			}
			// 数据类型为浮点
			else if ((dataType == Types.DECIMAL) || (dataType == Types.FLOAT)
					|| (dataType == Types.DOUBLE)) {
				// strValue = String.valueOf(rs.getFloat(i));
				// 采用下面的方法使得数据输出的时候不会产生科学计数法样式
				strValue = String.valueOf(rs.getBigDecimal(i));
				// 去零处理
				strValue = PubFun.getInt(strValue);
			}
			// 数据类型为整型
			else if ((dataType == Types.INTEGER)
					|| (dataType == Types.SMALLINT)) {
				strValue = String.valueOf(rs.getInt(i));
				strValue = PubFun.getInt(strValue);
			}
			// 数据类型为浮点
			else if (dataType == Types.NUMERIC) {
				if (dataScale == 0) {
					if (dataPrecision == 0) {
						// strValue = String.valueOf(rs.getDouble(i));
						// 采用下面的方法使得数据输出的时候不会产生科学计数法样式
						strValue = String.valueOf(rs.getBigDecimal(i));
					} else {
						strValue = String.valueOf(rs.getLong(i));
					}
				} else {
					// strValue = String.valueOf(rs.getDouble(i));
					// 采用下面的方法使得数据输出的时候不会产生科学计数法样式
					strValue = String.valueOf(rs.getBigDecimal(i));
				}
				strValue = PubFun.getInt(strValue);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return StrTool.cTrim(strValue);
	}
	
	/**
	 * 生成Sheet 考虑中文问题
	 * 
	 * @param sheetName
	 *            String[]
	 * @return HSSFSheet[]
	 */
	public HSSFSheet[] addSheetGBK(String[] sheetName) {
		sheet = new HSSFSheet[sheetName.length];
		for (int i = 0; i < sheetName.length; i++) {
			if (this.mSheetName == null) {
				mSheetName = sheetName[i];
			}
			sheet[i] = outputExecleFile.createSheet();
			outputExecleFile.setSheetName(i, sheetName[i], HSSFWorkbook.ENCODING_UTF_16);
		}
		return sheet;
	}
}
