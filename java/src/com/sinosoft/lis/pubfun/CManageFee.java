package com.sinosoft.lis.pubfun;

/**
 * <p>Title: Lis </p>
 * <p>Description: Life Insurance System</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft Co. Ltd.</p>
 * @author WUJS
 * @version 6.0
 */

import java.util.HashMap;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class CManageFee
{
    /**存放结果*/
    public VData mVResult = new VData();

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /**管理费描述*/
    private com.sinosoft.lis.vschema.LCGrpFeeSet mLCGrpFeeSet;

    /**保费项表*/
    private com.sinosoft.lis.vschema.LCPremSet mLCPremSet;

    private LCInsureAccFeeTraceSet mLCInsureAccFeeTraceSet;

    /**险种*/
    private com.sinosoft.lis.schema.LCPolSchema mLCPolSchema;

    /**保险账户管理费分类表*/
    private com.sinosoft.lis.vschema.LCInsureAccClassFeeSet mLCInsureAccClassFeeSet;

    private LCInsureAccFeeSet mLCInsureAccFeeSet;

    private LCInsureAccSet mLCInsureAccSet;

    private LCPremToAccSet mLCPremToAccSet;

    private LCInsureAccClassSet mInsureAccClassSet;

    //   private LCGetToAccSet mLCGetToAccSet;
    private LCInsureAccTraceSet mLCInsureAccTraceSet;

    /** 管理费缓存 */
    private HashMap LMRiskFeeMap = new HashMap();

    /** 计划相关要素 */
    private LCContPlanDutyParamSet mLCContPlanDutyParamSet = null;

    public CManageFee()
    {

    }

    /**
     * 初始化
     * @param cInputData VData
     */
    private void Initialize(VData cInputData)
    {
        mInputData = (VData) cInputData;
        //        mLCGrpFeeSet = (LCGrpFeeSet) mInputData.getObjectByObjectName(
        //                "LCGrpFeeSet", 0);
        mLCPolSchema = (LCPolSchema) mInputData.getObjectByObjectName("LCPolSchema", 0);
        mLCInsureAccClassFeeSet = (LCInsureAccClassFeeSet) mInputData
                .getObjectByObjectName("LCInsureAccClassFeeSet", 0);
        mLCInsureAccFeeSet = (LCInsureAccFeeSet) mInputData.getObjectByObjectName("LCInsureAccFeeSet", 0);
        mLCPremSet = (LCPremSet) mInputData.getObjectByObjectName("LCPremSet", 0);
        //得到生成的保险帐户表
        mLCInsureAccSet = (LCInsureAccSet) (mInputData.getObjectByObjectName("LCInsureAccSet", 0));

        //得到生成的缴费帐户关联表
        mLCPremToAccSet = (LCPremToAccSet) (mInputData.getObjectByObjectName("LCPremToAccSet", 0));
        //生成帐户管理费轨迹表
        mLCInsureAccFeeTraceSet = (LCInsureAccFeeTraceSet) mInputData
                .getObjectByObjectName("LCInsureAccFeeTraceSet", 0);
        mInsureAccClassSet = (LCInsureAccClassSet) mInputData.getObjectByObjectName("LCInsureAccClassSet", 0);

        //得到领取帐户关联表--目前不用
        //        mLCGetToAccSet = (LCGetToAccSet) (mInputData
        //                                          .getObjectByObjectName(
        //                "LCGetToAccSet",
        //                0));
        mLCInsureAccTraceSet = (LCInsureAccTraceSet) mInputData.getObjectByObjectName("LCInsureAccTraceSet", 0);

        mLCContPlanDutyParamSet = (LCContPlanDutyParamSet) mInputData
                .getObjectByObjectName("LCContPlanDutyParamSet", 0);

        //  if ( mLCInsureAccTraceSet==null ) mLCInsureAccTraceSet = new LCInsureAccTraceSet();
        if (mLCPolSchema == null)
        {
            CError.buildErr(this, "传入保单信息不能为空");
            // return false;
        }
        else
        {
            LCGrpFeeDB tLCGrpFeeDB = new LCGrpFeeDB();
            tLCGrpFeeDB.setGrpPolNo(mLCPolSchema.getGrpPolNo());
            tLCGrpFeeDB.setRiskCode(mLCPolSchema.getRiskCode());
            mLCGrpFeeSet = tLCGrpFeeDB.query();
        }

    }

    public VData getResult()
    {
        //        mInputData.add(mLCInsureAccTraceSet);
        //        return mInputData;
        return mInputData;
    }

    /**
     * 数据校验
     * @return boolean
     */
    private boolean checkData()
    {

        if (mInputData == null)
        {
            System.out.println("请先调用初始化函数:Initialize");
            CError.buildErr(this, "请先调用初始化函数:Initialize");
            return false;
        }
        if (mLCPolSchema == null || mLCInsureAccClassFeeSet == null)
        {
            CError.buildErr(this, "初始化信息不正确!");
            return false;
        }

        if (mLCPremSet == null)
        {
            CError.buildErr(this, "必须传入保费信息");
            return false;
        }

        if (mLCGrpFeeSet == null)
        {
            CError.buildErr(this, "初始化集体险种管理费描述信息");
            return false;
        }

        return true;

    }

    /**
     * 从数据苦中查询保单对应需要处理账户的所有保费项
     * @param tLCPolSchema LCPolSchema
     * @return LCPremSet
     */
    private LCPremSet queryLCPremSet(LCPolSchema tLCPolSchema)
    {

        LCPremSet tLCPremSet = new LCPremSet();
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
        tLCPremDB.setNeedAcc("1");
        tLCPremSet = tLCPremDB.query();
        if (tLCPremDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPremDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "DealAccount";
            tError.functionName = "queryLCPrem";
            tError.errorMessage = "保费项表查询失败!";
            this.mErrors.addOneError(tError);
            tLCPremSet.clear();

            return null;
        }
        if (tLCPremSet.size() == 0)
        {
            return null;
        }

        return tLCPremSet;

    }

    /**
     * 查询匹配的保费项
     * @param payPlanCode String
     * @return LCPremSchema
     */
    private LCPremSchema getLCPremSchema(String payPlanCode)
    {
        if (mLCPremSet == null)
        {
            CError.buildErr(this, "没有保费项信息!");
            return null;
        }
        for (int i = 1; i <= mLCPremSet.size(); i++)
        {
            if (mLCPremSet.get(i).getPayPlanCode().equals(payPlanCode))
            {
                return mLCPremSet.get(i);
            }
        }
        return null;
    }

    /**
     * 查询匹配的保险账户管理费分类表
     * @param InsureAccNo String
     * @param payPlanCode String
     * @return LCInsureAccClassFeeSchema
     */
    private LCInsureAccClassFeeSchema getLCInsureAccClassFee(String InsureAccNo, String payPlanCode)
    {
        LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = null;
        for (int i = 1; i <= mLCInsureAccClassFeeSet.size(); i++)
        {
            tLCInsureAccClassFeeSchema = mLCInsureAccClassFeeSet.get(i);
            if (tLCInsureAccClassFeeSchema.getInsuAccNo().equals(InsureAccNo)
                    && tLCInsureAccClassFeeSchema.getPayPlanCode().equals(payPlanCode))
            {

                break;
            }

        }
        return tLCInsureAccClassFeeSchema;
    }

    /**
     * 查询匹配的保险账户管理费分类表
     * @param InsureAccNo String
     * @param payPlanCode String
     * @return LCInsureAccClassFeeSchema
     */
    private LCInsureAccFeeSchema getLCInsureAccFee(String InsureAccNo)
    {
        LCInsureAccFeeSchema tLCInsureAccFeeSchema = null;
        for (int i = 1; i <= mLCInsureAccClassFeeSet.size(); i++)
        {
            tLCInsureAccFeeSchema = mLCInsureAccFeeSet.get(i);
            if (tLCInsureAccFeeSchema.getInsuAccNo().equals(InsureAccNo))
            {
                break;
            }

        }
        return tLCInsureAccFeeSchema;
    }

    /**
     * 查询团单管理费计算参数
     * @param InsureAccNo String
     * @param payPlanCode String
     * @param feebase double
     * @return LCGrpFeeParamSchema
     */
    private LCGrpFeeParamSchema queryLCGrpFeeParamSchema(String InsureAccNo, String payPlanCode, double feebase)
    {
        //String sql = "select * from LCGrpFeeParam where ";
        StringBuffer sb = new StringBuffer();
        sb.append(" select * from LCGrpFeeParam where ");
        sb.append("GrpPolNo='").append(mLCPolSchema.getGrpPolNo()).append("'");
        sb.append(" and InsuAccNo='").append(InsureAccNo).append("'");
        sb.append(" and PayPlanCode='").append(payPlanCode).append("'");
        sb.append(" and FeeMin<").append(feebase);
        sb.append(" and feemax>").append(feebase);
        LCGrpFeeParamDB tLCGrpFeeParamDB = new LCGrpFeeParamDB();
        LCGrpFeeParamSet tLCGrpFeeParamSet = tLCGrpFeeParamDB.executeQuery(sb.toString());
        if (tLCGrpFeeParamDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLCGrpFeeParamDB.mErrors);
            return null;
        }
        if (tLCGrpFeeParamSet == null || tLCGrpFeeParamSet.size() != 1)
        {
            CError.buildErr(this, "参数表描述取值不唯一!");
            return null;
        }

        return tLCGrpFeeParamSet.get(1);

    }

    /**
     * 计算管理费内扣费率
     * @param manaFee double
     * @param prem double
     * @return double
     */
    private double calInnerRate(double manaFee, double prem)
    {
        return manaFee / prem;
    }

    /**
     * 计算管理费外缴费率
     * @param manaFee double
     * @param prem double
     * @return double
     */
    private double calOutRate(double manaFee, double prem)
    {
        return manaFee / (prem - manaFee);
    }

    /**
     * 计算管理费 － 内扣
     * @param prem double
     * @param rate double
     * @return double
     */
    private double calInnerManaFee(double prem, double rate)
    {
        return prem * rate;
    }

    /**
     * 计算管理费 -外缴
     * @param prem double
     * @param rate double
     * @return double
     */
    private double calOutManaFee(double prem, double rate)
    {
        return (prem * rate) / (1 + rate);
    }

    /**
     * 固定值和比例结合，取较小值
     * @param calMode String
     * @param basePrem double
     * @param fixValue double
     * @param rate double
     * @param refValue double
     * @return double
     */
    private double calManaFeeMinRate(double basePrem, double fixValue, double rate)
    {
        double manaFee = 0;
        manaFee = this.calInnerManaFee(basePrem, rate);
        if (fixValue < manaFee)
        {
            return fixValue;
        }
        return manaFee;
    }

    /**
     * 固定值和比例结合，取较大值
     * @param calMode String
     * @param basePrem double
     * @param fixValue double
     * @param rate double
     * @param refValue double
     * @return double
     */
    private double calManaFeeMaxRate(double basePrem, double fixValue, double rate)
    {
        double manaFee = 0;
        manaFee = this.calInnerManaFee(basePrem, rate);
        if (manaFee < fixValue)
        {
            return fixValue;
        }
        return manaFee;
    }

    /**
     * 该方法有缺陷，不能用于签单程序，因为传入的数据中的得到的是保单号，可是库中的数据是
     * 尚未签单的数据，只有投保单号。
     * 保险账户资金注入(类型3 针对保费项,注意没有给出注入资金，内部会调用计算金额的函数)
     * 适用于：在生成帐户结构后，此时数据尚未提交到数据库，又需要执行帐户的资金注入。
     * 即在使用了 createInsureAcc()方法后，得到VData数据，接着修改VData中帐户的金额
     * @param inVData       使用了 createInsureAcc()方法后，得到的VData数据
     * @param pLCPremSet    保费项集合
     * @param AccCreatePos  参见 险种保险帐户缴费 LMRiskAccPay
     * @param OtherNo       参见 保险帐户表 LCInsureAcc
     * @param OtherNoType   号码类型
     * @param MoneyType     参见 保险帐户表记价履历表 LCInsureAccTrace
     * @param Rate          费率
     * @return VData(tLCInsureAccSet:update or insert ,tLCInsureAccTraceSet: insert)
     */
    public boolean calPremManaFee(VData tVData, String AccCreatePos, String OtherNo, String OtherNoType,
            String MoneyType)
    {
        if ((tVData == null) || (AccCreatePos == null) || (OtherNo == null) || (OtherNoType == null)
                || (MoneyType == null))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DealAccount";
            tError.functionName = "addPrem";
            tError.errorMessage = "传入数据不能为空";
            this.mErrors.addOneError(tError);

            return false;
        }
        this.Initialize(tVData);
        if (!checkData())
        {
            return false;
        }

        LCInsureAccClassSchema tClassSchema = null;

        for (int n = 1; n <= mLCPremSet.size(); n++)
        {
            LCPremSchema tLCPremSchema = mLCPremSet.get(n);

            //判断是否帐户相关
            if (!"1".equals(tLCPremSchema.getNeedAcc()))
            {
                continue;
            }

            for (int m = 1; m <= mLCPremToAccSet.size(); m++)
            {
                LCPremToAccSchema tLCPremToAccSchema = mLCPremToAccSet.get(m);

                //如果当前保费项和当前的缴费帐户关联表的保单号，责任编码，交费计划编码相同
                if (!tLCPremSchema.getPolNo().equals(tLCPremToAccSchema.getPolNo())
                        || !tLCPremSchema.getDutyCode().equals(tLCPremToAccSchema.getDutyCode())
                        || !tLCPremSchema.getPayPlanCode().equals(tLCPremToAccSchema.getPayPlanCode()))
                {
                    continue;
                }

                //只有注入比例大于0的时候才注入账户金额,否则不做
                if (tLCPremToAccSchema.getRate() <= 0)
                {
                    continue;
                }
                double inputMoney = 0;
                double manaFee = 0;
                double baseMoney = tLCPremSchema.getPrem() * tLCPremToAccSchema.getRate();
                //直接计算，不需要管理费
                if (tLCPremToAccSchema.getCalFlag() == null || "0".equals(tLCPremToAccSchema.getCalFlag()))
                {
                    inputMoney = baseMoney;

                }
                else
                {
                    if ("1".equals(tLCPremToAccSchema.getCalFlag()))
                    {
                        manaFee = computeManaFee(baseMoney, tLCPremToAccSchema);
                    }
                    if (manaFee < 0)
                    {
                        CError.buildErr(this, "管理费计算失败!");
                        return false;
                    }

                }
                /*-------------以后这个分支就是个险管理费计算的分支--------start-------*/
                double tFeeValue = 0; //管理费比例
                //                LCContDB mLCContDB = new LCContDB();
                //                System.out.println(tLCPremSchema.getContNo());
                //                mLCContDB.setContNo(mLCPolSchema.getContNo());
                //                if (mLCContDB.getInfo() == false)
                //                {
                //                	CError.buildErr(this, "查询合同失败!");
                //                    return false;
                //                }	

                if (manaFee == 0)
                {
                    double tInitFeeRate = 0;
                    if (mLCPolSchema != null)
                    {
                        tInitFeeRate = mLCPolSchema.getInitFeeRate();
                    }
                    manaFee = computeManaFee(tLCPremToAccSchema.getInsuAccNo(), tLCPremToAccSchema.getPayPlanCode(),
                            inputMoney, tInitFeeRate);
                }

                // 处理追加保费。计算追加保费管理费。
                double tDSupplementaryPrem = 0d;
                double tDSupplementaryPremFee = 0d;

                if (com.sinosoft.lis.bq.CommonBL.isULIRisk(mLCPolSchema.getRiskCode()))
                {
                    tDSupplementaryPrem = mLCPolSchema.getSupplementaryPrem();

                    // 如果存在追加保费，进行帐户处理。
                    if (tDSupplementaryPrem > 0d)
                    {
                        // 追加保费金额，存入帐户。
                        updateLCInsuerAccClass(null, tDSupplementaryPrem, tLCPremToAccSchema);
                        updateInsureAcc(OtherNo, OtherNoType, "ZF", tDSupplementaryPrem, tLCPremSchema,
                                tLCPremToAccSchema);
                        // ------------------------------------

                        // 计算追加保费的初始手续费用。
                        tDSupplementaryPremFee = computeSupplementaryPremFee(tLCPremToAccSchema.getInsuAccNo(),
                                tLCPremToAccSchema.getPayPlanCode(), tDSupplementaryPrem);
                        tDSupplementaryPremFee = PubFun.setPrecision(tDSupplementaryPremFee, "0.00");
                        // ------------------------------------

                        // 存入手续费帐户。
                        updateLCInsureAccFee(tLCPremToAccSchema.getInsuAccNo(), tDSupplementaryPremFee);
                        updateLCInsureAccClassFee(tLCPremToAccSchema.getInsuAccNo(), tDSupplementaryPremFee,
                                tLCPremToAccSchema.getPayPlanCode());
                        // ------------------------------------

                        // 手续费在帐户中充负。
                        updateLCInsuerAccClass(null, Double.parseDouble("-" + tDSupplementaryPremFee),
                                tLCPremToAccSchema);
                        updateInsureAcc(OtherNo, OtherNoType, "KF", Double.parseDouble("-" + tDSupplementaryPremFee),
                                tLCPremSchema, tLCPremToAccSchema);
                        // ------------------------------------
                    }
                }
                // --------------------------------

                /*-------------以后这个分支就是个险管理费计算的分支-------- end -------*/

                PubFun.setPrecision(manaFee, "0.00");
                //                by gzh 20110720 存入账户表数据由于由总费用减去管理费所得，先对管理费进行四舍五入，否则，当存在0.005时，差一分钱。
                inputMoney = tLCPremSchema.getPrem() - Arith.round(manaFee, 2);
                if (inputMoney < 0)
                {
                    CError.buildErr(this, "管理费计算错误,不能大于保费");
                    return false;
                }
                PubFun.setPrecision(inputMoney, "0.00");
                //更新管理费账户表
                updateLCInsureAccFee(tLCPremToAccSchema.getInsuAccNo(), manaFee);
                //更新管理费分类表账户表
                //                updateLCInsureAccClassFee(tLCPremToAccSchema.getInsuAccNo(),
                //                        manaFee, tLCPremToAccSchema.getPayPlanCode());

                //更新账户分类表
                updateLCInsuerAccClass(tClassSchema, inputMoney, tLCPremToAccSchema);
                //更新账户表
                updateInsureAcc(OtherNo, OtherNoType, MoneyType, inputMoney, tLCPremSchema, tLCPremToAccSchema);

            }
        }

        return true;
    }

    /**
     * 计算管理费(万能险)
     * @param pLCInsureAccClassSchema 子帐户信息
     * @param pLMRiskFeeSchema  管理费描述信息
     * @return double
     */
    private double computeManaFee(String sInsuAccNo, String sPayPlanCode, double dSumPrem, double cInitFeeRate)
    {
        double dClassFee = 0.0;

        //查询管理费
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(sInsuAccNo);
        tLMRiskFeeDB.setPayPlanCode(sPayPlanCode);
        tLMRiskFeeDB.setFeeKind("03"); //03-个单管理费
        tLMRiskFeeDB.setFeeTakePlace("01"); //01－交费时
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        if (tLMRiskFeeDB.mErrors.needDealError())
        {
            CError.buildErr(this, "账户管理费查询失败!");
            return -1;
        }
        if (tLMRiskFeeSet != null && tLMRiskFeeSet.size() > 0)
        {
            double dRiskFee;
            String feetype="GL";
            for (int k = 1; k <= tLMRiskFeeSet.size(); k++) //循环计算每条管理费
            {
                //计算管理费金额
                dRiskFee = calRiskFee(tLMRiskFeeSet.get(k), dSumPrem, cInitFeeRate);
                if (dRiskFee == -1)
                {
                    return -1;
                }
                LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = getLCInsureAccClassFee(sInsuAccNo, sPayPlanCode);
                
                if("000099".equals(tLMRiskFeeSet.get(k).getFeeCode())){
                	feetype="RP";
                }else{
                	feetype="GL";
                }
                //创建保险帐户管理费轨迹记表
                createFeeTrace(tLCInsureAccClassFeeSchema, dRiskFee, feetype, tLMRiskFeeSet.get(k).getFeeCode());

                dClassFee += dRiskFee;
            }
        }
        return dClassFee;
    }

    /**
     * 计算契约追加保费初始手续费。
     * @param sInsuAccNo
     * @param sPayPlanCode
     * @param dSumPrem
     * @return
     */
    private double computeSupplementaryPremFee(String sInsuAccNo, String sPayPlanCode, double dSumPrem)
    {
        double dClassFee = 0.0;

        //查询管理费
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(sInsuAccNo);
        tLMRiskFeeDB.setPayPlanCode(sPayPlanCode);
        tLMRiskFeeDB.setFeeKind("03"); //03-个单管理费
        tLMRiskFeeDB.setFeeTakePlace("06"); //06－追加保费初始扣费
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        if (tLMRiskFeeDB.mErrors.needDealError())
        {
            CError.buildErr(this, "账户管理费查询失败!");
            return -1;
        }
        if (tLMRiskFeeSet != null && tLMRiskFeeSet.size() > 0)
        {
            double dRiskFee;
            for (int k = 1; k <= tLMRiskFeeSet.size(); k++) //循环计算每条管理费
            {
                //计算管理费金额
                dRiskFee = calRiskFee(tLMRiskFeeSet.get(k), dSumPrem);
                if (dRiskFee == -1)
                {
                    return -1;
                }
                LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = getLCInsureAccClassFee(sInsuAccNo, sPayPlanCode);

                //创建保险帐户管理费轨迹记表
                createFeeTrace(tLCInsureAccClassFeeSchema, dRiskFee, "KF", tLMRiskFeeSet.get(k).getFeeCode());

                dClassFee += dRiskFee;
            }
        }
        return dClassFee;
    }

    /**
     * 创建结算轨迹记录 add by zhangtao 2006-11-21
     * @param pLCInsureAccClassFeeSchema
     * @param dMoney      结算金额
     * @param sMoneyType  金额类型
     * @param sFeeCode    管理费编码
     * @return boolean
     */
    private boolean createFeeTrace(LCInsureAccClassFeeSchema pLCInsureAccClassFeeSchema, double dMoney,
            String sMoneyType, String sFeeCode)
    {
        Reflections ref = new Reflections();
        //创建帐户轨迹记录
        LCInsureAccFeeTraceSchema tLCInsureAccFeeTraceSchema = new LCInsureAccFeeTraceSchema();
        ref.transFields(tLCInsureAccFeeTraceSchema, pLCInsureAccClassFeeSchema);
        String tLimit = PubFun.getNoLimit(pLCInsureAccClassFeeSchema.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

        tLCInsureAccFeeTraceSchema.setSerialNo(serNo);

        tLCInsureAccFeeTraceSchema.setMoneyType(sMoneyType);
        tLCInsureAccFeeTraceSchema.setState("0");
        tLCInsureAccFeeTraceSchema.setFee(dMoney);
        tLCInsureAccFeeTraceSchema.setPayDate(pLCInsureAccClassFeeSchema.getBalaDate());
        tLCInsureAccFeeTraceSchema.setFeeCode(sFeeCode);

        mLCInsureAccFeeTraceSet.add(tLCInsureAccFeeTraceSchema);
        return true;
    }

    /**
     * updateLCInsureAccClassFee
     *
     * @param tInsuAccNo String
     * @param manaFee double
     */
    private void updateLCInsureAccClassFee(String tInsuAccNo, double manaFee, String tPayPlanCode)
    {
        //管理非费账户
        LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = getLCInsureAccClassFee(tInsuAccNo, tPayPlanCode);
        if (tLCInsureAccClassFeeSchema == null)
        {
            System.out.println("没有从查找到对应的管理费账户");
            //return null;
        }
        tLCInsureAccClassFeeSchema.setFee(PubFun.setPrecision(tLCInsureAccClassFeeSchema.getFee() + manaFee, "0.00"));

    }

    /**
     * 计算管理费(万能险)
     * @param pLCInsureAccClassSchema 子帐户信息
     * @param pLMRiskFeeSchema  管理费描述信息
     * @return double
     */
    private double calRiskFee(LMRiskFeeSchema pLMRiskFeeSchema, double dSumPrem)
    {
        return calRiskFee(pLMRiskFeeSchema, dSumPrem, 0);
    }

    /**
     * 计算管理费(万能险)
     * @param pLCInsureAccClassSchema 子帐户信息
     * @param pLMRiskFeeSchema  管理费描述信息
     * @return double
     */
    private double calRiskFee(LMRiskFeeSchema pLMRiskFeeSchema, double dSumPrem, double cInitFeeRate)
    {
        double dRiskFee = 0.0;
        if (pLMRiskFeeSchema.getFeeCalModeType().equals("0")) //0-直接取值
        {
            if (pLMRiskFeeSchema.getFeeCalMode().equals("01")) //固定值内扣
            {
                dRiskFee = pLMRiskFeeSchema.getFeeValue();
            }
            else if (pLMRiskFeeSchema.getFeeCalMode().equals("02")) //固定比例内扣
            {
                dRiskFee = dSumPrem * pLMRiskFeeSchema.getFeeValue();
            }
            else
            {
                dRiskFee = pLMRiskFeeSchema.getFeeValue(); //默认情况
            }
        }
        else if (pLMRiskFeeSchema.getFeeCalModeType().equals("1")) //1-SQL算法描述
        {
            //准备计算要素
            Calculator tCalculator = new Calculator();
            tCalculator.setCalCode(pLMRiskFeeSchema.getFeeCalCode());
            //累计已交保费
            tCalculator.addBasicFactor("Prem", String.valueOf(dSumPrem));
            tCalculator.addBasicFactor("PayTimes", "1"); //首期是1
            //下面三个要素用来计算个险万能首期交费的初始费用计算。added by huxl @20080512
            tCalculator.addBasicFactor("Amnt", String.valueOf(mLCPolSchema.getAmnt()));
            tCalculator.addBasicFactor("PayIntv", String.valueOf(mLCPolSchema.getPayIntv()));
            tCalculator.addBasicFactor("CountTime", "1"); //首期是1
            tCalculator.addBasicFactor("AppAge", String.valueOf(mLCPolSchema.getInsuredAppAge()));
            tCalculator.addBasicFactor("Sex", String.valueOf(mLCPolSchema.getInsuredSex()));
            tCalculator.addBasicFactor("polno", String.valueOf(mLCPolSchema.getProposalNo()));
            
            // 传递万能初始费率
            tCalculator.addBasicFactor("InitFeeRate", String.valueOf(cInitFeeRate));
            // --------------------
            //add by lxs 2016-12-13
            LCContSubDB tLCContSubDB=new LCContSubDB();
            tLCContSubDB.setPrtNo(mLCPolSchema.getPrtNo());
            if(tLCContSubDB.getInfo()){
    			String Totaldiscountfactor=tLCContSubDB.getTotaldiscountfactor();
    			if (Totaldiscountfactor != ""&&Totaldiscountfactor!= "null"&&Totaldiscountfactor!=null) {
    				 tCalculator.addBasicFactor("Totaldiscountfactor",Totaldiscountfactor);
    			} 
    			else {
    				 tCalculator.addBasicFactor("Totaldiscountfactor","1");
    				 }
    			}else{
            	 tCalculator.addBasicFactor("Totaldiscountfactor","1");
            }
            String sCalResultValue = tCalculator.calculate();
            if (tCalculator.mErrors.needDealError())
            {
                CError.buildErr(this, "管理费计算失败!");
                return -1;
            }

            try
            {
                dRiskFee = Double.parseDouble(sCalResultValue);
            }
            catch (Exception e)
            {
                CError.buildErr(this, "管理费计算结果错误!" + "错误结果：" + sCalResultValue);
                return -1;
            }
        }

        return dRiskFee;
    }

    /**
     * 计算管理费
     * @param tLCPremSchema LCPremSchema
     * @param tLCPremToAccSchema LCPremToAccSchema
     * @return boolean
     */
    private double computeManaFee(double baseMoney, LCPremToAccSchema tLCPremToAccSchema)
    {
        double manaFee = 0;
        double manaFeeRate = 0;
        if (mLCGrpFeeSet == null)
        {
            System.out.println("目前只支持团单管理费计算");
            return 0;
        }
        //需要计算管理非
        for (int t = 1; t <= mLCInsureAccClassFeeSet.size(); t++)
        {
            LCInsureAccClassFeeSchema tClsFeeSchema = mLCInsureAccClassFeeSet.get(t);
            //查找相关的管理费账户分类表
            if (tClsFeeSchema.getPolNo().equals(tLCPremToAccSchema.getPolNo())
                    && tClsFeeSchema.getInsuAccNo().equals(tLCPremToAccSchema.getInsuAccNo())
                    && tClsFeeSchema.getPayPlanCode().equals(tLCPremToAccSchema.getPayPlanCode()))
            {
                for (int u = 1; u <= mLCGrpFeeSet.size(); u++)
                {
                    LCGrpFeeSchema tFeeSchema = mLCGrpFeeSet.get(u);

                    String key = getLMRiskFeeKey(tFeeSchema);
                    LMRiskFeeMap.get(key);
                    LMRiskFeeDB tLMRiskFeeDB = (LMRiskFeeDB) LMRiskFeeMap.get(key);
                    if (tLMRiskFeeDB == null)
                    {
                        tLMRiskFeeDB = new LMRiskFeeDB();
                        tLMRiskFeeDB.setFeeCode(tFeeSchema.getFeeCode());
                        tLMRiskFeeDB.setInsuAccNo(tFeeSchema.getInsuAccNo());
                        tLMRiskFeeDB.setPayPlanCode(tFeeSchema.getPayPlanCode());
                        tLMRiskFeeDB.setFeeCalMode(tFeeSchema.getFeeCalMode());
                        if (!tLMRiskFeeDB.getInfo())
                        {
                            String str = "查询管理费描述失败!";
                            buildError("computeManaFee", str);
                            System.out.println("在程序CManageFee.computeManaFee() - 523 : " + str);
                            return 0.0;
                        }
                        LMRiskFeeMap.put(key, tLMRiskFeeDB);
                    }

                    //查找相关的管理费描述描述
                    if (tFeeSchema.getGrpPolNo().equals(tClsFeeSchema.getGrpPolNo())
                            && tFeeSchema.getPayPlanCode().equals(tClsFeeSchema.getPayPlanCode())
                            && tFeeSchema.getInsuAccNo().equals(tClsFeeSchema.getInsuAccNo())
                            && tLMRiskFeeDB.getFeeTakePlace().equals("01"))
                    {
                        String calMode = tFeeSchema.getFeeCalMode();

                        if (calMode == null || "01".equals(calMode))
                        { //内扣固定值
                            manaFee = tFeeSchema.getFeeValue();
                            manaFeeRate = this.calInnerRate(manaFee, baseMoney);
                        }
                        else if (calMode.equals("02"))
                        { //内扣比例
                            manaFeeRate = tFeeSchema.getFeeValue();
                            manaFee = this.calInnerManaFee(baseMoney, manaFeeRate);

                        }
                        else if (calMode.equals("03"))
                        { //外缴-固定值
                            manaFee = tFeeSchema.getFeeValue();
                            manaFeeRate = this.calOutRate(manaFee, baseMoney);
                        }
                        else if (calMode.equals("04"))
                        { //外缴-比例值
                            manaFeeRate = tFeeSchema.getFeeValue();
                            manaFee = this.calOutManaFee(baseMoney, manaFeeRate);
                        }
                        else if (calMode.equals("05"))
                        { //固定值，比例计算后取较小值
                            manaFeeRate = tFeeSchema.getFeeValue();
                            manaFee = this.calManaFeeMinRate(baseMoney, tFeeSchema.getCompareValue(), manaFeeRate);
                        }
                        else if (calMode.equals("06"))
                        { //固定值，比例计算后取较大值
                            manaFeeRate = tFeeSchema.getFeeValue();
                            manaFee = this.calManaFeeMaxRate(baseMoney, tFeeSchema.getCompareValue(), manaFeeRate);
                        }
                        else if (calMode.equals("07"))
                        { //分档计算
                            LCGrpFeeParamSchema tLCGrpFeeParamSchema = this.queryLCGrpFeeParamSchema(tFeeSchema
                                    .getInsuAccNo(), tFeeSchema.getPayPlanCode(), baseMoney);
                            manaFeeRate = tLCGrpFeeParamSchema.getFeeRate();
                            manaFee = this.calInnerManaFee(baseMoney, manaFeeRate);
                        }
                        else if (calMode.equals("08"))
                        { //累计分党计算
                            //需求不明确，尚未完成
                            manaFeeRate = 0;
                            manaFee = 0;
                        }
                        // 根据要素确定管理费计算方式
                        else if (calMode.equals("09"))
                        {
                            String tChargeFeeRateType = null;
                            String tRiskCode = "";
                            if (mLCContPlanDutyParamSet != null)
                            {
                                for (int idx = 1; idx <= mLCContPlanDutyParamSet.size(); idx++)
                                {
                                    LCContPlanDutyParamSchema tTmpPlanParam = null;
                                    tTmpPlanParam = mLCContPlanDutyParamSet.get(idx);
                                    if ("ChargeFeeRateType".equals(tTmpPlanParam.getCalFactor())
                                            && StrTool.cTrim(tFeeSchema.getInsuAccNo()).equals(
                                                    tTmpPlanParam.getInsuAccNo()))
                                    {
                                        tChargeFeeRateType = tTmpPlanParam.getCalFactorValue();
                                        tRiskCode = tTmpPlanParam.getRiskCode();
                                        break;
                                    }
                                }
                            }
                            
                            LDCodeDB tLDCodeDB = new LDCodeDB();
                        	tLDCodeDB.setCodeType("chargefeeratetype");
                        	tLDCodeDB.setCode(tRiskCode);
                        	boolean isK= tLDCodeDB.getInfo();

                            if ("02".equals(tChargeFeeRateType))
                            {
                                manaFeeRate = tFeeSchema.getFeeValue();
                                manaFee = this.calInnerManaFee(baseMoney, manaFeeRate);
                            }
                            else if ("04".equals(tChargeFeeRateType)&&isK)
                            {
                                manaFeeRate = tFeeSchema.getFeeValue();
                                manaFee = this.calOutManaFee(baseMoney, manaFeeRate);
                            }else if("04".equals(tChargeFeeRateType)){
                            	manaFeeRate = tFeeSchema.getFeeValue();
                                manaFee = 0;
                            }

                        }
                        // ----------------------
                        LCInsureAccFeeTraceSchema tLCInsureAccFeeTraceSchema = this.createAccFeeTrace(manaFee,
                                tFeeSchema, tClsFeeSchema);
                        mLCInsureAccFeeTraceSet.add(tLCInsureAccFeeTraceSchema);

                        break;
                    }

                }
                //                tClsFeeSchema.setFee(PubFun.setPrecision(manaFee, "0.00"));
                //                by gzh 20110720 修改为四舍五入
                tClsFeeSchema.setFee(Arith.round(manaFee, 2));
                tClsFeeSchema.setFeeRate(PubFun.setPrecision(manaFeeRate, "0.000000"));

                break;
            }

        }
        return manaFee;
    }

    /**
     * 得到管理费账户轨迹表信息.
     * @param fee double 管理费
     * @param tLCGrpFeeSchema LCGrpFeeSchema
     * @param tLCInsureAccClassFeeSchema LCInsureAccClassFeeSchema
     * @return LCInsureAccFeeTraceSchema
     */
    private LCInsureAccFeeTraceSchema createAccFeeTrace(double fee, LCGrpFeeSchema tLCGrpFeeSchema,
            LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema)
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        String tLimit = PubFun.getNoLimit(tLCInsureAccClassFeeSchema.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        LCInsureAccFeeTraceSchema tLCInsureAccFeeTraceSchema = new LCInsureAccFeeTraceSchema();
        tLCInsureAccFeeTraceSchema.setSerialNo(serNo);
        tLCInsureAccFeeTraceSchema.setPolNo(tLCInsureAccClassFeeSchema.getPolNo());
        tLCInsureAccFeeTraceSchema.setGrpContNo(tLCInsureAccClassFeeSchema.getGrpContNo());
        tLCInsureAccFeeTraceSchema.setGrpPolNo(tLCInsureAccClassFeeSchema.getGrpPolNo());
        tLCInsureAccFeeTraceSchema.setContNo(tLCInsureAccClassFeeSchema.getContNo());
        tLCInsureAccFeeTraceSchema.setInsuAccNo(tLCInsureAccClassFeeSchema.getInsuAccNo());
        tLCInsureAccFeeTraceSchema.setRiskCode(tLCInsureAccClassFeeSchema.getRiskCode());
        tLCInsureAccFeeTraceSchema.setPayPlanCode(tLCInsureAccClassFeeSchema.getPayPlanCode());
        tLCInsureAccFeeTraceSchema.setOtherNo(tLCInsureAccClassFeeSchema.getOtherNo());
        tLCInsureAccFeeTraceSchema.setOtherType(tLCInsureAccClassFeeSchema.getOtherType());
        tLCInsureAccFeeTraceSchema.setAccAscription(tLCInsureAccClassFeeSchema.getAccAscription());
        tLCInsureAccFeeTraceSchema.setMoneyType("GL");
        tLCInsureAccFeeTraceSchema.setFee(fee);
        tLCInsureAccFeeTraceSchema.setFeeRate(tLCGrpFeeSchema.getFeeValue());
        //tLCInsureAccFeeTraceSchema.setPayDate(currentDate);
        tLCInsureAccFeeTraceSchema.setPayDate(tLCInsureAccClassFeeSchema.getBalaDate());
        tLCInsureAccFeeTraceSchema.setFeeCode(tLCGrpFeeSchema.getFeeCode());
        //tLCInsureAccFeeTraceSchema.setInerSerialNo();
        tLCInsureAccFeeTraceSchema.setManageCom(tLCInsureAccClassFeeSchema.getManageCom());
        tLCInsureAccFeeTraceSchema.setOperator(tLCInsureAccClassFeeSchema.getOperator());
        tLCInsureAccFeeTraceSchema.setModifyDate(currentDate);
        tLCInsureAccFeeTraceSchema.setModifyTime(currentTime);
        tLCInsureAccFeeTraceSchema.setMakeDate(currentDate);
        tLCInsureAccFeeTraceSchema.setMakeTime(currentTime);
        return tLCInsureAccFeeTraceSchema;
    }

    /**
     * getLMRiskFeeKey
     *
     * @param tFeeSchema LCGrpFeeSchema
     * @return String
     */
    private String getLMRiskFeeKey(LCGrpFeeSchema tFeeSchema)
    {
        return tFeeSchema.getFeeCode() + tFeeSchema.getInsuAccNo() + tFeeSchema.getPayPlanCode()
                + tFeeSchema.getFeeCalMode();
    }

    private void updateInsureAcc(String OtherNo, String OtherNoType, String MoneyType, double inputMoney,
            LCPremSchema tLCPremSchema, LCPremToAccSchema tLCPremToAccSchema)
    {
        for (int j = 1; j <= mLCInsureAccSet.size(); j++)
        {
            //如果当前缴费帐户关联表的保单号，账户号和当前的账户表的保单号，账户号相同并且资金不为0，将资金注入
            LCInsureAccSchema tLCInsureAccSchema = mLCInsureAccSet.get(j);
            if (tLCPremToAccSchema.getPolNo().equals(tLCInsureAccSchema.getPolNo())
                    && tLCPremToAccSchema.getInsuAccNo().equals(tLCInsureAccSchema.getInsuAccNo()))
            {
                //修改保险帐户金额
                tLCInsureAccSchema.setInsuAccBala(PubFun.setPrecision(tLCInsureAccSchema.getInsuAccBala() + inputMoney,
                        "0.00"));
                tLCInsureAccSchema.setSumPay(PubFun.setPrecision(tLCInsureAccSchema.getSumPay() + inputMoney, "0.00"));
                tLCInsureAccSchema.setLastAccBala(PubFun.setPrecision(tLCInsureAccSchema.getLastAccBala() + inputMoney,
                        "0.00"));

                //查询险种保险帐户缴费
                //                LMRiskAccPaySchema
                //                        tLMRiskAccPaySchema =
                //                        queryLMRiskAccPay3(
                //                        mLCPolSchema.getRiskCode(),
                //                        tLCPremToAccSchema);
                //                if (tLMRiskAccPaySchema == null)
                //                {
                //                    // return null;
                //                    System.out.println("查询描述表错误");
                //                    return;
                //                }
                //                if (tLMRiskAccPaySchema.
                //                    getPayNeedToAcc()
                //                    .equals("1"))
                //                {
                LCInsureAccTraceSchema tmpLCInsureAccTraceSchema = createAccTrace(OtherNo, OtherNoType, MoneyType,
                        inputMoney, tLCPremSchema, tLCInsureAccSchema);
                mLCInsureAccTraceSet.add(tmpLCInsureAccTraceSchema);

                //                }

                // break;
            }
        }

    }

    /**
     * 更新分类表
     * @param tClassSchema LCInsureAccClassSchema
     * @param manaFee double
     * @param tLCPremToAccSchema LCPremToAccSchema
     */
    private void updateLCInsuerAccClass(LCInsureAccClassSchema tClassSchema, double inputMoney,
            LCPremToAccSchema tLCPremToAccSchema)
    {
        //累计账户分类ss
        for (int tt = 1; tt <= mInsureAccClassSet.size(); tt++)
        {
            tClassSchema = mInsureAccClassSet.get(tt);
            if (tClassSchema.getInsuAccNo().equals(tLCPremToAccSchema.getInsuAccNo())
                    && tClassSchema.getPayPlanCode().equals(tLCPremToAccSchema.getPayPlanCode())
                    && tClassSchema.getPolNo().equals(tLCPremToAccSchema.getPolNo()))
            {

                tClassSchema.setSumPay(PubFun.setPrecision(tClassSchema.getSumPay() + inputMoney, "0.00"));
                tClassSchema.setInsuAccBala(PubFun.setPrecision(tClassSchema.getInsuAccBala() + inputMoney, "0.00"));
                tClassSchema.setLastAccBala(PubFun.setPrecision(tClassSchema.getLastAccBala() + inputMoney, "0.00"));
                // break;
            }

        }
    }

    /**
     * 更新管理非账户表
     * @param tClsFeeSchema LCInsureAccClassFeeSchema
     */
    private void updateLCInsureAccFee(String InsuAccNo, double fee)
    {
        //管理非费账户
        LCInsureAccFeeSchema tLCInsureAccFeeSchema = getLCInsureAccFee(InsuAccNo);
        if (tLCInsureAccFeeSchema == null)
        {
            System.out.println("没有从查找到对应的管理费账户");
            //return null;
        }else{
        	tLCInsureAccFeeSchema.setFee(Arith.round(tLCInsureAccFeeSchema.getFee() + fee, 2));
        }
        //        by gzh 20110720 修改管理费为四舍五入数据
        //        tLCInsureAccFeeSchema.setFee(PubFun.setPrecision(tLCInsureAccFeeSchema.getFee()+ fee, "0.00"));
        
    }

    /**
     * 创建账户轨迹表
     * @param OtherNo String
     * @param OtherNoType String
     * @param MoneyType String
     * @param tLCInsureAccTraceSet LCInsureAccTraceSet
     * @param tLCInsureAccTraceSchema LCInsureAccTraceSchema
     * @param inputMoney double
     * @param tLCPremSchema LCPremSchema
     * @param tLCInsureAccSchema LCInsureAccSchema
     */
    private LCInsureAccTraceSchema createAccTrace(String OtherNo, String OtherNoType, String MoneyType,
            double inputMoney, LCPremSchema tLCPremSchema, LCInsureAccSchema tLCInsureAccSchema)
    {

        //填充保险帐户表记价履历表
        String tLimit = PubFun.getNoLimit(tLCPremSchema.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
        tLCInsureAccTraceSchema.setSerialNo(serNo);
        //        tLCInsureAccTraceSchema.setInsuredNo(tLCInsureAccSchema
        //                                             .getInsuredNo());
        tLCInsureAccTraceSchema.setPolNo(tLCInsureAccSchema.getPolNo());
        tLCInsureAccTraceSchema.setMoneyType(MoneyType);
        tLCInsureAccTraceSchema.setRiskCode(tLCInsureAccSchema.getRiskCode());
        tLCInsureAccTraceSchema.setOtherNo(OtherNo);
        tLCInsureAccTraceSchema.setOtherType(OtherNoType);
        tLCInsureAccTraceSchema.setMoney(inputMoney);
        tLCInsureAccTraceSchema.setContNo(tLCInsureAccSchema.getContNo());
        tLCInsureAccTraceSchema.setGrpPolNo(tLCInsureAccSchema.getGrpPolNo());
        tLCInsureAccTraceSchema.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());

        tLCInsureAccTraceSchema.setPolNo(tLCInsureAccSchema.getPolNo());
        tLCInsureAccTraceSchema.setGrpContNo(tLCInsureAccSchema.getGrpContNo());
        tLCInsureAccTraceSchema.setState(tLCInsureAccSchema.getState());
        tLCInsureAccTraceSchema.setManageCom(tLCInsureAccSchema.getManageCom());
        tLCInsureAccTraceSchema.setOperator(tLCInsureAccSchema.getOperator());

        tLCInsureAccTraceSchema.setPolNo(tLCInsureAccSchema.getPolNo());
        tLCInsureAccTraceSchema.setGrpContNo(tLCInsureAccSchema.getGrpContNo());
        String CurrentDate = PubFun.getCurrentDate();
        String CurrentTime = PubFun.getCurrentTime();
        tLCInsureAccTraceSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
        tLCInsureAccTraceSchema.setState("0");
        tLCInsureAccTraceSchema.setPayDate(tLCInsureAccSchema.getBalaDate());
        //默认未归属
        tLCInsureAccTraceSchema.setAccAscription("0");
        tLCInsureAccTraceSchema.setMakeDate(CurrentDate);
        tLCInsureAccTraceSchema.setMakeTime(CurrentTime);
        tLCInsureAccTraceSchema.setModifyDate(CurrentDate);
        tLCInsureAccTraceSchema.setModifyTime(CurrentTime);
        tLCInsureAccTraceSchema.setOperator(tLCPremSchema.getOperator());

        return tLCInsureAccTraceSchema;
    }

    /**
     * 查询险种保险帐户缴费表3
     * @param riskcode
     * @param LCPremToAccSchema
     * @return LMRiskAccPaySchema
     */
    public LMRiskAccPaySchema queryLMRiskAccPay3(String riskCode, LCPremToAccSchema pLCPremToAccSchema)
    {
        if (riskCode == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DealAccount";
            tError.functionName = "queryLMRiskAccPay3";
            tError.errorMessage = "传入数据不能为空!";
            this.mErrors.addOneError(tError);

            return null;
        }

        String payPlanCode = pLCPremToAccSchema.getPayPlanCode();
        String InsuAccNo = pLCPremToAccSchema.getInsuAccNo();

        //查询险种保险帐户缴费表
        String sqlStr = "select * from LMRiskAccPay where RiskCode='" + riskCode + "' and payPlanCode='" + payPlanCode
                + "' and InsuAccNo='" + InsuAccNo + "'";
        LMRiskAccPaySchema tLMRiskAccPaySchema = new LMRiskAccPaySchema();
        LMRiskAccPaySet tLMRiskAccPaySet = new LMRiskAccPaySet();
        LMRiskAccPayDB tLMRiskAccPayDB = tLMRiskAccPaySchema.getDB();
        tLMRiskAccPaySet = tLMRiskAccPayDB.executeQuery(sqlStr);
        if (tLMRiskAccPayDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMRiskAccPayDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "DealAccount";
            tError.functionName = "queryLMRiskAccPay2";
            tError.errorMessage = "险种保险帐户缴费表查询失败!";
            this.mErrors.addOneError(tError);
            tLMRiskAccPaySet.clear();

            return null;
        }
        if (tLMRiskAccPaySet.size() == 0)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMRiskAccPayDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "DealAccount";
            tError.functionName = "queryLMRiskAccPay2";
            tError.errorMessage = "险种保险帐户缴费表没有查询到相关数据!";
            this.mErrors.addOneError(tError);
            tLMRiskAccPaySet.clear();

            return null;
        }

        return tLMRiskAccPaySet.get(1);
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CManageFee";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
