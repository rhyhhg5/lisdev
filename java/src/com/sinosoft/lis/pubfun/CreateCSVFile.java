package com.sinosoft.lis.pubfun;

import java.io.File;
import java.io.FileWriter;

public class CreateCSVFile {
	private File tFile;
	private FileWriter tFileWriter;
        private String sq="";
	public boolean initFile(String filePathAndName){
		System.out.println("...........CreateCSVFile here filePathAndName"
				+filePathAndName);
		tFile=new File(filePathAndName);
		if(tFile.exists()){
			tFile.delete();
		}
			try{
				tFile.createNewFile();
				tFileWriter=new FileWriter(tFile);
			}catch(Exception ex){
				ex.printStackTrace();
				return false;
			}
		return true;
	}
	public boolean doWrite(String[][] resultData){
		try{
			for(int i=0;i<resultData.length;i++){
				tFileWriter.write(resultData[i][0]+","+resultData[i][1]+","
						+resultData[i][2]+","+resultData[i][3]+","
						+resultData[i][4]+","+resultData[i][5]+","
						+resultData[i][6]+","+resultData[i][7]+","
						+resultData[i][8]+","+resultData[i][9]+","
						+resultData[i][10]+","+resultData[i][11]+","
						+resultData[i][12]+","+resultData[i][13]+","
						+resultData[i][14]+","+resultData[i][15]+","
						+resultData[i][16]+","+resultData[i][17]+","
						+resultData[i][18]+","+resultData[i][19]+","
						+resultData[i][20]+","+resultData[i][21]+","
                                                +resultData[i][22]+","+resultData[i][23]+","
                                                +resultData[i][24]+","+resultData[i][25]+","
						+resultData[i][26]+","+resultData[i][27]+","+resultData[i][28]+","+resultData[i][29]+"\r\n");
			}
			tFileWriter.flush();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

        public boolean doWrite(String[][] resultData,int n){
                try{

                        for(int i=0;i<resultData.length;i++){
                        	sq="";
                            for(int j=0;j<=n;j++){
                               sq=sq+resultData[i][j]+",";
                            }
                            tFileWriter.write(sq+"\r\n");
//                                tFileWriter.write(resultData[i][0]+","+resultData[i][1]+","
//                                                +resultData[i][2]+","+resultData[i][3]+","
//                                                +resultData[i][4]+","+resultData[i][5]+","
//                                                +resultData[i][6]+","+resultData[i][7]+","
//                                                +resultData[i][8]+","+resultData[i][9]+","
//                                                +resultData[i][10]+","+resultData[i][11]+","
//                                                +resultData[i][12]+","+resultData[i][13]+","
//                                                +resultData[i][14]+","+resultData[i][15]+","
//                                                +resultData[i][16]+","+resultData[i][17]+","
//                                                +resultData[i][18]+","+resultData[i][19]+","
//                                                +resultData[i][20]+","+resultData[i][21]+","
//                                                +resultData[i][22]+","+resultData[i][23]+","
//                                                +resultData[i][24]+"\r\n");
                        }
                        tFileWriter.flush();
                        return true;
                }catch(Exception e){
                        e.printStackTrace();
                        return false;
                }
        }
        public boolean doWrite(String[] resultData){
    		try{
    			for(int i=0;i<resultData.length;i++){
    				tFileWriter.write(resultData[i]+"\r\n");
    			}
    			tFileWriter.flush();
    			return true;
    		}catch(Exception e){
    			e.printStackTrace();
    			return false;
    		}
    	}
	public boolean doClose(){
		try{
			tFileWriter.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return true;
	}

}
