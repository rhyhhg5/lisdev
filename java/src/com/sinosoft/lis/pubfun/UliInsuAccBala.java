package com.sinosoft.lis.pubfun;

import java.util.ArrayList;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 万能险保单账户结算 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author YangYalin
 * @version 1.3
 * @CreateDate：2007-04-28
 */
public class UliInsuAccBala
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGI = null;
    private LCInsureAccSchema mLCInsureAccSchema = null;

    private String mBalanceSeq = null;
    private String mStartDate = null;
    private String mEndDate = null;
    private int mPolYear = 1; //保单年度
    private int mPolMonth = 1; //保单月度

    private Reflections ref = new Reflections();

    public MMap map = new MMap();
    //保存公共账户金额
//    private double GGAccValue = 0;
//    private boolean getGG = false;//公共账户是否已从数据库获取，false 未获取，true 已获取
    private String GGSequenceno = "";//公共账户是否已处理完存库，""为未存库，非""已存库。
    private String GGBalaCoutn = "";
    //记录结息后的子账户信息
    private LCInsureAccClassSet mLCInsureAccClassSet = null;//记录子账户结息后的信息
    //记录保证账户结算信息
    private LCInsureAccClassSet guratLCInsureAccClassSet = null;
    
    private String mGrpContNo = "";//记录每个保单的总人数
//    private LCInsureAccSchema mGGLCInsureAccSchema = new LCInsureAccSchema();//公共账户信息
//    private LCInsureAccClassSet mGGLCInsureAccClassSet = new LCInsureAccClassSet() ;

    public UliInsuAccBala()
    {
    }

    /**
     * submitData
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate,String grpcontno)
    {
        if (!getInputData(cInputData,grpcontno))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 业务处理不需要考虑保证利率，业务提供的利率不小于保证利率
     * @return boolean
     */
    public boolean dealData()
    {
        //查询出当前账户最后结算日期比已公布利率日期晚的账户，由于理赔或保全等原因，一个账户可能需要结算多次
    	String FeeSql = "select feevalue,riskfeemode,feecode from lcgrpfee where grpcontno = '"+mGrpContNo+"' and insuaccno = '000000'";
    	SSRS FeeSSRS = new ExeSQL().execSQL(FeeSql);
    	if(FeeSSRS == null || FeeSSRS.getMaxRow()<=0 || FeeSSRS.getMaxRow()>1){
    		mErrors.addOneError("获取管理费收取方式及金额失败！");
            return false;
    	}
    	double glFee = Double.parseDouble(FeeSSRS.GetText(1, 1));//管理费金额
    	String glFeeMode = FeeSSRS.GetText(1, 2);
    	String FeeCode = FeeSSRS.GetText(1, 3);
    	String Accsql = "select a.* from LCInsureAcc a where a.grpcontno = '"+mGrpContNo+"' and not exists (select 1 from lcpol where polno=a.polno and polstate in ('03060001','03060002')) order by baladate fetch first 1 rows only";
    	LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
    	LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.executeQuery(Accsql);
    	

    	Interest000001Set tInterest000001Set = getInterest000001(tLCInsureAccSet.get(1));
        if (tInterest000001Set == null)
        {
            return false;
        }
        //根据团单号获取利率
//        String RateByGrpContNosql = "select * from ";
        ArrayList tlist = new ArrayList(); //用以记录扣除管理费时余额不足的被保人客户号
        for (int m = 1; m <= tInterest000001Set.size(); m++)
        {
           LCInsureAccSchema mGGLCInsureAccSchema = new LCInsureAccSchema();//公共账户信息
           LCInsureAccClassSet mGGLCInsureAccClassSet = new LCInsureAccClassSet() ;
           double GGAccValue = 0;
           double GGAccValueGurat=0;
           boolean getGG = false;//公共账户是否已从数据库获取，false 未获取，true 已获取
           GGSequenceno = "";//公共账户是否已处理完存库，""为未存库，非""已存库。
           GGBalaCoutn = "";
           
        	String InsuredNoSql = "select distinct insuredno,polno,acctype from LCInsureAcc a where grpcontno = '"+mGrpContNo+"' and baladate<'"+tInterest000001Set.get(m).getEndDate()+"' " +
        			" and not exists (select 1 from lcpol where polno=a.polno and polstate in ('03060001','03060002')) order by acctype ";
        	SSRS InsuredNoSSRS = new ExeSQL().execSQL(InsuredNoSql);
        	if(InsuredNoSSRS == null || InsuredNoSSRS.getMaxRow()<=0){
        		mErrors.addOneError("获取需结息的被保人客户号码失败！");
                return false;
        	}
        	String mBcStartDate = tInterest000001Set.get(m).getStartDate();//本次月结的开始时间
            mEndDate = tInterest000001Set.get(m).getEndDate();
            String tRate =  String.valueOf(tInterest000001Set.get(m).getRate());
            String tRateIntv = tInterest000001Set.get(m).getRateIntv();
            String tRateType = tInterest000001Set.get(m).getRateType();
        	int tCount = InsuredNoSSRS.getMaxRow(); //记录每个保单的总人数
        	for(int b=1;b<=tCount;b++){
        		MMap tMMapVD=new MMap();
        		
        		boolean  takeOffFalg = true;
        		if(tlist.contains(InsuredNoSSRS.GetText(b, 1))){//如果该客户在上月月结时余额不足，则本月不进行月结。
        			System.out.println("由于被保人"+InsuredNoSSRS.GetText(b, 1)+"上月月结余额不足，本月不进行月结。");
        			continue;
        		}
        		else if(!checkTerminate(InsuredNoSSRS.GetText(b, 2),tInterest000001Set.get(m).getEndDate())){
        				
        				System.out.println("处理被保人" + InsuredNoSSRS.GetText(b, 1)
                                + " 已经满期，当月月结不再处理");
        				continue;
        			}
        			
        		double tAccValueGurat = 0; //保证账户收益
        		
        		long t0 = System.currentTimeMillis();
        		String aAIPolNo = InsuredNoSSRS.GetText(b, 2);
//        		mGGLCInsureAccSchema = new LCInsureAccSchema();//公共账户信息
//        	    mGGLCInsureAccClassSet = new LCInsureAccClassSet() ;
        	    mLCInsureAccClassSet = new LCInsureAccClassSet();//记录子账户结息后的信息
        	    guratLCInsureAccClassSet =new LCInsureAccClassSet();//201506 存储保证账户
//        	    double GGAccValue = 0;
        		Accsql = "select a.* from LCInsureAcc a where a.grpcontno = '"+mGrpContNo+"' and insuredno = '"+InsuredNoSSRS.GetText(b, 1)+"' order by insuaccno";
        		tLCInsureAccDB = new LCInsureAccDB();
            	tLCInsureAccSet = tLCInsureAccDB.executeQuery(Accsql);
            	MMap tPerMMap = new MMap();
            	for(int a=1;a<=tLCInsureAccSet.size();a++){
            		mLCInsureAccSchema = tLCInsureAccSet.get(a);
            		if (mLCInsureAccSchema == null)
                    {
                        return false;
                    }
            		if("001".equals(mLCInsureAccSchema.getAccType())){
            			mGGLCInsureAccSchema = tLCInsureAccSet.get(a);
            			aAIPolNo="";
            		}
            		
            		System.out.println("处理被保人" + mLCInsureAccSchema.getInsuredNo()
                            + ", 险种" + mLCInsureAccSchema.getRiskCode()
                            + ", 结算起始日期"
                            + tInterest000001Set.get(m).getStartDate() + "的结算");
            		mBalanceSeq = null; //由于月度不同，需要重新生成结算序号
//            		查询帐户子表信息
            		LCInsureAccClassSet tLCInsureAccClassSet = getAccClass(
            				mLCInsureAccSchema);
                    if (tLCInsureAccClassSet == null)
                    {
                        return false;
                    }
                    LCInsureAccClassSchema schemaAV = null; //处理后的账户分类价值
//                    LCInsureAccTraceSchema schemaTraceOne = null; //本次产生的任一个轨迹
                    MMap tMMap = new MMap();
                    for (int n = 1; n <= tLCInsureAccClassSet.size(); n++)
                    {
                        LCInsureAccClassSchema tLCInsureAccClassSchema
                                = tLCInsureAccClassSet.get(n);
                        mStartDate = tLCInsureAccClassSchema.getBalaDate();
                        if (!calPolYearMonth())
                        {
                            return false;
                        }
                        System.out.println("计算账户价值");
                        MMap tMMapAV = dealOneAccClass(tLCInsureAccClassSchema,
                        		tRateIntv, tRateType,tRate,"1");
                        if (tMMapAV == null)
                        {
                            return false;
                        }
                        tMMap.add(tMMapAV);
                        schemaAV = (LCInsureAccClassSchema) tMMapAV
                                   .getObjectByObjectName("LCInsureAccClassSchema", 0);
                        LCInsureAccTraceSchema schemaTraceOne=(LCInsureAccTraceSchema) tMMapAV
                        .getObjectByObjectName("LCInsureAccTraceSchema", 0);
                        if (schemaAV == null)
                        {
                            mErrors.addOneError("没有计算出账户价值");
                            return false;
                        }
                        //20150629 计算保证账户价值
                      //新增农民工险种根据印刷号查询保证利率
                    	String prtNo = new ExeSQL().getOneValue("select prtno from lcgrpcont where grpcontno = '"+mGrpContNo+"'");
                    	String riskCode = mLCInsureAccSchema.getRiskCode();
                        LDPromiseRateSchema mLDPromiseRateSchema=getLDPromiseRate(tLCInsureAccClassSchema,prtNo,riskCode);
                        if(null==mLDPromiseRateSchema){
                        	mErrors.addOneError("没有获取到保证利率");
                        	return false;
                        }
                        MMap tMMapGurat= dealOneAccClass(tLCInsureAccClassSchema,
                        		mLDPromiseRateSchema.getRateIntv(), mLDPromiseRateSchema.getRateType(),String.valueOf(mLDPromiseRateSchema.getRate()),"2");
                        if (tMMapGurat == null)
                        {
                            mErrors.addOneError("计算保证收益出错");
                            return false;
                        }
                        LCInsureAccClassSchema schema= (LCInsureAccClassSchema) tMMapGurat
                          .getObjectByObjectName("LCInsureAccClassSchema", 0);
		                if (schema == null)
		                {
		                    mErrors.addOneError("没有计算出保证账户价值");
		                    return false;
		                }
		                tAccValueGurat += schema.getInsuAccBala();
		                guratLCInsureAccClassSet.add(schema);
		                
		                //根据保证账户价值进行收益补差
		                MMap mMMapVD = dealAccountValueDif(schemaAV,schema,
                                    schemaTraceOne,mEndDate);
		                tMMapVD.add(mMMapVD);
                        mLCInsureAccClassSet.add(schemaAV);//将结息后的子账户信息存入全局变量
                        if("001".equals(schemaAV.getAccType())){
                        	mGGLCInsureAccClassSet.add(schemaAV);
                        	GGAccValue += schemaAV.getInsuAccBala();
                        	GGAccValueGurat +=tAccValueGurat;
                        }
                    }
                    tPerMMap.add(tMMap);
            	}//对个人的所有账户处理完毕
            	System.out.println("第"+b+"个人结息处理完成，共"+tCount+"人，用时："+(System.currentTimeMillis() - t0));
            	
            	
//            	//被保险人的账户价值大于0才扣除管理费
//            	LCInsureAccClassDB t2LCInsureAccClassDB = new LCInsureAccClassDB();
//            	t2LCInsureAccClassDB.setGrpContNo(mGrpContNo);
//            	t2LCInsureAccClassDB.setInsuredNo(InsuredNoSSRS.GetText(b, 1));
//            	LCInsureAccClassSet t2LCInsureAccClassSet = new LCInsureAccClassSet();
//            	t2LCInsureAccClassSet = t2LCInsureAccClassDB.query();
            	double insureAccBala1 = getAccBalaByAccClass(mLCInsureAccClassSet);//一个人所有账户的总额
            	
            	if(insureAccBala1<=0){
            		takeOffFalg = false;
            		System.out.println("被保险人"+InsuredNoSSRS.GetText(b, 1)+"的账户余额为0，不需要扣除管理费。");
            	}else{
            		System.out.println("被保险人"+InsuredNoSSRS.GetText(b, 1)+"的账户余额大于0，需要扣除管理费。");
            	}
            	
            	long t1 = System.currentTimeMillis();
//            	以下开始收取管理费
            	if("2".equals(glFeeMode)&& !getGG){// 从公共账户收取
            		if(mGGLCInsureAccClassSet==null ||mGGLCInsureAccClassSet.size()<=0){//公共账户已存库。
            			String GGAccSql = "select * from lcinsureacc where grpcontno = '"+mGrpContNo+"' and acctype = '001'";
            			LCInsureAccDB tGGLCInsureAccDB = new LCInsureAccDB();
            			mGGLCInsureAccSchema = tGGLCInsureAccDB.executeQuery(GGAccSql).get(1);
            			if(mGGLCInsureAccSchema==null){
            				mErrors.addOneError("没有获取到公共账户信息！");
                            return false;
            			}
            			mGGLCInsureAccClassSet= getAccClass(mGGLCInsureAccSchema);
            			GGAccValue = getAccBalaByAccClass(mGGLCInsureAccClassSet);
            			String BalaSql ="select Sequenceno,Balacount,InsuAccBalaAfter from LCInsureAccBalance where polno = '"+mGGLCInsureAccSchema.getPolNo()+"' order by balacount desc fetch first 1 rows only";
            			SSRS BalaSSRS = new ExeSQL().execSQL(BalaSql);
            			GGSequenceno = BalaSSRS.GetText(1, 1);
            			GGBalaCoutn = BalaSSRS.GetText(1, 2);
            		}
            		
            		
//            		查看公共账户是否足够，需考虑月结不足满月的情况            		
            		double sumGlFee = 0;
            		for(int i=1;i<=InsuredNoSSRS.getMaxRow();i++){
            			
            			//被保险人的账户价值大于0才扣除管理费
            			LCInsureAccClassDB t1LCInsureAccClassDB = new LCInsureAccClassDB();
            			t1LCInsureAccClassDB.setGrpContNo(mGrpContNo);
            			t1LCInsureAccClassDB.setInsuredNo(InsuredNoSSRS.GetText(i, 1));
            			LCInsureAccClassSet t1LCInsureAccClassSet = new LCInsureAccClassSet();
            			t1LCInsureAccClassSet = t1LCInsureAccClassDB.query();
            			double insureAccBala = getAccBalaByAccClass(t1LCInsureAccClassSet);//一个人所有账户的总额
            			if(insureAccBala>0){
            				sumGlFee = Arith.add(sumGlFee, getGlFeeByPolno(InsuredNoSSRS.GetText(i, 2),glFee,mBcStartDate));
            			}
            		}
            		if(sumGlFee>GGAccValue){
            			System.out.println("公共账户在扣除管理费时余额不足！");
            			glFeeMode = "1";
            		}
            		getGG = true;
            	}
            	double temp = getGlFeeByPolno(InsuredNoSSRS.GetText(b, 2),glFee,mBcStartDate);//获取当前被保人需缴纳的管理费用
            	if(!takeOffFalg){
            		temp = 0;
            	}
            	boolean flag = true;
        		if("2".equals(glFeeMode)){//从公共账户扣除
        	    	for(int j=1;j<=mGGLCInsureAccClassSet.size();j++){
        	    		LCInsureAccClassSchema tGGLCInsureAccClassSchema = mGGLCInsureAccClassSet.get(j);//公共账户只有一个，子账户也有一个
            			MMap MMapTrace = getTrace(tGGLCInsureAccClassSchema,temp,aAIPolNo,FeeCode);//生成管理费轨迹
            			tPerMMap.add(MMapTrace);
        	    	}
        	    	double tAccValue=0;
    	    		tAccValue = Arith.sub(GGAccValue, temp);
    	    		double tGGurat=0;//计算的保证账户时也要扣除管理费
    	    		tGGurat = Arith.sub(GGAccValueGurat, temp);
    	    		GGAccValueGurat=tGGurat;
//        			String BalaSql ="select Sequenceno,InsuAccBalaAfter from LCInsureAccBalance where polno = '"+mGGLCInsureAccSchema.getPolNo()+"' order by balacount desc fetch first 1 rows only";
//        			SSRS BalaSSRS = new ExeSQL().execSQL(BalaSql);
//        			GGSequenceno = BalaSSRS.GetText(1, 1);
        	    	MMap MMapBalance = getBalance(mGGLCInsureAccSchema,tInterest000001Set.get(m),tAccValue,tGGurat,GGSequenceno,"1");
    	    		GGAccValue =Arith.sub(GGAccValue, temp);
        			tPerMMap.add(MMapBalance);
        			tPerMMap.add(updateAccInfo(mGGLCInsureAccSchema));
        			for(int i=1;i<=tLCInsureAccSet.size();i++){
        				mBalanceSeq = null; //由于月度不同，需要重新生成结算序号
                		LCInsureAccSchema glLCInsureAccSchema = tLCInsureAccSet.get(i);
	        			if(!"001".equals(tLCInsureAccSet.get(i).getAccType())){
	        				mBalanceSeq = null;
	        				LCInsureAccClassSet grLCInsureAccClassSet = getAccClassSetFromM(tLCInsureAccSet.get(i));
	        				tAccValue = getAccBalaByAccClass(grLCInsureAccClassSet);
	        				double mValueGurat=getAccValueGurat(glLCInsureAccSchema);
		        			MMap MMapBalance1 = getBalance
		        			(glLCInsureAccSchema,tInterest000001Set.get(m),tAccValue,mValueGurat,"","0");
		        			tPerMMap.add(MMapBalance1);
		        			tPerMMap.add(updateAccInfo(glLCInsureAccSchema));
	        			}
        			}
        		}else{//从个人账户收取
        			double accbala = getAccBalaByAccClass(mLCInsureAccClassSet);//一个人所有账户的总额
        			if(temp>accbala){
        				System.out.println("处理被保人客户号码为"+InsuredNoSSRS.GetText(b, 1)+"的数据时余额不足！");
//        				tlist.add(InsuredNoSSRS.GetText(b, 1));
//                		continue;
                	}
        			for(int i=1;i<=tLCInsureAccSet.size();i++){
	        			mBalanceSeq = null; //由于月度不同，需要重新生成结算序号
	                	LCInsureAccSchema glLCInsureAccSchema = tLCInsureAccSet.get(i);
	        			LCInsureAccClassSet tLCInsureAccClassSet = getAccClassSetFromM(tLCInsureAccSet.get(i));//从全局变量中获取当前账户的子账户
	        			//2015-7-3 lzy 看来这里是对每个被保险人的账户只扣一笔管理费，而不是每个被保人的每个账户都扣。
	        			double mValueGurat=getAccValueGurat(tLCInsureAccSet.get(i));
	        			
	        			if(flag){
		        			double accbalacur = getAccBalaByAccClass(tLCInsureAccClassSet);//当前账户的余额
		        			if(accbalacur>temp){
		        				double accTemp = temp;
		        				for(int j=1;j<=tLCInsureAccClassSet.size();j++){
		        					double accTemp1 = accTemp;//本次扣除费用
		                			accTemp = Arith.sub(tLCInsureAccClassSet.get(j).getLastAccBala(), accTemp);
		                			if(accTemp>=0){
		                				MMap MMapTrace = getTrace(tLCInsureAccClassSet.get(j),accTemp1,"",FeeCode);//生成管理费轨迹
		                				tLCInsureAccClassSet.get(j).setInsuAccBala(accTemp);//本账户余额够，将剩余钱放入账户
		                				tPerMMap.add(MMapTrace);
		                				flag = false;
		                			}else{
		                				MMap MMapTrace = getTrace(tLCInsureAccClassSet.get(j),tLCInsureAccClassSet.get(j).getLastAccBala(),"",FeeCode);//生成管理费轨迹
		                				tLCInsureAccClassSet.get(j).setInsuAccBala(0);//本账户不够 全部扣除
		                				tPerMMap.add(MMapTrace);
		                				accTemp = Arith.sub(accTemp,tLCInsureAccClassSet.get(j).getLastAccBala());
		                			}
		        				}
		        			}else{//当前账户余额不够扣除管理费
		            			for(int j=1;j<=tLCInsureAccClassSet.size();j++){
		            				MMap MMapTrace = getTrace(tLCInsureAccClassSet.get(j),tLCInsureAccClassSet.get(j).getLastAccBala(),"",FeeCode);//生成管理费轨迹
		            				temp = Arith.sub(temp, tLCInsureAccClassSet.get(j).getLastAccBala());
		            				tLCInsureAccClassSet.get(j).setInsuAccBala(0);
		            				tPerMMap.add(MMapTrace);
		                		}
		        			}
		        			mValueGurat= Arith.sub(mValueGurat, temp);
		        			if(mValueGurat < 0){
		        				mValueGurat=0;
		        			}
		        			double tAccValue = getAccBalaByAccClass(tLCInsureAccClassSet);
		        			MMap MMapBalance = getBalance(glLCInsureAccSchema,tInterest000001Set.get(m),tAccValue,mValueGurat,"","0");
		        			tPerMMap.add(MMapBalance);
	        			}else{
		        			double tAccValue = getAccBalaByAccClass(tLCInsureAccClassSet);
		        			MMap MMapBalance = getBalance(glLCInsureAccSchema,tInterest000001Set.get(m),tAccValue,mValueGurat,"","0");
		        			tPerMMap.add(MMapBalance);
	        			}
	        			tPerMMap.add(updateAccInfo(glLCInsureAccSchema));
        			}
        		}
        		tPerMMap.add(tMMapVD);
	        	//保存管理费数据
	        	VData data = new VData();
	            data.add(tPerMMap);
	            PubSubmit tSubmit = new PubSubmit();
	            if (!tSubmit.submitData(data, ""))
	            {
	                buildError("UliInsureAccSubmit", "数据提交失败");
	                return false;
	            }
	            System.out.println("第"+b+"个人管理费处理完成，共"+tCount+"人，用时："+(System.currentTimeMillis() - t1));
	            System.out.println("第"+b+"个人处理完成，共"+tCount+"人，用时："+(System.currentTimeMillis() - t0));
//	            System.out.println(PubFun.getCurrentDate()+PubFun.getCurrentTime()+"===第"+b+"个人结束时间");
        	}
        }
        return true;
    }


    
    private boolean checkTerminate(String tPolNo,String tEndDate){
    	
    	 String strSql = "select  1 from lcget where " +
    	 					" getdutykind is not null and livegettype='0' and getstartdate < '" + tEndDate + "' " +
    	 					" and polno='"+tPolNo+"' with ur ";
    	 System.out.println("Sql ：" + strSql);
         SSRS tSSRS = new ExeSQL().execSQL(strSql);
         if (tSSRS.getMaxRow() > 0)
         {
        	 System.out.println("该人员在当前月结月份已经满期，不做月结处理");
             return false;
         }
    	return true;
    }

    /**
     * calPolYearMonth
     */
    private boolean calPolYearMonth()
    {
        String sql = "select year(date('" + mStartDate +
                     "') - CValidate) + 1, "
                     + "   month(date('" + mStartDate + "') - CValidate) + 1 "
                     + "from LCPol "
                     + "where PolNo = '" + mLCInsureAccSchema.getPolNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        mPolYear = Integer.parseInt(tSSRS.GetText(1, 1));
        mPolMonth = Integer.parseInt(tSSRS.GetText(1, 2));

        return true;
    }

    private MMap dealOneAccClass(LCInsureAccClassSchema tLCInsureAccClassSchema,
                                 String cRateIntv, String cRatetype,String tRate,String tBalaType )
    {
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("StartDate", mStartDate);
        tTransferData.setNameAndValue("EndDate", mEndDate);
        tTransferData.setNameAndValue("RiskCode",
                                      tLCInsureAccClassSchema.getRiskCode());
        tTransferData.setNameAndValue("RateIntv", cRateIntv); //利率间隔
        tTransferData.setNameAndValue("RateType", cRatetype); //利率类型
        tTransferData.setNameAndValue("PolYear", String.valueOf(mPolYear));
        tTransferData.setNameAndValue("PolMonth", String.valueOf(mPolMonth));
        
        tTransferData.setNameAndValue("Rate", tRate);//利率  根据cBalaType，利率对应结算利率和保证利率
        tTransferData.setNameAndValue("BalaType", tBalaType);// 1-计算账户价值；2-计算保证账户价值

        VData data = new VData();
        data.add(mGI);
        data.add(tLCInsureAccClassSchema);
        data.add(tTransferData);

        MMap tMMapAV = balanceOneAccRate(data);
        return tMMapAV;
    }

    /**
     * balanceOneAccRate
     * 对子账户进行一个月的收益结算
     * @param cInputDate VData
     * @return MMap
     */
    public MMap balanceOneAccRate(VData cInputDate)
    {
        TransferData tTransferData = (TransferData) cInputDate
                                     .getObjectByObjectName("TransferData", 0);
        mGI = (GlobalInput) cInputDate
              .getObjectByObjectName("GlobalInput", 0);
        LCInsureAccClassSchema tLCInsureAccClassSchema
                = (LCInsureAccClassSchema) cInputDate
                  .getObjectByObjectName("LCInsureAccClassSchema", 0);
        if (tTransferData == null || mGI == null
            || tLCInsureAccClassSchema == null)
        {
            mErrors.addOneError("传入的数据不完整");
            return null;
        }

        tTransferData.setNameAndValue("LCInsureAccClass",
                                      tLCInsureAccClassSchema);
        tTransferData.setNameAndValue("GI", mGI);
        
        //查询上一次结算信息
        LCInsureAccBalanceDB tLCInsureAccBalanceDB = new LCInsureAccBalanceDB();
        tLCInsureAccBalanceDB.setPolNo(tLCInsureAccClassSchema.getPolNo());
        tLCInsureAccBalanceDB.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
        tLCInsureAccBalanceDB.setDueBalaDate(tLCInsureAccClassSchema.
                                             getBalaDate());
        LCInsureAccBalanceSet tLCInsureAccBalanceSet
                = tLCInsureAccBalanceDB.query();
        if (tLCInsureAccBalanceSet.size() == 0)
        {
            mErrors.addOneError("没有查询到上一次结算信息");
            return null;
        }
        double tLastBala = tLCInsureAccBalanceSet.get(1).getInsuAccBalaAfter();
        double tLastGurat = tLCInsureAccBalanceSet.get(1).getInsuAccBalaGurat();
        String tBalaType=(String)tTransferData.getValueByName("BalaType");
        if("1".equals(tBalaType)){
        	tTransferData.setNameAndValue("LastBala", "" + tLastBala);
        }else if("2".equals(tBalaType)){
        	tLastGurat = (mPolYear != 1 && mPolMonth == 1 ? tLastBala :
        		tLastGurat);//计算第一保单月度的保证收益时以上一年度末账户价值为基础
        	tTransferData.setNameAndValue("LastBala", "" + tLastGurat);
        }
        

        //计算账户收益
        AccountManage tAccountManage = new AccountManage();
        MMap tMMapLX = tAccountManage.getUliBalaAccInterest(tTransferData);
        if (tMMapLX == null)
        {
            mErrors.copyAllErrors(tAccountManage.mErrors);
            return null;
        }

        LCInsureAccClassSchema accClassDelt
                = (LCInsureAccClassSchema) tMMapLX
                  .getObjectByObjectName("LCInsureAccClassSchema", 0);
        accClassDelt.setInsuAccBala(PubFun.setPrecision(accClassDelt.
                getInsuAccBala(), "0.00"));
        LCInsureAccTraceSchema accTraceDelt
                = (LCInsureAccTraceSchema) tMMapLX
                  .getObjectByObjectName("LCInsureAccTraceSchema", 0);
        accTraceDelt.setOtherType("6"); //月结算
        accTraceDelt.setOtherNo(getBalanceSequenceNo());
        accTraceDelt.setMoney(PubFun.setPrecision(accTraceDelt.getMoney(),
                                                  "0.00"));
        MMap tMMapAll = new MMap();
        tMMapAll.add(tMMapLX);

        return tMMapAll;
    }

    /**
     * 得到结算序号
     * @return String
     */
    public String getBalanceSequenceNo()
    {
        if (mBalanceSeq == null)
        {
            String[] dateArr = PubFun.getCurrentDate().split("-");
            String date = dateArr[0] + dateArr[1] + dateArr[2];
            mBalanceSeq = date
                          + PubFun1.CreateMaxNo("BalaSeq" + date, 10);
        }

        return mBalanceSeq;
    }


    private LCInsureAccClassSet getAccClass(LCInsureAccSchema
                                            tLCInsureAccSchema)
    {
        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
        tLCInsureAccClassDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
        LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
        if (tLCInsureAccClassDB.mErrors.needDealError())
        {
            CError.buildErr(this, "子帐户信息查询失败");
            return null;
        }
        if (tLCInsureAccClassSet == null || tLCInsureAccClassSet.size() < 1)
        {
            CError.buildErr(this, "没有子帐户信息");
            return null;
        }
        return tLCInsureAccClassSet;
    }

    /**
     * 传递参数
     * 准备账户号码和账户结算日期
     * @param cInputData VData
     * @return boolean
     */
    public boolean getInputData(VData cInputData,String grpcontno)
    {
        mGI = (GlobalInput) cInputData
              .getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) cInputData
                          .getObjectByObjectName("TransferData", 0);
        mGrpContNo = grpcontno;
        if(mGrpContNo == null || "".equals(mGrpContNo)){
        	buildError("getInputData", "获取团单号码失败！");
            return false;
        }
        if (tf != null)
        {
            mEndDate = (String) tf.getValueByName("EndDate");
            mBalanceSeq = (String) tf.getValueByName("EdorNo");
        }

        if (mGI == null)
        {
            buildError("getInputData", "请传入操作员信息");
            return false;
        }
        return true;
    }

    /**
     * 错误构建方法
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "InsuAccBala";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * getInsuAccRateUnBalance
     * 查询未结算的月收益利率
     * @param cLCInsureAccClassSchema LCInsureAccClassSchema，子账户
     * @return LMInsuAccRateSet，月收益率
     */
    private Interest000001Set getInterest000001(LCInsureAccSchema
            cLCInsureAccSchema)
    {
        String sql = " select a.* from Interest000001 a "
                     + " where 1 = 1 "
                     + "   and a.enddate > '"
                     + cLCInsureAccSchema.getBalaDate() + "' "
                     + " and a.enddate<='"+PubFun.getCurrentDate()+"' "
                     + " and (grpcontno = '000000' or grpcontno = '"+mGrpContNo+"') "
                     + " and a.riskcode='"+cLCInsureAccSchema.getRiskCode()+"' "
                     + " order by a.startdate ";
        System.out.println(sql+"====获取待结算利率信息");
        Interest000001DB tInterest000001DB = new Interest000001DB();
        Interest000001Set tInterest000001Set = tInterest000001DB.executeQuery(sql);
        String sqlByGrpContNo = " select a.* from Interest000001 a " +
        		"where 1 = 1  and a.enddate > '"+ cLCInsureAccSchema.getBalaDate() + "' " +
          		" and GrpContNo = '"+mGrpContNo+"'"
          		+ "order by a.startdate ";
        Interest000001Set tInterest000001Set1 = tInterest000001DB.executeQuery(sqlByGrpContNo);//用以保存个别团单录入的利率
        for(int i=1;i<=tInterest000001Set1.size();i++){
        	for(int j=1;j<=tInterest000001Set.size();j++){
        		if(tInterest000001Set1.get(i).getStartDate().equals(tInterest000001Set.get(j).getStartDate()) 
        		&& tInterest000001Set1.get(i).getEndDate().equals(tInterest000001Set.get(j).getEndDate())
        		&& tInterest000001Set1.get(i).getRiskCode().equals(tInterest000001Set.get(j).getRiskCode())
        		&& !tInterest000001Set1.get(i).getGrpContNo().equals(tInterest000001Set.get(j).getGrpContNo())){
        			tInterest000001Set.remove(tInterest000001Set.get(j));
        			j--;
        		}
        	}
        }
        if (tInterest000001Set.mErrors.needDealError())
        {
            System.out.println(tInterest000001DB.mErrors.getErrContent());
            mErrors.addOneError("查询符合该账户的月收益率发生异常");
            return null;
        }
        if (tInterest000001Set.size() == 0)
        {
            mErrors.addOneError("没有查询到符合该账户的月收益率，不能进行结算");
            return null;
        }

        return tInterest000001Set;
    }
    

    /**
     * getLCInsureAccBalance
     * 生成结算轨迹
     * @param cAccValue double
     * @param cAccValueGurat double
     * @return Object
     */
    private LCInsureAccBalanceSchema getLCInsureAccBalance1(LCInsureAccSchema glLCInsureAccSchema,Interest000001Schema
            cInterest000001Schema,
            double cAccValue,double cAccValueGurat,String flag,String isgg)
    {
        LCInsureAccBalanceSchema schema = new LCInsureAccBalanceSchema();
        ref.transFields(schema, glLCInsureAccSchema);

//        schema.setSequenceNo(getBalanceSequenceNo());
        if("1".equals(isgg) ){//isgg 1 为公共账户
        	if(!"".equals(GGSequenceno)){
        		schema.setSequenceNo(GGSequenceno);
        	}else{
        		schema.setSequenceNo(getBalanceSequenceNo());
        		GGSequenceno = schema.getSequenceNo();
        	}
        }else{
        	schema.setSequenceNo(getBalanceSequenceNo());
        }
        schema.setInsuAccBalaBefore(glLCInsureAccSchema.getInsuAccBala());
        schema.setInsuAccBalaAfter(cAccValue);
        //20150630 只有新产品安康无忧B款才计算保证账户价值
        if("370101".equals(glLCInsureAccSchema.getRiskCode())){
        	schema.setInsuAccBalaGurat("");
        }else{
        	schema.setInsuAccBalaGurat(cAccValueGurat);
        }
        schema.setDueBalaDate(cInterest000001Schema.getEndDate());
        schema.setDueBalaTime("00:00:00");
        schema.setRunDate(PubFun.getCurrentDate());
        schema.setRunTime(PubFun.getCurrentTime());
        schema.setPolYear(mPolYear);
        schema.setPolMonth(mPolMonth);
        schema.setPrintCount(0);
        schema.setOperator(mGI.Operator);
        PubFun.fillDefaultField(schema);
        if("".equals(flag)){
        	String sql = "select max(BalaCount + 1) from LCInsureAccBalance "
                + "where PolNo = '"
                + glLCInsureAccSchema.getPolNo() + "' "
                + "   and InsuAccNo = '"
                + glLCInsureAccSchema.getInsuAccNo() + "' ";
        	String maxBalaCount = new ExeSQL().getOneValue(sql);
        	if (maxBalaCount == null || maxBalaCount.equals("") ||
        			maxBalaCount.equals("null"))
        	{
        		maxBalaCount = "1";
        	}
        	schema.setBalaCount(maxBalaCount);
        	if("1".equals(isgg)){
        		GGBalaCoutn = maxBalaCount;
            }
        }else{
        	schema.setBalaCount(GGBalaCoutn);
        }
        return schema;
    }
    
    //管理费收取轨迹
    public MMap getTrace(LCInsureAccClassSchema tLCInsureAccClassSchema,double fee,String aAIPolNo,String FeeCode){
    	MMap tMMap = new MMap();
//		管理费轨迹
//		String RiskFeeSql = "select feecode,feevalue from LMRiskFee where insuaccno = '"+tLCInsureAccClassSchema.getInsuAccNo()+"' and payplancode = '"+tLCInsureAccClassSchema.getPayPlanCode()+"'";
//		SSRS RiskFeeSSRS = new ExeSQL().execSQL(RiskFeeSql);
		LCInsureAccFeeTraceSchema schema = new LCInsureAccFeeTraceSchema();
		ref.transFields(schema, tLCInsureAccClassSchema);
        String tLimit = PubFun.getNoLimit(schema.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        schema.setPayPlanCode(tLCInsureAccClassSchema.getPayPlanCode());
        schema.setSerialNo(serNo);
        schema.setFeeCode(FeeCode);
        schema.setFee(fee);
        schema.setOtherNo(this.getBalanceSequenceNo());
        schema.setOtherType("6"); //月结算
        schema.setFeeRate("0");
        schema.setPayDate(tLCInsureAccClassSchema.getBalaDate());
        schema.setState("0");
        PubFun.fillDefaultField(schema);
        String moneyType = "MF";
        schema.setMoneyType(moneyType);
        tMMap.put(schema, SysConst.INSERT);
        
//      管理费的账户轨迹
        LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
        ref.transFields(traceSchema, schema);

        traceSchema.setSerialNo(serNo);
        traceSchema.setAIPolNo(aAIPolNo);
        traceSchema.setMoney( -Math.abs(schema.getFee()));
        tMMap.put(traceSchema, SysConst.INSERT);
        return tMMap;
    }
    //生成结算信息
    public MMap getBalance(LCInsureAccSchema glLCInsureAccSchema,Interest000001Schema tInterest000001Schema,double tAccValue,double tAccValueGurat,String flag,String isGG){
    	MMap BaMMap = new MMap();
    	LCInsureAccBalanceSchema tLCInsureAccBalanceSchema
    	  = getLCInsureAccBalance1(glLCInsureAccSchema,tInterest000001Schema,
    	                          tAccValue,tAccValueGurat,flag,isGG);
    	    if (tLCInsureAccBalanceSchema == null)
    	    {
    	        return null;
    	    }
    	    if("".equals(flag)){// 公共账户未存库及所有个人账户处理
    	    	BaMMap.put(tLCInsureAccBalanceSchema, SysConst.INSERT);
    	    }else{
    	    	tLCInsureAccBalanceSchema.setSequenceNo(flag);
    	    	BaMMap.put(tLCInsureAccBalanceSchema, SysConst.UPDATE);
    	    }
    	    
    	    return BaMMap;
    }
    //根据子账户获取账户总金额
    public double getAccBalaByAccClass(LCInsureAccClassSet tLCInsureAccClassSet){
    	double accBala = 0;
    	for(int i=1;i<=tLCInsureAccClassSet.size();i++){
    		accBala = Arith.add(accBala, tLCInsureAccClassSet.get(i).getInsuAccBala());
    	}
    	return accBala;
    }
    
    //根据账户在全局变量中获取子账户
    public LCInsureAccClassSet getAccClassSetFromM(LCInsureAccSchema tLCInsureAccSchema){
    	LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
    		for(int i=1;i<=mLCInsureAccClassSet.size();i++){
        		if(mLCInsureAccClassSet.get(i).getPolNo().equals(tLCInsureAccSchema.getPolNo()) 
        				&& mLCInsureAccClassSet.get(i).getInsuAccNo().equals(tLCInsureAccSchema.getInsuAccNo())){
        			tLCInsureAccClassSet.add(mLCInsureAccClassSet.get(i));
        		}
        	}
    	return tLCInsureAccClassSet;
    }
    
    /**
     * updateAccInfo
     * 更新总账户信息
     * @return MMap
     */
    private MMap updateAccInfo(LCInsureAccSchema aLCInsureAccSchema)
    {
        MMap tMMap = new MMap();

        String sql = "update LCInsureAccClass a "
                     + "set (InsuAccBala, LastAccBala) "
                     + "   =(select sum(Money), sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo) "
                     + "where PolNo = '"
                     + aLCInsureAccSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + aLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        //更新账户信息
        sql = "update LCInsureAcc a "
              + "set (InsuAccBala, LastAccBala, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(InsuAccBala), sum(LastAccBala), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LCInsureAccClass where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + aLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + aLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        
        //按轨迹更新账户管理费信息
        sql = "update LCInsureAccClassFee a " +
              "set Fee =(select sum(Fee) from LCInsureAccFeeTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo ) "
              + "where PolNo = '"
              + aLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + aLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        //更新账户管理费信息
        sql = "update LCInsureAccFee a " +
              "set (Fee, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(Fee), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LCInsureAccClassFee where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + aLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + aLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        return tMMap;
    }
    
//   获取当前人的管理费
    private double getGlFeeByPolno(String aPolno,double aGlFee,String aBcStartDate){
    	String maxPaydateSql = "select min(paydate) from LCInsureAcctrace " +
    			"where grpcontno = '"+mGrpContNo+"' " +
    			"and polno = '"+aPolno+"' ";
		String minPaydate = new ExeSQL().getOneValue(maxPaydateSql);
		FDate fDate = new FDate();
		if(fDate.getDate(minPaydate).after(fDate.getDate(aBcStartDate))){
			int tIntv = PubFun.calInterval(minPaydate, mEndDate, "D");
			int monthLength = PubFun.monthLength(minPaydate);
//			System.out.println(Arith.div(aGlFee, monthLength,2));
//			System.out.println(Arith.mul(tIntv, Arith.div(aGlFee, monthLength,2)));
			return Arith.round(Arith.mul(tIntv, Arith.div(aGlFee, monthLength)),2);
		}else{
			return aGlFee;
		}
    }
    /**
     * 获取当前保证利率
     * @param cLCInsureAccSchema
     * @return
     */
    private LDPromiseRateSchema getLDPromiseRate(LCInsureAccClassSchema cLCInsureAccClassSchema,String prtNo,String riskCode)
    {
    	String subSQL = "";
    	if("370301".equals(riskCode)){
    		if(prtNo!=null && !"".equals(prtNo)){
    			subSQL = " and a.prtno = '"+prtNo+"'";
    		}
    	}else{
    		subSQL = " and a.prtno = '0000'";
    	}
        String sql = "SELECT * from ldpromiserate a where date('"+cLCInsureAccClassSchema.getBalaDate()+"')>=a.startdate "
        		+ " and date('"+cLCInsureAccClassSchema.getBalaDate()+"')<a.enddate "
        		+ " and exists (select riskcode from lmriskapp where risktype4='4'and Riskprop='G') "
        		+ " and riskcode='"+cLCInsureAccClassSchema.getRiskCode()+"'"
        		+ subSQL
        		+ " order by startdate ";
        LDPromiseRateDB tLDPromiseRateDB = new LDPromiseRateDB();
        LDPromiseRateSet tLDPromiseRateSet = tLDPromiseRateDB.executeQuery(sql);
        if (tLDPromiseRateSet.mErrors.needDealError())
        {
            System.out.println(tLDPromiseRateDB.mErrors.getErrContent());
            mErrors.addOneError("查询符合该账户的保证利率发生异常");
            return null;
        }
        if (tLDPromiseRateSet.size() == 0)
        {
            mErrors.addOneError("没有查询到符合该账户的保证利率，不能进行结算");
            return null;
        }
        return tLDPromiseRateSet.get(1);
    }
    /**
     * 获取结算的保证账户价值
     * @param tLCInsureAccSchema
     * @return
     */
    private double getAccValueGurat(LCInsureAccSchema tLCInsureAccSchema){
    	LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
		for(int i=1;i<=guratLCInsureAccClassSet.size();i++){
    		if(guratLCInsureAccClassSet.get(i).getPolNo().equals(tLCInsureAccSchema.getPolNo()) 
    				&& guratLCInsureAccClassSet.get(i).getInsuAccNo().equals(tLCInsureAccSchema.getInsuAccNo())){
    			tLCInsureAccClassSet.add(guratLCInsureAccClassSet.get(i));
    		}
    	}
		double value=tLCInsureAccClassSet.get(1).getInsuAccBala();
		return value;
    }
    
    /**
     * 
     * @param schema  结算账户价值
     * @param vschema 保证账户价值
     * @param cLCInsureAccTraceSchema	任一轨迹 辅助生成数据
     * @return
     */
    private MMap dealAccountValueDif(LCInsureAccClassSchema schema,LCInsureAccClassSchema vschema,
                                     LCInsureAccTraceSchema cLCInsureAccTraceSchema,String mEndDate)
    {
        MMap tMMap = new MMap();

        if (12 !=mPolMonth )
        {
            return tMMap;
        }

        double dif = schema.getInsuAccBala() - vschema.getInsuAccBala();
        if (dif >= 0)
        {
            return tMMap;
        }

        dif = Math.abs(PubFun.setPrecision(dif, "0.00"));
        LCInsureAccTraceSchema accTrace = new LCInsureAccTraceSchema();
        ref.transFields(accTrace, cLCInsureAccTraceSchema);
        accTrace.setMoney(dif);
        accTrace.setMoneyType("VD"); //年度收益补差

        String tLimit = PubFun.getNoLimit(accTrace.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        accTrace.setSerialNo(serNo);
        tMMap.put(accTrace, SysConst.INSERT);

        String sql = "update LCInsureAccClass a "
                     + "set InsuAccBala = InsuAccBala + " + dif + ", "
                     + "    LastAccBala = InsuAccBala + " + dif + " "
                     + "where PolNo = '" + accTrace.getPolNo() + "' "
                     + "   and InsuAccNo = '" + accTrace.getInsuAccNo() + "' "
                     + "   and PayPlanCode = '" + accTrace.getPayPlanCode() +
                     "' ";
        String sq2 = "update LCInsureAcc a "
            + "set InsuAccBala = InsuAccBala + " + dif + ", "
            + "    LastAccBala = InsuAccBala + " + dif + " "
            + "where PolNo = '" + accTrace.getPolNo() + "' "
            + "   and InsuAccNo = '" + accTrace.getInsuAccNo() + "' ";
        
        String sq3 = "update LCInsureAccBalance a "
            + "set InsuAccBalaAfter = InsuAccBalaAfter + " + dif + " "
            + "where PolNo = '" + accTrace.getPolNo() + "' "
            + " and InsuAccNo = '" + accTrace.getInsuAccNo() + "' "
            + " and dueBalaDate='"+mEndDate+"'"
            + " and polyear='"+mPolYear+"'"
            + " and polmonth='"+mPolMonth+"'";
        
        tMMap.put(sql, SysConst.UPDATE);
        tMMap.put(sq2, SysConst.UPDATE);
        tMMap.put(sq3, SysConst.UPDATE);

        return tMMap;
    }
    
    public static void main(String arg[])
    {

//    		System.out.println(PubFun.monthLength("2014-02-01"));
//    		System.out.println(PubFun.calInterval("2014-02-10", "2014-03-01", "D"));
//    		AccountManage tAccountManage = new AccountManage();
//    		System.out.println(tAccountManage.TransAccRate(0.4500000000, "1", "C", "D",28,365));
    	
//        double tmpIntrest = 1;
//        for(int i=1;i<=28;i++){
//        	tmpIntrest = Arith.mul(tmpIntrest, Arith.add(0.0010185006662843943, 1));
//        }
//        System.out.println(tmpIntrest);
////        
//        double aAccClassInterest = 0.0;
//        aAccClassInterest = Arith.add(Arith.mul(61763.020000, Arith.sub(
//				tmpIntrest, 1)), aAccClassInterest);
//        double resultMoney = Arith.add(61763.020000, aAccClassInterest);
//        System.out.println(aAccClassInterest+"===="+resultMoney);
//        resultMoney = Arith.round(resultMoney, 2);
//        System.out.println(resultMoney);
        
      double tmpIntrest = 1;
      for(int i=1;i<=19;i++){
      	tmpIntrest = Arith.mul(tmpIntrest, Arith.add(0.0010185006662843943, 1));
      }
      System.out.println(tmpIntrest);
//      
      double aAccClassInterest = 0.0;
      aAccClassInterest = Arith.add(Arith.mul(49950.00, Arith.sub(
				tmpIntrest, 1)), aAccClassInterest);
      double resultMoney = Arith.add(63548.81, aAccClassInterest);
      System.out.println(aAccClassInterest+"===="+resultMoney);
      resultMoney = Arith.round(resultMoney, 2);
      System.out.println(resultMoney);
        
        double tLX = Arith.round(Arith.sub(resultMoney, 61763.020000), 2);  //收益
        System.out.println(tLX);
    }
}
