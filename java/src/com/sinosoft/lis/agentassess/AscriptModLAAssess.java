package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAssessBDB;
import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAssessBSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class AscriptModLAAssess
{
//错误处理类
    public static CErrors mErrors = new CErrors();
//业务处理相关变量
    /** 全局数据 */
    private VData mInputData = new VData();
    public GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSet mLAAssessSet = new LAAssessSet();

    public AscriptModLAAssess()
    {
    }

    public static void main(String[] args)
    {
        AscriptModLAAssess ascriptModLAAssess1 = new AscriptModLAAssess();

    }

    public boolean dealData(VData cInputData)
    {
        System.out.println("----come into dealData---");
        System.out.println("----cInputData.size: " + cInputData.size());
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!verifyData())
        {
            return false;
        }

        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            dealError("dealData", "数据库连接失败！");
            return false;
        }

        LAAssessSchema tSch = new LAAssessSchema();
        int tCount = this.mLAAssessSet.size();
        if (tCount == 0)
        {
            dealError("DealData", "未找到考核信息！");
            return false;
        }

        try
        {
            conn.setAutoCommit(false);
            for (int i = 1; i <= tCount; i++)
            {
                tSch = this.mLAAssessSet.get(i);
                System.out.println("---dealdata--" + i + "---AgentCode: " +
                                   tSch.getAgentCode());
                if (!insLAAssessB(tSch, conn))
                {
                    conn.rollback();
                    conn.close();
                    dealError("dealData", "刷新失败！");
                    return false;
                }

                if (!updLAAssess(tSch, conn))
                {
                    conn.rollback();
                    conn.close();
                    dealError("dealData", "刷新失败！");
                    return false;
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}

            dealError("dealData", ex.toString());
            return false;
        }
        return true;
    }

    private boolean insLAAssessB(LAAssessSchema tSch, Connection conn)
    {
        LAAssessBDB tBDB = new LAAssessBDB(conn);
        LAAssessBSchema tBSch = new LAAssessBSchema();

        LAAssessDB tDB = new LAAssessDB(conn);
        LAAssessSchema tSch1 = new LAAssessSchema();
        tDB.setAgentCode(tSch.getAgentCode());
        tDB.setIndexCalNo(tSch.getIndexCalNo());
        if (!tDB.getInfo())
        {
            dealError("insLAAssessB", "查询当前考评信息表失败！");
            return false;
        }
        tSch1.setSchema(tDB.getSchema());

        Reflections tRf = new Reflections();

        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();
        tRf.transFields(tBSch, tSch1);
        tBSch.setModifyDate(tPF.getCurrentDate());
        tBSch.setModifyTime(tPF.getCurrentTime());
        tBSch.setMakeDate(tPF.getCurrentDate());
        tBSch.setMakeTime(tPF.getCurrentTime());
        tBSch.setEdorType("01");
        tBSch.setEdorNo(tPF1.CreateMaxNo("EdorNo", 20));
        tBSch.setEdorDate(tPF.getCurrentDate());
        tBDB.setSchema(tBSch);
        if (!tBDB.insert())
        {
            dealError("insLAAssessB", "插入考评信息备份表失败！");
            return false;
        }

        return true;
    }

    private boolean updLAAssess(LAAssessSchema tSch, Connection conn)
    {
        System.out.println("---come into updLAAssess---");
        LAAssessDB tDB = new LAAssessDB(conn);
        LAAssessSchema tOldSch = new LAAssessSchema();
        PubFun tPF = new PubFun();

        String tAgentGroup = "";
        ExeSQL tExeSql = new ExeSQL();
        String tSql =
                "select agentgroup from labranchgroup where branchattr = '"
                + tSch.getAgentGroup().trim() + "'";
        tAgentGroup = tExeSql.getOneValue(tSql);
        if (tAgentGroup == null || tAgentGroup.equals(""))
        {
            dealError("updLAAssess", "查询代理人当前展业机构编码失败！");
            return false;
        }

        tOldSch.setSchema(tSch);
        tOldSch.setModifyDate(tPF.getCurrentDate());
        tOldSch.setModifyTime(tPF.getCurrentTime());
        tOldSch.setConfirmer(this.mGlobalInput.Operator);
        tOldSch.setConfirmDate(tPF.getCurrentDate());
        tOldSch.setState("1");
        tOldSch.setAgentGroup(tAgentGroup);
        tOldSch.setAgentGroupNew("");

        tDB.setSchema(tOldSch);
        if (!tDB.update())
        {
            return false;
        }

        return true;
    }

    private boolean verifyData()
    {
        System.out.println("---come into verifyData---");
        LAAssessSchema tSch = new LAAssessSchema();
        int tCount = this.mLAAssessSet.size();
        for (int i = 1; i <= tCount; i++)
        {
            tSch = this.mLAAssessSet.get(i);
            if (tSch.getAgentGrade1() == null ||
                tSch.getAgentGrade1().equals(""))
            {
                dealError("verifyData", "建议的职级不得为空值!");
                return false;
            }
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
            this.mLAAssessSet = (LAAssessSet) cInputData.getObjectByObjectName(
                    "LAAssessSet", 0);
            System.out.println("---mLAAssessSet.size : " +
                               this.mLAAssessSet.size());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }

        int tCount = this.mLAAssessSet.size();
        if (tCount == 0)
        {
            dealError("getInputdata", "没有传入数据！");
            return false;
        }
        return true;
    }

    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "AssessConfirmSave";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }
} //end class
