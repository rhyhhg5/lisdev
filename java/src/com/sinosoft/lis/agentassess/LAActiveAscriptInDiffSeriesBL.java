package com.sinosoft.lis.agentassess;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LAAssessAccessorySet;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LARecomRelationBSet;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LARecomRelationBSchema;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LARecomRelationDB;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.agentcalculate.CalFormDrawRate;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.agent.LARearPICCHBL;

/**
 * <p>
 * Title: AgentSystem
 * </p>
 * <p>
 * Description: 销售管理系统
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 *
 * @author caigang
 * @version 1.0
 */
public class LAActiveAscriptInDiffSeriesBL {
	public CErrors mErrors = new CErrors();

	private GlobalInput mGlobalInput = new GlobalInput();

	private LAAssessAccessorySchema mLAAssessAccessorySchema = new LAAssessAccessorySchema();

	private LACommisionSet mLACommisionSet = new LACommisionSet();

	private LATreeSet mLATreeSet = new LATreeSet();

	private LATreeBSet mLATreeBSet = new LATreeBSet();

	private LAAgentSet mLAAgentSet = new LAAgentSet();

	private LAAgentBSet mLAAgentBSet = new LAAgentBSet();

	private LATreeSet elseLATreeSet = new LATreeSet();

	private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();

	private LABranchGroupSet mstopLABranchGroupSet = new LABranchGroupSet();

	private LABranchGroupBSet mLABranchGroupBSet = new LABranchGroupBSet();

	private AscriptBranchGroupOperateBL mBranchGroupOperateBL = new AscriptBranchGroupOperateBL();

	private LADimissionSet mLADimissionSet = new LADimissionSet();

	private LATreeSchema mLATreeSchema = new LATreeSchema();

	private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

	private LARecomRelationSet mLARecomRelationSet = new LARecomRelationSet();

	private LARecomRelationBSet mLARecomRelationBSet = new LARecomRelationBSet();

	private LARearRelationSet mLARearRelationSetUpdate = new LARearRelationSet();// 育成关系修改

	private LARearRelationBSet mLARearRelationBSet = new LARearRelationBSet();// 育成关系备份

	private LARearRelationSet mLARearRelationSetInsert = new LARearRelationSet();// 育成关系建立

	private LARearRelationDB tLARearRelationDBIndirect = new LARearRelationDB();// 间接育成关系

	private AscriptActiveRearQueryDealBL mRearQueryDealBL = new AscriptActiveRearQueryDealBL();

	private AscriptBusinessOperateBL mAscriptBusinessOperateBL = new AscriptBusinessOperateBL();

	private AscriptAgentInfoOperateBL mAgentInfoOperateBL = new AscriptAgentInfoOperateBL();

	private AscriptRelationOperateBL mRelationOperateBL = new AscriptRelationOperateBL();

	private LARearRelationSet mLARearRelationSetDelete = new LARearRelationSet();

	private LABranchGroupSchema mChangeLABranchGroupSchema = new LABranchGroupSchema();

	private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();

	LABranchGroupSchema NewGroupDB = new LABranchGroupSchema();

	private VData mInputData = new VData();

	private VData mOutputData = new VData();

	private MMap mMap = new MMap();

	private String mEdorType = "31";

	private String mEdorNo = "";

	private String mAscriptDate = "";

	private String empolyDate = "";

	private String mMAXATTR = "";

	private String mIndexCalNo = "";

	private String[] mEndDate = new String[2];

	private String currentDate = PubFun.getCurrentDate();

	private String currentTime = PubFun.getCurrentTime();

	public LAActiveAscriptInDiffSeriesBL() {
	}

	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		mLAAssessAccessorySchema = (LAAssessAccessorySchema) cInputData
				.getObjectByObjectName("LAAssessAccessorySchema", 0);
		if (mLAAssessAccessorySchema == null) {
			CError tError = new CError();
			tError.moduleName = "LAAscriptInSameSeriesBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "未得到足够数据!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean check() {
		return true;
	}

	private boolean getBaseInfo(LAAssessAccessorySchema cLAAssessAccessorySchema) {
		mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
		String tStandAssessFlag = ""
				+ cLAAssessAccessorySchema.getStandAssessFlag();
		if (tStandAssessFlag.equals("1")) {
			mEdorType = "01";// 考核 修改
		} else if (tStandAssessFlag.equals("0")) {
			mEdorType = "31";// 手动 页面 修改
		}
		String tIndexCalNo = cLAAssessAccessorySchema.getIndexCalNo();
		mAscriptDate = tIndexCalNo + "01";
		mAscriptDate = AgentPubFun.formatDate(mAscriptDate, "yyyy-MM-dd");
		mEndDate = PubFun.calFLDate(mAscriptDate);
		mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);// 开始时间
//		职级调整日期必须为考核月的下个月1号开始,除了新入司的以外
      	ExeSQL newExeSQL = new ExeSQL();
        String newSQL = "select employdate from laagent where agentcode='"+mLAAssessAccessorySchema.getAgentCode()+"' and branchtype='5' and branchtype2='01'";
        empolyDate = newExeSQL.getOneValue(newSQL);
        String empMonth= AgentPubFun.formatDate(empolyDate, "yyyyMM");
        mAscriptDate = tIndexCalNo +"01";
        mAscriptDate = AgentPubFun.formatDate(mAscriptDate, "yyyy-MM-dd");
        mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);
        
        String tAscriptTemp = AgentPubFun.formatDate(mAscriptDate, "yyyyMM");
        System.out.println("入司日期："+empMonth+"/归属日期："+tAscriptTemp);
        if(empMonth.compareTo(tIndexCalNo)==0)
        	mAscriptDate = empolyDate;
        
//        if(empMonth.compareTo(tAscriptTemp)==0)
//        	mAscriptDate = empolyDate;
//        System.out.println("----------->"+mAscriptDate);
		// 得到完整的LAAssessAccessory的信息
		LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
		tLAAssessAccessoryDB.setSchema(cLAAssessAccessorySchema);
		if (!tLAAssessAccessoryDB.getInfo()) {
			this.mErrors.copyAllErrors(tLAAssessAccessoryDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAActiveAscriptInSameSeriesBL";
			tError.functionName = "ascriptToBusSeries";
			tError.errorMessage = "未查询到"
					+ cLAAssessAccessorySchema.getAgentCode() + "在"
					+ tIndexCalNo + ",考核类型为"
					+ cLAAssessAccessorySchema.getAssessType() + "的考核结果记录！!";
			this.mErrors.addOneError(tError);
			return false;
		}
		cLAAssessAccessorySchema = tLAAssessAccessoryDB.getSchema();
		return true;
	}

	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("cOperate:" + cOperate);
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!check()) {
			return false;
		}
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			CError tError = new CError();
			tError.moduleName = "LAAscriptInDiffSeriesBL";
			tError.functionName = "dealData";
			tError.errorMessage = "往PubSubmit准备数据时出错！";
			this.mErrors.addOneError(tError);
			return false;
		}
		// 先建立起团队，再进行修改
//		if(!dealGBuild())
//		{
//			return false;
//		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAAscriptInDiffSeriesBL";
			tError.functionName = "dealData";
			tError.errorMessage = "PubSubmit保存数据时出错！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dealData() {
		mIndexCalNo = mLAAssessAccessorySchema.getIndexCalNo();
		if (!getBaseInfo(mLAAssessAccessorySchema))
			return false;
		String tOldGrade = mLAAssessAccessorySchema.getAgentGrade();
		String tAagentGrp = mLAAssessAccessorySchema.getAgentGroup();
		String tNewGrade = mLAAssessAccessorySchema.getAgentGrade1();
		String OldAgentSeries = this.mLAAssessAccessorySchema.getAgentSeries();
		String NewAgentSeries = this.mLAAssessAccessorySchema.getAgentSeries1();
		System.out.println("tOldGrade" + tOldGrade);
		System.out.println("tNewGrade" + tNewGrade);
		System.out.println("OldAgentSeries" + OldAgentSeries);
		System.out.println("NewAgentSeries" + NewAgentSeries);
		// 跨系列晋升:1、业务员晋升为主任，2、处长晋升为区经理
		if ((!tOldGrade.substring(0,1).equals("B") && OldAgentSeries
				.compareTo(NewAgentSeries) < 0)
				|| tOldGrade.equals("B01")
				&& !tNewGrade.equals("B01")
				&& NewAgentSeries.compareTo("1") >= 0
				|| (tNewGrade.equals("B01") && OldAgentSeries.equals("0"))) {
			mBranchGroupOperateBL = new AscriptBranchGroupOperateBL();
			if (!updateGroup(this.mLAAssessAccessorySchema.getAgentCode(),
					this.mLAAssessAccessorySchema.getAgentGroup(),
					NewAgentSeries)) {
				CError.buildErr(this, "更新机构失败！");
				return false;
			}
         //获取原团队的agentgroup
//			LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
//			tLABranchGroupDB.setAgentGroup(tAagentGrp);
//			tLABranchGroupDB.getInfo();
//			mLABranchGroupSchema = tLABranchGroupDB.getSchema();
			NewGroupDB.setSchema(mLABranchGroupSchema);

			if (NewGroupDB == null || NewGroupDB.getAgentGroup() == null
					|| NewGroupDB.getAgentGroup().equals("")) {
				CError.buildErr(this, "取得机构失败！");
				return false;
			}

			// 获得机构，进行机构的变化
			if (!changeAgentGroup(this.mLAAssessAccessorySchema.getAgentCode(),
					 NewGroupDB.getAgentGroup(),
					NewGroupDB.getUpBranch())) {
				CError.buildErr(this, "机构改变失败！");
				return false;
			}
			if (!changeAgentGrade(this.mLAAssessAccessorySchema.getAgentCode(),
					tNewGrade)) {
				CError.buildErr(this, "改变职级失败！");
				return false;
			}
			LABranchGroupDB QueryGroupDB = new LABranchGroupDB();
			QueryGroupDB.setAgentGroup(this.mLAAssessAccessorySchema
					.getAgentGroup());
			if (!QueryGroupDB.getInfo()) {
				CError.buildErr(this, "机构信息查询失败！");
				return false;
			}

			if (!changeOtherGroup(this.mLAAssessAccessorySchema.getAgentCode(),
					NewGroupDB.getAgentGroup())) {
				CError.buildErr(this, "连带关系处理失败！");
				return false;
			}
			if (mLAAssessAccessorySchema.getAgentSeries1().equals("2")) {
				// 查找出直辖组以及育成组
				String SQL = " select * from labranchgroup where agentgroup  in"
						+ "( select branchcode from latree where agentcode='"
						+ mLAAssessAccessorySchema.getAgentCode()
						+ "'"
						+ " union (select agentgroup From larearrelation where rearagentcode='"
						+ mLAAssessAccessorySchema.getAgentCode() + "'" + " ))";
				LABranchGroupDB cLABranchGroupDB = new LABranchGroupDB();
				LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
				System.out.println(SQL);
				tLABranchGroupSet = cLABranchGroupDB.executeQuery(SQL);
				if (cLABranchGroupDB.mErrors.needDealError()) {
					CError.buildErr(this, "直辖组以及育成组查询失败!");
					return false;
				}
				for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
					String cAgentGroup = tLABranchGroupSet.get(i)
							.getAgentGroup();
					if (!updateGroupInfo(cAgentGroup, NewGroupDB)) {
						return false;
					}
					LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
					tLABranchGroupSchema.setSchema(mChangeLABranchGroupSchema);
					// 修改业绩，晋升为区以后需要修改团队信息（包括本人）
					if (!setGrptoGroup(cAgentGroup, tLABranchGroupSchema)) {
						CError.buildErr(this, "修改代理人"
								+ mLAAssessAccessorySchema.getAgentCode()
								+ "的业绩信息失败！");
						return false;
					}

				}
			} else// 晋升为主任后的修改带走人业绩
			{

				if (!changeOtherCommision(mLAAssessAccessorySchema
						.getAgentCode(), NewGroupDB)) {
					CError.buildErr(this, "修改业绩失败!");
					return false;
				}
			}
			LATreeSchema selfLATreeSchema = new LATreeSchema();
			LATreeDB selfLATreeDB = new LATreeDB();
			selfLATreeDB.setAgentCode(mLAAssessAccessorySchema.getAgentCode());
			selfLATreeDB.getInfo();
			selfLATreeSchema.setSchema(selfLATreeDB.getSchema());
			// LABranchGroupSchema tmLABranchGroupSchema = new
			// LABranchGroupSchema();
			// tmLABranchGroupSchema = tLABranchGroupSet.get(1);//主管的直辖组
			System.out.println("selfLATreeSchema.getBranchCode()"
					+ selfLATreeSchema.getBranchCode());
			System.out.println("selfLATreeSchema.getBranchCode()"
					+ NewGroupDB.getAgentGroup());

			// 修改主管本人的业绩，其中区经理特殊处理
			if (!setPertoGroup(mLAAssessAccessorySchema.getAgentCode(),
					selfLATreeSchema.getBranchCode(), NewGroupDB)) {
				CError.buildErr(this, "修改代理人"
						+ mLAAssessAccessorySchema.getAgentCode() + "的业绩信息失败！");
				return false;
			}
			// 归属保单和险种表,归属应收表(只需要归属本人)
			if (!setContToGroupNew(
					mLAAssessAccessorySchema.getAgentCode(), NewGroupDB)) {
				CError.buildErr(this, "修改代理人"
						+ mLAAssessAccessorySchema.getAgentCode() + "的业绩信息失败！");
				return false;
			}
			/*
			 * 育成关系代处理
			 */

			String tUpagent = "select upagent from latree where agentcode = '"
			+ this.mLAAssessAccessorySchema.getAgentCode() + "'";
			ExeSQL tExeSQL = new ExeSQL();
			String Upagent = tExeSQL.getOneValue(tUpagent);

			//断掉育成关系
//			if(!BreakAgentRear(selfLATreeSchema))
//			{
//				CError.buildErr(this, "育成关系断裂失败！");
//				return false;
//			}

//函数没有去掉 直销处业务人员晋升处经理后，也与主管建立了育成关系，即可能出现处经理与区经理形成育成关系
//			if (!dealLARear(this.mLAAssessAccessorySchema.getAgentCode(),
//					NewGroupDB.getAgentGroup(),Upagent)) {
//                            if (!dealLARear(this.mLAAssessAccessorySchema.getAgentCode(),
//                            this.mLAAssessAccessorySchema.getAgentSeries1(),
//					NewGroupDB.getAgentGroup(),Upagent)) {
//				CError.buildErr(this, "育成关系处理失败！");
//				return false;
//			}
			/*
			 * 修改推荐关系的AgentGroup
			 */
	//		String tChangeGroup = selfLATreeSchema.getAgentGroup();
			// 处
			if (mLAAssessAccessorySchema.getAgentSeries1().equals("1")) {
				LATreeSet tRearLATreeSet = new LATreeSet();
				tRearLATreeSet.add(selfLATreeSchema);
				tRearLATreeSet.add(elseLATreeSet);
				if (!changeRecomRelation(tRearLATreeSet,
						NewGroupDB.getAgentGroup())) {
					CError.buildErr(this, "修改推荐关系失败！");
					return false;
				}
			}
			// 区
			else if (mLAAssessAccessorySchema.getAgentSeries1().equals("2")) {
				LATreeSet tRearLATreeSet = new LATreeSet();
				tRearLATreeSet.add(selfLATreeSchema);
				if (!changeRecomRelation(tRearLATreeSet,
						NewGroupDB.getAgentGroup())) {
					CError.buildErr(this, "修改推荐关系失败！");
					return false;
				}
			}
		} else// 降级
		{

			LABranchGroupDB OldGroupDB = new LABranchGroupDB();
			OldGroupDB.setAgentGroup(mLAAssessAccessorySchema.getAgentGroup());
			if (!OldGroupDB.getInfo()) {
				CError.buildErr(this, "得到代里人原来的机构信息失败！");
				return false;
			}
			// 找到要归属的团队
			if (!mRearQueryDealBL.BackToRearGroup(mLAAssessAccessorySchema
					.getAgentCode(), OldGroupDB, mLAAssessAccessorySchema
					.getAgentSeries1(),mLAAssessAccessorySchema.getAgentGrade1())) {
				this.mErrors.copyAllErrors(mRearQueryDealBL.mErrors);
				return false;
			}
			LABranchGroupSchema BacktoGroupDB = new LABranchGroupSchema();
			BacktoGroupDB.setSchema(mRearQueryDealBL.getOldGroup());
			// 如果找不到要归属的团队
			if (BacktoGroupDB == null || BacktoGroupDB.getAgentGroup() == null
					|| BacktoGroupDB.getAgentGroup().equals("")) {
				if (!Proceedfive()) {
					return false;
				}
				return true;
			}
			// 如果找到了归属的团队,处理主管本人的行政信息
			if (mLAAssessAccessorySchema.getAgentSeries1().equals("0")
					|| mLAAssessAccessorySchema.getAgentGrade1().equals("B01"))// 降为业务员
			{
				if (!changeAgentGroup(mLAAssessAccessorySchema.getAgentCode(),
						BacktoGroupDB.getAgentGroup(),
						BacktoGroupDB.getBranchManager())) {
					CError.buildErr(this, "AgentGroup归属失败！");
					return false;
				}


			} else if (mLAAssessAccessorySchema.getAgentSeries1().equals("1"))// 降为处长
			{
				LATreeSchema tLATreeSchema = new LATreeSchema();
				LATreeDB tLATreeDB = new LATreeDB();
				tLATreeDB.setAgentCode(mLAAssessAccessorySchema.getAgentCode());
				tLATreeDB.getInfo();
				tLATreeSchema.setSchema(tLATreeDB.getSchema());
				if (tLATreeSchema == null
						|| tLATreeSchema.getAgentCode() == null
						|| tLATreeSchema.getAgentCode().equals("")) {
					CError.buildErr(this, "取得代理人行政信息失败！");
					return false;
				}
				if (!changeAgentGroup(mLAAssessAccessorySchema.getAgentCode(),
						tLATreeSchema.getBranchCode(), BacktoGroupDB
								.getBranchManager())) {
					CError.buildErr(this, "改变代理人行政信息失败！");
					return false;
				}

			}
		   //处理主管的职级
			if (!changeAgentGrade(mLAAssessAccessorySchema.getAgentCode(),
					mLAAssessAccessorySchema.getAgentGrade1())) {
				CError.buildErr(this, "职级改变失败！");
				return false;
			}
			//处理降职人员连带的其他业务员行政信息
//			if(!changeOtherGroup(mLAAssessAccessorySchema.getAgentCode(),
//					BacktoGroupDB.getAgentGroup()))
//			{
//				CError.buildErr(this, "改变组或于成人的行政信息失败！");
//				return false;
//			}
			if (!mRearQueryDealBL.DisMissAgentSet(mLAAssessAccessorySchema
					.getAgentCode(),
					mLAAssessAccessorySchema.getAgentSeries1(),
					mLAAssessAccessorySchema.getAgentGrade1(),
					mLAAssessAccessorySchema.getAgentSeries(),
					mLAAssessAccessorySchema.getAgentGrade())) { // 如果是区经理降级,则找到他的下属主管;如果是处降级,则找到他的下属业务员
				CError.buildErr(this, "解散代理人直接下属失败！");
				return false;
			}

			VData AgentData = new VData();
			AgentData = mRearQueryDealBL.getAgentSet();

			LATreeSchema selfLATreeSchema = new LATreeSchema();
			LATreeDB selfLATreeDB = new LATreeDB();
			selfLATreeDB.setAgentCode(mLAAssessAccessorySchema.getAgentCode());
			selfLATreeDB.getInfo();
			selfLATreeSchema.setSchema(selfLATreeDB.getSchema());
			System.out.println("selfLATreeSchema.getAgentCode()"+selfLATreeSchema.getAgentCode());
			LATreeSet rearLATreeDBSet = new LATreeSet();
			rearLATreeDBSet = (LATreeSet) AgentData.getObjectByObjectName(
					"LATreeSet", 0);
			System.out.println("size大小="+rearLATreeDBSet.size());
			/*
			 * 修改推荐关系的AgentGroup， 降级为业务系列，人员团队需要发生变动所以需要修改所有需要归属的人
			 * 降级为主管，只有本人的agentgroup变动
			 */
			String tChangeGroup = BacktoGroupDB.getAgentGroup();
			System.out.println("tChangeGroup"+tChangeGroup);
			if (mLAAssessAccessorySchema.getAgentSeries1().equals("1")//降为处经理
					&& !mLAAssessAccessorySchema.getAgentGrade1().equals("B01")) {
				LATreeSet tRearLATreeSet = new LATreeSet();
				tRearLATreeSet.add(selfLATreeSchema);
				if (!changeRecomRelation(tRearLATreeSet,
						tChangeGroup)) {
					CError.buildErr(this, "修改推荐关系失败！");
					return false;
				}

			} else if (mLAAssessAccessorySchema.getAgentSeries1().equals("0")
					|| mLAAssessAccessorySchema.getAgentGrade1().equals("B01")) {
				LATreeSet tRearLATreeSet = new LATreeSet();
				tRearLATreeSet.add(selfLATreeSchema);
				tRearLATreeSet.add(rearLATreeDBSet);
				if (!changeRecomRelation(tRearLATreeSet,
						tChangeGroup)) {
					CError.buildErr(this, "修改推荐关系失败！");
					return false;
				}
			}
			if (mLAAssessAccessorySchema.getAgentSeries1().equals("1")
					&& !mLAAssessAccessorySchema.getAgentGrade1().equals("B01")) { // 如果是区经理降级
				rearLATreeDBSet.add(selfLATreeSchema);// 本人最后处理
				System.out.println("rearLATreeDBSet大小："+rearLATreeDBSet.size());
				System.out.println("AgentGroup:"+rearLATreeDBSet.get(1).getAgentGroup());
				for (int i = 1; i <= rearLATreeDBSet.size(); i++) {
					if (!updateGroupInfo(
							rearLATreeDBSet.get(i).getBranchCode(),
							BacktoGroupDB)) {
						CError.buildErr(this, "修改代理人"
								+ mLAAssessAccessorySchema.getAgentCode()
								+ "的信息失败！");
						return false;
					}
					LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
					tLABranchGroupSchema = mChangeLABranchGroupSchema;
					// 修改业绩，降级为处以后需要修改团队信息（包括本人）。。。有问题
					if (!setGrptoGroup(
							rearLATreeDBSet.get(i).getAgentGroup(),
							tLABranchGroupSchema)) {
						CError.buildErr(this, "修改代理人"
								+ mLAAssessAccessorySchema.getAgentCode()
								+ "的业绩信息失败！");
						return false;
					}
					// 归属保单和险种表,归属应收表(只需要归属本人)
					if (rearLATreeDBSet.size() == i) {// 本人在最后作团队处理
						if (!setContToGroupNew(
								mLAAssessAccessorySchema.getAgentCode(),
								tLABranchGroupSchema)) {
							CError.buildErr(this, "修改代理人"
									+ mLAAssessAccessorySchema.getAgentCode()
									+ "的业绩信息失败！");
							return false;
						}
					}
				}
					 rearLATreeDBSet.remove(selfLATreeSchema);
		                for (int j = 1; j <= rearLATreeDBSet.size(); j++)
		                {
		                    if (!changeManagerInfo(rearLATreeDBSet.get(j),
		                            BacktoGroupDB.getBranchManager()))
		                    {
		                        CError.buildErr(this, "改变代理人行政信息失败！");
		                        return false;
		                    }

		                }
                  System.out.println("~~~~~~~"+selfLATreeSchema.getAgentCode());
					 //处理育成关系
					if(!BreakAgentRear(selfLATreeSchema))
					{
						CError.buildErr(this, "培养关系处理失败！");
					    return false;
					}

//					String zxBranch = "select agentgroup from labranchgroup where upbranch='"+selfLATreeSchema.getAgentGroup()+"' and upbranchattr='1'";
//					ExeSQL zxExeSQL = new ExeSQL();
//				    String zxAgentGroup = zxExeSQL.getOneValue(zxBranch);
//				    if(zxAgentGroup==null||zxAgentGroup.equals(""))
//				    {
//				    	CError.buildErr(this, "主管直辖机构维护出错！");
//				    	return false;
//				    }
//					//育成关系处理的时候，当区降为处长：区育成区关系改为区育成处，向上育成关系
//					if (!dealLARear(this.mLAAssessAccessorySchema.getAgentCode(),
//							zxAgentGroup,BacktoGroupDB.getBranchManager())) {
//						CError.buildErr(this, "育成关系处理失败！");
//						return false;
//					}
				}

			else//降为业务员的处理
			{
				//处理下面的业务员信息
				for(int k=1;k<=rearLATreeDBSet.size();k++)
				{
					if(!updateAgentInfo(rearLATreeDBSet.
                            get(k).
                            getAgentCode(),
                            BacktoGroupDB)) {
                        CError.buildErr(this, "改变代理人行政信息失败！");
                        return false;
                    }
				}
				for (int i = 1; i <= rearLATreeDBSet.size(); i++) {
	                    if (!setPertoGroup(//不包括本人，本人在下面处理
	                            rearLATreeDBSet.
	                            get(i).getAgentCode(),
	                            BacktoGroupDB)) {
	                        CError.buildErr(this,
	                                        "修改代理人" + rearLATreeDBSet.
	                                        get(i).getAgentCode() +
	                                        "的业绩信息失败！");
	                        return false;
	                    }
	                    //归属保单和险种表,归属应收表(不包括本人),本人在下面处理
	                    if (!setContToGroupNew(
	                            rearLATreeDBSet.get(i).getAgentCode(),
	                            BacktoGroupDB))
	                    {
	                        CError.buildErr(this,
	                                        "修改代理人" +
	                                        mLAAssessAccessorySchema.getAgentCode() +
	                                        "的业绩信息失败！");
	                        return false;
	                    }

	               }
	               	if(!setPertoGroup(this.mLAAssessAccessorySchema.getAgentCode(),BacktoGroupDB))
	               	{
	               		CError.buildErr(this, "修改主管业绩失败！");
	               		return false;
	               	}
	               	if(!setContToGroupNew(mLAAssessAccessorySchema.getAgentCode(),BacktoGroupDB))
	               	{
	               		CError.buildErr(this, "修改主管保单、险种表失败!");
	               		return false;
	               	}
		            /*
		                    育成关系处理
		           */
			       if (!BreakAgentRear(selfLATreeSchema)) {
			           CError.buildErr(this, "断开血缘关系失败！");
			           return false;
		       		}
	            }

	            if (!backupThenStopGroup(OldGroupDB.getSchema())) {
	                CError.buildErr(this, "撤销机构失败！", "AscriptControlBLF", "Proceedthree");
	                return false;
	            }
	            //获得处理机构的Schema

			}

		return true;
	}
	 /*
    备份并撤销机构
    参数:cLABranchGroupDB-目的机构的信息
    */
   public boolean backupThenStopGroup(LABranchGroupSchema cLABranchGroupDB)//LATreeSchema cLATreeSchema)
   {
       LABranchGroupSchema tLABranchGroupDB=new LABranchGroupSchema();
       tLABranchGroupDB=cLABranchGroupDB;
       //在前面处理的时候已经做过备份
//       if(!backupBranchGroup(tLABranchGroupDB))
//       {
//           CError.buildErr("备份人员的原机构的信息出错！","backupThenStopGroup");
//           return false;
//       }
       tLABranchGroupDB.setEndDate(mEndDate[1]);
       tLABranchGroupDB.setEndFlag("Y");
       tLABranchGroupDB.setModifyDate(currentDate);
       tLABranchGroupDB.setModifyTime(currentTime);
       tLABranchGroupDB.setOperator(mGlobalInput.Operator);
       mstopLABranchGroupSet.add(tLABranchGroupDB.getSchema());
       return true;
   }

	/**
	 * 分解从其他类获得的数据
	 * @return
	 */
	private boolean prepareOutputData() {
		mInputData = new VData();
//		mMap = new MMap();
		if (NewGroupDB != null&&NewGroupDB.getAgentGroup()!=null&&!NewGroupDB.getAgentGroup().equals("")) {
			String groupManName = "";
        	groupManName=this.getbranchManagerName(mLAAssessAccessorySchema.getAgentCode());
        	
        	NewGroupDB.setBranchManagerName(groupManName);
        	
			mMap.put(NewGroupDB, "UPDATE");
		}
		if (mLATreeSet.size() > 0) {
			mMap.put(mLATreeSet, "UPDATE");
		}
		if (mLATreeBSet.size() > 0) {
			mMap.put(mLATreeBSet, "INSERT");
		}
		if (mLAAgentSet.size() > 0) {
			mMap.put(mLAAgentSet, "UPDATE");
		}
		if (mLAAgentBSet.size() > 0) {
			mMap.put(mLAAgentBSet, "INSERT");
		}
		if (mLACommisionSet.size() > 0) {
			mMap.put(mLACommisionSet, "UPDATE");
		}
		if (mLABranchGroupSet.size() > 0) {
			mMap.put(mLABranchGroupSet, "DELETE&INSERT");
		}
		if (mLABranchGroupBSet.size() > 0) {
			mMap.put(mLABranchGroupBSet, "INSERT");
		}
		if(mstopLABranchGroupSet.size()>0)
		{
			mMap.put(mstopLABranchGroupSet, "UPDATE");
		}
		if (mLARearRelationSetDelete.size() > 0) {
			mMap.put(mLARearRelationSetDelete, "DELETE");
		}
		if (mLARearRelationSetUpdate.size() > 0) {
			mMap.put(mLARearRelationSetUpdate, "UPDATE");
		}
		if (mLARearRelationSetInsert.size() > 0) {
			mMap.put(mLARearRelationSetInsert, "INSERT");
		}
		if(mLARearRelationBSet.size()>0)
		{
			mMap.put(mLARearRelationBSet, "INSERT");
		}
		if(mLARecomRelationSet.size()>0)
		{
			mMap.put(mLARecomRelationSet, "UPDATE");
		}
		if(mLARecomRelationBSet.size()>0)
		{
			mMap.put(mLARecomRelationBSet, "INSERT");
		}
		mInputData.add(mMap);
		return true;
	}

	   public boolean changeBranchManager(String cAgentGroup, String cNewBranchManager)
	    {
	        LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
	        tLABranchGroupDB.setAgentGroup(cAgentGroup);
	        if(!tLABranchGroupDB.getInfo())
	        {
	            CError.buildErr("查询机构"+cAgentGroup+"的信息失败！","changeBranchManager");
	            return false;
	        }
	        if (!backupBranchGroup(tLABranchGroupDB.getSchema()))
	        {
	            CError.buildErr("备份人员的原机构的信息出错！",
	                    "changeBranchManager");
	            return false;
	        }
	        tLABranchGroupDB.setBranchManager(cNewBranchManager);
	        tLABranchGroupDB.setBranchManagerName(getManageName(cNewBranchManager));
	        mLABranchGroupSet.add(tLABranchGroupDB.getSchema());
	        return true;
	    }
	 /*
	  *
	  */
	   private String getManageName(String cAgentCode)
	   {
		  String Name = "";
		  String nameSQL = "select name from laagent where agentcode='"+cAgentCode+"'";
		  ExeSQL tExeSQL = new ExeSQL();
		  Name = tExeSQL.getOneValue(nameSQL);
		  if(Name==null||!Name.equals(""))
		  {
			  return null;
		  }
		  else
		  {
		   return Name;
		  }
	   }
	    /*
	     改变一个部下的所有的组长的上级代理人
	     参数：cOldUpAgent-部的内部编码,cNewUpAgent-新的上级代理人的编码
	     */
	    public boolean changeUpAgentSet(String cOldUpAgent,String cNewUpAgent,String cDownAgentSeries)
	    {
	        LATreeSet tLATreeSet=new LATreeSet();
	        LATreeDB tLATreeDB=new LATreeDB();
	        LATreeSchema tLATreeSchema;
	        String strSQL="select * from latree a,laagent b where a.agentcode=b.agentcode and upagent='"
	                      +cOldUpAgent+
	                    //  "' and AgentSeries='"+cDownAgentSeries+
	                      "' and b.agentstate<='04'  and a.agentcode<>'"+cOldUpAgent+"'";
	        tLATreeSet=tLATreeDB.executeQuery(strSQL);
	        if(tLATreeDB.mErrors.needDealError())
	        {
	            CError.buildErr(tLATreeDB.mErrors.getFirstError(),"ChangeUpAgentSet");
	            return false;
	        }
	        for(int i=1;i<=tLATreeSet.size();i++)
	        {
	            tLATreeSchema=new LATreeSchema();
	            tLATreeSchema=tLATreeSet.get(i);
	            if (!backupAgentTree(tLATreeSchema.getSchema()))
	            {
	                CError.buildErr("备份代理人行政信息失败！",
	                        "ChangeUpAgentSet");
	                return false;
	            }
	            tLATreeSchema.setUpAgent(cNewUpAgent);
	            mLATreeSet.add(tLATreeSchema);
	        }

	        return true;
	    }

	   /**
	 * 处理无法回归的处或者区
	 *
	 * @return
	 */
	private boolean Proceedfive() {
		if (mLAAssessAccessorySchema.getAgentSeries().equals("1")) { // 无法回归的是一个处，那么只要改变它的职级就可以了
			// 改变所属机构
			if (!changeAgentGroup(mLAAssessAccessorySchema.getAgentCode(),
					mLAAssessAccessorySchema.getAgentGroup(), "")) {
				CError.buildErr(this, "改变人员行政信息失败！", "AscriptControlBLF",
						"Proceedfive");
				return false;
			}
			if (!changeAgentGrade(mLAAssessAccessorySchema.getAgentCode(),
					mLAAssessAccessorySchema.getAgentGrade1())) {
				CError.buildErr(this, "改变人员职级失败！");
				return false;
			}
			// 原来管理的机构主管空
			if (!changeBranchManager(
					mLAAssessAccessorySchema.getAgentGroup(), "")) {
				CError.buildErr(this,
						"将机构" + mLAAssessAccessorySchema.getAgentGroup()
								+ "的主管置空失败！", "AscriptControlBLF",
						"Proceedfive");
				return false;
			}
			// 改变组员的上级代理人
			if (!changeUpAgentSet(mLAAssessAccessorySchema
					.getAgentCode(), "", mLAAssessAccessorySchema
					.getAgentSeries1())) {
				CError.buildErr(this, "改变组员的上级代理人失败！", "AscriptControlBLF",
						"Proceedfive");
				return false;
			}
			LATreeDB selfLATreeDB = new LATreeDB();
			selfLATreeDB.setAgentCode(mLAAssessAccessorySchema.getAgentCode());
			LATreeSchema selfLATreeSchema = new LATreeSchema();
			selfLATreeSchema.setSchema(selfLATreeDB.getSchema());
			// 切断关系,向上的关系断开,向下的关系不一定断开
			if (!BreakAgentRear(selfLATreeSchema)) {
				CError.buildErr(this, "断开血缘关系失败！");
				return false;
			}

			return true;
		}

		// 无法回归的是一个区
		// 改变职级

        String zxBranch = "select agentgroup from labranchgroup where upbranch='"+mLAAssessAccessorySchema.getAgentGroup()+"' and upbranchattr='1'";
		ExeSQL zxExeSQL = new ExeSQL();
	    String zxAgentGroup = zxExeSQL.getOneValue(zxBranch);
	    if(zxAgentGroup==null||zxAgentGroup.equals(""))
	    {
	    	CError.buildErr(this, "主管直辖机构维护出错！");
	    	return false;
	    }

		if (!changeAgentGroup(mLAAssessAccessorySchema.getAgentCode(),
				zxAgentGroup, "")) {
			CError.buildErr(this, "改变人员行政信息失败！", "AscriptControlBLF",
					"Proceedfive");
			return false;
		}
		if (!changeAgentGrade(mLAAssessAccessorySchema.getAgentCode(),
				mLAAssessAccessorySchema.getAgentGrade1())) {
			CError.buildErr(this, "改变人员职级失败！");
			return false;
		}
		// 修改业绩，降级为处以后需要修改团队信息（只修改本人）
		LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
		LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
		LATreeSchema tLATreeSchema = new LATreeSchema();
		LATreeDB tLATreeDB = new LATreeDB();
		tLATreeDB.setAgentCode(this.mLAAssessAccessorySchema.getAgentCode());
		tLATreeDB.getInfo();
		tLATreeSchema.setSchema(tLATreeDB.getSchema());
		tLABranchGroupDB.setAgentGroup(tLATreeSchema.getBranchCode());
		tLABranchGroupDB.getInfo();
		tLABranchGroupSchema = tLABranchGroupDB.getSchema();
		if (!setPertoGroup(mLAAssessAccessorySchema.getAgentCode(),
				tLABranchGroupSchema)) {
			CError.buildErr(this, "修改代理人"
					+ mLAAssessAccessorySchema.getAgentCode() + "的业绩信息失败！");
			return false;
		}
		// 归属保单和险种表,归属应收表(只需要归属本人)
		if (!setContToGroupNew(
				mLAAssessAccessorySchema.getAgentCode(), tLABranchGroupSchema)) {
			CError.buildErr(this, "修改代理人"
					+ mLAAssessAccessorySchema.getAgentCode() + "的业绩信息失败！");
			return false;
		}
		// 原来管理的机构主管空
		if (!changeBranchManager(mLAAssessAccessorySchema
				.getAgentGroup(), "")) {
			CError.buildErr(this, "将机构"
					+ mLAAssessAccessorySchema.getAgentGroup() + "的主管置空失败！");
			return false;
		}

		// 改变组长的上级代理人
		if (!changeUpAgentSet(mLAAssessAccessorySchema.getAgentCode(), "",
						mLAAssessAccessorySchema.getAgentSeries1())) {
			CError.buildErr(this, "改变主任的上级代理人失败！");
			return false;
		}
		// 建立一个空的直辖组，没有组长(保证每个区都有直辖处 )
		System.out.println("tLATreeDB.getAgentGroup()"+tLATreeDB.getAgentGroup());
//		if (!createGroup("", tLATreeDB.getBranchCode(), "1")) {
//			CError.buildErr(this, "建立新的直辖组失败！");
//			return false;
//		}
		NewGroupDB.setSchema(mLABranchGroupSchema);
		if (NewGroupDB == null || NewGroupDB.getAgentGroup() == null
				|| NewGroupDB.getAgentGroup().equals("")) {
			CError.buildErr(this, "取得新机构失败！");
			return false;
		}
		// 改变原来直辖组的直辖关系
		if (!changeUpBranchAttr(tLATreeDB.getBranchCode(), "0")) {
			CError.buildErr(this, "改变机构直辖属性失败！");
			return false;
		}
		// 切断关系
		LATreeSchema selfLATreeSchema = new LATreeSchema();
		selfLATreeSchema.setSchema(tLATreeSchema);
		if (!BreakAgentRear(selfLATreeSchema)) {
			CError.buildErr(this, "断开血缘关系失败！", "AscriptControlBLF",
					"Proceedfive");
			return false;
		}
		// 修改推荐关系的AgentGroup,只修改本人,其他人没有变化,(本人是区经理AgentGroup会变化)
		LATreeSet tRearLATreeSet = new LATreeSet();
		tRearLATreeSet.add(selfLATreeSchema);
		if (!changeRecomRelation(tRearLATreeSet,
				selfLATreeSchema.getAgentGroup())) {
			CError.buildErr(this, "修改推荐关系失败！");
			return false;
		}

		return true;
	}
    /*
    改变一个组与其所在部的直辖关系
    参数：cAgentGroup-组的内部编码,cUpBranchAttr-直辖关系
    */
   public boolean changeUpBranchAttr(String cAgentGroup,String cUpBranchAttr)
   {
       LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
       tLABranchGroupDB.setAgentGroup(cAgentGroup);
       if(!tLABranchGroupDB.getInfo())
       {
           CError.buildErr("查询机构"+cAgentGroup+"的信息失败！","changeUpBranchAtrrr");
           return false;
       }
       if(!backupBranchGroup(tLABranchGroupDB.getSchema()))
       {
           CError.buildErr("备份人员的原机构的信息出错！","changeUpBranchAtrrr");
           return false;
       }
       tLABranchGroupDB.setUpBranchAttr(cUpBranchAttr);
       mLABranchGroupSet.add(tLABranchGroupDB.getSchema());
       return true;
   }
	/**
	 *
	 * 参数:cLATreeSchema-要备份的代理人行政信息
	 */
	private boolean backupAgentTree(LATreeSchema cLATreeSchema) {
		LATreeBSchema tLATreeBSchema = new LATreeBSchema();
		Reflections tReflections = new Reflections();
		tReflections.transFields(tLATreeBSchema, cLATreeSchema);
		tLATreeBSchema.setOperator2(cLATreeSchema.getOperator());
		tLATreeBSchema.setMakeDate2(cLATreeSchema.getMakeDate());
		tLATreeBSchema.setModifyDate2(cLATreeSchema.getModifyDate());
		tLATreeBSchema.setMakeTime2(cLATreeSchema.getMakeTime());
		tLATreeBSchema.setModifyTime2(cLATreeSchema.getModifyTime());

		tLATreeBSchema.setEdorNO(mEdorNo);
		tLATreeBSchema.setRemoveType("31");// 人员调岗结果
		tLATreeBSchema.setMakeDate(currentDate);
		tLATreeBSchema.setMakeTime(currentTime);
		tLATreeBSchema.setModifyDate(currentDate);
		tLATreeBSchema.setModifyTime(currentTime);
		tLATreeBSchema.setOperator(mGlobalInput.Operator);
		tLATreeBSchema.setIndexCalNo(mIndexCalNo);
		mLATreeBSet.add(tLATreeBSchema);
		return true;
	}

	/**
	 * 备份代理人的基本信息 参数:cLAAgentSchema-要备份的代理人的基本信息
	 */
	private boolean backupAgent(LAAgentSchema cLAAgentSchema) {
		LAAgentSchema tLAAgentSchema = new LAAgentSchema();
		LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
		Reflections tReflections = new Reflections();
		tLAAgentSchema = cLAAgentSchema;
		System.out.println(cLAAgentSchema.getAgentCode());
		tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
		System.out.println(tLAAgentBSchema.getAgentCode());
		tLAAgentBSchema.setEdorNo(mEdorNo);
		tLAAgentBSchema.setEdorType("31");// 人员调岗
		tLAAgentBSchema.setMakeDate(currentDate);
		tLAAgentBSchema.setMakeTime(currentTime);
		tLAAgentBSchema.setModifyDate(currentDate);
		tLAAgentBSchema.setModifyTime(currentTime);
		tLAAgentBSchema.setOperator(mGlobalInput.Operator);
		tLAAgentBSchema.setIndexCalNo(mIndexCalNo);
		mLAAgentBSet.add(tLAAgentBSchema);
		return true;
	}

	private String getBranchManagerName(String cAgentCode) {
		ExeSQL tExeSQL = new ExeSQL();
		SSRS nameR = new SSRS();
		nameR = tExeSQL.execSQL("select name from laagent where agentcode='"
				+ cAgentCode + "'");
		if (nameR.MaxRow > 0) {
			return nameR.GetText(1, 1);
		} else {
			return "";
		}
	}

	public boolean updateGroup(String cAgentCode, String cAgentGroup,
			String cNewSeries) {
		LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
		LABranchGroupSchema tLABranchGroupDBNew = new LABranchGroupSchema();// 保存新主管原来的上级机构
		String NewGroup = cAgentGroup;
		String NewBranchLever = "";
		String SQLstr;
		String tMaxAttr = "";
		// 说明不是部长直辖组中的人晋升 没有必要作这样的判断的 20050823 zxs
			if (cNewSeries.equals("2")) {
				// 晋升成区级经理，查原来所在的上级机构(可能是区或部)
				SQLstr = "select * from labranchgroup where EndFlag<>'Y' and agentgroup = (select upbranch from labranchgroup where Agentgroup = '"
						+ cAgentGroup + "')";
			} else { // 晋升成处级经理，查原来所在的处
				SQLstr = "select * from labranchgroup where EndFlag<>'Y' and agentgroup =  '"
						+ cAgentGroup + "'";
			}
			System.out.println(SQLstr);

			LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB
					.executeQuery(SQLstr);
			if (tLABranchGroupDB.mErrors.needDealError()) {
				CError.buildErr("查询机构" + tLABranchGroupDB.getAgentGroup()
						+ "的信息出错！", "createGroup");
				return false;
			}

			// 代理人原机构的上级机构
			tLABranchGroupDB.setSchema(tLABranchGroupSet.get(1));

		if (cNewSeries.equals("2")) {// 新系列大于1，说明是区级经理系列
			NewBranchLever = "02";
		} else if (cNewSeries.equals("1")) {// 互动branchlevel主任系列
			NewBranchLever = "51";
		}
		// 生成机构最大号
		String UpBranchAttr = tLABranchGroupDB.getBranchAttr();
		String tupBranchLevel = tLABranchGroupDB.getBranchLevel();
		System.out.println("UpBranchAttr"+UpBranchAttr);
		System.out.println("tupBranchLevel"+tupBranchLevel);
		// 如果是处晋升部，并且该处是直接在部下面（上级是部，不是区）
//		if (tupBranchLevel.equals("03") && cNewSeries.equals("2")) {
//			tMaxAttr = AgentPubFun.CreateBranchAttr(UpBranchAttr,
//					NewBranchLever, tLABranchGroupDB.getBranchType(),
//					tLABranchGroupDB.getBranchType2());
//		} else {
//			if(NewBranchLever.equals("51")){
//				tMaxAttr = AgentPubFun.CreateBranchAttr(
//						(UpBranchAttr.substring(0,UpBranchAttr.length() - 2)), 
////						修改原因是因为个险营业处编码可能为14位也可能是15位
////						(UpBranchAttr.substring(0,12)),
//						NewBranchLever,
//						tLABranchGroupDB.getBranchType(), tLABranchGroupDB
//								.getBranchType2());
//				
//			}
//			else {
//				tMaxAttr = AgentPubFun.CreateBranchAttr(
//						(UpBranchAttr.substring(0,UpBranchAttr.length() - 2)), 
//						NewBranchLever,
//						tLABranchGroupDB.getBranchType(), tLABranchGroupDB
//								.getBranchType2());	
//			}
//		}
		// 设置新机构的系列码
//		String UpBranchSeries = tLABranchGroupDB.getBranchSeries();
//		// 如果是处晋升部，并且该处是直接在部下面（上级是部，不是区）
//		if (tupBranchLevel.equals("03") && cNewSeries.equals("2")) {
//			tLABranchGroupDBNew
//					.setBranchSeries(UpBranchSeries + ":" + NewGroup);
//			tLABranchGroupDBNew.setUpBranch(tLABranchGroupDB.getAgentGroup());
//		} else {
//			tLABranchGroupDBNew.setBranchSeries(UpBranchSeries.substring(0,
//					UpBranchSeries.length() - 12)
//					+ NewGroup);
//			tLABranchGroupDBNew.setUpBranch(tLABranchGroupDB.getUpBranch());
//		}

		tLABranchGroupDBNew.setAgentGroup(NewGroup);
		tLABranchGroupDBNew.setSchema(tLABranchGroupSet.get(1));
		tLABranchGroupDBNew.setManageCom(tLABranchGroupDB.getManageCom());
//		tLABranchGroupDBNew.setBranchAttr(tMaxAttr);
		tLABranchGroupDBNew.setUpBranchAttr("0");// 新建立的机构与上级机构永远不可能是直辖关系
		tLABranchGroupDBNew.setBranchType(tLABranchGroupDB.getBranchType());
		tLABranchGroupDBNew.setBranchType2(tLABranchGroupDB.getBranchType2());
		tLABranchGroupDBNew.setEndFlag("N");
		tLABranchGroupDBNew.setBranchManager(cAgentCode);
//		tLABranchGroupDBNew.setFoundDate(this.mAscriptDate);
		String name = "";
		if (cAgentCode != null && !cAgentCode.equals("")) {
//			tLABranchGroupDBNew.setName("晋升生成");
			name = getBranchManagerName(tLABranchGroupDBNew.getBranchManager());
			if (name == null || name.equals("")) {
				CError.buildErr("查询机构管理员的姓名出错！", "createGroup");
				return false;
			}
		} else {// 新建立的机构，不设主管
			tLABranchGroupDBNew.setName("补充直辖组");
			tLABranchGroupDBNew.setUpBranchAttr("1");
		}
		//备份原来labranchgroup 表
		Reflections tReflections = new Reflections();
		LABranchGroupBSchema tLABranchGroupB = new LABranchGroupBSchema();
		tReflections.transFields(tLABranchGroupB, tLABranchGroupDBNew);
		tLABranchGroupB.setEdorNo(mEdorNo);
		tLABranchGroupB.setEdorType("31");// 调岗
		tLABranchGroupB.setOperator2(tLABranchGroupDBNew.getOperator());
		tLABranchGroupB.setMakeDate2(tLABranchGroupDBNew.getMakeDate());
		tLABranchGroupB.setModifyDate2(tLABranchGroupDBNew
				.getModifyDate());
		tLABranchGroupB.setMakeTime2(tLABranchGroupDBNew.getMakeTime());
		tLABranchGroupB.setModifyTime2(tLABranchGroupDBNew
				.getModifyTime());
		tLABranchGroupB.setBranchManager("");
		tLABranchGroupB.setMakeDate(currentDate);
		tLABranchGroupB.setMakeTime(currentTime);
		tLABranchGroupB.setModifyDate(currentDate);
		tLABranchGroupB.setModifyTime(currentTime);
		tLABranchGroupB.setOperator(mGlobalInput.Operator);
		tLABranchGroupB.setIndexCalNo(mIndexCalNo);
        
		tLABranchGroupDBNew.setBranchManagerName(name);
		tLABranchGroupDBNew.setBranchLevel(NewBranchLever);
//		tLABranchGroupDBNew.setMakeDate(this.mAscriptDate);
//		tLABranchGroupDBNew.setMakeTime(currentTime);
		tLABranchGroupDBNew.setModifyDate(currentDate);
		tLABranchGroupDBNew.setModifyTime(currentTime);
		tLABranchGroupDBNew.setOperator(mGlobalInput.Operator);
		mLABranchGroupSchema.setSchema(tLABranchGroupDBNew);
		mLABranchGroupBSet.add(tLABranchGroupB);
		return true;
	}

	// 修改个人的业绩
	public boolean setPertoGroup(String cAgentCode,
			LABranchGroupSchema cLABranchGroupSchema) {
		String tAgentGroup = cLABranchGroupSchema.getAgentGroup();
		String tBranchCode = cLABranchGroupSchema.getAgentGroup();
		String tBranchAttr = cLABranchGroupSchema.getBranchAttr();
		String tBranchSeries = cLABranchGroupSchema.getBranchSeries();

		String tSql = "select * from lacommision where agentcode='"
				+ cAgentCode + "' and (caldate>='" + this.mAscriptDate
				+ "' or caldate is null)";
		LACommisionSet tLACommisionSet = new LACommisionSet();
		LACommisionDB tLACommisionDB = new LACommisionDB();
		tLACommisionSet = tLACommisionDB.executeQuery(tSql);
		for (int j = 1; j <= tLACommisionSet.size(); j++) {
			LACommisionSchema tLACommisionSchema = new LACommisionSchema();
			tLACommisionSchema = tLACommisionSet.get(j);
			tLACommisionSchema.setAgentGroup(tAgentGroup);
			tLACommisionSchema.setBranchCode(tBranchCode);
			tLACommisionSchema.setBranchSeries(tBranchSeries);
			tLACommisionSchema.setBranchAttr(tBranchAttr);
			tLACommisionSchema.setModifyDate(currentDate);
			tLACommisionSchema.setModifyTime(currentTime);
			tLACommisionSchema.setOperator(mGlobalInput.Operator);
			this.mLACommisionSet.add(tLACommisionSchema);
		}

		return true;
		// mEdorNo=cEdorNo;
	}

	// 修改区经理的业绩（branchcode与agentgroup不同）
	public boolean setPertoGroup(String cAgentCode, String cBranchCode,
			LABranchGroupSchema cLABranchGroupSchema) {
		String tAgentGroup = cLABranchGroupSchema.getAgentGroup();
		// String tBranchCode=cLABranchGroupSchema.getAgentGroup();
		String tBranchAttr = cLABranchGroupSchema.getBranchAttr();
		String tBranchSeries = cLABranchGroupSchema.getBranchSeries();

		String tSql = "select * from lacommision where agentcode='"
				+ cAgentCode + "' and (caldate>='" + this.mAscriptDate
				+ "' or caldate is null)";
		LACommisionSet tLACommisionSet = new LACommisionSet();
		LACommisionDB tLACommisionDB = new LACommisionDB();
		tLACommisionSet = tLACommisionDB.executeQuery(tSql);
		for (int j = 1; j <= tLACommisionSet.size(); j++) {
			LACommisionSchema tLACommisionSchema = new LACommisionSchema();
			tLACommisionSchema = tLACommisionSet.get(j);
			tLACommisionSchema.setAgentGroup(tAgentGroup);
			tLACommisionSchema.setBranchCode(cBranchCode); // BranchCode与AgentGroup不一样
			tLACommisionSchema.setBranchSeries(tBranchSeries);
			tLACommisionSchema.setBranchAttr(tBranchAttr);
			tLACommisionSchema.setModifyDate(currentDate);
			tLACommisionSchema.setModifyTime(currentTime);
			tLACommisionSchema.setOperator(mGlobalInput.Operator);
			this.mLACommisionSet.add(tLACommisionSchema);
		}
		return true;
		// mEdorNo=cEdorNo; mGlobalInput=(GlobalInput)
	}

	 //修改个人的业绩
    public boolean setContToGroupNew(String cAgentCode,LABranchGroupSchema cLABranchGroupSchema)
    {
        String tAgentGroup=cLABranchGroupSchema.getAgentGroup();
        String tSql = "update lccont  set  agentgroup='" + tAgentGroup+
                      "',ModifyTime = '" + currentTime +
                      "',ModifyDate = '" + currentDate +
                      "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update lbcont  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update lpcont  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update lcpol  set  agentgroup='" + tAgentGroup+
               "',ModifyTime = '" + currentTime +
               "',ModifyDate = '" + currentDate +
               "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update lbpol  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update lppol  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update lcgrpcont  set  agentgroup='" + tAgentGroup+
           "',ModifyTime = '" + currentTime +
           "',ModifyDate = '" + currentDate +
           "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update lbgrpcont  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update lpgrpcont  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update lcgrppol  set  agentgroup='" + tAgentGroup+
           "',ModifyTime = '" + currentTime +
           "',ModifyDate = '" + currentDate +
           "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update lbgrppol  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update lpgrppol  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");



        tSql = "update ljspayperson  set  agentgroup='" + tAgentGroup+
               "',ModifyTime = '" + currentTime +
               "',ModifyDate = '" + currentDate +
               "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update ljspay  set  agentgroup='" + tAgentGroup+
               "',ModifyTime = '" + currentTime +
               "',ModifyDate = '" + currentDate +
               "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update ljspaygrp  set  agentgroup='" + tAgentGroup+
             "',ModifyTime = '" + currentTime +
             "',ModifyDate = '" + currentDate +
             "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        
        tSql = "update ljsgetendorse  set  agentgroup='" + tAgentGroup+
              "',ModifyTime = '" + currentTime +
              "',ModifyDate = '" + currentDate +
              "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");

       return true ;
       // mEdorNo=cEdorNo;
    }
	/**
	 * 改变业务员晋升主任后，带走的推荐人的业绩信息
	 */
	private boolean changeOtherCommision(String cAgentCode,
			LABranchGroupSchema cGroupSechma) {
		LATreeDB tLATreeDB = new LATreeDB();
		tLATreeDB.setAgentCode(cAgentCode);
		tLATreeDB.getInfo();
		String OthersTreeSQL = "select * from latree where agentcode in "
				+ "(select agentcode  from larecomrelation where recomagentcode='"
				+ cAgentCode + "' " + "and agentgroup='"
				+ tLATreeDB.getAgentGroup()
				+ "' and enddate is null ) order by agentcode";
		LATreeSet tLATreeSet = new LATreeSet();
		LATreeDB oLATreeDB = new LATreeDB();
		if (oLATreeDB.mErrors.needDealError()) {
			CError.buildErr(this, "被推荐人查询失败！");
			return false;
		}
		tLATreeSet.add(oLATreeDB.executeQuery(OthersTreeSQL));
		for (int i = 1; i <= tLATreeSet.size(); i++) {
			String commSQL = "select * from lacommision where agentcode='"
					+ tLATreeSet.get(i).getAgentCode() + "' " +
							"and (caldate>='"+this.mAscriptDate+"' or caldate is null)";
			LACommisionDB tLACommisionDB = new LACommisionDB();
			LACommisionSet tLACommisionSet = new LACommisionSet();
			if (tLACommisionDB.mErrors.needDealError()) {
				CError.buildErr(this, "业绩查询出错！");
				return false;
			}
			tLACommisionSet.add(tLACommisionDB.executeQuery(commSQL));
			for (int j = 1; j <= tLACommisionSet.size(); j++) {
				LACommisionSchema upLACommisionSchema = new LACommisionSchema();
				upLACommisionSchema = tLACommisionSet.get(j);
				upLACommisionSchema.setAgentGroup(cGroupSechma.getAgentGroup());
				upLACommisionSchema.setBranchCode(cGroupSechma.getAgentGroup());
				upLACommisionSchema.setBranchAttr(cGroupSechma.getBranchAttr());
				upLACommisionSchema.setModifyDate(currentDate);
				upLACommisionSchema.setModifyTime(currentTime);
				upLACommisionSchema.setBranchSeries(cGroupSechma
						.getBranchSeries());
				upLACommisionSchema.setOperator(this.mGlobalInput.Operator);
				mLACommisionSet.add(upLACommisionSchema);
			}
		}
		return true;
	}

	/**
	 * 改变连动人员的行政信息 cAgentcode-代理人编码 cNewGroup-代理人目的机构编码
	 */
	public boolean changeOtherGroup(String cAgentCode, String cNewGroup) {
		System.out.println("cAgentCode" + cAgentCode);
		System.out.println("cNewGroup" + cNewGroup);
		String tOldSeries = mLAAssessAccessorySchema.getAgentSeries();
		String tNewSeries = mLAAssessAccessorySchema.getAgentSeries1();
		LATreeSet otherLATreeSet = new LATreeSet();
		LAAgentSet otherLAAgentSet = new LAAgentSet();
		LATreeDB selfLATreeDB = new LATreeDB();
		LAAgentDB otherLAAgentDB = new LAAgentDB();
		selfLATreeDB.setAgentCode(cAgentCode);
		if (!selfLATreeDB.getInfo()) {
			CError.buildErr(this, "业务员的行政信息查询失败！");
			return false;
		}
		System.out.println("tLATreeDB.getAgentGroup()"
				+ selfLATreeDB.getAgentGroup());
		// 晋升
		if (tOldSeries.compareTo(tNewSeries) <= 0) {
			String OthersTreeSQL = "";
			String OthersAgentSQL = "";
			if (tNewSeries.equals("1")
					|| (tOldSeries.equals("1") && mLAAssessAccessorySchema
							.getAgentGrade().equals("B01")))// 由业务员晋升为主任
			{
				// 查询所在处的直接推荐人以及间接推荐人，一并归属到自己的zhi管处内
				OthersTreeSQL = "select * from latree where agentcode in"
						+ "(select agentcode  from larecomrelation where recomagentcode='"
						+ cAgentCode + "' " + "and agentgroup='"
						+ selfLATreeDB.getAgentGroup()
						+ "' and enddate is null ) order by agentcode";
				OthersAgentSQL = "select * from laagent where agentcode in"
						+ "(select agentcode from larecomrelation where recomagentcode='"
						+ cAgentCode + "' " + "and agentgroup='"
						+ selfLATreeDB.getAgentGroup()
						+ "' and enddate is null )order by agentcode";
			} else if (tNewSeries.equals("2"))// 处晋升到区
			{
				// 查询直接，间接培养关系，将其一并归属到区内，只需要更改被培养人的上级即可
				OthersTreeSQL = "select * from latree where agentcode in "
						+ "(select agentcode from larearrelation where rearagentcode = '"
						+ cAgentCode + "' and rearlevel='01')";
				OthersAgentSQL = "select * from laagent where agentcode in "
						+ "(select agentcode from larearrelation where rearagentcode = '"
						+ cAgentCode + "' and rearlevel='01')";
			}
			System.out.println(OthersTreeSQL);
			System.out.println(OthersAgentSQL);
			LAAgentDB oLAAgentDB = new LAAgentDB();
			LATreeDB oLATreeDB = new LATreeDB();
			otherLATreeSet.add(oLATreeDB.executeQuery(OthersTreeSQL));
			otherLAAgentSet.add(oLAAgentDB.executeQuery(OthersAgentSQL));
			if (oLATreeDB.mErrors.needDealError()) {
				CError.buildErr(this, "查询人员行政信息时出错！");
				return false;
			}
			if (oLAAgentDB.mErrors.needDealError()) {
				CError.buildErr(this, "查询人员基本信息出错！");
				return false;
			}
			if (tNewSeries.equals("1")
					|| (tOldSeries.equals("1") && mLAAssessAccessorySchema
							.getAgentGrade().equals("B01")))// 由业务员晋升为主任
			{
				// 去掉不能带走的推荐人
				for (int i = 1; i <= otherLATreeSet.size(); i++) {
					// 循环将离职的、已经是主管的除去,并做相应的归属
					LATreeSchema elseLATreeSchema = new LATreeSchema();
					LAAgentSchema elseLAAgentSchema = new LAAgentSchema();
					elseLATreeSchema = otherLATreeSet.get(i);
					elseLAAgentSchema = otherLAAgentSet.get(i);
					if (elseLAAgentSchema.getAgentState().compareTo("06") < 0
							&& !elseLATreeSchema.getAgentGrade().substring(0,1)
									.equals("V")) {
						// 备份代理人原有信息
						backupAgentTree(elseLATreeSchema);
						backupAgent(elseLAAgentSchema);
						elseLATreeSchema.setUpAgent(cAgentCode);
						elseLATreeSchema.setAgentGroup(cNewGroup);
						elseLATreeSchema.setBranchCode(cNewGroup);
						elseLATreeSchema.setModifyDate(this.mAscriptDate);
						elseLATreeSchema.setModifyTime(currentTime);
						elseLATreeSchema
								.setOperator(this.mGlobalInput.Operator);
						// 基本信息
						elseLAAgentSchema.setAgentGroup(cNewGroup);
						elseLAAgentSchema.setBranchCode(cNewGroup);
						elseLAAgentSchema.setModifyDate(mAscriptDate);
						elseLAAgentSchema.setModifyTime(currentTime);
						elseLAAgentSchema
								.setOperator(this.mGlobalInput.Operator);
						elseLATreeSet.add(elseLATreeSchema);
						mLATreeSet.add(elseLATreeSchema);
						mLAAgentSet.add(elseLAAgentSchema);

					}

				}
			} else {
				// 去掉不是本区的,根据所在机构的上级机构是否一致进行判断,育成人的处理
				for (int i = 1; i <= otherLATreeSet.size(); i++) {
					String upbranchSQL = "select upbranch from labranchgroup where agentgroup='"
							+ selfLATreeDB.getAgentGroup()
							+ "' and branchtype='5' and branchtype2='01'";
					String upbranchSQL1 = "select upbranch from labranchgroup where agentgroup='"
							+ otherLATreeSet.get(i).getAgentGroup()
							+ "' and branchtype='5' and branchtype2='01'";
					ExeSQL tExeSQL = new ExeSQL();
					String upbranch = tExeSQL.getOneValue(upbranchSQL);
					String upbranch1 = tExeSQL.getOneValue(upbranchSQL1);
					if (upbranch.compareTo(upbranch1) == 0)// 调整之前是在一个营业区内的才能带走
					{
						// 只需要改变育成处的行政信息以及代理人的直辖处人员信息,其中直辖处人员的agentgroup
						// 以及branchcode 不用变
						// 只需要修改上级代理人即可，归属在机构归属的时候处理
						LATreeSchema tLATreeSchema = new LATreeSchema();
						// LAAgentSchema tLAAgentSchema = new LAAgentSchema();
						tLATreeSchema = otherLATreeSet.get(i);
						// tLAAgentSchema = otherLAAgentSet.get(i);
						backupAgentTree(tLATreeSchema);
						// backupAgent(tLAAgentSchema);
						tLATreeSchema.setUpAgent(cAgentCode);
						tLATreeSchema.setModifyDate(this.mAscriptDate);
						tLATreeSchema.setModifyTime(currentTime);
						elseLATreeSet.add(tLATreeSchema);
						mLATreeSet.add(tLATreeSchema);
						// mLAAgentSet.add(tLAAgentSchema);//被育成人的agentgroup以及branchcode均不用发生变化
					}

				}
			}
		} else// 降级
		{
			// 获取回归区的经理
			String upmanagerSQL = "select branchmanager from labranchgroup where agentgroup = '"
					+ cNewGroup + "'";
			ExeSQL upExeSQL = new ExeSQL();
			String UpManager = upExeSQL.getOneValue(upmanagerSQL);
			if (tNewSeries.compareTo("1") == 0)// 降级为处经理，UpManager为一个区经理
			{
				// 将区下面的处经理上级都置为回归区经理,除了直辖组（前面已经处理）
				String SQL = "select * from labranchgroup where upbranch='"
						+ selfLATreeDB.getAgentGroup()
						+ "' and endflag<>'Y' and upbranchattr<>'1'";
				LABranchGroupDB upLABranchGroupDB = new LABranchGroupDB();
				LABranchGroupSet upLABranchGroupSet = new LABranchGroupSet();
				upLABranchGroupSet.add(upLABranchGroupDB.executeQuery(SQL));
				for (int i = 1; i <= upLABranchGroupSet.size(); i++) {
					// 有主管的将主管上级置为回归区经理，无主管的跳过
					LATreeDB downLATreeDB = new LATreeDB();
					LATreeSchema downLATreeSchema = new LATreeSchema();
					String downManager = upLABranchGroupSet.get(i)
							.getBranchManager();
					if (downManager != null && !downManager.equals("")) {

						downLATreeDB.setAgentCode(downManager);
						if (!downLATreeDB.getInfo()) {
							CError.buildErr(this, "下级机构主管行政信息查询失败！");
							return false;
						}
						downLATreeSchema.setSchema(downLATreeDB.getSchema());
						downLATreeSchema.setUpAgent(UpManager);
						downLATreeSchema.setModifyDate(this.mAscriptDate);
						downLATreeSchema.setModifyTime(currentTime);
						downLATreeSchema
								.setOperator(this.mGlobalInput.Operator);
						mLATreeSet.add(downLATreeSchema);
					}

				}
			} else if (tNewSeries.compareTo("0") == 0)// 降为业务员
			{
				// 直辖团队人员的上级均置空
				String downSQL = "select * from latree where agentgroup='"
						+ selfLATreeDB.getAgentGroup()
						+ "' and "
						+ "agentcode in (select agentcode from laagent where agentstate <='05') order by agentcode";// 离职未确认人员也要做相应的调整
				String downAgentSQL = "select * from laagent where agentgroup='"
						+ selfLATreeDB.getAgentGroup()
						+ "' and agentstate<='05' order by agentcode";

				LATreeDB downLATreeDB = new LATreeDB();
				LATreeSet downLATreeSet = new LATreeSet();
				downLATreeSet.add(downLATreeDB.executeQuery(downSQL));
				LAAgentDB downLAAgentDB = new LAAgentDB();
				LAAgentSet downLAAgentSet = new LAAgentSet();
				downLAAgentSet.add(downLAAgentDB.executeQuery(downAgentSQL));
				for (int i = 1; i <= downLATreeSet.size(); i++) {
					LATreeSchema downLATreeSchema = new LATreeSchema();
					LAAgentSchema downLAAgentSchema = new LAAgentSchema();
					downLAAgentSchema = downLAAgentSet.get(i);
					downLATreeSchema = downLATreeSet.get(i);
					downLATreeSchema.setUpAgent(UpManager);
					downLATreeSchema.setAgentGroup(cNewGroup);
					downLATreeSchema.setBranchCode(cNewGroup);
					downLATreeSchema.setModifyDate(this.mAscriptDate);
					downLATreeSchema.setModifyTime(currentTime);
					downLATreeSchema.setOperator(this.mGlobalInput.Operator);
					// 基本信息
					downLAAgentSchema.setAgentGroup(cNewGroup);
					downLAAgentSchema.setBranchCode(cNewGroup);
					downLAAgentSchema.setModifyDate(this.mAscriptDate);
					downLAAgentSchema.setModifyTime(currentTime);
					downLAAgentSchema.setOperator(this.mGlobalInput.Operator);
					mLATreeSet.add(downLATreeSchema);
					mLATreeSet.add(downLAAgentSchema);
				}

			}
		}
		return true;
	}

	/**
	 * 改变代理人的机构与上级,同时备份Agent 参数:cAgentcode-代理人编码
	 * cNewGroup-代理人目的机构编码
	 * cUpBranch-新机构的上级机构,降级的时候传入的是BRANCHMANAGER
	 */
	public boolean changeAgentGroup(String cAgentCode,
			String cNewGroup, String cUpBranch) {

		System.out.println("cNewGroup" + cNewGroup);
		System.out.println("cUpBranch" + cUpBranch);
		LATreeSchema tLATreeSchema = new LATreeSchema();
		LATreeDB tLATreeDB = new LATreeDB();
		LAAgentDB tLAAgentDB = new LAAgentDB();

		tLAAgentDB.setAgentCode(cAgentCode);// 取得代理人的基本信息
		tLATreeDB.setAgentCode(cAgentCode);
		tLATreeDB.getInfo();
		tLATreeSchema.setSchema(tLATreeDB.getSchema());// 取得代理人的行政信息
		if(!tLATreeDB.getInfo())
		{
			CError.buildErr("取代理人行政信息失败！", "AscriptAgentInfoOperateBL");
			return false;
		}
		if (!backupAgentTree(tLATreeDB.getSchema())) {
			CError.buildErr("备份代理人行政信息失败！", "AscriptAgentInfoOperateBL");
			return false;
		}
		if (!tLAAgentDB.getInfo()) {
			CError.buildErr("取代理人基本信息失败！", "AscriptAgentInfoOperateBL");
			return false;
		}
		if (!backupAgent(tLAAgentDB.getSchema())) {
			CError.buildErr("备份代理人基本信息失败！", "AscriptAgentInfoOperateBL");
			return false;
		}
		// 此函数主要是处理跨系列升降级的情况
		if (mLAAssessAccessorySchema.getAgentSeries().compareTo(
				mLAAssessAccessorySchema.getAgentSeries1()) < 0||
				mLAAssessAccessorySchema.getAgentGrade().equals("B01")&&
				mLAAssessAccessorySchema.getAgentSeries1().equals("1")) {
			LABranchGroupDB upLABranchGroupDB = new LABranchGroupDB();
			upLABranchGroupDB.setAgentGroup(cUpBranch);// 上级机构信息
			upLABranchGroupDB.getInfo();// 取得上级机构的信息
			tLATreeSchema.setAgentGroup(cNewGroup);
			tLAAgentDB.setAgentGroup(cNewGroup);
			// 上级机构的MANAGER即为晋升人员的UPAGENT
			if (upLABranchGroupDB.getBranchManager() == null
					|| upLABranchGroupDB.getBranchManager().equals("")) {
				tLATreeSchema.setUpAgent("");
			} else {
				tLATreeSchema.setUpAgent(upLABranchGroupDB.getBranchManager());
			}

			if (mLAAssessAccessorySchema.getAgentSeries1().equals("1")) {
				tLATreeSchema.setBranchCode(cNewGroup);
				tLAAgentDB.setBranchCode(cNewGroup);
			}
			if (mLAAssessAccessorySchema.getAgentSeries1().equals("2")) {
				// branchcode置他所在的直辖组编码
				tLATreeSchema.setBranchCode(tLATreeSchema.getBranchCode());
				tLAAgentDB.setBranchCode(tLATreeSchema.getBranchCode());
			}

		} else// 降级
		{
			//区经理降级行政信息中AGENTGROUp置直辖处的编码
			System.out.println(mLAAssessAccessorySchema.getAgentSeries());
			if(mLAAssessAccessorySchema.getAgentSeries1().equals("1"))
			{
				if(!mLAAssessAccessorySchema.getAgentGrade1().equals("B01"))
				{
					String zxBranch = "select agentgroup from labranchgroup where upbranch='"+tLATreeDB.getAgentGroup()+"' and upbranchattr='1'";
					ExeSQL zxExeSQL = new ExeSQL();
				    String zxAgentGroup = zxExeSQL.getOneValue(zxBranch);
				    if(zxAgentGroup==null||zxAgentGroup.equals(""))
				    {
				    	CError.buildErr(this, "主管直辖机构维护出错！");
				    	return false;

				    }
				    tLATreeSchema.setAgentGroup(zxAgentGroup);
				    tLAAgentDB.setAgentGroup(zxAgentGroup);
				}
				else
				{
					tLATreeSchema.setAgentGroup(cNewGroup);
					tLATreeSchema.setBranchCode(cNewGroup);
					tLAAgentDB.setAgentGroup(cNewGroup);
					tLAAgentDB.setBranchCode(cNewGroup);
				}
			}
			else
			{
				tLATreeSchema.setAgentGroup(cNewGroup);
				tLATreeSchema.setBranchCode(cNewGroup);
				tLAAgentDB.setAgentGroup(cNewGroup);
				tLAAgentDB.setBranchCode(cNewGroup);
			}
			if(cUpBranch==null||cUpBranch.equals(""))
			{
				tLATreeSchema.setUpAgent("");
			}
			else
			tLATreeSchema.setUpAgent(cUpBranch);
		}
		tLATreeSchema.setModifyDate(this.mAscriptDate);
		tLATreeSchema.setModifyTime(currentTime);
		tLATreeSchema.setOperator(mGlobalInput.Operator);
		tLAAgentDB.setModifyDate(this.mAscriptDate);
		tLAAgentDB.setModifyTime(currentTime);
		tLAAgentDB.setOperator(mGlobalInput.Operator);
		mLATreeSchema.setSchema(tLATreeSchema);
		mLAAgentSchema.setSchema(tLAAgentDB.getSchema());
		mLATreeSet.add(mLATreeSchema);
		mLAAgentSet.add(mLAAgentSchema);
		return true;
	}

	/**
	 * 改变代理人的职级,同时备份Tree
	 *
	 * @param cAgentCode
	 *            String 代理人编码
	 * @param cNewGrade
	 *            String 代理人的新职级
	 * @return boolean
	 */
	public boolean changeAgentGrade(String cAgentCode, String cNewGrade) {
		// 已经备份过了
		LATreeDB tLATreeDB = new LATreeDB();
		tLATreeDB.setAgentCode(cAgentCode);
		if (!tLATreeDB.getInfo()) {
			CError.buildErr(this, "获得代理人的新政信息失败!");
			return false;
		}
		System.out.println("cNewGrade" + cNewGrade);
		mLATreeSchema.setAgentLastGrade(tLATreeDB.getAgentGrade());
		mLATreeSchema.setAgentGrade(cNewGrade);
		mLATreeSchema.setOldStartDate(tLATreeDB.getStartDate());
		mLATreeSchema.setOldEndDate(mEndDate[1]);
		mLATreeSchema.setStartDate(mAscriptDate);
		mLATreeSchema.setAgentLastSeries(tLATreeDB.getAgentSeries());
		mLATreeSchema.setAgentSeries(AgentPubFun.getAgentSeries(cNewGrade));
		mLATreeSchema.setAstartDate(mAscriptDate);
		mLATreeSchema.setModifyDate(currentDate);
		mLATreeSchema.setModifyTime(currentTime);
		mLATreeSchema.setOperator(mGlobalInput.Operator);
		mLATreeSet.add(mLATreeSchema);
		return true;
	}
    /*
    修改recomrelation 表的AgentGroup
   */
  public boolean changeRecomRelation(LATreeSet tLATreeSet,String tGroup)
  {
      if (tLATreeSet == null || tLATreeSet.size() <= 0) {
          return true;
      }
      for (int i = 1; i <= tLATreeSet.size(); i++) {
          LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();

          LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();

          tLARecomRelationDB.setAgentCode(tLATreeSet.get(i).getAgentCode());
          tLARecomRelationSet = tLARecomRelationDB.query();
          if (tLARecomRelationSet == null || tLARecomRelationSet.size() <= 0) {
              continue;
          }
          for (int k = 1; k <= tLARecomRelationSet.size(); k++) {
              LARecomRelationBSchema tLARecomRelationBSchema = new
                      LARecomRelationBSchema();
              LARecomRelationSchema tLARecomRelationSchema = new
                      LARecomRelationSchema();
              tLARecomRelationSchema=tLARecomRelationSet.get(k);

              Reflections tReflections = new Reflections();
              tReflections.transFields(tLARecomRelationBSchema,
                                       tLARecomRelationSchema);
              tLARecomRelationBSchema.setEdorNo(mEdorNo);
              tLARecomRelationBSchema.setEdorType("31");
              tLARecomRelationBSchema.setMakeDate2(tLARecomRelationBSchema.
                      getMakeDate());
              tLARecomRelationBSchema.setMakeTime2(tLARecomRelationBSchema.
                      getMakeTime());
              tLARecomRelationBSchema.setModifyDate2(tLARecomRelationBSchema.
                      getModifyDate());
              tLARecomRelationBSchema.setModifyTime2(tLARecomRelationBSchema.
                      getModifyTime());
              tLARecomRelationBSchema.setMakeDate(currentDate);
              tLARecomRelationBSchema.setMakeTime(currentTime);
              tLARecomRelationBSchema.setModifyDate(currentDate);
              tLARecomRelationBSchema.setModifyTime(currentTime);

              tLARecomRelationSchema.setAgentGroup(tGroup);
              tLARecomRelationSchema.setModifyDate(currentDate);
              tLARecomRelationSchema.setModifyTime(currentTime);
              tLARecomRelationSchema.setOperator(mGlobalInput.Operator);

              mLARecomRelationSet.add(tLARecomRelationSchema);
              mLARecomRelationBSet.add(tLARecomRelationBSchema);
          }
      }
      return true;
  }
	/*
	 * 断开血缘关系 参数: cLATreeDB-断绝血缘关系的发起方的行政信息 ,向上的全部断开，向下的只断开同级别的关系，
	 * 不同级别关系不变也不需要修改
	 */
	public boolean BreakAgentRear(LATreeSchema cLATreeSchema) {
		LARearRelationDB tLARearRelationDB = new LARearRelationDB();
		LARearRelationSchema tLARearRelationSchema;
		// LATreeDB tLATreeDB=new LATreeDB();
		// LATreeSet tLATreeDBSet=new LATreeSet();
		String tRear = "";
		if (this.mLAAssessAccessorySchema.getAgentSeries().equals("2")) {
			tLARearRelationDB.setRearLevel("02");
		} else if (this.mLAAssessAccessorySchema.getAgentSeries().equals("1")) {
			tLARearRelationDB.setRearLevel("51");
		}
		String tAgentCode = cLATreeSchema.getAgentCode();
		LARearRelationSet tLARearRelationSet = new LARearRelationSet();
		String SQLstr;
		// 向下的只断开同级别的关系
		SQLstr = "Select * from larearrelation where rearlevel='"
				+ tLARearRelationDB.getRearLevel() +
				"' and  rearagentcode='" + cLATreeSchema.getAgentCode() + "' ";
		System.out.println("BreakAgentRear中查询所有现存关系的SQL： " + SQLstr);
		tLARearRelationSet.add(tLARearRelationDB.executeQuery(SQLstr));
		// 向上的全部删除
		SQLstr = "Select * from larearrelation where agentcode='"
				+ cLATreeSchema.getAgentCode() + "'";
		tLARearRelationSet.add(tLARearRelationDB.executeQuery(SQLstr));
		// 向上和向下的关系,全部删除
		//去掉此处，仍保存间接养成关系，modified by miaoxz，at 2010-7-6
//		SQLstr = "select * from larearrelation  where rearagentcode in "
//				+ "(select rearagentcode from larearrelation b "
//				+ "where  b.agentcode='" + tAgentCode + "') "
//				+ "and agentcode in (select agentcode from larearrelation b "
//				+ "where  b.rearagentcode='" + tAgentCode
//				+ "') and rearedgens > 1  ";
//		tLARearRelationSet.add(tLARearRelationDB.executeQuery(SQLstr));
		// 下面要循环备份
		if (tLARearRelationSet.size() > 0) {
			for (int i = 1; i <= tLARearRelationSet.size(); i++) {
				// tLARearRelationDB=new LARearRelationDB();
				tLARearRelationSchema = new LARearRelationSchema();
				tLARearRelationSchema = tLARearRelationSet.get(i);
				if (!BackupRear(tLARearRelationSchema)) {
					CError.buildErr("备份血缘关系失败！", "BreakAgentRear");
					return false;
				}
				mLARearRelationSetDelete.add(tLARearRelationSchema.getSchema());
			}
		}
		// mMap.put(mLARearRelationSetDelete,"DELETE");
		return true;
	}

	/**
	 * 育成关系处理 参数：cAgentCode--职级调整代理人，cNewbranch-代理人的新机构；upagent--发起人的育成人
	 */
	private boolean dealLARear(String cAgentCode, String cNewbranch,String upagent) {
		// 查找向上的关系，备份后改删掉
		 LARearRelationDB upLARearRelationDB = new LARearRelationDB();
		 upLARearRelationDB.setAgentCode(cAgentCode);
	        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
	        tLARearRelationSet = upLARearRelationDB.query();
	        if (upLARearRelationDB.mErrors.needDealError())
	        {
	            CError.buildErr(upLARearRelationDB.mErrors.getFirstError(),
	                    "CheckRear");
	            return false;
	        }
	        //下面要循环备份
	        if(tLARearRelationSet.size()>0)
	        {
	            for (int i = 1; i <= tLARearRelationSet.size(); i++) {
	                //tLARearRelationDB=new LARearRelationDB();
	                LARearRelationSchema tLARearRelationSchema = new
	                        LARearRelationSchema();
	                tLARearRelationSchema= tLARearRelationSet.get(i);
	                if (!BackupRear(tLARearRelationSchema)) {
	                    CError.buildErr("备份血缘关系失败！",
	                            "BreakAgentRear");
	                    return false;
	                }
	            }
	        }
	        mLARearRelationSetDelete.add(tLARearRelationSet);
	    //查找向下的关系，备份后修改为无效
	        LARearRelationDB downLARearRelationDB = new LARearRelationDB();
	        downLARearRelationDB.setRearAgentCode(cAgentCode);
	        downLARearRelationDB.setRearFlag("1");
	        LARearRelationSet downLARearRelationSet = new LARearRelationSet();
	        downLARearRelationSet = downLARearRelationDB.query();
	        if (downLARearRelationDB.mErrors.needDealError()) {
	            CError.buildErr(downLARearRelationDB.mErrors.getFirstError(),
	                   "CheckRear");
	            return false;
	        }
	        //下面要循环备份
	        if (downLARearRelationSet.size() > 0) {
	            for (int i = 1; i <= downLARearRelationSet.size(); i++) {
	                //tLARearRelationDB=new LARearRelationDB();
	                LARearRelationSchema tLARearRelationSchema = new
	                        LARearRelationSchema();
	                tLARearRelationSchema=downLARearRelationSet.get(i);
	                if (!BackupRear(tLARearRelationSchema)) {
	                    CError.buildErr( this ,"备份血缘关系失败！", "AscriptRelationOperateBL",
	                            "BreakAgentRear");
	                    return false;
	                }
	                tLARearRelationSchema.setRearCommFlag("0");
	                tLARearRelationSchema.setRearFlag("0");
	                mLARearRelationSetUpdate.add(tLARearRelationSchema);
	            }
	        }
	        LARearRelationSet directLARearRelationSet = new LARearRelationSet();
	        LARearRelationDB directLARearRelationDB = new LARearRelationDB();
	        //查询本人为被育成人的间接培养关系
	        String downsql = "select * from larearrelation  where rearagentcode in "
	                 + "(select rearagentcode from larearrelation b "
	                 + "where  b.agentcode='" + cAgentCode + "') "
	                 +
	                 "and agentcode in (select agentcode from larearrelation b "
	                 + "where  b.rearagentcode='" + cAgentCode + "') and rearedgens > 1  ";

	        directLARearRelationSet = directLARearRelationDB.executeQuery(downsql);
	        if (directLARearRelationDB.mErrors.needDealError()) {
	            CError.buildErr(this,directLARearRelationDB.mErrors.getFirstError(),
	                    "AscriptRelationOperateBL", "CheckRear");
	            return false;
	        }
	        //下面要循环备份
	        if (directLARearRelationSet.size() > 0) {
	            for (int i = 1; i <= directLARearRelationSet.size(); i++) {
	                //tLARearRelationDB=new LARearRelationDB();
	                LARearRelationSchema tLARearRelationSchema = new
	                        LARearRelationSchema();
	                tLARearRelationSchema=directLARearRelationSet.get(i);
	                if (!BackupRear(tLARearRelationSchema)) {
	                    CError.buildErr(this,"备份血缘关系失败！", "AscriptRelationOperateBL",
	                            "BreakAgentRear");
	                    return false;
	                }
	                mLARearRelationSetDelete.add(directLARearRelationSet);
	            }
	        }
	        // 建立新的血缘关系
		String SQLstr;
		LARearRelationDB newLARearRelationDB = new LARearRelationDB();
		// 把发起人的所有关系（增部或育成）找到
//		SQLstr = "Select * from larearrelation where " + "( agentcode='"
//				+ this.mLAAssessAccessorySchema.getAgentCode()
//				+ "' or rearagentcode='"
//				+ this.mLAAssessAccessorySchema.getAgentCode() + "') ";
//		System.out.println("BreakAgentRear中查询所有现存关系的SQL： " + SQLstr);
//		LARearRelationSet newLARearRelationSet = new LARearRelationSet();
//		newLARearRelationSet.add(newLARearRelationDB.executeQuery(SQLstr));
		String Upagent = upagent;

		if (Upagent != null && !Upagent.equals("")) {
			// 建立一代育成关系
			newLARearRelationDB.setAgentCode(this.mLAAssessAccessorySchema
					.getAgentCode());
			newLARearRelationDB.setRearedGens(1);
			if (mLAAssessAccessorySchema.getAgentSeries1().equals("2")) {
				newLARearRelationDB.setRearLevel("02");
			} else {
				newLARearRelationDB.setRearLevel("01");
			}
			newLARearRelationDB.setstartDate(mAscriptDate);
			newLARearRelationDB.setEndDate("");
			newLARearRelationDB.setRearFlag("1");
			newLARearRelationDB.setAgentGroup(cNewbranch);
			newLARearRelationDB.setRearAgentCode(Upagent);
			newLARearRelationDB.setMakeDate(this.mAscriptDate);
			newLARearRelationDB.setMakeTime(currentTime);
			newLARearRelationDB.setModifyDate(currentDate);
			newLARearRelationDB.setModifyTime(currentTime);
			newLARearRelationDB.setOperator(mGlobalInput.Operator);
			newLARearRelationDB.setRearCommFlag("1");
			newLARearRelationDB.setRearStartYear(1);
			mLARearRelationSetInsert.add(newLARearRelationDB.getSchema());
		}
		// 下面是间接关系
		SSRS rearR = new SSRS();
		SQLstr = "select RearAgentCode,RearLevel,RearedGens from larearrelation where agentcode='"
				+ Upagent
				+ "' and rearlevel='"
				+ newLARearRelationDB.getRearLevel()
				+ "' and rearflag='1' order by rearedgens desc";
		System.out.println("CreateAgentRear中的查找间接关系的SQL: " + SQLstr);
		ExeSQL tExeSQL = new ExeSQL();
		rearR = tExeSQL.execSQL(SQLstr);
		if (rearR.MaxRow > 0) {
			for (int i = 1; i <= rearR.MaxRow; i++) {
				tLARearRelationDBIndirect
						.setAgentCode(this.mLAAssessAccessorySchema
								.getAgentCode());
				tLARearRelationDBIndirect.setRearLevel(rearR.GetText(i, 2));
				tLARearRelationDBIndirect.setRearedGens(String.valueOf(Integer
						.parseInt(rearR.GetText(i, 3)) + 1)); //
				tLARearRelationDBIndirect.setRearCommFlag("1");
				tLARearRelationDBIndirect.setRearStartYear(1);

				tLARearRelationDBIndirect.setRearAgentCode(rearR.GetText(i, 1));
				tLARearRelationDBIndirect.setstartDate(mAscriptDate);
				tLARearRelationDBIndirect.setEndDate("");
				tLARearRelationDBIndirect.setRearFlag("1");
				tLARearRelationDBIndirect.setAgentGroup(cNewbranch);
				tLARearRelationDBIndirect.setMakeDate(this.mAscriptDate);
				tLARearRelationDBIndirect.setMakeTime(currentTime);
				tLARearRelationDBIndirect.setModifyDate(currentDate);
				tLARearRelationDBIndirect.setModifyTime(currentTime);
				tLARearRelationDBIndirect.setOperator(mGlobalInput.Operator);
				mLARearRelationSetInsert.add(tLARearRelationDBIndirect
						.getSchema());
			}
		}

		return true;
	}


        /**
                 * 育成关系处理 参数：cAgentCode--职级调整代理人，cNewbranch-代理人的新机构；upagent--发起人的育成人
                 */
                private boolean dealLARear(String cAgentCode,String cAgentSeries1, String cNewbranch,String upagent) {
                        // 查找向上的关系，备份后改删掉
                         LARearRelationDB upLARearRelationDB = new LARearRelationDB();
                         upLARearRelationDB.setAgentCode(cAgentCode);
                        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
                        tLARearRelationSet = upLARearRelationDB.query();
                        if (upLARearRelationDB.mErrors.needDealError())
                        {
                            CError.buildErr(upLARearRelationDB.mErrors.getFirstError(),
                                    "CheckRear");
                            return false;
                        }
                        //下面要循环备份
                        if(tLARearRelationSet.size()>0)
                        {
                            for (int i = 1; i <= tLARearRelationSet.size(); i++) {
                                //tLARearRelationDB=new LARearRelationDB();
                                LARearRelationSchema tLARearRelationSchema = new
                                        LARearRelationSchema();
                                tLARearRelationSchema= tLARearRelationSet.get(i);
                                if (!BackupRear(tLARearRelationSchema)) {
                                    CError.buildErr("备份血缘关系失败！",
                                            "BreakAgentRear");
                                    return false;
                                }
                            }
                        }
                        mLARearRelationSetDelete.add(tLARearRelationSet);
                    //查找向下的关系，备份后修改为无效
                        LARearRelationDB downLARearRelationDB = new LARearRelationDB();
                        downLARearRelationDB.setRearAgentCode(cAgentCode);
                        downLARearRelationDB.setRearFlag("1");
                        LARearRelationSet downLARearRelationSet = new LARearRelationSet();
                        downLARearRelationSet = downLARearRelationDB.query();
                        if (downLARearRelationDB.mErrors.needDealError()) {
                            CError.buildErr(downLARearRelationDB.mErrors.getFirstError(),
                                   "CheckRear");
                            return false;
                        }
                        //下面要循环备份
                        if (downLARearRelationSet.size() > 0) {
                            for (int i = 1; i <= downLARearRelationSet.size(); i++) {
                                //tLARearRelationDB=new LARearRelationDB();
                                LARearRelationSchema tLARearRelationSchema = new
                                        LARearRelationSchema();
                                tLARearRelationSchema=downLARearRelationSet.get(i);
                                if (!BackupRear(tLARearRelationSchema)) {
                                    CError.buildErr( this ,"备份血缘关系失败！", "AscriptRelationOperateBL",
                                            "BreakAgentRear");
                                    return false;
                                }
                                tLARearRelationSchema.setRearCommFlag("0");
                                tLARearRelationSchema.setRearFlag("0");
                                mLARearRelationSetUpdate.add(tLARearRelationSchema);
                            }
                        }
                        LARearRelationSet directLARearRelationSet = new LARearRelationSet();
                        LARearRelationDB directLARearRelationDB = new LARearRelationDB();
                        //查询本人为被育成人的间接培养关系
                        //根据ＩＴ要求，间接养成关系仍保持有效，不再断裂，modified by miaoxz at 2010-7-6
//                        String downsql = "select * from larearrelation  where rearagentcode in "
//                                 + "(select rearagentcode from larearrelation b "
//                                 + "where  b.agentcode='" + cAgentCode + "') "
//                                 +
//                                 "and agentcode in (select agentcode from larearrelation b "
//                                 + "where  b.rearagentcode='" + cAgentCode + "') and rearedgens > 1  ";
//
//                        directLARearRelationSet = directLARearRelationDB.executeQuery(downsql);
//                        if (directLARearRelationDB.mErrors.needDealError()) {
//                            CError.buildErr(this,directLARearRelationDB.mErrors.getFirstError(),
//                                    "AscriptRelationOperateBL", "CheckRear");
//                            return false;
//                        }
//                        //下面要循环备份
//                        if (directLARearRelationSet.size() > 0) {
//                            for (int i = 1; i <= directLARearRelationSet.size(); i++) {
//                                //tLARearRelationDB=new LARearRelationDB();
//                                LARearRelationSchema tLARearRelationSchema = new
//                                        LARearRelationSchema();
//                                tLARearRelationSchema=directLARearRelationSet.get(i);
//                                if (!BackupRear(tLARearRelationSchema)) {
//                                    CError.buildErr(this,"备份血缘关系失败！", "AscriptRelationOperateBL",
//                                            "BreakAgentRear");
//                                    return false;
//                                }
//                                mLARearRelationSetDelete.add(directLARearRelationSet);
//                            }
//                        }
                        //miaoxz modified end here 2010-7-6
                        
                        
                        // 建立新的血缘关系
                        String SQLstr;
                        String Upagentseries="";
                        LARearRelationDB newLARearRelationDB = new LARearRelationDB();
                        // 把发起人的所有关系（增部或育成）找到
//		SQLstr = "Select * from larearrelation where " + "( agentcode='"
//				+ this.mLAAssessAccessorySchema.getAgentCode()
//				+ "' or rearagentcode='"
//				+ this.mLAAssessAccessorySchema.getAgentCode() + "') ";
//		System.out.println("BreakAgentRear中查询所有现存关系的SQL： " + SQLstr);
//		LARearRelationSet newLARearRelationSet = new LARearRelationSet();
//		newLARearRelationSet.add(newLARearRelationDB.executeQuery(SQLstr));
                        String Upagent = upagent;
                        String tUpagentseries="";
                        //查询主管的序列，保证两者的职级序列相同
                        if (Upagent != null && !Upagent.equals("")) {
                       tUpagentseries="select agentseries from latree where agentcode = '"
                        +Upagent+ "'";
                       ExeSQL tExeSQL1 = new ExeSQL();
                       Upagentseries=tExeSQL1.getOneValue(tUpagentseries);
                       }

                        if (Upagent != null && !Upagent.equals("") && Upagentseries.equals(cAgentSeries1)) {
                                // 建立一代育成关系
                                newLARearRelationDB.setAgentCode(this.mLAAssessAccessorySchema
                                                .getAgentCode());
                                newLARearRelationDB.setRearedGens(1);
                                if (mLAAssessAccessorySchema.getAgentSeries1().equals("2")) {
                                        newLARearRelationDB.setRearLevel("02");
                                } else {
                                        newLARearRelationDB.setRearLevel("01");
                                }
                                newLARearRelationDB.setstartDate(mAscriptDate);
                                newLARearRelationDB.setEndDate("");
                                newLARearRelationDB.setRearFlag("1");
                                newLARearRelationDB.setAgentGroup(cNewbranch);
                                newLARearRelationDB.setRearAgentCode(Upagent);
                                newLARearRelationDB.setMakeDate(this.mAscriptDate);
                                newLARearRelationDB.setMakeTime(currentTime);
                                newLARearRelationDB.setModifyDate(currentDate);
                                newLARearRelationDB.setModifyTime(currentTime);
                                newLARearRelationDB.setOperator(mGlobalInput.Operator);
                                newLARearRelationDB.setRearCommFlag("1");
                                newLARearRelationDB.setRearStartYear(1);
                                mLARearRelationSetInsert.add(newLARearRelationDB.getSchema());
                        }
                        // 下面是间接关系
                     
                        SSRS rearR = new SSRS();
                        SQLstr = "select RearAgentCode,RearLevel,RearedGens from larearrelation where agentcode='"
                                        + Upagent
                                        + "' and rearlevel='"
                                        + newLARearRelationDB.getRearLevel()
                                        + "' and rearflag='1' order by rearedgens desc";
                        System.out.println("CreateAgentRear中的查找间接关系的SQL: " + SQLstr);
                        ExeSQL tExeSQL = new ExeSQL();
                        rearR = tExeSQL.execSQL(SQLstr);
                        if (rearR.MaxRow > 0) {
                                for (int i = 1; i <= rearR.MaxRow; i++) {
                                        tLARearRelationDBIndirect
                                                        .setAgentCode(this.mLAAssessAccessorySchema
                                                                        .getAgentCode());
                                        tLARearRelationDBIndirect.setRearLevel(rearR.GetText(i, 2));
                                        tLARearRelationDBIndirect.setRearedGens(String.valueOf(Integer
                                                        .parseInt(rearR.GetText(i, 3)) + 1)); //
                                        tLARearRelationDBIndirect.setRearCommFlag("1");
                                        tLARearRelationDBIndirect.setRearStartYear(1);

                                        tLARearRelationDBIndirect.setRearAgentCode(rearR.GetText(i, 1));
                                        tLARearRelationDBIndirect.setstartDate(mAscriptDate);
                                        tLARearRelationDBIndirect.setEndDate("");
                                        tLARearRelationDBIndirect.setRearFlag("1");
                                        tLARearRelationDBIndirect.setAgentGroup(cNewbranch);
                                        tLARearRelationDBIndirect.setMakeDate(this.mAscriptDate);
                                        tLARearRelationDBIndirect.setMakeTime(currentTime);
                                        tLARearRelationDBIndirect.setModifyDate(currentDate);
                                        tLARearRelationDBIndirect.setModifyTime(currentTime);
                                        tLARearRelationDBIndirect.setOperator(mGlobalInput.Operator);
                                        mLARearRelationSetInsert.add(tLARearRelationDBIndirect
                                                        .getSchema());
                                }
                        }
                        return true;
                }


	/*
	 * 备份目的机构 cLABranchGroupSchema-要备份机构的信息
	 */
	private boolean backupBranchGroup(LABranchGroupSchema cLABranchGroupSchema) {
		try {
			Reflections tReflections = new Reflections();
			LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
			LABranchGroupBSchema tLABranchGroupBDB = new LABranchGroupBSchema();
			tLABranchGroupSchema = cLABranchGroupSchema;
			tLABranchGroupBDB.setSchema((LABranchGroupBSchema) tReflections
					.transFields(tLABranchGroupBDB, tLABranchGroupSchema));
			tLABranchGroupBDB.setEdorNo(mEdorNo);
			tLABranchGroupBDB.setEdorType("31");// 调岗
			tLABranchGroupBDB.setUpBranchAttr((cLABranchGroupSchema
					.getUpBranchAttr() == null || cLABranchGroupSchema
					.getUpBranchAttr().equals("")) ? "0" : cLABranchGroupSchema
					.getUpBranchAttr());
			tLABranchGroupBDB.setOperator2(cLABranchGroupSchema.getOperator());
			tLABranchGroupBDB.setMakeDate2(cLABranchGroupSchema.getMakeDate());
			tLABranchGroupBDB.setModifyDate2(cLABranchGroupSchema
					.getModifyDate());
			tLABranchGroupBDB.setMakeTime2(cLABranchGroupSchema.getMakeTime());
			tLABranchGroupBDB.setModifyTime2(cLABranchGroupSchema
					.getModifyTime());
			tLABranchGroupBDB.setMakeDate(currentDate);
			tLABranchGroupBDB.setMakeTime(currentTime);
			tLABranchGroupBDB.setModifyDate(currentDate);
			tLABranchGroupBDB.setModifyTime(currentTime);
			tLABranchGroupBDB.setOperator(mGlobalInput.Operator);
			tLABranchGroupBDB.setIndexCalNo(mIndexCalNo);

			mLABranchGroupBSet.add(tLABranchGroupBDB);
		} catch (Exception e) {
			CError.buildErr(e.getMessage(), "backupBranchGroup");
			return false;
		}
		return true;
	}
	   /*
    修改cAgentCode的agentgroup和Branchcode为cGroupSchema
   */
  public boolean updateAgentInfo(String cAgentCode, LABranchGroupSchema cGroupSchema)
  {
      LATreeSchema tLATreeSchema = new LATreeSchema();
      LATreeDB tLAUpAgentTreeDB = new LATreeDB();
      LAAgentDB tLAAgentDB = new LAAgentDB();
      tLAAgentDB.setAgentCode(cAgentCode);
      if (!tLAAgentDB.getInfo()) {
          CError.buildErr(this,"取代理人基本信息失败！", "AscriptAgentInfoOperateBL",
                  "updateAgentInfo");
          return false;
      }
      if (!backupAgent(tLAAgentDB.getSchema())) {
    	  CError.buildErr(this,"备份代理人基本信息失败！", "AscriptAgentInfoOperateBL",
                  "changeAgentGroup");
          return false;
      }
      tLAUpAgentTreeDB.setAgentCode(cAgentCode);
      if (!tLAUpAgentTreeDB.getInfo()) {
    	  CError.buildErr(this,"取代理人基本信息失败！", "AscriptAgentInfoOperateBL",
                  "updateAgentInfo");
          return false;
      }
      if (!backupAgentTree(tLAUpAgentTreeDB.getSchema())) {
    	  CError.buildErr(this,"备份代理人基本信息失败！", "AscriptAgentInfoOperateBL",
                  "updateAgentInfo");
          return false;
      }
      tLATreeSchema = tLAUpAgentTreeDB.getSchema();
      String tNewGroup = cGroupSchema.getAgentGroup();
      String tBranchManager = cGroupSchema.getBranchManager();

      tLATreeSchema.setAgentGroup(tNewGroup);
      tLAAgentDB.setAgentGroup(tNewGroup);
      //降级为业务员
      tLATreeSchema.setUpAgent(tBranchManager);

      tLATreeSchema.setBranchCode(tNewGroup);
      tLAAgentDB.setBranchCode(tNewGroup);

      tLATreeSchema.setModifyDate(currentDate);
      tLATreeSchema.setModifyTime(currentTime);
      tLATreeSchema.setOperator(mGlobalInput.Operator);
      tLAAgentDB.setModifyDate(currentDate);
      tLAAgentDB.setModifyTime(currentTime);
      tLAAgentDB.setOperator(mGlobalInput.Operator);
      mLATreeSet.add(tLATreeSchema);
      mLAAgentSet.add(tLAAgentDB.getSchema());
      return true;
  }
	/*
	 * 修改目的组信息中有关上级机构的信息,主要是把组挂到新的部下面 参数:cAgentGroup-目的组的编码 LABranchGroupSchema
	 * cGroupSchema-目的机构的Schema
	 */
	public boolean updateGroupInfo(String cAgentGroup,
			LABranchGroupSchema cGroupSchema) {
		System.out.println("cAgentGroup" + cAgentGroup);
		System.out.println("tAgentGroup" + cGroupSchema.getAgentGroup());
		String tMaxAttr = "";
		LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
		tLABranchGroupDB.setAgentGroup(cAgentGroup);
		if (!tLABranchGroupDB.getInfo()) {
			CError.buildErr("查询人员的原机构的信息出错！", "updateGroupInfo");
			return false;
		}
		if (!backupBranchGroup(tLABranchGroupDB.getSchema())) {
			CError.buildErr("备份人员的原机构的信息出错！", "updateGroupInfo");
			return false;
		}
		tLABranchGroupDB.setUpBranch(cGroupSchema.getAgentGroup());
		tLABranchGroupDB.setBranchSeries(cGroupSchema.getBranchSeries() + ":"
				+ tLABranchGroupDB.getAgentGroup());
		if (tLABranchGroupDB.getBranchManager() == null
				|| tLABranchGroupDB.getBranchManager().equals("")) {// 如果没有组长就默认不是直辖组
			// dealErr("机构"+tLABranchGroupDB.getBranchAttr()+"没有主管，不能处理，请先补充相应的信息！","AscriptBranchGroupOperateBL","updateGroupInfo");
			// return false;
			tLABranchGroupDB.setUpBranchAttr("0");
		} else {
			if (tLABranchGroupDB.getBranchManager().equals(
					cGroupSchema.getBranchManager())) { // 设置部的直辖组
				tLABranchGroupDB.setUpBranchAttr("1");
			} else {
				tLABranchGroupDB.setUpBranchAttr("0");
			}
		}

//		String maxSQL = "select max(branchattr) from labranchgroupb where upbranch='"
//				+ cGroupSchema.getAgentGroup() + "'";
//		ExeSQL maxExeSQL = new ExeSQL();
//		mMAXATTR = maxExeSQL.getOneValue(maxSQL);
//		if (maxExeSQL.mErrors.needDealError()) {
//			CError.buildErr(this, "机构最大编码查询失败！");
//			return false;
//		}
		if (mMAXATTR.equals("")) {
			// 2005-07-04 modified by liujw
			mMAXATTR = AgentPubFun.CreateBranchAttr(cGroupSchema
					.getBranchAttr(), tLABranchGroupDB.getBranchLevel(),
					tLABranchGroupDB.getBranchType(), tLABranchGroupDB
							.getBranchType2());
			if (mMAXATTR == null) {
				CError.buildErr("生成销售机构号失败，因为机构级别描述表数据不全！", "updateGroupInfo");
				return false;
			}
			tMaxAttr = mMAXATTR;
		} else {
			tMaxAttr = AgentPubFun.getMaxBranchAttr(cGroupSchema
					.getBranchAttr(), mMAXATTR);
			 mMAXATTR = tMaxAttr;
		}
		tLABranchGroupDB.setBranchAttr(tMaxAttr);
		tLABranchGroupDB.setModifyDate(currentDate);
		tLABranchGroupDB.setModifyTime(currentTime);
		tLABranchGroupDB.setOperator(mGlobalInput.Operator);
		mChangeLABranchGroupSchema = tLABranchGroupDB.getSchema();
		mLABranchGroupSet.add(mChangeLABranchGroupSchema);
		return true;
	}

	private boolean BackupRear(LARearRelationSchema cLARearRelationSchema) {
		Reflections tReflections = new Reflections();
		LARearRelationBSchema tLARearRelationBSchema = new LARearRelationBSchema();
		LARearRelationSchema tLARearRelationSchema = new LARearRelationSchema();
		tLARearRelationSchema = cLARearRelationSchema;
		tReflections.transFields(tLARearRelationBSchema, tLARearRelationSchema);
		tLARearRelationBSchema.setEdorNo(mEdorNo);
		tLARearRelationBSchema.setEdorType("31");
		tLARearRelationBSchema.setEndDate(mEndDate[1]);
		tLARearRelationBSchema
				.setOperator2(cLARearRelationSchema.getOperator());
		tLARearRelationBSchema
				.setMakeDate2(cLARearRelationSchema.getMakeDate());
		tLARearRelationBSchema.setModifyDate2(cLARearRelationSchema
				.getModifyDate());
		tLARearRelationBSchema
				.setMakeDate2(cLARearRelationSchema.getMakeTime());
		tLARearRelationBSchema.setModifyTime2(cLARearRelationSchema
				.getModifyTime());
		tLARearRelationBSchema.setMakeDate(currentDate);
		tLARearRelationBSchema.setMakeTime(currentTime);
		tLARearRelationBSchema.setModifyDate(currentDate);
		tLARearRelationBSchema.setModifyTime(currentTime);
		tLARearRelationBSchema.setOperator(mGlobalInput.Operator);
		mLARearRelationBSet.add(tLARearRelationBSchema);
		//mMap.put(mLARearRelationBSet, "INSERT");
		return true;
	}

public boolean changeManagerInfo(LATreeSchema tLATreeSchema,String cBranchManager)
{
   tLATreeSchema.setUpAgent(cBranchManager);
   tLATreeSchema.setModifyDate(currentDate);
   tLATreeSchema.setModifyTime(currentTime);
   tLATreeSchema.setOperator(mGlobalInput.Operator);
   mLATreeSet.add(tLATreeSchema);
   return true;
}

	/**
	 * 修改业绩，包括本人
	 *
	 * @return
	 */
	private boolean setGrptoGroup(String cGroup,
			LABranchGroupSchema cLABranchGroupSchema) {
		String tAgentGroup = cLABranchGroupSchema.getAgentGroup();
		String tBranchCode = cLABranchGroupSchema.getAgentGroup();
		String tBranchAttr = cLABranchGroupSchema.getBranchAttr();
		String tBranchSeries = cLABranchGroupSchema.getBranchSeries();

		String tSql = "select * from lacommision where agentgroup='" + cGroup
				+ "' and (caldate>='" + mAscriptDate + "' or caldate is null)";
		LACommisionSet tmLACommisionSet = new LACommisionSet();
		LACommisionDB tLACommisionDB = new LACommisionDB();
		System.out.println(tSql);
		tmLACommisionSet = tLACommisionDB.executeQuery(tSql);
		for (int j = 1; j <= tmLACommisionSet.size(); j++) {
			LACommisionSchema tLACommisionSchema = new LACommisionSchema();
			tLACommisionSchema = tmLACommisionSet.get(j);
			tLACommisionSchema.setAgentGroup(tAgentGroup);
			tLACommisionSchema.setBranchCode(tBranchCode);
			tLACommisionSchema.setBranchSeries(tBranchSeries);
			tLACommisionSchema.setBranchAttr(tBranchAttr);
			tLACommisionSchema.setModifyDate(currentDate);
			tLACommisionSchema.setModifyTime(currentTime);
			tLACommisionSchema.setOperator(mGlobalInput.Operator);
			this.mLACommisionSet.add(tLACommisionSchema);
		}

		return true;

	}
	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "xxx";
		tG.ManageCom = "86";
		String tt ="dfafaaa";
	    String a =	tt.substring(0,2);
	    System.out.println(a);
		LAAssessAccessorySet mLAAssessAccessorySet = new LAAssessAccessorySet();
		LAAssessAccessorySchema tLAAssessAccessorySchema = new LAAssessAccessorySchema();
		tLAAssessAccessorySchema.setAgentCode("1201000011");
		tLAAssessAccessorySchema.setAgentGrade("B02");
		tLAAssessAccessorySchema.setAgentGrade1("B11");
		tLAAssessAccessorySchema.setAgentGroup("000000000499");
		tLAAssessAccessorySchema.setAgentGroupNew("000000001443");
		tLAAssessAccessorySchema.setStandAssessFlag("0");
		tLAAssessAccessorySchema.setAssessType("00");
		tLAAssessAccessorySchema.setIndexCalNo("200610");
		tLAAssessAccessorySchema.setBranchType("5");
		tLAAssessAccessorySchema.setBranchType2("01");
		tLAAssessAccessorySchema.setManageCom("86940000");
		tLAAssessAccessorySchema.setModifyFlag("03");
		tLAAssessAccessorySchema.setAgentSeries("1");
		tLAAssessAccessorySchema.setAgentSeries1("2");
		// tLAAssessAccessorySchema.setAssessType("00");
		mLAAssessAccessorySet.add(tLAAssessAccessorySchema);
		VData tVData = new VData();
		tVData.add(tG);
		tVData.addElement(mLAAssessAccessorySet);
		tVData.addElement(tLAAssessAccessorySchema);
		tVData.addElement("1");
		tVData.addElement("01");
		LAActiveAscriptInDiffSeriesBL mLAAscriptInDiffSeriesBL = new LAActiveAscriptInDiffSeriesBL();
		mLAAscriptInDiffSeriesBL.submitData(tVData, "");
		System.out.println("Error:"
				+ mLAAscriptInDiffSeriesBL.mErrors.getFirstError());
	}
	private boolean dealGBuild()
	{
		String tAscriptDate = AgentPubFun.formatDate(this.mAscriptDate,"yyyy-MM-dd");//变更下条件
		String tEndDate = getGBuildEnd(tAscriptDate);
            if(this.NewGroupDB.getAgentGroup()==null||this.NewGroupDB.getAgentGroup().equals(""))
            {
            	return true;
            }
			String tBranch = this.NewGroupDB.getBranchSeries().substring(0, 12);//得到营业部编码
			
			LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
			LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
			tLABranchGroupDB.setAgentGroup(tBranch);
			if (!tLABranchGroupDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustAgentBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "不存在所要修改的机构!";
                this.mErrors.addOneError(tError);
                return false;
            }
			tLABranchGroupSet=tLABranchGroupDB.query();
			LABranchGroupSchema t_LABranchGroupSchema = new LABranchGroupSchema();//存营业部编码
			t_LABranchGroupSchema=tLABranchGroupSet.get(1);
			
			if(NewGroupDB.getFoundDate().equals(this.mAscriptDate)&&NewGroupDB.getFoundDate().compareTo("2014-04-01")<0)				
			{
				if(t_LABranchGroupSchema.getApplyGBFlag()!=null&&t_LABranchGroupSchema.getApplyGBFlag().equals("Y"))
				{
					String sql = "update labranchgroup set applygbflag= 'Y',applygbstartdate='"+this.mAscriptDate+"',modifydate= current date,modifytime = current time where agentgroup = '"+NewGroupDB.getAgentGroup()+"'";
					if(t_LABranchGroupSchema.getGBuildFlag()!=null)
					{
				     if(NewGroupDB.getFoundDate().equals(this.mAscriptDate))
					   {
							sql = "update labranchgroup set applygbflag= 'Y',applygbstartdate='"+this.mAscriptDate+"',gbuildflag = '"+t_LABranchGroupSchema.getGBuildFlag()+"',gbuildstartdate = '"+this.mAscriptDate+"',gbuildenddate = '"+tEndDate+"',modifydate= current date,modifytime = current time where agentgroup = '"+NewGroupDB.getAgentGroup()+"'";
	   			       }					
					}
					 mMap.put(sql, "UPDATE");
				}
				
			}		
		return true;
	}
    /**
     *  得到考核截止日期
     * @param GBuildStartDate
     * @return
     */
    private String getGBuildEnd(String GBuildStartDate)
    {
    	String tGBuildEndDate ="";
    	 String tsql = "select enddate from LASTATSEGMENT where char(yearmonth) = db2inst1.DATE_FORMAT(((date('"+GBuildStartDate+"') + 1 year) - 1 month),'yyyymm') and  stattype = '1'";
		  ExeSQL tExeSQL = new ExeSQL();
		  tGBuildEndDate =tExeSQL.getOneValue(tsql);
		  if(tGBuildEndDate==null||tGBuildEndDate.equals(""))
		  {
			   CError tError = new CError();
	           tError.moduleName = "LABranchGroupBuildBL";
	           tError.functionName = "check";
	           tError.errorMessage = "团队建设起期格式录入有误!";
	           this.mErrors.addOneError(tError);
	           return "";  
		  }
    	return tGBuildEndDate;
    }
    
    /**
     * 功能：生成新建机构名称
     * GetGroupName  生成新建团队的名称，
     *  @param branchseries 团队序列
     *  @param branchAttr 所在机构编码
     *  @param branchLevel 机构级别
     */
    private String getGroupName(String branchseries,String branchAttr,String branchLevel) {
		//机构名
    	String groupName = "";
		//机构基本
    	String grade = "";
    	//上级机构团队编码
		String agentgroup = "";
		//上级团队名称
		String upbranchName = "";
		//机构的排序号
		String numstr ="";
		//获得上级机构所在团队编号
		String[] agentgroups = branchseries.split(":");
		if(agentgroups.length==2){
			agentgroup = agentgroups[0];
			numstr = branchAttr.substring(10);
		}else if(agentgroups.length==3){
			agentgroup = agentgroups[1];
			if(agentgroups[1].equals("000000000000")){
				agentgroup = agentgroups[0];
			}
			numstr = branchAttr.substring(12);
		}
		else if(agentgroups.length==1){
			agentgroup = agentgroups[0];
			numstr = branchAttr;
		}
		//使用SQL查询出上级机构名称
		ExeSQL tExeSQL=new ExeSQL();
        SSRS tSSRS=new SSRS();
        tSSRS=tExeSQL.execSQL("select name from LABRANCHGROUP where agentgroup='"+agentgroup+"'");
        if(tSSRS.MaxRow>0)
        {
        	upbranchName=tSSRS.GetText(1,1);
        }
		System.out.println(upbranchName);
		//获得该机构编号
		int num = Integer.parseInt(numstr);
		//确定机构级别
		if(branchLevel.equals("02")){
			grade = "区";
		}else if(branchLevel.equals("51")){
			grade = "部";
		}
		//连接字符,拼接到一起组成团队名称
		groupName = upbranchName + this.convertToChnNum(num) + grade;
		System.out.println("AscriptBranchGroupOperateBL" + groupName);
		return groupName;
	}
    
    //
    /**
     * 功能：将阿拉伯数字转化成中文数字
     * @param num  要转化的数字
     */
    private String convertToChnNum(int num){
		String[] ChnNum={"零","一","二","三","四","五","六","七","八","九","十","百","千","万"};
	    String chnnum = "";
	    if(num<10 && num>0){
	    	chnnum = ChnNum[num];
	    }else if(num>=10 && num<100){
	    	String tensdigit = "";
	    	if(num>=20){
	    		tensdigit = ChnNum[num/10];
	    	}
	        String unitsdigit = "";
	        if(num%10!=0){
	        	unitsdigit = ChnNum[num%10];
	        }
	    	chnnum = tensdigit+ChnNum[10]+unitsdigit;
	    }else if(num>=100 && num<1000){
	        String hundredths = "";
	        hundredths = ChnNum[num/100];
	        String numstr = this.convertToChnNum(num%100);
	        if(num%100<10 && num%100>0){
	        	numstr = ChnNum[0]+numstr;
	        }
	        if(num%100>=10 && num%100<20){
        		numstr = ChnNum[1]+numstr;
        	}
	        chnnum = hundredths+ChnNum[11]+numstr;
	    }else if(num>=1000 && num<10000){
	    	 String kilobit = ChnNum[num/1000];
	    	 String numstr =this.convertToChnNum(num%1000); 
	    	 if(num%1000>=10 && num%1000<20){
	    		 numstr = ChnNum[1]+numstr;
	    	 }
	    	 if(num%1000<100 && num%1000>0){
	    		 numstr = ChnNum[0]+numstr;
	    	 }
	    	 chnnum = kilobit +ChnNum[12]+numstr;
	    }
		return chnnum;
	}
    
    private String getbranchManagerName(String tBranchManager){
    	String tBranchManagerName ="";
		ExeSQL tExeSQL=new ExeSQL();
        SSRS tSSRS=new SSRS();
        tSSRS=tExeSQL.execSQL("select name from laagent where agentcode='"+tBranchManager+"'");
        if(tSSRS.MaxRow>0)
        {
        	tBranchManagerName=tSSRS.GetText(1,1);
        }
        return tBranchManagerName;
    }
}
