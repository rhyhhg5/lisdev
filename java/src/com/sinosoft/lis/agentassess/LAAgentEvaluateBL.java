package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;

/**
 * 考核评估计算程序
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAAgentEvaluateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap=new MMap();
    private VData mOutputDate=new VData();
    private LAAssessSet mLAAssessSet = new LAAssessSet();
    private LAAssessSet mLAAssessOutSet = new LAAssessSet();
    private LAAssessHistorySchema mLAAssessHistorySchema = new LAAssessHistorySchema();
    private LAAssessMainSet mLAAssessMainSet = new LAAssessMainSet();
    private String mManageCom = "";
    private String mIndexCalNo = "";
    private String mGradeSeries = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //处理前进行验证
        if(!check()){
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit=new PubSubmit();
        tPubSubmit.submitData(mOutputDate,"") ;
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError()) {
            // @@错误处理
            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 验证方法
     * @return boolean
     */
    private boolean check()
    {
        //如果是团险,必须传入人员系列
        if("2".equals(mLAAssessHistorySchema.getBranchType())
           && (mGradeSeries.indexOf("E") < 0 && mGradeSeries.indexOf("D") < 0))
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateBL";
            tError.functionName = "check";
            tError.errorMessage = "团险考核,请输入考核序列！";
            System.out.println("团险考核,没有录入考核序列！");
            this.mErrors.addOneError(tError);

            return false;
        }
        //检验当期考核是否归属过
        String tSQL = "";
        tSQL  = "select count(*) from laassessmain where indexcalno='";
        tSQL += mIndexCalNo+"' and managecom='";
        tSQL += mManageCom + "' and state='02'";
        tSQL += " and AgentGrade = '" + this.mGradeSeries + "'";
        tSQL += " and BranchType='"+this.mLAAssessHistorySchema.getBranchType()+"'";
        tSQL += " and BranchType2='"+this.mLAAssessHistorySchema.getBranchType2()+"'";

        if(execQuery(tSQL) > 0)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateBL";
            tError.functionName = "check";
            tError.errorMessage = "本期考核该机构已经归属完毕，请确认！";
            System.out.println("本期考核该机构已经归属完毕，请确认！");
            this.mErrors.addOneError(tError);

            return false;
        }
        for(int i=1;i<=mLAAssessSet.size();i++)
        {
            LAAssessSchema tLAAssessSchema = new LAAssessSchema();
            tLAAssessSchema=mLAAssessSet.get(i);
            if(tLAAssessSchema.getAgentGrade().equals("D01"))
            {
                String tSql=" select state from latree where agentcode='"+tLAAssessSchema.getAgentCode()
                            +"' ";
                ExeSQL tExeSQL = new ExeSQL();
                String tState=tExeSQL.getOneValue(tSql);
                if(tState.equals("4") && !tLAAssessSchema.getCalAgentGrade().equals("D01") &&
                   tLAAssessSchema.getAgentGrade().equals("D01"))
                {//延迟转正的人员第二次转正未通过,则必须为清退
//                    CError tError = new CError();
//                    tError.moduleName = "LAAgentEvaluateBL";
//                    tError.functionName = "check";
//                    tError.errorMessage = tLAAssessSchema.getAgentCode()+"第二次转正考核未通过,不能更改其职级！";
//                    System.out.println("第二次转正考核未通过，不能确认！");
//                    this.mErrors.addOneError(tError);
//                    return false;

                }
            }
        }

        return true;
    }

    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        String tSQL = "";
        try
        {
            tSQL  = "SELECT Count(*) FROM LAAssessMain";
            tSQL += " WHERE IndexCalNo = '"+mIndexCalNo+"' AND AgentGrade='"+mGradeSeries+"'";
            tSQL += "   AND ManageCom='"+mManageCom+"' AND AssessType='00'";

            mMap.put(this.mLAAssessOutSet, "UPDATE");
            if (execQuery(tSQL) < 1) {
                System.out.println("考核主表不存在此数据！");
                mMap.put(this.mLAAssessMainSet, "INSERT");
            }
            else
            {
                mMap.put(this.mLAAssessMainSet, "UPDATE");
            }
            this.mOutputDate.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            System.out.println("在准备往后层处理所需要的数据时出错。");
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
        LAAssessDB tLAAssessDB = new LAAssessDB();
        LAAssessSet tLAAssessSet = new LAAssessSet();
        LAAssessSet tLAAssessDealSet = new LAAssessSet();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAssessMainSchema tLAAssessMainSchema = new LAAssessMainSchema();

        int tAgentCount = mLAAssessSet.size();   //需要处理的人数
        //循环处理每个人的考核信息
        for(int i=1;i<=tAgentCount;i++)
        {
            tLAAssessSchema = dealAssessAgeng(mLAAssessSet.get(i));
            //如果更新信息成功
            if(tLAAssessSchema!=null)
            {
                tLAAssessDealSet.add(tLAAssessSchema);
            }else
            {
                CError tError = new CError();
                tError.moduleName = "LAAgentEvaluateBL";
                tError.functionName = "dealData";
                tError.errorMessage = "更新个人考核信息失败！";
                System.out.println("更新个人考核信息失败！");
                this.mErrors.addOneError(tError);

                return false;
            }
        }

        mLAAssessOutSet = tLAAssessDealSet;

        //设置考核主表信息
        tLAAssessMainSchema.setIndexCalNo(this.mIndexCalNo);
        tLAAssessMainSchema.setAgentGrade(this.mGradeSeries); // 默认  这里只关心管理机构不关心职级
        tLAAssessMainSchema.setManageCom(this.mManageCom);
        tLAAssessMainSchema.setAssessType("00");  // 考核类型
//        tLAAssessMainSchema.setBranchType("1");   // 这里是个险用的考核
//        tLAAssessMainSchema.setBranchType2("01"); // 这里是个险用的考核
        // 这里是个险用的考核
        tLAAssessMainSchema.setBranchType(this.mLAAssessHistorySchema.getBranchType());
        // 这里是个险用的考核
        tLAAssessMainSchema.setBranchType2(this.mLAAssessHistorySchema.getBranchType2());
        tLAAssessMainSchema.setState("01");       // 1-考核确认未归属
        tLAAssessMainSchema.setOperator(this.mGlobalInput.Operator);
        tLAAssessMainSchema.setMakeDate(currentDate);
        tLAAssessMainSchema.setMakeTime(currentTime);
        tLAAssessMainSchema.setModifyDate(currentDate);
        tLAAssessMainSchema.setModifyTime(currentTime);
        mLAAssessMainSet.add(tLAAssessMainSchema);

        return true;
    }

    /**
     * 处理每个人的调整信息
     * @param pmLAAssessSchema LAAssessSchema
     * @return LAAssessSchema
     */
    private LAAssessSchema dealAssessAgeng(LAAssessSchema pmLAAssessSchema)
    {
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAssessSet tLAAssessSet = new LAAssessSet();
        LAAssessDB tLAAssessDB = new LAAssessDB();
        String tAgentGrade = "";   // 原职级
        String tCalAgentGrade = "";  // 调整职级

        //设置条件
        tLAAssessDB.setAgentCode(pmLAAssessSchema.getAgentCode());
        tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
        tLAAssessSet = tLAAssessDB.query();

        if(tLAAssessSet.size() < 1)
        {
            return null;
        }

        tLAAssessSchema = tLAAssessSet.get(1);

        //更新信息 (新职级)
        tAgentGrade = pmLAAssessSchema.getAgentGrade();
        tCalAgentGrade = pmLAAssessSchema.getCalAgentGrade();
        tLAAssessSchema.setCalAgentGrade(tCalAgentGrade);
        //重新判断并设置升降级标记
        if((tCalAgentGrade.compareTo(tAgentGrade)==0))
          tLAAssessSchema.setModifyFlag("02");
        else if(tCalAgentGrade.compareTo(tAgentGrade)>0)
          tLAAssessSchema.setModifyFlag("03");
        else if(tCalAgentGrade.compareTo(tAgentGrade)<0||tCalAgentGrade.equals("A00"))
          tLAAssessSchema.setModifyFlag("01");

        return tLAAssessSchema;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAgentEvaluateBL.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLAAssessSet.set((LAAssessSet)cInputData.getObjectByObjectName(
            "LAAssessSet",0));
        this.mLAAssessHistorySchema.setSchema((LAAssessHistorySchema)cInputData.
                getObjectByObjectName("LAAssessHistorySchema",0));

        this.mManageCom = mLAAssessHistorySchema.getManageCom();
        this.mIndexCalNo = mLAAssessHistorySchema.getIndexCalNo();
        this.mGradeSeries = mLAAssessHistorySchema.getAgentGrade();

        System.out.println(mManageCom +" / "+ mIndexCalNo + " / " + mGradeSeries
                + " / " + mLAAssessHistorySchema.getBranchType()
                + " / " + mLAAssessHistorySchema.getBranchType2());

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql)
    {
        System.out.println(sql);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0;
            st = conn.prepareStatement(sql);
            if (st == null)return 0;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
              if (!conn.isClosed()) {
                  conn.close();
              }
              try {
                  st.close();
                  rs.close();
              } catch (Exception ex2) {
                  ex2.printStackTrace();
              }
              st = null;
              rs = null;
              conn = null;
            } catch (Exception e) {}

        }
    }
}
