package com.sinosoft.lis.agentassess;

/**
 * <p>Title: 职称考核接口</p>
 * <p>Description: 职称考核接口，从考核公共接口继承</p>
 * <p>Copyright: Copyright (c) sinosoft 2004</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public interface PositionAssessInterface extends AssessInterface
{
}
