package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * 考核评估计算程序(考核调整)
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author Howie
 * @version 1.0
 */
public class LAActiveAgentAssessAdjUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public static void main(String args[])
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "mak001";
        tG.ComCode = "001";
        tG.ManageCom = "8694";

        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAssessSet tLAAssessSet = new LAAssessSet();
        tLAAssessSchema.setAgentCode("9401000109");
        tLAAssessSchema.setBranchAttr("86940000010102");
        tLAAssessSchema.setAgentGrade("A01");
        tLAAssessSchema.setCalAgentGrade("A02"); //计算职级
        tLAAssessSchema.setAgentGrade1("A02");   //确认职级
        tLAAssessSet.add(tLAAssessSchema);

        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tLAAssessSet);
        tVData.add("8694");
        tVData.add("200606");
        tVData.add("1");
        tVData.add("01");
        tVData.add("0");       //职级系列 AgentSeries

        LAActiveAgentAssessAdjUI tLAAgentAssessAdjUI = new LAActiveAgentAssessAdjUI();
        if(tLAAgentAssessAdjUI.submitData(tVData,""))
        {
            System.out.println("成功了！");
        }
        else
        {
            System.out.println("失败了！");
        }
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        LAActiveAgentAssessAdjBL tLAActiveAgentAssessAdjBL = new LAActiveAgentAssessAdjBL();
        if(!tLAActiveAgentAssessAdjBL.submitData(cInputData,cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAActiveAgentAssessAdjBL.mErrors);
            return false;
        }
        return true;
    }
}
