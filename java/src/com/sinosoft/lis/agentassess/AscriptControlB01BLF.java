package com.sinosoft.lis.agentassess;

/**
 * <p>Title: AscriptControlBLF类</p>
 * <p>Description: 归属的BL类的调用逻辑控制流</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */

import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.db.LARecomRelationDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AscriptControlB01BLF
{
    public CErrors mErrors = new CErrors();
    public GlobalInput mGlobalInput = new GlobalInput(); //操作员的信息
    //本次需要归属的所有考核结果的集合
    private LAAssessSet mLAAssessSet = new LAAssessSet();
    private LAAssessSchema mLAAssessSchema = new LAAssessSchema();

    private String mIndexCalNo; //本次归属	的考核计算代码YYYYMM
    private String mOldGradeEndDate; //被归属人员原来职级的结束日期
    private String mNewGradeStartDate; //被归属人员新职级的开始日期
    private String mManageCom;
    private String mBranchType;
    private String mBranchType2;
    private String mEdorNo; //转储号 一次归属业务中每个代理人产生一个转储号
    private LARearRelationBSet mLARearRelationBSet=new LARearRelationBSet();
    private LARearRelationSet mLARearRelationSetInsert=new LARearRelationSet();
    private LARearRelationSet mLARearRelationSetDelete=new LARearRelationSet();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private MMap mMapA = new MMap();
    private MMap mMapB = new MMap();
    
    public AscriptControlB01BLF() {
    }

    /**
     * 前台传入的数据
     * @param cInputData VData
     * @return boolean
     */
    public boolean getInputData(VData cInputData) {
        mManageCom = (String) cInputData.get(0);
        mIndexCalNo = (String) cInputData.get(1);
        mBranchType = (String) cInputData.get(2);
        mBranchType2 = (String) cInputData.get(3);
        mNewGradeStartDate = (String) cInputData.get(4);
        return true;
    }

    /**
     * 查询得到晋升到营业处的业务序列
     * @return boolean
     */
    private boolean getAssessResultA() {
        LAAssessDB tLAAssessDB = new LAAssessDB();
        String strSQL = "";
        strSQL =
                " select * from laassess where "
                + " indexcalno='" + mIndexCalNo + "' "
                + " and branchtype='" + mBranchType + "' "
                + " and branchtype2='" + mBranchType2 + "' "
                + " and state='2' and managecom like '" + mManageCom + "%' " 
                + " and AgentGrade <'B01' and AgentSeries='0' and agentgrade1 ='B02'";
        strSQL += "order by agentcode"; 
        try
        {
            System.out.println(strSQL);
            mLAAssessSet = tLAAssessDB.executeQuery(strSQL);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return true;
    }

    /**
     * 查询得到晋升到营业处的营业组经理
     * @return boolean
     */
    private boolean getAssessResultB() {
        LAAssessDB tLAAssessDB = new LAAssessDB();
        String strSQL = "";
        strSQL =
                " select * from laassess where "
                + " indexcalno='" + mIndexCalNo + "' "
                + " and branchtype='" + mBranchType + "' "
                + " and branchtype2='" + mBranchType2 + "' "
                + " and state='2' and managecom like '" + mManageCom + "%' " 
                + " and AgentGrade ='B01' and AgentSeries='1' and agentgrade1 ='B02'";
        strSQL += "order by agentcode"; 
        try
        {
            System.out.println(strSQL);
            mLAAssessSet = tLAAssessDB.executeQuery(strSQL);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return true;
    }
    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    public boolean SubmitData(VData cInputData) {
        if (!getInputData(cInputData)) {
            return false;
        }
        //对于营业组团险代码进行更新
        if(!upDateAgentGroup2())
        {
        	return false;
        }
       
      //对于从营业组经理晋升到处经理且其推荐人为组经理（同时晋升片经理）的育成关系的人进行删除
        if (!DealDataB())
        {
            return false;
        }
        VData tInputDataB = new VData();
        mMapB.put(mLARearRelationBSet,"INSERT");
        mMapB.put(mLARearRelationSetDelete,"DELETE");
        tInputDataB.add(mMapB);
        
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start AscriptControlB01BLF.java submit...");
        if (!tPubSubmit.submitData(tInputDataB, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AscriptControlB01BLF";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //对于从业务序列晋升到处经理且其推荐人为组经理（同时晋升片经理）的育成关系的人进行处理
        if (!DealDataA())
        {
            return false;
        }
        VData tInputDataA = new VData();
        mMapA.put(mLARearRelationBSet,"INSERT");
        mMapA.put(mLARearRelationSetDelete,"DELETE");
        mMapA.put(mLARearRelationSetInsert,"INSERT");
        tInputDataA.add(mMapA);
        
        PubSubmit tPubSubmitA = new PubSubmit();
        System.out.println("Start AscriptControlB01BLF.java submit...");
        if (!tPubSubmitA.submitData(tInputDataA, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AscriptControlB01BLF";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
  //归属处理
    private boolean DealDataA() {
    	//得到 本次考核从业务序列晋升到营业处经理的业务员
        if (!getAssessResultA()) {
            return false;
        }
        if (mLAAssessSet == null || mLAAssessSet.size() <= 0) {
            return true;
        }
        for (int i = 1; i <= mLAAssessSet.size(); i++) {
            mLAAssessSchema = mLAAssessSet.get(i).getSchema();
            String tAgentCode = mLAAssessSchema.getAgentCode();
			String tRecomAgentSQL = "select * from larecomrelation a where 1=1 and agentcode ='"+tAgentCode+"'"
					//本次考核：推荐人为营业组经理，且也晋升为处经理
					+" and recomagentcode in (select agentcode from laassess c where c.branchtype='1' and branchtype2='01' and   managecom ='"+this.mManageCom+"' and IndexCalNo ='"+this.mIndexCalNo+"' and state ='2' and AgentGrade ='B01' and agentgrade1 ='B02')" 
					+" order by RecomGens fetch first 1 rows only "; 
			LATreeDB  tLATreeDB = new LATreeDB();
			tLATreeDB.setAgentCode(tAgentCode);
			LATreeSet tLATreeSet = new LATreeSet();
			tLATreeSet = tLATreeDB.query();
			LARecomRelationDB tLARearRelationDB = new LARecomRelationDB();
			LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
			tLARecomRelationSet = tLARearRelationDB.executeQuery(tRecomAgentSQL);
			if(tLARecomRelationSet == null || tLARecomRelationSet.size()<=0)
			{
				continue;
			}
			//删除原来的育成关系 
			BreakAgentRear("",tAgentCode);
			String tRecomAgentCode = tLARecomRelationSet.get(1).getRecomAgentCode();
			//形成新的育成关系
			createAgentRear(tRecomAgentCode,tLATreeSet.get(1));
        }
        return true;
    }
    
    //归属处理
    private boolean DealDataB() {
    	//得到 本次考核从营业组经理晋升到营业处经理的业务员
        if (!getAssessResultB()) {
            return false;
        }
        if (mLAAssessSet == null || mLAAssessSet.size() <= 0) {
            return true;
        }
        //删除营业组推荐 营业组经理，同时晋升形成的育成关系
        for (int i = 1; i <= mLAAssessSet.size(); i++) {
            mLAAssessSchema = mLAAssessSet.get(i).getSchema();
            String tAgentCode = mLAAssessSchema.getAgentCode();
            String tRecomAgentSQL = "select * from larecomrelation a where 1=1 and agentcode ='"+tAgentCode+"'"
			//本次考核：推荐人为营业组经理，且也晋升为处经理
			+" and recomagentcode in (select agentcode from laassess c where c.branchtype='1' and branchtype2='01' and   managecom ='"+this.mManageCom+"' and IndexCalNo ='"+this.mIndexCalNo+"' and state ='2' and AgentGrade ='B01' and agentgrade1 ='B02')" ;
            LARecomRelationDB tLARearRelationDB = new LARecomRelationDB();
			LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
			tLARecomRelationSet = tLARearRelationDB.executeQuery(tRecomAgentSQL);
			if(tLARecomRelationSet == null || tLARecomRelationSet.size()<=0)
			{
				continue;
			}
			for(int j =1;j<=tLARecomRelationSet.size();j++)
			{
				String tRecomAgentCode = tLARecomRelationSet.get(j).getRecomAgentCode();
				//删除原来的育成关系 
				BreakAgentRear(tRecomAgentCode,tAgentCode);
			}
        }
        return true;
    }

    //对于每次考核中，营业组经理职级的人员进行维护agentgroup2
    public boolean upDateAgentGroup2()
    {
    	//晋升营业组后其下的直接推荐人属于其营业组下
    	String sql1 =" update latree a  set agentgroup2 = " +
    			"(select agentgroup2 from latree where agentgrade ='B01' and agentcode =(select recomagentcode from larecomrelation where a.agentcode = agentcode and  recomlevel='01' and RecomGens=1  and RecomFlag='1')) " +
    			" where branchtype='1' and branchtype2='01' and agentgrade <'B01' " +
				" and agentcode in " +
				"( select agentcode from larecomrelation where recomagentcode in(" +
				"select agentcode from laassess where managecom ='"+this.mManageCom+"' and IndexCalNo ='"+this.mIndexCalNo+"' and state ='2' and AgentGrade1 ='B01'))" 
				;
    	//从营业组经理降级后，其下的组员营业组编码清空
    	String sql2 =" update latree a  set agentgroup2 = ''" +
				" where  branchtype='1' and branchtype2='01' and agentgroup2 is not null and agentgroup2 <>'' and   agentgrade <'B01' " +
				" and not exists( select 1 from latree where a.agentgroup2 = agentgroup2 and agentgrade='B01' )";
//				"and not exists(select 1 from larecomrelation b  where a.agentcode = b.agentcode and  recomlevel='01' and RecomGens=1  and RecomFlag='1' and b.recomagentcode  " +
//				"in(select  c.agentcode  from laassess c where c.branchtype='1' and branchtype2='01' and   managecom ='"+this.mManageCom+"' and IndexCalNo ='"+this.mIndexCalNo+"' and state ='2' and AgentGrade ='B01')  ) ";
    	//如果职级大于B01，即为B02,B11，营业组编码为空
    	String sql3 = "update latree a set agentgroup2 ='' where agentgrade in ('B02','B11') and managecom ='"+this.mManageCom+"' and agentgroup2 is not null and trim(agentgroup2)<>'' ";
    	ExeSQL tExeSQL = new ExeSQL();
    	if(!tExeSQL.execUpdateSQL(sql1))
    	{
    		CError.buildErr(this, "晋升营业组经理更新营业组编码报错！");
    		return false;
    	}
    	if(!tExeSQL.execUpdateSQL(sql2))
    	{
    		CError.buildErr(this, "营业组经理降级更新营业组编码报错！");
    		return false;
    	}
    	if(!tExeSQL.execUpdateSQL(sql3))
    	{
    		CError.buildErr(this, "营业处经理及营业区经理更新营业组编码报错！");
    		return false;
    	}
    	return true;
    }
    /*
    断开血缘关系
    参数: cLATreeDB-断绝血缘关系的发起方的行政信息  ,向上的全部断开，向下的只断开同级别的关系，
    不同级别关系不变也不需要修改
    */
   public boolean BreakAgentRear(String cRearAgentCode,String cAgentCode)
   {
       LARearRelationDB tLARearRelationDB=new LARearRelationDB();
       LARearRelationSchema tLARearRelationSchema;
       LARearRelationSet tLARearRelationSet=new LARearRelationSet();
       String SQLstr;
       //向下的只断开同级别的关系
       SQLstr="Select * from larearrelation where agentcode='"+cAgentCode+"'  " ;
       if(!"".equals(cRearAgentCode))
       {
    	   SQLstr +=" and rearagentcode ='"+cRearAgentCode+"'";
       }
       SQLstr +="with ur ";
       System.out.println("BreakAgentRear中查询所有现存关系的SQL： "+ SQLstr);
       tLARearRelationSet.add(tLARearRelationDB.executeQuery(SQLstr));
       
       //下面要循环备份
       if(tLARearRelationSet.size()>0)
       {
           for(int i=1;i<=tLARearRelationSet.size();i++)
           {
               //tLARearRelationDB=new LARearRelationDB();
               tLARearRelationSchema=new LARearRelationSchema();
               tLARearRelationSchema.setSchema((LARearRelationSchema)
                                           tLARearRelationSet.get(i));
               if(!BackupRear(tLARearRelationSchema.getSchema()))
               {
                   DealErr("备份血缘关系失败！","AscriptRelationOperateBL","BreakAgentRear");
                   return false;
               }
               mLARearRelationSetDelete.add(tLARearRelationSchema.getSchema());
           }
       }
       return true;
   }
   private boolean BackupRear(LARearRelationSchema cLARearRelationSchema)
   {
       Reflections tReflections=new Reflections();
       LARearRelationBSchema tLARearRelationBSchema=new LARearRelationBSchema();
       LARearRelationSchema tLARearRelationSchema=new LARearRelationSchema();
       tLARearRelationSchema=cLARearRelationSchema;
       tReflections.transFields(tLARearRelationBSchema,tLARearRelationSchema);
       String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20); //一个人生成一个转储号
       tLARearRelationBSchema.setEdorNo(tEdorNo);
       //推荐人为营业组经理，且两人同时考核晋升为处经理
       tLARearRelationBSchema.setEdorType("11");
       tLARearRelationBSchema.setEndDate(mOldGradeEndDate);
       tLARearRelationBSchema.setOperator2(cLARearRelationSchema.getOperator());
       tLARearRelationBSchema.setMakeDate2(cLARearRelationSchema.getMakeDate());
       tLARearRelationBSchema.setModifyDate2(cLARearRelationSchema.getModifyDate());
       tLARearRelationBSchema.setMakeDate2(cLARearRelationSchema.getMakeTime());
       tLARearRelationBSchema.setModifyTime2(cLARearRelationSchema.getModifyTime());
       tLARearRelationBSchema.setMakeDate(currentDate);
       tLARearRelationBSchema.setMakeTime(currentTime);
       tLARearRelationBSchema.setModifyDate(currentDate);
       tLARearRelationBSchema.setModifyTime(currentTime);
       tLARearRelationBSchema.setOperator(mGlobalInput.Operator);

       mLARearRelationBSet.add(tLARearRelationBSchema);
      
       return true;
   }
   private void DealErr(String cMsg,String cMName,String cFName)
   {
           CError tCError=new CError();
           tCError.moduleName=cMName;
           tCError.functionName=cFName;
           tCError.errorMessage=cMsg;
           mErrors.addOneError(tCError);
   }
   /*
   建立、断裂血缘关系,只能由被育成人或者被增部人发起
   参数: cLATreeDB-建立关系的发起方的行政信息
   */
  public boolean createAgentRear(String cRearAgentCode,LATreeSchema cLATreeSchema)  
  {
      LARearRelationDB tLARearRelationDB = new LARearRelationDB();
      LARearRelationDB tLARearRelationDBIndirect = new LARearRelationDB(); //直接血缘关系
      String tFatherCode = cRearAgentCode;
      ExeSQL tExeSQL = new ExeSQL();
      tLARearRelationDB.setAgentCode(cLATreeSchema.getAgentCode());
      tLARearRelationDB.setRearedGens(1);
      tLARearRelationDB.setRearLevel("01");
      tLARearRelationDB.setstartDate(mNewGradeStartDate);
      tLARearRelationDB.setEndDate("");
      tLARearRelationDB.setRearFlag("1");
      tLARearRelationDB.setAgentGroup(cLATreeSchema.getAgentGroup());
      tLARearRelationDB.setRearAgentCode(tFatherCode);
      tLARearRelationDB.setMakeDate(currentDate);
      tLARearRelationDB.setMakeTime(currentTime);
      tLARearRelationDB.setModifyDate(currentDate);
      tLARearRelationDB.setModifyTime(currentTime);
      tLARearRelationDB.setOperator(mGlobalInput.Operator);
      tLARearRelationDB.setRearCommFlag("1");
      tLARearRelationDB.setRearStartYear(1);
      mLARearRelationSetInsert.add(tLARearRelationDB.getSchema());

      //下面是间接关系
      //ExeSQL tExeSQL = new ExeSQL();
      SSRS rearR = new SSRS();
      String SQLstr = "select RearAgentCode,RearLevel,RearedGens from larearrelation where agentcode='"
               + tFatherCode + "' and rearlevel='"
               + tLARearRelationDB.getRearLevel() +
               "' and rearflag='1' order by rearedgens desc";
      System.out.println("CreateAgentRear中的查找间接关系的SQL: " + SQLstr);
      rearR = tExeSQL.execSQL(SQLstr);
      if (rearR.MaxRow > 0) {
          for (int i = 1; i <= rearR.MaxRow; i++) {
              tLARearRelationDBIndirect.setAgentCode(cLATreeSchema.
                      getAgentCode());
              tLARearRelationDBIndirect.setRearLevel(rearR.GetText(i,
                      2) /*getRearLevel()*/); //
              tLARearRelationDBIndirect.setRearedGens(String.valueOf(
                      Integer.parseInt(rearR.GetText(i, 3))
                      /*getRearedGens()*/+ 1)); //
              //当前检查的是2代关系,就用提奖金
              tLARearRelationDBIndirect.setRearCommFlag("1");
              tLARearRelationDBIndirect.setRearStartYear(1);

              tLARearRelationDBIndirect.setRearAgentCode(rearR.
                      GetText(i, 1));
              tLARearRelationDBIndirect.setstartDate(
                      mNewGradeStartDate);
              tLARearRelationDBIndirect.setEndDate("");
              tLARearRelationDBIndirect.setRearFlag("1");
              tLARearRelationDBIndirect.setAgentGroup(cLATreeSchema.
                      getAgentGroup());
              tLARearRelationDBIndirect.setMakeDate(currentDate);
              tLARearRelationDBIndirect.setMakeTime(currentTime);
              tLARearRelationDBIndirect.setModifyDate(currentDate);
              tLARearRelationDBIndirect.setModifyTime(currentTime);
              tLARearRelationDBIndirect.setOperator(mGlobalInput.Operator);
              mLARearRelationSetInsert.add(tLARearRelationDBIndirect.getSchema());
          }
      }
      return true;
  }
    public static void main(String[] args)
    {

        String manageCom = "86350300";
        String indexcalno = "201512";
        String branchtype = "1";
        String branchtype2 = "01";
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";

        System.out.println("B11".substring(0));
        VData input = new VData();
        input.add(manageCom);
        input.add(indexcalno);
        input.add(branchtype);
        input.add(branchtype2);
        input.add("2015-10-1");
        AscriptControlB01BLF tAscriptControlB01BLF = new AscriptControlB01BLF();
        tAscriptControlB01BLF.SubmitData(input);
    }
}
