package com.sinosoft.lis.agentassess;

import java.sql.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.DBConnPool;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description: 考核手工确认BL类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @author Howie
 * @version 1.0
 */
public class LAAgentAssessARiskBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private MMap mMap = new MMap();
	private LAAssessSet mLAAssessSet = new LAAssessSet();
	private LAAssessSet mLAAssessOutSet = new LAAssessSet();
	private LAAssessHistorySchema mLAAssessHistorySchema = new LAAssessHistorySchema();
	private String mManageCom = "";
	private String mIndexCalNo = "";
	private String currentDate = PubFun.getCurrentDate();
	private String currentTime = PubFun.getCurrentTime();

	/**
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		// 处理前进行验证
		if (!check()) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();
		tPubSubmit.submitData(mInputData, "");
		// 如果有需要处理的错误，则返回
		if (tPubSubmit.mErrors.needDealError()) {
			// @@错误处理
			// this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAAgentAssessARiskBL";
			tError.functionName = "submitDat";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 验证方法
	 * 
	 * @return boolean
	 */
	private boolean check() {
		if (AgentPubFun.ConverttoYM(currentDate).compareTo(mIndexCalNo) <= 0) {
			// @@错误处理
			CError.buildErr(this, "不能进行当月及以后的考核确认操作!");
			return false;
		}
		LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
		LAAssessMainSet tLAAssessMainSet = new LAAssessMainSet();
		String strSQL = "select * from laassessmain where  assesstype='00' and indexcalno='"
				+ mIndexCalNo
				+ "' and managecom like '"
				+ mManageCom
				+ "%' and state='0' ";
		strSQL += " and agentgrade = 'A01' ";
		System.out.println(strSQL);
		tLAAssessMainSet = tLAAssessMainDB.executeQuery(strSQL);
		if (tLAAssessMainSet.size() <= 0) {
			// 再查一下看看是不是已经确认了
			strSQL = "select * from laassessmain where assesstype='00' and indexcalno='"
					+ mIndexCalNo
					+ "' and managecom like '"
					+ mManageCom
					+ "%' and state>'0' ";
			strSQL += " and agentgrade = 'A01' ";

			tLAAssessMainSet = tLAAssessMainDB.executeQuery(strSQL);
			if (tLAAssessMainSet.size() <= 0) {
				CError.buildErr(this, "查询机构" + mManageCom + "下的考评主信息失败!");
			} else {
				CError.buildErr(this, "机构" + mManageCom + "下的考评信息已确认过!");
				return false;
			}
		}
		return true;
	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mMap.put(this.mLAAssessOutSet, "UPDATE");
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError.buildErr(this, "在准备往后层处理所需要的数据时出错。");
			return false;
		}
		return true;
	}

	/**
	 * 业务处理方法
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		LAAssessSet tLAAssessDealSet = new LAAssessSet();
		LAAssessSchema tLAAssessSchema = new LAAssessSchema();
		int tAgentCount = mLAAssessSet.size(); // 需要处理的人数
		// 循环处理每个人的考核信息
		for (int i = 1; i <= tAgentCount; i++) {
			tLAAssessSchema = dealAssessAgent(mLAAssessSet.get(i).getSchema());
			// 如果更新信息成功
			if (tLAAssessSchema != null) {
				tLAAssessDealSet.add(tLAAssessSchema);
			} else {
				CError tError = new CError();
				tError.moduleName = "LAAgentAssessARiskBL";
				tError.functionName = "dealData";
				tError.errorMessage = "更新个人考核信息失败！";
				System.out.println("更新个人考核信息失败！");
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		mLAAssessOutSet = tLAAssessDealSet;

		return true;
	}

	/**
	 * 处理每个人的调整信息
	 * 
	 * @param pmLAAssessSchema
	 *            LAAssessSchema
	 * @return LAAssessSchema
	 */
	private LAAssessSchema dealAssessAgent(LAAssessSchema pmLAAssessSchema) {
		// 设置条件
		LATreeSchema tLATreeSchema = new LATreeSchema();
		LATreeDB tLATreeDB = new LATreeDB();
		tLATreeDB.setAgentCode(pmLAAssessSchema.getAgentCode());
		tLATreeDB.getInfo();
		tLATreeSchema = tLATreeDB.getSchema();

		LAAgentSchema tLAAgentSchema = new LAAgentSchema();
		LAAgentDB tLAAgentDB = new LAAgentDB();
		tLAAgentDB.setAgentCode(pmLAAssessSchema.getAgentCode());
		tLAAgentDB.getInfo();
		tLAAgentSchema = tLAAgentDB.getSchema();

		LAAssessSchema tLAAssessSchema = new LAAssessSchema();
		LAAssessSet tLAAssessSet = new LAAssessSet();
		LAAssessDB tLAAssessDB = new LAAssessDB();
		String tAgentGrade = ""; // 原职级
		String tAgentGrade1 = ""; // 调整职级

		tLAAssessDB.setAgentCode(pmLAAssessSchema.getAgentCode());
		tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
		tLAAssessSet = tLAAssessDB.query();
		// 更新考核结果
		if (tLAAssessSet.size() != 0) {
			tLAAssessSchema = tLAAssessSet.get(1).getSchema();
		}
		// 插入新的考核结果
		else { // 插入新的考核结果
			tLAAssessSchema.setAgentCode(pmLAAssessSchema.getAgentCode());
			tLAAssessSchema.setIndexCalNo(this.mIndexCalNo);
			tLAAssessSchema.setAgentGroup(tLATreeSchema.getAgentGroup());

			if ("".equals(tLAAssessSchema.getBranchAttr())
					|| tLAAssessSchema.getBranchAttr() == null) {
				LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
				tLABranchGroupDB.setAgentGroup(tLATreeSchema.getAgentGroup());
				tLABranchGroupDB.getInfo();
				String tBranchAttr = tLABranchGroupDB.getSchema()
						.getBranchAttr();
				tLAAssessSchema.setBranchAttr(tBranchAttr);
			}
			tLAAssessSchema.setManageCom(tLATreeSchema.getManageCom());
			tLAAssessSchema.setBranchType(tLATreeSchema.getBranchType());
			tLAAssessSchema.setBranchType2(tLATreeSchema.getBranchType2());
			tLAAssessSchema.setFirstAssessFlag("1");
			tLAAssessSchema.setStandAssessFlag("0"); // 非正常考核
			tLAAssessSchema.setMakeDate(this.currentDate);
			tLAAssessSchema.setMakeTime(this.currentTime);
			tLAAssessSchema.setOperator(mGlobalInput.Operator);
		}

		// 更新信息 (新职级)
		tAgentGrade = pmLAAssessSchema.getAgentGrade();
		tAgentGrade1 = pmLAAssessSchema.getCalAgentGrade();
		tLAAssessSchema.setAgentGrade(tAgentGrade);
//		tLAAssessSchema.setAgentSeries(tLATreeSchema.getAgentSeries());
//		tLAAssessSchema.setCalAgentGrade(tCalAgentGrade);
//		tLAAssessSchema.setCalAgentSeries(tLATreeSchema.getAgentSeries());
		tLAAssessSchema.setAgentGrade1(tAgentGrade1);
//		tLAAssessSchema.setAgentSeries1(tLATreeSchema.getAgentSeries()); // 系列不变
		tLAAssessSchema.setState("1"); // 1-考核确认未归属
		tLAAssessSchema.setConfirmer(mGlobalInput.Operator);
		tLAAssessSchema.setConfirmDate(this.currentDate);
		tLAAssessSchema.setModifyDate(this.currentDate);
		tLAAssessSchema.setModifyTime(this.currentTime);
		// 重新判断并设置升降级标记
		if ((tAgentGrade1.compareTo(tAgentGrade) == 0)) {
			tLAAssessSchema.setModifyFlag("02");
		} else if (tAgentGrade1.compareTo(tAgentGrade) > 0) {
			tLAAssessSchema.setModifyFlag("03");

		} else if (tAgentGrade1.compareTo(tAgentGrade) < 0) {
			tLAAssessSchema.setModifyFlag("01");
		}

		return tLAAssessSchema;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		System.out.println("Begin LAAgentAssessARiskBL.getInputData.........");
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		this.mLAAssessSet.set((LAAssessSet) cInputData.getObjectByObjectName(
				"LAAssessSet", 0));
		this.mLAAssessHistorySchema
				.setSchema((LAAssessHistorySchema) cInputData
						.getObjectByObjectName("LAAssessHistorySchema", 0));
		this.mManageCom = mLAAssessHistorySchema.getManageCom();
		this.mIndexCalNo = mLAAssessHistorySchema.getIndexCalNo();

		System.out.println(mManageCom + " / " + mIndexCalNo + " / "
				+ mLAAssessHistorySchema.getBranchType() + " / "
				+ mLAAssessHistorySchema.getBranchType2());

		if (mGlobalInput == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAgentAssessARiskBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的信息！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (mLAAssessSet.size() == 0) {
			CError.buildErr(this, "没有要处理的数据！");
			return false;
		}
		return true;
	}

	/**
	 * 执行SQL文查询结果
	 * 
	 * @param sql
	 *            String
	 * @return int
	 */
	private int execQuery(String sql) {
		System.out.println(sql);
		Connection conn;
		conn = null;
		conn = DBConnPool.getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if (conn == null) {
				return 0;
			}
			st = conn.prepareStatement(sql);
			if (st == null) {
				return 0;
			}
			rs = st.executeQuery();
			if (rs.next()) {
				return rs.getInt(1);
			}
			return 0;
		} catch (Exception ex) {
			ex.printStackTrace();
			return -1;
		} finally {
			try {
				st.close();
				rs.close();
				conn.close();
				st = null;
				rs = null;
				conn = null;
			} catch (Exception e) {
			}
		}
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.ManageCom = "86";
		tG.Operator = "001";

		LAAssessSchema aLAAssessSchema = new LAAssessSchema();
		LAAssessSet aLAAssessSet = new LAAssessSet();
		aLAAssessSchema.setAgentCode("1102000126");
		aLAAssessSchema.setBranchAttr("8611000001");
		aLAAssessSchema.setAgentGrade("D01");
		// aLAAssessSchema.setAgentGrade1("D01");
		aLAAssessSchema.setCalAgentGrade("D00");

		aLAAssessSet.add(aLAAssessSchema);

		LAAssessHistorySchema aLAAssessHistorySchema = new LAAssessHistorySchema();
		aLAAssessHistorySchema.setManageCom("8611");
		aLAAssessHistorySchema.setIndexCalNo("200604");
		aLAAssessHistorySchema.setBranchType("2");
		aLAAssessHistorySchema.setBranchType2("01");

		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add(tG);
		tVData.addElement(aLAAssessHistorySchema);
		tVData.addElement(aLAAssessSet);
		System.out.println("add over");

		LAAgentAssessARiskBL aLAAgentAssessARiskBL = new LAAgentAssessARiskBL();
		boolean tag = aLAAgentAssessARiskBL.submitData(tVData, "");
		if (tag) {
			System.out.println("ok");
		} else {
			System.out.println("fail");
		}
	}
}
