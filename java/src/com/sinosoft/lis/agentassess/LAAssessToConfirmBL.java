package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.agent.ALADimissionAppUI;
import com.sinosoft.lis.agent.BankDimissionAppBL;
import com.sinosoft.lis.agent.BankDimissionBL;
import com.sinosoft.lis.agentbranch.LABranchGroup;

/**
 * <p>Title:银代业务经理考核确认和归属 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAAssessToConfirmBL {
    public LAAssessToConfirmBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 提交数据的容器 */
    private MMap mMap = new MMap();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSet mLAAssessSet = new LAAssessSet(); //传入前台送入的考核记录
    private LAAssessSet mLAAssessOutSet = new LAAssessSet(); //考核确认后的结果
    

    private LAAssessHistorySchema mLAAssessHistorySchema = new
            LAAssessHistorySchema();


    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LATreeSet groupLATreeSet = new LATreeSet(); //降级主管下的业务员
    private LAAgentSet groupLAAgentSet = new LAAgentSet();//降级主管下的业务员
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();//降级主管所管理的团队
    
    private String mEvaluateDate = ""; // 异动时间
    private String mManageCom = ""; // 管理机构
    private String mIndexCalNo = ""; // 考核序列号
    private String mGradeSeries = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    public static void main(String args[]) {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "001";
        LAAssessHistorySchema aLAAssessHistorySchema = new
                LAAssessHistorySchema();
        // 准备数据
        aLAAssessHistorySchema.setManageCom("8611");
        aLAAssessHistorySchema.setIndexCalNo("200612");
        aLAAssessHistorySchema.setBranchType("2");
        aLAAssessHistorySchema.setBranchType2("01");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(aLAAssessHistorySchema);
        System.out.println("add over");
        LAAssessToConfirmBL m = new LAAssessToConfirmBL();
        boolean tag = m.submitData(tVData, "");
        if (tag) {
            System.out.println("ok");
        } else {
            System.out.println("fail");
        }
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //取得异动时间
        if (!getEvaluateDate()) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        //统一提交
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        if (tPubSubmit.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecGrpYearBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 人员新职级归属完毕以后更改考核主表的状态
     * @return boolean
     */
    private boolean dealLAAssessMain() {
        return true;
    }


    /**
     * 取得异动时间(考核期次月一号)
     * @return boolean
     */
    private boolean getEvaluateDate() {
        String tSQL = "";
        tSQL = "select date(substr('" + mIndexCalNo + "',1,4)||'-'|| substr(";
        tSQL += "'" + mIndexCalNo + "',5,2)||'-01') + 1 month from dual";

        ExeSQL tExeSQL = new ExeSQL();
        // 取得异动时间
        mEvaluateDate = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaBankDecBL";
            tError.functionName = "getEvaluateDate";
            tError.errorMessage = "获取异动日期失败！";
            System.out.println("获取异动日期失败！");
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("获得异动日期：" + mEvaluateDate);
        return true;
    }

    /**
    *
    * @return boolean
    */
   private boolean checkPer(LAAssessSchema aLAAssessSchema) {
       LAAgentDB tLAAgentDB = new LAAgentDB();
       LAAgentSchema tLAAgentSchema = new LAAgentSchema();
       tLAAgentDB.setAgentCode(aLAAssessSchema.getAgentCode());
       tLAAgentDB.getInfo();
       tLAAgentSchema = tLAAgentDB.getSchema();
       if (AgentPubFun.ConverttoYM(tLAAgentSchema.getEmployDate()).compareTo(
               mIndexCalNo) > 0) {
           CError.buildErr(this,
                           "业务员" + aLAAssessSchema.getAgentCode() +
                           "入司年月不能大于考核归属年月!");
           return false;
       }
       String
               tSQL = "select count(*) from laassess where indexcalno>='"
                      + mIndexCalNo + "' and agentcode='"
                      + aLAAssessSchema.getAgentCode() + "' and state='02' ";
       if (execQuery(tSQL) > 0) {
           CError.buildErr(this,
                           "业务员" + aLAAssessSchema.getAgentCode() + "在" +
                           mIndexCalNo +
                           "月及以后已经考核归属过!");
           return false;
       }
//
//               String  bSQL = "select state from lawagehistory where branchtype='"
//                      + mLAAssessHistorySchema.getBranchType()
//                      + "' and branchtype2='"
//                      +  mLAAssessHistorySchema.getBranchType2()
//                      + "' and wageno='"
//                      + mIndexCalNo
//                      + "' and managecom like '"
//                      + mManageCom + "%' and state='14' ";
//               try {
//                   if (execQuery(bSQL) == 0) {
//                       // @@错误处理
//                       CError tError = new CError();
//                       tError.moduleName = "AgentCalExaBank";
//                       tError.functionName = "check";
//                       tError.errorMessage = "指定机构，已进行了薪资计算！";
//                       System.out.println("指定机构，已进行了薪资计算！。");
//                       this.mErrors.addOneError(tError);
//
//                       return false;
//                   }
//               } catch (Exception e) {
//                   System.out.println("未进行薪资计算_1");
//
//               }


       System.out.println("验证通过！");
       return true;
   }
    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        mInputData.clear();
        mMap.put(mLAAssessOutSet, "UPDATE");
        mMap.put(mLATreeSet, "UPDATE");
        mMap.put(mLATreeBSet, "INSERT");
        mMap.put(mLABranchGroupSet, "UPDATE");
        mMap.put(groupLATreeSet, "UPDATE");
        mInputData.add(mMap);
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */

    private boolean dealData() {

    	LAAssessDB  mLAAssessDB = new LAAssessDB();
    	mLAAssessDB.setManageCom(this.mManageCom);
    	mLAAssessDB.setIndexCalNo(this.mIndexCalNo);
    	mLAAssessDB.setBranchType(this.mBranchType);
    	mLAAssessDB.setBranchType2(this.mBranchType2);
    	mLAAssessDB.setState("01");
    	String strSql = "select * from laassess where indexcalno = '"+this.mIndexCalNo+"' and branchtype = '"+this.mBranchType+"'"
    				  +" and branchtype2 = '"+this.mBranchType2+"' and managecom like '"+this.mManageCom+"%' and state = '01'";
    	mLAAssessSet = mLAAssessDB.executeQuery(strSql);  
    	
    	
    	
//        LAAgentSet tLAAgentSet = new LAAgentSet();
//        LATreeSet tLATreeSet = new LATreeSet();
//        LATreeBSet tLATreeBSet = new LATreeBSet();
    	//循环处理每个人的考核信息
    	BankDimissionAppBL tBankDimissionAppBL = new BankDimissionAppBL();
    	BankDimissionBL tBankDimissionBL = new BankDimissionBL();
    	LADimissionSchema tLADimissionSchema = new LADimissionSchema();
    	for (int i = 1; i <= mLAAssessSet.size(); i++) {

        	LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        	tLAAssessSchema = mLAAssessSet.get(i);
        	
    		tLAAssessSchema.setState("02");
            tLAAssessSchema.setConfirmer(mGlobalInput.Operator);
            tLAAssessSchema.setConfirmDate(this.currentDate);
            tLAAssessSchema.setModifyDate(this.currentDate);
            tLAAssessSchema.setModifyTime(this.currentTime);
            mLAAssessOutSet.add(tLAAssessSchema);
        	//如果考核职级为清退，调用离职登记.离职确认接口,如不满足离职条件，则不做组织归属.
        	if("F00".equals(tLAAssessSchema.getCalAgentGrade())){
	    		tLADimissionSchema.setAgentCode(tLAAssessSchema.getAgentCode());
	    		tLADimissionSchema.setDepartTimes("1");
	    		tLADimissionSchema.setApplyDate(this.currentDate);
	    		tLADimissionSchema.setDepartDate(this.currentDate);
	    		tLADimissionSchema.setBranchType("3");
	    		tLADimissionSchema.setBranchType2("01");
	    		tLADimissionSchema.setDepartRsn("0");//离职原因，0代表考核
	    		VData cInputData = new VData();  		
	    		cInputData.addElement(tLADimissionSchema);
	    		cInputData.add(this.mGlobalInput);
	    		
	    		if(!tBankDimissionAppBL.submitData(cInputData, "INSERT||MAIN")){
	    			System.out.println("业务员"+tLAAssessSchema.getAgentCode()+"离职登记失败");
	    			continue;
	    		}	    
	    		if(!tBankDimissionBL.submitData(cInputData, "INSERT||MAIN")){
	    			System.out.println("业务员"+tLAAssessSchema.getAgentCode()+"离职确认失败");
	    			continue;
	    		}
	    		System.out.println("F00不需要改变行政职级");
	    		continue;
        	}      
            if (!dealAgentEvaluate(tLAAssessSchema)) {
                CError.buildErr(this, "进行人员归属操作时失败！");
                return false;
            }

    	}
    	
    	return true;
    }
    /**
     * 处理每个人的调整信息
     * @param pmLAAssessSchema LAAssessSchema
     * @return LAAssessSchema
     */
    private LAAssessSchema dealAssessAgent(LAAssessSchema pmLAAssessSchema) {
        //设置条件
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(pmLAAssessSchema.getAgentCode());
        tLATreeDB.getInfo();
        LATreeSchema tLATreeSchema = tLATreeDB.getSchema();

        LAAssessDB tLAAssessDB = new LAAssessDB();
        tLAAssessDB.setAgentCode(pmLAAssessSchema.getAgentCode());
        tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
        LAAssessSet tLAAssessSet = tLAAssessDB.query();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        //更新考核结果
        if (tLAAssessSet.size() != 0) {
            tLAAssessSchema = tLAAssessSet.get(1).getSchema();
        }

        //更新状态 (新职级)
        tLAAssessSchema.setState("02"); //01-调整确认 02-考核确认（类似于个险组织归属）
        tLAAssessSchema.setConfirmer(mGlobalInput.Operator);
        tLAAssessSchema.setConfirmDate(this.currentDate);
        tLAAssessSchema.setModifyDate(this.currentDate);
        tLAAssessSchema.setModifyTime(this.currentTime);

        return tLAAssessSchema;
    }

    /**
     * 对每个参加考核的人员进行逐个处理
     * @param pmLAAssessSchema LAAssessSchema
     * @return boolean
     */
    private boolean dealAgentEvaluate(LAAssessSchema pmLAAssessSchema) {

        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(pmLAAssessSchema.getAgentCode());
        tLATreeDB.getInfo();
        LATreeSchema tLATreeSchema = tLATreeDB.getSchema();
        //备份行政信息
        LATreeBSchema tLATreeBSchema = new LATreeBSchema();
        String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        Reflections tRefs = new Reflections();
        tRefs.transFields(tLATreeBSchema, tLATreeSchema);
        tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
        tLATreeBSchema.setOperator(mGlobalInput.Operator);
        tLATreeBSchema.setEdorNO(tEdorNo);
        tLATreeBSchema.setRemoveType("01"); //手工考核确认
        tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
        tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
        tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
        tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
        tLATreeBSchema.setMakeDate(currentDate);
        tLATreeBSchema.setMakeTime(currentTime);
        tLATreeBSchema.setModifyDate(currentDate);
        tLATreeBSchema.setModifyTime(currentTime);
        
        mLATreeBSet.add(tLATreeBSchema);
        
        //更新行政信息 由于助理营业部经理存在降为客户经理三级的情况需要处理
        tLATreeSchema.setAgentLastSeries(tLATreeSchema.getAgentSeries());
        if(tLATreeSchema.getAgentGrade().substring(0,1).equals("G")&&
        	pmLAAssessSchema.getCalAgentGrade().substring(0, 1).equals("F")){
        	tLATreeSchema.setAgentSeries("0");
        	tLATreeSchema.setAgentLastSeries("1");
        	LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        	LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        	tLABranchGroupDB.setBranchManager(pmLAAssessSchema.getAgentCode());
        	tLABranchGroupSet = tLABranchGroupDB.query();
        	if(tLABranchGroupSet==null||tLABranchGroupSet.size()==0){
        		System.out.println("该主管没有直辖团队");
        	}else{
        		for(int i=1;i<=tLABranchGroupSet.size();i++){
        			LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        			tLABranchGroupSchema = tLABranchGroupSet.get(i);
        			tLABranchGroupSchema.setBranchManager("");
        			tLABranchGroupSchema.setBranchManagerName("");
        			tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
        			tLABranchGroupSchema.setModifyDate(currentDate);
        			tLABranchGroupSchema.setModifyTime(currentTime);
        			
        			mLABranchGroupSet.add(tLABranchGroupSchema);
        			
        			LATreeSet downLATreeSet  = new LATreeSet();
        			LATreeDB  downLATreeDB = new LATreeDB();
        			downLATreeDB.setAgentGroup(tLABranchGroupSchema.getAgentGroup());
        			downLATreeDB.setBranchType(mBranchType);
        			downLATreeDB.setBranchType2(mBranchType2);
        			
        			downLATreeSet = downLATreeDB.query();
        			
                	if(downLATreeSet==null||downLATreeSet.size()==0){
                		System.out.println("没有需要处理的下级人员");
                	}else{
                		for(int j =1;j<=downLATreeSet.size();j++){
                			LATreeSchema tDownLATreeSchema = new LATreeSchema();
                			tDownLATreeSchema =  downLATreeSet.get(j);
                			tDownLATreeSchema.setUpAgent("");
                			tDownLATreeSchema.setOperator(mGlobalInput.Operator);
                			tDownLATreeSchema.setModifyDate(currentDate);
                			tDownLATreeSchema.setModifyTime(currentTime);
                			
                			groupLATreeSet.add(tDownLATreeSchema);
                		}              		
                	}        	
        		}
        	}
        	
        }

        
        tLATreeSchema.setAgentLastGrade(tLATreeBSchema.getAgentGrade());
        tLATreeSchema.setOldStartDate(tLATreeSchema.getStartDate());
        tLATreeSchema.setOldEndDate(PubFun.calDate(mEvaluateDate, -1,
                "D", null));
        tLATreeSchema.setAgentGrade1(pmLAAssessSchema.getCalAgentGrade()); //更新业务职级
        tLATreeSchema.setAgentGrade(pmLAAssessSchema.getCalAgentGrade());
        if("F00".equals(pmLAAssessSchema.getCalAgentGrade())){
        	tLATreeSchema.setAgentSeries("");
        }
        
        if (pmLAAssessSchema.getModifyFlag().equals("01")) {
            tLATreeSchema.setState("2");
        }
        if (pmLAAssessSchema.getModifyFlag().equals("03")) {
            tLATreeSchema.setState("1");
        }
        if (pmLAAssessSchema.getModifyFlag().equals("02")) {
            tLATreeSchema.setState("0");
        }
        tLATreeSchema.setStartDate(mEvaluateDate);
        tLATreeSchema.setAstartDate(mEvaluateDate);
        tLATreeSchema.setModifyDate(currentDate);
        tLATreeSchema.setModifyTime(currentTime);
        tLATreeSchema.setOperator(mGlobalInput.Operator);
        mLATreeSet.add(tLATreeSchema);
        
        
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        System.out.println(
                "Begin LAAssessToComfirmBL.getInputData.........");
        //全局变量
        System.out.println("Begin LAAssessToComfirmBL.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mManageCom = (String)cInputData.getObject(1);
        this.mIndexCalNo =(String)cInputData.getObject(2);
        this.mBranchType =(String)cInputData.getObject(3);
        this.mBranchType2 =(String)cInputData.getObject(4);
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessToComfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(mManageCom==null||mManageCom.equals("")||
        		mIndexCalNo==null||mIndexCalNo.equals("")||	
        		mBranchType==null||mBranchType.equals("")||
        		mBranchType2==null||mBranchType2.equals("")){
            CError tError = new CError();
            tError.moduleName = "LAAssessToComfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql) {
        System.out.println(sql);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null) {
                return 0;
            }
            st = conn.prepareStatement(sql);
            if (st == null) {
                return 0;
            }
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
        if (!conn.isClosed()) {
            conn.close();
        }
        try {
            st.close();
            rs.close();
        } catch (Exception ex2) {
            ex2.printStackTrace();
        }
        st = null;
        rs = null;
        conn = null;
      } catch (Exception e) {}

        }
    }
    private boolean checkDimission(LAAssessSchema pmLAAssessSchema)
    {

        LADimissionDB tLADimissionDB=new LADimissionDB();
        LADimissionSet tLADimissionSet=new LADimissionSet();
        tLADimissionDB.setAgentCode(pmLAAssessSchema.getAgentCode());
        tLADimissionSet=tLADimissionDB.query();
        if(tLADimissionSet.size()>0)
        {
            for(int i=1;i<=tLADimissionSet.size();i++)
            {
                LADimissionSchema tLADimissionSchema=new LADimissionSchema();
                tLADimissionSchema=tLADimissionSet.get(i);
                if(tLADimissionSchema.getDepartState().compareTo("03") >=0)
                {
                     return false;
                }
            }
        }
        return true;
    }

    private void jbInit() throws Exception {
    }

}
