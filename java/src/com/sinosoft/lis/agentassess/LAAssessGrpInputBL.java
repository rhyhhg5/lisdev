/*
 * <p>ClassName: OLAArchieveBL </p>
 * <p>Description: OLAArchieveBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:05:58
 */
package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.LATreeTempSchema;

import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;


public class LAAssessGrpInputBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */

  public CErrors mErrors = new CErrors();

  private VData mResult = new VData();


  /** 往后面传输数据的容器 */

  private VData mInputData = new VData();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private String mManageCom = "";
  private String mIndexCalNo = "";
  private String mBranchType = "";
  private String mBranchType2 = "";
  /** 数据操作字符串 */
  private String mOperate;
  private String currentDate = PubFun.getCurrentDate();
  private String currentTime = PubFun.getCurrentTime();
  /** 业务处理相关变量 */
  private LATreeTempSchema mLATreeTempSchema = new LATreeTempSchema();
  private LATreeTempSet mLATreeTempSet = new LATreeTempSet();
  private LATreeTempSet mNewLATreeTempSet = new LATreeTempSet();
  private LATreeTempSet mOldDeleteLATreeTempSet = new LATreeTempSet();
  private LATreeTempSet mOldInsertLATreeTempSet = new LATreeTempSet();
  private LATreeSet mupLATreeSet = new LATreeSet();
  private LATreeSet mdealLATreeSet = new LATreeSet();
  private LATreeSet mdealDifLATreeSet = new LATreeSet();
  private MMap mMap = new MMap();

  public LAAssessGrpInputBL()
  {
  }
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();

      cError.moduleName = "LAAssessGrpInputBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      this.mErrors.addOneError(cError);
    }
  public static void main(String[] args)
  {
      GlobalInput tG = new GlobalInput();
      tG.Operator = "xxx";
      tG.ManageCom = "86";
      LATreeTempSet tLATreeTempSet = new LATreeTempSet();
      LATreeTempSchema tLATreeTempSchema = new LATreeTempSchema();
      tLATreeTempSchema.setAgentCode("1");
      tLATreeTempSchema.setAgentGrade("D01");
      tLATreeTempSchema.setAgentGroup("1");
      tLATreeTempSchema.setStartDate("2006-09-01");
      tLATreeTempSchema.setCValiMonth("200608");
      tLATreeTempSchema.setBranchType("2");
      tLATreeTempSchema.setBranchType2("01");
      tLATreeTempSchema.setManageCom("86110000");
      tLATreeTempSchema.setCValiFlag("1");
  //     tLAAssessAccessorySchema.setAssessType("00");
      tLATreeTempSet.add(tLATreeTempSchema);
      VData tVData = new VData();
      tVData.add(tG);
      tVData.addElement(tLATreeTempSet);
      tVData.addElement("1");
      tVData.addElement("01");
      LAAssessGrpInputBL tLAAssessGrpInputBL = new LAAssessGrpInputBL();
      tLAAssessGrpInputBL.submitData(tVData, "INSERT||MAIN");

  }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
//       System.out.println("transact"+transact);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理

        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContRateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAContRateBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LARateStandPremBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }
    private boolean check(String tAgentCode,String tStartDate,String cAgentGrade,String tManageCom)
    {
//        String tlastdate = PubFun.calDate(currentDate, 1, "M", null);//当前月加一个月
//        String tlastno=AgentPubFun.formatDate(tlastdate,"yyyyMM");//当前月加一个月
//        String tcurrentno=AgentPubFun.formatDate(currentDate,"yyyyMM");//当前月

//    	String tindexcalno=AgentPubFun.formatDate(tStartDate,"yyyyMM");// 调动生效所属年月
//        String tpreStartDate = PubFun.calDate(tStartDate, 1, "M", null);
//        String tpreindexcalno=AgentPubFun.formatDate(tpreStartDate,"yyyyMM");
//        if(!tindexcalno.equals(tlastno) && !tindexcalno.equals(tcurrentno) &&
//           !tpreindexcalno.equals(tcurrentno))
//        {
//            CError tError = new CError();
//            tError.moduleName = "LAAssessGrpInputBL";
//            tError.functionName = "check";
//            tError.errorMessage = "日期必须在本月或则是本月的前后一个月!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        String tDay=AgentPubFun.formatDate(tStartDate,"dd");
        if(!tDay.equals("01"))
        {
            CError tError = new CError();
            tError.moduleName = "LAAssessGrpInputBL";
            tError.functionName = "check";
            tError.errorMessage = "生效日期必须是每个月一日!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tsql="select  max(indexcalno) from lawage  where  managecom like '"+tManageCom
                    +"%' and  branchtype ='2' and branchtype2 = '01'";
        ExeSQL tExeSQL = new ExeSQL();
        String tmaxIndexCalNo=tExeSQL.getOneValue(tsql);
        if(tmaxIndexCalNo!=null&&!tmaxIndexCalNo.equals(""))
        {
        	String tmaxIndexDate = AgentPubFun.formatDate(tmaxIndexCalNo+"01","yyyy-MM-dd");
        	String tlastno = PubFun.calDate(tmaxIndexDate, 1, "M", null);
        	System.out.println("支公司"+tManageCom+"最大薪资月次月1日为"+tlastno);
        	if(!tlastno.equals(tStartDate))
        	{
        	  CError tError = new CError();
              tError.moduleName = "LAAssessGrpInputBL";
              tError.functionName = "check";
              tError.errorMessage = "该公司最大薪资计算月为"+tmaxIndexCalNo+",调整生效日期只能为已经计算的最大薪资月的下个月1号!";
              this.mErrors.addOneError(tError);
              return false;
        	}
        }
//        if(tmaxIndexCalNo!=null && !tmaxIndexCalNo.equals(""))
//        {
//            CError tError = new CError();
//            tError.moduleName = "LAAssessGrpInputBL";
//            tError.functionName = "check";
//            tError.errorMessage = "人员"+tAgentCode+"在月份"+tIndexCalNo+
//                                  "已经计算过薪资,不能在"+tStartDate+"调整职级!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        LATreeDB tdealLATreeDB = new LATreeDB();
        LATreeSchema tLastLATreeSchema = new LATreeSchema();
        tdealLATreeDB.setAgentCode(tAgentCode);
        tdealLATreeDB.getInfo();
        tLastLATreeSchema=tdealLATreeDB.getSchema();
        String tLastAgentGrade=tLastLATreeSchema.getAgentGrade();
        String tLastAgentGroup=tLastLATreeSchema.getAgentGroup();
        String tLastAgentSeries = AgentPubFun.getAgentSeries(tLastAgentGrade);
        if (tLastAgentSeries == null || tLastAgentSeries.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "LAAssessGrpInputBL";
            tError.functionName = "dealData";
            tError.errorMessage = "获取职级" + tLastAgentGrade + "所对应的代理人系列返回为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tNewAgentSeries = AgentPubFun.getAgentSeries(cAgentGrade);
        if (tNewAgentSeries == null || tNewAgentSeries.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "LAAssessGrpInputBL";
            tError.functionName = "dealData";
            tError.errorMessage = "获取职级" + cAgentGrade + "所对应的代理人系列返回为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(tNewAgentSeries.equals("1") && tLastAgentSeries.equals("0"))
        {//业务到主管
            String tSql="select a.agentcode from  latree a ,laagentgrade b,laagent c  where a.agentgrade=b.gradecode "+
                        " and a.agentgroup='"+tLastAgentGroup+"' and  b.GradeProperty2='1' and a.agentcode=c.agentcode  and c.agentstate<'06' ";
            tExeSQL = new ExeSQL();
            String tgetAgentCode=tExeSQL.getOneValue(tSql);
            if(tgetAgentCode!=null && !tgetAgentCode.equals(""))
            {
                /*查询从主管是否要调动到别的团队
               **缺少处理
               */
                String tAgentGroup="";
//               if(tcurrentno.compareTo(tindexcalno)<0)
//               {
//                   tSql =
//                           "select agentgroup from  labranchchangetemp  where agentcode='" +
//                           tgetAgentCode
//                           + "' and CValiMonth='" + tindexcalno +
//                           "' and  BranchChangeType='04' "
//                           + "  and CValiFlag='1' ";
//                   tExeSQL = new ExeSQL();
//                   tAgentGroup = tExeSQL.getOneValue(tSql);
//                   if(tAgentGroup!=null && !tAgentGroup.equals(""))
//                   {
//                       tFlag=false;
//                   }
//               }
               CError tError = new CError();
               tError.moduleName = "LAAssessGrpInputBL";
               tError.functionName = "dealData";
               tError.errorMessage = "人员" + tAgentCode + "所在的团队已有主管，不能把它升为主管!";
               this.mErrors.addOneError(tError);
               return false;

            }
        }
        String tSQL="select *  from  latreetemp  where agentcode = '"+tAgentCode
                  //  +"' and CValiMonth<'"+tIndexCalNo+"'"
                    +"' and cvaliflag='1' "
                    +" order by startdate desc  fetch first 1 rows only ";
        LATreeTempDB tLATreeTempDB= new LATreeTempDB();
        LATreeTempSet tLATreeTempSet= new LATreeTempSet();
        LATreeTempSchema tLATreeTempSchema= new LATreeTempSchema();
        tLATreeTempSet=tLATreeTempDB.executeQuery(tSQL);
        System.out.println(tSQL);
        if(tLATreeTempSet!=null && tLATreeTempSet.size()>0)
        {
            tLATreeTempSchema = tLATreeTempSet.get(1);
            if (tStartDate.compareTo(tLATreeTempSchema.getStartDate()) < 0) {
                if (currentDate.compareTo(tLATreeTempSchema.getStartDate()) >=
                    0) {
                    buildError("dealData",
                               "该人员在" + tLATreeTempSchema.getStartDate()
                               + "已经进行过团队调动的处理,调动日期必须大于或等于原来的变动日期");
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        System.out.println("abbbbbbbbaaaa");
        for(int i=1;i<=mLATreeTempSet.size();i++)
        {
            LATreeTempSchema tLATreeTempSchema = new LATreeTempSchema();
            tLATreeTempSchema=mLATreeTempSet.get(i);
            String tStartDate=tLATreeTempSchema.getStartDate();//新职级开始日期
            String tAgentGrade=tLATreeTempSchema.getAgentGrade();//确认职级
            String tAgentCode=tLATreeTempSchema.getAgentCode();
            String tCValiMonth=tLATreeTempSchema.getCValiMonth();//生效年月
            String tManageCom = tLATreeTempSchema.getManageCom();
            if(!check(tAgentCode,tStartDate,tAgentGrade,tManageCom))
            {
                    // @@错误处理
                   CError tError = new CError();
                   tError.moduleName = "LAAssessGrpInputBL";
                   tError.functionName = "dealData";
                   tError.errorMessage = "数据处理失败!";
                   this.mErrors.addOneError(tError);
                   return false;
            }
            String tsql="select max(tempcount) from latreetemp  where agentcode='"+tAgentCode
                        +"' and cvalimonth='"+tCValiMonth+"'";
            ExeSQL t1ExeSQL = new ExeSQL();
            String tMaxCount=t1ExeSQL.getOneValue(tsql);
            int tCount=0;
            if (tMaxCount==null || tMaxCount.equals(""))
            {
                tCount=0;
            }
            else
            {
                tCount=Integer.parseInt(tMaxCount)+1;//一个月内可以多次进行人员职级变动
            }
            tLATreeTempSchema.setTempCount(tCount);
              //处理以前的记录，使原来的失效，每个月只有一条有效记录
            LATreeTempSet tLATreeTempSet = new LATreeTempSet();
            LATreeTempDB tLATreeTempDB = new LATreeTempDB();

            tLATreeTempDB.setAgentCode(tAgentCode);
            tLATreeTempDB.setCValiMonth(tCValiMonth);
            tLATreeTempDB.setCValiFlag("1" );

            tLATreeTempSet=tLATreeTempDB.query();
          //  mOldDeleteLATreeTempSet=tLATreeTempSet;
            if(tLATreeTempSet!=null && tLATreeTempSet.size()>=1)
            {
                for(int k=1;k<=tLATreeTempSet.size();k++)
                {
                    LATreeTempSchema tupLATreeTempSchema = new LATreeTempSchema();
                    tupLATreeTempSchema=tLATreeTempSet.get(k);
                    Reflections tReflections = new Reflections();
                    LATreeTempSchema tdeleteLATreeTempSchema = new LATreeTempSchema();
                    tReflections.transFields(tdeleteLATreeTempSchema, tupLATreeTempSchema);
                    mOldDeleteLATreeTempSet.add(tdeleteLATreeTempSchema);
                    tupLATreeTempSchema.setCValiFlag("0");
                    tupLATreeTempSchema.setModifyDate(currentDate);
                    tupLATreeTempSchema.setModifyTime(currentTime);
                    mOldInsertLATreeTempSet.add(tupLATreeTempSchema);

                }
            }
            /*处理职级，使原来该职级的月份失效
            *（缺少处理）
            */
            tLATreeTempSchema.setMakeDate(currentDate);
            tLATreeTempSchema.setMakeTime(currentTime);
            tLATreeTempSchema.setModifyDate(currentDate);
            tLATreeTempSchema.setModifyTime(currentTime);
            tLATreeTempSchema.setOperator(mGlobalInput.Operator);
            mNewLATreeTempSet.add(tLATreeTempSchema);

            //如果操作日期大于等于生效日期，则立即生效，插入latree表
            if(currentDate.compareTo(tStartDate)>=0)
            {
                LATreeSchema tLATreeSchema = new LATreeSchema();
                LATreeDB tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(tAgentCode);
                tLATreeDB.getInfo();
                tLATreeSchema=tLATreeDB.getSchema();
                //如果变更职级与原职级相同，则不进行处理，返回上一条，进行下一条处理
                if(tLATreeSchema.getAgentGrade().equals(tLATreeTempSchema.getAgentGrade()))
                {
                    continue;
                }
                String tLastAgentGrade=tLATreeSchema.getAgentGrade();
                String tLastAgentSeries = AgentPubFun.getAgentSeries(tLastAgentGrade);
                if (tLastAgentSeries == null || tLastAgentSeries.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "LAAssessGrpInputBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "获取职级" + tLastAgentGrade + "所对应的代理人系列返回为空!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                String tNewAgentGrade=tLATreeTempSchema.getAgentGrade();
                String tNewAgentSeries = AgentPubFun.getAgentSeries(tNewAgentGrade);
                if (tNewAgentSeries == null || tNewAgentSeries.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "LAAssessGrpInputBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "获取职级" + tNewAgentGrade + "所对应的代理人系列返回为空!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if(tLastAgentSeries.equals(tNewAgentSeries))
                {//处理同序列
                    tLATreeSchema.setAgentGrade(tLATreeTempSchema.getAgentGrade());
                    tLATreeSchema.setStartDate(tLATreeTempSchema.getStartDate());
                    mdealLATreeSet.add(tLATreeSchema);
                }
                else
                {//处理不同序列
                    tLATreeSchema.setAgentGrade(tLATreeTempSchema.getAgentGrade());
                    tLATreeSchema.setStartDate(tLATreeTempSchema.getStartDate());
                    mdealDifLATreeSet.add(tLATreeSchema);
                }
            }
        }
        //同序列职级处理
        if(mdealLATreeSet!=null && mdealLATreeSet.size()>0)
        {
            VData tInputData = new VData();
            tInputData.add(this.mGlobalInput);
            tInputData.add(this.mdealLATreeSet);
            tInputData.add(this.mBranchType);
            tInputData.add(this.mBranchType2) ;
            DealLATree tDealLATree = new DealLATree();
            if(!tDealLATree.submitData(tInputData, mOperate))
            {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tDealLATree.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAAssessGrpInputUI";
                    tError.functionName = "submitData";
                    tError.errorMessage = "数据提交失败!";
                    this.mErrors.addOneError(tError);
                    return false;
            }
            mMap=tDealLATree.getResult();
        }
         //不同序列职级处理
        if(mdealDifLATreeSet!=null && mdealDifLATreeSet.size()>0)
        {
            VData tInputData = new VData();
            tInputData.add(this.mGlobalInput);
            tInputData.add(this.mdealDifLATreeSet);
            tInputData.add(this.mBranchType);
            tInputData.add(this.mBranchType2) ;
            DealDifLATree tDealDifLATree = new DealDifLATree();
            if(!tDealDifLATree.submitData(tInputData, mOperate))
            {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tDealDifLATree.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAAssessGrpInputUI";
                    tError.functionName = "submitData";
                    tError.errorMessage = "数据提交失败!";
                    this.mErrors.addOneError(tError);
                    return false;
            }
            mMap=tDealDifLATree.getResult();
        }
        return true;
    }

    public VData getResult()
    {
        this.mResult.clear();
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
            this.mLATreeTempSet.set((LATreeTempSet) cInputData.
                                           getObjectByObjectName(
                    "LATreeTempSet",
                    0));
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput",
                    0));
            this.mBranchType = (String) cInputData.getObject(2);
            this.mBranchType2 = (String) cInputData.getObject(3);



        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {

        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            mMap.put(this.mNewLATreeTempSet,"INSERT") ;
            mMap.put(this.mOldDeleteLATreeTempSet,"DELETE") ;
            mMap.put(this.mOldInsertLATreeTempSet,"INSERT") ;
            LATreeTempSchema mOldInsertLATreeTempSchema=new LATreeTempSchema();
            mOldInsertLATreeTempSchema=mOldDeleteLATreeTempSet.get(1);
            this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContRateBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
  
    }

