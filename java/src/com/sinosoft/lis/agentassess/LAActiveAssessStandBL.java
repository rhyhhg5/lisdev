package com.sinosoft.lis.agentassess;


import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentwages.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: LAActiveAssessStandBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author xin
 * @version 1.0
 */
public class LAActiveAssessStandBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";

  private MMap map = new MMap();
  public  GlobalInput mGlobalInput = new GlobalInput();

  private LAAgentPromRadix5Set mLAAgentPromRadix5Set  = new LAAgentPromRadix5Set();

  public LAActiveAssessStandBL() {

  }

  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAActiveAssessStandBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LAActiveAssessStandBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAActiveAssessStandBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LAActiveAssessStandBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     this.mLAAgentPromRadix5Set.set((LAAgentPromRadix5Set) cInputData.get(1));
     System.out.println("LAAgentPromRadix5Set get"+mLAAgentPromRadix5Set.size());
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveAssessStandBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAActiveAssessStandBL.dealData........."+mOperate);
    try {
          LAAgentPromRadix5Set tLAAgentPromRadix5SetNew = new LAAgentPromRadix5Set();
    	  for (int i = 1; i <= mLAAgentPromRadix5Set.size(); i++) {
    		  LAAgentPromRadix5Schema tLAAgentPromRadix5Schemanew = new  LAAgentPromRadix5Schema();
    		  LAAgentPromRadix5Schema tLAAgentPromRadix5Schemaold = new  LAAgentPromRadix5Schema();
    		  LAAgentPromRadix5DB tLAAgentPromRadix5DB = new LAAgentPromRadix5DB();
    		  tLAAgentPromRadix5Schemanew = mLAAgentPromRadix5Set.get(i);
    		  String managecom = tLAAgentPromRadix5Schemanew.getManageCom();
    		  String agentGrade = tLAAgentPromRadix5Schemanew.getAgentGrade();
    		  String branchtype = tLAAgentPromRadix5Schemanew.getBranchType();
    		  String branchtype2 = tLAAgentPromRadix5Schemanew.getBranchType2();
    		  String assessType = tLAAgentPromRadix5Schemanew.getAssessType();
    		  tLAAgentPromRadix5DB.setManageCom(managecom);
    		  tLAAgentPromRadix5DB.setAgentGrade(agentGrade);
    		  tLAAgentPromRadix5DB.setBranchType(branchtype);
    		  tLAAgentPromRadix5DB.setBranchType2(branchtype2);
    		  tLAAgentPromRadix5DB.setAssessType(assessType);
    		  if(tLAAgentPromRadix5DB.getInfo())
    		  {
    			  tLAAgentPromRadix5Schemaold = tLAAgentPromRadix5DB.getSchema();
    			  if(mOperate.equals("INSERT"))
    			  {
    				  String message="";
    				  if("1".equals(assessType))
    				  {
    					  message = "月均互动开拓提奖";
    				  }
    				  else if("2".equals(assessType))
    				  {
    					  message = "季度规模保费";
    				  }
    				  CError tError = new CError();
    				  tError.moduleName = "LAActiveAssessStandBL";
    				  tError.functionName = "dealData";
    				  tError.errorMessage = "管理机构"+managecom+"已经录入过:"+message+"的考核信息！";
    				  this.mErrors.addOneError(tError);
    				  break;
    			  }
    			  if(mOperate.equals("UPDATE"))
    			  {
    				  tLAAgentPromRadix5Schemaold.setUpStandPrem(tLAAgentPromRadix5Schemanew.getUpStandPrem());
    				  tLAAgentPromRadix5Schemaold.setKeepStandPrem(tLAAgentPromRadix5Schemanew.getKeepStandPrem());
    				  tLAAgentPromRadix5Schemaold.setModifyDate(PubFun.getCurrentDate());
    				  tLAAgentPromRadix5Schemaold.setModifyTime(PubFun.getCurrentTime());
    				  tLAAgentPromRadix5SetNew.add(tLAAgentPromRadix5Schemaold);
    			  }
    		  }
    		  else
    		  {
    			  tLAAgentPromRadix5Schemanew.setOperator(this.mGlobalInput.Operator);
    			  tLAAgentPromRadix5Schemanew.setMakeDate(PubFun.getCurrentDate());
    			  tLAAgentPromRadix5Schemanew.setMakeTime(PubFun.getCurrentTime());
    			  tLAAgentPromRadix5Schemanew.setModifyDate(PubFun.getCurrentDate());
    			  tLAAgentPromRadix5Schemanew.setModifyTime(PubFun.getCurrentTime());
    			  tLAAgentPromRadix5SetNew.add(tLAAgentPromRadix5Schemanew);
    		  }
              
          }
            map.put(tLAAgentPromRadix5SetNew, mOperate);
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveAssessStandBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LAActiveAssessStandBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveAssessStandBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
