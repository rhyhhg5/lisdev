package com.sinosoft.lis.agentassess;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LAAssessAccessorySet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LAAgentGradeSchema;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.agentcalculate.CalFormDrawRate;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xiangchun
 * @version 1.0
 */
public class DealLATree {
  MMap map = new MMap();
  public CErrors mErrors = new CErrors();
  private GlobalInput mGlobalInput = new GlobalInput();
  private LAAssessAccessorySet mLAAssessAccessorySet = new
      LAAssessAccessorySet();
  private LATreeSet mLATreeSet = new LATreeSet();
  private LATreeBSet mLATreeBSet = new LATreeBSet();
  private LATreeSet mdealLATreeSet = new LATreeSet();
  private LAAgentSet mLAAgentSet = new LAAgentSet();
  private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
  private LADimissionSet mLADimissionSet = new LADimissionSet();
  private LACommisionSet mLACommisionSet = new LACommisionSet();
  private LACommisionSet mgetLACommisionSet = new LACommisionSet();
  private VData mInputData = new VData();
  private VData mOutputData = new VData();
  private MMap mMap = new MMap();
  private String mOperate = "";
  private String mAscriptDate = "";
  private String mIndexCalno = "";
  private String mStartDate = "";
  private String currentDate = PubFun.getCurrentDate();
  private String currentTime = PubFun.getCurrentTime();
  public DealLATree() {
  }

  private boolean getInputData(VData cInputData) {
    mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
        0);

    mdealLATreeSet = (LATreeSet) cInputData.getObjectByObjectName("LATreeSet",
        0);
    if (mdealLATreeSet == null || mdealLATreeSet.size() <= 0) {
      CError tError = new CError();
      tError.moduleName = "DealLATree";
      tError.functionName = "getInputData";
      tError.errorMessage = "未得到足够数据!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  private boolean check() {
    return true;
  }

  public boolean submitData(VData cInputData, String cOperate) {
    mOperate = cOperate;
    if (!getInputData(cInputData)) {
      return false;
    }
    if (!check()) {
      return false;
    }
    if (!dealData()) {
      return false;
    }
    if (!prepareOutputData()) {
      return false;
    }
    return true;
  }

  private boolean dealData() {
    String tEdorType = "31";
    String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
    for (int i = 1; i <= mdealLATreeSet.size(); i++) {
      LATreeSchema tdealLATreeSchema = new LATreeSchema();
      tdealLATreeSchema = mdealLATreeSet.get(i);
      mStartDate = tdealLATreeSchema.getStartDate();
      String tSql = "select date('" + mStartDate + "')-1 month  from  dual ";
      ExeSQL tExeSQL = new ExeSQL();
      String tdate = tExeSQL.getOneValue(tSql);
      mIndexCalno = AgentPubFun.formatDate(tdate, "yyyyMM");

      String tAgentCode = tdealLATreeSchema.getAgentCode();
      String tnewAgentGrade = tdealLATreeSchema.getAgentGrade();
      //新职级系列号
      String tAgentSeries1 = AgentPubFun.getAgentSeries(tnewAgentGrade);

      LATreeDB tdealLATreeDB = new LATreeDB();
      LATreeSchema tLastLATreeSchema = new LATreeSchema();
      tdealLATreeDB.setAgentCode(tAgentCode);
      tdealLATreeDB.getInfo();
      tLastLATreeSchema = tdealLATreeDB.getSchema();

      //得到当前职级
      String tLastAgentGradeNew = tLastLATreeSchema.getAgentGrade();
      String tLastAgentSeriesnew = AgentPubFun.getAgentSeries(
          tLastAgentGradeNew);
      // String  tLastAgentGrade=tLastLATreeSchema.getAgentGrade();
      //根据时间得到上次职级
      String tLastAgentGrade = getlastgrade(tAgentCode);
      //原职级系列号
      String tAgentSeries = AgentPubFun.getAgentSeries(tLastAgentGrade);
      if (tAgentSeries == null || tAgentSeries.equals("")) {
        CError tError = new CError();
        tError.moduleName = "DealLATree";
        tError.functionName = "dealData";
        tError.errorMessage = "获取职级" + tLastAgentGrade + "所对应的代理人系列返回为空!";
        this.mErrors.addOneError(tError);
        return false;
      }
      if (tAgentSeries1 == null || tAgentSeries1.equals("")) {
        CError tError = new CError();
        tError.moduleName = "DealLATree";
        tError.functionName = "dealData";
        tError.errorMessage = "获取职级" + tnewAgentGrade + "所对应的代理人系列返回为空!";
        this.mErrors.addOneError(tError);
        return false;
      }
      if (tLastAgentSeriesnew == null || tLastAgentSeriesnew.equals("")) {
        CError tError = new CError();
        tError.moduleName = "DealLATree";
        tError.functionName = "dealData";
        tError.errorMessage = "获取职级" + tLastAgentSeriesnew + "所对应的代理人系列返回为空!";
        this.mErrors.addOneError(tError);
        return false;
      }
      //当前职级与新职级比较,不能用上次职级与新职级比较
      if (!tLastAgentSeriesnew.equals(tAgentSeries1)) {
        CError tError = new CError();
        tError.moduleName = "DealLATree";
        tError.functionName = "dealData";
        tError.errorMessage = "本模块只能处理同系列的职级的异动!";
        this.mErrors.addOneError(tError);
        return false;
      }
      LAAgentDB tLAAgentDB = new LAAgentDB();
      LAAgentSchema tLAAgentSchema = new LAAgentSchema();
      tLAAgentDB.setAgentCode(tAgentCode);
      if (!tLAAgentDB.getInfo()) {
        this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "DealLATree";
        tError.functionName = "dealData";
        tError.errorMessage = "未查询到" + tAgentCode + "的原始信息!";
        this.mErrors.addOneError(tError);
        return false;
      }
      tLAAgentSchema = tLAAgentDB.getSchema();
      LATreeDB tLATreeDB = new LATreeDB();
      LATreeSchema tLATreeSchema = new LATreeSchema();
      tLATreeDB.setAgentCode(tAgentCode);
      if (!tLATreeDB.getInfo()) {
        this.mErrors.copyAllErrors(tLATreeDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "DealLATree";
        tError.functionName = "dealData";
        tError.errorMessage = "未查询到" + tAgentCode + "的行政信息!";
        this.mErrors.addOneError(tError);
        return false;
      }
      tLATreeSchema = tLATreeDB.getSchema();
      LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
      //原来职级
      LAAgentGradeSchema tLAAgentGradeSchema = new LAAgentGradeSchema();
      //新职级
      LAAgentGradeSchema tLAAgentGrade1Schema = new LAAgentGradeSchema();

      tLAAgentGradeDB.setGradeCode(tLastAgentGrade);
      if (!tLAAgentGradeDB.getInfo()) {
        this.mErrors.copyAllErrors(tLAAgentGradeDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "DealLATree";
        tError.functionName = "dealData";
        tError.errorMessage = "未查询到" + tLastAgentGrade + "的描述信息!";
        this.mErrors.addOneError(tError);
        return false;
      }
      tLAAgentGradeSchema = tLAAgentGradeDB.getSchema(); //原来的职级信息
      tLAAgentGradeDB.setGradeCode(tnewAgentGrade);
      if (!tLAAgentGradeDB.getInfo()) {
        this.mErrors.copyAllErrors(tLAAgentGradeDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "DealLATree";
        tError.functionName = "dealData";
        tError.errorMessage = "未查询到" + tnewAgentGrade + "的描述信息!";
        this.mErrors.addOneError(tError);
        return false;
      }
      tLAAgentGrade1Schema = tLAAgentGradeDB.getSchema(); //新职级信息
//            String sql = "select * from lawage where agentcode='" +
//                         tdealLATreeSchema.getAgentCode() + "'";
//            LAWageDB tLAWageDB = new LAWageDB();
//            LAWageSet tLAWageSet = new LAWageSet();
//            tLAWageSet = tLAWageDB.executeQuery(sql);
//            if (tLAWageSet == null || tLAWageSet.size() == 0) {
//                System.out.println("----没有计算过佣金，调整日期自动改为该业务员的入司日期！");
//                tLAAgentDB = new LAAgentDB();
//                tLAAgentDB.setAgentCode(tLATreeSchema.getAgentCode());
//                tLAAgentDB.getInfo();
//                mAscriptDate = tLAAgentDB.getEmployDate();
//
//            } else {

      mAscriptDate = tdealLATreeSchema.getStartDate();
      mAscriptDate = AgentPubFun.formatDate(mAscriptDate,
                                            "yyyy-MM-dd");
//                mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);

//            }


      if (tLAAgentGradeSchema.getGradeProperty4().equals("0") &&
          tLAAgentGrade1Schema.getGradeProperty4().equals("1")) {
        if (tLAAgentSchema.getInDueFormDate() == null ||
            tLAAgentSchema.getInDueFormDate().equals("")) {
          LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
          Reflections tReflections = new Reflections();
          tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
          tLAAgentBSchema.setModifyDate(currentDate);
          tLAAgentBSchema.setModifyTime(currentTime);

          tLAAgentBSchema.setIndexCalNo(mIndexCalno);
          tLAAgentBSchema.setEdorNo(tEdorNo);
          tLAAgentBSchema.setEdorType(tEdorType);
          tLAAgentBSchema.setMakeDate(currentDate);
          tLAAgentBSchema.setMakeTime(currentTime);
          tLAAgentBSchema.setModifyDate(currentDate);
          tLAAgentBSchema.setModifyTime(currentTime);
          tLAAgentBSchema.setOperator(mGlobalInput.Operator);
          tLAAgentSchema.setInDueFormDate(mAscriptDate); //置转正日期
          tLAAgentSchema.setModifyDate(currentDate);
          tLAAgentSchema.setModifyTime(currentTime);
          tLAAgentSchema.setOperator(mGlobalInput.Operator);
          mLAAgentBSet.add(tLAAgentBSchema);
        }
      }
      if (!tnewAgentGrade.equals(tLastAgentGradeNew)) {
        LATreeBSchema tLATreeBSchema = new LATreeBSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLATreeBSchema, tLATreeSchema);
        tLATreeBSchema.setEdorNO(tEdorNo);
        tLATreeBSchema.setRemoveType(tEdorType);
        tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
        tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
        tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
        tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
        tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
        tLATreeBSchema.setMakeDate(currentDate);
        tLATreeBSchema.setMakeTime(currentTime);
        tLATreeBSchema.setModifyDate(currentDate);
        tLATreeBSchema.setModifyTime(currentTime);
        tLATreeBSchema.setIndexCalNo(mIndexCalno);
        tLATreeBSchema.setOperator(mGlobalInput.Operator);
        mLATreeBSet.add(tLATreeBSchema);
        tLATreeSchema.setAgentGrade(tnewAgentGrade);
        tLATreeSchema.setAgentSeries(tAgentSeries1);
        // ↓ *** add *** LiuHao *** 2005-11-16 *** 追加新机构代码 *****
//                tLATreeSchema.setAgentGroup(tAgentGroupNew);
//                tLATreeSchema.setBranchCode(tAgentGroupNew);
        // ↑ *** add *** LiuHao *** 2005-11-16 *** 追加新机构代码 *****

        tLATreeSchema.setAgentLastGrade(tLastAgentGrade);
        tLATreeSchema.setAgentLastSeries(tAgentSeries);
        //得到职级开始时间
        String tStartDate = getlaststartdate(tLATreeSchema.getAgentCode());
        tLATreeSchema.setOldStartDate(tStartDate);
        tLATreeSchema.setOldEndDate(PubFun.calDate(mAscriptDate, -1,
            "D", null));

        if (tLastAgentGrade.compareTo(tnewAgentGrade) > 0) { //降级
          tLATreeSchema.setAssessType("2");
        }
        else if (tLastAgentGrade.compareTo(tnewAgentGrade) < 0) {
          tLATreeSchema.setAssessType("1"); //晋升
        }
        else {
          tLATreeSchema.setAssessType("0"); //晋升
        }
        tLATreeSchema.setStartDate(mAscriptDate);
        tLATreeSchema.setAstartDate(mAscriptDate);
        tLATreeSchema.setModifyDate(currentDate);
        tLATreeSchema.setModifyTime(currentTime);
      }
      if(tLATreeSchema.getAgentGrade().equals("D01"))
      {
          tLATreeSchema.setInDueFormDate("");
          tLATreeSchema.setInDueFormFlag("N");
      }
      mLAAgentSet.add(tLAAgentSchema);
      mLATreeSet.add(tLATreeSchema);

//            if(tLAAssessAccessorySchema.getAgentGrade().equals("B01") && tLAAssessAccessorySchema.getAgentGrade1().compareTo("B01")>0)
//            {
//
//            }
    }
    return true;
  }

  private String getlastgrade(String cAgentCode) {
    String tdate = PubFun.calDate(mStartDate, -1, "D", null);
    String tSql = "select agentgrade from latree where agentcode='" +
        cAgentCode
        + "' and startdate<='" + tdate + "'";
    ExeSQL tExeSQL = new ExeSQL();
    String tAgentGrade = tExeSQL.getOneValue(tSql);

    if (tAgentGrade == null || tAgentGrade.equals("")) {
      tSql = "select agentlastgrade from latree where agentcode='" + cAgentCode +
          "'";

      tExeSQL = new ExeSQL();
      tAgentGrade = tExeSQL.getOneValue(tSql);
      //================START===================//
      if (tAgentGrade == null || tAgentGrade.equals("")) {
        tSql = "select agentgrade from latree where agentcode='" +
            cAgentCode + "'";
        tExeSQL = new ExeSQL();
        tAgentGrade = tExeSQL.getOneValue(tSql);

      }
      //=================END================//

    }

    return tAgentGrade;
  }

  private String getlaststartdate(String cAgentCode) {
    String tdate = PubFun.calDate(mStartDate, -1, "D", null);
    String tSql = "select startDate from latree where agentcode='" + cAgentCode
        + "' and startdate<='" + tdate + "'";
    ExeSQL tExeSQL = new ExeSQL();
    String tStartDate = tExeSQL.getOneValue(tSql);

    if (tStartDate == null || tStartDate.equals("")) {
      tSql = "select oldstartDate from latree where agentcode='" + cAgentCode +
          "'";
      tExeSQL = new ExeSQL();
      tStartDate = tExeSQL.getOneValue(tSql);
      //================START===================//
      if (tStartDate == null || tStartDate.equals("")) {
        tSql = "select startDate from latree where agentcode='" +
            cAgentCode + "'";
        tExeSQL = new ExeSQL();
        tStartDate = tExeSQL.getOneValue(tSql);

      }
      //=================END================//

    }
    return tStartDate;

  }

  private boolean prepareOutputData() {
    //   mOutputData = new VData();
    mMap = new MMap();
    mMap.put(mLATreeSet, "UPDATE");
    mMap.put(mLATreeBSet, "INSERT");
    mMap.put(mLAAgentSet, "UPDATE");
    mMap.put(mLAAgentBSet, "INSERT");
    mMap.put(mLACommisionSet, "UPDATE");
    //  mOutputData.add(mMap);
    return true;
  }

  public MMap getResult() {
    return mMap;
  }
}
