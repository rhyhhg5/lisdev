package com.sinosoft.lis.agentassess;

/**
 * <p>Title: AscriptControlBLF类</p>
 * <p>Description: 归属的BL类的调用逻辑控制流</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */

import com.sinosoft.lis.agent.ALADimissionAppBL;
import com.sinosoft.lis.agent.ALADimissionBL;
import com.sinosoft.lis.agent.LARearPICCHBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.db.LAAssessMainDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAssessMainSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentGradeSet;
import com.sinosoft.lis.vschema.LAAssessMainSet;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AscriptControlBLF
{
    public CErrors mErrors = new CErrors();
    public GlobalInput mGlobalInput = new GlobalInput(); //操作员的信息
    private AscriptBusinessOperateBL mAscriptBusinessOperateBL = new
            AscriptBusinessOperateBL();
    private VData mInputData = new VData(); //传入BL的数据
    private VData mOutputDataBLS = new VData(); //传出到BLS的数据

    private LAAssessMainSchema mLAAssessMainSchema = new LAAssessMainSchema();
    //本次需要归属的所有考核结果的集合
    private LAAssessSet mLAAssessSet = new LAAssessSet();
    private LAAssessSchema mLAAssessSchema = new LAAssessSchema();

    //当前要修改状态的考核结果
    private LAAssessSchema mUpdateLAAssessSchema = new LAAssessSchema();
    private String mIndexCalNo; //本次归属	的考核计算代码YYYYMM
    private String mOldGradeEndDate; //被归属人员原来职级的结束日期
    private String mNewGradeStartDate; //被归属人员新职级的开始日期
    private String mManageCom;
    private String mBranchType;
    private String mBranchType2;
    private String mAgentSeries;
    private String mAgentGrade;
    private String mEdorNo; //转储号 一次归属业务中每个代理人产生一个转储号
    private int mChoice = -1;
    private AscriptControlB01BLF mAscriptControlB01BLF = new AscriptControlB01BLF();
    private AscriptAgentInfoOperateBL mAgentInfoOperateBL = new
            AscriptAgentInfoOperateBL();
    private AscriptBranchGroupOperateBL mBranchGroupOperateBL = new
            AscriptBranchGroupOperateBL();
    private AscriptRelationOperateBL mRelationOperateBL = new
            AscriptRelationOperateBL();
    private AscriptRearQueryDealBL mRearQueryDealBL = new
            AscriptRearQueryDealBL();
    private LARearPICCHBL mLARearPICCHBL = new
                                           LARearPICCHBL();
    public AscriptControlBLF() {
    }

    /**
     * 前台传入的数据
     * @param cInputData VData
     * @return boolean
     */
    public boolean getInputData(VData cInputData) {
        mManageCom = (String) cInputData.get(0);
        mIndexCalNo = (String) cInputData.get(1);
        mBranchType = (String) cInputData.get(2);
        mBranchType2 = (String) cInputData.get(3);
        mAgentSeries = (String) cInputData.get(4);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);

        String year = mIndexCalNo.substring(0, 4);
        String month = mIndexCalNo.substring(4);

        if (mIndexCalNo.length() != 6) {
            CError.buildErr(this, "前台传入的考核年月代码格式错误！");
            return false;
        }
        if (!(mManageCom != null && !mManageCom.equals("")
              &&
//              mAgentGrade != null && !mAgentGrade.equals("")
//              &&
              year != null && !year.equals("")
              &&
              month != null && !month.equals("")
              &&
              mBranchType != null && !mBranchType.equals("")
              &&
              mGlobalInput != null)) {
            CError.buildErr(this, "前台传入的信息缺失！");
            return false;
        }
        if (mManageCom.indexOf(mGlobalInput.ManageCom) != 0) { //操作机构不是被归属机构或其上级机构
            CError.buildErr(this, "您无权进行此机构的组织归属操作!");
            return false;
        }
        if (!(Integer.parseInt(year) > 0 && year.length() == 4
              &&
              Integer.parseInt(month) > 0 && month.length() <= 2)) {
            CError.buildErr(this, "前台传入的年月信息格式无效！");
            return false;
        }
        if (month.length() < 2) {
            month = "0" + month;
        }
        return true;
    }

    private boolean prepareOutputToBLSData() {
        mOutputDataBLS.clear();
        VData tVData = new VData();
        try {
            if (mChoice > 0) {
                tVData = mAgentInfoOperateBL.getOutputDataToBLS();
                if (!transVData(tVData, mOutputDataBLS)) {
                    CError.buildErr(this, "得到代理人信息数据失败！");
                    return false;
                }
            } else if (mChoice == 0) {
                //已经调用离职程序处理了
//                if (!transVData(tVData, mOutputDataBLS))
//                {
//                    CError.buildErr(this,"得到清退代理人的相关信息失败！");
//                    return false;
//                }
            }
            if (mChoice >= 2 && mChoice <= 3) {
                tVData = new VData();
                tVData = mAscriptBusinessOperateBL.getOutputDataToBLS();
                if (!transVData(tVData, mOutputDataBLS)) {
                    CError.buildErr(this, "得到代理人关系信息失败！");
                    return false;
                }
                tVData = mBranchGroupOperateBL.getOutputDataToBLS();
                if (!transVData(tVData, mOutputDataBLS)) {
                    CError.buildErr(this, "得到代理人机构信息失败！");
                    return false;
                }
                tVData = new VData();
                tVData = mRelationOperateBL.getOutputDataToBLS();
                if (!transVData(tVData, mOutputDataBLS)) {
                    CError.buildErr(this, "得到代理人关系信息失败！");
                    return false;
                }

            }

            mOutputDataBLS.add(mUpdateLAAssessSchema);
            mOutputDataBLS.add(mLAAssessMainSchema);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private boolean inputDataToBL() {
        try {
            mInputData.clear();
            mInputData.add(mOldGradeEndDate);
            mInputData.add(mNewGradeStartDate);
            mInputData.add(mGlobalInput);
            mInputData.add(mIndexCalNo);
            mInputData.add(mManageCom);
            mInputData.add(mAgentSeries);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 查询返回考核确认结果
     * @return boolean
     */
    private boolean getAssessResult() {
        LAAssessDB tLAAssessDB = new LAAssessDB();
        String strSQL = "";
        strSQL =
                " select * from laassess where "
                + " indexcalno='" + mIndexCalNo + "' "
                + " and branchtype='" + mBranchType + "' "
                + " and branchtype2='" + mBranchType2 + "' "
                + " and state='1' and managecom like '" + mManageCom + "%' ";
//        if (this.mAgentSeries.compareTo("0") == 0)
//        {
//            strSQL += " and agentgrade like 'A%' ";
//        }
//        else if (this.mAgentSeries.compareTo("0") > 0)
//        {
//            strSQL += " and agentgrade like 'B%' ";
//        }
        strSQL += "order by agentseries,agentgrade,agentcode,agentgrade1"; //先归属业务
        //必须先把低职级 归属，若果先归属处经理，处经理离职后，团队可能停业，其下面的业务人员
        //晋升后根据lassess表的agentgroup归属，agentgroup停业后就归属不到了
        try
        {
            System.out.println(strSQL);
            mLAAssessSet = tLAAssessDB.executeQuery(strSQL);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return true;
    }

    /**
     * 更新考核结果信息
     * @param cAgentCode String
     * @return boolean
     */
    private boolean updateAssess(String cAgentCode) {
        LAAssessDB tLAAssessDB = new LAAssessDB();
        tLAAssessDB.setBranchType(mBranchType);
        tLAAssessDB.setBranchType2(mBranchType2);
        tLAAssessDB.setIndexCalNo(mIndexCalNo);
        tLAAssessDB.setState("1");
        tLAAssessDB.setManageCom(mManageCom);
        tLAAssessDB.setAgentCode(cAgentCode);
        if (!tLAAssessDB.getInfo()) {
            CError.buildErr(this, "查询代理人" + cAgentCode + "的考核结果记录失败！");
            return false;
        }

        mUpdateLAAssessSchema = tLAAssessDB.getSchema();
        if (mChoice >0) {
            mUpdateLAAssessSchema.setAgentGroupNew(mAgentInfoOperateBL.
                    getCurAgentTree().getAgentGroup());
        }
        else
        {
            mUpdateLAAssessSchema.setAgentGroupNew(
                    mUpdateLAAssessSchema.getAgentGroup());
        }

        mUpdateLAAssessSchema.setState("2");
        mUpdateLAAssessSchema.setOperator(mGlobalInput.Operator);
        mUpdateLAAssessSchema.setModifyDate(PubFun.getCurrentDate());
        mUpdateLAAssessSchema.setModifyTime(PubFun.getCurrentTime());

        return true;
    }

    /*
     选择归属处理
     返回：1-Proceedone  2-Proceedtwo  3-Proceedthree  ...-其它情况
     */
    private int SelectProceed() {
        if (mLAAssessSchema.getAgentGrade1().equals("A00")) {
            return 0;
        }
        //同系列晋升或降级(B01到业务系列也做同系列计算，业务系列到 B01也是同系列）
        if (((mLAAssessSchema.getAgentSeries().equals( mLAAssessSchema.getAgentSeries1()))
            &&!mLAAssessSchema.getAgentGrade().equals(mLAAssessSchema.getAgentGrade1()) &&
            !mLAAssessSchema.getAgentGrade().equals("B01")&&!mLAAssessSchema.getAgentGrade1().equals("B01"))
            ||         (mLAAssessSchema.getAgentGrade().equals("B01") && mLAAssessSchema.getAgentSeries1().equals("0"))
          ||(mLAAssessSchema.getAgentGrade1().equals("B01") && mLAAssessSchema.getAgentSeries().equals("0"))
               )
        {
            return 1;
        }
        // 跨系列晋升(业务A系列->主管B系列，或者处职级->区职级)
        if ((!mLAAssessSchema.getAgentGrade1().equals("B01")&&!mLAAssessSchema.getAgentGrade().substring(0).equals(
                mLAAssessSchema.
                getAgentGrade1().substring(0)) &&
             mLAAssessSchema.
             getAgentSeries().compareTo(mLAAssessSchema.getAgentSeries1()) < 0)
            || (mLAAssessSchema.getAgentGrade().equals("B01") &&
                !mLAAssessSchema.getAgentGrade1().equals("B01")&&
                mLAAssessSchema.getAgentSeries1().compareTo("1") >= 0)
             ) {
            return 2;
        }
        //跨系列降级(主管B系列->业务A系列，或者区职级->处职级))
        if ((!mLAAssessSchema.getAgentGrade().substring(0).equals(
                mLAAssessSchema.
                getAgentGrade1().substring(0)) &&
             mLAAssessSchema.
             getAgentSeries().compareTo(mLAAssessSchema.getAgentSeries1()) > 0)
            || (mLAAssessSchema.getAgentGrade1().equals("B01") &&
                !mLAAssessSchema.getAgentGrade().equals("B01")&&
                mLAAssessSchema.getAgentSeries().compareTo("1") >= 0)) {
            return 3;
        }
        //职级没有发生变化
        if (mLAAssessSchema.getAgentGrade1().equals(mLAAssessSchema.
                getAgentGrade())) {
            return 4;
        }
        return -1;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    public boolean SubmitData(VData cInputData) {
        if (!getInputData(cInputData)) {
            return false;
        }

//        if (!InitBL())
//        {
//            return false;
//        }

        if (!checkData())
        {
            return false;
        }

        if (!DealData())
        {
            return false;
        }
        //对于营业组经理进行特殊处理
        VData tVData = new VData();
        tVData.add(this.mManageCom);
        tVData.add(this.mIndexCalNo);
        tVData.add(this.mBranchType);
        tVData.add(this.mBranchType2);
        tVData.add(this.mNewGradeStartDate);
        
        if(!mAscriptControlB01BLF.SubmitData(tVData))
        {
        	 CError.buildErr(this, "营业组处理失败！");
             return false;
        }
        return true;
    }

    //检查考核结果是否都已经确认完毕
    private boolean checkData()
    {
        LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
        LAAssessMainSet tLAAssessMainSet = new LAAssessMainSet();
        String strSQL = "";
        LAAgentGradeSet  tLAAgentGradeSet = new  LAAgentGradeSet();
        LAAgentGradeDB  tLAAgentGradeDB = new  LAAgentGradeDB();
        tLAAgentGradeDB.setBranchType(mBranchType);
        tLAAgentGradeDB.setBranchType2(mBranchType2);
        tLAAgentGradeSet=tLAAgentGradeDB.query();
        if(tLAAgentGradeSet==null || tLAAgentGradeSet.size()<=0 )
        {
            CError.buildErr(this, "查询职级信息出错！");
            return false;
        }
        for (int i=1;i<=tLAAgentGradeSet.size();i++)
        {
            String tAgentGrade = tLAAgentGradeSet.get(i).getGradeCode();
            if(tAgentGrade.equals("B21"))
            {
                continue;
            }
            strSQL =
                    " select * from laassessmain where "
                    + " indexcalno='" + mIndexCalNo + "' "
                    + " and branchtype='" + mBranchType + "' "
                    + " and branchtype2='" + mBranchType2 + "' "
                    + " and agentgrade= '" + tAgentGrade + "' "
                    + " and managecom like '" + mManageCom + "%' and  exists(select 1 from laassess where indexcalno=laassessmain.indexcalno and branchtype=laassessmain.branchtype" 
                    + " and branchtype2=laassessmain.branchtype2 and agentgrade=laassessmain.agentgrade and managecom=laassessmain.managecom)";
            try {
        	   
                System.out.println(strSQL);
                tLAAssessMainSet = tLAAssessMainDB.executeQuery(strSQL);
                if (tLAAssessMainSet == null || tLAAssessMainSet.size() < 0) {
                    // CError.buildErr(this, "考核结果还未审核确认完毕，请先审核确认！");
                    CError.buildErr(this,
                                    "职级" + tAgentGrade + "还未进行考核计算，请先考核计算！");
                    return false;
                } else {
                    String tState = tLAAssessMainSet.get(1).getState();
                    System.out.println("tState"+tState);
                    if (tState.compareTo("1")<0) {
                        CError.buildErr(this,
                                        "职级" + tAgentGrade + "还未审核确认完毕，请先审核确认！");
                        return false;
                    }
                }

            } catch (Exception ex) {
//                CError.buildErr(this, "查询考核结果信息出错！");
            }
        }
        return true;
    }
    
    //归属处理
    private boolean DealData() {
        if (!getAssessResult()) {
            return false;
        }
        if (mLAAssessSet == null || mLAAssessSet.size() <= 0) {
            CError.buildErr(this, "没有要归属的人员！");
            return false;

        }
        for (int i = 1; i <= mLAAssessSet.size(); i++) {
            mLAAssessSchema = mLAAssessSet.get(i).getSchema();
            //下面处理已离职人员的组织归属
            if(checkAgentState(mLAAssessSchema.getAgentCode())){
        		if(doUpdateLAAssess(mLAAssessSchema)){//对离职人员做组织归属，直接更改state为2
        			continue;
        		}else{
        			return false;
        		}
        	}
            mAgentSeries = mLAAssessSchema.getAgentSeries();
            if (!InitBL()) {
                return false;
            }
            mBranchGroupOperateBL.resetMAXATTR(); //外部编码清零
            mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20); //一个人生成一个转储号
            mChoice = SelectProceed();
            switch (mChoice) {
            case 0: //清退
                if (!Proceedzero()) {
                    CError.buildErr(this, "代理人清退失败！");
                    return false;
                }
                break;
            case 1: //同系列晋降
                if (!Proceedone()) {
                    CError.buildErr(this, "代理人同系列组织归属失败！");
                    return false;
                }
                break;
            case 2:
                if (!Proceedtwo()) {
                    CError.buildErr(this, "代理人晋升高级系列组织归属失败！");
                    return false;
                }
                break;
            case 3:
                if (!Proceedthree()) {
                    CError.buildErr(this, "代理人降级低级系列组织归属失败！");
                    return false;
                }
                break;
            case 4:
                if (!ProceedFour()) {
                    CError.buildErr(this, "代理人职级维持时组织归属失败！");
                    return false;
                }

                break;

            default: //没有任何变动
            }
            //更新考核结果信息
            if (!updateAssess(mLAAssessSchema.getAgentCode())) {
                CError.buildErr(this, "代理人考核结果信息归属失败！");
                return false;
            }
            //更新考核主表
            if (!updateAssessMain()) {
                CError.buildErr(this, "代理人考核主表信息归属失败！");
                return false;
            }
            if (!SubmitToBL()) {
                CError.buildErr(this, "保存代理人组织归属结果失败！");
                return false;
            }
        }
        return true;
    }
    /**
     * 检查代理人是否离职
     * 如果离职，返回true；如果没离职，返回false
     * 对已经离职的代理人则直接将laassess表的state置为2，不再做组织归属
     * @return
     */
    private boolean checkAgentState(String agentcode){
    	LAAgentDB tLAAgentDB=new LAAgentDB();
    	LAAgentSchema tLAAgentSchema=new LAAgentSchema();
    	tLAAgentDB.setAgentCode(agentcode);
    	tLAAgentDB.getInfo();
    	tLAAgentSchema.setSchema(tLAAgentDB.getSchema());
    	if(tLAAgentSchema.getAgentState().equals("06")
    			||tLAAgentSchema.getAgentState().equals("07")
    			||tLAAgentSchema.getAgentState().equals("08")){
    		return true;
    	}else{
    		return false;
    	}
    }
    /**
     * 处理离职的代理人的组织归属
     * @return
     */
    private boolean doUpdateLAAssess(LAAssessSchema tLAAssessSchema){
    	LAAssessDB tLAAssessDB=new LAAssessDB();
    	tLAAssessSchema.setState("2");
    	tLAAssessSchema.setOperator(mGlobalInput.Operator);
    	tLAAssessSchema.setModifyDate(PubFun.getCurrentDate());
    	tLAAssessSchema.setModifyTime(PubFun.getCurrentTime());
    	tLAAssessDB.setSchema(tLAAssessSchema);
    	if (!tLAAssessDB.update()) {
            this.mErrors.copyAllErrors(tLAAssessDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AscriptControlBLF.java";
            tError.functionName = "doUpdateLAAssess";
            tError.errorMessage = "修改LAAssess表错误!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	return true;
    }
    /**
     * 清退人员的特殊处理，
     * 判断是否有未回执回销或者正在处理过程中的保单
     * @return
     */
 private boolean SpecialDeal()
 {
	 String sql = "";
	 boolean tFlag = true;
	System.out.println(" tFlag;"+tFlag );
//	调用离职确认程序之前做限制判断:清退人员如有保单未回执或在处理过程中，不做离职确认，只需做一个标记
     if(mBranchType.equals("1")&&mBranchType2.equals("01"))
     {
     	sql=
     			"select contno  from  lccont where  agentcode='"+mLAAssessSchema.getAgentCode()+"' " +
     			"and   (appflag<>'1' or appflag is null ) and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null)" +
     			" and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" + 
     			" and conttype = '1' " + 
     			" union all " + 
     			" select lgc.grpcontno from lcgrpcont lgc where lgc.agentcode = '"+mLAAssessSchema.getAgentCode()+"' " + 
     			" and (lgc.appflag<>'1' or lgc.appflag is null ) and ((lgc.uwflag<>'a' and lgc.uwflag<>'8' and lgc.uwflag<>'1') or lgc.uwflag is null)" +
     			" and not exists (select 'X' from lcrnewstatelog where newgrpcontno = lgc.grpcontno)"
     			
     			;
     }
     else if (mBranchType.equals("2")&&mBranchType2.equals("01"))
     {
       sql="select grpcontno  from  lcgrpcont where  agentcode='"+mLAAssessSchema.getAgentCode()+"' " +
       		"and  (appflag<>'1' or appflag is null ) and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) ";
     }
     ExeSQL mExeSQL = new ExeSQL();
     SSRS mSSRS = new SSRS();
     mSSRS = mExeSQL.execSQL(sql);
     if(mSSRS.getMaxRow()>0)
     {
     	//还有未签单保单，在处理过程中
     	tFlag= false;
     }
     else
     {//判断是否有未回执回销保单
     	 if(mBranchType.equals("1")&&mBranchType2.equals("01"))
          {
     		 sql="select contno  from  lccont where  agentcode='"+mLAAssessSchema.getAgentCode()+"'" +
		 		"   and  customgetpoldate is null  and  uwflag<>'a' and uwflag<>'8' and uwflag<>'1'" + 
 		 		" and conttype = '1' and cardflag<>'2' " + 
 		 		" union all " +
 		 		" select lgc.grpcontno from lcgrpcont lgc where lgc.agentcode = '"+mLAAssessSchema.getAgentCode()+"'" +
 		 		" and  lgc.customgetpoldate is null  and  lgc.uwflag<>'a' and lgc.uwflag<>'8' and uwflag<>'1' and cardflag<>'2'";
          }
          else if (mBranchType.equals("2")&&mBranchType2.equals("01"))
          {
         	 sql="select grpcontno  from  lcgrpcont where  agentcode='"+mLAAssessSchema.getAgentCode()+"'   " +
         	 		"and  customgetpoldate is null  and  uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and cardflag<>'2'";
          }
     	 ExeSQL wExeSQL = new ExeSQL();
     	 SSRS wSSRS = new SSRS();
     	 wSSRS = wExeSQL.execSQL(sql);
          if(wSSRS.getMaxRow()>0)
          {
          	//还有未回执回销保单保单
          	tFlag= false;
          }
     }
     return tFlag;
 }


    /**
     * 清退
     * @return boolean
     */
    private boolean Proceedzero() {

        VData tInputVData = new VData();
        //清退的时候如果这个人已经不是在职状态，就不用作清退了
        ExeSQL tExeSQL = new ExeSQL();
        String agentState = "";
        String sql = "select agentstate from laagent where agentcode = '" +
                     mLAAssessSchema.getAgentCode() + "' ";
        agentState = tExeSQL.getOneValue(sql);
        if (!agentState.equals("") && agentState.compareTo("03") >= 0) { //非在职状态直接返回
            return true;
        }
        boolean mflag = SpecialDeal();
        if(mflag)
        {
        //生成离职信息schema
        String tSql=" select max(departtimes)+1 from LADimission where 1=1 and agentcode='"+mLAAssessSchema.getAgentCode()+"' ";
        tExeSQL = new ExeSQL();
        String tDepartTimes=tExeSQL.getOneValue(tSql);
        if(tDepartTimes.equals("")|| tDepartTimes.equals("0"))
        {
            tDepartTimes="1";
        }
        LADimissionSchema tLADimissionSchema = new LADimissionSchema();
        tLADimissionSchema.setAgentCode(mLAAssessSchema.getAgentCode());
        tLADimissionSchema.setDepartTimes(tDepartTimes);
        tLADimissionSchema.setApplyDate(mNewGradeStartDate);
        tLADimissionSchema.setBranchType(mBranchType);
        tLADimissionSchema.setBranchType2(mBranchType2);
        tLADimissionSchema.setDepartDate("");
        tLADimissionSchema.setDepartRsn("0");
        tLADimissionSchema.setOperator(mGlobalInput.Operator);

        tInputVData.add(tLADimissionSchema);
        tInputVData.add(mGlobalInput);
        //这里调用离职登记程序接口，独立事务处理
        ALADimissionAppBL tALADimissionAppBL = new ALADimissionAppBL();
        if (!tALADimissionAppBL.submitData(tInputVData, "INSERT||MAIN")) {
            CError.buildErr(this,
                            "清退时,代理人" + mLAAssessSchema.getAgentCode() + "离职登记失败！");
            return false;
        }

        //离职确认处理
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionDB.setAgentCode(mLAAssessSchema.getAgentCode());
        tLADimissionDB.setDepartTimes(1);
        if (!tLADimissionDB.getInfo()) {
            CError.buildErr(this,
                            "清退时,获取代理人" + mLAAssessSchema.getAgentCode() + "离职登记信息失败！");
        }
        tLADimissionSchema = new LADimissionSchema();
        tLADimissionSchema = tLADimissionDB.getSchema();
        tLADimissionSchema.setDepartDate(mNewGradeStartDate);
        tInputVData = new VData();
        tInputVData.addElement(tLADimissionSchema);
        tInputVData.add(mGlobalInput);
        //这里调用离职确认程序接口，独立事务处理
        ALADimissionBL tALADimissionBL = new ALADimissionBL();
        if (!tALADimissionBL.submitData(tInputVData, "INSERT||MAIN")) {
            CError.buildErr(this,
                            "清退时,代理人" + mLAAssessSchema.getAgentCode() +
                            "离职确认失败！");
            return false;
         }
       }

        return true;
    }

    /**
     * 同系列晋升或降级
     * @return boolean
     */
    private boolean Proceedone()
    {
        mAgentInfoOperateBL = new AscriptAgentInfoOperateBL();
        mBranchGroupOperateBL = new AscriptBranchGroupOperateBL();
        mRelationOperateBL = new AscriptRelationOperateBL();
        mRearQueryDealBL = new AscriptRearQueryDealBL();
        if (!mAgentInfoOperateBL.getInputData(mInputData))
        {
            CError.buildErr(this,"初始化mAgentInfoOperateBL失败");
            return false;
        }
        mAgentInfoOperateBL.setmEdorNo(mEdorNo);
        //业务系列
        if (mAgentSeries.equals("0") || mLAAssessSchema.getAgentGrade().equals("B01")
            || mLAAssessSchema.getAgentGrade1().equals("B01"))
        {
            if (!mAgentInfoOperateBL.changeAgentGrade(
                    mLAAssessSchema.getAgentCode(),
                    mLAAssessSchema.getAgentGrade1()))
            {
                CError.buildErr(this, "修改代理人职级失败！");
                return false;
            }
        }
        //主管系列
        else if((mAgentSeries.equals("1") || mAgentSeries.equals("2") )
                && !mLAAssessSchema.getAgentGrade().equals("B01")
                && !mLAAssessSchema.getAgentGrade1().equals("B01"))
        {
            String tGradePropety = AgentPubFun.getAgentSeries(mLAAssessSchema.
                    getAgentGrade());
            String tGradePropety1 = AgentPubFun.getAgentSeries(mLAAssessSchema.
                    getAgentGrade1());
            if (tGradePropety.equals(tGradePropety1) &&
                !mLAAssessSchema.getAgentGrade().equals("B01") &&
                !mLAAssessSchema.getAgentGrade1().equals("B01"))
            { //相同职级层级的职级变化（除了B01），只需要修改职级，团队和关系不发生变化
                if (!mAgentInfoOperateBL.changeAgentGrade(
                        mLAAssessSchema.getAgentCode(),
                        mLAAssessSchema.getAgentGrade1()))
                {
                    CError.buildErr(this, "修改代理人职级失败！");
                    return false;
                }
            }
//            else
//            { //不同层级的变化
//                if(mLAAssessSchema.getAgentGrade().equals("B01"))
//                {//B01晋升B02
//                    if (!Proceedtwo())
//                    {
//                        CError.buildErr(this, "见习经理晋升高级系列组织归属失败！");
//                        return false;
//                    }
//                }
//                else if (mLAAssessSchema.getAgentGrade1().equals("B01"))
//                {//营业主任到B01
//                    if (!Proceedthree())
//                    {
//                        CError.buildErr(this, "代理人降级到见习经理职级组织归属失败！");
//                        return false;
//                    }
//                }
//                else
//                {//其他情况（agentseries 1--2或则 2--1)

//                    if (!mBranchGroupOperateBL.getInputData(mInputData))
//                    {
//                        CError.buildErr(this, "初始化mAgentInfoOperateBL失败");
//                        return false;
//                    }
//                    if (!mRelationOperateBL.getInputData(mInputData))
//                    {
//                        CError.buildErr(this, "初始化mRelationOperateBL失败");
//                        return false;
//                    }
//                    if (!mRearQueryDealBL.getInputData(mInputData)) {
//                        CError.buildErr(this, "初始化mRearQueryDealBL失败");
//                        return false;
//                    }
//                    try
//                    {
//                        mAgentInfoOperateBL.setmEdorNo(mEdorNo);
//                        mBranchGroupOperateBL.setmEdorNo(mEdorNo);
//                        mRelationOperateBL.setmEdorNo(mEdorNo);
//                        if (!mAgentInfoOperateBL.changeAgentGrade(
//                                mLAAssessSchema.getAgentCode(),
//                                mLAAssessSchema.getAgentGrade1()))
//                        { //先处理职级
//                            CError.buildErr(this, "修改代理人职级失败！");
//                            return false;
//                        }
//                        //建立团队
//                        if (!mBranchGroupOperateBL.createGroup(mLAAssessSchema.
//                                getAgentCode(),
//                                mLAAssessSchema.getAgentGroup(),
//                                mLAAssessSchema.getAgentSeries1()))
//                        {
//                            CError.buildErr(this, "建立新机构失败！");
//                            return false;
//                        }
//                    }
//                    catch(Exception e)
//                    {
//                        return false ;
//                    }
//
//                }
//
//            }
            //@todo
        }
        //B01职级可能在团队信息表中为主管，他降级后需要修改团队主管为空，团队人员的上级代理人为空
        if(mLAAssessSchema.getAgentGrade().equals("B01") && !mLAAssessSchema.getAgentGrade1().equals("B01") )
        {

            //改变组员的上级代理人
            if (!mAgentInfoOperateBL.changeUpAgentSet(mLAAssessSchema.
                    getAgentCode()
                    , "",
                    mLAAssessSchema.getAgentSeries1())) {
                CError.buildErr(this, "改变组员的上级代理人失败！", "AscriptControlBLF",
                                "Proceedfive");
                return false;
            }
            //如果本人是团队主管,则其上级代理人为空
            LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(mLAAssessSchema.
                    getAgentGroup());
            if(!tLABranchGroupDB.getInfo())
            {
                CError.buildErr(this, "查询机构"+mLAAssessSchema.
                    getAgentGroup()+"的信息失败！", "AscriptControlBLF",
                                "Proceedfive");
                 return false;
              }
              //如果此人是主管
              String tBranchManager=tLABranchGroupDB.getBranchManager();
              if(mLAAssessSchema.getAgentCode().equals(tBranchManager))
              {
                  //原来管理的机构主管空
                  if (!mBranchGroupOperateBL.changeBranchManager(
                          mLAAssessSchema.
                          getAgentGroup(), "")) {
                      CError.buildErr(this,
                                      "将机构" + mLAAssessSchema.getAgentGroup() +
                                      "的主管置空失败！", "AscriptControlBLF",
                                      "Proceedfive");
                      return false;
                  }
                  //改变本人的上级代理人
                  if (!mAgentInfoOperateBL.changeUpAgentSchema(mLAAssessSchema.
                          getAgentCode()
                          , "",
                          mLAAssessSchema.getAgentSeries1())) {
                      CError.buildErr(this, "改变本人的上级代理人失败！",
                                      "AscriptControlBLF",
                                      "Proceedfive");
                      return false;
                  }
              }
        }
        return true;
    }

    /**
     * 待开发
     *
     * @return boolean
     */
    private boolean Proceedtwo()
    {
        mAgentInfoOperateBL = new AscriptAgentInfoOperateBL();
        mBranchGroupOperateBL = new AscriptBranchGroupOperateBL();
        mRelationOperateBL = new AscriptRelationOperateBL();
        mRearQueryDealBL = new AscriptRearQueryDealBL();
        if (!mAgentInfoOperateBL.getInputData(mInputData))
        {
            CError.buildErr(this, "初始化mAgentInfoOperateBL失败");
            return false;
        }
        if (!mBranchGroupOperateBL.getInputData(mInputData))
        {
            CError.buildErr(this, "初始化mAgentInfoOperateBL失败");
            return false;
        }
        if (!mRelationOperateBL.getInputData(mInputData))
        {
            CError.buildErr(this, "初始化mRelationOperateBL失败");
            return false;
        }
        if (!mRearQueryDealBL.getInputData(mInputData))
        {
            CError.buildErr(this, "初始化mRearQueryDealBL失败");
            return false;
        }
        try
        {
            mAgentInfoOperateBL.setmEdorNo(mEdorNo);
            mBranchGroupOperateBL.setmEdorNo(mEdorNo);
            mRelationOperateBL.setmEdorNo(mEdorNo);
            if (!mBranchGroupOperateBL.createGroup(mLAAssessSchema.getAgentCode(),
                    mLAAssessSchema.getAgentGroup(),
                    mLAAssessSchema.getAgentSeries1()))
            {
                CError.buildErr(this, "建立新机构失败！");
                return false;
            }
            LABranchGroupSchema NewGroupDB = new LABranchGroupSchema();
            NewGroupDB.setSchema(mBranchGroupOperateBL.getNewGroup());
            if (NewGroupDB == null || NewGroupDB.getAgentGroup() == null ||
                NewGroupDB.getAgentGroup().equals(""))
            {
                CError.buildErr(this, "取得新机构失败！");
                return false;
            }
            if (!mAgentInfoOperateBL.changeAgentGrade(mLAAssessSchema.
                    getAgentCode(),
                    mLAAssessSchema.getAgentGrade1()
                ))
            {
                CError.buildErr(this, "改变职级失败！");
                return false;
            }
            LABranchGroupDB QueryGroupDB = new LABranchGroupDB();
            QueryGroupDB.setAgentGroup(mLAAssessSchema.getAgentGroup());
            if (!QueryGroupDB.getInfo())
            {
                CError.buildErr(this, "取代理人" + mLAAssessSchema.getAgentCode() +
                                "的机构信息出错！");
                return false;
            }
            if (!mAgentInfoOperateBL.changeAgentGroup(mLAAssessSchema.
                    getAgentCode(), NewGroupDB.getBranchAttr(),
                    NewGroupDB.getAgentGroup(), NewGroupDB.getBranchManager()))
            {
                CError.buildErr(this, "改变行政信息失败！");
                return false;
            }

            if (mLAAssessSchema.getAgentSeries1().equals("2"))
            {//处晋升到区
                if (!mRearQueryDealBL.CollectRearAgent(mLAAssessSchema.
                                                       getAgentCode(),
                                                       QueryGroupDB.getUpBranch(),
                                                       mLAAssessSchema.getBranchType(),
                                                       mLAAssessSchema.getBranchType2()))
                {
                    return false;
                }
            }
             else if (mLAAssessSchema.getAgentSeries1().equals("1")) {
                 //业务晋升到处
                if (!mRearQueryDealBL.CollectIntroAgent(mLAAssessSchema.
                        getAgentCode(),
                        QueryGroupDB.getAgentGroup(),
                        mLAAssessSchema.getBranchType(),
                        mLAAssessSchema.getBranchType2()))
                {
                    return false;
                }

            }

            VData AgentData = new VData();
            AgentData = mRearQueryDealBL.getAgentSet();
            if (AgentData == null || AgentData.size() <= 0)
            {
                CError.buildErr(this, "取得代理人信息集合失败！");
                return false;
            }
            LATreeSchema selfLATreeSchema = new LATreeSchema();
            selfLATreeSchema = mAgentInfoOperateBL.getCurAgentTree();
            LATreeSet rearLATreeSet = new LATreeSet();
            LATreeSet InputRearLATreeSet = new LATreeSet();
            rearLATreeSet = (LATreeSet) AgentData.getObjectByObjectName(
                    "LATreeSet", 0);
            /*
            修改推荐关系的AgentGroup
            */
           String tChangeGroup=selfLATreeSchema.getAgentGroup();
           //处
           if(mLAAssessSchema.getAgentSeries1().equals("1"))
           {
               LATreeSet tRearLATreeSet = new LATreeSet();
               tRearLATreeSet.add(selfLATreeSchema);
               tRearLATreeSet.add(rearLATreeSet);
               if (!mRelationOperateBL.changeRecomRelation(tRearLATreeSet,tChangeGroup))
               {
                   CError.buildErr(this, "修改推荐关系失败！");
                   return false;
               }
           }
           //区
           else if(mLAAssessSchema.getAgentSeries1().equals("2"))
           {
               LATreeSet tRearLATreeSet = new LATreeSet();
               tRearLATreeSet.add(selfLATreeSchema);
               if (!mRelationOperateBL.changeRecomRelation(tRearLATreeSet,tChangeGroup)) {
                   CError.buildErr(this, "修改推荐关系失败！");
                   return false;
               }
           }

            //晋升为 区
            if (mLAAssessSchema.getAgentSeries1().equals("2")) {
                if (selfLATreeSchema == null ||
                    selfLATreeSchema.getAgentCode() == null ||
                    selfLATreeSchema.getAgentCode().equals("")) {
                    return false;
                }

                InputRearLATreeSet.add(selfLATreeSchema); //晋升的时候必须把直辖组放在第一个，最大号是001
                InputRearLATreeSet.add(rearLATreeSet);
                String tGroup;
                for (int i = 1; i <= InputRearLATreeSet.size(); i++) {
                    if (i == 1) { //判断是第一条记录了，也就是新主管自身，那么改变的应该是他的直辖组的信息
                        tGroup = InputRearLATreeSet.get(i).getBranchCode();
                    } else {
                        tGroup = InputRearLATreeSet.get(i).getAgentGroup();
                    }
                    if (!mBranchGroupOperateBL.updateGroupInfo(tGroup,
                            NewGroupDB)) {
                        return false;
                    }

                    LABranchGroupSchema tLABranchGroupSchema = new
                            LABranchGroupSchema();
                    tLABranchGroupSchema = mBranchGroupOperateBL.
                                           getChangeGroupSchema();
                    //修改业绩，晋升为区以后需要修改团队信息（包括本人）
                    if (!mAscriptBusinessOperateBL.setGrptoGroup(tGroup,
                            tLABranchGroupSchema)) {
                        CError.buildErr(this,
                                        "修改代理人" + mLAAssessSchema.getAgentCode() +
                                        "的业绩信息失败！");
                        return false;
                    }

                    if (i == 1) { // //修改本人的业绩，也就是新主管自身（区经理特殊处理，branchcode 不一样）
                        if (!mAscriptBusinessOperateBL.setPertoGroup(
                                mLAAssessSchema.getAgentCode(),
                                selfLATreeSchema.getBranchCode(),
                                tLABranchGroupSchema)) {
                            CError.buildErr(this,
                                            "修改代理人" +
                                            mLAAssessSchema.getAgentCode() +
                                            "的业绩信息失败！");
                            return false;
                        }
                        //归属保单和险种表,归属应收表(只需要归属本人)
                        if (!mAscriptBusinessOperateBL.setContToGroupNew(
                                mLAAssessSchema.getAgentCode(), NewGroupDB)) {
                            CError.buildErr(this,
                                            "修改代理人" +
                                            mLAAssessSchema.getAgentCode() +
                                            "的业绩信息失败！");
                            return false;
                        }
                    }
                }
                String[] tAgentCodeSet;
                //查询组员
                tAgentCodeSet = mRearQueryDealBL.CollectGroupAgent(
                        selfLATreeSchema.getAgentCode());
                String NewBranchAttr = (String) mBranchGroupOperateBL.
                                       getCurLABranchGroupSet().get(1).
                                       getBranchAttr();
                String NewAgentGroup = (String) mBranchGroupOperateBL.
                                       getCurLABranchGroupSet().get(1).
                                       getAgentGroup();
                String NewBranchSeries = (String) mBranchGroupOperateBL.
                                         getCurLABranchGroupSet().get(1).
                                         getBranchSeries();
            }
            AgentData.add(NewGroupDB);
            //设置代理人的upagent
            if (!mAgentInfoOperateBL.changeAgentGroupSet(AgentData,
                    mBranchGroupOperateBL.getCurLABranchGroupSet())) {
                return false;
            }


             //修改业绩，晋升为处以后需要修改团队信息（包括本人）
             if (mLAAssessSchema.getAgentSeries1().equals("1")&&
                 !mLAAssessSchema.getAgentGrade1().equals("B01")) {
                 InputRearLATreeSet.add(selfLATreeSchema); //晋升的时候必须把直辖组放在第一个，最大号是001
                 InputRearLATreeSet.add(rearLATreeSet);
                 for(int i=1;i<=InputRearLATreeSet.size();i++)
                 {
                     //归属扎账表
                     if (!mAscriptBusinessOperateBL.setPertoGroup(
                             InputRearLATreeSet.
                             get(i).getAgentCode(), NewGroupDB)) {
                         CError.buildErr(this,
                                         "修改代理人" + mLAAssessSchema.getAgentCode() +
                                         "的业绩信息失败！");
                         return false;
                     }
                     //
                     //归属保单和险种表,归属应收表
                    if (!mAscriptBusinessOperateBL.setContToGroupNew(
                            InputRearLATreeSet.
                            get(i).getAgentCode(), NewGroupDB)) {
                        CError.buildErr(this,
                                        "修改代理人" + mLAAssessSchema.getAgentCode() +
                                        "的业绩信息失败！");
                        return false;
                    }

                 }
             }
            /*
            育成关系代处理
            */
           //处理向上的关系
           //断裂
           if (!mRelationOperateBL.deleteRear(selfLATreeSchema))
           {
               return false;
            }
            //处理向下的关系
            //修改为无效
            if (!mRelationOperateBL.changeRear(selfLATreeSchema))
            {
                return false;
            }
            //处理向下和向上的间接关系,selfLATreeSchema.getAgentCode不是被培养人和培养人
            //断裂
            //根据IT要求，不断裂间接关系，modified by miaoxz at 2010-7-6
//            if (!mRelationOperateBL.deleteAsRear(selfLATreeSchema))
//            {
//                return false;
//            }
            //modified by miaoxz at 2010-7-6, end here
           //形成
            if (!mRelationOperateBL.createAgentRear(selfLATreeSchema))
            {
                return false;
            }


        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }


    private boolean Proceedthree()
    {
        mAgentInfoOperateBL = new AscriptAgentInfoOperateBL();
        mBranchGroupOperateBL = new AscriptBranchGroupOperateBL();
        mRelationOperateBL = new AscriptRelationOperateBL();
        mRearQueryDealBL = new AscriptRearQueryDealBL();
        if (!mAgentInfoOperateBL.getInputData(mInputData))
        {
            return false;
        }
        if (!mBranchGroupOperateBL.getInputData(mInputData))
        {
            return false;
        }
        if (!mRelationOperateBL.getInputData(mInputData))
        {
            return false;
        }
        if (!mRearQueryDealBL.getInputData(mInputData))
        {
            return false;
        }

        try
        {
            mAgentInfoOperateBL.setmEdorNo(mEdorNo);
            mBranchGroupOperateBL.setmEdorNo(mEdorNo);
            mRelationOperateBL.setmEdorNo(mEdorNo);

            LABranchGroupDB OldGroupDB = new LABranchGroupDB();
            OldGroupDB.setAgentGroup(mLAAssessSchema.getAgentGroup());
            if (!OldGroupDB.getInfo()) {
                CError.buildErr(this, "得到代里人原来的机构信息失败！");
                return false;
            }
            //找到要归属的团队
            if (!mRearQueryDealBL.BackToRearGroup(mLAAssessSchema.getAgentCode(),
                                                  OldGroupDB,
                                                  mLAAssessSchema.getAgentSeries1(),
                                                  mLAAssessSchema.getAgentGrade1())) {
                this.mErrors.copyAllErrors(mRearQueryDealBL.mErrors);
                return false;
            }
            LABranchGroupSchema BacktoGroupDB = new LABranchGroupSchema();
            if(mRearQueryDealBL.getOldGroup()==null)
            {
            	BacktoGroupDB = null;
            }
            else
            {
               BacktoGroupDB.setSchema(mRearQueryDealBL.getOldGroup());
            }
            //如果找不到要归属的团队
            if (BacktoGroupDB == null || BacktoGroupDB.getAgentGroup() == null ||
                BacktoGroupDB.getAgentGroup().equals("")) {
                if (!Proceedfive()) {
                    return false;
                }
                return true;
            }
            //如果找到要归属的团队
            if (!mAgentInfoOperateBL.changeAgentGrade(mLAAssessSchema.getAgentCode(),
                                                      mLAAssessSchema.getAgentGrade1())) {
                CError.buildErr(this, "改变代理人职级失败！");
                return false;
            }
            if (mLAAssessSchema.getAgentSeries1().equals("0") ||
                mLAAssessSchema.getAgentGrade1().equals("B01")) { //降级为业务系列
                if (!mAgentInfoOperateBL.changeAgentGroup(mLAAssessSchema.getAgentCode(),
                                                          BacktoGroupDB.getBranchAttr(),
                                                          BacktoGroupDB.getAgentGroup(),
                                                          BacktoGroupDB.
                                                          getBranchManager())) {
                    CError.buildErr(this, "改变代理人行政信息失败！");
                    return false;
                }
            } else if (mLAAssessSchema.getAgentSeries1().equals("1")) { //降为处级经理
                LATreeSchema tLATreeSchema = new LATreeSchema();
                tLATreeSchema = mAgentInfoOperateBL.getCurAgentTree();
                if (tLATreeSchema == null || tLATreeSchema.getAgentCode() == null ||
                    tLATreeSchema.getAgentCode().equals("")) {
                    CError.buildErr(this, "取得代理人行政信息失败！");
                    return false;
                }
                if (!mAgentInfoOperateBL.changeAgentGroup(mLAAssessSchema.getAgentCode(),
                                                          "", //回归的BranchAttr还没有生成呢，后边会处理
                                                          tLATreeSchema.getBranchCode(),
                                                          BacktoGroupDB.
                                                          getBranchManager())) {
                    CError.buildErr(this, "改变代理人行政信息失败！");
                    return false;
                }
            }
            if (!mRearQueryDealBL.DisMissAgentSet(mLAAssessSchema.getAgentCode(),
                                                  mLAAssessSchema.getAgentSeries1(),
                                                  mLAAssessSchema.getAgentGrade1(),
                                                  mLAAssessSchema.getAgentSeries(),
                                                  mLAAssessSchema.getAgentGrade())) { //如果是区经理降级,则找到他的下属主管;如果是处降级,则找到他的下属业务员
                CError.buildErr(this, "解散代理人直接下属失败！");
                return false;
            }

            VData AgentData = new VData();
            AgentData = mRearQueryDealBL.getAgentSet();

            LATreeSchema selfLATreeSchema = new LATreeSchema();
            selfLATreeSchema = mAgentInfoOperateBL.getCurAgentTree();
            LATreeSet rearLATreeDBSet = new LATreeSet();
            rearLATreeDBSet = (LATreeSet) AgentData.getObjectByObjectName(
                    "LATreeSet", 0);
            /*
             修改推荐关系的AgentGroup，
             降级为业务系列，人员团队需要发生变动所以需要修改所有需要归属的人
             降级为主管，只有本人的agentgroup变动
             */
            String tChangeGroup = BacktoGroupDB.getAgentGroup();
            if (mLAAssessSchema.getAgentSeries1().equals("1")
                && !mLAAssessSchema.getAgentGrade1().equals("B01")) {
                LATreeSet tRearLATreeSet = new LATreeSet();
                tRearLATreeSet.add(selfLATreeSchema);

                if (!mRelationOperateBL.changeRecomRelation(tRearLATreeSet,
                        tChangeGroup)) {
                    CError.buildErr(this, "修改推荐关系失败！");
                    return false;
                }
            } else if (mLAAssessSchema.getAgentSeries1().equals("0")
                       || mLAAssessSchema.getAgentGrade1().equals("B01")) {
                LATreeSet tRearLATreeSet = new LATreeSet();
                tRearLATreeSet.add(selfLATreeSchema);
                tRearLATreeSet.add(rearLATreeDBSet);
                if (!mRelationOperateBL.changeRecomRelation(tRearLATreeSet,
                        tChangeGroup)) {
                    CError.buildErr(this, "修改推荐关系失败！");
                    return false;
                }
            }
            //在降级时mBranchGroupOperateBL中保存机构的顺序和晋升时的顺序是不一样的,前者降级的部长的直辖组是最后一个,后者晋升成部长的直辖组是第一个,但是统一保存的轨迹的方法是按晋升的顺序处理。所以在这里调整一下顺序再传递参数
            if (mLAAssessSchema.getAgentSeries1().equals("1") &&
                !mLAAssessSchema.getAgentGrade1().equals("B01")) { //如果是区经理降级
                rearLATreeDBSet.add(selfLATreeSchema);//本人最后处理
                for (int i = 1; i <= rearLATreeDBSet.size(); i++) {
                    if (!mBranchGroupOperateBL.updateGroupInfo(rearLATreeDBSet.
                            get(i).getAgentGroup(), BacktoGroupDB)) {
                        CError.buildErr(this, "修改代理人" + mLAAssessSchema.getAgentCode() +
                                        "的信息失败！");
                        return false;
                    }
                    LABranchGroupSchema  tLABranchGroupSchema = new LABranchGroupSchema();
                    tLABranchGroupSchema=mBranchGroupOperateBL.getChangeGroupSchema();
                     //修改业绩，降级为处以后需要修改团队信息（包括本人）
                    if (!mAscriptBusinessOperateBL.setGrptoGroup(rearLATreeDBSet.
                            get(i).getBranchCode(),tLABranchGroupSchema))
                    {
                        CError.buildErr(this,
                                        "修改代理人" + mLAAssessSchema.getAgentCode() +
                                        "的业绩信息失败！");
                        return false;
                    }
                    //归属保单和险种表,归属应收表(只需要归属本人)
                    if(rearLATreeDBSet.size()==i)
                    {//本人在最后作团队处理
                        if (!mAscriptBusinessOperateBL.setContToGroupNew(
                                mLAAssessSchema.getAgentCode(),
                                tLABranchGroupSchema))
                        {
                            CError.buildErr(this,
                                            "修改代理人" +
                                            mLAAssessSchema.getAgentCode() +
                                            "的业绩信息失败！");
                            return false;
                        }
                    }
                }
                rearLATreeDBSet.remove(selfLATreeSchema);
                for (int i = 1; i <= rearLATreeDBSet.size(); i++)
                {
                    if (!mAgentInfoOperateBL.changeManagerInfo(rearLATreeDBSet.get(i),
                            BacktoGroupDB.getBranchManager()))
                    {
                        CError.buildErr(this, "改变代理人行政信息失败！");
                        return false;
                    }

                }

            }
            if (mLAAssessSchema.getAgentSeries().equals("1")) { //如果是处经理降级, 归属业务人员到其他团队(BacktoGroupDB)
                //本人已经过归属了，只要处理它下面的业务员
                for (int i = 1; i <= rearLATreeDBSet.size(); i++) {
                    if (!mAgentInfoOperateBL.updateAgentInfo(rearLATreeDBSet.
                            get(i).
                            getAgentCode(),
                            BacktoGroupDB)) {
                        CError.buildErr(this, "改变代理人行政信息失败！");
                        return false;
                    }
                }
                //修改业绩，降级为业务以后需要修改团队信息（不包括本人），本人在下面处理
                for (int i = 1; i <= rearLATreeDBSet.size(); i++) {
                    if (!mAscriptBusinessOperateBL.setPertoGroup(
                            rearLATreeDBSet.
                            get(i).getAgentCode(),
                            BacktoGroupDB)) {
                        CError.buildErr(this,
                                        "修改代理人" + rearLATreeDBSet.
                                        get(i).getAgentCode() +
                                        "的业绩信息失败！");
                        return false;
                    }
                    //归属保单和险种表,归属应收表(不包括本人),本人在下面处理
                    if (!mAscriptBusinessOperateBL.setContToGroupNew(
                            rearLATreeDBSet.get(i).getAgentCode(),
                            BacktoGroupDB))
                    {
                        CError.buildErr(this,
                                        "修改代理人" +
                                        mLAAssessSchema.getAgentCode() +
                                        "的业绩信息失败！");
                        return false;
                    }
                }
                //修改本人的信息
                if (!mAscriptBusinessOperateBL.setPertoGroup(selfLATreeSchema.getAgentCode(),
                             BacktoGroupDB))
                {
                    CError.buildErr(this,
                                    "修改代理人" + mLAAssessSchema.getAgentCode() +
                                    "的业绩信息失败！");
                    return false;
                }
                //归属保单和险种表,归属应收表(只需要归属本人)
                if (!mAscriptBusinessOperateBL.setContToGroupNew(
                        mLAAssessSchema.getAgentCode(),
                        BacktoGroupDB)) {
                    CError.buildErr(this,
                                    "修改代理人" +
                                    mLAAssessSchema.getAgentCode() +
                                    "的业绩信息失败！");
                    return false;
                    }
            }




            AgentData.add(BacktoGroupDB);

            if (!mBranchGroupOperateBL.backupThenStopGroup(OldGroupDB.getSchema())) {
                CError.buildErr(this, "撤销机构失败！", "AscriptControlBLF", "Proceedthree");
                return false;
            }

            /*
                         育成关系处理
             */
            if (!mRelationOperateBL.BreakAgentRear(selfLATreeSchema)) {
                CError.buildErr(this, "断开血缘关系失败！");
                return false;
            }
        }

        catch (Exception e)
        {
              return false;
        }
        return true;
    }
    //修改代理人的考核日期,职级没有变化的人需要修改考核开始日期
    private boolean ProceedFour()
    {
        mAgentInfoOperateBL = new AscriptAgentInfoOperateBL();
        mRearQueryDealBL = new AscriptRearQueryDealBL();
        if (!mAgentInfoOperateBL.getInputData(mInputData)) {
            CError.buildErr(this, "初始化mAgentInfoOperateBL失败");
            return false;
        }
        mAgentInfoOperateBL.setmEdorNo(mEdorNo);
        if (!mAgentInfoOperateBL.changeAStartDate(
                mLAAssessSchema.getAgentCode())) {
            CError.buildErr(this, "修改代理人考核开始时间失败！");
            return false;
        }
        return true;
    }
    /*
     找不到血缘关系的降级区主管的处理过程
     */
    private boolean Proceedfive()
    {
        if (mLAAssessSchema.getAgentSeries().equals("1")) { //无法回归的是一个处，那么只要改变它的职级就可以了
            if (!mAgentInfoOperateBL.changeAgentGrade(mLAAssessSchema.
                                                      getAgentCode()
                                                      ,
                                                      mLAAssessSchema.getAgentGrade1())) {
                CError.buildErr(this, "改变人员职级失败！");
                return false;
            }
            //原来管理的机构主管空
            if (!mBranchGroupOperateBL.changeBranchManager(mLAAssessSchema.
                    getAgentGroup(), "")) {
                CError.buildErr(this, "将机构" + mLAAssessSchema.getAgentGroup() +
                                "的主管置空失败！", "AscriptControlBLF", "Proceedfive");
                return false;
            }
            //改变组员的上级代理人
            if (!mAgentInfoOperateBL.changeUpAgentSet(mLAAssessSchema.
                                                      getAgentCode()
                                                      , "",
                                                      mLAAssessSchema.getAgentSeries1())) {
                CError.buildErr(this, "改变组员的上级代理人失败！", "AscriptControlBLF",
                                "Proceedfive");
                return false;
            }
            //改变本人的上级代理人
            if (!mAgentInfoOperateBL.changeUpAgentSchema(mLAAssessSchema.
                                                      getAgentCode()
                                                      , "",
                                                      mLAAssessSchema.getAgentSeries1())) {
                CError.buildErr(this, "改变本人的上级代理人失败！", "AscriptControlBLF",
                                "Proceedfive");
                return false;
            }

            LATreeSchema selfLATreeSchema = mAgentInfoOperateBL.getCurAgentTree();
            //切断关系,向上的关系断开,向下的关系不一定断开
            if (!mRelationOperateBL.BreakAgentRear(selfLATreeSchema)) {
                CError.buildErr(this, "断开血缘关系失败！");
                return false;
            }

            return true;
        }
        //无法回归的是一个区
        //改变职级
        if (!mAgentInfoOperateBL.changeAgentGrade(mLAAssessSchema.getAgentCode(),
                                                  mLAAssessSchema.getAgentGrade1()
            )) {
            CError.buildErr(this, "改变人员职级失败！");
            return false;
        }

        //改变所属机构
        if (!mAgentInfoOperateBL.changeAgentGroup(mLAAssessSchema.getAgentCode(),
                                                  "",
                                                  mAgentInfoOperateBL.
                                                  getCurAgentTree().
                                                  getBranchCode(), "")) {
            CError.buildErr(this, "改变人员行政信息失败！", "AscriptControlBLF", "Proceedfive");
            return false;
        }
        //修改业绩，降级为处以后需要修改团队信息（只修改本人）
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeSchema= mAgentInfoOperateBL.getCurAgentTree();
        tLABranchGroupDB.setAgentGroup(tLATreeSchema.getBranchCode());
        tLABranchGroupDB.getInfo();
        tLABranchGroupSchema=tLABranchGroupDB.getSchema();
         if (!mAscriptBusinessOperateBL.setPertoGroup(mLAAssessSchema.getAgentCode(),
                                                      tLABranchGroupSchema))
        {
           CError.buildErr(this,
                           "修改代理人" + mLAAssessSchema.getAgentCode() +
                           "的业绩信息失败！");
           return false;
        }
        //归属保单和险种表,归属应收表(只需要归属本人)
        if (!mAscriptBusinessOperateBL.setContToGroupNew(
                mLAAssessSchema.getAgentCode(), tLABranchGroupSchema)) {
            CError.buildErr(this,
                            "修改代理人" + mLAAssessSchema.getAgentCode() +
                            "的业绩信息失败！");
            return false;
        }
        //原来管理的机构主管空
        if (!mBranchGroupOperateBL.changeBranchManager(mLAAssessSchema.
                getAgentGroup(), "")) {
            CError.buildErr(this,
                            "将机构" + mLAAssessSchema.getAgentGroup() + "的主管置空失败！");
            return false;
        }

        //改变组长的上级代理人
        if (!mAgentInfoOperateBL.changeUpAgentSet(mLAAssessSchema.getAgentCode(),
                                                  "",
                                                  mLAAssessSchema.getAgentSeries1())) {
            CError.buildErr(this, "改变主任的上级代理人失败！");
            return false;
        }
        //建立一个空的直辖组，没有组长(保证每个区都有直辖处 )
        if (!mBranchGroupOperateBL.createGroup("",
                                               mAgentInfoOperateBL.getCurAgentTree().
                                               getBranchCode(), "1")) {
            CError.buildErr(this, "建立新的直辖组失败！");
            return false;
        }
        //改变原来直辖组的直辖关系
        if (!mBranchGroupOperateBL.changeUpBranchAttr(mAgentInfoOperateBL.
                                                      getCurAgentTree().
                                                      getBranchCode(), "0")) {
            CError.buildErr(this, "改变机构直辖属性失败！");
            return false;
        }
        //切断关系
        LATreeSchema selfLATreeSchema = mAgentInfoOperateBL.getCurAgentTree();
        if (!mRelationOperateBL.BreakAgentRear(selfLATreeSchema)) {
            CError.buildErr(this, "断开血缘关系失败！", "AscriptControlBLF", "Proceedfive");
            return false;
        }
        //修改推荐关系的AgentGroup,只修改本人,其他人没有变化,(本人是区经理AgentGroup会变化)
        LATreeSet tRearLATreeSet = new LATreeSet();
        tRearLATreeSet.add(selfLATreeSchema);
        if (!mRelationOperateBL.changeRecomRelation(tRearLATreeSet,selfLATreeSchema.getAgentGroup())) {
            CError.buildErr(this, "修改推荐关系失败！");
            return false;
        }

        return true;
    }


    /*
     设置上一职级的止期为考核月的工作最后一天,新职级的起期为考核月下一月的工作第一天
     */
    private boolean getAssessStartEndDate()
    {
        try
        {
            ExeSQL tExeSQL = new ExeSQL();
            SSRS dateR = new SSRS();
            String strSQL;
            //止期：
            strSQL =
                    "select EndDate from LAStatSegment where stattype='5' and yearmonth=" +
                    mIndexCalNo + "";
            dateR = tExeSQL.execSQL(strSQL);
            if (tExeSQL.mErrors.needDealError())
            {
                return false;
            }
            if (dateR.MaxRow <= 0)
            {
                return false;
            }
            mOldGradeEndDate = dateR.GetText(1, 1);
            //起期：
            mNewGradeStartDate = PubFun.calDate(mOldGradeEndDate, 1, "D", null);
            //设置业绩归属时的日期
            mAscriptBusinessOperateBL.setAstartDate(mNewGradeStartDate);
            mAscriptBusinessOperateBL.setGlobalInput(mGlobalInput);
        }
        catch (Exception e)
        {
        }
        return true;
    }

    private boolean InitBL()
    {
        if (!getAssessStartEndDate())
        {
            return false;
        }
        if (!inputDataToBL())
        {
            return false;
        }
        return true;
    }

    /**
     * 提交数据
     * @return boolean
     */
    private boolean SubmitToBL()
    {
        if (prepareOutputToBLSData())
        {
            AscriptBL tAscriptBL = new AscriptBL();
            if (!tAscriptBL.submitData(mOutputDataBLS, mChoice))
            {
                CError.buildErr(this, "准备提交到BLS的数据失败！");
                return false;
            }
        }
        return true;
    }
    /**
     * 数据容器转换
     * @param srcData VData
     * @param destData VData
     * @return boolean
     */
    private boolean transVData(VData srcData, VData destData)
    {
        if (srcData.size() > 0)
        {
            for (int i = 0; i < srcData.size(); i++)
            {
                try
                {
                    destData.add(srcData.get(i));
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private boolean updateAssessMain()
    {
        LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
        tLAAssessMainDB.setAgentGrade(mLAAssessSchema.getAgentGrade());
        tLAAssessMainDB.setAssessType("00");
        tLAAssessMainDB.setIndexCalNo(mIndexCalNo);
        tLAAssessMainDB.setBranchType(mBranchType);
        tLAAssessMainDB.setManageCom(mLAAssessSchema.getManageCom());
        if (!tLAAssessMainDB.getInfo())
        {
            CError.buildErr(this,"查询机构" + mManageCom + "下的职级" + mLAAssessSchema.getAgentGrade() +
                    "的考评主信息失败！不能做归属！");
            return false;
        }
        mLAAssessMainSchema.setSchema(tLAAssessMainDB.getSchema());
        mLAAssessMainSchema.setState("2");
        mLAAssessMainSchema.setConfirmCount(mLAAssessMainSchema.getConfirmCount() + 1);
        return true;
    }
    public static void main(String[] args)
    {

        String manageCom = "86110000";
        String agentSeries = "0";
        String indexcalno = "200703";
        String branchtype = "1";
        String branchtype2 = "01";
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";

        System.out.println("B11".substring(0));
        VData input = new VData();
        input.add(manageCom);
        input.add(indexcalno);
        input.add(branchtype);
        input.add(branchtype2);
        input.add(agentSeries);
        input.add(tGlobalInput);

//        AscriptControlBLF tAscriptControlBLF = new AscriptControlBLF();
//        if (tAscriptControlBLF.SubmitData(input))
//        {
//            System.out.println("ok!!");
//        }
//        else
//        {
//            System.out.println(tAscriptControlBLF.mErrors.getFirstError().
//                               toString());
//            System.out.println("fail!!");
//        }
    }
}
