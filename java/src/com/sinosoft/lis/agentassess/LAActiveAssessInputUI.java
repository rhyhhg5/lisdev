/*
 * <p>ClassName: LAAssessInputUI </p>
 * <p>Description: LAAssessUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-06-21
 */
package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;

public class LAActiveAssessInputUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessAccessorySchema mLAAssessAccessorySchema = new LAAssessAccessorySchema();
    public LAActiveAssessInputUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Start LAAssessInput UI Submit...");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        try {
            System.out.println("Start LAAssessInput UI Submit...");
            LAActiveAssessInputBL tLAActiveAssessInputBL = new LAActiveAssessInputBL();
            tLAActiveAssessInputBL.submitData(mInputData, mOperate);
            //如果有需要处理的错误，则返回
            if (tLAActiveAssessInputBL.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAActiveAssessInputBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAssessInputUI";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (mOperate.equals("QUERY||MAIN")) {
                this.mResult.clear();
                this.mResult = tLAActiveAssessInputBL.getResult();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        mInputData = null;
        System.out.println("End LAAssessInput UI Submit...");
        return true;
    }

    public static void main(String[] args)
    {
        //xjh add
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        LAAssessAccessorySchema tLAAssessAccessorySchema = new LAAssessAccessorySchema();
        tLAAssessAccessorySchema.setAgentCode("1201000011");
        tLAAssessAccessorySchema.setAgentGrade("B02");
        tLAAssessAccessorySchema.setAgentGrade1("B11");
        tLAAssessAccessorySchema.setAgentGroup("000000000499");
        tLAAssessAccessorySchema.setAgentGroupNew("861200000110");
        tLAAssessAccessorySchema.setStandAssessFlag("0");
        tLAAssessAccessorySchema.setAssessType("00");
        tLAAssessAccessorySchema.setIndexCalNo("200610");
        tLAAssessAccessorySchema.setBranchType("1");
        tLAAssessAccessorySchema.setBranchType2("01");
        tLAAssessAccessorySchema.setAgentSeries("1");
        tLAAssessAccessorySchema.setAgentSeries1("2");
        tLAAssessAccessorySchema.setManageCom("86120000");
        VData tVData = new VData();
        tVData.add(tG);
        tVData.addElement(tLAAssessAccessorySchema);
        tVData.addElement("1");
        tVData.addElement("01");
        LAActiveAssessInputUI mLAAssessInputUI = new LAActiveAssessInputUI();
        mLAAssessInputUI.submitData(tVData, "INSERT||MAIN");
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mLAAssessAccessorySchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessAccessoryInputUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //此处增加一些校验代码
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
            "GlobalInput", 0));
        this.mLAAssessAccessorySchema = (LAAssessAccessorySchema) cInputData.
                                        getObjectByObjectName(
                "LAAssessAccessorySchema", 0);
        System.out.println("Start LAAssessInput UI getinputdata Submit...");
        if(mLAAssessAccessorySchema!=null)
        {
            System.out.println("Start LAAssessInput UI getinputdata Submit...111");
        }

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessAccessoryInputUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 返回数据
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }
}
