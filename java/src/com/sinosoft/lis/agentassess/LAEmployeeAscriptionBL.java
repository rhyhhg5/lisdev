package com.sinosoft.lis.agentassess;

/**
 * <p>Title: 员工制组织归属 </p>
 * <p>Description: 员工制组织归属 </p>
 * <p>Copyright: Copyright (c) 2004-02-11</p>
 * <p>Company: sinosoft</p>
 * @author LL
 * @version 1.0
 */
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LAWelfareInfoDB;
import com.sinosoft.lis.db.LAWelfareRadixDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LAWelfareInfoBSchema;
import com.sinosoft.lis.schema.LAWelfareInfoSchema;
import com.sinosoft.lis.schema.LAWelfareRadixSchema;
import com.sinosoft.lis.vschema.LAAssessAccessorySet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LAWelfareInfoBSet;
import com.sinosoft.lis.vschema.LAWelfareInfoSet;
import com.sinosoft.lis.vschema.LAWelfareRadixSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class LAEmployeeAscriptionBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LATreeBSet mLATreeBSet = new LATreeBSet(); //用来存放要备份的行政信息
    private LATreeSet mLATreeSet = new LATreeSet(); //用来存放新的行政信息
    private LAWelfareInfoSet nLAWelfareInfoSet = new LAWelfareInfoSet(); //用来存放要删除的信息
    private LAWelfareInfoSet mLAWelfareInfoSet = new LAWelfareInfoSet(); //用来存放新的信息
    private LAWelfareInfoBSet mLAWelfareInfoBSet = new LAWelfareInfoBSet(); //用来存放要备份的信息
    //归属完毕后要把状态位设置为2，用来更新此条记录
    private LAAssessAccessorySchema mLAAssessAccessorySchema = new
            LAAssessAccessorySchema();

    /** 归属时用到的变量 */
    private String mYear; //计算年
    private String mMonth; //计算月
    private String mIndexCalNo; //计算编码
    private String mManageCom; //机构代码
    private String mAgentCode; //存放当前处理的代理人编码
    private String mBranchCode; //存放当前处理的代理人考核后的员工制建议待遇职级

    /** 取得系统日期、时间 */
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public LAEmployeeAscriptionBL()
    {

    }

    public static void main(String[] args)
    {
        LAEmployeeAscriptionBL tLAEmployeeAscriptionBL = new
                LAEmployeeAscriptionBL();
        //1 - 测试参数传入 状态：正确
        String tManageCom = "86110000";
        String tIndexCalNo = "200401";
        VData cInputData;
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";

        //1 - 取出某个管理机构下要进行员工制归属处理的业务员
        String tSQL = "select * from LAAssessAccessory where IndexCalNo = '" +
                      tIndexCalNo +
                      "' and ManageCom like '" + tManageCom + "%' " +
                      " and AssessType = '01'"
                      + " and agentcode = '8611001370'";
        LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
        LAAssessAccessorySet tLAAssessAccessorySet = new LAAssessAccessorySet();
        tLAAssessAccessorySet = tLAAssessAccessoryDB.executeQuery(tSQL);
        System.out.println("查询业务员Sql  : " + tSQL);
        System.out.println("要归属的员工个数: " + tLAAssessAccessorySet.size());

        // 2 - 逐个处理每个业务员的归属
        for (int i = 1; i <= tLAAssessAccessorySet.size(); i++)
        {
            //准备好往后台传的数据
            cInputData = new VData();
            cInputData.add(tGlobalInput);
            cInputData.add(tManageCom);
            cInputData.add(tIndexCalNo);
            cInputData.add(tLAAssessAccessorySet.get(i).getAgentCode());
            cInputData.add(tLAAssessAccessorySet.get(i).getAgentGrade1());
            //3 - 调用LAEmployeeAscriptionBL进行计算
            tLAEmployeeAscriptionBL.submitData(cInputData, "INSERT||MAIN");
        }

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAEmployeeAscriptionBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start LAEmployeeAscriptionBL Submit...");
        LAEmployeeAscriptionBLS tLAEmployeeAscriptionBLS = new
                LAEmployeeAscriptionBLS();
        tLAEmployeeAscriptionBLS.submitData(this.mInputData, cOperate);
        System.out.println("End LAEmployeeAscriptionBL Submit...");
        //如果有需要处理的错误，则返回
        if (tLAEmployeeAscriptionBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAEmployeeAscriptionBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //获得前台传过来的数据
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mManageCom = (String) cInputData.getObject(1);
//    this.mYear = (String)cInputData.getObject(2);
//    this.mMonth = (String)cInputData.getObject(3);
//    this.mIndexCalNo = mYear + mMonth;
        this.mIndexCalNo = (String) cInputData.getObject(2);
        this.mAgentCode = (String) cInputData.getObject(3);
        this.mBranchCode = (String) cInputData.getObject(4);
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //1 - 处理某个管理机构下业务员的归属
        if (!CalPerAscript())
        {
            System.out.println("Error:" + this.mErrors.getFirstError());
            return false;
        }

        return true;
    }

    /**
     * 把处理后的数据保存，准备传到后台
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLATreeSet); //1 做修改操作
            this.mInputData.add(this.mLATreeBSet); //2 做插入操作
            this.mInputData.add(this.nLAWelfareInfoSet); //3 做删除操作
            this.mInputData.add(this.mLAWelfareInfoSet); //4 做插入操作
            this.mInputData.add(this.mLAWelfareInfoBSet); //5 做插入操作
            this.mInputData.add(this.mLAAssessAccessorySchema); //6 做修改操作
//      this.mInputData.add(this.mLAComSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /****************************************************/
    /**********           归属处理函数部分        **********/
    /****************************************************/
    /**
     * 处理某个管理机构（8位）下业务员的归属
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean CalManageComAscript()
    {
        //1 - 取出某个管理机构下要进行员工制归属处理的业务员
        String tSQL = "select * from LAAssessAccessory where IndexCalNo = '" +
                      this.mIndexCalNo +
                      "' and ManageCom like '" + this.mManageCom + "%' " +
                      " and AssessType = '01'";
        LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
        LAAssessAccessorySet tLAAssessAccessorySet = new LAAssessAccessorySet();
        tLAAssessAccessorySet = tLAAssessAccessoryDB.executeQuery(tSQL);
        System.out.println("查询业务员Sql  : " + tSQL);
        System.out.println("要归属的员工个数: " + tLAAssessAccessorySet.size());

        // 2 - 逐个处理每个业务员的归属
        for (int i = 1; i <= tLAAssessAccessorySet.size(); i++)
        {
            //获得当前业务员的 代理人编码 和 建议职级
            this.mAgentCode = tLAAssessAccessorySet.get(i).getAgentCode();
            this.mBranchCode = tLAAssessAccessorySet.get(i).getCalAgentGrade();
            //3 - 处理每个业务员的归属
            if (!CalPerAscript())
            {
                System.out.println("Error:" + this.mErrors.getFirstError());
                return false;
            }
        }

        return true;
    }

    /**
     * 处理某个业务员的归属
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean CalPerAscript()
    {
        //1 - 根据建议职级进行不同的处理
        //a -  建议职级为Y00
        String tOldBranchCode = ""; //存放原来的待遇职级
        //用来访问LAWelfareInfo表 ?????这样做会不会影响效率
        LAWelfareInfoSchema tLAWelfareInfoSchema = new LAWelfareInfoSchema();
        LAWelfareInfoSet tLAWelfareInfoSet = new LAWelfareInfoSet();
        LAWelfareInfoDB tLAWelfareInfoDB = new LAWelfareInfoDB();

        /******************* 建议职级为Y00 *********************/
        if (this.mBranchCode.equals("Y00"))
        {
            //获得业务员原来的待遇职级
            tOldBranchCode = getOldBranchCode(this.mAgentCode);
            //判断待遇职级是否发生变化
            if (this.mBranchCode.equals(tOldBranchCode))
            {
                //准备好归属完毕后对LAAssessAccessory表修改的数据
                if (!updateLAAssessAccessory())
                {
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }
                return true;
            }
            else
            {
                //修改LATree表中该代理人的待遇职级，对LATree备份
                if (!lATreeBackUp(this.mAgentCode))
                {
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }
                //准备好归属完毕后对LAAssessAccessory表修改的数据
                if (!updateLAAssessAccessory())
                {
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }
                return true;
            }

        }
        /******************* 建议职级为Y01或者Y02 *********************/
        else if (this.mBranchCode.equals("Y01") ||
                 this.mBranchCode.equals("Y02"))
        {
            //获得业务员原来的待遇职级
            tOldBranchCode = getOldBranchCode(this.mAgentCode);

            //判断待遇职级是否发生变化
            if (!this.mBranchCode.equals(tOldBranchCode))
            {
                //修改LATree表中该代理人的待遇职级，对LATree备份
                if (!lATreeBackUp(this.mAgentCode))
                {
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }
            }

            //如果LAWelfareInfo表原来有此业务员的信息则备份
            //准备好要备份的LAWelfareInfo表中的数据
            tLAWelfareInfoSchema.setAgentCode(this.mAgentCode);
            tLAWelfareInfoDB.setSchema(tLAWelfareInfoSchema);
            tLAWelfareInfoSet = tLAWelfareInfoDB.query();
            //用来保存要删除的LAWelfareInfo表中的数据
            this.nLAWelfareInfoSet.add(tLAWelfareInfoSet);
            if (tLAWelfareInfoSet.size() != 0)
            {
                //备份LAWelfareInfo表
                if (!lAWelfareInfoBackUp(tLAWelfareInfoSet))
                {
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }
            }

            //准备好要插入LAWelfareInfo表的数据
            if (!prePareLAWelfareInfoData())
            {
                System.out.println("Error:" + this.mErrors.getFirstError());
                return false;
            }

            //准备好归属完毕后对LAAssessAccessory表修改的数据
            if (!updateLAAssessAccessory())
            {
                System.out.println("Error:" + this.mErrors.getFirstError());
                return false;
            }
        }

        /******************* 建议职级为Y03以上 *********************/
        else if (this.mBranchCode.compareTo("Y03") >= 0)
        {
            //获得业务员原来的待遇职级
            tOldBranchCode = getOldBranchCode(this.mAgentCode);

            //判断待遇职级是否发生变化
            if (!this.mBranchCode.equals(tOldBranchCode))
            {
                //修改LATree表中该代理人的待遇职级，对LATree备份
                if (!lATreeBackUp(this.mAgentCode))
                {
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }
            }

            //如果LAWelfareInfo表原来有此业务员的信息则备份
            //准备好要备份的LAWelfareInfo表中的数据
            tLAWelfareInfoSchema.setAgentCode(this.mAgentCode);
            tLAWelfareInfoDB.setSchema(tLAWelfareInfoSchema);
            tLAWelfareInfoSet = tLAWelfareInfoDB.query();
            //用来保存要删除的LAWelfareInfo表中的数据
            this.nLAWelfareInfoSet.add(tLAWelfareInfoSet);
            if (tLAWelfareInfoSet.size() != 0)
            {
                //备份LAWelfareInfo表
                if (!lAWelfareInfoBackUp(tLAWelfareInfoSet))
                {
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }
            }

            //准备好要插入LAWelfareInfo表的数据
            if (!prePareLAWelfareInfoData())
            {
                System.out.println("Error:" + this.mErrors.getFirstError());
                return false;
            }

            //准备好归属完毕后对LAAssessAccessory表修改的数据
            if (!updateLAAssessAccessory())
            {
                System.out.println("Error:" + this.mErrors.getFirstError());
                return false;
            }
        }
        return true;
    }

    /****************************************************/
    /**********           公用函数部分        *************/
    /****************************************************/
    /**
     * 获得业务员原来的待遇职级
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private String getOldBranchCode(String tAgentCode)
    {
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(tAgentCode);

        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBL";
            tError.functionName = "getBranchCode";
            tError.errorMessage = "代理人" + this.mAgentCode +
                                  "查询LATree表中原待遇职级信息出错！";
            this.mErrors.addOneError(tError);
            return "00";
        }
        String oldBranchCode = "";
        //判断是否为空，为空的话默认为Y00
        if (tLATreeDB.getBranchCode() == null ||
            tLATreeDB.getBranchCode().equals(""))
        {
            oldBranchCode = "Y00";
        }
        else
        {
            oldBranchCode = tLATreeDB.getBranchCode();
        }

        return oldBranchCode;
    }

    /****************************************************/
    /**********           更新、备份函数部分      **********/
    /****************************************************/
    /**
     * 备份LATree表
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean lATreeBackUp(String tAgentCode)
    {
        String tEdorNo = "";
        //查找原来的业务员信息
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(this.mAgentCode);

        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBL";
            tError.functionName = "lATreeBackUp";
            tError.errorMessage = "代理人" + this.mAgentCode + "查询LATree表信息出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //备份修改前 业务员行政信息
        LATreeBSchema tLATreeBSchema = new LATreeBSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLATreeBSchema, tLATreeDB.getSchema());
        tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        tLATreeBSchema.setEdorNO(tEdorNo);
        tLATreeBSchema.setRemoveType("01"); // ???????????
        tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
        tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
        tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
        tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
        tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
        tLATreeBSchema.setMakeDate(this.mCurrentDate);
        tLATreeBSchema.setMakeTime(this.mCurrentTime);
        tLATreeBSchema.setModifyDate(this.mCurrentDate);
        tLATreeBSchema.setModifyTime(this.mCurrentTime);
        tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
        tLATreeBSchema.setIndexCalNo(this.mIndexCalNo);
        this.mLATreeBSet.add(tLATreeBSchema);

        //修改 业务员行政信息
        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeSchema.setSchema(tLATreeDB.getSchema());
        tLATreeSchema.setBranchCode(this.mBranchCode);
        tLATreeSchema.setModifyDate(this.mCurrentDate);
        tLATreeSchema.setModifyTime(this.mCurrentTime);
        tLATreeSchema.setOperator(this.mGlobalInput.Operator);
        this.mLATreeSet.add(tLATreeSchema);
        return true;
    }

    /**
     * 备份LAWelfareInfo表
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean lAWelfareInfoBackUp(LAWelfareInfoSet tLAWelfareInfoSet)
    {
        String tEdorNo = "";
        tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        Reflections tReflections = new Reflections();
        //代理人福利信息表LAWelfareInfo表中数据的备份
        LAWelfareInfoBSchema tLAWelfareInfoBSchema;
        for (int i = 1; i <= tLAWelfareInfoSet.size(); i++)
        {
            tLAWelfareInfoBSchema = new LAWelfareInfoBSchema();
            tReflections.transFields(tLAWelfareInfoBSchema,
                                     tLAWelfareInfoSet.get(i));
            //修改：应该用同一个转储号
            //tEdorNo = PubFun1.CreateMaxNo("EdorNo",20);
            tLAWelfareInfoBSchema.setEdorNo(tEdorNo);
            tLAWelfareInfoBSchema.setEdorType("01");
            tLAWelfareInfoBSchema.setMakeDate(this.mCurrentDate);
            tLAWelfareInfoBSchema.setMakeTime(this.mCurrentTime);
            this.mLAWelfareInfoBSet.add(tLAWelfareInfoBSchema);
        }

        return true;
    }

    /**
     * 准备好往LAWelfareInfo表插入的数据
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean prePareLAWelfareInfoData()
    {
        // 总体概述：对享受不同待遇职级的业务员进行不同处理：
        // Y01、Y02只享受社保费用扶持金；
        // Y02以上待遇职级享受 底薪+四险一金；

        LAWelfareInfoSchema tLAWelfareInfoSchema;
        String tDate = "";
        /*********************************************************/
        //1 - 对Y01、Y02只享受社保费用扶持金，不享受四险一金，所以只插入一条记录.
        //    AClass = '12' 说明此人只享受社保费用扶持金，在此取出社保费用扶持金
        LAWelfareRadixSet tLAWelfareRadixSet = new LAWelfareRadixSet();
        LAWelfareRadixDB tLAWelfareRadixDB = new LAWelfareRadixDB();
        String tSql1 = "select * from LAWelfareRadix where ManageCom ='" +
                       this.mManageCom.substring(0, 6)
                       + "' and AgentGrade = '" + this.mBranchCode + "'"
                       + " and AClass = '12'";
        tLAWelfareRadixSet = tLAWelfareRadixDB.executeQuery(tSql1);

        //获得此业务员职级
        String tAgentGrade = this.getAgentGrade();
        if (tAgentGrade.equals("00"))
        {
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBL";
            tError.functionName = "prePareLAWelfareInfoData()";
            tError.errorMessage = "代理人" + this.mAgentCode + "查询LATree表职级信息出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (tLAWelfareRadixSet.size() != 0) //说明待遇职级为Y01或Y02
        {
            tLAWelfareInfoSchema = new LAWelfareInfoSchema();
            //置值
            tLAWelfareInfoSchema.setIndexCalNo(this.mIndexCalNo);
            tLAWelfareInfoSchema.setBranchType("1");
            tLAWelfareInfoSchema.setAgentCode(this.mAgentCode);
            tLAWelfareInfoSchema.setAgentGrade(tAgentGrade); //???????
            tLAWelfareInfoSchema.setManageCom(this.mManageCom);
            tLAWelfareInfoSchema.setBranchAttr(AgentPubFun.getAgentBranchAttr(this.
                    mAgentCode));
            tLAWelfareInfoSchema.setAClass(tLAWelfareRadixSet.get(1).getAClass());
            tLAWelfareInfoSchema.setWelfareType("01"); //?????? 发放
            //福利金额 --??????
            tLAWelfareInfoSchema.setSumMoney(tLAWelfareRadixSet.get(1).
                                             getDrawMoney());

            tLAWelfareInfoSchema.setServeYear(tLAWelfareRadixSet.get(1).
                                              getServeYear());
            //福利发放日期
            tDate = this.getWSendDate();
            tLAWelfareInfoSchema.setWSendDate(tDate);
            tLAWelfareInfoSchema.setWValidDate(tDate);
            tDate = this.getWSendEndDate(tDate,
                                         tLAWelfareRadixSet.get(1).getServeYear());
            tLAWelfareInfoSchema.setWSendEndDate(tDate);
            tLAWelfareInfoSchema.setWValidEndDate(tDate);
            tLAWelfareInfoSchema.setDoneFlag("0");
            tLAWelfareInfoSchema.setMakeDate(this.mCurrentDate);
            tLAWelfareInfoSchema.setMakeTime(this.mCurrentTime);
            tLAWelfareInfoSchema.setModifyDate(this.mCurrentDate);
            tLAWelfareInfoSchema.setModifyTime(this.mCurrentTime);
            tLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);

            //将结果加入
            this.mLAWelfareInfoSet.add(tLAWelfareInfoSchema);

            return true;
        }

        //如果当前职级为A04以上职级，则判断其是否为同业衔接期内的同业衔接主管
        String tReturn = this.isLinkPeriodMan(this.mAgentCode);
        if (tReturn.equals("0")) //调用函数有错误
        {
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBL";
            tError.functionName = "prePareLAWelfareInfoData()";
            tError.errorMessage = "调用函数isLinkPeriodMan()出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //如果返回1，说明衔接期未过，此时查询出某个职级对应的享受期限
        int tServerYear = 0;
        if (tReturn.equals("1"))
        {
            ExeSQL tExeSQL = new ExeSQL();
            String tSql6 = "select trim(code2) from ldcoderela "
                           + " where relatype = 'linkwelfareperiod' "
                           + " and code1 = '" + tAgentGrade + "'";
            //校验是否录入考核期
            String tempStr = tExeSQL.getOneValue(tSql6);
            //校验是否出错
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeAscriptionBL";
                tError.functionName = "prePareLAWelfareInfoData";
                tError.errorMessage = "查询衔接期享受期限出错！";
                this.mErrors.addOneError(tError);
                System.out.println("查询衔接期享受期限出错！");
                return false;
            }
            //this.mAssessPeriod = Integer.parseInt(tExeSQL.getOneValue(tSql1));
            if (tempStr == null || tempStr.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LAEmployeeAscriptionBL";
                tError.functionName = "prePareLAWelfareInfoData";
                tError.errorMessage = "查询衔接期享受期限信息出错,没有录入衔接期享受期限！";
                this.mErrors.addOneError(tError);
                System.out.println("查询衔接期享受信息出错,没有录入衔接期享受期限！");
                return false;
            }
            //设置享受期限
            tServerYear = Integer.parseInt(tempStr);
        }

        // 2- 对Y02以上职级业务员，应该享受底薪+四险一金待遇，所以要插入多条记录
        //    先取出不同待遇职级对应的底薪标准
        //    AClass = '11'" 说明此人享受四险一金待遇，在此先取出底薪标准
        LAWelfareRadixSet mLAWelfareRadixSet = new LAWelfareRadixSet();
        LAWelfareRadixDB mLAWelfareRadixDB = new LAWelfareRadixDB();
        String tSql2 = "select * from LAWelfareRadix where ManageCom ='" +
                       this.mManageCom.substring(0, 6)
                       + "' and AgentGrade = '" + this.mBranchCode + "'"
                       + " and AClass = '11'";
        mLAWelfareRadixSet = mLAWelfareRadixDB.executeQuery(tSql2);
        if (mLAWelfareRadixSet.size() != 0)
        {
            //把此业务员享受的底薪插入表中
            tLAWelfareInfoSchema = new LAWelfareInfoSchema();
            //置值
            tLAWelfareInfoSchema.setIndexCalNo(this.mIndexCalNo);
            tLAWelfareInfoSchema.setBranchType("1");
            tLAWelfareInfoSchema.setAgentCode(this.mAgentCode);
            tLAWelfareInfoSchema.setAgentGrade(tAgentGrade); //???????
            tLAWelfareInfoSchema.setManageCom(this.mManageCom);
            tLAWelfareInfoSchema.setBranchAttr(AgentPubFun.getAgentBranchAttr(this.
                    mAgentCode));
            tLAWelfareInfoSchema.setAClass(mLAWelfareRadixSet.get(1).getAClass());
            tLAWelfareInfoSchema.setWelfareType("01"); //??????发放
            //福利金额 --??????
            tLAWelfareInfoSchema.setSumMoney(mLAWelfareRadixSet.get(1).
                                             getDrawMoney());

            //福利发放日期
            tDate = this.getWSendDate();
            tLAWelfareInfoSchema.setWSendDate(tDate);
            tLAWelfareInfoSchema.setWValidDate(tDate);
            //处理同业衔接主管问题，如果是在衔接期内的同业衔接主管，则规定只让期享受某个时间段的待遇
            if (tReturn.equals("1")) //在同业衔接期内
            {
                tLAWelfareInfoSchema.setServeYear(tServerYear);
                tDate = this.getWSendEndDate(tDate, tServerYear);
            }
            else //同业衔接期过了，或不是同业衔接主管，则享受一个完整考核期待遇
            {
                tLAWelfareInfoSchema.setServeYear(mLAWelfareRadixSet.get(1).
                                                  getServeYear());
                tDate = this.getWSendEndDate(tDate,
                                             mLAWelfareRadixSet.get(1).
                                             getServeYear());
            }

            tLAWelfareInfoSchema.setWSendEndDate(tDate);
            tLAWelfareInfoSchema.setWValidEndDate(tDate);
            tLAWelfareInfoSchema.setDoneFlag("0");
            tLAWelfareInfoSchema.setMakeDate(this.mCurrentDate);
            tLAWelfareInfoSchema.setMakeTime(this.mCurrentTime);
            tLAWelfareInfoSchema.setModifyDate(this.mCurrentDate);
            tLAWelfareInfoSchema.setModifyTime(this.mCurrentTime);
            tLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);
            //将该业务员应该享受的底薪结果加入LAWelfareInfo表中
            this.mLAWelfareInfoSet.add(tLAWelfareInfoSchema);

            // 根据不同底薪标准设置其 四险一金
            // 取出待遇职级对应的四险一金
            LAWelfareRadixSet nLAWelfareRadixSet = new LAWelfareRadixSet();
            LAWelfareRadixDB nLAWelfareRadixDB = new LAWelfareRadixDB();
            String tSql3 = "select * from LAWelfareRadix where ManageCom ='" +
                           this.mManageCom.substring(0, 6)
                           + "' and AgentGrade = '" + this.mBranchCode + "'"
                           + " and AClass <> '11' order by aclass asc";
            System.out.println("四险一金查询Sql:" + tSql3);

            //查询档案是否调入
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(this.mAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeAscriptionBL";
                tError.functionName = "prePareLAWelfareInfoData";
                tError.errorMessage = "代理人" + this.mAgentCode +
                                      "查询LAAgent表档案信息出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            String tQualiPassFlag = tLAAgentDB.getQualiPassFlag();
            System.out.println("档案是否调入标志位： " + tQualiPassFlag);

            //应该查出四险一金的几个标准
            nLAWelfareRadixSet = nLAWelfareRadixDB.executeQuery(tSql3);

            //对档案是否调入情况进行处理
            //如果档案调入了则生成四险一金记录；档案没有调入则只生成一条发放金额记录
            if (tQualiPassFlag != null && tQualiPassFlag.equals("1")) //档案已经调入
            {
                for (int i = 1; i <= nLAWelfareRadixSet.size(); i++)
                {
                    tLAWelfareInfoSchema = new LAWelfareInfoSchema();
                    //置值
                    tLAWelfareInfoSchema.setIndexCalNo(this.mIndexCalNo);
                    tLAWelfareInfoSchema.setBranchType("1");
                    tLAWelfareInfoSchema.setAgentCode(this.mAgentCode);
                    tLAWelfareInfoSchema.setAgentGrade(tAgentGrade); //???????
                    tLAWelfareInfoSchema.setManageCom(this.mManageCom);
                    tLAWelfareInfoSchema.setBranchAttr(AgentPubFun.
                            getAgentBranchAttr(this.mAgentCode));
                    tLAWelfareInfoSchema.setAClass(nLAWelfareRadixSet.get(i).
                            getAClass());
                    tLAWelfareInfoSchema.setWelfareType("02"); //??????

                    /******* 处理档案问题 ********/

                    //判断档案是否调入，根据调入情况进行相应处理
                    //福利金额 --??????
                    double baseMoney = 0; //缴费基数
                    double perMoney = 0; //个人缴费基数
                    double grpMoney = 0; //集体缴费基数
                    System.out.println("档案是否调入标志：" + tQualiPassFlag);

                    //区分处理 补充商业医疗保险 和 其他保险
                    if (nLAWelfareRadixSet.get(i).getAClass().equals("14")
                        || nLAWelfareRadixSet.get(i).getAClass().equals("15"))
                    {
                        tLAWelfareInfoSchema.setSumMoney(nLAWelfareRadixSet.get(
                                i).getDrawMoney());
                    }
                    else
                    {
                        //档案调入应该享受所有的福利待遇 mLAWelfareRadixSet
                        //获得 缴费基数
                        baseMoney = getBaseMoney(mLAWelfareRadixSet.get(1),
                                                 nLAWelfareRadixSet.get(i));
                        //个人缴费
                        perMoney = baseMoney *
                                   nLAWelfareRadixSet.get(i).getDrawRate();
                        tLAWelfareInfoSchema.setPerMoney(perMoney);
                        //集体缴费
                        grpMoney = baseMoney *
                                   nLAWelfareRadixSet.get(i).getOthDrawRate();
                        tLAWelfareInfoSchema.setGrpMoney(grpMoney);
                        //福利金额
                        tLAWelfareInfoSchema.setSumMoney(perMoney + grpMoney);
                    }
                    /***************************/
                    //福利发放日期
                    tDate = this.getWSendDate();
                    tLAWelfareInfoSchema.setWSendDate(tDate);
                    tLAWelfareInfoSchema.setWValidDate(tDate);

                    //处理同业衔接主管问题，如果是在衔接期内的同业衔接主管，则规定只让期享受某个时间段的待遇
                    if (tReturn.equals("1")) //在同业衔接期内
                    {
                        tLAWelfareInfoSchema.setServeYear(tServerYear);
                        tDate = this.getWSendEndDate(tDate, tServerYear);
                    }
                    else //同业衔接期过了，或不是同业衔接主管，则享受一个完整考核期待遇
                    {
                        tLAWelfareInfoSchema.setServeYear(nLAWelfareRadixSet.
                                get(i).getServeYear());
                        tDate = this.getWSendEndDate(tDate,
                                nLAWelfareRadixSet.get(i).getServeYear());
                    }
                    tLAWelfareInfoSchema.setWSendEndDate(tDate);
                    tLAWelfareInfoSchema.setWValidEndDate(tDate);
                    tLAWelfareInfoSchema.setDoneFlag("0");
                    tLAWelfareInfoSchema.setMakeDate(this.mCurrentDate);
                    tLAWelfareInfoSchema.setMakeTime(this.mCurrentTime);
                    tLAWelfareInfoSchema.setModifyDate(this.mCurrentDate);
                    tLAWelfareInfoSchema.setModifyTime(this.mCurrentTime);
                    tLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);

                    //将结果加入
                    this.mLAWelfareInfoSet.add(tLAWelfareInfoSchema);
                }
            }
            else if (tQualiPassFlag == null || tQualiPassFlag.equals("0")
                     || tQualiPassFlag.equals("")) //档案还没有调入
            {
                //计算一半四险一金发放金饿
                //判断档案是否调入，根据调入情况进行相应处理
                //福利金额 --??????
                double baseMoney = 0; //缴费基数
                double perMoney = 0; //个人缴费基数
                double grpMoney = 0; //集体缴费基数
                double sumGrpMoney = 0;
                System.out.println("档案是否调入标志：" + tQualiPassFlag);

                for (int i = 1; i <= nLAWelfareRadixSet.size(); i++)
                {
                    //处理aclass为14的情况
                    if (nLAWelfareRadixSet.get(i).getAClass().equals("14"))
                    {
                        tLAWelfareInfoSchema = new LAWelfareInfoSchema();

                        tLAWelfareInfoSchema.setIndexCalNo(this.mIndexCalNo);
                        tLAWelfareInfoSchema.setBranchType("1");
                        tLAWelfareInfoSchema.setAgentCode(this.mAgentCode);
                        tLAWelfareInfoSchema.setAgentGrade(tAgentGrade); //???????
                        tLAWelfareInfoSchema.setManageCom(this.mManageCom);
                        tLAWelfareInfoSchema.setBranchAttr(AgentPubFun.
                                getAgentBranchAttr(this.mAgentCode));
                        tLAWelfareInfoSchema.setAClass("14");
                        tLAWelfareInfoSchema.setWelfareType("02");

                        tLAWelfareInfoSchema.setSumMoney(nLAWelfareRadixSet.get(
                                i).getDrawMoney());

                        //福利发放日期
                        tDate = this.getWSendDate();
                        tLAWelfareInfoSchema.setWSendDate(tDate);
                        tLAWelfareInfoSchema.setWValidDate(tDate);

                        //处理同业衔接主管问题，如果是在衔接期内的同业衔接主管，则规定只让期享受某个时间段的待遇
                        if (tReturn.equals("1")) //在同业衔接期内
                        {
                            tLAWelfareInfoSchema.setServeYear(tServerYear);
                            tDate = this.getWSendEndDate(tDate, tServerYear);
                        }
                        else //同业衔接期过了，或不是同业衔接主管，则享受一个完整考核期待遇
                        {
                            tLAWelfareInfoSchema.setServeYear(
                                    nLAWelfareRadixSet.get(1).getServeYear());
                            tDate = this.getWSendEndDate(tDate,
                                    nLAWelfareRadixSet.get(1).getServeYear());
                        }
                        tLAWelfareInfoSchema.setWSendEndDate(tDate);
                        tLAWelfareInfoSchema.setWValidEndDate(tDate);
                        tLAWelfareInfoSchema.setDoneFlag("0");
                        tLAWelfareInfoSchema.setMakeDate(this.mCurrentDate);
                        tLAWelfareInfoSchema.setMakeTime(this.mCurrentTime);
                        tLAWelfareInfoSchema.setModifyDate(this.mCurrentDate);
                        tLAWelfareInfoSchema.setModifyTime(this.mCurrentTime);
                        tLAWelfareInfoSchema.setOperator(this.mGlobalInput.
                                Operator);

                        //将结果加入
                        this.mLAWelfareInfoSet.add(tLAWelfareInfoSchema);
                    }
                    else if (!nLAWelfareRadixSet.get(i).getAClass().equals("15"))
                    {
                        //档案未调入享受部分福利
                        //获得 缴费基数
                        baseMoney = getBaseMoney(mLAWelfareRadixSet.get(1),
                                                 nLAWelfareRadixSet.get(i));
                        //个人缴费
                        //perMoney = baseMoney * nLAWelfareRadixSet.get(i).getDrawRate();
                        //tLAWelfareInfoSchema.setPerMoney(perMoney/2 );
                        //集体缴费
                        grpMoney = baseMoney *
                                   nLAWelfareRadixSet.get(i).getOthDrawRate();
                        sumGrpMoney = sumGrpMoney + grpMoney / 2;
                        //tLAWelfareInfoSchema.setGrpMoney(grpMoney/2);
                        //福利金额
                        //tLAWelfareInfoSchema.setSumMoney( perMoney/2 + grpMoney/2 );
                        //tLAWelfareInfoSchema.setSumMoney(sumGrpMoney);
                    }
                }
                //
                if (nLAWelfareRadixSet.size() > 1 ||
                    (nLAWelfareRadixSet.size() == 1 &&
                     !nLAWelfareRadixSet.get(1).getAClass().equals("14")))
                {
                    tLAWelfareInfoSchema = new LAWelfareInfoSchema();
                    tLAWelfareInfoSchema.setIndexCalNo(this.mIndexCalNo);
                    tLAWelfareInfoSchema.setBranchType("1");
                    tLAWelfareInfoSchema.setAgentCode(this.mAgentCode);
                    tLAWelfareInfoSchema.setAgentGrade(tAgentGrade); //???????
                    tLAWelfareInfoSchema.setManageCom(this.mManageCom);
                    tLAWelfareInfoSchema.setBranchAttr(AgentPubFun.
                            getAgentBranchAttr(this.mAgentCode));
                    tLAWelfareInfoSchema.setAClass("13");
                    tLAWelfareInfoSchema.setWelfareType("02");

                    tLAWelfareInfoSchema.setSumMoney(sumGrpMoney);

                    //福利发放日期
                    tDate = this.getWSendDate();
                    tLAWelfareInfoSchema.setWSendDate(tDate);
                    tLAWelfareInfoSchema.setWValidDate(tDate);

                    //处理同业衔接主管问题，如果是在衔接期内的同业衔接主管，则规定只让期享受某个时间段的待遇
                    if (tReturn.equals("1")) //在同业衔接期内
                    {
                        tLAWelfareInfoSchema.setServeYear(tServerYear);
                        tDate = this.getWSendEndDate(tDate, tServerYear);
                    }
                    else //同业衔接期过了，或不是同业衔接主管，则享受一个完整考核期待遇
                    {
                        tLAWelfareInfoSchema.setServeYear(nLAWelfareRadixSet.
                                get(1).getServeYear());
                        tDate = this.getWSendEndDate(tDate,
                                nLAWelfareRadixSet.get(1).getServeYear());
                    }
                    tLAWelfareInfoSchema.setWSendEndDate(tDate);
                    tLAWelfareInfoSchema.setWValidEndDate(tDate);
                    tLAWelfareInfoSchema.setDoneFlag("0");
                    tLAWelfareInfoSchema.setMakeDate(this.mCurrentDate);
                    tLAWelfareInfoSchema.setMakeTime(this.mCurrentTime);
                    tLAWelfareInfoSchema.setModifyDate(this.mCurrentDate);
                    tLAWelfareInfoSchema.setModifyTime(this.mCurrentTime);
                    tLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);
                    //将结果加入
                    this.mLAWelfareInfoSet.add(tLAWelfareInfoSchema);
                }
            }
        }
        /*********************************************************/

        return true;
    }

    /**
     * 准备好往LAWelfareInfo表插入的数据
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateLAAssessAccessory()
    {
        //查找此业务员原来的考核结果信息
        LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
        tLAAssessAccessoryDB.setIndexCalNo(this.mIndexCalNo);
        tLAAssessAccessoryDB.setAssessType("01");
        tLAAssessAccessoryDB.setAgentCode(this.mAgentCode);

        if (!tLAAssessAccessoryDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAssessAccessoryDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBL";
            tError.functionName = "updateLAAssessAccessory";
            tError.errorMessage = "代理人" + this.mAgentCode +
                                  "查询LAAssessAccessory表信息出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //获得查询结果,修改部分信息（例如：State）
        this.mLAAssessAccessorySchema = tLAAssessAccessoryDB.getSchema();
        this.mLAAssessAccessorySchema.setState("2"); //归属确认状态
        this.mLAAssessAccessorySchema.setOperator(this.mGlobalInput.Operator);
        this.mLAAssessAccessorySchema.setModifyDate(this.mCurrentDate);
        this.mLAAssessAccessorySchema.setModifyTime(this.mCurrentTime);

        return true;
    }

    /****************************************************/
    /**********           日期处理函数部分      ************/
    /****************************************************/
    /**
     * 获得福利发放日期
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private String getWSendDate()
    {
        String tDate = "";
        tDate = this.mIndexCalNo.substring(0, 4) + "-" +
                this.mIndexCalNo.substring(4) + "-01";
        //tDate = this.mYear + "-" + this.mMonth + "-01";
        tDate = PubFun.calDate(tDate, 1, "M", null);
        return tDate;
    }

    /**
     * 获得福利发放终止日期
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private String getWSendEndDate(String tDate, int interval)
    {
        tDate = PubFun.calDate(tDate, interval, "M", null);
        tDate = PubFun.calDate(tDate, -1, "D", null);
        return tDate;
    }

    /**
     * 获得缴费基数
     * mLAWelfareRadixSchema ： 用来存放底薪标准
     * nLAWelfareRadixSchema ： 用来存放四险一金标准
     * 返回缴费基数
     */
    private double getBaseMoney(LAWelfareRadixSchema mLAWelfareRadixSchema,
                                LAWelfareRadixSchema nLAWelfareRadixSchema)
    {
        //平均工资下限
        double lowBase = mLAWelfareRadixSchema.getDrawBase() *
                         nLAWelfareRadixSchema.getDrawBase1();
        //省平均工资上限
        double upBase = mLAWelfareRadixSchema.getDrawBase() *
                        nLAWelfareRadixSchema.getDrawBase2();
        //没有上下限限制，则直接返回底薪
        if (lowBase == 0 && upBase == 0)
        {
            return mLAWelfareRadixSchema.getDrawMoney();
        }
        //判断缴费基数（底薪）是否在某个范围内
        if (mLAWelfareRadixSchema.getDrawMoney() > upBase) //大于上限取上限
        {
            return upBase;
        }
        else if (mLAWelfareRadixSchema.getDrawMoney() < lowBase) //小于下限取下限
        {
            return lowBase;
        }
        else
        {
            return mLAWelfareRadixSchema.getDrawMoney(); //在上下限之间则直接返回
        }
    }

    /**
     * 获得业务员职级
     * mLAWelfareRadixSchema ： 用来存放底薪标准
     * nLAWelfareRadixSchema ： 用来存放四险一金标准
     * 返回缴费基数
     */
    private String getAgentGrade()
    {
        String tAgentGrade = "";

        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(this.mAgentCode);

        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBL";
            tError.functionName = "getAgentGrade";
            tError.errorMessage = "代理人" + this.mAgentCode + "查询LATree表职级信息出错！";
            this.mErrors.addOneError(tError);
            return "00";
        }

        return tLATreeDB.getAgentGrade();
    }

    /**
     * 判断某业务员是否为同业衔接期间内的同业衔接主管
     * 返回缴费基数
     * 0 - 程序中出错
     * 1 - 衔接期未过
     * 2 - 衔接期已过 或 不是同业衔接人员
     */
    private String isLinkPeriodMan(String tAgentCode)
    {
        //获得业务员当前职级
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(tAgentCode);

        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBL";
            tError.functionName = "isLinkPeriodMan";
            tError.errorMessage = "代理人" + this.mAgentCode + "查询LATree表职级信息出错！";
            this.mErrors.addOneError(tError);
            return "0";
        }

        //取得此业务员入司时的职级，如果入司时的职级大于A04，则为同业衔接主管
        //如果入司时的职级，小于A04，后来有手工维护为A04以上职级，则不算同业衔接主管
        String oldAgentGrade = getOldAgentGrade(tAgentCode);
        if (oldAgentGrade.equals("00")) //如果为00则说明此业务员没有发生过职级变动
        {
            oldAgentGrade = tLATreeDB.getAgentGrade();
        }

        // 判断当前职级是否为A04以上，同时保证入司时的职级也为A04以上
        if (tLATreeDB.getAgentGrade().compareTo("A04") >= 0
            && oldAgentGrade.compareTo("A04") >= 0)
        {
            //查找laagent表中信息,判断是否为同业衔接人员
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);

            if (!tLAAgentDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeAscriptionBL";
                tError.functionName = "isLinkPeriodMan";
                tError.errorMessage = "代理人" + this.mAgentCode +
                                      "查询LAAgent表信息出错！";
                this.mErrors.addOneError(tError);
                return "0";
            }
            //判断是否为同业衔接人员
            if (tLAAgentDB.getEmployDate().equals(tLAAgentDB.getInDueFormDate()))
            {
                // 对同业衔接人员的处理
                // 判断该同业衔接人员的衔接期是否过了
                // a - 获得筹备期 和 考核期
                ExeSQL tExeSQL = new ExeSQL();
                String tSql1 = "select trim(code3) from ldcoderela "
                               + " where relatype = 'limitperiod' "
                               +
                               " and (othersign = '01' or othersign is null )"
                               + " and code1 = '" + tLATreeDB.getAgentGrade() +
                               "'";
                //校验是否录入考核期
                String tempStr = tExeSQL.getOneValue(tSql1);
                //校验是否出错
                if (tExeSQL.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeAscriptionBL";
                    tError.functionName = "isLinkPeriodMan";
                    tError.errorMessage = "查询考核期信息出错！";
                    this.mErrors.addOneError(tError);
                    System.out.println("查询考核期信息出错！");
                    return "0";
                }
                //this.mAssessPeriod = Integer.parseInt(tExeSQL.getOneValue(tSql1));
                if (tempStr == null || tempStr.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeAscriptionBL";
                    tError.functionName = "isLinkPeriodMan";
                    tError.errorMessage = "查询考核期信息出错,没有录入考核期！";
                    this.mErrors.addOneError(tError);
                    System.out.println("查询考核期信息出错,没有录入考核期！");
                    return "0";
                }
                //设置考核期
                int tAssessPeriod = Integer.parseInt(tempStr);

                //修改：2004-08-02 LL
                //修改原因：新人新办法，老人老办法修改，查询同业衔接人员时增加了AreaType字段
                String tStr = "select calassareatype('" + tAgentCode +
                              "') from ldsysvar where sysvar = 'onerow'";
                ExeSQL t = new ExeSQL();
                System.out.println("新-老人查询地区类型SQL：" + tStr);
                String sResult = t.getOneValue(tStr); //存放地区类型
                System.out.println("查询地区类型结果为:" + sResult);

                String tSql2 = "select trim(ArrangePeriod) from LALinkAssess"
                               + " where " + this.mManageCom +
                               " like trim(ManageCom)||'%'"
                               + " and agentgrade = '" +
                               tLATreeDB.getAgentGrade() + "'"
                               + " and areatype = '" + sResult + "'";
                //校验是否录入筹备期
                tempStr = tExeSQL.getOneValue(tSql2);
                //this.mArrangePeriod = Integer.parseInt(tExeSQL.getOneValue(tSql2));
                //校验是否出错
                if (tExeSQL.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeAscriptionBL";
                    tError.functionName = "isLinkPeriodMan";
                    tError.errorMessage = "查询筹备期信息出错！";
                    this.mErrors.addOneError(tError);
                    System.out.println("查询筹备期信息出错！");
                    return "0";
                }
                if (tempStr == null || tempStr.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeAscriptionBL";
                    tError.functionName = "isLinkPeriodMan";
                    tError.errorMessage = "查询筹备期信息出错,没有录入筹备期！";
                    this.mErrors.addOneError(tError);
                    System.out.println("查询筹备期信息出错,没有录入筹备期！");
                    return "0";
                }
                //设置筹备期
                int tArrangePeriod = Integer.parseInt(tempStr);

                // b - 首先判断此衔接人员的入司日期是否过了某个日期
                //取出这个日期
                tExeSQL = new ExeSQL();
                String tSql3 =
                        "select trim(varvalue) from lasysvar where vartype = 'EmployLimit'";
                String tResult = tExeSQL.getOneValue(tSql3);
                //校验是否出错
                if (tExeSQL.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeAscriptionBL";
                    tError.functionName = "isLinkPeriodMan";
                    tError.errorMessage = "查询某月某个日期信息出错！";
                    this.mErrors.addOneError(tError);
                    System.out.println("查询筹备期信息出错！");
                    return "0";
                }
                //校验是否取出比较日期
                if (tResult == null || tResult.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeAscriptionBL";
                    tError.functionName = "isLinkPeriodMan";
                    tError.errorMessage = "查询某月某个日期信息出错,没有在数据库中取出这个日期！";
                    this.mErrors.addOneError(tError);
                    System.out.println("查询某月某个日期信息出错,没有在数据库中取出这个日期！");
                    return "0";
                }
                //构造出比较日期
                String tDate = tLAAgentDB.getEmployDate().substring(0, 8)
                               + tResult;
                //判断是否超过某个日期，根据判断结果设置起期
                String tBeginDate = "";
                String tEndDate = "";
                if (tLAAgentDB.getEmployDate().compareTo(tDate) > 0) //超过
                {
                    tBeginDate = PubFun.calDate(tDate, 1, "M", null);
                }
                else //没有超过
                {
                    tBeginDate = tLAAgentDB.getEmployDate();
                }

                //比较考核期是否超过筹备期
                String tStr1 = this.mIndexCalNo.substring(0, 4) + "-"
                               + this.mIndexCalNo.substring(4) + "-01";
                String s = PubFun.calDate(tBeginDate,
                                          tAssessPeriod + tArrangePeriod - 1,
                                          "M", null);
                s = s.substring(0, 4) + s.substring(5, 7);
                //if(tStr1.compareTo(PubFun.calDate(tBeginDate,tAssessPeriod + tArrangePeriod -1,"M",null)) <= 0)
                if (s.compareTo(this.mIndexCalNo) > 0)
                {
                    //衔接期未过
                    System.out.println("业务员" + this.mAgentCode + "衔接期未过");
                    return "1";
                }
            }
        }
        //衔接期已过 或 不是同业衔接人员
        return "2";
    }

    /**
     * 在LATree或着LATreeB表中查询某个代理人入司时的职级
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private String getOldAgentGrade(String tAgentCode)
    {
        //先查LATreeB表中最老的一条备份记录
        LATreeBSet tLATreeBSet = new LATreeBSet();
        LATreeBDB tLATreeBDB = new LATreeBDB();
        String tSql = "select * from LATreeB a where a.agentcode = '" +
                      tAgentCode
                      + "' and RemoveType <> '05' order by EdorNO asc";
        tLATreeBSet = tLATreeBDB.executeQuery(tSql);
        //校验是否出错
        if (tLATreeBDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLATreeBDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBL";
            tError.functionName = "getOldAgentGrade";
            tError.errorMessage = "查询LATreeB表中最老的一条备份记录信息出错！";
            this.mErrors.addOneError(tError);
            System.out.println("查询筹备期信息出错！");
        }
        //如果有记录则返回级别
        if (tLATreeBSet.size() != 0)
        {
            return tLATreeBSet.get(1).getAgentGrade();
        }
        //如果LATreeB表中没有记录则返回 00
        return "00";
    }
}
