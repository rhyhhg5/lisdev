package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.lis.agent.ALADimissionAppUI;
import com.sinosoft.lis.agentassess.LAAssessInputUI;
import com.sinosoft.lis.agentbranch.LABranchGroup;
import com.sinosoft.lis.agentcalculate.CalFormDrawRate;

/**
 * 考核计算回退，
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAAssessBackInsuredBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往后面传输数据的容器 ，进行回退计算*/
    private VData mVData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String mEdorNo;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSet mLAAssessSet = new LAAssessSet();  //要回退的人员
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LAAssessMainSchema mLAAssessMainSchema  = new LAAssessMainSchema();
    private LACommisionSet mLACommisionSet  = new LACommisionSet();
    private MMap mMap = new MMap();


    private String mEvaluateDate = "";           // 异动时间
    private String mManageCom = "";              // 管理机构
    private String mIndexCalNo = "";             // 考核序列号
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mGradeSeries = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    public LAAssessBackInsuredBL()
    {

    }
    public static void main(String args[])
    {
    }
    public MMap getResult()
    {
        return mMap;
    }
    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

 //       mInputData = null;
        return true;
    }
    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
     //   mOutputData = new VData();
        mMap = new MMap();
        mMap.put(mLATreeSet, "UPDATE");
        mMap.put(mLATreeBSet, "INSERT");
        mMap.put(mLAAgentSet, "UPDATE");
        mMap.put(mLAAgentBSet, "INSERT");
        mMap.put(mLAAssessMainSchema, "UPDATE");
        mMap.put(mLACommisionSet, "UPDATE");
      //  mOutputData.add(mMap);
        return true;
    }


    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
         String tEdorNo = mEdorNo;
        String tRemoveType="30";
        if(mLAAssessSet.size()>0)
        {
            for(int i=1;i<=mLAAssessSet.size();i++)
            {
                LAAssessSchema tLAAssessSchema = new LAAssessSchema();
                tLAAssessSchema = mLAAssessSet.get(i);
                String tAgentCode = tLAAssessSchema.getAgentCode();

                //处理LATree表
                //--------------------------------------------------------------------------------
                //String tAgentGrade=tLAAssessSchema.getAgentGrade();
                LATreeBSchema tNewLATreeBSchema = new LATreeBSchema();
                LATreeSchema tLATreeSchema = new LATreeSchema();
                LATreeDB tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(tAgentCode);
                tLATreeDB.getInfo();
                tLATreeSchema = tLATreeDB.getSchema();
                String tNowAgentGrade = tLATreeSchema.getAgentGrade(); //系统目前的职级
                String tNowStartDate = tLATreeSchema.getStartDate(); //系统目前的职级的开始时间
                String tAgentGroup = tLATreeSchema.getAgentGroup();
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setAgentGroup(tAgentGroup);
                tLABranchGroupDB.getInfo();
                LABranchGroupSchema tLABranchGroupSchema = new
                        LABranchGroupSchema();
                tLABranchGroupSchema = tLABranchGroupDB.getSchema();
                String tBranchAttr = tLABranchGroupSchema.getBranchAttr();
                String tBranchLevel = tLABranchGroupSchema.getBranchLevel();
                LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                LATreeBSet tLATreeBSet = new LATreeBSet();
                LATreeBDB tLATreeBDB = new LATreeBDB();
                //查询出由于考核确认操作备份的信息，查询最开始一次的备份(原始数据)
                String tSql = "select *  from latreeb where agentcode='" +
                              tAgentCode + "' and indexcalno='" +
                              mIndexCalNo +
                              "' and removetype='01' order by modifydate  asc ";
                tLATreeBSet = tLATreeBDB.executeQuery(tSql);
                if (tLATreeBSet.size() > 0) {
                    tLATreeBSchema = tLATreeBSet.get(1);
                } else {
                    //此人如果考核时没有修改职级,则查询他是否做过手工修改职级
                    tSql = "select *  from latreeb where agentcode='" +
                           tAgentCode + "' and indexcalno='" +
                           mIndexCalNo +
                           "' and removetype='31' order by modifydate  asc ";
                    tLATreeBSet = new LATreeBSet();
                    tLATreeBDB = new LATreeBDB();
                    tLATreeBSet = tLATreeBDB.executeQuery(tSql);
                    if (tLATreeBSet.size() > 0) {
                        tLATreeBSchema = tLATreeBSet.get(1);
                    } else {
                        //如果职级没有变过,则此人不用做考核确认的回退
                        continue;
                    }
                }

                LATreeBSchema tLastLATreeBSchema = new LATreeBSchema();
                tLastLATreeBSchema = tLATreeBSchema;
                String tAgentGrade = tLATreeBSchema.getAgentGrade();
                //取出tOldEdorNo tEdorType ,需要以此查询laagentb 表
                String tOldEdorNo = tLATreeBSchema.getEdorNO();
                String tEdorType = tLATreeBSchema.getRemoveType();
                //职级是否可以回退到考核前的状态，校验原来的职级和现在的团队是否一致,个险需要校验
                if (mBranchType.equals("1") && mBranchType2.equals("01")) {
                    if (!checkGroup(tBranchLevel, tBranchAttr, tAgentGrade,
                                    tAgentCode)) {
                        return false;
                    }
                    if (!checkAgentGrade(tAgentCode, tAgentGrade,
                                         tNowAgentGrade)) {
                        return false;
                    }
                }
                //备份信息
                Reflections tReflections = new Reflections();
                tReflections.transFields(tNewLATreeBSchema, tLATreeSchema);
                tNewLATreeBSchema.setEdorNO(tEdorNo);
                tNewLATreeBSchema.setRemoveType(tRemoveType);
                tNewLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
                tNewLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tNewLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
                tNewLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
                tNewLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
                tNewLATreeBSchema.setMakeDate(currentDate);
                tNewLATreeBSchema.setMakeTime(currentTime);
                tNewLATreeBSchema.setModifyDate(currentDate);
                tNewLATreeBSchema.setModifyTime(currentTime);
                tNewLATreeBSchema.setIndexCalNo(mIndexCalNo);
                tNewLATreeBSchema.setOperator(mGlobalInput.Operator);
                mLATreeBSet.add(tNewLATreeBSchema);

                //开始处理
                tLATreeSchema.setAgentGrade(tLastLATreeBSchema.getAgentGrade());
                tLATreeSchema.setAgentKind(tLastLATreeBSchema.getAgentKind());
                tLATreeSchema.setAgentLastGrade(tLastLATreeBSchema.
                                                getAgentLastGrade());
                tLATreeSchema.setAgentLastSeries(tLastLATreeBSchema.
                                                 getAscriptSeries());
                tLATreeSchema.setAgentLine(tLastLATreeBSchema.getAgentLine());
                tLATreeSchema.setAgentSeries(tLastLATreeBSchema.getAgentSeries());
//                tLATreeSchema.setAscriptSeries(tLastLATreeBSchema.getAscriptSeries());
//                tLATreeSchema.setAssessType(tLastLATreeBSchema.getAssessType());
//                tLATreeSchema.setAstartDate(tLastLATreeBSchema.getAstartDate());
//                tLATreeSchema.setBranchCode(tLastLATreeBSchema.getBranchCode());
//                tLATreeSchema.setBranchType(tLastLATreeBSchema.getBranchType());
//                tLATreeSchema.setBranchType2(tLastLATreeBSchema.getBranchType2());
//                tLATreeSchema.setEduManager(tLastLATreeBSchema.getEduManager());
//                  tLATreeSchema.setInitGrade();
//                tLATreeSchema.setInsideFlag(tLastLATreeBSchema.getInsideFlag());
//                tLATreeSchema.setIntroAgency(tLastLATreeBSchema.getIntroAgency());
//                tLATreeSchema.setIntroBreakFlag(tLastLATreeBSchema.getIntroBreakFlag());
//                tLATreeSchema.setIntroCommEnd(tLastLATreeBSchema.getIntroCommEnd());
//                tLATreeSchema.setIntroCommStart(tLastLATreeBSchema.getIntroCommStart());
//                tLATreeSchema.setisConnMan(tLastLATreeBSchema.getisConnMan());
//                  tLATreeSchema.setMakeDate();
//               tLATreeSchema.setMakeTime();
//                tLATreeSchema.setManageCom();
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOldEndDate(tLastLATreeBSchema.getOldEndDate());
                tLATreeSchema.setOldStartDate(tLastLATreeBSchema.
                                              getOldStartDate());
                tLATreeSchema.setOperator(mGlobalInput.Operator);
//                tLATreeSchema.setOthUpAgent(tLastLATreeBSchema.getOthUpAgent());
//                tLATreeSchema.setRearBreakFlag(tLastLATreeBSchema.getRearBreakFlag());
//                tLATreeSchema.setRearCommEnd(tLastLATreeBSchema.getRearCommEnd());
//                tLATreeSchema.setRearCommStart(tLastLATreeBSchema.getRearCommStart());
//                tLATreeSchema.setRoleAgentCode(tLastLATreeBSchema.getRoleAgentCode());
//                tLATreeSchema.setRoleFlag();
                tLATreeSchema.setStartDate(tLastLATreeBSchema.getStartDate());
                tLATreeSchema.setState(tLastLATreeBSchema.getState());
                mLATreeSet.add(tLATreeSchema);
                //LATree表处理完成,开始处理laagent表 tOldEdorNo  tNowAgentGrade
                //------------------------------------------------------------------
                //查询原职级是否为 见习
                String tLastAgentGrade = tLastLATreeBSchema.getAgentGrade();
                LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
                tLAAgentGradeDB.setGradeCode(tLastAgentGrade);
                tLAAgentGradeDB.getInfo();
                String tGradeProperty4 = tLAAgentGradeDB.getSchema().
                                         getGradeProperty4();

                //查询目前职级是否为 见习
                tLAAgentGradeDB = new LAAgentGradeDB();
                tLAAgentGradeDB.setGradeCode(tNowAgentGrade);
                tLAAgentGradeDB.getInfo();
                String tNowGradeProperty4 = tLAAgentGradeDB.getSchema().
                                            getGradeProperty4();
                int tFlag = 0;
                if (tGradeProperty4.equals("0") &&
                    tNowGradeProperty4.equals("1")) {
                    //原职级为见习,现职级不是见习,需要修改 laagent 表的 转正日期


                    LAAgentBSet tLAAgentBSet = new LAAgentBSet();
                    LAAgentBDB tLAAgentBDB = new LAAgentBDB();
                    String tSQL = "select * from laagentb where agentcode='" +
                                  tAgentCode +
                                  "' and edorNo='"
                                  + tOldEdorNo + "' and edortype='" + tEdorType +
                                  "' ";
                    tLAAgentBSet = tLAAgentBDB.executeQuery(tSQL);
                    if (tLAAgentBSet.size() <= 0) {
                        tFlag = 0;
                    } else {
                        LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
                        tLAAgentBSchema = tLAAgentBSet.get(1);
                        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                        String tLastIndueFormDate = tLAAgentBSet.get(1).
                                getInDueFormDate();
                        String tLastOutWorkDate = tLAAgentBSet.get(1).
                                                  getOutWorkDate();
                        String tLastAgentState = tLAAgentBSet.get(1).
                                                 getAgentState();
                        if (tLastIndueFormDate != null &&
                            !tLastIndueFormDate.equals("")) {
                            continue; //原来的转正日期存在,则不需要处理laagent表
                        }
                        LAAgentDB tLAAgentDB = new LAAgentDB();
                        tLAAgentDB.setAgentCode(tAgentCode);
                        tLAAgentDB.getInfo();
                        tLAAgentSchema = tLAAgentDB.getSchema();
                        tReflections = new Reflections();
                        //备份
                        tReflections.transFields(tLAAgentBSchema,
                                                 tLAAgentSchema);
                        String tNowIndueFormDate = tLAAgentSchema.
                                getInDueFormDate();
                        String tNowOutWorkDate = tLAAgentSchema.getOutWorkDate();
                        String tNowAgentState = tLAAgentSchema.getAgentState();

                        if (tNowIndueFormDate == null ||
                            tNowIndueFormDate.equals("")) {
                            continue; //目前转正日期为空,则不需要处理laagent表
                        }
                        //否则,把转正日期改为空
                        tLAAgentSchema.setInDueFormDate(tLastIndueFormDate);
                        tLAAgentSchema.setAgentState(tLastAgentState);
                        tLAAgentSchema.setOutWorkDate(tLastOutWorkDate);
                        tLAAgentSchema.setModifyDate(currentDate);
                        tLAAgentSchema.setModifyTime(currentTime);
                        tLAAgentSchema.setOperator(mGlobalInput.Operator);
                        mLAAgentSet.add(tLAAgentSchema);
                        //备份
                        tLAAgentBSchema.setModifyDate(currentDate);
                        tLAAgentBSchema.setModifyTime(currentTime);
                        tLAAgentBSchema.setOperator(mGlobalInput.Operator);
                        tLAAgentBSchema.setEdorNo(tEdorNo);
                        tLAAgentBSchema.setEdorType(tRemoveType);
                        mLAAgentBSet.add(tLAAgentBSchema);

                        tFlag = 1; //不需要再处理laagent 表
                    }

                } else if (tFlag == 0) {
                    //如果没有做过转正的处理,需要处理离职，laagent表。  tFlag=1表示laagent的离职信息处理已经完成
                    LAAgentDB tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(tAgentCode);
                    tLAAgentDB.getInfo();
                    LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                    tLAAgentSchema = tLAAgentDB.getSchema();
                    String tNowAgentState = tLAAgentSchema.getAgentState();
                    String tNowOutWorkDate = tLAAgentSchema.getOutWorkDate();
                    if (tNowAgentState.compareTo("03") >= 0) {
                        //离职，需要回退
                        //备份
                        LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
                        tReflections = new Reflections();
                        tReflections.transFields(tLAAgentBSchema,
                                                 tLAAgentSchema);
                        tLAAgentBSchema.setModifyDate(currentDate);
                        tLAAgentBSchema.setModifyTime(currentTime);
                        tLAAgentBSchema.setOperator(mGlobalInput.Operator);
                        tLAAgentBSchema.setEdorNo(tEdorNo);
                        tLAAgentBSchema.setEdorType(tRemoveType);
                        mLAAgentBSet.add(tLAAgentBSchema);
                        //如果备份表中有二次增原的标记，此人的状态为 02
                        LAAgentBSet tLAAgentBSet = new LAAgentBSet();
                        LAAgentBDB tLAAgentBDB = new LAAgentBDB();
                        tLAAgentBDB.setAgentCode(tAgentCode);
                        tLAAgentBDB.setAgentState("02");
                        tLAAgentBSet = tLAAgentBDB.query();
                        if (tLAAgentBSet.size() > 0) {
                            tLAAgentSchema.setAgentState("02");
                        } else {
                            tLAAgentSchema.setAgentState("01");
                        }
                        tLAAgentSchema.setOutWorkDate("");
                        tLAAgentSchema.setModifyDate(currentDate);
                        tLAAgentSchema.setModifyTime(currentTime);
                        tLAAgentSchema.setOperator(mGlobalInput.Operator);
                        mLAAgentSet.add(tLAAgentSchema);
                    }
                }
                //处理提数
                if (mBranchType.equals("1") && mBranchType2.equals("01")) {
                    tLAAgentGradeDB = new LAAgentGradeDB();
                    tLAAgentGradeDB.setGradeCode(tAgentGrade);
                    tLAAgentGradeDB.getInfo();
                    String tOldAgentSeries = tLAAgentGradeDB.getSchema().
                                             getGradeProperty4();
                    tLAAgentGradeDB = new LAAgentGradeDB();
                    tLAAgentGradeDB.setGradeCode(tNowAgentGrade);
                    tLAAgentGradeDB.getInfo();
                    String tNowAgentSeries = tLAAgentGradeDB.getSchema().
                                             getGradeProperty4();
                    if ((tOldAgentSeries.equals("0") &&
                         tNowAgentSeries.equals("1")) ||
                        (tOldAgentSeries.equals("1") &&
                         tNowAgentSeries.equals("0"))) {
                        //原来的职级为见习，先职级不位见习需要修改fyc；原职级不为见习，先职级为见习，需要修改 fyc
                        String sql =
                                "select * from lacommision where agentcode='" +
                                tAgentCode +
                                "' and (caldate is null or caldate>='" +
                                tNowStartDate +
                                "')";
                        LACommisionDB tLACommisionDB = new LACommisionDB();
                        LACommisionSet tLACommisionSet = new LACommisionSet();
                        tLACommisionSet = tLACommisionDB.executeQuery(sql);
                        CalFormDrawRate tCalFormDrawRate = new
                                CalFormDrawRate();
                        VData temp = new VData();
                        temp.clear();
                        temp.add(tLACommisionSet);
                        temp.add(tAgentGrade); //传入的是一个AgentGrade，提数计算的时候传空，归属的时候传入的是新的职级

                        if (!tCalFormDrawRate.submitData(temp,
                                CalFormDrawRate.CALFLAG)) {
                            this.mErrors.copyAllErrors(tCalFormDrawRate.
                                    mErrors);
                            CError tError = new CError();
                            tError.moduleName = "LAAssessBackInsuredBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = "打折处理的时候出错！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }

                        //    mLACommisionSet.clear();

                        LACommisionSet tgetLACommisionSet = new LACommisionSet();
                        tgetLACommisionSet.clear();
                        tgetLACommisionSet.set((LACommisionSet)
                                               tCalFormDrawRate.getResult().
                                               getObjectByObjectName(
                                "LACommisionSet", 0));
                        mLACommisionSet.add(tgetLACommisionSet);

                    }
                }
                /**
                 * 缺少团险的处理
                 * if(mBranchType.equals("1") && mBranchType2.equals("01"))
                 * @return boolean
                 */



                /**
                 * 离职表未删除，
                 *
               */
            }
        }
        //处理 laassessmain 表
        LAAssessMainDB tLAAssessMainDB  = new LAAssessMainDB();
        tLAAssessMainDB.setManageCom(mManageCom);
        tLAAssessMainDB.setBranchType(mBranchType);
        tLAAssessMainDB.setBranchType2(mBranchType2);
        tLAAssessMainDB.setIndexCalNo(mIndexCalNo);
        tLAAssessMainDB.setAssessType("00");
        LAAssessMainSet tLAAssessMainSet  = new LAAssessMainSet();
        tLAAssessMainSet=tLAAssessMainDB.query();
        LAAssessMainSchema tLAAssessMainSchema  = new LAAssessMainSchema();
        if(tLAAssessMainSet.size()<=0)
        {
            this.buildError("dealData", "查询分公司"+mManageCom+"最终考核信息没有数据");
            return false;
        }
        tLAAssessMainSchema=tLAAssessMainSet.get(1);
        tLAAssessMainSchema.setState("01");
        mLAAssessMainSchema=tLAAssessMainSchema;
        return true;
    }

    private boolean checkGroup(String tBranchLevel,String tBranchAttr,String tNewAgentGrade,String tAgentCode)
    {
        LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
        tLAAgentGradeDB.setGradeCode(tNewAgentGrade);
        tLAAgentGradeDB.getInfo();
        String tAgentSeries=tLAAgentGradeDB.getSchema().getGradeProperty2();

        if(tAgentSeries==null || tAgentSeries.equals(""))
        {
            this.buildError("checkGroup", "不能回退"+tAgentCode+"的职级,"+"不能回退到" + tNewAgentGrade + "职级");
            return false;
        }
        if ((tBranchLevel.equals("01")) && (tAgentSeries.compareTo("1")>0) )
        {
            this.buildError("checkGroup",  "不能回退"+tAgentCode+"的职级,"+"团队"+tBranchAttr+"没有" + tNewAgentGrade + "职级");
            return false;
        }
        else if ((tBranchLevel.equals("02")) && (!tAgentSeries.equals("2")) )
        {
            this.buildError("checkGroup",  "不能回退"+tAgentCode+"的职级,"+"团队"+tBranchAttr+"没有" + tNewAgentGrade + "职级");
            return false;
        }
        else if ((tBranchLevel.equals("03")) && (!tAgentSeries.equals("3")) )
        {
            this.buildError("checkGroup",  "不能回退"+tAgentCode+"的职级,"+"团队"+tBranchAttr+"没有" + tNewAgentGrade + "职级");
            return false;
        }
        return true;
    }
    /**
     * 校验目前职级和要回退到的目标职级
     * @return boolean
     */
    private boolean checkAgentGrade(String tAgentCode,String tOldAgentGrade,String tNowAgentGrade)
    {
        LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
        tLAAgentGradeDB.setGradeCode(tOldAgentGrade);
        tLAAgentGradeDB.getInfo();
        String tOldAgentSeries=tLAAgentGradeDB.getSchema().getGradeProperty2();
        tLAAgentGradeDB = new LAAgentGradeDB();
        tLAAgentGradeDB.setGradeCode(tNowAgentGrade);
        tLAAgentGradeDB.getInfo();
        String tNowAgentSeries=tLAAgentGradeDB.getSchema().getGradeProperty2();
        if(tOldAgentSeries==null || tOldAgentSeries.equals(""))
        {
            this.buildError("checkGroup",  "不能回退"+tAgentCode+"的职级,"+"不能回退到" + tOldAgentGrade + "职级");
            return false;
        }
        if(tNowAgentSeries==null || tNowAgentSeries.equals(""))
        {
            this.buildError("checkGroup",  "不能回退"+tAgentCode+"的职级,"+"不能回退到" + tOldAgentGrade + "职级");
            return false;
        }
        if(!tNowAgentSeries.equals(tOldAgentSeries))
        {
            this.buildError("checkGroup",  "不能回退"+tAgentCode+"的职级,"+"考核前职级与现职级属于不同的系列");
            return false;
        }
        return true ;
    }
    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealLatree()
    {
      //  LAtree
      return true;
    }
    private boolean check()
    {


        return true;
    }
    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAssessBackInsuredBL.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLAAssessSet.set((LAAssessSet)cInputData
                .getObjectByObjectName("LAAssessSet",0));
        this.mManageCom = (String) cInputData.get(2);
        this.mIndexCalNo =  (String) cInputData.get(3);
        this.mBranchType = (String) cInputData.get(4);
        this.mBranchType2 = (String) cInputData.get(5);
        this.mEdorNo = (String) cInputData.get(6);
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessBackInsuredBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LAAssessBackInsuredBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


}
