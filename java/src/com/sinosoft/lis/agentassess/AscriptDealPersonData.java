/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.agentcalculate.DealBusinessData;
import com.sinosoft.lis.db.LAAgentBDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.db.LABranchGroupBDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeAccessoryBDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeAccessoryBSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */
public class AscriptDealPersonData
{
//错误处理类
    public static CErrors mErrors = new CErrors();
//业务处理相关变量
    /** 全局数据 */
    private VData mInputData = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
//传入的数据
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private String mNewAgentGroup = "";
    private String mNewAgentAttr = "";
    private String mIndexCalNo = "";
    /** ModTypeFlag = "S" -- 被晋升的代理人
     *                "O" -- 普通代理人*/
    private String ModTypeFlag = "";
    /** InsFlag = "Y" -- 仅插入 LABranchGroup (生成新的 AgentGroup,BranchAttr)
     *            "N" -- 插入 LABranchGroupB 并刷新 LABranchGroup
     *                   (仅生成新的 BranchAttr,而AgentGroup不变) */
    private String InsFlag = "";
    private String mAgentCode = ""; //被晋升的代理人

    //日期
    private String[] MonDate = new String[2];

    public AscriptDealPersonData()
    {
    }

    public static void main(String[] args)
    {
        //AscriptDealData ascriptDealData1 = new AscriptDealData();
    }

    public boolean dealPersonData(VData cInputData, Connection conn)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        this.MonDate = genDate();

        if (this.ModTypeFlag.equals("S"))
        {
            //刷新代理人考评信息表
            if (!updateLAAssess(conn))
            {
                return false;
            }

            //插入展业机构表
            if (!insertLABranchGroup(conn))
            {
                return false;
            }
        }

        //插入代理人信息转储表
        if (!InsLAAgentB(conn))
        {
            return false;
        }

        //刷新代理人信息表
        if (!updateLAAgent(conn))
        {
            return false;
        }

        //插入代理人行政信息转储表
        if (!insertLATreeB(conn))
        {
            return false;
        }

        //刷新代理人行政信息表
        if (!updateLATree(conn))
        {
            return false;
        }

        //生成新组，刷新业务数据
        AgentPubFun tAPF = new AgentPubFun();
        String tCheckFlag = tAPF.AdjustCommCheck(this.mAgentCode.trim(),
                                                 this.MonDate[0]);
        if (!tCheckFlag.trim().equals("00"))
        {
            dealError("updLACommision", tCheckFlag.trim());
            return false;
        }

        //如果是A01－>组长，则先将业绩修改为不打8折
//zsj-delete--2004-3-9:因为每月计算佣金之前会处理打折的情况，所以归属时不需要对业绩打折
//cg-add--2004-05-08:因为计算佣金之前处理打折的时候只会处理A01的情况，所以归属的时候应该对业绩进行处理，恢复为不打折
        if (this.ModTypeFlag.trim().equals("S"))
        {
            if (this.mLATreeSchema.getAgentSeries().trim().equals("A"))
            {
                ExeSQL tExe = new ExeSQL(conn);
                String sql =
                        "update lacommision set fyc = lacommision.directwage,"
                        +
                        " fycrate = lacommision.standfycrate where trim(AgentCode)="
                        + "'" + this.mLATreeSchema.getAgentCode().trim()
                        + "' and (caldate >= '"
                        + this.MonDate[0].trim() + "' or caldate is null)";

                System.out.println("---SQL : " + sql);

                if (!tExe.execUpdateSQL(sql))
                {
                    dealError("updLACommision", "刷新LACommision失败！");
                    return false;
                }
            }
        }

        DealBusinessData tDeal = new DealBusinessData();
        VData tV = new VData();
        tV.clear();
        tV.add(0, this.mLATreeSchema.getAgentCode());
        tV.add(1, this.mNewAgentGroup);
        if (this.mLATreeSchema.getAgentSeries().trim().equals("A"))
        {
            tV.add(2, "Y");
        }
        else
        {
            tV.add(2, "N");
        }
        tV.add(3, this.mNewAgentAttr);
        tV.add(4, this.mIndexCalNo);
        tV.add(5, this.mNewAgentGroup);
        if (!tDeal.updateAgentGroup(tV, conn))
        {
            dealError("dealPersonData", tDeal.mErrors.getFirstError());
            return false;
        }
        return true;
    }

//  private boolean updLACommision(Connection conn)  {
//    PubFun tPF = new PubFun();
//    ExeSQL tExe = new ExeSQL(conn);
//
//    AgentPubFun tAPF = new AgentPubFun();
//    String tCheckFlag = tAPF.AdjustCommCheck(this.mAgentCode.trim(),this.MonDate[0].trim());
//    if ( !tCheckFlag.trim().equals("00") )  {
//      dealError("updLACommision",tCheckFlag.trim());
//      return false;
//    }
//
//    String tSql = "update lacommision set branchattr = '"
//                + this.mNewAgentAttr.trim() + "' where agentcode = '"
//                + this.mAgentCode.trim() + "' and (caldate >= '"
//                + this.MonDate[0] + "' or caldate is null)";
//
//    System.out.println("---刷新LACommision的branchattr ----tSql : " + tSql);
//    if ( !tExe.execUpdateSQL(tSql) )  {
//      dealError("updLACommision","刷新LACommision表的外部展业机构编码失败！" + tExe.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }

    private boolean updateLAAssess(Connection conn)
    {
        LAAssessDB tLAAssessDB = new LAAssessDB(conn);
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        PubFun tPF = new PubFun();

        tLAAssessDB.setAgentCode(mLATreeSchema.getAgentCode());
        tLAAssessDB.setIndexCalNo(this.mIndexCalNo);

        if (!tLAAssessDB.getInfo())
        {
            dealError("updateLAAssess", "取代理人考评信息失败！");
            return false;
        }
        tLAAssessSchema = tLAAssessDB.getSchema();
        tLAAssessSchema.setAgentGroupNew(this.mNewAgentGroup);
        tLAAssessSchema.setState("2");
        tLAAssessSchema.setModifyDate(tPF.getCurrentDate());
        tLAAssessSchema.setModifyTime(tPF.getCurrentTime());
        tLAAssessSchema.setOperator(this.mGlobalInput.Operator);
        tLAAssessDB.setSchema(tLAAssessSchema);

        if (!tLAAssessDB.update())
        {
            dealError("updateLAAssess", "更新代理人考评信息失败！");
            return false;
        }
        return true;
    }

    private boolean InsLAAgentB(Connection conn)
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSchema tSch = new LAAgentSchema();
        LAAgentBSchema tBSch = new LAAgentBSchema();
        LAAgentBDB tBDB = new LAAgentBDB(conn);
        PubFun1 tPF1 = new PubFun1();
        PubFun tPF = new PubFun();

        tLAAgentDB.setAgentCode(mLATreeSchema.getAgentCode());

        if (!tLAAgentDB.getInfo())
        {
            dealError("updateLAAgent", "取代理人信息失败！");
            return false;
        }
        tSch.setSchema(tLAAgentDB.getSchema());

        Reflections rf = new Reflections();
        rf.transFields(tBSch, tSch);

        tBSch.setEdorNo(OAgentAscript.getEdorNo());
        System.out.println(
                "---zsj---AscriptDealPersonData--insLAAgentB--EdorNo = " +
                OAgentAscript.getEdorNo());
        tBSch.setIndexCalNo(this.mIndexCalNo);
        tBSch.setEdorType("01");
        tBSch.setMakeDate(tPF.getCurrentDate());
        tBSch.setMakeTime(tPF.getCurrentTime());
        tBSch.setModifyDate(tPF.getCurrentDate());
        tBSch.setModifyTime(tPF.getCurrentTime());
        tBSch.setOperator(this.mGlobalInput.Operator);

        tBDB.setSchema(tBSch);
        if (!tBDB.insert())
        {
            dealError("InsLAAgentB", "插入LAAgentB备份表失败！");
            return false;
        }

        return true;
    }


    private boolean updateLAAgent(Connection conn)
    {
        LAAgentDB tLAAgentDB = new LAAgentDB(conn);
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        PubFun tPF = new PubFun();

        tLAAgentDB.setAgentCode(mLATreeSchema.getAgentCode());

        if (!tLAAgentDB.getInfo())
        {
            dealError("updateLAAgent", "取代理人信息失败！");
            return false;
        }
        tLAAgentSchema.setSchema(tLAAgentDB.getSchema());
        tLAAgentSchema.setAgentGroup(this.mNewAgentGroup);

        LAAssessDB tLeaderDB = new LAAssessDB();
        tLeaderDB.setAgentCode(this.mAgentCode);
        tLeaderDB.setIndexCalNo(this.mIndexCalNo);

        if (!tLeaderDB.getInfo())
        {
            return false;
        }

        String tAgentGrade1 = tLeaderDB.getAgentGrade1().trim();
        if (tAgentGrade1.equalsIgnoreCase("A04") ||
            tAgentGrade1.equalsIgnoreCase("A05"))
        {
            tLAAgentSchema.setBranchCode(this.mNewAgentGroup);
            if (this.ModTypeFlag.trim().equals("S"))
            {
                if (tLAAgentSchema.getInDueFormDate() == null ||
                    tLAAgentSchema.getInDueFormDate().equals(""))
                {
                    tLAAgentSchema.setInDueFormDate(this.MonDate[0]);
                }
            }
        }
        tLAAgentSchema.setModifyDate(tPF.getCurrentDate());
        tLAAgentSchema.setModifyTime(tPF.getCurrentTime());
        tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
        tLAAgentDB.setSchema(tLAAgentSchema);

        if (!tLAAgentDB.update())
        {
            dealError("updateLAAgent", "更新代理人信息失败！");
            return false;
        }
        return true;
    }

    private boolean insertLATreeB(Connection conn)
    {
        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();

        LATreeBDB tLATreeBDB = new LATreeBDB(conn);
        LATreeBSchema tLATreeBSchema = new LATreeBSchema();

        Reflections rf = new Reflections();
        rf.transFields(tLATreeBSchema, this.mLATreeSchema);

        tLATreeBSchema.setOperator(mGlobalInput.Operator);
        tLATreeBSchema.setOperator2(mLATreeSchema.getOperator());
        tLATreeBSchema.setRemoveType("01");
        tLATreeBSchema.setModifyDate(tPF.getCurrentDate());
        tLATreeBSchema.setModifyTime(tPF.getCurrentTime());
        tLATreeBSchema.setModifyDate2(mLATreeSchema.getModifyDate());
        tLATreeBSchema.setModifyTime2(mLATreeSchema.getModifyTime());
        tLATreeBSchema.setMakeDate(tPF.getCurrentDate());
        tLATreeBSchema.setMakeTime(tPF.getCurrentTime());
        tLATreeBSchema.setMakeDate2(mLATreeSchema.getMakeDate());
        tLATreeBSchema.setMakeTime2(mLATreeSchema.getMakeTime());
        tLATreeBSchema.setEdorNO(OAgentAscript.getEdorNo());
        System.out.println(
                "---zsj---AscriptDealPersonData--insertLATreeB--EdorNo = " +
                OAgentAscript.getEdorNo());
        tLATreeBSchema.setIndexCalNo(this.mIndexCalNo);

        tLATreeBDB.setSchema(tLATreeBSchema);
        if (!tLATreeBDB.insert())
        {
            dealError("InsLATreeB", "插入代理人行政信息转储表失败！");
            return false;
        }
        return true;
    }

//  private void getUseDate()  {
//    PubFun tPF = new PubFun();
//    String monDate[] = new String[2];
//    String lastMon[] = new String[2];
//    String pattern = "yyyy-MM-dd";
//    SimpleDateFormat df = new SimpleDateFormat(pattern);
//
//    java.util.Date today = new java.util.Date();
//    java.util.Date endDate = new java.util.Date();
//
//    endDate = tPF.calDate(today,-1,"M",today);
//    monDate = tPF.calFLDate(tPF.getCurrentDate());
//    lastMon = tPF.calFLDate(df.format(endDate));
//    this.mThisBeginDay = monDate[0];
//    this.mLastEndDay   = lastMon[1];
//  }


    private boolean updateLATree(Connection conn)
    {
        PubFun tPF = new PubFun();

        LATreeDB tLATreeDB = new LATreeDB(conn);
        LATreeSchema tSch = new LATreeSchema();

        tSch.setSchema(this.mLATreeSchema);
        tSch.setAgentGroup(this.mNewAgentGroup);

        if (this.ModTypeFlag.trim().equalsIgnoreCase("S"))
        {
            //取代理人考评信息
            LAAssessDB tLAAssessDB = new LAAssessDB();
            LAAssessSchema tLAAssessSchema = new LAAssessSchema();

            tLAAssessDB.setAgentCode(mLATreeSchema.getAgentCode());
            tLAAssessDB.setIndexCalNo(this.mIndexCalNo);

            //取代理人考评信息
            if (!tLAAssessDB.getInfo())
            {
                dealError("updateLATree", "取代理人考评信息失败！");
                return false;
            }
            tLAAssessSchema = tLAAssessDB.getSchema();

            tSch.setAgentLastGrade(mLATreeSchema.getAgentGrade());
            tSch.setAgentLastSeries(mLATreeSchema.getAgentSeries());
            tSch.setAgentGrade(tLAAssessSchema.getAgentGrade1());
            tSch.setAgentSeries(tLAAssessSchema.getAgentSeries1());

            if (tLAAssessSchema.getAgentGrade1().trim().equalsIgnoreCase("A09"))
            {
                tSch.setUpAgent("BC");
            }
            else
            {
// zsj--modified--2003-10-31:通过查询上级展业机构的branchManager得到上级代理人UpAgent
                //取上级的上级
//        String tUpAgent = this.mLATreeSchema.getUpAgent();
//        LATreeDB tUpDB = new LATreeDB();
//        tUpDB.setAgentCode(tUpAgent);
//        if (!tUpDB.getInfo())
//          return false;
//        tUpAgent = tUpDB.getUpAgent();

                // 取上级代理人
                String tUpAgent = "";
                LABranchGroupDB tBranchDB = new LABranchGroupDB();
                tBranchDB.setAgentGroup(this.mLATreeSchema.getAgentGroup().trim());
                if (!tBranchDB.getInfo())
                {
                    return false;
                }
                String tUpGroup = tBranchDB.getUpBranch().trim();
                if (tUpGroup == null || tUpGroup.equals(""))
                {
                    tUpAgent = "";
                }
                else
                {
                    LABranchGroupDB tUpBranchDB = new LABranchGroupDB();
                    tUpBranchDB.setAgentGroup(tUpGroup);
                    if (!tUpBranchDB.getInfo())
                    {
                        return false;
                    }
                    if (!this.mLATreeSchema.getAgentSeries().equals("A"))
                    {
                        LABranchGroupDB tUpBranchDB1 = new LABranchGroupDB();
                        tUpBranchDB1.setAgentGroup(tUpBranchDB.getUpBranch());
                        if (!tUpBranchDB1.getInfo())
                        {
                            return false;
                        }
                        if (tUpBranchDB1.getBranchManager() == null)
                        {
                            tUpAgent = "";
                        }
                        else
                        {
                            tUpAgent = tUpBranchDB1.getBranchManager().trim();
                        }
                    }
                    else
                    {
                        if (tUpBranchDB.getBranchManager() == null)
                        {
                            tUpAgent = "";
                        }
                        else
                        {
                            tUpAgent = tUpBranchDB.getBranchManager().trim();
                        }
                    }
                }
                tSch.setUpAgent(tUpAgent);
            }
            tSch.setEduManager(mLATreeSchema.getUpAgent());
        }
        else
        {
            tSch.setUpAgent(this.mAgentCode);
        }

        if (this.ModTypeFlag.trim().equalsIgnoreCase("S"))
        {
            tSch.setRearCommStart(this.MonDate[0]);
            tSch.setOldStartDate(mLATreeSchema.getStartDate());
            tSch.setOldEndDate(this.MonDate[1]);
            tSch.setStartDate(this.MonDate[0]);
            tSch.setAssessType("1");
            tSch.setState("1");
        }

        tSch.setModifyDate(tPF.getCurrentDate());
        tSch.setModifyTime(tPF.getCurrentTime());
        tSch.setOperator(this.mGlobalInput.Operator);
        tSch.setAstartDate(this.MonDate[0]);

        if (this.ModTypeFlag.trim().equalsIgnoreCase("S"))
        {
            String tAS = "";
//      tAS = getAscriptRelation(this.mLATreeSchema.getAscriptSeries().trim(),
//                               this.mLATreeSchema.getUpAgent().trim(),
//                               this.mLATreeSchema.getAgentSeries().trim());
            if (this.mLATreeSchema.getAscriptSeries() == null
                || this.mLATreeSchema.getAscriptSeries().equals(""))
            {
                tAS = this.mLATreeSchema.getUpAgent().trim();
            }
            else
            {
                if (this.mLATreeSchema.getUpAgent() == null)
                {
                    tAS = this.mLATreeSchema.getAscriptSeries().trim() + ":";
                }
                else
                {
                    tAS = this.mLATreeSchema.getAscriptSeries().trim()
                          + ":" + this.mLATreeSchema.getUpAgent().trim();
                }
            }
            tSch.setAscriptSeries(tAS);
        }

        tLATreeDB.setSchema(tSch);

        if (!tLATreeDB.update())
        {
            dealError("updateLATree", "更新代理人行政信息表失败！");
            return false;
        }

        // 插入代理人行政信息附属表
        if (this.ModTypeFlag.trim().equalsIgnoreCase("S"))
        {
            if (!updLATreeAccessory(conn))
            {
                dealError("updateLATree", "刷新代理人行政信息附属表失败！");
                return false;
            }
        }
        return true;
    }

    private String getAscriptRelation(String tAS, String tRearCode,
                                      String tAgentSeries)
    {
        String tRs = "";
        int tIndex1 = tAS.trim().indexOf(":");
        int tIndex2 = tAS.trim().indexOf(":", tIndex1 + 1);
        int tIndex3 = tAS.trim().lastIndexOf(":");

        if (tAgentSeries.equals("A"))
        {
            tRs = tRearCode.trim() + tAS;
        }

        if (tAgentSeries.equals("B"))
        {
            tRs = tAS.trim().substring(0, tIndex1 + 1) + tRearCode.trim()
                  + tAS.trim().substring(tIndex2);
        }

        if (tAgentSeries.equals("C"))
        {
            tRs = tAS.trim().substring(0, tIndex2 + 1) + tRearCode.trim()
                  + tAS.trim().substring(tIndex3);
        }

        if (tAgentSeries.equals("D"))
        {
            tRs = tAS.trim().substring(0, tIndex3 + 1) + tRearCode.trim();
        }

        return tRs;
    }


    private boolean updLATreeAccessory(Connection conn)
    {
        PubFun tPF = new PubFun();

        LATreeAccessoryDB tAccDB = new LATreeAccessoryDB(conn);
        LATreeAccessorySchema tAccSch = new LATreeAccessorySchema();
        LATreeAccessorySet tSet = new LATreeAccessorySet();

        LAAssessDB tAssessDB = new LAAssessDB();
        tAssessDB.setAgentCode(this.mLATreeSchema.getAgentCode());
        tAssessDB.setIndexCalNo(this.mIndexCalNo);
        if (!tAssessDB.getInfo())
        {
            return false;
        }
        System.out.println("---建议职级 agentgrade1 : " + tAssessDB.getAgentGrade1());

        //zsj--add--2004-3-19:因为代理人降级时不删除latreeaccessory表，所以当代理人降级后又晋升时，需要将旧的记录备份
        String tAgentGrade1 = tAssessDB.getAgentGrade1().trim();
        String tOrGrade = "";
        if (tAgentGrade1.trim().equals("A04"))
        {
            tOrGrade = "A05";
        }
        if (tAgentGrade1.trim().equals("A05"))
        {
            tOrGrade = "A04";
        }
        if (tAgentGrade1.trim().equals("A06"))
        {
            tOrGrade = "A07";
        }
        if (tAgentGrade1.trim().equals("A07"))
        {
            tOrGrade = "A06";
        }

        tAccDB.setAgentCode(mLATreeSchema.getAgentCode());
        tAccDB.setAgentGrade(mLATreeSchema.getAgentGrade());
        String sql = "SELECT * FROM LATreeAccessory WHERE AgentCode = '" +
                     this.mLATreeSchema.getAgentCode().trim() +
                     "' AND (AgentGrade = '" +
                     tAgentGrade1.trim() + "' or agentgrade = '" +
                     tOrGrade.trim() + "')";

        System.out.println("---sql: " + sql);

        tSet = tAccDB.executeQuery(sql);

        LATreeDB tDB = new LATreeDB();
        LATreeSchema tSch = new LATreeSchema();
        tDB.setAgentCode(this.mLATreeSchema.getAgentCode());
        if (!tDB.getInfo())
        {
            return false;
        }
        tSch.setSchema(tDB.getSchema());

        if (tSet.size() != 0)
        {
            tAccSch = tSet.get(1);

            // 插入备份表
            if (!InsLATreeAccessoryB(tAccSch, conn))
            {
                return false;
            }

            tAccSch.setAgentGrade(tAgentGrade1);
            tAccSch.setRearAgentCode(mLATreeSchema.getUpAgent());
            tAccSch.setAgentGroup(this.mNewAgentGroup);
            tAccSch.setCommBreakFlag("0");
            tAccSch.setRearFlag("0"); //cg 2004-04-14添加
            tAccSch.setstartdate(this.MonDate[0]);
            tAccSch.setenddate("");
            tAccSch.setIntroBreakFlag("0");
            tAccSch.setBackCalFlag("1");
            tAccSch.setModifyDate(tPF.getCurrentDate());
            tAccSch.setModifyTime(tPF.getCurrentTime());
            tAccSch.setOperator(this.mGlobalInput.Operator);

            tAccDB.setSchema(tAccSch);
            if (!tAccDB.update())
            {
                dealError("updateLATree", "刷新代理人行政信息附属表失败！");
                return false;
            }
        }
        else
        {
            tAccSch.setAgentCode(tSch.getAgentCode());
            tAccSch.setAgentGrade(tAssessDB.getAgentGrade1().trim());
            tAccSch.setAgentGroup(this.mNewAgentGroup);
            tAccSch.setCommBreakFlag("0");
            tAccSch.setRearFlag("0"); //cg 2004-04-14添加
            tAccSch.setenddate("");
            tAccSch.setstartdate(this.MonDate[0]);
            tAccSch.setBackCalFlag("1");
            tAccSch.setMakeDate(tPF.getCurrentDate());
            tAccSch.setMakeTime(tPF.getCurrentTime());
//      tAccSch.setManageCom(this.mGlobalInput.ManageCom);
//      tAccSch.setManageCom(this.mLATreeSchema.getManageCom() );//蔡刚 2004-04-14修改
            tAccSch.setModifyDate(tPF.getCurrentDate());
            tAccSch.setModifyTime(tPF.getCurrentTime());
            tAccSch.setOperator(this.mGlobalInput.Operator);
            tAccSch.setRearAgentCode(this.mLATreeSchema.getUpAgent());

            tAccDB.setSchema(tAccSch);
            if (!tAccDB.insert())
            {
                System.out.println(tAccDB.mErrors.getFirstError());
                dealError("updateLATree",
                          "插入代理人行政信息附属表失败！" + tAccDB.mErrors.getFirstError());
                return false;
            }
        }
        return true;
    }

    private boolean InsLATreeAccessoryB(LATreeAccessorySchema tSch,
                                        Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();
        PubFun tPF = new PubFun();
        LATreeAccessoryBSchema tBSch = new LATreeAccessoryBSchema();
        LATreeAccessoryBDB tDB = new LATreeAccessoryBDB(conn);

        Reflections rf = new Reflections();
        rf.transFields(tBSch, tSch);

        tBSch.setEdorNo(OAgentAscript.getEdorNo());
        System.out.println(
                "---zsj---AscriptDealPersonData--insLATreeaccessoryB--EdorNo = " +
                OAgentAscript.getEdorNo());
        tBSch.setEdorType("01"); //转储类型－考核结果
        tBSch.setIndexCalNo(this.mIndexCalNo);
        tBSch.setMakeDate(tPF.getCurrentDate());
        tBSch.setMakeTime(tPF.getCurrentTime());
        tBSch.setModifyDate(tPF.getCurrentDate());
        tBSch.setModifyTime(tPF.getCurrentTime());
        tBSch.setOperator(this.mGlobalInput.Operator);

        tDB.setSchema(tBSch);
        if (!tDB.insert())
        {
            dealError("InsLATreeAccessoryB", "备份附属表失败！");
            return false;
        }
        return true;
    }

    private boolean insertLABranchGroupB(Connection conn)
    {
        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();

        LABranchGroupDB tDB = new LABranchGroupDB();
        LABranchGroupSchema tSh = new LABranchGroupSchema();
        LABranchGroupBSchema tBSh = new LABranchGroupBSchema();
        LABranchGroupBDB tBDB = new LABranchGroupBDB(conn);

        tDB.setAgentGroup(this.mLATreeSchema.getAgentGroup());
        if (!tDB.getInfo())
        {
            dealError("DealGroupData", "查询展业机构信息失败！");
            return false;
        }
        tSh.setSchema(tDB.getSchema());

        Reflections rf = new Reflections();
        rf.transFields(tBSh, tSh);

        tBSh.setEdorNo(OAgentAscript.getEdorNo());
        System.out.println(
                "---zsj---AscriptDealPersonData--insLABranchgroupB--EdorNo = " +
                OAgentAscript.getEdorNo());
        tBSh.setIndexCalNo(this.mIndexCalNo);
        tBSh.setEdorType("01");
        tBSh.setEndDate(this.MonDate[0]);
        tBSh.setMakeDate2(tSh.getMakeDate());
        tBSh.setMakeDate(tPF.getCurrentDate());
        tBSh.setMakeTime2(tSh.getMakeTime());
        tBSh.setMakeTime(tPF.getCurrentTime());
        tBSh.setModifyDate2(tSh.getModifyDate());
        tBSh.setModifyDate(tPF.getCurrentDate());
        tBSh.setModifyTime2(tSh.getModifyTime());
        tBSh.setModifyTime(tPF.getCurrentTime());
        tBSh.setOperator(this.mGlobalInput.Operator);
        tBSh.setOperator2(tSh.getOperator());

        tBDB.setSchema(tBSh);
        if (!tBDB.insert())
        {
            return false;
        }

        return true;
    }

    private boolean insertLABranchGroup(Connection conn)
    {
        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();

        LABranchGroupDB tQryDB = new LABranchGroupDB();
        LABranchGroupDB tInsDB = new LABranchGroupDB(conn);
        LABranchGroupSchema tInsSch = new LABranchGroupSchema();
        String tUpBranch = "";

        //若由专员晋升为业务经理则其上级机构为当前组的UpBranch
        String tUpBranchFlag = "";
        LAAssessDB tLAAssessDB = new LAAssessDB();
        LAAssessSchema tLAAssessSch = new LAAssessSchema();
        tLAAssessDB.setAgentCode(this.mLATreeSchema.getAgentCode());
        tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
        if (!tLAAssessDB.getInfo())
        {
            return false;
        }
        if (tLAAssessDB.getAgentSeries1().trim().equalsIgnoreCase("B"))
        {
            tUpBranchFlag = "T";
        }

        //查询上级展业机构
        tQryDB.setAgentGroup(this.mLATreeSchema.getAgentGroup());
        if (!tQryDB.getInfo())
        {
            dealError("insertLABranchGroup", "查询上级展业机构失败！");
            return false;
        }

        if (!tUpBranchFlag.equals("T"))
        {
            tQryDB.setAgentGroup(tQryDB.getUpBranch());
            if (!tQryDB.getInfo())
            {
                dealError("insertLABranchGroup", "查询上级展业机构失败！");
                return false;
            }
        }

        if (this.mLATreeSchema.getAgentGrade().trim().equalsIgnoreCase("A08"))
        {
            ExeSQL tExe = new ExeSQL();
            String sql =
                    "select agentgroup from labranchgroup where branchattr = '"
                    + tQryDB.getManageCom().trim() + "'";
            tUpBranch = tExe.getOneValue(sql);
        }
        else
        {
            tUpBranch = tQryDB.getUpBranch();
        }

        tInsSch.setAgentGroup(this.mNewAgentGroup);
        tInsSch.setBranchSeries(AgentPubFun.getBranchSeries(this.mNewAgentGroup));
        tInsSch.setManageCom(this.mLATreeSchema.getManageCom());
        tInsSch.setUpBranch(tUpBranch);
        tInsSch.setBranchAttr(this.mNewAgentAttr);
        tInsSch.setUpBranchAttr("0");
        tInsSch.setName("晋升生成");

        //确定展业机构级别
        if (tLAAssessDB.getAgentSeries1().trim().equalsIgnoreCase("B"))
        {
            tInsSch.setBranchLevel("01");
        }
        if (tLAAssessDB.getAgentSeries1().trim().equalsIgnoreCase("C"))
        {
            tInsSch.setBranchLevel("02");
        }
        if (tLAAssessDB.getAgentGrade1().trim().equalsIgnoreCase("A08"))
        {
            tInsSch.setBranchLevel("03");
        }
        if (tLAAssessDB.getAgentGrade1().trim().equalsIgnoreCase("A09"))
        {
            tInsSch.setBranchLevel("04");
        }

        tInsSch.setBranchType(tLAAssessDB.getBranchType());
        tInsSch.setBranchManager(this.mLATreeSchema.getAgentCode());
        tInsSch.setEndFlag("N");
        tInsSch.setOperator(this.mGlobalInput.Operator);
        tInsSch.setFoundDate(this.MonDate[0]);
        tInsSch.setMakeDate(tPF.getCurrentDate());
        tInsSch.setMakeTime(tPF.getCurrentTime());
        tInsSch.setModifyDate(tPF.getCurrentDate());
        tInsSch.setModifyTime(tPF.getCurrentTime());

        //查询机构主管姓名
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(this.mLATreeSchema.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            return false;
        }
        tInsSch.setBranchManagerName(tLAAgentDB.getName());

        tInsDB.setSchema(tInsSch);

        if (!tInsDB.insert())
        {
            dealError("insertLABranchGroup", "插入展业机构表失败！");
            return false;
        }
        return true;
    }

    private boolean updateLABranchGroup(Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();
        PubFun tPF = new PubFun();
        VData tNewGroup = new VData();
        LABranchGroupDB tDB = new LABranchGroupDB(conn);
        LABranchGroupSchema tSh = new LABranchGroupSchema();

        tDB.setAgentGroup(this.mLATreeSchema.getAgentGroup());
        if (!tDB.getInfo())
        {
            dealError("updLABranchGroup", "查询展业机构信息失败！");
            return false;
        }
        tSh.setSchema(tDB.getSchema());

        //展业机构新名称暂时赋空值
        tSh.setName("生成");
        tSh.setUpBranch(this.mNewAgentGroup);

        tSh.setBranchAttr(this.mNewAgentAttr);
        tSh.setModifyDate(tPF.getCurrentDate());
        tSh.setModifyTime(tPF.getCurrentTime());
        tSh.setOperator(this.mGlobalInput.Operator);

        tDB.setSchema(tSh);
        if (!tDB.update())
        {
            return false;
        }

        return true;
    }

    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();
        tError.moduleName = "AscriptDealData";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }

    private String[] genDate()
    {
        String[] tMonDate = new String[2];
        PubFun tPF = new PubFun();

        String tYear = this.mIndexCalNo.trim().substring(0, 4);
        String tMon = this.mIndexCalNo.trim().substring(4, 6);

        //计算旧职级的止聘日期
        String tOldDate = tYear + "-" + tMon + "-01";
        tMonDate = tPF.calFLDate(tOldDate);
        String tOldDay = tMonDate[1];

        //计算新职级的起聘日期或转正日期
        if (tMon.equals("12"))
        {
            tYear = Integer.toString(Integer.parseInt(tYear) + 1);
            tMon = "01";
        }
        else
        {
            tMon = Integer.toString(Integer.parseInt(tMon) + 1);
        }
        int tLength = tMon.length();
        if (tLength == 1)
        {
            tMon = "0" + tMon;
        }
        String tDate = tYear.trim() + "-" + tMon.trim() + "-" + "01";

        tMonDate = tPF.calFLDate(tDate);
        String tNewDay = tMonDate[0];

        tMonDate[0] = tNewDay;
        tMonDate[1] = tOldDay;
        return tMonDate;
    }

    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
            this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                         getObjectByObjectName("LATreeSchema",
                    0));
            this.mIndexCalNo = (String) cInputData.getObjectByObjectName(
                    "String", 0);
            this.mNewAgentGroup = (String) cInputData.getObjectByObjectName(
                    "String", 1);
            this.mNewAgentAttr = (String) cInputData.getObjectByObjectName(
                    "String", 2);
            this.ModTypeFlag = (String) cInputData.getObjectByObjectName(
                    "String", 3);
            this.InsFlag = (String) cInputData.getObjectByObjectName("String",
                    4);
            this.mAgentCode = (String) cInputData.getObjectByObjectName(
                    "String", 5);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }

        if (mNewAgentGroup == null || mLATreeSchema == null || mIndexCalNo == null ||
            mNewAgentAttr == null)
        {
            dealError("getInputData", "没有得到足够数据！");
            return false;
        }
        return true;
    }

}
