package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 中英团险考核程序-机构处理</p>
 * <p>Description: 中英团险考核程序-机构处理</p>
 * <p>Copyright: Copyright (c) sinosoft 2004</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public class ACGrpGrdAssessComBL extends GradeAssessPublic
{
//    //全局变量
//    protected CErrors mErrors = new CErrors(); //错误处理类
//    protected GlobalInput mGlobalInput = new GlobalInput(); //保存传入参数
//    protected String mOperate; //保存传入的操作数
//    protected String mIndexCalNo; //考核指标计算编码
//    protected String mOperator; //操作人员
//    protected String mBranchType; //展业类型
//    protected String mAreaType; //地区类型
    //必须初始化的信息
    private String mManageCom; //管理机构,注意这个变量随着每个机构循环而变动
    private VData mGInputData; //往后台传入数据(机构)
    private VData mPInputData; //往后台传入数据(人员)
    //用来存储将要考核的管理机构
    private VData mManCom = new VData();
    private LAAssessMainSet mLAAssessMainSet = new LAAssessMainSet();

    ACGrpGrdAssessPerBL mACGrpGrdAssessPerBL; //单个人处理类
    ACGrpGrdAssessComBLS mACGrpGrdAssessComBLS; //数据库插入

    public ACGrpGrdAssessComBL()
    {
    }

    public static void main(String[] args)
    {
    }

    //前后台数据交互
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("--------ACGrpGrdAssessComBL.submitData--------");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            if (mErrors.needDealError())
            {
                System.out.println("考核异常结束原因：" + mErrors.getFirstError());
            }
            return false;
        }
//        //准备往后台的数据
//        if (!prepareOutputData())
//            return false;
        System.out.println("--------ACGrpGrdAssessComBL over--------");
        return true;
    }

    // 准备往后台的数据,将此次管理机构计算结果放入LAAssessMain中
    //由于是按照外层机构循环，内层人员循环的方式进行，所以计算完成后，插入LAAssessMain时
    //要将所有的级别都要统计
    public boolean prepareOutputData()
    {
        //从laagentgrade中取得所有级别，注意条件
        //查询机构，用前台管理机构代码查询相关所有8位管理机构
        String tSQL = "select * from laagentgrade where BranchType = '2' "
                      + "and GradeProperty6 = '1' order by GradeCode";
        System.out.println("职级查询Sql:" + tSQL);
        LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
        LAAgentGradeSet tLAAgentGradeSet = new LAAgentGradeSet();
        tLAAgentGradeSet = tLAAgentGradeDB.executeQuery(tSQL);
        System.out.println("职级个数:" + tLAAgentGradeSet.size());
        //循环处理每个职级
        for (int i = 1; i <= tLAAgentGradeSet.size(); i++)
        {
            //获得职级编码
            String tGradeCode = "";
            tGradeCode = tLAAgentGradeSet.get(i).getGradeCode().trim();
            ExeSQL tExeSQL = new ExeSQL();
            //统计人数
            String tSql = "select count(*) from LAAssessAccessory where "
                          + "indexcalno = '" + mIndexCalNo + "' "
                          + " and agentgrade = '" + tGradeCode
                          + " and managecom = '" + mManageCom
                          + "' and branchtype = '2' "
                          + "' and AssessType = '00'";
            String iCount = tExeSQL.getOneValue(tSql);
            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ACGrpGrdAssessComBL";
                tError.functionName = "prepareOutputData()";
                tError.errorMessage = "查询LAAssessAccessory表机构人数出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            //准备数据
            String currentDate = PubFun.getCurrentDate();
            String currentTime = PubFun.getCurrentTime();
            LAAssessMainSchema tLAAssessMainSchema = new LAAssessMainSchema();
            tLAAssessMainSchema.setIndexCalNo(mIndexCalNo);
            tLAAssessMainSchema.setAgentGrade(tGradeCode);
            tLAAssessMainSchema.setBranchType("2");
            tLAAssessMainSchema.setManageCom(mManageCom);
            tLAAssessMainSchema.setState("0");
            tLAAssessMainSchema.setAssessCount(iCount);
            //tLAAssessMainSchema.setConfirmCount(0);
            tLAAssessMainSchema.setOperator(mOperator);
            tLAAssessMainSchema.setMakeDate(currentDate);
            tLAAssessMainSchema.setMakeTime(currentTime);
            tLAAssessMainSchema.setModifyDate(currentDate);
            tLAAssessMainSchema.setModifyTime(currentTime);
            tLAAssessMainSchema.setBranchAttr("8611000000001");
            tLAAssessMainSchema.setAssessType("00");
            //放入集合
            mLAAssessMainSet.add(tLAAssessMainSchema);
        }
        //放入容器
        mGInputData = new VData();
        mGInputData.add(mGlobalInput);
        mGInputData.add(mLAAssessMainSet);
        return true;
    }

    //获得输入数据
    public boolean getInputData(VData cInputData)
    {
        System.out.println("--------ACGrpGrdAssessComBL.getInputData--------");
        String tYear; //指标计算年份
        String tMonth; //指标计算月份
        //信息收集
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        //获得管理机构
        mManageCom = (String) cInputData.getObject(1);
        //获得考核年份
        tYear = (String) cInputData.getObject(2);
        //获得考核月份
        tMonth = (String) cInputData.getObject(3);
        //获得展业类型
        mBranchType = (String) cInputData.getObject(4);
        //获得操作人员
        mOperator = mGlobalInput.Operator;
        System.out.println("ManageCom:" + mManageCom);
        //生成考核指标计算编码
        mIndexCalNo = tYear + tMonth;
        System.out.println("考核指标计算编码：" + mIndexCalNo);
        if (mGlobalInput == null || mManageCom == null || tYear == null ||
            tMonth == null)
        {
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (Integer.parseInt(tYear) <= 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "年份值没有初始化。";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (Integer.parseInt(tMonth) <= 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "月份值没有初始化。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //对数据进行业务处理
    public boolean dealData()
    {
        System.out.println("--------ACGrpGrdAssessComBL.dealData--------");
        //当操作数为"INSERT||MAIN"时进行考核计算
        if (mOperate.equals("INSERT||MAIN"))
        {
            if (!AssessCalculate())
            {
                if (mErrors.needDealError())
                {
                    System.out.println("程序异常结束原因：" + mErrors.getFirstError());
                }
                return false;
            }
        }
        else if (mOperate.equals("DELETE||MAIN"))
        {
            if (!AssessDelete())
            {
                if (mErrors.needDealError())
                {
                    System.out.println("程序异常结束原因：" + mErrors.getFirstError());
                }
                return false;
            }
        }
        return true;
    }

    /**
     * AssessDelete
     * 删除已经考核的内容
     * @return boolean
     */
    private boolean AssessDelete()
    {
        //查询机构，用前台管理机构代码查询相关所有8位管理机构
        String tSQL = "select * from ldcom where comcode like '" + mManageCom
                      + "%' and length(trim(comcode)) = 8 order by comcode asc";
        System.out.println("管理机构Sql:" + tSQL);
        LDComDB tLDComDB = new LDComDB();
        LDComSet cLDComSet = new LDComSet();
        cLDComSet = tLDComDB.executeQuery(tSQL);
        for (int i = 1; i <= cLDComSet.size(); i++)
        {
            //获得８位管理机构
            String tComCode = "";
            tComCode = cLDComSet.get(i).getComCode().trim();
            //保证只计算8位的分公司
            if (tComCode.length() != 8)
            {
                continue;
            }
            //放入容器
            mGInputData = new VData();
            mGInputData.add(mGlobalInput);
            mGInputData.add(mIndexCalNo);
            mGInputData.add(tComCode);
            mACGrpGrdAssessComBLS = new ACGrpGrdAssessComBLS();
            System.out.println("Start ACGrpGrdAssessComBLS Submit...");
            mACGrpGrdAssessComBLS.submitData(this.mGInputData, mOperate);
            System.out.println("End ACGrpGrdAssessComBLS Submit...");
            if (mACGrpGrdAssessComBLS.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(mACGrpGrdAssessComBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ACGrpGrdAssessComBLS";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败";
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        return true;
    }

    /**
     * CalManagecom
     * 计算那些管理机构需要考核
     * @return VData
     */
    public boolean CalManagecom()
    {
        //查询机构，用前台管理机构代码查询相关所有8位管理机构
        String tSQL = "select * from ldcom where comcode like '" + mManageCom
                      + "%' and length(trim(comcode)) = 8 order by comcode asc";
        System.out.println("管理机构Sql:" + tSQL);
        LDComDB tLDComDB = new LDComDB();
        LDComSet cLDComSet = new LDComSet();
        cLDComSet = tLDComDB.executeQuery(tSQL);
        System.out.println("管理机构个数:" + cLDComSet.size());
        //循环对每一个８位机构进行校验，校验条件为：
        //1 - 先校验机构是否满足本月佣金计算过，下个月佣金没算过的条件，
        //    不满则跳过此机构不做考核处理；
        //2 - 满足条件1则判断此机构是否满足本月作过佣金审核确认，没作过则给出错误提示；
        //3 - 如果前两个条件均通过则此机构可以参加考核计算；
        for (int i = 1; i <= cLDComSet.size(); i++)
        {
            //获得８位管理机构
            String tComCode = "";
            tComCode = cLDComSet.get(i).getComCode().trim();
            //保证只计算8位的分公司
            if (tComCode.length() != 8)
            {
                continue;
            }
//            System.out.println("*******************");
//            System.out.println("校验8位的分公司代码" + tComCode);
//            //对条件１进行校验
//            PubCheckField checkField1 = new PubCheckField();
//            VData cInputData = new VData();
//            //设置计算时要用到的参数值
//            TransferData tTransferData = new TransferData();
//            tTransferData.setNameAndValue("IndexCalNo", this.mIndexCalNo);
//            tTransferData.setNameAndValue("BranchType", this.mBranchType);
//            tTransferData.setNameAndValue("ManageCom", tComCode);
//            //通过CKBYFIELD
//            LMCheckFieldSchema tLMCheckFieldSchema = new LMCheckFieldSchema();
//            tLMCheckFieldSchema.setRiskCode("000000");
//            tLMCheckFieldSchema.setRiskVer("2004");
//            tLMCheckFieldSchema.setFieldName("WageCheckCalAssessBL");
//            //通过 CKBYFIELD 方式校验
//            cInputData.add(tTransferData);
//            cInputData.add(tLMCheckFieldSchema);
//            if (!checkField1.submitData(cInputData, "CKBYFIELD")) {
//                System.out.println("校验机构佣金时发现错误");
//                //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
//                if (checkField1.mErrors.needDealError()) {
//                    System.out.println("ERROR-S-" +
//                                       checkField1.mErrors.getFirstError());
//                    CError tError = new CError();
//                    tError.moduleName = "ACGrpGrdAssessComBL";
//                    tError.functionName = "AssessCalculate";
//                    tError.errorMessage = "对佣金校验处理时出错！";
//                    this.mErrors.addOneError(tError);
//                    System.out.println("对佣金校验处理时出错！！");
//                    return false;
//                } else {
//                    VData t = checkField1.getResultMess();
//                    System.out.println("管理机构" + tComCode + t.get(0).toString());
//                    System.out.println("不需对此管理机构进行考核计算：" + tComCode);
//                    //跳过此机构，不参加计算
//                    continue;
//                }
//            }
//            //对条件２进行校验
//            checkField1 = new PubCheckField();
//            cInputData = new VData();
//            //设置计算时要用到的参数值
//            tTransferData = new TransferData();
//            tTransferData.setNameAndValue("IndexCalNo", this.mIndexCalNo);
//            tTransferData.setNameAndValue("BranchType", this.mBranchType);
//            tTransferData.setNameAndValue("ManageCom", tComCode);
//            //通过CKBYFIELD
//            tLMCheckFieldSchema = new LMCheckFieldSchema();
//            tLMCheckFieldSchema.setRiskCode("000000");
//            tLMCheckFieldSchema.setRiskVer("2004");
//            tLMCheckFieldSchema.setFieldName("WageConfCalAssessBL");
//            //通过 CKBYFIELD 方式校验
//            cInputData.add(tTransferData);
//            cInputData.add(tLMCheckFieldSchema);
//            if (!checkField1.submitData(cInputData, "CKBYFIELD")) {
//                System.out.println("校验机构佣金是否审核时发现错误");
//                //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
//                if (checkField1.mErrors.needDealError()) {
//                    System.out.println("ERROR-S-" +
//                                       checkField1.mErrors.getFirstError());
//                    CError tError = new CError();
//                    tError.moduleName = "ACGrpGrdAssessComBL";
//                    tError.functionName = "AssessCalculate";
//                    tError.errorMessage = "判断机构佣金是否审核时出错！";
//                    this.mErrors.addOneError(tError);
//                    System.out.println("判断机构佣金是否审核时出错！！");
//                    return false;
//                } else {
//                    VData t = checkField1.getResultMess();
//                    CError tError = new CError();
//                    tError.moduleName = "ACGrpGrdAssessComBL";
//                    tError.functionName = "AssessCalculate";
//                    tError.errorMessage = "管理机构" + tComCode + t.get(0).toString();
//                    this.mErrors.addOneError(tError);
//                    System.out.println("管理机构" + tComCode + t.get(0).toString());
//                    System.out.println("不对此管理机构进行考核计算：" + tComCode);
//                    return false;
//                }
//            }
            //两个条件都满足则说明此机构可以参加考核计算
            mManCom.add(tComCode);
            System.out.println("校验后符合考核计算的机构为：" + tComCode);
        }
        return true;
    }

    /**
     * getAreaTypeForAssess
     * 获得地区类型，从8位机构编码中
     * @param cManagecom String
     * @return boolean
     */
    private boolean getAreaTypeForAssess(String cManagecom)
    {
        //获得二级管理机构
        String tManageCom = getSecondManageCom(mManageCom);
        if (tManageCom == null)
        {
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessComBL";
            tError.functionName = "getAreaTypeForAssess";
            tError.errorMessage = "查询管理机构" + cManagecom +
                                  "的地区类型出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //从二级管理机构查找出地区类型
        String cType="";
        if(mBranchType.equals("2"))  cType="02";
        if(mBranchType.equals("1"))  cType="01";
        String tAreaType = AgentPubFun.getAreaType(tManageCom,cType);
        if (tAreaType == null)
        {
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessComBL";
            tError.functionName = "getAreaTypeForAssess";
            tError.errorMessage = "查询管理机构" + cManagecom +
                                  "的地区类型出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mAreaType = tAreaType;
        return true;
    }

    /**
     * getSecondManageCom
     * 获得三级管理机构对应的二级管理机构
     * @param cManageCom String
     * @return String
     */
    private String getSecondManageCom(String cManageCom)
    {
//        String tSql = "Select trim(code2) From LDCodeRela Where RelaType = 'thirdtosecond' and trim(code1) = '" +
//                      cManageCom + "'";
//        ExeSQL tExeSQL = new ExeSQL();
//        String tSecond = tExeSQL.getOneValue(tSql);
//        if (tExeSQL.mErrors.needDealError())
//            return null;
//        if (tSecond == null || tSecond.equals(""))
//            return null;
//        return tSecond;
        //Modify xjh 2005/02/16
        String tSecond = new String();
        Calculator tCal = new Calculator();
        tCal.addBasicFactor("Managecom", cManageCom);
        tCal.setCalCode("xjh003");
        tSecond = tCal.calculate();
        return tSecond;
    }

    //进行考核计算
    public boolean AssessCalculate()
    {
        System.out.println(
                "--------ACGrpGrdAssessComBL.AssessCalculate--------");
        //计算那些管理机构需要考核
        if (!CalManagecom())
        {
            return false;
        }
        //1 - 判断是否存在符合考核计算条件的机构，如果存在则进行考核计算
        if (mManCom.size() != 0)
        {
            System.out.println("符合考核计算的8位管理机构个数为：" + mManCom.size());
            //循环处理每个要进行考核的管理机构
            for (int i = 0; i < mManCom.size(); i++)
            {
                String tComCode = "";
                this.mManageCom = (String) mManCom.getObject(i);
                System.out.println(String.valueOf(i) + "-----进行考核计算的8位分公司代码为：" +
                                   this.mManageCom);
                //获得地区类型
                if (!getAreaTypeForAssess(mManageCom))
                {
                    return false;
                }
                //校验管理机构是否作过考核计算
                if (!isAssesssCal())
                {
                    return false;
                }

                //逐个对每个管理机构进行计算
                System.out.println("开始计算 " + mManageCom + " 管理机构");
                if (!perCalculate())
                {
                    if (mErrors.needDealError())
                    {
                        System.out.println("程序异常结束原因：" + mErrors.getFirstError());
                    }
                    return false;
                }

                //准备往后台的数据，将此次计算结果放入LAAssessMain中
                if (!prepareOutputData())
                {
                    return false;
                }

                System.out.println(mManageCom + "管理机构计算完成准备往后台提交");
                System.out.println("Start ACGrpGrdAssessComBLS Submit...");
                mACGrpGrdAssessComBLS = new
                                        ACGrpGrdAssessComBLS();
                mACGrpGrdAssessComBLS.submitData(this.mGInputData, mOperate);
                System.out.println("End ACGrpGrdAssessComBLS Submit...");
                if (mACGrpGrdAssessComBLS.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(mACGrpGrdAssessComBLS.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ACGrpGrdAssessComBLS";
                    tError.functionName = "submitData";
                    tError.errorMessage = "数据提交失败";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                System.out.println("管理机构" + this.mManageCom + "计算成功！");
            }
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessComBL";
            tError.functionName = "AssessCalculate";
            tError.errorMessage = "不存在符合考核计算条件的机构！";
            this.mErrors.addOneError(tError);
            System.out.println("不存在符合考核计算条件的机构！");
            return false;
        }
        return true;
    }

    /**
     * isAssesssCal
     * 校验管理机构是否作过考核计算
     * @return boolean
     */
    private boolean isAssesssCal()
    {
        PubCheckField checkField1 = new PubCheckField();
        VData cInputData = new VData();
        //设置计算时要用到的参数值
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("IndexCalNo", this.mIndexCalNo);
        //团险部分输入参数中没有职级，即同时将所有职级进行考核
        //tTransferData.setNameAndValue("AgentGrade", this.mAgentGrade);
        tTransferData.setNameAndValue("ManageCom", this.mManageCom);
        tTransferData.setNameAndValue("AssessType", "00");
        tTransferData.setNameAndValue("BranchType", "2");
        //通过CKBYFIELD
        LMCheckFieldSchema tLMCheckFieldSchema = new LMCheckFieldSchema();
        tLMCheckFieldSchema.setRiskCode("000000");
        tLMCheckFieldSchema.setRiskVer("2004");
        tLMCheckFieldSchema.setFieldName("IsAssessCal");
        //通过 CKBYFIELD 方式校验
        cInputData.add(tTransferData);
        cInputData.add(tLMCheckFieldSchema);
        if (!checkField1.submitData(cInputData, "CKBYFIELD"))
        {
            System.out.println("校验机构是否作过考核时发现错误");
            //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
            if (checkField1.mErrors.needDealError())
            {
                System.out.println("ERROR-S-" +
                                   checkField1.mErrors.getFirstError());
                CError tError = new CError();
                tError.moduleName = "ACGrpGrdAssessComBL";
                tError.functionName = "isAssesssCal";
                tError.errorMessage = "判断机构是否作过考核计算时出错！";
                this.mErrors.addOneError(tError);
                System.out.println("判断机构是否作过考核计算时出错！！");
                return false;
            }
            else
            {
                VData t = checkField1.getResultMess();
                System.out.println("管理机构" + this.mManageCom +
                                   t.get(0).toString());
                CError tError = new CError();
                tError.moduleName = "ACGrpGrdAssessComBL";
                tError.functionName = "isAssesssCal";
                tError.errorMessage = "管理机构" + this.mManageCom +
                                      t.get(0).toString();
                this.mErrors.addOneError(tError);
                System.out.println("管理机构" + this.mManageCom +
                                   t.get(0).toString());
                return false;
            }
        }
        return true;
    }

    /**
     * perCalculate
     * 单个管理机构计算
     * @return boolean
     * @todo Implement this com.sinosoft.lis.agentassess.AssessInterface
     *   method
     */
    public boolean perCalculate()
    {
        //求管理机构下的每个人
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = new LATreeSet();
        String tSQL = " select * from LATree a where ManageCom = '" +
                      mManageCom + "' " //使用=不使用like是因为已经是三级管理机构了
                      +
                      " and exists (select 'X' from laagent where agentstate in ('01','02') and a.agentcode=agentcode) "
                      +
                      " and not exists (select 'X' from labranchgroup where a.agentgroup =agentgroup "
                      + "and state='1'" //等于1不计入正常组进行统计.
                      + " ) order by AgentGrade,AgentCode";
        System.out.println("查询" + mManageCom + "机构下所有代理人：" + tSQL);
        tLATreeSet = tLATreeDB.executeQuery(tSQL);
        System.out.println("该管理机构的下的业务员个数：" + tLATreeSet.size());
        for (int a = 1; a <= tLATreeSet.size(); a++)
        {
            String tAgentCode; //代理人代码
            LATreeSchema tLATreeSchema = new LATreeSchema();
            tLATreeSchema = tLATreeSet.get(a);
            tAgentCode = tLATreeSchema.getAgentCode();
            System.out.println("-------开始计算" + tAgentCode + "-------");
            //准备好数据，把这些数据提交给后台，对此业务员进行考核
            if (!prepareOutputDatatoPerBL(tLATreeSchema))
            {
                return false;
            }
            System.out.println("*****开始对" + tAgentCode + "业务员进行考核");
            System.out.println("Start ACGrpGrdAssessPerBL Submit...");
            mACGrpGrdAssessPerBL = new ACGrpGrdAssessPerBL();
            mACGrpGrdAssessPerBL.submitData(this.mPInputData, this.mOperate);
            System.out.println("End ACGrpGrdAssessPerBL Submit...");
            //如果有需要处理的错误，则返回
            if (mACGrpGrdAssessPerBL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mACGrpGrdAssessPerBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "ACGrpGrdAssessPerBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("*****" + tAgentCode + "业务员核结束");
        }

        return true;
    }

    /**
     * prepareOutputDatatoPerBL
     * 准备好数据，把这些数据提交给后台，对此业务员进行考核
     * @param cLATreeSchema LATreeSchema
     * @return boolean
     */
    private boolean prepareOutputDatatoPerBL(LATreeSchema cLATreeSchema)
    {
        String tAgentCode = cLATreeSchema.getAgentCode();
        String tAgentGrade = cLATreeSchema.getAgentGrade();
        mPInputData = new VData();
        mPInputData.add(mGlobalInput);
        mPInputData.add(mIndexCalNo);
        mPInputData.add(tAgentCode);
        mPInputData.add(tAgentGrade);
        mPInputData.add(mBranchType);
        mPInputData.add(mAreaType);
        return true;
    }
}
