/*
 * <p>ClassName: LAEmployeeWageAdjBL </p>
 * <p>Description: LAEmployeeWageAdjBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.db.LAWelfareInfoDB;
import com.sinosoft.lis.db.LAWelfareRadixDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAWelfareInfoBSchema;
import com.sinosoft.lis.schema.LAWelfareInfoSchema;
import com.sinosoft.lis.schema.LAWelfareRadixSchema;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.lis.vschema.LAWelfareInfoBSet;
import com.sinosoft.lis.vschema.LAWelfareInfoSet;
import com.sinosoft.lis.vschema.LAWelfareRadixSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;


public class LAEmployeeWageAdjBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate(); //入机日期
    private String currentTime = PubFun.getCurrentTime(); //入机时间
    /** 业务处理相关变量 */
    private LAWelfareInfoSchema mLAWelfareInfoSchema = new LAWelfareInfoSchema();

    private LAWelfareInfoBSet mLAWelfareInfoBSet = new LAWelfareInfoBSet(); //需要插入到备份表的纪录
    private LAWelfareInfoSet mLAWelfareInfoInsSet = new LAWelfareInfoSet(); //需要插入LAWelfareInfo表里面的纪录
    private LAWelfareInfoSet mLAWelfareInfoDelSet = new LAWelfareInfoSet(); //需要从表里面删除的纪录（如果这条记录需要修改，先备份再删除）

    private String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
    ; //转储号
    private String mAClass = "";
    String[] mSEDate = null;
    public LAEmployeeWageAdjBL()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "aa";
        tGlobalInput.ManageCom = "86110000";

        LAWelfareInfoSchema t1 = new LAWelfareInfoSchema();
        LAWelfareInfoSchema t2 = new LAWelfareInfoSchema();
        LAWelfareInfoSchema t3 = new LAWelfareInfoSchema();

        LAWelfareInfoSet tSet = new LAWelfareInfoSet();
        t1.setIndexCalNo("200403");
        t1.setAgentCode("8611000001");
        t1.setAClass("11");
        t1.setServeYear(6);
        t1.setBranchAttr("861100000401002003");
        t1.setManageCom("86110000");
        t1.setSumMoney(2000);

        VData tInputData = new VData();
        tInputData.add(t1);
        tInputData.add(tGlobalInput);
        LAEmployeeWageAdjBL LAEmployeeWageAdjBL1 = new LAEmployeeWageAdjBL();
        if (!LAEmployeeWageAdjBL1.submitData(tInputData, "INSERT||MAIN"))
        {
            for (int i = 0; i < LAEmployeeWageAdjBL1.mErrors.getErrorCount(); i++)
            {
                System.out.println(LAEmployeeWageAdjBL1.mErrors.getError(i).
                                   errorMessage);
            }
        }
    }

    private boolean check() //校验函数(如果一条记录定义的福利类别有重复，则经校验返回错误)
    {
        //修改：2004-04-13 LL
        //保证本月佣金计算过，下个月佣金没算过，才可进行录入
        //取此人所在管理机构信息 and 展业类型信息
        String tAgentCode = this.mLAWelfareInfoSchema.getAgentCode();
        String tIndexCalNo = this.mLAWelfareInfoSchema.getIndexCalNo();
        String tManageCom = "";
        String tBranchType = "";
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            buildError("check", "查询代理人" + tAgentCode + "管理机构或展业类型信息出错！");
            return false;
        }
        tManageCom = tLAAgentDB.getManageCom();
        tBranchType = tLAAgentDB.getBranchType();
        System.out.println("ManageCom为：" + tManageCom);
        System.out.println("BranchType为：" + tBranchType);

        //校验本月佣金计算过，下个月佣金没算过
        String tSql = "select * from LAWage where IndexCalNo = '" + tIndexCalNo
                      +
                      "' and not exists(select * From LAWage Where IndexCalNo > '"
                      + tIndexCalNo + "' and BranchType = '" + tBranchType
                      + "' and ManageCom = '" + tManageCom + "' )"
                      + " and BranchType = '" + tBranchType + "' and "
                      + " ManageCom = '" + tManageCom + "' ";
        System.out.println("佣金校验SQL：" + tSql);
        LAWageDB tLAWageDB = new LAWageDB();
        LAWageSet tLAWageSet = new LAWageSet();
        tLAWageSet = tLAWageDB.executeQuery(tSql);

        if (tLAWageDB.mErrors.needDealError())
        {
            buildError("check",
                       "代理人" + tAgentCode + "的" + tIndexCalNo + "的本月佣金计算校验出错");
            return false;
        }
        if (tLAWageSet.size() == 0)
        {
            buildError("check",
                       "代理人" + tAgentCode + "的" + tIndexCalNo +
                       "的本月佣金计算过，下月佣金没算过条件不满足");
            return false;
        }
        return true;
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {

            return false;
        }
        //放开条件，不考虑佣金是否计算过
        if (!check())
        {
            return false;
        }
        //检验此人的待遇职级
        if (!checkBranchCode())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAEmployeeWageAdjBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        LAEmployeeWageAdjBLS tLAEmployeeWageAdjBLS = new LAEmployeeWageAdjBLS();
        if (!tLAEmployeeWageAdjBLS.submitData(this.mInputData))
        {
            this.mErrors.copyAllErrors(tLAEmployeeWageAdjBLS.mErrors);
            buildError("submit", "BLS保存出错!");
            return false;
        }
        return true;

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        mAClass = this.mLAWelfareInfoSchema.getAClass();
        String tAgentCode = this.mLAWelfareInfoSchema.getAgentCode();
        /**
         *如果维护的是底薪,则需要自动生成四险一金的记录;如果维护的是社保扶持金则生成社保扶持金
         *
         */
        if ((mAClass.equals("11") || mAClass.equals("12")) &&
            this.mOperate.equals("INSERT||MAIN"))
        {
            String tsql = "select * from LAWelfareInfo where agentcode='" +
                          tAgentCode + "'"; //查询出AgentCode所对应的纪录
            LAWelfareInfoSet tLAWelfareInfoSet = new LAWelfareInfoSet();
            LAWelfareInfoDB tLAWelfareInfoDB = new LAWelfareInfoDB();
            tLAWelfareInfoSet = tLAWelfareInfoDB.executeQuery(tsql);

            if (tLAWelfareInfoDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLAWelfareInfoDB.mErrors);
                buildError("dealData",
                           "查询" + tAgentCode + "在LAWelfareInfo表中是否有记录时出错!");
                return false;
            }
            /**
             * 如果福利表LAWelfareInfo中有此代理人的底薪与四险一金记录,
             * 则需要先备份、后删除这些记录
             */
            if (tLAWelfareInfoSet.size() != 0)
            {
                LAWelfareInfoSchema tLAWelfareInfoSchema = new
                        LAWelfareInfoSchema();
                tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);

                for (int i = 1; i <= tLAWelfareInfoSet.size(); i++)
                {
                    tLAWelfareInfoSchema = tLAWelfareInfoSet.get(i);
                    LAWelfareInfoBSchema tLAWelfareInfoBSchema = new
                            LAWelfareInfoBSchema();
                    Reflections tReflections = new Reflections();
                    //将LAWelfareInfo表中的记录备份到LAWelfareInfoB中
                    tReflections.transFields(tLAWelfareInfoBSchema,
                                             tLAWelfareInfoSchema);
                    tLAWelfareInfoBSchema.setEdorNo(tEdorNo);
                    tLAWelfareInfoBSchema.setEdorType("02");
                    tLAWelfareInfoBSchema.setOperator(this.mGlobalInput.
                            Operator);
                    tLAWelfareInfoBSchema.setMakeDate(currentDate);
                    tLAWelfareInfoBSchema.setMakeTime(currentTime);
                    tLAWelfareInfoBSchema.setModifyDate(currentDate);
                    tLAWelfareInfoBSchema.setModifyTime(currentTime);
                    this.mLAWelfareInfoBSet.add(tLAWelfareInfoBSchema);
                    this.mLAWelfareInfoDelSet.add(tLAWelfareInfoSchema);
                }
            }
            if (mAClass.equals("11") && this.genWelfareSet(mLAWelfareInfoSchema) == null)
            {
                return false;
            }
            else
            {
                LATreeDB tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(mLAWelfareInfoSchema.getAgentCode());
                if (!tLATreeDB.getInfo())
                {
                    ////
                    return false;
                }
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(mLAWelfareInfoSchema.getAgentCode());
                if (!tLAAgentDB.getInfo())
                {
                    ////
                    return false;
                }
                mLAWelfareInfoSchema.setManageCom(tLATreeDB.getManageCom());
                mLAWelfareInfoSchema.setBranchType(tLAAgentDB.getBranchType());
                if (mSEDate == null)
                {
                    mSEDate = this.getFEDate(mLAWelfareInfoSchema.getIndexCalNo(),
                                             mLAWelfareInfoSchema.getServeYear());
                }
                mLAWelfareInfoSchema.setWSendDate(mSEDate[0]);
                mLAWelfareInfoSchema.setWSendEndDate(mSEDate[1]);
                mLAWelfareInfoSchema.setWValidDate(mSEDate[0]);
                mLAWelfareInfoSchema.setWValidEndDate(mSEDate[1]);
                mLAWelfareInfoSchema.setDoneFlag("1");
                mLAWelfareInfoSchema.setBranchAttr(mLAWelfareInfoSchema.
                        getBranchAttr());
                mLAWelfareInfoSchema.setAgentGrade(tLATreeDB.getAgentGrade());
                mLAWelfareInfoSchema.setModifyDate(currentDate);
                mLAWelfareInfoSchema.setModifyTime(currentTime);
                mLAWelfareInfoSchema.setMakeDate(currentDate);
                mLAWelfareInfoSchema.setMakeTime(currentTime);
                mLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);
                mLAWelfareInfoSchema.setWelfareType("01");
                mLAWelfareInfoSchema.setNoti("1");
                this.mLAWelfareInfoInsSet.add(mLAWelfareInfoSchema);
            }
        }
        else if (this.mOperate.equals("UPDATE||MAIN"))
        {
            String tAClass = mLAWelfareInfoSchema.getAClass();
            String tsql = "select * from LAWelfareInfo where agentcode='" +
                          tAgentCode + "' and aclass= '" + tAClass + "'";
            LAWelfareInfoSet tLAWelfareInfoSet = new LAWelfareInfoSet();
            LAWelfareInfoDB tLAWelfareInfoDB = new LAWelfareInfoDB();
            tLAWelfareInfoSet = tLAWelfareInfoDB.executeQuery(tsql);
            if (tLAWelfareInfoDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLAWelfareInfoDB.mErrors);
                buildError("dealData",
                           "查询" + tAgentCode + "在LAWelfareInfo表的记录时出错!");
                return false;
            }
//      tsql="select * from lawelfareinfo where agentcode='"+tAgentCode+"' and ";
            LAWelfareInfoSchema tLAWelfareInfoSchema = new LAWelfareInfoSchema();
            for (int i = 1; i <= tLAWelfareInfoSet.size(); i++)
            {
                tLAWelfareInfoSchema = tLAWelfareInfoSet.get(i);

                LAWelfareInfoBSchema tLAWelfareInfoBSchema = new
                        LAWelfareInfoBSchema();
                Reflections tReflections = new Reflections();
                //将LAWelfareInfo表中的记录备份到LAWelfareInfoB中
                tReflections.transFields(tLAWelfareInfoBSchema,
                                         tLAWelfareInfoSchema);
                tLAWelfareInfoBSchema.setEdorNo(tEdorNo);
                tLAWelfareInfoBSchema.setEdorType("02");
                tLAWelfareInfoBSchema.setOperator(this.mGlobalInput.Operator);
                tLAWelfareInfoBSchema.setMakeDate(currentDate);
                tLAWelfareInfoBSchema.setMakeTime(currentTime);
                tLAWelfareInfoBSchema.setModifyDate(currentDate);
                tLAWelfareInfoBSchema.setModifyTime(currentTime);
                this.mLAWelfareInfoBSet.add(tLAWelfareInfoBSchema);
                this.mLAWelfareInfoDelSet.add(tLAWelfareInfoSchema);
            }

            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(mLAWelfareInfoSchema.getAgentCode());
            if (!tLATreeDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeWageAdjBL";
                tError.functionName = "genWelfareSet";
                tError.errorMessage = "查询" + mLAWelfareInfoSchema.getAgentCode() +
                                      "的行政信息出错";
                this.mErrors.addOneError(tError);
                return false;
            }
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(mLAWelfareInfoSchema.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeWageAdjBL";
                tError.functionName = "genWelfareSet";
                tError.errorMessage = "查询" + mLAWelfareInfoSchema.getAgentCode() +
                                      "的基本信息出错";
                this.mErrors.addOneError(tError);
                return false;
            }

            mLAWelfareInfoSchema.setAgentGrade(tLATreeDB.getAgentGrade());
            mLAWelfareInfoSchema.setBranchType(tLAAgentDB.getBranchType());
            if (mSEDate == null)
            {
                mSEDate = this.getFEDate(mLAWelfareInfoSchema.getIndexCalNo(),
                                         mLAWelfareInfoSchema.getServeYear());
            }
            mLAWelfareInfoSchema.setWSendDate(mSEDate[0]);
            mLAWelfareInfoSchema.setWSendEndDate(mSEDate[1]);
            mLAWelfareInfoSchema.setWValidDate(mSEDate[0]);
            mLAWelfareInfoSchema.setWValidEndDate(mSEDate[1]);
            mLAWelfareInfoSchema.setNoti("1");
            if (tAClass.equals("12")) //只有社保扶持金的人不存在公司提供的四险一金
            {
                mLAWelfareInfoSchema.setWelfareType("01");
            }
            else
            {
                mLAWelfareInfoSchema.setWelfareType("02");
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
//      mLAWelfareInfoSchema.setSumMoney(mLAWelfareInfoSchema.getGrpMoney() +mLAWelfareInfoSchema.getPerMoney() ) ;
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            mLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);
            mLAWelfareInfoSchema.setModifyDate(currentDate);
            mLAWelfareInfoSchema.setModifyTime(currentTime);
            mLAWelfareInfoSchema.setMakeDate(currentDate);
            mLAWelfareInfoSchema.setMakeTime(currentTime);
            mLAWelfareInfoSchema.setManageCom(tLATreeDB.getManageCom());
            mLAWelfareInfoSchema.setBranchType(tLAAgentDB.getBranchType());
            mLAWelfareInfoSchema.setDoneFlag("1");
            mLAWelfareInfoSchema.setAgentGrade(tLATreeDB.getAgentGrade());
            mLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);
            mLAWelfareInfoSchema.setWelfareType("01");
            mLAWelfareInfoSchema.setNoti("1");

            this.mLAWelfareInfoInsSet.add(mLAWelfareInfoSchema);
        }
        return true;
    }

    /**
     * 根据tIndexCalNo和tServeYear,计算出享受待遇的起止日期
     * @param tIndexCalNo 生效年月
     * @param tServeYear 服务期限,以月为单位
     * @return String[0]起始日期
     *         String[1]截至日期
     */
    private String[] getFEDate(String tIndexCalNo, double tServeYear)
    {
        String strBuf = String.valueOf(tServeYear);
        System.out.println(strBuf);
        strBuf = strBuf.substring(0, strBuf.indexOf(".")); //取小数点以前的整数
        int interval = Integer.parseInt(strBuf) - 1; //将double型转化为整型

        String dateBuf = tIndexCalNo.substring(0, 4) + "-" +
                         tIndexCalNo.substring(4, 6) + "-20";
        String[] tResult = new String[2];

        String cIndexCalNo = "";
        dateBuf = PubFun.calDate(dateBuf, 1, "M", null);
        cIndexCalNo = AgentPubFun.formatDate(dateBuf, "yyyyMM");

        String dateBuf1 = AgentPubFun.formatDate(PubFun.calDate(dateBuf,
                interval, "M", null), "yyyyMM"); //格式化
        String tSQL =
                "select startDate from lastatsegment where stattype='1' and yearmonth='" +
                cIndexCalNo + "'"; //取起效年月的第一天
        ExeSQL tExeSQL = new ExeSQL();
        tResult[0] = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            buildError("getFEDate", "查询" + tIndexCalNo + "的自然月起期出错！");
            return null;
        }
        tSQL =
                "select endDate from lastatsegment where stattype='1' and yearmonth='" +
                dateBuf1 + "'"; //取服务截止月的最后一天
        tExeSQL = new ExeSQL();
        tResult[1] = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            buildError("getFEDate", "查询" + dateBuf1 + "的自然月止期出错！");
            return null;
        }
        return tResult;
    }

    private void buildError(String FuncName, String ErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAEmployeeWageAdjBL";
        cError.functionName = FuncName;
        cError.errorMessage = ErrMsg;
        this.mErrors.addOneError(cError);
    }

    private LAWelfareInfoSet genWelfareSet(LAWelfareInfoSchema
                                           cLAWelfareInfoSchema)
    {
        String tManageCom = cLAWelfareInfoSchema.getManageCom();
        String tWelfareGrade = "";
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(cLAWelfareInfoSchema.getAgentCode());
        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBL";
            tError.functionName = "genWelfareSet";
            tError.errorMessage = "查询" + cLAWelfareInfoSchema.getAgentCode() +
                                  "的行政信息出错";
            this.mErrors.addOneError(tError);
            return null;
        }
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(cLAWelfareInfoSchema.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBL";
            tError.functionName = "genWelfareSet";
            tError.errorMessage = "查询" + cLAWelfareInfoSchema.getAgentCode() +
                                  "的基本信息出错";
            this.mErrors.addOneError(tError);
            return null;
        }
        String documentIn = tLAAgentDB.getQualiPassFlag(); //档案是否调入对应比例比例不用
        double rate = 1;
        double supportMoney = 0;
        if (documentIn != null && documentIn.equals("1"))
        {
            rate = 1;
        }
        else
        {
            rate = 0.5;
        }
        tWelfareGrade = tLATreeDB.getBranchCode();
        if (tWelfareGrade != null)
        {
            System.out.println("YES");
        }
        if (tWelfareGrade == null || tWelfareGrade.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBL";
            tError.functionName = "genWelfareSet";
            tError.errorMessage = "" + tLATreeDB.getAgentCode() + "的待遇级别未维护";
            this.mErrors.addOneError(tError);
            return null;
        }
        String tsql = "select * from lawelfareradix where agentgrade='" +
                      tWelfareGrade + "'and managecom='" +
                      tManageCom.substring(0, 6) + "' and aclass='11' ";
        System.out.println(tsql);
        LAWelfareRadixDB tLAWelfareRadixDB = new LAWelfareRadixDB();
        LAWelfareRadixSet tLAWelfareRadixSet = new LAWelfareRadixSet();
        tLAWelfareRadixSet = tLAWelfareRadixDB.executeQuery(tsql);
        if (tLAWelfareRadixDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAWelfareRadixDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBL";
            tError.functionName = "genWelfareSet";
            tError.errorMessage = "查询地区类型为" + tManageCom.substring(0, 6) +
                                  "级别为" + tWelfareGrade + "的底薪出错";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (tLAWelfareRadixSet.size() == 0)
        {
            this.mErrors.copyAllErrors(tLAWelfareRadixDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBL";
            tError.functionName = "genWelfareSet";
            tError.errorMessage = "" + tLATreeDB.getAgentCode() + "的员工制级别为" +
                                  tWelfareGrade + ",不能享受员工底薪!";
            this.mErrors.addOneError(tError);
            return null;
        }
        double provinceAveWage = tLAWelfareRadixSet.get(1).getDrawBase();
        tsql = "select * from lawelfareradix where agentgrade='" +
               tWelfareGrade + "'and managecom='" + tManageCom.substring(0, 6) +
               "'  ";
        System.out.println(tsql);
        tLAWelfareRadixDB = new LAWelfareRadixDB();
        tLAWelfareRadixSet = new LAWelfareRadixSet();
        tLAWelfareRadixSet = tLAWelfareRadixDB.executeQuery(tsql);
        if (tLAWelfareRadixDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAWelfareRadixDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBL";
            tError.functionName = "genWelfareSet";
            tError.errorMessage = "查询地区类型为" + tManageCom.substring(0, 6) +
                                  "级别为" + tWelfareGrade + "的福利比例出错";
            this.mErrors.addOneError(tError);
            return null;
        }
        else
        {
            LAWelfareRadixSchema tLAWelfareRadixSchema = new
                    LAWelfareRadixSchema();
            for (int i = 1; i <= tLAWelfareRadixSet.size(); i++)
            {
                tLAWelfareRadixSchema = tLAWelfareRadixSet.get(i);
                if (tLAWelfareRadixSchema.getAClass().equals("14"))
                {
                    System.out.println(tLAWelfareRadixSchema.getAClass());
                    LAWelfareInfoSchema tLAWelfareInfoSchema = new
                            LAWelfareInfoSchema();
                    tLAWelfareInfoSchema.setAgentCode(this.mLAWelfareInfoSchema.
                            getAgentCode());
                    tLAWelfareInfoSchema.setAgentGrade(tLATreeDB.getAgentGrade());
                    tLAWelfareInfoSchema.setDoneFlag("1");
                    tLAWelfareInfoSchema.setManageCom(tLATreeDB.getManageCom());
                    tLAWelfareInfoSchema.setBranchType(tLAAgentDB.getBranchType());
                    tLAWelfareInfoSchema.setBranchAttr(mLAWelfareInfoSchema.
                            getBranchAttr());
                    tLAWelfareInfoSchema.setAClass(tLAWelfareRadixSchema.
                            getAClass());

                    tLAWelfareInfoSchema.setSumMoney(tLAWelfareRadixSchema.
                            getDrawMoney());
                    tLAWelfareInfoSchema.setIndexCalNo(mLAWelfareInfoSchema.
                            getIndexCalNo());
                    tLAWelfareInfoSchema.setServeYear(mLAWelfareInfoSchema.
                            getServeYear());
                    if (mSEDate == null)
                    {
                        mSEDate = this.getFEDate(mLAWelfareInfoSchema.
                                                 getIndexCalNo(),
                                                 mLAWelfareInfoSchema.
                                                 getServeYear());
                    }
                    tLAWelfareInfoSchema.setWSendDate(mSEDate[0]);
                    tLAWelfareInfoSchema.setWSendEndDate(mSEDate[1]);
                    tLAWelfareInfoSchema.setWValidDate(mSEDate[0]);
                    tLAWelfareInfoSchema.setWValidEndDate(mSEDate[1]);
                    tLAWelfareInfoSchema.setNoti("1");
                    tLAWelfareInfoSchema.setWelfareType("01");
                    tLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);
                    tLAWelfareInfoSchema.setModifyDate(currentDate);
                    tLAWelfareInfoSchema.setModifyTime(currentTime);
                    tLAWelfareInfoSchema.setMakeDate(currentDate);
                    tLAWelfareInfoSchema.setMakeTime(currentTime);
                    this.mLAWelfareInfoInsSet.add(tLAWelfareInfoSchema);
                }
                //中介费：只有纯员工（即Y03以上且档案调入人员才享有）
                else if (tLAWelfareRadixSchema.getAClass().equals("15") &&
                         rate == 1)
                {
                    System.out.println("中介费：" + tLAWelfareRadixSchema.getAClass());
                    LAWelfareInfoSchema tLAWelfareInfoSchema = new
                            LAWelfareInfoSchema();
                    tLAWelfareInfoSchema.setAgentCode(this.mLAWelfareInfoSchema.
                            getAgentCode());
                    tLAWelfareInfoSchema.setAgentGrade(tLATreeDB.getAgentGrade());
                    tLAWelfareInfoSchema.setDoneFlag("1");
                    tLAWelfareInfoSchema.setManageCom(tLATreeDB.getManageCom());
                    tLAWelfareInfoSchema.setBranchType(tLAAgentDB.getBranchType());
                    tLAWelfareInfoSchema.setBranchAttr(mLAWelfareInfoSchema.
                            getBranchAttr());
                    tLAWelfareInfoSchema.setAClass(tLAWelfareRadixSchema.
                            getAClass());

                    tLAWelfareInfoSchema.setSumMoney(tLAWelfareRadixSchema.
                            getDrawMoney());
                    tLAWelfareInfoSchema.setIndexCalNo(mLAWelfareInfoSchema.
                            getIndexCalNo());
                    tLAWelfareInfoSchema.setServeYear(mLAWelfareInfoSchema.
                            getServeYear());
                    if (mSEDate == null)
                    {
                        mSEDate = this.getFEDate(mLAWelfareInfoSchema.
                                                 getIndexCalNo(),
                                                 mLAWelfareInfoSchema.
                                                 getServeYear());
                    }
                    tLAWelfareInfoSchema.setWSendDate(mSEDate[0]);
                    tLAWelfareInfoSchema.setWSendEndDate(mSEDate[1]);
                    tLAWelfareInfoSchema.setWValidDate(mSEDate[0]);
                    tLAWelfareInfoSchema.setWValidEndDate(mSEDate[1]);
                    tLAWelfareInfoSchema.setNoti("1");
                    tLAWelfareInfoSchema.setWelfareType("01");
                    tLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);
                    tLAWelfareInfoSchema.setModifyDate(currentDate);
                    tLAWelfareInfoSchema.setModifyTime(currentTime);
                    tLAWelfareInfoSchema.setMakeDate(currentDate);
                    tLAWelfareInfoSchema.setMakeTime(currentTime);
                    this.mLAWelfareInfoInsSet.add(tLAWelfareInfoSchema);
                }
                else if (!tLAWelfareRadixSchema.getAClass().equals("11"))
                {
                    System.out.println(tLAWelfareRadixSchema.getAClass());
                    LAWelfareInfoSchema tLAWelfareInfoSchema = new
                            LAWelfareInfoSchema();
                    tLAWelfareInfoSchema.setAgentCode(this.mLAWelfareInfoSchema.
                            getAgentCode());
                    tLAWelfareInfoSchema.setAgentGrade(tLATreeDB.getAgentGrade());
                    tLAWelfareInfoSchema.setDoneFlag("1");
                    tLAWelfareInfoSchema.setManageCom(tLATreeDB.getManageCom());
                    tLAWelfareInfoSchema.setBranchType(tLAAgentDB.getBranchType());
                    tLAWelfareInfoSchema.setBranchAttr(mLAWelfareInfoSchema.
                            getBranchAttr());
                    tLAWelfareInfoSchema.setAClass(tLAWelfareRadixSchema.
                            getAClass());
                    double tBaseMoney = mLAWelfareInfoSchema.getSumMoney();

                    if (tLAWelfareRadixSchema.getDrawBase2() > 0 &&
                        tBaseMoney >
                        provinceAveWage * tLAWelfareRadixSchema.getDrawBase2())
                    {
                        tBaseMoney = provinceAveWage *
                                     tLAWelfareRadixSchema.getDrawBase2();
                    }
                    else
                    if (tLAWelfareRadixSchema.getDrawBase1() > 0 &&
                        tBaseMoney < provinceAveWage *
                        tLAWelfareRadixSchema.getDrawBase1())
                    {
                        tBaseMoney = provinceAveWage *
                                     tLAWelfareRadixSchema.getDrawBase1();
                    }
                    System.out.println("RATE:" + rate);
                    tLAWelfareInfoSchema.setGrpMoney(tBaseMoney *
                            tLAWelfareRadixSchema.getOthDrawRate() * rate);
                    tLAWelfareInfoSchema.setPerMoney(tBaseMoney *
                            tLAWelfareRadixSchema.getDrawRate() * rate);
                    tLAWelfareInfoSchema.setSumMoney(tLAWelfareInfoSchema.
                            getGrpMoney() + tLAWelfareInfoSchema.getPerMoney());
                    tLAWelfareInfoSchema.setIndexCalNo(mLAWelfareInfoSchema.
                            getIndexCalNo());
                    tLAWelfareInfoSchema.setServeYear(mLAWelfareInfoSchema.
                            getServeYear());
                    if (mSEDate == null)
                    {
                        mSEDate = this.getFEDate(mLAWelfareInfoSchema.
                                                 getIndexCalNo(),
                                                 mLAWelfareInfoSchema.
                                                 getServeYear());
                    }
                    tLAWelfareInfoSchema.setWSendDate(mSEDate[0]);
                    tLAWelfareInfoSchema.setWSendEndDate(mSEDate[1]);
                    tLAWelfareInfoSchema.setWValidDate(mSEDate[0]);
                    tLAWelfareInfoSchema.setWValidEndDate(mSEDate[1]);
                    tLAWelfareInfoSchema.setNoti("1");
                    tLAWelfareInfoSchema.setWelfareType("02");
                    tLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);
                    tLAWelfareInfoSchema.setModifyDate(currentDate);
                    tLAWelfareInfoSchema.setModifyTime(currentTime);
                    tLAWelfareInfoSchema.setMakeDate(currentDate);
                    tLAWelfareInfoSchema.setMakeTime(currentTime);
                    if (rate == 1)
                    {
                        this.mLAWelfareInfoInsSet.add(tLAWelfareInfoSchema);
                    }
                    else
                    {
                        supportMoney = supportMoney +
                                       tLAWelfareInfoSchema.getGrpMoney();
                    }
                }
            }
            if (rate != 1)
            {
                LAWelfareInfoSchema tLAWelfareInfoSchema = new
                        LAWelfareInfoSchema();
                tLAWelfareInfoSchema.setAgentCode(this.mLAWelfareInfoSchema.
                                                  getAgentCode());
                tLAWelfareInfoSchema.setAgentGrade(tLATreeDB.getAgentGrade());
                tLAWelfareInfoSchema.setDoneFlag("1");
                tLAWelfareInfoSchema.setManageCom(tLATreeDB.getManageCom());
                tLAWelfareInfoSchema.setBranchType(tLAAgentDB.getBranchType());
                tLAWelfareInfoSchema.setBranchAttr(mLAWelfareInfoSchema.
                        getBranchAttr());
                tLAWelfareInfoSchema.setAClass("13");
                tLAWelfareInfoSchema.setSumMoney(supportMoney);
                tLAWelfareInfoSchema.setIndexCalNo(mLAWelfareInfoSchema.
                        getIndexCalNo());
                tLAWelfareInfoSchema.setServeYear(mLAWelfareInfoSchema.
                                                  getServeYear());
                if (mSEDate == null)
                {
                    mSEDate = this.getFEDate(mLAWelfareInfoSchema.getIndexCalNo(),
                                             mLAWelfareInfoSchema.getServeYear());
                }
                tLAWelfareInfoSchema.setWSendDate(mSEDate[0]);
                tLAWelfareInfoSchema.setWSendEndDate(mSEDate[1]);
                tLAWelfareInfoSchema.setWValidDate(mSEDate[0]);
                tLAWelfareInfoSchema.setWValidEndDate(mSEDate[1]);
                tLAWelfareInfoSchema.setNoti("1");
                tLAWelfareInfoSchema.setWelfareType("01");
                tLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);
                tLAWelfareInfoSchema.setModifyDate(currentDate);
                tLAWelfareInfoSchema.setModifyTime(currentTime);
                tLAWelfareInfoSchema.setMakeDate(currentDate);
                tLAWelfareInfoSchema.setMakeTime(currentTime);
                this.mLAWelfareInfoInsSet.add(tLAWelfareInfoSchema);
            }
            cLAWelfareInfoSchema.setManageCom(tLATreeDB.getManageCom());
            cLAWelfareInfoSchema.setBranchType(tLAAgentDB.getBranchType());
            if (mSEDate == null)
            {
                mSEDate = this.getFEDate(cLAWelfareInfoSchema.getIndexCalNo(),
                                         cLAWelfareInfoSchema.getServeYear());
            }
            cLAWelfareInfoSchema.setWSendDate(mSEDate[0]);
            cLAWelfareInfoSchema.setWSendEndDate(mSEDate[1]);
            cLAWelfareInfoSchema.setWValidDate(mSEDate[0]);
            cLAWelfareInfoSchema.setWValidEndDate(mSEDate[1]);
            cLAWelfareInfoSchema.setDoneFlag("1");
            cLAWelfareInfoSchema.setBranchAttr(mLAWelfareInfoSchema.
                                               getBranchAttr());
            cLAWelfareInfoSchema.setAgentGrade(tLATreeDB.getAgentGrade());
            cLAWelfareInfoSchema.setModifyDate(currentDate);
            cLAWelfareInfoSchema.setModifyTime(currentTime);
            cLAWelfareInfoSchema.setMakeDate(currentDate);
            cLAWelfareInfoSchema.setMakeTime(currentTime);
            cLAWelfareInfoSchema.setOperator(this.mGlobalInput.Operator);
            cLAWelfareInfoSchema.setWelfareType("01");
            cLAWelfareInfoSchema.setNoti("1");
            this.mLAWelfareInfoInsSet.add(cLAWelfareInfoSchema);
        }
        return mLAWelfareInfoInsSet;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAWelfareInfoSchema = (LAWelfareInfoSchema) cInputData.
                                    getObjectByObjectName("LAWelfareInfoSchema",
                0);
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.addElement(this.mLAWelfareInfoBSet);
            this.mInputData.addElement(this.mLAWelfareInfoInsSet);
            this.mInputData.addElement(this.mLAWelfareInfoDelSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /*
     * 功能：校验此人是否满足生成条件
     */
    private boolean checkBranchCode()
    {
        //判断此人的待遇职级如果为Y00或null的话则不能生成福利待遇
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(this.mLAWelfareInfoSchema.getAgentCode());
        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeGenWelfareAdjBL";
            tError.functionName = "checkBranchCode";
            tError.errorMessage = "查询" + this.mLAWelfareInfoSchema.getAgentCode() +
                                  "的行政信息出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tBranchCode = tLATreeDB.getBranchCode();
        if (tBranchCode == null || tBranchCode.equals("") ||
            tBranchCode.equals("Y00"))
        {
            CError tError = new CError();
            tError.moduleName = "LAEmployeeGenWelfareAdjBL";
            tError.functionName = "checkBranchCode";
            tError.errorMessage = "代理人" +
                                  this.mLAWelfareInfoSchema.getAgentCode() +
                                  "的待遇级别不符合要求";
            this.mErrors.addOneError(tError);
            return false;
        }

        //根据前台传过来AClass做相关判断，
        //Y01,Y02 只能享受社保扶持金
        //Y03以上只能享受四险一金
        LAWelfareRadixSet tLAWelfareRadixSet = new LAWelfareRadixSet();
        LAWelfareRadixDB tLAWelfareRadixDB = new LAWelfareRadixDB();
        String tSQL = "select * from LAWelfareRadix where ManageCom = '"
                      + this.mLAWelfareInfoSchema.getManageCom().substring(0, 6)
                      + "' and AgentGrade = '" + tBranchCode
                      + "' and AClass = '" +
                      this.mLAWelfareInfoSchema.getAClass() + "'";
        System.out.println("校验福利待遇查询LAWelfareRadix表：" + tSQL);
        tLAWelfareRadixSet = tLAWelfareRadixDB.executeQuery(tSQL);

        if (tLAWelfareRadixDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAWelfareRadixDB.mErrors);
            buildError("checkBranchCode", "查询LAWelfareRadix表时出错!");
            return false;
        }
        //如果此职级没有对应的ACLAss，则报错
        if (tLAWelfareRadixSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "LAEmployeeGenWelfareAdjBL";
            tError.functionName = "checkBranchCode";
            tError.errorMessage = "代理人" +
                                  this.mLAWelfareInfoSchema.getAgentCode() +
                                  "的待遇级别" + tBranchCode + "不符合要求";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

}
