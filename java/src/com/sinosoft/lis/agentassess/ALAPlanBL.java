/*
 * <p>ClassName: ALAPlanBL </p>
 * <p>Description: LAPlanBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-06-21
 */
package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.LAPlanDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.schema.LAPlanSchema;
import com.sinosoft.lis.vschema.LAPlanSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import java.math.BigInteger;
import java.math.BigDecimal;

public class ALAPlanBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String  mPlanPeriod;
    /** 业务处理相关变量 */
    private LAPlanSchema mLAPlanSchema = new LAPlanSchema();
    private LAPlanSet mLAPlanSet = new LAPlanSet();
    public ALAPlanBL()
    {
    }

    public static void main(String[] args)
    {
        //tLAPlanSchema.setPlanCode(request.getParameter("PlanCode"));




    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAPlanBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start ALAPlanBL Submit...");
            ALAPlanBLS tLAPlanBLS = new ALAPlanBLS();
            tLAPlanBLS.submitData(mInputData, cOperate);
            System.out.println("End ALAPlanBL Submit...");
            //如果有需要处理的错误，则返回
            if (tLAPlanBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAPlanBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAPlanBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        String tPlanCode = "";

        double tTransStandMoney=0 ;
//        if(mLAPlanSchema.getPlanCond3().equals("02"))
//        {
//          String Sql_rate="select codealias from ldcode where codetype='larisktype' and code='"+mLAPlanSchema.getPlanCond1()+"'";
//          ExeSQL aExeSQL = new ExeSQL();
//          String tcodealias = aExeSQL.getOneValue(Sql_rate);
//          double tRate=Double.parseDouble(tcodealias);
//          tTransStandMoney= mulrate(mLAPlanSchema.getPlanValue(),tRate);
//        }
        this.mLAPlanSchema.setTransStandMoney(tTransStandMoney);

        if (this.mOperate.equals("INSERT||MAIN"))
        {
         //校验该计划该对象该时间段的是否已存在
         if (!checkRecord())
         {
             return false;
          }


            //校验计划起期与计划止期是否与计划时间单位相符 add by liuyy
          if(!checkTimePeriod())
          {
              return false;
          }


          //校验计划起期与计划止期是否与计划时间单位相符 add by liuyy
//         if(!checkPrem())
//         {
//             return false;
//         }


          //得到最大编号+1
            ExeSQL tExeSQL = new ExeSQL();
            String tSQL = "select trim(max(PlanCode)) from LAPlan";
            tPlanCode = tExeSQL.getOneValue(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAPlanBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询最大计划号失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tPlanCode == null || tPlanCode.equals(""))
            {
                tPlanCode = "P0000001";
            }
            else
            {
                int maxcode = Integer.parseInt("1" + tPlanCode.substring(1));
                maxcode = maxcode + 1;
                tPlanCode = "p" + String.valueOf(maxcode).substring(1);
            }
            System.out.println("PlanCode:" + tPlanCode);
            this.mLAPlanSchema.setPlanCode(tPlanCode);
            this.mLAPlanSchema.setOperator(this.mGlobalInput.Operator);
            this.mLAPlanSchema.setMakeDate(currentDate);
            this.mLAPlanSchema.setMakeTime(currentTime);
            this.mLAPlanSchema.setPlanPeriod(mPlanPeriod);
            this.mLAPlanSchema.setModifyDate(currentDate);
            this.mLAPlanSchema.setModifyTime(currentTime);
        }
        else if (this.mOperate.equals("UPDATE||MAIN"))
        {

        //校验计划起期与计划止期是否与计划时间单位相符 add by liuyy
         if(!checkTimePeriod())
         {
             return false;
         }

            tPlanCode = this.mLAPlanSchema.getPlanCode();
            LAPlanDB tLAPlanDB = new LAPlanDB();
            tLAPlanDB.setPlanCode(tPlanCode);
            if (!tLAPlanDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAPlanDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAPlanBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询原计划任务记录失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
//       tLAPlanDB.setPlanType(this.mLAPlanSchema.getPlanType());
//       tLAPlanDB.setBranchType(this.mLAPlanSchema.getBranchType());
//       tLAPlanDB.setPlanCond1(this.mLAPlanSchema.getPlanCond1());
//       tLAPlanDB.setPlanPeriod(this.mLAPlanSchema.getPlanPeriod());
//       tLAPlanDB.setPlanPeriodUnit(this.mLAPlanSchema.getPlanPeriodUnit());
//       tLAPlanDB.setPlanValue(this.mLAPlanSchema.getPlanValue());
//       tLAPlanDB.setPlanObject(this.mLAPlanSchema.getPlanObject());
//       this.mLAPlanSchema.setSchema(tLAPlanDB.getSchema());
            this.mLAPlanSchema.setOperator(this.mGlobalInput.Operator);
            this.mLAPlanSchema.setMakeDate(tLAPlanDB.getMakeDate());
            this.mLAPlanSchema.setMakeTime(tLAPlanDB.getMakeTime());
            this.mLAPlanSchema.setPlanPeriod(mPlanPeriod);
            this.mLAPlanSchema.setModifyDate(currentDate);
            this.mLAPlanSchema.setModifyTime(currentTime);
        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAPlanSchema.setSchema((LAPlanSchema) cInputData.
                                     getObjectByObjectName("LAPlanSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LAPlanBLQuery Submit...");
        LAPlanDB tLAPlanDB = new LAPlanDB();
        tLAPlanDB.setSchema(this.mLAPlanSchema);
        this.mLAPlanSet = tLAPlanDB.query();
        this.mResult.add(this.mLAPlanSet);
        System.out.println("End LAPlanBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAPlanDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAPlanDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAPlanSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlanBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
    /*
     *校验计划起期与计划止期是否与计划时间单位相符 add by liuyy
     */
   private boolean checkTimePeriod()
   {

    int type=mLAPlanSchema.getPlanPeriodUnit();
    String tPlanStartDate=mLAPlanSchema.getPlanStartDate();
    String tPlanEndDate=mLAPlanSchema.getPlanEndDate();
    String tFieldValue="";
    switch (type)
    {
       case 1:
           tFieldValue="1";  //月计划
           break;
       case 3:
            tFieldValue="2";  //季计划
            break;
       case 6:
            tFieldValue="3";  //半年计划
            break;
       case 12:
            tFieldValue="4";  //年计划
            break;
            default :tFieldValue="";
    }
    if(!tFieldValue.equals("4"))
    {
        String strSQL = "select count(*) from LAStatSegment where Stattype='" +
                        tFieldValue + "'"
                        + " and StartDate='" + tPlanStartDate +
                        "' and EndDate='" + tPlanEndDate + "'";
        ExeSQL nExeSQL = new ExeSQL();
        String i = nExeSQL.getOneValue(strSQL);
        if (i == null || i.equals("") || i.equals("0")) {
            CError tError = new CError();
            tError.moduleName = "ALAPlanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "计划起期与计划止期之间的时间间隔与计划期跨度不符";
            this.mErrors.addOneError(tError);
            return false;
        }
        strSQL = "select yearmonth from LAStatSegment where Stattype='" +
                 tFieldValue + "'"
                 + " and StartDate='" + tPlanStartDate + "' and EndDate='" +
                 tPlanEndDate + "'";
        nExeSQL = new ExeSQL();
        mPlanPeriod = nExeSQL.getOneValue(strSQL);
    }
    else
    {//年计划
        if(tPlanStartDate.compareTo(tPlanEndDate)>0)
        {
            CError tError = new CError();
            tError.moduleName = "ALAPlanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "计划起期 不能大于计划止期！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tDay=AgentPubFun.formatDate(tPlanStartDate,"dd");
        if(!"01".equals(tDay))
        {
            CError tError = new CError();
            tError.moduleName = "ALAPlanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "计划起期 必须是每月的1日！";
            this.mErrors.addOneError(tError);
            return false;
        }

        String tMonth=AgentPubFun.formatDate(tPlanEndDate,"yyyyMM");
        tMonth=tMonth.substring(0,4)+"12";
        String strSQL = "select enddate from LAStatSegment where Stattype='1'"
                 + " and yearmonth="+tMonth+" ";
        ExeSQL tExeSQL = new ExeSQL();
        String tEndDate = tExeSQL.getOneValue(strSQL);
        if(!tEndDate.equals(tPlanEndDate))
        {
            CError tError = new CError();
            tError.moduleName = "ALAPlanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "计划止期必须是每年12日31号！";
            this.mErrors.addOneError(tError);
            return false;
        }
       mPlanPeriod=AgentPubFun.formatDate(tPlanEndDate,"yyyy");
       String tStartYear=AgentPubFun.formatDate(tPlanStartDate,"yyyy");
       if(!tStartYear.equals(mPlanPeriod))
       {
           CError tError = new CError();
           tError.moduleName = "ALAPlanBL";
           tError.functionName = "dealData";
           tError.errorMessage = "计划起期和计划止期必须是同一年！";
           this.mErrors.addOneError(tError);
            return false;
       }
       mPlanPeriod=mPlanPeriod.substring(0,4);
     }
    return true;
   }

   /*
       *校验保费 add by
       */


   public static double mulrate(double v1,double v2)
   {

      BigDecimal b1 = new BigDecimal(Double.toString(v1));

      BigDecimal b2 = new BigDecimal(Double.toString(v2));

      return b1.multiply(b2).doubleValue();

    }

    /*
     *校验该计划该对象该年的是否已存在
     */
    private boolean checkRecord()
    {
        //校验该计划该对象该时间段的计划是否已存在
        LAPlanDB tLAPlanDB = new LAPlanDB();
        tLAPlanDB.setPlanObject(this.mLAPlanSchema.getPlanObject());
        tLAPlanDB.setPlanPeriodUnit(mLAPlanSchema.getPlanPeriodUnit());
        tLAPlanDB.setPlanStartDate(this.mLAPlanSchema.getPlanStartDate());
        tLAPlanDB.setPlanEndDate(this.mLAPlanSchema.getPlanEndDate());
        tLAPlanDB.setPlanCond3(this.mLAPlanSchema.getPlanCond3());
       // tLAPlanDB.setPlanCond1(this.mLAPlanSchema.getPlanCond1());
        System.out.println("query laplan");
        if (tLAPlanDB.query().size() > 0)
        {
            CError tError = new CError();
            tError.moduleName = "ALAPlanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "该对象" + this.mLAPlanSchema.getPlanStartDate() +
                                 "至"+this.mLAPlanSchema.getPlanEndDate()+ "已定任务！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLAPlanDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAPlanDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAPlanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询最大计划号失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //用计划区间单位交验
        int type=mLAPlanSchema.getPlanPeriodUnit();
        if(type==12)
        {
            tLAPlanDB = new LAPlanDB();
            String tPlanPeriod = AgentPubFun.formatDate(this.mLAPlanSchema.
                                              getPlanStartDate(), "yyyy");
            tLAPlanDB.setPlanObject(this.mLAPlanSchema.getPlanObject());
            tLAPlanDB.setPlanPeriodUnit(mLAPlanSchema.getPlanPeriodUnit());
            tLAPlanDB.setPlanCond3(this.mLAPlanSchema.getPlanCond3());
            tLAPlanDB.setPlanPeriod(tPlanPeriod);
            //tLAPlanDB.setPlanCond1(this.mLAPlanSchema.getPlanCond1());
            System.out.println("query laplan");
            if (tLAPlanDB.query().size() > 0) {
                CError tError = new CError();
                tError.moduleName = "ALAPlanBL";
                tError.functionName = "dealData";
                tError.errorMessage = "该对象的年计划在" + tPlanPeriod + "年已定任务！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLAPlanDB.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAPlanDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAPlanBL";
                tError.functionName = "dealData";
                tError.errorMessage = "交验年计划时查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }
}
