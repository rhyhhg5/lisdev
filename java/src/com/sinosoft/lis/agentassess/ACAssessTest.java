package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.VData;

/**
 * <p>Title: ACAssessTest</p>
 * <p>Description: ACAssessTest</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @xjh
 * @version 1.0
 */
public class ACAssessTest
{
    private static ACGrpAssessFactory tACGrpAssessFactory;
    public ACAssessTest()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "ac";
        tG.ManageCom = "86";
        VData tVData = new VData();
        tVData.add(tG); //管理机构
        tVData.add("86"); //管理机构
        tVData.add("2005"); //年份
        tVData.add("06"); //月份
        tVData.add("2"); //展业类型
        tVData.add("ac"); //操作人员
        tACGrpAssessFactory = new ACGrpAssessFactory();
        GradeAssessPublic tGradeAssessPublic = tACGrpAssessFactory.
                                               createGradeAssesss();
        tGradeAssessPublic.submitData(tVData, "INSERT||MAIN");
    }
}
