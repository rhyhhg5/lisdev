package com.sinosoft.lis.agentassess;


import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
/**
 * <p>Title: LABComPYRateSetUI</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author 
 * @version 1.0
 */
public class LAActiveAreaUI
{
	
	public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public LAActiveAreaUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        LAActiveAreaBL tLAActiveAreaBL=new LAActiveAreaBL();
        try
        {
            if (!tLAActiveAreaBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLAActiveAreaBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAActiveAreaUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
	
    
}
