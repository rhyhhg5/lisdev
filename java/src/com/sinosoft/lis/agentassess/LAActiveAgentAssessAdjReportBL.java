package com.sinosoft.lis.agentassess;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;

public class LAActiveAgentAssessAdjReportBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //取得的代理人编码
    private String mAgentCode = "";

//取得的机构外部代码 
    private String mBranchAttr = "";

//取得的考核年月
    private String mIndexCalNo = "";

//取得的代理人姓名
    private String mAgentName = "";

////取得的代理人序列
//    private String mAgentSeries = "";

//  取得的代理人序列
    private String mManageComName = "";

//  取得的晋降类型
    private String mAssessType = "";
    
//取得的管理机构代码
    private String mManageCom = "";
//取得当前日期
    private String mdate = "";
 //取得当期时间
    private String mtime = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
//    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
//    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
//    private LDComSchema mLDComSchema = new LDComSchema();

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean check(){
    	if(mManageCom.equals("")||mManageCom==null){
    		CError tCError=new CError();
    		tCError.moduleName="LAActiveAgentAssessAdjReportBL";
    		tCError.functionName="check";
    		tCError.errorMessage="数据录入错误，管理机构不能为空。";
    		this.mErrors.addOneError(tCError);
    		return false;
    	}
    	if(mIndexCalNo.equals("")||mIndexCalNo==null){
    		CError tCError=new CError();
    		tCError.moduleName="LAActiveAgentAssessAdjReportBL";
    		tCError.functionName="check";
    		tCError.errorMessage="数据录入错误，考核年月不能为空。";
    		this.mErrors.addOneError(tCError);
    		return false;
    	}
    	if(mAssessType.equals("")||mAssessType==null){
    		CError tCError=new CError();
    		tCError.moduleName="LAActiveAgentAssessAdjReportBL";
    		tCError.functionName="check";
    		tCError.errorMessage="数据录入错误，晋降类型不能为空。";
    		this.mErrors.addOneError(tCError);
    		return false;
    	}
//    	if(mAgentSeries.equals("")||mAgentSeries==null){
//    		CError tCError=new CError();
//    		tCError.moduleName="LAActiveAgentAssessAdjReportBL";
//    		tCError.functionName="check";
//    		tCError.errorMessage="数据传输错误，得不到代理人序列。";
//    		this.mErrors.addOneError(tCError);
//    		return false;
//    	}
    	return true;
    }
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
    	mManageCom = (String)cInputData.get(0);
    	mIndexCalNo = (String)cInputData.get(1);
    	mBranchAttr = (String)cInputData.get(2);
    	mAssessType=(String)cInputData.get(3);
    	mAgentCode = (String)cInputData.get(4);
    	mAgentName = (String)cInputData.get(5);
//    	mAgentSeries = (String)cInputData.get(6);
    	mManageComName = (String)cInputData.get(6);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAActiveAgentAssessAdjReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {       
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LAActiveAgentAssessAdjReport.vts", "printer"); //最好紧接着就初始化xml文档
        ListTable tListTable = new ListTable();
        tListTable.setName("LAActiveAgentAssessAdjReport");
        String[] title = {"", "", "", "", "" , "" , "" , "" , "" };
       
    	String tAgentGroup = "";
       //前台传入的参数为外部编码，因此需要做转换
       if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
       {
        String GroupSQL = "select agentgroup from labranchgroup where branchtype='5' and branchattr='"+this.mBranchAttr+"'";  
    	ExeSQL tExeSQL = new ExeSQL();
    	tAgentGroup = tExeSQL.getOneValue(GroupSQL);
       }
       //查询所有的记录
       msql = "select b.Groupagentcode , b.name, a.BranchAttr, "
    	   +"(select name from labranchgroup where agentgroup = b.agentgroup),"
    	   +"a.AgentGrade, a.CalAgentGrade, a.TurnAgentGrade , a.AgentGrade1,"
    	   +"(select gradename from laagentgrade where gradecode=a.agentgrade1)"
       		+"  FROM LAAssess a ,LAAgent b " 
       		+" where a.agentcode=b.agentcode and  a.state='0' "
       		+" and a.branchtype = '5'"
       		+" and a.branchtype2 = '01' "
       		+" and a.managecom like '"+mManageCom+"%'"
       		+" and a.indexcalno='"+mIndexCalNo+"'";
       if(!mAssessType.equals("00")){
    	   msql+=" and a.modifyflag='"+mAssessType+"'";
       }
//       if(this.mAgentSeries.equals("0"))    
//       {
//    	   msql += " and a.agentgrade like 'A%'";
//       }else if(this.mAgentSeries.equals("1"))    
//       {
//    	   msql += " and a.agentgrade like 'B%'";
//       }
       if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
       {
    	   msql += " and a.agentcode='"+this.mAgentCode+"'";  	   
       }
       if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
       {
    	   msql+= " and a.branchattr='"+this.mBranchAttr+"'";    	   
       }
       if(this.mAgentName!=null&&!this.mAgentName.equals(""))
       {
    	   msql+=" and b.mAgentName ='"+this.mAgentName+"'"; 
       }
       msql += " order by a.modifyflag,a.branchattr,a.agentgrade";
      ExeSQL mExeSQL = new ExeSQL();
      SSRS mSSRS = new SSRS();
      mSSRS = mExeSQL.execSQL(msql);
      System.out.println(msql);
      if (mSSRS.mErrors.needDealError()) {
          CError tCError = new CError();
          tCError.moduleName = "MakeXMLBL";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "查询XML数据出错！";

          this.mErrors.addOneError(tCError);

          return false;

      }
      if (mSSRS.getMaxRow() <= 0) {
          CError tCError = new CError();
          tCError.moduleName = "MakeXMLBL";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的考核信息！";

          this.mErrors.addOneError(tCError);

          return false;
      }
      if(mSSRS.getMaxRow()>=1)
      {
    	 for(int i=1;i<=mSSRS.getMaxRow();i++)
    	 {
    	  String Info[] = new String[9];
    	  System.out.println(mSSRS.GetText(i, 1));
    	  Info[0] = mSSRS.GetText(i, 1);
    	  System.out.println(mSSRS.GetText(i, 2));
    	  Info[1] = mSSRS.GetText(i, 2);
    	  Info[2] = mSSRS.GetText(i, 3);
    	  Info[3] = mSSRS.GetText(i, 4);
    	  Info[4] = mSSRS.GetText(i, 5);
    	  Info[5] = mSSRS.GetText(i, 6);
    	  Info[6] = mSSRS.GetText(i, 7);
    	  Info[7] = mSSRS.GetText(i, 8);
    	  Info[8] = mSSRS.GetText(i, 9);
    	  tListTable.add(Info);
    	 }
      }
      else {
          CError tCError = new CError();
          tCError.moduleName = "CreateXml";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的考核信息！";
          this.mErrors.addOneError(tCError);
          return false;
      }
//       try{
//    	   mManageComName=new String(mManageComName.getBytes("ISO8859-1"),"GBK");
//       }catch(Exception ex){
//    	   CError tCError = new CError();
//           tCError.moduleName = "LAActiveAgentAssessAdjReportBL";
//           tCError.functionName = "getPrintData";
//           tCError.errorMessage = "管理机构转码错误！";
//           this.mErrors.addOneError(tCError);
//           return false;
//       }
      	texttag.add("ManageComName",mManageComName);
      	texttag.add("IndexCalNo",mIndexCalNo);
        mdate = PubFun.getCurrentDate();
        mtime = PubFun.getCurrentTime();
         texttag.add("MakeDate", mdate); //日期
         texttag.add("MakeTime", mtime);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
        txmlexport.addListTable(tListTable,title);
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }

    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();

    }
}
