/*
 * <p>ClassName: LAAssessAccessoryInputBL </p>
 * <p>Description: 人员调岗业务逻辑，该类主要完成数据校验，
 * 并根据不同情况分别调用同序列调岗处理或异序列调岗处理</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-06-21
 */
package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAssessAccessoryBSchema;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAssessAccessorySet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.SSRS;

public class LAActiveAssessInputBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */

    //更新考核清退的人的状态为离职
    private LAAssessAccessorySchema mLAAssessAccessorySchema = new LAAssessAccessorySchema();

    private MMap mMap = new MMap();
    public LAActiveAssessInputBL() {
    }

    public static void main(String[] args) {
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LAAssessInputBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {

            if (!check()) {
            	
                return false;
            }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessInputBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAssessBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start LAAssessInputBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAssessInputBL";
            tError.functionName = "submitData";
            tError.errorMessage = "PubSubmit保存数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String OldAgentGrade = mLAAssessAccessorySchema.getAgentGrade();
        String OldAgentSeries = mLAAssessAccessorySchema.getAgentSeries();
        String NewAgentGrade = mLAAssessAccessorySchema.getAgentGrade1();
        String NewAgentSeries = mLAAssessAccessorySchema.getAgentSeries1();
        mInputData = new VData();
        mInputData.add(mLAAssessAccessorySchema);
        mInputData.add(mGlobalInput);
        //同系列晋升或降级
        if (OldAgentSeries.equals(NewAgentSeries)) {
            LAActiveAscriptInSameSeriesBL tLAActiveAscriptInSameSeriesBL = new
                    LAActiveAscriptInSameSeriesBL();
            if (!tLAActiveAscriptInSameSeriesBL.submitData(mInputData, "")) {
                this.mErrors.copyAllErrors(tLAActiveAscriptInSameSeriesBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAssessInputBL";
                tError.functionName = "submitData";
                tError.errorMessage = "对同系列职级人员归属失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //不同系列职级调整，分为1、跨系列晋升，2、跨系列降级
        else {
        	
            LAActiveAscriptInDiffSeriesBL tLAActiveAscriptInDiffSeriesBL = new
                    LAActiveAscriptInDiffSeriesBL();
             if (!tLAActiveAscriptInDiffSeriesBL.submitData(mInputData, mOperate)) {
                this.mErrors.copyAllErrors(tLAActiveAscriptInDiffSeriesBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAssessInputBL";
                tError.functionName = "submitData";
                tError.errorMessage = "对不同系列职级人员归属失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
//        //再进行LAAssessAccessory表的处理,回置调整后的新机构信息
//        if(!updateLAAssessAccessory())
//        {
//        	 CError tError = new CError();
//             tError.moduleName = "LAAssessInputBL";
//             tError.functionName = "updateLAAssessAccessory";
//             tError.errorMessage = "数据处理失败LAAssessBL-->updateLAAssessAccessory()!";
//             this.mErrors.addOneError(tError);
//             return false;
//        }
//        if(!prepareOutputData())
//        {
//        	 CError tError = new CError();
//             tError.moduleName = "LAAssessInputBL";
//             tError.functionName = "prepareOutputData";
//             tError.errorMessage = "数据处理失败LAAssessBL-->updateLAAssessAccessory()!";
//             this.mErrors.addOneError(tError);
//             return false;
//        }
//        tPubSubmit = new PubSubmit();
//        if (!tPubSubmit.submitData(mInputData, "")) {
//            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "LAAssessInputBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "PubSubmit保存数据失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
     }
    
        mInputData = null;
        return true;
    }
/**
 * 回置branchattr,newagentgroup
 * @return
 */
 private boolean updateLAAssessAccessory()
 {
	 //获得之前插入的信息
	 LAAssessAccessoryDB turnLAAssessAccessoryDB = new 
	 LAAssessAccessoryDB();
	 LAAssessAccessorySchema turnLAAssessAccessorySchema = new 
	 LAAssessAccessorySchema();
	 turnLAAssessAccessoryDB.setAgentCode(this.mLAAssessAccessorySchema.getAgentCode());
	 turnLAAssessAccessoryDB.setIndexCalNo(this.mLAAssessAccessorySchema.getIndexCalNo());
	 turnLAAssessAccessoryDB.setAssessType(this.mLAAssessAccessorySchema.getAssessType());
	 if(!turnLAAssessAccessoryDB.getInfo())
	 {
		 CError.buildErr(this,"获得考核信息失败！" );
		 return false;
	 }
	 turnLAAssessAccessorySchema.setSchema(turnLAAssessAccessoryDB.getSchema());
	 //获得代理人新的机构信息
	 LATreeDB newLATreeDB = new LATreeDB();
	 newLATreeDB.setAgentCode(mLAAssessAccessorySchema.getAgentCode());
	 newLATreeDB.getInfo();
     if(newLATreeDB.mErrors.needDealError())
     {
    	 CError.buildErr(this, "查询代理人新的行政信息失败！");
    	 return false;
     }
     turnLAAssessAccessorySchema.setAgentGroupNew(newLATreeDB.getAgentGroup());
     turnLAAssessAccessorySchema.setBranchAttr(getBranchAttr(newLATreeDB.getAgentGroup()));
     mMap = new MMap();
     mMap.put(turnLAAssessAccessorySchema, "UPDATE");
	 
     return true;
 }
 //根据内部编码获得外部编码
 private String getBranchAttr(String cAgentGroup)
 {
	 String BranchAttr = "";
	 String SQL = "select branchattr from labranchgroup where agentgroup='"+cAgentGroup+"'";
	 ExeSQL tExeSQL = new ExeSQL();
	 BranchAttr = tExeSQL.getOneValue(SQL);
	 if(BranchAttr==null||BranchAttr.equals(""))
	 {		
		return "";
	 }
	 else
	 return BranchAttr;
 }
    // pengcheng 2004-08-31  添加入司时间与考核时间校验
    private boolean check() {
        String aAgentCode = mLAAssessAccessorySchema.getAgentCode();
        String aAgentGroup = mLAAssessAccessorySchema.getAgentGroup();
        String tBranchAttr = aAgentGroup;
        String aAgentGrade = mLAAssessAccessorySchema.getAgentGrade();
        String aAgentGrade1 = mLAAssessAccessorySchema.getAgentGrade1().trim();
        String aIndexCalNo = mLAAssessAccessorySchema.getIndexCalNo();
        ExeSQL tExeSQL = new ExeSQL();
        //判断录入的考核年月是否正确
        String newSQL = "select employdate from laagent where agentcode='"+aAgentCode+"' and branchtype='5' and branchtype2='01'";
      	ExeSQL newExeSQL = new ExeSQL();
      	String empolyDate = newExeSQL.getOneValue(newSQL);
      	if(empolyDate==null||empolyDate.equals(""))
      	{
      		CError tError = new CError();
	            tError.moduleName = "check";
	            tError.functionName = "dealData";
	            tError.errorMessage = "业务员" + aAgentCode +"入司时间维护错误！";
	            this.mErrors.addOneError(tError);
	            return false;
      	}
      	System.out.println("empolyDate"+empolyDate);
  		String empMonth = AgentPubFun.formatDate(empolyDate, "yyyyMM");
      
        	 String tsql = "select max(IndexCalNo) from lawage where agentcode='" +
             aAgentCode +"' and IndexCalNo>'"+empMonth+"'";//同时校验二次入司的情况
             String MaxIndexCalNo = tExeSQL.getOneValue(tsql);
             if (!MaxIndexCalNo.equals("") && MaxIndexCalNo != null) 
             {
               if (!MaxIndexCalNo.equals(aIndexCalNo)) 
                {
                  CError tError = new CError();
                  tError.moduleName = "check";
                  tError.functionName = "dealData";
                  tError.errorMessage = "业务员" + aAgentCode +"最近一次计算薪资是" +
                                        MaxIndexCalNo + ",因此调整年月必须为" +
                                        MaxIndexCalNo +"";
                  this.mErrors.addOneError(tError);
                  return false;
                }
             }
          //判断考核月份是否已经做过组织归属
          //新员工未做过考核的要排除此校验
          String ttSQL = "select '1' from laassess where agentcode='"+aAgentCode+"' and indexcalno>='"+empMonth+"'";
          String tFlag = tExeSQL.getOneValue(ttSQL);
          if(tFlag!=null&&!tFlag.equals(""))
          {
  	        String mSQL = "select state from laassess where agentcode='"+aAgentCode+"'" +
  	        		" and  indexcalno=(select max(indexcalno) from laassess where agentcode='"+aAgentCode+"')" +
  	        				" and branchtype='5' and branchtype2='01'";
  	        String Flag = tExeSQL.getOneValue(mSQL);
  	        if(!Flag.equals("")||Flag!=null)
  	        {        	
  	        	if(!Flag.equals("2"))
  	        	{
  	        	CError tError = new CError();
  	            tError.moduleName = "check";
  	            tError.functionName = "dealData";
  	            tError.errorMessage = "业务员" + aAgentCode +"最近一次考核未做考核归属！";
  	            this.mErrors.addOneError(tError);
  	            return false;
  	            }
  	         }
          }
          else//新员工与入司时间比较
          {
          		System.out.println("empolyDate"+empolyDate);
 //         		String empMonth = AgentPubFun.formatDate(empolyDate, "yyyyMM");
          		System.out.println("empMonth"+empMonth);
          		if(empMonth.compareTo(aIndexCalNo)>0)
          		{
          			CError tError = new CError();
      	            tError.moduleName = "check";
      	            tError.functionName = "dealData";
      	            tError.errorMessage = "业务员" + aAgentCode +"入司月为"+empMonth+",调整月份不能小于入司月份！";
      	            this.mErrors.addOneError(tError);
      	            return false;
          		}
           }
      
      
        if(!checkFields(aIndexCalNo,aAgentCode))
            return false;
        //判断升降级
        String ogp2 = AgentPubFun.getAgentSeries(aAgentGrade);
        mLAAssessAccessorySchema.setAgentSeries(ogp2);
        String ngp2 = AgentPubFun.getAgentSeries(aAgentGrade1);
        mLAAssessAccessorySchema.setAgentSeries1(ngp2);
         int res = 0;
        try{
            res = ogp2.compareTo(ngp2);
        }catch (Exception ex){
            CError.buildErr(this,"调岗人员职级有误！"+ex.getMessage());
            return false;
        }

        if(res>0){
            //人员降序列
//            String trearagent = getRearAgent(aAgentCode);
//            if(trearagent==null){
//                if(tBranchAttr==null||tBranchAttr.equals("")){
//                    CError.buildErr(this,"该团队无有效的直接培养单位，请手工分配其降级后的归属团队。");
//                    return false;
//                }
//            }
//            else
//            {
//                String trearbranch = AgentPubFun.getAgentBranchAttr(trearagent);
//                if(tBranchAttr==null||tBranchAttr.equals("")){
//                    mLAAssessAccessorySchema.setAgentGroupNew(trearbranch);
//                    aAgentGroupNew = trearbranch;
//                } else {
//                    //此处应该加一个校验，验证该育成关系是否为正确的数据，
//                    //如果不是则应自动运行数据维护程序
//                    if(!trearbranch.equals(tBranchAttr)){
//                        CError.buildErr(this,"该销售团队降级，应回归到其直接培养单位："
//                                        +trearbranch+"，与您手工归属的销售团队"
//                                        +tBranchAttr+"不一致。");
//                        return false;
//                    }
//                }
//            }
        }else if (res == 0) {
//            if(!aAgentGroupNew.equals(aAgentGroup)){
//                System.out.println("||"+aAgentGroupNew+"||"+aAgentGroup);
//                CError.buildErr(this,"人员同序列调岗不允许改变销售团队!");
//                return false;
//            }
        }
//
//        if (aAgentGrade1.substring(0, 1).equals("B")
//            && !aAgentGrade1.equals("B01")) {
//            String strSql = "";
//            strSql =
//                    "select a.AgentCode,a.AgentGrade from LATree a,laagent b where a.agentgroup='" +
//                    strAgentGroup + "' and a.agentgrade >= 'B02' " +
//                    " and a.agentcode=b.agentcode  and  b.agentstate<='02'";
//            SSRS tss = tExeSQL.execSQL(strSql);
//            int tc=tss.getMaxRow();
//            if (tc > 1) {
//                this.buildError("check", "该团队数据有误，团队中有"+tc+"个主管！");
//                return false;
//            }
//            System.out.println(tc + "||" + aAgentGrade + "||" +aAgentGrade1);
//            if (tc == 1 ) {
//                && (
//                    (!aAgentGrade.equals("B11") && mNewAgentGrade.equals("B11"))
//                        ||
//                        (!aAgentGrade.equals("B21") && mNewAgentGrade.equals("B21"))
//                        || (mNewAgentGrade.compareTo("B02") >= 0
//                            && mNewAgentGrade.compareTo("B04") <= 0
//                            &&
//                            (aAgentGrade.compareTo("B02") < 0 ||
//                             aAgentGrade.compareTo("B04") > 0))
//                    )
//                String mgp2 = AgentPubFun.getAgentSeries(tss.GetText(1,2));
//                 String tOldAgentCode = tss.GetText(1,1) ;
//                if(ngp2.equals(mgp2) && !tOldAgentCode.equals(aAgentCode)){
//                    this.buildError("check", "该团队已有主管,一个团队不能存在多个主管！");
//                    return false;
//                }
//            }
//        }
//        tExeSQL = new ExeSQL();
//        String tSql= "select Agentstate from laagent where agentcode='"+aAgentCode+"' " ;
//        String tState= tExeSQL.getOneValue(tSql);
//        if(tState!=null && !tState.equals(""))
//        {
//            if(tState.compareTo("03")>=0)
//            {
//                this.buildError("check", "给人员已经离职登记或离职确认，不能修改职级！");
//                return false;
//            }
//        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        LAAssessAccessoryBSchema tLAAssessAccessoryBSchema;
        if (this.mOperate.equals("INSERT||MAIN")) {
            String tAgentCode = mLAAssessAccessorySchema.getAgentCode().trim();
            String tAgentGrade1 = mLAAssessAccessorySchema.getAgentGrade1().trim();
            String tAgentGrade = mLAAssessAccessorySchema.getAgentGrade();
            String tAssessFlag = mLAAssessAccessorySchema.getStandAssessFlag();

            //修改：2004-05-09 LL
            //修改原因：主要增加了对各职级人员包括同业衔接人员是否第一次考核的判断
            //查询该人该年月是否存有过考核记录
            LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
            tLAAssessAccessoryDB.setSchema(mLAAssessAccessorySchema);
            if (tLAAssessAccessoryDB.getInfo()) {
                if (tAssessFlag.equals("1") || //正常考核产生的记录
                    (tAssessFlag.equals("0") && //手工录入产生的且已归属
                     tLAAssessAccessoryDB.getState().equals("2"))) {
                    //备份考核信息表
                    String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                    tLAAssessAccessoryBSchema = new LAAssessAccessoryBSchema();
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLAAssessAccessoryBSchema,
                                             tLAAssessAccessoryDB.getSchema());
                    tLAAssessAccessoryBSchema.setEdorNo(tEdorNo);
                    tLAAssessAccessoryBSchema.setEdorDate(currentDate);
                    tLAAssessAccessoryBSchema.setEdorType("31"); //待定
                    tLAAssessAccessoryBSchema.setMakeDate(currentDate);
                    tLAAssessAccessoryBSchema.setMakeTime(currentTime);
                    tLAAssessAccessoryBSchema.setModifyDate(currentDate);
                    tLAAssessAccessoryBSchema.setModifyTime(currentTime);
                    tLAAssessAccessoryBSchema.setOperator(mGlobalInput.Operator);
                    mMap.put(tLAAssessAccessoryBSchema, "INSERT");
                }
                System.out.println("代理人" + tAgentCode + "该年月做过考核！");
                mLAAssessAccessorySchema.setFirstAssessFlag(
                        tLAAssessAccessoryDB.getFirstAssessFlag());
                mLAAssessAccessorySchema.setMakeDate(
                        tLAAssessAccessoryDB.getMakeDate());
                mLAAssessAccessorySchema.setMakeTime(
                        tLAAssessAccessoryDB.getMakeTime());
            } else {
                //修改：2004-05-12 LL
                //本人本月没有做过考核，其他年月不管是否做过第一次考核，第一次考核标志都置为'0'
                System.out.println("代理人" + tAgentCode + "该年月没有做过考核！");
                mLAAssessAccessorySchema.setFirstAssessFlag("0");
                mLAAssessAccessorySchema.setMakeDate(currentDate);
                mLAAssessAccessorySchema.setMakeTime(currentTime);
            }
            //升降标志
            int compare_r = tAgentGrade.compareTo(tAgentGrade1);
            if (compare_r > 0) {
                mLAAssessAccessorySchema.setModifyFlag("01"); //降级
            } else if (compare_r < 0) {
                mLAAssessAccessorySchema.setModifyFlag("03"); //升级
            } else {
                mLAAssessAccessorySchema.setModifyFlag("02"); //维持
            }

            //判断是否算作第一次考核()
            //本人本年月做过考核，则新记录的生成按原来做过的考核结果进行相关处理
            mLAAssessAccessorySchema.setState("1");
            mLAAssessAccessorySchema.setModifyDate(currentDate);
            mLAAssessAccessorySchema.setModifyTime(currentTime);
            mLAAssessAccessorySchema.setOperator(this.mGlobalInput.Operator);
            mMap.put(mLAAssessAccessorySchema, "DELETE&INSERT");

        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        try {
            this.mLAAssessAccessorySchema = (LAAssessAccessorySchema) cInputData.
                                            getObjectByObjectName(
                    "LAAssessAccessorySchema",0);
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput",0));
        } catch (Exception ex) {
            this.buildError("LAAssessInputBL", "数据不完整！");
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        System.out.println("Start LAAssessBLQuery Submit...");
        LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
        tLAAssessAccessoryDB.setSchema(mLAAssessAccessorySchema);
        //如果有需要处理的错误，则返回
        if (tLAAssessAccessoryDB.mErrors.needDealError()) {
            // @@错误处理
            CError.buildErr(this,"表LAAssessAccessory查询失败");
            return false;
        }
        LAAssessAccessorySet tLAAssessAccessorySet = tLAAssessAccessoryDB.query();
        this.mResult.add(tLAAssessAccessorySet);
        mInputData = null;
        System.out.println("End LAAssessBLQuery Submit...");
        return true;
    }

    private boolean prepareOutputData() {
        try {
        	 
        	mInputData = new VData();
            mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessInputBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    private boolean checkGroup(String aAgentGroup,String aAgentGrade) {
        if ((aAgentGroup.length() == 14) &&
            (aAgentGrade.compareTo("B04") > 0)) {
            this.buildError("checkGroup", "该团队没有" + aAgentGrade + "该职级");
            return false;
        } else if ((aAgentGroup.length() == 12) &&
                   (aAgentGrade.compareTo("B13") > 0)) {
            this.buildError("checkGroup", "该团队没有" + aAgentGrade + "该职级");
            return false;
        } else if ((aAgentGroup.length() == 10) &&
                   (aAgentGrade.compareTo("B21") > 0)) {
            this.buildError("checkGroup", "该团队没有" + aAgentGrade + "该职级");
            return false;
        }
        return true;
    }
    private boolean checkFields(String aIndexCalNo, String aAgentCode) {
        // pengcheng 2004-08-31  添加入司时间与考核时间校验
        PubCheckField checkField1 = new PubCheckField();
        VData cInputData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("IndexCalNo", aIndexCalNo);
        tTransferData.setNameAndValue("AgentCode", aAgentCode);
        //通过 CKBYSET 方式校验
        LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
        LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
        String tSql1 =
            "select * from lmcheckfield where riskcode = '000000'"
            + " and fieldname = 'LAAssessInputBL' order by serialno asc";
        tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql1);
        cInputData.add(tTransferData);
        cInputData.add(tLMCheckFieldSet);
        if (!checkField1.submitData(cInputData, "CKBYSET")) {
            System.out.println("Enter Error Field!");
            if (checkField1.mErrors.needDealError()) { //程序错误处理
                this.mErrors.copyAllErrors(checkField1.mErrors);
            } else {
                VData t = checkField1.getResultMess(); //校验错误处理
                buildError("dealData", t.get(0).toString());
            }
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }


}
