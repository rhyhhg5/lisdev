package com.sinosoft.lis.agentassess;


/**
 * <p>Title: 职级考核公共类</p>
 * <p>Description: 职级考核公共类，从考核公共类继承</p>
 * <p>Copyright: Copyright (c) sinosoft 2004</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public abstract class GradeAssessPublic extends AssessPublic implements
        GradeAssessInterface
{
    //全局变量
    //protected CErrors mErrors = new CErrors(); //错误处理类
    //protected GlobalInput mGlobalInput = new GlobalInput(); //保存传入参数
    protected String mOperate; //保存传入的操作数
    protected String mIndexCalNo; //考核指标计算编码
    protected String mOperator; //操作人员
    protected String mBranchType; //展业类型
    protected String mAreaType; //地区类型

    public GradeAssessPublic()
    {
    }

    public static void main(String[] args)
    {
    }
}
