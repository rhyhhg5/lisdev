package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.db.LABranchGroupBDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 代理人归属后处理组或部的数据</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author zhangsj
 * @version 1.0
 */

public class AscriptDealGroupData
{
//错误处理类
    public static CErrors mErrors = new CErrors();
//业务处理相关变量
    /** 全局数据 */
    private VData mInputData = new VData();
    public GlobalInput mGlobalInput = new GlobalInput();
//传入的数据
    private LATreeSchema mUpAgentSchema = new LATreeSchema();
    private LATreeSet mLATreeSet = new LATreeSet();
    private String mDGroup = "";
    private String mDAttr = "";
// mFlag： "D" -- 直辖组 、"I" -- 非直辖组
    private String mFlag = "";
    private String mIndexCalNo = "";

//日期
    private String[] MonDate = new String[2];


    public AscriptDealGroupData()
    {
    }

    public static void main(String[] args)
    {
        AscriptDealGroupData ascriptDealGroupData1 = new AscriptDealGroupData();
    }

    public boolean dealGroupData(VData cInputData, Connection conn)
    {
        LABranchGroupSchema tBGSchema = new LABranchGroupSchema();
        LABranchGroupBSchema tBGBSchema = new LABranchGroupBSchema();
        LATreeSchema tTreeSchema = new LATreeSchema();
        int tCount = 0;

        if (!getInputData(cInputData))
        {
            return false;
        }

        //取出当前被晋升的代理人所在的展业机构
        LABranchGroupDB tDB = new LABranchGroupDB();
        tDB.setAgentGroup(this.mUpAgentSchema.getAgentGroup());
        if (!tDB.getInfo())
        {
            dealError("dealGroupData", "查询当前被晋升的代理人所在的部失败!");
            return false;
        }
        String tDep = tDB.getUpBranch();

        tCount = mLATreeSet.size();

        for (int i = 1; i <= tCount; i++)
        {
            tTreeSchema.setSchema(mLATreeSet.get(i));

            //zsj--add--2004-3-17:因为代理人降级时改为不删除latreeaccessory表，所以需要判断该代理人是否已降级
            String sql = "select count(*) from latree where agentcode = '"
                         + tTreeSchema.getAgentCode().trim() +
                         "' and agentseries = '"
                         + this.mUpAgentSchema.getAgentSeries().trim() + "'";
            ExeSQL tExe = new ExeSQL();
            int count = Integer.parseInt(tExe.getOneValue(sql).trim());

            //count=0表示该代理人已经降级
            if (count == 0)
            {
                continue;
            }

            //取被归属展业机构所属的展业机构
            tDB.setAgentGroup(tTreeSchema.getAgentGroup());
            if (!tDB.getInfo())
            {
                dealError("dealGroupData", "查询被归属营业组所在的部失败!");
                return false;
            }

            String tDep1 = tDB.getUpBranch();

            // 归属以本展业机构为限
            if (!tDep.equals(tDep1))
            {
                continue;
            }

            // 如果是非直辖机构，刷新LATree
            if (this.mFlag.equals("I"))
            {
                if (!insLATreeB(tTreeSchema, conn))
                {
                    dealError("DealGroupData", "插入代理人行政信息转储表失败！");
                    return false;
                }
                //修改非直辖机构的上级代理人为被晋升的代理人
                if (!updLATree(tTreeSchema, conn))
                {
                    dealError("DealGroupData", "刷新代理人行政信息表失败！");
                    return false;
                }
            }

            //插入展业机构转储表
            if (!insLABranchGroupB(tTreeSchema.getAgentGroup(), conn))
            {
                dealError("DealGroupData", "插入展业机构转储表失败！");
                return false;
            }
            //刷新展业机构表(该函数中还更新扎帐表)
            if (!updLABranchGroup(tTreeSchema, conn))
            {
                dealError("DealGroupData", "刷新展业机构表失败！");
                return false;
            }
        }
        return true;
    }

    /**
     * 归属部下的所有营业组，生成新的branchattr
     * @param tUpAgentGroup
     * @param tNAttr
     * @return
     */

    private boolean dealSubAgency(String tUpAgentGroup, String tNAttr,
                                  Connection conn)
    {

        PubFun tPF = new PubFun();
        LABranchGroupDB tDB = new LABranchGroupDB(conn);
        LABranchGroupSet tSet = new LABranchGroupSet();
        LABranchGroupSchema tBranchSch = new LABranchGroupSchema();

        String tNewBranchAttr = "";
        String sql = "SELECT * FROM LABranchGroup WHERE UpBranch = '" +
                     tUpAgentGroup.trim() +
                     "' and ( endflag is null or  endflag <> 'Y' ) order by branchattr";
        System.out.println(sql);
        tSet = tDB.executeQuery(sql);
        int tCount = tSet.size();
        if (tCount <= 0)
        {
            return true;
        }

        for (int i = 1; i <= tCount; i++)
        {
            tBranchSch.setSchema(tSet.get(i));

            if (!insSubBranchGroupB(tBranchSch, conn))
            {
                return false;
            }

            //判断生成BranchAttr的长度
            CreateBranchAttr tCreateNo = CreateBranchAttr.getInstance();
            if (tBranchSch.getBranchLevel().trim().equals("01"))
            {
                tNewBranchAttr = tCreateNo.getTeamNo();
            }
            if (tBranchSch.getBranchLevel().trim().equals("02"))
            {
                tNewBranchAttr = tCreateNo.getDepNo();
            }
            if (tBranchSch.getBranchLevel().trim().equals("03"))
            {
                tNewBranchAttr = tCreateNo.getSDepNo();
            }

            tBranchSch.setBranchAttr(tNewBranchAttr);
            tBranchSch.setName("晋升生成");
            tBranchSch.setOperator(this.mGlobalInput.Operator);
            tBranchSch.setModifyDate(tPF.getCurrentDate());
            tBranchSch.setModifyTime(tPF.getCurrentTime());

            tDB.setSchema(tBranchSch);
            if (!tDB.update())
            {
                return false;
            }
            if (tBranchSch.getBranchLevel().trim().compareToIgnoreCase("02") >=
                0)
            {
                if (!dealSubAgency(tBranchSch.getAgentGroup(), tNewBranchAttr,
                                   conn))
                {
                    return false;
                }
            }
            else
            {
                if (!updLACommision(tBranchSch.getAgentGroup().trim(),
                                    tNewBranchAttr, conn))
                {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean insSubBranchGroupB(LABranchGroupSchema tSch,
                                       Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();
        PubFun tPF = new PubFun();
        LABranchGroupBSchema tBSh = new LABranchGroupBSchema();
        LABranchGroupBDB tBDB = new LABranchGroupBDB(conn);

        Reflections tf = new Reflections();
        tf.transFields(tBSh, tSch);

        tBSh.setEdorNo(OAgentAscript.getEdorNo());
        tBSh.setEdorType("01");
        System.out.println(
                "---zsj---AscriptDealGroupData--updSubBranchGroup--EdorNo = " +
                OAgentAscript.getEdorNo());
        tBSh.setMakeDate(tPF.getCurrentDate());
        tBSh.setMakeTime(tPF.getCurrentTime());
        tBSh.setModifyDate(tPF.getCurrentDate());
        tBSh.setModifyTime(tPF.getCurrentTime());
        tBSh.setMakeDate2(tSch.getMakeDate());
        tBSh.setMakeTime2(tSch.getMakeTime());
        tBSh.setModifyDate2(tSch.getModifyDate());
        tBSh.setModifyTime2(tSch.getModifyTime());
        tBSh.setOperator2(tSch.getOperator());
        tBSh.setOperator(this.mGlobalInput.Operator);
        tBSh.setIndexCalNo(this.mIndexCalNo);
        tBSh.setEndDate(this.MonDate[0]);
        tBDB.setSchema(tBSh);
        if (!tBDB.insert())
        {
            System.out.println("---insert error: " + tBDB.mErrors.getFirstError());
            return false;
        }
        return true;
    }


    private boolean insLATreeB(LATreeSchema tTreeSchema, Connection conn)
    {
        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();
        LATreeBDB tDB = new LATreeBDB(conn);
        LATreeBSchema tSh = new LATreeBSchema();

        Reflections rf = new Reflections();
        rf.transFields(tSh, tTreeSchema);

        tSh.setOperator(mGlobalInput.Operator);
        tSh.setOperator2(tTreeSchema.getOperator());
        tSh.setRemoveType("01");
        tSh.setModifyDate(tPF.getCurrentDate());
        tSh.setModifyTime(tPF.getCurrentTime());
        tSh.setModifyDate2(tTreeSchema.getModifyDate());
        tSh.setModifyTime2(tTreeSchema.getModifyTime());
        tSh.setMakeDate(tPF.getCurrentDate());
        tSh.setMakeTime(tPF.getCurrentTime());
        tSh.setMakeDate2(tTreeSchema.getMakeDate());
        tSh.setMakeTime2(tTreeSchema.getMakeTime());
        tSh.setEdorNO(OAgentAscript.getEdorNo());
        System.out.println(
                "---zsj---AscriptDealGroupData--insLATreeB--EdorNo = " +
                OAgentAscript.getEdorNo());
        tSh.setIndexCalNo(this.mIndexCalNo);

        tDB.setSchema(tSh);
        if (!tDB.insert())
        {
            return false;
        }

        return true;
    }

    private boolean updLATree(LATreeSchema tTreeSchema, Connection conn)
    {
        PubFun tPF = new PubFun();
        LATreeDB tDB = new LATreeDB(conn);

        tTreeSchema.setUpAgent(this.mUpAgentSchema.getAgentCode());
        tTreeSchema.setModifyDate(tPF.getCurrentDate());
        tTreeSchema.setModifyTime(tPF.getCurrentTime());
        tTreeSchema.setOperator(this.mGlobalInput.Operator);
        tTreeSchema.setAstartDate(this.MonDate[0]);
        tDB.setSchema(tTreeSchema);

        if (!tDB.update())
        {
            return false;
        }

        return true;
    }

    private boolean insLABranchGroupB(String tAgentGroup, Connection conn)
    {
        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();
        LABranchGroupDB tDB = new LABranchGroupDB();
        LABranchGroupSchema tSh = new LABranchGroupSchema();
        LABranchGroupBSchema tBSh = new LABranchGroupBSchema();
        LABranchGroupBDB tBDB = new LABranchGroupBDB(conn);

        tDB.setAgentGroup(tAgentGroup);
        if (!tDB.getInfo())
        {
            dealError("DealGroupData", "查询展业机构信息失败！");
            return false;
        }

        tSh.setSchema(tDB.getSchema());
        Reflections rf = new Reflections();
        rf.transFields(tBSh, tSh);

        tBSh.setEdorNo(OAgentAscript.getEdorNo());
        System.out.println(
                "---zsj---AscriptDealGroupData--insLABranchGroupB--EdorNo = " +
                OAgentAscript.getEdorNo());
        tBSh.setIndexCalNo(this.mIndexCalNo);
        tBSh.setEndDate(this.MonDate[0]);
        tBSh.setMakeDate2(tSh.getMakeDate());
        tBSh.setMakeDate(tPF.getCurrentDate());
        tBSh.setMakeTime2(tSh.getMakeTime());
        tBSh.setMakeTime(tPF.getCurrentTime());
        tBSh.setModifyDate2(tSh.getModifyDate());
        tBSh.setModifyDate(tPF.getCurrentDate());
        tBSh.setModifyTime2(tSh.getModifyTime());
        tBSh.setModifyTime(tPF.getCurrentTime());
        tBSh.setOperator(this.mGlobalInput.Operator);
        tBSh.setOperator2(tSh.getOperator());
        tBSh.setEdorType("01");

        tBDB.setSchema(tBSh);
        if (!tBDB.insert())
        {
            return false;
        }

        return true;
    }

    private boolean updLABranchGroup(LATreeSchema tTreeSch, Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();
        PubFun tPF = new PubFun();
        LABranchGroupDB tDB = new LABranchGroupDB(conn);
        LABranchGroupSchema tSh = new LABranchGroupSchema();
        String tTAttr = "";

        String tLevel = tTreeSch.getAgentSeries();

        tDB.setAgentGroup(tTreeSch.getAgentGroup());
        if (!tDB.getInfo())
        {
            dealError("updLABranchGroup", "查询展业机构信息失败！");
            return false;
        }
        tSh.setSchema(tDB.getSchema());

//zhangsj delete: 插入LABranchGroupB两次
//    if ( !updSubBranchGroup(tSh,conn) )
//      return false;

        //展业机构新名称暂时赋空值
        tSh.setName("晋升生成");
        tSh.setUpBranch(this.mDGroup);

        //生成新组号
        CreateBranchAttr tCreateNo = CreateBranchAttr.getInstance();
        if (tLevel.trim().equalsIgnoreCase("B"))
        {
            tTAttr = tCreateNo.getTeamNo();
        }
        if (tLevel.trim().equalsIgnoreCase("C"))
        {
            tTAttr = tCreateNo.getDepNo();
        }
        if (tLevel.trim().equalsIgnoreCase("D"))
        {
            tTAttr = tCreateNo.getSDepNo();
        }
        tSh.setBranchAttr(tTAttr);

        if (this.mFlag.equals("D"))
        {
            tSh.setUpBranchAttr("1");
        }
        else
        {
            tSh.setUpBranchAttr("0");
        }
        tSh.setModifyDate(tPF.getCurrentDate());
        tSh.setModifyTime(tPF.getCurrentTime());
        tSh.setOperator(this.mGlobalInput.Operator);

        tDB.setSchema(tSh);
        if (!tDB.update())
        {
            return false;
        }

        System.out.println("---agentSeries---" +
                           this.mUpAgentSchema.getAgentSeries().trim());

        if (tLevel.trim().compareToIgnoreCase("C") >= 0)
        {
            if (!dealSubAgency(tTreeSch.getAgentGroup(), tTAttr, conn))
            {
                dealError("dealGroupData", "归属下属机构失败！");
                return false;
            }
        }
        else
        {
            if (!updLACommision(tTreeSch.getAgentGroup().trim(), tTAttr, conn))
            {
                return false;
            }
        }

//    if ( this.mUpAgentSchema.getAgentGrade().trim().equalsIgnoreCase("D1") )
//      if ( !dealHighSubAgency(tAgentGroup,tTAttr) )  {
//        dealError("dealGroupData", "归属下属机构失败！");
//        return false;
//      }
        return true;
    }

    /**  备用，以备递归失败
      private boolean dealHighSubAgency(String tUpAgentGroup,String tNAttr)  {
        PubFun1 tPF1 = new PubFun1();
        LABranchGroupDB tDB = new LABranchGroupDB();
        LABranchGroupSet tSet = new LABranchGroupSet();
        LABranchGroupSchema tBranchSch = new LABranchGroupSchema();

        String tNewBranchAttr = "";
        String sql = "SELECT * FROM LATree WHERE UpBranch = '" +
                     tUpAgentGroup.trim() + "'";
        tSet = tDB.executeQuery(sql);
        int tCount = tSet.size();
        if ( tCount <= 0)
          return false;

        for ( int i=1;i<=tCount;i++ )  {
          tBranchSch = tSet.get(i);
          tNewBranchAttr = tPF1.CreateBranchAttr(tNAttr);
          tBranchSch.setBranchAttr(tNewBranchAttr);
          tDB.setSchema(tBranchSch);
          if (!tDB.update())
            return false;
          if ( !dealSubAgency(tBranchSch.getAgentGroup(),tNewBranchAttr) )
            return false;
        }
        return true;
      } */

    private boolean updLACommision(String tAgentGroup, String tNewAttr,
                                   Connection conn)
    {
        ExeSQL tExe = new ExeSQL(conn);
        PubFun tPF = new PubFun();
        String tStartDate = this.MonDate[0];

        //用BranchCode为了能够刷新到主管代理人的业绩

        AgentPubFun tAPF = new AgentPubFun();
        String tCheckFlag = tAPF.AdjustCommCheck(this.mUpAgentSchema.
                                                 getAgentCode().
                                                 trim(), tStartDate.trim());
        if (!tCheckFlag.trim().equals("00"))
        {
            dealError("updLACommision", tCheckFlag.trim());
            return false;
        }

        String tSql = "update lacommision set branchattr = '"
                      + tNewAttr.trim() + "' where branchcode = '"
                      + tAgentGroup.trim()
                      + "' and (caldate >= '" + tStartDate.trim()
                      + "' or caldate is null)";
        System.out.println(tSql);
        if (!tExe.execUpdateSQL(tSql))
        {
            dealError("updLACommision",
                      "刷新组员业绩表的BranchAttr失败！" + tExe.mErrors.getFirstError());
            return false;
        }

        return true;
    }


    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
            this.mFlag = (String) cInputData.getObjectByObjectName("String", 0);
            this.mLATreeSet = (LATreeSet) cInputData.getObjectByObjectName(
                    "LATreeSet", 0);
            this.mUpAgentSchema = (LATreeSchema) cInputData.
                                  getObjectByObjectName("LATreeSchema", 0);
            this.mIndexCalNo = (String) cInputData.getObjectByObjectName(
                    "String", 1);
            this.mDGroup = (String) cInputData.getObjectByObjectName("String",
                    2);
            this.mDAttr = (String) cInputData.getObjectByObjectName("String", 3);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }

        if (mFlag == null || mLATreeSet == null || mDGroup == null)
        {
            dealError("getInputdata", "没有传入足够数据！");
            return false;
        }

        this.MonDate = genDate();
        return true;
    }

    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "AscriptDealGroupData";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }

    private String[] genDate()
    {
        String[] tMonDate = new String[2];
        PubFun tPF = new PubFun();

        String tYear = this.mIndexCalNo.trim().substring(0, 4);
        String tMon = this.mIndexCalNo.trim().substring(4, 6);

        //计算旧职级的止聘日期
        String tOldDate = tYear + "-" + tMon + "-01";
        tMonDate = tPF.calFLDate(tOldDate);
        String tOldDay = tMonDate[1];

        //计算新职级的起聘日期或转正日期
        if (tMon.equals("12"))
        {
            tYear = Integer.toString(Integer.parseInt(tYear) + 1);
            tMon = "01";
        }
        else
        {
            tMon = Integer.toString(Integer.parseInt(tMon) + 1);
        }
        int tLength = tMon.length();
        if (tLength == 1)
        {
            tMon = "0" + tMon;
        }
        String tDate = tYear.trim() + "-" + tMon.trim() + "-" + "01";

        tMonDate = tPF.calFLDate(tDate);
        String tNewDay = tMonDate[0];

        tMonDate[0] = tNewDay;
        tMonDate[1] = tOldDay;
        return tMonDate;
    }

}
