package com.sinosoft.lis.agentassess;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAAgentRegularU01InputUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();
    private VData   mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    public LAAgentRegularU01InputUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData,String cOperate)
    {
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        LAAgentRegularU01InputBL tLAAgentRegularU01InputBL= new LAAgentRegularU01InputBL();
        try
        {
        	System.out.println("执行UI------------------");
            if (!tLAAgentRegularU01InputBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLAAgentRegularU01InputBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentRegularU01InputUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
 //       mResult=tLAAgentAssessBackBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
