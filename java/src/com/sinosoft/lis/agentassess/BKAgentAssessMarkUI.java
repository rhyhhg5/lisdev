/*
 * <p>ClassName: AgentWageCalDoUI </p>
 * <p>Description: LAAgentUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-10-31
 */
package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAssessMarkSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;

public class BKAgentAssessMarkUI {

    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private VData mInputData = new VData();
    private String mOperate;
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessMarkSchema mLAAssessMarkSchema = new LAAssessMarkSchema();
    private TransferData mTransferData = new TransferData();
    public BKAgentAssessMarkUI() {}

    public static void main(String[] args) {
//       VData tVData = new VData();
//       LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
//       //查询条件
//       tLAWageLogSchema.setWageYear("2002");
//       tLAWageLogSchema.setWageMonth("10");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "8611";
//       tVData.addElement(tLAWageLogSchema);
//       tVData.addElement(tG);
//       //查询
//       LAWageLogQueryUI tLAWageLogQueryUI = new LAWageLogQueryUI();
//       tLAWageLogQueryUI.submitData(tVData,"QUERY");
//       tVData.clear();
//       tVData = tLAWageLogQueryUI.getResult();
//       LAWageLogSet tLAWageLogSet = new LAWageLogSet();
//       tLAWageLogSet.set((LAWageLogSet)tVData.getObjectByObjectName("LAWageLogSet",0));
//       LAWageLogSchema nLAWageLogSchema = new LAWageLogSchema();
//       nLAWageLogSchema = (LAWageLogSchema)tLAWageLogSet.get(1);
//       tVData.clear();
//       //提交
//       tVData.addElement(nLAWageLogSchema);
//       tVData.addElement(tG);
//       AgentWageCalDoUI tAgentWageCalDoUI = new AgentWageCalDoUI();
//       //tAgentWageCalDoUI.submitData(tVData,"INSERT||CALWAGE");
//       tAgentWageCalDoUI.submitData(tVData,"INSERT||CALWAGEAGAIN");
//       System.out.println("Error:"+tAgentWageCalDoUI.mErrors.getFirstError());
        LAAssessMarkSchema mLAAssessMarkSchema = new LAAssessMarkSchema();
        BKAgentAssessMarkUI mBKAgentAssessMarkUI = new BKAgentAssessMarkUI();
        TransferData mmTransferData = new TransferData();

// AgentCode
//BranchType2
//BranchType201
//Idxnull
//AssessType02
//DoneDate2006-04-13
//AssessDate777701
//tToolMark11
//tChargeMark11
//tMeetMark11
//tRuleMark11
//tAssessMark11.00
//tAgentGroup000000000153
//tAssessMarkCount11
//tAssessPersonCount1

        mLAAssessMarkSchema.setAgentCode("1102000066");
        mLAAssessMarkSchema.setBranchType("2");
        mLAAssessMarkSchema.setBranchType2("01");
        mLAAssessMarkSchema.setIdx(null);
        mLAAssessMarkSchema.setAssessMarkType("02");
        mLAAssessMarkSchema.setDoneDate("2006-04-13");
        mLAAssessMarkSchema.setAssessYM("777701");
        mLAAssessMarkSchema.setToolMark(11);
        mLAAssessMarkSchema.setChargeMark(11);
        mLAAssessMarkSchema.setMeetMark(11);
        mLAAssessMarkSchema.setRuleMark(11);
        mLAAssessMarkSchema.setAssessMark(11);
        mLAAssessMarkSchema.setBranchAttr("000000000153"); //BranchAttr 在此为内部编码
        mLAAssessMarkSchema.setStandbyMark1(11);
        mLAAssessMarkSchema.setStandbyMark2(1);

        mmTransferData.setNameAndValue("AssessYM", "777777");
        VData tVData = new VData();
        tVData.addElement(tG);
        tVData.addElement(mLAAssessMarkSchema);
        tVData.addElement(mmTransferData);
        mBKAgentAssessMarkUI.submitData(tVData, "UPDATE||MAIN");

    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        BKAgentAssessMarkBL tBKAgentAssessMarkBL = new BKAgentAssessMarkBL();
        System.out.println("Start AgentWageCalDo11111111 UI Submit...");
        if (!tBKAgentAssessMarkBL.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tBKAgentAssessMarkBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End AgentWageCalDo UI Submit...");
        mInputData = null;
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(mGlobalInput);
            mInputData.add(mLAAssessMarkSchema);
            mInputData.add(mTransferData);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLAAssessMarkSchema = ((LAAssessMarkSchema) cInputData.
                               getObjectByObjectName("LAAssessMarkSchema", 0));
        this.mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
        if (mGlobalInput == null || mLAAssessMarkSchema == null) {
            CError tError = new CError();
            tError.moduleName = "tBKAgentAssessMarkBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

}
