package com.sinosoft.lis.agentassess;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LAhumandeveiSchema;
import com.sinosoft.utility.*;


/*
 * <p>Title: 个险人力发展 </p>
 * <p>Description: 上月人力发展指标计算 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: sinosoft</p>
 * @author WuHao
 * @version 1.1
 */
public class LaHumandeveIBL
{
    // @Field
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    //必须初始化的值
    private String mOperate;
    private VData mInputData = new VData();
    private MMap mMap = new MMap();

    private String mManageCom; //管理机构
    private String mAssessYearMonth; //考核年月
    private String mOperator;
    private String tManageCode = "";
    private ExeSQL tExeSQL = new ExeSQL();

    public LaHumandeveIBL()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }
        //对传入数据的简单校验
        if (!checkData())
        {
            return false;
        }

        System.out.println("dealData...");
        if (cOperate.equals("INSERT||MAIN"))
        {
        	//判断是否为合并机构 ，
        	String tString = "";
        	String tYear = mAssessYearMonth.substring(0,4);
        	String tSQL = "select code1 from ldcode1 where codetype = concat('"+tYear+"','includemanagecom') and code =  "+mManageCom;
            SSRS tSSRS = tExeSQL.execSQL(tSQL);
            if(tSSRS!=null && tSSRS.MaxRow>1){
            	for(int i = 1;i<=tSSRS.MaxRow;i++){
            		//如果为合并机构将机构代码拼起来
            		 tString += "'"+tSSRS.GetText(i,1 )+"',"; 
            	}         	
            	tManageCode = tString.substring(0, tString.length()-1);
            	
            }else {
            	tManageCode = "'"+mManageCom+"'";
            }
            if (!dealData())
            {
                if (mErrors.needDealError())
                {
                    System.out.println("程序异常结束原因：" + mErrors.getFirstError());
                }

                return false;
            }
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("Start CalBeforeAssessBL Submit...");

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError.buildErr(this,"数据提交失败!");
            return false;
        }

        System.out.println("End CalBeforeAssessBL Submit.");
        return true;
    }

    private boolean checkData()
    {
	
		 return true;
    }


    /**
     *通过录入的代理机构，查询出相关的其他下级的代理机构
     * 例如录入了8611，则会查出86110000
     */
    private boolean dealData()
    {
        System.out.println("be calculating... ");
//    	考核年，考核月，考核月区间
//      	String tSQL = "select codealias,comcode,code,code1,riskwrapplanname,othersign " +
//		"from ldcode1  " +
//		"where 1=1 and codetype='humandeveimonth' and codename = '"+mAssessYearMonth+"' ";
//      	System.out.println("tSQL:"+tSQL);
//      	
//
//        SSRS tSSRS = tExeSQL.execSQL(tSQL);
//
//        String tWagenoH = "";//上个薪资月，合格人力计算使用
//        String tDate = "";//上个薪资月限制日期,合格人力计算使用
//        String tDate1 = "";//考核月上限，有效合格计算都用
//        String tWageno = "";//考核起始,有效人力计算使用
//        String tDate2 = "2014-12-15";//方案起始日期固定为该日期，懒得配了，写死。后续此功能还需使用的话需要修改该日期
//        String tYear = "";//考核年
//        String tMonth = "";//考核月
//        String tYear1 = "";//上个考核月（年）
//        String tMonth1 = "";//上个考核月
//  for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
//  
//  	   tDate = tSSRS.GetText(i, 1);
//       tDate1 = tSSRS.GetText(i, 2);
//       tWageno = tSSRS.GetText(i, 5);
//       tYear = tSSRS.GetText(i, 3);
//       tMonth = tSSRS.GetText(i, 4);
//       tWagenoH = tSSRS.GetText(i, 6);
//       tYear1 = tWagenoH.substring(0,4);
//       tMonth1 = tWagenoH.substring(4,6);
//
//  }
        
        
        String tWagenoH = "";
        String tYear1 = "";
        String tMonth1 = "";
        String tYear = mAssessYearMonth.substring(0,4);
        String tMonth = mAssessYearMonth.substring(4, 6);
        String tDate2 = "2015-12-15";
        String tWageno = "201512";
        String tDate3 = "2017-1-1";
        //求上个月的 wageno 一些类型转换
        String rDate  = tYear+"-"+tMonth+"-"+"01";
        String rDate1 = PubFun.calDate(rDate, -1, "D", "2010-12-31");
        String rDate3 = AgentPubFun.formatDate(rDate1, "yyyyMMdd");
        
        tYear1 = rDate3.substring(0,4);
        tMonth1 = rDate3.substring(4, 6);
        tWagenoH = rDate3.substring(0, 6);
        //从201607月份开始，进行年度通算，合格率=（201601至本月合格人力之和）÷（201601至本月平均人力之和）
        
        //如果大于等于201607,则需要统计本月之前月份的合格人力之和，平均人力之和，累计fyc
        //本月之前合格人力之和
        String  sumQChu ="0";
      //本月之前平均人力之和
        String  sumAveHu="0";
      //本月之前fyc之和
        String sumFYC ="0";
      //本月之前实发金额之和
        String sumRealPay ="0";
        
        //在职人力中剔除组织归属后离职的人员
        int tYM=Integer.parseInt(this.mAssessYearMonth)-1;
        String delSQL="delete from lahumandesc where year ='"+tYear+"' and month='"+tMonth+"' and mngcom in("+tManageCode+") " +
        		"and agentcode in " +
        		"(select agentcode from laassess a where a.branchtype='1' and a.branchtype2='01' and a.state ='2' and a.agentgrade1 ='A00' " +
        		"and a.indexcalno ='"+tYM+"' and a.ManageCom in ("+tManageCode+") " +
        		"and a.agentcode in (select agentcode from ladimission where agentcode = a.agentcode and makedate <=a.modifydate ) " +
        		"and a.agentcode in (select agentcode from laagent where agentcode = a.agentcode and agentstate >='03') " +
        		"union " +
        		"select agentcode from laassess b where b.branchtype='1' and b.branchtype2='01' and b.state ='2' and b.agentgrade1 ='A00' " +
        		"and b.indexcalno ='"+tYM+"' and b.ManageCom in("+tManageCode+") " +
        		"and b.agentcode in (select agentcode from ladimission where agentcode = b.agentcode and departdate<=b.modifydate ) " +
        		"and b.agentcode in (select agentcode from laagent where agentcode = b.agentcode and agentstate ='02' and modifydate >= b.modifydate))";
        boolean tValue = tExeSQL.execUpdateSQL(delSQL);
        System.out.println("delSQL:"+delSQL);
        if (tExeSQL.mErrors.needDealError()) {
             // @@错误处理
             this.mErrors.copyAllErrors(tExeSQL.mErrors);
             CError tError = new CError();
             tError.moduleName = "LaHumandeveIBL";
             tError.functionName = "returnState";
             tError.errorMessage = "在职人力中剔除组织归属后离职的人员失败!";
             this.mErrors.addOneError(tError);
             System.out.println("在职人力中剔除组织归属后离职的人员失败!");
             return false;
         }
        
        System.out.println("201606".compareTo("201607"));
        System.out.println("201608".compareTo("201607"));
        if(mAssessYearMonth.compareTo("201607")>=0)
        {
        	String sumSQL="select sum(QCHu),sum(AveHu),sum(mfyc),sum(RealPay) from LAhumandevei where year ='"+tYear+"' and month<'"+tMonth+"' and mngcom ='"+this.mManageCom+"' with ur";
        	SSRS tSSRS = new SSRS();
        	tSSRS =  tExeSQL.execSQL(sumSQL);
        	if(tSSRS!=null&&tSSRS.getMaxRow()>0)
        	{
        		sumQChu = tSSRS.GetText(1, 1);
        		sumAveHu = tSSRS.GetText(1, 2);
        		sumFYC = tSSRS.GetText(1, 3);
        		sumRealPay = tSSRS.GetText(1, 4);
        	}
        }
//        
// 机构 ，方案编码
        String tSQL1 = " select code,codename from ldcode where codetype=concat('"+tYear+"','humandeveidesc') " +
        		" and code='"+this.mManageCom+"' with ur ";
        System.out.println("tSQL1:"+tSQL1);
        
        SSRS tSSRS1 = tExeSQL.execSQL(tSQL1);
        
        String tMngcom = tSSRS1.GetText(1, 1);//考核机构
        String tPlanCode = tSSRS1.GetText(1, 2);//考核机构方案编码

     // 方案，档次
        String tSQL2 = " select codealias,comcode,riskwrapplanname,othersign from ldcode1 where codetype=concat('"+tYear+"','humandeveiplan') and code='"+tPlanCode+"' ";
        System.out.println("tSQL2:"+tSQL2);
        
        SSRS tSSRS2 = tExeSQL.execSQL(tSQL2);
        
        String tPlan = tSSRS2.GetText(1, 1);//方案
        String tLevel = tSSRS2.GetText(1, 2);//档次
        String tSumFYCStand = tSSRS2.GetText(1, 3);//合格人力标准
        String tWin = tSSRS2.GetText(1, 4);//合格比例
        

        // 计划净增人力
        String tSQL3 = " select code1 from ldcode1 where codetype=concat('"+tYear+"','humandeveistand') " +
        		"and  codealias='"+tPlan+"' and comcode='"+tLevel+"' and othersign='"+tMonth+"' ";
        System.out.println("tSQL3:"+tSQL3);
        
        SSRS tSSRS3 = tExeSQL.execSQL(tSQL3);
        
        String tPlanHu = tSSRS3.GetText(1, 1);//计划净增人力


        //累计本月新增有效人力
        String tSQL4 = "";
 //       if("86120000".equals(mManageCom)){
        	
//         tSQL4 = "select count(1) from " +
//			   " (select  a.agentcode,sum(a.fyc) " +
//			   " from lacommision a,laagent b	" +
//			   " where 1=1 and a.agentcode=b.agentcode and a.BranchType='1' and a.branchtype2='01' and Payyear = 0 and a.renewcount ='0' and " +
//			   " exists(select 1 from LAhumanDesc c where c.mngcom in ('86120000','86120100') and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=b.agentcode) " +
//			   " and b.EmployDate>'"+ tDate2 +"' and a.managecom in ('86120000','86120100') " +
//			   " and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201411' and d.managecom='"+mManageCom+"') " +
////			   " and (( wageno='"+ tWageno +"' and Tmakedate >'"+ tDate2 +"' ) " +
////			   " or (wageno>'"+ tWageno +"' and wageno<'"+ mAssessYearMonth +"' ) " +
////			   " or(wageno='"+ mAssessYearMonth +"' and Tmakedate <='"+tDate1+"')) " +
//			   " and (wageno<='"+ mAssessYearMonth +"' and Tmakedate <='"+tDate1+"') " +
//			   " group by a.agentcode " +
//			   " having sum(a.fyc)>0)";
//        }else{
        	
         tSQL4 = "select count(1) from " +
        			   " (select  a.agentcode,sum(a.fyc) " +
        			   " from lacommision a,laagent b	" +
        			   " where 1=1 and a.agentcode=b.agentcode and a.BranchType='1' and a.branchtype2='01' and Payyear = 0 and a.renewcount ='0' and " +
        			   " exists(select 1 from LAhumanDesc c where c.mngcom in ("+tManageCode+") and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=b.agentcode) " +
        			   " and b.EmployDate>'"+ tDate2 +"' and a.managecom in ("+tManageCode+") " +
        			   " and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201511' and d.managecom in("+tManageCode+")) " +
//        			   " and (( wageno='"+ tWageno +"' and Tmakedate >'"+ tDate2 +"' ) " +
//        			   " or (wageno>'"+ tWageno +"' and wageno<'"+ mAssessYearMonth +"' ) " +
//        			   " or(wageno='"+ mAssessYearMonth +"' and Tmakedate <='"+tDate1+"')) " +
        			   " and Tmakedate >'"+ tDate2+"'"+
        			   " and (wageno between'"+ tWageno +"' and '"+ mAssessYearMonth +"')" +
        			   " and Tmakedate <'"+tDate3+"'"+
        			   " group by a.agentcode " +
        			   " having sum(a.fyc)>0)";
 //       }
        SSRS tSSRS4 = tExeSQL.execSQL(tSQL4);
        String tTMonthHuAdd = "";//累计有效人力
        tTMonthHuAdd = tSSRS4.GetText(1, 1);
        
       //基础人力中离职的人数
        String tSQL41 ="";
//        if("86120000".equals(mManageCom)){
//        	
//        	tSQL41 = "select count(1) from  LAhumanDesc a where a.mngcom in ('86120000','86120100') and a.year='2014' and a.month='12' " +
//   			   " and not exists(select 1 from LAhumanDesc b where b.mngcom in ('86120000','86120100') and b.year='"+tYear+"' and b.month='"+tMonth+"' and a.agentcode=b.agentcode) " ;
//   			  
//           }else{
//           	
       	   tSQL41 = "select count(1) from  LAhumanDesc a where a.mngcom in ("+tManageCode+")  and a.year='2016' and a.month='00' and a.calflag <> 'Y'"  +
   			   " and not exists(select 1 from LAhumanDesc b where b.mngcom in ("+tManageCode+")  and b.year='"+tYear+"' and b.month='"+tMonth+"' and a.agentcode=b.agentcode  ) " ;
//          }
        SSRS tSSRS41 = tExeSQL.execSQL(tSQL41);
        String tTMonthHuDL = "";//基础人力中离职的人数
        tTMonthHuDL = tSSRS41.GetText(1, 1);
        
        //月度净增累计有效人力
        int tTMonthHu =0;
        tTMonthHu = (Integer.parseInt(tTMonthHuAdd)-Integer.parseInt(tTMonthHuDL));
        
//        截至上月新增有效人力
        String tSQL5 = "select coalesce((select sum(TMonthHu) from lahumandevei where mngcom in ("+tManageCode+") and remark1='"+tWagenoH+"'),0) from dual where 1=1 ";
       
        SSRS tSSRS5 = tExeSQL.execSQL(tSQL5);
        
        String tLMonthHu = tSSRS5.GetText(1, 1);//截至上月新增有效人力
        
//        当月净增有效人力
//        String tSQL6 = "select "+tTMonthHu+"-"+tLMonthHu+" from dual where 1= 1 ";
//        
//        SSRS tSSRS6 = tExeSQL.execSQL(tSQL6);
        
//        String RealHu = tSSRS6.GetText(1, 1);//当月净增有效人力
    	int RealHu =tTMonthHu-Integer.parseInt(tLMonthHu);//当月净增有效人力
        
//        当月合格人力
        String tSQL7 = "";
//        if("86120000".equals(mManageCom)){
//        	
//        	tSQL7  = "select count(1) from " +
// 		   " (select  a.agentcode,sum(a.fyc) " +
// 		   " from lacommision a	" +
// 		   " where 1=1 and a.BranchType='1' and a.branchtype2='01' and Payyear = 0 and a.renewcount ='0' and " +
// 		   " exists(select 1 from LAhumanDesc c where c.mngcom in ('86120000','86120100') and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=a.agentcode) " +
// 		   " and a.managecom in ('86120000','86120100') " +
// 		   " and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201411' and d.managecom in ("+tManageCode+")) " +
// //		   " and (( wageno='"+ tWagenoH +"' and Tmakedate >'"+ tDate +"' ) " +
// //		   " or(wageno='"+ mAssessYearMonth +"' and Tmakedate <='"+tDate1+"')) " +
// 		   " and (wageno between '"+ tWageno +"' and '"+ mAssessYearMonth +"')"+
// 		   " group by a.agentcode " +
// 		   " having sum(a.fyc)>='"+tSumFYCStand+"')";
//        }else{
        
         tSQL7  = "select count(1) from " +
		   " (select  a.agentcode,sum(a.fyc) " +
		   " from lacommision a	" +
		   " where 1=1 and a.BranchType='1' and a.branchtype2='01'  and Payyear = 0 and a.renewcount ='0' and " +
		   " exists(select 1 from LAhumanDesc c where c.mngcom in("+tManageCode+") and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=a.agentcode) " +
		   " and a.managecom in ("+tManageCode+") " +
		   " and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201511' and d.managecom in ("+tManageCode+")) " ;
//		   " and Tmakedate >'"+ tDate2+"'"+
//		   " and (case when "+tMonth +"=01 then wageno between'"+ tWagenoH +"' and '"+ mAssessYearMonth +
//		   " else  wageno = "+mAssessYearMonth	+"')" +
//		   " and Tmakedate <'"+tDate3+"'"  +
		   if(tMonth.equals("01")){
			   tSQL7 +="and wageno between '"+tWagenoH+"' and '"+mAssessYearMonth+"' and Tmakedate > '"+tDate2+"'";
		   }else if(tMonth.equals("12")){
			   tSQL7 +="and wageno = '"+mAssessYearMonth+"' and Tmakedate < '"+tDate3+"'";
		   }else{
			   tSQL7 +="and wageno = '"+mAssessYearMonth+"'";
		   }
		   tSQL7 +=" group by a.agentcode  having sum(a.fyc)>='"+tSumFYCStand+"')";
 //       }
        SSRS tSSRS7 = tExeSQL.execSQL(tSQL7);
        String QCHu = tSSRS7.GetText(1, 1);
        
//        上月在职人数,期初人力
        String tSQL8 = "";
//        if("86120000".equals(mManageCom)){
//        	 tSQL8 = "select count(1) from LAhumanDesc where mngcom  in ('86120000','86120100') and year='"+tYear1+"' and month='"+tMonth1+"' ";
//        }else{
        if(tMonth.equals("01")){
        	tSQL8 = "select count(1) from LAhumanDesc where mngcom in("+tManageCode+") and year='2016' and month='00' ";
        }else{
         tSQL8 = "select count(1) from LAhumanDesc where mngcom in("+tManageCode+") and year='"+tYear1+"' and month='"+tMonth1+"' ";
        }
        //}
        SSRS tSSRS8 = tExeSQL.execSQL(tSQL8);
        String tTMonthHuW = tSSRS8.GetText(1, 1);
        
//        当月在职人数，期末人力
        String tSQL9 = "";
//        if("86120000".equals(mManageCom)){
//        	tSQL9 = "select count(1) from LAhumanDesc where mngcom in ('86120000','86120100') and year='"+tYear+"' and month='"+tMonth+"' ";
//        }else{
         tSQL9 = "select count(1) from LAhumanDesc where mngcom in("+tManageCode+") and year='"+tYear+"' and month='"+tMonth+"' ";
//        }
        SSRS tSSRS9 = tExeSQL.execSQL(tSQL9);
        String tLMonthHuW = tSSRS9.GetText(1, 1);

//        月均人力
        String tSQL10 = "select round(cast("+tTMonthHuW+"+"+tLMonthHuW+" as decimal(12,1)) /2,0) from dual where 1 = 1 ";
        SSRS tSSRS10 = tExeSQL.execSQL(tSQL10);
        String tAveHu = tSSRS10.GetText(1, 1);
        double aveNum = Double.parseDouble(tAveHu);
        if(0==aveNum)
        {
        	tAveHu = "0";
        }

//		获奖标准
        String tSQL11 = "select round(cast("+tTMonthHuW+"+"+tLMonthHuW+"+"+sumAveHu+"*2 as decimal(12,1)) /(2*"+tWin+"),0) from dual where 1 = 1 ";
        SSRS tSSRS11 = tExeSQL.execSQL(tSQL11);
        String tPreWin = tSSRS11.GetText(1, 1);
        double PreWinNum = Double.parseDouble(tPreWin);
        if(0==PreWinNum)
        {
        	tPreWin = "0";
        }
        
//        月度FYC
        String tSQL12 = "";
//        if("86120000".equals(mManageCom)){
//         tSQL12 = " select coalesce(sum(FYC),0) from lacommision where BranchType='1' and branchtype2='01' and managecom in ('86120000','86120100') and wageno='"+mAssessYearMonth+"'" +
//         		" and ((Payyear = 0 and renewcount =0) or  renewcount >0) ";
//        }else{
        	tSQL12 = " select coalesce(sum(FYC),0) from lacommision where BranchType='1' and branchtype2='01' and managecom in("+tManageCode+") and wageno='"+mAssessYearMonth+"' " +
        	" and ((Payyear = 0 and renewcount =0) or  renewcount >0) ";
 //       }
        SSRS tSSRS12 = tExeSQL.execSQL(tSQL12);
        String tMFYC = tSSRS12.GetText(1, 1);

//当月新增 但是 当月未出单人数
        String tSQL13 = "";
//        if("86120000".equals(mManageCom)){
//         tSQL13 = "select count(1) from laagent a where a.BranchType='1' and a.branchtype2='01' and a.managecom in ('86120000','86120100') " +
//         		" and exists(select 1 from LAhumanDesc c where c.mngcom in ('86120000','86120100') and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=a.agentcode) " +
//        		" and a.EmployDate >'"+tDate+"' and a.EmployDate<='"+tDate1+"' " +
//        		" and not exists(select 1 from lacommision " +
//        		" where  (( wageno='"+ tWagenoH +"' and Tmakedate >'"+ tDate +"' )  " +
//        		" or(wageno='"+ mAssessYearMonth +"' and Tmakedate <='"+tDate1+"')) " +
//        		" and managecom in ('86120000','86120100') and BranchType='1' and branchtype2='01' and agentcode=a.agentcode) ";
//        }else{
        	tSQL13 = "select count(1) from laagent a where a.BranchType='1' and a.branchtype2='01' and a.managecom in ("+tManageCode+") " +
    		" and not exists(select 1 from lacommision " +
			" where  Tmakedate >'"+ tDate2+"'"+
			" and (wageno between'"+ tWageno +"' and '"+ mAssessYearMonth +"')" +
			" and Tmakedate <'"+tDate3+"'"+
    		" and managecom in ("+tManageCode+") and BranchType='1' and branchtype2='01' and agentcode=a.agentcode) ";
  //      }
        SSRS tSSRS13 = tExeSQL.execSQL(tSQL13);
        String tRemark2 = tSSRS13.GetText(1, 1);
//	     达成率，获得费用   
        String tSQL15 = "select cast((round(CAST("+tTMonthHu+" AS DOUBLE)/"+tPlanHu+",4))*100 as decimal(10,2)), " +
        		" cast((case when "+QCHu+"+"+sumQChu+">="+tPreWin+"  then    " +
        					"case when '"+mAssessYearMonth+"'<='201606' then round(("+tMFYC+"+"+sumFYC+")*min(1,cast(round(CAST("+tTMonthHu+" AS decimal(12,5))/"+tPlanHu+",4) as decimal(12,4))),2) " +
							" else round(("+tMFYC+"+"+sumFYC+")*min(1,cast(round(CAST("+tTMonthHu+" AS decimal(12,5))/"+tPlanHu+",4) as decimal(12,4))),2) end" +
						" else 0   end) as decimal(10,2)) " +
        		" from dual   where 1=1 ";
        SSRS tSSRS15 = tExeSQL.execSQL(tSQL15);
        String tDaCL = tSSRS15.GetText(1, 1);
        String tfzFY = tSSRS15.GetText(1, 2);
        if("".equals(tfzFY)||null==tfzFY||"null".equals(tfzFY))
    	{
        	tfzFY ="0";
    	}
        if(Double.parseDouble(tfzFY)<0){
        	tfzFY = "0";
        }
        double tRealPay = Double.parseDouble(tfzFY)-Double.parseDouble(sumRealPay);
        /*if(Integer.parseInt(tMonth)>=4&&Integer.parseInt(tMonth)<=6){
        	String tSQL16 = "";
        	tSQL16="select sum(cast(remark3 as decimal(10,2))),sum(realpay) from lahumandevei where mngcom in ("+tManageCode+") and year = '"+tYear+"' and month < '"+tMonth+"'";
        	SSRS tSSRS16 = tExeSQL.execSQL(tSQL16);
        	String sRemark3 = tSSRS16.GetText(1, 1);
        	String sRealPay = tSSRS16.GetText(1, 2);
        	System.out.println(sRemark3+"==="+sRealPay);
        	if("".equals(sRemark3)||null==sRemark3||"null".equals(sRemark3))
        	{
        		sRemark3 ="0";
        	}
        	if("".equals(sRealPay)||null==sRealPay||"null".equals(sRealPay))
        	{
        		sRealPay ="0";
        	}
        	System.out.println(sRemark3+"==="+sRealPay);
        	tRealPay = Double.parseDouble(tfzFY)+Double.parseDouble(sRemark3)-Double.parseDouble(sRealPay) ;
        }*/
        if(tRealPay<0){
        	tRealPay = 0;
        }
        
        System.out.println("dachenglv:"+tDaCL);
        System.out.println("jine:"+tfzFY);
        
		LAhumandeveiSchema tLAhumandeveiSchema = new LAhumandeveiSchema();
		tLAhumandeveiSchema.setMngcom(tMngcom);
		tLAhumandeveiSchema.setYear(tYear);
		tLAhumandeveiSchema.setMonth(tMonth);
		tLAhumandeveiSchema.setOperator(mGlobalInput.Operator);
		tLAhumandeveiSchema.setPlanCode(tPlanCode);
		tLAhumandeveiSchema.setPlan(tPlan);
		tLAhumandeveiSchema.setLevel(tLevel);
		tLAhumandeveiSchema.setTMonthHu(tTMonthHu);
		tLAhumandeveiSchema.setLMonthHu(tLMonthHu);
		tLAhumandeveiSchema.setAveHu(tAveHu);
		tLAhumandeveiSchema.setPreWin(tPreWin);
		tLAhumandeveiSchema.setPlanHu(tPlanHu);
		tLAhumandeveiSchema.setRealHu(RealHu);
		tLAhumandeveiSchema.setQCHu(QCHu);
		tLAhumandeveiSchema.setMFYC(tMFYC);
		tLAhumandeveiSchema.setTMonthHuW(tTMonthHuW);
		tLAhumandeveiSchema.setLMonthHuW(tLMonthHuW);
		tLAhumandeveiSchema.setRemark1(mAssessYearMonth);
		tLAhumandeveiSchema.setRemark2(tRemark2);
		tLAhumandeveiSchema.setRemark3(tfzFY);
		tLAhumandeveiSchema.setRealPay(tRealPay);
		tLAhumandeveiSchema.setSumHu(tTMonthHuAdd);
		tLAhumandeveiSchema.setOutWorkHu(tTMonthHuDL);
		tLAhumandeveiSchema.setMakeDate(PubFun.getCurrentDate());
		tLAhumandeveiSchema.setMakeTime(PubFun.getCurrentTime());
		tLAhumandeveiSchema.setModifyDate(PubFun.getCurrentDate());
		tLAhumandeveiSchema.setModifyTime(PubFun.getCurrentTime());
		mMap.put(tLAhumandeveiSchema, "DELETE&INSERT");
        return true;
    }

    /**
     *校验输入参数合法性
     */
    public boolean getInputData(VData cInputData)
    {
        try
        {
            System.out.println("get input data...");
            this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            this.mManageCom = (String) cInputData.getObject(1);
            this.mAssessYearMonth = (String) cInputData.getObject(2);
            this.mOperator = mGlobalInput.Operator;
            System.out.println("mManageCom mAssessYearMonth:"+mManageCom+"-----"+mAssessYearMonth);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        if (mGlobalInput == null || mManageCom == null || mAssessYearMonth == null )
        {
            CError.buildErr(this, "没有得到足够的数据！");
            return false;
        }


       return true;
   }
    /**
     *准备传入后台数据
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();

                this.mInputData.add(mMap);

        }
        catch(Exception ex)
        {
            // @@错误处理
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错！");
            return false;
        }
        return true;
    }

 

  
    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "it001";
        tGlobalInput.ComCode = "86321100";
        tGlobalInput.ManageCom = tGlobalInput.ComCode;
        String tManageCom = "86321100";
        String tAssessYearMonth = "201607";
        String tYear = tAssessYearMonth.substring(0,4);
        String tMonth = tAssessYearMonth.substring(4, 6);
        String tDate2 = "2015-12-15";
        String tWageno = "201601";
        //求上个月的 wageno 一些类型转换
        String rDate  = tYear+"-"+tMonth+"-"+"01";
        String rDate1 = PubFun.calDate(rDate, -1, "D", "2010-12-31");
        String rDate3 = AgentPubFun.formatDate(rDate1, "yyyyMMdd");
        String tYear1 = rDate3.substring(0,4);
        String tMonth1 = rDate3.substring(4, 6);
        String tWagenoH = rDate3.substring(0, 6);
        String a = "";
        
//         a += "123";
//         a +="45"+
//         	 "67";
//        	System.out.println(tYear1);
//        	System.out.println(tMonth1);
//        	System.out.println(tWagenoH);
//        	System.out.println(a);
        VData tVData = new VData();
        tVData.addElement(tGlobalInput);
        tVData.addElement(tManageCom);
        tVData.addElement(tAssessYearMonth);
        LaHumandeveIBL tLaHumandeveIBL = new LaHumandeveIBL();
        tLaHumandeveIBL.submitData(tVData, "INSERT||MAIN");
////        tGEdorMJTask.oneCompany(tGlobalInput);\
        
    }
}	
