package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.agentcalculate.AgCalBase;
import com.sinosoft.lis.agentcalculate.CalIndex;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessIndexDB;
import com.sinosoft.lis.db.LAFirstAssessDB;
import com.sinosoft.lis.db.LAIndexVsAssessDB;
import com.sinosoft.lis.db.LAStatSegmentDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAssessMainSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LAFirstAssessSchema;
import com.sinosoft.lis.schema.LAIndexVsAssessSchema;
import com.sinosoft.lis.schema.LAStatSegmentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.lis.vschema.LAFirstAssessSet;
import com.sinosoft.lis.vschema.LAIndexVsAssessSet;
import com.sinosoft.lis.vschema.LAStatSegmentSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/*
 * <p>Title: 考核指标计算类 </p>
 * <p>Description: 计算某管理机构下所有代理人的考核指标值 </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author liujw
 * @version 1.0
 * @date 2003-02-09
 */
public class CalAssessBL1
{
    // @Field
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    //必须初始化的值
    private String mOperate;
    private VData mInputData;
    private String mAreaType; //地区类型
    private String mManageCom; //管理机构
    private String mAgentGrade; //考核职级
    private String mYear; //指标计算年份
    private String mMonth; //指标计算月份
    private String mFirstAssessDate;
    private String mLastEndDate = ""; //上次考核的止期
    private String mFirstRate; //针对第一次考核打折的比率
    private int mPreparePeriod; //针对第一次考核的筹备期限
    private String mBranchType;
    private String mOperator;

    private String mFirstAssessMark = "0"; //标记所输职级是否为第一次考核 1:第一次考核 0:非第一次考核
    private String mTYMark = "0"; //是否为同业衔接主管标志位  1：是同业衔接主管 0：非同业衔接主管
    private String mDegradeFlage = "0"; //是否讲解标志位  1：降级人员  0：非降级人员

    private String mIndexCalNo; //佣金计算编码
    private String mAgentCode; //代理人代码
    private CalIndex mCalIndex = new CalIndex();

    //管理机构下的代理人存放集
    private LATreeSet mLATreeSet = new LATreeSet();
    //存指标信息
    private LAAssessIndexDB mLAAssessIndexDB;
    //职级对应的考核指标存放处
    private String[][] GradeIndex; //存放数据格式：{职级 - 考核代码|考核类型}

    //考核指标的考核sql存放处
    private String[][] GradeSql; //存放考核SQL代码

    private LAAssessSet mLAAssessSet = new LAAssessSet();
    private LAAssessMainSchema mLAAssessMainSchema = new LAAssessMainSchema();

    public static void main(String[] args)
    {
        /********************************************************/
//    String tManageCom = "8632";
//    String tAgentGrade = "A04";
//    String tYear = "2004";
//    String tMonth = "06";
//    String tBranchType = "1";
//    VData cInputData ;
//    GlobalInput tGlobalInput = new GlobalInput();
//    tGlobalInput.Operator = "001";
//    cInputData = new VData();
//    cInputData.add(tGlobalInput);
//    cInputData.add(tManageCom);
//    cInputData.add(tAgentGrade);
//    cInputData.add(tYear);
//    cInputData.add(tMonth);
//    cInputData.add(tBranchType);
//
//    CalAssessBL tCalAssessBL = new CalAssessBL();
//    tCalAssessBL.submitData(cInputData,"INSERT||MAIN");
        String tSql = "select SetAssBeginDate('A01', '01', '01' , '1', '1', '0' , '0', 12, to_date('2003-07-01','yyyy-mm-dd') ,to_date('2004-06-30','yyyy-mm-dd')) from dual";
        ExeSQL tExtSQL = new ExeSQL();
        String tValue = tExtSQL.getOneValue(tSql);
    }

    public CalAssessBL1()
    {}

    /**
     *程序入口
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (cOperate.equals("INSERT||MAIN"))
        {
            if (!Calculate1())
            {
                if (mErrors.needDealError())
                {
                    System.out.println("程序异常结束原因：" + mErrors.getFirstError());
                }
                return false;
            }
        }
        else if (cOperate.equals("DELETE||MAIN"))
        {
            if (!prepareOutputData())
            {
                return false;
            }
            CalAssessBLS tCalAssessBLS = new CalAssessBLS();
            tCalAssessBLS.submitData(this.mInputData, "DELETE||MAIN");
            if (tCalAssessBLS.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tCalAssessBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalAssessBL1";
                tError.functionName = "submitData";
                tError.errorMessage = "数据删除失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     *通过录入的代理机构，查询出相关的其他下级的代理机构
     * 例如录入了8611，则会查出86110000，此机构为86110000的下级机构
     */
    private boolean Calculate1()
    {
        System.out.println("into into into ");
        //2004-06-01 LL
        //增加校验，要求管理机构只能录入4位代码

        //判断录入的机构代码是否为4位
        if (mManageCom.length() != 4)
        {
            CError tError = new CError();
            tError.moduleName = "CalAssessBL";
            tError.functionName = "Calculate1";
            tError.errorMessage = "管理机构只能录入4位代码！";
            this.mErrors.addOneError(tError);
            System.out.println("管理机构只能录入4位代码！！");
            return false;
        }
        //mManageCom
        String tSQL = "select * from ldcom where comcode like '" + mManageCom +
                      "%' "
                      + " and length(trim(comcode)) = 8 order by comcode asc";
        LDComDB tLDComDB = new LDComDB();
        LDComSet cLDComSet = new LDComSet();
        cLDComSet = tLDComDB.executeQuery(tSQL);
        System.out.println("用4位管理机构代码查询8位管理机构Sql : " + tSQL);
        System.out.println("count : " + cLDComSet.size());
        int count = 0;
        String tComCode = "";
        String manageCom = "";
        VData tManCom = new VData();
        //循环对每一个８位机构进行校验，校验条件为：
        //1 - 先校验机构是否满足本月佣金计算过，下个月佣金没算过的条件，
        //    不满则跳过此机构不做考核处理；
        //2 - 满足条件1则判断此机构是否满足本月作过佣金审核确认，没作过则给出错误提示；
        //3 - 如果前两个条件均通过则此机构可以参加考核计算；
        for (int i = 1; i <= cLDComSet.size(); i++)
        {
            //获得８位管理机构
            tComCode = cLDComSet.get(i).getComCode().trim();
            //保证只计算8位的分公司
            if (tComCode.length() != 8)
            {
                continue;
            }
            System.out.println(
                    "***********************************************");
            System.out.println("校验8位的分公司代码" + tComCode);

            //对条件１进行校验
            PubCheckField checkField1 = new PubCheckField();
            VData cInputData = new VData();
            //设置计算时要用到的参数值
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("IndexCalNo", this.mIndexCalNo);
            tTransferData.setNameAndValue("BranchType", this.mBranchType);
            tTransferData.setNameAndValue("ManageCom", tComCode);

            //通过CKBYFIELD
            LMCheckFieldSchema tLMCheckFieldSchema = new LMCheckFieldSchema();
            tLMCheckFieldSchema.setRiskCode("000000");
            tLMCheckFieldSchema.setRiskVer("2004");
            tLMCheckFieldSchema.setFieldName("WageCheckCalAssessBL");

            //通过 CKBYFIELD 方式校验
            cInputData.add(tTransferData);
            cInputData.add(tLMCheckFieldSchema);

            if (!checkField1.submitData(cInputData, "CKBYFIELD"))
            {
                System.out.println("校验机构佣金时发现错误");
                //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
                if (checkField1.mErrors.needDealError())
                {
                    System.out.println("ERROR-S-" +
                                       checkField1.mErrors.getFirstError());
                    CError tError = new CError();
                    tError.moduleName = "CalAssessBL";
                    tError.functionName = "Calculate1";
                    tError.errorMessage = "对佣金校验处理时出错！";
                    this.mErrors.addOneError(tError);
                    System.out.println("对佣金校验处理时出错！！");
                    return false;
                }
                else
                {
                    VData t = checkField1.getResultMess();
                    System.out.println("管理机构" + tComCode + t.get(0).toString());
                    System.out.println("不需对此管理机构进行考核计算：" + tComCode);
                    //跳过此机构，不参加计算
                    continue;
                }
            }

            //对条件２进行校验
            checkField1 = new PubCheckField();
            cInputData = new VData();
            //设置计算时要用到的参数值
            tTransferData = new TransferData();
            tTransferData.setNameAndValue("IndexCalNo", this.mIndexCalNo);
            tTransferData.setNameAndValue("BranchType", this.mBranchType);
            tTransferData.setNameAndValue("ManageCom", tComCode);

            //通过CKBYFIELD
            tLMCheckFieldSchema = new LMCheckFieldSchema();
            tLMCheckFieldSchema.setRiskCode("000000");
            tLMCheckFieldSchema.setRiskVer("2004");
            tLMCheckFieldSchema.setFieldName("WageConfCalAssessBL");

            //通过 CKBYFIELD 方式校验
            cInputData.add(tTransferData);
            cInputData.add(tLMCheckFieldSchema);

            if (!checkField1.submitData(cInputData, "CKBYFIELD"))
            {
                System.out.println("校验机构佣金是否审核时发现错误");
                //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
                if (checkField1.mErrors.needDealError())
                {
                    System.out.println("ERROR-S-" +
                                       checkField1.mErrors.getFirstError());
                    CError tError = new CError();
                    tError.moduleName = "CalAssessBL";
                    tError.functionName = "Calculate1";
                    tError.errorMessage = "判断机构佣金是否审核时出错！";
                    this.mErrors.addOneError(tError);
                    System.out.println("判断机构佣金是否审核时出错！！");
                    return false;
                }
                else
                {
                    VData t = checkField1.getResultMess();
                    CError tError = new CError();
                    tError.moduleName = "CalAssessBL";
                    tError.functionName = "Calculate1";
                    tError.errorMessage = "管理机构" + tComCode + t.get(0).toString();
                    this.mErrors.addOneError(tError);
                    System.out.println("管理机构" + tComCode + t.get(0).toString());
                    System.out.println("不对此管理机构进行考核计算：" + tComCode);
                    return false;
                }
            }

            //两个条件都满足则说明此机构可以参加考核计算
            tManCom.add(tComCode);
            System.out.println("校验后符合考核计算的机构为：" + tComCode);
        }

        //1 - 判断是否存在符合考核计算条件的机构，如果存在则进行考核计算
        if (tManCom.size() != 0)
        {
            System.out.println("符合考核计算的8位管理机构个数为：" + tManCom.size());
            PubCheckField checkField1;
            VData cInputData;
            TransferData tTransferData;
            LMCheckFieldSchema tLMCheckFieldSchema;
            for (int i = 0; i < tManCom.size(); i++)
            {
                this.mManageCom = (String) tManCom.getObject(i);
                System.out.println(String.valueOf(i) + "-----进行考核计算的8位分公司代码为：" +
                                   this.mManageCom);
                System.out.println("更新容器！");
                this.mLAAssessSet = new LAAssessSet();
                this.mLAAssessMainSchema = new LAAssessMainSchema();

                //获得地区类型
                manageCom = tComCode.substring(0, 4);
                //查找出地区类型，只用8位代码来查询
                String cType="";
                if(mBranchType.equals("2"))  cType="02";
                if(mBranchType.equals("1"))  cType="01";
                this.mAreaType = AgentPubFun.getAreaType(manageCom,cType);
                if (mAreaType == null)
                {
                    CError tError = new CError();
                    tError.moduleName = "CalAssessBL";
                    tError.functionName = "Calculate1";
                    tError.errorMessage = "查询管理机构" + this.mManageCom +
                                          "的地区类型出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                //校验管理机构是否作过考核计算
                checkField1 = new PubCheckField();
                cInputData = new VData();
                //设置计算时要用到的参数值
                tTransferData = new TransferData();
                tTransferData.setNameAndValue("IndexCalNo", this.mIndexCalNo);
                tTransferData.setNameAndValue("AgentGrade", this.mAgentGrade);
                tTransferData.setNameAndValue("ManageCom", this.mManageCom);

                //通过CKBYFIELD
                tLMCheckFieldSchema = new LMCheckFieldSchema();
                tLMCheckFieldSchema.setRiskCode("000000");
                tLMCheckFieldSchema.setRiskVer("2004");
                tLMCheckFieldSchema.setFieldName("IsAssessCalAssessBL ");

                //通过 CKBYFIELD 方式校验
                cInputData.add(tTransferData);
                cInputData.add(tLMCheckFieldSchema);

                if (!checkField1.submitData(cInputData, "CKBYFIELD"))
                {
                    System.out.println("校验机构是否作过考核时发现错误");
                    //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
                    if (checkField1.mErrors.needDealError())
                    {
                        System.out.println("ERROR-S-" +
                                           checkField1.mErrors.getFirstError());
                        CError tError = new CError();
                        tError.moduleName = "CalAssessBL";
                        tError.functionName = "Calculate1";
                        tError.errorMessage = "判断机构是否作过考核计算时出错！";
                        this.mErrors.addOneError(tError);
                        System.out.println("判断机构是否作过考核计算时出错！！");
                        return false;
                    }
                    else
                    {
                        VData t = checkField1.getResultMess();
                        System.out.println("管理机构" + this.mManageCom +
                                           t.get(0).toString());
                        CError tError = new CError();
                        tError.moduleName = "CalAssessBL";
                        tError.functionName = "Calculate1";
                        tError.errorMessage = "管理机构" + this.mManageCom +
                                              t.get(0).toString();
                        this.mErrors.addOneError(tError);
                        System.out.println("管理机构" + this.mManageCom +
                                           t.get(0).toString());
                        return false;
                    }
                }

                //逐个对每个管理机构进行计算
                System.out.println("开始计算 " + mManageCom + " 管理机构");
                if (!Calculate())
                {
                    if (mErrors.needDealError())
                    {
                        System.out.println("程序异常结束原因：" + mErrors.getFirstError());
                    }
                    return false;
                }

                //准备往后台的数据
                if (!prepareOutputData())
                {
                    return false;
                }

                System.out.println(mManageCom + "管理机构计算完成准备往后台提交");
                System.out.println("Start CalAssessBL Submit...");
                CalAssessBLS tCalAssessBLS = new CalAssessBLS();
                tCalAssessBLS.submitData(this.mInputData, mOperate);
                System.out.println("End CalAssessBL Submit...");
                if (tCalAssessBLS.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(tCalAssessBLS.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CalAssessBL";
                    tError.functionName = "submitData";
                    tError.errorMessage = "数据提交失败";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                System.out.println("管理机构" + this.mManageCom + "计算成功！");
            }
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "CalAssessBL";
            tError.functionName = "Calculate1";
            tError.errorMessage = "不存在符合考核计算条件的机构！";
            this.mErrors.addOneError(tError);
            System.out.println("不存在符合考核计算条件的机构！");
            return false;
        }

        return true;
    }

    /**
     *校验输入参数合法性
     */
    public boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mManageCom = (String) cInputData.getObject(1);
        mAgentGrade = (String) cInputData.getObject(2);
        mYear = (String) cInputData.getObject(3);
        mMonth = (String) cInputData.getObject(4);
        mBranchType = (String) cInputData.getObject(5);
        mOperator = mGlobalInput.Operator;
        System.out.println("ManageCom:" + mManageCom);
        mIndexCalNo = mYear + mMonth;
        System.out.println("生成考核指标计算编码：" + mIndexCalNo);

        if (mManageCom == null || mManageCom.equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "getInputData";
            tError.errorMessage = "计算时必须有计算编码。";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (Integer.parseInt(mYear) <= 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "getInputData";
            tError.errorMessage = "年份值没有初始化。";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (Integer.parseInt(mMonth) <= 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "getInputData";
            tError.errorMessage = "月份值没有初始化。";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("!!!!!!!!!!!***!!!!!!!!!!!");
        return true;
    }

    /**
     *准备传入后台数据
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            if (mOperate.equals("INSERT||MAIN"))
            {
                this.mInputData.add(this.mGlobalInput);
                this.mInputData.add(this.mLAAssessSet);
                this.mInputData.add(this.mLAAssessMainSchema);
            }
            else if (mOperate.equals("DELETE||MAIN"))
            {
                this.mInputData.add(mIndexCalNo);
                this.mInputData.add(mManageCom);
                this.mInputData.add(mAgentGrade);
                this.mInputData.add(mBranchType);
                this.mInputData.add(this.mGlobalInput);
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalAssessBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /** 计算考核指标 */
    private boolean Calculate()
    {
        String tSQL = "", tReturn = "";
        String tCalPrptyType = "", tAssessType = "";
        String tGrade = "", tAType = "";
        int tCount = 0, iMax = 0, iPos = 0, tATCount = 0;
        SSRS tCalPrptySSRS;
        boolean tExist = false, tNew = false;

        //查询计算日期的起止期
        System.out.println("计算日期的起止期");
        if (!setBeginEnd())
        {
            return false;
        }
        //最低职级无需检验上一职级

        if (!mAgentGrade.equals("A01"))
        {
            //检验该职级的低一职级是否考核确认
            if (!checkAssessConfirm())
            {
                return false;
            }
        }

        //查询考核指标的指标计算属性 循环计算 ，原则是先纵向后横向，但目前定义的指标的计算属性只有纵向即01
        tSQL =
                "select distinct CalPrpty from LAAssessIndex Where IndexType = '02' " //02即考核的指标
                + "order by CalPrpty asc";
        ExeSQL tExeSQL = new ExeSQL();
        tCalPrptySSRS = tExeSQL.execSQL(tSQL);
        tCount = tCalPrptySSRS.getMaxRow();

        //修改：2004-05-12 LL
        //修改原因：查询LATreeB表时增加了对removetype = '01'条件的判断

        //查询管理机构下的代理人（在考核年月之前入司的人员）ManageCom ='"+mManageCom+"' "
        LATreeDB tLATreeDB = new LATreeDB();
        tSQL = "select * from LATree where ManageCom = '" + mManageCom + "' "
               + "and AgentGrade = '" + mAgentGrade + "' "
//         +" and agentcode in ('8632000007','8632000275','8632001301') "
               +
               "and AgentCode in (select AgentCode from laagent where BranchType = '" +
               mBranchType + "' "
               + "and to_char(employdate,'yyyymm') <= '" + mIndexCalNo + "' "
               + "AND (AgentState in ('01','02') OR AgentState is null) and groupagentcode is not null) "
               +
               "and AgentCode not in (select AgentCode from LATreeB where IndexCalNo = '"
               + mIndexCalNo + "' and AgentGrade <= '" + mAgentGrade +
               "' and removetype = '01' ) "
               +
               " and AgentCode not in (select agentcode from laagent where branchcode in "
               + " (select agentgroup from labranchgroup where state ='1')) order by AgentCode asc";
        mLATreeSet = tLATreeDB.executeQuery(tSQL);
        LATreeSchema tLATreeSchema = new LATreeSchema();
        iMax = mLATreeSet.size();
        System.out.println("查询管理机构" + mManageCom + "的Sql为：" + tSQL);
        System.out.println("管理机构下的业务员个数：" + iMax);
        //if (iMax==0) return true;

        System.out.println("------------开始按指标计算属性循环" + tReturn);
        for (int i = 0; i < tCount; i++)
        {
            //从tReturn取指标类型
            tCalPrptyType = tCalPrptySSRS.GetText(i + 1, 1);
            System.out.println(String.valueOf(i) + "[--指标计算属性：" + tCalPrptyType +
                               "--]");
            if (tCalPrptyType.equals("") || tCalPrptyType == null)
            {
                continue;
            }

            //计算管理机构下所有代理人该指标计算属性的指标值
            System.out.println("---------代理人循环计算该指标计算属性的指标值");
            for (int j = 1; j <= iMax; j++)
            {
                tExist = false;
                tLATreeSchema = mLATreeSet.get(j);
                mAgentCode = tLATreeSchema.getAgentCode();
                System.out.print(String.valueOf(j) + "---" + this.mManageCom +
                                 "---代理人编码：" + mAgentCode);
                tGrade = tLATreeSchema.getAgentGrade().trim();
                System.out.println("     ------代理人职级：" + tGrade);
//                if (!mAgentGrade.equals(tGrade))
//                {
//                    System.out.println("不考核此职级！");
//                    continue;
//                }
                //主管以上判断是否是第一次考核
                if (mAgentGrade.compareTo("A04") >= 0)
                {
                    if (!IsFirstAssess(mAgentGrade, mAgentCode))
                    {
                        return false;
                    }
                }
                //设置降级标志
                //mCalIndex.tAgCalBase.setA1State(tLATreeSchema.getAssessType());
                //xjh Modify 2005/03/10 ，mResultIndexSet已经变为私有
                //mCalIndex.mResultIndexSet.clear();
                mCalIndex.clearIndexSet();
                mCalIndex.tAgCalBase.setRate("1");
                mCalIndex.tAgCalBase.setAgentGroup(tLATreeSchema.getAgentGroup());
                mCalIndex.tAgCalBase.setAgentGrade(mAgentGrade);
                mCalIndex.tAgCalBase.setZSGroupBranchAttr(AgentPubFun.
                        getAgentBranchAttr(this.mAgentCode));
                //数组中存在该职级否
                if (!tNew)
                {
                    System.out.println("[-----存储第一个职级考核指标编码----]");
                    GradeIndex = new String[1][]; //考核代码
                    GradeSql = new String[1][]; //考核sql
                    System.out.println("length:" +
                                       String.valueOf(GradeIndex.length));
                    if (!keepGrade(tGrade, 0))
                    {
                        System.out.println(tGrade + "  ---职级在考核指标对照表中没有对应的指标！");
                        String[][] temp = null;
                        GradeIndex = temp;
                        GradeSql = temp;
                        continue;
                    }
                    tNew = true;
                    iPos = 0;
                }
                else
                {
                    for (int k = 0; k < GradeIndex.length; k++)
                    {
                        if (GradeIndex[k][0].equals(tGrade.trim()))
                        {
                            System.out.println("    --职级数组中已存在该职级");
                            tExist = true;
                            iPos = k;
                            break;
                        }
                    }
                    //职级对应的考核代码集加入数组中
                    if (!tExist)
                    {
                        String[][] temp1 = GradeIndex;
                        String[][] temp2 = GradeSql;
                        GradeIndex = new String[GradeIndex.length + 1][];
                        GradeSql = new String[GradeSql.length + 1][];
                        for (int jj = 0; jj < temp1.length; jj++)
                        {
                            GradeIndex[jj] = temp1[jj];
                            GradeSql[jj] = temp2[jj];
                        }
                        System.out.println("存入职级数组中职级个数:" +
                                           String.valueOf(GradeIndex.length));
                        if (!keepGrade(tGrade, GradeIndex.length - 1))
                        {
                            System.out.println(tGrade + "职级在考核指标对照表中没有对应的指标！");
                            GradeIndex = temp1;
                            GradeSql = temp2;
                            continue;
                        }
                        iPos = GradeIndex.length - 1;
                    }
                }
                System.out.println("在职级数组中的位置iPos:" + String.valueOf(iPos));
                //循环计算所有考核指标值
                System.out.println();
                System.out.println("/-----------------开始循环计算该职级对应的考核代码");
                if (!WageCal(iPos, mAgentCode, tCalPrptyType, tCount - 1 == i))
                {
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }

                //修改：2004-04-29 LL
                //在计算完某个人维持晋升考核时得加此复位操作；例如对一个同业衔接主管
                //如果对其进行了是否第一次考核判断之后则此标志位被置为了‘1’，如果接下
                //去再对其下一个的主管（非同业衔接）进行考核的话，此标志位如果不复位的话
                //一直为‘1’，则会出问题
                this.mFirstAssessMark = "0";
                this.mDegradeFlage = "0";
                this.mTYMark = "0";
                this.mLastEndDate = "";

                System.out.println();
                System.out.println(
                        "************************************************************");
                System.out.println();
            }
        } //end of 指标计算属性for
        //将该次考核人数记录到考核主表
        if (!addLAAssessMain())
        {
            return false;
        }
        return true;
    }

    /**
     *循环计算一职级对应的所有考核代码
     * @return boolean 计算出错返回false
     **/
    private boolean WageCal(int cPos, String cAgentCode, String cIndexType,
                            boolean cIsAssess)
    {
        String tReturn = "";
        String tAssessCode = "", tAssessType = "";
        //执行优先考核sql
        if (cIndexType.equals("01"))
        {
            System.out.println("$-----------开始计算优先考核-----------$");
            tAssessCode = GradeIndex[cPos][1].substring(0,
                    GradeIndex[cPos][1].indexOf("|"));
            tAssessType = GradeIndex[cPos][1].substring(GradeIndex[cPos][1].
                    indexOf("|") + 1);
            switch (execFirstAssess(mAgentGrade, cAgentCode, tAssessCode)) //解约
            {
                case 0:
                    return false;
                case 1:
                {
                    System.out.println("不清退");
                    break;
                }
                case 2:
                {
                    System.out.println("清退");
                    return true;
                }
            }
        }
        //判断该业务员是否本月考核
        /* 设置基本参数 */
        if (!setBaseValue(mCalIndex.tAgCalBase, cAgentCode, mAgentGrade))
        {
            return false;
        }
        //CalIndex mCalIndex = new CalIndex();
        //维持和晋升都不考核则直接返回
        if (mCalIndex.tAgCalBase.AssessMark == 0)
        {
            System.out.println("代理人" + cAgentCode + "本次不考核！");
            return true;
        }
        for (int i = 1; i < GradeIndex[cPos].length; i++)
        {
            //--------计算考核代码对应指标代码
            System.out.println(String.valueOf(i) + "--考核代码：" +
                               GradeIndex[cPos][i]);
            tAssessCode = GradeIndex[cPos][i].substring(0,
                    GradeIndex[cPos][i].indexOf("|"));
            tAssessType = GradeIndex[cPos][i].substring(GradeIndex[cPos][i].
                    indexOf("|") + 1);
            //维持和考核的指标在LAIndexInfo表中存于两个记录里
            //xjh Modify 2005/03/10 ，mResultIndexSet已经变为私有
            //mCalIndex.mResultIndexSet.clear();
            mCalIndex.clearIndexSet();
            //System.out.println("CalAssessBL--ResultIndexSet--" +
            //                   mCalIndex.mResultIndexSet.size());
            System.out.println("CalAssessBL--ResultIndexSet--" +
                               mCalIndex.getIndexSetSize());
            if (tAssessType.equals("01") &&
                mCalIndex.tAgCalBase.AssessMark == 1)
            {
                System.out.println("不运行维持考核，跳过");
                continue;
            }
            if (tAssessType.equals("02") &&
                mCalIndex.tAgCalBase.AssessMark == 3)
            {
                System.out.println("不运行晋升考核，跳过");
                continue;
            }
            //查询考核代码对应的指标信息
            if (!queryIndex(cAgentCode, tAssessCode, GradeIndex[cPos][0]))
            {
                return false;
            }
            System.out.println("指标计算属性比较：" + mLAAssessIndexDB.getCalPrpty() +
                               "  " + cIndexType);
            if (mLAAssessIndexDB.getIndexType().trim().equals("03"))
            {
                System.out.println("临时类型指标不计算，跳过");
                continue;
            }
            if (mLAAssessIndexDB.getCalPrpty().trim().compareTo(cIndexType) > 0)
            {
                System.out.println("指标计算属性不符，横向指标跳过");
                continue;
            }
            //设置晋升考核的起期，不同于维持的起期
            if (!setPromoteBegin(mCalIndex.tAgCalBase, cAgentCode, mAgentGrade))
            {
                return false;
            }
            //指标计算
            //if(mLAAssessIndexDB.getCalPrpty().trim().equals(cIndexType))
            //{
            System.out.println("--该考核代码对应的指标值表中没有，开始计算。。。");
            mCalIndex.setAssessIndex(mLAAssessIndexDB);
            mCalIndex.setAgentCode(cAgentCode);
            mCalIndex.setIndexCalNo(mIndexCalNo);
            mCalIndex.setOperator(mOperator);
            mCalIndex.setIndexCalPrpty(cIndexType);
            System.out.println("TempBegin1:" +
                               mCalIndex.tAgCalBase.getTempBegin());
            System.out.println("AgentCode:" + mCalIndex.tAgCalBase.getAgentCode());
            System.out.println("---------------------------------");
            tReturn = mCalIndex.Calculate();
            if (mCalIndex.mErrors.needDealError())
            {
                System.out.println("Error:" + this.mErrors.getFirstError());
                // @@错误处理
                this.mErrors.copyAllErrors(mCalIndex.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalAssess";
                tError.functionName = "WageCal";
                tError.errorMessage = "考核指标计算出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("考核代码对应指标计算结果：" + tReturn);
            //}
//            if(!cIndexType.equals("02"))
            if (!cIsAssess)
            {
                System.out.println("现在不进行考核代码的计算！");
                continue;
            }
            //-------计算考核代码对应的sql计算编码
            System.out.println(String.valueOf(i) + "--sql计算编码：" +
                               GradeSql[cPos][i]);
            if ((GradeSql[cPos][i] != null) && (!GradeSql[cPos][i].equals("")))
            {
                /*
                         //查询判断考核代码对应的指标是否已存在于表中
                           System.out.println("查询判断考核代码对应的指标是否已存在于表中");
                 if (indexExist(cAgentCode,GradeIndex[cPos][i],GradeIndex[cPos][0],cIndexType,2))
                           {
                  System.out.println("--该考核代码对应的指标值已存在与表中");
                  continue;
                           }
                 */
                //计算考核sql
                System.out.println("--开始计算职级维持或晋升的sql。。。");
                Calculator tCal = new Calculator();
                //设置计算编码
                tCal.setCalCode(GradeSql[cPos][i]);
                //增加基本要素
                addAgCalBase(tCal);
                tReturn = tCal.calculate();
                if (tCal.mErrors.needDealError())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tCal.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CalAssess";
                    tError.functionName = "WageCal";
                    tError.errorMessage = "考核指标sql计算出错。";
                    this.mErrors.addOneError(tError);
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }
                if ((tReturn == null) || (tReturn.trim().equals("")))
                {
                    tReturn = "0";
                }
                //else
                //{
                System.out.println(
                        "--------------------insert sql-------------------------");
                if (!insertAssess(mIndexCalNo, mAgentCode, GradeIndex[cPos][0],
                                  tAssessCode, tReturn))
                {
                    System.out.println("插入考评信息表出错！！");
                    return false;
                }
                //}
                System.out.println("考核代码对应sql编码计算结果：" + tReturn);

                //修改：2004-04-29 LL
                //修改原因：对A02,A03会调用exeFirstAssess()函数执行优先考核，
                //此时会把一条记录插入到LAIndexInfo表中，插入时对StartDate置值会出错
                //所以在此计算完某个人的维持或晋升指标后会对其StartDate标志位置值
                //维持考核更新
                if (mAgentGrade.equals("A02") || mAgentGrade.equals("A03"))
                {
                    String sql1 = "";
                    if (tAssessType.equals("01"))
                    {
                        sql1 = "update LAIndexInfo set StartDate = '" +
                               mCalIndex.tAgCalBase.getTempBegin() + "' "
                               + " where IndexCalNo = '" + mIndexCalNo + "' "
                               + " and agentcode = '" + cAgentCode +
                               "' and IndexType = '02' ";
                    }
                    else if (tAssessType.equals("02"))
                    {
                        sql1 = "update LAIndexInfo set StartDate = '" +
                               mCalIndex.tAgCalBase.getTempBegin() + "' "
                               + " where IndexCalNo = '" + mIndexCalNo + "' "
                               + " and agentcode = '" + cAgentCode +
                               "' and IndexType = '03' ";
                    }
                    System.out.println("最后更新LAIndexInfo表中StartDate记录的SQL: " +
                                       sql1);
                    ExeSQL tExeSQL = new ExeSQL();
                    boolean tValue = tExeSQL.execUpdateSQL(sql1);
                    if (tExeSQL.mErrors.needDealError())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tExeSQL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "CalAssessBL";
                        tError.functionName = "WageCal";
                        tError.errorMessage = "执行SQL语句,更新LAIndexInfo表记录失败!";
                        this.mErrors.addOneError(tError);
                        System.out.println(
                                "执行SQL语句,更新LAIndexInfo表表中StartDate记录失败!");
                        return false;
                    }
                }

            }
            System.out.println();
            System.out.println(
                    "---------------------------------------------------------");
            System.out.println();

        }
        return true;
    }

    /**
     *判断指标在表中是否已存在
     * 参数：代理人编码，考核代码，代理人职级
     */
    private boolean queryIndex(String cAgentCode, String cWageCode,
                               String cAgentGrade)
    {
        String tTable = "", tCol = "", tSQL = "", tReturn = "";
        System.out.println("----取得考核代码对应的指标信息");
        LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
        tLAIndexVsAssessDB.setAgentGrade(cAgentGrade);
        tLAIndexVsAssessDB.setAssessCode(cWageCode);
        if (!tLAIndexVsAssessDB.getInfo())
        {
            //@错误处理
            this.mErrors.copyAllErrors(tLAIndexVsAssessDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "indexExist";
            tError.errorMessage = "查询考核指标对应的表名字段名出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mCalIndex.tAgCalBase.setAssessType(tLAIndexVsAssessDB.getAssessType());
        mCalIndex.tAgCalBase.setDestAgentGrade(tLAIndexVsAssessDB.
                                               getDestAgentGrade());
        mLAAssessIndexDB = new LAAssessIndexDB();
        mLAAssessIndexDB.setIndexCode(tLAIndexVsAssessDB.getIndexCode());
        if (!mLAAssessIndexDB.getInfo())
        {
            //@错误处理
            this.mErrors.copyAllErrors(mLAAssessIndexDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "indexExist";
            tError.errorMessage = "查询指标信息出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("   ----算法编码：" + mLAAssessIndexDB.getCalCode());
        System.out.println("   ----指标计算属性：" + mLAAssessIndexDB.getCalPrpty());
        System.out.println("   ----指标类型：" + mLAAssessIndexDB.getIndexType());
        return true;
    }

    /**
     *职级对应的考核代码和考核sql存入数组中
     */
    private boolean keepGrade(String cGrade, int i)
    {
        int iSize = 0;
        String tCalcd = "", tSql = "";
        //考核必须先考核 维持 后 晋升，所以要按 考核类型 排序
        tSql = "select * from LAIndexVsAssess where AgentGrade = '" + cGrade +
               "' order by DestAgentGrade";
        LAIndexVsAssessSet tLAIndexVsAssessSet = new LAIndexVsAssessSet();
        LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
        tLAIndexVsAssessSet = tLAIndexVsAssessDB.executeQuery(tSql); //职级对应的考核指标代码集
        iSize = tLAIndexVsAssessSet.size();
        if (iSize == 0)
        {
            return false;
        }
        System.out.println("考核对应指标个数：" + String.valueOf(iSize));
        GradeIndex[i] = new String[iSize + 1];
        GradeIndex[i][0] = cGrade;
        GradeSql[i] = new String[iSize + 1];
        GradeSql[i][0] = cGrade;
        LAIndexVsAssessSchema tLAIndexVsAssessSchema = new
                LAIndexVsAssessSchema();
        System.out.println("职级" + cGrade + ":");
        for (int ii = 1; ii <= iSize; ii++)
        {
            tLAIndexVsAssessSchema = tLAIndexVsAssessSet.get(ii);
            //考核代码|考核类型
            GradeIndex[i][ii] = tLAIndexVsAssessSchema.getAssessCode() + "|" +
                                tLAIndexVsAssessSchema.getAssessType();
            tCalcd = tLAIndexVsAssessSchema.getCalCode();
            if ((tCalcd == null) || (tCalcd.equals("")))
            {
                GradeSql[i][ii] = "";
            }
            else
            {
                GradeSql[i][ii] = tCalcd;
            }
            System.out.print("  " + GradeIndex[i][ii] + "|" + tCalcd);
        }
        return true;
    }

    /**
     * 在此要强调两个参数
     * cGrade      - 表示以前的职级
     * cIndexValue - 表示计算之后的职级
     */
    private boolean insertAssess(String cIndexCalNo, String cAgentCode,
                                 String cGrade, String cAssessCode,
                                 String cIndexValue)
    {
        String tSQL = "", tCount = "";
        String TableName = "", IColName = "", tModifyFlag = "";
        String tAgentSeries = "";
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        boolean tReturn = false;
        //查询佣金对应的表名和字段名
        LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
        tLAIndexVsAssessDB.setAgentGrade(cGrade);
        tLAIndexVsAssessDB.setAssessCode(cAssessCode);
        if (!tLAIndexVsAssessDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAIndexVsAssessDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "insertAssess";
            tError.errorMessage = "查询考核指标对照表出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        TableName = tLAIndexVsAssessDB.getATableName();
        IColName = tLAIndexVsAssessDB.getAColName();
        //先判断记录是否已存在
//        tSQL = "select Count(*) from "+TableName+" where IndexCalNo = '"+cIndexCalNo+"' "
//              +"and AgentCode = '"+cAgentCode+"' And ManageCom = '"+mManageCom+"'";
//        System.out.println("插入考核指标信息表的SQL:  "+tSQL);
//        ExeSQL tExeSQL = new ExeSQL();
//        tCount = tExeSQL.getOneValue(tSQL);
//        if(tExeSQL.mErrors.needDealError()||tCount==null||tCount.equals(""))
//        {
//            this.mErrors.copyAllErrors(tExeSQL.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "CalAssessBL";
//            tError.functionName = "insertAssess()";
//            tError.errorMessage = "查询代理人"+cAgentCode+"的考评记录是否已有出错!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
//        if (tCount.trim().equals("0"))
//        {
        tCount = this.mLAAssessSet.size() <= 0 ? ""
                 : this.mLAAssessSet.get(mLAAssessSet.size()).getAgentCode();

        if (tCount.equals("") || !tCount.equals(cAgentCode))
        {
            if (TableName.equals("laassess"))
            {
                LAAssessSchema tLAAssessSchema = new LAAssessSchema();
                tLAAssessSchema.setIndexCalNo(this.mIndexCalNo);
                tLAAssessSchema.setAgentCode(cAgentCode);
                tLAAssessSchema.setAgentGroup(mCalIndex.tAgCalBase.
                                              getAgentGroup());
                tLAAssessSchema.setManageCom(this.mManageCom);
                tLAAssessSchema.setBranchType(this.mBranchType);
                tLAAssessSchema.setModifyFlag("02"); // ModifyFlag在插入时默认为维持02
                tLAAssessSchema.setAgentGrade(mAgentGrade);
                tAgentSeries = AgentPubFun.getAgentSeries(mAgentGrade);
                tLAAssessSchema.setAgentSeries(tAgentSeries);
                tLAAssessSchema.setAgentGrade1(mAgentGrade); //初始插入时暂时为本职级
                tLAAssessSchema.setAgentSeries1(tAgentSeries);
                tLAAssessSchema.setState("0");
                tLAAssessSchema.setStandAssessFlag("1");
                tLAAssessSchema.setBranchAttr(mCalIndex.tAgCalBase.
                                              getZSGroupBranchAttr());

                //修改：2004-05-11 LL
                //修改原因：原来对不同职级人员分情况进行了处理，现在统一改为一种处理方法
                //方法：在laassess表中查找是否存在FirstAssessFlag = '1'的记录，有则
                // 说明做过第一次考核，没有则说明未做过第一次考核
                tSQL = "select Count(*) from " + TableName +
                       " where ManageCom = '" + mManageCom + "' "
                       + "and FirstAssessFlag = '1' and AgentCode = '" +
                       cAgentCode + "'";
                System.out.println("查询是否做过第一次考核sql:  " + tSQL);
                ExeSQL tExeSQL = new ExeSQL();
                tCount = tExeSQL.getOneValue(tSQL);
                if (tExeSQL.mErrors.needDealError() || tCount == null ||
                    tCount.equals(""))
                {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CalAssessBL";
                    tError.functionName = "insertAssess()";
                    tError.errorMessage = "查询代理人" + cAgentCode + "是否做过第一次考核出错!";
                    this.mErrors.addOneError(tError);
                    System.out.println("查询代理人" + cAgentCode + "是否做过第一次考核出错!");
                    return false;
                }
                System.out.println("是否做过第一次考核:  " + tCount);
                if (tCount.equals("0")) //没有过第一次考核记录
                {
                    tLAAssessSchema.setFirstAssessFlag("1");
                }
                else //有过第一次考核记录
                {
                    tLAAssessSchema.setFirstAssessFlag("0");
                }

                tLAAssessSchema.setMakeDate(currentDate);
                tLAAssessSchema.setMakeTime(currentTime);
                tLAAssessSchema.setModifyDate(currentDate);
                tLAAssessSchema.setModifyTime(currentTime);
                tLAAssessSchema.setOperator(mOperator);

                if (cIndexValue.equals("0") || (cIndexValue == null))
                {
                    System.out.println("不符晋升或降级，符维持");
                    this.mLAAssessSet.add(tLAAssessSchema);
                    return true;
                }
                if (cGrade.compareTo(cIndexValue) > 0) //降级 cGrade:原职级
                {
                    tModifyFlag = "01";
                }
                else if (cGrade.compareTo(cIndexValue) < 0) //升级
                {
                    tModifyFlag = "03";
                }
                else if (cGrade.compareTo(cIndexValue) == 0) //维持
                {
                    tModifyFlag = "02";
                }
                //确定新职级对应的代理人系列
                if (cIndexValue.equals("00")) //清退
                {
                    tAgentSeries = "0";
                }
                else
                {
                    tAgentSeries = AgentPubFun.getAgentSeries(cIndexValue);
                    if (tAgentSeries == null)
                    {
                        return false;
                    }
                }
                tLAAssessSchema.setModifyFlag(tModifyFlag);
                tLAAssessSchema.setAgentGrade1(cIndexValue);
                tLAAssessSchema.setAgentSeries1(tAgentSeries);
                tLAAssessSchema.setCalAgentGrade(cIndexValue);
                tLAAssessSchema.setCalAgentSeries(tAgentSeries);
                this.mLAAssessSet.add(tLAAssessSchema);
            }
        }
        else if ((TableName.equals("laassess") && !cIndexValue.equals("0")))
        {
            if (cGrade.compareTo(cIndexValue) > 0) //降级
            {
                tModifyFlag = "01";
                System.out.println("-----come 1");
            }
            else if (cGrade.compareTo(cIndexValue) < 0) //升级
            {
                tModifyFlag = "03";
                System.out.println("-----come 2");
            }
            if (cIndexValue == null)
            {
                System.out.println("indexValue is null!" + cIndexValue);
            }
            //确定新职级对应的代理人系列
            if (cIndexValue.equals("00")) //清退
            {
                tAgentSeries = "0";
            }
            else
            {
                tAgentSeries = AgentPubFun.getAgentSeries(cIndexValue);
                if (tAgentSeries == null)
                {
                    return false;
                }
            }
            this.mLAAssessSet.get(mLAAssessSet.size()).setModifyFlag(
                    tModifyFlag);
            this.mLAAssessSet.get(mLAAssessSet.size()).setAgentGrade1(
                    cIndexValue);
            this.mLAAssessSet.get(mLAAssessSet.size()).setAgentSeries1(
                    tAgentSeries);
            this.mLAAssessSet.get(mLAAssessSet.size()).setCalAgentGrade(
                    cIndexValue);
            this.mLAAssessSet.get(mLAAssessSet.size()).setCalAgentSeries(
                    tAgentSeries);
            this.mLAAssessSet.get(mLAAssessSet.size()).setModifyDate(
                    currentDate);
            this.mLAAssessSet.get(mLAAssessSet.size()).setModifyTime(
                    currentTime);
        }
        return true;
    }

    /**
     *判断某职级是否已考核归属完毕
     */
    private boolean checkAssessConfirm()
    {
        //得到上一职级
        String tLowGrade = "";
        String tSQL =
                "select Trim(code) from ldcode where codetype = 'agentgrade' "
                + "And CodeAlias = '" + mBranchType + "' and code < '" +
                mAgentGrade + "' "
                + "Order by Code desc";
        ExeSQL tExeSQL = new ExeSQL();
        tLowGrade = tExeSQL.getOneValue(tSQL);
        System.out.println("上一职级:" + tLowGrade);
        if (tExeSQL.mErrors.needDealError() ||
            tLowGrade == null || tLowGrade.equals(""))
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "checkAssessConfirm()";
            tError.errorMessage = "查询职级对应的级数出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tSQL = "Select Count(*) From LAAssessMain Where ManageCom = '" +
               mManageCom + "' "
               + "And BranchType = '" + mBranchType + "' And IndexCalNo = '" +
               mIndexCalNo + "' "
               + "And AgentGrade = '" + tLowGrade + "'";
        tExeSQL = new ExeSQL();
        String tCount = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError() ||
            tCount == null || tCount.equals(""))
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "checkAssessConfirm()";
            tError.errorMessage = "查询考核主表的考核记录出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tCount.equals("0"))
        {
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "checkAssessConfirm()";
            tError.errorMessage = "职级" + tLowGrade + "还未作考核，无法作该职级的考核";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            //是否已作归属
            tSQL = "select count(*) from laassess where indexcalno = '" +
                   mIndexCalNo + "' "
                   + "and agentgrade = '" + tLowGrade + "' and ManageCom = '" +
                   mManageCom + "' "
                   + "And State in ('0','1')";
            tExeSQL = new ExeSQL();
            tCount = tExeSQL.getOneValue(tSQL);
            if (tExeSQL.mErrors.needDealError() || tCount == null ||
                tCount.equals(""))
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalAssess";
                tError.functionName = "checkAssessConfirm";
                tError.errorMessage = "查询考核表中职级" + tLowGrade + "是否已组织归属出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (!tCount.equals("0"))
            {
                CError tError = new CError();
                tError.moduleName = "CalAssess";
                tError.functionName = "checkAssessConfirm()";
                tError.errorMessage = "职级" + tLowGrade + "还未作组织归属，无法作该职级的考核";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    //给基数类赋值
    private boolean setBaseValue(AgCalBase cAgCalBase, String cAgentCode,
                                 String cGrade)
    {
        System.out.println("欢迎进入维持起止期设置程序！");
        System.out.println("是否同业衔接人员标志位：" + this.mTYMark);
        System.out.println("是否第一次考核标志位：" + this.mFirstAssessMark);

        FDate fDate = new FDate();
        String tEndDate = cAgCalBase.getMonthEnd();
        cAgCalBase.setTempEnd(tEndDate);
        //考核期限--待定
        int assessInv = 0;
        String tSql = "";
        ExeSQL tExeSQL;
        //在cAgCalBase中设置基本信息
        cAgCalBase.setAgentCode(cAgentCode);
        cAgCalBase.setWageNo(mIndexCalNo);
        cAgCalBase.setAreaType(mAreaType);
        //初始化考核类型：在此设置维持和晋升均不考核，
        //具体考核类型在各个职级中具体处理
        cAgCalBase.AssessMark = 0;

        //修改：对A01九个月以上情况进行处理
        //修改时间：2004-03-22
        //修改者：LL
        if (cGrade.equals("A01")) //理财专员
        {
            cAgCalBase.setTempEnd(tEndDate);
            //获得代理人职级起聘日期
            String tPostDate = getPostBeginDate(cAgentCode);
            if (tPostDate == null)
            {
                return false;
            }
            System.out.println("代理人" + cAgentCode + "职级起聘日期为:" + tPostDate);
            //计算时间间隔
            if (tPostDate.compareTo(tEndDate) <= 0)
            {
                //计算时间间隔
                assessInv = calMonths(tPostDate, tEndDate);
                if (assessInv == -100)
                {
                    return false;
                }
                System.out.println("起止期间隔月数:" + assessInv);
                cAgCalBase.setTempBegin(tPostDate);
            }
            else
            {
                cAgCalBase.AssessMark = 0;
                return true;
            }
            //判断是否降级人员操作
            tSql = "select AgentCode from LAAssess where agentCode = '" +
                   cAgentCode + "'"
                   + " and AgentGrade1 = 'A01' and modifyFlag = '01'";
            tExeSQL = new ExeSQL();
            String tValue = tExeSQL.getOneValue(tSql);

            //没有降过级的A01的维持考核
            if ((tValue == null) || (tValue.equals("")))
            {
                //通过查询LAAssessRadix表获得比较期限，
                //根据比较期限进行相应处理
                if (assessInv <= 9) //小于等于比较期限
                {
                    //维持和晋升考核都做
                    cAgCalBase.AssessMark = 2;
                    cAgCalBase.setA1State("0"); //没有降级过;
                    //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
                    String tDate = "";
                    tDate = this.getBeginDate(cGrade, "01", assessInv,
                                              tPostDate, cAgCalBase.getTempEnd());
                    System.out.println("调用SetAssBeginDate计算结果为：" + tDate);
                    //校验计算结果，计算规则如下：
                    //0   - 表示程序处理过程中出错
                    //1   - 表示不参加本次考核
                    //日期 - 表示要设置的考核起期
                    if (tDate.equals("0"))
                    {
                        return false;
                    }
                    else if (tDate.equals("1"))
                    {
                        //只考核晋升
                        cAgCalBase.AssessMark = 1;
                        return true;
                    }
                    else
                    {
                        //设置考核统计起期
                        cAgCalBase.setTempBegin(tDate);
                    }
                }
                else if (assessInv > 9) //大于比较期限
                {
                    //维持和晋升考核都做,如果不是自然季的话考核是否连续两个月挂零
                    cAgCalBase.AssessMark = 2;
                    cAgCalBase.setA1State("1");
                    //大于考核期限情况考核时只有在自然季才会对其进行考核
                    //判断是不是自然季,如果是的话则进入以下处理，不是的话看是否连续两个月挂0
                    if (cAgCalBase.getQuauterMark().equals("1"))
                    {
                        //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
                        String tDate = "";
                        tDate = this.getBeginDate(cGrade, "01", assessInv,
                                                  tPostDate,
                                                  cAgCalBase.getTempEnd());

                        System.out.println("调用SetAssBeginDate计算结果为：" + tDate);
                        //校验计算结果计算规则如下：
                        //0   - 表示程序处理过程中出错
                        //1   - 表示不参加本次考核
                        //日期 - 表示要设置的考核起期
                        if (tDate.equals("0"))
                        {
                            return false;
                        }
                        else if (tDate.equals("1"))
                        {
                            //只考核晋升
                            cAgCalBase.AssessMark = 1;
                            return true;
                        }
                        else
                        {
                            //设置考核统计起期
                            cAgCalBase.setTempBegin(tDate);
                        }
                    }
                }
            }
            else //对降级的A01的维持考核情况处理
            {
                System.out.println("----对降级的理财服务专员A01的处理--------");
                //设置降级人员标志位
                this.mDegradeFlage = "1";
                System.out.println("是否降级人员标志位：" + this.mDegradeFlage);
                //降级的理财专员为最近一季考核期
                cAgCalBase.setA1State("1");
                //维持和晋升考核都做，如果不是自然季的话考核是否连续两个月挂零
                cAgCalBase.AssessMark = 2;
                if (cAgCalBase.getQuauterMark().equals("1"))
                {
                    System.out.println("进入对降级的A01处理模块！");
                    //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
                    String tDate = "";
                    tDate = this.getBeginDate(cGrade, "01", assessInv,
                                              tPostDate, cAgCalBase.getTempEnd());
                    System.out.println("调用SetAssBeginDate计算结果为：" + tDate);
                    //校验计算结果计算规则如下：
                    //0   - 表示程序处理过程中出错
                    //1   - 表示不参加本次考核
                    //日期 - 表示要设置的考核起期
                    if (tDate.equals("0"))
                    {
                        return false;
                    }
                    else if (tDate.equals("1"))
                    {
                        //只考核晋升
                        cAgCalBase.AssessMark = 1;
                        return true;
                    }
                    else
                    {
                        //设置考核统计起期
                        cAgCalBase.setTempBegin(tDate);
                    }
                }
            }
            //计算统计起期与考核止期之间的间隔月数
            System.out.println("设置后的考核统计起期为：" + cAgCalBase.getTempBegin());
            int tIntv1 = calMonths(cAgCalBase.getTempBegin(), tEndDate);
            System.out.println("统计起期与考核止期之间的间隔月数：" + tIntv1);

            //设置考核比例，非降级的A01不管任何处理都不存在打折问题
            cAgCalBase.setRate("1");

            //根据间隔月数，调用设置LAAssessPeriod函数设置LAAssessPeriod的值
            String LAAssessPeriod1 = getAssLimitPeriod(tIntv1, cGrade, "01");
            System.out.println("LimitPeriod为：" + LAAssessPeriod1);
            cAgCalBase.setLimitPeriod(LAAssessPeriod1);
        }
        else if (cGrade.equals("A02") || cGrade.equals("A03")) //理财主任 见习业务经理 --每季考核
        {
            System.out.println("欢迎进入A02-A03维持考核设置程序！");
            System.out.println("是否同业衔接人员标志位：" + this.mTYMark);
            System.out.println("是否第一次考核标志位：" + this.mFirstAssessMark);
            System.out.println("是否降级人员标志位：" + this.mDegradeFlage);
            if (cAgCalBase.getQuauterMark().equals("1"))
            {
                cAgCalBase.AssessMark = 2;
                cAgCalBase.setTempEnd(tEndDate);
                //获得职级起聘日期
                String tPostBeginDate = getPostBeginDate(cAgentCode);
                if (tPostBeginDate == null)
                {
                    return false;
                }
                if (tPostBeginDate.compareTo(cAgCalBase.getQuauterEnd()) > 0)
                {
                    cAgCalBase.AssessMark = 0;
                    return true;
                }
                System.out.println("代理人" + cAgentCode + "职级起聘日期为:" +
                                   tPostBeginDate);

                //计算时间间隔
                assessInv = calMonths(tPostBeginDate, tEndDate);
                if (assessInv == -100)
                {
                    return false;
                }
                System.out.println("起止期间隔月数为:" + assessInv);

                //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
                String tDate = "";
                tDate = this.getBeginDate(cGrade, "01", assessInv,
                                          tPostBeginDate, cAgCalBase.getTempEnd());
                System.out.println("调用SetAssBeginDate计算结果为：" + tDate);

                //校验计算结果计算规则如下：
                //0   - 表示程序处理过程中出错
                //1   - 表示不参加本次考核
                //日期 - 表示要设置的考核起期
                if (tDate.equals("0"))
                {
                    return false;
                }
                else if (tDate.equals("1"))
                {
                    //只考核晋升
                    cAgCalBase.AssessMark = 1;
                    return true;
                }
                else
                {
                    //设置考核统计起期
                    cAgCalBase.setTempBegin(tDate);
                }

                //计算统计起期与考核止期之间的间隔月数
                System.out.println("设置后的考核统计起期为：" + cAgCalBase.getTempBegin());
                int tIntv1 = calMonths(cAgCalBase.getTempBegin(), tEndDate);
                System.out.println("统计起期与考核止期之间的间隔月数：" + tIntv1);

                //设置考核比例，设置方法用：间隔月数除与考核期即为打折比例
                //1-查询出考核期月数
                String tValue = this.getAssessIntv(cGrade, "01");
                System.out.println("计算考核月数结果为：" + tValue);
                //设置打折比例
                tValue = String.valueOf(tIntv1) + "/" + tValue.trim();
                System.out.println("考核比例为：" + tValue);
                cAgCalBase.setRate(tValue);

                //根据间隔月数，调用设置LAAssessPeriod函数设置LAAssessPeriod的值
                String LAAssessPeriod1 = getAssLimitPeriod(tIntv1, cGrade, "01");
                System.out.println("LimitPeriod为：" + LAAssessPeriod1);
                cAgCalBase.setLimitPeriod(LAAssessPeriod1);
            }
            else
            {
                System.out.println("未到自然考核不考核!");
                cAgCalBase.AssessMark = 0;
                return true;
            }
        }
        else //其他职级 有固定的考核期
        {
            System.out.println("欢迎进入主管维持考核设置程序！");
            System.out.println("是否同业衔接人员标志位：" + this.mTYMark);
            System.out.println("是否第一次考核标志位：" + this.mFirstAssessMark);
            System.out.println("是否降级人员标志位：" + this.mDegradeFlage);
            cAgCalBase.setTempEnd(tEndDate);
            //设置第一次考核的起期
            if (this.mTYMark.equals("1") && this.mFirstAssessMark.equals("1"))
            {
                //保证此模块只有同业衔接主管第一次考核时才会进入
                System.out.println("欢迎进入同业衔接主管第一次考核模块");
                String tPostBeginDate = "";
                //修改：2004-05-12 LL
                //修改原因：原来取的是任职起期，现在应该改为取入司日期
                String tEmpDate = this.getEmpDate(mAgentCode);
                System.out.println("代理人" + mAgentCode + "入司日期为：" + tEmpDate);
                if (tEmpDate.equals("0")) //查询入司日期有错
                {
                    return false;
                }

                //计算入司到考核止期之间的间隔月数
                assessInv = calMonths(tEmpDate, tEndDate);
                if (assessInv == -100)
                {
                    return false;
                }
                //判断是否已过筹备期
                if (assessInv >= this.mPreparePeriod)
                {
                    cAgCalBase.AssessMark = 2;
                    cAgCalBase.setTempEnd(tEndDate);
                    //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
                    String tDate = "";
                    tDate = this.getBeginDate(cGrade, "01", assessInv, tEmpDate,
                                              cAgCalBase.getTempEnd());
                    System.out.println("调用SetAssBeginDate计算结果为：" + tDate);
                    //校验计算结果计算规则如下：
                    //0   - 表示程序处理过程中出错
                    //1   - 表示不参加本次考核
                    //日期 - 表示要设置的考核起期
                    if (tDate.equals("0"))
                    {
                        return false;
                    }
                    else if (tDate.equals("1"))
                    {
                        //只考核晋升
                        cAgCalBase.AssessMark = 1;
                        return true;
                    }
                    else
                    {
                        //设置考核统计起期
                        cAgCalBase.setTempBegin(tDate);
                    }

                    //设置考核比例，设置方法用：间隔月数除与考核期即为打折比例
                    cAgCalBase.setRate(mFirstRate);

                    //计算统计起期与考核止期之间的间隔月数
                    System.out.println("设置后的考核统计起期为：" + cAgCalBase.getTempBegin());
                    int tIntv1 = calMonths(cAgCalBase.getTempBegin(), tEndDate);
                    System.out.println("统计起期与考核止期之间的间隔月数：" + tIntv1);

                    //根据间隔月数，调用设置LAAssessPeriod函数设置LAAssessPeriod的值
                    String LAAssessPeriod1 = getAssLimitPeriod(tIntv1, cGrade,
                            "01");
                    System.out.println("LimitPeriod为：" + LAAssessPeriod1);
                    cAgCalBase.setLimitPeriod(LAAssessPeriod1);

                    cAgCalBase.FirstAssessMark = 1;

                }
                else
                {
                    System.out.println("第一次考核筹备期未满，不考核!");
                    cAgCalBase.AssessMark = 0;
                    return true;
                }
            }
            else //非第一次考核
            {
                System.out.println("进入主管非第一次考核处理模块");
                int PostInv = 0;
                String tThisBeginDate = "", tPostBeginDate = "";
                //设置本次考核的起期 = 上次考核止期+1
                //上次最后考核日期mLastEndDate在IsFirstAssess函数中设置了

                //修改：2004-05-18 LL
                //在次增加对“不是同业衔接人员，且没有做过任何考核”况的处理
                //具体设置见isFirstAssess函数

                if (!mLastEndDate.equals(""))
                {
                    tThisBeginDate = fDate.getString(PubFun.calDate(fDate.
                            getDate(mLastEndDate), 1, "D", null));
                }
                //任职起期
                tPostBeginDate = getPostBeginDate(cAgentCode);

                if (mLastEndDate.equals(""))
                {
                    tThisBeginDate = tPostBeginDate;
                }
                System.out.println("代理人" + cAgentCode + "任职起期为：" +
                                   tPostBeginDate);
                System.out.println("代理人" + cAgentCode + "最近一次考核止期+1为：" +
                                   tThisBeginDate);

                //比较上次考核期+1与任职期，有可能中途改变职级
                if (tPostBeginDate.compareTo(tThisBeginDate) > 0)
                {
                    tThisBeginDate = tPostBeginDate;
                }
                System.out.println("比较后的起期为：" + tThisBeginDate);

                if (tPostBeginDate == null)
                {
                    return false;
                }
                //非第一次考核 第二次考核的起期取上次考核期
                if (cGrade.equals("A04") || cGrade.equals("A05") ||
                    cGrade.equals("A06") || cGrade.equals("A07"))
                {
                    System.out.println("欢迎进入A04-A07维持考核设置！");
                    if (cAgCalBase.getQuauterMark().equals("1"))
                    {
                        cAgCalBase.AssessMark = 2;
                        cAgCalBase.setTempBegin(tThisBeginDate);
                        cAgCalBase.setTempEnd(tEndDate);
                        //计算起止期间隔月数
                        PostInv = calMonths(tThisBeginDate, tEndDate);
                        if (PostInv == -100)
                        {
                            return false;
                        }
                        System.out.println("起止期间隔月数：" + PostInv);

                        //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
                        String tDate = "";
                        tDate = this.getBeginDate(cGrade, "01", PostInv,
                                                  tThisBeginDate,
                                                  cAgCalBase.getTempEnd());
                        System.out.println("调用SetAssBeginDate计算结果为：" + tDate);

                        //校验计算结果:计算规则如下：
                        //0   - 表示程序处理过程中出错
                        //1   - 表示不参加本次考核
                        //日期 - 表示要设置的考核起期
                        if (tDate.equals("0"))
                        {
                            return false;
                        }
                        else if (tDate.equals("1"))
                        {
                            //只考核晋升
                            cAgCalBase.AssessMark = 1;
                            return true;
                        }
                        else
                        {
                            //设置考核统计起期
                            cAgCalBase.setTempBegin(tDate);
                        }

                        //计算统计起期与考核止期之间的间隔月数
                        System.out.println("设置后的考核统计起期为：" +
                                           cAgCalBase.getTempBegin());
                        int tIntv1 = calMonths(cAgCalBase.getTempBegin(),
                                               tEndDate);
                        System.out.println("统计起期与考核止期之间的间隔月数：" + tIntv1);

                        //设置考核比例，设置方法用：间隔月数除与考核期即为打折比例
                        //1-查询出考核期月数
                        String tValue = this.getAssessIntv(cGrade, "01");
                        System.out.println("计算考核月数结果为：" + tValue);
                        //设置打折比例
                        tValue = String.valueOf(tIntv1) + "/" + tValue.trim();
                        System.out.println("考核比例为：" + tValue);
                        cAgCalBase.setRate(tValue);

                        //根据间隔月数，调用设置LAAssessPeriod函数设置LAAssessPeriod的值
                        String LAAssessPeriod1 = getAssLimitPeriod(tIntv1,
                                cGrade, "01");
                        System.out.println("LimitPeriod为：" + LAAssessPeriod1);
                        cAgCalBase.setLimitPeriod(LAAssessPeriod1);

                    }
                    else
                    {
                        System.out.println("未到自然季，不考核!");
                        cAgCalBase.AssessMark = 0;
                        return true;
                    }
                }
                if (cGrade.equals("A08") || cGrade.equals("A09"))
                {
                    if (cAgCalBase.getYearMark().equals("1"))
                    {
                        cAgCalBase.AssessMark = 2;
                        cAgCalBase.setTempBegin(tThisBeginDate);
                        cAgCalBase.setTempEnd(tEndDate);
                        //设置考核比率
                        PostInv = calMonths(tThisBeginDate, tEndDate);
                        if (PostInv == -100)
                        {
                            return false;
                        }
                        System.out.println("起止期间隔月数：" + PostInv);

                        //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
                        String tDate = "";
                        tDate = this.getBeginDate(cGrade, "01", PostInv,
                                                  tThisBeginDate,
                                                  cAgCalBase.getTempEnd());
                        System.out.println("调用SetAssBeginDate计算结果为：" + tDate);

                        //校验计算结果:计算规则如下：
                        //0   - 表示程序处理过程中出错
                        //1   - 表示不参加本次考核
                        //日期 - 表示要设置的考核起期
                        if (tDate.equals("0"))
                        {
                            return false;
                        }
                        else if (tDate.equals("1"))
                        {
                            //只考核晋升
                            cAgCalBase.AssessMark = 1;
                            return true;
                        }
                        else
                        {
                            //设置考核统计起期
                            cAgCalBase.setTempBegin(tDate);
                        }

                        //计算统计起期与考核止期之间的间隔月数
                        System.out.println("设置后的考核统计起期为：" +
                                           cAgCalBase.getTempBegin());
                        int tIntv1 = calMonths(cAgCalBase.getTempBegin(),
                                               tEndDate);
                        System.out.println("统计起期与考核止期之间的间隔月数：" + tIntv1);

                        //设置考核比例，设置方法用：间隔月数除与考核期即为打折比例
                        //1-查询出考核期月数
                        String tValue = this.getAssessIntv(cGrade, "01");
                        System.out.println("计算考核月数结果为：" + tValue);
                        //设置打折比例
                        tValue = String.valueOf(tIntv1) + "/" + tValue.trim();
                        System.out.println("考核比例为：" + tValue);
                        cAgCalBase.setRate(tValue);

                        //根据间隔月数，调用设置LAAssessPeriod函数设置LAAssessPeriod的值
                        String LAAssessPeriod1 = getAssLimitPeriod(tIntv1,
                                cGrade, "01");
                        System.out.println("LimitPeriod为：" + LAAssessPeriod1);
                        cAgCalBase.setLimitPeriod(LAAssessPeriod1);

                    }
                    else
                    {
                        System.out.println("未到自然年度，不考核!");
                        cAgCalBase.AssessMark = 0;
                        return true;
                    }
                }
            }
        }

//    System.out.println("limitPeriod:"+tCode);
//    cAgCalBase.setLimitPeriod(tCode);
        return true;
    }

    /**
     * 函数功能：设置 自然月 、自然季 、 自然年 、 年 的起止期
     */
    private boolean setBeginEnd()
    {
        //从数据库中查出时间区间
        LAStatSegmentSet tLAStatSegmentSet = new LAStatSegmentSet();
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
        tLAStatSegmentDB.setYearMonth(Integer.parseInt(this.mIndexCalNo));
        tLAStatSegmentSet = tLAStatSegmentDB.query();
        if (tLAStatSegmentDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "CalAssess";
            tCError.functionName = "setBeginEnd()";
            tCError.errorMessage = "查询时间区间出错！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        int tCount = tLAStatSegmentSet.size();
        System.out.println("tCount:" + tCount + "," + this.mIndexCalNo);
        if (tCount < 1)
        {
            CError tCError = new CError();
            tCError.moduleName = "CalAssess";
            tCError.functionName = "setBeginEnd()";
            tCError.errorMessage = "所输年月有问题，无法查到时间区间";
            this.mErrors.addOneError(tCError);
            return false;
        }
        LAStatSegmentSchema tLAStatSegmentSchema = new LAStatSegmentSchema();
        String tStatType = "";
        for (int i = 1; i <= tCount; i++)
        {
            tLAStatSegmentSchema = tLAStatSegmentSet.get(i);
            tStatType = tLAStatSegmentSchema.getStatType().trim();
            if (tStatType.equals("1"))
            {
                mCalIndex.tAgCalBase.setMonthMark("1");
                mCalIndex.tAgCalBase.setMonthBegin(tLAStatSegmentSchema.
                        getStartDate());
                mCalIndex.tAgCalBase.setMonthEnd(tLAStatSegmentSchema.
                                                 getEndDate());
                continue;
            }
            if (tStatType.equals("2") &&
                !mCalIndex.tAgCalBase.getQuauterMark().equals("1"))
            {
                mCalIndex.tAgCalBase.setQuauterMark("1");
                mCalIndex.tAgCalBase.setQuauterBegin(tLAStatSegmentSchema.
                        getStartDate());
                mCalIndex.tAgCalBase.setQuauterEnd(tLAStatSegmentSchema.
                        getEndDate());
                continue;
            }
            if (tStatType.equals("3") &&
                !mCalIndex.tAgCalBase.getHalfYearMark().equals("1"))
            {
                mCalIndex.tAgCalBase.setHalfYearMark("1");
                mCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema.
                        getStartDate());
                mCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema.
                        getEndDate());
                continue;
            }
            if (tStatType.equals("4") &&
                !mCalIndex.tAgCalBase.getYearMark().equals("1"))
            {
                mCalIndex.tAgCalBase.setYearMark("1");
                mCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.
                                                  getStartDate());
                mCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
        }
//        mCalIndex.tAgCalBase.setAreaType(this.mManageCom.substring(0,4));
        System.out.println("beginDate:" + this.mIndexCalNo + ",endDate:");
        return true;
    }

    private void addAgCalBase(Calculator cCal)
    {
        cCal.addBasicFactor("AgentCode", mCalIndex.tAgCalBase.getAgentCode());
        cCal.addBasicFactor("WageNo", mCalIndex.tAgCalBase.getWageNo());
        cCal.addBasicFactor("WageCode", mCalIndex.tAgCalBase.getWageCode());
        cCal.addBasicFactor("AgentGrade", mCalIndex.tAgCalBase.getAgentGrade());
        cCal.addBasicFactor("AgentGroup", mCalIndex.tAgCalBase.getAgentGroup());
        cCal.addBasicFactor("MonthBegin", mCalIndex.tAgCalBase.getMonthBegin());
        cCal.addBasicFactor("MonthEnd", mCalIndex.tAgCalBase.getMonthEnd());
        cCal.addBasicFactor("QuauterBegin",
                            mCalIndex.tAgCalBase.getQuauterBegin());
        cCal.addBasicFactor("QuauterEnd", mCalIndex.tAgCalBase.getQuauterEnd());
        cCal.addBasicFactor("HalfYearBegin",
                            mCalIndex.tAgCalBase.getHalfYearBegin());
        cCal.addBasicFactor("HalfYearEnd", mCalIndex.tAgCalBase.getHalfYearEnd());
        cCal.addBasicFactor("YearBegin", mCalIndex.tAgCalBase.getYearBegin());
        cCal.addBasicFactor("YearEnd", mCalIndex.tAgCalBase.getYearEnd());
        cCal.addBasicFactor("MonthMark", mCalIndex.tAgCalBase.getMonthMark());
        cCal.addBasicFactor("QuauterMark", mCalIndex.tAgCalBase.getQuauterMark());
        cCal.addBasicFactor("HalfYearMark",
                            mCalIndex.tAgCalBase.getHalfYearMark());
        cCal.addBasicFactor("YearMark", mCalIndex.tAgCalBase.getYearMark());
        cCal.addBasicFactor("AreaType", mCalIndex.tAgCalBase.getAreaType());
        cCal.addBasicFactor("TempBegin", mCalIndex.tAgCalBase.getTempBegin());
        cCal.addBasicFactor("TempEnd", mCalIndex.tAgCalBase.getTempEnd());
        cCal.addBasicFactor("Rate", String.valueOf(mCalIndex.tAgCalBase.getRate()));
        cCal.addBasicFactor("A1State",
                            String.valueOf(mCalIndex.tAgCalBase.getA1State()));
        cCal.addBasicFactor("AssessType", mCalIndex.tAgCalBase.getAssessType());
        cCal.addBasicFactor("LimitPeriod", mCalIndex.tAgCalBase.getLimitPeriod());
        cCal.addBasicFactor("DestAgentGrade",
                            mCalIndex.tAgCalBase.getDestAgentGrade());
        cCal.addBasicFactor("BranchAttr", mCalIndex.tAgCalBase.getBranchAttr());
        cCal.addBasicFactor("FirstAssessMark",
                            String.valueOf(mCalIndex.tAgCalBase.FirstAssessMark));
        System.out.println("考核开始前奏：" + mCalIndex.tAgCalBase.getAgentGrade());
    }

    /**
     * 函数功能：设置各个职级人员晋升考核起期
     */
    private boolean setPromoteBegin(AgCalBase cAgCalBase, String cAgentCode,
                                    String cGrade)
    {
        FDate fDate = new FDate();
        String tEndDate = cAgCalBase.getTempEnd();
        int assessInv = cAgCalBase.AssessInv; //考核期限
        String tSql = "", tbeginDate = "";
        ExeSQL tExeSQL;
        //只针对晋升考核
        if (!cAgCalBase.getAssessType().equals("02"))
        {
            return true;
        }
        if (cGrade.equals("A01"))
        {
            //修改：对A01满9个月以上问题的处理
            //修改时间：2004-03-23
            //修改人：LL

            //对A01不管是否为降级人员，晋升考核情况都一样处理
            //对于 <= 9个月 ：按3、6、9情况处理
            //对于 >  9个月 ：取最近9个月的业绩
            System.out.println("欢迎进入A01晋升设置程序！");
            //获得职级起聘日期
            String tValue = getPostBeginDate(cAgentCode);
            //获得 职级起聘日期 到 考核止期 之间间隔月数
            int tIntv = this.calMonths(tValue, tEndDate);
            if (tIntv == -100)
            {
                return false;
            }
            System.out.println("起止期间隔月数:" + tIntv);
            System.out.println("职级起聘日期为:" + tValue);

            //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
            String tDate = "";
            tDate = this.getBeginDate(cGrade, "02", tIntv, tValue,
                                      cAgCalBase.getTempEnd());
            System.out.println("调用SetAssBeginDate计算结果为：" + tDate);

            //校验计算结果，计算规则如下：
            //0   - 表示程序处理过程中出错
            //1   - 表示不参加本次考核
            //日期 - 表示要设置的考核起期
            if (tDate.equals("0"))
            {
                return false;
            }
            else if (tDate.equals("1"))
            {
                return true;
            }
            else
            {
                //设置考核统计起期
                cAgCalBase.setTempBegin(tDate);
            }

            //计算统计起期与考核止期之间的间隔月数
            System.out.println("设置后的考核统计起期为：" + cAgCalBase.getTempBegin());
            int tIntv1 = calMonths(cAgCalBase.getTempBegin(), tEndDate);
            System.out.println("统计起期与考核止期之间的间隔月数：" + tIntv1);

            //设置考核比例
            cAgCalBase.setRate("1");

            //根据间隔月数，调用设置LAAssessPeriod函数设置LAAssessPeriod的值
            String LAAssessPeriod1 = getAssLimitPeriod(tIntv1, cGrade, "02");
            System.out.println("LimitPeriod为：" + LAAssessPeriod1);
            cAgCalBase.setLimitPeriod(LAAssessPeriod1);
        }
        if (cGrade.equals("A02") || cGrade.equals("A03"))
        {
            //获得 职级起聘日期 到 考核止期 之间间隔月数
            System.out.println("欢迎进入A02-A03晋升设置程序！");
            //获得职级起聘日期
            String tValue = getPostBeginDate(cAgentCode);
            //获得 职级起聘日期 到 考核止期 之间间隔月数
            int tIntv = this.calMonths(tValue, tEndDate);
            if (tIntv == -100)
            {
                return false;
            }
            System.out.println("起止期间隔月数:" + tIntv);
            System.out.println("职级起聘日期为:" + tValue);

            //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
            String tDate = "";
            tDate = this.getBeginDate(cGrade, "02", tIntv, tValue,
                                      cAgCalBase.getTempEnd());
            System.out.println("调用SetAssBeginDate计算结果为：" + tDate);

            //校验计算结果，计算规则如下：
            //0   - 表示程序处理过程中出错
            //1   - 表示不参加本次考核
            //日期 - 表示要设置的考核起期
            if (tDate.equals("0"))
            {
                return false;
            }
            else if (tDate.equals("1"))
            {
                return true;
            }
            else
            {
                //设置考核统计起期
                cAgCalBase.setTempBegin(tDate);
            }

            //计算统计起期与考核止期之间的间隔月数
            System.out.println("设置后的考核统计起期为：" + cAgCalBase.getTempBegin());
            int tIntv1 = calMonths(cAgCalBase.getTempBegin(), tEndDate);
            System.out.println("统计起期与考核止期之间的间隔月数：" + tIntv1);

            //设置考核比例
            cAgCalBase.setRate("1");

            //根据间隔月数，调用设置LAAssessPeriod函数设置LAAssessPeriod的值
            String LAAssessPeriod1 = getAssLimitPeriod(tIntv1, cGrade, "02");
            System.out.println("LimitPeriod为：" + LAAssessPeriod1);
            cAgCalBase.setLimitPeriod(LAAssessPeriod1);
        }
        else if (cGrade.equals("A04") || cGrade.equals("A05") ||
                 cGrade.equals("A06") || cGrade.equals("A07")) //业务经理一级
        {
            //同业衔接人员第一次考核
            if (this.mTYMark.equals("1") && this.mFirstAssessMark.equals("1"))
            {
                System.out.println("欢迎进入同业衔接主管（A04-A07）第一次晋升考核处理！");
                //修改：2004-07-27 LL
                //修改原因：如果从入司时间到考核期满6个月则前推6个月，不满则取入司日期
                String tEmpDate = this.getEmpDate(mAgentCode);
                if (tEmpDate.equals("0")) //查询入司日期有错
                {
                    return false;
                }
                System.out.println("代理人" + mAgentCode + "入司日期为：" + tEmpDate);

                int count = calMonths(tEmpDate, tEndDate);
                if (count == -100)
                {
                    return false;
                }

                //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
                String tDate = "";
                tDate = this.getBeginDate(cGrade, "02", count, tEmpDate,
                                          cAgCalBase.getTempEnd());
                System.out.println("调用SetAssBeginDate计算结果为：" + tDate);

                //校验计算结果，计算规则如下：
                //0   - 表示程序处理过程中出错
                //1   - 表示不参加本次考核
                //日期 - 表示要设置的考核起期
                if (tDate.equals("0"))
                {
                    return false;
                }
                else if (tDate.equals("1"))
                {
                    return true;
                }
                else
                {
                    //设置考核统计起期
                    cAgCalBase.setTempBegin(tDate);
                }

                //计算统计起期与考核止期之间的间隔月数
                System.out.println("设置后的考核统计起期为：" + cAgCalBase.getTempBegin());
                int tIntv1 = calMonths(cAgCalBase.getTempBegin(), tEndDate);
                System.out.println("统计起期与考核止期之间的间隔月数：" + tIntv1);

                //设置考核比例
                cAgCalBase.setRate("1");

                //根据间隔月数，调用设置LAAssessPeriod函数设置LAAssessPeriod的值
                String LAAssessPeriod1 = getAssLimitPeriod(tIntv1, cGrade, "02");
                System.out.println("LimitPeriod为：" + LAAssessPeriod1);
                cAgCalBase.setLimitPeriod(LAAssessPeriod1);
            }
            else
            {
                System.out.println("欢迎进入A04-A07非第一次晋升考核设置程序！");
                //如果不是第一次考核 ，则判断任职日期 与 tEndDate+1 之间的时间间隔
                //是否大于6来判断，如果 >= 6 就用 {（tEndDate+1）-6个月} 作为起期
                //如果 < 6 则用任职日期作为起期
                //获得职级起聘日期
                String tValue = getPostBeginDate(cAgentCode);
                //获得 职级起聘日期 到 考核止期 之间间隔月数
                int tIntv = this.calMonths(tValue, tEndDate);
                if (tIntv == -100)
                {
                    return false;
                }
                System.out.println("起止期间隔月数:" + tIntv);
                System.out.println("职级起聘日期为:" + tValue);

                //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
                String tDate = "";
                tDate = this.getBeginDate(cGrade, "02", tIntv, tValue,
                                          cAgCalBase.getTempEnd());
                System.out.println("调用SetAssBeginDate计算结果为：" + tDate);

                //校验计算结果，计算规则如下：
                //0   - 表示程序处理过程中出错
                //1   - 表示不参加本次考核
                //日期 - 表示要设置的考核起期
                if (tDate.equals("0"))
                {
                    return false;
                }
                else if (tDate.equals("1"))
                {
                    return true;
                }
                else
                {
                    //设置考核统计起期
                    cAgCalBase.setTempBegin(tDate);
                }

                //计算统计起期与考核止期之间的间隔月数
                System.out.println("设置后的考核统计起期为：" + cAgCalBase.getTempBegin());
                int tIntv1 = calMonths(cAgCalBase.getTempBegin(), tEndDate);
                System.out.println("统计起期与考核止期之间的间隔月数：" + tIntv1);

                //设置考核比例
                cAgCalBase.setRate("1");

                //根据间隔月数，调用设置LAAssessPeriod函数设置LAAssessPeriod的值
                String LAAssessPeriod1 = getAssLimitPeriod(tIntv1, cGrade, "02");
                System.out.println("LimitPeriod为：" + LAAssessPeriod1);
                cAgCalBase.setLimitPeriod(LAAssessPeriod1);
            }
        }
        else if (cGrade.equals("A08")) //督导长
        {
            //同业衔接主管第一次考核
            if (this.mTYMark.equals("1") && this.mFirstAssessMark.equals("1"))
            {
                //修改：2004-07-19 LL
                //在对A07做第一次晋升考核时，如果从入司日期到考核止期大于等于12个月，
                //则前推12个月，如果不满12个月则直接取入司日期
                //对A08的话直接前推12个月
                System.out.println("进入同业衔接主管A08第一次晋升考核处理！");
                //获得入司日期
                String tEmpDate = this.getEmpDate(mAgentCode);
                if (tEmpDate.equals("0")) //查询入司日期有错
                {
                    return false;
                }
                System.out.println("代理人" + mAgentCode + "入司日期为：" + tEmpDate);

                int count = calMonths(tEmpDate, tEndDate);
                if (count == -100)
                {
                    return false;
                }

                //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
                String tDate = "";
                tDate = this.getBeginDate(cGrade, "02", count, tEmpDate,
                                          cAgCalBase.getTempEnd());
                System.out.println("调用SetAssBeginDate计算结果为：" + tDate);

                //校验计算结果，计算规则如下：
                //0   - 表示程序处理过程中出错
                //1   - 表示不参加本次考核
                //日期 - 表示要设置的考核起期
                if (tDate.equals("0"))
                {
                    return false;
                }
                else if (tDate.equals("1"))
                {
                    return true;
                }
                else
                {
                    //设置考核统计起期
                    cAgCalBase.setTempBegin(tDate);
                }

                //设置考核比例
                cAgCalBase.setRate("1");

                //计算统计起期与考核止期之间的间隔月数
                System.out.println("设置后的考核统计起期为：" + cAgCalBase.getTempBegin());
                int tIntv1 = calMonths(cAgCalBase.getTempBegin(), tEndDate);
                System.out.println("统计起期与考核止期之间的间隔月数：" + tIntv1);

                //根据间隔月数，调用设置LAAssessPeriod函数设置LAAssessPeriod的值
                String LAAssessPeriod1 = getAssLimitPeriod(tIntv1, cGrade, "02");
                System.out.println("LimitPeriod为：" + LAAssessPeriod1);
                cAgCalBase.setLimitPeriod(LAAssessPeriod1);
            }
            else
            {
                System.out.println("欢迎进入A08非第一次考核处理模块！");
                //获得职级起聘日期
                String tValue = getPostBeginDate(cAgentCode);
                //获得 职级起聘日期 到 考核止期 之间间隔月数
                int tIntv = this.calMonths(tValue, tEndDate);
                if (tIntv == -100)
                {
                    return false;
                }
                System.out.println("起止期间隔月数:" + tIntv);
                System.out.println("职级起聘日期为:" + tValue);

                //在此通过相关参数的设置，调用SetAssBeginDate（）函数计算统计起期
                String tDate = "";
                tDate = this.getBeginDate(cGrade, "02", tIntv, tValue,
                                          cAgCalBase.getTempEnd());
                System.out.println("调用SetAssBeginDate计算结果为：" + tDate);

                //校验计算结果，计算规则如下：
                //0   - 表示程序处理过程中出错
                //1   - 表示不参加本次考核
                //日期 - 表示要设置的考核起期
                if (tDate.equals("0"))
                {
                    return false;
                }
                else if (tDate.equals("1"))
                {
                    return true;
                }
                else
                {
                    //设置考核统计起期
                    cAgCalBase.setTempBegin(tDate);
                }

                //计算统计起期与考核止期之间的间隔月数
                System.out.println("设置后的考核统计起期为：" + cAgCalBase.getTempBegin());
                int tIntv1 = calMonths(cAgCalBase.getTempBegin(), tEndDate);
                System.out.println("统计起期与考核止期之间的间隔月数：" + tIntv1);

                //设置考核比例
                cAgCalBase.setRate("1");

                //根据间隔月数，调用设置LAAssessPeriod函数设置LAAssessPeriod的值
                String LAAssessPeriod1 = getAssLimitPeriod(tIntv1, cGrade, "02");
                System.out.println("LimitPeriod为：" + LAAssessPeriod1);
                cAgCalBase.setLimitPeriod(LAAssessPeriod1);
            }
        }

//    System.out.println("limitPeriod:"+tCode);
//    cAgCalBase.setLimitPeriod(tCode);
        return true;
    }

    /**
     * 功能：执行优先考核函数，例如：对A02,A03是否连续3个月挂零进行优先判断，
     * 如果满足条件，则直接对此人做清退处理，不用进行下面的考核
     * 返回值说明： 1 - 不清退
     *            2 - 清退
     *            0 - 处理过程中有错误
     */
    private int execFirstAssess(String cAgentGrade, String cAgentCode,
                                String cAssessCode)
    {
        //cAssessCode只要是该职级的考核代码即可，目的为得到表名和字段名
        String tReturn = "";
        LAFirstAssessDB tLAFirstAssessDB = new LAFirstAssessDB();
        tLAFirstAssessDB.setAgentGrade(cAgentGrade);
        LAFirstAssessSet tLAFirstAssessSet = new LAFirstAssessSet();
        tLAFirstAssessSet = tLAFirstAssessDB.query();
        LAFirstAssessSchema tLAFirstAssessSchema;
        Calculator tCal;
        for (int i = 1; i <= tLAFirstAssessSet.size(); i++)
        {
            tCal = new Calculator();
            tLAFirstAssessSchema = tLAFirstAssessSet.get(i);
            //查询指标信息
            LAAssessIndexDB tLAAssessIndexDB = new LAAssessIndexDB();
            /*Lis5.3 upgrade get
             tLAAssessIndexDB.setIndexCode(tLAFirstAssessSchema.getCalCode());
             */
            if (!tLAAssessIndexDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAAssessIndexDB.mErrors);
                CError tCError = new CError();
                tCError.moduleName = "CalAssess";
                tCError.functionName = "execFirstAssess()";
                tCError.errorMessage = "优先考核对应的指标信息出错！";
                this.mErrors.addOneError(tCError);
                return 0;
            }
            mCalIndex.setAssessIndex(tLAAssessIndexDB);
            mCalIndex.setAgentCode(cAgentCode);
            System.out.println("优先考核代理人代码：" + cAgentCode);
            mCalIndex.setIndexCalNo(mIndexCalNo);
            mCalIndex.setOperator(mOperator);
            mCalIndex.setIndexCalPrpty("01");
            mCalIndex.tAgCalBase.setAgentCode(cAgentCode);
            /*************************************/
            //修改程序-ll：此处在设置起期的时候用的是月起期

            mCalIndex.tAgCalBase.setTempBegin(mCalIndex.tAgCalBase.
                                              getMonthBegin());
            /*************************************/
            mCalIndex.tAgCalBase.setTempEnd(mCalIndex.tAgCalBase.getMonthEnd());
            mCalIndex.tAgCalBase.setAssessType("01"); //维持考核
            tReturn = mCalIndex.Calculate();
            /*
                    tCal.setCalCode(tLAFirstAssessSchema.getCalCode());
                    tCal.addBasicFactor("AgentCode",cAgentCode);
                    tCal.addBasicFactor("WageNo",this.IndexCalNo);
             tCal.addBasicFactor("MonthBegin",mCalIndex.tAgCalBase.getMonthBegin());
             tCal.addBasicFactor("MonthEnd",mCalIndex.tAgCalBase.getMonthEnd());
                    tCal.addBasicFactor("Operator",this.Operator);
                    tReturn = tCal.calculate();*/
            if (tReturn.equals("0"))
            {
                CError tCError = new CError();
                tCError.moduleName = "CalAssess";
                tCError.functionName = "execFirstAssess()";
                tCError.errorMessage = "计算优先考核出错！";
                this.mErrors.addOneError(tCError);
                return 0;
            }
        }
        //插入考核信息表
        if (tReturn.equals("10")) //存储过程有两种返回值‘10’和'11'(不满足清退条件的结果)
        {
            if (!insertAssess(mIndexCalNo, cAgentCode, mAgentGrade, cAssessCode,
                              "00"))
            {
                System.out.println("插入考评信息表出错！！");
                return 0;
            }
            return 2; //清退
        }
        return 1; //不清退
    }

    /**
     * 根据LAAssess表中的FirstAssessFlag字段判断是否已作过第一次考核
     * 第一次考核取出考核的打折比率
     * 非第一次考核则取出上次考核维持的止起
     * 在此会处理五种情况
     * 1-是同业衔接主管，没做过第一次考核
     * 2-是同业衔接主管，做过第一次考核
     * 3-不是同业衔接主管，做过第一次考核
     * 4-不是同业衔接主管，没做过第一次考核，但做过其他考核（如手工考核）
     * 5-不是同业衔接主管，没做过第一次考核，也没做过其他任何考核
     */
    private boolean IsFirstAssess(String cAgentGrade, String cAgentCode)
    {
        //修改：2004-05-11 LL
        //修改原因：对主管增加一个是否为同业衔接主管的判断
        //1-判断此人是不是同业衔接主管
        //0-程序有错；1-是同业衔接主管；2-不是同业衔接主管
        String tFlag = this.isLinkPeriodMan(cAgentCode);
        System.out.println("调用是否同业衔接人员判断函数返回的结果：" + tFlag);
        if (tFlag.equals("0"))
        {
            return false; //有错，直接返回
        }
        //设置是否同业衔接主管标志位
        else if (tFlag.equals("1"))
        {
            this.mTYMark = "1"; //是同业衔接只管
        }
        else if (tFlag.equals("2"))
        {
            this.mTYMark = "0"; //不是同业衔接主管
        }

        //2-判断此人是否做过第一次考核
        //修改：2004-09-08　LL
        //修改原因：在此去掉管理机构的限制条件，避免出现在某个机构下作过第一次考核，
        //到了另外一个机构下再做第一次考核情况
        String tSQL = "select Count(*) from laassess where agentCode = '" +
                      cAgentCode + "' "
                      + "And FirstAssessFlag = '1' ";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount = tExeSQL.getOneValue(tSQL);

        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "CalAssess";
            tCError.functionName = "IsFirstAssess()";
            tCError.errorMessage = "查询代理人" + cAgentCode + "是否为第一次考核出错！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        //1 - 如果是第一次考核则在LALinkAssess表中取出 筹备期、打折比率进行相关设置
        //在此必须满足两个条件才算做第一次考核
        //1-是同业衔接主管
        //2-没有做过第一次考核
        if (Integer.parseInt(tCount) <= 0 && tFlag.equals("1"))
        {
            System.out.println("代理人" + cAgentCode + "满足第一次考核判断");
            //设置第一次考核标志位
            this.mFirstAssessMark = "1"; //第一次考核
            mCalIndex.tAgCalBase.FirstAssessMark = 1;
            //修改：2004-07-30 LL
            //修改原因：新人新办法，老人老办法修改，查询同业衔接人员时增加了AreaType字段
            String tStr = "select calassareatype('" + cAgentCode +
                          "') from ldsysvar where sysvar = 'onerow'";
            ExeSQL t = new ExeSQL();
            System.out.println("新-老人查询地区类型SQL：" + tStr);
            String tResult = t.getOneValue(tStr); //存放地区类型
            System.out.println("查询地区类型结果为:" + tResult);

            //修改：2004-07-27 LL
            //修改原因：查询考核比例时，管理机构只能是6位
            //查询该职级的打折比率和筹备期月数
            tSQL = "SELECT ArrangePeriod,AssessRate FROM LALinkAssess "
                   + "WHERE ManageCom = '" + mManageCom.substring(0, 6) +
                   "' AND AgentGrade = '" + cAgentGrade + "'"
                   + " and areatype = '" + tResult + "' ";
            System.out.println("查询ArrangePeriod,AssessRate:" + tSQL);
            tExeSQL = new ExeSQL();

            SSRS tSSRS = tExeSQL.execSQL(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tCError = new CError();
                tCError.moduleName = "CalAssess";
                tCError.functionName = "IsFirstAssess()";
                tCError.errorMessage = "查询职级" + mAgentGrade + "的打折比率和筹备期出错！";
                this.mErrors.addOneError(tCError);
                return false;
            }
            if (tSSRS.getMaxRow() == 1)
            {
                String arr[][] = tSSRS.getAllData();
                this.mPreparePeriod = Integer.parseInt(arr[0][0]);
                this.mFirstRate = arr[0][1];
                String assessPeriod = "";
                assessPeriod = getAssessIntv(cAgentGrade, "01");

                this.mPreparePeriod = mPreparePeriod +
                                      Integer.parseInt(assessPeriod);
                System.out.println("筹备期界限（筹备期 + 考核期）：" + this.mPreparePeriod);
                /*
                         //查询是否有手工录入的考核
                         //--有则之后的考核不给筹备期
                 tSQL = "SELECT Count(*) FROM LAAssess WHERE ManageCom = '"+mManageCom+"' "
                      +"AND AgentCode = '"+cAgentCode+"'";
                                 tExeSQL = new ExeSQL();
                                 tCount = tExeSQL.getOneValue(tSQL);
                                 if (tExeSQL.mErrors.needDealError())
                                 {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tCError = new CError();
                    tCError.moduleName = "CalAssess";
                    tCError.functionName = "IsFirstAssess()";
                    tCError.errorMessage = "查询代理人"+cAgentCode+"的考核记录出错！";
                    this.mErrors.addOneError(tCError);
                    return false;
                                 }
                                 if(tCount.equals("1"))
                                 {
                         //从考核止期向前推 考核期+半个月 筹备期设为零
                    this.mPreparePeriod = assessPeriod;
                    System.out.println("筹备期界限："+this.mPreparePeriod);
                                 }else
                                 {
                         //从考核止期向前推 考核期+筹备期+半个月
                    this.mPreparePeriod = mPreparePeriod + assessPeriod;
                    System.out.println("筹备期界限："+this.mPreparePeriod);
                                 }*/
            }
            else
            {
                CError tError = new CError();
                tError.moduleName = "CalAssessBL";
                tError.functionName = "IsFirstAssess";
                tError.errorMessage = "没有维护职级" + cAgentGrade + "的衔接考核数据！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //2 - 如果不是第一次考核
        else
        {
            //修改：2004-05-18 LL
            //在此增加一种判断情况：当操作者通过行政信息维护界面修改某人职级时
            //会出现如下情况：某人入司时为A03，后来通过界面把职级改为了A04，此时
            //此人的状态为：1-此人不算同业衔接人员；2-此人在laassess表中不会有任何记录
            //（假设此人以前没有作过任何考核）；
            //在此采用的处理方法是对mLastEndDate赋值为""，在setBaseValue函数中再对
            //此标志为进行一次判断设置相应的值；
            //在次增加对情况：5-不是同业衔接主管，没做过第一次考核，也没做过其他任何考核 的处理

            //取出有效的计算编码
            //在这里要区分出 0-手工录入 和 1-正常考核 两种情况
            //在此取出最近一次的考核期
            tSQL =
                    "SELECT a.IndexCalNo,TRIM(a.StandAssessFlag) FROM LAAssess a "
                    + "WHERE a.agentcode = '" + cAgentCode +
                    "' and rownum = 1 "
                    +
                    "AND ((a.standassessflag = '0') OR (a.standassessflag = '1' "
                    +
                    "AND EXISTS (SELECT * FROM LAIndexInfo WHERE indexcalno = a.indexcalno "
                    + "AND indextype = '02' AND AgentCode = '" + cAgentCode +
                    "'))) "
                    + "order by a.indexcalno desc";
            tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tCError = new CError();
                tCError.moduleName = "CalAssess";
                tCError.functionName = "IsFirstAssess()";
                tCError.errorMessage = "查询代理人" + cAgentCode + "上次考核维持的考核代码出错！";
                this.mErrors.addOneError(tCError);
                return false;
            }
            if (tSSRS.getMaxRow() == 1)
            {
                String arr[][] = tSSRS.getAllData();
                String tCalNo = arr[0][0];
                String tStandFlag = arr[0][1];
                //  0 - 非正常考核
                if (tStandFlag.equals("0")) //非正常考核
                {
                    //取上次考核止期 -- 根据LAAssess表中的最大的IndexCalNo去LAStatSegment表中查询核
                    //用考核年月代码去取出某个时间段的止期
                    tSQL =
                            "SELECT to_char(EndDate,'YYYY-MM-DD') FROM LAStatSegment "
                            + "Where StatType = '1' AND trim(YearMonth) = '" +
                            tCalNo + "'";
                }
                else //正查考核
                {
                    //在LAIndexInfo表中取出 统计止期StartEnd
                    tSQL =
                            "select to_char(StartEnd,'yyyy-mm-dd') from laIndexInfo "
                            + "where indexType = '02' and agentCode = '" +
                            cAgentCode + "' "
                            + "and indexCalno = '" + tCalNo + "'"; //02:维持考核
                }
                tExeSQL = new ExeSQL();
                mLastEndDate = tExeSQL.getOneValue(tSQL);
                System.out.println("最近一次考核止期为：" + mLastEndDate);

                if (tExeSQL.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tCError = new CError();
                    tCError.moduleName = "CalAssess";
                    tCError.functionName = "IsFirstAssess()";
                    tCError.errorMessage = "查询代理人" + cAgentCode + "上次考核的止期出错！";
                    this.mErrors.addOneError(tCError);
                    return false;
                }
            }
            //增加对：5-不是同业衔接主管，没做过第一次考核，也没做过其他任何考核 的处理
            //在此没有查出任何记录表示没有做过任何考核；
            else
            {
                //把mLastEndDate设置为""，在setBaseValue函数中会判断此情况
                System.out.println("代理人" + cAgentCode + "不是同业衔接主管，也没做过其他任何考核");
                mLastEndDate = "";
//          CError tError = new CError();
//          tError.moduleName = "CalAssess";
//          tError.functionName = "IsFirstAssess()";
//          tError.errorMessage = "查询上次考核年月代码出错！";
//          this.mErrors.addOneError(tError);
//          return false;
            }
        }
        System.out.println("上次考核止期：" + this.mLastEndDate);
        return true;
    }

    /**
     * 取得代理人当前职级的起聘日期
     */
    private String getPostBeginDate(String cAgentCode)
    {
        //取该督导长起聘日期，判断其任职月数
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(cAgentCode);
        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "CalAssess";
            tCError.functionName = "getLastAssessDate()";
            tCError.errorMessage = "取代理人" + cAgentCode + "当前职级的起聘日期出错！";
            this.mErrors.addOneError(tCError);
            return null;
        }
        String tStartDate = tLATreeDB.getStartDate().trim();
        if (tStartDate == null || tStartDate.equals(""))
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "CalAssess";
            tCError.functionName = "getLastAssessDate()";
            tCError.errorMessage = "取代理人" + cAgentCode + "当前职级的起聘日期为空！";
            this.mErrors.addOneError(tCError);
            return null;
        }
        return tStartDate;
    }

    //取得代理人当前职级的第二次考核起期
    private String getSecondBeginDate(String cAgentCode)
    {
        String tStartDate = "";
        String tSQL = "";
        tSQL =
                "select to_char(StartEnd,'yyyy-mm-dd') from laIndexInfo where indexType = '02' "
                + "and agentCode = '" + cAgentCode +
                "' and rownum = 1 order by indexcalno asc"; //02:维持考核
        ExeSQL tExeSQL = new ExeSQL();
        tStartDate = tExeSQL.getOneValue(tSQL);
        if (tStartDate == null || tStartDate.equals("") ||
            tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "CalAssess";
            tCError.functionName = "getLastAssessDate()";
            tCError.errorMessage = "取代理人第二次考核起期出错！";
            this.mErrors.addOneError(tCError);
            return "";
        }
        FDate fDate = new FDate();
        tStartDate = fDate.getString(PubFun.calDate(fDate.getDate(tStartDate),
                1, "D", null));

        return tStartDate;
    }

    /**
     * 计算两个日期之间的月数，16日之后的不算作一个月
     * ll修改-15号改为10号
     */
    public int calMonths(String cBeginDate, String cEndDate)
    {
        FDate fDate = new FDate();
        int monthIntv = 0;
        int dayInv = 0;
        try
        {
            ExeSQL tExeSQL = new ExeSQL();
            String tSql =
                    "select trim(varvalue) from lasysvar where vartype = 'EmployLimit'";
            String tValue = tExeSQL.getOneValue(tSql);
            System.out.println("查询特殊日子SQL:" + tSql);
            System.out.println("查询特殊日子:" + tValue);
            monthIntv = PubFun.calInterval(cBeginDate,
                                           fDate.getString(PubFun.calDate(fDate.
                    getDate(cEndDate), 1, "D", null)), "M");
//            dayInv = PubFun.calInterval(cBeginDate,cBeginDate.substring(0,cBeginDate.lastIndexOf("-"))+"-15","D");
            dayInv = PubFun.calInterval(cBeginDate,
                                        cBeginDate.substring(0,
                    cBeginDate.lastIndexOf("-")) + "-" + tValue, "D");
            if (dayInv >= 0 &&
                !cBeginDate.substring(cBeginDate.length() - 2).equals("01")) //若为15日以前入司，则到考核期，入司该月也算作一个月
            {
                monthIntv = monthIntv + 1;
            }
            System.out.println("monthIntv:" + monthIntv);
        }
        catch (Exception ex)
        {
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "calMonths";
            tError.errorMessage = "计算起期" + cBeginDate + "与止期" + cEndDate +
                                  "的月数出错!";
            this.mErrors.addOneError(tError);
            return -100;
        }
        return monthIntv;
    }

    /**
     * 修改日期：2004-08-26
     * 获取输入职级的考核期
     */
    public String getAssessIntv(String cAgentGrade, String cAssessType)
    {
        ExeSQL tExtSQL = new ExeSQL();
        String tSql1 = "select AssessMonths from LAAssessPeriod where "
                       + " AgentGrade = '" + cAgentGrade +
                       "' and AssessType = '" + cAssessType
                       + "' and AreaType = '" + this.mAreaType +
                       "' and BranchType ='" + this.mBranchType + "'";
        System.out.println("查询出考核期月数SQL为：" + tSql1);
        String tValue = tExtSQL.getOneValue(tSql1);

        return tValue;
    }

    /**
     *将本次考核的职级人数存于LAAssessMain表中
     */
    private boolean addLAAssessMain()
    {
        int iCount = this.mLAAssessSet.size();
        //准备数据
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        this.mLAAssessMainSchema.setAgentGrade(mAgentGrade);
        this.mLAAssessMainSchema.setManageCom(mManageCom);
        this.mLAAssessMainSchema.setBranchType(mBranchType);
        this.mLAAssessMainSchema.setIndexCalNo(mIndexCalNo);
        this.mLAAssessMainSchema.setState("0"); //暂时
        this.mLAAssessMainSchema.setAssessCount(iCount);
        this.mLAAssessMainSchema.setBranchAttr(mCalIndex.tAgCalBase.
                                               getZSGroupBranchAttr());
//        this.mLAAssessMainSchema.setConfirmCount("");
        this.mLAAssessMainSchema.setMakeDate(currentDate);
        this.mLAAssessMainSchema.setMakeTime(currentTime);
        this.mLAAssessMainSchema.setModifyDate(currentDate);
        this.mLAAssessMainSchema.setModifyTime(currentTime);
        this.mLAAssessMainSchema.setOperator(mOperator);

        return true;
    }

    /**
     * 功能：判断给定日期与某个指定日期之间的关系
     * 返回值说明：
     *     0 - 给定日期在某个指定日期之前
     *     1 - 给定日期在某个指定日期之后
     *     2 - 错误
     */
    private String judgeDate(String tDate)
    {
        //取出某个特定日期
        ExeSQL tExeSQL = new ExeSQL();
        String tSql =
                "select trim(varvalue) from lasysvar where vartype = 'EmployLimit'";
        String tResult = tExeSQL.getOneValue(tSql);

        //校验是否出错
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssessBL";
            tError.functionName = "judgeDate";
            tError.errorMessage = "查询某月某个日期信息出错！";
            this.mErrors.addOneError(tError);
            System.out.println("查询某月某个日期信息出错！");
            return "2";
        }
        //校验是否取出比较日期
        if (tResult == null || tResult.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "CalAssessBL";
            tError.functionName = "judgeDate";
            tError.errorMessage = "查询某月某个日期信息出错,没有在数据库中取出这个日期！";
            this.mErrors.addOneError(tError);
            System.out.println("查询某月某个日期信息出错,没有在数据库中取出这个日期！");
            return "2";
        }
        //构造出比较日期
        String comDate = tDate.substring(0, 8) + tResult;
        //判断是否超过某个日期
        if (tDate.compareTo(comDate) > 0)
        {
            return "1";
        }
        else
        {
            return "0";
        }
    }

    /**
     * 新增：2004-05-09 LL
     * 判断某业务员是否为同业衔接主管
     * 返回值说明：
     * 0 - 程序中出错
     * 1 - 是同业衔接主管
     * 2 - 不是同业衔接主管
     */
    private String isLinkPeriodMan(String tAgentCode)
    {
        System.out.println("进入同业衔接主管判断程序");
        //1 - 判断当前职级是否为A04以上职级
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(tAgentCode);

        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAssessinputBL";
            tError.functionName = "isLinkPeriodMan";
            tError.errorMessage = "代理人" + tAgentCode + "查询LATree表职级信息出错！";
            this.mErrors.addOneError(tError);
            return "0";
        }
        //当前职级小于A04则不是同业衔接主管
        System.out.println("代理人" + tAgentCode + "当前职级为" +
                           tLATreeDB.getAgentGrade());
        if (tLATreeDB.getAgentGrade().compareTo("A04") < 0)
        {
            return "2";
        }

        //2 - 判断入司时是否为A04以上职级
        //取得此业务员入司时的职级，如果入司时的职级大于A04，则为同业衔接主管
        //如果入司时的职级，小于A04，后来有手工维护为A04以上职级，则不算同业衔接主管
        String oldAgentGrade = getOldAgentGrade(tAgentCode);
        if (oldAgentGrade.equals("00")) //如果为00则说明此业务员没有发生过职级变动
        {
            oldAgentGrade = tLATreeDB.getAgentGrade();
        }
        System.out.println("代理人" + tAgentCode + "入司时职级为：" + oldAgentGrade);
        // 判断入司时的职级也为A04以上
        if (oldAgentGrade.compareTo("A04") >= 0)
        {
            //查找laagent表中信息,判断是否为同业衔接人员
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);

            if (!tLAAgentDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAssessinputBL";
                tError.functionName = "isLinkPeriodMan";
                tError.errorMessage = "代理人" + tAgentCode + "查询LAAgent表信息出错！";
                this.mErrors.addOneError(tError);
                return "0";
            }
            //判断是否为同业衔接人员
            if (tLAAgentDB.getEmployDate().equals(tLAAgentDB.getInDueFormDate()))
            {
                return "1";
            }
            else
            {
                return "2";
            }
        }
        return "2";
    }

    /**
     * 新增：2004-05-09 LL
     * 在LATree或着LATreeB表中查询某个代理人入司时的职级
     * 返回值说明：
     *  如果有记录则返回级别，如果LATreeB表中没有记录则返回 00
     */
    private String getOldAgentGrade(String tAgentCode)
    {
        //先查LATreeB表中最老的一条备份记录
        LATreeBSet tLATreeBSet = new LATreeBSet();
        LATreeBDB tLATreeBDB = new LATreeBDB();
        String tSql = "select * from LATreeB a where a.agentcode = '" +
                      tAgentCode
                      + "' and RemoveType <> '05' order by EdorNO asc";
        tLATreeBSet = tLATreeBDB.executeQuery(tSql);
        //校验是否出错
        if (tLATreeBDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLATreeBDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBL";
            tError.functionName = "getOldAgentGrade";
            tError.errorMessage = "查询LATreeB表中最老的一条备份记录信息出错！";
            this.mErrors.addOneError(tError);
            System.out.println("查询筹备期信息出错！");
        }
        //如果有记录则返回级别
        if (tLATreeBSet.size() != 0)
        {
            return tLATreeBSet.get(1).getAgentGrade();
        }
        //如果LATreeB表中没有记录则返回 00
        return "00";
    }

    /**
     * 新增：2004-05-12 LL
     * 取出某个人的入司日期
     * 返回值说明：
     *  0-查询出错；tEmpDate-入司日期；
     */
    private String getEmpDate(String tAgentCode)
    {
        //查找laagent表中信息,获得入司日期
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);

        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssessBL";
            tError.functionName = "getEmpDate";
            tError.errorMessage = "代理人" + tAgentCode + "查询LAAgent表信息出错！";
            this.mErrors.addOneError(tError);
            return "0";
        }
        String tEmpDate = tLAAgentDB.getEmployDate();

        return tEmpDate;
    }

    /**
     * 新增：2004-08-25 LL
     * 函数功能：获得考核统计起期函数
     * 参数说明：tAgentGrade - 职级
     *         tAssessType - 考核类型
     *         tAssInv     - 间隔月数
     *         tPostDate   - 起期
     *         tEndDate    - 统计止期
     * 返回值说明：
     *         0 - 表示程序处理过程中出错
     *         1 - 表示不参加本次考核
     *       日期 - 表示要设置的考核起期
     * */
    public String getBeginDate(String tAgentGrade, String tAssessType,
                               int tAssInv, String tPostDate, String tEndDate)
    {
        ExeSQL tExtSQL = new ExeSQL();
        String tDate = "";
        String tSql1 = "select SetAssBeginDate('"
                       + tAgentGrade + "', '" + tAssessType + "', '" +
                       this.mAreaType + "' , '"
                       + this.mBranchType + "', '" + this.mDegradeFlage
                       + "', '" + this.mTYMark + "' , '" +
                       this.mFirstAssessMark
                       + "', " + tAssInv + ", to_date('" + tPostDate +
                       "','yyyy-mm-dd') ,"
                       + "to_date('" + tEndDate + "','yyyy-mm-dd')"
                       + ") from dual ";
        System.out.println("调用SetAssBeginDate计算SQL为：" + tSql1);
        tDate = tExtSQL.getOneValue(tSql1);
        System.out.println("调用SetAssBeginDate计算结果为：" + tDate);

        return tDate;
    }

    /**
     * 新增：2004-08-25 LL
     * 函数功能：获得LimitPeriod
     */
    public String getAssLimitPeriod(int tMonthIntv, String tAgentGrade,
                                    String tAssessType)
    {
        String tSql =
                "select LimitPeriod , to_char(AssessMonths) from LAAssessPeriod where "
                + "AgentGrade = '" + tAgentGrade + "' and AssessType = '" +
                tAssessType + "' "
                + " and AreaType = '" + this.mAreaType + "' and BranchType = '" +
                this.mBranchType
                + "' order by AssessMonths asc";
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("考核期LimitPeriod查询SQL为" + tSql);
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(tSql);
        String tDate[][] = tSSRS.getAllData();

        //如果只查出一条记录则直接返回
        if (tSSRS.getMaxRow() == 1)
        {
            System.out.println("考核期限LimitPeriod为：" + tDate[0][0]);
            return tDate[0][0];
        }

        //当查询出的结果有多条记录时
        for (int i = 0; i < tDate.length; i++)
        {
            if (tMonthIntv <= Integer.parseInt(tDate[i][1]))
            {
                return tDate[i][0];
            }
        }

        return tDate[tDate.length - 1][0];
    }
}
