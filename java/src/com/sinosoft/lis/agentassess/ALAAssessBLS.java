/*
 * <p>ClassName: ALAAssessBLS </p>
 * <p>Description: LAAssessBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-06-21
 */
package com.sinosoft.lis.agentassess;


import java.sql.Connection;

import com.sinosoft.lis.vdb.LAAssessBDBSet;
import com.sinosoft.lis.vdb.LAAssessDBSet;
import com.sinosoft.lis.vdb.LADimissionDBSet;
import com.sinosoft.lis.vdb.LATreeBDBSet;
import com.sinosoft.lis.vdb.LATreeDBSet;
import com.sinosoft.lis.vschema.LAAssessBSet;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class ALAAssessBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public ALAAssessBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;

        System.out.println("Start ALAAssessBLS Submit...");
        tReturn = saveLAAssess(cInputData);
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End ALAAssessBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAssess(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAssessBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            //备份信息
            LAAssessBSet tLAAssessBSet = new LAAssessBSet();
            tLAAssessBSet.set((LAAssessBSet) mInputData.getObjectByObjectName(
                    "LAAssessBSet", 0));
            if (tLAAssessBSet.size() > 0)
            {
                System.out.println("come in ");
                LAAssessBDBSet tLAAssessBDBSet = new LAAssessBDBSet(conn);
                tLAAssessBDBSet.set(tLAAssessBSet);
                if (!tLAAssessBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAssessBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAAssessBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            //行政信息修改
            LAAssessSet tLAAssessSet = new LAAssessSet();
            tLAAssessSet.set((LAAssessSet) mInputData.getObjectByObjectName(
                    "LAAssessSet", 0));
            if (tLAAssessSet.size() > 0)
            {
                System.out.println("come in ");
                LAAssessDBSet tLAAssessDBSet = new LAAssessDBSet(conn);
                tLAAssessDBSet.set(tLAAssessSet);
                if (!tLAAssessDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAssessDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAAssessBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("Start 更新LATreeB表...");
            LATreeBSet tLATreeBSet = new LATreeBSet();
            tLATreeBSet.set((LATreeBSet) mInputData.getObjectByObjectName(
                    "LATreeBSet", 0));
            if (tLATreeBSet.size() > 0)
            {
                System.out.println("come in ");
                LATreeBDBSet tLATreeBDBSet = new LATreeBDBSet(conn);
                tLATreeBDBSet.set(tLATreeBSet);
                if (!tLATreeBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLATreeBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAAssessBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "LATree表更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            System.out.println("Start 更新LATree表...");
            LATreeSet tLATreeSet = new LATreeSet();
            tLATreeSet.set((LATreeSet) mInputData.getObjectByObjectName(
                    "LATreeSet", 0));
            if (tLATreeSet.size() > 0)
            {
                System.out.println("come in ");
                LATreeDBSet tLATreeDBSet = new LATreeDBSet(conn);
                tLATreeDBSet.set(tLATreeSet);
                if (!tLATreeDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLATreeDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAAssessBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "LATree表更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("Start 插入离职信息表...");
            LADimissionSet tLADimissionSet = new LADimissionSet();
            tLADimissionSet.set((LADimissionSet) mInputData.
                                getObjectByObjectName("LADimissionSet", 0));
            if (tLADimissionSet.size() > 0)
            {
                LADimissionDBSet tLADimissionDBSet = new LADimissionDBSet(conn);
                tLADimissionDBSet.set(tLADimissionSet);
                if (!tLADimissionDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLADimissionDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAAssessBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "LATree表更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("begin to update LAAgent's AgentState...");
            String updateSQL = (String) mInputData.getObject(4);
            if (!updateSQL.equals(""))
            {
                ExeSQL tExeSQL = new ExeSQL(conn);
                if (!tExeSQL.execUpdateSQL(updateSQL))
                {
                    CError tError = new CError();
                    tError.moduleName = "ALAAssessBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "更新代理人状态出错！";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAssessBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
