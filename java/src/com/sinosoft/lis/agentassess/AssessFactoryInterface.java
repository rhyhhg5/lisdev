package com.sinosoft.lis.agentassess;

/**
 * <p>Title: 考核抽象工厂接口</p>
 * <p>Description: 考核抽象工厂接口</p>
 * <p>Copyright: Copyright (c) sinosoft 2004</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public interface AssessFactoryInterface
{
    //职级考核
    public GradeAssessPublic createGradeAssesss();

    //职称考核
    public PositionAssessPublic createPositionAssess();
}
