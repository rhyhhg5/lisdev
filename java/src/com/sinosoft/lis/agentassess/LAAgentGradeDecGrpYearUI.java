package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * 团险见习业务经理考核归属
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAAgentGradeDecGrpYearUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSet mLAAssessSet = new LAAssessSet();
    private LAAssessHistorySchema mLAAssessHistorySchema = new LAAssessHistorySchema();
    private String mManageCom = "";
    private String mIndexCalNo = "";

    public static void main(String args[])
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ComCode = "8695";
        tG.ManageCom = "8695";

        LAAssessHistorySchema tLAAssessHistorySchema = new LAAssessHistorySchema();

        tLAAssessHistorySchema.setManageCom("8695");
        tLAAssessHistorySchema.setIndexCalNo("200701");
        tLAAssessHistorySchema.setBranchType("2");
        tLAAssessHistorySchema.setBranchType2("01");

        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tLAAssessHistorySchema);

        LAAgentGradeDecGrpYearUI tLAAgentGradeDecGrpYearUI = new LAAgentGradeDecGrpYearUI();
        if(tLAAgentGradeDecGrpYearUI.submitData(tVData,""))
        {
            System.out.println("成功了！");
        }else
        {
            System.out.println("失败了！");
        }
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = (VData)cInputData.clone();
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        LAAgentGradeDecGrpYearBL tLAAgentGradeDecGrpYearBL = new LAAgentGradeDecGrpYearBL();
        if(!tLAAgentGradeDecGrpYearBL.submitData(mInputData,mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentGradeDecGrpYearBL.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 验证方法
     * @return boolean
     */
    private boolean check()
    {
        return true;
    }

    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAgentGradeDecGrpYearUI.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecGrpYearUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
