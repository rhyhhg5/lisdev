package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.lis.agent.ALADimissionAppUI;
import com.sinosoft.lis.agentassess.LAAssessInputUI;

/**
 * 考核计算回退，
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAAgentAssessBackBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往后面传输数据的容器 ，进行回退计算*/
    private VData mVData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    // 备份 转储号码
    private String mEdorNo;
    //**
    private MMap mMMap=new MMap();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSet mLAAssessSet = new LAAssessSet();  //要回退的人员
    //回退轨迹
    private LARollBackTraceSet mLARollBackTraceSet = new LARollBackTraceSet();

    private LAAssessHistorySchema mLAAssessHistorySchema = new LAAssessHistorySchema();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private String mEvaluateDate = "";           // 异动时间
    private String mManageCom = "";              // 管理机构
    private String mIndexCalNo = "";             // 考核序列号
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mGradeSeries = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    public LAAgentAssessBackBL()
    {
    }
    public static void main(String args[])
    {
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        mEdorNo = PubFun1.CreateMaxNo("EDORNO", 20);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            return true;
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LAAgentAssessBackBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentAssessBackBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }


        }
        mInputData = null;
        return true;
    }
    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mMMap.put(mLARollBackTraceSet,"INSERT");
        mInputData.add(mMMap);
        return true;
    }
    /**
     * 准备后台的数据,处理下一步操作（考核确认回退，考核计算回退）
     * @return boolean
     */
    private boolean addInput()
    {
        try
        {
            System.out.println("Begin LAAgentAssessBackBL.prepareOutputData.........");
            mVData.clear();
            mVData.add(this.mGlobalInput);
            mVData.add(this.mLAAssessSet);
            mVData.add(this.mManageCom);
            mVData.add(this.mIndexCalNo);
            mVData.add(this.mBranchType);
            mVData.add(this.mBranchType2);
            mVData.add(this.mEdorNo);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentAssessBackBL";
            tError.functionName = "addInput";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
        //得到需要回退的代理人信息
        if(!getAgent())
        {
            return false;
        }
        if(!check())
        {
            return false;
        }
        if(!addInput())
        {
            return false;
        }
        if (!dealCalExamine()) {
            return false;
        }
        //再处理考核计算回退
        if (!dealInsured()) {
            return false;
                }
       if (!dealRollBackTrace())
       {
          return false;
       }

        return true;
    }
    /**
     * 得到要处理的业务员
     * @return boolean
     */
    private boolean getAgent()
    {
        LAAssessSet tLAAssessSet = new LAAssessSet();
        LAAssessDB tLAAssessDB = new LAAssessDB();
        String tSQL="select * from laassess where ManageCom like '"+mManageCom
                    +"%' and indexcalno='"+mIndexCalNo+"'  and BranchType='"+mBranchType
                    +"' and  BranchType2='"+mBranchType2+"'";

        mLAAssessSet=tLAAssessDB.executeQuery(tSQL);
        if(mLAAssessSet.size()<=0)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentAssessBackBL";
            tError.functionName = "getAgent";
            tError.errorMessage = "没有符合条件的人员需要回退！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }
    private boolean check()
    {
        if(mManageCom==null || mManageCom.length() != 4)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentAssessBackBL";
            tError.functionName = "check";
            tError.errorMessage = "管理机构必须为4位代码！";
            this.mErrors.addOneError(tError);
                return false;
        }
        LAAssessMainSet tLAAssessMainSet = new LAAssessMainSet();
        LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
        String tSql="select * from laassessmain  where managecom like '"+mManageCom
                    +"%' and BranchType='"+mBranchType+"' and  BranchType2='"+mBranchType2
                    +"' and state='02' order by  indexcalno desc ";
        tLAAssessMainSet=tLAAssessMainDB.executeQuery(tSql);
        if(tLAAssessMainSet.size()<=0)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentAssessBackBL";
            tError.functionName = "check";
            tError.errorMessage = "该分公司公司没有考核信息需要回退！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tWageNo=tLAAssessMainSet.get(1).getIndexCalNo();
        if(!mIndexCalNo.equals(tWageNo))
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentAssessBackBL";
            tError.functionName = "check";
            tError.errorMessage = "分公司最近一次考核为"+tWageNo+",必须回退最近一次的考核信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        tSql="select * from lawagehistory  where managecom like '"+mManageCom
                    +"%' and BranchType='"+mBranchType+"' and  BranchType2='"+mBranchType2
                    +"' and  wageno>'"+mIndexCalNo+"' and state='14' ";
        LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
        LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
        tLAWageHistorySet=tLAWageHistoryDB.executeQuery(tSql);
        if(tLAWageHistorySet.size()>0)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentAssessBackBL";
            tError.functionName = "check";
            tError.errorMessage = "分公司"+tWageNo+"以后月份的佣金计算已经确认，不能回退本月的考核信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
//        if(mLAAssessSet.size()>0)
//        {
//            for(int i=1;i<=mLAAssessSet.size();i++)
//            {
//
//            }
//        }
        return true;
    }
    /**
     * 先处理考核确认回退
     * @return boolean
     */
    private boolean dealCalExamine( )
    {
        LAAssessBackInsuredBL tLAAssessBackInsuredBL=new LAAssessBackInsuredBL();
        try
        {
            if (!tLAAssessBackInsuredBL.submitData(mVData, mOperate))
            {
                this.mErrors.copyAllErrors(tLAAssessBackInsuredBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentAssessBackBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            MMap tMMap=new MMap();
            tMMap=tLAAssessBackInsuredBL.getResult();
            mMMap.add(tMMap);
//            mInputData.add(tMMap);
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return true;
    }
    /**
     * 再处理考核计算回退
     * @return boolean
     */
    private boolean dealInsured()
    {
        LAAssessBackCalBL tLAAssessBackCalBL=new LAAssessBackCalBL();
        try
        {
            if (!tLAAssessBackCalBL.submitData(mVData, mOperate))
            {
                this.mErrors.copyAllErrors(tLAAssessBackCalBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentAssessBackBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            MMap tMMap=new MMap();
            tMMap=tLAAssessBackCalBL.getResult();
            mMMap.add(tMMap);
//            mInputData.add(tMMap);
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAgentGradeDecideBL.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mManageCom = (String) cInputData.get(1);
        this.mIndexCalNo =  (String) cInputData.get(2);
        this.mBranchType = (String) cInputData.get(3);
        this.mBranchType2 = (String) cInputData.get(4);

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecideBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 进行回退记录处理
     */
    private boolean dealRollBackTrace()
    {
        //条件划分符号
        String tSign = "&";
        //回退序号
        String tIdx = PubFun1.CreateMaxNo("IDX", 20);
        //回退条件
        String tTrem = "考核回退年月:" + mIndexCalNo;
        tTrem  += tSign + "管理机构:" + mManageCom;
        tTrem  += tSign + "展业机构:" + mBranchType;
        tTrem  += tSign + "展业渠道:" + mBranchType2;

        LARollBackTraceSchema tLARollBackTraceSchema = new LARollBackTraceSchema();
        tLARollBackTraceSchema.setIdx(tIdx);
        tLARollBackTraceSchema.setEdorNo(mEdorNo);
        tLARollBackTraceSchema.setoperator(mGlobalInput.Operator);
        tLARollBackTraceSchema.setstate("0");
        tLARollBackTraceSchema.setConditions(tTrem);
        tLARollBackTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLARollBackTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLARollBackTraceSchema.setRollBackType("30");//考核回退（确认）

        //缓存回退记录
        mLARollBackTraceSet.add(tLARollBackTraceSchema);

        return true;
    }


}
