package com.sinosoft.lis.agentassess;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LAAssessAccessorySet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LAAgentGradeSchema;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.agentcalculate.CalFormDrawRate;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.pubfun.Arith;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xiangchun
 * @version 1.0
 */
 public class DealDifLATree {
    MMap map = new MMap();
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessAccessorySet mLAAssessAccessorySet = new
            LAAssessAccessorySet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LATreeSet mdealLATreeSet = new LATreeSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LADimissionSet mLADimissionSet = new LADimissionSet();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LABranchGroupBSet mLABranchGroupBSet = new LABranchGroupBSet();
    private LACommisionSet mgetLACommisionSet = new LACommisionSet();
    private VData mInputData = new VData();
    private VData mOutputData = new VData();
    private MMap mMap = new MMap();
    private String mOperate = "";
    private String mAscriptDate = "";
    private String mIndexCalno= "";
    private String mStartDate= "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String tEdorNo ="";
    public DealDifLATree() {
    }

    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);

        mdealLATreeSet = (LATreeSet) cInputData.getObjectByObjectName("LATreeSet", 0);
        if (mdealLATreeSet == null  || mdealLATreeSet.size()<=0)
        {
            CError tError = new CError();
            tError.moduleName = "DealDifLATree";
            tError.functionName = "getInputData";
            tError.errorMessage = "未得到足够数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean check() {
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!check()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        return true;
    }

    private boolean dealData() {
        String tEdorType = "31";
        tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        for (int i = 1; i <= mdealLATreeSet.size(); i++) {
            LATreeSchema tdealLATreeSchema = new LATreeSchema();
            tdealLATreeSchema=mdealLATreeSet.get(i);
            mStartDate=tdealLATreeSchema.getStartDate();
            String tSql="select date('"+mStartDate+"')-1 month  from  dual ";
            ExeSQL tExeSQL = new ExeSQL();
            String tdate=tExeSQL.getOneValue(tSql);
            mIndexCalno=AgentPubFun.formatDate(tdate,"yyyyMM");

            String tAgentCode=tdealLATreeSchema.getAgentCode();
            String tnewAgentGrade=tdealLATreeSchema.getAgentGrade();
            //新职级系列号
            String tAgentSeries1 = AgentPubFun.getAgentSeries(tnewAgentGrade);

            LATreeDB tdealLATreeDB = new LATreeDB();
            LATreeSchema tLastLATreeSchema = new LATreeSchema();
            tdealLATreeDB.setAgentCode(tAgentCode);
            tdealLATreeDB.getInfo();
            tLastLATreeSchema=tdealLATreeDB.getSchema();
            //得到当前职级
            String tLastAgentGradeNew=tLastLATreeSchema.getAgentGrade();
            String tLastAgentSeriesnew=AgentPubFun.getAgentSeries(tLastAgentGradeNew);

           // String  tLastAgentGrade=tLastLATreeSchema.getAgentGrade();
            //根据时间得到上次职级
            String tLastAgentGrade=getlastgrade(tAgentCode);
            //原职级系列号
            String tAgentSeries = AgentPubFun.getAgentSeries(tLastAgentGrade);
            if (tAgentSeries == null || tAgentSeries.equals("")) {
                CError tError = new CError();
                tError.moduleName = "DealDifLATree";
                tError.functionName = "dealData";
                tError.errorMessage = "获取职级" + tLastAgentGrade + "所对应的代理人系列返回为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tAgentSeries1 == null || tAgentSeries1.equals("")) {
                CError tError = new CError();
                tError.moduleName = "DealDifLATree";
                tError.functionName = "dealData";
                tError.errorMessage = "获取职级" + tnewAgentGrade + "所对应的代理人系列返回为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLastAgentSeriesnew == null || tLastAgentSeriesnew.equals("")) {
                CError tError = new CError();
                tError.moduleName = "DealLATree";
                tError.functionName = "dealData";
                tError.errorMessage = "获取职级" + tLastAgentSeriesnew + "所对应的代理人系列返回为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //当前职级系列与新职级比较,不能用上次职级与新职级比较
            if (tLastAgentSeriesnew.equals(tAgentSeries1))
            {
                CError tError = new CError();
                tError.moduleName = "DealLATree";
                tError.functionName = "dealData";
                tError.errorMessage = "本模块只能处理不同系列的职级的异动!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //处理与该人员有关的其他人员和该人员的团队信息
            if(tAgentSeries1.equals("0"))
            {//从主管到业务
               if(!ascriptToBusSeries(tdealLATreeSchema))
               {
                   return false;
               }
            }
            else if (tAgentSeries1.equals("1"))
            {//从业务到主管
                if(!ascriptToMngSeries(tdealLATreeSchema))
                {
                    return false ;
                }
            }
            LAAgentDB tLAAgentDB = new LAAgentDB();
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            tLAAgentDB.setAgentCode(tAgentCode);
            if (!tLAAgentDB.getInfo()) {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "DealDifLATree";
                tError.functionName = "dealData";
                tError.errorMessage = "未查询到" + tAgentCode + "的原始信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLAAgentSchema = tLAAgentDB.getSchema();
            LATreeDB tLATreeDB = new LATreeDB();
            LATreeSchema tLATreeSchema = new LATreeSchema();
            tLATreeDB.setAgentCode(tAgentCode);
            if (!tLATreeDB.getInfo()) {
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "DealDifLATree";
                tError.functionName = "dealData";
                tError.errorMessage = "未查询到" + tAgentCode + "的行政信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLATreeSchema = tLATreeDB.getSchema();
            LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
            //原来职级
            LAAgentGradeSchema tLAAgentGradeSchema = new LAAgentGradeSchema();
            //新职级
            LAAgentGradeSchema tLAAgentGrade1Schema = new LAAgentGradeSchema();

            tLAAgentGradeDB.setGradeCode(tLastAgentGrade);
            if (!tLAAgentGradeDB.getInfo()) {
                this.mErrors.copyAllErrors(tLAAgentGradeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "DealDifLATree";
                tError.functionName = "dealData";
                tError.errorMessage = "未查询到" + tLastAgentGrade + "的描述信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLAAgentGradeSchema = tLAAgentGradeDB.getSchema();//原来的职级信息
            tLAAgentGradeDB.setGradeCode(tnewAgentGrade);
            if (!tLAAgentGradeDB.getInfo()) {
                this.mErrors.copyAllErrors(tLAAgentGradeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "DealDifLATree";
                tError.functionName = "dealData";
                tError.errorMessage = "未查询到" + tnewAgentGrade + "的描述信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLAAgentGrade1Schema = tLAAgentGradeDB.getSchema();//新职级信息
            mAscriptDate = tdealLATreeSchema.getStartDate();
            mAscriptDate = AgentPubFun.formatDate(mAscriptDate,
                        "yyyy-MM-dd");
//                mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);

//            }


            if (tLAAgentGradeSchema.getGradeProperty4().equals("0") &&
                tLAAgentGrade1Schema.getGradeProperty4().equals("1")) {
                if (tLAAgentSchema.getInDueFormDate() == null ||
                    tLAAgentSchema.getInDueFormDate().equals("")) {
                    LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
                    tLAAgentBSchema.setModifyDate(currentDate);
                    tLAAgentBSchema.setModifyTime(currentTime);

                    tLAAgentBSchema.setIndexCalNo(mIndexCalno);
                    tLAAgentBSchema.setEdorNo(tEdorNo);
                    tLAAgentBSchema.setEdorType(tEdorType);
                    tLAAgentBSchema.setMakeDate(currentDate);
                    tLAAgentBSchema.setMakeTime(currentTime);
                    tLAAgentBSchema.setModifyDate(currentDate);
                    tLAAgentBSchema.setModifyTime(currentTime);
                    tLAAgentBSchema.setOperator(mGlobalInput.Operator);
                    tLAAgentSchema.setInDueFormDate(mAscriptDate); //置转正日期
                    tLAAgentSchema.setModifyDate(currentDate);
                    tLAAgentSchema.setModifyTime(currentTime);
                    tLAAgentSchema.setOperator(mGlobalInput.Operator);
                    mLAAgentBSet.add(tLAAgentBSchema);
                }
            }

            if (  tLastAgentSeriesnew.equals("0") &&tnewAgentGrade.equals("1") )
            {
                String sql = "select * from lacommision where agentcode='" +
                      tdealLATreeSchema.getAgentCode() +
                      "' and (caldate is null or caldate>='" + mAscriptDate +
                      "')";
                LACommisionDB tLACommisionDB = new LACommisionDB();
                LACommisionSet tLACommisionSet = new LACommisionSet();
                tLACommisionSet = tLACommisionDB.executeQuery(sql);
                if(tLACommisionSet!=null && tLACommisionSet.size()>=1)
                {
                    for (int k = 1; k <= tLACommisionSet.size(); k++) {
                        LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                        tLACommisionSchema=tLACommisionSet.get(k);
                        String tSignDate=tLACommisionSchema.getSignDate();
                        String tSQL="select rate from  lafycrate where Startdate<='"+tSignDate
                                    +"' and enddate>='"+tSignDate+"' ";
                        tExeSQL= new ExeSQL();
                        String tRate=tExeSQL.getOneValue(tSQL);
                        double tdealRate=0;
                        if(tRate!=null && !tRate.equals(""))
                        {
                            tdealRate=Double.parseDouble(tRate);
                            tdealRate=Arith.round(tdealRate,4);
                        }
                        double tfycrate=tLACommisionSchema.getFYCRate();
                        double tfyc=tLACommisionSchema.getFYC();
                        tfycrate=Arith.div(tfycrate,tdealRate,2);
                        tfyc=Arith.div(tfyc,tdealRate,2);
                        tLACommisionSchema.setDirectWage(tfyc);
                        tLACommisionSchema.setStandFYCRate(tfycrate);
                        tLACommisionSchema.setFYCRate(tfycrate);
                        tLACommisionSchema.setFYC(tfyc);
                        mLACommisionSet.add(tLACommisionSchema);
                    }
                }

            }
            if (  tLastAgentSeriesnew.equals("1") &&tnewAgentGrade.equals("0") )
            {
                String sql = "select * from lacommision where agentcode='" +
                      tdealLATreeSchema.getAgentCode() +
                      "' and (caldate is null or caldate>='" + mAscriptDate +
                      "')";
                LACommisionDB tLACommisionDB = new LACommisionDB();
                LACommisionSet tLACommisionSet = new LACommisionSet();
                tLACommisionSet = tLACommisionDB.executeQuery(sql);
                if(tLACommisionSet!=null && tLACommisionSet.size()>=1)
                {
                    for (int k = 1; k <= tLACommisionSet.size(); k++)
                    {
                        LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                        tLACommisionSchema=tLACommisionSet.get(k);
                        String tSignDate=tLACommisionSchema.getSignDate();
                        String tSQL="select rate from  lafycrate where Startdate<='"+tSignDate
                                    +"' and enddate>='"+tSignDate+"' ";
                        tExeSQL= new ExeSQL();
                        String tRate=tExeSQL.getOneValue(tSQL);
                        double tdealRate=0;
                        if(tRate!=null && !tRate.equals(""))
                        {
                            tdealRate=Double.parseDouble(tRate);
                            tdealRate=Arith.round(tdealRate,4);
                        }
                        double tfycrate=tLACommisionSchema.getFYCRate();
                        double tfyc=tLACommisionSchema.getFYC();
                        tfycrate=Arith.mul(tfycrate,tdealRate);
                        tfycrate=Arith.round(tfycrate,2);
                        tfyc=Arith.mul(tfyc,tdealRate);
                        tfyc=Arith.round(tfyc,2);
                        tLACommisionSchema.setDirectWage(tfyc);
                        tLACommisionSchema.setStandFYCRate(tfycrate);
                        tLACommisionSchema.setFYCRate(tfycrate);
                        tLACommisionSchema.setFYC(tfyc);
                        mLACommisionSet.add(tLACommisionSchema);
                    }
                }

            }
            if (!tnewAgentGrade.equals(tLastAgentGradeNew)) {
                LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setEdorNO(tEdorNo);
                tLATreeBSchema.setRemoveType(tEdorType);
                tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSchema.setIndexCalNo(mIndexCalno);
                tLATreeBSchema.setOperator(mGlobalInput.Operator);
                mLATreeBSet.add(tLATreeBSchema);
                tLATreeSchema.setAgentGrade(tnewAgentGrade);
                tLATreeSchema.setAgentSeries(tAgentSeries1);
                tLATreeSchema.setAgentLastGrade(tLastAgentGrade);
                tLATreeSchema.setAgentLastSeries(tAgentSeries);
                //得到职级开始时间
                String tStartDate=getlaststartdate(tLATreeSchema.getAgentCode());
                tLATreeSchema.setOldStartDate(tStartDate);
                tLATreeSchema.setOldEndDate(PubFun.calDate(mAscriptDate, -1,
                            "D", null));

                if (tLastAgentGrade.compareTo(tnewAgentGrade) > 0) { //降级
                    tLATreeSchema.setAssessType("2");
                } else if (tLastAgentGrade.compareTo(tnewAgentGrade) < 0) {
                    tLATreeSchema.setAssessType("1"); //晋升
                } else {
                    tLATreeSchema.setAssessType("0"); //晋升
                }
                tLATreeSchema.setStartDate(mAscriptDate);
                tLATreeSchema.setAstartDate(mAscriptDate);
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
            }
            if(tLATreeSchema.getAgentGrade().equals("D01"))
            {
                tLATreeSchema.setInDueFormDate("");
                tLATreeSchema.setInDueFormFlag("N");
            }
            mLAAgentSet.add(tLAAgentSchema);
            mLATreeSet.add(tLATreeSchema);
        }
        return true;
    }
    private boolean  ascriptToBusSeries(LATreeSchema cLATreeSchema)
    {
        String tEdorType="07";
        String tStartDate=cLATreeSchema.getStartDate();
        String tAgentGroup=cLATreeSchema.getAgentGroup();
        String tManager=cLATreeSchema.getAgentCode();
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = getBranchGroupInfo(tAgentGroup);

        LAAgentSchema tLAAgentSchema =  new LAAgentSchema();
        tLAAgentSchema = getAgentBasicInfo(tManager);
        //备份处理团队信息
        if(tLABranchGroupSchema==null || tLAAgentSchema==null)
        {
            return false;
        }
        String tBranchManager=tLABranchGroupSchema.getBranchManager();
        boolean tflag=false;
        if(tBranchManager==null || tBranchManager.equals("") || !tBranchManager.equals(tManager))
        {
            tflag=true;
        }
        else
        {
            //备份信息
            LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
            tLABranchGroupBSchema.setEdorNo(tEdorNo);
            tLABranchGroupBSchema.setEdorType(tEdorType);
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.
                    getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.
                    getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                    getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                    getModifyTime());
            tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.
                    getOperator());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(this.mGlobalInput.
                            Operator);
            mLABranchGroupBSet.add(tLABranchGroupBSchema);
            //修改信息
            tLABranchGroupSchema.setBranchManager("");
            tLABranchGroupSchema.setBranchManagerName("");
            tLABranchGroupSchema.setModifyDate(currentDate);
            tLABranchGroupSchema.setModifyTime(currentTime);
            tLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
            mLABranchGroupSet.add(tLABranchGroupSchema);
        }
        String sql = "select * from latree where upagent='" + tManager + "' and exists (select 'X' "+
                     " from laagent where agentcode=latree.agentcode and agentstate<'06' )";
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = new LATreeSet();
        tLATreeSet = tLATreeDB.executeQuery(sql);
        if (tLATreeDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "DealDifLATree";
            tError.functionName = "ascriptToMngSeries";
            tError.errorMessage = "查询" + tAgentGroup + "的所有成员时失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("----tLATreeSet.size----" + tLATreeSet.size());
        for (int i = 1; i <= tLATreeSet.size(); i++)
        {
            LATreeSchema tLATreeSchema = new LATreeSchema();
            //备份组员的行政信息

            tLATreeSchema = tLATreeSet.get(i);
            if (tLATreeSchema.getAgentCode().equals(tManager)) {
                continue; //如果主管在这个组中，则主管的上级代理人信息不在这做修改
            }
            if (tLATreeSchema.getUpAgent() == null || tLATreeSchema.getUpAgent().equals("")
                || !tLATreeSchema.getUpAgent().equals(tManager))
            {
                continue; //如果上级代理人并没有变，则不用备份修改,也不用修改
            }
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLATreeBSchema, tLATreeSchema);
            tLATreeBSchema.setEdorNO(tEdorNo);
            mAscriptDate = tStartDate;
            mAscriptDate = AgentPubFun.formatDate(mAscriptDate,
                        "yyyy-MM-dd");
            /////mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);
            tLATreeBSchema.setRemoveType(tEdorType); //主管任命
            tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
            tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
            this.mLATreeBSet.add(tLATreeBSchema);
            //更新组员的行政信息
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tLATreeSchema.getAgentCode());
            tLAAgentDB.getInfo();
            String employdate = AgentPubFun.formatDate(tLAAgentDB.
                    getEmployDate(), "yyyy-MM-dd");
            if (employdate.compareTo(mAscriptDate) > 0) {
                tLATreeSchema.setAstartDate(employdate);
            } else {
                tLATreeSchema.setAstartDate(mAscriptDate);
            }

            tLATreeSchema.setUpAgent("");//上级代理人为空
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            tLATreeSchema.setOperator(this.mGlobalInput.Operator);
            if(tLATreeSchema.getAgentGrade().equals("D01"))
            {
                tLATreeSchema.setInDueFormDate("");
                tLATreeSchema.setInDueFormFlag("N");
            }
            this.mLATreeSet.add(tLATreeSchema);
        }
        return true;
    }
    private boolean  ascriptToMngSeries(LATreeSchema cLATreeSchema)
    {
        String tEdorType="07";
        String tStartDate=cLATreeSchema.getStartDate();
        String tAgentGroup=cLATreeSchema.getAgentGroup();
        String tManager=cLATreeSchema.getAgentCode();
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = getBranchGroupInfo(tAgentGroup);

        LAAgentSchema tLAAgentSchema =  new LAAgentSchema();
        tLAAgentSchema = getAgentBasicInfo(tManager);
        //备份处理团队信息
        if(tLABranchGroupSchema==null || tLAAgentSchema==null)
        {
            return false;
        }
        String tBranchManager=tLABranchGroupSchema.getBranchManager();
        boolean tflag=false;
        if(tBranchManager!=null && tBranchManager.equals(tManager))
        {
            tflag=true;
        }
        else
        {
            //备份信息
            LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
            tLABranchGroupBSchema.setEdorNo(tEdorNo);
            tLABranchGroupBSchema.setEdorType(tEdorType);
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.
                    getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.
                    getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                    getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                    getModifyTime());
            tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.
                    getOperator());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(this.mGlobalInput.
                            Operator);
            mLABranchGroupBSet.add(tLABranchGroupBSchema);
            //修改信息
            tLABranchGroupSchema.setBranchManager(tManager);
            tLABranchGroupSchema.setBranchManagerName(tLAAgentSchema.getName());
            tLABranchGroupSchema.setModifyDate(currentDate);
            tLABranchGroupSchema.setModifyTime(currentTime);
            tLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
            mLABranchGroupSet.add(tLABranchGroupSchema);
        }
        String strSQL = "select * from latree where agentgroup='" + tAgentGroup +
                     "'" + " And exists (Select 'X' From LAAgent " +
                     " Where AgentState <'06' and agentcode=latree.agentcode)";
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = new LATreeSet();
        tLATreeSet = tLATreeDB.executeQuery(strSQL);
        if (tLATreeDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "DealDifLATree";
            tError.functionName = "ascriptToMngSeries";
            tError.errorMessage = "查询" + tAgentGroup + "的所有成员时失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("----tLATreeSet.size----" + tLATreeSet.size());
        for (int i = 1; i <= tLATreeSet.size(); i++)
        {
            LATreeSchema tLATreeSchema = new LATreeSchema();
            //备份组员的行政信息

            tLATreeSchema = tLATreeSet.get(i);
            if (tLATreeSchema.getAgentCode().equals(tManager)) {
                continue; //如果主管在这个组中，则主管的上级代理人信息不在这做修改
            }
            if (tLATreeSchema.getUpAgent() != null &&
                tLATreeSchema.getUpAgent().equals(tManager)) {
                continue; //如果上级代理人并没有变，则不用备份修改,也不用修改
            }
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLATreeBSchema, tLATreeSchema);
            tLATreeBSchema.setEdorNO(tEdorNo);
            mAscriptDate = tStartDate;
            mAscriptDate = AgentPubFun.formatDate(mAscriptDate,
                        "yyyy-MM-dd");
            /////mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);
            tLATreeBSchema.setRemoveType(tEdorType); //主管任命
            tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
            tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
            this.mLATreeBSet.add(tLATreeBSchema);
            //更新组员的行政信息
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tLATreeSchema.getAgentCode());
            tLAAgentDB.getInfo();
            String employdate = AgentPubFun.formatDate(tLAAgentDB.
                    getEmployDate(), "yyyy-MM-dd");
            if (employdate.compareTo(mAscriptDate) > 0) {
                tLATreeSchema.setAstartDate(employdate);
            } else {
                tLATreeSchema.setAstartDate(mAscriptDate);
            }

            tLATreeSchema.setUpAgent(tManager);
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            tLATreeSchema.setOperator(this.mGlobalInput.Operator);
            this.mLATreeSet.add(tLATreeSchema);
        }
        return true;
    }
    private String getlastgrade(String cAgentCode)
    {
        String tdate=PubFun.calDate(mStartDate, -1,"D", null);
        String tSql="select agentgrade from latree where agentcode='"+cAgentCode
                   +"' and startdate<='" +tdate+"'";
        ExeSQL tExeSQL=new ExeSQL();
        String tAgentGrade=tExeSQL.getOneValue(tSql);

        if (tAgentGrade == null || tAgentGrade.equals(""))
        {
            tSql="select agentlastgrade from latree where agentcode='"+cAgentCode+"'";
            tExeSQL=new ExeSQL();
            tAgentGrade=tExeSQL.getOneValue(tSql);
        }
        return tAgentGrade;
    }
    private String getlaststartdate(String cAgentCode)
    {
        String tdate=PubFun.calDate(mStartDate, -1,"D", null);
        String tSql="select startDate from latree where agentcode='"+cAgentCode
                   +"' and startdate<='" +tdate+"'";
        ExeSQL tExeSQL=new ExeSQL();
        String tStartDate=tExeSQL.getOneValue(tSql);

        if (tStartDate == null || tStartDate.equals(""))
        {
            tSql="select oldstartDate from latree where agentcode='"+cAgentCode+"'";
            tExeSQL=new ExeSQL();
            tStartDate=tExeSQL.getOneValue(tSql);
        }
        return tStartDate;

    }
    /**
     * 查询机构编码为agentGroup的机构信息
     *
     * @param agentGroup String
     * @return com.sinosoft.lis.schema.LABranchGroupSchema
     */
    private LABranchGroupSchema getBranchGroupInfo(String agentGroup) {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(agentGroup);
        if (!tLABranchGroupDB.getInfo()) {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "DealDifLATree";
            tError.functionName = "getBranchGroupInfo";
            tError.errorMessage = "查询机构" + agentGroup + "的信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLABranchGroupDB.getSchema();
    }
    /**
     * 查询tAgentCode的基础信息
     *
     * @param tAgentCode String
     * @return com.sinosoft.lis.schema.LAAgentSchema
     */
    private LAAgentSchema getAgentBasicInfo(String tAgentCode) {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "DealDifLATree";
            tError.functionName = "getAgentBasicInfo";
            tError.errorMessage = "查询代理人" + tAgentCode + "的基础信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLAAgentDB.getSchema();
    }
    private boolean prepareOutputData()
    {
     //   mOutputData = new VData();mLABranchGroupSet
        mMap = new MMap();
        mMap.put(mLATreeSet, "UPDATE");
        mMap.put(mLATreeBSet, "INSERT");
        mMap.put(mLAAgentSet, "UPDATE");
        mMap.put(mLAAgentBSet, "INSERT");
        mMap.put(mLACommisionSet, "UPDATE");
        mMap.put(mLABranchGroupSet, "UPDATE");
        mMap.put(mLABranchGroupBSet, "INSERT");
      //  mOutputData.add(mMap);
        return true;
    }

    public MMap getResult() {
        return mMap;
    }
}
