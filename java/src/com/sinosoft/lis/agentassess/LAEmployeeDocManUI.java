package com.sinosoft.lis.agentassess;

/**
 * <p>Title: 档案管理程序 </p>
 * <p>Description: 实现档案调入、调出处理</p>
 * <p>Copyright: Copyright (c) 2004-04-14</p>
 * <p>Company: sinosoft</p>
 * @author LL
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAEmployeeDocManUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public LAEmployeeDocManUI()
    {
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        this.mInputData = (VData) cInputData.clone();
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据

        LAEmployeeDocManBL tLAEmployeeDocManBL = new LAEmployeeDocManBL();
        System.out.println("Start LAEmployeeDocManUI Submit...");
        tLAEmployeeDocManBL.submitData(mInputData, this.mOperate);
        System.out.println("End LAEmployeeDocManUI Submit...");
        //如果有需要处理的错误，则返回
        if (tLAEmployeeDocManBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAEmployeeDocManBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeDocManUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }
}
