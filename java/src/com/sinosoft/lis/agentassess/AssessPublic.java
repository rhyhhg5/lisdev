package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: 考核公共类</p>
 * <p>Description: 考核公共类</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public abstract class AssessPublic
{
    //全局变量
    protected CErrors mErrors = new CErrors(); //错误处理类
    protected GlobalInput mGlobalInput = new GlobalInput(); //保存传入参数

    public AssessPublic()
    {
    }

    public static void main(String[] args)
    {
    }
}
