/*
 * <p>ClassName: LAEmployeeAssessAdjUI </p>
 * <p>Description: LAEmployeeAssessAdjUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-06-21
 */
package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAEmployeeAssessAdjUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private VData mResult = new VData();

//业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public LAEmployeeAssessAdjUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        this.mInputData = (VData) cInputData.clone();
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据

        LAEmployeeAssessAdjBL tLAEmployeeAssessAdjBL = new
                LAEmployeeAssessAdjBL();
        System.out.println("Start LAEmployeeAssessAdj UI Submit...");
        tLAEmployeeAssessAdjBL.submitData(mInputData, cOperate);
        System.out.println("End LAEmployeeAssessAdj UI Submit...");
        //如果有需要处理的错误，则返回
        if (tLAEmployeeAssessAdjBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAEmployeeAssessAdjBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAssessAdjUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
//    mInputData=null;
        if (cOperate.equals("QUERY||MAIN"))
        {
            mResult = tLAEmployeeAssessAdjBL.getResult();
        }
        return true;
    }

    public static void main(String[] args)
    {
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }


}
