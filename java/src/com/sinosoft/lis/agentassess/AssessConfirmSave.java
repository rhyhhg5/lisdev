package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class AssessConfirmSave
{
    //错误处理类
    public static CErrors mErrors = new CErrors();
    //业务处理相关变量
    /** 全局数据 */
    private VData mInputData = new VData();
    public GlobalInput mGlobalInput = new GlobalInput();
    private String m_IndexCalNo = "";
    private String m_ManageCom = "";
    private String m_AgentGrade = "";

    public AssessConfirmSave()
    {
    }

    public static void main(String[] args)
    {
        AssessConfirmSave assessConfirmSave1 = new AssessConfirmSave();

        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "aa";

        VData tVData = new VData();
        tVData.clear();
        tVData.add(tGI);
        tVData.add(0, "86110000");
        tVData.add(1, "A02");
        tVData.add(2, "200306");

        if (!assessConfirmSave1.dealData(tVData))
        {
            System.out.println("failed!" + mErrors.getFirstError());
        }

    }

    public boolean dealData(VData cInputData)
    {
        System.out.println("----come into dealData---");
        System.out.println("----cInputData.size: " + cInputData.size());
        if (!getInputData(cInputData))
        {
            return false;
        }

//  zsj delete: 要整体确认，无需校验
//    if ( !verifyData() )
//      return false;

        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            dealError("dealData", "数据库连接失败！");
            return false;
        }

        LAAssessSchema tSch = new LAAssessSchema();
        LAAssessSet tSet = new LAAssessSet();
        LAAssessDB tDB = new LAAssessDB();
        String tSQL = "Select * from laassess where managecom like  concat('"
                      + this.m_ManageCom.trim()
                      + "','%') and AgentGrade = '"
                      + this.m_AgentGrade.trim()
                      + "' and IndexCalNo = '"
                      + this.m_IndexCalNo.trim() + "'"
                      + " and managecom like concat('" + mGlobalInput.ManageCom +
                      "','%')"
                      + " and state = '0'";
        System.out.println("---qry LAAssess---Sql : " + tSQL);
        tSet = tDB.executeQuery(tSQL);
        int tCount = tSet.size();
        if (tCount == 0)
        {
            dealError("DealData", "未找到考核信息！");
            return false;
        }

        try
        {
            conn.setAutoCommit(false);
            for (int i = 1; i <= tCount; i++)
            {
                tSch = tSet.get(i);

                if (!updLAAssess(tSch, conn))
                {
                    conn.rollback();
                    conn.close();
                    dealError("dealData", "刷新失败！");
                    return false;
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}

            dealError("dealData", ex.toString());
            return false;
        }
        return true;
    }

    private boolean updLAAssess(LAAssessSchema tSch, Connection conn)
    {
        System.out.println("---come into updLAAssess---");
        LAAssessDB tDB = new LAAssessDB(conn);
        LAAssessSchema tOldSch = new LAAssessSchema();
        PubFun tPF = new PubFun();

        tDB.setAgentCode(tSch.getAgentCode());
        tDB.setIndexCalNo(tSch.getIndexCalNo());
        if (!tDB.getInfo())
        {
            return false;
        }
        tOldSch.setSchema(tDB.getSchema());
        tOldSch.setAgentGrade1(tSch.getAgentGrade1());
        tOldSch.setAgentSeries1(tSch.getAgentSeries1());
        tOldSch.setConfirmer(this.mGlobalInput.Operator);
        tOldSch.setConfirmDate(tPF.getCurrentDate());
        tOldSch.setState("1");

        tDB.setSchema(tOldSch);
        if (!tDB.update())
        {
            return false;
        }

        return true;
    }

//  private boolean verifyData()  {
//    System.out.println("---come into verifyData---");
//    LAAssessSchema tSch = new LAAssessSchema();
//    int tCount = this.mLAAssessSet.size();
//    for ( int i=1;i<=tCount;i++ )  {
//      tSch = this.mLAAssessSet.get(i);
//      if ( tSch.getAgentGrade1() == null || tSch.getAgentGrade1() == "")
//      {
//        dealError("verifyData","建议的职级或系列或确认人不得为空值!");
//        return false;
//      }
//    }
//    return true;
//  }

    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
            this.m_ManageCom = (String) cInputData.getObjectByObjectName(
                    "String", 0);
            this.m_IndexCalNo = (String) cInputData.getObjectByObjectName(
                    "String", 1);
            this.m_AgentGrade = (String) cInputData.getObjectByObjectName(
                    "String", 2);
            System.out.println("---m_IndexCalNo: " + this.m_IndexCalNo);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }

        if ((this.m_AgentGrade == null || this.m_AgentGrade == "")
            || (this.m_IndexCalNo == null || this.m_IndexCalNo == "")
            || (this.m_ManageCom == null || this.m_ManageCom == "")
                )
        {
            dealError("getInputdata", "没有传入足够数据！");
            return false;
        }
        return true;
    }

    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "AssessConfirmSave";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }
} //end class
