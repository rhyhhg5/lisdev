package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.lis.db.LAAssessMainDB;
import com.sinosoft.lis.schema.LAAssessMainSchema;
import com.sinosoft.lis.vschema.LAAssessMainSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: Sinosoft</p>
 * @author
 * @version 1.0
 * @author
 * @modified by Howie
 */

public class HDAssessConFirmFBL
{
    //错误处理类
    public static CErrors mErrors = new CErrors();
    //业务处理相关变量
    /** 全局数据 */
    private VData mInputData = new VData();
    public GlobalInput mGlobalInput = new GlobalInput();
    private String mIndexCalNo = "";
    private String mManageCom = "";
    private String mBranchType = "";     //展业类型
    private String mBranchType2 = "";     //子渠道
    private String mAgentSeries = "";    //职级系列

    public HDAssessConFirmFBL()
    {
    }

    public static void main(String[] args)
    {
        HDAssessConFirmFBL assessConfirmSave1 = new HDAssessConFirmFBL();
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "001";

        VData tVData = new VData();
        tVData.clear();
        tVData.add(tGI);
        tVData.add("8611"); //机构
        tVData.add("200703"); //考核年月
        tVData.add("5"); //展业类型
        tVData.add("01"); //子渠道
        tVData.add("0"); //职级系列

        try
        {
            if (!assessConfirmSave1.submitData(tVData))
            {
                System.out.println("failed!" + mErrors.getFirstError());
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 获取前台传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        try
        {
            this.mGlobalInput.setSchema((GlobalInput) mInputData.
                                        getObjectByObjectName("GlobalInput", 0));
            this.mManageCom = (String) mInputData.getObject(1);
            this.mIndexCalNo = (String) mInputData.getObject(2);
            this.mBranchType = (String) mInputData.getObject(3);
            this.mBranchType2 = (String) mInputData.getObject(4);
            this.mAgentSeries = (String) mInputData.getObject(5);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }

        if ((this.mIndexCalNo == null || this.mIndexCalNo == "")
            || (this.mManageCom == null || this.mManageCom == "")
            || (this.mBranchType == null || this.mBranchType == "")
            || (this.mBranchType2 == null || this.mBranchType2 == "")
            || (this.mAgentSeries == null || this.mAgentSeries == ""))
        {
            dealError("getInputdata", "没有传入足够数据！");
            return false;
        }
        if (mManageCom.indexOf(mGlobalInput.ManageCom) != 0)
        { //操作机构不是被归属机构或其上级机构
            dealError("getInputData", "无权操作此机构的审核确认！");
            return false;
        }
        return true;
    }
    /**
     *
     * @return boolean
     */
    private boolean dealData()
    {
        System.out.println("----come into dealData---");
        Connection conn = DBConnPool.getConnection();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAssessSet tLAAssessSet = new LAAssessSet();
        LAAssessDB tLAAssessDB = new LAAssessDB();
        LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
        LAAssessMainSchema tLAAssessMainSchema = new LAAssessMainSchema();
        LAAssessMainSet tLAAssessMainSet = new LAAssessMainSet();
        String strSQL = "select * from laassessmain where  assesstype='00' and indexcalno='" +
                        mIndexCalNo + "' and managecom like '" + mManageCom +
                        "%' and state='0' ";

        if (this.mAgentSeries.compareTo("0")==0)
        {
            strSQL += " and agentgrade like 'U%' ";
        }
        else if (this.mAgentSeries.compareTo("0")>0)
        {
            strSQL += " and agentgrade like 'V%' ";
        }

        tLAAssessMainSet = tLAAssessMainDB.executeQuery(strSQL);
        if (tLAAssessMainSet.size() <= 0)
        {
            //再查一下看看是不是已经确认了
            strSQL =
                    "select * from laassessmain where assesstype='00' and indexcalno='" +
                    mIndexCalNo + "' and managecom like '" + mManageCom +
                    "%' and state>'0' ";
            if (this.mAgentSeries.compareTo("0") == 0)
            {
                strSQL += " and agentgrade like 'U%' ";
            }
            else if (this.mAgentSeries.compareTo("0") > 0)
            {
                strSQL += " and agentgrade like 'V%' ";
            }

            tLAAssessMainSet = tLAAssessMainDB.executeQuery(strSQL);
            if (tLAAssessMainSet.size() <= 0)
            {
                dealError("dealData",
                          "查询机构" + mManageCom + "下的考评主信息失败!");
                return false;
            }
            else
            {
                dealError("dealData",
                          "机构" + mManageCom + "下的考评信息已确认过!");
                return false;
            }
        }
        tLAAssessMainDB = new LAAssessMainDB(conn);
        int tCount = 0;
        for (int i = 1; i <= tLAAssessMainSet.size(); i++)
        {
//            if (tLAAssessMainSet.get(i).getAssessCount() <= 0)
//            { //这个机构没有人参加考核，不查考核结果记录
//                continue;
//            }
            LAAssessSet tmpLAAssessSet = new LAAssessSet();
            String tSQL =
                    "Select * from laassess where managecom =  '"
                    + tLAAssessMainSet.get(i).getManageCom() + "'"
                    + " and IndexCalNo = '"
                    + this.mIndexCalNo.trim() + "'"
                    + " and BranchType='" + mBranchType + "'"
                    + " and state = '0' "
                    + " and agentgrade = '"+tLAAssessMainSet.get(i).getAgentGrade()+"' "
                    ;
            if (this.mAgentSeries.compareTo("0") == 0)
            {
                tSQL += " and agentgrade like 'U%' ";
            }
            else if (this.mAgentSeries.compareTo("0") > 0)
            {
                tSQL += " and agentgrade like 'V%' ";
            }
            System.out.println("---query LAAssess---Sql : " + tSQL);
            tmpLAAssessSet = tLAAssessDB.executeQuery(tSQL);
            if (tmpLAAssessSet.size() > 0)
            {
                tLAAssessSet.add(tmpLAAssessSet);
            }
            int tLastCount = tCount;
            tCount = tLAAssessSet.size();
            if (tCount < tLastCount)
            {
                dealError("DealData", "未找到考核结果信息！");
                return false;
            }
            if (this.mAgentSeries.compareTo("0")>0)
            {
            	String tjudgeVSQL =" select * from laassess where managecom ='"+tLAAssessMainSet.get(i).getManageCom()+"'" +
                		" and indexcalno ='"+this.mIndexCalNo.trim()+"' and branchtype ='"+mBranchType+"' and branchtype2 = '"+mBranchType2+"' " +
                		" and agentgrade ='V01'and agentgrade1 = 'U10' and state='0' ";
                System.out.println("---query LAAssess---Sql : " + tjudgeVSQL);
                LAAssessSet tLAAssessSet1 = new LAAssessSet();
                tLAAssessSet1 = tLAAssessDB.executeQuery(tjudgeVSQL);
                if(tLAAssessSet1.size()>0)
                {
                	  dealError("DealData", "V01降级为业务序列时，最高只能为U07，需要通过考核调整确认职级");
                      return false;
                }
            }
            
            
        }
        System.out.println("total tLAAssessSet.size:" + tCount);
        try
        {
            conn.setAutoCommit(false);
            for (int i = 1; i <= tCount; i++)
            {
                tLAAssessSchema = tLAAssessSet.get(i);
                if (!updLAAssess(tLAAssessSchema, conn))
                {
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            //更新考核信息主表（日志）
            for (int i = 1; i <= tLAAssessMainSet.size(); i++)
            {
                tLAAssessMainSchema = new LAAssessMainSchema();
                tLAAssessMainSchema = tLAAssessMainSet.get(i);
                if (tLAAssessMainSchema.getAssessCount()>=0)
                {
                    tLAAssessMainSchema.setConfirmCount(tLAAssessMainSchema.getAssessCount());
                }
                tLAAssessMainSchema.setState("1"); //审核确认状态标记
                tLAAssessMainSchema.setOperator(mGlobalInput.Operator);
                tLAAssessMainSchema.setModifyDate(PubFun.getCurrentDate());
                tLAAssessMainSchema.setModifyTime(PubFun.getCurrentTime());
                tLAAssessMainDB.setSchema(tLAAssessMainSchema);

                if (!tLAAssessMainDB.update())
                {
                    conn.rollback();
                    conn.close();
                    dealError("dealData", "更新考评主信息失败！");
                    return false;
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            dealError("dealData", ex.toString());
            return false;
        }
        return true;
    }
    /**
     * 更新考核结果
     * @param tLAAssessSchema LAAssessSchema
     * @param conn Connection
     * @return boolean
     */
    private boolean updLAAssess(LAAssessSchema tLAAssessSchema, Connection conn)
    {
        System.out.println("---come into updLAAssess---");
        LAAssessDB tLAAssessDB = new LAAssessDB(conn);
        LAAssessSchema tOldSch = new LAAssessSchema();
        tLAAssessDB.setAgentCode(tLAAssessSchema.getAgentCode());
        tLAAssessDB.setIndexCalNo(tLAAssessSchema.getIndexCalNo());
        if (!tLAAssessDB.getInfo())
        {
            return false;
        }
        tOldSch.setSchema(tLAAssessDB.getSchema());
        tOldSch.setConfirmer(this.mGlobalInput.Operator);
        tOldSch.setConfirmDate(PubFun.getCurrentDate());
        tOldSch.setState("1");   //审核确认状态标记
        tOldSch.setOperator(this.mGlobalInput.Operator);
        tOldSch.setModifyDate(PubFun.getCurrentDate());
        tOldSch.setModifyTime(PubFun.getCurrentTime());
        tLAAssessDB.setSchema(tOldSch);
        if (!tLAAssessDB.update())
        {
            CError.buildErr(this,"代理人考评信息审核确认失败！");
            return false;
        }
        return true;
    }

    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();
        tError.moduleName = "AssessConfirmBL";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;
        mErrors.addOneError(tError);
    }
} //end class
