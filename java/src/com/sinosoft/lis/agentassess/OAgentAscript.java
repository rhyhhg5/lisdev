package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.agentcalculate.CalFormDrawRate;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vdb.LACommisionDBSet;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;


/**
 * <p>Title: 代理人组织归属 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zhangsj
 * @version 1.0
 */

public class OAgentAscript
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    //传入参数
    public GlobalInput mGlobalInput = new GlobalInput();
    private String m_ManageCom = "";
    private String m_AgentCode = "";
    private String m_AsptGrade = "";
    private static String mIndexCalNo = "";
    private LAAssessSchema mLAAssessSchema = new LAAssessSchema();
    private Connection conn = null;
    private String[] MonDate = new String[2];
    public static String mEdorNo = "";

    //处理跨系列升降级的情况
    private LAAssessSet mLAAssessSet = new LAAssessSet();


    public OAgentAscript()
    {
    }


    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        OAgentAscript tOA = new OAgentAscript();
        String tManageCom = "86110000";
        String tIndexCalNo = "200501";
        String tAgentGrade = "A06";
        System.out.println("--ManageCom--" + tManageCom);
        System.out.println("--IndexCalNo--" + tIndexCalNo);
        System.out.println("--AgentGrade--" + tAgentGrade);
        VData tVData = new VData();

        tVData.clear();
        tVData.add(tG);
        tVData.add(0, tManageCom);
        tVData.add(1, tIndexCalNo);
        tVData.add(2, tAgentGrade);

        if (!tOA.queryAgent(tVData))
        {
            System.out.println("Error: " + tOA.mErrors.getFirstError());
            return;
        }
        else
        {
            System.out.println("Succ! 归属成功！ ");
            return;
        }

//    try
//    {
//      PrintStream out=
//          new PrintStream(
//          new BufferedOutputStream(
//          new FileOutputStream("OAgentAscript.out")));
//      System.setOut(out) ;
//    OAgentAscript agentAscript1 = new OAgentAscript();
//    GlobalInput tGI = new GlobalInput();
//    tGI.ManageCom = "86110000";
//    tGI.ComCode = "8611";
//    tGI.Operator = "aa";
//
//
//    LAAssessDB tADB = new LAAssessDB();
//    LAAssessSchema tASch = new LAAssessSchema();
//    tADB.setIndexCalNo("200406");
//    tADB.setAgentCode("8611000177");
//    if ( !tADB.getInfo() )
//      System.out.println("----qry failed---");
//
//    VData tInput = new VData();
//    tInput.clear();
//    tInput.add(tGI);
//    tInput.add("200407");
//    tASch = tADB.getSchema();
//    if ( !agentAscript1.getInputForEach(tInput) )
//      System.out.println("----getinput failed---");
//    if ( !agentAscript1.dealEach(tASch) )
//      System.out.println("----Deal failed---"+agentAscript1.mErrors.getFirstError() );
//
//
////    VData cInputData = new VData();
////    cInputData.clear();
////    String tManageCom = "86110000";
////    cInputData.add(tGI);
////    cInputData.add(0,tManageCom);
////    cInputData.add(1,"200310");
////    cInputData.add(2,"A01");
////
////
////    if ( !agentAscript1.queryAgent(cInputData) )
////      System.out.println("---- failed!  ---" + agentAscript1.mErrors.getFirstError());
////    else
////      System.out.println("---- Succ!  ---");
//
////    String[] tMonDate = new String[2];
////    tMonDate = agentAscript1.genDate();
////    System.out.println("---firstDay : " + tMonDate[0]);
////    System.out.println("---lastDay: " + tMonDate[1]);
////    String tSql = "SELECT t.*,t.rowid from laassess t "
////                + "where indexcalno = '200403' "
////                + "and managecom = '86110000' "
////                + "and operator ='aa' "
////                + "and state = '1'";
////    System.out.println(tSql);
////    LAAssessDB tDB = new LAAssessDB();
////    LAAssessSet tSet = new LAAssessSet();
////    tSet = tDB.executeQuery(tSql);
////    VData tVData = new VData();
////    tVData.clear();
////    tVData.add(tGI);
////    tVData.add(tSet);
////    tVData.add("200403");
////    if ( !agentAscript1.getInputForSpecial(tVData) )
////      System.out.println("----Failed---:" + agentAscript1.mErrors.getFirstError());
////    if (!agentAscript1.dealSpecial())
////      System.out.println("---Error--" + agentAscript1.mErrors.getFirstError());
//    out.close() ;
//    }
//    catch(Exception e)
//    {
//      e.printStackTrace() ;
//    }
    }

    /**
     * 正常考核之后归属的入口函数
     * @param cInputData
     * @return
     */
    public boolean queryAgent(VData cInputData)
    {
        //将操作数据拷贝到本类中
        this.mInputData = cInputData;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        this.MonDate = genDate();
        System.out.println("---gen Date already!");

        //变量定义
        LAAssessDB tLAAssessDB = new LAAssessDB();
        String sql = "SELECT * FROM LAASSESS WHERE ManageCom like '"
                     + this.m_ManageCom.trim() + "%' and managecom like '" +
                     mGlobalInput.ManageCom + "%' AND IndexCalNo = '"
                     + this.mIndexCalNo.trim() + "' AND AgentGroupNew IS NULL"
                     + " AND trim(branchtype)='1' AND State = '1'"
                     + " and agentgrade = '" + this.m_AsptGrade.trim() + "'";
        System.out.println("---AgentQuery---sql : " + sql);

        LAAssessSet tLAAssessSet = new LAAssessSet();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();

        //查询代理人信息
        tLAAssessSet = tLAAssessDB.executeQuery(sql);

        //依次处理代理人组织归属
        int tCount = tLAAssessSet.size();
        String tEdorNo = "";
        for (int i = 1; i <= tCount; i++)
        {
            tEdorNo = createEdorNo();
            setEdorNo(tEdorNo);
            tLAAssessSchema = tLAAssessSet.get(i);
            if (!dealEach(tLAAssessSchema))
            {
                return false;
            }
        } //end for
        return true;
    }

    private static String createEdorNo()
    {
        String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        System.out.println("---zsj---OAgentAscript--createEdorNo : " + mEdorNo);
        return mEdorNo;
    }

    private void setEdorNo(String tEdorNo)
    {
        this.mEdorNo = tEdorNo;
    }

    public static String getEdorNo()
    {
        return mEdorNo;
    }

    /**
     * 处理跨系列的升级与降级的组织归属
     * @return
     */
    public boolean dealSpecial()
    {
        String[][] tDefine = new String[6][3];
        //分为6档
        /**
         * 没一档的第0个值是序号
         */
        tDefine[0][0] = "0";
        tDefine[0][1] = "00-00";
        tDefine[0][2] = "00";

        tDefine[1][0] = "1";
        tDefine[1][1] = "A01-A03";
        tDefine[1][2] = "A";

        tDefine[2][0] = "2";
        tDefine[2][1] = "A04-A05";
        tDefine[2][2] = "B";

        tDefine[3][0] = "3";
        tDefine[3][1] = "A06-A07";
        tDefine[3][2] = "C";

        tDefine[4][0] = "4";
        tDefine[4][1] = "A08-A08";
        tDefine[4][2] = "D";

        tDefine[5][0] = "5";
        tDefine[5][1] = "A09-A09";
        tDefine[5][2] = "D";

        String tAgentGrade1 = "";
        String tAgentSeries1 = "";
        String tAgentGrade = "";
        String tStartLevel = "";
        String tEndLevel = "";
        String tEdorNo = "";
        String tAgentCode = "";

        int tNum = this.mLAAssessSet.size();
        for (int i = 1; i <= tNum; i++)
        {
            LAAssessSchema tSch = new LAAssessSchema();
            tSch.setSchema(this.mLAAssessSet.get(i));
            tAgentGrade1 = tSch.getAgentGrade1().trim();
            tAgentSeries1 = tSch.getAgentSeries1().trim();
            tAgentGrade = tSch.getAgentGrade().trim();
            tAgentCode = tSch.getAgentCode().trim();

            if (tAgentGrade.trim().equals("00"))
            {
                dealError("dealSpecial", "请先将被清退的代理人恢复，再做职级调整！");
                return false;
            }

            for (int j = 0; j <= 5; j++)
            {
                int start = tDefine[j][1].indexOf("-");

                String tBeginGrade = tDefine[j][1].substring(0,
                        tDefine[j][1].indexOf("-"));
                String tEndGrade = tDefine[j][1].substring(tDefine[j][1].
                        indexOf("-") +
                        1);
                if (tAgentGrade.trim().compareToIgnoreCase(tBeginGrade) >= 0
                    && tAgentGrade.trim().compareToIgnoreCase(tEndGrade) <= 0)
                {
                    tStartLevel = tDefine[j][0];
                }

                if (tAgentGrade1.trim().compareToIgnoreCase(tBeginGrade) >= 0
                    && tAgentGrade1.trim().compareToIgnoreCase(tEndGrade) <= 0)
                {
                    tEndLevel = tDefine[j][0];
                }
            }

            int tCount = Integer.parseInt(tStartLevel.trim()) -
                         Integer.parseInt(tEndLevel.trim());
            String direction = "";
            if (tCount > 0)
            {
                direction = "Down";
            }
            else
            {
                direction = "Up";
                tCount = Math.abs(tCount);
            }

            if (tCount > 1)
            { //跨系列升降级
                int tLevel = 0;
                String tLastGrade = "";
                String tLastSeries = "";
                for (int m = 1; m <= tCount; m++)
                {
                    //产生最大转储号
                    tEdorNo = createEdorNo();
                    //将该转储号置为公用
                    setEdorNo(tEdorNo);

                    if (direction.trim().equals("Up"))
                    {
                        tLevel = Integer.parseInt(tStartLevel) + m;
                    }
                    else
                    {
                        tLevel = Integer.parseInt(tStartLevel) - m;
                    }
                    String transGrade = tDefine[tLevel][1].trim().substring(
                            tDefine[tLevel][1].trim().lastIndexOf("-") + 1);
                    String transSeries = tDefine[tLevel][2].trim();
                    //如果处理的是中间系列，则将原始职级和原始系列置为中间的某个职级与系列。
                    if (m > 1)
                    {
                        tSch.setAgentGrade(tLastGrade.trim());
                        tSch.setAgentSeries(tLastSeries);
                    }
                    //如果处理的是最后系列，则将建议职级和建议系列置为外界传入的职级和系列。
                    if (m == tCount)
                    {
                        tSch.setAgentGrade1(tAgentGrade1);
                        tSch.setAgentSeries1(tAgentSeries1);
                    }
                    //如果处理的是最开始的系列，则将建议职级和建议系列置为中间一个系列的第一个职级。
                    else
                    {
                        tSch.setAgentGrade1(transGrade.trim());
                        tSch.setAgentSeries1(transSeries.trim());
                    }

                    //由于后面程序的处理中需要从LAAssess取出AgentGrade1和AgentSeries1，所以需要先刷新LAAssess
                    LAAssessDB tDB = new LAAssessDB();
                    LAAgentDB tAgentDB = new LAAgentDB();
                    tAgentDB.setAgentCode(tAgentCode);
                    if (!tAgentDB.getInfo())
                    {
                        dealError("dealSpecial", tAgentDB.mErrors.getFirstError());
                        return false;
                    }

                    ExeSQL tExe = new ExeSQL();
                    String sql = "select branchattr from labranchgroup where "
                                 + "agentgroup = '" +
                                 tAgentDB.getBranchCode().trim()
                                 + "'";
                    String tBranchAttr = tExe.getOneValue(sql).trim();

                    tSch.setState("1");
                    tSch.setAgentGroup(tAgentDB.getAgentGroup());
                    tSch.setBranchAttr(tBranchAttr);
                    tSch.setAgentGroupNew("");
                    tDB.setSchema(tSch);
                    if (!tDB.update())
                    {
                        dealError("dealSpecial", "用中间职级刷新LAAssess表失败！");
                        return false;
                    }
                    if (!dealEach(tSch))
                    {
                        dealError("dealSpecial", this.mErrors.getFirstError());
                        return false;
                    }
                    tLastGrade = transGrade.trim();
                    tLastSeries = transSeries.trim();
                }
            }
            else
            {
                if (!dealEach(tSch))
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 处理单个代理人
     * @param tLAAssessSchema
     * @return
     */
    public boolean dealEach(LAAssessSchema tLAAssessSchema)
    {
        String t_AgentSeries = "";
        String t_AgentSeries1 = "";
        String t_AgentGrade = "";
        String t_AgentGrade1 = "";
        String t_AgentCode = "";

        t_AgentSeries = tLAAssessSchema.getAgentSeries();
        t_AgentSeries1 = tLAAssessSchema.getAgentSeries1();
        t_AgentGrade = tLAAssessSchema.getAgentGrade();
        t_AgentGrade1 = tLAAssessSchema.getAgentGrade1();
        this.m_AgentCode = tLAAssessSchema.getAgentCode();

        //校验：已被清退的人不参加组织归属
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSchema tLAAgentSch = new LAAgentSchema();
        tLAAgentDB.setAgentCode(tLAAssessSchema.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            dealError("dealEach", "查询代理人当前状态，判断代理人是否在职失败！");
            return false;
        }
        String checkWage = AgentPubFun.AdjustCommCheck(m_AgentCode, MonDate[0]);
        if (!checkWage.equals("00"))
        {
            dealError("dealEach", checkWage);
            return false;
        }

        tLAAgentSch.setSchema(tLAAgentDB.getSchema());
        String tAgentState = "";
        tAgentState = tLAAgentSch.getAgentState().trim();
        System.out.println("------dealEach ----agentstate = " + tAgentState);
        if (tAgentState.compareToIgnoreCase("03") >= 0)
        {
            this.conn = DBConnPool.getConnection();
            if (this.conn == null)
            {
                dealError("dealEach", "数据库连接失败！");
                return false;
            }
            try
            {
                conn.setAutoCommit(false);

                if (!updLAAssess(tLAAssessSchema))
                {
                    this.conn.rollback();
                    this.conn.close();
                    return false;
                }

                else
                {
                    conn.commit();
                    conn.close();
                    return true;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                try
                {
                    this.conn.rollback();
                    this.conn.close();
                }
                catch (Exception ep)
                {
                    ep.printStackTrace();
                    dealError("dealEach", ep.toString());
                    return false;
                }
                dealError("dealEach", e.toString());
                return false;

            }

        }

        // 校验：针对清退的情况，判断降级不能从组长以上清退。'0'表示清退
        if (t_AgentSeries1.trim().equals("0"))
        {
            if ((t_AgentSeries.compareTo("A") > 0))
            {
                dealError("dealEach", "不可跨越两个职级以上进行归属！");
                return false;
            }
        }
        else
        {
            if (t_AgentGrade.trim().compareTo("A08") < 0 &&
                t_AgentGrade1.trim().compareTo("A08") > 0)
            {
                dealError("dealEach", "不可跨越两个系列以上进行归属！");
                return false;
            }
            int tAscII = (int) t_AgentSeries.charAt(0);
            int tAscII1 = (int) t_AgentSeries1.charAt(0);
            int diff = tAscII1 - tAscII;
            diff = Math.abs(diff);
            if (diff >= 2)
            {
                dealError("dealEach", "不可跨越两个系列以上进行归属！");
                return false;
            }
        }

        //准备数据
        if (!prepareOutputData(tLAAssessSchema))
        {
            return false;
        }

        //晋升归属
        if (t_AgentSeries1.compareToIgnoreCase(t_AgentSeries) > 0 ||
            (t_AgentGrade.trim().equalsIgnoreCase("A08") &&
             t_AgentGrade1.trim().equalsIgnoreCase("A09")))
        {
            DoAscriptPromote tDoAscriptPromote = new DoAscriptPromote();
            if (!tDoAscriptPromote.promote(mInputData))
            {
                if (tDoAscriptPromote.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tDoAscriptPromote.mErrors);
                }
                return false;
            }
        }
        else
        {
            //降级归属
            if ((t_AgentSeries1.trim().equalsIgnoreCase("0")) ||
                (t_AgentSeries1.compareTo(t_AgentSeries) < 0) ||
                (t_AgentGrade.trim().equals("A09") &&
                 t_AgentGrade1.trim().equals("A08")))
            {
                DoAscriptDemote tDoAscriptDemote = new DoAscriptDemote();
                if (!tDoAscriptDemote.demote(mInputData))
                {
                    if (tDoAscriptDemote.mErrors.needDealError())
                    {
                        mErrors.copyAllErrors(tDoAscriptDemote.mErrors);
                    }
                    return false;
                }
            }
            else

            //没有系列的变化，不用对机构进行调整
            if (t_AgentSeries1.equals(t_AgentSeries))
            {
                this.conn = DBConnPool.getConnection();
                if (this.conn == null)
                {
                    dealError("dealEach", "数据库连接失败！");
                    return false;
                }

                try
                {
                    this.conn.setAutoCommit(false);

                    if (!updLAAssess(tLAAssessSchema))
                    {
                        this.conn.rollback();
                        this.conn.close();
                        return false;
                    }

                    if (!t_AgentGrade1.trim().equalsIgnoreCase(t_AgentGrade.
                            trim()))
                    {
                        if (!insLATreeB())
                        {
                            this.conn.rollback();
                            this.conn.close();
                            return false;
                        }

                        if (!updLATree(tLAAssessSchema))
                        {
                            this.conn.rollback();
                            this.conn.close();
                            return false;
                        }

                        System.out.println("---DealEach---AgentGrade : " +
                                           t_AgentGrade);
                        if (t_AgentGrade.trim().compareToIgnoreCase("A04") >= 0)
                        {
                            if (!updLATreeAccessory(tLAAssessSchema))
                            {
                                this.conn.rollback();
                                this.conn.close();
                                return false;
                            }
                        }
                    }

                    if (t_AgentGrade.trim().equalsIgnoreCase("A01") &&
                        t_AgentGrade1.trim().compareToIgnoreCase("A02") >= 0)
                    {
                        //zsj--delete--2004-3-9:因为每月计算佣金时都会重算，所以不对业绩打折
                        //cg-add--2004-05-08:因为计算佣金之前处理打折的时候只会处理A01的情况，所以归属的时候应该对业绩进行处理，恢复为不打折

                        if (!updLACommision("P"))
                        {
                            this.conn.rollback();
                            this.conn.close();
                            return false;
                        }

                        if (!updLAAgent())
                        {
                            this.conn.rollback();
                            this.conn.close();
                            return false;
                        }
                    }

//zsj--delete--2004-3-9:因为每月计算佣金时都会重算，所以不对业绩打折
//          if (t_AgentGrade.trim().compareToIgnoreCase("A02")>=0 &&
//              t_AgentGrade1.trim().equalsIgnoreCase("A01"))
//            if (!updLACommision("D")) {
//              this.conn.rollback();
//              this.conn.close();
//              return false;
//            }
                    this.conn.commit();
//          conn.rollback() ;
                    this.conn.close();
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    try
                    {
                        this.conn.rollback();
                        this.conn.close();
                    }
                    catch (Exception ep)
                    {
                        ep.printStackTrace();
                        dealError("dealEach", ep.toString());
                        return false;
                    }
                    dealError("dealEach", ex.toString());
                    return false;
                }
            }
        }
        return true;
    }


    private boolean updLATreeAccessory(LAAssessSchema tSch)
    {
        PubFun tPF = new PubFun();
        LATreeAccessoryDB tDB = new LATreeAccessoryDB(this.conn);
        LATreeAccessorySchema tAccSch = new LATreeAccessorySchema();
        String tSql = "select * from latreeaccessory where agentcode = '"
                      + tSch.getAgentCode() + "' and (agentgrade = '"
                      + tSch.getAgentGrade() + "' or agentgrade = '"
                      + tSch.getAgentGrade1() + "')";

        LATreeAccessorySet tAccSet = new LATreeAccessorySet();
        tAccSet = tDB.executeQuery(tSql);
        int tCount = tAccSet.size();
        if (tCount == 0)
        {
            return true;
        }
        System.out.println("---tCount = " + tCount);

        tAccSch.setSchema(tAccSet.get(1));

        tDB.setAgentCode(tAccSch.getAgentCode());
        tDB.setAgentGrade(tAccSch.getAgentGrade());
        if (!tDB.delete())
        {
            dealError("updLATreeAccessory", "删除LATreeAccessory表旧数据失败！");
            return false;
        }

        tAccSch.setAgentGrade(tSch.getAgentGrade1());
        tAccSch.setModifyDate(tPF.getCurrentDate());
        tAccSch.setModifyTime(tPF.getCurrentTime());
        tAccSch.setOperator(this.mGlobalInput.Operator);
        tDB.setSchema(tAccSch);
        if (!tDB.insert())
        {
            dealError("updLATreeAccessory", "刷新LATreeAccessory表失败！");
            return false;
        }
        return true;
    }

    private boolean updLAAgent()
    {
        LAAgentDB tDB = new LAAgentDB(this.conn);
        LAAgentSchema tSch = new LAAgentSchema();
        PubFun tPF = new PubFun();

        tDB.setAgentCode(this.m_AgentCode);
        if (!tDB.getInfo())
        {
            return false;
        }
        tSch.setSchema(tDB.getSchema());

        //置转正日期
        if (tSch.getInDueFormDate() == "" || tSch.getInDueFormDate() == null)
        {
            tSch.setInDueFormDate(this.MonDate[0]);
            tSch.setModifyDate(tPF.getCurrentDate());
            tSch.setModifyTime(tPF.getCurrentTime());
            tSch.setOperator(this.mGlobalInput.Operator);
            tDB.setSchema(tSch);
            if (!tDB.update())
            {
                return false;
            }
        }

        return true;
    }

    private String[] genDate()
    {
        String[] tMonDate = new String[2];
        PubFun tPF = new PubFun();
        String tYear = this.mIndexCalNo.trim().substring(0, 4);
        String tMon = this.mIndexCalNo.trim().substring(4, 6);

        //计算旧职级的止聘日期
        String tOldDate = tYear + "-" + tMon + "-01";
        tMonDate = tPF.calFLDate(tOldDate);
        String tOldDay = tMonDate[1];
        System.out.println("---tOldDay---" + tOldDay);
        //计算新职级的起聘日期或转正日期
        if (tMon.equals("12"))
        {
            tYear = Integer.toString(Integer.parseInt(tYear) + 1);
            tMon = "01";
        }
        else
        {
            tMon = Integer.toString(Integer.parseInt(tMon) + 1);
        }
        int tLength = tMon.length();
        if (tLength == 1)
        {
            tMon = "0" + tMon;
        }
        String tDate = tYear.trim() + "-" + tMon.trim() + "-" + "01";
        System.out.println("---tNewDay---" + tDate);

        tMonDate[0] = tDate;
        tMonDate[1] = tOldDay;
        return tMonDate;
    }


    private boolean insLATreeB()
    {
        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeBDB tLATreeBDB = new LATreeBDB(this.conn);
        LATreeBSchema tLATreeBSchema = new LATreeBSchema();
        LATreeSchema tLATreeSchema = new LATreeSchema();

        tLATreeDB.setAgentCode(this.m_AgentCode);
        if (!tLATreeDB.getInfo())
        {
            dealError("insLATreeB", "查询LATree表失败！");
            return false;
        }

        tLATreeSchema = tLATreeDB.getSchema();

        Reflections rf = new Reflections();
        rf.transFields(tLATreeBSchema, tLATreeSchema);

        tLATreeBSchema.setRemoveType("01");
        tLATreeBSchema.setModifyDate(tPF.getCurrentDate());
        tLATreeBSchema.setModifyTime(tPF.getCurrentTime());
        tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
        tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
        tLATreeBSchema.setMakeDate(tPF.getCurrentDate());
        tLATreeBSchema.setMakeTime(tPF.getCurrentTime());
        tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
        tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
        tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
        tLATreeBSchema.setEdorNO(OAgentAscript.createEdorNo());
        System.out.println("---zsj---OAgentAscript--insLATreeB--EdorNo = " +
                           OAgentAscript.createEdorNo());
        tLATreeBSchema.setIndexCalNo(this.mIndexCalNo);

        tLATreeBDB.setSchema(tLATreeBSchema);
        if (!tLATreeBDB.insert())
        {
            dealError("insLATreeB", "插入代理人行政信息转储表失败！");
            return false;
        }
        return true;
    }

    private boolean updLATree(LAAssessSchema tLAAssessSchema)
    {
        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();
        LATreeDB tDB = new LATreeDB(this.conn);
        LATreeSchema tSch = new LATreeSchema();
        String tOldGrade = tLAAssessSchema.getAgentGrade();
        String tNewGrade = tLAAssessSchema.getAgentGrade1();

        tDB.setAgentCode(this.m_AgentCode);
        if (!tDB.getInfo())
        {
            dealError("updLATree", "更新LATree时查询失败！");
            return false;
        }
        tSch = tDB.getSchema();

        tSch.setAgentLastGrade(tSch.getAgentGrade());
        tSch.setAgentLastSeries(tSch.getAgentSeries());
        tSch.setAgentGrade(tLAAssessSchema.getAgentGrade1());
        tSch.setAgentSeries(tLAAssessSchema.getAgentSeries1());
        tSch.setModifyDate(tPF.getCurrentDate());
        tSch.setModifyTime(tPF.getCurrentTime());
        tSch.setOperator(this.mGlobalInput.Operator);
        if (tOldGrade.compareToIgnoreCase(tNewGrade) > 0)
        {
            tSch.setAssessType("2"); //降级
            tSch.setState("2");
        }
        if (tOldGrade.compareToIgnoreCase(tNewGrade) < 0)
        {
            tSch.setAssessType("1"); //升级
            tSch.setState("1");
        }

        tSch.setOldEndDate(this.MonDate[1]);
        tSch.setOldStartDate(tSch.getStartDate());
        tSch.setStartDate(this.MonDate[0]);
        tSch.setAstartDate(this.MonDate[0]);

        tDB.setSchema(tSch);
        if (!tDB.update())
        {
            dealError("updLATree", "刷新LATree失败！");
            return false;
        }
        return true;
    }

    private boolean updLAAssess(LAAssessSchema tLAAssessSchema)
    {
        PubFun tPF = new PubFun();
        LAAssessDB tLAAssessDB = new LAAssessDB(this.conn);
        LATreeDB tLATreeDB = new LATreeDB();
        String tGroupNew = "";

        tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
        if (!tLATreeDB.getInfo())
        {
            return false;
        }
        tGroupNew = tLATreeDB.getAgentGroup();

        tLAAssessSchema.setAgentGroupNew(tGroupNew);
        tLAAssessSchema.setState("2");
        tLAAssessSchema.setModifyDate(tPF.getCurrentDate());
        tLAAssessSchema.setModifyTime(tPF.getCurrentTime());
        tLAAssessDB.setSchema(tLAAssessSchema);
        System.out.println("before update LAAssess");
        if (!tLAAssessDB.update())
        {
            this.mErrors.copyAllErrors(tLAAssessDB.mErrors);
            dealError("upLAAssess", "没有职级变动更新考评信息表失败！");
            return false;
        }
        return true;
    }

    /* tFlag = "P": A01->A02、"D": A02->A01 */
    private boolean updLACommision(String tFlag)
    {
        PubFun tPF = new PubFun();
        ExeSQL tExeSql = new ExeSQL(this.conn);
        String tSQL = "";

        String tStartDate = this.MonDate[0];

        System.out.println("---111---tFlag : " + tFlag);
        // A01->A02:升
        AgentPubFun tAPF = new AgentPubFun();
        String tCheckFlag = tAPF.AdjustCommCheck(this.m_AgentCode.trim(),
                                                 tStartDate);
        if (!tCheckFlag.trim().equals("00"))
        {
            dealError("updLACommision", tCheckFlag.trim());
            return false;
        }

        if (tFlag.trim().equals("P"))
        {
            tSQL = "update lacommision set fyc = lacommision.directwage,"
                   +
                   " fycrate = lacommision.standfycrate where trim(AgentCode)="
                   + "'" + this.m_AgentCode.trim() + "' and (caldate >= '"
                   + tStartDate.trim() + "' or caldate is null)";

            System.out.println("---tSQL : " + tSQL);

            if (!tExeSql.execUpdateSQL(tSQL))
            {
                dealError("updLACommision", "刷新LACommision失败！");
                return false;
            }
        }

        // A02->A01:降
        if (tFlag.trim().equals("D"))
        {
            CalFormDrawRate tCalRate = new CalFormDrawRate();
            LACommisionDB tDB = new LACommisionDB();
            LACommisionDBSet tDBSet = new LACommisionDBSet(this.conn);
            LACommisionSet tSet = new LACommisionSet();
            LACommisionSet tResult = new LACommisionSet();

            tSQL = "select * from lacommision where trim(agentcode) = '"
                   + this.m_AgentCode.trim() + "' and (caldate >= '"
                   + tStartDate.trim()
                   + "' or caldate is null) order by polno,ReceiptNo";

            System.out.println("--updLACommision--D--tSQL : " + tSQL);
            tSet = tDB.executeQuery(tSQL);
            int tSize = tSet.size();
            System.out.println("---updLACommision : size = " + tSize);
            if (tSize == 0)
            {
                return true;
            }

            VData cInputData = new VData();
            cInputData.clear();
            cInputData.add(tSet);
            cInputData.add(0, "A01");

            if (!tCalRate.submitData(cInputData, "RECALFYC||AGENTWAGE"))
            {
                dealError("updLACommision", tCalRate.mErrors.getFirstError());
                return false;
            }

            tResult = (LACommisionSet) tCalRate.getResult().
                      getObjectByObjectName("LACommisionSet", 0);

            int tCount = tResult.size();
            if (tSize != tCount)
            {
                dealError("updLACommision",
                          "传入类CalFormDrawRate.java中的数据和传出的数据条数不一致！");
                return false;
            }

            if (!tDBSet.set(tResult))
            {
                dealError("updLACommision", tDBSet.mErrors.getFirstError());
                return false;
            }

            if (!tDBSet.update())
            {
                dealError("updLACommision", tDBSet.mErrors.getFirstError());
                return false;
            }
        }
        return true;
    }

    /**
     * 正常考核后归属时所需要调用的
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.m_ManageCom = (String) cInputData.getObjectByObjectName("String",
                0);
        this.mIndexCalNo = (String) cInputData.getObjectByObjectName("String",
                1);
        this.m_AsptGrade = (String) cInputData.getObjectByObjectName("String",
                2);
        if (mGlobalInput == null || this.m_ManageCom == null
            || this.mIndexCalNo == null
            || this.m_AsptGrade == null)
        {
            // @@错误处理
            dealError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    /**
     * 行政考核升降级手工录入时需要调用的
     * @param cInputData
     * @return
     */
    public boolean getInputForEach(VData cInputData)
    {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mIndexCalNo = (String) cInputData.getObjectByObjectName("String",
                0);
        if (this.mGlobalInput == null || this.mIndexCalNo.equals("") ||
            this.mIndexCalNo == null)
        {
            dealError("getInputForEach", "传入数据不足！");
            return false;
        }
        this.MonDate = genDate();
        String tEdorNo = createEdorNo();
        setEdorNo(tEdorNo);
        return true;
    }

    /**
     * 跨系列归属时需要调用的
     * @param cInputData
     * @return
     */
    public boolean getInputForSpecial(VData cInputData)
    {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLAAssessSet = (LAAssessSet) cInputData.getObjectByObjectName(
                "LAAssessSet", 0);
        this.mIndexCalNo = (String) cInputData.getObjectByObjectName("String",
                0);
        if (this.mGlobalInput == null || this.mLAAssessSet == null)
        {
            dealError("getInputForSpecial", "传入数据不足！");
            return false;
        }
        this.MonDate = genDate();
        return true;
    }


    private boolean prepareOutputData(LAAssessSchema tAssess)
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(tAssess);
            this.mInputData.add(mIndexCalNo);
        }
        catch (Exception ex)
        {
            // @@错误处理
            ex.printStackTrace();
            dealError("prepareOutputData", "在准备往后层处理所需要的数据时出错!");
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
        return true;
    }

    private void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();
        tError.moduleName = "OAgentAscript";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;
        mErrors.addOneError(tError);
    }

//  zsj--2003-07-13--Modify: 改为界面传入
//  private void genIndexCalNo()
//  {
//   String pattern = "yyyyMM";
//   SimpleDateFormat df = new SimpleDateFormat(pattern);
//   java.util.Date today = new java.util.Date();
//   this.mIndexCalNo = df.format(today);
//  }

} //end class
