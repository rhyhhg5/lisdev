/*
 * <p>ClassName: LAEmployeeAssessAdjBL </p>
 * <p>Description: LAEmployeeAssessAdjBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agentassess;


import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAssessAccessoryBSchema;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAssessAccessoryBSet;
import com.sinosoft.lis.vschema.LAAssessAccessorySet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAEmployeeAssessAdjBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private VData mResult = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mOperate = "";
    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate(); //入机日期
    private String currentTime = PubFun.getCurrentTime(); //入机时间
    private String mManageCom = "";
    private String mAgentCode = "";
    private String mAgentGrade = "";
    private String mQueryType = "";
    private String mBranchAttr = "";
    private String mIndexCalNo = "";
    /** 业务处理相关变量 */
    private LAAssessAccessorySet mLAAssessAccessorySet = new
            LAAssessAccessorySet(); //从外部传入的一系列纪录
    private LAAssessAccessorySet mLAAssessAccessoryDelSet = new
            LAAssessAccessorySet();
    private LAAssessAccessorySet mLAAssessAccessoryInsSet = new
            LAAssessAccessorySet();
    private LAAssessAccessoryBSet mLAAssessAccessoryBSet = new
            LAAssessAccessoryBSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private String tEdorNo = ""; //转储号

    public LAEmployeeAssessAdjBL()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "aa";
        tGlobalInput.ManageCom = "86110000";

        try
        {
            PrintStream out =
                    new PrintStream(
                            new BufferedOutputStream(
                                    new FileOutputStream(
                                            "LAEmployeeAssessAdjBL.out")));
            System.setOut(out);
            VData tInputData = new VData();
            tInputData.clear();

            tInputData.add(0, "86110000");
            tInputData.add(1, "8611001196");
            tInputData.add(2, "A05");
            tInputData.add(3, "2");
            tInputData.add(4, "");
            tInputData.add(5, "200403");
            tInputData.add(6, tGlobalInput);
            LAEmployeeAssessAdjBL t = new LAEmployeeAssessAdjBL();
            if (!t.submitData(tInputData, "QUERY||MAIN"))
            {
                System.out.println(t.mErrors.getFirstError());
            }
            out.close();
        }
        catch (Exception e)
        {}
//    tInputData.add(tSet) ;
//    LAEmployeeWageAdjBL LAEmployeeWageAdjBL1=new LAEmployeeWageAdjBL();
//    if (!LAEmployeeWageAdjBL1.submitData(tInputData) )
//    {
//      for (int i=1;i<=LAEmployeeWageAdjBL1.mErrors .getErrorCount() ;i++)
//      {
//      System.out.println(LAEmployeeWageAdjBL1.mErrors .getError(i).errorMessage ) ;
//      }
//    }
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *
     * @return:
     */
    public boolean submitData(VData cInputData, String tOperate)
    {
        mOperate = tOperate;
        //得到外部传入的数据,将数据备份到本类中
        System.out.println(mOperate);
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (mOperate.equals("QUERY||MAIN"))
        {
            try
            {
                this.submitQuery();
                return true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
        }
        else
        {
            //进行业务处理
            if (!dealData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAEmployeeWageAdjBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败LAEmployeeWageAdjBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }

            //准备往后台的数据
            if (!prepareOutputData())
            {
                return false;
            }
            LAEmployeeAssessAdjBLS tLAEmployeeAssessAdjBLS = new
                    LAEmployeeAssessAdjBLS();
            if (!tLAEmployeeAssessAdjBLS.submitData(mInputData))
            {
                this.mErrors.copyAllErrors(tLAEmployeeAssessAdjBLS.mErrors);
                return false;
            }
            return true;
        }

    }

    private boolean submitQuery()
    {
        String aSql = "";
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSchema tLATreeSchema = new LATreeSchema();

        StringBuffer sb = new StringBuffer();
        sb.append("select latree.* from latree  where managecom='" + mManageCom + "' and agentcode in (select agentcode from laagent where branchtype='1' and agentstate in ('01','02') )");
        if (this.mAgentCode != null && !this.mAgentCode.equals(""))
        {
            sb.append(" and agentcode ='" + mAgentCode + "'");
        }
        if (mAgentGrade != null && !this.mAgentGrade.equals(""))
        {
            sb.append(" and agentgrade='" + mAgentGrade + "'");
        }
        sb.append(" order by agentcode asc ");
        System.out.println("sb.toString1" + sb.toString());
        mLATreeSet = tLATreeDB.executeQuery(sb.toString()); //取LATree一条纪录
        if (tLATreeDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            buildError("submitQuery", "查询出错!");
            return false;
        }
        System.out.println("resultSize:" + mLATreeSet.size());
        for (int i = 1; i <= mLATreeSet.size(); i++)
        {
            if (this.mErrors.needDealError())
            {
                return false;
            }
            tLATreeSchema = mLATreeSet.get(i);
            String tAgentCode = tLATreeSchema.getAgentCode();
            if (this.mBranchAttr != null && !this.mBranchAttr.equals(""))
            {
                if (!(AgentPubFun.getAgentBranchAttr(tAgentCode)).equals(
                        mBranchAttr)) //判断BranchAttr是否一致
                {
                    mLATreeSet.remove(tLATreeSchema);
                    i--;
                    continue;
                }
            }
            if (this.mQueryType.equals("0"))
            {
                continue;
            }
            else
            if (this.mQueryType.equals("1"))
            {
                if (!this.notConnQuery(tLATreeSchema))
                {
                    mLATreeSet.remove(tLATreeSchema);
                    i--;
                    continue;
                }
            }
            else
            if (this.mQueryType.equals("2"))
            {
                if (!this.connInAssessQuery(tLATreeSchema))
                {
                    mLATreeSet.remove(tLATreeSchema);
                    i--;
                    continue;
                }
            }
            else
            if (this.mQueryType.equals("3"))
            {
                if (!this.connNotInAssessQuery(tLATreeSchema))
                {
                    mLATreeSet.remove(tLATreeSchema);
                    i--;
                    continue;
                }
            }
        }

        SSRS tSSRS = new SSRS();
        System.out.println("mLATreeSetSize:" + mLATreeSet.size());
        for (int i = 1; i <= mLATreeSet.size(); i++)
        {

            String tAgentCode = mLATreeSet.get(i).getAgentCode().trim();
            String tAgentGroup = mLATreeSet.get(i).getAgentGroup().trim();
            String suggestGrade = mLATreeSet.get(i).getAgentLastGrade();
            String grade = mLATreeSet.get(i).getBranchCode();
            if (grade == null || grade.equals(""))
            {
                grade = "Y00";
            }
            if (suggestGrade == null || suggestGrade.equals("") ||
                !suggestGrade.startsWith("Y"))
            {
                suggestGrade = grade;
            }

            String tSQL =
                    "select a.agentcode ,b.branchattr,a.name, a.EmployDate, '" +
                    grade + "' , '" + suggestGrade +
                    "',' ' from labranchgroup b,laagent a where  "
                    + " b.agentgroup='" + tAgentGroup + "' "
                    + " and a.agentcode='" + tAgentCode + "' ";
            System.out.println(tSQL);
            ExeSQL tExeSQL = new ExeSQL();
            if (tSSRS.getMaxRow() < 1)
            {
                tSSRS = tExeSQL.execSQL(tSQL);
            }
            else
            {
                tSSRS.addRow(tExeSQL.execSQL(tSQL));
            }
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                return false;
            }

        }
        System.out.println(tSSRS.encode());
        mResult.add(tSSRS);
        return true;
    }

    private boolean connInAssessQuery(LATreeSchema cLATreeSchema)
    {
        if (cLATreeSchema.getAgentGrade().compareTo("A04") <= 0)
        {
            return false;
        }
        String tAgentCode = cLATreeSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("submitQuery", "查询" + tAgentCode + "的基本信息出错!");
            return false;
        }
        String tInDueFormDate = tLAAgentDB.getInDueFormDate(); //转正日期
        String tEmployDate = tLAAgentDB.getEmployDate(); //录用日期
        if (tInDueFormDate == null || !tInDueFormDate.equals(tEmployDate)) //两日期是否相等
        {
            return false;
        }
        else
        {
            LATreeBDB tLATreeBDB = new LATreeBDB();
            LATreeBSet tLATreeBSet = new LATreeBSet();
            String tSql = "select * from LATreeB where agentcode='" +
                          tAgentCode +
                          "' and RemoveType <>'05' order by EdorNO "; //将转储号升序排列
            tLATreeBSet = tLATreeBDB.executeQuery(tSql);
            if (tLATreeBDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLATreeBDB.mErrors);
                buildError("submitQuery", "查询" + tAgentCode + "的备份信息出错!");
                return false;
            }
            if (tLATreeBSet.size() != 0)
            {
                if ((tLATreeBSet.get(1).getAgentGrade().compareTo("A04") < 0)) //再从备份表中取最早一条纪录，如果入司时职级大于或等于A04，则为衔接主管
                {
                    return false;
                }
            }
            else
            {
                if (cLATreeSchema.getAgentGrade().compareTo("A04") < 0)
                {
                    return false;
                }
            }
            /////以上的判断可以断定该人为同业衔接主管
            String tLimitSql =
                    "select varvalue from lasysvar where vartype = 'EmployLimit'";
            ExeSQL tExeSQL = new ExeSQL();
            String EmployLimit = tExeSQL.getOneValue(tLimitSql);
            String tEmployDate1 = tEmployDate;
            if (tEmployDate.substring(8).compareTo(EmployLimit) > 0)
            {
                tEmployDate = PubFun.calDate(tEmployDate, 1, "M", null);
                tEmployDate1 = tEmployDate.substring(0, 7) + "01";
            }
            String tAreaType = "";
            String tAreaTypeSql = "select CalAssAreaType('" + tAgentCode +
                                  "') from ldsysvar where sysvar='onerow' ";
            tAreaType = tExeSQL.getOneValue(tAreaTypeSql);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                buildError("submitQuery", "判断" + tAgentCode + "对应的指标版本出错!");
                return false;
            }

            // 考核期
            String tSql1 = "select trim(code3) from ldcoderela "
                           + " where relatype = 'limitperiod' "
                           + " and (othersign = '01' or othersign is null )"
                           + " and code1 = '" + cLATreeSchema.getAgentGrade() +
                           "'";
            //  筹备期
            String tSql2 = "select trim(ArrangePeriod),nvl(linkenddate,to_date('3000-01-01','yyyy-mm-dd')) from LALinkAssess"
                           + " where '" + cLATreeSchema.getManageCom() +
                           "' like trim(ManageCom)||'%'"
                           + " and agentgrade = '" +
                           cLATreeSchema.getAgentGrade() + "' "
                           + " and areatype='" + tAreaType + "'";
            tExeSQL = new ExeSQL();
            String tAssessPeriod = tExeSQL.getOneValue(tSql1);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                buildError("submitQuery",
                           "查询" + cLATreeSchema.getAgentGrade() + "的考核期出错!");
                return false;
            }
            tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(tSql2);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                buildError("submitQuery",
                           "查询" + cLATreeSchema.getAgentGrade() + "的筹备出错!!");
                return false;
            }
            String tArrangePeriod = tSSRS.GetText(1, 1);
            String tArrangeLimit = tSSRS.GetText(1, 2);
            if (tArrangePeriod == null || tArrangePeriod.equals(""))
            {
                buildError("submitQuery",
                           "管理机构" + mManageCom + "的" +
                           cLATreeSchema.getAgentGrade() + "的筹备期信息没有");
                return false;
            }
            if (tAssessPeriod == null || tAssessPeriod.equals(""))
            {
                buildError("submitQuery",
                           "管理机构" + mManageCom + "的" +
                           cLATreeSchema.getAgentGrade() + "的考核期信息没有");
                return false;
            }
            if (AgentPubFun.formatDate(tArrangeLimit,
                                       "yyyyMM").compareTo(mIndexCalNo) > 0)
            {
                return false;
            }
            int inteval = Integer.parseInt(tArrangePeriod) +
                          Integer.parseInt(tAssessPeriod); //筹备期+考核期=衔接期
            String limitDate = PubFun.calDate(tEmployDate, inteval - 1, "M", null);
            if (AgentPubFun.formatDate(limitDate,
                                       "yyyyMM").compareTo(mIndexCalNo) <= 0) //(入司时间+衔接期)<=IndexCalNo
            {
                return false; //该同业衔接主管已经过了衔接期
            }

            inteval = 3 + Integer.parseInt(tArrangePeriod); //筹备期+3个月
            limitDate = PubFun.calDate(tEmployDate, inteval - 1, "M", null);
            if (!AgentPubFun.formatDate(limitDate, "yyyyMM").equals(mIndexCalNo)) //(入司时间+衔接期)<=IndexCalNo
            {
                return false;
            }
            else
            {
                String str1 = PubFun.calDate(tEmployDate,
                                             Integer.parseInt(tArrangePeriod),
                                             "M", null);
                System.out.println("BeginDate:" + str1);
                System.out.println("EndDate:" + mIndexCalNo);
                //cLATreeSchema.setAgentLastGrade(this.judgeEmployeeGrade(tAgentCode,AgentPubFun.formatDate(tEmployDate,"yyyyMM"),mIndexCalNo ) ) ;
                cLATreeSchema.setAgentLastGrade(this.judgeEmployeeGrade(
                        tAgentCode, AgentPubFun.formatDate(str1, "yyyyMM"),
                        mIndexCalNo));
            }
            return true;
        }

    }

    private boolean connNotInAssessQuery(LATreeSchema cLATreeSchema)
    {
        if (cLATreeSchema.getAgentGrade().compareTo("A04") < 0)
        {
            return false;
        }
        String tAgentCode = cLATreeSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("submitQuery", "查询" + tAgentCode + "的基本信息出错!");
            return false;
        }
        String tInDueFormDate = tLAAgentDB.getInDueFormDate(); //转正日期
        String tEmployDate = tLAAgentDB.getEmployDate(); //录用日期
        if (tInDueFormDate == null || !tInDueFormDate.equals(tEmployDate)) //两日期是否相等
        {
            return false;
        }
        else
        {
            LATreeBDB tLATreeBDB = new LATreeBDB();
            LATreeBSet tLATreeBSet = new LATreeBSet();
            String tSql = "select * from LATreeB where agentcode='" +
                          tAgentCode +
                          "' and  RemoveType <>'05' order by EdorNO "; //将转储号升序排列
            tLATreeBSet = tLATreeBDB.executeQuery(tSql);
            if (tLATreeBDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLATreeBDB.mErrors);
                buildError("submitQuery", "查询" + tAgentCode + "的备份信息出错!");
                return false;
            }
            if (tLATreeBSet.size() != 0)
            {
                if ((tLATreeBSet.get(1).getAgentGrade().compareTo("A04") < 0)) //再从备份表中取最早一条纪录，如果入司时职级大于或等于A04，则为衔接主管
                {
                    return false;
                }
            }
            else
            {
                if (cLATreeSchema.getAgentGrade().compareTo("A04") < 0)
                {
                    return false;
                }
            }
            /////以上的判断可以断定该人为同业衔接主管
            String tLimitSql =
                    "select varvalue from lasysvar where vartype = 'EmployLimit'";
            ExeSQL tExeSQL = new ExeSQL();
            String EmployLimit = tExeSQL.getOneValue(tLimitSql);
            String tEmployDate1 = tEmployDate;
            if (tEmployDate.substring(8).compareTo(EmployLimit) > 0)
            {
                tEmployDate = PubFun.calDate(tEmployDate, 1, "M", null);
                tEmployDate1 = tEmployDate.substring(0, 8) + "01";
            }
            System.out.println("tEmployDate:" + tEmployDate);
            System.out.println("tEmployDate1:" + tEmployDate1);
            String tAreaType = "";
            String tAreaTypeSql = "select CalAssAreaType('" + tAgentCode +
                                  "') from ldsysvar where sysvar='onerow' ";
            tAreaType = tExeSQL.getOneValue(tAreaTypeSql);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                buildError("submitQuery", "判断" + tAgentCode + "对应的指标版本出错!");
                return false;
            }

            // 考核期
            String tSql1 = "select trim(code3) from ldcoderela "
                           + " where relatype = 'limitperiod' "
                           + " and (othersign = '01' or othersign is null )"
                           + " and code1 = '" + cLATreeSchema.getAgentGrade() +
                           "'";
            //  筹备期
            String tSql2 = "select trim(ArrangePeriod) from LALinkAssess"
                           + " where '" + cLATreeSchema.getManageCom() +
                           "' like trim(ManageCom)||'%'"
                           + " and agentgrade = '" +
                           cLATreeSchema.getAgentGrade() + "' "
                           + "  and areaType='" + tAreaType + "' ";
            tExeSQL = new ExeSQL();
            String tAssessPeriod = tExeSQL.getOneValue(tSql1);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                buildError("submitQuery",
                           "查询" + cLATreeSchema.getAgentGrade() + "的考核期出错!");
                return false;
            }
            tExeSQL = new ExeSQL();
            String tArrangePeriod = tExeSQL.getOneValue(tSql2);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                buildError("submitQuery",
                           "查询" + cLATreeSchema.getAgentGrade() + "的筹备出错!!");
                return false;
            }
            if (tArrangePeriod == null || tArrangePeriod.equals(""))
            {
                buildError("submitQuery",
                           "管理机构" + mManageCom + "的" +
                           cLATreeSchema.getAgentGrade() + "的筹备期信息没有");
                return false;
            }
            if (tAssessPeriod == null || tAssessPeriod.equals(""))
            {
                buildError("submitQuery",
                           "管理机构" + mManageCom + "的" +
                           cLATreeSchema.getAgentGrade() + "的考核期信息没有");
                return false;
            }
            int inteval = Integer.parseInt(tArrangePeriod) +
                          Integer.parseInt(tAssessPeriod); //筹备期+考核期=衔接期
            String limitDate = PubFun.calDate(tEmployDate1, inteval - 1, "M", null);
            if (AgentPubFun.formatDate(limitDate,
                                       "yyyyMM").compareTo(mIndexCalNo) <= 0) //(入司时间+衔接期)<=IndexCalNo
            {
                return false; //该同业衔接主管已经过了衔接期
            }

            inteval = 3 + Integer.parseInt(tArrangePeriod); //筹备期+3个月
            limitDate = PubFun.calDate(tEmployDate1, inteval - 1, "M", null);
            if (AgentPubFun.formatDate(limitDate, "yyyyMM").equals(mIndexCalNo)) //(入司时间+衔接期)<=IndexCalNo
            {
                return false; //该同业衔接主管正好在考核期
            }
            else
            {
                return true;
            }
        }
    }

    private boolean notConnQuery(LATreeSchema cLATreeSchema)
    {

        String tAgentCode = cLATreeSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("submitQuery", "查询" + tAgentCode + "的基本信息出错!");
            return false;
        }
        String tInDueFormDate = tLAAgentDB.getInDueFormDate(); //转正日期
        String tEmployDate = tLAAgentDB.getEmployDate(); //录用日期
        if (tInDueFormDate == null || !tInDueFormDate.equals(tEmployDate)) //两日期是否相等
        {
            return true;
        }

        else
        {
            LATreeBDB tLATreeBDB = new LATreeBDB();
            LATreeBSet tLATreeBSet = new LATreeBSet();
            String tSql = "select * from LATreeB where agentcode='" +
                          tAgentCode +
                          "'and  RemoveType <>'05' order by EdorNO "; //将转储号升序排列
            tLATreeBSet = tLATreeBDB.executeQuery(tSql);
            if (tLATreeBDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLATreeBDB.mErrors);
                buildError("submitQuery", "查询" + tAgentCode + "的备份信息出错!");
                return false;
            }
            if (tLATreeBSet.size() != 0)
            {
                if ((tLATreeBSet.get(1).getAgentGrade().compareTo("A04") < 0)) //再从备份表中取最早一条纪录，如果入司时职级大于或等于A04，则为衔接主管
                {
                    return true;
                }
                else
                {
                    return hasPassConnPeriod(tLATreeBSet.get(1).getAgentCode(),
                                             tEmployDate,
                                             tLATreeBSet.get(1).getAgentGrade().
                                             trim());
                }
            }
            else
            {
                if (cLATreeSchema.getAgentGrade().compareTo("A04") < 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    }

    private boolean hasPassConnPeriod(String cAgentCode, String cEmployeeDate,
                                      String cAgentGrade)
    {
        /////////////////////////////////////////
        String tLimitSql =
                "select varvalue from lasysvar where vartype = 'EmployLimit'";
        ExeSQL tExeSQL = new ExeSQL();
        String EmployLimit = tExeSQL.getOneValue(tLimitSql);
        String tEmployDate1 = cEmployeeDate;
        if (cEmployeeDate.substring(8).compareTo(EmployLimit) > 0)
        {
            cEmployeeDate = PubFun.calDate(cEmployeeDate, 1, "M", null);
            tEmployDate1 = cEmployeeDate.substring(0, 7) + "01";
        }
        String tAreaType = "";
        String tAreaTypeSql = "select CalAssAreaType('" + cAgentCode +
                              "') from ldsysvar where sysvar='onerow' ";
        tAreaType = tExeSQL.getOneValue(tAreaTypeSql);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            buildError("submitQuery", "判断" + cAgentCode + "对应的指标版本出错!");
            return false;
        }

        // 考核期
        String tSql1 = "select trim(code3) from ldcoderela "
                       + " where relatype = 'limitperiod' "
                       + " and (othersign = '01' or othersign is null )"
                       + " and code1 = '" + cAgentGrade + "'";
        //  筹备期
        String tSql2 = "select trim(ArrangePeriod) from LALinkAssess"
                       + " where '" + mManageCom +
                       "' like trim(ManageCom)||'%'"
                       + " and agentgrade = '" + mAgentGrade + "' "
                       + " and  areatype='" + tAreaType + "'";
        tExeSQL = new ExeSQL();
        String tAssessPeriod = tExeSQL.getOneValue(tSql1);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            buildError("submitQuery", "查询" + cAgentGrade + "的考核期出错!");
            return false;
        }
        tExeSQL = new ExeSQL();
        String tArrangePeriod = tExeSQL.getOneValue(tSql2);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            buildError("submitQuery", "查询" + cAgentGrade + "的筹备出错!!");
            return false;
        }
        if (tArrangePeriod == null || tArrangePeriod.equals(""))
        {
            buildError("submitQuery",
                       "管理机构" + mManageCom + "的" + cAgentGrade + "的筹备期信息没有");
            return false;
        }
        if (tAssessPeriod == null || tAssessPeriod.equals(""))
        {
            buildError("submitQuery",
                       "管理机构" + mManageCom + "的" + cAgentGrade + "的考核期信息没有");
            return false;
        }
        int inteval = Integer.parseInt(tArrangePeriod) +
                      Integer.parseInt(tAssessPeriod); //筹备期+考核期=衔接期
        String limitDate = PubFun.calDate(cEmployeeDate, inteval - 1, "M", null);
        if (AgentPubFun.formatDate(limitDate, "yyyyMM").compareTo(mIndexCalNo) <
            0) //(入司时间+衔接期)>IndexCalNo
        {
            return true; //该同业衔接主管已经过了衔接期
        }
        else
        {
            return false;
        }
        ////////////////////////////////////////////
    }

    private String judgeEmployeeGrade(String tAgentCode, String tStartMonth,
                                      String tEndDate)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tSum = "";
        System.out.println(tSum);
        double tSumTransMoney = 0;
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(tAgentCode);
        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            return null;
        }
        String tAgentGrade = tLATreeDB.getAgentGrade();
        if (tAgentGrade.equals("A06") || tAgentGrade.equals("A08"))
        {
            String tAgentGroup = tLATreeDB.getAgentGroup();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tAgentGroup);
            if (!tLABranchGroupDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAEmployeeWageAdjBL";
                tError.functionName = "judgeEmployeeGrade";
                tError.errorMessage = "未查到机构" + tAgentGroup + "的机构信息";
                this.mErrors.addOneError(tError);
                return null;
            }
            String tBranchAttr = tLABranchGroupDB.getBranchAttr();
            String sql =
                    "select nvl(sum(standprem),0) from lacommision where branchattr like '" +
                    tBranchAttr + "%' and wageno>='" + tStartMonth +
                    "' and wageno<='" + tEndDate + "'";
            tSum = tExeSQL.getOneValue(sql);
            System.out.println("A06或A08-业绩查询Sql:" + sql);
            tSumTransMoney = Double.parseDouble(tSum);
            System.out.println("业绩总数：" + tSumTransMoney);
        }
        else if (tAgentGrade.equals("A05") || tAgentGrade.equals("A07") ||
                 tAgentGrade.equals("A09"))
        {
            tExeSQL = new ExeSQL();
            SSRS tSSRS = new SSRS();
            String tSQL =
                    "select distinct agentcode from latreeaccessory where rearagentcode='" +
                    tAgentCode + "' and CommBreakFlag <>'1' ";
            tSSRS = tExeSQL.execSQL(tSQL);
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                String agentcode = tSSRS.GetText(i, 1);
                tLATreeDB.setAgentCode(agentcode);
                if (!tLATreeDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                    return null;
                }
                String agentgrade = tLATreeDB.getAgentGrade();
                String sql = "";
                if (tAgentGrade.equals("A05"))
                {
                    sql = "select * from labranchgroup where branchmanager='" +
                          agentcode +
                          "' and branchlevel='01' and endflag<>'Y' ";
                }
                else if (tAgentGrade.equals("A07"))
                {
                    if (agentgrade.equals("A08") || agentgrade.equals("A09") ||
                        agentgrade.equals("A06") || agentgrade.equals("A07"))
                    {
                        sql =
                                "select * from labranchgroup where branchmanager='" +
                                agentcode +
                                "' and branchlevel='02' and endflag<>'Y' ";
                    }
                }
                else if (tAgentGrade.equals("A09"))
                {
//          if (agentgrade.equals("A08"))
//          {
//             sql="select * from labranchgroup where branchmanager='"+agentcode+"' and branchlevel='03' and endflag<>'Y'  ";
//          }
//          else
                    if (agentgrade.equals("A09"))
                    {
                        sql =
                                "select * from labranchgroup where branchmanager='" +
                                agentcode +
                                "' and branchlevel='04' and endflag<>'Y' ";
                    }
                }
                if (sql.equals(""))
                {
                    continue;
                }
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
                tLABranchGroupSet = tLABranchGroupDB.executeQuery(sql);
                if (tLABranchGroupDB.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeWageAdjBL";
                    tError.functionName = "judgeEmployeeGrade";
                    tError.errorMessage = "未查到机构主管为" + agentcode + "的机构信息";
                    this.mErrors.addOneError(tError);
                    return null;
                }
                if (tLABranchGroupSet.size() == 0)
                {
                    continue;
                }
                String branchattr = tLABranchGroupSet.get(1).getBranchAttr();
                String tsql =
                        "select nvl(sum(standprem),0) from lacommision where branchattr like '" +
                        branchattr + "%' and wageno>='" + tStartMonth +
                        "' and wageno<='" + tEndDate + "'";
                tSum = tExeSQL.getOneValue(tsql);
                System.out.println(tSum);
                System.out.println(tSumTransMoney);
                tSumTransMoney = tSumTransMoney + Double.parseDouble(tSum);
                System.out.println(tSumTransMoney);

            }
            tLATreeDB.setAgentCode(tAgentCode);
            if (!tLATreeDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                return null;
            }
            String agentgroup = tLATreeDB.getAgentGroup();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(agentgroup);
            if (!tLABranchGroupDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAEmployeeWageAdjBL";
                tError.functionName = "judgeEmployeeGrade";
                tError.errorMessage = "未查到机构" + agentgroup + "的机构信息";
                this.mErrors.addOneError(tError);
                return null;
            }
            String branchattr = tLABranchGroupDB.getBranchAttr();
            String sql =
                    "select nvl(sum(standprem),0) from lacommision where branchattr like '" +
                    branchattr + "%' and wageno>='" + tStartMonth +
                    "' and wageno<='" + tEndDate + "'";
            tSum = tExeSQL.getOneValue(sql);
            System.out.println(tSum);
            System.out.println(tSumTransMoney);
            tSumTransMoney = tSumTransMoney + Double.parseDouble(tSum);
            System.out.println(tSumTransMoney);
            System.out.println("业绩总数：" + tSumTransMoney);
        }

        String tManageCom = tAgentCode.substring(0, 6);
        String tCurrAgentGrade = "";
        String tSQL1 = "select agentgrade ,sum(baserate) from lalinkwage where wagecode='W00058' and managecom='" +
                       tManageCom +
                       "' and trim(paymonth)<='03' and payperiodtype='02' "
                       + " group by agentgrade "
                       + " order by agentgrade desc";
        tExeSQL = new ExeSQL();
        SSRS nSSRS = new SSRS();
        nSSRS = tExeSQL.execSQL(tSQL1);
        System.out.println("查询各职级指标SQL" + tSQL1);

        String agentgrade = "";
        double tSumStand = 0;

        for (int i = 1; i <= nSSRS.getMaxRow(); i++)
        {
            tSumStand = Double.parseDouble(nSSRS.GetText(i, 2));
            agentgrade = nSSRS.GetText(i, 1);
            System.out.println("职级" + agentgrade + "的标准为：" + tSumStand);
            System.out.println("此人的业绩为：" + tSumTransMoney);
            if (tSumTransMoney >= tSumStand)
            {
                tExeSQL = new ExeSQL();
                String sql =
                        "select code2 from ldcoderela where trim(relatype)='gradetowage' and code1='" +
                        agentgrade + "'";
                String tResult = tExeSQL.getOneValue(sql);
                return tResult;
            }
        }
        return "Y00";

    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        LAAssessAccessorySchema tLAAssessAccessorySchema = new
                LAAssessAccessorySchema();
        String currrentData = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        for (int i = 1; i <= mLAAssessAccessorySet.size(); i++)
        {
            tLAAssessAccessorySchema = this.mLAAssessAccessorySet.get(i);
//      tLAAssessAccessorySchema.setAgentGrade(getOldAgentGrade(tLAAssessAccessorySchema.getAgentCode() )) ;
//      tLAAssessAccessorySchema.setAgentGrade1(tLAAssessAccessorySchema.getAgentGrade() ) ;

            String tFirstAssessFlag = "1";
            String tAgentCode = tLAAssessAccessorySchema.getAgentCode();
            String tIndexCalNo = tLAAssessAccessorySchema.getIndexCalNo();
            String tSQL = "select * from laassessaccessory where agentcode='" +
                          tAgentCode + "' and indexcalno='" + tIndexCalNo +
                          "' and AssessType='01' ";
            System.out.println(tSQL);
            LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
            LAAssessAccessorySet tLAAssessAccessorySet = new
                    LAAssessAccessorySet();
            tLAAssessAccessorySet = tLAAssessAccessoryDB.executeQuery(tSQL);
            if (tLAAssessAccessoryDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLAAssessAccessoryDB.mErrors);
                return false;
            }
            if (tLAAssessAccessorySet.size() > 0)
            {
                tFirstAssessFlag = "0";
                String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                for (int j = 1; j <= tLAAssessAccessorySet.size(); j++)
                {
                    LAAssessAccessorySchema tSchema = new
                            LAAssessAccessorySchema();
                    tSchema = tLAAssessAccessorySet.get(j);
                    LAAssessAccessoryBSchema tBSchema = new
                            LAAssessAccessoryBSchema();
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tBSchema, tSchema);
                    tBSchema.setEdorNo(tEdorNo);
                    tBSchema.setEdorType("01");
                    tBSchema.setEdorDate(currentDate);
                    this.mLAAssessAccessoryBSet.add(tBSchema);
                    this.mLAAssessAccessoryDelSet.add(tSchema);
                    ;
                }
            }
            String series = this.getSeries(tLAAssessAccessorySchema.
                                           getAgentGrade1());
            tLAAssessAccessorySchema.setAgentSeries1(series);
            tLAAssessAccessorySchema.setCalAgentSeries(series);
            tLAAssessAccessorySchema.setAgentSeries(getSeries(
                    tLAAssessAccessorySchema.getAgentGrade()));
            tLAAssessAccessorySchema.setAssessType("01");
            tLAAssessAccessorySchema.setCalAgentSeries(getSeries(
                    tLAAssessAccessorySchema.getCalAgentGrade()));
            String tModifyFlag = "";
            if (tLAAssessAccessorySchema.getAgentGrade1().compareTo(
                    tLAAssessAccessorySchema.getCalAgentGrade()) < 0)
            {
                tModifyFlag = "03";
            }
            else
            if (tLAAssessAccessorySchema.getAgentGrade1().compareTo(
                    tLAAssessAccessorySchema.getCalAgentGrade()) > 0)
            {
                tModifyFlag = "01";
            }
            else
            {
                tModifyFlag = "02";
            }
            tLAAssessAccessorySchema.setModifyFlag(tModifyFlag);
            tLAAssessAccessorySchema.setFirstAssessFlag(tFirstAssessFlag);
            tLAAssessAccessorySchema.setConfirmDate(currentDate);
            tLAAssessAccessorySchema.setConfirmer(mGlobalInput.Operator);
            tLAAssessAccessorySchema.setMakeDate(currentDate);
            tLAAssessAccessorySchema.setMakeTime(currentTime);
            tLAAssessAccessorySchema.setModifyDate(currentDate);
            tLAAssessAccessorySchema.setModifyTime(currentTime);
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(tLAAssessAccessorySchema.getAgentCode());
            if (!tLATreeDB.getInfo())
            {
                return false;
            }
            tLAAssessAccessorySchema.setAgentGroup(tLATreeDB.getAgentGroup());
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tLATreeDB.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                return false;
            }
            tLAAssessAccessorySchema.setBranchType(tLAAgentDB.getBranchType());
            tLAAssessAccessorySchema.setBranchAttr(AgentPubFun.
                    getAgentBranchAttr(tLAAgentDB.getAgentCode()));
//      tLAAssessAccessorySchema.setAgentGroupNew(tLATreeDB.getAgentGroup() ) ;
            tLAAssessAccessorySchema.setManageCom(tLAAgentDB.getManageCom());
            tLAAssessAccessorySchema.setState("1");
            tLAAssessAccessorySchema.setStandAssessFlag("0");
            tLAAssessAccessorySchema.setOperator(mGlobalInput.Operator);
            this.mLAAssessAccessoryInsSet.add(tLAAssessAccessorySchema);
        }
        return true;
    }

//  private String getOldAgentGrade(String tAgentCode)
//  {
//    LATreeDB tLATreeDB=new LATreeDB();
//    String tsql="select * from latree where agentcode='"+tAgentCode+"'";
//    LATreeSet mLATreeSet=new LATreeSet();
//    mLATreeSet=tLATreeDB.executeQuery(tsql) ;
//    if (tLATreeDB.mErrors .needDealError()  )
//    {
//      this.mErrors .copyAllErrors(tLATreeDB.mErrors ) ;
//      return null;
//    }
//    return mLATreeSet.get(1).getBranchCode() ;
//  }
    private String getSeries(String tEmployeeGrade)
    {
        System.out.println("into");
        String tSQL =
                "select code2 from ldcoderela where trim(relatype)='wagetoseries' and code1='" +
                tEmployeeGrade + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String result = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            return null;
        }
        return result.trim();
    }

    private void buildError(String FuncName, String ErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAEmployeeWageAdjBL";
        cError.functionName = FuncName;
        cError.errorMessage = ErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            if (mOperate.equals("QUERY||MAIN"))
            {
                this.mManageCom = (String) cInputData.get(0);
                this.mAgentCode = (String) cInputData.get(1);
                this.mAgentGrade = (String) cInputData.get(2);
                this.mQueryType = (String) cInputData.get(3);
                this.mBranchAttr = (String) cInputData.get(4);
                this.mIndexCalNo = (String) cInputData.get(5);
                return true;
            }
            else
            {
                this.mLAAssessAccessorySet = (LAAssessAccessorySet) cInputData.
                                             getObjectByObjectName(
                        "LAAssessAccessorySet", 0);
                this.mGlobalInput = ((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
                return true;
            }
        }
        catch (Exception e)
        {
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "获取前台传入的参数时失败。";
            this.mErrors.addOneError(tError);
            return false;
        }

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.addElement(this.mLAAssessAccessoryBSet);
            this.mInputData.addElement(this.mLAAssessAccessoryInsSet);
            this.mInputData.addElement(this.mLAAssessAccessoryDelSet);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }


}
