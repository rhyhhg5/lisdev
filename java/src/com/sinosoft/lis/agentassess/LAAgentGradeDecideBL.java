package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.lis.agent.ALADimissionAppUI;
import com.sinosoft.lis.agentassess.LAAssessInputUI;

/**
 * 考核评估计算程序
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAAgentGradeDecideBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSet mLAAssessSet1 = new LAAssessSet();  //非清退人员
    private LAAssessSet mLAAssessSet2 = new LAAssessSet();  //清退人员
    private LAAssessSet mLAAssessSet3 = new LAAssessSet();  //延期转正
    private LAAssessHistorySchema mLAAssessHistorySchema = new LAAssessHistorySchema();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private String mEvaluateDate = "";           // 异动时间
    private String mManageCom = "";              // 管理机构
    private String mIndexCalNo = "";             // 考核序列号
    private String mGradeSeries = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    public static void main(String args[])
    {
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //取得异动时间
        if(!getEvaluateDate())
            return false;
        //处理前进行验证
        if(!check())
            return false;
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        //进行人员职级异动处理
        if(!dealAgentEvaluate())
            return false;
        //处理延期转正
        if(!dealAssess3())
            return false;
        //对考核主表进行处理
        if(!dealLAAssessMain())
            return false;

        //如果有可以转正的人员 进行转正处理
    //    if(!dealInDue())
    //        return false;

        return true;
    }

    /**
     * 进行转正处理
     * @return boolean
     */
    private boolean dealInDue()
    {
        LAAgentSet tLAAgentSet = new LAAgentSet();
        String tInDutDAte = currentDate.substring(0,currentDate.length()-2) + "-1";

        // 如果没有需要进行转正处理的人员 直接跳过该操作
        if(this.mLAAgentSet.size() == 0)
        {
            return true;
        }

        // 进行转正处理加入转正日期
        for(int i=1;i<mLAAgentSet.size();i++)
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(mLAAgentSet.get(i).getAgentCode());
            if(!tLAAgentDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAAgentGradeDecideBL";
                tError.functionName = "dealInDue";
                tError.errorMessage = "职级归属正常完成，但需要转正的人员处理失败！";
                System.out.println("查询转正业务员信息失败！");
                this.mErrors.addOneError(tError);

                return false;
            }
            System.out.println(tLAAgentDB.getInDueFormDate());
            tLAAgentDB.setInDueFormDate(tInDutDAte);
             System.out.println(tLAAgentDB.getInDueFormDate());
            tLAAgentSet.add(tLAAgentDB);
            System.out.println(""+tLAAgentSet.get(i).getInDueFormDate());
        }

        mLAAgentSet.clear();
        mLAAgentSet.set(tLAAgentSet);
         for(int i=1;i<mLAAgentSet.size();i++)
         {
             System.out.println("" + mLAAgentSet.get(i).getInDueFormDate());
         }
        VData tOutputDate = new VData();
        MMap tMap=new MMap();

        tMap.put(mLAAgentSet, "UPDATE");
        tOutputDate.add(tMap);
        PubSubmit tPubSubmit=new PubSubmit();
        tPubSubmit.submitData(tOutputDate,"");

        if(tPubSubmit.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecideBL";
            tError.functionName = "dealInDue";
            tError.errorMessage = "职级归属完成，但需要转正的人员处理失败！";
            System.out.println("写入转正信息时出错！");
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 人员新职级归属完毕以后更改考核主表的状态
     * @return boolean
     */
    private boolean dealLAAssessMain()
    {
        LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
        LAAssessMainSet tLAAssessMainSet = new LAAssessMainSet();
        LAAssessMainSchema tLAAssessMainSchema = new LAAssessMainSchema();

        tLAAssessMainDB.setIndexCalNo(this.mIndexCalNo);
        tLAAssessMainDB.setAgentGrade(this.mGradeSeries);
        tLAAssessMainDB.setManageCom(this.mManageCom);
        tLAAssessMainDB.setAssessType("00");
        tLAAssessMainSet = tLAAssessMainDB.query();

        if(tLAAssessMainDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecideBL";
            tError.functionName = "dealLAAssessMain";
            tError.errorMessage = "设置考核结束标记时失败！";
            System.out.println("查询考核主表信息失败！");
            this.mErrors.addOneError(tError);

            return false;
        }

        tLAAssessMainSchema = tLAAssessMainSet.get(1);
        tLAAssessMainDB.setSchema(tLAAssessMainSchema);
        tLAAssessMainDB.setState("02");
        tLAAssessMainDB.setModifyDate(currentDate);
        tLAAssessMainDB.setModifyTime(currentTime);

        if(!tLAAssessMainDB.update())
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecideBL";
            tError.functionName = "dealLAAssessMain";
            tError.errorMessage = "设置考核结束标记时失败！";
            System.out.println("更新考核主表信息失败！");
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 分类处理清退人员和职级发生变化的人员
     * @return boolean
     */
    private boolean dealAgentEvaluate()
    {
        //处理离职人员信息变量
        LADimissionSchema tLADimissionSchema = new LADimissionSchema();
        VData tVData = new VData();

        //处理清退人员
        if(mLAAssessSet2.size() > 0)
        {
            for(int i=1;i<=mLAAssessSet2.size();i++)
            {
                tLADimissionSchema = new LADimissionSchema();
                tLADimissionSchema.setAgentCode(mLAAssessSet2.get(i).getAgentCode());
                tLADimissionSchema.setApplyDate(mEvaluateDate); //处理时间
                tLADimissionSchema.setDepartRsn("0");  //离职原因
                tLADimissionSchema.setNoti("维持考核不达标！"); //备注
                tLADimissionSchema.setOperator(this.mGlobalInput.Operator);
                tLADimissionSchema.setBranchType(mLAAssessSet2.get(i).getBranchType());
                tLADimissionSchema.setBranchType2(mLAAssessSet2.get(i).getBranchType2());
                tLADimissionSchema.setDepartTimes("1");

                tVData = new VData();
                tVData.addElement(tLADimissionSchema);
                tVData.add(this.mGlobalInput);

                ALADimissionAppUI tLADimission = new ALADimissionAppUI();
                if(!tLADimission.submitData(tVData,"INSERT||MAIN"))
                {
                    this.mErrors.copyAllErrors(tLADimission.mErrors);
                    System.out.println("进行离职人员处理失败！");
                    return false;
                }
            }
        }

        //处理非清退人员(职级异动)
        if(mLAAssessSet1.size() > 0)
        {
            LAAssessAccessorySet tLAAssessAccessorySet = new LAAssessAccessorySet();
            LAAssessAccessorySchema tLAAssessAccessorySchema = new LAAssessAccessorySchema();

            for(int j=1;j<=mLAAssessSet1.size();j++)
            {
                tLAAssessAccessorySchema = new LAAssessAccessorySchema();
                LAAssessSchema tLAAssessSchema = new LAAssessSchema();
                tLAAssessSchema.setSchema(mLAAssessSet1.get(j));

                // 把原职级为'A01' 人事变动标记为'02' 晋升的统计起来 准备做转正
                if(("A01".equals(tLAAssessSchema.getAgentGrade())
                   || "D01".equals(tLAAssessSchema.getAgentGrade()))
                   && "03".equals(tLAAssessSchema.getModifyFlag()))
                {
                    LAAgentDB tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(tLAAssessSchema.getAgentCode());
                    if(!tLAAgentDB.getInfo())
                    {
                        continue;
                    }
                    this.mLAAgentSet.add(tLAAgentDB);
                }
                LATreeDB tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
                tLATreeDB.getInfo();
                String tNewAgentGroup=tLATreeDB.getSchema().getAgentGroup();
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setAgentGroup(tNewAgentGroup);
                tLABranchGroupDB.getInfo();
                String tBranchAttr=tLABranchGroupDB.getSchema().getBranchAttr();
                tLAAssessAccessorySchema = new LAAssessAccessorySchema();
                tLAAssessAccessorySchema.setAgentCode(tLAAssessSchema.getAgentCode());
                tLAAssessAccessorySchema.setBranchAttr(tBranchAttr);
                tLAAssessAccessorySchema.setAgentGrade(tLAAssessSchema.getAgentGrade());
                tLAAssessAccessorySchema.setAgentGrade1(tLAAssessSchema.getAgentGrade1());
                tLAAssessAccessorySchema.setAgentGrade1(tLAAssessSchema.getCalAgentGrade());
                tLAAssessAccessorySchema.setAgentGroup(tLAAssessSchema.getAgentGroup());
                tLAAssessAccessorySchema.setAgentGroupNew(tBranchAttr);
                tLAAssessAccessorySchema.setAgentSeries(tLAAssessSchema.getAgentSeries());
                tLAAssessAccessorySchema.setAssessType("00"); //默认为职级考核维度
                tLAAssessAccessorySchema.setManageCom(tLAAssessSchema.getManageCom());
                tLAAssessAccessorySchema.setBranchType(tLAAssessSchema.getBranchType());
                tLAAssessAccessorySchema.setBranchType2(tLAAssessSchema.getBranchType2());
                tLAAssessAccessorySchema.setIndexCalNo(tLAAssessSchema.getIndexCalNo());
                //区分值：1 根据基本法考核
                tLAAssessAccessorySchema.setStandAssessFlag("1");

                tLAAssessAccessorySet.add(tLAAssessAccessorySchema);
            }
            tVData = new VData();
            tVData.add(this.mGlobalInput);
            tVData.addElement(tLAAssessAccessorySet);
            tVData.addElement(mLAAssessSet1.get(1).getBranchType());
            tVData.addElement(mLAAssessSet1.get(1).getBranchType2());

            LAAssessInputUI tLAAssessInputUI  = new LAAssessInputUI();
            if(!tLAAssessInputUI.submitData(tVData,"INSERT||MAIN"))
            {
                this.mErrors.copyAllErrors(tLAAssessInputUI.mErrors);
                System.out.println("进行人员职级异动时处理失败！");
                return false;
            }
        }

        return true;
    }

    private boolean dealAssess3()
    {


       if(mLAAssessSet3.size()>=1)
       {
           LATreeSet tLATreeSet = new LATreeSet();
           for(int i=1;i<=mLAAssessSet3.size();i++)
           {
               LAAssessSchema tLAAssessSchema = new LAAssessSchema();
               tLAAssessSchema=mLAAssessSet3.get(i);
               LATreeSchema tLATreeSchema = new LATreeSchema();
               LATreeDB tLATreeDB = new LATreeDB();
               tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
               tLATreeDB.getInfo();
               tLATreeSchema=tLATreeDB.getSchema();
               tLATreeSchema.setState("4");
               tLATreeSchema.setModifyDate(currentDate);
               tLATreeSchema.setModifyTime(currentTime);
               tLATreeSet.add(tLATreeSchema);
           }
           if(tLATreeSet.size()>=1)
           {
               VData tOutputDate = new VData();
               MMap tMap=new MMap();

               tMap.put(tLATreeSet, "UPDATE");
               tOutputDate.add(tMap);
               PubSubmit tPubSubmit=new PubSubmit();
               tPubSubmit.submitData(tOutputDate,"");
               if(tPubSubmit.mErrors.needDealError())
               {
                   CError tError = new CError();
                   tError.moduleName = "LAAgentGradeDecideBL";
                   tError.functionName = "dealInDue";
                   tError.errorMessage = "职级归属完成，但需要延期转正的人员处理失败！";
                   System.out.println("写入转正信息时出错！");
                   this.mErrors.addOneError(tError);
                   return false;
               }

           }
       }
        return true;
    }

    /**
     * 取得异动时间(考核期次月一号)
     * @return boolean
     */
    private boolean getEvaluateDate()
    {
        String tSQL = "";

        tSQL  = "select date(substr('" + mIndexCalNo + "',1,4)||'-'|| substr(";
        tSQL += "'" + mIndexCalNo + "',5,2)||'-01')+1 months from dual";

        ExeSQL tExeSQL = new ExeSQL();
        // 取得异动时间
        mEvaluateDate = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecideBL";
            tError.functionName = "getEvaluateDate";
            tError.errorMessage = "获取异动日期失败！";
            System.out.println("获取异动日期失败！");
            this.mErrors.addOneError(tError);

            return false;
        }
        System.out.println("获得异动日期："+mEvaluateDate);
        return true;
    }

    /**
     * 验证方法
     * @return boolean
     */
    private boolean check()
    {
        String tSQL = "";
        //检验当期考核是否归属过
        tSQL = "select count(*) from laassessmain where indexcalno='";
        tSQL += mIndexCalNo + "' and managecom='";
        tSQL += mManageCom + "' and state='02'";
        tSQL += " and AgentGrade = '" + this.mGradeSeries + "'";
        tSQL += " and BranchType='"+this.mLAAssessHistorySchema.getBranchType()+"'";
        tSQL += " and BranchType2='"+this.mLAAssessHistorySchema.getBranchType2()+"'";

        if (execQuery(tSQL) > 0) {
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecideBL";
            tError.functionName = "check";
            tError.errorMessage = "本期考核该机构已经归属完毕，请确认！";
            System.out.println("本期考核该机构已经归属完毕，请确认！");
            this.mErrors.addOneError(tError);

            return false;
        }

        //检验当期考核是否已经进行过考核确认
        tSQL = "select count(*) from laassessmain where indexcalno='";
        tSQL += mIndexCalNo + "' and managecom='";
        tSQL += mManageCom + "' and state='01' ";
        tSQL += " and AgentGrade = '" + this.mGradeSeries + "'";
        tSQL += " and BranchType='"+this.mLAAssessHistorySchema.getBranchType()+"'";
        tSQL += " and BranchType2='"+this.mLAAssessHistorySchema.getBranchType2()+"'";

        if (execQuery(tSQL) < 1) {
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecideBL";
            tError.functionName = "check";
            tError.errorMessage = "本期考核该机构未进行考核确认，请确认！";
            System.out.println("本期考核该机构未进行考核确认，请确认！");
            this.mErrors.addOneError(tError);

            return false;
        }


        System.out.println("验证通过！");
        return true;
    }

    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
        LAAssessSet tLAAssessSet = new LAAssessSet();
        LAAssessDB tLAAssessDB = new LAAssessDB();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        int tAgentCont = 0;
        String tSQL = "";

        // 查询处该处参加考核的所有人员
        //tLAAssessDB.setManageCom(this.mManageCom);
        //tLAAssessDB.setIndexCalNo(this.mIndexCalNo);

        tSQL  = "SELECT * FROM LAAssess WHERE IndexCalNo='"+this.mIndexCalNo+"'";
        tSQL += " AND ManageCom LIKE '"+this.mManageCom+"%'";
        if("2".equals(this.mLAAssessHistorySchema.getBranchType()))
        {
            tSQL += " AND AgentGrade LIKE  '" +
                    this.mGradeSeries.subSequence(0, 1) + "%'";
        }
        tSQL += " AND BranchType = '"+this.mLAAssessHistorySchema.getBranchType()+"'";
        tSQL += " AND BranchType2 = '"+this.mLAAssessHistorySchema.getBranchType2()+"'";
System.out.println("查询LAAssess SQL:" + tSQL);
        tLAAssessSet = tLAAssessDB.executeQuery(tSQL);
        System.out.println("参加考核人员有[ "+tLAAssessSet.size()+" ]位");
        if(tLAAssessDB.mErrors.needDealError())
        {
            //this.mErrors.copyAllErrors(tLAAssessDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "LAAgentGradeDecideBL";
            tCError.functionName = "dealData";
            tCError.errorMessage = "查询参加考核的人员信息失败！";
            this.mErrors.addOneError(tCError);
            System.out.println("查询参加考核的人员信息失败！");

            return false;
        }
        if (tLAAssessSet.size() < 1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecideBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有找到参加考核的人员信息！";
            System.out.println("没有找到参加考核的人员信息！");
            this.mErrors.addOneError(tError);

            return false;
        }

        //取得参加考核人员人数
        tAgentCont = tLAAssessSet.size();

        //循环处理每个考核人员新职级归属
        for(int i=1;i<=tAgentCont;i++)
        {
            tLAAssessSchema = tLAAssessSet.get(i);
            if(!dealAgentEvaluate(tLAAssessSchema))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAgentGradeDecideBL";
                tError.functionName = "dealData";
                tError.errorMessage = "进行人员新职级归属操作时失败！";
                System.out.println("进行人员新职级归属操作时失败！");
                this.mErrors.addOneError(tError);

                return false;
            }
        }

        return true;
    }

    /**
     * 对每个参加考核的人员进行逐个处理
     * @param pmLAAssessSchema LAAssessSchema
     * @return boolean
     */
    private boolean dealAgentEvaluate(LAAssessSchema pmLAAssessSchema)
    {
        String tModifyFlag = pmLAAssessSchema.getModifyFlag();   //升降级标记
        String tAgentGrade = pmLAAssessSchema.getAgentGrade();   //原职级
        String tAgentGrade1 = pmLAAssessSchema.getCalAgentGrade(); //新职级
        String tAssessAgentGrade = pmLAAssessSchema.getAgentGrade1(); //考核得到的职级
        //判断业务员升降级分别进行处理 01:降级 02：维持 03：晋级
        if ("01".equals(tModifyFlag) || "03".equals(tModifyFlag))
        {
            // 处理清退职级业务员
            if ("A00".equals(tAgentGrade1) || "D00".equals(tAgentGrade1))
            {
                //对清退业务员进行离职处理
                mLAAssessSet2.add(pmLAAssessSchema);
            }else
            {
                //进行新职级归属
                mLAAssessSet1.add(pmLAAssessSchema);
            }
        }
        else if ("02".equals(tModifyFlag) )
        {
            if ( "D01".equals(tAgentGrade1) && "D00".equals(tAssessAgentGrade))
            {
                mLAAssessSet3.add(pmLAAssessSchema);  //延期转正
            }

        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAgentGradeDecideBL.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLAAssessHistorySchema.setSchema((LAAssessHistorySchema)cInputData
                .getObjectByObjectName("LAAssessHistorySchema",0));

        this.mManageCom = mLAAssessHistorySchema.getManageCom();
        this.mIndexCalNo = mLAAssessHistorySchema.getIndexCalNo();
        this.mGradeSeries = mLAAssessHistorySchema.getAgentGrade();

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecideBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql)
    {
        System.out.println(sql);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0;
            st = conn.prepareStatement(sql);
            if (st == null)return 0;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                if (!conn.isClosed()) {
                    conn.close();
                }
                try {
                    st.close();
                    rs.close();
                } catch (Exception ex2) {
                    ex2.printStackTrace();
                }
                st = null;
                rs = null;
                conn = null;
              } catch (Exception e) {}
        }
    }
}
