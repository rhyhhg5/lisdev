package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.agent.ALADimissionAppUI;
import com.sinosoft.lis.agentassess.LAAssessInputUI;


public class LAAgentRegularU01InputBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往后面传输数据的容器 ，进行回退计算*/
    private VData mVData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    
    //**
    private MMap mMMap=new MMap();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    //private LAAssessSet mLAAssessSet = new LAAssessSet();  //要回退的人员
    //回退轨迹
    //private LARollBackTraceSet mLARollBackTraceSet = new LARollBackTraceSet();

    //private LAAssessHistorySchema mLAAssessHistorySchema = new LAAssessHistorySchema();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeSchema mLATreeSchema  = new LATreeSchema(); 
    private LATreeBSchema mLATreeBSchema= new LATreeBSchema();
    private String newAgentGrade="";
    private String mEvaluateDate = "";           // 异动时间
    private String mManageCom = "";              // 管理机构
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mGradeSeries = "";
    private String tEdorNo = "";    //
    private String mAgentcode = "";   //业务员编码
    private String mgroupAgentcode = "";   //业务员集团统一工号
    private String indueformdate = "";
    private String InDueFormFlag = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    VData data = new VData();
    public LAAgentRegularU01InputBL()
    {
    }
    public static void main(String args[])
    {
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("执行Bl----------------");
    	//将操作数据拷贝到本类中
        this.mOperate = cOperate;
       // mEdorNo = PubFun1.CreateMaxNo("EDORNO", 20);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            return true;
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LAAgentRegularInputBL Submit...");
            if (!tPubSubmit.submitData(data, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentRegularInputBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }
    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
    	try
        {

    	  mMMap.put(this.mLAAgentBSet ,"INSERT") ;
    	  mMMap.put(this.mLATreeBSet,"INSERT") ;
    	  mMMap.put(this.mLATreeSet,"UPDATE") ;
    	  mMMap.put(this.mLAAgentSet,"UPDATE") ;
          data.add(mMMap);
    	  System.out.println("mMMap++++++++++++==============");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentRegularInputBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
    	System.out.println("11dealData--------------------");
    	//laagent 主键是agentcode  latree 主键是 agentcode
    	//备份latree数据
    	LATreeDB tLATreeDB = new LATreeDB();
    	tLATreeDB.setAgentCode(mAgentcode);
    	if (!tLATreeDB.getInfo())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentRegularInputBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询原行政信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	if (false) //这里查询是否有信息的改变
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentRegularInputBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询原行政信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	System.out.println("22dealData--------------------");
    	 LAAgentDB tLAAgentDB = new LAAgentDB();
         tLAAgentDB.setAgentCode(mAgentcode);
         if (!tLAAgentDB.getInfo())
         {
             // @@错误处理
             this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
             CError tError = new CError();
             tError.moduleName = "LAAgentRegularInputBL";
             tError.functionName = "submitDat";
             tError.errorMessage = "查询原代理人信息失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
         tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
         //备份原代理人信息
         Reflections tReflections = new Reflections();
         tReflections.transFields(this.mLAAgentBSchema, tLAAgentDB.getSchema());
         this.mLAAgentBSchema.setEdorNo(tEdorNo);
         this.mLAAgentBSchema.setEdorType("05"); //待确定，05是原始修改备份，这里是转正备份
         this.mLAAgentBSchema.setMakeDate(currentDate);
         this.mLAAgentBSchema.setMakeTime(currentTime);
         this.mLAAgentBSchema.setModifyDate(currentDate);
         this.mLAAgentBSchema.setModifyTime(currentTime);
         this.mLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
         
         System.out.println("33dealData--------------------");
         //修改laagent表
         this.mLAAgentSchema.setSchema(tLAAgentDB.getSchema());
//       置转正日期
         //System.out.println("indueformdate:" + mLAAgentSchema.getInDueFormDate());
         if (indueformdate == null ||indueformdate.equals(""))
         {
//        	 @@错误处理
             this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
             CError tError = new CError();
             tError.moduleName = "LAAgentRegularInputBL";
             tError.functionName = "submitDat";
             tError.errorMessage = "转正时间为空!";
             this.mErrors.addOneError(tError);
             return false;
         }
         
         this.mLAAgentSchema.setInDueFormDate(indueformdate);
         this.mLAAgentSchema.setModifyDate(currentDate);
         this.mLAAgentSchema.setModifyTime(currentTime);
         this.mLAAgentSchema.setOperator(this.mGlobalInput.Operator);
         
         //备份行政信息
         tReflections.transFields(this.mLATreeBSchema, tLATreeDB.getSchema());
         this.mLATreeBSchema.setEdorNO(tEdorNo);
         this.mLATreeBSchema.setRemoveType("05");
         this.mLATreeBSchema.setMakeDate2(this.mLATreeBSchema.getMakeDate());
         this.mLATreeBSchema.setMakeTime2(this.mLATreeBSchema.getMakeTime());
         this.mLATreeBSchema.setModifyDate2(this.mLATreeBSchema.
                                            getModifyDate());
         this.mLATreeBSchema.setModifyTime2(this.mLATreeBSchema.
                                            getModifyTime());
         this.mLATreeBSchema.setOperator2(this.mLATreeBSchema.getOperator());
         this.mLATreeBSchema.setMakeDate(currentDate);
         this.mLATreeBSchema.setMakeTime(currentTime);
         this.mLATreeBSchema.setModifyDate(currentDate);
         this.mLATreeBSchema.setModifyTime(currentTime);
         this.mLATreeBSchema.setOperator(this.mGlobalInput.Operator);
         
//       行政信息
         this.mLATreeSchema.setSchema(tLATreeDB.getSchema());
         this.mLATreeSchema.setAgentGrade(newAgentGrade);//保存转正后的职级
         this.mLATreeSchema.setInDueFormDate(indueformdate);//转正日期
         this.mLATreeSchema.setInDueFormFlag(InDueFormFlag);//转正状态
         this.mLATreeSchema.setModifyDate(currentDate);
         this.mLATreeSchema.setModifyTime(currentTime);
         this.mLATreeSchema.setOperator(this.mGlobalInput.Operator);
         System.out.println("BL:agentgrade:"+this.mLATreeSchema.getAgentGrade());
         
         this.mLAAgentBSet.add(mLAAgentBSchema);
         this.mLATreeBSet.add(mLATreeBSchema);
         this.mLAAgentSet.add(mLAAgentSchema);
         this.mLATreeSet.add(mLATreeSchema);
         System.out.println("set.add=======================");
         return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
    	
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0)); //这个0取得了所有放入GlobalInput中的SCHEMA,SCHEMA的下标从0开始     
        this.mLAAgentSchema = (LAAgentSchema) cInputData.get(1);
        this.mLATreeSchema = (LATreeSchema)cInputData.get(2);
        newAgentGrade=this.mLATreeSchema.getAgentGrade();
        this.indueformdate = mLAAgentSchema.getInDueFormDate(); //转正时间
        this.InDueFormFlag = mLATreeSchema.getInDueFormFlag();
        
        this.mAgentcode = mLAAgentSchema.getAgentCode(); //业务员编码
        this.mgroupAgentcode = mLAAgentSchema.getGroupAgentCode(); //集团统一工号
        System.out.println("业务员编码是-------"+mAgentcode);
        System.out.println("集团统一工号是-------"+mgroupAgentcode);
       // this.mManageCom = (String) cInputData.get(1);
        //this.mBranchType = (String) cInputData.get(3);
        //this.mBranchType2 = (String) cInputData.get(4);

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentRegularInputBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
   

}
