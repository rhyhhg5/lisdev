package com.sinosoft.lis.agentassess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.agentcalculate.AgCalBase;
import com.sinosoft.lis.agentcalculate.CalIndex;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.db.LAAssessHistoryDB;
import com.sinosoft.lis.db.LAAssessIndexDB;
import com.sinosoft.lis.db.LAAssessMainDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAIndexVsAssessDB;
import com.sinosoft.lis.db.LAStatSegmentDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAssessBSchema;
import com.sinosoft.lis.schema.LAAssessHistorySchema;
import com.sinosoft.lis.schema.LAAssessMainSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LAIndexVsAssessSchema;
import com.sinosoft.lis.schema.LAStatSegmentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentGradeSet;
import com.sinosoft.lis.vschema.LAAssessBSet;
import com.sinosoft.lis.vschema.LAAssessHistorySet;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LAIndexVsAssessSet;
import com.sinosoft.lis.vschema.LAStatSegmentSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: 人员考核指标计算流程类
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class AgentCalExamineARisk {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors(); // 错误处理类
	private VData mInputData = new VData();
	private MMap mMap = new MMap();
	/** 数据操作字符串 */
	private String mOperate = ""; // 操作符
	private String mIndexCalNo = ""; // 指标计算编码
	private String mManageCom = ""; // 管理机构
	private String mYear = ""; // 考核年
	private String mMonth = ""; // 考核月
	private String mBranchType = ""; // 展业类型
	private String mBranchType2 = ""; // 渠道
	private String mAgentGrade = ""; // 职级系列
	// 核心计算类
	private CalIndex tCalIndex = new CalIndex(); // 指标计算类
	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput(); // 保存传入参数
	private LAAssessSet mLAAssessSet = new LAAssessSet(); // 代理人考评信息
	private String currentDate = PubFun.getCurrentDate();
	private String currentTime = PubFun.getCurrentTime();
	private LAAssessHistorySchema mLAAssessHistorySchema = new LAAssessHistorySchema();
	private LAAssessMainSchema mLAAssessMainSchema = new LAAssessMainSchema();
	// 维持标准
	private int mKeepPrem = 0;
	// 晋升标准
	private int mUPPrem = 0;

	public static void main(String Args[]) {
		String cOperate = "INSERT||MAIN";
		String tManageCom = "8631";
		String tYear = "2007";
		String tMonth = "05";
		String tBranchType = "2";
		String tBranchType2 = "01";
		String tGradeSeries = "D";

		VData cInputData = new VData();
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.Operator = "mak002";
		tGlobalInput.ManageCom = "8631";
		tGlobalInput.ComCode = "8631";

		cInputData.add(tManageCom);
		cInputData.add(tYear);
		cInputData.add(tMonth);
		cInputData.add(tBranchType);
		cInputData.add(tBranchType2);
		cInputData.add(tGradeSeries);
		cInputData.add(tGlobalInput);

		AgentCalExamineARisk tAgentCalExamine = new AgentCalExamineARisk();
		boolean tReturn = tAgentCalExamine.submitData(cInputData, cOperate);
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行传入数据的合法性验证
		if (!checkState()) {
			return false;
		}

		// 人员考核计算主函数
		if (!Examine()) {
			return false;
		}
		if (!addLAAssessMain(mAgentGrade, this.mLAAssessSet.size())) {
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		System.out.println("Start AgentCalExamineARisk.java Submit...");

		PubSubmit tPubSubmit = new PubSubmit();
		tPubSubmit.submitData(mInputData, "");
		// 如果有需要处理的错误，则返回
		if (tPubSubmit.mErrors.needDealError()) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError.buildErr(this, "数据提交失败!");
			return false;
		}
		// 回滚计算状态
		if (!returnState()) {
			return false;
		}
		System.out.println("End AgentCalExamineARisk.java Submit.");
		return true;
	}

	/**
	 * 进行数据转储处理
	 * 
	 * @return boolean
	 */
	private boolean dealLAAssessB() {
		LAAssessDB tLAAssessDB = new LAAssessDB();
		LAAssessSet tLAAssessSet = new LAAssessSet();
		LAAssessSchema tLAAssessSchema = new LAAssessSchema();
		LAAssessBSet tLAAssessBSet = new LAAssessBSet();
		LAAssessBSchema tLAAssessBSchema = new LAAssessBSchema();
		VData tOutputDate = new VData();
		MMap tMap = new MMap();
		String tNewEdorNo = "";

		tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
		tLAAssessSet = tLAAssessDB.query();
		if (tLAAssessSet.size() > 0) {
			for (int i = 1; i <= tLAAssessSet.size(); i++) {
				Reflections tReflections = new Reflections();
				tLAAssessSchema.setSchema(tLAAssessSet.get(i));

				tLAAssessBSchema = new LAAssessBSchema();
				// 复制备份表中相同的项
				tReflections.transFields(tLAAssessBSchema, tLAAssessSchema);
				// 取得转储号码
				tNewEdorNo = PubFun1.CreateMaxNo("ASSESSEDORNO", 20);

				tLAAssessBSchema.setEdorNo(tNewEdorNo);
				tLAAssessBSchema.setEdorType("01");
				tLAAssessBSchema.setEdorDate(this.currentDate);

				tLAAssessBSet.add(tLAAssessBSchema);
			}

			tMap.put(tLAAssessBSet, "INSERT");
			tOutputDate.add(tMap);

			PubSubmit tPubSubmit = new PubSubmit();
			tPubSubmit.submitData(tOutputDate, "");
			if (tPubSubmit.mErrors.needDealError()) {
				CError tError = new CError();
				tError.moduleName = "AgentCalExamineARisk";
				tError.functionName = "dealLAAssessB";
				tError.errorMessage = "进行考核数据备份操作失败！!";
				this.mErrors.addOneError(tError);

				return false;
			}
		} else {
			System.out.println("没有考核数据生成，不需要做转储处理！");
		}

		return true;
	}

	/**
	 * 人员考核计算主函数
	 * 
	 * @return boolean
	 */
	private boolean Examine() {
		if (!dealAssess()) {
			// 考核出现错误，进行数据恢复
			dealRoolback();
			return false;
		}

		return true;
	}

	/**
	 * 考核出错 进行数据回滚
	 * 
	 * @return boolean
	 */
	private boolean dealRoolback() {
		LAAssessDB tLAAssessDB = new LAAssessDB();
		LAAssessSet tLAAssessSet = new LAAssessSet();
		String tSQL = "";
		tSQL = "select * from laassess where indexcalno='" + this.mIndexCalNo
				+ "' and state in ('0') and managecom like '" + this.mManageCom
				+ "%' and branchtype='" + this.mBranchType
				+ "' and branchtype2='" + this.mBranchType2 + "'"
				+ " and agentgrade ='" + this.mAgentGrade + "'";

		tLAAssessSet = tLAAssessDB.executeQuery(tSQL);

		VData tOutputDate = new VData();
		MMap tMap = new MMap();

		if (tLAAssessSet.size() != 0) {
			tMap.put(tLAAssessSet, "DELETE");
		}

		LAAssessHistoryDB tLAAssessHistoryDB = new LAAssessHistoryDB();
		LAAssessHistorySet tLAAssessHistorySet = new LAAssessHistorySet();
		tLAAssessHistoryDB.setIndexCalNo(mYear + mMonth); // 考核计算年月代码
		tLAAssessHistoryDB.setManageCom(this.mManageCom); // 管理机构
		tLAAssessHistoryDB.setBranchType(this.mBranchType); // 展业类型
		tLAAssessHistoryDB.setBranchType2(this.mBranchType2); // 渠道
		tLAAssessHistoryDB.setAgentGrade(this.mAgentGrade); // 职级

		tLAAssessHistorySet = tLAAssessHistoryDB.query();

		tMap.put(tLAAssessHistorySet, "DELETE");
		tOutputDate.add(tMap);

		PubSubmit tPubSubmit = new PubSubmit();
		tPubSubmit.submitData(tOutputDate, "");

		return true;
	}

	/**
	 * 根据各个机构和职级进行人员考核
	 * 
	 * @return boolean
	 */
	private boolean dealAssess() {
		String tSQL = ""; // 查询用SQL文
		ExeSQL tExeSQL = new ExeSQL();
		String indexCalDate = mIndexCalNo.substring(0, 4) + "-"
				+ mIndexCalNo.substring(4) + "-" + AgentPubFun.getAssessDay();

		LATreeDB tLATreeDB = new LATreeDB();

		tSQL = "select * from LATree a where a.ManageCom = '" + mManageCom
				+ "' " + "and a.AgentGrade = '" + mAgentGrade
				+ "' and (speciflag<>'01' or speciflag is null ) ";
		tSQL += "and exists (select 'X' from laagent where agentcode = a.AgentCode "
				+ " and employdate <= '"
				+ indexCalDate
				+ "' "
				// 筹备人员不参与考核
				+ " and (noworkflag='N' or noworkflag is null) "
				+ " and agentstate in ('01','02')) ";
		tSQL += " and not exists (select 'W' from laagent where agentcode =a.agentcode and branchcode in "
				+ " (select agentgroup from labranchgroup where state ='1')) ";
		System.out.println(tSQL);
		LATreeSet tLATreeSet = new LATreeSet(); // 人员行政信息
		// 查询指定机构，指定职级的所有人员
		tLATreeSet = tLATreeDB.executeQuery(tSQL);
		// 3、根据 机构+职级 中每个个人进行循环操作
		int contAgent = tLATreeSet.size();
		// 如果机构中不存在该职级的人员，跳过处理下一条记录
		if (contAgent == 0) {
			 CError.buildErr(this,"不存在符合考核条件的业务员！");
			return false;
		}

		// 3、循环人员进行处理
		for (int n = 1; n <= contAgent; n++) {
			// 取出一个人员的行政信息，进行考核
			LATreeSchema tLATreeSchema = new LATreeSchema();
			tLATreeSchema = tLATreeSet.get(n);
			// 入职时间
//			String tEmployDateSql = "select employdate from laagent where agentcode ='"
//					+ tLATreeSchema.getAgentCode() + "'";
//			String tEmployDate = tExeSQL.getOneValue(tEmployDateSql);
			//修改为职级开始日期，不再使用入职日期 2018-07 cbs00110763
			String tEmployDateSql = "select astartdate from latree where agentcode ='"
			+ tLATreeSchema.getAgentCode() + "'";
	        String tEmployDate = tExeSQL.getOneValue(tEmployDateSql);
			// 考核月份最后 一天
			LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
			tLAStatSegmentDB.setYearMonth(Integer.parseInt(this.mIndexCalNo));
			// 按月进行考核
			tLAStatSegmentDB.setStatType("1");
			String tEndDate = tLAStatSegmentDB.query().get(1).getEndDate();

			// 考核月数
			int assessInv = calMonths(tEmployDate, tEndDate);
			// 见习职级 滚动考核 最近三个月
			if (assessInv > 2) {
				assessInv = 2;
			}
			// 考核起始薪资月月
			String tStartWageNo = AgentPubFun.ConverttoYM(PubFun.calDate(
					tEndDate, -1 * assessInv, "M", ""));

			// 累计首年度直接佣金
			String tFycSql = "select sum(Fyc) from lacommision where ((payyear =0 and renewcount =0) or renewcount =1) and agentcode ='"
					+ tLATreeSchema.getAgentCode()
					+ "'"
					+ " and wageno between '"
					+ tStartWageNo
					+ "' and '"
					+ this.mIndexCalNo + "'";
			String tSumprem =tExeSQL.getOneValue(tFycSql);
			if("".equals(tSumprem)||null==tSumprem)
			{
				tSumprem = "0";
			}
			double tSumFyc = Double.parseDouble(tSumprem);
			// 晋升或维持职级
			String tDAgentGrade = tLATreeSchema.getAgentGrade();
			String tUpFlag = "02";
			if (this.mUPPrem <= tSumFyc) {
				tDAgentGrade = "A02";
				tUpFlag = "03";
			}
			String tIntSQL = "select count(*) from laassess where agentcode='"
					+ tLATreeSchema.getAgentCode()
					+ "' AND STATE IN ('0','1','2')";
			int tIntRt = execQuery(tIntSQL);
			LAAssessSchema tLAAssessSchema = new LAAssessSchema();
			tLAAssessSchema.setAgentSeries("0");
			tLAAssessSchema.setAgentSeries1("0");
			tLAAssessSchema.setCalAgentSeries("0");
			tLAAssessSchema.setIndexCalNo(mYear + mMonth); // 指标计算编码
			tLAAssessSchema.setAgentCode(tLATreeSchema.getAgentCode()); // 代理人编码
			tLAAssessSchema.setAgentGroup(tLATreeSchema.getAgentGroup()); // 展业机构
			tLAAssessSchema.setManageCom(tLATreeSchema.getManageCom()); // 管理机构
			tLAAssessSchema.setAgentGrade(tLATreeSchema.getAgentGrade()); // 代理人职级
			tLAAssessSchema.setAgentGrade1(tDAgentGrade);
			tLAAssessSchema.setCalAgentGrade(tDAgentGrade);
			tLAAssessSchema.setModifyFlag(tUpFlag);
			// tLAAssessSchema.setAgentSeries(); //代理人系列
			// tLAAssessSchema.setAgentSeries1(); //代理人新系列
			tLAAssessSchema.setState("0"); // 考核状态
			tLAAssessSchema.setOperator(mGlobalInput.Operator); // 操作员
			tLAAssessSchema.setMakeDate(this.currentDate); // 入机日期
			tLAAssessSchema.setMakeTime(this.currentTime); // 入机时间
			tLAAssessSchema.setModifyDate(this.currentDate); // 修改日期
			tLAAssessSchema.setModifyTime(this.currentTime); // 修改时间
			tLAAssessSchema.setBranchType(this.mBranchType); // 展业类型
			tLAAssessSchema.setBranchType2(this.mBranchType2); // 渠道
			tLAAssessSchema.setStandAssessFlag("1"); // 是基本法考核

			if (tIntRt == 0) {
				tLAAssessSchema.setFirstAssessFlag("1"); // 是第一次参加考核
			}
			else
			{
				tLAAssessSchema.setFirstAssessFlag("2");
			}
			tLAAssessSchema.setBranchAttr(AgentPubFun
					.getAgentBranchAttr(tLATreeSchema.getAgentCode())); // 机构外部编码
			mLAAssessSet.add(tLAAssessSchema);
		}
		return true;
	}

	/**
	 * 计算并获得考核起期止期
	 * 
	 * @param pmAgentCode
	 *            String
	 * @return boolean
	 */

	/**
	 * 查询上次考核日期
	 * 
	 * @param pmAgentCode
	 *            String
	 * @param pmAgentGrade
	 *            String
	 * @return String
	 */
	private String getAssessBegin(String pmAgentCode, String pmAgentGrade) {
		String sAssessBegin = "";
		PreparedStatement ps = null;
		ResultSet rs = null;

		String tSql = "select max(indexcalno) from laindexinfo where (indextype='02' or indextype='03')"
				+ " and agentcode='"
				+ pmAgentCode
				+ "' and agentgrade ='"
				+ pmAgentGrade + "'";
		System.out.println("查找最近考核日期SQL：" + tSql);

		try {
			Connection conn = DBConnPool.getConnection();
			ps = conn.prepareStatement(tSql);
			rs = ps.executeQuery();
			if (rs.next()) {
				sAssessBegin = rs.getString(1);
				rs.close();
				ps.close();
			}
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				ps.close();
				rs.close();
			} catch (Exception e) {
			}
		}

		return sAssessBegin;
	}

	/**
	 * 进行数据验证
	 * 
	 * @return boolean
	 */
	// 检查是否正在计算，不能同时多用户计算
	private boolean checkState() {
		String currentDate = PubFun.getCurrentDate();
		String currentTime = PubFun.getCurrentTime();
		String tState = "";
		LAAssessHistorySet tLAAssessHistorySet = new LAAssessHistorySet();
		LAAssessHistoryDB tLAAssessHistoryDB = new LAAssessHistoryDB();
		tLAAssessHistoryDB.setIndexCalNo(mIndexCalNo);
		tLAAssessHistoryDB.setManageCom(this.mManageCom);
		tLAAssessHistoryDB.setBranchType(mBranchType);
		tLAAssessHistoryDB.setBranchType2(mBranchType2);
		tLAAssessHistoryDB.setAgentGrade("A01");
		tLAAssessHistorySet = tLAAssessHistoryDB.query();

		if (tLAAssessHistorySet.size() > 0) {
			tState = tLAAssessHistorySet.get(1).getState();
			if (tState.equals("01")) {
				CError.buildErr(this, "本月考核正在进行计算，不能并发计算！");
				return false;
			} else if (tState.equals("00")) {
				tLAAssessHistoryDB = tLAAssessHistorySet.get(1).getDB();
				tLAAssessHistoryDB.setModifyDate(currentDate);
				tLAAssessHistoryDB.setModifyTime(currentTime);
				tLAAssessHistoryDB.setState("01"); // 设置为不可并发计算
				if (!tLAAssessHistoryDB.update()) {
					CError.buildErr(this, "增加机构" + mManageCom + "考核日志失败!");
					return false;
				}
			}
		} else {
			tLAAssessHistoryDB.setState("01");

			tLAAssessHistoryDB.setMakeDate(currentDate);
			tLAAssessHistoryDB.setMakeTime(currentTime);
			tLAAssessHistoryDB.setModifyDate(currentDate);
			tLAAssessHistoryDB.setModifyTime(currentTime);
			tLAAssessHistoryDB.setOperator(this.mGlobalInput.Operator);
			if (!tLAAssessHistoryDB.insert()) {
				CError.buildErr(this, "增加机构" + mManageCom + "考核日志失败!");
				return false;
			}
		}
		return true;
	}

	/**
	 * 执行SQL文查询结果
	 * 
	 * @param sql
	 *            String
	 * @return int
	 */
	private int execQuery(String sql) {
		System.out.println(sql);
		Connection conn;
		conn = null;
		conn = DBConnPool.getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if (conn == null) {
				return 0;
			}
			st = conn.prepareStatement(sql);
			if (st == null) {
				return 0;
			}
			rs = st.executeQuery();
			if (rs.next()) {
				return rs.getInt(1);
			}
			return 0;
		} catch (Exception ex) {
			ex.printStackTrace();
			return -1;
		} finally {
			try {
				if (!conn.isClosed()) {
					conn.close();
				}
				try {
					st.close();
					rs.close();
				} catch (Exception ex2) {
					ex2.printStackTrace();
				}
				st = null;
				rs = null;
				conn = null;
			} catch (Exception e) {
			}
		}
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		this.mManageCom = (String) cInputData.getObject(0); // 管理机构
		this.mYear = (String) cInputData.getObject(1); // 考核年
		this.mMonth = (String) cInputData.getObject(2); // 考核月
		this.mBranchType = (String) cInputData.getObject(3); // 展业类型
		this.mBranchType2 = (String) cInputData.getObject(4); // 展业类型
		this.mAgentGrade = (String) cInputData.getObject(5); // 职级系列
		this.mIndexCalNo = mYear + mMonth; // 指标计算编码
		// 地区类型
		String tAreaType = AgentPubFun.getAreaType(this.mManageCom.substring(0, 4), "01");

		if ("A".equals(tAreaType)) {
			this.mKeepPrem = 720;
			this.mUPPrem = 1440;
		} else if ("B".equals(tAreaType)) {
			this.mKeepPrem = 600;
			this.mUPPrem = 1200;
		} else if ("C".equals(tAreaType)) {
			this.mKeepPrem = 480;
			this.mUPPrem = 960;
		}
		System.out.println(mManageCom + " / " + mIndexCalNo + " / "
				+ mBranchType + " / " + mBranchType2 + " / " + mAgentGrade);

		if (mGlobalInput == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "AgentCalExamineARisk";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的信息！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 计算两个日期之间的月数，16日之后的不算作一个月
	 */
	public int calMonths(String cBeginDate, String cEndDate) {
		FDate fDate = new FDate();
		int monthIntv = 0;
		int dayInv = 0;
		String tSql = "";
		try {
			ExeSQL tExeSQL = new ExeSQL();
			// 考核月计算时的临界日期
			tSql = "select trim(code2) from ldcoderela where relatype = 'assessday'";
			String tValue = tExeSQL.getOneValue(tSql);
			monthIntv = PubFun.calInterval(cBeginDate, fDate.getString(PubFun
					.calDate(fDate.getDate(cEndDate), 1, "D", null)), "M");
			dayInv = PubFun.calInterval(cBeginDate, cBeginDate.substring(0,
					cBeginDate.lastIndexOf("-"))
					+ "-" + tValue, "D");
			if (dayInv >= 0
					&& !cBeginDate.substring(cBeginDate.length() - 2).equals(
							"01")) // 若为15日以前入司，则到考核期，入司该月也算作一个月
			{
				monthIntv = monthIntv + 1;
			}
		} catch (Exception ex) {
			CError.buildErr(this, "计算起期" + cBeginDate + "与止期" + cEndDate
					+ "的月数出错!");
			return -100;
		}
		System.out.println("monthIntv:" + monthIntv);
		return monthIntv;
	}

	// 个险考核计算结束，返回到可计算状态，更新状态为'00'，表示可以计算
	private boolean returnState() {
		String currentDate = PubFun.getCurrentDate();
		String currentTime = PubFun.getCurrentTime();
		String tSQL1 = "update LAAssessHistory set state = '00' ,modifydate='"
				+ currentDate + "',modifytime='" + currentTime + "'  where "
				+ " IndexCalNo = '" + mIndexCalNo + "' and ManageCom like '"
				+ this.mManageCom + "%' and BranchType = '" + mBranchType
				+ "' and BranchType2 ='" + mBranchType2
				+ "' and AgentGrade='A01'";
		System.out.println("最后更新LAAssessHistory表中state的SQL: " + tSQL1);
		ExeSQL tExeSQL = new ExeSQL();
		tExeSQL.execUpdateSQL(tSQL1);
		if (tExeSQL.mErrors.needDealError()) {
			// @@错误处理
			this.mErrors.copyAllErrors(tExeSQL.mErrors);
			CError.buildErr(this, "回滚更新LAssessHistory表记录时失败!");
			return false;
		}
		return true;
	}

	/**
	 * 将本次考核的职级人数存于LAAssessMain表中
	 * 
	 * @param cAgentGrade
	 *            String
	 * @param cMaxCount
	 *            int
	 * @return boolean
	 */
	private boolean addLAAssessMain(String cAgentGrade, int cFinalCount) {
		// 准备数据
		String currentDate = PubFun.getCurrentDate();
		String currentTime = PubFun.getCurrentTime();
		LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
		this.mLAAssessMainSchema = new LAAssessMainSchema();
		tLAAssessMainDB.setIndexCalNo(mIndexCalNo);
		tLAAssessMainDB.setManageCom(cAgentGrade);
		tLAAssessMainDB.setAgentGrade(mManageCom);
		if (tLAAssessMainDB.getInfo()) {
			this.mLAAssessMainSchema = tLAAssessMainDB.getSchema();
		} else {
			this.mLAAssessMainSchema.setIndexCalNo(mIndexCalNo);
			this.mLAAssessMainSchema.setAgentGrade(cAgentGrade);
			this.mLAAssessMainSchema.setManageCom(mManageCom);
			this.mLAAssessMainSchema.setBranchType(mBranchType);
			this.mLAAssessMainSchema.setBranchType2(mBranchType2);
			this.mLAAssessMainSchema.setState("0");
			this.mLAAssessMainSchema.setAssessType("00");
			this.mLAAssessMainSchema.setAssessCount(cFinalCount);
			this.mLAAssessMainSchema.setMakeDate(currentDate);
			this.mLAAssessMainSchema.setMakeTime(currentTime);
		}
		this.mLAAssessMainSchema.setModifyDate(currentDate);
		this.mLAAssessMainSchema.setModifyTime(currentTime);
		this.mLAAssessMainSchema.setOperator(this.mGlobalInput.Operator);
		return true;
	}

	/**
	 *准备传入后台数据
	 */
	private boolean prepareOutputData() {
		try {
			this.mInputData = new VData();
			if (mOperate.equals("INSERT||MAIN")) {
				this.mMap.put(this.mLAAssessSet, "DELETE&INSERT");
				this.mMap.put(this.mLAAssessMainSchema, "DELETE&INSERT");
				this.mInputData.add(mMap);
			} else if (mOperate.equals("DELETE||MAIN")) {
				this.mInputData.add(mIndexCalNo);
				this.mInputData.add(mManageCom);
				this.mInputData.add(mAgentGrade);
				this.mInputData.add(mBranchType);
				this.mInputData.add(this.mGlobalInput);
				// todo:删除考核记录功能
			}
		} catch (Exception ex) {
			// @@错误处理
			CError.buildErr(this, "在准备往后层处理所需要的数据时出错！");
			return false;
		}
		return true;
	}
}
