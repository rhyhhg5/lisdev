/*
 * <p>ClassName: ALAAssessBLS </p>
 * <p>Description: LAAssessBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-06-21
 */
package com.sinosoft.lis.agentassess;


import java.sql.Connection;

import com.sinosoft.lis.db.LAAssessMainDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAssessMainSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.vdb.LAAssessDBSet;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class CalAssessBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String mIndexCalNo; //佣金计算编码
    private String mAgentGrade; //考核职级
    private String mManageCom; //管理机构
    private String mBranchType;
    public CalAssessBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        mOperate = cOperate;
        System.out.println("Start CalAssessBLS Submit...");
        if (cOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLAAssess(cInputData);
        }
        else if (cOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLAAssess(cInputData);
        }

        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End CalAssessBLS Submit...");
        return tReturn;
    }

    private boolean getInputData(VData cInputData)
    {
        if (mOperate.equals("DELETE||MAIN"))
        {
            mIndexCalNo = (String) cInputData.get(1);
            mManageCom = (String) cInputData.get(2);
            mAgentGrade = (String) cInputData.get(3);
            this.mBranchType = (String) cInputData.get(4);
            mGlobalInput = (GlobalInput) cInputData.get(5);
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAssess(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAssessBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAssessSet tLAAssessSet = new LAAssessSet();
            tLAAssessSet.set((LAAssessSet) mInputData.getObjectByObjectName(
                    "LAAssessSet", 0));
            if (tLAAssessSet.size() > 0)
            {
                System.out.println("Size Of LAAssess :" + tLAAssessSet.size());
                LAAssessSchema tLAAssessSchema = tLAAssessSet.get(1);
                LAAssessDBSet tLAAssessDBSet = new LAAssessDBSet(conn);
                tLAAssessDBSet.set(tLAAssessSet);
                System.out.println("无数据！--插入");
                if (!tLAAssessDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAssessDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CalAssessBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("Start 插入LAAssessMain表...");
            LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB(conn);
            tLAAssessMainDB.setSchema((LAAssessMainSchema) mInputData.
                                      getObjectByObjectName(
                                              "LAAssessMainSchema", 0));
            if (!tLAAssessMainDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAssessMainDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalAssessBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalAssessBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 删除历史数据函数
     */
    private boolean deleteLAAssess(VData mInputData)
    {
        if (!getInputData(mInputData))
        {
            return false;
        }
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalAssessBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            ExeSQL tExeSQL = new ExeSQL(conn);
//            LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB(conn);
//            tLAAssessMainDB.setSchema((LAAssessMainSchema)mInputData.getObjectByObjectName("LAAssessMainSchema",0));
//
//            System.out.println("Start 删除LAIndexInfo表...");
//            ExeSQL tExeSQL = new ExeSQL(conn);
//            String deleteSQL = "Delete From LAIndexInfo Where IndexType in ('02','03') "
//                              +"And IndexCalNo = '"+tLAAssessMainDB.getIndexCalNo()+"' "
//                              +"And ManageCom = '"+tLAAssessMainDB.getManageCom()+"' "
//                              +"And exists  (Select 'X' From LAAssess "
//                              +"Where IndexCalNo = '"+tLAAssessMainDB.getIndexCalNo()+"' "
//                              +"And AgentGrade = '"+tLAAssessMainDB.getAgentGrade()+"' "
//                              +"And ManageCom = '"+tLAAssessMainDB.getManageCom()+"' "
//                              +"And BranchType = '"+tLAAssessMainDB.getBranchType()+"'"
//                              +"And agentcode=laindexinfo.agentcode)";
//            System.out.println("deleteSQL:"+deleteSQL);
//            if (!tExeSQL.execUpdateSQL(deleteSQL))
//            {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tExeSQL.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "CalAssessBLS";
//                tError.functionName = "saveData";
//                tError.errorMessage = "数据保存失败!";
//                this.mErrors.addOneError(tError) ;
//                conn.rollback();
//                conn.close();
//                return false;
//            }
            System.out.println("Start 删除LAAssess...");
//            LAAssessDB tLAAssessDB = new LAAssessDB(conn);
//            tLAAssessDB.setAgentGrade(tLAAssessMainDB.getAgentGrade());
//            tLAAssessDB.setIndexCalNo(tLAAssessMainDB.getIndexCalNo());
//            tLAAssessDB.setManageCom(tLAAssessMainDB.getManageCom());
//            tLAAssessDB.setBranchType(tLAAssessMainDB.getBranchType());
//            if (!tLAAssessDB.deleteSQL())
//            {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tLAAssessDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "CalAssessBLS";
//                tError.functionName = "saveData";
//                tError.errorMessage = "数据删除失败!";
//                this.mErrors .addOneError(tError) ;
//                conn.rollback();
//                conn.close();
//                return false;
//            }
            System.out.println("Start 插入LAAssessMain表...");
//            if (!tLAAssessMainDB.deleteSQL())
//            {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tLAAssessMainDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "CalAssessBLS";
//                tError.functionName = "saveData";
//                tError.errorMessage = "数据保存失败!";
//                this.mErrors.addOneError(tError) ;
//                conn.rollback();
//                conn.close();
//                return false;
//            }
            String asql = " delete laassessmain where indexcalno = '" +
                          mIndexCalNo + "' and agentgrade = '"
                          + mAgentGrade + "'and managecom like '" + mManageCom +
                          "%' and managecom like '" + mGlobalInput.ManageCom +
                          "%'"
                          + " and branchtype = '" + this.mBranchType + "'";
            if (!tExeSQL.execUpdateSQL(asql))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalAssessBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            String bsql = "delete laassess where indexcalno = '" + mIndexCalNo +
                          "' and agentgrade = '"
                          + mAgentGrade + "' and managecom like '" + mManageCom +
                          "%' and managecom like '" + mGlobalInput.ManageCom +
                          "%'"
                          + " and branchtype = '" + this.mBranchType + "'";
            if (!tExeSQL.execUpdateSQL(bsql))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalAssessBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            String csql = "delete laindexinfo a where a.indexcalno = '"
                          + mIndexCalNo +
                          "' and a.indextype in('02','03') and a.managecom like '" +
                          mManageCom + "%' and managecom like '" +
                          mGlobalInput.ManageCom + "%'"
                          + " and a.agentgrade = '" + this.mAgentGrade + "' ";
            if (!tExeSQL.execUpdateSQL(csql))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalAssessBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalAssessBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
