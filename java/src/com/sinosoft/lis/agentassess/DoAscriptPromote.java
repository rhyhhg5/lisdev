package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 代理人晋升组织归属</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author zhangsj
 * @version 1.0
 */

public class DoAscriptPromote
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public static CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局变量 */
    public GlobalInput mGlobalInput = new GlobalInput();
    /** 晋升代理人行政信息 */
    private LATreeSchema mLATreeSchema;
    private static String mIndexCalNo = "";
    private VData mOutputData = new VData();
//晋升代理人基本信息
    private LAAssessSchema mLAAssessSchema = new LAAssessSchema();
    private String m_AgentCode = "";
    private String m_AgentGroup = "";
//连接
    private Connection conn = null;

    public DoAscriptPromote()
    {
    }

    public static void main(String[] args)
    {
        //DoAscriptPromote doAscriptPromote1 = new DoAscriptPromote();
        String ss = "StartsWithMe";
        System.out.println(ss.startsWith("Starts"));

    }

    /**
     * 根据考核结果开始晋升一个代理人
     * @param cInputData
     * @return
     */
    public boolean promote(VData cInputData)
    {
        this.mInputData = cInputData;

        //得到传入的变量值
        if (!getInputData())
        {
            return false;
        }

        this.m_AgentCode = this.mLAAssessSchema.getAgentCode();

        //取晋升代理人行政信息表信息,准备全局变量
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(this.m_AgentCode);
        if (!tLATreeDB.getInfo())
        {
            dealError("promote", "取代理人行政信息失败！");
            return false;
        }
        this.mLATreeSchema = tLATreeDB.getSchema();
        this.m_AgentGroup = this.mLAAssessSchema.getAgentGroup();

        //判断属于哪一职级晋升
        String tSeries1 = this.mLAAssessSchema.getAgentSeries1();
        String tAgentGrade1 = this.mLAAssessSchema.getAgentGrade1();

        //建立一个连接
        this.conn = DBConnPool.getConnection();
        CreateBranchAttr tCreateNo = CreateBranchAttr.getInstance();
        if (conn == null)
        {
            dealError("Promote", "数据库连接失败！");
            return false;
        }

        try
        {
            conn.setAutoCommit(false);

            if (tSeries1.equals("B"))
            {
                if (!BGradePromoteNew(conn))
                {
                    conn.rollback();
                    System.out.println("---conn.rollback----");
                    conn.close();
                    tCreateNo.release();
                    return false;
                }
            }

            if (tSeries1.equals("C"))
            {
                if (!CGradePromoteNew(conn))
                {
                    conn.rollback();
                    System.out.println("---conn.rollback----");
                    conn.close();
                    tCreateNo.release();
                    return false;
                }
            }

            if (tAgentGrade1.trim().equalsIgnoreCase("A08"))
            {
                if (!DGradePromoteNew(conn))
                {
                    conn.rollback();
                    System.out.println("---conn.rollback----");
                    conn.close();
                    tCreateNo.release();
                    return false;
                }
            }

            if (tAgentGrade1.trim().equalsIgnoreCase("A09"))
            {
                if (!TopGradePromoteNew(conn))
                {
                    conn.rollback();
                    System.out.println("---conn.rollback----");
                    conn.close();
                    tCreateNo.release();
                    return false;
                }
            }

            System.out.println("---now commit---");
            conn.commit();
            conn.close();
            tCreateNo.release();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("promote", ex.toString());

            try
            {
                conn.rollback();
                System.out.println("-----in catch----conn.rollback----");
                conn.close();
                tCreateNo.release();
            }
            catch (Exception et)
            {
                et.printStackTrace();
                dealError("Promote", et.toString());
                System.out.println("----conn.rollback and close failed!----");
                return false;
            }
            return false;
        }
        return true;
    }

    private boolean BGradePromote(Connection conn)
    {
        String sql_where = "";
        String ModTypeFlag = ""; // ModTypeFlag : "O" -- 普通代理人;
        //               "S" -- 被晋升的代理人
        String InsFlag = "Y"; // InsFlag :     "Y" -- 仅插入 LABranchGroup;
        //               "N" -- 插入 LABranchGroupB 并刷新 LABranchGroup
        AscriptDealPersonData tAscriptDealPersonData = new
                AscriptDealPersonData();
        PubFun1 tPF1 = new PubFun1();

        //生成新组号
        String tNewAgentGroup = tPF1.CreateMaxNo("AGENTGROUP", 12);
        String tUpBranchAttr = getUpBranchAttr();
        if (tUpBranchAttr == null || tUpBranchAttr.equals(""))
        {
            dealError("BGradePromote", "生成新组号时取上级组号失败！");
            return false;
        }

        CreateBranchAttr tCreateAttr = CreateBranchAttr.getInstance();
        if (!tCreateAttr.init(tUpBranchAttr, "Y", 18))
        {
            dealError("BGradePromote", "生成新组号失败！");
            return false;
        }
        String tNewBranchAttr = tCreateAttr.getTeamNo();
        System.out.println("-----genTeamNo : " + tNewBranchAttr);

        String tAgentCode = "";
        String tAgentGroup = "";

        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeSet tLATreeSet = new LATreeSet();

        //取sql语句  //取组建新的组时所有的成员，这些成员是该代理人所增人员并且和该代理人
        //在相同的组下的人员。
        sql_where =
                "select a.* from latree a,laagent b where a.introagency = '"
                + this.m_AgentCode.trim() + "' and a.agentgroup = '"
                + this.m_AgentGroup.trim() + "' and a.agentcode = b.agentcode "
                + " and b.agentstate <'03'";

        System.out.println("---sql---" + sql_where);

        //取直接推荐人
        tLATreeSet = tLATreeDB.executeQuery(sql_where);
        int tCount = tLATreeSet.size();

        if (tCount > 0)
        {
            for (int i = 1; i <= tCount; i++)
            {
                tLATreeSchema = tLATreeSet.get(i);
                LATreeSet tQryResult = new LATreeSet();

                //取间接推荐人
                tQryResult = RecursiveQry(1, tLATreeSchema, tQryResult);

                //归属间接推荐人
                int tCount1 = tQryResult.size();
                LATreeSchema tLATreeSchema1 = new LATreeSchema();
                for (int j = 1; j <= tCount1; j++)
                {
                    tLATreeSchema1.setSchema(tQryResult.get(j));

                    /*准备向后传送的数据*/
                    if (!prepareOutputData(tLATreeSchema1, tNewAgentGroup,
                                           tNewBranchAttr,
                                           "O", InsFlag))
                    {
                        return false;
                    }
                    if (!tAscriptDealPersonData.dealPersonData(mOutputData,
                            conn))
                    {
                        this.mErrors.copyAllErrors(tAscriptDealPersonData.
                                mErrors);
                        return false;
                    }
                }

                //归属直接推荐人
                if (!prepareOutputData(tLATreeSchema, tNewAgentGroup,
                                       tNewBranchAttr,
                                       "O",
                                       InsFlag))
                {
                    return false;
                }
                if (!tAscriptDealPersonData.dealPersonData(mOutputData, conn))
                {
                    this.mErrors.copyAllErrors(tAscriptDealPersonData.mErrors);
                    return false;
                }
            }
        }

        //归属晋升代理人
        if (!prepareOutputData(mLATreeSchema, tNewAgentGroup, tNewBranchAttr,
                               "S",
                               InsFlag))
        {
            return false;
        }
        if (!tAscriptDealPersonData.dealPersonData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptDealPersonData.mErrors);
            return false;
        }

        return true;
    }

    private boolean BGradePromoteNew(Connection conn)
    {
        String sql_where = "";
        String ModTypeFlag = ""; // ModTypeFlag : "O" -- 普通代理人;
        //               "S" -- 被晋升的代理人
        String InsFlag = "Y"; // InsFlag :     "Y" -- 仅插入 LABranchGroup;
        //               "N" -- 插入 LABranchGroupB 并刷新 LABranchGroup
        AscriptDealPersonData tAscriptDealPersonData = new
                AscriptDealPersonData();
        PubFun1 tPF1 = new PubFun1();

        //生成新组号
        String tNewAgentGroup = tPF1.CreateMaxNo("AGENTGROUP", 12);
        String tUpBranchAttr = getUpBranchAttr();
        if (tUpBranchAttr == null || tUpBranchAttr.equals(""))
        {
            dealError("BGradePromote", "生成新组号时取上级组号失败！");
            return false;
        }

        CreateBranchAttr tCreateAttr = CreateBranchAttr.getInstance();
        if (!tCreateAttr.init(tUpBranchAttr, "Y", 18))
        {
            dealError("BGradePromote", "生成新组号失败！");
            return false;
        }
        String tNewBranchAttr = tCreateAttr.getTeamNo();
        System.out.println("-----genTeamNo : " + tNewBranchAttr);

        String tAgentCode = "";
        String tAgentGroup = "";

        LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeSet tLATreeSet = new LATreeSet();
        tLATreeSet.add(this.mLATreeSchema);
        LATreeSet tNewLATreeSet = new LATreeSet();
        tNewLATreeSet = this.RecursiveQryNew(1, tLATreeSet);

        System.out.println("before check:" + tNewLATreeSet.size());
        tLATreeSet = this.checkRecursiveQry(tNewLATreeSet, "A", "");
        System.out.println("after check:" + tLATreeSet.size());

        for (int i = 1; i <= tLATreeSet.size(); i++)
        {
            tLATreeSchema = tLATreeSet.get(i);
            if (!prepareOutputData(tLATreeSchema, tNewAgentGroup,
                                   tNewBranchAttr,
                                   "O", InsFlag))
            {
                return false;
            }
            if (!tAscriptDealPersonData.dealPersonData(mOutputData, conn))
            {
                this.mErrors.copyAllErrors(tAscriptDealPersonData.mErrors);
                return false;
            }
        }
        //归属晋升代理人
        if (!prepareOutputData(mLATreeSchema, tNewAgentGroup, tNewBranchAttr,
                               "S",
                               InsFlag))
        {
            return false;
        }
        if (!tAscriptDealPersonData.dealPersonData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptDealPersonData.mErrors);
            return false;
        }

        return true;
    }

    /**
     * caigang add in 2004-06-01
     * check edu or introagency
     * 判断下列条件：
     * 1：离职
     * 2：育成链断链
     * 3：机构在育成机构要在同一个范围内
     * @param aLATreeSet
     * @param series
     * @return
     */
    private LATreeSet checkRecursiveQry(LATreeSet aLATreeSet, String series,
                                        String area)
    {
        String agentCode = "";
        LATreeSet newSet = new LATreeSet();
//    for (int i=1;i<=aLATreeSet.size() ;i++)
//    {
//      System.out.println(aLATreeSet.get(i).getAgentCode()  ) ;
//    }
        for (int i = 1; i <= aLATreeSet.size(); i++)
        {
            agentCode = aLATreeSet.get(i).getAgentCode();
            if (agentCode.equals(this.mLATreeSchema.getAgentCode()))
            {
                continue;
            }
            System.out.println("---checking:" + agentCode);
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(agentCode);
            if (!tLAAgentDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                dealError("checkRecursiveQry",
                          "LAAgent表中未查找到" + agentCode + "的信息!");
                return null;
            }
            if (tLAAgentDB.getAgentState().compareTo("03") >= 0)
            {
                System.out.println("---checking:" + agentCode +
                                   " was fail for agentstate is " +
                                   tLAAgentDB.getAgentState());
                continue;
            }
            LATreeAccessoryDB tLATreeAccessoryDB = new LATreeAccessoryDB();
            String AgentGrade = "";
            String branchLevel = "";
            if (series.compareTo("A") > 0)
            {
                if (series.equals("B"))
                {
                    AgentGrade = " and (agentgrade='A04' or agentgrade='A05' )";
                    branchLevel = "01";
                }
                else
                if (series.equals("C"))
                {
                    AgentGrade = " and (agentgrade='A06' or agentgrade='A07' )";
                    branchLevel = "02";
                }
                else
                {
                    AgentGrade = " and agentgrade='A08' ";
                    branchLevel = "03";
                }
                String SQL = "select * from latreeaccessory where agentcode='" +
                             agentCode + "'" + AgentGrade;
                LATreeAccessorySet tLATreeAccessorySet = new LATreeAccessorySet();
                System.out.println(SQL);
                tLATreeAccessorySet = tLATreeAccessoryDB.executeQuery(SQL);
                LATreeAccessorySchema tLATreeAccessorySchema = new
                        LATreeAccessorySchema();
                tLATreeAccessorySchema = tLATreeAccessorySet.get(1);
                if (tLATreeAccessorySchema.getRearFlag() != null &&
                    tLATreeAccessorySchema.getRearFlag().equals("1"))
                {
                    System.out.println("---checking:" + agentCode +
                                       " was fail for reason LATreeAccessory.RearFlag ");
                    continue;
                }
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                SQL = "select * from labranchgroup where branchlevel='" +
                      branchLevel + "' and branchManager='" + agentCode +
                      "' and (endflag is null or endflag <>'Y') and branchAttr like '" +
                      area + "%'";
                LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
                System.out.println(SQL);
                tLABranchGroupSet = tLABranchGroupDB.executeQuery(SQL);
                if (tLABranchGroupSet.size() == 0)
                {
                    System.out.println("---checking:" + agentCode +
                                       " was fail for reason area LABranchGroup.BranchAttr ");
                    continue;
                }
            }
            else
            {
                if (!tLAAgentDB.getAgentGroup().equals(this.m_AgentGroup))
                {
                    System.out.println("---checking:" + agentCode +
                                       " was fail for reason agentgroup! ");
                    continue;
                }
            }
            System.out.println("---checking:" + agentCode + " is success! ");
            newSet.add(aLATreeSet.get(i));

        }
        for (int i = 1; i <= newSet.size(); i++)
        {
            System.out.println(newSet.get(i).getAgentCode());
        }

        return newSet;
    }

    /**
     * 处理从组长晋升到部长的情况
     * @param conn
     * @return
     */
    private boolean CGradePromote(Connection conn)
    {
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeSet tS1 = new LATreeSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        AscriptDealPersonData tAscriptPerson = new AscriptDealPersonData();
        AscriptDealGroupData tAscriptGroup = new AscriptDealGroupData();
        PubFun1 tPF1 = new PubFun1();

        VData tNewGroup = new VData();
        //新部号
        String tNewDGroup = "";

        String sql_where = "";
        int tCount = 0;

        /**生成新部号*/
        tNewDGroup = tPF1.CreateMaxNo("AGENTGROUP", 12);
        String tUpBranchAttr = getUpBranchAttr();
        if (tUpBranchAttr == null || tUpBranchAttr.equals(""))
        {
            dealError("CGradePromote", "生成新组号时取上级组号失败！");
            return false;
        }

        CreateBranchAttr tCreateAttr = CreateBranchAttr.getInstance();
        if (!tCreateAttr.init(tUpBranchAttr, "Y", 15))
        {
            dealError("CGradePromote", "生成新部号失败！");
            return false;
        }
        String tNewBranchAttr = tCreateAttr.getDepNo();
        System.out.println("-----genDepNo : " + tNewBranchAttr);

        /**归属直辖组*/
        System.out.println("-----@ 归属直辖组 @-----");

        tS1.clear();
        tS1.add(this.mLATreeSchema);
        System.out.println("----tS1.size---" + tS1.size());
        if (!prepareGrpOutputData("D", tS1, tNewDGroup, tNewBranchAttr))
        {
            return false;
        }

        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;
        }

        /** 由于没有生成新组，所以 AgentGroup 没变，且营业组下的代理人的 upAgent 没有改变，
         * 因此归属时只需要刷新该营业组的 LABranchGroup 以及改组的业务经理的 upAgent 即可,
         * 而无需修改营业组下的每个营销员。*/

        //查询直接育成人
        sql_where = "select * from latree a,laagent b where   a.agentcode=b.agentcode and trim(a.agentcode) in ("
                    + "select distinct agentcode from latreeaccessory"
                    + " where trim(rearagentcode) = '"
                    + this.m_AgentCode.trim()
                    +
                    "' and (trim(AgentGrade) = 'A04' or trim(AgentGrade) = 'A05'))"
                    + " and b.agentstate<'03' ";

        System.out.println("查询直接育成人 sql: " + sql_where.trim());

        tLATreeSet = tLATreeDB.executeQuery(sql_where);
        tCount = tLATreeSet.size();

        //晋升高级经理直接育成组不得少于2组
//zsj--delete--2004-3-9:不需要加此限制
//    if (tCount <= 0) {
//      dealError("CGradePromote", "取直接育成人失败！");
//      return false;
//    }

        for (int i = 1; i <= tCount; i++)
        {
            tLATreeSchema = tLATreeSet.get(i);
            LATreeSet tQryResult = new LATreeSet();

            //根据直接育成人查询间接育成人
            tQryResult = RecursiveQry(2, tLATreeSchema, tQryResult);
            if (tQryResult == null)
            {
                return false;
            }
            /**归属间接育成组*/
            System.out.println("------@ 归属间接育成组 @-----");
            if (!prepareGrpOutputData("I", tQryResult, tNewDGroup,
                                      tNewBranchAttr))
            {
                return false;
            }
            if (!tAscriptGroup.dealGroupData(mOutputData, conn))
            {
                this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
                return false;
            }
        }

        /**归属直接育成组*/
        System.out.println("------@ 归属直接育成组 @-----");
        if (!prepareGrpOutputData("I", tLATreeSet, tNewDGroup, tNewBranchAttr))
        {
            return false;
        }
        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;
        }

        /** 归属本代理人 */
        System.out.println("------@ 归属被晋升的代理人 @-----");
        if (!prepareOutputData(this.mLATreeSchema, tNewDGroup, tNewBranchAttr,
                               "S",
                               "Y"))
        {
            return false;
        }
        if (!tAscriptPerson.dealPersonData(this.mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptPerson.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 处理从组长晋升到部长的情况
     * @param conn
     * @return
     */
    private boolean CGradePromoteNew(Connection conn)
    {
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeSet tS1 = new LATreeSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        AscriptDealPersonData tAscriptPerson = new AscriptDealPersonData();
        AscriptDealGroupData tAscriptGroup = new AscriptDealGroupData();
        PubFun1 tPF1 = new PubFun1();

        VData tNewGroup = new VData();
        //新部号
        String tNewDGroup = "";

        String sql_where = "";
        int tCount = 0;

        /**生成新部号*/
        tNewDGroup = tPF1.CreateMaxNo("AGENTGROUP", 12);
        String tUpBranchAttr = getUpBranchAttr();
        if (tUpBranchAttr == null || tUpBranchAttr.equals(""))
        {
            dealError("CGradePromote", "生成新组号时取上级组号失败！");
            return false;
        }

        CreateBranchAttr tCreateAttr = CreateBranchAttr.getInstance();
        if (!tCreateAttr.init(tUpBranchAttr, "Y", 15))
        {
            dealError("CGradePromote", "生成新部号失败！");
            return false;
        }
        String tNewBranchAttr = tCreateAttr.getDepNo();
        System.out.println("-----genDepNo : " + tNewBranchAttr);

        /**归属直辖组*/
        System.out.println("-----@ 归属直辖组 @-----");

        tS1.clear();
        tS1.add(this.mLATreeSchema);
        System.out.println("----tS1.size---" + tS1.size());
        if (!prepareGrpOutputData("D", tS1, tNewDGroup, tNewBranchAttr))
        {
            return false;
        }

        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;
        }

        tLATreeSchema = new LATreeSchema();
        tLATreeSet = new LATreeSet();
        tLATreeSet.add(this.mLATreeSchema);
        LATreeSet tNewLATreeSet = new LATreeSet();
        tNewLATreeSet = this.RecursiveQryNew(2, tLATreeSet);

        System.out.println("before check:" + tNewLATreeSet.size());
        tLATreeSet = this.checkRecursiveQry(tNewLATreeSet, "B", tUpBranchAttr);
        System.out.println("after check:" + tLATreeSet.size());

        if (!prepareGrpOutputData("I", tLATreeSet, tNewDGroup, tNewBranchAttr))
        {
            return false;
        }
        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;

        }

        /** 归属本代理人 */
        System.out.println("------@ 归属被晋升的代理人 @-----");
        if (!prepareOutputData(this.mLATreeSchema, tNewDGroup, tNewBranchAttr,
                               "S",
                               "Y"))
        {
            return false;
        }
        if (!tAscriptPerson.dealPersonData(this.mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptPerson.mErrors);
            return false;
        }

        return true;
    }

    private boolean DGradePromote(Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeSet tS1 = new LATreeSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        AscriptDealPersonData tAscriptPerson = new AscriptDealPersonData();
        AscriptDealGroupData tAscriptGroup = new AscriptDealGroupData();

        String sql_where = "";
        int tCount = 0;

        /** 生成督导部展业机构编码 */
        String tNewAGroup = tPF1.CreateMaxNo("AGENTGROUP", 12);
        String tUpBranchAttr = getUpBranchAttr();
        if (tUpBranchAttr == null || tUpBranchAttr.equals(""))
        {
            dealError("BGradePromote", "生成新组号时取上级组号失败！");
            return false;
        }

        CreateBranchAttr tCreateAttr = CreateBranchAttr.getInstance();
        if (!tCreateAttr.init(tUpBranchAttr, "Y", 12))
        {
            dealError("DGradePromote", "生成新组号失败！");
            return false;
        }
        String tNewAAttr = tCreateAttr.getSDepNo();
        System.out.println("-----genSDepNo : " + tNewAAttr);

        /** 归属直辖机构 */
        System.out.println("-----@ 归属直辖机构 @-----");

        tS1.add(this.mLATreeSchema);
        if (!prepareGrpOutputData("D", tS1, tNewAGroup, tNewAAttr))
        {
            return false;
        }

        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;
        }

        /** 查询直接育成人 */
        sql_where =
                "select * from latree a,laagent b where a.agentcode=b.agentcode "
                + " and trim(a.agentcode) in ("
                + "select distinct agentcode from latreeaccessory"
                + " where trim(rearagentcode) = '"
                + this.m_AgentCode.trim()
                + "' and (trim(AgentGrade) = 'A06' or trim(AgentGrade)='A07'))"
                + " and b.agentstate<'03'";

        System.out.println("查询直接育成人 sql: " + sql_where.trim());

        tLATreeSet = tLATreeDB.executeQuery(sql_where);
        tCount = tLATreeSet.size();

        for (int i = 1; i <= tCount; i++)
        {
            tLATreeSchema = tLATreeSet.get(i);
            LATreeSet tQryResult = new LATreeSet();

            //根据直接育成人查询间接育成人
            tQryResult = RecursiveQry(3, tLATreeSchema, tQryResult);

            /** 归属间接育成机构 */
            System.out.println("------@ 归属间接育成机构 @-----");
            if (!prepareGrpOutputData("I", tQryResult, tNewAGroup, tNewAAttr))
            {
                return false;
            }
            if (!tAscriptGroup.dealGroupData(mOutputData, conn))
            {
                this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
                return false;
            }
        }

        /** 归属直接育成机构 */
        System.out.println("------@ 归属直接育成机构 @-----");
        if (!prepareGrpOutputData("I", tLATreeSet, tNewAGroup, tNewAAttr))
        {
            return false;
        }
        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;
        }

        /**归属本代理人*/
        System.out.println("------@ 归属被晋升的代理人 @-----");

        if (!prepareOutputData(this.mLATreeSchema, tNewAGroup, tNewAAttr, "S",
                               "Y"))
        {
            return false;
        }
        if (!tAscriptPerson.dealPersonData(this.mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptPerson.mErrors);
            return false;
        }
        return true;
    }

    private boolean DGradePromoteNew(Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeSet tS1 = new LATreeSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        AscriptDealPersonData tAscriptPerson = new AscriptDealPersonData();
        AscriptDealGroupData tAscriptGroup = new AscriptDealGroupData();

        String sql_where = "";
        int tCount = 0;

        /** 生成督导部展业机构编码 */
        String tNewAGroup = tPF1.CreateMaxNo("AGENTGROUP", 12);
        String tUpBranchAttr = getUpBranchAttr();
        if (tUpBranchAttr == null || tUpBranchAttr.equals(""))
        {
            dealError("BGradePromote", "生成新组号时取上级组号失败！");
            return false;
        }

        CreateBranchAttr tCreateAttr = CreateBranchAttr.getInstance();
        if (!tCreateAttr.init(tUpBranchAttr, "Y", 12))
        {
            dealError("DGradePromote", "生成新组号失败！");
            return false;
        }
        String tNewAAttr = tCreateAttr.getSDepNo();
        System.out.println("-----genSDepNo : " + tNewAAttr);

        /** 归属直辖机构 */
        System.out.println("-----@ 归属直辖机构 @-----");

        tS1.add(this.mLATreeSchema);
        if (!prepareGrpOutputData("D", tS1, tNewAGroup, tNewAAttr))
        {
            return false;
        }

        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;
        }

        tLATreeSchema = new LATreeSchema();
        tLATreeSet = new LATreeSet();
        tLATreeSet.add(this.mLATreeSchema);
        LATreeSet tNewLATreeSet = new LATreeSet();
        tNewLATreeSet = this.RecursiveQryNew(3, tLATreeSet);

        System.out.println("before check:" + tNewLATreeSet.size());
        tLATreeSet = this.checkRecursiveQry(tNewLATreeSet, "C", tUpBranchAttr);
        System.out.println("after check:" + tLATreeSet.size());

        if (!prepareGrpOutputData("I", tLATreeSet, tNewAGroup, tNewAAttr))
        {
            return false;
        }
        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;
        }

        /**归属本代理人*/
        System.out.println("------@ 归属被晋升的代理人 @-----");

        if (!prepareOutputData(this.mLATreeSchema, tNewAGroup, tNewAAttr, "S",
                               "Y"))
        {
            return false;
        }
        if (!tAscriptPerson.dealPersonData(this.mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptPerson.mErrors);
            return false;
        }
        return true;
    }

    private boolean TopGradePromote(Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeSet tS1 = new LATreeSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        AscriptDealPersonData tAscriptPerson = new AscriptDealPersonData();
        AscriptDealGroupData tAscriptGroup = new AscriptDealGroupData();

        String sql_where = "";
        int tCount = 0;

        /** 生成区域督导部展业机构编码 */
        String tNewAGroup = tPF1.CreateMaxNo("AGENTGROUP", 12);
        String tUpBranchAttr = getUpBranchAttr();
        if (tUpBranchAttr == null || tUpBranchAttr.equals(""))
        {
            dealError("BGradePromote", "生成新组号时取上级组号失败！");
            return false;
        }

        CreateBranchAttr tCreateAttr = CreateBranchAttr.getInstance();
        if (!tCreateAttr.init(tUpBranchAttr, "Y", 10))
        {
            dealError("TopGradePromote", "生成新区域督导编码失败！");
            return false;
        }
        String tNewAAttr = tCreateAttr.getASDepNo();
        System.out.println("-----genASDepNo : " + tNewAAttr);

        /** 归属直辖机构 */
        System.out.println("-----@ 归属直辖机构 @-----");

        tS1.add(this.mLATreeSchema);
        if (!prepareGrpOutputData("D", tS1, tNewAGroup, tNewAAttr))
        {
            return false;
        }

        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;
        }

        /** 查询直接育成人 */
        sql_where = "select * from latree a,laagent b where  a.agentcode=b.agentcode and a.agentcode in ( "
                    + "select distinct agentcode from latreeaccessory where  "
                    + "trim(rearagentcode) = '"
                    + this.m_AgentCode.trim()
                    + "' and agentgrade = 'A08') "
                    + " and b.agentstate<'03'  ";

        System.out.println("查询直接育成人 sql: " + sql_where.trim());

        tLATreeSet = tLATreeDB.executeQuery(sql_where);
        tCount = tLATreeSet.size();

        for (int i = 1; i <= tCount; i++)
        {
            tLATreeSchema = tLATreeSet.get(i);
            LATreeSet tQryResult = new LATreeSet();

            //根据直接育成人查询间接育成人
            tQryResult = RecursiveQry(4, tLATreeSchema, tQryResult);

            /** 归属间接育成机构 */
            System.out.println("------@ 归属间接育成机构 @-----");
            if (!prepareGrpOutputData("I", tQryResult, tNewAGroup, tNewAAttr))
            {
                return false;
            }
            if (!tAscriptGroup.dealGroupData(mOutputData, conn))
            {
                this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
                return false;
            }
        }

        /** 归属直接育成机构 */
        System.out.println("------@ 归属直接育成机构 @-----");
        if (!prepareGrpOutputData("I", tLATreeSet, tNewAGroup, tNewAAttr))
        {
            return false;
        }
        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;
        }

        /**归属本代理人*/
        System.out.println("------@ 归属被晋升的代理人 @-----");

        if (!prepareOutputData(this.mLATreeSchema, tNewAGroup, tNewAAttr, "S",
                               "Y"))
        {
            return false;
        }
        if (!tAscriptPerson.dealPersonData(this.mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptPerson.mErrors);
            return false;
        }

        return true;
    }

    private boolean TopGradePromoteNew(Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeSet tS1 = new LATreeSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        AscriptDealPersonData tAscriptPerson = new AscriptDealPersonData();
        AscriptDealGroupData tAscriptGroup = new AscriptDealGroupData();

        String sql_where = "";
        int tCount = 0;

        /** 生成区域督导部展业机构编码 */
        String tNewAGroup = tPF1.CreateMaxNo("AGENTGROUP", 12);
        String tUpBranchAttr = getUpBranchAttr();
        if (tUpBranchAttr == null || tUpBranchAttr.equals(""))
        {
            dealError("BGradePromote", "生成新组号时取上级组号失败！");
            return false;
        }

        CreateBranchAttr tCreateAttr = CreateBranchAttr.getInstance();
        if (!tCreateAttr.init(tUpBranchAttr, "Y", 10))
        {
            dealError("TopGradePromote", "生成新区域督导编码失败！");
            return false;
        }
        String tNewAAttr = tCreateAttr.getASDepNo();
        System.out.println("-----genASDepNo : " + tNewAAttr);

        /** 归属直辖机构 */
        System.out.println("-----@ 归属直辖机构 @-----");

        tS1.add(this.mLATreeSchema);
        if (!prepareGrpOutputData("D", tS1, tNewAGroup, tNewAAttr))
        {
            return false;
        }

        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;
        }

        tLATreeSchema = new LATreeSchema();
        tLATreeSet = new LATreeSet();
        tLATreeSet.add(this.mLATreeSchema);
        LATreeSet tNewLATreeSet = new LATreeSet();
        tNewLATreeSet = this.RecursiveQryNew(4, tLATreeSet);

        System.out.println("before check:" + tNewLATreeSet.size());
        tLATreeSet = this.checkRecursiveQry(tNewLATreeSet, "D", tUpBranchAttr);
        System.out.println("after check:" + tLATreeSet.size());

        if (!prepareGrpOutputData("I", tLATreeSet, tNewAGroup, tNewAAttr))
        {
            return false;
        }
        if (!tAscriptGroup.dealGroupData(mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptGroup.mErrors);
            return false;
        }

        /**归属本代理人*/
        System.out.println("------@ 归属被晋升的代理人 @-----");

        if (!prepareOutputData(this.mLATreeSchema, tNewAGroup, tNewAAttr, "S",
                               "Y"))
        {
            return false;
        }
        if (!tAscriptPerson.dealPersonData(this.mOutputData, conn))
        {
            this.mErrors.copyAllErrors(tAscriptPerson.mErrors);
            return false;
        }

        return true;
    }

    /*
      private String getUpAgent()  {
        String tUpAgent = "";
        LATreeDB tDB = new LATreeDB();
        LATreeSchema tSch = new LATreeSchema();
        if ( this.mLAAssessSchema.getAgentGrade1().trim().equals("D2") )
          // 区域督导长的上级代理人设置为“BC”
          tUpAgent = "BC";
        else  {
          tDB.setAgentCode(this.mLATreeSchema.getUpAgent());
          if ( !tDB.getInfo() )
            return "";
          tSch = tDB.getSchema();
          tUpAgent = tSch.getUpAgent();
        }
        return tUpAgent;
      }*/

    private LATreeSet RecursiveQry(int Flag, LATreeSchema tTreeSch,
                                   LATreeSet tResult)
    {
        String tSql = "";
        LATreeDB tDB = new LATreeDB();
        LATreeSet tSet = new LATreeSet();
        LATreeSchema tSch = new LATreeSchema();

        switch (Flag)
        {
            case 1: // 查询被推荐人
                tSql =
                        "select a.* from latree a,laagent b where a.introagency = '"
                        + tTreeSch.getAgentCode().trim()
                        + "' and a.agentgroup = '"
                        + tTreeSch.getAgentGroup().trim() + "'"
                        + " and a.agentcode = b.agentcode "
                        //+ " and b.agentstate < '03'"
                        ;
                break;

            case 2: // 查询被育成的组经理
                tSql = "select * from latree a,laagent b where a.agentcode=b.agentcode and trim(a.agentcode) in ("
                       + "select distinct agentcode from latreeaccessory"
                       + " where trim(rearagentcode) = '"
                       + tTreeSch.getAgentCode().trim()
                       +
                       "' and (trim(AgentGrade) = 'A04' or trim(AgentGrade) = 'A05')"
                       // + " and ( rearflag is null or rearflag<>'1' )
                       + ")" //caigang 2004-04-26添加，在查询育成关系的时候，加入RearFlag字段的限制
                       // +" and b.agentstate<'03' "
                       ;
                break;

            case 3: // 查询被育成的部经理
                tSql = "select * from latree a,laagent b where a.agentcode=b.agentcode and trim(a.agentcode) in ("
                       + "select distinct agentcode from latreeaccessory"
                       + " where trim(rearagentcode) = '"
                       + tTreeSch.getAgentCode().trim()
                       +
                       "' and (trim(AgentGrade) = 'A06' or trim(AgentGrade)='A07')"
                       // + " and ( rearflag is null or rearflag<>'1' )"
                       + ")" //caigang 2004-04-26添加，在查询育成关系的时候，加入RearFlag字段的限制
                       //+" and b.agentstate<'03' "
                       ;
                break;

            case 4: // 查询被育成的督导长
                tSql = "select * from latree a,laagent b where a.agentcode=b.agentcode and a. agentcode in ( "
                       +
                       "select distinct agentcode from latreeaccessory where "
                       + "trim(rearagentcode) = '"
                       + tTreeSch.getAgentCode().trim()
                       + "' and agentgrade = 'A08' )"
                       //+ " and ( rearflag is null or rearflag<>'1' )
                       + ")" //caigang 2004-04-26添加，在查询育成关系的时候，加入RearFlag字段的限制
                       //+" and b.agentstate<'03' "
                       ;
                break;

            default:
                tSql = "";
                break;
        }

        System.out.println("---tSql : " + tSql);
        tSet = tDB.executeQuery(tSql);
        int tCount = tSet.size();
        //递归出口
        if (tCount == 0)
        {
            return tResult;
        }
        tResult.add(tSet);
        LATreeSchema tS = new LATreeSchema();
        for (int i = 1; i <= tCount; i++)
        {
            tS.setSchema(tSet.get(i));
            tResult = RecursiveQry(Flag, tS, tResult);
        }
        System.out.println("RecursiveQry" + tTreeSch.getAgentCode() + "==" +
                           tResult.size());
        return tResult;
    }

    private LATreeSet RecursiveQryNew(int Flag, LATreeSet aLATreeSet)
    {
        String tSql = "";
        LATreeDB tDB = new LATreeDB();
        LATreeSet tResult = new LATreeSet();
        LATreeSchema tSch = new LATreeSchema();

        for (int i = 1; i <= aLATreeSet.size(); i++)
        {
            String agentCode = aLATreeSet.get(i).getAgentCode();
            switch (Flag)
            {
                case 1: // 查询被推荐人
                    tSql = "select a.* from latree a"
                           + ""
                           + " where a.introagency = '"
                           + agentCode.trim()
                           + "'"
                           // +" and a.agentgroup = '"
                           // + this.m_AgentGroup.trim() + "'"
                           // + " and a.agentcode = b.agentcode "
                           //+ " and b.agentstate < '03'"
                           ;
                    break;

                case 2: // 查询被育成的组经理
                    tSql =
                            "select a.* from latree a where  trim(a.agentcode) in ("
                            + "select distinct agentcode from latreeaccessory"
                            + " where trim(rearagentcode) = '"
                            + agentCode.trim()
                            +
                            "' and (trim(AgentGrade) = 'A04' or trim(AgentGrade) = 'A05')"
                            // + " and ( rearflag is null or rearflag<>'1' )
                            + ")" //caigang 2004-04-26添加，在查询育成关系的时候，加入RearFlag字段的限制
                            // +" and b.agentstate<'03' "
                            ;
                    break;

                case 3: // 查询被育成的部经理
                    tSql =
                            "select a.* from latree a where trim(a.agentcode) in ("
                            + "select distinct agentcode from latreeaccessory"
                            + " where trim(rearagentcode) = '"
                            + agentCode.trim()
                            +
                            "' and (trim(AgentGrade) = 'A06' or trim(AgentGrade)='A07')"
                            // + " and ( rearflag is null or rearflag<>'1' )"
                            + ")" //caigang 2004-04-26添加，在查询育成关系的时候，加入RearFlag字段的限制
                            //+" and b.agentstate<'03' "
                            ;
                    break;

                case 4: // 查询被育成的督导长
                    tSql = "select a.* from latree a where  a. agentcode in ( "
                           +
                           "select distinct agentcode from latreeaccessory where "
                           + "trim(rearagentcode) = '"
                           + agentCode.trim()
                           + "' and agentgrade = 'A08' )"
                           ;
                    break;

                default:
                    tSql = "";
                    break;
            }

            System.out.println("---tSql : " + tSql);
            LATreeSet tSet = new LATreeSet();
            tSet = tDB.executeQuery(tSql);
            tResult.add(tSet);
            if (tSet.size() > 0)
            {

                LATreeSet buf = RecursiveQryNew(Flag, tSet);
                tResult.add(buf);
            }
        }

        return tResult;
    }

    private String getUpBranchAttr()
    {
        String tUpBranchAttr = "";
        LABranchGroupDB tGroupDB = new LABranchGroupDB();

        tGroupDB.setAgentGroup(this.m_AgentGroup);
        if (!tGroupDB.getInfo())
        {
            return "";
        }

        if (this.mLATreeSchema.getAgentGrade().trim().equals("A08"))
        {
            tUpBranchAttr = tGroupDB.getManageCom();
        }
        else
        {
            tGroupDB.setAgentGroup(tGroupDB.getUpBranch());
            if (!tGroupDB.getInfo())
            {
                return "";
            }
            tUpBranchAttr = tGroupDB.getBranchAttr();
        }
        return tUpBranchAttr;
    }

    private boolean getInputData()
    {
        this.mGlobalInput.setSchema((GlobalInput)this.mInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLAAssessSchema = (LAAssessSchema)this.mInputData.
                               getObjectByObjectName("LAAssessSchema", 0);
        this.mIndexCalNo = (String)this.mInputData.getObjectByObjectName(
                "String",
                0);

        if (this.mGlobalInput == null || this.m_AgentCode == null)
        {
            // @@错误处理
            dealError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "DoAscriptPromote";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }

    private boolean prepareOutputData(LATreeSchema tLATreeSchema,
                                      String tNewAgentGroup,
                                      String tNewAgentAttr,
                                      String ModTypeFlag, String InsFlag)
    {
        try
        {
            this.mOutputData.clear();
            this.mOutputData.add(this.mGlobalInput);
            this.mOutputData.add(tLATreeSchema);
            this.mOutputData.add(0, this.mIndexCalNo);
            this.mOutputData.add(1, tNewAgentGroup);
            this.mOutputData.add(2, tNewAgentAttr);
            this.mOutputData.add(3, ModTypeFlag);
            this.mOutputData.add(4, InsFlag);
            this.mOutputData.add(5, this.m_AgentCode);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("BGradePromote", "在准备往后层处理所需要的数据时出错!");
            return false;
        }
        return true;
    }

    private boolean prepareGrpOutputData(String Flag, LATreeSet tLATreeSet,
                                         String tDGroup, String tDAttr)
    {
        try
        {
            this.mOutputData.clear();
            this.mOutputData.add(this.mGlobalInput);
            this.mOutputData.add(this.conn);
            this.mOutputData.add(0, Flag);
            this.mOutputData.add(1, this.mIndexCalNo);
            this.mOutputData.add(tLATreeSet);
            this.mOutputData.add(this.mLATreeSchema);
            this.mOutputData.add(2, tDGroup);
            this.mOutputData.add(3, tDAttr);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("BGradePromote", "在准备往后层处理所需要的数据时出错!");
            return false;
        }
        return true;
    }

} //end class
