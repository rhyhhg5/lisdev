package com.sinosoft.lis.agentassess;

import java.math.BigInteger;

import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>Title: CreateBranchAttr</p>
 * <p>Description: 生成展业机构的外部编码</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author not attributable
 * @version 1.0
 */

/**
 * 生成本机构下新成立的机构编码：BranchAttr
 * @param tUpGroup：新建机构的上级机构的BranchAttr编码
 * @param tLength ：编码长度(10位-区域督导部、12－督导部、15－营销服务部、18－营业组）
 * @return：新建机构的编码BranchAttr
 */

public class CreateBranchAttr
{

    private static String TeamNo = null;
    private static String DepNo = null;
    private static String SDepNo = null;
    private static String ASDepNo = null;
    private static String mFlag = "Y";

    private static CreateBranchAttr mInstance = new CreateBranchAttr();

    public static CErrors mErrors = new CErrors();

    private CreateBranchAttr()
    {
    }

    public static void main(String[] args)
    {
        CreateBranchAttr tCreateNo = new CreateBranchAttr();
        tCreateNo.init("861100000203007", "Y", 15);
        System.out.println("------TeamNo : " + tCreateNo.getTeamNo());

    }

    //DBFlag = 'Y':生成新的外部展业机构编码时需要查询数据库
    //       = 'N'：无需查询数据库，直接用传入的tUpAttr生成
    public synchronized boolean init(String tUpAttr, String DBFlag, int tLength)
    {
        if (mFlag.trim().equals("Y"))
        {
            mFlag = "F";
        }
        else
        {
            return false;
        }

        String aNewAttr = "";
        if (DBFlag.trim().equalsIgnoreCase("Y"))
        {
            LABranchGroupDB tDB = new LABranchGroupDB();
            LABranchGroupSchema tSch = new LABranchGroupSchema();
            ExeSQL tExeSql = new ExeSQL();

            String aNewAttrA = "";
            String aNewAttrB = "";

            if (tLength == 18)
            {
                tUpAttr = tUpAttr.trim().substring(0, 15);
            }
            if (tLength == 15)
            {
                tUpAttr = tUpAttr.trim().substring(0, 12);
            }
            if (tLength == 12)
            {
                tUpAttr = tUpAttr.trim().substring(0, 10);
            }
            if (tLength == 10)
            {
                tUpAttr = tUpAttr.trim().substring(0, 8);
            }

            String tSQL =
                    "select max(branchattr) from labranchgroup where trim(branchattr) like '"
                    + tUpAttr.trim() +
                    "%' and (trim(state) <> '1' or state is null)"
                    + " and branchtype = '1' and length(trim(branchattr)) = " +
                    tLength;

            // 查询备份表，比较得出一个最大号
            String tSQLB =
                    "select max(branchattr) from labranchgroupb where trim(branchattr) like '"
                    + tUpAttr.trim() +
                    "%' and (trim(state) <> '1' or state is null)"
                    + " and branchtype = '1' and length(trim(branchattr)) = " +
                    tLength;

            aNewAttrA = tExeSql.getOneValue(tSQL);
            aNewAttrB = tExeSql.getOneValue(tSQLB);
            System.out.println("---LABranchGroup---maximum = " + aNewAttrA);
            System.out.println("---LABranchGroupB--maximum = " + aNewAttrB);

            if (aNewAttrB == null || aNewAttrB.equals(""))
            {
                aNewAttr = aNewAttrA;
            }
            else
            {
                if (aNewAttrA.trim().compareToIgnoreCase(aNewAttrB.trim()) >= 0)
                {
                    aNewAttr = aNewAttrA.trim();
                }
                else
                {
                    aNewAttr = aNewAttrB.trim();
                }
            }
            System.out.println("-----maxium = " + aNewAttr.trim());
        }
        else
        {
            aNewAttr = tUpAttr;
        }

        if (DBFlag.trim().equalsIgnoreCase("Y"))
        {
            BigInteger tInt = null;
            BigInteger tAdd = null;
            try
            {
                tInt = new BigInteger(aNewAttr.trim());
                tAdd = new BigInteger("1");
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                return false;
            }
            tInt = tInt.add(tAdd);
            aNewAttr = tInt.toString();
            System.out.println("---aNewAttr = " + aNewAttr);
        }

        if (tLength == 18)
        {
            TeamNo = aNewAttr;
        }

        if (tLength == 15)
        {
            DepNo = aNewAttr;
            TeamNo = aNewAttr + "001";
        }

        if (tLength == 12)
        {
            SDepNo = aNewAttr;
            DepNo = aNewAttr.trim() + "001";
            TeamNo = DepNo.trim() + "001";
        }

        if (tLength == 10)
        {
            ASDepNo = aNewAttr;
            SDepNo = ASDepNo.trim() + "01";
            DepNo = SDepNo.trim() + "001";
            TeamNo = DepNo.trim() + "001";
        }

        return true;
    }

    static public CreateBranchAttr getInstance()
    {
        return mInstance;
    }

    public String getTeamNo()
    {
        String tTeamNo = TeamNo;

        BigInteger tInt = null;
        BigInteger tAdd = null;

        try
        {
            tInt = new BigInteger(TeamNo.trim());
            tAdd = new BigInteger("1");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
        tInt = tInt.add(tAdd);
        TeamNo = tInt.toString();

        return tTeamNo;
    }

    public String getDepNo()
    {
        String tDepNo = DepNo;

        BigInteger tInt = null;
        BigInteger tAdd = null;

        try
        {
            tInt = new BigInteger(DepNo.trim());
            tAdd = new BigInteger("1");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
        tInt = tInt.add(tAdd);
        DepNo = tInt.toString();

        initAgain("D", tDepNo);
        return tDepNo;
    }

    public String getSDepNo()
    {
        String tSDepNo = SDepNo;

        BigInteger tInt = null;
        BigInteger tAdd = null;

        try
        {
            tInt = new BigInteger(SDepNo.trim());
            tAdd = new BigInteger("1");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
        tInt = tInt.add(tAdd);
        SDepNo = tInt.toString();
        initAgain("S", tSDepNo);
        return tSDepNo;
    }

    public String getASDepNo()
    {
        String tASDepNo = ASDepNo;

        BigInteger tInt = null;
        BigInteger tAdd = null;
        try
        {
            tInt = new BigInteger(ASDepNo.trim());
            tAdd = new BigInteger("1");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
        tInt = tInt.add(tAdd);
        ASDepNo = tInt.toString();
        initAgain("A", tASDepNo);
        return tASDepNo;
    }

    public synchronized void release()
    {
        mFlag = "Y";
        System.out.println("-----createNo---release!---");
    }

    private void initAgain(String tLevel, String tNo)
    {
        if (tLevel.trim().equalsIgnoreCase("D"))
        {
            TeamNo = tNo + "001";
        }

        if (tLevel.trim().equalsIgnoreCase("S"))
        {
            DepNo = tNo + "001";
            TeamNo = DepNo + "001";
        }

        if (tLevel.trim().equalsIgnoreCase("A"))
        {
            SDepNo = tNo + "01";
            DepNo = SDepNo + "001";
            TeamNo = DepNo + "001";
        }
    }

}
