package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 代理人降级组织归属</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zhangsj
 * @version 1.0
 */

public class DoAscriptDemote
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public static CErrors mErrors = new CErrors();

    /** 全局变量 */
    public GlobalInput mGlobalInput = new GlobalInput();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private VData mInputData = new VData();
    private LAAssessSchema mLAAssessSchema = new LAAssessSchema();
    private String m_AgentCode = "";
    private String m_AgentGroup = "";
    private String mIndexCalNo = "";
    private String[] MonDate = new String[2];

    //连接
    private Connection conn = null;

    /** 往后面传输数据的容器 */
    private VData mOutputData = new VData();

    public DoAscriptDemote()
    {
    }

    public static void main(String[] args)
    {
        DoAscriptDemote doAscriptDemote1 = new DoAscriptDemote();

    }

    public boolean demote(VData cInputData)
    {
        this.mInputData = cInputData;

        //得到传入的变量值
        if (!getInputData())
        {
            return false;
        }

        //校验数据
        if (!dealData())
        {
            return false;
        }

        //生成一个字符串数组，存储两个日期，数组的第0个单元存储考核年月的下一个月的1号，数组的第1个单元
        //存储考核年月当月的最后一天
        this.MonDate = genDate();

        this.m_AgentCode = this.mLAAssessSchema.getAgentCode();
        String tAgentSeries1 = this.mLAAssessSchema.getAgentSeries1();
        String tAgentGrade1 = this.mLAAssessSchema.getAgentGrade1();
        String tAgentSeries = this.mLAAssessSchema.getAgentSeries();
        String tAgentGrade = this.mLAAssessSchema.getAgentGrade();

        //取降级代理人行政信息表信息,准备全局变量
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(this.m_AgentCode);
        if (!tLATreeDB.getInfo())
        {
            dealError("Demote", "取代理人行政信息失败！");
            return false;
        }
        //记录被降级代理人的当前组
        this.mLATreeSchema = tLATreeDB.getSchema();
        this.m_AgentGroup = this.mLAAssessSchema.getAgentGroup();

        CreateBranchAttr tCreateNo = CreateBranchAttr.getInstance();
        //建立一个连接
        this.conn = DBConnPool.getConnection();
        if (conn == null)
        {
            dealError("Demote", "数据库连接失败！");
            return false;
        }

        try
        {
            conn.setAutoCommit(false);
            //如果是清退的，则调用清退的函数demoteAgent()执行清退操作
            if (tAgentSeries1.trim().equalsIgnoreCase("0"))
            {
                if (!demoteAgent())
                {
                    tCreateNo.release();
                    conn.rollback();
                    System.out.println("----rollback----");
                    conn.close();
                    return false;
                }
                else
                {
                    tCreateNo.release();
                    conn.commit();
                    conn.close();
                    return true;
                }
            }
            //降职处理
            if (!doDemote())
            {
                tCreateNo.release();
                System.out.println("----rollback----");
                conn.rollback();
                conn.close();
                return false;
            }
            tCreateNo.release();
            conn.commit();
            System.out.println("---- Now commit rollback ----");
            //conn.rollback();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            try
            {
                tCreateNo.release();
                conn.rollback();
                conn.close();
            }
            catch (Exception ep)
            {
                ep.printStackTrace();
                dealError("Demote", ep.toString());
                System.out.println("----conn.rollback and close failed!----");

            }
            dealError("Demote", ex.toString());
            return false;
        }
        return true;
    }


    //代理人被清退
    private boolean demoteAgent()
    {
        PubFun tPF = new PubFun();
        LAAgentDB tDB = new LAAgentDB(this.conn);
        LAAgentSchema tSch = new LAAgentSchema();
        //调用DealRelaTab类备份LAAgent表
        DealRelaTab tDT = new DealRelaTab();
        VData tOutData = new VData();
        tOutData.clear();
        tOutData.add(this.mGlobalInput);
        tOutData.add(this.mIndexCalNo);
        if (!tDT.getInputData(tOutData))
        {
            return false;
        }

        if (!tDT.insLAAgentB(this.m_AgentCode, this.conn))
        {
            dealError("demoteAgent", tDT.mErrors.getFirstError());
            return false;
        }

        tDB.setAgentCode(this.m_AgentCode);
        if (!tDB.getInfo())
        {
            dealError("demoteAgent", "降级代理人操作失败！");
            return false;
        }
        //置被清退的代理人的AgentState为05，修改ModifyDate、ModifyTime、Operator
        tSch = tDB.getSchema();
        tSch.setAgentState("05"); //'05'表示考核清退
        tSch.setOutWorkDate(tPF.getCurrentDate());
        tSch.setModifyDate(tPF.getCurrentDate());
        tSch.setModifyTime(tPF.getCurrentTime());
        tSch.setOperator(this.mGlobalInput.Operator);
        tDB.setSchema(tSch);

        if (!tDB.update())
        {
            dealError("demoteAgent", "降级代理人操作失败！");
            return false;
        }

        //插入LATreeB表
//    if ( !tDT.insertLATreeB(this.mLATreeSchema,conn) )
//      return false;

        //刷新LATree表，修改LATree表中的AssessType、State字段、ModifyDate、ModifyTime，Operator
        LATreeDB tTreeDB = new LATreeDB(conn);
        LATreeSchema tTreeSch = new LATreeSchema();
        tTreeSch.setSchema(this.mLATreeSchema);

//    tTreeSch.setAgentLastGrade(this.mLATreeSchema.getAgentGrade());
//    tTreeSch.setAgentLastSeries(this.mLATreeSchema.getAgentSeries());
// zsj－2003-7-30-delete：被清退职级不变
//    tTreeSch.setAgentGrade("00");
//    tTreeSch.setAgentSeries("0");
        tTreeSch.setAssessType("3");
        tTreeSch.setState("3");
//    tTreeSch.setOldStartDate(this.mLATreeSchema.getStartDate());
//    tTreeSch.setStartDate(tPF.getCurrentDate());
//    tTreeSch.setAstartDate(tPF.getCurrentDate());
//    tTreeSch.setOldEndDate(tPF.getCurrentDate());

        tTreeSch.setModifyDate(tPF.getCurrentDate());
        tTreeSch.setModifyTime(tPF.getCurrentTime());
        tTreeSch.setOperator(this.mGlobalInput.Operator);
        //如果LATree表中的IntroAgency不为空，则将增员链断裂标记IntroBreakFlag
        //和增员抽佣止期置上
        if (tTreeSch.getIntroAgency() != null &&
            !tTreeSch.getIntroAgency().equals(""))
        {
            tTreeSch.setIntroBreakFlag("1");
            tTreeSch.setIntroCommEnd(this.MonDate[0]);
        }
        tTreeDB.setSchema(tTreeSch);
        if (!tTreeDB.update())
        {
            return false;
        }

        //插入离职表
        String tDimSql =
                "select max(departtimes) from ladimission where agentcode = '"
                + this.mLATreeSchema.getAgentCode().trim() + "'";
        System.out.println("--插入离职表--sql: " + tDimSql);
        ExeSQL tExe = new ExeSQL();
        String tDepartTimes = tExe.getOneValue(tDimSql);
        int tDepTimes = 0;
        if (tDepartTimes == null || tDepartTimes.trim().equals(""))
        {
            tDepTimes = 1;
        }
        else
        {
            tDepTimes = Integer.parseInt(tDepartTimes) + 1;
        }

        LADimissionDB tDimDB = new LADimissionDB(conn);
        LADimissionSchema tDimSch = new LADimissionSchema();
        tDimSch.setAgentCode(this.mLATreeSchema.getAgentCode());
        tDimSch.setBranchType("1");
        tDimSch.setCheckFlag(""); //结清标志
        tDimSch.setContractFlag(""); //《保险代理合同书》回收标志
        tDimSch.setDepartDate(tPF.getCurrentDate());
        tDimSch.setDepartRsn("考核清退");
        tDimSch.setDepartTimes(tDepTimes);
        tDimSch.setLostFlag(""); //丢失标志
        tDimSch.setMakeDate(tPF.getCurrentDate());
        tDimSch.setMakeTime(tPF.getCurrentTime());
        tDimSch.setModifyDate(tPF.getCurrentDate());
        tDimSch.setModifyTime(tPF.getCurrentTime());
        tDimSch.setNoti(""); //备注
        tDimSch.setOperator(this.mGlobalInput.Operator);
        tDimSch.setPbcFlag(""); //展业证回收标志
        tDimSch.setReceiptFlag(""); //保险费暂收据回收标志

        tDimSch.setWageFlag("Y");
        tDimSch.setWorkFlag(""); //工作证回收标志
        tDimSch.setBranchAttr(AgentPubFun.getAgentBranchAttr(mLATreeSchema.
                getAgentCode()));

        tDimDB.setSchema(tDimSch);
        if (!tDimDB.insert())
        {
            dealError("demoteAgent", "代理人清退插入离职表失败！");
            return false;
        }
        System.out.println("-----离职表insert Succ!");
        /** liujw begin*/
        //更新LATree 和 LATreeAccessory表中的字段,将状态置为'3'表示离职的,将RearFlag置为'1'，所有的育成关系都断裂
        ExeSQL tExeSQL = new ExeSQL(conn);
        String tSql =
                "Update LATreeAccessory set State = '3',rearflag='1' Where AgentCode = '" +
                mLATreeSchema.getAgentCode() + "'";
        tExeSQL = new ExeSQL(conn);
        if (!tExeSQL.execUpdateSQL(tSql))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "DoAscriptDemote";
            tError.functionName = "demoteAgent";
            tError.errorMessage = "修改行政附属信息表中的状态信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        /** liujw end*/
        //刷新LAAssess表
        if (!tDT.updateLAAssess(this.mLAAssessSchema, this.mLATreeSchema, "",
                                conn))
        {
            dealError("demoteAgent", tDT.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private String[] genDate()
    {
        String[] tMonDate = new String[2];
        PubFun tPF = new PubFun();
        String tYear = this.mIndexCalNo.trim().substring(0, 4);
        String tMon = this.mIndexCalNo.trim().substring(4, 6);

        //计算旧职级的止聘日期
        String tOldDate = tYear + "-" + tMon + "-01";
        tMonDate = tPF.calFLDate(tOldDate);
        String tOldDay = tMonDate[1];
        System.out.println("---tOldDay---" + tOldDay);
        //计算新职级的起聘日期或转正日期
        if (tMon.equals("12"))
        {
            tYear = Integer.toString(Integer.parseInt(tYear) + 1);
            tMon = "01";
        }
        else
        {
            tMon = Integer.toString(Integer.parseInt(tMon) + 1);
        }
        int tLength = tMon.length();
        if (tLength == 1)
        {
            tMon = "0" + tMon;
        }
        String tDate = tYear.trim() + "-" + tMon.trim() + "-" + "01";
        System.out.println("---tNewDay---" + tDate);

        tMonDate[0] = tDate;
        tMonDate[1] = tOldDay;
        return tMonDate;
    }


    private boolean doDemote()
    {
        LATreeSchema tTreeSh = new LATreeSchema();
        LATreeSet tTreeSet = new LATreeSet();
        DemoteDealData tDemote = new DemoteDealData();
        String tEduManager = "";
        String tEduGrade = "";
        String tToAgent = "";
        String tToGroup = "";
        String sql = "";

        //从代理人行政信息附属表中取当前职级的育成人
        LATreeAccessoryDB tAccDB = new LATreeAccessoryDB();
        tAccDB.setAgentCode(this.mLATreeSchema.getAgentCode());
        tAccDB.setAgentGrade(this.mLATreeSchema.getAgentGrade());
        if (!tAccDB.getInfo())
        {
            tEduManager = "";
            tEduGrade = "";
        }
        else
        {
            tEduManager = tAccDB.getRearAgentCode();
            tEduGrade = tAccDB.getAgentGrade();
        }

        String tGroupLimit = "";
        String tSeries = this.mLATreeSchema.getAgentSeries();
        if (this.mLATreeSchema.getAgentGrade().trim().equals("A09"))
        {
            tGroupLimit = this.mLATreeSchema.getManageCom();
        }
        else
        {
            tGroupLimit = getGroupLimit(tSeries);
        }

        String tAsLevel = "";
        if (tSeries.trim().equalsIgnoreCase("B"))
        {
            tAsLevel = "01";
        }

        if (tSeries.trim().equalsIgnoreCase("C"))
        {
            tAsLevel = "02";
        }

        if (this.mLATreeSchema.getAgentGrade().trim().equalsIgnoreCase("A08"))
        {
            tAsLevel = "03";
        }

        if (this.mLATreeSchema.getAgentGrade().trim().equalsIgnoreCase("A09"))
        {
            tAsLevel = "04";
        }

        /** 查询要回归的育成机构 */
        LATreeSchema tResultSch = new LATreeSchema();
        //如果被降级的代理人有育成人的话，调用findEduGrp方法，递归查询出被降级的代理人应该归属
        //的机构的主管的AgentCode
        if (tEduManager != null && !tEduManager.equals(""))
        {
            tResultSch = findEduGrp(tGroupLimit,
                                    this.mLATreeSchema.getAgentCode(),
                                    tEduManager, tAsLevel, tEduGrade);
            if (tResultSch != null)
            {
                tToAgent = tResultSch.getAgentCode();
                tToGroup = tResultSch.getAgentGroup();
            }
            else
            { //如果找不到应该归属的机构，则 1：如果被降级的代理人是组长，则调用getEndGroup()方法，取得所在部的AgentGroup和部长
                //2：等待公司统一处理
                if (tSeries.equals("B"))
                {
                    tToGroup = getEndGroup();
                    tToAgent = this.mLATreeSchema.getUpAgent();
                }
                else
                {
                    tToAgent = "";
                    tToGroup = "";
                }
            }
        }
        else
        {
            //如果被降级的代理人没有育成人，则 1：如果被降级的代理人是组长，则调用getEndGroup()方法，取得所在部的AgentGroup和部长
            //2：等待公司统一处理
            if (tSeries.equals("B"))
            {
                tToGroup = getEndGroup();
                tToAgent = this.mLATreeSchema.getUpAgent();
            }
            else
            {
                tToAgent = "";
                tToGroup = "";
            }
        }

        if (!tToAgent.equals("") || !tToGroup.equals(""))
        {
            //查询被降级代理人的下属
            //tJunior的值置为被降级代理人的系列的下一个系列(被降级代理人职级为A09除外)
            char tJunior = (char) ((int) (tSeries.charAt(0)) - 1);
            if (tAsLevel.trim().equals("04"))
            {
                sql = "select * from latree,laagent "
                      + "where latree.agentcode = laagent.agentcode "
                      + "latree.upagent = '" + this.m_AgentCode.trim()
                      + "' and latree.agentgrade = 'A08'"
                      + " and laagent.agentstate <'03'";
            }
            else
            {
                sql = "SELECT * FROM LATree,LAAgent "
                      + "WHERE LATree.agentcode = laagent.agentcode "
                      + "and laagent.agentstate < '03' and latree.UpAgent = '"
                      + this.m_AgentCode.trim() +
                      "' AND latree.AgentSeries = '"
                      + tJunior + "'";
            }
            System.out.println("sql: " + sql);
            LATreeDB tTreeDB = new LATreeDB();
            tTreeSet = tTreeDB.executeQuery(sql);
            if (tTreeSet.size() > 0)
            {
                // 归属下属展业机构
                if (!prepareGrpOutputData(tTreeSet, tToAgent, tToGroup))
                {
                    return false;
                }
                if (!tDemote.dealGrpData(mOutputData, conn))
                {
                    this.mErrors.copyAllErrors(tDemote.mErrors);
                    dealError("delaGrpData", "归属被降级代理人的下属展业机构失败！");
                    return false;
                }
            }
            else
            {
                //没有下属
                LABranchGroupDB tDB = new LABranchGroupDB();
                tDB.setAgentGroup(tToGroup);
                if (!tDB.getInfo())
                {
                    return false;
                }

                CreateBranchAttr tCreateNo = CreateBranchAttr.getInstance();
                if (tAsLevel.trim().equals("02"))
                {
                    tCreateNo.init(tDB.getBranchAttr(), "Y", 18);
                }

                if (tAsLevel.trim().equals("03"))
                {
                    tCreateNo.init(tDB.getBranchAttr(), "Y", 15);
                }

                if (tAsLevel.trim().equals("04"))
                {
                    tCreateNo.init(tDB.getBranchAttr(), "Y", 12);
                }
            }

            // 归属被降级的代理人
            if (!preparePsnOutputData(tToAgent, tToGroup))
            {
                return false;
            }
            if (!tDemote.dealPsnData(mOutputData, conn))
            {
                this.mErrors.copyAllErrors(tDemote.mErrors);
                return false;
            }
        }
        else
        //若没有找到可归属的展业机构，则暂不归属，等待公司统一处理
        {
            //未找到可归属的展业机构
            if (!dealNotAscript())
            {
                return false;
            }
        }
        return true;
    }


    /** 没有找到可归属机构的做如下处理：
     *  LATreeB：备份旧表
     *  LATree:  AgentGroup 变为直辖机构的agentgroup，其它字段与正常情况相同
     *  LAAgent：AgentGroup 变为直辖机构的agentgroup
     *  LAAssess:AgentGroupNew = 直辖机构的AgentGroup
     *  LABranchGroupB： 不备份旧表
     *  LABranchGroup ： 将UpBranch置空，endFlag＝‘Y’，//caigang 2004-07-06修改，endflag='N',branchManager="",branchManagerName=""
     *                   endDate＝this.mondate[1],
     *                   branchAddressCode = this.mIndexCalNo
     */
    public boolean dealNotAscript()
    {
        DealRelaTab tDealTab = new DealRelaTab();

        if (!prepareOutputData())
        {
            return false;
        }

        if (!tDealTab.getInputData(this.mOutputData))
        {
            return false;
        }

        if (!tDealTab.insLAAgentB(this.mLATreeSchema.getAgentCode().trim(),
                                  conn))
        {
            return false;
        }

        if (!tDealTab.updateLAAgent(this.mLATreeSchema, "", conn))
        {
            return false;
        }

        if (!tDealTab.insertLATreeB(this.mLATreeSchema, conn))
        {
            return false;
        }

        if (!tDealTab.updateLATree(this.mLATreeSchema, this.mLAAssessSchema, "",
                                   "", conn))
        {
            return false;
        }

        //zsj_delete--2004-3-5:不用插入B表，因为正常降级不插入B表
//    if ( !tDealTab.insertLABranchGroupB(this.mLATreeSchema.getAgentGroup(),"Y",conn) )
//      return false;
        //caigang 2004-09-03 添加，将该代理人的下级机构主管的UpAgent置为空
        ExeSQL tExeSQL = new ExeSQL();
        String sql = "select branchmanager from labranchgroup where upbranch='" +
                     this.mLATreeSchema.getAgentGroup() + "'"
                     + " and branchtype='1' and upbranchattr <>'1' and (endflag is null or endflag<>'Y')";
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String tBranchManager = tSSRS.GetText(i, 1);
            if (tBranchManager == null || tBranchManager.equals(""))
            {
                continue;
            }
            LATreeDB tDB = new LATreeDB();
            tDB.setAgentCode(tBranchManager);
            if (!tDB.getInfo())
            {
                dealError("dealNotAscript", "没有查找到" + tBranchManager + "的行政信息!");
                return false;
            }

            LATreeSchema tLATreeSchema = tDB.getSchema();
            if (tLATreeSchema.getState() != null &&
                !tLATreeSchema.getState().equals("3"))
            {
                LATreeBDB tLATreeBDB = new LATreeBDB(conn);
                LATreeDB tLATreeDB = new LATreeDB(conn);
                LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                Reflections tf = new Reflections();
                tf.transFields(tLATreeBSchema, tLATreeSchema);

                tLATreeBSchema.setOperator(mGlobalInput.Operator);
                tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
                tLATreeBSchema.setRemoveType("01");

                tLATreeBSchema.setModifyDate(PubFun.getCurrentDate());
                tLATreeBSchema.setModifyTime(PubFun.getCurrentTime());
                tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
                tLATreeBSchema.setMakeDate(PubFun.getCurrentDate());
                tLATreeBSchema.setMakeTime(PubFun.getCurrentTime());
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
                tLATreeBSchema.setIndexCalNo(this.mIndexCalNo);
                tLATreeBSchema.setEdorNO(OAgentAscript.getEdorNo());
                tLATreeBDB.setSchema(tLATreeBSchema);
                if (!tLATreeBDB.insert())
                {
                    dealError("dealNotAscript",
                              "备份被归属人员的下级机构主管" + tBranchManager + "的行政信息失败!");
                    return false;
                }
                tLATreeSchema.setUpAgent("");
                tLATreeDB.setSchema(tLATreeSchema);
                if (!tLATreeDB.update())
                {
                    dealError("dealNotAscript",
                              "更新被归属人员的下级机构主管" + tBranchManager + "的上级代理人信息失败!");
                    return false;
                }
            }

        }
        LABranchGroupDB tDB = new LABranchGroupDB();
        tDB.setAgentGroup(this.mLATreeSchema.getAgentGroup().trim());
        if (!tDB.getInfo())
        {
            return false;
        }
        if (!tDealTab.updateLABranchGroup(this.mLATreeSchema.getAgentGroup(),
                                          "", "", "N", conn))
        {
            return false;
        }

        if (!tDealTab.updateLAAssess(this.mLAAssessSchema, this.mLATreeSchema,
                                     "", conn))
        {
            return false;
        }

        //更新业绩
        String tDirGroup = getDirAgency(this.mLATreeSchema);
        ExeSQL tExe = new ExeSQL(conn);
        sql = "update lacommision set agentgroup = '" + tDirGroup.trim()
              + "' where agentcode = '" +
              this.mLATreeSchema.getAgentCode().trim()
              + "' and (caldate >='" + this.MonDate[0] +
              "' or caldate is null)";
        if (!tExe.execUpdateSQL(sql))
        {
            dealError("dealNotAscript", tExe.mErrors.getFirstError());
            return false;
        }

        return true;
    }

    private String getDirAgency(LATreeSchema tLATreeSchema)
    {
        LABranchGroupDB tDB = new LABranchGroupDB();
        LABranchGroupSet tSet = new LABranchGroupSet();
        LABranchGroupSchema tSch = new LABranchGroupSchema();
        String tLevel = "";

        if (tLATreeSchema.getAgentSeries().equals("C"))
        {
            tLevel = "01";
        }
        if (tLATreeSchema.getAgentGrade().equals("A08"))
        {
            tLevel = "02";
        }
        if (tLATreeSchema.getAgentGrade().equals("A09"))
        {
            tLevel = "03";
        }

        String sql = "SELECT * FROM LABranchGroup WHERE BranchManager = '" +
                     tLATreeSchema.getAgentCode().trim() +
                     "' AND BranchLevel = '" +
                     tLevel.trim() + "'";

        tSet = tDB.executeQuery(sql);
        if (tSet.size() <= 0)
        {
            return "";
        }

        tSch = tSet.get(1);
        return tSch.getAgentGroup();
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mOutputData.add(this.mGlobalInput);
            this.mOutputData.add(this.mIndexCalNo);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("prepareOutputData", "准备输出数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 根据被降级的代理人的系列，查询出该代理人的所辖机构的上级机构，做为查询当前机构应归属的
     * 机构的范围。这个范围是这个上级机构的AgentGroup，
     * @param tSeries
     * @return
     */
    private String getGroupLimit(String tSeries)
    {
        LABranchGroupDB tDB = new LABranchGroupDB();
        LABranchGroupSchema tSch = new LABranchGroupSchema();
        LABranchGroupSet tSet = new LABranchGroupSet();
        String tGroupLimit = "";
        String tAttrLimit = "";

        tDB.setAgentGroup(this.m_AgentGroup);
        if (!tDB.getInfo())
        {
            return "";
        }
        tAttrLimit = tDB.getBranchAttr(); //获取当前机构的BranchAttr

        if (tSeries.equals("B"))
        {
            //以本部为限
            tGroupLimit = tDB.getUpBranch();
        }
        else
        {
            if (tSeries.trim().equalsIgnoreCase("D"))
            {
                //以本区域督导部为限
                tAttrLimit = tAttrLimit.trim().substring(0, 10);
            }
            else
            {
                //以本督导部为限
                tAttrLimit = tAttrLimit.trim().substring(0, 12);
            }
            tSch.setBranchAttr(tAttrLimit);
            tDB.setSchema(tSch);
            tSet = tDB.query();
            if (tSet.size() <= 0)
            {
                return "";
            }
            tSch = tSet.get(1);
            tGroupLimit = tSch.getAgentGroup();
        }
        //返回的是范围内的内部编码
        return tGroupLimit;
    }


    private String getEndGroup()
    {
        String sql = "";
        String tEndGroup = "";
        LABranchGroupDB tDB = new LABranchGroupDB();
        LABranchGroupSet tSet = new LABranchGroupSet();
        LABranchGroupSchema tLABranchSh = new LABranchGroupSchema();

        sql = "SELECT * FROM LABranchGroup WHERE BranchLevel = '01'" +
              " AND BranchManager = '" + this.mLATreeSchema.getUpAgent() +
              "' and (endflag is null or endflag<>'Y')";
        tSet = tDB.executeQuery(sql);
        if (tSet.size() <= 0)
        {
            dealError("getEndGroup", "查询直辖机构编码失败！");
            return "";
        }
        else
        {
            tLABranchSh = tSet.get(1);
            tEndGroup = tLABranchSh.getAgentGroup();
        }
        return tEndGroup;
    }

    /**
     * 一个递归函数，用于查询被降级代理人降级后，他应该被归属的上级代理人的LATreeSchema信息
     * @param tGroupLimit  查询的范围，见getGroupLimit
     * @param tEduManager  被降级的代理人的育成人，如果这个育成人还有机构的话，则结果就是这个育成人
     * @param tAsLevel     查询的机构的级别，要和被降级的代理人的营业机构级别一致
     * @param tEduGrade    该育成人育成被降级的代理人的级别
     * @return
     */
    private LATreeSchema findEduGrp(String tGroupLimit, String tOrdinayAgent,
                                    String tEduManager, String tAsLevel,
                                    String tEduGrade)
    {
        LATreeDB tTreeDB = new LATreeDB();
        LATreeSchema tTreeSh = new LATreeSchema();
        LATreeSchema tResultSch = new LATreeSchema();
        LABranchGroupDB tBranchDB = new LABranchGroupDB();
        LABranchGroupSet tBranchSet = new LABranchGroupSet();
        LABranchGroupSchema tBranchSh = new LABranchGroupSchema();
        tResultSch = null; //tResultSch置为空null，当查询当中出错的时候，返回tResultSch，也即返回null
        String tEduGrp = "";
        String tAttrLimit = "";

        if (tEduManager.trim().equals(""))
        {
            return tResultSch;
        }

        String tOrEduGrade = "";
        if (tEduGrade.trim().equals("A04"))
        {
            tOrEduGrade = "A05";
        }
        if (tEduGrade.trim().equals("A05"))
        {
            tOrEduGrade = "A04";
        }
        if (tEduGrade.trim().equals("A06"))
        {
            tOrEduGrade = "A07";
        }
        if (tEduGrade.trim().equals("A07"))
        {
            tOrEduGrade = "A06";
        }

        //如果tAsLevel是'04'，表示是个区域督导，则范围就是整个ManageCom
        //当tAsLevel等于'04'的时候，传入的tGroupLimit恰好是ManageCom
        //否则，则通过传入的AgentGroup查出BranchAttr
        if (tAsLevel.trim().equals("04"))
        {
            tAttrLimit = tGroupLimit;
        }
        else
        {
            tBranchDB.setAgentGroup(tGroupLimit);
            if (!tBranchDB.getInfo())
            {
                return tResultSch;
            }
            //取得归属范围的BranchAttr
            tAttrLimit = tBranchDB.getBranchAttr();
        }
        int tLengthLimit = tAttrLimit.trim().length();

        tTreeDB.setAgentCode(tEduManager.trim());
        if (!tTreeDB.getInfo())
        {
            return tResultSch;
        }

        tTreeSh.setSchema(tTreeDB.getSchema());

        //caigang 2004-07-06添加，用于判断育成人和本人的RearFlag是否断裂
        LATreeAccessoryDB tLATreeAccessoryDB = new LATreeAccessoryDB();
        LATreeAccessorySet tLATreeAccessorySet = new LATreeAccessorySet();
        String sql_LATreeAcc =
                "select * from latreeaccessory where agentcode='" +
                tOrdinayAgent + "' and (agentgrade = '" + tEduGrade.trim()
                + "' or agentgrade = '" + tOrEduGrade.trim()
                + "')";
        System.out.println("sql_LATreeAcc:" + sql_LATreeAcc);
        tLATreeAccessorySet = tLATreeAccessoryDB.executeQuery(sql_LATreeAcc);
        if (tLATreeAccessorySet.size() == 0)
        {
            dealError("findEduGrp",
                      "没有" + tOrdinayAgent + "的" + tEduGrade + "育成关系");
            return tResultSch;
        }
        LATreeAccessorySchema tLATreeAccesssorySchema = new
                LATreeAccessorySchema();
        tLATreeAccesssorySchema = tLATreeAccessorySet.get(1);

        //2004-09-13 蔡刚添加，在查询育成机构的时候不判断RearFlag的值
//    if (tLATreeAccesssorySchema.getRearFlag()==null||!tLATreeAccesssorySchema.getRearFlag().equals("1") )
        {
            //查询育成人是否离职
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tEduManager);
            if (!tLAAgentDB.getInfo())
            {
                dealError("findEduGrp", "查询育成人" + tEduManager + "的原始信息失败！");
                return tResultSch;
            }
            if (tLAAgentDB.getAgentState() != null &&
                tLAAgentDB.getAgentState().compareTo("03") < 0)
            {
                //查询该育成人所在的展业机构是否存在
                String sql_Branch =
                        "SELECT * FROM LABranchGroup WHERE branchmanager = '"
                        + tTreeSh.getAgentCode().trim()
                        + "' and branchlevel = '"
                        + tAsLevel.trim() + "' and endflag <> 'Y'";

                tBranchSet = tBranchDB.executeQuery(sql_Branch);
                System.out.println(sql_Branch);
            }
        }

        if (tBranchSet.size() <= 0)
        {
            //若不存在，则返回查询该育成代理人的上级育成代理人
            String sql_Acc =
                    "select * from latreeaccessory where agentcode = '"
                    + tTreeSh.getAgentCode().trim()
                    + "' and (agentgrade = '"
                    + tEduGrade.trim()
                    + "' or agentgrade = '"
                    + tOrEduGrade.trim() + "')  ";
            System.out.println("---sql_Acc = " + sql_Acc);
            LATreeAccessoryDB tAccDB = new LATreeAccessoryDB();
            LATreeAccessorySet tAccSet = new LATreeAccessorySet();
            LATreeAccessorySchema tAccSch = new LATreeAccessorySchema();
            tAccSet = tAccDB.executeQuery(sql_Acc);
            String tRearCode = "";
            int tAccSetSize = tAccSet.size();
            if (tAccSetSize == 0)
            {
//        ExeSQL tExe = new ExeSQL();
//        String tQryEdorNo = "select max(edorno) from latreeaccessoryb "
//                          + "where (agentgrade = '" + tEduGrade.trim()
//                          + "' or agentgrade = '" + tOrEduGrade.trim()
//                          + "')" + " and agentcode = '"
//                          + tEduManager.trim() + "'";
//        System.out.println("tQryEdorNo = " + tQryEdorNo);
//        String tMaxNo = tExe.getOneValue(tQryEdorNo);
//        if ( tMaxNo == null || tMaxNo .equals(""))  {
//          dealError("findEduGrp","取育成人查询LATreeAccessoryB表失败！" + tExe.mErrors.getFirstError());
//          return tResultSch;
//        }
//        String tQryEdu = "select rearagentcode from latreeaccessoryb "
//                       + "where agentcode = '" + tEduManager.trim()
//                       + "' and (agentgrade = '" + tEduGrade.trim()
//                       + "' or agentgrade = '" + tOrEduGrade.trim()
//                       + "') and edorno = '" + tMaxNo.trim() + "'";
//        tRearCode = tExe.getOneValue(tQryEdu);
//
                tRearCode = "";
            }
            else
            {
                tAccSch = tAccSet.get(1);
//        tRearCode = tAccSch.getAgentCode();
                tRearCode = tAccSch.getRearAgentCode();
            }
            if (tRearCode == null || tRearCode.trim().equals(""))
            {
                return tResultSch;
            }
            tResultSch = findEduGrp(tGroupLimit, tEduManager, tRearCode.trim(),
                                    tAsLevel, tEduGrade);
        }
        else
        { //存在
            tBranchSh = tBranchSet.get(1);
            //检查该展业机构是否在归属限制的区域内
            tEduGrp = tBranchSh.getBranchAttr().substring(0, tLengthLimit);
            if (!tEduGrp.equals(tAttrLimit))
            {
                return tResultSch;
            }
            else
            {
                tTreeSh.setAgentGroup(tBranchSh.getAgentGroup());
                return tTreeSh;
            }
        }
        return tResultSch;
    }

    private boolean getInputData()
    {
        this.mGlobalInput.setSchema((GlobalInput)this.mInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLAAssessSchema = (LAAssessSchema)this.mInputData.
                               getObjectByObjectName("LAAssessSchema", 0);
        this.mIndexCalNo = (String)this.mInputData.getObjectByObjectName(
                "String", 0);

        if (this.mGlobalInput == null || this.m_AgentCode == null)
        {
            // @@错误处理
            dealError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
//    String tSeries = this.mLATreeSchema.getAscriptSeries();
//    String tEdu = this.mLATreeSchema.getEduManager();
//
//    if ( (tSeries != null || tSeries != "") && (tEdu == null || tEdu .equals(""))) {
//      dealError("dealData", "数据校验没有通过！");
//      return false;
//    }
//    if ( (tSeries == null || tSeries .equals("")) && (tEdu != null || tEdu != "")) {
//      dealError("dealData", "数据校验没有通过！");
//      return false;
//    }

        return true;
    }


    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "DoAscriptDemote";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }

    private boolean preparePsnOutputData(String tToAgent, String tToGroup)
    {
        try
        {
            this.mOutputData.clear();
            this.mOutputData.add(this.mGlobalInput);
            this.mOutputData.add(this.mLATreeSchema);
            this.mOutputData.add(this.mLAAssessSchema);
            this.mOutputData.add(0, tToAgent);
            this.mOutputData.add(1, tToGroup);
            this.mOutputData.add(2, this.mIndexCalNo);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("preparePsnOutputData", "在准备往后层处理所需要的数据时出错!");
            return false;
        }
        return true;
    }

    private boolean prepareGrpOutputData(LATreeSet tLATreeSet, String tToAgent,
                                         String tToGroup)
    {
        try
        {
            this.mOutputData.clear();
            this.mOutputData.add(this.mGlobalInput);
            this.mOutputData.add(tLATreeSet);
            this.mOutputData.add(this.mLATreeSchema);
            this.mOutputData.add(0, tToAgent);
            this.mOutputData.add(1, tToGroup);
            this.mOutputData.add(2, this.mIndexCalNo);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("prepareGrpOutputData", "在准备往后层处理所需要的数据时出错!");
            return false;
        }
        return true;
    }
}
