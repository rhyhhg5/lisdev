package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.agent.LAInteractionDimissionBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.db.LAAssessMainDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAssessHistorySchema;
import com.sinosoft.lis.schema.LAAssessMainSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAAssessMainSet;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: Sinosoft</p>
 * @author
 * @version 1.0
 * @author
 * @modified by Howie
 */

public class AssessActiveConfirmBL
{
    //错误处理类
    public static CErrors mErrors = new CErrors();
    //业务处理相关变量
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 提交数据的容器 */
    private MMap mMap = new MMap();
    /** 全局数据 */
    public GlobalInput mGlobalInput = new GlobalInput();
    private String mIndexCalNo = "";
    private String mManageCom = "";
    private String mBranchType = "";     //展业类型
    private String mBranchType2 = "";     //子渠道
    private LAAssessSet mLAAssessSet1 = new LAAssessSet(); //维持晋升人员
    private LAAssessSet mLAAssessSet2 = new LAAssessSet(); //清退人员,降为非转正
    private LAAssessSet mLAAssessSet3 = new LAAssessSet(); //清退人员
    private LAAssessSet mLAAssessSet4 = new LAAssessSet(); //非清退人员，转正晋升
    private String mEvaluateDate = ""; // 异动时间
    private LAAssessSet mLAAssessSet = new LAAssessSet();
    private LAAssessMainSet mLAAssessMainSet = new LAAssessMainSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    public AssessActiveConfirmBL()
    {
    }

    public static void main(String[] args)
    {
        AssessActiveConfirmBL assessConfirmSave1 = new AssessActiveConfirmBL();
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "001";

        VData tVData = new VData();
        tVData.clear();
        tVData.add(tGI);
        tVData.add("8611"); //机构
        tVData.add("200703"); //考核年月
        tVData.add("1"); //展业类型
        tVData.add("01"); //子渠道
        tVData.add("0"); //职级系列

        try
        {
            if (!assessConfirmSave1.submitData(tVData))
            {
                System.out.println("failed!" + mErrors.getFirstError());
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
       //取得异动时间
        if (!getEvaluateDate()) {
            return false;
        }
        if (!dealData())
        {
            return false;
        } 
        //进行维持和晋升处理
        if (!dealInDue1()) {
            return false;
        }
        //进行非转正处理
//        if (!dealInDue2()) {
//            return false;
//        }
        //进行离职处理
        if (!dealInDue3()) {
            return false;
        }
        //进行转正处理
//        if (!dealInDue4()) {
//            return false;
//        }
      //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        //统一提交
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        if (tPubSubmit.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecGrpYearBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获取前台传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
    	mErrors.clearErrors();
        mInputData = (VData) cInputData.clone();
        try
        {
            this.mGlobalInput.setSchema((GlobalInput) mInputData.
                                        getObjectByObjectName("GlobalInput", 0));
            this.mManageCom = (String) mInputData.getObject(1);
            this.mIndexCalNo = (String) mInputData.getObject(2);
            this.mBranchType = (String) mInputData.getObject(3);
            this.mBranchType2 = (String) mInputData.getObject(4);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }

        if ((this.mIndexCalNo == null || this.mIndexCalNo == "")
            || (this.mManageCom == null || this.mManageCom == "")
            || (this.mBranchType == null || this.mBranchType == "")
            || (this.mBranchType2 == null || this.mBranchType2 == "")
//            || (this.mAgentSeries == null || this.mAgentSeries == "")
            )
        {
            dealError("getInputdata", "没有传入足够数据！");
            return false;
        }
        if (mManageCom.indexOf(mGlobalInput.ManageCom) != 0)
        { //操作机构不是被归属机构或其上级机构
            dealError("getInputData", "无权操作此机构的审核确认！");
            return false;
        }
        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean dealData()
    {
        System.out.println("----come into dealData---");
        Connection conn = DBConnPool.getConnection();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAssessSet tLAAssessSet = new LAAssessSet();
        LAAssessDB tLAAssessDB = new LAAssessDB();
        LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
        LAAssessMainSchema tLAAssessMainSchema = new LAAssessMainSchema();
        LAAssessMainSet tLAAssessMainSet = new LAAssessMainSet();
        String strSQL = "select * from laassessmain where  assesstype='00' " +
				        "and indexcalno='" +mIndexCalNo + "' " +
				        "and managecom like '" + mManageCom +"%' " +
						"and branchtype ='"+mBranchType+"'" +
						"and branchtype2 ='"+mBranchType2+"'" +
                        " and state='0' ";
        System.out.println(strSQL);
        tLAAssessMainSet = tLAAssessMainDB.executeQuery(strSQL);
        if (tLAAssessMainSet.size() <= 0)
        {
            //再查一下看看是不是已经确认了
            strSQL =
                    "select * from laassessmain where assesstype='00' " +
                    "and indexcalno='" +mIndexCalNo + "' " +
                    "and managecom like '" + mManageCom +"%' " +
            		"and branchtype ='"+mBranchType+"'" +
            		"and branchtype2 ='"+mBranchType2+"'" +
                    "and state>'0' ";

            tLAAssessMainSet = tLAAssessMainDB.executeQuery(strSQL);
            if (tLAAssessMainSet.size() <= 0)
            {
                dealError("dealData",
                          "查询机构" + mManageCom + "下的考评主信息失败!");
                return false;
            }
            else
            {
                dealError("dealData",
                          "机构" + mManageCom + "下的考评信息已确认过!");
                return false;
            }
        }
        tLAAssessMainDB = new LAAssessMainDB(conn);
        int tCount = 0;
        for (int i = 1; i <= tLAAssessMainSet.size(); i++)
        {
//            if (tLAAssessMainSet.get(i).getAssessCount() <= 0)
//            { //这个机构没有人参加考核，不查考核结果记录
//                continue;
//            }
            LAAssessSet tmpLAAssessSet = new LAAssessSet();
            String tSQL =
                    "Select * from laassess where managecom =  '"
                    + tLAAssessMainSet.get(i).getManageCom() + "'"
                    + " and IndexCalNo = '"
                    + this.mIndexCalNo.trim() + "'"
                    + " and BranchType='" + mBranchType + "'"
                    + " and BranchType2='" + mBranchType2 + "'"
                    + " and state = '0' "
                    + " and agentgrade = '"+tLAAssessMainSet.get(i).getAgentGrade()+"' "
                    ;
//            if (this.mAgentSeries.compareTo("0") == 0)
//            {
//                tSQL += " and agentgrade like 'A%' ";
//            }
//            else if (this.mAgentSeries.compareTo("0") > 0)
//            {
//                tSQL += " and agentgrade like 'B%' ";
//            }
            System.out.println("---query LAAssess---Sql : " + tSQL);
            tmpLAAssessSet = tLAAssessDB.executeQuery(tSQL);
            if (tmpLAAssessSet.size() > 0)
            {
                tLAAssessSet.add(tmpLAAssessSet);
            }
            int tLastCount = tCount;
            tCount = tLAAssessSet.size();
            if (tCount < tLastCount)
            {
                dealError("DealData", "未找到考核结果信息！");
                return false;
            }
        }
        System.out.println("total tLAAssessSet.size:" + tCount);
        try
        {
            conn.setAutoCommit(false);
            for (int i = 1; i <= tCount; i++)
            {
                tLAAssessSchema = tLAAssessSet.get(i);
                if (!dealAgentEvaluate(tLAAssessSchema)) {
                    // @@错误处理
                    CError.buildErr(this, "进行人员归属操作时失败！");
                    return false;
                }
                if (!updLAAssess(tLAAssessSchema, conn))
                {
//                    conn.rollback();
//                    conn.close();
                    return false;
                }
            }
            //更新考核信息主表（日志）
            for (int i = 1; i <= tLAAssessMainSet.size(); i++)
            {
                tLAAssessMainSchema = new LAAssessMainSchema();
                tLAAssessMainSchema = tLAAssessMainSet.get(i);
                if (tLAAssessMainSchema.getAssessCount()>=0)
                {
                    tLAAssessMainSchema.setConfirmCount(tLAAssessMainSchema.getAssessCount());
                }
                tLAAssessMainSchema.setState("1"); //审核确认状态标记
                tLAAssessMainSchema.setOperator(mGlobalInput.Operator);
                tLAAssessMainSchema.setModifyDate(PubFun.getCurrentDate());
                tLAAssessMainSchema.setModifyTime(PubFun.getCurrentTime());
//                tLAAssessMainDB.setSchema(tLAAssessMainSchema);
//
//                if (!tLAAssessMainDB.update())
//                {
//                    conn.rollback();
//                    conn.close();
//                    dealError("dealData", "更新考评主信息失败！");
//                    return false;
//                }
                this.mLAAssessMainSet.add(tLAAssessMainSchema);
            }
//            conn.commit();
//            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
//            try
//            {
//                conn.rollback();
//                conn.close();
//            }
//            catch (Exception e)
//            {}
            dealError("dealData", ex.toString());
            return false;
        }
        return true;
    }
    /**
     * 更新考核结果
     * @param tLAAssessSchema LAAssessSchema
     * @param conn Connection
     * @return boolean
     */
    private boolean updLAAssess(LAAssessSchema tLAAssessSchema, Connection conn)
    {
        System.out.println("---come into updLAAssess---");
        LAAssessDB tLAAssessDB = new LAAssessDB(conn);
        LAAssessSchema tOldSch = new LAAssessSchema();
        tLAAssessDB.setAgentCode(tLAAssessSchema.getAgentCode());
        tLAAssessDB.setIndexCalNo(tLAAssessSchema.getIndexCalNo());
        if (!tLAAssessDB.getInfo())
        {
            return false;
        }
        tOldSch.setSchema(tLAAssessDB.getSchema());
        tOldSch.setConfirmer(this.mGlobalInput.Operator);
        tOldSch.setConfirmDate(PubFun.getCurrentDate());
        tOldSch.setState("1");   //审核确认状态标记
        tOldSch.setOperator(this.mGlobalInput.Operator);
        tOldSch.setModifyDate(PubFun.getCurrentDate());
        tOldSch.setModifyTime(PubFun.getCurrentTime());
//        tLAAssessDB.setSchema(tOldSch);
//        if (!tLAAssessDB.update())
//        {
//            CError.buildErr(this,"代理人考评信息审核确认失败！");
//            return false;
//        }
        this.mLAAssessSet.add(tOldSch);
        return true;
    }
    /**
     * 进行晋升和维持处理
     * @return boolean
     */
    private boolean dealInDue1() {
    	if (mLAAssessSet1.size() < 1) {
    		return true;
    	}
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeBSet tLATreeBSet = new LATreeBSet();
        LATreeDB tLATreeDB = new LATreeDB();

        Reflections tRefs = new Reflections();
        // 进行转正处理加入转正日期和标记
        for (int i = 1; i <= mLAAssessSet1.size(); i++) {
        	tLAAssessSchema = new LAAssessSchema();
            tLAAssessSchema = mLAAssessSet1.get(i).getSchema();

            //处理行政信息
            tLATreeDB = new LATreeDB();
            LATreeSchema tLATreeSchemaOld = new LATreeSchema();
            LATreeSchema tLATreeSchema = new LATreeSchema();
            tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
            tLATreeDB.getInfo();
            tLATreeSchemaOld = tLATreeDB.getSchema();
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            tRefs = new Reflections();
            tRefs.transFields(tLATreeBSchema, tLATreeSchemaOld);
            tRefs.transFields(tLATreeSchema, tLATreeSchemaOld);
            tLATreeBSchema.setOperator2(tLATreeSchemaOld.getOperator());
            tLATreeBSchema.setOperator(mGlobalInput.Operator);
            tLATreeBSchema.setEdorNO(tEdorNo);
            tLATreeBSchema.setRemoveType("01"); //手工考核确认
            tLATreeBSchema.setMakeDate2(tLATreeSchemaOld.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeSchemaOld.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeSchemaOld.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeSchemaOld.getModifyTime());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
           // tLATreeBSchema.setAgentGrade(tLAAssessSchema.getCalAgentGrade());
            tLATreeBSet.add(tLATreeBSchema);
            tLATreeSchema.setAgentLastSeries(tLATreeSchemaOld.getAgentSeries());
            tLATreeSchema.setAgentLastGrade(tLATreeSchemaOld.getAgentGrade());
            tLATreeSchema.setOldStartDate(tLATreeSchemaOld.getStartDate());
            tLATreeSchema.setAgentSeries(tLAAssessSchema.getAgentSeries1());
            tLATreeSchema.setAgentGrade(tLAAssessSchema.getAgentGrade1());
            tLATreeSchema.setInDueFormFlag("0"); //非转正标记
            tLATreeSchema.setStartDate(mEvaluateDate);
            tLATreeSchema.setAstartDate(mEvaluateDate);
            tLATreeSchema.setModifyDate(this.currentDate);
            tLATreeSchema.setModifyTime(this.currentTime);
            tLATreeSchema.setState("0");
            tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
            tLATreeSet.add(tLATreeSchema);
//            
//        	tLAAssessSchema = new LAAssessSchema();
//            tLAAssessSchema = mLAAssessSet1.get(i).getSchema();
//
//            //处理行政信息
//            tLATreeDB = new LATreeDB();
//            tLATreeSchema = new LATreeSchema();
//            tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
//            tLATreeDB.getInfo();
//            tLATreeSchema = tLATreeDB.getSchema();
//            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
//            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
//            tRefs = new Reflections();
//            tRefs.transFields(tLATreeBSchema, tLATreeSchema);
//            tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
//            tLATreeBSchema.setOperator(mGlobalInput.Operator);
//            tLATreeBSchema.setEdorNO(tEdorNo);
//            tLATreeBSchema.setRemoveType("01"); //手工考核确认
//            tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
//            tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
//            tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
//            tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
//            tLATreeBSchema.setMakeDate(currentDate);
//            tLATreeBSchema.setMakeTime(currentTime);
//            tLATreeBSchema.setModifyDate(currentDate);
//            tLATreeBSchema.setModifyTime(currentTime);
//           // tLATreeBSchema.setAgentGrade(tLAAssessSchema.getCalAgentGrade());
//            tLATreeBSet.add(tLATreeBSchema);
//            tLATreeSchema.setAgentSeries(tLAAssessSchema.getAgentSeries1());
//            tLATreeSchema.setAgentGrade(tLAAssessSchema.getAgentGrade1());
//            tLATreeSchema.setStartDate(mEvaluateDate);
//            tLATreeSchema.setAstartDate(mEvaluateDate);
//            tLATreeSchema.setModifyDate(this.currentDate);
//            tLATreeSchema.setModifyTime(this.currentTime);
//            tLATreeSchema.setState("0");
//            tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
//            tLATreeSet.add(tLATreeSchema);
        }
        /** @todo  */
        mLATreeSet.add(tLATreeSet);
        mLATreeBSet.add(tLATreeBSet);
        return true;
    }
    /**
     * 进行转正处理
     * @return boolean
     */
    private boolean dealInDue2() {
    	if (mLAAssessSet2.size() < 1) {
    		return true;
    	}
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeBSet tLATreeBSet = new LATreeBSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeDB tLATreeDB = new LATreeDB();

        Reflections tRefs = new Reflections();
        // 进行转正处理加入转正日期和标记
        for (int i = 1; i <= mLAAssessSet2.size(); i++) {
            tLAAssessSchema = new LAAssessSchema();
            tLAAssessSchema = mLAAssessSet2.get(i).getSchema();

            //处理行政信息
            tLATreeDB = new LATreeDB();
            tLATreeSchema = new LATreeSchema();
            tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
            tLATreeDB.getInfo();
//            tLATreeSchema = tLATreeDB.getSchema();
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            tRefs = new Reflections();
            tRefs.transFields(tLATreeBSchema, tLATreeDB);
            tLATreeBSchema.setOperator2(tLATreeDB.getOperator());
            tLATreeBSchema.setOperator(mGlobalInput.Operator);
            tLATreeBSchema.setEdorNO(tEdorNo);
            tLATreeBSchema.setRemoveType("01"); //手工考核确认
            tLATreeBSchema.setMakeDate2(tLATreeDB.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeDB.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeDB.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeDB.getModifyTime());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
           // tLATreeBSchema.setAgentGrade(tLAAssessSchema.getCalAgentGrade());
            tLATreeBSet.add(tLATreeBSchema);
            tLATreeSchema.setAgentLastSeries(tLATreeDB.getAgentSeries());
            tLATreeSchema.setAgentLastGrade(tLATreeDB.getAgentGrade());
            tLATreeSchema.setOldStartDate(tLATreeDB.getStartDate());
            tLATreeSchema.setAgentSeries(tLAAssessSchema.getAgentSeries1());
            tLATreeSchema.setAgentGrade(tLAAssessSchema.getAgentGrade1());
            tLATreeSchema.setInDueFormFlag("0"); //非转正标记
            tLATreeSchema.setStartDate(mEvaluateDate);
            tLATreeSchema.setAstartDate(mEvaluateDate);
            tLATreeSchema.setModifyDate(this.currentDate);
            tLATreeSchema.setModifyTime(this.currentTime);
            tLATreeSchema.setState("0");
            tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
            tLATreeSet.add(tLATreeSchema);
        }
        /** @todo  */
        mLATreeSet.add(tLATreeSet);
        mLATreeBSet.add(tLATreeBSet);
        return true;
    }
    /**
     * 进行转正处理
     * @return boolean
     */
    private boolean dealInDue4() {

        if (mLAAssessSet4.size() < 1) {
            return true;
        }
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeBSet tLATreeBSet = new LATreeBSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeDB tLATreeDB = new LATreeDB();
        Reflections tRefs = new Reflections();
        // 进行转正处理加入转正日期和标记
        for (int i = 1; i <= mLAAssessSet4.size(); i++) {
            tLAAssessSchema = new LAAssessSchema();
            tLAAssessSchema = mLAAssessSet4.get(i).getSchema();

            //处理行政信息
            tLATreeDB = new LATreeDB();
            tLATreeSchema = new LATreeSchema();
            tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
            tLATreeDB.getInfo();
            tLATreeSchema = tLATreeDB.getSchema();
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            tRefs = new Reflections();
            tRefs.transFields(tLATreeBSchema, tLATreeSchema);
            tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
            tLATreeBSchema.setOperator(mGlobalInput.Operator);
            tLATreeBSchema.setEdorNO(tEdorNo);
            tLATreeBSchema.setRemoveType("01"); //手工考核确认
            tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
           // tLATreeBSchema.setAgentGrade(tLAAssessSchema.getCalAgentGrade());
            tLATreeBSet.add(tLATreeBSchema);
            tLATreeSchema.setAgentSeries(tLAAssessSchema.getAgentSeries1());
            tLATreeSchema.setAgentGrade(tLAAssessSchema.getAgentGrade1());
            tLATreeSchema.setInDueFormFlag("1"); //转正标记
            tLATreeSchema.setStartDate(mEvaluateDate);
            tLATreeSchema.setAstartDate(mEvaluateDate);
            tLATreeSchema.setModifyDate(this.currentDate);
            tLATreeSchema.setModifyTime(this.currentTime);
            tLATreeSchema.setState("0");
            tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
            tLATreeSet.add(tLATreeSchema);
        }
        /** @todo  */
        mLATreeSet.add(tLATreeSet);
        mLATreeBSet.add(tLATreeBSet);
        return true;
    }
    /**
     * 取得异动时间(考核期次月一号)
     * @return boolean
     */
    private boolean getEvaluateDate() {
        String tSQL = "";

        tSQL = "select date(substr('" + mIndexCalNo + "',1,4)||'-'|| substr(";
        tSQL += "'" + mIndexCalNo + "',5,2)||'-01') +1 month  from dual";

        ExeSQL tExeSQL = new ExeSQL();
        // 取得异动时间
        mEvaluateDate = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecGrpYearBL";
            tError.functionName = "getEvaluateDate";
            tError.errorMessage = "获取异动日期失败！";
            System.out.println("获取异动日期失败！");
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("获得异动日期：" + mEvaluateDate);
        return true;
    }
    /**
     * 对每个参加考核的人员进行逐个处理
     * @param pmLAAssessSchema LAAssessSchema
     * @return boolean
     */
    private boolean dealAgentEvaluate(LAAssessSchema pmLAAssessSchema) {
        String tCalAgentGrade = pmLAAssessSchema.getCalAgentGrade(); //新职级
        String tAssessFlag = pmLAAssessSchema.getAssessFlag();
        System.out.println(pmLAAssessSchema.getAgentCode());
        //判断业务员升降级分别进行处理 01:降级 02：维持 03：晋级
        // if ("01".equals(tModifyFlag) || "03".equals(tModifyFlag)) {
        // 处理清退职级业务员
        if ("U93".equals(tAssessFlag)&&"U00".equals(tCalAgentGrade)) {
            //对清退业务员进行离职处理
            mLAAssessSet3.add(pmLAAssessSchema);
        }
//        else if("U94".equals(tAssessFlag)||"U95".equals(tAssessFlag))
//        {
//        	mLAAssessSet4.add(pmLAAssessSchema);
//        }
//        else if("U92".equals(tAssessFlag))
//        {
//        	mLAAssessSet2.add(pmLAAssessSchema);
//        }
        else
        {
        	mLAAssessSet1.add(pmLAAssessSchema);
        }
        return true;
    }
    /**
     * 分类处理清退人员和职级发生变化的人员
     * @return boolean
     */
    private boolean dealInDue3() {
        //处理离职人员信息变量
        LADimissionSchema tLADimissionSchema = new LADimissionSchema();
        VData tVData = new VData();

        //处理清退人员
        if (mLAAssessSet3.size() > 0) {
            for (int i = 1; i <= mLAAssessSet3.size(); i++) {
               //在进行考核清退插入时,首先校验是否已经有离职记录或者离职确认
                LADimissionDB tLADimissionDB=new LADimissionDB();
                LADimissionSet tLADimissionSet=new LADimissionSet();
                tLADimissionDB.setAgentCode(mLAAssessSet3.get(i).
                        getAgentCode());
                tLADimissionSet=tLADimissionDB.query();
                if(tLADimissionSet.size()>0)
                {
                	 continue;
                }
                else
                {
            	tLADimissionSchema = new LADimissionSchema();

                tLADimissionSchema.setAgentCode(mLAAssessSet3.get(i).
                                                getAgentCode());
                tLADimissionSchema.setApplyDate(mEvaluateDate); //处理时间
                tLADimissionSchema.setDepartRsn("0"); //离职原因
                tLADimissionSchema.setNoti("维持考核不达标！"); //备注
                tLADimissionSchema.setOperator(this.mGlobalInput.Operator);
                tLADimissionSchema.setBranchType(mLAAssessSet3.get(i).
                                                 getBranchType());
                tLADimissionSchema.setBranchType2(mLAAssessSet3.get(i).
                                                  getBranchType2());
                tLADimissionSchema.setDepartTimes("1");

                tVData = new VData();
                tVData.addElement(tLADimissionSchema);
                tVData.add(this.mGlobalInput);

                LAInteractionDimissionBL tLADimission = new LAInteractionDimissionBL();
                if (!tLADimission.submitData(tVData, "INSERT||MAIN")) {
                    this.mErrors.copyAllErrors(tLADimission.mErrors);
                    System.out.println("进行离职人员处理失败！");
                    return false;
                }
               }
            }
        }
        return true;
    }
    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        mInputData.clear();
        mMap.put(mLAAssessSet, "UPDATE");
        mMap.put(mLAAssessMainSet, "UPDATE");
        mMap.put(mLATreeSet, "UPDATE");
        mMap.put(mLATreeBSet, "INSERT");
        mInputData.add(mMap);
        return true;
    }
    private  void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();
        tError.moduleName = "AssessActiveConfirmBL";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;
        mErrors.addOneError(tError);
    }
} //end class

