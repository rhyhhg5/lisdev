package com.sinosoft.lis.agentassess;

import java.io.File;

import com.sinosoft.lis.agentconfig.LAGetOtherChargeDiskImporter;
import com.sinosoft.lis.db.LADiscountDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LADiscountSchema;
import com.sinosoft.lis.schema.LADiscountSchema;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.vschema.LADiscountSet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.lis.vschema.LMRiskSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportActiveStandRateBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();
    
    private int importPersons = 0; //记录导入成功的记录数
    private int unImportRecords = 0; //记录导入不成功的记录数
    
    private String diskimporttype = "";
    private String path = "";
    private String fileName = "";
    private String configName = "LAActiveStandRateDiskImport.xml";
    LDComDB mLDComDB=new LDComDB();
	LDComSet mLDComSet=new LDComSet();
	
    private LADiscountSet mLADiscountSetFinal=new LADiscountSet();
    
    private LADiscountSet mLADiscountSet = null;
    private LCGrpImportLogSet mLCGrpImportLogSet=new LCGrpImportLogSet();
    private int merrorNum=0;
    private StringBuffer merrorInfo=new StringBuffer();
    public LADiskImportActiveStandRateBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
    	//System.out.println("BL submited");
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkBatchno()){
        	return false;
        }
        //System.out.println("geted inputdata");
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("fileName");
        diskimporttype=(String)mTransferData.getValueByName("diskimporttype");
        System.out.println("存入类型dddddd: "+diskimporttype);
        String configFileName = path + File.separator + configName;
        System.out.println("begin import");
        //从磁盘导入数据
        LAGetOtherChargeDiskImporter importer = new LAGetOtherChargeDiskImporter(fileName,
                configFileName,
                diskimporttype);
        //System.out.println("LADiskImportActiveStandRateBL.java:diskimporttype"+diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        
        
        
        System.out.println("doimported");
        System.out.println("数据eee:"+diskimporttype);
        if (diskimporttype.equals("LADiscount")) {
            mLADiscountSet = (LADiscountSet) importer
                                  .getSchemaSet();
            System.out.println("存折信息表存在值:"+mLADiscountSet.size());
            System.out.println("存入类型："+diskimporttype);

        }
        if (!checkData()) {
            return false;
        }
        System.out.println("data checked");
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }
        this.mResult.add(mmap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("LADiskImportActiveStandRateBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);
        this.fileName=(String)mTransferData.getValueByIndex(4);
        
        System.out.println("...........mTransferData.getValueByIndex(4)"+mTransferData.getValueByIndex(4));
        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }
        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        System.out.println("...............mTransferData.getValueByName(branchtype)"
        		+mTransferData.getValueByName("branchtype"));
        return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkBatchno(){
    	String tSQL="select 1 from lcgrpimportlog where batchno='"+this.fileName+"' with ur";
    	ExeSQL tExeSQL=new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL).equals("1")){
        	mErrors.addOneError("该文件批次已经被使用过，请使用新的文件批次！");
    		return false;
    	}
    	return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
    	FDate chgdate = new FDate();
        //展业证信息校验
        if (diskimporttype.equals("LADiscount")) {
            if (mLADiscountSet == null) {
                mErrors.addOneError("导入折标系数信息失败！没有得到要导入的数据或者导入模版不是最新，请下载最新的模版。");
                return false;
            } else {
            	LMRiskDB tLMRiskDB=new LMRiskDB();
            	LMRiskSet tLMRiskSet=new LMRiskSet();
            	String tempSQL="select * from lmrisk where 1=1 with ur";
            	tLMRiskSet=tLMRiskDB.executeQuery(tempSQL);
            	tempSQL="select * from ldcom where  1=1 and sign ='1' ";
            	mLDComSet=mLDComDB.executeQuery(tempSQL);
            	System.out.println("mLDComSet size:"+mLDComSet.size());
            	LADiscountDB mTempLADiscountDB=new LADiscountDB();
            	//LADiscountSet mLADiscountSet=new LADiscountSet();
            	LADiscountSchema mTempLADiscountSchema=new LADiscountSchema();
            	LADiscountSchema mTempLADiscountSchema2;
            	LCGrpImportLogSchema tLCGrpImportLogSchema;
            	String importInfo;
                for (int i = 1; i <= mLADiscountSet.size(); i++) {
                	this.merrorNum=0;
                	this.merrorInfo.delete(0,merrorInfo.length());
                	System.out.println("..............checkdata here merrorInfo"
                			+this.merrorInfo); 
                	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                	mTempLADiscountSchema=mLADiscountSet.get(i);
                    if (mTempLADiscountSchema.getManageCom() == null ||
                    		mTempLADiscountSchema.getManageCom().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("管理机构不能为空/");
                    }
                    if (mTempLADiscountSchema.getManageCom().length()<this.mGlobalInput.ManageCom.length() 
                    		|| !mTempLADiscountSchema.getManageCom().substring(0,this.mGlobalInput.ManageCom.length())
                    		.equals(this.mGlobalInput.ManageCom)) {
                        this.merrorNum++;
                        this.merrorInfo.append("权限不够，设置该条提奖失败/");
                    }
                    if (mTempLADiscountSchema.getRiskCode() == null ||
                    		mTempLADiscountSchema.getRiskCode().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("险种编码不能为空/");
                    } 
                    if (mTempLADiscountSchema.getPayIntv() == null ||
                    		mTempLADiscountSchema.getPayIntv().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("缴费年限不能为空/");
                    } 
                    if(!mTempLADiscountSchema.getPayIntv().equals("0")
                    		  &&!mTempLADiscountSchema.getPayIntv().equals("1")
                		      &&!mTempLADiscountSchema.getPayIntv().equals("3")
                		      &&!mTempLADiscountSchema.getPayIntv().equals("5")
                    		  &&!mTempLADiscountSchema.getPayIntv().equals("10")
                    		  &&!mTempLADiscountSchema.getPayIntv().equals("99")){
                    	this.merrorNum++;
                        this.merrorInfo.append("不能录入此缴费年限/");
                    }
                    if(mTempLADiscountSchema.getManageCom()!=null
                    		&&!mTempLADiscountSchema.getManageCom().equals("")
                    		&&!mTempLADiscountSchema.getManageCom().equals("86")){
                    	int tempFlag1=0;
                        for(int m=1;m<=mLDComSet.size();m++){
                        	if(mLDComSet.get(m).getComCode()
                        			.equals(mTempLADiscountSchema.getManageCom())){
                        		tempFlag1=1;
                        		break;
                        	}
                        }
                        if(tempFlag1==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("管理机构代码不存在/");
                        }
                    }
                    int tempFlag2=0;
                    if(mTempLADiscountSchema.getRiskCode()!=null
                    		&&!mTempLADiscountSchema.getRiskCode().equals("")){
                    	for(int n=1;n<=tLMRiskSet.size();n++){
                    		if(tLMRiskSet.get(n).getRiskCode()
                        			.equals(mTempLADiscountSchema.getRiskCode())){
                        		
                        		tempFlag2=1;
                        		break;
                        	}
                        }
                        if(tempFlag2==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("该险种不存在/");
                        }
                    }
                    if(this.merrorNum==0){
                    	for(int j = i+1; j <= mLADiscountSet.size(); j++){
                        	mTempLADiscountSchema2=mLADiscountSet.get(j);
                        	if(mTempLADiscountSchema.getManageCom().equals(mTempLADiscountSchema2.getManageCom())
                        	&&mTempLADiscountSchema.getRiskCode().equals(mTempLADiscountSchema2.getRiskCode())
                        	&&mTempLADiscountSchema.getPayIntv().equals(mTempLADiscountSchema2.getPayIntv()))
                        	{
                        		this.merrorNum++;
                                this.merrorInfo.append("与第"+j+"行数据重复/");
                        	}
                        }
                    }
                    if(this.merrorNum==0){
                       checkExist(mTempLADiscountSchema,mTempLADiscountDB);
                    }
                    if(this.merrorNum>0){
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入错误！具体错误有："
                    			+this.merrorInfo.toString());
                    	tLCGrpImportLogSchema.setErrorState("1");
                    	tLCGrpImportLogSchema.setErrorType("1");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    	this.unImportRecords++;
                    }else{
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入成功！");
                    	tLCGrpImportLogSchema.setErrorState("0");
                    	tLCGrpImportLogSchema.setErrorType("0");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    	this.importPersons++;
                    }
                }
            }
        }
        return true;
    }
private boolean checkExist(LADiscountSchema mLADiscountSchema,LADiscountDB mLADiscountDB){

	LADiscountSet tempLADiscountSet=new LADiscountSet();
	String tempSql="select * from ladiscount where managecom='"
		+mLADiscountSchema.getManageCom()+"' and riskcode='"
		+mLADiscountSchema.getRiskCode()+"' and payintv='"
		+mLADiscountSchema.getPayIntv()+"' and branchtype='"
		+mTransferData.getValueByName("branchtype")+"' and branchtype2='"
		+mTransferData.getValueByName("branchtype2")+"' ";
	System.out.println("checkExist:tempsql"+tempSql);
	tempLADiscountSet=mLADiscountDB.executeQuery(tempSql);
	if(tempLADiscountSet.size()>0){
		System.out.println("unImportRecords appending one");
		this.merrorNum++;
        this.merrorInfo.append("在数据库中已经存在/");
	}
	else
	{
		LADiscountSchema tempLADiscountSchema=new LADiscountSchema();
		tempLADiscountSchema.setManageCom(mLADiscountSchema.getManageCom());
		tempLADiscountSchema.setRiskCode(mLADiscountSchema.getRiskCode());
		tempLADiscountSchema.setRate(mLADiscountSchema.getRate());
		tempLADiscountSchema.setPayIntv(mLADiscountSchema.getPayIntv());
		//tempLADiscountSchema=mLADiscountSchema;
		mLADiscountSetFinal.add(tempLADiscountSchema);
	}
	return true;
}
//校验缴费年期区间重复
    /**
     * 检查每个客户是否是本公司的代理人，若不是则剔除
     * @param
     * @return boolean，成功校验 trun
     */
    private boolean prepareData() {
    	String idxTemp = (String) mTransferData.getValueByName("idx");
    	int idx=Integer.parseInt(idxTemp)+1;
        if (diskimporttype.equals("LADiscount")) {
        	System.out.println("prepareData:doing");
            //System.out.println("prepareData:unImportPersons"+unImportPersons);
            if(mLADiscountSetFinal!=null&&mLADiscountSetFinal.size()>0){
             	//importPersons = mLADiscountSetFinal.size();
            	System.out.println("prepareData:importPersons"+importPersons);
            	for (int i = 1; i <= mLADiscountSetFinal.size(); i++) {
              	System.out.println("preparedata : managecom"+mLADiscountSetFinal.get(i).getManageCom());
              	String branchtype=(String) mTransferData.getValueByName("branchtype");
              	System.out.println("BL:branchtype  "+branchtype);
              	String branchtype2=(String) mTransferData.getValueByName("branchtype2");
              	System.out.println("BL:branchtype2  "+branchtype2);
              	mLADiscountSetFinal.get(i).setOperator(mGlobalInput.Operator);
              	mLADiscountSetFinal.get(i).setIdx(idx++);
              	mLADiscountSetFinal.get(i).setBranchType(branchtype);
              	mLADiscountSetFinal.get(i).setBranchType2(branchtype2); 
              	mLADiscountSetFinal.get(i).setDiscountType("01");
              	mLADiscountSetFinal.get(i).setMakeDate(PubFun.getCurrentDate());
              	mLADiscountSetFinal.get(i).setMakeTime(PubFun.getCurrentTime());
              	mLADiscountSetFinal.get(i).setModifyDate(PubFun.getCurrentDate());
              	mLADiscountSetFinal.get(i).setModifyTime(PubFun.getCurrentTime());
              	//mLADiscountSet.get(i).setRateType("11");
              	mmap.put(mLADiscountSetFinal.get(i), "INSERT");
              }
            }
            
            if(this.mLCGrpImportLogSet!=null&&this.mLCGrpImportLogSet.size()>0){
            	//importPersons = mLADiscountSetFinal.size();
            	for(int i=1;i<=this.mLCGrpImportLogSet.size();i++){
            		mLCGrpImportLogSet.get(i).setGrpContNo("00000000000000000000");
            		mLCGrpImportLogSet.get(i).setOperator(this.mGlobalInput.Operator);
            		mLCGrpImportLogSet.get(i).setMakeDate(PubFun.getCurrentDate());
            		mLCGrpImportLogSet.get(i).setMakeTime(PubFun.getCurrentTime());
            		mLCGrpImportLogSet.get(i).setBatchNo(this.fileName);
            		mLCGrpImportLogSet.get(i).setContNo("ActivetandRate");
            		mmap.put(mLCGrpImportLogSet.get(i), "INSERT");
            	}
            }
            System.out.println("prepareData:adding map");
            //this.mResult.add(mmapCompare);
            System.out.println("prepareData:did");
        }
        return true;
    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public int getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");
        
        VData v = new VData();
        v.add(g);
        v.add(t);

        LADiskImportActiveStandRateBL d = new LADiskImportActiveStandRateBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }

}
