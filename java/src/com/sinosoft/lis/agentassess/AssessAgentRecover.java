package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.agentcalculate.DealBusinessData;
import com.sinosoft.lis.db.LAAgentBDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vdb.LATreeBDBSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description:该类针对同系列的恢复、离职的恢复以及被降级代理人的行政信息及业绩恢复 </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author: zhangsj
 * @version 1.0
 */

public class AssessAgentRecover
{
    public CErrors mErrors = new CErrors();
    private GlobalInput mGI = new GlobalInput();
    private String mIndexCalNo = "";
    private String[] MonDate = new String[2];

    public AssessAgentRecover()
    {
    }

    public static void main(String[] args)
    {
        AssessAgentRecover assessAgentRecover1 = new AssessAgentRecover();
        VData tInput = new VData();
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86110000";
        tGI.Operator = "aa";

//    String tIndexCalNo = "200312";
//    String tAgentCode = "8611002014";
//    String tAimBranchAttr = "861100000103001001";
//
//    LAAssessSchema tAssessSch = new LAAssessSchema();
//    LAAssessDB tAssessDB = new LAAssessDB();
//    tAssessDB.setIndexCalNo(tIndexCalNo);
//    tAssessDB.setAgentCode(tAgentCode);
//    if ( !tAssessDB.getInfo() )  {
//      System.out.println("tAssessDB.getInfo failed!");
//    }
//    tAssessSch.setSchema(tAssessDB.getSchema());
//
//    tInput.clear();
//    tInput.add(tGI);
//    tInput.add(0, "8611000002");
//    tInput.add(1, "200401");
//    tInput.add(2, "0000000011");
//    tInput.add(3, "861100000102");
//    tInput.add(4, "861100000102");

//    tInput.add(0,tIndexCalNo);
//    tInput.add(1,tAimBranchAttr);
//    tInput.add(tAssessSch);

//    if (!assessAgentRecover1.agentRecover(tInput))
//      System.out.println("---failed--" +
//                         assessAgentRecover1.mErrors.getFirstError());
//    else
//      System.out.println("----Succ!---");
//
        // simpleRecover
        LAAssessDB tAssessDB = new LAAssessDB();
        String tIndexCalNo = "200407";
        String tAgentCode = "8611000057";

        tAssessDB.setIndexCalNo(tIndexCalNo);
        tAssessDB.setAgentCode(tAgentCode);
        if (!tAssessDB.getInfo())
        {
            System.out.println("tAssessDB.getInfo failed!");
        }
        tInput.add(tAssessDB.getSchema());
        tInput.add(tIndexCalNo);
        if (!assessAgentRecover1.simpleRecover(tInput))
        {
            System.out.println("---failed--" +
                               assessAgentRecover1.mErrors.getFirstError());
        }
        else
        {
            System.out.println("----Succ!---");
        }

        //agentrecover
//    tInput.add(0, "8611000113");
//    tInput.add(1, "200404");
//    tInput.add(2, "0000000098");
//    tInput.add(3, "861100000103004");
//    tInput.add(4, "861100000103004");
//
//    if (!assessAgentRecover1.agentRecover(tInput))
//      System.out.println("---failed--" +
//                         assessAgentRecover1.mErrors.getFirstError());
//    else
//      System.out.println("----Succ!---");
    }

    public boolean agentRecover(VData cInputData)
    {
        this.mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
                0);
        String tAgentCode = (String) cInputData.getObjectByObjectName("String",
                0);
        String tIndexCalNo = (String) cInputData.getObjectByObjectName("String",
                1);
        String tNewAgentGroup = (String) cInputData.getObjectByObjectName(
                "String",
                2);
        String tNewAttr = (String) cInputData.getObjectByObjectName("String", 3);
        String tOldAttr = (String) cInputData.getObjectByObjectName("String", 4);

        int tSize = 0;

        if (tAgentCode == null || tAgentCode.equals("") ||
            tIndexCalNo == null || tIndexCalNo.equals("") ||
            tNewAttr == null || tNewAttr.equals("") ||
            tOldAttr == null || tOldAttr.equals(""))
        {
            dealError("agentRecover", "没有传入足够数据！");
            return false;
        }
        this.mIndexCalNo = tIndexCalNo;
        genDate();

        //如果非第一次做机构恢复，则需查询被恢复的AgentGroup编码，用于做行政信息及业绩恢复
        if (tNewAgentGroup == null || tNewAgentGroup.equals(""))
        {
            ExeSQL tQryGroup = new ExeSQL();
            String tGetGroupSql = "select agentgroup from labranchgroup where "
                                  + "branchattr = '" + tNewAttr.trim() + "'";
            tNewAgentGroup = tQryGroup.getOneValue(tGetGroupSql);
            if (tNewAgentGroup == null || tNewAgentGroup.trim().equals(""))
            {
                dealError("agentRecover",
                          "继续做机构恢复时查询新的AgentGroup失败，请重新录入新的展业机构编码！" +
                          tQryGroup.mErrors.getFirstError());
                return false;
            }
        }

        String tNewFlag = "";
        if (!tNewAttr.trim().equals(tOldAttr.trim()))
        {
            tNewFlag = "Y";
        }
        else
        {
            tNewFlag = "N";
        }

        int tLength = tNewAttr.trim().length();
        String tBranchAttr = "";
        String tBranchLevel = "";
        String tUpAgent = "";
        String tAgentSeries = "";
        String tAgentGrade = "";
        if (tLength == 18)
        {
            tBranchAttr = tNewAttr.trim().substring(0, 15);
            tBranchLevel = "02";
            tAgentSeries = "B";
        }
        if (tLength == 15)
        {
            tBranchAttr = tNewAttr.trim().substring(0, 12);
            tBranchLevel = "03";
            tAgentSeries = "C";
        }
        if (tLength == 12)
        {
            tBranchAttr = tNewAttr.trim().substring(0, 10);
            tBranchLevel = "04";
            tAgentSeries = "D";
            tAgentGrade = "A08";
        }
        if (tLength == 10)
        {
            tUpAgent = "BC";
            tAgentSeries = "D";
            tAgentGrade = "A09";
        }

        if (!tUpAgent.trim().equals("BC"))
        {
            String tUpAgentSql =
                    "select nvl(branchmanager,'notfound') from labranchgroup where branchattr = '"
                    + tBranchAttr.trim() + "' and branchlevel = '"
                    + tBranchLevel.trim() + "'";
            System.out.println("---tUpAgentSql : " + tUpAgentSql);
            ExeSQL tExeSql = new ExeSQL();
            tUpAgent = tExeSql.getOneValue(tUpAgentSql).trim();

            if (tUpAgent == null || tUpAgent.trim().equals("") ||
                tUpAgent.trim().equals("notfound"))
            {
                if (tAgentGrade.trim().compareTo("A08") >= 0)
                {
                    tUpAgent = "";
                }
                else
                {
                    dealError("agentRecover", "查询上级代理人失败！");
                    return false;
                }
            }
        }

        PubFun tPF = new PubFun();

        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            dealError("agentRecover", "数据库连接失败！");
            return false;
        }

        try
        {
            conn.setAutoCommit(false);
            String tTreeSql = "";
            //恢复行政信息
            if (!tAgentSeries.trim().equals("D"))
            {
                tTreeSql = "select * from latreeb where agentcode = '"
                           + tAgentCode.trim() + "' and AgentGroup = '"
                           + tNewAgentGroup.trim()
                           + "' and indexcalno = '" + tIndexCalNo.trim()
                           + "' and removetype = '01' and agentseries = '"
                           + tAgentSeries.trim() + "'";
            }
            else
            {
                tTreeSql = "select * from latreeb where agentcode = '"
                           + tAgentCode.trim() + "' and AgentGroup = '"
                           + tNewAgentGroup.trim()
                           + "' and indexcalno = '" + tIndexCalNo.trim()
                           + "' and removetype = '01' and agentgrade = '"
                           + tAgentGrade.trim() + "'";
            }
            System.out.println("----111---tTreeSql: " + tTreeSql);

            LATreeBDB tTreeBDB = new LATreeBDB(conn);

            LATreeBSet tTreeBSet = new LATreeBSet();
            tTreeBSet = tTreeBDB.executeQuery(tTreeSql);
            tSize = tTreeBSet.size();
            System.out.println("---TreeB---size: " + tSize);
            if (tSize == 0)
            {
                dealError("agentRecover", "行政信息备份表中没有信息！");
                conn.rollback();
                conn.close();
                return false;
            }
            LATreeBSchema tTreeBSch = new LATreeBSchema();
            tTreeBSch.setSchema(tTreeBSet.get(1));

            LATreeSchema tTreeSch = new LATreeSchema();
            Reflections rf = new Reflections();
            LATreeSchema tSch = new LATreeSchema();
            rf.transFields(tTreeSch, tTreeBSch);

            tTreeSch.setUpAgent(tUpAgent.trim());
            tTreeSch.setMakeDate(tTreeBSch.getMakeDate2());
            tTreeSch.setMakeTime(tTreeBSch.getMakeTime2());
            tTreeSch.setModifyDate(tTreeBSch.getModifyDate2());
            tTreeSch.setModifyTime(tTreeBSch.getModifyTime2());
            tTreeSch.setOperator(tTreeBSch.getOperator2());
            //如果属新建情况，则AStartDate更新为考核期下月的一号
            if (tNewFlag.trim().equals("Y"))
            {
                tTreeSch.setAstartDate(this.MonDate[0]);
                tTreeSch.setModifyDate(tPF.getCurrentDate());
                tTreeSch.setModifyTime(tPF.getCurrentTime());
                tTreeSch.setOperator(this.mGI.Operator);
            }

            LATreeDB tTreeDB = new LATreeDB(conn);
            tTreeDB.setSchema(tTreeSch);
            if (!tTreeDB.update())
            {
                dealError("agentRecover", "刷新LATree失败！");
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("---222---end recover Tree--");
            if (tNewFlag.trim().equals("N"))
            {
                tTreeBDB.setAgentCode(tAgentCode.trim());
                tTreeBDB.setIndexCalNo(tIndexCalNo.trim());
                tTreeBDB.setRemoveType("01");
                tTreeBDB.setAgentGrade(tTreeBSch.getAgentGrade());
                if (!tTreeBDB.deleteSQL())
                {
                    dealError("agentRecover", "删除LATreeB失败！");
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            else
            {
                String tSql = "delete from latreeb where agentseries <> '"
                              + tAgentSeries.trim() + "' and agentcode = '"
                              + tAgentCode.trim() + "' and indexcalno = '"
                              + tIndexCalNo.trim() + "' and removetype = '01'";
                System.out.println("----delete latreeb---tSql : " + tSql);
                ExeSQL tExe = new ExeSQL(conn);
                if (!tExe.execUpdateSQL(tSql))
                {
                    dealError("agentRecover",
                              "删除LATreeB备份信息失败！" + tExe.mErrors.getFirstError());
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("---333---end recover Treeb--");

            //恢复行政信息附属表LATreeAccessory
            //zsj--Modified--2004-3-23:因为降级时将LATreeAccessory表的commBreakFlag置为1
            LATreeAccessoryDB tAccDB = new LATreeAccessoryDB(conn);
            tAccDB.setAgentCode(tAgentCode.trim());
            tAccDB.setAgentGrade(tTreeSch.getAgentGrade().trim());
            if (!tAccDB.getInfo())
            {
                dealError("agentRecover",
                          "查询LATreeAccessory信息失败！" +
                          tAccDB.mErrors.getFirstError());
                conn.rollback();
                conn.close();
                return false;
            }
            LATreeAccessorySchema tAccSch = new LATreeAccessorySchema();
            tAccSch.setSchema(tAccDB.getSchema());
//      if (!tAccDB.delete())
//      {
//        dealError("agentRecover",
//                  "删除LATreeAccessory信息失败！" + tAccDB.mErrors.getFirstError());
//        conn.rollback();
//        conn.close();
//        return false;
//
//      }
            tAccSch.setCommBreakFlag("0");
            tAccSch.setIntroBreakFlag("0");
            tAccSch.setRearFlag("0");
            tAccSch.setModifyDate(tPF.getCurrentDate());
            tAccSch.setModifyTime(tPF.getCurrentTime());
            tAccSch.setOperator(this.mGI.Operator);

            tAccDB.setSchema(tAccSch);
            if (!tAccDB.update())
            {
                dealError("agentRecover", "插入LATreeAccessory失败！");
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("----555---after ins accessory---");

            //恢复LAAgent
            String tAgentSql = "select * from laagentb where agentcode = '"
                               + tAgentCode.trim() + "' and AgentGroup = '"
                               + tNewAgentGroup.trim()
                               + "' and indexcalno = '" + tIndexCalNo.trim()
                               + "' and edortype = '01'";
            System.out.println(
                    "----777---begin recover laagent---tQryLAAgentB : " +
                    tAgentSql);
            LAAgentBDB tAgentBDB = new LAAgentBDB(conn);
            LAAgentBSet tAgentBSet = new LAAgentBSet();
            tAgentBSet = tAgentBDB.executeQuery(tAgentSql);
            tSize = tAgentBSet.size();
            if (tSize == 0)
            {
                dealError("agentRecover", "代理人信息备份表中没有信息！");
                conn.rollback();
                conn.close();
                return false;
            }

            LAAgentDB tAgentDB = new LAAgentDB(conn);
            LAAgentBSchema tAgentBSch = new LAAgentBSchema();
            tAgentBSch.setSchema(tAgentBSet.get(1));
            Reflections rfAgent = new Reflections();
            LAAgentSchema tAgentSch = new LAAgentSchema();
            LAAgentSchema tTempSch = new LAAgentSchema();
            tAgentDB.setAgentCode(tAgentCode);
            if (!tAgentDB.getInfo())
            {
                dealError("agentRecover", "从LAAgent表中查询入机日期失败！");
                conn.rollback();
                conn.close();
                return false;
            }
            tTempSch.setSchema(tAgentDB.getSchema());

            rfAgent.transFields(tAgentSch, tAgentBSch);
            tAgentSch.setModifyDate(tPF.getCurrentDate());
            tAgentSch.setModifyTime(tPF.getCurrentTime());
            tAgentSch.setMakeDate(tTempSch.getMakeDate());
            tAgentSch.setMakeTime(tTempSch.getMakeTime());
            tAgentSch.setOperator(this.mGI.Operator);

            //刷新LAAgent表
            tAgentDB.setSchema(tAgentSch);
            if (!tAgentDB.update())
            {
                dealError("agentRecover", "刷新LAAgent失败！");
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("----888---after upd LAAgent---");

            //删除LAAgentB表的记录
            tAgentBDB.setAgentCode(tAgentCode);
            tAgentBDB.setIndexCalNo(tIndexCalNo);
            tAgentBDB.setEdorType("01");
            if (!tAgentBDB.deleteSQL())
            {
                dealError("agentRecover", "删除LAAgentB失败！");
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("----999---after del laagentb---");

            // 刷新LAAssess
            LAAssessDB tAssDB = new LAAssessDB(conn);
            tAssDB.setAgentCode(tAgentCode);
            tAssDB.setIndexCalNo(tIndexCalNo);
            if (!tAssDB.getInfo())
            {
                dealError("agentRecover", "查询LAAssess信息失败！");
                conn.rollback();
                conn.close();
                return false;
            }

            LAAssessSchema tAssSch = new LAAssessSchema();
            tAssSch.setSchema(tAssDB.getSchema());
            tAssSch.setAgentGroupNew(tNewAgentGroup.trim());
            tAssSch.setState("2");
            tAssSch.setModifyDate(tPF.getCurrentDate());
            tAssSch.setModifyTime(tPF.getCurrentTime());
            tAssSch.setOperator(this.mGI.Operator);

            tAssDB.setSchema(tAssSch);
            if (!tAssDB.update())
            {
                dealError("agentRecover", "刷新LAAssess信息失败！");
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("----1010---after upd lAAssess---");
            //恢复业绩
            //准备BranchCode
            String tDirAttr = "";
            String tBranchCode = "";
            if (tLength == 18)
            {
                tBranchCode = tNewAgentGroup;
                tDirAttr = tNewAttr;
            }
            else
            {
                tBranchCode = tAgentSch.getBranchCode();
                LABranchGroupDB tDirDB = new LABranchGroupDB();
                tDirDB.setAgentGroup(tBranchCode);
                if (!tDirDB.getInfo())
                {
                    dealError("agentRecover",
                              "恢复代理人业绩查询直辖组的展业机构编码失败，检查是否已恢复直辖组！");
                    conn.rollback();
                    conn.close();
                    return false;
                }
                tDirAttr = tDirDB.getBranchAttr().trim();
                int tLen = tNewAttr.trim().length();
                String tAttr = tDirAttr.trim().substring(0, tLen);
                if (!tNewAttr.equals(tAttr))
                {
                    dealError("agentRecover", "检查是否已恢复直辖组！");
                    conn.rollback();
                    conn.close();
                    return false;

                }
            }

            if (tBranchCode == null || tBranchCode.equals(""))
            {
                dealError("agentRecover", "查询用于更新业绩的BranchCode失败！");
                conn.rollback();
                conn.close();
                return false;

            }

            //恢复业绩，进行校验
            AgentPubFun tAPF = new AgentPubFun();
            String tCheckFlag = tAPF.AdjustCommCheck(tAgentCode, this.MonDate[0]);
            if (!tCheckFlag.trim().equals("00"))
            {
                dealError("agentRecover", tCheckFlag.trim());
                conn.rollback();
                conn.close();
                return false;

            }

//zsj-delete--2004-3-9:因为每月计算佣金之前会处理打折的情况，所以归属时不需要对业绩打折
//      if (tAssSch.getAgentGrade().trim().equals("A01")
//          && tAssSch.getAgentSeries1().equals("B")) {
//        CalFormDrawRate tCalRate = new CalFormDrawRate();
//        LACommisionDB tDB = new LACommisionDB();
//        LACommisionDBSet tDBSet = new LACommisionDBSet(conn);
//        LACommisionSet tSet = new LACommisionSet();
//        LACommisionSet tResult = new LACommisionSet();
//
//        String tSQL = "select * from lacommision where trim(agentcode) = '"
//            + tAgentCode.trim() + "' and (caldate >= '"
//            + this.MonDate[0].trim()
//            + "' or caldate is null) order by polno,ReceiptNo";
//        System.out.println("--updLACommision--D--tSQL : " + tSQL);
//
//        tSet = tDB.executeQuery(tSQL);
//        int Size = tSet.size();
//        System.out.println("---updLACommision : size = " + tSize);
//        if (Size == 0)
//          return true;
//
//        VData tInputData = new VData();
//        tInputData.clear();
//        tInputData.add(tSet);
//        tInputData.add(0, "A04");
//
//        if (!tCalRate.submitData(tInputData,"RECALFYC||AGENTWAGE")) {
//          dealError("updLACommision", tCalRate.mErrors.getFirstError());
//          return false;
//        }
//
//        tResult = (LACommisionSet) tCalRate.getResult().getObjectByObjectName(
//            "LACommisionSet", 0);
//
//
//        int tCount = tResult.size();
//        if (Size != tCount) {
//          dealError("updLACommision", "传入类CalFormDrawRate.java中的数据和传出的数据条数不一致！");
//          return false;
//        }
//
//        if (!tDBSet.set(tResult)) {
//          dealError("updLACommision", tDBSet.mErrors.getFirstError());
//          return false;
//        }
//
//        if (!tDBSet.update()) {
//          dealError("updLACommision", tDBSet.mErrors.getFirstError());
//          return false;
//        }
//      }

            DealBusinessData tDealBusiness = new DealBusinessData();
            VData tInputData = new VData();
            tInputData.clear();
            tInputData.add(0, tAgentCode);
            tInputData.add(1, tNewAgentGroup);
            tInputData.add(2, "Y");
            tInputData.add(3, tDirAttr);
            tInputData.add(4, tIndexCalNo);
            tInputData.add(5, tBranchCode.trim());

            if (!tDealBusiness.updateAgentGroup(tInputData, conn))
            {
                dealError("agentRecover",
                          "恢复代理人业绩失败！" + tDealBusiness.mErrors.getFirstError());
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("----11 11---after upd businessdata---");
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            try
            {
                conn.close();
                conn.rollback();
            }
            catch (Exception ep)
            {
                ep.printStackTrace();
                dealError("agentRecover", ep.toString());
                return false;
            }
            dealError("agentRecover", ex.toString());
            return false;
        }
        return true;
    }

//  //zsj--modified--2004-3-10:不调用组织归属程序进行职级恢复，因为：latree中会记录上次职级
//  public boolean simpleRecoverOld(VData cInputData) {
//    GlobalInput tGI = new GlobalInput();
//    LAAssessSchema tAssessSch = new LAAssessSchema();
//    tGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
//    String tIndexCalNo = (String) cInputData.getObjectByObjectName("String", 0);
//    tAssessSch = (LAAssessSchema) cInputData.getObjectByObjectName(
//        "LAAssessSchema", 0);
//
//    if (tIndexCalNo .equals("") || tIndexCalNo == null || tAssessSch == null) {
//      dealError("simpleRecover", "没有传入足够数据！");
//      return false;
//    }
//    this.mIndexCalNo = tIndexCalNo;
//    genDate();
//
//    // 判断是否同系列职级恢复
//    String t_AgentSeries1 = tAssessSch.getAgentSeries1().trim();
//    String t_AgentSeries = tAssessSch.getAgentSeries().trim();
//    if (!t_AgentSeries1.equals(t_AgentSeries)) {
//      dealError("simpleRecover", "非同系列职级恢复，请重新选择！");
//      return false;
//    }
//
//    //判断是否第一次由A02->A01:取LAAgent.IndueFormDate 和 LATree 的StartDate比较
//    String t_AgentGrade = tAssessSch.getAgentGrade().trim();
//    String t_AgentGrade1 = tAssessSch.getAgentGrade1().trim();
//    String t_AgentCode = tAssessSch.getAgentCode().trim();
//    String updAgentFlag = "";
//    LAAgentSchema tAgentSch = new LAAgentSchema();
//    LATreeSchema tTreeSch = new LATreeSchema();
//    if ( (t_AgentGrade.equals("A02") || t_AgentGrade.equals("A03")) &&
//        t_AgentGrade1.equals("A01")) {
//      LAAgentDB tAgentDB = new LAAgentDB();
//      tAgentDB.setAgentCode(t_AgentCode);
//      if (!tAgentDB.getInfo()) {
//        dealError("simpleRecover", tAgentDB.mErrors.getFirstError());
//        return false;
//      }
//      tAgentSch = tAgentDB.getSchema();
//      String tIndueFormDate = tAgentSch.getInDueFormDate().trim();
//
//      //取LATree的StartDate
//      LATreeDB tTreeDB = new LATreeDB();
//      tTreeDB.setAgentCode(t_AgentCode);
//      if (!tTreeDB.getInfo()) {
//        dealError("simpleRecover", tAgentDB.mErrors.getFirstError());
//        return false;
//      }
//      tTreeSch = tTreeDB.getSchema();
//      String tStartDate = tTreeSch.getStartDate().trim();
//
//      if (tIndueFormDate.equalsIgnoreCase(tStartDate))
//        updAgentFlag = "T";
//      else
//        updAgentFlag = "F";
//    }
//
//    VData tOutPutData = new VData();
//    tOutPutData.clear();
//    tOutPutData.add(tGI);
//    tOutPutData.add(tIndexCalNo);
//
//    OAgentAscript tAscript = new OAgentAscript();
//    if (!tAscript.getInputForEach(tOutPutData)) {
//      dealError("agentDemote", "传入归属数据失败！");
//      return false;
//    }
//
//    if (!tAscript.dealEach(tAssessSch)) {
//      dealError("agentDemote", tAscript.mErrors.getFirstError());
//      return false;
//    }
//
//    Connection conn = DBConnPool.getConnection();
//    if (conn == null) {
//      dealError("agentDemote", "数据库连接失败！");
//      return false;
//    }
//
//    try {
//      conn.setAutoCommit(false);
//
//      //delete latreeb
//      LATreeBDB tTreeBDB = new LATreeBDB(conn);
//
//      // AgentGrade < 'A04' 时，LATreeb表中有可能有因组号变化而插入的备份记录，该记录不能删除
//      String tAgentGrade = tAssessSch.getAgentGrade().trim();
//      if (tAgentGrade.compareTo("A04") < 0) {
//        //删调最初做组织归属的记录
//        String tMinSql = "select min(edorno) from latreeb where agentcode = '"
//            + tAssessSch.getAgentCode() + "' and indexcalno = '"
//            + tIndexCalNo.trim() + "' and removetype = '01' ";
//        ExeSQL tExe = new ExeSQL();
//        String tMinEdorNo = "";
//        tMinEdorNo = tExe.getOneValue(tMinSql);
//        if (tMinEdorNo .equals("") || tMinEdorNo == null) {
//          dealError("simpleRecover", "查询LATreeB表的最小的EdorNo失败！");
//          conn.rollback();
//          conn.close();
//          return false;
//        }
//
//        //删除恢复做组织归属时的记录
//        String tMaxSql = "select max(edorno) from latreeb where agentcode = '"
//            + tAssessSch.getAgentCode() + "' and indexcalno = '"
//            + tIndexCalNo.trim() + "' and removetype = '01' ";
//        String tMaxEdorNo = "";
//        tMaxEdorNo = tExe.getOneValue(tMaxSql);
//        if (tMaxEdorNo .equals("") || tMaxEdorNo == null) {
//          dealError("simpleRecover", "查询LATreeB表的最大的EdorNo失败！");
//          conn.rollback();
//          conn.close();
//          return false;
//        }
//
//        // 删除
//        String tDelSql = " delete from latreeb where agentcode = '"
//            + tAssessSch.getAgentCode().trim() + "' and (EdorNo = '"
//            + tMinEdorNo.trim() + "' or edorno = '"
//            + tMaxEdorNo.trim() + "')";
//        if (!tExe.execUpdateSQL(tDelSql)) {
//          dealError("simpleRecover", "删除LATreeB表失败！");
//          conn.rollback();
//          conn.close();
//          return false;
//        }
//      }
//      else {
//        tTreeBDB.setAgentCode(t_AgentCode.trim());
//        tTreeBDB.setIndexCalNo(tIndexCalNo.trim());
//        tTreeBDB.setRemoveType("01");
//        if (!tTreeBDB.deleteSQL()) {
//          dealError("agentRecover", "删除LATreeB失败！");
//          conn.rollback();
//          conn.close();
//          return false;
//        }
//      }
//
//      // 更新LAAgent：A02->A01:indueformDate = ""
//      if (updAgentFlag.trim().equals("T")) {
//        LAAgentDB tLAAgentDB = new LAAgentDB(conn);
//        tAgentSch.setInDueFormDate("");
//        tLAAgentDB.setSchema(tAgentSch);
//        if (!tLAAgentDB.update()) {
//          dealError("simpleRecover", tLAAgentDB.mErrors.getFirstError());
//          conn.rollback();
//          conn.close();
//          return false;
//        }
//      }
//      conn.commit();
//      conn.close();
//    }
//    catch (Exception ex) {
//      ex.printStackTrace();
//      try {
//        conn.rollback();
//        conn.close();
//      }
//      catch (Exception ep) {
//        ep.printStackTrace();
//        dealError("simpleRecover", ep.toString());
//        return false;
//      }
//      dealError("simpleRecover", ex.toString());
//      return false;
//    }
//    return true;
//  }

    /**
     * 同系列恢复
     * @param cInputData
     * @return
     */
    public boolean simpleRecover(VData cInputData)
    {
        GlobalInput tGI = new GlobalInput();
        LAAssessSchema tAssessSch = new LAAssessSchema();
        tGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        String tIndexCalNo = (String) cInputData.getObjectByObjectName("String",
                0);
        //蔡刚 2004-05-21添加，用于记录上一次归属时的转储号
        String tEdorNo = "";
        tAssessSch = (LAAssessSchema) cInputData.getObjectByObjectName(
                "LAAssessSchema", 0);

        if (tIndexCalNo == null || tIndexCalNo.equals("") || tAssessSch == null)
        {
            dealError("simpleRecover", "没有传入足够数据！");
            return false;
        }
        this.mIndexCalNo = tIndexCalNo;
        genDate();
        LAAssessDB tLAAssessDB = new LAAssessDB();
        tLAAssessDB.setAgentCode(tAssessSch.getAgentCode());
        tLAAssessDB.setIndexCalNo(tAssessSch.getIndexCalNo());
        if (!tLAAssessDB.getInfo())
        {
            dealError("simpleRecover",
                      "" + tLAAssessDB.getAgentCode() + "还未进行调整授权或者已完成恢复！");
            return false;
        }
        // 判断是否同系列职级恢复
        String t_AgentSeries1 = tAssessSch.getAgentSeries1().trim();
        String t_AgentSeries = tAssessSch.getAgentSeries().trim();
        if (!t_AgentSeries1.equals(t_AgentSeries))
        {
            dealError("simpleRecover", "非同系列职级恢复，请重新选择！");
            return false;
        }

        //判断是否第一次由A02、A03->A01:取LAAgent.IndueFormDate 和 LATree 的StartDate比较
        String t_AgentGrade = tAssessSch.getAgentGrade().trim();
        String t_AgentGrade1 = tAssessSch.getAgentGrade1().trim();
        String t_AgentCode = tAssessSch.getAgentCode().trim();
        String updAgentFlag = "";
        LAAgentSchema tAgentSch = new LAAgentSchema();
        LATreeSchema tTreeSch = new LATreeSchema();
        //处理从A02、A03降级到A01的情况，这种情况需要考虑InDueFormDate字段是否需要重新
        //置为空""
        if ((t_AgentGrade.equals("A02") || t_AgentGrade.equals("A03")) &&
            t_AgentGrade1.equals("A01"))
        {
            LAAgentDB tAgentDB = new LAAgentDB();
            tAgentDB.setAgentCode(t_AgentCode);
            if (!tAgentDB.getInfo())
            {
                dealError("simpleRecover", tAgentDB.mErrors.getFirstError());
                return false;
            }
            tAgentSch = tAgentDB.getSchema();
            String tIndueFormDate = tAgentSch.getInDueFormDate().trim();

            //取LATree的StartDate
            LATreeDB tTreeDB = new LATreeDB();
            tTreeDB.setAgentCode(t_AgentCode);
            if (!tTreeDB.getInfo())
            {
                dealError("simpleRecover", tTreeDB.mErrors.getFirstError());
                return false;
            }
            tTreeSch.setSchema(tTreeDB.getSchema());
            String tStartDate = tTreeSch.getStartDate().trim();
            //如果转正日期和现职级的起聘日期一致的话，说明上次变动是第一次由A01转正
            if (tIndueFormDate.equalsIgnoreCase(tStartDate))
            {
                updAgentFlag = "T";
            }
            else
            {
                updAgentFlag = "F";
            }
        }
        // caigang 2004-05-21 添加else括号中内容
        else
        {
            LATreeDB tTreeDB = new LATreeDB();
            tTreeDB.setAgentCode(t_AgentCode);
            if (!tTreeDB.getInfo())
            {
                dealError("simpleRecover", tTreeDB.mErrors.getFirstError());
                return false;
            }
            tTreeSch.setSchema(tTreeDB.getSchema());
        }

        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            dealError("agentDemote", "数据库连接失败！");
            return false;
        }

        Reflections rf = new Reflections();
        try
        {
            conn.setAutoCommit(false);

            //恢复latree表
            LATreeBDB tTreeBDB = new LATreeBDB(conn);
            LATreeBSet tTreeBSet = new LATreeBSet();
            LATreeBSchema tTreeBSchema = new LATreeBSchema();
            String tSql = "select * from latreeb where agentcode = '"
                          + t_AgentCode.trim() + "' and indexcalno = '"
                          + tIndexCalNo.trim() + "' and removetype = '01' ";
            System.out.println("--tSql--:" + tSql);
            tTreeBSet = tTreeBDB.executeQuery(tSql);
            int tCount = tTreeBSet.size();
            if (tCount == 0)
            {
                dealError("simpleRecover", "此代理人本次组织归属时在latreeb表中没有备份信息！");
                conn.rollback();
                conn.close();
                return false;
            }

            tSql = "select * from latreeb where agentcode = '"
                   + t_AgentCode.trim() + "' and indexcalno = '"
                   + tIndexCalNo.trim() + "' and removetype = '01' "
                   + "and edorno in (select min(edorno) from latreeb where "
                   + "agentcode = '" + t_AgentCode.trim()
                   + "' and indexcalno = '"
                   + tIndexCalNo.trim() + "' and removetype = '01' )";
            System.out.println("---tSql = " + tSql);
            tTreeBSet = tTreeBDB.executeQuery(tSql);
            tTreeBSchema.setSchema(tTreeBSet.get(1));
            //tNewTree存储的是当恢复以后被恢复的代理人的LATree的信息
            LATreeSchema tNewTree = new LATreeSchema();
            rf.transFields(tNewTree, tTreeBSchema);
            tNewTree.setAgentGroup(tTreeSch.getAgentGroup());
            tNewTree.setUpAgent(tTreeSch.getUpAgent());
            tNewTree.setMakeDate(tTreeSch.getMakeDate());
            tNewTree.setMakeTime(tTreeSch.getMakeTime());
            //若tCount>1说明本月组织归属除职级变动外，还涉及到机构的变动
            if (tCount > 1)
            {
                tNewTree.setAstartDate(tTreeSch.getAstartDate()); //考核起期
                tNewTree.setModifyDate(tTreeSch.getModifyDate());
                tNewTree.setModifyTime(tTreeSch.getModifyTime());
                tNewTree.setOperator(tTreeSch.getOperator());
            }
            else
            {
                //tCount=1 说明该代理人本次组织归属只做了职级变动
                tNewTree.setModifyDate(tTreeBSchema.getModifyDate2());
                tNewTree.setModifyTime(tTreeBSchema.getModifyTime2());
                tNewTree.setOperator(tTreeBSchema.getOperator2());
            }

            LATreeDB tTreeDB = new LATreeDB(conn);
            tTreeDB.setSchema(tNewTree);
            if (!tTreeDB.update())
            {
                dealError("simpleRecover", tTreeDB.mErrors.getFirstError());
                conn.rollback();
                conn.close();
                return false;
            }

            //删除因职级变动存入备份表的信息
            tTreeBDB.setAgentCode(t_AgentCode);
            tTreeBDB.setEdorNO(tTreeBSchema.getEdorNO());
            tTreeBDB.setIndexCalNo(tIndexCalNo);
            if (!tTreeBDB.deleteSQL())
            {
                dealError("simpleRecover", tTreeBDB.mErrors.getFirstError());
                conn.rollback();
                conn.close();
                return false;
            }

            ExeSQL tExe = new ExeSQL(conn);
            //如果tCount>1，需要将备份表中的记录的职级和职级系列修改为职级变动之前的状态
            if (tCount > 1)
            {
                tSql = "select * from latreeb where agentcode = '"
                       + t_AgentCode.trim() + "' and indexcalno = '"
                       + tIndexCalNo.trim() + "' and removetype = '01' ";
                tTreeBSet = tTreeBDB.executeQuery(tSql);
                LATreeBSet tNewSet = new LATreeBSet();
                LATreeBSchema tNewSch = new LATreeBSchema();
                int tCount1 = tTreeBSet.size();
                for (int i = 1; i <= tCount1; i++)
                {
                    tNewSch.setSchema(tTreeBSet.get(i));
                    String tAgentGroup = tNewSch.getAgentGroup();
                    String tUpAgent = tNewSch.getUpAgent();
//          String tMakeDate=tNewSch.getMakeDate() ;
//          String tMakeTime=tNewSch.getMakeTime();
                    rf.transFields(tNewSch, tNewTree);
                    tNewSch.setAgentGrade(tNewSch.getAgentGrade());
                    tNewSch.setAgentSeries(tNewSch.getAgentSeries());
                    tNewSch.setAgentGroup(tAgentGroup);
                    tNewSch.setUpAgent(tUpAgent);
//          tNewSch.setMakeDate(tMakeDate) ;
//          tNewSch.setMakeTime(tMakeTime);

                    tNewSet.add(tNewSch);
                }
                LATreeBDBSet tBDBSet = new LATreeBDBSet(conn);
                tBDBSet.set(tNewSet);
                if (!tBDBSet.update())
                {
                    dealError("simpleRecover", tBDBSet.mErrors.getFirstError());
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            //若代理人的首次由A01变为A02或A03，需要恢复，则需要将代理人的indueformdate置为空
            if (updAgentFlag.trim().equals("T"))
            {
                tSql =
                        "update laagent set indueformdate = '' where agentcode ='"
                        + t_AgentCode.trim() + "'";
                if (!tExe.execUpdateSQL(tSql))
                {
                    dealError("simpleRecover", tExe.mErrors.getFirstError());
                    conn.rollback();
                    conn.close();
                    return false;
                }

                if (tCount > 1)
                {
                    tSql =
                            "update laagentb set indueformdate = '' where agentcode ='"
                            + t_AgentCode.trim() + "' and indexcalno = '"
                            + tIndexCalNo.trim() + "' and edortype = '01'";
                    if (!tExe.execUpdateSQL(tSql))
                    {
                        dealError("simpleRecover", tExe.mErrors.getFirstError());
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
            }

            //恢复LATreeAccessory
            if (t_AgentGrade.trim().compareTo("A04") >= 0)
            {
                tSql = "update latreeaccessory set agentgrade = '"
                       + t_AgentGrade1.trim() + "' where agentcode = '"
                       + t_AgentCode.trim() + "' and agentgrade='" +
                       t_AgentGrade.trim() + "'";
                if (!tExe.execUpdateSQL(tSql))
                {
                    dealError("simpleRecover", tExe.mErrors.getFirstError());
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            //刷新LAAssess
            PubFun tPF = new PubFun();
            LAAssessDB tAssessDB = new LAAssessDB(conn);
            tAssessSch.setState("2");
            tAssessSch.setModifyDate(tPF.getCurrentDate());
            tAssessSch.setModifyTime(tPF.getCurrentTime());
            tAssessSch.setAgentGroupNew(tNewTree.getAgentGroup());
            tAssessDB.setSchema(tAssessSch);
            if (!tAssessDB.update())
            {
                dealError("simpleRecover", tAssessDB.mErrors.getFirstError());
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception ep)
            {
                ep.printStackTrace();
                dealError("simpleRecover", ep.toString());
                return false;
            }
            dealError("simpleRecover", ex.toString());
            return false;
        }
        return true;
    }

    //离职清退恢复
    public boolean departRecover(VData cInputData)
    {
        LAAssessSchema tAssessSch = new LAAssessSchema();
        GlobalInput tGI = new GlobalInput();
        Connection conn = DBConnPool.getConnection();
        try
        {
            conn.setAutoCommit(false);
            tGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
                    0);
            String tIndexCalNo = (String) cInputData.getObjectByObjectName(
                    "String", 0);
            String tAimBranchAttr = (String) cInputData.getObjectByObjectName(
                    "String",
                    1);
            tAssessSch = (LAAssessSchema) cInputData.getObjectByObjectName(
                    "LAAssessSchema", 0);
            tAimBranchAttr = tAimBranchAttr.trim();
            if ((tIndexCalNo == null || tIndexCalNo.trim().equals(""))
                || tAssessSch == null
                || (tAimBranchAttr == null || tAimBranchAttr.trim().equals("")))
            {
                dealError("departRecover", "没有传入足够数据！");
                return false;
            }
            this.mIndexCalNo = tIndexCalNo;
            genDate();

            //得到内部编码
            ExeSQL tExe = new ExeSQL();
            String tAimSql =
                    "select agentgroup from labranchgroup where branchattr = '"
                    + tAimBranchAttr + "'";
            String tAimGroup = "";
            tAimGroup = tExe.getOneValue(tAimSql);
            if (tAimGroup == null || tAimGroup.equals(""))
            {
                dealError("departRecover", "目标组不存在！");
                return false;
            }

            // 恢复LAAgent表
            String t_AgentCode = tAssessSch.getAgentCode().trim();
            LAAgentDB tDB = new LAAgentDB(conn);
            LAAgentSchema tSch = new LAAgentSchema();
            tDB.setAgentCode(t_AgentCode);
            if (!tDB.getInfo())
            {
                dealError("departRecover", tDB.mErrors.getFirstError());
                conn.rollback();
                conn.close();
                return false;

            }

            tSch = tDB.getSchema();
            String tAgentState = tSch.getAgentState();

            if (!tAgentState.equals("05"))
            {
                dealError("departRecover", "非考核清退情况，请重新选择！");
                return false;
            }

            LAAgentBDB tAgentBDB = new LAAgentBDB(conn);
            LAAgentBSet tAgentBSet = new LAAgentBSet();
            LAAgentBSchema tAgentBSch = new LAAgentBSchema();
            PubFun tPF = new PubFun();

            String tSql = "select * from laagentb where agentcode = '"
                          + tAssessSch.getAgentCode().trim()
                          + "' and indexcalno = '"
                          + tIndexCalNo.trim() + "' and agentgroup = '"
                          + tSch.getAgentGroup().trim() + "' and edortype='01'";
            tAgentBSet = tAgentBDB.executeQuery(tSql);
            tAgentBSch.setSchema(tAgentBSet.get(1));

            tSch.setAgentGroup(tAimGroup.trim());
            tSch.setBranchCode(tAimGroup.trim());
            tSch.setAgentState(tAgentBSch.getAgentState());
            tSch.setOutWorkDate(""); //OutWorkDate离职日期
            tSch.setModifyDate(tPF.getCurrentDate());
            tSch.setModifyTime(tPF.getCurrentTime());
            tSch.setOperator(tGI.Operator);

            // 恢复LATree表
            LATreeDB tTreeDB = new LATreeDB(conn);
            LATreeSchema tTreeSch = new LATreeSchema();
            tTreeDB.setAgentCode(tAssessSch.getAgentCode());
            if (!tTreeDB.getInfo())
            {
                dealError("departRecover", tDB.mErrors.getFirstError());
                return false;
            }
            String tOldAgentGroup = tTreeDB.getAgentGroup().trim();
            System.out.println("-----222--oldAgentGroup = " + tOldAgentGroup);
            //若组变动需用当前的LATree插入LATreeB表
            LATreeSchema tOldTree = new LATreeSchema();
            tTreeSch.setSchema(tTreeDB.getSchema());
            tOldTree.setSchema(tTreeSch);

            tTreeSch.setAssessType("0");
            tTreeSch.setState("0");
            tTreeSch.setAgentGroup(tAimGroup);
            tTreeSch.setModifyDate(tPF.getCurrentDate());
            tTreeSch.setModifyTime(tPF.getCurrentTime());
            tTreeSch.setOperator(tGI.Operator);
            if (tTreeSch.getIntroAgency() != null ||
                tTreeSch.getIntroAgency() != "")
            {
                tTreeSch.setIntroBreakFlag("0");
                tTreeSch.setIntroCommEnd("");
            }
            System.out.println("-Update-agentgroup: " +
                               tTreeSch.getAgentGroup().trim());

            String tChangeFlag = ""; //组变动标记：0－AgentGroup变、1－AgentGroup不变

            //判断组是否变化，如果有变动则需要刷新业绩
            if (!tOldAgentGroup.trim().equals(tAimGroup.trim()))
            {
                tChangeFlag = "0";
                DealBusinessData tDealData = new DealBusinessData();
                VData tInputData = new VData();
                tInputData.clear();
                tInputData.add(0, t_AgentCode.trim());
                tInputData.add(1, tAimGroup.trim());
                tInputData.add(2, "Y");
                tInputData.add(3, tAimBranchAttr);
                tInputData.add(4, tIndexCalNo);
                tInputData.add(5, tAimGroup);

                if (!tDealData.updateAgentGroup(tInputData, conn))
                {
                    dealError("departRecover",
                              "离职恢复时由于组变动而刷新业务数据失败！" +
                              tDealData.mErrors.getFirstError());
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            else
            {
                tChangeFlag = "1";
                LABranchGroupDB tOldAttrDB = new LABranchGroupDB();
                tOldAttrDB.setAgentGroup(tOldAgentGroup);
                if (!tOldAttrDB.getInfo())
                {
                    dealError("departRecover", "离职恢复时判断外部展业机构编码查询失败！");
                    conn.rollback();
                    conn.close();
                    return false;
                }
                String tOldAttr = tOldAttrDB.getBranchAttr().trim();
                if (!tOldAttr.trim().equals(tAimBranchAttr.trim()))
                {
                    if (!updLACommision(tAimBranchAttr,
                                        tAssessSch.getAgentCode().trim(),
                                        conn))
                    {
                        conn.rollback();
                    }
                    conn.close();
                    return false;
                }
            }

            tDB.setSchema(tSch);
            if (!tDB.update())
            {
                dealError("departRecover",
                          "刷新LAAgent表失败！" + tDB.mErrors.getFirstError());
                conn.rollback();
                conn.close();
                return false;
            }

            if (tChangeFlag.trim().equals("1"))
            {
                tAgentBDB.setAgentCode(tAgentBSch.getAgentCode());
                tAgentBDB.setAgentGroup(tAgentBSch.getAgentGroup());
                tAgentBDB.setIndexCalNo(tIndexCalNo);
                tAgentBDB.setEdorType("01");
                if (!tAgentBDB.deleteSQL())
                {
                    dealError("departRecover",
                              "删除LAAgentB表失败！" +
                              tAgentBDB.mErrors.getFirstError());
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            tTreeDB.setSchema(tTreeSch);
            if (!tTreeDB.update())
            {
                dealError("departRecover",
                          "刷新LATree表失败！" + tTreeDB.mErrors.getFirstError());
                conn.rollback();
                conn.close();
                return false;
            }

            //若AgentGroup改变，则需要在LATreeB中插入一条，以保证代理人的轨迹完整
            if (tChangeFlag.trim().equals("0"))
            {
                Reflections rf = new Reflections();
                LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                LATreeBDB tLATreeBDB = new LATreeBDB(conn);
                rf.transFields(tLATreeBSchema, tOldTree);

                tLATreeBSchema.setRemoveType("01");
                tLATreeBSchema.setModifyDate(tPF.getCurrentDate());
                tLATreeBSchema.setModifyTime(tPF.getCurrentTime());
                tLATreeBSchema.setModifyDate2(tOldTree.getModifyDate());
                tLATreeBSchema.setModifyTime2(tOldTree.getModifyTime());
                tLATreeBSchema.setMakeDate(tPF.getCurrentDate());
                tLATreeBSchema.setMakeTime(tPF.getCurrentTime());
                tLATreeBSchema.setMakeDate2(tOldTree.getMakeDate());
                tLATreeBSchema.setMakeTime2(tOldTree.getMakeTime());
                tLATreeBSchema.setOperator2(tOldTree.getOperator());
                tLATreeBSchema.setOperator(this.mGI.Operator);
                tLATreeBSchema.setEdorNO(tAgentBSch.getEdorNo());
                tLATreeBSchema.setIndexCalNo(tIndexCalNo);

                tLATreeBDB.setSchema(tLATreeBSchema);
                if (!tLATreeBDB.insert())
                {
                    dealError("insLATreeB", "插入代理人行政信息转储表失败！");
                    return false;
                }
            }

            //删除离职信息
            LADimissionDB tDimDB = new LADimissionDB(conn);
            tDimDB.setAgentCode(tAssessSch.getAgentCode());
            tDimDB.setDepartRsn("考核清退");
            if (!tDimDB.deleteSQL())
            {
                dealError("departRecover",
                          "删除LADimission表失败！" + tDimDB.mErrors.getFirstError());
                conn.rollback();
                conn.close();
                return false;
            }

            // 刷新LAAssess
            LAAssessDB tAssDB = new LAAssessDB(conn);
            tAssDB.setAgentCode(tAssessSch.getAgentCode());
            tAssDB.setIndexCalNo(tIndexCalNo);
            if (!tAssDB.getInfo())
            {
                dealError("agentRecover", "查询LAAssess信息失败！");
                conn.rollback();
                conn.close();
                return false;
            }

            LAAssessSchema tAssSch = new LAAssessSchema();
            tAssSch.setSchema(tAssDB.getSchema());
            tAssSch.setAgentGroupNew(tAimGroup.trim());
            tAssSch.setState("2");
            tAssSch.setModifyDate(tPF.getCurrentDate());
            tAssSch.setModifyTime(tPF.getCurrentTime());
            tAssSch.setOperator(this.mGI.Operator);

            tAssDB.setSchema(tAssSch);
            if (!tAssDB.update())
            {
                dealError("agentRecover", "刷新LAAssess信息失败！");
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("departRecover", ex.toString());
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception ep)
            {
                ep.printStackTrace();
                dealError("departRecover", ep.toString());
                return false;
            }
            return false;
        }
        return true;
    }

    private void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "AssessOrgRecover";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        this.mErrors.addOneError(tError);
    }

    private String[] genDate()
    {
        String[] tMonDate = new String[2];
        PubFun tPF = new PubFun();

        String tYear = this.mIndexCalNo.trim().substring(0, 4);
        String tMon = this.mIndexCalNo.trim().substring(4, 6);

        //计算旧职级的止聘日期
        String tOldDate = tYear + "-" + tMon + "-01";
        tMonDate = tPF.calFLDate(tOldDate);
        String tOldDay = tMonDate[1];
        System.out.println("---tOldDay---" + tOldDay);
        //计算新职级的起聘日期或转正日期
        if (tMon.equals("12"))
        {
            tYear = Integer.toString(Integer.parseInt(tYear) + 1);
            tMon = "01";
        }
        else
        {
            tMon = Integer.toString(Integer.parseInt(tMon) + 1);
        }
        int tLength = tMon.length();
        if (tLength == 1)
        {
            tMon = "0" + tMon;
        }
        String tDate = tYear.trim() + "-" + tMon.trim() + "-" + "01";
        System.out.println("---tNewDay---" + tDate);

        this.MonDate[0] = tDate;
        this.MonDate[1] = tOldDay;
        return tMonDate;
    }

    private boolean updLACommision(String tNewAttr, String tAgentCode,
                                   Connection conn)
    {
        PubFun tPF = new PubFun();

        AgentPubFun tAPF = new AgentPubFun();
        String tCheckFlag = tAPF.AdjustCommCheck(tAgentCode.trim(),
                                                 this.MonDate[0]);
        if (!tCheckFlag.trim().equals("00"))
        {
            dealError("updLACommision", tCheckFlag.trim());
            return false;
        }

        String tSql = "update lacommision set branchattr = '"
                      + tNewAttr.trim() + "' where agentcode = '"
                      + tAgentCode.trim() + "' and (caldate >= '"
                      + this.MonDate[0] + "' or caldate is null)";

        ExeSQL tExe = new ExeSQL(conn);
        if (!tExe.execUpdateSQL(tSql))
        {
            dealError("updLACommision",
                      "刷新LACommision表的外部展业机构编码失败!" + tExe.mErrors.getFirstError());
            return false;
        }
        return true;
    }
}
