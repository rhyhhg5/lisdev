package com.sinosoft.lis.agentassess;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LAAssessAccessorySet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LAAgentGradeSchema;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.db.LABranchLevelDB;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.schema.LABranchLevelSchema;
import com.sinosoft.lis.agentcalculate.CalFormDrawRate;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.utility.TransferData;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author caigang
 * @version 1.0
 */
public class OLAAscriptInDiffSeriesBL {
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessAccessorySet mLAAssessAccessorySet = new
            LAAssessAccessorySet();
    private LAAssessAccessorySchema mLAAssessAccessorySchema = new
            LAAssessAccessorySchema();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LABranchGroupBSet mLABranchGroupBSet = new LABranchGroupBSet();
    private LADimissionSet mLADimissionSet = new LADimissionSet();
    private VData mInputData = new VData();
    private VData mOutputData = new VData();
    private MMap mMap = new MMap();
    private String mOperate = "";
    private String mEdorType = "31";
    private String tEdorNo = "";
    private String mAscriptDate = "";
    private String mIndexCalNo = "";
    private String oriManager = "";

    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    public OLAAscriptInDiffSeriesBL() {
    }

    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLAAssessAccessorySet = (LAAssessAccessorySet) cInputData.
                                getObjectByObjectName("LAAssessAccessorySet", 0);
        if (mLAAssessAccessorySet == null || mAscriptDate == null) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "未得到足够数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean check() {
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("cOperate:" + cOperate);
        mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!check()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mOutputData, "")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean dealData() {
        for (int i = 1; i <= mLAAssessAccessorySet.size(); i++) {
            mLAAssessAccessorySchema = new
                                       LAAssessAccessorySchema();
            mLAAssessAccessorySchema = mLAAssessAccessorySet.get(i);
            mIndexCalNo = mLAAssessAccessorySchema.getIndexCalNo();
            String tAgentGrade1 = mLAAssessAccessorySchema.getAgentGrade1();
            String tAgentGrade = mLAAssessAccessorySchema.getAgentGrade();
            LAAgentGradeDB tLAAgentGrade1DB = new LAAgentGradeDB();
            LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
            tLAAgentGrade1DB.setGradeCode(tAgentGrade1);
            if (!tLAAgentGrade1DB.getInfo()) {
                this.mErrors.copyAllErrors(tLAAgentGrade1DB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptInDiffSeriesBL";
                tError.functionName = "dealData";
                tError.errorMessage = "未查询到" + tAgentGrade1 + "的描述信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLAAgentGradeDB.setGradeCode(tAgentGrade);
            if (!tLAAgentGradeDB.getInfo()) {
                this.mErrors.copyAllErrors(tLAAgentGradeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptInDiffSeriesBL";
                tError.functionName = "dealData";
                tError.errorMessage = "未查询到" + tAgentGrade + "的描述信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLAAgentGrade1DB.getGradeProperty2().equals("0")) {
                if (!this.ascriptToBusSeries(mLAAssessAccessorySchema)) {
                    return false;
                }
                if (!prepareOutputData()) {
                    CError tError = new CError();
                    tError.moduleName = "LAAscriptInDiffSeriesBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "往PubSubmit准备数据时出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                PubSubmit tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAAscriptInDiffSeriesBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "PubSubmit保存数据时出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                mLATreeSet = new LATreeSet();
                mLATreeBSet = new LATreeBSet();
                mLAAgentSet = new LAAgentSet();
                mLAAgentBSet = new LAAgentBSet();
                mLADimissionSet = new LADimissionSet();
                mLACommisionSet = new LACommisionSet();
                mLAAssessAccessorySchema=new LAAssessAccessorySchema();
                mLABranchGroupSet = new LABranchGroupSet();
                mLABranchGroupBSet = new LABranchGroupBSet();
            } else {
                if (!this.ascriptToMngSeries(mLAAssessAccessorySchema)) {
                    return false;
                }
                if (!prepareOutputData()) {
                    CError tError = new CError();
                    tError.moduleName = "LAAscriptInDiffSeriesBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "往PubSubmit准备数据时出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                PubSubmit tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, "")) {
                    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAAscriptInDiffSeriesBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "PubSubmit保存数据时出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                mLATreeSet = new LATreeSet();
                mLATreeBSet = new LATreeBSet();
                mLAAgentSet = new LAAgentSet();
                mLAAgentBSet = new LAAgentBSet();
                mLADimissionSet = new LADimissionSet();
                mLACommisionSet = new LACommisionSet();
                mLAAssessAccessorySchema=new LAAssessAccessorySchema();
                mLABranchGroupSet = new LABranchGroupSet();
                mLABranchGroupBSet = new LABranchGroupBSet();
            }
        }
        return true;
    }

    private boolean prepareOutputData() {
        mInputData = new VData();
        mMap = new MMap();
        mMap.put(mLATreeSet, "UPDATE");
        mMap.put(mLATreeBSet, "INSERT");
        mMap.put(mLAAgentSet, "UPDATE");
        mMap.put(mLAAgentBSet, "INSERT");
        mMap.put(mLACommisionSet, "UPDATE");
        mMap.put(mLADimissionSet, "INSERT");
        mMap.put(mLAAssessAccessorySchema, "UPDATE");
        mMap.put(mLABranchGroupSet, "UPDATE");
        mMap.put(mLABranchGroupBSet, "INSERT");
        mInputData.add(mMap);
        return true;
    }

    /******************************************************************************/
    private boolean ascriptToMngSeries(LAAssessAccessorySchema
                                       cLAAssessAccessorySchema) {

        tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        if(cLAAssessAccessorySchema.getStandAssessFlag().equals("1"))
        {
            mEdorType="01";//考核 修改
        }
        else if(cLAAssessAccessorySchema.getStandAssessFlag().equals("0"))
        {
            mEdorType="31";//手动 页面 修改
            }
        String mAscriptDate = cLAAssessAccessorySchema.getIndexCalNo() + "01";
        mAscriptDate = AgentPubFun.formatDate(mAscriptDate, "yyyy-MM-dd");
        mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);
        LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
        tLAAssessAccessoryDB.setAgentCode(cLAAssessAccessorySchema.getAgentCode());
        tLAAssessAccessoryDB.setIndexCalNo(cLAAssessAccessorySchema.
                                           getIndexCalNo());
        tLAAssessAccessoryDB.setAssessType(cLAAssessAccessorySchema.
                                           getAssessType());
        if (!tLAAssessAccessoryDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAssessAccessoryDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "dealData";
            tError.errorMessage = "未查询到" +
                                  cLAAssessAccessorySchema.getAgentCode() +
                                  "在" +
                                  cLAAssessAccessorySchema.getIndexCalNo() +
                                  ",考核类型为" +
                                  cLAAssessAccessorySchema.getAssessType() +
                                  "的考核结果记录！!";
            this.mErrors.addOneError(tError);
            return false;
        }
        cLAAssessAccessorySchema = tLAAssessAccessoryDB.getSchema();
        String tAgentGroupNew = cLAAssessAccessorySchema.getAgentGroupNew();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tAgentGroupNew);
        tLABranchGroupDB.setBranchType(cLAAssessAccessorySchema.getBranchType());
        tLABranchGroupDB.setBranchType2(cLAAssessAccessorySchema.getBranchType2());
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "未查询目标销售单位信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = tLABranchGroupSet.get(1);
        String toBranchAttr=tLABranchGroupSchema.getBranchAttr();
        String toBranchseries=tLABranchGroupSchema.getBranchSeries();
        String toAgentGroup=tLABranchGroupSchema.getAgentGroup();
        if (tLABranchGroupSchema.getEndFlag() != null &&
            tLABranchGroupSchema.getEndFlag().equals("Y")) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "目标销售单位已经停业!";
            this.mErrors.addOneError(tError);
            return false;
        }
        cLAAssessAccessorySchema.setAgentGroupNew(tLABranchGroupSchema.
                                                  getAgentGroup());
        String tAgentCode = cLAAssessAccessorySchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "未查到业务员" + tLAAgentDB.getAgentCode() +
                                  "的基础信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(tAgentCode);
        if (!tLATreeDB.getInfo()) {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "未查到业务员" + tLATreeDB.getAgentCode() +
                                  "的行政信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        tAgentGroupNew = cLAAssessAccessorySchema.getAgentGroupNew();
        LABranchGroupSchema tDirBranchGroupSchema = new LABranchGroupSchema();
        tDirBranchGroupSchema = getDirTeam(tAgentGroupNew);
        if (tDirBranchGroupSchema == null) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "查询目标机构的直辖机构返回空!";
            this.mErrors.addOneError(tError);
            return false;

        }
        Reflections tReflections = new Reflections();
        LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
        tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
//        tLABranchGroupBSchema.setEdorNo(tEdorNo) ;
//        tLABranchGroupBSchema.setEdorType("01");
//        tLABranchGroupBSchema.setMakeDate2(tLABranchGroupBSchema.getMakeDate() );
//        tLABranchGroupBSchema.setMakeTime2(tLABranchGroupBSchema.getMakeTime());
//        tLABranchGroupBSchema.setModifyDate2(tLABranchGroupBSchema.getModifyDate() );
//        tLABranchGroupBSchema.setModifyTime2(tLABranchGroupBSchema.getModifyTime());
//        tLABranchGroupBSchema.setMakeDate(currentDate) ;
//        tLABranchGroupBSchema.setMakeTime(currentTime) ;
//        tLABranchGroupBSchema.setModifyDate(currentDate);
//        tLABranchGroupBSchema.setModifyTime(currentTime);
//        tLABranchGroupBSchema.setOperator2(tLABranchGroupBSchema.getOperator() );
//        tLABranchGroupBSchema.setOperator(mGlobalInput.Operator ) ;
//        this.mLABranchGroupBSet.add(tLABranchGroupBSchema) ;
        //原机构的主管代码，如果为空，则要置新机构所有人员的上级人员代码和主管代码
        //否则  新机构所有人员的上级人员代码和主管代码不变
         oriManager=tLABranchGroupSchema.getBranchManager();
        if (tLABranchGroupSchema.getBranchManager()==null || tLABranchGroupSchema.getBranchManager().equals("") )
         {
             tLABranchGroupSchema.setBranchManager(tLAAgentDB.getAgentCode());
             tLABranchGroupSchema.setBranchManagerName(tLAAgentDB.getName());
         }
         else
         {//如果机构主管是 B01 职级,并且 异动人员的 目标职级不是 B01(即  >B01 ),则应该任命大职级为主管
             LATreeDB tLAtreeDB= new LATreeDB();
             tLAtreeDB.setAgentCode(oriManager);
             tLAtreeDB.getInfo();
            if ( tLAtreeDB.getAgentGrade().equals("B01") && !cLAAssessAccessorySchema.getAgentGrade1().equals("B01"))
            {
             tLABranchGroupSchema.setBranchManager(tLAAgentDB.getAgentCode());
             tLABranchGroupSchema.setBranchManagerName(tLAAgentDB.getName());
            }
         }
        mAscriptDate = cLAAssessAccessorySchema.getIndexCalNo() + "01";
        mAscriptDate = AgentPubFun.formatDate(mAscriptDate, "yyyy-MM-dd");
        mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);
        tLABranchGroupSchema.setAStartDate(mAscriptDate);
        this.mLABranchGroupSet.add(tLABranchGroupSchema);
        tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = "select * from labranchgroup where branchmanager='" +
                           mLAAssessAccessorySchema.getAgentCode() +
                           "' and (endflag is null or endflag<>'Y')";
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(branchSQL);
        for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
            LABranchGroupSchema ttLABranchGroupSchema = new LABranchGroupSchema();
            ttLABranchGroupSchema = tLABranchGroupSet.get(i);

            tLABranchGroupBSchema = new LABranchGroupBSchema();
            tReflections.transFields(tLABranchGroupBSchema,
                                     ttLABranchGroupSchema);
            tLABranchGroupBSchema.setEdorNo(tEdorNo);
            tLABranchGroupBSchema.setEdorType("01");
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupBSchema.
                                               getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupBSchema.
                                               getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupBSchema.
                                                 getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupBSchema.
                                                 getModifyTime());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);

            tLABranchGroupBSchema.setOperator2(tLABranchGroupBSchema.
                                               getOperator());
            tLABranchGroupBSchema.setOperator(mGlobalInput.Operator);
            this.mLABranchGroupBSet.add(tLABranchGroupBSchema);
            ttLABranchGroupSchema.setBranchManager("");
            ttLABranchGroupSchema.setBranchManagerName("");
            mAscriptDate = cLAAssessAccessorySchema.getIndexCalNo() + "01";
            mAscriptDate = AgentPubFun.formatDate(mAscriptDate, "yyyy-MM-dd");
            mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);
            ttLABranchGroupSchema.setAStartDate(mAscriptDate);
            this.mLABranchGroupSet.add(ttLABranchGroupSchema);
        }

        String sql = "select * from lawage where agentcode='" +
                     cLAAssessAccessorySchema.getAgentCode() + "'";

        LAWageDB tLAWageDB = new LAWageDB();
        LAWageSet tLAWageSet = new LAWageSet();
        tLAWageSet = tLAWageDB.executeQuery(sql);
        if (tLAWageSet == null || tLAWageSet.size() == 0) {
            System.out.println("----没有计算过佣金，调整日期自动改为该业务员的入司日期！");
            tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(cLAAssessAccessorySchema.getAgentCode());
            tLAAgentDB.getInfo();
            mAscriptDate = tLAAgentDB.getEmployDate();

        } else {

            mAscriptDate = cLAAssessAccessorySchema.getIndexCalNo() + "01";
            mAscriptDate = AgentPubFun.formatDate(mAscriptDate, "yyyy-MM-dd");
            mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);

        }


        String tBranchCode = tDirBranchGroupSchema.getAgentGroup();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
        tLAAgentSchema = tLAAgentDB.getSchema();

        tReflections = new Reflections();

        LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeBSchema tLATreeBSchema = new LATreeBSchema();
        tLATreeSchema = tLATreeDB.getSchema();
        tReflections.transFields(tLATreeBSchema, tLATreeSchema);
        tLATreeBSchema.setEdorNO(tEdorNo);
        tLATreeBSchema.setRemoveType(mEdorType);
        tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
        tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
        tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
        tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
        tLATreeBSchema.setMakeDate(currentDate);
        tLATreeBSchema.setMakeTime(currentTime);
        tLATreeBSchema.setModifyDate(currentDate);
        tLATreeBSchema.setModifyTime(currentTime);
        tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
        tLATreeBSchema.setOperator(mGlobalInput.Operator);
        tLATreeBSchema.setIndexCalNo(cLAAssessAccessorySchema.getIndexCalNo());
        tLATreeSchema.setAgentGroup(tAgentGroupNew);
        System.out.println("latreeqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"+tAgentGroupNew);
        tLATreeSchema.setBranchCode(tBranchCode);
        tLATreeSchema.setModifyDate(currentDate);
        tLATreeSchema.setModifyTime(currentTime);
        tLATreeSchema.setOperator(mGlobalInput.Operator);
        tLATreeSchema.setUpAgent(tLABranchGroupSchema.getBranchManager());
        tLATreeSchema.setAstartDate(mAscriptDate);
        if (tLABranchGroupSchema.getUpBranch() != null &&
            !tLABranchGroupSchema.getUpBranch().equals("")) {
            LABranchGroupSchema tUpBranchGroupSchema = new LABranchGroupSchema();
            LABranchGroupDB tUpBranchGroupDB = new LABranchGroupDB();
            tUpBranchGroupDB.setAgentGroup(tLABranchGroupSchema.getUpBranch());
            if (!tUpBranchGroupDB.getInfo()) {
                this.mErrors.copyAllErrors(tUpBranchGroupDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptInDiffSeriesBL";
                tError.functionName = "ascriptToBusSeries";
                tError.errorMessage = "目标机构的上级机构信息错误,未找到相对应的机构与之对应!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (oriManager==null || oriManager.equals(""))
            {//如果新机构没有主管,则说明异动人员为主管,他的上级代理人为上级机构的主管

                tLATreeSchema.setUpAgent(tUpBranchGroupDB.getBranchManager());
            }
            else
            {// 否则 ,如果新机构主管为B01 职级,异动人员大于B01 ,上级代理人为上级机构主管
                //否则 ,他的上级代理人为新机构主管
                LATreeDB tLAtreeDB = new LATreeDB();
                tLAtreeDB.setAgentCode(oriManager);
                tLAtreeDB.getInfo();
                if (tLAtreeDB.getAgentGrade().equals("B01") && !cLAAssessAccessorySchema.getAgentGrade1().equals("B01")) {
                    tLATreeSchema.setUpAgent(tUpBranchGroupDB.getBranchManager());
                }
            }
        }


            String tAgentGrade = cLAAssessAccessorySchema.getAgentGrade();
            String tAgentSeries = AgentPubFun.getAgentSeries(tAgentGrade);
            tLATreeSchema.setAgentLastGrade(tAgentGrade);
            tLATreeSchema.setAgentLastSeries(tAgentSeries);
            tLATreeSchema.setOldStartDate(tLATreeSchema.getStartDate());
            tLATreeSchema.setOldEndDate(PubFun.calDate(mAscriptDate, -1,
                    "D", null));

        tLATreeSchema.setStartDate(mAscriptDate);
        tLATreeSchema.setAgentGrade(cLAAssessAccessorySchema.getAgentGrade1());
        tLATreeSchema.setAgentSeries(AgentPubFun.getAgentSeries(
                cLAAssessAccessorySchema.getAgentGrade1()));

        tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
        tLAAgentBSchema.setEdorNo(tEdorNo);
        tLAAgentBSchema.setEdorType(mEdorType);
        tLAAgentBSchema.setMakeDate(currentDate);
        tLAAgentBSchema.setMakeTime(currentTime);
        tLAAgentBSchema.setModifyDate(currentDate);
        tLAAgentBSchema.setModifyTime(currentTime);
        tLAAgentBSchema.setOperator(mGlobalInput.Operator);
        tLAAgentBSchema.setIndexCalNo(cLAAssessAccessorySchema.
                                      getIndexCalNo());
        this.mLAAgentBSet.add(tLAAgentBSchema);
        tLAAgentSchema.setAgentGroup(tAgentGroupNew);
        tLAAgentSchema.setBranchCode(tBranchCode);
        tLAAgentSchema.setModifyDate(currentDate);
        tLAAgentSchema.setModifyTime(currentTime);
        tLAAgentSchema.setOperator(mGlobalInput.Operator);
        this.mLAAgentSet.add(tLAAgentSchema);
        this.mLATreeSet.add(tLATreeSchema);
        this.mLATreeBSet.add(tLATreeBSchema);
        System.out.println("setDirInst Begin!");
        //设置下属人员

        if (!setDirInst(tLATreeSchema.getAgentGroup(),
                        tLATreeSchema.getAgentCode())) {
              return false;
        }
        System.out.println("setDirInst End!");
        cLAAssessAccessorySchema.setModifyDate(currentDate);
        cLAAssessAccessorySchema.setModifyTime(currentTime);
        cLAAssessAccessorySchema.setOperator(mGlobalInput.Operator);
        cLAAssessAccessorySchema.setState("2");
        tAgentGrade = cLAAssessAccessorySchema.getAgentGrade();
        String tAgentGrade1 = cLAAssessAccessorySchema.getAgentGrade1();
        tAgentSeries = AgentPubFun.getAgentSeries(tAgentGrade);
        String tAgentSeries1 = AgentPubFun.getAgentSeries(tAgentGrade1);
        PubCheckField checkField1 = new PubCheckField();
        VData cInputData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("branchtype",
                                      tLABranchGroupSchema.getBranchType());
        tTransferData.setNameAndValue("branchtype2",
                                      tLABranchGroupSchema.getBranchType2());
        tTransferData.setNameAndValue("branchlevel",
                                      tLABranchGroupSchema.getBranchLevel().
                                      trim());
        tTransferData.setNameAndValue("agentseries",
                                      tAgentSeries1);

        if (tAgentSeries == null || tAgentSeries.equals("")) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "获取职级" + tAgentGrade + "所对应的代理人系列返回为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tAgentSeries1 == null || tAgentSeries1.equals("")) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "获取职级" + tAgentGrade1 + "所对应的代理人系列返回为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tAgentSeries.equals(tAgentSeries1)) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "本模块只能处理跨系列的职级的异动!";
            this.mErrors.addOneError(tError);
            return false;
        }
        cLAAssessAccessorySchema.setAgentSeries(tAgentSeries);
        cLAAssessAccessorySchema.setAgentSeries1(tAgentSeries1);




        LAAgentGradeDB tLAAgentGradelastDB = new LAAgentGradeDB();
        tLAAgentGradelastDB.setGradeCode(cLAAssessAccessorySchema.getAgentGrade());
        if (!tLAAgentGradelastDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentGradelastDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "dealData";
            tError.errorMessage = "未查询到" +
                                  cLAAssessAccessorySchema.getAgentGrade() +
                                  "的描述信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
        tLAAgentGradeDB.setGradeCode(cLAAssessAccessorySchema.getAgentGrade1());
        if (!tLAAgentGradeDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentGradeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "dealData";
            tError.errorMessage = "未查询到" +
                                  cLAAssessAccessorySchema.getAgentGrade1() +
                                  "的描述信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //原职级为见习
        if ( tLAAgentGradelastDB.getGradeProperty4().equals("0") ){
            sql = "select * from lacommision where agentcode='" +
                  cLAAssessAccessorySchema.getAgentCode() +
                  "' and (caldate is null or caldate>='" + mAscriptDate + "')";
            LACommisionDB tLACommisionDB = new LACommisionDB();
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = tLACommisionDB.executeQuery(sql);
            System.out.println("tLACommisionDB!!!!!!!!!!!!!!!"+tLACommisionSet.size());
            CalFormDrawRate tCalFormDrawRate = new CalFormDrawRate();
            VData temp = new VData();
            temp.clear();
            temp.add(tLACommisionSet);
            temp.add(cLAAssessAccessorySchema.getAgentGrade1()); //传入的是一个AgentGrade，提数计算的时候传空，归属的时候传入的是新的职级
            if (!tCalFormDrawRate.submitData(temp, CalFormDrawRate.CALFLAG)) {
                this.mErrors.copyAllErrors(tCalFormDrawRate.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptInDiffSeriesBL";
                tError.functionName = "ascriptToBusSeries";
                tError.errorMessage = "打折处理的时候出错！";
                this.mErrors.addOneError(tError);
                return false;
            }

            mLACommisionSet.clear();
            mLACommisionSet.set((LACommisionSet) tCalFormDrawRate.getResult().
                                getObjectByObjectName("LACommisionSet", 0));
        }
        System.out.println("mLACommisionSet!!!!!!!!!!!!!!!"+mLACommisionSet.size());
        if ( !mLAAssessAccessorySchema.getAgentGroup().equals
               ( mLAAssessAccessorySchema.getAgentGroupNew()))
        {//如果销售单位发生变化，则业绩重新归属到新的团队
            if (mLACommisionSet.size()>0)
            {//如果目标职级为见习，并且归属日后有业绩，则mLACommisionSet.size>0
                for (int k = 1; k <= mLACommisionSet.size(); k++) {
                     mLACommisionSet.get(k).setAgentGroup(toAgentGroup);
                     mLACommisionSet.get(k).setBranchCode(toAgentGroup);
                     mLACommisionSet.get(k).setBranchAttr(toBranchAttr);
                     mLACommisionSet.get(k).setBranchSeries(toBranchseries);
                }
            }
            else
            {
            sql = "select * from lacommision where agentcode='" +
                  cLAAssessAccessorySchema.getAgentCode() +
                  "' and (caldate is null or caldate>='" + mAscriptDate + "')";
            LACommisionDB tLACommisionDB = new LACommisionDB();
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = tLACommisionDB.executeQuery(sql);
            System.out.println("tLACommisionSet!!!!!!!!!!!!!!!"+sql);
            for (int k = 1; k <= tLACommisionSet.size(); k++) {
                    LACommisionSchema tLACommisionSchema = new
                            LACommisionSchema();
                    tLACommisionSchema = tLACommisionSet.get(k);
                    tLACommisionSchema.setAgentGroup(toAgentGroup);
                    tLACommisionSchema.setBranchCode(toAgentGroup);
                    tLACommisionSchema.setBranchAttr(toBranchAttr);
                    tLACommisionSchema.setBranchSeries(toBranchseries);
                    mLACommisionSet.add(tLACommisionSchema);
              }
            }
        }
        return true;

    }

    private boolean ascriptToBusSeries(LAAssessAccessorySchema
                                       cLAAssessAccessorySchema) {
        String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        if(cLAAssessAccessorySchema.getStandAssessFlag().equals("1"))
        {
            mEdorType="01";//考核 修改
        }
        else if(cLAAssessAccessorySchema.getStandAssessFlag().equals("0"))
        {
            mEdorType="31";//手动 页面 修改
            }
        String mAscriptDate = cLAAssessAccessorySchema.getIndexCalNo() + "01";
        mAscriptDate = AgentPubFun.formatDate(mAscriptDate, "yyyy-MM-dd");
        mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);
        LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
        tLAAssessAccessoryDB.setAgentCode(cLAAssessAccessorySchema.getAgentCode());
        tLAAssessAccessoryDB.setIndexCalNo(cLAAssessAccessorySchema.
                                           getIndexCalNo());
        tLAAssessAccessoryDB.setAssessType(cLAAssessAccessorySchema.
                                           getAssessType());
        if (!tLAAssessAccessoryDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAssessAccessoryDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "未查询到" +
                                  cLAAssessAccessorySchema.getAgentCode() +
                                  "在" +
                                  cLAAssessAccessorySchema.getIndexCalNo() +
                                  ",考核类型为" +
                                  cLAAssessAccessorySchema.getAssessType() +
                                  "的考核结果记录！!";
            this.mErrors.addOneError(tError);
            return false;
        }
        cLAAssessAccessorySchema = tLAAssessAccessoryDB.getSchema();
        String tBranchAttr = cLAAssessAccessorySchema.getBranchAttr();
        String tAgentGroupNew = cLAAssessAccessorySchema.getAgentGroupNew();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tAgentGroupNew);
//        tLABranchGroupDB.setBranchAttr(tBranchAttr);
        tLABranchGroupDB.setBranchType(cLAAssessAccessorySchema.getBranchType());
        tLABranchGroupDB.setBranchType2(cLAAssessAccessorySchema.getBranchType2());
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "未查询到目标销售单位的信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = tLABranchGroupSet.get(1);
        String toBranchAttr=tLABranchGroupSchema.getBranchAttr();//目标机构编码
        String toAgentGroup=tAgentGroupNew;
        String toBranchseries=tLABranchGroupSchema.getBranchSeries();
        if (tLABranchGroupSchema.getEndFlag() != null &&
            tLABranchGroupSchema.getEndFlag().equals("Y")) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "目标销售单位已经停业!";
            this.mErrors.addOneError(tError);
            return false;
        }
        cLAAssessAccessorySchema.setAgentGroupNew(tLABranchGroupSchema.
                                                  getAgentGroup());
        String tBranchLevel = tLABranchGroupSchema.getBranchLevel();
        LABranchLevelDB tLABranchLevelDB = new LABranchLevelDB();
        tLABranchLevelDB.setBranchLevelCode(tBranchLevel);
        tLABranchLevelDB.setBranchType(cLAAssessAccessorySchema.getBranchType());
        tLABranchLevelDB.setBranchType2(cLAAssessAccessorySchema.getBranchType2());
        tLABranchLevelDB.getInfo();
//        String sql =
//                "select branchlevelid,branchlevelname from labranchlevel where branchtype='" +
//                cLAAssessAccessorySchema.getBranchType() +
//                "' and branchtype2='" + cLAAssessAccessorySchema.getBranchType2() +
//                "' and branchlevelid=(select min(branchlevelid) where branchtype='" +
//                cLAAssessAccessorySchema.getBranchType() +
//                "' and branchtype2='" +
//                cLAAssessAccessorySchema.getBranchType2() +
//                "')";
        String sql =
                "select branchlevelid,branchlevelname from labranchlevel where branchtype='" +
                cLAAssessAccessorySchema.getBranchType() +
                "' and branchtype2='" + cLAAssessAccessorySchema.getBranchType2() +
                "' and branchlevelid=(select min(branchlevelid) from labranchlevel where branchtype='" +
                cLAAssessAccessorySchema.getBranchType() +
                "' and branchtype2='" +
                cLAAssessAccessorySchema.getBranchType2() +
                "')";

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS == null || tSSRS.getMaxRow() == 0) {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "未查询到销售机构最低级别代码!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String minBranchLevelID = tSSRS.GetText(1, 1);
        if (!minBranchLevelID.equals(
            String.valueOf(tLABranchLevelDB.getBranchLevelID()))) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "主管到业务系列归属的目标机构必须是" + tSSRS.GetText(1, 2) +
                                  "!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tAgentCode = cLAAssessAccessorySchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "未查到业务员" + tLAAgentDB.getAgentCode() +
                                  "的基础信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(tAgentCode);
        if (!tLATreeDB.getInfo()) {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "未查到业务员" + tLATreeDB.getAgentCode() +
                                  "的行政信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = "select * from labranchgroup where branchmanager='" +
                           mLAAssessAccessorySchema.getAgentCode() +
                           "' and (endflag is null or endflag<>'Y')";
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(branchSQL);
        for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
            LABranchGroupSchema ttLABranchGroupSchema = new LABranchGroupSchema();
            ttLABranchGroupSchema = tLABranchGroupSet.get(i);
            LABranchGroupBSchema tLABranchGroupBSchema = new
                    LABranchGroupBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLABranchGroupBSchema,
                                     ttLABranchGroupSchema);
            tLABranchGroupBSchema.setEdorNo(tEdorNo);
            tLABranchGroupBSchema.setEdorType(mEdorType);
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupBSchema.
                                               getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupBSchema.
                                               getMakeTime());
//            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupBSchema.
//                                                 getModifyDate2());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupBSchema.
                                                 getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupBSchema.
                                                 getModifyTime());

            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator2(tLABranchGroupBSchema.
                                               getOperator());
            tLABranchGroupBSchema.setOperator(mGlobalInput.Operator);
            this.mLABranchGroupBSet.add(tLABranchGroupBSchema);
            ttLABranchGroupSchema.setBranchManager("");
            ttLABranchGroupSchema.setBranchManagerName("");
            mAscriptDate = cLAAssessAccessorySchema.getIndexCalNo() + "01";
            mAscriptDate = AgentPubFun.formatDate(mAscriptDate, "yyyy-MM-dd");
            mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);
            ttLABranchGroupSchema.setAStartDate(mAscriptDate);
            this.mLABranchGroupSet.add(ttLABranchGroupSchema);
        }

        tAgentGroupNew = cLAAssessAccessorySchema.getAgentGroupNew();
        LABranchGroupSchema tDirBranchGroupSchema = new LABranchGroupSchema();
        tDirBranchGroupSchema = getDirTeam(tAgentGroupNew);
        if (tDirBranchGroupSchema == null) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "ascriptToBusSeries";
            tError.errorMessage = "查询目标机构的直辖机构返回空!";
            this.mErrors.addOneError(tError);
            return false;

        }

        sql = "select * from lawage where agentcode='" +
              cLAAssessAccessorySchema.getAgentCode() + "'";
        LAWageDB tLAWageDB = new LAWageDB();
        LAWageSet tLAWageSet = new LAWageSet();
        tLAWageSet = tLAWageDB.executeQuery(sql);
        if (tLAWageSet == null || tLAWageSet.size() == 0) {
            System.out.println("----没有计算过佣金，调整日期自动改为该业务员的入司日期！");
            tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(cLAAssessAccessorySchema.getAgentCode());
            tLAAgentDB.getInfo();
            mAscriptDate = tLAAgentDB.getEmployDate();

        } else {

            mAscriptDate = cLAAssessAccessorySchema.getIndexCalNo() + "01";
            mAscriptDate = AgentPubFun.formatDate(mAscriptDate, "yyyy-MM-dd");
            mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);

        }

        String tBranchCode = tDirBranchGroupSchema.getAgentGroup();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
        tLAAgentSchema = tLAAgentDB.getSchema();

        Reflections tReflections = new Reflections();

        LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeBSchema tLATreeBSchema = new LATreeBSchema();
        tLATreeSchema = tLATreeDB.getSchema();
        tReflections.transFields(tLATreeBSchema, tLATreeSchema);
        tLATreeBSchema.setEdorNO(tEdorNo);
        tLATreeBSchema.setRemoveType(mEdorType);

        tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
        tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
        tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
        tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
        tLATreeBSchema.setMakeDate(currentDate);
        tLATreeBSchema.setMakeTime(currentTime);
        tLATreeBSchema.setModifyDate(currentDate);
        tLATreeBSchema.setModifyTime(currentTime);
        tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
        tLATreeBSchema.setOperator(mGlobalInput.Operator);
        tLATreeBSchema.setIndexCalNo(cLAAssessAccessorySchema.getIndexCalNo());
        tLATreeSchema.setAgentGroup(tAgentGroupNew);
        System.out.println("latreeqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"+tAgentGroupNew);
        tLATreeSchema.setBranchCode(tBranchCode);
        tLATreeSchema.setModifyDate(currentDate);
        tLATreeSchema.setModifyTime(currentTime);
        tLATreeSchema.setOperator(mGlobalInput.Operator);
        if (!cLAAssessAccessorySchema.getAgentGroup().equals(cLAAssessAccessorySchema.getAgentGroupNew()))
         {//如果在新机构和原机构不相同,则因为是从主管到业务异动,则应该置上级代理人为新机构主管
             tLATreeSchema.setUpAgent(tLABranchGroupSchema.getBranchManager());
         }
         else
         {//相同,如果异动人员是机构主管,则应该置上级代理人为空
            if(tLATreeSchema.getAgentCode().equals(tLABranchGroupSchema.getBranchManager()))
            {
              tLATreeSchema.setUpAgent("");
            }
         }

        tLATreeSchema.setAstartDate(mAscriptDate);

            String tAgentGrade = cLAAssessAccessorySchema.getAgentGrade();
            String tAgentSeries = AgentPubFun.getAgentSeries(tAgentGrade);
            tLATreeSchema.setAgentLastGrade(tAgentGrade);
            tLATreeSchema.setAgentLastSeries(tAgentSeries);
            tLATreeSchema.setOldStartDate(tLATreeSchema.getStartDate());
            tLATreeSchema.setOldEndDate(PubFun.calDate(mAscriptDate, -1,
                    "D", null));

        tLATreeSchema.setStartDate(mAscriptDate);
        tLATreeSchema.setAgentGrade(cLAAssessAccessorySchema.getAgentGrade1());
        tLATreeSchema.setAgentSeries(AgentPubFun.getAgentSeries(
                cLAAssessAccessorySchema.getAgentGrade1()));
        LAAgentGradeDB tLAAgentGradelastDB = new LAAgentGradeDB();
        tLAAgentGradelastDB.setGradeCode(cLAAssessAccessorySchema.getAgentGrade());
        if (!tLAAgentGradelastDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentGradelastDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "dealData";
            tError.errorMessage = "未查询到" +
                                  cLAAssessAccessorySchema.getAgentGrade() +
                                  "的描述信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
        tLAAgentGradeDB.setGradeCode(cLAAssessAccessorySchema.getAgentGrade1());
        if (!tLAAgentGradeDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentGradeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "dealData";
            tError.errorMessage = "未查询到" +
                                  cLAAssessAccessorySchema.getAgentGrade1() +
                                  "的描述信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //目标职级为见习
        if (  tLAAgentGradeDB.getGradeProperty4().equals("0") ){
            sql = "select * from lacommision where agentcode='" +
                  cLAAssessAccessorySchema.getAgentCode() +
                  "' and (caldate is null or caldate>='" + mAscriptDate + "') ";
            System.out.println("|||||"+sql);
            LACommisionDB tLACommisionDB = new LACommisionDB();
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = tLACommisionDB.executeQuery(sql);
            CalFormDrawRate tCalFormDrawRate = new CalFormDrawRate();
            VData temp = new VData();
            temp.clear();
            temp.add(tLACommisionSet);
            temp.add(cLAAssessAccessorySchema.getAgentGrade1()); //传入的是一个AgentGrade，提数计算的时候传空，归属的时候传入的是新的职级
            if (!tCalFormDrawRate.submitData(temp, CalFormDrawRate.CALFLAG)) {
                this.mErrors.copyAllErrors(tCalFormDrawRate.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptInDiffSeriesBL";
                tError.functionName = "ascriptToBusSeries";
                tError.errorMessage = "打折处理的时候出错！";
                this.mErrors.addOneError(tError);
                return false;
            }

            mLACommisionSet.clear();
            mLACommisionSet.set((LACommisionSet) tCalFormDrawRate.getResult().
                                getObjectByObjectName("LACommisionSet", 0));
        }
         System.out.println("mLACommisionSet!!!!!!!!!!!!!!!"+mLACommisionSet.size());
        if ( !mLAAssessAccessorySchema.getAgentGroup().equals
               ( mLAAssessAccessorySchema.getAgentGroupNew()))
        {//如果销售单位发生变化，则业绩重新归属到新的团队
            if (mLACommisionSet.size()>0)
            {//如果目标职级为见习，并且归属日后有业绩，则mLACommisionSet.size>0
                for (int k = 1; k <= mLACommisionSet.size(); k++) {
                     mLACommisionSet.get(k).setAgentGroup(toAgentGroup);
                     mLACommisionSet.get(k).setBranchCode(toAgentGroup);
                     mLACommisionSet.get(k).setBranchAttr(toBranchAttr);
                     mLACommisionSet.get(k).setBranchSeries(toBranchseries);
                }
            }                        else
            {
            sql = "select * from lacommision where agentcode='" +
                  cLAAssessAccessorySchema.getAgentCode() +
                  "' and (caldate is null or caldate>='" + mAscriptDate + "')";
            LACommisionDB tLACommisionDB = new LACommisionDB();
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = tLACommisionDB.executeQuery(sql);
            System.out.println("tLACommisionSet!!!!!!!!!!!!!!!"+sql);
            for (int k = 1; k <= tLACommisionSet.size(); k++) {
                    LACommisionSchema tLACommisionSchema = new
                            LACommisionSchema();
                    tLACommisionSchema = tLACommisionSet.get(k);
                    tLACommisionSchema.setAgentGroup(toAgentGroup);
                    tLACommisionSchema.setBranchCode(toAgentGroup);
                    tLACommisionSchema.setBranchAttr(toBranchAttr);
                    tLACommisionSchema.setBranchSeries(toBranchseries);
                    mLACommisionSet.add(tLACommisionSchema);
              }
            }
        }
        if (tLAAgentGradeDB.getGradeProperty2() == null ||
            tLAAgentGradeDB.getGradeProperty2().equals("")) { //被清退，置成清退状态
            LADimissionSchema tLADimissionSchema = new
                    LADimissionSchema();
            tLADimissionSchema.setAgentCode(tLATreeSchema.getAgentCode());
            tLADimissionSchema.setBranchType(cLAAssessAccessorySchema.
                                             getBranchType());
            tLADimissionSchema.setBranchType2(cLAAssessAccessorySchema.
                                              getBranchType2());
            tLADimissionSchema.setBranchAttr(AgentPubFun.
                                             getAgentBranchAttr(tLATreeSchema.
                    getAgentCode()));
            tLADimissionSchema.setApplyDate(mAscriptDate);
            tLADimissionSchema.setDepartRsn("考核清退");
            tLADimissionSchema.setDepartTimes(1);
            tLADimissionSchema.setDepartState("05");
            tLADimissionSchema.setMakeDate(currentDate);
            tLADimissionSchema.setMakeTime(currentTime);
            tLADimissionSchema.setModifyDate(currentDate);
            tLADimissionSchema.setModifyTime(currentTime);
            tLADimissionSchema.setOperator(mGlobalInput.
                                           Operator);
            mLADimissionSet.add(tLADimissionSchema);
            tLAAgentSchema.setAgentState("05");
        }

        tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
        tLAAgentBSchema.setEdorNo(tEdorNo);
        tLAAgentBSchema.setEdorType(mEdorType);
        tLAAgentBSchema.setMakeDate(currentDate);
        tLAAgentBSchema.setMakeTime(currentTime);
        tLAAgentBSchema.setModifyDate(currentDate);
        tLAAgentBSchema.setModifyTime(currentTime);
        tLAAgentBSchema.setOperator(mGlobalInput.Operator);
        tLAAgentBSchema.setIndexCalNo(cLAAssessAccessorySchema.getIndexCalNo());
        this.mLAAgentBSet.add(tLAAgentBSchema);
        tLAAgentSchema.setAgentGroup(tAgentGroupNew);
System.out.println("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"+tAgentGroupNew);
        tLAAgentSchema.setBranchCode(tBranchCode);
        tLAAgentSchema.setModifyDate(currentDate);
        tLAAgentSchema.setModifyTime(currentTime);
        tLAAgentSchema.setOperator(mGlobalInput.Operator);
        this.mLAAgentSet.add(tLAAgentSchema);
        this.mLATreeSet.add(tLATreeSchema);
        this.mLATreeBSet.add(tLATreeBSchema);
        //对下属人员进行处理 ，（有的人员上级人员是他自己，所以添加agentcode<>'" + tAgentCode + "'）
        sql = "select * from latree where upagent='" + tAgentCode + "' and agentcode<>'" + tAgentCode + "' and exists (select 'X' from laagent where agentcode=latree.agentcode and agentstate<'06' )";
        LATreeSet tLATreeSet = new LATreeSet();
        tLATreeSet = tLATreeDB.executeQuery(sql);
        System.out.println("---tsql！"+sql);
        for (int i = 1; i <= tLATreeSet.size(); i++) {
             String tEdorType="07";
            System.out.println("----没有计算过佣金，调整日期自动改为该业务员的入司日期！");
            sql = "select * from lawage where agentcode='" +
                  tLATreeSet.get(i).getAgentCode() + "'";
            tLAWageDB = new LAWageDB();
            tLAWageSet = new LAWageSet();
            tLAWageSet = tLAWageDB.executeQuery(sql);
            if (tLAWageSet == null || tLAWageSet.size() == 0) {
                System.out.println("----没有计算过佣金，调整日期自动改为该业务员的入司日期！");
                tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(cLAAssessAccessorySchema.getAgentCode());
                tLAAgentDB.getInfo();
                mAscriptDate = tLAAgentDB.getEmployDate();

            } else {

                mAscriptDate = cLAAssessAccessorySchema.getIndexCalNo() + "01";
                mAscriptDate = AgentPubFun.formatDate(mAscriptDate,
                        "yyyy-MM-dd");
                mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);

            }

            LATreeSchema TempLATreeSchema = tLATreeSet.get(i);
            LATreeBSchema TempLATreeBSchema = new LATreeBSchema();
            tReflections = new Reflections();
            tReflections.transFields(TempLATreeBSchema, TempLATreeSchema);
            TempLATreeBSchema.setEdorNO(tEdorNo);
            TempLATreeBSchema.setRemoveType(tEdorType);
            TempLATreeBSchema.setOperator2(mGlobalInput.Operator);
            TempLATreeBSchema.setMakeDate2(TempLATreeBSchema.getMakeDate());
            TempLATreeBSchema.setMakeTime2(TempLATreeBSchema.getMakeTime());
            TempLATreeBSchema.setModifyDate2(TempLATreeBSchema.getModifyDate());
            TempLATreeBSchema.setModifyTime2(TempLATreeBSchema.getModifyTime());
            TempLATreeBSchema.setMakeDate(currentDate);
            TempLATreeBSchema.setMakeTime(currentTime);
            TempLATreeBSchema.setModifyDate(currentDate);
            TempLATreeBSchema.setModifyTime(currentTime);
            TempLATreeBSchema.setIndexCalNo(cLAAssessAccessorySchema.
                                            getIndexCalNo());
            this.mLATreeBSet.add(TempLATreeBSchema);
            TempLATreeSchema.setUpAgent("");
            TempLATreeSchema.setAstartDate(mAscriptDate);
            TempLATreeSchema.setModifyDate(currentDate);
            TempLATreeSchema.setModifyTime(currentTime);
            TempLATreeSchema.setOperator(mGlobalInput.Operator);
            this.mLATreeSet.add(TempLATreeSchema);
            cLAAssessAccessorySchema.setModifyDate(currentDate);
            cLAAssessAccessorySchema.setModifyTime(currentTime);
            cLAAssessAccessorySchema.setOperator(mGlobalInput.Operator);
            cLAAssessAccessorySchema.setState("2");
             tAgentGrade = cLAAssessAccessorySchema.getAgentGrade();
            String tAgentGrade1 = cLAAssessAccessorySchema.getAgentGrade1();
             tAgentSeries = AgentPubFun.getAgentSeries(tAgentGrade);
            String tAgentSeries1 = AgentPubFun.getAgentSeries(tAgentGrade1);
            if (tAgentSeries == null || tAgentSeries.equals("")) {
                CError tError = new CError();
                tError.moduleName = "LAAscriptInSameSeriesBL";
                tError.functionName = "ascriptToBusSeries";
                tError.errorMessage = "获取职级" + tAgentGrade + "所对应的代理人系列返回为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tAgentSeries1 == null || tAgentSeries1.equals("")) {
                CError tError = new CError();
                tError.moduleName = "LAAscriptInSameSeriesBL";
                tError.functionName = "ascriptToBusSeries";
                tError.errorMessage = "获取职级" + tAgentGrade1 + "所对应的代理人系列返回为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tAgentSeries.equals(tAgentSeries1)) {
                CError tError = new CError();
                tError.moduleName = "LAAscriptInSameSeriesBL";
                tError.functionName = "ascriptToBusSeries";
                tError.errorMessage = "本模块只能处理同系列的职级的异动!";
                this.mErrors.addOneError(tError);
                return false;
            }
            cLAAssessAccessorySchema.setAgentSeries(tAgentSeries);
            cLAAssessAccessorySchema.setAgentSeries1(tAgentSeries1);

        }
        return true;
    }

    /**
     *
     *
     * @param tAgentGroup String
     * @return java.lang.String
     */
    private LABranchGroupSchema getDirTeam(String tAgentGroup) {
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tAgentGroup);
        tLABranchGroupDB.getInfo();
        tLABranchGroupSchema = tLABranchGroupDB.getSchema();
        if (tLABranchGroupSchema == null) {
            return null;
        }
        LABranchLevelDB tLABranchLevelDB = new LABranchLevelDB();
        LABranchLevelSchema tLABranchLevelSchema = new LABranchLevelSchema();
        tLABranchLevelDB.setBranchLevelCode(tLABranchGroupSchema.getBranchLevel());
        tLABranchLevelDB.setBranchType(tLABranchGroupSchema.getBranchType());
        tLABranchLevelDB.setBranchType2(tLABranchGroupSchema.getBranchType2());
        tLABranchLevelDB.getInfo();
        tLABranchLevelSchema = tLABranchLevelDB.getSchema();
        System.out.println("-----tLABranchLevelSchema=====");
        if (tLABranchLevelSchema == null) {
            System.out.println("-----tLABranchLevelSchema失败=====");
            return null;
        }
        if (tLABranchLevelSchema.getSubjectProperty().equals("1")) { //有直辖机构
            String tSQL = "select * from labranchgroup where upbranch" + "='" +
                          tAgentGroup +
                          "' and upbranchattr='1' and EndFlag<>'Y'";
            System.out.println(tSQL);
            LABranchGroupSchema sch = new LABranchGroupSchema();
            LABranchGroupSet set = new LABranchGroupSet();
            LABranchGroupDB db = new LABranchGroupDB();
            set = db.executeQuery(tSQL);
            if (db.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptInDiffSeriesBL";
                tError.functionName = "getDirTeam";
                tError.errorMessage = "查询" + tAgentGroup + "的直辖机构失败！";
                this.mErrors.addOneError(tError);
                return null;
            }
            if (set.size() == 0) {
                CError tError = new CError();
                tError.moduleName = "LAAscriptInDiffSeriesBL";
                tError.functionName = "getDirTeam";
                tError.errorMessage = "未查到" + tAgentGroup + "的直辖机构！";
                this.mErrors.addOneError(tError);
                return null;
            }
            if (set.size() > 1) {
                CError tError = new CError();
                tError.moduleName = "LAAscriptInDiffSeriesBL";
                tError.functionName = "getDirDepartment";
                tError.errorMessage = "查询" + tAgentGroup + "直辖机构是发现多个结果！";
                this.mErrors.addOneError(tError);
                return null;
            }
            sch = set.get(1);
            String mAgentGroup = sch.getAgentGroup();
            return this.getDirTeam(mAgentGroup);
        } else {
            System.out.println("-----tLABranchGroupSchema.getAgentGroup()=====" +
                               tLABranchGroupSchema.getAgentGroup());
            return tLABranchGroupSchema;
        }
    }

    /**
     * 递归设置当前机构的所有下属机构的管理人员的上级代理人，
     * 包括当前机构的直辖机构的下属机构，直到当前机构的直辖组
     *
     * @param tAgentGroup String
     * @param tManager String
     * @return boolean
     */
    private boolean setDirInst(String tAgentGroup, String tManager) {
        String tEdorType="07";
        LATreeSchema tLATreeSchema;
        LATreeBSchema tLATreeBSchema;
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = new LATreeSet();
        String sql = "select * from latree where upagent='" + tManager + "' and exists (select 'X' from laagent where agentcode=latree.agentcode and agentstate<'06' ) and agentcode<>'" + tManager + "'";
        System.out.println("sql:" + sql);
        tLATreeSet = tLATreeDB.executeQuery(sql);
        if (tLATreeDB.mErrors.needDealError()) {
            mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "setUpAgent";
            tError.errorMessage = "获取" + tManager + "的相关信息时为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= tLATreeSet.size(); i++) {
            tLATreeSchema = new LATreeSchema();
            tLATreeSchema = tLATreeSet.get(i);
            //备份原有信息
            tLATreeBSchema = new LATreeBSchema();
            sql = "select * from lawage where agentcode='" +
                  tLATreeSchema.getAgentCode() + "'";
            LAWageDB tLAWageDB = new LAWageDB();
            LAWageSet tLAWageSet = new LAWageSet();
            tLAWageSet = tLAWageDB.executeQuery(sql);
            if (tLAWageSet == null || tLAWageSet.size() == 0) {
                System.out.println("----没有计算过佣金，调整日期自动改为该业务员的入司日期！");
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tLATreeSchema.getAgentCode());
                tLAAgentDB.getInfo();
                mAscriptDate = tLAAgentDB.getEmployDate();

            } else {

                mAscriptDate = mIndexCalNo +
                               "01";
                mAscriptDate = AgentPubFun.formatDate(mAscriptDate,
                        "yyyy-MM-dd");
                mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);

            }

            Reflections tReflections = new Reflections();
            tReflections.transFields(tLATreeBSchema, tLATreeSchema);
            tLATreeBSchema.setEdorNO(tEdorNo);
            tLATreeBSchema.setRemoveType(tEdorType); //主管任命
            tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
            tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
            this.mLATreeBSet.add(tLATreeBSchema);
            //修改当前
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tLATreeSchema.getAgentCode());
            tLAAgentDB.getInfo();
            String employdate = AgentPubFun.formatDate(tLAAgentDB.getEmployDate(),
                    "yyyy-MM-dd");

            if (mAscriptDate == null) {
                mAscriptDate = "";
            }

            if (employdate.compareTo(mAscriptDate) > 0) {
                tLATreeSchema.setAstartDate(employdate);
            } else {
                tLATreeSchema.setAstartDate(mAscriptDate);
            }
            tLATreeSchema.setUpAgent("");
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            tLATreeSchema.setOperator(this.mGlobalInput.Operator);

//            tLATreeBSchema.setModifyDate(currentDate);
//            tLATreeBSchema.setModifyTime(currentTime);
//            tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
//            mLATreeSet.add(tLATreeSchema);
        }

        LABranchGroupSchema rLABranchGroupSchema = new LABranchGroupSchema();
        rLABranchGroupSchema = getBranchGroupInfo(tAgentGroup);
        LAAgentSchema tLAAgentSchema = getAgentBasicInfo(tManager);
        String strSQL = "select * from labranchgroup where UpBranch='" +
                        tAgentGroup +
                        "' and (endflag is null or endflag<>'Y') ";
        //mAdjustDate=this.mLABranchGroupSchema .getModifyDate();
        System.out.println("------strSQL---" + strSQL);
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(strSQL);
        if (tLABranchGroupDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "setDirInst";
            tError.errorMessage = "查询当前机构" + tAgentGroup + "的下级机构时出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("--tLABranchGroupSet.size()---" +
                           tLABranchGroupSet.size());
        /**如果该机构有下级机构，则
         * 逐机构修改机构主管的行政信息并修改机构主管的上级代理人信息
         * 如果没有，那么该机构一定是个组机构，则需要修改该组机构所有成员的
         * 上级代理人
         */
        if (tLABranchGroupSet.size() > 0) {
            //查询该机构的下级非直辖机构
            strSQL = "select * from labranchgroup where UpBranch='" +
                     tAgentGroup +
                     "' and upbranchattr='0' and (endflag is null or endflag<>'Y' )";
            System.out.println("*******strSQL----" + strSQL);
            tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupSet = new LABranchGroupSet();
            tLABranchGroupSet = tLABranchGroupDB.executeQuery(strSQL);
            if (tLABranchGroupDB.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptInDiffSeriesBL";
                tError.functionName = "setDirInst";
                tError.errorMessage = "查询当前机构" + tAgentGroup + "的下级机构时出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("-----tLABranchGroupSet.size()---" +
                               tLABranchGroupSet.size());
            for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
                LABranchGroupSchema tLABranchGroupSchema = tLABranchGroupSet.
                        get(i);
                tLATreeSchema = new LATreeSchema();
                tLATreeBSchema = new LATreeBSchema();
                LAAgentSchema ttLAAgentSchema = new LAAgentSchema();
                String currManager = tLABranchGroupSchema.getBranchManager();
                System.out.println("--tManager--" + tManager +
                                   "----currManager---" + currManager);
                //备份行政信息
                if ((currManager == null) || currManager.trim().equals("")) {
                    continue;
                }
                tLATreeSchema = getAgentTreeInfo(currManager);
                ttLAAgentSchema = getAgentBasicInfo(currManager);
                if (ttLAAgentSchema.getAgentState().compareTo("05") > 0) {
                    continue;
                }
                if ((tLATreeSchema.getUpAgent()==null)||(tLATreeSchema.getUpAgent().equals(tManager))) {
                    continue;
                }
                sql = "select * from lawage where agentcode='" +
                      tLATreeSchema.getAgentCode() + "'";
                LAWageDB tLAWageDB = new LAWageDB();
                LAWageSet tLAWageSet = new LAWageSet();
                tLAWageSet = tLAWageDB.executeQuery(sql);
                if (tLAWageSet == null || tLAWageSet.size() == 0) {
                    System.out.println("----没有计算过佣金，调整日期自动改为该业务员的入司日期！");
                    LAAgentDB tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(tLATreeBSchema.getAgentCode());
                    tLAAgentDB.getInfo();
                    mAscriptDate = tLAAgentDB.getEmployDate();

                } else {

                    mAscriptDate = mIndexCalNo +
                                   "01";
                    mAscriptDate = AgentPubFun.formatDate(mAscriptDate,
                            "yyyy-MM-dd");
                    mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);

                }

                tLATreeBSchema = new LATreeBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setEdorNO(tEdorNo);
                tLATreeBSchema.setRemoveType(tEdorType); //主管任命
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
                tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
                this.mLATreeBSet.add(tLATreeBSchema);
                //修改行政信息
                System.out.println(tLATreeSchema.getAgentCode() + "" +
                                   tLATreeSchema.getAstartDate());
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tLATreeSchema.getAgentCode());
                tLAAgentDB.getInfo();
                String employdate = AgentPubFun.formatDate(tLAAgentDB.
                        getEmployDate(), "yyyy-MM-dd");
                if (employdate.compareTo(mAscriptDate) > 0) {
                    tLATreeSchema.setAstartDate(employdate);
                } else {
                    tLATreeSchema.setAstartDate(mAscriptDate);
                }

                tLATreeSchema.setUpAgent(tManager);
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(this.mGlobalInput.Operator);
                this.mLATreeSet.add(tLATreeSchema);
            }

        }
        //如果没有下属机构，则该机构一定时一个组，则置组内的人员的上级代理人为新任主管，并且将该组的信息进行备份与更新
        else {
             //如果新机构主管为空,或者新机构主管委B01 ,异动人员的职级不等于B01(即大于B01 ),则重新修改上级代理人
             //否则 不用修改上级代理人
            LATreeDB tLAtreeDB = new LATreeDB();
            tLAtreeDB.setAgentCode(oriManager);
            tLAtreeDB.getInfo();

            if ( (oriManager==null || oriManager.equals("")) ||
                 (tLAtreeDB.getAgentGrade().equals("B01") &&
                  !mLAAssessAccessorySchema.getAgentGrade1().equals("B01")) )
            {
            strSQL = "select * from latree where agentgroup='" + tAgentGroup +
                     "'" + " And exists (Select 'X' From LAAgent " +
                     " Where AgentState <'06' and agentcode=latree.agentcode)";
            System.out.println("------strSQL---" + strSQL);
            tLATreeSet = new LATreeSet();
            tLATreeSchema = new LATreeSchema();
            tLATreeDB = new LATreeDB();
            tLATreeSet = tLATreeDB.executeQuery(strSQL);
            if (tLATreeDB.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptInDiffSeriesBL";
                tError.functionName = "setDirInst";
                tError.errorMessage = "查询" + tAgentGroup + "的所有成员时失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("----tLATreeSet.size----" + tLATreeSet.size());
            for (int i = 1; i <= tLATreeSet.size(); i++) {
                //备份组员的行政信息
                tLATreeSchema = tLATreeSet.get(i);
                if (tLATreeSchema.getAgentCode().equals(tManager)) {
                    continue; //如果主管在这个组中，则主管的上级代理人信息不在这做修改
                }
                if (tLATreeSchema.getUpAgent() != null &&
                    tLATreeSchema.getUpAgent().equals(tManager)) {
                    continue; //如果上级代理人并没有变，则不用备份修改
                }
                tLATreeBSchema = new LATreeBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setEdorNO(tEdorNo);
                sql = "select * from lawage where agentcode='" +
                      tLATreeBSchema.getAgentCode() + "'";
                LAWageDB tLAWageDB = new LAWageDB();
                LAWageSet tLAWageSet = new LAWageSet();
                tLAWageSet = tLAWageDB.executeQuery(sql);
                if (tLAWageSet == null || tLAWageSet.size() == 0) {
                    System.out.println("----没有计算过佣金，调整日期自动改为该业务员的入司日期！");
                    LAAgentDB tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(tLATreeBSchema.getAgentCode());
                    tLAAgentDB.getInfo();
                    mAscriptDate = tLAAgentDB.getEmployDate();

                } else {

                    mAscriptDate = mIndexCalNo +
                                   "01";
                    mAscriptDate = AgentPubFun.formatDate(mAscriptDate,
                            "yyyy-MM-dd");
                    mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);

                }

                tLATreeBSchema.setRemoveType(tEdorType); //主管任命
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
                tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
                this.mLATreeBSet.add(tLATreeBSchema);
                //更新组员的行政信息

                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tLATreeSchema.getAgentCode());
                tLAAgentDB.getInfo();
                String employdate = AgentPubFun.formatDate(tLAAgentDB.
                        getEmployDate(), "yyyy-MM-dd");
                if (employdate.compareTo(mAscriptDate) > 0) {
                    tLATreeSchema.setAstartDate(employdate);
                } else {
                    tLATreeSchema.setAstartDate(mAscriptDate);
                }

                tLATreeSchema.setUpAgent(tManager);
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(this.mGlobalInput.Operator);
                this.mLATreeSet.add(tLATreeSchema);
            }
        }
        }
        return true;
    }

    /**
     * 查询机构编码为agentGroup的机构信息
     *
     * @param agentGroup String
     * @return com.sinosoft.lis.schema.LABranchGroupSchema
     */
    private LABranchGroupSchema getBranchGroupInfo(String agentGroup) {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(agentGroup);
        if (!tLABranchGroupDB.getInfo()) {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "getBranchGroupInfo";
            tError.errorMessage = "查询机构" + agentGroup + "的信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLABranchGroupDB.getSchema();
    }

    /**
     * 查询tAgentCode的基础信息
     *
     * @param tAgentCode String
     * @return com.sinosoft.lis.schema.LAAgentSchema
     */
    private LAAgentSchema getAgentBasicInfo(String tAgentCode) {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "getAgentBasicInfo";
            tError.errorMessage = "查询代理人" + tAgentCode + "的基础信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLAAgentDB.getSchema();
    }

    /**
     * 查询代理人编码为agentCode的代理人的行政信息
     *
     * @param agentCode String
     * @return com.sinosoft.lis.schema.LATreeSchema
     */
    private LATreeSchema getAgentTreeInfo(String agentCode) {
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(agentCode);
        if (!tLATreeDB.getInfo()) {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInDiffSeriesBL";
            tError.functionName = "getAgentTreeInfo";
            tError.errorMessage = "查询" + agentCode + "的行政信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLATreeDB.getSchema();
    }


}
