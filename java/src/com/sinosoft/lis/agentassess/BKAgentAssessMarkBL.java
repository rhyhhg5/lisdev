/*
 * <p>ClassName: ALABankAgentBL </p>
 * <p>Description: ALABankAgentBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.LAAssessMarkDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAssessMarkSchema;
import com.sinosoft.lis.vschema.LAAssessMarkSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;


public class BKAgentAssessMarkBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /** 业务处理相关变量 */
    private LAAssessMarkSchema mLAAssessMarkSchema = new LAAssessMarkSchema();
    private LAAssessMarkSet mLAAssessMarkSet = new LAAssessMarkSet();
    private BKAgentAssessMarkBLS mBKAgentAssessMarkBLS = new
            BKAgentAssessMarkBLS();
    int MaxId = 0; //考评最大序列号。

    private TransferData mTransferData = new TransferData();

    private String mAssessYM = "";

    private String mAgentCode = "";
    public BKAgentAssessMarkBL() {
    }

    public static void main(String[] args) {

        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "mak001";
        tGlobalInput.ManageCom = "8611";
        tGlobalInput.ComCode = "mak001";

        LAAssessMarkSchema mLAAssessMarkSchema = new LAAssessMarkSchema();
        mLAAssessMarkSchema.setAgentCode(null);
        mLAAssessMarkSchema.setBranchType("2");
        mLAAssessMarkSchema.setBranchType2("01");
        mLAAssessMarkSchema.setIdx(null);
        mLAAssessMarkSchema.setAssessMarkType("03");
        mLAAssessMarkSchema.setDoneDate("2006-03-28");
        mLAAssessMarkSchema.setAssessYM(null);
        mLAAssessMarkSchema.setToolMark(100);
        mLAAssessMarkSchema.setChargeMark(100);
        mLAAssessMarkSchema.setMeetMark(100);
        mLAAssessMarkSchema.setRuleMark(100);
        mLAAssessMarkSchema.setAssessMark(100);
        mLAAssessMarkSchema.setBranchAttr("000000000004"); //BranchAttr 在此为内部编码
        mLAAssessMarkSchema.setStandbyMark1("0");
        mLAAssessMarkSchema.setStandbyMark2("0");
//            AgentCode
//            BranchType2
//            BranchType201
//            Idxnull
//            AssessType03
//            DoneDate2006-03-28
//            AssessDate200612
//            tToolMark1
//            tChargeMark1
//            tMeetMark1
//            tRuleMark1
//            tAssessMark1.00
//            tAgentGroup000000000003
//            tAssessMarkCount
//            tAssessPersonCount
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("AssessYM", "200601");
        mTransferData.setNameAndValue("AgentCode", "1102000002");
        VData tVData = new VData();
        tVData.addElement(tGlobalInput);
        tVData.addElement(mLAAssessMarkSchema);
        tVData.addElement(mTransferData);
        BKAgentAssessMarkBL tBKAgentAssessMarkBL = new BKAgentAssessMarkBL();

        if (!tBKAgentAssessMarkBL.submitData(tVData, "INSERT||MAIN")) {
            System.out.print(tBKAgentAssessMarkBL.mErrors.getContent());
        }

//   mBKAgentAssessMarkUI.submitData(tVData,Operator);//提交


    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        System.out.println("1888" + mLAAssessMarkSchema.getAgentCode());
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALABankAgentBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start LAAssessMarkSchema Submit...");
        BKAgentAssessMarkBLS tBKAgentAssessMarkBLS = new BKAgentAssessMarkBLS();
        if (!tBKAgentAssessMarkBLS.submitData(mInputData, cOperate)) {
            this.mErrors.copyAllErrors(tBKAgentAssessMarkBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;

        }
        System.out.println("End LAAssessMarkSchema Submit...");
        //如果有需要处理的错误，则返回
        if (tBKAgentAssessMarkBLS.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tBKAgentAssessMarkBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = true;
        String tAgentCode = "";
        String tMaxMarkId = "";
        String tSQL = "";
        ExeSQL tExeSQL;
        tAgentCode = mLAAssessMarkSchema.getAgentCode();
        System.out.println("1" + mLAAssessMarkSchema.getAgentCode());

        System.out.println("1" + mOperate);
        if (this.mOperate.equals("INSERT||MAIN")) {
            //考评记录主键信息
            if (!check(mOperate)) {
                return false;
            }

            tMaxMarkId = getMaxMarkId(tAgentCode);
            mLAAssessMarkSchema.setIdx(tMaxMarkId);
//            this.mLAAssessMarkSchema.setBranchAttr(AgentPubFun.
//                    getAgentBranchAttr(tAgentCode));
            this.mLAAssessMarkSchema.setOperator(mGlobalInput.Operator);
            this.mLAAssessMarkSchema.setMakeDate(currentDate);
            this.mLAAssessMarkSchema.setMakeTime(currentTime);
            this.mLAAssessMarkSchema.setModifyDate(currentDate);
            this.mLAAssessMarkSchema.setModifyTime(currentTime);
        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            if (!check(mOperate)) {
                return false;
            }

            //考评记录主键信息
            LAAssessMarkDB tLAAssessMarkDB = new LAAssessMarkDB();
            tLAAssessMarkDB.setAgentCode(this.mAgentCode);
            tLAAssessMarkDB.setIdx(mLAAssessMarkSchema.getIdx());
            tLAAssessMarkDB.setAssessMarkType(mLAAssessMarkSchema.
                                              getAssessMarkType());
//            tLAAssessMarkDB.setAssessYM(mLAAssessMarkSchema.getAssessYM());
            tLAAssessMarkDB.setAssessYM(this.mAssessYM);
            tLAAssessMarkDB.setBranchType(mLAAssessMarkSchema.
                                          getBranchType());
            tLAAssessMarkDB.setBranchType2(mLAAssessMarkSchema.
                                           getBranchType2());

            if (!tLAAssessMarkDB.getInfo()) {
                this.mErrors.copyAllErrors(tLAAssessMarkDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABankAgentBL";
                tError.functionName = "submitData";
                tError.errorMessage = "查询考评分数信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLAAssessMarkSchema.setNoti(tLAAssessMarkDB.getNoti());
            this.mLAAssessMarkSchema.setMakeDate(tLAAssessMarkDB.getMakeDate());
            this.mLAAssessMarkSchema.setMakeTime(tLAAssessMarkDB.getMakeTime());
            this.mLAAssessMarkSchema.setModifyDate(currentDate);
            this.mLAAssessMarkSchema.setModifyTime(currentTime);
            this.mLAAssessMarkSchema.setOperator(mGlobalInput.Operator);

        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        try {
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
            this.mLAAssessMarkSchema.setSchema((LAAssessMarkSchema) cInputData.
                                               getObjectByObjectName(
                    "LAAssessMarkSchema", 0));
            this.mTransferData = (TransferData) cInputData.
                                 getObjectByObjectName(
                                         "TransferData", 0);

            this.mAssessYM = (String) mTransferData.getValueByName("AssessYM");
            this.mAgentCode = (String) mTransferData.getValueByName("AgentCode");
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "数据不完整。";
            this.mErrors.addOneError(tError);
            return false;

        }
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            if (this.mOperate.equals("UPDATE||MAIN")) {
                this.mLAAssessMarkSchema.setAgentCode(this.mAgentCode);
                this.mLAAssessMarkSchema.setAssessYM(this.mAssessYM);
            }

            this.mInputData.add(this.mLAAssessMarkSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }


    private boolean check(String cOperate) {

        String tAgentCode = "";
        String tAssessYM = "";
        String tAssessType="";
        if (mLAAssessMarkSchema.getAgentCode() == null) {
            tAgentCode = this.mAgentCode;

        } else {
            tAgentCode = mLAAssessMarkSchema.getAgentCode();

        }

        if (mLAAssessMarkSchema.getAssessYM() == null) {
            tAssessYM = mAssessYM;
        } else {
            tAssessYM = mLAAssessMarkSchema.getAssessYM();
        }

        String tBranchType = mLAAssessMarkSchema.getBranchType();
        String tBranchType2 = mLAAssessMarkSchema.getBranchType2();
        String strSub = "";
        System.out.println("gggg" + tBranchType);

        if (cOperate.equals("INSERT||MAIN")) {

            LAAssessMarkSchema tLAAssessMarkSchema = new LAAssessMarkSchema();

            LAAssessMarkDB tLAAssessMarkDB = new LAAssessMarkDB();
            tLAAssessMarkDB.setAgentCode(tAgentCode);
            tLAAssessMarkDB.setAssessYM(tAssessYM);
            tLAAssessMarkDB.setBranchType(tBranchType);
            tLAAssessMarkDB.setBranchType2(tBranchType2);
            if (tLAAssessMarkDB.getInfo()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAssessMarkDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "BKAgentAssessMarkBL";
                tError.functionName = "check";
                tError.errorMessage = tAgentCode +
                                      "该月的评分信息已存在!";
                this.mErrors.addOneError(tError);
                return false;
            }

            strSub = tAssessYM.substring(4, tAssessYM.length());

        } else {
            strSub = this.mAssessYM.substring(4, this.mAssessYM.length());
        }

        System.out.println("12323" + mLAAssessMarkSchema.getAgentCode());

        String tAssessMarkCount = String.valueOf(mLAAssessMarkSchema.
                                                 getStandbyMark1());
        String tAssessPersonCount = String.valueOf(mLAAssessMarkSchema.
                getStandbyMark2());
       tAssessType=mLAAssessMarkSchema.getAssessMarkType();
        System.out.println("qqqqqqqqqqqqqq" + tAssessMarkCount);
        System.out.println("qqqqqqqqqqqqqq" + tAssessPersonCount);
        if(tAssessType.equals("02"))
        {
        if (tAssessMarkCount.equals("0.0") && tAssessPersonCount.equals("0.0")) {
         //  String strSub = tAssessYM.substring(4, tAssessYM.length());
           System.out.println("qqqqqqqqqqqqqq" + strSub);
           if (strSub.equals("12")) {
                CError tError = new CError();
                tError.moduleName = "BKAgentAssessMarkBL";
                tError.functionName = "check";
                tError.errorMessage = "考评年月为12月时，分数和人数不能为空！";
                // System.out.println("查询最大值出错！");
                this.mErrors.addOneError(tError);
                return false;
                }
        }
        }
        return true;
    }


    //根据代理人职级确定代理人系列
    private String getMaxMarkId(String cAgentCode) {
        String tMaxMarkId = "";
        String tSQL = "select max(Idx) from LAAssessMark where AgentCode = '" +
                      cAgentCode + "'";
        ExeSQL tExeSQL = new ExeSQL();
        tMaxMarkId = tExeSQL.getOneValue(tSQL); //取得该代理人考评的最大序列号
        if (tExeSQL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行SQL语句：从表中取值失败!";
            System.out.println("查询最大值出错！");
            this.mErrors.addOneError(tError);
            return null;
        }
        if ((tMaxMarkId == null) || (tMaxMarkId.length() == 0)) {
            MaxId = 0;
        } else {
            MaxId = Integer.parseInt(tMaxMarkId);
        }
        tMaxMarkId = String.valueOf(MaxId + 1);
        return tMaxMarkId;
    }
}
