/*
 * <p>ClassName: LAAssessInputBLS </p>
 * <p>Description: LAAssessBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-06-21
 */
package com.sinosoft.lis.agentassess;


import java.sql.Connection;

import com.sinosoft.lis.vdb.LAWelfareInfoBDBSet;
import com.sinosoft.lis.vdb.LAWelfareInfoDBSet;
import com.sinosoft.lis.vschema.LAWelfareInfoBSet;
import com.sinosoft.lis.vschema.LAWelfareInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class LAEmployeeGenWelfareBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;

    private LAWelfareInfoBSet mLAWelfareInfoBSet = new LAWelfareInfoBSet();
    private LAWelfareInfoSet mLAWelfareInfoInsSet = new LAWelfareInfoSet();
    private LAWelfareInfoSet mLAWelfareInfoDelSet = new LAWelfareInfoSet();

    public LAEmployeeGenWelfareBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    private boolean getInputData(VData cInputData)
    {
        mLAWelfareInfoBSet = (LAWelfareInfoBSet) cInputData.get(0);
        mLAWelfareInfoInsSet = (LAWelfareInfoSet) cInputData.get(1);
        mLAWelfareInfoDelSet = (LAWelfareInfoSet) cInputData.get(2);
        return true;
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        if (!getInputData(cInputData))
        {
            return true;
        }
        System.out.println("Start LAEmployeeWageAdjBLS  Submit...");
        if (!saveLAEmployeeWage())
        {
            return false;
        }
        System.out.println("End LAEmployeeWageAdjBLS Submit...");
        return true;
    }

    /**
     * 保存函数
     */
    private boolean saveLAEmployeeWage()
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 插入LAWelfareInfo...");
            LAWelfareInfoBDBSet tLAWelfareInfoBDBSet = new LAWelfareInfoBDBSet(
                    conn);
            tLAWelfareInfoBDBSet.add(mLAWelfareInfoBSet);
            if (!tLAWelfareInfoBDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWelfareInfoBDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeWageAdjBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAWelfareInfoDBSet tLAWelfareInfoDelDBSet = new LAWelfareInfoDBSet(
                    conn);
            tLAWelfareInfoDelDBSet.add(mLAWelfareInfoDelSet);
            if (!tLAWelfareInfoDelDBSet.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWelfareInfoDelDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeWageAdjBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            LAWelfareInfoDBSet tLAWelfareInfoInsDBSet = new LAWelfareInfoDBSet(
                    conn);
            tLAWelfareInfoInsDBSet.add(mLAWelfareInfoInsSet);
            if (!tLAWelfareInfoInsDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWelfareInfoInsDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeWageAdjBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();

        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeWageAdjBLS";
            tError.functionName = "submitData";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception ex)
            {}
        }

        return tReturn;
    }


}
