package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LACommisionDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionBSchema;
import com.sinosoft.lis.schema.LACommisionDetailBSchema;
import com.sinosoft.lis.schema.LACommisionDetailSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LACommisionBSet;
import com.sinosoft.lis.vschema.LACommisionDetailBSet;
import com.sinosoft.lis.vschema.LACommisionDetailSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class AscriptBusinessOperateBL {

    //错误类处理
    public CErrors mErrors = new CErrors();

    //传入数据
    private String[] mAgentCode;//可能是一批人的集合
    private String mAgentCodeStr="";
    //private String[] mOldBranchCode;//可能是一批组的集合
    //private String mOldBranchCodeStr;
    private String[] mOldAgentGroup;//可能是一批组的集合
    private String mOldAgentGroupStr;
    private String mNewAgentGroup = "";
    private String mNewBranchAttr = "";
    private String mNewBranchSeries = "";
    private String mNewBranchCode = "";
    private String mAdjustDate = "";//首期的业绩调动日期
    private String mAdjustDate2 = "";//续期的业绩调动日期
    private String mAStartDate ="" ;
    private String mLAcommisionCalDateCondition="";//实收的日期条件
    private String mEndDate = "";
    private String mFlag = ""; //0:只更新扎帐表和渠道应收表中的BranchAttr和BranchSeries 1:只更新扎帐表中的BranchAttr和FYC 2:更新数组中的所有表 3:更新数组中的所有表和扎帐表中的FYC
    private String mSpecialCondition="";//业绩更新时的特殊条件
    private String mManageCom="";
    public GlobalInput mGlobalInput = new GlobalInput();//操作员的信息
    private VData mOutputData=new VData(); //保存向外层传递的数据
    private VData mSQLstr=new VData();
    private VData mSQLstrInsertB=new VData();
    private LACommisionBSet mLACommisionBSet=new LACommisionBSet();
    private LACommisionSet mLACommisionSet=new LACommisionSet();
    private LACommisionDetailBSet mLACommisionDetailBSet=new LACommisionDetailBSet();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mEdorNo;
    private String[] mUpdateTable = {
                                    "LACommisionDetail","LACommision","LJSPayPerson",
                                    "LJSPay","LCCont","LCPol","LASPayPerson","LPCont","LPPol"
                                    /*"LCCont", "LCGrpCont",
                                    "LCGrpPol","LCPol",
                                    "LJAPayGrp","LJAPayPerson",
                                    "LJAGetEndorse"*/
                                     };

    public AscriptBusinessOperateBL() {
    }

    private boolean getInputData(VData cInputData)
    {
        //清空所有的变量
        mFlag = "";
        mNewBranchAttr = "";
        mOldAgentGroup = null;
        mNewBranchSeries = "";
        mNewAgentGroup = "";
        mNewBranchCode = "";
        mAdjustDate = "";
        mAgentCode = null;
        mEndDate = "";
        mGlobalInput=null;
        mSpecialCondition="";
        mOldAgentGroupStr="";
        mAgentCodeStr="";

        mFlag = (String)cInputData.get(0);
        mNewBranchAttr = (String)cInputData.get(1);
        mOldAgentGroup = (String[])cInputData.get(2);
        mNewBranchSeries = (String)cInputData.get(3);
        mNewAgentGroup = (String)cInputData.get(4);
        mNewBranchCode = (String)cInputData.get(5);
        mAdjustDate = (String)cInputData.get(6);
        mAgentCode = (String[])cInputData.get(7);
        mEndDate = (String)cInputData.get(8);
        mGlobalInput=(GlobalInput)cInputData.getObjectByObjectName("GlobalInput",9);
        if(cInputData.size()>10)
        {
            mSpecialCondition=(String)cInputData.get(10);
        }

        if(mGlobalInput==null)
        {
            DealErr("没有操作员信息！","AscriptBusinessOperateBL","getInputData");
            return false;
        }
        if (this.mFlag==null || this.mFlag.equals(""))
        {
            DealErr("没有指定业绩归属类型，不能做归属！","AscriptBusinessOperateBL","getInputData");
            return false;
        }
        if (this.mNewBranchAttr == null || this.mNewBranchAttr.equals(""))
        {
            DealErr("没有指定业绩归属至机构的外部编码，不能做归属！","AscriptBusinessOperateBL","getInputData");
            return false;
        }
        if (this.mNewBranchSeries == null || this.mNewBranchSeries.equals(""))
        {
            DealErr("没有指定业绩归属至机构的系列号，不能做归属！","AscriptBusinessOperateBL","getInputData");
            return false;
        }

        if (this.mOldAgentGroup == null || this.mOldAgentGroup.equals(""))
            {
                DealErr("没有指定业绩原属机构的编码，不能做归属！","AscriptBusinessOperateBL","getInputData");
                return false;
            }
        if(this.mFlag.compareTo("1")>0)
        {
            if (this.mNewAgentGroup == null || this.mNewAgentGroup.equals(""))
            {
                DealErr("没有指定业绩归属至机构的编码，不能做归属！","AscriptBusinessOperateBL","getInputData");
                return false;
            }

            if (this.mNewBranchCode == null || this.mNewBranchCode.equals(""))
            {
                DealErr("没有指定业绩归属至直辖机构的编码，不能做归属！","AscriptBusinessOperateBL","getInputData");
                return false;
            }

            if (this.mAdjustDate == null || this.mAdjustDate.equals(""))
            {
                DealErr("没有指定业绩归属的参考日期，不能做归属！","AscriptBusinessOperateBL","getInputData");
                return false;
            }
        }

        if(mOldAgentGroup!=null)
        {
            mOldAgentGroupStr="";
            for(int i=0;i<mOldAgentGroup.length;i++)
            {
                if(!mOldAgentGroup[i].equals("")&&mOldAgentGroup[i]!=null)
                {
                    mOldAgentGroupStr += "'" + mOldAgentGroup[i] + "'";
                    if(mManageCom==null||mManageCom.equals(""))
                    {
                        ExeSQL tExeSQL=new ExeSQL();
                        mManageCom=tExeSQL.getOneValue("select trim(managecom) from labranchgroup where agentgroup='"+mOldAgentGroup[i]+"' ");
                    }
                    if (i + 1 != mOldAgentGroup.length) { //不是最后一个串
                        mOldAgentGroupStr += ", ";
                    }
                }
            }
        }

        if(mAgentCode!=null)
        {
            mAgentCodeStr="";
            for(int i=0;i<mAgentCode.length;i++)
            {
                if(mAgentCode[i]!=null&&!mAgentCode[i].equals(""))
                mAgentCodeStr+="'"+mAgentCode[i]+"'";
                if(i+1!=mAgentCode.length)
                {//不是最后一个串
                    mAgentCodeStr+=", ";
                }
            }
        }
        mAdjustDate2 = PubFun.calDate(mAdjustDate,-2,"M",null);//将首期的调动日期向前简单退两个月,作为续期的点动日期
        return true;
    }

    private boolean prepareOutputDataToBLS()
    {
        try
        {
            mOutputData.clear();
            mOutputData.add(mSQLstr);
            mOutputData.add(mLACommisionSet);
            //mOutputData.add(mLACommisionDetailBSet);
        }
        catch(Exception e)
        {
            DealErr("准备传向BLS的数据失败！","AscriptBusinessOperateBL","prepareOutputDataToBLS");
            return false;
        }
        return true;
    }

    public VData getOutputDataToBLS()
    {
        if(!prepareOutputDataToBLS())
        {
            return null;
        }
        if(mOutputData!=null||mOutputData.size()>0)
        {
            return mOutputData;
        }
        return null;
    }

    private void DealErr(String cMsg,String cMName,String cFName)
    {
        CError tCError=new CError();
        tCError.moduleName=cMName;
        tCError.functionName=cFName;
        tCError.errorMessage=cMsg;
        mErrors.addOneError(tCError);
    }


    public static void main(String[] args) {
//        String s="2006-05-31";
//        System.out.println(s);
//        s = PubFun.calDate(s,-2,"M",null);
//        System.out.println(s);
    	AscriptBusinessOperateBL tAscriptBusinessOperateBL = new AscriptBusinessOperateBL();
    	LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
    	tLABranchGroupSchema.setAgentGroup("000000049512");
    	tLABranchGroupSchema.setManageCom("86141001");
    	tAscriptBusinessOperateBL.setContToGroupNew("1401008409",tLABranchGroupSchema);
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
            return false;

        //更新所有表的组别
        if (!updateAgentGroup())
            //returnfalse;
            return false;

        return true;
    }


    private boolean updateAgentGroup() {
        StringBuffer tSql = new StringBuffer("");
        String tTableName = "";
        if (this.mFlag.compareTo("1")<=0) {
            tSql.append("UPDATE LACommision SET ");
            tSql.append("BranchAttr = '" + this.mNewBranchAttr + "',");
            tSql.append("BranchSeries = '" + this.mNewBranchSeries + "',");
            tSql.append("ModifyDate = to_date('" + currentDate + "','yyyy-mm-dd') ,");
            tSql.append("ModifyTime = '" + currentTime + "', ");
            tSql.append("Operator = '" + mGlobalInput.Operator + "' ");
            if(this.mFlag.equals("1"))
                tSql.append(", FYC = DirectWage ");
            tSql.append("Where ((paycount<=1 and (CalDate IS NULL  OR CalDate >= '" +
                        this.mAdjustDate + "')) or (paycount>1 and CalDate>='"+mAdjustDate2+"')) ");

            if (mAgentCodeStr != null && !mAgentCodeStr.equals(""))
                tSql.append("And AgentCode in (" + mAgentCodeStr + ") ");
            if (mOldAgentGroupStr != null && !mOldAgentGroupStr.equals(""))
                tSql.append("AND BranchCode in (" + this.mOldAgentGroupStr + ") ");
            if (mSpecialCondition!=null && !mSpecialCondition.equals(""))
                tSql.append("AND "+ mSpecialCondition);
            System.out.println("SQL:" + tSql.toString());
            mSQLstr.add(tSql);

            //查询回来后备份
            String strSQL;
            strSQL="select * from LACommision ";
            strSQL+=tSql.substring(tSql.toString().indexOf("Where"));
            LACommisionSet tLACommisionSet=new LACommisionSet();
            LACommisionDB tLACommisionDB=new LACommisionDB();
            LACommisionSchema tLACommisionSchema;
            LACommisionBSchema tLACommisionBSchema;
            Reflections tReflections=new Reflections();
            tLACommisionSet=tLACommisionDB.executeQuery(strSQL);
            if(tLACommisionSet.size()>0)
            {
               for(int i=1;i<=tLACommisionSet.size();i++)
               {
                  tLACommisionSchema=new LACommisionSchema();
                  tLACommisionSchema=tLACommisionSet.get(i);
                  tLACommisionBSchema=new LACommisionBSchema();
                  tReflections.transFields(tLACommisionBSchema,tLACommisionSchema);
                  tLACommisionBSchema.setEdorNo(mEdorNo);
                  tLACommisionBSchema.setEdorType("01");
                  tLACommisionBSchema.setCommisionBaseNo(tLACommisionSchema.getCommisionSN());
                  tLACommisionBSchema.setModifyDate(currentDate);
                  tLACommisionBSchema.setModifyTime(currentTime);
                  tLACommisionBSchema.setOperator(this.mGlobalInput.Operator);
                  mLACommisionBSet.add(tLACommisionBSchema);
               }
            }
            //然后再处理渠道应收表
            tSql = new StringBuffer("");
            tSql.append("UPDATE LASPayPerson SET ");
            tSql.append("BranchSeries = '" + this.mNewBranchSeries + "',");
            tSql.append("ModifyDate = to_date('" + currentDate + "','yyyy-mm-dd') ,");
            tSql.append("ModifyTime = '" + currentTime + "', ");
            tSql.append("Operator = '" + mGlobalInput.Operator + "' ");
            //tSql.append("Where not exists (select 'Y' from lccontstate where contno = " + tTableName + ".contno and insuredno = '000000' and polno = " + tTableName + ".polno and statetype = 'Terminate' and state = '1' and (startdate<=to_date('"+mAdjustDate+"','yyyy-mm-dd') and (enddate>=to_date('"+mAdjustDate+"','yyyy-mm-dd') or enddate is null))) and actupayflag='0' ");
            tSql.append("Where LastPaytoDate>='"+mAdjustDate2+"' ");

            if (mAgentCodeStr != null && !mAgentCodeStr.equals(""))
                tSql.append("And AgentCode in (" + mAgentCodeStr + ") ");
            if (mOldAgentGroupStr != null && !mOldAgentGroupStr.equals(""))
                tSql.append("AND AgentGroup in (" + this.mOldAgentGroupStr + ") ");
            if (mSpecialCondition!=null && !mSpecialCondition.equals(""))
                tSql.append("AND "+ mSpecialCondition);
            System.out.println("SQL:" + tSql.toString());
            mSQLstr.add(tSql);

        }
        //更新某个代理人跨两个不同的组的情况 （一般为经过初始代理人信息维护的修改组别的情况）
        else if (this.mFlag.compareTo("1")>0) {
            int tLength = this.mUpdateTable.length;
            for (int i = 0; i < tLength; i++) {
                tSql = new StringBuffer("");
                tTableName = this.mUpdateTable[i];
                if (tTableName.equals("LACommision")) {
                    tSql.append("UPDATE LACommision SET ");
                    tSql.append("BranchCode = '" + this.mNewBranchCode + "',");
                    tSql.append("AgentGroup = '" + this.mNewAgentGroup + "',");
                    tSql.append("BranchAttr = '" + this.mNewBranchAttr + "',");
                    tSql.append("BranchSeries = '" + this.mNewBranchSeries + "',");
                    tSql.append("ModifyDate = to_date('" + currentDate + "','yyyy-mm-dd') ,");
                    tSql.append("ModifyTime = '" + currentTime + "', ");
                    tSql.append("Operator = '" + mGlobalInput.Operator + "' ");
                    if(this.mFlag.equals("3"))
                        tSql.append(", FYC = DirectWage ");
                    tSql.append("Where ((paycount<=1 and (CalDate IS NULL  OR CalDate >= '" +
                        this.mAdjustDate + "')) or (paycount>1 and CalDate>='"+mAdjustDate2+"')) ");

                    if (mAgentCodeStr != null && !mAgentCodeStr.equals(""))
                        tSql.append("And AgentCode in (" + mAgentCodeStr + ") ");
                    if (mOldAgentGroupStr != null && !mOldAgentGroupStr.equals(""))
                        tSql.append("AND BranchCode in (" + this.mOldAgentGroupStr + ") ");
                    if (mSpecialCondition!=null && !mSpecialCondition.equals(""))
                        tSql.append("AND "+ mSpecialCondition);

                        //查询回来后备份
                      String strSQL;
                      strSQL = "select * from LACommision ";
                      strSQL += tSql.substring(tSql.toString().indexOf("Where"));
                      LACommisionSet tLACommisionSet = new LACommisionSet();
                      LACommisionDB tLACommisionDB = new LACommisionDB();
                      LACommisionSchema tLACommisionSchema;
                      LACommisionBSchema tLACommisionBSchema;
                      Reflections tReflections = new Reflections();
                      tLACommisionSet = tLACommisionDB.executeQuery(strSQL);
                      if (tLACommisionSet.size() > 0) {
                        for (int j = 1; j <= tLACommisionSet.size(); j++) {
                          tLACommisionSchema = new LACommisionSchema();
                          tLACommisionSchema = tLACommisionSet.get(j);
                          tLACommisionBSchema = new LACommisionBSchema();
                          tReflections.transFields(tLACommisionBSchema, tLACommisionSchema);
                          tLACommisionBSchema.setEdorNo(mEdorNo);
                          tLACommisionBSchema.setEdorType("91");
                          tLACommisionBSchema.setCommisionBaseNo(tLACommisionSchema.getCommisionSN());
                          tLACommisionBSchema.setModifyDate(currentDate);
                          tLACommisionBSchema.setModifyTime(currentTime);
                          tLACommisionBSchema.setOperator(this.mGlobalInput.Operator);
                          mLACommisionBSet.add(tLACommisionBSchema);
                        }
                      }

                }
                else if(tTableName.equals("LACommisionDetail"))
                {
                    tSql.append("UPDATE " + tTableName + " SET AgentGroup = '" +
                                this.mNewAgentGroup + "',");
                    tSql.append("ModifyDate = to_date('" + currentDate + "','yyyy-mm-dd') ,");
                    tSql.append("ModifyTime = '" + currentTime + "' ,");
                    tSql.append("Operator = '" + mGlobalInput.Operator + "' ");
                    tSql.append("Where 1=1 ");
                    if (mAgentCodeStr != null && !mAgentCodeStr.equals(""))
                        tSql.append("And AgentCode in (" + mAgentCodeStr + ") ");
                    if (mOldAgentGroupStr != null && !mOldAgentGroupStr.equals(""))
                        tSql.append("AND AgentGroup in (" + this.mOldAgentGroupStr +") ");
                    if (mSpecialCondition!=null && !mSpecialCondition.equals(""))
                        tSql.append("AND "+ mSpecialCondition);

                      String strSQL;
                      strSQL = "select * from LACommisionDetail ";
                      strSQL += tSql.substring(tSql.toString().indexOf("Where"));
                      LACommisionDetailSet tLACommisionDetailSet=new LACommisionDetailSet();
                      LACommisionDetailDB tLACommisionDetailDB=new LACommisionDetailDB();
                      LACommisionDetailSchema tLACommisionDetailSchema;
                      LACommisionDetailBSchema tLACommisionDetailBSchema;
                      tLACommisionDetailSet=tLACommisionDetailDB.executeQuery(strSQL);
                      Reflections tReflections = new Reflections();
                      if(tLACommisionDetailSet.size()>0)
                      {
                         for(int j=1;j<=tLACommisionDetailSet.size();j++)
                         {
                             tLACommisionDetailSchema=new LACommisionDetailSchema();
                             tLACommisionDetailSchema=tLACommisionDetailSet.get(j);
                             tLACommisionDetailBSchema=new LACommisionDetailBSchema();
                             tReflections.transFields(tLACommisionDetailBSchema,tLACommisionDetailSchema);
                             tLACommisionDetailBSchema.setEdorNo(mEdorNo);
                             tLACommisionDetailBSchema.setEdorType("91");
                             tLACommisionDetailBSchema.setModifyDate(currentDate);
                             tLACommisionDetailBSchema.setModifyTime(currentTime);
                             tLACommisionDetailBSchema.setOperator(this.mGlobalInput.Operator);
                             mLACommisionDetailBSet.add(tLACommisionDetailBSchema);
                         }
                      }
                }
                else if(tTableName.equals("LASPayPerson")||tTableName.equals("LJSPayPerson"))
                {
                    tSql.append("UPDATE " + tTableName + " SET ");
                    if (tTableName.equals("LASPayPerson"))
                    {
                        tSql.append("AgentGroup = '" + this.mNewBranchCode + "',");
                        tSql.append("BranchSeries = '" + this.mNewBranchSeries + "',");
                        tSql.append("Distict = '" + this.mNewBranchSeries.substring(0, 12) + "',");
                        tSql.append("Department = '" + this.mNewBranchSeries.substring(13, 25) + "',");
                    }
                    else
                    {
                        tSql.append("AgentGroup = '" + this.mNewAgentGroup + "',");
                    }
                    tSql.append("ModifyDate = to_date('" + currentDate + "','yyyy-mm-dd') ,");
                    tSql.append("ModifyTime = '" + currentTime + "', ");
                    tSql.append("Operator = '" + mGlobalInput.Operator + "' ");
                    //tSql.append("Where not exists (select 'Y' from lccontstate where contno = " + tTableName + ".contno and insuredno = '000000' and polno = " + tTableName + ".polno and statetype = 'Terminate' and state = '1' and (startdate<=to_date('"+mAdjustDate+"','yyyy-mm-dd') and (enddate>=to_date('"+mAdjustDate+"','yyyy-mm-dd') or enddate is null))) and actupayflag='0' ");
                    tSql.append("Where LastPaytoDate>='"+mAdjustDate2+"' ");

                    if (mAgentCodeStr != null && !mAgentCodeStr.equals(""))
                        tSql.append("And AgentCode in (" + mAgentCodeStr + ") ");
                    if (mOldAgentGroupStr != null && !mOldAgentGroupStr.equals(""))
                        tSql.append("AND AgentGroup in (" + this.mOldAgentGroupStr + ") ");
                    if (mSpecialCondition!=null && !mSpecialCondition.equals(""))
                        tSql.append("AND "+ mSpecialCondition);

                }
                else
                {
                    tSql.append("UPDATE " + tTableName + " SET AgentGroup = '" +
                                this.mNewAgentGroup + "', ");
                    tSql.append("ModifyDate = to_date('" + currentDate + "','yyyy-mm-dd') ,");
                    tSql.append("ModifyTime = '" + currentTime + "' ,");
                    tSql.append("Operator = '" + mGlobalInput.Operator + "' ");
                    tSql.append("Where 1=1 ");

                    if (mAgentCodeStr != null && !mAgentCodeStr.equals(""))
                        tSql.append("And AgentCode in (" + mAgentCodeStr + ") ");
                    if (mOldAgentGroupStr != null && !mOldAgentGroupStr.equals(""))
                        tSql.append("AND AgentGroup in (" + this.mOldAgentGroupStr +") ");
                    if (mSpecialCondition!=null && !mSpecialCondition.equals(""))
                        tSql.append("AND "+ mSpecialCondition);
                    //已经迁移待分配的保单不能归属 zxs 2006-3-13
                    if (tTableName.equals("LCCont")||tTableName.equals("LCPol"))
                    {
                        tSql.append(" AND trim(managecom)='"+mManageCom+"' ");
                    }
                    //加入保全变更的判断
                    if (tTableName.equals("LPCont")||tTableName.equals("LPPol"))
                    {
                        tSql.append(" AND trim(managecom)='"+mManageCom+"' ");
                        tSql.append(" and exists (select 1 from lpedoritem b where b.edorType = "+tTableName+".edorType and b.edorNo = "+tTableName+".edorNo and b.contno = "+tTableName+".contno and b.edorstate in ('1', '2', '3', '5', '6', 'a')) ");
                    }
                }
                System.out.println("SQL:" + tSql.toString());
                mSQLstr.add(tSql);
            }
        }
        return true;
    }
    //修改整个团队的业绩
    public boolean setGrptoGroup(String cGroup,LABranchGroupSchema cLABranchGroupSchema)
    {
       // mEdorNo=cEdorNo;
//       update
       String tAgentGroup=cLABranchGroupSchema.getAgentGroup();
       String tBranchCode=cLABranchGroupSchema.getAgentGroup();
       String tBranchAttr=cLABranchGroupSchema.getBranchAttr();
       String tBranchSeries=cLABranchGroupSchema.getBranchSeries();

       String tSql = "select * from lacommision where agentgroup='" +
             cGroup+ "' and (caldate>='" +
             mAStartDate + "' or caldate is null)";
       LACommisionSet tLACommisionSet = new LACommisionSet();
       LACommisionDB tLACommisionDB = new LACommisionDB();
       tLACommisionSet = tLACommisionDB.executeQuery(tSql);
       for (int j = 1; j <= tLACommisionSet.size(); j++) {
           LACommisionSchema tLACommisionSchema = new LACommisionSchema();
           tLACommisionSchema = tLACommisionSet.get(j);
           tLACommisionSchema.setAgentGroup(tAgentGroup);
           tLACommisionSchema.setBranchCode(tBranchCode);
           tLACommisionSchema.setBranchSeries(tBranchSeries);
           tLACommisionSchema.setBranchAttr(tBranchAttr);
           tLACommisionSchema.setModifyDate(currentDate);
           tLACommisionSchema.setModifyTime(currentTime);
           tLACommisionSchema.setOperator(mGlobalInput.Operator);
           this.mLACommisionSet.add(tLACommisionSchema);
            }

       return true ;
    }
    //修改个人的业绩
    public boolean setPertoGroup(String cAgentCode,LABranchGroupSchema cLABranchGroupSchema)
    {
        String tAgentGroup=cLABranchGroupSchema.getAgentGroup();
        String tBranchCode=cLABranchGroupSchema.getAgentGroup();
        String tBranchAttr=cLABranchGroupSchema.getBranchAttr();
        String tBranchSeries=cLABranchGroupSchema.getBranchSeries();

        String tSql = "select * from lacommision where agentcode='" +
              cAgentCode+ "' and (caldate>='" +
              mAStartDate + "' or caldate is null)";
        LACommisionSet tLACommisionSet = new LACommisionSet();
        LACommisionDB tLACommisionDB = new LACommisionDB();
        tLACommisionSet = tLACommisionDB.executeQuery(tSql);
        for (int j = 1; j <= tLACommisionSet.size(); j++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema = tLACommisionSet.get(j);
            tLACommisionSchema.setAgentGroup(tAgentGroup);
            tLACommisionSchema.setBranchCode(tBranchCode);
            tLACommisionSchema.setBranchSeries(tBranchSeries);
            tLACommisionSchema.setBranchAttr(tBranchAttr);
            tLACommisionSchema.setModifyDate(currentDate);
            tLACommisionSchema.setModifyTime(currentTime);
            tLACommisionSchema.setOperator(mGlobalInput.Operator);
            this.mLACommisionSet.add(tLACommisionSchema);
        }

       return true ;
       // mEdorNo=cEdorNo;
    }
    //修改区经理的业绩（branchcode与agentgroup不同）
    public boolean setPertoGroup(String cAgentCode,String cBranchCode,LABranchGroupSchema cLABranchGroupSchema)
    {
        String tAgentGroup=cLABranchGroupSchema.getAgentGroup();
       // String tBranchCode=cLABranchGroupSchema.getAgentGroup();
        String tBranchAttr=cLABranchGroupSchema.getBranchAttr();
        String tBranchSeries=cLABranchGroupSchema.getBranchSeries();

        String tSql = "select * from lacommision where agentcode='" +
              cAgentCode+ "' and (caldate>='" +
              mAStartDate + "' or caldate is null)";
        LACommisionSet tLACommisionSet = new LACommisionSet();
        LACommisionDB tLACommisionDB = new LACommisionDB();
        tLACommisionSet = tLACommisionDB.executeQuery(tSql);
        for (int j = 1; j <= tLACommisionSet.size(); j++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema = tLACommisionSet.get(j);
            tLACommisionSchema.setAgentGroup(tAgentGroup);
            tLACommisionSchema.setBranchCode(cBranchCode); //BranchCode与AgentGroup不一样
            tLACommisionSchema.setBranchSeries(tBranchSeries);
            tLACommisionSchema.setBranchAttr(tBranchAttr);
            tLACommisionSchema.setModifyDate(currentDate);
            tLACommisionSchema.setModifyTime(currentTime);
            tLACommisionSchema.setOperator(mGlobalInput.Operator);
            this.mLACommisionSet.add(tLACommisionSchema);
        }
        return true ;
       // mEdorNo=cEdorNo; mGlobalInput=(GlobalInput)
    }
    //修改个人的业绩
    public boolean setContToGroupNew(String cAgentCode,LABranchGroupSchema cLABranchGroupSchema)
    {
        String tAgentGroup=cLABranchGroupSchema.getAgentGroup();
        String tManagecom = cLABranchGroupSchema.getManageCom();
        //由于 更新时，老是锁表，所以，先对于数据进行查询，如果没有数据，就不进行更新
        //2017-8-18  yangyang
        String tQuerySQL =" select 'lccont',count from lccont where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"'"
        				+ " union "
        				+ " select 'lbcont',count from lbcont where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'lpcont',count from lpcont where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'lcpol',count from lcpol where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'lbpol',count from lbpol where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'lppol',count from lppol where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'lcgrpcont',count from lcgrpcont where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'lbgrpcont',count from lbgrpcont where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'lpgrpcont',count from lpgrpcont where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'lcgrppol',count from lcgrppol where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'lbgrppol',count from lbgrppol where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'lpgrppol',count from lpgrppol where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'ljspayperson',count from ljspayperson where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'ljspay',count from ljspay where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'ljspaygrp',count from ljspaygrp where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " union "
						+ " select 'ljsgetendorse',count from ljsgetendorse where managecom='"+tManagecom+"' and  agentcode = '"+cAgentCode+"' "
						+ " with ur";
        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(tQuerySQL);
        if(null!=tSSRS&&0<tSSRS.getMaxRow())
        {
        	for(int i =1 ;i<=tSSRS.getMaxRow();i++)
        	{
        		String tCount = tSSRS.GetText(i, 2);
        		if(!"0".equals(tCount))
        		{
        			String tTableName = tSSRS.GetText(i, 1);
        			String tUpdateSQL = "update "+tTableName+"  set  agentgroup='" + tAgentGroup+
                            "',ModifyTime = '" + currentTime +
                            "',ModifyDate = '" + currentDate +
                            "',operator='"+mGlobalInput.Operator+"'  where managecom='"+tManagecom+"' and agentcode='"+cAgentCode+"'";
        			 mSQLstr.add(tUpdateSQL);
        		}
        		
        	}
        }
//        String tSql = "update lccont  set  agentgroup='" + tAgentGroup+
//                      "',ModifyTime = '" + currentTime +
//                      "',ModifyDate = '" + currentDate +
//                      "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
//        mSQLstr.add(tSql);
//        tSql = "update lbcont  set  agentgroup='" + tAgentGroup+
//        "',ModifyTime = '" + currentTime +
//        "',ModifyDate = '" + currentDate +
//        "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
//        mSQLstr.add(tSql);
//        tSql = "update lpcont  set  agentgroup='" + tAgentGroup+
//        "',ModifyTime = '" + currentTime +
//        "',ModifyDate = '" + currentDate +
//        "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
//        mSQLstr.add(tSql);
//        tSql = "update lcpol  set  agentgroup='" + tAgentGroup+
//               "',ModifyTime = '" + currentTime +
//               "',ModifyDate = '" + currentDate +
//               "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
//        mSQLstr.add(tSql);
//        tSql = "update lbpol  set  agentgroup='" + tAgentGroup+
//        "',ModifyTime = '" + currentTime +
//        "',ModifyDate = '" + currentDate +
//        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
//        mSQLstr.add(tSql);
//        tSql = "update lppol  set  agentgroup='" + tAgentGroup+
//        "',ModifyTime = '" + currentTime +
//        "',ModifyDate = '" + currentDate +
//        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
//        mSQLstr.add(tSql);
//        tSql = "update lcgrpcont  set  agentgroup='" + tAgentGroup+
//                     "',ModifyTime = '" + currentTime +
//                     "',ModifyDate = '" + currentDate +
//                     "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
//       mSQLstr.add(tSql);
//       tSql = "update lbgrpcont  set  agentgroup='" + tAgentGroup+
//       "',ModifyTime = '" + currentTime +
//       "',ModifyDate = '" + currentDate +
//       "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
//       mSQLstr.add(tSql);
//       tSql = "update lpgrpcont  set  agentgroup='" + tAgentGroup+
//       "',ModifyTime = '" + currentTime +
//       "',ModifyDate = '" + currentDate +
//       "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
//       mSQLstr.add(tSql);
//       tSql = "update lcgrppol  set  agentgroup='" + tAgentGroup+
//              "',ModifyTime = '" + currentTime +
//              "',ModifyDate = '" + currentDate +
//              "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
//       mSQLstr.add(tSql);
//       tSql = "update lbgrppol  set  agentgroup='" + tAgentGroup+
//       "',ModifyTime = '" + currentTime +
//       "',ModifyDate = '" + currentDate +
//       "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
//       mSQLstr.add(tSql);
//       tSql = "update lpgrppol  set  agentgroup='" + tAgentGroup+
//       "',ModifyTime = '" + currentTime +
//       "',ModifyDate = '" + currentDate +
//       "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
//       mSQLstr.add(tSql);
//        tSql = "update ljspayperson  set  agentgroup='" + tAgentGroup+
//               "',ModifyTime = '" + currentTime +
//               "',ModifyDate = '" + currentDate +
//               "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
//        mSQLstr.add(tSql);
//        tSql = "update ljspay  set  agentgroup='" + tAgentGroup+
//               "',ModifyTime = '" + currentTime +
//               "',ModifyDate = '" + currentDate +
//               "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
//        mSQLstr.add(tSql);
//        tSql = "update ljspaygrp  set  agentgroup='" + tAgentGroup+
//              "',ModifyTime = '" + currentTime +
//              "',ModifyDate = '" + currentDate +
//              "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
//       mSQLstr.add(tSql);
//       tSql = "update ljsgetendorse  set  agentgroup='" + tAgentGroup+
//              "',ModifyTime = '" + currentTime +
//              "',ModifyDate = '" + currentDate +
//              "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
//       mSQLstr.add(tSql);

       return true ;
       // mEdorNo=cEdorNo;
    }
    public void setmEdorNo(String cEdorNo)
    {
        mEdorNo=cEdorNo;
    }
    public void setAstartDate(String cAstartDate)
    {
        mAStartDate=cAstartDate;
    }
    public void setGlobalInput(GlobalInput cGlobalInput)
       {
           mGlobalInput=cGlobalInput;
    }
}
