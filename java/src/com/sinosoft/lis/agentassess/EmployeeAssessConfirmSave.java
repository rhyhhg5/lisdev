package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;
import com.sinosoft.lis.vschema.LAAssessAccessorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class EmployeeAssessConfirmSave
{
    //错误处理类
    public static CErrors mErrors = new CErrors();
    //业务处理相关变量
    /** 全局数据 */
    private VData mInputData = new VData();
    public GlobalInput mGlobalInput = new GlobalInput();
    private String m_IndexCalNo = "";
    private String m_ManageCom = "";
    private String m_AgentGrade = "";

    public EmployeeAssessConfirmSave()
    {
    }

    public static void main(String[] args)
    {
        EmployeeAssessConfirmSave EmployeeAssessConfirmSave1 = new
                EmployeeAssessConfirmSave();

        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "aa";

        VData tVData = new VData();
        tVData.clear();
        tVData.add(tGI);
        tVData.add(0, "86110000");
        tVData.add(2, "Y01");
        tVData.add(1, "200402");

//    if ( !EmployeeAssessConfirmSave1.getInputData(tVData) )  {
//      System.out.println("failed!" + mErrors.getFirstError());
//    }
//
//    if ( !EmployeeAssessConfirmSave1.dealAll() )  {
//      System.out.println("failed!" + mErrors.getFirstError());
//    }
    }

    public boolean dealAll()
    {
        LAAssessAccessorySchema tSch = new LAAssessAccessorySchema();
        LAAssessAccessorySet tSet = new LAAssessAccessorySet();
        LAAssessAccessoryDB tDB = new LAAssessAccessoryDB();
        //修改：2004-05-12 LL
        //放开了校验职级的条件
        String tSQL = "Select * from LAAssessAccessory where managecom like '"
                      + this.m_ManageCom.trim()
                      + "%' and IndexCalNo = '"
                      + this.m_IndexCalNo.trim() + "'"
                      + " and AssessType = '01' and state = '0'";
        if (!this.m_AgentGrade.trim().equals(""))
        {
            tSQL = tSQL + " and AgentGrade = '" + this.m_AgentGrade.trim() +
                   "'";
        }

        System.out.println("---qry LAAssessAccessory---Sql : " + tSQL);
        tSet = tDB.executeQuery(tSQL);

        if (!dealData(tSet))
        {
            return false;
        }

        return true;
    }


    public boolean dealData(LAAssessAccessorySet tSet)
    {
        System.out.println("----come into dealData---");
        System.out.println("----LAAssessAccessorySet.size: " + tSet.size());

//  zsj delete: 要整体确认，无需校验
//    if ( !verifyData() )
//      return false;

        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            dealError("dealData", "数据库连接失败！");
            return false;
        }

        int tCount = tSet.size();
        if (tCount == 0)
        {
            dealError("DealData", "未找到考核信息！");
            return false;
        }

        try
        {
            conn.setAutoCommit(false);
            for (int i = 1; i <= tCount; i++)
            {
                LAAssessAccessorySchema tSch = new LAAssessAccessorySchema();
                tSch = tSet.get(i);

                if (!updLAAssessAccessory(tSch, conn))
                {
                    conn.rollback();
                    conn.close();
                    dealError("dealData", "刷新失败！");
                    return false;
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}

            dealError("dealData", ex.toString());
            return false;
        }
        return true;
    }

    private boolean updLAAssessAccessory(LAAssessAccessorySchema tSch,
                                         Connection conn)
    {
        System.out.println("---come into updLAAssessAccessory---");
        LAAssessAccessoryDB tDB = new LAAssessAccessoryDB(conn);
        LAAssessAccessorySchema tOldSch = new LAAssessAccessorySchema();
        PubFun tPF = new PubFun();

        LAAssessAccessoryDB tDB1 = new LAAssessAccessoryDB();
        tDB1.setAgentCode(tSch.getAgentCode());
        tDB1.setIndexCalNo(tSch.getIndexCalNo());
        tDB1.setAssessType("01");
        if (!tDB1.getInfo())
        {
            return false;
        }
        tOldSch.setSchema(tDB1.getSchema());

        tOldSch.setConfirmer(this.mGlobalInput.Operator);
        tOldSch.setConfirmDate(tPF.getCurrentDate());
        tOldSch.setState("1");
        tOldSch.setModifyDate(tPF.getCurrentDate());
        tOldSch.setModifyTime(tPF.getCurrentTime());

        String tAgentGrade = tSch.getAgentGrade().trim();
        String tCalAgentGrade = tSch.getCalAgentGrade().trim();
        if (tAgentGrade.equals(tCalAgentGrade))
        {
            tOldSch.setModifyFlag("02");
        }

        if (tAgentGrade.compareToIgnoreCase(tCalAgentGrade) < 0)
        {
            tOldSch.setModifyFlag("03");
        }
        else
        {
            tOldSch.setModifyFlag("01");
        }

        tDB.setSchema(tOldSch);
        if (!tDB.update())
        {
            return false;
        }

        return true;
    }

//  private boolean verifyData()  {
//    System.out.println("---come into verifyData---");
//    LAAssessSchema tSch = new LAAssessSchema();
//    int tCount = this.mLAAssessSet.size();
//    for ( int i=1;i<=tCount;i++ )  {
//      tSch = this.mLAAssessSet.get(i);
//      if ( tSch.getAgentGrade1() == null || tSch.getAgentGrade1() == "")
//      {
//        dealError("verifyData","建议的职级或系列或确认人不得为空值!");
//        return false;
//      }
//    }
//    return true;
//  }

    public boolean getInputData(VData cInputData)
    {
        try
        {
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
            this.m_ManageCom = (String) cInputData.getObjectByObjectName(
                    "String", 0);
            this.m_IndexCalNo = (String) cInputData.getObjectByObjectName(
                    "String", 1);
            this.m_AgentGrade = (String) cInputData.getObjectByObjectName(
                    "String", 2);
            System.out.println("---m_IndexCalNo: " + this.m_IndexCalNo);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }

        if ((this.m_AgentGrade == null || this.m_AgentGrade == "")
            || (this.m_IndexCalNo == null || this.m_IndexCalNo == "")
            || (this.m_ManageCom == null || this.m_ManageCom == "")
                )
        {
            dealError("getInputdata", "没有传入足够数据！");
            return false;
        }
        return true;
    }

    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "EmployeeAssessConfirmSave";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }
} //end class
