package com.sinosoft.lis.agentassess;


import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentwages.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: LACrsSaleRateBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author xin
 * @version 1.0
 */
public class LACrsSaleRateBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";

  private MMap map = new MMap();
  public  GlobalInput mGlobalInput = new GlobalInput();

  private LACrsSaleRateSet mLACrsSaleRateSet  = new LACrsSaleRateSet();

  public LACrsSaleRateBL() {

  }

  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAActiveChargeBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LAActiveChargeBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAActiveChargeBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LAActiveChargeBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     this.mLACrsSaleRateSet.set((LACrsSaleRateSet) cInputData.get(1));
     System.out.println("LACrsSaleRateSet get"+mLACrsSaleRateSet.size());
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LACrsSaleRateSet";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LACrsSaleRateSet.dealData........."+mOperate);
    try {
          LACrsSaleRateSet tLACrsSaleRateSetNew = new LACrsSaleRateSet();
    	  for (int i = 1; i <= mLACrsSaleRateSet.size(); i++) {
    		  LACrsSaleRateSchema tLACrsSaleRateSchemanew = new  LACrsSaleRateSchema();
    		  LACrsSaleRateSchema tLACrsSaleRateSchemaold = new  LACrsSaleRateSchema();
    		  LACrsSaleRateDB tLACrsSaleRateDB = new LACrsSaleRateDB();
    		  tLACrsSaleRateSchemanew = mLACrsSaleRateSet.get(i);
    		  String managecom = tLACrsSaleRateSchemanew.getManageCom();
    		  String branchtype = tLACrsSaleRateSchemanew.getBranchType();
    		  String branchtype2 = tLACrsSaleRateSchemanew.getBranchType2();
    		  String assessFlag = tLACrsSaleRateSchemanew.getAssessFlag();
    		  tLACrsSaleRateDB.setManageCom(managecom);
    		  tLACrsSaleRateDB.setAssessFlag(assessFlag);
    		  tLACrsSaleRateDB.setBranchType(branchtype);
    		  tLACrsSaleRateDB.setBranchType2(branchtype2);
    		  if(tLACrsSaleRateDB.getInfo())
    		  {
    			  tLACrsSaleRateSchemaold = tLACrsSaleRateDB.getSchema();
    			  if(mOperate.equals("INSERT"))
    			  {
    				  String assessflagname ="";
    				  if("1".equals(assessFlag))
    				  {
    					  assessflagname = "月均互动开拓提奖";
    				  }
    				  else  if("2".equals(assessFlag))
    				  {
    					  assessflagname = "季度规模保费";
    				  }
    				  CError tError = new CError();
    				  tError.moduleName = "LACrsSaleRateSet";
    				  tError.functionName = "dealData";
    				  tError.errorMessage = "管理机构"+managecom+"下考核方式为"+assessflagname+"的比例已经存在！";
    				  this.mErrors.addOneError(tError);
    				  break;
    			  }
    			  if(mOperate.equals("UPDATE"))
    			  {
//    				  tLACrsSaleRateSchemaold.setManageCom(managecom);
//    				  tLACrsSaleRateSchemaold.setAssessFlag(assessFlag);
    				  tLACrsSaleRateSchemaold.setRate(tLACrsSaleRateSchemanew.getRate());
    				  tLACrsSaleRateSchemaold.setModifyDate(PubFun.getCurrentDate());
    				  tLACrsSaleRateSchemaold.setModifyTime(PubFun.getCurrentTime());
    				  tLACrsSaleRateSetNew.add(tLACrsSaleRateSchemaold);
    			  }
    		  }
    		  else
    		  {
    			  tLACrsSaleRateSchemanew.setOperator(this.mGlobalInput.Operator);
    			  tLACrsSaleRateSchemanew.setMakeDate(PubFun.getCurrentDate());
    			  tLACrsSaleRateSchemanew.setMakeTime(PubFun.getCurrentTime());
    			  tLACrsSaleRateSchemanew.setModifyDate(PubFun.getCurrentDate());
    			  tLACrsSaleRateSchemanew.setModifyTime(PubFun.getCurrentTime());
    			  tLACrsSaleRateSetNew.add(tLACrsSaleRateSchemanew);
    		  }
              
          }
            map.put(tLACrsSaleRateSetNew, mOperate);
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LACrsSaleRateSet";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LACrsSaleRateSet.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LACrsSaleRateSet";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
