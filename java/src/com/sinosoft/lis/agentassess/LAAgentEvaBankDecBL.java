package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.agent.ALADimissionAppUI;

/**
 * <p>Title:银代业务经理考核确认和归属 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAAgentEvaBankDecBL {
    public LAAgentEvaBankDecBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 提交数据的容器 */
    private MMap mMap = new MMap();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSet mLAAssessSet = new LAAssessSet(); //传入前台送入的考核记录
    private LAAssessSet mLAAssessOutSet = new LAAssessSet(); //保全考核确认后的结果

    private LAAssessHistorySchema mLAAssessHistorySchema = new
            LAAssessHistorySchema();

    private String mManageCom = ""; // 管理机构
    private String mIndexCalNo = ""; // 考核序列号
    private String mQuerySql = ""; // 考核sql
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    public static void main(String args[]) {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "001";
        LAAssessHistorySchema aLAAssessHistorySchema = new
                LAAssessHistorySchema();
        // 准备数据
        aLAAssessHistorySchema.setManageCom("8611");
        aLAAssessHistorySchema.setIndexCalNo("200612");
        aLAAssessHistorySchema.setBranchType("2");
        aLAAssessHistorySchema.setBranchType2("01");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(aLAAssessHistorySchema);
        System.out.println("add over");
        LAAgentEvaBankDecBL m = new LAAgentEvaBankDecBL();
        boolean tag = m.submitData(tVData, "");
        if (tag) {
            System.out.println("ok");
        } else {
            System.out.println("fail");
        }
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        //统一提交
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        if (tPubSubmit.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecGrpYearBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        mInputData.clear();
        mMap.put(mLAAssessOutSet, "UPDATE");
        mInputData.add(mMap);
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */

    private boolean dealData() {
    	LAAssessSet tLAAssessDealSet = new LAAssessSet();
    	LAAssessDB tLAAssessDB = new LAAssessDB();
    	mLAAssessSet = tLAAssessDB.executeQuery(this.mQuerySql);
    	if(mLAAssessSet==null||mLAAssessSet.size()<=0){
			CError tError = new CError();
			tError.moduleName = "LAAgentEvaBankDecBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有需要处理的数据！";
			System.out.println("没有需要处理的数据！");
			this.mErrors.addOneError(tError);
			return false;
    	}
    	LAAssessSchema tLAAssessSchema = new LAAssessSchema();
    	int tAgentCount = mLAAssessSet.size(); //需要处理的人数
    	//循环处理每个人的考核信息
    	for (int i = 1; i <= tAgentCount; i++) {
    		tLAAssessSchema = dealAssessAgent(mLAAssessSet.get(i).getSchema());
    		//如果更新信息成功
    		if (tLAAssessSchema != null) {
    			tLAAssessDealSet.add(tLAAssessSchema);
    		} else {
    			CError tError = new CError();
    			tError.moduleName = "LAAgentEvaBankDecBL";
    			tError.functionName = "dealData";
    			tError.errorMessage = "更新个人考核信息失败！";
    			System.out.println("更新个人考核信息失败！");
    			this.mErrors.addOneError(tError);
    			return false;
    		}
    	}
    	mLAAssessOutSet = tLAAssessDealSet;
    	

    	return true;
    }
    /**
     * 处理每个人的调整信息
     * @param pmLAAssessSchema LAAssessSchema
     * @return LAAssessSchema
     */
    private LAAssessSchema dealAssessAgent(LAAssessSchema pmLAAssessSchema) {
        //设置条件
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(pmLAAssessSchema.getAgentCode());
        tLATreeDB.getInfo();
        LATreeSchema tLATreeSchema = tLATreeDB.getSchema();

        LAAssessDB tLAAssessDB = new LAAssessDB();
        tLAAssessDB.setAgentCode(pmLAAssessSchema.getAgentCode());
        tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
        LAAssessSet tLAAssessSet = tLAAssessDB.query();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        //更新考核结果
        if (tLAAssessSet.size() != 0) {
            tLAAssessSchema = tLAAssessSet.get(1).getSchema();
        }
        //插入新的考核结果
        else { //插入新的考核结果
            tLAAssessSchema.setAgentCode(pmLAAssessSchema.getAgentCode());
            tLAAssessSchema.setIndexCalNo(this.mIndexCalNo);
            tLAAssessSchema.setAgentGroup(tLATreeSchema.getAgentGroup());

            if ("".equals(tLAAssessSchema.getBranchAttr()) ||
                tLAAssessSchema.getBranchAttr() == null) {
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setAgentGroup(tLATreeSchema.getAgentGroup());
                tLABranchGroupDB.getInfo();
                String tBranchAttr = tLABranchGroupDB.getSchema().getBranchAttr();
                tLAAssessSchema.setBranchAttr(tBranchAttr);
            }
            tLAAssessSchema.setManageCom(tLATreeSchema.getManageCom());
            tLAAssessSchema.setBranchType(tLATreeSchema.getBranchType());
            tLAAssessSchema.setBranchType2(tLATreeSchema.getBranchType2());
            tLAAssessSchema.setFirstAssessFlag("1");
            tLAAssessSchema.setStandAssessFlag("0"); //非正常考核
            tLAAssessSchema.setMakeDate(this.currentDate);
            tLAAssessSchema.setMakeTime(this.currentTime);
            tLAAssessSchema.setOperator(mGlobalInput.Operator);
        }

        //更新信息 (新职级)
        String tAgentGrade = pmLAAssessSchema.getAgentGrade();
        String tCalAgentGrade = pmLAAssessSchema.getCalAgentGrade();
        tLAAssessSchema.setAgentGrade(tAgentGrade);
        tLAAssessSchema.setAgentSeries(tLATreeSchema.getAgentSeries()); //银代需要保存业务职级
        tLAAssessSchema.setCalAgentGrade(tCalAgentGrade);
        tLAAssessSchema.setCalAgentSeries(tLATreeSchema.getAgentSeries()); //银代需要保存业务职级
        tLAAssessSchema.setAgentGrade1(tLAAssessSchema.getAgentGrade1());
        tLAAssessSchema.setAgentSeries1(tLAAssessSchema.getAgentSeries1()); //系列不变
        tLAAssessSchema.setState("01"); //01-调整确认 02-考核确认（类似于个险组织归属）
        tLAAssessSchema.setConfirmer(mGlobalInput.Operator);
        tLAAssessSchema.setConfirmDate(this.currentDate);
        tLAAssessSchema.setModifyDate(this.currentDate);
        tLAAssessSchema.setModifyTime(this.currentTime);
        //重新判断并设置升降级标记
        if ((tCalAgentGrade.compareTo(tAgentGrade) == 0)) {
            tLAAssessSchema.setModifyFlag("02");
        } else if (tCalAgentGrade.compareTo(tAgentGrade) > 0) {
            tLAAssessSchema.setModifyFlag("03");
        } else if (tCalAgentGrade.compareTo(tAgentGrade) < 0 ||
                   tCalAgentGrade.equals("F00")) {
            tLAAssessSchema.setModifyFlag("01");
        }
        return tLAAssessSchema;
    }

        /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        System.out.println(
                "Begin LAAgentEvaBankDecBL.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLAAssessHistorySchema.setSchema((LAAssessHistorySchema)
                                              cInputData.getObjectByObjectName(
                "LAAssessHistorySchema", 0));
        this.mQuerySql = (String) cInputData.getObject(2);
        this.mIndexCalNo = mLAAssessHistorySchema.getIndexCalNo();
        if (mGlobalInput == null||mQuerySql==null||mQuerySql=="") {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaBankDecBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (AgentPubFun.ConverttoYM(currentDate).compareTo(mIndexCalNo) <= 0) {
            // @@错误处理
            CError.buildErr(this, "考核年月不能大于或等于系统操作日期所在年月!");
            return false;
        }
        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql) {
        System.out.println(sql);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null) {
                return 0;
            }
            st = conn.prepareStatement(sql);
            if (st == null) {
                return 0;
            }
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
        if (!conn.isClosed()) {
            conn.close();
        }
        try {
            st.close();
            rs.close();
        } catch (Exception ex2) {
            ex2.printStackTrace();
        }
        st = null;
        rs = null;
        conn = null;
      } catch (Exception e) {}

        }
    }
    private boolean checkDimission(LAAssessSchema pmLAAssessSchema)
    {

        LADimissionDB tLADimissionDB=new LADimissionDB();
        LADimissionSet tLADimissionSet=new LADimissionSet();
        tLADimissionDB.setAgentCode(pmLAAssessSchema.getAgentCode());
        tLADimissionSet=tLADimissionDB.query();
        if(tLADimissionSet.size()>0)
        {
            for(int i=1;i<=tLADimissionSet.size();i++)
            {
                LADimissionSchema tLADimissionSchema=new LADimissionSchema();
                tLADimissionSchema=tLADimissionSet.get(i);
                if(tLADimissionSchema.getDepartState().compareTo("03") >=0)
                {
                     return false;
                }
            }
        }
        return true;
    }

    private void jbInit() throws Exception {
    }

}
