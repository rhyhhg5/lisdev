/*
 * <p>ClassName: OLAArchieveBL </p>
 * <p>Description: OLAArchieveBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:05:58
 */
package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.LATreeTempSchema;

import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;


public class DealLATreeUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */

  public CErrors mErrors = new CErrors();

  private VData mResult = new VData();


  /** 往后面传输数据的容器 */

  private VData mInputData = new VData();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  private String mBranchType = "";
  private String mBranchType2 = "";
  /** 数据操作字符串 */
  private String mOperate;
  private String currentDate = PubFun.getCurrentDate();
  private String currentTime = PubFun.getCurrentTime();
  /** 业务处理相关变量 */
  private LATreeTempSet mLATreeTempSet = new LATreeTempSet();
  private LATreeSet mdealLATreeSet = new LATreeSet();
  private LATreeSet mdealDifLATreeSet = new LATreeSet();
  private MMap mMap = new MMap();

  public DealLATreeUI()
  {
  }

  public static void main(String[] args)
  {
  }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContRateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败DealLATreeUI-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "DealLATreeUI";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
       if(mLATreeTempSet!=null && mLATreeTempSet.size()>=1)
       {
           for (int i = 1; i <= mLATreeTempSet.size(); i++) {
               LATreeTempSchema tLATreeTempSchema = new LATreeTempSchema();
               tLATreeTempSchema = mLATreeTempSet.get(i);
               LATreeSchema tLATreeSchema = new LATreeSchema();
               LATreeDB tLATreeDB = new LATreeDB();
               tLATreeDB.setAgentCode(tLATreeTempSchema.getAgentCode());
               tLATreeDB.getInfo();
               tLATreeSchema = tLATreeDB.getSchema();
               //如果变更职级与原职级相同，则不进行处理，返回上一条，进行下一条处理
               if (tLATreeSchema.getAgentGrade().equals(tLATreeTempSchema.
                       getAgentGrade())) {
                   continue;
               }
               String tLastAgentGrade = tLATreeSchema.getAgentGrade();
               String tLastAgentSeries = AgentPubFun.getAgentSeries(
                       tLastAgentGrade);
               if (tLastAgentSeries == null || tLastAgentSeries.equals("")) {
                   CError tError = new CError();
                   tError.moduleName = "LAAssessGrpInputBL";
                   tError.functionName = "dealData";
                   tError.errorMessage = "获取职级" + tLastAgentGrade +
                                         "所对应的代理人系列返回为空!";
                   this.mErrors.addOneError(tError);
                   return false;
               }
               String tNewAgentGrade = tLATreeTempSchema.getAgentGrade();
               String tNewAgentSeries = AgentPubFun.getAgentSeries(
                       tNewAgentGrade);
               if (tNewAgentSeries == null || tNewAgentSeries.equals("")) {
                   CError tError = new CError();
                   tError.moduleName = "LAAssessGrpInputBL";
                   tError.functionName = "dealData";
                   tError.errorMessage = "获取职级" + tNewAgentGrade +
                                         "所对应的代理人系列返回为空!";
                   this.mErrors.addOneError(tError);
                   return false;
               }
               if (tLastAgentSeries.equals(tNewAgentSeries)) { //处理同序列
                   tLATreeSchema.setAgentGrade(tLATreeTempSchema.getAgentGrade());
                   tLATreeSchema.setStartDate(tLATreeTempSchema.getStartDate());
                   mdealLATreeSet.add(tLATreeSchema);
               } else { //处理不同序列
                   tLATreeSchema.setAgentGrade(tLATreeTempSchema.getAgentGrade());
                   tLATreeSchema.setStartDate(tLATreeTempSchema.getStartDate());
                   mdealDifLATreeSet.add(tLATreeSchema);
               }
           }
           //同序列职级处理
           if (mdealLATreeSet != null && mdealLATreeSet.size() > 0) {
               VData tInputData = new VData();
               tInputData.add(this.mGlobalInput);
               tInputData.add(this.mdealLATreeSet);
               tInputData.add(this.mBranchType);
               tInputData.add(this.mBranchType2);
               DealLATree tDealLATree = new DealLATree();
               if (!tDealLATree.submitData(tInputData, mOperate)) {
                   // @@错误处理
                   this.mErrors.copyAllErrors(tDealLATree.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "DealLATreeUI";
                   tError.functionName = "submitData";
                   tError.errorMessage = "数据提交失败!";
                   this.mErrors.addOneError(tError);
                   return false;
               }
               mMap = tDealLATree.getResult();
           }
           //不同序列职级处理
           if (mdealDifLATreeSet != null && mdealDifLATreeSet.size() > 0) {
               VData tInputData = new VData();
               tInputData.add(this.mGlobalInput);
               tInputData.add(this.mdealDifLATreeSet);
               tInputData.add(this.mBranchType);
               tInputData.add(this.mBranchType2);
               DealDifLATree tDealDifLATree = new DealDifLATree();
               if (!tDealDifLATree.submitData(tInputData, mOperate)) {
                   // @@错误处理
                   this.mErrors.copyAllErrors(tDealDifLATree.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "LAAssessGrpInputUI";
                   tError.functionName = "submitData";
                   tError.errorMessage = "数据提交失败!";
                   this.mErrors.addOneError(tError);
                   return false;
               }
               mMap = tDealDifLATree.getResult();
           }
       }

        return true;
    }

    public VData getResult()
    {
        this.mResult.clear();
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLATreeTempSet.set((LATreeTempSet) cInputData.
                                           getObjectByObjectName(
                    "LATreeTempSet",0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput",
                    0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {

        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DealLATreeUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    }

