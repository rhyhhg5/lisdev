package com.sinosoft.lis.agentassess;

/**
 * <p>Title: 中英团险考核工厂</p>
 * <p>Description: 中英团险考核工厂</p>
 * <p>Copyright: Copyright (c) sinosoft 2004</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public class ACGrpAssessFactory implements AssessFactoryInterface
{
    //职级考核
    public GradeAssessPublic createGradeAssesss()
    {
        return new ACGrpGrdAssessComBL();
    }

    //职称考核
    public PositionAssessPublic createPositionAssess()
    {
        return null;
    }
}
