package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

 /**
  * <p>Title: </p>
  * <p>Description:  银代业务经理考核确认和归属UI类</p>
  * <p>Copyright: Copyright (c) 2007</p>
  * <p>Company: sinosoft</p>
  * @author Howie
  * @version 1.0
 */
public class LAAssessToConfirmUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";
    private String mIndexCalNo = "";
    private String mBranchType = "";
    private String mBranchType2 = "";

    public static void main(String args[])
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "mak001";
        tG.ComCode = "001";
        tG.ManageCom = "8611";

        LAAssessHistorySchema tLAAssessHistorySchema = new LAAssessHistorySchema();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAssessSet tLAAssessSet = new LAAssessSet();
        tLAAssessSchema.setAgentCode("1104000005");
        tLAAssessSchema.setBranchAttr("861100000104");
        tLAAssessSchema.setAgentGrade("F01");
        tLAAssessSchema.setCalAgentGrade("F00");
        tLAAssessSet.add(tLAAssessSchema);

        tLAAssessHistorySchema.setManageCom("8611");
        tLAAssessHistorySchema.setIndexCalNo("200710");
        tLAAssessHistorySchema.setBranchType("3");
        tLAAssessHistorySchema.setBranchType2("01");
       // tLAAssessHistorySchema.setAgentGrade("D01");

        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tLAAssessSet);
        tVData.add(tLAAssessHistorySchema);

        LAAssessToConfirmUI tLAAgentEvaBankDecUI = new LAAssessToConfirmUI();
        if(tLAAgentEvaBankDecUI.submitData(tVData,""))
        {
            System.out.println("成功了！");
        }else
        {
            System.out.println("失败了！");
        }
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        LAAssessToConfirmBL tLAAssessToConfirmBL = new LAAssessToConfirmBL();
        if(!tLAAssessToConfirmBL.submitData(cInputData,cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAssessToConfirmBL.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 验证方法
     * @return boolean
     */
    private boolean check()
    {
        return true;
    }

    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAssessToConfirmUI.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mManageCom = (String)cInputData.getObject(1);
        this.mIndexCalNo =(String)cInputData.getObject(2);
        this.mBranchType =(String)cInputData.getObject(3);
        this.mBranchType2 =(String)cInputData.getObject(4);
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessToConfirmUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
