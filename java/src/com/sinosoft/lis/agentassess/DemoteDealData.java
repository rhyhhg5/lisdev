package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.agentcalculate.DealBusinessData;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 代理人降级归属数据处理</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinoosoft </p>
 * @author:    zhangsj
 * @version 1.0
 */

public class DemoteDealData
{
    //错误处理类
    public static CErrors mErrors = new CErrors();
    //业务处理相关变量
    /** 全局数据 */
    private VData mInputData = new VData();
    private VData mOutputData = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSchema mLAAssessSchema = new LAAssessSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema(); //被降级代理人信息
    private LATreeSet mLATreeSet = new LATreeSet();
    private String mUpAgent = "";
    private String mAscriptGroup = "";
    private String mDealType = ""; //P--被降级的代理人，G--普通业务员
    private Connection conn;
    private DealRelaTab DealTab = new DealRelaTab();
    private String mIndexCalNo = "";
    private String[] MonDate = new String[2];
    private String mDirAgentGroup = "";
    private String mDirAttr = "";

    public DemoteDealData()
    {
    }

    public boolean dealPsnData(VData cInputData, Connection conn)
    {
        this.mInputData = cInputData;
        if (!getPsnInputData())
        {
            return false;
        }

        if (!prepareOutputData())
        {
            return false;
        }

        if (!this.DealTab.getInputData(mOutputData))
        {
            dealError("dealGrpData", "DealRelaTab.java 获取数据失败！");
            return false;
        }

        //插入代理人信息转储表
        if (!this.DealTab.insLAAgentB(this.mLATreeSchema.getAgentCode(), conn))
        {
            return false;
        }
        //刷新代理人信息表
        if (!this.DealTab.updateLAAgent(this.mLATreeSchema, this.mAscriptGroup,
                                        conn))
        {
            return false;
        }
        //插入代理人行政信息转储表
        if (!this.DealTab.insertLATreeB(this.mLATreeSchema, conn))
        {
            return false;
        }
        //刷新代理人行政信息表
        //只有被降级的代理人才能调用DealRelaTab.java的updateLATree()方法
        if (!this.DealTab.updateLATree(this.mLATreeSchema, this.mLAAssessSchema,
                                       this.mUpAgent, this.mAscriptGroup, conn))
        {
            return false;
        }

        if (!this.mLATreeSchema.getAgentSeries().equals("B"))
        {
            if (!dealDirAgency(conn))
            {
                dealError("dealGrpData", "处理直辖机构失败！");
                return false;
            }
        }

        //刷新代理人考评信息表
        if (!this.DealTab.updateLAAssess(this.mLAAssessSchema,
                                         this.mLATreeSchema,
                                         this.mAscriptGroup, conn))
        {
            return false;
        }

        //刷新展业机构表（不删除，只是刷新）：将BranchAddressCode置为IndexCalNo、EndFlag='Y'、EndDate置为下月一号
        if (!updDismissGroup(conn))
        {
            return false;
        }

        //更新业绩前要校验
        AgentPubFun tAPF = new AgentPubFun();
        String tCheckFlag = tAPF.AdjustCommCheck(this.mLATreeSchema.
                                                 getAgentCode().
                                                 trim(), this.MonDate[0]);
        if (!tCheckFlag.trim().equals("00"))
        {
            dealError("updLACommision", tCheckFlag.trim());
            return false;
        }

        //若为组长降为理财服务专员，则需要调用CalFormDrawRate，因为业绩需要打折
        //zsj-delete--2004-3-9:因为每月计算佣金之前会处理打折的情况，所以归属时不需要对业绩打折
//    if (this.mLAAssessSchema.getAgentSeries().equals("B")
//        && this.mLAAssessSchema.getAgentGrade1().equals("A01")) {
//
//
//      CalFormDrawRate tCalRate = new CalFormDrawRate();
//      LACommisionDB tDB = new LACommisionDB();
//      LACommisionDBSet tDBSet = new LACommisionDBSet(conn);
//      LACommisionSet tSet = new LACommisionSet();
//      LACommisionSet tResult = new LACommisionSet();
//
//      String tSQL = "select * from lacommision where trim(agentcode) = '"
//          + this.mLATreeSchema.getAgentCode().trim() + "' and (caldate >= '"
//          + this.MonDate[0].trim()
//          + "' or caldate is null) order by polno,ReceiptNo";
//      System.out.println("--updLACommision--D--tSQL : " + tSQL);
//
//      tSet = tDB.executeQuery(tSQL);
//      int tSize = tSet.size();
//      System.out.println("---updLACommision : size = " + tSize);
//      if (tSize == 0)
//        return true;
//
//      VData tInputData = new VData();
//      tInputData.clear();
//      tInputData.add(tSet);
//      tInputData.add(0, "A01");
//
//      if (!tCalRate.submitData(tInputData,"RECALFYC||AGENTWAGE")) {
//        dealError("updLACommision", tCalRate.mErrors.getFirstError());
//        return false;
//      }
//
//      tResult = (LACommisionSet) tCalRate.getResult().getObjectByObjectName(
//          "LACommisionSet", 0);
//
//      int tCount = tResult.size();
//      if (tSize != tCount) {
//        dealError("updLACommision", "传入类CalFormDrawRate.java中的数据和传出的数据条数不一致！");
//        return false;
//      }
//
//      if (!tDBSet.set(tResult)) {
//        dealError("updLACommision", tDBSet.mErrors.getFirstError());
//        return false;
//      }
//
//      if (!tDBSet.update()) {
//        dealError("updLACommision", tDBSet.mErrors.getFirstError());
//        return false;
//      }
//    }
//

        //生成新组，刷新业务数据
        DealBusinessData tDeal = new DealBusinessData();
        VData tV = new VData();
        tV.clear();
        tV.add(0, "");
        tV.add(1, "");
        tV.add(2, "");
        tV.add(3, "");
        tV.add(4, "");
        tV.add(5, "");

        tV.set(0, this.mLATreeSchema.getAgentCode());
        if (this.mLAAssessSchema.getAgentSeries().trim().equalsIgnoreCase("B"))
        {
            tV.set(1, this.mAscriptGroup);
            tV.set(2, "Y");
            LABranchGroupDB tBussDB = new LABranchGroupDB();
            tBussDB.setAgentGroup(this.mAscriptGroup);
            if (!tBussDB.getInfo())
            {
                return false;
            }
            tV.set(3, tBussDB.getBranchAttr().trim());
            tV.set(5, this.mAscriptGroup);
        }
        else
        {
            tV.set(1, this.mDirAgentGroup);
            tV.set(2, "N");
            tV.set(3, this.mDirAttr);
            tV.set(5, "");
        }
        tV.set(4, this.mIndexCalNo);

        if (!tDeal.updateAgentGroup(tV, conn))
        {
            dealError("dealPersonData",
                      "刷新业务数据失败！" + tDeal.mErrors.getFirstError());
            return false;
        }
        return true;
    }


    public boolean updDismissGroup(Connection conn)
    {
        LABranchGroupDB tDB = new LABranchGroupDB(conn);
        LABranchGroupSchema tSch = new LABranchGroupSchema();
        PubFun tPF = new PubFun();

        String tAgentGroup = this.mLATreeSchema.getAgentGroup().trim();
        tDB.setAgentGroup(tAgentGroup);
        if (!tDB.getInfo())
        {
            dealError("updDismissGroup", "刷新被降级代理人的展业机构信息失败！");
            return false;
        }

        tSch.setSchema(tDB.getSchema());
        tSch.setBranchAddressCode(this.mIndexCalNo);
        tSch.setEndFlag("Y");
        tSch.setEndDate(this.MonDate[0]);
        tSch.setModifyDate(tPF.getCurrentDate());
        tSch.setModifyTime(tPF.getCurrentTime());
        tSch.setOperator(this.mGlobalInput.Operator);

        tDB.setSchema(tSch);
        if (!tDB.update())
        {
            dealError("updDismissGroup",
                      "刷新被降级代理人展业机构失败！" + tDB.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    public boolean dealGrpData(VData cInputData, Connection conn)
    {
        this.mInputData = cInputData;
        if (!getGrpInputData())
        {
            return false;
        }

        if (!prepareOutputData())
        {
            return false;
        }

        if (!this.DealTab.getInputData(this.mOutputData))
        {
            dealError("dealGrpData", "DealRelaTab.java 获取数据失败！");
            return false;
        }

        // 生成新机构号，初始化类
        LABranchGroupDB tDB = new LABranchGroupDB();
        tDB.setAgentGroup(this.mAscriptGroup);
        if (!tDB.getInfo())
        {
            return false;
        }
        String tUpBranchAttr = tDB.getBranchAttr().trim();
        CreateBranchAttr tCreateNo = CreateBranchAttr.getInstance();

        LATreeSchema tSch = new LATreeSchema();
        int tCount = this.mLATreeSet.size();

        if (this.mLATreeSchema.getAgentSeries().equals("B"))
        {
            for (int i = 1; i <= tCount; i++)
            {
                tSch.setSchema(this.mLATreeSet.get(i));
                if (!dealA(tSch, conn))
                {
                    return false;
                }
            }
        }

        if (this.mLATreeSchema.getAgentSeries().equals("C"))
        {
            if (!tCreateNo.init(tUpBranchAttr, "Y", 18))
            {
                return false;
            }
            for (int i = 1; i <= tCount; i++)
            {
                tSch.setSchema(this.mLATreeSet.get(i));
                if (!dealB(tSch, "Y", conn))
                {
                    return false;
                }
            }
        }

        if (this.mLATreeSchema.getAgentGrade().equals("A08"))
        {
            if (!tCreateNo.init(tUpBranchAttr, "Y", 15))
            {
                return false;
            }
            for (int i = 1; i <= tCount; i++)
            {
                tSch.setSchema(this.mLATreeSet.get(i));
                if (!dealC(tSch, "Y", conn))
                {
                    return false;
                }
            }
        }

        if (this.mLATreeSchema.getAgentGrade().equals("A09"))
        {
            if (!tCreateNo.init(tUpBranchAttr, "Y", 12))
            {
                return false;
            }
            for (int i = 1; i <= tCount; i++)
            {
                tSch.setSchema(this.mLATreeSet.get(i));
                if (!dealD(tSch, "Y", conn))
                {
                    return false;
                }
            }
        }
        return true;
    }

    //业务经理降级为理财主任：
    private boolean dealA(LATreeSchema tSch, Connection conn)
    {
        if (!this.DealTab.insLAAgentB(tSch.getAgentCode(), conn))
        {
            return false;
        }
        if (!this.DealTab.updateLAAgent(tSch, this.mAscriptGroup, conn))
        {
            return false;
        }
        if (!this.DealTab.insertLATreeB(tSch, conn))
        {
            return false;
        }
        if (!updLATree(tSch, conn))
        {
            return false;
        }

        //代理人归属新组，刷新业务数据
        DealBusinessData tDeal = new DealBusinessData();
        VData tV = new VData();
        tV.clear();
        tV.add(0, tSch.getAgentCode());
        tV.add(1, this.mAscriptGroup);
        tV.add(2, "Y");

        LABranchGroupDB tBussDB = new LABranchGroupDB();
        tBussDB.setAgentGroup(this.mAscriptGroup);
        if (!tBussDB.getInfo())
        {
            return false;
        }
        tV.add(3, tBussDB.getBranchAttr().trim());
        tV.add(4, this.mIndexCalNo);
        tV.add(5, this.mAscriptGroup);

        if (!tDeal.updateAgentGroup(tV, conn))
        {
            dealError("dealPersonData", "刷新业务数据失败！");
            return false;
        }

        return true;
    }

    //高级经理降级为业务经理：
    private boolean dealB(LATreeSchema tSch, String Flag, Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();

        CreateBranchAttr tCreateNo = CreateBranchAttr.getInstance();
        String tNewGrpAttr = tCreateNo.getTeamNo();

        //当处理的机构是被降级代理人的直辖机构时，将生成的外部展业机构代码赋给全局变量，
        //当刷新被降级代理人业绩时用来刷新BranchAttr
        if (Flag.trim().equals("N"))
        {
            this.mDirAttr = tNewGrpAttr.trim();
        }
        System.out.println("-----Gen TeamNo : " + tNewGrpAttr);

        // 归属机构主管
        if (!AscriptMng(tSch, tNewGrpAttr, Flag, conn))
        {
            return false;
        }

        //因为外部展业机构编码改变，所以尽管AgentGroup没有改变，
        //但LACommision表的BranchAttr仍需更新
        if (!updLACommision(tSch.getAgentGroup().trim(), tNewGrpAttr, conn))
        {
            return false;
        }

        return true;
    }

    //督导长降级为高级经理：
    private boolean dealC(LATreeSchema tSch, String Flag, Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();

        // 生成新机构号
        CreateBranchAttr tCreateNo = CreateBranchAttr.getInstance();
        String tNewGrpAttr = tCreateNo.getDepNo();
        System.out.println("-----Gen DepNo : " + tNewGrpAttr);

        // 归属机构主管
        if (!AscriptMng(tSch, tNewGrpAttr, Flag, conn))
        {
            return false;
        }

        // 归属主管的下属机构
        if (!AscriptBottom(tSch.getAgentGroup(), tNewGrpAttr, conn))
        {
            return false;
        }

        if (Flag.trim().equals("Y"))
        {
            if (!updLACommision(tSch.getAgentGroup(), this.mDirAttr, conn))
            {
                return false;
            }
        }
        return true;
    }

    //区域督导长降级为督导长：
    private boolean dealD(LATreeSchema tSch, String Flag, Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();
        LABranchGroupDB tDB = new LABranchGroupDB();
        LABranchGroupSet tSet = new LABranchGroupSet();
        LABranchGroupSchema tS = new LABranchGroupSchema();
        String tNewAttr = "";

        // 生成新机构号
        CreateBranchAttr tCreateNo = CreateBranchAttr.getInstance();
        //生成一个督导部的BranchAttr
        String tNewGrpAttr = tCreateNo.getSDepNo();
        System.out.println("-----Gen SDepNo : " + tNewGrpAttr);

        // 归属机构主管
        if (!AscriptMng(tSch, tNewGrpAttr, Flag, conn))
        {
            return false;
        }

        // 归属下一级
        String sql = "SELECT * FROM LABranchGroup WHERE UpBranch = '" +
                     tSch.getAgentGroup().trim() + "' and endflag <> 'Y'";
        tSet = tDB.executeQuery(sql);
        int tCount = tSet.size();
        //没有下级机构，不做处理，返回true;--zsj--modified--2004-3-2
        if (tCount <= 0)
        {
            return true;
        }

        for (int i = 1; i <= tCount; i++)
        {
            tS = tSet.get(i);
            //生成一个新的营业部的BranchAttr
            tNewAttr = tCreateNo.getDepNo();
            System.out.println("-----Gen DepNo : " + tNewAttr);

            // 归属下一级的下一级
            if (!AscriptBottom(tS.getAgentGroup(), tNewAttr, conn))
            {
                return false;
            }

            if (!this.DealTab.insertLABranchGroupB(tS.getAgentGroup(), "N",
                    conn))
            {
                return false;
            }
            if (!this.DealTab.updateLABranchGroup(tS.getAgentGroup(), tNewAttr,
                                                  "",
                                                  "N", conn))
            {
                return false;
            }
            if (Flag.trim().equals("Y"))
            {
                if (!updLACommision(tSch.getAgentGroup(), this.mDirAttr, conn))
                {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean AscriptBottom(String tUpGroup, String tAttr,
                                  Connection conn)
    {
        LABranchGroupDB tDB = new LABranchGroupDB();
        LABranchGroupSet tSet = new LABranchGroupSet();
        LABranchGroupSchema tSch = new LABranchGroupSchema();
        CreateBranchAttr tCreateNo = CreateBranchAttr.getInstance();
        PubFun1 tPF1 = new PubFun1();
        String tNewAttr = "";

        //EndFlag<>'Y'的目的是为了防止已经被停业的机构被归属的新的机构下面
        String sql = "SELECT * FROM LABranchGroup WHERE UpBranch = '" +
                     tUpGroup.trim() + "' and endflag <> 'Y'";
        tSet = tDB.executeQuery(sql);
        int tCount = tSet.size();
        if (tCount <= 0)
        {
            return false;
        }

        for (int i = 1; i <= tCount; i++)
        {
            tSch = tSet.get(i);
            tNewAttr = tCreateNo.getTeamNo();
            System.out.println("---Gen TeamNo : " + tNewAttr);

            //将直辖组的BranchAttr赋给全局变量，
            //用于更新被降级代理人LACommision表的BranchAttr
            if (tSch.getUpBranchAttr().trim().equals("1"))
            {
                this.mDirAttr = tNewAttr.trim();
            }

            if (!updLACommision(tSch.getAgentGroup().trim(), tNewAttr, conn))
            {
                return false;
            }

            if (!this.DealTab.insertLABranchGroupB(tSch.getAgentGroup(), "N",
                    conn))
            {
                return false;
            }
            if (!this.DealTab.updateLABranchGroup(tSch.getAgentGroup(),
                                                  tNewAttr, "",
                                                  "N", conn))
            {
                return false;
            }
        }
        return true;
    }

//  private boolean updBusinessDataAttr(LABranchGroupSchema tBranchSch,String tNewAttr,Connection conn)
//  {
//    String tAgentCode = tBranchSch.getBranchManager().trim();
//    String tAgentGroup = tBranchSch.getAgentGroup().trim();
//
//    DealBusinessData tDeal = new DealBusinessData();
//
//    VData tInputData = new VData();
//    tInputData.clear();
//    tInputData.add(0,tAgentCode);
//    tInputData.add(1,tAgentGroup);
//    tInputData.add(2,"N");
//    tInputData.add(3,tNewAttr);
//    tInputData.add(4,this.mIndexCalNo);
//    tInputData.add(5,"");
//
//    if ( !tDeal.updateAgentGroup(tInputData,conn) )  {
//      dealError("updBusinessDataAttr", "刷新业绩失败！" + tDeal.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }
//

    private boolean AscriptMng(LATreeSchema tSch, String tAttr, String Flag,
                               Connection conn)
    {
        // 归属机构主管
        if (Flag.trim().equals("Y"))
        {
            if (!this.DealTab.insertLATreeB(tSch, conn))
            {
                dealError("AscriptMng", "归属机构主管时插入行政信息转储表失败！");
                return false;
            }

            if (!updLATree(tSch, conn))
            {
                dealError("AscriptMng", "归属机构主管刷新行政信息表失败！");
                return false;
            }
        }

        if (!this.DealTab.insertLABranchGroupB(tSch.getAgentGroup(), "N", conn))
        {
            dealError("AscriptMng", "归属机构主管时插入展业机构转储表失败！");
            return false;
        }

        if (!this.DealTab.updateLABranchGroup(tSch.getAgentGroup(), tAttr,
                                              this.mAscriptGroup, "U", conn))
        {
            dealError("AscriptMng", "归属机构主管时刷新展业机构表失败！");
            return false;
        }
        return true;
    }

    private boolean updLACommision(String tAgentGroup, String tNewAttr,
                                   Connection conn)
    {

        PubFun tPF = new PubFun();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tAgentGroup);
        tLABranchGroupDB.getInfo();

        //校验是否可以更新业绩
        AgentPubFun tAPF = new AgentPubFun();

        ExeSQL tExe = new ExeSQL(conn);
        String tSql = "update lacommision set branchattr = '"
                      + tNewAttr.trim() + "' where branchattr = '"
                      + tLABranchGroupDB.getBranchAttr().trim() + "'";
        System.out.println(tSql);

        if (!tExe.execUpdateSQL(tSql))
        {
            dealError("updLACommision",
                      "刷新组员业绩表的BranchAttr失败！" + tExe.mErrors.getFirstError());
            return false;
        }
        return true;
    }


    private boolean updLATree(LATreeSchema tSch, Connection conn)
    {
        LATreeDB tDB = new LATreeDB(conn);
        PubFun tPF = new PubFun();

        tSch.setUpAgent(this.mUpAgent);

        //归属业务经理以上职级的代理人agentgroup不变
        if (tSch.getAgentSeries().trim().equalsIgnoreCase("A"))
        {
            tSch.setAgentGroup(this.mAscriptGroup);
        }

        tSch.setModifyDate(tPF.getCurrentDate());
        tSch.setModifyTime(tPF.getCurrentTime());
        tSch.setAstartDate(this.MonDate[0]);
        tSch.setOperator(this.mGlobalInput.Operator);
        tDB.setSchema(tSch);
        System.out.println("--2-11--DemoteDealData--updLATree--Astartdate = " +
                           tDB.getAstartDate());
        if (!tDB.update())
        {
            dealError("updLATree", "刷新LATree失败！");
            return false;
        }
        return true;
    }

    /*
      private boolean dealDirAgency()  {
        LABranchGroupDB tBranchDB = new LABranchGroupDB();
        LATreeDB tTreeDB = new LATreeDB();
        LABranchGroupSet tBranchSet = new LABranchGroupSet();
        LATreeSet tTreeSet = new LATreeSet();
        LABranchGroupSchema tBranchSch = new LABranchGroupSchema();
        LATreeSchema tTreeSchema = new LATreeSchema();
        String tLevel = "";
        if (this.mLATreeSchema.getAgentSeries().trim().equals("B"))
          return true;
        if (this.mLATreeSchema.getAgentSeries().trim().equals("C"))
          tLevel = "01";
        if (this.mLATreeSchema.getAgentGrade().trim().equals("D1"))
          tLevel = "02";
        if (this.mLATreeSchema.getAgentGrade().trim().equals("D2"))
          tLevel = "03";
        // 查询直辖机构
     String sql_Branch = "SELECT * FROM LABranchGroup WHERE BranchManager = '" +
                     this.mLATreeSchema.getAgentCode().trim() +
                     "' AND BranchLevel = '" + tLevel.trim() + "'";
        tBranchSet = tBranchDB.executeQuery(sql_Branch);
        if ( tBranchSet.size() <= 0 )  {
          dealError("dealDirAgency","查询直辖机构失败！");
          return false;
        }
        tBranchSch = tBranchSet.get(1);
        // 为直辖机构生成新的AgentGroup
        PubFun1 tPF1 = new PubFun1();
        String tNewDirGroup = tPF1.CreateAttr(this.mAscriptGroup);
        if ( ! this.DealTab.insertLABranchGroupB(tBranchSch.getAgentGroup()) )
          return false;
        if ( !this.DealTab.updateLABranchGroup(tBranchSch.getAgentGroup(),tNewDirGroup,"U") )
          return false;
        return true;
      }*/

    private boolean dealDirAgency(Connection conn)
    {
        LATreeSchema tSch = new LATreeSchema();
        //tSch存的是被降级的代理人的LATreeSchema
        tSch.setSchema(this.mLATreeSchema);

        // 为查询直辖机构做准备
        String tLevel = "";
        String tGrade = this.mLATreeSchema.getAgentGrade().trim();
        String tSeries = this.mLATreeSchema.getAgentSeries().trim();

        if (tSeries.equals("C"))
        {
            tLevel = "01";
        }
        if (tGrade.equals("A08"))
        {
            tLevel = "02";
        }
        if (tGrade.equals("A09"))
        {
            tLevel = "03";
        }

        LABranchGroupDB tBranchDB = new LABranchGroupDB();
        LABranchGroupSet tBranchSet = new LABranchGroupSet();
        LABranchGroupSchema tBranchSch = new LABranchGroupSchema();
        String tSql = "";
        tSql = "select * from labranchgroup where branchmanager = '"
               + this.mLATreeSchema.getAgentCode().trim()
               + "' and branchlevel = '" + tLevel.trim() + "'";
        tBranchSet = tBranchDB.executeQuery(tSql);
        tBranchSch.setSchema(tBranchSet.get(1));

        //将直辖机构的组号赋给tSch，可以直接调用Deal*函数并且赋给全局变量，
        //当更新被降级的代理人的业绩时用来修改AgentGroup
        this.mDirAgentGroup = tBranchSch.getAgentGroup().trim();

        //将被降级代理人的AgentGroup赋值为直辖机构的AgentGroup
        tSch.setAgentGroup(tBranchSch.getAgentGroup());

        if (this.mLATreeSchema.getAgentSeries().trim().equals("C"))
        {
            if (!dealB(tSch, "N", conn))
            {
                return false;
            }
        }

        if (this.mLATreeSchema.getAgentGrade().trim().equals("A08"))
        {
            if (!dealC(tSch, "N", conn))
            {
                return false;
            }
        }

        if (this.mLATreeSchema.getAgentGrade().trim().equals("A09"))
        {
            if (!dealD(tSch, "N", conn))
            {
                return false;
            }
        }

        return true;
    }

//  private boolean updDemoteAgent(String tNewDirGroup)  {
//    LATreeDB tTreeDB = new LATreeDB(conn);
//    LAAgentDB tAgentDB = new LAAgentDB(conn);
//    LATreeSchema tTreeS = new LATreeSchema();
//    LAAgentSchema tAgentS = new LAAgentSchema();
//
//    tTreeDB.setAgentCode(this.mLATreeSchema.getAgentCode());
//    if ( !tTreeDB.getInfo() )
//      return false;
//    tTreeS = tTreeDB.getSchema();
//    tTreeS.setAgentGroup(tNewDirGroup);
//    tTreeDB.setSchema(tTreeS);
//    if ( !tTreeDB.update() )
//      return false;
//
//    tAgentDB.setAgentCode(this.mLATreeSchema.getAgentCode());
//    if ( !tAgentDB.getInfo() )
//      return false;
//    tAgentS = tAgentDB.getSchema();
//    tAgentS.setAgentGroup(tNewDirGroup);
//    tAgentDB.setSchema(tAgentS);
//    if ( !tAgentDB.update() )
//      return false;
//
//    return true;
//  }

    private boolean getPsnInputData()
    {
        this.mGlobalInput.setSchema((GlobalInput)this.mInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLAAssessSchema = (LAAssessSchema)this.mInputData.
                               getObjectByObjectName("LAAssessSchema", 0);
        this.mLATreeSchema = (LATreeSchema)this.mInputData.
                             getObjectByObjectName(
                                     "LATreeSchema", 0);
        this.mUpAgent = (String)this.mInputData.getObjectByObjectName("String",
                0);
        this.mAscriptGroup = (String)this.mInputData.getObjectByObjectName(
                "String",
                1);
        this.mIndexCalNo = (String)this.mInputData.getObjectByObjectName(
                "String",
                2);
        this.mDealType = "P";

        if (this.mGlobalInput == null || this.mLAAssessSchema == null ||
            this.mLATreeSchema == null)
        {
            // @@错误处理
            dealError("getInputData", "没有得到足够的信息！");
            return false;
        }
        this.MonDate = genDate();
        return true;
    }

    private boolean getGrpInputData()
    {
        this.mGlobalInput.setSchema((GlobalInput)this.mInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLATreeSet = (LATreeSet)this.mInputData.getObjectByObjectName(
                "LATreeSet", 0);
        this.mLATreeSchema = (LATreeSchema)this.mInputData.
                             getObjectByObjectName(
                                     "LATreeSchema", 0);
        this.mUpAgent = (String)this.mInputData.getObjectByObjectName("String",
                0);
        this.mAscriptGroup = (String)this.mInputData.getObjectByObjectName(
                "String",
                1);
        this.mIndexCalNo = (String)this.mInputData.getObjectByObjectName(
                "String",
                2);
        this.mDealType = "G";

        if (this.mGlobalInput == null || this.mLATreeSet == null ||
            this.mLATreeSchema == null)
        {
            // @@错误处理
            dealError("getInputData", "没有得到足够的信息！");
            return false;
        }

        this.MonDate = genDate();
        return true;
    }

    private String[] genDate()
    {
        String[] tMonDate = new String[2];
        PubFun tPF = new PubFun();

        String tYear = this.mIndexCalNo.trim().substring(0, 4);
        String tMon = this.mIndexCalNo.trim().substring(4, 6);

        //计算旧职级的止聘日期
        String tOldDate = tYear + "-" + tMon + "-01";
        tMonDate = tPF.calFLDate(tOldDate);
        String tOldDay = tMonDate[1];

        //计算新职级的起聘日期或转正日期
        if (tMon.equals("12"))
        {
            tMon = "01";
            tYear = Integer.toString(Integer.parseInt(tYear) + 1);
        }
        else
        {
            tMon = Integer.toString(Integer.parseInt(tMon) + 1);
        }
        String tDate = tYear.trim() + "-" + tMon.trim() + "-" + "01";
        tMonDate = tPF.calFLDate(tDate);
        String tNewDay = tMonDate[0];

        tMonDate[0] = tNewDay;
        tMonDate[1] = tOldDay;
        return tMonDate;
    }

    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "DoAscriptDemote";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mOutputData.add(this.mGlobalInput);
            this.mOutputData.add(this.mIndexCalNo);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("prepareOutputData", "准备输出数据失败！");
            return false;
        }
        return true;
    }

}
