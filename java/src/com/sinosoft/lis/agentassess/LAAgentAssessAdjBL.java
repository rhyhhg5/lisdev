package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 考核评估计算程序(考核调整)
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  Howie
 * @version 1.0
 */
public class LAAgentAssessAdjBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap = new MMap();
    private VData mOutputData = new VData();
    private LAAssessSet mLAAssessSet = new LAAssessSet();
    private LAAssessSet mLAAssessOutSet = new LAAssessSet();
    private LAAssessMainSet mLAAssessMainSet = new LAAssessMainSet();
    private String mManageCom = "";
    private String mIndexCalNo = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mGradeSeries = "";
    private String mTurnFlag = "";

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //处理前进行验证
        if (!check())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mOutputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            // @@错误处理
            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 验证方法
     * @return boolean
     */
    private boolean check()
    {
        //检验当期考核是否归属过
    	 System.out.println("mGradeSeries"+mGradeSeries);
        String tSQL = "";
        String tManagecom="";
        LAAssessDB tLAAssessDB=new LAAssessDB();
        for(int i=1;i<=this.mLAAssessSet.size();i++){

        	tLAAssessDB.setAgentCode(mLAAssessSet.get(i).getAgentCode());
        	tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
        	if(!tLAAssessDB.getInfo()){
        		CError.buildErr(this,"查找代理人管理机构信息出错！");
                return false;
        	}

        	tManagecom=tLAAssessDB.getManageCom();
        	System.out.println("...............here bl managecom"+tManagecom);
        	tSQL = "select count(*) from laassessmain where indexcalno='"
                + mIndexCalNo + "' and managecom='"
                + tManagecom + "' and state='2'"
                + " and BranchType='" + this.mBranchType + "'"
                + " and BranchType2='" + this.mBranchType2 + "'";
         //业务系列
         if (this.mGradeSeries.equals("0"))
         {
             tSQL += " and AgentGrade like 'A%' ";
         }
         //主管系列
         else if (this.mGradeSeries.compareTo("0")>0)
         {
             tSQL += " and AgentGrade like 'B%' ";
         }
         if (execQuery(tSQL) > 0)
         {
             CError.buildErr(this,"代理人:" +mLAAssessSet.get(i).getAgentCode()+
             		"所在管理机构本期考核该机构已经归属完毕,不能进行考核调整操作！，此次操作失败！");
             return false;
         }
        }


        //校验职级是否符合考核调整规则
        int count =  this.mLAAssessSet.size();
        String tCurAgentGrade = "";
        String tChkAgentGrade = "";
        String tTurnAgentGrade = "";
        String tAgentCode = "";
        for (int i = 1; i <= count; i++)
        {
            tAgentCode = this.mLAAssessSet.get(i).getAgentCode();
            tCurAgentGrade = this.mLAAssessSet.get(i).getAgentGrade();
            tChkAgentGrade = this.mLAAssessSet.get(i).getAgentGrade1();
            tTurnAgentGrade = this.mLAAssessSet.get(i).getTurnAgentGrade();
            //校验职级
            System.out.println("tTurnAgentGrade"+tTurnAgentGrade);
            System.out.println("tChkAgentGrade"+tChkAgentGrade);
            System.out.println("tCurAgentGrade"+tCurAgentGrade);
            System.out.println("mGradeSeries"+mGradeSeries);
            if (mGradeSeries.equals("0"))
            {
            if (!checkGrade(tCurAgentGrade, tChkAgentGrade))
              {
                  CError.buildErr(this,
                                  "营销员" + tAgentCode +
                                  "的调整后的确认职级不能为" +
                                  tChkAgentGrade+",只能为业务职级或处经理处经理职级！");
                  return false;
              }

//                if (!checkAgentGrade(tCurAgentGrade, tChkAgentGrade,tTurnAgentGrade))
//                {
//                    CError.buildErr(this,
//                                    "营销员" + tAgentCode +
//                                    "的调整后的确认职级不能为" +
//                                    tChkAgentGrade+",只能为上一职级或下一职级或套转职级！");
//                    return false;
//                }


            }
//            else if (mGradeSeries.equals("1"))
//            {
//                if (!tCurAgentGrade.equals("B01") && !tCurAgentGrade.equals("B02"))
//                {
//                    if (!checkAgentGrade(tCurAgentGrade, tChkAgentGrade,tTurnAgentGrade))
//                    {
//                        CError.buildErr(this,
//                                        "营销员" + tAgentCode +
//                                        "的调整后的确认职级不能为" +
//                                    tChkAgentGrade+",只能为上一职级或下一职级或套转职级！");
//                        return false;
//                    }
//                }
//            }

            else if (mGradeSeries.equals("1"))
          {

                  if (!checkGrade(tCurAgentGrade, tChkAgentGrade))
                  {
                      CError.buildErr(this,
                                      "营销员" + tAgentCode +
                                      "的调整后的确认职级不能为" +
                                  tChkAgentGrade+",只能为区经理职级或处经理处经理职级！");
                      return false;
                  }

          }



        }
        return true;
    }


    /**
     * 校验代理人的调整职级是否在当前职级调整范围内
     * @param curAgentGrade String
     * @param chkAgentGrade String
     * @return boolean
     */
    private boolean checkAgentGrade(String curAgentGrade, String chkAgentGrade,String turnAgentGrade)
    {
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String sql = "";
        StringBuffer sb = new StringBuffer();
        sql = "select gradecode from  laagentgrade where  branchtype='" +
              mBranchType + "' and branchtype2='"+mBranchType2+"'"
              +
                " and  abs(gradeid -(select b.gradeid from laagentgrade b where b.gradecode='" +
              curAgentGrade + "'))<=1  "
              + " order by gradecode ";
        try
        {
            tSSRS = tExeSQL.execSQL(sql);
            if (tSSRS.getMaxRow() < 0 || tSSRS == null)
            {
                CError.buildErr(this, "查询代理人职级信息失败！");
                return false;
            }
            int rows = tSSRS.getMaxRow();
            for (int i = 0; i < rows; i++)
            {
                sb.append(tSSRS.GetText(i + 1, 1));
            }
            //加入套转职级,在sb中索引确认职级
            sb.append(turnAgentGrade);
            //可包含清退A00
            sb.append("A00");
            if (sb.toString().indexOf(chkAgentGrade) == -1)
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            CError.buildErr(this, "查询代理人职级信息发生错误！");
            ex.printStackTrace();
        }
        return true;
    }



    /**
         * 校验代理人的调整职级是否在当前职级调整范围内
         * 只有不跨序列即可
         *
         * @param curAgentGrade String
         * @param chkAgentGrade String
         * @return boolean
         */
        private boolean checkGrade(String curAgentGrade, String chkAgentGrade)
        {
            if(chkAgentGrade.equals("A00")){
              return true;
            }
            try
            {
            String curAgentSeries=AgentPubFun.getAgentSeries(curAgentGrade);
            String chkAgentSeries=AgentPubFun.getAgentSeries(chkAgentGrade);
            if(   (curAgentSeries.equals("0")&&chkAgentSeries.equals("2"))
            ||(curAgentSeries.equals("2")&&chkAgentSeries.equals("0")))
            {
               CError.buildErr(this, "人员职级调整不能跨序列调整！业务职级不能与区经理职级相互调整！");
               return false;
            }
            }
            catch (Exception ex)
            {
                CError.buildErr(this, "查询代理人职级信息发生错误！");
                ex.printStackTrace();
            }
            return true;
    }


    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        try
        {
            mMap.put(this.mLAAssessOutSet, "UPDATE");
            this.mOutputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
        LAAssessSet tLAAssessDealSet = new LAAssessSet();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        int tAgentCount = mLAAssessSet.size(); //需要处理的人数
        //循环处理每个人的考核信息
        for (int i = 1; i <= tAgentCount; i++)
        {
            tLAAssessSchema = dealAssessAgent(mLAAssessSet.get(i));
            //如果更新信息成功
            if (tLAAssessSchema != null)
            {
                tLAAssessDealSet.add(tLAAssessSchema);
            }
            else
            {
                CError.buildErr(this,"更新个人考核信息失败！");
                return false;
            }
        }
        mLAAssessOutSet = tLAAssessDealSet;
        return true;
    }

    /**
     * 处理每个人的调整信息
     * @param pmLAAssessSchema LAAssessSchema
     * @return LAAssessSchema
     */
    private LAAssessSchema dealAssessAgent(LAAssessSchema pmLAAssessSchema)
    {
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAssessSet tLAAssessSet = new LAAssessSet();
        LAAssessDB tLAAssessDB = new LAAssessDB();
        String tAgentGrade = ""; // 原职级
        String tAgentGrade1 = ""; // 调整职级
        //设置条件
        tLAAssessDB.setAgentCode(pmLAAssessSchema.getAgentCode());
        tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
        tLAAssessSet = tLAAssessDB.query();
        if (tLAAssessSet.size() < 1)
        {
            return null;
        }
        tLAAssessSchema = tLAAssessSet.get(1).getSchema();
        //更新信息 (新职级)
        tAgentGrade = pmLAAssessSchema.getAgentGrade();
        tAgentGrade1 = pmLAAssessSchema.getAgentGrade1();
        tLAAssessSchema.setAgentGrade1(tAgentGrade1);
        tLAAssessSchema.setAgentSeries1(AgentPubFun.getAgentSeries(tAgentGrade1));
        //重新判断并设置升降级标记
        if ((tAgentGrade1.compareTo(tAgentGrade) == 0))
            tLAAssessSchema.setModifyFlag("02");
        else if (tAgentGrade1.compareTo(tAgentGrade) > 0)
            tLAAssessSchema.setModifyFlag("03");
        else if (tAgentGrade1.compareTo(tAgentGrade) < 0 ||
                 tAgentGrade1.equals("A00"))
            tLAAssessSchema.setModifyFlag("01");
        return tLAAssessSchema;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        this.mLAAssessSet.set((LAAssessSet) cInputData.getObjectByObjectName(
                "LAAssessSet", 0));
        this.mManageCom = (String)cInputData.getObject(2);
        this.mIndexCalNo = (String)cInputData.getObject(3);
        this.mBranchType = (String)cInputData.getObject(4);
        this.mBranchType2 = (String)cInputData.getObject(5);
        this.mGradeSeries = (String)cInputData.getObject(6);

        if (mGlobalInput == null)
        {
            // @@错误处理
            CError.buildErr(this,"没有得到足够的信息！");
            return false;
        }
        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql)
    {
        System.out.println(sql);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try
        {
            if (conn == null)
                return 0;
            st = conn.prepareStatement(sql);
            if (st == null)
                return 0;
            rs = st.executeQuery();
            if (rs.next())
            {
                return rs.getInt(1);
            }
            return 0;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return -1;
        } finally
        {
            try {
               if (!conn.isClosed()) {
                   conn.close();
               }
               try {
                   st.close();
                   rs.close();
               } catch (Exception ex2) {
                   ex2.printStackTrace();
               }
               st = null;
               rs = null;
               conn = null;
             } catch (Exception e) {}
        }
    }


    public static void main(String[] args)
    {
        LAAgentAssessAdjBL tCal = new LAAgentAssessAdjBL();
        String tManageCom = "8611";
        String tAgentSeries = "1";
        String tYear = "2007";
        String tMonth = "03";
        String tBranchType = "1";
        String tBranchType2 = "01";

        LAAssessDB tLAAssessDB = new LAAssessDB();
        tLAAssessDB.setIndexCalNo("200703");
        tLAAssessDB.setAgentCode("1101000007");
        LAAssessSet tLAAssessSet = new LAAssessSet();
        tLAAssessSet = tLAAssessDB.query();
        VData tVData;
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        tVData = new VData();

        tVData.add(tGlobalInput);
        tVData.addElement(tLAAssessSet);
        tVData.addElement(tManageCom);
        tVData.addElement(tYear+tMonth);
        tVData.addElement(tBranchType);
        tVData.addElement(tBranchType2);
        tVData.addElement(tAgentSeries);
        System.out.println("add over");

        try
        {
            boolean b = tCal.submitData(tVData, "INSERT||MAIN");
            if (b)
            {
                System.out.println("OK");
            }
            else
            {
                System.out.println("fail");
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }


}
