package com.sinosoft.lis.agentassess;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportActiveStandRateUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportActiveStandRateBL mLADiskImportActiveStandRateBL = null;

    public LADiskImportActiveStandRateUI()
    {
    	mLADiskImportActiveStandRateBL = new LADiskImportActiveStandRateBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLADiskImportActiveStandRateBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportActiveStandRateBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportActiveStandRateBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportActiveStandRateBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportActiveStandRateBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportActiveStandRateBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportActiveStandRateUI zLADiskImportBRCDSUI = new LADiskImportActiveStandRateUI();
    }
}
