package com.sinosoft.lis.agentassess;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAAgentRegularInputUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();
    private VData   mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    public LAAgentRegularInputUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData,String cOperate)
    {
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        LAAgentRegularInputBL tLAAgentRegularInputBL= new LAAgentRegularInputBL();
        try
        {
        	System.out.println("执行UI------------------");
            if (!tLAAgentRegularInputBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLAAgentRegularInputBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentRegularInputUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
 //       mResult=tLAAgentAssessBackBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
