package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.db.LARearRelationBDB;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vdb.LARearRelationDBSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.agentassess.AscriptAgentInfoOperateBL;
import com.sinosoft.lis.schema.LARecomRelationBSchema;
import com.sinosoft.lis.db.LARecomRelationDB;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.vschema.LARecomRelationBSet;
import com.sinosoft.lis.pubfun.MMap;

/**
 * <p>Title: AscriptRelationOperateBL</p>
 * <p>Description: 处理代理人在归属时的血缘关系</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
public class AscriptRelationOperateBL {

    public CErrors mErrors = new CErrors();
    public GlobalInput mGlobalInput = new GlobalInput();//操作员的信息

    private String mOldGradeEndDate; //降级人员原职级结束的日期，由BLF传入
    private String mNewGradeStartDate; //降级人员新职级开始的日期，由BLF传入
    private VData mOutputData=new VData(); //保存向外层传递的数据
    private String mEdorNo;

    private LARearRelationBSet mLARearRelationBSet=new LARearRelationBSet();
    private LARearRelationSet mLARearRelationSetInsert=new LARearRelationSet();
    private LARearRelationSet mLARearRelationSetUpdate=new LARearRelationSet();
    private LARearRelationSet mLARearRelationSetDelete=new LARearRelationSet();
    private LARecomRelationSet mLARecomRelationSet=new LARecomRelationSet();
    private LARecomRelationBSet mLARecomRelationBSet=new LARecomRelationBSet();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private MMap mMap = new MMap();
    public AscriptRelationOperateBL() {
    }

    public boolean getInputData(VData cInputData)
    {
        mOldGradeEndDate=(String)cInputData.get(0);
        mNewGradeStartDate=(String)cInputData.get(1);
        mGlobalInput=(GlobalInput)cInputData.getObjectByObjectName(
                     "GlobalInput",2);
        if(mNewGradeStartDate!=null&&!mNewGradeStartDate.equals("")
                &&
           mOldGradeEndDate!=null&&!mOldGradeEndDate.equals("")
                &&
           mGlobalInput!=null)
        {
            return true;
        }
        return false;
    }

    private boolean prepareOutputDataToBLS()
    {
        try
        {
            mOutputData.clear();
            mOutputData.add(mLARecomRelationSet);
            mOutputData.add(mLARecomRelationBSet);
            mOutputData.add(mLARearRelationBSet);
            mOutputData.add(mLARearRelationSetInsert);
            mOutputData.add(mLARearRelationSetUpdate);
            mOutputData.add(mLARearRelationSetDelete);

        }
        catch(Exception e)
        {
            DealErr("准备传向BLS的数据失败！","AscriptRelationOperateBL","prepareOutputDataToBLS");
            return false;
        }
        return true;
    }

    public VData getOutputDataToBLS()
    {
        if(!prepareOutputDataToBLS())
        {
            return null;
        }
        if(mOutputData!=null||mOutputData.size()>0)
        {
            return mOutputData;
        }
        return null;
    }

    private void DealErr(String cMsg,String cMName,String cFName)
    {
            CError tCError=new CError();
            tCError.moduleName=cMName;
            tCError.functionName=cFName;
            tCError.errorMessage=cMsg;
            mErrors.addOneError(tCError);
    }
    /*
         修改recomrelation 表的AgentGroup
        */
       public boolean changeRecomRelation(LATreeSet tLATreeSet,String tGroup)
       {
           if (tLATreeSet == null || tLATreeSet.size() <= 0) {
               return true;
           }
           for (int i = 1; i <= tLATreeSet.size(); i++) {
               LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();

               LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();

               tLARecomRelationDB.setAgentCode(tLATreeSet.get(i).getAgentCode());
               tLARecomRelationSet = tLARecomRelationDB.query();
               if (tLARecomRelationSet == null || tLARecomRelationSet.size() <= 0) {
                   continue;
               }
               for (int k = 1; k <= tLARecomRelationSet.size(); k++) {
                   LARecomRelationBSchema tLARecomRelationBSchema = new
                           LARecomRelationBSchema();
                   LARecomRelationSchema tLARecomRelationSchema = new
                           LARecomRelationSchema();
                   tLARecomRelationSchema=tLARecomRelationSet.get(k);

//                   Reflections tReflections = new Reflections();
//                   tReflections.transFields(tLARecomRelationBSchema,
//                                            tLARecomRelationSchema);
//                   tLARecomRelationBSchema.setEdorNo(mEdorNo);
//                   tLARecomRelationBSchema.setEdorType("01");
//                   tLARecomRelationBSchema.setMakeDate2(tLARecomRelationBSchema.
//                           getMakeDate());
//                   tLARecomRelationBSchema.setMakeTime2(tLARecomRelationBSchema.
//                           getMakeTime());
//                   tLARecomRelationBSchema.setModifyDate2(tLARecomRelationBSchema.
//                           getModifyDate());
//                   tLARecomRelationBSchema.setModifyTime2(tLARecomRelationBSchema.
//                           getModifyTime());
//                   tLARecomRelationBSchema.setMakeDate(currentDate);
//                   tLARecomRelationBSchema.setMakeTime(currentTime);
//                   tLARecomRelationBSchema.setModifyDate(currentDate);
//                   tLARecomRelationBSchema.setModifyTime(currentTime);

                   tLARecomRelationSchema.setAgentGroup(tGroup);
                   tLARecomRelationSchema.setModifyDate(currentDate);
                   tLARecomRelationSchema.setModifyTime(currentTime);
                   tLARecomRelationSchema.setOperator(mGlobalInput.Operator);

                   mLARecomRelationSet.add(tLARecomRelationSchema);
                   mLARecomRelationBSet.add(tLARecomRelationBSchema);
               }
           }
           return true;
       }
    /*
     建立、断裂血缘关系,只能由被育成人或者被增部人发起
     参数: cLATreeDB-建立关系的发起方的行政信息
     */
    public boolean createAgentRear(LATreeSchema cLATreeSchema) //不再用ascriptseries了，待改 20050705 zxs
    {
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        LARearRelationDB tLARearRelationDBIndirect = new LARearRelationDB(); //直接血缘关系
        String tFatherCode = "";
        String tFatherSeires = "";
        ExeSQL tExeSQL = new ExeSQL();
        String SQLstr = "select upagent from latree where agentcode='" +
                        cLATreeSchema.getAgentCode() + "'";
        System.out.println("SQLstr: " + SQLstr);
        tFatherCode = tExeSQL.getOneValue(SQLstr); //cLATreeSchema.getUpAgent();//直接和发起人的上级建立关系就好
         if (tFatherCode != null && !(tFatherCode.trim().equals(""))){
          ExeSQL mExeSQL = new ExeSQL();
          String mSQL = "select agentseries from latree where agentcode='" +
                         tFatherCode + "'";
          System.out.println("mSQL: " + mSQL);
          tFatherSeires = mExeSQL.getOneValue(mSQL);
        }
        if (tFatherCode != null && !(tFatherCode.trim().equals(""))&&(tFatherSeires.equals(cLATreeSchema.getAgentSeries())))
        { //取得了直接的关系人！
            tLARearRelationDB.setAgentCode(cLATreeSchema.getAgentCode());
            tLARearRelationDB.setRearedGens(1);
            if (cLATreeSchema.getAgentLastSeries().equals("1")&& !cLATreeSchema.getAgentLastGrade().equals("B01"))
            {
                tLARearRelationDB.setRearLevel("02");
            }
            else
            {
                tLARearRelationDB.setRearLevel("01");
            }
            tLARearRelationDB.setstartDate(mNewGradeStartDate);
            tLARearRelationDB.setEndDate("");
            tLARearRelationDB.setRearFlag("1");
            tLARearRelationDB.setAgentGroup(cLATreeSchema.getAgentGroup());
            tLARearRelationDB.setRearAgentCode(tFatherCode);
            tLARearRelationDB.setMakeDate(currentDate);
            tLARearRelationDB.setMakeTime(currentTime);
            tLARearRelationDB.setModifyDate(currentDate);
            tLARearRelationDB.setModifyTime(currentTime);
            tLARearRelationDB.setOperator(mGlobalInput.Operator);
            tLARearRelationDB.setRearCommFlag("1");
            tLARearRelationDB.setRearStartYear(1);
            //已经删除过了
//            if (!checkRear(tLARearRelationDB.getSchema()))
//            {
//                return false;
//            }
            mLARearRelationSetInsert.add(tLARearRelationDB.getSchema());

            //下面是间接关系
            //ExeSQL tExeSQL = new ExeSQL();
            SSRS rearR = new SSRS();
            SQLstr = "select RearAgentCode,RearLevel,RearedGens from larearrelation where agentcode='"
                     + tFatherCode + "' and rearlevel='"
                     + tLARearRelationDB.getRearLevel() +
                     "' and rearflag='1' order by rearedgens desc";
            System.out.println("CreateAgentRear中的查找间接关系的SQL: " + SQLstr);
            rearR = tExeSQL.execSQL(SQLstr);
            if (rearR.MaxRow > 0) {
                for (int i = 1; i <= rearR.MaxRow; i++) {
                    tLARearRelationDBIndirect.setAgentCode(cLATreeSchema.
                            getAgentCode());
                    tLARearRelationDBIndirect.setRearLevel(rearR.GetText(i,
                            2) /*getRearLevel()*/); //
                    tLARearRelationDBIndirect.setRearedGens(String.valueOf(
                            Integer.parseInt(rearR.GetText(i, 3))
                            /*getRearedGens()*/+ 1)); //
                    //当前检查的是2代关系,就用提奖金
                    tLARearRelationDBIndirect.setRearCommFlag("1");
                    tLARearRelationDBIndirect.setRearStartYear(1);

                    tLARearRelationDBIndirect.setRearAgentCode(rearR.
                            GetText(i, 1));
                    tLARearRelationDBIndirect.setstartDate(
                            mNewGradeStartDate);
                    tLARearRelationDBIndirect.setEndDate("");
                    tLARearRelationDBIndirect.setRearFlag("1");
                    tLARearRelationDBIndirect.setAgentGroup(cLATreeSchema.
                            getAgentGroup());
                    tLARearRelationDBIndirect.setMakeDate(currentDate);
                    tLARearRelationDBIndirect.setMakeTime(currentTime);
                    tLARearRelationDBIndirect.setModifyDate(currentDate);
                    tLARearRelationDBIndirect.setModifyTime(currentTime);
                    tLARearRelationDBIndirect.setOperator(mGlobalInput.
                            Operator);
//                    //检查是否已有此培养关系
//                    if (!checkRear(tLARearRelationDBIndirect.getSchema()))
//                    {
//                        return false;
//                    }
                    mLARearRelationSetInsert.add(
                            tLARearRelationDBIndirect.
                            getSchema());
                }
            }
        }

        return true;
    }

    /*
     断开血缘关系
     参数: cLATreeDB-断绝血缘关系的发起方的行政信息  ,向上的全部断开，向下的只断开同级别的关系，
     不同级别关系不变也不需要修改
     */
    public boolean BreakAgentRear(LATreeSchema cLATreeSchema)
    {
        LARearRelationDB tLARearRelationDB=new LARearRelationDB();
        LARearRelationSchema tLARearRelationSchema;
        //LATreeDB tLATreeDB=new LATreeDB();
        //LATreeSet tLATreeDBSet=new LATreeSet();
        String tRear="";
        if(cLATreeSchema.getAgentLastSeries().equals("2"))
        {
            tLARearRelationDB.setRearLevel("02");
        }
        else if(cLATreeSchema.getAgentLastSeries().equals("1"))
        {
            tLARearRelationDB.setRearLevel("01");
        }
        String tAgentCode=cLATreeSchema.getAgentCode();
        LARearRelationSet tLARearRelationSet=new LARearRelationSet();
        String SQLstr;
        //向下的只断开同级别的关系
        SQLstr="Select * from larearrelation where rearlevel='"+ tLARearRelationDB.getRearLevel() +
              // "' and ( agentcode='" +cLATreeSchema.getAgentCode() +
               "' and  rearagentcode='"+ cLATreeSchema.getAgentCode() + "' ";
        System.out.println("BreakAgentRear中查询所有现存关系的SQL： "+ SQLstr);
        tLARearRelationSet.add(tLARearRelationDB.executeQuery(SQLstr));
         //向上的全部删除
        SQLstr="Select * from larearrelation where agentcode='"+ cLATreeSchema.getAgentCode()+"'";
        tLARearRelationSet.add(tLARearRelationDB.executeQuery(SQLstr));
        //向上和向下的关系,全部删除
        //根据IT要求，不断裂间接养成关系，modified by miaoxz at 2010-7-6
//        SQLstr = "select * from larearrelation  where rearagentcode in "
//                         + "(select rearagentcode from larearrelation b "
//                         + "where  b.agentcode='" + tAgentCode + "') "
//                         +
//                         "and agentcode in (select agentcode from larearrelation b "
//                 + "where  b.rearagentcode='" + tAgentCode + "') and rearedgens > 1  ";
//        tLARearRelationSet.add(tLARearRelationDB.executeQuery(SQLstr));
//      modified by miaoxz at 2010-7-6 end here
        
        //下面要循环备份
        if(tLARearRelationSet.size()>0)
        {
            for(int i=1;i<=tLARearRelationSet.size();i++)
            {
                //tLARearRelationDB=new LARearRelationDB();
                tLARearRelationSchema=new LARearRelationSchema();
                tLARearRelationSchema.setSchema((LARearRelationSchema)
                                            tLARearRelationSet.get(i));
                if(!BackupRear(tLARearRelationSchema.getSchema()))
                {
                    DealErr("备份血缘关系失败！","AscriptRelationOperateBL","BreakAgentRear");
                    return false;
                }
                mLARearRelationSetDelete.add(tLARearRelationSchema.getSchema());
            }
        }
        mMap.put(mLARearRelationSetDelete,"DELETE");
        return true;
    }

    private boolean BackupRear(LARearRelationSchema cLARearRelationSchema)
    {
        Reflections tReflections=new Reflections();
        LARearRelationBSchema tLARearRelationBSchema=new LARearRelationBSchema();
        LARearRelationSchema tLARearRelationSchema=new LARearRelationSchema();
        tLARearRelationSchema=cLARearRelationSchema;
        tReflections.transFields(tLARearRelationBSchema,tLARearRelationSchema);
        tLARearRelationBSchema.setEdorNo(mEdorNo);
        tLARearRelationBSchema.setEdorType("01");
        tLARearRelationBSchema.setEndDate(mOldGradeEndDate);
        tLARearRelationBSchema.setOperator2(cLARearRelationSchema.getOperator());
        tLARearRelationBSchema.setMakeDate2(cLARearRelationSchema.getMakeDate());
        tLARearRelationBSchema.setModifyDate2(cLARearRelationSchema.getModifyDate());
        tLARearRelationBSchema.setMakeDate2(cLARearRelationSchema.getMakeTime());
        tLARearRelationBSchema.setModifyTime2(cLARearRelationSchema.getModifyTime());
        tLARearRelationBSchema.setMakeDate(currentDate);
        tLARearRelationBSchema.setMakeTime(currentTime);
        tLARearRelationBSchema.setModifyDate(currentDate);
        tLARearRelationBSchema.setModifyTime(currentTime);
        tLARearRelationBSchema.setOperator(mGlobalInput.Operator);

        mLARearRelationBSet.add(tLARearRelationBSchema);
        mMap.put(mLARearRelationBSet,"INSERT");
        return true;
    }

    public boolean DealRelationState(String cAgentCode)
    {
        String SQLstr;
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        //把发起人的所有关系（增部或育成）找到，备份后断绝
        SQLstr = "Select * from larearrelation where " +
                 "( agentcode='" + cAgentCode +
                 "' or rearagentcode='" + cAgentCode +
                 "') ";
        System.out.println("BreakAgentRear中查询所有现存关系的SQL： " + SQLstr);
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        tLARearRelationSet.add(tLARearRelationDB.executeQuery(SQLstr));
        //下面要循环备份，和修改
        if (tLARearRelationSet.size() > 0) {
            for (int i = 1; i <= tLARearRelationSet.size(); i++) {
                tLARearRelationDB = new LARearRelationDB();
                tLARearRelationDB.setSchema((LARearRelationSchema)
                                            tLARearRelationSet.get(i));
                if (!BackupRear(tLARearRelationDB.getSchema())) {
                    DealErr("备份血缘关系失败！", "AscriptRelationOperateBL",
                            "BreakAgentRear");
                    return false;
                }
                tLARearRelationDB.setEndDate(mOldGradeEndDate);
                tLARearRelationDB.setRearFlag("0");
    //                    tLARearRelationDB.setState("3");
                tLARearRelationDB.setModifyDate(currentDate);
                tLARearRelationDB.setModifyTime(currentTime);
                tLARearRelationDB.setOperator(mGlobalInput.Operator);
                mLARearRelationSetUpdate.add(tLARearRelationDB.getSchema());
            }
        }
        return true;
    }
    public void setmEdorNo(String cEdorNo)
    {
        mEdorNo=cEdorNo;
    }

    /*
     删除旧的关系
     */
    public boolean deleteRear(LATreeSchema cLATreeSchema)
    {
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        tLARearRelationDB.setAgentCode(cLATreeSchema.getAgentCode());
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        tLARearRelationSet = tLARearRelationDB.query();
        if (tLARearRelationDB.mErrors.needDealError())
        {
            DealErr(tLARearRelationDB.mErrors.getFirstError(),
                    "AscriptRelationOperateBL", "CheckRear");
            return false;
        }
        //下面要循环备份
        if(tLARearRelationSet.size()>0)
        {
            for (int i = 1; i <= tLARearRelationSet.size(); i++) {
                //tLARearRelationDB=new LARearRelationDB();
                LARearRelationSchema tLARearRelationSchema = new
                        LARearRelationSchema();
                tLARearRelationSchema.setSchema((LARearRelationSchema)
                                                tLARearRelationSet.get(i));
                if (!BackupRear(tLARearRelationSchema.getSchema())) {
                    DealErr("备份血缘关系失败！", "AscriptRelationOperateBL",
                            "BreakAgentRear");
                    return false;
                }
            }
        }
        mLARearRelationSetDelete.add(tLARearRelationSet);
        mMap.put(mLARearRelationSetDelete,"DELETE");
        return true;
    }
    /*
     修改为无效的关系
     */
    public boolean changeRear(LATreeSchema cLATreeSchema)
    {
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        tLARearRelationDB.setRearAgentCode(cLATreeSchema.getAgentCode());
        tLARearRelationDB.setRearFlag("1");
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        tLARearRelationSet = tLARearRelationDB.query();
        if (tLARearRelationDB.mErrors.needDealError()) {
            DealErr(tLARearRelationDB.mErrors.getFirstError(),
                    "AscriptRelationOperateBL", "CheckRear");
            return false;
        }
        //下面要循环备份
        if (tLARearRelationSet.size() > 0) {
            for (int i = 1; i <= tLARearRelationSet.size(); i++) {
                //tLARearRelationDB=new LARearRelationDB();
                LARearRelationSchema tLARearRelationSchema = new
                        LARearRelationSchema();
                tLARearRelationSchema.setSchema((LARearRelationSchema)
                                                tLARearRelationSet.get(i));
                if (!BackupRear(tLARearRelationSchema.getSchema())) {
                    DealErr("备份血缘关系失败！", "AscriptRelationOperateBL",
                            "BreakAgentRear");
                    return false;
                }
                tLARearRelationSchema.setRearCommFlag("0");
                tLARearRelationSchema.setRearFlag("0");
                mLARearRelationSetUpdate.add(tLARearRelationSchema);
            }
        }
        mMap.put(mLARearRelationSetUpdate,"UPDATE");
        return true;
    }
    /*
     断裂关系  ,由tAgentCode形成的间接培养关系(但是tAgentCode不是被培养人和培养人)
     */
    public boolean deleteAsRear(LATreeSchema cLATreeSchema)
    {
    	//根据IT要求，不断裂间接养成关系，modified by miaoxz at 2010-7-6
//        String tAgentCode = cLATreeSchema.getAgentCode();
//        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
//        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
//        //查询本人为被育成人的间接培养关系
//        String downsql = "select * from larearrelation  where rearagentcode in "
//                 + "(select rearagentcode from larearrelation b "
//                 + "where  b.agentcode='" + tAgentCode + "') "
//                 +
//                 "and agentcode in (select agentcode from larearrelation b "
//                 + "where  b.rearagentcode='" + tAgentCode + "') and rearedgens > 1  ";
//
//       tLARearRelationSet = tLARearRelationDB.executeQuery(downsql);
//        if (tLARearRelationDB.mErrors.needDealError()) {
//            DealErr(tLARearRelationDB.mErrors.getFirstError(),
//                    "AscriptRelationOperateBL", "CheckRear");
//            return false;
//        }
//        //下面要循环备份
//        if (tLARearRelationSet.size() > 0) {
//            for (int i = 1; i <= tLARearRelationSet.size(); i++) {
//                //tLARearRelationDB=new LARearRelationDB();
//                LARearRelationSchema tLARearRelationSchema = new
//                        LARearRelationSchema();
//                tLARearRelationSchema.setSchema((LARearRelationSchema)
//                                                tLARearRelationSet.get(i));
//                if (!BackupRear(tLARearRelationSchema.getSchema())) {
//                    DealErr("备份血缘关系失败！", "AscriptRelationOperateBL",
//                            "BreakAgentRear");
//                    return false;
//                }
//                mLARearRelationSetDelete.add(tLARearRelationSchema);
//            }
//        }
        
//         mMap.put(mLARearRelationSetDelete,"DELETE");
//    	modified by miaoxz at 2010-7-6 end here
        return true;
    }
    /*
     在建立关系的时候,检查要建立的被育成人的关系,是否曾经存在相同代数、相同级别的关系，存在就删除旧的关系，插入新的
     */
    private boolean checkRear(LARearRelationSchema cLARearRelationSchema)
    {
        LARearRelationDB tLARearRelationDB=new LARearRelationDB();
        tLARearRelationDB.setAgentCode(cLARearRelationSchema.getAgentCode());
        tLARearRelationDB.setRearedGens(cLARearRelationSchema.getRearedGens());
        tLARearRelationDB.setRearLevel(cLARearRelationSchema.getRearLevel());
        if(!tLARearRelationDB.getInfo())
        {
            if (tLARearRelationDB.mErrors.needDealError()) {
                DealErr(tLARearRelationDB.mErrors.getFirstError(),
                        "AscriptRelationOperateBL", "CheckRear");
                return false;
            }
        }
        mLARearRelationSetDelete.add(tLARearRelationDB.getSchema());
        return true;
    }
    public MMap getMap()
    {
        return mMap;
    }
}
