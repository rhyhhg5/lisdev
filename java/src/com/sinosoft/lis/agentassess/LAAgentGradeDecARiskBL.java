package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.agent.ALADimissionAppBL;
import com.sinosoft.lis.agent.ALADimissionAppUI;
import com.sinosoft.lis.agent.ALADimissionBL;
import com.sinosoft.lis.agentassess.LAAssessInputUI;

/**
 * 团险见习业务经理考核归属
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAAgentGradeDecARiskBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 提交数据的容器 */
    private MMap mMap = new MMap();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSet mLAAssessSet1 = new LAAssessSet(); //非清退人员，转正晋升
    private LAAssessSet mLAAssessSet2 = new LAAssessSet(); //清退人员
    private LAAssessSet mLAAssessSet3 = new LAAssessSet(); //考核维持，未转正维持
    private LAAssessHistorySchema mLAAssessHistorySchema = new
            LAAssessHistorySchema();

    private LAAssessSet mLAAssessSet = new LAAssessSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LAAssessMainSet mLAAssessMainSet = new LAAssessMainSet();
    private String mEvaluateDate = ""; // 异动时间
    private String mManageCom = ""; // 管理机构
    private String mIndexCalNo = ""; // 考核序列号
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mBranchAttr = ""; //前台 传入的销售机构编码
    private String mBranchName = ""; //前台传入的销售机构名称
    private String mAgentCode = ""; //前台传入的业务员代码
    private String mAgentName = ""; //前台传入的业务员姓名
    public static void main(String args[]) {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "001";
        LAAssessHistorySchema aLAAssessHistorySchema = new
                LAAssessHistorySchema();
        // 准备数据
        aLAAssessHistorySchema.setManageCom("8611");
        aLAAssessHistorySchema.setIndexCalNo("200612");
        aLAAssessHistorySchema.setBranchType("2");
        aLAAssessHistorySchema.setBranchType2("01");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(aLAAssessHistorySchema);
        System.out.println("add over");
        LAAgentGradeDecARiskBL m = new LAAgentGradeDecARiskBL();
        boolean tag = m.submitData(tVData, "");
        if (tag) {
            System.out.println("ok");
        } else {
            System.out.println("fail");
        }
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //取得异动时间
        if (!getEvaluateDate()) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //进行人员职级异动处理
        if (!dealAgentFormUp()) {
            return false;
        }
        //处理延期转正
        if (!dealAssess3()) {
            return false;
        }
        //对考核主表进行处理
        if (!dealLAAssessMain()) {
            return false;
        }
        
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        //统一提交
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        if (tPubSubmit.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecARiskBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 人员新职级归属完毕以后更改考核主表的状态
     * @return boolean
     */
    private boolean dealLAAssessMain() {
    	if (AgentPubFun.ConverttoYM(currentDate).compareTo(mIndexCalNo) <= 0) {
			// @@错误处理
			CError.buildErr(this, "不能进行当月及以后的考核确认操作!");
			return false;
		}
		LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
		LAAssessMainSet tLAAssessMainSet = new LAAssessMainSet();
		String strSQL = "select * from laassessmain where  assesstype='00' and indexcalno='"
				+ mIndexCalNo
				+ "' and managecom like '"
				+ mManageCom
				+ "%' and state='0' ";
		strSQL += " and agentgrade = 'A01' ";
		System.out.println(strSQL);
		tLAAssessMainSet = tLAAssessMainDB.executeQuery(strSQL);
		if (tLAAssessMainSet.size() <= 0) {
			// 再查一下看看是不是已经确认了
			strSQL = "select * from laassessmain where assesstype='00' and indexcalno='"
					+ mIndexCalNo
					+ "' and managecom like '"
					+ mManageCom
					+ "%' and state>'0' ";
			strSQL += " and agentgrade = 'A01' ";

			tLAAssessMainSet = tLAAssessMainDB.executeQuery(strSQL);
			if (tLAAssessMainSet.size() <= 0) {
				CError.buildErr(this, "查询机构" + mManageCom + "下的考评主信息失败!");
			} else {
				CError.buildErr(this, "机构" + mManageCom + "下的考评信息已确认过!");
				return false;
			}
		}
    	for (int i = 1; i <= tLAAssessMainSet.size(); i++) {
			LAAssessMainSchema tLAAssessMainSchema = new LAAssessMainSchema();
			tLAAssessMainSchema = tLAAssessMainSet.get(i);
			if (tLAAssessMainSchema.getAssessCount() >= 0) {
				tLAAssessMainSchema.setConfirmCount(tLAAssessMainSchema
						.getAssessCount());
			}
			tLAAssessMainSchema.setState("1"); // 审核确认状态标记
			tLAAssessMainSchema.setOperator(mGlobalInput.Operator);
			tLAAssessMainSchema.setModifyDate(PubFun.getCurrentDate());
			tLAAssessMainSchema.setModifyTime(PubFun.getCurrentTime());
			mLAAssessMainSet.add(tLAAssessMainSchema);
		}
        return true;
    }
    /**
     * 分类处理清退人员和职级发生变化的人员
     * @return boolean
     */
    private boolean dealAgentFormUp() {
        //处理离职人员信息变量
        VData tVData = new VData();
        if (mLAAssessSet2.size() > 0) {
        	ExeSQL tExeSQL = new ExeSQL();
            for (int i = 1; i <= mLAAssessSet2.size(); i++) {
            	VData tInputVData = new VData();
            	LADimissionSchema tLADimissionSchema = new LADimissionSchema();
            	LAAssessSchema tLAAssessSchema = new LAAssessSchema();
            	tLAAssessSchema = mLAAssessSet2.get(i);
                tLADimissionSchema = new LADimissionSchema();
                //处理清退人员
                	//在处理清退以前首先要判断是否已经进行过离职登记或者离职确认
                LADimissionSet tLADimissionSet=new LADimissionSet();
                String sql = "select agentstate from laagent where agentcode = '" +
                tLAAssessSchema.getAgentCode() + "' ";
				String  agentState = tExeSQL.getOneValue(sql);
			   if (!agentState.equals("") && agentState.compareTo("03") >= 0) { //非在职状态直接返回
				       return true;
			   	}
			   boolean mflag = SpecialDeal(tLAAssessSchema.getAgentCode());
		        if(mflag)
			        {
			        //生成离职信息schema
			        String tSql=" select max(departtimes)+1 from LADimission where 1=1 and agentcode='"+tLAAssessSchema.getAgentCode()+"' ";
			        tExeSQL = new ExeSQL();
			        String tDepartTimes=tExeSQL.getOneValue(tSql);
			        if(tDepartTimes.equals("")|| tDepartTimes.equals("0"))
			        {
			            tDepartTimes="1";
			        }
			        tLADimissionSchema.setAgentCode(tLAAssessSchema.getAgentCode());
			        tLADimissionSchema.setDepartTimes(tDepartTimes);
			        tLADimissionSchema.setApplyDate(mEvaluateDate);
			        tLADimissionSchema.setBranchType(tLAAssessSchema.getBranchType());
			        tLADimissionSchema.setBranchType2(tLAAssessSchema.getBranchType2());
			        tLADimissionSchema.setDepartDate("");
			        tLADimissionSchema.setDepartRsn("0");
			        tLADimissionSchema.setOperator(mGlobalInput.Operator);
	
			        tInputVData.add(tLADimissionSchema);
			        tInputVData.add(mGlobalInput);
			        //这里调用离职登记程序接口，独立事务处理
			        ALADimissionAppBL tALADimissionAppBL = new ALADimissionAppBL();
			        if (!tALADimissionAppBL.submitData(tInputVData, "INSERT||MAIN")) {
			            CError.buildErr(this,
			                            "清退时,代理人" + tLAAssessSchema.getAgentCode() + "离职登记失败！");
			            return false;
			        }
	
			        //离职确认处理
			        LADimissionDB tLADimissionDB = new LADimissionDB();
			        tLADimissionDB.setAgentCode(tLAAssessSchema.getAgentCode());
			        tLADimissionDB.setDepartTimes(1);
			        if (!tLADimissionDB.getInfo()) {
			            CError.buildErr(this,
			                            "清退时,获取代理人" + tLAAssessSchema.getAgentCode() + "离职登记信息失败！");
			        }
			        tLADimissionSchema = new LADimissionSchema();
			        tLADimissionSchema = tLADimissionDB.getSchema();
			        tLADimissionSchema.setDepartDate(mEvaluateDate);
			        tInputVData = new VData();
			        tInputVData.addElement(tLADimissionSchema);
			        tInputVData.add(mGlobalInput);
			        //这里调用离职确认程序接口，独立事务处理
			        ALADimissionBL tALADimissionBL = new ALADimissionBL();
			        if (!tALADimissionBL.submitData(tInputVData, "INSERT||MAIN")) {
			            CError.buildErr(this,
			                            "清退时,代理人" + tLAAssessSchema.getAgentCode() +
			                            "离职确认失败！");
			            return false;
			         }
		       }
            }
        }

        //处理非清退人员(职级异动),这里只有A01转正
        if (mLAAssessSet1.size() > 0) {
            LAAssessSchema tLAAssessSchema = new LAAssessSchema();

            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            LAAgentSet tLAAgentSet = new LAAgentSet();
            LATreeSet tLATreeSet = new LATreeSet();
            LATreeSchema tLATreeSchema = new LATreeSchema();
            LATreeDB tLATreeDB = new LATreeDB();
            Reflections tRefs = new Reflections();
            LATreeBSet tLATreeBSet = new LATreeBSet();
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            String tEdorNo = "";
            for (int j = 1; j <= mLAAssessSet1.size(); j++) {
                tLAAssessSchema = new LAAssessSchema();
                tLAAssessSchema.setSchema(mLAAssessSet1.get(j).getSchema());
                tLATreeDB = new LATreeDB();
                tLATreeSchema = new LATreeSchema();
                tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
                tLATreeDB.getInfo();
                tLATreeSchema = tLATreeDB.getSchema();
                //备份行政信息
                tLATreeBSchema = new LATreeBSchema();
                tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                tRefs = new Reflections();
                tRefs.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
                tLATreeBSchema.setOperator(mGlobalInput.Operator);
                tLATreeBSchema.setEdorNO(tEdorNo);
                tLATreeBSchema.setRemoveType("01"); //手工考核确认
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
               // tLATreeBSchema.setAgentGrade(tLAAssessSchema.getCalAgentGrade());
                tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSet.add(tLATreeBSchema);
                //更新行政信息
                tLATreeSchema.setAgentLastGrade(tLATreeSchema.getAgentGrade());
                tLATreeSchema.setAgentLastSeries(tLATreeSchema.getAgentSeries());
                tLATreeSchema.setAgentGrade(tLAAssessSchema.getAgentGrade1());
                tLATreeSchema.setAgentSeries(tLAAssessSchema.getAgentSeries1());
                tLATreeSchema.setOldStartDate(tLATreeSchema.getStartDate());
                tLATreeSchema.setOldEndDate(PubFun.calDate(mEvaluateDate, -1,
                        "D", null));
                tLATreeSchema.setAgentGrade(tLAAssessSchema.getAgentGrade1());
                if (!tLAAssessSchema.getCalAgentGrade().equals("A01")) {
                    tLATreeSchema.setInDueFormDate(mEvaluateDate);
                    tLATreeSchema.setInDueFormFlag("Y"); //转正标记
                }else
                {
                    tLATreeSchema.setInDueFormDate("");
                    tLATreeSchema.setInDueFormFlag("N"); //转正标记
                }
                if (tLAAssessSchema.getModifyFlag().equals("01")) {
                    tLATreeSchema.setState("2");
                }
                if (tLAAssessSchema.getModifyFlag().equals("03")) {
                  tLATreeSchema.setState("1");
                }
                if (tLAAssessSchema.getModifyFlag().equals("02")) {
                 tLATreeSchema.setState("0");
               }
                tLATreeSchema.setStartDate(mEvaluateDate);
                tLATreeSchema.setAstartDate(mEvaluateDate);
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(mGlobalInput.Operator);
                tLATreeSet.add(tLATreeSchema);

                tLAAgentSchema = new LAAgentSchema();
                tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tLAAssessSchema.getAgentCode());
                tLAAgentDB.getInfo();
                tLAAgentSchema = tLAAgentDB.getSchema();
                tLAAgentSchema.setModifyDate(this.currentDate);
                tLAAgentSchema.setModifyTime(this.currentTime);
                tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
                if (!tLAAssessSchema.getCalAgentGrade().equals("A01")) {
                    tLAAgentSchema.setInDueFormDate(mEvaluateDate);
                }
                else
                {
                    tLAAgentSchema.setInDueFormDate("");
                }

                tLAAgentSet.add(tLAAgentSchema);

            }
            /** @todo  */
            mLAAgentSet.add(tLAAgentSet);
            mLATreeSet.add(tLATreeSet);
            mLATreeBSet.add(tLATreeBSet);
        }
        return true;
    }
    
    /**
     * 处理延期转正的
     * @return boolean
     */
    private boolean dealAssess3() {
        if (mLAAssessSet3.size() >= 1) {
            LATreeSet tLATreeSet = new LATreeSet();
            LATreeBSet tLATreeBSet = new LATreeBSet();
            for (int i = 1; i <= mLAAssessSet3.size(); i++) {
                LAAssessSchema tLAAssessSchema = new LAAssessSchema();
                tLAAssessSchema = mLAAssessSet3.get(i).getSchema();
                LATreeSchema tLATreeSchema = new LATreeSchema();
                LATreeDB tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
                tLATreeDB.getInfo();
                tLATreeSchema = tLATreeDB.getSchema();
                LATreeBSchema tLATreeBSchema = new LATreeBSchema();
              //备份行政信息
                tLATreeBSchema = new LATreeBSchema();
                String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                Reflections tRefs = new Reflections();
                tRefs.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
                tLATreeBSchema.setOperator(mGlobalInput.Operator);
                tLATreeBSchema.setEdorNO(tEdorNo);
                tLATreeBSchema.setRemoveType("01"); //手工考核确认
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
               // tLATreeBSchema.setAgentGrade(tLAAssessSchema.getCalAgentGrade());
                tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSet.add(tLATreeBSchema);
                
                tLATreeSchema.setState("4");
                tLATreeSchema.setAgentLastGrade(tLATreeSchema.getAgentGrade());
                tLATreeSchema.setAgentLastSeries(tLATreeSchema.getAgentSeries());
                tLATreeSchema.setAstartDate(mEvaluateDate);
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(mGlobalInput.Operator);
                tLATreeSet.add(tLATreeSchema);
            }
            if (tLATreeSet.size() >= 1) {
                mLATreeSet.add(tLATreeSet);
            }
            if (tLATreeBSet.size() >= 1) {
                mLATreeBSet.add(tLATreeBSet);
            }
        }
        return true;
    }

    /**
     * 取得异动时间(考核期次月一号)
     * @return boolean
     */
    private boolean getEvaluateDate() {
        String tSQL = "";

        tSQL = "select date(substr('" + mIndexCalNo + "',1,4)||'-'|| substr(";
        tSQL += "'" + mIndexCalNo + "',5,2)||'-01')+1 months from dual";

        ExeSQL tExeSQL = new ExeSQL();
        // 取得异动时间
        mEvaluateDate = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecARiskBL";
            tError.functionName = "getEvaluateDate";
            tError.errorMessage = "获取异动日期失败！";
            System.out.println("获取异动日期失败！");
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("获得异动日期：" + mEvaluateDate);
        return true;
    }


    /**
     *
     * @return boolean
     */
    private boolean checkPer(LAAssessSchema aLAAssessSchema) {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tLAAgentDB.setAgentCode(aLAAssessSchema.getAgentCode());
        tLAAgentDB.getInfo();
        tLAAgentSchema = tLAAgentDB.getSchema();
        if (AgentPubFun.ConverttoYM(tLAAgentSchema.getEmployDate()).compareTo(
                mIndexCalNo) > 0) {
            CError.buildErr(this,
                            "业务员" + aLAAssessSchema.getAgentCode() +
                            "入司年月不能大于考核归属年月!");
            return false;
        }
        String
                tSQL = "select count(*) from laassess where indexcalno>='"
                       + mIndexCalNo + "' and agentcode='"
                       + aLAAssessSchema.getAgentCode() + "' and state='2' ";
        if (execQuery(tSQL) > 0) {
            CError.buildErr(this,
                            "业务员" + aLAAssessSchema.getAgentCode() + "在" +
                            mIndexCalNo +
                            "月及以后已经考核归属过!");
            return false;
        }

        //检验当期考核是否已经进行过考核确认
        tSQL = "select count(*) from laassess where indexcalno='"
               + mIndexCalNo + "' and agentcode='"
               + aLAAssessSchema.getAgentCode() + "' and state='1' ";
        if (execQuery(tSQL) < 1) {
            CError.buildErr(this,
                            "业务员" +tLAAgentSchema.getName()+"(工号："+ tLAAgentSchema.getGroupAgentCode() + "）未进行考核确认!");
            return false;
        }

        System.out.println("验证通过！");
        return true;
    }

    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        mInputData.clear();
        mMap.put(mLAAssessSet, "UPDATE");
        mMap.put(mLAAgentSet, "UPDATE");
        mMap.put(mLATreeSet, "UPDATE");
        mMap.put(mLAAssessMainSet, "UPDATE");
        mMap.put(mLATreeBSet, "INSERT");
        mInputData.add(mMap);
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData() {
        LAAssessSet tLAAssessSet = new LAAssessSet();
        LAAssessDB tLAAssessDB = new LAAssessDB();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        int tAgentCount = 0;
        String tSQL = "";

        // 查询处该处参加考核的所有人员
        //tLAAssessDB.setManageCom(this.mManageCom);
        //tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
        //查询满足界面条件的人员进行有条件归属
        tSQL = "SELECT * FROM LAAssess WHERE IndexCalNo='" + this.mIndexCalNo +
               "'";
        tSQL += " and state not in ('2') ";
        tSQL += " AND ManageCom LIKE '" + this.mManageCom + "%' AND AgentGrade='A01'";
        tSQL += " AND BranchType = '" +
                this.mLAAssessHistorySchema.getBranchType() + "'";
        tSQL += " AND BranchType2 = '" +
                this.mLAAssessHistorySchema.getBranchType2() + "'";
        if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
        {
        	tSQL += " and agentcode = '"+this.mAgentCode+"'";
        }
        if(this.mAgentName!=null&&!this.mAgentName.equals(""))
        {
        	tSQL += " and agentcode = (select agentcode from laagent where name='"+this.mAgentName+"' " +
        			"  and branchtype='"+this.mLAAssessHistorySchema.getBranchType()+"' " +
        					"and branchtype2='"+this.mLAAssessHistorySchema.getBranchType2()+"')";
        }
        if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
        {
        	tSQL += " and branchattr = '"+this.mBranchAttr+"'";
        }
        if(this.mBranchName!=null && !this.mBranchName.equals(""))
        {
        	tSQL += " and branchattr = (select branchattr from labranchgroup where name='"+this.mBranchName+"' " +
        			"and branchtype='"+this.mLAAssessHistorySchema.getBranchType()+"'" +
        					" and branchtype2='"+this.mLAAssessHistorySchema.getBranchType2()+"')";
        }
        System.out.println("查询LAAssess SQL:" + tSQL);
        System.out.println("查询LAAssess SQL:" + tSQL);
        tLAAssessSet = tLAAssessDB.executeQuery(tSQL);
        System.out.println("参加考核人员有[ " + tLAAssessSet.size() + " ]位");
        if (tLAAssessDB.mErrors.needDealError()) {
            // @@错误处理
            CError.buildErr(this, "查询参加考核的人员信息失败！");
            return false;
        }
        if (tLAAssessSet.size() < 1) {
            // @@错误处理
            CError.buildErr(this, "没有找到参加考核的人员信息！");
            return false;
        }
        //取得参加考核人员人数
        tAgentCount = tLAAssessSet.size();

        //循环处理每个考核人员新职级归属
        for (int i = 1; i <= tAgentCount; i++) {
            tLAAssessSchema = tLAAssessSet.get(i).getSchema();
            if (!checkPer(tLAAssessSchema)) {
                return false;
            }
            if (!dealAgentEvaluate(tLAAssessSchema)) {
                // @@错误处理
                CError.buildErr(this, "进行人员归属操作时失败！");
                return false;
            }
            tLAAssessSchema.setState("2"); //考核归属完毕
            tLAAssessSchema.setModifyDate(this.currentDate);
            tLAAssessSchema.setModifyTime(this.currentTime);
            tLAAssessSchema.setOperator(this.mGlobalInput.Operator);
            mLAAssessSet.add(tLAAssessSchema);
        }
        return true;
    }

    /**
     * 对每个参加考核的人员进行逐个处理
     * @param pmLAAssessSchema LAAssessSchema
     * @return boolean
     */
    private boolean dealAgentEvaluate(LAAssessSchema pmLAAssessSchema) {
        String tModifyFlag = pmLAAssessSchema.getModifyFlag(); //升降级标记
        String tAgentGrade1 = pmLAAssessSchema.getAgentGrade1(); //新职级
        //判断业务员升降级分别进行处理 01:降级 02：维持 03：晋级
        //if ("01".equals(tModifyFlag) || "03".equals(tModifyFlag)) {
            // 处理清退职级业务员
            if ("A00".equals(tAgentGrade1)) {
                //对清退业务员进行离职处理
                mLAAssessSet2.add(pmLAAssessSchema);//清退
            } else if(tModifyFlag.equals("02")){
            	mLAAssessSet3.add(pmLAAssessSchema); //维持，非非转正
            }else if(tModifyFlag.equals("03")){
            	mLAAssessSet1.add(pmLAAssessSchema);//转正
            }
        //}
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        System.out.println("Begin LAAgentGradeDecARiskBL.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        this.mLAAssessHistorySchema.setSchema((LAAssessHistorySchema)
                                              cInputData
                                              .getObjectByObjectName(
                "LAAssessHistorySchema", 0));
        this.mManageCom = mLAAssessHistorySchema.getManageCom();
        this.mIndexCalNo = mLAAssessHistorySchema.getIndexCalNo();
        this.mBranchAttr = (String)cInputData.get(3);
        this.mBranchName = (String)cInputData.get(4);
        this.mAgentCode = (String)cInputData.get(5);
        this.mAgentName = (String)cInputData.get(6);
        System.out.println("this.mAgentCode"+this.mAgentCode);
        //只对见习经理进行操作
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecARiskBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
//        if (AgentPubFun.ConverttoYM(currentDate).compareTo(mIndexCalNo) <= 0) {
//            // @@错误处理
//            CError.buildErr(this, "考核年月不能大于或等于系统操作日期所在年月!");
//            return false;
//        }

        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql) {
        System.out.println(sql);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null) {
                return 0;
            }
            st = conn.prepareStatement(sql);
            if (st == null) {
                return 0;
            }
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
             if (!conn.isClosed()) {
                 conn.close();
             }
             try {
                 st.close();
                 rs.close();
             } catch (Exception ex2) {
                 ex2.printStackTrace();
             }
             st = null;
             rs = null;
             conn = null;
           } catch (Exception e) {}

        }
    }
    /**
     * 清退人员的特殊处理，
     * 判断是否有未回执回销或者正在处理过程中的保单
     * @return
     */
 private boolean SpecialDeal(String cAgentCode)
 {
	 String sql = "";
	 boolean tFlag = true;
	System.out.println(" tFlag;"+tFlag );
//	调用离职确认程序之前做限制判断:清退人员如有保单未回执或在处理过程中，不做离职确认，只需做一个标记
 	sql=
 			"select contno  from  lccont where  agentcode='"+cAgentCode+"' " +
 			"and   (appflag<>'1' or appflag is null ) and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null)" +
 			" and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" + 
 			" and conttype = '1' " + 
 			" union all " + 
 			" select lgc.grpcontno from lcgrpcont lgc where lgc.agentcode = '"+cAgentCode+"' " + 
 			" and (lgc.appflag<>'1' or lgc.appflag is null ) and ((lgc.uwflag<>'a' and lgc.uwflag<>'8' and lgc.uwflag<>'1') or lgc.uwflag is null)" +
 			" and not exists (select 'X' from lcrnewstatelog where newgrpcontno = lgc.grpcontno)"
 			
 			;
     ExeSQL mExeSQL = new ExeSQL();
     SSRS mSSRS = new SSRS();
     mSSRS = mExeSQL.execSQL(sql);
     if(mSSRS.getMaxRow()>0)
     {
     	//还有未签单保单，在处理过程中
     	tFlag= false;
     }
     else
     {//判断是否有未回执回销保单
 		 sql="select contno  from  lccont where  agentcode='"+cAgentCode+"'" +
	 		"   and  customgetpoldate is null  and  uwflag<>'a' and uwflag<>'8' and uwflag<>'1'" + 
	 		" and conttype = '1' " + 
	 		" union all " +
	 		" select lgc.grpcontno from lcgrpcont lgc where lgc.agentcode = '"+cAgentCode+"'" +
	 		" and  lgc.customgetpoldate is null  and  lgc.uwflag<>'a' and lgc.uwflag<>'8' and uwflag<>'1'";
     	 ExeSQL wExeSQL = new ExeSQL();
     	 SSRS wSSRS = new SSRS();
     	 wSSRS = wExeSQL.execSQL(sql);
          if(wSSRS.getMaxRow()>0)
          {
          	//还有未回执回销保单保单
          	tFlag= false;
          }
     }
     return tFlag;
 }
}
