package com.sinosoft.lis.agentassess;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: </p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.pubfun.AgentPubFun;

public class LAGrpRearBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap = new MMap();
    private LARearRelationSchema mLARearRelationSchema = new LARearRelationSchema();
    private LARearRelationSchema InsetLARearRelationSchema = new LARearRelationSchema();

    public LAGrpRearBL() {
    }

    public static void main(String[] args)
    {

    }

    public boolean submitData(VData cInputData, String cOperate)
  {
      //将操作数据拷贝到本类中
      this.mOperate = cOperate;
      //得到外部传入的数据,将数据备份到本类中
      if (!getInputData(cInputData))
      {
          return false;
      }
      //进行业务处理
      if (!dealData())
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LAGrpRearBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LAGrpRearBL-->dealData!";
          this.mErrors.addOneError(tError);
          return false;
      }
      //准备往后台的数据
      if (!prepareOutputData())
      {
          return false;
      }

      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start LAGrpRearBL Submit...");
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAGrpRearBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
      }

      mInputData = null;
      return true;
  }
  private boolean getInputData(VData cInputData)
   {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
       this.mLARearRelationSchema.setSchema((LARearRelationSchema) cInputData.
                                    getObjectByObjectName(
                                            "LARearRelationSchema", 0));
       return true;
   }
   private boolean dealData()
   {
       String currentDate=PubFun.getCurrentDate();
       String currentTime=PubFun.getCurrentTime();
       if (mOperate.equals("INSERT||MAIN"))
       {
           String sql="select * from LARearRelation  where agentcode='"
                      +mLARearRelationSchema.getAgentCode()+"'  and rearflag='1'"
                      +" union select * from  LARearRelation  where rearagentcode='"
                      +mLARearRelationSchema.getAgentCode()+"'  and rearflag='1'";
            LARearRelationDB tLARearRelationDB=new LARearRelationDB();
            LARearRelationSet tLARearRelationSet=new LARearRelationSet();
            tLARearRelationSet=tLARearRelationDB.executeQuery(sql);
            if(tLARearRelationSet.size()>0)
            {
                CError tError = new CError();
                tError.moduleName = "LAGrpRearBL";
                tError.functionName = "dealData";
                tError.errorMessage = "被育成人"+mLARearRelationSchema.getAgentCode()+"已经存在有效的育成关系，不能再进行录入。";
                this.mErrors.addOneError(tError);
                return false;
            }
           String tq="select value(max(rearedgens),0)+1 from larearrelation  where agentcode='"+mLARearRelationSchema.getAgentCode()+"'";
           ExeSQL tExeSQL = new ExeSQL();
           SSRS rearedgens = tExeSQL.execSQL(tq);
           int gens=Integer.parseInt(rearedgens.GetText(1,1));

           String yearmonth1 = PubFun.calDate(mLARearRelationSchema.getstartDate(), 11, "M", null) ;
           String yearmonth= AgentPubFun.formatDate(yearmonth1, "yyyyMM");
           String sq ="select enddate from lastatsegment where stattype='5' and yearmonth=" + yearmonth + "";
           ExeSQL aExeSQL = new ExeSQL();
           SSRS aSSRS = new SSRS();
           aSSRS = aExeSQL.execSQL(sq);
           String enddate = aSSRS.GetText(1, 1);

           mLARearRelationSchema.setEndDate(enddate);
           mLARearRelationSchema.setRearedGens(gens);

           mLARearRelationSchema.setMakeDate(currentDate);
           mLARearRelationSchema.setMakeTime(currentTime);
           mLARearRelationSchema.setModifyDate(currentDate);
           mLARearRelationSchema.setModifyTime(currentTime);
           mLARearRelationSchema.setOperator(mGlobalInput.Operator);
           mMap.put(this.mLARearRelationSchema, "INSERT");
       }
       if (mOperate.equals("UPDATE||MAIN"))
     {
         String sql="select * from LARearRelation  where agentcode='"+
                    mLARearRelationSchema.getAgentCode()+"'  and rearlevel='"+
                    mLARearRelationSchema.getRearLevel()+"'  and rearedgens= "+mLARearRelationSchema.getRearedGens()+"";
          LARearRelationDB tLARearRelationDB=new LARearRelationDB();
          LARearRelationSet tLARearRelationSet=new LARearRelationSet();
          tLARearRelationSet=tLARearRelationDB.executeQuery(sql);
          if(tLARearRelationSet.size()<=0||tLARearRelationSet==null)
          {
              CError tError = new CError();
              tError.moduleName = "LAGrpRearBL";
              tError.functionName = "dealData";
              tError.errorMessage = "没有查询需要修改的数据信息";
              this.mErrors.addOneError(tError);
             return false;
          }
          LARearRelationSchema tLARearRelationSchema=new LARearRelationSchema();
          tLARearRelationSchema=tLARearRelationSet.get(1);
          InsetLARearRelationSchema=tLARearRelationSchema;
          Reflections tReflections = new Reflections();
          LARearRelationBSchema tLARearRelationBSchema = new LARearRelationBSchema();
          tReflections.transFields(tLARearRelationBSchema, tLARearRelationSchema);
          tLARearRelationBSchema.setEdorType("11");//修改备份
          String tEdorNo=com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("EdorNo", 20);
          tLARearRelationBSchema.setEdorNo(tEdorNo);
          tLARearRelationBSchema.setEndDate(currentDate);
          tLARearRelationBSchema.setMakeDate2(currentDate);
          tLARearRelationBSchema.setMakeTime2(currentTime);
          tLARearRelationBSchema.setModifyDate2(currentDate);
          tLARearRelationBSchema.setModifyTime2(currentTime);
          tLARearRelationBSchema.setOperator2(mGlobalInput.Operator);

          mLARearRelationSchema.setMakeDate(tLARearRelationSchema.getMakeDate());
          mLARearRelationSchema.setMakeTime(tLARearRelationSchema.getMakeTime());
          mLARearRelationSchema.setModifyDate(currentDate);
          mLARearRelationSchema.setModifyTime(currentTime);
          mLARearRelationSchema.setOperator(mGlobalInput.Operator);
          mMap.put(this.InsetLARearRelationSchema, "DELETE");
          mMap.put(this.mLARearRelationSchema, "INSERT");
          mMap.put(tLARearRelationBSchema, "INSERT");
     }


       return true;
   }
   private boolean submitquery()
   {
       return true;
   }
   private boolean prepareOutputData()
    {
        try
        {
           this.mInputData = new VData();
           this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGrpRearBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
       {
           this.mResult .clear() ;
           return mResult;
    }

}
