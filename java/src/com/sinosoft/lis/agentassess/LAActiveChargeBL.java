package com.sinosoft.lis.agentassess;


import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentwages.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: LAActiveAssessStandBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author xin
 * @version 1.0
 */
public class LAActiveChargeBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";

  private MMap map = new MMap();
  public  GlobalInput mGlobalInput = new GlobalInput();

  private LAActiveChargeSet mLAActiveChargeSet  = new LAActiveChargeSet();

  public LAActiveChargeBL() {

  }

  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAActiveChargeBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LAActiveChargeBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAActiveChargeBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LAActiveChargeBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     this.mLAActiveChargeSet.set((LAActiveChargeSet) cInputData.get(1));
     System.out.println("LAActiveChargeSet get"+mLAActiveChargeSet.size());
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveChargeBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAActiveChargeBL.dealData........."+mOperate);
    try {
          LAActiveChargeSet tLAActiveChargeSetNew = new LAActiveChargeSet();
    	  for (int i = 1; i <= mLAActiveChargeSet.size(); i++) {
    		  LAActiveChargeSchema tLAActiveChargeSchemanew = new  LAActiveChargeSchema();
    		  LAActiveChargeSchema tLAActiveChargeSchemaold = new  LAActiveChargeSchema();
    		  LAActiveChargeDB tLAActiveChargeDB = new LAActiveChargeDB();
    		  tLAActiveChargeSchemanew = mLAActiveChargeSet.get(i);
    		  String managecom = tLAActiveChargeSchemanew.getManageCom();
    		  String agentCode = tLAActiveChargeSchemanew.getAgentCode();
    		  String branchtype = tLAActiveChargeSchemanew.getBranchType();
    		  String branchtype2 = tLAActiveChargeSchemanew.getBranchType2();
    		  String wageno = tLAActiveChargeSchemanew.getWageNo();
    		  tLAActiveChargeDB.setManageCom(managecom);
    		  tLAActiveChargeDB.setAgentCode(agentCode);
    		  tLAActiveChargeDB.setBranchType(branchtype);
    		  tLAActiveChargeDB.setBranchType2(branchtype2);
    		  tLAActiveChargeDB.setWageNo(wageno);
    		  if(tLAActiveChargeDB.getInfo())
    		  {
    			  tLAActiveChargeSchemaold = tLAActiveChargeDB.getSchema();
    			  if(mOperate.equals("INSERT"))
    			  {
    				  CError tError = new CError();
    				  tError.moduleName = "LAActiveChargeBL";
    				  tError.functionName = "dealData";
    				  tError.errorMessage = "管理机构"+managecom+"下的此业务员已经录入过薪资月:"+wageno+"的代理手续费信息！";
    				  this.mErrors.addOneError(tError);
    				  break;
    			  }
    			  if(mOperate.equals("UPDATE"))
    			  {
    				  tLAActiveChargeSchemaold.setPropertyPrem(tLAActiveChargeSchemanew.getPropertyPrem());
    				  tLAActiveChargeSchemaold.setPropertyCharge(tLAActiveChargeSchemanew.getPropertyCharge());
    				  tLAActiveChargeSchemaold.setLifePrem(tLAActiveChargeSchemanew.getLifePrem());
    				  tLAActiveChargeSchemaold.setLifeCharge(tLAActiveChargeSchemanew.getLifeCharge());
    				  tLAActiveChargeSchemaold.setSumCharge(tLAActiveChargeSchemanew.getSumCharge());
    				  tLAActiveChargeSchemaold.setModifyDate(PubFun.getCurrentDate());
    				  tLAActiveChargeSchemaold.setModifyTime(PubFun.getCurrentTime());
    				  tLAActiveChargeSetNew.add(tLAActiveChargeSchemaold);
    			  }
    		  }
    		  else
    		  {
    			  tLAActiveChargeSchemanew.setOperator(this.mGlobalInput.Operator);
    			  tLAActiveChargeSchemanew.setMakeDate(PubFun.getCurrentDate());
    			  tLAActiveChargeSchemanew.setMakeTime(PubFun.getCurrentTime());
    			  tLAActiveChargeSchemanew.setModifyDate(PubFun.getCurrentDate());
    			  tLAActiveChargeSchemanew.setModifyTime(PubFun.getCurrentTime());
    			  tLAActiveChargeSetNew.add(tLAActiveChargeSchemanew);
    		  }
              
          }
            map.put(tLAActiveChargeSetNew, mOperate);
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveChargeBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LAActiveChargeBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveChargeBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
