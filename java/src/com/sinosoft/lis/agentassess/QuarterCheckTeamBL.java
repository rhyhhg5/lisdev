package com.sinosoft.lis.agentassess;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;

public class QuarterCheckTeamBL {

    public CErrors mErrors = new CErrors();

    private String mOperate = "";

    private VData mInputData = null;

    private VData mResult = new VData();

    private TextTag mTextTag = null;

    private XmlExport mXml = null;

    private ListTable mListTable = new ListTable();

    private TransferData mTransferData = new TransferData();

    private String mManageCom = "";

    private String mQuerter = "";

    private String mCheckEndDate = "";

    private String mCheckStartDate = "";

    private String mBranchType = "";

    private String mBranchType2 = "";

    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();

    private GlobalInput mGlobalInput = null;

    private PubFun mPubFun = new PubFun();

    public QuarterCheckTeamBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        if (!mOperate.equals("PRINT")) {
            this.buildError("submitData", "数据不完整！");
            return false;
        }

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        //人员考核计算主函数
        if (!Examine()) {
            return false;
        }

        return true;
    }

    /**
     *从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            this.mInputData = (VData) cInputData;

            if (mInputData == null) {
                this.buildError("getInputData", "数据不完整！");
                return false;
            }

            this.mTransferData = (TransferData)this.mInputData.
                                 getObjectByObjectName("TransferData", 0);

            if (mTransferData == null) {
                this.buildError("getInputData", "数据不完整！");
                return false;
            }

            this.mBranchType = (String) mTransferData.getValueByName(
                    "BranchType");

            this.mBranchType2 = (String) mTransferData.getValueByName(
                    "BranchType2");

            this.mQuerter = (String) mTransferData.getValueByName(
                    "AssessYear");

//            this.mCheckEndDate = (String) mTransferData.getValueByName(
//            "AssessYear");

            this.mManageCom = (String) mTransferData.getValueByName("ManageCom");

            this.mGlobalInput = (GlobalInput)this.mInputData.
                                getObjectByObjectName("GlobalInput", 0);

            if (this.mGlobalInput == null) {
                this.buildError("getInputData", "数据不完整！");
                return false;
            }

        } catch (Exception ex) {
            this.buildError("getInputData", "数据不完整！");
            return false;
        }

        return true;
    }

    /**
     * 考核主函数
     * @return boolean
     */
    private boolean Examine() {

        //查询管理机构下的所有单位代码
        if (!getBranchGroupSetByManageCom(mManageCom)) {
            return false;
        }

        //生成考核时间区间
//        if (!getExamineDate(mCheckEndDate)) {
//            return false;
//        }

        if (!getExamineDate(mQuerter)) {
            return false;
        }

        //生成考核数据
        if (!dealAssess()) {
            return false;
        }
        return true;
    }

    /**
     * 查询管理机构下的所有单位代码
     * @param cManageCom String
     * @return boolean
     */
    private boolean getBranchGroupSetByManageCom(String cManageCom) {

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();

        StringBuffer tSQL = new StringBuffer();

        tSQL.append("SELECT *  FROM LABranchGroup a  WHERE ManageCom like '" +
                    cManageCom + "%' AND");
        tSQL.append(" ManageCom like '" + this.mGlobalInput.ManageCom +
                    "%' and (a.state<>'1' or a.state is null) and  endflag='N' AND ");
        tSQL.append(" BranchType = '" + mBranchType + "' AND");
        tSQL.append(" BranchType2 = '" + mBranchType2 + "' " );
        tSQL.append(" ORDER BY branchattr");

        System.out.println(tSQL.toString());
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(tSQL.toString());

        if (tLABranchGroupDB.mErrors.needDealError()) {
            this.buildError("getBranchGroupSetByManageCom", "数据查询错误！");

            return false;
        }

        if (tLABranchGroupSet.size() == 0) {
            this.buildError("getBranchGroupSetByManageCom", "该分公司没有团队！");

            return false;

        }

        this.mLABranchGroupSet.set(tLABranchGroupSet);

        return true;
    }

    /**
     * 生成考核时间区间
     * @param cCheckDate String
     * @return boolean
     */
    private boolean getExamineDate(String cCheckDate) {
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();

        StringBuffer tSQL = new StringBuffer();

//        tSQL.append("select date_format(date(substr('" + cCheckDate +
//                    "',1,4)||'-' || substr('" + cCheckDate +
//                    "',5,2)||'-01')-2 month,'yyyymm')");
//        tSQL.append(" from dual");
        int tYearMonth = 0;

        String temp = this.mPubFun.getCurrentDate();

        if (cCheckDate.equals("1")) {
            tYearMonth = Integer.parseInt(temp.substring(0, 4) + "03");
        } else if (cCheckDate.equals("2")) {
            tYearMonth = Integer.parseInt(temp.substring(0, 4) + "06");
        } else if (cCheckDate.equals("3")) {
            tYearMonth = Integer.parseInt(temp.substring(0, 4) + "09");
        }

        tSQL.append(
                "select date_format(startdate,'yyyymm'),date_format(enddate,'yyyymm')");
        tSQL.append(" from LAStatSegment where stattype='2' and yearmonth=" +
                    tYearMonth + "");
        System.out.println(tSQL.toString());

        tSSRS = tExeSQL.execSQL(tSQL.toString());

        if (tExeSQL.mErrors.needDealError()) {
            this.buildError("getExamineDate", "查询不到考核区间！");

            System.out.println(this.mErrors.getFirstError().toString());
            return false;
        }

        this.mCheckStartDate = tSSRS.GetText(1, 1);

        this.mCheckEndDate = tSSRS.GetText(1, 2);

        return true;
    }

    /**
     * 生成考核数据
     * @return boolean
     */
    private boolean dealAssess() {
        //机构数目
        int countManage = this.mLABranchGroupSet.size();

        for (int i = 1; i <= countManage; i++) {
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema = mLABranchGroupSet.get(i);

            String strAngetGroup = tLABranchGroupSchema.getAgentGroup();

            ExeSQL tExeSQL = new ExeSQL();

            /***************************************************************************/
            StringBuffer strInfo = new StringBuffer();

            strInfo.append("SELECT c.NAME,c.AGENTCODE,b.name,b.branchattr");
            strInfo.append(
                    " FROM LABranchGroup b,laagent c WHERE b.agentgroup = c.agentgroup AND");
            strInfo.append(" b.agentgroup = '" + strAngetGroup +
                           "' AND c.branchtype='2' AND c.branchtype2='01' AND c.AgentState IN('01','02')");

            SSRS tSSRScount = new SSRS();

            tSSRScount = tExeSQL.execSQL(strInfo.toString());

            if (tExeSQL.mErrors.needDealError()) {
                this.buildError("dealAssess", "该机构没有在职的销售员！");
                return false;
            }
            System.out.println(strInfo.toString());
            int intCount = tSSRScount.getMaxRow();

            /***************************************************************************/


            StringBuffer tSQL = new StringBuffer();

            SSRS tSSRS = new SSRS();

            tSQL.append(
                    "SELECT c.NAME,a.AGENTCODE,b.name,b.branchattr,sum(a.transmoney) aa,");
            tSQL.append(
                    "sum(a.standprem) bb  FROM lacommision a,LABranchGroup b,");
            tSQL.append("laagent c WHERE a.branchtype='" + mBranchType +
                        "' AND");
            tSQL.append(" a.branchType2='" + mBranchType2 +
                        "' AND a.agentgroup='" + strAngetGroup + "'");
            tSQL.append(
                    " AND a.agentcode = c.agentcode AND a.agentgroup = b.agentgroup AND");
            tSQL.append(" a.wageno<='" + this.mCheckEndDate +
                        "' AND  a.wageno>='" + this.mCheckStartDate + "'");
             tSQL.append(" and c.AgentState IN('01','02') ");
            tSQL.append(
                    " GROUP BY b.branchattr,b.name,a.agentcode,c.NAME order by aa,bb");

            System.out.println(tSQL.toString());

            tSSRS = tExeSQL.execSQL(tSQL.toString());

            if (tExeSQL.mErrors.needDealError()) {
                this.buildError("dealAssess", "查询不到数据！");
                return false;
            }

            int count = tSSRS.getMaxRow();

            if (count > 0) {
                String tFlag="1" ;
                    //保单为零的人员信息添加,如果有,则这些人排名为最后
                    for (int k = 1; k <= tSSRScount.getMaxRow(); k++) {
                        String strCode = tSSRScount.GetText(k, 2);
                        int intequals = 0;
                        for (int n = 1; n <= tSSRS.getMaxRow(); n++) {

                            if (strCode.equals(tSSRS.GetText(n, 2))) {
                                intequals++;
                                System.out.println(tSSRS.GetText(n, 2));
                            }
                        }
                        if (intequals == 0) {

                            String[] info = new String[tSSRS.getMaxCol()];

                            info[0] = tSSRScount.GetText(k, 1);
                            info[1] = tSSRScount.GetText(k, 2);
                            info[2] = tSSRScount.GetText(k, 3);
                            info[3] = tSSRScount.GetText(k, 4);
                            info[4] = "0";
                            info[5] = "0";
                            this.mListTable.add(info);
                            tFlag="0";
                        }
                }
                if(tFlag.equals("1"))
                {//没有保单为0的人
                   for (int j = 1; j <= count; j++) {
                     //保费总额小于等于50000，且在所属营业部中保费排名最后
                     double tMinValue=Double.parseDouble(tSSRS.GetText(1, 5));//最小保费
                     if (Double.parseDouble(tSSRS.GetText(j, 6)) <= 50000 &&
                        Double.parseDouble(tSSRS.GetText(j, 5)) == tMinValue) {
                        String[] info = new String[tSSRS.getMaxCol()];

                        for (int k = 1; k <= tSSRS.getMaxCol(); k++) {
                            info[k - 1] = tSSRS.GetText(j, k);
                         }

                         this.mListTable.add(info);
                      }
                   }

                }
            }
                else {
                for (int k = 1; k <= tSSRScount.getMaxRow(); k++) {

                    String[] info = new String[tSSRS.getMaxCol()];

                    info[0] = tSSRScount.GetText(k, 1);
                    info[1] = tSSRScount.GetText(k, 2);
                    info[2] = tSSRScount.GetText(k, 3);
                    info[3] = tSSRScount.GetText(k, 4);
                    info[4] = "0";
                    info[5] = "0";
                    this.mListTable.add(info);
                }

            }

//        }

//        }

    }

    this.mListTable.setName("ZT");

    this.mXml = new XmlExport();

    this.mTextTag = new TextTag();

    mXml.createDocument("QuarterCheckTeamReport.vts", "printer");

    String ManageComSQL = "select name from ldcom where comcode='" +
                          this.mManageCom + "'";

    ExeSQL tExeSQL = new ExeSQL();
    SSRS tSSRS = new SSRS();

    tSSRS = tExeSQL.execSQL(ManageComSQL);

    if (tExeSQL.mErrors.needDealError()) {
        this.buildError("dealAssess", "查询XML数据错误！");
        return false;
    }

    String strManageCom = tSSRS.GetText(1, 1); //考核机构名称
    String strDate = this.mPubFun.getCurrentDate(); //考核日期
    String strStartDate = this.mCheckStartDate.substring(0, 4) + "年" +
                          this.mCheckStartDate.substring(4, 6) + "月"; //考核开始日期

    String strEndDate = this.mCheckEndDate.substring(0, 4) + "年" +
                        this.mCheckEndDate.substring(4, 6) + "月"; //考核结束日期

//添加参数
    this.mTextTag.add("ManageCom", strManageCom);

    this.mTextTag.add("Date", strDate);

    this.mTextTag.add("StartDate", strStartDate);

    this.mTextTag.add("EndDate", strEndDate);

//ADD TEXTTAG
    if (this.mTextTag.size() < 0) {
        this.buildError("dealAssess", "查询XML数据错误！");
        return false;
    } else {
        mXml.addTextTag(this.mTextTag);
    }

//添加表头
    String[] title = {"销售人员姓名", "销售人员代码", "所属团队1", "团队代码", "累计完成规模保费", "累计完成标准保费"};
    mXml.addListTable(mListTable, title);

//        mXml.outputDocumentToFile("c:\\", "TaskPrint1"); //输出xml文档到文件

    mResult.clear();

    mResult.addElement(mXml);

    return true;
}


/**
 * 返回数据
 * @return VData
 */
public VData getResult() {
    return this.mResult;
}

/**
 * 生成错误信息
 * @param cFunction String
 * @param cMsgError String
 */
private void buildError(String cFunction, String cMsgError) {

    CError tCError = new CError();
    tCError.moduleName = "QuarterCheckTeamBL";
    tCError.functionName = cFunction;
    tCError.errorMessage = cMsgError;

    this.mErrors.addOneError(tCError);

}

public static void main(String[] args) {
//        QuarterCheckTeam quartercheckteam = new QuarterCheckTeam();

    GlobalInput tG = new GlobalInput();
    tG.Operator = "xxx";
    tG.ManageCom = "86";
    TransferData tTransferData = new TransferData();

    tTransferData.setNameAndValue("ManageCom", "8611");
    tTransferData.setNameAndValue("AssessYear", "1");
    tTransferData.setNameAndValue("BranchType", "2");
    tTransferData.setNameAndValue("BranchType2", "01");

    VData tVData = new VData();
    tVData.addElement(tTransferData);
    tVData.addElement(tG);

    QuarterCheckTeamBL tQuarterCheckTeamBL = new QuarterCheckTeamBL();

    if (!tQuarterCheckTeamBL.submitData(tVData, "PRINT")) {
        System.out.println("11111111111");
    } else {
        System.out.println("22222222222");
    }

}
}
