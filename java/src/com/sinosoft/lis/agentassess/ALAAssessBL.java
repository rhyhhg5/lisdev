package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vdb.LAAssessDBSet;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */
public class ALAAssessBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";
    private String mIndexCalNo = "";
    private String mBranchType = "";

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LAAssessSchema mLAAssessSchema = new LAAssessSchema();
    private LAAssessSet mUpdateLAAssessSet = new LAAssessSet();
    private LATreeSet mLATreeSet = new LATreeSet(); //更新成考核后的新职级
    private LATreeBSet mLATreeBSet = new LATreeBSet(); //更新成考核后的新职级
    private LADimissionSet mLADimissionSet = new LADimissionSet(); //考核为清退
    private String mUpdateLAAgentSQL = ""; //更新代理人表的代理人状态的sql语句

    public ALAAssessBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("submitData");

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start ALAAssessBL Submit...");

        ALAAssessBLS tALAAssessBLS = new ALAAssessBLS();
        tALAAssessBLS.submitData(mInputData, cOperate);
        System.out.println("End ALAAssessBL Submit...");

        //如果有需要处理的错误，则返回
        if (tALAAssessBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tALAAssessBLS.mErrors);

            CError tError = new CError();
            tError.moduleName = "ALAAssessBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //        if (!updateData())
        //        {
        //            System.out.println("update fail--"+this.mErrors.getFirstError());
        //            return false;
        //        }
        mInputData = null;

        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0);
        this.mManageCom = (String) cInputData.getObject(1);
        this.mIndexCalNo = (String) cInputData.getObject(2);
        this.mBranchType = (String) cInputData.getObject(3);

        if ((this.mGlobalInput == null) || (this.mManageCom == null) ||
            (this.mIndexCalNo == null))
        {
            CError tError = new CError();
            tError.moduleName = "ALAssessBL";
            tError.functionName = "getInputData()";
            tError.errorMessage = "前台传入数据未初始化";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     *业务逻辑处理
     */
    private boolean dealData()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        String tAgentCode = "";
        String tAgentGrade1 = "";
        String tAgentSeries1 = "";
        LATreeSchema tLATreeSchema;
        LAAssessDB tLAAssessDB = new LAAssessDB();
        tLAAssessDB.setManageCom(this.mManageCom);
        tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
        tLAAssessDB.setBranchType(this.mBranchType);
        tLAAssessDB.setState("0");

        LAAssessSet tLAAssessSet = new LAAssessSet();
        tLAAssessSet = tLAAssessDB.query();

        if (tLAAssessDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "ALAssessBL";
            tError.functionName = "dealData()";
            tError.errorMessage = "查询考评信息表出错！";
            this.mErrors.addOneError(tError);

            return false;
        }

        for (int i = 1; i <= tLAAssessSet.size(); i++)
        {
            //考评信息
            mLAAssessSchema = tLAAssessSet.get(i);
            tAgentCode = mLAAssessSchema.getAgentCode();

            //银代、法人确认后状态直接置为2
            if (!this.mBranchType.equals("1"))
            {
                mLAAssessSchema.setState("2");
            }
            else
            {
                mLAAssessSchema.setState("1");
            }

            mLAAssessSchema.setModifyDate(currentDate);
            mLAAssessSchema.setModifyTime(currentTime);
            mLAAssessSchema.setOperator(this.mGlobalInput.Operator);
            this.mUpdateLAAssessSet.add(mLAAssessSchema);

            //如果职级为维持，那么latree表不做任何操作
            if (!mLAAssessSchema.getAgentGrade().equals(mLAAssessSchema.
                    getAgentGrade1()))
            {
                //修改行政信息表的职级
                LATreeDB tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(tAgentCode);

                if (!tLATreeDB.getInfo())
                {
                    CError tError = new CError();
                    tError.moduleName = "LAAssessInputBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询专管员" + tAgentCode + "行政信息出错！";
                    this.mErrors.addOneError(tError);

                    return false;
                }

                //行政信息表的职级备份
                LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLATreeBSchema, tLATreeDB.getSchema());
                tLATreeBSchema.setAstartDate(tLATreeDB.getAstartDate());

                String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                tLATreeBSchema.setEdorNO(tEdorNo);
                tLATreeBSchema.setRemoveType("06");
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
                tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
                tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
                this.mLATreeBSet.add(tLATreeBSchema);

                tLATreeSchema = new LATreeSchema();
                tLATreeSchema.setSchema(tLATreeDB.getSchema());
                tLATreeSchema.setAgentLastGrade(tLATreeSchema.getAgentGrade());
                tLATreeSchema.setAgentLastSeries(tLATreeSchema.getAgentSeries());

                //tLATreeSchema.setAgentSeries(tAgentSeries1);
                tLATreeSchema.setAgentGrade(mLAAssessSchema.getAgentGrade1());
                tLATreeSchema.setModifyDate(currentDate);

                String tDate = mIndexCalNo.substring(0, 4) + "-" +
                               mIndexCalNo.substring(4) + "-01";
                String ttDate = PubFun.calDate(tDate, 1, "M", null);
                tLATreeSchema.setStartDate(ttDate);
                tLATreeSchema.setAstartDate(ttDate);
                //xijh Add 01/25/05，对Assesstype和state字段置值。
                if (mLAAssessSchema.getAgentGrade1().equals("00"))
                {
                    tLATreeSchema.setState("3"); //离职
                }
                else
                {
                    if (mLAAssessSchema.getAgentGrade().compareToIgnoreCase(
                            mLAAssessSchema.
                            getAgentGrade1()) > 0)
                    {
                        tLATreeSchema.setAssessType("2"); //降级
                    }
                    if (mLAAssessSchema.getAgentGrade().compareToIgnoreCase(
                            mLAAssessSchema.
                            getAgentGrade1()) < 0)
                    {
                        tLATreeSchema.setAssessType("1"); //升级
                    }
                }

//                tLATreeSchema.setStartDate(this.mIndexCalNo.substring(0,4)+"-"
//                    +String.valueOf(Integer.parseInt(mIndexCalNo.substring(4,6))+1)+"-01");
//                tLATreeSchema.setAstartDate(this.mIndexCalNo.substring(0,4)+"-"
//                    +String.valueOf(Integer.parseInt(mIndexCalNo.substring(4,6))+1)+"-01");
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(this.mGlobalInput.Operator);
                this.mLATreeSet.add(tLATreeSchema);

                //对于考核清退的专管员的处理
                if (mLAAssessSchema.getAgentGrade1().equals("00"))
                {
                    //置LAAgent表中代理人状态为离职
                    this.mUpdateLAAgentSQL += (this.mUpdateLAAgentSQL.equals("")
                                               ? tAgentCode : ("," +
                            tAgentCode));

                    //在离职表中增加一条记录
                    LADimissionSchema tLADimissionSchema = new
                            LADimissionSchema();
                    tLADimissionSchema.setAgentCode(tAgentCode);
                    tLADimissionSchema.setBranchType(mLAAssessSchema.
                            getBranchType());
                    tLADimissionSchema.setDepartDate(currentDate);
                    tLADimissionSchema.setDepartRsn("考核清退");
                    tLADimissionSchema.setDepartTimes(1);
                    tLADimissionSchema.setMakeDate(currentDate);
                    tLADimissionSchema.setMakeTime(currentTime);
                    tLADimissionSchema.setModifyDate(currentDate);
                    tLADimissionSchema.setModifyTime(currentTime);
                    tLADimissionSchema.setOperator(this.mGlobalInput.Operator);
                    this.mLADimissionSet.add(tLADimissionSchema);
                }
            }
        }

        if (!this.mUpdateLAAgentSQL.equals(""))
        {
            this.mUpdateLAAgentSQL = "UPDATE LAAgent SET AgentState = '03'," +
                                     "ModifyDate = '" + currentDate + "'," +
                                     "ModifyTime = '" + currentTime + "' " +
                                     "WHERE AgentCode in (" +
                                     this.mUpdateLAAgentSQL + ")";
        }

        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mUpdateLAAssessSet);
            this.mInputData.add(this.mLATreeSet);
            this.mInputData.add(this.mLADimissionSet);
            this.mInputData.add(this.mUpdateLAAgentSQL);
            this.mInputData.add(this.mLATreeBSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAssessBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        boolean tReturn = true;
        System.out.println("Start Save...");

        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAssessBL";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 考评信息更新...");

            if (this.mUpdateLAAssessSet.size() > 0)
            {
                LAAssessDBSet tLAAssessDBSet = new LAAssessDBSet(conn);
                tLAAssessDBSet.set(mUpdateLAAssessSet);

                if (!tLAAssessDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAssessDBSet.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ALAssessBL";
                    tError.functionName = "updateData";
                    tError.errorMessage = "更新考评信息记录出错！";
                    this.mErrors.addOneError(tError);

                    try
                    {
                        conn.rollback();
                        conn.close();
                    }
                    catch (Exception e)
                    {
                    }

                    return false;
                }
            }

            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAssessBL";
            tError.functionName = "updateData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;

            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {
            }
        }

        return tReturn;
    }

    public static void main(String[] args)
    {
        ALAAssessBL tdd = new ALAAssessBL();
        VData tVData = new VData();
        GlobalInput tg = new GlobalInput();
        tg.Operator = "bb";
        tVData.add(tg);
        tVData.add("86130000");
        tVData.add("200312");
        tVData.add("3");
        tdd.submitData(tVData, "");
    }
}
