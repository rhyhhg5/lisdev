package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */

public class AdjustAssessBL
{
    // @Field
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mOperate = "";

//必须初始化的值
    private String ManageCom; //管理机构
    private String BranchType; //展业类型
    private String IndexCalNo; //佣金计算编码

    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private LAAssessSet mLAAssessSet = null;
    private LAAssessSet aLAAssessSet = new LAAssessSet();
    private LAAssessSchema mLAAssessSchema = null;

    private String AgentCode; //代理人代码

    public AdjustAssessBL()
    {
    }

// @Method
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        mLAAssessSet = (LAAssessSet) cInputData.getObjectByObjectName(
                "LAAssessSet", 0);
        return true;
    }

    private boolean dealData()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        System.out.println("update 插入lalaassess...");
        for (int i = 1; i <= mLAAssessSet.size(); i++)
        {
            mLAAssessSchema = mLAAssessSet.get(i);
            String SQL = "select managecom From laagent where agentcode ='" +
                         mLAAssessSchema.getAgentCode()
                         + "' and managecom like '" + mGlobalInput.ManageCom +
                         "%'";
            ExeSQL aExeSQL = new ExeSQL();
            String MM = aExeSQL.getOneValue(SQL);
            if (MM == null || MM.trim().equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AdjustAssessBL";
                tError.functionName = "submitData";
                tError.errorMessage = "此代理人不属于管辖范围";
                this.mErrors.addOneError(tError);
                return false;
            }
            String tSQL = "update LAAssess set state='9',ModifyDate='" +
                          currentDate + "',ModifyTime='" + currentTime
                          + "' where agentcode='" +
                          mLAAssessSchema.getAgentCode() + "' and managecom='" +
                          MM
                          + "' and indexcalno ='" +
                          mLAAssessSchema.getIndexCalNo() +
                          "' and branchtype='" + mLAAssessSchema.getBranchType() +
                          "'";
            ExeSQL tExeSQL = new ExeSQL();
            System.out.println("插入记录的SQL: " + tSQL);
            tExeSQL.execUpdateSQL(tSQL);

            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalIndex";
                tError.functionName = "insertAssess";
                tError.errorMessage = "执行SQL语句：判断记录是否存在失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            aLAAssessSet.add(mLAAssessSchema);
        }
        return true;
    }

    public boolean submitData(VData cInputData)
    {
        try
        {
            if (!getInputData(cInputData))
            {
                return false;
            }
            if (!dealData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AdjustAssessBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败AdjustAssessBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }

            System.out.println("End AdjustAssessBL Submit...");
            //如果有需要处理的错误，则返回
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "AdjustAssessBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
    }
}
