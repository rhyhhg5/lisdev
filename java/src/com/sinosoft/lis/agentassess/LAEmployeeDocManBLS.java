package com.sinosoft.lis.agentassess;

/**
 * <p>Title: 档案管理程序</p>
 * <p>Description: 实现档案调入、调出处理对数据库的操作</p>
 * <p>Copyright: Copyright (c) 2004-03-31</p>
 * <p>Company: sinosoft </p>
 * @author LL
 * @version 1.0
 */

import java.sql.Connection;

import com.sinosoft.lis.vdb.LAAgentBDBSet;
import com.sinosoft.lis.vdb.LAAgentDBSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class LAEmployeeDocManBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;

    public LAEmployeeDocManBLS()
    {
    }

    public static void main(String[] args)
    {
        LAEmployeeDocManBLS LAEmployeeDocManBLS1 = new LAEmployeeDocManBLS();
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start LAEmployeeDocManBLS Submit...");
        //保存数据
        tReturn = saveLAEmployeeDocMan(cInputData);

        if (tReturn)
        {
            System.out.println(" Successful");
        }
        else
        {
            System.out.println("Save Failed");
        }
        System.out.println("End LAEmployeeDocManBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAEmployeeDocMan(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeDocManBLS";
            tError.functionName = "saveLAEmployeeAscription";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");

            //备份LAAgentB表
            LAAgentBSet tLAAgentBSet = new LAAgentBSet();
            tLAAgentBSet.set((LAAgentBSet) mInputData.getObject(1));
            if (tLAAgentBSet.size() > 0)
            {
                System.out.println("备份LAAgentB表 ");
                LAAgentBDBSet tLAAgentBDBSet = new LAAgentBDBSet(conn);
                tLAAgentBDBSet.set(tLAAgentBSet);
                if (!tLAAgentBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAgentBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeDocManBLS";
                    tError.functionName = "saveLAEmployeeDocMan";
                    tError.errorMessage = "备份LAAgent表数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            //修改LAAgent表
            LAAgentSet tLAAgentSet = new LAAgentSet();
            tLAAgentSet.set((LAAgentSet) mInputData.getObject(2));
            if (tLAAgentSet.size() > 0)
            {
                System.out.println("备份LAAgent表 ");
                LAAgentDBSet tLAAgentDBSet = new LAAgentDBSet(conn);
                tLAAgentDBSet.set(tLAAgentSet);
                if (!tLAAgentDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAgentDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeDocManBLS";
                    tError.functionName = "saveLAEmployeeDocMan";
                    tError.errorMessage = "更新LAAgent表数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeDocManBLS";
            tError.functionName = "saveLAEmployeeDocMan";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            ;
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;

        return true;
    }
}
