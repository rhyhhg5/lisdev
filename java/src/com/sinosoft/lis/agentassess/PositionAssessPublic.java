package com.sinosoft.lis.agentassess;


/**
 * <p>Title: 职称考核公共类</p>
 * <p>Description: 职称考核公共类，从考核公共类继承</p>
 * <p>Copyright: Copyright (c) sinosoft 2004</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public abstract class PositionAssessPublic extends AssessPublic implements
        PositionAssessInterface
{
    //全局变量
    //protected CErrors mErrors = new CErrors(); //错误处理类
    //protected GlobalInput mGlobalInput = new GlobalInput(); //保存传入参数

    public PositionAssessPublic()
    {
    }

    public static void main(String[] args)
    {
    }
}
