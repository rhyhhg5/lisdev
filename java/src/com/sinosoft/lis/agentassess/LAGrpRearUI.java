package com.sinosoft.lis.agentassess;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;


public class LAGrpRearUI
{
    public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public LAGrpRearUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        LAGrpRearBL tLAGrpRearBL=new LAGrpRearBL();
        try
        {
            if (!tLAGrpRearBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLAGrpRearBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAGrpRearUI";
                tError.functionName = "submitData";
                tError.errorMessage = "BL�ദ��ʧ��!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
      //  mResult=LAGrpRearBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
