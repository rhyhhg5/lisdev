package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.agentcalculate.AgCalBase;
import com.sinosoft.lis.agentcalculate.CalIndex;
import com.sinosoft.lis.agentwages.AgentMonthCal;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.db.LAAssessHistoryDB;
import com.sinosoft.lis.db.LAAssessIndexDB;
import com.sinosoft.lis.db.LAAssessMainDB;
import com.sinosoft.lis.db.LAFirstAssessDB;
import com.sinosoft.lis.db.LAIndexVsAssessDB;
import com.sinosoft.lis.db.LAStatSegmentDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAssessMainSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LAFirstAssessSchema;
import com.sinosoft.lis.schema.LAIndexVsAssessSchema;
import com.sinosoft.lis.schema.LAStatSegmentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAAssessHistorySet;
import com.sinosoft.lis.vschema.LAAssessMainSet;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.lis.vschema.LAFirstAssessSet;
import com.sinosoft.lis.vschema.LAIndexVsAssessSet;
import com.sinosoft.lis.vschema.LAStatSegmentSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/*
 * <p>Title: 考核指标计算类 </p>
 * <p>Description: 计算某管理机构下所有代理人的考核指标值 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft</p>
 * @author WuHao
 * @version 1.1
 */
public class HDAssessBL
{
    // @Field
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    //必须初始化的值
    private String mOperate;
    private VData mInputData = new VData();
    private MMap mMap = new MMap();

    private String mAreaType; //地区类型
    private String mManageCom; //管理机构
    private String mStartManageCom; //管理机构
    private String mAgentSeries; //考核职级系列
    private String mYear; //指标计算年份
    private String mMonth; //指标计算月份
    private String mBranchType;
    private String mBranchType2;
    private String mCalFlag = "0";  //是否套转计算标志   0：正常计算非套转  1：套转计算
    private String mOperator;
    private String mFirstAssessMark = "0"; //标记职级是否为第一次考核 1:第一次考核 0:非第一次考核
    private String mDegradeFlag = "0"; //是否降级标志位  1：降级人员  0：非降级人员
    private String mIndexCalNo; //佣金计算编码
    private String mAssessIntv;//考核月数
    private String mPreIndexCalNo;//本考核季度前一考核季度
    private String mAgentCode; //代理人代码
    private String mAgentGrade; //考核职级
    private String mDestAgentGrade;//目标职级
    private String mAssessFlag;//维持标记
    private String mAssessType;//考核类型
    private CalIndex mCalIndex = new CalIndex();
    private VData vComData = new VData();  //通过薪资校验的管理机构
    //管理机构下的代理人存放集
    private LATreeSet mLATreeSet = new LATreeSet();
    //存指标信息
    private LAAssessIndexDB mLAAssessIndexDB;
    //职级对应的考核指标存放处
    private String[][] GradeIndex; //存放数据格式：{职级 - 考核代码|考核类型}
    //考核指标的考核sql存放处
    private String[][] GradeSql; //存放考核SQL代码
    private LAAssessSet mComLAAssessSet = new LAAssessSet(); //4位机构下所有的考核结果
    private LAAssessSet mLAAssessSet = new LAAssessSet(); //每个8位机构的考核结果
    private LAAssessMainSchema mLAAssessMainSchema = new LAAssessMainSchema();
    private LAAssessMainSet mLAAssessMainSet = new LAAssessMainSet();

    public HDAssessBL()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }
        //对传入数据的简单校验
        if (!checkData())
        {
            return false;
        }

        System.out.println("dealData...");
        if (cOperate.equals("INSERT||MAIN"))
        {
            if (!dealData())
            {
                if (mErrors.needDealError())
                {
                    System.out.println("程序异常结束原因：" + mErrors.getFirstError());
                }
                if (!returnState())
                {
                    return false;
                }
                return false;
            }
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("Start HDAssessBL Submit...");

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError.buildErr(this,"数据提交失败!");
            return false;
        }
        //回滚计算状态
        if (!returnState())
        {
            return false;
        }
        System.out.println("End HDAssessBL Submit.");
        return true;
    }

    private boolean checkData()
    {
        if (mAgentSeries.compareTo("2")>0)
        {
            CError.buildErr(this,"考核职级系列错误！");
            return false;
        }
        if (!checkState())
        {
            return false;
        }
        return true;
    }



    /**
     * 1 - 先校验机构是否满足本月佣金计算过，下个月佣金没算过的条件，
     * 不满则此机构不做考核处理；
     * 2 - 满足条件1则判断此机构是否满足本月作过佣金审核确认，没作过则给出错误提示；
     * 3 - 如果前两个条件均通过则此机构可以参加考核计算；
     * @return boolean
     */
    private boolean checkWageCal()
    {
        //增加校验，要求管理机构只能录入4位代码
        //判断录入的机构代码是否为4位
        if (mManageCom.length() != 8)
        {
            CError.buildErr(this, "管理机构只能录入8位代码！");
            return false;
        }
        String tSQL = "select * from ldcom where comcode like '" + mManageCom +
                      "%' and length(trim(comcode)) = 8 and sign='1'  order by comcode asc";
        LDComDB tLDComDB = new LDComDB();
        LDComSet cLDComSet = new LDComSet();
        cLDComSet = tLDComDB.executeQuery(tSQL);
        System.out.println("用4位管理机构代码查询8位管理机构Sql : " + tSQL);
        System.out.println("count : " + cLDComSet.size());
        String tComCode = "";

        //循环对每一个8位机构进行校验，校验条件为：
        for (int i = 1; i <= cLDComSet.size(); i++)
        {
            //获得8位管理机构,保证只计算8位管理机构
            tComCode = cLDComSet.get(i).getComCode().trim();
            //两个条件都满足则说明此机构可以参加考核计算
            vComData.add(tComCode);
            System.out.println("校验后符合考核计算的机构为：" + tComCode);
        }
        return true;
    }

    /**
     * 校验8位管理机构是否作过考核计算
     * @param cManageCom String
     * @param cAgentGrade String
     * @param cIndexCalno String
     * @return boolean
     */
    private boolean checkAssessCal(String cManageCom,
            String cAgentGrade, String cIndexCalno)
    {
        PubCheckField checkField1 = new PubCheckField();
        VData cInputData = new VData();
        TransferData tTransferData = new TransferData();
        LMCheckFieldSchema tLMCheckFieldSchema = new LMCheckFieldSchema();
        //设置计算时要用到的参数值
        tTransferData.setNameAndValue("IndexCalNo", cIndexCalno);
        tTransferData.setNameAndValue("AgentGrade", cAgentGrade);
        tTransferData.setNameAndValue("ManageCom", cManageCom);
        //通过CKBYFIELD
        tLMCheckFieldSchema.setRiskCode("000000");
        tLMCheckFieldSchema.setRiskVer("2004");
        tLMCheckFieldSchema.setFieldName("IsAlreadyAssessBL ");
        //通过 CKBYFIELD 方式校验
        cInputData.add(tTransferData);
        cInputData.add(tLMCheckFieldSchema);
        if (!checkField1.submitData(cInputData, "CKBYFIELD"))
        {
            System.out.println("校验机构是否作过考核时发现错误");
            //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
            if (checkField1.mErrors.needDealError())
            {
                System.out.println("ERROR-S-" +
                                   checkField1.mErrors.getFirstError());
                CError.buildErr(this,"判断机构是否作过考核计算时出错！！");
                return false;
            }
            else
            {
                VData t = checkField1.getResultMess();
                CError.buildErr(this,"管理机构" + this.mManageCom + ","
                                   + t.get(0).toString());
                return false;
            }
        }
        return true;
    }

    /**
     *通过录入的代理机构，查询出相关的其他下级的代理机构
     * 例如录入了8611，则会查出86110000
     */
    private boolean dealData()
    {
        System.out.println("be calculating... ");
        if (!checkWageCal())
        {
            return false;
        }
        //1 - 判断是否存在符合考核计算条件的机构，如果存在则进行考核计算
        if (vComData.size() != 0)
        {
            System.out.println("符合考核计算的8位管理机构个数为：" + vComData.size());
            for (int i = 0; i < vComData.size(); i++)
            {
                this.mManageCom = (String) vComData.getObject(i);
                System.out.println( "进行考核计算的8位机构代码为：" + mManageCom);

                //逐个对每个管理机构进行计算
                System.out.println("开始计算 " + mManageCom + " 管理机构...");

                if (!Calculate())
                {
//                    if (mErrors.needDealError())
//                    {
//                        System.out.println("程序异常结束原因：" + mErrors.getFirstError());
//                    }
                    return false;
                }
            }
        }
        else
        {
            CError.buildErr(this,"不存在符合考核计算条件的机构！");
            return false;
        }
        return true;
    }

    /**
     *校验输入参数合法性
     */
    public boolean getInputData(VData cInputData)
    {
        try
        {
            System.out.println("get input data...");
            this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            this.mManageCom = (String) cInputData.getObject(1);
            this.mAgentSeries = (String) cInputData.getObject(2);
            this.mYear = (String) cInputData.getObject(3);
            this.mMonth = (String) cInputData.getObject(4);
            this.mBranchType = (String) cInputData.getObject(5);
            this.mBranchType2 = (String) cInputData.getObject(6);
            this.mCalFlag = (String) cInputData.getObject(7);
            this.mOperator = mGlobalInput.Operator;
            mIndexCalNo = mYear + mMonth;
            String indexCalDate = mIndexCalNo.substring(0,4)+"-"+mIndexCalNo.substring(4)
            +"-"+AgentPubFun.getAssessDay();
            String tPreCalDate = PubFun.calDate(indexCalDate, -3, "M", "");
            mPreIndexCalNo = AgentPubFun.ConverttoYM(tPreCalDate);
            mStartManageCom=mManageCom;
//            ExeSQL tExeSQL = new ExeSQL();
           // tExeSQL.execUpdateSQL("refresh table LATEMPMISION");
            System.out.println("mManageCom");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        if (mGlobalInput == null || mManageCom == null || mYear == null ||mMonth == null)
        {
            CError.buildErr(this, "没有得到足够的数据！");
            return false;
        }

//       String cType = "";
//       if (mBranchType.equals("2"))
//           cType = "02";
//       if (mBranchType.equals("1"))
//           cType = "01";
//
       this.mAreaType = getAreaType(mManageCom);
       if (mAreaType == null)
       {
           //错误处理
           CError.buildErr(this, "查询管理机构" + this.mManageCom +
                           "的地区类型出错！");
           return false;
       }
       if (mCalFlag == null || mCalFlag.equals(""))
       {
           mCalFlag = "0"; //默认为正常计算
       }

       return true;
   }
    /**
     *准备传入后台数据
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            if (mOperate.equals("INSERT||MAIN"))
            {
            	this.mMap.put(this.mLAAssessMainSet,"DELETE&INSERT");
                this.mMap.put(this.mComLAAssessSet,"DELETE&INSERT");
                this.mInputData.add(mMap);
            }
            else if (mOperate.equals("DELETE||MAIN"))
            {
                this.mInputData.add(mIndexCalNo);
                this.mInputData.add(mManageCom);
                this.mInputData.add(mAgentGrade);
                this.mInputData.add(mBranchType);
                this.mInputData.add(this.mGlobalInput);
                //todo:删除考核记录功能
            }
        }
        catch(Exception ex)
        {
            // @@错误处理
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错！");
            return false;
        }
        return true;
    }

    /** 计算考核指标 */
    private boolean Calculate()
    {
        String tSQL = "", tReturn = "";
        String tCalPrptyType = "";
        String tGrade = "";
        int tCount = 0, iMax = 0, iPos = 0;
        SSRS tCalPrptySSRS;
        boolean tExist = false, tNew = false;
        System.out.println("get agentgrades array...");
        String[] agentGrades = this.getGradesBySeries(mAgentSeries,mBranchType,mBranchType2,mMonth);


        for (int m = 0; m < agentGrades.length; m++)
        {
            mAgentGrade = agentGrades[m];
            this.mLAAssessMainSchema = new LAAssessMainSchema();
            this.mLAAssessSet = new LAAssessSet();
            System.out.println("开始职级"+mAgentGrade+"考核计算...");

            //校验管理机构是否作过考核计算
            if (!checkAssessCal(mManageCom,mAgentGrade,mIndexCalNo))
            {
                return false;
            }

            //查询计算日期的起止期
            System.out.println("计算日期的起止期");
            if (!setBeginEnd())
            {
                return false;
            }
            //查询考核指标的指标计算属性 循环计算 ，原则是先纵向后横向，但目前定义的指标的计算属性只有纵向即01
            tSQL =
                    "select distinct CalPrpty from LAAssessIndex Where IndexType = '02' and branchtype ='5' and branchtype2='01' " //02即考核的指标
                    + "order by CalPrpty asc";
            ExeSQL tExeSQL = new ExeSQL();
            tCalPrptySSRS = tExeSQL.execSQL(tSQL);
            tCount = tCalPrptySSRS.getMaxRow();
            //查询管理机构下的代理人（在考核年月之前入司的人员）
            //考核月临界日期
            String indexCalDate = mIndexCalNo.substring(0,4)+"-"+mIndexCalNo.substring(4)
                                  +"-"+AgentPubFun.getAssessDay();
            /**
             * 不同职级的业务员的考核时间确定目前需求是
             * 15号以后入职的按下个月推算
             * 15号以前的以当前月计算，见习客户的考核时间不确定
             * 
             * 
             * 
             */
//            indexCalDate = PubFun.calDate(indexCalDate, -1, "D", "");
            /*
             * 入职时职级定位互动专员
             * 15号之前（含15号）入职，算一个考核月，
             * 15号之后入职的，算到下一个考核月。
             * 不足两个考核月的，不进行季度考核。
             */
            String caldate = PubFun.calDate(indexCalDate, -1, "M", "");
//            /*
//             * 入职时职级定为高级互动业务经理二级（含二级）以上的互动专员，  考核期从入职后的下一完整月份开始计算。
//             * 即，2月11号入职的高级互动业务经理二级，2月份不算考核月，
//             * 自三月份开始计算考核月，在4月份进行季度考核时，
//             * 不足两个月，不进行考核，在7月份考核时，进行通考。
//             */
//            String caldate2 = PubFun.calDate(indexCalDate, -15, "D", "");
            LATreeDB tLATreeDB = new LATreeDB();

            tSQL = "select * from LATree a where a.ManageCom = '" + mManageCom +
                   "' and branchtype ='"+mBranchType+"' and branchtype2='"+mBranchType2+"' "
                   +" and a.AgentGrade = '" + mAgentGrade + "' and (speciflag<>'01' or speciflag is null ) ";
//            if (mAgentGrade.compareTo("U05")<=0)
//            {
                tSQL +=
                        "and exists (select 'X' from laagent where agentcode = a.AgentCode "
                        + " and employdate <= '" + caldate + "' "
                        //筹备人员不参与考核
                        + " and (noworkflag='N' or noworkflag is null) "
                        + " and agentstate in ('01','02')) ";
//            }
//            else
//            {
//                tSQL +=
//                        "and exists (select 'Y' from laagent where agentcode = a.AgentCode "
//                        + "and employdate <= '" + PubFun.calDate(caldate2, -1, "M", null)+"' "
//                        //筹备人员不参与考核
//                        + " and (noworkflag='N' or noworkflag is null)   "
//                        + "and agentstate in ('01','02'))";
//            }

//            tSQL +=
//                    "and not exists (select 'Z' from LATreeB where agentcode =a.agentcode and IndexCalNo = '"
//                    + mIndexCalNo + "' and AgentGrade <= '" + mAgentGrade +
//                    "' and removetype = '01' ) ";
            tSQL += " and not exists (select 'W' from laagent where agentcode =a.agentcode and branchcode in "
                    + " (select agentgroup from labranchgroup where state ='1')) ";

            tSQL += "order by a.AgentCode asc";
            mLATreeSet = tLATreeDB.executeQuery(tSQL);
            LATreeSchema tLATreeSchema = new LATreeSchema();
            iMax = mLATreeSet.size();
            System.out.println("查询管理机构" + mManageCom + "的考核人数的Sql：" + tSQL);
            System.out.println("管理机构下职级为"+mAgentGrade+"的人员个数：" + iMax);
            System.out.println("------------开始按指标计算属性循环" + tReturn);
            for (int i = 0; i < tCount; i++)
            {
                //从tReturn取指标类型
                tCalPrptyType = tCalPrptySSRS.GetText(i + 1, 1);
                System.out.println(String.valueOf(i) + "[--指标计算属性：" +tCalPrptyType +"--]");
                if (tCalPrptyType.equals("") || tCalPrptyType == null)
                {
                    continue;
                }
                //计算管理机构下所有代理人该指标计算属性的指标值
                System.out.println("---------代理人循环计算该指标计算属性的指标值");
                for (int j = 1; j <= iMax; j++)
                {
                    tExist = false;
                    tLATreeSchema = new LATreeSchema();
                    tLATreeSchema = mLATreeSet.get(j);
                    mAgentCode = tLATreeSchema.getAgentCode();
                    mAssessType = tLATreeSchema.getAssessType();
//                    if(mCalFlag.equals("0"))
//                    {//转序列考核不需要计算基础指标
//                        if (!dealBaseCode(mAgentCode)) {
//
//                        }
//                    }
                    tGrade = tLATreeSchema.getAgentGrade().trim();
                    System.out.print(String.valueOf(j) + "--" +
                                     this.mManageCom + "--代理人编码：" + mAgentCode +
                                     "--职级：" + tGrade);
                    //设置降级标志
                    mCalIndex.clearIndexSet();
                    mCalIndex.tAgCalBase.setRate("1");
                    mCalIndex.tAgCalBase.setAgentGroup(tLATreeSchema.getAgentGroup());
                    mCalIndex.tAgCalBase.setBranchCode(tLATreeSchema.getBranchCode());
                    mCalIndex.tAgCalBase.setAgentGrade(mAgentGrade);
                    mCalIndex.tAgCalBase.setZSGroupBranchAttr(AgentPubFun.
                            getAgentBranchAttr(this.mAgentCode)); //所在直辖组外部编码
                    mCalIndex.tAgCalBase.setZSGroupBranchSeries(AgentPubFun.
                            getBranchSeries(tLATreeSchema.getAgentGroup()));
                    mCalIndex.tAgCalBase.setBranchType(tLATreeSchema.
                            getBranchType());
                    mCalIndex.tAgCalBase.setBranchType2(tLATreeSchema.
                            getBranchType2());
                    mCalIndex.tAgCalBase.setAssessType(tLATreeSchema.
                    		getAssessType());
                    mCalIndex.tAgCalBase.setManageCom(this.mManageCom);
                    mCalIndex.tAgCalBase.setAreaType(mAreaType);
                    //数组中存在该职级否
                    if (!tNew)
                    {
                        System.out.println("[-----存储第一个职级考核指标编码----]");
                        GradeIndex = new String[1][]; //考核代码
                        GradeSql = new String[1][]; //考核sql
                        System.out.println("length:" +
                                           String.valueOf(GradeIndex.length));
                        if (!keepGrade(tGrade, 0))
                        {
                            System.out.println(tGrade +
                                               "---职级在考核指标对照表中没有对应的指标！");
                            String[][] temp = null;
                            GradeIndex = temp;
                            GradeSql = temp;
                            continue;
                        }
                        tNew = true;
                        iPos = 0;
                    }
                    else
                    {
                        for (int k = 0; k < GradeIndex.length; k++)
                        {
                            if (GradeIndex[k][0].equals(tGrade.trim()))
                            {
                                System.out.println("--职级数组中已存在该职级");
                                tExist = true;
                                iPos = k;
                                break;
                            }
                        }
                        //职级对应的考核代码集加入数组中
                        if (!tExist)
                        {
                            String[][] temp1 = GradeIndex;
                            String[][] temp2 = GradeSql;
                            GradeIndex = new String[GradeIndex.length + 1][];
                            GradeSql = new String[GradeSql.length + 1][];
                            for (int jj = 0; jj < temp1.length; jj++)
                            {
                                GradeIndex[jj] = temp1[jj];
                                GradeSql[jj] = temp2[jj];
                            }
                            System.out.println("存入职级数组中职级个数:" +
                                               String.valueOf(GradeIndex.length));
                            if (!keepGrade(tGrade, GradeIndex.length - 1))
                            {
                                System.out.println(tGrade +
                                        "职级在考核指标对照表中没有对应的指标！");
                                GradeIndex = temp1;
                                GradeSql = temp2;
                                continue;
                            }
                            iPos = GradeIndex.length - 1;
                        }
                    }
                    System.out.println("在职级数组中的位置iPos:" + String.valueOf(iPos));
                    //循环计算所有考核指标值
                    System.out.println("开始循环计算该职级对应的考核代码");
                    System.out.println("mCalIndex.tAgCalBase,=+++++++-----"+mCalIndex.tAgCalBase.getTempBegin());
                    if (!WageCal(iPos, mAgentCode,mAssessType, tCalPrptyType,
                                 tCount - 1 == i))
                    {
                        System.out.println("Error:" +
                                           this.mErrors.getFirstError());
                        return false;
                    }
                    this.mFirstAssessMark = "0";
                    this.mDegradeFlag = "0";
                }
            } //end of 指标计算属性for
            //将当前考核职级考核人数记录到考核主表,记录正常考核的,套转的不记录考核主表
            if (this.mCalFlag.equals("0"))
            {
                if (!addLAAssessMain(mAgentGrade, this.mLAAssessSet.size()))
                {
                    return false;
                }
            }
            //将每个职级所有人的考核结果加入提交set中
            this.mComLAAssessSet.add(this.mLAAssessSet);
        }
        return true;
    }

    /**
     *循环计算一职级对应的所有考核代码
     * @return boolean 计算出错返回false
     **/
    private boolean WageCal(int cPos, String cAgentCode,String cAssessType, String cIndexType,
                            boolean cIsAssess)
    {
        String tReturn = "";
        String tAssessCode = "", tAssessType = "";
        String assessFlagSQL = "select max(assessflag) from laassess where agentcode ='"+cAgentCode+"' " +
        		"and substr('"+this.mIndexCalNo+"',1,4)=substr(indexcalno,1,4) and state =2 with ur";
        ExeSQL tExeSQL = new ExeSQL();
        this.mAssessFlag = tExeSQL.getOneValue(assessFlagSQL);
//        this.mAssessFlag="U99";
        //执行优先考核sql
//        if (cIndexType.equals("01"))
//        {
//            System.out.println("$-----------开始计算优先考核-----------$");
//            tAssessCode = GradeIndex[cPos][1].substring(0,
//                    GradeIndex[cPos][1].indexOf("|"));
//            tAssessType = GradeIndex[cPos][1].substring(GradeIndex[cPos][1].
//                    indexOf("|") + 1);
            //正常考核才做优先考核，套转不做优先考核
//            if (this.mCalFlag.equals("0"))
//            {
//                switch (execFirstAssess(mAgentGrade, cAgentCode, tAssessCode)) //解约
//                {
//                    case 0:
//                        return false;
//                    case 1:
//                    {
//                        System.out.println("不清退");
//                        break;
//                    }
//                    case 2:
//                    {
//                        System.out.println("清退");
//                        return true;
//                    }
//                }
//            }
//        }
        System.out.println("mCalIndex.tAgCalBase,=+++++++-----"+mCalIndex.tAgCalBase.getQuauterBegin());
        /* 设置基本参数 */
        if (!setBaseValue(mCalIndex.tAgCalBase, cAgentCode, mAgentGrade))
        {
            return false;
        }
        //CalIndex mCalIndex = new CalIndex();
        //维持和晋升都不考核则直接返回
        if (mCalIndex.tAgCalBase.AssessMark == 0)
        {
            System.out.println("代理人" + cAgentCode + "本次不考核！");
            return true;
        }
        for (int i = 1; i < GradeIndex[cPos].length; i++)
        {
            //--------计算考核代码对应指标代码
            System.out.println(String.valueOf(i) + "--考核代码：" +
                               GradeIndex[cPos][i]);
            tAssessCode = GradeIndex[cPos][i].substring(0,
                    GradeIndex[cPos][i].indexOf("|"));
            tAssessType = GradeIndex[cPos][i].substring(GradeIndex[cPos][i].
                    indexOf("|") + 1);
            //维持和考核的指标在LAIndexInfo表中存于两个记录里
            mCalIndex.clearIndexSet();
            System.out.println("HDAssessBL--ResultIndexSet--" +
                               mCalIndex.getIndexSetSize());
            //查询考核代码对应的指标信息
            if (!queryIndex(tAssessCode, GradeIndex[cPos][0]))
            {
                return false;
            }
            System.out.println("指标计算属性比较：" + mLAAssessIndexDB.getCalPrpty() +
                               "  " + cIndexType);
            if (mLAAssessIndexDB.getIndexType().trim().equals("03"))
            {
                System.out.println("临时类型指标不计算，跳过");
                continue;
            }
            if (mLAAssessIndexDB.getCalPrpty().trim().compareTo(cIndexType) > 0)
            {
                System.out.println("指标计算属性不符，横向指标跳过");
                continue;
            }
            //指标计算
            System.out.println("--该考核代码对应的指标值表中没有，开始计算。。。");
            mCalIndex.setAssessIndex(mLAAssessIndexDB.getSchema());
            mCalIndex.setAgentCode(cAgentCode);
            mCalIndex.setIndexCalNo(mIndexCalNo);
            mCalIndex.setOperator(mOperator);
            mCalIndex.setIndexCalPrpty(cIndexType);
                        System.out.println("TempBegin1:" +
                               mCalIndex.tAgCalBase.getTempBegin());
            System.out.println("AgentCode:" + mCalIndex.tAgCalBase.getAgentCode());
            tReturn = mCalIndex.Calculate();
            if (mCalIndex.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mCalIndex.mErrors);
                CError.buildErr(this,this.mErrors.getFirstError());
                return false;
            }
            if (!cIsAssess)
            {
                System.out.println("现在不进行考核代码的计算！");
                continue;
            }
            //-------计算考核代码对应的sql计算编码
            System.out.println(String.valueOf(i) + "--sql计算编码：" +
                               GradeSql[cPos][i]);
            if ((GradeSql[cPos][i] != null) && (!GradeSql[cPos][i].equals("")))
            {
                //计算考核sql
                Calculator tCal = new Calculator();
                //设置计算编码
                tCal.setCalCode(GradeSql[cPos][i]);
                tCal.addBasicFactor("BranchType", this.mBranchType);
                tCal.addBasicFactor("BranchType2", this.mBranchType2);
                tCal.addBasicFactor("BranchType3", cAssessType);
                tCal.addBasicFactor("ManageCom", mManageCom);
                tCal.addBasicFactor("AssessIntv",this.mAssessIntv);
                tCal.addBasicFactor("AssessFlag",this.mAssessFlag);
                tCal.addBasicFactor("IndexCalNo",this.mIndexCalNo);
                //增加基本要素
                addAgCalBase(tCal);
                tReturn = tCal.calculate();
                if (tCal.mErrors.needDealError())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tCal.mErrors);
                    CError.buildErr(this,"考核指标sql计算出错",this.mErrors);
                    return false;
                }
                if ((tReturn == null) || (tReturn.trim().equals("")))
                {
                    tReturn = "0";
                }
                if (!insertAssess(mIndexCalNo, mAgentCode, GradeIndex[cPos][0],
                                  tAssessCode, tReturn,tAssessType))
                {
                    CError.buildErr(this,"插入考评信息表出错！");
                    return false;
                }
                System.out.println("考核代码对应sql编码计算结果：" + tReturn);

                //执行优先考核时会把一条记录插入到LAIndexInfo表中，插入时对StartDate置值会出错
                //所以在此计算完某个人的维持或晋升指标后会对其StartDate标志置值
                //维持考核更新
//                if (mAgentGrade.equals("U01")||mAgentGrade.equals("U02"))
//                {
                    String sql1 = "";
                    sql1 = "update LAIndexInfo set StartDate = '" +
                               mCalIndex.tAgCalBase.getTempBegin() + "' "
                               + " where IndexCalNo = '" + mIndexCalNo + "' "
                               + " and agentcode = '" + cAgentCode + "'";
                    sql1 += " and IndexType = '03' ";
                    ExeSQL tExeSQL1 = new ExeSQL();
                    boolean tValue = tExeSQL1.execUpdateSQL(sql1);
                    if (tExeSQL1.mErrors.needDealError())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tExeSQL.mErrors);
                        CError.buildErr(this,"执行SQL语句,更新LAIndexInfo表表中StartDate记录失败!");
                        return false;
                    }
//                }
            }
        }
        return true;
    }

    /**
     *判断指标在表中是否已存在
     * 参数：代理人编码，考核代码，代理人职级
     */
    private boolean queryIndex(String cWageCode,String cAgentGrade)
    {
//        String tTable = "", tCol = "", tSQL = "", tReturn = "";
        System.out.println("----取得考核代码对应的指标信息");
        LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
        tLAIndexVsAssessDB.setAgentGrade(cAgentGrade);
        tLAIndexVsAssessDB.setAssessCode(cWageCode);
        if (!tLAIndexVsAssessDB.getInfo())
        {
            //@错误处理
            this.mErrors.copyAllErrors(tLAIndexVsAssessDB.mErrors);
            CError.buildErr(this,"查询考核指标对应的表名字段名出错!");
            return false;
        }
        mCalIndex.tAgCalBase.setAssessType(tLAIndexVsAssessDB.getAssessType());
        mCalIndex.tAgCalBase.setDestAgentGrade(tLAIndexVsAssessDB.
                                               getDestAgentGrade());
        this.mDestAgentGrade = tLAIndexVsAssessDB.getDestAgentGrade();
        mLAAssessIndexDB = new LAAssessIndexDB();
        mLAAssessIndexDB.setIndexCode(tLAIndexVsAssessDB.getIndexCode());
        if (!mLAAssessIndexDB.getInfo())
        {
            //@错误处理
            this.mErrors.copyAllErrors(mLAAssessIndexDB.mErrors);
            CError.buildErr(this,"查询指标信息出错!");
            return false;
        }
        System.out.println("--算法编码：" + mLAAssessIndexDB.getCalCode());
        System.out.println("--指标计算属性：" + mLAAssessIndexDB.getCalPrpty());
        System.out.println("--指标类型：" + mLAAssessIndexDB.getIndexType());
        return true;
    }

    /**
     *职级对应的考核代码和考核sql存入数组中
     */
    private boolean keepGrade(String cGrade, int i)
    {
        int iSize = 0;
        String tCalcd = "", tSql = "";
        //职级为V01时，满足V01指标，降为U10状态
        if (cGrade.equals("V01"))
        {
           
                tSql = "select * from LAIndexVsAssess where AgentGrade = '" +
                                       cGrade +
                       "' and branchtype ='5' and branchtype2='01'" +
                       " order by DestAgentGrade,AssessType";
            
        }
        else //同系列考核
        {
//            if (cGrade.equals("B01"))
//            {
//                tSql = "select * from LAIndexVsAssess where AgentGrade = '" + cGrade +
//               "' and (substr(AgentGrade,1,1) = substr(DestAgentGrade,1,1) or DestAgentGrade = 'A00') order by DestAgentGrade,AssessType";
//            }
//            else
//            {
                tSql = "select * from LAIndexVsAssess where AgentGrade = '" + cGrade +
               "' and substr(AgentGrade,1,1) = substr(DestAgentGrade,1,1) order by DestAgentGrade,AssessType";
//            }
        }
        LAIndexVsAssessSet tLAIndexVsAssessSet = new LAIndexVsAssessSet();
        LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
        tLAIndexVsAssessSet = tLAIndexVsAssessDB.executeQuery(tSql); //职级对应的考核指标代码集
        iSize = tLAIndexVsAssessSet.size();
        if (iSize == 0)
        {
            //CError.buildErr(this,"未查到"+cGrade+"职级对应的考核指标！");
            return false;
        }
        System.out.println("考核对应指标个数：" + String.valueOf(iSize));
        GradeIndex[i] = new String[iSize + 1];
        GradeIndex[i][0] = cGrade;
        GradeSql[i] = new String[iSize + 1];
        GradeSql[i][0] = cGrade;
        LAIndexVsAssessSchema tLAIndexVsAssessSchema = new
                LAIndexVsAssessSchema();
        for (int ii = 1; ii <= iSize; ii++)
        {
            tLAIndexVsAssessSchema = tLAIndexVsAssessSet.get(ii);
            //考核代码|考核类型
            GradeIndex[i][ii] = tLAIndexVsAssessSchema.getAssessCode() + "|" +
                                tLAIndexVsAssessSchema.getAssessType();
            tCalcd = tLAIndexVsAssessSchema.getCalCode();
            if ((tCalcd == null) || (tCalcd.equals("")))
            {
                GradeSql[i][ii] = "";
            }
            else
            {
                GradeSql[i][ii] = tCalcd;
            }
            System.out.print("  " + GradeIndex[i][ii] + "|" + tCalcd);
        }
        return true;
    }

    /**
     *
     * @param cIndexCalNo String       考核年月
     * @param cAgentCode String        代理人编码
     * @param cGrade String            当前职级
     * @param cAssessCode String       考核代码
     * @param cIndexValue String       计算结果职级
     * @param cAssessType String       考核类型（维持、晋升）
     * @return boolean
     */
    private boolean insertAssess(String cIndexCalNo, String cAgentCode,
                                 String cGrade, String cAssessCode,
                                 String cIndexValue, String cAssessType)
    {
        //准备数据
        String tSQL = "";
        String tLastAgentCode = "";  //set中的最后一个agentcode
        String TableName = "", IColName = "", tModifyFlag = "";
        String tAgentSeries = "";
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        tLastAgentCode = this.mLAAssessSet.size() <= 0 ? ""
                 :this.mLAAssessSet.get(mLAAssessSet.size()).getAgentCode();

        //如果有可能一个人会产生两个及以上的考核结果,那么只保留最低的一个
        if (this.mLAAssessSet.size() > 0 &&tLastAgentCode.equals(cAgentCode))
        { //要提交的Set中已经有了当前人的一个考核结果记录
            if (cAssessType.equals("03") && !cIndexValue.equals("0"))
            { //晋升考核
                String modifyflag = this.mLAAssessSet.get(mLAAssessSet.size()).getModifyFlag();
                if (modifyflag.equals("01"))
                { //如果上个考核结果职级小于这个结果职级,就不更新
                    return true;
                }
                else
                { //否则就用这次的,删掉上次的
                    mLAAssessSet.remove(mLAAssessSet.get(
                            mLAAssessSet.size()));
                }
            }
            else if(cAssessType.equals("02") && !cIndexValue.equals("0"))
            { //维持（降级）考核
                String modifyflag = this.mLAAssessSet.get(mLAAssessSet.size()).getModifyFlag();
                if (modifyflag.equals("03"))
                { 
                    return true;
                }
                else
                { //否则就用这次的,删掉上次的
                    mLAAssessSet.remove(mLAAssessSet.get(mLAAssessSet.size()));
                }
            }
        }

        //查询佣金对应的表名和字段名
        LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
        tLAIndexVsAssessDB.setAgentGrade(cGrade);
        tLAIndexVsAssessDB.setAssessCode(cAssessCode);
        if (!tLAIndexVsAssessDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAIndexVsAssessDB.mErrors);
            CError.buildErr(this,"查询考核指标对照表出错！");
            return false;
        }
        TableName = tLAIndexVsAssessDB.getATableName();
        IColName = tLAIndexVsAssessDB.getAColName();
        String tCount = this.mLAAssessSet.size() <= 0 ? ""
                 :this.mLAAssessSet.get(mLAAssessSet.size()).getAgentCode();
        if (tCount.equals("") || !tCount.equals(cAgentCode))
        {
            if (TableName.equalsIgnoreCase("LAAssess"))
            {
                LAAssessDB tLAAssessDB = new LAAssessDB();
                LAAssessSchema tLAAssessSchema = new LAAssessSchema();
                tLAAssessSchema.setIndexCalNo(this.mIndexCalNo);
                tLAAssessSchema.setAgentCode(cAgentCode);
                tLAAssessSchema.setBranchType(mBranchType);
                tLAAssessSchema.setBranchType2(mBranchType2);
                tLAAssessSchema.setAgentGroup(mCalIndex.tAgCalBase.getAgentGroup());
                tLAAssessSchema.setManageCom(this.mManageCom);
                tLAAssessSchema.setModifyFlag("02"); // ModifyFlag在插入时默认为维持02
                tLAAssessSchema.setAgentGrade(mAgentGrade);
                tAgentSeries = AgentPubFun.getAgentSeries(mAgentGrade);
                tLAAssessSchema.setAgentSeries(tAgentSeries);
                tLAAssessSchema.setAgentGrade1(mAgentGrade); //初始插入时暂时为本职级
                tLAAssessSchema.setAgentSeries1(tAgentSeries);
                tLAAssessSchema.setCalAgentGrade(mAgentGrade);
                tLAAssessSchema.setCalAgentSeries(tAgentSeries);
                tLAAssessSchema.setState("0");
                tLAAssessSchema.setStandAssessFlag("1");
                tLAAssessSchema.setBranchAttr(mCalIndex.tAgCalBase.
                        getZSGroupBranchAttr());
                tLAAssessSchema.setAssessFlag(cIndexValue);
                tLAAssessSchema.setMakeDate(currentDate);
                tLAAssessSchema.setMakeTime(currentTime);
                tLAAssessSchema.setModifyDate(currentDate);
                tLAAssessSchema.setModifyTime(currentTime);
                tLAAssessSchema.setOperator(mOperator);
//                if(mCalFlag.equals("0"))
//                {
//                    if (cIndexValue.equals(cGrade) || cIndexValue.equals("0")) {
//                        System.out.println("不符晋升或降级，符维持");
//                        tLAAssessSchema.setTurnFlag("0"); //不可套转标记
//                        tLAAssessSchema.setTurnAgentGrade(""); //套转职级为空
//                        tLAAssessSchema.setTurnAgentSeries("");
//                        this.mLAAssessSet.add(tLAAssessSchema);
//                        return true;
//                    }
//                }
                String assessFlag ="";
                if (cGrade.compareTo(cIndexValue) > 0)
                { //降级 cGrade:原职级
                    tModifyFlag = "01";
                 //不论业务员职级标准，达不到维持标准就降级   
                    
                    if("V01".equals(mAgentGrade))
                    {
//                    	//第一次考核 降级置成U10职级
//                    		cIndexValue = mAgentGrade;
                    		tLAAssessSchema.setAssessFlag("U91");
                    } 		
                    	
//                    	//第二次考核  降级保持原级
//                    	else if("U92".equals(assessFlag))
//                    	{
//                    		tLAAssessSchema.setAssessFlag("U93");
//                    	}
                   

                }
                else if (cGrade.compareTo(cIndexValue) < 0)
                { //升级
                    tModifyFlag = "03";
                    if("U99".equals(cIndexValue))
                    {
                    	cIndexValue = mDestAgentGrade;
                    }
                    //如果是上次是第二次考核 降级维持  第三季度各月底薪为70%,如果三季度考核晋升标准，则底薪正常发放
                    if("U92".equals(assessFlag))
                    {
                    	tLAAssessSchema.setAssessFlag("U95");
                    }
                }
                else if (cGrade.compareTo(cIndexValue) == 0)
                { //维持
                    tModifyFlag = "02";
//                  //如果是上次是第二次考核 降级维持  第三季度各月底薪为70%,如果三季度考核达到维持标准，则底薪正常发放
//                    if("U92".equals(assessFlag))
//                    {
//                    	tLAAssessSchema.setAssessFlag("U94");
//                    }
                }
                //确定新职级对应的代理人系列
//                if (cIndexValue.equals("A00"))
//                { //清退
//                    tAgentSeries = "0";
//                }
//                else
//                {
                    tAgentSeries = AgentPubFun.getAgentSeries(cIndexValue);
                    tAgentSeries = tAgentSeries == null?"":tAgentSeries;
//                }
                tLAAssessSchema.setModifyFlag(tModifyFlag);
                //同系列考核结果
                if (mCalFlag.equals("0"))
                {
                    tLAAssessSchema.setAgentGrade1(cIndexValue);
                    tLAAssessSchema.setAgentSeries1(tAgentSeries);
                    tLAAssessSchema.setCalAgentGrade(cIndexValue);
                    tLAAssessSchema.setCalAgentSeries(tAgentSeries);
                    tLAAssessSchema.setTurnFlag("0"); //不可套转
                    tLAAssessSchema.setTurnAgentGrade(""); //套转职级
                    tLAAssessSchema.setTurnAgentSeries("");
                } //套转考核结果
                else if (mCalFlag.equals("1"))
                {
                    if(cIndexValue.equals("0"))
                    {
                        return true ;
                    }
                    tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
                    tLAAssessDB.setAgentCode(cAgentCode);
                    if (tLAAssessDB.getInfo())
                    {
                       tLAAssessSchema = tLAAssessDB.getSchema();
                       tLAAssessSchema.setTurnFlag("1"); //可套转
                       tLAAssessSchema.setTurnAgentGrade(cIndexValue);
                       tLAAssessSchema.setTurnAgentSeries(tAgentSeries);
                       tLAAssessSchema.setModifyDate(currentDate);
                       tLAAssessSchema.setModifyTime(currentTime);
                       tLAAssessSchema.setOperator(mOperator);
                    }
                }
                this.mLAAssessSet.add(tLAAssessSchema);
            }
        }
        else if (TableName.equalsIgnoreCase("LAAssess") )
        {
            if ("".equals(cIndexValue))
            { //降级
                tModifyFlag = "01";
                cIndexValue= mAgentGrade;
                System.out.println("-----come 1");
            }
            else if (cGrade.compareTo(cIndexValue) < 0)
            { //升级
                tModifyFlag = "03";
                System.out.println("-----come 2");
            }
            if (cIndexValue == null)
            {
                System.out.println("indexValue is null!" + cIndexValue);
            }
            //确定新职级对应的代理人系列
//            if (cIndexValue.equals("A00"))
//            { //清退
//                tAgentSeries = "0";
//            }
//            else
//            {
                tAgentSeries = AgentPubFun.getAgentSeries(cIndexValue);
//            }
            this.mLAAssessSet.get(mLAAssessSet.size()).
                    setModifyFlag(
                            tModifyFlag);
            this.mLAAssessSet.get(mLAAssessSet.size()).
                    setAgentGrade1(
                            cIndexValue);
            this.mLAAssessSet.get(mLAAssessSet.size()).
                    setAgentSeries1(
                            tAgentSeries);
            //this.mLAAssessSet.get(mLAAssessSet.size()).setAgentGroupNew();
            this.mLAAssessSet.get(mLAAssessSet.size()).
                    setCalAgentGrade(
                            cIndexValue);
            this.mLAAssessSet.get(mLAAssessSet.size()).
                    setCalAgentSeries(
                            tAgentSeries);
            this.mLAAssessSet.get(mLAAssessSet.size()).
                    setModifyDate(
                            currentDate);
            this.mLAAssessSet.get(mLAAssessSet.size()).
                    setModifyTime(
                            currentTime);
        }
        return true;
    }



    /**
     *判断某职级是否已考核归属完毕
     */
    private boolean checkAssessConfirm()
    {
        //得到上一职级
        String tLowGrade = "";
        String tSQL =
                "  select Trim(gradecode) from laagentgrade where  "
                + " gradecode < '" + mAgentGrade + "' "
                + " Order by gradecode desc";
        ExeSQL tExeSQL = new ExeSQL();
        tLowGrade = tExeSQL.getOneValue(tSQL);
        System.out.println("上一职级:" + tLowGrade);
        if (tExeSQL.mErrors.needDealError() ||
            tLowGrade == null || tLowGrade.equals(""))
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError.buildErr(this,"查询职级对应的级数出错!");
            return false;
        }
        tSQL = "Select Count(*) From LAAssessMain Where ManageCom = '" +
               mManageCom + "' "
               + "And BranchType = '" + mBranchType + "' And IndexCalNo = '" +
               mIndexCalNo + "' "
               + "And AgentGrade = '" + tLowGrade + "'";
        tExeSQL = new ExeSQL();
        String tCount = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError() ||
            tCount == null || tCount.equals(""))
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError.buildErr(this,"查询考核主表的考核记录出错!");
            return false;
        }
        if (tCount.equals("0"))
        {
            CError.buildErr(this,"职级" + tLowGrade + "还未作考核，无法作该职级的考核!");
            return false;
        }
        else
        {
            //是否已作归属
            tSQL = "select count(*) from laassess where indexcalno = '" +
                   mIndexCalNo + "' "
                   + "and agentgrade = '" + tLowGrade + "' and ManageCom = '" +
                   mManageCom + "' "
                   + "And State in ('0','1')";
            tExeSQL = new ExeSQL();
            tCount = tExeSQL.getOneValue(tSQL);
            if (tExeSQL.mErrors.needDealError() || tCount == null ||
                tCount.equals(""))
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError.buildErr(this,"查询考核表中职级" + tLowGrade + "是否已组织归属出错！");
                return false;
            }
            if (!tCount.equals("0"))
            {
                CError.buildErr(this,"职级" + tLowGrade + "还未作组织归属，无法作该职级的考核！");
                return false;
            }
        }
        return true;
    }

    private boolean lowGradeSeriesAssess()
    {
        //得到上一职级及其系列
        String tLowGrade = "";
        String tLowGradeSeries = "";
        String tSQL = "";
        ExeSQL tExeSQL = new ExeSQL();
        //业务职级系列不作校验
        if (mAgentSeries.equals("0"))
        {
            return true;
        }
        try
        {
            tSQL =
                    " select trim(gradeproperty2),trim(GradeCode) from laagentgrade where "
                    + " gradeproperty2 < '" + mAgentSeries + "' "
                    + " and branchtype='" + mBranchType+"'"
                    + " and branchtype2='" + mBranchType2 +
                    "' Order by gradecode desc";

            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(tSQL);
            if (tSSRS !=null&&tSSRS.getMaxRow()>0)
            {
                tLowGradeSeries = tSSRS.GetText(1, 1);
                tLowGrade = tSSRS.GetText(1, 2);
            }
            System.out.println("上一职级系列:" + tLowGradeSeries);
            if (tLowGradeSeries == null || tLowGradeSeries.equals(""))
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError.buildErr(this, "查询职级系列出错!");
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        try
        {//对于B01上一职级的校验：非正常考核月的考核日志中只有A01的记录，只需判断存在业务系列的考核记录即可
            if (mAgentGrade.equals("B01"))
            {
                tSQL = "Select Count(*) From LAAssessMain Where ManageCom = '" +
                         mManageCom + "' "
                         + "And BranchType = '" + mBranchType +
                         "' And BranchType2 = '" + mBranchType2 +
                         "' And IndexCalNo = '" +
                         mIndexCalNo + "' "
                         + "And AgentGrade like '" + tLowGrade.substring(0,1) + "%'";
            }
            else
            {
                tSQL = "Select Count(*) From LAAssessMain Where ManageCom = '" +
                       mManageCom + "' "
                       + "And BranchType = '" + mBranchType +
                       "' And BranchType2 = '" + mBranchType2 +
                       "' And IndexCalNo = '" +
                       mIndexCalNo + "' "
                       + "And AgentGrade = '" + tLowGrade + "'";
            }
            tExeSQL = new ExeSQL();
            String tCount = tExeSQL.getOneValue(tSQL);
            if (tExeSQL.mErrors.needDealError() ||
                tCount == null || tCount.equals(""))
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError.buildErr(this, "查询考核主表的考核记录出错!");
                return false;
            }
            if (tCount.equals("0"))
            {
                CError.buildErr(this, "低一职级系列还未作考核，无法作该职级系列的考核!");
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return true;
    }

    /**
     * 给基数类赋值
     *
     */
    private boolean setBaseValue(AgCalBase cAgCalBase, String cAgentCode,
                                 String cGrade)
    {
    	System.out.print("开始考核时间"+cAgCalBase.getQuauterBegin());
        System.out.println("是否第一次考核标志位：" + this.mFirstAssessMark);
        String tEndDate = cAgCalBase.getQuauterEnd();
        cAgCalBase.setTempEnd(tEndDate);
        //考核期限--待定
        int assessInv = 0;
        String tSql = "";
        ExeSQL tExeSQL;
        //在cAgCalBase中设置基本信息
        cAgCalBase.setAgentCode(cAgentCode);
        cAgCalBase.setWageNo(mIndexCalNo);
        cAgCalBase.setAreaType(mAreaType);
        cAgCalBase.setRate("1");
        //初始化考核类型：在此设置维持和晋升均不考核，
        //具体考核类型在各个职级中具体处理
        cAgCalBase.AssessMark = 0;
        //由职级起聘日期到考核期末止期来计算考核月数
        //其正确性已由增员操作保证，所以这里不用入司日期来计算
        cAgCalBase.setTempEnd(tEndDate);
        //获得代理人行政信息表中记录的考核开始日期
        String tPostDate = getAssessBeginDate(cAgentCode);
        int tAssessDate = 15;
        
        if (tPostDate.equals("")||tPostDate==null)
        {
            CError.buildErr(this, "未查询到代理人" + cAgentCode + "职级考核起始日期！");
            return false;
        }
        
        System.out.println("代理人" + cAgentCode + "职级考核起始日期为:" + tPostDate);
        //判断是否职级晋降人员
        tSql = "select AgentCode from LAAssess where agentCode = '" + cAgentCode + "'"
               + " and AgentGrade1 = '" + cGrade + "' and indexcalno<'"+mIndexCalNo+"'";
        tExeSQL = new ExeSQL();
        String tValue = tExeSQL.getOneValue(tSql);
        String tQuauterBeginDate = cAgCalBase.getQuauterBegin();
        //没有发生职级晋降人员的考核，首次考核
        if (tValue == null || tValue.equals(""))
        {
            //维持和晋升考核都做
            cAgCalBase.AssessMark = 2;
            cAgCalBase.setA1State("0"); //没有降级过;
            
            //判断人员入职日期是否在上个季度考核月的上个月15号之后，若是按4个月通算
            if(tPostDate.compareTo(tQuauterBeginDate)<0)
            {
            	String tEmpolDate =  PubFun.calDate(tQuauterBeginDate,-2,"M","");
            	String tMidDate = PubFun.calDate(tEmpolDate,14,"D","");
            	if(tPostDate.compareTo(tMidDate)>=0)
            	{
            		assessInv=4;
            		String tAssDate = PubFun.calDate(tQuauterBeginDate,-1,"M","");
                	cAgCalBase.setTempBegin(tAssDate);
                    cAgCalBase.setWageNoBegin(AgentPubFun.ConverttoYM(tAssDate));
            	}
            	else
            	{
            	
            	assessInv=3;
            	cAgCalBase.setTempBegin(cAgCalBase.getQuauterBegin());
                cAgCalBase.setWageNoBegin(AgentPubFun.ConverttoYM(cAgCalBase.getTempBegin()));
            	}
            }
            else
            {
            	int day = Integer.parseInt(tPostDate.substring(tPostDate.lastIndexOf("-")+1, tPostDate.length())) ;
            	System.out.println(tPostDate.substring(tPostDate.lastIndexOf("-")+1, tPostDate.length()));
            	if(tAssessDate<=day)
            	{
            		tPostDate = PubFun.calDate(tPostDate,1,"M","");
            	}
            	if(tPostDate.compareTo(tEndDate)<=0)
            	{
            		//计算时间间隔,起止期间隔月数,或者通过考核月数函数获取
//                  assessInv = calMonths(tPostDate, tEndDate);
              	assessInv=PubFun.calInterval(tPostDate,tEndDate,"M");
              	assessInv++;
            	}
            	else
            	{
            		cAgCalBase.AssessMark = 0;
                    return true;
            	}
            	//考核月数小于2时，当季不考核
            	if (assessInv < 2)
                {
                    System.out.println("当考核季不参加考核");
                    cAgCalBase.AssessMark = 0;
                    return true;
                }
            	String tempBgein = PubFun.calDate(tEndDate, (assessInv-1)*(-1), "M", "");
                cAgCalBase.setWageNoBegin(AgentPubFun.ConverttoYM(tempBgein));
                cAgCalBase.setTempBegin(tempBgein.substring(0,tempBgein.lastIndexOf("-"))+"-01");
            }
            
        }
        else //发生过晋降人员的考核
        {
            cAgCalBase.AssessMark = 2;
            cAgCalBase.setA1State("1");
            //设置降级人员标志位
            this.mDegradeFlag = "1";
            assessInv=3;
            //设置考核统计起期
            cAgCalBase.setTempBegin(cAgCalBase.getQuauterBegin());
            cAgCalBase.setWageNoBegin(AgentPubFun.ConverttoYM(cAgCalBase.getTempBegin()));
        }
        
       
        //计算统计起期与考核止期之间的间隔月数
        cAgCalBase.setWageNoEnd(AgentPubFun.ConverttoYM(tEndDate));
        this.mAssessIntv = String.valueOf(assessInv);
        System.out.println("考核月数为：" + assessInv);
        return true;
    }

    /**
     * 函数功能：设置 自然月 、自然季 、 自然年 、 年 的起止期
     */
    private boolean setBeginEnd()
    {
        //从数据库中查出时间区间
        LAStatSegmentSet tLAStatSegmentSet = new LAStatSegmentSet();
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
        tLAStatSegmentDB.setYearMonth(Integer.parseInt(this.mIndexCalNo));
        tLAStatSegmentSet = tLAStatSegmentDB.query();
        if (tLAStatSegmentDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError.buildErr(this,"查询时间区间出错！");
            return false;
        }
        int tCount = tLAStatSegmentSet.size();
        if (tCount < 1)
        {
            CError.buildErr(this,"所输年月有问题，无法查到时间区间!");
            return false;
        }
        LAStatSegmentSchema tLAStatSegmentSchema = new LAStatSegmentSchema();
        String tStatType = "";
        for (int i = 1; i <= tCount; i++)
        {
            tLAStatSegmentSchema = tLAStatSegmentSet.get(i);
            tStatType = tLAStatSegmentSchema.getStatType().trim();
            if (tStatType.equals("1"))
            {
                mCalIndex.tAgCalBase.setMonthMark("1");
                mCalIndex.tAgCalBase.setMonthBegin(tLAStatSegmentSchema.
                        getStartDate());
                System.out.println("_________________"+mCalIndex.tAgCalBase.getMonthBegin());
                mCalIndex.tAgCalBase.setMonthEnd(tLAStatSegmentSchema.
                                                 getEndDate());
                continue;
            }
            if (tStatType.equals("2") &&
                !mCalIndex.tAgCalBase.getQuauterMark().equals("1"))
            {
                mCalIndex.tAgCalBase.setQuauterMark("1");
                mCalIndex.tAgCalBase.setQuauterBegin(tLAStatSegmentSchema.
                        getStartDate());
                mCalIndex.tAgCalBase.setQuauterEnd(tLAStatSegmentSchema.
                        getEndDate());
                continue;
            }
            if (tStatType.equals("3") &&
                !mCalIndex.tAgCalBase.getHalfYearMark().equals("1"))
            {
                mCalIndex.tAgCalBase.setHalfYearMark("1");
                mCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema.
                        getStartDate());
                mCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema.
                        getEndDate());
                continue;
            }
            if (tStatType.equals("4") &&
                !mCalIndex.tAgCalBase.getYearMark().equals("1"))
            {
                mCalIndex.tAgCalBase.setYearMark("1");
                mCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.
                                                  getStartDate());
                mCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
        }
        return true;
    }

    private void addAgCalBase(Calculator cCal)
    {
        cCal.addBasicFactor("AgentCode", mCalIndex.tAgCalBase.getAgentCode());
        cCal.addBasicFactor("WageNo", mCalIndex.tAgCalBase.getWageNo());
        cCal.addBasicFactor("WageCode", mCalIndex.tAgCalBase.getWageCode());
        cCal.addBasicFactor("AgentGrade", mCalIndex.tAgCalBase.getAgentGrade());
        cCal.addBasicFactor("AgentGroup", mCalIndex.tAgCalBase.getAgentGroup());
        cCal.addBasicFactor("BranchCode", mCalIndex.tAgCalBase.getBranchCode());
        cCal.addBasicFactor("MonthBegin", mCalIndex.tAgCalBase.getMonthBegin());
        cCal.addBasicFactor("MonthEnd", mCalIndex.tAgCalBase.getMonthEnd());
        cCal.addBasicFactor("QuauterBegin",
                            mCalIndex.tAgCalBase.getQuauterBegin());
        cCal.addBasicFactor("QuauterEnd", mCalIndex.tAgCalBase.getQuauterEnd());
        cCal.addBasicFactor("HalfYearBegin",
                            mCalIndex.tAgCalBase.getHalfYearBegin());
        cCal.addBasicFactor("HalfYearEnd", mCalIndex.tAgCalBase.getHalfYearEnd());
        cCal.addBasicFactor("YearBegin", mCalIndex.tAgCalBase.getYearBegin());
        cCal.addBasicFactor("YearEnd", mCalIndex.tAgCalBase.getYearEnd());
        cCal.addBasicFactor("MonthMark", mCalIndex.tAgCalBase.getMonthMark());
        cCal.addBasicFactor("QuauterMark", mCalIndex.tAgCalBase.getQuauterMark());
        cCal.addBasicFactor("HalfYearMark",
                            mCalIndex.tAgCalBase.getHalfYearMark());
        cCal.addBasicFactor("YearMark", mCalIndex.tAgCalBase.getYearMark());
        cCal.addBasicFactor("AreaType", mCalIndex.tAgCalBase.getAreaType());
        cCal.addBasicFactor("TempBegin", mCalIndex.tAgCalBase.getTempBegin());
        cCal.addBasicFactor("TempEnd", mCalIndex.tAgCalBase.getTempEnd());
        cCal.addBasicFactor("WageNoBegin", mCalIndex.tAgCalBase.getWageNoBegin());
        cCal.addBasicFactor("WageNoEnd", mCalIndex.tAgCalBase.getWageNoEnd());
        cCal.addBasicFactor("Rate", String.valueOf(mCalIndex.tAgCalBase.getRate()));
        cCal.addBasicFactor("A1State",
                            String.valueOf(mCalIndex.tAgCalBase.getA1State()));
        cCal.addBasicFactor("AssessType", mCalIndex.tAgCalBase.getAssessType());
        cCal.addBasicFactor("LimitPeriod", mCalIndex.tAgCalBase.getLimitPeriod());
        cCal.addBasicFactor("DestAgentGrade",
                            mCalIndex.tAgCalBase.getDestAgentGrade());
        cCal.addBasicFactor("BranchAttr", mCalIndex.tAgCalBase.getZSGroupBranchAttr());
        cCal.addBasicFactor("BranchSeries", mCalIndex.tAgCalBase.getZSGroupBranchSeries());
        cCal.addBasicFactor("FirstAssessMark",
                            String.valueOf(mCalIndex.tAgCalBase.FirstAssessMark));
        System.out.println("考核开始前奏：" + mCalIndex.tAgCalBase.getAgentGrade());
        System.out.println("考核开始前奏：" + mCalIndex.tAgCalBase.getWageNoEnd());
    }

    /**
     * 功能：执行优先考核函数，例如：对A01,A02是否连续2个月挂零进行优先判断，
     * 如果满足条件，则直接对此人做清退处理，不用进行下面的考核
     * 返回值说明： 1 - 不清退
     *            2 - 清退
     *            0 - 处理过程中有错误
     */
    private int execFirstAssess(String cAgentGrade, String cAgentCode,
                                String cAssessCode)
    {
        //cAssessCode只要是该职级的考核代码即可，目的为得到表名和字段名
        String tReturn = "";
        LAFirstAssessDB tLAFirstAssessDB = new LAFirstAssessDB();
        tLAFirstAssessDB.setAgentGrade(cAgentGrade);
//        tLAFirstAssessDB.setAssessCode(cAssessCode);
        LAFirstAssessSet tLAFirstAssessSet = new LAFirstAssessSet();
        tLAFirstAssessSet = tLAFirstAssessDB.query();
        int tNum = tLAFirstAssessSet.size();
        LAFirstAssessSchema tLAFirstAssessSchema = new LAFirstAssessSchema();
        for (int i = 1; i <= tNum; i++)
        {
            tLAFirstAssessSchema = tLAFirstAssessSet.get(i).getSchema();
            //查询指标信息
            LAAssessIndexDB tLAAssessIndexDB = new LAAssessIndexDB();
            tLAAssessIndexDB.setIndexCode(tLAFirstAssessSchema.getAssessCode());
            if (!tLAAssessIndexDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAAssessIndexDB.mErrors);
                CError.buildErr(this,"优先考核对应的指标信息出错！");
                return 0;
            }
            mCalIndex.setAssessIndex(tLAAssessIndexDB.getSchema());
            mCalIndex.setAgentCode(cAgentCode);
            System.out.println("优先考核代理人代码：" + cAgentCode);
            mCalIndex.setIndexCalNo(mIndexCalNo);
            mCalIndex.setOperator(mOperator);
            mCalIndex.setIndexCalPrpty("01");
            mCalIndex.tAgCalBase.setAgentCode(cAgentCode);
            //修改程序-ll：此处在设置起期的时候用的是考核起期
            mCalIndex.tAgCalBase.setTempBegin(getAssessBeginDate(cAgentCode));
            mCalIndex.tAgCalBase.setTempEnd(mCalIndex.tAgCalBase.getMonthEnd());
            mCalIndex.tAgCalBase.setAssessType("01"); //维持考核
            tReturn = mCalIndex.Calculate();
            if (tReturn.equals("0"))
            {//返回为0，不满足优先考核处理的条件，即不清退
                return 1;
            }
        }
        //插入考核信息表
        if (tReturn.equals(tLAFirstAssessSchema.getDefaultValue()))
        {
            if (!insertAssess(mIndexCalNo, cAgentCode, mAgentGrade, cAssessCode,
                              "A00","01"))
            {
                System.out.println("插入考评信息表出错！！");
                return 0;
            }
            return 2; //清退
        }
        return 1; //不清退
    }


    /**
     * 计算两个日期之间的月数，16日之后的不算作一个月
     */
    public int calMonths(String cBeginDate, String cEndDate)
    {
        FDate fDate = new FDate();
        int monthIntv = 0;
        int dayInv = 0;
        String tSql = "";
        try
        {
            ExeSQL tExeSQL = new ExeSQL();
            //考核月计算时的临界日期
            tSql =
                    "select trim(code2) from ldcoderela where relatype = 'assessday'";
            String tValue = tExeSQL.getOneValue(tSql);
            monthIntv = PubFun.calInterval(cBeginDate,
                                           fDate.getString(PubFun.calDate(fDate.
                    getDate(cEndDate), 1, "D", null)), "M");
            dayInv = PubFun.calInterval(cBeginDate,
                                        cBeginDate.substring(0,
                    cBeginDate.lastIndexOf("-")) + "-" + tValue, "D");
            if (dayInv >= 0 &&
                !cBeginDate.substring(cBeginDate.length() - 2).equals("01")) //若为15日以前入司，则到考核期，入司该月也算作一个月
            {
                monthIntv = monthIntv + 1;
            }
        }
        catch (Exception ex)
        {
            CError.buildErr(this,"计算起期" + cBeginDate + "与止期" + cEndDate +
                                  "的月数出错!");
            return -100;
        }
        System.out.println("monthIntv:"+monthIntv);
        return monthIntv;
    }

    public String toWageNoBegin(String cBeginDate)
    {
        String cWageNo = "";
        try
        {
            //考核月计算时的临界日期
            String tValue = AgentPubFun.getAssessDay();
            //若为15日以前入司，则到考核期，入司该月也算作一个月
            //若为15日以后入司的，则考核年月向后推一个月
            String tDay = cBeginDate.substring(cBeginDate.length() - 2);
            if (tDay.compareTo(tValue) > 0)
            {
           cWageNo = AgentPubFun.ConverttoYM(PubFun.calDate(cBeginDate, 1, "M", null));
           //cWageNo = AgentPubFun.ConverttoYM(cBeginDate);
            }
            else
            {
                cWageNo = AgentPubFun.ConverttoYM(cBeginDate);
            }
        }
        catch (Exception ex)
        {
            CError.buildErr(this, "设置考核起期年月值出错!");
            ex.printStackTrace();
        }
        return cWageNo;
    }

    /**
     * 将本次考核的职级人数存于LAAssessMain表中
     * @param cAgentGrade String
     * @param cMaxCount int
     * @return boolean
     */
    private boolean addLAAssessMain(String cAgentGrade,int cFinalCount)
    {
        //准备数据
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
        this.mLAAssessMainSchema = new LAAssessMainSchema();
        tLAAssessMainDB.setIndexCalNo(mIndexCalNo);
        tLAAssessMainDB.setManageCom(mManageCom);
        tLAAssessMainDB.setAgentGrade(cAgentGrade);
        if (tLAAssessMainDB.getInfo())
        {
            this.mLAAssessMainSchema = tLAAssessMainDB.getSchema();
        }
        else
        {
            this.mLAAssessMainSchema.setIndexCalNo(mIndexCalNo);
            this.mLAAssessMainSchema.setAgentGrade(cAgentGrade);
            this.mLAAssessMainSchema.setManageCom(mManageCom);
            this.mLAAssessMainSchema.setBranchType(mBranchType);
            this.mLAAssessMainSchema.setBranchType2(mBranchType2);
            this.mLAAssessMainSchema.setState("0");
            this.mLAAssessMainSchema.setBranchAttr(mCalIndex.tAgCalBase.
                    getZSGroupBranchAttr());
            this.mLAAssessMainSchema.setAssessType("00");
            this.mLAAssessMainSchema.setAssessCount(cFinalCount);
            this.mLAAssessMainSchema.setMakeDate(currentDate);
            this.mLAAssessMainSchema.setMakeTime(currentTime);
        }
        this.mLAAssessMainSchema.setModifyDate(currentDate);
        this.mLAAssessMainSchema.setModifyTime(currentTime);
        this.mLAAssessMainSchema.setOperator(mOperator);
        this.mLAAssessMainSet.add(mLAAssessMainSchema);
        return true;
    }

   /**
     * 取出某个人的入司日期
     * 返回值说明：
     *  0-查询出错；tEmpDate-入司日期；
     */
    private String getEmpDate(String tAgentCode)
    {
        //查找laagent表中信息,获得入司日期
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);

        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError.buildErr(this,"查询代理人" + tAgentCode + "入司日期时出错！");
            return "0";
        }
        String tEmpDate = tLAAgentDB.getEmployDate();
        return tEmpDate;
    }



    /**
     * 查询代理人考核季内的考核月数
     * @param cAgentCode String
     * @param cWageNo String
     * @return int  返回值说明：考核月数
     */
    private int getAssessMonthNum(String cAgentCode,String cWageNo)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String sql = "";
        int monthNum = 0;
        try
        {
            sql = "select AssessMonthNum('" + cAgentCode + "','" + cWageNo +
                  "') from ldsysvar where sysvar='onerow' ";
            monthNum = Integer.parseInt(tExeSQL.getOneValue(sql));
        }
        catch (Exception ex)
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError.buildErr(this, "查询代理人" + cAgentCode + "考核月数时出错！");
        }
        return monthNum;
    }

    /**
     * 查询参加考核的职级
     * @param cAgentSeries String   职级系列
     * @param cBranchType String    展业类型
     * @param cBranchType2 String   子渠道
     * @param cMonth String         考核月份
     * @return String[]             参加考核的职级
     */
    private String[] getGradesBySeries(String cAgentSeries, String cBranchType,
                                       String cBranchType2,
                                       String cMonth)
    {
        String gradesArr[] = null;
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String sql = "";

        if (!cAgentSeries.equals("") && cAgentSeries.equals("0"))
        {
            sql = "Select gradecode from LAAgentGrade where "
                  + " gradeproperty2 = '" + cAgentSeries + "'"
                  + " and BranchType = '" + cBranchType + "'"
                  + " and BranchType2 = '" + cBranchType2 +"'";
        }
        else if (!cAgentSeries.equals("") && cAgentSeries.equals("1"))
        {
            sql = "Select gradecode from LAAgentGrade where "
                  + " gradeproperty2 >= '" + cAgentSeries + "'"
                  + " and BranchType = '" + cBranchType + "'"
                  + " and BranchType2 = '" + cBranchType2 +"'";
        }
        try
        {
            System.out.println("查询参加考核的职级SQL:" + sql);
            tSSRS = tExeSQL.execSQL(sql);
            int rows = tSSRS.getMaxRow();
            if (rows < 1)
            {
                CError.buildErr(this, "未查到相应职级系列的职级！");
            }
            gradesArr = new String[rows];
            for (int i = 0; i < rows; i++)
            {
                gradesArr[i] = tSSRS.GetText(i + 1, 1);
            }
        }
        catch (Exception ex)
        {
            CError.buildErr(this, "根据职级系列查询职级出错！");
        }
        return gradesArr;
    }

    /**
      * 取得代理人行政信息表中职级考核开始的日期
      */
     public static String getAssessBeginDate(String cAgentCode)
     {
         String tAStartDate = "";
         LATreeDB tLATreeDB = new LATreeDB();
         tLATreeDB.setAgentCode(cAgentCode);
         try
         {
             if (!tLATreeDB.getInfo())
             {
                 return "";
             }
             tAStartDate = tLATreeDB.getAstartDate();
         }
         catch (Exception ex)
         {
             ex.printStackTrace();
         }
         if (tAStartDate == null || tAStartDate.equals(""))
         {
             return "";
         }
         return tAStartDate;
     }
     private boolean dealBaseCode(String tAgentCode)
     {
         LAAgentDB tLAAgentDB = new LAAgentDB();
         LAAgentSet tLAAgentSet = new LAAgentSet();

         String tSql = "select * from LAAgent where   agentcode='"+tAgentCode+"' ";
         tLAAgentSet = tLAAgentDB.executeQuery(tSql);
         System.out.println(tSql);
         VData tInputData = new VData();
         TransferData tTransferData = new TransferData();
         tTransferData.setNameAndValue("ManageCom", mManageCom);
         tTransferData.setNameAndValue("IndexCalNo", mIndexCalNo);
         tTransferData.setNameAndValue("GlobalInput", mGlobalInput);
         tTransferData.setNameAndValue("BranchType", mBranchType);
         tTransferData.setNameAndValue("BranchType2", mBranchType2);
         tTransferData.setNameAndValue("LAAgentSet", tLAAgentSet);
         tInputData.add(tTransferData);
         AgentMonthCal tAgentMonthCal = new AgentMonthCal();
//    String countSQL = "select count(*) from laindexinfo where managecom like '" +
//        mManageCom + "%' and indexcalno='" + mIndexCalNo +
//        "' and indextype='00' and agentcode in (select agentcode from laagent where managecom like '" +
//        mManageCom + "%'  and branchtype='" + mBranchType + "' )";
//    System.out.println(countSQL);
//    ExeSQL tExeSQL = new ExeSQL();
//    int count = Integer.parseInt(tExeSQL.getOneValue(countSQL));
//    if (count == 0)

         if (!tAgentMonthCal.submitData(tInputData, "")) { //佣金计算前的基础指标
             this.mErrors.copyAllErrors(tAgentMonthCal.mErrors);
             return false;
         }
         return true;
     }
     //检查是否正在计算，不能同时多用户计算
     private boolean checkState()
     {
         String currentDate = PubFun.getCurrentDate();
         String currentTime = PubFun.getCurrentTime();
         String tState = "";
         LAAssessHistorySet tLAAssessHistorySet = new LAAssessHistorySet();
         LAAssessHistoryDB tLAAssessHistoryDB = new LAAssessHistoryDB();
         tLAAssessHistoryDB.setIndexCalNo(mIndexCalNo);
         tLAAssessHistoryDB.setManageCom(mStartManageCom);
         tLAAssessHistoryDB.setBranchType(mBranchType);
         tLAAssessHistoryDB.setBranchType2(mBranchType2);
         tLAAssessHistoryDB.setAgentGrade("000");
         tLAAssessHistorySet = tLAAssessHistoryDB.query();

         if (tLAAssessHistorySet.size() > 0)
         {
             tState = tLAAssessHistorySet.get(1).getState();
             if (tState.equals("01"))
             {
                 CError.buildErr(this, "本月考核正在进行计算，不能并发计算！");
                 return false;
             }
             else if (tState.equals("00"))
             {
                 tLAAssessHistoryDB=tLAAssessHistorySet.get(1).getDB() ;
                 tLAAssessHistoryDB.setModifyDate(currentDate);
                 tLAAssessHistoryDB.setModifyTime(currentTime);
                 tLAAssessHistoryDB.setState("01");  //设置为不可并发计算
                 if (!tLAAssessHistoryDB.update())
                 {
                     CError.buildErr(this, "增加机构"+mManageCom+"考核日志失败!");
                     return false;
                 }
             }
         }
         else
         {
             tLAAssessHistoryDB.setState("01");

             tLAAssessHistoryDB.setMakeDate(currentDate);
             tLAAssessHistoryDB.setMakeTime(currentTime);
             tLAAssessHistoryDB.setModifyDate(currentDate);
             tLAAssessHistoryDB.setModifyTime(currentTime);
             tLAAssessHistoryDB.setOperator(mOperator);
             if (!tLAAssessHistoryDB.insert())
             {
                 CError.buildErr(this, "增加机构"+mManageCom+"考核日志失败!");
                 return false;
             }
         }
         return true;
     }

     // 个险考核计算结束，返回到可计算状态，更新状态为'00'，表示可以计算
     private boolean returnState()
     {
         String currentDate = PubFun.getCurrentDate();
         String currentTime = PubFun.getCurrentTime();
         String tSQL1 = "update LAAssessHistory set state = '00' ,modifydate='"+currentDate+
                        "',modifytime='"+currentTime+"'  where "
                        + " IndexCalNo = '" + mIndexCalNo +
                        "' and ManageCom like '" + mStartManageCom
                        + "%' and BranchType = '" + mBranchType
                        + "' and BranchType2 ='" + mBranchType2
                        + "' and AgentGrade='000'";
         System.out.println("最后更新LAAssessHistory表中state的SQL: " + tSQL1);
         ExeSQL tExeSQL = new ExeSQL();
         tExeSQL.execUpdateSQL(tSQL1);
         if (tExeSQL.mErrors.needDealError())
         {
             // @@错误处理
             this.mErrors.copyAllErrors(tExeSQL.mErrors);
             CError.buildErr(this, "回滚更新LAssessHistory表记录时失败!");
             return false;
         }
         return true;
     }

    public static void main(String[] args)
    {
        HDAssessBL tCal = new HDAssessBL();
//       String[] str = tCal.getGradesBySeries("0", "1","04");
//       for (int i = 0; i < str.length; i++)
//       {
//           System.out.println(i + ":" + str[i]);
//       }

        String tManageCom = "8611";
//       String tAgentGrade = "A02";
        String tAgentSeries = "1";
        String tYear = "2007";
        String tMonth = "03";
        String tBranchType = "1";
        String tBranchType2 = "01";
        String tCalFlag= "1";

        VData cInputData;
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        cInputData = new VData();
        cInputData.add(tGlobalInput);
        cInputData.add(tManageCom);
        cInputData.add(tAgentSeries);
        cInputData.add(tYear);
        cInputData.add(tMonth);
        cInputData.add(tBranchType);
        cInputData.add(tBranchType2);
        cInputData.add(tCalFlag);

        try
        {
            boolean b = tCal.submitData(cInputData, "INSERT||MAIN");
            if (b)
            {
                System.out.println("OK");
            }
            else
            {
                System.out.println("fail");
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    /*
     * 获取机构的区域类型
     */
    public  String getAreaType(String cManageCom)
    {
           //AreaType
           String tSql = "Select trim(assessflag) from LABankIndexRadix where branchtype = '5' and branchtype2 = '01' and managecom = '"+ cManageCom + "'";
           ExeSQL tExeSQL = new ExeSQL();
           String tAreaType = tExeSQL.getOneValue(tSql);
           if (tExeSQL.mErrors.needDealError()){
        	   tAreaType="";
           }
           if (tAreaType == null || tAreaType.equals("")){
        	   tAreaType="";
           }
           return tAreaType;
   }
}
