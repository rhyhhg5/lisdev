package com.sinosoft.lis.agentassess;


import java.sql.Connection;

import com.sinosoft.lis.vdb.LAAssessAccessoryBDBSet;
import com.sinosoft.lis.vdb.LAAssessAccessoryDBSet;
import com.sinosoft.lis.vschema.LAAssessAccessoryBSet;
import com.sinosoft.lis.vschema.LAAssessAccessorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;


public class LAEmployeeAssessAdjBLS
{
    public CErrors mErrors = new CErrors();
    private LAAssessAccessorySet mLAAssessAccessoryDelSet = new
            LAAssessAccessorySet();
    private LAAssessAccessorySet mLAAssessAccessoryInsSet = new
            LAAssessAccessorySet();
    private LAAssessAccessoryBSet mLAAssessAccessoryBSet = new
            LAAssessAccessoryBSet();

    public LAEmployeeAssessAdjBLS()
    {
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
//    if (!startSave())
//      return false;
        if (!this.startSave())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        try
        {
            mLAAssessAccessoryBSet = (LAAssessAccessoryBSet) cInputData.get(0);
            mLAAssessAccessoryInsSet = (LAAssessAccessorySet) cInputData.get(1);
            mLAAssessAccessoryDelSet = (LAAssessAccessorySet) cInputData.get(2);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    private boolean startSave()
    {
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBLS";
            tError.functionName = "startSave";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LAAssessAccessoryBDBSet tLAAssessAccessoryBDBSet = new
                    LAAssessAccessoryBDBSet(conn);
            tLAAssessAccessoryBDBSet.add(mLAAssessAccessoryBSet);
            if (!tLAAssessAccessoryBDBSet.insert())
            {
                this.mErrors.copyAllErrors(tLAAssessAccessoryBDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeAssessAdjBLS";
                tError.functionName = "startSave";
                tError.errorMessage = tLAAssessAccessoryBDBSet.mErrors.
                                      getFirstError();
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAAssessAccessoryDBSet tLAAssessAccessoryDBSet = new
                    LAAssessAccessoryDBSet(conn);
            tLAAssessAccessoryDBSet.add(mLAAssessAccessoryDelSet);
            if (!tLAAssessAccessoryDBSet.delete())
            {
                this.mErrors.copyAllErrors(tLAAssessAccessoryDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeAssessAdjBLS";
                tError.functionName = "startSave";
                tError.errorMessage = tLAAssessAccessoryDBSet.mErrors.
                                      getFirstError();
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            tLAAssessAccessoryDBSet = new LAAssessAccessoryDBSet(conn);
            tLAAssessAccessoryDBSet.add(mLAAssessAccessoryInsSet);
            if (!tLAAssessAccessoryDBSet.insert())
            {
                this.mErrors.copyAllErrors(tLAAssessAccessoryDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeAssessAdjBLS";
                tError.functionName = "startSave";
                tError.errorMessage = tLAAssessAccessoryDBSet.mErrors.
                                      getFirstError();
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
            return true;
        }
        catch (Exception ex)
        {
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAssessAdjBLS";
            tError.functionName = "startSave";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
    }

    public static void main(String[] args)
    {
        LAEmployeeAssessAdjBLS LAEmployeeAssessAdjBLS1 = new
                LAEmployeeAssessAdjBLS();
    }
}
