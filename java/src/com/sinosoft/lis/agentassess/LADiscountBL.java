/*
 * <p>ClassName: ALARewardPunishBL </p>
 * <p>Description: ALARewardPunishBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentassess;

//import com.sinosoft.lis.db.LAAgentDB;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vschema.*;
//import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.*;
//import com.sinosoft.lis.vschema.LAAgentSet;
//import java.util.ArrayList;

public class LADiscountBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMap = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate="";

    /** 业务处理相关变量 */
    //private LADiscountSchema mLADiscountSchema = new LADiscountSchema();

    //private LADiscountSchema tLADiscountSchema = new LADiscountSchema();

    private LADiscountSet mLADiscountSet = new LADiscountSet();
    private Reflections ref = new Reflections();
   // private LADiscountSet tLADiscountSet = new LADiscountSet();
  //  private LADiscountSet mmLADiscountSet = new LADiscountSet();//待删除记录
    private String RiskCodename = "";

    public LADiscountBL() {}

    public static void main(String[] args)
    { }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将操作数据拷贝到本类中
        this.mOperate = cOperate;

        // 得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {   System.out.println("dddddddddddddddddd");
            return false;
        }

        // 进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADiscountBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LADiscountBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        PubSubmit t = new PubSubmit();
        t.submitData(mInputData, "");

        // 准备往后台的数据
        if(!prepareOutputData()) {
                return false;
        }

        mInputData = null;
        return true;
    }



    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        //System.out.println(currentDate);



        if (this.mOperate.equals("INSERT"))
        {
            System.out.println("aaaaaaaaaaaaaaaaaaaa  begin insert");
            LADiscountSet tLADiscountSet = new
                    LADiscountSet();
            for (int i = 1; i <= mLADiscountSet.size(); i++) {
                LADiscountSchema tLADiscountSchemanew = new
                        LADiscountSchema();
                tLADiscountSchemanew = mLADiscountSet.get(i);
                String tRiskCode = tLADiscountSchemanew.getRiskCode();
                String tManageCom = tLADiscountSchemanew.getManageCom();
                String tInsureYear = tLADiscountSchemanew.getInsureYear();
                double tRate = tLADiscountSchemanew.getRate();

                LADiscountSet tLADiscountoldSet = new
                    LADiscountSet();
                LADiscountDB tLADiscountDB = new LADiscountDB();
                tLADiscountDB.setBranchType("3");
                tLADiscountDB.setBranchType2("01");
                tLADiscountDB.setRiskCode(tRiskCode);
                tLADiscountDB.setManageCom(tManageCom);
                tLADiscountDB.setInsureYear(tInsureYear);
                //tLADiscountDB.setRate(tRate);
                tLADiscountoldSet=tLADiscountDB.query();
                if(tLADiscountoldSet!=null && tLADiscountoldSet.size()>0)
                {
                    BuildError("dealData","机构"+tManageCom+"的险种"+tRiskCode+"在此保险期间"+tInsureYear+"的折标系数已经存在，不能重复录入！");
                    return false;
                }
                tLADiscountSchemanew.setOperator(mGlobalInput.
                        Operator);
                //tLADiscountSchemanew.setManageCom(mGlobalInput.ManageCom);
                tLADiscountSchemanew.setMakeDate(currentDate);
                tLADiscountSchemanew.setMakeTime(currentTime);
                tLADiscountSchemanew.setModifyDate(currentDate);
                tLADiscountSchemanew.setModifyTime(currentTime);
                tLADiscountSet.add(tLADiscountSchemanew);

            }
            mMap.put(tLADiscountSet, "INSERT");
            mInputData.add(mMap);
        }

        if (mOperate.equals("UPDATE") || mOperate.equals("DELETE")) {

                    String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);  //总公司需要备份两条，最大值和最小值
                 //   String tEdorNoMax = PubFun1.CreateMaxNo("EdorNo", 20);
                    System.out.println("Begin LadiscountBL.dealData.........1" +
                                       mLADiscountSet.size());
                    LADiscountSet tLADiscountSet = new LADiscountSet();
                    LADiscountBSet tLADiscountBSet = new LADiscountBSet();
                    for (int i = 1; i <= mLADiscountSet.size(); i++) {
                        System.out.println(
                                "Begin LadiscountBL.dealData.........2");
                        LADiscountSchema tLADiscountSchemaold = new
                                LADiscountSchema();
                        LADiscountSchema tLADiscountSchemanew = new
                                LADiscountSchema();
                        LADiscountDB tLADiscountDB = new LADiscountDB();
                        tLADiscountDB.setIdx(mLADiscountSet.get(i).getIdx());

                             System.out.println("mistakeooooooooooooooooooooooo"+mLADiscountSet.get(i).
                                                       getRiskCode());
                        tLADiscountSchemaold = tLADiscountDB.query().get(1);
                        tLADiscountSchemanew = mLADiscountSet.get(i);
                         System.out.println("tiaoshi              "+tLADiscountSchemaold.getRiskCode());
                         System.out.println("tiaoshi              "+tLADiscountSchemanew.getRiskCode());
                         //System.out.println("tiaoshi              "+tLADiscountSchemaold.getRiskCode());
                        if((!(tLADiscountSchemaold.getRiskCode()).equals(tLADiscountSchemanew.getRiskCode()))
                         ||(!(tLADiscountSchemaold.getManageCom()).equals(tLADiscountSchemanew.getManageCom()))
                         ||(!(tLADiscountSchemaold.getInsureYear()).equals(tLADiscountSchemanew.getInsureYear()))
                          )
                          {
                          BuildError("dealData","机构,险种,保险期间不能修改！");
                           return false;
                       }
                       else {

                        System.out.println("right!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

                        tLADiscountSchemanew.setOperator(mGlobalInput.Operator);
                        tLADiscountSchemanew.setMakeDate(tLADiscountSchemaold.
                                getMakeDate());
                        tLADiscountSchemanew.setMakeTime(tLADiscountSchemaold.
                                getMakeTime());
                        tLADiscountSchemanew.setModifyDate(currentDate);
                        tLADiscountSchemanew.setModifyTime(currentTime);

                        tLADiscountSet.add(tLADiscountSchemanew);

                        LADiscountBSchema tLADiscountBSchema = new
                                LADiscountBSchema();
                        ref.transFields(tLADiscountBSchema,
                                        tLADiscountSchemaold);
                        //获取最大的ID号
                        String OldIdx=String.valueOf(tLADiscountSchemaold.getIdx());
                        ExeSQL tExe = new ExeSQL();
                        String tSql ="select max(idx) from ladiscountb where edorno='"
                                     +OldIdx+"'  order by 1 desc ";//原来的表的idx在B表中的edorno，报表中一个edorno可对应多条idx
                        String strIdx = "";
                        int tMaxIdx = 0;
                        strIdx = tExe.getOneValue(tSql);
                        if (strIdx == null || strIdx.trim().equals("")) {
                          tMaxIdx = 0;
                        }
                        else {
                          tMaxIdx = Integer.parseInt(strIdx);
                          System.out.println(tMaxIdx);
                        }
                        tMaxIdx++;
                        tLADiscountBSchema.setIdx(tMaxIdx);
                        tLADiscountBSchema.setEdorNo(tLADiscountSchemaold.getIdx()+"");

                        if (mOperate.equals("UPDATE"))
                        {
                            System.out.println("begin update aaaaaaaaaaaaaaaaaaaaaaaaaa");
                            tLADiscountBSchema.setEdorType("11");

                        }
                        else
                        {
                            tLADiscountBSchema.setEdorType("22");

                        }
                        mMap.put(tLADiscountSet, mOperate);
                        tLADiscountBSet.add(tLADiscountBSchema);

                       }
//                      String sql = "select riskcode,managecom,insureyear from ladiscount "
//                                   + "where riskcode='"+tLADiscountSchemanew.getRiskCode()+"' "
//                                   + "and managecom='"+tLADiscountSchemanew.getManageCom()+"' "
//                                   + "and insureyear='"+tLADiscountSchemanew.getInsureYear()+"' "
//                                   + "and discounttype='01'";
//                      System.out.println(sql);
//                        ExeSQL tExeSQL = new ExeSQL();
//                        SSRS tSSRS = new SSRS();
//                        tSSRS = tExeSQL.execSQL(sql);
//                        if(tSSRS.getMaxRow()>0){
//                          String SQL = "";
//                                SQL  ="delete from ladiscount "
//                                  + "where riskcode='"+tLADiscountSchemanew.getRiskCode()+"' "
//                                   + "and managecom='"+tLADiscountSchemanew.getManageCom()+"' "
//                                   + "and insureyear='"+tLADiscountSchemanew.getInsureYear()+"' "
//                                   + "and discounttype='01'";
//                            mMap.put(SQL,"DELETE");
//                        }
//
//                        LADiscountBSchema tLADiscountBSchema = new
//                                LADiscountBSchema();
//                        ref.transFields(tLADiscountBSchema,
//                                        tLADiscountSchemaold);
//                        //获取最大的ID号
//                        String OldIdx=String.valueOf(tLADiscountSchemaold.getIdx());
//                        ExeSQL tExe = new ExeSQL();
//                        String tSql ="select max(idx) from ladiscountb where edorno='"
//                                     +OldIdx+"'  order by 1 desc ";//原来的表的idx在B表中的edorno，报表中一个edorno可对应多条idx
//                        String strIdx = "";
//                        int tMaxIdx = 0;
//                        strIdx = tExe.getOneValue(tSql);
//                        if (strIdx == null || strIdx.trim().equals("")) {
//                          tMaxIdx = 0;
//                        }
//                        else {
//                          tMaxIdx = Integer.parseInt(strIdx);
//                          System.out.println(tMaxIdx);
//                        }
//                        tMaxIdx++;
//                        tLADiscountBSchema.setIdx(tMaxIdx);
//                        tLADiscountBSchema.setEdorNo(tLADiscountSchemaold.getIdx()+"");
//
//                        if (mOperate.equals("UPDATE"))
//                        {
//                            System.out.println("begin update aaaaaaaaaaaaaaaaaaaaaaaaaa");
//                            tLADiscountBSchema.setEdorType("11");
//                            mMap.put(tLADiscountSet, mOperate);
//                        }
//                        else
//                        {
//                            tLADiscountBSchema.setEdorType("22");
//                            mMap.put(tLADiscountSet, mOperate);
//                        }
//                        tLADiscountBSet.add(tLADiscountBSchema);
                    }



                    mMap.put(tLADiscountBSet, "INSERT");
                    mInputData.add(mMap);
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("oooooooooooooooo");

         // RiskCodename = (String) cInputData.get(0);
          this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput",
                                   0));

          this.mLADiscountSet.set((LADiscountSet) cInputData.
               getObjectByObjectName(
                       "LADiscountSet",
                       0));



        if (mGlobalInput == null || RiskCodename == null) {
            CError tError = new CError();
            tError.moduleName = "LADiscountBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传入参数失败!";
            this.mErrors.addOneError(tError);
            return false;

        }
        return true;
    }

    private void BuildError(String tFun,String tMess)
    {
        CError tError = new CError();
        tError.moduleName = "LADiscountBL";
        tError.functionName = tFun;
        tError.errorMessage = tMess;
        this.mErrors.addOneError(tError);

 }

    /**
     * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
     */
//    private boolean submitquery() {
//        this.mResult.clear();
//        System.out.println("Start LADiscountBLQuery Submit...");
//        LADiscountDB tLADiscountDB = new LADiscountDB();
//        tLADiscountDB.setSchema(this.mLADiscountSchema);
//        this.mLADiscountSet = tLADiscountDB.query();
//        this.mResult.add(this.mLADiscountSet);
//        System.out.println("End LADiscountBLQuery Submit...");
//        // 如果有需要处理的错误，则返回
//        if (tLADiscountDB.mErrors.needDealError()) {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tLADiscountDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "LADiscountBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "数据提交失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
//        mInputData = null;
//        return true;
//    }

    private boolean prepareOutputData() {
        try {
            System.out.println(
          "Begin LADiscountBL.prepareOutputData.........");
          mInputData.clear();
          //mInputData.add(this.RiskCodename);
          mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADiscountBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LADiscountBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
