package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 中英团险考核程序-机构处理</p>
 * <p>Description: 中英团险考核程序-机构处理</p>
 * <p>Copyright: Copyright (c) sinosoft 2004</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public class ACGrpGrdAssessComBLS
{
    //全局变量
    protected CErrors mErrors = new CErrors(); //错误处理类
    protected GlobalInput mGlobalInput = new GlobalInput(); //保存传入参数
    /** 数据操作字符串 */
    private String mOperate;
    /**存放考核结果容器*/
    private LAAssessMainSet mLAAssessMainSet = new LAAssessMainSet();

    private String mManageCom; //管理机构
    private String mIndexCalNo; //考核指标计算编码

    public ACGrpGrdAssessComBLS()
    {
    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        this.mOperate = cOperate;
        //1 - 获得输入数据
        if (!getInputData(cInputData))
        {
            return false;
        }

        System.out.println("Start ACGrpGrdAssessComBLS Submit...");
        if (cOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLAAssessMain();
        }
        else if (cOperate.equals("DELETE||MAIN"))
        {
            tReturn = DeleteAssess();
        }

        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End ACGrpGrdAssessComBLS Submit...");
        return tReturn;
    }

    /**
     * DeleteAssess
     * 删除考核结果
     * @return boolean
     */
    private boolean DeleteAssess()
    {
        boolean tReturn = false;
        System.out.println("Start ACGrpGrdAssessComBLS Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "MSGrpStandAssessBLS";
            tError.functionName = "saveLAAssessMain";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            //删除LAAssessAccessory中的数据
            LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
            tLAAssessAccessoryDB.setIndexCalNo(mIndexCalNo);
            tLAAssessAccessoryDB.setManageCom(mManageCom);
            LAAssessAccessorySet tLAAssessAccessorySet = new
                    LAAssessAccessorySet();
            tLAAssessAccessorySet = tLAAssessAccessoryDB.query();
            LAAssessAccessoryDBSet tLAAssessAccessoryDBSet = new
                    LAAssessAccessoryDBSet(conn);
            tLAAssessAccessoryDBSet.set(tLAAssessAccessorySet);
            if (!tLAAssessAccessoryDBSet.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAssessAccessoryDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ACGrpGrdAssessComBLS";
                tError.functionName = "DeleteAssess";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            //删除LAAssessMain中的数据
            LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
            tLAAssessMainDB.setIndexCalNo(mIndexCalNo);
            tLAAssessMainDB.setManageCom(mManageCom);
            LAAssessMainSet tLAAssessMainSet = new
                                               LAAssessMainSet();
            tLAAssessMainSet = tLAAssessMainDB.query();
            LAAssessMainDBSet tLAAssessMainDBSet = new LAAssessMainDBSet(conn);
            tLAAssessMainDBSet.set(tLAAssessMainSet);
            if (!tLAAssessMainDBSet.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAssessMainDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ACGrpGrdAssessComBLS";
                tError.functionName = "DeleteAssess";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            //删除LAIndexInfo中的数据
            LAIndexInfoDB tLAIndexInfoDB = new LAIndexInfoDB();
            tLAIndexInfoDB.setIndexCalNo(mIndexCalNo);
            tLAIndexInfoDB.setManageCom(mManageCom);
            LAIndexInfoSet tLAIndexInfoSet = new
                                             LAIndexInfoSet();
            tLAIndexInfoSet = tLAIndexInfoDB.query();
            LAIndexInfoDBSet tLAIndexInfoDBSet = new LAIndexInfoDBSet(conn);
            tLAIndexInfoDBSet.set(tLAIndexInfoSet);
            if (!tLAIndexInfoDBSet.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAIndexInfoDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ACGrpGrdAssessComBLS";
                tError.functionName = "DeleteAssess";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
            tReturn = true;
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessComBLS";
            tError.functionName = "saveLAAssessMain";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 获得输入数据
     */
    public boolean getInputData(VData cInputData)
    {
        //获得前台传过来的数据
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            this.mLAAssessMainSet = (LAAssessMainSet) cInputData.getObject(1);
        }
        else if (this.mOperate.equals("DELETE||MAIN"))
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            this.mIndexCalNo = (String) cInputData.getObject(1);
            this.mManageCom = (String) cInputData.getObject(2);
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAssessMain()
    {
        boolean tReturn = false;
        System.out.println("Start ACGrpGrdAssessComBLS Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "MSGrpStandAssessBLS";
            tError.functionName = "saveLAAssessMain";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 开始插入LAAssessMain表...");
            LAAssessMainDBSet tLAAssessMainDBSet = new LAAssessMainDBSet(conn);
            tLAAssessMainDBSet.set(mLAAssessMainSet);
            if (!tLAAssessMainDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAssessMainDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ACGrpGrdAssessComBLS";
                tError.functionName = "saveLAAssessMain";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
            tReturn = true;
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessComBLS";
            tError.functionName = "saveLAAssessMain";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

}
