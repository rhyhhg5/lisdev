package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.agentcalculate.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 人员考核指标计算流程类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class AgentCalExaYear {
    public AgentCalExaYear() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors(); //错误处理类
    /** 数据操作字符串 */
    private String mOperate = ""; //操作符
    private String mIndexCalNo = ""; //指标计算编码
    private String mManageCom = ""; //管理机构
    private String mYear = ""; //考核年
    private String mMonth = ""; //考核月
    private String mBranchType = ""; //展业类型
    private String mBranchType2 = ""; //渠道
    private String mGradeSeries = ""; //职级系列
    private String mAreaType = ""; //地区类型
    private boolean mSuccFlag = false; //考核计算成功标记
    //核心计算类
    private CalIndex tCalIndex = new CalIndex(); //指标计算类
    private Calculator tCal = new Calculator();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput(); //保存传入参数
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet(); //参加考核的机构
    private LAAgentGradeSet mLAAgentGradeSet = new LAAgentGradeSet(); //各种职级信息
    private LAAssessSet mLAAssessSet = new LAAssessSet(); //代理人考评信息
    private String mWithoutAgentGrade = "'A00','D00','B21'"; //不用参加考核的职级
    private int mInterval = 0; //参加考核的时间限制
    private int mAgentInterval = 0; //参加考核的人员的时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mPreYear = "" ;
    private LAAssessHistorySchema mLAAssessHistorySchema = new
            LAAssessHistorySchema();

    public static void main(String Args[]) {
        String cOperate = "INSERT||MAIN";
        String tManageCom = "8611";
        String tYear = "2007";
        String tMonth = "01";
        String tBranchType = "2";
        String tBranchType2 = "01";
        String tGradeSeries = "E";

        VData cInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "mak002";
        tGlobalInput.ManageCom = "8611";
        tGlobalInput.ComCode = "8611";

        cInputData.add(tManageCom);
        cInputData.add(tYear);
        cInputData.add(tMonth);
        cInputData.add(tBranchType);
        cInputData.add(tBranchType2);
        cInputData.add(tGradeSeries);
        cInputData.add(tGlobalInput);

        AgentCalExaYear tAgentCalExaYear = new AgentCalExaYear();
        boolean tReturn = tAgentCalExaYear.submitData(cInputData, cOperate);
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        //进行传入数据的合法性验证
        if (!check()) {
            return false;
        }

        //纪录考核历史纪录防止同步
        if (!dealLAAssessHistory("0")) {
            return false;
        }

        //人员考核计算主函数
        if (!Examine()) {
            return false;
        }

        //进行考核结果备份
//        if(!dealLAAssessB())
//        {
//            return false;
//        }

        return true;
    }

    /**
     * 进行数据转储处理
     * @return boolean
     */
    private boolean dealLAAssessB() {
        LAAssessDB tLAAssessDB = new LAAssessDB();
        LAAssessSet tLAAssessSet = new LAAssessSet();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAssessBSet tLAAssessBSet = new LAAssessBSet();
        LAAssessBSchema tLAAssessBSchema = new LAAssessBSchema();
        VData tOutputDate = new VData();
        MMap tMap = new MMap();
        String tNewEdorNo = "";

        tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
        tLAAssessSet = tLAAssessDB.query();
        if (tLAAssessSet.size() > 0) {
            for (int i = 1; i <= tLAAssessSet.size(); i++) {
                Reflections tReflections = new Reflections();
                tLAAssessSchema.setSchema(tLAAssessSet.get(i));

                tLAAssessBSchema = new LAAssessBSchema();
                //复制备份表中相同的项
                tReflections.transFields(tLAAssessBSchema, tLAAssessSchema);
                //取得转储号码
                tNewEdorNo = PubFun1.CreateMaxNo("ASSESSEDORNO", 20);

                tLAAssessBSchema.setEdorNo(tNewEdorNo);
                tLAAssessBSchema.setEdorType("01");
                tLAAssessBSchema.setEdorDate(this.currentDate);

                tLAAssessBSet.add(tLAAssessBSchema);
            }

            tMap.put(tLAAssessBSet, "INSERT");
            tOutputDate.add(tMap);

            PubSubmit tPubSubmit = new PubSubmit();
            tPubSubmit.submitData(tOutputDate, "");
            if (tPubSubmit.mErrors.needDealError()) {
                CError tError = new CError();
                tError.moduleName = "AgentCalExamine";
                tError.functionName = "dealLAAssessB";
                tError.errorMessage = "进行考核数据备份操作失败！!";
                this.mErrors.addOneError(tError);

                return false;
            }
        } else {
            System.out.println("没有考核数据生成，不需要做转储处理！");
        }

        return true;
    }

    /**
     * 人员考核计算主函数
     * @return boolean
     */
    private boolean Examine() {
        //1、查询指定管理机构下的所有单位
        if (!getBranchGroupSetByManageCom(mManageCom)) {
            return false;
        }
        //2、验证每个机构是否都有设置计划 如果存在未计划的销售单位则不进行考核操作
        if (!checkBranchPlan()) {
            return false;
        }
        //3、查询所有人员职级
        if (!getAgentGrade()) {
            return false;
        }
        //4、根据各个机构和职级进行人员考核
        if (!dealAssess()) {
            // 考核出现错误，进行数据恢复
            dealRoolback();
            return false;
        }

        return true;
    }

    /**
     * 考核出错  进行数据回滚
     * @return boolean
     */
    private boolean dealRoolback() {
        LAAssessDB tLAAssessDB = new LAAssessDB();
        LAAssessSet tLAAssessSet = new LAAssessSet();
        String tSQL = "";
        tSQL = "select * from laassess where indexcalno='" + this.mIndexCalNo
               + "'  and state in ('00') and managecom like '" +
               this.mManageCom
               + "%' and branchtype='" + this.mBranchType
               + "' and branchtype2='" + this.mBranchType2;

        tSQL = tSQL + "' and agentgrade like '" + this.mGradeSeries + "%'";

        tLAAssessSet = tLAAssessDB.executeQuery(tSQL);

        VData tOutputDate = new VData();
        MMap tMap = new MMap();

        if (tLAAssessSet.size() != 0) {
            tMap.put(tLAAssessSet, "DELETE");
        }

        LAAssessHistoryDB tLAAssessHistoryDB = new LAAssessHistoryDB();
        LAAssessHistorySet tLAAssessHistorySet = new LAAssessHistorySet();
        tLAAssessHistoryDB.setIndexCalNo(mYear + mMonth); //考核计算年月代码
        tLAAssessHistoryDB.setManageCom(this.mManageCom); //管理机构
        tLAAssessHistoryDB.setBranchType(this.mBranchType); //展业类型
        tLAAssessHistoryDB.setBranchType2(this.mBranchType2); //渠道
        tLAAssessHistoryDB.setAgentGrade(mGradeSeries + "97"); //职级

        tLAAssessHistorySet = tLAAssessHistoryDB.query();

        tMap.put(tLAAssessHistorySet, "DELETE");
        tOutputDate.add(tMap);

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(tOutputDate, "");

        return true;
    }

    /**
     * 验证是否有机构没有设定计划 (暂时没什么用)
     * @return boolean
     */
    private boolean checkBranchPlan() {
        String tBranchGroup = ""; // 没有设定计划的机构
        String tSQL = ""; // 查询指定机构是否有计划的查询语句
        LDCodeSet tLDCodeSet = new LDCodeSet();
        LDCodeDB tLDCodeDB = new LDCodeDB();

        // 查询计划类型
        tSQL =
                "select * from ldcode where codetype = 'plankindpicc' order by Code";
        tLDCodeSet = tLDCodeDB.executeQuery(tSQL);
        if (tLDCodeSet.size() < 1) {
            System.out.println("不存在计划类型！");
            CError tError = new CError();
            tError.moduleName = "AgentCalExamineTeamGrp";
            tError.functionName = "checkBranchPlan";
            tError.errorMessage = "查询机构计划类型时，未找到相关数据！";
            this.mErrors.addOneError(tError);

            return false;
        }

        // 循环各机构
//        for(int i=1;i<=mLABranchGroupSet.size();i++)
//        {
//            // 循环计划类型
//            for(int j=1;j<=tLDCodeSet.size();j++)
//            {
//            }
//        }

        return true;
    }

    /**
     * 根据各个机构和职级进行人员考核
     * @return boolean
     */
    private boolean dealAssess() {
        String tSQL = ""; // 查询用SQL文
        int tAgentCont = 0; // 参加考核的人数
        //机构个数
        int contGroup = mLABranchGroupSet.size();
        //职级个数
        int contGrade = mLAAgentGradeSet.size();
        //1、根据机构作循环操作
        for (int i = 1; i <= contGroup; i++) {
            tAgentCont = 0; //初始化部门内参加考核的人数
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema = mLABranchGroupSet.get(i);
            System.out.println("机构编码：" + tLABranchGroupSchema.getBranchAttr());
            //取得该机构的管理机构编码，查处地区分类编码
            this.mAreaType = AgentPubFun.getAreaType(tLABranchGroupSchema.getAgentGroup());
            //2、根据职级作循环操作
            for (int j = 1; j <= contGrade; j++) {
                LATreeSet tLATreeSet = new LATreeSet(); //人员行政信息

                //查询指定机构，指定职级的所有人员
                tLATreeSet = getLATreeSetByGroupAndGrade(
                        mLABranchGroupSet.get(i).getAgentGroup(),
                        mLAAgentGradeSet.get(j).getGradeCode());
                System.out.println("机构[" +
                                   mLABranchGroupSet.get(i).getBranchAttr()
                                   + "]中[" +
                                   mLAAgentGradeSet.get(j).getGradeCode()
                                   + "]参加考核的人员有[" + tLATreeSet.size() + "]个");
                //3、根据 机构+职级 中每个个人进行循环操作
                int contAgent = tLATreeSet.size();
                // 如果机构中不存在该职级的人员，跳过处理下一条记录
                if (contAgent == 0) {
                    continue;
                }

                //3、循环人员进行处理
                for (int n = 1; n <= contAgent; n++) {
                    //取出一个人员的行政信息，进行考核
                    LATreeSchema tLATreeSchema = new LATreeSchema();
                    tLATreeSchema = tLATreeSet.get(n);
                    System.out.println("机构[" +
                                       mLABranchGroupSet.get(i).getBranchAttr()
                                       + "]  人员[" + tLATreeSchema.getAgentCode() +
                                       "]");
                    //延期转正的人员需要第一次考核3个月后才参加考核
                    if (tLATreeSchema.getAgentGrade().equals("D01") &&
                        tLATreeSchema.getState().equals("4")) {
                        String tsql =
                                "select indexcalno  from  laassess  where agentcode='"
                                + tLATreeSchema.getAgentCode()
                                +
                                "' order by  indexcalno desc fetch first 1 rows only ";
                        ExeSQL tExeSQL = new ExeSQL();
                        String tIndexCalNo = tExeSQL.getOneValue(tsql);
                        String tToday = mIndexCalNo + "01";
                        tToday = AgentPubFun.formatDate(tToday, "yyyy-MM-dd");
                        tsql = "select date('" + tToday +
                               "')-3 month from dual";
                        tExeSQL = new ExeSQL();
                        String tMonthNow = tExeSQL.getOneValue(tsql);
                        tMonthNow = AgentPubFun.formatDate(tMonthNow, "yyyyMM");
                        if (!tMonthNow.equals(tIndexCalNo)) {
                            continue; //必须是在上次考核后的第三个月参加考核
                        }

                        // if(mIndexCalNo.
                    }
                    //3.1、查询该职级需要做的各种考核类型
                    LAIndexVsAssessDB tLAIndexVsAssessDB = new
                            LAIndexVsAssessDB();
                    LAIndexVsAssessSet tLAIndexVsAssessSet = new
                            LAIndexVsAssessSet();
                    String tSql =
                            " select * from LAIndexVsAssess where AgentGrade='"
                            + mLAAgentGradeSet.get(j).getGradeCode()
                            + "'";
                    tSql +=
                            " and  AssessType in ('00','01','02','03') order by assesstype desc";

                    tLAIndexVsAssessSet = tLAIndexVsAssessDB.executeQuery(tSql);
                    //3.2、循环职级考核类型进行处理
                    for (int m = 1; m <= tLAIndexVsAssessSet.size(); m++) {

                        tCalIndex.tAgCalBase.setDestAgentGrade(
                                tLAIndexVsAssessSet.get(m).getDestAgentGrade());

                        //取得该考核类型的时间间隔
                        mInterval = this.getPeriod(
                                mLAAgentGradeSet.get(j).getGradeCode(),
                                tLAIndexVsAssessSet.get(m).getAssessType());
                        //计算并设置考核起止日期
                        if (!getBeginEnd(tLATreeSchema)) {
                            return false;
                        }
                        //取得计算出来的起止日期间间隔几个月
                        tSQL = "select months_between('" +
                               tCalIndex.tAgCalBase.getTempEnd()
                               + "','" + tCalIndex.tAgCalBase.getTempBegin() +
                               "') from dual";
                        mAgentInterval = execQuery(tSQL); //得到间隔月数
                        if (mAgentInterval < 0) {
                            System.out.println("查询起止日期间隔失败！");
                            return false;
                        }

                        boolean tReturn = false;

                        //判断是团险考核时所在机构是否允许参加可选考核项

                        //考核处理
                        tReturn = dealAgentAssess(tLATreeSchema,
                                                  tLABranchGroupSchema,
                                                  tLAIndexVsAssessSet.get(m).
                                                  getAssessType(),
                                                  1);

                        //}

                        //验证错误
                        if (!tReturn) {
                            //执行考核失败
                            CError tCError = new CError();
                            tCError.moduleName = "AgentCalExamine";
                            tCError.functionName = "dealAssess()";
                            tCError.errorMessage = "执行考核失败！";
                            System.out.println("执行考核失败！");
                            this.mErrors.addOneError(tCError);

                            return false;
                        }
                        //tAgentCont++;  //累加考核人数
                        if (mSuccFlag) {
                            break;
                        }
                    }
                    //插入LAAssess表
                    LAAssessDB tLAAssessDB = new LAAssessDB();
                    if (mLAAssessSet.size() > 0) {
                        tLAAssessDB.setSchema(mLAAssessSet.get(1));
                        if (!tLAAssessDB.insert()) {
                            this.mErrors.copyAllErrors(tLAAssessDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "AgentCalExamine";
                            tError.functionName = "dealAssess";
                            tError.errorMessage = "插入LAAssess表错误!";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                    }
                    //每次插入考核表后都要清除该集合
                    mLAAssessSet.clear();
                    tAgentCont++; //累加考核人数
                    System.out.println("一个人考核完毕！");
                }
                System.out.println("<对这个职级进行考评：" + mLAAgentGradeSet.get(j)
                                   .getGradeCode() + " 结束!>");
            }
            System.out.println("<对这个机构考评：" + tLABranchGroupSchema
                               .getAgentGroup() + " 结束!>");
        }
        //4、每算完一个机构后，更新laassesshistory表
        if (!dealLAAssessHistory("1")) {
            return false;
        }
        //5、每算完一个机构后，向laassessmain中插入一条记录
        if (!dealLAAssessMain()) {
            return false;
        }

        return true;
    }

    /**
     * 每算完一个机构后，向laassessmain中插入一条记录
     * @return boolean
     */
    private boolean dealLAAssessMain() {
        return true;
    }

    /**
     * 每次考核前纪录考核历史纪录 考核后更新laassesshistory表 防止同步运行
     * @param pmDealType String
     * @return boolean
     */
    private boolean dealLAAssessHistory(String pmDealType) {
        //LAAssessHistorySchema tLAAssessHistorySchema = new LAAssessHistorySchema();
        LAAssessHistoryDB tLAAssessHistoryDB = new LAAssessHistoryDB();
        if ("0".equals(pmDealType)) {
            // 设置考核计算历史表内容
            mLAAssessHistorySchema.setIndexCalNo(mYear + mMonth); //考核计算年月代码
            mLAAssessHistorySchema.setManageCom(this.mManageCom); //管理机构
            mLAAssessHistorySchema.setBranchType(this.mBranchType); //展业类型
            mLAAssessHistorySchema.setBranchType2(this.mBranchType2); //渠道
            mLAAssessHistorySchema.setAgentGrade(mGradeSeries + "97"); //职级
            mLAAssessHistorySchema.setState("00"); //考核计算状态
            mLAAssessHistorySchema.setOperator(mGlobalInput.Operator); //操作员
            mLAAssessHistorySchema.setMakeDate(currentDate); //入机日期
            mLAAssessHistorySchema.setMakeTime(currentTime); //入机时间
            mLAAssessHistorySchema.setModifyDate(currentDate); //修改日期
            mLAAssessHistorySchema.setModifyTime(currentTime); //修改时间

            tLAAssessHistoryDB.setSchema(mLAAssessHistorySchema);
            //插入考核计算历史纪录
            if (!tLAAssessHistoryDB.insert()) {
                this.mErrors.copyAllErrors(tLAAssessHistoryDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentCalExamine";
                tError.functionName = "dealLAAssessHistory";
                tError.errorMessage = "插入LAAssessHistory表错误!";
                this.mErrors.addOneError(tError);

                return false;
            }
        } else {
            mLAAssessHistorySchema.setState("01"); //考核计算状态
            tLAAssessHistoryDB.setSchema(mLAAssessHistorySchema);
            //插入考核计算历史纪录
            if (!tLAAssessHistoryDB.update()) {
                this.mErrors.copyAllErrors(tLAAssessHistoryDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentCalExamine";
                tError.functionName = "dealLAAssessHistory";
                tError.errorMessage = "修改LAAssessHistory表状态错误!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }

        return true;
    }

    /**
     * 维持或者晋升的时间标准
     * @param pmAgentGrade String
     * @param assesstype String
     * @return String
     */
    private int getPeriod(String pmAgentGrade, String assesstype) {
        String tSql1 = "";
        int tRes = 0;
        String tGradeS = "";

        if (pmAgentGrade.indexOf("E") < 0) {
            tGradeS = pmAgentGrade;
        } else {
            tGradeS = "E99";
        }

        tSql1 = "select assessmonths from laassessperiod where branchtype='"
                + this.mBranchType + "' and branchtype2='" + this.mBranchType2
                + "' and agentgrade='" + tGradeS + "' and assesstype='" +
                assesstype + "'"
                // and areatype='" + this.mAreaType + "'"
                + " order by assesstype";
        System.out.println("取得考核维持或者晋升的时间标准 SQL：" + tSql1);

        try {
            //查询返回结果
            tRes = execQuery(tSql1);
        } catch (Exception ex) {
            System.out.println("查询数据库失败_1");
        } finally {
        }
        System.out.println("标准类型：[" + assesstype + "] 结果月份数:" + tRes);
        return tRes;
    }


    /**
     * 对于业务员是否允许参加考核的验证
     * 除客户代表(A01)外其他职级都进行季度考核，客户代表如职时间在考核月10号后入司不参加考核
     * @param tLATreeSchema LATreeSchema
     * @param pmAgCalBase AgCalBase
     * @return boolean
     */
    private boolean checkJoinAssess(LATreeSchema tLATreeSchema,
                                    AgCalBase pmAgCalBase) {
        String tStartDate = pmAgCalBase.getTempBegin();
        String tEndDate = pmAgCalBase.getTempEnd();
        String tAgentGrade = tLATreeSchema.getAgentGrade();
        //判断 除D01外其余职级只允许进行年度考核：12月
        if (!tAgentGrade.equals("D01") && (!this.mMonth.equals("12"))) {
            System.out.println("[" + tLATreeSchema.getAgentCode() +
                               "]不符合参加考核标准！");
            return false;
        }
        return true;
    }

    /**
     * 计算并获得考核起期止期
     * @param pmAgentCode String
     * @return boolean
     */
    private boolean getBeginEnd(LATreeSchema pmLATreeSchema) {
        String tPostBegin = ""; //任职起期
        String tAssessBegin = ""; //最近一次考核的日期
        String tStartDate = ""; //考核起始日期
        String tEndDate = ""; //考核截止日期
        String tEmployDate= "" ;//入司日期（add by xiangchun ) ,以入司日期为考核开始日期

        tPostBegin = getPostBegin(
                pmLATreeSchema.getAgentCode()); //获得职级起聘日期
        tAssessBegin = getAssessBegin(
                pmLATreeSchema.getAgentCode(),
                pmLATreeSchema.getAgentGrade()); //获得上次考核日期

        if (tPostBegin.equals("")) { //判断职级起聘日期为空
            return false;
        }
        LAAgentDB tLAAgentDB= new LAAgentDB();
        tLAAgentDB.setAgentCode(pmLATreeSchema.getAgentCode());
        if(!tLAAgentDB.getInfo())
        {
            return false;
        }
        tEmployDate=tLAAgentDB.getEmployDate();
        String tToday = mIndexCalNo + "01";
        tToday = AgentPubFun.formatDate(tToday, "yyyy-MM-dd");
        String tsql = "select date('" + tToday + "')-1 month from dual";
        ExeSQL tExeSQL = new ExeSQL();
        String tMonthNow = tExeSQL.getOneValue(tsql);
        tMonthNow = AgentPubFun.formatDate(tMonthNow, "yyyyMM");
        tMonthNow = tMonthNow.substring(0, 4);
        String tStartDate1 = tMonthNow + "-01-01";
        // 判断设置考核起期
        if (tAssessBegin != null && !tAssessBegin.equals("")) {
            if ("D01".equals(pmLATreeSchema.getAgentGrade())) {
                //如果是D01职级，考核起期设置为职级起聘日期
                if (pmLATreeSchema.getState().equals("4")) { //如果是延期转正
                    tStartDate = tStartDate1;
                } else {
                    tStartDate = pmLATreeSchema.getStartDate();
                }
            } else {
                //如果前一次考核的日期存在  考核起期设置为系统考核起期
                tStartDate = tStartDate1;
            }
        } else { //前一次考核日期不存在  考核起期为职级起聘日期和考核起期中比较大的一个
           // tStartDate = pmLATreeSchema.getStartDate();
            if (tStartDate1.compareTo(tEmployDate) > 0) {
                tStartDate = tStartDate1;
            }
            else
            {
                tStartDate=tEmployDate;
            }
        }
        tEndDate = tMonthNow + "-12-31"; //获得考核止期：系统考核止期

        // 给指标计算类设置考核起止日期
        tCalIndex.tAgCalBase.setTempBegin(tStartDate);
        tCalIndex.tAgCalBase.setTempEnd(tEndDate);
        System.out.println("起始日：" + tStartDate + " / 终止日" + tEndDate);

        return true;
    }


    /**
     * 查询上次考核日期
     * @param pmAgentCode String
     * @param pmAgentGrade String
     * @return String
     */
    private String getAssessBegin(String pmAgentCode, String pmAgentGrade) {
        String sAssessBegin = "";
        PreparedStatement ps = null;
        ResultSet rs = null;

        String tSql =
                "select max(indexcalno) from laindexinfo where (indextype='02' or indextype='03')"
                + " and agentcode='" + pmAgentCode + "' and agentgrade ='" +
                pmAgentGrade + "'";
        System.out.println("查找最近考核日期SQL：" + tSql);

        try {
            Connection conn = DBConnPool.getConnection();
            ps = conn.prepareStatement(tSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                sAssessBegin = rs.getString(1);
                rs.close();
                ps.close();
            }
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                ps.close();
                rs.close();
            } catch (Exception e) {}
        }

        return sAssessBegin;
    }

    /**
     * 查询业务员的起聘日期
     * @param pmAgentCode String
     * @return String
     */
    private String getPostBegin(String pmAgentCode) {
        String tPostBegin = ""; //业务员的起聘日期
        LATreeDB tLATreeDB = new LATreeDB();

        tLATreeDB.setAgentCode(pmAgentCode);
        if (!tLATreeDB.getInfo()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentCalExamine";
            tError.functionName = "getPostBegin";
            tError.errorMessage = "获取人员职级起聘日期失败！";
            System.out.println("A:获取人员[" + pmAgentCode + "]职级起聘日期失败！");
            this.mErrors.addOneError(tError);

            return "";
        }
        tPostBegin = tLATreeDB.getStartDate().trim(); //得到业务员的起聘日期
        System.out.println("从LATree表中取业务员现职级起聘日期为:" + tPostBegin);
        //验证取到的起聘日期的合法性
        if (tPostBegin == null || tPostBegin.equals("")) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentCalExamine";
            tError.functionName = "getPostBegin";
            tError.errorMessage = "获取人员职级起聘日期失败！";
            System.out.println("B:获取人员[" + pmAgentCode + "]职级起聘日期为空！");
            this.mErrors.addOneError(tError);

            return "";
        }

        //取得日期所在月份的第一天和最后一天  如 2005-05-01  2005-05-31
        String[] StartDate = PubFun.calFLDate(tPostBegin);
        //System.out.println(StartDate[0]+" --- "+StartDate[1]);

        return tPostBegin;
    }

    /**
     * 进行人员考核
     * @param pmLATreeSchema LATreeSchema
     * @param pmLABranchGroupSchema LABranchGroupSchema
     * @param pmAssessType String
     * @return boolean
     */
    private boolean dealAgentAssess(LATreeSchema pmLATreeSchema,
                                    LABranchGroupSchema
                                    pmLABranchGroupSchema,
                                    String pmAssessType, int intType) {
        String tReturn = "";
        //给基数类赋值
        if (!setBaseValue(pmLATreeSchema.getAgentCode(),
                          this.tCalIndex.tAgCalBase)) {
            return false;
        }
        //查询 考核指标对应表
        if (!queryLaIndexVSAssess(pmLATreeSchema.getAgentGrade(),
                                  pmAssessType)) {
            System.out.println("查询LAIndexVSAssess表出错！");
            return false;
        }
        //设置基础信息
        tCalIndex.tAgCalBase.setAgentGrade(pmLATreeSchema.getAgentGrade()); //置业务员职级
        tCalIndex.tAgCalBase.setAgentCode(pmLATreeSchema.getAgentCode()); //置业务员编码
        tCalIndex.tAgCalBase.setAgentGroup(pmLATreeSchema.getAgentGroup()); //置业务员编码
        tCalIndex.tAgCalBase.setAssessType(pmAssessType); //考核类型
        tCalIndex.tAgCalBase.setBranchType(pmLATreeSchema.getBranchType());
        tCalIndex.tAgCalBase.setBranchType2(pmLATreeSchema.getBranchType2());
        tCalIndex.setAgentCode(pmLATreeSchema.getAgentCode()); //置业务员编码
        tCalIndex.setIndexCalNo(mYear + mMonth); //置考核日期
        tCalIndex.setOperator(mGlobalInput.Operator); //置操作员代码，在GlobalInput中
        tCalIndex.setIndexCalPrpty("01"); //“01”纵向关系；“02”横向关系

        //考核指标计算
        tReturn = tCalIndex.Calculate(); //开始计算
        System.out.println("考核指标计算结果" + tReturn);
        if (tCalIndex.mErrors.needDealError()) {
            System.out.println("Error:" + this.mErrors.getFirstError());
            // @@错误处理
            this.mErrors.copyAllErrors(tCalIndex.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentCalExamine";
            tError.functionName = "dealAgentAssess";
            tError.errorMessage = "考核指标计算出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println(
                "--------------------考核指标计算成功结束！--------------------");

        //计算考核sql
        System.out.println("－－－－－－－－开始用Sql找出建议职级－－－－－－－－－");
        Calculator tCal = new Calculator();

        //设置计算目标职级sql计算编码
        LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
        tLAIndexVsAssessDB.setAgentGrade(pmLATreeSchema.getAgentGrade()); //人员职级
        tLAIndexVsAssessDB.setAssessType(pmAssessType);
        LAIndexVsAssessSet tLAIndexVsAssessSet = tLAIndexVsAssessDB.query();
        tCal.setCalCode(tLAIndexVsAssessSet.get(1).getCalCode()); //查出sql计算编码
        //增加基本要素
        addAgCalBase(tCal);
        //13.计算目标职级
        tReturn = tCal.calculate(); //开始计算
        if (tCal.mErrors.needDealError()) {
            System.out.println("Error:" + this.mErrors.getFirstError());
            // @@错误处理
            this.mErrors.copyAllErrors(tCal.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "WageCal";
            tError.errorMessage = "考核指标sql计算出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("目标职级计算结果：" + tReturn);
        if (tReturn.equals("") || tReturn == null) {
            tCalIndex.clearIndexSet();
            mSuccFlag = false;
            return true;
        }

        //计算结束 进行考核表插入处理 (数据准备)
        // ↓ 整理考核记录信息
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        if (mLAAssessSet.size() == 0) {
            String tSQL = "select count(*) from laassess where agentcode='"
                          + pmLATreeSchema.getAgentCode() +
                          "' and State in ('00','01','02')";
            int tIntRt = execQuery(tSQL);
            if (tIntRt < 0) {
                //@错误处理
                CError tError = new CError();
                tError.moduleName = "AgentCalExamine";
                tError.functionName = "dealAgentAssess";
                tError.errorMessage = "查询原考核记录信息失败!";
                this.mErrors.addOneError(tError);

                return false;
            }

            tLAAssessSchema.setIndexCalNo(mYear + mMonth); //指标计算编码
            tLAAssessSchema.setAgentCode(pmLATreeSchema.getAgentCode()); //代理人编码
            tLAAssessSchema.setAgentGroup(pmLATreeSchema.getAgentGroup()); //展业机构
            tLAAssessSchema.setManageCom(pmLATreeSchema.getManageCom()); //管理机构
            tLAAssessSchema.setAgentGrade(pmLATreeSchema.getAgentGrade()); //代理人职级
            //代理人新职级、建议职级、代理人职级变更标志

            tLAAssessSchema.setAgentGrade1(tReturn);
            tLAAssessSchema.setCalAgentGrade(tReturn);
            if ((tReturn.compareTo(pmLATreeSchema.getAgentGrade()) == 0)) {
                tLAAssessSchema.setModifyFlag("02");
            } else if (tReturn.compareTo(pmLATreeSchema.getAgentGrade()) >
                       0) {
                tLAAssessSchema.setModifyFlag("03");
                if (pmLATreeSchema.getAgentGrade().equals("D01")) {
                    tLAAssessSchema.setAgentGrade1("D01");
                    tLAAssessSchema.setCalAgentGrade("D01");
                }
            } else if (tReturn.compareTo(pmLATreeSchema.getAgentGrade()) <
                       0
                       || tReturn.equals("A00")) {
                tLAAssessSchema.setModifyFlag("01");
            }
            if (tReturn.compareTo(pmLATreeSchema.getAgentGrade()) <
                0 && mGradeSeries.equals("D01") && tIntRt == 0) {
                tLAAssessSchema.setAgentGrade1("D01");
                tLAAssessSchema.setCalAgentGrade("D01");
            }

            //tLAAssessSchema.setAgentSeries(); //代理人系列
            //tLAAssessSchema.setAgentSeries1(); //代理人新系列
            tLAAssessSchema.setState("00"); //考核状态
            tLAAssessSchema.setOperator(mGlobalInput.Operator); //操作员
            tLAAssessSchema.setMakeDate(this.currentDate); //入机日期
            tLAAssessSchema.setMakeTime(this.currentTime); //入机时间
            tLAAssessSchema.setModifyDate(this.currentDate); //修改日期
            tLAAssessSchema.setModifyTime(this.currentTime); //修改时间
            tLAAssessSchema.setBranchType(this.mBranchType); //展业类型
            tLAAssessSchema.setBranchType2(this.mBranchType2); //渠道
            tLAAssessSchema.setStandAssessFlag("1"); //是基本法考核

            if (tIntRt == 0) {
                tLAAssessSchema.setFirstAssessFlag("1"); //是第一次参加考核
            } else {
                tLAAssessSchema.setFirstAssessFlag("0"); //不是第一次参加考核
            }
            tLAAssessSchema.setBranchAttr(pmLABranchGroupSchema.
                                          getBranchAttr()); //机构外部编码
        } else {
            tLAAssessSchema = mLAAssessSet.get(1);
            //如果没有降级进行下列处理
            if (!"01".equals(tLAAssessSchema.getModifyFlag())) {
                if ((tReturn != null) && (!tReturn.trim().equals(""))) {
                    tLAAssessSchema.setAgentGrade1(tReturn);
                    tLAAssessSchema.setCalAgentGrade(tReturn);
                    if (tReturn.compareTo(pmLATreeSchema.getAgentGrade()) ==
                        0) {
                        tLAAssessSchema.setModifyFlag("02");
                    } else if (tReturn.compareTo(pmLATreeSchema.
                                                 getAgentGrade()) >
                               0) {
                        tLAAssessSchema.setModifyFlag("03");
                    } else if (tReturn.compareTo(pmLATreeSchema.
                                                 getAgentGrade()) <
                               0
                               || tReturn.equals("D00")) {
                        tLAAssessSchema.setModifyFlag("01");
                    }
                }
            }
        }
        // ↑ 考核信息处理结束

        //增加考核记录
        mLAAssessSet.clear();
        mLAAssessSet.add(tLAAssessSchema);
        mSuccFlag = true;
        // 清空核心计算类中的相关指标计算集
        tCalIndex.clearIndexSet();

        return true;
    }

    private void addAgCalBase(Calculator cCal) {
        cCal.addBasicFactor("AgentCode", tCalIndex.tAgCalBase.getAgentCode());
        cCal.addBasicFactor("WageNo", tCalIndex.tAgCalBase.getWageNo());
        cCal.addBasicFactor("IndexCalNo", tCalIndex.tAgCalBase.getWageNo());
        cCal.addBasicFactor("WageCode", tCalIndex.tAgCalBase.getWageCode());
        cCal.addBasicFactor("AgentGrade",
                            tCalIndex.tAgCalBase.getAgentGrade());
        cCal.addBasicFactor("AgentGroup",
                            tCalIndex.tAgCalBase.getAgentGroup());
        cCal.addBasicFactor("MonthBegin",
                            tCalIndex.tAgCalBase.getMonthBegin());
        cCal.addBasicFactor("MonthEnd", tCalIndex.tAgCalBase.getMonthEnd());
        cCal.addBasicFactor("QuauterBegin",
                            tCalIndex.tAgCalBase.getQuauterBegin());
        cCal.addBasicFactor("QuauterEnd",
                            tCalIndex.tAgCalBase.getQuauterEnd());
        cCal.addBasicFactor("HalfYearBegin",
                            tCalIndex.tAgCalBase.getHalfYearBegin());
        cCal.addBasicFactor("HalfYearEnd",
                            tCalIndex.tAgCalBase.getHalfYearEnd());
        cCal.addBasicFactor("YearBegin", tCalIndex.tAgCalBase.getYearBegin());
        cCal.addBasicFactor("YearEnd", tCalIndex.tAgCalBase.getYearEnd());
        cCal.addBasicFactor("MonthMark", tCalIndex.tAgCalBase.getMonthMark());
        cCal.addBasicFactor("QuauterMark",
                            tCalIndex.tAgCalBase.getQuauterMark());
        cCal.addBasicFactor("HalfYearMark",
                            tCalIndex.tAgCalBase.getHalfYearMark());
        cCal.addBasicFactor("YearMark", tCalIndex.tAgCalBase.getYearMark());
        cCal.addBasicFactor("AreaType", mAreaType); //地区类型
        //cCal.addBasicFactor("AreaType", "A");   //地区类型
        cCal.addBasicFactor("TempBegin", tCalIndex.tAgCalBase.getTempBegin());
        cCal.addBasicFactor("TempEnd", tCalIndex.tAgCalBase.getTempEnd());
        cCal.addBasicFactor("Rate",
                            String.valueOf(tCalIndex.tAgCalBase.getRate()));
        cCal.addBasicFactor("A1State",
                            String.valueOf(tCalIndex.tAgCalBase.getA1State()));
        cCal.addBasicFactor("AssessType",
                            tCalIndex.tAgCalBase.getAssessType());
        cCal.addBasicFactor("LimitPeriod",
                            tCalIndex.tAgCalBase.getLimitPeriod());
        cCal.addBasicFactor("DestAgentGrade",
                            tCalIndex.tAgCalBase.getDestAgentGrade());
        cCal.addBasicFactor("BranchAttr",
                            tCalIndex.tAgCalBase.getBranchAttr());
        cCal.addBasicFactor("ChannelType",
                            tCalIndex.tAgCalBase.getChannelType());
        cCal.addBasicFactor("ChannelType",
                            tCalIndex.tAgCalBase.getChannelType());
        System.out.println("考核开始前奏：" + tCalIndex.tAgCalBase.getAgentGrade());
    }

    /**
     * 查询计算指标信息
     * @param sAgentGrade 业务员职级
     * @param sAssessType 考核类型（维持或晋升）
     * @return boolean 查询错误返回false
     */
    private boolean queryLaIndexVSAssess(String sAgentGrade,
                                         String sAssessType) {
        LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
        LAIndexVsAssessSet tLAIndexVsAssessSet = new LAIndexVsAssessSet();
        tLAIndexVsAssessDB.setAgentGrade(sAgentGrade);
        tLAIndexVsAssessDB.setAssessType(sAssessType);
        LAIndexVsAssessSchema tLAIndexVsAssessSchema = new
                LAIndexVsAssessSchema();
        tLAIndexVsAssessSet = tLAIndexVsAssessDB.query();
        if (tLAIndexVsAssessSet.size() < 1) {
            System.out.println("查询无结果");
            return false;
        } else {
            tLAIndexVsAssessSchema = tLAIndexVsAssessSet.get(1);
        }
        LAAssessIndexDB tLAAssessIndexDB = new LAAssessIndexDB();
        tLAAssessIndexDB.setIndexCode(tLAIndexVsAssessSchema.getIndexCode()); //算总分的sql代码
        if (!tLAAssessIndexDB.getInfo()) {
            //@错误处理
            this.mErrors.copyAllErrors(tLAAssessIndexDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentCalExamine";
            tError.functionName = "queryLaIndexVSAssess";
            tError.errorMessage = "查询指标信息出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tCalIndex.setAssessIndex(tLAAssessIndexDB); //把相关计算类加到指标计算集中
        return true;
    }

    /**给基数类赋值
     * @param cAgCalBase
     * @return boolean
     */
    private boolean setBaseValue(String pmAgentCode, AgCalBase pmAgCalBase) {
        tCalIndex.tAgCalBase.setAgentCode(pmAgentCode); //代理人编码
        tCalIndex.tAgCalBase.setAreaType(mAreaType);
        tCalIndex.tAgCalBase.setWageNo(mYear + mMonth); //考核计算编码（由考核年月得来）
        tCalIndex.tAgCalBase.setBranchAttr(AgentPubFun.getAgentBranchAttr(
                pmAgentCode)); //展业机构外部编码
        tCalIndex.tAgCalBase.setZSGroupBranchAttr(AgentPubFun.
                                                  getAgentBranchAttr(
                pmAgentCode)); //展业机构外部编码
        System.out.println("cAgCalBase.getTempBegin: " +
                           pmAgCalBase.getTempBegin());
        System.out.println("cAgCalBase.getTempEnd: " +
                           pmAgCalBase.getTempEnd());
        System.out.println("cAgCalBase.getBranchAttr():" +
                           pmAgCalBase.getBranchAttr());
        System.out.println("tCalIndex.tAgCalBase.getBranchAttr():" +
                           tCalIndex.tAgCalBase.getBranchAttr());

        return true;
    }

    /**
     * 查询指定机构，制定职级的所有人员
     * @param pmAgentGroup String
     * @param pmAgentCode String
     * @return LATreeSet
     */
    private LATreeSet getLATreeSetByGroupAndGrade(String pmAgentGroup,
                                                  String pmAgentGrade) {

        LATreeSet tLATreeSet = new LATreeSet();
        LATreeDB tLATreeDB = new LATreeDB();

        String tSQL = "";
        //查询机构中该职级当月10日以前入司的所有人员(不包括离职的，机构停业的)
        tSQL = "SELECT * FROM LATree a WHERE agentgroup = '" + pmAgentGroup +
               "'"
               + " AND AgentGrade = '" + pmAgentGrade +
               "' AND exists (SELECT 'X' FROM LAAgent "
               +
               " WHERE " +
               " AgentState <'06' AND " +
               " a.agentcode=agentcode"
               + "  and employdate<date('"+mPreYear+"-12-01') and InDueFormDate < date('"+mYear+"-01-01')) AND not exists " //入司不满一月不考核
               + " (SELECT 'X' FROM labranchgroup "
               + " WHERE a.agentgroup =agentgroup AND state='1' ) "
              // + " AND startdate<'" + mYear + "-" + mMonth +
             //  "-10' "
               + " AND BranchType='"
               + mBranchType + "' AND BranchType2='" + mBranchType2 + "'"
               + " AND InDueFormFlag='Y' "
               + " ORDER BY AgentCode ";

        System.out.println("查询人员SQL:" + tSQL);

        tLATreeSet = tLATreeDB.executeQuery(tSQL);

        return tLATreeSet;
    }

    /**
     * 查询所有人员职级
     * @return boolean
     */
    private boolean getAgentGrade() {
        String tSQL = "";
        tSQL = "SELECT";
        tSQL += "    *";
        tSQL += "  FROM";
        tSQL += "    LAAgentGrade";
        tSQL += " WHERE";
        tSQL += "    GradeProperty4 is not null AND";
        tSQL += "    BranchType = '" + mBranchType + "' AND";
        tSQL += "    BranchType2 = '" + mBranchType2 + "' AND";
        tSQL += "    GradeCode NOT IN (" + mWithoutAgentGrade + ") AND";
        tSQL += "    GradeCode LIKE '" + mGradeSeries + "%'";

        tSQL += " ORDER BY";
        tSQL += "    GradeCode";
        System.out.println("查询职级SQL文：" + tSQL);
        LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
        LAAgentGradeSet tLAAgentGradeSet = new LAAgentGradeSet();
        tLAAgentGradeSet = tLAAgentGradeDB.executeQuery(tSQL);
        System.out.println("查询各种职级,共[" + tLAAgentGradeSet.size() + "]个!");
        //判断查询是否成功
        if (tLAAgentGradeDB.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentCalExamine";
            tError.functionName = "Examine";
            tError.errorMessage = "查询职级时,访问数据库失败！";
            this.mErrors.addOneError(tError);

            return false;
        }
        if (tLAAgentGradeSet.size() == 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentCalExamine";
            tError.functionName = "Examine";
            tError.errorMessage = "查询职级没有数据！";
            this.mErrors.addOneError(tError);

            return false;
        }

        //保存职级信息到全局变量
        mLAAgentGradeSet.set(tLAAgentGradeSet);

        return true;
    }

    /**
     * 通过管理机构查询下属所有机构
     * @param pmManageCom String
     * @return boolean
     */
    private boolean getBranchGroupSetByManageCom(String pmManageCom) {
        String tSQL = "";
        tSQL = "SELECT";
        tSQL += "    *";
        tSQL += "  FROM";
        tSQL += "    LABranchGroup a";
        tSQL += " WHERE";
        tSQL += "    ManageCom like '" + pmManageCom + "%' AND";
        tSQL += "    ManageCom like '" + this.mGlobalInput.ManageCom +
                "%' AND";
        tSQL += "    BranchType = '" + mBranchType + "' AND";
        tSQL += "    BranchType2 = '" + mBranchType2 + "' AND";
        tSQL += "    (State <> '1' or State is null)"; // 公司业务不参加考核
        tSQL += " ORDER BY";
        tSQL += "    BranchSeries";
        System.out.println(tSQL);

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(tSQL);
        System.out.println("该四位管理机构下的八位机构数 : " + tLABranchGroupSet.size());
        //验证查询结果
        if (tLABranchGroupDB.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentCalExamine";
            tError.functionName = "Examine";
            tError.errorMessage = "查询机构失败！";
            this.mErrors.addOneError(tError);

            return false;
        }
        if (tLABranchGroupSet.size() == 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentCalExamine";
            tError.functionName = "Examine";
            tError.errorMessage = "查询机构失败！";
            this.mErrors.addOneError(tError);

            return false;
        }

        //负给全局变量
        mLABranchGroupSet.set(tLABranchGroupSet);

        return true;
    }

    /**
     * 进行数据验证
     * @return boolean
     */
    private boolean check() {
        String bSQL = "";

        //判断
       String tMonth="12";
                 //  mYear-1;
       bSQL = "select state from lawagehistory where branchtype='" +
              mBranchType +
        "' and branchtype2='" + mBranchType2 + "' and wageno='" + mPreYear +
              tMonth + "' and managecom like '" + mManageCom +
              "%' and state='14' ";


                  try {
            if (execQuery(bSQL) == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AgentCalExamine";
                tError.functionName = "check";
                tError.errorMessage = "指定机构，未进行薪资计算！";
                System.out.println("指定机构，未进行薪资计算！。");
                this.mErrors.addOneError(tError);

                return false;
            }
                  } catch (Exception e) {
            System.out.println("未进行薪资计算_1");
                  }

        // 如果考核的是主管系列，需要验证业务系列已经考核完毕
        if ("E".equals(mGradeSeries)) {
            bSQL =
                    "select count(*) from LAAssessHistory where indexcalno='"
                    + mYear + mMonth + "' and managecom='" + mManageCom +
                    "'"
                    + " and BranchType='" + mBranchType +
                    "' and BranchType2='"
                    + mBranchType2 +
                    "' and AgentGrade='D97' ";

            try {
                //查询考核记录，判断是否已经考核过业务系列人员
                if (execQuery(bSQL) < 1) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "AgentCalExamine";
                    tError.functionName = "check";
                    tError.errorMessage = "指定机构，未对业务系列进行考核，清先对业务系列人员进行考核！";
                    System.out.println("指定机构，未对业务系列进行考核。");
                    this.mErrors.addOneError(tError);

                    return false;
                }
            } catch (Exception ex) {
                System.out.println("查询数据库失败_1");
            }
        }

        // 根据考核历史标进行判断是否考核过
        bSQL = "select count(*) from LAAssessHistory where indexcalno='"
               + mYear + mMonth + "' and managecom='" + mManageCom + "'"
               + " and BranchType='" + mBranchType + "' and BranchType2='"
               + mBranchType2 + "' and AgentGrade='";
        bSQL = bSQL + mGradeSeries + "97'";
        System.out.println("判断二次考核SQL:" + bSQL);

        try {
            //查询考核记录，判断是否考核过
            if (execQuery(bSQL) > 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AgentCalExamine";
                tError.functionName = "check";
                tError.errorMessage = "指定机构，指定年月已经作过考核。请确认！";
                System.out.println("指定机构，指定年月已经作过考核。请确认！");
                this.mErrors.addOneError(tError);

                return false;
            }

            bSQL = "select count(*) from LAAssess where IndexCalNo = '"
                   + mYear + mMonth + "' and managecom='" + mManageCom +
                   "'"
                   + " and BranchType='" + mBranchType +
                   "' and BranchType2='"
                   + mBranchType2 + "' and AgentGrade like '"
                   ;
            bSQL = bSQL + mGradeSeries + "%' and state in ('00') ";

            if (execQuery(bSQL) > 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AgentCalExamine";
                tError.functionName = "check";
                tError.errorMessage = "指定机构，指定序列，指定年月已经作过考核。请确认！";
                System.out.println("指定机构，指定序列，指定年月已经作过考核。请确认！");
                this.mErrors.addOneError(tError);

                return false;
            }
        } catch (Exception ex) {
            System.out.println("查询数据库失败_1");
        } finally {
        }

        System.out.println("数据验证通过！");
        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql) {
        System.out.println(sql);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null) {
                return 0;
            }
            st = conn.prepareStatement(sql);
            if (st == null) {
                return 0;
            }
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
         if (!conn.isClosed()) {
             conn.close();
         }
         try {
             st.close();
             rs.close();
         } catch (Exception ex2) {
             ex2.printStackTrace();
         }
         st = null;
         rs = null;
         conn = null;
       } catch (Exception e) {}

        }
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName(
                                       "GlobalInput", 0));
        this.mManageCom = (String) cInputData.getObject(0); //管理机构
        this.mYear = (String) cInputData.getObject(1); //考核年
        this.mMonth = (String) cInputData.getObject(2); //考核月
        this.mBranchType = (String) cInputData.getObject(3); //展业类型
        this.mBranchType2 = (String) cInputData.getObject(4); //展业类型
        this.mGradeSeries = (String) cInputData.getObject(5); //职级系列
        this.mIndexCalNo = mYear + mMonth; //指标计算编码
        int tYear= Integer.parseInt(mYear)-1 ;
        mPreYear = String.valueOf(tYear) ;

        System.out.println(mManageCom + " / " + mIndexCalNo + " / " +
                           mBranchType
                           + " / " + mBranchType2 + " / " + mGradeSeries);

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentCalExamine";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private void jbInit() throws Exception {
    }

}
