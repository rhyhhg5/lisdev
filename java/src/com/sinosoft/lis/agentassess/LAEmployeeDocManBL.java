package com.sinosoft.lis.agentassess;

/**
 * <p>Title: 档案管理程序 </p>
 * <p>Description: 实现档案调入、调出处理</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author LL
 * @version 1.0
 */

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;


public class LAEmployeeDocManBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private String mManageCom; //保存管理机构
    private LAAgentSet mLAAgentSet = new LAAgentSet(); //存放前台传过来的要处理的人员

    private LAAgentSet mUpdateLAAgentSet = new LAAgentSet(); //用来存放要更新的数据
    private LAAgentBSet mBackUpLAAgentBSet = new LAAgentBSet(); //用来存放要备份的数据

    /** 取得系统日期、时间 */
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public LAEmployeeDocManBL()
    {
    }

    public static void main(String[] args)
    {
        LAEmployeeDocManBL tLAEmployeeDocManBL = new LAEmployeeDocManBL();
        //
        String tManageCom = "86110000";
        VData cInputData;
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        cInputData = new VData();
        cInputData.add(tGlobalInput);
        cInputData.add(tManageCom);

        LAAgentSet tLAAgentSet = new LAAgentSet();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tLAAgentSchema.setAgentCode("8611000013");
        tLAAgentSet.add(tLAAgentSchema);
        cInputData.add(tLAAgentSet);
        tLAEmployeeDocManBL.submitData(cInputData, "INPART||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeDocManBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAEmployeeDocManBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start LAEmployeeDocManBL Submit...");
        LAEmployeeDocManBLS tLAEmployeeDocManBLS = new LAEmployeeDocManBLS();
        tLAEmployeeDocManBLS.submitData(this.mInputData, this.mOperate);
        System.out.println("End LAEmployeeDocManBL Submit...");
        //如果有需要处理的错误，则返回
        if (tLAEmployeeDocManBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAEmployeeDocManBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeDocManBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //获得前台传过来的数据
        this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mManageCom = (String) cInputData.getObject(1);
        //判断是 整体操作 还是 部分操作
        if (this.mOperate.equals("INPART||MAIN") ||
            this.mOperate.equals("OUTPART||MAIN"))
        {
            this.mLAAgentSet = (LAAgentSet) cInputData.getObject(2);
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //对 整体处理 情况进行处理
        if (this.mOperate.equals("INALL||MAIN") ||
            this.mOperate.equals("OUTALL||MAIN"))
        {
            //查询出某个管理机构下的所有人员
            String tSql =
                    "select a.agentcode from laagent a where a.managecom like '" +
                    this.mManageCom + "%'"
                    + " and (a.agentstate < '03' or a.agentstate is null) "
                    + " and a.branchtype = '1' "
                    +
                    " and a.branchcode not  in (select agentgroup from labranchgroup "
                    + " where labranchgroup.branchtype='1' and labranchgroup.branchlevel='01' and labranchgroup.state='1' )";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(tSql);
            String[][] tAgentCode = tSSRS.getAllData();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            String tQualiPassFlag = "";
            //处理管理机构下所有数据
            for (int i = 0; i < tAgentCode.length; i++)
            {
                //查询业务员信息
                tLAAgentDB.setAgentCode(tAgentCode[i][0]);
                if (!tLAAgentDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeDocManBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "代理人" + tAgentCode[i][0] +
                                          "查询LAAgent表信息出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //判断此人档案调入情况，视情况进行相关处理
                tQualiPassFlag = tLAAgentDB.getQualiPassFlag();
                //整体调出操作 并且 档案调动标志位为 0 or null  ，则跳过此人
                if (this.mOperate.equals("OUTALL||MAIN") &&
                    (tQualiPassFlag == null || tQualiPassFlag.equals("0")))
                {
                    continue;
                }
                //整体调入操作 并且 档案调动标志位为1 ，则跳过此人
                if (this.mOperate.equals("INALL||MAIN") &&
                    (tQualiPassFlag != null && tQualiPassFlag.equals("1")))
                {
                    continue;
                }

                //备份原来档案情况 ,记录现在档案情况
                if (!lAAgentBackUp(tAgentCode[i][0]))
                {
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeDocManBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "备份代理人" + tAgentCode[i][0] +
                                          "LAAgent表信息出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }

            }
        }
        //对部分调入情况的处理
        else if (this.mOperate.equals("INPART||MAIN") ||
                 this.mOperate.equals("OUTPART||MAIN"))
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            String tQualiPassFlag = ""; //存放档案标志位
            //逐个循环对选种人员进行处理
            for (int i = 1; i <= this.mLAAgentSet.size(); i++)
            {
                //查询业务员信息
                tLAAgentDB.setAgentCode(this.mLAAgentSet.get(i).getAgentCode());
                if (!tLAAgentDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeDocManBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "代理人" +
                                          this.mLAAgentSet.get(i).getAgentCode() +
                                          "查询LAAgent表信息出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //判断此人档案调入情况，视情况进行相关处理
                tQualiPassFlag = tLAAgentDB.getQualiPassFlag();

                //部分调出操作 并且 档案调动标志位为 0 or null  ，则跳过此人
                if (this.mOperate.equals("OUTPART||MAIN") &&
                    (tQualiPassFlag == null || tQualiPassFlag.equals("0")))
                {
                    continue;
                }
                //部分调入操作 并且 档案调动标志位为1 ，则跳过此人
                if (this.mOperate.equals("INPART||MAIN") &&
                    (tQualiPassFlag != null && tQualiPassFlag.equals("1")))
                {
                    continue;
                }

                //备份原来档案情况 ,记录现在档案情况
                if (!lAAgentBackUp(this.mLAAgentSet.get(i).getAgentCode()))
                {
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeDocManBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "备份代理人" +
                                          this.mLAAgentSet.get(i).getAgentCode() +
                                          "LAAgent表信息出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 把处理后的数据保存，准备传到后台
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mBackUpLAAgentBSet); //1 做备份造作
            this.mInputData.add(this.mUpdateLAAgentSet); //2 做更新操作
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeDocManBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 备份LAAgentB表 ，同时记录新的人员信息
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean lAAgentBackUp(String tAgentCode)
    {
        String tEdorNo = "";
        //查找原来的业务员信息
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);

        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAEmployeeDocManBL";
            tError.functionName = "lAAgentBackUp";
            tError.errorMessage = "备份代理人" + tAgentCode + "LAAgent表信息出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //备份修改前 代理人信息
        LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLAAgentBSchema, tLAAgentDB.getSchema());
        tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        tLAAgentBSchema.setEdorNo(tEdorNo);
        //转储类型
        //档案调入备份
        if (this.mOperate.equals("INPART||MAIN") ||
            this.mOperate.equals("INALL||MAIN"))
        {
            tLAAgentBSchema.setEdorType("08");
        }
        //档案调出备份
        else if (this.mOperate.equals("OUTPART||MAIN") ||
                 this.mOperate.equals("OUTALL||MAIN"))
        {
            tLAAgentBSchema.setEdorType("09");
        }

        tLAAgentBSchema.setMakeDate(this.mCurrentDate);
        tLAAgentBSchema.setMakeTime(this.mCurrentTime);
        tLAAgentBSchema.setModifyDate(this.mCurrentDate);
        tLAAgentBSchema.setModifyTime(this.mCurrentTime);
        tLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
        this.mBackUpLAAgentBSet.add(tLAAgentBSchema);

        //修改新的代理人信息
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tLAAgentSchema.setSchema(tLAAgentDB.getSchema());
        //档案调动日期
        tLAAgentSchema.setTrainDate(this.mCurrentDate);
        //档案调动标志位
        if (this.mOperate.equals("INPART||MAIN") ||
            this.mOperate.equals("INALL||MAIN"))
        {
            tLAAgentSchema.setQualiPassFlag("1");
        }
        else if (this.mOperate.equals("OUTPART||MAIN") ||
                 this.mOperate.equals("OUTALL||MAIN"))
        {
            tLAAgentSchema.setQualiPassFlag("0");
        }

        tLAAgentSchema.setMakeDate(this.mCurrentDate);
        tLAAgentSchema.setMakeTime(this.mCurrentTime);
        tLAAgentSchema.setModifyDate(this.mCurrentDate);
        tLAAgentSchema.setMakeTime(this.mCurrentTime);
        this.mUpdateLAAgentSet.add(tLAAgentSchema);

        return true;
    }
}
