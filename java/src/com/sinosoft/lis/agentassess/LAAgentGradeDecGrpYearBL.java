package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.agent.ALADimissionAppUI;
import com.sinosoft.lis.agentassess.LAAssessInputUI;

/**
 * 团险见习业务经理考核归属
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAAgentGradeDecGrpYearBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 提交数据的容器 */
    private MMap mMap = new MMap();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSet mLAAssessSet1 = new LAAssessSet(); //非清退人员，转正晋升
    private LAAssessSet mLAAssessSet2 = new LAAssessSet(); //清退人员
    private LAAssessSet mLAAssessSet3 = new LAAssessSet(); //延期转正
    private LAAssessSet mLAAssessSet4 = new LAAssessSet(); //考核维持，转正维持
    private LAAssessHistorySchema mLAAssessHistorySchema = new
            LAAssessHistorySchema();

    private LAAssessSet mLAAssessSet = new LAAssessSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private String mEvaluateDate = ""; // 异动时间
    private String mManageCom = ""; // 管理机构
    private String mIndexCalNo = ""; // 考核序列号
    private String mGradeSeries = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mBranchAttr = ""; //前台 传入的销售机构编码
    private String mBranchName = ""; //前台传入的销售机构名称
    private String mAgentCode = ""; //前台传入的业务员代码
    private String mAgentName = ""; //前台传入的业务员姓名
    public static void main(String args[]) {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "001";
        LAAssessHistorySchema aLAAssessHistorySchema = new
                LAAssessHistorySchema();
        // 准备数据
        aLAAssessHistorySchema.setManageCom("8611");
        aLAAssessHistorySchema.setIndexCalNo("200612");
        aLAAssessHistorySchema.setBranchType("2");
        aLAAssessHistorySchema.setBranchType2("01");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(aLAAssessHistorySchema);
        System.out.println("add over");
        LAAgentGradeDecGrpYearBL m = new LAAgentGradeDecGrpYearBL();
        boolean tag = m.submitData(tVData, "");
        if (tag) {
            System.out.println("ok");
        } else {
            System.out.println("fail");
        }
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //取得异动时间
        if (!getEvaluateDate()) {
            return false;
        }
        //处理前进行验证
//        if(!check())
//        {
//            return false;
//        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //进行人员职级异动处理
        if (!dealAgentFormUp()) {
            return false;
        }
        //处理延期转正
        //if (!dealAssess3()) {
        // return false;
        //}
        //对考核主表进行处理
        if (!dealLAAssessMain()) {
            return false;
        }
        //进行转正处理
        if (!dealInDue()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        //统一提交
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        if (tPubSubmit.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecGrpYearBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 进行转正处理,职级不变(维持)
     * @return boolean
     */
    private boolean dealInDue() {
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        LAAgentSet tLAAgentSet = new LAAgentSet();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeDB tLATreeDB = new LATreeDB();

        if (mLAAssessSet4.size() < 1) {
            return true;
        }
        // 进行转正处理加入转正日期和标记
        for (int i = 1; i <= mLAAssessSet4.size(); i++) {
            tLAAssessSchema = new LAAssessSchema();
            tLAAssessSchema = mLAAssessSet4.get(i).getSchema();

            //处理基本信息
            tLAAgentDB = new LAAgentDB();
            tLAAgentSchema = new LAAgentSchema();
            tLAAgentDB.setAgentCode(tLAAssessSchema.getAgentCode());
            tLAAgentDB.getInfo();
            tLAAgentSchema = tLAAgentDB.getSchema();
            tLAAgentSchema.setInDueFormDate(mEvaluateDate);
            tLAAgentSchema.setModifyDate(this.currentDate);
            tLAAgentSchema.setModifyTime(this.currentTime);
            tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
            tLAAgentSet.add(tLAAgentSchema);

            //处理行政信息
            tLATreeDB = new LATreeDB();
            tLATreeSchema = new LATreeSchema();
            tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
            tLATreeDB.getInfo();
            tLATreeSchema = tLATreeDB.getSchema();
            tLATreeSchema.setInDueFormDate(mEvaluateDate);
            tLATreeSchema.setInDueFormFlag("Y"); //转正标记
            tLATreeSchema.setAstartDate(mEvaluateDate);
            tLATreeSchema.setModifyDate(this.currentDate);
            tLATreeSchema.setModifyTime(this.currentTime);
            tLATreeSchema.setState("0");
            tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
            tLATreeSet.add(tLATreeSchema);
        }
        /** @todo  */
        mLAAgentSet.add(tLAAgentSet);
        mLATreeSet.add(tLATreeSet);
        return true;
    }

    /**
     * 人员新职级归属完毕以后更改考核主表的状态
     * @return boolean
     */
    private boolean dealLAAssessMain() {
        return true;
    }

    /**
     * 分类处理清退人员和职级发生变化的人员
     * @return boolean
     */
    private boolean dealAgentFormUp() {
        //处理离职人员信息变量
        LADimissionSchema tLADimissionSchema = new LADimissionSchema();
        VData tVData = new VData();

        //处理清退人员
        if (mLAAssessSet2.size() > 0) {
            for (int i = 1; i <= mLAAssessSet2.size(); i++) {
               //在进行考核清退插入时,首先校验是否已经有离职记录或者离职确认
                LADimissionDB tLADimissionDB=new LADimissionDB();
                LADimissionSet tLADimissionSet=new LADimissionSet();
                tLADimissionDB.setAgentCode(mLAAssessSet2.get(i).
                        getAgentCode());
                tLADimissionSet=tLADimissionDB.query();
                if(tLADimissionSet.size()>0)
                {
                	 continue;
                }
                else
                {
            	tLADimissionSchema = new LADimissionSchema();

                tLADimissionSchema.setAgentCode(mLAAssessSet2.get(i).
                                                getAgentCode());
                tLADimissionSchema.setApplyDate(mEvaluateDate); //处理时间
                tLADimissionSchema.setDepartRsn("0"); //离职原因
                tLADimissionSchema.setNoti("维持考核不达标！"); //备注
                tLADimissionSchema.setOperator(this.mGlobalInput.Operator);
                tLADimissionSchema.setBranchType(mLAAssessSet2.get(i).
                                                 getBranchType());
                tLADimissionSchema.setBranchType2(mLAAssessSet2.get(i).
                                                  getBranchType2());
                tLADimissionSchema.setDepartTimes("1");

                tVData = new VData();
                tVData.addElement(tLADimissionSchema);
                tVData.add(this.mGlobalInput);

                ALADimissionAppUI tLADimission = new ALADimissionAppUI();
                if (!tLADimission.submitData(tVData, "INSERT||MAIN")) {
                    this.mErrors.copyAllErrors(tLADimission.mErrors);
                    System.out.println("进行离职人员处理失败！");
                    return false;
                }
               }
            }
        }

        //处理非清退人员(职级异动),这里只有D01转正为同系列D01以上职级的情形
        if (mLAAssessSet1.size() > 0) {
            LAAssessSchema tLAAssessSchema = new LAAssessSchema();
            LAAgentSet tLAAgentSet = new LAAgentSet();
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            LAAgentDB tLAAgentDB = new LAAgentDB();

            LATreeSet tLATreeSet = new LATreeSet();
            LATreeSchema tLATreeSchema = new LATreeSchema();
            LATreeDB tLATreeDB = new LATreeDB();
            Reflections tRefs = new Reflections();
            LATreeBSet tLATreeBSet = new LATreeBSet();
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            String tEdorNo = "";
            for (int j = 1; j <= mLAAssessSet1.size(); j++) {
                tLAAssessSchema = new LAAssessSchema();
                tLAAssessSchema.setSchema(mLAAssessSet1.get(j).getSchema());
                tLATreeDB = new LATreeDB();
                tLATreeSchema = new LATreeSchema();
                tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
                tLATreeDB.getInfo();
                tLATreeSchema = tLATreeDB.getSchema();
                //备份行政信息
                tLATreeBSchema = new LATreeBSchema();
                tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                tRefs = new Reflections();
                tRefs.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
                tLATreeBSchema.setOperator(mGlobalInput.Operator);
                tLATreeBSchema.setEdorNO(tEdorNo);
                tLATreeBSchema.setRemoveType("01"); //手工考核确认
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
               // tLATreeBSchema.setAgentGrade(tLAAssessSchema.getCalAgentGrade());
                tLATreeBSet.add(tLATreeBSchema);
                //更新行政信息
                //tLATreeSchema.setAgentGrade(tLAAssessSchema.getCalAgentGrade());
                tLATreeSchema.setAgentLastGrade(tLATreeBSchema.getAgentGrade());
                tLATreeSchema.setAgentLastSeries(tLATreeBSchema.getAgentSeries());
                tLATreeSchema.setOldStartDate(tLATreeSchema.getStartDate());
                tLATreeSchema.setOldEndDate(PubFun.calDate(mEvaluateDate, -1,
                        "D", null));
                tLATreeSchema.setAgentGrade(tLAAssessSchema.getCalAgentGrade());
                if (tLAAssessSchema.getCalAgentGrade().equals("D01")) {
                    tLATreeSchema.setInDueFormFlag("N"); //转正标记
                    tLATreeSchema.setInDueFormDate(""); //转正标记

                    tLAAgentDB = new LAAgentDB();
                    tLAAgentSchema = new LAAgentSchema();
                    tLAAgentDB.setAgentCode(tLAAssessSchema.getAgentCode());
                    tLAAgentDB.getInfo();
                    tLAAgentSchema = tLAAgentDB.getSchema();
                    tLAAgentSchema.setInDueFormDate("");
                    tLAAgentSchema.setModifyDate(this.currentDate);
                    tLAAgentSchema.setModifyTime(this.currentTime);
                    tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
                    tLAAgentSet.add(tLAAgentSchema);

                }
                if (tLAAssessSchema.getModifyFlag().equals("01")) {
                    tLATreeSchema.setState("2");
                }
                if (tLAAssessSchema.getModifyFlag().equals("03")) {
                    tLATreeSchema.setState("1");
                }
                if (tLAAssessSchema.getModifyFlag().equals("02"))
                {
                    tLATreeSchema.setState("0");
                }
                tLATreeSchema.setStartDate(mEvaluateDate);
                tLATreeSchema.setAstartDate(mEvaluateDate);

               // tLATreeSchema.setStartDate(mEvaluateDate);
               // tLATreeSchema.setAstartDate(mEvaluateDate);
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(mGlobalInput.Operator);
                tLATreeSet.add(tLATreeSchema);

            }
            /** @todo  */
            mLAAgentSet.add(tLAAgentSet);
            mLATreeSet.add(tLATreeSet);
            mLATreeBSet.add(tLATreeBSet);
        }
        return true;
    }

    /**
     * 处理延期转正的（这里只有D01维持D01，计算职级为D00）
     * @return boolean
     */
    private boolean dealAssess3() {
        if (mLAAssessSet3.size() >= 1) {
            LATreeSet tLATreeSet = new LATreeSet();
            for (int i = 1; i <= mLAAssessSet3.size(); i++) {
                LAAssessSchema tLAAssessSchema = new LAAssessSchema();
                tLAAssessSchema = mLAAssessSet3.get(i).getSchema();
                LATreeSchema tLATreeSchema = new LATreeSchema();
                LATreeDB tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(tLAAssessSchema.getAgentCode());
                tLATreeDB.getInfo();
                tLATreeSchema = tLATreeDB.getSchema();
                tLATreeSchema.setState("4");
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setModifyTime(mGlobalInput.Operator);
                tLATreeSet.add(tLATreeSchema);
            }
            if (tLATreeSet.size() >= 1) {
                mLATreeSet.add(tLATreeSet);
            }
        }
        return true;
    }

    /**
     * 取得异动时间(考核期次月一号)
     * @return boolean
     */
    private boolean getEvaluateDate() {
        String tSQL = "";

        tSQL = "select date(substr('" + mIndexCalNo + "',1,4)||'-'|| substr(";
        tSQL += "'" + mIndexCalNo + "',5,2)||'-01')  from dual";

        ExeSQL tExeSQL = new ExeSQL();
        // 取得异动时间
        mEvaluateDate = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecGrpYearBL";
            tError.functionName = "getEvaluateDate";
            tError.errorMessage = "获取异动日期失败！";
            System.out.println("获取异动日期失败！");
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("获得异动日期：" + mEvaluateDate);
        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean checkPer(LAAssessSchema aLAAssessSchema) {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tLAAgentDB.setAgentCode(aLAAssessSchema.getAgentCode());
        tLAAgentDB.getInfo();
        tLAAgentSchema = tLAAgentDB.getSchema();
        if (AgentPubFun.ConverttoYM(tLAAgentSchema.getEmployDate()).compareTo(
                mIndexCalNo) > 0) {
            CError.buildErr(this,
                            "业务员" + aLAAssessSchema.getAgentCode() +
                            "入司年月不能大于考核归属年月!");
            return false;
        }
        String
                tSQL = "select count(*) from laassess where indexcalno>='"
                       + mIndexCalNo + "' and agentcode='"
                       + aLAAssessSchema.getAgentCode() + "' and state='02' ";
        if (execQuery(tSQL) > 0) {
            CError.buildErr(this,
                            "业务员" + aLAAssessSchema.getAgentCode() + "在" +
                            mIndexCalNo +
                            "月及以后已经考核归属过!");
            return false;
        }

        //检验当期考核是否已经进行过考核确认
        tSQL = "select count(*) from laassess where indexcalno='"
               + mIndexCalNo + "' and agentcode='"
               + aLAAssessSchema.getAgentCode() + "' and state='01' ";
        if (execQuery(tSQL) < 1) {
            CError.buildErr(this,
                            "业务员" + aLAAssessSchema.getAgentCode() + "未进行考核确认!");
            return false;
        }

        System.out.println("验证通过！");
        return true;
    }

    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        mInputData.clear();
        mMap.put(mLAAssessSet, "UPDATE");
        mMap.put(mLAAgentSet, "UPDATE");
        mMap.put(mLATreeSet, "UPDATE");
        mMap.put(mLATreeBSet, "INSERT");
        mInputData.add(mMap);
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData() {
        LAAssessSet tLAAssessSet = new LAAssessSet();
        LAAssessDB tLAAssessDB = new LAAssessDB();
        LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        int tAgentCount = 0;
        String tSQL = "";

        // 查询处该处参加考核的所有人员
        //tLAAssessDB.setManageCom(this.mManageCom);
        //tLAAssessDB.setIndexCalNo(this.mIndexCalNo);
        //查询出满足前台条件的业务员进行条件归属---modifydate:20080312--songgh
        tSQL = "SELECT * FROM LAAssess WHERE IndexCalNo='" + this.mIndexCalNo +
               "'";
        tSQL += " AND ManageCom LIKE '" + this.mManageCom + "%'";

        tSQL += " AND  state = '01' ";

        tSQL += " AND BranchType = '" +
                this.mLAAssessHistorySchema.getBranchType() + "'";
        tSQL += " AND BranchType2 = '" +
                this.mLAAssessHistorySchema.getBranchType2() + "'";
        if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
        {
        	tSQL += " and agentcode = '"+this.mAgentCode+"'";
        }
        if(this.mAgentName!=null&&!this.mAgentName.equals(""))
        {
        	tSQL += " and agentcode = (select agentcode from laagent where name='"+this.mAgentName+"' " +
        			"  and branchtype='"+this.mLAAssessHistorySchema.getBranchType()+"' " +
        					"and branchtype2='"+this.mLAAssessHistorySchema.getBranchType2()+"')";
        }
        if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
        {
        	tSQL += " and branchattr = '"+this.mBranchAttr+"'";
        }
        if(this.mBranchName!=null && !this.mBranchName.equals(""))
        {
        	tSQL += " and branchattr = (select branchattr from labranchgroup where name='"+this.mBranchName+"' " +
        			"and branchtype='"+this.mLAAssessHistorySchema.getBranchType()+"'" +
        					" and branchtype2='"+this.mLAAssessHistorySchema.getBranchType2()+"')";
        }
        System.out.println("查询LAAssess SQL:" + tSQL);
        tLAAssessSet = tLAAssessDB.executeQuery(tSQL);
        System.out.println("参加考核人员有[ " + tLAAssessSet.size() + " ]位");
        if (tLAAssessDB.mErrors.needDealError()) {
            // @@错误处理
            CError.buildErr(this, "查询参加考核的人员信息失败！");
            return false;
        }
        if (tLAAssessSet.size() < 1) {
            // @@错误处理
            CError.buildErr(this, "没有找到参加考核的人员信息！");
            return false;
        }
        //取得参加考核人员人数
        tAgentCount = tLAAssessSet.size();

        //循环处理每个考核人员新职级归属
        for (int i = 1; i <= tAgentCount; i++) {
            tLAAssessSchema = tLAAssessSet.get(i).getSchema();
            if (!checkPer(tLAAssessSchema)) {
                return false;
            }
            if (!dealAgentEvaluate(tLAAssessSchema)) {
                // @@错误处理
                CError.buildErr(this, "进行人员归属操作时失败！");
                return false;
            }
            tLAAssessSchema.setState("02"); //考核归属完毕
            tLAAssessSchema.setModifyDate(this.currentDate);
            tLAAssessSchema.setModifyTime(this.currentTime);
            tLAAssessSchema.setOperator(this.mGlobalInput.Operator);
            mLAAssessSet.add(tLAAssessSchema);
        }
        return true;
    }

    /**
     * 对每个参加考核的人员进行逐个处理
     * @param pmLAAssessSchema LAAssessSchema
     * @return boolean
     */
    private boolean dealAgentEvaluate(LAAssessSchema pmLAAssessSchema) {
        String tModifyFlag = pmLAAssessSchema.getModifyFlag(); //升降级标记
        String tAgentGrade = pmLAAssessSchema.getAgentGrade(); //原职级
        String tCalAgentGrade = pmLAAssessSchema.getCalAgentGrade(); //新职级
        String tAssessAgentGrade = pmLAAssessSchema.getAgentGrade1(); //考核得到的职级
        String tFirstAssess = pmLAAssessSchema.getFirstAssessFlag();
        //判断业务员升降级分别进行处理 01:降级 02：维持 03：晋级
        // if ("01".equals(tModifyFlag) || "03".equals(tModifyFlag)) {
        // 处理清退职级业务员
        if ("D00".equals(tCalAgentGrade)) {
            //对清退业务员进行离职处理
            mLAAssessSet2.add(pmLAAssessSchema);
        }
        else if ("E00".equals(tCalAgentGrade))
        {
            mLAAssessSet2.add(pmLAAssessSchema);
        }
        else
        {
            //进行新职级归属，这里只有D01转正为同系列D01以上职级的情形
            mLAAssessSet1.add(pmLAAssessSchema);
        }

        //} else if ("02".equals(tModifyFlag)) {
        //    if ("D01".equals(tCalAgentGrade) && "D01".equals(tAgentGrade)&& tFirstAssess.equals("1")) {
        //      mLAAssessSet3.add(pmLAAssessSchema); //延期转正
        //  }
        //}
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        System.out.println(
                "Begin LAAgentGradeDecGrpYearBL.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        this.mLAAssessHistorySchema.setSchema((LAAssessHistorySchema)
                                              cInputData
                                              .getObjectByObjectName(
                "LAAssessHistorySchema", 0));
        this.mManageCom = mLAAssessHistorySchema.getManageCom();
        this.mIndexCalNo = mLAAssessHistorySchema.getIndexCalNo();
        this.mBranchAttr = (String)cInputData.get(2);
        this.mBranchName = (String)cInputData.get(3);
        this.mAgentCode = (String)cInputData.get(4);
        this.mAgentName = (String)cInputData.get(5);
        System.out.println("mAgentCode代理人姓名"+mAgentCode);
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecGrpYearBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (AgentPubFun.ConverttoYM(currentDate).compareTo(mIndexCalNo) <= 0) {
            // @@错误处理
            CError.buildErr(this, "考核年月不能大于或等于系统操作日期所在年月!");
            return false;
        }

        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql) {
        System.out.println(sql);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null) {
                return 0;
            }
            st = conn.prepareStatement(sql);
            if (st == null) {
                return 0;
            }
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
               if (!conn.isClosed()) {
                   conn.close();
               }
               try {
                   st.close();
                   rs.close();
               } catch (Exception ex2) {
                   ex2.printStackTrace();
               }
               st = null;
               rs = null;
               conn = null;
             } catch (Exception e) {}

        }
    }
}
