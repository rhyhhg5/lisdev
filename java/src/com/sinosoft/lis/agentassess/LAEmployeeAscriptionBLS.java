package com.sinosoft.lis.agentassess;

/**
 * <p>Title: 员工制组织归属</p>
 * <p>Description: 员工制组织归属</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author LL
 * @version 1.0
 */


import java.sql.Connection;

import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;
import com.sinosoft.lis.vdb.LATreeBDBSet;
import com.sinosoft.lis.vdb.LATreeDBSet;
import com.sinosoft.lis.vdb.LAWelfareInfoBDBSet;
import com.sinosoft.lis.vdb.LAWelfareInfoDBSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LAWelfareInfoBSet;
import com.sinosoft.lis.vschema.LAWelfareInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class LAEmployeeAscriptionBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;

    public LAEmployeeAscriptionBLS()
    {
    }

    public static void main(String[] args)
    {
        LAEmployeeAscriptionBLS LAEmployeeAscriptionBLS1 = new
                LAEmployeeAscriptionBLS();
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start LAEmployeeAscriptionBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLAEmployeeAscription(cInputData);
        }

        if (tReturn)
        {
            System.out.println(" Successful");
        }
        else
        {
            System.out.println("Save Failed");
        }
        System.out.println("End LAEmployeeAscriptionBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAEmployeeAscription(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAComBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        /*********   开始保存数据   ************/
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");

            //1 - 备份LATree表
            LATreeBSet tLATreeBSet = new LATreeBSet();
            tLATreeBSet.set((LATreeBSet) mInputData.getObject(2));
            if (tLATreeBSet.size() > 0)
            {
                System.out.println("备份LATree表 ");
                LATreeBDBSet tLATreeBDBSet = new LATreeBDBSet(conn);
                tLATreeBDBSet.set(tLATreeBSet);
                if (!tLATreeBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLATreeBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeAscriptionBLS";
                    tError.functionName = "saveLAEmployeeAscription";
                    tError.errorMessage = "备份LATree表数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            //2 - 修改LATree表
            LATreeSet tLATreeSet = new LATreeSet();
            tLATreeSet.set((LATreeSet) mInputData.getObject(1));
            if (tLATreeSet.size() > 0)
            {
                System.out.println("修改LATree表 ");
                LATreeDBSet tLATreeDBSet = new LATreeDBSet(conn);
                tLATreeDBSet.set(tLATreeSet);
                if (!tLATreeDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLATreeDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeAscriptionBLS";
                    tError.functionName = "saveLAEmployeeAscription";
                    tError.errorMessage = "修改LATree表数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            //3 - 备份LAWelfareInfo表中原有的数据
            LAWelfareInfoBSet tLAWelfareInfoBSet = new LAWelfareInfoBSet();
            tLAWelfareInfoBSet.set((LAWelfareInfoBSet) mInputData.getObject(5));
            if (tLAWelfareInfoBSet.size() > 0)
            {
                System.out.println("备份LAWelfareInfo表中原有的数据 ");
                LAWelfareInfoBDBSet tLAWelfareInfoBDBSet = new
                        LAWelfareInfoBDBSet(conn);
                tLAWelfareInfoBDBSet.set(tLAWelfareInfoBSet);
                if (!tLAWelfareInfoBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAWelfareInfoBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeAscriptionBLS";
                    tError.functionName = "saveLAEmployeeAscription";
                    tError.errorMessage = "备份LAWelfareInfo表数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            //3 - 删除LAWelfareInfo表中原有的数据
            LAWelfareInfoSet nLAWelfareInfoSet = new LAWelfareInfoSet();
            nLAWelfareInfoSet.set((LAWelfareInfoSet) mInputData.getObject(3));
            if (nLAWelfareInfoSet.size() > 0)
            {
                System.out.println("删除LAWelfareInfo表中原有的数据 ");
                LAWelfareInfoDBSet tLAWelfareInfoDBSet = new LAWelfareInfoDBSet(
                        conn);
                tLAWelfareInfoDBSet.set(nLAWelfareInfoSet);
                if (!tLAWelfareInfoDBSet.delete())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAWelfareInfoDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeAscriptionBLS";
                    tError.functionName = "saveLAEmployeeAscription";
                    tError.errorMessage = "删除LAWelfareInfo表数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            //4 - 往LAWelfareInfo表中插入新数据
            LAWelfareInfoSet mLAWelfareInfoSet = new LAWelfareInfoSet();
            mLAWelfareInfoSet.set((LAWelfareInfoSet) mInputData.getObject(4));
            if (mLAWelfareInfoSet.size() > 0)
            {
                System.out.println("往LAWelfareInfo表中插入新数据 ");
                LAWelfareInfoDBSet tLAWelfareInfoDBSet = new LAWelfareInfoDBSet(
                        conn);
                tLAWelfareInfoDBSet.set(mLAWelfareInfoSet);
                if (!tLAWelfareInfoDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAWelfareInfoDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAEmployeeAscriptionBLS";
                    tError.functionName = "saveLAEmployeeAscription";
                    tError.errorMessage = "插入LAWelfareInfo表数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            //6 - 修改LAAssessAccessory表
            LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
            tLAAssessAccessoryDB.setSchema((LAAssessAccessorySchema) mInputData.
                                           getObject(6));
            System.out.println("修改LAAssessAccessory表 ");
            if (!tLAAssessAccessoryDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAssessAccessoryDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAEmployeeAscriptionBLS";
                tError.functionName = "saveLAEmployeeAscription";
                tError.errorMessage = "修改LAAssessAccessory表数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeAscriptionBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            ;
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;

        return true;
    }
}
