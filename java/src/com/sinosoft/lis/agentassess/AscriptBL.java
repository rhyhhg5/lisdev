package com.sinosoft.lis.agentassess;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAssessMainSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LARecomRelationBSet;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AscriptBL
{
    public static CErrors mErrors = new CErrors();
    //private Connection mConn;
    MMap map = new MMap();

    public AscriptBL()
    {
    }

    private void dealErr(String cMsg, String cMName, String cFName)
    {
        CError tCError = new CError();
        tCError.moduleName = cMName;
        tCError.functionName = cFName;
        tCError.errorMessage = cMsg;
        mErrors.addOneError(tCError);
    }

    public boolean submitData(VData cInputData, int Perceed)
    {
        if (cInputData.size() <= 0)
        {
            dealErr("没有需要保存的数据！", "AscriptBL", "submitData");
            return false;
        }
        try
        {
            map = new MMap();
            //清退已调用离职处理接口作为独立事务处理完毕 ,所以只需要更新考核状态就行了
            if (Perceed != 0)
            {
                if (Perceed > 0)
                {
                    if (!submitTree(cInputData, Perceed)) {
                        return false;
                    }

                    if (!submitTreeB(cInputData, Perceed)) {
                        return false;
                    }
                    if (Perceed != 4) { //过程4没有这两个信息
                        if (!submitAgent(cInputData, Perceed)) {
                            return false;
                        }

                        if (!submitAgentB(cInputData, Perceed)) {
                            return false;
                        }
                    }
                }
                if (Perceed >= 2 && Perceed <= 3) {
                    if (!submitCommision(cInputData)) {
                        return false;
                    }

                    if (!submitBranchGroup(cInputData)) {
                        return false;
                    }

                    if (!submitBranchGroupB(cInputData)) {
                        return false;
                    }

                    if (!submitRearRelation(cInputData)) {
                        return false;
                    }

                    if (!submitRearRelationB(cInputData)) {
                        return false;
                    }
                    if (!submitRecomRelation(cInputData)) {
                        return false;
                    }

                    if (!submitRecomRelationB(cInputData)) {
                        return false;
                    }
                }
            }
            if (!submitAssessData(cInputData))
            {
                return false;
            }
            //mConn.commit();
            //mConn.close();
            VData mInputData = new VData();
            mInputData.add(map);
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start AscriptBL submit...");
            if (!tPubSubmit.submitData(mInputData, ""))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "AscriptBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        catch (Exception e)
        {
            dealErr(e.getMessage(), "AscriptBL", "submitData");
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        VData t = new VData();

        String name = t.toString();
        System.out.println(name);
    }

    private boolean submitTree(VData cInputData, int Perceed)
    {
        LATreeSet tLATreeSet = new LATreeSet();
        tLATreeSet.add((LATreeSet) cInputData.getObjectByObjectName("LATreeSet",
                0));
        if (tLATreeSet.size() > 0)
        {
            map.put(tLATreeSet, "UPDATE");
        }
        return true;
    }

    private boolean submitTreeB(VData cInputData, int Perceed)
    {
        LATreeBSet tLATreeBSet = new LATreeBSet();
        tLATreeBSet.add((LATreeBSet) cInputData.getObjectByObjectName(
                "LATreeBSet", 0));
        if (tLATreeBSet.size() > 0)
        {
            map.put(tLATreeBSet, "INSERT");
        }
        return true;

    }

    private boolean submitAgent(VData cInputData, int Perceed)
    {
        LAAgentSet tLAAgentSet = new LAAgentSet();
        //LAAgentDB tLAAgentDB = new LAAgentDB(mConn);
        tLAAgentSet.add((LAAgentSet) cInputData.getObjectByObjectName(
                "LAAgentSet", 0));
        if (tLAAgentSet.size() > 0 && tLAAgentSet.get(1).getAgentCode() != null &&
            !tLAAgentSet.get(1).getAgentCode().equals(""))
        {
            map.put(tLAAgentSet, "UPDATE");
            return true;
        }
        return true;
    }

    private boolean submitAgentB(VData cInputData, int Perceed)
    {
        LAAgentBSet tLAAgentBSet = new LAAgentBSet();
        //LAAgentBDB tLAAgentBDB = new LAAgentBDB(mConn);
        tLAAgentBSet.add((LAAgentBSet) cInputData.getObjectByObjectName(
                "LAAgentBSet", 0));
        if (tLAAgentBSet.size() > 0 && tLAAgentBSet.get(1).getAgentCode() != null &&
            !tLAAgentBSet.get(1).getAgentCode().equals(""))
        {
            map.put(tLAAgentBSet, "INSERT");
            return true;
        }
        return true;
    }
    private boolean submitCommision(VData cInputData)
    {
        LACommisionSet tLACommisionSet = new LACommisionSet();

        tLACommisionSet.add((LACommisionSet) cInputData.
                            getObjectByObjectName("LACommisionSet", 0));

        if (tLACommisionSet.size() > 0)
        {
            map.put(tLACommisionSet, "UPDATE");
        }
        VData srcData = new VData();
        srcData=(VData) cInputData.getObjectByObjectName("VData", 0);
        if (srcData.size() > 0)
        {
            for (int i=0;i<srcData.size();i++)
            {
                String tsql = (String) srcData.get(i);
                map.put(tsql, "UPDATE");
            }
        }
        return true;
    }

    private boolean submitBranchGroup(VData cInputData)
    {
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSet.add((LABranchGroupSet) cInputData.
                              getObjectByObjectName("LABranchGroupSet", 0));
        tLABranchGroupSchema.setSchema((LABranchGroupSchema) cInputData.
                                       getObjectByObjectName(
                "LABranchGroupSchema", 0));
        if (tLABranchGroupSet.size() > 0 &&
            tLABranchGroupSet.get(1).getAgentGroup() != null &&
            !tLABranchGroupSet.get(1).getAgentGroup().equals(""))
        {
            map.put(tLABranchGroupSet, "UPDATE");
        }
        if (tLABranchGroupSchema.getAgentGroup() != null &&
            !tLABranchGroupSchema.getAgentGroup().equals(""))
        { //可以没有新建立的机构
        	String groupName = "";
        	groupName=this.getGroupName(tLABranchGroupSchema.getBranchSeries(), tLABranchGroupSchema.getBranchAttr(),
        			tLABranchGroupSchema.getBranchLevel());
        	tLABranchGroupSchema.setName(groupName);
            map.put(tLABranchGroupSchema, "INSERT");
            // add new 
            if(!dealGBuild(tLABranchGroupSchema,tLABranchGroupSchema.getFoundDate()))
            {
            	return false;
            }
        }
        return true;
    }

    private boolean submitBranchGroupB(VData cInputData)
    {
        LABranchGroupBSet tLABranchGroupBSet = new LABranchGroupBSet();
        //LABranchGroupBDB tLABranchGroupBDB = new LABranchGroupBDB(mConn);
        tLABranchGroupBSet.add((LABranchGroupBSet) cInputData.
                               getObjectByObjectName("LABranchGroupBSet", 0));
        if (tLABranchGroupBSet.size() > 0 &&
            tLABranchGroupBSet.get(1).getAgentGroup() != null &&
            !tLABranchGroupBSet.get(1).getAgentGroup().equals(""))
        {
        	for(int i =1;i<=tLABranchGroupBSet.size();i++)
        	{
        		LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
        		tLABranchGroupBSchema = tLABranchGroupBSet.get(i);
        		 map.put(tLABranchGroupBSchema, "DELETE&INSERT");
        	}
//            map.put(tLABranchGroupBSet, "DELETE&INSERT");
        }
        return true;
    }

    private boolean submitRearRelation(VData cInputData)
    {
        LARearRelationSet tLARearRelationSetInsert = new LARearRelationSet();
        LARearRelationSet tLARearRelationSetUpdate = new LARearRelationSet();
        LARearRelationSet tLARearRelationSetDelete = new LARearRelationSet();
        //LARearRelationDB tLARearRelationDB = new LARearRelationDB(mConn);
        tLARearRelationSetInsert.add((LARearRelationSet) cInputData.
                                     getObjectByObjectName("LARearRelationSet",
                cInputData.size() - 5));
        tLARearRelationSetUpdate.add((LARearRelationSet) cInputData.
                                     getObjectByObjectName("LARearRelationSet",
                cInputData.size() - 4));
        tLARearRelationSetDelete.add((LARearRelationSet) cInputData.
                                     getObjectByObjectName("LARearRelationSet",
                cInputData.size() - 3));
        if (tLARearRelationSetDelete.size() > 0 &&
            tLARearRelationSetDelete.get(1).getAgentCode() != null &&
            !tLARearRelationSetDelete.get(1).getAgentCode().equals(""))
        {
            map.put(tLARearRelationSetDelete, "DELETE");
        }
        if (tLARearRelationSetUpdate.size() > 0 &&
            tLARearRelationSetUpdate.get(1).getAgentCode() != null &&
            !tLARearRelationSetUpdate.get(1).getAgentCode().equals(""))
        {
            map.put(tLARearRelationSetUpdate, "UPDATE");
        }
        if (tLARearRelationSetInsert.size() > 0 &&
            tLARearRelationSetInsert.get(1).getAgentCode() != null &&
            !tLARearRelationSetInsert.get(1).getAgentCode().equals(""))
        {
            map.put(tLARearRelationSetInsert, "INSERT");
        }
        return true;
    }

    private boolean submitRearRelationB(VData cInputData)
    {
        LARearRelationBSet tLARearRelationBSet = new LARearRelationBSet();
        tLARearRelationBSet.add((LARearRelationBSet) cInputData.
                                getObjectByObjectName("LARearRelationBSet", 0));
        if (tLARearRelationBSet.size() > 0 &&
            tLARearRelationBSet.get(1).getAgentCode() != null &&
            !tLARearRelationBSet.get(1).getAgentCode().equals(""))
        {
            map.put(tLARearRelationBSet, "INSERT");
        }
        return true;
    }
    private boolean submitRecomRelation(VData cInputData)
    {
        LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
        tLARecomRelationSet.add((LARecomRelationSet) cInputData.
                                getObjectByObjectName("LARecomRelationSet", 0));
        if (tLARecomRelationSet.size() > 0 &&
            tLARecomRelationSet.get(1).getAgentCode() != null &&
            !tLARecomRelationSet.get(1).getAgentCode().equals(""))
        {
            map.put(tLARecomRelationSet, "UPDATE");
        }
        return true;
    }
    private boolean submitRecomRelationB(VData cInputData)
    {
        LARecomRelationBSet tLARecomRelationBSet = new LARecomRelationBSet();
        tLARecomRelationBSet.add((LARecomRelationBSet) cInputData.
                                getObjectByObjectName("LARecomRelationBSet", 0));
        if (tLARecomRelationBSet.size() > 0 &&
            tLARecomRelationBSet.get(1).getAgentCode() != null &&
            !tLARecomRelationBSet.get(1).getAgentCode().equals(""))
        {
            map.put(tLARecomRelationBSet, "INSERT");
        }
        return true;
    }

    private boolean submitAssessData(VData mInputData)
    {
        LAAssessSchema tLAAssessSchema;
        tLAAssessSchema = (LAAssessSchema) mInputData.
                                   getObjectByObjectName(
                "LAAssessSchema",
                0);
        LAAssessMainSchema tLAAssessMainSchema;
        tLAAssessMainSchema = (LAAssessMainSchema) mInputData.
                              getObjectByObjectName("LAAssessMainSchema", 0);
        try
        {
            if (tLAAssessSchema != null)
            {
                map.put(tLAAssessSchema, "UPDATE");
                //return true;
            }
            if (tLAAssessMainSchema != null)
            {
                map.put(tLAAssessMainSchema, "UPDATE");
            }
            return true;
        }
        catch (Exception ex)
        {
            dealErr(ex.getMessage(), "AscriptBL", "submitAssessData");
            return false;
        }
    }
	private boolean dealGBuild(LABranchGroupSchema tLABranchGroupSchema,String tFoundDate)
	{
		String tAscriptDate = AgentPubFun.formatDate(tFoundDate,"yyyy-MM-dd");//变更下条件
		String tEndDate = getGBuildEnd(tAscriptDate);

			String tBranch = tLABranchGroupSchema.getBranchSeries().substring(0, 12);//得到营业部编码
			
			LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
			LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
			tLABranchGroupDB.setAgentGroup(tBranch);
			if (!tLABranchGroupDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustAgentBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "不存在所要修改的机构!";
                this.mErrors.addOneError(tError);
                return false;
            }
			tLABranchGroupSet=tLABranchGroupDB.query();
			LABranchGroupSchema t_LABranchGroupSchema = new LABranchGroupSchema();//存营业部编码
			t_LABranchGroupSchema=tLABranchGroupSet.get(1);
			if(tLABranchGroupSchema.getFoundDate().compareTo("2013-04-01")>=0&&tLABranchGroupSchema.getFoundDate().compareTo("2014-04-01")<0)
			{
				if(t_LABranchGroupSchema.getApplyGBFlag()!=null&&t_LABranchGroupSchema.getApplyGBFlag().equals("Y"))
				{
					String sql = "update labranchgroup set applygbflag= 'Y',applygbstartdate='"+tAscriptDate+"',modifydate= current date,modifytime = current time where agentgroup = '"+tLABranchGroupSchema.getAgentGroup()+"'";
					if(t_LABranchGroupSchema.getGBuildFlag()!=null)
					{
						sql = "update labranchgroup set applygbflag= 'Y',applygbstartdate='"+tAscriptDate+"',gbuildflag = '"+t_LABranchGroupSchema.getGBuildFlag()+"',gbuildstartdate = '"+tAscriptDate+"',gbuildenddate = '"+tEndDate+"',modifydate= current date,modifytime = current time where agentgroup = '"+tLABranchGroupSchema.getAgentGroup()+"'";		
					}
					map.put(sql, "UPDATE");
				}				
			}	
		return true;
	}
    /**
     *  得到考核截止日期
     * @param GBuildStartDate
     * @return
     */
    private String getGBuildEnd(String GBuildStartDate)
    {
    	String tGBuildEndDate ="";
    	 String tsql = "select enddate from LASTATSEGMENT where char(yearmonth) = db2inst1.DATE_FORMAT(((date('"+GBuildStartDate+"') + 1 year) - 1 month),'yyyymm') and  stattype = '1'";
		  ExeSQL tExeSQL = new ExeSQL();
		  tGBuildEndDate =tExeSQL.getOneValue(tsql);
		  if(tGBuildEndDate==null||tGBuildEndDate.equals(""))
		  {
			   CError tError = new CError();
	           tError.moduleName = "LABranchGroupBuildBL";
	           tError.functionName = "check";
	           tError.errorMessage = "团队建设起期格式录入有误!";
	           this.mErrors.addOneError(tError);
	           return "";  
		  }
    	return tGBuildEndDate;
    }

    
    /**
     * 功能：生成新建机构名称
     * GetGroupName  生成新建团队的名称，
     *  @param branchseries 团队序列
     *  @param branchAttr 所在机构编码
     *  @param branchLevel 机构级别
     */
    private String getGroupName(String branchseries,String branchAttr,String branchLevel) {
		//机构名
    	String groupName = "";
		//机构基本
    	String grade = "";
    	//上级机构团队编码
		String agentgroup = "";
		//上级团队名称
		String upbranchName = "";
		//机构的排序号
		String numstr ="";
		//获得上级机构所在团队编号
		String[] agentgroups = branchseries.split(":");
		if(agentgroups.length==2){
			agentgroup = agentgroups[0];
			numstr = branchAttr.substring(10);
		}else if(agentgroups.length==3){
			agentgroup = agentgroups[1];
			if(agentgroups[1].equals("000000000000")){
				agentgroup = agentgroups[0];
			}
			numstr = branchAttr.substring(12);
		}
		//使用SQL查询出上级机构名称
		ExeSQL tExeSQL=new ExeSQL();
        SSRS tSSRS=new SSRS();
        tSSRS=tExeSQL.execSQL("select name from LABRANCHGROUP where agentgroup='"+agentgroup+"'");
        if(tSSRS.MaxRow>0)
        {
        	upbranchName=tSSRS.GetText(1,1);
        }
		System.out.println(upbranchName);
		//获得该机构编号
		int num = Integer.parseInt(numstr);
		//确定机构级别
		if(branchLevel.equals("02")){
			grade = "区";
		}else if(branchLevel.equals("01")){
			grade = "处";
		}
		//连接字符,拼接到一起组成团队名称
		groupName = upbranchName + this.convertToChnNum(num) + grade;
		System.out.println("AscriptBL" + groupName);
		return groupName;
	}
    
    //
    /**
     * 功能：将阿拉伯数字转化成中文数字
     * @param num  要转化的数字
     */
    private String convertToChnNum(int num){
		String[] ChnNum={"零","一","二","三","四","五","六","七","八","九","十","百","千","万"};
	    String chnnum = "";
	    if(num<10 && num>0){
	    	chnnum = ChnNum[num];
	    }else if(num>=10 && num<100){
	    	String tensdigit = "";
	    	if(num>=20){
	    		tensdigit = ChnNum[num/10];
	    	}
	        String unitsdigit = "";
	        if(num%10!=0){
	        	unitsdigit = ChnNum[num%10];
	        }
	    	chnnum = tensdigit+ChnNum[10]+unitsdigit;
	    }else if(num>=100 && num<1000){
	        String hundredths = "";
	        hundredths = ChnNum[num/100];
	        String numstr = this.convertToChnNum(num%100);
	        if(num%100<10 && num%100>0){
	        	numstr = ChnNum[0]+numstr;
	        }
	        if(num%100>=10 && num%100<20){
        		numstr = ChnNum[1]+numstr;
        	}
	        chnnum = hundredths+ChnNum[11]+numstr;
	    }else if(num>=1000 && num<10000){
	    	 String kilobit = ChnNum[num/1000];
	    	 String numstr =this.convertToChnNum(num%1000); 
	    	 if(num%1000>=10 && num%1000<20){
	    		 numstr = ChnNum[1]+numstr;
	    	 }
	    	 if(num%1000<100 && num%1000>0){
	    		 numstr = ChnNum[0]+numstr;
	    	 }
	    	 chnnum = kilobit +ChnNum[12]+numstr;
	    }
		return chnnum;
	}
}
