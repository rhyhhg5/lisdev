package com.sinosoft.lis.agentassess;

/**
 * <p>Title: AscriptRearQueryDealBL类</p>
 *
 * <p>Description: 处理代理人归属时的血缘关系信息的查询</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author zxs 200506
 * @version 1.0
 */
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.agent.LARearPICCHBL;
import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.vschema.LAAssessSet;

public class AscriptRearQueryDealBL {

    public CErrors mErrors = new CErrors();
    public GlobalInput mGlobalInput = new GlobalInput();//操作员	t信息

    private String mOldGradeEndDate; //降级人员原职级结束	t日期，槁BLF传入
    private String mNewGradeStartDate; //降级人员新职级开始	t日期，槁BLF传入
    private VData mOutputData=new VData() ; //保存向外层传递	t数据
    private String mIndexCalNo; //考核计\uFFFD	t编 ，槁BLF传入

    //private int RoleBackCount=0;//记录递归次数
    private LAAgentSet mLAAgentSet=new LAAgentSet();
    private LATreeSet mLATreeSet=new LATreeSet();
    private LABranchGroupDB mLABranchGroupDB=new LABranchGroupDB();


    public AscriptRearQueryDealBL() {
    }

    public boolean getInputData(VData cInputData)
    {
        mOldGradeEndDate=(String)cInputData.get(0);
        mNewGradeStartDate=(String)cInputData.get(1);
        mGlobalInput=(GlobalInput)cInputData.getObjectByObjectName(
                     "GlobalInput",2);
        mIndexCalNo=(String)cInputData.get(3);
        if(mNewGradeStartDate!=null&&!mNewGradeStartDate.equals("")
                &&
           mOldGradeEndDate!=null&&!mOldGradeEndDate.equals("")
                &&
           mGlobalInput!=null
                &&
           mIndexCalNo!=null&&!mIndexCalNo.equals(""))
        {
            return true;
        }
        return false;

    }

    private boolean prepareOutputDataToBLS()
    {
        return true;
    }

    public VData getOutputDataToBLS()
    {
        if(!prepareOutputDataToBLS())
        {
            return null;
        }
        if(mOutputData!=null||mOutputData.size()>0)
        {
            return mOutputData;
        }
        return null;

    }

    private void DealErr(String cMsg,String cMName,String cFName)
    {
        CError tCError=new CError();
        tCError.moduleName=cMName;
        tCError.functionName=cFName;
        tCError.errorMessage=cMsg;
        mErrors.addOneError(tCError);
    }

    /*
     新行销部经理招揽育成人和间接育成人作为部下
     参数：cAgentCode-新晋升的部经理的人员编码 cAgentGroup-新晋升的部经理的直辖原来所属部
     */
    public boolean CollectRearAgent(String cAgentCode,String cAgentGroup,String cBranchType,String cBranchType2)
    {
        String SQLstr;
        LAAgentDB tLAAgentDB=new LAAgentDB();
        LATreeDB tLATreeDB=new LATreeDB();
        SQLstr="Select * from laagent where exists "+
                      "(select agentcode from larearrelation where "+
                      "agentcode=laagent.agentcode and rearagentcode ='"+ cAgentCode +
                      "' and rearlevel='01' "+
//                      " and rearflag='1' "+
                    //  " and (state<='3' or state is null) "+
//                      "and exists (select agentgroup from labranchgroup where "+
//                      "agentgroup = larearrelation.agentgroup and upbranch ='"+ cAgentGroup +
//                      "')
                     " and exists (select agentgrade from latree where "+
                      "agentcode = larearrelation.agentcode and agentseries<'2' )) "+
                      " and  agentstate<='03' "+
                      "and not exists (select agentcode from laassess where " +
                      "branchtype='"+cBranchType+"' and branchtype2='"+cBranchType2+"' " +
                      "and agentcode=laagent.agentcode and state='1' and indexcalno='"+mIndexCalNo+"' "+
                      "  and agentseries1>agentseries and agentseries1='2') " +
                      "order by laagent.agentcode";
        System.out.println("CollectRearAgent中查询直接育成组长的基本信息的SQL： "+ SQLstr);
        mLAAgentSet.add(tLAAgentDB.executeQuery(SQLstr));
        if(tLAAgentDB.mErrors.needDealError())
        {
            DealErr(tLAAgentDB.mErrors.getLastError(),"AscriptRearQueryDealBL","CollectRearAgent");
            return false;
        }


        SQLstr="Select * from latree where    agentseries<'2' and exists "+
               " (select agentcode from laagent where "+
               "agentcode = latree.agentcode and agentstate<='03' ) and  exists"+
               "(select agentcode from larearrelation where agentcode=latree.agentcode "+
               "and rearagentcode = '"+cAgentCode+"' and rearlevel='01' ) "+
//               " and rearflag='1')  "+
//               " exists (select agentgroup from labranchgroup "+
//               "where agentgroup = larearrelation.agentgroup and upbranch ='"+cAgentGroup+
//               "')) and "+
               " and not exists (select agentcode from laassess where "+
               "branchtype='"+cBranchType+"' and branchtype2='"+cBranchType2+"' " +
               "and agentcode=latree.agentcode and state='1' and indexcalno='"+mIndexCalNo+"' "+
               "  and agentseries1>agentseries and agentseries1='2') " +
               "order by latree.agentcode";
        System.out.println("CollectRearAgent中查询间接育成组长的行政信息的SQL： "+ SQLstr);
        mLATreeSet.add(tLATreeDB.executeQuery(SQLstr));
        if(tLATreeDB.mErrors.needDealError())
        {
            DealErr(tLATreeDB.mErrors.getLastError(),"AscriptRearQueryDealBL","CollectRearAgent");
            return false;
        }
        if(mLAAgentSet.size()!=mLATreeSet.size())
        {
            DealErr("系统中代理人的行政信息和基本信息有异常！","AscriptRearQueryDealBL","CollectRearAgent");
            return false;
        }
        return true;
    }

    /*
     新组主任招揽增援人和间接增援人作为部下、
     参数：cAgentCode-新晋升的主任的人员编码  cAgentGroup-组长原来所属的组
       主管职级的人员不能带走
     */
    public boolean CollectIntroAgent(String cAgentCode, String cAgentGroup,String cBranchType,String cBranchType2)
    {
        String SQLstr;
        LAAgentDB tLAAgentDB=new LAAgentDB();
        LATreeDB tLATreeDB=new LATreeDB();
        LAAgentSet tLAAgentDBSet=new LAAgentSet();
        LATreeSet tLATreeDBSet=new LATreeSet();

        {//求出直接增援人
            SQLstr = "Select * from laagent where exists " +
                     "(Select agentcode from larecomrelation where agentcode=laagent.agentcode and " +

                     "recomagentcode='" + cAgentCode +  "' and recomflag='1' ) "+
                     "and not exists (select agentcode from laassess where "+
                     "branchtype='"+cBranchType+"' and branchtype2='"+cBranchType2+"' " +
                     "and agentcode=laagent.agentcode and state='1' and indexcalno='"+mIndexCalNo+"' "+
                     "  and ((agentseries1>agentseries and   agentgrade1<>'B01')  or ( agentgrade ='B01' and   agentgrade1='B02'))) " +
                     " and  exists (select 1 from latree where agentcode=laagent.agentcode and "+
                     " (agentseries<'1' or agentgrade='B01') )  "+
                     "order by laagent.agentcode";
            System.out.println("CollectIntroAgent中的求出直接增援人基本信息的SQL: "+ SQLstr);
            tLAAgentDBSet.add(tLAAgentDB.executeQuery(SQLstr));
            //去掉不能带走的人增援的人

            /**/

            if (tLAAgentDB.mErrors.needDealError()) {
                DealErr(tLAAgentDB.mErrors.getLastError(),
                        "AscriptRearQueryDealBL", "CollectIntroAgent");
                return false;
            }
            SQLstr =
                    //"Select * from latree where (state is null or state<='3') and agentseries<'1' and " +
                    //"Select * from latree where agentseries<'1' and " +
                    "Select * from latree where exists " +
                     "(Select agentcode from larecomrelation where agentcode=latree.agentcode and " +

                     "recomagentcode='" + cAgentCode +  "' and recomflag='1' ) "+

                    "and not exists (select agentcode from laassess where "+
                    "branchtype='"+cBranchType+"' and branchtype2='"+cBranchType2+"' " +
                    "and agentcode=latree.agentcode and state='1' and indexcalno='"+mIndexCalNo+"' "+
                    "and   ((agentseries1>agentseries and   agentgrade1>'B01') or ( agentgrade ='B01' and   agentgrade1='B02')) ) " +
                    " and (agentseries<'1' or agentgrade='B01') " +
                    "order by latree.agentcode";
            System.out.println("CollectIntroAgent中的求出直接增援人行政信息的SQL: "+ SQLstr);
            tLATreeDBSet.add(tLATreeDB.executeQuery(SQLstr));
            //去掉不能带走的人增援的人
            /**/
            if (tLATreeDB.mErrors.needDealError()) {
                DealErr(tLATreeDB.mErrors.getLastError(),
                        "AscriptRearQueryDealBL", "CollectIntroAgent");
                return false;
            }
            if(tLATreeDBSet.size()!=tLAAgentDBSet.size())
            {
                DealErr("查询"+cAgentCode+"存在推荐关系的人数不一致！","AscriptRearQueryDealBL","CollectIntroAgent");
                        return false;
            }
            for(int i=1;i<=tLATreeDBSet.size();i++)
            {//循环将离职确认的、 已经晋升的人挑出去
                String tAgentCode1=tLAAgentDBSet.get(i).getAgentCode();
                String tAgentCode2=tLATreeDBSet.get(i).getAgentCode();
                if(!tAgentCode2.equals(tAgentCode1))
                {
                    DealErr("查询"+cAgentCode+"存在推荐关系出错！行政信息与基础信息代理人不一致","AscriptRearQueryDealBL","CollectIntroAgent");
                        return false;
                }
                String tState=tLAAgentDBSet.get(i).getAgentState().trim();
                String tBranchCode=tLATreeDBSet.get(i).getBranchCode();
                String tAgentSeries=tLATreeDBSet.get(i).getAgentSeries().trim();
                String tAgentGrade = tLATreeDBSet.get(i).getAgentGrade().trim();
                if(
                        (tState==null||tState.equals("")||tState.compareToIgnoreCase("03")<=0)

                        &&
                        (tAgentSeries.compareToIgnoreCase("1")<0 || tAgentGrade.equals("B01"))
                   )
                {
                   //A->B->C,如果A与B都升为主管,查询到A的推荐人 时，不能把C调走，
                   //因为C的推荐人B也为主管了，所以C 要调动B团队下
                    String tSQL =
                            "select recomgens from larecomrelation where agentcode='" +
                            tLAAgentDBSet.get(i).getAgentCode() +
                            "' and recomagentcode='" +
                            cAgentCode + "' and recomgens<>1";
                    ExeSQL tExeSQL = new ExeSQL();
                    String trecomgens = tExeSQL.getOneValue(tSQL);
                    if (tExeSQL.mErrors.needDealError())
                    {
                        DealErr("查询推荐关系出现错误！","AscriptRearQueryDealBL","CollectIntroAgent");
                        return false;
                    }
                    if(trecomgens==null || trecomgens.equals(""))
                    {
                        mLATreeSet.add(tLATreeDBSet.get(i));
                        mLAAgentSet.add(tLAAgentDBSet.get(i));
                    }
                    else
                    {

                        tSQL =
                                "select agentcode from larecomrelation a  where a.agentcode='" +
                                tLAAgentDBSet.get(i).getAgentCode() +
                                "' and recomgens<"  + trecomgens +  //查询比本次推荐级别近的推荐关系
                                "  and  exists (select agentcode from laassess where " +
                                " agentcode = a.recomagentcode and " +
                                " ((agentseries1>agentseries and agentgrade1<>'B01') " +
                                " or  (agentgrade='B01' and agentseries1>='1') or (agentseries='1' and agentseries1='1'and agentgrade1<>'B01')))";
                        tExeSQL = new ExeSQL();
                        String tAgentCode=tExeSQL.getOneValue(tSQL);
                        if (tExeSQL.mErrors.needDealError())
                        {
                            DealErr("查询推荐关系出现错误！","AscriptRearQueryDealBL","CollectIntroAgent");
                            return false;
                        }
                        if(tAgentCode==null || tAgentCode.equals(""))
                        {
                            mLATreeSet.add(tLATreeDBSet.get(i));
                            mLAAgentSet.add(tLAAgentDBSet.get(i));
                        }
                    }
                }
            }
        }
        if(mLAAgentSet.size()!=mLATreeSet.size())
        {
            DealErr("系统中代理人的行政信息和基本信息有异常！","AscriptRearQueryDealBL","CollectIntroAgent");
            return false;
        }
//        else if(tLATreeDBSet.size()>0)
//        {//查到就去递归
//            for(int i=1;i<=tLATreeDBSet.size();i++)
//            {
//                if(!CollectIntroAgent(tLATreeDBSet.get(i).getAgentCode(),cAgentGroup,cBranchType,cBranchType2))
//                {
//                    return false;
//                }
//            }
//        }

        return true;
    }

    /*
     得到部下的信息集
     */
    public VData getAgentSet()
    {
        VData trVData=new VData();
        trVData.add(mLATreeSet);
        trVData.add(mLAAgentSet);
        return trVData;
    }

    public boolean BackToRearGroup(String cAgentCode, LABranchGroupSchema cLABranchGroupSchema,String cNewSeries,String cNewGrade)
    {
        String AgentGroup = "";//育成人的团队
        String tRearAgentCode = "";//育成人
        String RearLevel="";
        if(cNewSeries.equals("0"))
        {
            RearLevel="01";
        }
        else if(cNewSeries.equals("1"))
        {
            RearLevel="02";
        }
        LARearRelationSet tLARearRelationSet= new LARearRelationSet();
        LARearPICCHBL tLARearPICCHBL= new LARearPICCHBL();
        tLARearRelationSet = tLARearPICCHBL.getDirectUpRelation(cAgentCode);
        if(tLARearRelationSet != null && tLARearRelationSet.size()>=1)
        {
            LARearRelationSchema tLARearRelationSchema = new
                    LARearRelationSchema();
            tLARearRelationSchema = tLARearRelationSet.get(1);
            tRearAgentCode = tLARearRelationSchema.getRearAgentCode();
            String tRearLevel = tLARearRelationSchema.getRearLevel();

            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(tRearAgentCode);
            if (!tLATreeDB.getInfo()) {
                DealErr("代理人" + cAgentCode + "的育成人" + tRearAgentCode +
                        "行政信息查询出错! ", "AscriptRearQueryDealBL",
                        "BackToRearGroup");
                return false;
            }
            if (cNewSeries.equals("1")&&!cNewGrade.equals("B01")) { //从区到处的降级,团队归属到育成区经理所在区
                AgentGroup = tLATreeDB.getAgentGroup();
            } else { //从处到业务系列的降级,团队所有人归属到育成代理人的处(如果育成人是区则为区直辖处)
                AgentGroup = tLATreeDB.getBranchCode();
            }
        }
        else
        {//如果没有直接育成人；则返回
            return true ;
        }
        mLABranchGroupDB.setAgentGroup(AgentGroup);
        if(!mLABranchGroupDB.getInfo())
        {
            mLABranchGroupDB = null;
            DealErr("代理人" + cAgentCode + "的育成人" + tRearAgentCode +
                    "团队信息查询出错! ", "AscriptRearQueryDealBL",
                    "BackToRearGroup");
            return false;
        }

        String BranchLevel=cLABranchGroupSchema.getBranchLevel();
        String RearBranchLevel=mLABranchGroupDB.getBranchLevel();
        //育成团队必须与被育成团队级别相同,才校验考核信息(如果是区与处的育成关系, 则处降级后还要归属到育成团队)
        if(BranchLevel.equals(RearBranchLevel))//
        { //如果育成团队的级别相同,查询育成人的本次考核情况,如果他也降级到与他同一级别,则不能归属被育成团队
            String tSql = "select * from laassess where agentcode='" +
                          tRearAgentCode +
                          "' and indexcalno='" + mIndexCalNo +
                          "' and  modifyflag='01'" +
                          " and agentseries1<agentseries ";
            
            LAAssessDB  tLAAssessDB= new   LAAssessDB();
            LAAssessSet  tLAAssessSet= new   LAAssessSet();
            tLAAssessSet=tLAAssessDB.executeQuery(tSql);
            if(tLAAssessSet!=null && tLAAssessSet.size()>=1)
            {
                mLABranchGroupDB=null;
            }
        }
        return true;
    }

    /*
     为降级的主管找应回归的机构
     参数：cAgentCode-要回归的代理人编码  cLABranchGroupSchema-cAgentCode的原属机构 cNewSeries-代理人的新系列
     返回：true，但是成员变量mLABranchGroupDB为空-公司统一归属   true而且成员变量mLABranchGroupDB不为空-成功找到回归组  false-失败
     */
    public boolean BackToGroup(String cAgentCode, LABranchGroupSchema cLABranchGroupSchema,String cNewSeries)//,String cAgentGroup, String cManageCom)
    {
        LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
        ExeSQL tExeSQL=new ExeSQL();
        SSRS treeSSRS=new SSRS();
        String RearLevel;
        String SQLstr;
        String AgentGroup="NOTFIND";//找到回归机构的编码，初始为没找到
        int i=0;//循环变量
        if(cNewSeries.equals("0"))
        {
            RearLevel="01";
        }
        else if(cNewSeries.equals("1"))
        {
            RearLevel="02";
        }
        else
        {
            DealErr("代理人"+cAgentCode+"新系列信息异常！","AscriptRearQueryDealBL","BackToGroup");
            return false;
        }
        //取回与cAgentCode在同一机构，现在与其有关系（父方），而且目前在职的人员的行政信息
        SQLstr="select latree.agentcode,latree.agentgroup,latree.upagent,latree.branchcode,latree.agentseries from latree,larearrelation "
               + "where (latree.state<'3' or latree.state is null) and "
               + "latree.managecom='"+cLABranchGroupSchema.getManageCom()+"' and larearrelation.rearagentcode=latree.agentcode "
               + "and larearrelation.agentcode='"+ cAgentCode +"' and larearrelation.rearlevel='"+RearLevel+"' "
               //+ "and (larearrelation.enddate is null or larearrelation.enddate='') "
//               + "and larearrelation.rearflag='1' "
               + "and exists(select 'Y' from labranchgroup where ((larearrelation.rearlevel = '02' and agentgroup=latree.agentgroup) or (larearrelation.rearlevel = '01' and agentgroup=latree.branchcode) ) and upbranch='"+cLABranchGroupSchema.getUpBranch()+"' )"
               //增加考核处理缺少“业务主任降级回归原育成人所辖组时，原育成人与降级的业务主任不在同一行销部时，降级业务员直接回归所在行销部经理直辖组”的情况处理
               + "order by larearrelation.rearedgens asc, larearrelation.startdate desc with ur";

        System.out.println("BackToGroup中的查询直接关系的SQL： "+ SQLstr);
        treeSSRS=tExeSQL.execSQL(SQLstr);
        if(tExeSQL.mErrors.needDealError())
        {
            DealErr(tExeSQL.mErrors.getLastError(),"AscriptRearQueryDealBL","BackToGroup");
            return false;
        }
        if(treeSSRS.MaxRow>0)
        {
            for(i=1;i<=treeSSRS.MaxRow;i++)
            {
                if(RearLevel.equals("02"))
                {//找的是增部人，那么直接回到增部人的部
                    AgentGroup=treeSSRS.GetText(i,2);//tLATreeDB.getAgentGroup();
                    break;
                }
                else if(treeSSRS.GetText(i,5)/*getUpAgent()*/!=null&&!treeSSRS.GetText(i,5)/*getUpAgent()*/.equals("2"))
                {//找到的是育成人，而且育成人是一个组长，
                    SSRS branchR=new SSRS();
                    SQLstr="select BranchManager from labranchgroup where agentgroup='"+cLABranchGroupSchema.getUpBranch()+"'";
                    branchR=tExeSQL.execSQL(SQLstr);
                    if(tExeSQL.mErrors.needDealError()||branchR.MaxRow<=0)
                    {
                        DealErr("查找代理人"+cLABranchGroupSchema.getBranchManager()+"所在的部失败！","AscriptRearQueryDealBL","BackToGroup");
                        return false;
                    }
//                    if(treeSSRS.GetText(i,3).equals(branchR.GetText(1,1)))
//                    {//又和本人在同一个部下，那么直接返回育成人的组
//                        AgentGroup = treeSSRS.GetText(i, 2); //getAgentGroup();
//                        break;
//                    }
                    String rearAgentGroup=treeSSRS.GetText(i,2);
                    SQLstr="select upbranch from labranchgroup where agentgroup='"+rearAgentGroup+"'";
                    String rearUpBranch=tExeSQL.getOneValue(SQLstr) ;
//                    if (rearUpBranch==null || rearUpBranch.trim() .equals("") )
//                    {
//                        rearUpBranch
//                    }
                    if (cLABranchGroupSchema.getUpBranch() .equals(rearUpBranch) )
                    {
                        AgentGroup=treeSSRS.GetText(i,2) ;
                        break;
                    }
                }
                else
                {//找到的是育成人，但是育成人是一个部长，直接回到育成人的直辖组
                    AgentGroup=treeSSRS.GetText(i,4)/*getBranchCode()*/;
                    break;
                }
            }
        }
        if(AgentGroup.equals("NOTFIND"))//现存关系不存在或者现存关系用不上
        {
            //从关系备份表中取回与cAgentCode在同一机构，曾经与其有关系（父方），而且目前在职的人员的行政信息,代数第一序增序，关系断绝日期第二序降序
            SQLstr="select latree.agentcode,latree.agentgroup,latree.upagent,latree.branchcode,latree.agentseries from latree,larearrelationb "
               + "where (latree.state<'3' or latree.state is null) and latree.agentseries='"+RearLevel.substring(1,2)+"' and "
               + "latree.managecom='"+cLABranchGroupSchema.getManageCom()+"' and larearrelationb.rearagentcode=latree.agentcode "
               + "and larearrelationb.agentcode='"+ cAgentCode +"' and larearrelationb.rearlevel='"+RearLevel+"' "
               //+ "and larearrelation.enddate is  not null "
//               + "and larearrelationb.rearflag='1' "//备份的是存在时的信息
               + "and exists(select 'Y' from labranchgroup where ((larearrelationb.rearlevel = '02' and agentgroup=latree.agentgroup) or (larearrelationb.rearlevel = '01' and agentgroup=latree.branchcode) ) and upbranch='"+cLABranchGroupSchema.getUpBranch()+"' )"
               //增加考核处理缺少“业务主任降级回归原育成人所辖组时，原育成人与降级的业务主任不在同一行销部时，降级业务员直接回归所在行销部经理直辖组”的情况处理
               + "order by larearrelationb.rearedgens asc, larearrelationb.enddate desc with ur";


           System.out.println("BackToGroup中的查询间接关系的SQL： "+ SQLstr);
           treeSSRS.Clear();
           treeSSRS=tExeSQL.execSQL(SQLstr);
           if (tExeSQL.mErrors.needDealError())
           {
               DealErr(tExeSQL.mErrors.getLastError(),
                    "AscriptRearQueryDealBL", "BackToGroup");
               return false;
           }
           if(treeSSRS.MaxRow>0)
           {
               for(i=1;i<=treeSSRS.MaxRow;i++)
               {
                   tLABranchGroupDB.setAgentGroup(treeSSRS.GetText(i,2)/*tLATreeDB.getAgentGroup()*/);
                   if(!tLABranchGroupDB.getInfo())
                   {
                        DealErr("取代理人"+treeSSRS.GetText(i,1)/*tLATreeDB.getAgentCode()*/+"	机构信息出错！","AscriptRearQueryDealBL","BackToGroup");
                        return false;
                   }
                   if(RearLevel.equals("02"))
                   {//找的是曾经的增部人
                       //if(tLABranchGroupDB.getUpBranch()!=null&&!tLABranchGroupDB.getUpBranch().equals(""))
                       if(treeSSRS.GetText(i,5)/*getUpAgent()*/!=null&&!treeSSRS.GetText(i,5)/*getUpAgent()*/.equals("2"))
                       {//原增部人已经降为组长了，那么回归这个增部人所在的部
                           AgentGroup=tLABranchGroupDB.getUpBranch();
                           break;
                       }
                       else
                       {//原增部人还是部长，那么回归这个增部人所辖的部
                           AgentGroup=tLABranchGroupDB.getAgentGroup();
                           break;
                       }
                   }
                   else
                   {//找的是曾经的育成人
                       if(tLABranchGroupDB.getBranchLevel().equals("01"))
                       {//这个育成人还是组长
                           if(tLABranchGroupDB.getUpBranch().equals(cLABranchGroupSchema.getUpBranch()))
                           {//这个育成人和要回归的人在同一个部下,那么直接回到这个育成人的组
                               AgentGroup = treeSSRS.GetText(i, 4)/*getBranchCode()*/;
                               break;
                           }
                       }
                       else if(tLABranchGroupDB.getBranchLevel().equals("02"))
                       {//这个育成人已经是部长了
                           if(tLABranchGroupDB.getAgentGroup().equals(cLABranchGroupSchema.getUpBranch()))
                           {//这个育成人正好是要回归的人的部长,那么直接回到这个育成人的直辖组
                               AgentGroup = treeSSRS.GetText(i, 4);//getAgentGroup();
                               break;
                           }
                       }
                   }
               }
           }
           else
           {
               /*说明没有人和cAgentCode有关系，由公司统一安排*/
               if (RearLevel.equals("01")) { //如果是组没有找到关系，回到当前所在部的直辖组
                   String strSQL =
                           "select agentgroup,branchmanager from labranchgroup where upbranch= '" +
                           cLABranchGroupSchema.getUpBranch() +
                           "' and UpBranchAttr='1' and endflag<>'Y' ";
                   System.out.println("查询组长所在部的直辖组的SQL：" + strSQL);
                   SSRS agentgroupR = new SSRS();
                   agentgroupR = tExeSQL.execSQL(strSQL);
                   if (agentgroupR.MaxRow <= 0) {
                       //DealErr("查询组长"+cAgentCode+"所在部的直辖组信息失败！","AscriptRearQueryDealBL","BackToGroup");
                       //return false;
                       return true;
                   }
                   AgentGroup = agentgroupR.GetText(1, 1);
                   String AgentCode = agentgroupR.GetText(1, 2);
                   if (AgentCode == null || AgentCode.equals("")) { //说明：这个直辖组是补充直辖组，而这个部是没有部长的
                       return true;
                   }
               } else {
                   return true;
               }
           }
        }
        mLABranchGroupDB.setAgentGroup(AgentGroup);
        if(!mLABranchGroupDB.getInfo())
        {
			mLABranchGroupDB=null;
            DealErr("取代理人"+treeSSRS.GetText(i,1)/*getAgentCode()*/+"的机构信息出错！","AscriptRearQueryDealBL","BackToGroup");
            return false;
        }
        return true;
    }

    /*
     得到BackToGroup方法所找到的回归机构信息
     */
    public LABranchGroupSchema getOldGroup()
    {
    	if(mLABranchGroupDB==null)
    	{
    		LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
    	   return 	tLABranchGroupSchema;
    	}
    	else
    	{
    	   return mLABranchGroupDB.getSchema();
    	}
    }

    /*
     查询降级人员的直接部下  部：查其下主任；主任：查其下组员
     cAgentCode-降级人员的编码,也就原来的主管 cNewSeriers-降级人员的新系列,也就原来的主管新系列
     */
    public boolean DisMissAgentSet(String cAgentCode,String cNewSeriers,String cNewGrade ,
                                   String cOldSeriers,String cOldGrade)
    {
        String SQLstr;
        LAAgentDB tLAAgentDB=new LAAgentDB();
        LATreeDB tLATreeDB=new LATreeDB();

        if (cNewSeriers.equals("0")
            ||
            (cNewGrade.equals("B01")  && cOldSeriers.compareTo("0")>0) ) {//新系列小于1，说明原来系列是1，也就是业务主任
            //查回所有的组员基本信息
            SQLstr = "select * from laagent where agentstate <= '04' and  exists "+
                     "(select agentcode from latree where "+
                     "agentcode=laagent.agentcode  "+ //离职未确认的也回归
                     "and upagent='"+cAgentCode+"' and agentcode<>'"+cAgentCode+"') order by laagent.agentcode with ur";
            System.out.println("DisMissAgentSet中查询组下的组员的SQL： "+SQLstr);
            mLAAgentSet.add(tLAAgentDB.executeQuery(SQLstr));
            if (tLAAgentDB.mErrors.needDealError())
            {
                DealErr(tLAAgentDB.mErrors.getLastError(), "AscriptRearQueryDealBL",
                        "DisMissAgentSet");
                return false;
            }
            //查回所有的组员行政信息
            SQLstr ="select * from latree where exists "+
                    "(select agentcode from laagent where "+
                     "agentcode=latree.agentcode  and laagent.agentstate<='04' ) "+
                     "and upagent='"+cAgentCode+"' and agentcode<>'"+cAgentCode+"' order by agentcode with ur";
            mLATreeSet.add(tLATreeDB.executeQuery(SQLstr));
            if (tLATreeDB.mErrors.needDealError()) {
                DealErr(tLATreeDB.mErrors.getLastError(), "AscriptRearQueryDealBL",
                        "DisMissAgentSet");
                return false;
            }
        }
        else if (cNewSeriers.equals("1"))
        {//新系列不小于1，说明原来系列是2，也就是行销部经理
            //查回所有部下的组长的基本信息
            SQLstr = " select * from laagent where agentstate<='04' and exists "+
                     "(select agentcode from latree where agentcode=laagent.agentcode "+
                     " and upagent='"+cAgentCode+ //离职未确认的也回归，20050705 zxs
                     "' and agentseries>='1' and agentgrade<>'B01') order by laagent.agentcode with ur";
//        	SQLstr = " select a.* from laagent a,latree b where a.agentstate<='04' and a.agentcode=b.agentcode" +
//        			" and b.upagent = '"+cAgentCode+"' and b.agentseries>='1' and b.agentgrade<>'B01' order by a.agentcode with ur";
            System.out.println("DisMissAgentSet中查询部下的主任的SQL： "+SQLstr);
            mLAAgentSet.add(tLAAgentDB.executeQuery(SQLstr));
            if (tLAAgentDB.mErrors.needDealError()) {
                DealErr(tLAAgentDB.mErrors.getLastError(), "AscriptRearQueryDealBL",
                        "DisMissAgentSet");
                return false;
            }
            //查回所有部下的组长的行政信息
            SQLstr ="select * from latree where "
                   + " exists (select agentcode from laagent where "+
                     " agentcode=latree.agentcode  and laagent.agentstate<='04' )  "  +
                     " and upagent='"+cAgentCode+"' and agentseries>='1' and agentgrade<>'B01' "
                    +" order by agentcode with ur";
            mLATreeSet.add(tLATreeDB.executeQuery(SQLstr));
            if (tLATreeDB.mErrors.needDealError()) {
                DealErr(tLATreeDB.mErrors.getLastError(), "AscriptRearQueryDealBL",
                        "DisMissAgentSet");
                return false;
            }
        }
        if (mLAAgentSet.size() != mLATreeSet.size())
        {//两种信息的个数肯定是相等的，否则出错！
                DealErr("系统中代理人行政信息和基本信息有异常！", "AscriptRearQueryDealBL",
                        "DisMissAgentSet");
                return false;
        }
        return true;
    }

    /*
     查询组中除了组长以外所有的组员
     cAgentCode-目标机构的组长
     */
    public String[] CollectGroupAgent(String cAgentCode)
    {
        String SQLstr ="select agentcode from latree where (state is null or state<='3') and upagent='"+cAgentCode //离职未确认的也回归，20050705 zxs
                    +"' and agentseries<'1' order by agentcode";
        ExeSQL tExeSQL=new ExeSQL();
        SSRS agentR=new SSRS();
        agentR=tExeSQL.execSQL(SQLstr);
        if(tExeSQL.mErrors.needDealError())	
        {
            DealErr(tExeSQL.mErrors.getFirstError(),"AscriptRearQueryDealBL","CollectGroupAgent");
            return null;
        }
        if(agentR.MaxRow>0)
        {
            String[] tAgentcode=new String[agentR.MaxRow];
            for(int i=0;i<agentR.MaxRow;i++)
            {
                tAgentcode[i]=agentR.GetText(i+1,1);
            }
            return tAgentcode;
        }
        return null;
    }
}
