package com.sinosoft.lis.agentassess;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.lis.agent.ALADimissionAppUI;
import com.sinosoft.lis.agentassess.LAAssessInputUI;
import com.sinosoft.lis.agentbranch.LABranchGroup;
import sun.reflect.Reflection;

/**
 * 考核计算回退，
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAAssessBackCalBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往后面传输数据的容器 ，进行回退计算*/
    private VData mVData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String mEdorNo;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSet mLAAssessSet = new LAAssessSet();  //要回退的人员

    private LAAssessMainSchema mLAAssessMainSchema  = new LAAssessMainSchema();
    private LAAssessHistorySchema mLAAssessHistorySchema = new LAAssessHistorySchema();
    private LAIndexInfoSet mLAIndexInfoSet = new LAIndexInfoSet();
    private LAIndexInfoBSet mLAIndexInfoBSet = new LAIndexInfoBSet();
    private LAAssessBSet mLAAssessBSet = new LAAssessBSet();

    private MMap mMap = new MMap();

    private String mEvaluateDate = "";           // 异动时间
    private String mManageCom = "";              // 管理机构
    private String mIndexCalNo = "";             // 考核序列号
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mGradeSeries = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    public LAAssessBackCalBL()
    {

    }
    public static void main(String args[])
    {
    }
    public MMap getResult()
    {
        return mMap;
    }
    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

 //       mInputData = null;
        return true;
    }
    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mMap = new MMap();
        mMap.put(mLAAssessMainSchema, "DELETE");
        mMap.put(mLAAssessHistorySchema, "DELETE");
        mMap.put(mLAIndexInfoSet, "DELETE");
        mMap.put(mLAIndexInfoBSet, "INSERT");
        mMap.put(mLAAssessSet, "DELETE");
        mMap.put(mLAAssessBSet, "INSERT");
      //  mOutputData.add(mMap);
        return true;
    }


    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
        String tEdorNo = mEdorNo;

        if(mLAAssessSet.size()>0)
        {
            for (int i = 1; i <= mLAAssessSet.size(); i++)
            {
                LAAssessSchema tLAAssessSchema = new LAAssessSchema();
                tLAAssessSchema = mLAAssessSet.get(i);
                LAAssessBSchema tLAAssessBSchema = new LAAssessBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLAAssessBSchema,tLAAssessSchema);
                tLAAssessBSchema.setEdorNo(tEdorNo);
                tLAAssessBSchema.setEdorType("01");
                tLAAssessBSchema.setEdorDate(currentDate);
                mLAAssessBSet.add(tLAAssessBSchema);
            }
            LAIndexInfoDB tLAIndexInfoDB = new LAIndexInfoDB();
            String tSQL="select * from laindexinfo where managecom like '"+mManageCom+"%' and BranchType='"+
                        mBranchType+"' and BranchType2='"+mBranchType2+"' and indexcalno ='"+
                        mIndexCalNo+"' and indextype>='02' ";
            mLAIndexInfoSet=tLAIndexInfoDB.executeQuery(tSQL);
            for (int i = 1; i <= mLAIndexInfoSet.size(); i++)
            {
                LAIndexInfoSchema tLAIndexInfoSchema = new LAIndexInfoSchema();
                tLAIndexInfoSchema=mLAIndexInfoSet.get(i);
                LAIndexInfoBSchema tLAIndexInfoBSchema = new LAIndexInfoBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLAIndexInfoBSchema,tLAIndexInfoSchema);
                tLAIndexInfoBSchema.setEdorNo(tEdorNo);
                tLAIndexInfoBSchema.setEdorType("30");//考核回退备份
                mLAIndexInfoBSet.add(tLAIndexInfoBSchema);

            }
        }
        //处理 laassessmain 表
        LAAssessMainDB tLAAssessMainDB  = new LAAssessMainDB();
        tLAAssessMainDB.setManageCom(mManageCom);
        tLAAssessMainDB.setBranchType(mBranchType);
        tLAAssessMainDB.setBranchType2(mBranchType2);
        tLAAssessMainDB.setIndexCalNo(mIndexCalNo);
        tLAAssessMainDB.setAssessType("00");
        LAAssessMainSet tLAAssessMainSet  = new LAAssessMainSet();
        tLAAssessMainSet=tLAAssessMainDB.query();
        LAAssessMainSchema tLAAssessMainSchema  = new LAAssessMainSchema();
        if(tLAAssessMainSet.size()<=0)
        {
            this.buildError("dealData", "查询分公司"+mManageCom+"最终考核信息没有数据");
            return false;
        }
        tLAAssessMainSchema=tLAAssessMainSet.get(1);
        mLAAssessMainSchema=tLAAssessMainSchema;
           //处理laassesshistory 表
        LAAssessHistoryDB tLAAssessHistoryDB  = new LAAssessHistoryDB();
        tLAAssessHistoryDB.setManageCom(mManageCom);
        tLAAssessHistoryDB.setBranchType(mBranchType);
        tLAAssessHistoryDB.setBranchType2(mBranchType2);
        tLAAssessHistoryDB.setIndexCalNo(mIndexCalNo);
        LAAssessHistorySet tLAAssessHistorySet  = new LAAssessHistorySet();
        tLAAssessHistorySet=tLAAssessHistoryDB.query();
        if(tLAAssessHistorySet.size()<=0)
        {
            this.buildError("dealData", "查询分公司"+mManageCom+"最终考核信息没有数据");
            return false;
        }
        mLAAssessHistorySchema=tLAAssessHistorySet.get(1);
        return true;
    }
    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAssessBackCalBL.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLAAssessSet.set((LAAssessSet)cInputData
                .getObjectByObjectName("LAAssessSet",0));
        this.mManageCom = (String) cInputData.get(2);
        this.mIndexCalNo =  (String) cInputData.get(3);
        this.mBranchType = (String) cInputData.get(4);
        this.mBranchType2 = (String) cInputData.get(5);
        this.mEdorNo = (String) cInputData.get(6);
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessBackCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LAAssessBackCalBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


}
