package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LAAgentDB;
/**
 * <p>Title: AscriptBranchGroupOperateBL类</p>
 * <p>Description: 处理代理人归属时的机构信息</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
public class AscriptBranchGroupOperateBL {

    public CErrors mErrors = new CErrors();
    public GlobalInput mGlobalInput = new GlobalInput();//操作员的信息

    private String mOldGradeEndDate; //降级人员原职级结束的日期，由BLF传入
    private String mNewGradeStartDate; //降级人员新职级开始的日期，由BLF传入
    private VData mOutputData=new VData(); //保存向外层传递的数据
    private String mEdorNo;
    private String mIndexCalNo; //考核计算的编码，由BLF传入

    private String mMAXATTR=""; //不能用静态的，也没有必要
    private LABranchGroupSchema mLABranchGroupSchema=new LABranchGroupSchema();//当前操作的机构，新建
    private LABranchGroupSchema mChangeLABranchGroupSchema=new LABranchGroupSchema();//当前操作的机构，
    private LABranchGroupSet mLABranchGroupSet=new LABranchGroupSet();//要往BLS传递的机构，更新
    private LABranchGroupBSet mLABranchGroupBSet=new LABranchGroupBSet();//要往BLS传递的机构备份

    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    public AscriptBranchGroupOperateBL() {
    }

    public boolean getInputData(VData cInputData)
    {
        mOldGradeEndDate=(String)cInputData.get(0);
        mNewGradeStartDate=(String)cInputData.get(1);
        mGlobalInput=(GlobalInput)cInputData.getObjectByObjectName(
                             "GlobalInput",2);
        mIndexCalNo=(String)cInputData.get(3);
        if(mNewGradeStartDate!=null&&!mNewGradeStartDate.equals("")
                        &&
            mOldGradeEndDate!=null&&!mOldGradeEndDate.equals("")
                        &&
            mGlobalInput!=null)
          {
              return true;
          }
        return false;
    }

    private boolean prepareOutputDataToBLS()
    {
        try
         {
             //mLABranchGroupSet.add(mLABranchGroupSchema);
             mOutputData.clear();
             mOutputData.add(mLABranchGroupSchema);
             mOutputData.add(mLABranchGroupSet);
             mOutputData.add(mLABranchGroupBSet);
         }
         catch(Exception e)
         {
             dealErr("准备传向BLS的数据失败！","AscriptBranchGroupOperateBL","prepareOutputDataToBLS");
             return false;
         }
         return true;

    }

    public VData getOutputDataToBLS()
    {
        if(!prepareOutputDataToBLS())
        {
            return null;
        }
        if(mOutputData!=null||mOutputData.size()>0)
        {
            return mOutputData;
        }
        return null;
    }

    private void dealErr(String cMsg,String cMName,String cFName)
    {
                CError tCError=new CError();
                tCError.moduleName=cMName;
                tCError.functionName=cFName;
                tCError.errorMessage=cMsg;
                mErrors.addOneError(tCError);
    }
    /*
     建立新的机构
     参数:cAgentCode-新机构将来的主管 cAgentGroup-未来主管原来机构的机构内部号 cNewSeries-主管的新系列
     */
    /*
    public boolean createGroup(String cAgentCode,String cBranchAttr,String cNewSeries)
    {
        LATreeDB tLATreeDB=new LATreeDB();
        LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
        LABranchGroupSchema tLABranchGroupDBNew=new LABranchGroupSchema();
        String NewGroup=PubFun1.CreateMaxNo("AGENTGROUP",12);//通用方法生成新的机构编号
        //String NewUpBranchAttr="";
        String NewBranchLever="";
        String SQLstr;
        String tMaxAttr="";
        int Count;
        ExeSQL tExeSQL=new ExeSQL();
        SSRS QueryResult=new SSRS();
        if(cNewSeries.equals("2"))
        {//新系列大于1，说明是行销部经理系列
            //NewUpBranchAttr="0";
            NewBranchLever="02";
            String FirstFour=cBranchAttr.substring(0,4);//记录原来外部编码的前4位
            //取得这个机构下所有的部对应的最大的外部编码
            SQLstr="Select max(branchattr) from labranchgroup where " +
                   "branchattr like '"+ FirstFour + "%' and length(trim(branchattr))=7 "
                   //+"and endflag='N'" 撤销的机构的外部编码也不能占用
                   ;
            System.out.println("CreateGroup中部对应的最大的外部编码sql： "+SQLstr);
            QueryResult=tExeSQL.execSQL(SQLstr);
            if(tExeSQL.mErrors.needDealError())
            {
                dealErr(tExeSQL.mErrors.getLastError(),"AscriptBranchGroupOperateBL","createGroup");
                return false;
            }
            if(QueryResult.MaxRow<=0)
            {
                dealErr("查询最大的机构外部编码出错！","AscriptBranchGroupOperateBL","createGroup");
                return false;
            }
            tMaxAttr=QueryResult.GetText(1,1);//！！！！！！应该行列都是从一开始！！！！！！！！！！！！！！
            tMaxAttr=String.valueOf(Integer.parseInt(tMaxAttr)+1);
            Count=7-tMaxAttr.length();//算出外部编码自增时前端丢弃几个零
            for(int i=0;i<Count;i++)
            {
                tMaxAttr='0'+tMaxAttr;
            }
            tLATreeDB.setAgentCode(cAgentCode);
            if(!tLATreeDB.getInfo())
            {
                dealErr("查询人员的原行政信息出错！","AscriptBranchGroupOperateBL","createGroup");
                return false;
            }
            tLABranchGroupDB.setAgentGroup(tLATreeDB.getAgentGroup());
            if(!tLABranchGroupDB.getInfo())
            {
                dealErr("查询人员的原机构的信息出错！","AscriptBranchGroupOperateBL","createGroup");
                return false;
            }
            //设置新机构的系列码，用原机构的系列码头13位连接新机构编码
            tLABranchGroupDBNew.setBranchSeries(tLABranchGroupDB.getBranchSeries().substring(0,13)
                    + NewGroup);
            tLABranchGroupDBNew.setUpBranch(tLABranchGroupDB.getBranchSeries().substring(0,13));
        }
        else if(cNewSeries.equals("1"))
        {//主任系列
            //NewUpBranchAttr="0";
            NewBranchLever="01";
            String FirstSeven=cBranchAttr.substring(0,7);
            //取得这个机构下所有的组对应的最大的外部编码
            SQLstr="Select max(branchattr) from labranchgroup where " +
                   "branchattr like '"+ FirstSeven + "%' and length(trim(branchattr))=10 "
                   //+"and endflag='N'" 撤销的机构的外部编码也不能占用
                   ;
            System.out.println("CreateGroup中组对应的最大的外部编码sql： "+SQLstr);
            QueryResult=tExeSQL.execSQL(SQLstr);
            if(tExeSQL.mErrors.needDealError())
            {
                dealErr(tExeSQL.mErrors.getLastError(),"AscriptBranchGroupOperateBL","createGroup");
               return false;
            }
            if(QueryResult.MaxRow<=0)
            {
                dealErr("查询最大的机构外部编码出错！","AscriptBranchGroupOperateBL","createGroup");
                return false;
            }
            tMaxAttr=QueryResult.GetText(1,1);//！！！！！！应该行列都是从一开始！！！！！！！！！！！！！！
            tMaxAttr=String.valueOf(Integer.parseInt(tMaxAttr)+1);
            Count=10-tMaxAttr.length();
            for(int i=0;i<Count;i++)
            {
                tMaxAttr='0'+tMaxAttr;
            }
            tLATreeDB.setAgentCode(cAgentCode);
            if(!tLATreeDB.getInfo())
            {
                dealErr("查询人员的原行政信息出错！","AscriptBranchGroupOperateBL","createGroup");
                return false;
            }
            tLABranchGroupDB.setAgentGroup(tLATreeDB.getAgentGroup());
            if(!tLABranchGroupDB.getInfo())
            {
                dealErr("查询人员的原机构的信息出错！","AscriptBranchGroupOperateBL","createGroup");
                return false;
            }
            tLABranchGroupDBNew.setBranchSeries(tLABranchGroupDB.getBranchSeries().substring(0,26)
                    + NewGroup);
            tLABranchGroupDBNew.setUpBranch(tLABranchGroupDB.getUpBranch());
        }
        tLABranchGroupDBNew.setAgentGroup(NewGroup);
        tLABranchGroupDBNew.setManageCom(tLABranchGroupDB.getManageCom());
        tLABranchGroupDBNew.setBranchAttr(tMaxAttr);
        tLABranchGroupDBNew.setUpBranchAttr("0");//新建立的机构与上级机构永远不可能是直辖关系
        tLABranchGroupDBNew.setBranchType(tLABranchGroupDB.getBranchType());
        tLABranchGroupDBNew.setBranchType2(tLABranchGroupDB.getBranchType2());
        tLABranchGroupDBNew.setEndFlag("N");
        tLABranchGroupDBNew.setBranchManager(cAgentCode);
        tLABranchGroupDBNew.setFoundDate(mNewGradeStartDate);
        tLABranchGroupDBNew.setName("晋升生成");
        String name=getBranchManagerName(tLABranchGroupDBNew.getBranchManager());
        if(name==null||name.equals(""))
        {
            dealErr("查询机构管理员的姓名出错！","AscriptBranchGroupOperateBL","createGroup");
            return false;
        }
        tLABranchGroupDBNew.setBranchManagerName(name);
        tLABranchGroupDBNew.setBranchLevel(NewBranchLever);
        tLABranchGroupDBNew.setMakeDate(currentDate);
        tLABranchGroupDBNew.setMakeTime(currentTime);
        tLABranchGroupDBNew.setModifyDate(currentDate);
        tLABranchGroupDBNew.setModifyTime(currentTime);
        tLABranchGroupDBNew.setOperator(mGlobalInput.Operator);
        mLABranchGroupSchema.setSchema(tLABranchGroupDBNew);
        //mLABranchGroupSetInsert.add(mLABranchGroupSchema);
        return true;
    } */
    public boolean createGroup(String cAgentCode,String cAgentGroup,String cNewSeries)
    {
            LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
            LABranchGroupSchema tLABranchGroupDBNew=new LABranchGroupSchema();//保存新主管原来的上级机构
            String NewGroup=PubFun1.CreateMaxNo("AGENTGROUP",12);//通用方法生成新的机构编号
            String NewBranchLever="";
            String SQLstr;
            String tMaxAttr="";
            //先判断是不是直辖组
//            tLABranchGroupDB.setAgentGroup(cAgentGroup);
//            if(!tLABranchGroupDB.getInfo())
//            {
//                dealErr("查询机构"+tLABranchGroupDB.getAgentGroup()+"的信息出错！", "AscriptBranchGroupOperateBL",
//                        "createGroup");
//                return false;
//            }
//            if(!tLABranchGroupDB.getUpBranchAttr().equals("1"))
            {//说明不是部长直辖组中的人晋升 没有必要作这样的判断的 20050823 zxs
                if(cNewSeries.equals("2"))
                {
                    //晋升成区级经理，查原来所在的上级机构(可能是区或部)
                    SQLstr = "select * from labranchgroup where EndFlag<>'Y' and agentgroup = (select upbranch from labranchgroup where Agentgroup = '" +
                             cAgentGroup + "')";
                }
                else
                {  //晋升成处级经理，查原来所在的处
                    SQLstr = "select * from labranchgroup where EndFlag<>'Y' and agentgroup =  '" +cAgentGroup + "'";
                }
                System.out.println(SQLstr);

                LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.
                        executeQuery(
                                SQLstr);
                if (tLABranchGroupDB.mErrors.needDealError()) {
                    dealErr("查询机构" + tLABranchGroupDB.getAgentGroup() +
                            "的信息出错！", "AscriptBranchGroupOperateBL",
                            "createGroup");
                    return false;
                }

                //代理人原机构的上级机构
                tLABranchGroupDB.setSchema(tLABranchGroupSet.get(1));
            }
            if(cNewSeries.equals("2"))
            {//新系列大于1，说明是区级经理系列
                NewBranchLever="02";
            }
          else if(cNewSeries.equals("1"))
           {//主任系列
                NewBranchLever="01";
           }
           //生成机构最大号
           String UpBranchAttr=tLABranchGroupDB.getBranchAttr();
           String tupBranchLevel=tLABranchGroupDB.getBranchLevel();
           //如果是处晋升部，并且该处是直接在部下面（上级是部，不是区）
           if(tupBranchLevel.equals("03") && cNewSeries.equals("2"))
           {
               tMaxAttr = AgentPubFun.CreateBranchAttr(UpBranchAttr, NewBranchLever,
                                                                tLABranchGroupDB.getBranchType(),
                                                                tLABranchGroupDB.getBranchType2());
           }
           else
           {
        	   if(NewBranchLever.equals("01")){
   				tMaxAttr = AgentPubFun.CreateBranchAttr(
   						//(UpBranchAttr.substring(0,UpBranchAttr.length() - 2)), 
//   						修改原因是因为个险营业处编码可能为14位也可能是15位
   						(UpBranchAttr.substring(0,12)),
   						NewBranchLever,
   						tLABranchGroupDB.getBranchType(), tLABranchGroupDB
   								.getBranchType2());
   				
   			}
   			else {
   				tMaxAttr = AgentPubFun.CreateBranchAttr(
   						(UpBranchAttr.substring(0,UpBranchAttr.length() - 2)), 
   						NewBranchLever,
   						tLABranchGroupDB.getBranchType(), tLABranchGroupDB
   								.getBranchType2());	
   			}
           }
           //设置新机构的系列码
           String UpBranchSeries=tLABranchGroupDB.getBranchSeries();
           //如果是处晋升部，并且该处是直接在部下面（上级是部，不是区）
           if(tupBranchLevel.equals("03") && cNewSeries.equals("2"))
           {
               tLABranchGroupDBNew.setBranchSeries(UpBranchSeries+":"+ NewGroup);
               tLABranchGroupDBNew.setUpBranch(tLABranchGroupDB.getAgentGroup());
           }
           else
           {
               tLABranchGroupDBNew.setBranchSeries(UpBranchSeries.substring(0,
                       UpBranchSeries.length() - 12)
                       + NewGroup);
               tLABranchGroupDBNew.setUpBranch(tLABranchGroupDB.getUpBranch());
           }
           //这个判断没有必要的 20050823 zxs
//           if(NewBranchLever.equals("02"))
//           {//生成部
//               tLABranchGroupDBNew.setUpBranch(tLABranchGroupDB.getUpBranch());
//           }
//           else
//           {//生成组
//               tLABranchGroupDBNew.setUpBranch(tLABranchGroupDB.getAgentGroup());
//           }

            tLABranchGroupDBNew.setAgentGroup(NewGroup);
            tLABranchGroupDBNew.setManageCom(tLABranchGroupDB.getManageCom());
            tLABranchGroupDBNew.setBranchAttr(tMaxAttr);
            tLABranchGroupDBNew.setUpBranchAttr("0");//新建立的机构与上级机构永远不可能是直辖关系
            tLABranchGroupDBNew.setBranchType(tLABranchGroupDB.getBranchType());
            tLABranchGroupDBNew.setBranchType2(tLABranchGroupDB.getBranchType2());
            tLABranchGroupDBNew.setEndFlag("N");
            tLABranchGroupDBNew.setBranchManager(cAgentCode);
            tLABranchGroupDBNew.setFoundDate(mNewGradeStartDate);
            String name="";
            if(cAgentCode!=null&&!cAgentCode.equals(""))
            {
                tLABranchGroupDBNew.setName("晋升生成");
                name = getBranchManagerName(tLABranchGroupDBNew.
                        getBranchManager());
                if (name == null || name.equals("")) {
                    dealErr("查询机构管理员的姓名出错！", "AscriptBranchGroupOperateBL",
                            "createGroup");
                    return false;
                }
            }
            else
            {//新建立的机构，不设主管
                tLABranchGroupDBNew.setName("补充直辖组");
                tLABranchGroupDBNew.setUpBranchAttr("1");
            }
            tLABranchGroupDBNew.setBranchManagerName(name);
            tLABranchGroupDBNew.setBranchLevel(NewBranchLever);
			tLABranchGroupDBNew.setAStartDate(mNewGradeStartDate);
            tLABranchGroupDBNew.setTrusteeShip(tLABranchGroupDB.getTrusteeShip());
            tLABranchGroupDBNew.setCostCenter(tLABranchGroupDB.getCostCenter());
            tLABranchGroupDBNew.setMakeDate(currentDate);
            tLABranchGroupDBNew.setMakeTime(currentTime);
            tLABranchGroupDBNew.setModifyDate(currentDate);
            tLABranchGroupDBNew.setModifyTime(currentTime);
            tLABranchGroupDBNew.setOperator(mGlobalInput.Operator);
            mLABranchGroupSchema.setSchema(tLABranchGroupDBNew);
            return true;
    }

    /*
     改变一个组与其所在部的直辖关系
     参数：cAgentGroup-组的内部编码,cUpBranchAttr-直辖关系
     */
    public boolean changeUpBranchAttr(String cAgentGroup,String cUpBranchAttr)
    {
        LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(cAgentGroup);
        if(!tLABranchGroupDB.getInfo())
        {
            dealErr("查询机构"+cAgentGroup+"的信息失败！","AscriptBranchGroupOperateBL","changeUpBranchAtrrr");
            return false;
        }
        if(!backupBranchGroup(tLABranchGroupDB.getSchema()))
        {
            dealErr("备份人员的原机构的信息出错！","AscriptBranchGroupOperateBL","changeUpBranchAtrrr");
            return false;
        }
        tLABranchGroupDB.setUpBranchAttr(cUpBranchAttr);
        mLABranchGroupSet.add(tLABranchGroupDB.getSchema());
        return true;
    }

    public boolean changeBranchManager(String cAgentGroup, String cNewBranchManager)
    {
        LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(cAgentGroup);
        if(!tLABranchGroupDB.getInfo())
        {
            dealErr("查询机构"+cAgentGroup+"的信息失败！","AscriptBranchGroupOperateBL","changeBranchManager");
            return false;
        }
        if (!backupBranchGroup(tLABranchGroupDB.getSchema()))
        {
            dealErr("备份人员的原机构的信息出错！", "AscriptBranchGroupOperateBL",
                    "changeBranchManager");
            return false;
        }
        LAAgentDB  tLAAgentDB=new LAAgentDB();
        tLAAgentDB.setAgentCode(cNewBranchManager);
        if(!tLAAgentDB.getInfo())
        {
            tLABranchGroupDB.setBranchManagerName("");
        }
        else
        {
            tLABranchGroupDB.setBranchManagerName(tLAAgentDB.getName());
        }
        tLABranchGroupDB.setBranchManager(cNewBranchManager);
        mLABranchGroupSet.add(tLABranchGroupDB.getSchema());
        return true;
    }

    /*
     修改目的组信息中有关上级机构的信息,主要是把组挂到新的部下面
     参数:cAgentGroup-目的组的编码  LABranchGroupSchema cGroupSchema-目的机构的Schema
     */
    public boolean updateGroupInfo(String cAgentGroup/*LATreeSchema cLATreeSchema*/, LABranchGroupSchema cGroupSchema)
    {
        LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(cAgentGroup);
        String tMaxAttr="";
        if(!tLABranchGroupDB.getInfo())
        {
            dealErr("查询人员的原机构的信息出错！","AscriptBranchGroupOperateBL","updateGroupInfo");
            return false;
        }
        if(!backupBranchGroup(tLABranchGroupDB.getSchema()))
        {
            dealErr("备份人员的原机构的信息出错！","AscriptBranchGroupOperateBL","updateGroupInfo");
            return false;
        }
        tLABranchGroupDB.setUpBranch(cGroupSchema.getAgentGroup());
        tLABranchGroupDB.setBranchSeries(cGroupSchema.getBranchSeries()+":"+
                                         tLABranchGroupDB.getAgentGroup());
        if (tLABranchGroupDB.getBranchManager()==null||tLABranchGroupDB.getBranchManager().equals(""))
        {//如果没有组长就默认不是直辖组
//            dealErr("机构"+tLABranchGroupDB.getBranchAttr()+"没有主管，不能处理，请先补充相应的信息！","AscriptBranchGroupOperateBL","updateGroupInfo");
//            return false;
            tLABranchGroupDB.setUpBranchAttr("0");
        }
        else
        {
            if (tLABranchGroupDB.getBranchManager().equals(cGroupSchema.getBranchManager()))
            { //设置部的直辖组
                tLABranchGroupDB.setUpBranchAttr("1");
            }
            else
            {
                tLABranchGroupDB.setUpBranchAttr("0");
            }
        }
        //目的部中最大的外部编码
        if(mMAXATTR.equals(""))
        {
            //2005-07-04 modified by liujw
            mMAXATTR = AgentPubFun.CreateBranchAttr(cGroupSchema.getBranchAttr(),
                    tLABranchGroupDB.getBranchLevel(),
                    tLABranchGroupDB.getBranchType(),
                    tLABranchGroupDB.getBranchType2());
            if (mMAXATTR == null)
           {
               dealErr("生成销售机构号失败，因为机构级别描述表数据不全！","AscriptBranchGroupOperateBL","updateGroupInfo");
               return false;
           }
           tMaxAttr = mMAXATTR;
        }
        else
        {
            tMaxAttr = AgentPubFun.getMaxBranchAttr(cGroupSchema.getBranchAttr(),
                    mMAXATTR);
            mMAXATTR = tMaxAttr;
        }
        tLABranchGroupDB.setBranchAttr(tMaxAttr);
        tLABranchGroupDB.setModifyDate(currentDate);
        tLABranchGroupDB.setModifyTime(currentTime);
        tLABranchGroupDB.setOperator(mGlobalInput.Operator);
        mChangeLABranchGroupSchema=tLABranchGroupDB.getSchema();
        mLABranchGroupSet.add(tLABranchGroupDB);
        return true;
    }

    /*
     备份并撤销机构
     参数:cLABranchGroupDB-目的机构的信息
     */
    public boolean backupThenStopGroup(LABranchGroupSchema cLABranchGroupDB)//LATreeSchema cLATreeSchema)
    {
        LABranchGroupSchema tLABranchGroupDB=new LABranchGroupSchema();
        tLABranchGroupDB=cLABranchGroupDB;
        if(!backupBranchGroup(tLABranchGroupDB))
        {
            dealErr("备份人员的原机构的信息出错！","AscriptBranchGroupOperateBL","backupThenStopGroup");
            return false;
        }
        tLABranchGroupDB.setEndDate("");
        tLABranchGroupDB.setBranchManager("");
        tLABranchGroupDB.setBranchManagerName("");
        tLABranchGroupDB.setEndFlag("N");
        tLABranchGroupDB.setModifyDate(currentDate);
        tLABranchGroupDB.setModifyTime(currentTime);
        tLABranchGroupDB.setOperator(mGlobalInput.Operator);
        mLABranchGroupSet.add(tLABranchGroupDB);
        return true;
    }

    /*
     得到新建立的机构码
     */
     public LABranchGroupSchema getNewGroup()
    {
        return mLABranchGroupSchema;
    }

    public LABranchGroupSet getCurLABranchGroupSet()
    {
        return mLABranchGroupSet;
    }

    /*
     得到最新的机构备份记录
     参数:cAgentCode-机构的主管 cGroupFlag-部的标志(true：部 false：组)  cEdorType-备份类型
     这个函数现在不必要了，没有实现
     */
    private LABranchGroupBSchema getBranchGroupB(String cAgentCode,Boolean cGroupFlag,String cEdorType)
    {
        return null;
    }

    /*
     备份目的机构
     cLABranchGroupSchema-要备份机构的信息
     */
    private boolean backupBranchGroup(LABranchGroupSchema cLABranchGroupSchema)
    {
        try{
            Reflections tReflections = new Reflections();
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            LABranchGroupBSchema tLABranchGroupBDB = new LABranchGroupBSchema();
            tLABranchGroupSchema = cLABranchGroupSchema;
            tLABranchGroupBDB.setSchema((LABranchGroupBSchema) tReflections.
                                        transFields(
                                                tLABranchGroupBDB,
                                                tLABranchGroupSchema));
            tLABranchGroupBDB.setEdorNo(mEdorNo);
            tLABranchGroupBDB.setEdorType("01");
            tLABranchGroupBDB.setUpBranchAttr((cLABranchGroupSchema.getUpBranchAttr()==null||cLABranchGroupSchema.getUpBranchAttr().equals(""))?
                    "0":cLABranchGroupSchema.getUpBranchAttr());
            tLABranchGroupBDB.setOperator2(cLABranchGroupSchema.getOperator());
            tLABranchGroupBDB.setMakeDate2(cLABranchGroupSchema.getMakeDate());
            tLABranchGroupBDB.setModifyDate2(cLABranchGroupSchema.getModifyDate());
            tLABranchGroupBDB.setMakeTime2(cLABranchGroupSchema.getMakeTime());
            tLABranchGroupBDB.setModifyTime2(cLABranchGroupSchema.getModifyTime());
            tLABranchGroupBDB.setMakeDate(currentDate);
            tLABranchGroupBDB.setMakeTime(currentTime);
            tLABranchGroupBDB.setModifyDate(currentDate);
            tLABranchGroupBDB.setModifyTime(currentTime);
            tLABranchGroupBDB.setOperator(mGlobalInput.Operator);
            tLABranchGroupBDB.setIndexCalNo(mIndexCalNo);

            mLABranchGroupBSet.add(tLABranchGroupBDB);
        }
        catch(Exception e)
        {
            dealErr(e.getMessage(),"AscriptBranchGroupOperateBL","backupBranchGroup");
            return false;
        }
        return true;
    }

    public void resetMAXATTR()
    {
        mMAXATTR="";
    }

    public void setmEdorNo(String cEdorNo)
    {
        mEdorNo=cEdorNo;
    }

    private String getBranchManagerName(String cAgentCode)
    {
        ExeSQL tExeSQL=new ExeSQL();
        SSRS nameR=new SSRS();
        nameR=tExeSQL.execSQL("select name from laagent where agentcode='"+cAgentCode+"'");
        if(nameR.MaxRow>0)
        {
            return nameR.GetText(1,1);
        }
        else
        {
            return "";
        }
    }

    public LABranchGroupSet getLABranchGroupSet()
    {
        return mLABranchGroupSet;
    }
    //团队变更后的团队信息
    public LABranchGroupSchema getChangeGroupSchema()
    {
        return mChangeLABranchGroupSchema;
    }

}
