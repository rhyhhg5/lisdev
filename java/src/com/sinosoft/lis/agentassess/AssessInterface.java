package com.sinosoft.lis.agentassess;

import com.sinosoft.utility.VData;

/**
 * <p>Title: 考核公共接口</p>
 * <p>Description: 考核公共接口</p>
 * <p>Copyright: Copyright (c) sinosoft 2004</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public interface AssessInterface
{
    //前后台数据交互
    public boolean submitData(VData cInputData, String cOperate);

    //获得输入数据
    public boolean getInputData(VData cInputData);

    //对数据进行业务处理
    public boolean dealData();

    //进行考核计算
    public boolean AssessCalculate();

    //准备往后台传输数据
    public boolean prepareOutputData();

//    //每个对象单独考核函数
//    public boolean perCalculate();
}
