package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAgentBDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessDB;
import com.sinosoft.lis.db.LABranchGroupBDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeAccessoryBDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAssessSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeAccessoryBSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;


/**
 * <p>Title: 降级后的表操作</p>
 * <p>Description: 对表进行增删改操作</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zhangsj
 * @version 1.0
 */

public class DealRelaTab
{

    //错误处理类
    public static CErrors mErrors = new CErrors();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mIndexCalNo = "";
    private String[] MonDate = new String[2];

    public DealRelaTab()
    {
    }

    public static void main(String[] args)
    {
//    String tEduManager = "8611000003";
//    String tSeries = "8611000001:8611000002";
//    String tLeftSeries = "";
//    String tCurEduMng = "";
//    int p = tSeries.lastIndexOf(tEduManager);
//   if ( p > 0 ) {
//     tLeftSeries = tSeries.substring(0, p - 1);
//     p = tSeries.lastIndexOf(":");
//     tCurEduMng = tSeries.substring(p + 1);
//   }
//
//   System.out.println("--tLeftSeries : " + tLeftSeries);
//   System.out.println("--tCurEduMng : " + tCurEduMng);

        DealRelaTab tDT = new DealRelaTab();
        VData cInputData = new VData();
        cInputData.clear();
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86110000";
        tGI.ComCode = "8611";
        tGI.Operator = "aa";

        cInputData.add(tGI);
        cInputData.add("200306");

        if (!tDT.getInputData(cInputData))
        {
            System.out.println("---Failed! ");
        }
        System.out.println("---FirsDay: " + tDT.MonDate[0]);
        System.out.println("---LastDay: " + tDT.MonDate[1]);
    }


    public boolean updateLAAssess(LAAssessSchema tLAAssessSchema,
                                  LATreeSchema tLATreeSchema,
                                  String tAscriptGroup, Connection conn)
    {
        PubFun tPF = new PubFun();
        LAAssessDB tLAAssessDB = new LAAssessDB(conn);

        System.out.println("aaa:" + tLAAssessSchema.getAgentSeries());
//陶佳杰 2004-04-09 修改bug 不能作A01的归属。
        tLAAssessSchema.setAgentGroupNew(tAscriptGroup);
        if (tLAAssessSchema.getAgentSeries().compareTo("B") > 0)
        {
            //无论是否找到应归属的机构，AgentGroup均变为代理人的直辖机构
            String tAgentGroup = getDirAgency(tLATreeSchema);
            if (tAgentGroup == null || tAgentGroup.equals(""))
            {
                dealError("UpdateAgent", "取直辖机构AgentGroup失败！");
                return false;
            }
            tLAAssessSchema.setAgentGroupNew(tAgentGroup);
        }

        tLAAssessSchema.setState("2");
        tLAAssessSchema.setModifyDate(tPF.getCurrentDate());
        tLAAssessSchema.setModifyTime(tPF.getCurrentTime());
        tLAAssessSchema.setOperator(this.mGlobalInput.Operator);
        tLAAssessDB.setSchema(tLAAssessSchema);

        if (!tLAAssessDB.update())
        {
            dealError("updateLAAssess", "更新代理人考评信息失败！");
            return false;
        }
        return true;
    }

    public boolean updateLAAgent(LATreeSchema tLATreeSchema,
                                 String tAscriptGroup, Connection conn)
    {
        LAAgentDB tLAAgentDB = new LAAgentDB(conn);
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        PubFun tPF = new PubFun();

        tLAAgentDB.setAgentCode(tLATreeSchema.getAgentCode());

        if (!tLAAgentDB.getInfo())
        {
            dealError("updateLAAgent", "取代理人信息失败！");
            return false;
        }
        tLAAgentSchema.setSchema(tLAAgentDB.getSchema());
        if (tLATreeSchema.getAgentSeries().trim().compareToIgnoreCase("B") <= 0)
        {
            tLAAgentSchema.setAgentGroup(tAscriptGroup);
            tLAAgentSchema.setBranchCode(tAscriptGroup);
        }
        else
        {
            // 查询直辖机构的AgentGroup
            String tAgentGroup = getDirAgency(tLATreeSchema);
            if (tAgentGroup == null || tAgentGroup.equals(""))
            {
                dealError("UpdateAgent", "取直辖机构AgentGroup失败！");
                return false;
            }
            tLAAgentSchema.setAgentGroup(tAgentGroup);
        }

        tLAAgentSchema.setModifyDate(tPF.getCurrentDate());
        tLAAgentSchema.setModifyTime(tPF.getCurrentTime());
        tLAAgentSchema.setOperator(this.mGlobalInput.Operator);

        tLAAgentDB.setSchema(tLAAgentSchema);

        if (!tLAAgentDB.update())
        {
            dealError("updateLAAgent", "更新代理人信息失败！");
            return false;
        }
        return true;
    }

    public boolean insLAAgentB(String tAgentCode, Connection conn)
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSchema tSch = new LAAgentSchema();
        LAAgentBSchema tBSch = new LAAgentBSchema();
        LAAgentBDB tBDB = new LAAgentBDB(conn);
        PubFun1 tPF1 = new PubFun1();
        PubFun tPF = new PubFun();

        tLAAgentDB.setAgentCode(tAgentCode);

        if (!tLAAgentDB.getInfo())
        {
            dealError("updateLAAgent", "取代理人信息失败！");
            return false;
        }
        tSch.setSchema(tLAAgentDB.getSchema());
        Reflections rf = new Reflections();
        rf.transFields(tBSch, tSch);

        tBSch.setEdorNo(OAgentAscript.getEdorNo());
        System.out.println("---zsj---DealRelaTab--insLAAgentB--EdorNo = " +
                           OAgentAscript.getEdorNo());
        tBSch.setEdorType("01");
        tBSch.setMakeDate(tPF.getCurrentDate());
        tBSch.setMakeTime(tPF.getCurrentTime());
        tBSch.setModifyDate(tPF.getCurrentDate());
        tBSch.setModifyTime(tPF.getCurrentTime());
        tBSch.setIndexCalNo(this.mIndexCalNo);
        tBSch.setOperator(this.mGlobalInput.Operator);

        tBDB.setSchema(tBSch);
        if (!tBDB.insert())
        {
            dealError("InsLAAgentB", "插入LAAgentB备份表失败！");
            return false;
        }

        return true;
    }

    private String getDirAgency(LATreeSchema tLATreeSchema)
    {
        LABranchGroupDB tDB = new LABranchGroupDB();
        LABranchGroupSet tSet = new LABranchGroupSet();
        LABranchGroupSchema tSch = new LABranchGroupSchema();
        String tLevel = "";

        if (tLATreeSchema.getAgentSeries().equals("C"))
        {
            tLevel = "01";
        }
        if (tLATreeSchema.getAgentGrade().equals("A08"))
        {
            tLevel = "02";
        }
        if (tLATreeSchema.getAgentGrade().equals("A09"))
        {
            tLevel = "03";
        }

        String sql = "SELECT * FROM LABranchGroup WHERE BranchManager = '" +
                     tLATreeSchema.getAgentCode().trim() +
                     "' AND BranchLevel = '" +
                     tLevel.trim() + "'";

        tSet = tDB.executeQuery(sql);
        if (tSet.size() <= 0)
        {
            return "";
        }

        tSch = tSet.get(1);
        return tSch.getAgentGroup();
    }

    public boolean insertLATreeB(LATreeSchema tLATreeSchema, Connection conn)
    {
        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();

        LATreeBDB tLATreeBDB = new LATreeBDB(conn);
        LATreeBSchema tBSch = new LATreeBSchema();
        Reflections tf = new Reflections();
        tf.transFields(tBSch, tLATreeSchema);

        tBSch.setOperator(mGlobalInput.Operator);
        tBSch.setOperator2(tLATreeSchema.getOperator());
        tBSch.setRemoveType("01");

        tBSch.setModifyDate(tPF.getCurrentDate());
        tBSch.setModifyTime(tPF.getCurrentTime());
        tBSch.setModifyDate2(tLATreeSchema.getModifyDate());
        tBSch.setModifyTime2(tLATreeSchema.getModifyTime());
        tBSch.setMakeDate(tPF.getCurrentDate());
        tBSch.setMakeTime(tPF.getCurrentTime());
        tBSch.setMakeDate2(tLATreeSchema.getMakeDate());
        tBSch.setMakeTime2(tLATreeSchema.getMakeTime());
        tBSch.setIndexCalNo(this.mIndexCalNo);

        tBSch.setEdorNO(OAgentAscript.getEdorNo());
        System.out.println("---zsj---DealRelaTab--insLATreeB--EdorNo = " +
                           OAgentAscript.getEdorNo());

        tLATreeBDB.setSchema(tBSch);

        if (!tLATreeBDB.insert())
        {
            dealError("insertLATreeB", "插入代理人行政信息转储表失败！");
            return false;
        }
        return true;
    }

    public boolean updateLATree(LATreeSchema tLATreeSchema,
                                LAAssessSchema tLAAssessSchema,
                                String tUpAgent, String tAscriptGroup,
                                Connection conn)
    {
        PubFun tPF = new PubFun();

        LATreeDB tLATreeDB = new LATreeDB(conn);
        LATreeSchema tSch = new LATreeSchema();

        tSch.setSchema(tLATreeSchema);
        tSch.setUpAgent(tUpAgent);

        //若没有找到应归属的机构，则AgentGroup不变
        if ((tAscriptGroup == null || tAscriptGroup.equals("")) &&
            (tUpAgent == null || tUpAgent.equals("")))
        {
            String tAgentGroup = getDirAgency(tLATreeSchema);
            if (tAgentGroup == null || tAgentGroup.equals(""))
            {
                dealError("UpdateAgent", "取直辖机构AgentGroup失败！");
                return false;
            }
            tSch.setAgentGroup(tAgentGroup);
        }
        else
        {
            if (tLATreeSchema.getAgentSeries().equals("B"))
            {
                tSch.setAgentGroup(tAscriptGroup);
            }
            else
            {
                // 查询直辖机构的AgentGroup
                String tAgentGroup = getDirAgency(tLATreeSchema);
                if (tAgentGroup == null || tAgentGroup.equals(""))
                {
                    dealError("UpdateAgent", "取直辖机构AgentGroup失败！");
                    return false;
                }
                tSch.setAgentGroup(tAgentGroup);
            }
        }

        tSch.setAgentLastGrade(tLATreeSchema.getAgentGrade());
        tSch.setAgentLastSeries(tLATreeSchema.getAgentSeries());
        tSch.setAgentGrade(tLAAssessSchema.getAgentGrade1());
        tSch.setAgentSeries(tLAAssessSchema.getAgentSeries1());
        tSch.setOldStartDate(tLATreeSchema.getStartDate());
        tSch.setOldEndDate(this.MonDate[1]);
        tSch.setRearCommEnd(this.MonDate[1]);
        tSch.setStartDate(this.MonDate[0]);
        tSch.setAstartDate(this.MonDate[0]);
        tSch.setAssessType("2");
        tSch.setState("2");

        /*zsj--delete--2004-3-19:为了抽取二代育成的佣金，需要找到间接育成人，所以降级时只是将表LATreeAccessory的mmbreakflag字段置为1*/
        // 刷新抽佣关系，刷新代理人行政信息附属表
        LATreeAccessoryDB tAccDB = new LATreeAccessoryDB(conn);
        LATreeAccessorySchema tAccSch = new LATreeAccessorySchema();

        tAccDB.setAgentCode(tLATreeSchema.getAgentCode());
        tAccDB.setAgentGrade(tLATreeSchema.getAgentGrade());
        if (tAccDB.getInfo())
        {

            tAccSch.setSchema(tAccDB.getSchema());

            tAccSch.setCommBreakFlag("1");
            tAccSch.setIntroBreakFlag("1");
            tAccSch.setRearFlag("1"); //caigang 2004-04-23修改，当当前代理人降级时，相应职级对应的LATreeAccessory表的记录中RearFlag置'1',表示育成关系断裂
            tAccSch.setModifyDate(tPF.getCurrentDate());
            tAccSch.setModifyTime(tPF.getCurrentTime());
            tAccSch.setOperator(this.mGlobalInput.Operator);

            tAccDB.setSchema(tAccSch);
            if (!tAccDB.update())
            {
                dealError("updateLATree", "刷新代理人行政信息附属表失败！");
                return false;
            }
        }
        String agentGrade1 = tLAAssessSchema.getAgentGrade1();
        String orAgentGrade = agentGrade1;
        if (agentGrade1.equals("A04") || agentGrade1.equals("A05"))
        {
            orAgentGrade = " and (agentgrade='A04' or agentgrade='A05' )";
        }
        if (agentGrade1.equals("A06") || agentGrade1.equals("A07"))
        {
            orAgentGrade = " and (agentgrade='A06' or agentgrade='A07' )";
        }
        if (agentGrade1.equals("A08"))
        {
            orAgentGrade = " and agentgrade='A08'";
        }
        if (agentGrade1.equals("A09"))
        {
            orAgentGrade = " and agentgrade='A09'";
        }

        String sql = "select * from latreeaccessory where agentcode='" +
                     tLATreeSchema.getAgentCode() + "' " + orAgentGrade;
        LATreeAccessoryDB tLATreeAccessoryDB = new LATreeAccessoryDB(conn);
        LATreeAccessorySet tLATreeAccessorySet = new LATreeAccessorySet();
        System.out.println(sql);
        tLATreeAccessorySet = tLATreeAccessoryDB.executeQuery(sql);

        if (tLATreeAccessorySet.size() > 0)
        {
            ExeSQL tExeSQL = new ExeSQL(conn);
            LATreeAccessorySchema tLATreeAccessorySchema = new
                    LATreeAccessorySchema();
            tLATreeAccessorySchema = tLATreeAccessorySet.get(1);
            String agentGrade = tLATreeAccessorySchema.getAgentGrade();
            tLATreeAccessorySchema.setAgentGrade(agentGrade1);
            String SQL = "update latreeaccessory set agentgrade='" +
                         agentGrade1 + "' where agentcode='" +
                         tLATreeSchema.getAgentCode() + "' and agentgrade='" +
                         agentGrade + "'";
            if (!tExeSQL.execUpdateSQL(SQL))
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                dealError("updateLATree", "刷新代理人行政信息附属表的职级失败！");
                return false;
            }
        }

        // 刷新育成和归属顺序
        VData tVD = new VData();
        String tCurEduManager = "";
        String tLeftSeries = "";
        tVD.clear();
        tVD = getRearRelation(tLATreeSchema.getAscriptSeries(),
                              tLATreeSchema.getAgentSeries(),
                              tLATreeSchema.getAgentGrade());
        String Flag = (String) tVD.getObjectByObjectName("String", 0);
        if (Flag.equals("S"))
        {
            tCurEduManager = (String) tVD.getObjectByObjectName("String", 2);
            tLeftSeries = (String) tVD.getObjectByObjectName("String", 1);
        }
        if (Flag.equals("F"))
        {
            dealError("updateLATree", "刷新育成和归属顺序失败！");
            return false;
        }

        tSch.setEduManager(tCurEduManager);
        tSch.setAscriptSeries(tLeftSeries);

        tSch.setModifyDate(tPF.getCurrentDate());
        tSch.setModifyTime(tPF.getCurrentTime());
        tSch.setOperator(this.mGlobalInput.Operator);

        tLATreeDB.setSchema(tSch);
        if (!tLATreeDB.update())
        {
            dealError("updateLATree", "更新代理人行政信息表失败！");
            return false;
        }
        return true;
    }

    private VData getRearRelation(String tSeries, String tAgentSeries,
                                  String tAgentGrade)
    {
        VData tReturn = new VData();
        String tLeftSeries = "";
        String tCurEduMng = "";
        String Flag = "";

        if (tAgentSeries.trim().equalsIgnoreCase("B"))
        {
            //int tIndex1 = tSeries.trim().indexOf(":");
            tCurEduMng = "";
            tLeftSeries = "";
            Flag = "S";
        }

        if (tAgentSeries.trim().equalsIgnoreCase("C"))
        {
            int tIndex1 = tSeries.trim().indexOf(":");

            Flag = "S";
            tLeftSeries = tSeries.trim().substring(0, tIndex1);
            tCurEduMng = tSeries.trim().substring(0, tIndex1);
        }

        if (tAgentGrade.trim().equalsIgnoreCase("A08"))
        {
            int tIndex1 = tSeries.trim().indexOf(":");
            int tIndex2 = tSeries.trim().indexOf(":", tIndex1 + 1);
            Flag = "S";
            tLeftSeries = tSeries.trim().substring(0, tIndex2);
            tCurEduMng = tSeries.trim().substring(tIndex1 + 1, tIndex2);
        }

        if (tAgentGrade.trim().equalsIgnoreCase("A09"))
        {
            Flag = "S";
            int tIndex1 = tSeries.trim().indexOf(":");
            int tIndex2 = tSeries.trim().indexOf(":", tIndex1 + 1);
            int tIndex3 = tSeries.trim().indexOf(":", tIndex2 + 1);

            tCurEduMng = tSeries.trim().substring(tIndex2 + 1, tIndex3);
            tLeftSeries = tSeries.trim().substring(0, tIndex3);
        }

        System.out.println("--tLeftSeries--" + tLeftSeries);
        System.out.println("--tCurEduMng--" + tCurEduMng);
        tReturn.clear();

        try
        {
            if (Flag == "S")
            {
                tReturn.add(0, Flag);
                tReturn.add(1, tLeftSeries);
                tReturn.add(2, tCurEduMng);
            }
            else
            {
                tReturn.add(0, "F");
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getRearRelation", "处理归属顺序出错！");
            return null;
        }
        return tReturn;
    }

    private boolean insLATreeAccessoryB(LATreeAccessorySchema tSch,
                                        Connection conn)
    {
        PubFun1 tPF1 = new PubFun1();
        PubFun tPF = new PubFun();
        LATreeAccessoryBSchema tBSch = new LATreeAccessoryBSchema();
        LATreeAccessoryBDB tDB = new LATreeAccessoryBDB(conn);

        Reflections rf = new Reflections();
        rf.transFields(tBSch, tSch);
        tBSch.setEdorNo(OAgentAscript.getEdorNo());
        System.out.println(
                "---zsj---DealRelaTab--insLATreeAccessoryB--EdorNo = " +
                OAgentAscript.getEdorNo());
        tBSch.setIndexCalNo(this.mIndexCalNo);
        tBSch.setEdorType("01"); //转储类型－考核结果
        tBSch.setMakeDate(tPF.getCurrentDate());
        tBSch.setMakeTime(tPF.getCurrentTime());
        tBSch.setModifyDate(tPF.getCurrentDate());
        tBSch.setModifyTime(tPF.getCurrentTime());
        tBSch.setOperator(this.mGlobalInput.Operator);

        //zsj--add--2004-2-20: 为了查询育成该职级的代理人抽佣的时间段
        tBSch.setenddate(this.MonDate[1]);

        tDB.setSchema(tBSch);
        if (!tDB.insert())
        {
            dealError("InsLATreeAccessoryB", "备份附属表失败！");
            return false;
        }
        return true;
    }

    // tEndFlag = "Y"--置停业标记、"N"--不置
    public boolean insertLABranchGroupB(String tAgentGroup, String tEndFlag,
                                        Connection conn)
    {
        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();

        LABranchGroupDB tDB = new LABranchGroupDB();
        LABranchGroupSchema tSh = new LABranchGroupSchema();
        LABranchGroupBSchema tBSh = new LABranchGroupBSchema();
        LABranchGroupBDB tBDB = new LABranchGroupBDB(conn);
        tDB.setAgentGroup(tAgentGroup);

        if (!tDB.getInfo())
        {
            dealError("insertLABranchGroupB", "查询展业机构信息失败！");
            return false;
        }
        tSh.setSchema(tDB.getSchema());
        Reflections tf = new Reflections();
        tf.transFields(tBSh, tSh);

        tBSh.setEdorNo(OAgentAscript.getEdorNo());
        System.out.println("---zsj---DealRelaTab--insLABranchGroupB--EdorNo = " +
                           OAgentAscript.getEdorNo());
        tBSh.setEdorType("01");
        if (tEndFlag.trim().equals("Y"))
        {
            tBSh.setEndFlag("Y");
        }
        tBSh.setEndDate(this.MonDate[0]);
        tBSh.setMakeDate2(tSh.getModifyDate());
        tBSh.setMakeTime2(tSh.getMakeTime());
        tBSh.setMakeDate(tPF.getCurrentDate());
        tBSh.setMakeTime(tPF.getCurrentTime());
        tBSh.setModifyDate2(tSh.getModifyDate());
        tBSh.setModifyTime2(tSh.getModifyTime());
        tBSh.setModifyDate(tPF.getCurrentDate());
        tBSh.setModifyTime(tPF.getCurrentTime());
        tBSh.setOperator2(tSh.getOperator());
        tBSh.setOperator(this.mGlobalInput.Operator);
        tBSh.setIndexCalNo(this.mIndexCalNo);

        tBDB.setSchema(tBSh);
        if (!tBDB.insert())
        {
            return false;
        }

        return true;
    }

    public boolean delLABranchGroup(LATreeSchema tSch, Connection conn)
    {
        LABranchGroupDB tDB = new LABranchGroupDB(conn);
        if (!tSch.getAgentSeries().trim().equalsIgnoreCase("B"))
        {
            //将其直辖机构的UpBranchAttr的属性改为0
            String tLevel = "";
            if (tSch.getAgentSeries().trim().equals("C"))
            {
                tLevel = "01";
            }
            if (tSch.getAgentGrade().trim().equals("A08"))
            {
                tLevel = "02";
            }
            if (tSch.getAgentGrade().trim().equals("A09"))
            {
                tLevel = "03";
            }

            String sql =
                    "select * from labranchgroup where trim(branchmanager) = '"
                    + tSch.getAgentCode() + "' and trim(branchlevel) = '"
                    + tLevel.trim() + "'";

            LABranchGroupSet tSet = new LABranchGroupSet();
            tSet = tDB.executeQuery(sql);
            LABranchGroupSchema tBranchSchema = new LABranchGroupSchema();
            tBranchSchema.setSchema(tSet.get(1));
            tBranchSchema.setUpBranchAttr("0");
            tDB.setSchema(tBranchSchema);
            if (!tDB.update())
            {
                return false;
            }
        }

        tDB.setAgentGroup(tSch.getAgentGroup());
        if (!tDB.delete())
        {
            dealError("delLABranchGroup", "删除展业机构表失败！");
            return false;
        }
        return true;
    }

    //Flag = 'U':刷新上级机构 、'N'：不变UpBranch
    public boolean updateLABranchGroup(String tAgentGroup, String tAttr,
                                       String tAscriptGroup, String Flag,
                                       Connection conn)
    {
        PubFun tPF = new PubFun();
        LABranchGroupDB tDB = new LABranchGroupDB(conn);
        LABranchGroupSchema tSh = new LABranchGroupSchema();

        tDB.setAgentGroup(tAgentGroup);
        if (!tDB.getInfo())
        {
            dealError("updLABranchGroup", "查询展业机构信息失败！");
            return false;
        }
        tSh.setSchema(tDB.getSchema());

        if (Flag.equals("U"))
        {
            tSh.setUpBranch(tAscriptGroup);
            tSh.setUpBranchAttr("0");
        }

        tSh.setName("降级生成");
        //zsj--modify--2004-2-25:对于找不到归属的目标机构，由公司统一处理的情况：将当前机构置为停业
        //一种情况是找不到应该归属到的机构，还有一种情况是处理停业机构
        if (tAttr == null || tAttr.trim().equals(""))
        {

            //caigang 2004-07-06修改，将EndFlag="N",BranchManager="",BranchManagerName=""
//      tSh.setEndFlag("Y");
//      tSh.setEndDate(this.MonDate[1]);
            tSh.setBranchManager("");
            tSh.setBranchManagerName("");

            tSh.setBranchAddressCode(this.mIndexCalNo);
        }
        else
        {
            tSh.setBranchAttr(tAttr.trim());
        }
        tSh.setModifyDate(tPF.getCurrentDate());
        tSh.setModifyTime(tPF.getCurrentTime());
        tSh.setOperator(this.mGlobalInput.Operator);

        tDB.setSchema(tSh);
        if (!tDB.update())
        {
            return false;
        }

        return true;
    }


    private String[] genDate()
    {
        String[] tMonDate = new String[2];
        PubFun tPF = new PubFun();

        String tYear = this.mIndexCalNo.trim().substring(0, 4);
        String tMon = this.mIndexCalNo.trim().substring(4, 6);

        //计算旧职级的止聘日期
        String tOldDate = tYear + "-" + tMon + "-01";
        tMonDate = tPF.calFLDate(tOldDate);
        String tOldDay = tMonDate[1];
// bug 计算日期有问题。2004-02-10 tjj 修改。
        //计算新职级的起聘日期或转正日期
        if (tMon.equals("12"))
        {
            tYear = Integer.toString(Integer.parseInt(tYear) + 1);
            tMon = "01";
        }
        else
        {
            tMon = Integer.toString(Integer.parseInt(tMon) + 1);
        }
        int tLength = tMon.length();
        if (tLength == 1)
        {
            tMon = "0" + tMon;
        }
        String tDate = tYear.trim() + "-" + tMon.trim() + "-" + "01";
        tMonDate = tPF.calFLDate(tDate);
        String tNewDay = tMonDate[0];

        tMonDate[0] = tNewDay;
        tMonDate[1] = tOldDay;
        return tMonDate;
    }


    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "DealRelaTab";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }

    public boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mIndexCalNo = (String) cInputData.getObjectByObjectName("String",
                0);

        if (this.mGlobalInput == null)
        {
            // @@错误处理
            dealError("getInputData", "没有得到足够的信息！");
            return false;
        }

        this.MonDate = genDate();
        System.out.println("--DealRelaTab--this.MonDate[0] = " + this.MonDate[0]);
        System.out.println("--DealRelaTab--this.MonDate[1] = " + this.MonDate[1]);
        return true;
    }

} //end class
