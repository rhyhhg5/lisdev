package com.sinosoft.lis.agentassess;

/**
 * <p>Title: 某个管理机构下员工的员工制归属 </p>
 * <p>Description: 某个管理机构下员工的员工制归属 </p>
 * <p>Copyright: Copyright (c) 2004-02-26 to 2004-02-26 </p>
 * <p>Company: sinosoft</p>
 * @author LL
 * @version 1.0
 */

import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LAAssessAccessorySet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAManComEmployeeAscriptionBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private String mManageCom; //保存管理机构
    private String mIndexCalNo; //保存计算编码

    /** 归属时用到的变量 */

    /** 取得系统日期、时间 */
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public LAManComEmployeeAscriptionBL()
    {
    }

    public static void main(String[] args)
    {
        LAManComEmployeeAscriptionBL tLAManComEmployeeAscriptionBL = new
                LAManComEmployeeAscriptionBL();
        //1 - 测试参数传入 状态：正确
        String tManageCom = "8613";
        String tIndexCalNo = "200401";
        VData cInputData;
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";

        cInputData = new VData();
        cInputData.add(tGlobalInput);
        cInputData.add(tManageCom);
        cInputData.add(tIndexCalNo);

        tLAManComEmployeeAscriptionBL.submitData(cInputData, "INSERT||MAIN");

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAManComEmployeeAscriptionBL";
            tError.functionName = "submitData";
            tError.errorMessage =
                    "数据处理失败LAManComEmployeeAscriptionBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
//    //准备往后台的数据
//    if (!prepareOutputData())
//      return false;

        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //获得前台传过来的数据
        this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mManageCom = (String) cInputData.getObject(1);
        this.mIndexCalNo = (String) cInputData.getObject(2);
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        // 实现采用少于8位的管理机构代码计算多个8位管理机构下的业务员的考核
        // 例如用 8611计算86110000
        String tSQL = "select * from ldcom where comcode like '" +
                      this.mManageCom + "%' "
                      + " and length(trim(comcode)) = 8 order by comcode asc";
        LDComDB tLDComDB = new LDComDB();
        LDComSet cLDComSet = new LDComSet();
        cLDComSet = tLDComDB.executeQuery(tSQL);
        if (tLDComDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLDComDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAManComEmployeeAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询管理机构失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("取多个管理机构的Sql : " + tSQL);
        System.out.println("管理机构个数为: " + cLDComSet.size());

        //循环处理每一个管理机构
        String tComCode = "";
        //用此变量来记录不满足 ”查询某个管理机构下业务员失败“条件
        //的机构个数
        int count = 0;
        for (int j = 1; j <= cLDComSet.size(); j++)
        {
            tComCode = cLDComSet.get(j).getComCode().trim();
            //保证只计算8位的分公司
            if (tComCode.length() != 8)
            {
                continue;
            }
            System.out.println(
                    "***********************************************");
            System.out.println("8位的分公司代码" + tComCode);
            //获得总公司下的分公司编码，分别对各个分公司进行计算
            this.mManageCom = tComCode;

            //1 - 取出某个管理机构下要进行员工制归属处理的业务员
            String tSQL1 =
                    "select * from LAAssessAccessory where IndexCalNo = '" +
                    this.mIndexCalNo +
                    "' and ManageCom like '" + this.mManageCom + "%' " +
                    " and AssessType = '01'"
                    + " and state = '1' order by agentgrade1 asc ";
            LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB();
            LAAssessAccessorySet tLAAssessAccessorySet = new
                    LAAssessAccessorySet();
            tLAAssessAccessorySet = tLAAssessAccessoryDB.executeQuery(tSQL1);
            System.out.println("查询业务员Sql  : " + tSQL1);
            System.out.println("要归属的员工个数: " + tLAAssessAccessorySet.size());
            if (tLAAssessAccessoryDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAssessAccessoryDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAManComEmployeeAscriptionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询某个管理机构下业务员失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //判断是否存在满足条件的归属业务员
            if (tLAAssessAccessorySet.size() == 0)
            {
//        CError tError = new CError();
//        tError.moduleName = "LAManComEmployeeAscriptionBL";
//        tError.functionName = "dealData";
//        tError.errorMessage ="没有满足归属条件业务员存在!";
//        this.mErrors .addOneError(tError) ;
                System.out.println("管理机构：" + this.mManageCom + "中不存在满足归属条件的业务员");
                count = count + 1;
                continue;
            }

            // 2 - 逐个处理每个业务员的归属
            VData cInputData;
            LAEmployeeAscriptionBL tLAEmployeeAscriptionBL;
            for (int i = 1; i <= tLAAssessAccessorySet.size(); i++)
            {
                //准备好往后台传的数据
                this.mInputData = new VData();
                this.mInputData.add(this.mGlobalInput);
                this.mInputData.add(this.mManageCom);
                this.mInputData.add(this.mIndexCalNo);
                this.mInputData.add(tLAAssessAccessorySet.get(i).getAgentCode());
                this.mInputData.add(tLAAssessAccessorySet.get(i).getAgentGrade1());

                System.out.println("*****开始对" +
                                   tLAAssessAccessorySet.get(i).getAgentCode() +
                                   "业务员进行员工制归属");
                tLAEmployeeAscriptionBL = new LAEmployeeAscriptionBL();
                System.out.println(
                        "Start LAManComEmployeeAscriptionBL Submit...");
                tLAEmployeeAscriptionBL.submitData(this.mInputData,
                        this.mOperate);
                System.out.println(
                        "End   LAManComEmployeeAscriptionBL Submit...");
                //如果有需要处理的错误，则返回
                if (tLAEmployeeAscriptionBL.mErrors.needDealError())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAEmployeeAscriptionBL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAManComEmployeeAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "数据提交失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            System.out.println(
                    "***********************************************");
            System.out.println("分公司:" + tComCode + "计算结束");
        }
        //在此比较不满足归属某管理机构下人员查询条件的机构个数与总共有的机构个数
        System.out.println("不满足人员查询的机构个数为：" + count);
        if (count == cLDComSet.size())
        {
            //如果相等，说明每个机构都满足条件，给出错误提示
            CError tError = new CError();
            tError.moduleName = "LAManComEmployeeAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "该机构不满足归属条件！";
            this.mErrors.addOneError(tError);
            System.out.println("该机构不满足归属条件！");
            return false;
        }
        return true;
    }
}
