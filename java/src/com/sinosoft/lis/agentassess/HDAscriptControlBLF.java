package com.sinosoft.lis.agentassess;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.agent.HDDimissionAppBL;
import com.sinosoft.lis.agent.HDDimissionBL;

/**
 * <p>Title:互动渠道业务组织归属 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class HDAscriptControlBLF {
    public HDAscriptControlBLF() {}

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 提交数据的容器 */
    private MMap mMap = new MMap();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessSet mLAAssessSet = new LAAssessSet(); //传入前台送入的考核记录
    private LAAssessSet mLAAssessOutSet = new LAAssessSet(); //考核确认后的结果    
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LATreeSet groupLATreeSet = new LATreeSet(); //降级主管下的业务员
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();//降级主管所管理的团队
    private String mOldGradeEndDate; //被归属人员原来职级的结束日期
    private String mEvaluateDate; //被归属人员新职级的开始日期
    private String mManageCom = ""; // 管理机构
    private String mIndexCalNo = ""; // 考核序列号
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();


    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //取得异动时间
        if (!getEvaluateDate()) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        //统一提交
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        if (tPubSubmit.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeDecGrpYearBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        mInputData.clear();
        mMap.put(mLAAssessOutSet, "UPDATE");
        mMap.put(mLATreeBSet, "INSERT");
        mMap.put(mLABranchGroupSet, "UPDATE");
        mMap.put(mLATreeSet, "UPDATE");
        mInputData.add(mMap);
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */

    private boolean dealData() {

    	LAAssessDB  mLAAssessDB = new LAAssessDB();
    	mLAAssessDB.setManageCom(this.mManageCom);
    	mLAAssessDB.setIndexCalNo(this.mIndexCalNo);
    	mLAAssessDB.setBranchType(this.mBranchType);
    	mLAAssessDB.setBranchType2(this.mBranchType2);
    	mLAAssessDB.setState("1");
    	String strSql = "select * from laassess where indexcalno = '"+this.mIndexCalNo+"' and branchtype = '"+this.mBranchType+"'"
    				  +" and branchtype2 = '"+this.mBranchType2+"' and managecom like '"+this.mManageCom+"%' and state = '1' order by agentgrade desc  ";
    	mLAAssessSet = mLAAssessDB.executeQuery(strSql);  
    	
    	//循环处理每个人的考核信息
    	HDDimissionAppBL tHDDimissionAppBL = new HDDimissionAppBL();
    	HDDimissionBL tHDDimissionBL = new HDDimissionBL();
    	LADimissionSchema tLADimissionSchema = new LADimissionSchema();
    	for (int i = 1; i <= mLAAssessSet.size(); i++) {

        	LAAssessSchema tLAAssessSchema = new LAAssessSchema();
        	tLAAssessSchema = mLAAssessSet.get(i);
        	
    		tLAAssessSchema.setState("2");
            tLAAssessSchema.setConfirmer(mGlobalInput.Operator);
            tLAAssessSchema.setConfirmDate(this.currentDate);
            tLAAssessSchema.setModifyDate(this.currentDate);
            tLAAssessSchema.setModifyTime(this.currentTime);
            mLAAssessOutSet.add(tLAAssessSchema);
        	//如果考核职级为清退，调用离职登记.离职确认接口,如不满足离职条件，则不做组织归属.
        	if("U00".equals(tLAAssessSchema.getAgentGrade1())){
	    		tLADimissionSchema.setAgentCode(tLAAssessSchema.getAgentCode());
	    		tLADimissionSchema.setDepartTimes("1");
	    		tLADimissionSchema.setApplyDate(this.currentDate);
	    		tLADimissionSchema.setDepartDate(this.currentDate);
	    		tLADimissionSchema.setBranchType("5");
	    		tLADimissionSchema.setBranchType2("01");
	    		tLADimissionSchema.setDepartRsn("0");//离职原因，0代表考核
	    		VData cInputData = new VData();  		
	    		cInputData.addElement(tLADimissionSchema);
	    		cInputData.add(this.mGlobalInput);
	    		
	    		if(!tHDDimissionAppBL.submitData(cInputData, "INSERT||MAIN")){
	    			System.out.println("业务员"+tLAAssessSchema.getAgentCode()+"离职登记失败");
	    			continue;
	    		}	    
	    		if(!tHDDimissionBL.submitData(cInputData, "INSERT||MAIN")){
	    			System.out.println("业务员"+tLAAssessSchema.getAgentCode()+"离职确认失败");
	    			continue;
	    		}
	    		System.out.println("U00不需要改变行政职级");
	    		continue;
        	}      
            if (!dealAgentEvaluate(tLAAssessSchema)) {
                CError.buildErr(this, "进行人员归属操作时失败！");
                return false;
            }

    	}
    	
    	return true;
    }

    /**
     * 对每个参加考核的人员进行逐个处理
     * @param pmLAAssessSchema LAAssessSchema
     * @return boolean
     */
    private boolean dealAgentEvaluate(LAAssessSchema pmLAAssessSchema) {

        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(pmLAAssessSchema.getAgentCode());
        tLATreeDB.getInfo();
        LATreeSchema tLATreeSchema = tLATreeDB.getSchema();
        //备份行政信息
        LATreeBSchema tLATreeBSchema = new LATreeBSchema();
        String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        Reflections tRefs = new Reflections();
        tRefs.transFields(tLATreeBSchema, tLATreeSchema);
        tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
        tLATreeBSchema.setOperator(mGlobalInput.Operator);
        tLATreeBSchema.setEdorNO(tEdorNo);
        tLATreeBSchema.setRemoveType("01"); //手工考核确认
        tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
        tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
        tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
        tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
        tLATreeBSchema.setMakeDate(currentDate);
        tLATreeBSchema.setMakeTime(currentTime);
        tLATreeBSchema.setModifyDate(currentDate);
        tLATreeBSchema.setModifyTime(currentTime);
        
        
        
        //更新行政信息 由于助理营业部经理存在降为客户经理三级的情况需要处理
        tLATreeSchema.setAgentLastSeries(tLATreeSchema.getAgentSeries());
        if(tLATreeSchema.getAgentGrade().substring(0,1).equals("V")&&
        	pmLAAssessSchema.getAgentGrade1().substring(0, 1).equals("U")){
        	tLATreeSchema.setAgentSeries("0");
        	tLATreeSchema.setAgentLastSeries("1");
        	LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        	LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        	tLABranchGroupDB.setBranchManager(pmLAAssessSchema.getAgentCode());
        	tLABranchGroupSet = tLABranchGroupDB.query();
        	if(tLABranchGroupSet==null||tLABranchGroupSet.size()==0){
        		System.out.println("该主管没有直辖团队");
        	}else{
        		for(int i=1;i<=tLABranchGroupSet.size();i++){
        			LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        			tLABranchGroupSchema = tLABranchGroupSet.get(i);
        			tLABranchGroupSchema.setBranchManager("");
        			tLABranchGroupSchema.setBranchManagerName("");
        			tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
        			tLABranchGroupSchema.setModifyDate(currentDate);
        			tLABranchGroupSchema.setModifyTime(currentTime);
        			
        			mLABranchGroupSet.add(tLABranchGroupSchema);
        			
        			LATreeSet downLATreeSet  = new LATreeSet();
        			LATreeDB  downLATreeDB = new LATreeDB();
        			downLATreeDB.setAgentGroup(tLABranchGroupSchema.getAgentGroup());
        			downLATreeDB.setBranchType(mBranchType);
        			downLATreeDB.setBranchType2(mBranchType2);
        			
        			downLATreeSet = downLATreeDB.query();
        			
                	if(downLATreeSet==null||downLATreeSet.size()==0){
                		System.out.println("没有需要处理的下级人员");
                	}else{
                		for(int j =1;j<=downLATreeSet.size();j++){
                			LATreeSchema tDownLATreeSchema = new LATreeSchema();
                			tDownLATreeSchema =  downLATreeSet.get(j);
                			tDownLATreeSchema.setUpAgent("");
                			tDownLATreeSchema.setOperator(mGlobalInput.Operator);
                			tDownLATreeSchema.setModifyDate(currentDate);
                			tDownLATreeSchema.setModifyTime(currentTime);
                			
                			groupLATreeSet.add(tDownLATreeSchema);
                			
                		}              		
                		mMap.put(groupLATreeSet, "UPDATE");
                		mInputData.add(mMap);
                		PubSubmit tPubSubmit = new PubSubmit();
                        tPubSubmit.submitData(mInputData, "");
                        if (tPubSubmit.mErrors.needDealError()) {
                            CError tError = new CError();
                            tError.moduleName = "LAAgentGradeDecGrpYearBL";
                            tError.functionName = "submitData";
                            tError.errorMessage = "数据提交失败！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                		
                	}        	
        		}
        	}
        	
        }

        
        tLATreeSchema.setAgentLastGrade(tLATreeBSchema.getAgentGrade());
        tLATreeSchema.setOldStartDate(tLATreeSchema.getStartDate());
        tLATreeSchema.setOldEndDate(PubFun.calDate(mEvaluateDate, -1,
                "D", null));
        tLATreeSchema.setAgentGrade1(pmLAAssessSchema.getAgentGrade1()); //更新业务职级
        tLATreeSchema.setAgentGrade(pmLAAssessSchema.getAgentGrade1());
        if("U00".equals(pmLAAssessSchema.getAgentGrade1())){
        	tLATreeSchema.setAgentSeries("");
        }
        
        if (pmLAAssessSchema.getModifyFlag().equals("01")) {
            tLATreeSchema.setState("2");
        }
        if (pmLAAssessSchema.getModifyFlag().equals("03")) {
            tLATreeSchema.setState("1");
        }
        if (pmLAAssessSchema.getModifyFlag().equals("02")) {
            tLATreeSchema.setState("0");
        }
        tLATreeSchema.setStartDate(mEvaluateDate);
        tLATreeSchema.setAstartDate(mEvaluateDate);
        tLATreeSchema.setModifyDate(currentDate);
        tLATreeSchema.setModifyTime(currentTime);
        tLATreeSchema.setOperator(mGlobalInput.Operator);
        mLATreeSet.add(tLATreeSchema);
        mLATreeBSet.add(tLATreeBSchema);
        
        
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        System.out.println(
                "Begin HDAscriptControlBLF.getInputData.........");
        //全局变量
        System.out.println("Begin HDAscriptControlBLF.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mManageCom = (String)cInputData.getObject(1);
        this.mIndexCalNo =(String)cInputData.getObject(2);
        this.mBranchType =(String)cInputData.getObject(3);
        this.mBranchType2 =(String)cInputData.getObject(4);
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HDAscriptControlBLF";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(mManageCom==null||mManageCom.equals("")||
        		mIndexCalNo==null||mIndexCalNo.equals("")||	
        		mBranchType==null||mBranchType.equals("")||
        		mBranchType2==null||mBranchType2.equals("")){
            CError tError = new CError();
            tError.moduleName = "HDAscriptControlBLF";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }
    /*
    设置上一职级的止期为考核月的工作最后一天,新职级的起期为考核月下一月的工作第一天
    */
   private boolean getEvaluateDate()
   {
       try
       {
           ExeSQL tExeSQL = new ExeSQL();
           SSRS dateR = new SSRS();
           String strSQL;
           //止期：
           strSQL =
                   "select EndDate from LAStatSegment where stattype='5' and yearmonth=" +
                   mIndexCalNo + "";
           dateR = tExeSQL.execSQL(strSQL);
           if (tExeSQL.mErrors.needDealError())
           {

               // @@错误处理
               CError tError = new CError();
               tError.moduleName = "HDAscriptControlBLF";
               tError.functionName = "getEvaluateDate";
               tError.errorMessage = "获取异动日期失败！";
               System.out.println("获取异动日期失败！");
               this.mErrors.addOneError(tError);
               return false;
           
           }
           if (dateR.MaxRow <= 0)
           {
               return false;
           }
           mOldGradeEndDate = dateR.GetText(1, 1);
           //起期：
           mEvaluateDate = PubFun.calDate(mOldGradeEndDate, 1, "D", null);
         
       }
       catch (Exception e)
       {
    	   System.out.println("取得异动日期失败！");
    	   return false;
       }
       System.out.println("取得的异动日期为："+mEvaluateDate);
       return true;
   }

}
