package com.sinosoft.lis.agentassess;

/**
 * <p>Title: AscriptAgentInfoOperateBL类</p>
 * <p>Description: 处理代理人在归属时的基本信息和行政信息</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;



public class AscriptAgentInfoOperateBL {

    public CErrors mErrors = new CErrors();
    public GlobalInput mGlobalInput = new GlobalInput();//操作员的信息

    private String mOldGradeEndDate; //人员原职级结束的日期，由BLF传入
    private String mNewGradeStartDate; //人员新职级开始的日期，由BLF传入
    private VData mOutputData=new VData(); //保存向外层传递的数据
    private String mEdorNo;
    private int mBusinessFlagCheck=0; //业绩更新标志修正值 0-不是人员转正不更新FYC 1-是人员转正更新FYC
    private String mIndexCalNo; //考核计算的编码，由BLF传入

    private LATreeSchema mLATreeSchema=new LATreeSchema();//代理人的行政信息
    private LAAgentSchema mLAAgentSchema=new LAAgentSchema();//代理人的基本信息

    private LATreeSet mLATreeSet=new LATreeSet();//一批代理人的行政信息，等待传入BLS
    private LAAgentSet mLAAgentSet=new LAAgentSet();//一批代理人的基本信息，等待传入BLS
    private LATreeBSet mLATreeBSet=new LATreeBSet();//代理人的行政备份信息，等待传入BLS
    private LAAgentBSet mLAAgentBSet=new LAAgentBSet();//代理人的基本备份信息
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    public AscriptAgentInfoOperateBL() {
    }

    public boolean getInputData(VData cInputData)
    {
        mOldGradeEndDate=(String)cInputData.get(0);
        mNewGradeStartDate=(String)cInputData.get(1);
        mGlobalInput=(GlobalInput)cInputData.getObjectByObjectName(
                     "GlobalInput",2);
        mIndexCalNo=(String)cInputData.get(3);
        if(mNewGradeStartDate!=null&&!mNewGradeStartDate.equals("")
                &&
           mOldGradeEndDate!=null&&!mOldGradeEndDate.equals("")
                &&
           mGlobalInput!=null)
        {
            return true;
        }
        return false;
    }

    private boolean prepareOutputDataToBLS()
    {
        try
        {
            mLATreeSet.add(mLATreeSchema);
            mLAAgentSet.add(mLAAgentSchema);
            mOutputData.clear();
            mOutputData.add(mLATreeSet);
            mOutputData.add(mLAAgentSet);
            mOutputData.add(mLATreeBSet);
            mOutputData.add(mLAAgentBSet);
        }
        catch(Exception e)
        {
            dealErr("准备传向BLS的数据失败！","AscriptAgentInfoOperateBL","prepareOutputDataToBLS");
            return false;
        }
        return true;
    }

    public VData getOutputDataToBLS()
    {
        if(!prepareOutputDataToBLS())
        {
            return null;
        }
        if(mOutputData!=null||mOutputData.size()>0)
        {
            return mOutputData;
        }
        return null;
    }

    private void dealErr(String cMsg,String cMName,String cFName)
    {
        CError tCError=new CError();
        tCError.moduleName=cMName;
        tCError.functionName=cFName;
        tCError.errorMessage=cMsg;
        mErrors.addOneError(tCError);
    }
    //修改代理人的考核日期
    public boolean changeAStartDate(String cAgentCode)
    {
            mLAAgentSchema = new LAAgentSchema();
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(cAgentCode);
            if (!tLATreeDB.getInfo())
            {
                dealErr("查询代理人信息失败！", "AscriptAgentInfoOperateBL",
                        "ChangeAgentGrade");
                return false;
            }
            if (!backupAgentTree(tLATreeDB.getSchema()))
            {
                dealErr("备份代理人行政信息失败！", "AscriptAgentInfoOperateBL",
                        "ChangeAgentGrade");
                return false;
            }

            tLATreeDB.setAstartDate(mNewGradeStartDate);
            tLATreeDB.setModifyDate(currentDate);
            tLATreeDB.setModifyTime(currentTime);
            tLATreeDB.setOperator(mGlobalInput.Operator);
            mLATreeSchema.setSchema(tLATreeDB.getSchema());
            mLATreeSet.add(mLATreeSchema);
            return true;
    }

    /**
     * 改变代理人的职级,同时备份Tree
     * @param cAgentCode String 代理人编码
     * @param cNewGrade String  代理人的新职级
     * @return boolean
     */
    public boolean changeAgentGrade(String cAgentCode, String cNewGrade)
    {
        mLAAgentSchema = new LAAgentSchema();
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(cAgentCode);
        if (!tLATreeDB.getInfo())
        {
            dealErr("查询代理人信息失败！", "AscriptAgentInfoOperateBL",
                    "ChangeAgentGrade");
            return false;
        }
        if (!backupAgentTree(tLATreeDB.getSchema()))
        {
            dealErr("备份代理人行政信息失败！", "AscriptAgentInfoOperateBL",
                    "ChangeAgentGrade");
            return false;
        }
        tLATreeDB.setAgentLastGrade(tLATreeDB.getAgentGrade());
        tLATreeDB.setAgentGrade(cNewGrade);
        tLATreeDB.setOldStartDate(tLATreeDB.getStartDate());
        tLATreeDB.setOldEndDate(mOldGradeEndDate);
        tLATreeDB.setStartDate(mNewGradeStartDate);
        tLATreeDB.setAgentLastSeries(tLATreeDB.getAgentSeries());
        tLATreeDB.setAgentSeries(AgentPubFun.getAgentSeries(cNewGrade));
        tLATreeDB.setAstartDate(mNewGradeStartDate);
        tLATreeDB.setModifyDate(currentDate);
        tLATreeDB.setModifyTime(currentTime);
        tLATreeDB.setOperator(mGlobalInput.Operator);
        mLATreeSchema.setSchema(tLATreeDB.getSchema());
        if("B01".equals(cNewGrade))
        {
        	String serialno = PubFun1.CreateMaxNo(mLATreeSchema.getAgentGroup(),4);
        	String tAgentGroup2= mLATreeSchema.getAgentGroup()+serialno;
        	mLATreeSchema.setAgentGroup2(tAgentGroup2);
        }
        else
        {
        	mLATreeSchema.setAgentGroup2("");
        }
        try
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(mLATreeSchema.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                CError.buildErr(this, "取代理人基本信息失败！");
                return false;
            }
            if (!backupAgent(tLAAgentDB.getSchema()))
            {
                CError.buildErr(this, "备份代理人基本信息失败！");
                return false;
            }
            tLAAgentDB.setInDueFormDate(mNewGradeStartDate);
            tLAAgentDB.setOperator(mGlobalInput.Operator);
            tLAAgentDB.setModifyDate(currentDate);
            tLAAgentDB.setModifyTime(currentTime);
            mLAAgentSchema.setSchema(tLAAgentDB.getSchema());
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }
    /*
      修改cAgentCode的agentgroup和Branchcode为cGroupSchema
     */
    public boolean updateAgentInfo(String cAgentCode, LABranchGroupSchema cGroupSchema)
    {
        LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeDB tLAUpAgentTreeDB = new LATreeDB();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(cAgentCode);
        if (!tLAAgentDB.getInfo()) {
            dealErr("取代理人基本信息失败！", "AscriptAgentInfoOperateBL",
                    "updateAgentInfo");
            return false;
        }
        if (!backupAgent(tLAAgentDB.getSchema())) {
            dealErr("备份代理人基本信息失败！", "AscriptAgentInfoOperateBL",
                    "changeAgentGroup");
            return false;
        }
        tLAUpAgentTreeDB.setAgentCode(cAgentCode);
        if (!tLAUpAgentTreeDB.getInfo()) {
            dealErr("取代理人基本信息失败！", "AscriptAgentInfoOperateBL",
                    "updateAgentInfo");
            return false;
        }
        if (!backupAgentTree(tLAUpAgentTreeDB.getSchema())) {
            dealErr("备份代理人基本信息失败！", "AscriptAgentInfoOperateBL",
                    "updateAgentInfo");
            return false;
        }
        tLATreeSchema = tLAUpAgentTreeDB.getSchema();
        String tNewGroup = cGroupSchema.getAgentGroup();
        String tBranchManager = cGroupSchema.getBranchManager();

        tLATreeSchema.setAgentGroup(tNewGroup);
        tLAAgentDB.setAgentGroup(tNewGroup);
        //降级为业务员
        tLATreeSchema.setUpAgent(tBranchManager);

        tLATreeSchema.setBranchCode(tNewGroup);
        tLAAgentDB.setBranchCode(tNewGroup);

        tLATreeSchema.setModifyDate(currentDate);
        tLATreeSchema.setModifyTime(currentTime);
        tLATreeSchema.setOperator(mGlobalInput.Operator);
        tLAAgentDB.setModifyDate(currentDate);
        tLAAgentDB.setModifyTime(currentTime);
        tLAAgentDB.setOperator(mGlobalInput.Operator);
        mLATreeSet.add(tLATreeSchema);
        mLAAgentSet.add(tLAAgentDB.getSchema());
        return true;
    }
    /*
     改变代理人的机构,同时备份Agent
     参数:cAgentcode-代理人编码 cNewGrade-代理人的新职级 cNewBranchAttr-新机构的外部编码（如果是空，则不保存这个人的调动轨迹） cNewGroup-代理人目的机构编码 cBranchManager-新机构的主管
     */
    public boolean changeAgentGroup(String cAgentCode,String cNewBranchAttr,String cNewGroup,String cBranchManager)
    {
        LATreeSchema tLATreeSchema=new LATreeSchema();
        LATreeDB tLAUpAgentTreeDB=new LATreeDB();
        LAAgentDB tLAAgentDB=new LAAgentDB();
        tLAAgentDB.setAgentCode(cAgentCode);
        tLATreeSchema.setSchema(mLATreeSchema);//操作对象要保持一致
        if(mLAAgentSchema.getAgentCode()==null)//这样来防止特殊情况干扰更新，20050705 zxs
        {
            if (!tLAAgentDB.getInfo()) {
                dealErr("取代理人基本信息失败！", "AscriptAgentInfoOperateBL",
                        "changeAgentGroup");
                return false;
            }
            if (!backupAgent(tLAAgentDB.getSchema())) {
                dealErr("备份代理人基本信息失败！", "AscriptAgentInfoOperateBL",
                        "changeAgentGroup");
                return false;
            }
        }
        else
        {
            tLAAgentDB.setSchema(mLAAgentSchema);
        }

         //保存机构变动轨迹
         //都不在这里保存了 20050823 zxs
//         String strSQL="select BranchLevelProperty2 from labranchlevel where BranchLevelCode='01' and exists " +
//         "(select 'Y' from latree where agentcode='"+cAgentCode+"' and branchtype=labranchlevel.branchtype and branchtype2=labranchlevel.branchtype2 ) ";
//        ExeSQL tExeSQL=new ExeSQL();
//        String BranchAttrLength=tExeSQL.getOneValue(strSQL);
//        if(BranchAttrLength==null||BranchAttrLength.equals(""))
//        {
//            dealErr("查询机构信息属性失败！","AscriptAgentInfoOperateBL","changeAgentGroup");
//            return false;
//        }
//        if(cNewBranchAttr.length()==Integer.parseInt(BranchAttrLength))
//        {//是组的时候才在这里保存
//            if (cNewBranchAttr != null && !cNewBranchAttr.equals("")) {
//                if (!saveLAManoeuvre(cAgentCode, "", "", cNewBranchAttr, "", cNewGroup)) {
//                    return false;
//                }
//            }
//        }
        tLATreeSchema.setAgentGroup(cNewGroup);
        tLAAgentDB.setAgentGroup(cNewGroup);
        if((tLATreeSchema.getAgentSeries().equals("1")&&
            tLATreeSchema.getAgentLastSeries().equals("0"))
           ||
           (tLATreeSchema.getAgentSeries().equals("1")&&
            tLATreeSchema.getAgentLastGrade().equals("B01"))  //生成主任
           ||
            (tLATreeSchema.getAgentSeries().equals("0"))//降为业务员
           ||
           (tLATreeSchema.getAgentSeries().equals("1") &&  //降为B01
            tLATreeSchema.getAgentGrade().equals("B01")) &&
            tLATreeSchema.getAgentLastSeries().equals("1")  )
        {//存在上级机构说明被归属的人变成(升)主任了或者是降职为业务员,就要修改他的branchcode和upagent
            if(tLATreeSchema.getAgentSeries().equals("1"))
            {//是升为组长
                tLAUpAgentTreeDB.setAgentCode(tLATreeSchema.getUpAgent());
                if(tLAUpAgentTreeDB.getAgentCode()==null||tLAUpAgentTreeDB.getAgentCode().equals(""))
                {
                    tLATreeSchema.setUpAgent("");
                }
                else
                {
                    if (!tLAUpAgentTreeDB.getInfo()) {
                        dealErr("取代理人原上级代理人行政信息失败！",
                                "AscriptAgentInfoOperateBL", "changeAgentGroup");
                        return false;
                    }
                    if (tLAUpAgentTreeDB.getAgentSeries().equals("1")) { //归属时的上司是主任
                        tLATreeSchema.setUpAgent(tLAUpAgentTreeDB.getUpAgent());
                    } else { //归属时的上司是部长
                        tLATreeSchema.setUpAgent(tLAUpAgentTreeDB.getAgentCode());
                    }
                }
            }
            else
            {//降级为业务员
                tLATreeSchema.setUpAgent(cBranchManager);
            }
            tLATreeSchema.setBranchCode(cNewGroup);
            tLAAgentDB.setBranchCode(cNewGroup);
        }
        else if(tLATreeSchema.getAgentSeries().equals("2")//升为部长
                ||
                (tLATreeSchema.getAgentSeries().equals("1")&&tLATreeSchema.getAgentLastSeries().equals("2"))//降为主任
                )
        {//不存在上级机构,说明被归属的人晋升部长了，或者是降级为主任，回归到一个部下边


            if(tLATreeSchema.getAgentSeries().equals("2"))
            {//升为区级经理
                tLAUpAgentTreeDB.setAgentCode(tLATreeSchema.getUpAgent());
                if (tLAUpAgentTreeDB.getAgentCode() == null ||
                    tLAUpAgentTreeDB.getAgentCode().equals(""))
                {
                    tLATreeSchema.setUpAgent("");
                } else
                {
                    if (!tLAUpAgentTreeDB.getInfo()) {
                        dealErr("取代理人原上级代理人行政信息失败！",
                                "AscriptAgentInfoOperateBL",
                                "changeAgentGroup");
                        return false;
                    }
                    if (tLAUpAgentTreeDB.getAgentSeries().equals("2")) { //归属时的上司是区经理
                       tLATreeSchema.setUpAgent(tLAUpAgentTreeDB.getUpAgent());
                   } else { //归属时的上司是部长
                       tLATreeSchema.setUpAgent(tLAUpAgentTreeDB.getAgentCode());
                   }

                }
            }
            else
            {//降为主任
                tLATreeSchema.setUpAgent(cBranchManager);
            }
        }
        tLATreeSchema.setModifyDate(currentDate);
        tLATreeSchema.setModifyTime(currentTime);
        tLATreeSchema.setOperator(mGlobalInput.Operator);
        tLAAgentDB.setModifyDate(currentDate);
        tLAAgentDB.setModifyTime(currentTime);
        tLAAgentDB.setOperator(mGlobalInput.Operator);
        mLATreeSchema.setSchema(tLATreeSchema);
        mLAAgentSchema.setSchema(tLAAgentDB.getSchema());
        return true;
    }
    /*
         改变代理人的机构,同时备份Agent
         参数:cAgentcode-代理人编码 cNewGrade-代理人的新职级 cNewBranchAttr-新机构的外部编码（如果是空，则不保存这个人的调动轨迹） cNewGroup-代理人目的机构编码 cBranchManager-新机构的主管
         */
    public boolean changeManagerInfo(LATreeSchema tLATreeSchema,String cBranchManager)
    {
        tLATreeSchema.setUpAgent(cBranchManager);
        tLATreeSchema.setModifyDate(currentDate);
        tLATreeSchema.setModifyTime(currentTime);
        tLATreeSchema.setOperator(mGlobalInput.Operator);
        mLATreeSet.add(tLATreeSchema);
        return true;
    }
    /**
     *
     * 参数:cLATreeSchema-要备份的代理人行政信息
     */
    private boolean backupAgentTree(LATreeSchema cLATreeSchema)
    {
        LATreeBSchema tLATreeBSchema=new LATreeBSchema();
        Reflections tReflections=new Reflections();
        tReflections.transFields(tLATreeBSchema,cLATreeSchema);
        tLATreeBSchema.setOperator2(cLATreeSchema.getOperator());
        tLATreeBSchema.setMakeDate2(cLATreeSchema.getMakeDate());
        tLATreeBSchema.setModifyDate2(cLATreeSchema.getModifyDate());
        tLATreeBSchema.setMakeTime2(cLATreeSchema.getMakeTime());
        tLATreeBSchema.setModifyTime2(cLATreeSchema.getModifyTime());

        tLATreeBSchema.setEdorNO(mEdorNo);
        tLATreeBSchema.setRemoveType("01");//考核结果
        tLATreeBSchema.setMakeDate(currentDate);
        tLATreeBSchema.setMakeTime(currentTime);
        tLATreeBSchema.setModifyDate(currentDate);
        tLATreeBSchema.setModifyTime(currentTime);
        tLATreeBSchema.setOperator(mGlobalInput.Operator);
        tLATreeBSchema.setIndexCalNo(mIndexCalNo);
        mLATreeBSet.add(tLATreeBSchema);
        return true;
    }

    /**
     *
     * @param cLATreeSchema LATreeSchema 要备份的代理人行政信息
     * @param cGlobalInput GlobalInput 非本对象的信息
     * @param cLATreeBDBSet LATreeBSet 非本对象的操作对象
     * @return boolean
     */
    public boolean backupAgentTree(LATreeSchema cLATreeSchema,
                                   GlobalInput cGlobalInput,
                                   LATreeBSet cLATreeBDBSet)
    {
        LATreeBSchema tLATreeBSchema = new LATreeBSchema();
        Reflections tReflections = new Reflections();

        tReflections.transFields(tLATreeBSchema, cLATreeSchema);
        tLATreeBSchema.setOperator2(cLATreeSchema.getOperator());
        tLATreeBSchema.setMakeDate2(cLATreeSchema.getMakeDate());
        tLATreeBSchema.setModifyDate2(cLATreeSchema.getModifyDate());
        tLATreeBSchema.setMakeTime2(cLATreeSchema.getMakeTime());
        tLATreeBSchema.setModifyTime2(cLATreeSchema.getModifyTime());

        tLATreeBSchema.setEdorNO(mEdorNo);
        tLATreeBSchema.setRemoveType("01"); //考核结果
        tLATreeBSchema.setMakeDate(currentDate);
        tLATreeBSchema.setMakeTime(currentTime);
        tLATreeBSchema.setModifyDate(currentDate);
        tLATreeBSchema.setModifyTime(currentTime);
        tLATreeBSchema.setOperator(cGlobalInput.Operator);
        tLATreeBSchema.setIndexCalNo(mIndexCalNo);
        cLATreeBDBSet.add(tLATreeBSchema);
        return true;
    }
    /*
         备份代理人的基本信息
         参数:cLAAgentSchema-要备份的代理人的基本信息
         */
        private boolean backupAgent(LAAgentSchema cLAAgentSchema)
        {
            LAAgentSchema tLAAgentSchema=new LAAgentSchema();
            LAAgentBSchema tLAAgentBSchema=new LAAgentBSchema();
            Reflections tReflections=new Reflections();
            tLAAgentSchema=cLAAgentSchema;
            System.out.println(cLAAgentSchema.getAgentCode());
            tReflections.transFields(tLAAgentBSchema,tLAAgentSchema);
            System.out.println(tLAAgentBSchema.getAgentCode());
            tLAAgentBSchema.setEdorNo(mEdorNo);
            tLAAgentBSchema.setEdorType("01");
            tLAAgentBSchema.setMakeDate(currentDate);
            tLAAgentBSchema.setMakeTime(currentTime);
            tLAAgentBSchema.setModifyDate(currentDate);
            tLAAgentBSchema.setModifyTime(currentTime);
            tLAAgentBSchema.setOperator(mGlobalInput.Operator);
            tLAAgentBSchema.setIndexCalNo(mIndexCalNo);
            mLAAgentBSet.add(tLAAgentBSchema);
            return true;
    }

    /*
         备份代理人的基本信息,方法复用,主要是为非本对象调用准备的
         参数:cLATreeSchema-要备份的代理人基本信息 cGlobalInput-非本对象的信息 cLAAgentBDBSet-非本对象的操作对象
         */
    public boolean backupAgent(LAAgentSchema cLAAgentSchema, GlobalInput cGlobalInput,LAAgentBSet cLAAgentBDBSet)//重载
        {
            LAAgentSchema tLAAgentSchema=new LAAgentSchema();
            LAAgentBSchema tLAAgentBSchema=new LAAgentBSchema();
            Reflections tReflections=new Reflections();
            tLAAgentSchema=cLAAgentSchema;
            tReflections.transFields(tLAAgentBSchema,tLAAgentSchema);
            tLAAgentBSchema.setEdorNo(mEdorNo);
            tLAAgentBSchema.setEdorType("01");
            tLAAgentBSchema.setMakeDate(currentDate);
            tLAAgentBSchema.setMakeTime(currentTime);
            tLAAgentBSchema.setModifyDate(currentDate);
            tLAAgentBSchema.setModifyTime(currentTime);
            tLAAgentBSchema.setOperator(cGlobalInput.Operator);
            tLAAgentBSchema.setIndexCalNo(mIndexCalNo);
            cLAAgentBDBSet.add(tLAAgentBSchema);
            return true;
    }

    /*
     改变一个部下的所有的组长的上级代理人
     参数：cOldUpAgent-部的内部编码,cNewUpAgent-新的上级代理人的编码
     */
    public boolean changeUpAgentSet(String cOldUpAgent,String cNewUpAgent,String cDownAgentSeries)
    {
        LATreeSet tLATreeSet=new LATreeSet();
        LATreeDB tLATreeDB=new LATreeDB();
        LATreeSchema tLATreeSchema;
        String strSQL="select * from latree a,laagent b where a.agentcode=b.agentcode and upagent='"
                      +cOldUpAgent+
                    //  "' and AgentSeries='"+cDownAgentSeries+
                      "' and b.agentstate<='04'  and a.agentcode<>'"+cOldUpAgent+"'";
        tLATreeSet=tLATreeDB.executeQuery(strSQL);
        if(tLATreeDB.mErrors.needDealError())
        {
            dealErr(tLATreeDB.mErrors.getFirstError(),"AscriptAgentInfoOperateBL","ChangeUpAgentSet");
            return false;
        }
        for(int i=1;i<=tLATreeSet.size();i++)
        {
            tLATreeSchema=new LATreeSchema();
            tLATreeSchema=tLATreeSet.get(i);
            if (!backupAgentTree(tLATreeSchema.getSchema()))
            {
                dealErr("备份代理人行政信息失败！", "AscriptAgentInfoOperateBL",
                        "ChangeUpAgentSet");
                return false;
            }
            tLATreeSchema.setUpAgent(cNewUpAgent);
            mLATreeSet.add(tLATreeSchema);
        }

        return true;
    }
    public boolean changeUpAgentSchema(String cOldUpAgent,String cNewUpAgent,String cDownAgentSeries)
    {
        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeSchema.setSchema(mLATreeSchema);
//        if (!backupAgentTree(tLATreeSchema.getSchema())) {
//            dealErr("备份代理人行政信息失败！", "AscriptAgentInfoOperateBL",
//                    "ChangeUpAgentSet");
//            return false;
//        }
        tLATreeSchema.setUpAgent(cNewUpAgent);
        mLATreeSchema.setSchema(tLATreeSchema);
        return true;
    }
    /*
     改变一批代理人的行政信息,同时备份Tree和Agent
     参数: cAgentInfoSet-存放代理人和机构的信息,包含代理人的基本信息和行政信息,还有目标机构
     */
    public boolean changeAgentGroupSet(VData cAgentInfoSet, LABranchGroupSet cLABranchGroupSet)
    {
        LABranchGroupSchema tLABranchGroupSchema=new LABranchGroupSchema();
        LATreeSet tLATreeSet=new LATreeSet();
        LAAgentSet tLAAgentSet=new LAAgentSet();

        tLABranchGroupSchema=(LABranchGroupSchema)cAgentInfoSet.getObjectByObjectName(
                             "LABranchGroupSchema",0);
        tLATreeSet=(LATreeSet)cAgentInfoSet.getObjectByObjectName(
                             "LATreeSet",0);
        tLAAgentSet=(LAAgentSet)cAgentInfoSet.getObjectByObjectName(
                             "LAAgentSet",0);
        LATreeSchema tLATreeSchema=new LATreeSchema();
        LAAgentSchema tLAAgentSchema=new LAAgentSchema();
        if(tLABranchGroupSchema.getBranchLevel().equals("01"))
        {//目标机构是组
            for(int i=1;i<=tLATreeSet.size();i++)
            {
                tLATreeSchema=tLATreeSet.get(i);
                tLAAgentSchema=tLAAgentSet.get(i);
                if(tLAAgentSchema.getAgentCode().equals(mLAAgentSchema.getAgentCode()))
                {//防止remove方法失败 20050823
                    continue;
                }
                if(!backupAgentTree(tLATreeSchema))
                {
                    dealErr("备份代理人行政信息失败！","AscriptAgentInfoOperateBL","changeAgentGroupSet");
                    return false;
                }
                tLATreeSchema.setAgentGroup(tLABranchGroupSchema.getAgentGroup());
                tLATreeSchema.setBranchCode(tLABranchGroupSchema.getAgentGroup());
                tLATreeSchema.setUpAgent(tLABranchGroupSchema.getBranchManager());
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(mGlobalInput.Operator);

                if(!backupAgent(tLAAgentSchema))
                {
                    dealErr("备份代理人基本信息失败！","AscriptAgentInfoOperateBL","changeAgentGroupSet");
                    return false;
                }
                tLAAgentSchema.setAgentGroup(tLABranchGroupSchema.getAgentGroup());
                tLAAgentSchema.setBranchCode(tLABranchGroupSchema.getAgentGroup());
                tLAAgentSchema.setModifyDate(currentDate);
                tLAAgentSchema.setModifyTime(currentTime);
                tLAAgentSchema.setOperator(mGlobalInput.Operator);
                mLAAgentSet.add(tLAAgentSchema);
                mLATreeSet.add(tLATreeSchema);
            }
        }
        else if(tLABranchGroupSchema.getBranchLevel().equals("02"))
        {//目的机构是区
            for(int i=1;i<=tLATreeSet.size();i++)
            {
                tLATreeSchema=tLATreeSet.get(i);
                if(!backupAgentTree(tLATreeSchema))
                {
                    dealErr("备份代理人行政信息失败！","AscriptAgentInfoOperateBL","changeAgentGroupSet");
                    return false;
                }
                tLATreeSchema.setUpAgent(tLABranchGroupSchema.getBranchManager());
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(mGlobalInput.Operator);
                mLATreeSet.add(tLATreeSchema);

                String OldAttr = getBranchAttr(tLATreeSchema.getAgentCode(), "","BranchAttr");//可以减少大量的查询次数
                String NewAttr = cLABranchGroupSet.get(i+1).getBranchAttr();
                String OldGroup = getBranchAttr(tLATreeSchema.getAgentCode(), "","AgentGroup");//可以减少大量的查询次数
		String NewGroup = cLABranchGroupSet.get(i+1).getAgentGroup();
                String OldSeries= getBranchAttr(tLATreeSchema.getAgentCode(), "","BranchSeries");//可以减少大量的查询次数
                String NewSeries=cLABranchGroupSet.get(i+1).getBranchSeries();

                if(OldAttr==null||OldAttr.equals("")||NewAttr==null||NewAttr.equals(""))
                {
                    return false;
                }
            }
        }
        return true;
    }

    /*
     得到当前的被归属的代理人的行政信息操作对象
     */
    public LATreeSchema getCurAgentTree()
    {
        return mLATreeSchema;
    }

    /*
         更新当前的被归属的代理人的行政信息操作对象
     */
    public void setCurAgentTree(LATreeSchema cLATreeSchema)
    {
        mLATreeSchema.setSchema(cLATreeSchema);
    }

    /*
         得到当前的被归属的代理人的基本信息操作对象
     */
    public LAAgentSchema getCurAgent()
    {
        return mLAAgentSchema;
    }

    /*
         更新当前的被归属的代理人的基本信息操作对象
     */
    public void setCurAgent(LAAgentSchema cLAAgentSchema)
    {
        mLAAgentSchema.setSchema(cLAAgentSchema);
    }

    public void setmEdorNo(String cEdorNo)
    {
        mEdorNo=cEdorNo;
    }


    /**
     *  检查职级是否为试用职级
     * @param cAgentGrade String
     * @return boolean true-试用 false-转正
     */

    private boolean checkGrade(String cAgentGrade)
    {
        ExeSQL tExeSQL=new ExeSQL();
        String GradePropertyR= "";
        String sql = "";
        try
        {
            sql = "select GradeProperty4 from LAAgentGrade where gradecode='"+cAgentGrade+"'";
            GradePropertyR=tExeSQL.getOneValue(sql);
        }
        catch (Exception ex)
        {
           CError.buildErr(this,"查询职级"+cAgentGrade+"的属性失败！");
           return false;
        }
        if(GradePropertyR.equals("0"))
        {
            return true;
        }
        else
            return false;
    }


    /*
     查询指定条件的机构外部编码
     参数：cAgentCode-以代理人编码做条件，cAgentGroup-以机构编码做条件, cColName-字段
     说明：如果两个条件都存在的时候优先以cAgentGroup做条件 cColName：BranchAttr和AgentGroup
     */
    private String getBranchAttr(String cAgentCode,String cAgentGroup,String cColName)
    {
        ExeSQL tExeSQL=new ExeSQL();
        SSRS attrR=new SSRS();
        String SQLstr="select "+cColName+" from labranchgroup where agentgroup=";
        if(cAgentCode==null||cAgentCode.equals(""))
        {
            SQLstr+="'"+cAgentGroup+"'";
        }
        else if(cAgentGroup==null||cAgentGroup.equals(""))
        {
            SQLstr+="(select branchcode from latree where agentcode='"+cAgentCode+"')";
        }
        else
        {
            dealErr("没有传递查询条件，不能查询机构外部编码！","AscriptAgentInfoOperateBL","getBranchAttr");
            return "";
        }
        attrR=tExeSQL.execSQL(SQLstr);
        if(tExeSQL.mErrors.needDealError())
        {
            dealErr(tExeSQL.mErrors.getFirstError(),"AscriptAgentInfoOperateBL","getBranchAttr");
            return "";
        }
        if(attrR.MaxRow<=0)
        {
            dealErr("查询机构的外部编码失败！","AscriptAgentInfoOperateBL","getBranchAttr");
            return "";
        }
        return attrR.GetText(1,1);
    }

    private String getAgentInfo(String cAgentCode,String cTableName,String cColName)
    {
        ExeSQL tExeSQL=new ExeSQL();
        SSRS infoR=new SSRS();
        String SQLstr="select "+cColName+" from "+cTableName+" where agentcode = '"+cAgentCode+"' ";
        infoR=tExeSQL.execSQL(SQLstr);
        if(tExeSQL.mErrors.needDealError())
        {
            dealErr(tExeSQL.mErrors.getFirstError(),"AscriptAgentInfoOperateBL","getAgentInfo");
            return "";
        }
        if(infoR.MaxRow<=0)
        {
            dealErr("查询代理人信息失败！","AscriptAgentInfoOperateBL","getAgentInfo");
            return "";
        }
        return infoR.GetText(1,1);
    }

    public int getBusinessFlagCheck()
    {
        return mBusinessFlagCheck;
    }
}
