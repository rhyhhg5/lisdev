package com.sinosoft.lis.agentassess;

import java.util.Date;

import com.sinosoft.lis.agentcalculate.AgCalBase;
import com.sinosoft.lis.agentcalculate.CalIndex;
import com.sinosoft.lis.bl.LAAgentBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessIndexDB;
import com.sinosoft.lis.db.LAIndexVsAssessDB;
import com.sinosoft.lis.db.LAStatSegmentDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAStatSegmentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAStatSegmentSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/*
 * <p>Title: 银行考核指标计算流程类 </p>
 * <p>Description: 考核计算的流程控制类 </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 * @date 2003-05-23
 */
public class CalExamine
{
    //全局变量
    public CErrors mErrors = new CErrors(); //错误处理类
    public GlobalInput mGlobalInput = new GlobalInput(); //保存传入参数

    //必须初始化的行政信息
    private String ManageCom; //管理机构
    private String AgentGrade; //考核职级
    private String Year; //指标计算年份
    private String Month; //指标计算月份
    private String BranchType; //指标计算月份
    private String mAreaType; //地区类型

    //操作人员
    private String Operator;

    //计算需要的人员信息
    private String AgentCode; //代理人代码
    private LATreeSet mLATreeSet; //代理人集和
    private int iMax; //代理人个数

    //计算需要的计算信息
    private String IndexCalNo; //考核计算编码
    private LAAssessIndexDB mLAAssessIndexDB; //存指标信息
    private CalIndex tCalIndex = new CalIndex(); //指标计算类

    public CalExamine()
    {
    }

    //提交函数,参数入口
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.getInputData(cInputData); //初始化考核参数
        if (!this.checkInputData()) //校验入参
        {
            if (mErrors.needDealError())
            {
                System.out.println("校验函数异常结束原因：" + mErrors.getFirstError());
            }
            return false;
        }
        if (cOperate.equals("Bank"))
        {
            if (!Examine()) //主函数
            {
                if (mErrors.needDealError())
                {
                    System.out.println("考核异常结束原因：" + mErrors.getFirstError());
                }
                return false;
            }
        }
        if (cOperate.equals("Grp"))
        {
            if (!ExamineGrp()) //计算法人主函数
            {
                if (mErrors.needDealError())
                {
                    System.out.println("考核异常结束原因：" + mErrors.getFirstError());
                }
                return false;
            }
        }
        return true;
    }

    private void getInputData(VData cInputData)
    {
        //信息收集
        try
        {
            this.ManageCom = (String) cInputData.getObject(0);
            this.Year = (String) cInputData.getObject(1);
            this.Month = (String) cInputData.getObject(2);
            this.BranchType = (String) cInputData.getObject(3);
            this.mGlobalInput.setSchema((GlobalInput) cInputData
                                        .getObjectByObjectName("GlobalInput", 4));
            this.Operator = mGlobalInput.Operator;
            System.out.println("--------    getInputData    --------");
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }

    //考核主函数
    private boolean Examine()
    {
        IndexCalNo = Year + Month;
        LDCodeSet tLDCodeSet = new LDCodeSet();
        LDCodeDB tLDCodeDB = new LDCodeDB();
        String cType="";
        if(BranchType.equals("2"))  cType="02";
        if(BranchType.equals("1"))  cType="01";
        mAreaType = AgentPubFun.getAreaType(ManageCom.substring(0, 4),cType); //通过管理机构计算地区类型，现在有‘01’‘02’两版方案

        String tSql = "";

        //1.银代考核职级查询
        tSql =
                "select * from ldcode where codetype = 'agentgrade' and codealias='"
                + BranchType + "' order by code ,othersign asc";
        tLDCodeSet = tLDCodeDB.executeQuery(tSql);
        if (tLDCodeSet.get(1).getCode() == null)
        {
            return false;
        }

        //2.对每一个职级做考核
        //     ExaminePub examinePub;
        //     examinePub=new ExaminePub(this.Year,this.Month,this.Operator);//生成一个考核公共类
        for (int i = 1; i <= tLDCodeSet.size(); i++) //对每一个职级做考核。
        {
            AgentGrade = tLDCodeSet.get(i).getCode();
            System.out.println(
                    "<程序入口通知你：-------------------------------对这个职级进行考评： "
                    + AgentGrade + "----------------------------------->");
            //       if(!examinePub.addGrade(AgentGrade)) return false;
            String tSQL = "";
            int iMax = 0;
            LATreeSet mLATreeSet; //代理人集和

            LATreeDB tLATreeDB = new LATreeDB();
            tSQL = "select * from LATree a where ManageCom like '" + ManageCom
                   + "%' and AgentGrade = '" + AgentGrade
                   +
                   "' and exists (select 'X' from laagent where agentstate in ('01','02')"
                   + " and a.agentcode=agentcode) and not exists (select 'X' from labranchgroup where a.agentgroup =agentgroup "
                   + "and state='1') order by AgentCode";
            mLATreeSet = tLATreeDB.executeQuery(tSQL);
            iMax = mLATreeSet.size();
            LATreeSchema tLATreeSchema = new LATreeSchema();
            System.out.println("该管理机构的" + AgentGrade + "职级下的业务员个数：" + iMax);

            for (int a = 1; a <= iMax; a++)
            {
                tLATreeSchema = new LATreeSchema();
                tLATreeSchema = mLATreeSet.get(a);
                AgentCode = tLATreeSchema.getAgentCode();
                System.out.println("------------开始按指标计算每个人" + AgentCode
                                   + "-----------------");

                //4.对每个代理人进行考核
                if (!getBeginEnd()) //得到起止日期，不同职级不同.和考核标志
                {
                    return false;
                }
                tCalIndex.tAgCalBase.setAgentGroup(tLATreeSchema.getAgentGroup());

                //9.具体计算
                if (!WageCal()) //代理人代码和其职级
                {
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }
                System.out.println("***************一个人终于考核完了******************");
            }
            System.out.println(
                    "<程序入口通知你：-------------------------------对这个职级进行考评： "
                    + AgentGrade + " 结束了!-------------------------->");
        }
        return true;
    }

    //考核主函数
    private boolean ExamineGrp()
    {
        IndexCalNo = Year + Month;
        LDCodeSet tLDCodeSet = new LDCodeSet();
        LDCodeDB tLDCodeDB = new LDCodeDB();
        String cType="";
        if(BranchType.equals("2"))  cType="02";
        if(BranchType.equals("1"))  cType="01";
        mAreaType = AgentPubFun.getAreaType(ManageCom.substring(0, 4),cType); //通过管理机构计算地区类型，现在有‘01’‘02’两版方案

        String tSql = "";

        //1.法人考核职级查询
        tSql =
                "select * from ldcode where codetype = 'agentgrade' and codealias='"
                + BranchType + "' order by code ,othersign asc";
        tLDCodeSet = tLDCodeDB.executeQuery(tSql);
        if (tLDCodeSet.get(1).getCode() == null)
        {
            return false;
        }

        //2.对每一个职级做考核
        for (int i = 1; i <= tLDCodeSet.size(); i++) //对每一个职级做考核。
        {
            AgentGrade = tLDCodeSet.get(i).getCode();
            System.out.println(
                    "<程序入口通知你：-------------------------------对这个职级进行考评： "
                    + AgentGrade + "----------------------------------->");
            String tSQL = "";
            int iMax = 0;
            LATreeSet mLATreeSet; //代理人集和

            //3.在树状表中查询该管理机构下该职级下所有的代理人
            LATreeDB tLATreeDB = new LATreeDB();
            tSQL = "select * from LATree a where ManageCom like '" + ManageCom
                   + "%' and AgentGrade = '" + AgentGrade
                   +
                   "' and exists (select 'X' from laagent where agentstate in ('01','02')"
                   + " and a.agentcode=agentcode) and not exists (select 'X' from labranchgroup where a.agentgroup =agentgroup "
                   + "and state='1') order by AgentCode";
            System.out.println("查询代理人" + tSQL);
            mLATreeSet = tLATreeDB.executeQuery(tSQL);
            iMax = mLATreeSet.size();
            LATreeSchema tLATreeSchema = new LATreeSchema();
            System.out.println("该管理机构的" + AgentGrade + "职级下的业务员个数：" + iMax);

            for (int a = 1; a <= iMax; a++)
            {
                tLATreeSchema = new LATreeSchema();
                tLATreeSchema = mLATreeSet.get(a);
                AgentCode = tLATreeSchema.getAgentCode();
                System.out.println("------------开始按指标计算每个人" + AgentCode
                                   + "-----------------");

                //4.对每个代理人进行考核
                if (!getBeginEnd()) //得到起止日期，不同职级不同.和考核标志
                {
                    return false;
                }
                tCalIndex.tAgCalBase.setAgentGroup(tLATreeSchema.getAgentGroup());

                //9.具体计算
                if (!WageCalGrp()) //代理人代码和其职级
                {
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }
                System.out.println("***************一个人终于考核完了******************");
            }
            System.out.println(
                    "<程序入口通知你：-------------------------------对这个职级进行考评： "
                    + AgentGrade + " 结束了!-------------------------->");
        }
        return true;
    }

    //10.给基数类赋值
    private boolean setBaseValue(AgCalBase cAgCalBase) //, String cAgentCode,String cGrade)
    {
        tCalIndex.tAgCalBase.setAgentCode(AgentCode);
        tCalIndex.tAgCalBase.setWageNo(IndexCalNo);
        tCalIndex.tAgCalBase.setBranchAttr(AgentPubFun.getAgentBranchAttr(
                AgentCode));

        System.out.println("cAgCalBase.getTempBegin: "
                           + cAgCalBase.getTempBegin());
        System.out.println("cAgCalBase.getTempEnd: " + cAgCalBase.getTempEnd());
        return true;
    }

    private boolean insertAssess(String cIndexCalNo, String cAgentCode,
                                 String cGrade, String cAssessCode,
                                 String cIndexValue)
    {
        String tSQL = "";
        String tCount = "";
        String TableName = "";
        String IColName = "";
        String tModifyFlag = "";
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        boolean tReturn = false;

        //查询佣金对应的表名和字段名
        LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
        tLAIndexVsAssessDB.setAgentGrade(cGrade);
        tLAIndexVsAssessDB.setAssessCode(cAssessCode);
        tLAIndexVsAssessDB.getInfo();
        TableName = tLAIndexVsAssessDB.getATableName();
        IColName = tLAIndexVsAssessDB.getAColName();
        ExeSQL tExeSQL = new ExeSQL();

        //先判断记录是否已存在
        tSQL = "select AgentCode from " + TableName + " where IndexCalNo = '"
               + cIndexCalNo + "' and AgentCode = '" + cAgentCode + "'";
        System.out.println("查询考核指标信息表是否已存在指标记录的SQL:  " + tSQL);
        tCount = tExeSQL.getOneValue(tSQL);
        System.out.println("该代理人的考核指标信息表记录是否已存在：" + tCount + "aa");
        if (tCount.trim().equals(""))
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(cAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                //@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalIndex";
                tError.functionName = "insertIndex";
                tError.errorMessage = "代理人代码非法!" + cAgentCode.trim();
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("Operator:" + Operator);
            System.out.println("laassess");
            if (cGrade.compareTo(cIndexValue) > 0) //降级
            {
                tModifyFlag = "01";
            }
            else if (cGrade.compareTo(cIndexValue) < 0) //升级
            {
                tModifyFlag = "03";
            }
            tSQL = "insert into " + TableName
                   +
                   "( IndexCalNo,AgentCode,AgentGroup,ManageCom,BranchType,ModifyFlag,"
                   + " AgentSeries,AgentGrade,AgentSeries1," + IColName
                   + ",operator,state,makeDate,makeTime,modifyDate,"
                   + " modifyTime,branchattr,StandAssessFlag) values('"
                   + cIndexCalNo + "','" + cAgentCode + "','"
                   + tLAAgentDB.getAgentGroup() + "','"
                   + tLAAgentDB.getManageCom() + "','"
                   + tLAAgentDB.getBranchType() + "','" + tModifyFlag + "','"
                   + AgentGrade.substring(0, 1) + "','" + AgentGrade + "','"
                   + cGrade.substring(0, 1) + "',trim('" + cIndexValue + "'),'"
                   + Operator + "','0','" + currentDate + "','" + currentTime
                   + "','" + currentDate + "','" + currentTime + "','"
                   + tCalIndex.tAgCalBase.getBranchAttr() + "','1')";

            System.out.println("插入记录的SQL: " + tSQL);
            tReturn = tExeSQL.execUpdateSQL(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalIndex";
                tError.functionName = "insertAssess";
                tError.errorMessage = "执行SQL语句：判断记录是否存在失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            return tReturn;
        }
        else if ((TableName.equals("laassess") && !cIndexValue.equals("0"))
                 || !TableName.equals("laassess"))
        {
            if (cGrade.compareTo(cIndexValue) > 0) //降级
            {
                tModifyFlag = "01";
            }
            else if (cGrade.compareTo(cIndexValue) < 0) //升级
            {
                tModifyFlag = "03";
            }
            tSQL = "update " + TableName + " set AgentSeries = '"
                   + cIndexValue.substring(0, 1) + "'," + IColName + "= trim('"
                   + cIndexValue + "'),modifyDate = '" + currentDate
                   + "',modifyTime = '" + currentTime + "'," +
                   " modifyFlag = '"
                   + tModifyFlag + "'" + " where IndexCalNo = '" + cIndexCalNo
                   + "' and AgentCode = '" + cAgentCode + "'";
            System.out.println("更新记录的SQL: " + tSQL);
            tReturn = tExeSQL.execUpdateSQL(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalAssess";
                tError.functionName = "insertAssess";
                tError.errorMessage = "执行SQL语句：向指标表中插入记录失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            return tReturn;
        }
        return true;
    }

    /**
     *判断指标在表中是否已存在
     * 参数：代理人编码，考核代码，代理人职级
     */
    private boolean queryIndex(String cAgentCode, String cWageCode,
                               String cAgentGrade)
    {
        String tTable = "";
        String tCol = "";
        String tSQL = "";
        String tReturn = "";
        System.out.println("----取得考核代码对应的指标信息");
        LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
        tLAIndexVsAssessDB.setAgentGrade(cAgentGrade);
        tLAIndexVsAssessDB.setAssessCode(cWageCode);
        if (!tLAIndexVsAssessDB.getInfo())
        {
            //@错误处理
            this.mErrors.copyAllErrors(tLAIndexVsAssessDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "indexExist";
            tError.errorMessage = "查询考核指标对应的表名字段名出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tCalIndex.tAgCalBase.setAssessType(tLAIndexVsAssessDB.getAssessType()); //考核类型
        //银代内勤计算个人总分
        if (cAgentGrade.substring(0, 1).equals("E"))
        {
            mLAAssessIndexDB = new LAAssessIndexDB();
            mLAAssessIndexDB.setIndexCode(tLAIndexVsAssessDB.getIndexCode()); //算总分的sql代码
            if (!mLAAssessIndexDB.getInfo())
            {
                //@错误处理
                this.mErrors.copyAllErrors(mLAAssessIndexDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalAssess";
                tError.functionName = "indexExist";
                tError.errorMessage = "查询指标信息出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("   ----LAAssessIndex里的算法编码："
                               + mLAAssessIndexDB.getCalCode());
            System.out.println("   ----LAAssessIndex里的指标计算属性："
                               + mLAAssessIndexDB.getCalPrpty());
            System.out.println("   ----LAAssessIndex里的指标类型："
                               + mLAAssessIndexDB.getIndexType());
        }
        return true;
    }

    private void addAgCalBase(Calculator cCal)
    {
        cCal.addBasicFactor("AgentCode", tCalIndex.tAgCalBase.getAgentCode());
        cCal.addBasicFactor("WageNo", tCalIndex.tAgCalBase.getWageNo());
        cCal.addBasicFactor("IndexCalNo", tCalIndex.tAgCalBase.getWageNo());
        cCal.addBasicFactor("WageCode", tCalIndex.tAgCalBase.getWageCode());
        cCal.addBasicFactor("AgentGrade", tCalIndex.tAgCalBase.getAgentGrade());
        cCal.addBasicFactor("AgentGroup", tCalIndex.tAgCalBase.getAgentGroup());
        cCal.addBasicFactor("MonthBegin", tCalIndex.tAgCalBase.getMonthBegin());
        cCal.addBasicFactor("MonthEnd", tCalIndex.tAgCalBase.getMonthEnd());
        cCal.addBasicFactor("QuauterBegin",
                            tCalIndex.tAgCalBase.getQuauterBegin());
        cCal.addBasicFactor("QuauterEnd", tCalIndex.tAgCalBase.getQuauterEnd());
        cCal.addBasicFactor("HalfYearBegin",
                            tCalIndex.tAgCalBase.getHalfYearBegin());
        cCal.addBasicFactor("HalfYearEnd", tCalIndex.tAgCalBase.getHalfYearEnd());
        cCal.addBasicFactor("YearBegin", tCalIndex.tAgCalBase.getYearBegin());
        cCal.addBasicFactor("YearEnd", tCalIndex.tAgCalBase.getYearEnd());
        cCal.addBasicFactor("MonthMark", tCalIndex.tAgCalBase.getMonthMark());
        cCal.addBasicFactor("QuauterMark", tCalIndex.tAgCalBase.getQuauterMark());
        cCal.addBasicFactor("HalfYearMark",
                            tCalIndex.tAgCalBase.getHalfYearMark());
        cCal.addBasicFactor("YearMark", tCalIndex.tAgCalBase.getYearMark());
        cCal.addBasicFactor("AreaType", mAreaType);
        cCal.addBasicFactor("TempBegin", tCalIndex.tAgCalBase.getTempBegin());
        cCal.addBasicFactor("TempEnd", tCalIndex.tAgCalBase.getTempEnd());
        cCal.addBasicFactor("Rate",
                            String.valueOf(tCalIndex.tAgCalBase.getRate()));
        cCal.addBasicFactor("A1State",
                            String.valueOf(tCalIndex.tAgCalBase.getA1State()));
        cCal.addBasicFactor("AssessType", tCalIndex.tAgCalBase.getAssessType());
        cCal.addBasicFactor("LimitPeriod", tCalIndex.tAgCalBase.getLimitPeriod());
        cCal.addBasicFactor("DestAgentGrade",
                            tCalIndex.tAgCalBase.getDestAgentGrade());
        cCal.addBasicFactor("BranchAttr", tCalIndex.tAgCalBase.getBranchAttr());
        cCal.addBasicFactor("ChannelType", tCalIndex.tAgCalBase.getChannelType());
        System.out.println("考核开始前奏：" + tCalIndex.tAgCalBase.getAgentGrade());
    }

    private boolean WageCal()
    {
        String tReturn = "";

        //    int count=0;       //记录计算的个数,对应于LaIndexVSAssess表的记录数。目的在于提取考核Sql。
        //CalIndex tCalIndex = new CalIndex();

        /* 设置基本参数 */

        //判断该业务员是否本月考核
        if (!setBaseValue(this.tCalIndex.tAgCalBase))
        {
            return false;
        }

        if (tCalIndex.tAgCalBase.AssessMark == 0)
        {
            System.out.println("！该职级代理人任职未满一个考核期或未到自然考核期！");
            return true;
        }

        //11.设置‘渠道类型‘基数
        LAAgentBL tLAAgentBL = new LAAgentBL();
        tCalIndex.tAgCalBase.setChannelType(tLAAgentBL.findChannel(AgentCode));
        System.out.println("ChannelType:"
                           + tCalIndex.tAgCalBase.getChannelType());

        //查询考核代码对应的指标信息是否存在于结果表中。
        if (AgentGrade.substring(0, 1).equals("B"))
        {
            if (!queryIndex(AgentCode, AgentGrade + "001", AgentGrade))
            {
                return false;
            }
        }
        else if (AgentGrade.substring(0, 1).equals("C"))
        {
            if (!queryIndex(AgentCode, "CP0101", AgentGrade))
            {
                return false;
            }
        }
        System.out.println("－－－－－开始按Sql计算该代理人的考评信息－－－－－");
        tCalIndex.setAssessIndex(mLAAssessIndexDB);
        tCalIndex.tAgCalBase.setAgentGrade(this.AgentGrade);
        tCalIndex.tAgCalBase.setAgentCode(this.AgentCode);
        tCalIndex.setAgentCode(AgentCode);
        tCalIndex.setIndexCalNo(IndexCalNo);
        tCalIndex.setOperator(Operator);
        tCalIndex.setIndexCalPrpty("01");

        System.out.println("IndexCalNo will cal tCalIndex.Calculate: "
                           + this.IndexCalNo);
        System.out.println("---------tCalIndex.Calculate()---------------");

        //15.考核指标计算，计算个人总分
        //xjh Modify 2005/03/10 ，mResultIndexSet已经变为私有
        //tCalIndex.mResultIndexSet.clear(); //首先清除已经计算过的结果集
        tCalIndex.clearIndexSet();

        tReturn = tCalIndex.Calculate();

        if (tCalIndex.mErrors.needDealError())
        {
            System.out.println("Error:" + this.mErrors.getFirstError());
            // @@错误处理
            this.mErrors.copyAllErrors(tCalIndex.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "WageCal";
            tError.errorMessage = "考核指标计算出错。";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("考核代码对应指标计算结果：" + tReturn);

        //-------计算考核代码对应的sql计算编码
        //      System.out.println("Calcode"+count);
        //      tSqlCode=(String)tExamineIndex.get("Calcode"+count);//该考评对应的考核代码。
        //      System.out.println(tWageCode+"--计算该职级考核的SQL编码："+tSqlCode);
        //有考核代码并且大于职级B10的职级的半年考评一年考核的
        if (((!(this.AgentGrade.compareTo("B10") > 0)
              || (this.AgentGrade.substring(0, 1).equals("C")
                  && (PubFun.calInterval(tCalIndex.tAgCalBase.getTempBegin(),
                                         tCalIndex.tAgCalBase.getTempEnd(), "M") <
                      7)))
             || ((this.AgentGrade.compareTo("B11") >= 0)
                 && tCalIndex.tAgCalBase.getYearMark().equals("1"))))
        {
            //计算考核sql
            System.out.println("－－－－－－－－开始用Sql找出建议职级－－－－－－－－－");
            Calculator tCal = new Calculator();

            //设置计算目标职级sql计算编码
            LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
            tLAIndexVsAssessDB.setAgentGrade(AgentGrade);
            if (AgentGrade.substring(0, 1).equals("C"))
            {
                tLAIndexVsAssessDB.setAssessCode("CP0101");
            }
            else
            {
                tLAIndexVsAssessDB.setAssessCode(AgentGrade + "001");
            }
            tLAIndexVsAssessDB.getInfo();
            tCal.setCalCode(tLAIndexVsAssessDB.getCalCode());
            //增加基本要素
            addAgCalBase(tCal);

            //13.计算目标职级
            tReturn = tCal.calculate();
            if (tCal.mErrors.needDealError())
            {
                System.out.println("Error:" + this.mErrors.getFirstError());
                // @@错误处理
                this.mErrors.copyAllErrors(tCal.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalAssess";
                tError.functionName = "WageCal";
                tError.errorMessage = "考核指标sql计算出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            if ((tReturn == null) || (tReturn.trim().equals("")))
            {
                tReturn = "0";
            }
            System.out.println(
                    "--------------------insert sql-------------------------");
            if (AgentGrade.substring(0, 1).equals("C"))
            {
                if (!insertAssess(IndexCalNo, AgentCode, AgentGrade, "CP0101",
                                  tReturn))
                {
                    System.out.println("把插入考核指标表出错！！");
                    return false;
                }
            }
            else if (!insertAssess(IndexCalNo, AgentCode, AgentGrade,
                                   AgentGrade + "001", tReturn))
            {
                System.out.println("把插入考核指标表出错！！");
                return false;
            }
            System.out.println("考核代码对应sql编码计算结果：" + tReturn);
        }
        System.out.println();
        System.out.println(
                "---------------------------------------------------------");
        System.out.println();
        //    }
        return true;
    }

    /**
     *循环计算一职级对应的所有考核代码
     * @return boolean 计算出错返回false
     **/
    private boolean WageCalGrp()
    {
        String tReturn = "";
        /* 设置基本参数 */
        if (!setBaseValue(this.tCalIndex.tAgCalBase))
        {
            return false;
        }

        if (tCalIndex.tAgCalBase.AssessMark == 0)
        {
            System.out.println("！该职级代理人任职未满一个考核期或未到自然考核期！");
            return true;
        }

        //11.设置‘渠道类型‘基数
        //查询考核代码对应的指标信息是否存在于结果表中。
        if (!queryIndex(AgentCode, AgentGrade + "001", AgentGrade))
        {
            return false;
        }
        System.out.println("－－－－－开始按Sql计算该代理人的考评信息－－－－－");
        //法人考核没有个人总分，直接取得目标职级
        if (AgentGrade.substring(0, 1).equals("E"))
        {
            tCalIndex.setAssessIndex(mLAAssessIndexDB);
        }
        tCalIndex.tAgCalBase.setAgentGrade(this.AgentGrade);
        tCalIndex.tAgCalBase.setAgentCode(this.AgentCode);
        tCalIndex.setAgentCode(AgentCode);
        tCalIndex.setIndexCalNo(IndexCalNo);
        tCalIndex.setOperator(Operator);
        tCalIndex.setIndexCalPrpty("01");

        System.out.println("IndexCalNo will cal tCalIndex.Calculate: "
                           + this.IndexCalNo);
        System.out.println("---------tCalIndex.Calculate()---------------");

        //15.考核指标计算，计算个人总分
        tReturn = tCalIndex.Calculate();

        if (tCalIndex.mErrors.needDealError())
        {
            System.out.println("Error:" + this.mErrors.getFirstError());
            // @@错误处理
            this.mErrors.copyAllErrors(tCalIndex.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "WageCal";
            tError.errorMessage = "考核指标计算出错。";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("考核代码对应指标计算结果：" + tReturn);
        //-------计算考核代码对应的sql计算编码
        //计算考核sql
        System.out.println("－－－－－－－－开始用Sql找出建议职级－－－－－－－－－");
        Calculator tCal = new Calculator();

        //设置计算目标职级sql计算编码
        LAIndexVsAssessDB tLAIndexVsAssessDB = new LAIndexVsAssessDB();
        tLAIndexVsAssessDB.setAgentGrade(AgentGrade);
        tLAIndexVsAssessDB.setAssessCode(AgentGrade + "001");
        tLAIndexVsAssessDB.getInfo();
        tCal.setCalCode(tLAIndexVsAssessDB.getCalCode());
        //增加基本要素
        addAgCalBase(tCal);

        //13.计算目标职级
        tReturn = tCal.calculate();
        if (tCal.mErrors.needDealError())
        {
            System.out.println("Error:" + this.mErrors.getFirstError());
            // @@错误处理
            this.mErrors.copyAllErrors(tCal.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "WageCal";
            tError.errorMessage = "考核指标sql计算出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        if ((tReturn == null) || (tReturn.trim().equals("")))
        {
            tReturn = "0";
        }
        System.out.println(
                "--------------------insert sql-------------------------");
        if (!insertAssess(IndexCalNo, AgentCode, AgentGrade,
                          AgentGrade + "001", tReturn))
        {
            System.out.println("把插入考核指标表出错！！");
            return false;
        }
        System.out.println("考核代码对应sql编码计算结果：" + tReturn);

        System.out.println();
        System.out.println(
                "---------------------------------------------------------");
        System.out.println();

        return true;
    }

    //取得代理人当前职级的起聘日期，增加15日前后的区分
    private String getPostBeginDate(String cAgentCode)
    {
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(cAgentCode);
        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "CalAssess";
            tCError.functionName = "getLastAssessDate()";
            tCError.errorMessage = "取代理人当前职级的起聘日期出错！";
            this.mErrors.addOneError(tCError);
            return "";
        }
        String tStartDate = tLATreeDB.getStartDate().trim();
        if ((tStartDate == null) || tStartDate.equals(""))
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "CalAssess";
            tCError.functionName = "getLastAssessDate()";
            tCError.errorMessage = "取代理人当前职级的起聘日期为空！";
            this.mErrors.addOneError(tCError);
            return "";
        }
        String[] StartDate = PubFun.calFLDate(tStartDate);
        String day = tStartDate.substring(8);
        tStartDate = StartDate[0];
        if (day.compareTo("15") > 0)
        {
            FDate fDate = new FDate();
            Date date1 = fDate.getDate(tStartDate);
            tStartDate = fDate.getString(PubFun.calDate(date1, 1, "M", null));
        }
        System.out.println("择算的起聘日期" + tStartDate);
        return tStartDate;
    }

    //到数据库中得到起止日期
    private boolean getBeginEnd()
    {
        String tPostBegin = ""; //任职起期
        tPostBegin = getPostBeginDate(AgentCode);
        if (tPostBegin.equals(""))
        {
            return false;
        }

        //5.从数据库中查出时间区间
        LAStatSegmentSet tLAStatSegmentSet = new LAStatSegmentSet();
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
        System.out.println("getBeginEnd 设置时间区间：" + this.IndexCalNo);
        tLAStatSegmentDB.setYearMonth(this.IndexCalNo);
        tLAStatSegmentSet = tLAStatSegmentDB.query();
        if (tLAStatSegmentDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "CalAssess";
            tCError.functionName = "setBeginEnd()";
            tCError.errorMessage = "查询时间区间出错！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        int tCount = tLAStatSegmentSet.size();
        System.out.println("tCount:" + tCount + ",考核年月：" + this.IndexCalNo);
        if (tCount < 1)
        {
            CError tCError = new CError();
            tCError.moduleName = "CalAssess";
            tCError.functionName = "setBeginEnd()";
            tCError.errorMessage = "所输年月有问题，无法查到时间区间";
            this.mErrors.addOneError(tCError);
            return false;
        }

        //首先重新建立基础信息表
        LAStatSegmentSchema tLAStatSegmentSchema = new LAStatSegmentSchema(); //月，季，半年，年。输入12将返回四个记录
        String tStatType = "";
        for (int i = 1; i <= tCount; i++)
        {
            tLAStatSegmentSchema = tLAStatSegmentSet.get(i);
            tStatType = tLAStatSegmentSchema.getStatType().trim();
            System.out.println("考核区间类型： " + tStatType);

            //6.处理B01和C01的情况,存在3个月考核情况
            if ((this.AgentGrade.equals("B01")
                 || (this.AgentGrade.equals("C01")))
                && tStatType.equals("1"))
            {
                FDate fDate = new FDate();
                Date date3 = new Date();
                Date date6 = new Date();
                Date date = new Date();
                String indate = IndexCalNo.substring(0, 4) + "-"
                                + IndexCalNo.substring(4, IndexCalNo.length()) +
                                "-01";
                date = fDate.getDate(indate);
                System.out.println("得到的现在日期：" + fDate.getString(date));
                String[] lastday = PubFun.calFLDate(indate);
                date3 = PubFun.calDate(date, -2, "M", date); //一级代理人的考评日期由现在的日期向前推三个月。
                date6 = PubFun.calDate(date, -5, "M", date); //一级代理人的考评日期由现在的日期向前推六个月。
                System.out.println("前推3个月的日期： " + fDate.getString(date3));
                System.out.println("前推6个月的日期： " + fDate.getString(date6));
                System.out.println("任职日期： " + tPostBegin);
                System.out.println(lastday[0] + "<--本月-->" + lastday[1]);
                if (tPostBegin.compareTo(fDate.getString(date3)) == 0)
                {
                    tCalIndex.tAgCalBase.AssessMark = 1; //存在季考核标志
                    tCalIndex.tAgCalBase.setQuauterMark("1"); //存在季考核标志
                    //置考核日期
                    tCalIndex.tAgCalBase.setQuauterBegin(tPostBegin); //入司日期
                    tCalIndex.tAgCalBase.setQuauterEnd(lastday[1]); //考核月末日期
                    tCalIndex.tAgCalBase.setTempBegin(tPostBegin);
                    tCalIndex.tAgCalBase.setTempEnd(lastday[1]);
                }
                else if (tPostBegin.compareTo(fDate.getString(date6)) == 0)
                {
                    tCalIndex.tAgCalBase.AssessMark = 1; //存在半年考核标志
                    tCalIndex.tAgCalBase.setHalfYearMark("1"); //存在半年考核标志
                    //置考核日期
                    tCalIndex.tAgCalBase.setHalfYearBegin(tPostBegin); //入司日期起期
                    tCalIndex.tAgCalBase.setHalfYearEnd(lastday[1]); //考核月末日期
                    tCalIndex.tAgCalBase.setTempBegin(tPostBegin);
                    tCalIndex.tAgCalBase.setTempEnd(lastday[1]);
                }
                else
                {
                    tCalIndex.tAgCalBase.AssessMark = 0;
                }
                break;
            }

            //7.处理B02－B10职级
            if (((this.AgentGrade.compareTo("B01") > 0)
                 && (this.AgentGrade.compareTo("B10") <= 0))
                && tStatType.equals("1"))
            {
                String asql = "select Count(*) from laassess where agentcode='"
                              + AgentCode + "'"; //考核次数
                ExeSQL aExeSQL = new ExeSQL();
                if (aExeSQL.getOneValue(asql).equals("0")) //试用期考核
                {
                    FDate fDate = new FDate();
                    Date date = new Date();
                    String indate = IndexCalNo.substring(0, 4) + "-"
                                    + IndexCalNo.substring(4, IndexCalNo.length()) +
                                    "-01";
                    date = fDate.getDate(indate);
                    System.out.println("得到的现在日期：" + fDate.getString(date));
                    String[] lastday = PubFun.calFLDate(indate);
                    date = PubFun.calDate(date, -2, "M", date); //一级代理人的考评日期由现在的日期向前推三个月。
                    System.out.println("前推3个月的日期： " + fDate.getString(date));
                    System.out.println("任职日期： " + tPostBegin);
                    System.out.println(lastday[0] + "<--本月-->" + lastday[1]);
                    if (tPostBegin.compareTo(fDate.getString(date)) <= 0)
                    {
                        tCalIndex.tAgCalBase.AssessMark = 1; //存在季考核标志
                        tCalIndex.tAgCalBase.setQuauterMark("1"); //存在季考核标志
                        //置考核日期
                        tCalIndex.tAgCalBase.setQuauterBegin(tPostBegin); //入司日期
                        tCalIndex.tAgCalBase.setQuauterEnd(lastday[1]); //考核月末日期
                        tCalIndex.tAgCalBase.setTempBegin(tPostBegin);
                        tCalIndex.tAgCalBase.setTempEnd(lastday[1]);
                    }
                    else
                    {
                        tCalIndex.tAgCalBase.AssessMark = 0;
                    }
                }
            }

            //8.正常考核六个月,包括法人部分D，E级别
            if ((!this.AgentGrade.equals("B01")
                 && !this.AgentGrade.equals("C01")) && tStatType.equals("3")
                && (tCalIndex.tAgCalBase.AssessMark != 1))
            {
                tCalIndex.tAgCalBase.AssessMark = 1;
                tCalIndex.tAgCalBase.setHalfYearMark("1"); //存在半年考核标志
                System.out.println("已经置半年考评标志！");
                if (tLAStatSegmentSchema.getStartDate().compareTo(tPostBegin) <
                    0)
                {
                    tCalIndex.tAgCalBase.setHalfYearBegin(tPostBegin);
                    tCalIndex.tAgCalBase.setTempBegin(tPostBegin);
                }
                else
                {
                    tCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema
                            .getStartDate());
                    tCalIndex.tAgCalBase.setTempBegin(tLAStatSegmentSchema
                            .getStartDate());
                }
                tCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema
                        .getEndDate());
                tCalIndex.tAgCalBase.setTempEnd(tLAStatSegmentSchema.getEndDate());
            }

            //年考核只有B11和B12两个职级,年考核增加E10－E12法人内勤
            if ((tStatType.equals("4")
                 && ((this.AgentGrade.compareTo("B11") >= 0)
                     && (this.AgentGrade.compareTo("C01") < 0)))
                || (((this.AgentGrade.compareTo("E10") >= 0)
                     && (this.AgentGrade.compareTo("E12") <= 0))
                    && !tCalIndex.tAgCalBase.getYearMark().equals("1")))
            {
                tCalIndex.tAgCalBase.setYearMark("1"); //存在年考核标志
            }
        }
        return true;
    }

    //校验入参,校验managecom，Year，Month
    private boolean checkInputData()
    {
        if ((this.ManageCom == null) || this.ManageCom.equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalExcamine";
            tError.functionName = "checkInputData";
            tError.errorMessage = "管理部门没有传入入口函数！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (Integer.parseInt(this.Year) <= 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "checkCalculate";
            tError.errorMessage = "年份值没有初始化。";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (Integer.parseInt(this.Month) <= 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "checkCalculate";
            tError.errorMessage = "月份值没有初始化。";
            this.mErrors.addOneError(tError);
            return false;
        }

        //算过佣金之后才能再进行考核
        String aSQL = "select count(*) from lawage where indexcalno='" + Year
                      + Month + "'" + " and branchtype='" + BranchType
                      + "' and managecom like '" + ManageCom + "%'";
        ExeSQL tExeSQL = new ExeSQL();
        if (tExeSQL.getOneValue(aSQL).equals("0"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "checkCalculate";
            tError.errorMessage = "本考核月还未算佣金，请先计算佣金！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //判断是否考核过
        String bSQL = "select count(*) from laassess where indexcalno='" + Year
                      + Month + "'" + " and branchtype='" + BranchType
                      + "' and managecom like '" + ManageCom + "%'";
        ExeSQL nExeSQL = new ExeSQL();
        if (!nExeSQL.getOneValue(bSQL).equals("0"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalAssess";
            tError.functionName = "checkCalculate";
            tError.errorMessage = "本考核月已经做过考核！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        System.out.println("---------------开始考核指标计算！-----------------");
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86110000";
        tGlobalInput.Operator = "zy";
        tGlobalInput.ComCode = "86";
        String ManageCom = "86110000";
        String WageYear = "2003";
        String WageMonth = "12";
        String BranchType = "2";
        VData tVData = new VData();
        tVData.addElement(ManageCom);
        tVData.addElement(WageYear);
        tVData.addElement(WageMonth);
        tVData.addElement(BranchType);
        tVData.addElement(tGlobalInput);
        CalExamine tCalAssess = new CalExamine();
        tCalAssess.submitData(tVData, "Grp");
        if (tCalAssess.mErrors.needDealError())
        {
            System.out.println("程序异常结束原因：" + tCalAssess.mErrors.getFirstError());
        }
        System.out.println("--------------结束考核指标计算！---------------");
    }
}
