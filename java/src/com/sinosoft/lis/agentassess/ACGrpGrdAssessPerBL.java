package com.sinosoft.lis.agentassess;

import java.util.Date;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LAIndexVsAssessAccessorySet;
import com.sinosoft.lis.vschema.LAStatSegmentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentcalculate.CalIndex;
import com.sinosoft.lis.agentcalculate.AgCalBase;

/**
 * <p>Title: 中英团险考核程序-人员处理</p>
 * <p>Description: 中英团险考核程序-人员处理</p>
 * <p>Copyright: Copyright (c) sinosoft 2004</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public class ACGrpGrdAssessPerBL extends GradeAssessPublic
{
    //全局变量
//    protected CErrors mErrors = new CErrors(); //错误处理类
//    protected GlobalInput mGlobalInput = new GlobalInput(); //保存传入参数
//    protected String mOperate; //保存传入的操作数
//    protected String mIndexCalNo; //考核指标计算编码
//    protected String mOperator; //操作人员
//    protected String mBranchType; //展业类型
//    protected String mAreaType; //地区类型
    private VData mInputData; //往后台传入数据
    private String mAgentCode; //代理人编码
    private String mOldAgentGrade; //代理人原职级
    private CalIndex mCalIndex = new CalIndex(); //指标计算类
    private LATreeSchema mLATreeSchema = new LATreeSchema(); //代理人行政信息
    private LAIndexVsAssessAccessorySet mLAIndexVsAssessAccessorySet = new
            LAIndexVsAssessAccessorySet(); //现有职级对应可能职级集合
    private LAAssessIndexSchema mLAAssessIndexSchema; //计算指标
    private String mNewAgentGrade = new String(); //新职级
    private LAAssessAccessorySchema mLAAssessAccessorySchema = new
            LAAssessAccessorySchema(); //考核结果
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema(); //代理人信息
    /** 取得系统日期、时间 */
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    ACGrpGrdAssessPerBLS mACGrpGrdAssessPerBLS; //数据库插入

    public ACGrpGrdAssessPerBL()
    {
    }

    public static void main(String[] args)
    {
    }

    //前后台数据交互
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("--------ACGrpGrdAssessPerBL.submitData--------");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            if (mErrors.needDealError())
            {
                System.out.println("考核异常结束原因：" + mErrors.getFirstError());
            }
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        //插入考核信息
        System.out.println("--------ACGrpGrdAssessPerBLS.Submit --------");
        mACGrpGrdAssessPerBLS = new ACGrpGrdAssessPerBLS();
        if (!mACGrpGrdAssessPerBLS.submitData(mInputData, mOperate))
        {
            return false;
        }
        System.out.println("--------ACGrpGrdAssessPerBL over--------");
        return true;
    }

    //获得输入数据
    public boolean getInputData(VData cInputData)
    {
        System.out.println("--------ACGrpGrdAssessPerBL.getInputData--------");
        //信息收集
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        //获得指标计算编码(考核年份)
        mIndexCalNo = (String) cInputData.getObject(1);
        //获得代理人编码
        mAgentCode = (String) cInputData.getObject(2);
        //获得代理人职级
        mOldAgentGrade = (String) cInputData.getObject(3);
        //获得展业类型
        mBranchType = (String) cInputData.getObject(4);
        //获地区类型
        mAreaType = (String) cInputData.getObject(5);
        //获得操作人员
        mOperator = mGlobalInput.Operator;
        if (mGlobalInput == null || mIndexCalNo == null || mAgentCode == null ||
            mOldAgentGrade == null || mBranchType == null || mAreaType == null)
        {
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessPerBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //获得行政信息
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(mAgentCode);
        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "ACGrpGrdAssessPerBL";
            tCError.functionName = "getInputData()";
            tCError.errorMessage = "取代理人的行政信息出错！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        mLATreeSchema = tLATreeDB.getSchema();
        //获得代理人信息
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "ACGrpGrdAssessPerBL";
            tCError.functionName = "getInputData()";
            tCError.errorMessage = "获得代理人信息出错！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        mLAAgentSchema = tLAAgentDB.getSchema();
        return true;
    }

    // 准备往后台的数据
    public boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAAssessAccessorySchema); //做插入操作
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessPerBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    //对数据进行业务处理
    public boolean dealData()
    {
        System.out.println("--------ACGrpGrdAssessPerBL.dealData--------");
        if (!AssessCalculate())
        {
            System.out.println("Error:" + this.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    /**
     * getBeginEnd
     * 到数据库中得到起止日期
     * @return boolean
     */
    private boolean getBeginEnd()
    {
        String tPostBegin = ""; //任职起期
        tPostBegin = getPostBeginDate(mAgentCode);
        if (tPostBegin.equals(""))
        {
            return false;
        }
        //从数据库中查出时间区间
        LAStatSegmentSet tLAStatSegmentSet = new LAStatSegmentSet();
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
        System.out.println("getBeginEnd 设置时间区间：" + this.mIndexCalNo);
        tLAStatSegmentDB.setYearMonth(this.mIndexCalNo);
        tLAStatSegmentSet = tLAStatSegmentDB.query();
        if (tLAStatSegmentDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "ACGrpGrdAssessPerBL";
            tCError.functionName = "setBeginEnd";
            tCError.errorMessage = "查询时间区间出错！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        int tCount = tLAStatSegmentSet.size();
        System.out.println("考核年月：" + this.mIndexCalNo + ",统计区间个数:" + tCount);
        if (tCount < 1)
        {
            CError tCError = new CError();
            tCError.moduleName = "ACGrpGrdAssessPerBL";
            tCError.functionName = "setBeginEnd";
            tCError.errorMessage = "所输年月有问题，无法查到时间区间";
            this.mErrors.addOneError(tCError);
            return false;
        }
        //首先重新建立基础信息表
        LAStatSegmentSchema tLAStatSegmentSchema = new LAStatSegmentSchema(); //月，季，半年，年。输入12将返回四个记录
        String tStatType = "";
        for (int i = 1; i <= tCount; i++)
        {
            tLAStatSegmentSchema = tLAStatSegmentSet.get(i);
            tStatType = tLAStatSegmentSchema.getStatType().trim();
            System.out.println("考核区间类型： " + tStatType);
            //中英团险代理人考核期为半年.
            //相对以前,取消职级判断因为只有中英考核,取消AssessMark因为现在程序并没有重新赋值
            if (tStatType.equals("3"))
            {
                mCalIndex.tAgCalBase.AssessMark = 1;
                mCalIndex.tAgCalBase.setHalfYearMark("1"); //存在半年考核标志
                System.out.println("已经置半年考评标志！");
                if (tLAStatSegmentSchema.getStartDate().compareTo(tPostBegin) <
                    0)
                {
                    mCalIndex.tAgCalBase.setHalfYearBegin(tPostBegin);
                    mCalIndex.tAgCalBase.setTempBegin(tPostBegin);
                }
                else
                {
                    mCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema
                            .getStartDate());
                    mCalIndex.tAgCalBase.setTempBegin(tLAStatSegmentSchema
                            .getStartDate());
                }
                mCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema
                        .getEndDate());
                mCalIndex.tAgCalBase.setTempEnd(tLAStatSegmentSchema.getEndDate());
            }
        }
        return true;
    }

    /**
     * getPostBeginDate
     * 取得代理人当前职级的起聘日期，增加15日前后的区分,15后为下个月1号,否则为本月1号
     * @param cAgentCode String
     * @return String
     */
    private String getPostBeginDate(String cAgentCode)
    {
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(cAgentCode);
        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "ACGrpGrdAssessPerBL";
            tCError.functionName = "getPostBeginDate";
            tCError.errorMessage = "取代理人当前职级的起聘日期出错！";
            this.mErrors.addOneError(tCError);
            return "";
        }
        String tStartDate = tLATreeDB.getStartDate().trim();
        if ((tStartDate == null) || tStartDate.equals(""))
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "ACGrpGrdAssessPerBL";
            tCError.functionName = "getPostBeginDate";
            tCError.errorMessage = "取代理人当前职级的起聘日期为空！";
            this.mErrors.addOneError(tCError);
            return "";
        }
        String[] StartDate = PubFun.calFLDate(tStartDate);
        String day = tStartDate.substring(8);
        tStartDate = StartDate[0];
        if (day.compareTo("15") > 0)
        {
            FDate fDate = new FDate();
            Date date1 = fDate.getDate(tStartDate);
            tStartDate = fDate.getString(PubFun.calDate(date1, 1, "M", null));
        }
        System.out.println("择算的起聘日期" + tStartDate);
        return tStartDate;
    }

    //进行考核计算
    //完成计算基本指标,往LAIndexInfo写数据,调用CalIndex类
    //计算基本指标时从LAIndexVsAssessAccessory中现有职级中找出所有对应职级（从高到低）,计算指标
    //符合那个职级就是那个职级
    //计算完成后，根据返回条件进行职级确定,往LAAssessAccessory表里写数据
    public boolean AssessCalculate()
    {
        //得到起止日期，不同职级不同.和考核标志
        if (!getBeginEnd())
        {
            return false;
        }
        if (mCalIndex.tAgCalBase.AssessMark == 0)
        {
            System.out.println("！该职级代理人任职未满一个考核期或未到自然考核期！");
            return true;
        }
        /* 设置基本参数 */
        //需求随描述变化
        if (!setBaseValue(this.mCalIndex.tAgCalBase))
        {
            return false;
        }
        //从LAIndexVsAssessAccessory中现有职级中找出所有对应职级（从高到低）
        if (!queryIndexVsAssess(mOldAgentGrade))
        {
            return false;
        }
        for (int i = 1; i <= mLAIndexVsAssessAccessorySet.size(); i++)
        {
            LAIndexVsAssessAccessorySchema tLAIndexVsAssessAccessorySchema = new
                    LAIndexVsAssessAccessorySchema();
            tLAIndexVsAssessAccessorySchema = mLAIndexVsAssessAccessorySet.get(
                    i);
            mCalIndex.tAgCalBase.setAssessType(tLAIndexVsAssessAccessorySchema.
                                               getAssessType()); //考核类型
            //对应指标编码
            String tCalIndex = tLAIndexVsAssessAccessorySchema.getIndexCode();
            //获得对应LAAssessIndexSchema
            mLAAssessIndexSchema = new LAAssessIndexSchema();
            if (queryIndex(tCalIndex))
            {
                return false;
            }
            //设置CalIndex.AssessIndex
            mCalIndex.setAssessIndex(mLAAssessIndexSchema);
            System.out.println("IndexCalNo will cal mCalIndex.Calculate: "
                               + mIndexCalNo);
            System.out.println("---------mCalIndex.Calculate()---------------");
            //考核指标计算
            mNewAgentGrade = mCalIndex.Calculate();
            if (mCalIndex.mErrors.needDealError())
            {
                System.out.println("Error:" + this.mErrors.getFirstError());
                // @@错误处理
                this.mErrors.copyAllErrors(mCalIndex.mErrors);
                CError tError = new CError();
                tError.moduleName = "ACGrpGrdAssessPerBL";
                tError.functionName = "AssessCalculate";
                tError.errorMessage = "考核指标计算出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (!mNewAgentGrade.equals("") || !mNewAgentGrade.equals(null))
            {
                break;
            }
        }
        //？？？
        if (!mNewAgentGrade.equals("") || !mNewAgentGrade.equals(null))
        {
            System.out.println("Error:" + this.mErrors.getFirstError());
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessPerBL";
            tError.functionName = "AssessCalculate";
            tError.errorMessage = "没有计算出相应职级。";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备数据
        if (!prepareLAAssessAccessoryData())
        {
            return false;
        }
        return true;
    }

    /**
     * prepareLAAssessAccessoryData
     * 填充mLAAssessAccessorySchema
     * @return boolean
     */
    private boolean prepareLAAssessAccessoryData()
    {
        this.mLAAssessAccessorySchema.setIndexCalNo(this.mIndexCalNo);
        this.mLAAssessAccessorySchema.setAssessType("00");
        this.mLAAssessAccessorySchema.setAgentCode(this.mAgentCode);
        this.mLAAssessAccessorySchema.setBranchType("2");
        this.mLAAssessAccessorySchema.setBranchAttr(AgentPubFun.
                getAgentBranchAttr(this.mAgentCode));
        this.mLAAssessAccessorySchema.setAgentGroup(this.mLAAgentSchema.
                getAgentGroup());
        this.mLAAssessAccessorySchema.setManageCom(this.mLAAgentSchema.
                getManageCom());
        if (mNewAgentGrade.compareTo(mOldAgentGrade) > 0)
        {
            this.mLAAssessAccessorySchema.setModifyFlag("01");
        }
        else if (mNewAgentGrade.compareTo(mOldAgentGrade) == 0)
        {
            this.mLAAssessAccessorySchema.setModifyFlag("02");
        }
        else if (mNewAgentGrade.compareTo(mOldAgentGrade) < 0)
        {
            this.mLAAssessAccessorySchema.setModifyFlag("03");
        }
        this.mLAAssessAccessorySchema.setAgentSeries("");
        this.mLAAssessAccessorySchema.setAgentGrade(mOldAgentGrade);
        this.mLAAssessAccessorySchema.setCalAgentSeries("");
        this.mLAAssessAccessorySchema.setCalAgentGrade(mNewAgentGrade);
        this.mLAAssessAccessorySchema.setAgentSeries1("");
        this.mLAAssessAccessorySchema.setAgentGrade1(mNewAgentGrade);
        //this.mLAAssessAccessorySchema.setAgentGroupNew();
        //this.mLAAssessAccessorySchema.setConfirmer();
        //this.mLAAssessAccessorySchema.setConfirmDate();
        this.mLAAssessAccessorySchema.setState("0");
        this.mLAAssessAccessorySchema.setStandAssessFlag("1");
        //this.mLAAssessAccessorySchema.setFirstAssessFlag();
        this.mLAAssessAccessorySchema.setOperator(mOperator);
        this.mLAAssessAccessorySchema.setMakeDate(mCurrentDate);
        this.mLAAssessAccessorySchema.setMakeTime(mCurrentTime);
        this.mLAAssessAccessorySchema.setModifyDate(mCurrentDate);
        this.mLAAssessAccessorySchema.setModifyTime(mCurrentDate);
        return true;
    }

    /**
     * queryIndexVsAssess
     * 从LAIndexVsAssessAccessory中现有职级中找出所有对应职级（从高到低）
     * @param cAgentGrade String
     * @return boolean
     */
    private boolean queryIndexVsAssess(String cAgentGrade)
    {
        System.out.println("----从LAIndexVsAssessAccessory中现有职级中找出所有对应职级（从高到低）");
        LAIndexVsAssessAccessoryDB tLAIndexVsAssessAccessoryDB = new
                LAIndexVsAssessAccessoryDB();
        mLAIndexVsAssessAccessorySet = tLAIndexVsAssessAccessoryDB.query();
        String tSQL =
                " select * from LAIndexVsAssessAccessory where AssessGrade = '"
                + cAgentGrade +
                "' and AssessWay = '00' order by DestGrade desc";
        System.out.println("查询" + cAgentGrade + "所有对应职级：" + tSQL);
        mLAIndexVsAssessAccessorySet = tLAIndexVsAssessAccessoryDB.executeQuery(
                tSQL);
        if (tLAIndexVsAssessAccessoryDB.mErrors.needDealError())
        {
            //@错误处理
            this.mErrors.copyAllErrors(tLAIndexVsAssessAccessoryDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessPerBL";
            tError.functionName = "queryIndexVsAssess";
            tError.errorMessage = "从LAIndexVsAssessAccessory表现有职级中找出所有对应职级时出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("该职级下的对应职级个数：" + mLAIndexVsAssessAccessorySet.size());
        return true;
    }

    /**
     * queryIndex
     * 查询指标，获得mLAAssessIndexSchema
     * @param cIndexCode String
     * @return boolean
     */
    private boolean queryIndex(String cIndexCode)
    {
        System.out.println("----取得考核代码对应的指标信息");
        LAAssessIndexDB tLAAssessIndexDB = new
                                           LAAssessIndexDB();
        tLAAssessIndexDB.setIndexCode(cIndexCode);
        if (!tLAAssessIndexDB.getInfo())
        {
            //@错误处理
            this.mErrors.copyAllErrors(tLAAssessIndexDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessPerBL";
            tError.functionName = "queryIndex";
            tError.errorMessage = "查询考核指标LAAssessIndex出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLAAssessIndexSchema = tLAAssessIndexDB.getSchema();
        return true;
    }

    /**
     * setBaseValue
     * 设置基本参数
     * @param cAgCalBase AgCalBase
     * @return boolean
     */
    private boolean setBaseValue(AgCalBase cAgCalBase)
    {
        mCalIndex.setAgentCode(mAgentCode);
        mCalIndex.setIndexCalNo(mIndexCalNo);
        mCalIndex.setOperator(mOperator);
        mCalIndex.setIndexCalPrpty("01");
        //根据需要添加
        mCalIndex.tAgCalBase.setAgentGroup(mLATreeSchema.getAgentGroup());
        mCalIndex.tAgCalBase.setAgentCode(mAgentCode);
        mCalIndex.tAgCalBase.setWageNo(mIndexCalNo);
        mCalIndex.tAgCalBase.setAreaType(mAreaType);
        mCalIndex.tAgCalBase.setBranchAttr(AgentPubFun.getAgentBranchAttr(
                mAgentCode));
        mCalIndex.tAgCalBase.setAgentGrade(this.mOldAgentGrade);

        System.out.println("cAgCalBase.getTempBegin: "
                           + cAgCalBase.getTempBegin());
        System.out.println("cAgCalBase.getTempEnd: " + cAgCalBase.getTempEnd());
        return true;
    }
}
