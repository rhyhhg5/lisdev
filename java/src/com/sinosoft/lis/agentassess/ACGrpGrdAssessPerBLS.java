package com.sinosoft.lis.agentassess;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;
import com.sinosoft.utility.*;

/**
 * <p>Title: 中英团险考核程序-人员处理</p>
 * <p>Description: 中英团险考核程序-人员处理</p>
 * <p>Copyright: Copyright (c) sinosoft 2004</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public class ACGrpGrdAssessPerBLS
{
    //全局变量
    protected CErrors mErrors = new CErrors(); //错误处理类
    protected GlobalInput mGlobalInput = new GlobalInput(); //保存传入参数
    /** 数据操作字符串 */
    private String mOperate;
    /**存放考核结果*/
    private LAAssessAccessorySchema mLAAssessAccessorySchema = new
            LAAssessAccessorySchema();

    public ACGrpGrdAssessPerBLS()
    {
    }

    public static void main(String[] args)
    {
        ACGrpGrdAssessPerBLS acgrpgrdassessperbls = new ACGrpGrdAssessPerBLS();
    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        this.mOperate = cOperate;
        //1 - 获得输入数据
        if (!getInputData(cInputData))
        {
            return false;
        }

        System.out.println("Start ACGrpGrdAssessPerBLS Submit...");
        if (cOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLAAssess();
        }

        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End ACGrpGrdAssessPerBLS Submit...");
        return tReturn;
    }

    /**
     * 获得输入数据
     */
    public boolean getInputData(VData cInputData)
    {
        //获得前台传过来的数据
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            this.mLAAssessAccessorySchema = (LAAssessAccessorySchema)
                                            cInputData.getObject(1);
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAssess()
    {
        boolean tReturn = false;
        System.out.println("Start ACGrpGrdAssessPerBLS Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessPerBLS";
            tError.functionName = "saveLAAssess";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 开始插入LAAssessAccessory表...");
            LAAssessAccessoryDB tLAAssessAccessoryDB = new LAAssessAccessoryDB(
                    conn);
            tLAAssessAccessoryDB.setSchema(mLAAssessAccessorySchema);
            if (!tLAAssessAccessoryDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAssessAccessoryDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ACGrpGrdAssessPerBLS";
                tError.functionName = "saveLAAssess";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
            tReturn = true;
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ACGrpGrdAssessPerBLS";
            tError.functionName = "saveLAAssess";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
