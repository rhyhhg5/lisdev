package com.sinosoft.lis.seriousillness;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCIllnessInsuredListSchema;
import com.sinosoft.lis.vschema.LCIllnessInsuredListSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 增加被保人磁盘导入类</p>
 * <p>Description: 把从磁盘导入的被保人清单添加到数据库 </p>
 * <p>增加功能，处理从磁盘导入的多Sheet文档</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @rewrite by Wulg
 * @version 1.0
 */

public class IllnessInsuredList {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 批次号 */
    private String mBatchNo = null;

    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    /** 节点名 */
    private String[] sheetName = {"InsuredInfo", "Ver", "RiskInfo"};
    /** 配置文件名 */
    private String configName = "SIDiskImport.xml";
    private ByteArrayOutputStream baoStream ;
    // cache stream
    private PrintStream cacheStream;
    private PrintStream oldStream;
    

    /**
     * 构造函数，新契约入口
     * @param GrpContNo String
     * @param gi GlobalInput
     */
    public IllnessInsuredList(String GrpContNo, GlobalInput gi) {
        this.mGlobalInput = gi;
        this.mGrpContNo = GrpContNo;
    }

    /**
     * 添加传入的多个Sheet数据
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName) {
    	System.out.println(11);
    	 baoStream = new ByteArrayOutputStream(1024);
         // cache stream
         cacheStream = new PrintStream(baoStream);
         oldStream = System.out;
         
         
         
         
        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

        //从磁盘导入数据
        IllnessDiskImport importFile = new IllnessDiskImport(path + fileName,
                path + configName,
                sheetName);
        if (!importFile.doImport()) {
            this.mErrors.copyAllErrors(importFile.mErrors);
            System.out.println(importFile.mErrors);
            return false;
        }

        LCIllnessInsuredListSet tLCIllnessInsuredListSet = (LCIllnessInsuredListSet) importFile.
                                             getSchemaSet();
        this.mResult.add(tLCIllnessInsuredListSet);

        
        
    	
        
        //存放Insert Into语句的容器
        MMap map = new MMap();
        for (int i = 1; i <= tLCIllnessInsuredListSet.size(); i++) {
        	/*
        	if(tLCIllnessInsuredListSet.get(i).getIDType().equals("0")){
        			String strSQL = "select * from LCIllnessInsuredList where name = '"+ tLCIllnessInsuredListSet.get(i).getName() +"' "
        				+" and idtype = '"+ tLCIllnessInsuredListSet.get(i).getIDType() +"' and idno = '"+ tLCIllnessInsuredListSet.get(i).getIDNo() +"' "
        		          + " and GrpContNo ='"+ mGrpContNo +"'"
        		          +" with ur";
        			System.out.println(strSQL);
        			LCIllnessInsuredListSet mLCIllnessInsuredListSet = tLCIllnessInsuredListDB.executeQuery(strSQL);
        	
        			if(mLCIllnessInsuredListSet != null && mLCIllnessInsuredListSet.size() > 0){
        				mErrors.addOneError("导入错误，该被保人已存在！");
        				return false;
        			}
        			
        			
        			for(int j=i+1;j<= tLCIllnessInsuredListSet.size();j++){
 	        		   if(tLCIllnessInsuredListSet.get(i).getName().equals(tLCIllnessInsuredListSet.get(j).getName())&&
 	        				   tLCIllnessInsuredListSet.get(i).getIDType().equals(tLCIllnessInsuredListSet.get(j).getIDType())&&
 	        				   tLCIllnessInsuredListSet.get(i).getIDNo().equals(tLCIllnessInsuredListSet.get(j).getIDNo())
 	        		   ){
 	        			   mErrors.addOneError("导入错误，该excl被保人重复！");
 	        			   return false;
 	        		   }
 	        	}
        			
        	}else if(!tLCIllnessInsuredListSet.get(i).getIDType().equals("0")){
        		
        		String strSQL = "select * from LCIllnessInsuredList where name = '"
						+ tLCIllnessInsuredListSet.get(i).getName()
						+ "' "
						+ " and idtype = '"
						+ tLCIllnessInsuredListSet.get(i).getIDType()
						+ "' and idno = '"
						+ tLCIllnessInsuredListSet.get(i).getIDNo()
						+ "' "
						+ " and sex = '"+ tLCIllnessInsuredListSet.get(i).getSex() +"' and birthday = '"+ tLCIllnessInsuredListSet.get(i).getBirthday() +"' and GrpContNo ='"+ mGrpContNo +"' "
						+ "with ur";
        		System.out.println(strSQL);
				LCIllnessInsuredListSet mLCIllnessInsuredListSet = tLCIllnessInsuredListDB
						.executeQuery(strSQL);
				

				if (mLCIllnessInsuredListSet != null
						&& mLCIllnessInsuredListSet.size() > 0) {
					mErrors.addOneError("导入错误，该被保人已存在！");
					return false;
				}
				
				for(int j=i+1;j<= tLCIllnessInsuredListSet.size();j++){
	        		   if(tLCIllnessInsuredListSet.get(i).getName().equals(tLCIllnessInsuredListSet.get(j).getName())&&
	        				   tLCIllnessInsuredListSet.get(i).getIDType().equals(tLCIllnessInsuredListSet.get(j).getIDType())&&
	        				   tLCIllnessInsuredListSet.get(i).getIDNo().equals(tLCIllnessInsuredListSet.get(j).getIDNo())&&
	        				   tLCIllnessInsuredListSet.get(i).getSex().equals(tLCIllnessInsuredListSet.get(j).getSex())&&
	        				   tLCIllnessInsuredListSet.get(i).getBirthday().equals(tLCIllnessInsuredListSet.get(j).getBirthday())
	        		   ){
	        			   mErrors.addOneError("导入错误，该excl被保人重复！");
	        			   return false;
	        		   }
	        	}
				
        	}
        	*/
        		
            //添加一个被保人
        		if(!addOneInsured(map, tLCIllnessInsuredListSet.get(i), i)){
        			return false;
        		}
        }

        //提交数据到数据库
        if (!submitData(map)) {
        	 try {
        		System.setOut(oldStream);
             	cacheStream.close();
     			baoStream.close();
     			
     		} catch (IOException e) {
     			// TODO Auto-generated catch block
     			e.printStackTrace();
     		}
            return false;
        }
        
        try {
        	cacheStream.close();
			baoStream.close();			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return true;
    }

    /**
     * 添加一个被保人
     * @param map MMap
     * @param aLCInsuredSchema LCInsuredSchema
     */
    private boolean addOneInsured(MMap map,
                               LCIllnessInsuredListSchema cLCIllnessInsuredListSchema, int i) {
        LCIllnessInsuredListSchema tLCIllnessInsuredListSchema = cLCIllnessInsuredListSchema;
        if (tLCIllnessInsuredListSchema.getContNo() == null ||
            tLCIllnessInsuredListSchema.getContNo().equals("")) {
            return false;
        }
        tLCIllnessInsuredListSchema.setGrpContNo(mGrpContNo);
        tLCIllnessInsuredListSchema.setState("0"); //0为未生效，1为有效
        tLCIllnessInsuredListSchema.setBatchNo(this.mBatchNo);
        //在磁盘投保时合同号码存储合同id
        if (StrTool.cTrim(tLCIllnessInsuredListSchema.getContNo()).equals("")) {
            tLCIllnessInsuredListSchema.setContNo(String.valueOf(i));
        }
        tLCIllnessInsuredListSchema.setOperator(mGlobalInput.Operator);
        tLCIllnessInsuredListSchema.setMakeDate(mCurrentDate);
        tLCIllnessInsuredListSchema.setMakeTime(mCurrentTime);
        tLCIllnessInsuredListSchema.setModifyDate(mCurrentDate);
        tLCIllnessInsuredListSchema.setModifyTime(mCurrentTime);
       // String aSeqNo = PubFun1.CreateMaxNo("ILLNESS", 20);
        
        String aSeqNo=mBatchNo;
        if(i<10){
        	aSeqNo=aSeqNo+"0000"+i;
        }else if(i<100&&i>=10){
        	aSeqNo=aSeqNo+"000"+i;
        }else if(i<1000&&i>=100){
        	aSeqNo=aSeqNo+"00"+i;
        }else if(i<10000&&i>=1000){
        	aSeqNo=aSeqNo+"0"+i;
        }else{
        	aSeqNo=aSeqNo+i;
        }
        tLCIllnessInsuredListSchema.setSeqNo(aSeqNo);
        
        String Name = tLCIllnessInsuredListSchema.getName();
        String Sex = tLCIllnessInsuredListSchema.getSex();
        String Birthday = tLCIllnessInsuredListSchema.getBirthday();
        String IDType = tLCIllnessInsuredListSchema.getIDType();
        String IDNo = tLCIllnessInsuredListSchema.getIDNo();
        
        String strReturn =PubFun.CheckIDNo(IDType,IDNo,Birthday,Sex);
		if(!strReturn.equals("")){
			String error="客户"+Name+"的"+strReturn;
			mErrors.addOneError(error);
			return false;
		}
			map.put(tLCIllnessInsuredListSchema, "INSERT");
			return true;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        System.setOut(cacheStream);
        if (!tPubSubmit.submitData(data, "")) {
//          mErrors.copyAllErrors(tPubSubmit.mErrors);  
            System.out.println(111);
            String message = baoStream.toString();
            System.setOut(oldStream);
            message=message.substring(message.lastIndexOf(mBatchNo)-8,message.lastIndexOf(mBatchNo)-3);
            String error="";
            try {
				int i=Integer.parseInt(message)+2;
				error="上传EXCEL中第"+i+"行(含前两行的标题行)导入出错，请核查改正后重新上传！";
			} catch (Exception e) {
				error="上传EXCEL中数据行的第"+message+"行(不含前两行的标题行)导入出错，请核查改正后重新上传！";
			}                   
            mErrors.addOneError(error);
            return false;
        }
        return true;
    }

    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

   
}
