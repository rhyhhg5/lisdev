package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单质押贷款提交类</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Nicholas
 * @version 1.0
 */

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorLNConfirmBL implements EdorConfirm {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData;

	private MMap mMap = new MMap();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 业务数据 */
	private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

	private LCContHangUpStateSchema mLCContHangUpStateSchema = new LCContHangUpStateSchema();

	private GlobalInput mGlobalInput = new GlobalInput();

	private Reflections mReflections = new Reflections();

	public PEdorLNConfirmBL() {
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括""和""
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}
		System.out.println("---End getInputData---");

		// 数据校验
		if (!checkData()) {
			return false;
		}

		System.out.println("after checkData...");

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");

		return true;
	}

	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */
	public VData getResult() {
		return mResult;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName("LPEdorItemSchema", 0);
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
		} catch (Exception e) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorLNConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 数据校验
	 * 
	 * @return: boolean
	 */
	private boolean checkData() {
		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
		tLPEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
		tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
		tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
		tLPEdorItemDB.setPolNo(mLPEdorItemSchema.getPolNo());
		if (!tLPEdorItemDB.getInfo()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorLNConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "查询批改项目信息失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mLPEdorItemSchema.setSchema(tLPEdorItemDB.getSchema());

		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		try {
			// 获得此时的日期和时间
			String strCurrentDate = PubFun.getCurrentDate();
			String strCurrentTime = PubFun.getCurrentTime();

			// 交换借款表数据
			LOLoanSchema tLOLoanSchema = null;
			LPLoanDB tLPLoanDB = new LPLoanDB();
			LPLoanSet tLPLoanSet = new LPLoanSet();
			LPLoanSchema tLPLoanSchema = null;
			tLPLoanDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
			tLPLoanDB.setEdorType(mLPEdorItemSchema.getEdorType());
			tLPLoanSet = tLPLoanDB.query();
			if (tLPLoanSet.size() != 1) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "PEdorLNConfirmBL";
				tError.functionName = "dealData";
				tError.errorMessage = "查询保全贷款记录信息失败！";
				this.mErrors.addOneError(tError);
				return false;
			}

			tLPLoanSchema = new LPLoanSchema();
			tLPLoanSchema = tLPLoanSet.get(1);
			tLOLoanSchema = new LOLoanSchema();
			mReflections.transFields(tLOLoanSchema, tLPLoanSchema);
			mMap.put(tLOLoanSchema, "DELETE&INSERT");

			// 改变保单状态为贷款（保单层）
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
			if (!tLCContDB.getInfo()) {
				CError tError = new CError();
				tError.moduleName = "PEdorFCConfirmBL";
				tError.functionName = "prepareData";
				tError.errorMessage = "查询保单信息失败！";
				this.mErrors.addOneError(tError);
				return false;
			}
			LCContSchema tLCContSchema = tLCContDB.getSchema();
			mLCContHangUpStateSchema.setContNo(tLCContSchema.getContNo());
			mLCContHangUpStateSchema.setAppntNo(tLCContSchema.getAppntNo());
			mLCContHangUpStateSchema.setHangUpType(BQ.HANGUPTYPE_BQ);
			mLCContHangUpStateSchema.setHangUpNo(mLPEdorItemSchema.getEdorAcceptNo());
			mLCContHangUpStateSchema.setState(BQ.HANGUSTATE_ON);
			mLCContHangUpStateSchema.setOperator(mGlobalInput.Operator);
			mLCContHangUpStateSchema.setMakeDate(PubFun.getCurrentDate());
			mLCContHangUpStateSchema.setMakeTime(PubFun.getCurrentTime());
			mLCContHangUpStateSchema.setModifyDate(PubFun.getCurrentDate());
			mLCContHangUpStateSchema.setModifyTime(PubFun.getCurrentTime());
			mMap.put(mLCContHangUpStateSchema, SysConst.DELETE_AND_INSERT);

			// 设置保全主表数据到保全确认状态
			this.mLPEdorItemSchema.setEdorState("0");
			mLPEdorItemSchema.setOperator(this.mGlobalInput.Operator);
			mLPEdorItemSchema.setModifyDate(strCurrentDate);
			mLPEdorItemSchema.setModifyTime(strCurrentTime);
			mMap.put(mLPEdorItemSchema, "UPDATE");

			mResult.clear();
			mResult.add(mMap);
		} catch (Exception e) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorLNConfirmBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理错误! " + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}
}
