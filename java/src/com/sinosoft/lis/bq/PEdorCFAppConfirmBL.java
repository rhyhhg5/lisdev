package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全变更投保人项目</p>
 * <p>Description: 保全理算</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorCFAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private DetailDataQuery mQuery = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private LPPolSet mLPPolSet = null;
    
    private LPInsuredSet mLPInsuredSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到输入数据
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.
                    getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) cInputData.
                    getObjectByObjectName("LPEdorItemSchema", 0);
            mEdorNo = mLPEdorItemSchema.getEdorNo();
            mEdorType = mLPEdorItemSchema.getEdorType();
            mContNo = mLPEdorItemSchema.getContNo();
            mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if(!getLPPol())
        {
        	 mErrors.addOneError("未找到要拆分的保单险种信息，请重新录入保全明细");
             return false;
        }
        
        if(!getLPInsured())
        {
        	 mErrors.addOneError("未找到要拆分的保单被保人信息");
             return false;
        }
        
        if(!setLPCont())
        {
        	 mErrors.addOneError("拆分保单时出错，请重新录入保全明细");
             return false;
        }
        if(!setLPAppnt())
        {
        	mErrors.addOneError("拆分保单时出错，没有找到信的投保人信息，请重新录入保全明细！");
            return false;
        }
        
        
        return true;
    }

    private boolean getLPPol()
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        tLPPolDB.setEdorType(mEdorType);
        mLPPolSet = tLPPolDB.query();
        return true;
    }

    private boolean getLPInsured()
    {
    	LPInsuredDB tLPInsuredDB = new LPInsuredDB();
    	tLPInsuredDB.setEdorNo(mEdorNo);
    	tLPInsuredDB.setEdorType(mEdorType);
    	tLPInsuredDB.setRelationToAppnt("00");
    	mLPInsuredSet = tLPInsuredDB.query();
        return true;
    }
    
    private boolean setLPCont()
    {
        String sql = "select sum(prem), sum(SumPrem), sum(Amnt) from LPPol " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        SSRS tSSRS = (new ExeSQL()).execSQL(sql);
        if ((tSSRS == null) || (tSSRS.getMaxNumber() == 0))
        {
            mErrors.addOneError("未找到保单拆分信息！");
            return false;
        }
        String prem = tSSRS.GetText(1, 1);
        String sumPrem = tSSRS.GetText(1, 2);
        String amnt = tSSRS.GetText(1, 3);

        LPPolSchema tLPPolSchema = mLPPolSet.get(1);
       
        String appntName = null;
        String appntSex = null;
        String appntBirthday = null;
        String idType = null;
        String idNo = null;
        String bankCode = null;
        String bankAccNo = null;
        String accName = null;
        String appntNo = tLPPolSchema.getAppntNo();
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setAppntNo(appntNo);
        LCAppntSet tLCAppntSet = tLCAppntDB.query();
        
        if (tLCAppntSet.size() == 0)
        {
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(appntNo);
            if (!tLDPersonDB.getInfo())
            {
                mErrors.addOneError("未找到新投保人信息！");
                return false;
            }
            appntName = tLDPersonDB.getName();
            appntSex = tLDPersonDB.getSex();
            appntBirthday = tLDPersonDB.getBirthday();
            idType = tLDPersonDB.getIDType();
            idNo = tLDPersonDB.getIDNo();
        }
        else
        {
            LCAppntSchema tLCAppntSchema = tLCAppntSet.get(1);
            appntName = tLCAppntSchema.getAppntName();
            appntSex = tLCAppntSchema.getAppntSex();
            appntBirthday = tLCAppntSchema.getAppntBirthday();
            idType = tLCAppntSchema.getIDType();
            idNo = tLCAppntSchema.getIDNo();
            bankCode = tLCAppntSchema.getBankCode();
            bankAccNo = tLCAppntSchema.getBankAccNo();
            accName = tLCAppntSchema.getAccName();
        } 
        
        LPContSchema tLPContSchema = (LPContSchema)mQuery.getDetailData("LCCont", mContNo);
        tLPContSchema.setContNo(tLPPolSchema.getContNo());
        tLPContSchema.setInsuredNo(mLPInsuredSet.get(1).getInsuredNo());
        tLPContSchema.setInsuredName(mLPInsuredSet.get(1).getName());
        tLPContSchema.setInsuredBirthday(mLPInsuredSet.get(1).getBirthday());
        tLPContSchema.setInsuredIDNo(mLPInsuredSet.get(1).getIDNo());
        tLPContSchema.setInsuredIDType(mLPInsuredSet.get(1).getIDType());
        tLPContSchema.setPrtNo(mLPInsuredSet.get(1).getPrtNo());
        tLPContSchema.setPayMode("1");
        tLPContSchema.setAppntNo(appntNo);
        tLPContSchema.setAppntName(appntName);
        tLPContSchema.setAppntSex(appntSex);
        tLPContSchema.setAppntBirthday(appntBirthday);
        tLPContSchema.setAppntIDType(idType);
        tLPContSchema.setAppntIDNo(idNo);
        tLPContSchema.setBankCode(bankCode);
        tLPContSchema.setBankAccNo(bankAccNo);
        tLPContSchema.setAccName(accName);
        tLPContSchema.setPrem(prem);
        tLPContSchema.setSumPrem(sumPrem);
        tLPContSchema.setAmnt(amnt);
        tLPContSchema.setOperator(mGlobalInput.Operator);
        tLPContSchema.setMakeDate(mCurrentDate);
        tLPContSchema.setMakeTime(mCurrentTime);
        tLPContSchema.setModifyDate(mCurrentDate);
        tLPContSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPContSchema, "DELETE&INSERT");
        return true;
    }

    private boolean setLPAppnt()
    {
        String appntName = null;
        String appntSex = null;
        String appntBirthday = null;
        String idType = null;
        String idNo = null;
        String bankCode = null;
        String bankAccNo = null;
        String accName = null;
        String occupationType = null;
        String occupationCode = null;
        String marriage = null;
        String addressNo = null;
        LPPolSchema tLPPolSchema = mLPPolSet.get(1);
        String appntNo = tLPPolSchema.getAppntNo();
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setAppntNo(appntNo);
        LCAppntSet tLCAppntSet = tLCAppntDB.query();
        if (tLCAppntSet.size() == 0)
        {
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(appntNo);
            if (!tLDPersonDB.getInfo())
            {
                mErrors.addOneError("未找到新投保人信息！");
                return false;
            }
            appntName = tLDPersonDB.getName();
            appntSex = tLDPersonDB.getSex();
            appntBirthday = tLDPersonDB.getBirthday();
            idType = tLDPersonDB.getIDType();
            idNo = tLDPersonDB.getIDNo();
            occupationType = tLDPersonDB.getOccupationType();
            occupationCode = tLDPersonDB.getOccupationCode();
            LCInsuredSchema tLCInsuredSchema =
                    CommonBL.getLCInsured(mContNo, appntNo);
            addressNo = tLCInsuredSchema.getAddressNo();
        }
        else
        {
            LCAppntSchema tLCAppntSchema = tLCAppntSet.get(1);
            appntName = tLCAppntSchema.getAppntName();
            appntSex = tLCAppntSchema.getAppntSex();
            appntBirthday = tLCAppntSchema.getAppntBirthday();
            idType = tLCAppntSchema.getIDType();
            idNo = tLCAppntSchema.getIDNo();
            bankCode = tLCAppntSchema.getBankCode();
            bankAccNo = tLCAppntSchema.getBankAccNo();
            accName = tLCAppntSchema.getAccName();
            occupationType = tLCAppntSchema.getOccupationType();
            occupationCode = tLCAppntSchema.getOccupationCode();
            marriage = tLCAppntSchema.getMarriage();
            addressNo = tLCAppntSchema.getAddressNo();
        }
        LPAppntSchema tLPAppntSchema = (LPAppntSchema)
                mQuery.getDetailData("LCAppnt", mContNo);
        tLPAppntSchema.setEdorNo(mEdorNo);
        tLPAppntSchema.setEdorType(mEdorType);
        tLPAppntSchema.setGrpContNo(BQ.GRPFILLDATA);
        tLPAppntSchema.setContNo(tLPPolSchema.getContNo());
        tLPAppntSchema.setPrtNo(tLPPolSchema.getPrtNo());
        tLPAppntSchema.setAppntNo(appntNo);
        tLPAppntSchema.setAppntGrade("");
        tLPAppntSchema.setAppntName(appntName);
        tLPAppntSchema.setPrtNo(mLPInsuredSet.get(1).getPrtNo());
        tLPAppntSchema.setAppntSex(appntSex);
        tLPAppntSchema.setAppntBirthday(appntBirthday);
        tLPAppntSchema.setAppntType("");
        tLPAppntSchema.setAddressNo(addressNo);
        tLPAppntSchema.setIDType(idType);
        tLPAppntSchema.setIDNo(idNo);
        tLPAppntSchema.setMarriage(marriage);
        tLPAppntSchema.setBankCode(bankCode);
        tLPAppntSchema.setBankAccNo(bankAccNo);
        tLPAppntSchema.setAccName(accName);
        tLPAppntSchema.setOccupationCode(occupationCode);
        tLPAppntSchema.setOccupationType(occupationType);
        tLPAppntSchema.setOperator(mGlobalInput.Operator);
        tLPAppntSchema.setMakeDate(mCurrentDate);
        tLPAppntSchema.setMakeTime(mCurrentTime);
        tLPAppntSchema.setModifyDate(mCurrentDate);
        tLPAppntSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPAppntSchema, "DELETE&INSERT");
        return true;
    }
}
