package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 协议退保明细</p>
 * <p>Copyright: Copyright (c) 2005.3.31</p>
 * <p>Company: Sinosoft</p>
 * @author LHS
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorXTDetailUI
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();


    /** 传出数据的容器 */
    private VData mResult = new VData();


    /** 数据操作字符串 */
    private String mOperate;


    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public PEdorXTDetailUI()
    {
    }


    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 数据操作字符串拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        PEdorXTDetailBL tPEdorXTDetailBL = new PEdorXTDetailBL();
        if (!tPEdorXTDetailBL.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPEdorXTDetailBL.mErrors);
            return false;
        }
        else
        {
            mResult = tPEdorXTDetailBL.getResult();
        }

        return true;
    }


    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }


    /**
     * 主函数，测试用
     */
    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "endor0";
        tGlobalInput.ManageCom = "86";

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorAcceptNo("20051020000026");
        tLPEdorItemSchema.setEdorNo("20051020000026");
        tLPEdorItemSchema.setContNo("00001551001");
        tLPEdorItemSchema.setEdorType("XT");
        tLPEdorItemSchema.setGetMoney(600.5);

        LPPolSet tLPPolSet = new LPPolSet();

        LPPolSchema schema = new LPPolSchema();
        schema.setEdorNo(tLPEdorItemSchema.getEdorNo());
        schema.setEdorType(tLPEdorItemSchema.getEdorType());
        schema.setContNo(tLPEdorItemSchema.getContNo());
        schema.setPolNo("21000008732");
        tLPPolSet.add(schema);

        LPPolSchema schema2 = new LPPolSchema();
        schema2.setEdorNo(tLPEdorItemSchema.getEdorNo());
        schema2.setEdorType(tLPEdorItemSchema.getEdorType());
        schema2.setContNo(tLPEdorItemSchema.getContNo());
        schema2.setPolNo("21000009086");
        tLPPolSet.add(schema2);

        LPPolSchema schema3 = new LPPolSchema();
        schema3.setEdorNo(tLPEdorItemSchema.getEdorNo());
        schema3.setEdorType(tLPEdorItemSchema.getEdorType());
        schema3.setContNo(tLPEdorItemSchema.getContNo());
        schema3.setPolNo("21000009087");
        tLPPolSet.add(schema3);


        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPEdorItemSchema);
        tVData.add("polPrem");
        tVData.add(tLPPolSet);;

        PEdorXTDetailUI tEdorXTDetailUI = new PEdorXTDetailUI();
        if (!tEdorXTDetailUI.submitData(tVData, "INSERT"))
        {
            System.out.println(tEdorXTDetailUI.mErrors.getErrContent());
        }
    }
}
