package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**s
 * <p>Title: 保全无名单实名化</p>
 * <p>Description: 项目明细录入</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorWDDetailBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mOperator = null;

    private String mEdorNo = null;

    private String mEdorType = BQ.EDORTYPE_WD;

    private String mGrpContNo = null;

    private String mEdorValiDate = null;

    private String mMessage = null;

    private LCInsuredSet mLCInsuredSet = null;

    private LPDiskImportSet mLPDiskImportSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private TransferData mTransferData = new TransferData();

    private String mCurrentTime = PubFun.getCurrentTime();

    private String mOP ;

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData(VData data, String operator)
    {
        this.mOperator = operator;
        if (mOperator == null)
        {
            mErrors.addOneError("缺少控制标志！");
            return false;
        }
            System.out.println("mOperator" + mOperator);
        if (!getInputData(data))
        {
            return false;
        }

        if(!checkDate())
        {
            return false ;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到错误信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到磁盘导入数据
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            LPGrpEdorItemSchema tLPGrpEdorItemSchema = (LPGrpEdorItemSchema)
                    cInputData.getObjectByObjectName("LPGrpEdorItemSchema", 0);
            mLCInsuredSet = (LCInsuredSet)
                    cInputData.getObjectByObjectName("LCInsuredSet", 0);
            mLPDiskImportSet = (LPDiskImportSet)
                    cInputData.getObjectByObjectName("LPDiskImportSet", 0);
            mEdorNo = tLPGrpEdorItemSchema.getEdorNo();
            mGrpContNo = tLPGrpEdorItemSchema.getGrpContNo();
            mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
            mOP =(String)mTransferData.getValueByName("operator");
            System.out.println(mOP);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入参数错误！");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mEdorNo);
        tLPGrpEdorItemDB.setEdorType(mEdorType);
        tLPGrpEdorItemDB.setGrpContNo(mGrpContNo);
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            mErrors.addOneError("未找到保全项目信息！");
            return false;
        }
        mEdorValiDate = tLPGrpEdorItemSet.get(1).getEdorValiDate();

        if (mOperator.equals("ADD"))
        {
            for (int i = 1; i <= mLCInsuredSet.size(); i++)
            {
                String insuredNo = mLCInsuredSet.get(i).getInsuredNo();
                LCInsuredDB tLCInsuredDB = new LCInsuredDB();
                tLCInsuredDB.setGrpContNo(mGrpContNo);
                tLCInsuredDB.setInsuredNo(insuredNo);
                LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
                if (tLCInsuredSet.size() != 1)
                {
                    mErrors.addOneError("保单数据错误，团体合同下存在多个被保人" + insuredNo);
                    return false;
                }
                System.out.println("edorValidate:" + mEdorValiDate);
                LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(1);
                LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
                tLPDiskImportSchema.setEdorNo(mEdorNo);
                tLPDiskImportSchema.setEdorType(mEdorType);
                tLPDiskImportSchema.setGrpContNo(mGrpContNo);
                tLPDiskImportSchema.setSerialNo(tLCInsuredSchema.getInsuredNo());
                tLPDiskImportSchema.setState("1");
                tLPDiskImportSchema.setInsuredNo(tLCInsuredSchema.getInsuredNo());
                tLPDiskImportSchema.setInsuredName(tLCInsuredSchema.getName());
                tLPDiskImportSchema.setSex(tLCInsuredSchema.getSex());
                tLPDiskImportSchema.setBirthday(tLCInsuredSchema.getBirthday());
                tLPDiskImportSchema.setIDNo(tLCInsuredSchema.getIDNo());
                tLPDiskImportSchema.setIDType(tLCInsuredSchema.getIDType());
                tLPDiskImportSchema.setContPlanCode(tLCInsuredSchema.getContPlanCode());
                tLPDiskImportSchema.setOccupationType(tLCInsuredSchema.getOccupationType());
                tLPDiskImportSchema.setEdorValiDate(mEdorValiDate);
                tLPDiskImportSchema.setBankCode(tLCInsuredSchema.getBankCode());
                tLPDiskImportSchema.setBankAccNo(tLCInsuredSchema.getBankAccNo());
                tLPDiskImportSchema.setAccName(tLCInsuredSchema.getAccName());
                tLPDiskImportSchema.setOthIDNo(tLCInsuredSchema.getOthIDNo());
                tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
                tLPDiskImportSchema.setMakeDate(mCurrentDate);
                tLPDiskImportSchema.setMakeTime(mCurrentTime);
                tLPDiskImportSchema.setModifyDate(mCurrentDate);
                tLPDiskImportSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPDiskImportSchema, "DELETE&INSERT");
            }
        }
        else if (mOperator.equals("DEL"))
        {
            System.out.println("mLPDiskImportSet.size():" + mLPDiskImportSet.size());
            for (int i = 1; i <= mLPDiskImportSet.size(); i++)
            {
                LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
                String sql = "delete from LPDiskImport " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + mEdorType + "' " +
                        "and GrpContNo = '" + mGrpContNo + "' " +
                        "and SerialNo = '" + tLPDiskImportSchema.getSerialNo() + "'";
                mMap.put(sql, "DELETE");
            }
        }
        else if (mOperator.equals("SAVE"))
        {
            mLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, mEdorType,
                    mGrpContNo, BQ.IMPORTSTATE_SUCC);
            if (mLPDiskImportSet.size() == 0)
            {
                mErrors.addOneError("找不到磁盘导入数据！");
                return false;
            }
            if (!checkData())
            {
                return false;
            }
            changeEdorState(BQ.EDORSTATE_INPUT);
        }
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if (!checkInsured())
        {
            return false;
        }
        return true;
    }

    private boolean checkInsured()
    {
        boolean flag = true;
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            String errorReason = "";
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            tLCInsuredSchema.setGrpContNo(mGrpContNo);
            tLCInsuredSchema.setName(tLPDiskImportSchema.getInsuredName());
            tLCInsuredSchema.setSex(tLPDiskImportSchema.getSex());
            tLCInsuredSchema.setBirthday(tLPDiskImportSchema.getBirthday());
            tLCInsuredSchema.setIDType(tLPDiskImportSchema.getIDType());
            tLCInsuredSchema.setIDNo(tLPDiskImportSchema.getIDNo());
            tLCInsuredSchema.setOthIDNo(tLPDiskImportSchema.getOthIDNo());
            OldCustomerCheck tOldCustomerCheck = new OldCustomerCheck(tLCInsuredSchema);
            if (tOldCustomerCheck.checkInsured() != OldCustomerCheck.OLD)
            {
                errorReason += "保单中不存在被保人" + tLCInsuredSchema.getName() +
                        "，导入无效！";
                flag = false;
            }
            if (!errorReason.equals(""))
            {
                String sql = "update LPDiskImport " +
                        "set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                        " ErrorReason = '" + errorReason + "', " +
                        " Operator = '" + mGlobalInput.Operator + "', " +
                        " ModifyDate = '" + mCurrentDate + "', " +
                        " ModifyTime = '" + mCurrentTime + "' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + mEdorType + "' " +
                        "and GrpContNo = '" + mGrpContNo + "' " +
                        "and SerialNo = '" + tLPDiskImportSchema.getSerialNo() +
                        "' ";
                mMap.put(sql, "UPDATE");
            }
            else
            {
                String edorValiDate = tLPDiskImportSchema.getEdorValiDate();
                if ((edorValiDate == null) || (edorValiDate.equals("")))
                {
                    edorValiDate = mEdorValiDate;
                }
                String sql = "update LPDiskImport " +
                        "set State = '" + BQ.IMPORTSTATE_SUCC + "', " +
                        " InsuredNo = '" + tOldCustomerCheck.getCustomerNo() + "', " +
                        " EdorValiDate = '" + edorValiDate + "', " +
                        " Operator = '" + mGlobalInput.Operator + "', " +
                        " ModifyDate = '" + mCurrentDate + "', " +
                        " ModifyTime = '" + mCurrentTime + "' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + mEdorType + "' " +
                        "and GrpContNo = '" + mGrpContNo + "' " +
                        "and SerialNo = '" + tLPDiskImportSchema.getSerialNo() +
                        "' ";
                mMap.put(sql, "UPDATE");
            }
        }
        if (flag == false)
        {
            mMessage = "导入数据中有无效的被保人信息！";
        }
        return true;
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void changeEdorState(String edorState)
    {
        String sql = "update  LPGrpEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }


    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    private boolean checkDate()
    {
        if(mOP.equals("SAVE"))
        {
            if (!checkLP()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkLP()
    {
        LPDiskImportSet  tLPDiskImportSet = new LPDiskImportSet();
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        tLPDiskImportDB.setEdorNo(mEdorNo);
        tLPDiskImportDB.setGrpContNo(mGrpContNo);
        tLPDiskImportSet = tLPDiskImportDB.query();
        for (int i = 1 ; i<= tLPDiskImportSet.size() ; i++)
        {
        	ExeSQL tExeSQL = new ExeSQL();
        	LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(i);
            String sql1 = "select insuredno from lcinsured where grpcontno = '" + mGrpContNo 
                        + "' and Name = '" + tLPDiskImportSchema.getInsuredName()
                        + "' and Sex = '" + tLPDiskImportSchema.getSex()
                        + "' and Birthday = '" + tLPDiskImportSchema.getBirthday()
                        + "' and IDType = '" + tLPDiskImportSchema.getIDType()
                        + "' and IDNo = '" + tLPDiskImportSchema.getIDNo() + "' ";
            String tcustomerno = tExeSQL.getOneValue(sql1);
            String sql ="select caseno From llcase where customerno='"+tcustomerno+"' and rgtstate not in ('14','11','12')";
            String tCaseNo = new ExeSQL().getOneValue(sql);

            if(!tCaseNo.equals("")&&tCaseNo != null)
            {
                mErrors.addOneError("客户"+tcustomerno+"有正在处理的理赔"+tCaseNo);
                return false ;
            }
            String sqlnew ="select a.caseno from llcase a,ljagetclaim b where a.customerno='"+tcustomerno+"' and a.rgtstate in ('11','12') and a.caseno=b.otherno and b.grpcontno='"+mGrpContNo+"' ";
            String tCaseNonew = new ExeSQL().getOneValue(sqlnew);
            if(!tCaseNonew.equals("")&&tCaseNonew != null)
            {
                mErrors.addOneError("客户"+tcustomerno+"有已经结案的理赔"+tCaseNonew);
                return false ;
            }
        }
        return true ;
    }
}
