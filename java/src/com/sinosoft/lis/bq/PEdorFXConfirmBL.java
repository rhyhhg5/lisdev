package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 1.0
 */
public class PEdorFXConfirmBL implements EdorConfirm
{
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private LPEdorItemSchema mLPEdorItemSchema =new LPEdorItemSchema();
//    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();

    private String mLastPayToDate = null;
    private String mPolPayToDate = null;
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();

    public PEdorFXConfirmBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public void setOperate(String cOperate)
    {
        this.mOperate = cOperate;
    }

    public String getOperate()
    {
        return this.mOperate;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.setOperate(cOperate);

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //数据准备操作
        if(!dealData())
        {
            return false;
        }

//        PubSubmit tSubmit = new PubSubmit();
//        if(!tSubmit.submitData(mResult, ""))
//        { //数据提交
//            // @@错误处理
//            this.mErrors.copyAllErrors(tSubmit.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "PEdorFXConfirmBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "数据提交失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }

        return true;
    }

    public VData getResult()
    {
        VData d = new VData();
        d.add(map);
        return d;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        LPEdorItemSchema tLPEdorItemSchema= (LPEdorItemSchema) cInputData.getObjectByObjectName(
            "LPEdorItemSchema", 0);
        mLPEdorItemSchema.setSchema(tLPEdorItemSchema);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
            "GlobalInput", 0));

        if(mLPEdorItemSchema == null || mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "GrpEdorNIAppConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if(tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
        {
            mErrors.copyAllErrors(tLPEdorItemDB.mErrors);
            mErrors.addOneError(new CError("查询保全项目信息失败！"));
            return false;
        }
        mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));

        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorMainDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorMainDB.setContNo(mLPEdorItemSchema.getContNo());
        if(!tLPEdorMainDB.getInfo())
        {
            mErrors.copyAllErrors(tLPEdorMainDB.mErrors);
            mErrors.addOneError(new CError("查询保全信息失败！"));
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData()
    {
    	
        boolean riskULIFlag = false;  //万能险复效标志
        boolean riskTaxFlag = false;//税优险种复效标志
        //查询复效险种
        StringBuffer sql = new StringBuffer();
        sql.append("select * from lppol where edorno ='")
            .append(mLPEdorItemSchema.getEdorNo()).append("' ")
            .append("   and EdorType = '")
            .append(mLPEdorItemSchema.getEdorType()).append("' ");
        System.out.println(sql);
        LPPolDB tLPPolDB = new LPPolDB();
        LPPolSet tLPPolSet = tLPPolDB.executeQuery(sql.toString());
        if(tLPPolSet == null || tLPPolSet.size() == 0)
        {
            this.mErrors.addOneError("复效险种查询错误！");
            return false;
        }

        //查询复效生效日
        EdorItemSpecialData tEdorItemSpecialData
            = new EdorItemSpecialData(mLPEdorItemSchema.getEdorNo()
                                      , mLPEdorItemSchema.getEdorType());
        if(!tEdorItemSpecialData.query())
        {
            this.mErrors.addOneError("复效日期查询错误！");
            return false;
        }
        String nextDay = PubFun.calDate(tEdorItemSpecialData.getEdorValue(
            "FX_D"), 1, "D", "");
        System.out.println(nextDay);

        boolean isUpdateStateFlag = true;
        for(int k = 1; k <= tLPPolSet.size(); k++)
        {
            int m = 0;
            if (tLPPolSet.get(k).getPayIntv() != 0) {
                if (!tLPPolSet.get(k).getPaytoDate().equals(tEdorItemSpecialData.getEdorValue("FX_D")))
                {
                    m = PubFun.calInterval(tLPPolSet.get(k).getPaytoDate(),
                                       tEdorItemSpecialData.getEdorValue("FX_D"),
                                       "M") / tLPPolSet.get(k).getPayIntv()+ 1;
                    //whl
                    if(PubFun.calInterval(tEdorItemSpecialData.getEdorValue("FX_D"),tLPPolSet.get(k).getPayEndDate(),"M")<=0)
                    {
                    	m=PubFun.calInterval(tLPPolSet.get(k).getPaytoDate(), tLPPolSet.get(k).getPayEndDate(), "M")
                        / tLPPolSet.get(k).getPayIntv() ;
                    }

                }
            }

            createPolPayInfo(tLPPolSet.get(k), m);
            /**
             * 税优险种后满期日期加宽限期小于当前日期,不恢复保单和险种状态2018-2-7添加
             */
            String checkTaxOptimal = "select 1 from lmriskapp where riskcode in (select riskcode from lcpol where polno = '"+tLPPolSet.get(k).getPolNo()+"') and riskType4 = '4' and taxOptimal = 'Y'";
            String isTaxOptimal = new ExeSQL().getOneValue(checkTaxOptimal);
            if(isTaxOptimal!=null && !"".equals(isTaxOptimal)){
            	riskTaxFlag = true;
            	//查询险种的宽限期
            	String gracePeriodSql = "select max(a.GracePeriod) from LMRiskPay a, LCPol b where a.RiskCode = b.RiskCode and polno = '"+tLPPolSet.get(k).getPolNo()+"'";
            	String gracePeriod = new ExeSQL().getOneValue(gracePeriodSql);
            	if(gracePeriod!=null && !"".equals(gracePeriod)){
            		if(CommonBL.stringToDate(PubFun.calDate(tLPPolSet.get(k).getEndDate(), Integer.valueOf(gracePeriod), "D", "")).before(CommonBL.stringToDate(mCurDate))){
            			isUpdateStateFlag = false;
            		}
            	}else{
            		CError tError = new CError();
	                tError.moduleName = "PEdorFXAppConfirmBL";
	                tError.functionName = "dealData";
	                tError.errorMessage = "未查询到该税优险种："+tLPPolSet.get(k).getRiskCode()+"的宽限期";
	                   
	                return false;
            	}
            }
            

            if(isUpdateStateFlag){
            	String updateStr =
            		"update LCPol "
            		+ "set StateFlag = '" + BQ.STATE_FLAG_SIGN + "', "
            		+ " PolState = null, "
            		+ "   LastEdorDate = '"
            		+ mLPEdorItemSchema.getEdorValiDate() + "', "
            		+ "   Operator = '" + mGlobalInput.Operator + "', "
            		+ "   ModifyDate = '" + mCurDate + "', "
            		+ "   ModifyTime = '" + mCurTime + "' "
            		+ "where PolNo = '" + tLPPolSet.get(k).getPolNo() + "' ";
            	map.put(updateStr, SysConst.UPDATE);
            	
            	updateStr = "update LCContState set State = '0' ,EndDate='"+mLPEdorItemSchema.getEdorValiDate()+"' "
            	+ "where contno ='"
            	+ this.mLPEdorItemSchema.getContNo()
            	+ "' and polno ='"
            	+ tLPPolSet.get(k).getPolNo()
            	+ "' and StateType ='Available' and State = '1' ";
            	map.put(updateStr, SysConst.UPDATE);
            }else{
            	String updateStr =
            		"update LCPol "
            		+ "set "
            		+ "   LastEdorDate = '"
            		+ mLPEdorItemSchema.getEdorValiDate() + "', "
            		+ "   Operator = '" + mGlobalInput.Operator + "', "
            		+ "   ModifyDate = '" + mCurDate + "', "
            		+ "   ModifyTime = '" + mCurTime + "' "
            		+ "where PolNo = '" + tLPPolSet.get(k).getPolNo() + "' ";
            	map.put(updateStr, SysConst.UPDATE);
            }
            
            if("231001".equals(tLPPolSet.get(k).getRiskCode()) || "231201".equals(tLPPolSet.get(k).getRiskCode())||"231801".equals(tLPPolSet.get(k).getRiskCode())){
            	String sqld = "select startdate from lccontstate where " +
            			" polno = '" + tLPPolSet.get(k).getPolNo() + "'" +
            					" and statetype='Available' and state='1' with ur";
	               String tstartdate = new ExeSQL().getOneValue(sqld);
	               if(tstartdate == null || tstartdate.equals(""))
	               {
	                   CError tError = new CError();
	                   tError.moduleName = "PEdorFXAppConfirmBL";
	                   tError.functionName = "checkData";
	                   tError.errorMessage = "未查到附加重疾险的失效日期";
	                   
	                   return false;
	                }

	               if(!tLPPolSet.get(k).getPaytoDate().equals(tEdorItemSpecialData.getEdorValue("FX_D"))){
	            	   riskULIFlag = true;
	               }else if(tLPPolSet.get(k).getPaytoDate().equals(tEdorItemSpecialData.getEdorValue("FX_D"))){
	            	   LCPolSchema tLCPolSchema = new LCPolSchema();
	            	   Reflections tReflections = new Reflections();
	            	   tReflections.transFields(tLCPolSchema, tLPPolSet.get(k).getSchema());
	            	   UARCalRPBL tUARCalRPBL = new UARCalRPBL();
	            	   if (tUARCalRPBL.submitDataP(tLPPolSet.get(k).getSchema(),tLCPolSchema,tstartdate,"12")) {
	            		   System.out.println("万能附加险种风险扣费计算成功");
	            	   }
	               }
	                  
            }
        }

        mResult.clear();
        this.mLPEdorItemSchema.setEdorState("0");
        if(isUpdateStateFlag){
        	LCContStateDB t = new LCContStateDB();
        	String s = "select * from lccontstate "
        		+ "where contno ='" + mLPEdorItemSchema.getContNo() + "' "
        		+ "   and polno = '000000' "
        		+ "   and StateType ='Available' and State = '1'";
        	LCContStateSet tS = t.executeQuery(s);
        	if(tS != null && tS.size() > 0)
        	{
        		sql = new StringBuffer();
        		sql.append("update LCContState set State = '0' ,EndDate='"+mLPEdorItemSchema.getEdorValiDate()+"' ")
        		.append("where contno ='")
        		.append(this.mLPEdorItemSchema.getContNo())
        		.append("' and polno ='000000' ")
        		.append(" and StateType ='Available' and State = '1' ");
        		map.put(sql.toString(), SysConst.UPDATE);
        		
        		map.put("update LCCont a set StateFlag = '"
        				+ BQ.STATE_FLAG_SIGN + "', "
        				+ "  PayToDate = (select min(PayToDate) from LCPOL "
        				+ "         where ContNo = a.ContNo and StateFlag = '1' ), "
        				+ "  Operator = '" + mGlobalInput.Operator + "', "
        				+ "  ModifyDate = '" + mCurDate + "', "
        				+ "  ModifyTime = '" + mCurTime + "' "
        				+ "where ContNo = '" + mLPEdorItemSchema.getContNo() + "' ",
        				SysConst.UPDATE);
        	}
        	map.put("update LCCont a set StateFlag = '"
        			+ BQ.STATE_FLAG_SIGN + "', "
        			+ "  PayToDate = (select min(PayToDate) from LCPOL "
        			+ "         where ContNo = a.ContNo and StateFlag = '1' ), "
        			+ "  Operator = '" + mGlobalInput.Operator + "', "
        			+ "  ModifyDate = '" + mCurDate + "', "
        			+ "  ModifyTime = '" + mCurTime + "' "
        			+ "where ContNo = '" + mLPEdorItemSchema.getContNo() + "' ",
        			SysConst.UPDATE);
        }else{
        	map.put("update LCCont a set "
        			+ "  PayToDate = (select min(PayToDate) from LCPOL "
        			+ "         where ContNo = a.ContNo ), "
        			+ "  Operator = '" + mGlobalInput.Operator + "', "
        			+ "  ModifyDate = '" + mCurDate + "', "
        			+ "  ModifyTime = '" + mCurTime + "' "
        			+ "where ContNo = '" + mLPEdorItemSchema.getContNo() + "' ",
        			SysConst.UPDATE);        	
        }
        map.put(mLPEdorItemSchema, SysConst.UPDATE);
       //删除打印管理表中的数据
        this.deleteFXInf();
        
        
        if(riskULIFlag){
        	//处理万能账户表信息
        	validateEdorData();
        	//查询万能账户表
        	LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        	LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
        	LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
        	tLCInsureAccDB.setContNo(mLPEdorItemSchema.getContNo());
        	tLCInsureAccSet = tLCInsureAccDB.query();
        	if(tLCInsureAccSet!=null&&tLCInsureAccSet.size()>0){
        		tLCInsureAccSchema = tLCInsureAccSet.get(1).getSchema();
        		updateAccInfo(tLCInsureAccSchema);
        	}
        }
        if(riskTaxFlag){
        	//处理万能账户表信息
        	validateEdorData();
        }
        
        
        mResult.add(map);
        
        return true;
    }
    
    /**
     * updateAccInfo
     * 更新总账户信息
     * @return MMap
     */
    private MMap updateAccInfo(LCInsureAccSchema mLCInsureAccSchema)
    {
        MMap tMMap = new MMap();

        String sql = "update LCInsureAccClass a "
                     + "set (InsuAccBala, LastAccBala) "
                     + "   =(select sum(Money), sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo) "
                     + "where PolNo = '"
                     + mLCInsureAccSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        //更新账户信息
        sql = "update LCInsureAcc a "
              + "set (InsuAccBala, LastAccBala, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(InsuAccBala), sum(LastAccBala), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LCInsureAccClass where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + mLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        
        //按轨迹更新账户管理费信息
        sql = "update LCInsureAccClassFee a " +
              "set Fee =(select sum(Fee) from LCInsureAccFeeTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo ) "
              + "where PolNo = '"
              + mLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        //更新账户管理费信息
        sql = "update LCInsureAccFee a " +
              "set (Fee, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(Fee), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LCInsureAccClassFee where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + mLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        return tMMap;
    }    
    
    
    
    private boolean validateEdorData()
    {

    	ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,
                mLPEdorItemSchema.getEdorNo(), mLPEdorItemSchema.getEdorType(),
                mLPEdorItemSchema.getContNo(), "ContNo");


        	String[] tables1 =
            {"LCInsureAcc", "LCInsureAccClass", "LCInsureAccFee",
            "LCInsureAccClassFee"};
            if (!validate.changeData(tables1))
            {
                mErrors.copyAllErrors(validate.mErrors);
                return false;
            }       
        MMap tmap = validate.getMap();
        map.add(tmap);
        String[] tables2 =
                {"LCInsureAccTrace", "LCInsureAccFeeTrace"};
        validate = new ValidateEdorData2(mGlobalInput,
                                         mLPEdorItemSchema.getEdorNo(),
                                         mLPEdorItemSchema.getEdorType(),
                                         mLPEdorItemSchema.getContNo(),
                                         "ContNo");
        if (!validate.addData(tables2))
        {
            mErrors.copyAllErrors(validate.mErrors);
            return false;
        }
        map.add(validate.getMap());
        return true;
    }
    
    /**
     * 如果是FX 则删除打印管理表中的数据
     * add by hyy
     * @return
     */
   private boolean deleteFXInf(){
    	String sql2="delete from LOPrtManager where OtherNo = '"+mLPEdorItemSchema.getContNo()+"' and code='42'";
    	map.put(sql2,"DELETE");
    	return true ;
    }
    private void updatePayToDate(String table, String payToDate, String polNo,int paytimes)
    {
    	if(!table.toUpperCase().equals("LCPREM")){
    		String sql = "update " + table + " "
    		+ "set PayToDate = '" + payToDate + "', "
    		+ "   Operator = '" + mGlobalInput.Operator + "', "
    		+ "   ModifyDate = '" + mCurDate + "', "
    		+ "   ModifyTime = '" + mCurTime + "' "
    		+ "where PolNo = '" + polNo + "' ";
    		map.put(sql, SysConst.UPDATE);
    	}else{
    		String sql = "update " + table + " "
    		+ "set PayToDate = '" + payToDate + "', "
    		+ "   paytimes = " + paytimes + ", "
    		+ "   Operator = '" + mGlobalInput.Operator + "', "
    		+ "   ModifyDate = '" + mCurDate + "', "
    		+ "   ModifyTime = '" + mCurTime + "' "
    		+ "where PolNo = '" + polNo + "' ";
    		map.put(sql, SysConst.UPDATE);
    	}
    }

    private void createPolPayInfo(LPPolSchema tLPPolSchema, int m)
    {
    	boolean isXUQI=false;
        LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
        double polGetMoneyLeft = getPolGetMoney(tLPPolSchema);
        //若没有追述前期保费，则不需要生成实收数据
        if(m == 0)
        {
            return;
        }

        mPolPayToDate = tLPPolSchema.getPaytoDate();
        
        //查询万能续期标志
        //查询复效生效日
        EdorItemSpecialData tEdorItemSpecialData
            = new EdorItemSpecialData(mLPEdorItemSchema.getEdorNo()
                                      , mLPEdorItemSchema.getEdorType());
        if(!tEdorItemSpecialData.query())
        {
        	return;
        }
        if(tEdorItemSpecialData.getEdorValue("FX_XUQI")!=null&&"1".equals(tEdorItemSpecialData.getEdorValue("FX_XUQI"))){
        	isXUQI = true;
        	 //将附加重疾的万能保单缓缴状态恢复以便续期核销处理
        	if("231001".equals(tLPPolSchema.getRiskCode())||"231201".equals(tLPPolSchema.getRiskCode()) ||"231801".equals(tLPPolSchema.getRiskCode())){
        		if(tLPPolSchema.getStandbyFlag1()!=null&&tLPPolSchema.getStandbyFlag1().equals("1")){
       	        	map.put("update lcpol set standbyflag1=null," +
       	        			" ModifyDate = '" + mCurDate + "', " +
       	        			" ModifyTime = '" + mCurTime + "' " +
       	        			" where contno='"+tLPPolSchema.getContNo()+"'", 
       	        			SysConst.UPDATE);
       	        }
        	}
        }
        
        //get prem
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(tLPPolSchema.getPolNo());
        LCPremSet tLCPremSet = tLCPremDB.query();
        for(int i = 1; i <= tLCPremSet.size(); i++)
        {
        	//复效两期保费以上LastPayToDate从复效时算起
        	mLastPayToDate = PubFun.calDate(tLCPremSet.get(i).getPaytoDate(),0,"M", "");
        	LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
        	if(isXUQI){
        		mPolPayToDate = PubFun.calDate(tLCPremSet.get(i).getPaytoDate(),
        				( tLCPremSet.get(i).getPayIntv()),
        				"M", "");
        		//生成LJAPAYPERSON
        		 tLJAPayPersonSchema
        		= getOneLJAPayPersonInfo(tLPPolSchema,
        				tLCPremSet.get(i),
        				tLCPremSet.get(i).getPrem());
        		mLJAPayPersonSet.add(tLJAPayPersonSchema);
        	}else{
        		mPolPayToDate = PubFun.calDate(tLCPremSet.get(i).getPaytoDate(),
        				( m*tLCPremSet.get(i).getPayIntv()),
        				"M", "");
        		//生成LJAPAYPERSON
        		 tLJAPayPersonSchema
        		= getOneLJAPayPersonInfo(tLPPolSchema,
        				tLCPremSet.get(i),
        				tLCPremSet.get(i).getPrem()*m);
        		mLJAPayPersonSet.add(tLJAPayPersonSchema);
        	}
//            mLastPayToDate = PubFun.calDate(tLCPremSet.get(i).getPaytoDate(),
//                                            ((m - 1)
//                                             * tLCPremSet.get(i).getPayIntv()),
//                                            "M", "");
            

            updatePayToDate("LCPrem", mPolPayToDate, tLPPolSchema.getPolNo(),tLCPremSet.get(i).getPayTimes()+1);
            polGetMoneyLeft -= tLJAPayPersonSchema.getSumActuPayMoney();
        }

//        //将利息累加最后一个缴费项目
//        mPolPayToDate = PubFun.calDate(tLPPolSchema.getPaytoDate(),
//                                       (m * tLCPremSet.get(tLCPremSet.size()).getPayIntv()),
//                                       "M", "");
//        LJAPayPersonSchema tLJAPayPersonSchema
//            = getOneLJAPayPersonInfo(tLPPolSchema,
//                                     tLCPremSet.get(tLCPremSet.size()),
//                                     polGetMoneyLeft);
//        mLJAPayPersonSet.add(tLJAPayPersonSchema);

        map.put(mLJAPayPersonSet, SysConst.INSERT);
        

        String sql = "update LJAPayPerson a "
                     + "set PayNo = "
                     + "   (select PayNo from LJAPay where IncomeNo = a.PayNo) "
                     + "where PolNo = '" + tLPPolSchema.getPolNo() + "' "
                     + "   and PayNo = '" + tLPPolSchema.getEdorNo() + "' ";
        map.put(sql, SysConst.INSERT);


        
        updatePayToDate("LCPol", mPolPayToDate, tLPPolSchema.getPolNo(),1);
        updatePayToDate("LCDuty", mPolPayToDate, tLPPolSchema.getPolNo(),1);
        //20150727add  by lzy 添加对缓交保单的处理
        dealPolState(tLPPolSchema,mPolPayToDate);
        
    }

    /**
     * 查询险种补退费
     * @param polNo String
     * @return double
     */
    private double getPolGetMoney(LPPolSchema cLPPolSchema)
    {
        String sql = "select sum(GetMoney) "
                     + "from LJSGetEndorse "
                     + "where EndorsementNo = '"
                     + cLPPolSchema.getEdorNo() + "' "
                     + "   and FeeOperationType = '"
                     + cLPPolSchema.getEdorType() + "' "
                     + "   and PolNo = '" + cLPPolSchema.getPolNo() + "' ";
        String sumMoney = new ExeSQL().getOneValue(sql);
        if("".equals(sumMoney) || "null".equals(sumMoney))
        {
            return 0;
        }

        return Double.parseDouble(sumMoney);
    }

    private LJAPayPersonSchema getOneLJAPayPersonInfo(LPPolSchema cLPPolSchema,
        LCPremSchema cLCPremSchema,
        double money)
    {
        LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();

        tLJAPayPersonSchema.setPolNo(cLCPremSchema.getPolNo());
        tLJAPayPersonSchema.setPayCount(cLCPremSchema.getPayTimes() + 1);
        tLJAPayPersonSchema.setGrpContNo(cLCPremSchema.getGrpContNo());
        tLJAPayPersonSchema.setGrpPolNo(cLPPolSchema.getGrpPolNo());
        tLJAPayPersonSchema.setContNo(cLCPremSchema.getContNo());
        tLJAPayPersonSchema.setManageCom(cLPPolSchema.getManageCom());
        tLJAPayPersonSchema.setRiskCode(cLPPolSchema.getRiskCode());
        tLJAPayPersonSchema.setAgentCode(cLPPolSchema.getAgentCode());
        tLJAPayPersonSchema.setAgentGroup(cLPPolSchema.getAgentGroup());
        tLJAPayPersonSchema.setAppntNo(cLCPremSchema.getAppntNo());
        tLJAPayPersonSchema.setPayNo(mLPEdorItemSchema.getEdorNo());
        tLJAPayPersonSchema.setPayAimClass("1");
        tLJAPayPersonSchema.setDutyCode(cLCPremSchema.getDutyCode());
        tLJAPayPersonSchema.setPayPlanCode(cLCPremSchema.getPayPlanCode());
        tLJAPayPersonSchema.setSumDuePayMoney(money);
        tLJAPayPersonSchema.setSumActuPayMoney(money);
        tLJAPayPersonSchema.setPayIntv(cLCPremSchema.getPayIntv());
        tLJAPayPersonSchema.setPayDate(mCurDate);
        tLJAPayPersonSchema.setPayType("ZC");
        tLJAPayPersonSchema.setEnterAccDate(mCurDate);
        tLJAPayPersonSchema.setConfDate(mCurDate);
        tLJAPayPersonSchema.setLastPayToDate(mLastPayToDate);
        tLJAPayPersonSchema.setCurPayToDate(mPolPayToDate);
        tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
        tLJAPayPersonSchema.setMakeDate(mCurDate);
        tLJAPayPersonSchema.setMakeTime(mCurTime);
        tLJAPayPersonSchema.setModifyDate(mCurDate);
        tLJAPayPersonSchema.setModifyTime(mCurTime);

        return tLJAPayPersonSchema;
    }
    
	private boolean dealPolState(LPPolSchema mLPPolSchema,String newPayToDate){
		String strSQL="select code from ldcode where codetype='"+BQ.STATETYPE_ULIDEFER+"' and code='"+mLPPolSchema.getRiskCode()+"'";
		String needDeal=new ExeSQL().getOneValue(strSQL);
		if(null==needDeal || "".equals(needDeal)){
			return true;
		}
		System.out.println("###进行缓交状态处理--");
		String mContNo=mLPPolSchema.getContNo();
		String strPol="select 1 from LCPol where contno='"+mContNo+"' "
					+ " and (standbyflag1 is not null and standbyflag1='"+BQ.ULI_DEFER+"')";
		String polstate=new ExeSQL().getOneValue(strPol);
		String strState="select * from LCContState where ContNo='"+mLPPolSchema.getContNo()+"'"
					+ " and StateType='"+BQ.STATETYPE_ULIDEFER+"' "
					+ " and state='1' ";
		LCContStateDB tLCContStateDB=new LCContStateDB();
		LCContStateSchema tLCContStateSchema=null;
		LCContStateSet tLCContStateSet=tLCContStateDB.executeQuery(strState);
		if(tLCContStateSet.size()==0){
			if(null==polstate || "".equals(polstate)){
				System.out.println(mLPPolSchema.getContNo()+"保单没有缓交记录");
				return true;
			}else{
				CError.buildErr(this, "保单缓交记录查询失败！");
				return false;
			}
		}
		for(int i=1; i<=tLCContStateSet.size(); i++){
			tLCContStateSchema=tLCContStateSet.get(i);
			//核销时还需校验保单新的交至日是否仍满期缓交条件
    	    if(null==newPayToDate || "".equals(newPayToDate)){
    	    	System.out.println("没有查询到险种"+tLCContStateSchema.getPolNo()+"的缴费记录");
    	    	continue;
    	    }
    	    //********添加第五保单年度后的处理
    	    int polYear=PubFun.calInterval(mLPPolSchema.getCValiDate(), newPayToDate, "Y");//续期后的保单年度
    	    String standByflag="null";
    	    
    	    FDate tFDate = new FDate();
    		String GraceDate=PubFun.calDate(newPayToDate, 60+1 , "D", "");
        	if(tFDate.getDate(GraceDate).before(tFDate.getDate(mCurDate))){
        		if(polYear<5){
        			System.out.println("险种新交至日仍需缓交");
	        		continue;
        		}
        		standByflag="1";//第五保单年度及以后只是缓交，对账户结算不影响
        	}
			tLCContStateSchema.setState("0");
			tLCContStateSchema.setEndDate(mCurDate);
			tLCContStateSchema.setModifyDate(mCurDate);
			tLCContStateSchema.setModifyTime(mCurTime);
			map.put(tLCContStateSchema, SysConst.DELETE_AND_INSERT);
			
			String update="update lcpol set standbyflag1="+standByflag+",modifydate=current date,modifytime=current time"
				+ " where contno='"+mContNo+"' ";
			map.put(update, SysConst.UPDATE);
		}
			
		return true;
	}

    public static void main(String args[])
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo("20121218000014");
        tLPEdorItemDB.setEdorType("FX");
        VData tVData = new VData();

        GlobalInput t = new GlobalInput();
        t.ManageCom = "86";
        t.Operator = "pa001";

        tVData.add(t);
        tVData.add(tLPEdorItemDB.query().get(1));

        PEdorFXConfirmBL test = new PEdorFXConfirmBL();
        if(!test.submitData(tVData, ""))
        {
            System.out.println(test.mErrors.getErrContent());
        }

    }
}
