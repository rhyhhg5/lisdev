package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全人工核保</p>
 * <p>Description: 体检通知书录入</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorUWManuRReportBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPRReportSchema mLPRReportSchema = null;

    private LPRReportItemSet mLPRReportItemSet = null;

    private String mEdorNo = null;

    private String mContNo = null;

    private String mAppntNo = null;

    private String mAppntName = null;

    private String mInsuredNo = null;

    private String mInsuredName = null;

    private String mPrtNo = null;

    private String mManageCom = null;

    private String mAgentCode = null;

    private String mAgentName = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回体检通知书号
     * @return String
     */
    public String getPrtNo()
    {
        return mPrtNo;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPRReportSchema = (LPRReportSchema) data.
                    getObjectByObjectName("LPRReportSchema", 0);
            mLPRReportItemSet = (LPRReportItemSet) data.
                    getObjectByObjectName("LPRReportItemSet", 0);
            mEdorNo = mLPRReportSchema.getEdorNo();
            mContNo = mLPRReportSchema.getContNo();
            LCContSchema tLCContSchema = CommonBL.getLCCont(mContNo);
            mAppntNo = tLCContSchema.getAppntNo();
            mManageCom = tLCContSchema.getManageCom();
            mAgentCode = tLCContSchema.getAgentCode();
            mAppntName = CommonBL.getAppntName(mAppntNo);
            mInsuredNo = mLPRReportSchema.getCustomerNo();
            mInsuredName = CommonBL.getInsuredName(mInsuredNo);
        }
        catch (Exception ex)
        {
            mErrors.addOneError("传入参数错误！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
        mPrtNo = createPrtNo();
        setLPRReport();
        setLPRReportItem();
        return true;
    }

    /**
     * 产生打印号
     * @return String
     */
    private String createPrtNo()
    {
        LPRReportDB tLPRReportDB = new LPRReportDB();
        tLPRReportDB.setEdorNo(mEdorNo);
        tLPRReportDB.setCustomerNo(mInsuredNo);
        LPRReportSet tLPRReportSet = tLPRReportDB.query();
        if (tLPRReportSet.size() != 0)
        {
            String prtNo = tLPRReportSet.get(1).getPrtSeq();
            if (prtNo != null)
            {
                return prtNo;
            }
        }
        return PubFun1.CreateMaxNo("PRTSEQNO","SN");
    }

    /**
     * 设置体检通知书主表
     */
    private void setLPRReport()
    {
        String sql = "delete from LPRReport " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and CustomerNo = '" + mInsuredNo + "' ";
        mMap.put(sql, "DELETE");
        mLPRReportSchema.setEdorNo(mEdorNo);
        mLPRReportSchema.setGrpContNo(BQ.GRPFILLDATA);
        mLPRReportSchema.setProposalContNo(mContNo);
        mLPRReportSchema.setName(mInsuredName);
        mLPRReportSchema.setPrtSeq(mPrtNo);
        mLPRReportSchema.setManageCom(mManageCom);
        mLPRReportSchema.setOperator(mGlobalInput.Operator);
        mLPRReportSchema.setMakeDate(mCurrentDate);
        mLPRReportSchema.setMakeTime(mCurrentTime);
        mLPRReportSchema.setModifyDate(mCurrentDate);
        mLPRReportSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPRReportSchema, "DELETE&INSERT");
    }

    /**
     * 设置体检项目表
     */
    private void setLPRReportItem()
    {
        String sql = "delete from LPRReportItem " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and PrtSeq = '" + mPrtNo + "' ";
        mMap.put(sql, "DELETE");
        for (int i = 1; i <= mLPRReportItemSet.size(); i++)
        {
            LPRReportItemSchema tLPRReportItemSchema =
                    mLPRReportItemSet.get(i);
            tLPRReportItemSchema.setEdorNo(mEdorNo);
            tLPRReportItemSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLPRReportItemSchema.setContNo(mContNo);
            tLPRReportItemSchema.setProposalContNo(mContNo);
            tLPRReportItemSchema.setPrtSeq(mPrtNo);
            tLPRReportItemSchema.setModifyDate(mCurrentDate);
            tLPRReportItemSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPRReportItemSchema, "DELETE&INSERT");
        }
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
