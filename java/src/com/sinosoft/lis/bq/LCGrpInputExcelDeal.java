package com.sinosoft.lis.bq;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi35.hssf.usermodel.HSSFRow;
import org.apache.poi35.hssf.usermodel.HSSFSheet;
import org.apache.poi35.hssf.usermodel.HSSFWorkbook;
import org.apache.poi35.ss.usermodel.Cell;
import org.apache.poi35.ss.usermodel.Sheet;
import org.apache.poi35.ss.usermodel.Workbook;
import org.apache.poi35.xssf.usermodel.XSSFRow;
import org.apache.poi35.xssf.usermodel.XSSFSheet;
import org.apache.poi35.xssf.usermodel.XSSFWorkbook;

import com.sinosoft.lis.schema.LCGrpSubInsuredImportSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.TransferData;

/**
 * 大团单excel处理类
 * 
 */
public class LCGrpInputExcelDeal {

	private InputStream is = null;

	private HSSFWorkbook wb = null;

	private HSSFSheet hs = null;

	private TransferData mTransferData;

	/**
	 * 构造函数，初始化流信息
	 * 
	 * @param path
	 * @param tTransferData
	 */
	public LCGrpInputExcelDeal(String path, TransferData tTransferData) {

		this.mTransferData = tTransferData;

		try {
			is = new FileInputStream(path);
			if(path.endsWith("xlsx")){
				//2007
				wb = new HSSFWorkbook(is);
			}else if(path.endsWith("xls")){
				//2003
				wb = new HSSFWorkbook(is);
			}
			// 获取文件XSSFSheet对象
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(wb != null){
				try {
					hs = wb.getSheetAt(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			wb = null;
		}
	}

	/**
	 * 获取excel信息
	 * 
	 * @param data Schema集合对象
	 * @param startLine 起始行数
	 * @param size 获取数量
	 * @return
	 */
	public int getListData(List<Schema> data, int startLine, int size) {

		try {

			if (hs == null) {
				return -1;
			}

			int rownum = startLine;
			int endLine = startLine + size - 1;

			// 不读第一行
			for (; rownum <= hs.getLastRowNum() && rownum <= endLine && rownum > 0; rownum++) {
				HSSFRow hRow = hs.getRow(rownum);
				Cell cell = hRow.getCell(0);
				
				if(cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
					// 获取对应行的Schema
					LCInsuredListSchema lgrpSchema = LCGrpInputSchemaDeal.setScheml(hRow, rownum, mTransferData);

					data.add(lgrpSchema);
                }else{
                	return -2;
                }

				
				
			}

			return rownum;

		} catch (Exception e) {

			e.printStackTrace();
			return -1;
		}
	}
}
