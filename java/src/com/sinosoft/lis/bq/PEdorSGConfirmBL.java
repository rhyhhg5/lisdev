package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PEdorSGConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    
    //刘鑫新增
    private String mEdorNo = null;

	private String mEdorType = null;

	private String mGrpContNo = null;
 
	private String mEdorValidate = null;
	
	private String mModifyDate = null;
	
	
    /** 业务对象 */
    private LCPolBL mLCPolBL = new LCPolBL();
    private LCPolBLSet mLCPolBLSet = new LCPolBLSet();
    private LPPolBL mLPPolBL = new LPPolBL();
    private LPPolBLSet mLPPolBLSet = new LPPolBLSet();
    private LPInsuredBLSet mLPInsuredBLSet = new LPInsuredBLSet();
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private ValidateEdorData2 mValidateEdorData = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();

    public PEdorSGConfirmBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public void setOperate(String cOperate)
    {
        this.mOperate = cOperate;
    }

    public String getOperate()
    {
        return this.mOperate;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.setOperate(cOperate);

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);
        System.out.println("after get input data....");
        
        /*if (!checkData()) {
        	mErrors.addOneError("试算日期和保全确认不是同一天,需重新试算!");
			return false;
		}*/
        //数据准备操作
        if (!prepareData())
            return false;
        System.out.println("after prepareData data....");

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
    
    /**
	 * 判断保全生效日期和试算日期是否为同一天，
	 * @return
	 */
	private boolean checkData() {

		if (mModifyDate.equals("")) {
			 CError.buildErr(this, "取到的试算日期为空！");
			return false;
		}
		String sqlli3 = "SELECT date('" + mModifyDate + "')-date('"
		+ PubFun.getCurrentDate() + "') FROM dual where 1 =1 ";
		System.out.println(sqlli3+"#########");
		ExeSQL exesql = new ExeSQL();
		SSRS InsuRSSRSli3 = new SSRS();
		InsuRSSRSli3 = exesql.execSQL(sqlli3);
		if (InsuRSSRSli3 != null && InsuRSSRSli3.getMaxRow() > 0
				&& (Integer.parseInt(InsuRSSRSli3.GetText(1, 1)) - 0) != 0) {
			return false;
		}

		return true;
	}
    
    
    /**
     * 从输入数据中得到所有对象
     * @param cInputData 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //刘鑫新增
        
        mEdorNo = mLPEdorItemSchema.getEdorNo();
		mEdorType = mLPEdorItemSchema.getEdorType();
		mGrpContNo = mLPEdorItemSchema.getGrpContNo();
		LPEdorItemDB mLPEdorItemDB = new LPEdorItemDB();
		mLPEdorItemDB.setEdorNo(mEdorNo);
		mLPEdorItemDB.setEdorType(mEdorType);
		mLPEdorItemDB.setGrpContNo(mGrpContNo);
		mModifyDate = mLPEdorItemDB.query().get(1).getModifyDate();
		
		System.out.println("mModifyDate1111111111"+mModifyDate);
		mEdorValidate = mLPEdorItemSchema.getEdorValiDate();//取出试算的生效日期
        
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mValidateEdorData = new ValidateEdorData2(mGlobalInput, mLPEdorItemSchema.getEdorNo(),
        		mLPEdorItemSchema.getEdorType(), mLPEdorItemSchema.getContNo(), "ContNo");
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
//	    在公共类中转移团单的数据
        String[] addTables = {"LCInsureAccTrace","LCInsureAccFeeTrace","LCInsureAccBalance"};
        mValidateEdorData.addData(addTables);
        String[] chgTables = {"LCInsureAcc","LCInsureAccFee","LCInsureAccClass","LCInsureAccClassFee"};
        mValidateEdorData.changeData(chgTables);
        map.add(mValidateEdorData.getMap());
    	
        String aPolNo = mLPEdorItemSchema.getPolNo();
        String aEdorNo = mLPEdorItemSchema.getEdorNo();
        LCPolDB tLCPolDB = new LCPolDB();

        ContCancel tContCancel = new ContCancel();
        tContCancel.setEdorType(mLPEdorItemSchema.getEdorType());
        if (mLPEdorItemSchema.getPolNo().equals("000000") &&
            mLPEdorItemSchema.getInsuredNo().equals("000000")) //合同退保
        {
            //处理医疗险的减人
            tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());

            MMap tMMap = tContCancel.prepareContData(
                mLPEdorItemSchema.getContNo(),
                aEdorNo);
            if(tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备个人保单数据失败！");
                return false;
            }
            map.add(tMMap);
        }
        else if (mLPEdorItemSchema.getPolNo().equals("000000")) //被保险人退保
        {
            tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
            tLCPolDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());

            MMap tMMap = tContCancel.prepareInsuredData(
                mLPEdorItemSchema.getContNo(),
                mLPEdorItemSchema.getInsuredNo(),
                aEdorNo);
            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备被保险人数据失败！");
                return false;
            }
            map.add(tMMap);

        }
        else
        {
            tLCPolDB.setPolNo(aPolNo);

            MMap tMMap = new MMap();
            tMMap = tContCancel.preparePolData(aPolNo, aEdorNo);
            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备个人险种数据失败！");
                return false;
            }
            map.add(tMMap);
        }

        mInputData.clear();
        System.out.println("\n\nend 撤单数据准备");

        String tInsuNo=new ExeSQL().getOneValue("select insuredno from lcpol where contno='"+mLPEdorItemSchema.getContNo()+"'");
        String tPersonUpdateSQL=" update ldperson set deathdate='"+mLPEdorItemSchema.getEdorValiDate()+"' where customerno='"+tInsuNo+"' ";
        map.put(tPersonUpdateSQL, "UPDATE");

        mLPEdorItemSchema.setEdorState("0");
        mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(mLPEdorItemSchema, "UPDATE");

        mResult.clear();
        mResult.add(map);

        return true;
    }
	public static void main(String[] args) {
		String mModifyDate="2011-04-21";
		
		if (mModifyDate.equals("") || mModifyDate == null) {

		System.out.print("取到的试算生效日期为空");

		}
		String sqlli3 = "SELECT date('" + mModifyDate + "')-date('"
				+ PubFun.getCurrentDate() + "') FROM dual where  1=1 ";
		ExeSQL exesql = new ExeSQL();
		SSRS InsuRSSRSli3 = new SSRS();
		InsuRSSRSli3 = exesql.execSQL(sqlli3);
		if (InsuRSSRSli3 != null && InsuRSSRSli3.getMaxRow() > 0
				&& (Integer.parseInt(InsuRSSRSli3.GetText(1, 1)) - 0) != 0) {
			//mErrors.addOneError("取到的试算生效日期为空");
			System.out.print("算日期和保全确认不是同一天,需重新试算");
		}
		
	}
}
