package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 预收保费 </p>
 * <p>Description: 保全确认</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorYSConfirmBL implements EdorConfirm
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    private AppAcc mAppAcc = new AppAcc();

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        getInputData(data);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private void getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                "GlobalInput", 0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) data.getObjectByObjectName(
                "LPGrpEdorItemSchema", 0);
        mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
        mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
        mEdorType = mLPGrpEdorItemSchema.getEdorType();
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!validateEdorData())
        {
            return false;
        }
        return true;
    }

    /**
     * 使保全数据生效
     */
    private boolean validateEdorData()
    {
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpAppntDB.getInfo())
        {
            mErrors.addOneError("未找到投保客户信息！");
            return false;
        }
        String customerNo = tLCGrpAppntDB.getCustomerNo();
        MMap map = mAppAcc.accConfirm(customerNo, mEdorNo, BQ.NOTICETYPE_G);
        mMap.add(map);
        return true;
    }
}
