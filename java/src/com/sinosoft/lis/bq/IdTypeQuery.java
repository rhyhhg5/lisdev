package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

public class IdTypeQuery
{
    private Map mIdTypeMap = new HashMap();

    public IdTypeQuery()
    {
        LDBankSet tLDBankSet = new LDBankDB().query();
        for (int i = 1; i < tLDBankSet.size(); i++)
        {
            LDBankSchema tLDBankSchema = tLDBankSet.get(i);
            mIdTypeMap.put(tLDBankSchema.getBankCode(), tLDBankSchema.getBankName());
        }
    }

    public String getIdTypeName(String idType)
    {
        System.out.println("IdType:" + idType);
        if ((idType == null) || (idType.equals("")))
        {
            return "";
        }
        String idTypeName = (String) mIdTypeMap.get(idType);
        if (idTypeName == null)
        {
            return "";
        }
        return idTypeName;
    }
}
