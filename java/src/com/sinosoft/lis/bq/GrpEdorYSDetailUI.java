package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 预收保费</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author  Lanjun
 * @version 1.0
 */

public class GrpEdorYSDetailUI
{
    private GEdorYSDetailBL mGEdorYSDetailBL = null;

    public GrpEdorYSDetailUI()
    {
        mGEdorYSDetailBL = new GEdorYSDetailBL();
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data, String operator)
    {
        if (!mGEdorYSDetailBL.submitData(data, operator))
        {
            return false;
        }
        return true;
    }

    /**
     * 得到错误
     * @return String
     */
    public String getError()
    {
        return mGEdorYSDetailBL.mErrors.getFirstError();
    }
}
