package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体保全集体下个人功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */
public class GEdorZTDetailBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    private LPInsuredSet mLPInsuredSet = null;
    private LPDiskImportSet mLPDiskImportSet = null;
    private LPPolSet mLPPolSet = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private Reflections ref = new Reflections();
    private String currDate = PubFun.getCurrentDate();
    private String currTime = PubFun.getCurrentTime();
    private MMap map = new MMap();

    private long mLPDiskImportSerialNo = 0;

    public GEdorZTDetailBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("---Start of GEdorZTDetail->submitData---");
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据
        if (!getInputData())
        {
            return false;
        }
        //数据校验操作
        if (!checkData())
        {
            return false;
        }
        //数据准备操作
        if (mOperate.equals("INSERT||EDOR"))
        {
            if (!prepareData())
            {
                return false;
            }
            System.out.println("---End prepareData---");
        }
        //数据准备操作
        if (mOperate.equals("DELETE||EDOR"))
        {
            if (!deleteDataZT())
            {
                return false;
            }
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("GedorDetailBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- GEdorZTDetail->submitData--- 数据提交成功。");

        return true;
    }

    /**
     * 减人撤销个人申请
     * @return boolean
     */
    private boolean deleteDataZT()
    {
        System.out.println("Beginning of GEdorZTDetailBL->deleteDataZT");
        if (!getLPInsuredInfo())
        {
            return false;
        }

        String contNos = "";
        for (int i = 1; i <= mLPEdorItemSet.size(); i++)
        {
            LPInsuredDB tLPInsuredDB = new LPInsuredDB();
            tLPInsuredDB.setEdorNo(mLPEdorItemSet.get(i).getEdorNo());
            tLPInsuredDB.setEdorType(mLPEdorItemSet.get(i).getEdorType());
            tLPInsuredDB.setContNo(mLPEdorItemSet.get(i).getContNo());
            tLPInsuredDB.setInsuredNo(mLPEdorItemSet.get(i).getInsuredNo());
            LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();

            if (needDeleteLPEdorItem(tLPInsuredSet))
            {
                String sql = "  delete from LPEdorItem "
                        + "where edorNo = '"
                        + mLPEdorItemSet.get(i).getEdorNo() + "' "
                        + "    and edorType = '"
                        + mLPGrpEdorItemSchema.getEdorType() + "' "
                        + "    and contNo = '"
                        + mLPEdorItemSet.get(i).getContNo() + "' ";
                map.put(sql, "DELETE");
            }
            else
            {
                //将insuredNo赋值为非000000
            }

            //删除险种信息
            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setEdorNo(mLPEdorItemSet.get(i).getEdorNo());
            tLPPolDB.setEdorType(mLPEdorItemSet.get(i).getEdorType());
            tLPPolDB.setContNo(mLPEdorItemSet.get(i).getContNo());
            tLPPolDB.setInsuredNo(mLPEdorItemSet.get(i).getInsuredNo());
            LPPolSet tLPPolSet = tLPPolDB.query();
            map.put(tLPPolSet, "DELETE");

            contNos += "'" + mLPEdorItemSet.get(i).getContNo() + "',";
        }
        contNos = contNos.substring(0, contNos.length() - 1);

        String sql = "  delete from LPEdorMain a "
                + "where a.edorNo = '"
                + mLPGrpEdorItemSchema.getEdorNo() + "' "
                + "    and a.contNo in (" + contNos + ") "
                + "    and 0 = ("
                + "        select count(edorNo) from LPEdorItem "
                + "        where edorNo = '"
                + mLPGrpEdorItemSchema.getEdorNo() + "' "
                + "            and contNo = a.contNo) ";
        map.put(sql, "DELETE");

        System.out.println("mLPInsuredSet.size() = " + mLPInsuredSet.size());
        map.put(mLPInsuredSet, "DELETE");

        for (int i = 1; i <= mLPInsuredSet.size(); i++)
        {
            String deleteSql = "  delete from LPDiskImport "
                    + "where edorNo = '"
                    + mLPInsuredSet.get(i).getEdorNo()
                    + "'  and edorType = '"
                    + mLPInsuredSet.get(i).getEdorType()
                    + "'  and grpContNo = '"
                    + mLPInsuredSet.get(i).getGrpContNo()
                    + "'  and insuredNo = '"
                    + mLPInsuredSet.get(i).getInsuredNo() + "' ";
            Object o = map.get(deleteSql);
            if (o == null)
            {
                map.put(deleteSql, "DELETE");
                System.out.println(deleteSql);
            }
        }
        //--------------start----------------
        // add by chenlei 2008-4-24 撤销个人保全时，修改保全项目的状态为"未录入"，需要重新点击"保全申请"，风险比较小。
        String mSql=" update LPGrpEdorItem set edorstate='3' where edortype='"+mLPGrpEdorItemSchema.getEdorType()+"'  "
                      +" and edorno ='"+mLPGrpEdorItemSchema.getEdorNo()+"'"
                      +" and grpcontno='"+mLPGrpEdorItemSchema.getGrpContNo()+"'";
       //加上此条件后，就会判断在P表没有要减的人时候才修改保全状态，担心可能存在风险  
       //             +" and not exists (select 'x' from lpinsured where edorno='"+mLPGrpEdorItemSchema.getEdorNo()+"' and edortype='"+mLPGrpEdorItemSchema.getEdorType()+"')";
        map.put(mSql,"UPDATE");
        
       //-----------end------------------- 
        mResult.clear();
        mResult.add(map);

        return true;
    }

    private boolean needDeleteLPEdorItem(LPInsuredSet tLPInsuredSet)
    {
        //若删除了原来选择的所有被保人，则需要删除个人申请
        int insuredCountInLPInsured = tLPInsuredSet.size();
        int insuredCountSelect = 0;

        for (int i = 1; i <= mLPInsuredSet.size(); i++)
        {
            //若选择的被保人不是主被保人，则需要校验其主被保人是否仍需要减去
            //若主被保人仍需要减去，则其连带被保人不能撤销申请
            if (!mLPInsuredSet.get(i).getRelationToMainInsured()
                    .equals(BQ.MAININSURED))
            {
                //处理连带被保人的删除
                System.out.println("需要增加处理连带被保人的删除");
            }

            for (int t = 1; t <= tLPInsuredSet.size(); t++)
            {
                System.out.println(tLPInsuredSet.get(t).getContNo() + " "
                        + mLPInsuredSet.get(i).getContNo() + " "
                        + tLPInsuredSet.get(t).getInsuredNo() + " "
                        + mLPInsuredSet.get(i).getInsuredNo());
                //保单号与被保人号都相等即为同一被保人
                if (tLPInsuredSet.get(t).getContNo()
                        .equals(mLPInsuredSet.get(i).getContNo())
                        && tLPInsuredSet.get(t).getInsuredNo()
                        .equals(mLPInsuredSet.get(i).getInsuredNo()))
                {
                    insuredCountSelect++;
                }
            }
        }
        if (insuredCountInLPInsured == insuredCountSelect)
        {
            return true;
        }

        return false;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        mLPEdorItemSet = (LPEdorItemSet) mInputData
                .getObjectByObjectName("LPEdorItemSet", 0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) mInputData
                .getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mLPInsuredSet = (LPInsuredSet) mInputData
                .getObjectByObjectName("LPInsuredSet", 0);
        mLPDiskImportSet = (LPDiskImportSet) mInputData
                .getObjectByObjectName("LPDiskImportSet", 0);
        mGlobalInput = (GlobalInput) mInputData
                .getObjectByObjectName("GlobalInput", 0);
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
       /*
        *解决保全全表查询LPGRPEDORITEM 【OoO?】杨天政 20120510
        */
        if (mLPGrpEdorItemSchema.getEdorNo()==null
        		||mLPGrpEdorItemSchema.getEdorNo().equals("")
        		||mLPGrpEdorItemSchema.getEdorNo().equals("null"))
    	{
        	mErrors.addOneError("保全项目号缺失，请重新操作");
            return false;
    	
    	}
        
        if (mLPGrpEdorItemSchema.getEdorType()==null
        		||mLPGrpEdorItemSchema.getEdorType().equals("")
        		||mLPGrpEdorItemSchema.getEdorType().equals("null"))
    	{
        	mErrors.addOneError("保全项目类型信息缺失，请重新操作");
            return false;
    	
    	}
        
        if (mLPGrpEdorItemSchema.getGrpContNo()==null
        		||mLPGrpEdorItemSchema.getGrpContNo().equals("")
        		||mLPGrpEdorItemSchema.getGrpContNo().equals("null"))
    	{
        	mErrors.addOneError("团体保单号信息缺失，请重新操作");
            return false;
    	
    	}
        
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            mErrors.addOneError("未找到保全项目信息！");
            return false;
        }
        mLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
        return true;
    }

    /**
     * 校验传入的数据的合法性
     * @return
     */
    private boolean checkData()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setSchema(mLPGrpEdorItemSchema);
        if (!tLPGrpEdorItemDB.getInfo())
        {
            System.out.println("GEdorDetailBL无保全申请数据");
            this.mErrors.addOneError("无保全申请数据");
            return false;
        }

        mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemDB.getSchema());
        if (tLPGrpEdorItemDB.getEdorState().trim().equals(BQ.EDORSTATE_CAL))
        {
            System.out.println("GEdorDetailBL该保全已经保全理算不能修改!");
            this.mErrors.addOneError("该保全已经保全理算不能修改!");
            return false;
        }

        return true;
    }

    /**
     * 减人：若选择了主被保人，则连带被保人也一起选择
     * @return boolean
     */
    private boolean dealMainInsured()
    {
        if (mLPInsuredSet == null)
        {
            mErrors.addOneError("请选择被保人");
            return false;
        }

        //得到完整的被保人信息
        if (!getLPInsuredInfo())
        {
            return false;
        }

        return true;
    }

    /**
     * 得到完整的被保人信息
     * @return boolean
     */
    private boolean getLPInsuredInfo()
    {
        LPInsuredSet tLPInsuredSet = new LPInsuredSet(); //处理后的被保人信息
        for (int i = 1; i <= mLPInsuredSet.size(); i++)
        {
            LCInsuredDB aLCInsuredDB = new LCInsuredDB();
            aLCInsuredDB.setContNo(mLPInsuredSet.get(i).getContNo());
            aLCInsuredDB.setInsuredNo(mLPInsuredSet.get(i).getInsuredNo());
            if (!aLCInsuredDB.getInfo())
            {
                mErrors.addOneError("查询被保人" + aLCInsuredDB.getInsuredNo()
                        + "出错。");
                return false;
            }

            ref.transFields(mLPInsuredSet.get(i), aLCInsuredDB.getSchema());
            mLPInsuredSet.get(i).setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
            mLPInsuredSet.get(i).setEdorType(mLPGrpEdorItemSchema.getEdorType());
            mLPInsuredSet.get(i).setOperator(mGlobalInput.Operator);
            mLPInsuredSet.get(i).setModifyDate(currDate);
            mLPInsuredSet.get(i).setModifyTime(currTime);
            mLPInsuredSet.get(i).setMakeDate(currDate);
            mLPInsuredSet.get(i).setMakeTime(currTime);

            tLPInsuredSet.add(mLPInsuredSet.get(i).getSchema());

            //校验是否是主被保人
            if (!mLPInsuredSet.get(i).getRelationToMainInsured()
                    .equals(BQ.MAININSURED))
            {
                continue;
            }

            //若选择了主被保人，则减去所有个单下被保人
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setContNo(mLPInsuredSet.get(i).getContNo());
            LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
            for (int temp = 1; temp <= tLCInsuredSet.size(); temp++)
            {
                //同时减去连带被保人
                if ((tLCInsuredSet.get(temp).getInsuredNo())
                        .equals(mLPInsuredSet.get(i).getInsuredNo()))
                {
                    continue;
                }
                boolean sameFlag = false;
                for (int j = 1; j <= mLPInsuredSet.size(); j++)
                {
                    if ((tLCInsuredSet.get(temp).getInsuredNo()).
                            equals(mLPInsuredSet.get(j).getInsuredNo()))
                    {
                        sameFlag = true;
                        break;
                    }
                }
                if (sameFlag)
                {
                    continue;
                }
                LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                tLPEdorItemSchema.setEdorAcceptNo(mLPGrpEdorItemSchema.
                        getEdorAcceptNo());
                tLPEdorItemSchema.setEdorNo(mLPInsuredSet.get(i).getEdorNo());
                tLPEdorItemSchema.setContNo(tLCInsuredSet.get(temp).getContNo());
                tLPEdorItemSchema.setInsuredNo(tLCInsuredSet.get(temp).
                        getInsuredNo());
                mLPEdorItemSet.add(tLPEdorItemSchema);

                LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
                ref.transFields(tLPInsuredSchema, tLCInsuredSet.get(temp));
                tLPInsuredSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
                tLPInsuredSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
                tLPInsuredSchema.setOperator(mGlobalInput.Operator);
                tLPInsuredSchema.setModifyDate(currDate);
                tLPInsuredSchema.setModifyTime(currTime);
                tLPInsuredSchema.setMakeDate(currDate);
                tLPInsuredSchema.setMakeTime(currTime);
//                mLPInsuredSet.add(tLPInsuredSchema);

                tLPInsuredSet.add(tLPInsuredSchema);

                if (mLPDiskImportSet != null)
                {
                    createOneLPDiskImportInfo(tLPInsuredSchema);
                }
            }
        }

        mLPInsuredSet.clear();
        mLPInsuredSet.add(tLPInsuredSet); //这样可以避免同一个单有多个主被保人时死循环的情况

        //非磁盘导入方式的减人
        if (this.mLPDiskImportSet == null)
        {
            mLPDiskImportSet = new LPDiskImportSet();
            for (int i = 1; i <= mLPInsuredSet.size(); i++)
            {
                createOneLPDiskImportInfo(mLPInsuredSet.get(i));
            }
        }
        return true;
    }

    private void createOneLPDiskImportInfo(LPInsuredSchema tLPInsuredSchema)
    {
        LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
        ref.transFields(tLPDiskImportSchema, tLPInsuredSchema);
        tLPDiskImportSchema.setState("1");
        tLPDiskImportSchema.setSerialNo(
                String.valueOf(mLPDiskImportSerialNo++));
        tLPDiskImportSchema.setInsuredName(tLPInsuredSchema.getName());
        tLPDiskImportSchema.setEdorValiDate(mLPGrpEdorItemSchema.getEdorValiDate());
        PubFun.fillDefaultField(tLPDiskImportSchema);
        this.mLPDiskImportSet.add(tLPDiskImportSchema);
    }

    //得到本项目导入被保人表的最大序列号
    private long getLPDiskImportSerialNo()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select max(int(SerialNo)) ")
                .append("from LPDiskImport ")
                .append("where edorNo = '")
                .append(mLPGrpEdorItemSchema.getEdorNo())
                .append("'  and edorType = '")
                .append(mLPGrpEdorItemSchema.getEdorType())
                .append("'  and grpContNo = '")
                .append(mLPGrpEdorItemSchema.getGrpContNo())
                .append("' ");
        ExeSQL e = new ExeSQL();
        String maxSerialNo = e.getOneValue(sql.toString());
        if (maxSerialNo.equals("") || maxSerialNo.equals("null"))
        {
            mLPDiskImportSerialNo = 1;
        }
        else
        {
            mLPDiskImportSerialNo = Long.parseLong(maxSerialNo) + 1;
        }

        mLPDiskImportSerialNo += (mLPDiskImportSet == null ? 0
                : mLPDiskImportSet.size());
        return mLPDiskImportSerialNo;
    }

    /**
     * 处理整单退保
     */
    private LPEdorItemSchema dealContZT(LPEdorItemSchema tLPEdorItemSchema)
    {
        int selectInsuredCount = 0;
        LPEdorItemSchema pLPEdorItemSchema = tLPEdorItemSchema.getSchema();

        //该团单下的所有被保人
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(pLPEdorItemSchema.getContNo());
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        int insuredCount = tLCInsuredSet.size();

        for (int i = 1; i <= tLCInsuredSet.size(); i++)
        {
            for (int t = 1; t <= mLPInsuredSet.size(); t++)
            {
                if (tLCInsuredSet.get(i).getInsuredNo()
                        .equals(mLPInsuredSet.get(t).getInsuredNo()))
                {
                    selectInsuredCount++;
                }
            }
        }

        //如果是主被保险人,则将所有的连带被保险人生效日期设置为主被保险人的生效日期.
        tLCInsuredDB.setInsuredNo(pLPEdorItemSchema.getInsuredNo());
        LCInsuredSet pLCInsuredSet = tLCInsuredDB.query();
        if(pLCInsuredSet.size()==1){
            if(pLCInsuredSet.get(1).getRelationToMainInsured().equals("00")){
//                System.out.println(pLPEdorItemSchema.getEdorValiDate()+pLPEdorItemSchema.getContNo()+"huxl");
                for (int i = 1; i <= mLPEdorItemSet.size(); i++) {
                    System.out.println(mLPEdorItemSet.get(i).getContNo()+i);
                    if(mLPEdorItemSet.get(i).getContNo().equals(pLPEdorItemSchema.getContNo())){
                        mLPEdorItemSet.get(i).setEdorValiDate(pLPEdorItemSchema.getEdorValiDate());
                    }
                }
            }
        }

        //所有被保人都选择即为整单退保
        if (insuredCount == selectInsuredCount)
        {
            pLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
        }

        return pLPEdorItemSchema;
    }

    /**
     * 准备需要保存的数据
     * @return
     */
    private boolean prepareData()
    {
        if (!dealMainInsured())
        {
            return false;
        }
        if (!dealLPPol())
        {
            return false;
        }

        //按个人保全主表进行处理
        //注意：若保单有连带被保人的话，入库时只生成条LPEdorItem记录，
        //因为会使用DELETE&INSERT会把重复的数据删掉
        //这样的好处是在确认是比较容易确定是否整单退保
        try
        {
            for (int i = 1; i <= mLPEdorItemSet.size(); i++)
            {
                LPEdorItemSchema tLPEdorItemSchema = mLPEdorItemSet.get(i);
                String edorValiDate = tLPEdorItemSchema.getEdorValiDate();

                tLPEdorItemSchema = dealContZT(tLPEdorItemSchema);

                ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);
                tLPEdorItemSchema.setPolNo("000000");
                tLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT); //未录入
                if (edorValiDate != null)
                {
                    tLPEdorItemSchema.setEdorValiDate(edorValiDate);
                }
                tLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
                tLPEdorItemSchema.setOperator(mGlobalInput.Operator);
                tLPEdorItemSchema.setUWFlag("0");
                tLPEdorItemSchema.setMakeDate(currDate);
                tLPEdorItemSchema.setMakeTime(currTime);
                tLPEdorItemSchema.setModifyDate(currDate);
                tLPEdorItemSchema.setModifyTime(currTime);
                map.put(tLPEdorItemSchema, "DELETE&INSERT");

                LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
                tLPEdorMainDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
                tLPEdorMainDB.setContNo(tLPEdorItemSchema.getContNo());
                LPEdorMainSet tLPEdorMainSet = new LPEdorMainSet();
                tLPEdorMainSet = tLPEdorMainDB.query();
                if (tLPEdorMainDB.mErrors.needDealError())
                {
                    CError.buildErr(this, "查询个人保全主表失败!");
                    return false;
                }
                if (tLPEdorMainSet.size() == 0)
                {
                    LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
                    ref.transFields(tLPEdorMainSchema, tLPEdorItemSchema);
                    tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
                    tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
                    tLPEdorMainSchema.setUWState("0");
                    tLPEdorMainSchema.setEdorState(BQ.EDORSTATE_INPUT);
                    tLPEdorMainSchema.setMakeDate(currDate);
                    tLPEdorMainSchema.setMakeTime(currTime);
                    tLPEdorMainSchema.setModifyDate(currDate);
                    tLPEdorMainSchema.setModifyTime(currTime);
                    map.put(tLPEdorMainSchema, "DELETE&INSERT");
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        //由于对连带被保人的处理，可能会有重复的记录，使用schema "DELETE&INSERT"的方式可以保证正确入库
        for (int i = 1; mLPInsuredSet != null && i <= mLPInsuredSet.size(); i++)
        {
            map.put(mLPInsuredSet.get(i).getSchema(), "DELETE&INSERT");
        }

        getLPDiskImportSerialNo();
        for (int i = 1; mLPDiskImportSet != null && i <= mLPDiskImportSet.size();
                i++)
        {
            mLPDiskImportSet.get(i).setSerialNo(
                    String.valueOf(mLPDiskImportSerialNo++));
            map.put("delete from LPDiskImport where edorNo = '"
                    + mLPDiskImportSet.get(i).getEdorNo()
                    + "'  and edorType = '"
                    + mLPDiskImportSet.get(i).getEdorType()
                    + "'  and grpContNo = '"
                    + mLPDiskImportSet.get(i).getGrpContNo()
                    + "'  and insuredNo = '"
                    + mLPDiskImportSet.get(i).getInsuredNo() + "' ", "DELETE");
            map.put(mLPDiskImportSet.get(i).getSchema(), "INSERT");
        }
        for (int i = 1; mLPPolSet != null && i <= mLPPolSet.size(); i++)
        {
            map.put(mLPPolSet.get(i).getSchema(), "DELETE&INSERT");
        }
//        map.put(mLPInsuredSet, "DELETE&INSERT");
//        map.put(mLPDiskImportSet, "DELETE&INSERT");
//        map.put(mLPPolSet, "DELETE&INSERT");

        mResult.clear();
        mResult.add(map);

        return true;
    }

    /**
     * 生成退保险种的信息
     * @return boolean
     */
    private boolean dealLPPol()
    {
        mLPPolSet = new LPPolSet();
        for (int i = 1; i <= mLPInsuredSet.size(); i++)
        {
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setContNo(mLPInsuredSet.get(i).getContNo());
            tLCPolDB.setInsuredNo(mLPInsuredSet.get(i).getInsuredNo());
            LCPolSet tLCPolSet = tLCPolDB.query();
            for (int temp = 1; temp <= tLCPolSet.size(); temp++)
            {
                if (tLCPolSet.get(temp).equals("0400&&&&"))
                {
                    mErrors.addOneError(
                            "被保人" + tLCPolSet.get(temp).getInsuredName() + "("
                            + tLCPolSet.get(temp).getInsuredNo() +
                            ")正在理赔，不能进行减人");
                    return false;
                }

                LPPolSchema tLPPolSchema = new LPPolSchema(); ;
                ref.transFields(tLPPolSchema, tLCPolSet.get(temp));
                tLPPolSchema.setEdorNo(mLPInsuredSet.get(i).getEdorNo());
                tLPPolSchema.setEdorType(mLPInsuredSet.get(i).getEdorType());
                tLPPolSchema.setOperator(mGlobalInput.Operator);
                tLPPolSchema.setModifyDate(currDate);
                tLPPolSchema.setModifyTime(currTime);
                tLPPolSchema.setMakeDate(currDate);
                tLPPolSchema.setMakeTime(currTime);

                mLPPolSet.add(tLPPolSchema);
            }
        }

        return true;
    }

    public static void main(String[] a)
    {
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();

        tLPGrpEdorItemSchema.setEdorAcceptNo("20060414000003");
        tLPGrpEdorItemSchema.setGrpContNo("0000115501");
        tLPGrpEdorItemSchema.setEdorNo(tLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPGrpEdorItemSchema.setEdorType("ZT");

        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        //1
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorAcceptNo(tLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemSchema.setEdorNo(tLPGrpEdorItemSchema.getEdorNo());
        tLPEdorItemSchema.setContNo("2300120153");
        tLPEdorItemSchema.setInsuredNo("000120258");
        tLPEdorItemSet.add(tLPEdorItemSchema);

        LPInsuredSet tLPInsuredSet = new LPInsuredSet();
        LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
        tLPInsuredSchema.setContNo(tLPEdorItemSchema.getContNo());
        tLPInsuredSchema.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
        tLPInsuredSet.add(tLPInsuredSchema);

        //2
        LPEdorItemSchema tLPEdorItemSchema2 = new LPEdorItemSchema();
        tLPEdorItemSchema2.setEdorAcceptNo(tLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemSchema2.setEdorNo(tLPGrpEdorItemSchema.getEdorNo());
        tLPEdorItemSchema2.setContNo("2300120153");
        tLPEdorItemSchema2.setInsuredNo("000120259");
        tLPEdorItemSet.add(tLPEdorItemSchema2);

        LPInsuredSchema tLPInsuredSchema2 = new LPInsuredSchema();
        tLPInsuredSchema2.setContNo(tLPEdorItemSchema2.getContNo());
        tLPInsuredSchema2.setInsuredNo(tLPEdorItemSchema2.getInsuredNo());
        tLPInsuredSet.add(tLPInsuredSchema2);

        //3
//        LPEdorItemSchema tLPEdorItemSchema3 = new LPEdorItemSchema();
//        tLPEdorItemSchema3.setEdorAcceptNo(tLPGrpEdorItemSchema.getEdorAcceptNo());
//        tLPEdorItemSchema3.setEdorNo(tLPGrpEdorItemSchema.getEdorNo());
//        tLPEdorItemSchema3.setContNo("2300120144");
//        tLPEdorItemSchema3.setInsuredNo("000120261");
//        tLPEdorItemSet.add(tLPEdorItemSchema3);
//
//        LPInsuredSchema tLPInsuredSchema3 = new LPInsuredSchema();
//        tLPInsuredSchema3.setContNo(tLPEdorItemSchema3.getContNo());
//        tLPInsuredSchema3.setInsuredNo(tLPEdorItemSchema3.getInsuredNo());
//        tLPInsuredSet.add(tLPInsuredSchema3);

        GlobalInput tG = new GlobalInput();
        tG.Operator = "endor0";
        tG.ComCode = "86";

        VData tVData = new VData();
        tVData.addElement(tG);
        tVData.addElement(tLPGrpEdorItemSchema);
        tVData.addElement(tLPEdorItemSet);
        tVData.addElement(tLPInsuredSet);

        //保存个人保单信息(保全)
        GEdorZTDetailBL tGEdorZTDetailBL = new GEdorZTDetailBL();
        tGEdorZTDetailBL.submitData(tVData, "INSERT||EDOR");
        System.out.println(tGEdorZTDetailBL.mErrors.getErrContent());
    }
}
