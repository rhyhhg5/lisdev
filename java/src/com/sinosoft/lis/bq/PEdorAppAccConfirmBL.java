package com.sinosoft.lis.bq;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体保全集体下个人功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */
public class PEdorAppAccConfirmBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;
  private AppAcc mAppAcc=new AppAcc();
  private LCAppAccTraceSchema mLCAppAccTraceSchema = new LCAppAccTraceSchema();
  private LPEdorAppDB mLPEdorAppDB = new LPEdorAppDB();

  private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
  private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();

  private EdorItemSpecialData mEdorItemSpecialData = null;

  private LPDutySchema mLPDutySchema = new LPDutySchema();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private Reflections ref = new Reflections();
  private String currDate = PubFun.getCurrentDate();
  private String currTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public PEdorAppAccConfirmBL() {
  }

  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    mOperate = cOperate;

    //得到外部传入的数据
    if (!getInputData())
      return false;
    System.out.println("---End getInputData---");

    //数据校验操作
    if (!checkData())
      return false;
    System.out.println("---End checkData---");

    if(!dealData())
        return false;

    if (!prepareData())
        return false;

    //　数据提交、保存
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start tPRnewManualDunBLS Submit...");

    if (!tPubSubmit.submitData(mResult, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);

      CError tError = new CError();
      tError.moduleName = "ContBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";

      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 处理业务数据
   * @return boolean
   */
  private boolean dealData() {
      if (mOperate.equals("INSERT||Param"))
      {
          mEdorItemSpecialData.insert();
      }
      else
      {
          if (!mEdorItemSpecialData.query())
          {
              mErrors.addOneError("参数错误");
              return false;
          }
          mLCAppAccTraceSchema.setCustomerNo(mEdorItemSpecialData.
                  getEdorValue("CustomerNo"));
          mLCAppAccTraceSchema.setAccType(mEdorItemSpecialData.
                  getEdorValue("AccType"));
          mLCAppAccTraceSchema.setOtherType(mEdorItemSpecialData.
                  getEdorValue("OtherType"));
          mLCAppAccTraceSchema.setOtherNo(mEdorItemSpecialData.
                  getEdorValue("OtherNo"));
          mLCAppAccTraceSchema.setDestSource(mEdorItemSpecialData.
                  getEdorValue("DestSource"));
          mLCAppAccTraceSchema.setBakNo(mLPEdorAppDB.getEdorAcceptNo());
          mLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);

          mOperate = mEdorItemSpecialData.getEdorValue("Fmtransact2");
          if (mOperate != null)
          {
              //数据准备操作
              if (mOperate.equals("INSERT||ShiftTo"))
              {
                  double tFee = mLPEdorAppDB.getGetMoney();
                  mLCAppAccTraceSchema.setMoney(tFee * -1);
                  map = mAppAcc.accShiftToFW(mLCAppAccTraceSchema);
                  if (map == null)
                  {
                      // @@错误处理
                      this.mErrors.copyAllErrors(mAppAcc.mErrors);
                      return false;
                  }
              }

              if (mOperate.equals("INSERT||TakeOut"))
              {
                  double tFee = mLPEdorAppDB.getGetMoney();
                  LCAppAccSchema tLCAppAccSchema = mAppAcc.getLCAppAcc(
                          mLCAppAccTraceSchema.getCustomerNo());
                  if (tLCAppAccSchema == null)
                  {
                      // @@错误处理
                      this.mErrors.copyAllErrors(mAppAcc.mErrors);
                      return false;
                  }
                  if (tFee > tLCAppAccSchema.getAccGetMoney())
                  {
                      mLCAppAccTraceSchema.setMoney(tLCAppAccSchema.
                              getAccGetMoney() * -1);
                  }
                  else
                  {
                      mLCAppAccTraceSchema.setMoney(tFee * -1);
                  }
                  map = mAppAcc.accTakeOutFW(mLCAppAccTraceSchema);
                  if (map == null)
                  {
                      // @@错误处理
                      this.mErrors.copyAllErrors(mAppAcc.mErrors);
                      return false;
                  }
              }
          }
      }
      return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData() {
    try {
      mLCAppAccTraceSchema = (LCAppAccTraceSchema) mInputData.getObjectByObjectName(
          "LCAppAccTraceSchema", 0);
      mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
              "GlobalInput", 0);
      mEdorItemSpecialData = (EdorItemSpecialData) mInputData.
              getObjectByObjectName(
              "EdorItemSpecialData", 0);
  }
    catch (Exception e) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PEdorAppAccConfirmBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 校验传入的数据的合法性
   * @return
   */
  private boolean checkData() {
      mLPEdorAppDB.setEdorAcceptNo(mLCAppAccTraceSchema.getOtherNo());
      if (!mLPEdorAppDB.getInfo()) {
          // @@错误处理
          this.mErrors.copyAllErrors(mLPEdorAppDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "PInsuredBL";
          tError.functionName = "Preparedata";
          tError.errorMessage = "无保全申请数据!";
          System.out.println("------" + tError);
          this.mErrors.addOneError(tError);
          return false;
      }


      return true;
  }

  /**
   * 准备需要保存的数据
   * @return
   */
  private boolean prepareData() {
      mResult.clear();
      mResult.add(map);
      return true;
  }
}



