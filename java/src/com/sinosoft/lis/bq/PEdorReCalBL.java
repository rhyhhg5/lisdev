package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 个单保全重复理算</p>
 * <p>Description: 个单保全重复理算</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorReCalBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    /** 保全号 */
    private String mEdorAcceptNo = null;

    /**
     * 构造函数
     * @param edorAcceptNo String
     */
    public PEdorReCalBL(String edorAcceptNo)
    {
        this.mEdorAcceptNo = edorAcceptNo;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!dealData())
        {
            return false;
        }
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到提交数据
     * @return MMap
     */
    public MMap getSubmitData()
    {
        if (!dealData())
        {
            return null;
        }
        return mMap;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
//        if(!AppAccUoDo())
//        {
//            return false;
//        }
        setPrem();
        setEdorState(BQ.EDORSTATE_INPUT);
        deleteFinaceData();
        dealEachItem(); //单独处理一些保全项目  added by huxl @20080411
        return true;
    }

    private boolean AppAccUoDo()
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorAcceptNo);
        if (!tLPEdorAppDB.getInfo())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLPEdorAppDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorReCalBL";
            tError.functionName = "AppAccUoDo";
            tError.errorMessage = "查询保全信息主表出错!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }

        AppAcc tAppAcc = new AppAcc();
        String noticeType = "";
        if (tLPEdorAppDB.getOtherNoType().equals("1"))
        {
            noticeType = BQ.NOTICETYPE_P;
        }
        if (tLPEdorAppDB.getOtherNoType().equals("2"))
        {
            noticeType = BQ.NOTICETYPE_G;
        }
        MMap map = tAppAcc.accUnDo(tLPEdorAppDB.getOtherNo(), mEdorAcceptNo,
                                   noticeType);
        if (map == null)
        {
            if (!tAppAcc.mErrors.getLastError().equals("查询投保人帐户轨迹表记录不存在!"))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tAppAcc.mErrors);
                return false;
            }
        }
        mMap.add(map);

        return true;
    }

    /**
     * 重设P表的保费
     */
    private void setPrem()
    {
        setContPrem();
        setPolPrem();
    }

    /**
     * 设置Cont表的保费
     */
    private void setContPrem()
    {
        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setEdorNo(mEdorAcceptNo);
        LPContSet tLPContSet = tLPContDB.query();
        for (int i = 1; i <= tLPContSet.size(); i++)
        {
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(tLPContSet.get(i).getContNo());
            if (tLCContDB.getInfo())
            {
                setLPContPrem(tLCContDB.getSchema());
            }
        }
    }

    /**
     * 设置LPCont表的保费
     * @param aLCContSchema LCContSchema
     */
    private void setLPContPrem(LCContSchema aLCContSchema)
    {
        String sql = "update LPCont " +
                     "set Prem = " + aLCContSchema.getPrem() + ", " +
                     "    SumPrem = " + aLCContSchema.getSumPrem() + " " +
                     "where EdorNo = '" + mEdorAcceptNo + "' " +
                     "and ContNo = '" + aLCContSchema.getContNo() + "'";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 设置Pol表的保费
     */
    private void setPolPrem()
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorAcceptNo);
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tLPPolSet.get(i).getPolNo());
            if (tLCPolDB.getInfo())
            {
                setLPPolPrem(tLCPolDB.getSchema());
            }
        }
    }

    /**
     * 设置LPol表的保费
     */
    private void setLPPolPrem(LCPolSchema aLCPolSchema)
    {
        String sql = "update LPPol " +
                     "set StandPrem = " + aLCPolSchema.getStandPrem() + ", " +
                     "    Prem = " + aLCPolSchema.getPrem() + ", " +
                     "    SumPrem = " + aLCPolSchema.getSumPrem() + " " +
                     "where EdorNo = '" + mEdorAcceptNo + "' " +
                     "and PolNo = '" + aLCPolSchema.getPolNo() + "'";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 设置保全状态，并使保费清零
     */
    private void setEdorState(String edorState)
    {
        String sql;
        sql = "update LPEdorApp set EdorState = '" + edorState + "', " +
              "ChgPrem = 0, ChgAmnt = 0, GetMoney = 0, GetInterest = 0 " +
              "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "update LPEdorMain set EdorState = '" + edorState + "', " +
              "ChgPrem = 0, ChgAmnt = 0, GetMoney = 0, GetInterest = 0 " +
              "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "update LPEdorItem set EdorState = '" + edorState + "', " +
              "ChgPrem = 0, ChgAmnt = 0, GetMoney = 0, GetInterest = 0 " +
              "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 删除财务数据
     */
    private void deleteFinaceData()
    {
    	//add by lijian at 2008-06-04 删除万能险补交保费生成的ljspayperson
    	String tSql="delete from ljspayperson where getnoticeno in ( select getnoticeno from ljspay where otherno='"
    				+mEdorAcceptNo+"' and othernotype='"
    				+BQ.NOTICETYPE_P+"' )";  
    	mMap.put(tSql, "DELETE");
    	//end add by lijian  
        String[] tables =
                {"LJSGetEndorse", "LJSPay", "LJSGet"};
        for (int i = 0; i < tables.length; i++)
        {
            StringBuffer sql = new StringBuffer("delete ");
            sql.append("from ").append(tables[i])
                    .append(" where OtherNo = '")
                    .append(mEdorAcceptNo)
                    .append("' ")
                    .append("and OtherNoType = '")
                    .append(BQ.NOTICETYPE_P)
                    .append("'");
            mMap.put(sql.toString(), "DELETE");
        }
        String sql = "delete from LJTempFeeClass " +
                     "where TempFeeNo in (select TempFeeNo from LJTempFee " +
                     "      where OtherNo = '" + mEdorAcceptNo + "' " +
                     "      and OtherNoType = '" + BQ.NOTICETYPE_P + "' and confmakedate is null ) ";
        mMap.put(sql, "DELETE");
        
        //add by whl 2011-11-22
        String sql1 = "delete from LJTempFee " +
			        " where  OtherNo = '" + mEdorAcceptNo + "' " +
			        "      and OtherNoType = '" + BQ.NOTICETYPE_P + "'  and confmakedate is null ";
	    mMap.put(sql1, "DELETE");
    }

    private void dealEachItem()
    {
        LPEdorItemDB LPEdorItemDB = new LPEdorItemDB();
        LPEdorItemDB.setEdorNo(this.mEdorAcceptNo);
        LPEdorItemSet tLPEdorItemSet = LPEdorItemDB.query();
        for (int i = 1; i <= tLPEdorItemSet.size(); i++)
        {
            
                dealInsuAccData(tLPEdorItemSet.get(i));  
            if(tLPEdorItemSet.get(i).getEdorType().equals("RF")||tLPEdorItemSet.get(i).getEdorType().equals("CT")|| tLPEdorItemSet.get(i).getEdorType().equals("LN")){
            	String sql = "delete from LPReturnLoan " +
                " where EdorNo = '" + mEdorAcceptNo + "' ";
            	mMap.put(sql, "DELETE");
            	String sql1 = "delete from LPLoan " +
                " where EdorNo = '" + mEdorAcceptNo + "' or ActuGetNo ='" + mEdorAcceptNo + "' ";
            	mMap.put(sql1, "DELETE");
            }
        }

    }

    private void dealInsuAccData(LPEdorItemSchema cLPEdorItemSchema)
    {
        String[] tables =
                {"LPInsureAcc", "LPInsureAccClass",
                "LPInsureAccTrace", "LPInsureAccFee",
                "LPInsureAccClassFee", "LPInsureAccFeeTrace"};
        for (int i = 0; i < tables.length; i++)
        {
            StringBuffer sql = new StringBuffer("delete ");
            sql.append("from ").append(tables[i])
                    .append(" where EdorNo = '")
                    .append(mEdorAcceptNo)
                    .append("' ")
                    .append("and EdorType = '")
                    .append(cLPEdorItemSchema.getEdorType())
                    .append("'");
            mMap.put(sql.toString(), "DELETE");
        }
        String sql = "delete from LCInsureAccBalance " +
                     "where SequenceNo = '" + mEdorAcceptNo +
                     "' and ContNo = '" + cLPEdorItemSchema.getContNo() + "' ";
        mMap.put(sql, "DELETE");
    }


    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        VData data = new VData();
        data.add(gi);

        PEdorReCalBL bl = new PEdorReCalBL("20061206000008");

        if (bl.submitData())
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
