package com.sinosoft.lis.bq;

import java.util.Date;

import bsh.This;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bq.CommonBL;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class PEdorTQAppConfirmBL implements EdorAppConfirm {
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = null;
    /** 数据操作字符串 */
    private String mOperate;
    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    //公共账户class的P表
    private LPInsureAccClassSchema mLPAppntAccClassSchema=new LPInsureAccClassSchema();
    
    private String tfeemode="";
    
    /** 保全号 */
    private String mEdorNo = null;
    /** 团体合同号 */
    private String mGrpContNo = null;
    
    /** 全局数据 */
    private GlobalInput mGlobalInput = null;
    private MMap map = new MMap();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public PEdorTQAppConfirmBL() {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }
        //数据校验
        if (!checkdate())
        {
        	
            return false;
        }
        //数据准备操作
        if (!prepareData()) {
            return false;
        }
        System.out.println("---End prepareData of PEdorZTAppConfirmBL---");
        

        prepareOutputData();
        
        return true;
    }
    /**
     * 数据校验。
     * @return
     */
    private boolean checkdate() {
    	String tValiDate = PubFun.getCurrentDate();
        tValiDate = PubFun.calDate(tValiDate, 1, "D", null);
        
        boolean flag = PubFun.isDoBQ(mLPEdorItemSchema.getGrpContNo(), tValiDate);
        /*System.out.println("保全生效日期mEdorValidate"+tValiDate);
        String mEdorValidate = tValiDate.substring(0, 7) + "-01";
        System.out.println("校验日期："+mEdorValidate);
        String sql = "select count(*) from lcinsureacctrace a  where grpcontno='"+mLPEdorItemSchema.getGrpContNo()+"' and exists (select 1 from lcinsureacc where grpcontno=a.grpcontno and polno=a.polno and acctype='001' and insuaccno=a.insuaccno ) and paydate =date('"+mEdorValidate+"') and insuaccno='671803' and MoneyType='MF' and othertype='6' ";
        
        ExeSQL tExeSQL = new ExeSQL();
        String resultString = tExeSQL.getOneValue(sql);
        if(resultString.equals("0")){
        	CError.buildErr(this, "该保单保全生效日的上个月还未月结，请先进行月结再操作该保全！");
        	return false;
        }*/
        if(!flag){
        	CError.buildErr(this, "该保单保全生效日的上个月还未月结，请先进行月结再操作该保全！");
        	return false;
        }else {
        	return true;
		}
	}

	/**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData() {
        mResult = new VData();
        mResult.add(map);
    }


    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData() {
        try {
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData.
                                getObjectByObjectName("LPEdorItemSchema", 0);
            mGlobalInput = ((GlobalInput) mInputData.
                            getObjectByObjectName("GlobalInput", 0));
            
        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }
    
    /**
     * 准备需要保存的数据
     */
    private boolean prepareData() {
        double aTotalReturn = 0; //总退费
       
        String tInsuNoOld=mLPEdorItemSchema.getInsuredNo();
        String tInsuNo=new ExeSQL().getOneValue("select insuredno from lcpol where contno='"+mLPEdorItemSchema.getContNo()+"'");
        mLPEdorItemSchema.setInsuredNo(tInsuNo);
        
        System.out.println(PubFun.calDate(mLPEdorItemSchema.getEdorValiDate(), 1, "D", null));
        //理算前，先修改lpedoritem表中的edorvalidate至为生效日
        //刘鑫新增 
        //确定保全生效日期，为保全理算日的次日
        Date tValiDate = null;
        FDate fDate = new FDate();
        tValiDate = fDate.getDate(PubFun.getCurrentDate());
        tValiDate = PubFun.calDate(tValiDate, 1, "D", null);
        String mEdorValidate =fDate.getString(tValiDate);
        System.out.println("保全生效日期mEdorValidate"+mEdorValidate);
        mLPEdorItemSchema.setEdorValiDate(mEdorValidate);
        
        //对该分单截止到保全生效日的区间进行月结,结算结果直接放到P表中
      	VData tVData = new VData();
    	tVData.add(mLPEdorItemSchema);  	
    	tVData.add(mGlobalInput);      
    	
        UliBQInsuAccBala tUliBQInsuAccBala = new UliBQInsuAccBala();
        if(!tUliBQInsuAccBala.submitData(tVData, "",mLPEdorItemSchema.getGrpContNo())){
        	CError.buildErr(this, "保单:" + mLPEdorItemSchema.getContNo() + " 结算失败; ");
        	return false;
        }
        this.tfeemode = tUliBQInsuAccBala.glFeeMode;
        mLPEdorItemSchema.setInsuredNo(tInsuNoOld);
        
        if(!getGrpAcc()){
        	CError.buildErr(this, "获取公共账户失败; ");
        	return false;
        }

        
        //进行退保
        if (!getZTData(mLPEdorItemSchema.getSchema())) {
            System.out.println("退保计算失败");
            return false;
        }

        //如果涉及补退费，则按照描述表中的财务类型进行修改
        LJSGetEndorseSchema tLJSGetEndorseSchema = null;
        LJSGetEndorseSet tempLJSGetEndorseSet = new LJSGetEndorseSet();
        LJSGetEndorseSet tLJSGetEndorseSet = mLJSGetEndorseSet;
        if (tLJSGetEndorseSet != null && tLJSGetEndorseSet.size() > 0) {
            for (int k = 1; k <= tLJSGetEndorseSet.size(); k++) {
                tLJSGetEndorseSchema = tLJSGetEndorseSet.get(k);
                tLJSGetEndorseSchema.setGetFlag("1");
                tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);

                //从描述表中获取财务接口类型，modify by Minim at 2003-12-23
                String finType = BqCalBL.getFinType(mLPEdorItemSchema.
                        getEdorType(), "TB", tLJSGetEndorseSchema.getPolNo());
                if (finType.equals("")) {
                    CError.buildErr(this, "在LDCode1中缺少保全财务接口转换类型编码!");
                    return false;
                }
                tLJSGetEndorseSchema.setFeeFinaType(finType);

                tempLJSGetEndorseSet.add(tLJSGetEndorseSchema);
                //计算出总费用
                aTotalReturn = aTotalReturn + tLJSGetEndorseSchema.getGetMoney();
            }
        }

        //设置保全状态为理算完成
        mLPEdorItemSchema.setGetMoney(aTotalReturn);
        mLPEdorItemSchema.setEdorState("2");
        mLPEdorItemSchema.setUWFlag("0");
        
        mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPEdorItemSchema.setEdorValiDate(mEdorValidate);
        
        mInputData.clear();
        map.put(mLPEdorItemSchema, "UPDATE");
        map.put(tempLJSGetEndorseSet, "DELETE&INSERT");
        mInputData.add(mLPEdorItemSchema);
        mInputData.add(tempLJSGetEndorseSet);

        return true;
    }



    /**
     * 准备需要退保的数据，单个个人保单退保入口，LPEdorItemSchema中只需要PolNo、EdorNo和EdorType即可
     * @return boolean
     */
    private boolean getGrpAcc() {
    	LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(mLPEdorItemSchema.getGrpContNo());
        tLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() != 1)
        {
        	mErrors.addOneError("line187-找不到公共账户个单险种信息！");
            return false;
        }
        String tInsuAccNo=CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP, tLCPolSet.get(1).getRiskCode());

        //P表中有的话直接获取
        LPInsureAccClassDB tLPInsureAccClassDB=new LPInsureAccClassDB();
        LPInsureAccClassSet tLPInsureAccClassSet=new LPInsureAccClassSet();
        tLPInsureAccClassDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPInsureAccClassDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPInsureAccClassDB.setPolNo(tLCPolSet.get(1).getPolNo());
        tLPInsureAccClassDB.setInsuAccNo(tInsuAccNo);
        tLPInsureAccClassSet=tLPInsureAccClassDB.query();
        if(tLPInsureAccClassSet.size()==1){
        	mLPAppntAccClassSchema.setSchema(tLPInsureAccClassSet.get(1));
        	return true;
        }else if(tLPInsureAccClassSet.size()>1){
        	mErrors.addOneError("line206-找不到公共账户信息！");
        	return false;
        }else {
//        	P表中没有需要手工生成
            LCInsureAccClassDB tLCInsureAccClassDB=new LCInsureAccClassDB();
            LCInsureAccClassSet tLCInsureAccClassSet=new LCInsureAccClassSet();
            tLCInsureAccClassDB.setPolNo(tLCPolSet.get(1).getPolNo());
            tLCInsureAccClassDB.setInsuAccNo(tInsuAccNo);
            tLCInsureAccClassSet=tLCInsureAccClassDB.query();
            if(tLCInsureAccClassSet.size()==1){
            	LPInsureAccClassSchema tLPInsureAccClassSchema=new LPInsureAccClassSchema();
                tLPInsureAccClassSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
                tLPInsureAccClassSchema.setEdorType(mLPEdorItemSchema.getEdorType());
                Reflections ref = new Reflections();
                ref.transFields(tLPInsureAccClassSchema, tLCInsureAccClassSet.get(1));
            	mLPAppntAccClassSchema.setSchema(tLPInsureAccClassSchema);
            	//同时在此处生成公共帐户的ACC和ACCCLASS的P表,以备以后调用
            	//插入ACCCLASS表
            	map.put(mLPAppntAccClassSchema,"INSERT");
            	
                LPInsureAccDB tLPInsureAccDB=new LPInsureAccDB();
                tLPInsureAccDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
                tLPInsureAccDB.setEdorType(mLPEdorItemSchema.getEdorType());
                tLPInsureAccDB.setPolNo(tLCPolSet.get(1).getPolNo());
                tLPInsureAccDB.setInsuAccNo(tInsuAccNo);
                if(!tLPInsureAccDB.getInfo()){
//              没查到ACC表的P表则进行插入            	
            	LCInsureAccDB tLCInsureAccDB=new LCInsureAccDB();
            	tLCInsureAccDB.setPolNo(tLCPolSet.get(1).getPolNo());
            	tLCInsureAccDB.setInsuAccNo(tInsuAccNo);
            	if(!tLCInsureAccDB.getInfo()){
            		CError.buildErr(this, "line228-找不到公共账户ACC信息！");
        			System.out
        					.println("PEdorTQAppConfirmBL+getGrpAcc++--");
        			return false;
            	}
            	LPInsureAccSchema tLPInsureAccSchema=new LPInsureAccSchema();
                tLPInsureAccSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
                tLPInsureAccSchema.setEdorType(mLPEdorItemSchema.getEdorType());
                ref.transFields(tLPInsureAccSchema, tLCInsureAccDB.getSchema());
                map.put(tLPInsureAccSchema,"INSERT");
                }
            	return true;
            }else{
            	mErrors.addOneError("line219-找不到公共账户信息！");
            	return false;
            }
        }
    }
    
    
    private LCInsuredSet getLPInsuredSet(LPEdorItemSchema tLPEdorItemSchema) {
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();

        if (tLPEdorItemSchema.getPolNo().equals("000000") &&
            tLPEdorItemSchema.getInsuredNo().equals("000000")) { //合同退保
            tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
            tLCInsuredSet = tLCInsuredDB.query();
            if (tLCInsuredDB.mErrors.needDealError()) {
                CError.buildErr(this, "查询被保险人失败！");
                return null;
            }
        } else if (tLPEdorItemSchema.getPolNo().equals("000000")) { //被保险人退保
            tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
            tLCInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
            tLCInsuredSet = tLCInsuredDB.query();
            if (tLCInsuredDB.mErrors.needDealError()) {
                CError.buildErr(this, "查询被保险人失败！");
                return null;
            }
        }
        return tLCInsuredSet;
    }

    /**
     * 准备需要退保的数据，单个个人保单退保入口，LPEdorItemSchema中只需要PolNo、EdorNo和EdorType即可
     * @return
     */
    public boolean getZTData(LPEdorItemSchema pLPEdorItemSchema) {
        try {
            //得到序要退保的被保人
            LCInsuredSet tLCInsuredSet = getLPInsuredSet(pLPEdorItemSchema);
            for (int t = 1; t <= tLCInsuredSet.size(); t++) {
                //得到被保人的险种
                LPPolSet tLPPolSet = getInsuredPol(pLPEdorItemSchema,tLCInsuredSet.get(t));
                //团险万能产品肯定为单险种
                if(tLPPolSet.size()!=1){
                	CError.buildErr(this, "line205-获取保单"+tLCInsuredSet.get(t).getContNo()+"险种信息失败！");
                    return false;
                }
                LPPolSchema tLPPolSchema=tLPPolSet.get(1);
                
                //获取当时的导入表,保存相关数据
                LPDiskImportSchema tLPDiskImportSchema=getImport(tLPPolSchema,tLCInsuredSet.get(t));
                if(tLPDiskImportSchema==null){
                	CError.buildErr(this, "line213-获取保单"+tLCInsuredSet.get(t).getContNo()+"导入信息失败！");
                    return false;
                }

                //为保单制作个单保全主表数据
                LPEdorItemSchema tLPEdorItemSchema = pLPEdorItemSchema.getSchema();
                tLPEdorItemSchema.setPolNo(tLPPolSchema.getPolNo());

                
                //获取个人账户单位部分保险账户号码
                String tGrpInsuNo=new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLPPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='4')) ");
                LPInsureAccClassSet tLPGrpAccClassSet=getAccClass(tLPPolSchema,tGrpInsuNo);
            	if(tLPGrpAccClassSet.size()!=1){
            		CError.buildErr(this, "line238-获取个人账户单位缴费账户失败！");
                    return false;
            	}
            	LPInsureAccClassSchema tLPGrpAccClassSchema=tLPGrpAccClassSet.get(1);
            	
//            	获取个人账户个人部分保险账户号码
                String tInsuNo = new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLPPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')) ");
                LPInsureAccClassSet tLPInsuredAccClassSet=getAccClass(tLPPolSchema,tInsuNo);
            	if(tLPInsuredAccClassSet.size()!=1){
            		CError.buildErr(this, "line238-获取个人账户个人缴费账户失败！");
                    return false;
            	}
            	LPInsureAccClassSchema tLPInsuredAccClassSchema=tLPInsuredAccClassSet.get(1);
            	
            	
            	//获取归属比例
            	Reflections ref = new Reflections();
            	LCInsureAccClassSchema taLCGrpAccClassSchema= new LCInsureAccClassSchema();
            	ref.transFields(taLCGrpAccClassSchema,tLPGrpAccClassSchema);
                double tRate =CommonBL.getULIRate(taLCGrpAccClassSchema,mLPEdorItemSchema.getEdorValiDate());
//                	getImportULIRate(tLPEdorItemSchema,tLCInsuredSet.get(t),tLPGrpAccClassSchema);
                if(tRate==-1){
                	CError.buildErr(this, "line293-获取归属比例失败！");
                    return false;
                }
            	
            	//进行金额归属
            	//归属到个单的金额
            	double tInsuredMoney=CommonBL.carry(tLPGrpAccClassSchema.getInsuAccBala()*tRate);
            	//归属到团单的金额
            	double tGrpMoney=Arith.sub(CommonBL.carry(tLPGrpAccClassSchema.getInsuAccBala()), tInsuredMoney);
            	System.out.println("..................归属到团单的金额:"+tGrpMoney);
            	//个人账户单位部分转出的金额轨迹
            	setLPAccTrace(tLPGrpAccClassSchema, -tInsuredMoney,tLPGrpAccClassSchema.getEdorType(),tLPInsuredAccClassSchema);
            	setLPAccTrace(tLPGrpAccClassSchema, -tGrpMoney,tLPGrpAccClassSchema.getEdorType(),mLPAppntAccClassSchema);
            	//个人账户个人部分转入的金额轨迹
            	setLPAccTrace(tLPInsuredAccClassSchema, tInsuredMoney,tLPGrpAccClassSchema.getEdorType(),tLPGrpAccClassSchema);
            	
                //	公共账户转入的轨迹:
            	setLPAccTrace(mLPAppntAccClassSchema, tGrpMoney,tLPGrpAccClassSchema.getEdorType(),tLPGrpAccClassSchema);
            	
            	//公共账户余额处理,涉及到批量提交,只能用SQL-update方式
            	String LPAppntAccUpdate="update lpinsureacc set insuaccbala=insuaccbala+"+tGrpMoney+" where edorno='"+mLPAppntAccClassSchema.getEdorNo()+"' and edortype='"+mLPAppntAccClassSchema.getEdorType()+"' and polno='"+mLPAppntAccClassSchema.getPolNo()+"' and insuaccno='"+mLPAppntAccClassSchema.getInsuAccNo()+"' ";
            	String LPAppntAccClassUpdate="update lpinsureaccclass set insuaccbala=insuaccbala+"+tGrpMoney+" where edorno='"+mLPAppntAccClassSchema.getEdorNo()+"' and edortype='"+mLPAppntAccClassSchema.getEdorType()+"' and polno='"+mLPAppntAccClassSchema.getPolNo()+"' and insuaccno='"+mLPAppntAccClassSchema.getInsuAccNo()+"'  and grpcontno='"+mLPAppntAccClassSchema.getGrpContNo()+"'  and payplancode='"+mLPAppntAccClassSchema.getPayPlanCode()+"'  and otherno='"+mLPAppntAccClassSchema.getOtherNo()+"'  and ACCASCRIPTION='"+mLPAppntAccClassSchema.getAccAscription()+"' ";
            	map.put(LPAppntAccUpdate,"UPDATE");
            	map.put(LPAppntAccClassUpdate,"UPDATE");
            	
            	
            	//设置导入表的个人账户个人部分归属前余额
                tLPDiskImportSchema.setPersonPrem(Double.toString(tLPInsuredAccClassSchema.getInsuAccBala()));
                Date tValiDate = null;
                FDate fDate = new FDate();
                tValiDate = fDate.getDate(PubFun.getCurrentDate());
                tValiDate = PubFun.calDate(tValiDate, 1, "D", null);
                String mEdorValidate =fDate.getString(tValiDate);
                tLPDiskImportSchema.setEdorValiDate(mEdorValidate);
                
            	//个人账户个人部分归属后的金额
            	double tAfterInsuredMoney=CommonBL.carry(Arith.add(tLPInsuredAccClassSchema.getInsuAccBala(), tInsuredMoney));
            	
            	//处理个人部分帐户余额,以便后期计算
            	if(!setLPAcc(tLPInsuredAccClassSchema,tLPInsuredAccClassSchema.getInsuAccBala()+tInsuredMoney)){
                    return false;
            	}
            	

//              解约管理费率
            	System.out.println("解约管理费率开始计算");
            	int tpolyear=PubFun.calPolYear(tLPPolSchema.getCValiDate(), mLPEdorItemSchema.getEdorValiDate());
            	String tfee = "";
            	String polRateSQL = "SELECT extractrate FROM LCRISKZTFEE WHERE grpcontno = '"+tLPPolSchema.getGrpContNo()+"'  and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear;
            	tfee = new ExeSQL().getOneValue(polRateSQL);
                if ((tfee == null) || (tfee.equals(""))){
                	tfee=new ExeSQL().getOneValue("select extractrate from lmriskztfee where riskcode='"+tLPPolSchema.getRiskCode()+"' and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear);
                	if ((tfee == null) || (tfee.equals(""))){
                		mErrors.addOneError("查询管理费为空！");
                		return false;
                	}
                }
            	double feeRate = Double.parseDouble(tfee); 
            	
//            	退保费用
            	double tZTfee=CommonBL.carry(tAfterInsuredMoney*feeRate);
//            	个人账户个人部分退保费用轨迹
            	setLPAccTrace(tLPInsuredAccClassSchema, -tZTfee,"MF",null);

//            	该被保人所退费用合计
            	double tGetMoney=Arith.sub(tAfterInsuredMoney, tZTfee);
//            	个人账户个人部分所退保费轨迹
            	setLPAccTrace(tLPInsuredAccClassSchema, -tGetMoney,"TB",null);
            	  
//              生成ljsgetendorse
            	LJSGetEndorseSchema tLJSGetEndorseSchema = getLJSGetEndorse(mLPEdorItemSchema,tLPPolSchema,"TB", Arith.round(-tGetMoney, 5), mGlobalInput);
                if(tLJSGetEndorseSchema==null){
                	mErrors.addOneError("保全补退费表生成失败！");
                    return false;
                }
                
//            	处理帐户余额为0,确认时交换
            	if(!(setLPAcc(tLPGrpAccClassSchema,0)&&setLPAcc(tLPInsuredAccClassSchema,0))){
                    return false;
            	}
            	
                //理算结果加入公共变量中
                mLJSGetEndorseSet.add(tLJSGetEndorseSchema);

                /*	将各项金额数据存到导入表中,对应字段如下:
                 *	归属比例 money2
                 *	个人账户单位部分公共账户归属 appntprem
                 *	个人账户单位部分个人账户归属 PersonOwnPrem
                 *	个人账户个人部分 personprem
                 *	退保费用(退保手续费) money
                 *	保单管理费 getmoney
                 *	本保单退费 insuaccbala
                 */
                tLPDiskImportSchema.setMoney2(Double.toString(tRate));
                tLPDiskImportSchema.setAppntPrem(Double.toString(tGrpMoney));
                tLPDiskImportSchema.setPersonOwnPrem(Double.toString(tInsuredMoney));

                //计算本单的管理费合计
                String tManageFee=getManageFee(tLPPolSchema);
                System.out.println("");
                if(tManageFee==null||tManageFee.equals("")){
                	mErrors.addOneError("line384-管理费生成失败！");
                    return false;
                }
                tLPDiskImportSchema.setGetMoney(tManageFee);
                tLPDiskImportSchema.setMoney(Double.toString(tZTfee));
                tLPDiskImportSchema.setInsuAccBala(Double.toString(tGetMoney));
                tLPDiskImportSchema.setState("1");
                
                //新添加入导入表的更新
                map.put(tLPDiskImportSchema,"UPDATE");
            }
        } catch (Exception ex) {
            mErrors.addOneError(ex.getMessage());
            ex.printStackTrace();
            return false;
        }

        return true;
    }
    
    
    /**
     * 获取月结管理费
     * @param LPPolSchema tLPPolSchema
     * @return String tManageFee
     */
    private String getManageFee(LPPolSchema tLPPolSchema)
    {
    	ExeSQL texesql=new ExeSQL();
    	//String tfeemode=texesql.getOneValue("select riskfeemode from lcgrpfee where grpcontno='"+tLPPolSchema.getGrpContNo()+"' and insuaccno='000000' and payplancode='000000'");
    	String tfeemodevalue = this.tfeemode;
    	if(tfeemodevalue==null||tfeemodevalue.equals("")){
        	return null;
        }else if(tfeemodevalue.equals("1")){
        	return texesql.getOneValue("select sum(money) from lpinsureacctrace where othertype='14' and moneytype='MF' and polno='"+tLPPolSchema.getPolNo()+"' and edorno='"+tLPPolSchema.getEdorNo()+"' and edortype='"+tLPPolSchema.getEdorType()+"'");
        }else if(tfeemodevalue.equals("2")){
        	return texesql.getOneValue("select sum(money) from lpinsureacctrace where othertype='14' and moneytype='MF' and AIPolNo='"+tLPPolSchema.getPolNo()+"' and polno='"+mLPAppntAccClassSchema.getPolNo()+"' and edorno='"+tLPPolSchema.getEdorNo()+"' and edortype='"+tLPPolSchema.getEdorType()+"'");
        }else{
        	return null;
        }
    }
    
    /**
     * 生成交退费记录
     * @param aLPEdorItemSchema
     * @param aLPPolSchema
     * @param aOperationType
     * @param aFeeType
     * @param aGetMoney
     * @param aGlobalInput
     * @return LJSGetEndorseSchema
     */
    public LJSGetEndorseSchema getLJSGetEndorse(LPEdorItemSchema aLPEdorItemSchema, LPPolSchema aLPPolSchema,
                                                 String aPayPlanCode, double aGetMoney, GlobalInput aGlobalInput)
    {
        try
        {
            LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();

            //生成批改交退费表
            tLJSGetEndorseSchema.setGetNoticeNo(aLPPolSchema.getEdorNo()); //给付通知书号码
            tLJSGetEndorseSchema.setEndorsementNo(aLPPolSchema.getEdorNo());
            tLJSGetEndorseSchema.setGrpContNo(aLPPolSchema.getGrpContNo());
            tLJSGetEndorseSchema.setContNo(aLPPolSchema.getContNo());
            tLJSGetEndorseSchema.setGrpPolNo(aLPPolSchema.getGrpPolNo());
            tLJSGetEndorseSchema.setPolNo(aLPPolSchema.getPolNo());
            tLJSGetEndorseSchema.setFeeOperationType(aLPPolSchema.getEdorType());
            tLJSGetEndorseSchema.setGetDate(aLPEdorItemSchema.getEdorValiDate());
            tLJSGetEndorseSchema.setGetMoney(aGetMoney);
            tLJSGetEndorseSchema.setFeeOperationType(aLPPolSchema.getEdorType()); //补退费业务类型
            tLJSGetEndorseSchema.setFeeFinaType("TB"); //补退费财务类型
            tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA); //无作用
            tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA); //无作用，但一定要，转ljagetendorse时非空
            tLJSGetEndorseSchema.setOtherNo(aLPEdorItemSchema.getEdorNo()); //其他号码置为保全批单号
            tLJSGetEndorseSchema.setOtherNoType("3"); //保全给付
            tLJSGetEndorseSchema.setGetFlag("0");
            tLJSGetEndorseSchema.setManageCom(aLPPolSchema.getManageCom());
            tLJSGetEndorseSchema.setAgentCode(aLPPolSchema.getAgentCode());
            tLJSGetEndorseSchema.setAgentCom(aLPPolSchema.getAgentCom());
            tLJSGetEndorseSchema.setAgentGroup(aLPPolSchema.getAgentGroup());
            tLJSGetEndorseSchema.setAgentType(aLPPolSchema.getAgentType());
            tLJSGetEndorseSchema.setInsuredNo(aLPPolSchema.getInsuredNo());
            tLJSGetEndorseSchema.setKindCode(aLPPolSchema.getKindCode());
            tLJSGetEndorseSchema.setAppntNo(aLPPolSchema.getAppntNo());
            tLJSGetEndorseSchema.setRiskCode(aLPPolSchema.getRiskCode());
            tLJSGetEndorseSchema.setRiskVersion(aLPPolSchema.getRiskVersion());
            tLJSGetEndorseSchema.setHandler(aLPPolSchema.getHandler());
            tLJSGetEndorseSchema.setApproveCode(aLPPolSchema.getApproveCode());
            tLJSGetEndorseSchema.setApproveDate(aLPPolSchema.getApproveDate());
            tLJSGetEndorseSchema.setApproveTime(aLPPolSchema.getApproveTime());
            tLJSGetEndorseSchema.setOperator(aGlobalInput.Operator);
            tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
            return tLJSGetEndorseSchema;
        }
        catch (Exception ex)
        {
            mErrors.addOneError(new CError("建立批改补退费信息异常！"));
            return null;
        }
    }
    
    
    /**
     * 获取账户
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param getMoney double
     */
    private LPInsureAccClassSet getAccClass(LPPolSchema tLPPolSchema, String tInsuAccNo)
    {
        LPInsureAccClassDB tLPInsureAccClassDB = new LPInsureAccClassDB();
        tLPInsureAccClassDB.setEdorNo(tLPPolSchema.getEdorNo());
        tLPInsureAccClassDB.setEdorType(tLPPolSchema.getEdorType());
        tLPInsureAccClassDB.setPolNo(tLPPolSchema.getPolNo());
        tLPInsureAccClassDB.setInsuAccNo(tInsuAccNo);
        return tLPInsureAccClassDB.query();
    }
    
    
    /**
     * 设置账户余额,包括acc和accclass两个表的P表
     * @param tLPInsureAccClassSchema LPInsureAccClassSchema
     * @param getMoney double
     */
    private boolean setLPAcc(LPInsureAccClassSchema tLPInsureAccClassSchema, double leftMoney)
    {
        LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
        LPInsureAccDB tLPInsureAccDB = new LPInsureAccDB();
        tLPInsureAccDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPInsureAccDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPInsureAccDB.setPolNo(tLPInsureAccClassSchema.getPolNo());
        tLPInsureAccDB.setInsuAccNo(tLPInsureAccClassSchema.getInsuAccNo());
        if(!tLPInsureAccDB.getInfo()){
        	CError.buildErr(this, "line455-获取账户P表失败！");
        	return false;
        }
        tLPInsureAccSchema=tLPInsureAccDB.getSchema();
        tLPInsureAccSchema.setInsuAccBala(leftMoney);
        map.put(tLPInsureAccSchema, "DELETE&INSERT");
        tLPInsureAccClassSchema.setInsuAccBala(leftMoney);
        map.put(tLPInsureAccClassSchema, "DELETE&INSERT");
        return true;
    }
    
    /**
     * 设置帐户金额轨迹
     * @param tLPInsureAccClassSchema LPInsureAccClassSchema 轨迹对应的当前账户
     * @param tMoney double
     * @param tMoneyType String 
     * @param tAILPInsureAccClassSchema LPInsureAccClassSchema 轨迹来源或者去向账户
     */
    private void setLPAccTrace(LPInsureAccClassSchema tLPInsureAccClassSchema, double tMoney ,String tMoneyType,LPInsureAccClassSchema tAILPInsureAccClassSchema)
    {
        String serialNo;
//        LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
//        tLPInsureAccTraceDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
//        tLPInsureAccTraceDB.setEdorType(mLPEdorItemSchema.getEdorType());
//        tLPInsureAccTraceDB.setGrpContNo(mLPEdorItemSchema.getGrpContNo());
//        tLPInsureAccTraceDB.setPolNo(tLPInsureAccClassSchema.getPolNo());
//        tLPInsureAccTraceDB.setInsuAccNo(tLPInsureAccClassSchema.getInsuAccNo());
//        LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
//        if (tLPInsureAccTraceSet.size() > 0)
//        {
//            serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
//            String sql = "delete from LPInsureAccTrace " +
//                    "where EdorNo = '" + mLPEdorItemSchema.getEdorNo() + "' " +
//                    "and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' " +
//                    "and SerialNo = '" + serialNo + "' ";
//            map.put(sql, "DELETE");
//        }
//        else
//        {
            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
//        }
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPInsureAccTraceSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPInsureAccTraceSchema.setGrpContNo(tLPInsureAccClassSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(tLPInsureAccClassSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(tLPInsureAccClassSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(tLPInsureAccClassSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(tLPInsureAccClassSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(tLPInsureAccClassSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(tLPInsureAccClassSchema.getPayPlanCode());
        tLPInsureAccTraceSchema.setOtherNo(mLPEdorItemSchema.getEdorNo());
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(tMoneyType);
        tLPInsureAccTraceSchema.setMoney(tMoney);  
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mLPEdorItemSchema.getEdorValiDate());
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setManageCom(tLPInsureAccClassSchema.getManageCom());
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        if(!(tAILPInsureAccClassSchema==null)){
        tLPInsureAccTraceSchema.setAIPolNo(tAILPInsureAccClassSchema.getPolNo());
        tLPInsureAccTraceSchema.setAIInsuAccNo(tAILPInsureAccClassSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setAIPayPlanCode(tAILPInsureAccClassSchema.getPayPlanCode());
        }
        map.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }


//获取单位缴费部分归属比例
    private double getImportULIRate(LPEdorItemSchema pLPEdorItemSchema,
                                  LCInsuredSchema tLCInsuredSchema,LPInsureAccClassSchema tLPGrpAccClassSchema) {
    	Reflections ref = new Reflections();
    	LCInsureAccClassSchema tLCGrpAccClassSchema= new LCInsureAccClassSchema();
    	ref.transFields(tLCGrpAccClassSchema,tLPGrpAccClassSchema);
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        tLPDiskImportDB.setGrpContNo(tLCInsuredSchema.getGrpContNo());
        tLPDiskImportDB.setEdorNo(pLPEdorItemSchema.getEdorNo());
        tLPDiskImportDB.setEdorType(pLPEdorItemSchema.getEdorType());
        tLPDiskImportDB.setInsuredNo(tLCInsuredSchema.getInsuredNo());
        LPDiskImportSet set = tLPDiskImportDB.query();
        if (set.size() == 0) {
        	//从公共方法获取归属比例
            return CommonBL.getULIRate(tLCGrpAccClassSchema,mLPEdorItemSchema.getEdorValiDate());
        }
        //没有导入保费
        if (set.get(1).getMoney2() == null || set.get(1).getMoney2().equals("")) {
            return CommonBL.getULIRate(tLCGrpAccClassSchema,mLPEdorItemSchema.getEdorValiDate());
        }
        return Double.parseDouble(set.get(1).getMoney2());
    }

    private LPDiskImportSchema getImport(LPPolSchema tLPPolSchema,LCInsuredSchema tLCInsuredSchema) {
    	LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();    	
    	tLPDiskImportDB.setGrpContNo(tLPPolSchema.getGrpContNo());
    	tLPDiskImportDB.setEdorNo(tLPPolSchema.getEdorNo());
    	tLPDiskImportDB.setEdorType(tLPPolSchema.getEdorType());
    	//根据五要素获取
    	tLPDiskImportDB.setSex(tLPPolSchema.getInsuredSex());
    	tLPDiskImportDB.setBirthday(tLPPolSchema.getInsuredBirthday());
    	tLPDiskImportDB.setIDType(tLCInsuredSchema.getIDType());
    	tLPDiskImportDB.setIDNo(tLCInsuredSchema.getIDNo());
    	tLPDiskImportDB.setInsuredName(tLPPolSchema.getInsuredName());
    	LPDiskImportSet tLPDiskImportSet = tLPDiskImportDB.query();
    	if (tLPDiskImportSet.size()!=1) {
    		CError.buildErr(this, "line311-查询保单"+tLPPolSchema.getContNo()+"导入信息失败！");
    		return null;
    	}
    	return tLPDiskImportSet.get(1);
    }
    
    private LPPolSet getInsuredPol(LPEdorItemSchema pLPEdorItemSchema,
                                   LCInsuredSchema tLCInsuredSchema) {
        Reflections ref = new Reflections();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(tLCInsuredSchema.getContNo());
        tLCPolDB.setInsuredNo(tLCInsuredSchema.getInsuredNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolDB.mErrors.needDealError()) {
            CError.buildErr(this, "查询保单失败！");
            return null;
        }

        LPPolSet tLPPolSet = new LPPolSet();
        for (int i = 1; i <= tLCPolSet.size(); i++) {
            LPPolSchema tLPPolSchema = new LPPolSchema();
            ref.transFields(tLPPolSchema, tLCPolSet.get(i));
            tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPPolSet.add(tLPPolSchema);
        }
        return tLPPolSet;
    }


    public static void main(String[] args) {
        LPEdorItemDB mLPEdorItemDB = new LPEdorItemDB();
        mLPEdorItemDB.setEdorNo("20060606000013");
        mLPEdorItemDB.setContNo("2300156307");
        mLPEdorItemDB.setEdorType("ZT");

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.Operator = "pa9501";
        mGlobalInput.ManageCom = "86";

        VData tVata = new VData();
        tVata.add(mLPEdorItemDB.query().get(1));
        tVata.add(mGlobalInput);
        PEdorTQAppConfirmBL tPEdorZTAppConfirmBL = new PEdorTQAppConfirmBL();
        tPEdorZTAppConfirmBL.submitData(tVata, "");
        if (tPEdorZTAppConfirmBL.mErrors.needDealError()) {
            System.out.println(tPEdorZTAppConfirmBL.mErrors.getErrContent());
        }
    }

}
