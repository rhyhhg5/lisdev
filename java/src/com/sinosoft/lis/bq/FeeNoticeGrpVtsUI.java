package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;

/**
 * <p>Title: 生成交退费通知书</p>
 * <p>Description: 新生成的批单中包含了交费方式等内容 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */
public class FeeNoticeGrpVtsUI
{
    private FeeNoticeGrpVtsBL mFeeNoticeGrpVtsBL = null;

    /**
     * 构造函数
     * @param edorAcceptNo String
     */
    public FeeNoticeGrpVtsUI(String edorNo)
    {
        mFeeNoticeGrpVtsBL = new FeeNoticeGrpVtsBL(edorNo);
    }

    /**
     * 调用业务逻辑
     * @param payInfo TransferData
     * @return boolean
     */
    public boolean submitData(TransferData payInfo)
    {
        if (!mFeeNoticeGrpVtsBL.submitData(payInfo))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mFeeNoticeGrpVtsBL.mErrors.getFirstError();
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        TransferData payData = new TransferData();
        payData.setNameAndValue("payMode", "1");
        payData.setNameAndValue("payDate", "");
        payData.setNameAndValue("endDate", "2005-04-10");
        payData.setNameAndValue("bank", "");

        FeeNoticeGrpVtsUI tFeeNoticeVtsUI = new FeeNoticeGrpVtsUI("20050713000019");
        tFeeNoticeVtsUI.submitData(payData);
    }
}
