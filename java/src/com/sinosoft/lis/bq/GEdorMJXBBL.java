package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.xb.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 满期结算中对保单进行续保
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GEdorMJXBBL
{
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;
    private LPGrpContSchema mLPGrpContSchema = null;
    private TransferData mTransferData = null;

    public GEdorMJXBBL()
    {
    }

    /**
     * 外部操作的提交方法
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        if(!getInputData(data))
        {
            return false;
        }
        if(!checkData())
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData data)
    {
        this.mLPGrpContSchema = (LPGrpContSchema) data
                           .getObjectByObjectName("LPGrpContSchema", 0);
        this.mGlobalInput = (GlobalInput) data
                            .getObjectByObjectName("GlobalInput", 0);
        this.mTransferData = (TransferData) data
                             .getObjectByObjectName("TransferData", 0);
        if(mLPGrpContSchema == null || mGlobalInput == null
            || mTransferData == null)
         {
             mErrors.addOneError("传入续保模块的数据不完整。");
             return false;
         }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LCGrpPol a, LPGrpPol b ")
            .append("where a.grpPolNo = b.grpPolNo ")
            .append("   and b.edorNo = '")
            .append(this.mLPGrpContSchema.getEdorNo())
            .append("' ");
        System.out.println(sql.toString());
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.executeQuery(sql.toString());
        if(tLCGrpPolSet.size() == 0)
        {
            mErrors.addOneError("每有查询到需要续保的险种信息。");
            return false;
        }

        sql = new StringBuffer();
        sql.append("select state ")
            .append("from LCRnewStateLog ")
            .append("where grpContNo = '")
            .append(mLPGrpContSchema.getGrpContNo()).append("' ")
            .append("   and state != '")
            .append(XBConst.RNEWSTATE_DELIVERED).append("' ")
            .append("order by state ");
        ExeSQL e = new ExeSQL();
        String state = e.getOneValue(sql.toString());
        if(state.equals(XBConst.RNEWSTATE_DELIVERED))
        {
            mErrors.addOneError("您不能进行本次操作，因为保单已续保成功。");
            return false;
        }

        boolean flag = false;  //控制续保流程

        if((state.equals("") || state.equals("null")))
        {
            flag = true;
            if(!xbApp(tLCGrpPolSet))
            {
                return false;
            }
        }
        if(flag || state.equals(XBConst.RNEWSTATE_UNCONFIRM))
        {
            flag = true;
            if(!xbSign(tLCGrpPolSet))
            {
                return false;
            }
        }
        if(flag || state.equals(XBConst.RNEWSTATE_CONFIRM))
        {
            if(!xbTransferData(tLCGrpPolSet))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 续保申请
     * @return boolean
     */
    private boolean xbApp(LCGrpPolSet tLCGrpPolSet)
    {
        VData tVData = new VData();
        tVData.add(this.mGlobalInput);
        tVData.add(tLCGrpPolSet);

        GRnewContAppBL tGRnewContAppBL = new GRnewContAppBL();
        if(!tGRnewContAppBL.submitData(tVData, "MJ"))
        {
            mErrors.copyAllErrors(tGRnewContAppBL.mErrors);
            return false;
        }

        return true;
    }

    private boolean xbSign(LCGrpPolSet tLCGrpPolSet)
    {
        VData tVData = new VData();
        tVData.add(tLCGrpPolSet);
        tVData.add(this.mGlobalInput);
        tVData.add(this.mTransferData);

        GRnewContSignBL tGRnewContSignBL = new GRnewContSignBL();
        if(!tGRnewContSignBL.submitData(tVData, ""))
        {
            mErrors.copyAllErrors(tGRnewContSignBL.mErrors);
            return false;
        }

        return true;
    }

    private boolean xbTransferData(LCGrpPolSet tLCGrpPolSet)
    {
        LCGrpContSchema schema = new LCGrpContSchema();
        schema.setGrpContNo(tLCGrpPolSet.get(1).getGrpContNo());
        TransferData tf = new TransferData();
        tf.setNameAndValue("EdorNo", mLPGrpContSchema.getEdorNo());

        VData data = new VData();
        data.add(this.mGlobalInput);
        data.add(schema);
        data.add(tf);

        TransferCGDataToBG tTransferCGDataToBG = new TransferCGDataToBG();
        if(!tTransferCGDataToBG.submitData(data, ""))
        {
            mErrors.copyAllErrors(tTransferCGDataToBG.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {

        GEdorMJXBBL bl = new GEdorMJXBBL();
    }
}
