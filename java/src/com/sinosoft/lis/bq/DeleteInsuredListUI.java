package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:被保险人资料变更功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */

public class DeleteInsuredListUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    public DeleteInsuredListUI()
    {}

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        DeleteInsuredListBL tDeleteInsuredListBL = new DeleteInsuredListBL();
        if (!tDeleteInsuredListBL.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(tDeleteInsuredListBL.mErrors);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "endor";

        LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
        tLCInsuredListSchema.setGrpContNo("0000008002");
        tLCInsuredListSchema.setInsuredID("2");
        LCInsuredListSet tLCInsuredListSet = new LCInsuredListSet();
        tLCInsuredListSet.add(tLCInsuredListSchema);

        //输入参数
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tLCInsuredListSet);

        DeleteInsuredListUI tDeleteInsuredListUI = new DeleteInsuredListUI();
        if (!tDeleteInsuredListUI.submitData(tVData, "INVALID"))
        {
        }
    }
}
