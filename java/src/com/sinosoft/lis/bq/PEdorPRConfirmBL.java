package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 1.0
 */
public class PEdorPRConfirmBL implements EdorConfirm {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private MMap mMap = new MMap();

    private LPEdorItemSchema mLPEdorItemSchema = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();


    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        
        if(!dealData()){
        	return false;
        }

        //数据准备操作（preparedata())
        if (!prepareData()) {
            return false;
        }

        return true;
    }

    /**
     * 
     *解决在保全确认之前因客户联系地址信息有新增记录而导致保全确认失败的问题 
     * 
     */
    private boolean dealData() {
    
       LPAddressDB tLPAddressDB = new LPAddressDB();
       tLPAddressDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
       tLPAddressDB.setEdorType(mLPEdorItemSchema.getEdorType());
       LPAddressSet tLPAddressSet = tLPAddressDB.query();
       if(tLPAddressSet.size()==0){
    	   System.out.println("没有查询到保单联系地址保全信息");
    	   return true;
       }
       LPAddressSchema tLPAddressSchema=tLPAddressSet.get(1);
       
       LPAppntDB tLPAppntDB=new LPAppntDB();
       tLPAppntDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
       tLPAppntDB.setEdorType(mLPEdorItemSchema.getEdorType());
       LPAppntSet tLpAppntSet=tLPAppntDB.query();
       if(tLpAppntSet.size()==0){
    	   System.out.println("没有查询到投保人保全信息");
    	   return true;
       }
       LPAppntSchema tLPAppntSchema=tLpAppntSet.get(1);
        
    	ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String sql = "  Select Case When max(int(AddressNo)) Is Null Then 1 "
                     + "Else max(int(AddressNo))+1  End "
                     + "from LCAddress "
                     + "where CustomerNo='"
                     + tLPAppntSchema.getAppntNo() + "'";
        tSSRS = tExeSQL.execSQL(sql);
        String addressNo=tSSRS.GetText(1, 1);
    	System.out.print("原地址码为:"+tLPAddressSchema.getAddressNo()+",当前的地址码应为:"+addressNo);
        if(!tLPAddressSchema.getAddressNo().equals(addressNo)){
        	String strAddress="update lpaddress set addressno='"+addressNo+"',"
        					+" modifydate=current date,modifytime=current time " 
        					+" where edorno='"+mLPEdorItemSchema.getEdorNo()+"' " 
        					+" and customerno='"+tLPAddressSchema.getCustomerNo()+"' "
        					+" and addressno='"+tLPAddressSchema.getAddressNo()+"' ";
        	
        	tLPAppntSchema.setAddressNo(addressNo);
        	tLPAppntSchema.setModifyDate(PubFun.getCurrentDate());
        	tLPAppntSchema.setModifyTime(PubFun.getCurrentTime());
        	
        	//需要立即更新数据
        	MMap tmMap=new MMap();
        	tmMap.put(strAddress, "UPDATE");
        	tmMap.put(tLPAppntSchema, "UPDATE");
        	VData tVData=new VData();
        	tVData.add(tmMap);
        	PubSubmit tPubSubmit = new PubSubmit();
    		if (!tPubSubmit.submitData(tVData, "")) {
    			mErrors.addOneError(new CError("PubSubmit:更新保全联系地址信息失败!"));
    		}
        }
        
		return true;
	}


	public VData getResult() {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mLPEdorItemSchema == null || mGlobalInput == null) {
            mErrors.addOneError(new CError("传入数据不完全！"));
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        tLPEdorItemDB.setPolNo(mLPEdorItemSchema.getPolNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1) {
            mErrors.addOneError(new CError("查询批改项目信息失败！"));
            return false;
        }
        mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));
        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData() {

        String[] tables = {"LCCont", "LCPol", "LCAppnt", "LCGet", "LCPrem",
                          "LCInsured"};
        String contNo = mLPEdorItemSchema.getContNo();
        String edorNo = mLPEdorItemSchema.getEdorNo();
        String edorType = mLPEdorItemSchema.getEdorType();
        ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput, edorNo,
                edorType, contNo, "ContNo");
        validate.changeData(tables);
        mMap.add(validate.getMap());
        LPAddressDB tLPAddressDB = new LPAddressDB();
        tLPAddressDB.setEdorNo(edorNo);
        tLPAddressDB.setEdorType(edorType);
        LPAddressSet tLPAddressSet = tLPAddressDB.query();
        LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        Reflections tRef = new Reflections();
        if (tLPAddressSet.size() > 0) {
            tRef.transFields(tLCAddressSchema, tLPAddressSet.get(1));
            tLCAddressSchema.setMakeDate(PubFun.getCurrentDate());
            tLCAddressSchema.setMakeTime(PubFun.getCurrentTime());
            tLCAddressSchema.setModifyDate(PubFun.getCurrentDate());
            tLCAddressSchema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(tLCAddressSchema, "INSERT");
        }

        //生成业务员归属信息
        //将目前有效的分配信息设置为无效
        String sql = " update LAAscription set ValidFlag='Y',modifydate='"
                     + PubFun.getCurrentDate() + "',modifytime='"
                     + PubFun.getCurrentTime() + "',operator='"
                     + this.mGlobalInput.Operator
                     + "' where ValidFlag='N' and contno ='" + contNo + "'"
                     ;
        mMap.put(sql, "UPDATE");
        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setEdorNo(edorNo);
        tLPContDB.setEdorType(edorType);
        tLPContDB.setContNo(contNo);
        if (!tLPContDB.getInfo()) {
            mErrors.addOneError(new CError("查询批改保单表失败！"));
            return false;
        }
        //添加对于万能帐户表管理机构的修改 add by xp 10-08-17
        String tManageCom=tLPContDB.getManageCom();
        String sqlacc = " update LCInsureAcc set ManageCom='"+tManageCom+"',modifydate='"
            + PubFun.getCurrentDate() + "',modifytime='"
            + PubFun.getCurrentTime() + "',operator='"
            + this.mGlobalInput.Operator
            + "' where  contno ='" + contNo + "'"
            ;
        mMap.put(sqlacc, "UPDATE");
        String sqlaccclass = " update LCInsureAccClass set ManageCom='"+tManageCom+"',modifydate='"
        	+ PubFun.getCurrentDate() + "',modifytime='"
        	+ PubFun.getCurrentTime() + "',operator='"
        	+ this.mGlobalInput.Operator
        	+ "' where  contno ='" + contNo + "'"
        	;
        mMap.put(sqlaccclass, "UPDATE");
        String sqlaccfee = " update LCInsureAccFee set ManageCom='"+tManageCom+"',modifydate='"
    		+ PubFun.getCurrentDate() + "',modifytime='"
    		+ PubFun.getCurrentTime() + "',operator='"
    		+ this.mGlobalInput.Operator
    		+ "' where  contno ='" + contNo + "'"
    		;
        mMap.put(sqlaccfee, "UPDATE");
        String sqlfeeclass = " update LCInsureAccClassFee set ManageCom='"+tManageCom+"',modifydate='"
    		+ PubFun.getCurrentDate() + "',modifytime='"
    		+ PubFun.getCurrentTime() + "',operator='"
    		+ this.mGlobalInput.Operator
    		+ "' where  contno ='" + contNo + "'"
    		;
        mMap.put(sqlfeeclass, "UPDATE");
        
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(contNo);
        if (!tLCContDB.getInfo()) {
            mErrors.addOneError(new CError("查询保单表失败！"));
            return false;
        }
        LAAgentDB oldAgent = new LAAgentDB();
        oldAgent.setAgentCode(tLCContDB.getAgentCode());
        if (!oldAgent.getInfo()) {
            mErrors.addOneError(new CError("原业务员查询失败！"));
            return false;
        }
        String countSql = "select count(1)+1 from LAAscription where contno ='" +
                          contNo + "'";
        String count = new ExeSQL().getOneValue(countSql);
        LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
        tLAAscriptionSchema.setAscripNo(edorNo);
        tLAAscriptionSchema.setAClass("21"); //保单迁移
        tLAAscriptionSchema.setValidFlag("N");
        tLAAscriptionSchema.setAscripState("3");
        tLAAscriptionSchema.setContNo(contNo);
        tLAAscriptionSchema.setAscriptionCount(count);
        tLAAscriptionSchema.setAgentOld(tLCContDB.getAgentCode());
        tLAAscriptionSchema.setAgentNew(tLPContDB.getAgentCode());
        tLAAscriptionSchema.setAscriptionDate(PubFun.getCurrentDate());
        tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
        tLAAscriptionSchema.setMakeDate(PubFun.getCurrentDate());
        tLAAscriptionSchema.setMakeTime(PubFun.getCurrentTime());
        tLAAscriptionSchema.setModifyDate(PubFun.getCurrentDate());
        tLAAscriptionSchema.setModifyTime(PubFun.getCurrentTime());
        tLAAscriptionSchema.setManageCom(tLCContDB.getManageCom());
        String tSQL = "select a.agentgroup,a.branchattr from labranchgroup a,laagent b where a.agentgroup=b.agentgroup and b.agentcode='"
                      + tLCContDB.getAgentCode() + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        tLAAscriptionSchema.setAgentGroup(tSSRS.GetText(1, 1));
        tLAAscriptionSchema.setBranchAttr(tSSRS.GetText(1, 2));
        tLAAscriptionSchema.setBranchType(oldAgent.getBranchType());
        tLAAscriptionSchema.setBranchType2(oldAgent.getBranchType2());
        tLAAscriptionSchema.setMakeType("10");
        mMap.put(tLAAscriptionSchema, "INSERT");
        dealGuErDan();
        
        mResult.add(mMap);
        return true;
    }
    /**
     * 孤儿单备份后删除
     */
    private boolean dealGuErDan()
    {
    	LAOrphanPolicyDB tLAOrphanPolicyDB= new LAOrphanPolicyDB();
    	tLAOrphanPolicyDB.setContNo(mLPEdorItemSchema.getContNo());
    	LAOrphanPolicySet tLAOrphanPolicySet=tLAOrphanPolicyDB.query();
    	if(tLAOrphanPolicySet.size()>0)
    	{
    		LAOrphanPolicySchema tLAOrphanPolicySchema=tLAOrphanPolicySet.get(1);
        	
        	LAOrphanPolicyBSchema tLAOrphanPolicyBSchema=new LAOrphanPolicyBSchema();
        	tLAOrphanPolicyBSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
        	tLAOrphanPolicyBSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        	tLAOrphanPolicyBSchema.setContNo(tLAOrphanPolicySchema.getContNo());
        	tLAOrphanPolicyBSchema.setAgentCode(tLAOrphanPolicySchema.getAgentCode());
        	tLAOrphanPolicyBSchema.setAgentGroup(tLAOrphanPolicySchema.getAgentGroup());
        	tLAOrphanPolicyBSchema.setAppntNo(tLAOrphanPolicySchema.getAppntNo());
        	tLAOrphanPolicyBSchema.setBranchType(tLAOrphanPolicySchema.getBranchType());
        	tLAOrphanPolicyBSchema.setBranchType2(tLAOrphanPolicySchema.getBranchType2());
        	tLAOrphanPolicyBSchema.setFlag(tLAOrphanPolicySchema.getFlag());
        	tLAOrphanPolicyBSchema.setMakeDate(tLAOrphanPolicySchema.getMakeDate());
        	tLAOrphanPolicyBSchema.setMakeTime(tLAOrphanPolicySchema.getMakeTime());
        	tLAOrphanPolicyBSchema.setManageCom(tLAOrphanPolicySchema.getManageCom());
        	tLAOrphanPolicyBSchema.setModifyDate(tLAOrphanPolicySchema.getModifyDate());
        	tLAOrphanPolicyBSchema.setModifyTime(tLAOrphanPolicySchema.getModifyTime());
        	tLAOrphanPolicyBSchema.setOperator(tLAOrphanPolicySchema.getOperator());
        	tLAOrphanPolicyBSchema.setReasonType(tLAOrphanPolicySchema.getReasonType());
        	tLAOrphanPolicyBSchema.setT1(tLAOrphanPolicySchema.getT1());
        	tLAOrphanPolicyBSchema.setT2(tLAOrphanPolicySchema.getT2());
        	tLAOrphanPolicyBSchema.setZipCode(tLAOrphanPolicySchema.getZipCode());
        	mMap.put(tLAOrphanPolicyBSchema, "DELETE&INSERT");
        	mMap.put(tLAOrphanPolicySchema, "DELETE");
    	}
    	   	
    	return true;
    }
    
    
}
