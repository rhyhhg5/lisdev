/*
 * <p>ClassName: OLCGrpEdorUI </p>
 * <p>Description: OLCGrpEdorUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-02-23 09:59:56
 */
package com.sinosoft.lis.bq;
import com.sinosoft.lis.schema.LCGrpEdorSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
public class LCGrpEdorDealUI {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData =new VData();
/** 数据操作字符串 */
private String mOperate;
//业务处理相关变量
 /** 全局数据 */
private LCGrpEdorSchema mLCGrpEdorSchema=new LCGrpEdorSchema();
public LCGrpEdorDealUI ()
{
}
 /**
传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
  //将操作数据拷贝到本类中
  this.mOperate =cOperate;
  
  System.out.println("Start OLCGrpEdor UI Submit...");

  LCGrpEdorDealBL tOLCGrpEdorBL=new LCGrpEdorDealBL();


  tOLCGrpEdorBL.submitData(cInputData,mOperate);
  System.out.println("End OLCGrpEdor UI Submit...");
  //如果有需要处理的错误，则返回
  if (tOLCGrpEdorBL.mErrors.needDealError() )
  {
     // @@错误处理
     this.mErrors.copyAllErrors(tOLCGrpEdorBL.mErrors);
     CError tError = new CError();
     tError.moduleName = "OLCGrpEdorUI";
     tError.functionName = "submitData";
     tError.errorMessage = "数据提交失败!";
     this.mErrors .addOneError(tError) ;
     return false;
  }
  if (mOperate.equals("INSERT||MAIN"))
  {
     this.mResult.clear();
     this.mResult=tOLCGrpEdorBL.getResult();
  }
  mInputData=null;
  return true;
  }
  public static void main(String[] args)
  {
  }

public VData getResult()
{
  return this.mResult;
}
}
