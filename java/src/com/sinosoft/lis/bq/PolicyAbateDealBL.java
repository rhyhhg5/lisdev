package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.operfee.IndiLJSCancelBL;
import com.sinosoft.lis.operfee.IndiLJSCancelUI;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PolicyAbateDealBL 
{
    private GlobalInput mG = new GlobalInput();
    private String mManageCom = "";
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    private LLCaseCommon mLLCaseCommon = new LLCaseCommon();
    private String mGrpContNo;
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mEnd = "";
    private ExeSQL tExeSQL = new ExeSQL();
    private String mGracePeriod = "";
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private LJSPaySchema mLJSPaySchema = null;

    public PolicyAbateDealBL() {}

    private boolean getInputData(VData cInputData) 
    {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mManageCom = mG.ManageCom;
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {
        cInputData = (VData) cInputData.clone();
        //qulq add 作废失效保单的应收记录
        DeadPolicyIndiCancel tDeadPolicyIndiCancel = new DeadPolicyIndiCancel();
        if (!getInputData(cInputData)) 
        {
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("Available")) 
        {
            try
            {
                // 发现程序有时执行不到作废应收的部分,所以现在每个独立的
                // 批处理都单独try起来.
                try
                {
                      /*个单险种失效批处理*/
                     CallSingleAvailable();
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage()+"个险失效批处理出现错误!");
                }

               try
               {
                     /*团体保单失效批处理*/
                     CallGrpPause();
               }
               catch(Exception e)
               {
                    System.out.println(e.getMessage()+"团险失效批处理出现错误!");
               }

               try
               {
                    /*团体保单满未结算暂停*/
                    callGrpPauseJS();
               }
               catch(Exception e)
               {
                    System.out.println(e.getMessage()+"团体保单满未结算暂停出现错误!");
               }
               
              try
              {
            	  /*团险重疾失效批处理*/
            	  Call280101();
            	  
              }
              catch(Exception e)
              {
            	  System.out.println(e.getMessage()+"团险重疾失效批处理出现错误！");
              }
              try
              {
            	  /*万能险重疾失效批处理*/
            	  Call231001();
            	  
              }
              catch(Exception e)
              {
            	  System.out.println(e.getMessage()+"万能险重疾失效批处理出现错误！");
              }   
              try
              {
            	  /*税优保单失效*/
            	  CallTphiAvailable();
              }
              catch(Exception e)
              {
            	  System.out.println(e.getMessage()+"税优失效批处理出现错误！");
              }   
               //处理作废应收..
                tDeadPolicyIndiCancel.dealData();
            }
            catch(Exception ex)
            {
                System.out.print(ex.toString());
            }
        } 
        else if(cOperate != null && cOperate.trim().equals("Terminate")) 
        {
            /*个单险种满期终止批处理*/
            callSingleTerminate();
            /*团体保单满期终止AvailableJS*/
            callGrpTerminate();
        } 
        else 
        {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }
    
    /**
     * 税优失效中止
     */
    private void CallTphiAvailable(){
    	System.out.println("税优失效中止运行开始。。。。");

        String subSql = getSQLManageCom();
        
        //sql效率优化
        String riskSql = "select riskcode from lmriskapp where risktype4 = '4' and TaxOptimal='Y' with ur";
        String riskStr = "";
        SSRS riskSSRS= new ExeSQL().execSQL(riskSql);
        if(null!=riskSSRS && riskSSRS.getMaxRow() > 0){
        	for(int i=1; i <= riskSSRS.getMaxRow(); i++){
        		riskStr +="'"+riskSSRS.GetText(i, 1)+"'";
        		if(i < riskSSRS.getMaxRow()){
        			riskStr +=",";
        		}
        	}
        }

        String strSql = "select * from lccont  where 1 = 1  and contno in (" 
        		    + " select contno from lcpol a where 1 = 1 "
	                + " and appflag = '1'  and conttype = '1' "  //只处理已签单个单
	                + " and StateFlag not in ('2', '3') " //未失效、未满期
	                + " and payintv <> 0 "  //趸缴不存在未缴费失效情况
	                + " and polno = mainpolno  "
	                + " and paytodate + 60 days <= '" + mCurrentDate + "' "
	                + " and riskcode in ("+ riskStr +") "
	                + subSql 
	                + " and exists (select 1 from  lccont where contno=a.contno) "
	                + " ) "
	                + " with ur";
        			
        System.out.println(strSql);

        LCContSet tLCContSet = new LCContSet();
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLCContSet,strSql);
        do {
            rswrapper.getData();
            //循环取得5000条数据，进行处理
            for(int i=1;i<=tLCContSet.size();i++)
            {
            	LCContSet aLCContSet=new LCContSet();
            	aLCContSet.add(tLCContSet.get(i));
            	//逐条调用保单失效处理
            	if (SinglePolDeal(aLCContSet)) 
            	{
            		System.out.println("个单险种失效处理成功");
            	}
            }
        }while (tLCContSet.size() > 0);

        /*
         *根据保单下险种失效情况，设置保单的失效状态：若所有险种失效，则整单失效
         */
//        System.out.println("税优保单失效开始……");
//
//        String strPolicySql ="select * from lccont a where 1 = 1 "
//		                + " and appflag = '1'  and conttype = '1' "
//		                + " and not exists(select 'X' from lccontstate b where b.statetype in ('Available','Terminate') and b.state = '1' "
//		                + " and b.contno = a.contno and b.polno = '000000' ) "
//		                + " and payintv <> 0 "
//		                + " and StateFlag not in ('2', '3') "
//		                + " and paytodate < '" + mCurrentDate + "' "
//		                + " and exists (select 1 from lcpol where contno=a.contno and appflag='1' and riskcode in ("+riskStr+") )"
//		                + " and not exists (select 1 from lcpol where contno=a.contno and appflag='1' and stateflag not in ('2','3') )"
//		                + subSql
//		                + " with ur ";
//        System.out.println(strPolicySql);
//        LCContSet tLCContSet = new LCContSet();
//        RSWrapper rswrappercont = new RSWrapper();
//        rswrappercont.prepareData(tLCContSet,strPolicySql);
//        do {
//        	rswrappercont.getData();
//            //循环取得5000条数据，进行处理
//            for(int i=1;i<=tLCContSet.size();i++)
//            {
//            	LCContSet aLCContSet=new LCContSet();
//            	aLCContSet.add(tLCContSet.get(i));
//            	if (SinglePolicyDeal(aLCContSet)) 
//            	{
//            		System.out.println("个单保单失效处理成功");
//            	}
//            }
//        }while (tLCContSet.size() > 0);
    }

    /**
     * 团单未结算暂停
     */
    private void callGrpPauseJS() 
    {
        String subSql = getSQLManageCom();
        String strSql =
                "select * from LJSPay a where 1 = 1 "
                + "   and otherNoType = '13' "
                + "   and payDate < current Date "
                + "   and not exists "
                + "      (select 'X' from lcgrpcontstate b "
                +
                "      where b.statetype in ('Pause','Terminate') and b.state = '1' "
                + "         and b.enddate is null and b.grpcontno = "
                +
                "             (select grpContNo from LPGrpEdorItem where edorNo = a.otherNo)) "
                + subSql + " with ur";
        System.out.println(strSql);
        LJSPaySet tLJSPaySet = new LJSPayDB().executeQuery(strSql);
        if (tLJSPaySet.size() == 0) 
        {
            return;
        }

        MMap tMMap = new MMap();

        for (int i = 1; i <= tLJSPaySet.size(); i++) 
        {
            LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i);

            String sql = "select distinct a.* "
                         + "from LCGrpCont a, LPGrpEdorItem b "
                         + "where a.grpContNo = b.grpContNo "
                         + "   and b.edorNo = '"
                         + tLJSPaySchema.getOtherNo() + "' ";
            System.out.println(sql);
            LCGrpContSet tLCGrpContSet = new LCGrpContDB().executeQuery(sql);
            if (tLCGrpContSet.size() == 0) 
            {
                continue;
            }
            LCGrpContSchema tLCGrpContSchema = tLCGrpContSet.get(1);

            LGWorkDB tLGWorkDB = new LGWorkDB();
            tLGWorkDB.setWorkNo(tLJSPaySchema.getOtherNo());
            if (!tLGWorkDB.getInfo()) 
            {
                mErrors.addOneError("没有查询到定期结算工单" + tLGWorkDB.getWorkNo());
                continue;
            }

            LCGrpContStateSchema tLCGrpContStateSchema = new LCGrpContStateSchema();

            tLCGrpContStateSchema.setGrpContNo(tLCGrpContSchema.getGrpContNo());
            tLCGrpContStateSchema.setGrpPolNo("000000");
            tLCGrpContStateSchema.setStateType("Pause");
            tLCGrpContStateSchema.setStateReason("11");
            tLCGrpContStateSchema.setState("1");
            tLCGrpContStateSchema.setOtherNo(tLJSPaySchema.getGetNoticeNo());
            tLCGrpContStateSchema.setOtherNoType(tLJSPaySchema.getOtherNoType());
            tLCGrpContStateSchema.setOperator("000");
            tLCGrpContStateSchema.setStartDate(tLJSPaySchema.getPayDate());
            tLCGrpContStateSchema.setMakeDate(PubFun.getCurrentDate());
            tLCGrpContStateSchema.setMakeTime(PubFun.getCurrentTime());
            tLCGrpContStateSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGrpContStateSchema.setModifyTime(PubFun.getCurrentTime());
            tLCGrpContStateSchema.setContPlanCode("00");
            tMMap.put(tLCGrpContStateSchema, "DELETE&INSERT");
            SetGrpContState(tLCGrpContSchema, "2");
//            String tStartDate = PubFun.calDate(mEnd, 1, "D", null);
            String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerSchema.setPrtSeq(serNo);
            tLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
            tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //个人保单号
            tLOPRTManagerSchema.setCode("58"); //58为未结算暂停通知书的代码
            tLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
            tLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setReqOperator("000");
            tLOPRTManagerSchema.setPrtType("0"); //前台打印
            tLOPRTManagerSchema.setStateFlag("0"); //提交打印
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            tLOPRTManagerSchema.setStandbyFlag1(tLCGrpContStateSchema.getStartDate()); //失效日期
            tLOPRTManagerSchema.setStandbyFlag2(tLJSPaySchema.getPayDate());
            tLOPRTManagerSchema.setStandbyFlag3(tLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSchema.setStandbyFlag4(tLJSPaySchema.getOtherNoType());
            tMMap.put(tLOPRTManagerSchema, "DELETE&INSERT");

            System.out.println(tLJSPaySchema.getStartPayDate() + " " + tLJSPaySchema.getPayDate());
        }

        VData data = new VData();
        data.add(tMMap);

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) 
        {
            CError tError = new CError();
            tError.moduleName = "PolicyAbateDealBL";
            tError.functionName = "callGrpPauseJS";
            tError.errorMessage = tPubSubmit.mErrors.getErrContent();
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
        }
    }

    /***************************个单未交费失效处理区域*******************************/
    /*个单险种失效批处理*/
    private boolean CallSingleAvailable() 
    {
        System.out.println("个单险种失效批处理运行开始。。。。");

        String subSql = getSQLManageCom();

        String strSql =
        //主险       
        		"select * from lccont  where 1 = 1  and contno in (" +
                " select contno from lcpol a where 1 = 1 " +
                " and StateFlag not in('2', '3') " +  //未失效、未满期
                " and payintv <> 0 " +  //趸缴不存在未缴费失效情况
                " and appflag = '1'  and conttype = '1' " +  //只处理已签单个单
                " and polno = mainpolno  " +
                " and paytodate < '" + mCurrentDate + "' " +  //对宽限期的计算在后续程序中考虑
                " and (paytodate + nvl((select max(GracePeriod) from LMRiskPay where riskcode = a.riskcode),0) days)  <= '" + mCurrentDate + "' " +
                " and riskcode not in (select riskcode from lmriskapp where risktype4 = '4') " +
                " and ((riskcode not in (select riskcode from lmriskapp where riskperiod='L')) or paytodate<>payenddate) "+
                " and riskcode not in (select riskcode from lmriskpay where graceperiod is null) "+
                " and riskcode not in (select code from ldcode1 where exists (select 1 from lmriskapp where riskcode = ldcode1.code1 and risktype4= '4'))" +
                subSql +
                " ) " +
                " and not exists (select 1 from ldcode where codetype='DSSX' and (code = lccont.agentcode or code = lccont.agentcom) and lccont.salechnl=codealias) "+
                " with ur";
                
                System.out.println(strSql);
                

                LCContSet tLCContSet = new LCContSet();
                RSWrapper rswrapper = new RSWrapper();
                rswrapper.prepareData(tLCContSet,strSql);
                System.out.println("总条数："+tLCContSet.size());
                ExeSQL tExeSQL = new ExeSQL();
                int count = 1;
                do {
                	String time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                	String insertSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+time+"','第"+count+"次取数开始','1','1') ";
                	tExeSQL.execUpdateSQL(insertSQL);
                    rswrapper.getData();
                    System.out.println("第"+count+"次取数条数为："+tLCContSet.size());
                    time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                    insertSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+time+"','第"+count+"次取数结束','2','1') ";
                    tExeSQL.execUpdateSQL(insertSQL);
                    //循环取得5000条数据，进行处理
                    for(int i=1;i<=tLCContSet.size();i++)
                    {
                    	LCContSet aLCContSet=new LCContSet();
                    	aLCContSet.add(tLCContSet.get(i));
                    	//逐条调用保单失效处理
                    	if (SinglePolDeal(aLCContSet)) 
                    	{
                    		System.out.println("个单险种失效处理成功");
                    	}

                    }
                    count ++;
                }while (tLCContSet.size() > 0);
                String polEndTime = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                String insertPolEndSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+polEndTime+"','所有主险失效处理结束','3','1') ";
                tExeSQL.execUpdateSQL(insertPolEndSQL);
                
                
                
                //附加险
                strSql =
                "select * from lccont  where 1 = 1  and contno in (" +
                " select contno from lcpol c where 1 = 1 " +
                " and StateFlag not in('2', '3') " +  //未失效、未满期
                " and polno <> mainpolno " +
                " and exists(select 'X' from lcpol where polno = mainpolno and payintv != 0 and appflag = '1' and polno = c.mainpolno " +
                subSql + " ) " +
                " and appflag = '1' and conttype = '1' " +
                " and paytodate < '" + mCurrentDate + "' " +
                " and (paytodate + nvl((select max(GracePeriod) from LMRiskPay where riskcode = c.riskcode),0) days)  <= '" + mCurrentDate + "' " +
                " and riskcode not in (select riskcode from lmriskpay where graceperiod is null) "+
                " and riskcode not in (select riskcode from lmriskapp where risktype4 = '4' ) " +
                " and ((riskcode not in (select riskcode from lmriskapp where riskperiod='L')) or paytodate<>payenddate) "+
                " and riskcode not in (select code from ldcode1 where exists (select 1 from lmriskapp where riskcode = ldcode1.code1 and risktype4= '4'))" +
                subSql + 
                " ) " +
                " and not exists (select 1 from ldcode where codetype='DSSX' and (code = lccont.agentcode or code = lccont.agentcom) and lccont.salechnl=codealias) "+
                " with ur";
                
                System.out.println(strSql);
                

                tLCContSet = new LCContSet();
                rswrapper = new RSWrapper();
                rswrapper.prepareData(tLCContSet,strSql);
                System.out.println("总条数："+tLCContSet.size());
                do {
                	String time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                	String insertSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+time+"','第"+count+"次取数开始','1','2') ";
                	tExeSQL.execUpdateSQL(insertSQL);
                    rswrapper.getData();
                    System.out.println("第"+count+"次取数条数为："+tLCContSet.size());
                    time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                    insertSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+time+"','第"+count+"次取数结束','2','2') ";
                    tExeSQL.execUpdateSQL(insertSQL);
                    //循环取得5000条数据，进行处理
                    for(int i=1;i<=tLCContSet.size();i++)
                    {
                    	LCContSet aLCContSet=new LCContSet();
                    	aLCContSet.add(tLCContSet.get(i));
                    	//逐条调用保单失效处理
                    	if (SinglePolDeal(aLCContSet)) 
                    	{
                    		System.out.println("个单险种失效处理成功");
                    	}
                    }
                    count ++;
                }while (tLCContSet.size() > 0);
                polEndTime = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                insertPolEndSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+polEndTime+"','所有附加险失效处理结束','3','2') ";
                tExeSQL.execUpdateSQL(insertPolEndSQL);
                
                
                //20081203 zhanggm 增加短期险趸缴失效 cbs00021277                
                strSql =
                	"select * from lccont  where 1 = 1  and contno in (" 	
                	+ " select a.contno from lcpol a,lmriskapp app "
                	+ " where 1 = 1 "
                	+ " and appflag = '1'and conttype = '1' "
                	+ " and a.riskcode = app.riskcode "
                	+ " and app.riskperiod != 'L' "
                	+ " and app.risktype4 <> '4' "
                	+ " and StateFlag not in ('2', '3') "
                	+ " and payintv = 0 "
                	+ " and paytodate <'" + mCurrentDate + "'"
                	+ " and (paytodate + nvl((select max(GracePeriod) from LMRiskPay where riskcode = a.riskcode),0) days)<='" + mCurrentDate + "' "
                	+ " and not exists (select 1 from lmriskpay where riskcode = a.riskcode and graceperiod is null)"
                	+ " and a.riskcode not in (select code from ldcode1 where app.riskcode = ldcode1.code1 and app.risktype4= '4' and ldcode1.codetype in ('mainsubriskrela','checkappendrisk'))"
                	+ subSql 
                	+ " ) " 
                	+ " and not exists (select 1 from ldcode where codetype='DSSX' and (code = lccont.agentcode or code = lccont.agentcom) and lccont.salechnl=codealias) "
                	+ " with ur";
                
                
                System.out.println(strSql);
                

                tLCContSet = new LCContSet();
                rswrapper = new RSWrapper();
                rswrapper.prepareData(tLCContSet,strSql);
                System.out.println("总条数："+tLCContSet.size());
                do {
                	String time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                	String insertSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+time+"','第"+count+"次取数开始','1','3') ";
                	tExeSQL.execUpdateSQL(insertSQL);
                    rswrapper.getData();
                    System.out.println("第"+count+"次取数条数为："+tLCContSet.size());
                    time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                    insertSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+time+"','第"+count+"次取数结束','2','3') ";
                    tExeSQL.execUpdateSQL(insertSQL);
                    //循环取得5000条数据，进行处理
                    for(int i=1;i<=tLCContSet.size();i++)
                    {
                    	LCContSet aLCContSet=new LCContSet();
                    	aLCContSet.add(tLCContSet.get(i));
                    	//逐条调用保单失效处理
                    	if (SinglePolDeal(aLCContSet)) 
                    	{
                    		System.out.println("个单险种失效处理成功");
                    	}
                    }
                    count ++;
                }while (tLCContSet.size() > 0);
                polEndTime = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                insertPolEndSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+polEndTime+"','所有趸交短期险失效处理结束','3','3') ";
                tExeSQL.execUpdateSQL(insertPolEndSQL);
                
                
                
                    //附加万能险种失效处理
                strSql =
                	"select * from lccont  where 1 = 1  and contno in (" +
                    " select contno from lcpol c where 1 = 1 " +
                    " and StateFlag not in('2', '3') " +  //未失效、未满期
                    " and appflag = '1' and conttype = '1' " +
                    " and paytodate < '" + mCurrentDate + "' " +
                    " and (paytodate + nvl((select max(GracePeriod) from LMRiskPay where riskcode = c.riskcode),0) days)  <= '" + mCurrentDate + "' " +
                    " and riskcode not in (select riskcode from lmriskpay where graceperiod is null) "+
                    " and riskcode  in (select riskcode from lmriskapp where risktype4 = '4' and subriskflag='S' ) " +
                    " and paytodate<payenddate "+
                    subSql + 
                    " ) " +
                    "with ur";
                System.out.println(strSql);
                

                tLCContSet = new LCContSet();
                rswrapper = new RSWrapper();
                rswrapper.prepareData(tLCContSet,strSql);
                System.out.println("总条数："+tLCContSet.size());
                do {
                	String time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                	String insertSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+time+"','第"+count+"次取数开始','1','4') ";
                	tExeSQL.execUpdateSQL(insertSQL);
                    rswrapper.getData();
                    System.out.println("第"+count+"次取数条数为："+tLCContSet.size());
                    time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                    insertSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+time+"','第"+count+"次取数结束','2','4') ";
                    tExeSQL.execUpdateSQL(insertSQL);
                    //循环取得5000条数据，进行处理
                    for(int i=1;i<=tLCContSet.size();i++)
                    {
                    	LCContSet aLCContSet=new LCContSet();
                    	aLCContSet.add(tLCContSet.get(i));
                    	//逐条调用保单失效处理
                    	if (SinglePolDeal(aLCContSet)) 
                    	{
                    		System.out.println("个单险种失效处理成功");
                    	}
                    }
                    count ++;
                }while (tLCContSet.size() > 0);
                polEndTime = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
                insertPolEndSQL = "INSERT INTO ldtimetest VALUES ('bqpol',current time,'"+polEndTime+"','所有附加万能险失效处理结束','3','4') ";
                tExeSQL.execUpdateSQL(insertPolEndSQL);	


        /*个单保单失效批处理
         根据保单下险种失效情况，设置保单的失效状态：若所有险种失效，则整单失效
         */
//        System.out.println("个单保单失效批处理开始……");
//
//        String strPolicySql =
//                "select * from lccont a where 1 = 1 " +
//                "and not exists(select 'X' from lccontstate b where b.statetype in ('Available','Terminate') and b.state = '1' " +
//                "and b.contno = a.contno and b.polno = '000000' ) " +
//                "and payintv <> 0 " +
//                "and appflag = '1'  and conttype = '1' " +
//                "and StateFlag not in ('2', '3') " +
//                "and paytodate < '" + mCurrentDate + "' " + subSql +
//                "and not exists (select 1 from lcpol pol where a.contno = pol.contno and stateflag = '1')"+
//                "union " +
//                "select * from lccont c where 1 = 1 " +
//                "and appflag = '1' and conttype = '1' " +
//                "and paytodate < '" + mCurrentDate + "' " +
//                "and StateFlag not in ('2', '3')" +
//                subSql +
//                "and not exists (select 1 from lcpol pol where c.contno = pol.contno and stateflag = '1')"+
//                " with ur ";
//        System.out.println(strPolicySql);
//        LCContSet tLCContSet = new LCContSet();
//        RSWrapper rswrappercont = new RSWrapper();
//        rswrappercont.prepareData(tLCContSet,strPolicySql);
//        count = 1;
//        do {
//        	String time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
//        	String insertSQL = "INSERT INTO ldtimetest VALUES ('bqcont',current time,'"+time+"','第"+count+"次取数开始','1',NULL) ";
//        	tExeSQL.execUpdateSQL(insertSQL);
//        	rswrappercont.getData();
//            time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
//            insertSQL = "INSERT INTO ldtimetest VALUES ('bqcont',current time,'"+time+"','第"+count+"次取数结束','2',NULL) ";
//            tExeSQL.execUpdateSQL(insertSQL);
//            //循环取得5000条数据，进行处理
//            for(int i=1;i<=tLCContSet.size();i++)
//            {
//            	LCContSet aLCContSet=new LCContSet();
//            	aLCContSet.add(tLCContSet.get(i));
//            	if (SinglePolicyDeal(aLCContSet)) 
//            	{
//            		System.out.println("个单保单失效处理成功");
//            	}
//            }
//            count ++;
//        }while (tLCContSet.size() > 0);
//        String contEndTime = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
//        String insertContEndSQL = "INSERT INTO ldtimetest VALUES ('bqcont',current time,'"+contEndTime+"','所有保单失效处理结束','3',NULL) ";
//        tExeSQL.execUpdateSQL(insertContEndSQL);
        return true;
    }

    /*设置个单险种失效状态*/
    private boolean SinglePolDeal(LCContSet aLCContSet) 
    {
        for (int i = 1; i <= aLCContSet.size(); i++) 
        {
        	LCContSchema aLCContSchema = new LCContSchema();
            
            aLCContSchema.setSchema(aLCContSet.get(i));
            LCPolSet aLCPolSet = new LCPolSet();
            LCPolDB aLCPolDB = new LCPolDB();
            aLCPolDB.setContNo(aLCContSchema.getContNo());
            aLCPolSet=aLCPolDB.query();
            int polCount=0;
            for (int j = 1; j <= aLCPolSet.size(); j++){
            	LCPolSchema aLCPolSchema = new LCPolSchema();
            	aLCPolSchema=aLCPolSet.get(j).getSchema();
            	String aPolNo = aLCPolSchema.getPolNo();
            	String aContNo = aLCPolSchema.getContNo();
            	String contType = aLCPolSchema.getContType();//保单类型（团、个）
            	String stateflag = aLCPolSchema.getStateFlag();//保单状态
            	if(!stateflag.equals("1")){
            		System.out.println(aPolNo + "险种已失效或未生效，不进行失效中止处理");
            		continue;
            	}
            	//续期交费未核销
            	if (!booIsGetMoney("OperFee", aContNo)) {
            		System.out.println(aPolNo + "续期交费未核销");
            		continue;
            	}
            	//保全交费未核销
            	if (!booIsGetMoney("Endorse", aContNo)) {
            		System.out.println(aPolNo + "保全缴费未核销");
            		continue;
            	}

            	/*
            	 * 宽限期校验：
             	1、无续期，过了宽限期，则置失效
             	2、有续期，缴至日期和首次缴费日+宽限期(长期险60天，短期险30天)比较大的一个超过当天的，置为失效
             	20100326 zhanggm 修改校验2 缴费截至日期(ljspayb的paydate)和缴至日期+宽限期(长期险60天，短期险30天)比较大的一个超过当天的，置为失效
            	 */
            	if (!checkLapseDate(aLCPolSchema)) 
            	{
            		System.out.println(aPolNo + "未过宽限期");
            		continue;
            	}

            	/*保全状态*/
            	if (edorseState(aContNo,contType)) 
            	{
            		System.out.println(aPolNo + "在做保全项目");
            		continue;
            	}
            	/*理赔状态*/
            	if (mLLCaseCommon.getClaimState(aPolNo).equals("1") ||
            			mLLCaseCommon.getClaimState(aPolNo).equals("2")) 
            	{
             	  System.out.println(aPolNo + "在做理赔项目");
             	  continue;
            	}
            	
            	if(isUliRisk(aPolNo)){
               	  System.out.println(aPolNo + "万能险种");
               	  continue;
            	}
            	if(isPayEndDate(aPolNo)){
                 	  System.out.println(aPolNo + "缴费期间已满");
                 	  continue;
              	}
            	/*设置险种失效*/
            	setPolAbate(aLCPolSchema, aLCPolSchema.getPaytoDate(), "Available", "1", "02");
                
            	polCount++;
            }
            //保单和险种一起失效处理，若保单下所有险种均失效，则保单层失效
            String  sql=" select count(1) from LCPol where contno = '"+aLCContSchema.getContNo()+"' and stateflag in ('2','3') ";
            ExeSQL mExeSQL = new ExeSQL();
            String tPolCount = mExeSQL.getOneValue(sql.toString());
            int mpolCount=0;
            if(mExeSQL.mErrors.needDealError())
      	 	{
            	CError tError = new CError();
            	tError.moduleName = "GrpEdorWTConfirmBL";
            	tError.functionName = "getLPPolCount";
            	tError.errorMessage = "查询退保险种数出错";
            	mErrors.addOneError(tError);
            	System.out.println(tError.errorMessage);
      	 	}
            
            if(tPolCount.equals("") || tPolCount.equals("null"))
            {
            	mpolCount=0;
            }
            mpolCount=Integer.parseInt(tPolCount);
    	    
            if((polCount + mpolCount) == aLCPolSet.size()){
            	if (SinglePolicyDeal(aLCContSet)) 
            	{
            		System.out.println("个单保单失效处理成功");
            	}
            }
            
        	//保单下有险种失效，则将应收记录作废，
            if(polCount>0){
            	if (dealIndiCancel(aLCContSchema)!= null) 
            	{
            		System.out.println("个单保单续期作废处理成功");
            	}
            }
            
        	if (!SubmitMap()) 
        	{
        		return false;
        	}

            
        }
        
        return true;
    }

    /*是否已经缴满*/
    private boolean isPayEndDate(String tPolNo) {
        String strsql = "select '1' from lcpol  where payenddate <=paytodate and polno = '" +tPolNo + 
        		  "' and appflag = '1' and conttype = '1' " +
                  " and (riskcode in (select riskcode from lmriskapp where riskperiod='L'))  "+
                  " with ur " ;
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tssrs = tExeSQL.execSQL(strsql);
        if (tssrs != null && tssrs.getMaxRow() == 1) {
            return true; 
        }
        return false;
    }
    /*万能主险及附加险*/
    private boolean isUliRisk(String tPolNo) {
        String strsql = "select '1' from lcpol  where  polno = '" +tPolNo + 
        		  "' and appflag = '1' and conttype = '1' " +
                  " and (riskcode in (select riskcode from lmriskapp where risktype4='4' and subriskflag<>'S'))  "+
                  " and riskcode not in (select code from ldcode1 where exists (select 1 from lmriskapp where riskcode = ldcode1.code1 and risktype4= '4'))" +
                  " with ur " ;
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tssrs = tExeSQL.execSQL(strsql);
        if (tssrs != null && tssrs.getMaxRow() == 1) {
            return true; 
        }
        return false;
    }
    /*个单保单失效批处理*/
    private boolean SinglePolicyDeal(LCContSet tLCContSet) 
    {
        //判断保单是否存在
        for (int i = 1; i <= tLCContSet.size(); i++) 
        {
            LCContSchema mLCContSchema = new LCContSchema();
            mLCContSchema.setSchema(tLCContSet.get(i));

//            //若所有险种都终止或失效，则保单失效
//            String sql = "select count(1) from LCPol "
//                         + "where StateFlag not in('2','3')"
//                         + "   and ContNo ='" + mLCContSchema.getContNo() + "' ";
//            String countStr = tExeSQL.getOneValue(sql);
//            if(!"0".equals(countStr))
//            {
//                continue;
//            }

            if (!setPolicyAbate(mLCContSchema, mLCContSchema.getPaytoDate(), "Available", "1", "02")) 
            {
                System.out.println("保单" + mLCContSchema.getContNo() + "设置失效失败");
            } 
            else 
            {
                sendInvaliNotice(mLCContSchema, mManageCom, "42", null);
            }
                
//            if (!SubmitMap()) 
//            {
//                return false;
//            }
        }
        
        return true;
    }

    /*设置个人险种失效*/
    public boolean setPolAbate(LCPolSchema tLCPolSchema,
                               String tEndDate,
                               String tStateType,
                               String tState, String tStateReason
            ) 
    {
        if (tLCPolSchema == null || tEndDate == null ||
            tStateType == null || tState == null ||
            tEndDate.trim().equals("") ||
            tStateType.trim().equals("") ||
            tState.trim().equals("")) 
        {
        	
            return false;
        }
        //新状态起始日期为旧状态结束日期次日
        String tStartDate = PubFun.calDate(tEndDate, 1, "D", null);

        LCContStateSchema tLCContStateSchema = new LCContStateSchema();

        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(tLCPolSchema.getContNo());
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() != 0) 
        {
            tLCContStateSchema.setOtherNoType(tLJSPaySet.get(1).getOtherNoType());
            tLCContStateSchema.setOtherNo(tLJSPaySet.get(1).getGetNoticeNo());
        }

        tLCContStateSchema.setGrpContNo("000000");
        tLCContStateSchema.setContNo(tLCPolSchema.getContNo());
        tLCContStateSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
        tLCContStateSchema.setPolNo(tLCPolSchema.getPolNo());
        tLCContStateSchema.setStateType(tStateType);
        tLCContStateSchema.setStateReason(tStateReason);
        tLCContStateSchema.setState(tState);
        tLCContStateSchema.setOperator("000");
        tLCContStateSchema.setStartDate(tStartDate);
        tLCContStateSchema.setMakeDate(PubFun.getCurrentDate());
        tLCContStateSchema.setMakeTime(PubFun.getCurrentTime());
        tLCContStateSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContStateSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLCContStateSchema, "DELETE&INSERT");
        SetPolState(tLCPolSchema, "2");

        return true;
    }

    /*设置个险保单失效*/
    public boolean setPolicyAbate(LCContSchema tLCContSchema, String tEndDate,
                                  String tStateType, String tState,
                                  String tStateReason) 
    {
        if (tLCContSchema == null || tEndDate == null ||
            tStateType == null || tState == null ||
            tEndDate.trim().equals("") ||
            tStateType.trim().equals("") ||
            tState.trim().equals("")) {
            return false;
        }
        //新状态起始日期为旧状态结束日期次日
        String tStartDate = PubFun.calDate(tEndDate, 1, "D", null);
        LCContStateDB tLCContStateDB = new LCContStateDB();
        LCContStateSet tLCContStateSet = new LCContStateSet();
        LCContStateSchema oldLCContStateSchema = new LCContStateSchema();
        if (!tStateType.equals("Available"))
        {
            String strSql = " select * from lccontstate where statetype = 'Available' " +
                            " and enddate is null and contno = '" +
                            tLCContSchema.getContNo() +
                            "' and polno = '000000'";
            tLCContStateSet = tLCContStateDB.executeQuery(strSql);
            if (!tLCContStateDB.mErrors.needDealError()) 
            {
                if (tLCContStateSet != null && tLCContStateSet.size() > 0) {
                    oldLCContStateSchema = tLCContStateSet.get(1).getSchema();
                    oldLCContStateSchema.setEndDate(tEndDate);
                    oldLCContStateSchema.setStateReason(tStateReason);
                    oldLCContStateSchema.setModifyDate(PubFun.getCurrentDate());
                    oldLCContStateSchema.setModifyTime(PubFun.getCurrentTime());
                    mMap.put(oldLCContStateSchema, "DELETE&INSERT");
                }
            } 
            else 
            {
                System.out.println("保单状态查询失败");
                return false;
            }
        }
        LCContStateSchema tLCContStateSchema = new LCContStateSchema();
        tLCContStateSchema.setGrpContNo("000000");
        tLCContStateSchema.setContNo(tLCContSchema.getContNo());
        tLCContStateSchema.setInsuredNo(tLCContSchema.getInsuredNo());
        tLCContStateSchema.setPolNo("000000");
        tLCContStateSchema.setStateType(tStateType);
        tLCContStateSchema.setStateReason(tStateReason);
        
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(tLCContSchema.getContNo());
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() != 0) 
        {
            tLCContStateSchema.setOtherNoType(tLJSPaySet.get(1).getOtherNoType());
            tLCContStateSchema.setOtherNo(tLJSPaySet.get(1).getGetNoticeNo());
        }
        tLCContStateSchema.setState(tState);
        tLCContStateSchema.setOperator("000");
        tLCContStateSchema.setStartDate(tStartDate);
        tLCContStateSchema.setMakeDate(PubFun.getCurrentDate());
        tLCContStateSchema.setMakeTime(PubFun.getCurrentTime());
        tLCContStateSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContStateSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLCContStateSchema, "DELETE&INSERT");
        if (tStateType.equals("Available")) 
        {
            SetContState(tLCContSchema, "2");
        }
        if (tStateType.equals("Terminate")) 
        {
            SetLCContState(tLCContSchema, "3", mMap);
        }
        return true;
    }

    //发失效通知书
    private boolean sendInvaliNotice(LCContSchema tLCContSchema, String tCom,
                                     String tCode, String tPayDate) 
    {
        if(tPayDate == null)
        {
            LJSPayDB tLJSPayDB = new LJSPayDB();
            tLJSPayDB.setOtherNo(tLCContSchema.getContNo());
            LJSPaySet set = tLJSPayDB.query();
            if(set.size() > 0)
            {
                mLJSPaySchema = set.get(1);
                tPayDate = mLJSPaySchema.getPayDate();
            }
        }

        String tLimit = PubFun.getNoLimit(tCom);
        String serNo = PubFun1.CreateMaxNo("XXNO", tLimit);
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq(serNo);
        tLOPRTManagerSchema.setOtherNo(tLCContSchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //个人保单号
        tLOPRTManagerSchema.setCode(tCode); //42为失效通知书的代码
        tLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(tCom);
        tLOPRTManagerSchema.setReqOperator("000");
        tLOPRTManagerSchema.setPrtType("0"); //前台打印
        tLOPRTManagerSchema.setStateFlag("0"); //提交打印
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        tLOPRTManagerSchema.setStandbyFlag1(tLCContSchema.getPaytoDate()); //失效日期=交至日期
        tLOPRTManagerSchema.setStandbyFlag2(tPayDate);
        if (mLJSPaySchema != null) 
        {
            tLOPRTManagerSchema.setStandbyFlag3(mLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSchema.setStandbyFlag4(mLJSPaySchema.getOtherNoType());
        }
        mMap.put(tLOPRTManagerSchema, "DELETE&INSERT");
        return true;
    }
    //by hehongwei  应收记录作废
    private MMap dealIndiCancel(LCContSchema mLCContSchema){
    	GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom ="86";
        mGlobalInput.Operator = "Server";
        String sql = "select getnoticeno from ljspay a where othernotype = '2' "
//              +" and exists (select 1 from ljspayperson b , lcpol c "
//              +" where b.polno = c. polno and c.stateflag in ('2','3') "
//              +" and b.getnoticeno = a.getnoticeno )"
              +" and otherno ='"+mLCContSchema.getContNo()+"' "
              ;
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        IndiLJSCancelBL tIndiLJSCancelBL = new IndiLJSCancelBL();
        for(int i =1 ;i<=tSSRS.getMaxRow() ;i++)
        {
        	try {
        		LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        		tLJSPaySchema.setGetNoticeNo(tSSRS.GetText(i,1));

        		TransferData tTransferData = new TransferData();
        		//作废原因为收费失败
        		tTransferData.setNameAndValue("CancelMode", "4");
        		// 20180730 保单失效作废应收时催收状态设置为"已撤销"
        		tTransferData.setNameAndValue("IsRepeal", "2");

        		VData tVData = new VData();
        		tVData.addElement(tLJSPaySchema);
        		tVData.addElement(mGlobalInput);
        		tVData.addElement(tTransferData);

        		if(tIndiLJSCancelBL.getSubmitMap(tVData, "INSERT") == null)
        		{
        			mErrors.addOneError("个单失效保单作废应收"+tSSRS.GetText(i,1)+"失败");
        		}
        	} catch (Exception ex) {
        		mErrors.addOneError(ex.toString());
        	}
        }
        return mMap;
    }

    /***************************个单满期终止处理********************************/
    private boolean callSingleTerminate() 
    {
        System.out.println("个单险种满期终止批处理运行开始。。。。");
        String subSql = getSQLManageCom();
        String strSql = "select * from LCPol a where  1=1 "
                        + " and not exists(select 'X' from lccontstate where  enddate is null and state = '1' and  statetype = 'Terminate' and polno = a.polno) " //失效状态下不参与满期终止
                        +
                        " and a.AppFlag = '1' and conttype = '1' and a.EndDate <= '" +
                        mCurrentDate + "' " +
                        " and riskcode not in (select riskcode from lmriskapp where risktype4 = '4') " +
                        " and riskcode not in (select code from ldcode1 where exists (select 1 from lmriskapp where riskcode = ldcode1.code1 and risktype4= '4'))"
                        +" and exists (select 1 from LMRiskApp where RiskPeriod ='L' AND A.riskcode= riskCode)  " //长期险满期终止
                        + subSql + " and stateflag ='1'  "
                        +" union  "
                        +" select * from LCPol c where  1=1 "
                        +" and not exists(select 'X' from lccontstate where  enddate is null and state = '1' and  statetype = 'Terminate' and polno = c.polno) "
                        +" and c.AppFlag = '1' and conttype = '1' and c.EndDate <=  '"+mCurrentDate+"' "
                        +" and riskcode not in (select riskcode from lmriskapp where risktype4 = '4') "
                        +" and riskcode not in (select code from ldcode1 where exists (select 1 from lmriskapp where riskcode = ldcode1.code1 and risktype4= '4')) "
                        +" and stateflag ='1' "
                        +" and polstate in('03050001','03050002') "//续期应收记录作废、保全终止缴费。
                        +subSql+" with ur "
                        ;
        System.out.println("Sql ：" + strSql);
        RSWrapper rswrapper = new RSWrapper();
        LCPolSet tLCPolSet = new LCPolSet();
        rswrapper.prepareData(tLCPolSet,strSql);
        do {
            rswrapper.getData();
            //循环取得5000条数据，进行处理
            for(int i=1;i<=tLCPolSet.size();i++)
            {
            	LCPolSet aLCPolSet=new LCPolSet();
            	aLCPolSet.add(tLCPolSet.get(i));
            	//逐条调用险种终止处理
            	if (!SingleTerminateDeal(aLCPolSet)) 
            	{
            		return false;
            	}
            }
        }while (tLCPolSet.size() > 0);
            	
            	
            	
        System.out.println("个单保单满期终止批处理运行开始。。。。");
        //查询未失效、无应收的个单
        String Sql = "select * from LCCont a where  1=1 "
                     + " and not exists(select 'X' from lccontstate where  enddate is null and state = '1' and  statetype = 'Terminate' and contno = a.contno and polno = '000000') " //失效状态下不参与满期终止
                     + 
                     " and a.AppFlag = '1' and conttype = '1' and a.cinvalidate <= '" +
                     mCurrentDate + "' "
                     + " and not exists(select 'X' from ljspayperson where contno = a.contno) and stateflag ='1'  "
                     + subSql + " with ur";
        System.out.println("Sql ：" + Sql);
        RSWrapper rswrappercont = new RSWrapper();
        LCContSet tLCContSet = new LCContSet();
        rswrappercont.prepareData(tLCContSet,Sql);
        do {
            rswrappercont.getData();
            //循环取得5000条数据，进行处理
            for(int i=1;i<=tLCContSet.size();i++)
            {
            	LCContSet aLCContSet=new LCContSet();
            	aLCContSet.add(tLCContSet.get(i));
            	//保单满期终止处理
            	if (!SinglePolicyTerminateDeal(aLCContSet)) 
            	{
            		return false;
            	}
            }
        }while (tLCContSet.size() > 0);	
        System.out.println("满期终止批处理运行结束。。。。");

        return true;
    }

    private boolean SinglePolicyTerminateDeal(LCContSet tLCContSet) 
    {
        for (int i = 1; i <= tLCContSet.size(); i++) 
        {
            LCContSchema mLCContSchema = new LCContSchema();
            mLCContSchema.setSchema(tLCContSet.get(i));
            mEnd = mLCContSchema.getCInValiDate();
            //若保单没有未终止的险种，则保单终止
            String sql = "select count(1) from LCPol a "
                              + "where ContNo = '"
                              + mLCContSchema.getContNo() + "' "
                              + "   and (StateFlag is null or StateFlag != '"
                              + BQ.STATE_FLAG_TERMINATE + "') ";
            String available = tExeSQL.getOneValue(sql);
            if ("0".equals(available)) 
            {
                if (!setPolicyAbate(mLCContSchema,
                                    mLCContSchema.getCInValiDate(),
                                    "Terminate", "1", "01")) 
                {
                    System.out.println("保单" + mLCContSchema.getContNo() +
                                       "设置失效失败");
                } 
                else 
                {
                    sendTerminateNotice(mLCContSchema, mManageCom, "21");
                }
            }
            if (!SubmitMap()) 
            {
                return false;
            }
        }
        return true;
    }

    private boolean SingleTerminateDeal(LCPolSet aLCPolSet) 
    {
        LCContStateDB tLCContStateDB = new LCContStateDB();
        LCContStateSet tLCContStateSet = new LCContStateSet();
        LCContStateSchema tLCContStateSchema = new LCContStateSchema();
        MMap tMap = new MMap();
        VData tData = new VData();
        PubSubmit tPubSubmit = new PubSubmit();

        for (int i = 1; i <= aLCPolSet.size(); i++) 
        {
            tLCContStateSet.clear();
            // 判断是否满足剩余的条件
            if (!canTerminate(aLCPolSet.get(i))) 
            {
                continue;
            }

            tMap = new MMap();
            tPubSubmit = new PubSubmit();

            tLCContStateDB.setContNo(aLCPolSet.get(i).getContNo());
            tLCContStateDB.setPolNo(aLCPolSet.get(i).getPolNo());
            tLCContStateDB.setStateType("Available");
            tLCContStateSet = tLCContStateDB.query();
            if (!tLCContStateDB.mErrors.needDealError()) 
            {
                if (tLCContStateSet != null && tLCContStateSet.size() > 0) 
                {
                    tLCContStateSchema = tLCContStateSet.get(1).getSchema();
                    tLCContStateSet.get(1).setEndDate(aLCPolSet.get(i).
                            getEndDate());
                    tMap.put(tLCContStateSet.get(1), "DELETE&INSERT");
                } 
                else 
                {
                    tLCContStateSchema = new LCContStateSchema();
                    tLCContStateSchema.setContNo(aLCPolSet.get(i).getContNo());
                    tLCContStateSchema.setPolNo(aLCPolSet.get(i).getPolNo());

                    tLCContStateSchema.setGrpContNo("000000");
                    String mInsuredNo = "";
                    if (aLCPolSet.get(i).getInsuredNo().trim().equals("")) 
                    {
                        mInsuredNo = "000000";
                    } 
                    else 
                    {
                        mInsuredNo = aLCPolSet.get(i).getInsuredNo();
                    }
                    tLCContStateSchema.setInsuredNo(mInsuredNo);
                    tLCContStateSchema.setStateType("Terminate");
                    tLCContStateSchema.setOtherNoType("");
                    tLCContStateSchema.setOtherNo("");
                    tLCContStateSchema.setState("1");
                    tLCContStateSchema.setStateReason("01");
                    //modified by liurx 06-01-10;张超在bug5579中提出，满期终止状态起期应为保险止期零时，而不是次日。
                    tLCContStateSchema.setStartDate(aLCPolSet.get(i).getEndDate());
                    tLCContStateSchema.setEndDate("");
                    tLCContStateSchema.setRemark("");
                    tLCContStateSchema.setOperator("000");
                    tLCContStateSchema.setMakeDate(PubFun.getCurrentDate());
                    tLCContStateSchema.setMakeTime(PubFun.getCurrentTime());
                    tLCContStateSchema.setModifyDate(PubFun.getCurrentDate());
                    tLCContStateSchema.setModifyTime(PubFun.getCurrentTime());
                    tMap.put(tLCContStateSchema, "DELETE&INSERT");
                    SetLCPolState(aLCPolSet.get(i), "3",tMap);
                }
                System.out.println(aLCPolSet.get(i).getPolNo() + "满期终止");
                tData.clear();
                tData.add(tMap);
                tPubSubmit.submitData(tData, "");
            }
        }
        return true;
    }

    /*发送满期终止通知书*/
    private boolean sendTerminateNotice(LCContSchema tLCContSchema, String tCom,
                                        String tCode) 
    {
        String tLimit = PubFun.getNoLimit(tCom);
        String serNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq(serNo);
        tLOPRTManagerSchema.setOtherNo(tLCContSchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //个人保单号
        tLOPRTManagerSchema.setCode(tCode); //42为失效通知书的代码
        tLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(tCom);
        tLOPRTManagerSchema.setReqOperator("000");
        tLOPRTManagerSchema.setPrtType("0"); //前台打印
        tLOPRTManagerSchema.setStateFlag("0"); //提交打印
        tLOPRTManagerSchema.setStandbyFlag1(tLCContSchema.getCInValiDate());  //tLCContSchema.getCInValiDate()=保单保障满期日+1天
        tLOPRTManagerSchema.setStandbyFlag2(tLCContSchema.getCInValiDate());
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mMap.put(tLOPRTManagerSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 提供给外界调用的单个险种满期终止函数
     * @param: tPolNo 输入的数据
     * @return:  boolean
     */
    public boolean terminatePol(String tPolNo) {
        LCPolSchema tLCPolSchema = getLCPol(tPolNo);
        if (!terminatePol(tLCPolSchema)) {
            return false;
        }
        return true;
    }

    /**
     * 提供给外界调用的单个险种满期终止函数
     * @param: tLCPolSchema 输入的数据
     * @return:  boolean
     */
    public boolean terminatePol(LCPolSchema tLCPolSchema) {
        if (tLCPolSchema == null) {
            mErrors.addOneError(new CError("传入数据为空！"));
            return false;
        }
        if (!checkTerminate(tLCPolSchema)) {
            return false;
        }
        if (!dealTerminatePol(tLCPolSchema)) {
            return false;
        }
        return true;
    }

    /**
     * 单个险种满期终止函数
     * @param: tLCPolSchema 输入的数据
     * @return:  boolean
     */
    private boolean dealTerminatePol(LCPolSchema tLCPolSchema) {
        MMap tMap = new MMap();
        VData tData = new VData();
        PubSubmit tPubSubmit = new PubSubmit();
        //险种终止
        LCPolSchema uptLCPolSchema = new LCPolSchema();
        uptLCPolSchema.setSchema(tLCPolSchema);
        uptLCPolSchema.setAppFlag("4");
        uptLCPolSchema.setExpiryFlag("1");
        uptLCPolSchema.setModifyDate(PubFun.getCurrentDate());
        uptLCPolSchema.setModifyTime(PubFun.getCurrentTime());
        tMap.put(uptLCPolSchema, "UPDATE");

        boolean mainPolFlag = false;
        LCContSchema tLCContSchema = null;
        //如果是主险，保单也终止
        if (isMainPol(tLCPolSchema.getPolNo(), tLCPolSchema.getMainPolNo())) {
            mainPolFlag = true;
            tLCContSchema = getLCCont(tLCPolSchema.getContNo());
            if (tLCContSchema != null) {
                tLCContSchema.setAppFlag("4");
                tLCContSchema.setModifyDate(PubFun.getCurrentDate());
                tLCContSchema.setModifyTime(PubFun.getCurrentTime());
                tMap.put(tLCContSchema, "UPDATE");
            }
        }
        //更新保单状态表数据
        LCContStateDB tLCContStateDB = new LCContStateDB();
        LCContStateSet tLCContStateSet = new LCContStateSet();
        LCContStateSchema tLCContStateSchema = new LCContStateSchema();

        tLCContStateDB.setContNo(tLCPolSchema.getContNo());
        tLCContStateDB.setPolNo(tLCPolSchema.getPolNo());
        tLCContStateDB.setStateType("Terminate");
        tLCContStateSet = tLCContStateDB.query();
        if (!tLCContStateDB.mErrors.needDealError()) {
            if (tLCContStateSet != null && tLCContStateSet.size() > 0) {
                tLCContStateSchema = tLCContStateSet.get(1).getSchema();
                tLCContStateSet.get(1).setEndDate(tLCPolSchema.getEndDate());
                tMap.put(tLCContStateSet.get(1), "DELETE&INSERT");
            } else {
                tLCContStateSchema = new LCContStateSchema();
                tLCContStateSchema.setContNo(tLCPolSchema.getContNo());
                tLCContStateSchema.setPolNo(tLCPolSchema.getPolNo());
            }
            tLCContStateSchema.setInsuredNo("000000");
            tLCContStateSchema.setStateType("Terminate");
            tLCContStateSchema.setState("1");
            tLCContStateSchema.setStateReason("01");
            tLCContStateSchema.setStartDate(tLCPolSchema.getEndDate());
            tLCContStateSchema.setRemark("");
            tLCContStateSchema.setOperator("000");
            tLCContStateSchema.setMakeDate(PubFun.getCurrentDate());
            tLCContStateSchema.setMakeTime(PubFun.getCurrentTime());
            tLCContStateSchema.setModifyDate(PubFun.getCurrentDate());
            tLCContStateSchema.setModifyTime(PubFun.getCurrentTime());
            tMap.put(tLCContStateSchema, "DELETE&INSERT");

            //准备满期终止通知书打印管理表数据
            if (mainPolFlag) {
                String tLimit = "";
                String serNo = "";
                LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
                tLimit = PubFun.getNoLimit("86");
                serNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                tLOPRTManagerSchema.setPrtSeq(serNo);
                tLOPRTManagerSchema.setOtherNo(tLCPolSchema.getPolNo());
                tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_INDPOL); //个人保单号
                tLOPRTManagerSchema.setCode(""); //满期终止通知书的代码
                tLOPRTManagerSchema.setManageCom(tLCPolSchema.getManageCom());
                tLOPRTManagerSchema.setAgentCode(tLCPolSchema.getAgentCode());
                tLOPRTManagerSchema.setReqCom("86");
                tLOPRTManagerSchema.setReqOperator("000");
                tLOPRTManagerSchema.setPrtType("0"); //前台打印
                tLOPRTManagerSchema.setStateFlag("0"); //提交打印
                tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                tMap.put(tLOPRTManagerSchema, "DELETE&INSERT");
            }
            System.out.println(tLCPolSchema.getPolNo() + "满期终止");
            tData.clear();
            tData.add(tMap);
            if (!tPubSubmit.submitData(tData, "")) {
                mErrors.addOneError(new CError("提交满期终止数据失败！"));
                return false;
            }
        }
        return true;
    }

    /**
     * 当外界调用满期终止时的校验
     * @param: tLCPolSchema 输入的数据
     * @return:  boolean
     */
    private boolean checkTerminate(LCPolSchema tLCPolSchema) {
        if (tLCPolSchema.getAppFlag() != null &&
            tLCPolSchema.getAppFlag().equals("4")) {
            mErrors.addOneError(new CError("该保单已经处于终止状态！"));
            return false;
        }
        FDate tFDate = new FDate();
        if (tFDate.getDate(tLCPolSchema.getEndDate()).after(tFDate.getDate(
                mCurrentDate))) {
            mErrors.addOneError(new CError("该保单尚未到保险责任终止日期" +
                                           tLCPolSchema.getEndDate() + "！"));
            return false;
        }
        //如果处于保全挂起则不终止
        if (edorseState(tLCPolSchema.getContNo(),tLCPolSchema.getContType())) {
            mErrors.addOneError(new CError("该保单处于保全挂起状态，不能满期终止！"));
            return false;
        }
        //判断保单状态是否为失效
        if (booPolState(tLCPolSchema.getPolNo(), "Available", "1")) {
            mErrors.addOneError(new CError("该保单处于失效状态，不能满期终止！"));
            return false;
        }

        if (!canTerminate(tLCPolSchema)) {
            return false;
        }
        return true;
    }

    /**
     * @param tLCPolSchema LCPolSchema
     * @return boolean
     */
    private boolean canTerminate(LCPolSchema tLCPolSchema) {
        //如果处于保全挂起则不终止
        if (edorseState(tLCPolSchema.getContNo(),tLCPolSchema.getContType())) {
            return false;
        }

        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(tLCPolSchema.getContNo());
        LJSPaySet set = tLJSPayDB.query();
        for(int i = 1; i <= set.size(); i++)
        {
            LJSPaySchema schema = set.get(i);

            //若正电话催缴、正转账，不置状态
            if("2".equals(schema.getBankOnTheWayFlag())
                || "1".equals(schema.getBankOnTheWayFlag()))
            {
                return false;
            }
        }

        /*个单一年期险种只有以下情况才会终止，其中1和2由续保自动核保程序校验，若不能续保，将险种预标记为续保终止（此标记非保单状态），满期后批处理程序置为满期终止。
         * 1)、由于年龄限制不可能再续保，即到达续保年龄上限。此种情况在满期失效程序中不进行判断，由续保自动核保程序校验，若不能续保，将险种预标记为续保终止（此标记非保单状态），满期后批处理程序置为满期终止。
         * 2)、险种（提前）已被置了续保终止标示，如客户已经提了续保终止申请，或抽档后的应收记录已被置为应收撤销。
         */
        String sql = "select RiskPeriod from LMRiskApp where riskCode = '"
                     + tLCPolSchema.getRiskCode() + "' ";
        String riskPeriod = tExeSQL.getOneValue(sql);
        if(!"L".equals(riskPeriod))
        {
            //若险种PolState状态为续保终止，则需要置终止
            if(BQ.POLSTATE_XBEND.equals(tLCPolSchema.getPolState())||BQ.POLSTATE_ZFEND.equals(tLCPolSchema.getPolState()))
            {
                return true;
            }
            return false;
        }

        //如果是附加险且处于自动续保
        if (!tLCPolSchema.getPolNo().equals(tLCPolSchema.getMainPolNo()) &&
            tLCPolSchema.getRnewFlag() == -1) {
            //如果主险满期，则附加险直接终止
            if (isEndMainPol(tLCPolSchema.getContNo())) {
                return true;
            }
            FDate tFDate = new FDate();
            String tLapseDate =
                    EdorCalZT.CalLapseDate(tLCPolSchema.getRiskCode(),
                                           tLCPolSchema.getEndDate());
            if (tLapseDate == null || tLapseDate.trim().equals("")) {
                //System.out.println(tLCPolSchema.getPolApplyDate() + "计算宽限期止期失败");
                mErrors.addOneError(new CError(tLCPolSchema.getPolApplyDate() +
                                               "计算宽限期止期失败"));
                return false;
            }
            if (tFDate.getDate(mCurrentDate).before(tFDate.getDate(tLapseDate))) {
                //System.out.println(sPolNo + "未到宽限期止期");
                mErrors.addOneError(new CError("未到宽限期止期!"));
                return false;
            } else {
                //如果已经交费尚未核销，不能终止
                sql = " select 1 from ljtempfee where tempfeeno = " +
                      " (select getnoticeno from ljspay where otherno = '" +
                      tLCPolSchema.getContNo() + "') ";
                tExeSQL = new ExeSQL();
                String sHasPayFlag = tExeSQL.getOneValue(sql);
                if (tExeSQL.mErrors.needDealError()) {
                    //CError.buildErr(this, "暂交费查询失败!");
                    mErrors.addOneError(new CError("暂交费查询失败!"));
                    return false;
                }
                if (sHasPayFlag != null && sHasPayFlag.trim().equals("1")) {
                    mErrors.addOneError(new CError("已经有暂交费!"));
                    return false; //已经有暂交费
                }
                sql = " select bankonthewayflag from ljspay " +
                      " where otherno = '" + tLCPolSchema.getContNo() + "' ";
                SSRS tssrs = tExeSQL.execSQL(sql);
                if (tssrs != null && tssrs.getMaxRow() >= 1) {
                    String tBankFlag = tssrs.GetText(1, 1);
                    if (tBankFlag != null && !tBankFlag.trim().equals("") &&
                        tBankFlag.trim().equals("1")) {
                        mErrors.addOneError(new CError("处于银行划款期间"));
                        return false;
                    }
                }

            }
        }
        return true;
    }

    private boolean isMainPol(String tPolNo, String tMainPolNo) {
        if (tPolNo != null && tMainPolNo != null &&
            tPolNo.trim().equals(tMainPolNo.trim())) {
            return true;
        }
        return false;
    }

    private LCContSchema getLCCont(String tContNo) {
        LCContDB tLCContDB = new LCContDB();
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContDB.setContNo(tContNo);
        if (!tLCContDB.getInfo()) {
            return null;
        }
        tLCContSchema = tLCContDB.getSchema();
        return tLCContSchema;
    }

    private LCPolSchema getLCPol(String tPolNo) {
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolDB.setPolNo(tPolNo);
        if (!tLCPolDB.getInfo()) {
            return null;
        }
        tLCPolSchema = tLCPolDB.getSchema();
        return tLCPolSchema;
    }

    /***************************团单未交费暂停处理区域*******************************/
    /*团体保单暂停批处理*/
    private boolean CallGrpPause() {

        System.out.println("团体失效批处理运行开始。。。。");
        String subSql = getSQLManageCom();
        String strSql =
                "select * from lcgrpcont a where 1 = 1 " +
                " and not exists(select 'X' from lcgrpcontstate b where b.statetype in ('Pause','Terminate') and b.state = '1' and b.grpcontno = a.grpcontno ) " +
                " and payintv <> 0 " +
                " and not exists(select 'X' from ljspaygrp where grpcontno = a.grpcontno " +  //modify by fuxin 2008-6-6 有应收的保单不失效。
                subSql + " ) " +
                " and appflag = '1' and stateflag = '1' " + 
                " and (select max(paytodate) from lcgrppol where grpcontno = a.grpcontno) < '" +
                mCurrentDate + "' " +
                subSql +
                " union " +
                " select * from lcgrpcont c where 1 = 1 " +
                " and not exists(select 'X' from ljspaygrp where grpcontno = c.grpcontno " +
                subSql + " ) " +
                " and appflag = '1'  and stateflag = '1'" +
                " and (select max(paytodate) from lcgrppol where grpcontno = c.grpcontno) < '" +
                mCurrentDate + "' " +
                subSql + " with ur";
        System.out.println("GrpSql : " + strSql);
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContSet = tLCGrpContDB.executeQuery(strSql);
        GrpDealData(tLCGrpContSet);

        return true;
    }

    /*设置团体暂停状态*/
    private boolean GrpDealData(LCGrpContSet tLCGrpContSet) 
    {
        for (int i = 1; i <= tLCGrpContSet.size(); i++) 
        {
            LCGrpContSchema tLCGrpContSchema = tLCGrpContSet.get(i);
            mGrpContNo = tLCGrpContSchema.getGrpContNo();
            //判断团体保单是否已经失效
            if (booGrpPolicyState(mGrpContNo, "Pause", "1")) 
            {
                continue;
            }
            //特需险种不在这里做失效
            if (isSpecialPol(mGrpContNo)) {
                continue;
            }
            /*保全状态*/
            if (edorseState(mGrpContNo,"2")) 
            {
                continue;
            }
            //是否理赔校验 qulq 2007-5-28
            String sql = "select rgtno from llregister where rgtobjno='"+mGrpContNo
                         +"' and rgtstate not in ('04','05') and declineflag is null ";
            ExeSQL temp = new ExeSQL();
            SSRS rs = temp.execSQL(sql);
            if (rs != null && rs.getMaxRow() > 0)
            {
                continue;
            }
            
            String tSql = "select max(paytodate) from lcgrppol where grpcontno = '" + mGrpContNo + "'";
            mEnd = tExeSQL.getOneValue(tSql);

            String strLJSPaySql = "select * from LJSPay  where otherno = '" +
                                  mGrpContNo + "' and othernotype = '1'";
            System.out.println(strLJSPaySql);
            mLJSPaySchema = new LJSPayDB().executeQuery(strLJSPaySql).get(1);

            //是否在做续期
            if (operFeeState(mGrpContNo)) { //做续期了
                //须发生续期收费的保单,团体保单失效处理
                if (!OperAbateDeal(strLJSPaySql)) {
                    continue;
                } else
                if (setGrpPolicyAbate(tLCGrpContSchema, mEnd, "Pause", "1",
                                      "01")) {
                    sendGrpInvaliNotice(tLCGrpContSchema, "86", "42",
                                        mLJSPaySchema.getPayDate());
                }
            } else {//没做续期
                //是否过了宽限期
                if (!isEndCheckDate(mGrpContNo)) {
                    continue;
                } else
                if (setGrpPolicyAbate(tLCGrpContSchema, mEnd, "Pause", "1",
                                      "01")) {
                    sendGrpInvaliNotice(tLCGrpContSchema, mManageCom, "42",null);
                }
            }
            if (!SubmitMap()) {
                return false;
            }
        }
        
        return true;

    }

    //团体暂停实效发失效通知书
    private boolean sendGrpInvaliNotice(LCGrpContSchema tLCGrpContSchema,
                                        String tCom, String tCode,
                                        String tPayDate) {
        String tStartDate = PubFun.calDate(mEnd, 1, "D", null);
        String tLimit = PubFun.getNoLimit(tCom);
        String serNo = PubFun1.CreateMaxNo("XXNO", tLimit);
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq(serNo);
        tLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //个人保单号
        tLOPRTManagerSchema.setCode(tCode); //42为失效通知书的代码
        tLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(tCom);
        tLOPRTManagerSchema.setReqOperator("000");
        tLOPRTManagerSchema.setPrtType("0"); //前台打印
        tLOPRTManagerSchema.setStateFlag("0"); //提交打印
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        tLOPRTManagerSchema.setStandbyFlag1(tStartDate); //失效日期
        tLOPRTManagerSchema.setStandbyFlag2(tPayDate);
        if (mLJSPaySchema != null) {
            tLOPRTManagerSchema.setStandbyFlag3(mLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSchema.setStandbyFlag4(mLJSPaySchema.getOtherNoType());
        }
        mMap.put(tLOPRTManagerSchema, "DELETE&INSERT");
        return true;
    }

    /***************************团单满期终止处理区域*******************************/
    private boolean callGrpTerminate() {
        System.out.println("团体满期终止批处理运行开始。。。。");
        String subSql = getSQLManageCom();
        String strSql =
                "select * from lcgrpcont a where 1 = 1 " +
                " and not exists(select 'X' from lcgrpcontstate b where b.statetype in ('Terminate') and b.state = '1' and  b.enddate is null and b.grpcontno = a.grpcontno ) " +
                " and appflag = '1' and stateflag <> '3' " +
                " and a.cinvalidate < '" + mCurrentDate + "' " +
                " and not exists(select 'X' from ljspaygrp where grpcontno = a.grpcontno ) " +
                subSql +
                " union " +
                " select * from lcgrpcont c where 1 = 1 " +
                " and appflag = '1' and stateflag <> '3' " +
                " and c.cinvalidate < '" + mCurrentDate + "' " +
                " and not exists(select 'X' from ljspaygrp where grpcontno = c.grpcontno ) " +
                subSql + 
                " union " +
                " select * from lcgrpcont d where 1 = 1 " +
                " and appflag = '1' and stateflag <> '3' " +
                " and state='03050002' " +
                " and (select max(paytodate) from lcgrppol where grpcontno=d.grpcontno and appflag='1') < '" + mCurrentDate + "' " +
                " and not exists (select 'X' from ljspaygrp where grpcontno = d.grpcontno ) " +
                subSql + " with ur";
        System.out.println("strSql : " + strSql);
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContSet = tLCGrpContDB.executeQuery(strSql);
        GrpTerminateDealData(tLCGrpContSet);

        return true;
    }

    /*设置团体满期终止状态*/
    private boolean GrpTerminateDealData(LCGrpContSet tLCGrpContSet) {
        for (int i = 1; i <= tLCGrpContSet.size(); i++) {
            LCGrpContSchema tLCGrpContSchema = tLCGrpContSet.get(i);
            mEnd = tLCGrpContSchema.getCInValiDate();
            mGrpContNo = tLCGrpContSchema.getGrpContNo();
            //特需险种不在这里做满期终止
            if (isSpecialPol(mGrpContNo)) {
                continue;
            }
            //判断团体保单是否已经失效
            if (booGrpPolicyState(mGrpContNo, "Terminate", "1")) {
                continue;
            }

            /*保全状态*/
            if (edorseState(mGrpContNo,"2")) {
                continue;
            }
            if (setGrpPolicyAbate(tLCGrpContSchema, mEnd, "Terminate", "1",
                                  "01")) {
                sendGrpTerminateNotice(tLCGrpContSchema, mManageCom, "21");
            }
            if (!SubmitMap()) {
                return false;
            }
        }

        return true;
    }

    /*发送满期终止通知书*/
    private boolean sendGrpTerminateNotice(LCGrpContSchema tLCGrpContSchema,
                                           String tCom, String tCode) {
        String tStartDate = PubFun.calDate(mEnd, 1, "D", null);
        String tLimit = PubFun.getNoLimit(tCom);
        String serNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq(serNo);
        tLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_GRPCONT); //团体保单号
        tLOPRTManagerSchema.setCode(tCode);
        tLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(tCom);
        tLOPRTManagerSchema.setReqOperator("000");
        tLOPRTManagerSchema.setPrtType("0"); //前台打印
        tLOPRTManagerSchema.setStateFlag("0"); //提交打印
        tLOPRTManagerSchema.setStandbyFlag1(tStartDate);
        tLOPRTManagerSchema.setStandbyFlag2(tLCGrpContSchema.getCInValiDate());
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mMap.put(tLOPRTManagerSchema, "DELETE&INSERT");
        return true;
    }

    /***************************工具程序区域*******************************/
    /*设置团体保单失效*/
    public boolean setGrpPolicyAbate(LCGrpContSchema tLCGrpContSchema,
                                     String tEndDate, String tStateType,
                                     String tState, String tStateReason) {
        if (tLCGrpContSchema == null || tEndDate == null ||
            tStateType == null || tState == null ||
            tEndDate.trim().equals("") ||
            tStateType.trim().equals("") ||
            tState.trim().equals("")) {
            return false;
        }
        //新状态起始日期为旧状态结束日期次日
        String tStartDate = PubFun.calDate(tEndDate, 1, "D", null);
        LCGrpContStateDB tLCGrpContStateDB = new LCGrpContStateDB();
        LCGrpContStateSet tLCGrpContStateSet = new LCGrpContStateSet();
        LCGrpContStateSchema oldLCGrpContStateSchema = new LCGrpContStateSchema();
        if (!tStateType.equals("Pause"))
        {
            String strSql =
                    " select * from lcgrpcontstate where statetype = 'Pause' " +
                    " and enddate is null and grpcontno = '" +
                    tLCGrpContSchema.getGrpContNo() +
                    "' and grppolno = '000000'";
            tLCGrpContStateSet = tLCGrpContStateDB.executeQuery(strSql);
            if (!tLCGrpContStateDB.mErrors.needDealError()) {
                if (tLCGrpContStateSet != null && tLCGrpContStateSet.size() > 0) {
                    oldLCGrpContStateSchema = tLCGrpContStateSet.get(1).
                                              getSchema();
                    oldLCGrpContStateSchema.setEndDate(tEndDate);
                    oldLCGrpContStateSchema.setStateReason(tStateReason);
                    oldLCGrpContStateSchema.setModifyDate(PubFun.getCurrentDate());
                    oldLCGrpContStateSchema.setModifyTime(PubFun.getCurrentTime());
                    mMap.put(oldLCGrpContStateSchema, "DELETE&INSERT");
                }
            } else {
                System.out.println("保单状态查询失败");
                return false;
            }
        }
        LCGrpContStateSchema tLCGrpContStateSchema = new LCGrpContStateSchema();

        tLCGrpContStateSchema.setGrpContNo(tLCGrpContSchema.getGrpContNo());
        tLCGrpContStateSchema.setGrpPolNo("000000");
        tLCGrpContStateSchema.setStateType(tStateType);
        tLCGrpContStateSchema.setStateReason(tStateReason);
        tLCGrpContStateSchema.setState(tState);
        if (mLJSPaySchema != null) {
            tLCGrpContStateSchema.setOtherNo(mLJSPaySchema.getGetNoticeNo());
            tLCGrpContStateSchema.setOtherNoType(mLJSPaySchema.
                                                 getOtherNoType());
        }
        tLCGrpContStateSchema.setOperator("000");
        tLCGrpContStateSchema.setStartDate(tStartDate);
        tLCGrpContStateSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpContStateSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpContStateSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpContStateSchema.setModifyTime(PubFun.getCurrentTime());
        tLCGrpContStateSchema.setContPlanCode("00");
        mMap.put(tLCGrpContStateSchema, "DELETE&INSERT");
        if (tStateType.equals("Pause")) {
            SetGrpContState(tLCGrpContSchema, "2");
        }
        if (tStateType.equals("Terminate")) {
            SetGrpContState(tLCGrpContSchema, "3");
        }
        return true;
    }

    /*返回TRUE是表示已经是失效失效，返回FALSE是表示未失效*/
    /*aStateType表示状态表中状态类型，aState表示状态表中状态*/
    /*团单单保单状态*/
    public boolean booGrpPolicyState(String aGrpContNo, String aStateType,
                                     String aState) {
        String AbateSql = "Select * from LCGrpContState where GrpContNo = '" +
                          aGrpContNo
                          + "' and Enddate is null";
        LCGrpContStateDB aLCGrpContStateDB = new LCGrpContStateDB();
        LCGrpContStateSet aLCGrpContStateSet = new LCGrpContStateSet();
        LCGrpContStateSchema aLCGrpContStateSchema = new LCGrpContStateSchema();
        aLCGrpContStateSet = aLCGrpContStateDB.executeQuery(AbateSql);
        if (aLCGrpContStateSet.size() > 0) {
            aLCGrpContStateSchema.setSchema(aLCGrpContStateSet.get(1));
            if (aLCGrpContStateSchema.getStateType().equals(aStateType)
                && aLCGrpContStateSchema.getState().equals(aState)) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /*个单保单状态*/
    public boolean booPolicyState(String aContNo, String aStateType,
                                  String aState) {
        String AbateSql = "Select * from LCContState where ContNo = '" +
                          aContNo
                          + "' and GrpContNo = '000000' and EndDate is null";
        LCContStateDB aLCContStateDB = new LCContStateDB();
        LCContStateSet aLCContStateSet = new LCContStateSet();
        LCContStateSchema aLCContStateSchema = new LCContStateSchema();
        aLCContStateSet = aLCContStateDB.executeQuery(AbateSql);
        aLCContStateSchema.setSchema(aLCContStateSet.get(1));
        if (aLCContStateSet.size() > 0) {
            if (aLCContStateSchema.getStateType().equals(aStateType)
                && aLCContStateSchema.getState().equals(aState)) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /*个单险种状态*/
    public boolean booPolState(String aPolNo, String aStateType, String aState) {
        String AbateSql = "Select * from LCContState where PolNo = '" + aPolNo
                          + "' and EndDate is null";
        LCContStateDB aLCContStateDB = new LCContStateDB();
        LCContStateSet aLCContStateSet = new LCContStateSet();
        LCContStateSchema aLCContStateSchema = new LCContStateSchema();
        aLCContStateSet = aLCContStateDB.executeQuery(AbateSql);

        if (aLCContStateSet.size() > 0) {
            aLCContStateSchema.setSchema(aLCContStateSet.get(1));
            if (aLCContStateSchema.getStateType().equals(aStateType)
                && aLCContStateSchema.getState().equals(aState)) {
                return true;
            }
            return false;
        } else {
            return false;
        }

    }

    /*保全状态*/
    public boolean edorseState(String aContNo,String aContType) {
        String tableName = "";
        String contType = "";
        //个单
        if ("1".equals(aContType)) {
            tableName = " LPEdorMain ";
            contType = " contNo ";
        } else {
            tableName = " LPGrpEdorMain ";
            contType = " grpContNo ";
        }

        StringBuffer sql = new StringBuffer();
        sql.append("select a.edorAcceptNo ")
                .append("from LPEdorApp a, ")
                .append(tableName).append(" b ")
                .append("where a.edorAcceptNo = b.edorAcceptNo ")
                .append("    and b.").append(contType).append(" = '")
                .append(aContNo).append("' ")
                .append("    and a.edorState != '")
                .append(BQ.EDORSTATE_CONFIRM).append("' ")
                .append("   and not exists(select 1 from LGwork ")
                .append("           where WorkNo = a.EdorAcceptNo and StatusNo = '8') "); //撤销
        System.out.println(sql.toString());
        ExeSQL e = new ExeSQL();
        SSRS tSSRS = e.execSQL(sql.toString());
        if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            return true;
        }

        return false;
    }

    //续期状态
    public boolean operFeeState(String aContNo) {
        //这个判断函数有问题，不知道是用来干什么的
//        if (PubFun.isDueFee(aContNo, mCurrentDate, mCurrentDate)) {
        ExeSQL tExeSQL = new ExeSQL();
            String count=null;
            StringBuffer tSBql = new StringBuffer(128);
            tSBql.append("SELECT COUNT(*) FROM LJSPay a WHERE a.otherNo='")
                    .append(aContNo)
                    .append("'")
                    ;
            String strSql = tSBql.toString();
            count = tExeSQL.getOneValue(strSql);
            if ((count == null) || count.equals("0")) {
                return false;
        } else {
            return true;
        }

    }

    /*返回False表示续期收费成功，返回TURE表示续期收费失败*/
    /*须发生续期收费的保单,保单失效处理*/
    private boolean OperAbateDeal(String aSql) {
        LJSPayDB aLJSPayDB = new LJSPayDB();
        LJSPaySet aLJSPaySet = new LJSPaySet();
        aLJSPaySet.set(aLJSPayDB.executeQuery(aSql));
        LJSPaySchema aLJSPaySchema = new LJSPaySchema();
        //应该发生续期收费的保单,状态为收费失败的情况下
        aLJSPaySchema.setSchema(aLJSPaySet.get(1));
        //是否交费为核销
        if (!booIsGetMoney("OperFee", aLJSPaySchema.getOtherNo())) {
            return false;
        }
        if (("2".equals(aLJSPaySchema.getBankOnTheWayFlag()) ||
            !PubFun.getLaterDate(aLJSPaySchema.getPayDate(), mCurrentDate).equals(mCurrentDate)))
        {
            return false;
        }
        return true;
    }

    /**
     * 是否已交费
     * 已缴费false，否则true
     */
    private boolean booIsGetMoney(String aFlag, String aContNo) {
        //如果已经交费尚未核销，不能终止
        String sql = "";
        if (aFlag.equals("OperFee")) {
            sql = " select 1 from ljtempfee where tempfeeno = " +
                  " (select getnoticeno from ljspay where otherno = '" +
                  aContNo + "') ";
        }
        if (aFlag.equals("Endorse")) {
            String EndorseSql =
                    "select distinct endorsementno from ljsgetendorse where contno = '" +
                    aContNo +
                    "' and endorsementno like '20%' ";
            sql = " select 1 from ljtempfee where tempfeeno = " +
                  " (select getnoticeno from ljspay where otherno in (" +
                  EndorseSql + ")) ";
        }
        tExeSQL = new ExeSQL();
        String sHasPayFlag = tExeSQL.getOneValue(sql);
        if (tExeSQL.mErrors.needDealError()) {
            //CError.buildErr(this, "暂交费查询失败!");
            mErrors.addOneError(new CError("暂交费查询失败!"));
            return false;
        }
        if (sHasPayFlag != null && sHasPayFlag.trim().equals("1")) {
            mErrors.addOneError(new CError("已经有暂交费!"));
            return false; //已经有暂交费
        }
        sql = " select bankonthewayflag from ljspay " +
              " where otherno = '" + aContNo + "' ";
        SSRS tssrs = tExeSQL.execSQL(sql);
        if (tssrs != null && tssrs.getMaxRow() >= 1) {
            String tBankFlag = tssrs.GetText(1, 1);
            if (tBankFlag != null && !tBankFlag.trim().equals("") &&
                tBankFlag.trim().equals("1")) {
                mErrors.addOneError(new CError("处于银行划款期间"));
                return false;
            }
        }
        return true;
    }

    /**
     * 宽限期校验：
      1、无续期，过了宽限期，则置失效
      2、有续期，缴至日期和首次缴费日+宽限期(长期险60天，短期险30天)比较大的一个超过当天的，置为失效
      20100326 zhanggm 修改校验2 缴费截至日期(ljspayb的paydate)和缴至日期+宽限期(长期险60天，短期险30天)比较大的一个超过当天的，置为失效
     * @param tLCPolSchema LCPolSchema
     * @return boolean：过可以置失效true
     */
    private boolean checkLapseDate(LCPolSchema tLCPolSchema) {
        FDate tFDate = new FDate();

        //查询宽限期
        String sql = "select max(a.GracePeriod) "
                     + "from LMRiskPay a, LCPol b "
                     + "where a.RiskCode = b.RiskCode "
                     + "   and b.ContNo = '" + tLCPolSchema.getContNo() + "' "
                     + " and (b.stateflag = '1' or b.StateFlag is null) and b.AppFlag = '1'"
                     ;
        String maxGracePeriod = new ExeSQL().getOneValue(sql);
        if(maxGracePeriod.equals("") || maxGracePeriod.equals("null"))
        {
            System.out.println(tLCPolSchema.getPolNo() + "计算宽限期失败");
                return false;
        }

        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(tLCPolSchema.getContNo());
        LJSPaySet set = tLJSPayDB.query();

        //1、无续期，过了宽限期止期，则置失效
        if(set.size() == 0)
        {
            String tLapseDate = PubFun.calDate(tLCPolSchema.getPaytoDate(),
                           Integer.parseInt(maxGracePeriod), "D", null);
            mEnd = tLapseDate;
            mGracePeriod = tLapseDate;
            if(tFDate.getDate(tLapseDate).after(tFDate.getDate(mCurrentDate)))
            {
                System.out.println(tLCPolSchema.getPolNo() + "无续期未到宽限期止期");
                return false;
            }

            return true;
        }

        //2、有续期，缴至日期和首次缴费日+宽限期(长期险60天，短期险30天)比较大的一个超过当天的，置为失效
        //20100326 zhanggm 修改校验2 缴费截至日期(ljspayb的paydate)和缴至日期+宽限期(长期险60天，短期险30天)比较大的一个超过当天的，置为失效
        for(int i = 1; i <= set.size(); i++)
        {
            LJSPaySchema schema = set.get(i);

            //若正电话催缴、正转账，不置状态
            if("2".equals(schema.getBankOnTheWayFlag())
                || "1".equals(schema.getBankOnTheWayFlag()))
            {
                return false;
            }

            if(tFDate.getDate(schema.getPayDate()).after(tFDate.getDate(mCurrentDate)))
            {
                System.out.println(tLCPolSchema.getPolNo() + "续期未到缴费截止期");
                return false;
            }

            String maxDate = schema.getPayDate();
            //缴费截至日期(ljspayb的paydate)和缴至日期+宽限期(长期险60天，短期险30天)比较大的一个
            if(tFDate.getDate(PubFun.calDate(tLCPolSchema.getPaytoDate(),
                                            Integer.parseInt(maxGracePeriod),
                                            "D", null)).after(tFDate.getDate(maxDate)))
            {
               // maxDate = tLCPolSchema.getPaytoDate();
               maxDate = PubFun.calDate(tLCPolSchema.getPaytoDate(),
                                            Integer.parseInt(maxGracePeriod),
                                            "D", null);

            }
            //超过当天的，置为失效
            if(tFDate.getDate(maxDate).after(tFDate.getDate(mCurrentDate)))
            {
                System.out.println(tLCPolSchema.getPolNo() + "续期未到宽限期止期");
                return false;
            }
            mGracePeriod = maxDate;
        }

        return true;
    }

    /*是否是主险*/
    private boolean isEndMainPol(String tContNo) {
        String strsql = "select '1' from lcpol where enddate <='" +
                        mCurrentDate + "' and polno = mainpolno and contno = '" +
                        tContNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tssrs = tExeSQL.execSQL(strsql);
        if (tssrs != null && tssrs.getMaxRow() == 1) {
            return true; //主险满期，附加险终止
        }
        return false;
    }

    /*团体是否超过宽限期*/
    private boolean isEndCheckDate(String aGrpContNo) {
        int aGracePeriod = 0;
        String sql = "select '1' from lmriskapp where riskcode in (" +
                     "select riskcode from lcgrppol where grpcontno = '" +
                     aGrpContNo + "') " +
                     "and riskperiod = 'L'";
        tExeSQL = new ExeSQL();
        String strIsLongPeriod = tExeSQL.getOneValue(sql);
        if (strIsLongPeriod.equals("1")) {
            aGracePeriod = 60;
        } else {
            aGracePeriod = 30;
        }

        String tSql = "select max(paytodate) from lcgrppol where grpcontno = '" +
                      aGrpContNo + "'";
        String strPayToDate = tExeSQL.getOneValue(tSql);
        /*当天与交至日期的间隔是否大于宽限期，并且当天比较靠后的*/
        if (PubFun.calInterval(strPayToDate, mCurrentDate, "D") > aGracePeriod
            &&
            PubFun.getLaterDate(strPayToDate, mCurrentDate).equals(mCurrentDate)) {
            return true;
        } else {
            return false;
        }
    }

    /*是否含有特需险种*/
    private boolean isSpecialPol(String aGrpContNo) {
        String sql = "select distinct 'X' from LCGRPPOL where 1=1 " +
                     "and grpcontno = '" + aGrpContNo + "' " +
                     "and riskcode in (select RiskCode from LMRiskApp where RiskType3 = '7'  and riskcode!='170101')";
        ExeSQL mExeSQL = new ExeSQL();
        String mFlag = mExeSQL.getOneValue(sql);
        if (mFlag.equals("X")) {
            return true;
        } else {
            return false;
        }
    }

    /*SQL中取管理结构工具*/
    private String getSQLManageCom() {
        String mSQL = "";
        if (mManageCom != null && !mManageCom.trim().equals("")) {
            if (mManageCom.length() >= 4) {

                mSQL = " and ManageCom like '" + mManageCom.substring(0, 4) +
                       "%' ";
            } else {
                mSQL = " and ManageCom like '86%' ";
            }
        } else {
        	 mSQL = " ";
        }
        return mSQL;
    }

    /*个单保单设置状态*/
    private boolean SetContState(LCContSchema tLCContSchema, String StateFlag) {
//        tLCContSchema.setStateFlag(StateFlag);
        String updatecont = "update LCCont Set StateFlag = '"
                            + StateFlag + "',ModifyDate = '" + PubFun.getCurrentDate()
                            + "',ModifyTime = '" + PubFun.getCurrentTime() +
                            "' where Contno = '" + tLCContSchema.getContNo() +
                            "'";

        mMap.put(updatecont, "UPDATE");

        return true;
    }

    /*个单险种设置状态*/
    private boolean SetPolState(LCPolSchema tLCPolSchema, String StateFlag) {
        String updatepol = "update LCPol Set StateFlag = '"
                           + StateFlag + "',ModifyDate = '" + PubFun.getCurrentDate()
                           + "',ModifyTime = '" + PubFun.getCurrentTime() +
                           "' where PolNo = '" + tLCPolSchema.getPolNo() + "'";
        mMap.put(updatepol, "UPDATE");
        return true;
    }

    /*险种状态设置*/
    private boolean SetLCPolState(LCPolSchema tLCPolSchema, String StateFlag,MMap tMap) {
        String updatepol = "update LCPol Set StateFlag = '"
                           + StateFlag + "',ModifyDate = '" + PubFun.getCurrentDate()
                           + "',ModifyTime = '" + PubFun.getCurrentTime() +
                           "' where PolNo = '" + tLCPolSchema.getPolNo() + "'";
        tMap.put(updatepol, "UPDATE");
        return true;
    }

    /*个单保单设置状态*/
    private boolean SetLCContState(LCContSchema tLCContSchema, String StateFlag,MMap tMap) {
        //        tLCContSchema.setStateFlag(StateFlag);
        String updatecont = "update LCCont Set StateFlag = '"
                            + StateFlag + "',ModifyDate = '" + PubFun.getCurrentDate()
                            + "',ModifyTime = '" + PubFun.getCurrentTime() +
                            "' where Contno = '" + tLCContSchema.getContNo() +
                            "'";
        tMap.put(updatecont, "UPDATE");
        return true;
    }

    /*团体保单设置状态*/
    private boolean SetGrpContState(LCGrpContSchema tLCGrpContSchema,
                                    String StateFlag) {
        String aGrpContNo = tLCGrpContSchema.getGrpContNo();
        if (aGrpContNo == null || aGrpContNo.equals("") ||
            aGrpContNo.equals("null")) {
            System.out.println("合同号传入错误在程序第1690");
            return false;
        }
        String updateGrpCont = "update LCGrpCont Set StateFlag = '"
                               + StateFlag + "',ModifyDate = '" + PubFun.getCurrentDate()
                               + "',ModifyTime = '" + PubFun.getCurrentTime() +
                               "' where Grpcontno = '" + aGrpContNo + "'";
        mMap.put(updateGrpCont, "UPDATE");

        String grppolsql = "select * from lcgrppol where grpcontno = '" +
                           aGrpContNo + "'";
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        tLCGrpPolSet = tLCGrpPolDB.executeQuery(grppolsql);
        for (int intgrppol = 1; intgrppol <= tLCGrpPolSet.size(); intgrppol++) {
            LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
            tLCGrpPolSchema = tLCGrpPolSet.get(intgrppol);
            String updateGrpPol = "update LCGrpPol Set StateFlag = '"
                                  + StateFlag + "',ModifyDate = '" +
                                  PubFun.getCurrentDate()
                                  + "',ModifyTime = '" + PubFun.getCurrentTime() +
                                  "' where GrpPolNo = '" +
                                  tLCGrpPolSchema.getGrpPolNo() + "'";
            mMap.put(updateGrpPol, "UPDATE");
        }
        String updatecont = "update LCCont Set StateFlag = '"
                            + StateFlag + "',ModifyDate = '" + PubFun.getCurrentDate()
                            + "',ModifyTime = '" + PubFun.getCurrentTime() +
                            "' where GrpContNo = '" + aGrpContNo +
                            "'";
        mMap.put(updatecont, "UPDATE");
        String updatepol = "update LCPol Set StateFlag = '"
                           + StateFlag + "',ModifyDate = '" + PubFun.getCurrentDate()
                           + "',ModifyTime = '" + PubFun.getCurrentTime() +
                           "' where GrpContNo = '" + aGrpContNo +
                           "'";
        mMap.put(updatepol, "UPDATE");

        return true;
    }

    private boolean Call280101()
    {
	   System.out.println("280101 险种失效批处理开始运行：" + PubFun.getCurrentDate() +
	                  PubFun.getCurrentTime());
	   String SQL = " select * From LCPol a where RiskCode ='280101' and stateflag ='1' and appflag ='1' and payintv !=0  "
	              + " and paytodate <= '" + mCurrentDate + "'"
	              + " and not exists (select 1 from ljspaygrp where a.grpcontno = grpcontno) "
	              + " with ur ";
	   LCPolDB tLCPolDB = new LCPolDB();
       LCPolSet tLCPolSet = tLCPolDB.executeQuery(SQL);
       
       //逐条调用保单失效处理
       if (PolDealData(tLCPolSet)) {
           System.out.println("个单险种失效处理成功");
       }
       
	    System.out.println("280101 险种失效批处理结束：" + PubFun.getCurrentDate() +
	               PubFun.getCurrentTime());
	
	    System.out.println("280101 保单失效失效批处理开始：" + PubFun.getCurrentDate() +
	               PubFun.getCurrentTime());
	    String LCContSql = " select * From lccont a where appflag ='1' and stateflag ='1'"
	                     + " and exists (select 1 from lcpol where RiskCode ='280101' and stateflag ='2' and a.contno = contno and appflag ='1')  with ur";
		LCContDB tLCContDB = new LCContDB();
		LCContSet tLCContSet = tLCContDB.executeQuery(LCContSql);
		
		if (!LContDealData(tLCContSet)) {
			return false;
		}

       
       return true;
    }
    
    private boolean Call231001()
    {
	   System.out.println("231001 险种失效批处理开始运行：" + PubFun.getCurrentDate() +
	                  PubFun.getCurrentTime());
	   String SQL = " select * from lcpol c where 1 = 1 and riskcode in ('231001','231201','231801')  " +
               " and StateFlag not in('2', '3') " +  //未失效、未满期
               " and polno <> mainpolno " +
               " and exists(select 'X' from lcpol where polno = mainpolno and payintv != 0 and appflag = '1' and polno = c.mainpolno " +
                " ) " +
               " and appflag = '1' and conttype = '1' " +
               " and paytodate < '" + mCurrentDate + "' " +
               " and riskcode not in (select riskcode from lmriskpay where graceperiod is null) "+
               " and riskcode not in (select riskcode from lmriskapp where risktype4 = '4') " +
               " and ((riskcode not in (select riskcode from lmriskapp where riskperiod='L')) or paytodate<>payenddate) "+
               " and riskcode  in (select code from ldcode1 where exists (select 1 from lmriskapp where riskcode = ldcode1.code1 and risktype4= '4'))";
	   LCPolDB tLCPolDB = new LCPolDB();
       LCPolSet tLCPolSet = tLCPolDB.executeQuery(SQL);
       System.out.println(SQL);
       //逐条调用保单失效处理
       if (UARDealData(tLCPolSet)) {
           System.out.println("万能附加险种失效处理成功");
           
       }
       
	    System.out.println("231001 险种失效批处理结束：" + PubFun.getCurrentDate() +
	               PubFun.getCurrentTime());
	
	    System.out.println("231001 保单失效失效批处理开始：" + PubFun.getCurrentDate() +
	               PubFun.getCurrentTime());
	    
	    
//	    String LCContSql = " select * From lccont a where appflag ='1' and stateflag ='1'"
//	                     + " and exists (select 1 from lcpol where RiskCode ='280101' and stateflag ='2' and a.contno = contno and appflag ='1')  with ur";
//		LCContDB tLCContDB = new LCContDB();
//		LCContSet tLCContSet = tLCContDB.executeQuery(LCContSql);
//		
//		if (!LContDealData(tLCContSet)) {
//			return false;
//		}

       
       return true;
    }
    
    /**
     * 万能附加重疾险种失效处理
     * @param aLCPolSet
     * @return
     */
    private boolean UARDealData(LCPolSet aLCPolSet){

        for (int i = 1; i <= aLCPolSet.size(); i++) 
        {
            LCPolSchema aLCPolSchema = new LCPolSchema();
            aLCPolSchema.setSchema(aLCPolSet.get(i));
            String aPolNo = aLCPolSchema.getPolNo();
            String aContNo = aLCPolSchema.getContNo();
            String contType = aLCPolSchema.getContType();//保单类型（团、个）
            //续期交费未核销
            if (!booIsGetMoney("OperFee", aContNo)) {
                System.out.println(aPolNo + "续期交费未核销");
                continue;
            }
            //保全交费未核销
            if (!booIsGetMoney("Endorse", aContNo)) {
                System.out.println(aPolNo + "保全缴费未核销");
                continue;
            }

            /*
             * 宽限期校验：
             1、无续期，过了宽限期，则置失效
             2、有续期，缴至日期和首次缴费日+宽限期(长期险60天，短期险30天)比较大的一个超过当天的，置为失效
             20100326 zhanggm 修改校验2 缴费截至日期(ljspayb的paydate)和缴至日期+宽限期(长期险60天，短期险30天)比较大的一个超过当天的，置为失效
             */
            if (!checkLapseDate(aLCPolSchema)) 
            {
                System.out.println(aPolNo + "未过宽限期");
                continue;
            }

            /*保全状态*/
            if (edorseState(aContNo,contType)) 
            {
                System.out.println(aPolNo + "在做保全项目");
                continue;
            }
            /*理赔状态*/
            if (mLLCaseCommon.getClaimState(aPolNo).equals("1") ||
                mLLCaseCommon.getClaimState(aPolNo).equals("2")) 
            {
                System.out.println(aPolNo + "在做理赔项目");
                continue;
            }

            /*设置险种失效*/
            setPolAbate(aLCPolSchema, PubFun.calDate(mGracePeriod, -1, "D", null), "Available", "1", "02");
            UARCalRPBL tUARCalRPBL = new UARCalRPBL();
            if (tUARCalRPBL.submitData(aLCPolSchema,mGracePeriod,"11")) {
 	           System.out.println("万能附加险种风险扣费计算成功");
 	       }
                
            if (!SubmitMap()) 
            {
                return false;
            }
        }
        
        return true;
    
    }
    
    /**
     * 万能重疾计算风险扣费
     * @param aLCPolSet
     * @return
     */
//    private boolean UARCalRP(LCPolSet aLCPolSet){
//
//
//        for (int i = 1; i <= aLCPolSet.size(); i++) 
//        {
//            LCPolSchema aLCPolSchema = new LCPolSchema();
//            aLCPolSchema.setSchema(aLCPolSet.get(i));
//            String aPolNo = aLCPolSchema.getPolNo();
//            String aContNo = aLCPolSchema.getContNo();
//            String contType = aLCPolSchema.getContType();//保单类型（团、个）
//            
//            LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
//            LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
//            tLCInsureAccDB.setContNo(aContNo);
//                       
//            LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
//            if (tLCInsureAccDB.mErrors.needDealError())
//            {
//                System.out.println(tLCInsureAccDB.mErrors.getErrContent());
//                mErrors.addOneError("查询月结信息时出错");
//                return false;
//            }
//            tLCInsureAccSchema = tLCInsureAccSet.get(1).getSchema(); 
//            
//            if (!SubmitMap()) 
//            {
//                return false;
//            }
//        }
//        
//        return true;
//    
//    
//    	
//    }
    
    private boolean PolDealData(LCPolSet aLCPolSet) {
        for (int i = 1; i <= aLCPolSet.size(); i++) {
            LCPolSchema aLCPolSchema = new LCPolSchema();
            aLCPolSchema.setSchema(aLCPolSet.get(i));
            setPolAbate(aLCPolSchema, aLCPolSchema.getPaytoDate(),
                        "Available", "1", "02");
            if (!SubmitMap()) 	{
                return false;
            }
        }
        
        return true;
    }
    
    private boolean LContDealData(LCContSet tLCContSet) {
        for (int i = 1; i <= tLCContSet.size(); i++) {
            LCContSchema tLCContSchema = tLCContSet.get(i).getSchema();
            //若所有险种都终止或失效，则保单失效
            String sql = "select count(1) from LCPol "
                         + "where StateFlag not in('2','3')"
                         + "   and ContNo ='" + tLCContSchema.getContNo() +
                         "' ";
            String countStr = tExeSQL.getOneValue(sql);
            if (!"0".equals(countStr)) {
                continue;
            }
            if (!setPolicyAbate(tLCContSchema, tLCContSchema.getPaytoDate(),
                                "Available", "1", "02")) {
                System.out.println("保单" + tLCContSchema.getContNo() +
                                   "设置失效失败");
            }
            if (!SubmitMap()) {
                return false;
            }
        }
        
        return true;
    }

    /*数据处理*/
    private boolean SubmitMap() {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult.clear();
        mResult.add(mMap);
        if (!tPubSubmit.submitData(mResult, "")) 
        {
            System.out.println("提交数据失败！");
//            return false;
        }
        this.mMap = new MMap();
        return true;
    }
    
    public static void main(String[] args) 
    {
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.Operator="001";
        PolicyAbateDealBL mPolicyAbateDealBL = new PolicyAbateDealBL();
        mPolicyAbateDealBL.mG=mGlobalInput;
        mPolicyAbateDealBL.mManageCom="86";
//        mPolicyAbateDealBL.Call231001();
        mPolicyAbateDealBL.CallSingleAvailable();
    }


}


