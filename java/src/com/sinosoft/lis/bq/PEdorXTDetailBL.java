package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统
 *
 * </p>
 * <p>Description:
 * 生成协议退保的退费记录
 * 退保分为整单退保和部分险种退保两种
 * 部分险种退保中若被保人的所有险种都被退，则该被保人也被退
 * </p>
 * <p>Copyright: Copyright (c) 2005.3.31</p>
 * <p>Company: Sinosoft</p>
 * @author LHS  modified by Yang Yalin
 * @version 1.1
 */

import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class PEdorXTDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 传出数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局基础数据 */
    private GlobalInput mGlobalInput = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LPPolSet mLPPolSet = null;
    //保存退费金额
    private EdorItemSpecialData mEdorItemSpecialData = null;
    
    private String currDate = PubFun.getCurrentDate();
    private String currTime = PubFun.getCurrentTime();

    private DisabledManageBL tDisabledManageBL = new DisabledManageBL();

    public PEdorXTDetailBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //校验录入数据的合法性
        if(!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareData())
        {
            return false;
        }

        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, "")) //数据提交
        {
            mErrors.addOneError("数据提交失败");
            return false;
        }
        System.out.println("PEdorXTDetailBL End PubSubmit");

        return true;
    }


    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData
                           .getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) cInputData
                                .getObjectByObjectName("LPEdorItemSchema", 0);
            mLPPolSet = (LPPolSet) cInputData
                        .getObjectByObjectName("LPPolSet", 0);
            mEdorItemSpecialData = (EdorItemSpecialData) cInputData
                        .getObjectByObjectName("EdorItemSpecialData", 0);
        }
        catch (Exception e)
        {
            // @@错误处理
            e.printStackTrace();
            this.mErrors.addOneError("接收数据失败");
            return false;
        }
				//数据校验
				if(mGlobalInput == null|| mLPEdorItemSchema == null ||
					mLPPolSet == null || mLPPolSet.size() == 0 || mEdorItemSpecialData == null
           || mEdorItemSpecialData.getSpecialDataSet().size() == 0)
        {
            mErrors.addOneError("传入的数据不满足要求");
            return false;
        }
        return true;
    }

    /**
     * 校验录入数据的合法性
     * @return boolean
     */
    private boolean checkData()
    {
        if (mGlobalInput == null || mLPEdorItemSchema == null
            || mLPPolSet == null)
        {
            this.mErrors.addOneError("接收数据失败");
            return false;
        }
        
        DetailDataQuery tQuery = new DetailDataQuery(mLPEdorItemSchema.getEdorNo(), mLPEdorItemSchema.getEdorType());
        for(int i = 1; i <= mLPPolSet.size(); i++)
        {
            LPPolSchema tLPPolSchema =  (LPPolSchema) tQuery.getDetailData("LCPol", mLPPolSet.get(i).getPolNo());
            tLPPolSchema.setOperator(mGlobalInput.Operator);
            tLPPolSchema.setMakeDate(currDate);
            tLPPolSchema.setMakeTime(currTime);
            tLPPolSchema.setModifyDate(currDate);
            tLPPolSchema.setModifyTime(currTime);
            mLPPolSet.get(i).setSchema(tLPPolSchema);
        }

//      （某些）附加险或者主险任一险种出险，不可以单独退保或者一起退保，添加相关校验
        //如果是理赔人员进行的，可以不用此校验 zhangjl add
        String sOperator=mGlobalInput.Operator;
        if(!sOperator.substring(0, 2).equals("cm")){
        	ExeSQL tLPExeSQL = new ExeSQL();
            String sql_LP="select 1 from llcontdeal where EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"' with ur";
            String tLP =tLPExeSQL.getOneValue(sql_LP);
        	if(!tLP.equals("1")){
        		if(!checkClaimPartOf())
                {
                	return false;
                }
        	}
        }
           
        if(!checkEdorItem())
        {
            return false;
        }

        //处理录入的险种信息（C->P）
        if(!setPolInfo())
        {
            return false;
        }

        //检验主险下附加险是否选择
        if(!checkMainPol())
        {
            return false;
        }
        
//      20110804 校验万能险
        if(!checkULIXT())
        {
        	return false;
        }
        
        return true;
    }

    /**
     * 检验项目信息的完整性
     * @return boolean
     */
    private boolean checkEdorItem()
    {
        double tGetMoney = mLPEdorItemSchema.getGetMoney();
        String reasonCode = mLPEdorItemSchema.getReasonCode();

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());

        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1)
        {
            this.mErrors.addOneError("输入数据有误,LPEdorItem中没有相关数据!");
            return false;
        }

        
        
        
        mLPEdorItemSchema = tLPEdorItemSet.get(1);
        mLPEdorItemSchema.setEdorState("1");
        mLPEdorItemSchema.setGetMoney(tGetMoney);
        mLPEdorItemSchema.setReasonCode(reasonCode);

        return true;
    }

    /**
     * 处理录入的险种信息
     * @return boolean
     */
    private boolean setPolInfo()
    {
        LPEdorItemSchema tLPEdorItemSchema = mLPEdorItemSchema.getSchema();
        for (int i = 1; i <= mLPPolSet.size(); i++)
        {
            tLPEdorItemSchema.setPolNo(mLPPolSet.get(i).getPolNo());

            LPPolBL tLPPolBL = new LPPolBL();
            tLPPolBL.setSchema(mLPPolSet.get(i));
            tLPPolBL.queryLPPol(tLPEdorItemSchema);
            if (tLPPolBL.mErrors.needDealError())
            {
                CError.buildErr(this, "查询保单险种失败！");
                return false;
            }
            
            /*
             * 解决协议退保可以添加TD核保解约之外的险种的问题。
             * 
             */
            ExeSQL tTDExeSQL = new ExeSQL();
            String sql_TD="select edorno from lpedoritem a where" +
            		" contno='"+tLPEdorItemSchema.getContNo()+"'" +
            				" and exists (" +
            				"select 1 from lpedorapp where edoracceptno =a.edorno" +
            				" and edorstate<>'0'" +
            				")" +
            				"and edortype='TB'";
            
            String tEdorno =tTDExeSQL.getOneValue(sql_TD);
            
              if(tEdorno!=null&&!tEdorno.equals(null)&&!tEdorno.equals(""))
            {
            String sql_XT="select polno from lppol where edortype<>'DL' and edorno='" +tEdorno+"'";
            ExeSQL tXTExeSQL = new ExeSQL();
            SSRS tXTSSRS = tXTExeSQL.execSQL(sql_XT);
            
             for(int XTint =1 ;XTint<=tXTSSRS.getMaxRow();XTint++)
             {
                int j=1;
                for (; j <= mLPPolSet.size(); j++)
                {
                	System.out.println(mLPPolSet.get(j).getPolNo());
                    if (mLPPolSet.get(j).getPolNo().equals(tXTSSRS.GetText(XTint,1)))
                    {
                    	
                    	CError.buildErr(this, "存在核保结论不为解约的险种也进行了解约操作，险种号："+mLPPolSet.get(j).getPolNo());
                    	return false;
                    }
                }
                
              }
            }
            
            mLPPolSet.get(i).setSchema(tLPPolBL.getSchema());
        }

        return true;
    }

    /**
     * 检验主险下附加险是否全部选择
     * @return boolean
     */
    private boolean checkMainPol()
    {
         ExeSQL tExeSQL = new ExeSQL();
 //        HashMap tHashMap = new HashMap();
        for (int i = 1; i <= mLPPolSet.size(); i++)
        {
            LPPolSchema tLPPolSchema = new LPPolSchema();
            tLPPolSchema = mLPPolSet.get(i);
            StringBuffer error = new StringBuffer();

            //如果是主险
            if (tLPPolSchema.getMainPolNo().equals(tLPPolSchema.getPolNo()))
            {
                //得到主险下的附加险
                String sql = "select polno  from LCPOL where polno !='"
                             +tLPPolSchema.getPolNo()
                             +"' and mainpolno = '"
                             +tLPPolSchema.getPolNo()
                             +"'";
                SSRS tSSRS = tExeSQL.execSQL(sql);
                if(tSSRS==null||tSSRS.getMaxRow()==0)
                    continue;

                //校验主险下的附加险
                for(int k =1 ;k<=tSSRS.getMaxRow();k++)
                {
                    int j=1;
                    for (; j <= mLPPolSet.size(); j++)
                    {
                        if (mLPPolSet.get(j).getPolNo().equals(tSSRS.GetText(k,1)))
                        {
                            break;
                        }
                    }
                    //选择的险种没有找到全部的附加险
                    if(j>mLPPolSet.size())
                    {
                        error.append("保单险种号为:").append(tLPPolSchema.getPolNo())
                                .append("的险种下的附加险:").append(tSSRS.GetText(k,1)).append("没有选择");
                    }
                }
                if(!(error.toString()==null)&&!error.toString().equals(""))
                {
                    CError.buildErr(this,error.toString());
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //生成协议退费数据信息
        setXTFeeInfo();

        if(!dealLPCont())
        {
            return false;
        }

        //处理被保人
        if(!dealLPInsured())
        {
            return false;
        }

        return true;
    }

    /**
     * dealLPCont
     *
     * @return boolean
     */
    private boolean dealLPCont()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        //整单退保
        if(tLCPolSet.size() == mLPPolSet.size())
        {
            //保全保单
            LPContBL tLPContBL = new LPContBL();
            tLPContBL.queryLPCont(mLPEdorItemSchema);
            if(tLPContBL.mErrors.needDealError())
            {
                CError.buildErr(this, "查询个人保单出错！");
                return false;
            }
            mMap.put(tLPContBL.getSchema(), "DELETE&INSERT");
        }
        else
        {
            String sql = "delete from LPCont "
                         + "where EdorNo = '"
                         + mLPEdorItemSchema.getEdorNo() + "' "
                         + "   and EdorType = '"
                         + mLPEdorItemSchema.getEdorType() + "' "
                         + "   and ContNo = '"
                         + mLPEdorItemSchema.getContNo() + "' ";
            mMap.put(sql, SysConst.DELETE);
        }

        return true;
    }

    /**
     * 生成协议退费数据信息
     */
    private void setXTFeeInfo()
    {
        mMap.put("  delete from LPEdorEspecialData "
                 + "where edorNo = '"
                 + mLPEdorItemSchema.getEdorNo()
                 + "'  and edorType = '"
                 + mLPEdorItemSchema.getEdorType() + "' "
                 + "   and PolNo in(select PolNo from LCPol "
                 + "        where ContNo = '"
                 + mLPEdorItemSchema.getContNo() + "')",
                 "DELETE");
        mMap.put(mEdorItemSpecialData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     * 生成被保人备份信息
     * @return boolean
     */
    private boolean dealLPInsured()
    {
        HashSet tHashSet = new HashSet();

        //应该被退的被保人
        LPInsuredSet tLPInsuredSet = new LPInsuredSet();
        Reflections tReflections = new Reflections();

        //当前保单下的所有被保人
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();

        for(int j = 1; j <= mLPPolSet.size(); j++)
        {
            //由此可避免重复插入
            if(tHashSet.contains(mLPPolSet.get(j).getInsuredNo()))
            {
                continue;
            }

            tHashSet.add(mLPPolSet.get(j).getInsuredNo());

            tLCInsuredDB.setContNo(mLPEdorItemSchema.getContNo());
            tLCInsuredDB.setInsuredNo(mLPPolSet.get(j).getInsuredNo());
            if(tLCInsuredDB.getInfo())
            {
                this.mErrors.addOneError("查询被保人失败");
            }
            LPInsuredSchema schema = new LPInsuredSchema();
            tReflections.transFields(schema, tLCInsuredDB.getSchema());
            schema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            schema.setEdorType(mLPEdorItemSchema.getEdorType());
            schema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(schema);

            tLPInsuredSet.add(schema);
        }

        mMap.put("  delete from LPInsured "
                 + "where edorNo = '"
                 + mLPEdorItemSchema.getEdorNo() + "' "
                 + "  and edorType = '"
                 + mLPEdorItemSchema.getEdorType() + "' "
                 + "  and contno = '"
                 + mLPEdorItemSchema.getContNo() + "' ",
                 "DELETE");
        mMap.put(tLPInsuredSet, "DELETE&INSERT");

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareData()
    {
        mMap.put(mLPEdorItemSchema, "UPDATE"); //更新补退费金额
        mMap.put("  delete from LPPol "
         + "where edorNo = '"
         + mLPEdorItemSchema.getEdorNo()
         + "'  and edorType = '" + mLPEdorItemSchema.getEdorType()
         + "'  and contno = '" + mLPEdorItemSchema.getContNo() + "' ",
         "DELETE");
        mMap.put(mLPPolSet, "DELETE&INSERT");
        mResult.clear();
        mResult.add(mMap);
        return true;
    }
    
//	万能险套餐，所谓的主险不能单独退保=退保险种中包含主险，且不是整单退保，返回报错    
//  附加重疾可以单独退保
//  附加意外不可以单独退保，只能随主险一同退保。
    private boolean checkULIXT() 
    {
    	String tContNo = mLPEdorItemSchema.getContNo();
    	
    	if (tContNo == null || tContNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorXTDetailBL";
            tError.functionName = "checkULIWT";
            tError.errorMessage = "保单号查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	 LCPolSet tLCPolSet = CommonBL.getLeavingPolNoInfo(mLPPolSet);
    	 if(tLCPolSet == null || tLCPolSet.size()==0)
         {
             return true;
         }
         else //非整单退保
         {
       	  for(int i=1;i<=mLPPolSet.size();i++)
             {   	
           	  //分红险主险和附加险必须同时退保
           	  if(mLPPolSet.get(i).getRiskCode().equals("730101"))
           	  {
           		  CError tError = new CError();
                     tError.moduleName = "PEdorCTDetailBL";
                     tError.functionName = "checkULICT";
                     tError.errorMessage = "保单号查询失败!";
                     this.mErrors.addOneError("险种"+mLPPolSet.get(i).getRiskCode()+"只能同分红险的附加险一同退保!");
                     return false;
           	  }
             }
         }
    	 
    	 
//    	保单不包含万能险，不判断
    	if(!CommonBL.hasULIRisk(tContNo))
    	{
    		return true;
    	}
    	
        //未选择的险种数量为0，整单退保
       
        if(tLCPolSet == null || tLCPolSet.size()==0)
        {
            return true;
        }
        else //非整单退保
        {
      	  for(int i=1;i<=mLPPolSet.size();i++)
            {
      		  //万能险套餐，所谓的主险不能单独退保=退保险种中包含主险，且不是整单退保，返回报错
          	  if(CommonBL.isULIRisk(mLPPolSet.get(i).getRiskCode()))
          	  {
          		  CError tError = new CError();
                    tError.moduleName = "PEdorCTDetailBL";
                    tError.functionName = "checkULICT";
                    tError.errorMessage = "保单号查询失败!";
                    this.mErrors.addOneError("险种"+mLPPolSet.get(i).getRiskCode()+"只能同万能主险一同退保");
                    return false;
          	  }
          	  //6201不准单独退保        	  
          	  if(CommonBL.is62Risk(mLPPolSet.get(i).getRiskCode()))
          	  {
          		  CError tError = new CError();
                    tError.moduleName = "PEdorCTDetailBL";
                    tError.functionName = "checkULICT";
                    tError.errorMessage = "保单号查询失败!";
                    this.mErrors.addOneError("险种"+mLPPolSet.get(i).getRiskCode()+"只能同万能主险一同退保");
                    return false;
          	  }
            }
        }
        
        //若未选择的险种包含主险,判断附加险能否退保
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            if(CommonBL.isULIRisk(tLCPolSchema.getRiskCode()))
        	{
        		for (int j = 1; j <= mLPPolSet.size(); j++)
                {
        			LPPolSchema tLPPolSchema = mLPPolSet.get(j);
        			LMRiskAppDB subLMRiskAppDB = new LMRiskAppDB();
        			subLMRiskAppDB.setRiskCode(tLPPolSchema.getRiskCode());
        			if(subLMRiskAppDB.getInfo())
        			{
        				String tRiskType3 = subLMRiskAppDB.getRiskType3();
        				if(tRiskType3.equals("3")) //重疾
        				{
        					continue;
        				}
        				else if(tRiskType3.equals("4")) //意外
        				{
        					CError tError = new CError();
        		            tError.errorMessage = "附加险"+subLMRiskAppDB.getRiskCode()+"只能和主险同时退保!";
        		            this.mErrors.addOneError(tError);
        		            return false;
        				}
        			}
                }
            }
        }
        return true;
	}
    
  
    
    private boolean checkClaimPartOf()
    {
    	   String tcheckohterPolContNo = mLPEdorItemSchema.getContNo();
    	  	
    	  	if (tcheckohterPolContNo == null || tcheckohterPolContNo.equals(""))
    	      {
    	          CError tError = new CError();
    	          tError.moduleName = "PEdorXTDetailBL";
    	          tError.functionName = "checkohterPol";
    	          tError.errorMessage = "保单号查询失败!";
    	          this.mErrors.addOneError(tError);
    	          return false;
    	      }
    	  	
    	     	  for(int i=1;i<=mLPPolSet.size();i++)
    	           {   	
    	     		  //先判断某险种是否存在理赔

    	     		  String strPolno = null;
    	     		  String SQL_checkPolno="select caseno from llcase a where caseno in " +
    	     		  		" (select caseno from llclaimpolicy" +
    	     		  		" where polno ='"+strPolno+"')" +
    	     		  		" and not exists (select 1 from llclaim where caseno=a.caseno and givetype='3')" +
    	     		  		" with ur";

    	     		  SSRS tSSRS = null;
    	     		  String tMRiskcode = null;
    	     		  String tcheckpolSQL = null;
    	     		  String mriskcode = null;
    	     		  String friskcode = null;
    	     		  LPPolSchema tLPPolSchema=new LPPolSchema();
    	     		  ExeSQL tcheckotherpolExeSQL = new ExeSQL();
    	     		  String checkPolon = null;
    	     		  String tcheck_F_polSQL = null;
    	     		  String check_F_polno = null;
    	     		  tcheckpolSQL="select riskcode from lcpol where polno='"+checkPolon+"' " +
    			  		"and contno='"+mLPEdorItemSchema.getContNo()+"'";
    	     		  
    	     		  tLPPolSchema=mLPPolSet.get(i);
    	     		System.out.println(tLPPolSchema.getPolNo());
    	     		System.out.println(tLPPolSchema.getMainPolNo());
    	     		  if(!tLPPolSchema.getMainPolNo().equals(tLPPolSchema.getPolNo()))
    	     		  {
    	     			 strPolno = tLPPolSchema.getPolNo();//校验险种号为附加险校验险种号
    	     			SQL_checkPolno="select caseno from llcase a where caseno in " +
    	 		  		" (select caseno from llclaimpolicy" +
    	 		  		" where polno ='"+strPolno+"')" +
    	 		  		" and not exists (select 1 from llclaim where caseno=a.caseno and givetype='3')" +
    	 		  		" with ur";
    	     			 tSSRS = new ExeSQL().execSQL(SQL_checkPolno);
    	     			if (tSSRS.getMaxRow()== 0) //附加险没有出险
    	                {
    	     				strPolno = tLPPolSchema.getMainPolNo();//校验险种号为主险校验险种号
    	     				SQL_checkPolno="select caseno from llcase a where caseno in " +
    	     		  		" (select caseno from llclaimpolicy" +
    	     		  		" where polno ='"+strPolno+"')" +
    	     		  		" and not exists (select 1 from llclaim where caseno=a.caseno and givetype='3')" +
    	     		  		" with ur";
    	     				tSSRS = new ExeSQL().execSQL(SQL_checkPolno);
    	     				 if (tSSRS.getMaxRow()== 0) //主险也没有出险
    	     	             {
    	     	            	 continue; 
    	     	             }
    	     				 else//主险出险
    	     				 {
    	     	     			 checkPolon=tLPPolSchema.getMainPolNo();//主险出险得到主险的种号
    	     	     			tcheckpolSQL="select riskcode from lcpol where polno='"+checkPolon+"' " +
    	     			  		"and contno='"+mLPEdorItemSchema.getContNo()+"'";
    	     	     		     tMRiskcode =tcheckotherpolExeSQL.getOneValue(tcheckpolSQL);
    	     	     	          if(tMRiskcode==null&&tMRiskcode.equals(null)&&tMRiskcode.equals(""))
    	     	     	          {
    	     	     	          	CError tError = new CError();
    	     	     	          	tError.moduleName = "PEdorXTDetailBL";
    	     	     	              tError.functionName = "checkohterPol";
    	     	     	              tError.errorMessage = "保单号查询失败!";
    	     	     	              this.mErrors.addOneError("险种信息缺失---XTDEtail--628");
    	     	     	              return false;
    	     	     	          }
    	     	     	          mriskcode=tMRiskcode;
    	     	     	          friskcode=tLPPolSchema.getRiskCode();
    	     	     		   
    	     				 }
    	                }
    	     			else //附加险出险
    	     			{
    	     			 checkPolon=tLPPolSchema.getMainPolNo();//附加险出险得到主险的险种号
    	     			tcheckpolSQL="select riskcode from lcpol where polno='"+checkPolon+"' " +
    			  		"and contno='"+mLPEdorItemSchema.getContNo()+"'";
    	     		     tMRiskcode =tcheckotherpolExeSQL.getOneValue(tcheckpolSQL);

    	     	          if(tMRiskcode==null&&tMRiskcode.equals(null)&&tMRiskcode.equals(""))
    	     	          {
    	     	          	CError tError = new CError();
    	     	          	tError.moduleName = "PEdorXTDetailBL";
    	     	              tError.functionName = "checkohterPol";
    	     	              tError.errorMessage = "保单号查询失败!";
    	     	              this.mErrors.addOneError("险种信息缺失---XTDEtail--628");
    	     	              return false;
    	     	          }
    	     	         mriskcode=tMRiskcode;
    	     	         friskcode=tLPPolSchema.getRiskCode();
    	     		   }
    	     		  }
    	     		  //------------选中的只是主险------------
    	     		 if(tLPPolSchema.getMainPolNo().equals(tLPPolSchema.getPolNo()))
    	    		  {
    	    			 strPolno = tLPPolSchema.getMainPolNo();//校验险种号为主险种号
    	    			 SQL_checkPolno="select caseno from llcase a where caseno in " +
    	  		  		" (select caseno from llclaimpolicy" +
    	  		  		" where polno ='"+strPolno+"')" +
    	  		  		" and not exists (select 1 from llclaim where caseno=a.caseno and givetype='3')" +
    	  		  		" with ur";
    	    			 tSSRS = new ExeSQL().execSQL(SQL_checkPolno);
    	    			if (tSSRS.getMaxRow()== 0) //主险没有出险
    	               {
    	    				//得到附加险的险种号
    	    				tcheck_F_polSQL="select polno from lcpol where MainPolNo='"+tLPPolSchema.getMainPolNo()+"' " +
    		  		  		" and polno<>mainpolno " +
    		  		  		" and  contno='"+mLPEdorItemSchema.getContNo()+"'";
    	    				check_F_polno =tcheckotherpolExeSQL.getOneValue(tcheck_F_polSQL);
    	    				//--------------------------------------------
    	    				strPolno = check_F_polno;//校验险种号为附加险校验险种号
    	    				SQL_checkPolno="select caseno from llcase a where caseno in " +
    	     		  		" (select caseno from llclaimpolicy" +
    	     		  		" where polno ='"+strPolno+"')" +
    	     		  		" and not exists (select 1 from llclaim where caseno=a.caseno and givetype='3')" +
    	     		  		" with ur";
    	    				tSSRS = new ExeSQL().execSQL(SQL_checkPolno);
    	    				 if (tSSRS.getMaxRow()== 0) 
    	    	             {
    	    	            	 continue; 
    	    	             }
    	    				 else//附加险出险
    	    				 {
    	    	     			 checkPolon=check_F_polno;//主险出险得到附加险的种号
    	    	     			tcheckpolSQL="select riskcode from lcpol where polno='"+checkPolon+"' " +
    	    			  		"and contno='"+mLPEdorItemSchema.getContNo()+"'";
    	    	     		     tMRiskcode =tcheckotherpolExeSQL.getOneValue(tcheckpolSQL);
    	    	     	          if(tMRiskcode==null&&tMRiskcode.equals(null)&&tMRiskcode.equals(""))
    	    	     	          {
    	    	     	          	CError tError = new CError();
    	    	     	          	tError.moduleName = "PEdorXTDetailBL";
    	    	     	              tError.functionName = "checkohterPol";
    	    	     	              tError.errorMessage = "保单号查询失败!";
    	    	     	              this.mErrors.addOneError("险种信息缺失---XTDEtail--628");
    	    	     	              return false;
    	    	     	          }
    	    	     	          mriskcode=tLPPolSchema.getRiskCode();
    	    	     	          friskcode=tMRiskcode;
    	    				 }
    	               }
    	    			else//主险出险
    	    			{
//    	    				得到附加险的险种号
    	    				tcheck_F_polSQL="select riskcode from lcpol where MainPolNo='"+tLPPolSchema.getMainPolNo()+"' " +
    		  		  		" and polno<>mainpolno " +
    		  		  		" and  contno='"+mLPEdorItemSchema.getContNo()+"'";
    	    				check_F_polno =tcheckotherpolExeSQL.getOneValue(tcheck_F_polSQL);
    	    				//--------------------------------------------
    	    		     tMRiskcode =check_F_polno;
    	    	          if(tMRiskcode==null&&tMRiskcode.equals(null)&&tMRiskcode.equals(""))
    	    	          {
    	    	          	CError tError = new CError();
    	    	          	tError.moduleName = "PEdorXTDetailBL";
    	    	              tError.functionName = "checkohterPol";
    	    	              tError.errorMessage = "保单号查询失败!";
    	    	              this.mErrors.addOneError("险种信息缺失---XTDEtail--628");
    	    	              return false;
    	    	          }
    	    	          mriskcode=tLPPolSchema.getRiskCode();
    	     	          friskcode=tMRiskcode;
    	    		   }
    	    		  }
    	 		        
//    	          LDCode1Schema tLDCode1Schema=new LDCode1Schema();
    	          LDCode1DB tLDCode1DB =new LDCode1DB();
    	          tLDCode1DB.setCode(friskcode);
    	          tLDCode1DB.setCode1(mriskcode);
    	          tLDCode1DB.setCodeType("checkclaimrisk");
    	          if(!tLDCode1DB.getInfo())
    	          {
    	        	  
    	          	continue;
    	          }
    	          else
    	          {
    	          	CError tError = new CError();
    	              tError.moduleName = "PEdorXTDetailBL";
    	              tError.functionName = "checkohterPol";
    	              tError.errorMessage = "保单号查询失败!";
    	              this.mErrors.addOneError("险种已经出险"+tSSRS.GetText(1, 1)+"，" +
    	              		"和其存在主副险关系的"+tMRiskcode+"都不可以退保!");
    	              return false;
    	          }           	  
    	         }	
    	  	return true;
    	  }
    
    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }
    public static void main(String arg[])
    {

    }
}
