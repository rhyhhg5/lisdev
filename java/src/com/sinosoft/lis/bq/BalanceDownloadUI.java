package com.sinosoft.lis.bq;

//程序名称：BalanceDownloadUI.java
//程序功能：万能保单月度结算清单下载
//创建日期：2007-12-17
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BalanceDownloadUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public BalanceDownloadUI()
    {
    	System.out.println("UI->BalanceDownloadUI()");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	BalanceDownloadBL bl = new BalanceDownloadBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        return true;
    }
}
