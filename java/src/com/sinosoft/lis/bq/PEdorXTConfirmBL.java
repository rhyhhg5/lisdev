package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PEdorXTConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LPInsuredDB mLPInsuredDB = null;
    private LPPolDB mLPPolDB = null;
    private LCPolDB mLCPolDB = null;
    private ExeSQL mExeSQL = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();

    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!prepareData())
            return false;

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        String aEdorNo = mLPEdorItemSchema.getEdorNo();
//        ContCancel tContCancel = new ContCancel(mGlobalInput, mLPEdorItemSchema);
        //modify by fuxin 2010-4-2
        //原来的构造方法会修改B表的makedate，这样就无法统计万能险种的某一个时间的账户价值。
        ContCancel  tContCancel = new ContCancel();
        LPPolSet tLPPolSet = null;

        MMap tMMap = new MMap();  //退保信息
        int ctLPInsuredCount = 0; //退保的被保人数量
        LPInsuredSet tLPInsuredSet = getLPInsured();
        if (tLPInsuredSet.size() == 0)
        {
            return false;
        }

        for (int j = 1; j <= tLPInsuredSet.size(); j++)
        {
            tLPPolSet = getLPPol(tLPInsuredSet.get(j));
            int lcpolCount = getLCPolCount(tLPInsuredSet.get(j)); //个人险种数
            if (tLPPolSet.size() == 0 || lcpolCount == 0)
            {
                mErrors.addOneError("险种退保信息不完整。");
                return false;
            }
            //被保人退保
            if (tLPPolSet.size() == lcpolCount)
            {
                ctLPInsuredCount++;

                tMMap.add(tContCancel.prepareInsuredData(
                    tLPInsuredSet.get(j).getContNo(),
                    tLPInsuredSet.get(j).getInsuredNo(),
                    aEdorNo));

                if (tContCancel.mErrors.needDealError())
                {
                    CError.buildErr(this, "准备被保险人数据失败！");
                    return false;
                }
            }
            //险种退保
            else
            {
                for (int t = 1; t <= tLPPolSet.size(); t++)
                {
                    tMMap.add(tContCancel.preparePolData(
                        tLPPolSet.get(t).getPolNo(),
                        aEdorNo));
                    if (tContCancel.mErrors.needDealError())
                    {
                        CError.buildErr(this, "准备个人险种数据失败！");
                        return false;
                    }
                }
            } //险种退保
        } //保单下需要处理的被保人

        //若被保人都被退保，则个单退保，tMMap可以直接通过prepareContData方法得到
        int lcInsuredCount = getLCInsuredCount(mLPEdorItemSchema.getContNo());
        if (ctLPInsuredCount == lcInsuredCount)
        {
            tMMap = tContCancel.prepareContData(mLPEdorItemSchema.getContNo(),
                                                aEdorNo);
            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备个人保单数据失败！");
                return false;
            }
        }
        map.add(tMMap);
        
        //添加保费留存的数据修改
        map.add(getRemainMap());

        updateEdorState();
        mResult.clear();
        mResult.add(map);

        return true;
    }
    
    private MMap getRemainMap(){
    	MMap tMMap = new MMap();
    	
    	String updateSQL = "";
    	
    	String remainSQL = "select 1 from lccontremain where remainstate='1' " +
    			" and exists (select 1 from lpcont where contno=lccontremain.contno " +
    			" and (cvalidate + 2 years) > '"+mLPEdorItemSchema.getEdorValiDate()+"' " +
    			" and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"') " +
    			" and applyyears='2' " +
    			" and contno='"+mLPEdorItemSchema.getContNo()+"' ";
    	
    	ExeSQL tExeSQL = new ExeSQL();
    	
    	String remainFlag = tExeSQL.getOneValue(remainSQL);
    	
    	if(remainFlag!=null&&"1".equals(remainFlag)){
    		 updateSQL = " update  lccontremain set remainstate='3',edorZTdate='"+PubFun.getCurrentDate()+"',edorZTfee="+mLPEdorItemSchema.getGetMoney()+", " +
    				" modifydate='"+PubFun.getCurrentDate()+"',modifytime='"+PubFun.getCurrentTime()+"' where contno='"+mLPEdorItemSchema.getContNo()+"' and applyyears='2' ";
    		 tMMap.put(updateSQL, SysConst.UPDATE);
    	}
    	
    	
    	return tMMap;
    }

    private void updateEdorState()
    {
        map.put("  update LPEdorItem "
                + "set edorState = '" + BQ.EDORSTATE_CONFIRM
                + "',   operator = '" + mGlobalInput.Operator
                + "',  modifyDate = '" + PubFun.getCurrentDate()
                + "',  modifyTime = '" + PubFun.getCurrentTime()
                + "' where edorNo = '" + mLPEdorItemSchema.getEdorNo()
                + "'   and edorType = '" + mLPEdorItemSchema.getEdorType()
                + "'   and contNo = '" + mLPEdorItemSchema.getContNo()+ "' ",
            "UPDATE");
    }

    private LPContSchema getLPCont()
    {
        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPContDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPContDB.setContNo(mLPEdorItemSchema.getContNo());
        LPContSet tLPContSet = tLPContDB.query();

        if (tLPContSet.size() == 0)
        {
            CError.buildErr(this, "查询个人保单数据失败！");
            return null;
        }

        return tLPContSet.get(1);
    }

    private LPInsuredSet getLPInsured()
    {
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        tLPInsuredDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPInsuredDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPInsuredDB.setContNo(mLPEdorItemSchema.getContNo());
        LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();
        if (tLPInsuredSet.size() == 0)
        {
            CError.buildErr(this, "查询个人保单被保险人数据失败！");
        }

        return tLPInsuredSet;
    }

    private LPPolSet getLPPol(LPInsuredSchema schema)
    {
        if(mLPPolDB == null)
        {
            mLPPolDB = new LPPolDB();
        }
        mLPPolDB.setEdorNo(schema.getEdorNo());
        mLPPolDB.setEdorType(schema.getEdorType());
        mLPPolDB.setContNo(schema.getContNo());
        mLPPolDB.setInsuredNo(schema.getInsuredNo());
        LPPolSet set = mLPPolDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("查询被保人" + schema.getName() + "失败");
        }
        return set;
    }

    private int getLCPolCount(LPInsuredSchema schema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select count(1) ")
            .append("from LCPol ")
            .append("where contNo = '").append(schema.getContNo())
            .append("'   and insuredNo = '").append(schema.getInsuredNo())
            .append("' ");
        if(mExeSQL == null)
        {
            mExeSQL = new ExeSQL();
        }
        String polCount = mExeSQL.getOneValue(sql.toString());
        if(polCount.equals("") || polCount.equals("null"))
        {
            return 0;
        }
        return Integer.parseInt(polCount);
    }

    private int getLCInsuredCount(String contNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select count(1) ")
            .append("from LCInsured ")
            .append("where contNo = '").append(contNo)
            .append("' ");
        if(mExeSQL == null)
        {
            mExeSQL = new ExeSQL();
        }
        String insuredCount = mExeSQL.getOneValue(sql.toString());
        if(insuredCount.equals("") || insuredCount.equals("null"))
        {
            return 0;
        }
        return Integer.parseInt(insuredCount);
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo("20061226000008");

        VData data = new VData();
        data.add(gi);
        data.add(tLPEdorItemDB.query().get(1));

        PEdorXTConfirmBL bl = new PEdorXTConfirmBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("All OK");
        }
    }
}

