package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.vschema.LPInsuredSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class PEdorCFSaveDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mEdorState = BQ.EDORSTATE_INPUT;
    
    private String mEdorState2 = BQ.EDORSTATE_INIT;
    
    private LPInsuredSet mLPInsuredSet = null;

    public PEdorCFSaveDetailBL(GlobalInput gi, LPEdorItemSchema edorItem)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorItem.getEdorNo();
        this.mEdorType = edorItem.getEdorType();
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData(VData data)
    {
    	if (!getInputData(data))
        {
            return false;
        }
    	
    	
    	if(!checkData())
    	{
    		return false;
    	}
    	
    	if(!DealData(data))
    	{
    		return false;
    	}

        setEdorState();
        
        if (!submit())
        {
            return false;
        }
        return true;
    }
    
    private boolean DealData(VData data)
    {
    	if(data.size()!=0)
    	{
          for(int index = 1 ;index<=mLPInsuredSet.size();index++ )
         {
    	   LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
    	   tLPInsuredSchema = mLPInsuredSet.get(index);
    	   
    	   String sqlUpdateRelationship
    	   =" update lpinsured set RelationToAppnt='"+tLPInsuredSchema.getRelationToAppnt()+"'" +
    	   		",RelationToMainInsured='"+tLPInsuredSchema.getRelationToMainInsured()+"'," +
    	   				"modifydate=current date,modifytime=current time where " +
    	   				"edorno = '"+mEdorNo+"' and insuredno='"+tLPInsuredSchema.getInsuredNo()+"'";
    	   mMap.put(sqlUpdateRelationship, "UPDATE");  
         }
    	}
        return true;
    }
    

    /**
     * 设置item状态
     */
    private void setEdorState()
    {
        String sql = "update  LPEdorItem " +
                "set EdorState = '" + mEdorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }
    
    private boolean checkData()
    {
        LPPolDB tLPPolDB = new LPPolDB();
        LPPolSet tLPPolSet = new LPPolSet();
        tLPPolDB.setEdorNo(mEdorNo);
        tLPPolDB.setEdorType(mEdorType);
        tLPPolSet=tLPPolDB.query();
        if(tLPPolSet.size()==0)
        {
        	String sql = "update  LPEdorItem " +
            "set EdorState = '" + mEdorState2 + "', " +
            "    Operator = '" + mGlobalInput.Operator + "', " +
            "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
            "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
            "where  EdorNo = '" + mEdorNo + "' " +
            "and EdorType = '" + mEdorType + "' ";
             mMap.put(sql, "UPDATE");
             
              if (!submit())
              {
                return false;
               }
        	
        	mErrors.addOneError("您还没有拆分保单，请您先拆分保单。");
            return false;
        }
        return true;
    }
    
    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     */
    private boolean getInputData(VData data)
    {
    	
        try
        {
        	if(data.size()!=0)
        	{
            mLPInsuredSet  = (LPInsuredSet) data.getObjectByObjectName("LPInsuredSet", 0);
        	}
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入参数错误！");
            return false;
        }
        return true;
    }
    
    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
