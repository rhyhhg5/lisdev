package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: lis</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class EdorCTTestBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private String mGrpContNo = null;
    private String mEdorValiDate = null;
    private String mCurPayToDateLongPol = null;
    private String mOperator ;
    private EdorCalZTTestBL mEdorCalZTTestBL = new EdorCalZTTestBL();

    public EdorCTTestBL()
    {
    }

    /**
     * 对保障计划contPlanCode下的险种riskCode退保试算
     * @param contPlanCode String：保障计划
     * @param riskCode String：险种代码
     * @param needResultFlag boolean：是否需要试算结果
     * @return VData：包含总金额String和算费结果数据LPBudgetResult和财务数据LJSGetEndorseSet
     */
    public VData budget(String contPlanCode, String riskCode, boolean needResultFlag)
    {
        double getMoney = 0;
        LPBudgetResultSet tLPBudgetResultSet = new LPBudgetResultSet();
        LJSGetEndorseSet tLJSGetEndorseSet = new LJSGetEndorseSet();
        LGErrorLogSet tLGErrorLogSet =new  LGErrorLogSet();
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = null;
        int start = 1;
        int count = 500;

        do
        {
            tLCPolDB.setGrpContNo(mGrpContNo);
            tLCPolDB.setContPlanCode(contPlanCode);
            tLCPolDB.setRiskCode(riskCode);
           if(CommonBL.isNoNameGrpCont(mGrpContNo)) {
        	   tLCPolDB.setPolTypeFlag("1");
//           }else if(CommonBL.hasSpecialRisk(mGrpContNo) || CommonBL.isTDBCPol(riskCode)){
//        	   tLCPolDB.setPolTypeFlag("2");
        	}else if(CommonBL.hasSpecialRisk(mGrpContNo)){
            	   tLCPolDB.setPolTypeFlag("2");
           } 
            tLCPolSet = tLCPolDB.query(start, count);
            if(tLCPolDB.mErrors.needDealError())
            {
                mErrors.addOneError("查询险种出错");
                System.out.println(tLCPolDB.mErrors.getErrContent());
                return null;
            }
            if(tLCPolSet.size() == 0)
            {
                continue;
            }
            //不断循环险种计算退费
            VData tVData = budget(tLCPolSet, needResultFlag);
            if(tVData == null && mErrors.needDealError())
            {
                return null;
            }

            tLPBudgetResultSet.add((LPBudgetResultSet) tVData
                                   .getObjectByObjectName("LPBudgetResultSet", 0));
            tLJSGetEndorseSet.add((LJSGetEndorseSet) tVData
                                  .getObjectByObjectName("LJSGetEndorseSet", 0));
            tLGErrorLogSet.add((LGErrorLogSet) tVData
                                  .getObjectByObjectName("LGErrorLogSet", 0));
            getMoney += Double.parseDouble((String)tVData
                         .getObjectByObjectName("String", 0));

            start += count;
        }
        while(tLCPolSet.size() > 0);

        VData data = new VData();
        data.add(tLPBudgetResultSet);
        data.add(tLJSGetEndorseSet);
        data.add(tLGErrorLogSet);
        data.add(Double.toString(getMoney));

        return data;
    }

    /**
     * 对险种tLCPolSet中的所有险种退保试算
     * @param tLCPolSet LCPolSet
     * @param needResultFlag boolean
     * @return VData：包含总金额String和算费结果数据LPBudgetResult和财务数据LJSGetEndorseSet
     */
    public VData budget(LCPolSet tLCPolSet, boolean needResultFlag)
    {
        double getMoney = 0;
        LJSGetEndorseSet tLJSGetEndorseSet = new LJSGetEndorseSet();
        LPBudgetResultSet tLPBudgetResultSet = new LPBudgetResultSet();
        LGErrorLogSet sumLGErrorLogSet = new LGErrorLogSet();

        mEdorCalZTTestBL.setNeedBugetResultFlag(needResultFlag);
        mEdorCalZTTestBL.setEdorValiDate(this.mEdorValiDate);
        mEdorCalZTTestBL.setCurPayToDateLongPol(this.mCurPayToDateLongPol);
        mEdorCalZTTestBL.setOperator(mOperator);
        for(int i = 1; i <= tLCPolSet.size(); i++)
        {
            LJSGetEndorseSchema schema
                = mEdorCalZTTestBL.budgetOnePol(tLCPolSet.get(i));
            if(schema == null && mEdorCalZTTestBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(mEdorCalZTTestBL.mErrors);
                return null;
            }
            if(schema == null)
            {
                continue;
            }
            LGErrorLogSet tLGErrorLogSet = mEdorCalZTTestBL.getOnePolLGErrorLogSet();
            sumLGErrorLogSet.add(tLGErrorLogSet);
            getMoney += schema.getGetMoney();
            getMoney = PubFun.setPrecision(getMoney, "0.00");
            tLJSGetEndorseSet.add(schema);
            if(needResultFlag)
            {
                tLPBudgetResultSet.add(mEdorCalZTTestBL
                                       .getOnePolLPBudgetResult());

            }
        }
        VData data = new VData();
        data.add(tLJSGetEndorseSet);
        data.add(tLPBudgetResultSet);
        data.add(sumLGErrorLogSet);
        data.add(Double.toString(getMoney));

        return data;
    }

    /**
     * 设置是否需要试算结果标志
     * @param flag boolean
     */
    public void setGrpContNo(String grpContNo)
    {
        mGrpContNo = grpContNo;
    }

    public void setEdorValiDate(String edorValiDate)
    {
        mEdorValiDate = edorValiDate;
    }
    public void setOperator(String operator)
    {
       this.mOperator = operator;
    }
    public void setCurPayToDateLongPol(String curPayToDateLongPol)
    {
        mCurPayToDateLongPol = curPayToDateLongPol;
    }

    public static void main(String[] args)
    {
        String grpContNo = "0000491601";
//        String contNo = "00014808501";
        EdorCTTestBL tEdorCTTestBL = new EdorCTTestBL();
        tEdorCTTestBL.setEdorValiDate("2007-03-28");
        tEdorCTTestBL.setCurPayToDateLongPol("2008-03-20");
        tEdorCTTestBL.setGrpContNo(grpContNo);
        tEdorCTTestBL.setOperator("endor0");


//        LCPolDB tLCPolDB = new LCPolDB();
//        tLCPolDB.setContNo(contNo);
//        tLCPolDB.setGrpContNo(grpContNo);
//        LCPolSet set = tLCPolDB.query();
//        bl.budget(set, true);

        tEdorCTTestBL.budget("A", "170206", false);
    }
}
