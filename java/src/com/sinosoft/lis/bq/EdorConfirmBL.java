package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全确认逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class EdorConfirmBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    private VData pInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /**保全申请类型*/
    private String mContType;

    /** 全局数据 */
    private MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
    private LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
    private LPEdorMainSet mLPEdorMainSet = new LPEdorMainSet();
    private LPGrpEdorMainSet mLPGrpEdorMainSet = new LPGrpEdorMainSet();
    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    public EdorConfirmBL()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        if (!getInputData())
            return false;

       // if (!checkData())
       //     return false;

        if (!prepareData())
            return false;

        if (!dealData())
        {
            return false;
        }

        if (!prepareOutputData())
        {
            return false;
        }

        return true;
    }

    /**
     * 准备处理好的数据，返回给调用类
     * @return
     */
    private boolean prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
        mResult.add(map);
        return true;
    }

    /**
     * 业务逻辑处理方法
     * @return
     */
    public boolean dealData()
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        if (mOperate.equals("INSERT||EDORCONFIRM") ||
            mOperate.equals("INSERT||GRPEDORCONFIRM") ||
            mOperate.equals("INSERT||EDORACPTCONFIRM"))
        {
            pInputData = new VData();
            String tEdorNo = mLPEdorMainSchema.getEdorNo();
            for (int i = 1; i <= mLPEdorItemSet.size(); i++)
            {
                tLPEdorItemSchema = mLPEdorItemSet.get(i);
                String tEdorType = tLPEdorItemSchema.getEdorType();

                tLPEdorItemSchema.setEdorState("0");
                try
                {
                    Class tClass = Class.forName("com.sinosoft.lis.bq.PEdor" +
                                                 tEdorType +
                                                 "ConfirmBL");
                    EdorConfirm tEdorConfirm = (EdorConfirm)
                                               tClass.newInstance();
                    VData tVData = new VData();

                    tVData.add(mGlobalInput);
                    tVData.add(tLPEdorItemSchema);

                    if (!tEdorConfirm.submitData(tVData,
                                                 "CONFIRM||" + tEdorType))
                    {
                        CError.buildErr(this,
                                        "保全项目" + tEdorType + "申请时失败！失败原因：" +
                                        tEdorConfirm.mErrors.getFirstError());

                        return false;
                    }
                    else
                    {
                        VData rVData = tEdorConfirm.getResult();
                        MMap tMap = new MMap();
                        tMap = (MMap) rVData.getObjectByObjectName("MMap", 0);
                        if (tMap == null)
                        {
                            CError.buildErr(this,
                                            "得到保全项目为:" + tEdorType +
                                            "的保全确认结果时失败！");
                            return false;

                        }
                        else
                        {
                            map.add(tMap);
                        }
                    }

                }
                catch (ClassNotFoundException ex)
                {
                    map.put(tLPEdorItemSchema, "UPDATE");
                }
                catch (Exception ex)
                {
                    CError.buildErr(this, "保全项目" + tEdorType + "申请确认时失败！");
                    return false;
                }

            }
            mLPEdorMainSchema.setEdorState("0");
            mLPEdorMainSchema.setConfDate(theCurrentDate);
            mLPEdorMainSchema.setConfTime(theCurrentTime);
            mLPEdorMainSchema.setConfOperator(mGlobalInput.Operator);

            mLPEdorMainSet.add(mLPEdorMainSchema);
            map.put(mLPEdorMainSchema, "UPDATE");

            map.put("update LCCont set Prem = (select sum(Prem) from LCPol where LCCont.ContNo = LCPol.ContNo), "
                    + " Amnt = (select sum(Amnt) from LCPol where LCCont.ContNo = LCPol.ContNo), "
                    + " SumPrem = (select sum(SumPrem) from LCPol where LCCont.ContNo = LCPol.ContNo), "
                    + " Operator = '" + mGlobalInput.Operator
                    + "', ModifyDate = '" + theCurrentDate
                    + "', ModifyTime = '" + theCurrentTime
                    + "'  where LCCont.ContNo = '" +
                    mLPEdorMainSchema.getContNo() + "'", "UPDATE");


        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData()
    {
        try
        {
            mLPEdorMainSchema = (LPEdorMainSchema) mInputData.
                                getObjectByObjectName("LPEdorMainSchema", 0);
            mLPGrpEdorMainSchema = (LPGrpEdorMainSchema) mInputData.
                                   getObjectByObjectName("LPGrpEdorMainSchema",
                    0);
            mGlobalInput.setSchema((GlobalInput) mInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mContType = (String) mInputData.get(1);
            map = new MMap();
        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {
        /*
        if (mContType.equals("I"))
        {
            mGlobalInput.ManageCom = mLPEdorMainSchema.getManageCom();
        }
        else
        {
            mGlobalInput.ManageCom = mLPGrpEdorMainSchema.getManageCom();
        }
*/

        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = new LJSPaySet();

        tLJSPayDB.setOtherNo(mLPEdorMainSchema.getEdorNo());
        tLJSPayDB.setOtherNoType("3");
        tLJSPaySet = tLJSPayDB.query();

        if (tLJSPaySet.size() > 0)
        {
            String aGetNoticeNo = tLJSPaySet.get(1).getGetNoticeNo();
            LJFinaConfirm tLJFinaConfirm = new LJFinaConfirm(aGetNoticeNo,
                    "I");
            tLJFinaConfirm.setOperator(mGlobalInput.Operator);
            tLJFinaConfirm.setLimit(PubFun.getNoLimit(mGlobalInput.
                    ManageCom));
            System.out.println("start LJFinaConfirm...");
            if (!tLJFinaConfirm.submitData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "EdorConfirmBL";
                tError.functionName = "checkData";
                tError.errorMessage = "保全交费核销失败！请确认是否已交费！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        LJSGetDB tLJSGetDB = new LJSGetDB();
        LJSGetSet tLJSGetSet = new LJSGetSet();
        tLJSGetDB.setOtherNo(mLPEdorMainSchema.getEdorNo());
        tLJSGetDB.setOtherNoType("3");
        tLJSGetSet = tLJSGetDB.query();

        if (tLJSGetSet.size() > 0)
        {
            String aGetNoticeNo = tLJSGetSet.get(1).getGetNoticeNo();
            LJFinaConfirm tLJFinaConfirm = new LJFinaConfirm(aGetNoticeNo,
                    "O");
            tLJFinaConfirm.setOperator(mGlobalInput.Operator);
            tLJFinaConfirm.setLimit(PubFun.getNoLimit(mGlobalInput.
                    ManageCom));

            if (!tLJFinaConfirm.submitData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "EdorConfirmBL";
                tError.functionName = "checkData";
                tError.errorMessage = "保全退费核销失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        int m;
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();

        String sql = "select * from LPEdorItem "
                     + " where EdorNo='" +
                     mLPEdorMainSchema.getEdorNo() + "' and ContNo='" +
                     mLPEdorMainSchema.getContNo()
                     + "' order by MakeDate, MakeTime"; //去掉按EdorValiDate排序
        System.out.println(sql);
        mLPEdorItemSet.clear();
        mLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = mLPEdorItemSet.size();

        if (m > 0)
        {
            mInputData.clear();
            mInputData.addElement(mLPEdorItemSet);
            mInputData.addElement(mGlobalInput);
            mResult.clear();
            mResult.addElement(mLPEdorItemSet);
            mResult.addElement(mGlobalInput);
        }
        if (mContType.equals("G"))
        {
            mLPGrpEdorMainSet.clear();
            sql =  "select * from LPGrpEdorMain where edorState='2'  and EdorNo='" +
                    mLPGrpEdorMainSchema.getEdorNo() +
                    "' order by EdorValiDate,Makedate,MakeTime";
            LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
            mLPGrpEdorMainSet = tLPGrpEdorMainDB.executeQuery(sql);
            mInputData.addElement(mLPGrpEdorMainSet);
            mResult.addElement(mLPGrpEdorMainSet);
        }
        return true;

    }

    public static void main(String[] args)
    {
        VData tInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        EdorConfirmBL aEdorConfirmBL = new EdorConfirmBL();
        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tGlobalInput.ManageCom = "86110000";
        tGlobalInput.Operator = "Admin";


        tInputData.addElement(tLPEdorMainSchema);
        tInputData.addElement(tGlobalInput);

        aEdorConfirmBL.submitData(tInputData, "INSERT||EDORCONFIRM");
    }

}
