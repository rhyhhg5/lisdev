package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import java.util.HashMap;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 团单犹豫期撤保被保人清单
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author not attributable
 * @version 1.1
 */
public class PrtGrpInsuredListWTBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;
    private LGWorkSchema mLGWorkSchema = null;
    private LPEdorAppSchema mLPEdorAppSchema = null;

    private HashMap mHashMap = new HashMap();

    private XmlExport xmlexport;
    private ListTable mtListTable;
    private VData mResult = null;

    public PrtGrpInsuredListWTBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }
        //校验数据合法性
        if(!checkData())
        {
            return false;
        }
        //获取打印所需数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mLPEdorAppSchema = (LPEdorAppSchema) cInputData.
                           getObjectByObjectName("LPEdorAppSchema", 0);
        if (mLPEdorAppSchema == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("PrtGrpInsuredListWTBL->传入的数据不完整");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mLPEdorAppSchema.getEdorAcceptNo());
        if(!tLGWorkDB.getInfo())
        {
            mErrors.addOneError("查询公单信息出错。");
            return false;
        }
        mLGWorkSchema = tLGWorkDB.getSchema();

        mLPEdorAppSchema = getLPEdorAppInfo(mLPEdorAppSchema.getEdorAcceptNo());
        if (mLPEdorAppSchema == null)
        {
            return false;
        }

        return true;
    }

    /**
     * 得到保全受理信息
     * @param edorAcceptNo String
     * @return LPEdorAppSchema
     */
    private LPEdorAppSchema getLPEdorAppInfo(String edorAcceptNo)
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(edorAcceptNo);
        if(!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("没有查询到保全受理信息。");
            return null;
        }

        return tLPEdorAppDB.getSchema();
    }

    //获取打印所需数据
    private boolean getPrintData()
    {
        mtListTable = new ListTable();

        TextTag tag = new TextTag();
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PrtGrpInsuredListWT.vts", "printer"); //最好紧接着就初始化xml文档

        //得到公司名
        tag.add("GrpName", mLGWorkSchema.getCustomerName());
        String edorType = BQ.EDORTYPE_WT;
        tag.add("EdorName", CommonBL.getEdorInfo(edorType, "G").getEdorName());
        tag.add("EdorNo", mLPEdorAppSchema.getEdorAcceptNo());
        xmlexport.addTextTag(tag);

        ListTable tListTable = getListTable();
        if (tListTable == null)
        {
            return false;
        }
        String[] title =
                         {"序号", "被保人", "客户号", "性别", "出生日期",
                         "证件类型", "证件号码", "退费金额"};
        xmlexport.addListTable(tListTable, title);
        xmlexport.outputDocumentToFile("C:\\", "PrtGrpInsuredListZTBL");

        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     * 得到表格信息
     * @return ListTable
     */
    private ListTable getListTable()
    {
        //通过LPEdorMain得到保单号，然后得到公司名称
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorAcceptNo(mLPEdorAppSchema.getEdorAcceptNo());
        tLPGrpEdorItemDB.setEdorType(BQ.EDORTYPE_WT);
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();

        for(int i = 1; i <= tLPGrpEdorItemSet.size(); i++)
        {
            LPGrpEdorItemSchema schema = tLPGrpEdorItemSet.get(i);

            //为了在保全确认后查询，需要连LCInsured和LBInsured
            String sql = "select a.insuredName, a.insuredNo, "
                         + "  (select codeName from LDCode "
                         + "  where codeType = 'sex' and code = a.insuredSex),"
                         + "  a.insuredBirthday, b.IDType, b.IDNo,sum(a.prem), "
                         + "  a.polTypeFlag sort "
                         + "from LPPol a, LCInsured b "
                         + "where a.contNo = b.contNo "
                         + "  and a.insuredNo = b.insuredNo "
                         + "  and a.edorNo = '" + schema.getEdorNo() + "' "
                         + "  and a.edorType='" + schema.getEdorType() + "' "
                         + "  and a.grpContNo='" + schema.getGrpContNo() + "' "
                         + "group by a.insuredName, a.insuredNo, a.insuredSex, "
                         + "  a.insuredBirthday, b.IDType, b.IDNo, polTypeFlag "
                         + "union "

                         + "select a.insuredName, a.insuredNo, "
                         + "  (select codeName from LDCode "
                         + "  where codeType = 'sex' and code = a.insuredSex),"
                         + "  a.insuredBirthday, b.IDType, b.IDNo,sum(a.prem),"
                         + "  a.polTypeFlag sort "
                         + "from LPPol a, LBInsured b "
                         + "where a.contNo = b.contNo "
                         + "  and a.insuredNo = b.insuredNo "
                         + "  and a.edorNo = b.edorNo "
                         + "  and a.edorNo = '" + schema.getEdorNo() + "' "
                         + "  and a.edorType = '" + schema.getEdorType() + "' "
                         + "  and a.grpContNo = '" + schema.getGrpContNo() + "'"
                         + "group by a.insuredName, a.insuredNo, a.insuredSex, "
                         + "  a.insuredBirthday, b.IDType, b.IDNo, polTypeFlag "
                         + "order by sort ";
            System.out.println(sql);
            int start = 1;
            int count = 10000;
            SSRS tSSRS = new SSRS();

            do
            {
                tSSRS = new ExeSQL().execSQL(sql, start, count);
                if(tSSRS.getMaxRow() == 0 && start < count)
                {
                    CError tError = new CError();
                    tError.moduleName = "PrtGrpInsuredListWTBL";
                    tError.functionName = "getListTable";
                    tError.errorMessage = "没有查询到被保人信息";
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                    return null;
                }

                for(int t = 1; t <= tSSRS.getMaxRow(); t++)
                {
                    String[] info = new String[8];
                    info[0] = "" + t;
                    info[1] = tSSRS.GetText(t, 1);
                    info[2] = tSSRS.GetText(t, 2);
                    info[3] = tSSRS.GetText(t, 3);
                    info[4] = StrTool.cTrim(CommonBL
                                            .decodeDate(tSSRS.GetText(t, 4)));
                    info[5] = getIDType("IDType", tSSRS.GetText(t, 5));
                    info[6] = StrTool.cTrim(tSSRS.GetText(t, 6));
                    info[7] = String.valueOf(tSSRS.GetText(t, 7));
                    mtListTable.add(info);

                }
                start += count;
            }
            while(tSSRS.getMaxRow() > 0);
        }

        mtListTable.setName("WT");
        return mtListTable;
    }

    /**
     * 生成被保人信息
     * @param t int
     * @param tLPInsuredSchema LPInsuredSchema
     * @param accPayType String
     * @param insuredType String
     */
    private void getInsuredInfo(int t,
                                LPInsuredSchema tLPInsuredSchema,
                                String insuredType)
    {
        System.out.println("insured index:" + t);

    }

    private String getSexName(String sex)
    {
        if (sex == null)
        {
            return "";
        }
        if (sex.equals("0"))
        {
            return "男";
        }
        else if (sex.equals("1"))
        {
            return "女";
        }
        else
        {
            return "";
        }
    }

    /**
     * 得到类型为codeType，值为code所对应的汉字
     * 使用HashMap可以有效的避免频繁访库
     * @param codeType String
     * @param code String
     * @return String
     */
    private String getIDType(String codeType, String code)
    {
        String codeName;
        Object o = mHashMap.get(codeType + code);
        if(o == null)
        {
            String sql = "  select codeName "
                         + "from LDCode "
                         + "where codeType = '" + codeType.toLowerCase()
                         + "'  and code = '" + code + "' ";
            ExeSQL e = new ExeSQL();
            codeName = e.getOneValue(sql);
            mHashMap.put(codeType + code, codeName);
            return codeName;
        }
        codeName = (String) o;
        return codeName;
    }

    /**
     * 得到被保人的退费
     * @param tLPInsuredSchema LPInsuredSchema
     * @return double
     */
    private double getOneInsuredPrem(LPInsuredSchema tLPInsuredSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append(" select sum(prem) ")
            .append("from LPPol ")
            .append("where insuredNo = '")
            .append(tLPInsuredSchema.getInsuredNo())
            .append("'  and contNo = '").append(tLPInsuredSchema.getContNo())
            .append("'  and edorType = '").append(tLPInsuredSchema.getEdorType())
            .append("'  and edorNo = '").append(tLPInsuredSchema.getEdorNo())
            .append("' ");
        ExeSQL e = new ExeSQL();
        String sumPrem = e.getOneValue(sql.toString());
        if(sumPrem.equals("") || sumPrem.equals("null"))
        {
            sumPrem = "0";
        }

        return PubFun.setPrecision(Double.parseDouble(sumPrem), "0.00");
    }

    /**
     * 得到处理后的XML数据
     * @return XmlExport
     */
    public XmlExport getXmlExport()
    {
        return xmlexport;
    }

    public static void main(String[] args)
    {
        LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
        tLPEdorAppSchema.setEdorAcceptNo("20061025000004");

        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        VData data = new VData();
        data.add(gi);
        data.add(tLPEdorAppSchema);

        PrtGrpInsuredListWTBL bl = new PrtGrpInsuredListWTBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
