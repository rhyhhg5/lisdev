package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.LPGrpEdorMainDB;
import com.sinosoft.lis.db.LPGrpEdorItemDB;
import com.sinosoft.lis.db.LPEdorMainDB;
import com.sinosoft.lis.vschema.LPEdorMainSet;
import com.sinosoft.lis.db.LPEdorAppDB;

/**
 * <p>Title: </p>
 * <p>Description:个单保全申请撤销UI类
 * </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Lanjun
 * @version 1.0
 */

public class PGrpEdorCancelUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /** 撤销申请原因 */
  private String delReason;
  private String reasonCode;



  public PGrpEdorCancelUI() {}

  /**
   * 接收页面提交的数据
   * @param cInputData VData
   * @param cOperate String
   * @return boolean
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //System.out.println("删除原因是："+delReason);
    PGrpEdorCancelBL tPGrpEdorCancelBL = new PGrpEdorCancelBL();
    System.out.println("---UI BEGIN---"+mOperate);
    if (tPGrpEdorCancelBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tPGrpEdorCancelBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "tPGrpEdorCancelUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据撤销失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    return true;
  }


  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {

    LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
    LPEdorAppSet tLPEdorAppSet = new LPEdorAppSet();
    tLPEdorAppSet = tLPEdorAppDB.executeQuery("select * from LPEdorApp");

    for(int i = 1; i<=tLPEdorAppSet.size();i++)
    {

      GlobalInput tGlobalInput = new GlobalInput();
      tGlobalInput.Operator = "001";
      tGlobalInput.ComCode = "86";
      tGlobalInput.ManageCom = "86";

      PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();

      TransferData tTransferData = new TransferData();
      String delReason = "dfdsf";
      String reasonCode = "1";

      tTransferData.setNameAndValue("DelReason", delReason);
      tTransferData.setNameAndValue("ReasonCode", reasonCode);

      VData tVData = new VData();
      tVData.addElement(tGlobalInput);
      tVData.addElement(tLPEdorAppSet.get(i));
      tVData.addElement(tTransferData); //撤销申请原因
      //tVData.addElement(reasonCode);
      //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
      tPGrpEdorCancelUI.submitData(tVData, "I&EDORAPP");

    }


  }
}
