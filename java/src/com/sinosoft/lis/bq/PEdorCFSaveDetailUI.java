package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 保存明细</p>
 * <p>Description: 个单保存明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorCFSaveDetailUI
{
    private PEdorCFSaveDetailBL mPEdorCFSaveDetailBL = null;

    /**
     * 构造函数
     * @param edorAcceptNo String
     */
    public PEdorCFSaveDetailUI(GlobalInput gi, LPEdorItemSchema edorItem)
    {
    	mPEdorCFSaveDetailBL = new PEdorCFSaveDetailBL(gi, edorItem);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData(VData data)
    {
    	  System.out.println("OK6");
        if (!mPEdorCFSaveDetailBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mPEdorCFSaveDetailBL.mErrors.getFirstError();
    }
}
