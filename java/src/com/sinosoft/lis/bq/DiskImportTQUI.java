package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LPDiskImportSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class DiskImportTQUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportTQBL mDiskImportTQBL = null;

    public DiskImportTQUI()
    {
        mDiskImportTQBL = new DiskImportTQBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!mDiskImportTQBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportTQBL.mErrors);
            return false;
        }
        //执行成功当时部分人员不是本保单客户
        if(mDiskImportTQBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportTQBL.mErrors);
        }

        return true;
    }

    /**
     * 得到减人明细处理后成功导入的人数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportTQBL.getImportPersons();
    }

    /**
     * 得到减人明细处理后成功导入的人数
     * @return int
     */
     public int getNotImportPersons()//*******
    {
        return mDiskImportTQBL.mImportPersonsForTQCF;
    }
    /**
     * 得到重复人的信息
     * @return LPDiskImportSet
     */
    public LPDiskImportSet getLPDiskImportSetForTQCF(){
        return mDiskImportTQBL.mLPDiskImportSetForTQCF;
    }

    /**
     * 传给减人明细处理类的人数
     * @return int
     */
    public int getImportPersonsForTQ()
    {
        return mDiskImportTQBL.getImportPersonsForTQ();
    }
    /**
     * 获得重复人信息,姓名,证件号
     * @return String
     */
    public String getDiskImportName(){
        String message=" ";

        for(int i=1;i<=mDiskImportTQBL.mLPDiskImportSetForTQCF.size();i++)
        {
          message +="姓名: "+ mDiskImportTQBL.mLPDiskImportSetForTQCF.get(i).getInsuredName()
                  +" , "
                  + "证件号: "+mDiskImportTQBL.mLPDiskImportSetForTQCF.get(i).getIDNo()+"<br>";

        }
        return message;
    }

    public static void main(String[] args)
    {
        DiskImportTQUI ztdiskimportui = new DiskImportTQUI();
    }
}
