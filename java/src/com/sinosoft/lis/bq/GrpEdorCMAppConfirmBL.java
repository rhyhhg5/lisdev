package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 处理团单客户资料变更到导入的联系电话</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author lzy
 * @version
 */

public class GrpEdorCMAppConfirmBL implements EdorAppConfirm
{
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;  //保全项目信息
    private GlobalInput mGI = null;  //完整的操作员信息
    private String mGrpContNo = null;
    private String mEdorNo =  null;
    private String mEdorType = null;
    private String mCurDate = PubFun.getCurrentDate();//当前日期
    private String mCurTime = PubFun.getCurrentTime();//当前时间
    private LPDiskImportSet mLPDiskImportSet=null;
    private MMap map = new MMap();

    public GrpEdorCMAppConfirmBL()
    {
    }

    /**
     * 操作的提交方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象，包含：
     A.	GlobalInput对象，完整的登陆用户信息。
     B.	LPGrpEdorItem对象，保全项目信息。
     * @param cOperate 数据操作字符串，主要包括""和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("G--CM appconfirm");
        //得到外部传入的数据
        if (!getInputData(cInputData))
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 得到处理后的数据集
     * @return VData：处理后的数据集，失败为null
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(map);

        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData
                               .getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mGI = ((GlobalInput) cInputData.
                               getObjectByObjectName("GlobalInput", 0));
        if (mLPGrpEdorItemSchema == null || mGI == null)
        {
            this.mErrors.addOneError("接收数据失败");
            return false;
        }

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1)
        {
            mErrors.addOneError(new CError("查询保全项目信息失败！"));
            System.out.println(tLPGrpEdorItemDB.mErrors);
            return false;
        }
        mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemSet.get(1));
        mGrpContNo=mLPGrpEdorItemSchema.getGrpContNo();
        mEdorNo=mLPGrpEdorItemSchema.getEdorNo();
        mEdorType=mLPGrpEdorItemSchema.getEdorType();
        return true;
    }

    /**
     * 处理团单退保理算业务逻辑，对保全明细录入的数据进行预算，得到客户交退费信息：
     i.	按险种生成险种缴费信息LJSGetEndorse，若险种有加费，则同时生成加费的退费信息LJSGetEndore，为销售提数作准备。
     ii.生成工本费财务信息。
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
  {
      if(!checkData())
      {
          return false;
      }

      if(!dealInsuredPhone())
      {
          return false;
      }
      setGrpEdorItem();

      return true;
    }
    /**
     * -
     * @return boolean
     */
   private boolean dealInsuredPhone()
   {
	   mLPDiskImportSet=CommonBL.getLPDiskImportSet(mEdorNo, mEdorType, mGrpContNo, BQ.IMPORTSTATE_SUCC);
	   if(mLPDiskImportSet.size()<=0){
		   System.out.println("**00** 工单"+mLPGrpEdorItemSchema.getEdorNo()+"没有查询到有效的导入数据,不处理了。。。。");
		   return true;
	   }
	   ExeSQL tExeSQL = new ExeSQL();
	   Reflections ref = new Reflections();
	   for(int i=1; i<=mLPDiskImportSet.size(); i++){
		   LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
		   String query="select * from LCInsured where grpcontno='"+mGrpContNo+"' and insuredno='"+tLPDiskImportSchema.getInsuredNo()+"' ";
           LCInsuredDB tLCInsuredDB=new LCInsuredDB();
           LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
           LCInsuredSet tLCInsuredSet=tLCInsuredDB.executeQuery(query);
           if(tLCInsuredSet.size() > 0){
        	   tLCInsuredSchema=tLCInsuredSet.get(1);
           }else{
        	   mErrors.addOneError(new CError("查询被保险人"+tLPDiskImportSchema.getInsuredNo()+"信息失败！"));
        	   return false;
           }
           
           LPInsuredSchema tLPInsuredSchema = null;
           //若存在P表说明在录入时已经添加了个人保全，直接修改联系电话，没有的再生成P表
           String strSQL="select * from LPInsured where EdorNo='"+mEdorNo+"' "
           		+" and EdorType='"+mEdorType+"' and Insuredno='"+tLPDiskImportSchema.getInsuredNo()+"' ";
           LPInsuredDB tLPInsuredDB = new LPInsuredDB();
           LPInsuredSet tLPInsuredSet=tLPInsuredDB.executeQuery(strSQL);
           if(tLPInsuredSet.size() > 0 ){
        	   //或许明细时有关联其他团单的被保人
        	   for(int j=1; j<=tLPInsuredSet.size(); j++){
        		   tLPInsuredSchema = new LPInsuredSchema();
        		   tLPInsuredSchema=tLPInsuredSet.get(j);
        		   tLPInsuredSchema.setGrpInsuredPhone(tLPDiskImportSchema.getPhone());
        		   tLPInsuredSchema.setModifyDate(mCurDate);
                   tLPInsuredSchema.setModifyTime(mCurTime);
                   map.put(tLPInsuredSchema, SysConst.DELETE_AND_INSERT);
        	   }
           }else{
        	   //模板导入的就不再关联其他团单的被保人了。
        	   tLPInsuredSchema = new LPInsuredSchema();
               ref.transFields(tLPInsuredSchema, tLCInsuredSchema);
               tLPInsuredSchema.setGrpInsuredPhone(tLPDiskImportSchema.getPhone());
               tLPInsuredSchema.setEdorNo(mEdorNo);
               tLPInsuredSchema.setEdorType(mEdorType);
               tLPInsuredSchema.setMakeDate(mCurDate);
               tLPInsuredSchema.setMakeTime(mCurTime);
               tLPInsuredSchema.setModifyDate(mCurDate);
               tLPInsuredSchema.setModifyTime(mCurTime);
               tLPInsuredSchema.setOperator(mGI.Operator);
               map.put(tLPInsuredSchema, SysConst.DELETE_AND_INSERT);
           }
	   }
	   
	   
       return true;
   }
   
   
   private void setGrpEdorItem()
   {
       String wherePart = "where EdorNo='" +
               mEdorNo + "' and EdorType='" +
               mEdorType + "'";
       String updateSql =  "update LPGrpEdorItem set ChgPrem= nvl((select sum(ChgPrem) from LPEdorItem "
               + wherePart + "),0), "
               + "ChgAmnt= nvl((select sum(ChgAmnt) from LPEdorItem "
               + wherePart + "),0), "
               +
               "GetMoney= nvl((select sum(GetMoney) from LPEdorItem "
               + wherePart + "),0), "
               +
               "GetInterest= nvl((select sum(GetInterest) from LPEdorItem "
               + wherePart + "),0) "
               + wherePart;
       map.put(updateSql, "UPDATE");
   }

    /**
     * 校验是否可以理算
     * @return boolean：成功true，否则false
     */
     private boolean checkData()
     {
        if(!mLPGrpEdorItemSchema.getEdorState().equals("1"))
        {
            this.mErrors.addOneError("当前状态无法进行理算!");
            return false;
        }
        return true;
	}
      
}
