package com.sinosoft.lis.bq;

import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 客户资料变更
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Tjj
 * @ReWrite ZhangRong,Luomin
 * @author rewrite by QiuYang 2005
 * @version 1.0
 */

public class GEdorRRDetailBL {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	private MMap mMap = new MMap();

	private DetailDataQuery mQuery = null;

	private GlobalInput mGlobalInput = null;

	private String mTypeFlag = null;

//	90天满期标志
	private String mFlag = "";

	private String mEdorNo = null;

	private String mEdorType = null;

	private String mResumeDate = null;

	private String mGrpContNo = null;

	//本次暂停时间/天
	int mIntv=0;
	
//  保单本次的暂停日期
    String mStopDate=null;
	
	private LCGrpContStateSchema mLCGrpContStateSchema = new LCGrpContStateSchema();

	private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();

    /** 修改数据 */
	private LPGrpContSchema mLPGrpContSchema = new LPGrpContSchema();
	private LPGrpPolSet mLPGrpPolSet = new LPGrpPolSet();
    private LPPolSet mLPPolSet = new LPPolSet();
    private LPDutySet mLPDutySet = new LPDutySet();
    private LPPremSet mLPPremSet = new LPPremSet();
    private LPGetSet mLPGetSet = new LPGetSet();
    private LPContSet mLPContSet = new LPContSet();
   
    
	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/**
	 * 提交数据
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	public boolean submitData(VData cInputData) {
		System.out.println("Begin --------PEdorJMDetailBL");
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData())// 检验数据未处理
		{
			return false;
		}

		if (!dealData()) {
			return false;
		}

		if (!submit()) {
			return false;
		}
		return true;
	}

	/**
	 * 得到传入数据
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		try {
			mLCGrpContStateSchema = (LCGrpContStateSchema) cInputData
					.getObjectByObjectName("LCGrpContStateSchema", 0);
			TransferData mTransferData = new TransferData();
			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
			mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
					"GlobalInput", 0);

			this.mEdorNo = mLCGrpContStateSchema.getOtherNo();
			this.mEdorType = mLCGrpContStateSchema.getOtherNoType();
			this.mResumeDate = mLCGrpContStateSchema.getEndDate();
			this.mGrpContNo = mLCGrpContStateSchema.getGrpContNo();
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	/*
     * 
     * */
	private String getOccupationType(String cOccupationCode) {
		String sql = "select OccupationType from LDOccupation where '1218610553000'='1218610553000' and  OccupationCode = '"
				+ cOccupationCode + "'  fetch first 2000 rows only ";
		ExeSQL tExeSQL = new ExeSQL();
		String tOccupationType = tExeSQL.getOneValue(sql);
		return tOccupationType;
	}

	/**
	 * 校验数据
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		//查询 LCGrpContState
        String strSql = "select * from LCGrpContState where statetype = 'Stop' and state = '1' "
            +"and enddate is null and GrpContNo = '"+mGrpContNo+"'";
        LCGrpContStateDB tLCGrpContStateDB = new LCGrpContStateDB();
        LCGrpContStateSchema tLCGrpContStateSchema = new LCGrpContStateSchema();
        LCGrpContStateSet tLCGrpContStateSet = new LCGrpContStateSet();
        tLCGrpContStateSet=tLCGrpContStateDB.executeQuery(strSql);
        if(tLCGrpContStateSet.size()<1){
        	CError.buildErr(this, "该团单之前未操作过保单暂停！");
            return false;
        }
        
        try
        {
        	tLCGrpContStateSchema=tLCGrpContStateSet.get(1);
        }
        catch (Exception ex)
        {
            CError.buildErr(this, "查询团体保单状态表出现异常！");
            return false;
        }
        
//      保单暂停日期
        mStopDate = tLCGrpContStateSchema.getStartDate(); 
        
//      暂停间隔
        mIntv = PubFun.calInterval(mStopDate, mResumeDate, "D"); 
        
//      总暂停间隔
        int tIntvAll=mIntv;
        
        String strSqlall = "select * from LCGrpContState where statetype = 'Stop' and state = '1' "
            +"and enddate is not null and startdate is not null and GrpContNo = '"+mGrpContNo+"'";
        LCGrpContStateSet tLCGrpContStateSetall = new LCGrpContStateSet();
        tLCGrpContStateSetall=tLCGrpContStateDB.executeQuery(strSqlall);
        if(tLCGrpContStateSetall.size()>0){
        	for (int i = 0; i < tLCGrpContStateSetall.size(); i++) {
        		LCGrpContStateSchema aLCGrpContStateSchema = new LCGrpContStateSchema();
        		aLCGrpContStateSchema=tLCGrpContStateSetall.get(i+1);
        		tIntvAll+=PubFun.calInterval(aLCGrpContStateSchema.getStartDate(), aLCGrpContStateSchema.getEndDate(), "D"); 
			}
        }
        ExeSQL texesql = new ExeSQL();
        String jianGongXian = texesql.getOneValue("select 1 from lcgrppol where  grpcontno='"+ mGrpContNo + "'  and riskcode not in ('190106','590206','5901','590401','190401') ");
		if ((jianGongXian == null) || (jianGongXian.equals(""))) {
			if(tIntvAll>180){
	        	CError.buildErr(this, "保单的总暂停时间已经超过180天,请重新输入保单恢复时间！");
	            return false;
	        }
			else
			{
				if(tIntvAll==180){
		        	mFlag="1";
		        }
			}
		}
		else {
				if(tIntvAll>90)
				{
		        	CError.buildErr(this, "保单的总暂停时间已经超过90天,请重新输入保单恢复时间！");
		            return false;
				}
				else
				{
					if(tIntvAll==90){
			        	mFlag="1";
			        }  
				}
        }
              
		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

        
        String tSql = "";
        ExeSQL tExeSQL = new ExeSQL();
        tSql = "SELECT max(enddate)"
            + " FROM lcpol"
            + " WHERE GrpContNo='" + mGrpContNo + "' ";
        String tEndDate = tExeSQL.getOneValue(tSql); //暂停前保单终止日期
        if (tEndDate == null || tEndDate.equals(""))
        {
            System.out.println("查询保单原终止日期时错误！");
            CError tError = new CError();
            tError.moduleName = "GrpEdorRRDetailBL";
            tError.errorMessage = "查询保单原终止日期时错误！！";
            this.mErrors.addOneError(tError);
            return false;
        }
		//暂停后保单终止日期
        String nEndDate = "";
        

//      恢复后的保单新的终止日期
        nEndDate = PubFun.calDate(tEndDate, mIntv, "D", null); 
        
        
        Reflections tReflections=new Reflections();
		
//      修改LCGrpCont
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
        if(!tLCGrpContDB.getInfo()){
        	System.out.println("查询原保单错误！");
            CError tError = new CError();
            tError.moduleName = "GrpEdorRRDetailBL";
            tError.errorMessage = "查询原保单错误！！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCGrpContSchema = tLCGrpContDB.getSchema();
        tReflections.transFields(mLPGrpContSchema, mLCGrpContSchema);
        mLPGrpContSchema.setEdorNo(mEdorNo);
        mLPGrpContSchema.setEdorType(mEdorType);
        //团单Cinvalidate比终止日早一天,因为终止日是计算的0时,cinvalidate计算的是24时
        mLPGrpContSchema.setCInValiDate(PubFun.calDate(nEndDate, -1, "D", null));
        mLPGrpContSchema.setModifyDate(mCurrentDate);
        mLPGrpContSchema.setModifyTime(mCurrentTime);
        mLPGrpContSchema.setOperator(mGlobalInput.Operator);
        
        
      //修改LCGrpPol、LCCont、LCPol、LCDuty、LCGet、LCPrem
        /*－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
         * No.1 更新LCGrpPol表
         * －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
         */
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
        mLCGrpPolSet = tLCGrpPolDB.query();
        for (int n = 1; n <= mLCGrpPolSet.size(); n++)
        {
            String tGrpPolno = mLCGrpPolSet.get(n).getGrpPolNo();

            LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();

            mLCGrpPolSchema = mLCGrpPolSet.get(n);

            mLCGrpPolSchema = mLCGrpPolSet.get(n);
            LPGrpPolSchema mLPGrpPolSchema = new LPGrpPolSchema();
            tReflections.transFields(mLPGrpPolSchema, mLCGrpPolSchema);
            mLPGrpPolSchema.setEdorNo(mEdorNo);
            mLPGrpPolSchema.setEdorType(mEdorType);
            mLPGrpPolSchema.setPayEndDate(nEndDate);
            mLPGrpPolSchema.setPaytoDate(nEndDate);
            mLPGrpPolSchema.setModifyDate(mCurrentDate);
            mLPGrpPolSchema.setModifyTime(mCurrentTime);
            mLPGrpPolSchema.setOperator(mGlobalInput.Operator);
            mLPGrpPolSet.add(mLPGrpPolSchema);

            /*－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
             * No.2 插入LCPol表
             * －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
             */
            LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet tLCPolSet = new LCPolSet();
            tSql = "select * from lcpol where grpcontno = '" +
                mGrpContNo +
                "' and grppolno = '" + tGrpPolno + "'";
            tLCPolSet = tLCPolDB.executeQuery(tSql);
            String tContNo = "";

            for (int i = 1; i <= tLCPolSet.size(); i++)
            {
                LCPolSchema tLCPolSchema = new LCPolSchema();
                LPPolSchema tLPPolSchema = new LPPolSchema();
                tLCPolSchema = tLCPolSet.get(i);
                tReflections.transFields(tLPPolSchema, tLCPolSchema);

                String tPolNo = tLCPolSet.get(i).getPolNo();
                tContNo = tLCPolSet.get(i).getContNo();
                tLPPolSchema.setEdorNo(mEdorNo);
                tLPPolSchema.setEdorType(mEdorType);
                tLPPolSchema.setModifyDate(mCurrentDate);
                tLPPolSchema.setModifyTime(mCurrentTime);
                tLPPolSchema.setPayEndDate(nEndDate);
                tLPPolSchema.setPaytoDate(nEndDate);
                tLPPolSchema.setEndDate(nEndDate);
                tLPPolSchema.setAcciEndDate(nEndDate);
                tLPPolSchema.setOperator(mGlobalInput.Operator);
                mLPPolSet.add(tLPPolSchema);
                /*－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
                 * No.3 插入LCDuty表
                 * －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
                 */
                LCDutyDB tLCDutyDB = new LCDutyDB();
                LCDutySet tLCDutySet = new LCDutySet();

                tSql = "select * from lcduty where polno ='" + tPolNo +
                    "'";
                tLCDutySet = tLCDutyDB.executeQuery(tSql);
                if (tLCDutySet.size() < 1)
                {
                    mErrors.addOneError("查询个人险种号[" + tPolNo +
                                        "]的LCDuty表失败！");
                    return false;
                }
                for (int t = 1; t <= tLCDutySet.size(); t++)
                {
                    LCDutySchema tLCDutySchema = new LCDutySchema();
                    LPDutySchema tLPDutySchema = new LPDutySchema();
                    tLCDutySchema = tLCDutySet.get(t);
                    tReflections.transFields(tLPDutySchema, tLCDutySchema);
                    tLPDutySchema.setPayEndDate(nEndDate);
                    tLPDutySchema.setPaytoDate(nEndDate);
                    tLPDutySchema.setModifyDate(mCurrentDate);
                    tLPDutySchema.setModifyTime(mCurrentTime);
                    tLPDutySchema.setEdorNo(mEdorNo);
                    tLPDutySchema.setEdorType(mEdorType);
                    tLPDutySchema.setOperator(mGlobalInput.Operator);
                    mLPDutySet.add(tLPDutySchema);
                }
                /*－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
                 * No.4 插入LCPrem表
                 * －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
                 */
                LCPremDB tLCPremDB = new LCPremDB();
                LCPremSet tLCPremSet = new LCPremSet();
                tSql = "select * from lcprem where polno ='" + tPolNo +
                    "'";
                tLCPremSet = tLCPremDB.executeQuery(tSql);
                if (tLCPremSet.size() < 1)
                {
                    mErrors.addOneError("查询个人险种号[" + tPolNo +
                                        "]的LCPrem表失败！");
                    return false;
                }
                for (int t = 1; t <= tLCPremSet.size(); t++)
                {
                    LCPremSchema tLCPremSchema = new LCPremSchema();
                    LPPremSchema tLPPremSchema = new LPPremSchema();
                    tLCPremSchema = tLCPremSet.get(t);
                    tReflections.transFields(tLPPremSchema, tLCPremSchema);
                    tLPPremSchema.setPayEndDate(nEndDate);
                    tLPPremSchema.setPaytoDate(nEndDate);
                    tLPPremSchema.setModifyDate(mCurrentDate);
                    tLPPremSchema.setModifyTime(mCurrentTime);
                    tLPPremSchema.setEdorNo(mEdorNo);
                    tLPPremSchema.setEdorType(mEdorType);
                    tLPPremSchema.setOperator(mGlobalInput.Operator);
                    mLPPremSet.add(tLPPremSchema);
                }

                /*－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
                 * No.5 插入LCGet表
                 * －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
                 */
                LCGetDB tLCGetDB = new LCGetDB();
                LCGetSet tLCGetSet = new LCGetSet();
                tSql = "select * from lcget where polno ='" + tPolNo +
                    "'";
                tLCGetSet = tLCGetDB.executeQuery(tSql);
                if (tLCGetSet.size() < 1)
                {
                    mErrors.addOneError("查询个人险种号[" + tPolNo +
                                        "]的LCGet表失败！");
                    return false;
                }
                for (int t = 1; t <= tLCGetSet.size(); t++)
                {
                    LCGetSchema tLCGetSchema = new LCGetSchema();
                    LPGetSchema tLPGetSchema = new LPGetSchema();
                    tLCGetSchema = tLCGetSet.get(t);
                    tReflections.transFields(tLPGetSchema, tLCGetSchema);
                    tLPGetSchema.setGetEndDate(nEndDate);
                    tLPGetSchema.setModifyDate(mCurrentDate);
                    tLPGetSchema.setModifyTime(mCurrentTime);
                    tLPGetSchema.setEdorNo(mEdorNo);
                    tLPGetSchema.setEdorType(mEdorType);
                    tLPGetSchema.setOperator(mGlobalInput.Operator);
                    mLPGetSet.add(tLPGetSchema);
                }
            }
        }

        /*－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
         * No.6 插入LCCont表
         * －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
         */

        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = new LCContSet();
        tSql = "select * from lccont where grpcontno ='" + mGrpContNo +
            "'";
        tLCContSet = tLCContDB.executeQuery(tSql);
        if (tLCContSet.size() < 1)
        {
            mErrors.addOneError("查询个人保单号[" + mGrpContNo +
                                "]的LCCont表失败！");
            return false;
        }
        for (int t = 1; t <= tLCContSet.size(); t++)
        {
            LCContSchema tLCContSchema = new LCContSchema();
            LPContSchema tLPContSchema = new LPContSchema();
            tLCContSchema = tLCContSet.get(t);
            tReflections.transFields(tLPContSchema, tLCContSchema);
            tLPContSchema.setPaytoDate(nEndDate);
            tLPContSchema.setModifyDate(mCurrentDate);
            tLPContSchema.setModifyTime(mCurrentTime);
            tLPContSchema.setEdorNo(mEdorNo);
            tLPContSchema.setEdorType(mEdorType);
            tLPContSchema.setOperator(mGlobalInput.Operator);
            mLPContSet.add(tLPContSchema);
        }
            
        mMap.put(mLPGrpContSchema, "DELETE&INSERT");
        mMap.put(mLPGrpPolSet, "DELETE&INSERT");
        mMap.put(mLPPolSet, "DELETE&INSERT");
        mMap.put(mLPGetSet, "DELETE&INSERT");
        mMap.put(mLPDutySet, "DELETE&INSERT");
        mMap.put(mLPPremSet, "DELETE&INSERT");
        mMap.put(mLPContSet, "DELETE&INSERT");

//        安邦使用
//        String SQL = "insert into lpcont (select '" + mEdorNo + "','" + mEdorType + "',a.* from lccont a where contno in (select distinct contno from lppol where edorno = '" + mEdorNo + "' and edortype = '" + mEdorType + "' and grpcontno = '" + mGrpContNo + "'))";
//        mMap.put("delete from lpcont where edorno = '" + mEdorNo + "' and edortype = '" + mEdorType + "' and grpcontno = '" + mGrpContNo + "'","DELETE");
//        mMap.put(SQL,"INSERT");
//        mMap.put("update lpcont a set paytodate='" + nEndDate + "',ModifyDate='" + mCurrentDate + "',ModifyTime='" + mCurrentTime + "',Operator = '" + mGlobalInput.Operator + "' where edorno = '" + mEdorNo + "' and edortype = '" + mEdorType + "' and grpcontno = '" + mGrpContNo + "'","UPDATE");

        //垃圾处理
        tReflections = null;

		mLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorNo);
		mLPEdorEspecialDataSchema.setEdorNo(mEdorNo);
		mLPEdorEspecialDataSchema.setEdorType(mEdorType);
		mLPEdorEspecialDataSchema.setDetailType("RESUMEDATE");
		mLPEdorEspecialDataSchema.setEdorValue(mResumeDate);
		mLPEdorEspecialDataSchema.setPolNo(mGrpContNo);

		mMap.put(mLPEdorEspecialDataSchema, "DELETE&INSERT");
		
		if(mFlag.equals("1")){
			mLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorNo);
			mLPEdorEspecialDataSchema.setEdorNo(mEdorNo);
			mLPEdorEspecialDataSchema.setEdorType(mEdorType);
			mLPEdorEspecialDataSchema.setDetailType("FLAG");
			mLPEdorEspecialDataSchema.setEdorValue("1");
			mLPEdorEspecialDataSchema.setPolNo(mGrpContNo);

			mMap.put(mLPEdorEspecialDataSchema, "DELETE&INSERT");
		}

		setEdorState("1");
		return true;
	}

	/**
	 * 校验是否暂停期满
	 * 
	 * @return boolean
	 */
	private boolean CanStop() {
		ExeSQL texesql = new ExeSQL();
		String tIsEnd = texesql
				.getOneValue("select 1 from lcgrpedor where  grpcontno='"
						+ mGrpContNo + "' and ValidFlag='1' and state='0' ");
		if ((tIsEnd == null) || (tIsEnd.equals(""))) {
			return false;
		}
		LCGrpContStateDB tLCGrpContStateDB = new LCGrpContStateDB();
        tLCGrpContStateDB.setGrpContNo(mGrpContNo);
        tLCGrpContStateDB.setStateType("Stop"); //复用注意修改
        tLCGrpContStateDB.setEndDate("");
		return true;
	}

	private void setLPEdoreSpecialData(String ZiDuanName, String value) {
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchema.setEdorNo(mEdorNo);
		tLPEdorEspecialDataSchema.setEdorType(mEdorType);
		tLPEdorEspecialDataSchema.setPolNo("000000");
		tLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorNo);
		tLPEdorEspecialDataSchema.setDetailType(ZiDuanName);
		tLPEdorEspecialDataSchema.setEdorValue(value);
		mMap.put(tLPEdorEspecialDataSchema, "DELETE&INSERT");
	}

	/**
	 * 把保全状态设为已录入
	 * 
	 * @param edorState
	 *            String
	 */
	private void setEdorState(String edorState) {
		String sqlgrpitem = "update  LPgrpEdorItem " + "set EdorState = '"
				+ edorState + "', " + "    Operator = '"
				+ mGlobalInput.Operator + "', " + "    ModifyDate = '"
				+ mCurrentDate + "', " + "    ModifyTime = '" + mCurrentTime
				+ "' " + "where  EdorNo = '" + mEdorNo + "' "
				+ "and EdorType = '" + mEdorType + "' ";
		mMap.put(sqlgrpitem, "UPDATE");
		String sqlgrpmain = "update  LPgrpEdormain " + "set EdorState = '"
				+ edorState + "', " + "    Operator = '"
				+ mGlobalInput.Operator + "', " + "    ModifyDate = '"
				+ mCurrentDate + "', " + "    ModifyTime = '" + mCurrentTime
				+ "' " + "where  EdorNo = '" + mEdorNo + "' ";
		mMap.put(sqlgrpmain, "UPDATE");
		String sqlapp = "update  LPEdorapp " + "set EdorState = '" + edorState
				+ "', " + "    Operator = '" + mGlobalInput.Operator + "', "
				+ "    ModifyDate = '" + mCurrentDate + "', "
				+ "    ModifyTime = '" + mCurrentTime + "' "
				+ "where  EdoracceptNo = '" + mEdorNo + "' ";
		mMap.put(sqlapp, "UPDATE");
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit() {
		VData data = new VData();
		data.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}
}
