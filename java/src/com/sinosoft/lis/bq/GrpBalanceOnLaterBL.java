package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.*;
import com.sinosoft.utility.*;
import java.util.GregorianCalendar;

/**
 * <p>Title: Web业务系统</p>
 *
 * <p>Description:保全延期结算处理类 </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author  Huxl 2006-8-25
 * @version 1.0
 */
public class GrpBalanceOnLaterBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /** 团体合同号 */
    private String mGrpContNo = null;
    /** 工单号 */
    private String mDetailWorkNo = null;
    /**延期结算日期*/
    private String mLaterPayDate = null;
    /** 审批*/
    private String mFailType = BQ.FALSE;
    /** 应收号*/
    private String mActuNo = null;
    

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();
    public LJAPaySet mLJAPaySet;
    public LJAGetSet mLJAGetSet;
    public LCGrpBalPlanDB mLCGrpBalPlanDB;

    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public GrpBalanceOnLaterBL() {
    }

    /**
     * 前台界面调用接口
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }
        System.out.println("---End getInputData---");

        if (!checkData()) {
            return false;
        }
        // 根据业务逻辑对数据进行处理
        if (!this.dealData()) {
            return false;
        }

        //数据准备操作
        System.out.println("---End dealData---");
        if (!prepareOutputData()) {
            return false;
        }

        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnLaterBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获取前台传入的参数:保单号，受理号，延期结算日期。
     * @return boolean
     */
    private boolean getInputData() {
        try {
            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            TransferData tTransferData = (TransferData) mInputData.
                                         getObjectByObjectName("TransferData",
                    0);
            mGrpContNo = (String) tTransferData.getValueByName("GrpNo");
            mDetailWorkNo = (String) tTransferData.getValueByName(
                    "DetailWorkNo");
            mLaterPayDate = (String) tTransferData.getValueByName(
                    "LaterPayDate");
        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }
        return true;
    }

    /**
     * 校验该单是否为定期结算，延期结算日期是否不超过下次结算日期
     * @return boolean
     */
    private boolean checkData() {
        mLCGrpBalPlanDB = new LCGrpBalPlanDB();
        mLCGrpBalPlanDB.setGrpContNo(mGrpContNo);
        if (!mLCGrpBalPlanDB.getInfo()) {
            this.mErrors.copyAllErrors(mLCGrpBalPlanDB.mErrors);
            return false;
        }
        if (mLCGrpBalPlanDB.getBalIntv() == 0) {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTimeBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保单的结算频次为随时结算，不需要做延期结算!";
            this.mErrors.addOneError(tError);
            return false;
        }
        GrpBalanceOnTime tGrpBalanceOnTime = new GrpBalanceOnTime();
        String theNextToDate = tGrpBalanceOnTime.getNextDate(mLCGrpBalPlanDB.
                getBalStartDate(), mLCGrpBalPlanDB.getBalIntv(), "M",
                mCurrentDate);
        GregorianCalendar mCalendar = new GregorianCalendar();
        GregorianCalendar cCalendar = new GregorianCalendar();
        FDate tFDate = new FDate();
        mCalendar.setTime(tFDate.getDate(theNextToDate));
        cCalendar.setTime(tFDate.getDate(mLaterPayDate));
        if (cCalendar.after(mCalendar)) {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTimeBL";
            tError.functionName = "checkData";
            tError.errorMessage = "延期结算日期不得晚于下次定期结算日期!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 延期结算处理类
     * @return boolean
     */
    private boolean dealData() {
        if (!autoExamination()) {
            return false;
        }
        if (!dealBalancedFiniceData()) {
            return false;
        }
        if (!taskCancle()) {
            return false;
        }
        return true;
    }

    private boolean prepareOutputData() {
        return true;
    }

    /**
     *  校验是否拥有项目确认权或者审批通过
     * @return boolean
     */
    private boolean autoExamination() {
        VData data = new VData();
        LGWorkSchema workSchema = new LGWorkSchema();
        workSchema.setWorkNo(mDetailWorkNo);
        data.add(mGlobalInput);
        data.add(workSchema);
        String remark = "延期结算日期:" + mLaterPayDate;
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("remark", remark);
        data.add(tTransferData);
        TaskAutoExaminationBL exame = new TaskAutoExaminationBL();
        if (!exame.submitData(data, "")) {
            //送审批成功，则本类错误信息中保存送审批原因
            if (exame.sendConfirmSuccess()) {
                mErrors.copyAllErrors(exame.getSendConfirmReason());
                mFailType = BQ.TRUE;
            } else {
                mErrors.copyAllErrors(exame.mErrors);
                mFailType = BQ.FALSE;
            }
            return false;
        }
        return true;
    }

    /**
     * 处理已经结案的白条财务数据，将其由J修改为B。同时删除本次生成的应收财务数据
     * 修改定期结算表的状态，将其标志为可以结算,同时修改下次结算日期为延期结算选择的日期
     * @return boolean
     */
    private boolean dealBalancedFiniceData() {
        String sql = "select GetNoticeNo from ljspay where OtherNo='" +
                     mDetailWorkNo + "' and OtherNoType='13'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        String tActuGetNo = tSSRS.GetText(1, 1);
        mActuNo=tActuGetNo;
        String sqlPay = "update ljapay set IncomeType = 'B' where payno in (select  BTActuNo from LJAEdorBalDetail " +
                        "where ActuNo = '" + tActuGetNo +
                        "') and IncomeType='J'";
        String sqlGet = "update ljaget set paymode = 'B' where ActuGetNo in (select  BTActuNo from LJAEdorBalDetail " +
                        "where ActuNo = '" + tActuGetNo + "') and paymode='J'";
        this.map.put(sqlPay, "UPDATE");
        this.map.put(sqlGet, "UPDATE");
        this.map.put("delete from ljspay where GetNoticeNo = '" + tActuGetNo +
                     "' and othernotype = '13'", "DELETE");
        this.map.put("delete from ljtempfee where TempFeeNo = '" + tActuGetNo +
                     "'", "DELETE");
        this.map.put("delete from ljtempfeeclass where TempFeeNo = '" +
                     tActuGetNo + "'", "DELETE");
        this.map.put("update ljspayb set dealState = '3' where GetNoticeNo = '" +
                     tActuGetNo + "' and othernotype = '13'", "UPDATE");
        this.map.put("update lcgrpbalplan set nextdate = '" + mLaterPayDate +
                     "',state = '0' where grpcontno = '" + mGrpContNo + "'",
                     "UPDATE");
        return true;
    }

    private boolean taskCancle() {
    	String sql = "select * from lpgrpedormain where edoracceptno = '" +
                     mDetailWorkNo + "'";
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.executeQuery(sql);
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("DelReason", "延期结算");
        tTransferData.setNameAndValue("ReasonCode", "202");

        VData tVData = new VData();
        tVData.addElement(mGlobalInput);
        tVData.addElement(tLPGrpEdorMainSet.get(1));
        tVData.addElement(tTransferData);

        GEdorMainCancelBL tGEdorMainCancelBL = new GEdorMainCancelBL();
        if (!tGEdorMainCancelBL.submitData(tVData, "EDORMAIN")) {
            this.mErrors.copyAllErrors(tGEdorMainCancelBL.mErrors);
            return false;
        } else {
            this.map.add(tGEdorMainCancelBL.getMap());
        }
        //由于原来的保全工单撤销不对LPEdorApp进行处理.
        this.map.put("delete from lpedorapp where edoracceptno = '"+mDetailWorkNo+"'","DELETE");
        //由于原来的保全工单撤销不对LJAEdorBalDetail进行处理.20110804[OoO]ytz
        this.map.put("delete from LJAEdorBalDetail where ActuNo = '"+ mActuNo +"'","DELETE");

        return true;
    }

    /**
     *
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "pa0003";
        tGI.ManageCom = "86";
        tGI.ComCode = "86";

        String GrpContNo = "0000123201";
        String DetailWorkNo = "20060908000004";
        String laterPayDate = "2006-9-11";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("GrpNo",GrpContNo);
        tTransferData.setNameAndValue("DetailWorkNo",DetailWorkNo);
        tTransferData.setNameAndValue("LaterPayDate",laterPayDate);

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tTransferData);


        GrpBalanceOnLaterBL tGrpBalanceOnLaterBL= new GrpBalanceOnLaterBL();
        if(!tGrpBalanceOnLaterBL.submitData(tVData, ""))
        {
                System.out.println("end");

        }

    }
}
