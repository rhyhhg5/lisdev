package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: Sinosoft</p>
 * @author Cz
 * @version 1.0
 */
public class PEdorTFConfirmBL implements EdorConfirm
{
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private LPEdorItemSchema mLPEdorItemSchema =new LPEdorItemSchema();
    private String mPolPayToDate = null;
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();

    public PEdorTFConfirmBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public void setOperate(String cOperate)
    {
        this.mOperate = cOperate;
    }

    public String getOperate()
    {
        return this.mOperate;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.setOperate(cOperate);

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //数据准备操作
        if(!dealData())
        {
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        VData d = new VData();
        d.add(map);
        return d;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        LPEdorItemSchema tLPEdorItemSchema= (LPEdorItemSchema) cInputData.getObjectByObjectName(
            "LPEdorItemSchema", 0);
        mLPEdorItemSchema.setSchema(tLPEdorItemSchema);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
            "GlobalInput", 0));

        if(mLPEdorItemSchema == null || mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "GrpEdorNIAppConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if(tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
        {
            mErrors.copyAllErrors(tLPEdorItemDB.mErrors);
            mErrors.addOneError(new CError("查询保全项目信息失败！"));
            return false;
        }
        mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));

        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorMainDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorMainDB.setContNo(mLPEdorItemSchema.getContNo());
        if(!tLPEdorMainDB.getInfo())
        {
            mErrors.copyAllErrors(tLPEdorMainDB.mErrors);
            mErrors.addOneError(new CError("查询保全信息失败！"));
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData()
    {
        //查询复效险种
        StringBuffer sql = new StringBuffer();
        sql.append("select * from lppol where edorno ='")
            .append(mLPEdorItemSchema.getEdorNo()).append("' ")
            .append("   and EdorType = '")
            .append(mLPEdorItemSchema.getEdorType()).append("' ");
        System.out.println(sql);
        LPPolDB tLPPolDB = new LPPolDB();
        LPPolSet tLPPolSet = tLPPolDB.executeQuery(sql.toString());
        if(tLPPolSet == null || tLPPolSet.size() == 0)
        {
            this.mErrors.addOneError("复效险种查询错误！");
            return false;
        }

        //查询复效生效日
        EdorItemSpecialData tEdorItemSpecialData
            = new EdorItemSpecialData(mLPEdorItemSchema.getEdorNo()
                                      , mLPEdorItemSchema.getEdorType());
        if(!tEdorItemSpecialData.query())
        {
            this.mErrors.addOneError("复效日期查询错误！");
            return false;
        }

        for(int k = 1; k <= tLPPolSet.size(); k++)
        {
            String updateStr =
                "update LCPol "
                + "set StateFlag = '" + BQ.STATE_FLAG_SIGN + "', "
                + "   ModifyDate = '" + mCurDate + "', "
                + "   ModifyTime = '" + mCurTime + "' "
                + "where PolNo = '" + tLPPolSet.get(k).getPolNo() + "' ";
            map.put(updateStr, SysConst.UPDATE);

            updateStr = "update LCContState set State = '0' "
                        + "where contno ='"
                        + this.mLPEdorItemSchema.getContNo()
                        + "' and polno ='"
                        + tLPPolSet.get(k).getPolNo()
                        + "'  and State = '1' ";
            map.put(updateStr, SysConst.UPDATE);
        }

        mResult.clear();
        this.mLPEdorItemSchema.setEdorState("0");
        LCContStateDB t = new LCContStateDB();
        String s = "select * from lccontstate "
                   + "where contno ='" + mLPEdorItemSchema.getContNo() + "' "
                   + "   and polno = '000000' "
//                   + "   and StateType ='Available'
                   + " and State = '1'";
        LCContStateSet tS = t.executeQuery(s);
        if(tS != null && tS.size() > 0)
        {
            sql = new StringBuffer();
            sql.append("update LCContState set State = '0'")
                .append("where contno ='")
                .append(this.mLPEdorItemSchema.getContNo())
                .append("' and polno ='000000' ")
                .append("  and State = '1' ");
            map.put(sql.toString(), SysConst.UPDATE);

            map.put("update LCCont a set StateFlag = '"
                    + BQ.STATE_FLAG_SIGN + "', "
                    + "  ModifyDate = '" + mCurDate + "', "
                    + "  ModifyTime = '" + mCurTime + "' "
                    + "where ContNo = '" + mLPEdorItemSchema.getContNo() + "' ",
                    SysConst.UPDATE);
        }
        map.put(mLPEdorItemSchema, SysConst.UPDATE);
       //删除打印管理表中的数据
        this.deleteTFInf();
        mResult.add(map);
        return true;
    }

    /**
     * 如果是TF 则删除打印管理表中的数据
     * @return
     */
   private boolean deleteTFInf(){
    	String sql2="delete from LOPrtManager where OtherNo = '"+mLPEdorItemSchema.getContNo()+"' and code='42'";
    	map.put(sql2,"DELETE");
    	return true ;
    }
}
