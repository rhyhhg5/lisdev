//程序名称：BqDeleteRedundanceDataUI.java
//程序功能：保全增人被保人冗余数据维护
//创建日期：20190108
//创建人  ：ly
package com.sinosoft.lis.bq;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BqDeleteRedundanceDataUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量

	public BqDeleteRedundanceDataUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;
		this.mInputData = (VData) cInputData.clone();// ?把数据复制到了自己定义的变量中,不操作直接传过来的参数,为啥?
		BqDeleteRedundanceDataBL RedundanceDataBL = new BqDeleteRedundanceDataBL();
		try {
			if (!RedundanceDataBL.submitData(mInputData, mOperate)) {
				this.mErrors.copyAllErrors(RedundanceDataBL.mErrors);
				CError tError = new CError();
				tError.moduleName = "BqDeleteRedundanceDataUI";
				tError.functionName = "submitDat";
				tError.errorMessage = "BL类处理失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
