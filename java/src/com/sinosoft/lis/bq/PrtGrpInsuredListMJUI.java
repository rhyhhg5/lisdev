package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright:
 * 生成满期结算被保人列表
 * Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PrtGrpInsuredListMJUI
{
    /**错误的容器*/
    public CErrors mErrors = null;

    private String mEdorAcceptNo = null;

    private GlobalInput mGlobalInput = null;

    private PrtGrpInsuredListMJBL mPrtGrpInsuredListMJBL = null;

    public PrtGrpInsuredListMJUI(String edorAcceptNo, GlobalInput globalInput)
    {
        mEdorAcceptNo = edorAcceptNo;
        mGlobalInput = globalInput;
    }

    /**
     * 操作提交方法
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData()
    {
        mPrtGrpInsuredListMJBL = new PrtGrpInsuredListMJBL(mEdorAcceptNo,
            mGlobalInput);
        if (!mPrtGrpInsuredListMJBL.submitData())
        {
            mErrors = mPrtGrpInsuredListMJBL.mErrors;
            return false;
        }

        return true;
    }

    /**
     * 得到被保人清单的XmlExport对象
     * @return XmlExport
     */
    public XmlExport getXmlExport()
    {
        return mPrtGrpInsuredListMJBL.getXmlExport();
    }


    public static void main(String[] args)
    {
        PrtGrpInsuredListMJUI ui = new PrtGrpInsuredListMJUI("", null);
    }
}
