

package com.sinosoft.lis.bq;

//程序名称：PEdorBADetailBL.java
//程序功能：
//创建日期：2008-05-20
//创建人  ：pst
//更新记录：  更新人    更新日期     更新原因/内容

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorBADetailBL {
/** 传入数据的容器 */
private VData mInputData = new VData();
/** 传出数据的容器 */
private VData mResult = new VData();
/** 数据操作字符串 */
private String mOperate;
/** 错误处理类 */
public CErrors mErrors = new CErrors();
/** 全局基础数据 */
private GlobalInput mGlobalInput = null;

private LPEdorItemSchema mLPEdorItemSchema = null;


private MMap map = new MMap();


private String currDate = PubFun.getCurrentDate();

private String currTime = PubFun.getCurrentTime();

private String mEdorNo = null;

private String mContNo=null;

private String mInsuredNo=null;

private LPEdorEspecialDataSet mLPEdorEspecialDataSet = new LPEdorEspecialDataSet();

public PEdorBADetailBL() 
{
}

/**
 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
 * @param cInputData 传入的数据,VData对象
 * @param cOperate 数据操作字符串，主要包括"INSERT"
 * @return 布尔值（true--提交成功, false--提交失败）
 */
public boolean submitData(VData cInputData, String cOperate) 
{

  //将操作数据拷贝到本类中
  this.mInputData = (VData) cInputData.clone();
  this.mOperate = cOperate;
  

  //得到外部传入的数据,将数据备份到本类中
  if (!getInputData()) 
  {
    return false;
  }

  if (!checkData()) 
  {
    return false;
  }

  //进行业务处理
  if (!dealData()) 
  {
    return false;
  }

  //准备往后台的数据
  if (!prepareData()) 
  {
    return false;
  }
  PubSubmit tSubmit = new PubSubmit();

  if (!tSubmit.submitData(mResult, "")) 
  { //数据提交
    // @@错误处理
    this.mErrors.copyAllErrors(tSubmit.mErrors);
    CError tError = new CError();
    tError.moduleName = "PEdorBADetailBL";
    tError.functionName = "submitData";
    tError.errorMessage = "数据提交失败!";
    this.mErrors.addOneError(tError);
    return false;
  }
  System.out.println("PEdorBADetailBL End PubSubmit");
  return true;
}

/**
 * 将外部传入的数据分解到本类的属性中
 * @param: 无
 * @return: boolean
 */
private boolean getInputData() {
  try 
  {
    mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
    mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName("LPEdorItemSchema", 0);
    
    if (mGlobalInput == null || mLPEdorItemSchema == null) 
    {
      CError tError = new CError();
      tError.moduleName = "PEdorBADetailBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "输入数据有误!";
      this.mErrors.addOneError(tError);
      return false;
    }
    
    mEdorNo = mLPEdorItemSchema.getEdorNo();
    if(mEdorNo==null || mEdorNo.equals("") || mEdorNo.equals("null"))
    {
    	CError tError = new CError();
	    tError.moduleName = "PEdorBADetailBL";
	    tError.functionName = "getInputData";
	    tError.errorMessage = "mEdorNo为空!";
	    this.mErrors.addOneError(tError);
	    return false;
    }
    
    mContNo = mLPEdorItemSchema.getContNo();
    if(mContNo==null || mContNo.equals("") || mContNo.equals("null"))
    {
    	CError tError = new CError();
	    tError.moduleName = "PEdorBADetailBL";
	    tError.functionName = "getInputData";
	    tError.errorMessage = "mContNo为空!";
	    this.mErrors.addOneError(tError);
	    return false;
    }
    
    String tSql="select InsuredNo from LCPol a where ContNo='"+mContNo+"' and PolNo = MainPolNo " 
    	+ "and exists (select 1 from LMRiskapp where RiskCode = a.RiskCode and RiskType4= '4') with ur";
    ExeSQL tExeSQL=new ExeSQL();
    mInsuredNo=tExeSQL.getOneValue(tSql);
    if(mInsuredNo==null || mInsuredNo.equals("") || mInsuredNo.equals("null"))
    {
    	CError tError = new CError();
	    tError.moduleName = "PEdorBADetailBL";
	    tError.functionName = "getInputData";
	    tError.errorMessage = "mInsuredNo为空!";
	    this.mErrors.addOneError(tError);
	    return false;
    }
    
    mLPEdorEspecialDataSet = (LPEdorEspecialDataSet) mInputData.getObjectByObjectName("LPEdorEspecialDataSet", 0);
    if(mLPEdorEspecialDataSet.size()==0)
    {
    	CError tError = new CError();
	    tError.moduleName = "PEdorBADetailBL";
	    tError.functionName = "getInputData";
	    tError.errorMessage = "mLPEdorEspecialDataSet为空!";
	    this.mErrors.addOneError(tError);
	    return false;
    }
  }
  catch (Exception e) 
  {
    // @@错误处理
    e.printStackTrace();
    CError tError = new CError();
    tError.moduleName = "PEdorBADetailBL";
    tError.functionName = "getInputData";
    tError.errorMessage = "接收数据失败!!";
    this.mErrors.addOneError(tError);
    return false;
  }
  return true;
}

/**
 * 根据前面的输入数据，进行逻辑处理
 * 因为退保项目必须在其他项目都结案之后才能进行，所以相应的保全数据可以从C表中得到
 * @return 如果在处理过程中出错，则返回false,否则返回true  
 */
private boolean dealData() 
{
	//处理附加险
	LCPolDB tLCPolDB = new LCPolDB();
	
	String getPol = "select * from lcpol a where ContNo='"+mContNo+"' "
    	+ "and exists (select 1 from lmriskapp where riskcode = a.riskcode and (risktype4 = '4' or RiskCode = '231001' or RiskCode = '231201') ) with ur";
	
	LCPolSet tLCPolSet = tLCPolDB.executeQuery(getPol);
	
	String tMainPolNo = null; //主险险种号
	LPEdorEspecialDataSchema mainLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();//主险记录
	
	for (int t = 1; t <= tLCPolSet.size(); t ++)
	{
		String tRiskCode = tLCPolSet.get(t).getRiskCode();
		String sql = "SELECT SubRiskFlag FROM LMRiskApp WHERE RiskCode = '" + tRiskCode + "' and RiskType4 = '4' with ur";
		String tSubRiskFlag = new ExeSQL().getOneValue(sql);
		if(tSubRiskFlag.equals("M")) //主险
		{
			tMainPolNo = tLCPolSet.get(t).getPolNo();
		}
	}
	
	if(tMainPolNo==null || tMainPolNo.equals(""))
	{
		CError tError = new CError();
	    tError.moduleName = "PEdorBADetailBL";
	    tError.functionName = "dealData";
	    tError.errorMessage = "获取万能主险PolNo失败!";
	    this.mErrors.addOneError(tError);
		return false;
	}

	HashMap hashMap = new HashMap();
	//查出主险，把主险和231001的PolNo放在 hashmap中，因为set中只有主险和231001
	for(int i=1; i<=mLPEdorEspecialDataSet.size();i++)
	{
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchema.setSchema(mLPEdorEspecialDataSet.get(i).getSchema());
		String tPolNo = tLPEdorEspecialDataSchema.getPolNo();
		hashMap.put(tPolNo, "");
		if(tMainPolNo.equals(tPolNo))
		{
			mainLPEdorEspecialDataSchema.setSchema(tLPEdorEspecialDataSchema);
		}
	}
	//hashmap中没有的，就是另一个附加险，再将另一个附加险放入Set，除了PolNo，其它信息都=主险，与主险保额相同。
	for(int j=1; j<=tLCPolSet.size();j++)
	{
		String cPolNo = tLCPolSet.get(j).getPolNo();
		if(!hashMap.containsKey(cPolNo))
		{
			LPEdorEspecialDataSchema subLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
			subLPEdorEspecialDataSchema.setSchema(mainLPEdorEspecialDataSchema);
			subLPEdorEspecialDataSchema.setPolNo(cPolNo);
			mLPEdorEspecialDataSet.add(subLPEdorEspecialDataSchema);
		}
	}
  mLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
  mLPEdorItemSchema.setModifyDate(currDate);
  mLPEdorItemSchema.setModifyTime(currTime);
  map.put(mLPEdorItemSchema, "UPDATE");
  map.put("delete from LPEdorEspecialData where edorno='"+mEdorNo+"' and edortype='BA' ", "DELETE");
  map.put(mLPEdorEspecialDataSet, "DELETE&INSERT");
  return true;
}

/**
 * 根据前面的输入数据，进行校验处理
 * @return 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean checkData()
{
	for(int i=1;i<=mLPEdorEspecialDataSet.size();i++)
	{
		if(mLPEdorEspecialDataSet.get(i).getDetailType().equals("AA"))
		{//如果是增加保额
			 LPCustomerImpartDB tLPCustomerImpartDB = new
		     LPCustomerImpartDB();
			 tLPCustomerImpartDB.setEdorNo(mEdorNo);
			 tLPCustomerImpartDB.setCustomerNo(mInsuredNo);
			 if (tLPCustomerImpartDB.query().size() == 0) 
			 {
				 mErrors.addOneError("请录入被保人的健康告知！");
				 return false;
			 }
		}
	}
		
	LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
    tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
    tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
    tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
    LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
     if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1)
     {
         CError tError = new CError();
         tError.moduleName = "PEdorBADetailBL";
         tError.functionName = "checkData";
         tError.errorMessage = "输入数据有误,LPEdorItem中没有相关数据!";
         this.mErrors.addOneError(tError);
         return false;
     }

     mLPEdorItemSchema = tLPEdorItemSet.get(1);
	  
    if(mLPEdorItemSchema.getEdorState().trim().equals(BQ.EDORSTATE_CAL))
    {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "PEdorBADetailBL";
        tError.functionName = "checkData";
        tError.errorMessage = "该保全已经保全理算，不能修改!";
        this.mErrors.addOneError(tError);
        return false;
    }
 
    return true;
}

/**
 * 准备往后层输出所需要的数据
 * @return 如果准备数据时发生错误则返回false,否则返回true
 */
private boolean prepareData() 
{
  mResult.clear();
  mResult.add(map);
  return true;
}

/**
 * 数据输出方法，供外界获取数据处理结果
 * @return 包含有数据查询结果字符串的VData对象
 */
public VData getResult() 
{
  return mResult;
}
}
