package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import java.util.HashMap;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 生成满期结算被保人列表
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PrtGrpInsuredListMJBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    /**受理号*/
    private String mEdorAcceptNo = null;

    /**保全类型*/
    private String mEdorType = null;

    /**被保人状态，0，已无效，1，本次变更有效*/
    private final String INSURED_STATE_ZT = "0";
    private final String INSURED_STATE_VALID = "1";
    private final int COLNUMBER = 11;

    /**存储工单信息*/
    private LGWorkSchema mLGWorkSchema = null;

    /**存储被保人与员工关系，加快查找*/
    private HashMap mRelationHashMap = null;

    /**存储证件类型，加快查找*/
    private HashMap mIDTypeHashMap = null;

    /**存储账户类型，加快查找*/
    private HashMap mAccTypeHashMap = null;

    private XmlExport mXmlExport = null;

    public PrtGrpInsuredListMJBL(String edorAcceptNo, GlobalInput tGlobalInput)
    {
        mEdorAcceptNo = edorAcceptNo;
        mGlobalInput = tGlobalInput;
    }

    /**
     * 操作提交的方法
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData()
    {
        long a = System.currentTimeMillis();
        if(!checkData())
        {
            return false;
        }

        if(!getPrintData())
        {
            return false;
        }
        System.out.println("生成清单共用："
                           + (System.currentTimeMillis() - a) / 1000 + "秒");

        return true;
    }

    private boolean getPrintData()
    {
        TextTag tag = new TextTag();
        mXmlExport = new XmlExport(); //新建一个XmlExport的实例
        mXmlExport.createDocument("PrtGrpInsuredListMJ.vts", "printer"); //最好紧接着就初始化xml文档

        //得到公司名
        tag.add("GrpName", mLGWorkSchema.getCustomerName());
        mEdorType = BQ.EDORTYPE_MJ;
        tag.add("EdorName", "满期结算");
        tag.add("EdorNo", mEdorAcceptNo);
        mXmlExport.addTextTag(tag);

        ListTable tListTable = getListTable();
        if (tListTable == null)
        {
            return false;
        }
        String[] title =
                         {"序号", "姓名", "责任终止时间", "员工姓名", "与员工关系	",
                         "证件类型", "证件号码", "生日", "结算日本金",
                         "结算利息", "帐户余额"};
        mXmlExport.addListTable(tListTable, title);
        mXmlExport.outputDocumentToFile("C:\\", "PrtGrpInsuredListMJBL");

        return true;
    }

    private ListTable getListTable()
    {
        ListTable tListTable = new ListTable();
        tListTable.setName("MJ");

        LPDiskImportSet tLPDiskImportSet = getInsured(mLGWorkSchema.getContNo(),
                                                      BQ.POLTYPEFLAG_INSURED);
        String publicAccInsuredNo;  //公共账户保单号
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setGrpContNo(mLGWorkSchema.getContNo());
        tLPPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
        LPPolSet tLPPolSet = tLPPolDB.query();
        if (tLPPolSet.size() == 0)
        {
            publicAccInsuredNo = "";
        }
        else
        {
            publicAccInsuredNo = tLPPolSet.get(1).getInsuredNo();
        }
        mRelationHashMap = new HashMap();
        mIDTypeHashMap = new HashMap();
        mAccTypeHashMap = new HashMap();
        int insuredCount;
        for (insuredCount = 1; insuredCount <= tLPDiskImportSet.size();
                            insuredCount++)
        {
            LPDiskImportSchema insured = tLPDiskImportSet.get(insuredCount);
            //公共账户不处理
            if(publicAccInsuredNo.equals(insured.getInsuredNo()))
            {
                tLPDiskImportSet.removeRange(insuredCount, insuredCount);
                insuredCount--;
                continue;
            }

            String[] info = new String[COLNUMBER];
            info[0] = "" + insuredCount;
            info[1] = StrTool.cTrim(insured.getInsuredName());
            info[2] = StrTool.cTrim(getDutyEndDate(insured,
                this.INSURED_STATE_VALID));
            info[3] = StrTool.cTrim(insured.getEmployeeName());
            info[4] = StrTool.cTrim(getRelationToMainInsured(
                insured.getRelation()));
            info[5] = StrTool.cTrim(getIDType(insured.getIDType()));
            info[6] = StrTool.cTrim(insured.getIDNo());
            info[7] = CommonBL.decodeDate(insured.getBirthday());
            info[8] = getBalance(insured);
            info[9] = getInterest(insured);
            info[10] = String.valueOf(PubFun.setPrecision(
                insured.getGetMoney(),
                "0.00"));

            if (info == null)
            {
                return null;
            }

            tListTable.add(info);
        }

        //处理已退保的被保人
        LBInsuredDB tLBInsuredDB = new LBInsuredDB();
        tLBInsuredDB.setGrpContNo(mLGWorkSchema.getContNo());
        LBInsuredSet tLBInsuredSet = tLBInsuredDB.query();
        for(int i = 1; i <= tLBInsuredSet.size(); i++)
        {
            LBInsuredSchema insured = tLBInsuredSet.get(i);

            String[] info = new String[COLNUMBER];
            info[0] = "" + (insuredCount + i);
            info[1] = StrTool.cTrim(insured.getName());
            info[2] = StrTool.cTrim(getDutyEndDate(insured, INSURED_STATE_ZT));
            info[3] = StrTool.cTrim(getMainInsured(insured));
            info[4] = StrTool.cTrim(getRelationToMainInsured(
                insured.getRelationToMainInsured()));
            info[5] = StrTool.cTrim(getIDType(insured.getIDType()));
            info[6] = StrTool.cTrim(insured.getIDNo());
            info[7] = StrTool.cTrim(CommonBL.decodeDate(insured.getBirthday()));
            info[8] = "-";
            info[9] = "-";
            info[10] = "-";

            if(info == null)
            {
                return null;
            }

            tListTable.add(info);
        }

        return tListTable;
    }

    private String getMainInsured(LBInsuredSchema insured)
    {
        String mainInsured = "";
        if (insured.getRelationToMainInsured() == null
            || insured.getRelationToMainInsured().equals("")
            || insured.getRelationToMainInsured().equals("1"))
        {
            return insured.getName();
        }

        LBInsuredDB tLBInsuredDB = new LBInsuredDB();
        tLBInsuredDB.setGrpContNo(insured.getGrpContNo());
        tLBInsuredDB.setContNo(insured.getContNo());
        tLBInsuredDB.setRelationToMainInsured(BQ.MAININSURED);

        LBInsuredSet set = tLBInsuredDB.query();
        if(set.size() == 0)
        {
            //主被保人没有被退保，构造在LPInsured中查询住被保人的参数
            LPInsuredSchema schema = new LPInsuredSchema();
            schema.setInsuredNo(insured.getInsuredNo());
            schema.setContNo(insured.getContNo());
            mainInsured = getMainInsured(schema);
            if(mainInsured == null)
            {
                mErrors.addOneError("没有得到与被保人对应的的员工信息。");
                return null;
            }
        }

        return mainInsured;
    }

    /**
     * 得到被保人责任终止时间
     * @return String
     */
    private String getDutyEndDate(Schema insured, String state)
    {
        String inValiDate = "-";
        if(state.equals(INSURED_STATE_ZT))
        {
            LBInsuredSchema tLBInsuredSchema =(LBInsuredSchema) insured;
            LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
            tLPEdorAppDB.setEdorAcceptNo(tLBInsuredSchema.getEdorNo());
            if(!tLPEdorAppDB.getInfo())
            {
                mErrors.addOneError("投保人已失效，但是没有查询难道对他的操作信息。");
                return null;
            }
            inValiDate = tLPEdorAppDB.getConfDate();

            if(inValiDate == null)
            {
                inValiDate = tLPEdorAppDB.getModifyDate();
            }
        }

        return inValiDate;
    }

    /**
     * 得到证件类型
     * 通过Hash表的方式可以大大减少访问数据库的时间，提高程序运行速度
     * @param idType String
     * @return String
     */
    private String getIDType(String idType)
    {
        String idTypeName = (String) mIDTypeHashMap.get(idType);
        if(idTypeName == null)
        {
            idTypeName =  StrTool.cTrim(
                ChangeCodeBL.getCodeName("IDType",idType));
            mIDTypeHashMap.put(idType, idTypeName);
        }

        return idTypeName;
    }

    /**
     * 计算被保人账户余额的利息：逐个计算被保人的险种余额帐户利息
     * @param insured LPInsuredSchema
     * @return String
     */
    private String getInterest(LPDiskImportSchema insured)
    {
        double interest = insured.getGetMoney() - insured.getMoney();

        return String.valueOf(PubFun.setPrecision(interest, "0.00"));
    }
    /**
     * 得到账户利率
     * @return double
     */
    private double getRate()
    {
        String rate = (String) mAccTypeHashMap.get("accType");
        if(rate == null)
        {
            LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
            tLPEdorEspecialDataDB.setEdorAcceptNo(mEdorAcceptNo);
            tLPEdorEspecialDataDB.setEdorNo(mEdorAcceptNo);
            tLPEdorEspecialDataDB.setEdorType(BQ.EDORTYPE_MJ);
            tLPEdorEspecialDataDB.setPolNo(BQ.FILLDATA);
            tLPEdorEspecialDataDB.setDetailType(BQ.DETAILTYPE_ACCRATE);
            if(!tLPEdorEspecialDataDB.getInfo())
            {
                rate = "0";
            }
            rate = tLPEdorEspecialDataDB.getEdorValue();
            mAccTypeHashMap.put("accType", rate);
        }
        return Double.parseDouble(rate);
    }

    /**
     * 计算被保人的账户余额
     * @param insured LPInsuredSchema
     * @return String
     */
    private String getBalance(LPDiskImportSchema insured)
    {
        double sumAccBala = PubFun.setPrecision(insured.getMoney(), "0.00");

        return String.valueOf(sumAccBala);
    }

    /**
     * 得到员工姓名
     * @param insured LPInsuredSchema
     * @return String
     */
    private String getMainInsured(LPInsuredSchema insured)
    {
        if(insured.getRelationToMainInsured() == null
           || insured.getRelationToMainInsured().equals(BQ.MAININSURED))
        {
            return insured.getName();
        }

        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        tLPInsuredDB.setGrpContNo(insured.getGrpContNo());
        tLPInsuredDB.setContNo(insured.getContNo());
        tLPInsuredDB.setRelationToMainInsured(BQ.MAININSURED);

        LPInsuredSet set = tLPInsuredDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有得到与被保人对应的的员工信息。");
            return null;
        }

        return set.get(1).getName();
    }

    /**
     * 将与主被保人关系代码解析为汉字
     * @param relationToMainInsured String
     * @return String
     */
    private String getRelationToMainInsured(String relationToMainInsured)
    {
        if(relationToMainInsured == null || relationToMainInsured.equals("")
           || relationToMainInsured.equals("1"))
        {
            relationToMainInsured = BQ.MAININSURED;  //本人
        }
        String relation = (String) mRelationHashMap.get(relationToMainInsured);
        if(relation == null)
        {
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("relation");
            tLDCodeDB.setCode(relationToMainInsured);
            tLDCodeDB.getInfo();

            relation = tLDCodeDB.getCodeName();
            mRelationHashMap.put(relationToMainInsured, relation);
        }

        return relation;
    }

    private LPDiskImportSet getInsured(String grpContNo, String polTypeFlag)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * from LPDiskImport ")
            .append("where edorNo = '")
            .append(mLGWorkSchema.getWorkNo())
            .append("'  and edorType = '")
            .append(mEdorType)
            .append("'  and grpContNo = '").append(grpContNo)
            .append("'  ");
        System.out.println(sql.toString());
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        LPDiskImportSet tLPDiskImportSet
            = tLPDiskImportDB.executeQuery(sql.toString());

        return tLPDiskImportSet;
    }


    /**
     * 校验数据的合法性
     * @return boolean
     */
    private boolean checkData()
    {
        boolean flag = false;
        if(mEdorAcceptNo == null)
        {
            flag = true;
        }

        mLGWorkSchema = com.sinosoft.task.CommonBL.getLGWorkInfo(mEdorAcceptNo);
        if(mLGWorkSchema == null)
        {
            flag = true;
        }

        if(flag)
        {
            mErrors.addOneError("没有得到生成清单的数据。");
            return false;
        }

        return true;
    }

    /**
     * 得到被保人清单的XmlExport对象
     * @return XmlExport
     */
    public XmlExport getXmlExport()
    {
        return mXmlExport;
    }

    public static void main(String[] args)
    {
        PrtGrpInsuredListMJBL bl = new PrtGrpInsuredListMJBL("20060301000011",
                                   null);
        if(!bl.submitData())
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
