package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LPAppRReportResultSchema;
import com.sinosoft.lis.vschema.LPAppRReportResultSet;
import com.sinosoft.lis.schema.LPAppRReportSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class PEdorAppRReportResultUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private VData mInputData;

    /** 数据操作字符串 */
    private String mOperate;

    public PEdorAppRReportResultUI()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;

        PEdorAppRReportResultBL tPEdorAppRReportResultBL = new PEdorAppRReportResultBL();

        System.out.println("--------PEdorAppRReportResult Start!---------");
        tPEdorAppRReportResultBL.submitData(cInputData, cOperate);
        System.out.println("--------PEdorAppRReportResult End!---------");

        //如果有需要处理的错误，则返回
        if (tPEdorAppRReportResultBL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tPEdorAppRReportResultBL.mErrors);
        }
        System.out.println("error num=" + mErrors.getErrorCount());
        mInputData = null;
        mResult = tPEdorAppRReportResultBL.getResult();

        return true;
    }

    /**
     * 返回结果方法
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
//        VData tVData = new VData();
//        GlobalInput mGlobalInput = new GlobalInput();
//
//        /** 全局变量 */
//        mGlobalInput.Operator = "001";
    //    mGlobalInput.ComCode = "86";
//        mGlobalInput.ManageCom = "86";
//
//         LCRReportSchema tLCRReportSchema = new LCRReportSchema();
//
//
//         tLCRReportSchema.setProposalContNo("130110000013806");
//         tLCRReportSchema.setPrtSeq("810000000000675");
//          tLCRReportSchema.setReplyContente("dsfdsf");
//
//
//        LCPEdorAppRReportResultSet tLCPEdorAppRReportResultSet = new LCPEdorAppRReportResultSet();
//        LCPEdorAppRReportResultSchema tLCPEdorAppRReportResultSchema = new
//                LCPEdorAppRReportResultSchema();
//        LCPEdorAppRReportResultSchema tLCPEdorAppRReportResultSchema1 = new
//                LCPEdorAppRReportResultSchema();
//        tLCPEdorAppRReportResultSchema.setContNo("130110000013806");
//        tLCPEdorAppRReportResultSchema.setGrpContNo("32136");
//        tLCPEdorAppRReportResultSchema.setProposalContNo("130110000013806");
//        tLCPEdorAppRReportResultSchema.setPrtSeq("810000000000675");
//        tLCPEdorAppRReportResultSchema.setCustomerNo("0000493520");
//        tLCPEdorAppRReportResultSchema.setPEdorAppRReportResult("sdfsdf");
//        tLCPEdorAppRReportResultSchema.setICDCode("asdfsdf");
//        tLCPEdorAppRReportResultSet.add(tLCPEdorAppRReportResultSchema);
//
//
//        tVData.add(mGlobalInput);
//
//        tVData.add(tLCRReportSchema);
//          tVData.add(tLCPEdorAppRReportResultSet);
//        PEdorAppPEdorAppRReportResultUI tPEdorAppPEdorAppRReportResultUI = new PEdorAppPEdorAppRReportResultUI();
//        try
//        {
//            if (tPEdorAppPEdorAppRReportResultUI.submitData(tVData, ""))
//            {
//
//            }
//            else
//            {
//                System.out.println("error:" +
//                                   tPEdorAppPEdorAppRReportResultUI.mErrors.getError(0).
//                                   errorMessage);
//            }
//        }
//        catch (Exception e)
//        {
//            System.out.println("error:" + e);
//        }

    }

}
