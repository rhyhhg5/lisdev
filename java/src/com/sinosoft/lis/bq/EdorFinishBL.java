package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.httpclientybk.deal.YbkXBTB;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.*;
import com.sinosoft.lis.operfee.IndiCancelAndDueFeeBL;
import com.sinosoft.lis.operfee.IndiDueFeeBL;

import java.util.Date;

/**
 * <p>Title: 保全结案</p>
 * <p>Description: 保全结案时工单也要结案 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class EdorFinishBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorAcceptNo = null;

    private String mMessage = null;

    private IndiCancelAndDueFeeBL mIndiCancelAndDueFeeBL = null; //处理续期应收数据撤销和重抽档数据

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public EdorFinishBL(GlobalInput gi, String edorAcceptNo)
    {
        this.mGlobalInput = gi;
        this.mEdorAcceptNo = edorAcceptNo;
    }

    /**
     * 提交数据
     * @param operator String
     * @return boolean
     */
    public boolean submitData(String operator)
    {
        if (!checkData())
        {
            return false;
        }

        if (!dealData(operator))
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        //处理续期抽档,不管是否成功,此时PEdorConfirmBL最终返回的都是true
        dealPRnewDueFee();

        //调用万能续期程序
        if(!indidueFee()){
//        	return false;
        } 

        return true;
    }

    /**
     * 处理续期数据的重抽档，抽档失败也返回true，表示保全确认成功
     * @return boolean
     */
    private boolean dealPRnewDueFee()
    {	
    	//上海医保新增逻辑
    	String xbsql = " select contno from lpedoritem where edorno = '"+mEdorAcceptNo+"' and edortype = 'EW' with ur ";
    	ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(xbsql);
        if (tSSRS != null && tSSRS.getMaxRow() > 0) {
    		VData tVData = new VData();
    		TransferData tTransferData=new TransferData();
    		tTransferData.setNameAndValue("StartDate","2005-01-01");
    		tTransferData.setNameAndValue("EndDate","3000-12-31");
    		tTransferData.setNameAndValue("ContNo",tSSRS.GetText(1, 1));
    		tTransferData.setNameAndValue("ManageCom","86310000");
    		tTransferData.setNameAndValue("BusinessFlag","2"); 
    		tTransferData.setNameAndValue("flag",true);
    		tVData.add(mGlobalInput);
    		tVData.add(tTransferData);
    		YbkXBTB tYbkXBTB = new YbkXBTB();
    		tYbkXBTB.submitData(tVData, "INSERT");
    	}else{
    		if (!mIndiCancelAndDueFeeBL.getPRnewDueFeeMap(mEdorAcceptNo))
    		{
    			mMessage += mIndiCancelAndDueFeeBL.getMessage();
    		}
    	}

        return true;
    }

    /**
     * 得到错误信息
     * @return String
     */
    public String getError()
    {
        return mErrors.getFirstError();
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData(String operator)
    {
    	//add by xp 添加保全确认事务的并发锁
    	lockWorkNo();
        if (operator.equals(BQ.CONTTYPE_P)) //个单
        {
            if (!personalFinish())
            {
                return false;
            }
        } else if (operator.equals(BQ.CONTTYPE_G)) //团单
        {
            if (!groupFinish())
            {
                return false;
            }
        }

        //提交人工核保附加条件
        if (!confirmUWAddContion())
        {
            return false;
        }

        //确认保费余额
        if (!confirmAppAcc(operator))
        {
            return false;
        }

        //工单结案
        if (!taskFinish())
        {
            return false;
        }

        //作废续期数据
        if (!cancelDueFee())
        {
            return false;
        }
        
        
        return true;
    }
    
    //调用续期抽挡程序
    private boolean indidueFee(){
    	boolean isXUQI = false;
        //查询复效生效日
    	/*复效（FX）及免息复效（MF）*/
    	String MFSql = "select 1 from lpedoritem where edorno='"+mEdorAcceptNo+"' and edortype='MF'";
    	String MFResult = new ExeSQL().getOneValue(MFSql);
    	String EdorType = "";
    	if(MFResult != null && !"".equals(MFResult) &&"1".equals(MFResult)){
    		EdorType = "MF";
    	} else {
    		EdorType = "FX";
    	}
    	EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(mEdorAcceptNo, EdorType);
        String StartDate="";
        System.out.println("==========================22222");
        if(!tEdorItemSpecialData.query())
        {
        	System.out.println("111");
        	return false;
        }
        if("FX".equals(EdorType)){
        	if(tEdorItemSpecialData.getEdorValue("FX_XUQI")!=null&&"1".equals(tEdorItemSpecialData.getEdorValue("FX_XUQI"))){
            	isXUQI = true;
            }
            if(tEdorItemSpecialData.getEdorValue("FX_D")!=null&&!"".equals(tEdorItemSpecialData.getEdorValue("FX_D"))){
            	 StartDate = "1900-01-01";
            }else{
            	CError tError = new CError();
            	tError.moduleName = "BqConfirmBL";
            	tError.functionName = "updatePrint";
            	tError.errorMessage = "调用万能续期查询复效日期失败";
            	mErrors.addOneError(tError);
            	System.out.println(tError.errorMessage);
            	return false;
            }
        } else if ("MF".equals(EdorType)){
        	if(tEdorItemSpecialData.getEdorValue("MF_XUQI")!=null&&"1".equals(tEdorItemSpecialData.getEdorValue("MF_XUQI"))){
            	isXUQI = true;
            }
            if(tEdorItemSpecialData.getEdorValue("MF_D")!=null&&!"".equals(tEdorItemSpecialData.getEdorValue("MF_D"))){
            	 StartDate = "1900-01-01";
            }else{
            	CError tError = new CError();
            	tError.moduleName = "BqConfirmBL";
            	tError.functionName = "updatePrint";
            	tError.errorMessage = "调用万能续期查询免息复效日期失败";
            	mErrors.addOneError(tError);
            	System.out.println(tError.errorMessage);
            	return false;
            }
        }
        /*复效（FX）及免息复效（MF）*/
        
        if(isXUQI){
        	//查询续期保单
        	String queryContSQL= "select * from lccont where contno=(select contno from lpedoritem where edoracceptno='"+mEdorAcceptNo+"')";
        	LCContDB tLCContDB = new LCContDB();
        	LCContSchema tLCContSchema = new LCContSchema();
        	LCContSet tLCContSet = tLCContDB.executeQuery(queryContSQL);
        	if(tLCContSet!=null&&tLCContSet.size()>0){
        		tLCContSchema = tLCContSet.get(1).getSchema();
        	}else{
        		CError tError = new CError();
        		tError.moduleName = "BqConfirmBL";
        		tError.functionName = "updatePrint";
        		tError.errorMessage = "调用万能续期查询保单号失败";
        		mErrors.addOneError(tError);
        		System.out.println(tError.errorMessage);
        		return false;
        	}
        	VData tVData = new VData();
        	tVData.add(tLCContSchema);
        	tVData.add(mGlobalInput);
        	
            TransferData mTransferData=new TransferData();
        	mTransferData.setNameAndValue("StartDate",StartDate);
        	mTransferData.setNameAndValue("EndDate",PubFun.getCurrentDate());
        	
//        	mTransferData.setNameAndValue(FeeConst.GETNOTICENO, getGetNoticeNo());
        	tVData.add(mTransferData);
        	String queryType = "2";
        	tVData.add(queryType);
        	
        	//若没有需要续期抽档的险种，可能会有需要续保抽档的险种，正常退出
        	IndiDueFeeBL tIndiDueFeeBL = new IndiDueFeeBL();
        	MMap tMMap = tIndiDueFeeBL.getSubmitMMap(tVData, "INSERT");
        	if((tMMap == null || tMMap.size() == 0) && tIndiDueFeeBL.mErrors.needDealError())
        	{
        		if(mEdorAcceptNo != null && !tIndiDueFeeBL.getHasePolNeedFee())
        		{
        			
        			CError tError = new CError();
        			tError.moduleName = "BqConfirmBL";
        			tError.functionName = "updatePrint";
        			tError.errorMessage = "万能续期失败";
        			mErrors.addOneError(tError);
        			System.out.println(tError.errorMessage);
        			return false;
        			
        		}
        		
        		mErrors.copyAllErrors(tIndiDueFeeBL.mErrors);
        		return false;
        	}
        	
        	VData d = new VData();
        	d.add(tMMap);
        	
        	PubSubmit p = new PubSubmit();
        	if (!p.submitData(d, ""))
        	{
        		CError tError = new CError();
        		tError.moduleName = "BqConfirmBL";
        		tError.functionName = "updatePrint";
        		tError.errorMessage = "调用万能续期时更新续期表出错";
        		mErrors.addOneError(tError);
        		System.out.println(tError.errorMessage);
        		return false;
        	}        
        }
        
        
        return true;

    }    
    
    

    /**
     * 作废续期应收数据
     * @return boolean
     */
    private boolean cancelDueFee()
    {
        mIndiCancelAndDueFeeBL = new IndiCancelAndDueFeeBL(this.mGlobalInput);
        MMap tMMap = mIndiCancelAndDueFeeBL.getCancelData(this.mEdorAcceptNo);
        mMap.add(tMMap);

        if (mIndiCancelAndDueFeeBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mIndiCancelAndDueFeeBL.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        //需要财务交费则检查有没有交费
        if (CommonBL.checkGetMoney(mEdorAcceptNo))
        {
            if (!checkTempFee())
            {
                //把保全设为待收费状态
                setEdorState(BQ.EDORSTATE_WAITPAY);
                submit();
                return false;
            }
        }
        //LIDO  判断是否到了保全生效日
        if (!isValiBAEdor(mEdorAcceptNo))
        {
            //把保全设为延期结案
            setEdorState(BQ.EDORSTATE_DELAY_FINISH);
            submit();
            return false;
        }

        return true;
    }

    private boolean isValiBAEdor(String tEdorAcceptNo)
    {
        String tSql =
                "select Contno,edorvalidate,Edortype from lpedoritem where edoracceptno='" +
                tEdorAcceptNo + "' and Edortype in ('BA','CM') ";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS lpedoritemSet = tExeSQL.execSQL(tSql);

        //add by fuxin 2008-9-25 添加延期结案条件，风险保费变化时才进入延期结案。
        //如果产生风险保费变化会在tLJSGetEndorse中产生记录，否则则不会。
        SSRS tSSRS = new SSRS();
        String sqlMoney = "select sum(getmoney) from LJSGetEndorse where EndorsementNo = '" + tEdorAcceptNo + "' with ur";
        System.out.println("查询保全是否产生收付费:"+sqlMoney);
        tSSRS = new ExeSQL().execSQL(sqlMoney);
        String strCount = tSSRS.GetText(1, 1);
        boolean moneyflag = false;
        if(null!=strCount && !"0".equals(strCount) && !"null".equals(strCount))
        {
        	moneyflag = true;
        }
        if ( lpedoritemSet.getMaxRow() > 0 && moneyflag && CommonBL.hasULIRisk(lpedoritemSet.GetText(1, 1)))
        {
            if (!isValiDate(mEdorAcceptNo))
            {
                return false;
            }

            String contNo = lpedoritemSet.GetText(1, 1);
            String edorvaliDate = lpedoritemSet.GetText(1, 2);
            String eodrType= lpedoritemSet.GetText(1, 3);

            if("BA".equals(eodrType) || "BA" == eodrType)
            {
                String sql = "select a.Polno,b.Insuaccno from LCPOL a, LCInsureAcc b where a.contno=b.contno and a.contno='" +
                contNo + "' and a.kindcode  = 'U' with ur";
				SSRS LCPOLInsurSet = tExeSQL.execSQL(sql);
				String polno = LCPOLInsurSet.GetText(1, 1);
				String insuaccno = LCPOLInsurSet.GetText(1, 2);
				LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
				tLCInsureAccSchema = CommonBL.getLCInsureAcc(polno, insuaccno);
				String balaDate = tLCInsureAccSchema.getBalaDate();
				FDate chgdate = new FDate();
				Date dbdate = chgdate.getDate(balaDate);
				Date dedate = chgdate.getDate(edorvaliDate);
				if (dbdate.compareTo(dedate) < 0)
				{
				   mErrors.addOneError("该保单月结还未结算到本次保全生效日期，进入延期结案状态！");
				   return false;
				}
            }
        }
        return true;
    }

    /**
     * 检查是否已到了保全生效日期
     * @return boolean
     */
    private boolean isValiDate(String tEdorAcceptNo)
    {
        String tSql =
                "select max(edorvalidate) from lpedoritem where edoracceptno='" +
                tEdorAcceptNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String lastValidate = tExeSQL.execSQL(tSql).GetText(1, 1);
        if (lastValidate == null || lastValidate.equals(""))
        {
            mErrors.addOneError("没有查到保全生效日不能保全确认");
            return false;
        }
        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(PubFun.getCurrentDate());
        Date dedate = chgdate.getDate(lastValidate);

        if (dbdate.compareTo(dedate) < 0)
        {
            mErrors.addOneError("没有到保全生效日,进入延期结案状态！");
            return false;
        }
        return true;
    }

    /**
     * 检查是否已交费
     * @return boolean
     */
    private boolean checkTempFee()
    {
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeDB.setOtherNo(mEdorAcceptNo);
        LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();
        if (tLJTempFeeSet.size() == 0) //LJTempFee为空则不能保全确认
        {
            mErrors.addOneError("需要财务交费！");
            return false;
        }
        String enterAccDate = tLJTempFeeSet.get(1).getEnterAccDate();
        if (enterAccDate == null) //到帐日期为空则没有交费
        {
            mErrors.addOneError("需要财务交费！");
            return false;
        }
        return true;
    }

    /**
     * 把保全设为待收费状态
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
        String sql;
        sql = "update LPEdorApp set EdorState = '" + edorState + "' " +
              "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "update LPEdorMain set EdorState = '" + edorState + "' " +
              "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "update LPEdorItem set EdorState = '" + edorState + "' " +
              "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 个单保全确认结案
     * @return boolean
     */
    private boolean personalFinish()
    {
        PEdorConfirmBL tPEdorConfirmBL =
                new PEdorConfirmBL(mGlobalInput, mEdorAcceptNo);
        MMap map = tPEdorConfirmBL.getSubmitData();
        if (map == null)
        {
            mErrors.copyAllErrors(tPEdorConfirmBL.mErrors);
            return false;
        }
        mMap.add(map);
        mMessage = tPEdorConfirmBL.getMessage();
        return true;
    }

    /**
     * 团单保全确认结案
     * @return boolean
     */
    private boolean groupFinish()
    {
        LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
        tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
        tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);

        String strTemplatePath = "";
        VData data = new VData();
        data.add(strTemplatePath);
        data.add(tLPGrpEdorMainSchema);
        data.add(mGlobalInput);

        PGrpEdorConfirmBL tPGrpEdorConfirmBL = new PGrpEdorConfirmBL();
        MMap map = tPGrpEdorConfirmBL.getSubmitData(data,
                "INSERT||GRPEDORCONFIRM");
        if (map == null)
        {
            mErrors.copyAllErrors(tPGrpEdorConfirmBL.mErrors);
            return false;
        }
        //根据数据校验添加修改SQL,将lpedoritem状态设置正确
        System.out.println("EdorFinishBL-更新状态不：LPEdorItem");
        String updateLPEdorItem = " update LPEdorItem "
            + "set edorState='0' "
            + ",modifydate=current date,modifytime=current time "
            + "where EdorAcceptNo='"
            + mEdorAcceptNo + "' ";
        System.out.println("EdorFinishBL-更新状态了，哈哈：LPEdorItem");
        map.put(updateLPEdorItem, "UPDATE");

        mMap.add(map);
        return true;
    }

    /**
     * 提交人工核保附加条件
     * @return boolean
     */
    private boolean confirmUWAddContion()
    {
        return true;
    }

    /**
     * 工单结案
     * @param edorAcceptNo String
     * @return boolean
     */
    private boolean taskFinish()
    {
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
        tLGWorkSchema.setTypeNo("03"); //结案状态

        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLGWorkSchema);
        TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
        MMap map = tTaskAutoFinishBL.getSubmitData(data, "");
        if (map == null)
        {
            mErrors.copyAllErrors(tTaskAutoFinishBL.mErrors);
            return false;
        }
        mMap.add(map);
        String reason = "待催缴工单" + mEdorAcceptNo + "交费结案。";
        FinishPhoneHastenBL tFinishPhoneHastenBL =
                new FinishPhoneHastenBL(mGlobalInput, mEdorAcceptNo, reason);
        MMap phoneMap = tFinishPhoneHastenBL.getSubmitData();
        if (map == null)
        {
            mErrors.copyAllErrors(tFinishPhoneHastenBL.mErrors);
            return false;
        }
        mMap.add(phoneMap);
        return true;
    }

    /**
     * 确认帐户余额
     * @return boolean
     */
    private boolean confirmAppAcc(String operator)
    {
        String otherNoType = "";
        if (operator.equals(BQ.CONTTYPE_P))
        {
            otherNoType = BQ.NOTICETYPE_P;
        }
        if (operator.equals(BQ.CONTTYPE_G))
        {
            otherNoType = BQ.NOTICETYPE_G;
        }
        System.out.println("otherNoType:" + otherNoType);
        String sql = "select * from LJSPay " +
                     "where OtherNo = '" + mEdorAcceptNo + "' " +
                     "and OtherNoType in ('3', '10') ";
        System.out.println(sql);
        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql);
        if ((tLJSPaySet.size() > 0) &&
            (tLJSPaySet.get(1).getSumDuePayMoney() > 0))
        {
            AppAcc tAppAcc = new AppAcc();
            MMap map = tAppAcc.accConfirm(tLJSPaySet.get(1).getAppntNo(),
                                          mEdorAcceptNo, otherNoType);
//            if (map == null)
//            {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tAppAcc.mErrors);
//                return false;
//            }
            mMap.add(map);
        }
        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    
    private boolean lockWorkNo()
    {
    	MMap tCekMap = null;
    	tCekMap = lockConfirm(mEdorAcceptNo);
        if (tCekMap == null)
        {
            return false;
        }
        mMap.add(tCekMap);
        return true;

    }
    
    /**
     * 锁定动作
     * @param aGrpContNo
     * @return MMap
     */
    private MMap lockConfirm(String aEdorAcceptNo)
    {
        MMap tMMap = null;
        /**保全确认"XQ"*/
        String tLockNoType = "BQ";
        /**锁定有效时间（秒）*/
        String tAIS = "18000";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", aEdorAcceptNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }

    public static void main(String[] s)
    {

        GlobalInput gi = new GlobalInput();
        gi.Operator = "pa0001";
        gi.ComCode = "86";
        gi.ManageCom = "86";

        EdorFinishBL tEdorFinishBL = new EdorFinishBL(gi, "20120612000002");
        if (!tEdorFinishBL.submitData(BQ.CONTTYPE_P))
        {
            System.out.println(tEdorFinishBL.mErrors.getErrContent());
        }

    }
}
