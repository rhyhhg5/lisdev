package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title:保全重新理算类 </p>
 * <p>Description:重新理算，删除与财务相关的记录，为重新理算作准备</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company:sinosoft </p>
 * @version picch v 1.13
 */
public class PrepareReCalBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /**将原来的updateSql都放入map后作为返回结果，以后在PubSubmit中作数据库操作----Alex*/
    MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;
    private String mEdorno;
    private String actionFlag; //true--团单，false--个单

    public PrepareReCalBL() {
    }

    public PrepareReCalBL(String tEdorNo, String flag)
    {
        this.mEdorno = tEdorNo;
        this.actionFlag = flag;
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData() {
        if (!checkData()) {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (this.dealData() == false) {
            return false;
        }
        System.out.println("---dealDate---");

        // 装配处理好的数据，准备给后台进行保存

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start PrepareReCalBL Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            buildError("PrepareReCalBL", "数据提交失败!");
            return false;
        }
        return true;
    }

    private boolean dealData() {
        String delSql;
        String[] asscociateTable_OtherNo = {"LJSGet", "LJSPay"};
        String[] pEdorTables = {"LPEdorApp", "LPEdorMain", "LPEdorItem"};
        //String[] businessTables = {"LPPENotice", "LPPENoticeItem", "LPEdorEspecialData"};

        //删除相应的财务记录
        delSql = "delete from LJSGetEndorse where EndorsementNo = '" +
                 mEdorno + "'";
        map.put(delSql, "DELETE");

        for (int i = 0; i < asscociateTable_OtherNo.length; i++) {
             delSql = "delete from  " + asscociateTable_OtherNo[i] +
                      " where OtherNo = '" + mEdorno +
                      "' and OtherNoType='10'";
             map.put(delSql, "DELETE");
        }

        //for(int i = 0; i < businessTables.length; i++)
        //{
        //    delSql = "delete from  " + businessTables[i]
        //              + " where edorAcceptNo = '" + mEdorno + "' ";
        //     map.put(delSql, "DELETE");

        //}

        for (int i = 0; i < pEdorTables.length; i++)
        {
            map.put("update " + pEdorTables[i] + " "
                    + "set ChgPrem = 0, ChgAmnt = 0, GetMoney = 0, GetInterest = 0 "
                    + "where edorAcceptNo = '" + mEdorno + "' ", "UPDATE");
        }

        //更新批改状态和核保状态
        String updateSql;
        updateSql = "update LPEdorApp set edorstate='1' ,uwstate='0' "
                    + "where EdorAcceptNo = '" + mEdorno + "' ";
        map.put(updateSql, "UPDATE");

        //团单
        if ("GRP".equals(actionFlag)) {
            for (int i = 0; i < asscociateTable_OtherNo.length; i++) {
                delSql = "delete from  " + asscociateTable_OtherNo[i] +
                         " where OtherNo = '" + mEdorno +
                         "' and OtherNoType='3'";
                map.put(delSql, "DELETE");
}

            map.put("update LPGRPEdorItem set edorstate='1' where edorno='" +
                    mEdorno + "'", "UPDATE");
            map.put(
                    "update LPGRPEdorMain set edorstate='1',uwstate='0' where edorno='" +
                    mEdorno + "'", "UPDATE");
        }

        map.put("update LPEdorItem set edorstate='1' where edorno='" + mEdorno +
                "'", "UPDATE");
        map.put(
                "update LPEdorMain set edorstate='1',uwstate='0' where edorno='" +
                mEdorno + "'", "UPDATE");
        map.put("update LPUWMasterMain set AutoUWFlag = '' where edorNo = '" + mEdorno + "' ", "UPDATE");


        mResult.add(map);
        mInputData.clear();
        mInputData.add(map);
        return true;
    }


    /**
     * 校验数据
     * @return boolean
     */
    public boolean checkData() {
        if (mEdorno.equals("")) {
            this.buildError("CheckData", "没有相应的批单号！");
            return false;
        }
        if(!"GRP".equals(actionFlag) && !"P".equals(actionFlag))
        {
            this.buildError("CheckData", "非法参数，只能使用规定参数！");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PrepareReCalBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String [] args)
    {
        PrepareReCalBL rc = new PrepareReCalBL("20050701000023","P");
        rc.submitData();
    }
}
