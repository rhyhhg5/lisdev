package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;

/**
* <p>Title: 特需医疗账户资金分配</p>
* <p>Description: 个人账户资金分配清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListGDUI
{
    PrtGrpInsuredListGDBL mPrtGrpInsuredListGDBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListGDUI(GlobalInput gi, String edorNo)
    {
        mPrtGrpInsuredListGDBL = new PrtGrpInsuredListGDBL(gi, edorNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPrtGrpInsuredListGDBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult()
    {
        return mPrtGrpInsuredListGDBL.getResult();
    }
}
