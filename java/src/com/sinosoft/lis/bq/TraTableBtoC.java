package com.sinosoft.lis.bq;

import java.util.Set;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LBInsuredSet;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
/**
 * 回退时B表到C表的还原 
 *
 */
public class TraTableBtoC
{
	private MMap mMap = new MMap();
	private MMap InsMap = new MMap();
	private MMap delMap = new MMap();
	/**
	 * 
	 * @param String 保单号
	 * @param String 保全号
	 * @return
	 */
	public boolean submitData(String pContNo,String pEdorNo)
	{
		if(!this.TraTableA(pContNo,pEdorNo))
		{
			return false;
		}
		VData tInputData = new VData();
		mMap.add(InsMap);
		mMap.add(delMap);
		if(!this.SetState(pContNo,pEdorNo))
		{
			return false;
		}
//		PrtMap(mMap);
		tInputData.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start Data Submit...");
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            System.out.println("数据提交失败!");
            return false;
        }
        System.out.println("End Data Submit...");
		return true;
	}

	private boolean TraTableA(String pContNo,String pEdorNo)
	{
		String[] lcTable =
		{
				"LCCont", "LCAppnt", "LCCustomerImpart", "LCCustomerImpartParams"
		};
		String[] lbTable =
		{
				"LBCont", "LBAppnt", "LBCustomerImpart", "LBCustomerImpartParams"
		};
		for (int i = 0; i < lcTable.length; i++)
		{
			String tSqlIns ="";
			String tSqlDel ="";
			if (lcTable[i].toUpperCase().equals("LCCUSTOMERIMPART") || lcTable[i].toUpperCase().equals("LCCUSTOMERIMPARTPARAMS"))
			{
				String tSqlCol = GetColName(lcTable[i]);
				if(tSqlCol == null || "".equals(tSqlCol))
				{
					return false;
				}
				tSqlIns = "insert into " + lcTable[i] + "(" + tSqlCol + ")" + " (select " + tSqlCol + " from " + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and ContNo='" + pContNo + "' and CustomerNoType='0')";
				tSqlDel = "delete from " + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and ContNo='" + pContNo + "' and CustomerNoType='0'";
				InsMap.put(tSqlIns, "INSERT");
				delMap.put(tSqlDel, "DELETE");
			}
			else
			{
				String tSqlCol = GetColName(lcTable[i]);
				if(tSqlCol == null || "".equals(tSqlCol))
				{
					return false;
				}
				tSqlIns = "insert into " + lcTable[i] + "(" + tSqlCol + ")" + " (select " + tSqlCol + " from " + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and ContNo='" + pContNo + "')";
				tSqlDel = "delete from " + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and ContNo='" + pContNo + "'";
				InsMap.put(tSqlIns, "INSERT");
				delMap.put(tSqlDel, "DELETE");
			}
//			 System.out.println(tSqlIns);
		}
        LBInsuredSet tLBInsuredSet = new LBInsuredSet();
        LBInsuredDB tLBInsuredDB = new LBInsuredDB();
        tLBInsuredDB.setEdorNo(pEdorNo);
        tLBInsuredDB.setContNo(pContNo);
        tLBInsuredSet = tLBInsuredDB.query();
        if (tLBInsuredDB.mErrors.needDealError())
        {
        	System.out.println("查询个人合同" + pContNo + "被保险人失败！");
            return false;
        }
        for (int i = 1; i <= tLBInsuredSet.size(); i++)
        {
        	MMap tMap = new MMap();
        	tMap = TraTableB(pContNo,tLBInsuredSet.get(i).getInsuredNo(),pEdorNo);
        	if(tMap == null)
        	{
        		return false;
        	}
        	InsMap.add(tMap);
        }
		return true;
	}

	private MMap TraTableB(String pContNo, String pInsuredNo,String pEdorNo)
	{
		MMap tMap = new MMap();

        LBPolSet tLBPolSet = new LBPolSet();
        LBPolDB tLBPolDB = new LBPolDB();
        tLBPolDB.setEdorNo(pEdorNo);
        tLBPolDB.setContNo(pContNo);
        tLBPolDB.setInsuredNo(pInsuredNo);
        tLBPolSet = tLBPolDB.query();
        if (tLBPolDB.mErrors.needDealError())
        {
        	System.out.println("查询被保险人" + pInsuredNo + "的险种保单失败！");
            return null;
        }
        for (int i = 1; i <= tLBPolSet.size(); i++)
        {
        	LBPolSchema tLBPolSchema = new LBPolSchema();
            tLBPolSchema = tLBPolSet.get(i);
            MMap tPolMap = new MMap();
            tPolMap = TraTableC(tLBPolSchema.getPolNo(),pEdorNo);
            tMap.add(tPolMap);
        }
		String[] lcTable =
		{
				"LCInsured", "LCCustomerImpart", "LCCustomerImpartParams"
		};
		String[] lbTable =
		{
				"LBInsured", "LBCustomerImpart", "LBCustomerImpartParams"
		};
		for (int i = 0; i < lcTable.length; i++)
		{
			String tSqlIns ="";
			String tSqlDel ="";
			if (lcTable[i].toUpperCase().equals("LCCUSTOMERIMPART") || lcTable[i].toUpperCase().equals("LCCUSTOMERIMPARTPARAMS"))
			{
				String tSqlCol = GetColName(lcTable[i]);
				if(tSqlCol == null || "".equals(tSqlCol))
				{
					return null;
				}
				tSqlIns = "insert into " + lcTable[i] + "(" + tSqlCol + ")" + " (select " + tSqlCol + " from " + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and ContNo ='" + pContNo + "' and CustomerNo ='" + pInsuredNo + "'  and CustomerNoType='I')";
				tSqlDel = "delete from " + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and ContNo ='" + pContNo + "' and CustomerNo ='" + pInsuredNo + "'  and CustomerNoType='I'";
				tMap.put(tSqlIns, "INSERT");
				delMap.put(tSqlDel, "DELETE");
			}
			else
			{
				String tSqlCol = GetColName(lcTable[i]);
				if(tSqlCol == null || "".equals(tSqlCol))
				{
					return null;
				}
				tSqlIns = "insert into " + lcTable[i] + "(" + tSqlCol + ")" + " (select " + tSqlCol + " from " + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and ContNo='" + pContNo + "' and InsuredNo = '" + pInsuredNo + "')";
				tSqlDel = "delete from " + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and ContNo='" + pContNo + "' and InsuredNo = '" + pInsuredNo + "'";
				tMap.put(tSqlIns, "INSERT");
				delMap.put(tSqlDel, "DELETE");
			}
		}
		return tMap;
	}

	private MMap TraTableC(String pPolNo,String pEdorNo)
	{
		MMap tMap = new MMap();
		String[] lcTable =
		{
				"LCPol", "LCInsureAcc", "LCInsureAccTrace", "LCDuty", "LCPrem", "LCGet", "LCBnf", "LCInsuredRelated", "LCPremToAcc", "LCGetToAcc", "LCInsureAccFee", "LCInsureAccClassFee", "LCInsureAccClass", "LCInsureAccFeeTrace", "LCCustomerImpart", "LCCustomerImpartParams"
		};
		String[] lbTable =
		{
				"LBPol", "LBInsureAcc", "LBInsureAccTrace", "LBDuty", "LBPrem", "LBGet", "LBBnf", "LBInsuredRelated", "LBPremToAcc", "LBGetToAcc", "LBInsureAccFee", "LBInsureAccClassFee", "LBInsureAccClass", "LBInsureAccFeeTrace", "LBCustomerImpart", "LBCustomerImpartParams"
		};
		for (int i = 0; i < lcTable.length; i++)
		{
			String tSqlIns ="";
			String tSqlDel ="";
			if (lcTable[i].toUpperCase().equals("LCCUSTOMERIMPART") || lcTable[i].toUpperCase().equals("LCCUSTOMERIMPARTPARAMS"))
			{
				String tSqlCol = GetColName(lcTable[i]);
				if(tSqlCol == null || "".equals(tSqlCol))
				{
					return null;
				}
				tSqlIns = "insert into " + lcTable[i] + "(" + tSqlCol + ")" + " (select " + tSqlCol + " from " + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and ContNo in (select ContNO from LCPol where PolNo='" + pPolNo + "') and CustomerNo in (select CustomerNo from LCBnf where PolNo='" + pPolNo + "' ) and CustomerNoType='3')";
				tSqlDel = "delete from "  + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and ContNo in (select ContNO from LCPol where PolNo='" + pPolNo + "') and CustomerNo in (select CustomerNo from LCBnf where PolNo='" + pPolNo + "' ) and CustomerNoType='3'";
				tMap.put(tSqlIns, "INSERT");
				delMap.put(tSqlDel, "DELETE");
			}
			else
			{
				String tSqlCol = GetColName(lcTable[i]);
				if(tSqlCol == null || "".equals(tSqlCol))
				{
					return null;
				}
				tSqlIns = "insert into " + lcTable[i] + "(" + tSqlCol + ")" + " (select " + tSqlCol + " from " + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and polno='" + pPolNo + "')";
				tSqlDel = "delete from " + lbTable[i] + " where EdorNo= '" + pEdorNo + "' and polno='" + pPolNo + "'";
				tMap.put(tSqlIns, "INSERT");
				delMap.put(tSqlDel, "DELETE");
			}
//			System.out.println(tSqlIns);
		}
		return tMap;
	}

	private boolean SetState(String pContNo,String pEdorNo)
	{
		String tSqlUpdate = "update lccont set stateflag = '1' where contno='" + pContNo + "'";
		mMap.put(tSqlUpdate, "UPDATE");
		tSqlUpdate = "update lcpol set stateflag = '1' where contno='" + pContNo + "'";
		mMap.put(tSqlUpdate, "UPDATE");
		tSqlUpdate = "update lccontstate set State = '0' where contno='" + pContNo + "' and polno ='000000' and StateType ='Terminate' and State = '1'";
		mMap.put(tSqlUpdate, "UPDATE");
		String tSqlQuery = "select polno from lbpol where contno = '" + pContNo + "' and EdorNo= '" + pEdorNo + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(tSqlQuery);
		if(tSSRS ==null || tSSRS.getMaxRow() < 1)
		{
			System.out.println("查询个人险种表失败！");
			return false;
		}
		for (int i=1;i<=tSSRS.getMaxRow();i++)
		{
			tSqlUpdate = "update lccontstate set State = '0' where contno='" + pContNo + "' and polno ='" + tSSRS.GetText(i, 1) + "' and StateType ='Terminate' and State = '1'";
			mMap.put(tSqlUpdate, "UPDATE");
		}
		return true;
	}

	private String GetColName(String pTableName)
	{
		String tSqlCol = "select colname from SYSCAT.COLUMNS where tabname='" + pTableName.toUpperCase() + "' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(tSqlCol);
		if (tSSRS == null || tSSRS.getMaxRow() == 0)
		{
			System.out.println("取得表[" + pTableName + "]的列名失败！");
			return null;
		}
		String tSql="";
		for (int k = 1; k <= tSSRS.getMaxRow(); k++)
		{
			if (k == 1)
			{
				tSql = tSql + tSSRS.GetText(k, 1);
			}
			else
			{
				tSql = tSql + "," + tSSRS.GetText(k, 1);
			}
		}
		return tSql;
	}

	/**
	 * 输出MAP内容(检查SQL用)
	 * @param tMap
	 */
	private void PrtMap(MMap tMap)
	{
		Set set = tMap.keySet();
		for (int j = 0; j < set.size(); j++)
        {
			Object o = tMap.getOrder().get(String.valueOf(j + 1));
			System.out.println((String)o);
        }
	}
	public static void main(String[] args)
	{
		TraTableBtoC tTraTableBtoC = new TraTableBtoC();
		tTraTableBtoC.submitData("000920385000002","20130708000021");
		//2010-12-20--cbs00042944
//		tTraTableBtoC.submitData("004746515000003","20101208000672");
//		2011-1-18
//		tTraTableBtoC.submitData("010271267000001","20110110000231");
//		2011-5-31
//		tTraTableBtoC.submitData("007329690000001","20110530000324");
//		2011-6-1
//		tTraTableBtoC.submitData("008237380000001","20110530000337");
//		2011-6-24
//		tTraTableBtoC.submitData("002512844000001","20110608000199");
//		2011-7-6
//		tTraTableBtoC.submitData("004728614000004","20110627001096");
//		2011-11-17
//		tTraTableBtoC.submitData("003278934000002",null);
//		tTraTableBtoC.submitData("003282330000002",null);
	
	}
}
