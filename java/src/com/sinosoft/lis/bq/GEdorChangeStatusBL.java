package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GEdorChangeStatusBL
{
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTTransferData = new TransferData();

    public GEdorChangeStatusBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }

        String updateSql = "";
        try
        {
            updateSql =
                    "update "
                    + (String) mTTransferData.getValueByName("tableName")
                    + " set edorState='"
                    + (String) mTTransferData.getValueByName("edorState") + "', "
                    + "Operator='" + this.mGlobalInput.Operator + "', "
                    + "ModifyDate='" + PubFun.getCurrentDate() + "', "
                    + "ModifyTime='" + PubFun.getCurrentTime() + "' "
                    + "where edorAcceptNo='"
                    + (String) mTTransferData.getValueByName("edorAcceptNo") + "' ";
        }
        catch(Exception e)
        {
            mErrors.addOneError("转入的数据不完整");
            return false;
        }

        MMap map = new MMap();
        VData inputData = new VData();
        PubSubmit tPubSubmit = new PubSubmit();

        map.put(updateSql, "UPDATE");
        inputData.add(map);

        if (!tPubSubmit.submitData(inputData, "UPDATE||MAIN"))
        {
            this.mErrors.addOneError("需要缴费，但是在修改保全申请状态为\"待缴费\"时出错");
            return false;
        }

        return true;
    }

    private boolean getInputData(VData inputData)
    {
        try
        {
            mGlobalInput.setSchema((GlobalInput) inputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTTransferData = (TransferData) inputData.
                             getObjectByObjectName("TransferData", 0);
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入后台的数据不完整");
            return false;
        }

        return true;
    }

    public static void main(String s[])
    {
        VData tVData = new VData();

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "endor";
        tGI.ComCode = "86";

        TransferData td = new TransferData();
        td.setNameAndValue("tableName", "LPEdorApp");
        td.setNameAndValue("edorState", "9");
        td.setNameAndValue("edorAcceptNo", "20050817000004");

        tVData.add(td);
        tVData.add(tGI);

        GEdorChangeStatusBL tGEdorChangeStatusBL = new GEdorChangeStatusBL();
        if (tGEdorChangeStatusBL.submitData(tVData, "ok") == false)
        {
            System.out.println(tGEdorChangeStatusBL.mErrors.getErrContent());
        }
        else
        {
            System.out.println("fail");
        }
    }
}

