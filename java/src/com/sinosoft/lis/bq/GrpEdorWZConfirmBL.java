package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 无名单增人确认类
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class GrpEdorWZConfirmBL implements EdorConfirm
{
    private GlobalInput mGlobalInput = null;
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    private MMap map = new MMap();

    public GrpEdorWZConfirmBL()
    {
    }

    /**
     * @return VData：处理后的结果集
     * @todo Implement this com.sinosoft.lis.bq.EdorConfirm method
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(map);

        return data;
    }

    /**
     * submitData
     * 外部操作的提交方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     * @todo Implement this com.sinosoft.lis.bq.EdorConfirm method
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 处理保全无名单增人业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
        if(!exchangeContDate())
        {
            return true;
        }

        return true;
    }

    /**
     * 交换保单数据
     * @return boolean：成功true，否则false
     */
    private boolean exchangeContDate()
    {
        String[] tables =
        {"LCCont", "LCPol", "LCDuty", "LCPrem"};
        ValidateEdorData validate = new ValidateEdorData(tables,
            mLPGrpEdorItemSchema.getEdorNo(), mLPGrpEdorItemSchema.getEdorType());
        if(!validate.submitData())
        {
            mErrors.copyAllErrors(validate.mErrors);
            return false;
        }
        MMap tMMap = validate.getMap();
        map.add(tMMap);

        return true;
    }

    private boolean getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) data
                               .getObjectByObjectName("LPGrpEdorItemSchema", 0);
        if(mLPGrpEdorItemSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo("20060628000002");
        tLPGrpEdorItemDB.setEdorType("WZ");

        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        VData d = new VData();
        d.add(g);
        d.add(tLPGrpEdorItemDB.query().get(1));

        GrpEdorWZConfirmBL b = new GrpEdorWZConfirmBL();
        if(!b.submitData(d, ""))
        {
            System.out.println(mErrors.getErrContent());
        }
        else
        {
            d.clear();
            d = b.getResult();

            PubSubmit p = new PubSubmit();
            if(!p.submitData(d, ""))
            {
                System.out.println(p.mErrors.getErrContent());
            }
        }
    }
}
