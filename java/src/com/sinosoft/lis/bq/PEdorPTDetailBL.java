package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PEdorPTDetailBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /**  */
//  private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
  private LPPolSchema mLPPolSchema = new LPPolSchema();
  private LPPolSet mLPPolSet = new LPPolSet();
  private LPDutySchema mLPDutySchema = new LPDutySchema();
  private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
  private MMap map = new MMap();


  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  public PEdorPTDetailBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public void setOperate(String cOperate)
  {
    this.mOperate=cOperate;
  }
  public  String getOperate()
  {
    return this.mOperate;
  }

  public boolean submitData(VData cInputData,String cOperate)
  {
  	System.out.println("----------PTDetail BL -------------------");
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    this.setOperate(cOperate);

    //得到外部传入的数据,将数据备份到本类中
    getInputData(cInputData)  ;

    System.out.println("after getInputData()");

    //数据查询业务处理(queryData())
    if (cOperate.equals("QUERY||MAIN")||cOperate.equals("QUERY||DETAIL"))
    {
      if(!queryData())
        return false;
      else
        return true;
    }


//数据校验操作（checkdata)
//    if (!checkData())
//      return false;

    System.out.println("BL: after check Data!");
    //数据准备操作（preparedata())
    if(!prepareData() )
      return false;

    System.out.println("BL: after prepareData");
    cOperate=this.getOperate();
    System.out.println("---oper"+cOperate);

    // 数据操作业务处理(新增insertData();修改updateData();删除deletedata())
    if (cOperate.equals("INSERT||MAIN")||cOperate.equals("INSERT||GRPMAIN"))
    {
      PubSubmit tSubmit = new PubSubmit();
      if (!tSubmit.submitData(mInputData, ""))
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "PEdorIODetailBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
      }


    }

	return true;

  }

  public VData getResult()
  {
    return mResult;
  }
  /**数据查询业务处理(queryData())
   *
   */
  private boolean queryData()
  {
    if (this.getOperate().equals("QUERY||MAIN"))
    {
      LPPolBL tLastLPPolBL = new LPPolBL();
      LPPolBL tLPPolBL = new LPPolBL();
      LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
      tLPEdorItemSchema = mLPEdorItemSchema.getSchema();
      LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
      tLPEdorItemDB.setSchema(tLPEdorItemSchema);
      if (!tLPEdorItemDB.getInfo()) {
      	System.out.println("LPEdorItemDB get info fail");
      	return false;
      }
      tLPEdorItemSchema = tLPEdorItemDB.getSchema();

      tLastLPPolBL.queryLastLPPol(mLPEdorItemSchema,mLPPolSchema);

      tLPPolBL.queryLPPol(mLPEdorItemSchema);
      double curAmnt = tLPPolBL.getAmnt();
      double curMulti = tLPPolBL.getMult();
      System.out.println("curAmnt: " + curAmnt);
      System.out.println("tLastLPPolBL : " + tLastLPPolBL);
      String tReturn;
      tReturn = tLastLPPolBL.encode();
      System.out.println("tReturn : " + tReturn);

      //现在获取保单对应的责任的计算方法，以便前台的显示需要
      String sqlStr = "select * from lcduty where polno = '" + tLPPolBL.getPolNo()+"'";
      System.out.println("SQL:" + sqlStr);
      LCDutyDB tLCDutyDB = new LCDutyDB();
      LCDutySet tLCDutySet = new LCDutySet();
      tLCDutySet = tLCDutyDB.executeQuery(sqlStr);

      System.out.println("tLCDutySet size :" + tLCDutySet.size());

      //目前情况下每个保单只有一个责任
      if (tLCDutySet == null || tLCDutySet.size() != 1)
          return false;

      LCDutySchema tempDutySchema = new LCDutySchema();
      String tDutyCode = tLCDutySet.get(1).getDutyCode();
      //利用责任编码找到计算方法编码
      sqlStr = "select * from LMDuty where DutyCode = " + tDutyCode;
      LMDutyDB tLMDutyDB = new LMDutyDB();
      LMDutySet tLMDutySet = new LMDutySet();
      tLMDutySet = tLMDutyDB.executeQuery(sqlStr);
      if (tLMDutySet == null || tLMDutySet.size() != 1)
          return false;

      String calMode = tLMDutySet.get(1).getCalMode();

      if (calMode.equals("O")) {
	      //将本次的份数填加到字符串后，以便前台显示
    	  tReturn  = tReturn + '|' + curMulti;
      } else {
      	   //将本次的保额填加到字符串后，以便前台显示
      		tReturn  = tReturn + '|' + curAmnt;
      }


      //最后加入编码串的calMode是为了前台保额不同退保的显示用的
      tReturn = "0|1" + "^" + tReturn + "|" + calMode + "|";

      System.out.println("tReturn :" + tReturn);
      mResult.clear();
      mResult.add(tReturn);
      return true;
    }
    return true;

  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private void getInputData(VData cInputData)
  {
    mLPPolSchema=(LPPolSchema)cInputData.getObjectByObjectName("LPPolSchema",0);
    mLPEdorItemSchema = (LPEdorItemSchema)cInputData.getObjectByObjectName("LPEdorItemSchema",0);
    mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
    LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
    tLPEdorItemDB.setSchema(mLPEdorItemSchema);
    if(!tLPEdorItemDB.getInfo()){
        // @@错误处理
        this.mErrors.copyAllErrors(tLPEdorItemDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "get edoritem err";
        tError.functionName = "getData";
        tError.errorMessage = "数据失败!";
        this.mErrors.addOneError(tError);
    }
    mLPEdorItemSchema = tLPEdorItemDB.getSchema();
    mLPEdorItemSchema.setEdorState("1");
    mLPEdorItemSchema.setEdorValiDate(PubFun.getCurrentDate());
    mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
    mLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
    mLPEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
    mLPEdorItemSchema.setMakeTime(PubFun.getCurrentTime());

    mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
    mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
  }


  /**
   * 校验传入的数据的合法性
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkData()
  {

    boolean flag = true;
    LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
    tLPEdorItemDB.setSchema(mLPEdorItemSchema);
    if (!tLPEdorItemDB.getInfo())
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PAppntIndBL";
      tError.functionName = "Preparedata";
      tError.errorMessage = "无保全申请数据!";
      System.out.println("------"+tError);
      this.mErrors.addOneError(tError);
      return false;
    }
    if (tLPEdorItemDB.getEdorState().trim().equals("2"))
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PAppntIndBL";
      tError.functionName = "Preparedata";
      tError.errorMessage = "该保全已经申请确认不能修改!";
      System.out.println("------"+tError);
      this.mErrors.addOneError(tError);
      return false;
    }

    return flag;

  }

  /**
   * 准备需要保存的数据
   */
  private boolean prepareData()
  {

      if (mLPEdorItemSchema == null || mLPPolSet == null)
      {
          CError tError = new CError();
          tError.moduleName = "PEdorPTDetailBL";
          tError.functionName = "Preparedata";
          tError.errorMessage = "保单为空失败!";
          System.out.println("------" + tError);
          this.mErrors.addOneError(tError);
          return false;
      }

      LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
      tLPEdorItemSchema.setSchema(mLPEdorItemSchema);
      tLPEdorItemSchema.setPolNo(mLPPolSchema.getPolNo());

      LPPolBL tLPPolBL = new LPPolBL();
      tLPPolBL.queryLPPol(tLPEdorItemSchema);
      LPPolSchema tLPPolSchema = tLPPolBL.getSchema();
      tLPPolSchema.setModifyDate(PubFun.getCurrentDate());
      tLPPolSchema.setModifyTime(PubFun.getCurrentTime());
      if (mLPPolSchema.getAmnt() > 0) //前台传入的是剩余保额
          tLPPolSchema.setAmnt(mLPPolSchema.getAmnt());
      if (mLPPolSchema.getMult() > 0) //前台传入的是剩余份数
          tLPPolSchema.setMult(mLPPolSchema.getMult());
      Reflections tReflections = new Reflections();
        tReflections.transFields(mLPPolSchema,tLPPolSchema);

          //获得保全责任

      LCDutyDB tLCDutyDB = new LCDutyDB();
      LCDutySchema tLCDutySchema = new LCDutySchema();
      tLCDutySchema.setPolNo(mLPPolSchema.getPolNo());
      tLCDutyDB.setSchema(tLCDutySchema);
      LCDutySet tLCDutySet = new LCDutySet();
      tLCDutySet = tLCDutyDB.query();

      //     System.out.println("LCDutySet size is :" + tLCDutySet.size());

      if (tLCDutySet==null||tLCDutySet.size() == 0)
      {
          CError tError = new CError();
          tError.moduleName = "PEdorPTDetailBL";
          tError.functionName = "Preparedata";
          tError.errorMessage = "责任表查询失败!";
          System.out.println("------" + tError);
          this.mErrors.addOneError(tError);
          return false;
      }

      //先实现单责任的减额功能
      if (tLCDutySet.size() != 1)
      {
          CError tError = new CError();
          tError.moduleName = "PEdorPTDetailBL";
          tError.functionName = "Preparedata";
          tError.errorMessage = "尚未实现险种下多责任的减额!";
          System.out.println("------" + tError);
          this.mErrors.addOneError(tError);
          return false;
      }


      LPDutyBL tLPDutyBL = new LPDutyBL();
      LPDutySet tLPDutySet = new LPDutySet();
      for (int j = 1; j <= tLCDutySet.size(); j++)
      {
          LCDutySchema tempLCDutySchema = tLCDutySet.get(j);
          LPDutySchema tLPDutySchema = new LPDutySchema();
          tLPDutySchema.setEdorNo(mLPPolSchema.getEdorNo());
          tLPDutySchema.setEdorType(mLPPolSchema.getEdorType());
          tLPDutySchema.setPolNo(mLPPolSchema.getPolNo());
          tLPDutySchema.setDutyCode(tempLCDutySchema.getDutyCode());

          tLPDutyBL.queryLPDuty(tLPDutySchema);
          if (tLPDutyBL == null)
              return false;

          LPDutySchema resultDutySchema = new LPDutySchema();
          resultDutySchema = tLPDutyBL.getSchema();
          resultDutySchema.setAmnt(mLPPolSchema.getAmnt());
          resultDutySchema.setModifyDate(PubFun.getCurrentDate());
          resultDutySchema.setModifyTime(PubFun.getCurrentTime());
          tLPDutySet.add(resultDutySchema);
      }


      //删除已有的项目，重新生成
      String sql = "delete from lpedoritem where edorno='"+ mLPEdorItemSchema.getEdorNo()
                   +"' and edortype='PT' and polno='000000' ";
      map.put(sql,"DELETE");
      map.put(mLPEdorItemSchema, "DELETE&INSERT");
      map.put(mLPPolSchema, "DELETE&INSERT");
      map.put(tLPDutySet, "DELETE&INSERT");
      mInputData.clear();
      mInputData.add(map);
      mResult.clear();
      mResult.add(map);

      return true;

  }

}
