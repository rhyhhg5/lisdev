package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PEdorZTConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /** 业务对象 */
    private LCPolBL mLCPolBL = new LCPolBL();
    private LCPolBLSet mLCPolBLSet = new LCPolBLSet();
    private LPPolBL mLPPolBL = new LPPolBL();
    private LPPolBLSet mLPPolBLSet = new LPPolBLSet();
    private LPInsuredBLSet mLPInsuredBLSet = new LPInsuredBLSet();
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();

    public PEdorZTConfirmBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public void setOperate(String cOperate)
    {
        this.mOperate = cOperate;
    }

    public String getOperate()
    {
        return this.mOperate;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.setOperate(cOperate);

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);
        System.out.println("after get input data....");

        //数据准备操作（preparedata()),撤单数据准备
        if (!prepareData())
            return false;
        System.out.println("after prepareData data....");

//    this.setOperate("CONFIRM||ZT");
//    System.out.println("---"+mOperate);
//
//    // 数据操作业务处理(新增insertData();修改updateData();删除deletedata())
//    PEdorConfirmBLS tPEdorConfirmBLS = new PEdorConfirmBLS();
//    System.out.println("Start Confirm ZT BL Submit...");
//    if (!tPEdorConfirmBLS.submitData(mInputData,mOperate)) {
//      // @@错误处理
//      this.mErrors.copyAllErrors(tPEdorConfirmBLS.mErrors);
//      CError tError = new CError();
//      tError.moduleName = "PEdorConfirmBL";
//      tError.functionName = "submitData";
//      tError.errorMessage = "数据提交失败!";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        String aPolNo = mLPEdorItemSchema.getPolNo();
        String aEdorNo = mLPEdorItemSchema.getEdorNo();
        LCPolDB tLCPolDB = new LCPolDB();

        ContCancel tContCancel = new ContCancel();
        tContCancel.setEdorType(mLPEdorItemSchema.getEdorType());
        if (mLPEdorItemSchema.getPolNo().equals("000000") &&
            mLPEdorItemSchema.getInsuredNo().equals("000000")) //合同退保
        {
            //处理医疗险的减人
            tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
            LCPolSet tLCPolSet = tLCPolDB.query();
            for(int i = 1; i <= tLCPolSet.size(); i++)
            {
                if (!dealEspecialPolAcc(tLCPolSet.get(i)))
                {
                    return false;
                }
            }

            MMap tMMap = tContCancel.prepareContData(
                mLPEdorItemSchema.getContNo(),
                aEdorNo);
            if(tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备个人保单数据失败！");
                return false;
            }
            map.add(tMMap);
        }
        else if (mLPEdorItemSchema.getPolNo().equals("000000")) //被保险人退保
        {
            tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
            tLCPolDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
            LCPolSet tLCPolSet = tLCPolDB.query();
            for(int i = 1; i <= tLCPolSet.size(); i++)
            {
                if (!dealEspecialPolAcc(tLCPolSet.get(i)))
                {
                    return false;
                }
            }

            MMap tMMap = tContCancel.prepareInsuredData(
                mLPEdorItemSchema.getContNo(),
                mLPEdorItemSchema.getInsuredNo(),
                aEdorNo);
            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备被保险人数据失败！");
                return false;
            }
            map.add(tMMap);

        }
        else
        {
            tLCPolDB.setPolNo(aPolNo);
            LCPolSet tLCPolSet = tLCPolDB.query();
            for(int i = 1; i <= tLCPolSet.size(); i++)
            {
                if (!dealEspecialPolAcc(tLCPolSet.get(i)))
                {
                    return false;
                }
            }

            MMap tMMap = new MMap();
            tMMap = tContCancel.preparePolData(aPolNo, aEdorNo);
            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备个人险种数据失败！");
                return false;
            }
            map.add(tMMap);
        }

        mInputData.clear();
        System.out.println("\n\nend 撤单数据准备");


        mLPEdorItemSchema.setEdorState("0");
        mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(mLPEdorItemSchema, "UPDATE");

        mResult.clear();
        mResult.add(map);

        return true;
    }

    /**
     * 若是医疗险减人，且退费选择了直接转入团体帐号的方式，则需要记录余额的流动
     * @return boolean
     */
    private boolean dealEspecialPolAcc(LCPolSchema tLCPolSchema)
    {
    	if (!CommonBL.isEspecialPol(tLCPolSchema.getRiskCode()))
        {
            return true;
        }
    	
        //查询退费方式
        EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(
            mLPEdorItemSchema.getEdorAcceptNo(), BQ.EDORTYPE_ZT);
        if (!tEdorItemSpecialData.query())
        {
            return true;
        }

        String balaPayType = tEdorItemSpecialData
                             .getEdorValue(BQ.DETAILTYPE_BALAPAYTYPE);
        if (balaPayType == null
            ||!balaPayType.equals(BQ.DETAILTYPE_BALAPAYTYPE1))
        {
            return true;
        }

        if (!createInsuAccTrace(tLCPolSchema))
        {
            return false;
        }
        transferMoney(tLCPolSchema);

        return true;
    }

    /**
     * 校验险种是否是特需险种
     * @param riskCode String
     * @return boolean
     */
    private boolean isEspecialPol(String riskCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(riskCode);
        tLMRiskAppDB.getInfo();
        String riskType = tLMRiskAppDB.getRiskType3();
        if ((riskType != null) && (riskType.equals(BQ.RISKTYPE3_SPECIAL)))
        {
            return true;
        }

        return false;
    }

    /**
     * 保险帐户表记价履历
     * @param tLCPolSchema LCPolSchema
     * @return boolean
     */
    private boolean createInsuAccTrace(LCPolSchema tLCPolSchema)
    {
        //团体帐户履历
        String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        LCPolSchema groupAccLCPolSchema = getGroupAccPolNo(
            tLCPolSchema.getGrpContNo());
        if (groupAccLCPolSchema == null)
        {
            return false;
        }
        double transMoney= getInsuredAccBala(tLCPolSchema.getPolNo());

        LCInsureAccTraceSchema tLCInsureAccTraceGrp =
            new LCInsureAccTraceSchema();
        tLCInsureAccTraceGrp.setGrpContNo(groupAccLCPolSchema.getGrpContNo());
        tLCInsureAccTraceGrp.setGrpPolNo(groupAccLCPolSchema.getGrpPolNo());
        tLCInsureAccTraceGrp.setContNo(groupAccLCPolSchema.getContNo());
        tLCInsureAccTraceGrp.setPolNo(groupAccLCPolSchema.getPolNo());
        tLCInsureAccTraceGrp.setSerialNo(serNo);
        tLCInsureAccTraceGrp.setInsuAccNo(CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP,groupAccLCPolSchema.getRiskCode()));
        tLCInsureAccTraceGrp.setRiskCode(groupAccLCPolSchema.getRiskCode());
        tLCInsureAccTraceGrp.setPayPlanCode(getPayPlanCode(tLCPolSchema));
        tLCInsureAccTraceGrp.setOtherNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLCInsureAccTraceGrp.setOtherType(BQ.NOTICETYPE_G);
        tLCInsureAccTraceGrp.setAccAscription("0");
        tLCInsureAccTraceGrp.setMoneyType("BF");
        tLCInsureAccTraceGrp.setMoney(transMoney);
        tLCInsureAccTraceGrp.setPayDate(PubFun.getCurrentDate());
        tLCInsureAccTraceGrp.setState("0");
        tLCInsureAccTraceGrp.setManageCom(mGlobalInput.ManageCom);
        tLCInsureAccTraceGrp.setOperator(mGlobalInput.Operator);
        tLCInsureAccTraceGrp.setMakeDate(PubFun.getCurrentDate());
        tLCInsureAccTraceGrp.setMakeTime(PubFun.getCurrentTime());
        tLCInsureAccTraceGrp.setModifyDate(PubFun.getCurrentDate());
        tLCInsureAccTraceGrp.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCInsureAccTraceGrp, "INSERT");

        //个人帐户履历--保单退保时会自动生成不需要再插入。
//        String serNoInsured = PubFun1.CreateMaxNo("SERIALNO", tLimit);
//        LCInsureAccTraceSchema tLCInsureAccTraceSchema =
//            new LCInsureAccTraceSchema();
//        tLCInsureAccTraceSchema.setSchema(tLCInsureAccTraceGrp);
//        tLCInsureAccTraceSchema.setContNo(tLCPolSchema.getContNo());
//        tLCInsureAccTraceSchema.setPolNo(tLCPolSchema.getPolNo());
//        tLCInsureAccTraceSchema.setInsuAccNo(CommonBL
//                                             .getInsuAccNo(BQ.ACCTYPE_INSURED,tLCPolSchema.getRiskCode()));
//        tLCInsureAccTraceSchema.setMoney( -transMoney);
//        tLCInsureAccTraceSchema.setSerialNo(serNoInsured);
//        map.put(tLCInsureAccTraceSchema, "INSERT");

        return true;
    }

    /**
     * 通过保险账户分类表查询payPlanCode
     * 若有多个则任选一
     * @param tLCPolSchema LCPolSchema
     * @return String
     */
    private String getPayPlanCode(LCPolSchema tLCPolSchema)
    {
        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setPolNo(tLCPolSchema.getPolNo());
        tLCInsureAccClassDB.setInsuredNo(tLCPolSchema.getInsuredNo());
        LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
        return tLCInsureAccClassSet.size() == 0 ? BQ.FILLDATA
            : tLCInsureAccClassSet.get(1).getPayPlanCode();
    }

    /**
     * 查询团单团体理赔账户险种号
     * @param grpContNo String
     * @return String
     */
    private LCPolSchema getGroupAccPolNo(String grpContNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(grpContNo);
        tLCPolDB.setPolTypeFlag("2");
        tLCPolDB.setAccType(BQ.ACCTYPE_PUBLIC);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if(tLCPolSet.size() == 0)
        {
            mErrors.addOneError("个人账户余额需要转入团体账户，但没有查询到团体账户。");
            return null;
        }

        return tLCPolSet.get(1);
    }

    /**
     * 个人账户余额方到团体固定账户中
     * @param tLCPolSchema LCPolSchema
     */
    private void transferMoney(LCPolSchema tLCPolSchema)
    {
        double insuredAccBala = getInsuredAccBala(tLCPolSchema.getPolNo());
        String polNo = getGroupAccPolNo(tLCPolSchema.getGrpContNo()).getPolNo();
        String sql = "  update LCInsureAcc "
                     + "set SumPay = SumPay + " + insuredAccBala + ", "
                     + "    InsuAccBala = InsuAccBala + " + insuredAccBala + " "
                     + "where grpContNo = '" + tLCPolSchema.getGrpContNo() + "' "
                     + "    and polNo = '" + polNo + "' "
                     + "    and accType = '" + BQ.ACCTYPE_GROUP + "' ";
        map.put(sql, "UPDATE");
    }

    /**
     * 查询个人账户余额
     * @param polNo String
     * @return double
     */
    private double getInsuredAccBala(String polNo)
    {
        String sql = "  select sum(InsuAccBala) "
                     + "from LCInsureAcc "
                     + "where polNo = '" + polNo + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String insuAccBala = tExeSQL.getOneValue(sql);
        return insuAccBala.equals("") ? 0 : Double.parseDouble(insuAccBala);
    }

}
