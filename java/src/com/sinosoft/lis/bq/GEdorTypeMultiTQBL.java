package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.util.*;
public class GEdorTypeMultiTQBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private String mOperate;

    private LPDiskImportSet mLPDiskImportSet = null;
    
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema=null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
    private MMap map = new MMap();


    public GEdorTypeMultiTQBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("---Start of GEdorZTDetail->submitData---");
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;
        
        System.out.println("GEdorZTDetailBL中mOperate = "+mOperate);

        //得到外部传入的数据
        if (!getInputData())
        {
            return false;
        }
        MMap tMMap = new MMap();
        tMMap.put(mLPDiskImportSet, SysConst.UPDATE);
        String sqlString = "update LPGrpEdorItem set edorstate='3' where edorno= '"+mLPGrpEdorItemSchema.getEdorNo()
                            +"'   and  edortype = '" +mLPGrpEdorItemSchema.getEdorType()
                            +"'   and  grpcontno = '" +mLPGrpEdorItemSchema.getGrpContNo()+"' ";
        tMMap.put(sqlString, SysConst.UPDATE);
        mResult.add(tMMap);
        //数据准备操作
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("GedorDetailBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- GEdorZTDetail->submitData--- 数据提交成功。");

        return true;
    }



    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
    	mLPDiskImportSet = (LPDiskImportSet) mInputData
                .getObjectByObjectName("LPDiskImportSet", 0);
    	
    	mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) mInputData
        .getObjectByObjectName("LPGrpEdorItemSchema", 0);
    	
        if (mLPDiskImportSet.size() == 0)
        {
            mErrors.addOneError("未找到信息！");
            return false;
        }
        if (mLPGrpEdorItemSchema==null)
        {
            mErrors.addOneError("未找到信息！");
            return false;
        }
        return true;
    }
    
}
