package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LPInsuredDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LPAddressSchema;
import com.sinosoft.lis.schema.LPAppntSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.schema.LPPersonSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LPInsuredSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 保全变更投保人项目</p>
 * <p>Description: 录入保全明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 * 【OoO】杨天政修改
 */

public class PEdorAEDetailBL
{
    public CErrors mErrors = new CErrors();

    private String mMessage = null;

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPAppntSchema mLPAppntSchema = null;

    private LPAddressSchema mLPAddressSchema = null;
    
    private LPInsuredSet mLPInsuredSet = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mInsuredNo = null;

    private String mRelation = null;
    
    private String mRelationToAppnt = null;
    
    private String mInsuredno = null;

    private String msql = null;
    
    private String mPayMode = null;

    private String mAddressNo = null;

    private OldCustomerCheck mCheck = null;

    private DetailDataQuery mQuery = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
    {    System.out.println("Begin the PEDORAEDETAILBL");
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 取得传入的数据
     * @param data VData
     * @return boolean
     */
    private boolean getInputData(VData data)
    {   
    	System.out.println("Begin the PEDORAEDETAILBL.getInputData(VData data)");
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput",
                    0);
            mLPAppntSchema = (LPAppntSchema) data.getObjectByObjectName(
                    "LPAppntSchema", 0);
            mLPAddressSchema = (LPAddressSchema) data.getObjectByObjectName(
                    "LPAddressSchema", 0);
            TransferData td = (TransferData) data.getObjectByObjectName(
                    "TransferData", 0);
            mLPInsuredSet = (LPInsuredSet) data.getObjectByObjectName(
                    "LPInsuredSet", 0);
            
            
            
            mEdorNo = mLPAppntSchema.getEdorNo();
            mEdorType = mLPAppntSchema.getEdorType();
            mContNo = mLPAppntSchema.getContNo();
            mPayMode = (String) td.getValueByName("PayMode");
            mRelation = (String) td.getValueByName("Relation");
            mQuery = new DetailDataQuery(mEdorNo, mEdorType);
            
           
           // mRelationToAppnt = (String) td.getValueByName("RelationToAppnt");
          //  mInsuredno = (String) td.getValueByName("Insuredno");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
    	System.out.println("Begin the PEDORAEDETAILBL.dealData()");
        int checkRet = checkOldCustomer();
        if (checkRet == OldCustomerCheck.OLD) //如果是老客户则按老客户处理
        {
            mMessage = "该客户是老客户，将使用原来的客户号。";
            if (!dealAsOldCustomer())
            {
                return false;
            }
        }
        else //新客户和可能是老客户都按新客户处理
        {
            if (checkRet == OldCustomerCheck.MAYBEOLD)
            {
//                String workNo = mCheck.createCombineTask(mGlobalInput);
//                if (workNo == null)
//                {
//                    mErrors.addOneError("生成作业合并工单失败！");
//                    return false;
//                }
//                mMessage = "该客户可能是老客户，已新建工单号为"
//                        + workNo + "的客户合并任务。";
                mMessage = "该客户可能是老客户！";
            }
            if (!dealAsNewCustomer())
            {
                return false;
            }
        }
        System.out.println("Begin the dealData()");
        
//        setRelation2Appnt();
        setEdorState();
        return true;
    }
    
    /**
     * 处理被保人和投保人之间的关系
     *
     */
    
    private void setRelation2Appnt()
    {
    	
    	System.out.println("Begin the PEDORAEDETAILBL.dealData().setRelation2Appnt()");
    	LPInsuredDB tLPInsuredDB =new LPInsuredDB();
    	LPInsuredSchema tLPInsuredShema = new LPInsuredSchema(); 
    	 for(int index=1;index<=mLPInsuredSet.size();index++)
         {
         System.out.println("Begin the PEDORAEDETAILBL.dealData().setRelation2Appnt().for()"+index);
         tLPInsuredDB.setEdorNo(mEdorNo);
         tLPInsuredDB.setEdorType(mEdorType);
         tLPInsuredDB.setContNo(mContNo);
         tLPInsuredDB.setInsuredNo(mLPInsuredSet.get(index).getInsuredNo());
         
            if(!tLPInsuredDB.getInfo())
             {
        	    return ;
        	
             }
            
              tLPInsuredShema=tLPInsuredDB.getSchema();
              tLPInsuredShema.setRelationToAppnt(mLPInsuredSet.get(index).getRelationToAppnt());
//         msql = "update LPInsured set relationtoappnt = '"+ mLPInsuredSet.get(index).getRelationToAppnt()+"' " +
//                 " where insuredno = '" + mLPInsuredSet.get(index).getInsuredNo() + "' ";
                 
         mMap.put(tLPInsuredShema, "UPDATE");
         
        }
 
    }
    
    /**
     * 得到原保单地址信息
     * @return LCAddressSchema
     */
    private LCAddressSchema getLCAddress()
    {
        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(mCheck.getCustomerNo());
        tLCAddressDB.setAddressNo(mAddressNo);
        tLCAddressDB.getInfo();
        return tLCAddressDB.getSchema();
    }

    /**
     * 校验该客户是否是老客户
     * @return int
     */
    private int checkOldCustomer()
    {
    	System.out.println("Begin the PEDORAEDETAILBL.dealData().checkOldCustomer()");
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        if(!"0".equals(mLPAppntSchema.getIDType())){
        	tLDPersonSchema.setName(mLPAppntSchema.getAppntName());
        	tLDPersonSchema.setSex(mLPAppntSchema.getAppntSex());
        	tLDPersonSchema.setBirthday(mLPAppntSchema.getAppntBirthday());
        	tLDPersonSchema.setIDType(mLPAppntSchema.getIDType());
        	tLDPersonSchema.setIDNo(mLPAppntSchema.getIDNo());
        }else{
        	tLDPersonSchema.setName(mLPAppntSchema.getAppntName());
        	tLDPersonSchema.setIDType(mLPAppntSchema.getIDType());
        	tLDPersonSchema.setIDNo(mLPAppntSchema.getIDNo());
        	
        }
        
        mCheck = new OldCustomerCheck(tLDPersonSchema);
        return mCheck.check();
    }

    /**
     * 按老客户处理
     * @return boolean
     */
    private boolean dealAsOldCustomer()
    {
    	System.out.println("Begin the PEDORAEDETAILBL.dealData().dealAsOldCustomer()");
        setLPPerson();
        setLPAppnt();
        setLPCont();
        setLPPol();
        setLPAddress();
        
        if(!setRelation())
        {
        	return false;
        }
        return true;
    }

    /**
     * 按新客户处理
     * @return boolean
     */
    private boolean dealAsNewCustomer()
    {
    	System.out.println("Begin the PEDORAEDETAILBL.dealData().dealAsNewCustomer()");
        setLPPerson();
        setLPAppnt();
        setLPCont();
        setLPPol();
        setLPAddress();
        if(!setRelation())
        {
        	return false;
        }
        return true;
    }

    /**
     * 设置客户信息
     */
    private void setLPPerson()
    {
        LDPersonSchema tLDPersonSchema = mCheck.getLDPerson();
        tLDPersonSchema.setName(mLPAppntSchema.getAppntName());
        tLDPersonSchema.setSex(mLPAppntSchema.getAppntSex());
        tLDPersonSchema.setBirthday(mLPAppntSchema.getAppntBirthday());
        tLDPersonSchema.setIDType(mLPAppntSchema.getIDType());
        tLDPersonSchema.setIDNo(mLPAppntSchema.getIDNo());
        tLDPersonSchema.setOccupationCode(mLPAppntSchema.getOccupationCode());
        tLDPersonSchema.setOccupationType(mLPAppntSchema.getOccupationType());
        tLDPersonSchema.setOperator(mGlobalInput.Operator);
        tLDPersonSchema.setMakeDate(mCurrentDate);
        tLDPersonSchema.setMakeTime(mCurrentTime);
        tLDPersonSchema.setModifyDate(mCurrentDate);
        tLDPersonSchema.setModifyTime(mCurrentTime);

        LPPersonSchema tLPPersonSchema = new LPPersonSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPPersonSchema, tLDPersonSchema);
        tLPPersonSchema.setEdorNo(mEdorNo);
        tLPPersonSchema.setEdorType(mEdorType);
        mMap.put(tLPPersonSchema, "DELETE&INSERT");
    }

    /**
     * 设置投保人信息
     */
    public void setLPAppnt()
    {
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mContNo);
        if (!tLCAppntDB.getInfo())
        {
            mErrors.addOneError("未找到投保人信息！");
            return;
        }

        LPAppntSchema tLPAppntSchema = (LPAppntSchema)
        mQuery.getDetailData("LCAppnt", tLCAppntDB.getContNo());
        tLPAppntSchema.setAppntNo(mCheck.getCustomerNo());
        tLPAppntSchema.setAppntName(mLPAppntSchema.getAppntName());
        tLPAppntSchema.setAppntSex(mLPAppntSchema.getAppntSex());
        tLPAppntSchema.setAppntBirthday(mLPAppntSchema.getAppntBirthday());
        tLPAppntSchema.setIDType(mLPAppntSchema.getIDType());
        tLPAppntSchema.setIDNo(mLPAppntSchema.getIDNo());
        tLPAppntSchema.setOccupationCode(mLPAppntSchema.getOccupationCode());
        tLPAppntSchema.setOccupationType(mLPAppntSchema.getOccupationType());
        tLPAppntSchema.setBankCode(mLPAppntSchema.getBankCode());
        tLPAppntSchema.setBankAccNo(mLPAppntSchema.getBankAccNo());
        tLPAppntSchema.setAccName(mLPAppntSchema.getAccName());
        tLPAppntSchema.setMarriage("");
        tLPAppntSchema.setOperator(mGlobalInput.Operator);
        tLPAppntSchema.setModifyDate(mCurrentDate);
        tLPAppntSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPAppntSchema, "DELETE&INSERT");

        //得到AddressNo
        mAddressNo = tLPAppntSchema.getAddressNo();
    }

    /**
     * 设置保单信息
     */
    private void setLPCont()
    {
        LPContSchema tLPContSchema = (LPContSchema)
                mQuery.getDetailData("LCCont", mContNo);
        tLPContSchema.setAppntNo(mCheck.getCustomerNo());
        tLPContSchema.setAppntName(mLPAppntSchema.getAppntName());
        tLPContSchema.setAppntSex(mLPAppntSchema.getAppntSex());
        tLPContSchema.setAppntBirthday(mLPAppntSchema.getAppntBirthday());
        tLPContSchema.setAppntIDType(mLPAppntSchema.getIDType());
        tLPContSchema.setAppntIDNo(mLPAppntSchema.getIDNo());
        tLPContSchema.setPayMode(mPayMode);
        tLPContSchema.setBankCode(mLPAppntSchema.getBankCode());
        tLPContSchema.setBankAccNo(mLPAppntSchema.getBankAccNo());
        tLPContSchema.setAccName(mLPAppntSchema.getAccName());
        tLPContSchema.setOperator(mGlobalInput.Operator);
        tLPContSchema.setModifyDate(mCurrentDate);
        tLPContSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPContSchema, "DELETE&INSERT");
        this.mInsuredNo = tLPContSchema.getInsuredNo();
    }

    /**
     * 设置险种信息
     */
    private void setLPPol()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mContNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            LPPolSchema tLPPolSchema = (LPPolSchema)
                    mQuery.getDetailData("LCPol", tLCPolSchema.getPolNo());
            tLPPolSchema.setAppntNo(mCheck.getCustomerNo());
            tLPPolSchema.setAppntName(mLPAppntSchema.getAppntName());
            tLPPolSchema.setPayMode(mPayMode);
            tLPPolSchema.setOperator(mGlobalInput.Operator);
            tLPPolSchema.setModifyDate(mCurrentDate);
            tLPPolSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPPolSchema, "DELETE&INSERT");
        }
    }

    //2011-04-04杨天政添加，增加投保人与被保人的关系。要创建LPINSURED表。
    private boolean setRelation()
    {
    	LPInsuredDB tLPInsuredDB =new LPInsuredDB();
    	LPInsuredSet tLPInsuredSet1 = new LPInsuredSet();
    	LPInsuredSet tLPInsuredSet = new LPInsuredSet();
        LCInsuredDB tLCInsuredDB =new LCInsuredDB();
    	 for(int index=1;index<=mLPInsuredSet.size();index++)
         {
         System.out.println("Begin the PEDORAEDETAILBL.dealData().setRelation2Appnt().for()"+index);
         tLPInsuredDB.setEdorNo(mEdorNo);
         tLPInsuredDB.setEdorType(mEdorType);
         tLPInsuredDB.setContNo(mContNo);
         tLPInsuredDB.setInsuredNo(mLPInsuredSet.get(index).getInsuredNo());
         
                if(!tLPInsuredDB.getInfo())
                 {
    	
                   tLCInsuredDB.setContNo(mContNo);	 
                   tLCInsuredDB.setInsuredNo(mLPInsuredSet.get(index).getInsuredNo());
                   if(!tLCInsuredDB.getInfo())
                       {
                	     mErrors.addOneError("没有查到保险人的信息");
   	                     return false;
                       }

//                   if(tLCInsuredDB.getRelationToMainInsured()!=null
//                		   &&!tLCInsuredDB.getRelationToMainInsured().equals("")
//                		   &&!tLCInsuredDB.getRelationToMainInsured().equals("null"))
//                   {
//                	   if(tLCInsuredDB.getRelationToMainInsured().equals("00")
//                		   &&(mLPInsuredSet.get(index).getRelationToAppnt()==null
//                				   ||mLPInsuredSet.get(index).getRelationToAppnt().equals("")
//                				   ||mLPInsuredSet.get(index).getRelationToAppnt().equals("null")))
//                       {
//                	     mErrors.addOneError("没有录入主被保险人"+tLCInsuredDB.getName()+"和投保人的关系");
//                	     return false;
//                       }
//                   }
                   //----------------添加，关于getRelationToMainInsured为空的校验---
                   if(tLCInsuredDB.getRelationToMainInsured()==null||tLCInsuredDB.getRelationToMainInsured().equals("00"))
                   {
                        if(mLPInsuredSet.get(index).getRelationToAppnt()==null
                           ||mLPInsuredSet.get(index).getRelationToAppnt().equals("")
                           ||mLPInsuredSet.get(index).getRelationToAppnt().equals("null"))
                          {
                           mErrors.addOneError("没有录入主被保险人"+tLCInsuredDB.getName()+"和投保人的关系");
                           return false;
                          }
                   }
                     LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema(); 
                     tLPInsuredSchema.setEdorNo(mEdorNo);
                     tLPInsuredSchema.setEdorType(mEdorType);
                     tLPInsuredSchema.setContNo(mContNo);
                     tLPInsuredSchema.setInsuredNo(mLPInsuredSet.get(index).getInsuredNo());
                     tLPInsuredSchema.setGrpContNo(tLCInsuredDB.getGrpContNo());
                     tLPInsuredSchema.setPrtNo(tLCInsuredDB.getPrtNo());
                     tLPInsuredSchema.setManageCom(tLCInsuredDB.getManageCom());
                     tLPInsuredSchema.setExecuteCom(tLCInsuredDB.getExecuteCom());
                     tLPInsuredSchema.setName(tLCInsuredDB.getName());
                     tLPInsuredSchema.setSex(tLCInsuredDB.getSex());
                     tLPInsuredSchema.setBirthday(tLCInsuredDB.getBirthday());
                     tLPInsuredSchema.setIDType(tLCInsuredDB.getIDType());
                     tLPInsuredSchema.setIDNo(tLCInsuredDB.getIDNo());
                     tLPInsuredSchema.setAppntNo(mCheck.getCustomerNo());
                     tLPInsuredSchema.setRelationToMainInsured(tLCInsuredDB.getRelationToMainInsured());
                     tLPInsuredSchema.setRelationToAppnt(mLPInsuredSet.get(index).getRelationToAppnt());
                     tLPInsuredSchema.setOperator(mGlobalInput.Operator);
                     tLPInsuredSchema.setMakeDate(mCurrentDate);
                     tLPInsuredSchema.setMakeTime(mCurrentTime);
                     tLPInsuredSchema.setModifyDate(mCurrentDate);
                     tLPInsuredSchema.setModifyTime(mCurrentTime);
                     System.out.println("Begin the PEDORAEDETAILBL.dealData().setRelation2Appnt().for()"+index);
                     tLPInsuredSet.add(tLPInsuredSchema);
                 }
                else
                {   
                	LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema(); 
                	tLPInsuredSchema=tLPInsuredDB.getSchema();
                	tLPInsuredSchema.setRelationToAppnt(mLPInsuredSet.get(index).getRelationToAppnt());
                	tLPInsuredSet1.add(tLPInsuredSchema);       	
                }    
         }
    	 
    	 
    	 mMap.put(tLPInsuredSet1, "UPDATE");
    	 mMap.put(tLPInsuredSet, "DELETE&INSERT");
    	 
    	 return true;
    }


    /**
     * 设置地址信息，因为LPAddress表AddressNo是主键，不能用"DELETE&INSERT"方式删除
     */
    private void setLPAddress()
    {
        //先删除上次录入的数据
        String sql = "delete from LPAddress " +
                     "where EdorNo = '" + mLPAppntSchema.getEdorNo() + "' " +
                     "and EdorType = '" + mLPAppntSchema.getEdorType() + "' " +
                     "and AddressNo = '" + mAddressNo + "'";
        mMap.put(sql, "DELETE");

        LCAddressSchema tLCAddressSchema = getLCAddress();
        tLCAddressSchema.setPhone(mLPAddressSchema.getPhone());
        tLCAddressSchema.setPostalAddress(mLPAddressSchema.getPostalAddress());
        tLCAddressSchema.setZipCode(mLPAddressSchema.getZipCode());
        tLCAddressSchema.setOperator(mGlobalInput.Operator);
        tLCAddressSchema.setMakeDate(mCurrentDate);
        tLCAddressSchema.setMakeTime(mCurrentTime);
        tLCAddressSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAddressSchema.setModifyTime(PubFun.getCurrentTime());

        LPAddressSchema tLPAddressSchema = new LPAddressSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPAddressSchema, tLCAddressSchema);
        tLPAddressSchema.setEdorNo(mEdorNo);
        tLPAddressSchema.setEdorType(mEdorType);
        mMap.put(tLPAddressSchema, "INSERT");
    }

    /**
     * 设置保全状态,"1"为明细录入完成状态
     */
    private void setEdorState()
    {
        String sql;
        sql = "update LPEdorApp set EdorState = '1' " +
                "where EdorAcceptNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "update LPEdorMain set EdorState = '1' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and ContNo = '" + mContNo + "'";
        mMap.put(sql, "UPDATE");
        sql = "update LPEdorItem set EdorState = '1' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "and ContNo = '" + mContNo + "'";
        mMap.put(sql, "UPDATE");
    }
    
    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
