package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单犹豫期退保明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Alex
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorWTDetailUI {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public PEdorWTDetailUI() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate)	{
    // 数据操作字符串拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    PEdorWTDetailBL tPEdorWTDetailBL = new PEdorWTDetailBL();
    if(!tPEdorWTDetailBL.submitData(mInputData, mOperate))	{
      // @@错误处理
      this.mErrors.copyAllErrors(tPEdorWTDetailBL.mErrors);
      return false;
    }	else {
      mResult = tPEdorWTDetailBL.getResult();
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 主函数，测试用
   */
  public static void main(String[] args)
  {
      GlobalInput tGlobalInput = new GlobalInput();
      tGlobalInput.Operator = "pa0001";
      tGlobalInput.ManageCom = "86";

      LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
      tLPEdorItemSchema.setEdorNo("20051216000031");
      tLPEdorItemSchema.setEdorAcceptNo(tLPEdorItemSchema.getEdorNo());
      tLPEdorItemSchema.setContNo("00001700001");
      tLPEdorItemSchema.setEdorType("WT");

      //险种信息
      LPPolSet mLPPolSet = new LPPolSet();
      LPPolSchema tLPPolSchema = new LPPolSchema();
      tLPPolSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
      tLPPolSchema.setEdorType(tLPEdorItemSchema.getEdorType());
      tLPPolSchema.setContNo(tLPEdorItemSchema.getContNo());
      tLPPolSchema.setInsuredNo("000017000");
      tLPPolSchema.setPolNo("21000013567");
      mLPPolSet.add(tLPPolSchema);

      //体检费
      LPPENoticeSet tLPPENoticeSet = new LPPENoticeSet();
      LPPENoticeSchema schema = new LPPENoticeSchema();
      schema.setEdorAcceptNo(tLPEdorItemSchema.getEdorNo());
      schema.setEdorNo(tLPEdorItemSchema.getEdorNo());
      schema.setEdorType(tLPEdorItemSchema.getEdorType());
      schema.setCustomerNo("000017000");
      schema.setProposalContNo("13000020162");
      schema.setPrtSeq("12040551005201");
      tLPPENoticeSet.add(schema);

      //工本费
      /*
      LPEdorEspecialDataSchema tLPEdorEspecialDataSchema =
            new LPEdorEspecialDataSchema();
      tLPEdorEspecialDataSchema.setEdorAcceptNo("20051020000015");
      tLPEdorEspecialDataSchema.setEdorNo("20051020000015");
      tLPEdorEspecialDataSchema.setEdorType("WT");
      tLPEdorEspecialDataSchema.setDetailType(BQ.DETAILTYPE_GB);
      tLPEdorEspecialDataSchema.setEdorValue("10");
      */


      VData tVData = new VData();
      tVData.add(tGlobalInput);
      tVData.add(tLPEdorItemSchema);
      tVData.add(mLPPolSet);
      tVData.add(tLPPENoticeSet);
      //tVData.add(tLPEdorEspecialDataSchema);

      PEdorWTDetailUI tPEdorWTDetailUI = new PEdorWTDetailUI();
      if (!tPEdorWTDetailUI.submitData(tVData, "INSERT"))
      {
          System.out.println(tPEdorWTDetailUI.mErrors.getErrContent());
      }
  }
}
