package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团单退保保全确认</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 2.0
 */
public class GrpEdorCMConfirmBL implements EdorConfirm
{
    /** 全局数据 */
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null; //保全项目信息
    private GlobalInput mGI = null; //完整的操作员信息

    private String mEdorNo = null; //受理号
    private String mEdorType = null; //项目类型
    private String mGrpContNo = null; //团单号
    private String mCrrDate = PubFun.getCurrentDate();
    private String mCrrTime = PubFun.getCurrentTime();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private MMap map = new MMap();

    public GrpEdorCMConfirmBL()
    {
    }

    /**
     * 
     * 通过模板导入被保人的没有生成个人保全项目，
     * 在团体保全层将这些保全数据生效
     * @param cInputData VData：包含：
     A.	GlobalInput对象，完整的登陆用户信息。
     B.	LPGrpEdorItem对象，保全项目信息。
     * @param cOperate String:此为“”
     * @return boolean, 成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //一失足成千古恨，早知道在导入的时候直接自动生成个人保全就完事了。。。
        if(!getInputData(cInputData))
        {
            return false;
        }
		if(!checkData())
		{
			return false;
		}
        //处理客户资料变更导入的数据
        if(!dealData())
        {
            return false;
        }

        return true;
    }

    private boolean checkData() {
    	
		return true;
	}

	public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @param: 无
     * @return: boolean，操作成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) data
                               .getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mGI = ((GlobalInput) data
               .getObjectByObjectName("GlobalInput", 0));

        if(mLPGrpEdorItemSchema == null || mGI == null)
        {
            this.mErrors.addOneError("接收数据失败");
            return false;
        }

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if(tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1)
        {
            mErrors.addOneError("查询保全项目信息失败！");
            System.out.println("GrpEdorWTConfirmBL->getInputData: "
                               + tLPGrpEdorItemDB.mErrors.getErrContent());
            return false;
        }
        mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemSet.get(1));

        mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
        mEdorType = mLPGrpEdorItemSchema.getEdorType();
        mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
        if(mEdorNo == null || mEdorType == null || mGrpContNo == null)
        {
            mErrors.addOneError("传入的数据不完整");
            return false;
        }

        return true;
    }

    /**
     * 因为通过模板导入被保人的没有生成个人保全项目，在这里将保全数据生效
     * 
     * @return boolean，操作成功true，否则false
     */
    private boolean dealData()
    {
    	//只处理模板导入的且没有添加个人保全项目的
    	String pSql="select a.* from lpinsured a,lpdiskimport b "
						+" where a.edorno=b.edorno "
						+" and a.edortype=b.edortype "
						+" and a.insuredno=b.insuredno "
						+" and a.grpcontno='"+mGrpContNo+"' "
						+" and a.edorno='"+mEdorNo+"' and a.edortype='"+mEdorType+"' "
						+" and not exists (select 1 from lpedoritem where edorno=a.edorno and edortype=a.edortype and insuredno=a.insuredno) "
						+" with ur ";
    	LPInsuredDB tLPInsuredDB=new LPInsuredDB();
    	LPInsuredSet tLPInsuredSet=tLPInsuredDB.executeQuery(pSql);
    	LPInsuredSet saveLPInsuredSet=new LPInsuredSet();
    	LCInsuredSet saveLCInsuredSet=new LCInsuredSet();
    	
    	Reflections tRef = new Reflections();
    	
    	for(int i=1; i<=tLPInsuredSet.size(); i++){
    		LPInsuredSchema aLPInsuredSchema=tLPInsuredSet.get(i);
    		
    		LCInsuredSchema aLCInsuredSchema = new LCInsuredSchema();
    		LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
    		LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
    		
    		LCInsuredDB aLCInsuredDB = new LCInsuredDB();
			aLCInsuredDB.setContNo(aLPInsuredSchema.getContNo());
			aLCInsuredDB.setInsuredNo(aLPInsuredSchema.getInsuredNo());
			if (!aLCInsuredDB.getInfo())
			{
				mErrors.copyAllErrors(aLCInsuredDB.mErrors);
				mErrors.addOneError(new CError("查询被保人信息失败！"));
				return false;
			}
			aLCInsuredSchema = aLCInsuredDB.getSchema();
			tRef.transFields(tLPInsuredSchema, aLCInsuredSchema);
			tLPInsuredSchema.setEdorNo(aLPInsuredSchema.getEdorNo());
			tLPInsuredSchema.setEdorType(aLPInsuredSchema.getEdorType());
			tLPInsuredSchema.setModifyDate(mCrrDate);
			tLPInsuredSchema.setModifyTime(mCrrTime);

			tRef.transFields(tLCInsuredSchema, aLPInsuredSchema);
			tLCInsuredSchema.setModifyDate(mCrrDate);
			tLCInsuredSchema.setModifyTime(mCrrTime);

			saveLPInsuredSet.add(tLPInsuredSchema);
			saveLCInsuredSet.add(tLCInsuredSchema);
    	}
    	map.put(saveLCInsuredSet, SysConst.DELETE_AND_INSERT);
    	map.put(saveLPInsuredSet, SysConst.DELETE_AND_INSERT);
        mResult.clear();
        mResult.add(map);

        return true;
    }



}
