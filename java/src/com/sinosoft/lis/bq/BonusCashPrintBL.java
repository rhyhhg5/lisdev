package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDBonusSumDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LOBonusPolDB;
import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDBonusSumSchema;
import com.sinosoft.lis.schema.LDRiskRateSchema;
import com.sinosoft.lis.schema.LOBonusPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LDBonusSumSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class BonusCashPrintBL  implements PrintService{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LCContSchema mLCContSchema = new LCContSchema();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
    private LOBonusPolSchema mLOBonusPolSchema= new LOBonusPolSchema();
    
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    TextTag textTag = new TextTag(); //新建一个TextTag的实例

    private ExeSQL mExeSQL=new ExeSQL();
    private String mOperate="";

    public BonusCashPrintBL() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("BonusCashPrintBL begin");
        mOperate=cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();
        
        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        
        if (!dealPrintMag()) {
            return false;
        }

        System.out.println("BonusCashPrintBL end");
        return true;
    }



    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                               getObjectByObjectName("LOPRTManagerSchema", 0);
        String tPolNo=ttLOPRTManagerSchema.getStandbyFlag2();
        String tFiscalYear = ttLOPRTManagerSchema.getStandbyFlag1();
        LOBonusPolDB tLOBonusPolDB = new LOBonusPolDB();
        tLOBonusPolDB.setPolNo(tPolNo);
        tLOBonusPolDB.setFiscalYear(tFiscalYear);
        if (!tLOBonusPolDB.getInfo()) {
            // @@错误处理
			System.out.println("BonusCashPrintBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "BonusCashPrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "数据为空--打印入口没有得到足够的信息！";
			mErrors.addOneError(tError);
			return false;
        }
        mLOBonusPolSchema.setSchema(tLOBonusPolDB.getSchema());
        if(mLOBonusPolSchema==null){
//        	 @@错误处理
			System.out.println("BonusCashPrintBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "BonusCashPrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "数据表为空--VTS打印入口没有得到足够的信息！";
			mErrors.addOneError(tError);
			return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private boolean getSchema() {
//    	获取投保人地址
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mLOBonusPolSchema.getContNo());
        if (!tLCAppntDB.getInfo()) {
            CError.buildErr(this,
                            "投保人表中缺少数据");
            return false;
        }
        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(tLCAppntDB.getAppntNo());
        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
        if(!tLCAddressDB.getInfo()){
        	// @@错误处理
			System.out.println("BonusCashPrintBL+getPrintData++--");
			CError tError = new CError();
			tError.moduleName = "BonusCashPrintBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "地址表缺少数据!";
			mErrors.addOneError(tError);
			return false;
        }
        mLCAddressSchema.setSchema(tLCAddressDB.getSchema());
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLOBonusPolSchema.getContNo());
        if(!tLCContDB.getInfo()){
        	// @@错误处理
			System.out.println("BonusCashPrintBL+getPrintData++--");
			CError tError = new CError();
			tError.moduleName = "BonusCashPrintBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "获取保单信息失败!";
			mErrors.addOneError(tError);
			return false;
        }
        mLCContSchema.setSchema(tLCContDB.getSchema());
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mLOBonusPolSchema.getPolNo());
        if(!tLCPolDB.getInfo()){
        	// @@错误处理
			System.out.println("BonusCashPrintBL+getPrintData++--");
			CError tError = new CError();
			tError.moduleName = "BonusCashPrintBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "获取保单信息失败!";
			mErrors.addOneError(tError);
			return false;
        }
        mLCPolSchema.setSchema(tLCPolDB.getSchema());
        return true;
    }

    private boolean getPrintData() {
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("BonusCashPrint.vts", "printer");
        
        textTag.add("JetFormType", "HL001");
        if ("batch".equals(mOperate)) {
            textTag.add("previewflag", "0");
        } else {
            textTag.add("previewflag", "1");
        }
        if(!getSchema()){
        	return false;
        }
        
        if(!mLCPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETMONEY)){
        	String isE =mExeSQL.getOneValue("select 1 from lppol where contno='"+mLCPolSchema.getContNo()+"' and edortype='DD' and BonusGetMode='"+BQ.BONUSGET_GETMONEY+"' order by makedate desc with ur");
         if(!(isE!=null && "1".equals(isE))){
        	// @@错误处理
			System.out.println("BonusInterestPrintBL+getPrintData++--");
			CError tError = new CError();
			tError.moduleName = "BonusInterestPrintBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "领取方式错误!";
			mErrors.addOneError(tError);
			return false;
		 }
        }
        LDBonusSumDB tLDBonusSumDB=new LDBonusSumDB();
        LDBonusSumSet tLDBonusSumSet=new LDBonusSumSet();
        LDBonusSumSchema tLDBonusSumSchema=new LDBonusSumSchema();
        tLDBonusSumSet=tLDBonusSumDB.executeQuery("select * from LDBonusSum where riskcode='"+mLCPolSchema.getRiskCode()+"' and BonusYear='"+mLOBonusPolSchema.getFiscalYear()+"'");
        if(tLDBonusSumSet.size()==0){
        	// @@错误处理
			System.out.println("BonusCashPrintBL+getPrintData++--");
			CError tError = new CError();
			tError.moduleName = "BonusCashPrintBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "获取分红信息失败!";
			mErrors.addOneError(tError);
			return false;
        }
        tLDBonusSumSchema=tLDBonusSumSet.get(1);
        
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            // @@错误处理
			System.out.println("BonusCashPrintBL+getPrintData++--");
			CError tError = new CError();
			tError.moduleName = "BonusCashPrintBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "操作员机构查询出错！";
			mErrors.addOneError(tError);
			return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);
        textTag.add("ManageComLength4", printcode);
        textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        textTag.add("Operator", mGlobalInput.Operator);

        String appntPhoneStr = " ";
        if (mLCAddressSchema.getPhone() != null &&
            !mLCAddressSchema.getPhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getPhone() + "、";
        }
        if (mLCAddressSchema.getHomePhone() != null &&
            !mLCAddressSchema.getHomePhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getHomePhone() + "、";
        }
        if (mLCAddressSchema.getCompanyPhone() != null &&
            !mLCAddressSchema.getCompanyPhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getCompanyPhone() + "、";
        }
        if (mLCAddressSchema.getMobile() != null &&
            !mLCAddressSchema.getMobile().equals("")) {
            appntPhoneStr += mLCAddressSchema.getMobile() + "、";
        }
        appntPhoneStr = appntPhoneStr.substring(0, appntPhoneStr.length() - 1);

//      页眉
        textTag.add("ZipCode", mLCAddressSchema.getZipCode());
        textTag.add("Address", mLCAddressSchema.getPostalAddress());
        textTag.add("AppntName", mLCContSchema.getAppntName());

//		正文        
//      AppntName已有
        SSRS tSSRS=new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
        String sql="select Year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  from  ldriskrate where  codetype='CFNL' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
        tSSRS=tExeSQL.execSQL(sql);
        textTag.add("Year1", tSSRS.GetText(1, 1));
        textTag.add("Quarter1", tSSRS.GetText(1, 2));
        textTag.add("Solvency", tSSRS.GetText(1, 3));
        textTag.add("Flag", tSSRS.GetText(1, 4));
        
        String sql2="select Year,quarter,riskrate  from  ldriskrate where  codetype='FXDJ' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
        tSSRS=tExeSQL.execSQL(sql2);
        textTag.add("Year2", tSSRS.GetText(1, 1));
        textTag.add("Quarter2", tSSRS.GetText(1, 2));
        textTag.add("RiksRate", tSSRS.GetText(1, 3));
        
        textTag.add("ContNo", mLCContSchema.getContNo());
        textTag.add("RiskName", mExeSQL.getOneValue("select riskname from lmrisk where riskcode='"+mLCPolSchema.getRiskCode()+"'"));
        textTag.add("CValiDate", mLCContSchema.getCValiDate());
//      AppntName已有
        textTag.add("InsuredName", mLCPolSchema.getInsuredName());
        textTag.add("PayMode", mExeSQL.getOneValue("select codename from ldcode where codetype='paymode' and code='"+mLCContSchema.getPayMode()+"'"));
        textTag.add("SumPolMoney",mExeSQL.getOneValue("select sum(sumduepaymoney) from ljapayperson where polno='"+mLCPolSchema.getPolNo()+"' and paytype='ZC' and confdate<='"+mLOBonusPolSchema.getAGetDate()+"'"));
        textTag.add("ShouldDate", CommonBL.decodeDate(mLOBonusPolSchema.getSGetDate()));
        
//      列表信息
        String tPolYear=String.valueOf(PubFun.calInterval(mLCPolSchema.getCValiDate(),mLOBonusPolSchema.getSGetDate(), "Y"));
        textTag.add("PolYear", tPolYear);
//      ShouldDate已有
        textTag.add("ActuDate", CommonBL.decodeDate(mLOBonusPolSchema.getAGetDate()));
        textTag.add("BonusMoney", mLOBonusPolSchema.getBonusMoney());
//      add by xp #1243 分红险迟发利息 20130717
        textTag.add("Interest", mLOBonusPolSchema.getBonusInterest());
        textTag.add("BeforeBonusMoney", mExeSQL.getOneValue("select nvl(sum(BonusMoney),0) from LOBonusPol where polno='"+mLOBonusPolSchema.getPolNo()+"' and FiscalYear<"+mLOBonusPolSchema.getFiscalYear()+" with ur"));
        
//      提示信息
//        String tHLYear=mExeSQL.getOneValue("select year('"+mLOBonusPolSchema.getSGetDate()+"') from dual");
        textTag.add("HLYear", mLOBonusPolSchema.getFiscalYear());
        textTag.add("HLAllSum", tLDBonusSumSchema.getHLAllSum());
        textTag.add("HLAppntSum", tLDBonusSumSchema.getHLAppntSum());        
    	String trate=mExeSQL.getOneValue("select nvl(b.hlinterestrate*100,0) from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+mLOBonusPolSchema.getPolNo()+"' " +
    				" and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+"-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+")))) ");
        textTag.add("InterestRate",trate);
    	

        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        //textTag.add("AgentCode", tLaAgentDB.getAgentCode());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("AgentPhone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
                           +
                " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                           +
                " (select agentgroup from laagent where agentcode ='"
                           + tLaAgentDB.getAgentCode() + "'))"
                           ;
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                branchSQL);
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());
        textTag.add("BarCode1", mLOBonusPolSchema.getContNo());
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        setFixedInfo();
        
        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }
        
        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlexport.addListTable(tEndTable, tEndTitle);
        
        xmlexport.outputDocumentToFile("F:\\", "HL001xp");
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    private void setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLCContSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
    }
    

    public static void main(String[] a) {
             GlobalInput tG = new GlobalInput();
             tG.Operator = "pa0001";
             tG.ComCode = "86";
             tG.ClientIP="127.0.0.1";
             LOPRTManagerSchema ttLOPRTManagerSchema=new LOPRTManagerSchema();
             ttLOPRTManagerSchema.setStandbyFlag2("21000616869");
             ttLOPRTManagerSchema.setStandbyFlag1("2012");
             VData tVData = new VData();
             tVData.addElement(tG);
             tVData.addElement(ttLOPRTManagerSchema);

             BonusCashPrintBL bl = new BonusCashPrintBL();
             if(bl.submitData(tVData, "PRINT"))
             {
            System.out.println(bl.mErrors.getErrContent());
             }
//        MMap tMap = new MMap();
//        VData tVData = new VData();
//        tVData.add(tMap);
//        PubSubmit tPubSubmit = new PubSubmit();
//        tPubSubmit.submitData(tVData, "");

    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        mLOPRTManagerSchema.setOtherNoType("00");
        mLOPRTManagerSchema.setCode("HL001");
        mLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mLOPRTManagerSchema.setStandbyFlag2(mLOBonusPolSchema.getPolNo()); //这里存放保单号
        mLOPRTManagerSchema.setStandbyFlag1(String.valueOf(mLOBonusPolSchema.getFiscalYear()));
        mResult.addElement(mLOPRTManagerSchema);
    	
        /*MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, "INSERT");
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }*/
        return true;
    }

}
