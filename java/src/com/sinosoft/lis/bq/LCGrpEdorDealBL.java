/*
 * <p>ClassName: OLCGrpEdorBL </p>
 * <p>Description: OLCGrpEdorBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-02-23 09:59:56
 */
package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCGrpEdorDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpEdorSchema;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LGWorkSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LCGrpEdorDealBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();
    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private String mGrpContNo;
    /** 业务处理相关变量 */
    private String mEdorType;
    /** 业务处理相关变量 */
    private LCGrpEdorSchema mLCGrpEdorSchema = new LCGrpEdorSchema();
    /** 业务处理相关变量 */
    private LCGrpEdorDB mLCGrpEdorDB = new LCGrpEdorDB();
    
    public LCGrpEdorDealBL()
    {
    	System.out.println("IN LCGrpEdorDealBL...");
    }

    public static void main(String[] args)
    {
        LCGrpEdorSchema schema = new LCGrpEdorSchema();

        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        schema.setGrpContNo("00003045000003");
        schema.setEdorType("RS");
        VData vData = new VData();
        vData.add(schema);
        vData.add(g);

        LCGrpEdorDealBL bl = new LCGrpEdorDealBL();
        if(!bl.submitData(vData, "INSERT||MAIN"))
        {
            System.out.println(bl.mErrors);
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLDDiseaseBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (mOperate.equals("INSERT||MAIN"))
        {
        	if(!checkInsert())
            {
                return false;
            }
//        	对于保单暂停来说 state:"0"表示未暂停满90天,"1"表示暂停已满90天
        	mLCGrpEdorSchema.setState("0");
//        	ValidFlag:"0"表示无效,"1"表示有效
        	mLCGrpEdorSchema.setValidFlag("1");
        	mLCGrpEdorSchema.setMakeDate(mCurrentDate);
            mLCGrpEdorSchema.setMakeTime(mCurrentTime);
            mLCGrpEdorSchema.setModifyDate(mCurrentDate);
            mLCGrpEdorSchema.setModifyTime(mCurrentTime);
            mLCGrpEdorSchema.setOperator(mGlobalInput.Operator);
            map.put(mLCGrpEdorSchema, "INSERT"); //插入
        }
        /*if (mOperate.equals("UPDATE||MAIN"))
        {
            if(!checkUpdate())
            {
                return false;
            }

            String sql = "Update LCGrpEdor set " +
                         "OwnerTypeNo = '" + mLCGrpEdorSchema.getOwnerTypeNo() +
                         "', " +
                         "OwnerNo = '" + mLCGrpEdorSchema.getOwnerNo() + "', " +
                         "Operator = '" + mGlobalInput.Operator + "', " +
                         "ModifyDate = '" + mCurrentDate + "', " +
                         "ModifyTime = '" + mCurrentTime + "' " +
                         "Where  WorkBoxNo = '" + mLCGrpEdorSchema.getWorkBoxNo() +
                         "' ";
            map.put(sql, "UPDATE"); //修改
        }*/
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            if(!checkDelete())
            {
                mErrors.addOneError("该团单不能被删除。");
                return false;
            }
            mLCGrpEdorDB.setGrpContNo(mGrpContNo);
            mLCGrpEdorDB.setEdorType(mEdorType);
            mLCGrpEdorDB.getInfo();
            mLCGrpEdorSchema=mLCGrpEdorDB.getSchema();
            map.put(mLCGrpEdorSchema, "DELETE"); //删除
        }

        return true;
    }

    private boolean checkInsert()
    {
    	if(mEdorType.equals("RS")){
        String sql = "  select * "
            + "from LCGrpPol a "
            + "where riskcode not in ('5601','1607') "
            + "    and  exists (select 1 from lcgrppol where grpcontno=a.grpcontno and riskcode  in ('5601','1607')) "
            + "    and grpcontno = '"+mGrpContNo + "' with ur "; 
        System.out.println(sql);
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.executeQuery(sql);
        if(tLCGrpPolSet.size() > 0)
        {
        	mErrors.addOneError("该保单下不包括'5601','1607'这两个险种或者保单下包含这两个险种之外的险种,不能进行配置");
        	return false;
        }
    	}
        return true;
    }

    private boolean checkDelete()
    {
        String sql = "  select a.* "
                     + "from LGWork a, LPGrpEdorItem b "
                     + "where a.workNo = b.edorno "
                     + "    and a.statusno != '8' "
                     + "    and b.edortype = 'RS' "
                     + "    and b.grpcontno = '"+mGrpContNo + "' with ur "; 
        System.out.println(sql);
        LGWorkDB tLGWorkDB = new LGWorkDB();
        LGWorkSet tLGWorkSet = tLGWorkDB.executeQuery(sql);
        if(tLGWorkSet.size() > 0)
        {
            mErrors.addOneError("此保单之前做过保单暂停的保全项目，不能删除");
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLCGrpEdorSchema.setSchema((LCGrpEdorSchema) cInputData.
                                        getObjectByObjectName("LCGrpEdorSchema",
                0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mGrpContNo=mLCGrpEdorSchema.getGrpContNo();
        this.mEdorType=mLCGrpEdorSchema.getEdorType();
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LCGrpEdorDB tLCGrpEdorDB = new LCGrpEdorDB();
        tLCGrpEdorDB.setSchema(this.mLCGrpEdorSchema);
        //如果有需要处理的错误，则返回
        if (tLCGrpEdorDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGrpEdorDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpEdorBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLCGrpEdorSchema);
            mInputData.add(this.map);
            mResult.clear();
            mResult.add(this.mLCGrpEdorSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpEdorBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
