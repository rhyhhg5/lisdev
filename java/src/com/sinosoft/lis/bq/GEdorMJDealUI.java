package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GEdorMJDealUI
{
    public CErrors mErrors = null;

    private GEdorMJDealBL mGEdorMJDealBL = new GEdorMJDealBL();

    public GEdorMJDealUI()
    {
    }

    /**
     * 操作提交方法
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String operate)
    {

        if(!mGEdorMJDealBL.submitData(cInputData, operate))
        {
            mErrors = mGEdorMJDealBL.mErrors;
            return false;
        }

        return true;
    }

    /**
     * 是否需要上级审批的标志
     * @return boolean:true需要，false不需要
     */
    public boolean needConfirm()
    {
        return mGEdorMJDealBL.needConfirm();
    }

    public static void main(String[] args)
    {
        GEdorMJDealUI gedormjdealui = new GEdorMJDealUI();
    }
}
