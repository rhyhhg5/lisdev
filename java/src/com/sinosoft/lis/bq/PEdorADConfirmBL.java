package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong
 * @version 1.0
 */
public class PEdorADConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
	private MMap mMap = new MMap();

    private LPEdorItemSchema mLPEdorItemSchema = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();


    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //数据准备操作（preparedata())
        if (!prepareData())
        {
            return false;
        }

        return true;
    }
    /**
     * 被保人联系方式变更  li caiyan
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitInsuData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //数据准备操作（preparedata())
        if (!prepareInsuData())
        {
            return false;
        }

        return true;
    }



    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        if (mLPEdorItemSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError(new CError("传入数据不完全！"));
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        tLPEdorItemDB.setPolNo(mLPEdorItemSchema.getPolNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
        {
            mErrors.addOneError(new CError("查询批改项目信息失败！"));
            return false;
        }
        mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));
        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
		LCAppntSet saveLCAppntSet = new LCAppntSet();
		LCAddressSet saveLCAddressSet = new LCAddressSet();

		LPAppntSet saveLPAppntSet = new LPAppntSet();
		LPAddressSet saveLPAddressSet = new LPAddressSet();

		Reflections tRef = new Reflections();

		//得到当前保单的投保人资料
		LCAppntSchema aLCAppntSchema = new LCAppntSchema();
		LPAppntSchema aLPAppntSchema = new LPAppntSchema();
		LCAppntSet tLCAppntSet = new LCAppntSet();
		LPAppntSet tLPAppntSet = new LPAppntSet();

		LPAppntSchema tLPAppntSchema = new LPAppntSchema();
		tLPAppntSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPAppntSchema.setEdorType(mLPEdorItemSchema.getEdorType());
//		tLPAppntSchema.setAppntNo(mLPEdorItemSchema.getInsuredNo());

		LPAppntDB tLPAppntDB = new LPAppntDB();
		tLPAppntDB.setSchema(tLPAppntSchema);
		tLPAppntSet = tLPAppntDB.query();
		for (int j = 1; j <= tLPAppntSet.size(); j++)
		{
			aLPAppntSchema = tLPAppntSet.get(j);
			LCAppntSchema tLCAppntSchema = new LCAppntSchema();
			tLPAppntSchema = new LPAppntSchema();

			LCAppntDB aLCAppntDB = new LCAppntDB();
			aLCAppntDB.setContNo(aLPAppntSchema.getContNo());
			aLCAppntDB.setAppntNo(aLPAppntSchema.getAppntNo());
			if (!aLCAppntDB.getInfo())
			{
				mErrors.copyAllErrors(aLCAppntDB.mErrors);
				mErrors.addOneError(new CError("查询投保人信息失败！"));
				return false;
			}
			aLCAppntSchema = aLCAppntDB.getSchema();
			tRef.transFields(tLPAppntSchema, aLCAppntSchema);
			tLPAppntSchema.setEdorNo(aLPAppntSchema.getEdorNo());
			tLPAppntSchema.setEdorType(aLPAppntSchema.getEdorType());

			//转换成保单个人信息。
			tRef.transFields(tLCAppntSchema, aLPAppntSchema);
			tLCAppntSchema.setModifyDate(PubFun.getCurrentDate());
			tLCAppntSchema.setModifyTime(PubFun.getCurrentTime());

			saveLPAppntSet.add(tLPAppntSchema);
			tLCAppntSet.add(tLCAppntSchema);
			saveLCAppntSet.add(tLCAppntSchema);
		}
		
		
		//当保单的投保人与被保险人为同一客户时,也对被保险人信息进行处理
		LCInsuredSet saveLCInsuredSet=new LCInsuredSet();
		LPInsuredSet saveLPInsuredSet=new LPInsuredSet();
		
		LCInsuredSchema aLCInsuredSchema=new LCInsuredSchema();
		LPInsuredSchema aLPInsuredSchema=new LPInsuredSchema();
		LPInsuredSet tLPInsuredSet=new LPInsuredSet();
		
		LPInsuredSchema tLPInsuredSchema=new LPInsuredSchema();
		tLPInsuredSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPInsuredSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		if(!BQ.FILLDATA.equals(mLPEdorItemSchema.getInsuredNo())){//兼容简易保全
			tLPInsuredSchema.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
		}
		LPInsuredDB tLPInsuredDB=new LPInsuredDB();
		tLPInsuredDB.setSchema(tLPInsuredSchema);
		tLPInsuredSet=tLPInsuredDB.query();
		
		for(int i=1;i<= tLPInsuredSet.size();i++){
			
			aLPInsuredSchema=tLPInsuredSet.get(i);
			LCInsuredSchema tLCInsuredSchema=new LCInsuredSchema();
			
			LCInsuredDB aLCInsuredDB=new LCInsuredDB();
			aLCInsuredDB.setContNo(aLPInsuredSchema.getContNo());
			aLCInsuredDB.setInsuredNo(aLPInsuredSchema.getInsuredNo());
			if (!aLCInsuredDB.getInfo())
			{
				mErrors.copyAllErrors(aLCInsuredDB.mErrors);
				mErrors.addOneError(new CError("查询被保人信息失败！"));
				return false;
			}
			aLCInsuredSchema=aLCInsuredDB.getSchema();
			tRef.transFields(tLPInsuredSchema, aLCInsuredSchema);
			tLPInsuredSchema.setEdorNo(aLPInsuredSchema.getEdorNo());
			tLPInsuredSchema.setEdorType(aLPInsuredSchema.getEdorType());
			
			tRef.transFields(tLCInsuredSchema,aLPInsuredSchema);
			tLCInsuredSchema.setModifyDate(PubFun.getCurrentDate());
			tLCInsuredSchema.setModifyTime(PubFun.getCurrentTime());
			
			saveLCInsuredSet.add(tLCInsuredSchema);
			saveLPInsuredSet.add(tLPInsuredSchema);
			
		}
		

		//得到当前保单的投保人资料
		boolean tNewAdd = false;
		LCAddressSchema aLCAddressSchema = new LCAddressSchema();
		LPAddressSchema aLPAddressSchema = new LPAddressSchema();
		LCAddressSet tLCAddressSet = new LCAddressSet();
		LPAddressSet tLPAddressSet = new LPAddressSet();

		LPAddressSchema tLPAddressSchema = new LPAddressSchema();
		tLPAddressSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPAddressSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		if(!BQ.FILLDATA.equals(mLPEdorItemSchema.getInsuredNo())){//兼容简易保全
			tLPAddressSchema.setCustomerNo(mLPEdorItemSchema.getInsuredNo());
		}else{
			tLPAddressSchema.setCustomerNo(tLPAppntSchema.getAppntNo());
		}
		LPAddressDB tLPAddressDB = new LPAddressDB();
		tLPAddressDB.setSchema(tLPAddressSchema);
		tLPAddressSet = tLPAddressDB.query();
		for (int j = 1; j <= tLPAddressSet.size(); j++)
		{
			aLPAddressSchema = tLPAddressSet.get(j);
			LCAddressSchema tLCAddressSchema = new LCAddressSchema();
			tLPAddressSchema = new LPAddressSchema();

			LCAddressDB aLCAddressDB = new LCAddressDB();
			aLCAddressDB.setCustomerNo(aLPAddressSchema.getCustomerNo());
			aLCAddressDB.setAddressNo(aLPAddressSchema.getAddressNo());
            if (!aLCAddressDB.getInfo())
            {
                if (aLCAddressDB.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(aLCAddressDB.mErrors);
                    mErrors.addOneError(new CError("查询投保人地址信息失败！"));
                    return false;
                }
                tNewAdd = true;
                tRef.transFields(tLCAddressSchema, aLPAddressSchema);
                tLCAddressSchema.setModifyDate(PubFun.getCurrentDate());
                tLCAddressSchema.setModifyTime(PubFun.getCurrentTime());
                tLCAddressSet.add(tLCAddressSchema);
                saveLCAddressSet.add(tLCAddressSchema);
            }
            else
            {
                tNewAdd = false;
                aLCAddressSchema = aLCAddressDB.getSchema();
                tRef.transFields(tLPAddressSchema, aLCAddressSchema);
                tLPAddressSchema.setEdorNo(aLPAddressSchema.getEdorNo());
                tLPAddressSchema.setEdorType(aLPAddressSchema.
                                             getEdorType());

                //转换成保单个人信息。
                tRef.transFields(tLCAddressSchema, aLPAddressSchema);
                tLCAddressSchema.setModifyDate(PubFun.getCurrentDate());
                tLCAddressSchema.setModifyTime(PubFun.getCurrentTime());

                saveLPAddressSet.add(tLPAddressSchema);
                tLCAddressSet.add(tLCAddressSchema);
                saveLCAddressSet.add(tLCAddressSchema);
            }
        }

		//得到当前LPEdorItem保单信息主表的状态，并更新状态为申请确认。
		mLPEdorItemSchema.setEdorState("0");

		mMap.put(mLPEdorItemSchema, "UPDATE");
		mMap.put(saveLCAppntSet, "UPDATE");
		mMap.put(saveLPAppntSet, "UPDATE");
		mMap.put(saveLCInsuredSet, "UPDATE");
		mMap.put(saveLPInsuredSet, "UPDATE");
		
		if (tNewAdd)
		{
			mMap.put(saveLCAddressSet, "INSERT");
		}
		else
		{
			mMap.put(saveLCAddressSet, "UPDATE");
			mMap.put(saveLPAddressSet, "UPDATE");
		}
                mResult.add(mMap);
		return true;
    }
    
    
    /**
     * 被保人联系方式变更 li caiyan
     * 
     */
    private boolean prepareInsuData()
    {
		LCInsuredSet saveLCInsuredSet = new LCInsuredSet();
		LCAddressSet saveLCAddressSet = new LCAddressSet();

		LPInsuredSet saveLPInsuredSet = new LPInsuredSet();
		LPAddressSet saveLPAddressSet = new LPAddressSet();

		Reflections tRef = new Reflections();

		//得到当前保单的投保人资料
		LCInsuredSchema aLCInsuredSchema = new LCInsuredSchema();
		LPInsuredSchema aLPInsuredSchema = new LPInsuredSchema();
		LCInsuredSet tLCInsuredSet = new LCInsuredSet();
		LPInsuredSet tLPInsuredSet = new LPInsuredSet();

		LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
		tLPInsuredSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPInsuredSchema.setEdorType(mLPEdorItemSchema.getEdorType());

		LPInsuredDB tLPInsuredDB = new LPInsuredDB();
		tLPInsuredDB.setSchema(tLPInsuredSchema);
		tLPInsuredSet = tLPInsuredDB.query();
		for (int j = 1; j <= tLPInsuredSet.size(); j++)
		{
			aLPInsuredSchema = tLPInsuredSet.get(j);
			LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
			tLPInsuredSchema = new LPInsuredSchema();

			LCInsuredDB aLCInsuredDB = new LCInsuredDB();
			aLCInsuredDB.setContNo(aLPInsuredSchema.getContNo());
			aLCInsuredDB.setInsuredNo(aLPInsuredSchema.getInsuredNo());
			if (!aLCInsuredDB.getInfo())
			{
				mErrors.copyAllErrors(aLCInsuredDB.mErrors);
				mErrors.addOneError(new CError("查询被保人信息失败！"));
				return false;
			}
			aLCInsuredSchema = aLCInsuredDB.getSchema();
			tRef.transFields(tLPInsuredSchema, aLCInsuredSchema);
			tLPInsuredSchema.setEdorNo(aLPInsuredSchema.getEdorNo());
			tLPInsuredSchema.setEdorType(aLPInsuredSchema.getEdorType());

			//转换成保单个人信息。
			tRef.transFields(tLCInsuredSchema, aLPInsuredSchema);
			tLCInsuredSchema.setModifyDate(PubFun.getCurrentDate());
			tLCInsuredSchema.setModifyTime(PubFun.getCurrentTime());

			saveLPInsuredSet.add(tLPInsuredSchema);
			tLCInsuredSet.add(tLCInsuredSchema);
			saveLCInsuredSet.add(tLCInsuredSchema);
		}

		//得到当前保单的投保人资料
		boolean tNewAdd = false;
		LCAddressSchema aLCAddressSchema = new LCAddressSchema();
		LPAddressSchema aLPAddressSchema = new LPAddressSchema();
		LCAddressSet tLCAddressSet = new LCAddressSet();
		LPAddressSet tLPAddressSet = new LPAddressSet();

		LPAddressSchema tLPAddressSchema = new LPAddressSchema();
		tLPAddressSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPAddressSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPAddressSchema.setCustomerNo(tLPInsuredSchema.getInsuredNo());

		LPAddressDB tLPAddressDB = new LPAddressDB();
		tLPAddressDB.setSchema(tLPAddressSchema);
		tLPAddressSet = tLPAddressDB.query();
		for (int j = 1; j <= tLPAddressSet.size(); j++)
		{
			aLPAddressSchema = tLPAddressSet.get(j);
			LCAddressSchema tLCAddressSchema = new LCAddressSchema();
			tLPAddressSchema = new LPAddressSchema();

			LCAddressDB aLCAddressDB = new LCAddressDB();
			aLCAddressDB.setCustomerNo(aLPAddressSchema.getCustomerNo());
			aLCAddressDB.setAddressNo(aLPAddressSchema.getAddressNo());
            if (!aLCAddressDB.getInfo())
            {
                if (aLCAddressDB.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(aLCAddressDB.mErrors);
                    mErrors.addOneError(new CError("查询被保人地址信息失败！"));
                    return false;
                }
                tNewAdd = true;
                tRef.transFields(tLCAddressSchema, aLPAddressSchema);
                tLCAddressSchema.setModifyDate(PubFun.getCurrentDate());
                tLCAddressSchema.setModifyTime(PubFun.getCurrentTime());
                tLCAddressSet.add(tLCAddressSchema);
                saveLCAddressSet.add(tLCAddressSchema);
            }
            else
            {
                tNewAdd = false;
                aLCAddressSchema = aLCAddressDB.getSchema();
                tRef.transFields(tLPAddressSchema, aLCAddressSchema);
                tLPAddressSchema.setEdorNo(aLPAddressSchema.getEdorNo());
                tLPAddressSchema.setEdorType(aLPAddressSchema.
                                             getEdorType());

                //转换成保单个人信息。
                tRef.transFields(tLCAddressSchema, aLPAddressSchema);
                tLCAddressSchema.setModifyDate(PubFun.getCurrentDate());
                tLCAddressSchema.setModifyTime(PubFun.getCurrentTime());

                saveLPAddressSet.add(tLPAddressSchema);
                tLCAddressSet.add(tLCAddressSchema);
                saveLCAddressSet.add(tLCAddressSchema);
            }
        }

		//得到当前LPEdorItem保单信息主表的状态，并更新状态为申请确认。
		mLPEdorItemSchema.setEdorState("0");

		mMap.put(mLPEdorItemSchema, "UPDATE");
		mMap.put(saveLCInsuredSet, "UPDATE");
		mMap.put(saveLPInsuredSet, "UPDATE");
		if (tNewAdd)
		{
			mMap.put(saveLCAddressSet, "INSERT");
		}
		else
		{
			mMap.put(saveLCAddressSet, "UPDATE");
			mMap.put(saveLPAddressSet, "UPDATE");
		}
                mResult.add(mMap);
		return true;
    }
}
