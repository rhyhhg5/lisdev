package com.sinosoft.lis.bq;

import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 客户资料变更</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong,Luomin
 * @author rewrite by QiuYang 2005
 * @version 1.0
 */

public class GrpEdorRSConfirmBL implements EdorConfirm
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private DetailDataQuery mQuery = null;

    private GlobalInput mGlobalInput = null;

    private String mTypeFlag = null;

    private String stateFlag = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;
    
    private LCGrpContStateSchema mLCGrpContStateSchema = new LCGrpContStateSchema();
    
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    
    private ValidateEdorData2 mValidateEdorData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData cInputData,String a)
    {
    	System.out.println("Begin --------PEdorRSConfirmBL");
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())//检验数据未处理
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到传入数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
        	mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.
                    getObjectByObjectName("LPGrpEdorItemSchema", 0);
            mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
            mEdorType = mLPGrpEdorItemSchema.getEdorType();
            mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    /*
     * 
     * */
    private String getOccupationType(String cOccupationCode){
    	String sql="select OccupationType from LDOccupation where '1218610553000'='1218610553000' and  OccupationCode = '"+cOccupationCode+"'  fetch first 2000 rows only ";
    	ExeSQL tExeSQL = new ExeSQL();     
    	String tOccupationType= tExeSQL.getOneValue(sql);
    	return tOccupationType;
    }
    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        /*if ((mCustomerNo == null) || (mCustomerNo.equals("")))
        {
            mErrors.addOneError("客户号为空！");
            return false;
        } */
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {

    	LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(mEdorNo);
        tLPEdorEspecialDataDB.setEdorType(mEdorType);
        tLPEdorEspecialDataDB.setDetailType("STOPDATE");
        LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
        mLPEdorEspecialDataSchema = tLPEdorEspecialDataDB.query().get(1);
    	
    	mLCGrpContStateSchema.setGrpPolNo("000000");
    	mLCGrpContStateSchema.setContPlanCode("00");
    	mLCGrpContStateSchema.setStartDate(mLPEdorEspecialDataSchema.getEdorValue());
    	mLCGrpContStateSchema.setGrpContNo(mGrpContNo);
    	mLCGrpContStateSchema.setOtherNoType(mEdorType);
    	mLCGrpContStateSchema.setOtherNo(mEdorNo);
    	//保单暂停的satettype为Stop
    	mLCGrpContStateSchema.setStateType("Stop");
    	mLCGrpContStateSchema.setState("1");
    	mLCGrpContStateSchema.setStateReason("01");
    	mLCGrpContStateSchema.setOperator(mGlobalInput.Operator);
    	mLCGrpContStateSchema.setMakeDate(mCurrentDate);
    	mLCGrpContStateSchema.setMakeTime(mCurrentTime);
    	mLCGrpContStateSchema.setModifyDate(mCurrentDate);
    	mLCGrpContStateSchema.setModifyTime(mCurrentTime);
    	
    	mMap.put(mLCGrpContStateSchema, "INSERT");
        return true;
    }
    
    /**
     * 返回计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }
    
    
/*    *//**
     * 校验是否为激活卡
     * @return boolean
     *//*
    private boolean IsCard(){
    	ExeSQL texesql=new ExeSQL();
    	String tcard=texesql.getOneValue("select 1 from lccont where  contno='"+mContNo+"' and exists (select 1 from lcgrpcont where grpcontno=lccont.grpcontno and cardflag='2') and exists (select 1 from licertify where prtno=lccont.prtno and activeflag='02') ");
    	if ((tcard == null) || (tcard.equals(""))) {
			return false;
		}else
		{
    	return true;}
    }*/
   
    
    private void setLPEdoreSpecialData(String ZiDuanName,String value)
    {
    	LPEdorEspecialDataSchema tLPEdorEspecialDataSchema=new LPEdorEspecialDataSchema();
    	tLPEdorEspecialDataSchema.setEdorNo(mEdorNo);
    	tLPEdorEspecialDataSchema.setEdorType(mEdorType);
    	tLPEdorEspecialDataSchema.setPolNo("000000");
    	tLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorNo);
    	tLPEdorEspecialDataSchema.setDetailType(ZiDuanName);
    	tLPEdorEspecialDataSchema.setEdorValue(value);
    	mMap.put(tLPEdorEspecialDataSchema, "DELETE&INSERT");
    }
  

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
