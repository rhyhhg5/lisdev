package com.sinosoft.lis.bq;
//程序名称：LDBonusInterestRateBL.java
//程序功能：
//创建日期：2011-11-23
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.util.*;

public class LDBonusInterestRateBL 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	
	private VData mResult = new VData();
	
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	private MMap map = new MMap();
	
	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	
	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput() ;
	
	/** 数据操作字符串 */
	private String mOperate ;
	
	/** 操作员 */
	private String mOperater ;
	
	/** 业务处理相关变量 */
	private LDBonusInterestRateSchema mLDBonusInterestRateSchema = new LDBonusInterestRateSchema();
	
	/** 结算月份 */
//	private String mBalaMonth = null;
	
	/** 需要删除的业务处理相关变量 */
	private LDBonusInterestRateSchema mDLDBonusInterestRateSchema = new LDBonusInterestRateSchema();
	
	/** 需要删除的结算月份 */
	private String mDBalaMonth = null;
	
	public LDBonusInterestRateBL() 
	{
	}
	
	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
  public boolean submitData(VData cInputData,String cOperate)
	{
      //将操作数据拷贝到本类中
	    this.mOperate = cOperate;
	    if (!getInputData(cInputData))
	    {
	    	CError tError = new CError();
          tError.moduleName = "LMInsuAccRateBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "数据处理失败LMInsuAccRateBL-->getInputData!";
          this.mErrors.addOneError(tError);
	        return false;
	    }
	    
      //数据校验
      if (!checkData()) {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LDBonusInterestRateBL";
          tError.functionName = "checkData";
          tError.errorMessage = "数据处理失败LDBonusInterestRateBL-->checkData!";
          this.mErrors.addOneError(tError);
          return false;
      }
	    
	    //进行业务处理
	    if (!dealData())
	    {
	        // @@错误处理
          CError tError = new CError();
	        tError.moduleName = "LDBonusInterestRateBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败LDBonusInterestRateBL-->dealData!";
	        this.mErrors .addOneError(tError) ;
	        return false;
	    }

	    if (!prepareOutputData())
	    {
	        return false;
	    }
	    
	    PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "LDBonusInterestRateBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";

          this.mErrors.addOneError(tError);
          return false;
      }
	    
	    mInputData = null;
	    return true;
	}
	 /**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean dealData()
	{
		System.out.println("进行数据处理.....");
	    if (mOperate.equals("INSERT||MAIN"))
	    {
	    	return insertData();
	    }
	    if (mOperate.equals("UPDATE||MAIN"))
        {
           return updateData();
        }
	    if (this.mOperate.equals("DELETE||MAIN"))
	    {
	        return deleteData(); //删除
	    }
	    return true;
	}
	
	 /**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
	    this.mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0);
	    this.mOperater = this.mGlobalInput.Operator;
	    
	    TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
	    this.mLDBonusInterestRateSchema = (LDBonusInterestRateSchema)tTransferData.getValueByName("tLDBonusInterestRateSchema");
		this.mDLDBonusInterestRateSchema = (LDBonusInterestRateSchema)tTransferData.getValueByName("dLDBonusInterestRateSchema");
		return true;
	}
	 
  private boolean prepareOutputData()
  {
      try
      {
          this.mInputData.clear();
          this.mInputData.add(this.mLDBonusInterestRateSchema);
          this.mInputData.add(this.mDLDBonusInterestRateSchema);
          mInputData.add(this.map);
          mResult.clear();
          mResult.add(this.mLDBonusInterestRateSchema);
          mResult.add(this.mDLDBonusInterestRateSchema);
      }
		catch(Exception ex)
		{
	 		// @@错误处理
			CError tError = new CError();
	 		tError.moduleName = "LDBonusInterestRateBL";
	 		tError.functionName = "prepareOutputData";
	 		tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
	 		this.mErrors .addOneError(tError) ;
			return false;
		}
		return true;
  }
  
  /**
   * checkData
   *
   * @return boolean
   */
  private boolean checkData() 
  {
      System.out.println("进行校验.....");
      if (mLDBonusInterestRateSchema == null) {
      	System.out.println("mLDBonusInterestRateSchema == null");
          return false;
      }
      if (mDLDBonusInterestRateSchema==null) {
        	System.out.println("mLDBonusInterestRateSchema == null");
            return false;
        }
      if (mGlobalInput == null) {
      	System.out.println("mGlobalInput == null");
          return false;
      }
      return true;
  }
  
  //更新数据
  private boolean updateData(){
	  String riskCode = "";
      String bonusYear = "";
      String appYear = "";
  	
      riskCode = this.mLDBonusInterestRateSchema.getRiskCode();
      bonusYear = this.mLDBonusInterestRateSchema.getBonusYear();
      appYear = this.mLDBonusInterestRateSchema.getAppYear();
     
      LDBonusInterestRateSchema tLDBonusInterestRateSchema = new LDBonusInterestRateSchema();
      tLDBonusInterestRateSchema.setRiskCode(riskCode);
      tLDBonusInterestRateSchema.setBonusYear(bonusYear);
      tLDBonusInterestRateSchema.setAppYear(appYear);
      tLDBonusInterestRateSchema.setHLInterestRate(this.mLDBonusInterestRateSchema.getHLInterestRate());
      tLDBonusInterestRateSchema.setModifyDate(mCurrentDate);
      tLDBonusInterestRateSchema.setModifyTime(mCurrentTime);
      tLDBonusInterestRateSchema.setOperator(mGlobalInput.Operator);
      
      LDBonusInterestRateDB tLDBonusInterestRateDB=new LDBonusInterestRateDB();
      tLDBonusInterestRateDB.setSchema(tLDBonusInterestRateSchema);
      if(!tLDBonusInterestRateDB.getInfo()){
    	  // @@错误处理
		System.out.println("LDBonusInterestRateBL+updateData++--");
		CError tError = new CError();
		tError.moduleName = "LDBonusInterestRateBL";
		tError.functionName = "updateData";
		tError.errorMessage = "查询记录失败!";
		mErrors.addOneError(tError);
		return false;
      }
      tLDBonusInterestRateSchema.setMakeDate(tLDBonusInterestRateDB.getMakeDate());
      tLDBonusInterestRateSchema.setMakeTime(tLDBonusInterestRateDB.getMakeTime());      
      
      map.put(tLDBonusInterestRateSchema, "UPDATE");
      
	  return true; 
  }
  //插入数据
  private boolean insertData()
  {
	  for(int i=Integer.parseInt(mLDBonusInterestRateSchema.getAppYear());i<=Integer.parseInt(mDLDBonusInterestRateSchema.getAppYear());i++){
	LDBonusInterestRateSchema tLDBonusInterestRateSchema = new LDBonusInterestRateSchema();
	tLDBonusInterestRateSchema.setRiskCode(mLDBonusInterestRateSchema.getRiskCode());
  	System.out.println("riskcode='''''''"+mLDBonusInterestRateSchema.getRiskCode());
  	tLDBonusInterestRateSchema.setBonusYear(mLDBonusInterestRateSchema.getBonusYear());
  	tLDBonusInterestRateSchema.setAppYear(String.valueOf(i));
  	tLDBonusInterestRateSchema.setHLInterestRate(mLDBonusInterestRateSchema.getHLInterestRate());
  	tLDBonusInterestRateSchema.setMakeDate(mCurrentDate);
  	tLDBonusInterestRateSchema.setMakeTime(mCurrentTime);
  	tLDBonusInterestRateSchema.setModifyDate(mCurrentDate);
  	tLDBonusInterestRateSchema.setModifyTime(mCurrentTime);
  	tLDBonusInterestRateSchema.setOperator(mGlobalInput.Operator);
  	LDBonusInterestRateDB tLDBonusInterestRateDB=new LDBonusInterestRateDB();
  	tLDBonusInterestRateDB.setSchema(tLDBonusInterestRateSchema);
  	if(tLDBonusInterestRateDB.getInfo()){
  		// @@错误处理
		System.out.println("LDBonusInterestRateBL+insertData++--");
		CError tError = new CError();
		tError.moduleName = "LDBonusInterestRateBL";
		tError.functionName = "insertData";
		tError.errorMessage = "已经存在相同的记录,请查询后重新录入!";
		mErrors.addOneError(tError);
		return false;
  	}
  	PubFun.fillDefaultField(mLDBonusInterestRateSchema);
      map.put(tLDBonusInterestRateSchema, "INSERT");
	  }
      
  	return true;
  }
  
  //删除数据
  private boolean deleteData()
  {
	  System.out.println(mLDBonusInterestRateSchema.getAppYear());
	  System.out.println(mLDBonusInterestRateSchema.getBonusYear());
	  System.out.println(mLDBonusInterestRateSchema.getRiskCode());
	  System.out.println(mLDBonusInterestRateSchema.getHLInterestRate());
	  map.put(mLDBonusInterestRateSchema, "DELETE");
  	return true;
  }
  public VData getResult()
  {
      return this.mResult;
	}
}
