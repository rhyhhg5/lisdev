package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 受益人变更
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Tjj
 * @ReWrite ZhangRong
 * @author QiuYang
 * @version 1.0
 */
public class PEdorBCConfirmBL implements EdorConfirm {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGlobalInput = null;

	private MMap mMap = new MMap();

	private String mEdorNo = null;

	private String mEdorType = null;

	private String mContNo = null;

	private String mInsuredNo = null;

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/**
	 * 返回结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		VData data = new VData();
		data.add(mMap);
		return data;
	}

	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		LPEdorItemSchema edorItem = (LPEdorItemSchema) cInputData
				.getObjectByObjectName("LPEdorItemSchema", 0);
		mEdorNo = edorItem.getEdorNo();
		mEdorType = edorItem.getEdorType();
		mContNo = edorItem.getContNo();
		mInsuredNo = edorItem.getInsuredNo();
		return true;
	}

	/**
	 * 准备需要保存的数据
	 * 
	 * @return boolean
	 */
	private boolean dealData()
    {
        LPBnfDB tLPBnfDB = new LPBnfDB();
        tLPBnfDB.setEdorNo(mEdorNo);
        tLPBnfDB.setEdorType(mEdorType);
        tLPBnfDB.setContNo(mContNo);
        tLPBnfDB.setInsuredNo(mInsuredNo);
        LPBnfSet tLPBnfSet = tLPBnfDB.query();
        
        LCBnfDB tLCBnfDB = new LCBnfDB();
        tLCBnfDB.setContNo(mContNo);
        tLCBnfDB.setInsuredNo(mInsuredNo);
        LCBnfSet tLCBnfSet = tLCBnfDB.query();

        String sql = "delete from LCBnf " +
                     "where ContNo = '" + mContNo + "' " +
                     "and InsuredNo = '" + mInsuredNo + "' ";
        
        mMap.put(sql, "DELETE");

        for (int i = 1; i <= tLPBnfSet.size(); i++)
        {   
        	LCBnfSchema tLCBnfSchema = new LCBnfSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLCBnfSchema, tLPBnfSet.get(i));
            tLCBnfSchema.setOperator(mGlobalInput.Operator);
            tLCBnfSchema.setModifyDate(mCurrentDate);
            tLCBnfSchema.setModifyTime(mCurrentTime);
            mMap.put(tLCBnfSchema, "DELETE&INSERT");
        }

        sql = "delete from LPBnf " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "and ContNo = '" + mContNo + "' " +
                "and InsuredNo = '" + mInsuredNo + "' ";
        mMap.put(sql, "DELETE");

        for (int i = 1; i <= tLCBnfSet.size(); i++)
        {
            LPBnfSchema tLPBnfSchema = new LPBnfSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLPBnfSchema, tLCBnfSet.get(i));
            tLPBnfSchema.setEdorNo(mEdorNo);
            tLPBnfSchema.setEdorType(mEdorType);
            tLPBnfSchema.setOperator(mGlobalInput.Operator);
            tLPBnfSchema.setModifyDate(mCurrentDate);
            tLPBnfSchema.setModifyTime(mCurrentTime);
          
            mMap.put(tLPBnfSchema, "DELETE&INSERT");
        }
        return true;
    }
}
