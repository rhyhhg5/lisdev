package com.sinosoft.lis.bq;

	import java.util.Date;
	import java.util.GregorianCalendar;

	import com.sinosoft.lis.db.*;
	import com.sinosoft.lis.pubfun.*;
	import com.sinosoft.lis.schema.*;
	import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

	/**
	 * <p>
	 * Title: Web业务系统
	 * </p>
	 * <p>
	 * Description: 客户资料变更
	 * </p>
	 * <p>
	 * Copyright: Copyright (c) 2002
	 * </p>
	 * <p>
	 * Company: Sinosoft
	 * </p>
	 * 
	 * @author Tjj
	 * @ReWrite ZhangRong,Luomin
	 * @author rewrite by QiuYang 2005
	 * @version 1.0
	 */

	public class GrpEdorGBConfirmBL implements EdorConfirm {
		/** 错误处理类 */
		public CErrors mErrors = new CErrors();

		private MMap mMap = new MMap();

		private DetailDataQuery mQuery = null;

		private GlobalInput mGlobalInput = null;

		private String mTypeFlag = null;

		private String stateFlag = null;

		private String mEdorNo = null;

		private String mEdorType = null;

		private String mStopDate = null;
		
		private String mResumeDate = null;

		private String mGrpContNo = null;

		private LCGrpContStateSchema mLCGrpContStateSchema = new LCGrpContStateSchema();
		
		private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();

		private String mCurrentDate = PubFun.getCurrentDate();

		private String mCurrentTime = PubFun.getCurrentTime();
		
//		互换的数据
	    private LCGrpPositionSet mLCGrpPositionSet = new LCGrpPositionSet();
	    private LPGrpPositionSet mLPGrpPositionSet = new LPGrpPositionSet();
	    private LCAscriptionRuleParamsSet mLCAscriptionRuleParamsSet = new LCAscriptionRuleParamsSet();
	    private LPAscriptionRuleParamsSet mLPAscriptionRuleParamsSet = new LPAscriptionRuleParamsSet();
	    private LCAscriptionRuleFactorySet mLCAscriptionRuleFactorySet = new LCAscriptionRuleFactorySet();
	    private LPAscriptionRuleFactorySet mLPAscriptionRuleFactorySet = new LPAscriptionRuleFactorySet();

		/**
		 * 提交数据
		 * 
		 * @param cInputData
		 *            VData
		 * @return boolean
		 */
		public boolean submitData(VData cInputData ,String a) {
			System.out.println("Begin --------GrpEdorGBConfirmlBL.java");
			if (!getInputData(cInputData)) {
				return false;
			}

			if (!checkData())// 检验数据未处理
			{
				return false;
			}

			if (!dealData()) {
				return false;
			}
			return true;
		}

	    /**
	     * 返回计算结果
	     * @return VData
	     */
	    public VData getResult()
	    {
	        VData data = new VData();
	        data.add(mMap);
	        return data;
	    }
	    
	    
		/**
		 * 得到传入数据
		 * 
		 * @param cInputData
		 *            VData
		 * @return boolean
		 */
		private boolean getInputData(VData cInputData) {
			try {
		        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
		                "GlobalInput", 0);
		        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.
		                getObjectByObjectName("LPGrpEdorItemSchema", 0);
		        mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
		        mEdorType = mLPGrpEdorItemSchema.getEdorType();
		        mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
			} catch (Exception ex) {
				ex.printStackTrace();
				return false;
			}
			return true;
		}

		/*
	     * 
	     * */
		private String getOccupationType(String cOccupationCode) {
			String sql = "select OccupationType from LDOccupation where '1218610553000'='1218610553000' and  OccupationCode = '"
					+ cOccupationCode + "'  fetch first 2000 rows only ";
			ExeSQL tExeSQL = new ExeSQL();
			String tOccupationType = tExeSQL.getOneValue(sql);
			return tOccupationType;
		}

		/**
		 * 校验数据
		 * 
		 * @return boolean
		 */
		private boolean checkData() {

			return true;
		}

		/**
		 * 处理业务数据
		 * 
		 * @return boolean
		 */
		private boolean dealData() {
			
	      
	        Reflections tReflections = new Reflections();
/*

	        ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,
	                mEdorNo, mEdorType,mGrpContNo,"GrpContNo");

	        String[] tables = {"LCAscriptionRuleFactory", "LCAscriptionRuleParams"};

	        if (!validate.changeData(tables))
	        {
	            mErrors.copyAllErrors(validate.mErrors);
	            return false;
	        }
	        MMap map = validate.getMap();
	        mMap.add(map);*/

	        mMap.put("delete from LCAscriptionRuleFactory where grppolno='" 
	        		+ mGrpContNo +
	                "'" , "DELETE");
	        mMap.put("delete from LPAscriptionRuleFactory where  grpcontno = '"+mGrpContNo+"' and edorno='"+mEdorNo+"'", "DELETE");
	        LPAscriptionRuleFactoryDB tLPAscriptionRuleFactoryDB = new LPAscriptionRuleFactoryDB();
	        LPAscriptionRuleFactorySet tLPAscriptionRuleFactorySet = new LPAscriptionRuleFactorySet();
	        tLPAscriptionRuleFactoryDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
	        tLPAscriptionRuleFactoryDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
	        tLPAscriptionRuleFactorySet = tLPAscriptionRuleFactoryDB.query();
	        if (tLPAscriptionRuleFactorySet.size() < 1)
	        {
	            CError.buildErr(this, "没有得到LPAscriptionRuleFactory表信息");
	            return false;
	        }
	        for (int j = 1; j <= tLPAscriptionRuleFactorySet.size(); j++)
	        {
	            LPAscriptionRuleFactorySchema tLPAscriptionRuleFactorySchema = new LPAscriptionRuleFactorySchema();
	            LCAscriptionRuleFactorySchema tLCAscriptionRuleFactorySchema = new LCAscriptionRuleFactorySchema();
	            tLPAscriptionRuleFactorySchema = tLPAscriptionRuleFactorySet.get(j);
	            tReflections.transFields(tLCAscriptionRuleFactorySchema, tLPAscriptionRuleFactorySchema);
	            tLCAscriptionRuleFactorySchema.setModifyDate(mCurrentDate);
	            tLCAscriptionRuleFactorySchema.setModifyTime(mCurrentTime);
	            mLCAscriptionRuleFactorySet.add(tLCAscriptionRuleFactorySchema);
	        }

	        LCAscriptionRuleFactoryDB aLCAscriptionRuleFactoryDB = new LCAscriptionRuleFactoryDB();
	        LCAscriptionRuleFactorySet aLCAscriptionRuleFactorySet = new LCAscriptionRuleFactorySet();
	        aLCAscriptionRuleFactorySet = aLCAscriptionRuleFactoryDB.executeQuery("select * from LCAscriptionRuleFactory where  grpcontno = '"+mGrpContNo+"'");
	        if (aLCAscriptionRuleFactorySet.size() < 1)
	        {
	            CError.buildErr(this, "没有得到LLCAscriptionRuleFactory表信息");
	            return false;
	        }
	        for (int j = 1; j <= aLCAscriptionRuleFactorySet.size(); j++)
	        {
	            LCAscriptionRuleFactorySchema aLCAscriptionRuleFactorySchema = new LCAscriptionRuleFactorySchema();
	            LPAscriptionRuleFactorySchema aLPAscriptionRuleFactorySchema = new LPAscriptionRuleFactorySchema();
	            aLCAscriptionRuleFactorySchema = aLCAscriptionRuleFactorySet.get(j);
	            tReflections.transFields(aLPAscriptionRuleFactorySchema, aLCAscriptionRuleFactorySchema);
	            aLPAscriptionRuleFactorySchema.setEdorNo(mEdorNo);
	            aLPAscriptionRuleFactorySchema.setEdorType(mEdorType);
	            mLPAscriptionRuleFactorySet.add(aLPAscriptionRuleFactorySchema);
	        }

	        mMap.put(mLCAscriptionRuleFactorySet, "DELETE&INSERT");
	        mMap.put(mLPAscriptionRuleFactorySet, "DELETE&INSERT");

	        mMap.put("delete from LCAscriptionRuleParams where grpcontno='" 
	        		+ mGrpContNo +
	                "'" , "DELETE");
	        mMap.put("delete from LPAscriptionRuleParams where  grpcontno = '"+mGrpContNo+"' and edorno='"+mEdorNo+"'", "DELETE");
        LPAscriptionRuleParamsDB tLPAscriptionRuleParamsDB = new LPAscriptionRuleParamsDB();
	        LPAscriptionRuleParamsSet tLPAscriptionRuleParamsSet = new LPAscriptionRuleParamsSet();
	        tLPAscriptionRuleParamsDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
	        tLPAscriptionRuleParamsDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
	        tLPAscriptionRuleParamsSet = tLPAscriptionRuleParamsDB.query();
	        if (tLPAscriptionRuleParamsSet.size() < 1)
	        {
	            CError.buildErr(this, "没有得到LPAscriptionRuleParams表信息");
	            return false;
	        }
	        for (int j = 1; j <= tLPAscriptionRuleParamsSet.size(); j++)
	        {
	            LPAscriptionRuleParamsSchema tLPAscriptionRuleParamsSchema = new LPAscriptionRuleParamsSchema();
	            LCAscriptionRuleParamsSchema tLCAscriptionRuleParamsSchema = new LCAscriptionRuleParamsSchema();
	            tLPAscriptionRuleParamsSchema = tLPAscriptionRuleParamsSet.get(j);
	            tReflections.transFields(tLCAscriptionRuleParamsSchema, tLPAscriptionRuleParamsSchema);
	            tLCAscriptionRuleParamsSchema.setModifyDate(mCurrentDate);
	            tLCAscriptionRuleParamsSchema.setModifyTime(mCurrentTime);
	            mLCAscriptionRuleParamsSet.add(tLCAscriptionRuleParamsSchema);
	        }

	        LCAscriptionRuleParamsDB aLCAscriptionRuleParamsDB = new LCAscriptionRuleParamsDB();
	        LCAscriptionRuleParamsSet aLCAscriptionRuleParamsSet = new LCAscriptionRuleParamsSet();
	        aLCAscriptionRuleParamsSet = aLCAscriptionRuleParamsDB.executeQuery("select * from LCAscriptionRuleParams where  grpcontno = '"+mGrpContNo+"'");
	        if (aLCAscriptionRuleParamsSet.size() < 1)
	        {
	            CError.buildErr(this, "没有得到LLCAscriptionRuleParams表信息");
	            return false;
	        }
	        for (int j = 1; j <= aLCAscriptionRuleParamsSet.size(); j++)
	        {
	            LCAscriptionRuleParamsSchema aLCAscriptionRuleParamsSchema = new LCAscriptionRuleParamsSchema();
	            LPAscriptionRuleParamsSchema aLPAscriptionRuleParamsSchema = new LPAscriptionRuleParamsSchema();
	            aLCAscriptionRuleParamsSchema = aLCAscriptionRuleParamsSet.get(j);
	            tReflections.transFields(aLPAscriptionRuleParamsSchema, aLCAscriptionRuleParamsSchema);
	            aLPAscriptionRuleParamsSchema.setEdorNo(mEdorNo);
	            aLPAscriptionRuleParamsSchema.setEdorType(mEdorType);
	            mLPAscriptionRuleParamsSet.add(aLPAscriptionRuleParamsSchema);
	        }

	        mMap.put(mLCAscriptionRuleParamsSet, "DELETE&INSERT");
	        mMap.put(mLPAscriptionRuleParamsSet, "DELETE&INSERT");
	        
	      
//	      LCDUTY中没有grpcontno,土法子更新LCDuty和LPDuty表
	        LPGrpPositionDB tLPGrpPositionDB = new LPGrpPositionDB();
	        LPGrpPositionSet tLPGrpPositionSet = new LPGrpPositionSet();
	        tLPGrpPositionDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
	        tLPGrpPositionDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
	        tLPGrpPositionSet = tLPGrpPositionDB.query();
	        if (tLPGrpPositionSet.size() < 1)
	        {
	          System.out.println("没有级别信息的变化");
	        }else{
	        mMap.put("delete from  LCGrpPosition where grpcontno='" 
	        		+ mGrpContNo +
	                "'" , "DELETE");
	        mMap.put("delete from  LPGrpPosition where  grpcontno = '"+mGrpContNo+"' and edorno='"+mEdorNo+"'", "DELETE");
	        for (int j = 1; j <= tLPGrpPositionSet.size(); j++)
	        {
	            LPGrpPositionSchema tLPGrpPositionSchema = new LPGrpPositionSchema();
	            LCGrpPositionSchema tLCGrpPositionSchema = new LCGrpPositionSchema();
	            tLPGrpPositionSchema = tLPGrpPositionSet.get(j);
	            tReflections.transFields(tLCGrpPositionSchema, tLPGrpPositionSchema);
	            tLCGrpPositionSchema.setModifyDate(mCurrentDate);
	            tLCGrpPositionSchema.setModifyTime(mCurrentTime);
	            mLCGrpPositionSet.add(tLCGrpPositionSchema);
	        }

	        LCGrpPositionDB aLCGrpPositionDB = new LCGrpPositionDB();
	        LCGrpPositionSet aLCGrpPositionSet = new LCGrpPositionSet();
	        aLCGrpPositionSet = aLCGrpPositionDB.executeQuery("select * from LCGrpPosition where  grpcontno = '"+mGrpContNo+"'");
	        if (aLCGrpPositionSet.size() < 1)
	        {
	            CError.buildErr(this, "没有得到LLCGrpPosition表信息");
	            return false;
	        }
	        for (int j = 1; j <= aLCGrpPositionSet.size(); j++)
	        {
	            LCGrpPositionSchema aLCGrpPositionSchema = new LCGrpPositionSchema();
	            LPGrpPositionSchema aLPGrpPositionSchema = new LPGrpPositionSchema();
	            aLCGrpPositionSchema = aLCGrpPositionSet.get(j);
	            tReflections.transFields(aLPGrpPositionSchema, aLCGrpPositionSchema);
	            aLPGrpPositionSchema.setEdorNo(mEdorNo);
	            aLPGrpPositionSchema.setEdorType(mEdorType);
	            mLPGrpPositionSet.add(aLPGrpPositionSchema);
	        }

	        mMap.put(mLCGrpPositionSet, "DELETE&INSERT");
	        mMap.put(mLPGrpPositionSet, "DELETE&INSERT");
	        }

	        System.out.println("GrpEdorGBConfirmBL.dealData() 成功");
	        return true;
		}

		/**
		 * 提交数据到数据库
		 * 
		 * @return boolean
		 */
		private boolean submit() {
			VData data = new VData();
			data.add(mMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(data, "")) {
				mErrors.copyAllErrors(tPubSubmit.mErrors);
				return false;
			}
			return true;
		}
	}

