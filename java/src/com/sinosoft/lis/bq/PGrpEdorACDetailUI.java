package com.sinosoft.lis.bq;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:被保险人资料变更功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PGrpEdorACDetailUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public PGrpEdorACDetailUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    PGrpEdorACDetailBL tPGrpEdorACDetailBL = new PGrpEdorACDetailBL();
    System.out.println("---UI BEGIN---"+mOperate);
    if (tPGrpEdorACDetailBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tPGrpEdorACDetailBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "PGrpEdorACDetailUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
      mResult = tPGrpEdorACDetailBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
	  LPGrpPolSchema tLPGrpPolSchema = new LPGrpPolSchema();
	  LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
	  PGrpEdorACDetailUI tPGrpEdorACDetailUI = new PGrpEdorACDetailUI();
	  LPGrpAppntSchema tLPGrpAppntSchema = new LPGrpAppntSchema();
	  LPGrpSchema tLPGrpSchema = new LPGrpSchema();
	  LPGrpAddressSchema tLPGrpAddressSchema = new LPGrpAddressSchema();

	  CErrors tError = null;
	  //后面要执行的动作：添加，修改

	  String tRela = "";
	  String FlagStr = "";
	  String Content = "";
	  String transact = "";
	  String Result = "";

	  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	  transact = "INSERT||EDORAC";
	  System.out.println("------transact:" + transact);
	  GlobalInput tG = new GlobalInput();
	  System.out.println("------------------begin ui");
	  tG.ManageCom = "86";
	  tG.Operator = "endor";

	  LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	  tLPGrpEdorItemSchema.setEdorAcceptNo("20050704000074");
	  tLPGrpEdorItemSchema.setEdorNo("20050704000074");
	  tLPGrpEdorItemSchema.setEdorType("AC");
	  tLPGrpEdorItemSchema.setGrpContNo("0000000301");

	  //个人保单批改信息
	  //团单投保人信息  LCGrpAppnt
	  tLPGrpAppntSchema.setEdorNo("20050704000074");
	  tLPGrpAppntSchema.setEdorType("AC");
	  tLPGrpAppntSchema.setGrpContNo("0000000301"); //集体投保单号码
	  tLPGrpAppntSchema.setCustomerNo("00000003"); //客户号码
	  tLPGrpAppntSchema.setName("东信集团");
	  tLPGrpAppntSchema.setPostalAddress("ffff");
	  tLPGrpAppntSchema.setZipCode("11111");

	  //团体客户信息  LPGrp
	  tLPGrpSchema.setEdorNo("20050704000074");
	  tLPGrpSchema.setEdorType("AC");
	  tLPGrpSchema.setCustomerNo("00000003"); //客户号码
	  tLPGrpSchema.setGrpName("东信集团"); //单位名称

	  //团体客户地址  LCGrpAddress
	  tLPGrpAddressSchema.setEdorNo("20050704000074");
	  tLPGrpAddressSchema.setEdorType("AC");
	  tLPGrpAddressSchema.setCustomerNo("00000003"); //客户号码
	  tLPGrpAddressSchema.setGrpAddress("fffff"); //单位地址
	  System.out.println("*******************" + ("GrpAddress"));
	  tLPGrpAddressSchema.setGrpZipCode("123123"); //单位邮编
	/*  //保险联系人一
	  tLPGrpAddressSchema.setLinkMan1("asdfasd");
	  tLPGrpAddressSchema.setDepartment1("");
	  tLPGrpAddressSchema.setHeadShip1("");
	  tLPGrpAddressSchema.setPhone1("");
	  tLPGrpAddressSchema.setE_Mail1("");
	  tLPGrpAddressSchema.setFax1("");
	  //保险联系人二
	  tLPGrpAddressSchema.setLinkMan2("");
	  tLPGrpAddressSchema.setDepartment2("");
	  tLPGrpAddressSchema.setHeadShip2("");
	  tLPGrpAddressSchema.setPhone2("");
	  tLPGrpAddressSchema.setE_Mail2("");
	  tLPGrpAddressSchema.setFax2("");
*/
	String addrFlag = "MOD";
	  // 准备传输数据 VData
	  VData tVData = new VData();
	  //保存个人保单信息(保全)
	  tVData.addElement(addrFlag);
	  tVData.addElement(tG);
	  tVData.addElement(tLPGrpEdorItemSchema);
	  tVData.addElement(tLPGrpAppntSchema);
	  tVData.addElement(tLPGrpSchema);
	  tVData.addElement(tLPGrpAddressSchema);
	  tPGrpEdorACDetailUI.submitData(tVData, "INSERT||EDORAC");
	  System.out.println("-------test...");
  }
}
