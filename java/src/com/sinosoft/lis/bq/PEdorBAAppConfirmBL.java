

package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

//程序名称：PEdorBAAppConfirmBL.java
//程序功能：
//创建日期：2008-05-20
//创建人  ：pst
//更新记录：  更新人    更新日期     更新原因/内容
public class PEdorBAAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String aEdorNo = null;

    /** 保全类型 */
    private String aEdorType = null;

    /** 合同号 */
    private String mContNo = null;

    /** 保单险种号 */
    private String mPolNo = null;
    /** 保单险种编码 */
    private String mRiskCode=null;

    private LCPolSet mLCPolSet = null;
    
    private Reflections ref = new Reflections();
    /** 保单险种号 */
    private LCPolSchema mLCPolSchema = null;

    /**保单相关信息表*/
    private LPDutySet mLPDutySet =  new LPDutySet();
    private LPPolSet mLPPolSet =  new LPPolSet();
    private LPPolSet aLPPolSet =  new LPPolSet();
    private LPGetSet mLPGetSet = new LPGetSet();
    private LPPremSet mLPPremSet=new LPPremSet();
    private LPContSchema mLPContSchema =  new LPContSchema();
    
    private ReCalBL mReCalBL = null;
    /** 保全生效日期 */
    private String mEdorValiDate = null;

    /** 保全交费总额 */
    private double mGetMoney = 0.00;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String aMakeDate = PubFun.getCurrentDate();

    private String aMakeTime = PubFun.getCurrentTime();

    private String aOperator;
    
    private ExeSQL mExeSQL = new ExeSQL();
    
    private double mSumAmnt=0.0;
    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            System.out.println("PEdorBAAppConfirmBL.java->getInputData(cInputData)失败");
        	return false;
        }

        if (!dealData())
        {
        	System.out.println("PEdorBAAppConfirmBL.java->dealData()失败");
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema)cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
        aEdorNo = mLPEdorItemSchema.getEdorNo();
        aEdorType = mLPEdorItemSchema.getEdorType();
        mContNo = mLPEdorItemSchema.getContNo();
        mEdorValiDate = mLPEdorItemSchema.getEdorValiDate();     
        aOperator=mGlobalInput.Operator;
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData() {
    	if(!getLPPol()){
    		System.out.println("PEdorBAAppConfirmBL.java->dealData()->getLPPol()失败");
			return false;
    	}
		if (!dealPols()) {
			System.out.println("PEdorBAAppConfirmBL.java->dealData()->dealPols()失败");
			return false;
		}
		setLPCont();
		setLPTables();
		setEdorItem();
		return true;
	}
    
    private boolean setLPCont(){
    	for(int i=1;i<=this.mLPPolSet.size();i++){
    		this.mSumAmnt+=mLPPolSet.get(i).getAmnt();
    	}
    	this.mLPContSchema.setAmnt(this.mSumAmnt);
    	return true;
    }
    /**
     * 按险种处理保全数据
     * @return boolean
     */
    private boolean dealPols()
    {
       
        for (int i = 1; i <= aLPPolSet.size(); i++)
        {
            if (!dealOnePol(aLPPolSet.get(i)))
            {
                return false;
            }
        }
        return true;
    }
    /**
     * 处理一个险种
     * @param aLPPolSchema LPPolSchema
     * @return boolean
     */
    private boolean dealOnePol(LPPolSchema aLPPolSchema)
    {
        try
        {
            LCPolSchema tLCPolSchema = CommonBL.getLCPol(aLPPolSchema.getPolNo());
            if (tLCPolSchema ==  null)
            {
                mErrors.addOneError("未找到序号" + tLCPolSchema.getRiskSeqNo()
                        + "险种" + tLCPolSchema.getRiskCode() + "信息!");
                return false;
            }
            mReCalBL = new ReCalBL(aLPPolSchema, mLPEdorItemSchema);
            if (!mReCalBL.recal())
            {
                mErrors.copyAllErrors(mReCalBL.mErrors);
                return false;
            }
            this.mLPPolSet.add(mReCalBL.aftLPPolSet);
            this.mLPDutySet.add(mReCalBL.aftLPDutySet);
            this.mLPGetSet.add(mReCalBL.aftLPGetSet);
            this.mLPPremSet.add(mReCalBL.aftLPPremSet); 
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    private void setLPTables()
    {
        mMap.put(this.mLPPolSet, "DELETE&INSERT");
        mMap.put(this.mLPDutySet, "DELETE&INSERT");
        mMap.put(this.mLPGetSet, "DELETE&INSERT");
        mMap.put(this.mLPPremSet, "DELETE&INSERT");
        mMap.put(this.mLPContSchema, "DELETE&INSERT");
    }
    /**生成lppol*/
    private boolean getLPPol()
    {
    	LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(mContNo);
		LCPolSet tLCPolSet = new LCPolSet();
		tLCPolSet=tLCPolDB.query();
		if(tLCPolSet.size()<=0)
		{
			return false;
		}
		
		//获得主险PolNo
		String mainPolNo = "";
		for (int i = 1; i <= tLCPolSet.size(); i ++)
		{
			if(CommonBL.isULIRisk(tLCPolSet.get(i).getRiskCode()))
			{
				mainPolNo = tLCPolSet.get(i).getPolNo();
			}
		}
		
		//获得主险保额
		String mainAmnt = "";
		LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
		tLPEdorEspecialDataDB.setEdorNo(aEdorNo);
		tLPEdorEspecialDataDB.setEdorType(aEdorType);
		tLPEdorEspecialDataDB.setPolNo(mainPolNo);
		LPEdorEspecialDataSet tLPEdorEspecialDataSet = tLPEdorEspecialDataDB.query();
		if(tLPEdorEspecialDataSet==null || tLPEdorEspecialDataSet.size()==0)
		{
			System.out.println("获得主险保额失败");
			return false;
		}
		else
		{
			for(int j=1; j<= tLPEdorEspecialDataSet.size(); j++)
			{
				mainAmnt = tLPEdorEspecialDataSet.get(j).getEdorValue();
			}
		}
		
		
		for (int i = 1; i <= tLCPolSet.size(); i ++)
		{
			LPPolSchema tLPPolSchema = new LPPolSchema();
			Reflections ref = new Reflections();
			ref.transFields(tLPPolSchema, tLCPolSet.get(i));
			tLPPolSchema.setEdorNo(this.aEdorNo);
			tLPPolSchema.setEdorType(this.aEdorType);
			
			String tAmnt = null;
			if(tLPPolSchema.getAmnt()==0)
			{
				tAmnt = "0";
			}
			else
			{
				LPEdorEspecialDataDB tEspecialDB = new LPEdorEspecialDataDB();
				tEspecialDB.setEdorNo(aEdorNo);
				tEspecialDB.setEdorType(aEdorType);
				tEspecialDB.setPolNo(tLPPolSchema.getPolNo());
				
				LPEdorEspecialDataSet tEspecialSet = new LPEdorEspecialDataSet();
				tEspecialSet = tEspecialDB.query();
				if(tEspecialSet==null || tEspecialSet.size()==0)
				{
					tAmnt = mainAmnt;
				}
				else
				{
					for(int j=1; j<= tEspecialSet.size(); j++)
					{
						tAmnt = tEspecialSet.get(j).getEdorValue();
					}
				}
			}
			
			if(tAmnt != null && !tAmnt.equals(""))
			{
				tLPPolSchema.setAmnt(tAmnt);
				tLPPolSchema.setRiskAmnt(tAmnt);
			}
			aLPPolSet.add(tLPPolSchema);
		}
		LCContSchema tLCContSchema = new LCContSchema();
	    tLCContSchema=CommonBL.getLCCont(mContNo);
	    ref.transFields(mLPContSchema,tLCContSchema);
	    //处理保单信息
	        
	    mLPContSchema.setEdorNo(aEdorNo);
	    mLPContSchema.setEdorType(aEdorType);
	    mLPContSchema.setModifyDate(aMakeDate);
		mLPContSchema.setModifyTime(aMakeTime);
    	return true;
    }

    /**
     * 设置item表中的费用和状态
     */
    private void setEdorItem()
    {
        String sql = "update LPEdorItem " +
                "set "+
                "EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                "ModifyDate = '" + aMakeDate + "', " +
                "ModifyTime = '" + aMakeTime + "' " +
                "where EdorNo = '" + aEdorNo + "' " +
                "and EdorType = '" + aEdorType + "' ";
        mMap.put(sql.toString(), "UPDATE");
    }
 }
