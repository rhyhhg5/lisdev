package com.sinosoft.lis.bq;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author lh  modify by Alex 2005.1.10
 * @version 1.0
 */

import java.io.*;
import java.text.*;
import org.jdom.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class PrtGrpEndorsementBL {
    private Document myDocument;
    private Document myDocumentDetail;
    private XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例，对于集体
    private XmlExport xmlexportDetail = new XmlExport(); //新建一个XmlExport的实例，对于个人明细

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private MMap mMap = new MMap();

    private String mGrpContNo = "";
    //批单号
    private String mEdorNo = "";

    //业务处理相关变量
    private TextTag textTag = new TextTag(); //新建一个TextTag的实例，对于集体
    private TextTag textTagDetail = new TextTag(); //新建一个TextTag的实例，对于个人明细

    /** 全局数据 */
    private GlobalInput mGlobalInput = null;
    private ListTable tlistTable;
    private ListTable tlistTableDetail;
    double sum1 = 0; //本次收款金额
    double sum2 = 0; //本次付款金额
    double sum4 = 0; //实收/实付金额
    int sum3 = 0; //详细变动清单改动的人数
    int ab = 0;
    int extra = 0; //1:考虑附加险 0：不考虑附加险
    int detailno = 0; //记录详细批单的序号

    //private String strArr[] = null;
    int showExRisk = 0; //判断批单打印是否显示附加险 1：显示 0：不显示
    int showAllBnf = 0; //判断批单打印是否显示所有受益人 1：显示 0：不显示

    public PrtGrpEndorsementBL() {
    }

    public PrtGrpEndorsementBL(String aAcceptNo, String aGetNoticeNo) {
    }

    //从前台传批单号
    public PrtGrpEndorsementBL(String edorNo) {
        mEdorNo = edorNo;
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!submit()) {
            return false;
        }
        return true;
    }

    /**
     * 得到处理后生成的XmlExport
     * @param cInputData VData
     * @param cOperate String
     * @return XmlExport
     */
    public XmlExport getXmlExport(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return null;
        }

        if (!dealData()) {
            return null;
        }

        return xmlexport;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit() {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
     private boolean dealData(){
     	String tcheckJM = new ExeSQL().getOneValue("select 1 from lpgrpedoritem where edortype='JM' and edorno='"+mEdorNo+"'");
     	if(tcheckJM == null || tcheckJM.equals("") ||
            tcheckJM.equals("null")){
     		String tcheckUM = new ExeSQL().getOneValue("select 1 from lpgrpedoritem where edortype='UM' and edorno='"+mEdorNo+"'");
     		if(tcheckUM == null || tcheckUM.equals("") ||
     				tcheckUM.equals("null")){
	     		if (dealDataNormal()) {
	                return true;
	            }
     		}else{
     			if(dealDataUM())
     			return true;
     		}
     	}
     	else{
     		if (dealDataJM()) {
                return true;
            }
     	}
     	return false;
    }
     
	/**
	 * JM的单独处理函数
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealDataJM() {
        boolean tFlag = false; //打印主批单标记
        String tContNo="";
        
        //设置公司名称，地址等固定信息
        setFixedInfo();
        xmlexport.createDocuments("PrtGrpEndorsementJM.vts", mGlobalInput); //最好紧接着就初始化xml文档

        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        tLPEdorAppDB.getInfo();

//      //查询团险保全批改表中批单号为mEdorNo，批改状态为2（批改确认），核保状态为9（核保通过）的记录
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorNo(mEdorNo);
        LPGrpEdorMainSet tLPGrpEdorMainSet = new LPGrpEdorMainSet();
        tLPGrpEdorMainSet.set(tLPGrpEdorMainDB.query());
        if (tLPGrpEdorMainSet.size() == 0) {
            buildError("dealDataJM", "在LPGrpEdorMain中无相关批单号的数据");
            return false;
        }
        LPGrpEdorMainSchema tLPGrpEdorMainSchema = tLPGrpEdorMainSet.get(1);

        mGrpContNo = tLPGrpEdorMainSchema.getGrpContNo(); //取得保全记录的合同号

        //查询团险保全项目表中批单号为mEdorNo的记录
        String sql = "select * from LPGrpEdorItem where edorno='" + mEdorNo +
                     "' order by MakeDate,MakeTime";
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();
        tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(sql);
        if (tLPGrpEdorItemSet.size() == 0) {
            buildError("dealDataJM", "团险保全项目表中无相关批单号的数据");
            return false;
        }
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);

//      查询个保单保全批改表
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorNo(mEdorNo);
        LPEdorMainSet tLPEdorMainSet = new LPEdorMainSet();
        tLPEdorMainSet.set(tLPEdorMainDB.query());
        if (tLPEdorMainSet.size() == 0) {
            buildError("dealDataJM", "在LPEdorMain中无相关批单号的数据");
            return false;
        }
        LPEdorMainSchema tLPEdorMainSchema = tLPEdorMainSet.get(1);

        tContNo = tLPEdorMainSchema.getContNo(); //取得保全记录的合同号

        //查询团险保全项目表中批单号为mEdorNo的记录
        String sql1 = "select * from LPEdorItem where edorno='" + mEdorNo +
                     "' order by MakeDate,MakeTime";
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql1);
        if (tLPEdorItemSet.size() == 0) {
            buildError("dealDataJM", "团险保全项目表中无相关批单号的数据");
            return false;
        }
        LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(1);

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        tLCGrpContDB.getInfo();
        tLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        
        LCContSchema tLCContSchema = new LCContSchema();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        tLCContDB.getInfo();
        tLCContSchema.setSchema(tLCContDB.getSchema());
        
        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        //textTag.add("AgentCode", tLaAgentDB.getAgentCode());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") ||
            temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("Phone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
			+" (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
			+" (select agentgroup from laagent where agentcode ='"
                        + tLaAgentDB.getAgentCode()+"'))";
		LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(branchSQL);
        if(tLABranchGroupSet==null||tLABranchGroupSet.size()==0)
        {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());
        
        xmlexport.addDisplayControl("displayHead");
        textTag.add("EdorAcceptNo", tLPGrpEdorMainSchema.getEdorAcceptNo());
        textTag.add("EdorNo", tLPGrpEdorMainSchema.getEdorNo());
        textTag.add("GrpContNo", tLPEdorMainSchema.getContNo());
        textTag.add("BarCode1", tLPGrpEdorMainSchema.getEdorAcceptNo());
        textTag.add("BarCodeParam1", "BarHeight=25&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        textTag.add("AppDate",
                    CommonBL.decodeDate(tLPEdorAppDB.getEdorAppDate()));
        textTag.add("ConfDate",
                    CommonBL.decodeDate(tLPEdorAppDB.getMakeDate()));

        //查询经办人姓名
        String operatorName = "";
        String sql2 =
                "select userName "
                + "from LDUser "
                + "where userCode='" + tLPGrpEdorMainSchema.getOperator() +
                "' ";
        SSRS tSSRS = new SSRS();
        ExeSQL tExcSQL = new ExeSQL();

        tSSRS = tExcSQL.execSQL(sql2);
        if (tSSRS.getMaxRow() > 0) {
            operatorName = tSSRS.GetText(1, 1);
        } else {
            operatorName = tLPGrpEdorMainSchema.getOperator();
        }
        textTag.add("Operator", operatorName);
        
/*******讨论后确定传入投保人,取不到则取被保人*************************/
        
        
        textTag.add("CustomerNo",tContNo);
        WFAppntListDB tWFAppntListDB = new WFAppntListDB();
        WFAppntListSet tWFAppntListSet = new WFAppntListSet();
        tWFAppntListSet = tWFAppntListDB.executeQuery("select * from WFAppntList where cardno='"+tContNo+"'");
        if (tWFAppntListSet.size() == 0) {
        	System.out.println("dealDataJM:WF投保人表中无相关卡号的数据");
        	//开始从EDORESPECIALDATA中取出最新的被保人信息
        	EdorItemSpecialData tSpecialData = new EdorItemSpecialData(mEdorNo,"JM");
            if(!tSpecialData.query())
            {
            	buildError("dealDataJM", "未找到本次被保人信息！");
                return false;
            }
            //设置SpecialData中PolNo为"000000"
            String [] polnos = tSpecialData.getGrpPolNos();
            String polno = polnos[0];
            tSpecialData.setPolNo(polno);

            //取出最新信息
            textTag.add("AppntName", tSpecialData.getEdorValue("Name"));
    		textTag.add("EdorAppZipCode", tSpecialData.getEdorValue("ZipCode"));
    		textTag.add("EdorAppAddress", tSpecialData.getEdorValue("PostalAddress"));
    		textTag.add("EdorAppName", tSpecialData.getEdorValue("Name"));
        }
        else{
        WFAppntListSchema tWFAppntListSchema = tWFAppntListSet.get(1);
        textTag.add("AppntName", tWFAppntListSchema.getName());
		textTag.add("EdorAppZipCode", tWFAppntListSchema.getZipCode());
		textTag.add("EdorAppAddress", tWFAppntListSchema.getPostalAddress());
		textTag.add("EdorAppName", tWFAppntListSchema.getName());
        }
        
/*************************************************************************/
		
		

     if (tLPGrpEdorItemSchema.getEdorType().equals("JM")) { //激活卡客户资料变更
            	System.out.println("-----------------------------------");
                xmlexport.addDisplayControl("displayJM");
            if (!this.getDetailJM(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            }
            else { //对没有的保全项目类型生成空打印数据
                if (!this.getDetailForBlankType(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            }

        //打印主批单
        if (!tFlag) {
            buildError("dealData", "发生一个未知错误使主批单不能打印！");
            return false;
        }

        if (textTag.size() > 0) {
            myDocument = xmlexport.addTextTag(textTag);
        }
        //结束标记
        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");
        //结束标记
        xmlexport.addListTable(tEndListTable);

        mResult.clear();

        //生成主打印批单schema
        LPEdorPrintSchema tLPEdorPrintSchemaMain = new LPEdorPrintSchema();
        tLPEdorPrintSchemaMain.setEdorNo(mEdorNo);
        tLPEdorPrintSchemaMain.setManageCom(mGlobalInput.ManageCom);
        tLPEdorPrintSchemaMain.setPrtFlag("N");
        tLPEdorPrintSchemaMain.setPrtTimes(0);
        tLPEdorPrintSchemaMain.setMakeDate(PubFun.getCurrentDate());
        tLPEdorPrintSchemaMain.setMakeTime(PubFun.getCurrentTime());
        tLPEdorPrintSchemaMain.setOperator(mGlobalInput.Operator);
        tLPEdorPrintSchemaMain.setModifyDate(PubFun.getCurrentDate());
        tLPEdorPrintSchemaMain.setModifyTime(PubFun.getCurrentTime());
        InputStream ins = xmlexport.getInputStream();
        xmlexport.outputDocumentToFile("c:\\", "xmlexport");
        tLPEdorPrintSchemaMain.setEdorInfo(ins);
        mMap.put("delete from lpedorprint where edorno='" + mEdorNo + "'",
                 "DELETE");
        mMap.put(tLPEdorPrintSchemaMain, "BLOBINSERT");

        System.out.println("BLOBINSERT");

        mResult.addElement(mMap);
        return true;
    }


    /**
     * 处理普通保全项目
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealDataNormal() {
        boolean tFlag = false; //打印主批单标记
        boolean DFlag = false; //打印明细批单标记
        boolean qFlag = false; //打印保全变动清单所关联操作的标记

        //设置公司名称，地址等固定信息
        setFixedInfo();
        xmlexport.createDocuments("PrtGrpEndorsementApp.vts", mGlobalInput); //最好紧接着就初始化xml文档

        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        tLPEdorAppDB.getInfo();

//      //查询团险保全批改表中批单号为mEdorNo，批改状态为2（批改确认），核保状态为9（核保通过）的记录
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorNo(mEdorNo);
        LPGrpEdorMainSet tLPGrpEdorMainSet = new LPGrpEdorMainSet();
        tLPGrpEdorMainSet.set(tLPGrpEdorMainDB.query());
        if (tLPGrpEdorMainSet.size() == 0) {
            buildError("dealData", "在LPGrpEdorMain中无相关批单号的数据");
            return false;
        }
        LPGrpEdorMainSchema tLPGrpEdorMainSchema = tLPGrpEdorMainSet.get(1);

        mGrpContNo = tLPGrpEdorMainSchema.getGrpContNo(); //取得保全记录的合同号

        //查询团险保全项目表中批单号为mEdorNo的记录
        String sql = "select * from LPGrpEdorItem where edorno='" + mEdorNo +
                     "' order by MakeDate,MakeTime";
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();
        tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(sql);
        if (tLPGrpEdorItemSet.size() == 0) {
            buildError("dealData", "团险保全项目表中无相关批单号的数据");
            return false;
        }

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo()) {
            LBGrpContDB tLBGrpContDB = new LBGrpContDB();
            tLBGrpContDB.setGrpContNo(mGrpContNo);
            if (!tLBGrpContDB.getInfo()) {
                return false;
            }
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLCGrpContSchema, tLBGrpContDB.getSchema());
        } else {
            tLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        }
        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCGrpContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        //textTag.add("AgentCode", tLaAgentDB.getAgentCode());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") ||
            temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("Phone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
//        tLABranchGroupDB.setAgentGroup(tLCGrpContSchema.getAgentGroup());
//        tLABranchGroupDB.getInfo();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
			+" (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
			+" (select agentgroup from laagent where agentcode ='"
                        + tLaAgentDB.getAgentCode()+"'))"
			;
				LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(branchSQL);
        if(tLABranchGroupSet==null||tLABranchGroupSet.size()==0)
        {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());
//        textTag.add("AgentGroup", tLABranchGroupDB.getName());

        xmlexport.addDisplayControl("displayHead");
        textTag.add("EdorAcceptNo", tLPGrpEdorMainSchema.getEdorAcceptNo());
        textTag.add("EdorNo", tLPGrpEdorMainSchema.getEdorNo());
        textTag.add("GrpContNo", tLPGrpEdorMainSchema.getGrpContNo());
        textTag.add("BarCode1", tLPGrpEdorMainSchema.getEdorAcceptNo());
        textTag.add("BarCodeParam1", "BarHeight=25&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        textTag.add("AppDate",
                    CommonBL.decodeDate(tLPEdorAppDB.getEdorAppDate()));
        textTag.add("ConfDate",
                    CommonBL.decodeDate(tLPEdorAppDB.getMakeDate()));

        //查询经办人姓名
        String operatorName = "";
        String sql2 =
                "select userName "
                + "from LDUser "
                + "where userCode='" + tLPGrpEdorMainSchema.getOperator() +
                "' ";
        SSRS tSSRS = new SSRS();
        ExeSQL tExcSQL = new ExeSQL();

        tSSRS = tExcSQL.execSQL(sql2);
        if (tSSRS.getMaxRow() > 0) {
            operatorName = tSSRS.GetText(1, 1);
        } else {
            operatorName = tLPGrpEdorMainSchema.getOperator();
        }

        textTag.add("Operator", operatorName);
        textTag.add("CustomerNo", tLCGrpContSchema.getAppntNo());

        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(tLPGrpEdorMainSchema.getGrpContNo());
        if (!tLCGrpAppntDB.getInfo()) {
            buildError("getDetailAD", "团单投保人表LCGrpAppnt中无相关集体合同号的记录");
            return false;
        }
        LCGrpAppntSchema tLCGrpAppntSchema = tLCGrpAppntDB.getSchema();

        //判断是否含有AC项目
        boolean hasAC = false;
        for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++) {
            if (tLPGrpEdorItemSet.get(i).getEdorType().equals("AC")) {
                hasAC = true;
                break;
            }
        }

        //得到单位地址信息，如果是AC项目则取P表，否则取C表
        if (hasAC == true) {
            LPGrpAddressDB tLPGrpAddressDB = new LPGrpAddressDB();
            tLPGrpAddressDB.setEdorNo(mEdorNo);
            tLPGrpAddressDB.setEdorType("AC");
            tLPGrpAddressDB.setCustomerNo(tLCGrpAppntSchema.getCustomerNo());
            LPGrpAddressSet tLPGrpAddressSet = tLPGrpAddressDB.query();
            LPGrpAddressSchema tLPGrpAddressSchema = tLPGrpAddressSet.get(1);

            LPGrpContDB tLPGrpContDB = new LPGrpContDB();
            tLPGrpContDB.setEdorNo(mEdorNo);
            tLPGrpContDB.setEdorType("AC");
            tLPGrpContDB.setGrpContNo(tLPGrpEdorMainSchema.getGrpContNo());
            tLPGrpContDB.getInfo();

            textTag.add("EdorAppZipCode", tLPGrpAddressSchema.getGrpZipCode());
            textTag.add("EdorAppAddress", tLPGrpAddressSchema.getGrpAddress());
            textTag.add("EdorAppName", tLPGrpAddressSchema.getLinkMan1());
            textTag.add("AppntName", tLPGrpContDB.getGrpName());
        } else {
            LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
            tLCGrpAddressDB.setCustomerNo(tLCGrpAppntSchema.getCustomerNo());
            tLCGrpAddressDB.setAddressNo(tLCGrpAppntSchema.getAddressNo());
            if (!tLCGrpAddressDB.getInfo()) {
                buildError("", "未查到单位地址信息！");
                return false;
            }
            LCGrpAddressSchema tLCGrpAddressSchema = tLCGrpAddressDB.getSchema();
            textTag.add("EdorAppZipCode", tLCGrpAddressSchema.getGrpZipCode());
            textTag.add("EdorAppAddress", tLCGrpAddressSchema.getGrpAddress());
            if("TQ".equals(tLPGrpEdorItemSet.get(1).getEdorType())){
            	String tGrpContNo = tLPGrpEdorItemSet.get(1).getGrpContNo();
                LPInsuredDB tLPInsuredDB = new LPInsuredDB();
                tLPInsuredDB.setEdorNo(tLPGrpEdorItemSet.get(1).getEdorNo());
                tLPInsuredDB.setEdorType(tLPGrpEdorItemSet.get(1).getEdorType());
                tLPInsuredDB.setGrpContNo(tGrpContNo);
                LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();
                if (tLPInsuredSet.size() == 0) {
                    mErrors.addOneError("查询被保人出错。");
                    System.out.println("查询被保人出错。"
                                       + tLPInsuredDB.mErrors.getErrContent());
                    return false;
                }
            	textTag.add("EdorAppName", tLPInsuredSet.get(1).getName());
            }else if("SG".equals(tLPGrpEdorItemSet.get(1).getEdorType())){
            	//死亡领取这里取申请人姓名
            	String tEdorAppName=tExcSQL.getOneValue("select applyname from lgwork where workno='"+tLPGrpEdorItemSet.get(1).getEdorNo()+"'");
            	textTag.add("EdorAppName", tEdorAppName);
            }
            else{
            	textTag.add("EdorAppName", tLCGrpAddressSchema.getLinkMan1());
            }
            
            textTag.add("AppntName", tLCGrpContSchema.getGrpName());
        }

        for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++) {
            LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
            tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(i);
            if (tLPGrpEdorItemSchema.getEdorType().equals("AD")) { //投保人联系方式变更
                xmlexport.addDisplayControl("displayAD");
                if (!this.getDetailAD(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("BC")) { //受益人资料变更
                //xmlexport.addDisplayControl("displayBC");
                if (!this.getDetailBC(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("NI")) { //被保人增加
                xmlexport.addDisplayControl("displayNI");
                if (!this.getDetailNI(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
                DFlag = true;
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("TZ")) { //团险万能被保人增加
                xmlexport.addDisplayControl("displayTZ");
                if (!this.getDetailTZ(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
                DFlag = true;
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("BB")) { //被保险人信息变更
                xmlexport.addDisplayControl("displayBB");
                if (!this.getDetailBB(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("BJ")) { //被保险人信息变更
                xmlexport.addDisplayControl("displayBJ");
                if (!this.getDetailBJ(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("AC")) { //投保险人信息变更
                xmlexport.addDisplayControl("displayAC");
                if (!this.getDetailAC(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("GA")) { //投保险人信息变更
                xmlexport.addDisplayControl("displayGA");
                if (!this.getDetailGA(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("GD")) { //投保险人信息变更
                xmlexport.addDisplayControl("displayGD");
                if (!this.getDetailGD(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("TA")) { //团体万能资金分配 by gzh 
                xmlexport.addDisplayControl("displayTA");
                if (!this.getDetailTA(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("LR")) { //保单遗失补发
                xmlexport.addDisplayControl("displayLR");
                if (!this.getDetailLR(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("PR")) { //保单遗失补发
                xmlexport.addDisplayControl("displayPR");
                if (!this.getDetailPR(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("XT")) { //协议退保
                xmlexport.addDisplayControl("displayXT");
                if (!this.getDetailXT(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("CM")) { //协议退保
                xmlexport.addDisplayControl("displayCM");
                if (!this.getDetailCM(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("JM")) { //激活卡客户资料变更
            	System.out.println("-----------------------------------");
                xmlexport.addDisplayControl("displayJM");
                if (!this.getDetailJM(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("ZB")) { //追加保费
                xmlexport.addDisplayControl("displayZB");
                if (!this.getDetailZB(tLPGrpEdorItemSchema))
                {
                    return false;
                }
                tFlag = true;
                DFlag = true;
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("ZA")) { //追加保费
                xmlexport.addDisplayControl("displayZA");
                if (!this.getDetailZA(tLPGrpEdorItemSchema))
                {
                    return false;
                }
                tFlag = true;
                DFlag = true;
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("ZG")) { //追加管理费
                xmlexport.addDisplayControl("displayZG");
                if (!this.getDetailZG(tLPGrpEdorItemSchema))
                {
                    return false;
                }
                tFlag = true;
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("TY")) { //万能险追加保费
                xmlexport.addDisplayControl("displayTY");
                if (!this.getDetailTY(tLPGrpEdorItemSchema))
                {
                    return false;
                }
                tFlag = true;
                DFlag = true;
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("RS")) { //保单暂停
                xmlexport.addDisplayControl("displayRS");
                if (!this.getDetailRS(tLPGrpEdorItemSchema))
                {
                    return false;
                }
                tFlag = true;
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("RR")) { //保单恢复
                xmlexport.addDisplayControl("displayRR");
                if (!this.getDetailRR(tLPGrpEdorItemSchema))
                {
                    return false;
                }
                tFlag = true;
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("LQ"))
            { //提前领取
                xmlexport.addDisplayControl("displayLQ");
                if (!this.getDetailLQ(tLPGrpEdorItemSchema))
                {
                    return false;
                }
                tFlag = true;
                DFlag = true;
            }

            else if (tLPGrpEdorItemSchema.getEdorType().equals("ZT")) { //减人
                xmlexport.addDisplayControl("displayZT");
                if (!this.getDetailZT(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
                DFlag = true;
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("TQ")) { //减人
                xmlexport.addDisplayControl("displayTQ");
                if (!this.getDetailTQ(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
                DFlag = true;
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("RT")) {
                //险种减人
                xmlexport.addDisplayControl("displayRT");
                if (!this.getDetailRT(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("LT")) { //险种退保
                xmlexport.addDisplayControl("displayLT");
                if (!this.getDetailLT(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("WT")) { //犹豫期退保
                xmlexport.addDisplayControl("displayWT");
                if (!this.getDetailWT(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("UM")) { //犹豫期退保
                xmlexport.addDisplayControl("displayUM");
                if (!this.getDetailUM(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("WD"))
            { //无名单实名化删除
                xmlexport.addDisplayControl("displayWD");
                if (!this.getDetailWD(tLPGrpEdorItemSchema))
                {
                    return false;
                }
                tFlag = true; //不用明细表
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("WS"))
            { //无名单实名化
                xmlexport.addDisplayControl("displayWS");
                if (!this.getDetailWS(tLPGrpEdorItemSchema))
                {
                    return false;
                }
                tFlag = true; //不用明细表
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("CT")) { //合同终止
                //xmlexport.addDisplayControl("displayCT");
                if (!this.getDetailCT(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("IC")) { //被保人重要信息变更
                xmlexport.addDisplayControl("displayIC");
                if (!this.getDetailIC(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
                DFlag = true;
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("LP")) { //被保人重要信息变更
                xmlexport.addDisplayControl("displayLP");
                if (!this.getDetailLP(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
                DFlag = false;
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("WZ")) { //投保人联系方式变更
                xmlexport.addDisplayControl("displayWZ");
                if (!this.getDetailWZ(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("WJ")) { //投保人联系方式变更
                xmlexport.addDisplayControl("displayWJ");
                if (!this.getDetailWJ(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
            }else if (tLPGrpEdorItemSchema.getEdorType().equals("FP")) { //被保人重要信息变更
                xmlexport.addDisplayControl("displayFP");
                if (!this.getDetailFP(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
                DFlag = false;
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("YS"))
            { //被保人重要信息变更
                xmlexport.addDisplayControl("displayYS");
                if (!this.getDetailYS(tLPGrpEdorItemSchema))
                {
                    return false;
                }
                tFlag = true;
                DFlag = false;
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("TF"))
            {
                xmlexport.addDisplayControl("displayTF");
                if (!this.getDetailTF(tLPGrpEdorItemSchema))
                {
                    return false ;
                }
                tFlag = true ;
                DFlag = false ;
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("GB"))
            {
                xmlexport.addDisplayControl("displayGB");
                if (!this.getDetailGB(tLPGrpEdorItemSchema))
                {
                    return false ;
                }
                tFlag = true ;
                DFlag = false ;
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("TL"))
            {
                xmlexport.addDisplayControl("displayTL");
                if (!this.getDetailTL(tLPGrpEdorItemSchema))
                {
                    return false ;
                }
                tFlag = true ;
                DFlag = true ;
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("HL"))
            {
                xmlexport.addDisplayControl("displayHL");
                if (!this.getDetailHL(tLPGrpEdorItemSchema))
                {
                    return false ;
                }
                tFlag = true ;
                DFlag = true ;
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("SG"))
            {
                xmlexport.addDisplayControl("displaySG");
                if (!this.getDetailSG(tLPGrpEdorItemSchema))
                {
                    return false ;
                }
                tFlag = true ;
                DFlag = true ;
            }
            else if(tLPGrpEdorItemSchema.getEdorType().equals("TG"))
            {
            	String riskCode = new ExeSQL().getOneValue("select riskcode from lcgrppol where grpcontno = '"+tLPGrpEdorItemSchema.getGrpContNo()+"'");
            	if("370301".equals(riskCode)){
            		xmlexport.addDisplayControl("displayTG2");
            	}else{
	            	xmlexport.addDisplayControl("displayTG");
	            }
            	if (!this.getDetailTG(tLPGrpEdorItemSchema))
            	{
            		return false ;
            	}
            	tFlag = true ;
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("ZF"))
            {
                xmlexport.addDisplayControl("displayZF");
                if (!this.getDetailZF(tLPGrpEdorItemSchema))
                {
                    return false ;
                }
                tFlag = true ;
                DFlag = false ;
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("ZE"))
            {
                xmlexport.addDisplayControl("displayZE");
                if (!this.getDetailZE(tLPGrpEdorItemSchema))
                {
                    return false ;
                }
                tFlag = true ;
                DFlag = false ;
            }
          else { //对没有的保全项目类型生成空打印数据
                if (!this.getDetailForBlankType(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            }

            sql = "select * from ljsgetendorse where 1=1 and EndorsementNo ='" +
            	mEdorNo + "' fetch first 1 row only ";
            LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
            LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.executeQuery(
                    sql);
            if (tLJSGetEndorseSet == null || tLJSGetEndorseSet.size() < 1) {
                System.out.println("没有收退费记录");
            } else {
                this.xmlexport.addDisplayControl("displayMN");
                sql = "select getMoney from LPEdorApp where edoracceptno='" +
                      mEdorNo + "'";
                ExeSQL tExeSQL = new ExeSQL();
                String tReturn = tExeSQL.getOneValue(sql);
                double tmoney = Double.parseDouble(tReturn);
                if (tmoney < 0.0) {
                    this.textTag.add("MN_Type", "应退");
                     this.textTag.add("MN_Money", CommonBL.bigDoubleToCommonString(Math.abs(tmoney), "0.00"));
                } else if (tmoney > 0.0) {
                    this.textTag.add("MN_Type", "应收");
                     this.textTag.add("MN_Money", CommonBL.bigDoubleToCommonString(Math.abs(tmoney), "0.00"));
                } else {
                    this.textTag.add("MN_Type", "应收");
                    this.textTag.add("MN_Money", 0);
                }
            }

            //判断是否进行保全变动清单的打印
            LMEdorItemDB tLMEdorItemDB = new LMEdorItemDB();
            tLMEdorItemDB.setEdorCode(tLPGrpEdorItemSchema.getEdorType());
            tLMEdorItemDB.setAppObj("G");
            LMEdorItemSet tLMEdorItemSet = tLMEdorItemDB.query();
            if (tLMEdorItemSet.size() == 0) {
                buildError("tLMEdorItemSet", "在取得LMEdorItemSet的数据时发生错误");
                return false;
            }

            tLMEdorItemDB.setSchema(tLMEdorItemSet.get(1));

            if (tLMEdorItemDB.getGrpNeedList() != null &&
                tLMEdorItemDB.getGrpNeedList().equals("Y")) {
                qFlag = true;
                int tDisplayFlag; //取得保全清单是否打迎险种信息的表示，1，2－不需要打印险种信息，3－需要打迎险种信息
                if (tLMEdorItemDB.getDisplayFlag() != null &&
                    !"".equals(tLMEdorItemDB.getDisplayFlag())) {
                    tDisplayFlag = Integer.parseInt(tLMEdorItemDB.
                            getDisplayFlag());
                } else {
                    tDisplayFlag = 2;
                }
                /**if (!this.BillData(tLPGrpEdorItemSchema, tDisplayFlag))
                                 {
                    return false;
                                 }*/
            }
        }
        if (DFlag && !qFlag) {
            CError.buildErr(this, "DFlag表识为真，但LMEdorItemSet表中无打印保全清单描述");
            return false;
        }
        //打印补退费明细表
//        getPayGetDetails(tLPGrpEdorItemSet);

        //生成保全清单的xml数据
        //qFlag = false;
        if (qFlag) {
            xmlexportDetail.createDocument("PrtGrpEndorsementBill.vts",
                                           "print");
            tlistTableDetail = new ListTable();
            tlistTableDetail.setName("PRTGRPED");
            //生成主险名
            LMRiskDB bLMRiskDB = new LMRiskDB();
            LCGrpPolDB bLCGrpPolDB = new LCGrpPolDB();
            bLCGrpPolDB.setGrpContNo(mGrpContNo);
            LCGrpPolSet bLCGrpPolSet = new LCGrpPolSet();
            bLCGrpPolSet.set(bLCGrpPolDB.query());
            if (bLCGrpPolSet.size() == 0) {
                return false;
            } else {
                boolean flag = false;
                LCGrpPolSchema bLCGrpPolSchema;
                LMRiskAppDB bLMRiskAppDB = new LMRiskAppDB();
                String mainRiskName = "";
                String subRiskName = "";
                for (int j = 1; j <= bLCGrpPolSet.size(); j++) {
                    bLCGrpPolSchema = new LCGrpPolSchema();
                    bLCGrpPolSchema.setSchema(bLCGrpPolSet.get(j));
                    bLMRiskAppDB.setRiskCode(bLCGrpPolSchema.getRiskCode());
                    if (!bLMRiskAppDB.getInfo()) {
//                        mErrors.copyAllErrors(bLMRiskAppDB.mErrors);
//                        buildError("dealData-getRiskApp",
//                                   "在取得LMRiskApp的数据时发生错误");
//                        return false;
                        continue;
                    }
                    if (bLMRiskAppDB.getSubRiskFlag().equals("M")) {
                        bLMRiskDB.setRiskCode(bLCGrpPolSchema.getRiskCode());
                        if (!bLMRiskDB.getInfo()) {
//                            mErrors.copyAllErrors(bLMRiskDB.mErrors);
//                            buildError("desalData-getLMRisk",
//                                       "在取得LMRisk的数据时发生错误");
//                            return false;
                            continue;
                        }
                        flag = true;
                        mainRiskName += bLMRiskDB.getRiskName() + "、";
                    } else if (bLMRiskAppDB.getSubRiskFlag().equals("S")) {
                        bLMRiskDB.setRiskCode(bLCGrpPolSchema.getRiskCode());
                        if (!bLMRiskDB.getInfo()) {
//                            mErrors.copyAllErrors(bLMRiskDB.mErrors);
//                            buildError("desalData-getLMRisk",
//                                       "在取得LMRisk的数据时发生错误");
//                            return false;
                            continue;
                        }
                        flag = true;
                        subRiskName += bLMRiskDB.getRiskName() + "、";
                    }
                    if (flag == false) {
//                        buildError("deal-getrisk", "没有相对应的险种！");
//                        return false;
                        continue;
                    }
                }
                if (mainRiskName.endsWith("、")) {
                    mainRiskName = mainRiskName.substring(0,
                            mainRiskName.length() - 1);
                }
                if (subRiskName.endsWith("、")) {
                    subRiskName = subRiskName.substring(0,
                            subRiskName.length() - 1);
                }
                textTagDetail.add("RiskNameM", mainRiskName);
                textTagDetail.add("RiskNameS", subRiskName);
            }

            //生成当天日期
            SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
            textTagDetail.add("Today", df.format(new java.util.Date()));

            textTagDetail.add("EdorNo", mEdorNo);
            textTagDetail.add("GrpContNo", mGrpContNo);
            textTagDetail.add("GrpName", tLCGrpContSchema.getGrpName());
            textTagDetail.add("GrpNo", tLCGrpContSchema.getAppntNo());
            String sum11 = new DecimalFormat("0.00").format(Double.valueOf(
                    String.valueOf(sum1)));
            String sum22 = new DecimalFormat("0.00").format(Double.valueOf(
                    String.valueOf(sum2)));
            String sum44 = new DecimalFormat("0.00").format(Double.valueOf(
                    String.valueOf(sum4)));
            textTagDetail.add("sum1", sum11);
            textTagDetail.add("sum2", sum22);
            textTagDetail.add("sum3", sum3);
            textTagDetail.add("sum4", sum44);

            //加入textTagDetail
            if (textTagDetail.size() > 0) {
                myDocumentDetail = xmlexportDetail.addTextTag(textTagDetail);
            }

            //加入tlistTableDetail
            String strArr[] = new String[17];
            strArr[0] = "num";
            strArr[1] = "Name";
            strArr[2] = "sex";
            strArr[3] = "Birthday";
            strArr[4] = "IDType";
            strArr[5] = "IDNo";
            strArr[6] = "WorkType";
            strArr[7] = "RiskName";
            strArr[8] = "RiskName1";
            strArr[9] = "Prem";
            strArr[10] = "Amnt";
            strArr[11] = "Years";
            strArr[12] = "BnfName";
            strArr[13] = "GetMoney";
            strArr[14] = "Interest";
            strArr[15] = "sum";
            strArr[16] = "";
            xmlexportDetail.addListTable(tlistTableDetail, strArr);
        }

        //打印主批单
        if (!tFlag) {

            buildError("dealData", "发生一个未知错误使主批单不能打印！");
            return false;

            //return true;
        }

        if (textTag.size() > 0) {
            myDocument = xmlexport.addTextTag(textTag);
        }
        //结束标记
        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");
        //结束标记
        xmlexport.addListTable(tEndListTable);

        mResult.clear();

        //生成主打印批单schema
        LPEdorPrintSchema tLPEdorPrintSchemaMain = new LPEdorPrintSchema();
        tLPEdorPrintSchemaMain.setEdorNo(mEdorNo);
        tLPEdorPrintSchemaMain.setManageCom(mGlobalInput.ManageCom);
        tLPEdorPrintSchemaMain.setPrtFlag("N");
        tLPEdorPrintSchemaMain.setPrtTimes(0);
        tLPEdorPrintSchemaMain.setMakeDate(PubFun.getCurrentDate());
        tLPEdorPrintSchemaMain.setMakeTime(PubFun.getCurrentTime());
        tLPEdorPrintSchemaMain.setOperator(mGlobalInput.Operator);
        tLPEdorPrintSchemaMain.setModifyDate(PubFun.getCurrentDate());
        tLPEdorPrintSchemaMain.setModifyTime(PubFun.getCurrentTime());
        InputStream ins = xmlexport.getInputStream();
        xmlexport.outputDocumentToFile("c:\\", "xmlexport");
        tLPEdorPrintSchemaMain.setEdorInfo(ins);
        mMap.put("delete from lpedorprint where edorno='" + mEdorNo + "'",
                 "DELETE");
        mMap.put(tLPEdorPrintSchemaMain, "BLOBINSERT");

        System.out.println("BLOBINSERT");
        //生成变动清单打印schema
        qFlag = false;
        if (qFlag) {
            LPEdorPrint2Schema tLPEdorPrint2SchemaDetail = new
                    LPEdorPrint2Schema();
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLPEdorPrint2SchemaDetail,
                                     tLPEdorPrintSchemaMain);
            InputStream ins1 = xmlexportDetail.getInputStream();
            xmlexportDetail.outputDocumentToFile("c:\\", "PrtGrpEndorsementBL");
            tLPEdorPrint2SchemaDetail.setEdorInfo(ins1);
            mMap.put("delete from lpedorprint2 where edorno='" + mEdorNo + "'",
                     "DELETE");
            mMap.put(tLPEdorPrint2SchemaDetail, "BLOBINSERT");
        }

        mResult.addElement(mMap);
        return true;
    }



    /**
     * 处理普通保全项目
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealDataUM() {
        boolean tFlag = false; //打印主批单标记
        boolean DFlag = false; //打印明细批单标记
        boolean qFlag = false; //打印保全变动清单所关联操作的标记

        //设置公司名称，地址等固定信息
        setFixedInfo();
        xmlexport.createDocuments("PrtGrpEndorsementUM.vts", mGlobalInput); //最好紧接着就初始化xml文档

        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        tLPEdorAppDB.getInfo();

//      //查询团险保全批改表中批单号为mEdorNo，批改状态为2（批改确认），核保状态为9（核保通过）的记录
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorNo(mEdorNo);
        LPGrpEdorMainSet tLPGrpEdorMainSet = new LPGrpEdorMainSet();
        tLPGrpEdorMainSet.set(tLPGrpEdorMainDB.query());
        if (tLPGrpEdorMainSet.size() == 0) {
            buildError("dealData", "在LPGrpEdorMain中无相关批单号的数据");
            return false;
        }
        LPGrpEdorMainSchema tLPGrpEdorMainSchema = tLPGrpEdorMainSet.get(1);

        mGrpContNo = tLPGrpEdorMainSchema.getGrpContNo(); //取得保全记录的合同号

        //查询团险保全项目表中批单号为mEdorNo的记录
        String sql = "select * from LPGrpEdorItem where edorno='" + mEdorNo +
                     "' order by MakeDate,MakeTime";
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();
        tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(sql);
        if (tLPGrpEdorItemSet.size() == 0) {
            buildError("dealData", "团险保全项目表中无相关批单号的数据");
            return false;
        }

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo()) {
            LBGrpContDB tLBGrpContDB = new LBGrpContDB();
            tLBGrpContDB.setGrpContNo(mGrpContNo);
            if (!tLBGrpContDB.getInfo()) {
                return false;
            }
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLCGrpContSchema, tLBGrpContDB.getSchema());
        } else {
            tLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        }
        
        textTag.add("GrpName", tLCGrpContSchema.getGrpName());
        textTag.add("GrpNo", tLCGrpContSchema.getAppntNo());
        
        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCGrpContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        //textTag.add("AgentCode", tLaAgentDB.getAgentCode());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") ||
            temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("Phone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
//        tLABranchGroupDB.setAgentGroup(tLCGrpContSchema.getAgentGroup());
//        tLABranchGroupDB.getInfo();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
			+" (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
			+" (select agentgroup from laagent where agentcode ='"
                        + tLaAgentDB.getAgentCode()+"'))"
			;
				LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(branchSQL);
        if(tLABranchGroupSet==null||tLABranchGroupSet.size()==0)
        {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());
//        textTag.add("AgentGroup", tLABranchGroupDB.getName());

        xmlexport.addDisplayControl("displayHead");
        xmlexport.addDisplayControl("displayHeadUM");
        xmlexport.addDisplayControl("displayCashUM");
        textTag.add("EdorAcceptNo", tLPGrpEdorMainSchema.getEdorAcceptNo());
        textTag.add("EdorNo", tLPGrpEdorMainSchema.getEdorNo());
        textTag.add("GrpContNo", tLPGrpEdorMainSchema.getGrpContNo());
        textTag.add("BarCode1", tLPGrpEdorMainSchema.getEdorAcceptNo());
        textTag.add("BarCodeParam1", "BarHeight=25&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        textTag.add("AppDate",
                    CommonBL.decodeDate(tLPEdorAppDB.getEdorAppDate()));
        textTag.add("ConfDate",
                    CommonBL.decodeDate(tLPEdorAppDB.getMakeDate()));

        //查询经办人姓名
        String operatorName = "";
        String sql2 =
                "select userName "
                + "from LDUser "
                + "where userCode='" + tLPGrpEdorMainSchema.getOperator() +
                "' ";
        SSRS tSSRS = new SSRS();
        ExeSQL tExcSQL = new ExeSQL();

        tSSRS = tExcSQL.execSQL(sql2);
        if (tSSRS.getMaxRow() > 0) {
            operatorName = tSSRS.GetText(1, 1);
        } else {
            operatorName = tLPGrpEdorMainSchema.getOperator();
        }

        textTag.add("Operator", operatorName);
        textTag.add("CustomerNo", tLCGrpContSchema.getAppntNo());

        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(tLPGrpEdorMainSchema.getGrpContNo());
        if (!tLCGrpAppntDB.getInfo()) {
            buildError("getDetailAD", "团单投保人表LCGrpAppnt中无相关集体合同号的记录");
            return false;
        }
        LCGrpAppntSchema tLCGrpAppntSchema = tLCGrpAppntDB.getSchema();

        //判断是否含有AC项目
        boolean hasAC = false;
        for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++) {
            if (tLPGrpEdorItemSet.get(i).getEdorType().equals("AC")) {
                hasAC = true;
                break;
            }
        }

        //得到单位地址信息，如果是AC项目则取P表，否则取C表
        if (hasAC == true) {
            LPGrpAddressDB tLPGrpAddressDB = new LPGrpAddressDB();
            tLPGrpAddressDB.setEdorNo(mEdorNo);
            tLPGrpAddressDB.setEdorType("AC");
            tLPGrpAddressDB.setCustomerNo(tLCGrpAppntSchema.getCustomerNo());
            LPGrpAddressSet tLPGrpAddressSet = tLPGrpAddressDB.query();
            LPGrpAddressSchema tLPGrpAddressSchema = tLPGrpAddressSet.get(1);

            LPGrpContDB tLPGrpContDB = new LPGrpContDB();
            tLPGrpContDB.setEdorNo(mEdorNo);
            tLPGrpContDB.setEdorType("AC");
            tLPGrpContDB.setGrpContNo(tLPGrpEdorMainSchema.getGrpContNo());
            tLPGrpContDB.getInfo();

            textTag.add("EdorAppZipCode", tLPGrpAddressSchema.getGrpZipCode());
            textTag.add("EdorAppAddress", tLPGrpAddressSchema.getGrpAddress());
            textTag.add("EdorAppName", tLPGrpAddressSchema.getLinkMan1());
            textTag.add("AppntName", tLPGrpContDB.getGrpName());
        } else {
            LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
            tLCGrpAddressDB.setCustomerNo(tLCGrpAppntSchema.getCustomerNo());
            tLCGrpAddressDB.setAddressNo(tLCGrpAppntSchema.getAddressNo());
            if (!tLCGrpAddressDB.getInfo()) {
                buildError("", "未查到单位地址信息！");
                return false;
            }
            LCGrpAddressSchema tLCGrpAddressSchema = tLCGrpAddressDB.getSchema();
            textTag.add("EdorAppZipCode", tLCGrpAddressSchema.getGrpZipCode());
            textTag.add("EdorAppAddress", tLCGrpAddressSchema.getGrpAddress());
            if("TQ".equals(tLPGrpEdorItemSet.get(1).getEdorType())){
            	String tGrpContNo = tLPGrpEdorItemSet.get(1).getGrpContNo();
                LPInsuredDB tLPInsuredDB = new LPInsuredDB();
                tLPInsuredDB.setEdorNo(tLPGrpEdorItemSet.get(1).getEdorNo());
                tLPInsuredDB.setEdorType(tLPGrpEdorItemSet.get(1).getEdorType());
                tLPInsuredDB.setGrpContNo(tGrpContNo);
                LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();
                if (tLPInsuredSet.size() == 0) {
                    mErrors.addOneError("查询被保人出错。");
                    System.out.println("查询被保人出错。"
                                       + tLPInsuredDB.mErrors.getErrContent());
                    return false;
                }
            	textTag.add("EdorAppName", tLPInsuredSet.get(1).getName());
            }else if("SG".equals(tLPGrpEdorItemSet.get(1).getEdorType())){
            	//死亡领取这里取申请人姓名
            	String tEdorAppName=tExcSQL.getOneValue("select applyname from lgwork where workno='"+tLPGrpEdorItemSet.get(1).getEdorNo()+"'");
            	textTag.add("EdorAppName", tEdorAppName);
            }
            else{
            	textTag.add("EdorAppName", tLCGrpAddressSchema.getLinkMan1());
            }
            
            textTag.add("AppntName", tLCGrpContSchema.getGrpName());
        }

        for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++) {
            LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
            tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(i);
            if (tLPGrpEdorItemSchema.getEdorType().equals("AD")) { //投保人联系方式变更
                xmlexport.addDisplayControl("displayAD");
                if (!this.getDetailAD(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true;
            } else if (tLPGrpEdorItemSchema.getEdorType().equals("UM")) { //犹豫期退保
                xmlexport.addDisplayControl("displayUM");
                if (!this.getDetailUM(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            }
            else if (tLPGrpEdorItemSchema.getEdorType().equals("ZF"))
            {
                xmlexport.addDisplayControl("displayZF");
                if (!this.getDetailZF(tLPGrpEdorItemSchema))
                {
                    return false ;
                }
                tFlag = true ;
                DFlag = false ;
            }
          else { //对没有的保全项目类型生成空打印数据
                if (!this.getDetailForBlankType(tLPGrpEdorItemSchema)) {
                    return false;
                }
                tFlag = true; //不用明细表
            }

            sql = "select * from ljsgetendorse where 1=1 and EndorsementNo ='" +
            	mEdorNo + "' fetch first 1 row only ";
            LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
            LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.executeQuery(
                    sql);
            if (tLJSGetEndorseSet == null || tLJSGetEndorseSet.size() < 1 || tLJSGetEndorseSet.get(1).getFeeOperationType().equals("TZ")) {
                System.out.println("没有收退费记录");
            } else {
                this.xmlexport.addDisplayControl("displayMN");
                sql = "select getMoney from LPEdorApp where edoracceptno='" +
                      mEdorNo + "'";
                ExeSQL tExeSQL = new ExeSQL();
                String tReturn = tExeSQL.getOneValue(sql);
                double tmoney = Double.parseDouble(tReturn);
                if (tmoney < 0.0) {
                    this.textTag.add("MN_Type", "应退");
                     this.textTag.add("MN_Money", CommonBL.bigDoubleToCommonString(Math.abs(tmoney), "0.00"));
                } else if (tmoney > 0.0) {
                    this.textTag.add("MN_Type", "应收");
                     this.textTag.add("MN_Money", CommonBL.bigDoubleToCommonString(Math.abs(tmoney), "0.00"));
                } else {
                    this.textTag.add("MN_Type", "应收");
                    this.textTag.add("MN_Money", 0);
                }
            }

            //判断是否进行保全变动清单的打印
            LMEdorItemDB tLMEdorItemDB = new LMEdorItemDB();
            tLMEdorItemDB.setEdorCode(tLPGrpEdorItemSchema.getEdorType());
            tLMEdorItemDB.setAppObj("G");
            LMEdorItemSet tLMEdorItemSet = tLMEdorItemDB.query();
            if (tLMEdorItemSet.size() == 0) {
                buildError("tLMEdorItemSet", "在取得LMEdorItemSet的数据时发生错误");
                return false;
            }

            tLMEdorItemDB.setSchema(tLMEdorItemSet.get(1));

            if (tLMEdorItemDB.getGrpNeedList() != null &&
                tLMEdorItemDB.getGrpNeedList().equals("Y")) {
                qFlag = true;
                int tDisplayFlag; //取得保全清单是否打迎险种信息的表示，1，2－不需要打印险种信息，3－需要打迎险种信息
                if (tLMEdorItemDB.getDisplayFlag() != null &&
                    !"".equals(tLMEdorItemDB.getDisplayFlag())) {
                    tDisplayFlag = Integer.parseInt(tLMEdorItemDB.
                            getDisplayFlag());
                } else {
                    tDisplayFlag = 2;
                }
            }
        }
        if (DFlag && !qFlag) {
            CError.buildErr(this, "DFlag表识为真，但LMEdorItemSet表中无打印保全清单描述");
            return false;
        }

        //生成保全清单的xml数据
        //qFlag = false;
        if (qFlag) {
            xmlexportDetail.createDocument("PrtGrpEndorsementBill.vts",
                                           "print");
            tlistTableDetail = new ListTable();
            tlistTableDetail.setName("PRTGRPED");
            //生成主险名
            LMRiskDB bLMRiskDB = new LMRiskDB();
            LCGrpPolDB bLCGrpPolDB = new LCGrpPolDB();
            bLCGrpPolDB.setGrpContNo(mGrpContNo);
            LCGrpPolSet bLCGrpPolSet = new LCGrpPolSet();
            bLCGrpPolSet.set(bLCGrpPolDB.query());
            if (bLCGrpPolSet.size() == 0) {
                return false;
            } else {
                boolean flag = false;
                LCGrpPolSchema bLCGrpPolSchema;
                LMRiskAppDB bLMRiskAppDB = new LMRiskAppDB();
                String mainRiskName = "";
                String subRiskName = "";
                for (int j = 1; j <= bLCGrpPolSet.size(); j++) {
                    bLCGrpPolSchema = new LCGrpPolSchema();
                    bLCGrpPolSchema.setSchema(bLCGrpPolSet.get(j));
                    bLMRiskAppDB.setRiskCode(bLCGrpPolSchema.getRiskCode());
                    if (!bLMRiskAppDB.getInfo()) {
//                        mErrors.copyAllErrors(bLMRiskAppDB.mErrors);
//                        buildError("dealData-getRiskApp",
//                                   "在取得LMRiskApp的数据时发生错误");
//                        return false;
                        continue;
                    }
                    if (bLMRiskAppDB.getSubRiskFlag().equals("M")) {
                        bLMRiskDB.setRiskCode(bLCGrpPolSchema.getRiskCode());
                        if (!bLMRiskDB.getInfo()) {
//                            mErrors.copyAllErrors(bLMRiskDB.mErrors);
//                            buildError("desalData-getLMRisk",
//                                       "在取得LMRisk的数据时发生错误");
//                            return false;
                            continue;
                        }
                        flag = true;
                        mainRiskName += bLMRiskDB.getRiskName() + "、";
                    } else if (bLMRiskAppDB.getSubRiskFlag().equals("S")) {
                        bLMRiskDB.setRiskCode(bLCGrpPolSchema.getRiskCode());
                        if (!bLMRiskDB.getInfo()) {
//                            mErrors.copyAllErrors(bLMRiskDB.mErrors);
//                            buildError("desalData-getLMRisk",
//                                       "在取得LMRisk的数据时发生错误");
//                            return false;
                            continue;
                        }
                        flag = true;
                        subRiskName += bLMRiskDB.getRiskName() + "、";
                    }
                    if (flag == false) {
//                        buildError("deal-getrisk", "没有相对应的险种！");
//                        return false;
                        continue;
                    }
                }
                if (mainRiskName.endsWith("、")) {
                    mainRiskName = mainRiskName.substring(0,
                            mainRiskName.length() - 1);
                }
                if (subRiskName.endsWith("、")) {
                    subRiskName = subRiskName.substring(0,
                            subRiskName.length() - 1);
                }
                textTagDetail.add("RiskNameM", mainRiskName);
                textTagDetail.add("RiskNameS", subRiskName);
            }

            //生成当天日期
            SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
            textTagDetail.add("Today", df.format(new java.util.Date()));

            textTagDetail.add("EdorNo", mEdorNo);
            textTagDetail.add("GrpContNo", mGrpContNo);
            textTagDetail.add("GrpName", tLCGrpContSchema.getGrpName());
            textTagDetail.add("GrpNo", tLCGrpContSchema.getAppntNo());
            String sum11 = new DecimalFormat("0.00").format(Double.valueOf(
                    String.valueOf(sum1)));
            String sum22 = new DecimalFormat("0.00").format(Double.valueOf(
                    String.valueOf(sum2)));
            String sum44 = new DecimalFormat("0.00").format(Double.valueOf(
                    String.valueOf(sum4)));
            textTagDetail.add("sum1", sum11);
            textTagDetail.add("sum2", sum22);
            textTagDetail.add("sum3", sum3);
            textTagDetail.add("sum4", sum44);

            //加入textTagDetail
            if (textTagDetail.size() > 0) {
                myDocumentDetail = xmlexportDetail.addTextTag(textTagDetail);
            }

            //加入tlistTableDetail
            String strArr[] = new String[17];
            strArr[0] = "num";
            strArr[1] = "Name";
            strArr[2] = "sex";
            strArr[3] = "Birthday";
            strArr[4] = "IDType";
            strArr[5] = "IDNo";
            strArr[6] = "WorkType";
            strArr[7] = "RiskName";
            strArr[8] = "RiskName1";
            strArr[9] = "Prem";
            strArr[10] = "Amnt";
            strArr[11] = "Years";
            strArr[12] = "BnfName";
            strArr[13] = "GetMoney";
            strArr[14] = "Interest";
            strArr[15] = "sum";
            strArr[16] = "";
            xmlexportDetail.addListTable(tlistTableDetail, strArr);
        }

        //打印主批单
        if (!tFlag) {

            buildError("dealData", "发生一个未知错误使主批单不能打印！");
            return false;

            //return true;
        }

        if (textTag.size() > 0) {
            myDocument = xmlexport.addTextTag(textTag);
        }
        //结束标记
        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");
        //结束标记
        xmlexport.addListTable(tEndListTable);

        mResult.clear();

        //生成主打印批单schema
        LPEdorPrintSchema tLPEdorPrintSchemaMain = new LPEdorPrintSchema();
        tLPEdorPrintSchemaMain.setEdorNo(mEdorNo);
        tLPEdorPrintSchemaMain.setManageCom(mGlobalInput.ManageCom);
        tLPEdorPrintSchemaMain.setPrtFlag("N");
        tLPEdorPrintSchemaMain.setPrtTimes(0);
        tLPEdorPrintSchemaMain.setMakeDate(PubFun.getCurrentDate());
        tLPEdorPrintSchemaMain.setMakeTime(PubFun.getCurrentTime());
        tLPEdorPrintSchemaMain.setOperator(mGlobalInput.Operator);
        tLPEdorPrintSchemaMain.setModifyDate(PubFun.getCurrentDate());
        tLPEdorPrintSchemaMain.setModifyTime(PubFun.getCurrentTime());
        InputStream ins = xmlexport.getInputStream();
        xmlexport.outputDocumentToFile("c:\\", "xmlexport");
        tLPEdorPrintSchemaMain.setEdorInfo(ins);
        mMap.put("delete from lpedorprint where edorno='" + mEdorNo + "'",
                 "DELETE");
        mMap.put(tLPEdorPrintSchemaMain, "BLOBINSERT");

        System.out.println("BLOBINSERT");
        //生成变动清单打印schema
        qFlag = false;
        if (qFlag) {
            LPEdorPrint2Schema tLPEdorPrint2SchemaDetail = new
                    LPEdorPrint2Schema();
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLPEdorPrint2SchemaDetail,
                                     tLPEdorPrintSchemaMain);
            InputStream ins1 = xmlexportDetail.getInputStream();
            xmlexportDetail.outputDocumentToFile("c:\\", "PrtGrpEndorsementBL");
            tLPEdorPrint2SchemaDetail.setEdorInfo(ins1);
            mMap.put("delete from lpedorprint2 where edorno='" + mEdorNo + "'",
                     "DELETE");
            mMap.put(tLPEdorPrint2SchemaDetail, "BLOBINSERT");
        }

        mResult.addElement(mMap);
        return true;
    }

    /**
     * 设置批单显示的固定信息
     */
    private void setFixedInfo() {
        //查询受理人
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mEdorNo);
        tLGWorkDB.getInfo();
        textTag.add("ApplyName", tLGWorkDB.getApplyName());

        //查询受理渠道
        if(tLGWorkDB.getAcceptWayNo()!=null){
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("acceptwayno");
            tLDCodeDB.setCode(tLGWorkDB.getAcceptWayNo());
            tLDCodeDB.getInfo();
            textTag.add("AcceptWay", tLDCodeDB.getCodeName());
        }

        //查询受理机构
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(tLGWorkDB.getAcceptorNo());
        tLDUserDB.getInfo();

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLDUserDB.getComCode());
        tLDComDB.getInfo();

        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("ServiceFax", tLDComDB.getFax());
        textTag.add("ServiceAddress", tLDComDB.getLetterServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
    }

    //被保人基本信息变更
		 private boolean getDetailBB(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
		        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		        tLCGrpContDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
		        if (!tLCGrpContDB.getInfo()) {
		            mErrors.addOneError(new CError("相关的集体保单表中没有记录！"));
		            return false;
		        }
		        tLCGrpContSchema = tLCGrpContDB.getSchema();
		        textTag.add("BB_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
		        textTag.add("BB_EdorValiDate",
		                    CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));
		        textTag.add("BB_EdorValue", aLPGrpEdorItemSchema.getGetMoney()) ;
		        return true;
		    }
  private boolean getDetailBJ(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        if (!tLCGrpContDB.getInfo()) {
            mErrors.addOneError(new CError("相关的集体保单表中没有记录！"));
            return false;
        }
        tLCGrpContSchema = tLCGrpContDB.getSchema();
        textTag.add("BJ_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        textTag.add("BJ_EdorValiDate",
                    CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));
        //textTag.add("LR_LostTimes", tLCGrpContSchema.getLostTimes() + 1);
        textTag.add("BJ_EdorValue", (-1)*aLPGrpEdorItemSchema.getGetMoney()) ;
       /* if (aLPGrpEdorItemSchema.getGetMoney() > 0) {
            textTag.add("LR_GetMoney",
                        "因本次变更，投保人需补费" + aLPGrpEdorItemSchema.getGetMoney() +
                        "元。");
        }*/

        return true;
    }

    //被保险人重要资料变更
    private boolean getDetailIC(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        tLPInsuredDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        tLPInsuredDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        LPInsuredSet tLPInsuredSet = new LPInsuredSet();
        tLPInsuredSet.set(tLPInsuredDB.query());
        int n = tLPInsuredSet.size();

        tlistTable = new ListTable(); //设置循环的listTable
        tlistTable.setName("IC");
        LPAddressDB tLPAddressDB = new LPAddressDB();
        LDCodeDB tLDCodeDB = new LDCodeDB();
        for (int i = 1; i <= n; i++) {
            LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
            tLPInsuredSchema = tLPInsuredSet.get(i);

            //取（保全个单被保人表）和（被保人表）的相关记录，生称相应schema
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setInsuredNo(tLPInsuredSchema.getInsuredNo());
            tLCInsuredDB.setContNo(tLPInsuredSchema.getContNo());
            if (!tLCInsuredDB.getInfo()) {
                buildError("getDetailIC", "LCInsured中无相关记录");

            } else {
                tLCInsuredSchema = tLCInsuredDB.getSchema();
            }
            String[] strArr = new String[8];
            tLPAddressDB.setEdorNo(tLPInsuredSchema.getEdorNo());
            tLPAddressDB.setEdorType(tLPInsuredSchema.getEdorType());
            tLPAddressDB.setCustomerNo(tLPInsuredSchema.getInsuredNo());
            tLPAddressDB.setAddressNo(tLPInsuredSchema.getAddressNo());
            strArr[0] = tLPInsuredSchema.getInsuredNo();
            strArr[1] = tLPInsuredSchema.getName();
            strArr[2] = ChangeCodeBL.getCodeName("Sex", tLPInsuredSchema.getSex());
            strArr[3] = tLPInsuredSchema.getBirthday();
            strArr[4] = ChangeCodeBL.getCodeName("idtype",
                                                 tLPInsuredSchema.getIDType());
            strArr[5] = tLPInsuredSchema.getIDNo();
            strArr[6] = "";
            strArr[7] = "";
            if (tLPAddressDB.getInfo()) {
                strArr[6] = "" + tLPAddressDB.getSchema().getPostalAddress();
                strArr[7] = "" + tLPAddressDB.getSchema().getPhone();
            }

            tlistTable.add(strArr);

        }
        String[] strArr1 = new String[8];
        strArr1[0] = "被保险人客户号";
        strArr1[1] = "被保险人姓名";
        strArr1[2] = "性别";
        strArr1[3] = "出生日期";
        strArr1[4] = "证件类型";
        strArr1[5] = "证件号码";
        strArr1[6] = "通讯地址";
        strArr1[7] = "通讯电话";

        xmlexport.addListTable(tlistTable, strArr1);

        textTag.add("IC_EdorValiDate",
                    CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));
        double getMoney = aLPGrpEdorItemSchema.getGetMoney();
        if (getMoney > 0) {
            textTag.add("IC_GetMoney", "因本次变更，投保人共计缴纳保险费" + Math.abs(getMoney) +
                        "元。");
        } else if (getMoney < 0) {

            textTag.add("IC_GetMoney", "因本次变更，共计退还投保人保险费" +
                        String.valueOf(Math.abs(getMoney)) +
                        "元。");
        }
        return true;
    }

    /**
     *
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailWD(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        String sql = "select count(*) from LPInsured " +
                "where EdorNo = '" + edorNo + "' " +
                "and EdorType = '" + edorType + "' " +
                "and GrpContNo = '" + grpContNo + "' ";
        String num = (new ExeSQL()).getOneValue(sql);
        textTag.add("WD_GrpContNo", grpContNo);
        textTag.add("WD_Num", num);
        return true;
    }

    private boolean getDetailWS(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType= aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        String sql = "select count(*) from LPCont " +
                "where EdorNo = '" + edorNo + "' " +
                "and EdorType = '" + edorType + "' " +
                "and GrpContNo = '" + grpContNo + "' " +
                "and PolType <> '1'";
        String num = (new ExeSQL()).getOneValue(sql);
        textTag.add("WS_GrpContNo", grpContNo);
        textTag.add("WS_Num", num);
        return true;
    }
    
    private boolean getDetailUM(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
    	
    	EdorItemSpecialData tSpecialData = new EdorItemSpecialData(mEdorNo,"UM");
        if(!tSpecialData.query())
        {
        	buildError("dealDataUM", "未找到本次被保人信息！");
            return false;
        }
        //设置SpecialData中PolNo为"000000"
        String [] polnos = tSpecialData.getGrpPolNos();
        String polno = polnos[0];
        tSpecialData.setPolNo(polno);
        String idno=new ExeSQL().getOneValue("select idno from lcinsured where  grpcontno='"+mGrpContNo+"' and  insuredno='"+tSpecialData.getEdorValue("InsuredNo")+"'");
//        textTag.add("UMDrawerName", tSpecialData.getEdorValue("InsuredName"));
//        textTag.add("UMDrawerID", idno);
        textTag.add("InsuredIDNo", idno);
        
        //取出最新信息
        textTag.add("InsuredNo", tSpecialData.getEdorValue("InsuredNo"));
        textTag.add("InsuredName", tSpecialData.getEdorValue("InsuredName"));
        textTag.add("GrpSum", tSpecialData.getEdorValue("GrpSum"));
		textTag.add("InsuredSum", tSpecialData.getEdorValue("InsuredSum"));
		textTag.add("Sum", tSpecialData.getEdorValue("Sum"));
		textTag.add("GetDate", tSpecialData.getEdorValue("GetDate"));
		textTag.add("Mode", "一次性领取");
		textTag.add("PayMode", new ExeSQL().getOneValue("select codename from ldcode where codetype='paymode' and code=(select paymode from lcgrpcont where grpcontno='"+mGrpContNo+"')"));
        return true;
    }

    /**
     * 由于其撤保批单内容
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailWT(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        textTag.add("WT_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        textTag.add("WT_EdorValiDate", aLPGrpEdorItemSchema.getEdorValiDate());
        String sql="";
        System.out.println("testesttest");
        if(jugdeUliByGrpContNo()){
        	   sql = "select '', (select riskSeqNo from LCGrpPol  where grpPolNo = a.grpPolNo),b.riskname,c.peoples2,sum(a.prem) from LPPol a, LMRisk b ,lcgrpcont c where a.riskCode = b.riskCode and a.grpcontno=c.grpcontno "
                  + "   and edorNo = '"
                  + aLPGrpEdorItemSchema.getEdorNo() + "' "
                  + "   and edorType ='"
                  + aLPGrpEdorItemSchema.getEdorType() + "' "
                  + "   and c.grpContNo = '"
                  + aLPGrpEdorItemSchema.getGrpContNo() + "' "
                  + "group by  a.grpPolNo, b.riskName,c.peoples2 ";
        }else{
       sql = "select a.contPlanCode, "
                     + "   (select riskSeqNo from LCGrpPol "
                     + "   where grpPolNo = a.grpPolNo), " //险种序号
                     +"   b.riskName,count(distinct a.insuredNo), sum(a.prem) "
                     + "from LPPol a, LMRisk b "
                     + "where a.riskCode = b.riskCode "
                     + "   and edorNo = '"
                     + aLPGrpEdorItemSchema.getEdorNo() + "' "
                     + "   and edorType ='"
                     + aLPGrpEdorItemSchema.getEdorType() + "' "
                     + "   and grpContNo = '"
                     + aLPGrpEdorItemSchema.getGrpContNo() + "' "
                     + "group by a.contPlanCode, a.grpPolNo, b.riskName "
                     + "order by a.contPlanCode ";
        }
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.equals("") || tSSRS.equals("null"))
        {
            CError tError = new CError();
            tError.moduleName = "PrtGrpEndorsementBL";
            tError.functionName = "getDetailWT";
            tError.errorMessage = "生成批单信息时查询保全退费失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }


        ListTable alistTable = new ListTable();
        alistTable.setName("WT");
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String printContent[] = new String[6];
            printContent[0] = tSSRS.GetText(i, 1);
            printContent[1] = tSSRS.GetText(i, 2);
            printContent[2] = tSSRS.GetText(i, 3);
            printContent[3] = tSSRS.GetText(i, 4);
            printContent[4] = aLPGrpEdorItemSchema.getEdorValiDate();
            printContent[5] = tSSRS.GetText(i, 5);

            alistTable.add(printContent);
        }

        String strhead[] = new String[6];
        strhead[0] = "保障计划";
        strhead[1] = "险种序号";
        strhead[2] = "险种名称";
        strhead[3] = "被保人数";
        strhead[4] = "生效日期";
        strhead[5] = "退保金额";
        xmlexport.addListTable(alistTable, strhead);

        String getMoney = "0"; //总退费
        sql = "select abs(sum(getMoney)) "
              + "from LJSGetEndorse "
              + "where endorsementNo = '"
              + aLPGrpEdorItemSchema.getEdorNo() + "' "
              + "   and feeOperationType = '"
              + aLPGrpEdorItemSchema.getEdorType() + "' "
              + "   and grpContNo = '"
              + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        String rs = new ExeSQL().getOneValue(sql);
        if(!rs.equals("") && !rs.equals("null"))
        {
            getMoney = rs;
        }
        textTag.add("WT_GetMoney", CommonBL.bigDoubleToCommonString(Double.parseDouble(getMoney),"0.00"));

        String gbFee = "0";  //工本费
        String gbSql = sql + "  and FeeFinaType = '" + BQ.DETAILTYPE_GB + "' ";
        rs = new ExeSQL().getOneValue(gbSql);
        if(!rs.equals("") && !rs.equals("null"))
        {
            gbFee = rs;
        }
        textTag.add("WT_GB", CommonBL.bigDoubleToCommonString(Double.parseDouble(gbFee),"0.00"));

        String tjFee = "0";
        textTag.add("WT_TJ", CommonBL.bigDoubleToCommonString(Double.parseDouble(tjFee),"0.00"));

        //判断是整单退还是退险种，处理方式不一样
        sql = "select count(distinct contPlanCode) "
              + "from LCPol "
              + "where grpContNo = '"
              + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        rs = new ExeSQL().getOneValue(sql); //保单保障计划数
        if(rs.equals("") || rs.equals("null"))
        {
            CError tError = new CError();
            tError.moduleName = "PrtGrpEndorsementBL";
            tError.functionName = "getDetailWT";
            tError.errorMessage = "查询保单保障计划失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        int contPlanCount = Integer.parseInt(rs);

        LPContPlanRiskDB tLPContPlanRiskDB = new LPContPlanRiskDB();
        tLPContPlanRiskDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        tLPContPlanRiskDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        tLPContPlanRiskDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        LPContPlanRiskSet set = tLPContPlanRiskDB.query();

        String head = null;
//        if(contPlanCount == set.size())
//        {
            head = "保单" + aLPGrpEdorItemSchema.getGrpContNo()
                   + "办理犹豫期撤保，保单效力自始无效，扣除体检费" + CommonBL.bigDoubleToCommonString(Double.parseDouble(tjFee),"0.00")
                   + "元，扣除撤保成本费" + CommonBL.bigDoubleToCommonString(Double.parseDouble(gbFee),"0.00") + "元，共退还保险金"
                   + CommonBL.bigDoubleToCommonString(Double.parseDouble(getMoney),"0.00") + "元。";
//        }
//        else
//        {
            head = "保单" + aLPGrpEdorItemSchema.getGrpContNo()
                   + "以下险种办理犹豫期撤保，险种效力自始无效，扣除体检费"
                   + CommonBL.bigDoubleToCommonString(Double.parseDouble(tjFee),"0.00") + "元，工本费"+ CommonBL.bigDoubleToCommonString(Double.parseDouble(gbFee),"0.00") + "元，共退还保险金"
                   + CommonBL.bigDoubleToCommonString(Double.parseDouble(getMoney),"0.00") + "元。";
//        }
        textTag.add("WT_Head", head);
        createAppAccList(aLPGrpEdorItemSchema);
        return true;
    }

    private boolean getDetailXT(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();

        ExeSQL tExeSQL = new ExeSQL();
        String sql = "select distinct PolNo "
                     + "from LPEdorEspecialData "
                     + "where EdorNo = '"
                     + aLPGrpEdorItemSchema.getEdorNo() + "' "
                     + "   and EdorType = '"
                     + aLPGrpEdorItemSchema.getEdorType() + "' "
                     + "order by PolNo ";
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if(tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "PrtGrpEndorsementBL";
            tError.functionName = "getDetailXT";
            tError.errorMessage = "查询退保数据出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if(tSSRS.getMaxRow() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PrtGrpEndorsementBL";
            tError.functionName = "getDetailXT";
            tError.errorMessage = "没有查询到退保数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        tlistTable = new ListTable();
        tlistTable.setName("XTG");

        double getMoney = 0;
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String[] temp = tSSRS.GetText(i, 1).split(",");
            String contPlanCode = temp[0];
            String riskCode = temp[1];
            //20081223 zhanggm 590206、190106的正常解约金额直接显示实交保费金额
            if("590206".equals(riskCode)||"190106".equals(riskCode))
            {
            	sql = "select RiskSeqNo, "
                    + "  (select RiskName from LMRiskApp "
                    + "  where RiskCode = a.RiskCode), "
                    + "(select sum(b.SumActuPayMoney) "
          	        + "  from LJAPayPerson b, LCPol c "
          	        + "  where b.PolNo = c.PolNo "
          	        + "   and b.GrpContNo = a.GrpContNo "
          	        + "   and c.ContPlanCode = '" + contPlanCode + "' "
          	        + "   and c.RiskCode = a.RiskCode )"
                    + "from LCGrpPol a "
                    + "where a.GrpContNo = '" + grpContNo + "' "
                    + "   and RiskCode = '" + riskCode + "' ";

            }
            else
            {
            	sql = "select RiskSeqNo, "
                    + "  (select RiskName from LMRiskApp "
                    + "  where RiskCode = a.RiskCode), "
                    + "  (select abs(sum(GetMoney)) from LPBudgetResult "
                    + "  where EdorNo = '" + edorNo + "' "
                    + "     and EdorType = '" + edorType + "' "
                    + "     and GrpContNo = '" + grpContNo + "' "
                    + "     and ContPlanCode = '" + contPlanCode + "' "
                    + "     and RiskCode = '" + riskCode + "') "
                    + "from LCGrpPol a "
                    + "where a.GrpContNo = '" + grpContNo + "' "
                    + "   and RiskCode = '" + riskCode + "' ";
            }

            SSRS aSSRS = tExeSQL.execSQL(sql);
            if(aSSRS.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "PrtGrpEndorsementBL";
                tError.functionName = "getDetailXT";
                tError.errorMessage = "查询退保数据出错" + contPlanCode;
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            if(aSSRS.getMaxRow() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "PrtGrpEndorsementBL";
                tError.functionName = "getDetailXT";
                tError.errorMessage = "没有查询到退保数据" + contPlanCode;
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            String[] info = new String[7];
            info[0] = contPlanCode;
            info[1] = aSSRS.GetText(1, 1);
            info[2] = riskCode;
            info[3] = aSSRS.GetText(1, 2);
            info[4] = CommonBL.bigDoubleToCommonString(
                Double.parseDouble(aSSRS.GetText(1, 3)), "0.00");

            String xtMoney = getSpecialData(aLPGrpEdorItemSchema,
                                     contPlanCode + "," + riskCode,
                                     BQ.DETAILTYPE_XTFEEG);
            xtMoney = xtMoney.substring(xtMoney.indexOf("-") + 1);
            info[5] = CommonBL.bigDoubleToCommonString(
                Double.parseDouble(xtMoney), "0.00");

            String rate = getSpecialData(aLPGrpEdorItemSchema,
                                     contPlanCode + "," + riskCode,
                                     BQ.XTFEERATEG);
            info[6] = rate;
            if(!info[6].equals("-"))
            {
                info[6] = CommonBL.bigDoubleToCommonString(
                    Double.parseDouble(rate), "0.0000");
            }
            tlistTable.add(info);

            getMoney += Double.parseDouble(info[5]);
        }

        String[] title = {"保障划划","险种序号","险种代码","险种名称","正常退保金",
                         "协议解约金","协议解约比例"};
        xmlexport.addListTable(tlistTable, title);

        String head = "";

        LPGrpContDB tLPGrpContDB = new LPGrpContDB();
        tLPGrpContDB.setEdorNo(edorNo);
        tLPGrpContDB.setEdorType(edorType);
        tLPGrpContDB.setGrpContNo(grpContNo);
        if(tLPGrpContDB.getInfo())
        {
            head = "保单号" + grpContNo + "办理协议解约，生效日期"
                   + CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate())
                   + "，保单效力自始无效，应退协议解约金额"
                   + CommonBL.bigDoubleToCommonString(getMoney, "0.00") + "元。";
        }
        else
        {
            head = "保单号" + grpContNo + "以下险种办理协议解约，生效日期"
                   + CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate())
                   + "，险种效力自始无效，应退协议解约金额"
                   + CommonBL.bigDoubleToCommonString(getMoney, "0.00")+ "元。";
        }

        textTag.add("XTG_Head", head);
        textTag.add("XTG_GetMoney", getMoney);
        textTag.add("XTG_EdorValiDate", aLPGrpEdorItemSchema.getEdorValiDate());

        createAppAccList(aLPGrpEdorItemSchema);
        return true;
    }
    
    //团体万能协议退保批单
    private boolean getDetailTG(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();

        ExeSQL tExeSQL = new ExeSQL();
        String sql = "select detailType,edorValue "
                     + "from LPEdorEspecialData "
                     + "where EdorNo = '"
                     + aLPGrpEdorItemSchema.getEdorNo() + "' "
                     + "   and EdorType = '"
                     + aLPGrpEdorItemSchema.getEdorType() + "' "
                     + " with ur ";
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if(tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "PrtGrpEndorsementBL";
            tError.functionName = "getDetailTG";
            tError.errorMessage = "查询退保数据出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if(tSSRS.getMaxRow() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PrtGrpEndorsementBL";
            tError.functionName = "getDetailTG";
            tError.errorMessage = "没有查询到退保数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        tlistTable = new ListTable();
        tlistTable.setName("TG");
        double getMoney = 0;
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {

            String xtFee = CommonBL.bigDoubleToCommonString(
                Double.parseDouble(tSSRS.GetText(i, 2)), "0.00");
            if("XTFEE_G".equals(tSSRS.GetText(i, 1))){
            	textTag.add("XTFEE_G", xtFee);
            }else if("XTFEE_P1".equals(tSSRS.GetText(i, 1)))
            {
            	textTag.add("XTFEE_P1", xtFee);
            }else if("XTFEE_P2".equals(tSSRS.GetText(i, 1)))
            {
            	textTag.add("XTFEE_P2", xtFee);
            }else{
            	continue;
            }
            getMoney += Double.parseDouble(xtFee);
        }

        String head = "";
        LPGrpContDB tLPGrpContDB = new LPGrpContDB();
        tLPGrpContDB.setEdorNo(edorNo);
        tLPGrpContDB.setEdorType(edorType);
        tLPGrpContDB.setGrpContNo(grpContNo);
        if(tLPGrpContDB.getInfo())
        {
            head = "保单号" + grpContNo + "办理协议解约，生效日期"
                   + CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate())
                   + "，保单效力自始无效，应退协议解约金额"
                   + CommonBL.bigDoubleToCommonString(getMoney, "0.00") + "元。";
        }
        else
        {
            head = "保单号" + grpContNo + "以下险种办理协议解约，生效日期"
                   + CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate())
                   + "，险种效力自始无效，\n应退协议解约金额"
                   + CommonBL.bigDoubleToCommonString(getMoney, "0.00")+ "元。";
        }
        
        textTag.add("TG_Head", head);
        textTag.add("TG_GetMoney", CommonBL.bigDoubleToCommonString(getMoney, "0.00"));
        textTag.add("TG_EdorValiDate", aLPGrpEdorItemSchema.getEdorValiDate());

        return true;
    }

    /**
     * 查询特殊数据信息
     * @param tLPGrpEdorItemSchema LPGrpEdorItemSchema：保全项目
     * @param polNo String：险种号
     * @param detailType String：类型
     * @return String
     */
    private String getSpecialData(LPGrpEdorItemSchema tLPGrpEdorItemSchema,
                                  String polNo, String detailType)
    {
        EdorItemSpecialData tEdorItemSpecialData
            = new EdorItemSpecialData(tLPGrpEdorItemSchema);
        tEdorItemSpecialData.setPolNo(polNo);
        if(!tEdorItemSpecialData.query())
        {
            return null;
        }
        return tEdorItemSpecialData.getEdorValue(detailType);
    }

    private boolean getDetailRT(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        int decreaseCount = 0;
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        tLPEdorItemDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        tLPEdorItemDB.setEdorAcceptNo(aLPGrpEdorItemSchema.getEdorAcceptNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemDB.mErrors.needDealError()) {
            CError.buildErr(this, "查询减人项目人数时失败");
            return false;
        }
        tlistTable = new ListTable();
        tlistTable.setName("RT");

        decreaseCount = tLPEdorItemSet.size();
        for (int i = 0; i < tLPEdorItemSet.size(); i++) {
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i + 1);
            LPPolSchema tLPPolSchema = new LPPolSchema();
            LPPolDB tLPPolDB = new LPPolDB();
            LMRiskDB tLMRiskDB = new LMRiskDB();
            LMRiskSchema tLMRiskSchema = new LMRiskSchema();
            tLPPolDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPPolDB.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPPolDB.setPolNo(tLPEdorItemSchema.getPolNo());
            if (!tLPPolDB.getInfo()) {
                CError.buildErr(this, "查询LPOL信息失败sc");
                return false;
            }
            tLPPolSchema = tLPPolDB.getSchema();

            tLMRiskDB.setRiskCode(tLPPolSchema.getRiskCode());
            if (!tLMRiskDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "PrtAppEndorsementBL";
                tError.functionName = "getInsuredCM";
                tError.errorMessage = "LMRisk不存在相应的记录!sc";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLMRiskSchema = tLMRiskDB.getSchema();
            String[] strArr = new String[5];
            strArr[0] = StrTool.cTrim(tLPEdorItemSchema.getInsuredNo());
            strArr[1] = StrTool.cTrim(tLPPolSchema.getInsuredName());
            strArr[2] = tLMRiskSchema.getRiskCode();
            strArr[3] = tLMRiskSchema.getRiskName();
            strArr[4] = Double.toString(tLPEdorItemSchema.getGetMoney());
            tlistTable.add(strArr);
        } //end for
        String[] strArr1 = new String[5];
        strArr1[0] = "被保人编号";
        strArr1[1] = "被保人姓名";
        strArr1[2] = "险种编号";
        strArr1[3] = "险种名称";
        strArr1[4] = "应补/退费";

        xmlexport.addListTable(tlistTable, strArr1);
      textTag.add("RT_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        textTag.add("RT_EdorValiDate",
                    CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));
        return true;

    }

    private boolean getDetailAC(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        LPGrpAppntDB tLPGrpAppntDB = new LPGrpAppntDB();
        tLPGrpAppntDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        tLPGrpAppntDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
//        tLPGrpAppntDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        LPGrpAppntSet tLPGrpAppntSet = new LPGrpAppntSet();
        tLPGrpAppntSet.set(tLPGrpAppntDB.query());
        if (tLPGrpAppntSet==null||tLPGrpAppntSet.size()==0) {
            buildError("getDetailAC", "保全团单投保人表LPGrpAppnt中无相关批单号的记录");
            return false;
        }
        String ACGrpContNo = " ";
        for(int i=1 ;i<=tLPGrpAppntSet.size();i++)
        {
            ACGrpContNo += tLPGrpAppntSet.get(i).getGrpContNo()+" ";
        }
        textTag.add("AC_GrpContNo", ACGrpContNo);
        tLPGrpAppntDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        LPGrpAppntSchema tLPGrpAppntSchema = new LPGrpAppntSchema();
        if(tLPGrpAppntDB.getInfo())
        {
        	tLPGrpAppntSchema = tLPGrpAppntDB.getSchema();
        }
        else
        {
        	tLPGrpAppntSchema = tLPGrpAppntSet.get(1);
        }


        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(tLPGrpAppntSchema.getGrpContNo());
        if (!tLCGrpAppntDB.getInfo()) {
            buildError("getDetailAC", "团单投保人表LCGrpAppnt中无相关集体合同号的记录");
            return false;
        }
        LCGrpAppntSchema tLCGrpAppntSchema = tLCGrpAppntDB.getSchema();

        LDGrpDB tLDGrpDB = new LDGrpDB();
        tLDGrpDB.setCustomerNo(tLPGrpAppntSchema.getCustomerNo());
        if (!tLDGrpDB.getInfo()) {
            buildError("getDetailAC", "团体客户表LCGrp中无相关集体合同号的记录");
            return false;
        }
        LDGrpSchema tLDGrpSchema = tLDGrpDB.getSchema();

        LPGrpSchema tLPGrpSchema = new LPGrpSchema();
        LPGrpDB tLPGrpDB = new LPGrpDB();
        tLPGrpDB.setEdorNo(tLPGrpAppntSchema.getEdorNo());
        tLPGrpDB.setEdorType(tLPGrpAppntSchema.getEdorType());
        tLPGrpDB.setCustomerNo(tLPGrpAppntSchema.getCustomerNo());
        if (!tLPGrpDB.getInfo()) { //可能修改后的地址用的是原来LCGrpAddress中的地址，所以在LPGrpAddress中没有记录
            //尝试从LCGrpAddress中取出此地址记录
            tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(tLPGrpAppntSchema.getCustomerNo());
            if (!tLDGrpDB.getInfo()) {
                buildError("getDetailAC", "保全团体客户地址表LPGrpAddress中无相关集体合同号的记录");
                return false;
            }
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLPGrpSchema,
                                     tLDGrpDB.getSchema());
        } else {
            tLPGrpSchema = tLPGrpDB.getSchema();
        }

        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        tLCGrpAddressDB.setCustomerNo(tLCGrpAppntSchema.getCustomerNo());
        tLCGrpAddressDB.setAddressNo(tLCGrpAppntSchema.getAddressNo());
        if (!tLCGrpAddressDB.getInfo()) {
            buildError("getDetailAC", "团体客户地址表LCGrpAddress中无相关集体合同号的记录");
            return false;
        }
        LCGrpAddressSchema tLCGrpAddressSchema = tLCGrpAddressDB.getSchema();

        LPGrpAddressSchema tLPGrpAddressSchema = new LPGrpAddressSchema();
        LPGrpAddressDB tLPGrpAddressDB = new LPGrpAddressDB();
        tLPGrpAddressDB.setEdorNo(tLPGrpAppntSchema.getEdorNo());
        tLPGrpAddressDB.setEdorType(tLPGrpAppntSchema.getEdorType());
        tLPGrpAddressDB.setCustomerNo(tLPGrpAppntSchema.getCustomerNo());
        tLPGrpAddressDB.setAddressNo(tLPGrpAppntSchema.getAddressNo());

        if (!tLPGrpAddressDB.getInfo()) { //可能修改后的地址用的是原来LCGrpAddress中的地址，所以在LPGrpAddress中没有记录
            //尝试从LCGrpAddress中取出此地址记录
            tLCGrpAddressDB = new LCGrpAddressDB();
            tLCGrpAddressDB.setCustomerNo(tLPGrpAppntSchema.getCustomerNo());
            tLCGrpAddressDB.setAddressNo(tLPGrpAppntSchema.getAddressNo());
            if (!tLCGrpAddressDB.getInfo()) {
                buildError("getDetailAC", "保全团体客户地址表LPGrpAddress中无相关集体合同号的记录");
                return false;
            }
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLPGrpAddressSchema,
                                     tLCGrpAddressDB.getSchema());
        } else {
            tLPGrpAddressSchema = tLPGrpAddressDB.getSchema();
        }

        String[] strArr = new String[1];
        tlistTable = new ListTable();
        tlistTable.setName("AC");
        if (!StrTool.cTrim(tLDGrpSchema.getGrpName()).equals(StrTool.
                cTrim(
                        tLPGrpSchema.getGrpName()))) {
            strArr = new String[1];
            strArr[0] = "投保团体名称：" + tLDGrpSchema.getGrpName() +
                        "\n变为：" + tLPGrpSchema.getGrpName();
            tlistTable.add(strArr);
        }

        //税务登记证号码
        if (!StrTool.cTrim(tLCGrpAppntSchema.getTaxNo()).equals(StrTool.
                cTrim(tLPGrpAppntSchema.getTaxNo()))) {
            strArr = new String[1];
            strArr[0] = "税务登记证号码：" + CommonBL.notNull(tLCGrpAppntSchema.getTaxNo()) +
                        "，变为：" + CommonBL.notNull(tLPGrpAppntSchema.getTaxNo());
            tlistTable.add(strArr);
        }
        
        //法人姓名
        if (!StrTool.cTrim(tLCGrpAppntSchema.getLegalPersonName()).equals(StrTool.
                cTrim(tLPGrpAppntSchema.getLegalPersonName()))) {
            strArr = new String[1];
            strArr[0] = "法人姓名：" + CommonBL.notNull(tLCGrpAppntSchema.getLegalPersonName()) +
                        "，变为：" + CommonBL.notNull(tLPGrpAppntSchema.getLegalPersonName());
            tlistTable.add(strArr);
        }
        
        //法人身份证号
        if (!StrTool.cTrim(tLCGrpAppntSchema.getLegalPersonIDNo()).equals(StrTool.
                cTrim(tLPGrpAppntSchema.getLegalPersonIDNo()))) {
            strArr = new String[1];
            strArr[0] = "法人身份证号：" + CommonBL.notNull(tLCGrpAppntSchema.getLegalPersonIDNo()) +
                        "，变为：" + CommonBL.notNull(tLPGrpAppntSchema.getLegalPersonIDNo());
            tlistTable.add(strArr);
        }
        
        //投保单位电话
        if (!StrTool.cTrim(tLDGrpSchema.getPhone()).equals(StrTool.
                cTrim(tLPGrpSchema.getPhone()))) {
            strArr = new String[1];
            strArr[0] = "单位联系电话：" + CommonBL.notNull(tLDGrpSchema.getPhone()) +
                        "，变为：" + CommonBL.notNull(tLPGrpSchema.getPhone());
            tlistTable.add(strArr);
        }

        //比较地址是否变化
        if (!StrTool.cTrim(tLCGrpAddressSchema.getGrpAddress()).equals(StrTool.
                cTrim(
                        tLPGrpAddressSchema.getGrpAddress()))) {
            strArr = new String[1];
            strArr[0] = "投保单位地址：" +
                        CommonBL.notNull(tLCGrpAddressSchema.getGrpAddress()) +
                        "\n变为：" +
                        CommonBL.notNull(tLPGrpAddressSchema.getGrpAddress());
            tlistTable.add(strArr);
        }

        //比较邮编
        if (!StrTool.cTrim(tLCGrpAddressSchema.getGrpZipCode()).equals(StrTool.
                cTrim(tLPGrpAddressSchema.getGrpZipCode()))) {
            strArr = new String[1];
            strArr[0] = "邮政编码：" +
                        CommonBL.notNull(tLCGrpAddressSchema.getGrpZipCode()) +
                        "，变为：" +
                        CommonBL.notNull(tLPGrpAddressSchema.getGrpZipCode());
            tlistTable.add(strArr);
        }

        //比较联系人1
        if (!StrTool.cTrim(tLCGrpAddressSchema.getLinkMan1()).equals(StrTool.
                cTrim(tLPGrpAddressSchema.getLinkMan1()))) {
            strArr = new String[1];
            strArr[0] = "联系人姓名：" +
                        CommonBL.notNull(tLCGrpAddressSchema.getLinkMan1()) +
                        "，变为：" +
                        CommonBL.notNull(tLPGrpAddressSchema.getLinkMan1());
            tlistTable.add(strArr);
        }

        //比较联系电话1是否变化
        if (!StrTool.cTrim(tLCGrpAddressSchema.getPhone1()).equals(StrTool.
                cTrim(
                        tLPGrpAddressSchema.getPhone1()))) {
            strArr = new String[1];
            strArr[0] = "联系人联系电话：" +
                        CommonBL.notNull(tLCGrpAddressSchema.getPhone1()) +
                        "，变为：" + CommonBL.notNull(tLPGrpAddressSchema.getPhone1());
            tlistTable.add(strArr);
        }

        //比较传真1是否变化
        if (!StrTool.cTrim(tLCGrpAddressSchema.getFax1()).equals(StrTool.
                cTrim(
                        tLPGrpAddressSchema.getFax1()))) {
            strArr = new String[1];
            strArr[0] = "联系人传真：" + CommonBL.notNull(tLCGrpAddressSchema.getFax1()) +
                        "，变为：" + CommonBL.notNull(tLPGrpAddressSchema.getFax1());
            tlistTable.add(strArr);
        }

        //比较Email是否变化
        if (!StrTool.cTrim(tLCGrpAddressSchema.getE_Mail1()).equals(StrTool.
                cTrim(
                        tLPGrpAddressSchema.getE_Mail1()))) {
            strArr = new String[1];
            strArr[0] = "联系人电子邮箱：" +
                        CommonBL.notNull(tLCGrpAddressSchema.getE_Mail1()) +
                        "，变为：" +
                        CommonBL.notNull(tLPGrpAddressSchema.getE_Mail1());
            tlistTable.add(strArr);
        }
        
        //比较证件类型是否变化
        if (!StrTool.cTrim(tLCGrpAppntSchema.getIDType()).equals(StrTool.
                cTrim(
                		tLPGrpAppntSchema.getIDType()))) {
            strArr = new String[1];
            String idtype1 = "";
            String idtype2 = "";
            if("0".equals(tLCGrpAppntSchema.getIDType())){
            	idtype1 = "身份证";
            }
            if("4".equals(tLCGrpAppntSchema.getIDType())){
            	idtype1 = "其他";
            }
            if("0".equals(tLPGrpAppntSchema.getIDType())){
            	idtype2 = "身份证";
            }
            if("4".equals(tLPGrpAppntSchema.getIDType())){
            	idtype2 = "其他";
            }
            strArr[0] = "联系人证件类型：" +
                        CommonBL.notNull(idtype1) +
                        "，变为：" +
                        CommonBL.notNull(idtype2);
            tlistTable.add(strArr);
        }
        
        //比较证件号码是否变化
        if (!StrTool.cTrim(tLCGrpAppntSchema.getIDNo()).equals(StrTool.
                cTrim(
                		tLPGrpAppntSchema.getIDNo()))) {
            strArr = new String[1];
            strArr[0] = "联系人证件号码：" +
                        CommonBL.notNull(tLCGrpAppntSchema.getIDNo()) +
                        "，变为：" +
                        CommonBL.notNull(tLPGrpAppntSchema.getIDNo());
            tlistTable.add(strArr);
        }
        
        //比较证件生效日期是否变化
        if (!StrTool.cTrim(tLCGrpAppntSchema.getIDStartDate()).equals(StrTool.
                cTrim(
                		tLPGrpAppntSchema.getIDStartDate()))) {
            strArr = new String[1];
            strArr[0] = "联系人证件生效日期：" +
                        CommonBL.notNull(tLCGrpAppntSchema.getIDStartDate()) +
                        "，变为：" +
                        CommonBL.notNull(tLPGrpAppntSchema.getIDStartDate());
            tlistTable.add(strArr);
        }
        
        //比较证件失效日期是否变化
        if (!StrTool.cTrim(tLCGrpAppntSchema.getIDEndDate()).equals(StrTool.
                cTrim(
                		tLPGrpAppntSchema.getIDEndDate()))) {
            strArr = new String[1];
            strArr[0] = "联系人证件失效日期：" +
                        CommonBL.notNull(tLCGrpAppntSchema.getIDEndDate()) +
                        "，变为：" +
                        CommonBL.notNull(tLPGrpAppntSchema.getIDEndDate());
            tlistTable.add(strArr);
        }
        
        //比较证件长期有效状态是否变化
        if (!StrTool.cTrim(tLCGrpAppntSchema.getIDLongEffFlag()).equals(StrTool.
                cTrim(
                		tLPGrpAppntSchema.getIDLongEffFlag()))) {
            strArr = new String[1];
            String IDLongEffFlag1 = "";
            String IDLongEffFlag2 = "";
            if("Y".equals(tLCGrpAppntSchema.getIDLongEffFlag())){
            	IDLongEffFlag1 = "长期有效";
            } else {
            	IDLongEffFlag1 = "非长期有效";
            }
            if("Y".equals(tLPGrpAppntSchema.getIDLongEffFlag())){
            	IDLongEffFlag2 = "长期有效";
            } else {
            	IDLongEffFlag2 = "非长期有效";
            }
            strArr[0] = "联系人证件状态：" +
                        CommonBL.notNull(IDLongEffFlag1) +
                        "，变为：" +
                        CommonBL.notNull(IDLongEffFlag2);
            tlistTable.add(strArr);
        }
        
        //比较手机1是否变化
        if (!StrTool.cTrim(tLCGrpAddressSchema.getMobile1()).equals(StrTool.
                cTrim(
                        tLPGrpAddressSchema.getMobile1()))) {
            strArr = new String[1];
            strArr[0] = "联系人手机：" +
                        CommonBL.notNull(tLCGrpAddressSchema.getMobile1()) +
                        "，变为：" +
                        CommonBL.notNull(tLPGrpAddressSchema.getMobile1());
            tlistTable.add(strArr);
        }

        //行业性质
        if (!StrTool.cTrim(tLDGrpSchema.getBusinessType()).equals(StrTool.
                cTrim(
                        tLPGrpSchema.getBusinessType()))) {
            strArr = new String[1];
            strArr[0] = "行业性质：" +
                        ChangeCodeBL.getCodeName("BusinessType",
                                                 tLDGrpSchema.getBusinessType()) +
                        "（行业编码：" + tLDGrpSchema.getBusinessType() +
                        "）\n变为：" +
                        ChangeCodeBL.getCodeName("BusinessType",
                                                 tLPGrpSchema.getBusinessType()) +
                        "（行业编码：" + tLPGrpSchema.getBusinessType() + "）";
            tlistTable.add(strArr);
        }

        //企业类型
        if (!StrTool.cTrim(tLDGrpSchema.getGrpNature()).equals(StrTool.
                cTrim(
                        tLPGrpSchema.getGrpNature()))) {
            strArr = new String[1];
            strArr[0] = "企业类型：" +
                        ChangeCodeBL.getCodeName("GrpNature",
                                                 tLDGrpSchema.getGrpNature()) +
                        "（企业类型编码：" + tLDGrpSchema.getGrpNature() +
                        "），变为：" + ChangeCodeBL.getCodeName("GrpNature",
                    tLPGrpSchema.getGrpNature()) +
                        "（企业类型编码：" + tLPGrpSchema.getGrpNature() + "）";
            tlistTable.add(strArr);
        }

        //员工总人数
        if (tLDGrpSchema.getPeoples() != tLPGrpSchema.getPeoples()) {
            strArr = new String[1];
            strArr[0] = "员工总人数：" + tLDGrpSchema.getPeoples() +
                        "，变为：" + tLPGrpSchema.getPeoples();
            tlistTable.add(strArr);
        }

        //在职人数
        if (tLDGrpSchema.getOnWorkPeoples() != tLPGrpSchema.getOnWorkPeoples()) {
            strArr = new String[1];
            strArr[0] = "在职人数：" + tLDGrpSchema.getOnWorkPeoples() +
                        "，变为：" + tLPGrpSchema.getOnWorkPeoples();
            tlistTable.add(strArr);
        }

        //退休人数
        if (tLDGrpSchema.getOffWorkPeoples() != tLPGrpSchema.getOffWorkPeoples()) {
            strArr = new String[1];
            strArr[0] = "退休人数：" + tLDGrpSchema.getOffWorkPeoples() +
                        "，变为：" + tLPGrpSchema.getOffWorkPeoples();
            tlistTable.add(strArr);
        }

        //其它人员数
        if (tLDGrpSchema.getOtherPeoples() != tLPGrpSchema.getOtherPeoples()) {
            strArr = new String[1];
            strArr[0] = "其它人员数：" + tLDGrpSchema.getOtherPeoples() +
                        "，变为：" + tLPGrpSchema.getOtherPeoples();
            tlistTable.add(strArr);
        }

        String[] strArrHead = new String[1];
        strArrHead[0] = "投保单位资料变更";
        xmlexport.addListTable(tlistTable, strArrHead);
        return true;
    }

    /**
     * 无名单增人
     * @param tLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailWZ(LPGrpEdorItemSchema tLPGrpEdorItemSchema)
    {
        tlistTable = new ListTable();
        tlistTable.setName("WZ");

        StringBuffer sql = new StringBuffer();
        sql.append("select a.contPlanCode, a.noNamePeoples, a.edorPrem, ")
            .append("   a.edorValiDate, b.peoples, b.prem ")
            .append("from LCInsuredList a, LPCont b ")
            .append("where a.edorNo = b.edorNo ")
            .append("   and a.contNo = b.contNo ")
            .append("   and a.edorNo = '")
            .append(tLPGrpEdorItemSchema.getEdorAcceptNo())
            .append("'  and b.edorType = '")
            .append(tLPGrpEdorItemSchema.getEdorType())
            .append("'  and a.grpContNo = '")
            .append(tLPGrpEdorItemSchema.getGrpContNo()).append("' ");
        System.out.println(sql.toString());
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql.toString());
        if(tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("没有查询到无名单增人信息");
            return false;
        }

        int peoples2Add = 0;  //本次增加人数
        double sumPremAdd = 0;   //多保障总交费
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            peoples2Add += Integer.parseInt(tSSRS.GetText(i, 2));
            sumPremAdd += Double.parseDouble(tSSRS.GetText(i, 3));

            String[] strArr = new String[1];
            strArr[0] = "计划" + tSSRS.GetText(i, 1) + "增加"
                        + tSSRS.GetText(i, 2) + "人，应交保费"
                        + CommonBL.bigDoubleToCommonString(
                            Double.parseDouble(tSSRS.GetText(i, 3)), "0.00")
                        + "元，生效日期"
                        + CommonBL.decodeDate(tSSRS.GetText(i, 4))
                        + "，该保障计划人数变更为" + tSSRS.GetText(i, 5)
                        + "人，保费变更为"
                        + CommonBL.bigDoubleToCommonString(
                            Double.parseDouble(tSSRS.GetText(i, 6)), "0.00")
                        + "元。";
            tlistTable.add(strArr);
        }
        xmlexport.addListTable(tlistTable, new String[1]);

        textTag.add("WZ_GrpContNo", tLPGrpEdorItemSchema.getGrpContNo());
        textTag.add("WZ_Peoples2", peoples2Add);
        textTag.add("WZ_SumPrem",
                    CommonBL.bigDoubleToCommonString(sumPremAdd, "0.00"));

//        SSRS aSSRS = getNoNameGrpContPeoples2AndPrem(tLPGrpEdorItemSchema
//                                                     .getGrpContNo());
//        if(aSSRS == null)
//        {
//            return false;
//        }
        //增人后人数
//        textTag.add("WZ_SumPeoples2",
//                    String.valueOf(Integer.parseInt(aSSRS.GetText(1, 1))
//                                   + peoples2Add));
//        //增人后期交保费
//        textTag.add("WZ_Prem",
//                    CommonBL.bigDoubleToCommonString(
//                        Double.parseDouble(aSSRS.GetText(1, 2)) + sumPremAdd,
//                        "0.00"));

        return true;
    }

    /**
         * 无名单减人
         * @param tLPGrpEdorItemSchema LPGrpEdorItemSchema
         * @return boolean
         */
        private boolean getDetailWJ(LPGrpEdorItemSchema tLPGrpEdorItemSchema)
        {
            tlistTable = new ListTable();
            tlistTable.setName("WJ");

            StringBuffer sql = new StringBuffer();
            sql.append("select a.contPlanCode, abs(int(a.noNamePeoples)), ")
                .append("   abs(double(a.edorPrem)), a.edorValiDate, ")
                .append("   b.peoples, b.prem ")
                .append("from LCInsuredList a, LPCont b ")
                .append("where a.edorNo = b.edorNo ")
                .append("   and a.contNo = b.contNo ")
                .append("   and a.edorNo = '")
                .append(tLPGrpEdorItemSchema.getEdorAcceptNo())
                .append("'  and b.edorType = '")
                .append(tLPGrpEdorItemSchema.getEdorType())
                .append("'  and a.grpContNo = '")
                .append(tLPGrpEdorItemSchema.getGrpContNo()).append("' ");

            System.out.println(sql.toString());
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(sql.toString());
            if(tSSRS.getMaxRow() == 0)
            {
                mErrors.addOneError("没有查询到无名单增人信息");
                return false;
            }

            int peoples2Add = 0;  //本次增加人数
            double sumPremAdd = 0;   //多保障总交费
            for(int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                peoples2Add += Integer.parseInt(tSSRS.GetText(i, 2));
                sumPremAdd += Double.parseDouble(tSSRS.GetText(i, 3));

                String[] strArr = new String[1];
                strArr[0] = "计划" + tSSRS.GetText(i, 1) + "减少"
                            + tSSRS.GetText(i, 2) + "人，应退保费"
                            + CommonBL.bigDoubleToCommonString(
                                Double.parseDouble(tSSRS.GetText(i, 3)), "0.00")
                            + "元，生效日期"
                            + CommonBL.decodeDate(tSSRS.GetText(i, 4))
                            + "，该保障计划人数变更为" + tSSRS.GetText(i, 5)
                            + "人，保费变更为"
                            + CommonBL.bigDoubleToCommonString(
                                Double.parseDouble(tSSRS.GetText(i, 6)), "0.00")
                            + "元。";
                tlistTable.add(strArr);
            }
            xmlexport.addListTable(tlistTable, new String[1]);

            textTag.add("WJ_GrpContNo", tLPGrpEdorItemSchema.getGrpContNo());
            textTag.add("WJ_Peoples2", peoples2Add);
            textTag.add("WJ_SumPrem",
                        CommonBL.bigDoubleToCommonString(sumPremAdd, "0.00"));

//            SSRS aSSRS = getNoNameGrpContPeoples2AndPrem(tLPGrpEdorItemSchema
//                                                         .getGrpContNo());
//            if(aSSRS == null)
//            {
//                return false;
//            }
            //减人后人数
//            textTag.add("WJ_SumPeoples2",
//                        String.valueOf(Integer.parseInt(aSSRS.GetText(1, 1))
//                                       - peoples2Add));
            //减人后期交保费
//            textTag.add("WJ_Prem",
//                        CommonBL.bigDoubleToCommonString(
//                            Double.parseDouble(aSSRS.GetText(1, 2)) - sumPremAdd,
//                            "0.00"));

            return true;
    }

    /**
     * 查询无名单人数和保费
     * @param grpContNo String
     * @return SSRS
     */
    private SSRS getNoNameGrpContPeoples2AndPrem(String grpContNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append("select sum(peoples), sum(prem) ")
            .append("from LCCont ")
            .append("where polType = '1' ")
            .append("   and grpContNo = '")
            .append(grpContNo)
            .append("' ");
        System.out.println(sql.toString());
        SSRS tSSRS = new ExeSQL().execSQL(sql.toString());
        if(tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("查询无名单人数出错。");
            return null;
        }
        return tSSRS;
    }


    //投保人联系方式变更
    private boolean getDetailAD(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        //取（保全团单投保人表）和（团单投保人表）的相关记录，生称相应schema
        LPGrpAppntDB tLPGrpAppntDB = new LPGrpAppntDB();
        tLPGrpAppntDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        tLPGrpAppntDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        tLPGrpAppntDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        LPGrpAppntSet tLPGrpAppntSet = new LPGrpAppntSet();
        tLPGrpAppntSet.set(tLPGrpAppntDB.query());
        if (!tLPGrpAppntDB.getInfo()) {
            buildError("getDetailAD", "保全团单投保人表LPGrpAppnt中无相关批单号的记录");
            //return false;
        }
        LPGrpAppntSchema tLPGrpAppntSchema = tLPGrpAppntDB.getSchema();

        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(tLPGrpAppntSchema.getGrpContNo());
        if (!tLCGrpAppntDB.getInfo()) {
            buildError("getDetailAD", "团单投保人表LCGrpAppnt中无相关集体合同号的记录");
            return false;
        }
        LCGrpAppntSchema tLCGrpAppntSchema = tLCGrpAppntDB.getSchema();

        //取（保全团体客户地址表）和（团体客户地址表）的相关记录，生称相应schema

        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        tLCGrpAddressDB.setCustomerNo(tLCGrpAppntSchema.getCustomerNo());
        tLCGrpAddressDB.setAddressNo(tLCGrpAppntSchema.getAddressNo());
        if (!tLCGrpAddressDB.getInfo()) {
            buildError("getDetailAD", "团体客户地址表LCGrpAddress中无相关集体合同号的记录");
            return false;
        }
        LCGrpAddressSchema tLCGrpAddressSchema = tLCGrpAddressDB.getSchema();

        LPGrpAddressSchema tLPGrpAddressSchema = new LPGrpAddressSchema();
        LPGrpAddressDB tLPGrpAddressDB = new LPGrpAddressDB();
        tLPGrpAddressDB.setEdorNo(tLPGrpAppntSchema.getEdorNo());
        tLPGrpAddressDB.setEdorType(tLPGrpAppntSchema.getEdorType());
        tLPGrpAddressDB.setCustomerNo(tLPGrpAppntSchema.getCustomerNo());
        tLPGrpAddressDB.setAddressNo(tLPGrpAppntSchema.getAddressNo());

        if (!tLPGrpAddressDB.getInfo()) { //可能修改后的地址用的是原来LCGrpAddress中的地址，所以在LPGrpAddress中没有记录
            //尝试从LCGrpAddress中取出此地址记录
            tLCGrpAddressDB = new LCGrpAddressDB();
            tLCGrpAddressDB.setCustomerNo(tLPGrpAppntSchema.getCustomerNo());
            tLCGrpAddressDB.setAddressNo(tLPGrpAppntSchema.getAddressNo());
            if (!tLCGrpAddressDB.getInfo()) {
                buildError("getDetailAD", "保全团体客户地址表LPGrpAddress中无相关集体合同号的记录");
                return false;
            }
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLPGrpAddressSchema,
                                     tLCGrpAddressDB.getSchema());
        } else {
            tLPGrpAddressSchema = tLPGrpAddressDB.getSchema();
        }

        String[] strArr = new String[3];
        tlistTable = new ListTable();
        tlistTable.setName("AD");

        //比较地址是否变化
        if (!StrTool.cTrim(tLCGrpAddressSchema.getGrpAddress()).equals(StrTool.
                cTrim(
                        tLPGrpAddressSchema.getGrpAddress()))) {
            strArr[0] = "地址";
            strArr[1] = tLCGrpAddressSchema.getGrpAddress();
            strArr[2] = tLPGrpAddressSchema.getGrpAddress();
            tlistTable.add(strArr);
        }

        //比较邮编
        if (!StrTool.cTrim(tLCGrpAddressSchema.getGrpZipCode()).equals(StrTool.
                cTrim(tLPGrpAddressSchema.getGrpZipCode()))) {
            strArr[0] = "邮编";
            strArr[1] = tLCGrpAddressSchema.getGrpZipCode();
            strArr[2] = tLPGrpAddressSchema.getGrpZipCode();
            tlistTable.add(strArr);
        }

        //比较联系人1
        if (!StrTool.cTrim(tLCGrpAddressSchema.getLinkMan1()).equals(StrTool.
                cTrim(tLPGrpAddressSchema.getLinkMan1()))) {
            strArr[0] = "联系人1";
            strArr[1] = tLCGrpAddressSchema.getLinkMan1();
            strArr[2] = tLPGrpAddressSchema.getLinkMan1();
            tlistTable.add(strArr);
        }

        //比较联系电话1是否变化
        if (!StrTool.cTrim(tLCGrpAddressSchema.getPhone1()).equals(StrTool.
                cTrim(
                        tLPGrpAddressSchema.getPhone1()))) {
            strArr[0] = "联系电话1";
            strArr[1] = tLCGrpAddressSchema.getPhone1();
            strArr[2] = tLPGrpAddressSchema.getPhone1();
            tlistTable.add(strArr);
        }

        //比较传真1是否变化
        if (!StrTool.cTrim(tLCGrpAddressSchema.getFax1()).equals(StrTool.cTrim(
                tLPGrpAddressSchema.getFax1()))) {
            strArr[0] = "传真1";
            strArr[1] = tLCGrpAddressSchema.getFax1();
            strArr[2] = tLPGrpAddressSchema.getFax1();
            tlistTable.add(strArr);
        }

        //比较联系人2
        if (!StrTool.cTrim(tLCGrpAddressSchema.getLinkMan2()).equals(StrTool.
                cTrim(tLPGrpAddressSchema.getLinkMan2()))) {
            strArr[0] = "联系人2";
            strArr[1] = tLCGrpAddressSchema.getLinkMan2();
            strArr[2] = tLPGrpAddressSchema.getLinkMan2();
            tlistTable.add(strArr);
        }

        //比较联系电话2是否变化
        if (!StrTool.cTrim(tLCGrpAddressSchema.getPhone2()).equals(StrTool.
                cTrim(
                        tLPGrpAddressSchema.getPhone2()))) {
            strArr[0] = "联系电话2";
            strArr[1] = tLCGrpAddressSchema.getPhone2();
            strArr[2] = tLPGrpAddressSchema.getPhone2();
            tlistTable.add(strArr);
        }

        //比较传真2是否变化
        if (!StrTool.cTrim(tLCGrpAddressSchema.getFax2()).equals(StrTool.cTrim(
                tLPGrpAddressSchema.getFax2()))) {
            strArr[0] = "传真2";
            strArr[1] = tLCGrpAddressSchema.getFax2();
            strArr[2] = tLPGrpAddressSchema.getFax2();
            tlistTable.add(strArr);
        }

        String[] strArrHead = new String[3];
        strArrHead[0] = "变更项目";
        strArrHead[1] = "变更前";
        strArrHead[2] = "变更后";

        textTag.add("AC_EdorValiDate",
                    CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));
        xmlexport.addListTable(tlistTable, strArrHead);
        return true;
    }

    //增加被保险人
    private boolean getDetailNI(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        ExeSQL tExeSQL = new ExeSQL();
        String sql = "select count(*) from LCInsuredList " +
                     "where GrpContNo = '" +
                     aLPGrpEdorItemSchema.getGrpContNo() + "' " +
                     "and EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
                     "and State = '1' " +
                     "and ContPlanCode != 'FM' ";
        String num = tExeSQL.getOneValue(sql);

        sql = "select GetMoney from LPGrpEdorItem " +
              "where EdorNo = '" +
              aLPGrpEdorItemSchema.getEdorNo() + "' " +
              "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "'";
        String getMoney = tExeSQL.getOneValue(sql);

        //得到管理费
        EdorItemSpecialData specialData = new EdorItemSpecialData(
                aLPGrpEdorItemSchema);
        specialData.query();
        String manageFee = specialData.getEdorValue("ManageFee");

        textTag.add("NI_Num", num);
        textTag.add("NI_GetMoney", CommonBL.carry(getMoney));
        textTag.add("NI_EdorValiDate",
                    CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));

        String content = "";
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        if (CommonBL.hasSpecialRisk(grpContNo) && !CommonBL.hasNotSpecialRisk(mGrpContNo))
        {
            String createAccFlag = specialData.getEdorValue("CreateAccFlag");
            if ((createAccFlag != null) && (createAccFlag.equals("1"))) { //设置帐户
                String source = specialData.getEdorValue("Source");
                if ((source != null) && (source.equals("1"))) {
                    content += "按保单规定分别建立个人医疗帐户，" +
                            "医疗帐户资金来源为：投保单位追加保费。\n" +
                            "本次变更特需险种应收费" + CommonBL.carry(getMoney) +
                            "元，扣除管理费" + manageFee + "元。";
                }
                if ((source != null) && (source.equals("2"))) {
                    double grpAccountMoney = CommonBL.getGrpAccountMoney(
                            grpContNo);
                    double leftMoney = grpAccountMoney -
                                       aLPGrpEdorItemSchema.getChgPrem();
                    if (leftMoney >= 0) {
                        content += "按保单规定分别建立个人医疗帐户" +
                                "医疗帐户资金来源为：团体帐户转出。\n" +
                                "团体帐户转移上述金额后余额为" + bigDoubleToCommonString(leftMoney , "0.00")+ "元。";
                    } else {
                        content += "按保单规定分别建立个人医疗帐户" +
                                "医疗帐户资金来源为：团体帐户转出。\n" +
                                "团体帐户转移上述金额后余额为0元。";
                        content += "\n本次变更特需险种应收费" + CommonBL.carry(getMoney) +
                                "元，扣除管理费" + manageFee + "元。";
                    }
                }
            }
        }
        content += "\n本次应收保费合计" + CommonBL.carry(getMoney) + "元。";
        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("NI");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);

        sql = "select sum(Prem) from LCCont " +
              "where GrpContNo = '" +
              aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        String totalMoney = tExeSQL.getOneValue(sql);
        textTag.add("NI_TotalMoney", "期交保费合计" +
                    CommonBL.carry(totalMoney) + "元。");

        //生成被保人清单
        PrtGrpInsuredList tPrtGrpInsuredList =
                new PrtGrpInsuredList(mGlobalInput, mEdorNo);
        tPrtGrpInsuredList.submitData();
        return true;
    }
    
    //团单万能增加被保险人
    private boolean getDetailTZ(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        ExeSQL tExeSQL = new ExeSQL();
        String sql = "select count(1) from LCInsuredList " +
                     "where GrpContNo = '" +
                     aLPGrpEdorItemSchema.getGrpContNo() + "' " +
                     "and EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
                     "and State = '1' " +
                     "and (ContPlanCode != 'FM' or ContPlanCode is null) ";
        String num = tExeSQL.getOneValue(sql);

        sql = "select GetMoney from LPGrpEdorItem " +
              "where EdorNo = '" +
              aLPGrpEdorItemSchema.getEdorNo() + "' " +
              "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "'";
        String getMoney = tExeSQL.getOneValue(sql);

        //得到管理费
        EdorItemSpecialData specialData = new EdorItemSpecialData(
                aLPGrpEdorItemSchema);
        specialData.query();
        String manageFee = specialData.getEdorValue("ManageFee");

        textTag.add("TZ_Num", num);
        textTag.add("TZ_GetMoney", CommonBL.carry(getMoney));
        textTag.add("TZ_EdorValiDate",
                    CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));

        String content = "";
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();

        
/*        //团险万能账户部分
            String createAccFlag = specialData.getEdorValue("CreateAccFlag");
            if ((createAccFlag != null) && (createAccFlag.equals("1"))) { //设置帐户
                String source = specialData.getEdorValue("Source");
                if ((source != null) && (source.equals("1"))) {
                    content += "按保单规定分别建立个人医疗帐户，" +
                            "医疗帐户资金来源为：投保单位追加保费。\n" +
                            "本次变更特需险种应收费" + CommonBL.carry(getMoney) +
                            "元，扣除管理费" + manageFee + "元。";
                }
                if ((source != null) && (source.equals("2"))) {
                    double grpAccountMoney = CommonBL.getGrpAccountMoney(
                            grpContNo);
                    double leftMoney = grpAccountMoney -
                                       aLPGrpEdorItemSchema.getChgPrem();
                    if (leftMoney >= 0) {
                        content += "按保单规定分别建立个人医疗帐户" +
                                "医疗帐户资金来源为：团体帐户转出。\n" +
                                "团体帐户转移上述金额后余额为" + bigDoubleToCommonString(leftMoney , "0.00")+ "元。";
                    } else {
                        content += "按保单规定分别建立个人医疗帐户" +
                                "医疗帐户资金来源为：团体帐户转出。\n" +
                                "团体帐户转移上述金额后余额为0元。";
                        content += "\n本次变更特需险种应收费" + CommonBL.carry(getMoney) +
                                "元，扣除管理费" + manageFee + "元。";
                    }
                }
            }*/
        
        content += "\n本次应收保费合计" + CommonBL.carry(getMoney) + "元。";
        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("TZ");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);

        sql = "select sum(Prem) from LCCont " +
              "where GrpContNo = '" +
              aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        String totalMoney = tExeSQL.getOneValue(sql);
        textTag.add("TZ_TotalMoney", "期交保费合计" +
                    CommonBL.carry(totalMoney) + "元。");

        //生成被保人清单
        PrtGrpInsuredListTZBL tPrtGrpInsuredListTZBL =
                new PrtGrpInsuredListTZBL(mGlobalInput, mEdorNo);
        tPrtGrpInsuredListTZBL.submitData();
        return true;
    }
    private  boolean getDetailGB(LPGrpEdorItemSchema aLPGrpEdorItemSchema){

        String sql = "select * from LPGrpEdorItem " +
                     "where EdorAcceptNo = '" +
                     aLPGrpEdorItemSchema.getEdorAcceptNo() + "' " +
                     "and EdorType='" +
                     aLPGrpEdorItemSchema.getEdorType() + "' " +
                     "order by edorno";
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(sql);
        if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() < 1) {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getInsuredCM";
            tError.errorMessage = "申请号" + aLPGrpEdorItemSchema.getEdorAcceptNo() +
                                  "下无相应的保全项目信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //归属规则变更之前
        String CAscriptionrule =getCAscriptionrule(aLPGrpEdorItemSchema);
        String PAscriptionrule =getPAscriptionrule(aLPGrpEdorItemSchema);
        System.out.println("PAscriptionrule"+PAscriptionrule);
        //得到本次CM变更的被保险人数
     

        textTag.add("GB_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        textTag.add("GB_CAS", CAscriptionrule);
        textTag.add("GB_PAS", PAscriptionrule);
        return true;
    
    }
    

    private boolean getDetailCM(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        String tCustomerNo = "";
        String tFormerCustomerNo = "";

        String sql = "select * from LPEdorItem " +
                     "where EdorAcceptNo = '" +
                     aLPGrpEdorItemSchema.getEdorAcceptNo() + "' " +
                     "and EdorType='" +
                     aLPGrpEdorItemSchema.getEdorType() + "' " +
                     "order by edorno,insuredno";
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        String strImport="select count(1) from LPDiskImport where EdorNo='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' " +
        		" and EdorType='"+aLPGrpEdorItemSchema.getEdorType()+"' and state<>'"+BQ.IMPORTSTATE_FAIL+"' with ur ";
        String ImportCount=new ExeSQL().getOneValue(strImport);
        if(null != ImportCount && !"0".equals(ImportCount.trim())){
    		tlistTable = new ListTable();
    		String importInfo="变更联系电话的被保人资料详见所附清单";
    		textTag.add("CM_IMPORTINFO",importInfo);
    	}
        System.out.println("-----------------------------------------");
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
        	if(null != ImportCount && !"0".equals(ImportCount.trim())){
        		textTag.add("CM_SIZE", "0");
        		return true;
        	}
        	System.out.println("------------******************------------");
	            CError tError = new CError();
	            tError.moduleName = "PrtAppEndorsementBL";
	            tError.functionName = "getInsuredCM";
	            tError.errorMessage = "申请号" + aLPGrpEdorItemSchema.getEdorAcceptNo() +
	                                  "下无相应的保全项目信息!";
	            this.mErrors.addOneError(tError);
	            return false;
        }
        //得到本次CM变更的被保险人数
        LPInsuredDB tempLPInsuredDB = new LPInsuredDB();
        tempLPInsuredDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        tempLPInsuredDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        LPInsuredSet tempLPInsuredSet = tempLPInsuredDB.query();

        textTag.add("CM_SIZE", tempLPInsuredSet.size());
        int m = 0 ;
        for (int i = 0; i < tLPEdorItemSet.size(); i++) {
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i + 1);
            LPInsuredDB tLPInsuredDB = new LPInsuredDB();
            tLPInsuredDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPInsuredDB.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
//            由于连带被保险人和主被保险人都进行了CM,但LPEdorItemSchema只存储了一条信息。所以不能关联Insuredno
//            tLPInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
//            if (!tLPInsuredDB.getInfo()) {
//                mErrors.addOneError("找不到被保人信息！");
//                return false;
//            }
            LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();
            for (int j = 0; j < tLPInsuredSet.size(); j++) {
               tCustomerNo = tLPInsuredSet.get(j+1).getInsuredNo();
               if (!tFormerCustomerNo.equals(tCustomerNo)) {
                    System.out.println("aaa" + tLPEdorItemSchema.getEdorNo());
                    System.out.println("vvvv" + tLPEdorItemSchema.getContNo());
//                    System.out.println("bbb" + tLPEdorItemSchema.getInsuredNo());
                    LCInsuredDB tLCInsuredDB = new LCInsuredDB();
                    tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
                    tLCInsuredDB.setInsuredNo(tCustomerNo);
                    if (!tLCInsuredDB.getInfo()) {
                        CError tError = new CError();
                        tError.moduleName = "PrtAppEndorsementBL";
                        tError.functionName = "getInsuredCM";
                        tError.errorMessage = "LCInsuredDB不存在相应记录!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    LPInsuredSchema tLPInsuredSchema = tLPInsuredSet.get(j+1);
                    LCInsuredSchema tLCInsuredSchema = tLCInsuredDB.getSchema();

                    String printCustomer;
                    tlistTable = new ListTable();
                    tlistTable.setName("CM" +( i + j + m));
                    printCustomer = "被保人：" + tLPInsuredSchema.getName() +
                                    "，客户号：" + tLPInsuredSchema.getInsuredNo() +
                                    "\n";
                    //姓名变更
                    if (!StrTool.cTrim(tLPInsuredSchema.getName()).equals(
                            StrTool.
                            cTrim(tLCInsuredSchema.getName()))) {
                        String[] change = new String[1];
                        change[0] = "姓名：" + tLCInsuredSchema.getName() +
                                    "，变更为：" + tLPInsuredSchema.getName();
                        tlistTable.add(change);
                    }
                    //生日变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getBirthday()).equals(
                            StrTool.cTrim(tLPInsuredSchema.getBirthday()))) {
                        String[] change = new String[1];
                        change[0] = "生日：" +
                                    CommonBL.decodeDate(tLCInsuredSchema.
                                getBirthday()) +
                                    "，变更为：" +
                                    CommonBL.decodeDate(tLPInsuredSchema.
                                getBirthday());
                        tlistTable.add(change);
                    }

                    //性别变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getSex()).equals(
                            StrTool.
                            cTrim(tLPInsuredSchema.getSex()))) {
                        String[] change = new String[1];
                        change[0] = "性别：" +
                                    ChangeCodeBL.getCodeName("Sex",
                                tLCInsuredSchema.getSex()) +
                                    "，变更为：" +
                                    ChangeCodeBL.getCodeName("Sex",
                                tLPInsuredSchema.getSex());
                        tlistTable.add(change);
                    }

                    //证件类型变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getIDType()).equals(
                            StrTool.
                            cTrim(tLPInsuredSchema.getIDType()))) {
                        String[] change = new String[1];
                        change[0] = "证件类型：" +
                                    ChangeCodeBL.getCodeName("IDType",
                                tLCInsuredSchema.getIDType()) +
                                    "，变更为：" +
                                    ChangeCodeBL.getCodeName("IDType",
                                tLPInsuredSchema.getIDType());
                        tlistTable.add(change);
                    }

                    //证件号码变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getIDNo()).equals(
                            StrTool.
                            cTrim(
                                    tLPInsuredSchema.getIDNo()))) {
                        String[] change = new String[1];
                        change[0] = "证件号码：" +
                                    CommonBL.notNull(tLCInsuredSchema.getIDNo()) +
                                    "，变更为：" +
                                    CommonBL.notNull(tLPInsuredSchema.getIDNo());
                        tlistTable.add(change);
                    }

                    //职业(Code)变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getOccupationCode()).
                        equals(
                                StrTool.cTrim(tLPInsuredSchema.
                                              getOccupationCode()))) {
                        String[] change = new String[1];
                        change[0] = "职业：" +
                                    CommonBL.notNull(ChangeCodeBL.getCodeName(
                                            "OccupationCode",
                                            tLCInsuredSchema.getOccupationCode(),
                                            "OccupationCode")) +
                                    "，变更为：" +
                                    CommonBL.notNull(ChangeCodeBL.getCodeName(
                                            "OccupationCode",
                                            tLPInsuredSchema.getOccupationCode(),
                                            "OccupationCode"));
                        tlistTable.add(change);
                    }

                    //婚姻状况变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getMarriage()).equals(
                            StrTool.cTrim(tLPInsuredSchema.getMarriage()))) {
                        String[] change = new String[1];
                        change[0] = "婚姻状况：" +
                                    CommonBL.notNull(ChangeCodeBL.getCodeName(
                                            "Marriage",
                                            tLCInsuredSchema.getMarriage())) +
                                    "，变更为：" +
                                    CommonBL.notNull(ChangeCodeBL.getCodeName(
                                            "Marriage",
                                            tLPInsuredSchema.getMarriage()));
                        tlistTable.add(change);
                    }
                    textTag.add("CM_CUSTOMER" + (i+j+m), printCustomer);

                    //added by luomin   @2005-7-27 理赔金帐户
                    System.out.println("BankCode:" +
                                       tLCInsuredSchema.getBankCode());
                    
                    
                    //理赔金转帐银行变更
                    if (!StrTool.cTrim(tLPInsuredSchema.getBankCode()).equals(
                            StrTool.
                            cTrim(
                                    tLCInsuredSchema.getBankCode()))) {
                        String[] change = new String[1];
                        change[0] = "理赔金转帐银行：" +
                                    CommonBL.notNull(ChangeCodeBL.getCodeName(
                                            "bank",
                                            tLCInsuredSchema.getBankCode(),
                                            "BankCode")) +
                                    "，变更为：" +
                                    CommonBL.notNull(ChangeCodeBL.getCodeName(
                                            "bank",
                                            tLPInsuredSchema.getBankCode(),
                                            "BankCode"));
                        tlistTable.add(change);
                    }

                    //理赔金转帐帐号变更
                    if (!StrTool.cTrim(tLPInsuredSchema.getBankAccNo()).equals(
                            StrTool.
                            cTrim(
                                    tLCInsuredSchema.getBankAccNo()))) {
                        String[] change = new String[1];
                        change[0] = "理赔金转帐帐号：" +
                                    CommonBL.notNull(tLCInsuredSchema.
                                getBankAccNo()) +
                                    "，变更为：" +
                                    CommonBL.notNull(tLPInsuredSchema.
                                getBankAccNo());
                        tlistTable.add(change);
                    }
                    //理赔金转帐帐户变更
                    if (!StrTool.cTrim(tLPInsuredSchema.getAccName()).equals(
                            StrTool.
                            cTrim(
                                    tLCInsuredSchema.getAccName()))) {
                        String[] change = new String[1];
                        change[0] = "理赔金转帐帐户：" +
                                    CommonBL.notNull(tLCInsuredSchema.
                                getAccName()) +
                                    "，变更为：" +
                                    CommonBL.notNull(tLPInsuredSchema.
                                getAccName());
                        tlistTable.add(change);
                    }

                    //与主被保人关系变更
                    String relation = "";
                    if (tLCInsuredDB.getInfo()) {
                        relation = tLPInsuredSchema.getRelationToMainInsured();
                    }

                    if (!StrTool.cTrim(relation).equals(
                            StrTool.cTrim(tLCInsuredDB.getRelationToMainInsured()))) {
                        String[] change = new String[1];
                        change[0] = "与主被保人关系：" +
                                    CommonBL.notNull(ChangeCodeBL.getCodeName(
                                            "Relation",
                                            tLCInsuredDB.
                                            getRelationToMainInsured())) +
                                    "，变更为：" +
                                    CommonBL.notNull(ChangeCodeBL.getCodeName(
                                            "Relation",
                                            relation));
                        tlistTable.add(change);
                    }

                    //在职状态变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getInsuredStat()).
                        equals(
                                StrTool.cTrim(tLPInsuredSchema.getInsuredStat()))) {
                        String[] change = new String[1];
                        change[0] = "在职状态：" +
                                    ChangeCodeBL.getCodeName("workstate",
                                tLCInsuredSchema.getInsuredStat())
                                    + "  变更为：" +
                                    ChangeCodeBL.getCodeName("workstate",
                                tLPInsuredSchema.getInsuredStat());
                        tlistTable.add(change);
                    }
                    //团单被保险人联系电话变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getGrpInsuredPhone()).
                            equals(
                                    StrTool.cTrim(tLPInsuredSchema.getGrpInsuredPhone()))) {
                            String[] change = new String[1];
                            change[0] = "联系电话：" +
                            CommonBL.notNull(tLCInsuredSchema.getGrpInsuredPhone())
                                        + "  变更为：" +
                                        CommonBL.notNull(tLPInsuredSchema.getGrpInsuredPhone());
                            tlistTable.add(change);
                        }
                    //学平险汇交件保单显示投保人信息变更内容
                    LCInsuredListDB tLCInsuredListDB=new LCInsuredListDB();
                    tLCInsuredListDB.setGrpContNo(tLPInsuredSchema.getEdorNo());
                    tLCInsuredListDB.setInsuredNo(tLCInsuredSchema.getInsuredNo());
                    tLCInsuredListDB.setEdorNo(tLPInsuredSchema.getEdorNo());
                    LCInsuredListSet tLCInsuredListSet=tLCInsuredListDB.query();
                    if(tLCInsuredListSet.size()>0){
                    	LBInsuredListDB tLBInsuredListDB=new LBInsuredListDB();
                    	tLBInsuredListDB.setGrpContNo(tLCInsuredSchema.getGrpContNo());
                    	tLBInsuredListDB.setInsuredNo(tLCInsuredSchema.getInsuredNo());
                    	LBInsuredListSet tLBInsuredListSet=tLBInsuredListDB.query();
                    	LBInsuredListSchema tLBInsuredListSchema=new LBInsuredListSchema();
                    	LCInsuredListSchema tLCInsuredListSchema=tLCInsuredListSet.get(1);
                    	if(tLBInsuredListSet.size()>0){
                    		tLBInsuredListSchema=tLBInsuredListSet.get(1);
                    	}
                    	if (!StrTool.cTrim(tLCInsuredListSchema.getAppntName()).
                                equals(
                                        StrTool.cTrim(tLBInsuredListSchema.getAppntName()))) {
                                String[] change = new String[1];
                                change[0] = "投保人姓名：" +
                                			tLBInsuredListSchema.getAppntName()
                                            + "  变更为：" +
                                            tLCInsuredListSchema.getAppntName();
                                tlistTable.add(change);
                            }
                    	if (!StrTool.cTrim(tLCInsuredListSchema.getAppntSex()).
                                equals(
                                        StrTool.cTrim(tLBInsuredListSchema.getAppntSex()))) {
                                String[] change = new String[1];
                                change[0] = "投保人性别：" +
                                			ChangeCodeBL.getCodeName("Sex",
                                			tLBInsuredListSchema.getAppntSex())
                                            + "  变更为：" +
                                            ChangeCodeBL.getCodeName("Sex",
                                            tLCInsuredListSchema.getAppntSex());
                                tlistTable.add(change);
                            }
                    	if (!StrTool.cTrim(tLCInsuredListSchema.getAppntIdType()).
                                equals(
                                        StrTool.cTrim(tLBInsuredListSchema.getAppntIdType()))) {
                                String[] change = new String[1];
                                change[0] = "投保人证件类型：" +
                                			ChangeCodeBL.getCodeName("IDType",
                                			tLBInsuredListSchema.getAppntIdType())
                                            + "  变更为：" +
                                            ChangeCodeBL.getCodeName("IDType",
                                            tLCInsuredListSchema.getAppntIdType());
                                tlistTable.add(change);
                            }
                    	if (!StrTool.cTrim(tLCInsuredListSchema.getAppntIdNo()).
                                equals(
                                        StrTool.cTrim(tLBInsuredListSchema.getAppntIdNo()))) {
                                String[] change = new String[1];
                                change[0] = "投保人证件号码：" +
                                			tLBInsuredListSchema.getAppntIdNo()
                                            + "  变更为：" +
                                            tLCInsuredListSchema.getAppntIdNo();
                                tlistTable.add(change);
                            }
                    	if (!StrTool.cTrim(tLCInsuredListSchema.getAppntBirthday()).
                                equals(
                                        StrTool.cTrim(tLBInsuredListSchema.getAppntBirthday()))) {
                                String[] change = new String[1];
                                change[0] = "投保人出生日期：" +
                                			tLBInsuredListSchema.getAppntBirthday()
                                            + "  变更为：" +
                                            tLCInsuredListSchema.getAppntBirthday();
                                tlistTable.add(change);
                            }
                    }
                    
                    
                  //万能团险变更添加
                    LCPolDB ttLCPolDB = new LCPolDB();
                    ttLCPolDB.setContNo(tLPEdorItemSchema.getContNo());
                    ttLCPolDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
                    LCPolSet ttLCPolSet = ttLCPolDB.query();
                    if (ttLCPolSet == null || ttLCPolSet.size() < 1) {
                        CError tError = new CError();
                        tError.moduleName = "PrtAppEndorsementBL";
                        tError.functionName = "getInsuredCM";
                        tError.errorMessage = "LCPol不存在相应的记录!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if(ttLCPolSet.get(1).getContType().equals("2") 
                    		&&CommonBL.hasULIRisk(tLCInsuredSchema.getContNo())){
                    	if (!StrTool.cTrim(tLCInsuredSchema.getPosition()).equals(
                                StrTool.
                                cTrim(
                                        tLPInsuredSchema.getPosition()))) {
                            String[] change = new String[1];
                            change[0] = "级别：" +
                                        CommonBL.notNull(getPositionName(tLCInsuredSchema.getPosition())) +
                                        "，变更为：" +
                                        CommonBL.notNull(getPositionName(tLPInsuredSchema.getPosition()));
                            tlistTable.add(change);
                        }
                    	if (!StrTool.cTrim(tLCInsuredSchema.getJoinCompanyDate()).equals(
                                StrTool.
                                cTrim(
                                        tLPInsuredSchema.getJoinCompanyDate()))) {
                            String[] change = new String[1];
                            change[0] = "服务年数起始：" +
                                        CommonBL.notNull(tLCInsuredSchema.getJoinCompanyDate()) +
                                        "，变更为：" +
                                        CommonBL.notNull(tLPInsuredSchema.getJoinCompanyDate());
                            tlistTable.add(change);
                        }
                    	LPGetDB tLPGetDB = new LPGetDB();
                    	tLPGetDB.setContNo(tLPEdorItemSchema.getContNo());
                    	tLPGetDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
                    	LPGetSet tLPGetSet = tLPGetDB.query();
                        if (tLPGetSet != null && tLPGetSet.size() >= 1) {
                        	LPGetSchema tLPGetSchema = tLPGetSet.get(1);
                        	LCGetDB tLCGetDB = new LCGetDB();
                        	tLCGetDB.setPolNo(tLPGetSchema.getPolNo());
                        	tLCGetDB.setDutyCode(tLPGetSchema.getDutyCode());
                        	tLCGetDB.setGetDutyCode(tLPGetSchema.getGetDutyCode());
                        	LCGetSet tLCGetSet = tLCGetDB.query();
                            if(tLCGetSet==null||tLPGetSet.size() < 1){
                            	CError tError = new CError();
                                tError.moduleName = "PrtAppEndorsementBL";
                                tError.functionName = "getInsuredCM";
                                tError.errorMessage = "LCGet不存在相应的记录!";
                                this.mErrors.addOneError(tError);
                                return false;
                            }
                            LCGetSchema tLCGetSchema = tLCGetSet.get(1);
                            if (!StrTool.cTrim(tLCGetSchema.getGetDutyKind()).equals(
                                    StrTool.
                                    cTrim(
                                    		tLPGetSchema.getGetDutyKind()))) {
                                String[] change = new String[1];
                                String tCKind = getDutyKindCode(tLCGetSchema.getGetDutyKind());
                                String tPKind = getDutyKindCode(tLPGetSchema.getGetDutyKind());
                                change[0] = "老年护理金领取方式：" +
                                            CommonBL.notNull(tCKind) +
                                            "，变更为：" +
                                            CommonBL.notNull(tPKind);
                                tlistTable.add(change);
                            }
                        }
                        
                    }
                    

                    String[] strhead = new String[1];
                    strhead[0] = "批改内容";
                    xmlexport.addListTable(tlistTable, strhead);

                    //资料更新导致的费用变更
                    sql = "select sum(GetMoney) from LJSGetEndorse " +
                          "where EndorsementNo = '" +
                          tLPEdorItemSchema.getEdorNo() + "' " +
                          "and FeeOperationType = '" +
                          tLPEdorItemSchema.getEdorType() + "' " +
                          "and InsuredNo = '" +
                          tLPEdorItemSchema.getInsuredNo() + "'";
                    ExeSQL tExeSQL = new ExeSQL();
                    String chgPrem = tExeSQL.getOneValue(sql);
                    if (chgPrem != null && !"".equals(chgPrem) &&
                        Double.parseDouble(chgPrem) != 0) { //保费变更，需要添加费用变更打印内容
                        String printContent = "由于上述资料变更，保单" +
                                              tLPEdorItemSchema.getContNo() +
                                              "相关险种承保内容变更如下：";
                        textTag.add("CM_FEE" + (i+j+m), printContent);

                        LCPolDB tLCPolDB = new LCPolDB();
                        tLCPolDB.setContNo(tLPEdorItemSchema.getContNo());
                        tLCPolDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
                        LCPolSet tLCPolSet = tLCPolDB.query();
                        if (tLCPolSet == null || tLCPolSet.size() < 1) {
                            CError tError = new CError();
                            tError.moduleName = "PrtAppEndorsementBL";
                            tError.functionName = "getInsuredCM";
                            tError.errorMessage = "LCPol不存在相应的记录!";
                            this.mErrors.addOneError(tError);
                            return false;
                        }

                        tlistTable = new ListTable();
                        tlistTable.setName("FEE" + (i+j+m));
                        for (int k = 0; k < tLCPolSet.size(); k++) {
                            LCPolSchema tLCPOLSchema = tLCPolSet.get(k + 1);

                            //得到各个险种的变更费用
                            sql = "select sum(GetMoney) from LJSGetEndorse " +
                                  "where EndorsementNo = '" +
                                  tLPEdorItemSchema.getEdorNo() + "' " +
                                  "and FeeOperationType = '" +
                                  tLPEdorItemSchema.getEdorType() + "' " +
                                  "and InsuredNo = '" +
                                  tLPEdorItemSchema.getInsuredNo() + "' " +
                                  "and RiskCode = '" +
                                  tLCPOLSchema.getRiskCode() + "' ";
                            tExeSQL = new ExeSQL();
                            chgPrem = tExeSQL.getOneValue(sql);
                            if ((chgPrem == null) || (chgPrem.equals(""))) {
                                continue;
                            }

                            LPPolDB tLPPolDB = new LPPolDB();
                            tLPPolDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
                            tLPPolDB.setEdorType(tLPEdorItemSchema.getEdorType());
                            tLPPolDB.setPolNo(tLCPOLSchema.getPolNo());
                            if (!tLPPolDB.getInfo()) {
                                CError tError = new CError();
                                tError.moduleName = "PrtAppEndorsementBL";
                                tError.functionName = "getInsuredCM";
                                tError.errorMessage = "LPPol不存在相应的记录!";
                                this.mErrors.addOneError(tError);
                                return false;
                            }
                            LMRiskDB tLMRiskDB = new LMRiskDB();
                            tLMRiskDB.setRiskCode(tLCPOLSchema.getRiskCode());
                            if (!tLMRiskDB.getInfo()) {
                                CError tError = new CError();
                                tError.moduleName = "PrtAppEndorsementBL";
                                tError.functionName = "getInsuredCM";
                                tError.errorMessage = "LMRisk不存在相应的记录!";
                                this.mErrors.addOneError(tError);
                                return false;
                            }

                            String[] fee = new String[1];
                            fee[0] = "序号" + tLCPOLSchema.getRiskSeqNo() +
                                     "险种（" +
                                     tLMRiskDB.getRiskName() + "）保费变更为：" +
                                     CommonBL.carry(tLPPolDB.getPrem()) + "，";

                            if (Double.parseDouble(chgPrem) < 0) {
                                fee[0] += "应退费" +
                                        CommonBL.carry(Math.abs(Double.
                                        parseDouble(
                                                chgPrem))) +
                                        "元。\n";
                            } else if (Double.parseDouble(chgPrem) > 0) {
                                fee[0] += "应补费" +
                                        CommonBL.carry(Double.parseDouble(
                                        chgPrem)) +
                                        "元。\n";
                            }

                            tlistTable.add(fee);
                        }
                        String[] feeHead = new String[1];
                        strhead[0] = "费用";
                        xmlexport.addListTable(tlistTable, feeHead);
                    }
                }
                tFormerCustomerNo = tCustomerNo;
            }
            m += tLPInsuredSet.size()-1;
        }
        return true;
    }
    
//whl
    private boolean getDetailJM(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
    	String tCustomerNo = "";

        String sql = "select * from LPEdorItem " +
                     "where EdorAcceptNo = '" +
                     aLPGrpEdorItemSchema.getEdorAcceptNo() + "' " +
                     "and EdorType='" +
                     aLPGrpEdorItemSchema.getEdorType() + "' " +
                     "order by edorno,insuredno";
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getInsuredCM";
            tError.errorMessage = "申请号" + aLPGrpEdorItemSchema.getEdorAcceptNo() +
                                  "下无相应的保全项目信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
              
        LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(1);
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        tLPInsuredDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
        tLPInsuredDB.setEdorType(tLPEdorItemSchema.getEdorType());
        tLPInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
//        加上多被保人的支持
        tLPInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
        if (!tLPInsuredDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getInsuredCM";
            tError.errorMessage = "申请号" + aLPGrpEdorItemSchema.getEdorAcceptNo() +
                                  "下无相应的被保人修改信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LPInsuredSchema tLPInsuredSchema = tLPInsuredDB.getSchema();
        tCustomerNo = tLPInsuredSchema.getInsuredNo();   


//        LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();
//        tCustomerNo = tLPInsuredSet.get(1).getInsuredNo();


        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
        tLCInsuredDB.setInsuredNo(tCustomerNo);
        if (!tLCInsuredDB.getInfo()) {
        		CError tError = new CError();
        		tError.moduleName = "PrtAppEndorsementBL";
        		tError.functionName = "getInsuredCM";
        		tError.errorMessage = "LCInsuredDB不存在相应记录!";
        		this.mErrors.addOneError(tError);
        		return false;
        }
        LCInsuredSchema tLCInsuredSchema = tLCInsuredDB.getSchema();
        

        String printCustomer="";

        printCustomer = "被保人：" + tLPInsuredSchema.getName() +
            "，客户号：" + tLPInsuredSchema.getInsuredNo() +
            "\n";
        textTag.add("JM_CUSTOMER", printCustomer);
        String change = new String();
        //姓名变更
        if (!StrTool.cTrim(tLPInsuredSchema.getName()).equals(
                StrTool.
                cTrim(tLCInsuredSchema.getName()))) {
            change += "姓名：" + tLCInsuredSchema.getName() +
            "，变更为：" + tLPInsuredSchema.getName()+
            "\n";
        }
        //生日变更
        if (!StrTool.cTrim(tLCInsuredSchema.getBirthday()).equals(
                StrTool.cTrim(tLPInsuredSchema.getBirthday()))) {

            change += "生日：" +
            CommonBL.decodeDate(tLCInsuredSchema.
        getBirthday()) +
            "，变更为：" +
            CommonBL.decodeDate(tLPInsuredSchema.
        getBirthday())+
        "\n";

        }

        //性别变更
        if (!StrTool.cTrim(tLCInsuredSchema.getSex()).equals(
                StrTool.
                cTrim(tLPInsuredSchema.getSex()))) {

            change += "性别：" +
            ChangeCodeBL.getCodeName("Sex",
        tLCInsuredSchema.getSex()) +
            "，变更为：" +
            ChangeCodeBL.getCodeName("Sex",
        tLPInsuredSchema.getSex())+
        "\n";

        }

        //证件号码变更
        if (!StrTool.cTrim(tLCInsuredSchema.getIDNo()).equals(
                StrTool.
                cTrim(
            tLPInsuredSchema.getIDNo()))) {

            change += "证件号码：" +
            CommonBL.notNull(tLCInsuredSchema.getIDNo()) +
            "，变更为：" +
            CommonBL.notNull(tLPInsuredSchema.getIDNo())+
            "\n";

        }
        
        LICardActiveInfoListDB tLICardActiveInfoListDB=new LICardActiveInfoListDB();
        tLICardActiveInfoListDB.setCardNo(tLPEdorItemSchema.getContNo());
        tLICardActiveInfoListDB.setName(tLCInsuredSchema.getName());
        LICardActiveInfoListSet tLICardActiveInfoListSet=tLICardActiveInfoListDB.query();
        if(tLICardActiveInfoListSet.size()>0)
        {
        LICardActiveInfoListSchema tLICardActiveInfoListSchema=tLICardActiveInfoListSet.get(1);
        
        //手机号变更
        if (!StrTool.cTrim(tLICardActiveInfoListSchema.getMobile()).equals(
                StrTool.
                cTrim(
                		getEdorValueByDetailType("Mobile",tLPEdorItemSchema.getEdorNo())))) {

            change += "手机号码：" +
            CommonBL.notNull(tLICardActiveInfoListSchema.getMobile()) +
            "，变更为：" +
            CommonBL.notNull(getEdorValueByDetailType("Mobile",tLPEdorItemSchema.getEdorNo()))+
            "\n";

        }
        //固定电话变更
        if (!StrTool.cTrim(tLICardActiveInfoListSchema.getPhone()).equals(
                StrTool.
                cTrim(
                		getEdorValueByDetailType("Phone",tLPEdorItemSchema.getEdorNo())))) {

            change += "固定电话号码：" +
            CommonBL.notNull(tLICardActiveInfoListSchema.getPhone()) +
            "，变更为：" +
            CommonBL.notNull(getEdorValueByDetailType("Phone",tLPEdorItemSchema.getEdorNo()))+
            "\n";

        }
//                  通讯地址变更
        if (!StrTool.cTrim(tLICardActiveInfoListSchema.getPostalAddress()).equals(
                StrTool.
                cTrim(
                		getEdorValueByDetailType("PostalAddress",tLPEdorItemSchema.getEdorNo())))) {

            change += "通讯地址：" +
            CommonBL.notNull(tLICardActiveInfoListSchema.getPostalAddress()) +
            "，变更为：" +
            CommonBL.notNull(getEdorValueByDetailType("PostalAddress",tLPEdorItemSchema.getEdorNo()))+
            "\n";

        }
//                  邮编变更
        if (!StrTool.cTrim(tLICardActiveInfoListSchema.getZipCode()).equals(
                StrTool.
                cTrim(
                		getEdorValueByDetailType("ZipCode",tLPEdorItemSchema.getEdorNo())))) {

            change += "邮编：" +
            CommonBL.notNull(tLICardActiveInfoListSchema.getZipCode()) +
            "，变更为：" +
            CommonBL.notNull(getEdorValueByDetailType("ZipCode",tLPEdorItemSchema.getEdorNo()))+
            "\n";

        }
//                  Email地址变更
        if (!StrTool.cTrim(tLICardActiveInfoListSchema.getEMail()).equals(
                StrTool.
                cTrim(
                		getEdorValueByDetailType("EMail",tLPEdorItemSchema.getEdorNo())))) {

            change += "Email地址：" +
            CommonBL.notNull(tLICardActiveInfoListSchema.getEMail()) +
            "，变更为：" +
            CommonBL.notNull(getEdorValueByDetailType("EMail",tLPEdorItemSchema.getEdorNo()))+
            "\n";

        }
//        职业代码变更
        if (!StrTool.cTrim(tLICardActiveInfoListSchema.getOccupationCode()).equals(
                StrTool.
                cTrim(
                		getEdorValueByDetailType("OccupationCode",tLPEdorItemSchema.getEdorNo())))) {

            change += "职业代码：" +
            CommonBL.notNull(tLICardActiveInfoListSchema.getOccupationCode()) +
            "，变更为：" +
            CommonBL.notNull(getEdorValueByDetailType("OccupationCode",tLPEdorItemSchema.getEdorNo()))+
            "\n";

        }
//                  职业类别变更
        if (!StrTool.cTrim(tLICardActiveInfoListSchema.getOccupationType()).equals(
                StrTool.
                cTrim(
                		getEdorValueByDetailType("OccupationType",tLPEdorItemSchema.getEdorNo())))) {

            change += "职业类别：" +
            CommonBL.notNull(tLICardActiveInfoListSchema.getOccupationType()) +
            "，变更为：" +
            CommonBL.notNull(getEdorValueByDetailType("OccupationType",tLPEdorItemSchema.getEdorNo()))+
            "\n";

        }
//                  证件类型变更
        if (!StrTool.cTrim(tLICardActiveInfoListSchema.getIdType()).equals(
                StrTool.
                cTrim(
                		getEdorValueByDetailType("IDType",tLPEdorItemSchema.getEdorNo())))) {

            change += "证件类型：" +
            CommonBL.notNull(tLICardActiveInfoListSchema.getIdType()) +
            "，变更为：" +
            CommonBL.notNull(getEdorValueByDetailType("IDType",tLPEdorItemSchema.getEdorNo()))+
            "\n";

        }
        }
        else {
            LICertifyInsuredDB tLICertifyInsuredDB=new LICertifyInsuredDB();
            tLICertifyInsuredDB.setCardNo(tLPEdorItemSchema.getContNo());
            tLICertifyInsuredDB.setName(tLCInsuredSchema.getName());
            LICertifyInsuredSet tLICertifyInsuredSet=tLICertifyInsuredDB.query();
            
            if(tLICertifyInsuredSet.size()==0)
            {
                mErrors.addOneError(new CError("无法查询到该卡相关的被保人信息！"));
                return false;
            }
            
            LICertifyInsuredSchema tLICertifyInsuredSchema=tLICertifyInsuredSet.get(1);
            
            //手机号变更
            if (!StrTool.cTrim(tLICertifyInsuredSchema.getMobile()).equals(
                    StrTool.
                    cTrim(
                    		getEdorValueByDetailType("Mobile",tLPEdorItemSchema.getEdorNo())))) {

                change += "手机号码：" +
                CommonBL.notNull(tLICertifyInsuredSchema.getMobile()) +
                "，变更为：" +
                CommonBL.notNull(getEdorValueByDetailType("Mobile",tLPEdorItemSchema.getEdorNo()))+
                "\n";

            }
            //固定电话变更
            if (!StrTool.cTrim(tLICertifyInsuredSchema.getPhone()).equals(
                    StrTool.
                    cTrim(
                    		getEdorValueByDetailType("Phone",tLPEdorItemSchema.getEdorNo())))) {

                change += "固定电话号码：" +
                CommonBL.notNull(tLICertifyInsuredSchema.getPhone()) +
                "，变更为：" +
                CommonBL.notNull(getEdorValueByDetailType("Phone",tLPEdorItemSchema.getEdorNo()))+
                "\n";

            }
//                      通讯地址变更
            if (!StrTool.cTrim(tLICertifyInsuredSchema.getPostalAddress()).equals(
                    StrTool.
                    cTrim(
                    		getEdorValueByDetailType("PostalAddress",tLPEdorItemSchema.getEdorNo())))) {

                change += "通讯地址：" +
                CommonBL.notNull(tLICertifyInsuredSchema.getPostalAddress()) +
                "，变更为：" +
                CommonBL.notNull(getEdorValueByDetailType("PostalAddress",tLPEdorItemSchema.getEdorNo()))+
                "\n";

            }
//                      邮编变更
            if (!StrTool.cTrim(tLICertifyInsuredSchema.getZipCode()).equals(
                    StrTool.
                    cTrim(
                    		getEdorValueByDetailType("ZipCode",tLPEdorItemSchema.getEdorNo())))) {

                change += "邮编：" +
                CommonBL.notNull(tLICertifyInsuredSchema.getZipCode()) +
                "，变更为：" +
                CommonBL.notNull(getEdorValueByDetailType("ZipCode",tLPEdorItemSchema.getEdorNo()))+
                "\n";

            }
//                      Email地址变更
            if (!StrTool.cTrim(tLICertifyInsuredSchema.getEMail()).equals(
                    StrTool.
                    cTrim(
                    		getEdorValueByDetailType("EMail",tLPEdorItemSchema.getEdorNo())))) {

                change += "Email地址：" +
                CommonBL.notNull(tLICertifyInsuredSchema.getEMail()) +
                "，变更为：" +
                CommonBL.notNull(getEdorValueByDetailType("EMail",tLPEdorItemSchema.getEdorNo()))+
                "\n";

            }
//            职业代码变更
            if (!StrTool.cTrim(tLICertifyInsuredSchema.getOccupationCode()).equals(
                    StrTool.
                    cTrim(
                    		getEdorValueByDetailType("OccupationCode",tLPEdorItemSchema.getEdorNo())))) {

                change += "职业代码：" +
                CommonBL.notNull(tLICertifyInsuredSchema.getOccupationCode()) +
                "，变更为：" +
                CommonBL.notNull(getEdorValueByDetailType("OccupationCode",tLPEdorItemSchema.getEdorNo()))+
                "\n";

            }
//                      职业类别变更
            if (!StrTool.cTrim(tLICertifyInsuredSchema.getOccupationType()).equals(
                    StrTool.
                    cTrim(
                    		getEdorValueByDetailType("OccupationType",tLPEdorItemSchema.getEdorNo())))) {

                change += "职业类别：" +
                CommonBL.notNull(tLICertifyInsuredSchema.getOccupationType()) +
                "，变更为：" +
                CommonBL.notNull(getEdorValueByDetailType("OccupationType",tLPEdorItemSchema.getEdorNo()))+
                "\n";

            }
//                      证件类型变更
            if (!StrTool.cTrim(tLICertifyInsuredSchema.getIdType()).equals(
                    StrTool.
                    cTrim(
                    		getEdorValueByDetailType("IDType",tLPEdorItemSchema.getEdorNo())))) {

                change += "证件类型：" +
                CommonBL.notNull(tLICertifyInsuredSchema.getIdType()) +
                "，变更为：" +
                CommonBL.notNull(getEdorValueByDetailType("IDType",tLPEdorItemSchema.getEdorNo()))+
                "\n";

            }
            
            
        }

        textTag.add("JM_CHANGE", change);
    	
        return true;
    } 
    private String getEdorValueByDetailType(String DetailType,String Edorno)
    {
    	LPEdorEspecialDataDB tLPEdorEspecialDataDB=new LPEdorEspecialDataDB();
    	tLPEdorEspecialDataDB.setDetailType(DetailType);
    	tLPEdorEspecialDataDB.setEdorNo(mEdorNo);
    	tLPEdorEspecialDataDB.setEdorType("JM");
    	LPEdorEspecialDataSet tLPEdorEspecialDataSet=tLPEdorEspecialDataDB.query();
    	if(tLPEdorEspecialDataSet!=null&&tLPEdorEspecialDataSet.size()>0)
    	{
    		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema=tLPEdorEspecialDataSet.get(1);
    		return tLPEdorEspecialDataSchema.getEdorValue();
    	}
    	else
    	{
    		return null;
    	}
    	
    }
    
    
    //qulq 061230
   private String getSpecialRisk(String grpContNo) {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLCGrpPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo()) {
                return null;
            }
            String riskType = tLMRiskAppDB.getRiskType3();//特需
            String riskType8= tLMRiskAppDB.getRiskType8();//TPA
            
            if(riskType != null && riskType.equals(BQ.RISKTYPE3_SPECIAL))
            {
                return tLCGrpPolSet.get(i).getRiskCode();
            }
            
            if(riskType8 != null && riskType8.equals(BQ.RISKTYPE8_TPA))
            {
            	return tLCGrpPolSet.get(i).getRiskCode();
            }
        }
        return null;
    }

    /**
     * 特需医疗团体账户资金分配
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailGA(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        String edorValiDate = aLPGrpEdorItemSchema.getEdorValiDate();
// qulq modify
//        String insuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP);
        String riskcode = getSpecialRisk(grpContNo);
        if(riskcode == null)
        {
        	return false;
        }
        ExeSQL tExeSQL = new ExeSQL();
        String insuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP,riskcode);
        String sql = "select b.InsuAccBala from LCPol a, LCInsureAcc b " +
                     "where a.polNo = b.polNo " +
                     "and a.grpContNo = b.grpContNo " +
                     "and a.polTypeFlag = '" + BQ.POLTYPEFLAG_PUBLIC + "' " +
                     "and a.acctype = '" + BQ.ACCTYPE_PUBLIC + "' " + 
                     "and b.grpContNo = '" + grpContNo + "' " +
                     "and b.InsuAccNo = '" + insuAccNo + "' ";
        String grpMoney = tExeSQL.getOneValue(sql);
        sql = "select InsuAccBala from LPInsureAcc " +
              "where EdorNo = '" + edorNo + "' " +
              "and EdorType = '" + edorType + "' " +
              "and grpContNo = '" + grpContNo + "' " +
              "and InsuAccNo = '" + insuAccNo + "' ";
        String curGrpMoney = tExeSQL.getOneValue(sql);
        String changMoney =  CommonBL.bigDoubleToCommonString(CommonBL.carry(grpMoney) -
                                           CommonBL.carry(curGrpMoney),"0.00");
        textTag.add("GA_GrpContNo", grpContNo);
        textTag.add("GA_ChangMoney", changMoney);
        textTag.add("GA_CurGrpMoney", CommonBL.bigDoubleToCommonString(Double.parseDouble(curGrpMoney),"0.00"));
        textTag.add("GA_EdorValiDate", CommonBL.decodeDate(edorValiDate));
        return true;
    }
    
    /**
     * 补充A团体公共保额分配
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailGD(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
    	ExeSQL tExeSQL = new ExeSQL();
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        String edorValiDate = aLPGrpEdorItemSchema.getEdorValiDate();
        
        String str="select distinct a.riskcode from lcgrppol a "
			+ " where a.grpcontno='" + mGrpContNo + "'"
			+ " and exists (select 1 from lmrisktoacc where riskcode=a.riskcode)"
			+ " with ur";
		SSRS mSSRS=tExeSQL.execSQL(str);
		if(mSSRS.getMaxRow()!=1){
			 mErrors.addOneError("保单下存在多个险种！");
		     return false;
		}
		String riskcode =mSSRS.GetText(1, 1);
        if(riskcode == null)
        {
        	return false;
        }
        String insuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP,riskcode);
        String sql = "select b.InsuAccBala from LCPol a, LCInsureAcc b " +
                     "where a.polNo = b.polNo " +
                     "and a.grpContNo = b.grpContNo " +
                     "and a.polTypeFlag = '" + BQ.POLTYPEFLAG_PUBLIC + "' " +
                     "and a.acctype = '" + BQ.ACCTYPE_PUBLIC + "' " + 
                     "and b.grpContNo = '" + grpContNo + "' " +
                     "and b.InsuAccNo = '" + insuAccNo + "' ";
        String grpMoney = tExeSQL.getOneValue(sql);
        sql = "select InsuAccBala from LPInsureAcc " +
              "where EdorNo = '" + edorNo + "' " +
              "and EdorType = '" + edorType + "' " +
              "and grpContNo = '" + grpContNo + "' " +
              "and InsuAccNo = '" + insuAccNo + "' ";
        String curGrpMoney = tExeSQL.getOneValue(sql);
        String changMoney = String.valueOf(CommonBL.carry(grpMoney) -
                                           CommonBL.carry(curGrpMoney));
        textTag.add("GD_GrpContNo", grpContNo);
        textTag.add("GD_ChangMoney", changMoney);
        textTag.add("GD_CurGrpMoney", curGrpMoney);
        textTag.add("GD_EdorValiDate", CommonBL.decodeDate(edorValiDate));
        return true;
    }
    
    /**
     * 团体万能公共账户资金分配
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailTA(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        String edorValiDate = aLPGrpEdorItemSchema.getEdorValiDate();
// qulq modify
//        String insuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP);
        String riskcode = getUliRisk(grpContNo);
        if(riskcode == null)
        {
        	return false;
        }
        ExeSQL tExeSQL = new ExeSQL();
        String insuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP,riskcode);
        String sql = "select b.InsuAccBala from LCPol a, LCInsureAcc b " +
                     "where a.polNo = b.polNo " +
                     "and a.grpContNo = b.grpContNo " +
                     "and a.polTypeFlag = '" + BQ.POLTYPEFLAG_PUBLIC + "' " +
//                     "and a.acctype = '" + BQ.ACCTYPE_PUBLIC + "' " + 
                     "and b.grpContNo = '" + grpContNo + "' " +
                     "and b.InsuAccNo = '" + insuAccNo + "' ";
        String grpMoney = tExeSQL.getOneValue(sql);
        sql = "select InsuAccBala from LPInsureAcc " +
              "where EdorNo = '" + edorNo + "' " +
              "and EdorType = '" + edorType + "' " +
              "and grpContNo = '" + grpContNo + "' " +
              "and InsuAccNo = '" + insuAccNo + "' ";
        String curGrpMoney = tExeSQL.getOneValue(sql);
        String changMoney = String.valueOf(Arith.sub(CommonBL.carry(grpMoney), CommonBL.carry(curGrpMoney)));
        textTag.add("TA_GrpContNo", grpContNo);
        textTag.add("TA_ChangMoney", changMoney);
        textTag.add("TA_CurGrpMoney", curGrpMoney);
        
        //批单预览时生效日置为空，保全确认生成正式批单时置为保全确认次日2状态为保全理算后的状态
        System.out.println("保全生效日期edorValiDate"+edorValiDate);
        System.out.println("保全生效日期edorValiDate"+CommonBL.decodeDate(edorValiDate));
        textTag.add("TA_EdorValiDate", CommonBL.decodeDate(edorValiDate));
        
//      生成被保人清单
        PrtGrpInsuredListTABL tPrtGrpInsuredListTABL =
                new PrtGrpInsuredListTABL(mGlobalInput, mEdorNo);
        tPrtGrpInsuredListTABL.submitData();
        return true;
    }

    /**
     * 保单遗失补发
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailLR(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        if (!tLCGrpContDB.getInfo()) {
            mErrors.addOneError(new CError("相关的集体保单表中没有记录！"));
            return false;
        }
        tLCGrpContSchema = tLCGrpContDB.getSchema();
        textTag.add("LR_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        textTag.add("LR_EdorValiDate",
                    CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));
        textTag.add("LR_LostTimes", tLCGrpContSchema.getLostTimes() + 1);
        textTag.add("GBMoney", aLPGrpEdorItemSchema.getGetMoney()) ;
        if (aLPGrpEdorItemSchema.getGetMoney() > 0) {
            textTag.add("LR_GetMoney",
                        "因本次变更，投保人需补费" + aLPGrpEdorItemSchema.getGetMoney() +
                        "元。");
        }

        return true;
    }

    /**
     * 保单暂停
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailRS(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        if (!tLCGrpContDB.getInfo()) {
            mErrors.addOneError(new CError("相关的集体保单表中没有记录！"));
            return false;
        }
        tLCGrpContSchema = tLCGrpContDB.getSchema();
        textTag.add("RS_GrpName", tLCGrpContSchema.getGrpName());
        String tDate=new ExeSQL().getOneValue("select edorvalue from lpedorespecialdata where edorno ='"+aLPGrpEdorItemSchema.getEdorNo()+"' and edortype='RS' and detailtype='STOPDATE'");
        textTag.add("RS_Date",tDate);

        return true;
    }
    
    /**
     * 保单恢复
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailRR(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        if (!tLCGrpContDB.getInfo()) {
            mErrors.addOneError(new CError("相关的集体保单表中没有记录！"));
            return false;
        }
        tLCGrpContSchema = tLCGrpContDB.getSchema();
        textTag.add("RR_GrpName", tLCGrpContSchema.getGrpName());
        String tDate=new ExeSQL().getOneValue("select edorvalue from lpedorespecialdata where edorno ='"+aLPGrpEdorItemSchema.getEdorNo()+"' and edortype='RR' and detailtype='RESUMEDATE'");
        textTag.add("RR_Date",tDate);

        return true;
    }

    //理赔帐户变更
    private boolean getDetailLP(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        String tSQL = "select count(1) from lpinsured where edorno = '"
                      + aLPGrpEdorItemSchema.getEdorNo() + "' and edortype = '"
                      + aLPGrpEdorItemSchema.getEdorType() + "'";
        ExeSQL tExeSQL0 = new ExeSQL();
        String tCount = tExeSQL0.getOneValue(tSQL);
        textTag.add("LP_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        if (!tCount.equals("0") && !tCount.equals("")) {
            xmlexport.addDisplayControl("displayLP_2");
            textTag.add("LP_GrpInusredNum", tCount);
        }

        LPGrpAppntDB tLPGrpAppntDB = new LPGrpAppntDB();
        tLPGrpAppntDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        tLPGrpAppntDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        tLPGrpAppntDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        if (tLPGrpAppntDB.getInfo()) {
            xmlexport.addDisplayControl("displayLP_1");
            textTag.add("LP_BankCode_New",
                        CommonBL.notNull(ChangeCodeBL.getCodeName("bank",
                    tLPGrpAppntDB.getClaimBankCode(), "BankCode")));
            textTag.add("LP_BankAccNo_New",
                        CommonBL.notNull(tLPGrpAppntDB.getClaimBankAccNo()));
            textTag.add("LP_AccName_New",
                        CommonBL.notNull(tLPGrpAppntDB.getClaimAccName()));

            LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
            tLCGrpAppntDB.setGrpContNo(tLPGrpAppntDB.getGrpContNo());
            tLCGrpAppntDB.getInfo();
            textTag.add("LP_BankCode_Old",
                        CommonBL.notNull(ChangeCodeBL.getCodeName("bank",
                    tLCGrpAppntDB.getClaimBankCode(), "BankCode")));
            textTag.add("LP_BankAccNo_Old",
                        CommonBL.notNull(tLCGrpAppntDB.getClaimBankAccNo()));
            textTag.add("LP_AccName_Old",
                        CommonBL.notNull(tLCGrpAppntDB.getClaimAccName()));
        }
        return true;
    }

    //服务频次变更
    private boolean getDetailFP(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        if (!tLCGrpContDB.getInfo()) {
            mErrors.addOneError(new CError("相关的集体保单表中没有记录！"));
            return false;
        }
        LCGrpContSchema tLCGrpContSchema = tLCGrpContDB.getSchema();

        ExeSQL tExeSQL0 = new ExeSQL();
        GrpBalanceOnTime tGrpBalanceOnTime = new GrpBalanceOnTime();
        String tSQL = "select (select ServChooseRemark from LDServChooseInfo "
                      +
                      "where servkind=a.servkind and servdetail=a.servdetail "
                      + "and ServChoose=a.servChoose) from lpgrpservinfo a "
                      + "where a.edorno='" + aLPGrpEdorItemSchema.getEdorNo()
                      + "' and edortype = '" + aLPGrpEdorItemSchema.getEdorType()
                      + "' and a.servkind='1' and a.servdetail='2'";
        String FP_ServChoose1_New = tExeSQL0.getOneValue(tSQL);
        if (!FP_ServChoose1_New.equals("")) {
            xmlexport.addDisplayControl("displayFP_1");
            textTag.add("FP_ServChoose1_New", FP_ServChoose1_New);
            tSQL = "select (select ServChooseRemark from LDServChooseInfo "
                   + "where servkind=a.servkind and servdetail=a.servdetail "
                   + "and ServChoose=a.servChoose) from lcgrpservinfo a, lcgrpcont b "
                   + "where a.proposalgrpcontno=b.proposalgrpcontno and b.grpcontno='" + aLPGrpEdorItemSchema.getGrpContNo()
                   + "' and a.servkind='1' and a.servdetail='2'";
            String FP_ServChoose1_Old = tExeSQL0.getOneValue(tSQL);
            if (FP_ServChoose1_Old.equals("")) {
                FP_ServChoose1_Old = "随时";
            }

            textTag.add("FP_ServChoose1_Old", FP_ServChoose1_Old);
        }

        tSQL = "select (select ServChooseRemark from LDServChooseInfo "
               + "where servkind=a.servkind and servdetail=a.servdetail "
               + "and ServChoose=a.servChoose) from lpgrpservinfo a "
               + "where a.edorno='" + aLPGrpEdorItemSchema.getEdorNo()
               + "' and edortype = '" + aLPGrpEdorItemSchema.getEdorType()
               + "' and a.servkind='1' and a.servdetail='3'";
        String FP_ServChoose2_New = tExeSQL0.getOneValue(tSQL);
        if (!FP_ServChoose2_New.equals("")) {
            xmlexport.addDisplayControl("displayFP_2");
            textTag.add("FP_ServChoose2_New", FP_ServChoose2_New);
            tSQL = "select (select ServChooseRemark from LDServChooseInfo "
                   + "where servkind=a.servkind and servdetail=a.servdetail "
                   + "and ServChoose=a.servChoose) from lcgrpservinfo a ,lcgrpcont b "
                   + "where a.proposalgrpcontno=b.proposalgrpcontno and b.grpcontno='" + aLPGrpEdorItemSchema.getGrpContNo()
                   + "' and a.servkind='1' and a.servdetail='3'";
            String FP_ServChoose2_Old = tExeSQL0.getOneValue(tSQL);
            if (FP_ServChoose2_Old.equals("")) {
                FP_ServChoose2_Old = "随时";
            }

            textTag.add("FP_ServChoose2_Old", FP_ServChoose2_Old);

            LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
            tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
            tLPEdorAppDB.getInfo();

            tSQL = "select a.servChoose from lpgrpservinfo a "
                   + "where a.edorno='" + aLPGrpEdorItemSchema.getEdorNo()
                   + "' and edortype = '" + aLPGrpEdorItemSchema.getEdorType()
                   + "' and a.servkind='1' and a.servdetail='3'";
            String servChoose = tExeSQL0.getOneValue(tSQL);
            if (servChoose.equals("")) {
                CError tError = new CError();
                tError.moduleName = "PrtAppEndorsementBL";
                tError.functionName = "getDetailFP";
                tError.errorMessage = "lpgrpservinfo不存在相应的记录!";
                this.mErrors.addOneError(tError);
                return false;

            } else if (servChoose.equals("4")) {
                textTag.add("FP_NextBalance_Date", "");
            } else if (servChoose.equals("1")) {
                textTag.add("FP_NextBalance_Date",
                            "下一结算日为：" +
                            tGrpBalanceOnTime.getNextDate(tLCGrpContSchema.
                        getCValiDate(), 1, "M", PubFun.getCurrentDate()));
            } else if (servChoose.equals("2")) {
                textTag.add("FP_NextBalance_Date",
                            "下一结算日为：" +
                            tGrpBalanceOnTime.getNextDate(tLCGrpContSchema.
                        getCValiDate(), 3, "M", PubFun.getCurrentDate()));
            } else if (servChoose.equals("3")) {
                textTag.add("FP_NextBalance_Date",
                            "下一结算日为：" +
                            tGrpBalanceOnTime.getNextDate(tLCGrpContSchema.
                        getCValiDate(), 6, "M", PubFun.getCurrentDate()));
            } else if (servChoose.equals("5")) {
                textTag.add("FP_NextBalance_Date",
                            "下一结算日为：" +
                            tGrpBalanceOnTime.getNextDate(tLCGrpContSchema.
                        getCValiDate(), 12, "M", PubFun.getCurrentDate()));
            } else {
                CError tError = new CError();
                tError.moduleName = "PrtAppEndorsementBL";
                tError.functionName = "getDetailFP";
                tError.errorMessage = "lpgrpservinfo有与LDCODE不相应的记录!";
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        textTag.add("FP_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        return true;
    }

    /**
     * getDetailPR
     *
     * @param tLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailPR(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        if (!tLCGrpContDB.getInfo()) {
            mErrors.addOneError(new CError("相关的集体保单表中没有记录！"));
            return false;
        }
        tLCGrpContSchema = tLCGrpContDB.getSchema();
        textTag.add("PR_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        LPGrpAddressDB tLPGrpAddressDB = new LPGrpAddressDB();

        String sql = "select * from lpGrpaddress where EdorNo = '" +
                     aLPGrpEdorItemSchema.getEdorNo()
                     + "' and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() +
                     "' ";
        textTag.add("PR_PostalAddress",
                    tLPGrpAddressDB.executeQuery(sql).get(1).getGrpAddress());

        String sqlCom =
                "select * from ldcode where codetype='station' and code='" +
                aLPGrpEdorItemSchema.getManageCom() + "'";
        LDCodeDB tLDCodeDB = new LDCodeDB();
        textTag.add("PR_ManageCom",
                    tLDCodeDB.executeQuery(sqlCom).get(1).getCodeName());
        return true;
    }


    /**
     * 追加保费
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailZB(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        String edorValiDate = aLPGrpEdorItemSchema.getEdorValiDate();
        double getMoney = aLPGrpEdorItemSchema.getGetMoney();
        textTag.add("ZB_GrpContNo", grpContNo);
        textTag.add("ZB_GetMoney", CommonBL.bigDoubleToCommonString(getMoney,"0.00"));
        textTag.add("ZB_EdorValiDate", CommonBL.decodeDate(edorValiDate));

        String grpPolNo = null;
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        tLCGrpPolDB.setAppFlag(BQ.APPFLAG_SIGN);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            grpPolNo = tLCGrpPolSet.get(i).getGrpPolNo();
            if (CommonBL.isEspecialPol(tLCGrpPolSet.get(i).getRiskCode())) {
                break;
            }
        }
        if (grpPolNo == null) {
            return false;
        }

        String content = "";
        EdorItemSpecialData specialData =
                new EdorItemSpecialData(edorNo, edorType);
        specialData.query();
        specialData.setGrpPolNo(grpPolNo);
        String grpGetMoney = specialData.getEdorValue("GrpGetMoney");
        if ((grpGetMoney != null) && (!grpGetMoney.equals(""))) {
            content += "团体医疗账户：增加金额" +
            		CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("GrpGetMoney")),"0.00") + "元，" +
                    "扣除手续费：" +
                    CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("GrpManageMoney")),"0.00") + "元，" +
                    "当前该团体医疗账户余额：" +
                    CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("GrpLeftMoney")),"0.00") + "元。\n";
        }
        String insuredGetMoney = specialData.getEdorValue("InsuredGetMoney");
        if ((insuredGetMoney != null) && (!insuredGetMoney.equals(""))) {
            content += "个人医疗账户：本次共增加金额" +
            		CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("InsuredGetMoney")),"0.00") + "元，" +
                    "扣除手续费：" +
                    CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("InsuredManageMoney")),"0.00") + "元，" +
                    "本次个人医疗账户追加保费情况详见所附清单。";
        }
        textTag.add("ZB_Content", content);
        return true;
    }
    
    

    /**
     * 团体医疗追加保费
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailZA(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        String edorValiDate = aLPGrpEdorItemSchema.getEdorValiDate();
        double getMoney = aLPGrpEdorItemSchema.getGetMoney();
        textTag.add("ZA_GrpContNo", grpContNo);
        textTag.add("ZA_GetMoney", getMoney);
        textTag.add("ZA_EdorValiDate", CommonBL.decodeDate(edorValiDate));

        String grpPolNo = null;
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        tLCGrpPolDB.setAppFlag(BQ.APPFLAG_SIGN);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            grpPolNo = tLCGrpPolSet.get(i).getGrpPolNo();
        }
        if (grpPolNo == null) {
            return false;
        }

        String content = "";
        EdorItemSpecialData specialData =
                new EdorItemSpecialData(edorNo, edorType);
        specialData.query();
        specialData.setGrpPolNo(grpPolNo);
        String grpGetMoney = specialData.getEdorValue("GrpGetMoney");
        if ((grpGetMoney != null) && (!grpGetMoney.equals(""))) {
            content += "本次公共部分追加保费" +
                    specialData.getEdorValue("GrpGetMoney") + "元，\n";
        }
        String insuredGetMoney = specialData.getEdorValue("InsuredGetMoney");
        if ((insuredGetMoney != null) && (!insuredGetMoney.equals(""))) {
        	
        	content += "本次个人追加保费情况详见所附清单。";
        }
        textTag.add("ZA_Content", content);
        return true;
    }
    /**
     * 团体万能追加保费
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailTY(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        String edorValiDate = aLPGrpEdorItemSchema.getEdorValiDate();
        double getMoney = aLPGrpEdorItemSchema.getGetMoney();
        textTag.add("TY_GrpContNo", grpContNo);
        textTag.add("TY_GetMoney", CommonBL.bigDoubleToCommonString(getMoney,"0.00"));
        textTag.add("TY_EdorValiDate", CommonBL.decodeDate(edorValiDate));

        String grpPolNo = null;
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        tLCGrpPolDB.setAppFlag(BQ.APPFLAG_SIGN);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            grpPolNo = tLCGrpPolSet.get(i).getGrpPolNo();
            if (CommonBL.isEspecialPol(tLCGrpPolSet.get(i).getRiskCode())) {
                break;
            }
        }
        if (grpPolNo == null) {
            return false;
        }

        String content = "";
        EdorItemSpecialData specialData =
                new EdorItemSpecialData(edorNo, edorType);
        specialData.query();
        specialData.setGrpPolNo(grpPolNo);
        String grpGetMoney = specialData.getEdorValue("GrpGetMoney");
        if ((grpGetMoney != null) && (!grpGetMoney.equals(""))) {
            content += "公共账户：增加金额" +
                    specialData.getEdorValue("GrpGetMoney") + "元，" +
                    "扣除初始费用：" +
                    specialData.getEdorValue("GrpManageMoney") + "元，" +
                    "原公共账户余额：" +
                    specialData.getEdorValue("GrpBeforeMoney") + "元，" +
                    "当前该公共账户余额：" +
                    specialData.getEdorValue("GrpLeftMoney") + "元。\n";
        }
        String insuredGetMoney = specialData.getEdorValue("InsuredGetMoney");
        if ((insuredGetMoney != null) && (!insuredGetMoney.equals(""))) {
            content += "个人账户：本次共增加金额" +
                    specialData.getEdorValue("InsuredGetMoney") + "元，" +
                    "扣除初始费用：" +
                    specialData.getEdorValue("InsuredManageMoney") + "元，" +
                    "其中单位缴费共计：" +
                    specialData.getEdorValue("insuredGrpMoney") + "元，" +
                    "个人缴费共计：" +
                    specialData.getEdorValue("insuredPersonMoney") + "元，" +
                    "本次个人账户追加保费情况详见所附清单。";
        }
        textTag.add("TY_Content", content);
        return true;
    }
    
    /**
     * 追加管理费
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailZG(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        String edorValiDate = aLPGrpEdorItemSchema.getEdorValiDate();
        double getMoney = aLPGrpEdorItemSchema.getGetMoney();
        textTag.add("ZG_GrpContNo", grpContNo);
        textTag.add("ZG_GetMoney", getMoney);
        textTag.add("ZG_EdorValiDate", CommonBL.decodeDate(edorValiDate));
        System.out.println("ZG_GetMoney-"+getMoney);
        System.out.println("ZG_EdorValiDate-"+CommonBL.decodeDate(edorValiDate));
        return true;
    }
    
    /**
     * 团险部分领取
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private  boolean getDetailTL(LPGrpEdorItemSchema aLPGrpEdorItemSchema){//团险部分领取功能


        
       	//批单上 EdorItemSpecialData中数据存储:得到相应的费用
            EdorItemSpecialData specialData = new EdorItemSpecialData(aLPGrpEdorItemSchema);
            if(!specialData.query())
            {
            	mErrors.addOneError("未找到本次领取金额！");
                return false;
            }
            String [] polnos = specialData.getGrpPolNos();
            String polno = polnos[0];
            specialData.setPolNo(polno);
            
             String   GrpGetMoney = specialData.getEdorValue("GrpGetMoney");//公共帐户领取金额:GrpGetMoney 
             String   GrpManageMoney = specialData.getEdorValue("GrpManageMoney");//退保费用:GrpManageMoney
             String   grpBeforeMoney = specialData.getEdorValue("GRPBEFOREMONEY");//公共帐户领取之前余额:GRPBEFOREMONEY 
             String   GrpLeftMoney = specialData.getEdorValue("GrpLeftMoney");//退保费用:GrpManageMoney
             
         	 String   InsuredGetMoney = specialData.getEdorValue("INSUREDGETMONEY"); // 个人帐户领取金额合计:InsuredGetMoney 
             String   InsuredManageMoney = specialData.getEdorValue("INSUREDMANAGEMONEY");//退保费用合计:InsuredManageMoney
             
             if(GrpGetMoney==null){
                 textTag.add("GrpGetMoney","0" );
             }else{
                 textTag.add("GrpGetMoney",GrpGetMoney );             
             }
             if(GrpManageMoney==null){
                 textTag.add("GrpManageMoney","0" );
             }else{
                 textTag.add("GrpManageMoney",GrpManageMoney );
             }
             if(grpBeforeMoney==null){
            	 textTag.add("grpBeforeMoney","0" );
             }else{
            	 textTag.add("grpBeforeMoney",grpBeforeMoney );
             }
             if(GrpLeftMoney==null){
            	 textTag.add("GrpLeftMoney","0" );
             }else{
            	 textTag.add("GrpLeftMoney",GrpLeftMoney );
             }
             if(InsuredGetMoney==null){
                 textTag.add("InsuredGetMoney","0.0" );
             }else{
                 textTag.add("InsuredGetMoney",InsuredGetMoney );
             }
             if(InsuredManageMoney==null){
                 textTag.add("InsuredManageMoney","0.0" );
             }else{
                 textTag.add("InsuredManageMoney", InsuredManageMoney);
             }
             
             //生成被保人清单
             PrtGrpInsuredListTLBL tPrtGrpInsuredListTLBL =
                     new PrtGrpInsuredListTLBL(mGlobalInput, mEdorNo);
             tPrtGrpInsuredListTLBL.submitData();
           
            return true;

       }
   
    
    /**
     * 追加保费
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailLQ(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        String edorValiDate = aLPGrpEdorItemSchema.getEdorValiDate();
        //double getMoney = aLPGrpEdorItemSchema.getGetMoney();
        textTag.add("LQ_GrpContNo", grpContNo);
        textTag.add("LQ_EdorValiDate", CommonBL.decodeDate(edorValiDate));

        String grpPolNo = null;
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        tLCGrpPolDB.setAppFlag(BQ.APPFLAG_SIGN);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            //qulq 061228 modify
            String riskcode = tLCGrpPolSet.get(i).getRiskCode();
//            grpPolNo = tLCGrpPolSet.get(i).getGrpPolNo();
            if (CommonBL.isEspecialPol(riskcode))
            {
                grpPolNo = tLCGrpPolSet.get(i).getGrpPolNo();
                break;
            }
        }
        if (grpPolNo == null)
        {
            return false;
        }

        String content = "";
        EdorItemSpecialData specialData =
                new EdorItemSpecialData(edorNo, edorType);
        specialData.query();
        specialData.setGrpPolNo(grpPolNo);
        double grpMoney = 0.0;
        try
        {
            grpMoney = Double.parseDouble(specialData.getEdorValue(
                    "GrpMoney"));
        }
        catch (NumberFormatException ex)
        {
            ex.printStackTrace();
            grpMoney = 0;
        }
        if (grpMoney != 0)
        {
            content += "团体账户：减少金额" +
            		CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("GrpMoney")),"0.00") + "元，" +
            		"当前该团体医疗账户余额：" +
            		CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("GrpLeftMoney")),"0.00") + "元，\n" +
            		"扣除手续费：" +
            		CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("GrpManageMoney")),"0.00") + "元，" +
            		"应领取金额：" + CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("GrpGetMoney")),"0.00")
            		+ "元。\n";
        }

        double grpFixMoney = 0.0;
        try
        {
            grpFixMoney = Double.parseDouble(specialData.getEdorValue(
                    "GrpFixMoney"));
        }
        catch (NumberFormatException ex)
        {
            ex.printStackTrace();
            grpMoney = 0;
        }
        if (grpFixMoney != 0)
        {
            content += "团体固定账户：减少金额" +
            		CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("GrpFixMoney")),"0.00") + "元，" +
                    "当前该团体固定账户余额：" +
                    CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("GrpFixLeftMoney")),"0.00") + "元，\n" +
                    "扣除手续费：" +
                    CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("GrpFixManageMoney")),"0.00") + "元，" +
                    "应领取金额：" + CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("GrpFixGetMoney")),"0.00")
                    + "元。\n";
        }

        double insuredGetMoney = 0.0;
        try
        {
            insuredGetMoney = Double.parseDouble(specialData.
                    getEdorValue("InsuredGetMoney"));
        }
        catch (NumberFormatException ex1)
        {
            ex1.printStackTrace();
            insuredGetMoney = 0.0;
        }
        if (insuredGetMoney != 0)
        {
            content += "个人医疗账户：本次共减少金额" +
            		CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("InsuredGetMoney")),"0.00") + "元，" +
                    "扣除手续费：" +
                    CommonBL.bigDoubleToCommonString(Double.parseDouble(specialData.getEdorValue("InsuredManageMoney")),"0.00") + "元，" +
                    "本次个人账户部分领取情况详见所附清单。\n";
        }
        content += "本变更自" + CommonBL.decodeDate(edorValiDate) + "开始生效";
        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("LQ");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);
        textTag.add("LQ_GetMoney",
                CommonBL.bigDoubleToCommonString(grpMoney + grpFixMoney + insuredGetMoney, "0.00"));
        return true;
    }


    //团险万能离职领取
    private boolean getDetailTQ(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
    	String tGrpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        tLPInsuredDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        tLPInsuredDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        tLPInsuredDB.setGrpContNo(tGrpContNo);
        LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();
        if (tLPInsuredSet.size() == 0) {
            mErrors.addOneError("查询被保人出错。");
            System.out.println("查询被保人出错。"
                               + tLPInsuredDB.mErrors.getErrContent());
            return false;
        }
        tlistTable = new ListTable();
        tlistTable.setName("TQ");
        textTag.add("InsuNum", tLPInsuredSet.size());
        textTag.add("GrpContNo", tGrpContNo);
        
        
        /*	将各项金额数据存到导入表lpdiskimport中,对应字段如下:
         *	归属比例 money2
         *	个人账户单位部分公共账户归属 appntprem
         *	个人账户单位部分个人账户归属 PersonOwnPrem
         *	个人账户个人部分 personprem
         *	退保费用(退保手续费) money
         *	保单管理费 getmoney
         *	本保单退费 insuaccbala
         */
        double  tSumPersonPrem=0.0;
        double  tSumPersonOwnPrem=0.0;
        double  tSumAppntPrem=0.0;
        double  tSumGetMoney=0.0;
        double  tSumMoney=0.0;
        
        SSRS edorSSRS = new ExeSQL().execSQL("select personprem,personownprem,appntprem,-getmoney,money from lpdiskimport where edorno='"+aLPGrpEdorItemSchema.getEdorNo()+"' and edortype='"+aLPGrpEdorItemSchema.getEdorType()+"'");
        for(int j=1;j<=edorSSRS.getMaxRow();j++)
        {
        	tSumPersonPrem += Double.parseDouble(edorSSRS.GetText(j,1));
        	tSumPersonOwnPrem += Double.parseDouble(edorSSRS.GetText(j,2));
        	tSumAppntPrem += Double.parseDouble(edorSSRS.GetText(j,3));
        	tSumGetMoney += Double.parseDouble(edorSSRS.GetText(j,4));
        	tSumMoney += Double.parseDouble(edorSSRS.GetText(j,5));
        }
        
        textTag.add("SumPersonPrem", Double.toString(CommonBL.carry(tSumPersonPrem)));
        textTag.add("SumPersonOwnPrem", Double.toString(CommonBL.carry(tSumPersonOwnPrem)));
        textTag.add("SumAppntPrem", Double.toString(CommonBL.carry(tSumAppntPrem)));
        textTag.add("SumGetMoney", Double.toString(CommonBL.carry(tSumGetMoney)));
        textTag.add("SumMoney1", Double.toString(CommonBL.carry(tSumMoney)));
        textTag.add("EdorValidate",getEdorValidate(aLPGrpEdorItemSchema));

        
        //生成被保人清单
        PrtGrpInsuredListTQBL tPrtGrpInsuredListTQBL =
                new PrtGrpInsuredListTQBL(mGlobalInput, mEdorNo);
        tPrtGrpInsuredListTQBL.submitData();
        return true;
    }
    
    //减人
    private boolean getDetailZT(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
    	String tGrpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        tLPInsuredDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        tLPInsuredDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        tLPInsuredDB.setGrpContNo(tGrpContNo);
        LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();
        if (tLPInsuredSet.size() == 0) {
            mErrors.addOneError("查询被保人出错。");
            System.out.println("查询被保人出错。"
                               + tLPInsuredDB.mErrors.getErrContent());
            return false;
        }
        tlistTable = new ListTable();
        tlistTable.setName("ZT");

        textTag.add("ZT_DecreaseInsuredCount", tLPInsuredSet.size());
        textTag.add("ZT_GrpContNo", tGrpContNo);
        //textTag.add("ZT_EdorValiDate", "变更生效日期为"
        //            + CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));
        textTag.add("ZT_GetMoney", Math.abs(aLPGrpEdorItemSchema.getGetMoney()));

        if (hasESpecialPol(aLPGrpEdorItemSchema)) {
            if (!setEspecialPolContentZT(aLPGrpEdorItemSchema)) {
                return false;
            }
        }
        if (hasCommonPol(aLPGrpEdorItemSchema)) {
            setCommonPolContentZT(aLPGrpEdorItemSchema,
                                  Math.abs(aLPGrpEdorItemSchema.getGetMoney()));
        }

        //计算下期期保费
        //if (getZTNextPrem(aLPGrpEdorItemSchema) != null)
        //{
        //    textTag.add("ZT_NextPrem", getZTNextPrem(aLPGrpEdorItemSchema));
        //}

        return true;
    }

    /**
     * 到减人费用类型：颓废，退账户余额
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return String
     */
    private boolean hasESpecialPol(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(aLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        for (int i = 1; i <= tLPEdorItemSet.size(); i++) {
            StringBuffer sql = new StringBuffer();
            sql.append("select a.* ")
                    .append("from LPPol a, LMRiskApp b ")
                    .append("where a.riskCode = b.riskCode ")
                    .append("   and a.contNo = '")
                    .append(tLPEdorItemSet.get(i).getContNo())
                    .append("'  and ((b.risktype3 is not null and b.riskType3 = '")
                    .append(BQ.RISKTYPE3_SPECIAL)
                    .append("') or (b.risktype8 is not null and b.riskType8 = '")
                    .append(BQ.RISKTYPE8_TPA).append("'))");
            LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql.toString());
            System.out.println(sql.toString());

            if (tLCPolSet.size() > 0) {
                return true;
            }
        }

        return false;
    }

    private boolean hasCommonPol(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(aLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        for (int i = 1; i <= tLPEdorItemSet.size(); i++) {
            StringBuffer sql = new StringBuffer();
            sql.append("select a.* ")
                    .append("from LPPol a, LMRiskApp b ")
                    .append("where a.riskCode = b.riskCode ")
                    .append("   and a.contNo = '")
                    .append(tLPEdorItemSet.get(i).getContNo())
                    .append("'  and (b.riskType3 is null or b.riskType3 != '")
                    .append(BQ.RISKTYPE3_SPECIAL)
                    .append("') and (b.riskType8 is null or b.riskType8 != '")
                    .append(BQ.RISKTYPE8_TPA)
                    .append("') ");
            LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql.toString());
            System.out.println(sql.toString());

            if (tLCPolSet.size() > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * 特需医疗险种退保批单信息
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @param ZT_GetMoney double
     * @return boolean
     */
    private boolean setEspecialPolContentZT(LPGrpEdorItemSchema
                                            aLPGrpEdorItemSchema) {
        EdorItemSpecialData tEdorItemSpecialData =
                new EdorItemSpecialData(aLPGrpEdorItemSchema);
        tEdorItemSpecialData.query();
        String edorValue = tEdorItemSpecialData
                           .getEdorValue(BQ.DETAILTYPE_BALAPAYTYPE);
        //放入团体账号
        if (edorValue.equals(BQ.DETAILTYPE_BALAPAYTYPE1)) {
            double grpAccBala = getGrpAccBala(aLPGrpEdorItemSchema)
                                + getSumInsuredAccBala(aLPGrpEdorItemSchema);
            DecimalFormat tDecimalFormat = new DecimalFormat("0.00");
            String totalAccMoney = tDecimalFormat.format(
                    Double.parseDouble(String.valueOf(grpAccBala)));

            StringBuffer tZT_Content = new StringBuffer("本次应退还帐户余额");
            tZT_Content.append(bigDoubleToCommonString(
                    getSumInsuredAccBala(aLPGrpEdorItemSchema), "0.00"))
                    .append("元，进入团体帐户，团体帐户余额")
                    .append(totalAccMoney)
                    .append("元。");
            textTag.add("ZT_EspecialPolContent", tZT_Content.toString());
        } else {
            double sumInsuredAccBala = getSumInsuredAccBala(
                    aLPGrpEdorItemSchema);
            double poundage = getPoundage(aLPGrpEdorItemSchema,
                                          sumInsuredAccBala); //手续费
            double returnMoney = PubFun.setPrecision(sumInsuredAccBala
                    - poundage, "0.00");

            StringBuffer tZT_Content = new StringBuffer("本次应退还个人帐户余额总计");
            tZT_Content.append(bigDoubleToCommonString(sumInsuredAccBala,
                    "0.00"))
                    .append("元，扣除手续费")
                    .append(bigDoubleToCommonString(poundage, "0.00"))
                    .append("元，实际退还投保单位")
                    .append(bigDoubleToCommonString(returnMoney, "0.00"))
                    .append("元。");
            textTag.add("ZT_EspecialPolContent", tZT_Content.toString());
        }

        return true;
    }

    /**
     * 若数据太大，double数据会以科学计数法的新式显示
     * 本方法用来将值value转换成字符串表示的pattern精度的正常显示
     * @param value Double
     * @param pattern String
     * @return String
     */
    private String bigDoubleToCommonString(double value, String pattern) {
        try {
            DecimalFormat tDecimalFormat = new DecimalFormat(pattern);
            String valueString = tDecimalFormat.format(value);
            return valueString;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 计算手续费：解约管理费
     * @return double
     */
    private double getPoundage(LPGrpEdorItemSchema aLPGrpEdorItemSchema,
                               double sumInsuredAccBala)
    {
        double rate = CommonBL.getManageFeeRateByCont(aLPGrpEdorItemSchema.getGrpContNo(), BQ.FEECODE_CT); //解约管理费比率
        return PubFun.setPrecision(rate * sumInsuredAccBala, "0.00");
    }

    /**
     * 得到团体理赔账号余额
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return String
     */
    private double getGrpAccBala(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        StringBuffer sql = new StringBuffer();
        String riskCode = CommonBL.getSpecialRisk(aLPGrpEdorItemSchema.getGrpContNo());
        sql.append("select insuAccBala ")
                .append("from LCPol a, LCInsureAcc b ")
                .append("where a.PolNo = b.PolNo and a.acctype = '").append(BQ.ACCTYPE_PUBLIC).append("' ")
                .append("   and a.GrpContNo = b.GrpContNo ")
                .append("and a.PolTypeFlag = '" + BQ.POLTYPEFLAG_PUBLIC)
                .append("'  and b.InsuAccNo = '")
                .append(CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP,riskCode))
                .append("'   and a.grpContNo = '")
                .append(aLPGrpEdorItemSchema.getGrpContNo())
                .append("' ");
        System.out.println(sql.toString());
        ExeSQL tExeSQL = new ExeSQL();
        String insureAccBala = tExeSQL.getOneValue(sql.toString());

        return Double.parseDouble(insureAccBala.equals("")
                                  ? "0" : insureAccBala);
    }

    /**
     * 减去的个人账户余额总和
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return double
     */
    private double getSumInsuredAccBala(LPGrpEdorItemSchema
                                        aLPGrpEdorItemSchema) {
        StringBuffer sql = new StringBuffer();
        String riskCode = CommonBL.getSpecialRisk(aLPGrpEdorItemSchema.getGrpContNo());
        sql.append("select sum(b.insuAccBala) ")
                .append("from LCPol a, LCInsureAcc b ")
                .append("where a.polNo = b.polNo ")
                .append("   and a.grpContNo = b.grpContNo ")
                .append("and a.polTypeFlag != '")
                .append(BQ.POLTYPEFLAG_PUBLIC)
                .append("'  and b.InsuAccNo = '")
                .append(CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,riskCode))
                .append("'  and a.contNo in (")
                .append("      select contNo ")
                .append("      from LPEdorItem ")
                .append("      where edorNo = '")
                .append(aLPGrpEdorItemSchema.getEdorNo())
                .append("'         and edorType = '")
                .append(aLPGrpEdorItemSchema.getEdorType())
                .append("') ")
                .append("     and a.insuredno in (")
                .append("      select insuredno ")
                .append("      from LPInsured ")
                .append("      where edorNo = '")
                .append(aLPGrpEdorItemSchema.getEdorNo())
                .append("'         and edorType = '")
                .append(aLPGrpEdorItemSchema.getEdorType())
                .append("') ")
                
                
                ;
        System.out.println(sql.toString());
        ExeSQL tExeSQL = new ExeSQL();
        String sumInsuredAccBala = tExeSQL.getOneValue(sql.toString());
        double returnValue = Double.parseDouble(sumInsuredAccBala.equals("")
                                                ? "0" : sumInsuredAccBala);
        return PubFun.setPrecision(returnValue, "0.00");
    }

    /**
     * 普通险种退保批单信息
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @param ZT_GetMoney double
     * @return boolean
     */
    private boolean setCommonPolContentZT(LPGrpEdorItemSchema
                                          aLPGrpEdorItemSchema,
                                          double ZT_GetMoney) {
        String sql = "  select sum(getMoney) "
                     + "from LJSGetEndorse a, LMRiskApp b "
                     + "where a.riskCode = b.riskCode "
                     + "and ((b.riskType3 is not null and b.riskType3 = '" + BQ.RISKTYPE3_SPECIAL
                     + "') or (b.riskType8 is not null and b.riskType8 = '" + BQ.RISKTYPE8_TPA
                     + "')) and a.feeOperationType = '"
                     + aLPGrpEdorItemSchema.getEdorType()
                     + "'  and a.endorsementNo = '"
                     + aLPGrpEdorItemSchema.getEdorNo() + "' ";
        ExeSQL e = new ExeSQL();
        String sumEspecialPolGetMoney = e.getOneValue(sql);
        double money = 0;
        if (!sumEspecialPolGetMoney.equals("")
            && !sumEspecialPolGetMoney.equals("null")) {
            money = Math.abs(Double.parseDouble(sumEspecialPolGetMoney));
        }
        String st = hasESpecialPol(aLPGrpEdorItemSchema) ? "其他险种" : "";
        StringBuffer tZT_Content = new StringBuffer("本次");
        tZT_Content.append(st).append("应退保费");
        tZT_Content.append(bigDoubleToCommonString(ZT_GetMoney - money, "0.00"))
                .append("元。");
        textTag.add("ZT_CommonPolContent", tZT_Content.toString());

        return true;
    }

    //得到下期期交保费
    private String getZTNextPrem(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        LPEdorItemDB db = new LPEdorItemDB();
        db.setEdorAcceptNo(this.mEdorNo);
        db.setEdorType("ZT");
        db.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        LPEdorItemSet set = db.query();

        if (set != null && set.size() > 0) {
            String contNos = "";
            for (int i = 0; i < set.size(); i++) {
                contNos += "'" + set.get(i + 1).getContNo() + "', ";
            }
            contNos = contNos.substring(0, contNos.length() - 2);

            String sql = "select round(sum(prem), 2) "
                         + "from LCCont where grpContNo = '"
                         + aLPGrpEdorItemSchema.getGrpContNo() + "' "
                         + "    and contNo not in ("
                         + contNos + ") ";
            System.out.println(sql);
            ExeSQL e = new ExeSQL();
            SSRS s = e.execSQL(sql);
            if (s != null && s.getMaxRow() > 0) {
                if (s.GetText(1, 1).indexOf(".") < 0) {
                    return s.GetText(1, 1) + ".00";
                } else {
                    return s.GetText(1, 1);
                }
            }
        }

        return null;
    }

    //险种退保
    private boolean getDetailLT(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        String tEdorNo = aLPGrpEdorItemSchema.getEdorNo();
        String tEdorType = aLPGrpEdorItemSchema.getEdorType();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        String sql = "select * from lcgrppol where grppolno in"
                     + " (select grppolno from lcpol where polno in"
                     + " (select polno from lpedoritem where edorno = '" +
                     tEdorNo
                     + "' and EdorType='" + tEdorType + "'))";
        tLCGrpPolSet = tLCGrpPolDB.executeQuery(sql);
        if (tLCGrpPolDB.mErrors.needDealError()) {
            CError.buildErr(this, "查询险种退保项目团体险种单时失败");
            return false;
        }
        String grppolno = "";
        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            if (i == tLCGrpPolSet.size()) {
                grppolno = grppolno + tLCGrpPolSet.get(i).getGrpPolNo();
            } else {
                grppolno = grppolno + tLCGrpPolSet.get(i).getGrpPolNo() + ",";
            }
        }
        textTag.add("LT_GrpPolNo", grppolno);
        textTag.add("LT_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        textTag.add("LT_EdorValiDate",
                    CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));
        textTag.add("LT_GetMoney", aLPGrpEdorItemSchema.getGetMoney());

        return true;
    }

    //团单合同终止
    private boolean getDetailCT(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        textTag.add("CT_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        textTag.add("CT_EdorValiDate",
                    CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));
        textTag.add("CT_Total", ""
                    + CommonBL.bigDoubleToCommonString(Math.abs(aLPGrpEdorItemSchema.getGetMoney()),"0.00"));
        ListTable tListTableCT = new ListTable();
        tListTableCT.setName("CT");
        ListTable tListTableCTEP = new ListTable();
        tListTableCTEP.setName("CTEP"); //特需险种列表
        
        ListTable tListTableCTULI = new ListTable();
        tListTableCTULI.setName("CTULI"); //团险万能信息列表

        //判断是否为团险万能解约
        boolean isULI = false;
        String hasULIRiskSQL = "select 1 from lcgrppol where GrpContNo = '"+aLPGrpEdorItemSchema.getGrpContNo()+"' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')";
        ExeSQL tExeSQL5 = new ExeSQL();
        if(tExeSQL5.getOneValue(hasULIRiskSQL)!=null&&!"".equals(tExeSQL5.getOneValue(hasULIRiskSQL))){
        	isULI = true;
        }
        if(!isULI){
        	//查询退保的保障计划
        	LPContPlanRiskDB tLPContPlanRiskDB = new LPContPlanRiskDB();
        	tLPContPlanRiskDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
        	tLPContPlanRiskDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        	tLPContPlanRiskDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        	LPContPlanRiskSet tLPContPlanRiskSet = tLPContPlanRiskDB.query();
        	if (tLPContPlanRiskSet.size() == 0) {
        		mErrors.addOneError("没有查询到退保的保障计划");
        		return false;
        	}
        }else{
        	getPrintContentULI(aLPGrpEdorItemSchema,tListTableCTULI);
        }
        getPrintContentESP(aLPGrpEdorItemSchema,tListTableCTEP);
        

        ExeSQL mExeSQL = new ExeSQL();
//        for (int i = 1; i <= tLPContPlanRiskSet.size(); i++) {

            String endorSql = "select b.contplancode,(select f.riskSeqNo from LCGrpPol f where f.grppolNo = a.grppolno),"
                         +" (select riskName from LMRisk where riskCode = b.riskcode),"
                         +" count(DISTINCT b.insuredno),"
                         +" (select min(CValiDate) from lppol where GRPPOLNO = A.GRPPOLNO  and edorno=a.EndorsementNo ),abs(sum(a.getmoney))"
                         +" from ljsgetendorse a ,lppol b where a.grppolno = b.grppolno and a.polno = b.polno "
                         +" and a.EndorsementNo='"+aLPGrpEdorItemSchema.getEdorNo()+"' and a.EndorsementNo = b.edorno and a.feeOperationType = 'CT'"
                         +" and a.riskcode not in (select riskcode from lmriskapp where (risktype3 ='7' or RiskType8='6') and (risktype ='H' or risktype ='O'))"
                         +" group by b.contplancode,a.grppolno,b.riskcode,a.EndorsementNo "
                         ;
            SSRS edorSSRS = mExeSQL.execSQL(endorSql);
            double CT_Money = 0.0;
            for(int j=1;j<=edorSSRS.getMaxRow();j++)
            {
               tListTableCT.add(edorSSRS.getRowData(j));
               CT_Money += Double.parseDouble(edorSSRS.GetText(j,6));
            }
            
            
            textTag.add("CT_Money", "险种共退费"+CommonBL.bigDoubleToCommonString(CT_Money, "0.00")+"元");
        
/*
            String[] printContent = new String[5];
            String sql = "select riskName from LMRisk where riskCode = '"
                         +tLPContPlanRiskSet.get(i).getRiskCode()
                         +"'";
            SSRS tSSRS = mExeSQL.execSQL(sql);
            if (tSSRS.getMaxRow() == 0) {
                mErrors.addOneError("没有查询到险种名称。");
                return false;
            }
             printContent[0] = StrTool.cTrim(tLPContPlanRiskSet.get(i).getContPlanName());
             printContent[2] = tSSRS.GetText(1,1);
            sql = "select count(1) from LCInsured c, LCPol d ,LCGrpPol f "
                  +" where c.contNo = d.contNo and c.insuredNo = d.insuredNo "
                  +" and c.contPlanCode = '"
                  +tLPContPlanRiskSet.get(i).getContPlanCode()
                  +"' and d.grpPolNo = f.grpPolNo "
                  + "   and f.grpContNo = '"
                  +tLPContPlanRiskSet.get(i).getGrpContNo()
                  + "'   and f.riskCode = '"
                  +tLPContPlanRiskSet.get(i).getRiskCode()
                  +"'"
                  ;
            tSSRS = mExeSQL.execSQL(sql);
            if (tSSRS.getMaxRow() == 0)
            {
                mErrors.addOneError("查询保障计划错误");
                return false;
            }


                printContent[3] = tSSRS.GetText(1,1);
                sql = "select f.riskSeqNo from LCGrpPol f where f.grpContNo = '"
                      +tLPContPlanRiskSet.get(i).getGrpContNo()
                      + "'   and f.riskCode = '"
                      +tLPContPlanRiskSet.get(i).getRiskCode()
                      +"'"
                      ;
                tSSRS = mExeSQL.execSQL(sql);
                if (tSSRS.getMaxRow() == 0)
                {
                    mErrors.addOneError("查询险种序号错误");
                    return false;
                }


                 printContent[1] = tSSRS.GetText(1,1);


                sql = "select CValiDate from lppol where grpcontno ='"
                      +tLPContPlanRiskSet.get(i).getGrpContNo()
                      +"'"
                      ;
                tSSRS = mExeSQL.execSQL(sql);
                if (tSSRS.getMaxRow() == 0)
                {
                    mErrors.addOneError("查询生效日期失败");
                    return false;
                }
                printContent[4] = tSSRS.GetText(1,1);
                tListTableCT.add(printContent);
  */

//        }
        //061123 QULQ
            String edorNameAndContInfo = null;
            if(!isULI){
        //判断是整单退还是退险种，处理方式不一样
       String  sql = "select count(distinct contPlanCode) "
              + "from LCPol "
              + "where grpContNo = '"
              + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        String rs = new ExeSQL().getOneValue(sql); //保单保障计划数
        if (rs.equals("") || rs.equals("null")) {
            CError tError = new CError();
            tError.moduleName = "PrtGrpEndorsementBL";
            tError.functionName = "getDetailWT";
            tError.errorMessage = "查询保单保障计划失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        int contPlanCount = Integer.parseInt(rs);

//        tLPContPlanRiskDB = new LPContPlanRiskDB();
//        tLPContPlanRiskDB.setEdorNo(aLPGrpEdorItemSchema.getEdorNo());
//        tLPContPlanRiskDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
//        tLPContPlanRiskDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
//        LPContPlanRiskSet set = tLPContPlanRiskDB.query();
        String psql = "select count(distinct contplancode) "
                      +" from lpcontplanrisk "
                      +" where edorno ='"
                      +aLPGrpEdorItemSchema.getEdorNo()
                      +"' and edortype = '"
                      +aLPGrpEdorItemSchema.getEdorType()
                      +"' and grpcontno = '"
                      +aLPGrpEdorItemSchema.getGrpContNo()
                      +"'"
                      ;
        String setsize = new ExeSQL().getOneValue(psql); //保全保障计划数
        int set = Integer.parseInt(setsize);
        if (contPlanCount == set) {
            edorNameAndContInfo = "保单号" + aLPGrpEdorItemSchema.getGrpContNo()
                   + "办理解约，保单效力自" +CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate())
                   + "无效，共退还保险金"
                   + CommonBL.bigDoubleToCommonString(Math.abs(aLPGrpEdorItemSchema.getGetMoney()),"0.00") + "元。";
        }
        } else {
            edorNameAndContInfo = "保单号" + aLPGrpEdorItemSchema.getGrpContNo()
                   + "项下以下险种办理解约，险种效力自"+CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate())
                   +"无效，共退还保险金"
                   + CommonBL.bigDoubleToCommonString(Math.abs(aLPGrpEdorItemSchema.getGetMoney()),"0.00") + "元。";
        }

        //特需类险种
        if (tListTableCTEP.size() > 0) {
            //若存在特需险种，则将项目名和保单信息显示在特需险种之前
            String sqlBJ = "select abs(SUM(GETMONEY)) FROM LJSGETENDORSE where EndorsementNo='"
               +aLPGrpEdorItemSchema.getEdorNo()
               +"' and ( riskcode in (select RISKCODE from lmriskapp where (risktype3 = '7' or RiskType8='6') and (risktype ='H' or risktype ='O') )"
               +" or riskcode ='000000' ) " //录入利息的险种编码是“000000” add by fuxin  2009-11-10
               ;
            mExeSQL = new ExeSQL();
            SSRS tSSRS = mExeSQL.execSQL(sqlBJ);
            xmlexport.addDisplayControl("displayCTEP");
            xmlexport.addListTable(tListTableCTEP, new String[0]);
            textTag.add("CTEP_EdorName", edorNameAndContInfo);
            textTag.add("sumCTEPMoney", CommonBL.bigDoubleToCommonString(Double.parseDouble(tSSRS.GetText(1,1)),"0.00"));
        }
        if(tListTableCTULI.size()>0){
            //若存在特需险种，则将项目名和保单信息显示在特需险种之前
            String sqlBJ = "select abs(SUM(GETMONEY)) FROM LJSGETENDORSE where EndorsementNo='"
               +aLPGrpEdorItemSchema.getEdorNo()
               +"' and ( riskcode in (select RISKCODE from lmriskapp where risktype4 = '4'  )"
               +" or riskcode ='000000' ) " //录入利息的险种编码是“000000” add by fuxin  2009-11-10
               ;
            mExeSQL = new ExeSQL();
            SSRS tSSRS = mExeSQL.execSQL(sqlBJ);
            xmlexport.addDisplayControl("displayCTULI");
            xmlexport.addListTable(tListTableCTULI, new String[0]);
            textTag.add("CTULI_EdorName", edorNameAndContInfo);
            textTag.add("sumCTULIMoney", CommonBL.bigDoubleToCommonString(Double.parseDouble(tSSRS.GetText(1,1)),"0.00"));        	
        }
        if (tListTableCT.size() > 0) {
            if (tListTableCTEP.size() <= 0&&tListTableCTULI.size()<=0) {
                //若只有其它险种，则将项目名和保单信息显示在其他险种之前
                textTag.add("CT_EdorName", edorNameAndContInfo);
                String strhead[] = new String[3];
                strhead[0] = "保险号";
                strhead[1] = "险种名称";
                strhead[2] = "退险金额";
                xmlexport.addDisplayControl("displayCT");
                xmlexport.addListTable(tListTableCT, strhead);
            } else if(tListTableCTEP.size() > 0&&tListTableCTULI.size()<=0){
                //若两类险种都有
                textTag.add("CT_EdorName", "普通险种退费信息：");
                String strhead[] = new String[3];
                strhead[0] = "保险号";
                strhead[1] = "险种名称";
                strhead[2] = "退险金额";
                xmlexport.addDisplayControl("displayCT");
                xmlexport.addListTable(tListTableCT, strhead);
            }
        }
        createAppAccList(aLPGrpEdorItemSchema);
        return true;
    }
    
    /**
     * 特需退费信息
     * @return String[]
     */
    private void getPrintContentULI(LPGrpEdorItemSchema aLPGrpEdorItemSchema,ListTable tListTableCTULI) {

            //查询万能险种信息
        String sqlBJ = "select DISTINCT riskcode , grppolno FROM lcgrppol where grpcontno='"
                       +aLPGrpEdorItemSchema.getGrpContNo()
                       +"' and riskcode in (select RISKCODE from lmriskapp where RiskType4 = '4' )" ;
        ExeSQL mExeSQL = new ExeSQL();
        SSRS tSSRS = mExeSQL.execSQL(sqlBJ);
        if (tSSRS.getMaxRow() == 0) {
            return ;
        }
        String grpPolNo = tSSRS.GetText(1,2);
        String riskCode = tSSRS.GetText(1,1);
        
        //公共账户账户编码
        String inGsuaccno = new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+riskCode+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='3')) ");
      //获取个人账户单位部分保险账户号码
        String tGrpInsuNo = new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+riskCode+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='4')) ");
        //个人账户
        String tInsuNo =  new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+riskCode+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')) ");
        String info1[] = new String[6];
        String info2[] = new String[6];
        String info3[] = new String[6];
        String info4[] = new String[6];
        String insureAccNo[] = null;
        if("370301".equals(riskCode)){
        	insureAccNo = new String[1];
        	insureAccNo[0] = inGsuaccno;
        }else{
        	insureAccNo = new String[3];
        	insureAccNo[0] = inGsuaccno;
        	insureAccNo[1] = tGrpInsuNo;
        	insureAccNo[2] = tInsuNo;
        }
        
        double sumLastBala = 0.0 ;
        double sumNowLX = 0.0 ;
        double sumNowMF = 0.0 ;
        double sumNowTM = 0.0 ;
        double sumNowCT = 0.0 ;
        for(int i=0;i<insureAccNo.length;i++){
        	String queryMoneySQL = "select insuaccno, " +
        	" (case when insuaccno = '"+inGsuaccno+"' then '公共账户' when insuaccno = '"+tGrpInsuNo+"' then '个人账户单位部分' when " +
        	" insuaccno = '"+tInsuNo+"' then '个人账户个人部分' else '个人直接缴费部分' end ) 账户类型, " +
        	" sum(money) as 解约日本金, " +
        	" (SELECT sum(case when otherno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' and moneytype='LX' then money else 0 end ) " +
        	" FROM LPINSUREACCTRACE WHERE grpcontno='"+aLPGrpEdorItemSchema.getGrpContNo()+"' AND insuaccno = '"+insureAccNo[i]+"' and  edorno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' ) 补发利息,  " +
        	" (SELECT sum(case when otherno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' and moneytype='MF' then money else 0 end )  " +
        	" FROM LPINSUREACCTRACE WHERE grpcontno='"+aLPGrpEdorItemSchema.getGrpContNo()+"' AND insuaccno = '"+insureAccNo[i]+"' and  edorno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' ) 月结管理费, " +
        	" (SELECT sum(case when otherno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' and moneytype='TM' then money else 0 end ) " +
        	" FROM LPINSUREACCTRACE WHERE grpcontno='"+aLPGrpEdorItemSchema.getGrpContNo()+"' AND insuaccno = '"+insureAccNo[i]+"' and  edorno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' ) 解约管理费, " +
        	" (SELECT sum(case when otherno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' and moneytype='CT' then money else 0 end ) " +
        	"  FROM LPINSUREACCTRACE WHERE grpcontno='"+aLPGrpEdorItemSchema.getGrpContNo()+"' AND insuaccno = '"+insureAccNo[i]+"' and  edorno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' )  该账户退费   " +
        	" from lcinsureacctrace where grpcontno='"+aLPGrpEdorItemSchema.getGrpContNo()+"' AND insuaccno = '"+insureAccNo[i]+"'  " +
        	" group by insuaccno  with ur  ";
        	tSSRS = mExeSQL.execSQL(queryMoneySQL);
        	if(tSSRS.GetText(1,1).equals(inGsuaccno)){
        		info1[0] = "团体公共账户";
        		info1[1] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,3)), "0.00")),"0.00");
        		info1[2] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,4)), "0.00")),"0.00");
        		info1[3] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,5)), "0.00")),"0.00");
        		info1[4] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,6)), "0.00")),"0.00");
        		info1[5] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,7)), "0.00")),"0.00");
        		
        		
        		sumLastBala +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,3)), "0.00")); 
        		sumNowLX +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,4)), "0.00")); 
        		sumNowMF +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,5)), "0.00")); 
        		sumNowTM +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,6)), "0.00")); 
        		sumNowCT +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,7)), "0.00")); 
        		
        		
        	}
        	if(tSSRS.GetText(1,1).equals(tGrpInsuNo)){
        		info2[0] = "个人账户单位缴费总和";
        		info2[1] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,3)), "0.00")),"0.00");
        		info2[2] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,4)), "0.00")),"0.00");
        		info2[3] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,5)), "0.00")),"0.00");
        		info2[4] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,6)), "0.00")),"0.00");
        		info2[5] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,7)), "0.00")),"0.00");

        		
        		
        		sumLastBala +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,3)), "0.00")); 
        		sumNowLX +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,4)), "0.00")); 
        		sumNowMF +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,5)), "0.00")); 
        		sumNowTM +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,6)), "0.00")); 
        		sumNowCT +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,7)), "0.00")); 
        	}
        	if(tSSRS.GetText(1,1).equals(tInsuNo)){
        		info3[0] = "个人账户个人缴费总和";
        		info3[1] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,3)), "0.00")),"0.00");
        		info3[2] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,4)), "0.00")),"0.00");
        		info3[3] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,5)), "0.00")),"0.00");
        		info3[4] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,6)), "0.00")),"0.00");
        		info3[5] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,7)), "0.00")),"0.00");
        		
        		
        		sumLastBala +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,3)), "0.00")); 
        		sumNowLX +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,4)), "0.00")); 
        		sumNowMF +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,5)), "0.00")); 
        		sumNowTM +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,6)), "0.00")); 
        		sumNowCT +=Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(1,7)), "0.00")); 
        	}
        	
        }

        
        info4[0] = "合计";
        info4[1] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(sumLastBala, "0.00")),"0.00");
        info4[2] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(sumNowLX, "0.00"),"0.00");
        info4[3] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(sumNowMF, "0.00"),"0.00");
        info4[4] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(sumNowTM, "0.00"),"0.00");
        info4[5] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(sumNowCT, "0.00"),"0.00");
      
        tListTableCTULI.add(info1);
        if(!"370301".equals(riskCode)){
        	tListTableCTULI.add(info2);
        	tListTableCTULI.add(info3);
        }
        tListTableCTULI.add(info4);
    }    
    
    


    /**
     * 特需退费信息
     * @return String[]
     */
    private void getPrintContentESP(LPGrpEdorItemSchema aLPGrpEdorItemSchema,ListTable aListTableCTEP) {
       //获得录入利息
       String inputLXSQL= " select edorvalue From lpedorespecialdata where edorno ='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' "
                        + "  and detailtype ='RATEMONEY'";
       double inputLX = 0.00;
       String testLX = new ExeSQL().getOneValue(inputLXSQL);
       if (null==testLX || "".equals(testLX))
       {
           inputLX = 0.00;
       }else
       {
           inputLX = Double.parseDouble(testLX);
       }
            //团险帐户信息
        String sqlBJ = "select DISTINCT riskcode , grppolno FROM LJSGETENDORSE where EndorsementNo='"
                       +aLPGrpEdorItemSchema.getEdorNo()
                       +"' and riskcode in (select RISKCODE from lmriskapp where (RiskType3 = '7' or RiskType8='6') and (risktype ='H' or risktype ='O') )"
                       ;
        ExeSQL mExeSQL = new ExeSQL();
        SSRS tSSRS = mExeSQL.execSQL(sqlBJ);
        if (tSSRS.getMaxRow() == 0) {
            return ;
        }
        String grpPolNo = tSSRS.GetText(1,2);
        String riskCode = tSSRS.GetText(1,1);
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
//        tLCInsureAccDB.setGrpPolNo(grpPolNo);
//        tLCInsureAccDB.setPolNo(CommonBL.getPubliAccPolNo(grpPolNo));
        String sql = "select * from LCInsureAcc where grppolno ='"+grpPolNo
                     +"' and polno in (select distinct polno from LPPOL WHERE EDORNO ='"
                     +aLPGrpEdorItemSchema.getEdorNo()
                     +"' AND EDORTYPE ='"+aLPGrpEdorItemSchema.getEdorType()
                     +"' AND RISKCODE IN (SELECT RISKCODE FROM LMRISKAPP WHERE (RISKTYPE ='H' or RISKTYPE='O') AND (RISKTYPE3 ='7' or RiskType8='6')))";
        LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.executeQuery(sql);

        double grpFixAccBala = 0.0;
        double grpAccBala = 0.0 ;
        double perAccBala = 0.0;
        //团体委托管理固定帐户
        String grpFixdAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_FIXED,riskCode);
        //团体委托管理理赔帐户
        String grpAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP,riskCode);
        //个人账户
        String personAccno =  CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,riskCode);
        for (int i = 1; i <= tLCInsureAccSet.size(); i++)
        {
            if (tLCInsureAccSet.get(i).getInsuAccNo().equals(grpFixdAccNo))
            {
                //团体固定
                grpFixAccBala += tLCInsureAccSet.get(i).getInsuAccBala();
            }
            if(tLCInsureAccSet.get(i).getInsuAccNo().equals(grpAccNo))
            {
                //团体
                grpAccBala += tLCInsureAccSet.get(i).getInsuAccBala();
            }
            if(tLCInsureAccSet.get(i).getInsuAccNo().equals(personAccno))
            {
                //个人
                perAccBala += tLCInsureAccSet.get(i).getInsuAccBala();
            }
        }
        //帐户退费
        String tfAccBala = "SELECT payplancode,sum(getmoney) FROM LJSGETENDORSE where EndorsementNo='"
                                 + aLPGrpEdorItemSchema.getEdorNo()
                                 + "' and riskcode = '"
                                 + riskCode
                                 + "' group by payplancode";
        tSSRS = mExeSQL.execSQL(tfAccBala);
        double tfFixBala = 0.0 ;
        double tfGrpBala = 0.0 ;
        double tfperBala = 0.0 ;
        for(int i =1 ;i<= tSSRS.getMaxRow();i++)
        {
            if(tSSRS.GetText(i,1).equals(grpFixdAccNo))
                tfFixBala =Double.parseDouble(tSSRS.GetText(i,2));
            else if(tSSRS.GetText(i,1).equals(grpAccNo))
                tfGrpBala = Double.parseDouble(tSSRS.GetText(i,2));
            else
                tfperBala += Double.parseDouble(tSSRS.GetText(i,2));
        }

        //        double grpAccInterest = getAccInterest(); //利息
//        double grpAccMngFeeCT = getGrpAccMngFeeCT(grpAccBala + grpAccInterest,
//                                                  polFeeInfo[3]);
        //add by fuxin 2009-11-6
        String LXsql =" select payplancode,sum(getmoney)  From ljsgetendorse a where getnoticeno ='"+aLPGrpEdorItemSchema.getEdorNo()+"'"
                     +"  and getmoney != 0 "
                     +" and feefinatype ='LX' group by payplancode with ur "
                     ;
       SSRS LXSSRS = new ExeSQL().execSQL(LXsql);

       double grpFixdAccNoLX = 0.0;
       double grpAccNoLX = 0.0;
       double personAccnoLX =0.0;
       for (int i = 1 ; i <= LXSSRS.getMaxRow() ; i++)
       {
           if (grpFixdAccNo.equals(LXSSRS.GetText(i,1)))
           {
               //团体委托管理固定帐户利息
                grpFixdAccNoLX =Double.parseDouble(LXSSRS.GetText(i,2));
           }
           else if (grpAccNo.equals(LXSSRS.GetText(i,1)))
           {
               //团体委托管理理赔帐户利息
                grpAccNoLX =Double.parseDouble(LXSSRS.GetText(i,2));
           }
           else
           {
               //个人账户利息，个人账户现在不计息
                personAccnoLX = Double.parseDouble(LXSSRS.GetText(i,2));
           }
       }

       String MFsql = " select insuaccno,sum(fee) from LPInsureAccFeeTrace where edorno ='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' "
                    + "  and moneytype ='MF' group by insuaccno "
                    ;

       SSRS MFSSRS = new ExeSQL().execSQL(MFsql);

       double grpFixdAccNoMF = 0.0;
       double grpAccNoMF = 0.0;
       double personAccnoMF = 0.0;

       for ( int j = 1 ; j <= MFSSRS.getMaxRow(); j ++)
       {
           if (grpFixdAccNo.equals(MFSSRS.GetText(j,1)))
           {
               grpFixdAccNoMF = Double.parseDouble(MFSSRS.GetText(j,2));
           }
           else if(grpAccNo.equals(MFSSRS.GetText(j,1)))
           {
               grpAccNoMF = Double.parseDouble(MFSSRS.GetText(j,2));
           }else
           {
               personAccnoMF = Double.parseDouble(MFSSRS.GetText(j,2));
           }
       }

        String info1[] = new String[5];
        String info2[] = new String[5];
        String info3[] = new String[5];
        String info4[] = new String[5];

        info1[0] = "团体固定账户";
        info1[1] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(grpFixAccBala, "0.00")),"0.00");
        info1[4] = CommonBL.bigDoubleToCommonString(Math.abs(tfFixBala),"0.00");
        

        info2[0] = "团体医疗账户";
        info2[1] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(grpAccBala, "0.00")),"0.00");
        
        
        if(inputLX == 0.00)
        {
            info2[4] = CommonBL.bigDoubleToCommonString(Math.abs(tfGrpBala),"0.00");
        }else
        {
            info2[4] =  CommonBL.bigDoubleToCommonString(PubFun.setPrecision(Math.abs(tfGrpBala)+inputLX,"#.00"),"0.00");
        }
        info3[0] = "个人账户总和";
        info3[1] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(perAccBala, "0.00")),"0.00");
        info3[4] = CommonBL.bigDoubleToCommonString(Math.abs(tfperBala),"0.00");
        

        info4[0] = "合计";
        info4[1] =  CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(grpFixAccBala+grpAccBala+perAccBala, "0.00")),"0.00");
        
        
        if(inputLX == 0.0)
        {
            info4[4] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(Math.abs(tfFixBala) + Math.abs(tfGrpBala) +
                                     Math.abs(tfperBala), "#.00"),"0.00");
        }else
        {
            info4[4] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(Math.abs(tfFixBala) + Math.abs(tfGrpBala) +
                                     Math.abs(tfperBala)+inputLX, "#.00"),"0.00");
        }

        double rate = getAccMngRateCT(grpPolNo);
        if(rate<0.00001&&rate>-0.00001)
        {
            info1[2] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(grpFixdAccNoLX, "0.00"),"0.00");
            info2[2] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(grpAccNoLX, "0.00"),"0.00");
            info3[2] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(Double.parseDouble(info3[4])-Double.parseDouble(info3[1]),"0.00"),"0.00");
            info4[2] = CommonBL.bigDoubleToCommonString(Double.parseDouble(info1[2])+Double.parseDouble(info2[2])+Double.parseDouble(info3[2]),"0.00");
            

            info1[3] = "0";
            info2[3] = "0";
            info3[3] = "0";
            info4[3] = "0";
        }
        else
        {
            info1[3] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(grpFixdAccNoMF, "0.00")),"0.00");
            info1[2] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(Double.parseDouble(info1[4])+Double.parseDouble( info1[3])-Double.parseDouble(info1[1]),"0.00"),"0.00");

            info2[3] = CommonBL.bigDoubleToCommonString(Math.abs(PubFun.setPrecision(grpAccNoMF, "0.00")),"0.00");
            info2[2] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(Double.parseDouble(info2[4])+Double.parseDouble( info2[3])-Double.parseDouble(info2[1]),"0.00"),"0.00");


            info3[3] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(Math.abs(personAccnoMF),"0.00") ,"0.00");
            info3[2] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(Double.parseDouble(info3[4])+Double.parseDouble( info3[3])-Double.parseDouble(info3[1]),"0.00"),"0.00");

            info4[2] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(Double.parseDouble(info1[2])+Double.parseDouble(info2[2])+Double.parseDouble(info3[2]),"#.00"),"0.00");
            info4[3] = CommonBL.bigDoubleToCommonString(PubFun.setPrecision(Double.parseDouble(info1[3])+Double.parseDouble(info2[3])+Double.parseDouble(info3[3]),"0.00"),"0.00");
            
        
        }
        aListTableCTEP.add(info1);
        aListTableCTEP.add(info2);
        aListTableCTEP.add(info3);
        aListTableCTEP.add(info4);
    }

    /**
     * 计算个人账户余额
     * @param grpPolNo String
     * @return double
     */
    private double getInsuredAccBala(String grpPolNo) {
        double insuredAccBala = CommonBL.getInsuredAccountMoney(grpPolNo);
        return insuredAccBala;
    }

    /**
     * 计算团体解约管理费，（余额+利息）*管理费率
     * @param grpPolNo String
     * @return double
     */
    private double getGrpAccMngFeeCT(double insuaccMoney, String grpPolNo) {
        double rate = getAccMngRateCT(grpPolNo);
        return insuaccMoney * rate;
    }


    private double getGrpAccBala(String grpPolNo,String riskCode) {
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(grpPolNo);
        tLCInsureAccDB.setPolNo(CommonBL.getPubliAccPolNo(grpPolNo));
        LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();

        double grpAccBala = 0;
        String grpFixdAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_FIXED,riskCode);
        String grpAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP,riskCode);
        for (int i = 1; i <= tLCInsureAccSet.size(); i++) {
            if (tLCInsureAccSet.get(i).getInsuAccNo().equals(grpFixdAccNo)
                || tLCInsureAccSet.get(i).getInsuAccNo().equals(grpAccNo)) {
                //团体
                grpAccBala += tLCInsureAccSet.get(i).getInsuAccBala();
            }
        }
        return grpAccBala;
    }

    /**
     *  查询帐户余额利息
     * @return double
     */
    private double getAccInterest() {
        return 0;
    }

    /**
     * 查询账户解约管理费：个人账户和团体帐户相同
     * @param grpPolNo String
     * @return double
     */
    private double getAccMngRateCT(String grpPolNo) {
    	String sql1 = "select RiskCode from LCGrpPol where GrpPolNo ='" + grpPolNo + "' ";
    	String riskcode = new ExeSQL().getOneValue(sql1);
    	String wherepart = null;
    	if("170301".equals(riskcode))
    	{
    		wherepart = "000003";
    	}
    	else
    	{
    		wherepart = BQ.FEECODE_CT;
    	}
        String sql = "  select b.feeName, a.feeValue "
                     + "from LCGrpFee a, LMRiskFee b "
                     + "where a.feeCode = b.feeCode "
                     + "   and a.insuAccNo = b.insuAccNo "
                     + "   and a.payPlanCode = b.payPlanCode "
                     + "   and a.grpPolNo = '" + grpPolNo + "' "
//                     + "   and b.feeCode = '" + wherepart + "' "
                     + "   and b.FeeItemType = '05'"
                     ;
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            return Double.parseDouble(tSSRS.GetText(1, 2));
        }

        return 0;

    }

    //对没有的保全项目类型生成空打迎数据
    private boolean getDetailForBlankType(LPGrpEdorItemSchema
                                          aLPGrpEdorItemSchema) {
        return true;
    }

    private boolean getDetailBC(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        tlistTable = new ListTable();
        tlistTable.setName("BC");

        String insuredHasNoBnf = "";

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(aLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorType(aLPGrpEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();

        //具体到每个被保人
        for (int i = 1; i <= tLPEdorItemSet.size(); i++) {
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i);

            LPBnfDB tLPBnfDB = new LPBnfDB();
            tLPBnfDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPBnfDB.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPBnfDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
            LPBnfSet tLPBnfSet = tLPBnfDB.query();

            if (tLPBnfDB.mErrors.needDealError()) {
                buildError("getDetailBC", "查询变更后的受益人出错");
                return false;
            }

            if (tLPBnfSet.size() == 0) {
                insuredHasNoBnf += getInsuredName(tLPEdorItemSchema)
                        + "(" + tLPEdorItemSchema.getInsuredNo() + ") ";
                getInsuredName(tLPEdorItemSchema);
                continue;
            }

            String[] strArr = null;

            for (int m = 1; m <= tLPBnfSet.size(); m++) {
                LPBnfSchema tLPBnfSchema = tLPBnfSet.get(m).getSchema();
                strArr = new String[9];
                ExeSQL tExeSQL = new ExeSQL();

                strArr[0] = "" + m;
                String sql = "select Name from LCInsured where InsuredNo='" +
                             tLPBnfSchema.getInsuredNo() + "' and ContNo='" +
                             tLPBnfSchema.getContNo() + "'";
                strArr[1] = StrTool.cTrim(tExeSQL.getOneValue(sql));
                String tempstr = ChangeCodeBL.getCodeName("BnfType",
                        tLPBnfSchema.getBnfType());
                strArr[2] = StrTool.cTrim(tempstr);
                strArr[3] = StrTool.cTrim(tLPBnfSchema.getName());
                tempstr = ChangeCodeBL.getCodeName("IDType",
                        tLPBnfSchema.getIDType());
                strArr[4] = StrTool.cTrim(tempstr);
                strArr[5] = StrTool.cTrim(tLPBnfSchema.getIDNo());
                tempstr = ChangeCodeBL.getCodeName("Relation",
                        tLPBnfSchema.
                        getRelationToInsured());
                strArr[6] = StrTool.cTrim(tempstr);
                strArr[7] = ChangeCodeBL.getCodeName("BnfGrade",
                        tLPBnfSchema.getBnfGrade());
                strArr[8] = StrTool.cTrim(String.valueOf(tLPBnfSchema.getBnfLot()));

                tlistTable.add(strArr);
            }
        }

        String edorName = "§受益人变更";
        String contNotice = "保单" + aLPGrpEdorItemSchema.getGrpContNo()
                            + "资料变更如下:\n";
        if (!insuredHasNoBnf.equals("")) {
            xmlexport.addDisplayControl("displayBC2");
            textTag.add("BC_HasNoBnf", edorName + "\n" + contNotice
                        + "以下被保人变更为法定："
                        + insuredHasNoBnf);
        }
        if (tlistTable.size() > 0) {
            xmlexport.addDisplayControl("displayBC");
            if (insuredHasNoBnf.equals("")) {
                textTag.add("BC_EdorName", edorName);
                textTag.add("BC_ContNotice", contNotice);
            } else {
                textTag.add("BC_ContNotice", "其他变更如下：");
            }
        }

        String[] strArrHead = new String[10];
        strArrHead[0] = "序号";
        strArrHead[1] = "被保人";
        strArrHead[2] = "受益人类别";
        strArrHead[3] = "姓名";
        strArrHead[4] = "证件类型";
        strArrHead[5] = "证件号码";
        strArrHead[6] = "与被保人关系";
        strArrHead[7] = "受益顺序";
        strArrHead[8] = "受益份额";
        textTag.add("BC_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        textTag.add("BC_EdorValiDate",
                    CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));
        xmlexport.addListTable(tlistTable, strArrHead);
        return true;
    }

    /**
     * 预收保费
     * @param aLPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailYS(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(grpContNo);
        if (!tLCGrpAppntDB.getInfo())
        {
            mErrors.addOneError("未找到投保人信息！");
            return false;
        }
        AppAcc appAcc = new AppAcc();
        LCAppAccTraceSchema tLCAppAccTraceSchema =
                appAcc.getLCAppAccTrace(tLCGrpAppntDB.getCustomerNo(), edorNo,
                BQ.NOTICETYPE_G);
        textTag.add("YS_Money", String.valueOf(tLCAppAccTraceSchema.getMoney()));
        textTag.add("YS_AccBala",
                String.valueOf(tLCAppAccTraceSchema.getAccBala()));
        textTag.add("YS_EdorValiDate",
                CommonBL.decodeDate(aLPGrpEdorItemSchema.getEdorValiDate()));
        return true;
    }

    private String getInsuredName(LPEdorItemSchema itemSchema) {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setInsuredNo(itemSchema.getInsuredNo());
        tLCInsuredDB.setContNo(itemSchema.getContNo());
        LCInsuredSet set = tLCInsuredDB.query();
        if (set.size() == 0) {
            return "";
        }

        return StrTool.cTrim(set.get(1).getName());
    }


    private String getFeeFinaName(String strFeeFinaType) {
        LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCode(strFeeFinaType);
        LDCode1Set tLDCode1Set = tLDCode1DB.query();
        if (tLDCode1Set != null && tLDCode1Set.size() > 0) {
            return tLDCode1Set.get(1).getCodeAlias();
        } else {
            return strFeeFinaType;
        }
    }
    
    private String getDutyKindCode(String tGetDutyCode){
    	
    	LDCodeDB tLDCodeDB = new LDCodeDB();
    	tLDCodeDB.setCode(tGetDutyCode);
    	tLDCodeDB.setCodeType("getdutykind");
        LDCodeSet tLDCodeSet = tLDCodeDB.query();
        if (tLDCodeSet != null && tLDCodeSet.size() > 0) {
            return tLDCodeSet.get(1).getCodeName();
        } else {
            return null;
        }
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "PrtGrpEndorsementBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    String decodeQuery(String codetype, String code) {

        LDCodeDB tLDCodeDB = new LDCodeDB();
        LDCodeSchema tLDCodeSchema = new LDCodeSchema();
        tLDCodeDB.setCodeType(codetype);
        tLDCodeDB.setCode(code);
        tLDCodeDB.getInfo();
        return tLDCodeDB.getCodeName();

    }

    /**
     * 生成帐户余额清单
     */
    private boolean createAppAccList(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        //判断客户是否有有效保单
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
        if (!tLCGrpAppntDB.getInfo())
        {
            mErrors.addOneError("查询投保人信息出错！");
            return false;
        }
        String sql = "select * from LCGrpCont " +
                "where Appflag = '1' " +
                "and appntno = '" + tLCGrpAppntDB.getCustomerNo() + "' " +
                "and not exists(select * from LPGrpCont " +
                "    where EdorNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' " +
                "    and GrpContNo = LCGrpCont.GrpContNo " +
                "    and EdorType in ('CT', 'XT', 'WT')) ";
        System.out.println("acc sql:" + sql);
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(sql);
        if (tLCGrpContSet.size() == 0)
        {
            String customerNo = tLCGrpAppntDB.getCustomerNo();
            String customerName = tLCGrpAppntDB.getName();
            textTag.add("CustomerNo", customerNo);
            textTag.add("CustomerName", customerName);
            tlistTable = new ListTable();
            LCAppAccTraceDB tLCAppAccTraceDB = new LCAppAccTraceDB();
            tLCAppAccTraceDB.setCustomerNo(customerNo);
            LCAppAccTraceSet tLCAppAccTraceSet = tLCAppAccTraceDB.query();
            if (tLCAppAccTraceSet.size() > 0)
            {
                xmlexport.addDisplayControl("displayAcc");
            }
            for (int i = 1; i <= tLCAppAccTraceSet.size(); i++)
            {
                LCAppAccTraceSchema tLCAppAccTraceSchema = tLCAppAccTraceSet.
                        get(i);
                String[] info = new String[6];
                info[0] = String.valueOf(i);
                info[1] = CommonBL.decodeDate(tLCAppAccTraceSchema.getConfirmDate());
                info[2] = CommonBL.bigDoubleToCommonString(tLCAppAccTraceSchema.getMoney(),"0.00");
                info[3] = StrTool.cTrim(ChangeCodeBL.getCodeName(
                        "appaccdestsource",
                        tLCAppAccTraceSchema.getDestSource()));
                info[4] = StrTool.cTrim(tLCAppAccTraceSchema.getOtherNo());
                info[5] = CommonBL.bigDoubleToCommonString(tLCAppAccTraceSchema.getAccBala(),"0.00");
                tlistTable.add(info);
            }
            tlistTable.setName("APPACC");
            xmlexport.addListTable(tlistTable, new String[6]);
        }
        return true;
    }

    private boolean getDetailTF(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema = CommonBL.getLCGrpCont(aLPGrpEdorItemSchema.getGrpContNo());
        tlistTable = new ListTable();
        tlistTable.setName("TF");
        textTag.add("grpName",tLCGrpContSchema.getGrpName());
        textTag.add("EdorValiDate",aLPGrpEdorItemSchema.getEdorValiDate()); //保全生效日期。
        textTag.add("EdorAppDate",aLPGrpEdorItemSchema.getEdorAppDate()); //保全受理日期。
        textTag.add("grpContNo",tLCGrpContSchema.getGrpContNo());
        xmlexport.addListTable(tlistTable);
        return true ;
    }
    private boolean getDetailHL(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {


    	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema = CommonBL.getLCGrpCont(aLPGrpEdorItemSchema.getGrpContNo());
        //归属规则变更之前

        return true;
    
    
    }
    
    //增加一个方法用来判断是否是万能险还是之前的险种，根据此方法区分 add by cjg
    public boolean jugdeUliByGrpContNo()
    {
        StringBuffer tSBql2 = new StringBuffer(256);
        tSBql2.append("select 1 from lcgrppol where GrpContNo = '");
        tSBql2.append(mGrpContNo);
        tSBql2.append("' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')");
        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
        if (tSSRS5.getMaxRow() < 1)
        {
            return false;
        }
        return true;
    }
    // add by cjg
    public String getPositionName(String Posotion){
		String sql= "select gradename from lcgrpposition where grpcontno='"+mGrpContNo+"' and gradecode='"+Posotion+"'";
		  ExeSQL tExeSQL = new ExeSQL();
          String PositionName = tExeSQL.getOneValue(sql);
	    return PositionName;
   }
    //add by cjg
    public String getCAscriptionrule(LPGrpEdorItemSchema aLPGrpEdorItemSchema){
    	StringBuffer tAscriptionRuleInfo = new StringBuffer();
    	 NumberFormat nt = NumberFormat.getPercentInstance();
         nt.setMaximumFractionDigits(6);
    	 String str = "select distinct Ascriptionrulecode,Ascriptionrulename from lcAscriptionrulefactory a where a.GrpContNo='"
             + aLPGrpEdorItemSchema.getGrpContNo() + "'";
     ExeSQL tExeSQLScri = new ExeSQL();
     SSRS tSSRSScri = tExeSQLScri.execSQL(str.toString());
     if (tSSRSScri != null && tSSRSScri.getMaxRow() > 0)
     {

         for (int i = 1; i <= tSSRSScri.getMaxRow(); i++)
         {
             String str1 = "select  Ascriptionrulecode,Ascriptionrulename,Calremark,inerserialno from lcAscriptionrulefactory a where a.GrpContNo='"
                     + aLPGrpEdorItemSchema.getGrpContNo()
                     + "' and Ascriptionrulecode='"
                     + tSSRSScri.GetText(i, 1)
                     + "' order by InerSerialNo";

             ExeSQL tExeSQLScri1 = new ExeSQL();
             SSRS tSSRSScri1 = tExeSQLScri1.execSQL(str1.toString());
             
             System.out.println("tSSRSScri1.getMaxRow()" + tSSRSScri1.getMaxRow());
             // 循环获取计划信息
             for (int j = 1; j <= tSSRSScri1.getMaxRow(); j++)
             {
                 String str2 = "select  Paramname,param from LCAscriptionRuleParams a where a.GrpContNo='"
                         + aLPGrpEdorItemSchema.getGrpContNo() + "' and Ascriptionrulecode='"
                         + tSSRSScri.GetText(i, 1) + "'and inerserialno ='" + tSSRSScri1.GetText(j, 4) + "' order by paramname";
                 ExeSQL tExeSQLScri2 = new ExeSQL();
                 SSRS tSSRSScri2 = tExeSQLScri2.execSQL(str2.toString());
                 String SourceString = tSSRSScri1.GetText(j, 3);
                 String TargetString = "";
                 for (int m = 1; m <= tSSRSScri2.getMaxRow(); m++)
                 {
                     System.out.println(tSSRSScri2.GetText(m, 1) + "代替了" + tSSRSScri2.GetText(m, 2));
                     if(m==tSSRSScri2.getMaxRow()){   	
                    	    TargetString = SourceString.replaceAll(tSSRSScri2.GetText(m, 1), nt.format(Double.parseDouble(tSSRSScri2.GetText(m, 2))));
                     }else{
                    	    TargetString = SourceString.replaceAll(tSSRSScri2.GetText(m, 1), tSSRSScri2.GetText(m, 2));
                     }
                 
                     System.out.println(TargetString);
                     SourceString = TargetString;

                 }
                 tAscriptionRuleInfo.append(SourceString);
                 tAscriptionRuleInfo.append(";");
             }  
         }
     }
     return tAscriptionRuleInfo.toString();
   }
//  add by cjg
    public String getPAscriptionrule(LPGrpEdorItemSchema aLPGrpEdorItemSchema){
    	 NumberFormat nt = NumberFormat.getPercentInstance();
         nt.setMaximumFractionDigits(6);
    	StringBuffer tAscriptionRuleInfo = new StringBuffer();
    	 String str = "select distinct Ascriptionrulecode,Ascriptionrulename from lPAscriptionrulefactory a where a.GrpContNo='"
             + aLPGrpEdorItemSchema.getGrpContNo() + "'";
     ExeSQL tExeSQLScri = new ExeSQL();
     SSRS tSSRSScri = tExeSQLScri.execSQL(str.toString());
     if (tSSRSScri != null && tSSRSScri.getMaxRow() > 0)
     {

         for (int i = 1; i <= tSSRSScri.getMaxRow(); i++)
         {
             String str1 = "select  Ascriptionrulecode,Ascriptionrulename,Calremark,inerserialno from lPAscriptionrulefactory a where a.GrpContNo='"
                     + aLPGrpEdorItemSchema.getGrpContNo()
                     + "' and Ascriptionrulecode='"
                     + tSSRSScri.GetText(i, 1)
                     + "' order by InerSerialNo";

             ExeSQL tExeSQLScri1 = new ExeSQL();
             SSRS tSSRSScri1 = tExeSQLScri1.execSQL(str1.toString());
             
             System.out.println("tSSRSScri1.getMaxRow()" + tSSRSScri1.getMaxRow());
             // 循环获取计划信息
             for (int j = 1; j <= tSSRSScri1.getMaxRow(); j++)
             {
                 String str2 = "select  Paramname,param from LPAscriptionRuleParams a where a.GrpContNo='"
                         + aLPGrpEdorItemSchema.getGrpContNo() + "' and Ascriptionrulecode='"
                         + tSSRSScri.GetText(i, 1) + "'and inerserialno ='" + tSSRSScri1.GetText(j, 4) + "'order by paramname";
                 ExeSQL tExeSQLScri2 = new ExeSQL();
                 SSRS tSSRSScri2 = tExeSQLScri2.execSQL(str2.toString());
                 String SourceString = tSSRSScri1.GetText(j, 3);
                 String TargetString = "";
                 for (int m = 1; m <= tSSRSScri2.getMaxRow(); m++)
                 {  
                	   if(m==tSSRSScri2.getMaxRow()){   	
                   	    TargetString = SourceString.replaceAll(tSSRSScri2.GetText(m, 1), nt.format(Double.parseDouble(tSSRSScri2.GetText(m, 2))));
                    }else{
                        TargetString = SourceString.replaceAll(tSSRSScri2.GetText(m, 1), tSSRSScri2.GetText(m, 2));	
                    }
                     System.out.println(tSSRSScri2.GetText(m, 1) + "代替了" + tSSRSScri2.GetText(m, 2));
                     System.out.println(TargetString);
                     SourceString = TargetString;

                 }
                 tAscriptionRuleInfo.append(SourceString);
                 tAscriptionRuleInfo.append(";");


             }  
         }
     }
     return tAscriptionRuleInfo.toString();
   }
    
    //by gzh  获取团体万能险种编码
    private String getUliRisk(String grpContNo) {
         LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
         tLCGrpPolDB.setGrpContNo(grpContNo);
         LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
         for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
             LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
             tLMRiskAppDB.setRiskCode(tLCGrpPolSet.get(i).getRiskCode());
             if (!tLMRiskAppDB.getInfo()) {
                 return null;
             }
             String riskType4 = tLMRiskAppDB.getRiskType4();//团体
             String riskProp= tLMRiskAppDB.getRiskProp();//万能
             
             if(riskType4 != null && riskType4.equals("4") && riskProp !=null && "G".equals(riskProp))
             {
                 return tLCGrpPolSet.get(i).getRiskCode();
             }             
         }
         return null;
     }
    //这个生效日虽然正确，但是取的地方 是不对的。日后要更改过来。
    private String getEdorValidate(LPGrpEdorItemSchema aLPGrpEdorItemSchema){
        String str1 = "select edorvalidate from LPEdorItem where edorno='"+aLPGrpEdorItemSchema.getEdorNo()+"'";
        ExeSQL tExeSQL = new ExeSQL();
        String dateString = tExeSQL.getOneValue(str1);
        return CommonBL.decodeDate(dateString);
    }
    public static void main(String[] args) {
        PrtGrpEndorsementBL tPrtGrpEndorsementBL = new PrtGrpEndorsementBL(
                "20070309000001");
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "endor0";
        tVData.addElement(tGlobalInput);
        tPrtGrpEndorsementBL.submitData(tVData, "");
        VData vdata = tPrtGrpEndorsementBL.getResult();
        PubSubmit ps = new PubSubmit();
        if (ps.submitData(vdata, "")) {
            System.out.println("succeed in pubsubmit");
        }

    }
    
    //团险万能身故处理20110603【OoO】yangtianzheng
    private boolean getDetailSG(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
    	//开始-----------------------
        ExeSQL tExeSQL = new ExeSQL();
        //1.得到保单号，因为每个“团险万能身故处理”下只有一个LPEDORITEM表，因此，可以用edorno直接关联。
        String sql = "select contno from lpedoritem where edorno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"'";
        String aContNo = tExeSQL.getOneValue(sql);     
        if (aContNo == null || aContNo.equals("")) 
        {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getInsuredCM";
            tError.errorMessage = "申请号" + aLPGrpEdorItemSchema.getEdorAcceptNo() +
                                  "下无相应的保全项目信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //2.得到应退费用getmoney。由于，表中存储的getmoney是负数，
        //因此需要在取值时添加负号，已达到界面上显示正数的效果。
        String sql_getmoney = "select -getmoney from lpedoritem where edorno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"'";
        String aGetMoney = tExeSQL.getOneValue(sql_getmoney);   
        //3.得到被保人号码。这个号码需要在LCPOL表中取得。
        String aInsuredNo=new ExeSQL().getOneValue("select insuredno from lcpol where contno='"+aContNo+"'");
        //4分别得到已故人的姓名和身份证号。
        String sql_Name = "select name from ldperson where customerno='"+aInsuredNo+"'";
        String sql_IdNo = "select idno from ldperson where customerno='"+aInsuredNo+"'";
        String aName = tExeSQL.getOneValue(sql_Name);
        String aIdNo = tExeSQL.getOneValue(sql_IdNo);
        //5.得到“个人账户个人缴费金额“。
        String aPersonPayMoney=new ExeSQL().getOneValue("select edorvalue from lpedorespecialdata where detailtype='PERSONPAYMONEY' and edorno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' and edortype='SG' ");
        //6.得到”个人账户单位缴费金额“。
        String aGrpPayMoney=new ExeSQL().getOneValue("select edorvalue from lpedorespecialdata where detailtype='GRPPAYMONEY'and edorno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' and edortype='SG' ");
        //7.得到”保单管理费合计“。
        String aManageFee=new ExeSQL().getOneValue("select edorvalue from lpedorespecialdata where detailtype='MANAGEFEE'and edorno='"+aLPGrpEdorItemSchema.getEdorAcceptNo()+"' and edortype='SG' ");
        //8.最后生成批单信息。
        textTag.add("SG_GrpContNo", aLPGrpEdorItemSchema.getGrpContNo());
        textTag.add("SG_INSuredName", aName);
        textTag.add("SG_IdNo", aIdNo);
        textTag.add("SG_PersonPayMoney", aPersonPayMoney);
        textTag.add("SG_GrpPayMoney", aGrpPayMoney);
        textTag.add("SG_ManageFee", aManageFee);
        textTag.add("SG_GetMoney", aGetMoney);
        //9.成功的完成了批单的生成，返回一个TRUE。
        return true; 
        //结束------------------------------------
    }
    
 // 团险终止缴费
 	// add by lh
 	private boolean getDetailZF(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
 		String edorNo = aLPGrpEdorItemSchema.getEdorNo();
 		String edorType = aLPGrpEdorItemSchema.getEdorType();
 		String grpcontNo = aLPGrpEdorItemSchema.getGrpContNo();
 		String date = aLPGrpEdorItemSchema.getEdorAppDate();
//                 tlistTable = new ListTable();
//                 tlistTable.setName("ZF");
                 //String[] strArray = new String[4];
 		// 险种号码
 		// 查询出表格要显示的内容
// 		String sql2 = "select riskseqno,lp.RiskCode ,(select lm.RiskName from lmrisk lm where lm.RiskCode = lp.RiskCode ) ,lp.PaytoDate "
// 				+ "from LPPol lp "
// 				+ "where lp.edorNo ='"
// 				+ edorNo
// 				+ "'"
// 				+ "and lp.edortype ='"
// 				+ edorType
// 				+ "'"
// 				+ "and lp.contNo ='"
// 				+ contNo + "'";
// 		SSRS result = new ExeSQL().execSQL(sql2);
// 		if (result == null || result.getMaxRow() == 0) {
// 			mErrors.addOneError("为找到查询信息！");
// 			return false;
// 		}
// 		for (int i = 1; i <= result.getMaxRow(); i++) {
//                     strArray = new String[4];
// 			strArray[0] = result.GetText(i, 1);
// 			strArray[1] = result.GetText(i, 2);
// 			strArray[2] = result.GetText(i, 3);
// 			strArray[3] = result.GetText(i, 4);
//                         tlistTable.add(strArray);
// 		}
 		String sql = "select (case payintv when -1 then (select max(paytodate) from lcgrppayplandetail " +
 					"	where prtno=a.prtno and state='1') else paytodate end),grpcontno from LCGrpPol a " +
 					"	where GrpContNo='"+grpcontNo+"'";
 		String tPaytodate = new ExeSQL().getOneValue(sql);
 		if (tPaytodate == null) {
 			mErrors.addOneError("未找到缴费截止日信息！");
 			return false;
 		}
 		textTag.add("ZFnumber", grpcontNo);
 		textTag.add("ZFdate", date);
 		textTag.add("Paytodate", tPaytodate);
                 //xmlexport.addListTable(tlistTable,strArray);

 		return true;
 	}
 	
    /**
     * 管理式医疗减少保险金额
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @return boolean
     */
    private boolean getDetailZE(LPGrpEdorItemSchema aLPGrpEdorItemSchema) {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        String edorValiDate = aLPGrpEdorItemSchema.getEdorValiDate();
        double getMoney = aLPGrpEdorItemSchema.getGetMoney();
        textTag.add("ZE_GrpContNo", grpContNo);
//        textTag.add("ZE_GetMoney", getMoney);
        textTag.add("ZE_EdorValiDate", CommonBL.decodeDate(edorValiDate));

        String grpPolNo = null;
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        tLCGrpPolDB.setAppFlag(BQ.APPFLAG_SIGN);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            grpPolNo = tLCGrpPolSet.get(i).getGrpPolNo();
        }
        if (grpPolNo == null) {
            return false;
        }

        String content = "";
        EdorItemSpecialData specialData =
                new EdorItemSpecialData(edorNo, edorType);
        specialData.query();
        specialData.setGrpPolNo(grpPolNo);

        String insuredGetMoney = specialData.getEdorValue("InsuredGetMoney");
        textTag.add("ZE_GetMoney", insuredGetMoney);
        if ((insuredGetMoney != null) && (!insuredGetMoney.equals(""))) {

            content += "减少金额" + specialData.getEdorValue("InsuredGetMoney") + "元，" +
            "应领取金额：" + specialData.getEdorValue("InsuredGetMoney") + "元，" +
            "情况详见清单。\n";
        }
        textTag.add("ZE_Content", content);
        return true;
    }
}
