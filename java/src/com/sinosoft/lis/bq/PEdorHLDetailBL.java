package com.sinosoft.lis.bq;

import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 护理老年金领取</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author CJG
 * @author rewrite by cjg 2011
 * @version 1.0
 */

public class PEdorHLDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();


    private GlobalInput mGlobalInput = null;

    private String mTypeFlag = null;
    
    private String mPayMode = null;
    
    private LCGetSchema mLCGetSchema = null;
    
    private String mPerPayMode = null;
    
    private String mMoney = null;
    
    private ExeSQL exesql = new ExeSQL();

    private String mEdorNo = null;

    private String mEdorType = null;
    
    private String minsuaccno =null; //保存个人账户个人缴费的账户号
    
    private String mDutyCode = null; //保存个人账户个人缴费的责任号
    
    private String mGetDutyCode = null; //保存老年金护理的给付号码

    private String mContNo = null;

    private String mInsuredNo = null;
    
    private String mEndDate = null;
    
    private  LPEdorItemSchema tLPEdorItemSchema = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    private Reflections ref = new Reflections();
    
    TransferData mTransferData = new TransferData();
    
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

    /**
     * 提交数据
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到传入数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mTypeFlag = (String) cInputData.get(0);
            mPayMode = (String) cInputData.get(1);  //存取领取方式
            System.out.println("领取方式为"+mPayMode);
            
            mPerPayMode=(String) cInputData.get(2);// 存取分次领取方式
            System.out.println("分次领取方式为"+mPerPayMode);
            mMoney = (String) cInputData.get(3); // 存取领取金额
            System.out.println("领取金额 为"+mMoney);
           mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
                  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
             tLPEdorItemSchema = (LPEdorItemSchema)
                    cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
            this.mEdorNo = tLPEdorItemSchema.getEdorNo();
            this.mEdorType = tLPEdorItemSchema.getEdorType();
            this.mContNo = tLPEdorItemSchema.getContNo();
            this.mInsuredNo = tLPEdorItemSchema.getInsuredNo(); //

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {   
    	  if ((mEdorNo == null) || (mEdorNo.equals("")))
          {
              mErrors.addOneError("保单受理号为空！");
              return false;
          }
    	  if ((mEdorType == null) || (mEdorType.equals("")))
          {
              mErrors.addOneError("保全类型为空！");
              return false;
          }
        if ((mInsuredNo == null) || (mInsuredNo.equals("")))
        {
            mErrors.addOneError("客户号为空！");
            return false;
        }
        
        if ((mContNo == null) || (mContNo.equals("")))
        {
            mErrors.addOneError("个人合同号为空！");
            return false;
        }
        if((mPayMode==null)|| (mPayMode.equals(""))){
        	mErrors.addOneError("领取方式不能为空");
        	return false;
        }
        
        if(mPayMode=="D"||mPayMode.equals("D")){
           if(mPerPayMode==null || mPerPayMode.equals("")){
        		mErrors.addOneError("请选择按年或按月领取方式!");
        		return false;
        	}
           if(mMoney==null || mMoney.equals("")){
        		mErrors.addOneError("你已选择按年或按月领取方式，领取金额不能为空!");
        		return false;
        	}
         }
        
        return true;
    }

    /**
     * 取出生效日期
     * @return boolean
     */
    private boolean getValidate()
    {   
    	String tsql = "select a.EdorValiDate from lpedoritem a where a.contno = '"
			+ mContNo
			+ "' and a. InsuredNo='"+mInsuredNo+"' and a.edorno='"+mEdorNo+"' and a.edortype ='"+mEdorType+"'";
    	SSRS tSSRS = new SSRS();
    	tSSRS = new ExeSQL().execSQL(tsql.toString());
    	
    	if(tSSRS.MaxRow==0)
    	{
    		System.out.println("被保人"+mInsuredNo+"没有生效日期");
    		CError.buildErr(this,"被保人"+mInsuredNo+"没有生效日期");
             return false;
    	}
    	else
    	{  	
    		mEndDate = tSSRS.GetText(1, 1);
    	}

        return true;
    }
    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {

    	  if (!getValidate())
          {
              return false;
          }
    	
         if (!SaveHLFS())
         {
             return false;
         }
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
    }
    
    /**
     * 保存领取设置的表
     */
    private boolean SaveHLFS()
    {

					// 处理年金险
		String tsql = "select a.* from LCCont a where a.contno = '"
				+ mContNo
				+ "' and a.InsuredNo='"+mInsuredNo+"'";
		LCContDB tLCContDB = new LCContDB();
		LCContSet tLCContSet = tLCContDB.executeQuery(tsql);
		for (int i = 1; i <= tLCContSet.size(); i++) {
			LCContSchema tLCContSchema = tLCContSet.get(i);
			LPContSchema tLPContSchema = new LPContSchema();
			ref.transFields(tLPContSchema, tLCContSchema);
			tLPContSchema
					.setEdorNo(mEdorNo);
			tLPContSchema.setEdorType(mEdorType);
			tLPContSchema.setModifyDate(PubFun.getCurrentDate());
			tLPContSchema.setModifyTime(PubFun.getCurrentTime());
			
			mMap.put("delete from lpcont where contno = '" + mContNo + "' and insuredno='"+mInsuredNo+"'", "DELETE"); 
			mMap.put(tLPContSchema, "INSERT");	
			tsql = "select a.* from lcpol a where a.contno = '"
			+ mContNo
			+ "'and a.InsuredNO='"+mInsuredNo+"'";
			LCPolDB tLCPolDB = new LCPolDB();
			LCPolSet tLCPolSet = tLCPolDB.executeQuery(tsql);
			for (int j = 1; j <= tLCPolSet.size(); j++) {
				// 保全的生效日期必须在团单下需领取分单最后一次帐户变动轨迹之后。
				//处理之前判断该被保险人的上一月的管理费是否已收取
				String ISBeforeLastAccBala =CommonBL.ISBeforeLastAccBala(tLCPolSet.get(j).getPolNo(), mEndDate);
				System.out.println(ISBeforeLastAccBala.substring(0, 1));
		    	if(ISBeforeLastAccBala.substring(0, 1).equals("1"))
		    	{
		    		  mErrors.addOneError("保全项目生效日"+mEndDate+"不应早于该被保人"+tLCPolSet.get(j).getInsuredName()+"的最后一次账户资金变化日"+ISBeforeLastAccBala.substring(1,ISBeforeLastAccBala.length()));
		    		  return false;
		    	}
		    	
		    	if(!CommonBL.ISLastMonthAccBala(tLCPolSet.get(j).getPolNo(), mEndDate))
		    	{
		    		 mErrors.addOneError("被保人" +tLCPolSet.get(j).getInsuredName()  + "保全生效日 "
		    				 +mEndDate
								+ "的上个月还未月结，请先进行月结再申请该保全");
					return false;
		    	}
				LCPolSchema tLCPolSchema = tLCPolSet.get(j);
				LPPolSchema tLPPolSchema = new LPPolSchema();
				ref.transFields(tLPPolSchema, tLCPolSchema);
				tLPPolSchema
						.setEdorNo(mEdorNo);
				tLPPolSchema.setEdorType(mEdorType);
				tLPPolSchema.setModifyDate(PubFun.getCurrentDate());
				tLPPolSchema.setModifyTime(PubFun.getCurrentTime());
				mMap.put(tLPPolSchema, "DELETE&INSERT");
				// 责任
				LCDutyDB tLCDutyDB = new LCDutyDB();
				tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
				LCDutySet tLCDutySet = tLCDutyDB.query();
				for (int k = 1; k <= tLCDutySet.size(); k++) {
					LPDutySchema tLPDutySchema = new LPDutySchema();
					ref.transFields(tLPDutySchema, tLCDutySet.get(k));
					tLPDutySchema.setEdorNo(mEdorNo);
					tLPDutySchema.setEdorType(mEdorType);
					tLPDutySchema
							.setModifyDate(PubFun.getCurrentDate());
					tLPDutySchema
							.setModifyTime(PubFun.getCurrentTime());
					mMap.put(tLPDutySchema, "DELETE&INSERT");
				}

				// prem
				LCPremDB tLCPremDB = new LCPremDB();
				tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
				LCPremSet tLCPremSet = tLCPremDB.query();
				for (int k = 1; k <= tLCPremSet.size(); k++) {
					LPPremSchema tLPPremSchema = new LPPremSchema();
					ref.transFields(tLPPremSchema, tLCPremSet.get(k));
					tLPPremSchema.setEdorNo(mEdorNo);
					tLPPremSchema.setEdorType(mEdorType);
					tLPPremSchema
							.setModifyDate(PubFun.getCurrentDate());
					tLPPremSchema
							.setModifyTime(PubFun.getCurrentTime());
					mMap.put(tLPPremSchema, "DELETE&INSERT");
				}

			    minsuaccno=new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLPPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')) ");
		        //取出责任编码
		        mDutyCode =new ExeSQL().getOneValue("select dutycode from lmdutypayrela l  where exists (select 1 from lmdutypay m where l.payplancode =m.payplancode and m.AccPayClass='5') and exists (select 1 from lmriskduty m where dutycode =l.dutycode and exists (select 1 from lmriskapp where risktype4='4' and Riskprop='G' and riskcode=m.riskcode))");
		        //取出个人账户老年金领取的给付号码
		        mGetDutyCode= new ExeSQL().getOneValue("select getdutycode from lmdutyget e  where e.type='0' and exists (select 1 from lmdutygetrela t where t.getdutycode=e.getdutycode and  exists (select 1 from lmdutypayrela l   where  l.dutycode =t.dutycode and l.dutycode='"+mDutyCode+"' and exists (select 1 from lmdutypay m where l.payplancode =m.payplancode and m.AccPayClass='5')))");
		    
				LCGetDB tLCGetDB = new LCGetDB();
				tLCGetDB.setPolNo(tLCPolSchema.getPolNo());
				tLCGetDB.setGetDutyCode(mGetDutyCode);//年金领取的金额保存在671201上，所以只对lcget中的这条记录，更新生成P表就可以
				LCGetSet tLCGetSet = tLCGetDB.query();
				for (int k = 1; k <= tLCGetSet.size(); k++) {
					LPGetSchema tLPGetSchema = new LPGetSchema();
					ref.transFields(tLPGetSchema, tLCGetSet.get(k));
					tLPGetSchema.setEdorNo(mEdorNo);
					tLPGetSchema.setEdorType(mEdorType);
					tLPGetSchema.setModifyDate(PubFun.getCurrentDate());
					tLPGetSchema.setModifyTime(PubFun.getCurrentTime());
				    if("A".equals(mPayMode)){
				    	tLPGetSchema.setGetDutyKind("A");
				    }else if("D".equals(mPayMode)){
				    	tLPGetSchema.setGetDutyKind("D");
				    }
					if(minsuaccno.equals(tLPGetSchema.getDutyCode())&& "D".equals(mPayMode)){
						tLPGetSchema.setUrgeGetFlag("Y");
						tLPGetSchema.setActuGet(mMoney);
						tLPGetSchema.setStandMoney(mMoney);
						//保存按年或按月领取
						if(mPerPayMode.equals("0")){
						tLPGetSchema.setGetIntv(12);
						}
						if(mPerPayMode.equals("1")){
					    tLPGetSchema.setGetIntv(1);
						}
					}
					
					mMap.put(tLPGetSchema, "DELETE&INSERT");
				}

			}
		}

		mResult.clear();
		mResult.add(mMap);
		return true;
	}

    

  
	 // 检验在保全生效日上一个月的管理费是否已经收取

	private boolean CheckPolManagementFee(String polNo, String insuredNo,
			String ContNo) {
		String sqlli4 = "select poltype from lccont where contno = '" + ContNo
				+ "'";

		SSRS InsuRSSRSli4 = new SSRS();
		InsuRSSRSli4 = exesql.execSQL(sqlli4);

		if (InsuRSSRSli4 != null && InsuRSSRSli4.getMaxRow() > 0
				&& "0".equals(InsuRSSRSli4.GetText(1, 1))) {
			//加上和保单生效日的比较，从保单生效日开始收取管理费
			String sqlli2 = " select max(a.paydate) from lcinsureacctrace a, lcpol b"
			              + " where a.polno = '"+polNo+"' and moneytype = 'GL' and a.paydate >= b.cvalidate "
			              + " and a.polno = b.polno ";
			SSRS InsuRSSRSli2 = new SSRS();
			InsuRSSRSli2 = exesql.execSQL(sqlli2);
			if("".equals(InsuRSSRSli2.GetText(1, 1)) || null == InsuRSSRSli2.GetText(1, 1)){
				String sqlliPol = " select cvalidate from lcpol "
		              + " where polno = '"+polNo+"' ";
				InsuRSSRSli2 = new SSRS();
				InsuRSSRSli2 = exesql.execSQL(sqlliPol);
			}
			
			if (InsuRSSRSli2 != null && InsuRSSRSli2.getMaxRow() > 0
					&& !("".equals(InsuRSSRSli2.GetText(1, 1)))
					&& mEndDate != null && !("".equals(mEndDate))) {
				
				String sqlli5 = "SELECT date('" + mEndDate
				+ "')-date('"
				+ InsuRSSRSli2.GetText(1, 1)
				+ "') FROM dual";
		        SSRS InsuRSSRSli5 = new SSRS();
		    	InsuRSSRSli5 = exesql.execSQL(sqlli5);
				if (InsuRSSRSli5 != null
						&& InsuRSSRSli5.getMaxRow() > 0
						&& (Integer.parseInt(InsuRSSRSli5.GetText(1, 1)) - 0) < 0) {
					
					   mErrors.addOneError("被保人" + insuredNo + " "
								+ FormateDate(InsuRSSRSli2.GetText(1, 1))
								+ "的最后轨迹日期不能大于保全的生效日期");
					return false;
				}
				String sqlli3 = "SELECT date('" + mEndDate
						+ "')-(date('"
						+ InsuRSSRSli2.GetText(1, 1)
						+ "')+1 month) FROM dual";
				SSRS InsuRSSRSli3 = new SSRS();
				InsuRSSRSli3 = exesql.execSQL(sqlli3);
				if (InsuRSSRSli3 != null
						&& InsuRSSRSli3.getMaxRow() > 0
						&& (Integer.parseInt(InsuRSSRSli3.GetText(1, 1)) - 0) > 0) {					
					 mErrors.addOneError("被保人" + insuredNo + " "
								+ FormateDate(InsuRSSRSli2.GetText(1, 1))
								+ "的保单管理费还没有收取，不能做这个时间之后的保全");
					return false;
				}
			}
		}
		return true;

	}

	/**
	 * 格式化日期 输入2011-03-08 返回2011年03月
	 * 
	 * @param date
	 * @return
	 */
	private String FormateDate(String date) {
		String newdate = FormatDateByDate(date);
		return newdate.substring(0, newdate.indexOf("月") + 1);
	}
	
	/**
	 * 格式化日期 输入2011-03-08 返回2011年03月
	 * 
	 * @param date
	 * @return
	 */
	private String FormatDateByDate(String cDate) {
		if (cDate == null || "".equals(cDate)) {
			return "";
		}

		cDate = cDate.replaceFirst("-", "年");
		cDate = cDate.replaceFirst("-", "月");

		StringBuffer tSBql = new StringBuffer();
		tSBql.append(cDate);
		tSBql.append("日");

		return tSBql.toString();
	}
    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
        String sql = "update  LPEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "and InsuredNo = '" + mInsuredNo + "'";
        mMap.put(sql, "UPDATE");
    }
  


    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
