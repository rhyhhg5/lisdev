package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全交费方式变更</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author  Lanjun
 * @version 1.0
 */

public class PEdorDDDetailUI
{
    private PEdorDDDetailBL mPEdorDDDetailBL = null;

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        PEdorDDDetailBL mPEdorDDDetailBL = new PEdorDDDetailBL();
        System.out.println("来到了UI层");
        if (!mPEdorDDDetailBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 得到错误
     * @return String
     */
    public String getError()
    {
        return mPEdorDDDetailBL.mErrors.getFirstError();
    }

    /**
     * 主函数，测试用
     */
    public static void main(String[] args)
    {
    }
}
