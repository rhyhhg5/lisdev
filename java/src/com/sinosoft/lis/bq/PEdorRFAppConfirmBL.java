package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单质押贷款清偿BL</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xp 2011/11/14
 * @version 1.0
 */

import java.util.Date;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorRFAppConfirmBL implements EdorAppConfirm {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
//	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData;

	private MMap mMap = new MMap();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 保全号 */
	private String mEdorNo = null;

	/** 保全类型 */
	private String mEdorType = null;

	/** 保全项目特殊数据 */
	private LPLoanSchema mLPLoanSchema = new LPLoanSchema();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/** 业务数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

	private TransferData mTransferData = new TransferData();

	/** 计算要素 */
	private BqCalBase mBqCalBase = new BqCalBase();

	private Reflections mReflections = new Reflections();

	private LPReturnLoanSchema mLPReturnLoanSchema = new LPReturnLoanSchema();

	public PEdorRFAppConfirmBL() {
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括""和""
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		// 数据校验操作
		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		return true;
	}

	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */
	public VData getResult() {
		mResult.clear();
		mResult.add(mMap);
		return mResult;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			// 保全项目校验
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
			mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName("LPEdorItemSchema", 0);
			return true;
		} catch (Exception e) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorRFDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
	}

	/**
	 * 校验传入的数据的合法性 输出：如果发生错误则返回false,否则返回true
	 */
	private boolean checkData() {
		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();

		tLPEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
		tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());

		LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
		if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorRFDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "查询保全项目失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mLPEdorItemSchema = tLPEdorItemSet.get(1);

		tLPEdorItemDB.setSchema(mLPEdorItemSchema);
		if (!tLPEdorItemDB.getInfo()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorRFDetailBL";
			tError.functionName = "checkData";
			tError.errorMessage = "无保全申请数据！";
			this.mErrors.addOneError(tError);
			return false;
		}

		mLPEdorItemSchema.setSchema(tLPEdorItemDB.getSchema());
		mEdorNo = mLPEdorItemSchema.getEdorNo();
		mEdorType = mLPEdorItemSchema.getEdorType();

		// 查询即往借款信息,取得上次借款的本金额度和利息
		LOLoanDB tLOLoanDB = new LOLoanDB();
		LOLoanSet tLOLoanSet = tLOLoanDB.executeQuery("select * from loloan where polno in (select polno from lcpol where contno='" + mLPEdorItemSchema.getContNo()
				+ "') and loantype='0' and payoffflag='0' with ur");
		// //loanType 1-自垫 0-借款
		// tLOLoanDB.setLoanType("0");
		// //1-还清
		// tLOLoanDB.setPayOffFlag("0");

		if (tLOLoanSet != null && tLOLoanSet.size() != 1) {
			// @@错误处理
			System.out.println("PEdorRFAppConfirmBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorRFAppConfirmBL";
			tError.functionName = "checkData";
			tError.errorMessage = "获取贷款表失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mReflections.transFields(mLPLoanSchema, tLOLoanSet.get(1));
		//actugetno为还款工单号
		mLPLoanSchema.setActuGetNo(mEdorNo);
		mLPLoanSchema.setEdorType(mEdorType);
		
		String tloanDate=new ExeSQL().getOneValue("select loandate from loloan where polno in (select polno from lcpol where contno='"+ mLPEdorItemSchema.getContNo() +"') and payoffflag='0' and loantype='0'");
		if(tloanDate!=null&&(!tloanDate.equals(""))&&(!tloanDate.equals("null"))){
			FDate chgdate = new FDate();
			Date rTloanDate = chgdate.getDate(tloanDate); 
        	Date tCurreDate = chgdate.getDate(mCurrentDate);
        	
        	if(rTloanDate.compareTo(tCurreDate) > 0){
        		
        		System.out.println("PEdorRFAppConfirmBL+checkData++--");
    			CError tError = new CError();
    			tError.moduleName = "PEdorRFAppConfirmBL";
    			tError.functionName = "checkData";
    			tError.errorMessage = "保单还款日期不能早于贷款起息日期"+tloanDate;
    			mErrors.addOneError(tError);
    			return false;
        	}
		}

		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		
		LCPolDB tLCPolDB=new LCPolDB();
		tLCPolDB.setPolNo(mLPLoanSchema.getPolNo());
		if(!tLCPolDB.getInfo()){
			// @@错误处理
			System.out.println("PEdorRFAppConfirmBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorRFAppConfirmBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取险种信息失败!";
			mErrors.addOneError(tError);
			return false;
		}
		LPPolSchema tLPPolSchema=new LPPolSchema();
		tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		mReflections.transFields(tLPPolSchema,tLCPolDB.getSchema());
		String aRiskCode=tLPPolSchema.getRiskCode();
		if(aRiskCode==null||aRiskCode.equals("")||aRiskCode.equals("null")){
			// @@错误处理
			System.out.println("PEdorRFAppConfirmBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorRFAppConfirmBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取险种失败!";
			mErrors.addOneError(tError);
			return false;
		}
		double tInterest=0;
		
		String countdate=mCurrentDate;
		
/*		redmine #1276 分红险保单贷款算法及规则修改 
 *		去掉复效后还款还款日期只算到失效当天的处理
 *		20130521
		String LoanAbateDate=new ExeSQL().getOneValue("select max(startdate)+ 1 day from lccontstate where polno='"+mLPLoanSchema.getPolNo()+"' and OtherNo='"+mLPLoanSchema.getEdorNo()+"' and StateType='Available' and StateReason='LN'");
		if(LoanAbateDate!=null&&(!LoanAbateDate.equals(""))&&(!LoanAbateDate.equals("null"))){
			countdate=LoanAbateDate;
		}
*/
		//查询保费以及保单管理机构
			LCContDB tLCContDB = new LCContDB();
			LCContSchema tLCContSchema = new LCContSchema();
			tLCContDB.setContNo(tLPPolSchema.getContNo());
			
			if(!tLCContDB.getInfo()){
				// @@错误处理
				System.out.println("PEdorRFAppConfirmBL+dealData++--");
				CError tError = new CError();
				tError.moduleName = "PEdorRFAppConfirmBL";
				tError.functionName = "dealData";
				tError.errorMessage = "获取保单失败!";
				mErrors.addOneError(tError);
				return false;				
			}
			tLCContSchema = tLCContDB.getSchema();
//		获取贷款利息
		AccountManage tAccountManage=new AccountManage();
		tInterest=CommonBL.carry(tAccountManage.getLoanInterest(mLPLoanSchema.getLoanDate(), mLPLoanSchema.getSumMoney(),tLCContSchema.getManageCom(),tLCContSchema.getPrem()+"", countdate, aRiskCode));
		if(tInterest==-1){
			// @@错误处理
			System.out.println("PEdorRFAppConfirmBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorRFAppConfirmBL";
			tError.functionName = "dealData";
			tError.errorMessage = "计算贷款利息失败!";
			mErrors.addOneError(tError);
			return false;
		}
		
//		 生成“保全还款业务表”数据-------------------->
		// 记录还款轨迹
		mLPReturnLoanSchema.setPolNo(mLPLoanSchema.getPolNo());
		mLPReturnLoanSchema.setEdorType(mLPLoanSchema.getEdorType());
		mLPReturnLoanSchema.setSerialNo(mLPLoanSchema.getSerialNo()); // 是否生成？
		mLPReturnLoanSchema.setActuGetNo(mLPLoanSchema.getActuGetNo());
		mLPReturnLoanSchema.setLoanType(mLPLoanSchema.getLoanType());
		mLPReturnLoanSchema.setOrderNo(mLPLoanSchema.getOrderNo());
		mLPReturnLoanSchema.setLoanDate(mLPLoanSchema.getLoanDate());
//		计算利息截止到前一天
		mLPReturnLoanSchema.setPayOffDate(mCurrentDate);
		mLPReturnLoanSchema.setSumMoney(mLPLoanSchema.getSumMoney());
		mLPReturnLoanSchema.setInputFlag(mLPLoanSchema.getInputFlag());
		mLPReturnLoanSchema.setInterestType(mLPLoanSchema.getInterestType());
		mLPReturnLoanSchema.setInterestRate(mLPLoanSchema.getInterestRate());
		mLPReturnLoanSchema.setInterestMode(mLPLoanSchema.getInterestMode());
		mLPReturnLoanSchema.setRateCalType(mLPLoanSchema.getRateCalType());
		mLPReturnLoanSchema.setRateCalCode(mLPLoanSchema.getRateCalCode());
		mLPReturnLoanSchema.setSpecifyRate(mLPLoanSchema.getSpecifyRate());
		mLPReturnLoanSchema.setLeaveMoney("0");
		mLPReturnLoanSchema.setPayOffFlag("1");
		mLPReturnLoanSchema.setOperator(this.mGlobalInput.Operator);
		mLPReturnLoanSchema.setMakeDate(mCurrentDate);
		mLPReturnLoanSchema.setMakeTime(mCurrentTime);
		mLPReturnLoanSchema.setModifyDate(mCurrentDate);
		mLPReturnLoanSchema.setModifyTime(mCurrentTime);
		mLPReturnLoanSchema.setEdorNo(mLPLoanSchema.getActuGetNo());//还款批单号
		mLPReturnLoanSchema.setLoanNo(mLPLoanSchema.getEdorNo()); // 借款批单号
		mLPReturnLoanSchema.setReturnMoney(mLPLoanSchema.getSumMoney());
		mLPReturnLoanSchema.setReturnInterest(tInterest);
		

		LJSGetEndorseSet tLJSGetEndorseSet = new LJSGetEndorseSet();
		BqCalBL bqCalBL = new BqCalBL();
//		还款金额
		LJSGetEndorseSchema tLoanLJSGetEndorseSchema = bqCalBL.initLJSGetEndorse(
                mLPEdorItemSchema, tLPPolSchema, null, "RFBF", mLPLoanSchema.getSumMoney(),mGlobalInput);
		tLJSGetEndorseSet.add(tLoanLJSGetEndorseSchema);
//		利息
		LJSGetEndorseSchema tInterestLJSGetEndorseSchema = bqCalBL.initLJSGetEndorse(
                mLPEdorItemSchema, tLPPolSchema, null, "RFLX", tInterest,mGlobalInput);
		tLJSGetEndorseSet.add(tInterestLJSGetEndorseSchema);
		

        //已经还款
		mLPLoanSchema.setPayOffFlag("1");
		mLPLoanSchema.setPayOffDate(mCurrentDate);
        mLPLoanSchema.setModifyDate(mCurrentDate);
        mLPLoanSchema.setModifyTime(mCurrentTime);

        mMap.put(mLPLoanSchema, "DELETE&INSERT");
		mMap.put(mLPReturnLoanSchema, "DELETE&INSERT");
		mMap.put(tLJSGetEndorseSet, "DELETE&INSERT");
		
		

		// 修改“个险保全项目表”相应信息
		mLPEdorItemSchema.setEdorValiDate(mCurrentDate);
		mLPEdorItemSchema.setEdorState("2");
		mLPEdorItemSchema.setOperator(this.mGlobalInput.Operator);
		mLPEdorItemSchema.setModifyDate(mCurrentDate);
		mLPEdorItemSchema.setModifyTime(mCurrentTime);
		mLPEdorItemSchema.setGetMoney(mLPLoanSchema.getSumMoney()+tInterest);
		mMap.put(mLPEdorItemSchema, "UPDATE");

		return true;
	}
}
