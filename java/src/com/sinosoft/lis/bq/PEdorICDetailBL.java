package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.text.DecimalFormat;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 个单/团单下个单被保人重要资料变更项目明细</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong,Alex
 * @version 1.0
 */
public class PEdorICDetailBL
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData;

	/** 往界面传输数据的容器 */
	private MMap mMap = new MMap();
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 地址标注符 */
	private String addrFlag;
	boolean newaddrFlag = false;

	/**  */
	private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
	private LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
	private LPEdorMainSet mLPEdorMainSet = new LPEdorMainSet();
	private LPInsuredSchema mLPInsuredSchema = new LPInsuredSchema();
	private LPInsuredSet mLPInsuredSet = new LPInsuredSet();
	private LPAppntSet mLPAppntSet = new LPAppntSet();
	private LPPersonSet mLPPersonSet = new LPPersonSet();

	//新增 BB和IC合并
	private LPAddressSchema mLPAddressSchema = new LPAddressSchema();

	private LPPremSet mLPPremSet = new LPPremSet();
	private LPGetSet mLPGetSet = new LPGetSet();
	private LPDutySet mLPDutySet = new LPDutySet();
	private LPPolSet mLPPolSet = new LPPolSet();
	private LPContSet mLPContSet = new LPContSet();
	private double mGetMoney = 0.0;
	private double mChgPrem = 0.0;
	private double mChgAmnt = 0.0;

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public PEdorICDetailBL()
	{
	}

/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据
	 *         cOperate 数据操作
	 * @return:
	 */
	public void setOperate(String cOperate)
	{
		this.mOperate = cOperate;
	}

	public String getOperate()
	{
		return this.mOperate;
	}

	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		this.setOperate(cOperate);

		//得到外部传入的数据,将数据备份到本类中
		getInputData(cInputData);

		//数据查询业务处理(queryData())
		if (cOperate.equals("QUERY||MAIN") || cOperate.equals("QUERY||DETAIL"))
		{
			if (!queryData())
			{
				return false;
			}
			else
			{
return true;
			}
		}

		//数据校验操作（checkdata)
		if (!checkData())
		{
			return false;
		}

		// 数据操作业务处理(新增insertData();修改updateData();删除deletedata())
		if (!dealData())
		{
			return false;
		}

		if (!prepareOutputData())
		{
			return false;
		}

		PubSubmit tSubmit = new PubSubmit();
		if (!tSubmit.submitData(mResult, ""))
		{
// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorICDetailBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	public boolean getSubmitData(VData cInputData, String cOperate) //不含数据提交的数据处理
	{
		//将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		this.setOperate(cOperate);

		//得到外部传入的数据,将数据备份到本类中
		getInputData(cInputData);

		//数据查询业务处理(queryData())
		if (cOperate.equals("QUERY||MAIN") || cOperate.equals("QUERY||DETAIL"))
		{
			if (!queryData())
			{
				return false;
			}
			else
			{
return true;
			}
		}

		//数据校验操作（checkdata)
		// 数据操作业务处理(新增insertData();修改updateData();删除deletedata())
		if (!dealData())
		{
			return false;
		}

		if (!prepareOutputData())
		{
			return false;
		}

		return true;
	}

	public VData getResult()
	{
		return mResult;
	}

	/**数据查询业务处理(queryData())
	 *
	 */
	private boolean queryData()
	{
		if (this.getOperate().equals("QUERY||MAIN"))
		{
			LPInsuredBL tLPInsuredBL = new LPInsuredBL();
			LPInsuredSet tLPInsuredSet = new LPInsuredSet();
			tLPInsuredBL.setSchema(mLPInsuredSchema);

			tLPInsuredSet = tLPInsuredBL.queryAllLPInsured(mLPEdorItemSchema);
			String tReturn;
			tReturn = tLPInsuredSet.encode();
			tReturn = "0|" + tLPInsuredSet.size() + "^" + tReturn;

			mInputData.clear();
			mInputData.add(tLPInsuredSet);

			mResult.clear();
			mResult.add(tReturn);
		}
		else if (this.getOperate().equals("QUERY||DETAIL"))
		{
			LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
			LPInsuredBL tLPInsuredBL = new LPInsuredBL();
			if (!tLPInsuredBL.queryLPInsured(mLPInsuredSchema))
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "PEdorICDetailBL";
				tError.functionName = "QueryData";
				tError.errorMessage = "明细查询失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			else
			{
				tLPInsuredSchema = tLPInsuredBL.getSchema();
			}

			String tReturn = tLPInsuredSchema.encode();
			mInputData.clear();
			mInputData.add(tLPInsuredSchema);

			mResult.clear();
			mResult.add(tReturn);
		}
		return true;
	}

/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
		addrFlag = (String) cInputData.get(0);
		mLPInsuredSchema = (LPInsuredSchema) cInputData.getObjectByObjectName(
				"LPInsuredSchema", 0);
		mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
				"LPEdorItemSchema", 0);
		//CM项目调用本文件，如果addrFlag的标志为CM，则不需要更新地址信息
		if (!"CM".equals(addrFlag))
		{
			mLPAddressSchema = (LPAddressSchema) cInputData.
					getObjectByObjectName("LPAddressSchema", 0);
		}
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		if (mLPInsuredSchema == null || mLPEdorItemSchema == null ||
			mGlobalInput == null)
		{
			mErrors.addOneError(new CError("数据传输不完全！"));
			return false;
		}
		return true;
	}

/**
	 * 校验传入的数据的合法性
	 * 输出：如果发生错误则返回false,否则返回true
	 */
	private boolean checkData()
	{
		boolean flag = true;
		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
		tLPEdorItemDB.setSchema(mLPEdorItemSchema);
		if (this.getOperate().equals("INSERT||MAIN"))
		{
			LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
			if (tLPEdorItemSet == null || tLPEdorItemSet.size() <= 0)
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "PEdorICDetailBL";
				tError.functionName = "Preparedata";
				tError.errorMessage = "无保全申请数据!";
				this.mErrors.addOneError(tError);
				return false;
			}

			//获取保全主表数据，节省查询次数
			mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1).getSchema());
			System.out.println("mLPEdorItemSchema.getEdorState()" +
							   mLPEdorItemSchema.getEdorState());

			if (mLPEdorItemSchema.getEdorState().trim().equals("2"))
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "PEdorICDetailBL";
				tError.functionName = "Preparedata";
				tError.errorMessage = "该保全已经申请确认不能修改!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}

		// 校验被保人投保年龄
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
		LCPolSet tLCPolSet = tLCPolDB.query();
		if (tLCPolDB.mErrors.needDealError())
		{
			mErrors.copyAllErrors(tLCPolDB.mErrors);
			mErrors.addOneError(new CError("查询个人险种保单失败！"));
			return false;
		}
		LCPolSchema tLCPolSchema = null;
		int n = tLCPolSet.size();
		System.out.println("come here" + n);
		for (int i = 1; i <= n; i++)
		{
			tLCPolSchema = tLCPolSet.get(i);
			LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
			tLMRiskAppDB.setRiskCode(tLCPolSchema.getRiskCode());
			System.out.println("come here" + tLCPolSchema.getRiskCode());
			if (tLMRiskAppDB.getInfo() == false)
			{
				// @@错误处理
				this.mErrors.copyAllErrors(tLMRiskAppDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "PEdorICDetailBL";
				tError.functionName = "checkData";
				tError.errorMessage = "LMRiskApp表查询失败!";
				this.mErrors.addOneError(tError);
				return false;
			}

			int tInsuredAge = PubFun.calInterval(mLPInsuredSchema.getBirthday(),
												 tLCPolSchema.getCValiDate(),
												 "Y");

			if (tInsuredAge < tLMRiskAppDB.getMinInsuredAge() ||
				(tInsuredAge > tLMRiskAppDB.getMaxInsuredAge() &&
				 tLMRiskAppDB.getMaxInsuredAge() > 0))
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "PEdorICDetailBL";
				tError.functionName = "dealDataPerson";
				tError.errorMessage = "被保人投保年龄不符合投保要求!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}

		return flag;
	}

/**
	 * 准备需要保存的数据
	 */
	private boolean dealData()
	{
		//准备个人批改主表的信息
		LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();

		//查询主险保单数据
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
		tLCPolDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
		LCPolSet tLCPolSet = tLCPolDB.query(); //从C表获得该合同下该被保人的所有险种保单信息
		if (tLCPolSet == null || tLCPolSet.size() <= 0)
		{
			mErrors.copyAllErrors(tLCPolDB.mErrors);
			mErrors.addOneError(new CError("查询个人险种保单失败！"));
			return false;
		}
		int n = tLCPolSet.size();
		for (int i = 1; i <= n; i++)
		{
			LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
			tLPEdorItemSchema.setSchema(mLPEdorItemSchema.getSchema());
			tLPEdorItemSchema.setInsuredNo(mLPInsuredSchema.getInsuredNo());
			tLPEdorItemSchema.setPolNo(tLCPolSet.get(i).getPolNo());
			tLPEdorItemSchema.setGrpContNo(tLCPolSet.get(i).getGrpContNo());
			tLPEdorItemSchema.setEdorState("1");
			tLPEdorItemSchema.setUWFlag("0");
			tLPEdorItemSchema.setChgPrem("0");
			tLPEdorItemSchema.setChgAmnt("0");
			tLPEdorItemSchema.setGetMoney("0");
			tLPEdorItemSchema.setGetInterest("0");
			tLPEdorItemSchema.setOperator(mGlobalInput.Operator);
			tLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
			tLPEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
			tLPEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
			tLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
			tLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
			mLPEdorItemSet.add(tLPEdorItemSchema); //为每个险种保单建立一个批改项目，只是方便后面的保费重算，但是最后并不插入数据库中
		}

		LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
		Reflections tReflections = new Reflections();
		tReflections.transFields(tLPEdorMainSchema, mLPEdorItemSet.get(1));
		tLPEdorMainSchema.setChgAmnt(0);
		tLPEdorMainSchema.setChgPrem(0);
		tLPEdorMainSchema.setGetMoney(0);
		tLPEdorMainSchema.setGetInterest(0);
		mLPEdorMainSet.add(tLPEdorMainSchema);

		//准备个人保单（保全）的信息
		tLPInsuredSchema.setSchema(mLPInsuredSchema);

		//准备cOperate
		LPInsuredBL tLPInsuredBL = new LPInsuredBL();
		LPInsuredSet tLPInsuredSet = new LPInsuredSet();
		tLPInsuredSet = tLPInsuredBL.queryLPInsured(mLPEdorItemSchema);
		if (tLPInsuredSet.size() <= 0)
		{
		    // @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorICDetailBL";
			tError.functionName = "Preparedata";
			tError.errorMessage = "被保人资料查询失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		else
		{
			for (int i = 1; i <= tLPInsuredSet.size(); i++)
			{
				if (tLPInsuredSet.get(i).getInsuredNo().equals(mLPInsuredSchema.
getInsuredNo()) &&
					tLPInsuredSet.get(i).getContNo().equals(mLPInsuredSchema.
						getContNo()))
				{
					tLPInsuredSchema.setSchema(tLPInsuredSet.get(i));
				}
			}
		}

		//准备保单的客户相关信息
		LPContSchema tLPContSchema = new LPContSchema();
		LPPolBL tLPPolBL = new LPPolBL();
		LPContBL tLPContBL = new LPContBL();
		LPEdorItemSchema tLPEdorItemSchema = mLPEdorItemSchema;
		double tOldPrem = 0;
		double tOldAmnt = 0;

		tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(mLPInsuredSchema.getContNo());
		tLCPolDB.setInsuredNo(mLPInsuredSchema.getInsuredNo());
	    tLCPolSet = tLCPolDB.query();
		for (int i = 1; i <= tLCPolSet.size(); i ++)
		{
			LPPolSchema tLPPolSchema = new LPPolSchema();
			Reflections ref = new Reflections();
			ref.transFields(tLPPolSchema, tLCPolSet.get(i));
			int tAppAge = PubFun.calInterval(mLPInsuredSchema.getBirthday(),
											 tLPPolSchema.getCValiDate(), "Y");
			tLPPolSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
			tLPPolSchema.setEdorType(tLPEdorItemSchema.getEdorType());
			tLPPolSchema.setInsuredSex(mLPInsuredSchema.getSex());
			tLPPolSchema.setInsuredBirthday(mLPInsuredSchema.getBirthday());
			tLPPolSchema.setInsuredName(mLPInsuredSchema.getName());
			tLPPolSchema.setInsuredAppAge(tAppAge);
			tLPPolSchema.setOccupationType(mLPInsuredSchema.getOccupationType());
			mLPPolSet.add(tLPPolSchema);
		}

		tLPContBL.queryLPCont(tLPEdorItemSchema);
		tLPContSchema = tLPContBL.getSchema();
		if (mLPInsuredSchema.getInsuredNo().equals(tLPContSchema.getInsuredNo())) //如果是主被保人，则更改合同单中被保人信息
		{
			tLPContSchema.setInsuredSex(mLPInsuredSchema.getSex());
			tLPContSchema.setInsuredBirthday(mLPInsuredSchema.getBirthday());
			tLPContSchema.setInsuredName(mLPInsuredSchema.getName());
			tLPContSchema.setInsuredIDType(mLPInsuredSchema.getIDType());
			tLPContSchema.setInsuredIDNo(mLPInsuredSchema.getIDNo());
		}
//        tLPContSchema.setPrem(Double.parseDouble(new DecimalFormat("0.00").format(
//                mChgPrem + tLPContSchema.getPrem())));
//            tLPContSchema.setSumPrem(Double.parseDouble(new DecimalFormat("0.00").format(
//                mChgPrem + tLPContSchema.getSumPrem())));
//        tLPContSchema.setAmnt(Double.parseDouble(new DecimalFormat("0.00").format(
//                mChgAmnt + tLPContSchema.getAmnt())));

		mLPContSet.add(tLPContSchema);

		if (tLPContSchema.getContType().equals("1") &&
			mLPInsuredSchema.getInsuredNo().equals(tLPContSchema.getAppntNo())) //若投保人和被保人相同，则修改投保人信息
		{
			LPAppntBL tLPAppntBL = new LPAppntBL();
			LPAppntSet tLPAppntSet = tLPAppntBL.queryLPAppnt(tLPEdorItemSchema);
			if (tLPAppntSet == null || tLPAppntSet.size() <= 0)
			{
				mErrors.copyAllErrors(tLPAppntSet.mErrors);
				mErrors.addOneError(new CError("查询投保人失败！"));
				return false;
			}
			LPAppntSchema tLPAppntSchema = tLPAppntSet.get(1);
			tLPAppntSchema.setAppntName(mLPInsuredSchema.getName());
			tLPAppntSchema.setAppntSex(mLPInsuredSchema.getSex());
			tLPAppntSchema.setAppntBirthday(mLPInsuredSchema.getBirthday());
			tLPAppntSchema.setIDNo(mLPInsuredSchema.getIDNo());
			tLPAppntSchema.setIDType(mLPInsuredSchema.getIDType());
			tLPAppntSchema.setOccupationCode(mLPInsuredSchema.getOccupationCode());
			tLPAppntSchema.setOccupationType(mLPInsuredSchema.getOccupationType());
			tLPAppntSchema.setMarriage(mLPInsuredSchema.getMarriage());
			mLPAppntSet.add(tLPAppntSchema);

			tLPContSchema.setAppntName(tLPAppntSchema.getAppntName());
			tLPContSchema.setAppntSex(tLPAppntSchema.getAppntSex());
			tLPContSchema.setAppntBirthday(tLPAppntSchema.getAppntBirthday());
			tLPContSchema.setAppntIDNo(tLPAppntSchema.getIDNo());
			tLPContSchema.setAppntIDType(tLPAppntSchema.getIDType());

			for (int i = 1; i <= mLPPolSet.size(); i++)
			{
				mLPPolSet.get(i).setAppntName(tLPAppntSchema.getAppntName());
			}
		}

		LPPersonBL tLPPersonBL = new LPPersonBL();
		LPPersonSchema tLPPersonSchema = new LPPersonSchema();
		tLPPersonSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
		tLPPersonSchema.setEdorType(tLPEdorItemSchema.getEdorType());
		tLPPersonSchema.setCustomerNo(tLPEdorItemSchema.getInsuredNo());
		if (!tLPPersonBL.queryLPPerson(tLPPersonSchema))
		{
			mErrors.copyAllErrors(tLPPersonBL.mErrors);
			mErrors.addOneError(new CError("查询投保人失败！"));
			return false;
		}
		tLPPersonSchema = tLPPersonBL.getSchema();
		tLPPersonSchema.setName(mLPInsuredSchema.getName());
		tLPPersonSchema.setSex(mLPInsuredSchema.getSex());
		tLPPersonSchema.setBirthday(mLPInsuredSchema.getBirthday());
		tLPPersonSchema.setIDNo(mLPInsuredSchema.getIDNo());
		tLPPersonSchema.setIDType(mLPInsuredSchema.getIDType());
		tLPPersonSchema.setOccupationCode(mLPInsuredSchema.getOccupationCode());
		tLPPersonSchema.setOccupationType(mLPInsuredSchema.getOccupationType());
		tLPPersonSchema.setMarriage(mLPInsuredSchema.getMarriage());
		mLPPersonSet.add(tLPPersonSchema);

		mLPEdorItemSchema.setInsuredNo(mLPInsuredSchema.getInsuredNo());
		mLPEdorItemSchema.setPolNo("000000");
		mLPEdorItemSchema.setGrpContNo(mLPPolSet.get(1).getGrpContNo());
		mLPEdorItemSchema.setEdorState("1");
		mLPEdorItemSchema.setUWFlag("0");
		mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
		mLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
		mLPEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
		mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());

		tLPInsuredSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPInsuredSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPInsuredSchema.setName(mLPInsuredSchema.getName());
		tLPInsuredSchema.setSex(mLPInsuredSchema.getSex());
		tLPInsuredSchema.setBirthday(mLPInsuredSchema.getBirthday());
		tLPInsuredSchema.setIDType(mLPInsuredSchema.getIDType());
		tLPInsuredSchema.setIDNo(mLPInsuredSchema.getIDNo());
		tLPInsuredSchema.setOccupationCode(mLPInsuredSchema.getOccupationCode());
		tLPInsuredSchema.setOccupationType(mLPInsuredSchema.getOccupationType());
		tLPInsuredSchema.setMarriage(mLPInsuredSchema.getMarriage());
		tLPInsuredSchema.setRelationToMainInsured(mLPInsuredSchema.
												  getRelationToMainInsured());

		//修改理赔金帐号 add by Lanjun 2005－05－24
		//----------------------------------------------------------------
		tLPInsuredSchema.setBankCode(mLPInsuredSchema.getBankCode());
		tLPInsuredSchema.setBankAccNo(mLPInsuredSchema.getBankAccNo());
		tLPInsuredSchema.setAccName(mLPInsuredSchema.getAccName());
		//----------------------------------------------------------------

		tLPInsuredSchema.setOperator(mGlobalInput.Operator);
		tLPInsuredSchema.setMakeDate(PubFun.getCurrentDate());
		tLPInsuredSchema.setMakeTime(PubFun.getCurrentTime());
		tLPInsuredSchema.setModifyDate(PubFun.getCurrentDate());
		tLPInsuredSchema.setModifyTime(PubFun.getCurrentTime());

		//更新地址号
		tLPInsuredSchema.setAddressNo(mLPAddressSchema.getAddressNo());

		mLPInsuredSchema.setSchema(tLPInsuredSchema);
		mLPInsuredSet.add(mLPInsuredSchema);

		if (!this.getOperate().equals("INSERT||GRPMAIN"))
		{
			LPInsuredDB tLPInsuredDB = new LPInsuredDB();
			tLPInsuredDB.setSchema(mLPInsuredSchema);
			if (!tLPInsuredDB.getInfo())
			{
//                this.setOperate("INSERT||MAIN");
			}
			else
			{
//                this.setOperate("UPDATE||MAIN");
			}
		}

		return true;
	}

	private boolean prepareOutputData()
	{
		UpdateEdorState tUpdateEdorState = new UpdateEdorState();
		mMap.put(tUpdateEdorState.getUpdateEdorStateSql(mLPEdorItemSchema),
				 "UPDATE");

		if (mOperate.equals("INSERT||MAIN") ||
			mOperate.equals("INSERT||GRPMAIN"))
		{
			String tSql = "delete from LPEdorItem where EdorNo = '" +
					mLPEdorItemSet.get(1).getEdorNo()
					+ "' and EdorType = '" + mLPEdorItemSet.get(1).getEdorType() +
					"' and PolNo = '000000'"
					+ " and ContNo = '" + mLPEdorItemSet.get(1).getContNo()
					+ "' and InsuredNo = '" +
					mLPEdorItemSet.get(1).getInsuredNo() + "'";
			mMap.put(tSql, "DELETE");
			mMap.put(mLPInsuredSet, "DELETE&INSERT");
			if (!"CM".equals(addrFlag))
			{
				mMap.put(mLPAddressSchema, "DELETE&INSERT");
			}
			mMap.put(mLPPersonSet, "DELETE&INSERT");
			mMap.put(mLPAppntSet, "DELETE&INSERT");
		}
		if (mOperate.equals("UPDATE||MAIN"))
		{
			mMap.put(mLPInsuredSet, "UPDATE");
			mMap.put(mLPEdorItemSchema, "UPDATE");
			mMap.put(mLPAddressSchema, "DELETE&INSERT");
			mMap.put(mLPAppntSet, "UPDATE");
			mMap.put(mLPPersonSet, "UPDATE");
		}
		mMap.put(mLPContSet, "DELETE&INSERT");
		mMap.put(mLPPolSet, "DELETE&INSERT");

		mResult.clear();
		mResult.add(mMap);

		return true;
	}

}
