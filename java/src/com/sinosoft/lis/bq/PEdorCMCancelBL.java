package com.sinosoft.lis.bq;

import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title:个单CM 删除BL层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong modify by Rainsky
 * @version 1.0
 */
public class PEdorCMCancelBL implements EdorCancel
{
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 往前面传输数据的容器 */
  private VData mResult = new VData();

  /**传输到后台处理的map*/
  private MMap mMap = new MMap();

  /** 数据操作字符串 */
  private String mOperate;
  private String mOperator;
  private String mManageCom;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  /**业务处理相关变量*/
  private GlobalInput mGlobalInput = new GlobalInput();

  private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

  private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema(); //新的表结构

  //统一更新日期，时间
  private String theCurrentDate = PubFun.getCurrentDate();
  private String theCurrentTime = PubFun.getCurrentTime();

  public PEdorCMCancelBL()
  {
  }

  /**
   * 处理实际的业务逻辑。
   * @param cInputData VData   从前台接收的表单数据
   * @param cOperate String    操作字符串
   * @return boolean
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
    //将数据取到本类变量中
    mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;

    //将VData数据还原成业务需要的类
    if (this.getInputData() == false)
    {
      System.out.println("批改状态不可以");
      return false;

    }

    System.out.println("---getInputData successful---");

    if (this.dealData() == false)
    {
      return false;
    }

    System.out.println("---dealdata successful---");

    // 装配处理好的数据，准备给后台进行保存
    this.prepareOutputData();
    System.out.println("---prepareOutputData---");

    PubSubmit tPubSubmit = new PubSubmit();

    if (!tPubSubmit.submitData(mResult, cOperate))
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);

      return false;
    }

    return true;
  }

  /**
   * 将UI层传输来得数据根据业务还原成具体的类
   * @return boolean
   */
  private boolean getInputData()
  {
    //全局变量实例
    ExeSQL tExeSQL = new ExeSQL();

    mGlobalInput.setSchema( (GlobalInput) mInputData.getObjectByObjectName(
        "GlobalInput", 0));

    if (mGlobalInput == null)
    {
      mErrors.addOneError(new CError("没有得到全局量信息"));

      return false;
    }

    mOperator = mGlobalInput.Operator;
    mManageCom = mGlobalInput.ManageCom;

    //团体保单实例
    mLPEdorItemSchema.setSchema( (LPEdorItemSchema) mInputData.
                                getObjectByObjectName(
        "LPEdorItemSchema", 0));

    if (mLPEdorItemSchema == null)
    {
      this.mErrors.addOneError(new CError("传入的信息为空！"));

      return false;
    }
    return true;
  }

  /**
   * 对业务数据进行加工
   * 对于新增的操作，这里需要有生成新合同号和新客户号的操作。
   * @return boolean
   */
  private boolean dealData()
  {
    String delSql;
    String tEdorNo = mLPEdorItemSchema.getEdorNo();

    String tEdorType = mLPEdorItemSchema.getEdorType();

    //处理LPedorMain表中的数据
    if (this.mOperate.equals("EDORMAINCANCEL"))
    {
//      String findSql =
//          "select * from LPedoritem where  edorType='CM' and edorNo <>'" +
//          tEdorNo +"' and edoracceptno in (select edoracceptno from LpedorMain where edorNo='" +
//          tEdorNo +"') and insuredNo in (select insuredno from Lpedoritem where edorNo ='" +
//          tEdorNo + "' and edorType='CM')";
//      LPEdorItemSchema fPEdorItemSchema = new LPEdorItemSchema();
//      LPEdorItemDB fLPEdorItemDB = new LPEdorItemDB();
//      LPEdorItemSet fLPEdorItemSet = new LPEdorItemSet();
//      fLPEdorItemSet = fLPEdorItemDB.executeQuery(findSql);
//
//      if (fLPEdorItemSet.size() >= 1 || fLPEdorItemSet != null) {
//        String[] tableForDel = {
//            "LPGeneral", "LPCont", "LPGeneralToRisk",
//            "LPPol",
//            "LPGetToAcc", "LPInsureAcc",
//            "LPPremToAcc", "LPPrem", "LPGet",
//            "LPDuty", "LPPrem_1", "LPInsureAccClass",
//            "LPInsureAccTrace",
//            "LPContPlanDutyParam", "LPContPlanRisk",
//            "LPContPlan",
//            "LPAppnt", "LPCustomerImpart",
//            "LPInsuredRelated",
//            "LPBnf", "LPInsured", "LPCustomerImpartParams",
//            "LPInsureAccFee", "LPInsureAccClassFee",
//            "LPContPlanFactory",
//            "LPContPlanParam", "LPMove",
//            "LPEdorPrint", "LPAppntTrace",
//            "LPLoan", "LPReturnLoan", "LPEdorPrint2",
//            "LPEdorPrint3",
//            "LPGUWError", "LPGUWMaster", "LPCUWError",
//            "LPCUWMaster",
//            "LPCUWSub",
//            "LPUWError", "LPUWMaster", "LPUWSub",
//            "LPPENoticeItem", "LPPENotice", "LPGCUWError",
//            "LPGCUWMaster",
//            "LPGCUWSub", "LPGUWSub", "LPSpec", "LPIssuePol",
//            "LPCSpec", "LPAccount",
//            "LPPerson", "LPAddress",
//            "LPPayRuleFactory", "LPPayRuleParams",
//            "LPRReportResult",
//            "LPRReport", "LPRReportItem",
//            "LPCustomerImpartDetail",
//            "LPAccMove", "LPEdorItem",
//            "LPEdorMain"};
//
//        for (int i = 1; i <= fLPEdorItemSet.size(); i++) {
//          fPEdorItemSchema = fLPEdorItemSet.get(i);
//          String fEdorNo = fPEdorItemSchema.getEdorNo();
//          for (int j = 0; j < tableForDel.length; j++) {
//            delSql = "delete from  " + tableForDel[j] + " where EdorNo = '" +
//                fEdorNo + "'";
//            System.out.println("************************//"+delSql);
//            mMap.put(delSql, "DELETE");
//          }
//
//          //删除批改补退费表中相关记录
//          delSql = "delete from  LJSGetEndorse where EndorsementNo = '" +
//              fEdorNo +
//              "'";
//          mMap.put(delSql, "DELETE");
//
//        }
//      }
    }

    //处理LPedorItem表中的数据
    else
    {

      String findSql =
          "select * from LPedoritem where  edorType='CM' and edorNo <>'" +
          tEdorNo +"' and edoracceptno in (select edoracceptno from LpedorMain where edorNo='" +
          tEdorNo +"') and insuredNo in (select insuredno from Lpedoritem where edorNo ='" +
          tEdorNo + "' and edorType='CM')";
      LPEdorItemSchema fPEdorItemSchema = new LPEdorItemSchema();
      LPEdorItemDB fLPEdorItemDB = new LPEdorItemDB();
      LPEdorItemSet fLPEdorItemSet = new LPEdorItemSet();
      fLPEdorItemSet = fLPEdorItemDB.executeQuery(findSql);
      if (fLPEdorItemSet.size() >= 1 || fLPEdorItemSet != null)
      {
        String[] tableForDel =
        {
            "LPGeneral", "LPCont", "LPGeneralToRisk", "LPPol",
            "LPGetToAcc", "LPInsureAcc",
            "LPPremToAcc", "LPPrem", "LPGet", "LPDuty", "LPPrem_1",
            "LPInsureAccClass", "LPInsureAccTrace", "LPContPlanDutyParam",
            "LPContPlanRisk", "LPContPlan", "LPAppnt", "LPCustomerImpart",
            "LPInsuredRelated", "LPBnf", "LPInsured",
            "LPCustomerImpartParams", "LPInsureAccFee",
            "LPInsureAccClassFee",
            "LPContPlanFactory", "LPContPlanParam",
            "LPMove", "LPAppntTrace", "LPLoan",
            "LPReturnLoan", "LPEdorPrint3", "LPGUWError", "LPGUWMaster",
            "LPCUWError",
            "LPCUWMaster", "LPCUWSub", "LPUWError", "LPUWMaster", "LPUWSub",
            "LPPENoticeItem", "LPPENotice",
            "LPGCUWError", "LPGCUWMaster", "LPGCUWSub", "LPGUWSub",
            "LPSpec", "LPIssuePol",
            "LPCSpec", "LPAccount", "LPPerson", "LPAddress",
            "LPPayRuleFactory", "LPPayRuleParams",
            "LPCustomerImpartDetail", "LPAccMove", "LPEdorItem"
        };

        String [] tableForDelEdor =
        {
            "LpedorPrint","LPEdorPrint2","LPRReportResult",
            "LPRReport","LPRReportItem","LPEodrMain"
        };

        for (int i = 1; i <= fLPEdorItemSet.size(); i++)
        {
          fPEdorItemSchema = fLPEdorItemSet.get(i);
          String fEdorNo = fPEdorItemSchema.getEdorNo();

          for (int j = 0; j < tableForDel.length; j++)
          {
            delSql = "delete from  " + tableForDel[j] + " where EdorNo = '" +
                fEdorNo + "' and EdorType='CM'";
            mMap.put(delSql, "DELETE");
          }
          delSql = "delete from  LJSGetEndorse where EndorsementNo = '" +fEdorNo +"'";
          mMap.put(delSql, "DELETE");
          for (int k = 0; k<tableForDelEdor.length; k++)
          {
            mMap.put("delete from "+ tableForDelEdor[k] +" where EdorNo= '" + fEdorNo +
                     "' and 0=(select count(*) from LpEdorItem where lpedorItem.EdorNo='" +
                     fEdorNo + "')","DELETE"); //若是最后一条项目，则删除对应批单
          }
        }
      }
    }
    return true;
  }

  /**
   * 准备数据，重新填充数据容器中的内容
   */
  private void prepareOutputData() {
    //记录当前操作员
    mResult.clear();
    mResult.add(mMap);
  }

  /**
   * 操作结果
   * @return VData
   */
  public VData getResult() {
    return mResult;
  }

  public static void main(String[] args) {
    System.out.println("dddd");
  }

}
