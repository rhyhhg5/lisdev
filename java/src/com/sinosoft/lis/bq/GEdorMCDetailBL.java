package com.sinosoft.lis.bq;

import java.util.Date;
import java.util.GregorianCalendar;

import utils.system;

import com.informix.util.stringUtil;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 客户资料变更</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong,Luomin
 * @author rewrite by QiuYang 2005
 * @version 1.0
 * 修改人：杨天政  时间：2011-04-22  原因：添加校验
 */

public class GEdorMCDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private DetailDataQuery mQuery = null;

    private GlobalInput mGlobalInput = null;

    private String mTypeFlag = null;

    private String stateFlag = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mAppntNo = null;

    private String mCustomerNo = null;

    private String mName = null;

    private String mSex = null;

    private String mBirthday = null;

    private String mIdType = null;

    private String mIdNo = null;

    private String mOccupationCode = null;

    private String mOccupationType = null;

    private String mMarriage = null;

    private String mRelation = null;

    private String mRelationToAppnt = null;
    
    private String mBankCode2 = null;

    private String mBankAccNo2 = null;

    private String mAccName2 = null;
    
    private String mBankCode = null;

    private String mBankAccNo = null;

    private String mAccName = null;

    private String mInsuredState = null;
    
    private TransferData mTransferData = null;
    
    private String mStartDate = null;
    
    private String mGetWay = null;
    
    private String mGrpContNo = null;

    private String mPosition = null;//增加团体万能保单级别
    
    private String mJoinCompanyDate = null;//增加团体万能保单服务年限

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到传入数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mTypeFlag = (String) cInputData.get(0);
            TransferData mTransferData = new TransferData();
            mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
            if(mTransferData != null)
            {
            	stateFlag = (String) mTransferData.getValueByName("stateFlag");
            }
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            LPEdorItemSchema tLPEdorItemSchema = (LPEdorItemSchema)
                    cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
            LDPersonSchema tLDPersonSchema = (LDPersonSchema)
                    cInputData.getObjectByObjectName("LDPersonSchema", 0);
            LCInsuredSchema tLCInsuredSchema = (LCInsuredSchema)
                    cInputData.getObjectByObjectName("LCInsuredSchema", 0);
            LCContSchema tLCContSchema = (LCContSchema)
            cInputData.getObjectByObjectName("LCContSchema", 0);
            mTransferData = (TransferData) cInputData.
            getObjectByObjectName("TransferData", 0);
            this.mStartDate = (String) mTransferData.getValueByName("startDate");
            this.mGetWay = (String) mTransferData.getValueByName("getWay");
            this.mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
            this.mEdorNo = tLPEdorItemSchema.getEdorNo();
            this.mEdorType = tLPEdorItemSchema.getEdorType();
            this.mContNo = tLPEdorItemSchema.getContNo();
            this.mAppntNo = tLPEdorItemSchema.getInsuredNo(); //这里存的是投保人
            this.mCustomerNo = tLDPersonSchema.getCustomerNo();
            this.mName = tLDPersonSchema.getName();
            this.mSex = tLDPersonSchema.getSex();
            this.mBirthday = tLDPersonSchema.getBirthday();
            this.mIdType = tLDPersonSchema.getIDType();
            this.mIdNo = tLDPersonSchema.getIDNo();
            this.mOccupationCode = tLDPersonSchema.getOccupationCode();
            this.mOccupationType = tLDPersonSchema.getOccupationType();
            this.mMarriage = tLDPersonSchema.getMarriage();
            this.mRelation = tLCInsuredSchema.getRelationToMainInsured();
            this.mBankCode2 = tLCInsuredSchema.getBankCode();
            this.mBankAccNo2 = tLCInsuredSchema.getBankAccNo();
            this.mPosition = tLCInsuredSchema.getPosition();
            this.mJoinCompanyDate = tLCInsuredSchema.getJoinCompanyDate();
            this.mRelationToAppnt = tLCInsuredSchema.getRelationToAppnt();
            this.mAccName2 = tLCInsuredSchema.getAccName();
            this.mInsuredState = tLCInsuredSchema.getInsuredStat();
            this.mQuery = new DetailDataQuery(mEdorNo, mEdorType);
            this.mAccName = tLCContSchema.getAccName();
            this.mBankAccNo = tLCContSchema.getBankAccNo();
            this.mBankCode = tLCContSchema.getBankCode();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    /*
     * 
     * */
    private String getOccupationType(String cOccupationCode){
    	String sql="select OccupationType from LDOccupation where '1218610553000'='1218610553000' and  OccupationCode = '"+cOccupationCode+"'  fetch first 2000 rows only ";
    	ExeSQL tExeSQL = new ExeSQL();     
    	String tOccupationType= tExeSQL.getOneValue(sql);
    	return tOccupationType;
    }
    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if ((mTypeFlag == null) || (mTypeFlag.equals("")))
        {
            mErrors.addOneError("缺少个/团单处理标志！");
            return false;
        }
        if ((!mTypeFlag.equals("G")) &&
                ((mAppntNo == null) || (mAppntNo.equals(""))))
        {
            mErrors.addOneError("投保人客户号为空！");
            return false;
        }
        if ((mCustomerNo == null) || (mCustomerNo.equals("")))
        {
            mErrors.addOneError("客户号为空！");
            return false;
        }
        if(!compareDate(mBirthday)){
        	mErrors.addOneError("输入的年龄不能比系统当前时间晚");
        	return false;
        }
        //万能险被保险人发生年龄性别职业变更暂时不允许操作。按照旧的客户资料变更这里会进行重算保费。 added by huxl @ 20080806,万能CM上线后需要重新修改
        if(CommonBL.hasULIRisk(this.mContNo)){
            if(isInsured()&&needChangePrem()){
               //mErrors.addOneError("万能险被保险人发生年龄性别职业变更暂时不允许操作！");
               //return false;
							Date mt = PubFun.calDate(new FDate().getDate(mCurrentDate), 1, "M", null);
							GregorianCalendar mCalendar = new GregorianCalendar();
							mCalendar.setTime(mt);
							int mYears = mCalendar.get(mCalendar.YEAR);
							int mMonths = mCalendar.get(mCalendar.MONTH) + 1;
							String monAfterCurrent = mYears + "-" + mMonths + "-"  + 1;
	            String sql = "update LPEdorItem " +
	                         "set EdorValiDate = '" +monAfterCurrent+"'" +
	                         " where EdorNo = '" + mEdorNo + "' " +
	                         " and EdorType = '" + mEdorType + "' " +
	                         " and InsuredNo = '" + mCustomerNo + "'";
	             mMap.put(sql, "UPDATE");
            }
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        //个单和团单分开操作，个单按投保人操作，团单按被保人操作
        if (mTypeFlag.equals("G"))
        {
        	if (IsCard()) {
       		 mErrors.addOneError("此单为激活卡,不允许做保全！");
                return false;
			}
        	 if(!setLPPerson()){
            	 
             	mErrors.addOneError("客户信息缺失。");
                  return false;
             }
             if(!setGrpLPCont()){
             	
             	 mErrors.addOneError("团单信息缺失。");
                  return false;
             }
             if(!setGrpLPInsured()){
             	
             	 mErrors.addOneError("团体被保人信息缺失");
                  return false;
             }
             if(!setGrpLPPol()){
             	
             	 mErrors.addOneError("团体险种信息缺失。");
                  return false;
             }
             if(!setOtherLP()){
             	
                  return false;
             }
             if(!setGrpLPGet()){
            	 mErrors.addOneError("给付表信息查询失败。");
                 return false;
             }
        }
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
    }
   private boolean setGrpLPGet(){
	   LPGetSchema tLPGetSchema = null;
	   LPGetSet tLPGetSet = new LPGetSet();
	   LCGetDB tLCGetDB = new LCGetDB();
	   tLCGetDB.setGrpContNo(mGrpContNo);
	   tLCGetDB.setContNo(mContNo);
	   tLCGetDB.setState("Y");
	   LCGetSet tLCGetSet = tLCGetDB.query();
	   if(tLCGetSet.size()==0){
		   return false;
	   }
	   Reflections reflections = new Reflections();
	   System.out.println(tLCGetSet.size());
	   for(int i=1;i<=tLCGetSet.size();i++){
		   tLPGetSchema = new LPGetSchema();
		   reflections.transFields(tLPGetSchema, tLCGetSet.get(i));
		   tLPGetSchema.setEdorNo(mEdorNo);
		   tLPGetSchema.setEdorType(mEdorType);
		   tLPGetSet.add(tLPGetSchema);
	   }
	   System.out.println(mStartDate+"2222222222222");
	   for(int j=1;j<=tLPGetSet.size();j++){
		   tLPGetSet.get(j).setGetDutyKind(mGetWay);
		   tLPGetSet.get(j).setGetStartDate(receiveDate());
	   }
	   mMap.put(tLPGetSet, "DELETE&INSERT");
	   return true;
   } 
    /**
     * 校验是否为激活卡
     * @return boolean
     */
    private boolean IsCard(){
    	ExeSQL texesql=new ExeSQL();
    	String tcard=texesql.getOneValue("select 1 from lccont where  contno='"+mContNo+"' and exists (select 1 from lcgrpcont where grpcontno=lccont.grpcontno and cardflag='2') and exists (select 1 from licertify where prtno=lccont.prtno and activeflag='02') ");
    	if ((tcard == null) || (tcard.equals(""))) {
			return false;
		}else
		{
    	return true;}
    }

    /**
     * 设置LPPerson表
     */
    private boolean setLPPerson()
    {
        LPPersonSchema tLPPersonSchema = (LPPersonSchema)
                mQuery.getDetailData("LDPerson", mCustomerNo);
        if(mName==null||mName.equals("")){
    		
    		mErrors.addOneError("客户姓名信息缺失");
    		return false;
    	}
        tLPPersonSchema.setName(mName);
        tLPPersonSchema.setSex(mSex);
        tLPPersonSchema.setBirthday(mBirthday);
        tLPPersonSchema.setIDType(mIdType);
        tLPPersonSchema.setIDNo(mIdNo);
        tLPPersonSchema.setOccupationCode(mOccupationCode);
        tLPPersonSchema.setOccupationType(mOccupationType);
        tLPPersonSchema.setMarriage(mMarriage);
        tLPPersonSchema.setState(mInsuredState);
        tLPPersonSchema.setOperator(mGlobalInput.Operator);
        tLPPersonSchema.setModifyDate(mCurrentDate);
        tLPPersonSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPPersonSchema, "DELETE&INSERT");
        
        return true;
    }
    
    /**
     * 设置LPInsured表
     */
    private boolean setLPInsured()
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        //tLCInsuredDB.setAppntNo(mAppntNo);
        //LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        String sql = "select * from LCInsured a where AppntNo = '" + mAppntNo + "' "
                   + "and exists (select 1 from LCCont where ContNo = a.ContNo and (StateFlag is null or StateFlag not in ('0')))";
        if(stateFlag != null && stateFlag.equals("0"))
        {
        	sql = "select * from LCInsured a where AppntNo = '" + mAppntNo + "' ";
        }
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.executeQuery(sql);
        
        if(tLCInsuredSet.size()==0)
        {
        	
        	 mErrors.addOneError("没有查到相关的被保人信息");
        	return false;
        }
        
        for (int i = 1; i <= tLCInsuredSet.size(); i++)
        {
            LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(i);
            System.out.println("mCustomerNo:" + mCustomerNo);
            if (tLCInsuredSchema.getInsuredNo().equals(mCustomerNo))
            {
                String[] keys = new String[2];
                keys[0] = tLCInsuredSchema.getContNo();
                keys[1] = tLCInsuredSchema.getInsuredNo();
                LPInsuredSchema tLPInsuredSchema = (LPInsuredSchema)
                        mQuery.getDetailData("LCInsured", keys);
                tLPInsuredSchema.setName(mName);
                tLPInsuredSchema.setSex(mSex);
                tLPInsuredSchema.setBirthday(mBirthday);
                tLPInsuredSchema.setIDType(mIdType);
                tLPInsuredSchema.setIDNo(mIdNo);
                tLPInsuredSchema.setOccupationCode(mOccupationCode);
                tLPInsuredSchema.setOccupationType(mOccupationType);
                tLPInsuredSchema.setMarriage(mMarriage);
                tLPInsuredSchema.setRelationToMainInsured(mRelation);
                tLPInsuredSchema.setRelationToAppnt(mRelationToAppnt);
                tLPInsuredSchema.setBankCode(mBankCode2);
                tLPInsuredSchema.setBankAccNo(mBankAccNo2);
                tLPInsuredSchema.setAccName(mAccName2);
                tLPInsuredSchema.setInsuredStat(mInsuredState);
                tLPInsuredSchema.setOperator(mGlobalInput.Operator);
                tLPInsuredSchema.setModifyDate(mCurrentDate);
                tLPInsuredSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPInsuredSchema, "DELETE&INSERT");
                setLPPol(tLPInsuredSchema.getContNo(),
                        tLPInsuredSchema.getInsuredNo());
            }
        }
        
        return true;
    }

    /**
     * 设置LCCont表
     */
    private boolean setLPCont()
    {
        LCContDB tLCContDB = new LCContDB();
        //tLCContDB.setAppntNo(mAppntNo);
        //LCContSet tLCContSet = tLCContDB.query();
        String sql = "select * from LCCont  where AppntNo = '" + mAppntNo + "' and (StateFlag is null or StateFlag not in ('0'))";
        if(stateFlag != null && stateFlag.equals("0"))
        {
        	sql = "select * from LCCont  where AppntNo = '" + mAppntNo + "'";
        }
        
        LCContSet tLCContSet = tLCContDB.executeQuery(sql);
        
        if(tLCContSet.size()==0)
        {
        	
        	 mErrors.addOneError("没有查到客户的保单");
        	return false;
        }
        for (int i = 1; i <= tLCContSet.size(); i++)
        {
            LCContSchema tLCContSchema = tLCContSet.get(i);
            if (mCustomerNo.equals(tLCContSchema.getAppntNo()) ||
                    mCustomerNo.equals(tLCContSchema.getInsuredNo()))
            {
                LPContSchema tLPContSchema = (LPContSchema)
                        mQuery.getDetailData("LCCont", tLCContSchema.getContNo());
                if (mCustomerNo.equals(tLCContSchema.getAppntNo()))
                {
                    tLPContSchema.setAppntName(mName);
                    tLPContSchema.setAppntSex(mSex);
                    tLPContSchema.setAppntBirthday(mBirthday);
                    tLPContSchema.setAppntIDType(mIdType);
                    tLPContSchema.setAppntIDNo(mIdNo);
                }
                if (mCustomerNo.equals(tLCContSchema.getInsuredNo()))
                {
                    tLPContSchema.setInsuredName(mName);
                    tLPContSchema.setInsuredSex(mSex);
                    tLPContSchema.setInsuredBirthday(mBirthday);
                    tLPContSchema.setInsuredIDType(mIdType);
                    tLPContSchema.setInsuredIDNo(mIdNo);
                }
                tLPContSchema.setOperator(mGlobalInput.Operator);
 //               tLPContSchema.setMakeDate(mCurrentDate);
//                tLPContSchema.setMakeTime(mCurrentTime);
                tLPContSchema.setModifyDate(mCurrentDate);
                tLPContSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPContSchema, "DELETE&INSERT");
            }
        }
        
        return true;
    }

    /**
     * 设置LPPol表
     */
    private void setLPPol(String contNo, String insuredNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(contNo);
        tLCPolDB.setInsuredNo(insuredNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLPolSchema = tLCPolSet.get(i);
            if (mCustomerNo.equals(tLPolSchema.getAppntNo()) ||
                    mCustomerNo.equals(tLPolSchema.getInsuredNo()))
            {
                LPPolSchema tLPPolSchema = (LPPolSchema)
                        mQuery.getDetailData("LCPol", tLPolSchema.getPolNo());
                if (mCustomerNo.equals(tLPolSchema.getAppntNo()))
                {
                    tLPPolSchema.setAppntName(mName);
                }
                if (mCustomerNo.equals(tLPolSchema.getInsuredNo()))
                {
                    int appAge = PubFun.calInterval(mBirthday,
                            tLPPolSchema.getCValiDate(), "Y");
                    tLPPolSchema.setInsuredName(mName);
                    tLPPolSchema.setInsuredSex(mSex);
                    tLPPolSchema.setInsuredBirthday(mBirthday);
                    tLPPolSchema.setInsuredAppAge(appAge);
                    tLPPolSchema.setOccupationType(mOccupationType);
                }
                tLPPolSchema.setOperator(mGlobalInput.Operator);
//                tLPPolSchema.setMakeDate(mCurrentDate);
 //               tLPPolSchema.setMakeTime(mCurrentTime);
                tLPPolSchema.setModifyDate(mCurrentDate);
                tLPPolSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPPolSchema, "DELETE&INSERT");
            }
        }
    }

    /**
     * 团单设置LPCont表
     */
    private boolean setGrpLPCont()
    {
        LPContSchema tLPContSchema = (LPContSchema)
                mQuery.getDetailData("LCCont", mContNo);
        //如果是连带被保人则不操作
        if (!mCustomerNo.equals(tLPContSchema.getInsuredNo()))
        {
            return true;
        }
        
       if(mSex==null||mSex.equals("")){
    		
    		mErrors.addOneError("客户性别信息缺失");
    		return false;
    	}
        
        tLPContSchema.setInsuredName(mName);
        tLPContSchema.setInsuredSex(mSex);
        tLPContSchema.setInsuredBirthday(mBirthday);
        tLPContSchema.setInsuredIDType(mIdType);
        tLPContSchema.setInsuredIDNo(mIdNo);
        tLPContSchema.setAccName(mAccName);
        tLPContSchema.setBankAccNo(mBankAccNo);
        tLPContSchema.setBankCode(mBankCode);
        tLPContSchema.setOperator(mGlobalInput.Operator);
        tLPContSchema.setModifyDate(mCurrentDate);
        tLPContSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPContSchema, "DELETE&INSERT");
        
        return true;
    }

    /**
     * 团单设置LPInsured表
     */
    private boolean setGrpLPInsured()
    {
        String[] keys = new String[2];
        keys[0] = mContNo;
        keys[1] = mCustomerNo;
        LPInsuredSchema tLPInsuredSchema = (LPInsuredSchema)
                mQuery.getDetailData("LCInsured", keys);
        
       if(mBirthday==null||mBirthday.equals("")){
    		
    		mErrors.addOneError("客户生日信息缺失");
    		return false;
    	}
        tLPInsuredSchema.setName(mName);
        tLPInsuredSchema.setSex(mSex);
        tLPInsuredSchema.setBirthday(mBirthday);
        tLPInsuredSchema.setIDType(mIdType);
        tLPInsuredSchema.setIDNo(mIdNo);
        tLPInsuredSchema.setOccupationCode(mOccupationCode);
        tLPInsuredSchema.setOccupationType(mOccupationType);
        tLPInsuredSchema.setMarriage(mMarriage);
        tLPInsuredSchema.setRelationToMainInsured(mRelation);
        tLPInsuredSchema.setRelationToAppnt(mRelationToAppnt);
        tLPInsuredSchema.setBankCode(mBankCode2);
        System.out.println("mPosition"+mPosition);
        tLPInsuredSchema.setPosition(mPosition);
        System.out.println("mJoinCompanyDate"+mJoinCompanyDate);
        tLPInsuredSchema.setJoinCompanyDate(mJoinCompanyDate);
        tLPInsuredSchema.setBankAccNo(mBankAccNo2);
        tLPInsuredSchema.setAccName(mAccName2);
        tLPInsuredSchema.setInsuredStat(mInsuredState);
        tLPInsuredSchema.setOperator(mGlobalInput.Operator);
        tLPInsuredSchema.setModifyDate(mCurrentDate);
        tLPInsuredSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsuredSchema, "DELETE&INSERT");
        
        return true;
    }

    /**
     * 团单设置LPPol表
     */
    private boolean setGrpLPPol()
    {
        
    	if(mContNo==null||mContNo.equals("")){
    		
    		mErrors.addOneError("保单号缺失。");
    		return false;
    	}
    	
        if(mCustomerNo==null||mCustomerNo.equals("")){
    		
    		mErrors.addOneError("客户号缺失。");
    		return false;
    	}
        
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mContNo);
        tLCPolDB.setInsuredNo(mCustomerNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLPolSchema = tLCPolSet.get(i);
            LPPolSchema tLPPolSchema = (LPPolSchema)
                    mQuery.getDetailData("LCPol", tLPolSchema.getPolNo());
            tLPPolSchema.setInsuredName(mName);
            tLPPolSchema.setInsuredSex(mSex);
            tLPPolSchema.setInsuredBirthday(mBirthday);
            tLPPolSchema.setOccupationType(mOccupationType);
            tLPPolSchema.setOperator(mGlobalInput.Operator);
            tLPPolSchema.setModifyDate(mCurrentDate);
            tLPPolSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPPolSchema, "DELETE&INSERT");
        }
        
        return true;
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
        String sql = "update  LPEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "and InsuredNo = '" + mCustomerNo + "'";
        mMap.put(sql, "UPDATE");
    }
    /**
    * 判断判断保全变更的客户是否是被保人
    * @return boolean
    */
   private boolean isInsured()
   {
       LCInsuredDB tLCInsuredDB = new LCInsuredDB();
       tLCInsuredDB.setContNo(this.mContNo);
       tLCInsuredDB.setInsuredNo(this.mCustomerNo);
       if (tLCInsuredDB.query().size() == 0)
       {
           return false;
       }
       return true;
   }
   /**
 * 判断是否需要重算保费
 * @return boolean 需要重算保费返回true
 */
private boolean needChangePrem()
{
    LCInsuredDB tLCInsuredDB = new LCInsuredDB();
    tLCInsuredDB.setContNo(mContNo);
    tLCInsuredDB.setInsuredNo(mCustomerNo);
    if (!tLCInsuredDB.getInfo())
    {
        mErrors.addOneError("找不到被保人信息！");
        return false;
    }
    String tOccupationType="";
    if(tLCInsuredDB.getOccupationType()==null||"".equals(tLCInsuredDB.getOccupationType().trim())){
    	tOccupationType = getOccupationType(tLCInsuredDB.getOccupationCode());
    }else{
    	/*
    	 * >cbs00017611
           >cbs00017569
		   >cbs00017366
           >
           >这三个cq都是同一个问题。程序改完了，你找个单子测试下，没问题就 check in到vss上，我看下。如果可以的话，就更新正式机。

    	 * */
    	tOccupationType =tLCInsuredDB.getOccupationType();
    }
   // 如果性别和年龄改变则重算保额保费
    if (!StrTool.compareString(tLCInsuredDB.getSex(),
                               mSex) ||
        !StrTool.compareString(tLCInsuredDB.getBirthday(),
                               mBirthday) 
//                               ||
//        !StrTool.compareString(tOccupationType,
//                               mOccupationType)
                               )
    {
        return true;
    }
    return false;
}

/**
 * 同时处理该客户作为其它团单被保人的资料变更
 * 20090320 zhanggm cbs00024868 
 */
private boolean setOtherLP()
{
	StringBuffer sql = new StringBuffer();
	sql.append("select contno from lcinsured a where insuredno = '" +mCustomerNo + "' ");
	sql.append("and grpcontno <> '" + BQ.GRPFILLDATA + "' ");
	sql.append("and contno <> '" + mContNo + "'");
	sql.append("and exists (select 1 from lccont where contno = a.contno and appflag = '1') ");
	System.out.println("查询关联保单："+sql);
	SSRS tSSRS = new SSRS();
	tSSRS = new ExeSQL().execSQL(sql.toString());
	
	if(tSSRS.MaxRow==0)
	{
		System.out.println("被保人"+mCustomerNo+"没有其它关联团单需要变更");
		return true;
	}
	else
	{
		for(int i=1;i<=tSSRS.MaxRow;i++)
		{
			String tContNo = tSSRS.GetText(i, 1);
			System.out.println("PEdorCMDetailBL.java-L613->保单："+tContNo);
			
			 if(tContNo==null||tContNo.equals("")){
	             	
                 return false;
            }

			 if(!setOtherGrpLPCont(tContNo)){
	             	
                 return false;
            }
			 if(!setOtherGrpLPInsured(tContNo)){
	             	
                 return false;
            }
			 if(!setOtherGrpLPPol(tContNo)){
	             	
                 return false;
            }
		}
	}
	
	return true;
}

/**
 * 该客户作为其它团单被保人的团单设置LPCont表
 */
private boolean setOtherGrpLPCont(String aContNo)
{
	LPContSchema tLPContSchema = (LPContSchema)mQuery.getDetailData("LCCont", aContNo);
    //如果是连带被保人则不操作
    if (!mCustomerNo.equals(tLPContSchema.getInsuredNo()))
    {
        return true;
    }
	tLPContSchema.setInsuredName(mName);
    tLPContSchema.setInsuredSex(mSex);
    tLPContSchema.setInsuredBirthday(mBirthday);
    tLPContSchema.setInsuredIDType(mIdType);
    tLPContSchema.setInsuredIDNo(mIdNo);
    tLPContSchema.setOperator(mGlobalInput.Operator);
    tLPContSchema.setModifyDate(mCurrentDate);
    tLPContSchema.setModifyTime(mCurrentTime);
    mMap.put(tLPContSchema, "DELETE&INSERT");
    
    return true;
}

/**
 * 该客户作为其它团单被保人的团单设置LPInsured表
 */
private boolean setOtherGrpLPInsured(String aContNo)
{
    String[] keys = new String[2];
    keys[0] = aContNo;
    keys[1] = mCustomerNo;
    LPInsuredSchema tLPInsuredSchema = (LPInsuredSchema)mQuery.getDetailData("LCInsured", keys);
    tLPInsuredSchema.setName(mName);
    tLPInsuredSchema.setSex(mSex);
    tLPInsuredSchema.setBirthday(mBirthday);
    tLPInsuredSchema.setIDType(mIdType);
    tLPInsuredSchema.setIDNo(mIdNo);
    tLPInsuredSchema.setPosition(mPosition);//  增加团体万能保单级别
    tLPInsuredSchema.setJoinCompanyDate(mJoinCompanyDate);//  增加团体万能保单级别
    tLPInsuredSchema.setOccupationCode(mOccupationCode);
    tLPInsuredSchema.setOccupationType(mOccupationType);
    tLPInsuredSchema.setMarriage(mMarriage);
    tLPInsuredSchema.setRelationToMainInsured(mRelation);
    tLPInsuredSchema.setRelationToAppnt(mRelationToAppnt);
    tLPInsuredSchema.setBankCode(mBankCode2);
    tLPInsuredSchema.setBankAccNo(mBankAccNo2);
    tLPInsuredSchema.setAccName(mAccName2);
    tLPInsuredSchema.setInsuredStat(mInsuredState);
    tLPInsuredSchema.setOperator(mGlobalInput.Operator);
    tLPInsuredSchema.setModifyDate(mCurrentDate);
    tLPInsuredSchema.setModifyTime(mCurrentTime);
    mMap.put(tLPInsuredSchema, "DELETE&INSERT");
    
    return true;
}

/**
 * 该客户作为其它团单被保人的团单设置LPPol表
 */
private boolean setOtherGrpLPPol(String aContNo)
{
	LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setContNo(aContNo);
    tLCPolDB.setInsuredNo(mCustomerNo);
    LCPolSet tLCPolSet = tLCPolDB.query();
    for (int i = 1; i <= tLCPolSet.size(); i++)
    {
        LCPolSchema tLPolSchema = tLCPolSet.get(i);
        LPPolSchema tLPPolSchema = (LPPolSchema)mQuery.getDetailData("LCPol", tLPolSchema.getPolNo());
        tLPPolSchema.setInsuredName(mName);
        tLPPolSchema.setInsuredSex(mSex);
        tLPPolSchema.setInsuredBirthday(mBirthday);
        tLPPolSchema.setOccupationType(mOccupationType);
        tLPPolSchema.setOperator(mGlobalInput.Operator);
        tLPPolSchema.setModifyDate(mCurrentDate);
        tLPPolSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPPolSchema, "DELETE&INSERT");
    }
    
    return true;
}

private boolean compareDate(String date1){
	if(!PubFun.validateDate(date1)){
		return false;
	}
	return true;
}


    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    
    private String receiveDate(){
    	String mbirYear = mBirthday.substring(0,4);
    	String mcurrYear = PubFun.getCurrentDate().substring(0,4);
    	
    	//timeJG 为个人年龄
    	int timeJG=Integer.parseInt(mcurrYear)-Integer.parseInt(mbirYear)-1;
    	//getStateDate 还需要多少年投保
    	int getStateDate = Integer.parseInt(mStartDate)-timeJG;
    	
    	int getEndDateYear = getStateDate+Integer.parseInt(mcurrYear);
    	
    	String sqlString  = "select cvalidate from lcpol where grpcontno='"+mGrpContNo+"' and contno='"+mContNo+"'";
    	String cvalidate = new ExeSQL().getOneValue(sqlString);
    	
    	String result = getEndDateYear+"-"+cvalidate.substring(6);
    	return result;
    }
}

