package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PGrpEdorCTDetailBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
    private LPContPlanRiskSet mLPContPlanRiskSet = null;
    private LPEdorMainSet mLPEdorMainSet = null;
    private LPEdorItemSet mLPEdorItemSet = null;
    private LPContSet mLPContSet = null;
    private LPInsuredSet saveLPInsuredSet = null;
    private LPDiskImportSet mLPDiskImportSet = null;

    /** 全局数据 */
    private Reflections ref = new Reflections();
    private GlobalInput mGlobalInput = null;
    MMap map = null;
    private String currDate = PubFun.getCurrentDate();
    private String currTime = PubFun.getCurrentTime();
    private String mEdorNo = null;
    private String mEdorType = null;
    private String mGrpContNo = null;

    public PGrpEdorCTDetailBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */

    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("...Now in PGrpEdorCTDetail->submitData");
        if(!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }
        if (!prepareData())
        {
            return false;
        }

        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(mResult, cOperate) == false)
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        map.put(mLPEdorMainSet, "DELETE&INSERT");
        map.put(mLPEdorItemSet, "DELETE&INSERT");
        map.put(mLPContSet, "DELETE&INSERT");
        //map.put(mLPPolSet, "DELETE&INSERT");
        map.put(mLPGrpEdorItemSchema, "UPDATE");
        map.put(mLPContPlanRiskSet, "INSERT");
        //map.put(saveLPInsuredSet, "DELETE&INSERT");
        mResult.clear();
        mResult.add(map);
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData
                               .getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mGlobalInput = (GlobalInput) cInputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLPContPlanRiskSet = (LPContPlanRiskSet) cInputData
                             .getObjectByObjectName("LPContPlanRiskSet", 0);

        if (mLPContPlanRiskSet == null || mLPContPlanRiskSet.size() == 0
            || mGlobalInput == null || mLPGrpEdorItemSchema == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        return true;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {
        boolean flag = true;
        String reasonCode = mLPGrpEdorItemSchema.getReasonCode();

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());

        if (!tLPGrpEdorItemDB.getInfo())
        {
            this.mErrors.addOneError("无保全项目数据。");
            return false;
        }
        if (tLPGrpEdorItemDB.getEdorState().trim().equals("2"))
        {
            this.mErrors.addOneError("该保全项目已经申请确认不能修改");
            return false;
        }
        mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemDB.getSchema());
        mLPGrpEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
        mLPGrpEdorItemSchema.setReasonCode(reasonCode);

        this.mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
        this.mEdorType = mLPGrpEdorItemSchema.getEdorType();
        this.mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
        if(mEdorNo == null || mEdorType == null || mGrpContNo == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        //若选择了特需险的公共账户保障计划，则需选择特需被保人的所有保障计划
        //若现在了特需险的所有被保人保障计划，则需选择公共账户的保障计划
        for(int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
            if(!CommonBL.isEspecialPol(mLPContPlanRiskSet.get(i).getRiskCode()))
            {
                continue;
            }
            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
            tLCContPlanRiskDB.setGrpContNo(mLPContPlanRiskSet.get(i)
                                           .getGrpContNo());
            tLCContPlanRiskDB.setRiskCode(mLPContPlanRiskSet.get(i)
                                          .getRiskCode());
            LCContPlanRiskSet set = tLCContPlanRiskDB.query(); //特需险保障计划数
            int selectedPlanCount = 0; //选择了的当前保障计划险种下的保障计划数
            //本保障是否公共帐户保障
            boolean isPublicAccPlan = mLPContPlanRiskSet.get(i).getContPlanCode()
                                      .equals("11");
            boolean hasPublicAccPlan = false;  //当前选择的保障计划是否包含公共帐户保障计划
            //得到险种下所有被选择的保障计划数，并判断当前保障计划是否公共帐户保障计划
            for(int t = 1; t <= mLPContPlanRiskSet.size(); t++)
            {
                if(!mLPContPlanRiskSet.get(i).getRiskCode()
                   .equals(mLPContPlanRiskSet.get(t).getRiskCode())) //只处理当前险种
                {
                    continue;
                }
                if(mLPContPlanRiskSet.get(t).getContPlanCode().equals("11"))
                {
                    hasPublicAccPlan = true;
                }
                selectedPlanCount++;
            }
            if (isPublicAccPlan && selectedPlanCount != set.size())
            {
                mErrors.addOneError("您选择了特需险种公共账户保障计划，"
                                    + "请同时选择所有被保人保障计划。");
                return false;
            }
            //特需险种保障计划数 = 被保人保障计划数 + 1(公共帐户保障计划数)
            if(!isPublicAccPlan && !hasPublicAccPlan
               && set.size() == selectedPlanCount + 1)
            {
                mErrors.addOneError("您选择了所有特需险种的被保人保障计划，"
                                    + "请同时选择公共账户保障计划。");
                return false;
            }
        }

        return flag;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        map = new MMap();

        if(!dealLPContPlanRisk())
        {
            return false;
        }

        String contPlanCodes = "";
        String riskCodes = "";
        for (int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
            contPlanCodes += "'"
                + mLPContPlanRiskSet.get(i).getContPlanCode() + "',";
            riskCodes += "'" + mLPContPlanRiskSet.get(i).getRiskCode() + "',";
        }
        contPlanCodes = contPlanCodes.substring(0, contPlanCodes.length() - 1);
        riskCodes = riskCodes.substring(0, riskCodes.length() - 1);
        System.out.println("contPlanCodes: " + contPlanCodes + "   "
                           + "riskCodes: " + riskCodes);

        //查询条件
        StringBuffer fromAndCondition = new StringBuffer();
        fromAndCondition.append("from LCInsured a, LCPol b ")
            .append("where a.contNo = b.contNo ")
            .append("  and a.insuredNo = b.insuredNo ")
            .append("   and a.contPlanCode in (").append(contPlanCodes)
            .append(")  and b.riskCode in (").append(riskCodes)
            .append(")  and a.grpContNo = '").append(mGrpContNo)
            .append("' ");
        System.out.println(fromAndCondition);

        if(!createLCGrpCont())
        {
            return false;
        }

        if(!createLPGrpPol(riskCodes))
        {
            return false;
        }

        if(!createLPEdorInfo())
        {
            return false;
        }

        //生成被保人保全数据
        if(!createLPInsured())
        {
            return false;
        }

        //生成被保人清单信息
        if(!createLPDiskImport())
        {
            return false;
        }

        //生成险种信息
        if(!createLPPol())
        {
            return false;
        }

//        if(!dealPublicAccPol(riskCodes))
//        {
//            return false;
//        }

        if(!changeCommonInfo())
        {
            return false;
        }
        System.out.println("End of prepareData.");

        return true;
    }

    /**
     * 生成团单保单保全数据
     * @return boolean
     */
    private boolean createLCGrpCont()
    {
        map.put("  delete from LPGrpCont "
                + "where edorNo = '" + mEdorNo + "' "
                + "  and edorType = '" + mEdorType + "' "
                + "  and grpContNo = '" + mGrpContNo + "' ", "DELETE");
        StringBuffer tLPInsuredSql = new StringBuffer();

        tLPInsuredSql.append("insert into LPGrpCont (")
            .append("select '").append(mEdorNo).append("', ")
            .append("'").append(mEdorType).append("', ")
            .append("a.* ")
            .append("from LCGrpCont a ")
            .append("where grpContNo = '").append(mGrpContNo)
            .append("') ");
        System.out.println(tLPInsuredSql.toString());
        map.put(tLPInsuredSql.toString(), "INSERT");

        return true;
    }

    /**
     * 生成团单保单保全数据
     * @return boolean
     */
    private boolean createLPGrpPol(String riskCodes)
    {
        map.put("  delete from LPGrpPol "
                + "where edorNo = '" + mEdorNo + "' "
                + "   and edorType = '" + mEdorType + "' "
                + "   and grpContNo = '" + mGrpContNo + "' ", "DELETE");
        StringBuffer tLPInsuredSql = new StringBuffer();

        tLPInsuredSql.append("insert into LPGrpPol (")
            .append("select '").append(mEdorNo).append("', ")
            .append("'").append(mEdorType).append("', ")
            .append("a.* ")
            .append("from LCGrpPol a ")
            .append("where grpContNo = '").append(mGrpContNo)
            .append("'  and riskCode in (").append(riskCodes).append(")) ");
        System.out.println(tLPInsuredSql.toString());
        map.put(tLPInsuredSql.toString(), "INSERT");

        return true;
    }

    private boolean createLPPol()
    {
        //生成被保人保全数据
        map.put("  delete from LPPol "
                + "where edorNo = '" + mEdorNo + "' "
                + "  and edorType = '" + mEdorType + "' "
                + "  and grpContNo = '" + mGrpContNo + "' ", "DELETE");

        String polNos = getOneFieldValues("LCPol", "polNo", "");
        if(polNos.equals(""))
        {
            return false;
        }
        StringBuffer tLPInsuredSql = new StringBuffer();
        tLPInsuredSql.append("insert into LPPol (")
            .append("select '").append(mEdorNo).append("', ")
            .append("'").append(mEdorType).append("', ")
            .append("a.* ")
            .append("from LCPol a ")
            .append("where polNo in (").append(polNos).append(") ")
            .append(") ");
        System.out.println(tLPInsuredSql.toString());
        map.put(tLPInsuredSql.toString(), "INSERT");

        return true;
    }

    /**
     * 处理保障计划
     * @return boolean
     */
    private boolean dealLPContPlanRisk()
    {
        map.put("  delete from LPContPlanRisk "
                + "where edorNo = '" + mEdorNo + "' "
                + "  and edorType = '" + mEdorType + "' "
                + "  and grpContNo = '" + mGrpContNo + "' ", "DELETE");

        LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
        for (int i = 1; i <= this.mLPContPlanRiskSet.size(); i++)
        {
            tLCContPlanRiskDB.setGrpContNo(mGrpContNo);
            tLCContPlanRiskDB.setContPlanCode(
                mLPContPlanRiskSet.get(i).getContPlanCode());
            tLCContPlanRiskDB.setMainRiskCode(
                mLPContPlanRiskSet.get(i).getMainRiskCode());
            tLCContPlanRiskDB.setRiskCode(mLPContPlanRiskSet.get(i).getRiskCode());
            tLCContPlanRiskDB.setPlanType(mLPContPlanRiskSet.get(i).getPlanType());

            LCContPlanRiskSet set = tLCContPlanRiskDB.query();
            if(set.size() == 0)
            {
                mErrors.addOneError("没有查询到相应的保障计划。");
                return false;
            }
            LPContPlanRiskSchema schema = new LPContPlanRiskSchema();
            ref.transFields(schema, set.get(1));
            schema.setEdorNo(mEdorNo);
            schema.setEdorType(mEdorType);
            PubFun.fillDefaultField(schema);
            mLPContPlanRiskSet.set(i, schema);
        }

        return true;
    }

    private boolean createLPDiskImport()
    {
        //得到本工单的导入被保人表的最大序列号
        String maxSerialNo = getMaxSerialNo();
        if(maxSerialNo == null)
        {
            return false;
        }

        String insuredNos = getOneFieldValues("LCInsured", "insuredNo", "");
        String queryLCInsured = "  select a.* "
                                + "from LCInsured a "
                                + "where grpContNo = '" + mGrpContNo
                                + "'   and insuredNo in (" + insuredNos + ") ";
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        LCInsuredSet set;
        int start = 1;
        int count = 1000;
        mLPDiskImportSet = new LPDiskImportSet();
        do
        {
            set = tLCInsuredDB.executeQuery(queryLCInsured, start, count);
            for(int i = 1; i <= set.size(); i++)
            {
                LPDiskImportSchema schema = new LPDiskImportSchema();
                ref.transFields(schema, set.get(i));
                schema.setEdorNo(mEdorNo);
                schema.setEdorType(mEdorType);
                schema.setState("1");
                schema.setSerialNo(
                    String.valueOf(Long.parseLong(maxSerialNo) + start + i - 1));
                schema.setInsuredName(set.get(i).getName());
                PubFun.fillDefaultField(schema);
                mLPDiskImportSet.add(schema);
            }
            start += count;
        }
        while (set.size() > 0);
        map.put("  delete from LPDiskImport "
                + "where edorNo = '" + mEdorNo + "' "
                + "  and edorType = '" + mEdorType + "' "
                + "  and grpContNo = '" + mGrpContNo + "' ", "DELETE");
        map.put(mLPDiskImportSet, "INSERT");

        return true;
    }

    /**
     * 得到本次保全所需的table表的field字段的所有值,
     * table必须是LCCont, LCInsured, LCPol中的一个
     * @param table String
     * @param field String
     * @return String[]
     */
    private String getOneFieldValues(String table,
                                     String field,
                                     String condition)
    {
        StringBuffer sql = new StringBuffer();
        for (int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
            sql.append("select ").append(table).append(".").append(field)
                .append(" from LCCont, LCInsured , LCPol ")
                .append("where LCCont.contNo = LCInsured.contNo ")
                .append("   and LCInsured.contNo = LCPol.contNo ")
                .append("   and LCInsured.insuredNo = LCPol.insuredNo ")
                .append("   and LCInsured.contPlanCode = '")
                .append(mLPContPlanRiskSet.get(i).getContPlanCode())
                .append("'   and LCPol.riskCode = '")
                .append(mLPContPlanRiskSet.get(i).getRiskCode())
                .append("'   and LCCont.grpContNo = '")
                .append(mGrpContNo)
                .append("' ")
                .append(condition)
                .append(" union ");
        }
        SSRS tSSRS = null;
        try
        {
            String fieldValueSql = sql.substring(0,
                                                 sql.lastIndexOf("union") - 1);
            System.out.println(fieldValueSql);

            ExeSQL e = new ExeSQL();
            tSSRS = e.execSQL(fieldValueSql);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (tSSRS == null || tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("查询出错。");
            return "";
        }

        StringBuffer fieldValues = new StringBuffer();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            fieldValues.append("'").append(tSSRS.GetText(i, 1)).append("',");
        }
        if (fieldValues.length() == 0)
        {
            return "";
        }
        return fieldValues.toString().substring(0, fieldValues.lastIndexOf(","));
    }

    private boolean createLPInsured()
    {
        //生成被保人保全数据
        map.put("  delete from LPInsured "
                + "where edorNo = '" + mEdorNo + "' "
                + "  and edorType = '" + mEdorType + "' "
                + "  and grpContNo = '" + mGrpContNo + "' ", "DELETE");

        //通过保障计划和险种确定被保人
        String insuredNos = getOneFieldValues("LCInsured", "insuredNo", "");
        if(insuredNos.equals(""))
        {
            return false;
        }
        StringBuffer tLPInsuredSql = new StringBuffer();
        tLPInsuredSql.append("insert into LPInsured (")
            .append("select '").append(mEdorNo).append("', ")
            .append("'").append(mEdorType).append("', ")
            .append("a.* from LCInsured a ")
            .append("where insuredNo in (").append(insuredNos).append(")" )
            .append("    and grpContNo = '").append(mGrpContNo).append("') ");
        System.out.println(tLPInsuredSql.toString());

        map.put(tLPInsuredSql.toString(), "INSERT");

        return true;
    }

    private boolean createLPEdorInfo()
    {
        //生成被保人保全数据
        if(!deleteOldLPEdorInfo())
        {
            return false;
        }

        String contNos = getOneFieldValues("LCCont", "contNo", "");
        if(contNos.equals(""))
        {
            return false;
        }
        StringBuffer sql = new StringBuffer();
        sql.append(" select a.* ")
            .append("from LCCont a ")
            .append("where a.contNo in (").append(contNos).append(")");
        System.out.println(sql);

        mLPEdorItemSet = new LPEdorItemSet();
        mLPEdorMainSet = new LPEdorMainSet();
        LCContDB tLCContDB = new LCContDB();
        mLPContSet = new LPContSet();
        LCContSet set = null;
        int start= 1;
        int count = 100;

        do
        {
            set = tLCContDB.executeQuery(sql.toString(), start, count);
            for (int i = 1; i <= set.size(); i++)
            {
                LCContSchema tLCContSchema = set.get(i);
                if(!createOneLPEdorInfo(tLCContSchema))
                {
                    return false;
                }
            }
            start += count;
        }
        while(set.size() > 0);

        return true;
    }

    private boolean createOneLPEdorInfo(LCContSchema tLCContSchema)
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);
        tLPEdorItemSchema.setContNo(tLCContSchema.getContNo());
        tLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
        tLPEdorItemSchema.setPolNo(BQ.FILLDATA);
        tLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        tLPEdorItemSchema.setUWFlag("0");
        tLPEdorItemSchema.setMakeDate(currDate);
        tLPEdorItemSchema.setMakeTime(currTime);
        tLPEdorItemSchema.setModifyDate(currDate);
        tLPEdorItemSchema.setModifyTime(currTime);
        tLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
        mLPEdorItemSet.add(tLPEdorItemSchema);

        LPContSchema tLPContSchema = new LPContSchema();
        ref.transFields(tLPContSchema, tLCContSchema);
        tLPContSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPContSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPContSchema.setMakeDate(currDate);
        tLPContSchema.setMakeTime(currTime);
        tLPContSchema.setModifyDate(currDate);
        tLPContSchema.setModifyTime(currTime);
        mLPContSet.add(tLPContSchema);

        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        ref.transFields(tLPEdorMainSchema, mLPGrpEdorItemSchema);
        tLPEdorMainSchema.setContNo(tLCContSchema.getContNo());
        tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
        tLPEdorMainSchema.setUWState("0");
        tLPEdorMainSchema.setEdorState("1");
        tLPEdorMainSchema.setMakeDate(currDate);
        tLPEdorMainSchema.setMakeTime(currTime);
        tLPEdorMainSchema.setModifyDate(currDate);
        tLPEdorMainSchema.setModifyTime(currTime);
        mLPEdorMainSet.add(tLPEdorMainSchema);

        return true;
    }

    private boolean deleteOldLPEdorInfo()
    {
        map.put("  delete from LPCont "
                + "where edorNo = '" + mEdorNo + "' "
                + "  and edorType = '" + mEdorType + "' "
                + "  and grpContNo = '" + mGrpContNo + "' ", "DELETE");
        map.put("  delete from LPEdorMain "
                + "where edorNo = '" + mEdorNo + "' "
                + "  and contNo in ("
                + "      select distinct contNo "
                + "      from LCCont "
                + "      where grpContNo = '" + mGrpContNo + "') ", "DELETE");
        map.put("  delete from LPEdorItem "
                + "where edorNo = '" + mEdorNo + "' "
                + "  and edorType = '" + mEdorType + "' "
                + "  and grpContNo = '" + mGrpContNo + "' ", "DELETE");

        return true;
    }

    private String getMaxSerialNo()
    {
        //得到本工单的导入被保人表的最大序列号
        StringBuffer sql = new StringBuffer();
        sql.append("select max(SerialNo) ")
            .append("from LPDiskImport ")
            .append("where edorNo = '")
            .append(mLPGrpEdorItemSchema.getEdorNo())
            .append("'  and edorType = '")
            .append(mLPGrpEdorItemSchema.getEdorType())
            .append("'  and grpContNo = '")
            .append(mLPGrpEdorItemSchema.getGrpContNo())
            .append("' ");
        ExeSQL e = new ExeSQL();
        String maxSerialNo = e.getOneValue(sql.toString());
        if(e.mErrors.needDealError())
        {
            mErrors.copyAllErrors(e.mErrors);
            return null;
        }
        if (maxSerialNo.equals("") || maxSerialNo.equals("null"))
        {
            maxSerialNo = "0";
        }
        return maxSerialNo;
    }

    /**
     * 若团险下的个险全退保,则公共帐户退保
     * @param contPlanCodes String
     * @param riskCodes String
     * @return boolean
     */
    private boolean dealPublicAccPol(String riskCodes)
    {
        riskCodes = riskCodes.replaceAll("'", "");
        String[] allRiskCode = riskCodes.split(",");
        for(int i = 0; i < allRiskCode.length; i++)
        {
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setGrpContNo(mGrpContNo);
            tLCPolDB.setRiskCode(allRiskCode[i]);
            LCPolSet set = tLCPolDB.query();
            int tLCPolCount = set.size();  //险种allRiskCode[i]下的个险数

            tLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
            LCPolSet publicAccPolSet = tLCPolDB.query();
            if(publicAccPolSet.size() == 0) //险种没有公共帐户
            {
                continue;
            }
            //公共帐户保单号
            String publicAccContNo = publicAccPolSet.get(1).getContNo();

            //选择了的险种
            String polNos = getOneFieldValues(
                "LCPol", "polNo", "and riskCode = '" + allRiskCode[i] + "' ");
            polNos = polNos.replaceAll("'", "");

            //帐户退保，团单下险种数 = 被保人险种数 + 公共帐户数(1)
            if (tLCPolCount == polNos.split(",").length + 1)
            {
                map.put("delete from LPPol "
                        + "where edorNo = '" + mEdorNo
                        + "'  and edortype = '" + mEdorType
                        + "'  and contNo = '" + publicAccContNo + "' ", "DELETE");
                StringBuffer tLPInsuredSql = new StringBuffer();
                tLPInsuredSql.append("insert into LPPol (")
                    .append("select '").append(mEdorNo).append("', ")
                    .append("'").append(mEdorType).append("', ")
                    .append("a.* ")
                    .append("from LCPol a ")
                    .append("where contNo = '").append(publicAccContNo)
                    .append("') ");
                System.out.println(tLPInsuredSql.toString());
                map.put(tLPInsuredSql.toString(), "INSERT");

                map.put("delete from LPInsured "
                        + "where edorNo = '" + mEdorNo
                        + "'  and edortype = '" + mEdorType
                        + "'  and contNo = '" + publicAccContNo + "' ", "DELETE");
                tLPInsuredSql = new StringBuffer();
                tLPInsuredSql.append("insert into LPInsured (")
                    .append("select '").append(mEdorNo).append("', ")
                    .append("'").append(mEdorType).append("', ")
                    .append("a.* ")
                    .append("from LCInsured a ")
                    .append("where contNo = '").append(publicAccContNo)
                    .append("') ");
                System.out.println(tLPInsuredSql.toString());
                map.put(tLPInsuredSql.toString(), "INSERT");

                LCContDB tLCContDB = new LCContDB();
                tLCContDB.setContNo(publicAccContNo);
                tLCContDB.getInfo();
                if(!createOneLPEdorInfo(tLCContDB.getSchema()))
                {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean changeCommonInfo()
    {
        String[] tables = {"LPGrpCont", "LPGrpPol", "LPInsured", "LPPol"};
        for (int i = 0; i < tables.length; i++)
        {
            map.put("  update " + tables[i]
                    + " set operator = '" + mGlobalInput.Operator + "', "
                    + "   makeDate = '" + currDate + "', "
                    + "   makeTime = '" + currTime + "', "
                    + "   modifyDate = '" + currDate + "', "
                    + "   modifyTime = '" + currTime + "' "
                    + "where edorNo = '" + mEdorNo + "' "
                    + "   and edorType = '" + mEdorType + "' "
                    + "   and grpContNo = '" + mGrpContNo + "' ", "UPDATE");
        }

        return true;
    }

    public static void main(String args[])
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "edor0";
        tG.ComCode = "86";

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorNo("20060316000013");
        tLPGrpEdorItemSchema.setEdorAcceptNo(tLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemSchema.setEdorType(BQ.EDORTYPE_CT);
        tLPGrpEdorItemSchema.setGrpContNo("0000025901");

        LPContPlanRiskSet set = new LPContPlanRiskSet();

        LPContPlanRiskSchema schema = new LPContPlanRiskSchema();
        schema.setEdorNo(tLPGrpEdorItemSchema.getEdorNo());
        schema.setEdorType(tLPGrpEdorItemSchema.getEdorType());
        schema.setProposalGrpContNo("1400000800");
        schema.setContPlanCode("B");
        schema.setMainRiskCode("1605");
        schema.setRiskCode("1605");
        schema.setPlanType("0");
        set.add(schema);
/*
        LPContPlanRiskSchema schema2 = new LPContPlanRiskSchema();
        schema2.setEdorNo(tLPGrpEdorItemSchema.getEdorNo());
        schema2.setEdorType(tLPGrpEdorItemSchema.getEdorType());
        schema2.setProposalGrpContNo("1400000309");
        schema2.setContPlanCode("B");
        schema2.setMainRiskCode("1602");
        schema2.setRiskCode("1602");
        schema2.setPlanType("0");
        set.add(schema2);
*/


        VData tVData = new VData();
        tVData.addElement(tG);
        tVData.addElement(tLPGrpEdorItemSchema);
        tVData.add(set);

        PGrpEdorCTDetailBL tPGrpEdorCTDetailBL = new PGrpEdorCTDetailBL();
        if (!tPGrpEdorCTDetailBL.submitData(tVData, ""))
        {
            System.out.println(tPGrpEdorCTDetailBL.mErrors.getErrContent());
        }
    }
}
