package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;


/**
 * <p>Title: 保全变更投保人项目</p>
 * <p>Description: 录入保全明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorUWManuHealthQUI
{
    private PEdorUWManuHealthQBL mPEdorUWManuHealthQBL = null;

    /**
     * 构造函数
     */
    public PEdorUWManuHealthQUI()
    {
        mPEdorUWManuHealthQBL = new PEdorUWManuHealthQBL();
    }

    /**
     * 调用业务逻辑
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!mPEdorUWManuHealthQBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mPEdorUWManuHealthQBL.mErrors.getFirstError();
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args)
    {

    }
}
