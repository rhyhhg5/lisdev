package com.sinosoft.lis.bq;

import java.util.HashMap;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMRiskBonusDB;
import com.sinosoft.lis.db.LMRiskBonusRateDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.AccountManage;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.DealAccount;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCInsureAccTraceSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJABonusGetSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LMRiskInsuAccSchema;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.schema.LOBonusPolSchema;
import com.sinosoft.lis.vschema.LCGetToAccSet;
import com.sinosoft.lis.vschema.LCInsureAccClassFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccClassSet;
import com.sinosoft.lis.vschema.LCInsureAccFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccFeeTraceSet;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.lis.vschema.LCInsureAccTraceSet;
import com.sinosoft.lis.vschema.LCPremToAccSet;
import com.sinosoft.lis.vschema.LMRiskBonusRateSet;
import com.sinosoft.lis.vschema.LMRiskBonusSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 分红险分红处理 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company: sinosoft</p>
 * @author XP
 * @version 1.0
 * @CreateDate：2012-08-02
 */

public class BonusGetBL {
	
	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	private GlobalInput mGI = new GlobalInput();

	public CErrors mErrors = new CErrors();

	private LCContSchema mLCContSchema = new LCContSchema();
	
	private LCPolSchema mLCPolSchema = new LCPolSchema();

	private String mPolNo = "";// 处理红利险种号
	
	private String mDrawerName = "";// 红利领取人

	private String mDrawerID = "";// 红利领取人证件号码

	private MMap mMMap = new MMap();
	
	public int COUNT = 100;
	
//	本次给付红利
	public double mBonus= 0;
	
//	现金红利迟发利息
	public double mBonusInterest= 0;

	private AccountManage tAccountManage = new AccountManage();
	
	private String HLDate="";
	
	private String TBCalCode="";
	
	private LMRiskInsuAccSchema mLMRiskInsuAccSchema;
	
	private int mFisCalYear;
	
//	险种红利算法缓存容器
	private HashMap tHashMap = new HashMap();
	//满期红利算法缓存器
	private HashMap MQHashMap = new HashMap();
	
	private ExeSQL mExeSQL=new ExeSQL();
	
    /**
     * submitData
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
//    	获取前台传入的数据
        if (!getInputData(cInputData))
        {
            return false;
        }
        
//    	校验数据
        if (!checkData())
        {
            return false;
        }        
        
//    	执行业务处理
        if (!dealData())
        {
            return false;
        }
        
//    	执行数据提交
        if (!SubmitData())
        {
            return false;
        }

        return true;
    }
    
    /**
     * 校验数据
     *
     */
    private boolean checkData() {
    	if(mLCPolSchema.getBonusGetMode()==null){
//    		 @@错误处理
			System.out.println("BonusGetBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "checkData";
			tError.errorMessage = "红利领取方式错误!";
			mErrors.addOneError(tError);
			return false;
    	}
    	if(!(mLCPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETMONEY)||mLCPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETINTEREST))){
    		// @@错误处理
			System.out.println("BonusGetBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "checkData";
			tError.errorMessage = "红利领取方式错误!";
			mErrors.addOneError(tError);
			return false;
    	}
    	
        LPEdorItemSet set = CommonBL.getEdorItemMayChangePrem(mLCPolSchema.getContNo());
            if(set.size() > 0)
            {
                String edorInfo = "";
                for(int i = 1; i <= set.size(); i++)
                {
                    edorInfo += set.get(i).getEdorNo() + " "
                        + set.get(i).getEdorType() + ", ";
                }
                mErrors.addOneError("保单有未结案的保全项目:" + edorInfo);
                return false;
            }
    	
        if(!LLCaseCommon.checkClaimState(mLCPolSchema.getPolNo()))
        {
        	// @@错误处理
			System.out.println("BonusGetBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "checkData";
			tError.errorMessage = "险种有未结案的理赔，不能进行结算!";
			mErrors.addOneError(tError);
			return false;
        }
        return true;
    }
    
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
        mGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mPolNo = (String) mInputData.get(1); //分红险种号
        String tYear= (String) mInputData.get(2); //分红保单年度

        if ((mGI == null) || (mPolNo == null) ) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(tYear== null||tYear.trim().equals("null")||tYear.trim().equals("")){
        	// @@错误处理
			System.out.println("BonusGetBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收结算年度失败!";
			mErrors.addOneError(tError);
			return false;
        }
        mFisCalYear = Integer.parseInt(tYear);

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mPolNo);
        if (!tLCPolDB.getInfo()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCPolSchema.setSchema(tLCPolDB.getSchema());
        
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLCPolSchema.getContNo());
        if (tLCContDB.getInfo() == false) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCContSchema.setSchema(tLCContDB.getSchema());
        return true;
    }

    
    private boolean SubmitData(){
        VData data = new VData();
        data.clear();
        data.add(mMMap);
        if (data == null) {
            // @@错误处理
			System.out.println("BonusGetBL+enclosing_method++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "enclosing_method";
			tError.errorMessage = "提交数据为空";
			mErrors.addOneError(tError);
			return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) {
            this.mErrors.addOneError("PubSubmit:生成数据失败!");
            return false;
        }
        return true;
    }
    
    
    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
    	System.out.println("进入---------------BonusGetBL.java---------------");
/*    	校验是否存在累积生息帐户,如果不存在则生成.
    	这里会有一个数据库提交,提交失败则返回false
   */
    	MMap tCekMap = null;
    	tCekMap = lockBalaCount(mLCPolSchema.getPolNo()+String.valueOf(mFisCalYear));
        if (tCekMap == null)
        {
            // @@错误处理
			System.out.println("BonusGetBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "dealData";
			tError.errorMessage = "并发处理失败!";
			mErrors.addOneError(tError);
			return false;
        }
        mMMap.add(tCekMap);
    	
//      累积生息第一次进系统时处理帐户初始化
        if(mLCPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETINTEREST)){
	        if (!HaveAcc())
	        {
	        	// @@错误处理
				System.out.println("BonusGetBL+dealData++--");
				CError tError = new CError();
				tError.moduleName = "BonusGetBL";
				tError.functionName = "dealData";
				tError.errorMessage = "生成帐户失败!";
				mErrors.addOneError(tError);
				return false;
	        }
        }
//      分配对应的红利
        if(!GetBonus()){
        	// @@错误处理
			System.out.println("BonusGetBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "dealData";
			tError.errorMessage = "分配红利失败!";
			mErrors.addOneError(tError);
			return false;
        }
        if(mBonus>0){
	        if(mLCPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETINTEREST)){
	//			处理累积生息业务流程
	        	if(!setAcc()){
	        		return false;
	        	}
	        }else if(mLCPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETMONEY)){
	//        	处理现金领取业务流程
	        	setLJA();        	
	        }else{
	        	// @@错误处理
				System.out.println("BonusGetBL+dealData++--");
				CError tError = new CError();
				tError.moduleName = "BonusGetBL";
				tError.functionName = "dealData";
				tError.errorMessage = "领取方式处理失败!";
				mErrors.addOneError(tError);
				return false;
	        }
        }
        return true;
    }
 
    /**
     * 处理红利分配
     * @return boolean
     */
    private boolean GetBonus()
    {
    	boolean IsAbate=false;
    	boolean IsFX=false;
//    	获取分红对应的日期
    	if(!getHLDate()){
    		// @@错误处理
			System.out.println("BonusGetBL+GetBonus++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "GetBonus";
			tError.errorMessage = "获取分红日期失败";
			mErrors.addOneError(tError);
			return false;
    	}
    	String abate=mExeSQL.getOneValue("select 1 from lccontstate where polno='"+mLCPolSchema.getPolNo()+"' and statetype='Available' and startdate<='"+HLDate+"' and enddate>'"+HLDate+"' " +
    			" union select 1 from lccontstate where polno='"+mLCPolSchema.getPolNo()+"' and statetype='Available' and startdate<='"+HLDate+"' and enddate is null ");
    	if(abate!=null&&abate.equals("1")){
    		IsAbate=true;
    	}
    	String FXDate=mExeSQL.getOneValue("select max(enddate) from lccontstate where polno='"+mLCPolSchema.getPolNo()+"' and statetype='Available' and state='0' " +
    			" and enddate is not null and  enddate between (date('"+HLDate+"') - 1 year) and '"+HLDate+"' with ur");
    	if(FXDate!=null&&(!FXDate.equals(""))){
    		IsFX=true;
    	}
    	LMRiskBonusDB tLMRiskBonusDB=new LMRiskBonusDB();
    	tLMRiskBonusDB.setRiskCode(mLCPolSchema.getRiskCode());
    	LMRiskBonusSet tLMRiskBonusSet=tLMRiskBonusDB.query();
    	if(tLMRiskBonusSet.size()!=1){
    		// @@错误处理
			System.out.println("BonusGetBL+GetBonus++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "GetBonus";
			tError.errorMessage = "获取分红公式失败";
			mErrors.addOneError(tError);
			return false;
    	}
    	String tCvaliYear=mExeSQL.getOneValue("select year('"+mLCPolSchema.getCValiDate()+"') from dual with ur");
    	if(tCvaliYear==null||tCvaliYear.equals("")||tCvaliYear.equals("null")){
    		// @@错误处理
			System.out.println("BonusGetBL+GetBonus++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "GetBonus";
			tError.errorMessage = "获取生效日期失败";
			mErrors.addOneError(tError);
			return false;
    	}
//    	LMRiskBonusRateDB tLMRiskBonusRateDB =new LMRiskBonusRateDB();
//    	LMRiskBonusRateSet tLMRiskBonusRateSet=new LMRiskBonusRateSet();
//    	String tratesql="select * from LMRiskBonusRate where riskcode='"+mLCPolSchema.getRiskCode()+"' and bonusyear='"+mFisCalYear+"' " +
//		" and appyear='"+tCvaliYear+"' and insuyears="+mLCPolSchema.getInsuYear()+" and payyears="+mLCPolSchema.getPayEndYear()+" and minage<="+mLCPolSchema.getInsuredAppAge()+" and maxage>="+mLCPolSchema.getInsuredAppAge()+" with ur";
//    	System.out.println("tratesql:"+tratesql);
//    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
//    	if(tLMRiskBonusRateSet.size()!=1){
//    		// @@错误处理
//			System.out.println("BonusGetBL+GetBonus++--");
//			CError tError = new CError();
//			tError.moduleName = "BonusGetBL";
//			tError.functionName = "GetBonus";
//			tError.errorMessage = "获取分红率失败";
//			mErrors.addOneError(tError);
//			return false;
//    	}    	
//    	TODO :2月29的情况
    	Calculator tCalculator = new Calculator();
        tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
        tCalculator.addBasicFactor("Get", String.valueOf(mLCPolSchema.getAmnt()));
        tCalculator.addBasicFactor("Prem",String.valueOf(mLCPolSchema.getPrem()));
        tCalculator.addBasicFactor("payendyear",String.valueOf(mLCPolSchema.getPayEndYear()));
        tCalculator.addBasicFactor("payintv",String.valueOf(mLCPolSchema.getPayIntv()));
        tCalculator.addBasicFactor("insuyear",String.valueOf(mLCPolSchema.getInsuYear()));
        tCalculator.addBasicFactor("sex",mLCPolSchema.getInsuredSex());
        tCalculator.addBasicFactor("appyear",tCvaliYear);
        tCalculator.addBasicFactor("insuredage",String.valueOf(mLCPolSchema.getInsuredAppAge()));
        tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(mLCPolSchema.getCValiDate(),HLDate, "Y")-1));
//        tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
        tCalculator.addBasicFactor("bonusyear",String.valueOf(mFisCalYear));
        if(FXDate.equals("")){
        	tCalculator.addBasicFactor("startdate",HLDate);
        }else{
        	tCalculator.addBasicFactor("startdate",FXDate);
        }
        tCalculator.addBasicFactor("enddate",HLDate);
        
//    	失效的情况
    	if(IsAbate){
    		mBonus=0;
    	}
//    	复效的情况
    	else if(IsFX){
            tCalculator.addBasicFactor("caltype","FX");
    		mBonus= CommonBL.carry(tCalculator.calculate());
    	}
//    	正常情况下的红利分配
    	else{
            tCalculator.addBasicFactor("caltype","YX");
    		mBonus= CommonBL.carry(tCalculator.calculate());
    		if(mBonus==0){
    			// @@错误处理
				System.out.println("BonusGetBL+GetBonus++--");
				CError tError = new CError();
				tError.moduleName = "BonusGetBL";
				tError.functionName = "GetBonus";
				tError.errorMessage = "红利计算失败";
				mErrors.addOneError(tError);
				return false;
    		}
    	}
//    	add by xp #1243 分红险迟发利息 20130717
//    	只有在当前时间大于应给付红利时间时,才进行计算
    	if(mLCPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETMONEY)&&PubFun.getBeforeDate(HLDate, CurrentDate).equals(HLDate)&&(!PubFun.getLaterDate(HLDate, CurrentDate).equals(HLDate))){
    		tCalculator = new Calculator();
//    		这个字段存储迟发利息计算公式
            tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusCoefCode());
            String rate=GetRate(CurrentDate);
            if(rate==null||rate.equals("")||rate.equals("null")){
            	// @@错误处理
				System.out.println("BonusGetBL+GetBonus++--");
				CError tError = new CError();
				tError.moduleName = "BonusGetBL";
				tError.functionName = "GetBonus";
				tError.errorMessage = "获取迟发利息失败!";
				mErrors.addOneError(tError);
				return false;
            }
            tCalculator.addBasicFactor("InterestRate",rate);
            tCalculator.addBasicFactor("Bonus",String.valueOf(mBonus));
            tCalculator.addBasicFactor("Sgetdate",HLDate);
            tCalculator.addBasicFactor("Agetdate",CurrentDate);
    		mBonusInterest=CommonBL.carry(tCalculator.calculate());
    		if(mBonusInterest==0){
    			// @@错误处理
				System.out.println("BonusGetBL+GetBonus++--");
				CError tError = new CError();
				tError.moduleName = "BonusGetBL";
				tError.functionName = "GetBonus";
				tError.errorMessage = "现金红利迟发利息计算失败!";
				mErrors.addOneError(tError);
				return false;
    		}
    	}
    	mMMap.put(CreateBonusPol(HLDate), SysConst.INSERT);
    	return true;
    }
    
//  返回该日期对应的利率
  public String GetRate(String tdate){
  	String tPolYear=String.valueOf(PubFun.calInterval(mLCPolSchema.getCValiDate(),tdate, "Y"));
  	String trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+mPolNo+"' " +
  				" and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+"-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+")))) ");
  	return trate;
  }
    
    private boolean getHLDate(){
    	/*SimpleDateFormat simple=new SimpleDateFormat("yyyy-MM-dd");
    	int tMonth=0;
    	try{
    		Date date =simple.parse(mLCPolSchema.getCValiDate());
    		tMonth=date.getMonth();
    	}
    	catch (Exception e){
    		// @@错误处理
			System.out.println("BonusGetBL+getHLDate++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "getHLDate";
			tError.errorMessage = "获取日期失败";
			mErrors.addOneError(tError);
			return false;
    	}
    	if(tMonth<=6&&tMonth>=1){
    		HLDate=String.valueOf(mFisCalYear+1)+mLCPolSchema.getCValiDate().substring(5);
    	}else if(tMonth<=12&&tMonth>=7){
    		HLDate=String.valueOf(mFisCalYear)+mLCPolSchema.getCValiDate().substring(5);
    	}else{
    		// @@错误处理
			System.out.println("BonusGetBL+getHLDate++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "getHLDate";
			tError.errorMessage = "获取日期失败";
			mErrors.addOneError(tError);
			return false;
    	}*/
    	StringBuffer tSQL=new StringBuffer();
    	tSQL.append("select case when month('").append(mLCPolSchema.getCValiDate()).append("')<=6 then ")
    	.append("(date('").append(mLCPolSchema.getCValiDate()).append("')+(").append(mFisCalYear)
    	.append("-year(date('").append(mLCPolSchema.getCValiDate()).append("'))+1) year) ")
    	.append("else (date('").append(mLCPolSchema.getCValiDate()).append("')+(").append(mFisCalYear)
    	.append("-year(date('").append(mLCPolSchema.getCValiDate()).append("'))) year) end from dual");
    	SSRS tSSRS=mExeSQL.execSQL(tSQL.toString());
    	if(tSSRS.MaxRow!=1){
    		// @@错误处理
			System.out.println("BonusGetBL+getHLDate++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "getHLDate";
			tError.errorMessage = "查询红利日期失败";
			mErrors.addOneError(tError);
			return false;
    	}
    	HLDate=tSSRS.GetText(1, 1);
    	if(HLDate==null||HLDate.equals("")||HLDate.equals("null")){
    		// @@错误处理
			System.out.println("BonusGetBL+getHLDate++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "getHLDate";
			tError.errorMessage = "获取红利日期失败";
			mErrors.addOneError(tError);
			return false;
    	}
    	return true;
    }
    
	private LOBonusPolSchema CreateBonusPol(String HLPiont){
		LOBonusPolSchema tLOBonusPolSchema=new LOBonusPolSchema();
		tLOBonusPolSchema.setContNo(mLCPolSchema.getContNo());
		tLOBonusPolSchema.setPolNo(mLCPolSchema.getPolNo());
		tLOBonusPolSchema.setFiscalYear(mFisCalYear);
		tLOBonusPolSchema.setGroupID(1);
		tLOBonusPolSchema.setAGetDate(CurrentDate);
		tLOBonusPolSchema.setSGetDate(HLPiont);
//		这里BonusFlag存领取方式
		tLOBonusPolSchema.setBonusFlag(mLCPolSchema.getBonusGetMode());
		tLOBonusPolSchema.setBonusMakeDate(CurrentDate);
		tLOBonusPolSchema.setBonusMoney(mBonus);
		tLOBonusPolSchema.setBonusInterest(mBonusInterest);
		tLOBonusPolSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
		tLOBonusPolSchema.setMakeDate(CurrentDate);
		tLOBonusPolSchema.setMakeTime(CurrentTime);
		tLOBonusPolSchema.setModifyDate(CurrentDate);
		tLOBonusPolSchema.setModifyTime(CurrentTime);
		tLOBonusPolSchema.setOperator(mGI.Operator);
		return tLOBonusPolSchema;
	}
    
//	设置累积生息的帐户数据
	private boolean setAcc(){
        //处理帐户主表
        LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
        LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
        
        tLCInsureAccSchema = CommonBL.getLCInsureAcc(mLCPolSchema.getPolNo(),
        		CommonBL.getInsuAccNo(BQ.ACCTYPE_BONUS,mLCPolSchema.getRiskCode()));
        if(tLCInsureAccSchema==null){
        	// @@错误处理
			System.out.println("BonusGetBL+setAcc++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "setAcc";
			tError.errorMessage = "查询帐户表失败";
			mErrors.addOneError(tError);
			return false;
        }
        //处理帐户分类表
        LCInsureAccClassSet tLCInsureAccClassSet = new
                LCInsureAccClassSet();
        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setPolNo(mLCPolSchema.getPolNo());
        tLCInsureAccClassDB.setAccType(BQ.ACCTYPE_BONUS);
        tLCInsureAccClassSet = tLCInsureAccClassDB.query();
        if (tLCInsureAccClassSet.size() != 1)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorZBAppConfirmBL";
            tError.functionName = "setInsuredAcc";
            tError.errorMessage = "查询帐户分类表数据失败!";
            mErrors.addOneError(tError);
            return false;
        }
        tLCInsureAccClassSchema=tLCInsureAccClassSet.get(1);
        
//        设置账户余额
        tLCInsureAccSchema.setInsuAccBala(tLCInsureAccSchema.getInsuAccBala() + mBonus);
        mMMap.put(tLCInsureAccSchema, SysConst.UPDATE);
        tLCInsureAccClassSchema.setInsuAccBala(tLCInsureAccClassSchema.getInsuAccBala() + mBonus);
		mMMap.put(tLCInsureAccClassSchema, SysConst.UPDATE);
        setLCInsureAccTrace(tLCInsureAccClassSchema);
		return true;
	}
	
    /**
     * 设置帐户红利轨迹
     * @param aLCInsureAccClassSchema LCInsureAccClassSchema
     */
    private void setLCInsureAccTrace(LCInsureAccClassSchema aLCInsureAccClassSchema)
    {
        String serialNo = PubFun1.CreateMaxNo("SERIALNO",
                                       aLCInsureAccClassSchema.getManageCom());

        LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
        tLCInsureAccTraceSchema.setGrpContNo(aLCInsureAccClassSchema.
                                             getGrpContNo());
        tLCInsureAccTraceSchema.setGrpPolNo(aLCInsureAccClassSchema.getGrpPolNo());
        tLCInsureAccTraceSchema.setContNo(aLCInsureAccClassSchema.getContNo());
        tLCInsureAccTraceSchema.setPolNo(aLCInsureAccClassSchema.getPolNo());
        tLCInsureAccTraceSchema.setSerialNo(serialNo);
        tLCInsureAccTraceSchema.setInsuAccNo(aLCInsureAccClassSchema.
                                             getInsuAccNo());
        tLCInsureAccTraceSchema.setRiskCode(aLCInsureAccClassSchema.getRiskCode());
        tLCInsureAccTraceSchema.setPayPlanCode(aLCInsureAccClassSchema.
                                               getPayPlanCode());
        tLCInsureAccTraceSchema.setManageCom(aLCInsureAccClassSchema.
                                             getManageCom());
        tLCInsureAccTraceSchema.setAccAscription("0");
        tLCInsureAccTraceSchema.setMoneyType("HL");
        tLCInsureAccTraceSchema.setMoney(mBonus);
        tLCInsureAccTraceSchema.setUnitCount("0");
        tLCInsureAccTraceSchema.setPayDate(HLDate);
        tLCInsureAccTraceSchema.setState("0");
        tLCInsureAccTraceSchema.setOtherNo(Integer.toString(mFisCalYear));
        tLCInsureAccTraceSchema.setOtherType("10");
        tLCInsureAccTraceSchema.setOperator(mGI.Operator);
        tLCInsureAccTraceSchema.setMakeDate(CurrentDate);
        tLCInsureAccTraceSchema.setMakeTime(CurrentTime);
        tLCInsureAccTraceSchema.setModifyDate(CurrentDate);
        tLCInsureAccTraceSchema.setModifyTime(CurrentTime);
        mMMap.put(tLCInsureAccTraceSchema, SysConst.INSERT);
    }
	
//	设置现金红利的实付数据
	private void setLJA(){
		String tLimit = PubFun.getNoLimit(mLCPolSchema.getManageCom());
		String tSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);// 产生流水号码
		
		String tActuGetNo=PubFun1.CreateMaxNo("GETNO", null);
		LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema(); 
		tLJABonusGetSchema.setActuGetNo(tActuGetNo);
		tLJABonusGetSchema.setOtherNo(mLCPolSchema.getContNo());
		tLJABonusGetSchema.setOtherNoType("7");// 同上
		tLJABonusGetSchema.setPayMode(mLCContSchema.getPayMode());
		tLJABonusGetSchema.setBonusYear(String.valueOf(mFisCalYear));
		tLJABonusGetSchema.setGetMoney(mBonus);
		tLJABonusGetSchema.setGetDate(CurrentDate);
		tLJABonusGetSchema.setManageCom(mLCPolSchema.getManageCom());
		tLJABonusGetSchema.setAgentCom(mLCPolSchema.getAgentCom());
		tLJABonusGetSchema.setAgentType(mLCPolSchema.getAgentType());
		tLJABonusGetSchema.setAPPntName(mLCPolSchema.getAppntName());
		tLJABonusGetSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
		tLJABonusGetSchema.setAgentCode(mLCPolSchema.getAgentCode());
		tLJABonusGetSchema.setFeeFinaType("TF"); // 红利退费
		tLJABonusGetSchema.setFeeOperationType("HL");
		tLJABonusGetSchema.setState("0");
		tLJABonusGetSchema.setSerialNo(tSerialNo);
		tLJABonusGetSchema.setOperator(mGI.Operator);
		tLJABonusGetSchema.setMakeDate(CurrentDate);
		tLJABonusGetSchema.setMakeTime(CurrentTime);
		tLJABonusGetSchema.setModifyDate(CurrentDate);
		tLJABonusGetSchema.setModifyTime(CurrentTime);
		tLJABonusGetSchema.setGetNoticeNo(tActuGetNo);
		
//    	add by xp #1243 分红险迟发利息 20130717
		if(mBonusInterest>0){
			LJABonusGetSchema tLJABonusGetSchemaInterest = new LJABonusGetSchema(); 
			tLJABonusGetSchemaInterest.setActuGetNo(tActuGetNo);
			tLJABonusGetSchemaInterest.setOtherNo(mLCPolSchema.getContNo());
			tLJABonusGetSchemaInterest.setOtherNoType("7");// 同上
//			联合主键只有paymode这个可用了...太尴尬了.
//			用来区分该保单当年的红利和迟发利息.
			tLJABonusGetSchemaInterest.setPayMode("LX");
			tLJABonusGetSchemaInterest.setBonusYear(String.valueOf(mFisCalYear));
			tLJABonusGetSchemaInterest.setGetMoney(mBonusInterest);
			tLJABonusGetSchemaInterest.setGetDate(CurrentDate);
			tLJABonusGetSchemaInterest.setManageCom(mLCPolSchema.getManageCom());
			tLJABonusGetSchemaInterest.setAgentCom(mLCPolSchema.getAgentCom());
			tLJABonusGetSchemaInterest.setAgentType(mLCPolSchema.getAgentType());
			tLJABonusGetSchemaInterest.setAPPntName(mLCPolSchema.getAppntName());
			tLJABonusGetSchemaInterest.setAgentGroup(mLCPolSchema.getAgentGroup());
			tLJABonusGetSchemaInterest.setAgentCode(mLCPolSchema.getAgentCode());
			tLJABonusGetSchemaInterest.setFeeFinaType("TF"); // 红利退费
//			跟财务接口约定,迟发利息设置此字段为HLLX
			tLJABonusGetSchemaInterest.setFeeOperationType("HLLX");
			tLJABonusGetSchemaInterest.setState("0");
			tLJABonusGetSchemaInterest.setSerialNo(tSerialNo);
			tLJABonusGetSchemaInterest.setOperator(mGI.Operator);
			tLJABonusGetSchemaInterest.setMakeDate(CurrentDate);
			tLJABonusGetSchemaInterest.setMakeTime(CurrentTime);
			tLJABonusGetSchemaInterest.setModifyDate(CurrentDate);
			tLJABonusGetSchemaInterest.setModifyTime(CurrentTime);
			tLJABonusGetSchemaInterest.setGetNoticeNo(tActuGetNo);
			mMMap.put(tLJABonusGetSchemaInterest, SysConst.INSERT);
		}

		// 实付数据-总表

		LJAGetSchema tLJAGetSchema = new LJAGetSchema();
		tLJAGetSchema.setActuGetNo(tActuGetNo);
		tLJAGetSchema.setOtherNo(mLCPolSchema.getContNo()); // 保单险种号码
		tLJAGetSchema.setOtherNoType("7"); // 7代表正常分红则OtherNo存放保单险种号码
		tLJAGetSchema.setPayMode("4");// 现金
		tLJAGetSchema.setAppntNo(mLCPolSchema.getAppntNo());
//    	add by xp #1243 分红险迟发利息 20130717
		if(mBonusInterest>0){
			tLJAGetSchema.setSumGetMoney(CommonBL.carry(mBonus+mBonusInterest));
		}else{
			tLJAGetSchema.setSumGetMoney(mBonus);
		}
		tLJAGetSchema.setSaleChnl(mLCPolSchema.getSaleChnl());
		tLJAGetSchema.setShouldDate(CurrentDate);
		tLJAGetSchema.setApproveCode(mLCPolSchema.getApproveCode());
		tLJAGetSchema.setApproveDate(mLCPolSchema.getApproveDate());
		tLJAGetSchema.setGetNoticeNo(tActuGetNo);
		tLJAGetSchema.setBankAccNo(mLCContSchema.getBankAccNo());
		tLJAGetSchema.setBankCode(mLCContSchema.getBankCode());
		tLJAGetSchema.setAccName(mLCContSchema.getAccName());
		tLJAGetSchema.setDrawer(mLCContSchema.getAppntName());
		tLJAGetSchema.setDrawerID(mLCContSchema.getAppntIDNo());
		tLJAGetSchema.setSerialNo(tSerialNo);
		tLJAGetSchema.setOperator(mLCPolSchema.getOperator());
		tLJAGetSchema.setMakeDate(CurrentDate);
		tLJAGetSchema.setMakeTime(CurrentTime);
		tLJAGetSchema.setModifyDate(CurrentDate);
		tLJAGetSchema.setModifyTime(CurrentTime);
		tLJAGetSchema.setManageCom(mLCPolSchema.getManageCom());
		tLJAGetSchema.setAgentCom(mLCPolSchema.getAgentCom());
		tLJAGetSchema.setAgentType(mLCPolSchema.getAgentType());
		tLJAGetSchema.setAgentCode(mLCPolSchema.getAgentCode());
		tLJAGetSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
		if(!mLCContSchema.getPayMode().equals("4")){
			tLJAGetSchema.setCanSendBank("1");
		}
		mMMap.put(tLJABonusGetSchema, SysConst.INSERT);
		mMMap.put(tLJAGetSchema, SysConst.INSERT);
		
	}
	
    /**
     * 锁定动作
     * @param polBalaCount
     * @return MMap
     */
    private MMap lockBalaCount(String polBonus)
    {
        MMap tMMap = null;
        /**万能险月结算"UJ"*/
        String tLockNoType = "HL";
        /**锁定有效时间（秒）*/
        String tAIS = "36000";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", polBonus);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGI);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }
	
    /**
     * 处理帐户初步生成
     * !!!!!!这里有分步提交数据库!!!!!!
     * @return boolean
     */
    private boolean HaveAcc()
    {
    	String Haveacc = mExeSQL.getOneValue("select 1 from lcinsureacc where polno='"+mPolNo+"' fetch first 1 row only with ur");
    	if(Haveacc!=null&&Haveacc.equals("1")){
    		System.out.println("已经生成帐户了!");
    		return true;
    	}
    	
    	LMRiskSchema tLMRiskSchema=new LMRiskSchema();
    	LMRiskDB tLMRiskDB=new LMRiskDB();
    	tLMRiskDB.setRiskCode(mLCPolSchema.getRiskCode());
    	if(!tLMRiskDB.getInfo()){
    		// @@错误处理
			System.out.println("BonusGetBL+HaveAcc++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "HaveAcc";
			tError.errorMessage = "获取险种描述信息失败!";
			mErrors.addOneError(tError);
			return false;
    	}
    	tLMRiskSchema=tLMRiskDB.getSchema();
    	DealAccount tDealAccount = new DealAccount();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("AccCreatePos", "3"); //生成位置：领取时
        tTransferData.setNameAndValue("OtherNoType", "1"); //其它号码类型：个人险种保单号
        tTransferData.setNameAndValue("PolNo", mPolNo);
        tTransferData.setNameAndValue("OtherNo", mPolNo);

        //生成帐户结构
        VData tVData = tDealAccount.createInsureAccHL(tTransferData);

        if (tDealAccount.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tDealAccount.mErrors);
            return false;
        }
        //计算并填充账户结构
        if (tVData == null)
        {
        	// @@错误处理
			System.out.println("BonusGetBL+HaveAcc++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "HaveAcc";
			tError.errorMessage = "创建账户结构失败!";
			mErrors.addOneError(tError);
			return false;
        }
        
//      执行业务数据提交
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(tVData, ""))
        {
        	// @@错误处理
			System.out.println("BonusGetBL+HaveAcc++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "HaveAcc";
			tError.errorMessage = "帐户创建数据提交失败!";
			mErrors.addOneError(tError);
			return false;
        }        
    	return true;    	
    }

}

