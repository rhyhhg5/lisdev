package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 删除新增的险种信息
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PEdorNSDetailDelPolUI
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public PEdorNSDetailDelPolUI()
    {
    }

    /**
     * 数据提交的公共方法，进行新增险种明细录入逻辑处理并提交数据库，
     * 提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象：包括：
     * A.GlobalInput对象，完整的登陆用户信息。
     * B.LPEdorItemSchema对象，包含EdorNo,EdorType,InsuredNo和PolNo
     * @param cOperate 数据操作字符串，可为空""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("---GEdorNSDetail BL BEGIN---");
        PEdorNSDetailDelPolBL tPEdorNSDetailDelPolBL = new PEdorNSDetailDelPolBL();

        if(tPEdorNSDetailDelPolBL.submitData(cInputData, cOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPEdorNSDetailDelPolBL.mErrors);
            return false;
        }
        System.out.println("---GEdorNSDetail BL END---");

        return true;
    }

    public static void main(String[] args)
    {
        PEdorNSDetailDelPolUI pedornsdetaildelpolui = new PEdorNSDetailDelPolUI();
    }
}
