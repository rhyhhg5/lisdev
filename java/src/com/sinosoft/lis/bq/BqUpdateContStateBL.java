//程序名称：BqUpdateContStateBL.java
//程序功能：保单状态维护
//创建日期：20170426
//创建人  ：wxd
//程序功能：保单状态维护
//创建日期：20170426
//创建人  ：wxd
package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BqUpdateContStateBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();
	// 录入
	private String LContNo = null;
	private StringBuffer mPolNo = null;
	//保单可能存在多条失效记录，只维护最新一条，即保单失效终止startdate对应的失效中止的enddate那条
	private String mStartDate = null;
	// private String AfterTempFeeType = "";
	private MMap mMap = new MMap();

	public BqUpdateContStateBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		// 判断所需参数是否为空
		if (!getInputData(cInputData)) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败BqUpdateContStateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start BqUpdateContStateBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "BqUpdateContStateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);

		LContNo = (String) tTransferData.getValueByName("TContNo");

		mPolNo = (StringBuffer) tTransferData.getValueByName("tPolNo");

		if (LContNo == null || "".equals(LContNo)) {
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "保单号为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dealData() {
		// 处理的相对笨重
		// 根据页面传来字符串，分解为数组
		String[] tPolNo = mPolNo.toString().split(",");

		String mSql1 = "";

		StringBuffer sb = new StringBuffer();
		// 再将数组元素拼成字符串写进sql
		for (int i = 0; i < tPolNo.length; i++) {
			System.out.println(tPolNo[i]);
			sb.append("'" + tPolNo[i] + "',");
		}
		mSql1 = "update lcpol set stateflag='2',modifydate='"
			+ CurrentDate + "',modifytime='"
			+ CurrentTime + "' where polno in ("
			+ sb.toString().substring(0, sb.toString().length() - 1)
			+ ") and stateflag='3'";

		// 循环险种更新lccontstate
		for (int i = 0; i < tPolNo.length; i++) {
			// 取失效开始时间
			String mSql3 = " select startdate from lccontstate where polno='"
					+ tPolNo[i]
					+ "' and statetype='Terminate' and state = '1' and statereason = '09'  ";
			mStartDate = new ExeSQL().getOneValue(mSql3);// 执行SQL语句

			if (null == mStartDate || "".equals(mStartDate)) {// 判断开始时间是否为空
				return false;
			}

			String mSql5 = "update lccontstate set enddate=null,state='1',modifydate='"
					+ CurrentDate
					+ "',"
					+ "modifytime='"
					+ CurrentTime
					+ "' where polno='"
					+ tPolNo[i]
					+ "'"
					+ " and enddate='"
					+ mStartDate
					+ "'"
					+ " and statetype='Available' and state='0'  and statereason='02'";
			mMap.put(mSql5, SysConst.UPDATE);

		}

		// 判断是否整单失效
		String contnoSql = "select 1 from lccont where contno = '" + LContNo
				+ "' and stateflag = '3'";
		String flag = new ExeSQL().getOneValue(contnoSql);

		if ("1".equals(flag)) {
			String mSql2 = "update lccont set stateflag='2', "
					+ " modifydate='" + CurrentDate + "',"
					+ " modifytime='" + CurrentTime
					+ "'  where contno='" + LContNo + "' and stateflag='3'";
			mMap.put(mSql2, SysConst.UPDATE);

			String tSQL6 = "update lccontstate set enddate = null, state = '1',modifydate = '"
					+ CurrentDate
					+ "', "
					+ "modifytime = '"
					+ CurrentTime
					+ "' where polno = '000000' and contno = '"
					+ LContNo
					+ "' and statetype = 'Available' and state = '0' and enddate = '"+ mStartDate +"' ";
			mMap.put(tSQL6, SysConst.UPDATE);
		}

		String mSql4 = "delete from lccontstate where contno='"
				+ LContNo
				+ "' "
				+ " and statetype='Terminate' and state='1'  and statereason='09'";

		mMap.put(mSql1, SysConst.UPDATE);
		mMap.put(mSql4, SysConst.DELETE);
		return true;
	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
