package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author not attributable
 * @version 1.1
 */
public class PrtGrpInsuredListWTUI
{
    /**错误的容器*/
    public CErrors mErrors = null;
    private PrtGrpInsuredListWTBL mPrtGrpInsuredListWTBL = null;

    public PrtGrpInsuredListWTUI()
    {
    }

    /**
     * 操作的提交方法
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String operate)
    {
        mPrtGrpInsuredListWTBL = new PrtGrpInsuredListWTBL();
        if(!mPrtGrpInsuredListWTBL.submitData(cInputData, operate))
        {
            mErrors = mPrtGrpInsuredListWTBL.mErrors;
            return false;
        }

        return true;
    }

    /**
     * 得到处理后的XML数据
     * @return XmlExport
     */
    public XmlExport getXmlExport()
    {
        return mPrtGrpInsuredListWTBL.getXmlExport();
    }


    public static void main(String[] args)
    {
        PrtGrpInsuredListWTUI prtgrpinsuredlistwtui = new PrtGrpInsuredListWTUI();
    }
}
