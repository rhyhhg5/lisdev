package com.sinosoft.lis.bq;
/*
 * 
 * 计算退保金类
 * yukun 2017-01-13新增
 * 
 * */
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

public class YbkRefundTFJS {
	
	public YbkRefundTFJS() {
	}

	public CErrors mErrors = new CErrors();
	
	public double getZTmoney(String contNo,String edorValidate){
    	//查询 长期险预计交至日期  
    	String payToDateLongPol = new ExeSQL().getOneValue("select max(paytodate) from lcpol where contno='"+contNo+"'");
    	if(payToDateLongPol==null||"".equals(payToDateLongPol)){
            mErrors.addOneError("保单号不存在");
            return 0;
    	}
    	
    	//查询退保的险种
    	LCPolDB tLCPolDB = new LCPolDB();
    	LCPolSet tLCPolSet = new LCPolSet();
    	tLCPolDB.setContNo(contNo);
    	tLCPolSet = tLCPolDB.query();
    	
    	EdorCalZTTestBL tEdorCalZTTestBL = new EdorCalZTTestBL();
    	tEdorCalZTTestBL.setEdorValiDate(edorValidate);
    	tEdorCalZTTestBL.setCurPayToDateLongPol(payToDateLongPol);
    	tEdorCalZTTestBL.setNeedBugetResultFlag(true);
    	tEdorCalZTTestBL.setOperator("YBT");
    	  
    	double getmoney = 0;
    	  
    	for(int i=1;i<=tLCPolSet.size();i++){
    		LJSGetEndorseSchema tLJSGetEndorseSchema  = tEdorCalZTTestBL.budgetOnePol(tLCPolSet.get(i).getPolNo());	
    		if(tLJSGetEndorseSchema != null){
    			getmoney+=tLJSGetEndorseSchema.getGetMoney();
    		}		
    	}
        getmoney = PubFun.setPrecision(getmoney, "0.00");
        if(getmoney != 0)
        {
          getmoney = -getmoney;
        }
    	return getmoney;
    }
}
