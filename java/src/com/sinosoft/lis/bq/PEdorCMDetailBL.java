package com.sinosoft.lis.bq;

import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 客户资料变更</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong,Luomin
 * @author rewrite by QiuYang 2005
 * @version 1.0
 */

public class PEdorCMDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private DetailDataQuery mQuery = null;

    private GlobalInput mGlobalInput = null;

    private String mTypeFlag = null;
    
    private String mGUFlag = null;

    private String stateFlag = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mAppntNo = null;

    private String mCustomerNo = null;

    private String mName = null;

    private String mSex = null;

    private String mBirthday = null;

    private String mIdType = null;

    private String mIdNo = null;

    private String mOccupationCode = null;

    private String mOccupationType = null;

    private String mMarriage = null;

    private String mRelation = null;
    
    private String mRelationToAppnt = null;

    private String mBankCode = null;

    private String mBankAccNo = null;

    private String mAccName = null;

    private String mInsuredState = null;
    
    private String mNationality = null; //国籍
    
    private String pNationality = null; //国籍

    private String mPosition = null; // 岗位职务

    private double mSalary = 0.0; //  年薪
    
    private String mIDStartDate = null; //证件有效日期
    
    private String mIDEndDate = null;  //证件失效日期
    
    private String mJoinCompanyDate = null; // 服务年数起始日
    
    private String mGetDutyKind = null; // 老年护理金领取方式
    
    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    private String mPolno = null;
    
    private LCInsuredListSchema tLCInsuredListSchema=null;	//团险学平险存放投保人信息
    
    private String mGrpInsuredPhone = null; //团体被保人联系电话
    
    private String mTaxPayerType = null;//个税征收方式
    
    private String mTaxNo = null;//个人税务登记证代码
    
    private String mCreditCode = null;//个人社会信用代码
    
    private String mGTaxNo = null;//单位税务登记证代码
    
    private String mGOrgancomCode = null;//单位社会信用代码
    
    private String mPrtNo = null;//印刷号码 
    
    /**
     * 提交数据
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到传入数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mTypeFlag = (String) cInputData.get(0);
            mGUFlag = (String) cInputData.get(1);
            TransferData mTransferData = new TransferData();
            mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
            if(mTransferData != null)
            {
            	stateFlag = (String) mTransferData.getValueByName("stateFlag");
            }
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            LPEdorItemSchema tLPEdorItemSchema = (LPEdorItemSchema)
                    cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
            LDPersonSchema tLDPersonSchema = (LDPersonSchema)
                    cInputData.getObjectByObjectName("LDPersonSchema", 0);
            LCInsuredSchema tLCInsuredSchema = (LCInsuredSchema)
                    cInputData.getObjectByObjectName("LCInsuredSchema", 0);
            tLCInsuredListSchema = (LCInsuredListSchema)
            cInputData.getObjectByObjectName("LCInsuredListSchema", 0);
            LPContSubSchema tLpContSubSchema = (LPContSubSchema)
            cInputData.getObjectByObjectName("LPContSubSchema", 0);
            
            this.mEdorNo = tLPEdorItemSchema.getEdorNo();
            this.mEdorType = tLPEdorItemSchema.getEdorType();
            this.mContNo = tLPEdorItemSchema.getContNo();
            this.mAppntNo = tLPEdorItemSchema.getInsuredNo(); //这里存的是投保人
            this.mCustomerNo = tLDPersonSchema.getCustomerNo();
            this.mName = tLDPersonSchema.getName();
            this.mSex = tLDPersonSchema.getSex();
            this.mBirthday = tLDPersonSchema.getBirthday();
            this.mIdType = tLDPersonSchema.getIDType();
            this.mIdNo = tLDPersonSchema.getIDNo();
            this.pNationality = tLDPersonSchema.getNativePlace();
            this.mOccupationCode = tLDPersonSchema.getOccupationCode();
            this.mOccupationType = tLDPersonSchema.getOccupationType();
            this.mMarriage = tLDPersonSchema.getMarriage();
            this.mRelation = tLCInsuredSchema.getRelationToMainInsured();
            this.mRelationToAppnt = tLCInsuredSchema.getRelationToAppnt();
            this.mBankCode = tLCInsuredSchema.getBankCode();
            this.mBankAccNo = tLCInsuredSchema.getBankAccNo();
            this.mAccName = tLCInsuredSchema.getAccName();
            this.mInsuredState = tLCInsuredSchema.getInsuredStat();
            this.mNationality = tLCInsuredSchema.getNativePlace(); //得到国籍
            this.mPosition = tLCInsuredSchema.getPosition(); // 得到职位
            this.mSalary = tLCInsuredSchema.getSalary(); // 得到年薪
            this.mIDStartDate = tLCInsuredSchema.getIDStartDate() ; //得到证件有效日期
            this.mIDEndDate = tLCInsuredSchema.getIDEndDate() ; //得到证件失效日期
            this.mGrpInsuredPhone=tLCInsuredSchema.getGrpInsuredPhone();
            
            if(tLpContSubSchema!=null){
            	this.mTaxPayerType = tLpContSubSchema.getTaxPayerType();
            	this.mTaxNo = tLpContSubSchema.getTaxNo();
            	this.mCreditCode = tLpContSubSchema.getCreditCode();
            	this.mGTaxNo = tLpContSubSchema.getGTaxNo();
            	this.mGOrgancomCode = tLpContSubSchema.getGOrgancomCode();
            	this.mPrtNo = tLpContSubSchema.getPrtNo();
            	
            }
            
            LCGetSchema tLCGetSchema = new LCGetSchema();
            if("Y".equals(mGUFlag)){
            	this.mJoinCompanyDate = tLCInsuredSchema.getJoinCompanyDate(); // 服务年数起始日
            	tLCGetSchema = (LCGetSchema)
                        cInputData.getObjectByObjectName("LCGetSchema", 0);
            	this.mGetDutyKind = tLCGetSchema.getGetDutyKind();
            }
            this.mQuery = new DetailDataQuery(mEdorNo, mEdorType);
            System.out.println(this.mNationality);
            System.out.println(this.pNationality);
            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    /*
     * 
     * */
    private String getOccupationType(String cOccupationCode){
    	String sql="select OccupationType from LDOccupation where '1218610553000'='1218610553000' and  OccupationCode = '"+cOccupationCode+"'  fetch first 2000 rows only ";
    	ExeSQL tExeSQL = new ExeSQL();     
    	String tOccupationType= tExeSQL.getOneValue(sql);
    	return tOccupationType;
    }
    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if ((mTypeFlag == null) || (mTypeFlag.equals("")))
        {
            mErrors.addOneError("缺少个/团单处理标志！");
            return false;
        }
        if ((!mTypeFlag.equals("G")) &&
                ((mAppntNo == null) || (mAppntNo.equals(""))))
        {
            mErrors.addOneError("投保人客户号为空！");
            return false;
        }
        if ((mCustomerNo == null) || (mCustomerNo.equals("")))
        {
            mErrors.addOneError("客户号为空！");
            return false;
        }
        //万能险被保险人发生年龄性别职业变更暂时不允许操作。按照旧的客户资料变更这里会进行重算保费。 added by huxl @ 20080806,万能CM上线后需要重新修改
        if(CommonBL.hasULIRisk(this.mContNo)&&!isTPHIRisk(this.mContNo)){
            if(isInsured()&&needChangePrem()){
               //mErrors.addOneError("万能险被保险人发生年龄性别职业变更暂时不允许操作！");
               //return false;
							Date mt = PubFun.calDate(new FDate().getDate(mCurrentDate), 1, "M", null);
							GregorianCalendar mCalendar = new GregorianCalendar();
							mCalendar.setTime(mt);
							int mYears = mCalendar.get(mCalendar.YEAR);
							int mMonths = mCalendar.get(mCalendar.MONTH) + 1;
							String monAfterCurrent = mYears + "-" + mMonths + "-"  + 1;
	            String sql = "update LPEdorItem " +
	                         "set EdorValiDate = '" +monAfterCurrent+"'" +
	                         " where EdorNo = '" + mEdorNo + "' " +
	                         " and EdorType = '" + mEdorType + "' " +
	                         " and InsuredNo = '" + mCustomerNo + "'";
	             mMap.put(sql, "UPDATE");
            }
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        //个单和团单分开操作，个单按投保人操作，团单按被保人操作
        if (mTypeFlag.equals("G"))
        {
        	if (IsCard()) {
       		 mErrors.addOneError("此单为激活卡,不允许做保全！");
                return false;
			}
        	 if(!setLPPerson()){
            	 
             	mErrors.addOneError("客户信息缺失");
                  return false;
             }
             if(!setGrpLPCont()){
             	
             	 mErrors.addOneError("团单信息缺失");
                  return false;
             }
             if(!setGrpLPInsured()){
             	
             	 mErrors.addOneError("团体客户信息缺失");
                  return false;
             }
             if(!setGrpLPPol()){
             	
             	 mErrors.addOneError("团体险种信息缺失");
                  return false;
             }
             if(!setOtherLP()){
             	
             	 mErrors.addOneError("暂定为其他信息缺失");
                  return false;
             }
             if("Y".equals(mGUFlag)){
              	
              	if(!setUGLPGet()){
                  	
                 	
                      return false;
                 }
              }
        }
        else
        {
        	if(!setLPPerson()){
            	
           	 mErrors.addOneError("客户信息缺失！");
                return false;
           }
          if(!setLPAppnt()){
       	   
       	   mErrors.addOneError("投保人信息缺失");
              return false;
          }
           if(!setLPCont())
           {
           	
           	 mErrors.addOneError("客户保单信息缺失");
                return false;
           }
           if(!setLPInsured()){
           	
           	 mErrors.addOneError("被保人信息缺失");
                return false;
           }
           if(!setLPContSub()){
        	   mErrors.addOneError("印刷号信息缺失");
        	   return false;
           }
        }
        setEdorState(BQ.EDORSTATE_INPUT);
        System.out.println("**********************************************");
       	System.out.println("**********************************************");	
        return true;
    }
    
    /**
     * 设置lpcontsub表
     */
    private boolean setLPContSub(){
    	if(mTaxPayerType==null){
    		return true;
    	}
    	if(mPrtNo==null||"".equals(mPrtNo)){
    		mErrors.addOneError("印刷号信息缺失");
    		return false;
    	}
    	LPContSubSchema lpContSubSchema = (LPContSubSchema)mQuery.getDetailData("LCContSub", mPrtNo);
    	lpContSubSchema.setEdorNo(mEdorNo);
    	lpContSubSchema.setEdorType(mEdorType);
    	lpContSubSchema.setOperator(mGlobalInput.Operator);
    	lpContSubSchema.setTaxPayerType(mTaxPayerType);
    	lpContSubSchema.setTaxNo(mTaxNo);
    	lpContSubSchema.setCreditCode(mCreditCode);
    	lpContSubSchema.setGTaxNo(mGTaxNo);
    	lpContSubSchema.setGOrgancomCode(mGOrgancomCode);
    	lpContSubSchema.setModifyDate(mCurrentDate);
    	lpContSubSchema.setModifyTime(mCurrentTime);
    	mMap.put(lpContSubSchema, "DELETE&INSERT");
    	return true;
    }
    
    /**
     * 校验是否为激活卡
     * @return boolean
     */
    private boolean IsCard(){
    	ExeSQL texesql=new ExeSQL();
    	String tcard=texesql.getOneValue("select 1 from lccont where  contno='"+mContNo+"' and exists (select 1 from lcgrpcont where grpcontno=lccont.grpcontno and cardflag='2') and exists (select 1 from licertify where prtno=lccont.prtno and activeflag='02') ");
    	if ((tcard == null) || (tcard.equals(""))) {
			return false;
		}else
		{
    	return true;}
    }

    /**
     * 设置LPPerson表
     */
    private boolean setLPPerson()
    {
    	if(mName==null&&mName.equals("")){
    		
    		mErrors.addOneError("客户姓名信息缺失");
    		return false;
    	}
        LPPersonSchema tLPPersonSchema = (LPPersonSchema)
                mQuery.getDetailData("LDPerson", mCustomerNo);
        tLPPersonSchema.setName(mName);
        tLPPersonSchema.setSex(mSex);
        tLPPersonSchema.setBirthday(mBirthday);
        tLPPersonSchema.setIDType(mIdType);
        tLPPersonSchema.setIDNo(mIdNo);
        tLPPersonSchema.setOccupationCode(mOccupationCode);
        tLPPersonSchema.setOccupationType(mOccupationType);
        tLPPersonSchema.setMarriage(mMarriage);
        tLPPersonSchema.setState(mInsuredState);
        tLPPersonSchema.setOperator(mGlobalInput.Operator);
        tLPPersonSchema.setModifyDate(mCurrentDate);
        tLPPersonSchema.setModifyTime(mCurrentTime);
        /*
         * 添加字段
         */
        System.out.println(mNationality);
        tLPPersonSchema.setNativePlace(mNationality);
        tLPPersonSchema.setSalary(mSalary);
        tLPPersonSchema.setPosition(mPosition);
        /*
         * 添加字段
         */
        mMap.put(tLPPersonSchema, "DELETE&INSERT");
    	
        return true;
    }

    /**
     * 设置LPAppnt表
     */
    private boolean setLPAppnt()
    {
        LCAppntDB tLCAppntDB = new LCAppntDB();
        String sql = "select * from LCAppnt a where AppntNo = '" + mAppntNo + "' "
                   + "and exists (select 1 from LCCont where ContNo = a.ContNo and (StateFlag is null or StateFlag not in ('0')))";
        if(stateFlag != null && stateFlag.equals("0"))
        {
        	sql = "select * from LCAppnt a where AppntNo = '" + mAppntNo + "' ";
        }
        LCAppntSet tLCAppntSet = tLCAppntDB.executeQuery(sql);
        for (int i = 1; i <= tLCAppntSet.size(); i++)
        {
            LCAppntSchema tLCAppntSchema = tLCAppntSet.get(i);
            if (tLCAppntSchema.getAppntNo().equals(mCustomerNo))
            {
                LPAppntSchema tLPAppntSchema = (LPAppntSchema)
                        mQuery.getDetailData("LCAppnt", tLCAppntSchema.getContNo());
                tLPAppntSchema.setAppntName(mName);
                tLPAppntSchema.setAppntSex(mSex);
                tLPAppntSchema.setAppntBirthday(mBirthday);
                tLPAppntSchema.setIDType(mIdType);
                tLPAppntSchema.setIDNo(mIdNo);
                tLPAppntSchema.setOccupationCode(mOccupationCode);
                tLPAppntSchema.setOccupationType(mOccupationType);
                tLPAppntSchema.setMarriage(mMarriage);
                tLPAppntSchema.setOperator(mGlobalInput.Operator);
                tLPAppntSchema.setModifyDate(mCurrentDate);
                tLPAppntSchema.setModifyTime(mCurrentTime);
                System.out.println(mNationality);
                tLPAppntSchema.setNativePlace(mNationality);
                tLPAppntSchema.setSalary(mSalary);
                tLPAppntSchema.setPosition(mPosition);
                tLPAppntSchema.setIDStartDate(mIDStartDate);
                tLPAppntSchema.setIDEndDate(mIDEndDate);
                mMap.put(tLPAppntSchema, "DELETE&INSERT");
                setLPPolAppnt(tLPAppntSchema.getContNo());                
            }
            else{
            
            		
            		
            		return true;
            	
            	
            }
            
        }
        return true;
    }
    
    /**
     * 设置LPPol表 //add by xp 090706 修改投保人姓名时不会改LCPOL表的错误
     */
    private void setLPPolAppnt(String contNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(contNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLPolSchema = tLCPolSet.get(i);
            if (mCustomerNo.equals(tLPolSchema.getAppntNo()))
            {
                LPPolSchema tLPPolSchema = (LPPolSchema)
                        mQuery.getDetailData("LCPol", tLPolSchema.getPolNo());
                tLPPolSchema.setAppntName(mName);         
                tLPPolSchema.setOperator(mGlobalInput.Operator);
//                tLPPolSchema.setMakeDate(mCurrentDate);
 //               tLPPolSchema.setMakeTime(mCurrentTime);
                tLPPolSchema.setModifyDate(mCurrentDate);
                tLPPolSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPPolSchema, "DELETE&INSERT");
            }
        }
    }

    /**
     * 设置LPInsured表
     */
    private boolean setLPInsured()
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        String sql = "select * from LCInsured a where AppntNo = '" + mAppntNo + "' "
                   + "and exists (select 1 from LCCont where ContNo = a.ContNo and (StateFlag is null or StateFlag not in ('0')))";
        if(stateFlag != null && stateFlag.equals("0"))
        {
        	sql = "select * from LCInsured a where AppntNo = '" + mAppntNo + "' ";
        }
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.executeQuery(sql);
//        if(tLCInsuredSet.size()==0)
//        {
//        	
//        	 mErrors.addOneError("没有查到被保人信息");
//        	return false;
//        }
        for (int i = 1; i <= tLCInsuredSet.size(); i++)
        {
            LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(i);
            System.out.println("mCustomerNo:" + mCustomerNo);
            if (tLCInsuredSchema.getInsuredNo().equals(mCustomerNo))
            {
                String[] keys = new String[2];
                keys[0] = tLCInsuredSchema.getContNo();
                keys[1] = tLCInsuredSchema.getInsuredNo();
                LPInsuredSchema tLPInsuredSchema = (LPInsuredSchema)
                        mQuery.getDetailData("LCInsured", keys);
                tLPInsuredSchema.setName(mName);
                tLPInsuredSchema.setSex(mSex);
                tLPInsuredSchema.setBirthday(mBirthday);
                tLPInsuredSchema.setIDType(mIdType);
                tLPInsuredSchema.setIDNo(mIdNo);
                tLPInsuredSchema.setOccupationCode(mOccupationCode);
                tLPInsuredSchema.setOccupationType(mOccupationType);
                tLPInsuredSchema.setMarriage(mMarriage);
                if(mContNo.equals(tLCInsuredSchema.getContNo())) {  //关联保单的被保人的相关关系不变 by LHY in 2018-08
                	tLPInsuredSchema.setRelationToMainInsured(mRelation);
                	tLPInsuredSchema.setRelationToAppnt(mRelationToAppnt);
                }
                tLPInsuredSchema.setBankCode(mBankCode);
                tLPInsuredSchema.setBankAccNo(mBankAccNo);
                tLPInsuredSchema.setAccName(mAccName);
                tLPInsuredSchema.setInsuredStat(mInsuredState);
                tLPInsuredSchema.setOperator(mGlobalInput.Operator);
                //--2011-12-01 add by 【OoO】
                tLPInsuredSchema.setIDStartDate(mIDStartDate);
                tLPInsuredSchema.setIDEndDate(mIDEndDate);                	
                System.out.println(mNationality);
                tLPInsuredSchema.setNativePlace(mNationality);
                tLPInsuredSchema.setPosition(mPosition);
                tLPInsuredSchema.setSalary(mSalary);
                tLPInsuredSchema.setModifyDate(mCurrentDate);
                tLPInsuredSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPInsuredSchema, "DELETE&INSERT");
                setLPPol(tLPInsuredSchema.getContNo(),
                        tLPInsuredSchema.getInsuredNo());
            }
        }
        return true;
    }

    /**
     * 设置LCCont表
     */
    private boolean setLPCont()
    {
    	
        LCContDB tLCContDB = new LCContDB();
        String sql = "select * from LCCont  where AppntNo = '" + mAppntNo + "' and (StateFlag is null or StateFlag not in ('0'))";
        if(stateFlag != null && stateFlag.equals("0"))
        {
        	sql = "select * from LCCont  where AppntNo = '" + mAppntNo + "'";
        }
        LCContSet tLCContSet = tLCContDB.executeQuery(sql);
//        if(tLCContSet.size()==0)
//        {
//        	
//        	 mErrors.addOneError("没有查到客户的保单");
//        	return false;
//        }
        for (int i = 1; i <= tLCContSet.size(); i++)
        {
            LCContSchema tLCContSchema = tLCContSet.get(i);
            if (mCustomerNo.equals(tLCContSchema.getAppntNo()) ||
                    mCustomerNo.equals(tLCContSchema.getInsuredNo()))
            {
                LPContSchema tLPContSchema = (LPContSchema)
                        mQuery.getDetailData("LCCont", tLCContSchema.getContNo());
                
                if (mCustomerNo.equals(tLCContSchema.getAppntNo()))
                {
                    tLPContSchema.setAppntName(mName);
                    tLPContSchema.setAppntSex(mSex);
                    tLPContSchema.setAppntBirthday(mBirthday);
                    tLPContSchema.setAppntIDType(mIdType);
                    tLPContSchema.setAppntIDNo(mIdNo);
                }
                if (mCustomerNo.equals(tLCContSchema.getInsuredNo()))
                {
                    tLPContSchema.setInsuredName(mName);
                    tLPContSchema.setInsuredSex(mSex);
                    tLPContSchema.setInsuredBirthday(mBirthday);
                    tLPContSchema.setInsuredIDType(mIdType);
                    tLPContSchema.setInsuredIDNo(mIdNo);
                }
                tLPContSchema.setOperator(mGlobalInput.Operator);
                tLPContSchema.setModifyDate(mCurrentDate);
                tLPContSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPContSchema, "DELETE&INSERT");
            }
        }
        return true;
    }

    /**
     * 设置LPPol表
     */
    private boolean setLPPol(String contNo, String insuredNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(contNo);
        tLCPolDB.setInsuredNo(insuredNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
        	mPolno = tLCPolSet.get(i).getMainPolNo();
            LCPolSchema tLPolSchema = tLCPolSet.get(i);
            if (mCustomerNo.equals(tLPolSchema.getAppntNo()) ||
                    mCustomerNo.equals(tLPolSchema.getInsuredNo()))
            {
                LPPolSchema tLPPolSchema = (LPPolSchema)
                        mQuery.getDetailData("LCPol", tLPolSchema.getPolNo());
                if (mCustomerNo.equals(tLPolSchema.getAppntNo()))
                {
                    tLPPolSchema.setAppntName(mName);
                }
                if (mCustomerNo.equals(tLPolSchema.getInsuredNo()))
                {
//                  int appAge = PubFun.calInterval(mBirthday,
//		                    tLPPolSchema.getCValiDate(), "Y");
		      	  int appAge = PubFun.calInterval(mBirthday,
		          		  tLPPolSchema.getPolApplyDate(), "Y");
                    tLPPolSchema.setInsuredName(mName);
                    tLPPolSchema.setInsuredSex(mSex);
                    tLPPolSchema.setInsuredBirthday(mBirthday);
                    tLPPolSchema.setInsuredAppAge(appAge);
                    tLPPolSchema.setOccupationType(mOccupationType);
                }
                tLPPolSchema.setOperator(mGlobalInput.Operator);
                tLPPolSchema.setModifyDate(mCurrentDate);
                tLPPolSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPPolSchema, "DELETE&INSERT");
            }
        }
        return true;
    }

    /**
     * 团单设置LPCont表
     */
    private boolean setGrpLPCont()
    {
        LPContSchema tLPContSchema = (LPContSchema)
                mQuery.getDetailData("LCCont", mContNo);
        //如果是连带被保人则不操作
        if (!mCustomerNo.equals(tLPContSchema.getInsuredNo()))
        {
            return true;
        }
        tLPContSchema.setInsuredName(mName);
        tLPContSchema.setInsuredSex(mSex);
        tLPContSchema.setInsuredBirthday(mBirthday);
        tLPContSchema.setInsuredIDType(mIdType);
        tLPContSchema.setInsuredIDNo(mIdNo);
        tLPContSchema.setOperator(mGlobalInput.Operator);
        tLPContSchema.setModifyDate(mCurrentDate);
        tLPContSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPContSchema, "DELETE&INSERT");
        
        return true;
    }

    /**
     * 团单设置LPInsured表
     */
    private boolean setGrpLPInsured()
    {
        String[] keys = new String[2];
        keys[0] = mContNo;
        keys[1] = mCustomerNo;
        LPInsuredSchema tLPInsuredSchema = (LPInsuredSchema)
                mQuery.getDetailData("LCInsured", keys);
//        if (!mCustomerNo.equals(tLPInsuredSchema.getInsuredNo()))
//        {
//            return false;
//        }
        tLPInsuredSchema.setName(mName);
        tLPInsuredSchema.setSex(mSex);
        tLPInsuredSchema.setBirthday(mBirthday);
        tLPInsuredSchema.setIDType(mIdType);
        tLPInsuredSchema.setIDNo(mIdNo);
        tLPInsuredSchema.setOccupationCode(mOccupationCode);
        tLPInsuredSchema.setOccupationType(mOccupationType);
        tLPInsuredSchema.setMarriage(mMarriage);
        tLPInsuredSchema.setRelationToMainInsured(mRelation);
        tLPInsuredSchema.setRelationToAppnt(mRelationToAppnt);
        tLPInsuredSchema.setBankCode(mBankCode);
        tLPInsuredSchema.setBankAccNo(mBankAccNo);
        tLPInsuredSchema.setAccName(mAccName);
        tLPInsuredSchema.setInsuredStat(mInsuredState);
        //--2011-12-01 add by 【OoO】
        System.out.println(mNationality);
        tLPInsuredSchema.setNativePlace(mNationality);
        tLPInsuredSchema.setPosition(mPosition);
        tLPInsuredSchema.setSalary(mSalary);
        
        //被保险人联系电话，既可以导入、也可从页面修改，为保持一致所以在页面修改时同步到导入的数据
        tLPInsuredSchema.setGrpInsuredPhone(mGrpInsuredPhone);
        String updateImport="update LPDiskImport set Phone='"+mGrpInsuredPhone+"',modifydate=current date,modifytime=current time "
        			+" where EdorNo='"+mEdorNo+"' and EdorType='"+mEdorType+"' "
        			+" and insuredno='"+mCustomerNo+"' and state='"+BQ.IMPORTSTATE_SUCC+"' ";
        mMap.put(updateImport, SysConst.UPDATE);
        
        if("Y".equals(mGUFlag)){
        	
        	tLPInsuredSchema.setJoinCompanyDate(this.mJoinCompanyDate);  // 服务年数起始日
        	
        }
        tLPInsuredSchema.setOperator(mGlobalInput.Operator);
        tLPInsuredSchema.setModifyDate(mCurrentDate);
        tLPInsuredSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsuredSchema, "DELETE&INSERT");
        
        //学平险汇交件保单可变更投保人信息
        String strXFlag="select grpcontno from lcgrpcont where grpcontno= "
        				+ " (select grpcontno from lccont where contno='"+ mContNo +"') "
        				+ " and ContPrintType='5' with ur ";
        
        ExeSQL texesql=new ExeSQL();
    	String grpContNo=texesql.getOneValue(strXFlag);
    	if(null!=grpContNo && ""!=grpContNo){
    		if(null!=tLCInsuredListSchema){
    			LBInsuredListDB tLBInsuredListDB=new LBInsuredListDB();
    			tLBInsuredListDB.setGrpContNo(grpContNo);
    			tLBInsuredListDB.setInsuredNo(mCustomerNo);
    			LBInsuredListSet tLBInsuredListSet=tLBInsuredListDB.query();
    			if(tLBInsuredListSet.size()!=1){
    				mErrors.addOneError("获取投保人信息LBInsuredList失败！");
        			return false;
    			}
    			Reflections mReflections=new Reflections();
    			LCInsuredListSchema mLCInsuredListSchema=new LCInsuredListSchema();
    			mReflections.transFields(mLCInsuredListSchema,tLBInsuredListSet.get(1));
    			mLCInsuredListSchema.setAppntName(tLCInsuredListSchema.getAppntName());
    			mLCInsuredListSchema.setAppntSex(tLCInsuredListSchema.getAppntSex());
    			mLCInsuredListSchema.setAppntIdNo(tLCInsuredListSchema.getAppntIdNo());
    			mLCInsuredListSchema.setAppntIdType(tLCInsuredListSchema.getAppntIdType());
    			mLCInsuredListSchema.setAppntBirthday(tLCInsuredListSchema.getAppntBirthday());
    			mLCInsuredListSchema.setGrpContNo(mEdorNo);
    			mLCInsuredListSchema.setInsuredID(mCustomerNo);//避免主键冲突,保全数据insuredID字段存客户号
    			mLCInsuredListSchema.setEdorNo(mEdorNo);
    			mLCInsuredListSchema.setMakeDate(mCurrentDate);
    			mLCInsuredListSchema.setModifyDate(mCurrentDate);
    			mLCInsuredListSchema.setMakeTime(mCurrentTime);
    			mLCInsuredListSchema.setModifyTime(mCurrentTime);
    			
    			//避免误删增人数据
    			String strDel="delete from LCInsuredList where grpcontno='"+ mEdorNo +"' " 
    						+ " and insuredno='"+ mCustomerNo +"' and edorno='"+ mEdorNo +"' ";
    			mMap.put(strDel,"DELETE");
    			mMap.put(mLCInsuredListSchema, "INSERT");
    		}else{
    			mErrors.addOneError("获取投保人信息失败！");
    			return false;
    		}
    	}
    	
        return true;
    }

    /**
     * 团单设置LPPol表
     */
    private boolean setGrpLPPol()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mContNo);
        tLCPolDB.setInsuredNo(mCustomerNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
       
	    if(tLCPolSet.size()==0){
    		
    		mErrors.addOneError("险种信息缺失");
    		return false;
    	}
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLPolSchema = tLCPolSet.get(i);
            LPPolSchema tLPPolSchema = (LPPolSchema)
                    mQuery.getDetailData("LCPol", tLPolSchema.getPolNo());
	      	int insuredAppAge = PubFun.calInterval(mBirthday,
	          		  tLPPolSchema.getCValiDate(), "Y");
	      	tLPPolSchema.setInsuredAppAge(insuredAppAge);
            tLPPolSchema.setInsuredName(mName);
            tLPPolSchema.setInsuredSex(mSex);
            tLPPolSchema.setInsuredBirthday(mBirthday);
            tLPPolSchema.setOccupationType(mOccupationType);
            tLPPolSchema.setOperator(mGlobalInput.Operator);
            tLPPolSchema.setModifyDate(mCurrentDate);
            tLPPolSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPPolSchema, "DELETE&INSERT");
            
        }
        return true;
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
    	 String w = "";
    	 if (!mTypeFlag.equals("G"))
         {
           if((mPolno!=null)&&(!mPolno.equals("")))
            {
              w="    polno='"+mPolno+"', ";
            }
         }
    	 else
    	 {
    		 w=" ";
    	 }
        
        String sql = "update  LPEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                 w +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +                
                	
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "and InsuredNo = '" + mCustomerNo + "'";
        mMap.put(sql, "UPDATE");
    }
    /**
    * 判断判断保全变更的客户是否是被保人
    * @return boolean
    */
   private boolean isInsured()
   {
       LCInsuredDB tLCInsuredDB = new LCInsuredDB();
       tLCInsuredDB.setContNo(this.mContNo);
       tLCInsuredDB.setInsuredNo(this.mCustomerNo);
       if (tLCInsuredDB.query().size() == 0)
       {
           return false;
       }
       return true;
   }
   /**
 * 判断是否需要重算保费
 * @return boolean 需要重算保费返回true
 */
private boolean needChangePrem()
{
    LCInsuredDB tLCInsuredDB = new LCInsuredDB();
    tLCInsuredDB.setContNo(mContNo);
    tLCInsuredDB.setInsuredNo(mCustomerNo);
    if (!tLCInsuredDB.getInfo())
    {
        mErrors.addOneError("找不到被保人信息！");
        return false;
    }
    String tOccupationType="";
    if(tLCInsuredDB.getOccupationType()==null||"".equals(tLCInsuredDB.getOccupationType().trim())){
    	tOccupationType = getOccupationType(tLCInsuredDB.getOccupationCode());
    }else{
    	/*
    	 * >cbs00017611
           >cbs00017569
		   >cbs00017366
           >
           >这三个cq都是同一个问题。程序改完了，你找个单子测试下，没问题就 check in到vss上，我看下。如果可以的话，就更新正式机。

    	 * */
    	tOccupationType =tLCInsuredDB.getOccupationType();
    }
   // 如果性别和年龄改变则重算保额保费
    if (!StrTool.compareString(tLCInsuredDB.getSex(),
                               mSex) ||
        !StrTool.compareString(tLCInsuredDB.getBirthday(),
                               mBirthday) 
//                               ||
//        !StrTool.compareString(tOccupationType,
//                               mOccupationType)
                               )
    {
        return true;
    }
    return false;
}

//判断是否是税优保单
private boolean isTPHIRisk(String tContNo){
    String strSYFlag="select 1 from lcpol where contno='"+tContNo+"' and riskcode in  "
		+ " (select riskcode from lmriskapp where taxoptimal='Y') "
		+ "  with ur ";

    ExeSQL texesql=new ExeSQL();
    String isTPHI=texesql.getOneValue(strSYFlag);
    if(isTPHI!=null && isTPHI!=""){
    	return true;
    }
    return false;
}
/**
 * 同时处理该客户作为其它团单被保人的资料变更
 * 20090320 zhanggm cbs00024868 
 */
private boolean setOtherLP()
{
	StringBuffer sql = new StringBuffer();
	sql.append("select contno from lcinsured a where insuredno = '" +mCustomerNo + "' ");
	sql.append("and grpcontno <> '" + BQ.GRPFILLDATA + "' ");
	sql.append("and contno <> '" + mContNo + "'");
	sql.append("and exists (select 1 from lccont where contno = a.contno and appflag = '1') ");
	System.out.println("查询关联保单："+sql);
	SSRS tSSRS = new SSRS();
	tSSRS = new ExeSQL().execSQL(sql.toString());
	
	if(tSSRS.MaxRow==0)
	{
		System.out.println("被保人"+mCustomerNo+"没有其它关联团单需要变更");
		return true;
	}
	else
	{
		for(int i=1;i<=tSSRS.MaxRow;i++)
		{
			String tContNo = tSSRS.GetText(i, 1);
			System.out.println("PEdorCMDetailBL.java-L613->保单："+tContNo);
			setOtherGrpLPCont(tContNo);
			setOtherGrpLPInsured(tContNo);
			setOtherGrpLPPol(tContNo);
		}
	}
	return true;
}

/**
 * 该客户作为其它团单被保人的团单设置LPCont表
 */
private void setOtherGrpLPCont(String aContNo)
{
	LPContSchema tLPContSchema = (LPContSchema)mQuery.getDetailData("LCCont", aContNo);
    //如果是连带被保人则不操作
    if (!mCustomerNo.equals(tLPContSchema.getInsuredNo()))
    {
        return;
    }
	tLPContSchema.setInsuredName(mName);
    tLPContSchema.setInsuredSex(mSex);
    tLPContSchema.setInsuredBirthday(mBirthday);
    tLPContSchema.setInsuredIDType(mIdType);
    tLPContSchema.setInsuredIDNo(mIdNo);
    tLPContSchema.setOperator(mGlobalInput.Operator);
    tLPContSchema.setModifyDate(mCurrentDate);
    tLPContSchema.setModifyTime(mCurrentTime);
    mMap.put(tLPContSchema, "DELETE&INSERT");
}

/**
 * 该客户作为其它团单被保人的团单设置LPInsured表
 */
private void setOtherGrpLPInsured(String aContNo)
{
    String[] keys = new String[2];
    keys[0] = aContNo;
    keys[1] = mCustomerNo;
    LPInsuredSchema tLPInsuredSchema = (LPInsuredSchema)mQuery.getDetailData("LCInsured", keys);
    tLPInsuredSchema.setName(mName);
    tLPInsuredSchema.setSex(mSex);
    tLPInsuredSchema.setBirthday(mBirthday);
    tLPInsuredSchema.setIDType(mIdType);
    tLPInsuredSchema.setIDNo(mIdNo);
    tLPInsuredSchema.setOccupationCode(mOccupationCode);
    tLPInsuredSchema.setOccupationType(mOccupationType);
    tLPInsuredSchema.setMarriage(mMarriage);
    tLPInsuredSchema.setRelationToMainInsured(mRelation);
    tLPInsuredSchema.setRelationToAppnt(mRelationToAppnt);
    tLPInsuredSchema.setBankCode(mBankCode);
    tLPInsuredSchema.setBankAccNo(mBankAccNo);
    tLPInsuredSchema.setAccName(mAccName);
    tLPInsuredSchema.setInsuredStat(mInsuredState);
    System.out.println(mNationality);
    tLPInsuredSchema.setNativePlace(mNationality);
    tLPInsuredSchema.setPosition(mPosition);
    tLPInsuredSchema.setSalary(mSalary);
    tLPInsuredSchema.setOperator(mGlobalInput.Operator);
    tLPInsuredSchema.setModifyDate(mCurrentDate);
    tLPInsuredSchema.setModifyTime(mCurrentTime);
    mMap.put(tLPInsuredSchema, "DELETE&INSERT");
}

/**
 * 该客户作为其它团单被保人的团单设置LPPol表
 */
private void setOtherGrpLPPol(String aContNo)
{
	LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setContNo(aContNo);
    tLCPolDB.setInsuredNo(mCustomerNo);
    LCPolSet tLCPolSet = tLCPolDB.query();
    for (int i = 1; i <= tLCPolSet.size(); i++)
    {
        LCPolSchema tLPolSchema = tLCPolSet.get(i);
        LPPolSchema tLPPolSchema = (LPPolSchema)mQuery.getDetailData("LCPol", tLPolSchema.getPolNo());
        tLPPolSchema.setInsuredName(mName);
        tLPPolSchema.setInsuredSex(mSex);
        tLPPolSchema.setInsuredBirthday(mBirthday);
      	int insuredAppAge = PubFun.calInterval(mBirthday,
      		  tLPPolSchema.getCValiDate(), "Y");
      	tLPPolSchema.setInsuredAppAge(insuredAppAge);
        tLPPolSchema.setOccupationType(mOccupationType);
        tLPPolSchema.setOperator(mGlobalInput.Operator);
        tLPPolSchema.setModifyDate(mCurrentDate);
        tLPPolSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPPolSchema, "DELETE&INSERT");
    }
}

/**
 * 设置LPGet表
 */
private boolean setUGLPGet()
{
    LCGetDB tLCGetDB = new LCGetDB();
    String sql = "select * from LCGet a where insuredno = '" + mCustomerNo + "' "
               + "and ContNo='"+mContNo+"' and getdutykind is not null with ur";
    
    LCGetSet tLCGetSet = tLCGetDB.executeQuery(sql);
    for (int i = 1; i <= tLCGetSet.size(); i++)
    {
        LCGetSchema tLCGetSchema = tLCGetSet.get(i);
        if(!mGetDutyKind.equals(tLCGetSchema.getGetDutyKind())){
        	String tbalaSql = "select baladate from lcinsureacc a where insuredno = '" + mCustomerNo + "' "
                    + "and ContNo='"+mContNo+"' fetch first 1 rows only with ur";
         
        	ExeSQL tExeSQL = new ExeSQL();     
        	String tbaladate = tExeSQL.getOneValue(tbalaSql);
        	Date tCurrentDate = new FDate().getDate(mCurrentDate); // 当前日期
        	Date tGetStartDate = new FDate().getDate(tLCGetSchema.getGetStartDate()); //老年护理金起期
        	Date tDBalaDate = new FDate().getDate(tbaladate); //最后一次结算日期
        	
        	if(tGetStartDate.compareTo(tCurrentDate)<0){
        		
        		mErrors.addOneError("团体万能险老年护理金调整时间"+mCurrentDate+"不能晚于老年护理金起期"+tLCGetSchema.getGetStartDate());
                return false;
        	}
        	if(tbaladate!=null&&!"".equals(tbaladate)){
        		
        		if(tGetStartDate.compareTo(tDBalaDate)<0){
            		
            		mErrors.addOneError("团体万能险保单最后一次月结时间"+tbaladate+"晚于老年护理金起期"+tLCGetSchema.getGetStartDate()+",不能做老年护理金调整的操作");
                    return false;
            	}
        		
        	}
        	
        }
        String[] keys = new String[3];
        keys[0] = tLCGetSchema.getPolNo();
        keys[1] = tLCGetSchema.getDutyCode();
        keys[2] = tLCGetSchema.getGetDutyCode();
        
        LPGetSchema tLPGetSchema = (LPGetSchema)
        mQuery.getDetailData("LCGet", keys);
        tLPGetSchema.setGetDutyKind(mGetDutyKind);
        tLPGetSchema.setOperator(mGlobalInput.Operator);
        tLPGetSchema.setModifyDate(mCurrentDate);
        tLPGetSchema.setModifyTime(mCurrentTime);
        System.out.println(mGetDutyKind);
        mMap.put(tLPGetSchema, "DELETE&INSERT");
                        
                
    }
    return true;
	}


    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
