package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

//程序名称：PEdorWXConfirmBL.java
//程序功能：
//创建日期：2011-08-04
//创建人  ：xp
//更新记录：  更新人    更新日期     更新原因/内容

public class PEdorWXConfirmBL implements EdorConfirm {
	private MMap mMap = new MMap();

	private GlobalInput mGlobalInput = null;

	private LPEdorItemSchema mLPEdorItemSchema = null;

	private String mEdorNo = null;

	private String mEdorType = null;

	private String mContNo = null;

	private ValidateEdorData2 mValidateEdorData = null;

	/** 保全项目特殊数据 */
	private EdorItemSpecialData mEdorItemSpecialData = null;

	// 是否配置该保单,配置后该保单以后算初始扣费均按当前保额
	private String mFeeMode = "";

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!dealData()) {
			return false;
		}
		return true;
	}

	/**
	 * 返回计算结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		VData data = new VData();
		data.add(mMap);
		return data;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
		mEdorNo = mLPEdorItemSchema.getEdorNo();
		mEdorType = mLPEdorItemSchema.getEdorType();
		mContNo = mLPEdorItemSchema.getContNo();
		mValidateEdorData = new ValidateEdorData2(mGlobalInput, mEdorNo, mEdorType, mContNo, "ContNo");
		mEdorItemSpecialData = new EdorItemSpecialData(mLPEdorItemSchema);
		if (!mEdorItemSpecialData.query()) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取明细录入信息失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mFeeMode = mEdorItemSpecialData.getEdorValue("FEEMODE");
		if (mFeeMode == null || mFeeMode.equals("") || mFeeMode.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单配置信息失败";
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 处理业务逻辑
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		if (!validateEdorData()) {
			return false;
		}
		if (mFeeMode.equals("1")) {
			// 配置保单
			LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
			tLPEdorEspecialDataSchema.setEdorNo(mContNo);
			tLPEdorEspecialDataSchema.setEdorAcceptNo(mContNo);
			tLPEdorEspecialDataSchema.setDetailType("ULIFEE");
			tLPEdorEspecialDataSchema.setEdorType("WN");
			tLPEdorEspecialDataSchema.setEdorValue("1");
			tLPEdorEspecialDataSchema.setPolNo("000000");
			mMap.put(tLPEdorEspecialDataSchema, SysConst.DELETE_AND_INSERT);
		}else if(mFeeMode.equals("0")){
//			对以前的配置也进行删除
			LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
			tLPEdorEspecialDataSchema.setEdorNo(mContNo);
			tLPEdorEspecialDataSchema.setEdorAcceptNo(mContNo);
			tLPEdorEspecialDataSchema.setDetailType("ULIFEE");
			tLPEdorEspecialDataSchema.setEdorType("WN");
			tLPEdorEspecialDataSchema.setEdorValue("1");
			tLPEdorEspecialDataSchema.setPolNo("000000");
			mMap.put(tLPEdorEspecialDataSchema, SysConst.DELETE);
		}
		
		return true;
	}

	/**
	 * 使保全数据生效
	 * 
	 * @return boolean
	 */
	private boolean validateEdorData() {
		String[] addTables = { "LCInsureAccTrace", "LCInsureAccFeeTrace" };
		mValidateEdorData.addData(addTables);
		String[] chgTables = { "LCInsureAcc", "LCInsureAccFee", "LCInsureAccClass", "LCInsureAccClassFee" };
		mValidateEdorData.changeData(chgTables);
		mMap.add(mValidateEdorData.getMap());
		return true;
	}
}
