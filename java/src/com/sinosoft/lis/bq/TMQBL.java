package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContGetPolDB;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContStateSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.vschema.LCContPlanFactorySet;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全删除业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author lh
 * @version 1.0
 */

//功能：查询出个人批改主表中本次申请的批改类型
//入口参数：个单的保单号、批单号
//出口参数：每条记录的个单的保单号、批单号和批改类型
public class TMQBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    
    private MMap map = new MMap();
    private ExeSQL tExeSQL=new ExeSQL();
    /** 数据操作字符串 */
    private String mOperate;
    private String mContNo ="";
    private String mPolNo = "";
    private String mStateType ="";
    private LCContStateSchema mLCContStateSchema = new LCContStateSchema();
    private LCContStateSet mLCContStateSet = new LCContStateSet();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();
    public TMQBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData tInputData,String tOperate) {
    	System.out.println("BL---submite-------start");
    	mInputData = (VData) tInputData.clone();
        this.mOperate = tOperate;
        if(!getInputData()) {
        	System.out.println("BL----getInputData-----方法出错！");
            return false;
        }
        
        if(!checkData()) {
            return false;
        }
            
        if (!dealData())
        {
            return false;
        }
        
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
           if (!tPubSubmit.submitData(mInputData, null)) {
                 this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                 return false;
           }
       
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */

    private boolean checkData()
    {
    	 
    	System.out.println("进入----BL----checkData---");
            if (mOperate.equals("UPDATE"))
            {
                //判断该保单下是否存在未保全确认的该申请项目
                for (int i = 1; i <= mLCContStateSet.size(); i++)
                {
                	LCContStateSchema tLCContStateSchema = new LCContStateSchema();
                    tLCContStateSchema.setSchema(mLCContStateSet.get(i));
                    System.out.println("BL---tLCContStateSchema="+tLCContStateSchema);
                    String sql = "select 1 from LPEdorItem " +
                                 "where ContNo = '" +mContNo + "' " +
                                 "and   EdorType = 'FX' " +
                                 "and   EdorState in ('1', '2') ";
                    ExeSQL tExeSQL = new ExeSQL();
                    String count = tExeSQL.getOneValue(sql);
                    if (count != null&&!"".equals(count))
                    {
                    	
                        CError tError = new CError();
                        tError.moduleName = "TMQBL";
                        tError.functionName = "checkData";
                        tError.errorMessage = "保单下存在未结案的复效保全！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }
        return true;
    }
    
    private boolean dealData()
    {
    	System.out.println("进入----BL----dealData()中");
        if (!dealGetDate())
        {
            return false;
        }

        return true;
    }
    
    private boolean dealGetDate(){
    	System.out.println("进入---BL---dealGetDate()");
    	//System.out.println(mStateType+"-----");
    	for(int i=1;i <= mLCContStateSet.size();i++){
    		mLCContStateSchema = mLCContStateSet.get(i);
    		this.mPolNo = mLCContStateSchema.getPolNo();
    		this.mStateType = mLCContStateSchema.getStateType();
    		System.out.println("---"+mPolNo+"---"+mStateType);
    		if(mStateType.equals("Terminate"))
    		{
    			System.out.println("进入---失效终止方法----");
    			String sql1 = "update lccontstate set state='0',modifydate='"+theCurrentDate+"', " +
    			"modifytime='"+theCurrentTime+"',enddate='"+theCurrentDate+"' " +
    			"where contno='"+mContNo+"' and polno='"+mPolNo+"' and state='1' and " +
    			"enddate is null with ur ";
    			String sql2 = "update lccont set stateflag='2',modifydate='"+theCurrentDate+"',modifytime='"+theCurrentTime+"' " +
    			"where contno='"+mContNo+"' and stateflag='3' with ur ";
    			String sql3 = "update lcpol set modifydate='"+theCurrentDate+"',modifytime='"+theCurrentTime+"',stateflag='2'  " +
    			"where contno='"+mContNo+"' and polno='"+mPolNo+"' and stateflag='3' with ur ";
    			//String sql4 = "delete from lccontstate where contno='"+mContNo+"' and polno='"+mPolNo+"' and statetype='Terminate'";
    			map.put(sql1,"UPDATE");
    			map.put(sql2,"UPDATE");
    			map.put(sql3,"UPDATE");
    			//map.put(sql4,"DELETE");
    		}
    		if(mStateType.equals("Available"))
    		{
    			System.out.println("进入---失效中止方法----");
    			String sql1 = "update lccontstate set state='0',modifydate='"+theCurrentDate+"', " +
    			"modifytime='"+theCurrentTime+"',enddate=current date " +
    			"where contno='"+mContNo+"' and polno='"+mPolNo+"' and state='1' and " +
    			"enddate is null with ur ";
    			String sql2 = "update lccont set stateflag='1',modifydate='"+theCurrentDate+"',modifytime='"+theCurrentTime+"' " +
    			"where contno='"+mContNo+"' and stateflag='2' with ur ";
    			String sql3 = "update lcpol set modifydate='"+theCurrentDate+"',modifytime='"+theCurrentTime+"',stateflag='1'  " +
    			"where contno='"+mContNo+"' and polno='"+mPolNo+"' and stateflag='2' with ur ";
    			
    			//String sql4 = "delete from lccontstate ";
    			map.put(sql1, "UPDATE");
    			map.put(sql2, "UPDATE");
    			map.put(sql3, "UPDATE");
    			
    		}
    	}
    	return true;
    }
    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData()
    {
    	//System.out.println("进入----BL---getInputData()");
    	try
        {
    		System.out.println("进入----BL---getInputData()");
    		mLCContStateSet = (LCContStateSet)mInputData.getObjectByObjectName("LCContStateSet", 0);
    		System.out.println("长度是："+mLCContStateSet.size());
    		for(int i=1;i <= mLCContStateSet.size();i++){
    			this.mContNo = mLCContStateSet.get(i).getContNo();
    	    	System.out.println("---mContNo="+mContNo);
    		}
    			
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ScanDeleBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
   /* private void dealPol(LCContStateSchema tLCContStateSchema){
    	System.out.println("进入dealpol()方法-------");
    	this.mContNo = tLCContStateSchema.getContNo();
    	System.out.println("---mContNo="+mContNo);
    }*/
    private boolean prepareOutputData() {

		try {
			System.out.println("进入---BL---prepareOutputData()");
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TMQBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "准备传输数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
    
    public VData getResult() {
        return mResult;
    }
    public static void main(String[] args)
    {
        /*    PEdorAppCancelBL tPEdorAppCancelBL=new PEdorAppCancelBL();
            VData tVData=new VData();
            LPEdorMainSchema tLPEdorMainSchema=new LPEdorMainSchema();
            tLPEdorMainSchema.setContNo("00000120020420000083");
            tLPEdorMainSchema.setPolNo("00000120020210000016");
            tLPEdorMainSchema.setEdorState("1");
            tVData.addElement(tLPEdorMainSchema);
            tPEdorAppCancelBL.submitData(tVData,"UPDATE||EDOR");
        */
    }
}

