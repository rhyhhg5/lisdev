package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 卡折增加连带被保人</p>
 * <p>Description: 卡折增加连带被保人</p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Sinosoft</p>
 * @author Zhanggm
 * @version 1.0
 */

public class LLWSCertifyUI
{
    private LLWSCertifyBL mLLWSCertifyBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public LLWSCertifyUI(GlobalInput gi, String edorNo, String grpContNo)
    {
        mLLWSCertifyBL = new LLWSCertifyBL(gi, edorNo, grpContNo);
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mLLWSCertifyBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mLLWSCertifyBL.mErrors.getFirstError();
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mLLWSCertifyBL.getMessage();
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
    }
}
