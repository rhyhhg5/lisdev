package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong
 * @version 1.0
 */
public class PEdorADDetailBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    private MMap mMap = new MMap();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LPAppntSchema mLPAppntSchema = new LPAppntSchema();
    private LPInsuredSchema mLPInsuredSchema = new LPInsuredSchema();
    private LPInsuredSchema bqLPInsuredSchema = new LPInsuredSchema();//BQ
    private LPAppntSet saveLPAppntSet = new LPAppntSet();
    private LPInsuredSet saveLPInsuredSet = new LPInsuredSet();
    private LPInsuredSet bqLPInsuredSet = new LPInsuredSet();//BQ
	private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
	private LPAddressSet mLPAddressSet = new LPAddressSet();
    private LPContSet mLPContSet = new LPContSet();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String currDate=PubFun.getCurrentDate();
    private String currTime=PubFun.getCurrentTime();
    private boolean insuredFlag = false;//-----标识是不是被保人联系方式变更
    private boolean isInsured=false;//20141014被保险人变更--BQ
    

    public PEdorADDetailBL()
    {
    }

	public boolean submitData(VData cInputData, String cOperate)
	{
        if (getSubmitData(cInputData, cOperate) == null)
        {
            return false;
        }
		PubSubmit tSubmit = new PubSubmit();
		if (!tSubmit.submitData(mResult, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorICDetailBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}
	
	/**
	 * 被保人联系方式变更
	 * @param cInputData
	 * @param cOperate
	 * @return
	 */
	public boolean submitInsuData(VData cInputData, String cOperate)
	{
		insuredFlag = true;
        if (getSubmitInsuData(cInputData, cOperate) == null)
        {
            return false;
        }
		PubSubmit tSubmit = new PubSubmit();
		
		if (!tSubmit.submitData(mResult, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorICDetailBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	public MMap getSubmitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		mOperate = cOperate;

		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData())
		{
			return null;
		}

		//数据查询业务处理
		if (cOperate.equals("QUERY||MAIN") || cOperate.equals("QUERY||DETAIL"))
		{
			if (!queryData())
			{
				return null;
			}
			else
			{
				return null;
			}
		}

		//数据准备操作
		if (!dealData())
		{
			return null;
		}
		return mMap;
	}
	public MMap getSubmitInsuData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		mOperate = cOperate;

		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData())
		{
			return null;
		}
		//数据查询业务处理
		if (cOperate.equals("QUERY||MAIN") || cOperate.equals("QUERY||DETAIL"))
		{
			if (!queryInsuData())//被保人信息----
			{
				return null;
			}
			else
			{
				return null;
			}
		}
		
		//数据准备操作
		if (!dealData())
		{
			return null;
		}
		return mMap;
	}

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 数据查询业务处理(queryData())
     */
    private boolean queryData()
    {
        if (mOperate.equals("QUERY||MAIN"))
        {
            LPAppntBL tLPAppntBL = new LPAppntBL();
            LPAppntSet tLPAppntSet = new LPAppntSet();
            tLPAppntBL.setSchema(mLPAppntSchema);

            tLPAppntSet = tLPAppntBL.queryLPAppnt(mLPEdorItemSchema);

            String tReturn;
            tReturn = tLPAppntSet.encode();
            tReturn = "0|" + tLPAppntSet.size() + "^" + tReturn;

            mResult.clear();
            mResult.add(tReturn);
        }
        else if (mOperate.equals("QUERY||DETAIL"))
        {
            LPAppntSchema tLPAppntSchema = new LPAppntSchema();
            LPAppntBL tLPAppntBL = new LPAppntBL();
            tLPAppntBL.setSchema(mLPAppntSchema);

            if (!tLPAppntBL.queryLPAppnt(mLPAppntSchema))
            {
                return false;
            }

            tLPAppntSchema = tLPAppntBL.getSchema();

            String tReturn = tLPAppntSchema.encode();
            mResult.clear();
            mResult.add(tReturn);
        }
        return true;
    }

    /**
     * 数据查询业务处理(queryInsuData())
     * (被保人联系方式变更--------------------------)
     */
    private boolean queryInsuData()
    {
        if (mOperate.equals("QUERY||MAIN"))
        {
            LPInsuredBL tLPInsuredBL = new LPInsuredBL();
            LPInsuredSet tLPInsuredSet = new LPInsuredSet();
            tLPInsuredBL.setSchema(mLPInsuredSchema);

            tLPInsuredSet = tLPInsuredBL.queryLPInsured(mLPEdorItemSchema);

            String tReturn;
            tReturn = tLPInsuredSet.encode();
            tReturn = "0|" + tLPInsuredSet.size() + "^" + tReturn;

            mResult.clear();
            mResult.add(tReturn);
        }
        else if (mOperate.equals("QUERY||DETAIL"))
        {
            LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
            LPInsuredBL tLPInsuredBL = new LPInsuredBL();
            tLPInsuredBL.setSchema(mLPInsuredSchema);

            if (!tLPInsuredBL.queryLPInsured(mLPInsuredSchema))
            {
                return false;
            }

            tLPInsuredSchema = tLPInsuredBL.getSchema();

            String tReturn = tLPInsuredSchema.encode();
            mResult.clear();
            mResult.add(tReturn);
        }
        return true;
    }

    

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData()
    {
        try
        {
            mLPAppntSchema = (LPAppntSchema) mInputData
                             .getObjectByObjectName("LPAppntSchema", 0);
            mLPInsuredSchema = (LPInsuredSchema) mInputData
            				 .getObjectByObjectName("LPInsuredSchema", 0);
            bqLPInsuredSchema = (LPInsuredSchema) mInputData
            				 .getObjectByObjectName("LPInsuredSchema", 0);
            mLCAddressSchema = (LCAddressSchema) mInputData
                               .getObjectByObjectName("LCAddressSchema", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData
                                .getObjectByObjectName("LPEdorItemSchema", 0);
            mLPContSet = (LPContSet) mInputData
                         .getObjectByObjectName("LPContSet", 0);
            mGlobalInput = (GlobalInput) mInputData.
                           getObjectByObjectName("GlobalInput", 0);
            
            String StrSQL="select 1 from LCAppnt where contno='"+mLPEdorItemSchema.getContNo()+"' " +
            		" and appntno='"+mLPEdorItemSchema.getInsuredNo()+"' with ur";
            String isAppnt=new ExeSQL().getOneValue(StrSQL);
            if(null==isAppnt||"".equals(isAppnt)){//非投保人即为被保人联系方式变更--BQ
            	
            	if(!BQ.FILLDATA.equals(mLPEdorItemSchema.getInsuredNo())){	//保全项目中客户号为空则认为为投保人变更
            		isInsured=true;
            		System.out.println("bq---被保险人【'"+mLPEdorItemSchema.getInsuredNo()+"'】联系方式变更");
            	}
            }
            //兼容简易保全
            String strInsuNo="select insuredno from lpedoritem " +
    			" where edorno='"+mLPEdorItemSchema.getEdorNo()+"' " +
    			" and edortype='"+mLPEdorItemSchema.getEdorType()+"' with ur";
            String customerno=new ExeSQL().getOneValue(strInsuNo);
            if(BQ.FILLDATA.equals(customerno)){
            	mLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
            }
            
            if(isInsured){
            	if (bqLPInsuredSchema == null || mLCAddressSchema == null
                        || mLPEdorItemSchema == null || mLPContSet == null)
                    {
                        CError.buildErr(this, "接收数据失败");
                        return false;
                    }
            }
            if(insuredFlag){///----------被保人联系方式变更-----
            	 if (mLPInsuredSchema == null || mLCAddressSchema == null
                         || mLPEdorItemSchema == null || mLPContSet == null)
                     {
                         CError.buildErr(this, "接收数据失败");
                         return false;
                     }
            }else{
            	 if (mLPAppntSchema == null || mLCAddressSchema == null
                         || mLPEdorItemSchema == null || mLPContSet == null)
                     {
                         CError.buildErr(this, "接收数据失败");
                         return false;
                     }
            }
           
        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData()
    {
        //处理地址编号
        if (StrTool.compareString(mLCAddressSchema.getAddressNo(), ""))
        {
            ExeSQL tExeSQL = new ExeSQL();
            String sql = "";
            if(insuredFlag){
            	sql = "  Select Case When max(int(AddressNo)) Is Null Then '1' "
                    + "Else max(int(AddressNo))  End "
                    + "from LCAddress "
                    + "where CustomerNo='"
                    + mLPInsuredSchema.getInsuredNo() + "'";
            }else{
            	if(isInsured){//--BQ
            		sql = "  Select Case When max(int(AddressNo)) Is Null Then '1' "
                        + "Else max(int(AddressNo))  End "
                        + "from LCAddress "
                        + "where CustomerNo='"
                        + bqLPInsuredSchema.getInsuredNo() + "'";
            	}else{
	            	sql = "  Select Case When max(int(AddressNo)) Is Null Then '1' "
	                    + "Else max(int(AddressNo))  End "
	                    + "from LCAddress "
	                    + "where CustomerNo='"
	                    + mLPAppntSchema.getAppntNo() + "'";
            	}
            }
            

            String tAddressNo = tExeSQL.getOneValue(sql);
            Integer firstinteger = Integer.valueOf(tAddressNo);
            int ttNo = firstinteger.intValue() + 1;

            mLCAddressSchema.setAddressNo("" + ttNo);
        }
        LPAddressSchema tLPAddressSchema = new LPAddressSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPAddressSchema, mLCAddressSchema);
        
        tLPAddressSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPAddressSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPAddressSchema.setOperator(mGlobalInput.Operator);
        tLPAddressSchema.setMakeDate(PubFun.getCurrentDate());
        tLPAddressSchema.setMakeTime(PubFun.getCurrentTime());
        tLPAddressSchema.setModifyDate(PubFun.getCurrentDate());
        tLPAddressSchema.setModifyTime(PubFun.getCurrentTime());
        mLPAddressSet.add(tLPAddressSchema);

        if(insuredFlag){//是被保人联系方式变更
        	for (int i=1;i<=mLPContSet.size();i++)
            {
                LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
                LPInsuredBL tLPInsuredBL = new LPInsuredBL();
                tLPInsuredBL.setEdorNo(mLPEdorItemSchema.getEdorNo());
                tLPInsuredBL.setContNo(mLPContSet.get(i).getContNo());
                tLPInsuredBL.setInsuredNo(mLCAddressSchema.getCustomerNo());
                tLPInsuredBL.queryLPInsured(tLPInsuredBL.getSchema());
                if (tLPInsuredBL.mErrors.needDealError())
                {
                    // @@错误处理
                    CError.buildErr(this, "查询被保人信息失败！");
                    return false;
                }
                else
                {
                	tLPInsuredSchema.setSchema(tLPInsuredBL.getSchema());
                }
                tLPInsuredSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
                tLPInsuredSchema.setEdorType(mLPEdorItemSchema.getEdorType());
                tLPInsuredSchema.setAddressNo(mLCAddressSchema.getAddressNo());
                saveLPInsuredSet.add(tLPInsuredSchema);
            }
        }else{//是投保人联系方式变更
        	if(isInsured){	//20141014添加被保险人联系方式变更--BQ
        		for (int i=1;i<=mLPContSet.size();i++)
                {
                    LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
                    LPInsuredBL tLPInsuredBL = new LPInsuredBL();
                    tLPInsuredBL.setEdorNo(mLPEdorItemSchema.getEdorNo());
                    tLPInsuredBL.setContNo(mLPContSet.get(i).getContNo());
                    tLPInsuredBL.setInsuredNo(mLCAddressSchema.getCustomerNo());
                    tLPInsuredBL.queryLPInsured(tLPInsuredBL.getSchema());
                    if (tLPInsuredBL.mErrors.needDealError())
                    {
                        // @@错误处理
                        CError.buildErr(this, "查询被保人信息失败！");
                        return false;
                    }
                    else
                    {
                    	tLPInsuredSchema.setSchema(tLPInsuredBL.getSchema());
                    }
                    tLPInsuredSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
                    tLPInsuredSchema.setEdorType(mLPEdorItemSchema.getEdorType());
                    tLPInsuredSchema.setAddressNo(mLCAddressSchema.getAddressNo());
                    bqLPInsuredSet.add(tLPInsuredSchema);
                }
        	}else{
	        	for (int i=1;i<=mLPContSet.size();i++)
	            {
	                LPAppntSchema tLPAppntSchema = new LPAppntSchema();
	                LPAppntBL tLPAppntBL = new LPAppntBL();
	                tLPAppntBL.setEdorNo(mLPEdorItemSchema.getEdorNo());
	                tLPAppntBL.setContNo(mLPContSet.get(i).getContNo());
	                tLPAppntBL.setAppntNo(mLCAddressSchema.getCustomerNo());
	                tLPAppntBL.queryLPAppnt(tLPAppntBL.getSchema());
	                if (tLPAppntBL.mErrors.needDealError())
	                {
	                    // @@错误处理
	                    CError.buildErr(this, "查询投保人信息失败！");
	                    return false;
	                }
	                else
	                {
	                    tLPAppntSchema.setSchema(tLPAppntBL.getSchema());
	                }
	                tLPAppntSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
	                tLPAppntSchema.setEdorType(mLPEdorItemSchema.getEdorType());
	                tLPAppntSchema.setAddressNo(mLCAddressSchema.getAddressNo());
	                saveLPAppntSet.add(tLPAppntSchema);
	                
	                //如果保单的投保人与被保险人是同一个客户,则将被保险人的联系地址信息一同变更
	        		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
	                tLCInsuredDB.setContNo(mLPContSet.get(1).getContNo());
	                LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
	        		if(tLCInsuredSet.size()==0){
	        			CError.buildErr(this, "查询被保险人失败！");
	                    return false;
	        		}else{
	        			for(int m=1;m<=tLCInsuredSet.size();m++){
	        				LCInsuredSchema tLCInsuredSchema=tLCInsuredSet.get(m);
	        				if(tLCInsuredSchema.getInsuredNo().equals(tLPAppntSchema.getAppntNo())){
	        					System.out.println("被保人:"+tLCInsuredSet.get(m).getInsuredNo()+",与投保人为同一客户");
	        					LPInsuredSchema tLPInsuredSchema=new LPInsuredSchema();
	        					Reflections tReflections=new Reflections();
	        					tReflections.transFields(tLPInsuredSchema, tLCInsuredSchema);
	        					tLPInsuredSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
	        					tLPInsuredSchema.setEdorType(mLPEdorItemSchema.getEdorType());
	        					tLPInsuredSchema.setAddressNo(mLCAddressSchema.getAddressNo());
	        					mMap.put(tLPInsuredSchema,"DELETE&INSERT");
	        				}
	        			}
	        		}
	            }
        	}
        }
        

        //处理保全项目状态
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
      
        tLPEdorItemDB.setEdorType("AD");
        tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        mLPEdorItemSchema = tLPEdorItemSet.get(1);
        
        mLPEdorItemSchema.setEdorState("1"); //录入完成
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setModifyDate(currDate);
        mLPEdorItemSchema.setModifyTime(currTime);

        if(insuredFlag){
        	mMap.put(saveLPInsuredSet, "DELETE&INSERT");
        }else{
        	if(isInsured){
        		mMap.put(bqLPInsuredSet, "DELETE&INSERT");
        	}else{
        		mMap.put(saveLPAppntSet, "DELETE&INSERT");
        	}
        }
		mMap.put(mLPAddressSet, "DELETE&INSERT");
		mMap.put(mLPEdorItemSchema, "UPDATE");
        mResult.clear();
        mResult.add(mMap);

        return true;
    }
}
