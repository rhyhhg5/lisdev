package com.sinosoft.lis.bq;

import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LPInsuredSet;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LBContSet;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.vschema.LBInsuredSet;
import com.sinosoft.lis.db.LBInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.schema.LBInsuredSchema;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.schema.LBPolSchema;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.vschema.LBPremSet;
import com.sinosoft.lis.db.LBPremDB;
import com.sinosoft.lis.schema.LPPremSchema;
import com.sinosoft.lis.schema.LBPremSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LPPremSet;
import com.sinosoft.lis.vschema.LPDutySet;
import com.sinosoft.lis.vschema.LBDutySet;
import com.sinosoft.lis.db.LBDutyDB;
import com.sinosoft.lis.schema.LBDutySchema;
import com.sinosoft.lis.schema.LPDutySchema;
import com.sinosoft.lis.vschema.LBGetSet;
import com.sinosoft.lis.db.LBGetDB;
import com.sinosoft.lis.schema.LBGetSchema;
import com.sinosoft.lis.schema.LPGetSchema;
import com.sinosoft.lis.vschema.LPGetSet;
import com.sinosoft.lis.vschema.LBAppntSet;
import com.sinosoft.lis.db.LBAppntDB;
import com.sinosoft.lis.schema.LBAppntSchema;
import com.sinosoft.lis.schema.LPAppntSchema;
import com.sinosoft.lis.vschema.LPAppntSet;
import com.sinosoft.lis.vschema.LBBnfSet;
import com.sinosoft.lis.db.LBBnfDB;
import com.sinosoft.lis.schema.LBBnfSchema;
import com.sinosoft.lis.schema.LPBnfSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.lis.vschema.LPBnfSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import java.sql.Time;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author fuxin
 * @version 1.0
 */
public class PEdorRBDetailBL {

    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LPInsuredSet mLPInsuredSet = new LPInsuredSet();
    private LPContSet mLPContSet = new LPContSet();
    private LPPolSet mLPPolSet = new LPPolSet();
    private LPPremSet mLPPremSet = new LPPremSet();
    private LPGetSet mLPGetSet = new LPGetSet();
    private LPDutySet mLPDutySet = new LPDutySet();
    private LPAppntSet mLPAppntSet = new LPAppntSet();
    private LPBnfSet mLPBnfSet = new LPBnfSet();


    private MMap map = new MMap();
    private Reflections ref = new Reflections();
    TransferData tempTransferData = new TransferData();
    VData  mVData = new VData();
    private String oldEdorNo = null ;

    private String currDate = PubFun.getCurrentDate();
    private String currTime = PubFun.getCurrentTime();



    public PEdorRBDetailBL() {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        //准备往后台的数据
        if (!prepareData()) {
            return false;
        }
        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, "")) { //数据提交
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("PEdorRBDetailBL End PubSubmit");
        return true;
    }

    public boolean getInputData() {
        try {
            mGlobalInput = (GlobalInput) mInputData
                           .getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData
                                .getObjectByObjectName("LPEdorItemSchema", 0);
            tempTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
            oldEdorNo =(String)tempTransferData.getValueByName("oldEdorAcceptNo");
        } catch (Exception e) {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mGlobalInput == null || mLPEdorItemSchema == null
            || oldEdorNo == null ) {
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "输入数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public boolean checkData()
    {
        if (!checkEdorItem())
        {
            return false ;
        }
        if (!checkLP())
        {
            return false ;
        }
        if (!checkBQ())
        {
            return false ;
        }
        // 判断是否有应收
        if (!chenkLjspay())
        {
            return false ;
        }
        // 判断是否是保单年度不一致的。
        if (!checkPayToDate())
        {
            return false ;
        }
        // 有续期未核销的不允许做保全回退。
        if (!checkIndiDueFeeBL())
        {
            return false ;
        }
        return true;
    }

    public boolean dealData()
    {
        String edorno = mLPEdorItemSchema.getEdorNo();
        String edortype = mLPEdorItemSchema.getEdorType();
        String sql = " edorno='" + edorno + "' and edortype='" + edortype + "'";
        map.put("delete from lpcont where" + sql, "DELETE");
        map.put("delete from lppol where" + sql, "DELETE");
        map.put("delete from lpprem where" + sql, "DELETE");
        map.put("delete from lpduty where" + sql, "DELETE");
        map.put("delete from lpget where" + sql, "DELETE");
        map.put("delete from lpappnt where" + sql, "DELETE");
        map.put("delete from lpinsured where" + sql, "DELETE");

        //备份保单信息
        LBContSet tLBContSet = getLBCcont();
        for(int i = 1; i <= tLBContSet.size() ;i++)
        {
            LBContSchema tLBContSchema = tLBContSet.get(i);
            LPContSchema tLPContSchema = new LPContSchema();
            ref.transFields(tLPContSchema,tLBContSchema);
            tLPContSchema.setOperator(mGlobalInput.Operator);
            tLPContSchema.setEdorNo(edorno);
            tLPContSchema.setEdorType(edortype);
            mLPContSet.add(tLPContSchema);

            map.put(mLPContSet,"DELETE&INSERT");
        }
        //备份险种信息
        LBPolSet tLBPolSet = getLBPol();
        if(tLBPolSet.size()==0)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
//            tError.functionName = "";
            tError.errorMessage = "没有需要回退的险种！请删除工单！";
            this.mErrors.addOneError(tError);
            return false;
        }
        for(int i = 1 ;i <= tLBPolSet.size() ; i++)
        {
            LBPolSchema tLBPolSchema = tLBPolSet.get(i);
            LPPolSchema tLPPolSchema = new LPPolSchema();
            ref.transFields(tLPPolSchema,tLBPolSchema);
            tLPPolSchema.setEdorNo(edorno);
            tLPPolSchema.setEdorType(edortype);
            tLPPolSchema.setOperator(mGlobalInput.Operator);
            mLPPolSet.add(tLPPolSchema);

            map.put(mLPPolSet,"DELETE&INSERT");
            
        }
        //备份prem中的数据到P表
        LBPremSet tLBPremSet = getLBPrem();
        for(int i=1 ; i<= tLBPremSet.size(); i++)
        {
            LBPremSchema tLBPremSchema = tLBPremSet.get(i);
            LPPremSchema tLPPremSchema = new LPPremSchema();
            ref.synchronizeFields(tLPPremSchema,tLBPremSchema);
            tLPPremSchema.setEdorNo(edorno);
            tLPPremSchema.setEdorType(edortype);
            tLPPremSchema.setOperator(mGlobalInput.Operator);
            mLPPremSet.add(tLPPremSchema);

            map.put(mLPPremSet,"DELETE&INSERT");
        }
        // 备份责任表Lpduty
        LBDutySet tLBDutySet = getLBDuty();
        for(int i=1 ; i<=tLBDutySet.size() ; i++)
        {
            LBDutySchema tLBDutySchema = tLBDutySet.get(i);
            LPDutySchema tLPDutySchema = new LPDutySchema();
            ref.synchronizeFields(tLPDutySchema,tLBDutySchema);
            tLPDutySchema.setEdorNo(edorno);
            tLPDutySchema.setEdorType(edortype);
            tLPDutySchema.setOperator(mGlobalInput.Operator);
            mLPDutySet.add(tLPDutySchema);

            map.put(mLPDutySet,"DELETE&INSERT");
        }
        // 备份给付责任表 lpget
        LBGetSet tLBGetSet = getLBGet();
        for (int i=1 ; i <= tLBGetSet.size() ; i++)
        {
            LBGetSchema  tLBGetSchema = tLBGetSet.get(i);
            LPGetSchema  tLPGetSchema = new LPGetSchema();
            ref.synchronizeFields(tLPGetSchema,tLBGetSchema);
            tLPGetSchema.setEdorNo(edorno);
            tLPGetSchema.setEdorType(edortype);
            tLPGetSchema.setOperator(mGlobalInput.Operator);
            mLPGetSet.add(tLPGetSchema);

            map.put(mLPGetSet,"DELETE&INSERT");
        }
        // 备份被保人信息
        LBInsuredSet tLBInsuredSet = getLBInsured();
        for (int i = 1; i <= tLBInsuredSet.size(); i++) {
            LBInsuredSchema tLBInsuredSchema = tLBInsuredSet.get(i);

            LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
            ref.transFields(tLPInsuredSchema, tLBInsuredSchema);
            tLPInsuredSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(tLPInsuredSchema);
            tLPInsuredSchema.setEdorNo(edorno);
            tLPInsuredSchema.setEdorType(edortype);
            mLPInsuredSet.add(tLPInsuredSchema);

            map.put(mLPInsuredSet, "DELETE&INSERT");
        }
        // 备份投保人信息
        LBAppntSet tLBAppntSet = getLBAppnt();
        for(int i = 1; i <= tLBAppntSet.size();i++)
        {
            LBAppntSchema tLBAppntSchema = tLBAppntSet.get(i);
            LPAppntSchema tLPAppntSchema = new LPAppntSchema();
            ref.synchronizeFields(tLPAppntSchema,tLBAppntSchema);
            tLPAppntSchema.setEdorNo(edorno);
            tLPAppntSchema.setEdorType(edortype);
            tLPAppntSchema.setOperator(mGlobalInput.Operator);
            mLPAppntSet.add(tLPAppntSchema);

            map.put(mLPAppntSet, "DELETE&INSERT");
        }
        // 受益人
        LBBnfSet tLBBnfSet = getLBBnf();
        for(int i=1;i<=tLBBnfSet.size(); i++)
        {
            LBBnfSchema  tLBBnfSchema = tLBBnfSet.get(i);
            LPBnfSchema  tLPBnfSchema = new LPBnfSchema();
            ref.synchronizeFields(tLPBnfSchema,tLBBnfSchema);
            tLPBnfSchema.setEdorNo(edorno);
            tLPBnfSchema.setEdorType(edortype);
            tLPBnfSchema.setOperator(mGlobalInput.Operator);
            mLPBnfSet.add(tLPBnfSchema);

            map.put(mLPBnfSet, "DELETE&INSERT");
        }
        
        if(tLBContSet.size()>0&&tLBPolSet.size()>0){
        	for(int i=1;i<=tLBContSet.size();i++){
        		//判断保单下是否有万能险或者分红险
        		if(this.hasULIRisk(tLBContSet.get(i).getContNo())||this.hasBonusRisk(tLBContSet.get(i).getContNo())){
        			//准备万能账户需要备份的表
        			String[] lPTable =
        			{
        					 "LPInsureAcc", "LPInsureAccTrace", "LPInsuredRelated", "LPPremToAcc", "LPGetToAcc", "LPInsureAccFee", "LPInsureAccClassFee", "LPInsureAccClass", "LPInsureAccFeeTrace", "LPCustomerImpart", "LPCustomerImpartParams"
        			};
        			String[] lbTable =
        			{
        					 "LBInsureAcc", "LBInsureAccTrace", "LBInsuredRelated", "LBPremToAcc", "LBGetToAcc", "LBInsureAccFee", "LBInsureAccClassFee", "LBInsureAccClass", "LBInsureAccFeeTrace", "LBCustomerImpart", "LBCustomerImpartParams"
        			};	
        			//备份准备好的万能账户相关表
        			for(int m=1;m<=tLBPolSet.size();m++){
        				for(int j = 0;j < lPTable.length; j++){
        					String tSqlIns ="";
        					String tSqlDel ="";
        					if (lPTable[j].toUpperCase().equals("LPCUSTOMERIMPART") || lPTable[j].toUpperCase().equals("LPCUSTOMERIMPARTPARAMS"))
        					{
        						String tSqlCol = GetColName(lPTable[j]);
        						if(tSqlCol == null || "".equals(tSqlCol))
        						{
        							return false;
        						}
        						tSqlDel = "delete from "  + lPTable[j] + " where EdorNo= '" + mLPEdorItemSchema.getEdorAcceptNo() + "' and ContNo in (select ContNO from LCPol where PolNo='" + tLBPolSet.get(m).getPolNo() + "') and CustomerNo in (select CustomerNo from LCBnf where PolNo='" + tLBPolSet.get(m).getPolNo() + "' ) and CustomerNoType='3'";
        						tSqlIns = "insert into " + lPTable[j] + "( EDORNO,edortype," + tSqlCol + ")" + " (select  '"+mLPEdorItemSchema.getEdorAcceptNo()+"','RB'," + tSqlCol + " from " + lbTable[j] + " where EdorNo= '" + oldEdorNo + "' and ContNo in (select ContNO from LCPol where PolNo='" + tLBPolSet.get(m).getPolNo() + "') and CustomerNo in (select CustomerNo from LCBnf where PolNo='" + tLBPolSet.get(m).getPolNo() + "' ) and CustomerNoType='3')";
        						map.put(tSqlDel, "DELETE");
        						map.put(tSqlIns, "INSERT");
        					}else if(lPTable[j].toUpperCase().equals("LPINSUREACCTRACE") || lPTable[j].toUpperCase().equals("LPINSUREACCFEETRACE")){
        						String tSqlCol = GetColName(lPTable[j]);
        						if(tSqlCol == null || "".equals(tSqlCol))
        						{
        							return false;
        						}
        						tSqlDel = "delete from " + lPTable[j] + " where EdorNo= '" + mLPEdorItemSchema.getEdorAcceptNo() + "' and polno='" + tLBPolSet.get(m).getPolNo() + "'";
        						tSqlIns = "insert into " + lPTable[j] + "( EDORNO,edortype," + tSqlCol + ")" + " (select '"+mLPEdorItemSchema.getEdorAcceptNo()+"','RB'," + tSqlCol + " from " + lbTable[j] + " where EdorNo= '" + oldEdorNo + "' and polno='" + tLBPolSet.get(m).getPolNo() + "' and otherno= '"+oldEdorNo+"')";
        						map.put(tSqlDel, "DELETE");
        						map.put(tSqlIns, "INSERT");
        					}
        					else
        					{
        						String tSqlCol = GetColName(lPTable[j]);
        						if(tSqlCol == null || "".equals(tSqlCol))
        						{
        							return false;
        						}
        						tSqlDel = "delete from " + lPTable[j] + " where EdorNo= '" + mLPEdorItemSchema.getEdorAcceptNo() + "' and polno='" + tLBPolSet.get(m).getPolNo() + "'";
        						tSqlIns = "insert into " + lPTable[j] + "( EDORNO,edortype," + tSqlCol + ")" + " (select '"+mLPEdorItemSchema.getEdorAcceptNo()+"','RB'," + tSqlCol + " from " + lbTable[j] + " where EdorNo= '" + oldEdorNo + "' and polno='" + tLBPolSet.get(m).getPolNo() + "' )";
        						map.put(tSqlDel, "DELETE");
        						map.put(tSqlIns, "INSERT");
        					}
        				}
        				
        			}
        		}
        		
        	}
        }
        
        setLGworkInnersource();
        mLPEdorItemSchema.setEdorState("1");
        mLPEdorItemSchema.setModifyDate(currDate);
        mLPEdorItemSchema.setModifyTime(currTime);
        map.put(mLPEdorItemSchema, "UPDATE");
               
        return true ;
    }

	private String GetColName(String pTableName)
	{
		String tSqlCol = "select colname from SYSCAT.COLUMNS where tabname='" + pTableName.toUpperCase() + "' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(tSqlCol);
		if (tSSRS == null || tSSRS.getMaxRow() == 0)
		{
			System.out.println("取得表[" + pTableName + "]的列名失败！");
			return null;
		}
		String tSql="";
		for (int k = 1; k <= tSSRS.getMaxRow(); k++)
		{
			if (k == 1)
			{
				if(tSSRS.GetText(k, 1).toUpperCase().equals("EDORTYPE")||tSSRS.GetText(k, 1).toUpperCase().equals("EDORNO")){
					continue;
				}else{
					tSql = tSql + tSSRS.GetText(k, 1);
				}
			}
			else
			{
				if(tSSRS.GetText(k, 1).toUpperCase().equals("EDORTYPE")||tSSRS.GetText(k, 1).toUpperCase().equals("EDORNO")){
					continue;
				}else if(tSql !=null&&!"".equals(tSql)){
				tSql = tSql + "," + tSSRS.GetText(k, 1);
				}else{
					tSql = tSql + tSSRS.GetText(k, 1);
				}
			}
		}
		return tSql;
	}
    
    public boolean prepareData()
    {
        mResult.clear();
//        tempTransferData.setNameAndValue("oldEdorNo",oldEdorNo);
//        mResult.add(tempTransferData);
        mResult.add(map);
        return true ;
    }
    
    /**
     * 判断保单下是否含有万能险种
     * @return boolean
     */
    public boolean hasULIRisk(String ContNo)
    {
        LBPolDB tLBPolDB = new LBPolDB();
        tLBPolDB.setContNo(ContNo);
        LBPolSet tLBPolSet = tLBPolDB.query();
        for (int i = 1; i <= tLBPolSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLBPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                return false;
            }
            String riskType4 = tLMRiskAppDB.getRiskType4();
            if ((riskType4 != null) && (riskType4.equals(BQ.RISKTYPE1_ULI)))
            {
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * 判断保单下是否含有分红险种
     * @return boolean
     */
    public boolean hasBonusRisk(String ContNo)
    {
    	LBPolDB tLBPolDB = new LBPolDB();
        tLBPolDB.setContNo(ContNo);
        LBPolSet tLBPolSet = tLBPolDB.query();
        for (int i = 1; i <= tLBPolSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLBPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                return false;
            }
            String riskType4 = tLMRiskAppDB.getRiskType4();
            if ((riskType4 != null) && (riskType4.equals(BQ.RISKTYPE4_BONUS)))
            {
                return true;
            }
        }
        return false;
    }


    private boolean checkEdorItem()
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();

        if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "输入数据有误,LPEdorItem中没有相关数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLPEdorItemSchema = tLPEdorItemSet.get(1);
        mLPEdorItemSchema.setEdorState("1");

        return true ;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 得到被保人信息
     * @return LCInsuredSet
     */
    private LBInsuredSet getLBInsured() {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LBInsured ")
                .append("where EdorNo = '")
                .append(oldEdorNo)
                .append("' ");
        System.out.println(sql.toString());

        LBInsuredDB tLBInsuredDB = new LBInsuredDB();
        LBInsuredSet tLBInsuredSet = tLBInsuredDB.executeQuery(sql.toString());

        return tLBInsuredSet;
    }
    /**
     * 得到保单信息
     */
    private LBContSet getLBCcont()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LBCont ")
                .append("where EdorNo = '")
                .append(oldEdorNo)
                .append("' ");
        System.out.println(sql.toString());
        LBContDB tLBContDB = new LBContDB();
        LBContSet tLBContSet = tLBContDB.executeQuery(sql.toString());
        return tLBContSet;
    }

    /**
     * 得到险种信息
     */
    private LBPolSet getLBPol()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LBPol ")
                .append("where EdorNo = '")
                .append(oldEdorNo)
                .append("' ");
        System.out.println(sql.toString());
        LBPolDB tLBPolDB = new LBPolDB();
        LBPolSet tLBPolSet = tLBPolDB.executeQuery(sql.toString());
        return tLBPolSet ;
    }
    /**
     * Prem 表
     * @return LBPremSet
     */
    private LBPremSet getLBPrem()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LBPrem ")
                .append("where EdorNo = '")
                .append(oldEdorNo)
                .append("' ");
        System.out.println(sql.toString());
        LBPremDB tLBPremDB = new LBPremDB();
        LBPremSet tLBPremSet = tLBPremDB.executeQuery(sql.toString());
        return tLBPremSet;
    }
    /**
     *
     */
    private LBDutySet getLBDuty()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LBDuty ")
                .append("where EdorNo = '")
                .append(oldEdorNo)
                .append("' ");
        System.out.println(sql.toString());
        LBDutyDB tLBDutyDB = new LBDutyDB();
        LBDutySet tLBDutySet = tLBDutyDB.executeQuery(sql.toString());
        return tLBDutySet ;
    }
    /**
     * lbget信息
     * @return LBGetSet
     */
    private LBGetSet getLBGet()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LBGet ")
                .append("where EdorNo = '")
                .append(oldEdorNo)
                .append("' ");
        System.out.println(sql.toString());
        LBGetDB tLBGetDB =new LBGetDB();
        LBGetSet tLBGetSet = tLBGetDB.executeQuery(sql.toString());
        return tLBGetSet;
    }
    private LBAppntSet getLBAppnt()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LBAppnt ")
                .append("where EdorNo = '")
                .append(oldEdorNo)
                .append("' ");
        System.out.println(sql.toString());
        LBAppntDB tLBAppntDB = new LBAppntDB();
        LBAppntSet  tLBAppntSet = tLBAppntDB.executeQuery(sql.toString());
        return tLBAppntSet ;
    }
    private LBBnfSet getLBBnf()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LBBnf ")
                .append("where EdorNo = '")
                .append(oldEdorNo)
                .append("' ");
        System.out.println(sql.toString());
        LBBnfDB tLBBnfDB = new LBBnfDB();
        LBBnfSet tLBBnfSet = tLBBnfDB.executeQuery(sql.toString());
        return tLBBnfSet;
    }
    /**
     * 判断在保全生日前是否发生了理赔，如果有理赔不能做回退。
     */
    private boolean checkLP()
    {
    	//2011-07-19 修改关于保全后又有理赔不可以进行退保回退的校验
        FDate tFDate = new FDate();
        //1.先查询解约日期
        String sql = " select a.Edorappdate from lpedorapp a,lpedoritem b where a.edoracceptno=b.edoracceptno "
//        	         + " b.edortype in ('CT','WT','XT') "
                     + " and b.edoracceptno ='"
                     +oldEdorNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        Date mEdorappdate = tFDate.getDate(tExeSQL.getOneValue(sql));
        //2.如果解约日期为空，则报错
        if(mEdorappdate==null||mEdorappdate.equals(null))
        {
        	CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "checkLP";
            tError.errorMessage = "没有查寻到解约日期。";
            this.mErrors.addOneError(tError);
            return false;
        }
        //3.然后查询理赔的日期
        String sqlLp =" select max(rgtdate) From llcase where customerno in(select distinct insuredno from lbpol where edorno ='"+oldEdorNo+"') and Rgtstate not in('14')";
        String dateLP = tExeSQL.getOneValue(sqlLp);

        if (dateLP == null || dateLP .equals(""))
        {
            return true ;
        }
        Date mrgtdate = tFDate.getDate(dateLP);
        //4.判断是否是解约当天进行进行的理赔，如果是，则进一步比较完成理赔的时间        
        if(mEdorappdate.compareTo(mrgtdate)==0)
        {
            String sqltime = " select a.modifytime from lpedorapp a,lpedoritem b where a.edoracceptno=b.edoracceptno "
//   	         + " b.edortype in ('CT','WT','XT') "
                + " and b.edoracceptno ='"
                +oldEdorNo+ "'";
            System.out.println(sqltime);
            
            Time edorTime = Time.valueOf(tExeSQL.getOneValue(sqltime));
            String sqlLptime =" select max(modifytime) From llcase where customerno in(select distinct insuredno from lbpol where edorno ='"+oldEdorNo+"') " +
            		" and modifydate = '"+ dateLP +"' and  Rgtstate not in('14')";
            System.out.println(sqlLptime);
            String checktimeIfnull=tExeSQL.getOneValue(sqlLptime);
            
            if(!checktimeIfnull.equals(""))
            {
            		Time Lptime = Time.valueOf(tExeSQL.getOneValue(sqlLptime));
            		/*
                     * public boolean after(Timestamp ts)Indicates whether this Timestamp object is later than the given Timestamp object. 

                       Parameters:
                       ts - the Timestamp value to compare with 
                       Returns:
                       true if this Timestamp object is later; false otherwise

                     */
                    //5.判断时间先后
                    if(edorTime.before(Lptime))
                    {
                        CError tError = new CError();
                        tError.moduleName = "PEdorRBDetailBL";
                        tError.functionName = "checkLP";
                        tError.errorMessage = "已经发生理赔不可以做保全回退。";
                        this.mErrors.addOneError(tError);
                        return false;

                    }
            }
            
        }
        /*
         * public boolean after(Date when)Tests if this date is after the specified date. 

           Parameters:
           when - a date. 
           Returns:
           true if and only if the instant represented by this Date object is strictly later than the instant represented by when; false otherwise. 
           Throws: 
           NullPointerException - if when is null.

         */
        //6.判断日期先后
        if(mEdorappdate.before(mrgtdate))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "checkLP";
            tError.errorMessage = "已经发生理赔不可以做保全回退。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 校验是否有应收
     * @return boolean
     */
    private boolean chenkLjspay()
    {
        String sql = " select 1 From ljspay where othernotype='2' and  otherno ='"+mLPEdorItemSchema.getContNo()+"'";
        ExeSQL tExeSQL = new ExeSQL();
        if(tExeSQL.execSQL(sql).getMaxRow()>0)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "chenkLjspay";
            tError.errorMessage = "保单有应收记录不可以做保全回退！";
            this.mErrors.addOneError(tError);
            return false ;
        }
        return true ;
    }
    /**
     * 在lgwork中存入上一次退保的工单号。
     */
    private void setLGworkInnersource()
    {
        String update =" update lgwork set innersource ='"+oldEdorNo+"'"
                      +" ,modifydate = '"+currDate+"'"
                      +", modifytime = '"+currTime +"' where workno ='"+mLPEdorItemSchema.getEdorAcceptNo()+"'"
                      ;
       System.out.println(update);
       map.put(update,"UPDATE");
    }

    /**
     * 校验解约以后是否有保全项目，如果有保全项目，不允许做保全回退。
     */
    private boolean checkBQ()
    {
        FDate tFDate = new FDate();
        // 解约类型的最后一次的结案时间
        String sql1 = " select max(b.Confdate) from lpedoritem a,lpedorapp b where contno ='"+mLPEdorItemSchema.getContNo()+"' and a.edoracceptno = b.edoracceptno and edortype in('CT','WT','XT')";
        // 除解约和回退类型的最后一次结案时间
        String sql2 = " select max(b.Confdate) from lpedoritem a,lpedorapp b where contno ='"+mLPEdorItemSchema.getContNo()+"' and a.edoracceptno = b.edoracceptno and edortype not in('CT','WT','XT','RB')";
        ExeSQL tExeSQL = new ExeSQL();

        Date D1  = tFDate.getDate(tExeSQL.getOneValue(sql1));
        Date D2  = tFDate.getDate(tExeSQL.getOneValue(sql2));

        if( D1 ==null || D2== null)
        {
            return true ;
        }

        if(D1.compareTo(D2)==0)
        {
            // 解约类型的最后一次的结案时间
            String sql3 =
                    " select max(b.modifytime) from lpedoritem a,lpedorapp b where contno ='" +
                    mLPEdorItemSchema.getContNo() +
                    "' and a.edoracceptno = b.edoracceptno and edortype in('CT','WT','XT')";
            // 除解约和回退类型的最后一次结案时间
            String sql4 =
                    " select b.modifytime from lpedoritem a,lpedorapp b where contno ='" +
                    mLPEdorItemSchema.getContNo() +
                    "' and a.edoracceptno = b.edoracceptno and edortype not in('CT','WT','XT','RB') order by b.confdate desc fetch first 1 row only";
            Time tTime = new Time(100);

             Time D3 = tTime.valueOf(tExeSQL.getOneValue(sql3));
             Time D4 = tTime.valueOf(tExeSQL.getOneValue(sql4));
             if(D4.after(D3))
             {
                 CError tError = new CError();
                 tError.moduleName = "PEdorRBDetailBL";
                 tError.functionName = "checkBQ";
                 tError.errorMessage = "在解约项目以后发生了保全项目，不允许做保全回退！";
                 this.mErrors.addOneError(tError);
                 return false;
             }
        }

        if (D2.after(D1) )
        {
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "checkBQ";
            tError.errorMessage = "在解约项目以后发生了保全项目，不允许做保全回退！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true ;
    }

    private boolean checkPayToDate()
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tPayToDate =tExeSQL.getOneValue(" select PayToDate from lcpol where contno='"+mLPEdorItemSchema.getContNo()+"'");
        String oPayToDate =tExeSQL.getOneValue(" select PayToDate from lbpol where contno='"+mLPEdorItemSchema.getContNo()+"'");

        if(tPayToDate== null || tPayToDate.equals(""))
        {
            return true ;
        }
        if(!tPayToDate.equals(oPayToDate))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "checkPayToDate";
            tError.errorMessage = "B表和C表不属于同一个保单年度不可以做保全回退！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true ;
    }

    private boolean checkIndiDueFeeBL()
    {
        String sql = " select dealstate From ljspayb where othernotype='2' and  otherno ='" +mLPEdorItemSchema.getContNo() + "'";
        ExeSQL tExeSQL = new ExeSQL();

        if ("4".equals(tExeSQL.getOneValue(sql))) {
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "chenkLjspay";
            tError.errorMessage = "保单有待核销记录，不可以做保全回退！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    

}
