package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.task.*;

public class FinishPhoneHastenBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mReason = null;

    public FinishPhoneHastenBL(GlobalInput gi, String edorNo, String reason)
    {
        this.mEdorNo = edorNo;
        this.mGlobalInput = gi;
        this.mReason = reason;
    }

    public MMap getSubmitData()
    {
        if (!dealData())
        {
            return null;
        }
        return mMap;
    }

    /**
     * 提交数据
     * @param data VData
     * @return boolean
     */
    public boolean submitData()
    {
        if (getSubmitData() == null)
        {
            return false;
        }
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 校验传入的数据的合法性
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return boolean 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //处理的保全必须处于待收费状态
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        if (!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("查询保全LPEdorApp表出错！");
            return false;
        }
        String edorState = tLPEdorAppDB.getEdorState();
        if ((edorState != null) && (edorState.equals(BQ.EDORSTATE_WAITPAY)))
        {
            LGPhoneHastenDB tLGPhoneHastenDB = new LGPhoneHastenDB();
            tLGPhoneHastenDB.setEdorAcceptNo(mEdorNo);
            LGPhoneHastenSet tLGPhoneHastenSet = tLGPhoneHastenDB.query();
            if (tLGPhoneHastenSet.size() > 0)
            {
                //设置工单状态和作业历史
                String workNo = tLGPhoneHastenSet.get(1).getWorkNo();
                VData data = new VData();
                LGWorkSchema tLGWorkSchema = new LGWorkSchema();
                LGWorkRemarkSchema tLGWorkRemarkSchema = new LGWorkRemarkSchema();
                tLGWorkSchema.setWorkNo(workNo);
                tLGWorkRemarkSchema.setRemarkContent(mReason);

                data.add(tLGWorkSchema);
                data.add(tLGWorkRemarkSchema);
                data.add(mGlobalInput);

                TaskFinishBL tTaskFinishBL = new TaskFinishBL();
                MMap map = tTaskFinishBL.getSubmitData(data, "");
                if (map == null)
                {
                    mErrors.copyAllErrors(tTaskFinishBL.mErrors);
                    return false;
                }
                mMap.add(map);
            }
        }
        return true;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

}
