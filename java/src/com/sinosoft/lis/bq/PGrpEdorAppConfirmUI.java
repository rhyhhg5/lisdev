package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:保全申请确认功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PGrpEdorAppConfirmUI
{
    private PGrpEdorAppConfirmBL mPGrpEdorAppConfirmBL = null;

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mPGrpEdorAppConfirmBL = new PGrpEdorAppConfirmBL();
        if (!mPGrpEdorAppConfirmBL.submitData(cInputData, cOperate))
        {
            return false;
        }

        return true;
    }

    public CErrors getError()
    {
        return mPGrpEdorAppConfirmBL.mErrors;
    }

    public static void main(String[] args)
    {
        LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
        tLPGrpEdorMainSchema.setGrpContNo("0000001301"); // 86330020030220000012
        tLPGrpEdorMainSchema.setEdorAcceptNo("20070207000004");
        tLPGrpEdorMainSchema.setEdorNo(tLPGrpEdorMainSchema.getEdorAcceptNo());
        tLPGrpEdorMainSchema.setEdorValiDate(PubFun.getCurrentDate());
        tLPGrpEdorMainSchema.setEdorAppDate(PubFun.getCurrentDate());
        PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI = new PGrpEdorAppConfirmUI();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "endor0";
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";

        VData tVData = new VData();
        tVData.addElement(tLPGrpEdorMainSchema);
        tVData.addElement(tGlobalInput);
        if(!tPGrpEdorAppConfirmUI.submitData(tVData, "INSERT||EDORAPPCONFIRM"))
        {
            System.out.println(tPGrpEdorAppConfirmUI.getError().getFirstError());
        }

    }
}
