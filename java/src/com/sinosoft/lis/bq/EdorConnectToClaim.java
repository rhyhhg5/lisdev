package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import java.util.ArrayList;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 与理赔的交互类
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class EdorConnectToClaim
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    /**存储与理赔交互所需要的数据*/
    private TransferData mTransferData = null;

    private ArrayList claimedPolNoArrayList = new ArrayList();

    private LCPolSet claimedLCPolSet = null;

    private GlobalInput mGlobalInput = null;

    public EdorConnectToClaim()
    {
    }

    public EdorConnectToClaim(TransferData tTransferData,
                              GlobalInput tGlobalInput)
    {
        mTransferData = tTransferData;
        mGlobalInput = tGlobalInput;
    }

    /**
     * 得到团单的理赔状态
     * 0 非理赔中，1 理赔中
     * @return String
     */
    public String getGrpPolClaimState()
    {
        String grpPolNo = (String) mTransferData.getValueByName("grpPolNo");

        GCheckClaimBL tGCheckClaimBL = new GCheckClaimBL();

        claimedLCPolSet = tGCheckClaimBL.getLCPolClaimed(grpPolNo);
        for(int i = 1; i <= claimedLCPolSet.size(); i++)
        {
            claimedPolNoArrayList.add(claimedLCPolSet.get(i).getPolNo());
        }

        return claimedLCPolSet.size() == 0 ? "0" : "1";
    }

    /**
     * 得到已发生理赔的险种号
     * @return ArrayList
     */
    public ArrayList getClaimedPolNo()
    {
        return claimedPolNoArrayList;
    }

    /**
     * 发生理赔的险种信息
     * @return LCPolSet
     */
    public LCPolSet getClaimedLCPolSet()
    {
        return claimedLCPolSet;
    }

    public static void main(String[] args)
    {
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("calFlag", "1");
        tTransferData.setNameAndValue("grpPolNo", "2200005626");

        EdorConnectToClaim edorconnecttoclaim =
            new EdorConnectToClaim(tTransferData, new GlobalInput());
        String state = edorconnecttoclaim.getGrpPolClaimState();
    }
}










