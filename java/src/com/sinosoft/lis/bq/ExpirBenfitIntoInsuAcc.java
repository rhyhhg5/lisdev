package com.sinosoft.lis.bq;

import com.cbsws.obj.EdorAppInfo;
import com.cbsws.obj.EdorItemInfo;
import com.cbsws.obj.PayInfo;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPInsuredSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.task.TaskAutoFinishBL;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/***
 * describtion:满期金转入附加万能账户
 * date： 20160713
 * @author lzy
 *
 */
public class ExpirBenfitIntoInsuAcc {

	private GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	private String mEdorAcceptNo;	//工单受理号
	private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
	private LCContSchema mLCContSchema;
	public CErrors mErrors = new CErrors();
	private LCPolSchema mLCPolSchema = new LCPolSchema();
	
	private String mMoney = null;
	
	private String mEdorvalidate = null;
	
	private String mGetNoticeno = null;
	
	private String mCurrDate = PubFun.getCurrentDate();
	private String mCurrTime = PubFun.getCurrentTime();

	public ExpirBenfitIntoInsuAcc() {
	}

	public ExpirBenfitIntoInsuAcc(GlobalInput tGlobalInput, EdorItemInfo tEdorItemInfo,
			LCContSchema tLCContSchema) {
		this.mGlobalInput = tGlobalInput;
		this.mLCContSchema = tLCContSchema;
	}

	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		mLCPolSchema = (LCPolSchema) cInputData.getObjectByObjectName(
				"LCPolSchema", 0);
		mLCContSchema = (LCContSchema) cInputData.getObjectByObjectName(
				"LCContSchema", 0);
		TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName(
				"TransferData", 0);
		if(null == tTransferData){
			mErrors.addOneError("传入参数信息错误");
			return false;
		}
		mMoney = (String)tTransferData.getValueByName("Money");
		mEdorvalidate = (String)tTransferData.getValueByName("EdorValidate");
		mGetNoticeno = (String)tTransferData.getValueByName("GetNoticeno");
		if(null == mMoney || "".equals(mMoney) || null == mEdorvalidate || "".equals(mEdorvalidate)){
			mErrors.addOneError("传入参数信息缺失");
			return false;
		}

		if (mGlobalInput == null) {
			mErrors.addOneError("传入参数信息错误");
			return false;
		}
		return true;
	}

	public boolean submitData(VData cInputData) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!deal()) {
			return false;
		}

		return true;
	}

	private boolean deal() {
		
		try {
			// 申请工单
			if (!createWorkNo()) {
				return false;
			}
			// 添加保全
			if (!addEdorItem()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORAPP")) {
					return false;
				}
				return false;
			}
	
			// 录入明细
			if (!saveDetail()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}

			// 理算确认
			if (!appConfirm()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}
	
			if(!creatPrintVts()){
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}

			// 保全确认
			if (!edorConfirm()) {
				// 撤销工单
				if (!cancelEdorItem("1", "I&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "I&EDORAPP")) {
						return false;
					}
				}
				return false;
			}
		} catch (Exception e) {
			System.out.println("附加万能追加保费错误：撤销工单+"+e.getMessage());
			mErrors.addOneError("保全处理异常，请求失败。");
			cancelEdorItem("1", "I&EDORMAIN");
			cancelEdorItem("1", "I&EDORAPP");
			return false;
		}

		// 修改批单以及财务数据
		if (!endEdor()) {
			return false;
		}
		//添加账户是否需要补计息的标记
		setBalaFlag();

		return true;
	}

	public LPEdorItemSchema getEdorItem() {
		return mLPEdorItemSchema;
	}
	
	private boolean setBalaFlag(){
		//满期金应进入账户日期晚于账户结算日则不需要补计利息的， 加个标记
		String strsql = "select baladate from lpinsureacc where edorno='"+mEdorAcceptNo+"' "
						+ " and edortype='ZB' and baladate<='"+mEdorvalidate+"' ";
		String mBalaDate = new ExeSQL().getOneValue(strsql);
		if(null != mBalaDate && !"".equals(mBalaDate)){
			LPEdorEspecialDataSchema tEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
			tEdorEspecialDataSchema.setEdorAcceptNo(mEdorAcceptNo);
			tEdorEspecialDataSchema.setEdorNo(mEdorAcceptNo);
			tEdorEspecialDataSchema.setEdorType("ZB");
			tEdorEspecialDataSchema.setDetailType("BALASTATE");
			tEdorEspecialDataSchema.setEdorValue("0");
			tEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
			MMap tMMap =new MMap();
			tMMap.put(tEdorEspecialDataSchema, SysConst.DELETE_AND_INSERT);
			if (!submit(tMMap)) {
				return false;
			}
		}
		return true;
	}

	private boolean cancelEdorItem(String edorstate, String transact) {
		PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
		LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
		LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
		TransferData tTransferData = new TransferData();
		VData tVData = new VData();
		tVData.addElement(mGlobalInput);

		if ("I&EDORAPP".equals(transact)) {
			tLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);
			tLPEdorAppSchema.setEdorState(edorstate);

			String delReason = "";
			String reasonCode = "002";

			tTransferData.setNameAndValue("DelReason", delReason);
			tTransferData.setNameAndValue("ReasonCode", reasonCode);
			tVData.addElement(tLPEdorAppSchema);
			// 准备传输数据 VData
			tVData.addElement(tTransferData);
		} else if ("I&EDORMAIN".equals(transact)) {
			tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
			tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
			tLPEdorMainSchema.setEdorState(edorstate);
			tLPEdorMainSchema.setContNo(mLCContSchema.getContNo());
			String delReason = "";
			String reasonCode = "002";
			System.out.println(delReason);

			tTransferData.setNameAndValue("DelReason", delReason);
			tTransferData.setNameAndValue("ReasonCode", reasonCode);
			tVData.addElement(tLPEdorMainSchema);
			tVData.addElement(tTransferData);
		}
		try {
			// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			System.out.println("hello");
			tPGrpEdorCancelUI.submitData(tVData, transact);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mErrors.addOneError("撤销工单失败" + e);
			return false;
		}
		return true;
	}

	private boolean createWorkNo() {

		// 得到工单信息
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(mLCContSchema.getAppntNo());
		tLGWorkSchema.setTypeNo("0300017");//个单万能追加保费
		tLGWorkSchema.setContNo(mLCContSchema.getContNo());
		tLGWorkSchema.setApplyTypeNo("0");
		tLGWorkSchema.setAcceptWayNo("0");
		tLGWorkSchema.setAcceptDate(mCurrDate);
		tLGWorkSchema.setInnerSource(mGetNoticeno);
		tLGWorkSchema.setRemark("附加万能自动追加保费工单");

		VData data = new VData();
		GlobalInput tGlobalInput = mGlobalInput;
		//设置一虚拟的信箱
		String workBoxNo="000000";
		data.add(tGlobalInput);
		data.add(tLGWorkSchema);
		data.add(workBoxNo);
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (!tTaskInputBL.submitData(data, "")) {
			mErrors.addOneError("生成保全工单失败" + tTaskInputBL.mErrors);
			return false;
		}
		mEdorAcceptNo = tTaskInputBL.getWorkNo();

		return true;
	}

	private boolean addEdorItem() {
//		if (!checkEdorItem()) {
//			return false;
//		}
		mLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
//		mLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
//		mLPEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
		mLPEdorItemSchema.setDisplayType("1");
		mLPEdorItemSchema.setEdorType(BQ.EDORTYPE_ZB);
		mLPEdorItemSchema.setGrpContNo(BQ.GRPFILLDATA);
		mLPEdorItemSchema.setContNo(mLCContSchema.getContNo());
		mLPEdorItemSchema.setEdorState("3");
		mLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
		mLPEdorItemSchema.setPolNo(mLCPolSchema.getPolNo());
		mLPEdorItemSchema.setManageCom(mLCContSchema.getManageCom());
		mLPEdorItemSchema.setEdorValiDate(mEdorvalidate);
		mLPEdorItemSchema.setEdorAppDate(mCurrDate);
		mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
		mLPEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
		mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
		mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
		PEdorAppItemUI tPEdorAppItemUI=new PEdorAppItemUI();
		LPEdorItemSet mLPEdorItemSet=new LPEdorItemSet();
		mLPEdorItemSet.add(mLPEdorItemSchema);
		
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("DisplayType","1");
		VData tVData = new VData();
		//后台还有查用户
		mGlobalInput.Operator="it001";
        tVData.add(mLPEdorItemSet);
        tVData.add(tTransferData);
        tVData.add(mGlobalInput);
        
		if (!tPEdorAppItemUI.submitData(tVData,"INSERT||EDORITEM"))
        {
			mErrors.addOneError("生成保全工单失败" + tPEdorAppItemUI.mErrors.getFirstError());
			return false;
        }
		return true;
	}

	private boolean saveDetail() {
		MMap map = new MMap();

		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPEdorEspecialDataSchema.setEdorNo(mEdorAcceptNo);
		tLPEdorEspecialDataSchema.setEdorType(BQ.EDORTYPE_ZB);
		tLPEdorEspecialDataSchema.setDetailType("ZF");
		tLPEdorEspecialDataSchema.setEdorValue(mMoney);
		tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
		
		mLPEdorItemSchema.setEdorState("1");
		
		map.put(tLPEdorEspecialDataSchema, SysConst.DELETE_AND_INSERT);
		map.put(mLPEdorItemSchema, SysConst.DELETE_AND_INSERT);
		// 提交数据库
		if (!submit(map)) {
			return false;
		}
		return true;
	}

	/**
	 * 产生打印数据
	 * 
	 * @return boolean
	 */
	private boolean creatPrintVts() {
		// 生成打印数据
		VData data = new VData();
		data.add(mGlobalInput);
		PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
				mEdorAcceptNo);
		if (!tPrtAppEndorsementBL.submitData(data, "")) {
			mErrors.addOneError("生成保全服务批单失败！");
			return false;
		}
		return true;
	}

	private boolean appConfirm() {
		
		PEdorAppConfirmUI tPEdorAppConfirmUI = new PEdorAppConfirmUI(mGlobalInput, mEdorAcceptNo);
	    if (!tPEdorAppConfirmUI.submitData())
	    {
	    	mErrors.addOneError("保全理算失败：" + tPEdorAppConfirmUI.getError());
			return false;
	    }
	    
		return true;
	}

	/**
	 * 查询险种polNo发生的加费和
	 * 
	 * @param polNo
	 *            String
	 * @return double
	 */
	private double getSumAddFee(String polNo) {
		String sql = "select sum(SumActuPayMoney) " + "from LJAPayPerson "
				+ "where payPlanCode like '000000%' " + "    and  polNo = '"
				+ polNo + "' ";
		ExeSQL e = new ExeSQL();
		String sumActuPayMoney = e.getOneValue(sql);

		return sumActuPayMoney.equals("") ? 0 : Double
				.parseDouble(sumActuPayMoney);
	}

	private boolean checkEdorItem() {
		String mContNo=mLCContSchema.getContNo();
		//正在操作保全的无法添加犹豫期退保
		String edorSQL = " select edoracceptno from lpedoritem where contno='"
				+ mContNo
				+ "' and edoracceptno!='"+ mEdorAcceptNo+ "' "
				+ " and exists (select 1 from lpedorapp where edoracceptno=lpedoritem.edoracceptno and edorstate!='0') with ur  ";
		ExeSQL tExeSQL = new ExeSQL();
		String edorFlag = tExeSQL.getOneValue(edorSQL);
		if (edorFlag != null && !"".equals(edorFlag)) {
			mErrors.addOneError("保单" + mContNo
					+ "正在操作保全，工单号为：" + edorFlag + "无法做解约");
			return false;
		}
		
		return true;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.addOneError("提交数据库发生错误" + tPubSubmit.mErrors);
			return false;
		}
		return true;
	}

	/**
	 * 保全确认
	 * 
	 * @param edorAcceptNo
	 *            String
	 * @return boolean
	 */
	private boolean edorConfirm() {
		MMap map = new MMap();

		// 生成财务应收数据
		FinanceDataBL tFinanceDataBL = new FinanceDataBL(mGlobalInput,
				mEdorAcceptNo, BQ.NOTICETYPE_P, "");
		if (!tFinanceDataBL.submitData()) {
			mErrors.addOneError("生成财务数据错误" + tFinanceDataBL.mErrors);
			return false;
		}
		//生成财务收费数据
		LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(mEdorAcceptNo);
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if(null == tLJSPaySet || tLJSPaySet.size()==0){
        	mErrors.addOneError("保全结案发生错误" + tFinanceDataBL.mErrors);
			return false;
        }
        //生成暂收数据
        MMap tempMap = setTempInfo(tLJSPaySet.get(1));

		// 个人保全结案
		PEdorConfirmBL tPEdorConfirmBL = new PEdorConfirmBL(mGlobalInput,
				mEdorAcceptNo);
		map = tPEdorConfirmBL.getSubmitData();
		if (null == map) {
			mErrors.addOneError("保全结案发生错误" + tFinanceDataBL.mErrors);
			return false;
		}

		// 工单结案
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
		tLGWorkSchema.setTypeNo("03"); // 结案状态

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
		MMap tmap = tTaskAutoFinishBL.getSubmitData(data, "");
		if (null == tmap) {
			mErrors.addOneError("工单结案失败" + tFinanceDataBL.mErrors);
			return false;
		}
		map.add(tmap);
		map.add(tempMap);
		if (!submit(map)) {
			return false;
		}
		return true;
	}

	private boolean endEdor() {
		
		System.out.println("交退费通知书" + mEdorAcceptNo);
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("payMode", "5");//内部流转
		tTransferData.setNameAndValue("endDate", "");
		tTransferData.setNameAndValue("payDate", mCurrDate);
		tTransferData.setNameAndValue("chkYesNo", "no");

		// 生成交退费通知书
		FeeNoticeVtsUI tFeeNoticeVtsUI = new FeeNoticeVtsUI(mEdorAcceptNo);
		if (!tFeeNoticeVtsUI.submitData(tTransferData)) {
			mErrors.addOneError("生成批单失败！原因是：" + tFeeNoticeVtsUI.getError());
		}

//		VData data = new VData();
//		data.add(mGlobalInput);
//		data.add(tTransferData);
//		SetPayInfo spi = new SetPayInfo(mEdorAcceptNo);
//		if (!spi.submitDate(data, "1")) {
//			System.out.println("设置收退费方式失败！");
//			mErrors.addOneError("设置收退费方式失败！原因是：" + spi.mErrors.getFirstError());
//		}

		return true;
	}
	
	/**
     * 设置暂交费表
     * @param map MMap
     * @param cLJSPaySchema LJSPaySchema
     */
    private MMap setTempInfo(LJSPaySchema cLJSPaySchema)
    {
    	MMap mMap = new MMap();
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeDB.setOtherNo(mEdorAcceptNo);
        LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();

        //生成暂交费号
        String tempFeeNo = cLJSPaySchema.getGetNoticeNo();

        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setTempFeeNo(tempFeeNo);
        tLJTempFeeSchema.setTempFeeType("4"); //保全
        tLJTempFeeSchema.setRiskCode(cLJSPaySchema.getRiskCode());
        tLJTempFeeSchema.setOtherNo(mEdorAcceptNo);
        tLJTempFeeSchema.setOtherNoType(cLJSPaySchema.getOtherNoType());
        tLJTempFeeSchema.setPayDate(mCurrDate);
        tLJTempFeeSchema.setEnterAccDate(mCurrDate);
        tLJTempFeeSchema.setConfMakeDate(mCurrDate);
        tLJTempFeeSchema.setConfMakeTime(mCurrTime);
        
        tLJTempFeeSchema.setPayMoney(cLJSPaySchema.getSumDuePayMoney());
        tLJTempFeeSchema.setManageCom(mLCContSchema.getManageCom());
        tLJTempFeeSchema.setConfFlag("0");
        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJTempFeeSchema, SysConst.DELETE_AND_INSERT);

        //设置暂交费分类表
        LJTempFeeClassSchema tLJTempFeeClassSchema = new
                LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(tempFeeNo);
        tLJTempFeeClassSchema.setPayMode("5"); //内部流转
        tLJTempFeeClassSchema.setPayDate(mCurrDate);
        tLJTempFeeClassSchema.setEnterAccDate(mCurrDate);
        tLJTempFeeClassSchema.setConfMakeDate(mCurrDate);
        tLJTempFeeClassSchema.setConfMakeTime(mCurrTime);
        tLJTempFeeClassSchema.setPayMoney(cLJSPaySchema.getSumDuePayMoney());
        tLJTempFeeClassSchema.setManageCom(mLCContSchema.getManageCom());
        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJTempFeeClassSchema, SysConst.DELETE_AND_INSERT);
        
        return mMap;
    }

}
