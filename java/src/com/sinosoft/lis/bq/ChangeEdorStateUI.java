package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;


public class ChangeEdorStateUI
{
    private ChangeEdorStateBL mChangeEdorStateBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorItem LPGrpEdorItemSchema
     */
    public ChangeEdorStateUI(GlobalInput gi, LPGrpEdorItemSchema edorItem)
    {
        mChangeEdorStateBL = new ChangeEdorStateBL(gi, edorItem);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mChangeEdorStateBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mChangeEdorStateBL.mErrors.getFirstError();
    }
}
