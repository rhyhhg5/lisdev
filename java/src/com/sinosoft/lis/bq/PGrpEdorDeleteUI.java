package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LGWorkSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LGWorkSet;
/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:个单保全删除UI类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2018
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author XunZhiHan
 * @version 1.0
 */

public class PGrpEdorDeleteUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	/** 数据操作字符串 */
	private String mOperate;

	/** 刪除判斷 */
	private String delFlag;

	public PGrpEdorDeleteUI() {
	}

	/**
	 * 接收页面提交的数据
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// System.out.println("删除原因是："+delReason);
		PGrpEdorDeleteBL tPGrpEdorDeleteBL = new PGrpEdorDeleteBL();
//		TransferData tTransferData = new TransferData();
//		String delFlag=(String)tTransferData.getValueByName("DelFlag");
//    	System.out.println(delFlag);
//		System.out.println("---UI BEGIN---" + mOperate);
		if (tPGrpEdorDeleteBL.submitData(cInputData, mOperate) == false) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPGrpEdorDeleteBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "tPGrpEdorDeleteUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据删除失败!";
			this.mErrors.addOneError(tError);
			mResult.clear();
			return false;
		}
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args) {

		LGWorkDB tLGWorkDB = new LGWorkDB();
		LGWorkSet LGWorkSet = new LGWorkSet();
		LGWorkSet tLGWorkSet = tLGWorkDB.executeQuery("select * from LGWork");

		for (int i = 1; i <= tLGWorkSet.size(); i++) {

			GlobalInput tGlobalInput = new GlobalInput();
			
			tGlobalInput.Operator = "001";
			tGlobalInput.ComCode = "86";
			tGlobalInput.ManageCom = "86";

			PGrpEdorDeleteUI tPGrpEdorDeleteUI = new PGrpEdorDeleteUI();
			TransferData tTransferData = new TransferData();
			
			VData tVData = new VData();
			tVData.addElement(tGlobalInput);
			tVData.addElement(tLGWorkSet.get(i));
			//tVData.addElement(tTransferData);
			// tVData.addElement(reasonCode);
			// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			tPGrpEdorDeleteUI.submitData(tVData, "DELETE");

		}

	}
}
