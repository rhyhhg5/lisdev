package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全增减人金额总数小于0.1元校验</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author xiep
 * @version 2.0
 */
public class GrpNIZTMoneyCheckBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData= new VData();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = null;
    
    MMap map = new MMap();

    private String mEdorNo = "";
    private String mMoney = "";//LjsGetEndorse中sum(GetMoney)
    

    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    private LPEdorMainSet mLPEdorMainSet = new LPEdorMainSet();

    public GrpNIZTMoneyCheckBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData
     * @param cOperate
     * @return
     */
    public void checkMoney(String EdorNo)
    {
        //将工单号传入
        mEdorNo= EdorNo;

//      校验是否为需要处理的数据
        if (!checkData()) return;
        
        System.out.println("---Start dealData---");
        if (!dealData()) {
            return;
        }
        // 数据操作业务处理
        //　数据提交、保存
        if (!prepareOutputData()) {
            return;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start GrpNIZTMoneyCheckBL Submit...");

        if (!tPubSubmit.submitData(mInputData, null)) 
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return;
        }

        return;
    }
    
    /**
     * 处理需要忽略的金额
     * 按照NI的标准来修改ZT,包括ZT的lpedoritem表
     * @return boolean
     */
    private boolean dealData()
    {
        try
        {
        	//金额修改关联的ContNo和PolNo
        	String tContNo="";
        	String tPolNo="";
        	String tDutyCode="";
        	String tContPlanCode="";
        	String sqlcont="select max(ContNo) from LPEdorItem where EdorNo='"+mEdorNo+"'";
        	ExeSQL tExeSQL=new ExeSQL();
        	tContNo=tExeSQL.getOneValue(sqlcont);
        	String sqlpol="select max(PolNo) from LjsGetEndorse where EndorseMentNo='"+mEdorNo+"' and ContNo='"+tContNo+"' and FeeOperationType='ZT'";
        	tPolNo=tExeSQL.getOneValue(sqlpol);
        	String sqlduty="select max(DutyCode) from LjsGetEndorse where EndorseMentNo='"+mEdorNo+"' and ContNo='"+tContNo+"' and FeeOperationType='ZT' and  PolNo='"+tPolNo+"'";
        	tDutyCode=tExeSQL.getOneValue(sqlduty);
        	String sqlplan="select max(PayPlanCode) from LjsGetEndorse where EndorseMentNo='"+mEdorNo+"' and ContNo='"+tContNo+"' and FeeOperationType='ZT' and  PolNo='"+tPolNo+"' and DutyCode='"+tDutyCode+"'";
        	tContPlanCode=tExeSQL.getOneValue(sqlplan);
        	String gpritemSQL="update LPGrpEdorItem set GetMoney=GetMoney-("+mMoney+") where EdorNo='"+mEdorNo+"' and edortype='ZT' ";
        	String itemSQL="update LPEdorItem set GetMoney=GetMoney-("+mMoney+") where EdorNo='"+mEdorNo+"' and ContNo='"+tContNo+"'";
        	String grpmainSQL="update LPGrpEdorMain set GetMoney=0 where EdorNo='"+mEdorNo+"' ";
        	String appSQL="update LPEdorApp set GetMoney=0 where EdorAcceptNo='"+mEdorNo+"' ";
        	String getendorseSQL="update LjsGetEndorse set GetMoney=GetMoney-("+mMoney+") where EndorseMentNo='"+mEdorNo+"' and FeeOperationType='ZT' and PolNo='"+tPolNo+"' and ContNo='"+tContNo+"' and DutyCode='"+tDutyCode+"' and PayPlanCode='"+tContPlanCode+"'";
        	map.put(gpritemSQL,"UPDATE");
        	map.put(itemSQL,"UPDATE");
        	map.put(grpmainSQL,"UPDATE");
        	map.put(appSQL,"UPDATE");
        	map.put(getendorseSQL,"UPDATE");
        	System.out.println("插入完成");
        	return true; 

        }
        catch (Exception e)
        {
            CError tError = new CError();
            tError.moduleName = "GrpNIZTMoneyCheckBL";
            tError.functionName = "DealData";
            tError.errorMessage = "checkData中出错";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    
    /**
     * 校验该工单是否需要处理
     * @return boolean
     */
    private boolean checkData()
    {
        try
        {
        	String tSQL1="select distinct EdorType from LPGrpEdorItem where EdorNo='"+mEdorNo+"' order by EdorType with ur";
        	SSRS tSSRS1 = new ExeSQL().execSQL(tSQL1);
        	if(!(tSSRS1.getMaxRow()==2)){
        		System.out.println("此工单不需要进行金额忽略处理1");
        		return false;
        	}
        	String type1=tSSRS1.GetText(1, 1);
        	String type2=tSSRS1.GetText(2, 1);
        	if (!(type1!=null&&type1!=""&&type1.equals("NI")&&type2!=null&&type2!=""&&type2.equals("ZT")))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
             {
        		System.out.println("此工单不需要进行金额忽略处理2");	
                return false;	            	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
             }
        	String tSQL2="select sum(GetMoney) from LjsGetEndorse where EndorseMentNo='"+mEdorNo+"' with ur";
        	SSRS tSSRS2 = new ExeSQL().execSQL(tSQL2);
        	mMoney=tSSRS2.GetText(1, 1);//此处获得需要忽略的总金额
        	if((Math.abs(Double.parseDouble(mMoney))-0.1>=0)||Double.parseDouble(mMoney)==0)
        	 {
        		System.out.println("此工单不需要进行金额忽略处理3");	
                return false;
        	 }
        	
        	return true; 

        }
        catch (Exception e)
        {
            CError tError = new CError();
            tError.moduleName = "GrpNIZTMoneyCheckBL";
            tError.functionName = "checkData";
            tError.errorMessage = "checkData中出错";
            this.mErrors.addOneError(tError);
            return false;
        }
    }
    
    private boolean prepareOutputData() {
    	
        mInputData.add(map);

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult()
    {
        return mResult;
    }


    public static void main(String[] args)
    {
    	String EdorNo ="20051219000016";
    	GrpNIZTMoneyCheckBL tGrpNIZTMoneyCheckBL=new GrpNIZTMoneyCheckBL();
    	tGrpNIZTMoneyCheckBL.checkMoney(EdorNo);
    }
}
