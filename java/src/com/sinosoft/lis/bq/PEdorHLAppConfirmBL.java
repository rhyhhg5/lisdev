package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LCInsureAccClassSet;
import com.sinosoft.lis.vschema.LCInsureAccTraceSet;
import com.sinosoft.lis.vschema.LPInsureAccClassSet;
import com.sinosoft.lis.vschema.LPInsureAccTraceSet;
import com.sinosoft.lis.vschema.LPInsureAccSet;

import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.lis.vschema.LPGetSet;
import com.sinosoft.utility.*;
/**
 * <p>Title: 保全项目理算</p>
 * <p>Description: 老年护理金领取</p>
 * <p>Copyright: Copyright (c) 2011</p>
 * <p>Company: Sinosoft</p>
 * @author chenjg
 * @version 1.0
 */
public class PEdorHLAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private LPEdorItemSchema mLPEdorItemSchema = null;
    
    //记录结息后的子账户信息
    private LPInsureAccTraceSet mLPInsureAccTraceSet = new LPInsureAccTraceSet();//用来存取计算利息生成的轨迹表
    
    private LPInsureAccClassSet mLPInsureAccClassSet = new LPInsureAccClassSet();//用来存取计算利息生成的分类账户表
    
    private LPGetSchema  tLPGetSchema = null;

    private GlobalInput mGlobalInput = new GlobalInput();
    
    private VData mInputData = null;

    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到输入数据
     * @param cInputData VData
     * @return boolean
     */
    private void getInputData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.
                getObjectByObjectName("LPEdorItemSchema", 0);

    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
    	mLPEdorItemSchema = (LPEdorItemSchema) mInputData.
        getObjectByObjectName("LPEdorItemSchema", 0);
           mGlobalInput.setSchema((GlobalInput) mInputData.
           getObjectByObjectName("GlobalInput", 0));
    	System.out.println(mLPEdorItemSchema.getGrpContNo());
    	VData tVData = new VData();
    	tVData.add(mLPEdorItemSchema);  	
    	tVData.add(mGlobalInput);      
        UliBQInsuAccBala tUliBQInsuAccBala = new UliBQInsuAccBala();
     
            if(!tUliBQInsuAccBala.submitData(tVData, "",mLPEdorItemSchema.getGrpContNo()))
            {
        
                String errInfo ="团单:" + mLPEdorItemSchema.getGrpContNo() + " 结算失败: ";
                errInfo += tUliBQInsuAccBala.mErrors.getErrContent();
                mErrors.addOneError(errInfo);
                System.out.println(errInfo);
                return false;
            }
            else
            {
            	 getLPPolSet(mLPEdorItemSchema);
            	 LPGetSet tLPGetSet = getLPGetSet(mLPEdorItemSchema);
            	  if (tLPGetSet != null && tLPGetSet.size() > 0)
                  {
               tLPGetSchema =tLPGetSet.get(1);
                  }else{
                	  mErrors.addOneError("查询的领取信息有误！"); 
                  }
	        	//计算生成lcget表，用来存取领取表
	        	//领取的金额.
            	 //取出一次性待领取的金额 
                 double money = getMoney();
                 tLPGetSchema.setActuGet(money);
                 tLPGetSchema.setSumMoney(money);
                 tLPGetSchema.setStandMoney(money);
                 mMap.put(tLPGetSchema,"DELETE&INSERT");    
            }

        return true;
    }

    /**
     * 判断判断保全变更的客户是否是被保人
     * @return boolean
     */
    private boolean isInsured()
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mLPEdorItemSchema.getContNo());
        tLCInsuredDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        if (tLCInsuredDB.query().size() == 0)
        {
            return false;
        }
        return true;
    }
    /**
     * 得到需要领取的险种
     * @return
     */
    private LPPolSet getLPPolSet(LPEdorItemSchema tLPEdorItemSchema)
    {
        //查找出所有的保单
        LPPolSet tLPPolSet = new LPPolSet();
        LPPolDB  tLPPolDB=new LPPolDB();
        tLPPolDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
        tLPPolDB.setContNo(tLPEdorItemSchema.getContNo());
        tLPPolDB.setEdorType(tLPEdorItemSchema.getEdorType());
        tLPPolSet=tLPPolDB.query();
        if (tLPPolDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询待领取险种时失败！");
            return null;
        }

        return tLPPolSet;
    }
    
    /**
     * 得到需要领取的领取信息
     * @return
     */
    private LPGetSet getLPGetSet(LPEdorItemSchema tLPEdorItemSchema)
    {
        //查找出所有的保单
    	LPGetSet tLPGetSet = new LPGetSet();
        LPGetDB  tLPGetDB=new LPGetDB();
        tLPGetDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
        tLPGetDB.setContNo(tLPEdorItemSchema.getContNo());
        tLPGetDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
        tLPGetDB.setEdorType(tLPEdorItemSchema.getEdorType());
        tLPGetDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
        tLPGetDB.setDutyCode("671004");
        tLPGetDB.setGetDutyCode("671201");
        tLPGetSet=tLPGetDB.query();
        if (tLPGetDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询待领取险种时失败！");
            return null;
        }

        return tLPGetSet;
    }
    /**
     * 取出待领取的金额
     * @return boolean
     */
    private double  getMoney(){
//    	查找出所有的保单
    	LPInsureAccSet tLPInsureAccSet = new LPInsureAccSet();
    	LPInsureAccDB  tLPInsureAccDB=new LPInsureAccDB();
    	tLPInsureAccDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
    	tLPInsureAccDB.setContNo(mLPEdorItemSchema.getContNo());
    	tLPInsureAccDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
    	tLPInsureAccDB.setEdorType(mLPEdorItemSchema.getEdorType());
    	tLPInsureAccDB.setEdorNo(mLPEdorItemSchema.getEdorNo());

    	tLPInsureAccSet=tLPInsureAccDB.query();
        if (tLPInsureAccDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询待领取险种时失败！");
            return 0.0 ;
        }
        
    	return tLPInsureAccSet.get(1).getInsuAccBala();
    }
    
    
    
    /**
     * 做被保人客户资料变更
     * @return boolean
     */
    private boolean doInsuredEdor()
    {
        PEdorICAppConfirmBL tPEdorICAppConfirmBL = new
                PEdorICAppConfirmBL();
        if (!tPEdorICAppConfirmBL.submitData(mInputData, ""))
        {
            mErrors.copyAllErrors(tPEdorICAppConfirmBL.mErrors);
            return false;
        }
        mMap.add((MMap) tPEdorICAppConfirmBL.getResult().
                getObjectByObjectName("MMap", 0));
        //直接利用理算完成的数据会有问题
        //同样也会造成payenddate错误的问题,添加lpprem,lpduty,lppol中的相关日期修改 add by xp 101014
        String tUpdateLCprem = " update lpprem a set (urgepayflag,paytimes,rate,needacc,payenddate) = (select urgepayflag,paytimes,rate,needacc,payenddate from lcprem where polno = a.polno and dutycode = a.dutycode and payplancode = a.payplancode) "
                     		   + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
                               ;
        String tUpdateLCget = " update lpget a set (summoney,baladate,state) = (select summoney,baladate,state from lcget where polno = a.polno and dutycode = a.dutycode and getdutycode = a.getdutycode) "
                               + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
                               ;
        String tUpdateLCpol = " update lppol a set payenddate = (select payenddate from lcpol where polno = a.polno) "
        					   + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
        					   ;
        String tUpdateLCduty = " update lpduty a set payenddate = (select payenddate from lcduty where polno = a.polno and dutycode=a.dutycode) "
			   + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
			   ;
        mMap.put(tUpdateLCget,"UPDATE");
        mMap.put(tUpdateLCpol,"UPDATE");
        mMap.put(tUpdateLCprem,"UPDATE");
        mMap.put(tUpdateLCduty,"UPDATE");
        return true;
    }
}
