package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.util.HashMap;

/**
 * <p>Title: 卡折增加连带被保人</p>
 * <p>Description: 卡折增加连带被保人</p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Sinosoft</p>
 * @author Zhanggm
 * @version 1.0
 */

public class LLWSCertifyBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = "WS";

    private String mContNo = null;

    private String mMessage = null;

    private LPDiskImportSet mLPDiskImportSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    public LPDiskImportSet mLPDiskImportSetCF = null;   //重复人记录 ******
    
    private Reflections ref = new Reflections();


    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     * @param grpContNo String
     */
    public LLWSCertifyBL(GlobalInput gi, String edorNo, String contNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mContNo = contNo;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }
        
        if (!dealData())
        {
            return false;
        }
        
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到错误信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到磁盘导入数据
     * @return boolean
     */
    private boolean getInputData()
    {
        mLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, mEdorType,mContNo, BQ.IMPORTSTATE_SUCC);
        if (mLPDiskImportSet.size() == 0)
        {
            mErrors.addOneError("找不到磁盘导入数据！");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
    	for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            if (tLPDiskImportSchema.getSex() == null)
            {
                tLPDiskImportSchema.setSex("2");
            }
            if (tLPDiskImportSchema.getBirthday() == null)
            {
                tLPDiskImportSchema.setBirthday("1900-01-01");
            }
            if (!dealOneInsured(tLPDiskImportSchema))
            {
                return false;
            }
        }
    	bakDiskImport();
        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }


    /**
     * 处理一个被保人
     * @param aLPDiskImportSchema LPDiskImportSchema
     * @return boolean
     */
    private boolean dealOneInsured(LPDiskImportSchema aLPDiskImportSchema)
    {
    	String tCustomerNo = setLDPerson(aLPDiskImportSchema);
    	LCContDB tLCContDB = new LCContDB();
    	tLCContDB.setContNo(mContNo);
    	LCContSchema tLCContSchema = new LCContSchema();
    	if(!tLCContDB.getInfo())
    	{
    		mErrors.addOneError("没有查询到激活卡保单信息！");
            return false;
    	}
    	tLCContSchema.setSchema(tLCContDB.getSchema());
        if(!setLCPol(tLCContSchema, aLPDiskImportSchema, tCustomerNo))
        {
        	return false;
        }
        if(!setLCInsured(tLCContSchema, aLPDiskImportSchema, tCustomerNo))
        {
        	return false;
        }
        
        return true;
    }
    
    /**
     * 设置客户信息，先判断是不是老客户
     * @param contNo String
     * @param aLPDiskImportSchema LPDiskImportSchema
     * @return boolean
     */
    private String setLDPerson(LPDiskImportSchema aLPDiskImportSchema)
    {
        String customerNo = null;
        LDPersonSchema oldLDPersonSchema = new LDPersonSchema();
        oldLDPersonSchema.setName(aLPDiskImportSchema.getInsuredName());
        oldLDPersonSchema.setSex(aLPDiskImportSchema.getSex());
        oldLDPersonSchema.setBirthday(aLPDiskImportSchema.getBirthday());
        oldLDPersonSchema.setIDType(aLPDiskImportSchema.getIDType());
        oldLDPersonSchema.setIDNo(aLPDiskImportSchema.getIDNo());
        OldCustomerCheck tOldCustomerCheck = new OldCustomerCheck(oldLDPersonSchema);
        if (tOldCustomerCheck.checkOldCustomer() == OldCustomerCheck.OLD)
        {
            customerNo = tOldCustomerCheck.getCustomerNo();
        }
        else
        {
            customerNo = PubFun1.CreateMaxNo("CUSTOMERNO", null);
            LDPersonSchema tLDPersonSchema = new LDPersonSchema();
            tLDPersonSchema.setCustomerNo(customerNo);
            tLDPersonSchema.setName(aLPDiskImportSchema.getInsuredName());
            tLDPersonSchema.setSex(aLPDiskImportSchema.getSex());
            tLDPersonSchema.setBirthday(aLPDiskImportSchema.getBirthday());
            tLDPersonSchema.setIDType(aLPDiskImportSchema.getIDType());
            tLDPersonSchema.setIDNo(aLPDiskImportSchema.getIDNo());
            tLDPersonSchema.setOccupationType(aLPDiskImportSchema.getOccupationType());
            tLDPersonSchema.setOthIDNo(aLPDiskImportSchema.getOthIDNo());
            tLDPersonSchema.setState("1");
            tLDPersonSchema.setOperator(mGlobalInput.Operator);
            tLDPersonSchema.setMakeDate(mCurrentDate);
            tLDPersonSchema.setMakeTime(mCurrentTime);
            tLDPersonSchema.setModifyDate(mCurrentDate);
            tLDPersonSchema.setModifyTime(mCurrentTime);
            mMap.put(tLDPersonSchema, SysConst.INSERT);
        }
        return customerNo;
    }
    
    /**
     * 设置合同信息
     * @param contNo String
     * @param aLPDiskImportSchema LPDiskImportSchema
     * @return boolean
     */
    private boolean setLCPol(LCContSchema aLCContParam,
            LPDiskImportSchema aLPDiskImportSchema, String aCustomerNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(aLCContParam.getContNo());
        tLCPolDB.setInsuredNo(aLCContParam.getInsuredNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        if(tLCPolSet.size()==0)
        {
        	mErrors.addOneError("没有查询到激活卡主被保人险种信息！");
            return false;
        }
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            LCPolSchema newLCPolSchema = new LCPolSchema();
            String newPolNo = PubFun1.CreateMaxNo("POLNO", null);
            ref.transFields(newLCPolSchema, tLCPolSchema);
            newLCPolSchema.setPolNo(newPolNo);
            newLCPolSchema.setProposalNo(newPolNo);
            newLCPolSchema.setMainPolNo(newPolNo);
            newLCPolSchema.setInsuredNo(aCustomerNo);
            newLCPolSchema.setInsuredName(aLPDiskImportSchema.getInsuredName());
            newLCPolSchema.setInsuredSex(aLPDiskImportSchema.getSex());
            newLCPolSchema.setInsuredBirthday(aLPDiskImportSchema.getBirthday());
            newLCPolSchema.setStandPrem(0);
            newLCPolSchema.setPrem(0);
            newLCPolSchema.setSumPrem(0);
            newLCPolSchema.setOperator(mGlobalInput.Operator);
            newLCPolSchema.setMakeDate(mCurrentDate);
            newLCPolSchema.setMakeTime(mCurrentTime);
            newLCPolSchema.setModifyDate(mCurrentDate);
            newLCPolSchema.setModifyTime(mCurrentTime);
            mMap.put(newLCPolSchema, SysConst.INSERT);
            if(!setLCDuty(tLCPolSchema, newLCPolSchema, aCustomerNo))
            {
            	return false;
            }
            	
        }
        return true;
    }
    
    private boolean setLCDuty(LCPolSchema aLCPolParam, LCPolSchema newLCPolSchema,String aCustomerNo)
    {
        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(aLCPolParam.getPolNo());
        LCDutySet tLCDutySet = tLCDutyDB.query();
        if(tLCDutySet.size()==0)
        {
        	mErrors.addOneError("没有查询到激活卡主被保人险种"+aLCPolParam.getRiskCode()+"下责任！");
            return false;
        }
        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            LCDutySchema tLCDutySchema = tLCDutySet.get(i);
            LCDutySchema newLCDutySchema = new LCDutySchema();
            ref.transFields(newLCDutySchema, tLCDutySchema);
            newLCDutySchema.setContNo(newLCPolSchema.getContNo());
            newLCDutySchema.setPolNo(newLCPolSchema.getPolNo());
            newLCDutySchema.setStandPrem(0);
            newLCDutySchema.setPrem(0);
            newLCDutySchema.setSumPrem(0);
            newLCDutySchema.setOperator(mGlobalInput.Operator);
            newLCDutySchema.setMakeDate(mCurrentDate);
            newLCDutySchema.setMakeTime(mCurrentTime);
            newLCDutySchema.setModifyDate(mCurrentDate);
            newLCDutySchema.setModifyTime(mCurrentTime);
            mMap.put(newLCDutySchema, SysConst.INSERT);

            if(!setLCPrem(tLCDutySchema, newLCDutySchema))
            {
            	return false;
            }
            	
            if(!setLCGet(tLCDutySchema, newLCDutySchema, aCustomerNo))
            {
            	return false;
            }
        }
        return true;
    }

    private boolean setLCPrem(LCDutySchema aLCDutyParam, LCDutySchema newLCDutySchema)
    {
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(aLCDutyParam.getPolNo());
        tLCPremDB.setDutyCode(aLCDutyParam.getDutyCode());
        LCPremSet tLCPremSet = tLCPremDB.query();
        if(tLCPremSet.size()==0)
        {
        	mErrors.addOneError("没有查询到激活卡主被保人责任"+aLCDutyParam.getDutyCode()+"保费信息！");
            return false;
        }
        for (int i = 1; i <= tLCPremSet.size(); i++)
        {
            LCPremSchema tLCPremSchema = tLCPremSet.get(i);
            LCPremSchema newLCPremSchema = new LCPremSchema();
            ref.transFields(newLCPremSchema, tLCPremSchema);
            newLCPremSchema.setContNo(newLCDutySchema.getContNo());
            newLCPremSchema.setPolNo(newLCDutySchema.getPolNo());
            newLCPremSchema.setStandPrem(0);
            newLCPremSchema.setPrem(0);
            newLCPremSchema.setSumPrem(0);
            newLCPremSchema.setOperator(mGlobalInput.Operator);
            newLCPremSchema.setMakeDate(mCurrentDate);
            newLCPremSchema.setMakeTime(mCurrentTime);
            newLCPremSchema.setModifyDate(mCurrentDate);
            newLCPremSchema.setModifyTime(mCurrentTime);
            mMap.put(newLCPremSchema, SysConst.INSERT);
        }
        return true;
    }

    private boolean setLCGet(LCDutySchema aLCDutyParam, LCDutySchema newLCDutySchema,String aCustomerNo)
    {
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setPolNo(aLCDutyParam.getPolNo());
        tLCGetDB.setDutyCode(aLCDutyParam.getDutyCode());
        LCGetSet tLCGetSet = tLCGetDB.query();
        if(tLCGetSet.size()==0)
        {
        	mErrors.addOneError("没有查询到激活卡主被保人责任"+aLCDutyParam.getDutyCode()+"给付信息！");
            return false;
        }
        for (int i = 1; i <= tLCGetSet.size(); i++)
        {
            LCGetSchema tLCGetSchema = tLCGetSet.get(i);
            LCGetSchema newLCGetSchema = new LCGetSchema();
            ref.transFields(newLCGetSchema, tLCGetSchema);
            newLCGetSchema.setContNo(newLCDutySchema.getContNo());
            newLCGetSchema.setPolNo(newLCDutySchema.getPolNo());
            newLCGetSchema.setInsuredNo(aCustomerNo);
            newLCGetSchema.setOperator(mGlobalInput.Operator);
            newLCGetSchema.setMakeDate(mCurrentDate);
            newLCGetSchema.setMakeTime(mCurrentTime);
            newLCGetSchema.setModifyDate(mCurrentDate);
            newLCGetSchema.setModifyTime(mCurrentTime);
            mMap.put(newLCGetSchema, SysConst.INSERT);
        }
        return true;
    }

    /**
     * 设置被保人信息，LCinsured和LCCont是一对一关系
     * @param contNo String
     * @param aLPDiskImportSchema LPDiskImportSchema
     * @return boolean
     */
    private boolean setLCInsured(LCContSchema aLCContSchema, LPDiskImportSchema aLPDiskImportSchema, String aCustomerNo)
    {
    	LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(aLCContSchema.getContNo());
        tLCInsuredDB.setInsuredNo(aLCContSchema.getInsuredNo());
        if (!tLCInsuredDB.getInfo())
        {
        	mErrors.addOneError("没有查询到激活卡主被保人信息！");
            return false;
        }
    	 
        LCInsuredSchema tLCInsuredSchema = tLCInsuredDB.getSchema();
        LCInsuredSchema newLCInsuredSchema = new LCInsuredSchema();
        ref.transFields(newLCInsuredSchema, tLCInsuredSchema);
        newLCInsuredSchema.setContNo(aLCContSchema.getContNo());
        newLCInsuredSchema.setInsuredNo(aCustomerNo);
        newLCInsuredSchema.setInsuredStat(aLPDiskImportSchema.getRetire());
        newLCInsuredSchema.setName(aLPDiskImportSchema.getInsuredName());
        newLCInsuredSchema.setSex(aLPDiskImportSchema.getSex());
        newLCInsuredSchema.setBirthday(aLPDiskImportSchema.getBirthday());
        newLCInsuredSchema.setIDType(aLPDiskImportSchema.getIDType());
        newLCInsuredSchema.setIDNo(aLPDiskImportSchema.getIDNo());
        newLCInsuredSchema.setOthIDNo(aLPDiskImportSchema.getOthIDNo());
        newLCInsuredSchema.setBankCode(aLPDiskImportSchema.getBankCode());
        newLCInsuredSchema.setBankAccNo(aLPDiskImportSchema.getBankAccNo());
        newLCInsuredSchema.setAccName(aLPDiskImportSchema.getAccName());
        newLCInsuredSchema.setAddressNo("0");
        newLCInsuredSchema.setOperator(mGlobalInput.Operator);
        newLCInsuredSchema.setMakeDate(mCurrentDate);
        newLCInsuredSchema.setMakeTime(mCurrentTime);
        newLCInsuredSchema.setModifyDate(mCurrentDate);
        newLCInsuredSchema.setModifyTime(mCurrentTime);
        mMap.put(newLCInsuredSchema, SysConst.INSERT);
        
        String sql = "select max(int(AddressNo)) from LCAddress where customerno = '" + aCustomerNo + "' ";
        String no = new ExeSQL().getOneValue(sql);
        String newNo = null;
        LCAddressSchema newLCAddressSchema = new LCAddressSchema();
        if("".equals(no))
        {
        	newNo = "0";
        }
        else
        {
        	LCAddressDB tLCAddressDB = new LCAddressDB();
        	tLCAddressDB.setCustomerNo(aCustomerNo);
        	tLCAddressDB.setAddressNo(no);
        	LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        	if(tLCAddressDB.getInfo())
        	{
        		tLCAddressSchema = tLCAddressDB.getSchema();
        	}
        	ref.transFields(newLCAddressSchema, tLCAddressSchema);
        	newNo = String.valueOf(Integer.parseInt(no) + 1 );
        }
        newLCAddressSchema.setCustomerNo(aCustomerNo);
        newLCAddressSchema.setAddressNo(newNo);
        newLCAddressSchema.setOperator(mGlobalInput.Operator);
        newLCAddressSchema.setMakeDate(mCurrentDate);
        newLCAddressSchema.setMakeTime(mCurrentTime);
        newLCAddressSchema.setModifyDate(mCurrentDate);
        newLCAddressSchema.setModifyTime(mCurrentTime);
        mMap.put(newLCAddressSchema, SysConst.INSERT);
        
        aLPDiskImportSchema.setInsuredNo(aCustomerNo);
    	aLPDiskImportSchema.setOperator(mGlobalInput.Operator);
        aLPDiskImportSchema.setModifyDate(mCurrentDate);
        aLPDiskImportSchema.setModifyTime(mCurrentTime);
        mMap.put(aLPDiskImportSchema, SysConst.UPDATE);
        return true;
    }
    
    
    /**
     * 将 LPDiskImport 转移到 LBDiskImport
     * @param aCustomerNo String
     * @param aLPDiskImportSchema LPDiskImportSchema
     * @return boolean
     */
    private void bakDiskImport()
    {
    	String tEdorNo = com.sinosoft.task.CommonBL.createWorkNo();
    	LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        tLPDiskImportDB.setEdorNo(mEdorNo);
        tLPDiskImportDB.setEdorType(mEdorType);
        tLPDiskImportDB.setGrpContNo(mContNo);
        LPDiskImportSet tLPDiskImportSet = tLPDiskImportDB.query();
        for(int i=1;i<=tLPDiskImportSet.size();i++)
        {
        	LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(i);
        	LBDiskImportSchema tLBDiskImportSchema = new LBDiskImportSchema();
        	ref.transFields(tLBDiskImportSchema, tLPDiskImportSchema);
            tLBDiskImportSchema.setEdorNo(tEdorNo);
            tLBDiskImportSchema.setEdorType(mEdorType);
            mMap.put(tLPDiskImportSchema, SysConst.DELETE);
            mMap.put(tLBDiskImportSchema, SysConst.INSERT);
        }
    	
    }
}
