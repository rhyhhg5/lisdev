package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 新保加人保全项目</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * ReWrite ZhangRong
 * @version 1.0
 */

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PEdorNSCancelBL implements EdorCancel
{
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

    /** 传出数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();

    public PEdorNSCancelBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) data.getObjectByObjectName("LPEdorItemSchema", 0);
        }
        catch (Exception e)
        {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorNSCancelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mGlobalInput == null || mLPEdorItemSchema == null)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorNSCancelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "输入数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
        tLCPolDB.setAppFlag(BQ.APPFLAG_EDOR);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if(tLCPolSet == null || tLCPolSet.size() < 1)
        {
            return true;
        }

        for(int i = 1 ;i <= tLCPolSet.size(); i++)
        {
            LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPEdorItemSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPEdorItemSchema.setInsuredNo(tLCPolSet.get(i).getInsuredNo());
            tLPEdorItemSchema.setPolNo(tLCPolSet.get(i).getPolNo());

            VData tVData = new VData();
            tVData.add(tLPEdorItemSchema);
            tVData.add(mGlobalInput);

            PEdorNSDetailDelPolBL bl = new PEdorNSDetailDelPolBL();
            MMap tMMap = bl.getSubmitMap(tVData, "DELETE||INSUREDRISK");
            if(tMMap == null)
            {
                mErrors.copyAllErrors(bl.mErrors);
                return false;
            }
            mMap.add(tMMap);
        }
        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

}
