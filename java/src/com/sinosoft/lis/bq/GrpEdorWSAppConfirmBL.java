package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: 无名单实名化</p>
 * <p>Description: 无名单实名化理算</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorWSAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = BQ.EDORTYPE_WS;

    /** 磁盘导入数据  */
    private LPDiskImportSet mLPDiskImportSet = null;

    /** 团体合同号 */
    private String mGrpContNo = null;

    private String mCValidate = null;

    //private GrpNoNameManager mGrpNoNameManager = null;

    private List mNoNameList = new ArrayList();

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mEdorNo = edorItem.getEdorNo();
        mEdorType = edorItem.getEdorType();
        mGrpContNo = edorItem.getGrpContNo();
        LCGrpContSchema tLCGrpContSchema = CommonBL.getLCGrpCont(mGrpContNo);
        if (tLCGrpContSchema == null)
        {
            mErrors.addOneError("未找到团体保单信息！");
            return false;
        }
        mCValidate = tLCGrpContSchema.getCValiDate();
        if (mCValidate == null)
        {
            mErrors.addOneError("团体保单的生效日期为空！");
            return false;
        }
        mLPDiskImportSet = CommonBL.getLPDiskImportSet
                (mEdorNo, mEdorType, mGrpContNo, BQ.IMPORTSTATE_SUCC);
        //mGrpNoNameManager = new GrpNoNameManager(mGrpContNo);
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        setNoNameList();
        //查询导入清单，以后需要修改为游标读入 qulq
        //修改为单人提交 qulq 2007-12-15
        VData tInputData = new VData();
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
        		mMap = new MMap();
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            if (tLPDiskImportSchema.getSex() == null)
            {
                tLPDiskImportSchema.setSex("2");
            }
            if (tLPDiskImportSchema.getBirthday() == null)
            {
                tLPDiskImportSchema.setBirthday("1900-01-01");
            }
            if (!dealOneInsured(tLPDiskImportSchema))
            {
                return false;
            }
            PubSubmit tPubSubmit = new PubSubmit();
            tInputData.clear();
        		tInputData.add(mMap);
           	if (!tPubSubmit.submitData(tInputData, ""))
        		{
            // @@错误处理
            	this.mErrors.copyAllErrors(tPubSubmit.mErrors);

	            CError tError = new CError();
 	           	tError.moduleName = "GrpEdorWSAppConfirmBL";
  	          tError.functionName = "dealData";
    	        tError.errorMessage = "实名化数据提交失败!";
      	      this.mErrors.addOneError(tError);
        	    return false;
        		}
        }
        mMap = new MMap();//返回给框架
        return true;
    }

    /**
     * 把批次先查出来，用的时候再取可以提高效率
     */
    private void setNoNameList()
    {
        //得到所有人数未满的批次
        String sql = "select a.ContNo, a.Peoples, b.ContPlanCode from LCCont a, LCInsured b " +
                "where a.ContNo = b.ContNo and a.grpcontno = b.grpcontno " +
                "and a.GrpContNo = '" + mGrpContNo + "' " +
                " and a.conttype ='2' and a.PolType = '1'" +
                "and (a.Peoples - (select count(distinct ContNo) from LCPol d " +
                " where d.GrpContNo = a.GrpContNo and exists (select 1 from LCPol c where c.ContNo = a.ContNo " +
                " and c.PolNo = d.MasterPolNo) " +
                " and d.PolTypeFlag <> '1' and d.grpcontno=a.grpcontno )) "+
                " >=(select count(1) from lpdiskimport e where e.edorno='"+mEdorNo+"' and e.grpcontno=a.grpcontno and b.ContPlanCode=e.ContPlanCode and relation='00' )" 
                ;
        SSRS tSSRS = (new ExeSQL()).execSQL(sql);
        System.out.println(sql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String contNo = tSSRS.GetText(i, 1);
            String peoples = tSSRS.GetText(i, 2);
            String contPlanCode = tSSRS.GetText(i, 3);
            NoNameInfo tNoNameInfo = new NoNameInfo();
            tNoNameInfo.setContNo(contNo);
            tNoNameInfo.setPeoples(Integer.parseInt(peoples));
            tNoNameInfo.setContPlanCode(contPlanCode);
            mNoNameList.add(tNoNameInfo);
        }
    }

    /**
     * 得到保障计划下的一个批次
     * @param contPlanCode String
     * @return String
     */
    private String getOneCont(String contPlanCode)
    {
        for (int i = 0; i < mNoNameList.size(); i++)
        {
            NoNameInfo tNoNameInfo = (NoNameInfo) mNoNameList.get(i);
            if (contPlanCode.equals(tNoNameInfo.getContPlanCode()))
            {
                String contNo = tNoNameInfo.getContNo();
                int peoples = tNoNameInfo.getPeoples() - 1;
                tNoNameInfo.setPeoples(peoples);
                if (peoples == 0)
                {
                    mNoNameList.remove(i);
                }
                return contNo;
            }
        }
        return null;
    }

    /**
     * 处理一个被保人
     * @param aLPDiskImportSchema LPDiskImportSchema
     * @return boolean
     */
    private boolean dealOneInsured(LPDiskImportSchema aLPDiskImportSchema)
    {
        String contNo = getOneCont(aLPDiskImportSchema.getContPlanCode());
        if (contNo == null)
        {
            mErrors.addOneError("未找到保障计划" + aLPDiskImportSchema.getContPlanCode() +
                    "下可供实名化的批次！");
            return false;
        }
        String customerNo = setLPPerson(aLPDiskImportSchema);
        String proposalContNo = setLPCont(contNo, customerNo,
                aLPDiskImportSchema);
        setLPInsured(contNo, proposalContNo, customerNo, aLPDiskImportSchema);
/*
 * 		add by xp 20140304
 * 		添加被保人地址表的处理逻辑 
*/        
        if((aLPDiskImportSchema.getOthIDType()!=null&&(!aLPDiskImportSchema.getOthIDType().equals("")))||(aLPDiskImportSchema.getImportFileName()!=null&&(!aLPDiskImportSchema.getImportFileName().equals("")))){
        	if(!setLPAddress(customerNo, aLPDiskImportSchema)){
            	mErrors.addOneError("被保人" + customerNo+"添加联系地址失败！");
                return false;
            }
        }
        
        //修改状态为理算完毕
        aLPDiskImportSchema.setState(BQ.IMPORTSTATE_CAL);
        aLPDiskImportSchema.setInsuredNo(customerNo);
        aLPDiskImportSchema.setOperator(mGlobalInput.Operator);
        aLPDiskImportSchema.setModifyDate(mCurrentDate);
        aLPDiskImportSchema.setMakeTime(mCurrentTime);
        mMap.put(aLPDiskImportSchema, "DELETE&INSERT");
        return true;
    }
    
    
    private boolean setLPAddress(String tCustomerNo,LPDiskImportSchema aLPDiskImportSchema){
    	ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String sql = "  Select Case When max(int(AddressNo)) Is Null Then '1' "
                     + "Else max(int(AddressNo))  End "
                     + "from LCAddress "
                     + "where CustomerNo='"
                     + tCustomerNo + "'";

        tSSRS = tExeSQL.execSQL(sql);
        Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
        int ttNo = firstinteger.intValue() + 1;
        System.out.println("得到的地址码是：" + ttNo);
        
	    LPAddressSchema tLPAddressSchema = new LPAddressSchema();
	    tLPAddressSchema.setEdorNo(mEdorNo);
	    tLPAddressSchema.setEdorType(mEdorType);
	    tLPAddressSchema.setCustomerNo(tCustomerNo);
	    tLPAddressSchema.setAddressNo(String.valueOf(ttNo));
//	    这个字段存联系电话
	    tLPAddressSchema.setPhone(aLPDiskImportSchema.getOthIDType());
//	    这个字段存联系地址
	    tLPAddressSchema.setPostalAddress(aLPDiskImportSchema.getImportFileName());
	    tLPAddressSchema.setOperator(mGlobalInput.Operator);
	    tLPAddressSchema.setMakeDate(PubFun.getCurrentDate());
	    tLPAddressSchema.setMakeTime(PubFun.getCurrentTime());
	    tLPAddressSchema.setModifyDate(PubFun.getCurrentDate());
	    tLPAddressSchema.setModifyTime(PubFun.getCurrentTime());
	    mMap.put(tLPAddressSchema, SysConst.DELETE_AND_INSERT);
	    
//	    更新lcinsured中addressno
	    mMap.put("update lpinsured set addressno='"+String.valueOf(ttNo)+"' where edorno='"+mEdorNo+"' and edortype='"+mEdorType+"' and insuredno='"+tCustomerNo+"' ", SysConst.UPDATE);
    	return true;
    }

    /**
     * 设置客户信息，先判断是不是老客户
     * @param contNo String
     * @param aLPDiskImportSchema LPDiskImportSchema
     * @return boolean
     */
    private String setLPPerson(LPDiskImportSchema aLPDiskImportSchema)
    {
        String customerNo = null;
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tLDPersonSchema.setName(aLPDiskImportSchema.getInsuredName());
        tLDPersonSchema.setSex(aLPDiskImportSchema.getSex());
        tLDPersonSchema.setBirthday(aLPDiskImportSchema.getBirthday());
        tLDPersonSchema.setIDType(aLPDiskImportSchema.getIDType());
        tLDPersonSchema.setIDNo(aLPDiskImportSchema.getIDNo());
        OldCustomerCheck tOldCustomerCheck = new OldCustomerCheck(tLDPersonSchema);
        if (tOldCustomerCheck.checkOldCustomer() == OldCustomerCheck.OLD)
        {
            customerNo = tOldCustomerCheck.getCustomerNo();
        }
        else
        {
            customerNo = PubFun1.CreateMaxNo("CUSTOMERNO", null);
            LPPersonSchema tLPPersonSchema = new LPPersonSchema();
            tLPPersonSchema.setEdorNo(mEdorNo);
            tLPPersonSchema.setEdorType(mEdorType);
            tLPPersonSchema.setCustomerNo(customerNo);
            tLPPersonSchema.setName(aLPDiskImportSchema.getInsuredName());
            tLPPersonSchema.setSex(aLPDiskImportSchema.getSex());
            tLPPersonSchema.setBirthday(aLPDiskImportSchema.getBirthday());
            tLPPersonSchema.setIDType(aLPDiskImportSchema.getIDType());
            tLPPersonSchema.setIDNo(aLPDiskImportSchema.getIDNo());
            tLPPersonSchema.setOccupationType(aLPDiskImportSchema.
                    getOccupationType());
            tLPPersonSchema.setOthIDNo(aLPDiskImportSchema.getOthIDNo());
            tLPPersonSchema.setState("1");
            tLPPersonSchema.setOperator(mGlobalInput.Operator);
            tLPPersonSchema.setMakeDate(mCurrentDate);
            tLPPersonSchema.setMakeTime(mCurrentTime);
            tLPPersonSchema.setModifyDate(mCurrentDate);
            tLPPersonSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPPersonSchema, "DELETE&INSERT");
        }
        return customerNo;
    }

    /**
     * 设置合同信息
     * @param contNo String
     * @param aLPDiskImportSchema LPDiskImportSchema
     * @return boolean
     */
    private String setLPCont(String contNo, String insuredNo,
            LPDiskImportSchema aLPDiskImportSchema)
    {
        String valiDate = aLPDiskImportSchema.getEdorValiDate();
        if ((valiDate == null) || (valiDate.equals("")))
        {
            valiDate = mCValidate;
            aLPDiskImportSchema.setEdorValiDate(valiDate);
        }
        String proposalContNo = PubFun1.CreateMaxNo("PROPOSALCONTNO", null);
        LCContSchema tLCContSchema = CommonBL.getLCCont(contNo);
        LPContSchema tLPContSchema = new LPContSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPContSchema, tLCContSchema);
        tLPContSchema.setEdorNo(mEdorNo);
        tLPContSchema.setEdorType(mEdorType);
        tLPContSchema.setContNo(proposalContNo);
        tLPContSchema.setProposalContNo(proposalContNo);
        tLPContSchema.setPolType("0");
        tLPContSchema.setInsuredNo(insuredNo);
        tLPContSchema.setInsuredName(aLPDiskImportSchema.getInsuredName());
        tLPContSchema.setInsuredSex(aLPDiskImportSchema.getSex());
        tLPContSchema.setInsuredBirthday(aLPDiskImportSchema.getBirthday());
        tLPContSchema.setInsuredIDType(aLPDiskImportSchema.getIDType());
        tLPContSchema.setInsuredIDNo(aLPDiskImportSchema.getIDNo());
        tLPContSchema.setPeoples(1);
        tLPContSchema.setPrem(0);
        tLPContSchema.setSumPrem(0);
        tLPContSchema.setCValiDate(valiDate); //生效日期
        tLPContSchema.setOperator(mGlobalInput.Operator);
        tLPContSchema.setMakeDate(mCurrentDate);
        tLPContSchema.setMakeTime(mCurrentTime);
        tLPContSchema.setModifyDate(mCurrentDate);
        tLPContSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPContSchema, "DELETE&INSERT");

        LCContSchema tLCContPrama = new LCContSchema();
        tLCContPrama.setContNo(contNo); //批次的合同号
        tLCContPrama.setProposalContNo(proposalContNo); //新生成的合同号
        tLCContPrama.setInsuredNo(insuredNo);
        setLPPol(tLCContPrama, aLPDiskImportSchema);
        return proposalContNo;
    }

    /**
     * 设置被保人信息，LCinsured和LCCont是一对一关系
     * @param contNo String
     * @param aLPDiskImportSchema LPDiskImportSchema
     * @return boolean
     */
    private boolean setLPInsured(String contNo, String proposalContNo,
            String insuredNo, LPDiskImportSchema aLPDiskImportSchema)
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(contNo);
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() == 0)
        {
            return false;
        }
        LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(1);
        LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPInsuredSchema, tLCInsuredSchema);
        tLPInsuredSchema.setEdorNo(mEdorNo);
        tLPInsuredSchema.setEdorType(mEdorType);
        tLPInsuredSchema.setContNo(proposalContNo);
        tLPInsuredSchema.setInsuredNo(insuredNo);
        tLPInsuredSchema.setInsuredStat(aLPDiskImportSchema.getRetire());
        tLPInsuredSchema.setName(aLPDiskImportSchema.getInsuredName());
        tLPInsuredSchema.setSex(aLPDiskImportSchema.getSex());
        tLPInsuredSchema.setBirthday(aLPDiskImportSchema.getBirthday());
        tLPInsuredSchema.setIDType(aLPDiskImportSchema.getIDType());
        tLPInsuredSchema.setIDNo(aLPDiskImportSchema.getIDNo());
        tLPInsuredSchema.setOthIDNo(aLPDiskImportSchema.getOthIDNo());
        tLPInsuredSchema.setBankCode(aLPDiskImportSchema.getBankCode());
        tLPInsuredSchema.setBankAccNo(aLPDiskImportSchema.getBankAccNo());
        tLPInsuredSchema.setAccName(aLPDiskImportSchema.getAccName());
        tLPInsuredSchema.setOperator(mGlobalInput.Operator);
        tLPInsuredSchema.setMakeDate(mCurrentDate);
        tLPInsuredSchema.setMakeTime(mCurrentTime);
        tLPInsuredSchema.setModifyDate(mCurrentDate);
        tLPInsuredSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsuredSchema, "DELETE&INSERT");
        return true;
    }

    private boolean setLPPol(LCContSchema aLCContParam,
            LPDiskImportSchema aLPDiskImportSchema)
    {
        String valiDate = aLPDiskImportSchema.getEdorValiDate();
        if ((valiDate == null) || (valiDate.equals("")))
        {
            valiDate = mCValidate;
        }
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(aLCContParam.getContNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            String proposalNo = PubFun1.CreateMaxNo("PROPOSALNO", null);
            LPPolSchema tLPPolSchema = new LPPolSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLPPolSchema, tLCPolSchema);
            tLPPolSchema.setEdorNo(mEdorNo);
            tLPPolSchema.setEdorType(mEdorType);
            tLPPolSchema.setContNo(aLCContParam.getProposalContNo());
            tLPPolSchema.setProposalContNo(aLCContParam.getProposalContNo());
            tLPPolSchema.setPolNo(proposalNo);
            tLPPolSchema.setProposalNo(proposalNo);
            tLPPolSchema.setMasterPolNo(tLCPolSchema.getPolNo());
            tLPPolSchema.setPolTypeFlag("0");
            tLPPolSchema.setInsuredNo(aLCContParam.getInsuredNo());
            tLPPolSchema.setInsuredName(aLPDiskImportSchema.getInsuredName());
            tLPPolSchema.setInsuredSex(aLPDiskImportSchema.getSex());
            tLPPolSchema.setInsuredBirthday(aLPDiskImportSchema.getBirthday());
            /*
             * 解决 无名单团单无名单实名化过程中的年龄固定化的问题
             */
            tLPPolSchema.setInsuredAppAge(GetInsuredAppAge(aLPDiskImportSchema.getBirthday()));
            //-----------------------------------------------
            tLPPolSchema.setInsuredPeoples(0);
            tLPPolSchema.setPrem(0);
            tLPPolSchema.setSumPrem(0);
            tLPPolSchema.setCValiDate(valiDate);
            tLPPolSchema.setOperator(mGlobalInput.Operator);
            tLPPolSchema.setMakeDate(mCurrentDate);
            tLPPolSchema.setMakeTime(mCurrentTime);
            tLPPolSchema.setModifyDate(mCurrentDate);
            tLPPolSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPPolSchema, "DELETE&INSERT");

            LCPolSchema tLCPolParam = new LCPolSchema();
            tLCPolParam.setContNo(tLPPolSchema.getContNo());
            tLCPolParam.setPolNo(tLPPolSchema.getPolNo());
            tLCPolParam.setProposalNo(proposalNo);
            setLPDuty(tLCPolSchema.getPolNo(), tLCPolParam,
                    aLCContParam.getInsuredNo());
        }
        return true;
    }

    private boolean setLPDuty(String polNo, LCPolSchema aLCPolParam, String insuredNo)
    {
        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(polNo);
        LCDutySet tLCDutySet = tLCDutyDB.query();
        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            LCDutySchema tLCDutySchema = tLCDutySet.get(i);
            LPDutySchema tLPDutySchema = new LPDutySchema();
            Reflections ref = new Reflections();
            ref.transFields(tLPDutySchema, tLCDutySchema);
            tLPDutySchema.setEdorNo(mEdorNo);
            tLPDutySchema.setEdorType(mEdorType);
            tLPDutySchema.setContNo(aLCPolParam.getContNo());
            tLPDutySchema.setPolNo(aLCPolParam.getPolNo());
            tLPDutySchema.setPrem(0);
            tLPDutySchema.setSumPrem(0);
            tLPDutySchema.setOperator(mGlobalInput.Operator);
            tLPDutySchema.setMakeDate(mCurrentDate);
            tLPDutySchema.setMakeTime(mCurrentTime);
            tLPDutySchema.setModifyDate(mCurrentDate);
            tLPDutySchema.setModifyTime(mCurrentTime);
            mMap.put(tLPDutySchema, "DELETE&INSERT");

            LCDutySchema tLCDutyParam = new LCDutySchema();
            tLCDutyParam.setContNo(tLPDutySchema.getContNo());
            tLCDutyParam.setPolNo(tLPDutySchema.getPolNo());
            tLCDutyParam.setDutyCode(tLPDutySchema.getDutyCode());
            setLPPrem(tLCDutySchema.getPolNo(), tLCDutyParam);
            setLPGet(tLCDutySchema.getPolNo(), tLCDutyParam, insuredNo);
        }
        return true;
    }

    private boolean setLPPrem(String polNo, LCDutySchema aLCDutyParam)
    {
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(polNo);
        tLCPremDB.setDutyCode(aLCDutyParam.getDutyCode());
        LCPremSet tLCPremSet = tLCPremDB.query();
        for (int i = 1; i <= tLCPremSet.size(); i++)
        {
            LCPremSchema tLCPremSchema = tLCPremSet.get(i);
            LPPremSchema tLPPremSchema = new LPPremSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLPPremSchema, tLCPremSchema);
            tLPPremSchema.setEdorNo(mEdorNo);
            tLPPremSchema.setEdorType(mEdorType);
            tLPPremSchema.setContNo(aLCDutyParam.getContNo());
            tLPPremSchema.setPolNo(aLCDutyParam.getPolNo());
            tLPPremSchema.setPrem(0);
            tLPPremSchema.setSumPrem(0);
            tLPPremSchema.setOperator(mGlobalInput.Operator);
            tLPPremSchema.setMakeDate(mCurrentDate);
            tLPPremSchema.setMakeTime(mCurrentTime);
            tLPPremSchema.setModifyDate(mCurrentDate);
            tLPPremSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPPremSchema, "DELETE&INSERT");
        }
        return true;
    }

    private int GetInsuredAppAge(String tBirthday)
    {
    	String birthday = tBirthday;
    	String year = birthday.substring(0, birthday.indexOf("-"));
    	String month = birthday.substring(
    	        birthday.indexOf("-") + 1,
    	        birthday.indexOf("-") + 3);
    	String day = birthday
    	        .substring(birthday.lastIndexOf("-") + 1);

    	String today = null;
        today=mCValidate;    		
    	String tyear = today.substring(0, today.indexOf("-"));
    	String tmonth = today.substring(today.indexOf("-") + 1,
    	        today.indexOf("-") + 3);
    	String tday = today.substring(today.lastIndexOf("-") + 1);

    	int age = Integer.parseInt(tyear) - Integer.parseInt(year);
    	if (Integer.parseInt(tmonth) == Integer.parseInt(month))
    	{
    	    if (Integer.parseInt(tday) < Integer.parseInt(day))
    	    {
    	        age = age - 1;
    	    }
    	}
    	else if (Integer.parseInt(tmonth) < Integer.parseInt(month))
    	{
    	    age = age - 1;
    	}
    	System.out.println("Insured person's age is ： " + age);
    	
    	return age;
    }
    
    private boolean setLPGet(String polNo, LCDutySchema aLCDutyParam, String insuredNo)
    {
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setPolNo(polNo);
        tLCGetDB.setDutyCode(aLCDutyParam.getDutyCode());
        LCGetSet tLCGetSet = tLCGetDB.query();
        for (int i = 1; i <= tLCGetSet.size(); i++)
        {
            LCGetSchema tLCGetSchema = tLCGetSet.get(i);
            LPGetSchema tLPGetSchema = new LPGetSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLPGetSchema, tLCGetSchema);
            tLPGetSchema.setEdorNo(mEdorNo);
            tLPGetSchema.setEdorType(mEdorType);
            tLPGetSchema.setContNo(aLCDutyParam.getContNo());
            tLPGetSchema.setPolNo(aLCDutyParam.getPolNo());
            tLPGetSchema.setInsuredNo(insuredNo);
            tLPGetSchema.setOperator(mGlobalInput.Operator);
            tLPGetSchema.setMakeDate(mCurrentDate);
            tLPGetSchema.setMakeTime(mCurrentTime);
            tLPGetSchema.setModifyDate(mCurrentDate);
            tLPGetSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPGetSchema, "DELETE&INSERT");
        }
        return true;
    }
}

class NoNameInfo
{
    String contNo;

    String contPlanCode;

    int peoples;

    public String getContPlanCode()
    {
        return contPlanCode;
    }

    public int getPeoples()
    {
        return peoples;
    }

    public void setContNo(String contNo)
    {
        this.contNo = contNo;
    }

    public void setContPlanCode(String contPlanCode)
    {
        this.contPlanCode = contPlanCode;
    }

    public void setPeoples(int peoples)
    {
        this.peoples = peoples;
    }

    public String getContNo()
    {
        return contNo;
    }
}

