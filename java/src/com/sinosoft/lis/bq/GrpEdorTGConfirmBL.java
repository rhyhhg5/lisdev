package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体万能协议退保保全确认</p>
 * <p>Copyright: Copyright (c) 2005.4.1</p>
 * <p>Company: Sinosoft</p>
 * @author 
 * @version 2.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class GrpEdorTGConfirmBL implements EdorConfirm {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData;

	private MMap map = new MMap();
	private ExeSQL mExeSQL = new ExeSQL();
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	private ContCancel mContCalcel = new ContCancel(); // 退保保单转移核心类
	
	/** 数据操作字符串 */
	private String mOperate;

	/** 全局数据 */
	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
	private GlobalInput mGlobalInput = new GlobalInput();
	private String mEdorNo = null; // 受理号
	private String mEdorType = null; // 项目类型
	private String mGrpContNo = null; // 团单号

	public GrpEdorTGConfirmBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}
		System.out.println("---End getInputData---");

		if (!dealData()) {
			return false;
		}

		// 数据准备操作
		if (!prepareData()) {
			return false;
		}

		System.out.println("---End prepareData---");
		return true;
	}

	/**
	 * dealData 进行退保数据转移处理
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		// GrpEdorWTConfirmBL tGrpEdorWTConfirmBL = new GrpEdorWTConfirmBL();
		// if(!tGrpEdorWTConfirmBL.submitData(this.mInputData, ""))
		// {
		// mErrors.copyAllErrors(tGrpEdorWTConfirmBL.mErrors);
		// return false;
		// }
		//
		// VData rs = tGrpEdorWTConfirmBL.getResult();
		// MMap tMMap = (MMap) rs.getObjectByObjectName("MMap", 0);
		// map.add(tMMap);
		
		if(!validateEdorData()){
			return false;
		}
		if (!cancelULIContULI()) {
			return false;
		}
		if (!dealLCGrpCont()) {
			return false;
		}

		return true;
	}

	/**
	 * 处理个单退保
	 * 
	 * @return boolean：成功true；否则false
	 */
	private boolean cancelULIContULI() {
		MMap contPlanMap = new MMap();

		// 得到所有保单
		String sql = "select a.* " + "from LCCont a, LPPol b "
				+ "where a.contNo = b.contNo " + "   and b.edorNo = '"
				+ mEdorNo + "' " + "   and b.edorType = '" + mEdorType + "' "
				+ "   and b.grpContNo = '" + mGrpContNo + "' ";
		LCContDB tLCContDB = new LCContDB();
		LCContSet tLCContSet = null;
		VData data = new VData(); // 入库数据的容器

		int start = 1;
		int count = 10000; // 每次取count条个险数据进行处理

		do {
			tLCContSet = tLCContDB.executeQuery(sql, start, count);
			for (int j = 1; j <= tLCContSet.size(); j++) {
				MMap tMMap = cancelOneCont(tLCContSet.get(j));
				if (tMMap == null) {
					return false;
				}
				contPlanMap.add(tMMap);
			}
			// 若本次处理保单数 <= count，则所有数据一起提交，否则直接提交
			if (tLCContSet.size() <= count && start < count) {
				break;
			}

			data.clear();
			data.add(contPlanMap);
			PubSubmit p = new PubSubmit();
			if (!p.submitData(data, "")) {
				System.out.println(p.mErrors.getErrContent());
				CError tError = new CError();
				tError.moduleName = "GrpEdorWTAppConfirmBL";
				tError.functionName = "setLJSGetEndorse";
				tError.errorMessage = "生成财务数据失败";
				mErrors.addOneError(tError);
				return false;
			}

			contPlanMap = new MMap(); // 用这种方式清空待提交的数据
			start += count;
		} while (tLCContSet.size() > 0);

		map.add(contPlanMap);

		return true;
	}

	private boolean validateEdorData() {
		LPEdorItemDB tLPEdorItemDB=new LPEdorItemDB();
		tLPEdorItemDB.setEdorNo(mEdorNo);
		LPEdorItemSet tLPEdorItemSet=tLPEdorItemDB.query();
		for (int i = 1; i <= tLPEdorItemSet.size(); i++) {

			ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,
					mEdorNo, mEdorType, tLPEdorItemSet.get(i).getContNo(), "ContNo");
			String[] tables1 = { "LCInsureAcc", "LCInsureAccClass",
					"LCInsureAccFee", "LCInsureAccClassFee" };
			if (!validate.changeData(tables1)) {
				mErrors.copyAllErrors(validate.mErrors);
				return false;
			}
			MMap tmap = validate.getMap();
			map.add(tmap);
			String[] tables2 = { "LCInsureAccTrace", "LCInsureAccFeeTrace" };
			validate = new ValidateEdorData2(mGlobalInput, mEdorNo, mEdorType,
					tLPEdorItemSet.get(i).getContNo(), "ContNo");
			if (!validate.addData(tables2)) {
				mErrors.copyAllErrors(validate.mErrors);
				return false;
			}
			map.add(validate.getMap());
		}
		return true;
	}

	/**
	 * 处理个单退保 1、若被保人所有险种退保，则被保人退保。
	 * 2、由于被保人退保时会调用险种退保程序，所以被保人退保时不用存储preparePolData得到的数据 3、若该保单所有被保人退保，则保单退保
	 * 4、由于保单退保时会调用被保人退保程序，所以保单退保时不用存储prepareInsuredData得到的数据
	 * 
	 * @param tLCContSchema
	 *            LCContSchema：需要退保的保单
	 * @return MMap
	 */
	private MMap cancelOneCont(LCContSchema tLCContSchema) {
		mContCalcel.setEdorType(mEdorType);
		MMap contCTMap = new MMap(); // 个单退保数据，只存储整单退保prepareContData得到的数据
		MMap insuredCTMap = new MMap(); // 被保人退保数据，只存储被保人退保prepareInsuredData得到的数据
		int insuredCount = 0; // 该保单退保人数

		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
		tLCInsuredDB.setContNo(tLCContSchema.getContNo());
		LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
		for (int i = 1; i <= tLCInsuredSet.size(); i++) {
			MMap polCTMap = new MMap(); // 险种退保数据，只存储preparePolData得到的数据

			// 该被保人需要退保的险种退保
			LPPolDB tLPPolDB = new LPPolDB();
			tLPPolDB.setEdorNo(mEdorNo);
			tLPPolDB.setEdorType(mEdorType);
			tLPPolDB.setContNo(tLCContSchema.getContNo());
			tLPPolDB.setInsuredNo(tLCInsuredSet.get(i).getInsuredNo());
			LPPolSet tLPPolSet = tLPPolDB.query();
			for (int t = 1; t <= tLPPolSet.size(); t++) {
				MMap tMMap = mContCalcel.preparePolData(tLPPolSet.get(t)
						.getPolNo(), mEdorNo);
				polCTMap.add(tMMap);
			}

			// 若被保人所有险种退保，则被保人退保。
			// 由于被保人退保时会调用险种退保程序，所以被保人退保时不用存储preparePolData得到的数据
			LCPolDB tLCPolDB = new LCPolDB();
			tLCPolDB.setContNo(tLCContSchema.getContNo());
			tLCPolDB.setInsuredNo(tLCInsuredSet.get(i).getInsuredNo());
			LCPolSet tLCPolSet = tLCPolDB.query();
			int b=90;
			if (tLCPolSet.size() == tLPPolSet.size()) {
				MMap tMMap = mContCalcel.prepareInsuredData(tLCContSchema
						.getContNo(), tLCInsuredSet.get(i).getInsuredNo(),
						mEdorNo);
				insuredCTMap.add(tMMap);
				insuredCount++;
			} else {
				insuredCTMap.add(polCTMap);
			}
		}

		// 若该保单所有被保人退保，则保单退保
		// 由于保单退保时会调用被保人退保程序，所以保单退保时不用存储prepareInsuredData得到的数据
		if (tLCInsuredSet.size() == insuredCount) {
			MMap tMMap = mContCalcel.prepareContData(tLCContSchema.getContNo(),
					mEdorNo);
			contCTMap.add(tMMap);
		} else {
			contCTMap.add(insuredCTMap);
		}

		return contCTMap;
	}

	/**
	 * dealLCGrpPol 1、若团险下所有个险退保，则团险退保
	 * 
	 * @return boolean：成功true；否则false
	 */
	private boolean dealLCGrpCont() {
		int grpPolCTCount = 0; // 团险种退保个数

		LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
		tLCGrpPolDB.setGrpContNo(mGrpContNo);
		LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
		for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
			// 该团险下个险数
			int polCountC = getLCPolCount(tLCGrpPolSet.get(i).getGrpPolNo());
			if (polCountC == Integer.MIN_VALUE) {
				return false;
			}

			// 该团险下退保的个险数
			int polCountP = getLPPolCount(tLCGrpPolSet.get(i).getGrpPolNo());
			if (polCountP == Integer.MIN_VALUE) {
				return false;
			}

			// 若团险下所有个险退保，则团险退保
			if (polCountC == polCountP) {
				MMap tMMap = mContCalcel.prepareGrpPolData(tLCGrpPolSet.get(i)
						.getGrpPolNo(), mEdorNo);
				if (tMMap == null) {
					return false;
				}
				map.add(tMMap);

				grpPolCTCount++;
			}
		}

		// 若团险下所有团险退保，则团单退保
		if (tLCGrpPolSet.size() == grpPolCTCount) {
			MMap tMMap = mContCalcel.prepareGrpContData(mGrpContNo, mEdorNo);
			if (tMMap == null) {
				return false;
			}
			map.add(tMMap);
		}

		return true;
	}

	/**
	 * 团险下个险数
	 * 
	 * @param tGrpPolNo
	 *            String
	 * @return int
	 */
	private int getLCPolCount(String tGrpPolNo) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select count(1) ").append("from LCPol ").append(
				"where grpPolNo = '").append(tGrpPolNo).append("' ");
		String tLPPolCount = mExeSQL.getOneValue(sql.toString());
		if (mExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "GrpEdorWTConfirmBL";
			tError.functionName = "getLPPolCount";
			tError.errorMessage = "查询退保险种数出错";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return Integer.MIN_VALUE;
		}

		if (tLPPolCount.equals("") || tLPPolCount.equals("null")) {
			return 0;
		}
		return Integer.parseInt(tLPPolCount);
	}

	/**
	 * 退保个险数量
	 * 
	 * @return int
	 */
	private int getLPPolCount(String tGrpPolNo) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select count(1) ").append("from LPPol ").append(
				"where edorNo = '").append(mEdorNo).append(
				"'  and edorType = '").append(mEdorType).append(
				"'  and grpPolNo = '").append(tGrpPolNo).append("' ");
		String tLPPolCount = mExeSQL.getOneValue(sql.toString());

		if (mExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "GrpEdorWTConfirmBL";
			tError.functionName = "getLPPolCount";
			tError.errorMessage = "查询退保险种数出错";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return Integer.MIN_VALUE;
		}

		if (tLPPolCount.equals("") || tLPPolCount.equals("null")) {
			return 0;
		}
		return Integer.parseInt(tLPPolCount);
	}

	public VData getResult() {
		return mResult;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) mInputData
					.getObjectByObjectName("LPGrpEdorItemSchema", 0);
			mGlobalInput.setSchema((GlobalInput) mInputData
					.getObjectByObjectName("GlobalInput", 0));
		} catch (Exception e) {
			CError.buildErr(this, "接收数据失败");
			return false;
		}

		if (mLPGrpEdorItemSchema == null || mGlobalInput == null) {
			CError tError = new CError();
			tError.moduleName = "GrpEdorNIAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}

		LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
		tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
		tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
		tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
		LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
		if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1) {
			mErrors.copyAllErrors(tLPGrpEdorItemDB.mErrors);
			mErrors.addOneError(new CError("查询保全项目信息失败！"));
			return false;
		}
		mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemSet.get(1));

		LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
		tLPGrpEdorMainDB
				.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
		tLPGrpEdorMainDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
		if (!tLPGrpEdorMainDB.getInfo()) {
			mErrors.copyAllErrors(tLPGrpEdorMainDB.mErrors);
			mErrors.addOneError(new CError("查询保全信息失败！"));
			return false;
		}
		mLPGrpEdorMainSchema = tLPGrpEdorMainDB.getSchema();
		mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
		mEdorType = mLPGrpEdorItemSchema.getEdorType();
		mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();

		return true;
	}

	/**
	 * 准备需要保存的数据
	 */
	private boolean prepareData() {
		mResult.clear();

		mLPGrpEdorMainSchema.setEdorState("0");
		mLPGrpEdorMainSchema.setConfDate(PubFun.getCurrentDate());
		mLPGrpEdorMainSchema.setConfOperator(mGlobalInput.Operator);
		// 当前日期为保全生效日期
		mLPGrpEdorMainSchema.setEdorValiDate(PubFun.getCurrentDate());

		mLPGrpEdorItemSchema.setEdorState("0");
		mLPGrpEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
		mLPGrpEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
		mLPGrpEdorItemSchema.setOperator(mGlobalInput.Operator);

		map.put(mLPGrpEdorItemSchema, "UPDATE");
		map.put(mLPGrpEdorMainSchema, "UPDATE");

		mResult.add(map);
		return true;
	}

	/**
	 * 调试方法
	 */
	public static void main(String[] args) {
		GlobalInput gi = new GlobalInput();
		gi.Operator = "endor0";
		gi.ComCode = "86";

		LPGrpEdorItemDB db = new LPGrpEdorItemDB();
		db.setEdorNo("20070207000002");
		db.setEdorType("XT");
		LPGrpEdorItemSet set = db.query();

		VData data = new VData();
		data.add(gi);
		data.add(set.get(1));

		GrpEdorTGConfirmBL bl = new GrpEdorTGConfirmBL();
		if (!bl.submitData(data, "")) {
			System.out.println(bl.mErrors.getErrContent());
		}
	}
}
