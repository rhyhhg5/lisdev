package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全客户资料变更项目</p>
 * <p>Description: 保全确认</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorICConfirmBL implements EdorConfirm
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mCustomerNo = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        getInputData(data);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private void getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                "GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) data.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        mEdorNo = mLPEdorItemSchema.getEdorNo();
        mContNo = mLPEdorItemSchema.getContNo();
        mEdorType = mLPEdorItemSchema.getEdorType();
        mCustomerNo = mLPEdorItemSchema.getInsuredNo();
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        setLDPerson();
        setLCContSub();
//        使当前操作保全申请的保单数据生效
        if (!validateEdorData())
        {
            return false;
        }
/*      同时处理其他关联个单的保单数据生效,分为两种情况:
 *		1.变更资料的客户在其他个险保单中作为投保人存在.
 *		2.存在其他个险保单和变更资料的客户所在个险保单的投保人和被保人均一致,也在本工单一并处理的情况.
 */
        if(!validateOtherContP())
        {
        	return false;
        }
//        同时处理该客户作为其他团单的保单数据生效
        if(!validateOtherCont())
        {
        	return false;
        }
        return true;
    }

    /**
     * 使保全数据生效
     */
    private boolean validateEdorData()
    {
    	//LIDO 增加帐户表的交换，lcinsureacc ,lcinsureaccclass,lcinsureaccfee,lcinsureaccclassfee
    	//增加 lcinsureacctrace,lcinsureaccfeetrace  
    	String[] addTables = {"LCInsureAccTrace","LCInsureAccFeeTrace"};
        String[] tables = {"LCAppnt", "LCInsured",
                "LCCont", "LCPol", "LCDuty", "LCPrem", "LCGet","LCInsureAcc", "LCInsureAccFee","LCInsureAccClass","LCInsureAccClassFee"};
        ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,
                mEdorNo, mEdorType,mContNo,"ContNo");
        if (!validate.addData(addTables))
        {
            mErrors.copyAllErrors(validate.mErrors);
            return false;
        }
        if (!validate.changeData(tables))
        {
            mErrors.copyAllErrors(validate.mErrors);
            return false;
        }
        MMap map = validate.getMap();
        mMap.add(map);
        
        //团险学平险汇交件保单变更投保人信息
        String strXFlag="select grpcontno from lcgrpcont where grpcontno= "
			+ " (select grpcontno from lccont where contno='"+ mContNo +"') "
			+ " and ContPrintType='5' with ur ";

        ExeSQL texesql=new ExeSQL();
        String grpContNo=texesql.getOneValue(strXFlag);
        if(null!=grpContNo && ""!=grpContNo){
        	LCInsuredListDB tLCInsuredListDB=new LCInsuredListDB();
        	LBInsuredListDB tLBInsuredListDB=new LBInsuredListDB();
        	LCInsuredListSchema tLCInsuredListSchema=null;
        	LBInsuredListSchema tLBInsuredListSchema=null;
        	
        	tLCInsuredListDB.setGrpContNo(mEdorNo);
        	tLCInsuredListDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        	tLCInsuredListDB.setEdorNo(mEdorNo);
        	LCInsuredListSet tLCInsuredListSet=tLCInsuredListDB.query();
        	
        	tLBInsuredListDB.setGrpContNo(grpContNo);
        	tLBInsuredListDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        	LBInsuredListSet tLBInsuredListSet=tLBInsuredListDB.query();
        	
        	if(tLCInsuredListSet.size()>0 && tLBInsuredListSet.size()>0){
        		tLCInsuredListSchema=tLCInsuredListSet.get(1);
        		tLBInsuredListSchema=tLBInsuredListSet.get(1);
        	}
        	if(null!=tLCInsuredListSchema && null!=tLBInsuredListSchema){
        		tLBInsuredListSchema.setAppntName(tLCInsuredListSchema.getAppntName());
        		tLBInsuredListSchema.setAppntBirthday(tLCInsuredListSchema.getAppntBirthday());
        		tLBInsuredListSchema.setAppntIdNo(tLCInsuredListSchema.getAppntIdNo());
        		tLBInsuredListSchema.setAppntIdType(tLCInsuredListSchema.getAppntIdType());
        		tLBInsuredListSchema.setAppntSex(tLCInsuredListSchema.getAppntSex());
        		mMap.put(tLBInsuredListSchema,"DELETE&INSERT");
        	}
        }
        
        
        return true;
    }

    /**
     * 设置客户信息
     */
    private void setLDPerson()
    {
        LPPersonDB tLPPersonDB = new LPPersonDB();
        tLPPersonDB.setEdorNo(mEdorNo);
        tLPPersonDB.setEdorType(mEdorType);
        // added by huxl 2006-11-14 解决CM重复交换数据
        tLPPersonDB.setCustomerNo(mCustomerNo);
        LPPersonSet tLPPersonSet = tLPPersonDB.query();
        for (int i = 1; i <= tLPPersonSet.size(); i++)
        {
            LPPersonSchema tLPPersonSchema = tLPPersonSet.get(i);
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(tLPPersonSchema.getCustomerNo());
            tLDPersonDB.getInfo();
            LDPersonSchema tLDPersonSchema = new LDPersonSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLDPersonSchema, tLPPersonSchema);
            ref.transFields(tLPPersonSchema, tLDPersonDB.getSchema());
            mMap.put(tLDPersonSchema, "DELETE&INSERT");
            mMap.put(tLPPersonSchema, "DELETE&INSERT");
        }
    }
    
    /**
     * add by xp 2011-7-15
     * 同时处理其他关联个单的保单数据生效,分为两种情况:
 *		1.变更资料的客户在其他个险保单中作为投保人存在.
 *		2.存在其他个险保单和变更资料的客户所在个险保单的投保人和被保人均一致,也在本工单一并处理的情况.
     */
    private boolean validateOtherContP()
    {
    	StringBuffer sql = new StringBuffer();
    	sql.append("select contno from lpcont where edorno = '" +mEdorNo + "' ");
    	sql.append("and edortype = '" + mEdorType + "' ");
    	sql.append("and conttype = '1' ");
    	sql.append("and contno <> '" + mContNo + "'");
    	System.out.println("查询关联保单："+sql);
    	SSRS tSSRS = new SSRS();
    	tSSRS = new ExeSQL().execSQL(sql.toString());
    	
    	if(tSSRS.MaxRow==0)
    	{
    		System.out.println("被保人"+mCustomerNo+"没有其它关联团单需要变更");
    		return true;
    	}
    	else
    	{
    		for(int i=1;i<=tSSRS.MaxRow;i++)
    		{
    			String tContNo = tSSRS.GetText(i, 1);
    			System.out.println("PEdorICConfirmBL.java-L179->保单："+tContNo);
    			String[] addTables = {"LCInsureAccTrace","LCInsureAccFeeTrace"};
    	        String[] tables = {"LCAppnt", "LCInsured",
    	                "LCCont", "LCPol", "LCDuty", "LCPrem", "LCGet","LCInsureAcc", "LCInsureAccFee","LCInsureAccClass","LCInsureAccClassFee"};
    	        ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,
    	                mEdorNo, mEdorType,tContNo,"ContNo");
    	        if (!validate.addData(addTables))
    	        {
    	            mErrors.copyAllErrors(validate.mErrors);
    	            return false;
    	        }
    	        if (!validate.changeData(tables))
    	        {
    	            mErrors.copyAllErrors(validate.mErrors);
    	            return false;
    	        }
    	        MMap map = validate.getMap();
    	        mMap.add(map);
    		}
    	}
        return true;
    }
    
    /**
     * 同时处理该客户作为其它团单被保人的资料变更
     * 20090320 zhanggm cbs00024868 
     */
    private boolean validateOtherCont()
    {
    	StringBuffer sql = new StringBuffer();
    	sql.append("select contno from lcinsured where insuredno = '" +mCustomerNo + "' ");
    	sql.append("and grpcontno <> '" + BQ.GRPFILLDATA + "' ");
    	sql.append("and contno <> '" + mContNo + "'");
    	System.out.println("查询关联保单："+sql);
    	SSRS tSSRS = new SSRS();
    	tSSRS = new ExeSQL().execSQL(sql.toString());
    	
    	if(tSSRS.MaxRow==0)
    	{
    		System.out.println("被保人"+mCustomerNo+"没有其它关联团单需要变更");
    		return true;
    	}
    	else
    	{
    		for(int i=1;i<=tSSRS.MaxRow;i++)
    		{
    			String tContNo = tSSRS.GetText(i, 1);
    			System.out.println("PEdorICConfirmBL.java-L179->保单："+tContNo);
    			String[] addTables = {"LCInsureAccTrace","LCInsureAccFeeTrace"};
    	        String[] tables = {"LCAppnt", "LCInsured",
    	                "LCCont", "LCPol", "LCDuty", "LCPrem", "LCGet","LCInsureAcc", "LCInsureAccFee","LCInsureAccClass","LCInsureAccClassFee"};
    	        ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,
    	                mEdorNo, mEdorType,tContNo,"ContNo");
    	        if (!validate.addData(addTables))
    	        {
    	            mErrors.copyAllErrors(validate.mErrors);
    	            return false;
    	        }
    	        if (!validate.changeData(tables))
    	        {
    	            mErrors.copyAllErrors(validate.mErrors);
    	            return false;
    	        }
    	        MMap map = validate.getMap();
    	        mMap.add(map);
    		}
    	}
        return true;
    }
    private boolean setLCContSub(){
    	LPContSubDB tlpContSubDB = new LPContSubDB();
    	tlpContSubDB.setEdorNo(mEdorNo);
    	tlpContSubDB.setEdorType(mEdorType);
    	tlpContSubDB.setPrtNo(new ExeSQL().getOneValue("select prtno from lccont where contno = '"+mContNo+"' union select prtno from lbcont where contno = '"+mContNo+"'"));
        LPContSubSet tlpcContSubSet = tlpContSubDB.query();
        for (int i = 1; i <= tlpcContSubSet.size(); i++)
        {
            LPContSubSchema tLPContSubSchema = tlpcContSubSet.get(i);
            LCContSubDB tlcContSubDB = new LCContSubDB();
            tlcContSubDB.setPrtNo(tLPContSubSchema.getPrtNo());
            tlcContSubDB.getInfo();
            LCContSubSchema tLcContSubSchema= new LCContSubSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLcContSubSchema, tLPContSubSchema);
            ref.transFields(tLPContSubSchema, tlcContSubDB.getSchema());
            mMap.put(tLcContSubSchema, "DELETE&INSERT");
            mMap.put(tLPContSubSchema, "DELETE&INSERT");
        }
    	return true;
    }
}
