package com.sinosoft.lis.bq;

import java.io.*;
import org.jdom.input.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 重新生成批单</p>
 * <p>Description: 新生成的批单中包含了交费方式等内容 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class RenewGrpVts
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 保全受理号 */
    private String mEdorNo;

    /** 保存批单数据的Schema */
    private LPEdorPrintSchema mPrintSchema = new LPEdorPrintSchema();

    /**
     * 构造函数
     * @param edorAcceptNo String
     */
    public RenewGrpVts(String edorNo)
    {
        this.mEdorNo = edorNo;
    }

    /**
     * 重新生成批单
     * @param payData TransferData
     */
    public boolean doRenew(TransferData payInfo)
    {
        try
        {
            XmlExport xmlExport = new XmlExport();
            xmlExport.setDocument(getDocument());
            addPayMode(xmlExport, payInfo);
            if (!updateData(xmlExport))
            {
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 把xml数据转换成Document
     * @return Document
     */
    private org.jdom.Document getDocument() throws Exception
    {
        InputStream xmlStream = getXmlStream();
        SAXBuilder saxBuilder = new SAXBuilder();
        org.jdom.Document document = saxBuilder.build(xmlStream);
        return document;
    }

    /**
     * 从数据库中查出生成批单的xml数据
     * @return InputStream
     */
    private InputStream getXmlStream()
    {
        LPEdorPrintDB tLPEdorPrintDB = new LPEdorPrintDB();
        tLPEdorPrintDB.setEdorNo(mEdorNo);
        tLPEdorPrintDB.getInfo();
        //把查出的数据保存到mPrintSchema中
        mPrintSchema.setSchema(tLPEdorPrintDB.getSchema());
        return tLPEdorPrintDB.getEdorInfo();
    }

    /**
     * 向批单中添加交费方式等内容
     * @param xmlExport XmlExport
     */
    private boolean addPayMode(XmlExport xmlExport, TransferData payInfo)
            throws Exception
    {
        String payMode = (String) payInfo.getValueByName("payMode");
        String payDate = (String) payInfo.getValueByName("payDate"); //转帐日期
        String endDate = (String) payInfo.getValueByName("endDate"); //截止日期
        String bank = (String) payInfo.getValueByName("bank");
        String bankAccno = (String) payInfo.getValueByName("bankAccno");
        String outPayMode = ""; //交费方式
        String outNotice = "";  //注意事项
        String getNoticeNo = "";  //退费记录号
        TextTag textTag = new TextTag();

        //得到变更后的差额
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(mEdorNo);
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
        if (tLPGrpEdorMainSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "RenewGrpVts";
            tError.functionName = "addPayMode";
            tError.errorMessage = "查不到受理信息！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //缴费方式
        if (payMode == null)
        {
            CError tError = new CError();
            tError.moduleName = "RenewGrpVts";
            tError.functionName = "addPayMode";
            tError.errorMessage = "交费方式填写错误！";
            this.mErrors.addOneError(tError);
            return false;
        }
        double getMoney = tLPGrpEdorMainSet.get(1).getGetMoney();
        //交费
        if (getMoney > 0)
        {
            //现金
            if (payMode.equals("1"))
            {
                outPayMode = "您的交费方式为：现金，请凭本通知书在" +
                        CommonBL.decodeDate(endDate) +
                        "前到我公司交费。";
                outNotice = "超过以上日期本公司未收到应收保费的，本批注内容无效。";
            }
            //支票
            if (payMode.equals("2"))
            {
                outPayMode = "您的交费方式为：支票，请凭本通知书在" +
                        CommonBL.decodeDate(endDate) +
                        "前到我公司交费。";
                outNotice = "超过以上日期本公司未收到应收保费的，本批注内容无效。";
            }
            //银行转帐
            else if (payMode.equals("4"))
            {
                outPayMode = "您的交费方式为：银行转帐，交费银行为：" +
                        ChangeCodeBL.getCodeName("Bank", bank) +
                        "，账号为：" + bankAccno +
                        "，转帐总金额为：" + Math.abs(getMoney) +
                        "元。\n我公司将在" + CommonBL.decodeDate(payDate) +
                        "进行转账，请在转账日前确认该账号是否有足够金额。";
                outNotice = "本公司未收到应收保费的，本批注内容无效。";
            }
        }
        //退费
        else if (getMoney < 0)
        {
            //现金
            if (payMode.equals("1"))
            {
                outPayMode = "退费方式为：现金，请凭本通知书到我公司领款。";
            }
            //银行转帐
            else if (payMode.equals("4"))
            {
                outPayMode = "退费方式为：转帐支票，交易银行为：" +
                        ChangeCodeBL.getCodeName("Bank", bank) +
                        "，交易账号为：" + bankAccno +
                        "，转帐总金额为：" + Math.abs(getMoney) +
                        "元。\n我公司将在" + CommonBL.decodeDate(payDate) +
                        "进行转账，请注意查收。";
            }
            //得到退费记录号
            String sql = "select ActuGetNo from LJAGet " +
                         "where OtherNo = '" + mEdorNo + "' " +
                         "and OtherNoType = '3' ";
            ExeSQL tExeSQL = new ExeSQL();
            getNoticeNo = tExeSQL.getOneValue(sql);
            if (getNoticeNo != null)
            {
                getNoticeNo = "退费记录号：" + getNoticeNo;
                textTag.add("GetNoticeNo", getNoticeNo);
            }
        }
        textTag.add("PayMode", outPayMode);
        textTag.add("Notice", outNotice);
        xmlExport.addTextTag(textTag);
        xmlExport.outputDocumentToFile("c:\\", "xmlexport");
        return true;
    }

    /**
     * 向数据库中更新批单的Blob
     * @param xmlExport XmlExport
     */
    private boolean updateData(XmlExport xmlExport) throws Exception
    {
        LPEdorPrintSchema printSchema = new LPEdorPrintSchema();
        printSchema.setSchema(mPrintSchema);
        printSchema.setEdorInfo(xmlExport.getInputStream());
        printSchema.setModifyDate(PubFun.getCurrentDate());
        printSchema.setModifyTime(PubFun.getCurrentTime());
        MMap map = new MMap();
        map.put(printSchema, "BLOBUPDATE");

        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "RenewGrpVts";
            tError.functionName = "updateData";
            tError.errorMessage = "批单数据提交失败!";
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        TransferData payData = new TransferData();
        payData.setNameAndValue("payMode", "1");
        payData.setNameAndValue("payDate", "");
        payData.setNameAndValue("endDate", "2005-07-25");
        payData.setNameAndValue("bank", "");

        RenewGrpVts ren = new RenewGrpVts("20050722000001");
        ren.doRenew(payData);
    }
}
