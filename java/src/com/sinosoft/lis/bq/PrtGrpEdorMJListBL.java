package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.task.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import java.util.*;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.f1print.PrintPDFManagerBL;
import com.sinosoft.lis.f1print.PrintService;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PrtGrpEdorMJListBL  implements PrintService{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = new TransferData();

    private GlobalInput mGlobalInput = null;

    private TextTag mTextTag = null;

    private XmlExport mXmlExport = null;

    private MMap map = new MMap();

    private LGWorkDB mLGWorkDB = null;

    private LPGrpPolSet mLPGrpPolSet = null;

    private String mGetNoticeNo = null;

    private VData mResult = new VData();
    private int needPrt = -1;
    private String mOperate = "";
    private String PrintManagerCode = "";
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    public PrtGrpEdorMJListBL() {
    }
    public CErrors getErrors() {
        return mErrors;
    }
    public VData getResult() {
    return this.mResult;
}


    /**
     * 操作提交方法
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String operate) {
        mOperate = operate;
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        if (!submit()) {
            return false;
        }

        return true;
    }

    private boolean checkData()
    {

        LPGrpPolDB tLPGrpPolDB = new LPGrpPolDB();
        tLPGrpPolDB.setEdorNo(mLGWorkDB.getWorkNo());
        tLPGrpPolDB.setEdorType(BQ.EDORTYPE_MJ);

        mLPGrpPolSet = tLPGrpPolDB.query();
        if(mLPGrpPolSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PrtGrpEdorMJListBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有查询到险种信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * 得到传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if(mOperate.equals("PRINT"))
        {//已经存入打印管理表
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
            if(tLOPRTManagerSchema == null)
            {// @@错误处理
                CError tError = new CError();
                tError.moduleName = "PrtGrpEdorMJListBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "LOPRTManagerSchema没有得到数据，请您确认!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mTransferData.setNameAndValue("edorAcceptNo", tLOPRTManagerSchema.getStandbyFlag2());
            mTransferData.setNameAndValue("edorType", tLOPRTManagerSchema.getStandbyFlag1());
            mTransferData.setNameAndValue("prtType",tLOPRTManagerSchema.getStandbyFlag4());
        }
        else if(mOperate.equals("INSERT"))
        {//未存入打印管理表
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
        }
        mLGWorkDB = getLGWorkInfo();
        if (mGlobalInput == null || mTransferData == null || mLGWorkDB == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据，生成结算单
     * @return boolean
     */
    private boolean dealData()
    {
        mXmlExport = new XmlExport();
        mXmlExport.createDocument("PrtGrpEdorMJLis.vts", "printer");
        mTextTag = new TextTag();

        String prtType = (String) mTransferData.getValueByName("prtType");

        if (!setCommonData())
        {
            return false;
        }

        if (prtType.equals("cal"))
        {
            mTextTag.add("Title", "人保健康特需医疗满期结算单");
            mTextTag.add("BarCode1", mLGWorkDB.getWorkNo());
            mTextTag.add("BarCodeParam1", "BarHeight=25&BarRation=3&BarWidth=1"
                         +"&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
            mTextTag.add("MJNo", "结算单号：" + mLGWorkDB.getWorkNo());
            PrintManagerCode = PrintPDFManagerBL.CODE_CAL;
            if (!setCalInfo())
            {
                return false;
            }
        }
        else if (prtType.equals("MJDeal"))
        {
            mTextTag.add("Title", "人保健康满期终止批单");
            PrintManagerCode = PrintPDFManagerBL.CODE_MJ;
            if (!setMJInfo())
            {
                return false;
            }
        }
        else if (prtType.equals("XBDeal"))
        {
            mTextTag.add("Title", "人保健康特需医疗续保批单");
            PrintManagerCode = PrintPDFManagerBL.CODE_XB;
            if (!setXBInfo())
            {
                return false;
            }
        }

        mXmlExport.addTextTag(mTextTag);
        mXmlExport.outputDocumentToFile("C:\\", "prtGrpEdorMJList.xml");
        mResult.clear();
        mResult.addElement(mXmlExport);

        //放入打印列表
        if(mOperate.equals("INSERT"))
        {
            LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setStandbyFlag2(mLGWorkDB.getWorkNo());
            tLOPRTManagerDB.setCode(PrintManagerCode);
            tLOPRTManagerSet = tLOPRTManagerDB.query();
            needPrt = tLOPRTManagerSet.size();
            if (needPrt == 0)
            { //没有数据,进行封装
                LCGrpContDB tLCGrpContDB = new LCGrpContDB();
                tLCGrpContDB.setGrpContNo(mLGWorkDB.getContNo());
                if(!tLCGrpContDB.getInfo())
                {
                    mErrors.addOneError("准备打印管理表数据时，不存在"+mLGWorkDB.getContNo()+"号团单。");
                    return false;
                }
                LCGrpContSchema tLCGrpContSchema = tLCGrpContDB.getSchema();

                String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
                String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                mLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
                mLOPRTManagerSchema.setOtherNoType("01");
                mLOPRTManagerSchema.setCode(PrintManagerCode);
                mLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
                mLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
                mLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
                mLOPRTManagerSchema.setReqOperator(tLCGrpContSchema.getOperator());
                mLOPRTManagerSchema.setPrtType("0");
                mLOPRTManagerSchema.setStateFlag("0");
                mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                mLOPRTManagerSchema.setStandbyFlag1(BQ.EDORTYPE_MJ); //这里存放edorType
                mLOPRTManagerSchema.setStandbyFlag2(mLGWorkDB.getWorkNo()); //这里存放WorkNo
                mLOPRTManagerSchema.setStandbyFlag3(tLCGrpContSchema.getGrpContNo()); //这里存放GrpContNo
                mLOPRTManagerSchema.setStandbyFlag4((String) mTransferData.getValueByName("prtType")); //这里存放prtType
                map.put(mLOPRTManagerSchema,"INSERT");
            }
        }
        return true;
    }

    /**
     * 生成满期结算批单
     * @return boolean
     */
    private boolean setCalInfo()
    {
        mXmlExport.addDisplayControl("displayCal");
        if (!setAccInfo())
        {
            return false;
        }

        if (!setFeedBackInfo())
        {
            return false;
        }

        return true;
    }

    /**
     * 生成满期终止批单
     * @return boolean
     */
    private boolean setMJInfo()
    {
        mXmlExport.addDisplayControl("displayMJ");

        if (!setMJForCustomerInfo())
        {
            return false;
        }

        return true;
    }

    /**
     * 生成满期终止单的客户联信息
     * @return boolean
     */
    private boolean setMJForCustomerInfo()
    {
        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setOtherNo(mLGWorkDB.getWorkNo());
        LJAGetSet set = tLJAGetDB.query();
        if (set.size() == 0)
        {
            mErrors.addOneError("没有财务退费信息。");
            return false;
        }

        if (!setGetNoticeNo(set.get(1).getActuGetNo()))
        {
            return false;
        }

        mTextTag.add("EdorAppSex", "先生/女士");
        mTextTag.add("NoticeHead", "根据本公司于"
                     + CommonBL.decodeDate(mLGWorkDB.getMakeDate())
                     + "收到的服务申请，对您的保单作如下批注：");

        ListTable tlistTable = new ListTable();
        tlistTable.setName("MJ");
        String[] content = new String[1];
        String totleBala = getTotalBala();
        content[0] = "保单号" + mLGWorkDB.getContNo() + "满期终止，满期日："
                     + CommonBL.decodeDate(getEndDate(mLGWorkDB.getContNo()))
                     + "\n退还账户金额合计：" + totleBala
                     + "元。\n该保单保险责任终止。";
        tlistTable.add(content);
        mXmlExport.addListTable(tlistTable, new String[1]);

        mTextTag.add("TotalBala", totleBala);
        if (!setGetMoneyWay(set.get(1))) {
            return false;
        }

        return true;
    }

    /**
     * 得到满期日
     * @return String
     */
    private String getEndDate(String grpContNo) {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        tLCGrpContDB.getInfo();

        return tLCGrpContDB.getCInValiDate();
    }

    /**
     * 领款方式
     * @param tLJAGetSchema LJAGetSchema
     * @return boolean
     */
    private boolean setGetMoneyWay(LJAGetSchema tLJAGetSchema) {
        StringBuffer getMoneyWay = new StringBuffer("您选择的账户领取方式为：");
        String payMode = tLJAGetSchema.getPayMode();
        if(payMode == null || payMode.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PrtGrpEdorMJListBL";
            tError.functionName = "setGetMoneyWay";
            tError.errorMessage = "没有查询到录入的账户领取方式";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("paymode");
        tLDCodeDB.setCode(payMode);
        tLDCodeDB.getInfo();

        getMoneyWay.append(tLDCodeDB.getCodeName());

        //银行转帐
        if (payMode.equals("4")) {
            getMoneyWay.append("转帐时间 ")
                    .append(CommonBL.decodeDate(tLJAGetSchema.getShouldDate()))
                    .append("，转帐银行 ")
                    .append(ChangeCodeBL.getCodeName("Bank",
                    tLJAGetSchema.getBankCode(),
                    "BankCode"))
                    .append("，转帐帐号 ")
                    .append(tLJAGetSchema.getBankAccNo());
        }
        getMoneyWay.append("。");
        mTextTag.add("GetMoneyWay", getMoneyWay.toString());

        return true;
    }

    /**
     * 计算满期终止总余额
     * @return String
     */
    private String getTotalBala() {
        String[] detailType = {BQ.ACCBALA_FIEXD, BQ.DETAILTYPE_INTEREST_FIXED,
                BQ.ACCBALA_GROUP, BQ.DETAILTYPE_INTEREST_GROUP,
                BQ.ACCBALA_INSURED, BQ.DETAILTYPE_INTEREST_INSURED};
        String detailTypes = "";
        for (int i = 0; i < detailType.length; i++) {
            detailTypes += "'" + detailType[i] + "',";
        }
        detailTypes = detailTypes.substring(0, detailTypes.lastIndexOf(","));

        StringBuffer sql = new StringBuffer();
        sql.append("select sum(decimal(edorValue, 20, 3)) ")
                .append("from LPEdorEspecialData ")
                .append("where edorAcceptNo = '")
                .append(mLGWorkDB.getWorkNo())
                .append("'  and edorType = '")
                .append(BQ.EDORTYPE_MJ)
                .append("'  and detailType in (").append(detailTypes).append(
                ")");
        ExeSQL tExeSQL = new ExeSQL();
        String result = tExeSQL.getOneValue(sql.toString());
        if (result == null || result.equals("")) {
            result = "0";
        }

        return CommonBL.bigDoubleToCommonString(
                Double.parseDouble(result), "0.00");
    }

    /**
     * 得到退费通知书号
     * @return boolean
     */
    private boolean setGetNoticeNo(String actuGetNo) {

        mTextTag.add("GetNoticeNo", "退费通知号：" + actuGetNo);

        mTextTag.add("BarCode1", actuGetNo);
        mTextTag.add("BarCodeParam1", "BarHeight=25&BarRation=3&BarWidth=1"
                     + "&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        mTextTag.add("BarCode2", actuGetNo);
        mTextTag.add("BarCodeParam2", "BarHeight=25&BarRation=3&BarWidth=1"
                     + "&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        return true;
    }

    /**
     * 生成批单公共部分信息
     * @return boolean
     */
    private boolean setCommonData() {
        mTextTag.add("EndDate", mLGWorkDB.getMakeDate()); //在满期日期生成结算任务
        mTextTag.add("AppntName", mLGWorkDB.getCustomerName());
        mTextTag.add("CustomerNo", mLGWorkDB.getCustomerNo());
        mTextTag.add("Operator",
                     GetMemberInfo.getMemberName(mLGWorkDB.getOperator()));
        mTextTag.add("ConfDate", CommonBL.decodeDate(mLGWorkDB.getModifyDate()));
        mTextTag.add("ContNo", mLGWorkDB.getContNo());
        mTextTag.add("CInValiDate",
                     CommonBL.decodeDate(mLGWorkDB.getMakeDate())); //在保单到期日生成满期处理任务
        //续保终止日期
        String sql = " select date('" + mLGWorkDB.getMakeDate() +
                     "') + 30 day "
                     + "from dual ";
        ExeSQL e = new ExeSQL();
        String xbValiDate = e.getOneValue(sql);
        mTextTag.add("XBInValiDate", CommonBL.decodeDate(xbValiDate));

        if (!setContactInfo(mLGWorkDB, mLGWorkDB.getContNo())) {
            return false;
        }

        return true;
    }

    /**
     * 查询工单信息
     * @return LGWorkDB
     */
    private LGWorkDB getLGWorkInfo()
    {
        String edorAcceptNo = (String) mTransferData.getValueByName("edorAcceptNo");
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(edorAcceptNo);
        tLGWorkDB.getInfo();

        return tLGWorkDB;
    }

    /**
     * 设置联系信息
     * @return boolean
     */
    private boolean setContactInfo(LGWorkDB tLGWorkDB, String grpContNo) {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLGWorkDB.getContNo());
        if (!tLCGrpContDB.getInfo()) {
            mErrors.addOneError("没有查询到保单信息。");
            System.out.println(tLCGrpContDB.mErrors.getErrContent());
            return false;
        }
        //添加业务员信息 added by huxl @ 2006-10-25
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCGrpContDB.getAgentCode());
        tLaAgentDB.getInfo();
        mTextTag.add("AgentName", tLaAgentDB.getName());
        mTextTag.add("AgentCode", tLaAgentDB.getAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") ||
            temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        mTextTag.add("Phone", agntPhone);

        //机构信息  added by huxl @ 2006-10-25
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tLCGrpContDB.getAgentGroup());
        tLABranchGroupDB.getInfo();
        mTextTag.add("AgentGroup", tLABranchGroupDB.getName());

        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(grpContNo);
        if (!tLCGrpAppntDB.getInfo()) {
            mErrors.addOneError("没有查询到投保单位信息。");
            return false;
        }

        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        tLCGrpAddressDB.setCustomerNo(tLCGrpAppntDB.getCustomerNo());
        tLCGrpAddressDB.setAddressNo(tLCGrpAppntDB.getAddressNo());
        if (!tLCGrpAddressDB.getInfo()) {
            mErrors.addOneError("没有查询到投保单位的地址信息。");
            return false;
        }

        //联系方式
        mTextTag.add("EdorAppZipCode", tLCGrpAddressDB.getGrpZipCode());
        mTextTag.add("EdorAppAddress", tLCGrpAddressDB.getGrpAddress());
        mTextTag.add("EdorAppName", tLCGrpAddressDB.getLinkMan1());
        String appntPhoneStr = "";
        if (tLCGrpAddressDB.getPhone1() != null)
        {
            appntPhoneStr = tLCGrpAddressDB.getPhone1();
        } else if (tLCGrpAddressDB.getMobile1() != null)
        {
            appntPhoneStr = tLCGrpAddressDB.getMobile1();
        }
        mTextTag.add("AppntPhone", appntPhoneStr);

        //查询受理机构
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(tLGWorkDB.getOperator());
        tLDUserDB.getInfo();

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLDUserDB.getComCode());
        tLDComDB.getInfo();

        mTextTag.add("ServicePhone", tLDComDB.getServicePhone());
        mTextTag.add("ServiceFax", tLDComDB.getFax());
        mTextTag.add("ServiceAddress", tLDComDB.getLetterServicePostAddress());
        mTextTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        mTextTag.add("ComName", tLDComDB.getLetterServiceName());
        mTextTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));

        return true;
    }

    /**
     * 账户余额信息
     * @return boolean
     */
    private boolean setAccInfo() {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("MJ");

        String[][] accType = { {"账户类型", "账户余额类型", "账户利息类型"}, {"团体固定账户",
                BQ.ACCBALA_FIEXD, BQ.DETAILTYPE_INTEREST_FIXED}, {"团体医疗账户",
                BQ.ACCBALA_GROUP, BQ.DETAILTYPE_INTEREST_GROUP}, {"个人账户总和",
                BQ.ACCBALA_INSURED, BQ.DETAILTYPE_INTEREST_INSURED}
        };

        double totalBala = 0;
        for (int i = 1; i < accType.length; i++) {
            String[] accInfo = new String[4];
            accInfo[0] = accType[i][0];
            accInfo[1] = getAccInfo(accType[i][1]);
            accInfo[2] = getAccInfo(accType[i][2]);
            accInfo[3] = getAccBalaAndIntst(accInfo[1], accInfo[2]);
            totalBala += Double.parseDouble(accInfo[3]);

            tlistTable.add(accInfo);
        }

        mTextTag.add("TotalBala",
                     CommonBL.bigDoubleToCommonString(totalBala, "0.00") + "元");
        mXmlExport.addListTable(tlistTable, accType[0]);

        return true;
    }

    /**
     * 账户利息
     * @param accType String
     * @return String
     */
    private String getAccInfo(String detailType) {
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(mLGWorkDB.getWorkNo());
        tLPEdorEspecialDataDB.setEdorNo(mLGWorkDB.getWorkNo());
        tLPEdorEspecialDataDB.setEdorType(BQ.EDORTYPE_MJ);
        tLPEdorEspecialDataDB.setDetailType(detailType);
        tLPEdorEspecialDataDB.setPolNo(BQ.FILLDATA);

        if (!tLPEdorEspecialDataDB.getInfo()) {
            tLPEdorEspecialDataDB.setPolNo(mLPGrpPolSet.get(1).getGrpPolNo());
             if (!tLPEdorEspecialDataDB.getInfo())
             {
                 return "0";
             }

             double edorValue = Double.parseDouble(
                 tLPEdorEspecialDataDB.getEdorValue());
             return CommonBL.bigDoubleToCommonString(edorValue, "0.00");

        }
        else
        {
            double edorValue = Double.parseDouble(
                tLPEdorEspecialDataDB.getEdorValue());
            return CommonBL.bigDoubleToCommonString(edorValue, "0.00");
        }
    }

    /**
     * 计算本金和利息的和
     * @param accBala String[]
     * @return String
     */
    private String getAccBalaAndIntst(String bala, String intrst) {
        double balance = 0;
        double interest = 0;
        try {
            balance = Double.parseDouble(bala);
            interest = Double.parseDouble(intrst);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            mErrors.addOneError("余额或利息有误。");
        }

        return CommonBL.bigDoubleToCommonString(balance + interest, "0.00");
    }

    /**
     *
     * @return boolean
     */
    private boolean setFeedBackInfo() {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("FeedBack");

        String[] feedBaceInfo = new String[2];
        feedBaceInfo[0] = mLGWorkDB.getContNo();
        feedBaceInfo[1] = "续保口         保单终止口";

        tlistTable.add(feedBaceInfo);
        String[] infoTitle = {"保单号", "续保选择（在相应方框打√）"};
        mXmlExport.addListTable(tlistTable, infoTitle);

        return true;
    }

    /**
     * 生成续保批单信息
     * @return boolean
     */
    private boolean setXBInfo() {
        mTextTag.add("BarCode1", mLGWorkDB.getWorkNo());
        mTextTag.add("BarCodeParam1", "BarHeight=25&BarRation=3&BarWidth=1"
                     + "&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        mXmlExport.addDisplayControl("displayXB");
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLGWorkDB.getContNo());
        if (!tLCGrpContDB.getInfo()) {
            mErrors.addOneError("没有查询到保单信息。");
            System.out.println(tLCGrpContDB.mErrors.getErrContent());
            return false;
        }

        this.mTextTag.add("CValiDateAfterXB",
                          CommonBL.decodeDate(tLCGrpContDB.getCValiDate()));
        this.mTextTag.add("CInValiDateAfterXB",
                          CommonBL.decodeDate(tLCGrpContDB.getCInValiDate()));

        return true;
    }

    /**
     * 存储数据
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        return true;
    }

    private boolean prepareOutputData()
    {
        EdorItemSpecialData tEdorItemSpecialData =
                new EdorItemSpecialData(mLGWorkDB.getWorkNo(), BQ.EDORTYPE_MJ);
        tEdorItemSpecialData.query();
        LPEdorEspecialDataSet tLPEdorEspecialDataSet =
                tEdorItemSpecialData.getSpecialDataSet();

        for (int i = 1; i <= tLPEdorEspecialDataSet.size(); i++)
        {
            //若为结案则更新状态
            if (tLPEdorEspecialDataSet.get(i).getDetailType().equals(BQ.DETAILTYPE_MJSTATE)
                && !tLPEdorEspecialDataSet.get(i).getEdorValue().equals(BQ.MJSTATE_FINISH))
            {
                LPEdorEspecialDataSchema schema = tLPEdorEspecialDataSet.get(i);
                schema.setEdorValue(BQ.MJSTATE_PRINTLIST);
                map.put(schema, "DELETE&INSERT");
                return true;
            }
        }

        return true;
    }

    /**
     * 得到批单信息
     * @return XmlExport
     */
    public XmlExport getDealXmlExport() {
        return mXmlExport;
    }

    public static void main(String[] args)
    {
        //普通打印
//        GlobalInput tGlobalInput = new GlobalInput();
//        tGlobalInput.Operator = "endor0";
//        tGlobalInput.ComCode = "86";
//        tGlobalInput.ManageCom = "86";
//
//        TransferData tTransferData = new TransferData();
//        tTransferData.setNameAndValue("edorAcceptNo", "20060920000006");
//        tTransferData.setNameAndValue("prtType", "MJDeal");
//        tTransferData.setNameAndValue("edorType", "MJ");
//
//        VData data = new VData();
//        data.add(tGlobalInput);
//        data.add(tTransferData);
//
//        PrtGrpEdorMJListUI tPrtGrpEdorMJListUI = new PrtGrpEdorMJListUI();
//        if (!tPrtGrpEdorMJListUI.submitData(data, "PRINT")) {
//            System.out.println(tPrtGrpEdorMJListUI.mErrors.getErrContent());
//        }


        //PDF打印

        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "endor0";
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";

        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema.setStandbyFlag2("20060920000006");
        ttLOPRTManagerSchema.setStandbyFlag1("MJ");
        ttLOPRTManagerSchema.setStandbyFlag4("MJDeal");


        VData data = new VData();
        data.add(tGlobalInput);
        data.add(ttLOPRTManagerSchema);

        PrtGrpEdorMJListUI tPrtGrpEdorMJListUI = new PrtGrpEdorMJListUI();
        if (!tPrtGrpEdorMJListUI.submitData(data, "PRINT"))
        {
            System.out.println(tPrtGrpEdorMJListUI.mErrors.getErrContent());
        }
    }
}
