package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.xb.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LPUWMasterDB;
import com.sinosoft.lis.vschema.LPUWMasterSet;
import com.sinosoft.lis.db.LCRnewStateLogDB;
import com.sinosoft.lis.vschema.LCRnewStateLogSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LGWorkDB;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 保全确认类，函件下发后进行保全确认，生成应收数据
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class PEdorXBConfirmBL implements EdorConfirm
{
    private MMap map = new MMap();

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private GlobalInput mGlobalInput = null;

    private String message = null;
    
    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    private boolean mPassFlag3 = true ;//少儿险续保状态，true-整单续保 ； false-整单终止 

    public PEdorXBConfirmBL()
    {
    }

    /**
     * getResult
     * 得到处理后的结果集合
     * @return VData
     * @todo Implement this com.sinosoft.lis.bq.EdorAppConfirm method
     */
    public VData getResult()
    {
        VData d = new VData();
        d.add(map);
        d.add(message);

        return d;
    }

    /**
     * submitData
     * 处理续保保全确认操作
     * @param inputData VData：包括：LPEdorItemSchema, GlobalInput
     * GlobalInput：操作员信息
     * @param operate String，在此为“”
     * @return boolean：操作成功true：否则false
     * @todo Implement this com.sinosoft.lis.bq.EdorConfirm method
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 处理业务逻辑
     * 调用抽档程序生成应收数据
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
        LCContSchema tLCContSchema = new LCContSchema(); // 个人保单表
        tLCContSchema.setContNo(getOldContNo(mLPEdorItemSchema.getContNo()));
        
        String queryType = "2"; //2-普通单，3-少儿单
        
        LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
        tLPUWMasterDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPUWMasterDB.setEdorType(mLPEdorItemSchema.getEdorType());
        LPUWMasterSet tLPUWMasterSet = tLPUWMasterDB.query();
        for(int i = 1; i <= 1; i++)
        {
        	String sql = "select 1 from lcpol where polno = '" +  tLPUWMasterSet.get(i).getPolNo() + "' and riskcode in ('320106','120706') " ;
        	String tCount = new ExeSQL().getOneValue(sql);
        	if(Integer.parseInt(tCount)>0)
        	{
        		queryType = "3";
        	}
        	else
        	{
        		queryType = "2";
        	}
        }
        
        //杨红于2005-07-16 添加页面输入的  起始时间   和终止时间  ，目的是传到后台作校验动作。
        TransferData tempTransferData = new TransferData();
        tempTransferData.setNameAndValue("StartDate", "1000-01-01");  //核保险种一定会进行抽档，故将日期设为最大最小
        tempTransferData.setNameAndValue("EndDate", "3000-12-31");
        tempTransferData.setNameAndValue("EdorNo", mLPEdorItemSchema.getEdorNo());  //进行校验时排除当前工单
        tempTransferData.setNameAndValue("queryType",queryType); //2-普通单 ，3-少儿单

        VData tVData = new VData();
        tVData.add(tLCContSchema);
        tVData.add(this.mGlobalInput);
        tVData.add(tempTransferData);

        PRnewDueFeeBL bl = new PRnewDueFeeBL();
        MMap tMMap = bl.getSubmitMap(tVData, "INSERT");
        
        if((tMMap == null || tMMap.size() == 0) && bl.mErrors.needDealError())
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        map.add(tMMap);

        if(!dealXBStopPol(tLPUWMasterSet))
        {
            return false;
        }
        
        if(!mPassFlag3 && queryType.equals("3"))
        {
        	if(!dealLJSPay(tLPUWMasterSet))
            {
                return false;
            }
        }

        this.message = bl.getMessage();

        mLPEdorItemSchema.setEdorState(BQ.EDORSTATE_CONFIRM);
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLPEdorItemSchema, "UPDATE");

        return true;
    }

    /**
     *
     * @param schema LCContSchema：新保单数据
     * @return String
     */
    private String getOldContNo(String newContNo)
    {

        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        tLCRnewStateLogDB.setNewContNo(newContNo);
        LCRnewStateLogSet set = tLCRnewStateLogDB.query();
        if(set.size() > 0)
        {
            return set.get(1).getContNo();
        }
        else
        {
            return newContNo;
        }
    }

    /**
     * dealXBStopPol
     * 删除续期续保终止险种:
     * 1、若核保结论为条件通过
     * 2、若核保结论为条件通过，客户不同意且不同意处理为终止续保
     * @return boolean
     */
    private boolean dealXBStopPol(LPUWMasterSet aLPUWMasterSet)
    {
        VData tVData = new VData();
        
        PRnewAppCancelPolBL tPRnewAppCancelPolBL = new PRnewAppCancelPolBL();

        for(int i = 1; i <= aLPUWMasterSet.size(); i++)
        {
        	tVData.clear();
        	tVData.add(this.mGlobalInput);
            boolean cancelFlag = false;  //需要续保终止标志
            //若续保终止
            if(aLPUWMasterSet.get(i).getPassFlag()
                    .equals(BQ.PASSFLAG_STOPXB))
            {
                tVData.add(aLPUWMasterSet.get(i).getSchema());
                cancelFlag= true;
            }
            //若核保结论为条件通过
            else if(aLPUWMasterSet.get(i).getPassFlag().equals(BQ.PASSFLAG_CONDITION))
            {
                //客户不同意且不同意处理为终止续保
                if(aLPUWMasterSet.get(i).getCustomerReply() != null
                   && aLPUWMasterSet.get(i).getCustomerReply().equals("2")
                   && aLPUWMasterSet.get(i).getDisagreeDeal() != null
                   && aLPUWMasterSet.get(i).getDisagreeDeal().equals("3"))
                {
                    tVData.add(aLPUWMasterSet.get(i).getSchema());
                    cancelFlag= true;
                }
            }

            if(!cancelFlag)
            {
                continue;
            }
            
            mPassFlag3 = false;

            MMap tMMap = tPRnewAppCancelPolBL.getSubmitMap(tVData, "");
            if((tMMap == null || tMMap.size() == 0)
               && tPRnewAppCancelPolBL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tPRnewAppCancelPolBL.mErrors);
                return false;
            }
            map.add(tMMap);

        }

        return true;
    }
    
    /**
     * dealLJSPay
     * 作废应收:
     * 1、若核保结论为条件通过
     * 2、若核保结论为条件通过，客户不同意且不同意处理为终止续保
     * @return boolean
     */
    private boolean dealLJSPay(LPUWMasterSet aLPUWMasterSet)
    {
    	String tGetNoticeNo = null;
    	LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mLPEdorItemSchema.getEdorAcceptNo());
        if(!tLGWorkDB.getInfo())
        {
            mErrors.addOneError("查询续保工单失败");
            return false;
        }
        if(tLGWorkDB.getInnerSource() == null
           || tLGWorkDB.getInnerSource().equals(""))
        {
            mErrors.addOneError("没有查询到应收记录号");
            return false;
        }
        tGetNoticeNo = tLGWorkDB.getInnerSource();
        
    	EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(tGetNoticeNo,"XB");
    	tEdorItemSpecialData.add("BackDate",mCurrentDate);
    	tEdorItemSpecialData.add("dealType","2"); //1-续保，2-满期终止
    	map.put(tEdorItemSpecialData.getSpecialDataSet(), SysConst.INSERT);
    		
    	String sql1 = "delete from LJSPay where GetNoticeNo = '" + tGetNoticeNo + "' ";
    	String sql2 = "delete from LJSPayPerson where GetNoticeNo = '" + tGetNoticeNo + "' ";
    	String sql3 = "update LJSPayB set DealState = '3',CancelReason = '3' where GetNoticeNo = '" + tGetNoticeNo + "' ";
    	String sql4 = "update LJSPayPersonB set DealState = '3',CancelReason = '3' where GetNoticeNo = '" + tGetNoticeNo + "' ";
    	
    	map.put(sql1, SysConst.DELETE);
    	map.put(sql2, SysConst.DELETE);
    	map.put(sql3, SysConst.UPDATE);
    	map.put(sql4, SysConst.UPDATE);
    	
    	for(int i=1;i<= aLPUWMasterSet.size();i++)
    	{
    		String sql5 = "update LCPol set PolState = '3',ModifyDate = '" + mCurrentDate + "', ModifyTime = '" 
            	+ mCurrentTime + "', Operator = '" + mGlobalInput.Operator + "' where PolNo = '" + aLPUWMasterSet.get(i).getPolNo() + "' ";
    		map.put(sql5, SysConst.UPDATE);
    	}

        return true;
    }

    /**
     * 得到传入的数据
     * @param data VData：submitData中传入的VData集合
     * @return boolean：成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) data
                            .getObjectByObjectName("LPEdorItemSchema", 0);
        if(mLPEdorItemSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }
}
