/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BqUpdateDScardflagBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String myFlag = "";

	private String mContNo = "";

	private MMap mMap = new MMap();

	public BqUpdateDScardflagBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中,判断所需参数是否为空
		if (!getInputData(cInputData)) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BqUpdateDScardflagBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start BqUpdateDScardflagBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {// 感觉挺像原来的 jdbc 的...
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "BqUpdateDScardflagBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mContNo = (String) tTransferData.getValueByName("ContNo");
		if (mContNo == null || "".equals(mContNo)) {
			CError tError = new CError();
			tError.moduleName = "BqUpdateDScardflagBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "合同号为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		myFlag = (String) tTransferData.getValueByName("myFlag");
		if (myFlag == null || "".equals(myFlag)) {
			CError tError = new CError();
			tError.moduleName = "BqUpdateDScardflagBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "不告诉你判断的什么!!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dealData() {
		// System.out.println("dealData-->myFlag:" + myFlag);
		// System.out.println("dealData-->mContNo:" + mContNo);
		if ("0".equals(myFlag)) {
			String mSql = "update lccont set cardflag='0', modifydate=current date, modifytime=current time where contno='" + mContNo + "' and cardflag='b'";
			// System.out.println("dealData-->mSql:" + mSql);
			mMap.put(mSql, SysConst.UPDATE);
		}
		if ("1".equals(myFlag)) {
			String mSql = "update lccont set cardflag='b', modifydate=current date, modifytime=current time where contno='" + mContNo + "' and cardflag='0'";
			// System.out.println("dealData-->mSql:" + mSql);
			mMap.put(mSql, SysConst.UPDATE);
		}
		return true;

	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
