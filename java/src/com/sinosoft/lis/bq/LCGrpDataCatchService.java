package com.sinosoft.lis.bq;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.utility.Schema;

/**
 * @author LY
 * 
 */
public class LCGrpDataCatchService {
	private final static int mSize = LCGrpThreadConfig.dataCatchSize; // 数据缓存队列大小

	private LCInsuredListSet mLCInsuredListSet;
	
	public LCGrpDataCatchService(LCInsuredListSet tLCInsuredListSet) {
		
		this.mLCInsuredListSet = tLCInsuredListSet;
	}

	public boolean hasNext() {
		return mLCInsuredListSet.size() > 0 ? true : false;
	}

	public List<Schema> getDatas(int cMaxRowNum) {
		List<Schema> tResultVals = null;

		synchronized (this) {
			int tMaxRowNum = cMaxRowNum;

			//实际每次取的数量
			int tCurMaxRowNum = mLCInsuredListSet.size() > tMaxRowNum ? tMaxRowNum : mLCInsuredListSet.size();

			tResultVals = Collections.synchronizedList(new ArrayList<Schema>());

			for (int i = 0; i < tCurMaxRowNum; i++) {
				tResultVals.add(mLCInsuredListSet.get(1));
				mLCInsuredListSet.remove(mLCInsuredListSet.get(1));
//				System.out.println("一次拿的数："+i);
			}
		}

		return tResultVals;
	}

}
