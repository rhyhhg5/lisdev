
package com.sinosoft.lis.bq;

//程序名称：PEdorZBDetailBL.java  
//程序功能：
//创建日期：2008-05-20
//创建人  ：pst
//更新记录：  更新人    更新日期     更新原因/内容
  
import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorZBDetailBL {
/** 传入数据的容器 */
private VData mInputData = new VData();
/** 传出数据的容器 */
private VData mResult = new VData();
/** 数据操作字符串 */
private String mOperate;
/** 错误处理类 */
public CErrors mErrors = new CErrors();
/** 全局基础数据 */
private GlobalInput mGlobalInput = null;

private LPEdorItemSchema mLPEdorItemSchema = null;

private   LJSPayPersonSet mLJSPayPersonSet= new LJSPayPersonSet();
private  LJSPaySet mLJSPaySet = new LJSPaySet();
private MMap map = new MMap();

/**主险险种号*/
private String mPolNo = null;

private String mContNo=null;

private Reflections ref = new Reflections();

private String currDate = PubFun.getCurrentDate();

private String currTime = PubFun.getCurrentTime();

private String mEdorNo = null;

private static String mEdorType = "ZB";

private String mLimit=null;

private String mGetNoticeNo=null;

private String mSerialNo=null;

private double mSumPayMoney=0;

private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();


private String aMakeDate =PubFun.getCurrentDate();
private String aMakeTime =PubFun.getCurrentTime();

private String aOperator;

public PEdorZBDetailBL() 
{
}

/**
 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
 * @param cInputData 传入的数据,VData对象
 * @param cOperate 数据操作字符串，主要包括"INSERT"
 * @return 布尔值（true--提交成功, false--提交失败）
 */
public boolean submitData(VData cInputData, String cOperate) 
{

  //将操作数据拷贝到本类中
  this.mInputData = (VData) cInputData.clone();
  this.mOperate = cOperate;
  
  System.out.println("追捕费标志(ZF--追加保费 BF--补交保费):"+mOperate);

  //得到外部传入的数据,将数据备份到本类中
  if (!getInputData()) 
  {
    return false;
  }

  if (!checkData()) 
  {
    return false;
  }

  //进行业务处理
  if (!dealData()) 
  {
    return false;
  }

  //准备往后台的数据
  if (!prepareData()) 
  {
    return false;
  }
  PubSubmit tSubmit = new PubSubmit();

  if (!tSubmit.submitData(mResult, "")) 
  { //数据提交
    // @@错误处理
    this.mErrors.copyAllErrors(tSubmit.mErrors);
    CError tError = new CError();
    tError.moduleName = "PEdorZBDetailBL";
    tError.functionName = "submitData";
    tError.errorMessage = "数据提交失败!";
    this.mErrors.addOneError(tError);
    return false;
  }
  System.out.println("PEdorZBDetailBL End PubSubmit");
  return true;
}

/**
 * 将外部传入的数据分解到本类的属性中
 * @param: 无
 * @return: boolean
 */
private boolean getInputData() {
  try {
    mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
    mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName("LPEdorItemSchema", 0);
    mLPEdorEspecialDataSchema = (LPEdorEspecialDataSchema) mInputData.getObjectByObjectName("LPEdorEspecialDataSchema", 0);
    mEdorNo = mLPEdorItemSchema.getEdorNo();
    mContNo =mLPEdorItemSchema.getContNo();
    aOperator=mGlobalInput.Operator;
  }
  catch (Exception e) {
    // @@错误处理
    e.printStackTrace();
    CError tError = new CError();
    tError.moduleName = "PEdorZBDetailBL";
    tError.functionName = "getInputData";
    tError.errorMessage = "接收数据失败!!";
    this.mErrors.addOneError(tError);
    return false;
  }

  if (mGlobalInput == null || mLPEdorItemSchema == null) 
  {
    CError tError = new CError();
    tError.moduleName = "PEdorZBDetailBL";
    tError.functionName = "getInputData";
    tError.errorMessage = "输入数据有误!";
    this.mErrors.addOneError(tError);
    return false;
  }
  

  return true;
}

/**
 * 根据前面的输入数据，进行逻辑处理
 * 因为退保项目必须在其他项目都结案之后才能进行，所以相应的保全数据可以从C表中得到
 * @return 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean dealData() 
{
	
  //说明保单有欠交保费而且,处于缓交或者自动缓交状态,对保单进行临时补催收,生成LJSPay,LJSPayPercon
  if("init".equals(mOperate))
  {
	  
	  LCContSchema tLCContSchema = CommonBL.getLCCont(mContNo);
	  mLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
      mGetNoticeNo = PubFun1.CreateMaxNo("PAYNOTICENO", mLimit);
      mSerialNo = PubFun1.CreateMaxNo("SERIALNO", mLimit);  
	  String tPayToDate=tLCContSchema.getPaytoDate();
	  if(!ReCountPayData(tLCContSchema,aMakeDate,tPayToDate))
	  {
		    CError tError = new CError();
		    tError.moduleName = "PEdorZBDetailBL";
		    tError.functionName = "dealData";
		    tError.errorMessage = "获取主险保单号失败!";
		    this.mErrors.addOneError(tError);
		    return false;
	  }	  
  }
  else{
	  mLPEdorItemSchema.setEdorState("1");
	  mLPEdorItemSchema.setModifyDate(currDate);
	  mLPEdorItemSchema.setModifyTime(currTime);
  }
 
//  mLPEdorEspecialDataSchema.setPolNo(mPolNo);
  if(mLJSPayPersonSet.size()>0)
  {
	  map.put(mLJSPayPersonSet,"DELETE&INSERT");
	  LJAPaySchema tLJAPaySchema = new LJAPaySchema();
	  tLJAPaySchema=CommonBL.getLJAPay(this.mContNo, "2");
	  LJSPaySchema tLJSPaySchema = new LJSPaySchema();
	  ref.transFields(tLJSPaySchema, tLJAPaySchema);
	  tLJSPaySchema.setGetNoticeNo(mGetNoticeNo);
	  tLJSPaySchema.setSerialNo(mSerialNo);
	  tLJSPaySchema.setOtherNo(mLPEdorItemSchema.getEdorAcceptNo());
	  tLJSPaySchema.setOtherNoType("10");
	  //存放保单本次发生交费的日期
	  tLJSPaySchema.setPayDate(aMakeDate);
	  tLJSPaySchema.setSumDuePayMoney(mSumPayMoney);
	  tLJSPaySchema.setBankOnTheWayFlag("0");
	  tLJSPaySchema.setBankSuccFlag("0");
	  tLJSPaySchema.setPayDate(aMakeDate);	  
	  tLJSPaySchema.setRiskCode("000000");
	  tLJSPaySchema.setMakeDate(aMakeDate);
	  tLJSPaySchema.setMakeTime(aMakeTime);
	  tLJSPaySchema.setModifyDate(aMakeDate);
	  tLJSPaySchema.setModifyTime(aMakeTime);
	  tLJSPaySchema.setOperator(aOperator);	
	  mLJSPaySet.add(tLJSPaySchema);
  }
  if(mLJSPaySet.size()>0)
  {
	  map.put(mLJSPaySet,"DELETE&INSERT");  
  }

  map.put(mLPEdorItemSchema, "UPDATE");
  map.put(mLPEdorEspecialDataSchema, "DELETE&INSERT");
  return true;
}

/**
 * 根据前面的输入数据，进行校验处理
 * @return 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean checkData()
{	
	LPEdorItemDB tLPEdorItemDB=new LPEdorItemDB();
	tLPEdorItemDB.setSchema(mLPEdorItemSchema);
    if (!tLPEdorItemDB.getInfo()) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PEdorZBDetailBL";
      tError.functionName = "checkDate";
      tError.errorMessage = "无保全申请数据!";
      System.out.println("------" + tError);
      this.mErrors.addOneError(tError);
      return false;
    }

    //将查询出来的保全主表数据保存至模块变量中，省去其它的重复查询
    mLPEdorItemSchema.setSchema(tLPEdorItemDB.getSchema());
    if(mLPEdorItemSchema.getEdorState().trim().equals(BQ.EDORSTATE_CAL))
    {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "PEdorZBDetailBL";
        tError.functionName = "checkData";
        tError.errorMessage = "该保全已经保全理算，不能修改!";
        this.mErrors.addOneError(tError);
        return false;
    }
 
    return true;
}

/**
 * 递归进行补催收
 * @param LCContSchema LCContSchema 保单数据
 * @param String tCurDate 日期
 * @return 
 * */
	private boolean ReCountPayData(LCContSchema tLCContSchema, String tCurDate,
			String tPayToDate) 
	{

		// 趸交直接返回
		ExeSQL tExeSQL=new ExeSQL();
		if ("0".equals(String.valueOf(tLCContSchema.getPayIntv()))) 
		{
			return true;
		}
		// 时间递归线
		String rPaytoDate = PubFun.calDate(tPayToDate, tLCContSchema
				.getPayIntv(), "M", "");
		
		
		int tDayIntv = PubFun.calInterval(tPayToDate, tCurDate, "D");
		if (tDayIntv < 0) 
		{
			return true;
		}else
		{
			//查询续期是否已经催出应收数据
			String tSQL="select count(*) from ljspayperson where polno=(select polno from lcpol where contno='"+tLCContSchema.getContNo()+"' and polno=mainPolno) and curpaytodate='"+rPaytoDate+"'";
			String tFlag=tExeSQL.getOneValue(tSQL);
			if(tFlag.equals("0")){//如果没有应收，生成应收
				if (!dealOneIntv(tLCContSchema, rPaytoDate)) 
				{
					CError tError = new CError();
					tError.moduleName = "PEdorZBDetailBL";
					tError.functionName = "dealOneIntv";
					tError.errorMessage = "处理一期补收数据失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
			}
			
			
		}
		if (!ReCountPayData(tLCContSchema, tCurDate, rPaytoDate)) 
		{
			CError tError = new CError();
			tError.moduleName = "PEdorZBDetailBL";
			tError.functionName = "ReCountPayData";
			tError.errorMessage = "递归补催收失败!";
			this.mErrors.addOneError(tError);
			return false;
		}


		return true;
	}


/** 处理一期保单应收数据 */
private boolean dealOneIntv(LCContSchema tLCContSchema,String tPayToDate)
{
	  LCPolSet tLCPolSet = new LCPolSet();
	  LCPolDB tLCPolDB =new LCPolDB();
	  tLCPolDB.setContNo(tLCContSchema.getContNo());
	  tLCPolSet=tLCPolDB.query();
      // 产生通知书号
      
     
      // 产生统一流水号
      String tLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
      String aSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
      int payCount=PubFun.calInterval(tLCContSchema.getCValiDate(), tPayToDate, "M")/tLCContSchema.getPayIntv();
      
      
	  for(int i=1;i<=tLCPolSet.size();i++)
	  {
		  LCPolSchema tLCPolSchema = new LCPolSchema();
		  tLCPolSchema=tLCPolSet.get(i);
		  LCPremSet  tLCPremSet  =new LCPremSet();
		  LCPremDB  tLCPremDB  =new LCPremDB();
		  tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
		  tLCPremSet=tLCPremDB.query();
		  for(int j=1;j<=tLCPremSet.size();j++)
		  {
			  

			  LJSPayPersonSchema tLJSPayPersonSchema= new LJSPayPersonSchema();
			  LJAPayPersonSchema tLJAPayPersonSchema= new LJAPayPersonSchema();
			  tLJAPayPersonSchema=CommonBL.getLJAPayPerson(tLCPremSet.get(j).getPolNo());
			  ref.transFields(tLJSPayPersonSchema, tLJAPayPersonSchema);
			  //存放保单本次发生交费的日期
			  tLJSPayPersonSchema.setPayDate(aMakeDate);
			  tLJSPayPersonSchema.setPayCount(payCount);
			  tLJSPayPersonSchema.setLastPayToDate(tLCPremSet.get(j).getPaytoDate());
			  if("0".equals(String.valueOf(tLCPremSet.get(j).getPayIntv())))
			  {
				  tLJSPayPersonSchema.setCurPayToDate(tLCPremSet.get(j).getPayEndDate());  
			  }else
			  {
				  tLJSPayPersonSchema.setCurPayToDate(tPayToDate);
			  }	
			  mSumPayMoney+=tLCPremSet.get(j).getPrem();
			  tLJSPayPersonSchema.setBankOnTheWayFlag("0");
			  tLJSPayPersonSchema.setBankSuccFlag("0");
			  tLJSPayPersonSchema.setDutyCode(tLCPremSet.get(j).getDutyCode());
			  tLJSPayPersonSchema.setPayPlanCode(tLCPremSet.get(j).getPayPlanCode());
			  tLJSPayPersonSchema.setPayIntv(tLCPremSet.get(j).getPayIntv());
			  tLJSPayPersonSchema.setSumDuePayMoney(tLCPremSet.get(j).getPrem());
			  tLJSPayPersonSchema.setSumActuPayMoney(tLCPremSet.get(j).getPrem());
			  tLJSPayPersonSchema.setSerialNo(aSerialNo);
			  tLJSPayPersonSchema.setGetNoticeNo(mGetNoticeNo);		  
			  tLJSPayPersonSchema.setMakeDate(aMakeDate);
			  tLJSPayPersonSchema.setMakeTime(aMakeTime);
			  tLJSPayPersonSchema.setModifyDate(aMakeDate);
			  tLJSPayPersonSchema.setModifyTime(aMakeTime);
			  tLJSPayPersonSchema.setOperator(aOperator);	
			  mLJSPayPersonSet.add(tLJSPayPersonSchema);
		  }
		  
	  }
	  
	  
	    
	return true;
	}
/**
 * 准备往后层输出所需要的数据
 * @return 如果准备数据时发生错误则返回false,否则返回true
 */
private boolean prepareData() 
{
  //mMap.put(mLPEdorMainSchema, "UPDATE");
  //mMap.put(mLPEdorItemSchema, "UPDATE");
  mResult.clear();
  mResult.add(map);

  return true;
}

/**
 * 数据输出方法，供外界获取数据处理结果
 * @return 包含有数据查询结果字符串的VData对象
 */
public VData getResult() 
{
  return mResult;
}
}
