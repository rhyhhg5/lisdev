package com.sinosoft.lis.bq;

import com.sinosoft.utility.ExeSQL;

/**
 * 大团单线程控制类
 * 
 * @author 
 */
public class LCGrpThreadConfig {

	/** 数据缓冲池大小 */
	public static int dataCatchSize = initDataCatchSize();

	/** 最大线程数 */
	public static int threadMaxCount = initThreadMaxCount();

	/** 线程最大获取数据量 */
	public static int threadDataSize = initThreadDataSize();

	private static int initDataCatchSize() {
		ExeSQL tExeSQL = new ExeSQL();
		String dataCatchSize = tExeSQL.getOneValue("select code1 from ldcode1 where codetype='lgrp' and code='DataCatchSize'");
		return Integer.parseInt(dataCatchSize);
	}

	private static int initThreadMaxCount() {
		ExeSQL tExeSQL = new ExeSQL();
		String threadMaxCount = tExeSQL.getOneValue("select code1 from ldcode1 where codetype='lgrp' and code='ThreadMaxCount'");
		return Integer.parseInt(threadMaxCount);
	}

	private static int initThreadDataSize() {
		ExeSQL tExeSQL = new ExeSQL();
		String threadDataSize = tExeSQL.getOneValue("select code1 from ldcode1 where codetype='lgrp' and code='ThreadDataSize'");
		return Integer.parseInt(threadDataSize);
	}

	public static void reload() {
		dataCatchSize = initDataCatchSize();
		threadMaxCount = initThreadMaxCount();
		threadDataSize = initThreadDataSize();
	}
}
