package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PEdorHLConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private LPEdorItemSchema mLPEdorItemSchema = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();

    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!prepareData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
    	
        if (!validateEdorData())
        {
            return false;
        } 
//      更新保单状态，置stateflag='3'
         updatecontstate();
          updateEdorState();
          //增加领取金额，trace表中应该为负数，
          
          mResult.clear();
          mResult.add(map);

        return true;
    }
    private void updatecontstate(){
    	   map.put("  update lccont "
                   + "set stateflag = '3'"
                   + " where  grpcontno = '" + mLPEdorItemSchema.getGrpContNo()
                   + "'   and insuredno = '" + mLPEdorItemSchema.getInsuredNo()
                   + "'   and contNo = '" + mLPEdorItemSchema.getContNo() + "' ",
                   "UPDATE");
    	   map.put("  update lcpol "
                   + "set stateflag = '3'"
                   + " where  grpcontno = '" + mLPEdorItemSchema.getGrpContNo()
                   + "'   and insuredno = '" + mLPEdorItemSchema.getInsuredNo()
                   + "'   and contNo = '" + mLPEdorItemSchema.getContNo() + "' ",
                   "UPDATE");
    	   
    }
    private void updateEdorState()
    {
        map.put("  update LPEdorItem "
                + "set edorState = '" + BQ.EDORSTATE_CONFIRM
                + "',   operator = '" + mGlobalInput.Operator
                + "',  modifyDate = '" + PubFun.getCurrentDate()
                + "',  modifyTime = '" + PubFun.getCurrentTime()
                + "' where edorNo = '" + mLPEdorItemSchema.getEdorNo()
                + "'   and edorType = '" + mLPEdorItemSchema.getEdorType()
                + "'   and contNo = '" + mLPEdorItemSchema.getContNo() + "' ",
                "UPDATE");
          
    }

  
    private boolean validateEdorData()
    {

        String[] tables1 = {"LCCont", "LCPol", "LCDuty", "LCPrem", "LCGet","LCInsureAcc", "LCInsureAccFee","LCInsureAccClass","LCInsureAccClassFee"};
        
        ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,
                mLPEdorItemSchema.getEdorNo(), mLPEdorItemSchema.getEdorType(),
                mLPEdorItemSchema.getContNo(), "ContNo");
        if (!validate.changeData(tables1))
        {
            mErrors.copyAllErrors(validate.mErrors);
            return false;
        }
        MMap tmap = validate.getMap();
        map.add(tmap);
        String[] tables2 =
                {"LCInsureAccTrace", "LCInsureAccFeeTrace","LCInsureAccBalance"};
        validate = new ValidateEdorData2(mGlobalInput,
                                         mLPEdorItemSchema.getEdorNo(),
                                         mLPEdorItemSchema.getEdorType(),
                                         mLPEdorItemSchema.getContNo(),
                                         "ContNo");
        if (!validate.addData(tables2))
        {
            mErrors.copyAllErrors(validate.mErrors);
            return false;
        }
        map.add(validate.getMap());
        return true;
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo("20110315000003");

        VData data = new VData();
        data.add(gi);
        data.add(tLPEdorItemDB.query().get(1));

        PEdorHLConfirmBL bl = new PEdorHLConfirmBL();
        if (!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        } else
        {
            System.out.println("All OK");
        }
    }
}

