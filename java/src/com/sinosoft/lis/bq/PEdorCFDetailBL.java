package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全保单拆分</p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company: Sinosoft</p>
 * author YangTianZheng【OoO?】
 * @version 1.0
 */

public class PEdorCFDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private DetailDataQuery mQuery = null;

    private String mOperator = null;

    private String mEdorNo = null;

    private String mEdorType = BQ.EDORTYPE_CF;

    private LCPolSet mLCPolSet = null;
    
    private LCInsuredSet mLCInsuredSet = null;
    
    private LPPolSet mLPPolSet = null;
    
    private LDPersonSet mLDPersonSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    private String mAppntNo = null;
    
    private String mContNo = null;
    
    private String mbContNo = null;
    
    private String mPrtNo = null;
    
    private String mPrtnoCount = null;
    
    

    /**
     * 构造函数
     * @param edorNo String
     */
    public PEdorCFDetailBL(String edorNo,String appntno,String contno)
    {
        this.mEdorNo = edorNo;
        this.mAppntNo = appntno;
        this.mContNo = contno;
        this.mQuery = new DetailDataQuery(mEdorNo, mEdorType);
    }

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData data, String operator)
    {
        this.mOperator = operator;

        if (!getInputData(data))
        {
            return false;
        }
        
        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            
            mLCInsuredSet  = (LCInsuredSet) data.getObjectByObjectName(
                    "LCInsuredSet", 0);
            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入参数错误！");
            return false;
        }
        
        if((mOperator != null) && mOperator.equals("SPLIT"))
        {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setCustomerNo(mAppntNo);
        mLDPersonSet = tLDPersonDB.query();
          if (mLDPersonSet.size() == 0)
          {
            mErrors.addOneError("没有查到新投保人客户信息");
            return false;
          }
        }
        return true;
    }

    private boolean checkData()
    {
    	 if ((mOperator != null) && mOperator.equals("SPLIT"))
         {
    		 LPPolDB tLPPolDB = new LPPolDB();
             tLPPolDB.setEdorNo(mEdorNo);
             tLPPolDB.setEdorType(mEdorType);
             mLPPolSet = tLPPolDB.query();
             if (mLPPolSet.size() != 0)
             {
                 mErrors.addOneError("已经拆分了保单，请如需修改请先取消拆分");
                 return false;
             }
         }
    	
    	LCInsuredDB tLCInsuedDB = new LCInsuredDB();
    	LCInsuredSet tLCInsuredSet = new LCInsuredSet();
    	tLCInsuedDB.setContNo(mContNo);
    	tLCInsuredSet = tLCInsuedDB.query();
    	
        if(tLCInsuredSet.size()==mLCInsuredSet.size())
        {
        	mErrors.addOneError("不可以选择全部的被保人，请您重新选择。");
			return false;
        }
    	
        if((mOperator != null) && mOperator.equals("SPLIT"))
        {
        LCAppntDB fLCAppntDB = new LCAppntDB();
		LCAppntSet fLCAppntSet = new LCAppntSet();
		fLCAppntDB.setAppntNo(mAppntNo);
		fLCAppntDB.setContNo(mContNo);
		fLCAppntSet = fLCAppntDB.query();
		
		if(fLCAppntSet.size()!=0)
		{
			 mErrors.addOneError("客户"+mAppntNo+
					 " 为原保单"+mContNo+
					 "的投保人，不可以作为新保单的投保人");
			 return false;
		}
        }
    	for (int i = 1; i <= mLCInsuredSet.size(); i++)
        {
    		LCAppntDB tLCAppntDB = new LCAppntDB();
    		LCAppntSet tLCAppntSet = new LCAppntSet();
    		tLCAppntDB.setAppntNo(mLCInsuredSet.get(i).getInsuredNo());
    		tLCAppntDB.setContNo(mLCInsuredSet.get(i).getContNo());
    		tLCAppntSet = tLCAppntDB.query();
    		
    		if(tLCAppntSet.size()!=0)
    		{
    			 mErrors.addOneError("客户"+mLCInsuredSet.get(i).getInsuredNo()+
    					 " 为原保单"+mLCInsuredSet.get(i).getContNo()+
    					 "的投保人，不可以进行拆分");
    			 return false;
    		}
        }
    	
    	LCInsuredDB aLCInsuedDB = new LCInsuredDB();
    	LCInsuredSet aLCInsuredSet = new LCInsuredSet();
    	aLCInsuedDB.setContNo(mContNo);
    	aLCInsuedDB.setInsuredNo(mAppntNo);
    	aLCInsuredSet = aLCInsuedDB.query();
    	
    	if(aLCInsuredSet.size()==0)
		{
			 mErrors.addOneError("客户"+mAppntNo+
					 " 为不在"+mContNo+
					 "的保单中，不可以作为新保单的投保人");
			 return false;
		}
    	
    	
    	    
        return true;
    }
    
    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if ((mOperator != null) && mOperator.equals("SPLIT"))
        {
            if(!setLPPol())
            {
            	return false;
            }
        }
        else if ((mOperator != null) && mOperator.equals("DEL"))
        {
            delLPPol();
        }
        return true;
    }


    /**
     * 得到新合同号
     * @return String
     */
    private String getNewContNo(String appntNo)
    {
        return PubFun1.CreateMaxNo("ContNo", appntNo);
    }


    /**
     * 设置拆分数据
     */
    private boolean setLPPol()
    {
    	mbContNo = getNewContNo(mAppntNo);
    	
        for (int i = 1; i <= mLCInsuredSet.size(); i++)
        {
        	LCInsuredSchema tLCInsuredShema = mLCInsuredSet.get(i);
            String keyValue[] =new String[2];
        	keyValue[1]=tLCInsuredShema.getInsuredNo();
        	keyValue[0]=tLCInsuredShema.getContNo();   
        	LPInsuredSchema tLPInsuredSchema = (LPInsuredSchema)mQuery.getDetailData("LCInsured", keyValue);
        	tLPInsuredSchema.setContNo(mbContNo);
        	tLPInsuredSchema.setAppntNo(mAppntNo);
        	tLPInsuredSchema.setOperator(mGlobalInput.Operator);
        	tLPInsuredSchema.setMakeDate(mCurrentDate);
        	tLPInsuredSchema.setMakeTime(mCurrentTime);
        	tLPInsuredSchema.setModifyDate(mCurrentDate);
        	tLPInsuredSchema.setModifyTime(mCurrentTime);
        	ExeSQL tExeSQL =new ExeSQL();
        	String prtnocountmax = null;
        	String MiggleTheLine = tLPInsuredSchema.getPrtNo().substring(0, tLPInsuredSchema.getPrtNo().length()-2);
        	if(MiggleTheLine.endsWith("CF"))
        	{
        		mPrtnoCount ="select count(1)+1 from lccont where prtno like '"+MiggleTheLine+"%'";
      		  	prtnocountmax=tExeSQL.getOneValue(mPrtnoCount);
      		    mPrtNo = MiggleTheLine+"0"+prtnocountmax;
        	}else {
        		mPrtnoCount ="select count(1) from lccont where prtno like '"+tLPInsuredSchema.getPrtNo()+"%'";
        		prtnocountmax=tExeSQL.getOneValue(mPrtnoCount);
        		mPrtNo = tLPInsuredSchema.getPrtNo()+mEdorType+"0"+prtnocountmax;
        		
        	}
//        	if(tLPInsuredSchema.getPrtNo().length()==11||tLPInsuredSchema.getPrtNo().length()==12)
//        	{
//        		mPrtnoCount ="select count(1) from lccont where prtno like '"+tLPInsuredSchema.getPrtNo()+"%'";
//        		prtnocountmax=tExeSQL.getOneValue(mPrtnoCount);
//        		 mPrtNo = tLPInsuredSchema.getPrtNo()+mEdorType+"0"+prtnocountmax;
//        	}
//        	else
//        	{
//        		String MiggleTheLine = tLPInsuredSchema.getPrtNo().substring(0, tLPInsuredSchema.getPrtNo().length()-2);
//        		mPrtnoCount ="select count(1) from lccont where prtno like '"+MiggleTheLine+"%'";
//        		  prtnocountmax=tExeSQL.getOneValue(mPrtnoCount);
//                  mPrtNo = MiggleTheLine+mEdorType+"0"+prtnocountmax;
//        	}
      	  
           tLPInsuredSchema.setPrtNo(mPrtNo);
           
        	if(tLPInsuredSchema.getInsuredNo().equals(mAppntNo))
        	{
        		tLPInsuredSchema.setRelationToAppnt("00");
        		tLPInsuredSchema.setRelationToMainInsured("00");
        	}
        	
            mMap.put(tLPInsuredSchema, "DELETE&INSERT");
                      
            LCPolSet tLCPolSet = new LCPolSet();
            LCPolDB tLCPolDB = new LCPolDB();
            
            tLCPolDB.setContNo(tLCInsuredShema.getContNo());
            tLCPolDB.setInsuredNo(tLCInsuredShema.getInsuredNo());
            
            tLCPolSet=tLCPolDB.query();
            
            for(int index=1;index<=tLCPolSet.size();index++)
            {
               LCPolSchema tLCPolSchema = new LCPolSchema();
               tLCPolSchema=tLCPolSet.get(index);
               LPPolSchema tLPPolSchema = (LPPolSchema)mQuery.getDetailData("LCPol", tLCPolSchema.getPolNo());
               tLPPolSchema.setContNo(mbContNo);
               tLPPolSchema.setAppntNo(mAppntNo);
               tLPPolSchema.setAppntName(mLDPersonSet.get(1).getName());
               tLPPolSchema.setPayMode("1");
               tLPPolSchema.setPrtNo(mPrtNo);
               tLPPolSchema.setOperator(mGlobalInput.Operator);
               tLPPolSchema.setMakeDate(mCurrentDate);
               tLPPolSchema.setMakeTime(mCurrentTime);
               tLPPolSchema.setModifyDate(mCurrentDate);
               tLPPolSchema.setModifyTime(mCurrentTime);
               
//               if(tLPPolSchema.getAppntNo().equals(tLPPolSchema.getInsuredNo())&&tLPPolSchema.getInsuredAppAge()<18)
//               {
//            	    mErrors.addOneError("新保单投保人年龄小于18");
//            	   return false;
//               }
               ExeSQL aExeSQL =new ExeSQL();
       		   String aggcheck ="select year('"+tLPPolSchema.getCValiDate()+"') -year( '"+tLPInsuredSchema.getBirthday()+"') from dual ";
       		   System.out.println(aggcheck);
       		   int checkage = Integer.parseInt(aExeSQL.getOneValue(aggcheck));
       		   if(tLPPolSchema.getAppntNo().equals(tLPPolSchema.getInsuredNo())&&checkage<18)
       		   {
       		   mErrors.addOneError("新保单投保人年龄小于18");
               return false;
       		   }
               
               mMap.put(tLPPolSchema, "DELETE&INSERT");
            }         
        }
        
        return true;
    }

    /**
     * 删除拆分数据
     */
    private void delLPPol()
    {

            String sql = "delete from LPPol " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " ;
            
            System.out.println("删除LPPol的SQL语句："+sql);
            
            mMap.put(sql, "DELETE");
            
            String sql2 = "delete from LPInsured " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " ;
            
            System.out.println("删除LPInsured的SQL语句："+sql);
            
            mMap.put(sql2, "DELETE");
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
