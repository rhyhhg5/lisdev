/**
 * 关联交易清单导入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  Houyd
 * @version 1.0
 */
package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LDRelatedPartyDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class AddRelatedTransactionList {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mResult = new VData();
	/** 接受前台数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();
	private String mBlackListCode;

	/** 节点名 */
	private String[] mSheetName = { "Sheet1" };
	/** 配置文件名 */
	private String mConfigName = "RelatedTransactionImport.xml";
	/** 文件路径 */
	private String mPath;
	/** 文件名 */
	private String mFileName = "";
	/** 导入成功的记录数 */
	private int mSuccNum = 0;
	/** 批次号 */
	private String mBatchNo = "";

	private MMap mMap = new MMap();

	/**
	 * 默认无参构造函数
	 */
	public AddRelatedTransactionList() {
	}

	/**
	 * 解析Excel的构造函数
	 * 
	 * @param path
	 *            String
	 * @param fileName
	 *            String
	 */
	public AddRelatedTransactionList(String aPath, String aFileName,
			GlobalInput aGlobalInput) {
		this.mPath = aPath;
		this.mFileName = aFileName;
		this.mGlobalInput = aGlobalInput;

	}

	/**
	 * 添加传入的一个Sheet数据
	 * 
	 * @param path
	 *            String
	 * @param fileName
	 *            String
	 */
	public boolean doAdd(String aPath, String aFileName) {

		// 从磁盘导入数据
		RelatedTransactionImportFile importFile = new RelatedTransactionImportFile(
				aPath + aFileName, aPath + mConfigName, mSheetName);
		if (!importFile.doImport()) {
			this.mErrors.copyAllErrors(importFile.mErrors);
			return false;
		}

		LDRelatedPartySet tLDRelatedPartySet = importFile.getLDRelatedPartySet();

		MMap map = new MMap();

		mSuccNum = tLDRelatedPartySet.size();
		System.out.println("导入数据容量：" + tLDRelatedPartySet.size());
		// 对sheet中的数据进行校验
		for (int i = 1; i <= tLDRelatedPartySet.size(); i++) {
			LDRelatedPartySchema tLDRelatedPartySchema = new LDRelatedPartySchema();
			tLDRelatedPartySchema = tLDRelatedPartySet.get(i).getSchema();

			String pName = tLDRelatedPartySchema.getName();
			String pIDNo = tLDRelatedPartySchema.getIDNo();
			String pRelatedType = tLDRelatedPartySchema.getRelatedType();

			if (pName == null || "".equals(pName)) {
				mErrors.addOneError("导入关联方清单中，关联方名称不能为空，请检查！");
				this.mErrors.setContent("导入关联方清单中，关联方名称不能为空，请检查！");
				return false;
			}

			if (pRelatedType == null || "".equals(pRelatedType)) {
				mErrors.addOneError("导入关联方清单中，关联方"+pName+"的关联方类别不能为空，请检查！");
				this.mErrors.setContent("导入关联方清单中，关联方"+pName+"的关联方类别不能为空，请检查！");
				return false;
			}
			// 个人关联方证件类型不能为空
			if (pRelatedType == "1" || "1".equals(pRelatedType)) {
				if (pIDNo == null || "".equals(pIDNo)) {
					mErrors.addOneError("个人关联方"+pName+"的证件号码不能为空，请检查！");
					this.mErrors.setContent("个人关联方"+pName+"的证件号码不能为空，请检查！");
					return false;
				}
			}else if(pRelatedType =="2" || "2".equals(pRelatedType)){
			}else{
				mErrors.addOneError("导入关联方清单中，关联方"+pName+"的关联方类别不符合规范，请检查！");
				this.mErrors.setContent("导入关联方清单中，关联方"+pName+"的关联方类别不符合规范，请检查！");
				return false;
			}
		}
		//模板中是否有重复数据
		for(int i = 1; i < tLDRelatedPartySet.size(); i++){
			LDRelatedPartySchema tLDRelatedPartySchema = new LDRelatedPartySchema();
			tLDRelatedPartySchema = tLDRelatedPartySet.get(i).getSchema();

			String pName = tLDRelatedPartySchema.getName();
			String pIDNo = tLDRelatedPartySchema.getIDNo();
			String pRelatedType = tLDRelatedPartySchema.getRelatedType();
			for(int j=i+1; j<=tLDRelatedPartySet.size(); j++){
				LDRelatedPartySchema tLDRelatedPartySchema2 = new LDRelatedPartySchema();
				tLDRelatedPartySchema2 = tLDRelatedPartySet.get(j).getSchema();

				String pName2 = tLDRelatedPartySchema2.getName();
				String pIDNo2 = tLDRelatedPartySchema2.getIDNo();
				String pRelatedType2 = tLDRelatedPartySchema2.getRelatedType();
				if(pRelatedType.equals(pRelatedType2)){
					if("1".equals(pRelatedType) && pName.equals(pName2) && pIDNo.equals(pIDNo2)){
						mErrors.addOneError("导入关联方清单中，已经存在关联方："+pName+"，不能重复添加，请检查！");
						this.mErrors.setContent("导入关联方清单中，已经存在关联方："+pName+"，不能重复添加，请检查！");
						return false;
					}
					if("2".equals(pRelatedType) && pName.equals(pName2)){
						mErrors.addOneError("导入关联方清单中，已经存在关联方："+pName+"，不能重复添加，请检查！");
						this.mErrors.setContent("导入关联方清单中，已经存在关联方："+pName+"，不能重复添加，请检查！");
						return false;
					}
				}
			}
				
		}
		String sql = "select * from ldrelatedparty with ur";
		LDRelatedPartySet dLDRelatedPartySet = new LDRelatedPartyDB().executeQuery(sql);
		map.put(dLDRelatedPartySet, "DELETE");
		for (int i = 1; i <= tLDRelatedPartySet.size(); i++) {
			LDRelatedPartySchema tLDRelatedPartySchema = tLDRelatedPartySet.get(i).getSchema();
			String pRelatedType = tLDRelatedPartySchema.getRelatedType();
			LDRelatedPartySchema mLDRelatedPartySchema = new LDRelatedPartySchema();
			String pRelateNo = PubFun1.CreateMaxNo("RELATENO", 10);
			System.out.println(pRelateNo);
			mLDRelatedPartySchema.setName(tLDRelatedPartySchema.getName());
			String tsql="";
			if (pRelatedType == "2" || "2".equals(pRelatedType)) {// 团体关联方
				mLDRelatedPartySchema.setRelateNo(pRelateNo);
				mLDRelatedPartySchema.setRgtMoney(tLDRelatedPartySchema
						.getRgtMoney());
				mLDRelatedPartySchema.setBusinessScope(tLDRelatedPartySchema
						.getBusinessScope());
				mLDRelatedPartySchema.setChairmanName(tLDRelatedPartySchema
						.getChairmanName());
				mLDRelatedPartySchema
						.setGeneralManagerName(tLDRelatedPartySchema
								.getGeneralManagerName());
			} else if (pRelatedType == "1" || "1".equals(pRelatedType)) {
				mLDRelatedPartySchema.setRelateNo(pRelateNo);
				mLDRelatedPartySchema.setSex(tLDRelatedPartySchema.getSex());
				mLDRelatedPartySchema.setPosition(tLDRelatedPartySchema.getPosition());
				mLDRelatedPartySchema.setIDNo(tLDRelatedPartySchema.getIDNo());
				mLDRelatedPartySchema.setOtherOrganization(tLDRelatedPartySchema.getOtherOrganization());
			}
			mLDRelatedPartySchema.setRelatedType(pRelatedType);
			mLDRelatedPartySchema.setOperator(mGlobalInput.Operator);
			mLDRelatedPartySchema.setMakeDate(CurrentDate);
			mLDRelatedPartySchema.setMakeTime(CurrentTime);
			mLDRelatedPartySchema.setModifyDate(CurrentDate);
			mLDRelatedPartySchema.setModifyTime(CurrentTime);

			// 将每一条数据放入返回的结果集
			this.mResult.add(mLDRelatedPartySchema);
			// 添加一条信息
			addOne(map, mLDRelatedPartySchema);

		}

		// 提交数据到数据库
		if (!submit(map)) {
			return false;
		}
		return true;
	}

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!doAdd(mPath, mFileName)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "AddRelatedTransactionList";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败AddRelatedTransactionList-->doAdd!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println(mPath);
		System.out.println(mFileName);

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);
		TransferData tTransferData = new TransferData();
		tTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mPath = (String) tTransferData.getValueByName("FilePath");
		mFileName = (String) tTransferData.getValueByName("FileName");
		return true;

	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "AddRelatedTransactionList";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	/**
	 * 添加一个关联方
	 *
	 */
	private void addOne(MMap aMap, LDRelatedPartySchema aLDRelatedPartySchema) {
		aMap.put(aLDRelatedPartySchema, "INSERT");
	}

	/**
	 * 数据提交公共方法
	 * 
	 * @return
	 */
	public boolean submit(MMap aMap) {
		PubSubmit pb = new PubSubmit();
		VData tVData = new VData();
		tVData.add(aMap);
		if (!pb.submitData(tVData, "")) {
			// @@错误处理
			this.mErrors.copyAllErrors(pb.mErrors);

			CError tError = new CError();
			tError.moduleName = "AddRelatedTransactionList";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";

			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public int getSuccNum() {
		return mSuccNum;
	}

	public void setMSuccNum(int succNum) {
		mSuccNum = succNum;
	}

	public String getBatchNo() {
		return mBatchNo;
	}
}
