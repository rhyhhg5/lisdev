package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.LCContDBSet;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorInureBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    
    /** 往后面传输数据的容器 */
    private VData mInputData= new VData();
    
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();
    
    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();
    
    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();
    
    /** 数据操作字符串 */
    private String mOperate; 
    
    private String mContNo;
    
    public PEdorInureBL(GlobalInput gi,String contNo){
    	this.mGlobalInput = gi;
    	this.mContNo = contNo;
    }
    
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(){
  
    	if (!getInputData()){
            return false;
        }

    	if(!checkData()){
    		return false;
    	}
    	
    	//进行业务处理
    	if(!dealData()){
    		
            return false;
    	}
    	
    	//装配处理好的数据，准备给后台进行保存    	
    	if (!prepareOutputData()){
            return false;
        }
        
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)){
            // @@错误处理
            System.out.println("OLGGroupTestBL中提交数据出错");
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "TaskDeliverBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        
    	return true;
    }
    
    private boolean getInputData(){
    	
        return true;
    }
    
    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData(){
    	LCContDB tLCContDB = new LCContDB();
    	tLCContDB.setContNo(mContNo);
    	if(!tLCContDB.getInfo()){
    		CError tError = new CError();
    	    tError.moduleName = "LCContDB";
    	    tError.functionName = "LCContDB";
    	    tError.errorMessage = "无数据!";
    	    System.out.println("------" + tError);
    	    this.mErrors.addOneError(tError);
    	    return false;
    	}
    	LCPolDB tLCPolDB = new LCPolDB();
    	tLCPolDB.setContNo(mContNo);
    	if(tLCPolDB.query().size() <= 0){
    		CError tError = new CError();
    	    tError.moduleName = "LCPolDB";
    	    tError.functionName = "LCPolDB";
    	    tError.errorMessage = "无数据!";
    	    System.out.println("------" + tError);
    	    this.mErrors.addOneError(tError);
    	    return false;
    	}
    	
    	LCContStateDB tLCContStateDB = new LCContStateDB();
    	tLCContStateDB.setContNo(mContNo);
    	if(tLCContStateDB.query().size() <= 0){
    		CError tError = new CError();
    	    tError.moduleName = "LCContState";
    	    tError.functionName = "LCContState";
    	    tError.errorMessage = "无数据!";
    	    System.out.println("------" + tError);
    	    this.mErrors.addOneError(tError);
    	    return false;
    	}
    	
    	LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
    	tLOPRTManagerDB.setOtherNo(mContNo);
    	if(tLOPRTManagerDB.query().size() <= 0){
    		CError tError = new CError();
    	    tError.moduleName = "LOPRTManagerDB";
    	    tError.functionName = "LOPRTManagerDB";
    	    tError.errorMessage = "无数据!";
    	    System.out.println("------" + tError);
    	    this.mErrors.addOneError(tError);
    	    return false;
    	}
    	
    	LJSPayBDB tLJSPayBDB = new LJSPayBDB();
    	tLJSPayBDB.setOtherNo(mContNo);
    	if(tLJSPayBDB.query().size() <= 0){
    		CError tError = new CError();
    	    tError.moduleName = "LJSPayBDB";
    	    tError.functionName = "LJSPayBDB";
    	    tError.errorMessage = "无数据!";
    	    System.out.println("------" + tError);
    	    this.mErrors.addOneError(tError);
    	    return false;
    	}
    	
        return true;
    }
    
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData(){
    	String strsql = "";
    	String getnoticeno = getGetNoticeNo(mContNo);
    	
    	if(getnoticeno != null && !getnoticeno.equals("")){
    		strsql = "polstate='', ";
    		
    		String sql = "update LJSPayB "
                       + "set dealstate='2', "
                       + "ModifyDate='" + mCurrentDate + "', "
                       + "ModifyTime='" + mCurrentTime + "', "
                       + "Operator='" + mGlobalInput.Operator + "' "
                       + "where Getnoticeno='" + getnoticeno + "' ";
    		map.put(sql, "UPDATE");
    	}
    	
    	String sql = "update LCCont "
                   + "set StateFlag='1', "
                   + "ModifyDate='" + mCurrentDate + "', "
                   + "ModifyTime='" + mCurrentTime + "', "
                   + "Operator='" + mGlobalInput.Operator + "' "
                   + "where ContNo='" + mContNo + "' ";
    	map.put(sql, "UPDATE");
    	
        String sql2 = "update LCPol "
                    + "set stateflag='1', "
                    + strsql
                    + "ModifyDate='" + mCurrentDate + "', "
                    + "ModifyTime='" + mCurrentTime + "', "
                    + "Operator='" + mGlobalInput.Operator + "' "
                    + "where ContNo='" + mContNo + "' ";
        map.put(sql2, "UPDATE");
        
        String sql3 = "delete from LCContState where ContNo='" + mContNo + "' ";
        map.put(sql3, "DELETE");
        
        String sql4 = "delete from LOPRTManager where Otherno='" + mContNo + "' "
                    + "and code='42' ";
        map.put(sql4, "DELETE");
        
        return true;
    }
    
    private String getGetNoticeNo(String contno){
    	String sql = "select distinct Getnoticeno from ljspayb "
                   + "where Otherno='" + contno + "' and dealstate='3' ";
    	SSRS tSSRS = null;
    	ExeSQL e = new ExeSQL();
    	String getnoticeno;
        tSSRS = e.execSQL(sql);
        if(tSSRS == null || tSSRS.getMaxRow() == 0){
        	getnoticeno = "";
        }else{
        	getnoticeno = tSSRS.GetText(1, 1);
        }
        
    	return getnoticeno;
    }
    
    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private boolean prepareOutputData(){
    	try{
            this.mInputData.clear();
            mInputData.add(this.map); 
        }catch(Exception ex){
    		CError tError =new CError();
     		tError.moduleName="LGGroupBL";
     		tError.functionName="prepareData";
     		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
     		this.mErrors .addOneError(tError) ;
    		return false;
    	}
    	return true;
    }
    
    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult(){
  	    return this.mResult;
	}
}
