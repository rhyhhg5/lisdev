package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCGrpAddressDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LICertifyInsuredDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.vschema.LPEdorEspecialDataSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class WSCertifyToWorkBL {
	public CErrors mErrors = new CErrors();

	private String mMessage = null;

	private MMap mMap = new MMap();

	private GlobalInput mGlobalInput = null;

	private EdorItemSpecialData mSpecialData = null;

	private String mEdorNo = null;

	private String mEdorType = "JM";

	private String mCardNo = null;
	
	private String mName = null;
	
	private String mIDNo = null;
	
	private String mInsuredNo = null;

	//JM对应的typeno
	private String mTypeNo = "0310022";

	private String mGrpContNo = null;

	private LICertifySchema mLICertifySchema = null;
	
    private TransferData mTransferData = null;

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/**
	 * 构造函数
	 * 
	 * @param gi
	 *            GlobalInput
	 * @param edorNo
	 *            String
	 * @param grpContNo
	 *            String
	 */
	public WSCertifyToWorkBL() {
	}

	public String getmEdorNo() {
		return mEdorNo;
	}

	public void setmEdorNo(String mEdorNo) {
		this.mEdorNo = mEdorNo;
	}

	/**
	 * 提交数据
	 * 
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		if (!submit()) {
			return false;
		}
		return true;
	}

	/**
	 * 得到提示信息
	 * 
	 * @return String
	 */
	public String getMessage() {
		return mMessage;
	}

	/**
	 * 得到输入数据
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {

		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		mLICertifySchema = (LICertifySchema) cInputData.getObjectByObjectName(
				"LICertifySchema", 0);
		mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null||mGlobalInput == null)
        {
        	CError tError = new CError();
			tError.moduleName = "WSCertifyToWorkBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "所需参数不完整。";
			this.mErrors.addOneError(tError);
			return false;
        }
		mCardNo = mLICertifySchema.getCardNo();
		mName = (String) mTransferData.getValueByName("Name");
		mIDNo = (String) mTransferData.getValueByName("IDNo");
		if (mCardNo.equals("") || mCardNo == null||mName.equals("") || mName == null||mIDNo.equals("") || mIDNo == null) {
			CError tError = new CError();
			tError.moduleName = "WSCertifyToWorkBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "line112:数据获取失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
//		修改为多被保人激活卡修改客户资料变更,因而从licertifyinsured中获取被保人号.
		LICertifyInsuredDB tLICertifyInsuredDB=new LICertifyInsuredDB();
		tLICertifyInsuredDB.setCardNo(mCardNo);
		tLICertifyInsuredDB.setName(mName);
		tLICertifyInsuredDB.setIdNo(mIDNo);
		LICertifyInsuredSet tLICertifyInsuredSet=new LICertifyInsuredSet();
		tLICertifyInsuredSet=tLICertifyInsuredDB.query();
		if (!(tLICertifyInsuredSet.size()==1)) {
			CError tError = new CError();
			tError.moduleName = "WSCertifyToWorkBL";
			tError.functionName = "CreatEdor";
			tError.errorMessage = "line230:tLICertifyInsuredSet获取失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInsuredNo=tLICertifyInsuredSet.get(1).getCustomerNo();
		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

		if (!CreatWork()) {
			return false;
		}
		if (!CreatGrpEdor()) {
			return false;
		}
		if (!CreatEdor()) {
			return false;
		}
		if (!SaveCardNo()) {
			return false;
		}
		return true;
	}

	/**
	 * 校验数据
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		String tIsModified="";
//		修改为按保单号和被保人号一起进行校验
//		tIsModified=new ExeSQL().getOneValue("Select EdorNo From LPEdorEspecialData a Where EdorType='JM' And DetailType='CARDNO' And Edorvalue='"+mCardNo+"' And Exists (select 1 from lgwork where statusno!='8' and workno=a.edorno)");
		tIsModified=new ExeSQL().getOneValue("Select edorno From lpedoritem a Where EdorType='JM' And insuredno='"+mInsuredNo+"' And contno='"+mCardNo+"' And Exists (select 1 from lgwork where statusno!='8' and workno=a.edorno) and edorreasonno is null ");
		if(tIsModified!=null&&(!tIsModified.equals(""))&&(!tIsModified.equals("null")))
		{
			CError tError = new CError();
			tError.moduleName = "WSCertifyToWorkBL";
			tError.functionName = "checkData";
			tError.errorMessage = "此卡号已经做过一次激活卡客户资料变更,无法再次变更!之前的工单号为"+tIsModified+"!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 将卡号存入Edorespecialdata
	 * 
	 * @return boolean
	 */
	private boolean SaveCardNo() {
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorNo);
		tLPEdorEspecialDataSchema.setEdorNo(mEdorNo);
		tLPEdorEspecialDataSchema.setEdorType(mEdorType);
		tLPEdorEspecialDataSchema.setDetailType("CARDNO");
		tLPEdorEspecialDataSchema.setPolNo("000000");
		tLPEdorEspecialDataSchema.setEdorValue(mCardNo);
		LPEdorEspecialDataSchema aLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		aLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorNo);
		aLPEdorEspecialDataSchema.setEdorNo(mEdorNo);
		aLPEdorEspecialDataSchema.setEdorType(mEdorType);
		aLPEdorEspecialDataSchema.setDetailType("INSUREDNO");
		aLPEdorEspecialDataSchema.setPolNo("000000");
		aLPEdorEspecialDataSchema.setEdorValue(mInsuredNo);

		LPEdorEspecialDataSet tLPEdorEspecialDataSet = new LPEdorEspecialDataSet();
		tLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema);
		tLPEdorEspecialDataSet.add(aLPEdorEspecialDataSchema);
		mMap.put(tLPEdorEspecialDataSet, "DELETE&INSERT");
		return true;
	}

	/**
	 * 生成团单保全
	 * 
	 * @return boolean
	 */
	private boolean CreatGrpEdor() {
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		TransferData tTransferData = new TransferData();

		tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorNo);
		tLPGrpEdorMainSchema.setEdorNo(mEdorNo);
		tLPGrpEdorMainSchema.setGrpContNo(mGrpContNo);
		tLPGrpEdorMainSchema.setEdorAppDate(mCurrentDate);
		tLPGrpEdorMainSchema.setEdorValiDate(mCurrentDate);

		tLPGrpEdorItemSchema.setEdorAcceptNo(mEdorNo);
		tLPGrpEdorItemSchema.setEdorNo(mEdorNo);
		tLPGrpEdorItemSchema.setEdorAppNo(mEdorNo);
		tLPGrpEdorItemSchema.setEdorType(mEdorType);
		tLPGrpEdorItemSchema.setGrpContNo(mGrpContNo);
		tLPGrpEdorItemSchema.setEdorAppDate(mCurrentDate);
		tLPGrpEdorItemSchema.setEdorValiDate(mCurrentDate);
		tLPGrpEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
		mLPGrpEdorItemSet.add(tLPGrpEdorItemSchema);

		// 准备传输数据 VData
		VData tVData = new VData();

		tVData.add(mLPGrpEdorItemSet);
		tVData.add(tLPGrpEdorMainSchema);
		tVData.add(tTransferData);
		tVData.add(mGlobalInput);

		GrpEdorItemBL tGrpEdorItemBL = new GrpEdorItemBL();
		MMap tmap=tGrpEdorItemBL.getsubmitData(tVData, "INSERT||GRPEDORITEM");
		if(tmap==null)
		{
			this.mErrors=tGrpEdorItemBL.mErrors;
			return false;
		}
		
		mMap.add(tmap);

		return true;
	}

	/**
	 * 生成个单保全
	 * 
	 * @return boolean
	 */
	private boolean CreatEdor() {
		// 个人主表批改信息
		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
		LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
		// 团体项目批改信息
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();

//		LCContDB tLCContDB = new LCContDB();
//		tLCContDB.setContNo(mCardNo);
//		if (!tLCContDB.getInfo()) {
//			CError tError = new CError();
//			tError.moduleName = "WSCertifyToWorkBL";
//			tError.functionName = "CreatEdor";
//			tError.errorMessage = "line230:LCCont获取失败!";
//			this.mErrors.addOneError(tError);
//			return false;
//		}
		
		tLPGrpEdorItemSchema.setEdorAcceptNo(mEdorNo);
		tLPGrpEdorItemSchema.setGrpContNo(mGrpContNo);
		tLPGrpEdorItemSchema.setEdorNo(mEdorNo);
		tLPGrpEdorItemSchema.setEdorType(mEdorType);

		tLPEdorItemSchema.setEdorAcceptNo(mEdorNo);
		tLPEdorItemSchema.setContNo(mCardNo);
		tLPEdorItemSchema.setEdorNo(mEdorNo);
		tLPEdorItemSchema.setInsuredNo(mInsuredNo);
		tLPEdorItemSet.add(tLPEdorItemSchema);

		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.addElement(mGlobalInput);
		tVData.addElement(tLPEdorItemSet);
		tVData.addElement(tLPGrpEdorItemSchema);

		// 保存个人保单信息(保全)
		GEdorDetailBL tGEdorDetailBL = new GEdorDetailBL();
		
		MMap tmap=tGEdorDetailBL.getsubmitData(tVData, "INSERT||EDOR");
		if(tmap==null)
		{
			this.mErrors=tGEdorDetailBL.mErrors;
			return false;
		}
		
		mMap.add(tmap);

		return true;
	}

	/**
	 * 生成新建工单
	 * 
	 * @return boolean
	 */
	private boolean CreatWork() {
		String tGrpContNo = new ExeSQL()
				.getOneValue("select grpcontno from lccont where contno='"
						+ mCardNo + "'");
		mGrpContNo = tGrpContNo;
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(tGrpContNo);
		if (!tLCGrpContDB.getInfo()) {
			CError tError = new CError();
			tError.moduleName = "WSCertifyToWorkBL";
			tError.functionName = "CreatWork";
			tError.errorMessage = "line165:LCGrpCont获取失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
		tLCGrpAddressDB.setCustomerNo(tLCGrpContDB.getAppntNo());
		tLCGrpAddressDB.setAddressNo(tLCGrpContDB.getAddressNo());
		if (!tLCGrpAddressDB.getInfo()) {
			CError tError = new CError();
			tError.moduleName = "WSCertifyToWorkBL";
			tError.functionName = "CreatWork";
			tError.errorMessage = "line177:LCGrpAddress获取失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 输入参数
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(tLCGrpContDB.getAppntNo());
		tLGWorkSchema.setCustomerName(tLCGrpContDB.getGrpName());
		tLGWorkSchema.setStatusNo("3");
		tLGWorkSchema.setTypeNo(mTypeNo);
		tLGWorkSchema.setDateLimit("5");
		tLGWorkSchema.setApplyTypeNo("0");
		tLGWorkSchema.setApplyName(tLCGrpAddressDB.getLinkMan1());
		tLGWorkSchema.setAcceptDate(mCurrentDate);

		VData tVData = new VData();
		tVData.add(tLGWorkSchema);
		tVData.add(mGlobalInput);

		TaskInputBL tTaskInputBL = new TaskInputBL();

		MMap tmap=tTaskInputBL.getSubmitData(tVData, "");
		if(tmap==null)
		{
			this.mErrors=tTaskInputBL.mErrors;
			return false;
		}
		
		mMap.add(tmap);
		mEdorNo = tTaskInputBL.getWorkNo();

		return true;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit() {
		VData data = new VData();
		data.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		WSCertifyToWorkBL bl =new WSCertifyToWorkBL();
//		bl.mCardNo="21000002134";
//		bl.mEdorNo="20100902000002";
//		bl.mGrpContNo="00003372000001";
//		bl.mEdorType="JM";
		GlobalInput gi=new GlobalInput();
		gi.ManageCom="86";
		gi.Operator="xp";

		LICertifySchema tLICertifySchema = new LICertifySchema();
		tLICertifySchema.setCardNo("21000000465");
	    VData tVData = new VData();

	    TransferData tTransferData = new TransferData();

	    tVData.add(tLICertifySchema);
	    tVData.add(tTransferData);
	    tVData.add(gi);
	    bl.submitData(tVData, null);
	}
	
}
