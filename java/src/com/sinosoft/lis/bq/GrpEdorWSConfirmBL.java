package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class GrpEdorWSConfirmBL implements EdorConfirm
{
    private GlobalInput mGlobalInput = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);
        if (!dealData())
        {
            return false;
        }
        if (!dealUrgeLog("3", "UPDATE")) 
        {
            return false;
        }

        return true;
    }

    /**
     * 返回计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(new MMap());
        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
        mEdorType = mLPGrpEdorItemSchema.getEdorType();
        mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();

    }

    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
       try 
       {
			if (!dealUrgeLog("1", "INSERT"))
			   {
			         return false;
			   }
			    VData tInputData = new VData();
			    RSWrapper rswrapper = new RSWrapper();
			    LPContSet tLPContSet = new LPContSet();
			    //查询没有确认成功的保单,取在P表，但是不在C表的数据。
			    String queryData = "select * from lpcont a where edorno ='"+mEdorNo+"' and edortype ='WS' "
			                       +" and grpcontno ='"+mGrpContNo+"' and "
			                       +" not exists (select 1 from lccont b where b.proposalcontno = a.proposalcontno and b.grpcontno = a.grpcontno) with ur"
			                       ;
			    rswrapper.prepareData(tLPContSet,queryData);
			    do {
			        rswrapper.getData();
			        //循环取得5000条数据，进行处理
			        for(int i=1;i<=tLPContSet.size();i++)
			        {
			            LPContSchema tLPContSchema = tLPContSet.get(i);
			            MMap tMMap = new MMap();
			            if (!validateEdorData(tLPContSchema,tMMap))
			            {
			                return false;
			            }
			            //保单换号
			            if(!changeContNo(tLPContSchema,tMMap))
			            {
			                return false;
			            }
			            //险种换号
			            if(!changePolNo(tLPContSchema,tMMap))
			            {
			                return false;
			            }
//			            添加地址表的变更逻辑
			    		LPAddressDB tLPAddressDB = new LPAddressDB();
			    		tLPAddressDB.setEdorNo(tLPContSchema.getEdorNo());
			    		tLPAddressDB.setEdorType(tLPContSchema.getEdorType());
			    		tLPAddressDB.setCustomerNo(tLPContSchema.getInsuredNo());
			    		LPAddressSet tLPAddressSet = tLPAddressDB.query();
			    		for (int j = 1; j <= tLPAddressSet.size(); j++)
			    		{
			    			LCAddressSchema tLCAddressSchema = new LCAddressSchema();
			    			Reflections ref =new Reflections();
			    			ref.transFields(tLCAddressSchema, tLPAddressSet.get(j));
			    			tLCAddressSchema.setModifyDate(mCurrentDate);
			    			tLCAddressSchema.setModifyTime(mCurrentTime);
			    			tMMap.put(tLCAddressSchema, SysConst.DELETE_AND_INSERT);
			    		}
			            //提交数据库
			            PubSubmit tPubSubmit = new PubSubmit();
			            tInputData.clear();
			            tInputData.add(tMMap);
			            if (!tPubSubmit.submitData(tInputData, ""))
			            {
			                    // @@错误处理
			                    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			                    CError tError = new CError();
			                    tError.moduleName = "GrpEdorWSConfirmBL";
			                    tError.functionName = "dealData";
			                    tError.errorMessage = "实名化保全确认提交失败!";
			                    this.mErrors.addOneError(tError);
			                    return false;
			            }
			        }
			     }while (tLPContSet.size() > 0);
	   } 
       catch (Exception e) 
       {
			e.printStackTrace();
			dealUrgeLog("1", "DELETE");
			return false;
	   }

         return true;
    }

    /**
     * 加入到保全结案日志表数据
     * @param
     * @return boolean
     */
    private boolean dealUrgeLog(String pmDealState, String pmOpreat) {

        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                LCUrgeVerifyLogSchema();
        //加到保全结案日志表
        tLCUrgeVerifyLogSchema.setSerialNo(mEdorNo);
        tLCUrgeVerifyLogSchema.setRiskFlag("1");
        tLCUrgeVerifyLogSchema.setEndDate(mCurrentDate);

        tLCUrgeVerifyLogSchema.setOperateType("3"); //1：续期催收操作,2：续期核销操作，3：保全结案操作
        tLCUrgeVerifyLogSchema.setOperateFlag("3"); //1：个案操作,2：批次操作  3：保全团单结案操作
        tLCUrgeVerifyLogSchema.setOperator(mGlobalInput.Operator);
        tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成
        if (pmOpreat.equals("INSERT")) {
            tLCUrgeVerifyLogSchema.setMakeDate(mCurrentDate);
            tLCUrgeVerifyLogSchema.setMakeTime(mCurrentTime);
            tLCUrgeVerifyLogSchema.setModifyDate(mCurrentDate);
            tLCUrgeVerifyLogSchema.setModifyTime(mCurrentTime);
            tLCUrgeVerifyLogSchema.setErrReason("");

        } 
        else if(pmOpreat.equals("UPDATE"))
        {
            LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mEdorNo);
            tLCUrgeVerifyLogDB.setOperateType("3");
            tLCUrgeVerifyLogDB.setOperateFlag("3");
            tLCUrgeVerifyLogDB.setRiskFlag("1");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
            if (!(tLCUrgeVerifyLogSet == null) &&
                tLCUrgeVerifyLogSet.size() > 0) {
                tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                tLCUrgeVerifyLogSchema.setDealState(pmDealState);
            }else{
                return false;
            }
        }


        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("该工单正在进行保全确认!");
            return false;
        }
        return true;
    }
    
    
    
    
    /**
     * 使保全数据生效
     * @return boolean
     */
    private boolean validateEdorData(LPContSchema aLPContSchema,MMap aMMap)
    {
        validateLDPerson(aLPContSchema,aMMap);
        String[] tables = {"LCCont", "LCInsured", "LCPol", "LCDuty" ,"LCPrem", "LCGet"};
        ValidateEdorData2 tValidateEdorData = new ValidateEdorData2(mGlobalInput, mEdorNo, mEdorType,
                aLPContSchema.getContNo(), "ContNo");
        tValidateEdorData.addData(tables);
        aMMap.add(tValidateEdorData.getMap());
        return true;
    }

    /**
     * 处理LDPerson
     * @return boolean
     */
    private boolean validateLDPerson(LPContSchema aLPContSchema,MMap aMMap)
    {
        LPPersonDB tLPPersonDB = new LPPersonDB();
        String querySql = "Select * from LPPerson a where a.edorno='"+aLPContSchema.getEdorNo()
                          +"' and a.edortype ='"+aLPContSchema.getEdorType()+"' and a.customerno in ("
                          +"select insuredno from lpinsured b where b.edorno = a.edorno and b.edortype = a.edortype"
                          +" and contno = '"+aLPContSchema.getContNo()+"')"
                          ;

        LPPersonSet tLPPersonSet = tLPPersonDB.executeQuery(querySql);
        if(tLPPersonSet.size()<1)
        {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWSConfirmBL";
            tError.functionName = "validateLDPerson";
            tError.errorMessage = "查询实名化客户数据错误!";
            this.mErrors.addOneError(tError);
            return false;
        }

        for (int i = 1; i <= tLPPersonSet.size(); i++)
        {
            LPPersonSchema tLPPersonSchema = tLPPersonSet.get(i);
            LDPersonSchema tLDPersonSchema = new LDPersonSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLDPersonSchema, tLPPersonSchema);
            tLDPersonSchema.setOperator(mGlobalInput.Operator);
            tLDPersonSchema.setModifyDate(mCurrentDate);
            tLDPersonSchema.setModifyTime(mCurrentTime);
            aMMap.put(tLDPersonSchema, "DELETE&INSERT");
        }

        return true;
    }


    /**
     * 换保单号
     * @return boolean
     */
    private boolean changeContNo(LPContSchema aLPContSchema,MMap aMMap)
    {
        String[] lcTables = {"LCCont", "LCInsured", "LCPol", "LCDuty", "LCPrem", "LCGet"};
        String[] lpTables = {"LPCont", "LPInsured", "LPPol", "LPDuty", "LPPrem", "LPGet"};

            LPContSchema tLPContSchema = aLPContSchema;
            String contNo = PubFun1.CreateMaxNo("GRPPERSONCONTNO", null);
            for (int j = 0; j < lcTables.length; j++)
            {
                String sql = "update " + lcTables[j] +
                        " set ContNo = '" + contNo + "' " +
                        "where ContNo = '" + tLPContSchema.getContNo() + "' ";
                aMMap.put(sql, "UPDATE");
            }
            for (int j = 0; j < lpTables.length; j++)
            {
                String sql = "update " + lpTables[j] +
                        " set ContNo = '" + contNo + "' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + mEdorType + "' " +
                        "and ContNo = '" + tLPContSchema.getContNo() + "' ";
                aMMap.put(sql, "UPDATE");
            }
        return true;
    }

    private boolean changePolNo(LPContSchema aLPContSchema,MMap aMMap)
    {
        String[] lcTables =
                {"LCPol", "LCDuty", "LCPrem", "LCGet"};
        String[] lpTables =
                {"LPPol", "LPDuty", "LPPrem", "LPGet"};
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        tLPPolDB.setEdorType(mEdorType);
        tLPPolDB.setGrpContNo(mGrpContNo);
        tLPPolDB.setContNo(aLPContSchema.getContNo());
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
            LPPolSchema tLPPolSchema = tLPPolSet.get(i);
            String polNo = PubFun1.CreateMaxNo("POLNO", null);
            for (int j = 0; j < lcTables.length; j++)
            {
                String sql = "update " + lcTables[j] +
                        " set PolNo = '" + polNo + "' " +
                        "where PolNo = '" + tLPPolSchema.getPolNo() + "' ";
                aMMap.put(sql, "UPDATE");
            }
            for (int j = 0; j < lpTables.length; j++)
            {
                String sql = "update " + lpTables[j] +
                        " set PolNo = '" + polNo + "' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + mEdorType + "' " +
                        "and PolNo = '" + tLPPolSchema.getPolNo() + "' ";
                aMMap.put(sql, "UPDATE");
            }
        }
        return true;
    }
}
