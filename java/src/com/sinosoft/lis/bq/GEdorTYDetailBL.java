package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCInsureAccTraceDB;


public class GEdorTYDetailBL
{
    public CErrors mErrors = new CErrors();

    private String mMessage = null;

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private EdorItemSpecialData mSpecialData = null;

    private String mEdorNo = null;

    private static String mEdorType = BQ.EDORTYPE_TY;

    private String mGrpContNo = null;

    private LPDiskImportSet mLPDiskImportSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     * @param grpContNo String
     */
    public GEdorTYDetailBL(GlobalInput gi, String edorNo,
            String grpContNo, EdorItemSpecialData specialData)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = grpContNo;
        this.mSpecialData = specialData;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到输入数据
     * @return boolean
     */
    private boolean getInputData()
    {
        mLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, mEdorType,
                mGrpContNo, BQ.IMPORTSTATE_SUCC);
        //if (mLPDiskImportSet.size() == 0)
        //{
        //    mErrors.addOneError("找不到磁盘导入数据！");
        //    return false;
        //}
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!checkData())
        {
            return false;
        }
        setSpecialData();
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if (!checkClaiming())
        {
            return false ;
        }
        if (!checkBQ())
        {
            return false ;
        }
        if (!checkCustomers())
        {
            return false;
        }
        return true;
    }

    /**
     * 检查客户的有效性
     * @return boolean
     */
    private boolean checkCustomers()
    {
        boolean flag = true;
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            tLCInsuredSchema.setGrpContNo(mGrpContNo);
            tLCInsuredSchema.setName(tLPDiskImportSchema.getInsuredName());
            tLCInsuredSchema.setSex(tLPDiskImportSchema.getSex());
            tLCInsuredSchema.setBirthday(tLPDiskImportSchema.getBirthday());
            tLCInsuredSchema.setIDType(tLPDiskImportSchema.getIDType());
            tLCInsuredSchema.setIDNo(tLPDiskImportSchema.getIDNo());
            OldCustomerCheck customerCheck = new OldCustomerCheck(tLCInsuredSchema);
            if (customerCheck.checkInsured() != OldCustomerCheck.OLD)
            {
                StringBuffer sql = new StringBuffer("Update LPDiskImport ");
                sql.append("Set State = '")
                        .append(BQ.IMPORTSTATE_FAIL).append("', ")
                        .append(" ErrorReason = '保单下不存在该客户，导入无效！', ")
                        .append(" Operator = '")
                        .append(mGlobalInput.Operator).append("', ")
                        .append(" ModifyDate = '")
                        .append(mCurrentDate ).append("', ")
                        .append(" ModifyTime = '")
                        .append(mCurrentTime).append("' ")
                        .append("where EdorNo = '")
                        .append(mEdorNo).append("' ")
                        .append("and EdorType = '")
                        .append(mEdorType).append("' ")
                        .append("and GrpContNo = '")
                        .append(mGrpContNo).append("' ")
                        .append("and SerialNo = '")
                        .append(tLPDiskImportSchema.getSerialNo())
                        .append("' ");
                mMap.put(sql.toString(), "UPDATE");
                flag = false;
            }
            else
            {
                StringBuffer sql = new StringBuffer("Update LPDiskImport ");
                sql.append("Set State = '")
                        .append(BQ.IMPORTSTATE_SUCC).append("', ")
                        .append("InsuredNo = '")
                        .append(StrTool.cTrim(customerCheck.getCustomerNo()))
                        .append("', ")
                        .append("Operator = '")
                        .append(mGlobalInput.Operator).append("', ")
                        .append("ModifyDate = '")
                        .append(mCurrentDate).append("', ")
                        .append("ModifyTime = '")
                        .append(mCurrentTime).append("' ")
                        .append("where EdorNo = '")
                        .append(mEdorNo).append("' ")
                        .append("and EdorType = '")
                        .append(mEdorType).append("' ")
                        .append("and GrpContNo = '")
                        .append(mGrpContNo).append("' ")
                        .append("and SerialNo = '")
                        .append(tLPDiskImportSchema.getSerialNo())
                        .append("' ");
                mMap.put(sql.toString(), "UPDATE");
            }
        }
        if (flag == false)
        {
            mMessage = "导入数据中有无效客户！";
        }
        return true;
    }

    /**
     * 保存团单追加金额
     */
    private void setSpecialData()
    {
        mMap.put(mSpecialData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
        String sql = "update  LPGrpEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    /**
     *
     * @return boolean
     */

    private boolean checkClaiming()
    {
        String sql = " select rgtno from llcase where customerno in(select insuredno from lcinsured where grpcontno='"+mGrpContNo+"') and rgtstate not in('11','12','14') with ur";
        ExeSQL temp = new ExeSQL();

        SSRS rs = temp.execSQL(sql);
        if(rs==null||rs.getMaxRow()==0)
        {
            LCInsureAccTraceDB db = new LCInsureAccTraceDB();
            db.setGrpContNo(mGrpContNo);
            db.setState("temp");
            LCInsureAccTraceSet tempSet = db.query();
            if(tempSet ==null || tempSet.size()==0)
            {
                return true;
            }
            else
            {
                mErrors.addOneError("帐户轨迹表中有理赔临时记录，不能操作");
                return false;
            }
        }
        else
        {
            StringBuffer tem = new StringBuffer();
            tem.append("该单正在理赔或有未撤件的理赔申请，不能操作,批次号");
            for(int i = 1;i<=rs.getMaxRow();i++)
            {
                tem.append(rs.GetText(i,1));
                tem.append("  ");
            }
            mErrors.addOneError(tem.toString());
            return false;
        }
    }
    private boolean checkBQ()
    {
        String sql = " select 1 from lpgrpedoritem a where grpcontno='"+mGrpContNo+"' and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0') and (edorno!='"+mEdorNo+"' or edortype!='"+mEdorType+"') with ur";
        ExeSQL temp = new ExeSQL();

        SSRS rs = temp.execSQL(sql);
        if(rs==null||rs.getMaxRow()==0)
        {
        	return true;            
        }
        else
        {
        	// @@错误处理
			System.out.println("GEdorTYDetailBL+checkBQ++--");
			CError tError = new CError();
			tError.moduleName = "GEdorTYDetailBL";
			tError.functionName = "checkBQ";
			tError.errorMessage = "该单存在其他保全项目";
			mErrors.addOneError(tError);
			return false;
        }
    }
}
