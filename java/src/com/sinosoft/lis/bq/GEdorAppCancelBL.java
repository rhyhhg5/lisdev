package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全删除业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author lh
 * @version 1.0
 */

//功能：查询出个人批改主表中本次申请的批改类型
//入口参数：个单的保单号、批单号
//出口参数：每条记录的个单的保单号、批单号和批改类型
public class GEdorAppCancelBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;
    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    private GlobalInput mGlobalInput = new GlobalInput();

    public GEdorAppCancelBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("start BL submit");

        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {

            return false;
        }
        if (cOperate.equals("DELETE||EDOR"))
        {
            if (!checkData())
            {

                return false;

            }

            if (!prepareData())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        System.out.println("======");

        // 查询条件
        System.out.println("moperator:" + mOperate);
        if (mOperate.equals("DELETE||EDOR"))
        {

            mLPGrpEdorItemSchema.setSchema((LPGrpEdorItemSchema) cInputData
                .getObjectByObjectName("LPGrpEdorItemSchema", 0));
        }
        System.out.println("after getInputData");
        return true;
    }






    /**
     *检查数据的合法性
    **/
    private boolean checkData()
    {
        System.out.println("start check data");

        //首先获取EdorState
        ExeSQL tExeSQL = new ExeSQL();

        String strSql = " select EdorState from LPGrpEdorItem where EdorNo = '"
            + mLPGrpEdorItemSchema.getEdorNo() + "'";

        String grpEdorState = tExeSQL.getOneValue(strSql);

        mLPGrpEdorItemSchema.setEdorState(grpEdorState);

        if (mLPGrpEdorItemSchema.getEdorState() == null)
        {
            System.out.println("null");
        }

        //正在申请的批单可以直接删除
        if (mLPGrpEdorItemSchema.getEdorState().equals("1"))
        {

            return true;
        }

        System.out.println("not in state 1");

        //如果是已申请确认的，必须是保单的最后一个批单才可删除
        strSql = "select Max(a.EdorNo) from lpGrpEdormain a,lpGrpEdorItem b where a.GrpContNo = '"
            + mLPGrpEdorItemSchema.getGrpContNo() + "' and EdorState = '2' ";

        String MaxEdorNo = tExeSQL.getOneValue(strSql);

        System.out.println("MaxEdorNo: " + MaxEdorNo);

        if ((MaxEdorNo == null) || (MaxEdorNo.trim() == ""))
        {

            return false;
        }

        if (!MaxEdorNo.equals(mLPGrpEdorMainSchema.getEdorNo()))
        {
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        int m;
        int i;
         System.out .println("prepare data");

         LPGrpEdorItemSchema aLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();
        tLPGrpEdorItemDB.setSchema(mLPGrpEdorItemSchema);

        //    tLPEdorMainDB.setEdorState("1");

        tLPGrpEdorItemSet.set(tLPGrpEdorItemDB.query());

        if (tLPGrpEdorItemDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLPGrpEdorItemDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GEdorAppCancelBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "集体批改主表查询失败!";
            this.mErrors.addOneError(tError);
            tLPGrpEdorItemSet.clear();
            return false;
        }
        if (tLPGrpEdorItemSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GEdorAppCancelBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);
            tLPGrpEdorItemSet.clear();
            return false;
        }
        m = tLPGrpEdorItemSet.size();

        for (i = 1; i <= m; i++)
        {
            aLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(i);
            mInputData.clear();
            mInputData.add(aLPGrpEdorItemSchema);

            GrpEdorAppCancelBL tGrpEdorAppCancelBL = new GrpEdorAppCancelBL();

            if (!tGrpEdorAppCancelBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tGrpEdorAppCancelBL.mErrors);
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args)
    {
        /*    PEdorAppCancelBL tPEdorAppCancelBL=new PEdorAppCancelBL();
            VData tVData=new VData();
            LPEdorMainSchema tLPEdorMainSchema=new LPEdorMainSchema();
            tLPEdorMainSchema.setEdorNo("00000120020420000083");
            tLPEdorMainSchema.setPolNo("00000120020210000016");
            tLPEdorMainSchema.setEdorState("1");
            tVData.addElement(tLPEdorMainSchema);
            tPEdorAppCancelBL.submitData(tVData,"DELETE||EDOR");
        */
    }
}
