package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.get.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.util.*;

/**
 * 保全退保计算类.
 * <p>Title: 保全退保计算类</p>
 * <p>Description: 通过传入的保单信息和保费项目信息计算出交退费变动信息，或保额变动信息</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author Minim
 * @version 1.0
 */
public class EdorCalCT
{
    // @Field
    private final int Year = 365;
    private final int Month = 30;
    private final int CriticalYearInterval = 2;

    //超出临界年限年初生存金退保公式
    private String CalExpressions = "(1-K/Y)*(V1-G1)+K/Y*V2+F";

    //超出临界年限年末生存金退保公式
    private String CalExpressions1 = "(1-K/Y)*V1+K/Y*(V2+G2)+F";
    private LJSGetEndorseSchema mLJSGetEndorseSchema = new LJSGetEndorseSchema();
    private BqCalBase mBqCalBase = new BqCalBase();
    public CErrors mErrors = new CErrors(); // 错误信息

    //
    public LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();

    //
    public LJSGetEndorseSet mLXLJSGetEndorseSet = new LJSGetEndorseSet();

    //
    public LJSGetEndorseSet mMFLJSGetEndorseSet = new LJSGetEndorseSet();

    //
    public LPEdorItemSchema mLPEdorItemSchema = null;

    //
    public LPPolSchema mLPPolSchema = new LPPolSchema();

    //
    public LPDutySchema mLPDutySchema = null;

    //
    public LPPremSchema mLPPremSchema = null;

    //
    public LMEdorZT1Schema mLMEdorZT1Schema = new LMEdorZT1Schema();

    //
    public LMEdorZTAccSchema mLMEdorZTAccSchema = null;

    //
    public LPInsureAccSchema mLPInsureAccSchema = null;

    //TransFlag－－'1' ：简单计算；'2' ：普通计算；'3' ：复杂计算；
    private String mTransFlag = "";

    //ZTFlag－－'1' ：生存退保；'2' ：死亡退保；'3' ：外部转移；
    private String mZTType = "";

    //退保计算类型，是按保费还是保额计算－－'0'：按保费；'1'：按保额；'2'：按帐户
    private String mCalType = "";

    //退保保单年度
    private int mZTYear = 0;

    //退保保单缴费间隔
    private int mPayYears = 0;
    //退保保单缴费间隔标志
    //A－年龄，M－月，D－日，Y－年
    private String mPayEndYearFlag = "";

    //退保点（退保生效日）
    private String mZTPoint = "";

    //计算用保单交至日期
    private String mPayToDate = "";

    private String mCurPayToDateForesee = null;

    //计算用保单交费间隔
    private int mPayIntv = 0;

    //计算用保单交费间隔
    private String mPolCValiDate = "";

    //计算用保单交费间隔
    private String mRiskCode = "";

    private boolean mGetDataFromC = false;

    //计算用退保点距保单生效对应日的天数
    private int mZTDays = 0;

    //计算要素免交保费
    private double mOwePrem = 0;

    //计算要素预交保费
    private double mPrepayPrem = 0;

    //计算要素未经过保费
    private double mNetPrem = 0;

    //计算要素垫交保费
    private double mTrayPrem = 0;

    //计算要素垫交保费利息
    private double mTrayPremInterest = 0;

    //计算要素当年应交未交保费
    private double mUnfillPrem = 0;

    //年初生存金
    private double mGetAliveBegin = 0;

    //年末生存金
    private double mGetAliveEnd = 0;

    //年初现金价值
    private double mCashValueBegin = 0;

    //年末现金价值
    private double mCashValueEnd = 0;

    //退保金额，初始化为Double.MIN_VALUE便于处理产生退费为0的情况
    //在生成财务数据时由于截取有效位数，Double.MIN_VALUE将会因为舍入得到0值，因此不会影响财务数据的正确性
    private double mTBMoney = Double.MIN_VALUE;

    private double mLXMoney = Double.MIN_VALUE;
    //未扣管理费利息
    public double mLXFMoney = Double.MIN_VALUE;
    //扣管理费利息
    public double mLXEMoney = Double.MIN_VALUE;
    //退费金额（预交保费）
    private double mTFMoney = Double.MIN_VALUE;

    //退帐户金额
    private double mAccTBMoney = Double.MIN_VALUE;
    public double mAccTBFMoney = Double.MIN_VALUE;

    public double mAccMoney = Double.MIN_VALUE;

    //计算次数
    private int mCalTimes = 0;

    //计算比率
    private double mCalRate = 0;

    //借款金额
    private double mLoanMoney = 0;

    //保全生效日
    private String mEdorValidate = "";

    private final String LAST = "LAST"; //上一个
    private final String NEXT = "NEXT"; //下一个

    //qulq
    private LGErrorLogSet mLGErrorLogSet = new LGErrorLogSet();

    public EdorCalCT()
    {
    }

    /**
     * 构造函数
     * @param aZTType '1' ：生存退保；'2' ：死亡退保；'3' ：外部转移；
     */
    public EdorCalCT(String aZTType)
    {
        mZTType = aZTType;
    }

    /**
     * 构造函数
     * @param aTransFlag '1' ：简单计算；'2' ：普通计算；'3' ：复杂计算；
     * @param aZTType '1' ：生存退保；'2' ：死亡退保；'3' ：外部转移；
     */
    public EdorCalCT(String aTransFlag, String aZTType)
    {
        mTransFlag = aTransFlag;
        mZTType = aZTType;
    }

    /**
     * 构造函数
     * @param tLPPolSchema：需要处理的险种信息
     * @param getDataFromC：险种信息
     * @param aZTType '1' ：生存退保；'2' ：死亡退保；'3' ：外部转移；
     */
    public EdorCalCT(LPPolSchema tLPPolSchema,
                     boolean getDataFromC,
                     String aZTType)
    {
        mLPPolSchema = tLPPolSchema;
        mGetDataFromC = getDataFromC;
        mZTType = aZTType;
    }

    /**
     * 保全信息接口
     * @param aLPEdorItemSchema
     */
    public void setEdorInfo(LPEdorItemSchema aLPEdorItemSchema)
    {
        mLPEdorItemSchema.setSchema(aLPEdorItemSchema);
    }

    public void setCurPayToDateForesee(String date)
    {
        mCurPayToDateForesee = date;
    }

    public LPEdorItemSchema getEdorInfo()
    {
        return mLPEdorItemSchema;
    }

    public void setZTPoint(String aZTPoint)
    {
        mZTPoint = aZTPoint;
    }

    public String getZTPoint()
    {
        return mZTPoint;
    }

    public void setEdorValidate(String aEdorValidate)
    {
        mEdorValidate = aEdorValidate;
    }

    public String getEdorValidate()
    {
        return mEdorValidate;
    }

    public void setCalType(String aCalType)
    {
        mCalType = aCalType;
    }

    public String getCalType()
    {
        return mCalType;
    }

    private void setPayToDate(String aPayToDate)
    {
        mPayToDate = aPayToDate;
    }

    private String getPayToDate()
    {
        return mPayToDate;
    }

    private void setZTDays(int aZTDays)
    {
        mZTDays = aZTDays;
    }

    private int getZTDays()
    {
        return mZTDays;
    }

    private void setOwePrem(double aOwePrem)
    {
        mOwePrem = aOwePrem;
    }

    private double getOwePrem()
    {
        return mOwePrem;
    }

    private void setPrepayPrem(double aPrepayPrem)
    {
        mPrepayPrem = aPrepayPrem;
    }

    private double getPrepayPrem()
    {
        return mPrepayPrem;
    }

    private void setNetPrem(double aNetPrem)
    {
        mNetPrem = aNetPrem;
    }

    private double getNetPrem()
    {
        return mNetPrem;
    }

    private void setTBMoney(double aTBMoney)
    {
        mTBMoney = aTBMoney;
    }

    public double getTBMoney()
    {
        return mTBMoney;
    }

    private void setTFMoney(double aTFMoney)
    {
        mTFMoney = aTFMoney;
    }

    public double getTFMoney()
    {
        return mTFMoney;
    }

    private void setAccTBMoney(double aAccTBMoney)
    {
        mAccTBMoney = aAccTBMoney;
    }

    public double getAccTBMoney()
    {
        return mAccTBMoney;
    }

    private void setTrayPrem(double aTrayPrem)
    {
        mTrayPrem = aTrayPrem;
    }

    public double getTrayPrem()
    {
        return mTrayPrem;
    }

    private void setTrayPremInterest(double aTrayPremInterest)
    {
        mTrayPremInterest = aTrayPremInterest;
    }

    public double getTrayPremInterest()
    {
        return mTrayPremInterest;
    }

    private void setUnfillPrem(double aUnfillPrem)
    {
        mUnfillPrem = aUnfillPrem;
    }

    public double getUnfillPrem()
    {
        return mUnfillPrem;
    }

    private void setZTYear(int aZTYear)
    {
        mZTYear = aZTYear;
    }

    public int getZTYear()
    {
        return mZTYear;
    }

    public void setLPPolSchema(LPPolSchema tLPPolSchema)
    {
        mLPPolSchema = tLPPolSchema;
    }

    /**
     * 计算退保点，交至日期，欠交保费，退保年度
     * @param pLPEdorItemSchema
     * @param pLPDutySchema
     */
    public void calZTPoint()
    {
        //按保额
        if (mCalType.equals("1"))
        {
            getZTPoint(mLPEdorItemSchema.getEdorValiDate(),
                       mLPPolSchema.getCValiDate(), mLPDutySchema.getPaytoDate(),
                       mLPDutySchema.getPayEndDate(), mLPPolSchema.getRiskCode(),
                       mLPDutySchema.getPayIntv(), mLPDutySchema.getPrem(),
                       mLPDutySchema.getFreeFlag(), mLPDutySchema.getFreeRate());
        }

        //按保费
        else if (mCalType.equals("0"))
        {
            getZTPoint(mLPEdorItemSchema.getEdorValiDate(),
                       mLPPolSchema.getCValiDate(), mLPPremSchema.getPaytoDate(),
                       mLPPremSchema.getPayEndDate(), mLPPolSchema.getRiskCode(),
                       mLPPremSchema.getPayIntv(), mLPPremSchema.getPrem(),
                       mLPDutySchema.getFreeFlag(), mLPDutySchema.getFreeRate());
        }
    }

    /**
     * 计算退保点，交至日期，欠交保费，退保年度，在自动垫交（AutoPay）中调用，所有用LCPol表数据
     * @param pPolNo
     * @param pEdorValiDate
     * @return
     */
    public String getZTPoint(String pPolNo, String pEdorValiDate)
    {
        //获取保单信息
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(pPolNo);

        if (!tLCPolDB.getInfo())
        {
            CError.buildErr(this, "获取保单信息失败");
        }

        getZTPoint(pEdorValiDate, tLCPolDB.getCValiDate(),
                   tLCPolDB.getPaytoDate(), tLCPolDB.getPayEndDate(),
                   tLCPolDB.getRiskCode(), tLCPolDB.getPayIntv(),
                   tLCPolDB.getPrem(),
                   "", 0);
// 暂时注掉 by PQ            tLCPolDB.getFreeFlag(), tLCPolDB.getFreeRate());

        return this.getZTPoint();
    }

    /**
     * 计算退保点，交至日期，欠交保费，退保年度，退保点距保单生效对应日的天数
     * @param pEdorValiDate 原保全生效日
     * @param pPolCValiDate 保单生效日
     * @param pPayToDate 原交至日期
     * @param pPayEndDate 交费止期
     * @param pRiskCode 险种代码
     * @param pPayIntv 交费间隔
     * @param pPrem 保费
     * @param pFreeFlag 免交标记
     * @param pFreeRate 免交比例
     */
    public void getZTPoint(String pEdorValiDate, String pPolCValiDate,
                           String pPayToDate, String pPayEndDate,
                           String pRiskCode, int pPayIntv,
                           double pPrem, String pFreeFlag, double pFreeRate)
    {
        try
        {
            FDate tFDate = new FDate();

            //宽限期止期
            String tLapseDate = CalLapseDate(pRiskCode, pPayToDate);

            //责任终止期（失效日期）= 宽限期止期 + 1天
            String tEndDate = transCalDate(tLapseDate, 1, "D", null);
            System.out.println("责任终止期（失效日期）:" + tEndDate);
            System.out.println("交至日期:" + pPayToDate);
            System.out.println("交费间隔:" + pPayIntv);

            //计算退保开始年度（批改生效日－保单生效日）
            int tZTYear = PubFun.calInterval(pPolCValiDate, pEdorValiDate, "Y");
            System.out.println("退保开始年度:" + tZTYear);

            //趸交和满期处理，退保点为保全生效日
            if ((pPayIntv == 0) || (pPayIntv == -1)
                || pPayToDate.equals(pPayEndDate))
            {
                this.setPayToDate(pPayToDate);
                this.setZTPoint(pEdorValiDate);
                this.setOwePrem(0);
                System.out.println("满期退保");
            }

            //两年内退保
            else if (tZTYear < CriticalYearInterval)
            {
                //1.保全生效日大于、等于交至日期，退保点为交至日期的前一天
                if (pEdorValiDate.compareTo(pPayToDate) >= 0)
                {
                    this.setPayToDate(pPayToDate);
                    this.setZTPoint(transCalDate(pPayToDate, -1, "D", null));
                    this.setOwePrem(0);
                    System.out.println("两年内保全生效日大于、等于交至日期退保");
                }

                //2.保全生效日小于交至日期，退保点为保全生效日
                else
                {
                    this.setPayToDate(pPayToDate);
                    this.setZTPoint(pEdorValiDate);
                    this.setOwePrem(0);
                    System.out.println("两年内保全生效日小于交至日期退保");
                }
            }

            //两年以后退保
            else
            {
                //交保费的年数，用于判断是否交足第三年保费
                int payYear = PubFun.calInterval(pPolCValiDate, pPayToDate, "Y");

                //3.未交足第三年保费，退保点为交至日期的前一天
                if (payYear < 3)
                {
                    this.setPayToDate(pPayToDate);
                    this.setZTPoint(transCalDate(pPayToDate, -1, "D", null));
                    this.setOwePrem(0);
                    System.out.println("两年以后未交足第三年保费退保");
                }

                //4.已交三年保费，保全生效日大于等于宽限止期
                else if (pEdorValiDate.compareTo(tLapseDate) >= 0)
                {
                    //交至日期小于等于宽限止期，补交N期保费至大于宽限止期，并记入免交保费
                    while (pPayToDate.compareTo(tLapseDate) <= 0)
                    {
                        pPayToDate = this.transCalDate(pPayToDate, pPayIntv,
                                "M", null);
                        this.setOwePrem(this.getOwePrem() + pPrem);
                    }

                    //判断免交标志，1为免交，欠交保费要去除免交的部分
                    if ((pFreeFlag != null) && pFreeFlag.equals("1"))
                    {
                        this.setOwePrem(this.getOwePrem() * (1 - pFreeRate));
                    }

                    this.setPayToDate(pPayToDate);
                    this.setZTPoint(tEndDate);
                    System.out.println("两年以后保全生效日大于等于宽限止期退保");
                }

                //5.保全生效日大于交至日期，并且小于宽限止期
                else if ((pEdorValiDate.compareTo(pPayToDate) > 0)
                         && (pEdorValiDate.compareTo(tLapseDate) < 0))
                {
                    //交至日期小于等于保全生效日，补交N期保费至大于保全生效日，并记入免交保费
                    while (pPayToDate.compareTo(pEdorValiDate) <= 0)
                    {
                        pPayToDate = this.transCalDate(pPayToDate, pPayIntv,
                                "M", null);
                        this.setOwePrem(this.getOwePrem() + pPrem);
                    }

                    //判断免交标志，1为免交，欠交保费要去除免交的部分
                    if ((pFreeFlag != null) && pFreeFlag.equals("1"))
                    {
                        this.setOwePrem(this.getOwePrem() * (1 - pFreeRate));
                    }

                    this.setPayToDate(pPayToDate);
                    this.setZTPoint(pEdorValiDate);
                    System.out.println("两年以后保全生效日大于等于交至日，并且小于宽限止期退保");
                }

                //6.保全生效日在交至日期当天
                else if (pEdorValiDate.compareTo(pPayToDate) == 0)
                {
                    this.setPayToDate(pPayToDate);
                    this.setZTPoint(pEdorValiDate);
                    this.setOwePrem(0);
                    System.out.println("两年以后保全生效日在交至日期当天退保");
                }

                //其他正常退保日
                else
                {
                    this.setPayToDate(pPayToDate);
                    this.setZTPoint(pEdorValiDate);
                    this.setOwePrem(0);
                }
            }

            //用退保点来重新计算退保年度
            tZTYear = PubFun.calInterval(pPolCValiDate, this.getZTPoint(), "Y");
            this.setZTYear(tZTYear);
            System.out.println("重新计算退保年度:" + mZTYear);

            //计算退保点距保单生效对应日的天数
            this.setZTDays(PubFun.calInterval(this.transCalDate(pPolCValiDate,
                    PubFun.calInterval(pPolCValiDate, this.getZTPoint(), "Y"),
                    "Y", null), this.getZTPoint(), "D"));

            // 设置交费间隔
            this.mPayIntv = pPayIntv;
        } catch (Exception ex)
        {
            CError.buildErr(this, "计算退保点失败");
        }
    }

    /**
     * 计算当年应交未交保费
     * @param pLPDutySchema
     */
    public void calUnfillPrem()
    {
        //按保额
        if (mCalType.equals("1"))
        {
            getUnfillPrem(mLPDutySchema.getPaytoDate(),
                          mLPDutySchema.getPayIntv(), mLPDutySchema.getPrem(),
                          mLPPolSchema.getCValiDate(), this.getZTYear());
        }

        //按保费
        else if (mCalType.equals("0"))
        {
            getUnfillPrem(mLPPremSchema.getPaytoDate(),
                          mLPPremSchema.getPayIntv(), mLPPremSchema.getPrem(),
                          mLPPolSchema.getCValiDate(), this.getZTYear());
        }
    }

    /**
     * 计算当年应交未交保费
     * @param pPayToDate
     * @param pPayIntv
     * @param pPrem
     * @param pPolCValiDate
     * @param pZTYear 退保年度
     */
    public void getUnfillPrem(String pPayToDate, int pPayIntv, double pPrem,
                              String pPolCValiDate, int pZTYear)
    {
        //得到下一保险单周年日
        FDate tFDate = new FDate();
        String nextYear = transCalDate(pPolCValiDate, (pZTYear + 1), "Y", null);

        //计算当年应交未交保费的月数
        int intvMonth = PubFun.calInterval(pPayToDate, nextYear, "M");

        //计算当年应交未交保费
        double unfillPrem = 0.0;

        if ((pPayIntv != 0) && (pPayIntv != -1))
        {
            unfillPrem = intvMonth / pPayIntv * pPrem;
        }

        this.setUnfillPrem(unfillPrem);
    }

    /**
     * 计算未经过保费
     * @param pLPDutySchema
     */
    public void calNetPrem()
    {
        //按保额
        if (mCalType.equals("1"))
        {
            getNetPrem(mLPDutySchema.getPaytoDate(),
                       mLPDutySchema.getPayIntv(), this.mRiskCode,
                       this.getZTPoint(), this.getZTYear());
        }

        //按保费
        else if (mCalType.equals("0"))
        {
            getNetPrem(mLPPremSchema.getPaytoDate(),
                       mLPPremSchema.getPayIntv(), this.mRiskCode,
                       this.getZTPoint(), this.getZTYear());
        }
    }

    /**
     * 根据LCPol的终交年龄年期标志PayEndYearFlag判断PayYears类型
     * A－年龄，M－月，D－日，Y－年
     * @param aPayYears int
     * @return int
     */
    public int getPayYears(int aPayYears) throws Exception
    {
        //TODO:如果缴费间隔不是年缴,需要考虑其他情况
        if (this.mPayEndYearFlag.trim().equals("Y"))
        {
            mPayYears = aPayYears;
            return aPayYears;
        } else
        {
            CError.buildErr(this, "得到的缴费间隔不是年缴");
            throw new Exception("得到的缴费间隔不是年缴");
        }
    }

    /**
     * 计算未经过保费，华夏公式为：1000*（1-费率）
     *      * @param pPayToDate
     * @param pPayIntv 交费间隔
     * @param pPrem
     */
    public void getNetPrem(String pPayToDate, int pPayIntv, String riskcode,
                           String pZTPoint, int pZTYear)
    {
        try
        {
            //没有预交的交至日期－退保生效日期＝未经过保费天数
            int tInterval = PubFun.calInterval(pZTPoint, pPayToDate, "D");
            System.out.println("未经过保费天数:" + tInterval);

            if (tInterval > 0)
            {
                //获取折算比例
                LMEdorNetDB tLMEdorNetDB = new LMEdorNetDB();
                int payYears = getPayYears(pPayIntv);
                String strSql =
                        "select * from LMEdorNet where  ((EndYear-StartYear)=" +
                        this.mZTYear + " " +
                        "or " + mZTYear + ">= 5 ) and PayShortYear<" + payYears +
                        " and PayLongYear>=" + payYears + "" +
                        " and riskcode='" + riskcode + "'";
                System.out.println("strSql = " + strSql);
                LMEdorNetSet tLMEdorNetSet = tLMEdorNetDB.executeQuery(strSql);

                if (tLMEdorNetSet.size() == 0)
                {
                    CError.buildErr(this, "获取未经过保费折算比例失败");
                    throw new Exception("获取未经过保费折算比例失败");
                }

                //不定期交暂不处理
                if (pPayIntv != -1)
                {
//                    double netPrem = ((double) (12 / pPayIntv) * tInterval) / this.Year * pPrem * tLMEdorNetSet.get(1)
//                                                                                                               .getNetRate();
                    //华夏纯保费计算公式
                    double netPrem = 1000 *
                                     (1 - tLMEdorNetSet.get(1).getNetRate());
                    this.setNetPrem(netPrem);
                    System.out.println("未经过保费:" + netPrem);
                }
            }
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 计算垫交保费（不同时段），现在未处理，因为退保入口控制了必须先还垫交和借款
     * @return
     */
    public void calTrayPrem()
    {
        //计算垫交保费金额
        this.setTrayPrem(0);

        //计算垫交保费利息
        this.setTrayPremInterest(0);
    }

    /**
     * 计算宽限期止期
     * @param aRiskCode
     * @param aPayToDate
     * @return
     */
    public static String CalLapseDate(String aRiskCode, String aPayToDate)
    {
        String aLapseDate = "";
        Date tLapseDate = null;
        FDate tFDate = new FDate();

        if ((aPayToDate == null) || aPayToDate.trim().equals(""))
        {
            System.out.println("没有交至日期，计算宽限止期失败");

            return aLapseDate;
        }

        //获取险种交费失效描述
        LMRiskPayDB tLMRiskPayDB = new LMRiskPayDB();
        tLMRiskPayDB.setRiskCode(aRiskCode);

        LMRiskPaySet tLMRiskPaySet = tLMRiskPayDB.query();

        if (tLMRiskPaySet.size() > 0)
        {
            if ((tLMRiskPaySet.get(1).getGracePeriodUnit() == null)
                || tLMRiskPaySet.get(1).getGracePeriodUnit().equals(""))
            {
                System.out.println("缺少险种交费失效描述!");

                int tLapseInterval = 60;
                aLapseDate = transCalDate(aPayToDate, tLapseInterval, "D", null);
            } else
            {
                int tLapseInterval = tLMRiskPaySet.get(1).getGracePeriod();
                aLapseDate = transCalDate(aPayToDate, tLapseInterval,
                                          tLMRiskPaySet.get(1).
                                          getGracePeriodUnit(), null);

                //按月进位，舍弃日精度
                if (tLMRiskPaySet.get(1).getGraceDateCalMode().equals("1"))
                {
//                    tLapseDate = tFDate.getDate(aLapseDate);
//                    tLapseDate.setMonth(tLapseDate.getMonth() + 1);
//                    tLapseDate.setDate(1);
//                    aLapseDate = tFDate.getString(tLapseDate);

                    GregorianCalendar tCalendar = new GregorianCalendar();
                    tCalendar.setTime(tFDate.getDate(aLapseDate));
                    //月份进位，舍弃日精度
                    tCalendar.set(tCalendar.get(Calendar.YEAR),
                                  tCalendar.get(Calendar.MONTH) + 1, 1);
                    aLapseDate = tFDate.getString(tCalendar.getTime());
                }

                //按年进位，只舍弃了日精度，为什么不舍弃月精度？
                else if (tLMRiskPaySet.get(1).getGraceDateCalMode().equals("2"))
                {
//                    tLapseDate = tFDate.getDate(aLapseDate);
//                    tLapseDate.setYear(tLapseDate.getYear() + 1);
//                    tLapseDate.setDate(1);
//                    aLapseDate = tFDate.getString(tLapseDate);

                    GregorianCalendar tCalendar = new GregorianCalendar();
                    tCalendar.setTime(tFDate.getDate(aLapseDate));
                    //年份进位，舍弃日精度，不舍弃月精度
                    tCalendar.set(tCalendar.get(Calendar.YEAR) + 1,
                                  tCalendar.get(Calendar.MONTH), 1);
                    aLapseDate = tFDate.getString(tCalendar.getTime());
                }
            }
        } else
        {
            int tLapseInterval = 60;
            aLapseDate = transCalDate(aPayToDate, tLapseInterval, "D", null);
        }

        return aLapseDate;
    }

    /**
     * 计算退保金，mTransFlag在构造函数中进行付值
     * @param pTransFlag LMEdorZT1中的计算代码类型CalCodeType
     * @return
     */
    public double getTBJ() throws Exception
    {
        try
        {
            //简单计算退保
            if (mTransFlag.equals("1"))
            {
                if (!this.calZTMoney1())
                {
                    throw new Exception("简单计算退保失败");
                }
            } else if (mTransFlag.equals("2"))
            {
                if (!calZTDiscount())
                {
                    throw new Exception("按现金价值退保失败");
                }
            }

            //插值退保算法，直接取程序算出的保单年度进行计算，两年内和两年后一样
            else if (mTransFlag.equals("3"))
            {
                if (!this.calZTMoney())
                {
                    throw new Exception("插值退保算法退保失败");
                }
            }

            //贴现退保算法，民生确认，两年内直接用程序算出的保单年度，两年后保单年度＝程序算出的保单年度＋1
            else if (mTransFlag.equals("4"))
            {
                if (!this.calZTMoney4())
                {
                    throw new Exception("贴现退保算法退保失败");
                }
            }
        } catch (Exception ex)
        {
            throw ex;
        }

        return this.getTBMoney();
    }

    /**
     * 通过现金价值表计算退费
     * @return boolean
     */
    private boolean calZTDiscount()
    {
        int payIntv = 0;
        String tCalCode = "";
        if (mCalType.equals("0"))
        {
            payIntv = mLPPremSchema.getPayIntv();
        } else if (mCalType.equals("1"))
        {
            payIntv = mLPDutySchema.getPayIntv();
        }
        if ((payIntv == 0) || (payIntv == -1))
        {
            tCalCode = mLMEdorZT1Schema.getOnePayCalCode();
        } else
        {
            tCalCode = mLMEdorZT1Schema.getCycPayCalCode();
        }

        initBqCalBase();
        mBqCalBase.setUpPolYears(getUpPolYears(mLPEdorItemSchema));
        mBqCalBase.setSumPrem(getActuMoney(mLPDutySchema));

        BqCalBL tBqCalBL = new BqCalBL(mLPEdorItemSchema,
                                       mBqCalBase,
                                       tCalCode,
                                       "");
        double tCashValue = tBqCalBL.calGetEndorse(tCalCode, mBqCalBase);
        if (tBqCalBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tBqCalBL.mErrors);
            CError.buildErr(this, "计算退保金失败!");
            return false;
        }
        System.out.println("现金价值，aCashValue: " + tCashValue);

        setTBMoney(tCashValue);

        return true;
    }

    /**
     * 得到本年保单年度
     * @param tLPEdorItemSchema LPEdorItemSchema
     */
    public String getUpPolYears(LPEdorItemSchema tLPEdorItemSchema)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo())
        {
            mErrors.addOneError("未查询到保单" + tLCContDB.getContNo() + "的信息");
            return null;
        }
        String tCValiDate = tLCContDB.getCValiDate();
        String tEdorValiDate = tLPEdorItemSchema.getEdorValiDate();
        if (tCValiDate == null || tEdorValiDate == null)
        {
            return null;
        }

        int compared = tEdorValiDate.compareTo(tCValiDate);
        if (compared < 0)
        {
            String errMsg = "保全生效日期" + tEdorValiDate
                            + "应晚于保单生效日期" + tCValiDate;
            mErrors.addOneError(errMsg);
            System.out.println(errMsg);
            return "1";
        } else if (compared == 0)
        {
            return "1";
        }

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tLPEdorItemSchema.getPolNo());
        if (!tLCPolDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "EdorCalZT";
            tError.functionName = "getUpPolYears";
            tError.errorMessage = "没有查询到传入的险种信息" + tLCPolDB.getPolNo();
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return "-1";
        }
        String polPayToDate = tLCPolDB.getPaytoDate();
        if (mCurPayToDateForesee != null)
        {
            polPayToDate = mCurPayToDateForesee;
        }
        
        String tHaveLong=new ExeSQL().getOneValue("select 1 from ldcode where codetype='longriskct' and code='"+tLCPolDB.getRiskCode()+"'");
        if(tHaveLong!=null&&(!tHaveLong.equals(""))){
        	if(tLCPolDB.getPaytoDate().equals(tLCPolDB.getPayEndDate())){
        		polPayToDate = tLCPolDB.getEndDate();
        	}
        }

        //这里要减去一天，这样才能算出保单年度
        String sql = "select case when (date('" + tEdorValiDate +
                     "') - 1 days) "
                     + " > (date('" + polPayToDate + "') -1 days ) "
                     + "   then (date('" + polPayToDate + "')-1 days)"
                     + "  else  (date('" + tEdorValiDate + "') - 1 days) end "
                     + "from lcpol where polno ='"
                     + tLPEdorItemSchema.getPolNo() + "' ";
        ExeSQL e = new ExeSQL();
        tEdorValiDate = e.getOneValue(sql);
        if (tEdorValiDate == null || tEdorValiDate.equals(""))
        {
            return null;
        }
        //因为这里计算出的保单年度是从0开始的，所以下面要加一
        int years = PubFun.calInterval(tCValiDate, tEdorValiDate, "Y");

        return String.valueOf(years + 1);
    }

    /**
     * 插值退保算法，直接取程序算出的保单年度进行计算，两年内和两年后一样
     * @return
     */
    private boolean calZTMoney()
    {
        //计算退保点，交至日期，欠交保费，退保年度
        calZTPoint();

        //计算当年应交未交保费
        calUnfillPrem();

        //计算未经过保费
        calNetPrem();

        //计算垫交保费
        calTrayPrem();

        double prem = 0;

        if (mCalType.equals("0"))
        {
            prem = mLPPremSchema.getPrem();
        } else if (mCalType.equals("1"))
        {
            prem = mLPDutySchema.getPrem();
        }

        return calZTMoney(mLMEdorZT1Schema,
                          mLPEdorItemSchema.getEdorValiDate(),
                          mLPPolSchema.getCValiDate(),
                          this.mPayToDate, this.mZTYear, this.mPayIntv,
                          this.mZTPoint, prem);
    }

    /**
     * 插值退保算法，直接取程序算出的保单年度进行计算
     * 参数暂时保留,待进一步修改
     * @return
     */
    private boolean calZTMoney(LMEdorZT1Schema pLMEdorZT1Schema,
                               String pEdorValiDate, String pPolCValiDate,
                               String pPayToDate,
                               int pZTYear, int pPayIntv, String pZTPoint,
                               double pPrem)
    {
        //得到年初年末的生存金
        mGetAliveBegin = this.getAliveBegin();
        mGetAliveEnd = this.getAliveEnd();

        //退保年限小于临界年限(计算纯保费)
        if (pZTYear < this.mPayYears)
        {
            System.out.println(mPayYears + "年内退保处理..........................");
//
//            int tZTYear = 0;
//            double tTotalMoney = 0;
//            double tGetMoney = 0;
//
//            int tZTMonth = PubFun.calInterval(pPolCValiDate, pPayToDate, "M");
//            System.out.println("两年内退保月份:" + tZTMonth);
//
//            //默认折算现金价值比例为1,处理两年以内的月缴情况
//            double tCashRate = 1;
//
//            if (pPayIntv == 0)
//            {
//                tCashRate = 1;
//            }
//            else
//            {
//                tCashRate = (double) tZTMonth / ((1 + pZTYear) * 12);
//            }
//
//            System.out.println("两年内退保年度:" + pZTYear);
//            System.out.println("两年内退保比例:" + tCashRate);
//
//            FDate tFDate = new FDate();
//
//            if (tFDate.getDate(pZTPoint).after(tFDate.getDate(pPayToDate)))
//            {
//                pZTYear--;
//            }
//
//            //退保计算
//            this.mCashValueBegin = this.getCashValue();
//            System.out.println("两年内退保金:" + (this.mCashValueBegin * tCashRate));
//            this.setTBMoney(this.mCashValueBegin * tCashRate);
            double lastYearCashValue = getCashValue(this.mZTDays - 1,
                    mLMEdorZT1Schema);
            double curYearCashValue = getCashValue(mZTDays, mLMEdorZT1Schema);
            double tTBMoney = lastYearCashValue * (1 - mZTDays / 365)
                              +
                              (curYearCashValue + mGetAliveEnd) *
                              (mZTDays / 365)
                              + mNetPrem * (1 - mZTDays / 365);
            System.out.println("sc华夏退保金:" + tTBMoney);
            setTBMoney(tTBMoney);

        }

        //退保年度超出临界年限（不计算纯保费）
        else
        {
            double lastYearCashValue = getCashValue(this.mZTDays - 1,
                    mLMEdorZT1Schema);
            double curYearCashValue = getCashValue(mZTDays, mLMEdorZT1Schema);
            double tTBMoney = lastYearCashValue * (1 - mZTDays / 365)
                              +
                              (curYearCashValue + mGetAliveEnd) *
                              (mZTDays / 365);

            System.out.println("sc华夏退保金:" + tTBMoney);
            setTBMoney(tTBMoney);

//            System.out.println(mPayYears+"年内退保处理..........................");
//
//            //得到年初，年末现金价值
//            this.mCashValueBegin = this.getCashValue();
//            System.out.println("年初现金价值:" + mCashValueBegin);
//
//            pZTYear = pZTYear + 1;
//            this.setZTYear(pZTYear);
//            System.out.println("两年后保单年度＝系统保单年度＋1，ZTYear : " + pZTYear);
//
//            this.mCashValueEnd = this.getCashValue();
//            System.out.println("年末现金价值:" + mCashValueEnd);
//
//            System.out.println("本年度退保天数:" + mZTDays);
//
//            //得到年初年末的生存金
//            mGetAliveBegin = this.getAliveBegin();
//            mGetAliveEnd = this.getAliveEnd();
//
//            //根据生存金计算方法确定退保金（同时处理未经过保费)
//            if ((this.mZTType != null) && mZTType.equals("1"))
//            {
//                if (mLMEdorZT1Schema.getLiveGetType().equals("0"))
//                {
//                    double tGetMoney = ((1
//                        - (Double.parseDouble(String.valueOf(mZTDays)) / Year)) * (mCashValueBegin
//                        - mGetAliveBegin)) + ((mZTDays * mCashValueEnd) / Year)
//                        + this.getNetPrem();
//                    double tdisMoney = ((1
//                        - (Double.parseDouble(String.valueOf(mZTDays)) / Year)) * (mCashValueBegin
//                        - mGetAliveBegin)) + ((mZTDays * mCashValueEnd) / Year);
//                    System.out.println("现金价值差值:" + tdisMoney);
//                    this.setTBMoney(tGetMoney);
//                }
//                else
//                {
//                    double tGetMoney = ((1
//                        - (Double.parseDouble(String.valueOf(mZTDays)) / Year)) * mCashValueBegin)
//                        + ((mZTDays * (mCashValueEnd + mGetAliveEnd)) / Year)
//                        + this.getNetPrem();
//                    double tdisMoney = ((1
//                        - (Double.parseDouble(String.valueOf(mZTDays)) / Year)) * (mCashValueBegin
//                        - mGetAliveBegin)) + ((mZTDays * mCashValueEnd) / Year);
//                    System.out.println("现金价值差值:" + tdisMoney);
//                    this.setTBMoney(tGetMoney);
//                }
//            }
//            else if ((mZTType != null) && mZTType.equals("2"))
//            {
//                if (mLMEdorZT1Schema.getLiveGetType().equals("0"))
//                {
//                    double tGetMoney = ((1
//                        - (Double.parseDouble(String.valueOf(mZTDays)) / Year)) * (mCashValueBegin
//                        - mGetAliveBegin)) + ((mZTDays * mCashValueEnd) / Year)
//                        + this.getNetPrem();
//                    this.setTBMoney(tGetMoney);
//                }
//                else
//                {
//                    double tGetMoney = ((1
//                        - (Double.parseDouble(String.valueOf(mZTDays)) / Year)) * mCashValueBegin)
//                        + ((mZTDays * (mCashValueEnd + mGetAliveEnd)) / Year)
//                        + this.getNetPrem();
//                    this.setTBMoney(tGetMoney);
//                }
//            }
//            else if ((mZTType != null) && mZTType.equals("3"))
//            {
//                if (mLMEdorZT1Schema.getLiveGetType().equals("0"))
//                {
//                    double tGetMoney = ((1
//                        - (Double.parseDouble(String.valueOf(mZTDays)) / Year)) * (mCashValueBegin
//                        - mGetAliveBegin)) + ((mZTDays * mCashValueEnd) / Year)
//                        + this.getNetPrem();
//                    this.setTBMoney(tGetMoney);
//                }
//                else
//                {
//                    double tGetMoney = ((1
//                        - (Double.parseDouble(String.valueOf(mZTDays)) / Year)) * mCashValueBegin)
//                        + ((mZTDays * (mCashValueEnd + mGetAliveEnd)) / Year)
//                        + this.getNetPrem();
//                    this.setTBMoney(tGetMoney);
//                }
//            }
//
//            //退保金要去除欠交保费
//            System.out.println("欠交保费:" + this.getOwePrem());
//            this.setTBMoney(this.getTBMoney() - this.getOwePrem());
//
//            System.out.println("垫交保费:" + this.getTrayPrem());
//            System.out.println("垫交利息:" + this.getTrayPremInterest());
//            this.setTBMoney(this.getTBMoney() - this.getTrayPrem()
//                - this.getTrayPremInterest());

            System.out.println("退保金:" + this.getTBMoney());
        }

        return true;
    }

    /**
     * 得到年初生存金
     * @return
     */
    private double getAliveBegin()
    {
        return getAliveBegin(mLPPolSchema.getCValiDate(),
                             mLPPolSchema.getPolNo(), this.mZTPoint);
    }

    /**
     * 得到年初生存金
     * @return
     */
    private double getAliveBegin(String pPolCValiDate, String pPolNo,
                                 String pZTPoint)
    {
        double aGetMoney = 0;

        if (mCalTimes > 0)
        {
            String tDate = this.transCalDate(pPolCValiDate,
                                             PubFun.calInterval(pPolCValiDate,
                    pZTPoint, "Y"), "Y", null);
            String tDate1 = this.transCalDate(tDate, 1, "Y", null);
            String tSql = "select sum(GetMoney) from ljaGetDraw where PolNo='"
                          + pPolNo + "' and LastGetToDate>='" + tDate
                          + "' and CurGetToDate<'" + tDate1 + "'";
            ExeSQL tExeSQL = new ExeSQL();
            String tReturn = tExeSQL.getOneValue(tSql);

            if (tReturn == null)
            {
                tReturn = "0";
            }

            if (mCalTimes == 0)
            {
                aGetMoney = Double.parseDouble(tReturn);
            }
        }

        if (aGetMoney < 0)
        {
            aGetMoney = -aGetMoney;
        }

        return aGetMoney;
    }

    /**
     * 得到年末生存金，不了解算法，Minim at 2004-3-9
     * @return
     */
    private double getAliveEnd()
    {
        double aGetMoney = 0;

        if (mCalTimes > 0)
        {
            VData tVData = new VData();
            LCGetSchema tLCGetSchema = new LCGetSchema();
            TransferData tTransferData = new TransferData();

            String tDate = this.transCalDate(mLPPremSchema.getPayStartDate(),
                                             PubFun.calInterval(mLPPremSchema.
                    getPayStartDate(),
                    this.getZTPoint(), "Y"), "Y", null);
            String tDate1 = this.transCalDate(tDate, 1, "Y", null);

            tTransferData.setNameAndValue("timeEnd", tDate1);
            tLCGetSchema.setPolNo(mLPPremSchema.getPolNo());

            tVData.addElement(tLCGetSchema);
            tVData.addElement(tTransferData);

            PayPlanBL tPayPlanBL = new PayPlanBL();

            if (tPayPlanBL.getEdorData(tVData))
            {
                if (tPayPlanBL.dealData(tVData))
                {
                    if ((tPayPlanBL.getLJSGetDrawSet() != null)
                        && (tPayPlanBL.getLJSGetDrawSet().size() > 0))
                    {
                        for (int i = 1;
                                     i <= tPayPlanBL.getLJSGetDrawSet().size();
                                     i++)
                        {
                            aGetMoney = aGetMoney
                                        + tPayPlanBL.getLJSGetDrawSet().get(i)
                                        .getGetMoney();
                        }
                    }
                }
            }
        }

        if (aGetMoney < 0)
        {
            aGetMoney = -aGetMoney;
        }

        return aGetMoney;
    }

    /**
     * 简单计算退保
     * @return
     */
    private boolean calZTMoney1()
    {
        String payToDate = "";
        int payIntv = 0;

        if (mCalType.equals("0"))
        {
            payToDate = mLPPremSchema.getPaytoDate();
            payIntv = mLPPremSchema.getPayIntv();
        } else if (mCalType.equals("1"))
        {
            payToDate = mLPDutySchema.getPaytoDate();
            payIntv = mLPDutySchema.getPayIntv();
        }

        return calZTMoney1(mLMEdorZT1Schema,
                           mLPEdorItemSchema.getEdorValiDate(),
                           mLPPolSchema.getCValiDate(),
                           payToDate, payIntv);
    }

    /**
     * 简单计算退保
     * @return
     */
    private boolean calZTMoney1(LMEdorZT1Schema pLMEdorZT1Schema,
                                String pEdorValiDate, String pPolCValiDate,
                                String pPayToDate,
                                int pPayIntv)
    {
        //取得趸交、期交算法编码，用约进法重新计算退保年度，设置退保点
        String tIntvType = "";
        String tCalCode = "";
        int tZTYear = 0;

        if ((pPayIntv == 0) || (pPayIntv == -1))
        {
            tCalCode = pLMEdorZT1Schema.getOnePayCalCode();
            if (tCalCode == null || tCalCode.equals(""))
            {
                mErrors.addOneError("没有得到趸缴退保计算公式。");
                System.out.println("EdorCalZT-calZTMoney1没有得到趸缴退保计算公式。");
                return false;
            }
            tIntvType = pLMEdorZT1Schema.getOnePayIntvType();
            //if(tIntvType == null || tIntvType.equals(""))
            //{
            //    mErrors.addOneError("没有得到趸缴时间间隔。");
            //    System.out.println("EdorCalZT-calZTMoney1没有得到趸缴时间间隔。");
            //    return false;
            //}

            //退保年度,使用舍弃法计算退保年度
            if (pLMEdorZT1Schema.getZTYearType().equals("0"))
            {
                tZTYear = PubFun.calInterval(pPolCValiDate, pEdorValiDate,
                                             tIntvType);
            }

            //约进法
            else if (pLMEdorZT1Schema.getZTYearType().equals("1"))
            {
                tZTYear = PubFun.calInterval2(pPolCValiDate, pEdorValiDate,
                                              tIntvType);
            }

            //退保点，趸交，不定期交为保全申请日
            this.setZTPoint(pEdorValiDate);
        } else
        {
            tCalCode = pLMEdorZT1Schema.getCycPayCalCode();
            tIntvType = pLMEdorZT1Schema.getCycPayIntvType();

            //退保年度,使用舍弃法计算退保年度
            if (pLMEdorZT1Schema.getZTYearType().equals("0"))
            {
                tZTYear = PubFun.calInterval(pPolCValiDate, pPayToDate,
                                             tIntvType);
            }

            //约进法
            else if (pLMEdorZT1Schema.getZTYearType().equals("1"))
            {
                tZTYear = PubFun.calInterval2(pPolCValiDate, pPayToDate,
                                              tIntvType);
            }

            //退保点，年交现在只有团体年金，按条款为最后一期交至日期，见精算确认文档
            this.setZTPoint(pPayToDate);
        }

        //初始化计算要素
        initBqCalBase();

        //退保年度
        mBqCalBase.setInterval(tZTYear);
        mBqCalBase.setUpPolYears(getUpPolYears(mLPEdorItemSchema));

        //按照比例法计算退保
        BqCalBL tBqCalBL = new BqCalBL(mLPEdorItemSchema, mBqCalBase, tCalCode,
                                       "");
        double tCashValue = tBqCalBL.calGetEndorse(tCalCode, mBqCalBase);
        if (tBqCalBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tBqCalBL.mErrors);
            CError.buildErr(this, "计算退保金失败!");
            return false;
        }
        System.out.println("End 计算，按比例退保算法，aCashValue: " + tCashValue);

        this.setTBMoney(tCashValue);

        return true;
    }

    /**
     * 贴现退保算法，民生确认，两年内直接用程序算出的保单年度，两年后保单年度＝程序算出的保单年度＋1
     * @return
     */
    private boolean calZTMoney4()
    {
        //计算退保点，交至日期，欠交保费，退保年度
        calZTPoint();

        //计算当年应交未交保费
        calUnfillPrem();

        //计算未经过保费
        calNetPrem();

        //计算垫交保费
        calTrayPrem();

        String payToDate = "";
        int payIntv = 0;

        if (mCalType.equals("0"))
        {
            payToDate = mLPPremSchema.getPaytoDate();
            payIntv = mLPPremSchema.getPayIntv();
        } else if (mCalType.equals("1"))
        {
            payToDate = mLPDutySchema.getPaytoDate();
            payIntv = mLPDutySchema.getPayIntv();
        }

        return calZTMoney4(mLPPolSchema.getCValiDate(), payToDate, mZTYear,
                           payIntv, mZTPoint);
    }

    /**
     * 贴现退保算法，民生确认，两年内直接用程序算出的保单年度，两年后保单年度＝程序算出的保单年度＋1
     * @return
     */
    private boolean calZTMoney4(String pPolCValiDate, String pPayToDate,
                                int pZTYear, int pPayIntv, String pZTPoint)
    {
        //两年内退保
        if (pZTYear < this.CriticalYearInterval)
        {
            System.out.println("两年内退保处理，直接取系统保单年度ZTYear: " + pZTYear);
            System.out.println("交费间隔：" + pPayIntv);

            //年交，趸交，期交一样处理
            if ((pPayIntv == 12) || (pPayIntv == 0) || (pPayIntv == -1))
            {
                double cashValue = getCashValue();
                setTBMoney(cashValue);
            }

            //其他交费方式如季交...
            else
            {
                //已交费的次数
                int payedTimes = PubFun.calInterval(pPolCValiDate, pPayToDate,
                        "M") / pPayIntv;

                //应交费的所有次数，系统保单年度＝实际保单年度－1，即第一年为0
                int allPayTimes = ((pZTYear + 1) * 12) / pPayIntv;

                double TBMoney = (getCashValue() * payedTimes) / allPayTimes;
                setTBMoney(TBMoney);
            }

            //两年后退保
        } else
        {
            System.out.println("两年后退保处理..........................");

            //得到贴现用利率=欠缴保计算利用率=精算规定为0.025，定义在LDSysVar中
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("ZTI2");

            if (!tLDSysVarDB.getInfo())
            {
                CError.buildErr(this, "获取贴现用利率失败");

                return false;
            }

            double I2 = Double.parseDouble(tLDSysVarDB.getSysVarValue());

            pZTYear = pZTYear + 1;
            this.setZTYear(pZTYear);
            System.out.println("两年后保单年度＝系统保单年度＋1，ZTYear : " + pZTYear);

            //得到下一保险单周年日
            FDate tFDate = new FDate();
            String nextYear = transCalDate(pPolCValiDate, pZTYear, "Y", null);

            //计算下一保单周年日 - 退保点生效日期
            int remainDays = PubFun.calInterval(pZTPoint, nextYear, "D");
            System.out.println("(下一保单周年日 - 退保点生效日期)remainDays: " + remainDays);

            //计算现金价值
            double discount = (1 - ((I2 * remainDays) / 365)) * getCashValue();
            System.out.println("(现金价值)discount: " + discount);

            //计算退保金
            double TBMoney = (discount - this.getUnfillPrem()
                              + this.getNetPrem()) - getOwePrem();
            setTBMoney((double) TBMoney);
        }

        return true;
    }

    /**
     * 准备计算要素，按保费
     * @param pLPDutySchema
     */
    private void initBqCalBase(LPPremSchema pLPPremSchema)
    {
        double prem = pLPPremSchema.getPrem();
        double actuMoney = getActuMoney(pLPPremSchema);
        if ((actuMoney > prem)||("252001".equals(pLPPremSchema.getDutyCode()))||("253001".equals(pLPPremSchema.getDutyCode())))
        { //如果实交保费大于期交则取期交  //modify by xp 090904 如果是少儿险也直接取期交保费
            actuMoney = prem;
        }
        mBqCalBase.setPayIntv(pLPPremSchema.getPayIntv());
        //mBqCalBase.setPrem(pLPPremSchema.getPrem());
        mBqCalBase.setPrem(actuMoney);
        mBqCalBase.setSumPrem(pLPPremSchema.getSumPrem());
        mBqCalBase.setGetMoney(pLPPremSchema.getStandPrem());

    }

    /**
     * 得到实交保费
     * @param tLPPremSchema LPPremSchema
     * @return String
     */
    private double getActuMoney(LPPremSchema tLPPremSchema)
    {
        String sql = "  select sum(sumActuPayMoney) "
                     + "from LJAPayPerson "
                     + "where CurPayToDate = '"
                     + tLPPremSchema.getPaytoDate() + "' "
                     + "  and polNo = '" + tLPPremSchema.getPolNo() + "' "
                     + "  and dutyCode = '" + tLPPremSchema.getDutyCode() +
                     "' "
                     + "  and payPlanCode = '"
                     + tLPPremSchema.getPayPlanCode() + "' "
                     + "  and PayType = 'ZC' ";
        ExeSQL tExeSQL = new ExeSQL();
        String sumActuPayMoney = tExeSQL.getOneValue(sql);

        if (sumActuPayMoney.equals("") || sumActuPayMoney.equals("null"))
        {
            //为了容错，从LJASGetEndorse中的得到增人实交保费，
            //责任实交 = 总实交 * 缴费项期交保费 / 险种期交保费
            double getMoney = getMoneyFromLJAGetEndorse(tLPPremSchema.getPolNo(),
                    BQ.EDORTYPE_NI);
            sql = "  select * "
                  + "from LCPrem "
                  + "where polNo = '" + tLPPremSchema.getPolNo() + "' ";
            LCPremDB tLCPremDB = new LCPremDB();
            LCPremSet set = tLCPremDB.executeQuery(sql);
            if (set.size() == 0)
            {
                return 0;
            }
            double sumPrem = 0;
            for (int i = 1; i <= set.size(); i++)
            {
                sumPrem += set.get(i).getPrem();
            }
            if (sumPrem == 0)
            {
                return 0;
            }
            sumActuPayMoney = String.valueOf(
                    getMoney * tLPPremSchema.getPrem() / sumPrem);
        }

        return sumActuPayMoney.equals("") ? 0
                : Double.parseDouble(sumActuPayMoney);
    }

    /**
     * 准备计算要素，按保额
     * @param pLPDutySchema
     */
    private void initBqCalBase(LPDutySchema pLPDutySchema)
    {
        double prem = pLPDutySchema.getPrem();
        double actuMoney = getActuMoney(pLPDutySchema);
        if ((actuMoney > prem))
        { //如果实交保费大于期交则取期交 
             actuMoney = prem;
        }
        mBqCalBase.setPayIntv(pLPDutySchema.getPayIntv());
        mBqCalBase.setPayEndYear(pLPDutySchema.getPayEndYear());
        mBqCalBase.setYears(pLPDutySchema.getYears());
        mBqCalBase.setPayEndYearFlag(pLPDutySchema.getPayEndYearFlag());
        mBqCalBase.setGetStartFlag(pLPDutySchema.getGetYearFlag());
        //mBqCalBase.setPrem(pLPDutySchema.getPrem());
        mBqCalBase.setPrem(actuMoney);
        mBqCalBase.setSumPrem(pLPDutySchema.getSumPrem());
        mBqCalBase.setGet(pLPDutySchema.getAmnt());
        mBqCalBase.setGetMoney(pLPDutySchema.getStandPrem());
        mBqCalBase.setInsuYear(pLPDutySchema.getInsuYear());
        mBqCalBase.setInsuYearFlag(pLPDutySchema.getInsuYearFlag());
        mBqCalBase.setGetStartYear(pLPDutySchema.getGetYear());
    }

    /**
     * 得到实交保费
     * @param tLPPremSchema LPPremSchema
     * @return String
     */
    private double getActuMoney(LPDutySchema tLPDutySchema)
    {
        String sql = "  select sum(sumActuPayMoney) "
                     + "from LJAPayPerson "
                     + "where CurPayToDate = '"
                     + tLPDutySchema.getPaytoDate() + "' "
                     + "  and dutyCode= '" + tLPDutySchema.getDutyCode() + "' "
                     + "  and polNo = '" + tLPDutySchema.getPolNo() + "' "
                     + "  and PayType = 'ZC' ";
        ExeSQL tExeSQL = new ExeSQL();
        String sumActuPayMoney = tExeSQL.getOneValue(sql);
        if (sumActuPayMoney.equals("") || sumActuPayMoney.equals("null"))
        {
            //为了容错，从LJASGetEndorse中的得到增人实交保费，
            //责任实交 = 总实交 * 责任期交保费 / 险种期交保费
            double getMoney = getMoneyFromLJAGetEndorse(tLPDutySchema.getPolNo(),
                    BQ.EDORTYPE_NI);
            sql = "  select * "
                  + "from LCDuty "
                  + "where polNo = '" + tLPDutySchema.getPolNo() + "' ";
            LCDutyDB tLCDutyDB = new LCDutyDB();
            LCDutySet set = tLCDutyDB.executeQuery(sql);
            if (set.size() == 0)
            {
                return 0;
            }
            double sumDutyPrem = 0;
            for (int i = 1; i <= set.size(); i++)
            {
                sumDutyPrem += set.get(i).getPrem();
            }
            if (sumDutyPrem == 0)
            {
                return 0;
            }
            sumActuPayMoney = String.valueOf(
                    getMoney * tLPDutySchema.getPrem() / sumDutyPrem);
        }
        return (sumActuPayMoney.equals("") || sumActuPayMoney.equals("null"))
                ? 0 : Double.parseDouble(sumActuPayMoney);
    }

    /**
     * 为了容错，从LJASGetEndorse中的得到实交保费
     * @param polNo String
     * @return String
     */
    private double getMoneyFromLJAGetEndorse(String polNo, String edorType)
    {
        String sql = "  select endorsementNo, polNo, sum(getMoney) "
                     + "from LJAGetEndorse "
                     + "where polNo = '" + polNo + "' "
                     + "   and feeOperationType = '" + edorType + "' "
                     + "group by endorsementNo, polNo "
                     + "union all "
                     + "select endorsementNo, polNo, sum(getMoney) "
                     + "from LJSGetEndorse "
                     + "where polNo = '" + polNo + "' "
                     + "   and feeOperationType = '" + edorType + "' "
                     + "group by endorsementNo, polNo "
                     + "order by endorsementNo ";
        System.out.println(sql);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
//            sql = "select insuredName, riskCode from LCPol where polNo = '" + polNo + "' ";
//            tSSRS = tExeSQL.execSQL(sql);
//            mErrors.addOneError("没有查询到" + tSSRS.GetText(1, 1) + "的"
//                                + tSSRS.GetText(1, 2) + "险种的缴费信息");
            return 0;
        } else
        {
            return Double.parseDouble(tSSRS.GetText(1, 3));
        }
    }

    /**
     * 准备计算要素
     * @return
     */
    private void initBqCalBase()
    {
        mBqCalBase = new BqCalBase();

        //退保年度
        mBqCalBase.setInterval(this.getZTYear());

        setInsuredInfo();
        mBqCalBase.setGrpContNo(mLPPolSchema.getGrpContNo());
        mBqCalBase.setCValiDate(mLPPolSchema.getCValiDate());
        mBqCalBase.setPolNo(mLPPolSchema.getPolNo());
        mBqCalBase.setMult(mLPPolSchema.getMult());

        mBqCalBase.setEdorNo(mLPEdorItemSchema.getEdorNo());
        mBqCalBase.setEdorType(mLPEdorItemSchema.getEdorType());
        mBqCalBase.setEdorValiDate(mLPEdorItemSchema.getEdorValiDate());
        mBqCalBase.setInsuredAppAge(mLPPolSchema.getInsuredAppAge());

        mBqCalBase.setCInValiDate(this.getCInValiDate());
        //qulq 061129 add for 现金价值计算

        mBqCalBase.setGet(mLPDutySchema.getAmnt());
        mBqCalBase.setInsuYear(mLPDutySchema.getInsuYear());
        mBqCalBase.setPayEndYear(mLPDutySchema.getPayEndYear());

        mBqCalBase.setStandByFlag1(mLPDutySchema.getStandbyFlag1());
        System.out.println("StandByFlag1= " + mBqCalBase.getStandByFlag1());

        String polPayToDate = "";
        if (mCalType.equals("0"))
        {
            initBqCalBase(mLPPremSchema);
            polPayToDate = mLPPremSchema.getPaytoDate();
        } else if (mCalType.equals("1"))
        {
            initBqCalBase(mLPDutySchema);
            polPayToDate = mLPDutySchema.getPaytoDate();
        }
        setPayToDateInfo(polPayToDate);
        setCalType();
    }

    /**
     * 得到投保年龄，性别
     */
    private void setInsuredInfo()
    {
        LPInsuredBL tLPInsuredBL = new LPInsuredBL();
        LPInsuredSet tLPInsuredSet;
        if (this.mGetDataFromC)
        {
            String sql = "  select '" + mLPPolSchema.getEdorNo()
                         + "' EdorNo, '"
                         + mLPPolSchema.getEdorType()
                         + "' EdorType, a.* "
                         + "from LCInsured a "
                         + "where contNo = '" + mLPPolSchema.getContNo()
                         + "'  and insuredNo = '" + mLPPolSchema.getInsuredNo()
                         + "' ";
            LPInsuredDB tLPInsuredDB = new LPInsuredDB();
            tLPInsuredSet = tLPInsuredDB.executeQuery(sql);
            if (tLPInsuredSet.size() == 0)
            {
                CError.buildErr(this, "获取保单险种数据失败");
            }
        } else
        {
            tLPInsuredSet = tLPInsuredBL.queryAllLPInsured(mLPEdorItemSchema);
        }
        for (int j = 1; j <= tLPInsuredSet.size(); j++)
        {
            if (tLPInsuredSet.get(j).getInsuredNo()
                .equals(mLPPolSchema.getInsuredNo()))
            {
                mBqCalBase.setAppAge(PubFun.calInterval(
                        tLPInsuredSet.get(j).getBirthday(),
                        mLPPolSchema.getCValiDate(), "Y"));
                mBqCalBase.setSex(tLPInsuredSet.get(j).getSex());
                //qulq 被保人年龄
                mBqCalBase.setInsuredApp(PubFun.calInterval(
                        tLPInsuredSet.get(j).getBirthday(),
                        mLPPolSchema.getCValiDate(), "Y"));
                return;
            }
        }
    }

    /**
     * 查询保单的终止日期
     * @return String
     */
    private String getCInValiDate()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo())
        {
            return "";
        }
        return tLCContDB.getCInValiDate();
    }

    /**
     * 初始化计算要素中的curpaytodate和lastpaytodate
     */
    private void setPayToDateInfo(String polPayToDate)
    {
        String lastPayToDate = "";
        String curPayToDate = "";
        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        String sql = "  select * "
                     + "from LJAPayPerson "
                     + "where polNo = '" + mLPPolSchema.getPolNo() + "' "
                     + "    and CurPayToDate = '" + polPayToDate + "' ";
        LJAPayPersonSet set = tLJAPayPersonDB.executeQuery(sql);
        System.out.println(sql);
        if (set.size() == 0)
        {
            //为了容增人没往实收表存数据的错
            lastPayToDate = mLPPolSchema.getCValiDate();
            curPayToDate = mLPPolSchema.getPaytoDate();
        } else
        {
            lastPayToDate = set.get(1).getLastPayToDate();
            curPayToDate = set.get(1).getCurPayToDate();
        }

        mBqCalBase.setLastPayToDate(lastPayToDate);
        if (mCurPayToDateForesee != null)
        {
            mBqCalBase.setPayToDate(mCurPayToDateForesee);
        } else
        {
            mBqCalBase.setPayToDate(curPayToDate);
        }
//        checkEdorValiDate();
    }

    /**
     * 校验保全生效日期的合法性
     */
    private void checkEdorValiDate()
    {
        if (mBqCalBase.getEdorValiDate()
            .compareTo(mBqCalBase.getLastPayToDate()) < 0)
        {
            //mErrors.addOneError("保全生效日期不能早于本期应缴日期");
            //某些险种可以在生效日之前退保，查询LDCode1的描述进行控制
            LDCode1DB tLDCode1DB = new LDCode1DB();
            tLDCode1DB.setCodeType("BeforeCvalidate");
            tLDCode1DB.setCode(mLPPolSchema.getRiskCode());
            if (tLDCode1DB.query().size() == 0)
            {
                //不能早于生效日退保
                System.out.println(mLPPolSchema.getPolNo()
                                   + "保全生效日期" + mBqCalBase.getEdorValiDate()
                                   + "早于本期应交日期"
                                   + mBqCalBase.getLastPayToDate()
                                   + "，自动调整为应交日期");
                mBqCalBase.setEdorValiDate(mBqCalBase.getLastPayToDate());
            }
        }
        if (mBqCalBase.getEdorValiDate()
            .compareTo(mBqCalBase.getPayToDate()) > 0)
        {
            System.out.println(mBqCalBase.getEdorValiDate());
            System.out.println(mBqCalBase.getPayToDate());
            mErrors.addOneError("保全生效日期不能晚于本期缴至日期");
        }
    }

    /**
     * 初始化计算要素中的调用算法类型
     */
    private void setCalType()
    {
        //减人算法分为“条款规定算法”：1，和“月比例算法”：2
        if (mLPPolSchema.getEdorType().equals(BQ.EDORTYPE_ZT))
        {
            EdorItemSpecialData mSpecialData =
                    new EdorItemSpecialData(mLPPolSchema.getEdorNo(),
                                            mLPPolSchema.getEdorType());
            if (!mSpecialData.query())
            {
                mErrors.addOneError("请选择算法！");
                return;
            }
            String calTime = mSpecialData.getEdorValue("CalTime");
            String feeRate = mSpecialData.getEdorValue("FeeRate");
            mBqCalBase.setCalTime(calTime);
            mBqCalBase.setFeeRate(feeRate);
        } else if (mLPPolSchema.getEdorType().equals(BQ.EDORTYPE_CT))
        {
            mBqCalBase.setCalType("1");
            mBqCalBase.setCalTime("");
            mBqCalBase.setFeeRate("0");
        }
    }

    /**
     * 构建批改补退费表数据
     * @return
     */
    public LJSGetEndorseSet getLJSGetEndorseSet()
    {
        BqCalBL tBqCalBL = new BqCalBL(mBqCalBase, "", "");
        LJSGetEndorseSet tLJSGetEndorseSet = new LJSGetEndorseSet();
        LJSGetEndorseSchema tLJSGetEndorseSchema = null;
        LJSGetEndorseSchema tlxLJSGetEndorseSchema = null;
        LJSGetEndorseSchema tMFLJSGetEndorseSchema = null;
        GlobalInput aGlobalInput = new GlobalInput();
        //退费处理
        if (this.getTFMoney() != Double.MIN_VALUE)
        {
            tLJSGetEndorseSchema = tBqCalBL.initLJSGetEndorse(mLPEdorItemSchema,
                    mLPPolSchema,
                    null, "TF", Arith.round( -this.getTFMoney(), 2),
                    aGlobalInput);
            this.setTFMoney(Double.MIN_VALUE);
        }

        //退保处理
        else if (this.getTBMoney() != Double.MIN_VALUE)
        {
            tLJSGetEndorseSchema = tBqCalBL.initLJSGetEndorse(mLPEdorItemSchema,
                    mLPPolSchema,
                    null, "TB", Arith.round( -this.getTBMoney(), 2),
                    aGlobalInput);
            this.setTBMoney(Double.MIN_VALUE);
        }

        //退帐户
        else if (this.getAccTBMoney() != Double.MIN_VALUE)
        {
            tLJSGetEndorseSchema = tBqCalBL.initLJSGetEndorse(mLPEdorItemSchema,
                    mLPPolSchema,
                    null, "TB", Arith.round( -mLPInsureAccSchema.getInsuAccBala(), 2),
                    aGlobalInput);
            double tAccTBMoney = this.getAccTBMoney();
            this.setAccTBMoney(Double.MIN_VALUE);
            if (mLPEdorItemSchema.getEdorType().equals("CT"))
            {
                if (this.getLXMoney() != Double.MIN_VALUE)
                {
                    tlxLJSGetEndorseSchema = tBqCalBL.initLJSGetEndorse(
                            mLPEdorItemSchema, mLPPolSchema,
                            null, "LX", Arith.round( -this.getLXMoney(), 2),
                            aGlobalInput);
                    this.setLXMoney(Double.MIN_VALUE);
                }
                double tMF = mAccMoney-tAccTBMoney+mLXFMoney-mLXEMoney;
                if (tMF != Double.MIN_VALUE){
                    tMFLJSGetEndorseSchema = tBqCalBL.initLJSGetEndorse(
                            mLPEdorItemSchema, mLPPolSchema,
                            null, "MF", Arith.round( tMF, 2),
                            aGlobalInput);
                }
            }
        }

        if (tLJSGetEndorseSchema != null)
        {
            //退费
            if (mCalType.equals("0"))
            {
                tLJSGetEndorseSchema.setDutyCode(mLPPremSchema.getDutyCode());
                tLJSGetEndorseSchema.setPayPlanCode(mLPPremSchema
                        .getPayPlanCode());
            }

            //退保
            else if (mCalType.equals("1"))
            {
                tLJSGetEndorseSchema.setDutyCode(mLPDutySchema.getDutyCode());
            }

            //退帐户
            else if (mCalType.equals("2"))
            {
                if (mLPEdorItemSchema.getEdorType().equals("CT"))
                {
                    tLJSGetEndorseSchema.setPayPlanCode(mLPInsureAccSchema.
                            getInsuAccNo());
                    if (tlxLJSGetEndorseSchema != null)
                    {
                        tlxLJSGetEndorseSchema.setPayPlanCode(
                                mLPInsureAccSchema.getInsuAccNo());
                    }
                    if (tMFLJSGetEndorseSchema != null){
                        tMFLJSGetEndorseSchema.setPayPlanCode(mLPInsureAccSchema.getInsuAccNo());
                    }
                }
            }

            if (mLPPolSchema.getContType() != null
                && mLPPolSchema.getContType().equals("2"))
            {
                tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
                if (tlxLJSGetEndorseSchema != null)
                {
                    tlxLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
                }
                if (tMFLJSGetEndorseSchema != null){
                    tMFLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
                }
            } else
            {
                tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_P);
            }
            tLJSGetEndorseSchema.setManageCom(mLPPolSchema.getManageCom());
            if (tlxLJSGetEndorseSchema != null)
            {
                tlxLJSGetEndorseSchema.setManageCom(mLPPolSchema.getManageCom());
                mLXLJSGetEndorseSet.add(tlxLJSGetEndorseSchema);
            }
            if (tMFLJSGetEndorseSchema !=null){
                tMFLJSGetEndorseSchema.setManageCom(mLPPolSchema.getManageCom());
                mMFLJSGetEndorseSet.add(tMFLJSGetEndorseSchema);
            }
            tLJSGetEndorseSet.add(tLJSGetEndorseSchema);

        }

        return tLJSGetEndorseSet;
    }

    /**
     * 计算退保年度的现金价值
     * @return
     */
    private double getCashValue()
    {
        return getCashValue(this.getZTYear(), mLMEdorZT1Schema);
    }

    /**
     * 计算退保年度的现金价值
     * @return
     */
    private double getCashValue(int pZTYear, LMEdorZT1Schema pLMEdorZT1Schema)
    {
        double tCashValue = 0;

        //准备计算要素
        initBqCalBase();
        mBqCalBase.setInterval(pZTYear);

        String aIntvType = pLMEdorZT1Schema.getCycPayIntvType();
        String tCalCode = pLMEdorZT1Schema.getCashValueCode();

        if ((tCalCode == null) || tCalCode.trim().equals(""))
        {
            CError.buildErr(this, "缺少退保现金价值险种描述编码");

            return tCashValue;
        }

        BqCalBL tBqCalBL = new BqCalBL(mBqCalBase, tCalCode, "");
        tCashValue = tBqCalBL.calGetEndorse(tCalCode, mBqCalBase);
        if (tBqCalBL.mErrors.needDealError())
        {
            CError.buildErr(this, "计算退保金失败!");
            return tCashValue;
        }

        System.out.println("CashValue: " + tCashValue);

        return tCashValue;
    }

    /**
     * 算费后调用方可得到正确值
     * @return String
     */
    public String getLastPayToDate()
    {
        return mBqCalBase.getLastPayToDate();
    }

    /**
     * 算费后调用方可得到正确值
     * @return String
     */
    public String getCurPayToDate()
    {
        return mBqCalBase.getPayToDate();
    }

    /**
     * 转换计算日期类型
     * @param baseDate
     * @param interval
     * @param unit
     * @param compareDate
     * @return
     */
    private static String transCalDate(String baseDate, int interval,
                                       String unit, String compareDate)
    {
        FDate tFDate = new FDate();
        Date tEndDate = PubFun.calDate(tFDate.getDate(baseDate), interval,
                                       unit, tFDate.getDate(compareDate));

        return tFDate.getString(tEndDate);
    }

    /**
     * 判断失效的新方法
     * @param aRiskCode   保单险种
     * @param aPayToDate  交至日期
     * @param aCurDate    当前日期
     * @param aPayIntv    交费间隔
     * @param aEndDate    保单终止日期
     * @return
     */
    public static boolean JudgeLapse(String aRiskCode, String aPayToDate,
                                     String aCurDate, String aPayIntv,
                                     String aEndDate)
    {
        //如果交费方式是不定期交或者是趸交:判断保单终止日期是否小于当天
        if (aPayIntv.equals("-1") || aPayIntv.equals("0"))
        {
            FDate tFDate = new FDate();

            if (tFDate.getDate(aEndDate).before(tFDate.getDate(aCurDate)))
            {
                return true;
            }

            return false;
        }

        //否则判断交至日期是否小于当天
        else
        {
            String tLapseDate = CalLapseDate(aRiskCode, aPayToDate);
            FDate tFDate = new FDate();

            if (tFDate.getDate(tLapseDate).before(tFDate.getDate(aCurDate)))
            {
                return true;
            }

            return false;
        }
    }

    /**
     * 计算单个保单的生存退保数据
     */
    public void calZTData(LPEdorItemSchema pLPEdorItemSchema) throws Exception
    {
        try
        {
            queryLPPol(pLPEdorItemSchema);
            //根据具体每个保单的责任项进行退保
            getPolZTData(pLPEdorItemSchema);
            //根据具体每个保单的帐户进行退保
            getPolAccZTData(pLPEdorItemSchema);
        } catch (Exception ex)
        {
            throw ex;
        }

        //发生理赔的险种退费为0
        if (dealClaim())
        {
        }
    }

    private void queryLPPol(LPEdorItemSchema pLPEdorItemSchema) throws
            Exception
    {
        if (mLPPolSchema != null && mLPPolSchema.getEdorNo() != null
            && mLPPolSchema.getEdorType() != null)
        {
            return;
        }

        if (this.mGetDataFromC)
        {
            String sql = "  select '" + pLPEdorItemSchema.getEdorNo() + "', '"
                         + pLPEdorItemSchema.getEdorType() + "', a.* "
                         + "from LCPol a "
                         + "where polNo = '" + pLPEdorItemSchema.getPolNo()
                         + "' ";
            LPPolDB tLPPolDB = new LPPolDB();
            LPPolSet set = tLPPolDB.executeQuery(sql);
            if (set.size() == 0)
            {
                CError.buildErr(this, "获取保单险种数据失败");
                throw new Exception("获取保单险种数据失败");
            }
            this.mLPPolSchema = set.get(1).getSchema();
        } else
        {
            LPPolBL tLPPolBL = new LPPolBL();
            if (!tLPPolBL.queryLPPol(pLPEdorItemSchema))
            {
                CError.buildErr(this, "获取保单险种数据失败");
                throw new Exception("获取保单险种数据失败");
            }
            this.mLPPolSchema = tLPPolBL.getSchema();
        }
    }

    /**
     * 发生理赔的险种退费为0
     * @param tLJSGetEndorseSet LJSGetEndorseSet
     * @return boolean：发生过理赔：true，否则：false
     */
    private boolean dealClaim()
    {
        //若险种发生过理赔，则退费为0
        GCheckClaimBL check = new GCheckClaimBL();
        for (int i = 1; i <= mLJSGetEndorseSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(mLJSGetEndorseSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                mErrors.addOneError("没有查询到险种"
                                    + tLMRiskAppDB.getRiskCode() + "的描述。");
                return false;
            }
            if (tLMRiskAppDB.getRiskType3().equals(BQ.RISKTYPE3_SPECIAL))
            {
                return false;
            }

            //检查是否发生过理赔，发生理赔返回true
            if (check.checkClaimed(mLJSGetEndorseSet.get(i)))
            {
                mLJSGetEndorseSet.get(i).setGetMoney(0);
                System.out.println(mLJSGetEndorseSet.get(i).getPolNo());
                //qulq 061128 添加错误LOG  for 退保试算
                String seriNo = PubFun1.CreateMaxNo("ERRORLOGNO", 20);
                LGErrorLogSchema tLGErrorLogSchema = new LGErrorLogSchema();
                tLGErrorLogSchema.setSerialNo(seriNo);
                tLGErrorLogSchema.setErrorType("0001");
                tLGErrorLogSchema.setErrorTypeSub("02");
                tLGErrorLogSchema.setDescribe("保全退保算费，发生理赔");
                mLGErrorLogSet.add(tLGErrorLogSchema);

//                return true;
            }
            if (check.mErrors.needDealError())
            {
                mErrors.copyAllErrors(check.mErrors);
                return false;
            }
        }
        return false;
    }

    /**
     * 获取单个保单的退保数据
     * @param pLPEdorItemSchema
     * @param pLPPolSchema
     * @return
     */
    public double getPolZTData(LPEdorItemSchema pLPEdorItemSchema) throws
            Exception
    {
        try
        {
            //查询出该保单下所有的责任信息
            LPDutyBL tLPDutyBL = new LPDutyBL();
            LPDutySet tLPDutySet = null;
            if (mGetDataFromC)
            {
                String sql = "select '" + pLPEdorItemSchema.getEdorNo()
                             + "' EdorNo, '" + pLPEdorItemSchema.getEdorType()
                             + "' EdorType, a.* "
                             + "from LCDuty a "
                             + "where polNo = '"
                             + pLPEdorItemSchema.getPolNo() + "' ";
                LPDutyDB tLPDutyDB = new LPDutyDB();
                tLPDutySet = tLPDutyDB.executeQuery(sql);
            } else
            {
                tLPDutySet = tLPDutyBL.queryAllLPDuty(pLPEdorItemSchema);
            }
            //循环保单对应的所有责任项，进行以责任为基础的退保处理
            double totalMoney = 0;
            for (int i = 0; i < tLPDutySet.size(); i++)
            {
                totalMoney = totalMoney
                             +
                             getDutyZTData(pLPEdorItemSchema,
                                           tLPDutySet.get(i + 1));
            }

            return totalMoney;
        } catch (Exception ex)
        {
            throw ex;
        }
    }

    /**
     * 获取单个责任项的退保数据
     * @param pLPEdorItemSchema
     * @param pLPDutySchema
     * @return
     */
    public double getDutyZTData(LPEdorItemSchema pLPEdorItemSchema,
                                LPDutySchema pLPDutySchema) throws Exception
    {
        try
        {
            boolean exDuty = false;
            String oldDutyCode = "";
            if ((pLPDutySchema.getDutyCode().length() == 10)
                && (pLPDutySchema.getDutyCode().substring(6, 7).equals("1")))
            {
                LCDutyDB tLCDutyDB = new LCDutyDB();
                tLCDutyDB.setPolNo(pLPDutySchema.getPolNo());
                tLCDutyDB.setDutyCode(pLPDutySchema.getDutyCode().substring(0,
                        6));
                if (!tLCDutyDB.getInfo())
                {
                    CError.buildErr(this, "获取保单主责任数据失败");
                    throw new Exception("获取保单主责任数据失败");
                }

                pLPDutySchema.setPayEndYear(tLCDutyDB.getPayEndYear());
                pLPDutySchema.setInsuYear(tLCDutyDB.getInsuYear());
                pLPDutySchema.setPayYears(tLCDutyDB.getPayYears());

                //保留附加责任的编码
                exDuty = true;
                oldDutyCode = pLPDutySchema.getDutyCode();
                pLPDutySchema.setDutyCode(pLPDutySchema.getDutyCode().substring(
                        0,
                        7));
            }

            //如果是非标准6位的责任编码，需要截掉末几位
            else if (pLPDutySchema.getDutyCode().length() > 6)
            {
                //保留附加责任的编码
                exDuty = true;
                oldDutyCode = pLPDutySchema.getDutyCode();
                pLPDutySchema.setDutyCode(pLPDutySchema.getDutyCode().substring(
                        0,
                        6));
            }

            //获取生存退保计算责任描述
            LMEdorZTDutyDB tLMEdorZTDutyDB = new LMEdorZTDutyDB();
            tLMEdorZTDutyDB.setRiskCode(mLPPolSchema.getRiskCode());
            mPayEndYearFlag = pLPDutySchema.getPayEndYearFlag();
            this.mRiskCode = mLPPolSchema.getRiskCode();
            tLMEdorZTDutyDB.setDutyCode(pLPDutySchema.getDutyCode());
            if (!tLMEdorZTDutyDB.getInfo())
            {
                CError.buildErr(this, "获取生存退保计算责任描述表数据失败");
                throw new Exception("获取生存退保计算责任描述表数据失败");
            }

            //判断退保类型，是否按账户退保，0－－非帐户退保；1－－帐户退保
            if (tLMEdorZTDutyDB.getPayByAcc().equals("1"))
            {
                System.out.println("该责任项（" + pLPDutySchema.getDutyCode()
                                   + "）要求按帐户退保，不能进行该流程");
                return 0;
            }

            //判断保险责任是否终止
            if (pLPDutySchema.getEndDate().compareTo(pLPEdorItemSchema
                    .getEdorValiDate()) < 0)
            {
                System.out.println("该责任项（" + pLPDutySchema.getPolNo() + ", "
                                   + pLPDutySchema.getDutyCode()
                                   + "）已终止，不能进行退保(endDate"
                                   + pLPDutySchema.getEndDate()
                                   + ",edorValiDate"
                                   + pLPEdorItemSchema.getEdorValiDate());
                //qulq 061128 添加错误LOG  for 退保试算
                String seriNo = PubFun1.CreateMaxNo("ERRORLOGNO", 20);
                LGErrorLogSchema tLGErrorLogSchema = new LGErrorLogSchema();
                tLGErrorLogSchema.setSerialNo(seriNo);
                tLGErrorLogSchema.setErrorType("0001");
                tLGErrorLogSchema.setErrorTypeSub("01");
                tLGErrorLogSchema.setOtherNoType(pLPEdorItemSchema.getEdorType());
                tLGErrorLogSchema.setDescribe("保全退保算费，责任满期");
                mLGErrorLogSet.add(tLGErrorLogSchema);
                //将退费置为零，便于统一处理
                mLJSGetEndorseSet.add(this.getZeroTFLJSGetEndorseSet(
                        pLPEdorItemSchema, mLPPolSchema, pLPDutySchema));

                return 0;
            }

            double totalMoney = 0;

            //判断退保计算类型，是按保费还是保额计算，0－－按保费；1－－按保额
            if (tLMEdorZTDutyDB.getPayCalType().equals("1"))
            {
                //获取生存退保计算描述
                LMEdorZT1DB tLMEdorZT1DB = new LMEdorZT1DB();
                tLMEdorZT1DB.setRiskCode(mLPPolSchema.getRiskCode());
                this.mRiskCode = mLPPolSchema.getRiskCode();
                mPayEndYearFlag = mLPPolSchema.getPayEndYearFlag();
                tLMEdorZT1DB.setDutyCode(pLPDutySchema.getDutyCode());
                tLMEdorZT1DB.setPayPlanCode("000000");
                if (!tLMEdorZT1DB.getInfo())
                {
                    CError.buildErr(this, "获取生存退保计算描述表1数据失败");
                    throw new Exception("获取生存退保计算描述表1数据失败");
                }

                //准备退保计算要素
                this.mTransFlag = tLMEdorZT1DB.getCalCodeType();
                this.mLPEdorItemSchema = pLPEdorItemSchema.getSchema();
//                this.mLPPolSchema = tLPPolBL.getSchema();
                this.mLPDutySchema = pLPDutySchema;
                this.mLMEdorZT1Schema = tLMEdorZT1DB.getSchema();
                this.setCalType(tLMEdorZTDutyDB.getPayCalType()); //按保额

                //计算退保金
                totalMoney = this.getTBJ();

                //恢复附加责任的编码，否则插入批改补退费表中会出错
                if (exDuty)
                {
                    this.mLPDutySchema.setDutyCode(oldDutyCode);
                } else
                {
                    //用退保点作为保全生效日
                    //mLPEdorItemSchema.setEdorValiDate(mLPEdorItemSchema
                    //    .getEdorValiDate());

                    //                    mLPEdorItemSchema.setEdorValiDate(this.getZTPoint());
                    //this.mEdorValidate = this.getZTPoint();
                }
            }

            //按保费进行退保计算
            else if (tLMEdorZTDutyDB.getPayCalType().equals("0"))
            {
                //获取责任下的所有保费项信息
                LPPremSet tLPPremSet = null;
                if (mGetDataFromC)
                {
                    String sql = "select '" + pLPEdorItemSchema.getEdorNo()
                                 + "' EdorNo, '" +
                                 pLPEdorItemSchema.getEdorType()
                                 + "' EdorType, a.* "
                                 + "from LCPrem a "
                                 + "where polNo = '"
                                 + pLPEdorItemSchema.getPolNo()
                                 + "'  and dutyCode = '"
                                 + pLPDutySchema.getDutyCode() + "' ";
                    LPPremDB tLPPremDB = new LPPremDB();
                    tLPPremSet = tLPPremDB.executeQuery(sql);
                } else
                {
                    LPPremBL tLPPremBL = new LPPremBL();
                    tLPPremSet = tLPPremBL.queryAllLPPrem(pLPDutySchema);
                }
                if (tLPPremSet.size() == 0)
                {
                    System.out.println("该责任（" + pLPDutySchema.getDutyCode()
                                       + "）下没有保费项信息");
                }

                for (int i = 0; i < tLPPremSet.size(); i++)
                {
                    totalMoney = totalMoney
                                 +
                                 getPremZTData(pLPEdorItemSchema, pLPDutySchema,
                                               tLPPremSet.get(i + 1));
                }

                //用退保点作为保全生效日
                //mLPEdorItemSchema.setEdorValiDate(mLPEdorItemSchema
                //    .getEdorValiDate());
                //this.mEdorValidate = this.getZTPoint();
            }

            //设置补退费表
            mLJSGetEndorseSet.add(this.getLJSGetEndorseSet());

            return totalMoney;
        } catch (Exception ex)
        {
            throw ex;
        }
    }

    /**
     * 获取单个保费项的退保数据
     * @param pLPEdorItemSchema
     * @param pLPDutySchema
     * @param pLCPremSchema
     * @return
     */
    public double getPremZTData(LPEdorItemSchema pLPEdorItemSchema,
                                LPDutySchema pLPDutySchema,
                                LPPremSchema pLPPremSchema) throws Exception
    {
        try
        {
            //判断是否属于加费项，加费一般不退保
            //if (pLPPremSchema.getPayPlanCode().substring(0, 6) == "000000")
            //{
            //    CError.buildErr(this,
            //        "该保费项（" + pLPPremSchema.getPayPlanCode() + "）属于加费，不能进行退保");
            //    return 0;
            //}

            //获取保单的险种信息
//            LPPolBL tLPPolBL = new LPPolBL();
//            if (!tLPPolBL.queryLPPol(pLPEdorItemSchema))
//            {
//                CError.buildErr(this, "获取保单数据失败");
//                throw new Exception("获取保单数据失败");
//            }

            //获取生存退保计算描述
            LMEdorZT1DB tLMEdorZT1DB = new LMEdorZT1DB();

            //若发生过加费
            if (pLPPremSchema.getPayPlanCode().substring(0, 6).equals("000000"))
            {
                String sql = "  select * "
                             + "from LMEdorZT1 "
                             + "where RiskCode = '" + mLPPolSchema.getRiskCode() +
                             "' "
                             + "    and DutyCode = '" +
                             pLPPremSchema.getDutyCode() + "' "
                             + "    and PayPlanCode like '000000%' ";
                LMEdorZT1Set set = tLMEdorZT1DB.executeQuery(sql);
                if (set.size() == 0)
                {
                    CError.buildErr(this, "获取生存退保计算描述表1数据失败，发生过加费");
                    throw new Exception("取生存退保计算描述表1数据失败，发生过加费");
                }
                this.mTransFlag = set.get(1).getCalCodeType();
                this.mLMEdorZT1Schema = set.get(1).getSchema();
            } else
            {
                tLMEdorZT1DB.setRiskCode(mLPPolSchema.getRiskCode());
                tLMEdorZT1DB.setDutyCode(pLPPremSchema.getDutyCode());
                tLMEdorZT1DB.setPayPlanCode(pLPPremSchema.getPayPlanCode());
                if (!tLMEdorZT1DB.getInfo())
                {
                    CError.buildErr(this, "获取生存退保计算描述表1数据失败");
                    throw new Exception("获取生存退保计算描述表1数据失败");
                }
                this.mTransFlag = tLMEdorZT1DB.getCalCodeType();
                this.mLMEdorZT1Schema = tLMEdorZT1DB.getSchema();
            }

            //准备退保计算要素
            this.mLPEdorItemSchema = pLPEdorItemSchema;
            //this.mLPPolSchema = tLPPolBL.getSchema();
            this.mLPDutySchema = pLPDutySchema;
            this.mLPPremSchema = pLPPremSchema;
            this.setCalType("0"); //按保费

            //计算退保金
            double totalMoney = this.getTBJ();

            //设置补退费表
            mLJSGetEndorseSet.add(this.getLJSGetEndorseSet());

            //用退保点作为保全生效日
            //mLPEdorItemSchema.setEdorValiDate(mLPEdorItemSchema
            //    .getEdorValiDate());
            //mLPEdorItemSchema.setEdorValiDate(this.getZTPoint());

            return totalMoney;
        } catch (Exception ex)
        {
            throw ex;
        }
    }

    /**
     * 生成0退费的财务数据
     * @param tLPDutySchema LPDutySchema
     * @return boolean
     */
    private LJSGetEndorseSchema getZeroTFLJSGetEndorseSet(
            LPEdorItemSchema aLPEdorItemSchema,
            LPPolSchema aLPPolSchema,
            LPDutySchema aLPDutySchema)
    {
        BqCalBL tBqCalBL = new BqCalBL(mBqCalBase, "", "");
        return tBqCalBL.getZeroTFLJSGetEndorseSet(aLPEdorItemSchema,
                                                  aLPPolSchema,
                                                  aLPDutySchema);
    }

    /**
     * 获取单个保单的帐户退保数据
     * @param pLPEdorItemSchema
     * @return
     */
    public boolean getPolAccZTData(LPEdorItemSchema pLPEdorItemSchema)
    {
        try
        {
            //获取保单的所有帐户信息
            LPInsureAccBL tLPInsureAccBL = new LPInsureAccBL();
            LPInsureAccSet tLPInsureAccSet = tLPInsureAccBL.queryAllLPInsureAcc(
                    pLPEdorItemSchema);
            for (int i = 0; i < tLPInsureAccSet.size(); i++)
            {
            	//全局变量滥用导致重复计算,在此清空重算 081127
                mLXFMoney = Double.MIN_VALUE;
                mLXEMoney = Double.MIN_VALUE;
                //理算时对万能险账户按照保证利率结算
                if (CommonBL.hasULIRisk(pLPEdorItemSchema.getContNo()))
                {
                    VData tVData = new VData();
                    GlobalInput mGI = new GlobalInput();
                    mGI.ManageCom = pLPEdorItemSchema.getManageCom();
                    mGI.Operator = pLPEdorItemSchema.getOperator();
                    mGI.ComCode = mGI.ManageCom;
                    tVData.add(mGI);
                    tVData.add(tLPInsureAccSet.get(i + 1));
                    tVData.add(pLPEdorItemSchema);

                    TransferData tf = new TransferData();
                    tf.setNameAndValue("EndDate",pLPEdorItemSchema.getEdorValiDate());
                    tf.setNameAndValue("EdorNo", pLPEdorItemSchema.getEdorNo());
                    tVData.add(tf);
                    InsuAccBala tInsuAccBala = new InsuAccBala();
                    if (tInsuAccBala.submitEdorCTData(tVData, ""))
                    {
                        LPInsureAccDB tLPInsureAccDB = new LPInsureAccDB();
                        tLPInsureAccDB.setEdorNo(tLPInsureAccSet.get(i + 1).getEdorNo());
                        tLPInsureAccDB.setEdorType(tLPInsureAccSet.get(i + 1).getEdorType());
                        tLPInsureAccDB.setPolNo(tLPInsureAccSet.get(i + 1).getPolNo());
                        tLPInsureAccDB.setInsuAccNo(tLPInsureAccSet.get(i + 1).getInsuAccNo());
                        tLPInsureAccDB.getInfo(); //需要获取结算的LPInsureAcc
                        getAccZTData(pLPEdorItemSchema,tLPInsureAccDB.getSchema());
                    }
                } else
                {
                    getAccZTData(pLPEdorItemSchema, tLPInsureAccSet.get(i + 1));
                }
            }
        } catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public double getAccZTData(LPEdorItemSchema pLPEdorItemSchema,
                               LPInsureAccSchema pLPInsureAccSchema) throws
            Exception
    {
        try
        {
            double totalMoney = 0;

            LMEdorZTAccDB tLMEdorZTAccDB = new LMEdorZTAccDB();
            tLMEdorZTAccDB.setRiskCode(pLPInsureAccSchema.getRiskCode());
            tLMEdorZTAccDB.setInsuAccNo(pLPInsureAccSchema.getInsuAccNo());

            //只有部分险种才有帐户退保描述，所有没有描述也正常
            if (!tLMEdorZTAccDB.getInfo())
            {
                return 0;
            }

            //获取保单的险种信息
            LPPolBL tLPPolBL = new LPPolBL();
            if (!tLPPolBL.queryLPPol(pLPEdorItemSchema))
            {
                CError.buildErr(this, "获取保单数据失败");
                throw new Exception("获取保单数据失败");
            }

            //准备退保计算要素
            this.mLMEdorZTAccSchema = tLMEdorZTAccDB.getSchema();
            this.mLPPolSchema = tLPPolBL.getSchema();
            this.mLPEdorItemSchema = pLPEdorItemSchema;
            this.mLPInsureAccSchema = pLPInsureAccSchema;
            this.setCalType("2"); //按帐户

            //计算退保金
            totalMoney = getAccTBJ();

            //设置补退费表
            mLJSGetEndorseSet.add(this.getLJSGetEndorseSet());

            return totalMoney;
        } catch (Exception ex)
        {
            throw ex;
        }
    }

    /**
     * 计算退保金，mTransFlag在构造函数中进行付值
     * @param pTransFlag LMEdorZT1中的计算代码类型CalCodeType
     * @return
     */
    public double getAccTBJ() throws Exception
    {
        try
        {
            //简单计算退保
            if (mLMEdorZTAccSchema.getCalCodeType().equals("6"))
            {
                if (!this.calAccZTMoney())
                {
                    throw new Exception("简单计算退保失败");
                }
            } else if (mLMEdorZTAccSchema.getCalCodeType().equals("7"))
            {
                //健康险个人帐户退保,扣除管理费
                if (!this.calAccZTMoney(mLMEdorZTAccSchema))
                {
                    throw new Exception("健康险扣除解约费退保");
                }
            }
        } catch (Exception ex)
        {
            throw ex;
        }

        return this.getAccTBMoney();
    }

    /**
     * 健康险个人帐户退保
     * @return
     */
    private boolean calAccZTMoney(LMEdorZTAccSchema pLMEdorZTAccSchema)
    {
        //取得趸交、期交算法编码；扣除2%的解约费
        String tCalCode = pLMEdorZTAccSchema.getCalCode();
        //取得帐户现金余额(本金+利息)
        mBqCalBase = new BqCalBase();
        //先算本金管理费
        String tSQL = "select year('" + mLPEdorItemSchema.getEdorValiDate() +
                      "'-CValidate)+1 from LCPol where Polno = '" +
                      mLPInsureAccSchema.getPolNo() + "'";
        //设置保单年度，万能计算解约手续费需要此参数
        mBqCalBase.setUpPolYears(new ExeSQL().getOneValue(tSQL));
        mBqCalBase.setGetMoney(mLPInsureAccSchema.getInsuAccBala());
        mBqCalBase.setInsuAccBala(mLPInsureAccSchema.getInsuAccBala());
        mAccMoney = mLPInsureAccSchema.getInsuAccBala();
        mBqCalBase.setGrpContNo(mLPInsureAccSchema.getGrpContNo());
        mBqCalBase.setGrpPolNo(mLPInsureAccSchema.getGrpPolNo());
        mBqCalBase.setRiskCode(mLPInsureAccSchema.getRiskCode());
        mBqCalBase.setContNo(mLPInsureAccSchema.getContNo());
        mBqCalBase.setPolNo(mLPInsureAccSchema.getPolNo());
        mBqCalBase.setAccRate(100); //利率
        mBqCalBase.setAccManageFeeRateZT(
                CommonBL.getManageFeeRate(mLPInsureAccSchema.getGrpPolNo(),
                                          BQ.FEECODE_CT));

        BqCalBL tBqCalBL = new BqCalBL(mLPEdorItemSchema, mBqCalBase,
                                       tCalCode,
                                       "");
        double tAccValue = tBqCalBL.calGetEndorse(tCalCode, mBqCalBase);
        System.out.println("tAccValue: " + tAccValue);
        //061123 qulq  070530 该死的批单。金额要分离出来。
        this.setAccTBMoney(tAccValue);
        mAccTBFMoney += tAccValue;
        //非万能险，需要再算利息管理费
        if (!CommonBL.hasULIRisk(mLPEdorItemSchema.getContNo()))
        {
            mLXMoney = rateCal(pLMEdorZTAccSchema, mLPInsureAccSchema);
            mLXFMoney += mLXMoney;
            mBqCalBase.setGetMoney(mLXMoney);
            mBqCalBase.setInsuAccBala(mLXMoney);
            mLXMoney = tBqCalBL.calGetEndorse(tCalCode, mBqCalBase);
            mLXEMoney += mLXMoney;
            this.setLXMoney(mLXMoney);
        }
        return true;
    }

    /**
     * qulq 061123 加利息计算
     * @return boolean
     */
    private double rateCal(LMEdorZTAccSchema pLMEdorZTAccSchema,
                           LPInsureAccSchema aLPInsureAccSchema)
    {
        //计算当前帐户利息 qulq 061121
        String InsuAccNo = pLMEdorZTAccSchema.getInsuAccNo();
        LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
        tLMRiskInsuAccDB.setInsuAccNo(InsuAccNo);
        if (tLMRiskInsuAccDB.getInfo() == false)
        {
            CError.buildErr(this,
                            "账户描述表查询失败:" + InsuAccNo +
                            tLMRiskInsuAccDB.mErrors.getFirstError());
        }

        if (this.mLPEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_CT))
        {
            //依据帐户计算利息 qulq 061121

            EdorItemSpecialData tEdorItemSpecialData =
                    new EdorItemSpecialData(this.mLPEdorItemSchema.getEdorNo(),
                                            this.mLPEdorItemSchema.getEdorNo(),
                                            BQ.EDORTYPE_CT);
            tEdorItemSpecialData.query();
            if (tEdorItemSpecialData.getSpecialDataSet() == null ||
                tEdorItemSpecialData.getSpecialDataSet().size() < 1)
            {
                return 0.0;
            }

            //确定帐户类型 004 团体固定1,001 团体2, 002 个人3
            int type = 0;
            double rate = 0;
            double rateFee = 0.0;
            if (tLMRiskInsuAccDB.getAccType().equals("004"))
            {
                type = 1;
            } else if (tLMRiskInsuAccDB.getAccType().equals("001"))
            {
                type = 2;
            } else if (tLMRiskInsuAccDB.getAccType().equals("002"))
            {
                type = 3;
            }
            //如果是约定利息金额，就不用计算了 qulq 061121
            if (getEdorValue("RATEMONEY",
                             tEdorItemSpecialData.getSpecialDataSet()) != null)
            {
                //在这里进行账户利息拆分,
                /*
                                     对于在特需退保时录入利息，将按照一下规则进行利息拆分：(指退保时是否)
                                     1.若录入利息，则按以下顺序将利息放入账户：
                   a	若有团体理赔账户，则进入团体理赔账户。
                   b	若没有团体理赔账户，则进入团体固定账户。
                   c	若没有团体固定账户，则进入个人账户。
                                     2.	若利息进入个人账户，则按个人账户金额比例分配至每个个人账户，
                    若不能均分，则差额放在最后一个个人账户。
                 */
                String specialRiskcode = CommonBL.getSpecialRisk(
                        aLPInsureAccSchema.getGrpContNo());
                LPPolDB tLPPolDB = new LPPolDB();
                tLPPolDB.setEdorNo(aLPInsureAccSchema.getEdorNo());
                tLPPolDB.setEdorType(aLPInsureAccSchema.getEdorType());
                tLPPolDB.setGrpContNo(aLPInsureAccSchema.getGrpContNo());
                tLPPolDB.setRiskCode(specialRiskcode);
                tLPPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
                LPPolSet tLPPolSet = tLPPolDB.query();
                if (tLPPolSet != null && tLPPolSet.size() > 0)
                {
                    LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                    tLCInsureAccDB.setGrpContNo(aLPInsureAccSchema.getGrpContNo());
                    tLCInsureAccDB.setPolNo(tLPPolSet.get(1).getPolNo());
                    tLCInsureAccDB.setInsuAccNo(CommonBL.getInsuAccNo(BQ.
                            ACCTYPE_GROUP, specialRiskcode));
                    if (tLCInsureAccDB.getInfo())
                    {
                        if (tLCInsureAccDB.getPolNo().equals(aLPInsureAccSchema.
                                getPolNo())
                            &&
                            tLCInsureAccDB.getInsuAccNo().equals(
                                    aLPInsureAccSchema.
                                    getInsuAccNo()))
                        {
                            return Double.parseDouble(getEdorValue("RATEMONEY",
                                    tEdorItemSpecialData.getSpecialDataSet()));
                        }
                    } else
                    {
                        tLCInsureAccDB.setInsuAccNo(CommonBL.getInsuAccNo(BQ.
                                ACCTYPE_FIXED, specialRiskcode));
                        if (tLCInsureAccDB.getInfo())
                        {
                            if (tLCInsureAccDB.getPolNo().equals(
                                    aLPInsureAccSchema.getPolNo())
                                &&
                                tLCInsureAccDB.getInsuAccNo().equals(
                                        aLPInsureAccSchema.
                                        getInsuAccNo()))
                            {
                                return Double.parseDouble(getEdorValue(
                                        "RATEMONEY",
                                        tEdorItemSpecialData.getSpecialDataSet()));
                            }
                        } else
                        {
                            return 0.0;
                        }
                    }
                } else
                { //没有公共账户，需要拆分到个人账户
                    //取得账户总金额,只向个人账户拆分
                    if (!aLPInsureAccSchema.getInsuAccNo().equals(CommonBL.
                            getInsuAccNo(BQ.ACCTYPE_INSURED, specialRiskcode)))
                    {
                        return 0.0;
                    }
                    double sumMoney = 0.0;
                    int count = 0;
                    String money = "select sum(insuaccbala),count(distinct polno) from lcinsureacc where grpcontno ='"
                                   + aLPInsureAccSchema.getGrpContNo() + "'"
                                   +
                                   " and polno in (SELECT POLNO FROM LPPOL WHERE EDORNO ='"
                                   + aLPInsureAccSchema.getEdorNo()
                                   + "' and edorType = '" +
                                   aLPInsureAccSchema.getEdorType() +
                                   "'and riskcode in (select riskcode from lmriskapp where risktype ='H' AND RISKTYPE3 ='7'))";
                    ExeSQL tExeSQL = new ExeSQL();
                    SSRS tSSRS = tExeSQL.execSQL(money);
                    if (tSSRS != null && tSSRS.getMaxRow() > 0)
                    {
                        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                        {
                            sumMoney = Double.parseDouble(tSSRS.GetText(i, 1));
                            count = Integer.parseInt(tSSRS.GetText(i, 2));
                        }
                    }
                    if (sumMoney < 0.00001 && sumMoney > -0.00001)
                    {
                        if (count == 0)
                        {
                            return 0.0;
                        } else
                        {
                            return Double.parseDouble(getEdorValue("RATEMONEY",
                                    tEdorItemSpecialData.getSpecialDataSet())) /
                                    count;
                        }
                    } else
                    {
                        double ratemoney = Double.parseDouble(getEdorValue(
                                "RATEMONEY",
                                tEdorItemSpecialData.getSpecialDataSet()));
                        if (ratemoney > -0.0001 && ratemoney < 0.0001)
                        {
                            return ratemoney;
                        } else
                        {
                            return (ratemoney *
                                    aLPInsureAccSchema.getInsuAccBala()) /
                                    sumMoney;
                        }

                    }
                }
            }

            AccountManage tAccountManage = new AccountManage();
            LCInsureAccClassSchema tLCInsureAccClassSchema = new
                    LCInsureAccClassSchema();
            tLCInsureAccClassSchema.setPolNo(mLPInsureAccSchema.getPolNo());
            tLCInsureAccClassSchema.setInsuAccNo(mLPInsureAccSchema.
                                                 getInsuAccNo());
            tEdorItemSpecialData.setGrpPolNo(mLPInsureAccSchema.getGrpPolNo());
            switch (type)
            {
            case 1:

                //在前台将利息填如相应字段（无利息为0），统一计算
                //添加polno 关联

                rate = Double.parseDouble(tEdorItemSpecialData.getEdorValue(
                        "FIXEDRATE"));

                //计算利息

                rateFee = tAccountManage.getOneAccClassInterest(
                        tLCInsureAccClassSchema, rate,
                        mLPEdorItemSchema.getEdorValiDate(), 365);
                break;
            case 2:

                //在前台将利息填如相应字段（无利息为0），统一计算
                rate = Double.parseDouble(tEdorItemSpecialData.getEdorValue(
                        "GROUPRATE"));

                //计算利息
                rateFee = tAccountManage.getOneAccClassInterest(
                        tLCInsureAccClassSchema, rate,
                        mLPEdorItemSchema.getEdorValiDate(), 365);
                break;
            case 3:

                //在前台将利息填如相应字段（无利息为0），统一计算
                rate = Double.parseDouble(tEdorItemSpecialData.getEdorValue(
                        "PERSONALRATE"));

                //计算利息

                rateFee = tAccountManage.getOneAccClassInterest(
                        tLCInsureAccClassSchema, rate,
                        mLPEdorItemSchema.getEdorValiDate(), 365);
                break;
            default:
                this.mErrors.addOneError("计算利息错误！");
                break;

            }
            return rateFee;
        }

        return 0.0;
    }

    /**
     * 简单计算退保
     * @return
     */
    private boolean calAccZTMoney()
    {
        getZTPoint(mLPInsureAccSchema.getPolNo(),
                   mLPEdorItemSchema.getEdorValiDate());

        return calAccZTMoney(mLMEdorZTAccSchema,
                             mLPEdorItemSchema.getEdorValiDate(),
                             mLPPolSchema.getCValiDate(),
                             getZTPoint());
    }

    /**
     * 简单计算退保
     * @return
     */
    private boolean calAccZTMoney(LMEdorZTAccSchema pLMEdorZTAccSchema,
                                  String pEdorValiDate, String pPolCValiDate,
                                  String pZTPoint)
    {
        //取得趸交、期交算法编码，用约进法重新计算退保年度，设置退保点
        String tCalCode = pLMEdorZTAccSchema.getCalCode();

        //退保年度，舍弃法
        int tZTYear = PubFun.calInterval(pPolCValiDate, pZTPoint, "Y");

        //初始化计算要素
        initBqCalBase();

        //退保年度
        mBqCalBase.setInterval(tZTYear);
        mBqCalBase.setGetMoney(mLPInsureAccSchema.getInsuAccBala());

        //按照比例法计算退保
        BqCalBL tBqCalBL = new BqCalBL(mLPEdorItemSchema, mBqCalBase, tCalCode,
                                       "");
        double tAccValue = tBqCalBL.calGetEndorse(tCalCode, mBqCalBase);
        System.out.println("End 计算，按帐户比例退保算法，tAccValue: " + tAccValue);

        this.setAccTBMoney(tAccValue);

        return true;
    }

    /**
     * 使用个人批改主表数据获取该保单的现价
     * @param aLPEdorItemSchema
     * @return
     */
    public static double getCashValue(LPEdorItemSchema aLPEdorItemSchema)
    {
        System.out.println("\nStart Get Cash Value...");

        LPPolBL tLPPolBL = new LPPolBL();
        LPPolSchema nullSchema = new LPPolSchema();
        tLPPolBL.queryLastLPPol(aLPEdorItemSchema, nullSchema);

        LCPolSchema tLCPolSchema = new LCPolSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLCPolSchema, tLPPolBL.getSchema());

        BqCalBase tBqCalBase = new BqCalBase();

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setSchema(tLCPolSchema);

        //得到交费年期
        tBqCalBase.setPayEndYear(tLCPolDB.getPayEndYear());

        //退保年度
        tBqCalBase.setInterval(PubFun.calInterval(tLCPolDB.getCValiDate(),
                                                  aLPEdorItemSchema.
                                                  getEdorValiDate(), "Y"));
        tBqCalBase.setYears(tLCPolDB.getYears());

        //得到投保年龄，性别
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        LPInsuredBL tLPInsuredBL = new LPInsuredBL();
        LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
        tLPInsuredSchema.setInsuredNo(tLCPolDB.getInsuredNo());
        tLPInsuredSchema.setContNo(aLPEdorItemSchema.getContNo());
        tLPInsuredBL.queryLastLPInsured(aLPEdorItemSchema, tLPInsuredSchema);

        tBqCalBase.setAppAge(PubFun.calInterval(
                tLPInsuredBL.getBirthday(), tLCPolDB.getCValiDate(), "Y"));
        tBqCalBase.setSex(tLPInsuredBL.getSex());

        tBqCalBase.setCValiDate(aLPEdorItemSchema.getEdorValiDate());
        tBqCalBase.setPolNo(tLCPolDB.getPolNo());
        tBqCalBase.setEdorValiDate(aLPEdorItemSchema.getEdorValiDate());
        tBqCalBase.setMult(tLCPolDB.getMult());
        tBqCalBase.setPayEndYearFlag(tLCPolDB.getPayEndYearFlag());
        tBqCalBase.setGetStartFlag(tLCPolDB.getGetYearFlag());
        tBqCalBase.setPrem(tLCPolDB.getPrem());
        tBqCalBase.setGet(tLCPolDB.getAmnt());
        tBqCalBase.setPayIntv(tLCPolDB.getPayIntv()); //add by Minim

        tBqCalBase.setInsuYear(tLCPolDB.getInsuYear()); //保险年龄年期
        tBqCalBase.setInsuYearFlag(tLCPolDB.getInsuYearFlag()); //保险年龄年期标志
        tBqCalBase.setGetStartYear(tLCPolDB.getGetYear()); //领取年龄年期

        double aCashValue = 0;
        String tCalCode = "";

        LMEdorZT1Set tLMEdorZT1Set = new LMEdorZT1Set();
        LMEdorZT1DB tLMEdorZT1DB = new LMEdorZT1DB();
        tLMEdorZT1DB.setRiskCode(tLCPolDB.getRiskCode());
        tLMEdorZT1Set = tLMEdorZT1DB.query();

        if (tLMEdorZT1Set.size() <= 0)
        {
            CError tError = new CError();
            tError.moduleName = "EdorCalZT";
            tError.functionName = "CalMoney";
            tError.errorMessage = "缺少退保险种描述!";

            return aCashValue;
        }

        for (int j = 1; j <= tLMEdorZT1Set.size(); j++)
        {
            if ((tLMEdorZT1Set.get(j).getCashValueCode() != null)
                && !tLMEdorZT1Set.get(j).getCashValueCode().equals(""))
            {
                tCalCode = tLMEdorZT1Set.get(j).getCashValueCode();

                if ((tCalCode == null) || tCalCode.trim().equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "PEdorGADetailBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "缺少退保现金价值险种描述编码!";

                    return aCashValue;
                }

                BqCalBL tBqCalBL = new BqCalBL(tBqCalBase, tCalCode, "");
                aCashValue = aCashValue
                             + tBqCalBL.calGetEndorse(tCalCode, tBqCalBase);
                if (tBqCalBL.mErrors.needDealError())
                {
                    CError tError = new CError();
                    tError.moduleName = "PEdorGADetailBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "计算退保金失败!";

                    return aCashValue;
                }

            }
        }

        System.out.println("\nCashValue:" + aCashValue + "End Get Cash Value\n");
        return aCashValue;
    }

    /**
     * 得到某年度的现金价值
     * @return
     */
    public static double getCashValue(String aPolNo, String aGetDate)
    {
        double aCashValue = 0;
        String tCalCode = "";
        String tFlag = "";
        BqCalBase tBqCalBase = new BqCalBase();

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(aPolNo);

        if (!tLCPolDB.getInfo())
        {
            return aCashValue;
        }

        //得到交费年期
        tBqCalBase.setPayEndYear(tLCPolDB.getPayEndYear());

        //退保年度
        tBqCalBase.setInterval(PubFun.calInterval(tLCPolDB.getCValiDate(),
                                                  aGetDate, "Y"));
        tBqCalBase.setYears(tLCPolDB.getYears());

        //得到投保年龄，性别
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(tLCPolDB.getContNo());
        tLCInsuredSet = tLCInsuredDB.query();

        for (int j = 1; j <= tLCInsuredSet.size(); j++)
        {

            tBqCalBase.setAppAge(PubFun.calInterval(
                    tLCInsuredSet.get(j).getBirthday(),
                    tLCPolDB.getCValiDate(), "Y"));
            tBqCalBase.setSex(tLCInsuredSet.get(j).getSex());

        }

        tBqCalBase.setCValiDate(aGetDate);
        tBqCalBase.setPolNo(tLCPolDB.getPolNo());
        tBqCalBase.setEdorValiDate(aGetDate);
        tBqCalBase.setMult(tLCPolDB.getMult());
        tBqCalBase.setPayEndYearFlag(tLCPolDB.getPayEndYearFlag());
        tBqCalBase.setGetStartFlag(tLCPolDB.getGetYearFlag());
        tBqCalBase.setPrem(tLCPolDB.getPrem());
        tBqCalBase.setGet(tLCPolDB.getAmnt());

        tBqCalBase.setPayIntv(tLCPolDB.getPayIntv()); //add by Minim
        tBqCalBase.setFloatRate(tLCPolDB.getFloatRate()); //add by Minim

        LMEdorZT1Set tLMEdorZT1Set = new LMEdorZT1Set();
        LMEdorZT1DB tLMEdorZT1DB = new LMEdorZT1DB();
        tLMEdorZT1DB.setRiskCode(tLCPolDB.getRiskCode());

        //        tLMEdorZT1DB.setDutyCode(tLCPolDB.getRiskVersion());
        tLMEdorZT1Set = tLMEdorZT1DB.query();

        if (tLMEdorZT1Set.size() <= 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "EdorCalZT";
            tError.functionName = "CalMoney";
            tError.errorMessage = "缺少退保险种描述!";

            return aCashValue;
        }

        for (int j = 1; j <= tLMEdorZT1Set.size(); j++)
        {
            if ((tLMEdorZT1Set.get(j).getCashValueCode() != null)
                && !tLMEdorZT1Set.get(j).getCashValueCode().equals(""))
            {
                tCalCode = tLMEdorZT1Set.get(j).getCashValueCode();

                if ((tCalCode == null) || tCalCode.trim().equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "PEdorGADetailBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "缺少退保现金价值险种描述编码!";

                    return aCashValue;
                }

                BqCalBL tBqCalBL = new BqCalBL(tBqCalBase, tCalCode, "");
                aCashValue = aCashValue
                             + tBqCalBL.calGetEndorse(tCalCode, tBqCalBase);
                if (tBqCalBL.mErrors.needDealError())
                {
                    CError tError = new CError();
                    tError.moduleName = "PEdorGADetailBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "计算退保金失败!";

                    return aCashValue;
                }

            }
        }

        System.out.println("\nCashValue:" + aCashValue + "End Get Cash Value\n");
        return aCashValue;

        //        for (int j = 1; j <= tLMEdorZT1Set.size(); j++)
        //        {
        //            if ((tLMEdorZT1Set.get(j).getCashValueCode() != null)
        //                    && !tLMEdorZT1Set.get(j).getCashValueCode().equals(""))
        //            {
        //                tCalCode = tLMEdorZT1Set.get(j).getCashValueCode();
        //            }
        //        }
        //
        //        if ((tCalCode == null) || tCalCode.trim().equals(""))
        //        {
        //            // @@错误处理
        //            CError tError = new CError();
        //            tError.moduleName = "PEdorGADetailBL";
        //            tError.functionName = "dealData";
        //            tError.errorMessage = "缺少退保现金价值险种描述编码!";
        //
        //            return aCashValue;
        //        }
        //
        //        BqCalBL tBqCalBL = new BqCalBL(tBqCalBase, tCalCode, tFlag);
        //        aCashValue = tBqCalBL.calGetEndorse(tCalCode, tBqCalBase);
        //
        //        return aCashValue;
    }

    /**
     * 为保单个人帐户中单位交费部分结息，同时减去手续费，适用于团体众悦年金，add by Minim at 2004-2-20
     * 团体借款在使用
     * @param PolNo
     */
    public static double getGrpUnderPersonAcc(String polNo, String ztPoint,
                                              int ztYear)
    {
        try
        {
            //获取所有的帐户操作轨迹，包括可能正在操作的资金帐户调整内容
            LPInsureAccTraceBL tLPInsureAccTraceBL = new LPInsureAccTraceBL();
            LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceBL
                    .queryAllTrace(polNo);

            //把非单位交费部分去除掉
            LPInsureAccTraceSet outLPInsureAccTraceSet = new
                    LPInsureAccTraceSet();
            for (int i = 0; i < tLPInsureAccTraceSet.size(); i++)
            {
                if (tLPInsureAccTraceSet.get(i +
                                             1).getInsuAccNo().equals("000002"))
                {
                    outLPInsureAccTraceSet.add(tLPInsureAccTraceSet.get(i + 1));
                }
            }

            return getAccCashValue(outLPInsureAccTraceSet, ztPoint, ztYear);
        } catch (Exception ex)
        {
            ex.printStackTrace();

            return 0;
        }
    }

    /**
     * 为保单个人帐户结息，同时减去手续费，适用于团体众悦年金，add by Minim at 2004-2-16
     * @param PolNo
     */
    public static double getAllPersonAcc(String polNo, String ztPoint,
                                         int ztYear)
    {
        try
        {
            LPInsureAccTraceBL tLPInsureAccTraceBL = new LPInsureAccTraceBL();

            //获取所有的帐户操作轨迹，包括可能正在操作的资金帐户调整内容
            LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceBL
                    .queryAllTrace(polNo);

            return getAccCashValue(tLPInsureAccTraceSet, ztPoint, ztYear);
        } catch (Exception ex)
        {
            ex.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取帐户现价价值，add by Minim at 2004-2-20
     * @param tLPInsureAccTraceSet
     * @param ztPoint 退保点
     * @param ztYear 退保年度
     * @return
     */
    public static double getAccCashValue(
            LPInsureAccTraceSet tLPInsureAccTraceSet, String ztPoint,
            int ztYear)
    {
        double totalMoney = 0;
        ExeSQL e = new ExeSQL();

        for (int i = 0; i < tLPInsureAccTraceSet.size(); i++)
        {
            String strSql = "select * from rateedor601 where startYear<="
                            + (ztYear) + " and endYear>" + (ztYear);
            System.out.println("众悦年金 Sql: " + strSql);

            SSRS s = e.execSQL(strSql);
            System.out.println("众悦年金 Rate: " + s.GetText(1, 3) + "轨迹金额："
                               + tLPInsureAccTraceSet.get(i + 1).getMoney() +
                               "银行分段利息："
                               + AccountManage.getMultiAccInterest("0",
                    tLPInsureAccTraceSet.get(i + 1).getMoney(),
                    tLPInsureAccTraceSet.get(i + 1).getMakeDate(), ztPoint,
                    "C", "Y"));
            totalMoney = totalMoney
                         + ((tLPInsureAccTraceSet.get(i + 1).getMoney()
                             + AccountManage.getMultiAccInterest("0",
                    tLPInsureAccTraceSet.get(i + 1).getMoney(),
                    tLPInsureAccTraceSet.get(i + 1).getMakeDate(), ztPoint,
                    "C", "Y")) * Double.parseDouble(s.GetText(1, 3)));
        }

        System.out.println("众悦年金 Money: " + totalMoney);

        return totalMoney;
    }

    /**
     * 为保单的公共帐户结息，适用于团体众悦年金，add by Minim at 2004-2-18
     * @param PolNo
     */
    public static double getGrpAccInterest(String polNo, String ztPoint)
    {
        try
        {
            LPInsureAccTraceBL tLPInsureAccTraceBL = new LPInsureAccTraceBL();

            //获取所有的帐户操作轨迹，包括可能正在操作的资金帐户调整内容
            LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceBL
                    .queryAllTrace(polNo);

            double totalMoney = 0;

            for (int i = 0; i < tLPInsureAccTraceSet.size(); i++)
            {
                totalMoney = totalMoney
                             + (tLPInsureAccTraceSet.get(i + 1).getMoney()
                                + AccountManage.getMultiAccInterest("0",
                        tLPInsureAccTraceSet.get(i + 1).getMoney(),
                        tLPInsureAccTraceSet.get(i + 1).getMakeDate(), ztPoint,
                        "C", "Y"));
            }

            return totalMoney;
        } catch (Exception ex)
        {
            ex.printStackTrace();

            return 0;
        }
    }

    //
    public LGErrorLogSet getErrorLogSet()
    {
        return this.mLGErrorLogSet;
    }

    //qulq
    private String getEdorValue(String detailType,
                                LPEdorEspecialDataSet mSpecialDataSet)
    {
        for (int i = 1; i <= mSpecialDataSet.size(); i++)
        {
            LPEdorEspecialDataSchema tEspecialSchema = mSpecialDataSet.get(i);
            String dt = tEspecialSchema.getDetailType();
            if ((dt != null) && (dt.equalsIgnoreCase(detailType)))
            {
                return tEspecialSchema.getEdorValue();
            }
        }
        return null;
    }

    public void setLXMoney(double aMoney)
    {
        this.mLXMoney = aMoney;
    }

    public double getLXMoney()
    {
        return this.mLXMoney;
    }



    public static void main(String[] args)
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();

        tLPEdorItemDB.setEdorNo("20061128000002");
        tLPEdorItemDB.setEdorType("CT");
        tLPEdorItemSchema.setSchema(tLPEdorItemDB.query().get(1));

        EdorCalZT tEdorCalZT = new EdorCalZT();
        try
        {
            System.out.println(tEdorCalZT.getUpPolYears(tLPEdorItemSchema));
        } catch (Exception ex)
        {
        }
    }
}
