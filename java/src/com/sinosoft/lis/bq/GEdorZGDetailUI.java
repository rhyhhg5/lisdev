package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 追加管理费保全数据</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author lzy
 * @version 1.0
 */

public class GEdorZGDetailUI
{
    private GEdorZGDetailBL mGEdorZGDetailBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public GEdorZGDetailUI(GlobalInput gi, String edorNo,
            String grpContNo, EdorItemSpecialData specialData)
    {
        mGEdorZGDetailBL = new GEdorZGDetailBL(gi, edorNo, grpContNo, specialData);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mGEdorZGDetailBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public  String getError()
    {
        return mGEdorZGDetailBL.mErrors.getFirstError();
    }

    /**
     * 得到需显示的提示信息
     * @return String
     */
    public String getMessage()
    {
        return mGEdorZGDetailBL.getMessage();
    }
}
