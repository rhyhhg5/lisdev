package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;

/**
 * <p>Title: 保全变更投保人项目</p>
 * <p>Description: 录入保全明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorAEDetailUI
{
    private PEdorAEDetailBL mPEdorAEDetailBL = null;

    /**
     * 构造函数
     */
    public PEdorAEDetailUI()
    {
        mPEdorAEDetailBL = new PEdorAEDetailBL();
    }

    /**
     * 调用业务逻辑
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!mPEdorAEDetailBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mPEdorAEDetailBL.mErrors.getFirstError();
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mPEdorAEDetailBL.getMessage();
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.ManageCom = "86";
        gi.Operator = "endor";

        LPAppntSchema tLPAppntSchema = new LPAppntSchema();
        tLPAppntSchema.setEdorNo("20050926000002");
        tLPAppntSchema.setEdorType("AE");
        tLPAppntSchema.setContNo("00000000201");
        tLPAppntSchema.setAppntName("qq");
        tLPAppntSchema.setAppntSex("0");
        tLPAppntSchema.setAppntBirthday("1985-05-06");
        tLPAppntSchema.setIDType("2");
        tLPAppntSchema.setIDNo("11111");
        tLPAppntSchema.setOccupationCode("00103");
        tLPAppntSchema.setOccupationType("2");
        tLPAppntSchema.setBankCode("");
        tLPAppntSchema.setBankAccNo("");
        tLPAppntSchema.setAccName("");

        LPAddressSchema tLPAddressSchema = new LPAddressSchema();

        TransferData tTransferData = new TransferData();

        VData data = new VData();
        data.add(gi);
        data.add(tLPAppntSchema);
        data.add(tLPAddressSchema);
        data.add(tTransferData);
        PEdorAEDetailUI tPEdorAEDetailUI = new PEdorAEDetailUI();
        if (!tPEdorAEDetailUI.submitData(data))
        {
            System.out.println(tPEdorAEDetailUI.getError());
        }
    }
}
