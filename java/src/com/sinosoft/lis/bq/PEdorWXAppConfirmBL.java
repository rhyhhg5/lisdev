package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCInsureAccClassFeeDB;
import com.sinosoft.lis.db.LCInsureAccDB;
import com.sinosoft.lis.db.LCInsureAccFeeDB;
import com.sinosoft.lis.db.LCInsureAccFeeTraceDB;
import com.sinosoft.lis.db.LCInsureAccTraceDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMInsuAccRateDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPInsureAccClassFeeSchema;
import com.sinosoft.lis.schema.LPInsureAccClassSchema;
import com.sinosoft.lis.schema.LPInsureAccFeeSchema;
import com.sinosoft.lis.schema.LPInsureAccFeeTraceSchema;
import com.sinosoft.lis.schema.LPInsureAccSchema;
import com.sinosoft.lis.schema.LPInsureAccTraceSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LCInsureAccClassFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccClassSet;
import com.sinosoft.lis.vschema.LCInsureAccFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccFeeTraceSet;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.lis.vschema.LCInsureAccTraceSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMInsuAccRateSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

//程序名称：PEdorWXAppConfirmBL.java
//程序功能：
//创建日期：2011-08-04
//创建人  ：xp
//更新记录：  更新人    更新日期     更新原因/内容
public class PEdorWXAppConfirmBL implements EdorAppConfirm {
	private MMap mMap = new MMap();

	private GlobalInput mGlobalInput = null;

	/** 保全号 */
	private String mEdorNo = null;

	/** 保全类型 */
	private String mEdorType = null;

	/** 合同号 */
	private String mContNo = null;

	private String mInsuAccNo = null;

	private Reflections ref = new Reflections();

	/** 帐户相关信息表 */
	private LPInsureAccSchema mLPInsureAccSchema = new LPInsureAccSchema();

	private LPInsureAccClassSchema mLPInsureAccClassSchema = new LPInsureAccClassSchema();

	private LPInsureAccFeeSchema mLPInsureAccFeeSchema = new LPInsureAccFeeSchema();

	private LPInsureAccClassFeeSchema mLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();

	/** 补费轨迹表 */
	private LPInsureAccFeeTraceSchema mLPInsureAccFeeTraceSchema = new LPInsureAccFeeTraceSchema();

	private LPInsureAccTraceSchema mLPInsureAccTraceSchema = new LPInsureAccTraceSchema();

	/** 存放主险保单数据 */
	private LCPolSchema mLCPolSchema = null;

	private LMInsuAccRateSet mLMInsuAccRateSet = new LMInsuAccRateSet();

	/** 保全生效日期 */
	private String mEdorValiDate = null;

	/** 保全项目特殊数据 */
	private EdorItemSpecialData mEdorItemSpecialData = null;

	private LPEdorItemSchema mLPEdorItemSchema = null;

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/** 续期补费金额 */
	private double mFee = 0;
	
	private double mLjsGetEndorMoneyGL = 0.0;
	
	private double mLjsGetEndorMoneyLX = 0.0;
	
	/** 续期核销日期是否在月结截止日期之后 */
	private boolean mIsLate=false;

	/** 补费利息 */
	private double mFeeLX = 0;

	/** 补费利息计算起始日期 */
	private String mFeeLXDate = "";
	
	private String mGetNoticeNo = "";

	private String mFeePayDate = "";

	/**
	 * 提交数据
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			System.out.println("PEdorZBAppConfirmBL.java->getInputData(cInputData)失败");
			return false;
		}

		if (!dealData()) {
			System.out.println("PEdorZBAppConfirmBL.java->dealData()失败");
			return false;
		}
		return true;
	}

	/**
	 * 返回理算结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		VData data = new VData();
		data.add(mMap);
		return data;
	}

	/**
	 * 得到传入参数
	 * 
	 * @param cInputData
	 *            VData
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		if (mGlobalInput == null) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取操作用户信息失败";
			mErrors.addOneError(tError);
			return false;
		}
		mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
		if (mLPEdorItemSchema == null) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保全项目失败";
			mErrors.addOneError(tError);
			return false;
		}
		mEdorNo = mLPEdorItemSchema.getEdorNo();
		if (mEdorNo == null || mEdorNo.equals("") || mEdorNo.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保全号失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mEdorType = mLPEdorItemSchema.getEdorType();
		if (mEdorType == null || mEdorType.equals("") || mEdorType.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保全类型失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mContNo = mLPEdorItemSchema.getContNo();
		if (mContNo == null || mContNo.equals("") || mContNo.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单号失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mEdorValiDate = mLPEdorItemSchema.getEdorValiDate();
		if (mEdorValiDate == null || mEdorValiDate.equals("") || mEdorValiDate.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保全生效日期失败!";
			mErrors.addOneError(tError);
			return false;
		}
		LCPolDB tLCPolDB = new LCPolDB();
		String tSQL = "select * from lcpol a where contno='" + mContNo + "' and polno=mainpolno" + " and exists (select 1 from lmriskapp " + " where riskcode=a.riskcode and risktype4='4') ";

		LCPolSet tLCPolSet = tLCPolDB.executeQuery(tSQL);
		if (tLCPolSet.size() != 1) {
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "inputData";
			tError.errorMessage = "获取个人险种数据失败!";
			mErrors.addOneError(tError);
			return false;
		}
		this.mLCPolSchema = tLCPolSet.get(1);
		this.mInsuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED, mLCPolSchema.getRiskCode());
		mEdorItemSpecialData = new EdorItemSpecialData(mLPEdorItemSchema);
		if (!mEdorItemSpecialData.query()) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取明细录入信息失败!";
			mErrors.addOneError(tError);
			return false;
		}
		String tFee = mEdorItemSpecialData.getEdorValue("FEE");
		if (tFee == null || tFee.equals("") || tFee.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取补费金额失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mFee = Arith.round(Double.parseDouble(tFee), 2);
		if (mFee == 0) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取补费失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mGetNoticeNo = mEdorItemSpecialData.getEdorValue("GETNOTICENO");
		if (mGetNoticeNo == null || mGetNoticeNo.equals("") || mGetNoticeNo.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取续期应收号失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mFeePayDate = mEdorItemSpecialData.getEdorValue("FEEPAYDATE");
		if (mFeePayDate == null || mFeePayDate.equals("") || mFeePayDate.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取续期收费日期失败!";
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/*
	 * 获取帐户相关信息
	 */
	private boolean getAccInfo() {
		// Accclass获取
		LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
		LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
		tLCInsureAccClassDB.setPolNo(mLCPolSchema.getPolNo());
		tLCInsureAccClassDB.setInsuAccNo(mInsuAccNo);
		tLCInsureAccClassSet = tLCInsureAccClassDB.query();
		if (tLCInsureAccClassSet.size() != 1) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getAccInfo++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getAccInfo";
			tError.errorMessage = "获取帐户信息1失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mLPInsureAccClassSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		mLPInsureAccClassSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		ref.transFields(mLPInsureAccClassSchema, tLCInsureAccClassSet.get(1));
		//
		LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
		LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
		tLCInsureAccDB.setPolNo(mLCPolSchema.getPolNo());
		tLCInsureAccDB.setInsuAccNo(mInsuAccNo);
		tLCInsureAccSet = tLCInsureAccDB.query();
		if (tLCInsureAccSet.size() != 1) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getAccInfo++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getAccInfo";
			tError.errorMessage = "获取帐户信息2失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mLPInsureAccSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		mLPInsureAccSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		ref.transFields(mLPInsureAccSchema, tLCInsureAccSet.get(1));

		LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
		LCInsureAccFeeSet tLCInsureAccFeeSet = new LCInsureAccFeeSet();
		tLCInsureAccFeeDB.setPolNo(mLCPolSchema.getPolNo());
		tLCInsureAccFeeDB.setInsuAccNo(mInsuAccNo);
		tLCInsureAccFeeSet = tLCInsureAccFeeDB.query();
		if (tLCInsureAccFeeSet.size() != 1) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getAccInfo++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getAccInfo";
			tError.errorMessage = "获取帐户信息3失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mLPInsureAccFeeSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		mLPInsureAccFeeSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		ref.transFields(mLPInsureAccFeeSchema, tLCInsureAccFeeSet.get(1));

		LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
		LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
		tLCInsureAccClassFeeDB.setPolNo(mLCPolSchema.getPolNo());
		tLCInsureAccClassFeeDB.setInsuAccNo(mInsuAccNo);
		tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
		if (tLCInsureAccClassFeeSet.size() != 1) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getAccInfo++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getAccInfo";
			tError.errorMessage = "获取帐户信息3失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mLPInsureAccClassFeeSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		mLPInsureAccClassFeeSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		ref.transFields(mLPInsureAccClassFeeSchema, tLCInsureAccClassFeeSet.get(1));

		return true;
	}

	/*
	 * 获取帐户相关信息
	 */
	private boolean getRateInfo() {
		String sql = " select a.* from LMInsuAccRate a " + "where 1 = 1 " + "   and RateType = 'C' " + "   and RateIntv = 1 " + "   and RateIntvUnit = 'Y' " + "   and a.BalaDate > '" + mFeePayDate
				+ "'   and a.BalaDate <= '" + mLPInsureAccSchema.getBalaDate() + "' " + "   and RiskCode = '" + mLCPolSchema.getRiskCode() + "' " + "   and InsuAccNo = '" + mInsuAccNo + "' "
				+ "order by a.BalaDate ";
		System.out.println(sql);
		LMInsuAccRateDB tLMInsuAccRateDB = new LMInsuAccRateDB();
		mLMInsuAccRateSet = tLMInsuAccRateDB.executeQuery(sql);
		return true;
	}

	/*
	 * 计算补费利息多少
	 */
	private boolean getFeeLX() {
		if (mLMInsuAccRateSet.size() == 0) {
//			当续期核销日之后没有月结的情况下,利息为0
			mFeeLX = 0;
			//续期核销日期是否在月结截止日期之后
			mIsLate=true;
		} else {
//			当续期核销日之后有月结的情况下,按月计算复利利息。
//	        获取算费代码
	        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
	        tLMCalModeDB.setRiskCode(mLPInsureAccSchema.getRiskCode());
	        tLMCalModeDB.setType("K");
	        LMCalModeSet set = tLMCalModeDB.query();
	        if(set.size() != 1)
	        {
	            // @@错误处理
				System.out.println("PEdorWXAppConfirmBL+getFeeLX++--");
				CError tError = new CError();
				tError.moduleName = "PEdorWXAppConfirmBL";
				tError.functionName = "getFeeLX";
				tError.errorMessage = "没有查询到账户收益算法!";
				mErrors.addOneError(tError);
				return false;
	        }
//	        先存上初始值,以后按月叠加复利
	        double tTraceMoney=mFee;
			for (int i = 1; i <= mLMInsuAccRateSet.size(); i++) {
				Calculator tCalculator = new Calculator();
				tCalculator.setCalCode(set.get(1).getCalCode());
				tCalculator.addBasicFactor("RateType", "1");
		        tCalculator.addBasicFactor("InsuAccNo", mLPInsureAccSchema.getInsuAccNo());
		        tCalculator.addBasicFactor("RiskCode", mLPInsureAccSchema.getRiskCode());
				tCalculator.addBasicFactor("TraceMoney", String.valueOf(tTraceMoney));
				if(i==1){
//					第一次续期核销日期为利息的起始日期
					tCalculator.addBasicFactor("TraceStartDate", mFeePayDate);
				}else{
//					第一次之后为月结起始日期
					tCalculator.addBasicFactor("TraceStartDate", mLMInsuAccRateSet.get(i).getStartBalaDate());
				}
				tCalculator.addBasicFactor("StartDate", mLMInsuAccRateSet.get(i).getStartBalaDate());
				tCalculator.addBasicFactor("EndDate", mLMInsuAccRateSet.get(i).getBalaDate());
				tTraceMoney=Double.parseDouble(tCalculator.calculate());
			}
			mFeeLX=Arith.round(tTraceMoney-mFee,2);
		}
		
		mLjsGetEndorMoneyLX=mFeeLX;
		
//		插入特殊数据表以备后续批单显示.
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchema.setEdorNo(mEdorNo);
		tLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorNo);
		tLPEdorEspecialDataSchema.setDetailType("FEELX");
		tLPEdorEspecialDataSchema.setEdorType("WX");
		tLPEdorEspecialDataSchema.setEdorValue(String.valueOf(mFeeLX));
		tLPEdorEspecialDataSchema.setPolNo("000000");
		mMap.put(tLPEdorEspecialDataSchema, SysConst.DELETE_AND_INSERT);
		return true;
	}

	/*
	 * 设置帐户补费轨迹
	 */
	private boolean setFeeInfo() {
		LCInsureAccFeeTraceSet tLCInsureAccFeeTraceSet = new LCInsureAccFeeTraceSet();
		LCInsureAccFeeTraceDB tLCInsureAccFeeTraceDB = new LCInsureAccFeeTraceDB();
		tLCInsureAccFeeTraceDB.setPolNo(mLCPolSchema.getPolNo());
		tLCInsureAccFeeTraceDB.setInsuAccNo(mInsuAccNo);
		tLCInsureAccFeeTraceDB.setOtherNo(mGetNoticeNo);
		tLCInsureAccFeeTraceDB.setOtherType("2");
		tLCInsureAccFeeTraceDB.setMoneyType("KF");
		tLCInsureAccFeeTraceSet = tLCInsureAccFeeTraceDB.query();
		if (tLCInsureAccFeeTraceSet.size() != 1) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+setFeeInfo++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "setFeeInfo";
			tError.errorMessage = "获取续期扣费轨迹表失败";
			mErrors.addOneError(tError);
			return false;
		}
		mLPInsureAccFeeTraceSchema.setEdorNo(mEdorNo);
		mLPInsureAccFeeTraceSchema.setEdorType(mEdorType);
		ref.transFields(mLPInsureAccFeeTraceSchema, tLCInsureAccFeeTraceSet.get(1));
		mLPInsureAccFeeTraceSchema.setFee(-mFee);
		String serialNo1 = PubFun1.CreateMaxNo("SERIALNO",mLPInsureAccTraceSchema.getManageCom());
		mLPInsureAccFeeTraceSchema.setSerialNo(serialNo1);
		if(mIsLate){
			mLPInsureAccFeeTraceSchema.setPayDate(mFeePayDate);	
			//在此处添加利息的后续计算日期 20110914 by xp
			mFeeLXDate=mFeePayDate;
		}else{
			mLPInsureAccFeeTraceSchema.setPayDate(mLPInsureAccSchema.getBalaDate());
			mFeeLXDate=mLPInsureAccSchema.getBalaDate();
		}
		mLPInsureAccFeeTraceSchema.setOtherNo(mEdorNo);
		mLPInsureAccFeeTraceSchema.setOtherType("10"); // 10是个单保全
		mLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
		mLPInsureAccFeeTraceSchema.setMakeDate(mCurrentDate);
		mLPInsureAccFeeTraceSchema.setMakeTime(mCurrentTime);
		mLPInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
		mLPInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
		mMap.put(mLPInsureAccFeeTraceSchema, SysConst.DELETE_AND_INSERT);

		LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
		LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
		tLCInsureAccTraceDB.setPolNo(mLCPolSchema.getPolNo());
		tLCInsureAccTraceDB.setInsuAccNo(mInsuAccNo);
		tLCInsureAccTraceDB.setOtherNo(mGetNoticeNo);
		tLCInsureAccTraceDB.setOtherType("2");
		tLCInsureAccTraceDB.setMoneyType("GL");
		tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
		if (tLCInsureAccTraceSet.size() != 1) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+setFeeInfo++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "setFeeInfo";
			tError.errorMessage = "获取续期扣费轨迹表失败";
			mErrors.addOneError(tError);
			return false;
		}
//		这块取错了,调整下.
//		mLjsGetEndorMoneyGL=tLCInsureAccTraceSet.get(1).getMoney();//存财务数据用
		mLjsGetEndorMoneyGL=mFee;
		
		mLPInsureAccTraceSchema.setEdorNo(mEdorNo);
		mLPInsureAccTraceSchema.setEdorType(mEdorType);
		ref.transFields(mLPInsureAccTraceSchema, tLCInsureAccTraceSet.get(1));
		mLPInsureAccTraceSchema.setMoney(mFee);
		String serialNo2 = PubFun1.CreateMaxNo("SERIALNO",mLPInsureAccTraceSchema.getManageCom());
		mLPInsureAccTraceSchema.setSerialNo(serialNo2);
		if(mIsLate){
			mLPInsureAccTraceSchema.setPayDate(mFeePayDate);
		}else{
			mLPInsureAccTraceSchema.setPayDate(mLPInsureAccSchema.getBalaDate());
		}
		mLPInsureAccTraceSchema.setOtherNo(mEdorNo);
		mLPInsureAccTraceSchema.setOtherType("10"); // 10是个单保全
		mLPInsureAccTraceSchema.setMoneyType("WX");
		mLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
		mLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
		mLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
		mLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
		mLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
		mMap.put(mLPInsureAccTraceSchema, SysConst.DELETE_AND_INSERT);
		return true;
	}

	/*
	 * 设置帐户P表合计后费用
	 */
	private boolean setAccInfo() {
		mLPInsureAccClassSchema.setInsuAccBala(mLPInsureAccClassSchema.getInsuAccBala() + mFee + mFeeLX);
		mLPInsureAccSchema.setInsuAccBala(mLPInsureAccSchema.getInsuAccBala() + mFee + mFeeLX);
		mLPInsureAccFeeSchema.setFee(mLPInsureAccFeeSchema.getFee() - mFee);
		mLPInsureAccClassFeeSchema.setFee(mLPInsureAccClassFeeSchema.getFee() - mFee);
		mMap.put(mLPInsureAccClassSchema, SysConst.DELETE_AND_INSERT);
		mMap.put(mLPInsureAccSchema, SysConst.DELETE_AND_INSERT);
		mMap.put(mLPInsureAccFeeSchema, SysConst.DELETE_AND_INSERT);
		mMap.put(mLPInsureAccClassFeeSchema, SysConst.DELETE_AND_INSERT);
		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		/*
		 * 3处轨迹中的moneytype如何设置需要和财务协商
		 */
		// 获取相关帐户表
		if (!getAccInfo()) {
			return false;
		}
//		需求调整,去掉利息计算 20110831
//		 获取相关利息表
		if (!getRateInfo()) {
			return false;
		}
		if (mLMInsuAccRateSet.size() == 0) {
//			当续期核销日之后没有月结的情况下,利息为0
			mFeeLX = 0;
			//续期核销日期是否在月结截止日期之后
			mIsLate=true;
		}
//		// 计算利息
//		if (!getFeeLX()) {
//			return false;
//		}
		// 设置补费轨迹
		if (!setFeeInfo()) {
			return false;
		}
		// 设置利息轨迹相关帐户表
//		if(mFeeLX!=0){
//		setLPAccTrace(mLPInsureAccClassSchema, mFeeLX, "WL");
//		}
		if (!setAccInfo()) {
			return false;
		}
		//设置财务数据
		if(!setLJSGetEndorseWX())
		{
			return false;
		}
		setEdorItem();
		
		//在此处添加利息的后续计算日期 20110914 by xp
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchemafee = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchemafee.setEdorAcceptNo(mEdorNo);
		tLPEdorEspecialDataSchemafee.setEdorNo(mEdorNo);
		tLPEdorEspecialDataSchemafee.setEdorType(mEdorType);
		tLPEdorEspecialDataSchemafee.setDetailType("FEELXDATE");
		tLPEdorEspecialDataSchemafee.setPolNo(BQ.FILLDATA);
		tLPEdorEspecialDataSchemafee.setEdorValue(mFeeLXDate);
		mMap.put(tLPEdorEspecialDataSchemafee, "DELETE&INSERT");
		return true;
	}

	/**
	 * 设置帐户金额轨迹
	 * 
	 * @param tLPInsureAccClassSchema
	 *            LPInsureAccClassSchema 轨迹对应的当前账户
	 * @param tMoney
	 *            double
	 * @param tMoneyType
	 *            String
	 * @param tAILPInsureAccClassSchema
	 *            LPInsureAccClassSchema 轨迹来源或者去向账户
	 */
	private void setLPAccTrace(LPInsureAccClassSchema tLPInsureAccClassSchema, double tMoney, String tMoneyType) {
		String serialNo = PubFun1.CreateMaxNo("SERIALNO",mLPInsureAccTraceSchema.getManageCom());
		LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
		tLPInsureAccTraceSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPInsureAccTraceSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPInsureAccTraceSchema.setGrpContNo(tLPInsureAccClassSchema.getGrpContNo());
		tLPInsureAccTraceSchema.setGrpPolNo(tLPInsureAccClassSchema.getGrpPolNo());
		tLPInsureAccTraceSchema.setContNo(tLPInsureAccClassSchema.getContNo());
		tLPInsureAccTraceSchema.setPolNo(tLPInsureAccClassSchema.getPolNo());
		tLPInsureAccTraceSchema.setSerialNo(serialNo);
		tLPInsureAccTraceSchema.setInsuAccNo(tLPInsureAccClassSchema.getInsuAccNo());
		tLPInsureAccTraceSchema.setRiskCode(tLPInsureAccClassSchema.getRiskCode());
		tLPInsureAccTraceSchema.setPayPlanCode(tLPInsureAccClassSchema.getPayPlanCode());
		tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
		tLPInsureAccTraceSchema.setOtherType("10"); // 10是个单保全
		tLPInsureAccTraceSchema.setAccAscription("0");
		tLPInsureAccTraceSchema.setMoneyType(tMoneyType);
		tLPInsureAccTraceSchema.setMoney(tMoney);
		tLPInsureAccTraceSchema.setUnitCount("0");
		tLPInsureAccTraceSchema.setPayDate(mLPInsureAccSchema.getBalaDate());
		tLPInsureAccTraceSchema.setState("0");
		tLPInsureAccTraceSchema.setManageCom(tLPInsureAccClassSchema.getManageCom());
		tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
		tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
		tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
		tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
		mMap.put(tLPInsureAccTraceSchema, SysConst.DELETE_AND_INSERT);
	}

	/**
	 * 设置批改补退费表
	 * 
	 * @return boolean
	 */

	 private boolean setLJSGetEndorseWX()
	    {
		 String sql="select * from lcpol a where  contno='"+mContNo+"' and polno=mainpolno and exists (select 1 from lmriskapp where risktype4='4' and riskcode =a.riskcode)";
		 LCPolDB tLCPolDB = new LCPolDB();
		 System.out.println(sql);
		 LCPolSet tLCPolSet=tLCPolDB.executeQuery(sql);
		 if(tLCPolSet==null||tLCPolSet.size()!=1)
		 {
			 // @@错误处理
				System.out.println("PEdorWXAppConfirmBL+getFeeLX++--");
				CError tError = new CError();
				tError.moduleName = "PEdorWXAppConfirmBL";
				tError.functionName = "getFeeLX";
				tError.errorMessage = "没有查询到万能主险信息!";
				mErrors.addOneError(tError);
				return false;
		 }
		 LPPolSchema  aLPPolSchema =new LPPolSchema();
		 LCPolSchema aLCPolSchema=tLCPolSet.get(1);	 
		 Reflections tRef = new Reflections();
		 tRef.transFields(aLPPolSchema, aLCPolSchema);
		 aLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		 aLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		 
		 
		 
  //一开始来初始化相关变量
	        BqCalBL bqCalBL = new BqCalBL();
	        String feeType1 = null;
	        String feeType2 = null;
	        
//	      处理管理费部分
	        if(mLjsGetEndorMoneyGL!=0){
	        if (mLjsGetEndorMoneyGL < 0) //补费
	        {
	            feeType1 = "BF";
	            feeType2 = "TB";
	        } else if (mLjsGetEndorMoneyGL > 0) //退费
	        {
	            feeType1 = "BF";
	            feeType2 = "TF";
	        }
	        LJSGetEndorseSchema tLJSGetEndorseSchemaGL1 = bqCalBL.initLJSGetEndorse(
	                mLPEdorItemSchema, aLPPolSchema, null, feeType2,
	                mLjsGetEndorMoneyGL,
	                mGlobalInput);
	        LJSGetEndorseSchema tLJSGetEndorseSchemaGL2 = bqCalBL.initLJSGetEndorse(
	                mLPEdorItemSchema, aLPPolSchema, null, feeType1,
	                -mLjsGetEndorMoneyGL,
	                mGlobalInput);
	        tLJSGetEndorseSchemaGL1.setManageCom(aLPPolSchema.getManageCom());
	        tLJSGetEndorseSchemaGL2.setManageCom(aLPPolSchema.getManageCom());
	        mMap.put(tLJSGetEndorseSchemaGL1, "DELETE&INSERT");
	        mMap.put(tLJSGetEndorseSchemaGL2, "DELETE&INSERT");
	        }
	        
//	      利息部分
//	        if(mLjsGetEndorMoneyLX!=0)
//	        {
//	            if (mLjsGetEndorMoneyLX < 0) //补费
//	            {
//	                feeType1 = "BF";
//	                feeType2 = "TB";
//	            } else if (mLjsGetEndorMoneyLX > 0) //退费
//	            {
//	                feeType1 = "BF";
//	                feeType2 = "TF";
//	            }
//	            LJSGetEndorseSchema tLJSGetEndorseSchema1LX = bqCalBL.initLJSGetEndorse(
//	                    mLPEdorItemSchema, aLPPolSchema, null, feeType2,
//	                    mLjsGetEndorMoneyLX,
//	                    mGlobalInput);
//	           
//	            tLJSGetEndorseSchema1LX.setManageCom(aLPPolSchema.getManageCom());
//	            
//	            mMap.put(tLJSGetEndorseSchema1LX, "DELETE&INSERT");
//	            
//	            }
//	        if((mLjsGetEndorMoneyGL+mLjsGetEndorMoneyLX)!=0)
//	        {
//	        	if ((mLjsGetEndorMoneyGL+mLjsGetEndorMoneyLX) < 0) //补费
//	            {
//	                feeType1 = "BF";
//	                feeType2 = "TB";
//	            } else if ((mLjsGetEndorMoneyGL+mLjsGetEndorMoneyLX) > 0) //退费
//	            {
//	                feeType1 = "BF";
//	                feeType2 = "TF";
//	            }
//	        	 LJSGetEndorseSchema tLJSGetEndorseSchema2LXGL = bqCalBL.initLJSGetEndorse(
//		                    mLPEdorItemSchema, aLPPolSchema, null, feeType1,
//		                    Math.abs(mLjsGetEndorMoneyLX+mLjsGetEndorMoneyGL),
//		                    mGlobalInput);
//	        	 tLJSGetEndorseSchema2LXGL.setManageCom(aLPPolSchema.getManageCom());
//	        	 mMap.put(tLJSGetEndorseSchema2LXGL, "DELETE&INSERT");
//	        }
	        
	        return true;
	    }
	
	
	
	
	
	/**
	 * 设置item表中的费用和状态
	 */
	private void setEdorItem() {
		String sql = "update LPEdorItem " + "set GetMoney = 0, " + "EdorState = '" + BQ.EDORSTATE_CAL + "', " + "ModifyDate = '" + mCurrentDate + "', " + "ModifyTime = '" + mCurrentTime + "' "
				+ "where EdorNo = '" + mEdorNo + "' " + "and EdorType = '" + mEdorType + "' ";
		mMap.put(sql.toString(), "UPDATE");
	}
}
