package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GEdorMJDetailUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = null;
    private GEdorMJDetailBL tGEdorMJDetailBL = new GEdorMJDetailBL();

    public GEdorMJDetailUI()
    {
    }

    /**
     * 公共的数据提交方法
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        if(!tGEdorMJDetailBL.submitData(data, operate))
        {
            mErrors = tGEdorMJDetailBL.mErrors;
            return false;
        }
        return true;
    }

    /**
    * 得到计算后的利息
    * @return TransferData
    */
   public TransferData getInterest()
   {
       return tGEdorMJDetailBL.getInterest();
   }

}
