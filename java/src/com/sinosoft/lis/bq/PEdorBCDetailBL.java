package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.util.HashMap;

public class PEdorBCDetailBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private String mOperate;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPEdorItemSet outLPEdorItemSet = new LPEdorItemSet();
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LPBnfSet mLPBnfSet = new LPBnfSet();

    private HashMap mHashMap = new HashMap();

    public boolean submitData(VData cInputData, String cOperate)
    {

        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);
        //数据查询业务处理(queryData())
        if (cOperate.equals("QUERY||MAIN") ||
                cOperate.equals("QUERY||FIRST") ||
                cOperate.equals("QUERY||PERSON"))
        {
            System.out.println("   ----- queryData");
            if (!queryData())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        //数据校验操作（checkdata)
        if (!checkData())
        {
            return false;
        }
        //数据准备操作（preparedata())
        if (!prepareData())
        {
            return false;
        }
        System.out.println("   ----- oper" + cOperate);
        // 数据操作业务处理(新增insertData();修改updateData();删除deletedata())
        if (cOperate.equals("INSERT||MAIN") ||
                cOperate.equals("INSERT||GRPMAIN") ||
                cOperate.equals("INSERT||PERSON"))
        {
            PubSubmit tPEdorBCBLS = new PubSubmit();
            System.out.println("-----------------------01");
            if (tPEdorBCBLS.submitData(mResult, "") == false)
            {
                //@@错误处理
                this.mErrors.copyAllErrors(tPEdorBCBLS.mErrors);
                return false;
            }
            System.out.println("-----------------------02");
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private boolean queryData()
    {
        if (mOperate.equals("QUERY||MAIN") ||
                mOperate.equals("QUERY||FIRST"))
        {
            LPBnfBL tLPBnfBL = new LPBnfBL();
            LPBnfSet tLPBnfSet = new LPBnfSet();
            System.out.println("   ----- start query0");

            //定义LPEdorMain表操作变量
            LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

            /*从LPEdorMain表中查找当前批改记录。通过CalCode字段判断是否保存过申请
             *依此来分别第一次查询和保存后的查询（包括又一次进入项目明细）。*/
            tLPEdorItemDB.setPolNo(mLPEdorItemSchema.getPolNo());
            tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
            tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
            tLPEdorItemSet = tLPEdorItemDB.query();
            System.out.println("   -----" + tLPEdorItemSet.size());

            if (tLPEdorItemSet.size() != 1)
            {
                //报错:当前批改信息记录个数有误！
                CError.buildErr(this, "个人批改表（LPEdorMain）:批改信息记录个数有误!");
                return false;
            }
            //得到当前保全申请的BC项目批改记录。
            tLPEdorItemSchema = tLPEdorItemSet.get(1);
            System.out.println("   ----- Now Query");
            tLPBnfSet = tLPBnfBL.queryLPBnf(tLPEdorItemSchema);

            if (tLPBnfSet.size() < 1)
            {
                System.out.println("   ----- There is no Data.");
                mInputData.clear();
                mResult.clear();
                return true;
            }
            String tReturn;
            tReturn = tLPBnfSet.encode();
            tReturn = "0|" + tLPBnfSet.size() + "^" + tReturn;
            System.out.println("tReturn: " + tReturn);
            mInputData.clear();
            mInputData.add(tLPBnfSet);
            mResult.clear();
            mResult.add(tReturn);
        }
        else if (mOperate.equals("QUERY||PERSON"))
        {
            String tag = "";
            System.out.println("   -----Start person query.");
            //先查p表
            LPBnfDB tLPBnfDB = new LPBnfDB();
            LPBnfSet tLPBnfSet = new LPBnfSet();
            String sql = "select * from lpbnf where  edorno='" +
                    mLPEdorItemSchema.getEdorNo() + "' and edortype='" +
                    mLPEdorItemSchema.getEdorType() + "' and insuredno='" +
                    mLPEdorItemSchema.getInsuredNo() + "'";
            tLPBnfSet = tLPBnfDB.executeQuery(sql);
            System.out.println("sql = " + sql);
            int size = tLPBnfSet.size();
            if (size > 0)
                tag = sql;
                //查p表有没有其它项目做过修改
            if (size == 0)
            {

                sql = "select * from lpbnf where  insuredno='" +
                        mLPEdorItemSchema.getInsuredNo() + "' and ContNo = '" +
                        mLPEdorItemSchema.getContNo() + "' and (MakeDate<'" +
                        mLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
                        mLPEdorItemSchema.getMakeDate() + "' and MakeTime<='" +
                        mLPEdorItemSchema.getMakeTime() +
                        "')) order by MakeDate,MakeTime desc";
                System.out.println("SQL:  ====   " + sql);
                tLPBnfSet = tLPBnfDB.executeQuery(sql);
                size = tLPBnfSet.size();
                if (size > 0)
                    tag = sql;
                    //再查c表
                if (size == 0)
                {
                    LCBnfDB tLCBnfDB = new LCBnfDB();
                    LCBnfSet tLCBnfSet = new LCBnfSet();
                    tLCBnfDB.setPolNo(mLPEdorItemSchema.getPolNo());
                    tLCBnfDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
                    tLCBnfSet = tLCBnfDB.query();
                    size = tLCBnfSet.size();
                    if (size != 0)
                    {
                        tag = "get Data from LCBnf.......................";
                        for (int i = 1; i <= tLCBnfSet.size(); i++)
                        {
                            LPBnfSchema tempSchema = new LPBnfSchema();
                            Reflections ref = new Reflections();
                            ref.transFields(tempSchema, tLCBnfSet.get(i));
                            tempSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
                            tempSchema.setEdorType(mLPEdorItemSchema.
                                    getEdorType());
                            tLPBnfSet.add(tempSchema);
                        }
                    }
                }
            }

            if (tLPBnfSet.size() < 1)
            {
                System.out.println("   ----- There is no Data.");
                mInputData.clear();
                mResult.clear();
                return true;
            }
            String tReturn;
            tReturn = tLPBnfSet.encode();
            tReturn = "0|" + tLPBnfSet.size() + "^" + tReturn;
            System.out.println("tReturn: " + tReturn);
            mInputData.clear();
            mInputData.add(tLPBnfSet);
            mResult.clear();
            mResult.add(tReturn);
            System.out.println("tag -=-=-=-=-=-=-=-=-=-=-=-= " + tag);
        }
        return true;
    }

    private void getInputData(VData cInputData)
    {
        mLPBnfSet = (LPBnfSet) cInputData.getObjectByObjectName("LPBnfSet", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema",
                0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0);
    }

    private boolean checkData()
    {
        boolean flag = true;
        if (mOperate.equals("INSERT||MAIN"))
        {
            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            LPEdorItemSchema tempLPEdorItemSchema = new LPEdorItemSchema();
            tempLPEdorItemSchema.setSchema(mLPEdorItemSchema);
            tLPEdorItemDB.setSchema(tempLPEdorItemSchema);
            if (!tLPEdorItemDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "PBnfBL";
                tError.functionName = "Preparedata";
                tError.errorMessage = "无保全申请数据!";
                System.out.println("!! " + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
            mLPEdorItemSchema.setSchema(tLPEdorItemDB.getSchema());
            if (tLPEdorItemDB.getEdorState().trim().equals(BQ.EDORSTATE_CAL))
            {
                CError tError = new CError();
                tError.moduleName = "PBnfBL";
                tError.functionName = "Preparedata";
                tError.errorMessage = "该保全已经申请确认不能修改!";
                System.out.println("!! " + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else if (mOperate.equals("INSERT||PERSON"))
        {
            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            //      tLPEdorItemDB.setSchema(mLPEdorItemSchema);
            tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
            tLPEdorItemDB.setPolNo("000000");
            tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
            LPEdorItemSet testLPEdorItemSet = new LPEdorItemSet();
            testLPEdorItemSet = tLPEdorItemDB.query();
            //System.out.println("ttttttttt");
            if (testLPEdorItemSet.size() < 1)
            {
                CError tError = new CError();
                tError.moduleName = "PBnfBL";
                tError.functionName = "Preparedata";
                tError.errorMessage = "无保全申请数据!";
                System.out.println("!! " + tError);
                this.mErrors.addOneError(tError);
                return false;
            }

            mLPEdorItemSchema.setSchema(testLPEdorItemSet.get(1));
            if (mLPEdorItemSchema.getEdorState().trim().equals(BQ.EDORSTATE_CONFIRM))
            {
                CError tError = new CError();
                tError.moduleName = "PBnfBL";
                tError.functionName = "Preparedata";
                tError.errorMessage = "该保全已经申请确认不能修改!";
                System.out.println("!! " + tError);
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        if (mLPBnfSet.size() > 0)
        {
            LPBnfSet tLPBnfSet = new LPBnfSet();
            for (int i = 1; i <= mLPBnfSet.size(); i++)
            {
                LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                if ((mLPBnfSet.get(i).getName() == null) ||
                        mLPBnfSet.get(i).getName().trim().equals(""))
                {
                    this.mErrors.addOneError("受益人姓名不能为空!");
                    return false;
                }
                if ((mLPBnfSet.get(i).getCustomerNo() != null) &&
                        !mLPBnfSet.get(i).getCustomerNo().trim().equals(""))
                {
                    LDPersonDB tLDPersonDB = new LDPersonDB();
                    tLDPersonDB.setCustomerNo(mLPBnfSet.get(i).getCustomerNo());
                    if (!tLDPersonDB.getInfo())
                    {
                        CError tError = new CError();
                        tError.moduleName = "PBnfBL";
                        tError.functionName = "checkdata";
                        tError.errorMessage = "客户号有误,请重新输入!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    else
                    {
                        tLDPersonSchema = tLDPersonDB.getSchema();
                    }
                }
                if (i == 1)
                {
                    tLPBnfSet.add(mLPBnfSet.get(i).getSchema());
                }
                else
                {
                    boolean tBnfFlag = false;
                    for (int j = 1; j <= tLPBnfSet.size(); j++)
                    {
                        if (mLPBnfSet.get(i).getPolNo().equals(
                                tLPBnfSet.get(j).getPolNo()) &&
                                mLPBnfSet.get(i).getBnfType().equals(
                                tLPBnfSet.get(j).getBnfType()) &&
                                mLPBnfSet.get(i).getBnfGrade().equals(
                                tLPBnfSet.get(j).getBnfGrade()))
                        {
                            tLPBnfSet.get(j).setBnfLot(mLPBnfSet.get(i)
                                    .getBnfLot() +
                                    tLPBnfSet.get(j).getBnfLot());
                            tBnfFlag = true;
                        }
                    }
                    if (!tBnfFlag)
                    {
                        tLPBnfSet.add(mLPBnfSet.get(i).getSchema());
                    }
                }
            }
            for (int i = 1; i <= tLPBnfSet.size(); i++)
            {
                if (PubFun.setPrecision(tLPBnfSet.get(i).getBnfLot(), "00") != 1)
                {
                    CError tError = new CError();
                    tError.moduleName = "PBnfBL";
                    tError.functionName = "checkdata";
                    tError.errorMessage = "受益级别" +
                            tLPBnfSet.get(i).getBnfGrade() + "之和不等于一!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }

            if(!checkBnfWhenSelf(mLPBnfSet))
            {
                return false;
            }
        }
        return flag;
    }

    //校验受益人为本人时录入信息是否与被保人一致
    private boolean checkBnfWhenSelf(LPBnfSet tLPBnfSet)
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mLPEdorItemSchema.getContNo());
        tLCInsuredDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        if(!tLCInsuredDB.getInfo())
        {
            mErrors.addOneError("没有查询到被保人信息。");
            return false;
        }

        String errMsg = "";
        for (int i = 1; i <= tLPBnfSet.size(); i++)
        {
            if(!(tLPBnfSet.get(i).getRelationToInsured().equals("00")))
            {
                continue;
            }
            if(tLPBnfSet.get(i).getBnfType().equals("1"))
            {
                mErrors.addOneError("身故受益人不能为本人。");
                return false;
            }
            if (!(StrTool.unicodeToGBK(tLPBnfSet.get(i).getName())
                  .equals(StrTool.cTrim(tLCInsuredDB.getName()))))
            {
                errMsg += "录入的姓名与原姓名不相符，";
            }
            if (!(tLPBnfSet.get(i).getIDType()
                  .equals(StrTool.cTrim(tLCInsuredDB.getIDType()))))
            {
                errMsg += "证件类型与原证件类型不相符，";
            }
            if (!(StrTool.unicodeToGBK(tLPBnfSet.get(i).getIDNo())
                  .equals(StrTool.cTrim(tLCInsuredDB.getIDNo()))))
            {
                errMsg += "证件号码与原证件号码不相符，";
            }
            if (!(StrTool.unicodeToGBK(tLPBnfSet.get(i).getSex())
                    .equals(StrTool.cTrim(tLCInsuredDB.getSex()))))
              {
                  errMsg += "性别与原性别不相符，";
              }
            if (!(StrTool.unicodeToGBK(tLPBnfSet.get(i).getBirthday())
                    .equals(StrTool.cTrim(tLCInsuredDB.getBirthday()))))
              {
                  errMsg += "出生日期与原出生日期不相符，";
              }

            if(!errMsg.equals(""))
            {
                mErrors.addOneError("您录入了本人为受益人，但是" + errMsg
                    + "请确认.");
                return false;
            }
        }

        return true;
    }

    private boolean prepareData()
    {
        LPBnfSet aLPBnfSet = new LPBnfSet();
        //准备个人批改受益人的信息
        for (int i = 1; i <= mLPBnfSet.size(); i++)
        {
            LPBnfSchema tLPBnfSchema = new LPBnfSchema();
            tLPBnfSchema.setSchema(mLPBnfSet.get(i));
            tLPBnfSchema.setBnfNo(getNextBnfNo(mLPBnfSet.get(i)));
            tLPBnfSchema.setContNo(mLPEdorItemSchema.getContNo());
            tLPBnfSchema.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
            tLPBnfSchema.setOperator(mGlobalInput.Operator);
            tLPBnfSchema.setMakeDate(PubFun.getCurrentDate());
            tLPBnfSchema.setMakeTime(PubFun.getCurrentTime());
            tLPBnfSchema.setModifyDate(PubFun.getCurrentDate());
            tLPBnfSchema.setModifyTime(PubFun.getCurrentTime());
            aLPBnfSet.add(tLPBnfSchema);
            System.out.println("Bnflots : " + tLPBnfSchema.getBnfLot());
        }
        //设置个儿保全主表到保全申请状态
        setLPEdorItem();
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPGrpEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPEdorItemSchema.getGrpContNo());
        MMap map = new MMap();
        if (tLPGrpEdorItemDB.getInfo())
        {
            LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
            tLPGrpEdorItemSchema = tLPGrpEdorItemDB.getSchema();
            tLPGrpEdorItemSchema.setEdorState("1");
            map.put(tLPGrpEdorItemSchema, "DELETE&INSERT");
        }
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorMainDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorMainDB.setContNo(mLPEdorItemSchema.getContNo());
        if (tLPEdorMainDB.getInfo())
        {
            LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
            tLPEdorMainSchema = tLPEdorMainDB.getSchema();
            tLPEdorMainSchema.setEdorState("1");
            map.put(tLPEdorMainSchema, "DELETE&INSERT");
        }
        mInputData.clear();

        map.put(outLPEdorItemSet, "DELETE&INSERT");
        String sql = "delete from LPBnf " +
                "where edorNo = '" + mLPEdorItemSchema.getEdorNo() + "' " +
                "and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' " +
                "and ContNo = '" + mLPEdorItemSchema.getContNo() + "' " +
                "and InsuredNo = '" + mLPEdorItemSchema.getInsuredNo() + "'";
        map.put(sql, "DELETE");
        map.put(aLPBnfSet, "INSERT");
        mResult.add(map);
        return true;
    }

    /**
     * 得到险种PolNo的下一受益人编码
     * @param cPolNo String
     * @return String
     */
    private int getNextBnfNo(LPBnfSchema tLPBnfSchema)
    {
        //若没受益人没有变更，则使用原来的序号
        LCBnfDB tLCBnfDB = new LCBnfDB();
        tLCBnfDB.setPolNo(tLPBnfSchema.getPolNo());
        tLCBnfDB.setName(tLPBnfSchema.getName());
        tLCBnfDB.setIDType(tLPBnfSchema.getIDType());
        tLCBnfDB.setIDNo(tLPBnfSchema.getIDNo());
        tLCBnfDB.setRelationToInsured(tLPBnfSchema.getRelationToInsured());
        LCBnfSet set = tLCBnfDB.query();
        if(set.size() > 0)
        {
            return set.get(1).getBnfNo();
        }

        String nextBnfNo = (String) mHashMap.get(tLPBnfSchema.getPolNo());
        int nextBnfNoInt = 0;

        if(nextBnfNo != null)
        {
            nextBnfNoInt = Integer.parseInt(nextBnfNo) + 1;
            mHashMap.remove(tLPBnfSchema.getPolNo());
            mHashMap.put(tLPBnfSchema.getPolNo(), String.valueOf(nextBnfNoInt));
            return nextBnfNoInt;
        }

        String sql = "select max(a) from( "
                     + " select max(BnfNo) a "
                     + "from LCBnf "
                     + "where PolNo = '" + tLPBnfSchema.getPolNo() + "' "
                     + "union "
                     + " select max(BnfNo) a "
                     + "from LBBnf "
                     + "where PolNo = '" + tLPBnfSchema.getPolNo() + "' "
                     + ") t ";
        String maxBnfNo = new ExeSQL().getOneValue(sql);
        if(maxBnfNo.equals("") || maxBnfNo.equals("null"))
        {
            nextBnfNoInt = 1;
        }
        else
        {
            nextBnfNoInt = Integer.parseInt(maxBnfNo) + 1;
        }

        mHashMap.put(tLPBnfSchema.getPolNo(), String.valueOf(nextBnfNoInt));
        return nextBnfNoInt;
    }

    private void setLPEdorItem()
    {
        String strEdorType = "";
        mLPEdorItemSchema.setEdorState("1"); //批改申请
        mLPEdorItemSchema.setEdorAppDate(PubFun.getCurrentDate());
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
        //设置修改标记(保存申请)
        strEdorType = mLPEdorItemSchema.getEdorType();
        System.out.println("==> Change Flag of Chenged, Flag = " + strEdorType +
                "YESC");
        outLPEdorItemSet.add(mLPEdorItemSchema);
    }

    public static void main(String[] a)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo("20070428000001");
        LPEdorItemSet set = tLPEdorItemDB.query();

        LPBnfSet tLPBnfSet = new LPBnfSet();
        LPBnfSchema tLPBnfSchema = new LPBnfSchema();
        tLPBnfSchema.setPolNo("21000192749");
        tLPBnfSchema.setEdorNo(tLPEdorItemDB.getEdorAcceptNo());
        tLPBnfSchema.setEdorType(set.get(1).getEdorType());
        tLPBnfSchema.setBnfType("0");
        tLPBnfSchema.setName("52010111003");
        tLPBnfSchema.setIDType("3");
        tLPBnfSchema.setIDNo("");
        tLPBnfSchema.setRelationToInsured("12");
        tLPBnfSchema.setBnfGrade("1");
        tLPBnfSchema.setBnfLot("0.5");
        tLPBnfSet.add(tLPBnfSchema);

        LPBnfSchema tLPBnfSchema2 = new LPBnfSchema();
        tLPBnfSchema2.setPolNo(tLPBnfSchema.getPolNo());
        tLPBnfSchema2.setEdorNo(tLPEdorItemDB.getEdorAcceptNo());
        tLPBnfSchema2.setEdorType(set.get(1).getEdorType());
        tLPBnfSchema2.setBnfType(tLPBnfSchema.getBnfType());
        tLPBnfSchema2.setName("ff");
        tLPBnfSchema2.setIDType("2");
        tLPBnfSchema2.setIDNo("");
        tLPBnfSchema2.setRelationToInsured("23");
        tLPBnfSchema2.setBnfGrade("1");
        tLPBnfSchema2.setBnfLot("0.2");
        tLPBnfSet.add(tLPBnfSchema2);
//
//        LPBnfSchema tLPBnfSchema3 = new LPBnfSchema();
//        tLPBnfSchema3.setPolNo(tLPBnfSchema.getPolNo());
//        tLPBnfSchema3.setEdorNo(tLPEdorItemDB.getEdorAcceptNo());
//        tLPBnfSchema3.setEdorType(set.get(1).getEdorType());
//        tLPBnfSchema3.setBnfType(tLPBnfSchema.getBnfType());
//        tLPBnfSchema3.setName("ff");
//        tLPBnfSchema3.setIDType("2");
//        tLPBnfSchema3.setIDNo("");
//        tLPBnfSchema3.setRelationToInsured("23");
//        tLPBnfSchema3.setBnfGrade("1");
//        tLPBnfSchema3.setBnfLot("0.3");
//        tLPBnfSet.add(tLPBnfSchema3);
//
////---------------------------------------------
//        LPBnfSchema tLPBnfSchema4 = new LPBnfSchema();
//        tLPBnfSchema4.setPolNo(tLPBnfSchema.getPolNo());
//        tLPBnfSchema4.setEdorNo(tLPEdorItemDB.getEdorAcceptNo());
//        tLPBnfSchema4.setEdorType(set.get(1).getEdorType());
//        tLPBnfSchema4.setBnfType("1");
//        tLPBnfSchema4.setName("ff");
//        tLPBnfSchema4.setIDType("2");
//        tLPBnfSchema4.setIDNo("");
//        tLPBnfSchema4.setRelationToInsured("23");
//        tLPBnfSchema4.setBnfGrade("1");
//        tLPBnfSchema4.setBnfLot("0.2");
//        tLPBnfSet.add(tLPBnfSchema4);
//
//        LPBnfSchema tLPBnfSchema5 = new LPBnfSchema();
//        tLPBnfSchema5.setPolNo(tLPBnfSchema.getPolNo());
//        tLPBnfSchema5.setEdorNo(tLPEdorItemDB.getEdorAcceptNo());
//        tLPBnfSchema5.setEdorType(set.get(1).getEdorType());
//        tLPBnfSchema5.setBnfType("1");
//        tLPBnfSchema5.setName("ff");
//        tLPBnfSchema5.setIDType("2");
//        tLPBnfSchema5.setIDNo("");
//        tLPBnfSchema5.setRelationToInsured("23");
//        tLPBnfSchema5.setBnfGrade("1");
//        tLPBnfSchema5.setBnfLot("0.5");
//        tLPBnfSet.add(tLPBnfSchema5);
//
//        LPBnfSchema tLPBnfSchema6 = new LPBnfSchema();
//        tLPBnfSchema6.setPolNo(tLPBnfSchema.getPolNo());
//        tLPBnfSchema6.setEdorNo(tLPEdorItemDB.getEdorAcceptNo());
//        tLPBnfSchema6.setEdorType(set.get(1).getEdorType());
//        tLPBnfSchema6.setBnfType("1");
//        tLPBnfSchema6.setName("ff");
//        tLPBnfSchema6.setIDType("2");
//        tLPBnfSchema6.setIDNo("");
//        tLPBnfSchema6.setRelationToInsured("23");
//        tLPBnfSchema6.setBnfGrade("1");
//        tLPBnfSchema6.setBnfLot("0.2");
//        tLPBnfSet.add(tLPBnfSchema6);
//
//        LPBnfSchema tLPBnfSchema7 = new LPBnfSchema();
//        tLPBnfSchema7.setPolNo(tLPBnfSchema.getPolNo());
//        tLPBnfSchema7.setEdorNo(tLPEdorItemDB.getEdorAcceptNo());
//        tLPBnfSchema7.setEdorType(set.get(1).getEdorType());
//        tLPBnfSchema7.setBnfType("1");
//        tLPBnfSchema7.setName("ff");
//        tLPBnfSchema7.setIDType("2");
//        tLPBnfSchema7.setIDNo("");
//        tLPBnfSchema7.setRelationToInsured("23");
//        tLPBnfSchema7.setBnfGrade("1");
//        tLPBnfSchema7.setBnfLot("0.1");
//        tLPBnfSet.add(tLPBnfSchema7);



        VData tVData = new VData();
        tVData.addElement(g);
        tVData.addElement(set.get(1));
        tVData.addElement(tLPBnfSet);

        PEdorBCDetailBL bl = new PEdorBCDetailBL();
        bl.submitData(tVData, "");
        if(!bl.mErrors.needDealError())
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}
