package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author not attributable
 * @version 1.1
 */
public class GEdorMJReturnCalUI
{
    public CErrors mErrors = null;
    private GEdorMJReturnCalBL mGEdorMJReturnCalBL = null;

    public GEdorMJReturnCalUI()
    {
    }

    public GEdorMJReturnCalUI(String edorNo, String edorType, String grpPolNo)
    {
        mGEdorMJReturnCalBL = new GEdorMJReturnCalBL(edorNo, edorType, grpPolNo);
    }

    public boolean submitData()
    {
        if(!mGEdorMJReturnCalBL.submitData())
        {
            mErrors = mGEdorMJReturnCalBL.mErrors;
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        GEdorMJReturnCalUI gedormjreturncalui = new GEdorMJReturnCalUI();
    }
}
