package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

//程序名称：PEdorLQAppConfirm.java
//程序功能：
//创建日期：2008-04-10
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
public class PEdorLQAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = null;

    /** 合同号 */
    private String mContNo = null;

    /** 保单险种号 */
    private String mPolNo = null;

    /** 险种代码 */
    private String mRiskCode = null;

    /** 保单险种 */
    private LCPolSchema mLCPolSchema = null;

    /** 保全生效日期 */
    private String mEdorValiDate = null;

    /** 保全交费总额 */
    private double mGetMoney = 0.00;

    /** 保全项目特殊数据 */
    private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = null;

    private DetailDataQuery mQuery = null;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mVtsData = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private LCInsureAccSchema mLCInsureAccSchema = null;

    private LCInsureAccClassSchema mLCInsureAccClassSchema = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            System.out.println("PEdorLQAppConfirm.java->getInputData(cInputData)失败");
        	return false;
        }

        if (!dealData())
        {
        	System.out.println("PEdorLQAppConfirm.java->dealData()失败");
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema)cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
        mEdorNo = mLPEdorItemSchema.getEdorNo();
        mEdorType = mLPEdorItemSchema.getEdorType();
        mContNo = mLPEdorItemSchema.getContNo();
        mEdorValiDate = mLPEdorItemSchema.getEdorValiDate();
        mPolNo = mLPEdorItemSchema.getPolNo();
        String sql = "select * from LCPol a where polNo = '" + mLPEdorItemSchema.getPolNo() + "' "
                   + "and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') "
                   + "with ur";
        System.out.println("查询万能险种是否存在：" + sql);
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolSet = tLCPolDB.executeQuery(sql);
        if (tLCPolSet.size() == 0)
        {
         mErrors.addOneError("找不到万能险种信息！");
         return false;
        }
        else
        {
        	mPolNo = tLCPolSet.get(1).getPolNo();
        	mRiskCode = tLCPolSet.get(1).getRiskCode();
        }
        mLCPolSchema = tLCPolSet.get(1).getSchema();
        mLCInsureAccSchema = CommonBL.getLCInsureAcc(mPolNo,CommonBL.getInsuAccNo("002", mRiskCode));
        if (mLCInsureAccSchema == null)
        {
            mErrors.addOneError("找不到万能账户信息！");
            return false;
        }

        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setPolNo(mPolNo);
        tLCInsureAccClassDB.setInsuAccNo(mLCInsureAccSchema.getInsuAccNo());
        mLCInsureAccClassSchema = tLCInsureAccClassDB.query().get(1);
        if (mLCInsureAccClassSchema == null)
        {
        	mErrors.addOneError("找不到万能账户分类！");
            return false;
        }

        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(mEdorNo);
        tLPEdorEspecialDataDB.setEdorType(mEdorType);
        tLPEdorEspecialDataDB.setDetailType("AppMoney");
        mVtsData = new EdorItemSpecialData(mLPEdorItemSchema);
        mLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
        mLPEdorEspecialDataSchema = tLPEdorEspecialDataDB.query().get(1);
        mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!setInsuredAcc())
        {
            return false;
        }
        setEdorItem();
        return true;
    }

    /**
     * 设置被保人账户保费金额
     * @return boolean
     */
    private boolean setInsuredAcc()
    {
        String sql = "select year(EdorValidate-'" + mLCPolSchema.getCValiDate() + "')+1 "
                   + "from LPEdorItem where EdorNo = '" + mEdorNo +"' "
                   + "and EdorType = '" + mEdorType + "' and ContNo = '" + mContNo + "'";
        String tPolYear =  new ExeSQL().getOneValue(sql);
        
        String sqlprtno = " select prtno "
        + " from lccont where "
        + " ContNo = '" + mContNo + "' ";
           String tprtno =  new ExeSQL().getOneValue(sqlprtno);
        
//        int tIntPolYear = tPolYear>=6 ? 0 : 6-Integer.parseInt(tPolYear);
        //modify by fuxin  2009-1-5 根据描述获得比例。
        LMPolDutyEdorCalSchema edorCalSchema = getEdorCal(mRiskCode);
        Calculator tCalculator = new Calculator();
        tCalculator.setCalCode(edorCalSchema.getChgPremCalCode());
        
        double tmoney=0;
        //modify 20150608 加上福泽B款
        if(edorCalSchema.getChgPremCalCode()!=null&&(edorCalSchema.getChgPremCalCode().equals("320701")||edorCalSchema.getChgPremCalCode().equals("361701")||edorCalSchema.getChgPremCalCode().equals("399701") ))
        {
        	try{
        		String sqlPolYear = "select year(current date -'" + mLCPolSchema.getCValiDate() + "')+1 "
        						+ "from LPEdorItem where EdorNo = '" + mEdorNo +"' "
        						+ "and EdorType = '" + mEdorType + "' and ContNo = '" + mContNo + "'";
        	       	
        		tPolYear =  new ExeSQL().getOneValue(sqlPolYear);
        	
        		String tSumMoneySql = "select COALESCE(sum(-lja.getmoney),0)+"+mLPEdorEspecialDataSchema.getEdorValue()+" from  lcpol lcp ,ljagetendorse lja" +
        							"	where  lcp.polno = lja.polno " +
        							"	  and lja.contno = '" + mContNo + "' " +
        							" and lja.polno = '"+mPolNo+"' and lja.feeoperationtype = 'LQ' " +
        							"	and lja.makedate between (lcp.cvalidate +( "+tPolYear+" -1 ) year) " +
        									"	and (lcp.cvalidate +( "+tPolYear+"  ) year)  " +
        							"	with ur ";
        	
        		
        		String tInitMoneySql = "select COALESCE(sum(lcinacc.money),0) from  lcpol lcp ,lcinsureacctrace lcinacc" +
									"	where  lcp.polno = lcinacc.polno " +
									"	  and lcinacc.contno = '" + mContNo + "' " +
									" and lcinacc.polno = '"+mPolNo+"'  " +
									"	and lcinacc.PayDate < (lcp.cvalidate +( "+tPolYear+" -1 ) year) " +
									"	with ur ";
        		if("1".equals(tPolYear)){
        			tInitMoneySql = "select COALESCE(sum(lcinacc.money),0) from  lcpol lcp ,lcinsureacctrace lcinacc" +
							"	where  lcp.polno = lcinacc.polno " +
							"	  and lcinacc.contno = '" + mContNo + "' " +
							" and lcinacc.polno = '"+mPolNo+"'  " +
							"	and lcinacc.PayDate <= (lcp.cvalidate +( "+tPolYear+" -1 ) year) " +
							"	with ur ";
        		}
        	
        		
        		String tCurYearSumLQMoney =  new ExeSQL().getOneValue(tSumMoneySql);
        		String tCurYearInitMoney =  new ExeSQL().getOneValue(tInitMoneySql);
        		tCalculator.addBasicFactor("IntPolYear",tPolYear);
        		tCalculator.addBasicFactor("CurYearSumLQMoney",tCurYearSumLQMoney);
        		tCalculator.addBasicFactor("CurYearInitMoney",tCurYearInitMoney);
        		double tGetMoeny = Double.parseDouble(tCurYearSumLQMoney)-(Double.parseDouble(tCurYearInitMoney)*0.3)-Double.parseDouble(mLPEdorEspecialDataSchema.getEdorValue());
        		if(tGetMoeny<0){
        			tmoney = Double.parseDouble(tCurYearSumLQMoney)-(Double.parseDouble(tCurYearInitMoney)*0.3);
        		}
        		else{
        			tmoney = Double.parseDouble(mLPEdorEspecialDataSchema.getEdorValue());
        		}
        	} catch (Exception ex) {
        		ex.printStackTrace();
        		mErrors.addOneError("查询" + mContNo + "保单的账户信息时出错！");
                return false;
        	}
        }
        tCalculator.addBasicFactor("IntPolYear",tPolYear);
//        if(mRiskCode.equals("332901")) // 功能 #641: 《健康人生个人护理保险（万能型，G款）》定义功能 #663部分领取，解约扣费比例录入相关保全程序调整
//        {
        	tCalculator.addBasicFactor("prtno", tprtno);	
//        }
        tCalculator.addBasicFactor("PolNo",mPolNo); 
        String tStr = tCalculator.calculate();
        double feeRate = Double.parseDouble(tStr);
        double insuredMoney = Double.parseDouble(mLPEdorEspecialDataSchema.getEdorValue());
        double manageMoney = CommonBL.carry(insuredMoney * feeRate);  //计算管理费
        if(edorCalSchema.getChgPremCalCode()!=null&&(edorCalSchema.getChgPremCalCode().equals("320701")||edorCalSchema.getChgPremCalCode().equals("361701")||edorCalSchema.getChgPremCalCode().equals("399701")))
        {
        	
        	manageMoney = CommonBL.carry(tmoney * feeRate);  //计算管理费
        }
        //添加现有程序对于固定管理费的险种部分领取的支持 100120 by xp
        if(edorCalSchema.getChgPremCalCode()!=null&&edorCalSchema.getChgPremCalCode().equals("241701"))
        {
//        	331801险种的部分领取当前保单年度内的前三次免费,之后每次固定20元 100125 by xp
            String tSQL1="select count(distinct a.edorno) from lpedoritem a,lccont b where 1=1 "+
			" and (b.cvalidate +( "+tPolYear+" -1 ) year)<=a.edorvalidate "+
			" and (b.cvalidate +( "+tPolYear+"  ) year - 1 day)>=a.edorvalidate "+
			" and a.contno=b.contno and a.edortype='LQ' and b.contno=  '"+mContNo+"'"+
			" and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate='0') with ur";
            double tCount=Double.parseDouble(new ExeSQL().getOneValue(tSQL1));
            if(tCount<2.5)
            {
            	manageMoney=0;
            }
            else
            {
            	manageMoney=feeRate;
            }
        }
        double leftMoney = CommonBL.carry(mLCInsureAccSchema.getInsuAccBala() - insuredMoney); //计算余额
        double getMoney = insuredMoney - manageMoney;  //计算实领金额
        if (leftMoney < 0)
        {
            mErrors.addOneError("被保人" + mLCInsureAccSchema.getInsuredNo() + "的账户余额不足！");
            return false;
        }
        LCPolSchema tLCPolSchema = CommonBL.getLCPol(mPolNo);
        if (tLCPolSchema == null)
        {
            mErrors.addOneError("找不到客户" + mLCInsureAccSchema.getInsuredNo() + "的险种信息！");
            return false;
        }
        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema1 = new LPEdorEspecialDataSchema();
        tLPEdorEspecialDataSchema1.setEdorAcceptNo(mEdorNo);
        tLPEdorEspecialDataSchema1.setEdorNo(mEdorNo);
        tLPEdorEspecialDataSchema1.setEdorType(mEdorType);
        tLPEdorEspecialDataSchema1.setDetailType("LeftMoney");
        tLPEdorEspecialDataSchema1.setPolNo(mPolNo);
        tLPEdorEspecialDataSchema1.setEdorValue(String.valueOf(leftMoney));

        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema2 = new LPEdorEspecialDataSchema();
        tLPEdorEspecialDataSchema2.setEdorAcceptNo(mEdorNo);
        tLPEdorEspecialDataSchema2.setEdorNo(mEdorNo);
        tLPEdorEspecialDataSchema2.setEdorType(mEdorType);
        tLPEdorEspecialDataSchema2.setDetailType("ManageMoney");
        tLPEdorEspecialDataSchema2.setPolNo(mPolNo);
        tLPEdorEspecialDataSchema2.setEdorValue(String.valueOf(manageMoney));

        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema3 = new LPEdorEspecialDataSchema();
        tLPEdorEspecialDataSchema3.setEdorAcceptNo(mEdorNo);
        tLPEdorEspecialDataSchema3.setEdorNo(mEdorNo);
        tLPEdorEspecialDataSchema3.setEdorType(mEdorType);
        tLPEdorEspecialDataSchema3.setDetailType("GetMoney");
        tLPEdorEspecialDataSchema3.setPolNo(mPolNo);
        tLPEdorEspecialDataSchema3.setEdorValue(String.valueOf(getMoney));
        mMap.put(tLPEdorEspecialDataSchema1, "DELETE&INSERT");
        mMap.put(tLPEdorEspecialDataSchema2, "DELETE&INSERT");
        mMap.put(tLPEdorEspecialDataSchema3, "DELETE&INSERT");

        setLPInsureAcc(mLCInsureAccClassSchema, insuredMoney, leftMoney);
        setLPInsureAccTrace(mLCInsureAccClassSchema, insuredMoney);
        setLPInsureAccClass(mLCInsureAccClassSchema, insuredMoney, leftMoney);
        setLPInsureAccFee(mLCInsureAccClassSchema, manageMoney);
        setLPInsureAccFeeTrace(mLCInsureAccClassSchema, manageMoney, feeRate);
        setLPInsureAccClassFee(mLCInsureAccClassSchema, manageMoney, feeRate);
        setGetEndorse(tLCPolSchema, getMoney);
        mGetMoney += getMoney;
        return true;
    }

    private void setLPInsureAcc(LCInsureAccClassSchema aLCInsureAccClassSchema, double money, double leftMoney)
    {
        double sumPaym = aLCInsureAccClassSchema.getSumPaym() + money; //累计领取
        LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPInsureAccSchema, aLCInsureAccClassSchema);
        tLPInsureAccSchema.setEdorNo(mEdorNo);
        tLPInsureAccSchema.setEdorType(mEdorType);
        tLPInsureAccSchema.setInsuAccBala(leftMoney);
        tLPInsureAccSchema.setSumPaym(sumPaym);
        tLPInsureAccSchema.setPrtNo(mLCInsureAccSchema.getPrtNo());
        tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccSchema.setModifyDate(mCurrentDate);
        tLPInsureAccSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
    }

    /**
     * 设置帐户领取轨迹
     * @param aLCInsureAccClassSchema LCInsureAccClassSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTrace(LCInsureAccClassSchema aLCInsureAccClassSchema, double grpMoney)
    {
        String serialNo;
        LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
        tLPInsureAccTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccTraceDB.setEdorType(mEdorType);
        tLPInsureAccTraceDB.setContNo(mContNo);
        tLPInsureAccTraceDB.setPolNo(mPolNo);
        tLPInsureAccTraceDB.setInsuAccNo(aLCInsureAccClassSchema.getInsuAccNo());
        LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
        if (tLPInsureAccTraceSet.size() > 0)
        {
            serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccTrace " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        }
        else
        {
        	serialNo = PubFun1.CreateMaxNo("SERIALNO", aLCInsureAccClassSchema.getManageCom());
        }
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccClassSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccClassSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(aLCInsureAccClassSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(aLCInsureAccClassSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccClassSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccClassSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(aLCInsureAccClassSchema.getPayPlanCode());
        tLPInsureAccTraceSchema.setManageCom(aLCInsureAccClassSchema.getManageCom());
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(BQ.EDORTYPE_LQ);
        tLPInsureAccTraceSchema.setMoney(-Math.abs(grpMoney));  //退费
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("10");
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }

    private void setLPInsureAccClass(LCInsureAccClassSchema aLCInsureAccClassSchema, double money, double leftMoney)
    {
        double sumPaym = aLCInsureAccClassSchema.getSumPaym() + money; //累计领取
        LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPInsureAccClassSchema, aLCInsureAccClassSchema);
        tLPInsureAccClassSchema.setEdorNo(mEdorNo);
        tLPInsureAccClassSchema.setEdorType(mEdorType);
        tLPInsureAccClassSchema.setInsuAccBala(leftMoney);
        tLPInsureAccClassSchema.setSumPaym(sumPaym);
        tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
        tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
        tLPInsureAccClassSchema.setAccAscription("0");
        mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
    }

    private void setLPInsureAccFee(LCInsureAccClassSchema aLCInsureAccClassSchema, double manageMoney)
    {
        String[] keys = new String[2];
        keys[0] = aLCInsureAccClassSchema.getPolNo();
        keys[1] = aLCInsureAccClassSchema.getInsuAccNo();
        LPInsureAccFeeSchema tLPInsureAccFeeSchema = (LPInsureAccFeeSchema)
                mQuery.getDetailData("LCInsureAccFee", keys);
        tLPInsureAccFeeSchema.setFee(tLPInsureAccFeeSchema.getFee() + manageMoney);
        tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccFeeSchema.setModifyDate(mCurrentDate);
        tLPInsureAccFeeSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccFeeSchema, "DELETE&INSERT");
    }

    /**
     * 设置帐户领取轨迹
     * @param aLCInsureAccClassSchema LCInsureAccClassSchema
     * @param grpMoney double
     */
    private void setLPInsureAccFeeTrace(LCInsureAccClassSchema aLCInsureAccClassSchema,  double manageMoney, double feeRate)
    {
        String serialNo;
        LPInsureAccFeeTraceDB tLPInsureAccFeeTraceDB = new LPInsureAccFeeTraceDB();
        tLPInsureAccFeeTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccFeeTraceDB.setEdorType(mEdorType);
        tLPInsureAccFeeTraceDB.setContNo(mContNo);
        tLPInsureAccFeeTraceDB.setPolNo(mPolNo);
        tLPInsureAccFeeTraceDB.setInsuAccNo(aLCInsureAccClassSchema.getInsuAccNo());
        LPInsureAccFeeTraceSet tLPInsureAccFeeTraceSet = tLPInsureAccFeeTraceDB.query();
        if (tLPInsureAccFeeTraceSet.size() > 0)
        {
            serialNo = tLPInsureAccFeeTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccFeeTrace " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        }
        else
        {
            serialNo = PubFun1.CreateMaxNo("SERIALNO", aLCInsureAccClassSchema.getManageCom());
        }
        LPInsureAccFeeTraceSchema tLPInsureAccFeeTraceSchema = new LPInsureAccFeeTraceSchema();
        tLPInsureAccFeeTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccFeeTraceSchema.setEdorType(mEdorType);
        tLPInsureAccFeeTraceSchema.setGrpContNo(aLCInsureAccClassSchema.getGrpContNo());
        tLPInsureAccFeeTraceSchema.setGrpPolNo(aLCInsureAccClassSchema.getGrpPolNo());
        tLPInsureAccFeeTraceSchema.setContNo(aLCInsureAccClassSchema.getContNo());
        tLPInsureAccFeeTraceSchema.setPolNo(aLCInsureAccClassSchema.getPolNo());
        tLPInsureAccFeeTraceSchema.setSerialNo(serialNo);
        tLPInsureAccFeeTraceSchema.setInsuAccNo(aLCInsureAccClassSchema.getInsuAccNo());
        tLPInsureAccFeeTraceSchema.setRiskCode(aLCInsureAccClassSchema.getRiskCode());
        tLPInsureAccFeeTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccFeeTraceSchema.setOtherType("10");
        tLPInsureAccFeeTraceSchema.setPayPlanCode(mLCInsureAccClassSchema.getPayPlanCode());
        tLPInsureAccFeeTraceSchema.setAccAscription("0");
        tLPInsureAccFeeTraceSchema.setMoneyType(BQ.EDORTYPE_LQ);
        tLPInsureAccFeeTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccFeeTraceSchema.setState("0");
        tLPInsureAccFeeTraceSchema.setManageCom(aLCInsureAccClassSchema.getManageCom());
        tLPInsureAccFeeTraceSchema.setFeeCode("000006");
        tLPInsureAccFeeTraceSchema.setFeeRate(feeRate);
        tLPInsureAccFeeTraceSchema.setFee(manageMoney);
        tLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccFeeTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccFeeTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccFeeTraceSchema, "DELETE&INSERT");
    }

    /**
     * 设置管理费分类 轨迹
     * @param aLCInsureAccClassSchema LCInsureAccClassSchema
     * @param manageMoney double
     * @param feeRate double
     */
    private void setLPInsureAccClassFee(LCInsureAccClassSchema aLCInsureAccClassSchema, double manageMoney, double feeRate)
    {
    	String[] keys = new String[2];
        keys[0] = aLCInsureAccClassSchema.getPolNo();
        keys[1] = aLCInsureAccClassSchema.getInsuAccNo();
        LPInsureAccFeeSchema tLPInsureAccFeeSchema = (LPInsureAccFeeSchema)
                mQuery.getDetailData("LCInsureAccFee", keys);
        LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
        tLPInsureAccClassFeeSchema.setEdorNo(mEdorNo);
        tLPInsureAccClassFeeSchema.setEdorType(mEdorType);
        tLPInsureAccClassFeeSchema.setPolNo(aLCInsureAccClassSchema.getPolNo());
        tLPInsureAccClassFeeSchema.setInsuAccNo(aLCInsureAccClassSchema.getInsuAccNo());
        tLPInsureAccClassFeeSchema.setPayPlanCode(aLCInsureAccClassSchema.getPayPlanCode());
        tLPInsureAccClassFeeSchema.setAccAscription("0");
        tLPInsureAccClassFeeSchema.setRiskCode(aLCInsureAccClassSchema.getRiskCode());
        tLPInsureAccClassFeeSchema.setContNo(aLCInsureAccClassSchema.getContNo());
        tLPInsureAccClassFeeSchema.setGrpPolNo(aLCInsureAccClassSchema.getGrpPolNo());
        tLPInsureAccClassFeeSchema.setGrpContNo(aLCInsureAccClassSchema.getGrpContNo());
        tLPInsureAccClassFeeSchema.setManageCom(aLCInsureAccClassSchema.getManageCom());
        tLPInsureAccClassFeeSchema.setInsuredNo(aLCInsureAccClassSchema.getInsuredNo());
        tLPInsureAccClassFeeSchema.setAppntNo(aLCInsureAccClassSchema.getAppntNo());
        tLPInsureAccClassFeeSchema.setAccType(aLCInsureAccClassSchema.getAccType());
        tLPInsureAccClassFeeSchema.setAccComputeFlag(aLCInsureAccClassSchema.getAccComputeFlag());
        tLPInsureAccClassFeeSchema.setAccFoundDate(aLCInsureAccClassSchema.getAccFoundDate());
        tLPInsureAccClassFeeSchema.setAccFoundTime(aLCInsureAccClassSchema.getAccFoundTime());
        tLPInsureAccClassFeeSchema.setBalaDate(aLCInsureAccClassSchema.getBalaDate());
        tLPInsureAccClassFeeSchema.setBalaTime(aLCInsureAccClassSchema.getBalaTime());
        tLPInsureAccClassFeeSchema.setFeeRate(feeRate);
        tLPInsureAccClassFeeSchema.setFee(tLPInsureAccFeeSchema.getFee() + manageMoney);
        tLPInsureAccClassFeeSchema.setFeeUnit(0);
        tLPInsureAccClassFeeSchema.setOtherNo(aLCInsureAccClassSchema.getOtherNo());
        tLPInsureAccClassFeeSchema.setOtherType(aLCInsureAccClassSchema.getOtherType());
        tLPInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccClassFeeSchema.setMakeDate(mCurrentDate);
        tLPInsureAccClassFeeSchema.setMakeTime(mCurrentTime);
        tLPInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
        tLPInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccClassFeeSchema, "DELETE&INSERT");
    }

	/**
     * 设置批改补退费表
     * @return boolean
     */
    private boolean setGetEndorse(LCPolSchema aLCPolSchema, double edorPrem)
    {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGrpContNo(aLCPolSchema.getGrpContNo());
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
        tLJSGetEndorseSchema.setGetMoney(-Math.abs(edorPrem));
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_TB);//万能部分领取的补/退费财务类型属于冲退保金
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(mLCInsureAccClassSchema.getPayPlanCode());
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType("10");
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 设置item表中的费用和状态
     */
    private void setEdorItem()
    {
        System.out.println(mGetMoney);
        String sql = "update LPEdorItem " +
                "set GetMoney = -" + mGetMoney + ", " + //这里是负数
                "EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                "ModifyDate = '" + mCurrentDate + "', " +
                "ModifyTime = '" + mCurrentTime + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql.toString(), "UPDATE");
    }

    /**
     * 得到保全算费的计算代码
     * @param riskCode String
     * @return String
     */
    private LMPolDutyEdorCalSchema getEdorCal(String riskCode) {
        LMPolDutyEdorCalDB tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB();
        tLMPolDutyEdorCalDB.setRiskCode(riskCode);
        tLMPolDutyEdorCalDB.setEdorType(mEdorType);
        LMPolDutyEdorCalSet tLMPolDutyEdorCalSet = tLMPolDutyEdorCalDB.query();
        if (tLMPolDutyEdorCalSet.size() == 0) {
            return null;
        }
        return tLMPolDutyEdorCalSet.get(1);
    }
 }
