package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PEdorWTAppConfirmBL implements EdorAppConfirm
{
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    private LPEdorItemSchema mLPEdorItemSchema = null;

    private GlobalInput mGlobalInput = null;

    public PEdorWTAppConfirmBL()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //处理数据
        if (!dealData())
        {
            return false;
        }
        System.out.println("after dealData data....");

        //数据准备操作
        if (!prepareData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData
                            .getObjectByObjectName("LPEdorItemSchema", 0);
        mGlobalInput = (GlobalInput) mInputData
                       .getObjectByObjectName("GlobalInput", 0);
    }

    /**
     * 处理需要保存的数据
     */
    private boolean dealData()
    {
        try
        {
            LJSGetEndorseBLSet tLJSGetEndorseBLSet = new LJSGetEndorseBLSet();

            //得到需要退保的保单（主附险）
            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPPolDB.setEdorType(mLPEdorItemSchema.getEdorType());
            LPPolSet tLPPolSet = tLPPolDB.query();
            if (tLPPolSet == null || tLPPolSet.size() < 1)
            {
                CError tError = new CError();
                tError.errorMessage = "保全个人险种表中没有对应的记录!";
                this.mErrors.addOneError(tError);
                return false;
            }

            LJSGetEndorseBL aLJSGetEndorseBL = new LJSGetEndorseBL();
            //查询需要扣除体检费的客户
            LPPENoticeSet tLPPENoticeSet = getPENotices();
            //生成退费记录
            for (int i = 1; i <= tLPPolSet.size(); i++)
            {
                //查询是否发生了加费
                double sumAddFee = getSumAddFee(tLPPolSet.get(i).getPolNo());
                //add by zhangym 2008-07-04
                double supplementaryPrem =tLPPolSet.get(i).getSupplementaryPrem();

                //生成批改交退费表
                //退保累计交费
                LJSGetEndorseBL tLJSGetEndorseBL = new LJSGetEndorseBL();
                LJSGetEndorseSchema tLJSGetEndorseSchema = new
                        LJSGetEndorseSchema();
                tLJSGetEndorseSchema.setGetNoticeNo(mLPEdorItemSchema.getEdorNo());
                tLJSGetEndorseSchema.setEndorsementNo(mLPEdorItemSchema.
                        getEdorNo());
                tLJSGetEndorseSchema.setFeeOperationType(mLPEdorItemSchema.
                        getEdorType());
                tLJSGetEndorseSchema.setGetDate(mLPEdorItemSchema.
                                                getEdorValiDate());
                tLJSGetEndorseSchema.setAppntNo(tLPPolSet.get(i).getAppntNo());
                tLJSGetEndorseSchema.setInsuredNo(tLPPolSet.get(i).getInsuredNo());
                tLJSGetEndorseSchema.setRiskCode(tLPPolSet.get(i).getRiskCode());
                tLJSGetEndorseSchema.setRiskVersion(
                    tLPPolSet.get(i).getRiskVersion());
                tLJSGetEndorseSchema.setAgentCom(tLPPolSet.get(i).getAgentCom());
                tLJSGetEndorseSchema.setAgentType(tLPPolSet.get(i).getAgentType());
                tLJSGetEndorseSchema.setPolType(tLPPolSet.get(i).getPolTypeFlag());
                tLJSGetEndorseSchema.setGrpContNo(
                        tLPPolSet.get(i).getGrpContNo());
                tLJSGetEndorseSchema.setApproveCode(
                    tLPPolSet.get(i).getApproveCode());
                tLJSGetEndorseSchema.setApproveDate(
                    tLPPolSet.get(i).getApproveDate());
                tLJSGetEndorseSchema.setApproveTime(
                    tLPPolSet.get(i).getApproveTime());
                tLJSGetEndorseSchema.setHandler(tLPPolSet.get(i).getHandler());
                tLJSGetEndorseSchema.setContNo(tLPPolSet.get(i).getContNo());
                tLJSGetEndorseSchema.setGrpPolNo(tLPPolSet.get(i).getGrpPolNo());
                tLJSGetEndorseSchema.setPolNo(tLPPolSet.get(i).getPolNo());
                tLJSGetEndorseSchema.setAppntNo(tLPPolSet.get(i).getAppntNo());
                tLJSGetEndorseSchema.setInsuredNo(tLPPolSet.get(i).getInsuredNo());
                tLJSGetEndorseSchema.setAppntNo(tLPPolSet.get(i).getAppntNo());
                tLJSGetEndorseSchema.setKindCode(tLPPolSet.get(i).getKindCode());
                tLJSGetEndorseSchema.setRiskCode(tLPPolSet.get(i).getRiskCode());
                tLJSGetEndorseSchema.setRiskVersion(tLPPolSet.get(i).
                        getRiskVersion());
                tLJSGetEndorseSchema.setAgentCom(tLPPolSet.get(i).getAgentCom());
                tLJSGetEndorseSchema.setAgentCode(tLPPolSet.get(i).getAgentCode());
                tLJSGetEndorseSchema.setAgentType(tLPPolSet.get(i).getAgentType());
                tLJSGetEndorseSchema.setAgentGroup(tLPPolSet.get(i).
                        getAgentGroup());
                tLJSGetEndorseSchema.setGetMoney(
                        -(tLPPolSet.get(i).getPrem() - sumAddFee));//扣除加费和
                //从描述表中获取财务接口类型
                String finType = BqCalBL.getFinType(mLPEdorItemSchema.
                        getEdorType(),
                        "TF",
                        mLPEdorItemSchema.getPolNo());
                if (finType.equals(""))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.errorMessage = "在LDCode1中缺少保全财务接口转换类型编码!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tLJSGetEndorseSchema.setFeeFinaType(finType);
                tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
                tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
                tLJSGetEndorseSchema.setOtherNo(mLPEdorItemSchema.getEdorNo());
                tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_P);
                tLJSGetEndorseSchema.setPolType("1");
                tLJSGetEndorseSchema.setSerialNo("");
                tLJSGetEndorseSchema.setGetFlag("1");
                tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
                tLJSGetEndorseSchema.setManageCom(tLPPolSet.get(i).getManageCom());
                tLJSGetEndorseBL.setSchema(tLJSGetEndorseSchema);
                tLJSGetEndorseBL.setDefaultFields();

                //生理赔的险种不退钱
                GCheckClaimBL tGCheckClaimBL = new GCheckClaimBL();
                if(tGCheckClaimBL.checkClaimed(tLJSGetEndorseBL.getPolNo()))
                {
                    //tLJSGetEndorseBL.setGetMoney(0);
                }

                mLJSGetEndorseSet.add(tLJSGetEndorseBL.getSchema());

                //若发生加费，则生成加费的退费财务接口数据：
                //销售提取佣金的时候，需去除加费和后再算佣金，
                //在此以PAYPLANCODE_ADDFEE做标识
                LJSGetEndorseSchema tLJSGetEndorseSchemaAddFee = null;
                if(sumAddFee != 0)
                {
                    tLJSGetEndorseSchemaAddFee = tLJSGetEndorseBL.getSchema();
                    tLJSGetEndorseSchemaAddFee.
                        setPayPlanCode(BQ.PAYPLANCODE_ADDFEE);
                    tLJSGetEndorseSchemaAddFee.setGetMoney(-sumAddFee);
                    mLJSGetEndorseSet.add(tLJSGetEndorseSchemaAddFee);
                }
                if(supplementaryPrem!=0){
                	tLJSGetEndorseSchemaAddFee = tLJSGetEndorseBL.getSchema();
                    tLJSGetEndorseSchemaAddFee.
                        setPayPlanCode(BQ.PAYPLANCODE_SUPPLEMENTARY);
                    tLJSGetEndorseSchemaAddFee.setGetMoney(-supplementaryPrem);
                    mLJSGetEndorseSet.add(tLJSGetEndorseSchemaAddFee);
                }

                //为生成工本费记录做准备
                aLJSGetEndorseBL.setSchema(tLJSGetEndorseBL);
                //生成体检费用记录
                double money = getPeNoticeMoney(
                        tLPPolSet.get(i), tLPPENoticeSet);
                if (!setPENoticeMoney(tLJSGetEndorseBL, money))
                {
                    return false;
                }

                //主表信息更新
                mLPEdorItemSchema.setChgPrem(
                    mLPEdorItemSchema.getChgPrem()
                    + tLJSGetEndorseSchema.getGetMoney()
                    + (tLJSGetEndorseSchemaAddFee == null ? 0
                       : tLJSGetEndorseSchemaAddFee.getGetMoney()));
                mLPEdorItemSchema.setGetMoney(
                    mLPEdorItemSchema.getGetMoney()
                    + tLJSGetEndorseSchema.getGetMoney()
                    + (tLJSGetEndorseSchemaAddFee == null ? 0
                       : tLJSGetEndorseSchemaAddFee.getGetMoney()));
                //
                /*
                //查找余额退还
                if (tLPPolSet.get(i).getLeavingMoney() > 0)
                {
                    LJSGetEndorseBL tLJS = new LJSGetEndorseBL();
                    tLJS.setSchema(tLJSGetEndorseBL.getSchema());
                    //从描述表中获取财务接口类型，modify by Minim at 2003-12-23
                    finType = BqCalBL.getFinType(mLPEdorItemSchema.getEdorType(),
                                                 "YE",
                                                 mLPEdorItemSchema.getPolNo());
                    if (finType.equals(""))
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.errorMessage = "在LDCode1中缺少保全财务接口转换类型编码!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    tLJS.setFeeFinaType(finType);

                    tLJS.setGetMoney( -tLPPolSet.get(i).getLeavingMoney());
                    mLJSGetEndorseSet.add(tLJS);
                    mLPEdorItemSchema.setGetMoney(mLPEdorItemSchema.getGetMoney() +
                                                  tLJS.getGetMoney());
                }
*/
            }


            //计算工本费
            if(!setGBInfo(aLJSGetEndorseBL))
            {
                return false;
            }

            mLPEdorItemSchema.setEdorState("2");
            mLPEdorItemSchema.setUWFlag("0");

            mInputData.clear();
            mInputData.add(mLPEdorItemSchema);
            mInputData.add(tLJSGetEndorseBLSet);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError tError = new CError();
            tError.errorMessage = "WT项目申请确认时出现异常错误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     *  查询险种polNo发生的加费和
     * @param polNo String
     * @return double
     */
    private double getSumAddFee(String polNo)
    {
        String sql = "select sum(SumActuPayMoney) "
                     + "from LJAPayPerson "
                     + "where payPlanCode like '000000%' "
                     + "    and  polNo = '" + polNo+ "' ";
        ExeSQL e = new ExeSQL();
        String sumActuPayMoney = e.getOneValue(sql);

        return sumActuPayMoney.equals("") ? 0
                : Double.parseDouble(sumActuPayMoney);
    }

    /**
     * 查询需要扣除一体检费的客户
     * @param tLPEdorItemSchema LPEdorItemSchema
     * @return LPPENoticeSet
     */
    private LPPENoticeSet getPENotices()
    {
        LPPENoticeDB db = new LPPENoticeDB();
        db.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        db.setEdorNo(mLPEdorItemSchema.getEdorNo());
        db.setContNo(mLPEdorItemSchema.getContNo());
        return db.query();
    }

    /**
     * 查询本险种的客户的体检费
     * @param tLPPolSchema LPPolSchema
     * @return double
     */
    private double getPeNoticeMoney(LPPolSchema tLPPolSchema,
                                    LPPENoticeSet tLPPENoticeSet)
    {
        double pEMoney = 0;
        for (int i = 1; i <= tLPPENoticeSet.size(); i++)
        {
            //若该客户需要扣除体检费
            if (tLPPolSchema.getInsuredNo()
                .equals(tLPPENoticeSet.get(i).getCustomerNo()))
            {
                LPPENoticeItemDB tLPPENoticeItemDB = new LPPENoticeItemDB();
                tLPPENoticeItemDB.setEdorAcceptNo(tLPPENoticeSet.get(i).getEdorAcceptNo());
                tLPPENoticeItemDB.setEdorNo(tLPPENoticeSet.get(i).getEdorNo());
                tLPPENoticeItemDB.setEdorType(tLPPENoticeSet.get(i).getEdorType());
                tLPPENoticeItemDB.setProposalContNo(tLPPENoticeSet.get(i).getProposalContNo());
                tLPPENoticeItemDB.setPrtSeq(tLPPENoticeSet.get(i).getPrtSeq());
                LPPENoticeItemSet tLPPENoticeItemSet = tLPPENoticeItemDB.query();

                //得到每个体检项的价格
                for(int t = 1; t <= tLPPENoticeItemSet.size(); t++)
                {
                    LDTestPriceMgtDB db = new LDTestPriceMgtDB();
                    db.setHospitCode(tLPPENoticeSet.get(i).getPEAddress());
                    db.setMedicaItemCode(tLPPENoticeItemSet.get(t).getPEItemCode());

                    LDTestPriceMgtSet set = db.query();
                    if(set.size() > 0)
                    {
                        pEMoney += Double.parseDouble(
                                set.get(1).getMedicaItemPrice());
                    }
                }
                //移除该体检通知记录，避免多次扣除体检费
                tLPPENoticeSet.removeRange(i, i);
                i--;
            }
        }

        return pEMoney;
    }

    /**
     * 生成体检费记录
     * @param tLJSGetEndorseBL LJSGetEndorseBL
     * @param money double
     * @return boolean
     */
    private boolean setPENoticeMoney(
            LJSGetEndorseBL tLJSGetEndorseBL, double money)
    {
        if(money <= 0)
        {
            return true;
        }

        LJSGetEndorseBL lJSGetEndorseBL = new LJSGetEndorseBL();
        lJSGetEndorseBL.setSchema(tLJSGetEndorseBL);

        String finType = BqCalBL.getFinType(mLPEdorItemSchema.getEdorType(),
                                     "TJ",
                                     mLPEdorItemSchema.getPolNo());
        if (finType.equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.errorMessage = "在LDCode1中缺少保全财务接口转换类型编码!";
            this.mErrors.addOneError(tError);
            return false;
        }
        lJSGetEndorseBL.setFeeFinaType(finType);

        lJSGetEndorseBL.setGetMoney(money);
        mLJSGetEndorseSet.add(lJSGetEndorseBL);
        mLPEdorItemSchema.setGetMoney(mLPEdorItemSchema.getGetMoney()
                                      + money);
        return true;
    }

    /**
     * 计算工本费
     * @param tLJSGetEndorseBL LJSGetEndorseBL
     * @return boolean
     */
    private boolean setGBInfo(LJSGetEndorseBL tLJSGetEndorseBL)
    {
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorEspecialDataDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorEspecialDataDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPEdorEspecialDataDB.setDetailType(BQ.DETAILTYPE_GB);
        tLPEdorEspecialDataDB.setPolNo(BQ.FILLDATA);
        if (!tLPEdorEspecialDataDB.getInfo())
        {
            return true;
        }
        if (Double.parseDouble(tLPEdorEspecialDataDB.getEdorValue()) <= 0)
        {
            return true;
        }

        LJSGetEndorseBL lJSGetEndorseBL = new LJSGetEndorseBL();
        lJSGetEndorseBL.setSchema(tLJSGetEndorseBL);
        lJSGetEndorseBL.setGrpPolNo(BQ.FILLDATA);
        lJSGetEndorseBL.setPolNo(BQ.FILLDATA);
        lJSGetEndorseBL.setGetMoney(Double.parseDouble(
                tLPEdorEspecialDataDB.getEdorValue()));
        lJSGetEndorseBL.setPayPlanCode(BQ.FILLDATA);
        lJSGetEndorseBL.setDutyCode(BQ.FILLDATA);
        lJSGetEndorseBL.setInsuredNo(BQ.FILLDATA);
        lJSGetEndorseBL.setInsuredNo(BQ.FILLDATA);
        lJSGetEndorseBL.setRiskCode(BQ.FILLDATA);
        lJSGetEndorseBL.setRiskVersion(BQ.FILLDATA);
        lJSGetEndorseBL.setKindCode("0");
        lJSGetEndorseBL.setRiskCode("0");
        lJSGetEndorseBL.setRiskVersion("0");

        String finType = BqCalBL.getFinType(mLPEdorItemSchema.getEdorType(),
                                     BQ.FEEFINATYPE_GB,
                                     mLPEdorItemSchema.getPolNo());
        if (finType.equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.errorMessage = "在LDCode1中缺少保全财务接口转换类型编码!";
            this.mErrors.addOneError(tError);
            return false;
        }
        lJSGetEndorseBL.setFeeFinaType(finType);
        lJSGetEndorseBL.setGetMoney(tLPEdorEspecialDataDB.getEdorValue());
        mLJSGetEndorseSet.add(lJSGetEndorseBL);
        mLPEdorItemSchema.setGetMoney(
                mLPEdorItemSchema.getGetMoney()
                + Double.parseDouble(tLPEdorEspecialDataDB.getEdorValue()));
        return true;
    }

    private boolean prepareData()
    {
        mResult.clear();
        MMap map = new MMap();
        map.put(mLJSGetEndorseSet, "DELETE&INSERT");
        map.put(mLPEdorItemSchema, "UPDATE");
        mResult.add(map);
        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "pa0001";
        tGlobalInput.ManageCom = "86";

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo("20051216000012");


        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPEdorItemDB.query().get(1));

        PEdorWTAppConfirmBL tPEdorWTAppConfirmBL = new
                                                   PEdorWTAppConfirmBL();
        if (!tPEdorWTAppConfirmBL.submitData(tVData, "INSERT"))
        {
            System.out.println(tPEdorWTAppConfirmBL.mErrors.getErrContent());
        }

    }


    }
