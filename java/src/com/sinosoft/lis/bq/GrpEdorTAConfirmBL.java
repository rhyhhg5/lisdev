package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LPGrpEdorItemDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class GrpEdorTAConfirmBL implements EdorConfirm {
	private MMap mMap = new MMap();

	private GlobalInput mGlobalInput = null;

	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

	private String mEdorNo = null;

	private String mEdorType = null;

	private String mGrpContNo = null;
 
	private String mEdorValidate = null;
	
	private String mModifyDate = null;
	
	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * @param cInputData VData
	 * @param cOperate String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		getInputData(cInputData);
		if (!checkData()) {
			return false;
		}
		if (!dealData()) {
			return false;
		}
		return true;
	}

	/**
	 * 判断保全生效日期和试算日期是否为同一天，
	 * @return
	 */
	private boolean checkData() {

		if (mModifyDate.equals("")) {
			mErrors.addOneError("取到的试算日期为空");
			return false;
		}
		String sqlli3 = "SELECT date('" + mModifyDate + "')-date('"
		+ PubFun.getCurrentDate() + "') FROM dual where 1 =1 ";
		System.out.println(sqlli3+"#########");
		ExeSQL exesql = new ExeSQL();
		SSRS InsuRSSRSli3 = new SSRS();
		InsuRSSRSli3 = exesql.execSQL(sqlli3);
		if (InsuRSSRSli3 != null && InsuRSSRSli3.getMaxRow() > 0
				&& (Integer.parseInt(InsuRSSRSli3.GetText(1, 1)) - 0) != 0) {
			mErrors.addOneError("试算日期和保全确认不是同一天,需重新试算");
			return false;
		}

		return true;
	}

	/**
	 * 返回计算结果
	 * @return VData
	 */
	public VData getResult() {
		VData data = new VData();
		data.add(mMap);
		return data;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * @param cInputData VData
	 */
	private void getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData
				.getObjectByObjectName("LPGrpEdorItemSchema", 0);
		mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
		mEdorType = mLPGrpEdorItemSchema.getEdorType();
		mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
		LPGrpEdorItemDB mGLPGrpEdorItemDB = new LPGrpEdorItemDB();
		mGLPGrpEdorItemDB.setEdorNo(mEdorNo);
		mGLPGrpEdorItemDB.setEdorType(mEdorType);
		mGLPGrpEdorItemDB.setGrpContNo(mGrpContNo);
		mModifyDate = mGLPGrpEdorItemDB.query().get(1).getModifyDate();
		
		System.out.println("mModifyDate1111111111"+mModifyDate);
		mEdorValidate = mLPGrpEdorItemSchema.getEdorValiDate();//取出试算的生效日期

	}

	/**
	 * 处理业务逻辑
	 * @return boolean
	 */
	private boolean dealData() {
		//置lpgrpedoritem 的状态为保全确认完毕。PICC这边不置啊
	      mMap.put("update lpgrpedoritem set EdorValidate='"+mEdorValidate+"',edorstate='0' where  Edorno='"+mEdorNo+"' and GrpContno='"+mGrpContNo+"'", "UPDATE");
		if (!validateEdorData()) {
			return false;
		}
		return true;
	}

	/**
	 * 使保全数据生效
	 * @return boolean
	 */
	private boolean validateEdorData() {
		/*
		 String[] tables = {"LCInsureAcc"};
		 ValidateEdorData validate = new ValidateEdorData(tables,
		 mEdorNo, mEdorType);
		 if (!validate.submitData())
		 {
		 mErrors.copyAllErrors(validate.mErrors);
		 return false;
		 }
		 mMap.add(validate.getMap());
		 return true;
		 */
		ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,
				mEdorNo, mEdorType, mGrpContNo, "GrpContNo");
		String[] addTables = { "LCInsureAccTrace" };
		validate.addData(addTables);
		String[] chgTables = { "LCInsureAcc", "LCInsureAccClass" };
		validate.changeData(chgTables);
		mMap.add(validate.getMap());
		return true;
	}
	public static void main(String[] args) {
		String mModifyDate="2010-9-12";
		
		if (mModifyDate.equals("") || mModifyDate == null) {

		System.out.print("取到的试算生效日期为空");

		}
		String sqlli3 = "SELECT date('" + mModifyDate + "')-date('"
				+ PubFun.getCurrentDate() + "') FROM dual where  1=1 ";
		ExeSQL exesql = new ExeSQL();
		SSRS InsuRSSRSli3 = new SSRS();
		InsuRSSRSli3 = exesql.execSQL(sqlli3);
		if (InsuRSSRSli3 != null && InsuRSSRSli3.getMaxRow() > 0
				&& (Integer.parseInt(InsuRSSRSli3.GetText(1, 1)) - 0) != 0) {
			//mErrors.addOneError("取到的试算生效日期为空");
			System.out.print("取到的试算生效日期为空--------");
		}
		
	}

}
