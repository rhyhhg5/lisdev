package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 收费方式变更保全确认处理类</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Lanjun
 * @version 1.0
 */
public class PEdorCCConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /**  */

    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public PEdorCCConfirmBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public void setOperate(String cOperate)
    {
        this.mOperate = cOperate;
    }

    public String getOperate()
    {
        return this.mOperate;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.setOperate(cOperate);

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //数据操作
        if (!dealData())
        {
            return false;
        }

        //输出数据准备
        if (!prepareOutputData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**数据查询业务处理(queryData())
     *
     */


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mLPEdorItemSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError(new CError("传入数据不完全！"));
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        tLPEdorItemDB.setPolNo(mLPEdorItemSchema.getPolNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
        {
            mErrors.addOneError(new CError("查询批改项目信息失败！"));
            return false;
        }
        mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));
        return true;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {

        boolean flag = true;
        return flag;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData()
    {
        Reflections tRef = new Reflections();

        LPContSchema tLPContSchema = new LPContSchema();
        LCContSchema tLCContSchema = new LCContSchema();
        LPContSchema aLPContSchema = new LPContSchema();
        LCContSchema aLCContSchema = new LCContSchema();

        tLPContSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPContSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPContSchema.setContNo(mLPEdorItemSchema.getContNo());

        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setContNo(mLPEdorItemSchema.getContNo());
        tLPContDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPContDB.setEdorType(mLPEdorItemSchema.getEdorType());

        if(!tLPContDB.getInfo())
        {
            mErrors.addOneError(new CError("合同信息不完整，请确保保单信息的完整性！"));
            return false;
        }

        aLPContSchema = tLPContDB.getSchema();
        tLCContSchema = new LCContSchema();
        tLPContSchema = new LPContSchema();

        LCContDB aLCContDB = new LCContDB();
        aLCContDB.setContNo(aLPContSchema.getContNo());
        aLCContDB.setInsuredNo(aLPContSchema.getInsuredNo());
        if (!aLCContDB.getInfo())
        {
            mErrors.copyAllErrors(aLCContDB.mErrors);
            mErrors.addOneError(new CError("查询被保人信息失败！"));
            return false;
        }

        //交换c表和p表
        aLCContSchema = aLCContDB.getSchema();
        tRef.transFields(tLPContSchema, aLCContSchema);
        tLPContSchema.setEdorNo(aLPContSchema.getEdorNo());
        tLPContSchema.setEdorType(aLPContSchema.getEdorType());

        tRef.transFields(tLCContSchema, aLPContSchema);
        tLCContSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContSchema.setModifyTime(PubFun.getCurrentTime());

        //得到当前LPEdorItem保单信息主表的状态，并更新状态为申请确认。
        mLPEdorItemSchema.setEdorState("0");
        mMap.put(mLPEdorItemSchema, "UPDATE");
        mMap.put(tLCContSchema, "UPDATE");
        mMap.put(tLPContSchema, "UPDATE");
        setLCPol();
        return true;
    }

    private boolean prepareOutputData()
    {
        mResult.clear();
        mResult.add(mMap);
        return true;
    }
    
    private void setLCPol()
    {
    	ValidateEdorData2 tV = new ValidateEdorData2(mGlobalInput, mLPEdorItemSchema.getEdorAcceptNo(),mLPEdorItemSchema.getEdorType(), mLPEdorItemSchema.getContNo(), "ContNo");
    	String[] chgTables = {"LCPol"};
    	tV.changeData(chgTables);
        mMap.add(tV.getMap());
    }
    

}
