package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PrtGrpInsuredListCTUI
{
    /**
     * ���������
     */
    public CErrors mErrors = null;
    private String mEdorAcceptNo = null;

    public PrtGrpInsuredListCTUI(String edorAcceptNo)
    {
        mEdorAcceptNo = edorAcceptNo;
    }

    public boolean submitData(VData inputData, String operator)
    {
        PrtGrpInsuredListCTBL bl = new PrtGrpInsuredListCTBL(mEdorAcceptNo);
        if(!bl.submitData(inputData, operator))
        {
            mErrors = bl.mErrors;
            return false;
        }
        return true;
    }
}
