package com.sinosoft.lis.bq;

import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LPInsuredSet;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.bl.LPContBL;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.bl.LPPolBL;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PEdorZFDetailBL {

    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LPInsuredSet mLPInsuredSet = new LPInsuredSet();
    private LPPolSet mLPPolSet = null;
    private MMap map = new MMap();
    private Reflections ref = new Reflections();

    private String currDate = PubFun.getCurrentDate();
    private String currTime = PubFun.getCurrentTime();


    public PEdorZFDetailBL() {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */

    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }
        if (!checkData())
        {
            return false ;
        }
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareData()) {
            return false;
        }
        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, "")) { //数据提交
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorCTDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("PEdorZFDetailBL End PubSubmit");
        return true;

    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    public boolean getInputData()
    {
        try {
            mGlobalInput = (GlobalInput) mInputData
                           .getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData
                                .getObjectByObjectName("LPEdorItemSchema", 0);
            mLPPolSet = (LPPolSet) mInputData.
                        getObjectByObjectName("LPPolSet", 0);
        } catch (Exception e) {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorZFDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mGlobalInput == null || mLPEdorItemSchema == null
            || mLPPolSet == null || mLPPolSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "PEdorCTDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "输入数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    private boolean checkData() {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        String reasonCode = mLPEdorItemSchema.getReasonCode();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();

        if (mLPPolSet.size() == 0) {
            mErrors.addOneError("请选择险种。");
            return false;
        }

        if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
            CError tError = new CError();
            tError.moduleName = "PEdorWTDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "输入数据有误,LPEdorItem中没有相关数据!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLPEdorItemSchema = tLPEdorItemSet.get(1);
        mLPEdorItemSchema.setEdorState("1");
        mLPEdorItemSchema.setReasonCode(reasonCode);

        if (mLPEdorItemSchema.getEdorState().trim().equals(BQ.EDORSTATE_CAL)) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorCTDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保全已经保全理算，不能修改!";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= mLPPolSet.size(); i++) {
            LPPolBL tLPPolBL = new LPPolBL();
            tLPPolBL.setSchema(mLPPolSet.get(i));
            LPEdorItemSchema tmLPEdorItemSchema = new LPEdorItemSchema();
            tmLPEdorItemSchema.setSchema(mLPEdorItemSchema);
            tmLPEdorItemSchema.setPolNo(mLPPolSet.get(i).getPolNo());
            tLPPolBL.queryLPPol(tmLPEdorItemSchema);
            if (tLPPolBL.mErrors.needDealError()) {
                CError.buildErr(this, "查询保单险种失败！");
                return false;
            }
            mLPPolSet.get(i).setSchema(tLPPolBL.getSchema());
        }

        if(!checkAppendPol())
        {
            return false;
        }
        return true ;
    }

    private boolean dealData()
    {
     //首先更新其后的保全项目的状态
      map.put(UpdateEdorState.getUpdateEdorStateSql(mLPEdorItemSchema), "UPDATE");
      String edorno = mLPEdorItemSchema.getEdorNo();
      String edortype = mLPEdorItemSchema.getEdorType();
      String sql = " edorno='" + edorno + "' and edortype='" + edortype + "'";
      map.put("delete from lppol where" + sql, "DELETE");
      map.put("delete from lpcont where" + sql, "DELETE");
//      map.put("delete from lpinsured where" + sql, "DELETE");

      //备份保单信息
      LPContBL tLPContBL = new LPContBL();
      tLPContBL.queryLPCont(mLPEdorItemSchema);
      if (tLPContBL.mErrors.needDealError()) {
          CError.buildErr(this, "查询个人保单出错！");
               return false;
          }
//      map.put(tLPContBL.getSchema(), "DELETE&INSERT");

      //备份被保人信息
      LCInsuredSet tLCInsuredSet = getLCInsured();
      for (int i = 1; i <= tLCInsuredSet.size(); i++) {
      LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(i);

      LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
      tLPInsuredSchema.setEdorNo(edorno);
      tLPInsuredSchema.setEdorType(edortype);
      ref.transFields(tLPInsuredSchema, tLCInsuredSchema);
      tLPInsuredSchema.setOperator(mGlobalInput.Operator);
      PubFun.fillDefaultField(tLPInsuredSchema);
      mLPInsuredSet.add(tLPInsuredSchema);

//      map.put(mLPInsuredSet, "DELETE&INSERT");
      map.put(mLPPolSet, "DELETE&INSERT");
        }
      mLPEdorItemSchema.setEdorState("1");
      mLPEdorItemSchema.setModifyDate(currDate);
      mLPEdorItemSchema.setModifyTime(currTime);
      map.put(mLPEdorItemSchema, "UPDATE");

      return true;
  }

    private boolean checkAppendPol()
    {
        LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCodeType("mainsubriskrela");

        //未选择的险种
        LCPolSet tLCPolSet = getLeavingPolNoInfo(mLPPolSet);
        if (tLCPolSet == null) {
            return true;
        }

        //若未选择的险种是附加险且其主险已选择，则提示
        for (int i = 1; i <= tLCPolSet.size(); i++) {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);

            if (tLCPolSchema.getMainPolNo().equals(mLPPolSet.get(1).getMainPolNo())) {
                continue; //不是附加险
            }

            for (int j = 1; j <= mLPPolSet.size(); j++) {
                LPPolSchema tLPPolSchema = mLPPolSet.get(j);
                if (tLPPolSchema.getPolNo().equals(tLCPolSchema.getMainPolNo())
                    && tLPPolSchema.getInsuredNo()
                    .equals(tLCPolSchema.getInsuredNo())) {
                    mErrors.addOneError("险种 " + tLPPolSchema.getRiskCode()
                                        + " 有附加险 " + tLCPolSchema.getRiskCode()
                                        + "，请选择其附加险一起做满期终止。");
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 查询未选择的险种信息
     * @param tLPPolSet LPPolSet
     * @return LCPolSet
     */
    private LCPolSet getLeavingPolNoInfo(LPPolSet tLPPolSet)
    {
        String polNos = "";
        for (int i = 1; i <= tLPPolSet.size(); i++) {
            polNos += "'" + tLPPolSet.get(i).getPolNo() + "', ";
        }

        String sql = "  select * "
                     + "from LCPol "
                     + "where contNo = '" + tLPPolSet.get(1).getContNo() + "' "
                     + "    and polNo not in ("
                     + polNos.substring(0, polNos.lastIndexOf(",")) + ")";
        System.out.println(sql);

        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);

        return tLCPolSet;
    }

    private LCInsuredSet getLCInsured()
    {
        String insuredNos = "";
        for (int i = 1; i <= mLPPolSet.size(); i++) {
            insuredNos += "'" + mLPPolSet.get(i).getInsuredNo() + "',";
        }
        insuredNos = insuredNos.substring(0, insuredNos.lastIndexOf(","));
        StringBuffer sql = new StringBuffer();

        sql.append("select * ")
                .append("from LCInsured ")
                .append("where contNo = '")
                .append(this.mLPEdorItemSchema.getContNo())
                .append("'  and insuredNo in (").append(insuredNos).append(") ");
        System.out.println(sql.toString());

        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.executeQuery(sql.toString());

        return tLCInsuredSet;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareData() {
        mResult.clear();
        mResult.add(map);
        return true;
    }

}
