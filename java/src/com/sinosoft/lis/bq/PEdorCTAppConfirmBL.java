package com.sinosoft.lis.bq;

import java.util.HashMap;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCInsureAccClassFeeDB;
import com.sinosoft.lis.db.LCInsureAccDB;
import com.sinosoft.lis.db.LCInsureAccFeeDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LMDutyGetAliveDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskBonusDB;
import com.sinosoft.lis.db.LMRiskBonusRateDB;
import com.sinosoft.lis.db.LMRiskFeeDB;
import com.sinosoft.lis.db.LOLoanDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.db.LPGetDB;
import com.sinosoft.lis.db.LPInsureAccClassDB;
import com.sinosoft.lis.db.LPInsureAccDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.pubfun.AccountManage;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.BqCalBase;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.UliBQInsuAccBala;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCInsureAccClassFeeSchema;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCInsureAccFeeSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LMDutyGetAliveSchema;
import com.sinosoft.lis.schema.LOBonusPolSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPGetSchema;
import com.sinosoft.lis.schema.LPInsureAccClassFeeSchema;
import com.sinosoft.lis.schema.LPInsureAccClassSchema;
import com.sinosoft.lis.schema.LPInsureAccFeeSchema;
import com.sinosoft.lis.schema.LPInsureAccFeeTraceSchema;
import com.sinosoft.lis.schema.LPInsureAccSchema;
import com.sinosoft.lis.schema.LPInsureAccTraceSchema;
import com.sinosoft.lis.schema.LPLoanSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.schema.LPReturnLoanSchema;
import com.sinosoft.lis.vdb.LCInsureAccDBSet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCInsureAccClassFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccClassSet;
import com.sinosoft.lis.vschema.LCInsureAccFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.vschema.LMDutyGetAliveSet;
import com.sinosoft.lis.vschema.LMRiskBonusRateSet;
import com.sinosoft.lis.vschema.LMRiskBonusSet;
import com.sinosoft.lis.vschema.LMRiskFeeSet;
import com.sinosoft.lis.vschema.LOLoanSet;
import com.sinosoft.lis.vschema.LPGetSet;
import com.sinosoft.lis.vschema.LPInsureAccClassFeeSet;
import com.sinosoft.lis.vschema.LPInsureAccClassSet;
import com.sinosoft.lis.vschema.LPInsureAccFeeSet;
import com.sinosoft.lis.vschema.LPInsureAccSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class PEdorCTAppConfirmBL implements EdorAppConfirm
{
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    
    /** 数据操作字符串 */
    private String mOperate;

    /** 业务对象 */
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();
    private ExeSQL mExeSQL=new ExeSQL();
    
//    是否存在未还贷款标志
    private boolean IsLoan = false;
//    原始生效日期
    private String mEdorValiDate = "";

	/** 保全项目特殊数据 */
	private LPLoanSchema mLPLoanSchema = new LPLoanSchema();
	private LPReturnLoanSchema mLPReturnLoanSchema = new LPReturnLoanSchema();

//    是否曾因为当前未还贷款失效标志
    private boolean IsLoanAbate = false;
    
    private Reflections ref=new Reflections();
    
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public PEdorCTAppConfirmBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }
        if(!checkData())
        {
        	return false;
        }
        /* MAYBE 2012-2-15 xp 团险万能功能尚未添加,为保持外测模拟版本一致,注释掉此段代码
         * //由于团体万能跟其他保单有区别，这边分开对其进行处理，单独一个理算类对其进行理算
        ExeSQL tExeSQL = new ExeSQL();
        String verifySql ="select 1  from lcgrppol a where a.grpcontno='"+mLPEdorItemSchema.getGrpContNo()+"' and riskcode in (select riskcode from  lmriskapp where risktype4='4'and Riskprop='G') ";
        String verifySqlResult = tExeSQL.getOneValue(verifySql);
        if (verifySqlResult.trim().equals("1")) {
        	PEdorUliCTAppConfirm mEdorUliCTAppConfirm  = new PEdorUliCTAppConfirm();
        	mEdorUliCTAppConfirm.submitData(cInputData, cOperate);
        	mResult.add(mEdorUliCTAppConfirm.getResult());
        } else {*/
        //数据准备操作
        if (!prepareData())
        {
            return false;
        }
//        }
        prepareOutputData();

        return true;
    }
    
    /**
     * 校验数据
     */
    private boolean checkData()
    {
    	if(mLPEdorItemSchema.getEdorValiDate()==null||mLPEdorItemSchema.getEdorValiDate().equals("")){
    		// @@错误处理
			System.out.println("PEdorCTAppConfirmBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorCTAppConfirmBL";
			tError.functionName = "checkData";
			tError.errorMessage = "获取保全生效日期失败!";
			mErrors.addOneError(tError);
			return false;
    	}
    	//保全生效日期与理算日的校验只针对分红险
    	String tcheckFH=mExeSQL.getOneValue("select 1 from lppol a where edorno='"+mLPEdorItemSchema.getEdorNo()+"' and contno='"+mLPEdorItemSchema.getContNo()+"' " +
    					" and exists (select 1 from lmriskapp where riskcode=a.riskcode and RiskType4= '2') with ur");
    	if(tcheckFH!=null&&(!tcheckFH.equals(""))){
    		if(!PubFun.getLaterDate(mLPEdorItemSchema.getEdorValiDate(), mCurrentDate).equals(mLPEdorItemSchema.getEdorValiDate())){
        		// @@错误处理
    			System.out.println("PEdorCTAppConfirmBL+checkData++--");
    			CError tError = new CError();
    			tError.moduleName = "PEdorCTAppConfirmBL";
    			tError.functionName = "checkData";
    			tError.errorMessage = "保全生效日期必须大于或者等于理算当天!";
    			mErrors.addOneError(tError);
    			return false;
        	}
    	}
    	
    	//判断是否有需要赠送持续奖金后再解约的险种
        LPPolSet tLPPolSet = getLPPolSet(mLPEdorItemSchema.getSchema());
        for(int m=1;m<=tLPPolSet.size();m++){
    	String sustainedBsql="select * from ldcode1 where codetype='SustainedBSendTime' and code='"+tLPPolSet.get(m).getRiskCode()+"' order by int(code1) asc with ur ";
    	LDCode1DB tLDCode1DB = new LDCode1DB();
    	LDCode1Set tLDCode1Set = new LDCode1Set();
    	tLDCode1Set = tLDCode1DB.executeQuery(sustainedBsql);
    	for(int i=1;i<=tLDCode1Set.size();i++){
    		//从第一次赠送开始判断(赠送时间在解约之前)
    		if(!CommonBL.stringToDate(PubFun.getLastDate(tLPPolSet.get(m).getCValiDate(), Integer.parseInt(tLDCode1Set.get(i).getCodeAlias()), "Y")).after(CommonBL.stringToDate(mLPEdorItemSchema.getEdorValiDate()))){
    			//判断是否已经赠送持续奖金
    			String sendFlag = mExeSQL.getOneValue("select 1 from lcinsureacctrace where moneytype='B' and contno='"+tLPPolSet.get(m).getContNo()+"' " +
    					" and paydate>='"+PubFun.getLastDate(tLPPolSet.get(m).getCValiDate(), Integer.parseInt(tLDCode1Set.get(i).getCodeAlias()), "Y")+"'");
    			if(sendFlag==null||"".equals(sendFlag)){
            		// @@错误处理
        			System.out.println("PEdorCTAppConfirmBL+checkData++--");
        			CError tError = new CError();
        			tError.moduleName = "PEdorCTAppConfirmBL";
        			tError.functionName = "checkData";
        			tError.errorMessage = "险种号"+tLPPolSet.get(m).getPolNo()+"有未赠送的持续奖金，请先撤销保全项目并于明天再添加保全项目进行解约处理";
        			mErrors.addOneError(tError);
        			return false;    				
    			}
    		}
    	}
        }
    	
//    	 查询即往借款信息,取得上次借款的本金额度和利息
		LOLoanDB tLOLoanDB = new LOLoanDB();
		LOLoanSet tLOLoanSet = tLOLoanDB.executeQuery("select * from loloan where polno in (select polno from lcpol where contno='" + mLPEdorItemSchema.getContNo()
				+ "') and loantype='0' and payoffflag='0' with ur");
		// //loanType 1-自垫 0-借款
		// tLOLoanDB.setLoanType("0");
		// //1-还清
		// tLOLoanDB.setPayOffFlag("0");

		if (tLOLoanSet.size() > 1) {
			// @@错误处理
			System.out.println("PEdorRFAppConfirmBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorRFAppConfirmBL";
			tError.functionName = "checkData";
			tError.errorMessage = "获取贷款表失败!";
			mErrors.addOneError(tError);
			return false;
		}
		if(tLOLoanSet.size()==1){
			IsLoan=true;
			ref.transFields(mLPLoanSchema, tLOLoanSet.get(1));
			//actugetno为还款工单号
			mLPLoanSchema.setActuGetNo(mLPEdorItemSchema.getEdorNo());
			mLPLoanSchema.setEdorType(mLPEdorItemSchema.getEdorType());
//        	是否存在未还贷款,并且由此贷款曾经导致过保单失效.
            String tcheck=mExeSQL.getOneValue("select min(b.startdate) from loloan a,lccontstate b "
            		+ " where a.payoffflag='0' and a.polno=b.polno and a.actugetno=b.otherno and b.othernotype='10' " 
            		+ " and b.statereason='LN' and  b.contno='"+mLPEdorItemSchema.getContNo()+"'");
            if(tcheck!=null&&(!tcheck.equals(""))){
//            	保全生效日期必须大于或者等于保单当时的失效日期
            	if(PubFun.getLaterDate(mLPEdorItemSchema.getEdorValiDate(), tcheck).equals(mLPEdorItemSchema.getEdorValiDate())){
//            		存储原始生效日期
            		mEdorValiDate=mLPEdorItemSchema.getEdorValiDate();
//            		设置生效日期为失效当天
            		mLPEdorItemSchema.setEdorValiDate(tcheck);
            		IsLoanAbate=true;
            	}else{
    	        	// @@错误处理
    				System.out.println("PEdorCTAppConfirmBL+checkData++--");
    				CError tError = new CError();
    				tError.moduleName = "PEdorCTAppConfirmBL";
    				tError.functionName = "checkData";
    				tError.errorMessage = "保全生效日期必须晚于由于贷款而失效的当天,该保单该日期为:"+tcheck;
    				mErrors.addOneError(tError);
    				return false;
            	}
            }
		}
		//122501健康守望C发生过特定疾病住院医疗责任给付，则不允许解约
		String Sql="select  1"
            +" from llclaimdetail a,llcase b"
            +" where   a.caseno=b.caseno " 
            +" and a.riskcode='122501' and a.getdutycode='409203' and a.contno='"+mLPEdorItemSchema.getContNo()
            +"' and a.polno in (select polno from lcpol where contno='"+mLPEdorItemSchema.getContNo()
            +"') and b.rgtstate in('11','12')  with ur ";
        
		String JianKang122501=mExeSQL.getOneValue(Sql);
		System.out.println(Sql);
        System.out.println("JianKang122501="+JianKang122501);
     if (null!=JianKang122501&&!"".equals(JianKang122501))
        {

     	// @@错误处理
			System.out.println("PEdorCTAppConfirmBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorCTAppConfirmBL";
			tError.functionName = "checkData";
			tError.errorMessage = "保单已发生过特定疾病住院医疗责任给付，不能解约！";
			mErrors.addOneError(tError);
			return false;
        }
        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData()
    {
        mResult.add(map);
    }


    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData()
    {
        try
        {
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData.
                                getObjectByObjectName("LPEdorItemSchema", 0);
            mGlobalInput.setSchema((GlobalInput) mInputData.
                                   getObjectByObjectName("GlobalInput", 0));
        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 得到需要退保的保单（主附险），去除作犹豫期退保的保单
     * @return
     */
    private LPPolSet getLPPolSet(LPEdorItemSchema tLPEdorItemSchema)
    {
        //查找出所有的保单
        LPPolSet tLPPolSet = new LPPolSet();
        LPPolDB  tLPPolDB=new LPPolDB();
        tLPPolDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
        tLPPolDB.setContNo(tLPEdorItemSchema.getContNo());
        tLPPolDB.setEdorType(tLPEdorItemSchema.getEdorType());
        tLPPolSet=tLPPolDB.query();
        if (tLPPolDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询待退保险种时失败！");
            return null;
        }

        System.out.println("需要退保的保单数:" + tLPPolSet.size());
        return tLPPolSet;
    }

    /**
     * 准备需要退保的数据，单个个人保单退保入口，LPEdorItemSchema中只需要PolNo、EdorNo和EdorType即可
     * @return
     */
    public boolean getZTData(LPEdorItemSchema pLPEdorItemSchema)
    {
    	//查询保单信息
    	LCContSchema tLCContSchema = CommonBL.getLCCont(pLPEdorItemSchema.getContNo());
    	if(tLCContSchema==null){
        	CError.buildErr(this, "查询分单"+pLPEdorItemSchema.getContNo()+"失败！");
        	return false;                		
    	}
    	//判断是否为团险万能退保
    	if("2".equals(tLCContSchema.getContType())&&CommonBL.hasULIRisk(tLCContSchema.getContNo())){
    		if(!getGRPULIZTData(pLPEdorItemSchema)){
    			return false;
    		}
    	}else{
    		
        EdorCalZT tEdorCalZT = null;
        try
        {
            //得到需要退保的保单（主附险），去除作犹豫期退保的保单
            LPPolSet tLPPolSet = getLPPolSet(pLPEdorItemSchema);

            //循环每个保单，进行退保处理
            for (int i = 0; i < tLPPolSet.size(); i++)
            {
                //为保单制作个单保全主表数据
                LPEdorItemSchema tLPEdorItemSchema = pLPEdorItemSchema.
                        getSchema();
                tLPEdorItemSchema.setPolNo(tLPPolSet.get(i + 1).getPolNo());

                tEdorCalZT = new EdorCalZT("1");
                tEdorCalZT.calZTData(tLPEdorItemSchema);
                if(tEdorCalZT.mErrors.needDealError())
                {
                    CError.buildErr(this, "退保计算失败！");
                    mErrors.copyAllErrors(tEdorCalZT.mErrors);
                    System.out.println(mErrors.getErrContent());
                    return false;
                }

                //为了与减人算法保持一致
                //为处理增减人取相同算法时由于精度问题导致费用误差而将算费公式精度提高并将财务数据汇总为一条
                LJSGetEndorseSet set = tEdorCalZT.mLJSGetEndorseSet;

                if(set.size() > 0)
                {
                        /**
                         * modify by fuxin 2009-11-6
                         * 原因：原来团体账户型险种将团体理赔账户的金额进行了合计，由于要支持
                         * TPA险种，现将理算信息按账户类型存储。
                         */
//                    boolean flag = true;
//                    LCPolDB tLCPolDB = new LCPolDB();
//                    if(CommonBL.isEspecialPol(set.get(1).getRiskCode()))
//                    {
//                        tLCPolDB.setGrpPolNo(set.get(1).getGrpPolNo());
//                        tLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
//                        tLCPolDB.setAccType(BQ.ACCTYPE_PUBLIC); //理赔账户
//                        LCPolSet tLCPolSet = tLCPolDB.query();
//                        if (tLCPolSet.size() >0 && set.get(1).getPolNo().equals(tLCPolSet.get(1).getPolNo()))
//                        {
//                            flag = false;
//                        }
//                    }
//                    if(flag)
//                    {
//                        double getMoney = 0;
//                        for (int temp = 1; temp <= set.size(); temp++) {
//                            getMoney += set.get(temp).getGetMoney();
//                        }
//                        LJSGetEndorseSchema schema = set.get(1);
////                        schema.setDutyCode(BQ.FILLDATA);
////                        schema.setPayPlanCode(BQ.FILLDATA);
//                        schema.setGetMoney(PubFun.setPrecision(getMoney, "0.00"));
//                        mLJSGetEndorseSet.add(schema);
//                    }
//                    else
//                    {
                        mLJSGetEndorseSet.add(set);
//                    }
                LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                tLCInsureAccDB.setPolNo(tLPPolSet.get(i + 1).getPolNo());
                System.out.println("保单号："+tLPPolSet.get(i + 1).getContNo());
                LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
                for (int j=1;j<=tLCInsureAccSet.size();j++){
                    LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccSet.get(j);
                    if (tLCInsureAccSchema.getInsuAccBala() > 0){
                        String sql = "delete from LPInsureAccFeeTrace " +
                                     "where EdorNo = '" + tLPEdorItemSchema.getEdorNo() + "' " +
                                     "and EdorType = '" + tLPEdorItemSchema.getEdorType() + "' " +
                                     "and polno = '" + tLCInsureAccSchema.getPolNo() + "'";
                        map.put(sql, "DELETE");

                        double tAccBF =  tLCInsureAccSchema.getInsuAccBala()-tEdorCalZT.mAccTBFMoney;
                        double tAccLX  = tEdorCalZT.mLXFMoney - tEdorCalZT.mLXEMoney;
                        System.out.println(tAccBF+tAccLX);
                        setInsureAccFee(tLCInsureAccSchema,
                                        pLPEdorItemSchema,
                                        tAccBF,
                                        CommonBL.getManageFeeRate(tLPPolSet.get(i + 1).getGrpPolNo(),BQ.FEECODE_CT),
                                        BQ.FEECODE_CT);
                        setInsureAccFee(tLCInsureAccSchema,
                                        pLPEdorItemSchema,
                                        tAccLX,
                                        CommonBL.getManageFeeRate(tLPPolSet.get(i + 1).getGrpPolNo(),BQ.FEECODE_CT),
                                        BQ.FEECODE_CT);
                        setLPInsureAccFee(tLCInsureAccSchema,pLPEdorItemSchema,tAccBF+tAccLX);
                    }
                }

                }
                mLJSGetEndorseSet.add(tEdorCalZT.mLXLJSGetEndorseSet);
            }
        }
        catch (Exception ex)
        {
            mErrors.copyAllErrors(tEdorCalZT.mErrors);
            CError.buildErr(this, ex.getMessage());
            ex.printStackTrace();
            return false;
        }
    	}
        return true;
    }
    
    /**
     * 准备需要退保的数据，团险万能解约操作
     * @return
     */
    public boolean getGRPULIZTData(LPEdorItemSchema pLPEdorItemSchema)
    {
    	
    	//查询保单信息
    	LCContSchema tLCContSchema = CommonBL.getLCCont(pLPEdorItemSchema.getContNo());
    	if(tLCContSchema==null){
        	CError.buildErr(this, "查询分单"+pLPEdorItemSchema.getContNo()+"失败！");
        	return false;                		
    	}

    	//保单上次月结与保全生效日期之间不能超过一个月
    	String edorValidate = pLPEdorItemSchema.getEdorValiDate();
    	//查询保单的最后一次月结时间
    	String lastBalaDate = new ExeSQL().getOneValue("select baladate from lcinsureaccclass where contno=( " +
    			" select contno from lcpol where grpcontno='"+pLPEdorItemSchema.getGrpContNo()+"' " +
    			" and poltypeflag='2' fetch first 1 rows only )  fetch first 1 rows only with ur ");
    	if(lastBalaDate!=null&&!"".equals(lastBalaDate)){
    		if(CommonBL.stringToDate(edorValidate).before(CommonBL.stringToDate(lastBalaDate))){
    			mErrors.addOneError("解约生效日必须大于或者等于保单最后一次月结日期！");
    			return false;   
    		}else{
//    			if(CommonBL.changeMonth(lastBalaDate, 1).before(CommonBL.stringToDate(edorValidate))){
//					CError tError = new CError();
//					tError.moduleName = "PEdorCTAppConfirmBL";
//					tError.functionName = "prepareData";
//					tError.errorMessage = "解约生效日和保单最后一次月结日期的间隔不能超过1个月!";
//					mErrors.addOneError(tError);    				
//    				return false;   
//    			}
    		}
    	}else{
    		mErrors.addOneError("查询保单最后一次月结信息失败！");
			return false;   
    	}
    	
    	LCPolDB tLCPolDB = new LCPolDB();
    	LCPolSet tLCPolSet = new LCPolSet();
    	tLCPolDB.setContNo(tLCContSchema.getContNo());
    	tLCPolSet = tLCPolDB.query();
    	for(int i=1;i<=tLCPolSet.size();i++){
    		//首先校验是否做过老年护理金满期领取
    		String checkSQL = "select 1 from LJAGetEndorse where FeeOperationType = 'UM' and PolNo = '"+tLCPolSet.get(i).getPolNo()+"' with ur ";
    		String hasUM = new ExeSQL().getOneValue(checkSQL);
    		if(hasUM != null && !"".equals(hasUM)){
    			if(!creatLPInsureAcc(tLCPolSet.get(i).getPolNo(),pLPEdorItemSchema)){
    				return false;
    			}
    			continue;
    		}
    		
    		//团险万能解约开始
    		pLPEdorItemSchema.setInsuredNo(tLCPolSet.get(i).getInsuredNo());
    		VData tVData = new VData();
    		GlobalInput mGI = new GlobalInput();
    		mGI.ManageCom = pLPEdorItemSchema.getManageCom();
    		mGI.Operator = pLPEdorItemSchema.getOperator();
    		mGI.ComCode = mGI.ManageCom;
    		tVData.add(mGI);
    		tVData.add(pLPEdorItemSchema);
//			对保单用保证利率进行月结    		
    		UliBQInsuAccBala tUliBQInsuAccBala = new UliBQInsuAccBala();
    		//针对农民工险种做解约，370301只有公共账户进行月结。20180408 lzj
    		if(!("370301".equals(tLCPolSet.get(i).getRiskCode())&&"0".equals(tLCPolSet.get(i).getPolTypeFlag()))&&!("370301".equals(tLCPolSet.get(i).getRiskCode())&&"1".equals(tLCPolSet.get(i).getPolTypeFlag()))){
    			if(!tUliBQInsuAccBala.submitData(tVData, "",tLCContSchema.getGrpContNo())){
    				mErrors.addOneError("保单:" + mLPEdorItemSchema.getContNo() + " 结算失败; ");
    				return false;
    			}
    		}
    		
    	}
        		//计算团险万能解约费用
    	if(!("370301".equals(tLCPolSet.get(1).getRiskCode())&&"0".equals(tLCPolSet.get(1).getPolTypeFlag()))&&!("370301".equals(tLCPolSet.get(1).getRiskCode())&&"1".equals(tLCPolSet.get(1).getPolTypeFlag()))){
    		if(!getULIZTData(pLPEdorItemSchema)){
    			mErrors.addOneError("保单:" + mLPEdorItemSchema.getContNo() + " 计算解约费用失败; ");
    			return false;                			
    		}
    	}
    	
        return true;
    }    
    
    /**
     * 准备需要退保的数据，单个个人保单退保入口，LPEdorItemSchema中只需要PolNo、EdorNo和EdorType即可
     * @return
     */
    public boolean getULIZTData(LPEdorItemSchema pLPEdorItemSchema) {
        try {
            //得到序要退保的被保人
            LCInsuredSet tLCInsuredSet = getLPInsuredSet(pLPEdorItemSchema);
            for (int t = 1; t <= tLCInsuredSet.size(); t++) {
                //得到被保人的险种
                LPPolSet tLPPolSet = getInsuredPol(pLPEdorItemSchema,tLCInsuredSet.get(t));
                //团险万能产品肯定为单险种
                if(tLPPolSet.size()!=1){
					CError tError = new CError();
					tError.moduleName = "PEdorCTAppConfirmBL";
					tError.functionName = "prepareData";
					tError.errorMessage = "line205-获取保单"+tLCInsuredSet.get(t).getContNo()+"险种信息失败！";
					mErrors.addOneError(tError);
                    return false;
                }
                LPPolSchema tLPPolSchema=tLPPolSet.get(1);

                //对应月结，校验是否做过老年护理金满期领取
                String checkSQL = "select 1 from LJAGetEndorse where FeeOperationType = 'UM' and PolNo = '"+tLPPolSchema.getPolNo()+"' with ur ";
                String hasUM = new ExeSQL().getOneValue(checkSQL);
                if(hasUM != null && !"".equals(hasUM)){
                	continue;
                }
//              解约管理费率
            	System.out.println("解约管理费率开始计算");
            	int tpolyear=PubFun.calPolYear(tLPPolSchema.getCValiDate(), mLPEdorItemSchema.getEdorValiDate());
            	String tfee = "";
            	String polRateSQL = "SELECT extractrate FROM LCRISKZTFEE WHERE grpcontno = '"+tLPPolSchema.getGrpContNo()+"'  and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear;
            	tfee = new ExeSQL().getOneValue(polRateSQL);
                if ((tfee == null) || (tfee.equals(""))){
                	tfee=new ExeSQL().getOneValue("select extractrate from lmriskztfee where riskcode='"+tLPPolSchema.getRiskCode()+"' and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear);
                	if ((tfee == null) || (tfee.equals(""))){
                		mErrors.addOneError("查询管理费为空！");
                		return false;
                	}
                }
            	double feeRate = Double.parseDouble(tfee); 
            	
            	
                if("2".equals(tLPPolSchema.getPolTypeFlag())){
                	//获取公共账户保险账户号码
                	String insuaccno=new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLPPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='3')) ");
                    LPInsureAccClassSet tLPGAccClassSet=getAccClass(tLPPolSchema,insuaccno);
                	if(tLPGAccClassSet.size()!=1){
                		mErrors.addOneError("line238-获取个人账户单位缴费账户失败！");
                        return false;
                	} 
                	LPInsureAccClassSchema tLPGAccClassSchema=tLPGAccClassSet.get(1);
                	
//                	公共账户的退保费用
                	double tZTfee=CommonBL.carry(tLPGAccClassSchema.getInsuAccBala()*feeRate);
//                	公共账户退保管理费用轨迹
                	setLPAccTrace(tLPGAccClassSchema, -tZTfee,"TM",null);
//                	个人账户退保费用轨迹       
                	setLPAccTrace(tLPGAccClassSchema, -(tLPGAccClassSchema.getInsuAccBala()-tZTfee),pLPEdorItemSchema.getEdorType(),null);            	

//                	该被保人单位账户所退费用合计
                	double tGetGRPMoney=tLPGAccClassSchema.getInsuAccBala()-tZTfee;
                	
                	LJSGetEndorseSchema tGLJSGetEndorseSchema = getLJSGetEndorse(pLPEdorItemSchema,tLPPolSchema,tLPGAccClassSchema, Arith.round(-tGetGRPMoney, 5), mGlobalInput);
                    if(tGLJSGetEndorseSchema==null){
                    	mErrors.addOneError("保全补退费表生成失败！");
                        return false;
                    }                
                    
                    mLJSGetEndorseSet.add(tGLJSGetEndorseSchema);
                    
//                	处理帐户余额为0,确认时交换
                	if(!(setLPAcc(tLPGAccClassSchema,0)&&setLPAcc(tLPGAccClassSchema,0))){
                        return false;
                	}
                	
                }else{
                //为保单制作个单保全主表数据
                LPEdorItemSchema tLPEdorItemSchema = pLPEdorItemSchema.getSchema();
                tLPEdorItemSchema.setPolNo(tLPPolSchema.getPolNo());

                
                //获取个人账户单位部分保险账户号码
                String tGrpInsuNo=new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLPPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='4')) ");
                LPInsureAccClassSet tLPGrpAccClassSet=getAccClass(tLPPolSchema,tGrpInsuNo);
            	if(tLPGrpAccClassSet.size()!=1){
            		mErrors.addOneError("line238-获取个人账户单位缴费账户失败！");
                    return false;
            	}
            	LPInsureAccClassSchema tLPGrpAccClassSchema=tLPGrpAccClassSet.get(1);
            	
//            	获取个人账户个人部分保险账户号码
                String tInsuNo = new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLPPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')) ");
                LPInsureAccClassSet tLPInsuredAccClassSet=getAccClass(tLPPolSchema,tInsuNo);
            	if(tLPInsuredAccClassSet.size()!=1){
            		mErrors.addOneError("line238-获取个人账户个人缴费账户失败！");
                    return false;
            	}
            	LPInsureAccClassSchema tLPInsuredAccClassSchema=tLPInsuredAccClassSet.get(1);
            	
            	
//            	个人账户单位部分的退保费用
            	double tZTGrpfee=CommonBL.carry(tLPGrpAccClassSchema.getInsuAccBala()*feeRate);
//            	个人账户单位部分退保管理费用轨迹
            	setLPAccTrace(tLPGrpAccClassSchema, -tZTGrpfee,"TM",null);
//            	个人账户单位部分退保费用轨迹       
            	setLPAccTrace(tLPGrpAccClassSchema, -(tLPGrpAccClassSchema.getInsuAccBala()-tZTGrpfee),pLPEdorItemSchema.getEdorType(),null);            	
            	
            	//个人账户个人部分的退保费用
            	double tZTfee=CommonBL.carry(tLPInsuredAccClassSchema.getInsuAccBala()*feeRate);
//            	个人账户个人部分退保管理费用轨迹
            	setLPAccTrace(tLPInsuredAccClassSchema, -tZTfee,"TM",null);
//            	个人账户个人部分退保费用轨迹       
            	setLPAccTrace(tLPInsuredAccClassSchema, -(tLPInsuredAccClassSchema.getInsuAccBala()-tZTfee),pLPEdorItemSchema.getEdorType(),null);            	
            	

//            	该被保人单位账户所退费用合计
            	double tGetGRPMoney=tLPGrpAccClassSchema.getInsuAccBala()-tZTGrpfee;
            	
//            	该被保人单位账户所退费用合计
            	double tGetMoney=tLPInsuredAccClassSchema.getInsuAccBala()-tZTfee;            	
            	  
//              生成ljsgetendorse
            	LJSGetEndorseSchema tLJSGetEndorseSchema1 = getLJSGetEndorse(pLPEdorItemSchema ,tLPPolSchema,tLPGrpAccClassSchema, Arith.round(-tGetGRPMoney, 5), mGlobalInput);
                if(tLJSGetEndorseSchema1==null){
                	mErrors.addOneError("保全补退费表生成失败！");
                    return false;
                }
                mLJSGetEndorseSet.add(tLJSGetEndorseSchema1);

            	LJSGetEndorseSchema tLJSGetEndorseSchema = getLJSGetEndorse(pLPEdorItemSchema,tLPPolSchema,tLPInsuredAccClassSchema, Arith.round(-tGetMoney, 5), mGlobalInput);
                if(tLJSGetEndorseSchema==null){
                	mErrors.addOneError("保全补退费表生成失败！");
                    return false;
                }                
                
                mLJSGetEndorseSet.add(tLJSGetEndorseSchema);
                
//            	处理帐户余额为0,确认时交换
            	if(!(setLPAcc(tLPGrpAccClassSchema,0)&&setLPAcc(tLPInsuredAccClassSchema,0))){
                    return false;
            	}
            }
            }
        } catch (Exception ex) {
            mErrors.addOneError(ex.getMessage());
            ex.printStackTrace();
            return false;
        }

        return true;
    }       
    
    private LCInsuredSet getLPInsuredSet(LPEdorItemSchema tLPEdorItemSchema) {
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();

            tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
            tLCInsuredSet = tLCInsuredDB.query();
            if (tLCInsuredDB.mErrors.needDealError()) {
                CError.buildErr(this, "查询被保险人失败！");
                return null;
            }
        return tLCInsuredSet;
    } 
    
    private LPPolSet getInsuredPol(LPEdorItemSchema pLPEdorItemSchema,
            LCInsuredSchema tLCInsuredSchema) {
	Reflections ref = new Reflections();
	LCPolDB tLCPolDB = new LCPolDB();
	tLCPolDB.setContNo(tLCInsuredSchema.getContNo());
	tLCPolDB.setInsuredNo(tLCInsuredSchema.getInsuredNo());
	LCPolSet tLCPolSet = tLCPolDB.query();
	if (tLCPolDB.mErrors.needDealError()) {
	CError.buildErr(this, "查询保单失败！");
	return null;
	}
	
	LPPolSet tLPPolSet = new LPPolSet();
	for (int i = 1; i <= tLCPolSet.size(); i++) {
	LPPolSchema tLPPolSchema = new LPPolSchema();
	ref.transFields(tLPPolSchema, tLCPolSet.get(i));
	tLPPolSchema.setEdorNo(pLPEdorItemSchema.getEdorNo());
	tLPPolSchema.setEdorType(pLPEdorItemSchema.getEdorType());
	tLPPolSet.add(tLPPolSchema);
	}
	return tLPPolSet;
	}    
    
    /**
     * 获取账户
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param getMoney double
     */
    private LPInsureAccClassSet getAccClass(LPPolSchema tLPPolSchema, String tInsuAccNo)
    {
        LPInsureAccClassDB tLPInsureAccClassDB = new LPInsureAccClassDB();
        tLPInsureAccClassDB.setEdorNo(tLPPolSchema.getEdorNo());
        tLPInsureAccClassDB.setEdorType(tLPPolSchema.getEdorType());
        tLPInsureAccClassDB.setPolNo(tLPPolSchema.getPolNo());
        tLPInsureAccClassDB.setInsuAccNo(tInsuAccNo);
        return tLPInsureAccClassDB.query();
    }
    
    /**
     * 设置帐户金额轨迹
     * @param tLPInsureAccClassSchema LPInsureAccClassSchema 轨迹对应的当前账户
     * @param tMoney double
     * @param tMoneyType String 
     * @param tAILPInsureAccClassSchema LPInsureAccClassSchema 轨迹来源或者去向账户
     */
    private void setLPAccTrace(LPInsureAccClassSchema tLPInsureAccClassSchema, double tMoney ,String tMoneyType,LPInsureAccClassSchema tAILPInsureAccClassSchema)
    {
        String serialNo;

            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPInsureAccTraceSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPInsureAccTraceSchema.setGrpContNo(tLPInsureAccClassSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(tLPInsureAccClassSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(tLPInsureAccClassSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(tLPInsureAccClassSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(tLPInsureAccClassSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(tLPInsureAccClassSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(tLPInsureAccClassSchema.getPayPlanCode());
        tLPInsureAccTraceSchema.setOtherNo(mLPEdorItemSchema.getEdorNo());
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(tMoneyType);
        tLPInsureAccTraceSchema.setMoney(tMoney);  
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mLPEdorItemSchema.getEdorValiDate());
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setManageCom(tLPInsureAccClassSchema.getManageCom());
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        if(!(tAILPInsureAccClassSchema==null)){
        tLPInsureAccTraceSchema.setAIPolNo(tAILPInsureAccClassSchema.getPolNo());
        tLPInsureAccTraceSchema.setAIInsuAccNo(tAILPInsureAccClassSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setAIPayPlanCode(tAILPInsureAccClassSchema.getPayPlanCode());
        }
        map.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }    
    
    /**
     * 生成交退费记录
     * @param aLPEdorItemSchema
     * @param aLPPolSchema
     * @param aOperationType
     * @param aFeeType
     * @param aGetMoney
     * @param aGlobalInput
     * @return LJSGetEndorseSchema
     */
    public LJSGetEndorseSchema getLJSGetEndorse(LPEdorItemSchema aLPEdorItemSchema, LPPolSchema aLPPolSchema,
    		LPInsureAccClassSchema tLPInsuredAccClassSchema, double aGetMoney, GlobalInput aGlobalInput)
    {
        try
        {
            LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();

            //生成批改交退费表
            tLJSGetEndorseSchema.setGetNoticeNo(aLPPolSchema.getEdorNo()); //给付通知书号码
            tLJSGetEndorseSchema.setEndorsementNo(aLPPolSchema.getEdorNo());
            tLJSGetEndorseSchema.setGrpContNo(aLPPolSchema.getGrpContNo());
            tLJSGetEndorseSchema.setContNo(aLPPolSchema.getContNo());
            tLJSGetEndorseSchema.setGrpPolNo(aLPPolSchema.getGrpPolNo());
            tLJSGetEndorseSchema.setPolNo(aLPPolSchema.getPolNo());
            tLJSGetEndorseSchema.setFeeOperationType(aLPPolSchema.getEdorType());
            tLJSGetEndorseSchema.setGetDate(aLPEdorItemSchema.getEdorValiDate());
            tLJSGetEndorseSchema.setGetMoney(aGetMoney);
            tLJSGetEndorseSchema.setFeeOperationType(aLPPolSchema.getEdorType()); //补退费业务类型
            tLJSGetEndorseSchema.setFeeFinaType("TB"); //补退费财务类型
            tLJSGetEndorseSchema.setPayPlanCode(tLPInsuredAccClassSchema.getPayPlanCode()); //无作用
            tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA); //无作用，但一定要，转ljagetendorse时非空
            tLJSGetEndorseSchema.setOtherNo(aLPEdorItemSchema.getEdorNo()); //其他号码置为保全批单号
            tLJSGetEndorseSchema.setOtherNoType("3"); //保全给付
            tLJSGetEndorseSchema.setGetFlag("0");
            tLJSGetEndorseSchema.setManageCom(aLPPolSchema.getManageCom());
            tLJSGetEndorseSchema.setAgentCode(aLPPolSchema.getAgentCode());
            tLJSGetEndorseSchema.setAgentCom(aLPPolSchema.getAgentCom());
            tLJSGetEndorseSchema.setAgentGroup(aLPPolSchema.getAgentGroup());
            tLJSGetEndorseSchema.setAgentType(aLPPolSchema.getAgentType());
            tLJSGetEndorseSchema.setInsuredNo(aLPPolSchema.getInsuredNo());
            tLJSGetEndorseSchema.setKindCode(aLPPolSchema.getKindCode());
            tLJSGetEndorseSchema.setAppntNo(aLPPolSchema.getAppntNo());
            tLJSGetEndorseSchema.setRiskCode(aLPPolSchema.getRiskCode());
            tLJSGetEndorseSchema.setRiskVersion(aLPPolSchema.getRiskVersion());
            tLJSGetEndorseSchema.setHandler(aLPPolSchema.getHandler());
            tLJSGetEndorseSchema.setApproveCode(aLPPolSchema.getApproveCode());
            tLJSGetEndorseSchema.setApproveDate(aLPPolSchema.getApproveDate());
            tLJSGetEndorseSchema.setApproveTime(aLPPolSchema.getApproveTime());
            tLJSGetEndorseSchema.setOperator(aGlobalInput.Operator);
            tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
            return tLJSGetEndorseSchema;
        }
        catch (Exception ex)
        {
            mErrors.addOneError(new CError("建立批改补退费信息异常！"));
            return null;
        }
    }
 
    /**
     * 设置账户余额,包括acc和accclass两个表的P表
     * @param tLPInsureAccClassSchema LPInsureAccClassSchema
     * @param getMoney double
     */
    private boolean setLPAcc(LPInsureAccClassSchema tLPInsureAccClassSchema, double leftMoney)
    {
        LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
        LPInsureAccDB tLPInsureAccDB = new LPInsureAccDB();
        tLPInsureAccDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPInsureAccDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPInsureAccDB.setPolNo(tLPInsureAccClassSchema.getPolNo());
        tLPInsureAccDB.setInsuAccNo(tLPInsureAccClassSchema.getInsuAccNo());
        if(!tLPInsureAccDB.getInfo()){
        	CError.buildErr(this, "line455-获取账户P表失败！");
        	return false;
        }
        tLPInsureAccSchema=tLPInsureAccDB.getSchema();
        tLPInsureAccSchema.setInsuAccBala(leftMoney);
        map.put(tLPInsureAccSchema, "DELETE&INSERT");
        tLPInsureAccClassSchema.setInsuAccBala(leftMoney);
        map.put(tLPInsureAccClassSchema, "DELETE&INSERT");
        return true;
    }
    
    /**
     * 获取账户
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param getMoney double
     */
    private LCInsureAccClassSet getAccClassC(LPPolSchema tLPPolSchema, String tInsuAccNo)
    {
        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setContNo(tLPPolSchema.getContNo());
        tLCInsureAccClassDB.setPolNo(tLPPolSchema.getPolNo());
        tLCInsureAccClassDB.setInsuAccNo(tInsuAccNo);
        return tLCInsureAccClassDB.query();
    }      

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        double aTotalReturn = 0;
        double tChgPrem = 0;
        

        //普通险进行退保
        if (!getZTData(mLPEdorItemSchema.getSchema()))
        {
            System.out.println("退保计算失败");
            return false;
        }

        //如果涉及补退费，则按照描述表中的财务类型进行修改
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        LJSGetEndorseSet tempLJSGetEndorseSet = new LJSGetEndorseSet();
        LJSGetEndorseSet tLJSGetEndorseSet = mLJSGetEndorseSet;
        if (tLJSGetEndorseSet != null && tLJSGetEndorseSet.size() > 0)
        {
            for (int k = 1; k <= tLJSGetEndorseSet.size(); k++)
            {
                tLJSGetEndorseSchema = tLJSGetEndorseSet.get(k);
                tLJSGetEndorseSchema.setGetFlag("1");
                tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);

                //从描述表中获取财务接口类型，modify by Minim at 2003-12-23
                String finType = BqCalBL.getFinType(mLPEdorItemSchema.
                        getEdorType(), "TB", tLJSGetEndorseSchema.getPolNo());
                if (finType.equals(""))
                {
                    CError.buildErr(this, "在LDCode1中缺少保全财务接口转换类型编码!");
                    return false;
                }
                //不检验LX的财务类型的
                if(!"LX".equals(tLJSGetEndorseSchema.getFeeFinaType()))
                {
                    tLJSGetEndorseSchema.setFeeFinaType(finType);
                }
                tempLJSGetEndorseSet.add(tLJSGetEndorseSchema);
                System.out.println("GetNoticeNo :" +
                                   tLJSGetEndorseSchema.getGetNoticeNo());

                //计算出总费用
                aTotalReturn =Arith.round(aTotalReturn, 2);
                double tempGetMoney = Arith.round(tLJSGetEndorseSchema.getGetMoney(),2);
                aTotalReturn = Arith.round((aTotalReturn + tempGetMoney),2);
            }
        }

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mLPEdorItemSchema.getPolNo());
        tLCPolDB.getInfo();
        LCPolSchema tLCPolSchema = tLCPolDB.getSchema();

        tChgPrem = tChgPrem + tLCPolSchema.getSumPrem();

        //处理挂帐问题
        //查找余额退还
        if (tLCPolSchema.getLeavingMoney() > 0)
        {
            LJSGetEndorseSchema tLJS = new LJSGetEndorseSchema();
            if (tempLJSGetEndorseSet.size() > 0)
            {
                tLJS.setSchema(tLJSGetEndorseSchema);

                //从描述表中获取财务接口类型，modify by Minim at 2003-12-23
                String finType = BqCalBL.getFinType(mLPEdorItemSchema.
                        getEdorType(), "YE", tLJSGetEndorseSchema.getPolNo());
                if (finType.equals(""))
                {
                    CError.buildErr(this, "在LDCode1中缺少保全财务接口转换类型编码!");
                    return false;
                }
                tLJS.setFeeFinaType(finType);

                tLJS.setGetMoney(tLCPolSchema.getLeavingMoney());
                tLJS.setPolNo(tLCPolSchema.getPolNo());
                tLJS.setPayPlanCode("0");
            }
            else
            {
                BqCalBase tBqCalBase = new BqCalBase();
                BqCalBL tBqCalBL = new BqCalBL(tBqCalBase, "", "");

                //从描述表中获取财务接口类型，modify by Minim at 2003-12-23
                String finType = BqCalBL.getFinType(mLPEdorItemSchema.
                        getEdorType(), "YE", tLJSGetEndorseSchema.getPolNo());
                if (finType.equals(""))
                {
                    CError.buildErr(this, "在LDCode1中缺少保全财务接口转换类型编码!");
                    return false;
                }

                tLJS = tBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
                                                  tLCPolSchema,
                                                  null,
                                                  finType,
                                                  tLCPolSchema.getLeavingMoney(),
                                                  mGlobalInput);
                tLJS.setOperator(mGlobalInput.Operator);
            }

            aTotalReturn =Arith.round( aTotalReturn ,2)+ Arith.round(tLCPolSchema.getLeavingMoney(),2);
            System.out.println("GetNoticeNo :" +
                               tLJSGetEndorseSchema.getGetNoticeNo());
            tempLJSGetEndorseSet.add(tLJS);
        }
//    System.out.println("--------allmoney"+mLPEdorItemSchema.getGetMoney());
        
//      开始处理由于当前贷款导致保单失效的情况
        if(IsLoan){
//        	贷款利息
        	double tInterest=0;
            LCPolDB tLCLoanPolDB = new LCPolDB();
            tLCLoanPolDB.setPolNo(mLPLoanSchema.getPolNo());
            tLCLoanPolDB.getInfo();
            LCPolSchema tLCLoanPolSchema = tLCLoanPolDB.getSchema();
            LJSGetEndorseSchema tLoanLJS = new LJSGetEndorseSchema();
            LJSGetEndorseSchema tLoanLXLJS = new LJSGetEndorseSchema();
            BqCalBase tLoanBqCalBase = new BqCalBase();
            BqCalBL tLoanBqCalBL = new BqCalBL(tLoanBqCalBase, "", "");
            tLoanLJS=tLoanBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
            		tLCLoanPolSchema,
                    null,
                    "RFBF",
                    mLPLoanSchema.getSumMoney(),
                    mGlobalInput);
            if(IsLoanAbate){
            	tInterest=Arith.round(-mLPLoanSchema.getSumMoney()-aTotalReturn ,2);
            	aTotalReturn=0;
            }else{
            	//查询保单信息  --wujun
            	LCContDB tLCContDB = new LCContDB();
            	tLCContDB.setContNo(tLCLoanPolSchema.getContNo());
            	tLCContDB.getInfo();
            	LCContSchema tLCContSchema = tLCContDB.getSchema();
//    			获取贷款利息 
	    		AccountManage tAccountManage=new AccountManage();
	    		tInterest=CommonBL.carry(tAccountManage.getLoanInterest(mLPLoanSchema.getLoanDate(), mLPLoanSchema.getSumMoney(),tLCContSchema.getManageCom(),tLCContSchema.getPrem()+"", mLPEdorItemSchema.getEdorValiDate(), tLCLoanPolSchema.getRiskCode()));
	    		if(tInterest==-1){
	    			// @@错误处理
					System.out.println("PEdorCTAppConfirmBL+prepareData++--");
					CError tError = new CError();
					tError.moduleName = "PEdorCTAppConfirmBL";
					tError.functionName = "prepareData";
					tError.errorMessage = "贷款利息计算失败!";
					mErrors.addOneError(tError);
					return false;
	    		}
//	    		如果解约金额小于贷款本息合计,则阻断保全理算
	    		if((tInterest+mLPLoanSchema.getSumMoney()+aTotalReturn)>0){
	    			// @@错误处理
					System.out.println("PEdorCTAppConfirmBL+prepareData++--");
					CError tError = new CError();
					tError.moduleName = "PEdorCTAppConfirmBL";
					tError.functionName = "prepareData";
					tError.errorMessage = "贷款本息合计大于解约所退保费!";
					mErrors.addOneError(tError);
					return false;
	    		}
	    		aTotalReturn=Arith.round(tInterest+mLPLoanSchema.getSumMoney()+aTotalReturn ,2);
            }
            tLoanLXLJS=tLoanBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
            		tLCLoanPolSchema,
                    null,
                    "RFLX",
                    tInterest,
                    mGlobalInput);
            tempLJSGetEndorseSet.add(tLoanLJS);
            tempLJSGetEndorseSet.add(tLoanLXLJS);
            
            //已经还款
    		mLPLoanSchema.setPayOffFlag("1");
    		mLPLoanSchema.setPayOffDate(mLPEdorItemSchema.getEdorValiDate());
            mLPLoanSchema.setModifyDate(mCurrentDate);
            mLPLoanSchema.setModifyTime(mCurrentTime);

            map.put(mLPLoanSchema, "DELETE&INSERT");
            
//          设置还款信息
//   		 生成“保全还款业务表”数据-------------------->
    		// 记录还款轨迹
    		mLPReturnLoanSchema.setPolNo(mLPLoanSchema.getPolNo());
    		mLPReturnLoanSchema.setEdorType(mLPLoanSchema.getEdorType());
    		mLPReturnLoanSchema.setSerialNo(mLPLoanSchema.getSerialNo()); // 是否生成？
    		mLPReturnLoanSchema.setActuGetNo(mLPLoanSchema.getActuGetNo());
    		mLPReturnLoanSchema.setLoanType(mLPLoanSchema.getLoanType());
    		mLPReturnLoanSchema.setOrderNo(mLPLoanSchema.getOrderNo());
    		mLPReturnLoanSchema.setLoanDate(mLPLoanSchema.getLoanDate());
//    		计算利息截止到前一天
    		mLPReturnLoanSchema.setPayOffDate(mLPEdorItemSchema.getEdorValiDate());
    		mLPReturnLoanSchema.setSumMoney(mLPLoanSchema.getSumMoney());
    		mLPReturnLoanSchema.setInputFlag(mLPLoanSchema.getInputFlag());
    		mLPReturnLoanSchema.setInterestType(mLPLoanSchema.getInterestType());
    		mLPReturnLoanSchema.setInterestRate(mLPLoanSchema.getInterestRate());
    		mLPReturnLoanSchema.setInterestMode(mLPLoanSchema.getInterestMode());
    		mLPReturnLoanSchema.setRateCalType(mLPLoanSchema.getRateCalType());
    		mLPReturnLoanSchema.setRateCalCode(mLPLoanSchema.getRateCalCode());
    		mLPReturnLoanSchema.setSpecifyRate(mLPLoanSchema.getSpecifyRate());
    		mLPReturnLoanSchema.setLeaveMoney("0");
    		mLPReturnLoanSchema.setPayOffFlag("1");
    		mLPReturnLoanSchema.setOperator(this.mGlobalInput.Operator);
    		mLPReturnLoanSchema.setMakeDate(mCurrentDate);
    		mLPReturnLoanSchema.setMakeTime(mCurrentTime);
    		mLPReturnLoanSchema.setModifyDate(mCurrentDate);
    		mLPReturnLoanSchema.setModifyTime(mCurrentTime);
    		mLPReturnLoanSchema.setEdorNo(mLPLoanSchema.getActuGetNo());//还款批单号
    		mLPReturnLoanSchema.setLoanNo(mLPLoanSchema.getEdorNo()); // 借款批单号
    		mLPReturnLoanSchema.setReturnMoney(mLPLoanSchema.getSumMoney());
    		mLPReturnLoanSchema.setReturnInterest(tInterest);
            map.put(mLPReturnLoanSchema, "DELETE&INSERT");
        }
        
//        将生效日期设置回来
        if(IsLoanAbate){
        	mLPEdorItemSchema.setEdorValiDate(mEdorValiDate);
        }
        
        if(CommonBL.hasBonusRisk(mLPEdorItemSchema.getContNo())&&EdorHasBonusRisk(mLPEdorItemSchema.getContNo())){
        	LPPolDB tLPBonusPolDB = new LPPolDB();
        	LPPolSet tLPBonusPolSet = new LPPolSet();
        	tLPBonusPolSet=tLPBonusPolDB.executeQuery("select * from lppol where contno='"+mLPEdorItemSchema.getContNo()+"' and edorno='"+mLPEdorItemSchema.getEdorNo()+"' and riskcode in (select riskcode from lmriskapp where risktype4='2') with ur");
        	if(tLPBonusPolSet.size()==1){
                LJSGetEndorseSchema tBonusLJS = new LJSGetEndorseSchema();
                LJSGetEndorseSchema tBonusLXLJS = new LJSGetEndorseSchema();
                BqCalBase tBonusBqCalBase = new BqCalBase();
                BqCalBL tBonusBqCalBL = new BqCalBL(tBonusBqCalBase, "", "");
            	LCPolDB tLCBonusPolDB = new LCPolDB();
            	tLCBonusPolDB.setPolNo(tLPBonusPolSet.get(1).getPolNo());
            	tLCBonusPolDB.getInfo();
            	LCPolSchema tLCBonusPolSchema = new LCPolSchema();
            	tLCBonusPolSchema.setSchema(tLCBonusPolDB.getSchema());
            	String lastbonusdate=mExeSQL.getOneValue("select max(SGetDate) from LOBonusPol where polno='"+tLCBonusPolSchema.getPolNo()+"' ");
            	String tCvaliYear=mExeSQL.getOneValue("select year('"+tLCBonusPolSchema.getCValiDate()+"') from dual with ur");
            	String tFXdate=mExeSQL.getOneValue("select max(enddate) from lccontstate where enddate is not null and polno='"+tLCBonusPolSchema.getPolNo()+"' and statetype='Available' ");
            	if(tCvaliYear==null||tCvaliYear.equals("")||tCvaliYear.equals("null")){
            		// @@错误处理
        			System.out.println("BonusGetBL+GetBonus++--");
        			CError tError = new CError();
        			tError.moduleName = "BonusGetBL";
        			tError.functionName = "GetBonus";
        			tError.errorMessage = "获取生效日期失败";
        			mErrors.addOneError(tError);
        			return false;
            	}
            	LMRiskBonusDB tLMRiskBonusDB=new LMRiskBonusDB();
            	tLMRiskBonusDB.setRiskCode(tLCBonusPolSchema.getRiskCode());
            	LMRiskBonusSet tLMRiskBonusSet=tLMRiskBonusDB.query();
            	if(tLMRiskBonusSet.size()!=1){
            		// @@错误处理
        			System.out.println("BonusGetBL+GetBonus++--");
        			CError tError = new CError();
        			tError.moduleName = "BonusGetBL";
        			tError.functionName = "GetBonus";
        			tError.errorMessage = "获取分红公式失败";
        			mErrors.addOneError(tError);
        			return false;
            	}
            	if(lastbonusdate.equals("")){
            		if(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),mLPEdorItemSchema.getEdorValiDate(), "Y")>0){
            			LMRiskBonusRateDB tLMRiskBonusRateDB =new LMRiskBonusRateDB();
                    	LMRiskBonusRateSet tLMRiskBonusRateSet=new LMRiskBonusRateSet();
                    	String tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear=(case when month('"+tLCBonusPolSchema.getCValiDate()+"')>6 then char(year('"+tLCBonusPolSchema.getCValiDate()+"')+1) else char(year('"+tLCBonusPolSchema.getCValiDate()+"')) end) " +
                		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
                    	System.out.println("tratesql:"+tratesql);
                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
                    	if(tLMRiskBonusRateSet.size()<1){
                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
                    		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
                        	System.out.println("tratesql:"+tratesql);
                        	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
                        	if(tLMRiskBonusRateSet.size()<1){
                        		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
                        		"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
                            	System.out.println("tratesql:"+tratesql);
                            	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
                            	if(tLMRiskBonusRateSet.size()<1){
                            		// @@错误处理
        							System.out.println("PEdorCTAppConfirmBL+prepareData++--");
        							CError tError = new CError();
        							tError.moduleName = "PEdorCTAppConfirmBL";
        							tError.functionName = "prepareData";
        							tError.errorMessage = "获取分红率失败!";
        							mErrors.addOneError(tError);
        							return false;
                            	}
                        	}
                    	}
                    	Calculator tCalculator = new Calculator();
                        tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
                        tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
                        tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
                        tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
                        tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
                        tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
                        tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
                        tCalculator.addBasicFactor("appyear",tCvaliYear);
                        tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
                        tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),mLPEdorItemSchema.getEdorValiDate(), "Y")-1));
//                        tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
                        tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
                        tCalculator.addBasicFactor("enddate",PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null));
                        double tHL=0;
                        double tHLLXnoAcc=0;
                        if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null)).equals(tFXdate)){
                        	tCalculator.addBasicFactor("startdate",tFXdate);
                            tCalculator.addBasicFactor("caltype","FX");
                            tHL= Double.parseDouble((tCalculator.calculate()));
                            tHLLXnoAcc=tHL;
                        }else if(tFXdate.equals("")){
                        	tCalculator.addBasicFactor("startdate",tLCBonusPolSchema.getCValiDate());
                            tCalculator.addBasicFactor("caltype","YX");
                            tHL= Double.parseDouble((tCalculator.calculate()));
                            tHLLXnoAcc=tHL;
                        }else{
                            tHL= 0;
                            tHLLXnoAcc=tHL;
                        }                        
                        tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear=(case when month('"+tLCBonusPolSchema.getCValiDate()+"')>6 then char(year('"+tLCBonusPolSchema.getCValiDate()+"')+2) else char(year('"+tLCBonusPolSchema.getCValiDate()+"')+1) end) " +
                		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
                    	System.out.println("tratesql:"+tratesql);
                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
                    	if(tLMRiskBonusRateSet.size()<1){
                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
                    		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
                        	System.out.println("tratesql:"+tratesql);
                        	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
                        	if(tLMRiskBonusRateSet.size()<1){
                        		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
                        		"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
                            	System.out.println("tratesql:"+tratesql);
                            	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
                            	if(tLMRiskBonusRateSet.size()<1){
                            		// @@错误处理
        							System.out.println("PEdorCTAppConfirmBL+prepareData++--");
        							CError tError = new CError();
        							tError.moduleName = "PEdorCTAppConfirmBL";
        							tError.functionName = "prepareData";
        							tError.errorMessage = "获取分红率失败!";
        							mErrors.addOneError(tError);
        							return false;
                            	}
                        	}
                    	}
                        
                    	tCalculator = new Calculator();
                        tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
                        tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
                        tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
                        tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
                        tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
                        tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
                        tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
                        tCalculator.addBasicFactor("appyear",tCvaliYear);
                        tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
                        tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),mLPEdorItemSchema.getEdorValiDate(), "Y")));
//                        tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
                        tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
                        if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null)).equals(tFXdate)&&PubFun.getBeforeDate(tFXdate, mLPEdorItemSchema.getEdorValiDate()).equals(tFXdate)){
                        	tCalculator.addBasicFactor("startdate",tFXdate);
                        }else{
                        	tCalculator.addBasicFactor("startdate",PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null));                        	
                        }                    	
                        tCalculator.addBasicFactor("enddate",mLPEdorItemSchema.getEdorValiDate());
                        tCalculator.addBasicFactor("caltype","JY");
                        tHL += Double.parseDouble(tCalculator.calculate());
                        tHL=CommonBL.carry(tHL);
                        tBonusLJS=tBonusBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
                        		tLCBonusPolSchema,
                                null,
                                "HLBF",
                                -tHL,
                                mGlobalInput);
                        aTotalReturn=CommonBL.carry(aTotalReturn-tHL);
                    	tempLJSGetEndorseSet.add(tBonusLJS);
                    	
	                	if(tLCBonusPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETINTEREST)){
	                		String tPolYear=String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),mLPEdorItemSchema.getEdorValiDate(), "Y"));
	                    	String trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
	                    				" and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+"-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+")))) ");
	                    	if(trate.equals("")){
	                    		trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
                    				" order by bonusyear desc fetch first 1 row only with ur  ");
	                    	}
	                    	tCalculator=new Calculator();
	                    	tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusInterestCode());
	                    	tCalculator.addBasicFactor("caltype1","JY");
	                    	String JYCalCode=tCalculator.calculate();
	                    	Calculator tCalculatorLX=new Calculator();
	                    	tCalculatorLX.setCalCode(JYCalCode);
	                    	tCalculatorLX.addBasicFactor("lastriskbonus", String.valueOf(tHLLXnoAcc));
	                    	tCalculatorLX.addBasicFactor("enddate", mLPEdorItemSchema.getEdorValiDate());
	                    	tCalculatorLX.addBasicFactor("rate", trate);
	                    	if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null)).equals(tFXdate)&&PubFun.getBeforeDate(tFXdate, mLPEdorItemSchema.getEdorValiDate()).equals(tFXdate)){
	                    		tCalculatorLX.addBasicFactor("lastaccountdate",tFXdate);
	                        }else{
	                        	tCalculatorLX.addBasicFactor("lastaccountdate",PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null));                        	
	                        }	                    	
	                    	double tHLLX= CommonBL.carry(Double.parseDouble(tCalculatorLX.calculate())-tHLLXnoAcc);
	                		tBonusLXLJS=tBonusBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
		                    		tLCBonusPolSchema,
		                            null,
		                            "HLLX",
		                            -tHLLX,
		                            mGlobalInput);
		                    aTotalReturn=CommonBL.carry(aTotalReturn-tHLLX);
		                	tempLJSGetEndorseSet.add(tBonusLXLJS);
	                	}                    	
            		}else{
	            		LMRiskBonusRateDB tLMRiskBonusRateDB =new LMRiskBonusRateDB();
	                	LMRiskBonusRateSet tLMRiskBonusRateSet=new LMRiskBonusRateSet();
	                	String tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear=(case when month('"+tLCBonusPolSchema.getCValiDate()+"')>6 then char(year('"+tLCBonusPolSchema.getCValiDate()+"')+1) else char(year('"+tLCBonusPolSchema.getCValiDate()+"')) end) " +
	            		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
	                	System.out.println("tratesql:"+tratesql);
	                	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                	if(tLMRiskBonusRateSet.size()<1){
	                		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
	                		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
	                    	System.out.println("tratesql:"+tratesql);
	                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                    	if(tLMRiskBonusRateSet.size()<1){
	                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
		                		"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
		                    	System.out.println("tratesql:"+tratesql);
		                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                    	if(tLMRiskBonusRateSet.size()<1){
		                    		// @@错误处理
									System.out.println("PEdorCTAppConfirmBL+prepareData++--");
									CError tError = new CError();
									tError.moduleName = "PEdorCTAppConfirmBL";
									tError.functionName = "prepareData";
									tError.errorMessage = "获取分红率失败!";
									mErrors.addOneError(tError);
									return false;
		                    	}
	                    	}
	                	}
	                	Calculator tCalculator = new Calculator();
	                    tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
	                    tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
	                    tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
	                    tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
	                    tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
	                    tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
	                    tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
	                    tCalculator.addBasicFactor("appyear",tCvaliYear);
	                    tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
	                    tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),mLPEdorItemSchema.getEdorValiDate(), "Y")));
	//                    tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
	                    tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
	                    if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, mLPEdorItemSchema.getEdorValiDate()).equals(tFXdate)){
		                	tCalculator.addBasicFactor("startdate",tFXdate);
	                    }else{
		                	tCalculator.addBasicFactor("startdate",tLCBonusPolSchema.getCValiDate());
	                    }
	                    tCalculator.addBasicFactor("enddate",mLPEdorItemSchema.getEdorValiDate());
	                    tCalculator.addBasicFactor("caltype","JY");
	                    double tHL= CommonBL.carry(tCalculator.calculate());
	                    tBonusLJS=tBonusBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
	                    		tLCBonusPolSchema,
	                            null,
	                            "HLBF",
	                            -tHL,
	                            mGlobalInput);
	                    aTotalReturn=CommonBL.carry(aTotalReturn-tHL);
	                	tempLJSGetEndorseSet.add(tBonusLJS);
            		}
            	}else{
            		if(PubFun.calInterval(lastbonusdate,mLPEdorItemSchema.getEdorValiDate(), "Y")>0){
            			String tFiscalYear=mExeSQL.getOneValue("select max(FiscalYear)+1 from LOBonusPol where polno='"+tLCBonusPolSchema.getPolNo()+"' ");
	            		LMRiskBonusRateDB tLMRiskBonusRateDB =new LMRiskBonusRateDB();
	                	LMRiskBonusRateSet tLMRiskBonusRateSet=new LMRiskBonusRateSet();
	                	String tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear='"+tFiscalYear+"' " +
	            		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
	                	System.out.println("tratesql:"+tratesql);
	                	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                	if(tLMRiskBonusRateSet.size()<1){
	                		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
	                		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
	                    	System.out.println("tratesql:"+tratesql);
	                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                    	if(tLMRiskBonusRateSet.size()<1){
	                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
		                		"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
		                    	System.out.println("tratesql:"+tratesql);
		                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                    	if(tLMRiskBonusRateSet.size()<1){
		                    		// @@错误处理
									System.out.println("PEdorCTAppConfirmBL+prepareData++--");
									CError tError = new CError();
									tError.moduleName = "PEdorCTAppConfirmBL";
									tError.functionName = "prepareData";
									tError.errorMessage = "获取分红率失败!";
									mErrors.addOneError(tError);
									return false;
		                    	}
	                    	}
	                	}
	                	Calculator tCalculator = new Calculator();
	                    tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
	                    tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
	                    tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
	                    tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
	                    tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
	                    tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
	                    tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
	                    tCalculator.addBasicFactor("appyear",tCvaliYear);
	                    tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
	                    tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),lastbonusdate, "Y")));
	//                    tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
	                    tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
	                    tCalculator.addBasicFactor("enddate",PubFun.calDate(lastbonusdate, 1, "Y", null));
                        double tHL=0;
                        double tHLbak=0;
                        if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, PubFun.calDate(lastbonusdate, 1, "Y", null)).equals(tFXdate)&&PubFun.getLaterDate(tFXdate, lastbonusdate).equals(tFXdate)){
                        	tCalculator.addBasicFactor("startdate",tFXdate);
                            tCalculator.addBasicFactor("caltype","FX");
                            tHL= Double.parseDouble((tCalculator.calculate()));
                            tHLbak=tHL;
                        }else if(tFXdate.equals("")){
                        	tCalculator.addBasicFactor("startdate",lastbonusdate);
                            tCalculator.addBasicFactor("caltype","YX");
                            tHL= Double.parseDouble((tCalculator.calculate()));
                            tHLbak=tHL;
                        }else if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, lastbonusdate).equals(lastbonusdate)){
                        	tCalculator.addBasicFactor("startdate",lastbonusdate);
                            tCalculator.addBasicFactor("caltype","YX");
                            tHL= Double.parseDouble((tCalculator.calculate()));
                            tHLbak=tHL;
                        }else if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, PubFun.calDate(lastbonusdate, 1, "Y", null)).equals(PubFun.calDate(lastbonusdate, 1, "Y", null))){
                        	tHL= 0;
                            tHLbak=tHL;
                        }
	                    
	                    tFiscalYear=mExeSQL.getOneValue("select max(FiscalYear)+2 from LOBonusPol where polno='"+tLCBonusPolSchema.getPolNo()+"' ");
	                    tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear='"+tFiscalYear+"' " +
	            		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
	                	System.out.println("tratesql:"+tratesql);
	                	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                	if(tLMRiskBonusRateSet.size()<1){
	                		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
	                		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
	                    	System.out.println("tratesql:"+tratesql);
	                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                    	if(tLMRiskBonusRateSet.size()<1){
	                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
		                		"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
		                    	System.out.println("tratesql:"+tratesql);
		                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                    	if(tLMRiskBonusRateSet.size()<1){
		                    		// @@错误处理
									System.out.println("PEdorCTAppConfirmBL+prepareData++--");
									CError tError = new CError();
									tError.moduleName = "PEdorCTAppConfirmBL";
									tError.functionName = "prepareData";
									tError.errorMessage = "获取分红率失败!";
									mErrors.addOneError(tError);
									return false;
		                    	}
	                    	}
	                	}
	                	tCalculator = new Calculator();
	                    tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
	                    tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
	                    tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
	                    tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
	                    tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
	                    tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
	                    tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
	                    tCalculator.addBasicFactor("appyear",tCvaliYear);
	                    tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
	                    tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),lastbonusdate, "Y")+1));
	//                    tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
	                    tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
                    	if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, PubFun.calDate(lastbonusdate, 1, "Y", null)).equals(tFXdate)&&PubFun.getBeforeDate(tFXdate, mLPEdorItemSchema.getEdorValiDate()).equals(tFXdate)){
                    		tCalculator.addBasicFactor("startdate",tFXdate);
                        }else{
    	                	tCalculator.addBasicFactor("startdate",PubFun.calDate(lastbonusdate, 1, "Y", null));                        	
                        }
	                    tCalculator.addBasicFactor("enddate",mLPEdorItemSchema.getEdorValiDate());
	                    tCalculator.addBasicFactor("caltype","JY");
	                    tHL+= Double.parseDouble(tCalculator.calculate());
	                    tHL = CommonBL.carry(tHL);
	                    tBonusLJS=tBonusBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
	                    		tLCBonusPolSchema,
	                            null,
	                            "HLBF",
	                            -tHL,
	                            mGlobalInput);
	                    aTotalReturn=CommonBL.carry(aTotalReturn-tHL);
	                	tempLJSGetEndorseSet.add(tBonusLJS);
	                	if(tLCBonusPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETINTEREST)){
	                		LCInsureAccSchema tLCInsureAccSchema=new LCInsureAccSchema();
	                		LCInsureAccDB tLCInsureAccDB=new LCInsureAccDB();
	                		tLCInsureAccDB.setPolNo(tLCBonusPolSchema.getPolNo());
	                		LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
	                		double tHLLX=  0;
	                		tLCInsureAccSet = tLCInsureAccDB.query();
	                		if(tLCInsureAccSet!=null&&tLCInsureAccSet.size()>0){
	                		tLCInsureAccSchema.setSchema(tLCInsureAccSet.get(1));
	                		String tPolYear=String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),mLPEdorItemSchema.getEdorValiDate(), "Y")-1);
	                    	String trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
	                    				" and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+"-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+")))) ");
	                    	if(trate.equals("")){
	                    		trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
                    				" order by bonusyear desc fetch first 1 row only with ur  ");
	                    	}
	                    	String tbala=mExeSQL.getOneValue("select sum(money) from lcinsureacctrace where polno='"+tLCInsureAccSchema.getPolNo()+"' and paydate<='"+tLCInsureAccSchema.getBalaDate()+"' with ur");
	                    	tCalculator=new Calculator();
	                    	tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusInterestCode());
	                    	tCalculator.addBasicFactor("caltype1","YX");
	                    	String YXCalCode=tCalculator.calculate();
	                    	Calculator tCalculatorLX=new Calculator();
	                    	tCalculatorLX.setCalCode(YXCalCode);
	                    	tCalculatorLX.addBasicFactor("lastriskbonus", tbala);
	                    	tCalculatorLX.addBasicFactor("accountdate", PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear)+1, "Y", null));
	                    	tCalculatorLX.addBasicFactor("rate", trate);
	                    	double tHLLXYX=0;
	                        if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear)+1, "Y", null)).equals(tFXdate)&&PubFun.getLaterDate(tFXdate, tLCInsureAccSchema.getBalaDate()).equals(tFXdate)){
		                    	tCalculatorLX.addBasicFactor("lastaccountdate",  tFXdate);
		                		tHLLXYX=Double.parseDouble(tCalculatorLX.calculate());
	                        }else if(tFXdate.equals("")){
		                    	tCalculatorLX.addBasicFactor("lastaccountdate",  tLCInsureAccSchema.getBalaDate());
		                		tHLLXYX=Double.parseDouble(tCalculatorLX.calculate());
	                        }else if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, tLCInsureAccSchema.getBalaDate()).equals(tLCInsureAccSchema.getBalaDate())){
		                    	tCalculatorLX.addBasicFactor("lastaccountdate",  tLCInsureAccSchema.getBalaDate());
		                		tHLLXYX=Double.parseDouble(tCalculatorLX.calculate());
	                        }else if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear)+1, "Y", null)).equals(PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear)+1, "Y", null))){
		                		tHLLXYX=Double.parseDouble(tbala);
	                        }
	                		tPolYear=String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),mLPEdorItemSchema.getEdorValiDate(), "Y"));
	                		trate=new String();
	                		trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
	                    				" and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+"-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+")))) ");
	                    	if(trate.equals("")){
	                    		trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
                    				" order by bonusyear desc fetch first 1 row only with ur  ");
	                    	}
	                    	tCalculator=new Calculator();
	                    	tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusInterestCode());
	                    	tCalculator.addBasicFactor("caltype1","JY");
	                    	String JYCalCode=tCalculator.calculate();
	                    	tCalculatorLX=new Calculator();
	                    	tCalculatorLX.setCalCode(JYCalCode);
	                    	tCalculatorLX.addBasicFactor("lastriskbonus", String.valueOf(tHLbak+tHLLXYX));
	                    	tCalculatorLX.addBasicFactor("enddate", mLPEdorItemSchema.getEdorValiDate());
	                    	tCalculatorLX.addBasicFactor("rate", trate);
	                    	if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear), "Y", null)).equals(tFXdate)&&PubFun.getBeforeDate(tFXdate, mLPEdorItemSchema.getEdorValiDate()).equals(tFXdate)){
	                    		tCalculatorLX.addBasicFactor("lastaccountdate",tFXdate);
	                        }else{
	                        	tCalculatorLX.addBasicFactor("lastaccountdate",  PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear), "Y", null));
	                        }
	                    	 tHLLX= CommonBL.carry(Double.parseDouble(tCalculatorLX.calculate())-tHLbak);
	                		}
	                    	tBonusLXLJS=tBonusBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
		                    		tLCBonusPolSchema,
		                            null,
		                            "HLLX",
		                            -tHLLX,
		                            mGlobalInput);
		                    aTotalReturn=CommonBL.carry(aTotalReturn-tHLLX);
		                	tempLJSGetEndorseSet.add(tBonusLXLJS);
	                	}
            		}else{
	            		String tFiscalYear=mExeSQL.getOneValue("select max(FiscalYear)+1 from LOBonusPol where polno='"+tLCBonusPolSchema.getPolNo()+"' ");
	            		LMRiskBonusRateDB tLMRiskBonusRateDB =new LMRiskBonusRateDB();
	                	LMRiskBonusRateSet tLMRiskBonusRateSet=new LMRiskBonusRateSet();
	                	String tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear='"+tFiscalYear+"' " +
	            		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
	                	System.out.println("tratesql:"+tratesql);
	                	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                	if(tLMRiskBonusRateSet.size()<1){
	                		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
	                		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
	                    	System.out.println("tratesql:"+tratesql);
	                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                    	if(tLMRiskBonusRateSet.size()<1){
	                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
		                		"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
		                    	System.out.println("tratesql:"+tratesql);
		                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                    	if(tLMRiskBonusRateSet.size()<1){
		                    		// @@错误处理
									System.out.println("PEdorCTAppConfirmBL+prepareData++--");
									CError tError = new CError();
									tError.moduleName = "PEdorCTAppConfirmBL";
									tError.functionName = "prepareData";
									tError.errorMessage = "获取分红率失败!";
									mErrors.addOneError(tError);
									return false;
		                    	}
	                    	}
	                	}
	                	Calculator tCalculator = new Calculator();
	                    tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
	                    tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
	                    tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
	                    tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
	                    tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
	                    tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
	                    tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
	                    tCalculator.addBasicFactor("appyear",tCvaliYear);
	                    tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
	                    tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),lastbonusdate, "Y")));
	//                    tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
	                    tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
	                    if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, mLPEdorItemSchema.getEdorValiDate()).equals(tFXdate)&&PubFun.getLaterDate(tFXdate, lastbonusdate).equals(tFXdate)){
		                	tCalculator.addBasicFactor("startdate",tFXdate);
	                    }else{
		                	tCalculator.addBasicFactor("startdate",lastbonusdate);
	                    }
	                    tCalculator.addBasicFactor("enddate",mLPEdorItemSchema.getEdorValiDate());
	                    tCalculator.addBasicFactor("caltype","JY");
	                    double tHL= CommonBL.carry(tCalculator.calculate());
	                    tBonusLJS=tBonusBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
	                    		tLCBonusPolSchema,
	                            null,
	                            "HLBF",
	                            -tHL,
	                            mGlobalInput);
	                    aTotalReturn=CommonBL.carry(aTotalReturn-tHL);
	                	tempLJSGetEndorseSet.add(tBonusLJS);
	                	if(tLCBonusPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETINTEREST)){
	                		LCInsureAccSchema tLCInsureAccSchema=new LCInsureAccSchema();
	                		LCInsureAccDB tLCInsureAccDB=new LCInsureAccDB();
	                		tLCInsureAccDB.setPolNo(tLCBonusPolSchema.getPolNo());
	                		LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
	                		tLCInsureAccSet = tLCInsureAccDB.query();
	                		double tHLLX= 0;
	                		if(tLCInsureAccSet!=null&&tLCInsureAccSet.size()>0){
	                		tLCInsureAccSchema.setSchema(tLCInsureAccSet.get(1));
	                		String tPolYear=String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),mLPEdorItemSchema.getEdorValiDate(), "Y"));
	                    	String trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
	                    				" and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+"-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+")))) ");
	                    	if(trate.equals("")){
	                    		trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
                    				" order by bonusyear desc fetch first 1 row only with ur  ");
	                    	}
	                    	tCalculator=new Calculator();
	                    	tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusInterestCode());
	                    	tCalculator.addBasicFactor("caltype1","JY");
	                    	String JYCalCode=tCalculator.calculate();
	                    	Calculator tCalculatorLX=new Calculator();
	                    	tCalculatorLX.setCalCode(JYCalCode);
	                    	tCalculatorLX.addBasicFactor("lastriskbonus", String.valueOf(tLCInsureAccSchema.getInsuAccBala()));
	                    	tCalculatorLX.addBasicFactor("enddate", mLPEdorItemSchema.getEdorValiDate());
	                    	tCalculatorLX.addBasicFactor("rate", trate);
		                    if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, mLPEdorItemSchema.getEdorValiDate()).equals(tFXdate)&&PubFun.getLaterDate(tFXdate, tLCInsureAccSchema.getBalaDate()).equals(tFXdate)){
		                    	tCalculatorLX.addBasicFactor("lastaccountdate", tFXdate);
		                    }else{
		                    	tCalculatorLX.addBasicFactor("lastaccountdate",  tLCInsureAccSchema.getBalaDate());
		                    }
	                    	 tHLLX= CommonBL.carry(tCalculatorLX.calculate());
	                		}
	                		tBonusLXLJS=tBonusBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
		                    		tLCBonusPolSchema,
		                            null,
		                            "HLLX",
		                            -tHLLX,
		                            mGlobalInput);
		                    aTotalReturn=CommonBL.carry(aTotalReturn-tHLLX);
		                	tempLJSGetEndorseSet.add(tBonusLXLJS);
	                	}
            		}
            	}
        	}else{
        		// @@错误处理
				System.out.println("PEdorCTAppConfirmBL+prepareData++--");
				CError tError = new CError();
				tError.moduleName = "PEdorCTAppConfirmBL";
				tError.functionName = "prepareData";
				tError.errorMessage = "获取分红险种失败";
				mErrors.addOneError(tError);
				return false;
        	}
        }
        //add by lzy 20150805计算金生无忧满期金及利息
        String strJS="select 1 from lppol where edorno='"+mLPEdorItemSchema.getEdorNo()+"' " +
        				" and contno='"+mLPEdorItemSchema.getContNo()+"' and riskcode='340101'";
        String haveJS=mExeSQL.getOneValue(strJS);
        if(null!=haveJS && !"".equals(haveJS)){
        	System.out.println("*************开始处理金生无忧保单满期金*************");
        	//copy C to P 
        	String transPget="INSERT INTO LPGET ( SELECT '"+mLPEdorItemSchema.getEdorNo()+"','"+mLPEdorItemSchema.getEdorType()+"',LCGET.* "
        			+" FROM LCGET WHERE CONTNO='"+mLPEdorItemSchema.getContNo()+"' "
        			+" AND POLNO IN (SELECT POLNO FROM LPPOL WHERE EDORNO='"+mLPEdorItemSchema.getEdorNo()+"'))";
        	String deleteLPget="Delete From Lpget Where Edorno='"+mLPEdorItemSchema.getEdorNo()+"' and ContNo='"+mLPEdorItemSchema.getContNo()+"' ";
        	MMap tMmap=new MMap();
        	tMmap.put(deleteLPget, SysConst.DELETE);//先删除历史数据
        	tMmap.put(transPget, SysConst.INSERT);
        	try {
        		VData mVdata=new VData();
            	mVdata.add(tMmap);
            	PubSubmit ttPubSubmit=new PubSubmit();//立即提交
            	ttPubSubmit.submitData(mVdata, "");
			} catch (Exception e) {
				System.out.println("金生无忧保单满期金处理失败");
				CError tError = new CError();
				tError.moduleName = "PEdorCTAppConfirmBL";
				tError.functionName = "prepareData";
				tError.errorMessage = "满期金处理失败";
				mErrors.addOneError(tError);
				return false;
			}
			LJSGetEndorseSet ebLJSGetEndorseSet=calBenefitGet();
			tempLJSGetEndorseSet.add(ebLJSGetEndorseSet);
			for(int i=1;i<=ebLJSGetEndorseSet.size();i++){
				aTotalReturn+=ebLJSGetEndorseSet.get(i).getGetMoney();
			}
			
        }

        //暂时不处理帐户的平衡问题
        //得到当前LPEdorMain保单信息主表的状态，并更新状态为申请确认。
        mLPEdorItemSchema.setChgPrem(tChgPrem);
        //付款要用负数，打印团体变动清单时统计要用，by Minim at 2003-12-11
        mLPEdorItemSchema.setGetMoney(aTotalReturn);
        mLPEdorItemSchema.setEdorState("2");
        mLPEdorItemSchema.setUWFlag("0");

        mInputData.clear();
        map.put(mLPEdorItemSchema, "UPDATE");
        map.put(tempLJSGetEndorseSet, "DELETE&INSERT");
        mInputData.add(mLPEdorItemSchema);
        mInputData.add(tempLJSGetEndorseSet);
        
        //20110121 zhangm 健康宝A附加重疾单独解约后，结算当月风险保费后从账户扣除。
        if(!dealULIAppendFee())
        {
        	return false;
        }
        return true;
    }
    
    /**
     * 计算金生无忧保单可领取且未领取的满期金（老年护理金、健康维护金）
     * @return
     */
    private LJSGetEndorseSet calBenefitGet() {
    	LJSGetEndorseSet getLJSGetEndorseSet=new LJSGetEndorseSet();
		String contNo = mLPEdorItemSchema.getContNo();
		String edorNo = mLPEdorItemSchema.getEdorNo();
		mEdorValiDate = mLPEdorItemSchema.getEdorValiDate();
		// 通过LPGET表计算保单未领取的满期金及利息
		String sql = "select distinct insuredno from lpget a,lmdutygetalive b "
				+ " where a.getdutycode = b.getdutycode and contno ='"+ contNo+"'"
				+ " and a.gettodate<='"+ mEdorValiDate+ "'"
				+ " and a.edorno='"+ edorNo+ "'"
				+ " and ((a.getdutykind ='0' and (a.gettodate<=a.getenddate or a.getenddate is null)"
				+ " and not exists (select 1 from ljsgetdraw where contno=a.contno and getdutycode=a.getdutycode and insuredno=a.insuredno)"
				+ ") or( a.getdutykind !='0' and"
				+ " ((a.gettodate<a.getenddate) or ((select max(curgettodate) from ljsgetdraw where contno=a.contno and getdutycode=a.getdutycode and insuredno=a.insuredno)<=a.getenddate))))"
				+ " with ur";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		MMap mMMap = null;
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			String insuredNo = tSSRS.GetText(i, 1);
			LPGetSet tLPGetSet = getLCGetNeedGet(edorNo, contNo, insuredNo,true);
			
			String strPol="select * from lppol where edorno='"+edorNo+"' and contno='"+contNo+"' and insuredno='"+insuredNo+"' and riskcode='340101'";
			LPPolSet getLPPolSet=new LPPolDB().executeQuery(strPol);
			LPPolSchema getLPPolSchema=getLPPolSet.get(1);
			double sumGetMoney = 0; // 总领取金额
			double sumLX = 0; // 总领取金额
			while (tLPGetSet.size() > 0) {
				mMMap=new MMap();
				for (int j = 1; j <= tLPGetSet.size(); j++) {
					// 获取合同下的每条给付的明细信息
					LPGetSchema tLPGetSchema = tLPGetSet.get(j).getSchema();

					String birthdaySQL = "select birthday from lcinsured where contno ='"
							+ contNo+ "'"+" and insuredno='"+ insuredNo+ "' ";
					String mBirthday = mExeSQL.getOneValue(birthdaySQL);
					String tAppntAge = String.valueOf(PubFun.calInterval(
							mBirthday, tLPGetSchema.getGettoDate(), "Y"));
					Calculator tCalculator = new Calculator();
					tCalculator.addBasicFactor("AppntAge", tAppntAge);
					tCalculator.addBasicFactor("Age", tAppntAge);
					LMDutyGetAliveDB tLMDutyGetaLiveDB = new LMDutyGetAliveDB();

					tLMDutyGetaLiveDB.setGetDutyCode(tLPGetSchema
							.getGetDutyCode());
					LMDutyGetAliveSet tLMDutyGetAliveSet = tLMDutyGetaLiveDB
							.query();
					LMDutyGetAliveSchema tLMDutyGetAliveSchema = tLMDutyGetAliveSet
							.get(1);
					String pCalCode = "";
					if (tLMDutyGetAliveSchema.getCalCode() != null
							&& !"".equals(tLMDutyGetAliveSchema.getCalCode())) {
						pCalCode = tLMDutyGetAliveSchema.getCalCode();
					}
					if (tLMDutyGetAliveSchema.getCnterCalCode() != null
							&& !"".equals(tLMDutyGetAliveSchema
									.getCnterCalCode())) {
						pCalCode = tLMDutyGetAliveSchema.getCnterCalCode();
					}
					if (tLMDutyGetAliveSchema.getOthCalCode() != null
							&& !""
									.equals(tLMDutyGetAliveSchema
											.getOthCalCode())) {
						pCalCode = tLMDutyGetAliveSchema.getOthCalCode();
					}
					tCalculator.setCalCode(pCalCode);

					// 责任保额
					String strAmnt = "select sum(amnt) from lcduty where polno='"
							+ tLPGetSchema.getPolNo()
							+ "' and dutycode='"
							+ tLPGetSchema.getDutyCode() + "'";
					String amnt = mExeSQL.getOneValue(strAmnt);

					if (tLMDutyGetAliveSchema.getGetDutyKind().equals("12")) {
						String kindsql = " select d.* From lmdutygetclm a, lmriskduty b ,lmdutygetrela c ,llclaimdetail d "
								+ " where a.getdutycode = c.getdutycode and b.dutycode = c.dutycode "
								+ " and d.getdutycode = c.getdutycode "
								+ " and d.polno ='"
								+ tLPGetSchema.getPolNo()
								+ "' " + " and afterget ='003' ";
						SSRS kindSSRS = mExeSQL.execSQL(kindsql);
						if (kindSSRS.getMaxRow() > 0) {
							System.out.println("责任已经理赔，不能做满期给付。");
							continue;
						}
					}
					// 判断主险是否出险,满期给付责任在主险。
					if (getPolAttribute(tLPGetSchema.getPolNo())
							&& tLMDutyGetAliveSchema.getDiscntFlag()
									.equals("0")) {
						// 得到该险种的附加险种
						String annexPolNoSQL = "select code From ldcode1 where code1 =(select riskcode from lcpol where polno ='"
								+ tLPGetSchema.getPolNo()
								+ "')  and codetype='checkappendrisk' ";
						String annexRisk = mExeSQL.getOneValue(annexPolNoSQL);
						String annexPolNo = "select polno from lcpol where contno='"
								+ tLPGetSchema.getContNo()
								+ "' and riskcode ='" + annexRisk + "'";
						if (annexRisk.equals("") && annexRisk == "") {
							System.out.println("没有得到附加险信息。");
							continue;
						}
						// 1."如果附加的有LP就不做给付";
						String sqlcliam = "select polNo From ljagetclaim where polno ='"
								+ new ExeSQL().getOneValue(annexPolNo) + "'";
						String cliam = mExeSQL.getOneValue(sqlcliam);
						if (!cliam.equals("") && cliam != "" && cliam != "null") {
							System.out.println("附加险已经出险，不能做满期给付。");
							continue;
						}
						// 2."主险出现了并且是死亡责任的不做给付"
						String mainSQL = "select polNo From ljagetclaim where polno ='"
								+ tLPGetSchema.getPolNo()
								+ "'"
								+ " and getdutykind in('501','502','503','504') ";
						String main = mExeSQL.getOneValue(mainSQL);
						if (!main.equals("") && main != "null") {
							System.out.println("主险责任出现理赔，并且是死亡责任，不能满期给付。");
							continue;
						}
						// 3.如果满期金方式的责任出险，不做给付。
						if (tLMDutyGetAliveSchema.getGetDutyKind().equals("0")) {
							String SQL = "select polNo From ljagetclaim where polno ='"
									+ tLPGetSchema.getPolNo() + "'";
							String polSLQ = mExeSQL.getOneValue(SQL);
							if (!polSLQ.equals("") && polSLQ != "null") {
								System.out.println("满期责任已经出险，不做给付。");
								continue;
							}
						}
					}
					tCalculator.addBasicFactor("Amnt", amnt);
					tCalculator.addBasicFactor("Get", CommonBL
							.bigDoubleToCommonString(tLPGetSchema.getActuGet(),
									"0.00"));
					tCalculator
							.addBasicFactor("PolNo", tLPGetSchema.getPolNo());
					// tCalculator.addBasicFactor("Prem",mPrem);
					String tStr = tCalculator.calculate();
					double tValue = 0;
					if (tStr == null || tStr.trim().equals("")) {
						tValue = 0;
					} else {
						tValue = Double.parseDouble(tStr);
					}
					sumGetMoney += tValue;
					// 计算迟领利息
					if ("294202".equals(tLPGetSchema.getGetDutyCode())
							|| "294203".equals(tLPGetSchema.getGetDutyCode())) {
						double getMoney = tValue;
						if (getMoney > 0) {
							FDate tfdate = new FDate();
							// 过了应领取日才会有给付利息
							if (tfdate.getDate(tLPGetSchema.getGettoDate())
									.compareTo(tfdate.getDate(mEdorValiDate)) < 0) {
								double interest = 0;
								// 迟发天数
								int days = PubFun.calInterval(tLPGetSchema
										.getGettoDate(), mEdorValiDate, "D");
								// 以2.5%的年复利计息
								double rate = 2.5 / 100;

								double sumMoney = Arith.mul(getMoney, Math.pow(
										Arith.add(1, rate), Arith
												.div(days, 365)));
								interest = Arith.sub(sumMoney, getMoney);
								sumLX += interest;
								System.out.println("******保单" + contNo
										+ "，的满期金：" + getMoney + ",迟发天数：" + days
										+ ",迟发利息：" + interest);

							}
						}
					}
					// 更新给付责任数据
					if (!tLPGetSchema.getGetDutyKind().equals("0")) {
						String newGetToDate = " select gettodate +"
								+ tLPGetSchema.getGetDutyKind()
								+ " month From lpget where edorno='"+edorNo+"' and polno ='"
								+ tLPGetSchema.getPolNo() + "'"
								+ " and getDutyCode = '"
								+ tLPGetSchema.getGetDutyCode() + "'";
						tLPGetSchema.setGettoDate(mExeSQL.getOneValue(
								newGetToDate).trim()); // 得到下次给付日期
						tLPGetSchema.setModifyDate(PubFun.getCurrentDate());
						tLPGetSchema.setModifyTime(PubFun.getCurrentTime());
						mMMap.put(tLPGetSchema, SysConst.UPDATE);
					}
				}
				VData mVData=new VData();
				mVData.add(mMMap);
				PubSubmit mPubSubmit=new PubSubmit();
				mPubSubmit.submitData(mVData, "");
				if(mPubSubmit.mErrors.needDealError()){
					System.out.println("更新满期给付责任数据是发生错误");
					mErrors.addOneError(mPubSubmit.mErrors.getFirstError());
					return null;
				}
				//获取最新的给付责任数据
				tLPGetSet = getLCGetNeedGet(edorNo, contNo, insuredNo,false);
			}
			sumGetMoney=PubFun.setPrecision(sumGetMoney, "0.00");
			sumLX=PubFun.setPrecision(sumLX, "0.00");
			BqCalBL getCalBL=new BqCalBL();
			LJSGetEndorseSchema getLS=getCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),getLPPolSchema,null,"EB",-sumGetMoney,mGlobalInput);
			LJSGetEndorseSchema getLxLS=getCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),getLPPolSchema,null,"EBLX",-sumLX,mGlobalInput);
			getLJSGetEndorseSet.add(getLS);
			getLJSGetEndorseSet.add(getLxLS);
		}
		return getLJSGetEndorseSet;
	}
    /**
     * 获得金生无忧险种的给付责任
     * @param edorNo
     * @param contNo
     * @param insuredNo
     * @param firstFlag 第一次使用查询为true
     * @return 第一次查询给付责任时要查询出老年护理金和健康维护金 , 第二次之后查询的只查 健康维护金
     */
    private LPGetSet getLCGetNeedGet(String edorNo,String contNo, String insuredNo,boolean firstFalg) {
        String tSBql = " select distinct b.* from lppol a,lpget b , lmdutygetalive c  where 1=1 "
                        + " and a.polno = b.polno and b.getdutycode = c.getdutycode   "
                        + " and a.edorno = b.edorno "
                        + " and a.edortype = b.edortype "
                        + " and a.edorno = '"+edorNo+"' "//modify by lzy不小心埋了个雷，现在补一下
                        + " and a.contno='" + contNo + "' "
                        + " and a.appflag='1' "
                        + " and a.riskcode ='340101' "
                        + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate))  "
                        + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno ) "
                        + " and ( "+ (firstFalg==false ? "" : "(b.getdutykind ='0' and  (b.gettodate<=b.getenddate or b.getenddate is null)) or ")
                        + " ( b.getdutykind !='0' and ((b.gettodate<b.getenddate) or ((select max(Curgettodate) from ljsgetdraw where contno=b.contno and getdutycode=b.getdutycode and insuredno=b.insuredno)<=b.getenddate))))"
                        + " and b.gettodate <= '"+mEdorValiDate+"'"
                        + " and b.gettodate <= a.paytodate "//用paytodate和edorvalidate均限制上，也就是按这两个日期中较早的那个限制。
                        + " and b.insuredno='"+insuredNo+"'"
                        + " with ur ";

        LPGetSet tLPGetSet = new LPGetSet();
        LPGetDB tLPGetDB = new LPGetDB();
        tLPGetSet = tLPGetDB.executeQuery(tSBql);
        System.out.println(tSBql.toString());

        if (tLPGetSet.size() == 0) {
            System.out.println("保单号为：" + contNo + "，没有查询到可给付的满期金");
            return new LPGetSet();
        }
        return tLPGetSet;
    }
    
    /**
     * 判断给付责任的险种是主险还是附加险
     * 主险返回: true;
     * 附加返回：false;
     * @param polNo String
     * @return boolean
     */

    private boolean getPolAttribute(String polNo)
    {
        String polSQL = " select subriskflag from lcpol a,lmriskapp b where  a.riskcode = b.riskcode and a.polno ='"+polNo+"'";
        SSRS tSSRS = mExeSQL.execSQL(polSQL);

        if (tSSRS.getMaxRow()==0)
        {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "getPolAttribute";
            tError.errorMessage = "获得主险信息描述失败。";
            mErrors.addOneError(tError);
            return false;
        }
        // M-- 主险
        // S-- 附加险
        if (tSSRS.GetText(1,1).equals("M"))
        {
            return true ;
        }
        else{
            return false ;
        }
    }
    
    private boolean EdorHasBonusRisk(String tContNo){
    	String tcheck=mExeSQL.getOneValue("select riskcode from lcpol where contno='"+mLPEdorItemSchema.getContNo()+"' and riskcode in (select riskcode from lmriskapp where risktype4='2') with ur");
    	if(tcheck==null||tcheck.equals("")||tcheck.equals("null")){
    		System.out.println("本次不涉及分红险种的退保!");
    		return false;
    	}else{
    		LCPolSet tLCBonusPolSet=new LCPolDB().executeQuery("select * from lcpol where contno='"+mLPEdorItemSchema.getContNo()+"' and riskcode in (select riskcode from lmriskapp where risktype4='2') with ur");
    		if(tLCBonusPolSet.size()==1){
    		LCPolSchema tLCBonusPolSchema=tLCBonusPolSet.get(1);
    		String tchecksql1="select 1 from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
                    "  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
    		String tcheckresult1=mExeSQL.getOneValue(tchecksql1);

    		String tchecksql2="select 1 from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
            "  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
	String tcheckresult2=mExeSQL.getOneValue(tchecksql2);
    		
	    		if(tcheckresult1==null||tcheckresult1.equals("")||tcheckresult1.equals("null")){
	    			
	    			if(tcheckresult2==null||tcheckresult2.equals("")||tcheckresult2.equals("null")){
	        		System.out.println("本次退保分红险种未录入分红率,跳过分红处理!");
	        		return false;
	    			}
	        	}
    		}else{
    			System.out.println("本次不涉及分红险种表的退保!");
        		return false;
    		}
    	}
    	return true;
    }
    
    

    private void setInsureAccFee(LCInsureAccSchema aLCInsureAccSchema,LPEdorItemSchema aLPEdorItemSchema,double aFee, double aFeeRate,String aFeeCode){
        String serialNo;
        serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        LPInsureAccFeeTraceSchema tLPInsureAccFeeTraceSchema = new LPInsureAccFeeTraceSchema();
        tLPInsureAccFeeTraceSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPInsureAccFeeTraceSchema.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPInsureAccFeeTraceSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccFeeTraceSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccFeeTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccFeeTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccFeeTraceSchema.setSerialNo(serialNo);
        tLPInsureAccFeeTraceSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccFeeTraceSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
        tLPInsureAccFeeTraceSchema.setPayPlanCode(BQ.FILLDATA);
        tLPInsureAccFeeTraceSchema.setOtherNo(aLPEdorItemSchema.getEdorNo());
        tLPInsureAccFeeTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccFeeTraceSchema.setAccAscription("0");
        tLPInsureAccFeeTraceSchema.setMoneyType("MF");
        tLPInsureAccFeeTraceSchema.setFeeRate(aFeeRate);
        tLPInsureAccFeeTraceSchema.setFee(Math.abs(aFee));  //退费
        tLPInsureAccFeeTraceSchema.setFeeUnit("0");
        tLPInsureAccFeeTraceSchema.setPayDate(aLPEdorItemSchema.getEdorValiDate());
        tLPInsureAccFeeTraceSchema.setState("0");
        tLPInsureAccFeeTraceSchema.setFeeCode(aFeeCode);
        tLPInsureAccFeeTraceSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccFeeTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLPInsureAccFeeTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLPInsureAccFeeTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLPInsureAccFeeTraceSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLPInsureAccFeeTraceSchema, "DELETE&INSERT");
    }

    private void setLPInsureAccFee(LCInsureAccSchema aLCInsureAccSchema,LPEdorItemSchema aLPEdorItemSchema, double manageMoney){
        String tACCsql = "delete from LPInsureAccFee " +
             "where EdorNo = '" + aLPEdorItemSchema.getEdorNo() + "' " +
             "and EdorType = '" + aLPEdorItemSchema.getEdorType() + "' " +
             "and polno = '" + aLCInsureAccSchema.getPolNo() + "'";
        System.out.println(tACCsql);
        map.put(tACCsql, "DELETE");

        String[] keys = new String[2];
        DetailDataQuery mQuery = new DetailDataQuery(aLPEdorItemSchema.getEdorNo(), aLPEdorItemSchema.getEdorType());
        keys[0] = aLCInsureAccSchema.getPolNo();
        keys[1] = aLCInsureAccSchema.getInsuAccNo();
        LPInsureAccFeeSchema tLPInsureAccFeeSchema = new LPInsureAccFeeSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPInsureAccFeeSchema, aLCInsureAccSchema);
        tLPInsureAccFeeSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPInsureAccFeeSchema.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPInsureAccFeeSchema.setFee(tLPInsureAccFeeSchema.getFee() + manageMoney);
        tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccFeeSchema.setModifyDate(PubFun.getCurrentDate());
        tLPInsureAccFeeSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLPInsureAccFeeSchema, "DELETE&INSERT");
    }
    
//  20110121 zhangm 健康宝A附加重疾单独解约后，结算当月风险保费后从账户扣除。
    private boolean dealULIAppendFee()
    {
    	String tContNo = mLPEdorItemSchema.getContNo();
    	if (tContNo == null || tContNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorWTDetailBL";
            tError.functionName = "dealULIAppendFee";
            tError.errorMessage = "保单号查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	
//    	保单不包含万能险，不需要处理
    	if(!CommonBL.hasULIRisk(tContNo))
    	{
    		return true;
    	}
    	
//    	得到需要退保的Pol
        LPPolSet tLPPolSet = getLPPolSet(mLPEdorItemSchema.getSchema());
    	
    	// 退保的险种,含有万能主险，整单退保，不需要处理
    	for(int i=1; i<=tLPPolSet.size(); i++)
    	{
    		if(CommonBL.isULIRisk(tLPPolSet.get(i).getRiskCode()))
    		{
    			return true ;
    		}
    	}
    	
    	//不含主险，则只能是附加重疾单独退保
    	for(int j=1; j<=tLPPolSet.size(); j++)
    	{
    		LPPolSchema tLPPolSchema = tLPPolSet.get(j).getSchema();
            String tRiskCode = tLPPolSchema.getRiskCode(); //附加险RiskCode
            String tMainPolNo = tLPPolSchema.getMainPolNo();//主险PolNo
           
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tRiskCode);
            if(tLMRiskAppDB.getInfo())
            {
            	String tRiskType3 = tLMRiskAppDB.getRiskType3();
            	if(tRiskType3.equals("3")) //重疾
				{
            		LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
                	tLCInsureAccClassDB.setPolNo(tMainPolNo);
                	
                	LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
                	if(tLCInsureAccClassSet.size()==0)
                	{
                		CError.buildErr(this, "查询万能账户信息失败！");
                		System.out.println("查询万能账户信息失败！");
                        return false;
                	}
                	
                	LCInsureAccClassSchema tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1).getSchema();
                	
                	//当前账户余额
                	double nowInsuAccBala = tLCInsureAccClassSchema.getInsuAccBala();
                	
//            		计算上次结算日到保全生效日的风险保费
            		double tRiskPrem = getRiskPrem(tLPPolSchema,tLCInsureAccClassSchema);
            		
            		if(Math.abs(tRiskPrem)<=0.001)
            		{
            			System.out.println("风险保费为0");
            			return true;
            		}
            		
            		//解约后账户余额
            		double newInsuAccBala = nowInsuAccBala - tRiskPrem;
            		
            		String tLimit = PubFun.getNoLimit(mLPEdorItemSchema.getManageCom());
		            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
		            
					LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
					ref.transFields(tLPInsureAccTraceSchema, tLCInsureAccClassSchema);
					tLPInsureAccTraceSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccTraceSchema.setEdorType(mLPEdorItemSchema.getEdorType());
					tLPInsureAccTraceSchema.setSerialNo(serNo);
					tLPInsureAccTraceSchema.setOtherNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccTraceSchema.setOtherType(BQ.NOTICETYPE_P);
					tLPInsureAccTraceSchema.setMoneyType("RP");
					tLPInsureAccTraceSchema.setMoney(-tRiskPrem);
					tLPInsureAccTraceSchema.setPayDate(mLPEdorItemSchema.getEdorValiDate());
					tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
					tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
					tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
					tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
					
					LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
					ref.transFields(tLPInsureAccClassSchema, tLCInsureAccClassSchema);
					tLPInsureAccClassSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccClassSchema.setEdorType(mLPEdorItemSchema.getEdorType());
					tLPInsureAccClassSchema.setInsuAccBala(newInsuAccBala);
					tLPInsureAccClassSchema.setMakeDate(mCurrentDate);
					tLPInsureAccClassSchema.setMakeTime(mCurrentTime);
					tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
					tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
            		
					LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
					tLCInsureAccDB.setContNo(tContNo);
					tLCInsureAccDB.setPolNo(tMainPolNo);
					LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
					if(tLCInsureAccSet == null || tLCInsureAccSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorCTAppConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败Acc!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccSet.get(1).getSchema();
					LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
					ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
					tLPInsureAccSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccSchema.setEdorType(mLPEdorItemSchema.getEdorType());
					tLPInsureAccSchema.setInsuAccBala(newInsuAccBala);
					tLPInsureAccSchema.setMakeDate(mCurrentDate);
					tLPInsureAccSchema.setMakeTime(mCurrentTime);
					tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccSchema.setModifyDate(mCurrentDate);
					tLPInsureAccSchema.setModifyTime(mCurrentTime);
					
					LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
					tLCInsureAccFeeDB.setContNo(tContNo);
					tLCInsureAccFeeDB.setPolNo(tMainPolNo);
					LCInsureAccFeeSet tLCInsureAccFeeSet = tLCInsureAccFeeDB.query();
					if(tLCInsureAccFeeSet == null || tLCInsureAccFeeSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorCTAppConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败Fee!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LCInsureAccFeeSchema tLCInsureAccFeeSchema = tLCInsureAccFeeSet.get(1).getSchema();
					LPInsureAccFeeSchema tLPInsureAccFeeSchema = new LPInsureAccFeeSchema();
					ref.transFields(tLPInsureAccFeeSchema, tLCInsureAccFeeSchema);
					tLPInsureAccFeeSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccFeeSchema.setEdorType(mLPEdorItemSchema.getEdorType());
					double oldFee = tLCInsureAccFeeSchema.getFee();
					double newFee = oldFee+tRiskPrem;
					tLPInsureAccFeeSchema.setFee(newFee);
					tLPInsureAccFeeSchema.setMakeDate(mCurrentDate);
					tLPInsureAccFeeSchema.setMakeTime(mCurrentTime);
					tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccFeeSchema.setModifyDate(mCurrentDate);
					tLPInsureAccFeeSchema.setModifyTime(mCurrentTime);
					
					LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
					tLCInsureAccClassFeeDB.setContNo(tContNo);
					tLCInsureAccClassFeeDB.setPolNo(tMainPolNo);
					LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
					if(tLCInsureAccClassFeeSet == null || tLCInsureAccClassFeeSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorCTAppConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败ClassFee!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = tLCInsureAccClassFeeSet.get(1).getSchema();
					LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
					ref.transFields(tLPInsureAccClassFeeSchema, tLCInsureAccClassFeeSchema);
					tLPInsureAccClassFeeSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccClassFeeSchema.setEdorType(mLPEdorItemSchema.getEdorType());
					tLPInsureAccClassFeeSchema.setFee(newFee);
					tLPInsureAccClassFeeSchema.setMakeDate(mCurrentDate);
					tLPInsureAccClassFeeSchema.setMakeTime(mCurrentTime);
					tLPInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
					tLPInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
					
					
					String tLimit1 = PubFun.getNoLimit(mLPEdorItemSchema.getManageCom());
		            String serNo1 = PubFun1.CreateMaxNo("SERIALNO", tLimit1);
		            
					LPInsureAccFeeTraceSchema tLPInsureAccFeeTraceSchema = new LPInsureAccFeeTraceSchema();
					ref.transFields(tLPInsureAccFeeTraceSchema, tLPInsureAccClassFeeSchema);
					tLPInsureAccFeeTraceSchema.setSerialNo(serNo1);
					tLPInsureAccFeeTraceSchema.setOtherNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccFeeTraceSchema.setOtherType(BQ.NOTICETYPE_P);
					tLPInsureAccFeeTraceSchema.setMoneyType("RP");
					tLPInsureAccFeeTraceSchema.setFee(tRiskPrem);
					tLPInsureAccFeeTraceSchema.setPayDate(mLPEdorItemSchema.getEdorValiDate());
					tLPInsureAccFeeTraceSchema.setState("0");
					tLPInsureAccFeeTraceSchema.setFeeCode("000012");
					tLPInsureAccFeeTraceSchema.setMakeDate(mCurrentDate);
					tLPInsureAccFeeTraceSchema.setMakeTime(mCurrentTime);
					tLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
					tLPInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
					
					map.put(tLPInsureAccSchema, SysConst.DELETE_AND_INSERT);
					map.put(tLPInsureAccClassSchema, SysConst.DELETE_AND_INSERT);
					map.put(tLPInsureAccFeeSchema, SysConst.DELETE_AND_INSERT);
					map.put(tLPInsureAccClassFeeSchema, SysConst.DELETE_AND_INSERT);
					
					String deltrace = "delete from LPInsureAccTrace where EdorNo = '" + mLPEdorItemSchema.getEdorNo() 
					+ "' and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' ";
					String delfeetrace = "delete from LPInsureAccFeeTrace where EdorNo = '" + mLPEdorItemSchema.getEdorNo()
					+ "' and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' ";
					
					map.put(deltrace, SysConst.DELETE);
					map.put(tLPInsureAccTraceSchema, SysConst.INSERT);
					map.put(delfeetrace, SysConst.DELETE);
					map.put(tLPInsureAccFeeTraceSchema, SysConst.INSERT);
                		
                	
                	
                	
				}
				else if(tRiskType3.equals("4")) //意外
				{
					continue;
				}
            }
    	}
    	
    	return true;
    }

    //计算风险保费
    private double getRiskPrem(LPPolSchema aLPPolSchema,LCInsureAccClassSchema aLCInsureAccClassSchema)
    {
    	double riskPrem = 0.0;
    	
    	LCPolSchema mainLCPolSchema = getMainPol(aLPPolSchema);
//    	 上次结算日
		String tBalaDate = aLCInsureAccClassSchema.getBalaDate();
		//保全生效日
		String tEdorValidate = mLPEdorItemSchema.getEdorValiDate();
//		风险加费
		String sql = "select Rate from LCPrem where PolNo = '" + aLPPolSchema.getPolNo() 
			+ "' and PayPlanCode like '00000%' order by PayPlanCode desc " ; 
		String tRiskRate = new ExeSQL().getOneValue(sql);
		if(tRiskRate==null || tRiskRate.equals(""))
		{
			tRiskRate = "0";
		}
//		被保人年龄
		AccountManage tAccountManage = new AccountManage(mainLCPolSchema);
        String tInsuredAppAge = tAccountManage.getInsuredAppAge(tBalaDate,aLCInsureAccClassSchema);
        if (tInsuredAppAge == null)
        {
            mErrors.copyAllErrors(tAccountManage.mErrors);
            tInsuredAppAge = "0";
        }
		
		LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(aLCInsureAccClassSchema.getInsuAccNo());
        tLMRiskFeeDB.setPayPlanCode(aLCInsureAccClassSchema.getPayPlanCode());
        tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
        tLMRiskFeeDB.setFeeNoti(aLPPolSchema.getRiskCode());//附加重疾
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        
        for(int j=1 ;j<=tLMRiskFeeSet.size(); j++)
        {
        	Calculator tCalculator = new Calculator();
            tCalculator.setCalCode(tLMRiskFeeSet.get(j).getFeeCalCode());
            tCalculator.addBasicFactor("Sex",aLPPolSchema.getInsuredSex());
            tCalculator.addBasicFactor("SubAmnt", String.valueOf(aLPPolSchema.getAmnt()));
            tCalculator.addBasicFactor("RiskRate", tRiskRate);
            tCalculator.addBasicFactor("StartDate", tBalaDate);
            tCalculator.addBasicFactor("EndDate", tEdorValidate);
            tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);
            double fee = Math.abs(PubFun.setPrecision(Double.parseDouble(tCalculator.calculate()), "0.00"));
            if (tCalculator.mErrors.needDealError())
            {
                System.out.println(tCalculator.mErrors.getErrContent());
                mErrors.addOneError("计算管理费出错");
                return 0.0;
            }
            riskPrem += fee; 
  		}
    		
        return riskPrem;
    }
    
    private String getPayYearsSQL(LCPolSchema tLCBonusPolSchema){
    	String payYearsSQL = "";
    	if("730101".equals(tLCBonusPolSchema.getRiskCode()) || "730301".equals(tLCBonusPolSchema.getRiskCode())){
    		payYearsSQL = "case when "+tLCBonusPolSchema.getInsuYear()+" in (5,6) then 1 else "+tLCBonusPolSchema.getPayEndYear()+" end ";
    	}else if ("730201".equals(tLCBonusPolSchema.getRiskCode()) || "730401".equals(tLCBonusPolSchema.getRiskCode()) || "730503".equals(tLCBonusPolSchema.getRiskCode())){
    		payYearsSQL = "case when "+tLCBonusPolSchema.getPayIntv()+" =0 then 1 else "+tLCBonusPolSchema.getPayEndYear()+" end ";
    	}
    	return payYearsSQL;
    }
    
    //得到该Pol对应主险的Pol
    private LCPolSchema getMainPol(LPPolSchema aLPPolSchema)
    {
    	LCPolDB tLCPolDB = new LCPolDB();
    	tLCPolDB.setPolNo(aLPPolSchema.getMainPolNo());
    	LCPolSet tLCPolSet = tLCPolDB.query();
    	if(tLCPolSet.size()==0)
    	{
    		return null;
    	}
    	else
    	{
    		return tLCPolSet.get(1).getSchema();
    	}
    	
    }
    
    public boolean creatLPInsureAcc(String polNo,LPEdorItemSchema pLPEdorItemSchema){
    	MMap map = new MMap();
    	Reflections ref=new Reflections();
    	LCInsureAccDB accDB = new LCInsureAccDB();
    	accDB.setPolNo(polNo);
    	LCInsureAccSet accSet = accDB.query();
    	for(int i = 1;i<=accSet.size();i++){
    		LPInsureAccSchema pAccSchema = new LPInsureAccSchema();
    		ref.transFields(pAccSchema, accSet.get(i));
    		pAccSchema.setEdorNo(pLPEdorItemSchema.getEdorNo());
    		pAccSchema.setEdorType(pLPEdorItemSchema.getEdorType());
    		PubFun.fillDefaultField(pAccSchema);
    		map.put(pAccSchema, "INSERT");
    	}
    	
    	LCInsureAccClassDB accClassDB = new LCInsureAccClassDB();
    	accClassDB.setPolNo(polNo);
    	LCInsureAccClassSet accClassSet = accClassDB.query();
    	for(int i = 1;i<=accClassSet.size();i++){
    		LPInsureAccClassSchema accClassSchema = new LPInsureAccClassSchema();
    		ref.transFields(accClassSchema, accClassSet.get(i));
    		accClassSchema.setEdorNo(pLPEdorItemSchema.getEdorNo());
    		accClassSchema.setEdorType(pLPEdorItemSchema.getEdorType());
    		PubFun.fillDefaultField(accClassSchema);
    		map.put(accClassSchema, "INSERT");
    	}
    	
    	LCInsureAccFeeDB accFeeDB = new LCInsureAccFeeDB();
    	accFeeDB.setPolNo(polNo);
    	LCInsureAccFeeSet accFeeSet = accFeeDB.query();
    	for(int i = 1;i<=accFeeSet.size();i++){
    		LPInsureAccFeeSchema pAccFeeSchema = new LPInsureAccFeeSchema();
    		ref.transFields(pAccFeeSchema, accFeeSet.get(i));
    		pAccFeeSchema.setEdorNo(pLPEdorItemSchema.getEdorNo());
    		pAccFeeSchema.setEdorType(pLPEdorItemSchema.getEdorType());
    		PubFun.fillDefaultField(pAccFeeSchema);
    		map.put(pAccFeeSchema, "INSERT");
    	}
    	
    	LCInsureAccClassFeeDB accClassFeeDB = new LCInsureAccClassFeeDB();
    	accClassFeeDB.setPolNo(polNo);
    	LCInsureAccClassFeeSet accClassFeeSet = accClassFeeDB.query();
    	for(int i = 1;i<=accClassFeeSet.size();i++){
    		LPInsureAccClassFeeSchema pAccClassFeeSchema = new LPInsureAccClassFeeSchema();
    		ref.transFields(pAccClassFeeSchema, accClassFeeSet.get(i));
    		pAccClassFeeSchema.setEdorNo(pLPEdorItemSchema.getEdorNo());
    		pAccClassFeeSchema.setEdorType(pLPEdorItemSchema.getEdorType());
    		PubFun.fillDefaultField(pAccClassFeeSchema);
    		map.put(pAccClassFeeSchema, "INSERT");
    	}
		VData data = new VData();
        data.add(map);
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(data, ""))
        {
        	mErrors.addOneError("creatLPInsureAcc方法:生成p表账户失败！");
            return false;
        }
        return true;
    }
    
    public static void main(String[] args)
    {
        LPEdorItemDB mLPEdorItemDB = new LPEdorItemDB();
        mLPEdorItemDB.setEdorNo("20140307000922");
        mLPEdorItemDB.setEdorType("CT");
        mLPEdorItemDB.setContNo("014841155000001");

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.Operator = "edor";
        mGlobalInput.ManageCom = "86";

        VData tVata = new VData();
        tVata.add(mLPEdorItemDB.query().get(1));
        tVata.add(mGlobalInput);

        PEdorCTAppConfirmBL tPEdorCTAppConfirmBL = new PEdorCTAppConfirmBL();
        if(!tPEdorCTAppConfirmBL.submitData(tVata, ""))
        {
            System.out.println(tPEdorCTAppConfirmBL.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
//System.out.println(PubFun.calDate("2012-7-19", 1, "Y", null));
//System.out.println(PubFun.getLastDate("2008-09-04", 5, "Y"));
    }

}
