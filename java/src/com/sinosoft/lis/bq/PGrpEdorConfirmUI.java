package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import java.lang.reflect.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:保全申请确认功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PGrpEdorConfirmUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public PGrpEdorConfirmUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    PGrpEdorConfirmBL tPGrpEdorConfirmBL = new PGrpEdorConfirmBL();
    System.out.println("---UI BEGIN---"+mOperate);
    if (tPGrpEdorConfirmBL.submitData(cInputData,mOperate) == false)
    {
  		// @@错误处理
      this.mErrors.copyAllErrors(tPGrpEdorConfirmBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "PGrpEdorConfirmUI";
      tError.functionName = "submitData";
      tError.errorMessage = "提交失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
	mResult = tPGrpEdorConfirmBL.getResult();
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  public static void main(String[] args)
  {
	  VData tInputData = new VData();
	  GlobalInput tGlobalInput = new GlobalInput();
	  PGrpEdorConfirmBL aPGrpEdorConfirmBL = new PGrpEdorConfirmBL();
	  LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
	  tGlobalInput.ManageCom = "86";
	  tGlobalInput.Operator = "endor";

	  tLPGrpEdorMainSchema.setEdorAcceptNo("20060307000048");
	  tLPGrpEdorMainSchema.setGrpContNo("0000035701");
	  tLPGrpEdorMainSchema.setEdorNo("20060307000048");
	  String strTemplatePath = "xerox/printdata/";

	  tInputData.addElement(strTemplatePath);
	  tInputData.addElement(tLPGrpEdorMainSchema);
	  tInputData.addElement(tGlobalInput);

      if (!aPGrpEdorConfirmBL.submitData(tInputData, "INSERT||GRPEDORCONFIRM"))
      {
          System.out.println(aPGrpEdorConfirmBL.mErrors.getErrContent());
      }

  //  LPGrpContDB tLPGrpContDB = new LPGrpContDB();
  //  tLPGrpContDB.setGrpContNo("0000035701");
  //  tLPGrpContDB.setEdorNo("20060307000048");
  //  tLPGrpContDB.setEdorType("AC");
  //  if (!tLPGrpContDB.getInfo())
  //  {
  //  }

  }
}
