package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全减人项目</p>
 * <p>Description: 录入保全明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorTQDetailUI
{
    GrpEdorTQDetailBL mGrpEdorTQDetailBL = null;

    /**
     * 调用业务逻辑
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
    {
    	mGrpEdorTQDetailBL = new GrpEdorTQDetailBL();
        if (!mGrpEdorTQDetailBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mGrpEdorTQDetailBL.mErrors.getFirstError();
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mGrpEdorTQDetailBL.getMessage();
    }

    /**
     * 主函数，测试用
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "pa0001";
        gi.ManageCom = "86";

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorNo("20060824000024");
        tLPGrpEdorItemSchema.setEdorType("ZT");
        tLPGrpEdorItemSchema.setGrpContNo("0000002701");

        String feeRate = "25";
        EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(
                tLPGrpEdorItemSchema);
        tEdorItemSpecialData.add("CalTime", "1");
        tEdorItemSpecialData.add("FeeRate",
                String.valueOf(Double.parseDouble(feeRate) / 100));

        VData data = new VData();
        data.add(gi);
        data.add(tLPGrpEdorItemSchema);
        data.add(tEdorItemSpecialData);

        GrpEdorTQDetailUI tGrpEdorTQDetailUI = new GrpEdorTQDetailUI();
        if (!tGrpEdorTQDetailUI.submitData(data))
        {
            System.out.println(tGrpEdorTQDetailUI.getError());
        }

    }
}
