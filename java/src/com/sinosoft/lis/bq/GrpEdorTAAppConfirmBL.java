package com.sinosoft.lis.bq;

import java.util.Date;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 特需医疗团体账户资金分配</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class GrpEdorTAAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = BQ.EDORTYPE_TA;

    /** 保全生效日期 */
    private String mEdorValiDate = null;

    /** 磁盘导入数据  */
    private LPDiskImportSet mLPDiskImportSet = null;

    /** 团体合同号 */
    private String mGrpContNo = null;

    /**特许险种号*/
    private String riskcode = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mEdorNo = edorItem.getEdorNo();
        mEdorType = edorItem.getEdorType();
        mGrpContNo = edorItem.getGrpContNo();
        mEdorValiDate = edorItem.getEdorValiDate();
        mLPDiskImportSet = CommonBL.getLPDiskImportSet
                (mEdorNo, mEdorType, mGrpContNo, BQ.IMPORTSTATE_SUCC);
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        riskcode = getUliRisk(mGrpContNo);
        if(riskcode==null)
        {
            mErrors.addOneError("查询保单的险种错误！");
            return false;
        }

        if (!setInsuredAcc())
        {
            return false;
        }
        if (!setPublicAcc())
        {
            return false;
        }
        //确定保全生效日期，为保全理算日的次日
        /*Date tValiDate = null;
        FDate fDate = new FDate();
        tValiDate = fDate.getDate(PubFun.getCurrentDate());
        tValiDate = PubFun.calDate(tValiDate, 1, "D", null);*/
        String tValiDate = PubFun.getCurrentDate();
        
        boolean flag = PubFun.isDoBQ(mGrpContNo, tValiDate);
        if(!flag){
        	CError.buildErr(this, "该保单保全生效日的上个月还未月结，请先进行月结再操作该保全！");
         	return false;
        }
        tValiDate = PubFun.calDate(tValiDate, 1, "D", null);
        mMap.put("update lpgrpedormain set EdorValidate='"+tValiDate+"' where  Edorno='"+mEdorNo+"' and GrpContno='"+mGrpContNo+"'", "UPDATE");
        mMap.put("update lpgrpedoritem set EdorValidate='"+tValiDate+"', modifydate='"+PubFun.getCurrentDate()+"',edorstate='2' where  Edorno='"+mEdorNo+"' and GrpContno='"+mGrpContNo+"'", "UPDATE");
        return true;
    }
    /**
     * 把LCInsureAcc中的信息存入P表
     * @return boolean
     */
    private boolean setInsuredAcc()
    {

        String insuAccNo = getInsuAccNo(BQ.ACCTYPE_INSURED,riskcode);
        String tPayPlanCode = getPayPlanCode(insuAccNo);
        LCInsureAccSet tGGLCInsureAccSet = getPublicAcc();
        LCInsureAccSchema tGGLCInsureAccSchema = tGGLCInsureAccSet.get(1);
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsureAccSet tLCInsureAccSet = getLCInsureAcc(tLPDiskImportSchema);
            for (int j = 1; j <= tLCInsureAccSet.size(); j++)
            {
                LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccSet.get(j);
                if ((insuAccNo != null) &&
                        insuAccNo.equals(tLCInsureAccSchema.getInsuAccNo()))
                {
                    LPInsureAccSchema tLPInsureAccSchema = new
                            LPInsureAccSchema();
                    Reflections ref = new Reflections();
                    ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
                    double money = tLPInsureAccSchema.getInsuAccBala() +
                            tLPDiskImportSchema.getMoney();
                    tLPInsureAccSchema.setInsuAccBala(money);
                    tLPInsureAccSchema.setEdorNo(mEdorNo);
                    tLPInsureAccSchema.setEdorType(mEdorType);
                    tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
                  //  tLPInsureAccSchema.setMakeDate(mCurrentDate);
                  //  tLPInsureAccSchema.setMakeTime(mCurrentTime);
                    tLPInsureAccSchema.setModifyDate(mCurrentDate);
                    tLPInsureAccSchema.setModifyTime(mCurrentTime);
                    mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
                    setDiskImport(tLPDiskImportSchema, money);
                    
                    LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
                    tLCInsureAccClassDB.setGrpContNo(mGrpContNo);
                    tLCInsureAccClassDB.setInsuAccNo(insuAccNo);
                    tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
                    LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
                    if(tLCInsureAccClassSet.size()>=1)
                    {
                    	LCInsureAccClassSchema tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1).getSchema();
                    	LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
                    	ref.transFields(tLPInsureAccClassSchema, tLCInsureAccClassSchema);
                    	tLPInsureAccClassSchema.setInsuAccBala(money);
                    	tLPInsureAccClassSchema.setEdorNo(mEdorNo);
                    	tLPInsureAccClassSchema.setEdorType(mEdorType);
                    	tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
                    	tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
                    	tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
                        mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
                    }
                    
                    //导入金额的帐户为正
                    setLPInsureAccTrace(tLCInsureAccSchema,Math.abs(tLPDiskImportSchema.getMoney()),tGGLCInsureAccSchema.getPolNo(),tGGLCInsureAccSchema.getInsuAccNo(),tPayPlanCode);
                }
            }
        }
        return true;
    }

    /**
     * 处理公共账户
     * @return boolean
     */
    private boolean setPublicAcc()
    {
        double importMoney = getImportMoney();
        double grpAccountMeoney = getGrpAccountMoney(mGrpContNo);
        double interest = getInterest();
        if (importMoney > (grpAccountMeoney + interest))
        {
            mErrors.addOneError("转入各账户的金额超过团体帐户资金余额加利息！");
            return false;
        }

        String insuAccNo = getInsuAccNo(BQ.ACCTYPE_GROUP,riskcode);
        String tPayPlanCode = getPayPlanCode(insuAccNo);
        LCInsureAccSet tLCInsureAccSet = getPublicAcc();
        for (int i = 1; i <= tLCInsureAccSet.size(); i++)
        {
            LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccSet.get(i);
            if ((insuAccNo != null) &&
                    insuAccNo.equals(tLCInsureAccSchema.getInsuAccNo()))
            {
                LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
                Reflections ref = new Reflections();
                ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
                double money = grpAccountMeoney - importMoney;
                double lastAccBala = tLPInsureAccSchema.getInsuAccBala();
                double sumPay = tLPInsureAccSchema.getSumPay();
                double newSumPay = sumPay - importMoney;
                tLPInsureAccSchema.setLastAccBala(lastAccBala);
                tLPInsureAccSchema.setInsuAccBala(money);
                tLPInsureAccSchema.setSumPay(newSumPay);
                tLPInsureAccSchema.setEdorNo(mEdorNo);
                tLPInsureAccSchema.setEdorType(mEdorType);
                tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
          //     tLPInsureAccSchema.setMakeDate(mCurrentDate);
          //      tLPInsureAccSchema.setMakeTime(mCurrentTime);
                tLPInsureAccSchema.setModifyDate(mCurrentDate);
                tLPInsureAccSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
                
                LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
                tLCInsureAccClassDB.setGrpContNo(mGrpContNo);
                tLCInsureAccClassDB.setInsuAccNo(insuAccNo);
                tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
                LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
                if(tLCInsureAccClassSet.size()>=1)
                {
                	LCInsureAccClassSchema tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1).getSchema();
                	LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
                	ref.transFields(tLPInsureAccClassSchema, tLCInsureAccClassSchema);
                	tLPInsureAccClassSchema.setLastAccBala(lastAccBala);
                	tLPInsureAccClassSchema.setInsuAccBala(money);
                	tLPInsureAccClassSchema.setSumPay(newSumPay);
                	tLPInsureAccClassSchema.setEdorNo(mEdorNo);
                	tLPInsureAccClassSchema.setEdorType(mEdorType);
                	tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
                	tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
                	tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
                    mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
                }
                
                //导入金额的帐户为正--轨迹为付
                setLPInsureAccTrace(tLCInsureAccSchema,-Math.abs(importMoney),"","",tPayPlanCode);
            }
        }
        
        
        return true;
    }

    /**
     * 得到团体公共帐户信息
     * @param polNo String
     * @return LCInsureAccSchema
     */
    private LCInsureAccSet getPublicAcc()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(mGrpContNo);
        tLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
//        tLCPolDB.setAccType(BQ.ACCTYPE_PUBLIC);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() == 0)
        {
            return null;
        }
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(tLCPolSet.get(1).getPolNo());
        return tLCInsureAccDB.query();
    }

    /**
     * 得到磁盘导入的资金总数
     * @return double
     */
    private double getImportMoney()
    {
        double importMoney = 0.00;
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            importMoney += mLPDiskImportSet.get(i).getMoney();
        }
        return importMoney;
    }

    /**
     * 得到利息
     * @return double
     */
    private double getInterest()
    {
        double interest = 0.00;
        return interest;
    }

    /**
     * 得到LCInsureAcc表的数据
     * @param aLPDiskImportSchema LPDiskImportSchema
     * @return LCInsureAccSchema
     */
    private LCInsureAccSet getLCInsureAcc(LPDiskImportSchema aLPDiskImportSchema)
    {
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setGrpContNo(aLPDiskImportSchema.getGrpContNo());
        tLCInsureAccDB.setInsuredNo(aLPDiskImportSchema.getInsuredNo());
        return tLCInsureAccDB.query();
    }

    /**
     * 设置LPDiskImport中显示的信息
     */
    private void setDiskImport(LPDiskImportSchema tLPDiskImportSchema,
            double leftMoney)
    {
        tLPDiskImportSchema.setGetMoney(leftMoney);
        tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
        tLPDiskImportSchema.setModifyDate(mCurrentDate);
        tLPDiskImportSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPDiskImportSchema, "DELETE&INSERT");
    }
        /**
     * 设置帐户分配轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTrace(LCInsureAccSchema aLCInsureAccSchema, double grpMoney,String aAIPolNo,String aAIInsuAccNo,String aPayPlanCode)
    {
        String serialNo;
        LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
        tLPInsureAccTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccTraceDB.setEdorType(mEdorType);
        tLPInsureAccTraceDB.setGrpContNo(mGrpContNo);
        tLPInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
        if (tLPInsureAccTraceSet.size() > 0)
        {
            serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccTrace " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        }
        else
        {
            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        }
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(aPayPlanCode);
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType("TA");
        tLPInsureAccTraceSchema.setMoney(grpMoney);
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        tLPInsureAccTraceSchema.setAIPolNo(aAIPolNo);
        tLPInsureAccTraceSchema.setAIInsuAccNo(aAIInsuAccNo);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }
    
    // by gzh 获取险种编码
    private String getUliRisk(String aGrpContNo){
    	LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(aGrpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLCGrpPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                return null;
            }
            String riskType4 = tLMRiskAppDB.getRiskType4();
            String riskProp= tLMRiskAppDB.getRiskProp();
            if ((riskType4 != null && riskType4.equals("4")) 
            		&& (riskProp != null && riskProp.equals("G")))
            {
                return tLCGrpPolSet.get(i).getRiskCode();
            }
        }
        return null;
    }
    // by gzh 
    /**
     * 得到团体万能险种团体理赔帐户余额
     * @param grpContNo String
     * @throws Exception
     * @return double
     */
    private double getGrpAccountMoney(String grpContNo)
    {
        
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setGrpContNo(grpContNo);
        tLCInsureAccDB.setAccType(BQ.ACCTYPE_GROUP);
        tLCInsureAccDB.setRiskCode(riskcode);
        LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
        return tLCInsureAccSet.get(1).getInsuAccBala();
    }
    /**
     * 根据账户类型和险种编码得到账户类型
     * @param insuAccNo String
     * @return String
     */
    private String getInsuAccNo(String accType, String riskCode)
    {
        String sql = "select distinct a.InsuAccNo from "
                + "LMRiskInsuAcc a, LMRiskToAcc b "
                + "where a.InsuAccNo = b.InsuAccNo " + "and a.AccType = '"
                + accType + "' " + "and b.riskCode = '" + riskCode + "' "
                +" order by insuaccno";
        return (new ExeSQL()).getOneValue(sql);
    }
    
    //获取对应账户的保障计划编码
    private String getPayPlanCode(String aInsuAccNo){
    	String sql = "select payplancode from lmriskaccpay where riskcode = '"+riskcode+"' and insuaccno = '"+aInsuAccNo+"'";
    	return (new ExeSQL()).getOneValue(sql);
    }
}
