package com.sinosoft.lis.bq;

/**
 * <p>Title: 保全团险新增被保人保存申请</p>
 * <p>Description: 把保全状态置为申请确认状态</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;

public class GEdorNICheckBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private MMap mMap = new MMap();

    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局基础数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();

    public GEdorNICheckBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData VData 传入的数据
     * @param cOperate String 数据操作字符串
     * @return boolean  true--提交成功, false--提交失败
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getSubmitData(cInputData, cOperate);

         //数据提交
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "GEdorNICheckBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public boolean getSubmitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @return boolean
     */
    private boolean getInputData()
    {

        return true;
    }

    private boolean getInsuredList()
    {
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        tLCInsuredListDB.setGrpContNo("");
        mLCInsuredListSet = tLCInsuredListDB.query();
        if (mLCInsuredListSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "GEdorNICheckBL";
            tError.functionName = "submitData";
            tError.errorMessage = "查不到导入的数据！";
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return boolean 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (!getInsuredList())
        {
            return false;
        }

        //检查新增的被保人是否和已有客户重复
        if (!checkInsured())
        {
            return false;
        }

        if (!checkContPlan())
        {
            return false;
        }

        return true;
    }

    /**
     * 检查客户资料是否重复
     * @return boolean
     */
    private boolean checkInsured()
    {
        for (int i = 1; i <= mLCInsuredListSet.size(); i++)
        {
           LCInsuredListSchema tLCInsuredListSchema = mLCInsuredListSet.get(i);
           LCInsuredDB tLCInsuredDB = new LCInsuredDB();
           tLCInsuredDB.setName(tLCInsuredListSchema.getInsuredName());
           tLCInsuredDB.query();
        }
        return true;
    }

    private boolean checkContPlan()
    {
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.add(mMap);
        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

}
