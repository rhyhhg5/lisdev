package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.PubFun;

import java.util.HashMap;
import java.util.Vector;

import com.sinosoft.lis.pubfun.AccountManage;
import com.sinosoft.lis.pubfun.BqCalBase;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 本类用来实现退保试算功能
 * 在协议退保等业务操作前，可能会预先计算正常退保退费，评估操作的可行性
 * 调用正常退费核心程序进行算费
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author not attributable
 * @version 1.1
 */
public class EdorCalZTTestBL
{
    /**错误的容器*/
    private static final String LCPOLSCHEMA = LCPolSchema.class.getName();
    private static final String STRING = java.lang.String.class.getName();

    public CErrors mErrors = new CErrors();
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;  //团体保全项目
    private LPEdorItemSchema mLPEdorItemSchema = null;  //个体保全项目

    private Reflections ref = new Reflections();
    private String mEdorValiDate = null;   //试算生效日期
    private String mCurPayToDateLongPol = null;  //长期限预计交至日期
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
    private String operator = null;
    private FDate tFDate = new FDate();

    private HashMap mHashMap = new HashMap();
    private boolean mNeedBugetResult = false;
    private LGErrorLogSet mLGErrorLogSet = new LGErrorLogSet();
    private LPBudgetResultSchema mLPBudgetResultSchema = null;  //试算结果
    private int mPolYear ;
    private int mPolMonth ;

    public EdorCalZTTestBL()
    {
    }
    public void setOperator(String operator)
    {
        this.operator = operator;
    }
    /**
     * 传入团单保全项目信息，为走正常退保流程做准备
     * @param item LPGrpEdorItemSchema
     */
    public void setLPGrpEdorItemInfo(LPGrpEdorItemSchema item)
    {
        this.mLPGrpEdorItemSchema = item;
    }

    /**
     * 传入个单保全项目信息，为走正常退保流程做准备
     * @param item LPGrpEdorItemSchema
     */
    public void setLPEdorItemInfo(LPEdorItemSchema item)
    {
        this.mLPEdorItemSchema = item;
    }

    /**
     * 个人险种退保试算
     * 由于退保试算走正常退费的流程，需要构造保全项目信息
     * @param obj Object
     * @return LJSGetEndorseSet
     */
    public LJSGetEndorseSchema budgetOnePol(Object obj)
    {
        //构造保全项目信息
        if (this.mLPEdorItemSchema == null)
        {
            this.mLPEdorItemSchema = createCmnLPEdorItemInfo();
            if (this.mLPEdorItemSchema == null)
            {
                return null;
            }
        }

        LCPolSchema tLCPolSchema = getLCPolInfo(obj);

        mLPEdorItemSchema.setContNo(tLCPolSchema.getContNo());
        mLPEdorItemSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
        mLPEdorItemSchema.setPolNo(tLCPolSchema.getPolNo());
        mLPEdorItemSchema.setOperator(operator);
        //qulq 061127
        //查找保单
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tLCPolSchema.getContNo());
        if(!tLCContDB.getInfo())
        {
            mErrors.addOneError("查询保单失败。");
            return null;
        }

        if(!this.checkEdorDate(mLPEdorItemSchema.getEdorValiDate(),tLCContDB.getSchema()))
        {
            mErrors.addOneError("预计生效日期不能晚于保单的终止日。");
            return null;
        }
        if(!this.checkPol(mLPEdorItemSchema.getEdorValiDate(),tLCPolSchema))
        {
            if(isLongPol(tLCPolSchema))
                mErrors.addOneError("所选险种为长期险，预计退保生效日不能晚于险种终止日。");
            else
                mErrors.addOneError("所选险种为短期险，预计退保生效日期不能晚于本期交至日。");
            return null;
        }
        if(CommonBL.hasULIRisk(tLCPolSchema.getContNo())&&CommonBL.hasAttachULIRisk(tLCPolSchema.getContNo()))
        {
        	return budgetULIPol(tLCPolSchema);
        }
        else if(!CommonBL.hasAttachULIRisk(tLCPolSchema.getContNo())&&CommonBL.isULIRisk(tLCPolSchema.getRiskCode()))
        {
        	return budgetULIPol(tLCPolSchema);
        }else{
        	mLGErrorLogSet.clear();
        	EdorCalZT mEdorCalZT = new EdorCalZT("1");  //退保数据处理类
            if(isLongPol(tLCPolSchema))
            {
                mEdorCalZT.setCurPayToDateForesee(this.mCurPayToDateLongPol);
            }
            else
            {
                mEdorCalZT.setCurPayToDateForesee(null);
            }

            //调用退计算类计算退费
            try
            {
                mEdorCalZT.calZTData(mLPEdorItemSchema);
            }
            catch(Exception e)
            {
                mErrors.copyAllErrors(mEdorCalZT.mErrors);
                mErrors.addOneError("退保试算失败");
                System.out.println(mErrors.getErrContent());
                e.printStackTrace();
                return null;
            }
            if (mEdorCalZT.mErrors.needDealError())
            {
                mErrors.copyAllErrors(mLPEdorItemSchema.mErrors);
                mErrors.addOneError("退保计算失败");
                return null;
            }
            //取得计算错误信息
            LGErrorLogSet tLGErrorLogSet = mEdorCalZT.getErrorLogSet();
            for (int i = 1; i <= tLGErrorLogSet.size(); i++) {
                LGErrorLogSchema tLGErrorLogSchema = tLGErrorLogSet.get(i);
                tLGErrorLogSchema.setOtherNoType(mLPEdorItemSchema.getEdorType());
                tLGErrorLogSchema.setOtherNo(mLPEdorItemSchema.getEdorAcceptNo());
                tLGErrorLogSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                tLGErrorLogSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                tLGErrorLogSchema.setContNo(tLCPolSchema.getContNo());
                tLGErrorLogSchema.setPolNo(tLCPolSchema.getPolNo());
                tLGErrorLogSchema.setOperator(operator);
                String currDate = PubFun.getCurrentDate();
                String currTime = PubFun.getCurrentTime();
                tLGErrorLogSchema.setMakeDate(currDate);
                tLGErrorLogSchema.setMakeTime(currTime);
                tLGErrorLogSchema.setModifyDate(currDate);
                tLGErrorLogSchema.setModifyTime(currTime);
                mLGErrorLogSet.add(tLGErrorLogSchema.getSchema());
                System.out.println(tLGErrorLogSchema.getDescribe()+"fdfjasdklfjsad");
            }
            if(CommonBL.isTDBCPol(tLCPolSchema.getRiskCode()))
            {
            	MMap map = new MMap();
            	String accSQL="delete from lpinsureacc where edorno='000000' and grpcontno='"+tLCPolSchema.getGrpContNo()+"' and polno='"+tLCPolSchema.getPolNo()+"'";
            	String accFeeSQL="delete from lpinsureaccfee where edorno='000000' and grpcontno='"+tLCPolSchema.getGrpContNo()+"' and polno='"+tLCPolSchema.getPolNo()+"'";
            	String accTraceSQL="delete from lpinsureacctrace where edorno='000000' and grpcontno='"+tLCPolSchema.getGrpContNo()+"' and polno='"+tLCPolSchema.getPolNo()+"'";
            	String accClassSQL="delete from lpinsureaccclass where edorno='000000' and grpcontno='"+tLCPolSchema.getGrpContNo()+"' and polno='"+tLCPolSchema.getPolNo()+"'";
            	String accClassFeeSQL="delete from lpinsureaccclassfee where edorno='000000' and grpcontno='"+tLCPolSchema.getGrpContNo()+"' and polno='"+tLCPolSchema.getPolNo()+"'";
            	VData tVData = new VData();
            	map.put(accSQL, SysConst.DELETE);
            	map.put(accFeeSQL, SysConst.DELETE);
            	map.put(accTraceSQL, SysConst.DELETE);
            	map.put(accClassSQL, SysConst.DELETE);
            	map.put(accClassFeeSQL, SysConst.DELETE);
                tVData.add(map);
                PubSubmit p = new PubSubmit();
                if (!p.submitData(tVData, "DELETE")) {
                    mErrors.copyAllErrors(p.mErrors);
                }
            	
            }

            if(mNeedBugetResult)
            {
                //提交错误日志
                MMap map = new MMap();
                map.put(mLGErrorLogSet, "DELETE&INSERT");
                VData tVData = new VData();
                tVData.add(map);

                PubSubmit p = new PubSubmit();
                if (!p.submitData(tVData, "DELETE&INSERT")) {
                    mErrors.copyAllErrors(p.mErrors);
                }
            }
            //转换成按险种生成财务数据
            LJSGetEndorseSet set = mEdorCalZT.mLJSGetEndorseSet;
            if(set == null || set.size() == 0)
            {
                return null;
            }
            for(int i = 2; i <= set.size(); i++)
            {
                set.get(1).setGetMoney(set.get(1).getGetMoney()
                                       + set.get(i).getGetMoney());
            }
            set.get(1).setGetMoney(PubFun.setPrecision(set.get(1)
                                                       .getGetMoney(), "0.00"));
            //add by lzy20150909在试算金额中添加金生无忧满期金及利息
            if("340101".equals(tLCPolSchema.getRiskCode())){
            	LJSGetEndorseSet EbSet=calBenefitGet();
            	double getEb=0;
            	for(int j = 1; j <= EbSet.size(); j++)
                {
            		getEb +=EbSet.get(j).getGetMoney();
                }
            	getEb=PubFun.setPrecision(getEb, "0.00");
            	set.get(1).setGetMoney(set.get(1).getGetMoney()+getEb);
            	//在备注里加上满期金额的提示
            	LGErrorLogSchema tLGErrorLogSchema=new LGErrorLogSchema();
            	String seriNo = PubFun1.CreateMaxNo("ERRORLOGNO", 20);
            	tLGErrorLogSchema.setSerialNo(seriNo);
            	tLGErrorLogSchema.setErrorType("0001");
                tLGErrorLogSchema.setErrorTypeSub("02");
            	tLGErrorLogSchema.setOtherNoType(mLPEdorItemSchema.getEdorType());
                tLGErrorLogSchema.setOtherNo(mLPEdorItemSchema.getEdorAcceptNo());
                tLGErrorLogSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                tLGErrorLogSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                tLGErrorLogSchema.setContNo(tLCPolSchema.getContNo());
                tLGErrorLogSchema.setPolNo(tLCPolSchema.getPolNo());
                tLGErrorLogSchema.setOperator(operator);
                String currDate = PubFun.getCurrentDate();
                String currTime = PubFun.getCurrentTime();
                tLGErrorLogSchema.setMakeDate(currDate);
                tLGErrorLogSchema.setMakeTime(currTime);
                tLGErrorLogSchema.setModifyDate(currDate);
                tLGErrorLogSchema.setModifyTime(currTime);
                tLGErrorLogSchema.setDescribe("金生无忧退保金包括保单未领取的满期金及利息："+Math.abs(getEb));
                mLGErrorLogSet.add(tLGErrorLogSchema.getSchema());
                //提交错误日志
                MMap map = new MMap();
                map.put(mLGErrorLogSet, "DELETE&INSERT");
                VData tVData = new VData();
                tVData.add(map);

                PubSubmit p = new PubSubmit();
                if (!p.submitData(tVData, "DELETE&INSERT")) {
                    mErrors.copyAllErrors(p.mErrors);
                }
            }
            
            //生成试算结果数据
            if(mNeedBugetResult)
            {
                mLPBudgetResultSchema = new LPBudgetResultSchema();
                ref.transFields(mLPBudgetResultSchema, set.get(1));
                mLPBudgetResultSchema.setContPlanCode(tLCPolSchema.getContPlanCode());
                mLPBudgetResultSchema.setDutyCode(BQ.FILLDATA);
                mLPBudgetResultSchema.setPayPlanCode(BQ.FILLDATA);
                mLPBudgetResultSchema.setLastPayToDate(mEdorCalZT.getLastPayToDate());
                mLPBudgetResultSchema.setCurPayToDate(mEdorCalZT.getCurPayToDate());
                mLPBudgetResultSchema.setEdorValiDate(mLPEdorItemSchema.getEdorValiDate());
                mLPBudgetResultSchema.setBudgetDate(mCurDate);
                mLPBudgetResultSchema.setBudgetTime(mCurTime);
                PubFun.fillDefaultField(mLPBudgetResultSchema);
            }
            return set.get(1);
        }
    }

    /**
     * 设置需要试算结果标志
     * @param flag boolean
     */
    public void setNeedBugetResultFlag(boolean flag)
    {
        this.mNeedBugetResult = flag;
    }

    /**
     * 得到险种试算的结果
     * @return LPBudgetResultSchema
     */
    public LPBudgetResultSchema getOnePolLPBudgetResult()
    {
        return mLPBudgetResultSchema;
    }
    public LGErrorLogSet getOnePolLGErrorLogSet()
    {
        return mLGErrorLogSet;
    }
    /**
     * 判断险种是否长期险
     * @param schema LCPolSchema
     * @return boolean
     */
    private boolean isLongPol(LCPolSchema schema)
    {
        String riskPeriod = (String) mHashMap.get(schema.getRiskCode());
        if(riskPeriod == null)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(schema.getRiskCode());
            tLMRiskAppDB.getInfo();

            mHashMap.put(schema.getRiskCode(), tLMRiskAppDB.getRiskPeriod());
            riskPeriod = tLMRiskAppDB.getRiskPeriod();
        }

        if(riskPeriod.equals("L"))
        {
            return true;
        }
        return false;
    }

    /**
     * 得到需要退保试算的险种信息
     * @param obj Object：可能以险种号或LCPolSchema两种方式传入
     * 若传入险种号，则需查询得到险种信息
     * @return LCPolSchema
     */
    private LCPolSchema getLCPolInfo(Object obj)
    {
        String className = obj.getClass().getName();
        if(className.equals(LCPOLSCHEMA))
        {
            return (LCPolSchema) obj;
        }
        else if (className.equals(STRING))
        {
            LCPolDB db = new LCPolDB();
            db.setPolNo((String) obj);
            if (!db.getInfo())
            {
                mErrors.addOneError("没有查询到险种信息。");
                System.out.println("没有查询到险种信息" + (String) obj);
                return null;
            }
            return db.getSchema();
        }
        return null;
    }

    /**
     * 通过团单项目信息生成个单项目公共信息，不包括具体的个保单或被保人或险种信息
     * @param polNo String
     * @return LPEdorItemSchema
     */
    private LPEdorItemSchema createCmnLPEdorItemInfo()
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();

        if(this.mLPGrpEdorItemSchema != null)
        {
            ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);
            return tLPEdorItemSchema;
        }

        tLPEdorItemSchema.setEdorAcceptNo("000000");  //这么写是为了数据的完整性，试算过程中不EdorAcceptNo没有实际意义
        tLPEdorItemSchema.setEdorAppNo(tLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemSchema.setEdorNo(tLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemSchema.setEdorType(BQ.EDORTYPE_CT);
        tLPEdorItemSchema.setEdorAppDate(mCurDate);
        tLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
        if(mEdorValiDate == null)
        {
            tLPEdorItemSchema.setEdorValiDate(mCurDate);
        }
        else
        {
            tLPEdorItemSchema.setEdorValiDate(mEdorValiDate);
        }
        return tLPEdorItemSchema;
    }

    /**
     * qulq 061127 添加 退保试算时间校验
     * @param date String  退保日期
     * @param tLCPolSchema 退保保单
     */
    private boolean checkEdorDate(String date,LCContSchema tLCContSchema)
    {
        int interval = PubFun.calInterval(date,tLCContSchema.getCInValiDate(),"D");
        return (interval>=0);
    }
    /**
     *  qulq 061127 添加 退保试算时间校验
     * @param date String 退保日期
     * @param tLCPolSchema LCPolSchema 退保保单
     * @return boolean
     */
    private boolean checkPol(String date,LCPolSchema tLCPolSchema)
    {
         int interval;
        if(this.isLongPol(tLCPolSchema))
        {
            interval = PubFun.calInterval(date, tLCPolSchema.getEndDate(),
                                              "D");
            return (interval >= 0) ;
        }
        else
        {
            interval = PubFun.calInterval(date,tLCPolSchema.getPaytoDate(),
                                           "D");
            return (interval >= 0) ;
        }
    }

    public void setEdorValiDate(String date)
    {
        mEdorValiDate = date;
    }

    public void setCurPayToDateLongPol(String date)
    {
        mCurPayToDateLongPol = date;
    }
    
    //20081204 zhanggm 万能退保试算
    public LJSGetEndorseSchema budgetULIPol(LCPolSchema aLCPolSchema)
    {
    	LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
    	ExeSQL tExeSQL = new ExeSQL();
    	String tRiskCode = aLCPolSchema.getRiskCode();
    	double tGetMoney = 0.0;
    	String sql = "select RiskType4 from LMRiskApp where RiskCode = '" + tRiskCode + "' with ur";
    	String tRiskType4 =  tExeSQL.getOneValue(sql);
    	if(tRiskType4 != null && tRiskType4.equals(BQ.RISKTYPE1_ULI))
    	{
    		LCInsureAccClassSchema tLCInsureAccClassSchema = getLCInsureAccClass(aLCPolSchema);
        	calPolYearMonth(tLCInsureAccClassSchema);//获取保单年度
    		String tPolNo = aLCPolSchema.getPolNo();
            String tInsuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,aLCPolSchema.getRiskCode());
            sql = "select InsuAccBala from LCInsureAcc where PolNo = '" + tPolNo + "' "
                + "and InsuAccNo = '" + tInsuAccNo + "' with ur";
            double tInsureAccBala = Double.parseDouble(tExeSQL.getOneValue(sql)); //当前账户余额
            double tGuratRate = 0.025;//最低保证利率
            sql = "select max(Duebaladate) from LCInsureAccBalance where PolNo = '" + tPolNo + "' "
                + "and InsuAccNo = '" + tInsuAccNo + "' with ur";
            String tLastBalaDate = tExeSQL.getOneValue(sql);//最近一次结算日期
            int tDays = 0;//试算生效日期与最近一次结算日期的间隔,单位：天
            double tGuratLX= 0; //最低保证利息
            String checkDate = PubFun.calDate(aLCPolSchema.getCValiDate(), 1, "Y", null);
//          试算生效日期大于最近一次结算日期
            if (mEdorValiDate.compareTo(tLastBalaDate) > 0)
            {
            	tDays = PubFun.calInterval(tLastBalaDate, mEdorValiDate, "D");
            	 if((("332601".equals(aLCPolSchema.getRiskCode())||"332901".equals(aLCPolSchema.getRiskCode())
         				||"333001".equals(aLCPolSchema.getRiskCode())||"333201".equals(aLCPolSchema.getRiskCode())))&&(tFDate.getDate(checkDate).
         						compareTo(tFDate.getDate(PubFun.getCurrentDate()))<=0))
		         {
		         		tGuratLX = getBalaAccInterestU(tLCInsureAccClassSchema,aLCPolSchema);
		          
		         	
		         }
            	 else
            		 //tGuratLX = tGuratRate*tInsureAccBala*tDays/365;
            		 tGuratLX = getBalaAccInterest(tLCInsureAccClassSchema,aLCPolSchema);
            	tGuratLX = PubFun.setPrecision(tGuratLX, "0.00");
            }
            tGetMoney = tInsureAccBala + tGuratLX ;
            double tManageFee = getManageFee(tLCInsureAccClassSchema,aLCPolSchema);
            tGetMoney -= tManageFee;
            double tCTFee = getCTFee(tLCInsureAccClassSchema,tGetMoney,aLCPolSchema);
            tGetMoney -= tCTFee;
            System.out.println("当前账户余额: "+tInsureAccBala);
            System.out.println("最低保证利率: "+tGuratRate);
            System.out.println("最近结算日期: "+tLastBalaDate);
            System.out.println("结算利息: "+tGuratLX);
            System.out.println("当前年度解约手续： "+tCTFee);
            System.out.println("上一结算日到退保生效日的管理费及风险扣费和： "+tManageFee);
            System.out.println("退费金额: "+tGetMoney);
            tLJSGetEndorseSchema.setGetMoney("-"+tGetMoney);
    	}
    	else
    	{
    		tLJSGetEndorseSchema.setGetMoney(0);
    	}
    	
        //生成试算结果数据
        if(mNeedBugetResult)
        {
            mLPBudgetResultSchema = new LPBudgetResultSchema();
            ref.transFields(mLPBudgetResultSchema, mLPEdorItemSchema);
            mLPBudgetResultSchema.setContPlanCode(aLCPolSchema.getContPlanCode());
            mLPBudgetResultSchema.setDutyCode(BQ.FILLDATA);
            mLPBudgetResultSchema.setPayPlanCode(BQ.FILLDATA);
            mLPBudgetResultSchema.setLastPayToDate(mEdorValiDate);//保全生效日期
            mLPBudgetResultSchema.setCurPayToDate(mEdorValiDate);//保全生效日期
            mLPBudgetResultSchema.setEdorValiDate(mLPEdorItemSchema.getEdorValiDate());
            mLPBudgetResultSchema.setBudgetDate(mCurDate);
            mLPBudgetResultSchema.setBudgetTime(mCurTime);
            mLPBudgetResultSchema.setRiskCode(tRiskCode);
            mLPBudgetResultSchema.setFeeFinaType("TB");
            mLPBudgetResultSchema.setGetMoney(tGetMoney);
            mLPBudgetResultSchema.setGrpContNo(aLCPolSchema.getGrpContNo());
            mLPBudgetResultSchema.setContNo(aLCPolSchema.getContNo());
            mLPBudgetResultSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
            mLPBudgetResultSchema.setOperator(operator);
            PubFun.fillDefaultField(mLPBudgetResultSchema);
        }
        return tLJSGetEndorseSchema;
    }
    
   
    //20090113 Zhang Guoming 万能退保试算扣除解约管理费 
    private double getCTFee(LCInsureAccClassSchema tLCInsureAccClassSchema,double aGetMoney,LCPolSchema aLCPolSchema)
    {
    	double tCTFee = 0.0;
    	double tCTFeeRate = 0.0;
    	ExeSQL tExeSQL = new ExeSQL();
        LMEdorZTAccDB tLMEdorZTAccDB = new LMEdorZTAccDB();
        tLMEdorZTAccDB.setRiskCode(tLCInsureAccClassSchema.getRiskCode());
        tLMEdorZTAccDB.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());

        //只有部分险种才有帐户退保描述，所有没有描述也正常
        if (!tLMEdorZTAccDB.getInfo())
        {

        String tRiskCode=aLCPolSchema.getRiskCode().trim();
        int Years = PubFun.calInterval(aLCPolSchema.getCValiDate(), mEdorValiDate, "Y")+1;
        
        String sql = "select Rate from db2inst1.CTrate"+tRiskCode+" where MaxYear >= "+Years+" and MinYear< "+Years+" with ur";
        
        if(tRiskCode.equals("332901") || tRiskCode.equals("333001") ){
        	
        	sql = " select (case when "+Years+" = 1 then  " +
        			" 0.02 when "+Years+" <= 5 and "+Years+" >= 2 then  " +
        					" (select Cancelrate     from LCRiskFeeRate   " +
        					"  where '"+aLCPolSchema.getPrtNo()+"' = prtno  and  "+Years+" = Feeyear) " +
        							"when "+Years+" > 5 then  0  end) from dual with ur ";
        }
        
    	System.out.println("查询万能解约费率: "+sql);
    	
    	//由于存在解约费率为0,费率表不存在的情况,现将这段try起来,报错就表示无法查询,费用默认为0 add by xp 20100310
        try{
        	tCTFeeRate = Double.parseDouble(tExeSQL.getOneValue(sql));
        }
        catch (Exception e) {        		
        		System.out.println("查询万能解约费率出错,险种"+tRiskCode);
			}     
        
        System.out.println("当前年度解约手续费率: "+tCTFeeRate);
        
        tCTFee = aGetMoney * tCTFeeRate;
        }else{
            //取得趸交、期交算法编码；扣除2%的解约费
            String tCalCode = tLMEdorZTAccDB.getSchema().getCalCode();
            //取得帐户现金余额(本金+利息)
            BqCalBase tBqCalBase = new BqCalBase();
            //先算本金管理费
            String tSQL = "select year('" + mLPEdorItemSchema.getEdorValiDate() +
                          "'-CValidate)+1 from LCPol where Polno = '" +
                          tLCInsureAccClassSchema.getPolNo() + "'";
            //设置保单年度，万能计算解约手续费需要此参数
            tBqCalBase.setUpPolYears(new ExeSQL().getOneValue(tSQL));
            tBqCalBase.setGetMoney(aGetMoney);
            tBqCalBase.setInsuAccBala(aGetMoney);
            tBqCalBase.setGrpContNo(tLCInsureAccClassSchema.getGrpContNo());
            tBqCalBase.setGrpPolNo(tLCInsureAccClassSchema.getGrpPolNo());
            tBqCalBase.setRiskCode(tLCInsureAccClassSchema.getRiskCode());
            tBqCalBase.setContNo(tLCInsureAccClassSchema.getContNo());
            tBqCalBase.setPolNo(tLCInsureAccClassSchema.getPolNo());
            tBqCalBase.setAccRate(100); //利率
            tBqCalBase.setAccManageFeeRateZT(
                    CommonBL.getManageFeeRate(tLCInsureAccClassSchema.getGrpPolNo(),
                                              BQ.FEECODE_CT));
            //税优保单
            String str="select 1 from lcpol where contno='" +mLPEdorItemSchema.getContNo()+ "' and riskcode in (select riskcode from lmriskapp where  taxoptimal='Y')";
            String sy=new ExeSQL().getOneValue(str);
            if(null != sy && !"".equals(sy)){
                String tTaxMoney="0";//试算时先将税收优惠额度默认为零，在理算时处理。
                tBqCalBase.setSYTaxMoney(tTaxMoney);
            }
            BqCalBL tBqCalBL = new BqCalBL(mLPEdorItemSchema, tBqCalBase,
                                           tCalCode,
                                           "");
            tCTFee = aGetMoney-tBqCalBL.calGetEndorse(tCalCode, tBqCalBase);
        }
    	return PubFun.setPrecision(tCTFee, "0.00");
    }
    
    
    /**
     * 获取万能险保单保单年度
     * @return
     */
    private boolean calPolYearMonth(LCInsureAccClassSchema tLCInsureAccClassSchema)
    {
        String sql = "select year(date('" + tLCInsureAccClassSchema.getBalaDate() +
                     "') - CValidate) + 1, "
                     + "   month(date('" + tLCInsureAccClassSchema.getBalaDate() + "') - CValidate) + 1 "
                     + "from LCPol "
                     + "where PolNo = '" + tLCInsureAccClassSchema.getPolNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        mPolYear = Integer.parseInt(tSSRS.GetText(1, 1));
        mPolMonth = Integer.parseInt(tSSRS.GetText(1, 2));

        return true;
    }
    
    /**
     * 获取万能账户信息
     * @param aLCPolSchema
     * @return
     */
    private LCInsureAccClassSchema getLCInsureAccClass(LCPolSchema aLCPolSchema){
    	
    	LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
    	LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
    	LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
    	tLCInsureAccClassDB.setPolNo(aLCPolSchema.getPolNo());
    	tLCInsureAccClassSet = tLCInsureAccClassDB.query();
    	if (tLCInsureAccClassSet != null && tLCInsureAccClassSet.size() == 1)
        {
    		tLCInsureAccClassSchema =  tLCInsureAccClassSet.get(1).getSchema();
    		return tLCInsureAccClassSchema;
        }
    	return null;
        
    }
    
    private LMRiskFeeSet getLMRiskFeeSetB(LCInsureAccClassSchema
            cLCInsureAccClassSchema)
    {
		//String tYear = (String) tTransferData.getValueByName("PolYear");
		int tMonth = mPolMonth;
		//满保单年度时才处理持续奖金.
		if (tMonth != 12)
		{
		return new LMRiskFeeSet();
		}
		//由于通过产品描述来判断较为复杂,暂时写死,先开发331201的持续奖金,后续调整为ldcode中描述
		if(!cLCInsureAccClassSchema.getRiskCode().equals("331201")){
		return new LMRiskFeeSet();
		}
		String tIsDead=new ExeSQL().getOneValue("select polno from ljagetclaim a where a.othernotype='5' " +
					" and exists (select 1 from ljagetclaim where actugetno=a.actugetno and feefinatype='SW') " +
					" and polno = '"+cLCInsureAccClassSchema.getPolNo()+"'  group by a.polno having sum(a.pay) >0 with ur ");
		//判断是否已经身故,通过是否发生身故理赔来判断
		if(tIsDead!=null&&(!tIsDead.equals(""))&&(!tIsDead.equals("null"))){
		return new LMRiskFeeSet();
		}
		LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
		tLMRiskFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
		tLMRiskFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
		tLMRiskFeeDB.setFeeTakePlace("07"); //每月扣除
		tLMRiskFeeDB.setFeeKind("03"); //个单管理费
		LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
		
		return tLMRiskFeeSet;
    }
    
    /**
     * 获取管理费及风险扣费金额
     * @return
     */
    private double getManageFee(LCInsureAccClassSchema cLCInsureAccClassSchema,LCPolSchema mLCPolSchema){

        //计算管理费
    	double fee = 0;
    	double sumfee = 0;
    	LCInsureAccBalanceDB tLCInsureAccBalanceDB = new LCInsureAccBalanceDB();
        tLCInsureAccBalanceDB.setPolNo(cLCInsureAccClassSchema.getPolNo());
        tLCInsureAccBalanceDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tLCInsureAccBalanceDB.setDueBalaDate(cLCInsureAccClassSchema.
                                             getBalaDate());
        LCInsureAccBalanceSet tLCInsureAccBalanceSet
                = tLCInsureAccBalanceDB.query();
        if (tLCInsureAccBalanceSet.size() == 0)
        {
            mErrors.addOneError("没有查询到上一次结算信息");
            return 0;
        }
    	TransferData tTransferData =new TransferData();
    	tTransferData.setNameAndValue("StartDate", cLCInsureAccClassSchema.getBalaDate());
        tTransferData.setNameAndValue("EndDate", mEdorValiDate);
        tTransferData.setNameAndValue("RiskCode",
        		mLCPolSchema.getRiskCode());
        tTransferData.setNameAndValue("BalaType", "1"); //1：账户价值结算，2：保障价值计算
        tTransferData.setNameAndValue("RateType", "2"); //1：取公布利率，2：取保证利率
        tTransferData.setNameAndValue("PolYear", String.valueOf(mPolYear));
        tTransferData.setNameAndValue("PolMonth", String.valueOf(mPolMonth));
        double tLastBala = tLCInsureAccBalanceSet.get(1).getInsuAccBalaAfter();
        double tLastBalaGurat = tLCInsureAccBalanceSet.get(1).
                                getInsuAccBalaGurat();
        tLastBalaGurat = (mPolYear != 1 && mPolMonth == 1 ? tLastBala :
                          tLastBalaGurat); //计算第一保单年度的保证收益时已上一年度末账户价值为基础
        tTransferData.setNameAndValue("LastBala", "" + tLastBala);
        tTransferData.setNameAndValue("LastBalaGurat", "" + tLastBalaGurat);
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tLMRiskFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        if (tLMRiskFeeDB.mErrors.needDealError())
        {
            System.out.println(tLMRiskFeeDB.mErrors.getErrContent());
            mErrors.addOneError("查询管理费时出现异常");
            return 0;
        }

        tLMRiskFeeSet.add(getLMRiskFeeSetB(cLCInsureAccClassSchema));

        String tEndDate = (String) tTransferData.getValueByName("EndDate");
        if (tEndDate == null)
        {
            mErrors.addOneError("请传入截至日期");
            return 0;
        }
        
        /**add by lzy 20151208处理税优未满期风险保费*/
        String str="select 1 from lmriskapp where riskcode='" +cLCInsureAccClassSchema.getRiskCode()+ "' and taxoptimal='Y'";
        String sy=new ExeSQL().getOneValue(str);
        if(null != sy && !"".equals(sy)){
        	LMRiskFeeDB tmLMRiskFeeDB = new LMRiskFeeDB();
        	tmLMRiskFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        	tmLMRiskFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        	tmLMRiskFeeDB.setFeeCode("T00001");
        	tmLMRiskFeeDB.setFeeKind("03");
        	tmLMRiskFeeDB.setFeeTakePlace("07");
        	tmLMRiskFeeDB.setFeeItemType("05");
        	tLMRiskFeeSet.add(tmLMRiskFeeDB.query());
        }

        LCInsureAccClassFeeDB tAccClassFeeDB = new LCInsureAccClassFeeDB();
        tAccClassFeeDB.setPolNo(cLCInsureAccClassSchema.getPolNo());
        tAccClassFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tAccClassFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tAccClassFeeDB.setOtherNo(cLCInsureAccClassSchema.getPolNo());
        LCInsureAccClassFeeSet set = tAccClassFeeDB.query();
        if (set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户管理费分类信息");
            return 0;
        }

        MMap tMMap = new MMap();

        //计算管理费
        for (int i = 1; i <= tLMRiskFeeSet.size(); i++)
        {
            Calculator tCalculator = new Calculator();
            tCalculator.setCalCode(tLMRiskFeeSet.get(i).getFeeCalCode());
            tCalculator.addBasicFactor("ContNo",
                                       cLCInsureAccClassSchema.getContNo());
            tCalculator.addBasicFactor("PolNo",
                                       cLCInsureAccClassSchema.getPolNo());
            tCalculator.addBasicFactor("InsuredNo",
                                       cLCInsureAccClassSchema.getInsuredNo());
            tCalculator.addBasicFactor("InsuAccNo",
                                       cLCInsureAccClassSchema.getInsuAccNo());
            tCalculator.addBasicFactor("PayPlanCode",
                                       cLCInsureAccClassSchema.getPayPlanCode());

//            LCPolDB tLCPolDB = new LCPolDB();
//            tLCPolDB.setPolNo(cLCInsureAccClassSchema.getPolNo());
//            tLCPolDB.getInfo();
            //新增变量Prem，支持常无忧万能的持续奖励计算。 added by huxl @20080616
            tCalculator.addBasicFactor("Prem", String.valueOf(mLCPolSchema.getPrem()));
            //新增变量Sex，保证计算风险保费的时候能够兼容保全的重算。 added by huxl @20080619
            tCalculator.addBasicFactor("Sex", mLCPolSchema.getInsuredSex());
            //新增变量Amnt，保证计算各险万能风险保费。 added by huxl @20080619
            tCalculator.addBasicFactor("Amnt", String.valueOf(mLCPolSchema.getAmnt()));
            
            tCalculator.addBasicFactor("CValiDate", String.valueOf(mLCPolSchema.getCValiDate()));
            
            //20120130 新增变量 持续奖金计算终止日期
            tCalculator.addBasicFactor("CalEndDate", tEndDate);
//          20101221 zhanggm 新增变量，附加险保额SubAmnt,计算附加重疾风险保费；如果存在风险加费比例，传入参数RiskRate
            String subRiskCode = ""; 
            //subRiskCode ,附加险险种代码，存储在附加险月结风险保费trace的PayPlanCode字段
			if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
				subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
				LCPolDB subLCPolDB = new LCPolDB();
				subLCPolDB.setContNo(mLCPolSchema.getContNo());
				subLCPolDB.setMainPolNo(mLCPolSchema.getPolNo());
				subLCPolDB.setRiskCode(subRiskCode);
				LCPolSet subLCPolSet = subLCPolDB.query();
				if(subLCPolSet==null || subLCPolSet.size()==0)
				{
					continue;
				}
				else
				{
					LCPolSchema subLCPolSchema = subLCPolSet.get(1).getSchema();
					String tSubAmnt = String.valueOf(subLCPolSchema.getAmnt());
					tCalculator.addBasicFactor("SubAmnt", tSubAmnt);
					
					String sql = "select Rate from LCPrem where PolNo = '" + subLCPolSchema.getPolNo() 
						+ "' and PayPlanCode like '00000%' order by PayPlanCode desc " ;
					String tRiskRate = new ExeSQL().getOneValue(sql);
					if(tRiskRate==null || tRiskRate.equals(""))
					{
						tCalculator.addBasicFactor("RiskRate", "0");
					}
					else
					{
						tCalculator.addBasicFactor("RiskRate", tRiskRate);
					}
				}
			}
			
			//健康一生保险系列计算风险保费，需传入风险保额riskAmnt,
			//是否为健康一生险种 add by fengzhihang			
			String isJKYSSql = "select 1 from ldcode where codetype = 'jkys' and code = '" +cLCInsureAccClassSchema.getRiskCode() + "'";
	        String jkysResult = new ExeSQL().getOneValue(isJKYSSql);
			if(jkysResult != null && jkysResult.equals("1")){
					String sql1 = " select a.birthday,b.cvalidate from lcinsured a,lcpol b where a.contno=b.contno and  a.contno='" + cLCInsureAccClassSchema.getContNo()
					+ "' and a.insuredno='"+cLCInsureAccClassSchema.getInsuredNo()+ "' and b.riskcode ='"+cLCInsureAccClassSchema.getRiskCode()+ "' ";
//				String tBirthday = new ExeSQL().getOneValue(sql1);
				SSRS mSSRS1 = new ExeSQL().execSQL(sql1);
				if(mSSRS1.MaxRow == 0){
					mErrors.addOneError("获取被保人生日信息失败！");
				}
				else 
				{
					FDate tFDate= new FDate();
					String getMidDate=PubFun.calDate(mSSRS1.GetText(1, 1), 61, "Y", mSSRS1.GetText(1, 2));

					String tSql="select a.duebaladate,a.insuaccbalaafter from LCInsureAccBalance a,lcinsureaccclass b where a.polno=b.polno and  a.polno='"+cLCInsureAccClassSchema.getPolNo()+"'" 
						    	+" and a.duebaladate=b.baladate ";
					SSRS accSSRS = new ExeSQL().execSQL(tSql);
					System.out.println(tSql);
					if(accSSRS.MaxRow == 0){
						mErrors.addOneError("获取账户月结信息失败！");
		                return 0;
					}
					
					String mAccSql="select sum(money) from lcinsureacctrace where contno='"+cLCInsureAccClassSchema.getContNo()+"'   and paydate<'"+tEndDate+"'  ";
					String mBFSql="select sum(money) from lcinsureacctrace where contno='"+cLCInsureAccClassSchema.getContNo()+"' and moneytype in ('ZF','BF','LQ')  and paydate<'"+tEndDate+"'  ";
					String mGLSql="select sum(fee) from lcinsureaccfeetrace where contno='"+cLCInsureAccClassSchema.getContNo()+"' and moneytype='GL' and othertype='1'";
					//缴纳的保费和追加保费-部分领取的金额 (实收的保费，所以要算上管理费)
					double BFMoney = PubFun.setPrecision(
							Double.valueOf(new ExeSQL().getOneValue(mBFSql)).doubleValue()
									+ Double.valueOf(new ExeSQL().getOneValue(mGLSql)).doubleValue(),"0.00");
					double accMoney = PubFun.setPrecision(Double.valueOf(new ExeSQL().getOneValue(mAccSql)).doubleValue(),"0.00");//个人账户余额
					//保险金额：     61岁的保单周年日（不含）之前  
					//1)	160%×（交纳的保险费-累计已领取的“部分领取”金额）；
					//2)	个人账户价值。
					// 61岁的保单周年日（含 ）之后
					//1)	120%×（交纳的保险费-累计已领取的“部分领取”金额）；
					//2)	个人账户价值。
					//风险保额是指本合同的保险金额与个人账户价值的差额，差额为零时不收取风险保险费
					double minMoney=0;//差
					double riskAmnt=0;//风险保额
					String mStartDate=cLCInsureAccClassSchema.getBalaDate();
					if(tFDate.getDate(mStartDate).compareTo(tFDate.getDate(getMidDate))==-1){
						BFMoney=BFMoney*1.6;
						minMoney=PubFun.setPrecision(BFMoney - accMoney, "0.00");
					}
					else 
					{
						BFMoney=BFMoney*1.2;
						minMoney=PubFun.setPrecision(BFMoney - accMoney, "0.00");
					}
					if(minMoney < 0){
						riskAmnt = 0;
					}else{
						riskAmnt=Math.abs(minMoney);
					}
					tCalculator.addBasicFactor("RiskAmnt", String.valueOf(riskAmnt));
				}
	        	
	        }
	        
			//20151209税优增加未满器净风险保费的计算
			if(null != sy && !"".equals(sy)){
				String strSQL="select a.payintv,a.insuredappage,b.totaldiscountfactor from lcpol a,lccontsub b where a.prtno=b.prtno and a.polno='"+ cLCInsureAccClassSchema.getPolNo() +"' ";
				SSRS tSSRS = new ExeSQL().execSQL(strSQL);
				if(tSSRS.MaxRow == 0){
					mErrors.addOneError("获取保单详细信息失败！");
	                return 0;
				}
				tCalculator.addBasicFactor("PayIntv", tSSRS.GetText(1, 1));
				tCalculator.addBasicFactor("AppAge", tSSRS.GetText(1, 2));
				//增加折扣因子
				if(null != tSSRS.GetText(1, 3) && !"".equals(tSSRS.GetText(1, 3))){
					tCalculator.addBasicFactor("Totaldiscountfactor", tSSRS.GetText(1, 3));
				}else
				{
					tCalculator.addBasicFactor("Totaldiscountfactor", "1");
				}
        	}

            setCalculatorFactor(tTransferData, tCalculator);

            AccountManage tAccountManage = new AccountManage(mLCPolSchema);
            String tInsuredAppAge = tAccountManage
                                    .getInsuredAppAge(cLCInsureAccClassSchema.getBalaDate(),
                    cLCInsureAccClassSchema);
            if (tInsuredAppAge == null)
            {
                mErrors.copyAllErrors(tAccountManage.mErrors);
                return 0;
            }
            tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);

            fee = Math.abs(PubFun.setPrecision(
                    Double.parseDouble(tCalculator.calculate()), "0.00"));
            if (tCalculator.mErrors.needDealError())
            {
                System.out.println(tCalculator.mErrors.getErrContent());
                mErrors.addOneError("计算管理费出错");
                return 0;
            }
            //20151209税优未满期净风险保费处理（已发生保险金给付的，未满期净风险保险费为零）
			if(null != sy && !"".equals(sy) && tLMRiskFeeSet.get(i).getFeeCode().equals("T00001")){
				GCheckClaimBL check = new GCheckClaimBL();
				if(check.checkClaimed(cLCInsureAccClassSchema.getPolNo())){
					fee=0;
				}else{
					fee= -Math.abs(fee);//这里是补费不是扣费。
				}
			}
            sumfee+= fee;

            
        }

        

        return PubFun.setPrecision(sumfee, "0.00");
    
    }
    
    
    public double getBalaAccInterest(LCInsureAccClassSchema tAccClassSchema,LCPolSchema mLCPolSchema)
    {
    	LCInsureAccBalanceDB tLCInsureAccBalanceDB = new LCInsureAccBalanceDB();
        tLCInsureAccBalanceDB.setPolNo(tAccClassSchema.getPolNo());
        tLCInsureAccBalanceDB.setInsuAccNo(tAccClassSchema.getInsuAccNo());
        tLCInsureAccBalanceDB.setDueBalaDate(tAccClassSchema.
                                             getBalaDate());
        LCInsureAccBalanceSet tLCInsureAccBalanceSet
                = tLCInsureAccBalanceDB.query();
        if (tLCInsureAccBalanceSet.size() == 0)
        {
            mErrors.addOneError("没有查询到上一次结算信息");
            return 0;
        }
    	TransferData tTransferData =new TransferData();
    	tTransferData.setNameAndValue("StartDate", tAccClassSchema.getBalaDate());
        tTransferData.setNameAndValue("EndDate", mEdorValiDate);
        tTransferData.setNameAndValue("RiskCode",
        		mLCPolSchema.getRiskCode());
        tTransferData.setNameAndValue("BalaType", "1"); //1：账户价值结算，2：保障价值计算
        tTransferData.setNameAndValue("RateType", "2"); //1：取公布利率，2：取保证利率
        tTransferData.setNameAndValue("PolYear", String.valueOf(mPolYear));
        tTransferData.setNameAndValue("PolMonth", String.valueOf(mPolMonth));
        double tLastBala = tLCInsureAccBalanceSet.get(1).getInsuAccBalaAfter();
        double tLastBalaGurat = tLCInsureAccBalanceSet.get(1).
                                getInsuAccBalaGurat();
        tLastBalaGurat = (mPolYear != 1 && mPolMonth == 1 ? tLastBala :
                          tLastBalaGurat); //计算第一保单年度的保证收益时已上一年度末账户价值为基础
        tTransferData.setNameAndValue("LastBala", "" + tLastBala);
        tTransferData.setNameAndValue("LastBalaGurat", "" + tLastBalaGurat);
        
        String tStartDate = (String)tTransferData.getValueByName("StartDate");
        String tEndDate = (String)tTransferData.getValueByName("EndDate");
        String tBalaType = (String)tTransferData.getValueByName("BalaType");
        String tLastBalaStr = (String)tTransferData.getValueByName("LastBala");
        String tLastBalaGuratStr = (String)tTransferData.getValueByName("LastBalaGurat");

        String oldMoneyStr = "1".equals(tBalaType) ? tLastBalaStr : tLastBalaGuratStr;
        if(oldMoneyStr == null || oldMoneyStr.equals(""))
        {
            mErrors.addOneError("请传入上一次结算金额");
            return 0.00;
        }
        double oldMoney = Double.parseDouble(oldMoneyStr);

        //计算LastBala产生的计算金额
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setRiskCode((String)tTransferData.getValueByName("RiskCode"));
        tLMCalModeDB.setType("J");
        LMCalModeSet set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return 0.00;
        }

        Calculator tCalculator = createCalculator(tTransferData, tAccClassSchema,mLCPolSchema);
        if(tCalculator == null)
        {
            return 0.00;
        }

        tCalculator.setCalCode(set.get(1).getCalCode());
        String money = tCalculator.calculate();
        if(tCalculator.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tCalculator.mErrors);
            return 0.00;
        }
        double resultMoney = Double.parseDouble(money);

        //计算后轨迹本金及利息
        tLMCalModeDB.setType("K");
        set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return 0.00;
        }
        tCalculator.setCalCode(set.get(1).getCalCode());

        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
        String sql = "select * from LCInsureAccTrace "
                     + "where PayDate >= '" + tStartDate + "' "
                     + "   and paydate < '" + tEndDate + "' "
                     + "   and PolNo = '" + tAccClassSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + tAccClassSchema.getInsuAccNo() + "' "
                     + "   and PayPlanCode = '"
                     + tAccClassSchema.getPayPlanCode() + "' "
                     + "   and OtherType not in('1', '6') ";  //契约轨迹和月结轨迹不计算利息
        System.out.println("查询sql==="+sql);
        tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(sql);

        for(int i = 1; i <= tLCInsureAccTraceSet.size(); i++)
        {
            tCalculator = createCalculator(tTransferData, tAccClassSchema,mLCPolSchema);
            tCalculator.setCalCode(set.get(1).getCalCode());
            tCalculator.addBasicFactor("TraceStartDate",
                                       tLCInsureAccTraceSet.get(i).getPayDate()); //轨迹计算区间为产生轨迹日期到EndDate
            tCalculator.addBasicFactor("TraceMoney",
                                       "" + tLCInsureAccTraceSet.get(i).getMoney());

            oldMoney += tLCInsureAccTraceSet.get(i).getMoney();
            money = tCalculator.calculate();
            if(tCalculator.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tCalculator.mErrors);
                return 0.00;
            }
            resultMoney += Double.parseDouble(money);
        }
        resultMoney = PubFun.setPrecision(resultMoney, "0.00");
        double tLX = resultMoney - oldMoney;  //收益

        System.out.println("上月价值及本周期轨迹计算后总价值(未扣除相关费用):" + resultMoney);

        

        return tLX;
    }
    
    public double getBalaAccInterestU(LCInsureAccClassSchema tAccClassSchema,LCPolSchema mLCPolSchema)
    {
    	LCInsureAccBalanceDB tLCInsureAccBalanceDB = new LCInsureAccBalanceDB();
        tLCInsureAccBalanceDB.setPolNo(tAccClassSchema.getPolNo());
        tLCInsureAccBalanceDB.setInsuAccNo(tAccClassSchema.getInsuAccNo());
        tLCInsureAccBalanceDB.setDueBalaDate(tAccClassSchema.
                                             getBalaDate());
        LCInsureAccBalanceSet tLCInsureAccBalanceSet
                = tLCInsureAccBalanceDB.query();
        if (tLCInsureAccBalanceSet.size() == 0)
        {
            mErrors.addOneError("没有查询到上一次结算信息");
            return 0;
        }
    	TransferData tTransferData =new TransferData();
    	tTransferData.setNameAndValue("StartDate", tAccClassSchema.getBalaDate());
        tTransferData.setNameAndValue("EndDate", mEdorValiDate);
        tTransferData.setNameAndValue("RiskCode",
        		mLCPolSchema.getRiskCode());
        tTransferData.setNameAndValue("BalaType", "1"); //1：账户价值结算，2：保障价值计算
        tTransferData.setNameAndValue("RateType", "3"); //1：取公布利率，2：取保证利率,3:取最后一次公布利率
        tTransferData.setNameAndValue("PolYear", String.valueOf(mPolYear));
        tTransferData.setNameAndValue("PolMonth", String.valueOf(mPolMonth));
        double tLastBala = tLCInsureAccBalanceSet.get(1).getInsuAccBalaAfter();
        double tLastBalaGurat = tLCInsureAccBalanceSet.get(1).
                                getInsuAccBalaGurat();
        tLastBalaGurat = (mPolYear != 1 && mPolMonth == 1 ? tLastBala :
                          tLastBalaGurat); //计算第一保单年度的保证收益时已上一年度末账户价值为基础
        tTransferData.setNameAndValue("LastBala", "" + tLastBala);
        tTransferData.setNameAndValue("LastBalaGurat", "" + tLastBalaGurat);
        
        String tStartDate = (String)tTransferData.getValueByName("StartDate");
        String tEndDate = (String)tTransferData.getValueByName("EndDate");
        String tBalaType = (String)tTransferData.getValueByName("BalaType");
        String tLastBalaStr = (String)tTransferData.getValueByName("LastBala");
        String tLastBalaGuratStr = (String)tTransferData.getValueByName("LastBalaGurat");

        String oldMoneyStr = "1".equals(tBalaType) ? tLastBalaStr : tLastBalaGuratStr;
        if(oldMoneyStr == null || oldMoneyStr.equals(""))
        {
            mErrors.addOneError("请传入上一次结算金额");
            return 0.00;
        }
        double oldMoney = Double.parseDouble(oldMoneyStr);

        //计算LastBala产生的计算金额
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setRiskCode((String)tTransferData.getValueByName("RiskCode"));
        tLMCalModeDB.setType("N");
        LMCalModeSet set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return 0.00;
        }

        Calculator tCalculator = createCalculator(tTransferData, tAccClassSchema,mLCPolSchema);
        if(tCalculator == null)
        {
            return 0.00;
        }

        tCalculator.setCalCode(set.get(1).getCalCode());
        String money = tCalculator.calculate();
        if(tCalculator.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tCalculator.mErrors);
            return 0.00;
        }
        double resultMoney = Double.parseDouble(money);

        //计算后轨迹本金及利息
        tLMCalModeDB.setType("R");
        set = tLMCalModeDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到账户收益算法");
            return 0.00;
        }
        tCalculator.setCalCode(set.get(1).getCalCode());

        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
        String sql = "select * from LCInsureAccTrace "
                     + "where PayDate >= '" + tStartDate + "' "
                     + "   and paydate < '" + tEndDate + "' "
                     + "   and PolNo = '" + tAccClassSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + tAccClassSchema.getInsuAccNo() + "' "
                     + "   and PayPlanCode = '"
                     + tAccClassSchema.getPayPlanCode() + "' "
                     + "   and OtherType not in('1', '6') ";  //契约轨迹和月结轨迹不计算利息
        System.out.println("查询sql==="+sql);
        tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(sql);

        for(int i = 1; i <= tLCInsureAccTraceSet.size(); i++)
        {
            tCalculator = createCalculator(tTransferData, tAccClassSchema,mLCPolSchema);
            tCalculator.setCalCode(set.get(1).getCalCode());
            tCalculator.addBasicFactor("TraceStartDate",
                                       tLCInsureAccTraceSet.get(i).getPayDate()); //轨迹计算区间为产生轨迹日期到EndDate
            tCalculator.addBasicFactor("TraceMoney",
                                       "" + tLCInsureAccTraceSet.get(i).getMoney());

            oldMoney += tLCInsureAccTraceSet.get(i).getMoney();
            money = tCalculator.calculate();
            if(tCalculator.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tCalculator.mErrors);
                return 0.00;
            }
            resultMoney += Double.parseDouble(money);
        }
        resultMoney = PubFun.setPrecision(resultMoney, "0.00");
        double tLX = resultMoney - oldMoney;  //收益

        System.out.println("上月价值及本周期轨迹计算后总价值(未扣除相关费用):" + resultMoney);

        

        return tLX;
    }
    
    private Calculator createCalculator(TransferData cTransferData,
            LCInsureAccClassSchema cAccClassSchema,LCPolSchema mLCPolSchema)
	{
		Calculator tCalculator = new Calculator();
		
		Vector tVector = cTransferData.getValueNames();
		for(int i = 0; i < tVector.size(); i++)
		{
		Object tNameObject = null;
		Object tValueObject = null;
		try
		{
		tNameObject = tVector.get(i);
		tValueObject = cTransferData.getValueByName(tNameObject);
		tCalculator.addBasicFactor((String)tNameObject, (String) tValueObject);
		}
		catch(Exception ex)
		{
		//对象无法转换为字符串，不需要做处理
		}
		}
		
		tCalculator.addBasicFactor("ContNo", cAccClassSchema.getContNo());
		tCalculator.addBasicFactor("PolNo", cAccClassSchema.getPolNo());
		tCalculator.addBasicFactor("InsuredNo", cAccClassSchema.getInsuredNo());
		tCalculator.addBasicFactor("InsuAccNo", cAccClassSchema.getInsuAccNo());
		tCalculator.addBasicFactor("PayPlanCode", cAccClassSchema.getPayPlanCode());
		tCalculator.addBasicFactor("RiskCode", cAccClassSchema.getRiskCode());
		tCalculator.addBasicFactor("InsuAccNo", cAccClassSchema.getInsuAccNo());
		
		String tStartDate = (String)cTransferData.getValueByName("StartDate");
		if(tStartDate == null)
		{
		mErrors.addOneError("请传入结算起始日期");
		return null;
		}
		
		String tInsuredAppAge = getInsuredAppAge(tStartDate, cAccClassSchema,mLCPolSchema);
		if(tInsuredAppAge == null)
		{
		return null;
		}
		tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);
		
		return tCalculator;
	}
    
    public String getInsuredAppAge(String cStartDate,
            LCInsureAccClassSchema cAccClassSchema,LCPolSchema mLCPolSchema)
    {
		LCPolDB tLCPolDB = new LCPolDB();
		
		String tPolNo = mLCPolSchema.getPolNo();
		if(!"".equals(tPolNo) && tPolNo != null)
		{
		tLCPolDB = mLCPolSchema.getDB();
		}
		else
		{
		tLCPolDB.setPolNo(cAccClassSchema.getPolNo());
		if(!tLCPolDB.getInfo())
		{
		mErrors.addOneError("没有查询得到被保人险种信息："
		             + cAccClassSchema.getInsuredNo());
		return null;
		}
		}
		
		int tAge = PubFun.getInsuredAppAge(cStartDate,
		                    tLCPolDB.getInsuredBirthday());
		if(tAge < 0)
		{
		mErrors.addOneError("被保人投保年龄小于0："
		         + cAccClassSchema.getInsuredNo());
		return null;
		}
		
		return "" + tAge;
    }
    
    private void setCalculatorFactor(TransferData tTransferData,
            Calculator tCalculator)
    {
		Vector tVector = tTransferData.getValueNames();
		for (int i = 0; i < tVector.size(); i++)
		{
		Object tNameObject = null;
		Object tValueObject = null;
		try
		{
		tNameObject = tVector.get(i);
		tValueObject = tTransferData.getValueByName(tNameObject);
		tCalculator.addBasicFactor((String) tNameObject,
		                  (String) tValueObject);
		} catch (Exception ex)
		{
		//对象无法转换为字符串，不需要做处理
		}
		}
    }
    
    /**
     * 计算金生无忧保单可领取且未领取的满期金（老年护理金、健康维护金）
     * @return
     */
    private LJSGetEndorseSet calBenefitGet() {
    	LJSGetEndorseSet getLJSGetEndorseSet=new LJSGetEndorseSet();
		String contNo = mLPEdorItemSchema.getContNo();
		String edorNo = mLPEdorItemSchema.getEdorNo();
		mEdorValiDate = mLPEdorItemSchema.getEdorValiDate();
		
		ExeSQL mExeSQL=new ExeSQL();
		// 通过LPGET表计算保单未领取的满期金及利息
		String sql = "select distinct insuredno from lcget a,lmdutygetalive b "
				+ " where a.getdutycode = b.getdutycode and contno ='"+ contNo+"'"
				+ " and a.polno='"+mLPEdorItemSchema.getPolNo()+"' "
				+ " and a.gettodate<='"+ mEdorValiDate+ "'"
				+ " and ((a.getdutykind ='0' and (a.gettodate<=a.getenddate or a.getenddate is null)"
				+ " and not exists (select 1 from ljsgetdraw where contno=a.contno and getdutycode=a.getdutycode and insuredno=a.insuredno)"
				+ ") or( a.getdutykind !='0' and"
				+ " ((a.gettodate<a.getenddate) or ((select max(curgettodate) from ljsgetdraw where contno=a.contno and getdutycode=a.getdutycode and insuredno=a.insuredno)<=a.getenddate))))"
				+ " with ur";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) { 
			String insuredNo = tSSRS.GetText(i, 1);
			LCGetSet tLCGetSet = getLCGetNeedGet(edorNo, contNo, insuredNo,true);
			
			String strPol="select * from lcpol where contno='"+contNo+"' and polno='"+mLPEdorItemSchema.getPolNo()+"' and insuredno='"+insuredNo+"'";
			LCPolSet getLCPolSet=new LCPolDB().executeQuery(strPol);
			if(getLCPolSet.size()==0){
				return null;
			}
			LCPolSchema getLCPolSchema=getLCPolSet.get(1);
			LPPolSchema getLPPolSchema=new  LPPolSchema();
			new Reflections().transFields(getLPPolSchema, getLCPolSchema);
			double sumGetMoney = 0; // 总领取金额
			double sumLX = 0; // 总领取金额
			while (tLCGetSet.size() > 0) {
				for (int j = 1; j <= tLCGetSet.size(); j++) {
					// 获取合同下的每条给付的明细信息
					LCGetSchema tLCGetSchema = tLCGetSet.get(j).getSchema();

					String birthdaySQL = "select birthday from lcinsured where contno ='"
							+ contNo+ "'"+" and insuredno='"+ insuredNo+ "' ";
					String mBirthday = mExeSQL.getOneValue(birthdaySQL);
					String tAppntAge = String.valueOf(PubFun.calInterval(
							mBirthday, tLCGetSchema.getGettoDate(), "Y"));
					Calculator tCalculator = new Calculator();
					tCalculator.addBasicFactor("AppntAge", tAppntAge);
					tCalculator.addBasicFactor("Age", tAppntAge);
					LMDutyGetAliveDB tLMDutyGetaLiveDB = new LMDutyGetAliveDB();

					tLMDutyGetaLiveDB.setGetDutyCode(tLCGetSchema
							.getGetDutyCode());
					LMDutyGetAliveSet tLMDutyGetAliveSet = tLMDutyGetaLiveDB
							.query();
					LMDutyGetAliveSchema tLMDutyGetAliveSchema = tLMDutyGetAliveSet
							.get(1);
					String pCalCode = "";
					if (tLMDutyGetAliveSchema.getCalCode() != null
							&& !"".equals(tLMDutyGetAliveSchema.getCalCode())) {
						pCalCode = tLMDutyGetAliveSchema.getCalCode();
					}
					if (tLMDutyGetAliveSchema.getCnterCalCode() != null
							&& !"".equals(tLMDutyGetAliveSchema
									.getCnterCalCode())) {
						pCalCode = tLMDutyGetAliveSchema.getCnterCalCode();
					}
					if (tLMDutyGetAliveSchema.getOthCalCode() != null
							&& !""
									.equals(tLMDutyGetAliveSchema
											.getOthCalCode())) {
						pCalCode = tLMDutyGetAliveSchema.getOthCalCode();
					}
					tCalculator.setCalCode(pCalCode);

					// 责任保额
					String strAmnt = "select sum(amnt) from lcduty where polno='"
							+ tLCGetSchema.getPolNo()
							+ "' and dutycode='"
							+ tLCGetSchema.getDutyCode() + "'";
					String amnt = mExeSQL.getOneValue(strAmnt);

					if (tLMDutyGetAliveSchema.getGetDutyKind().equals("12")) {
						String kindsql = " select d.* From lmdutygetclm a, lmriskduty b ,lmdutygetrela c ,llclaimdetail d "
								+ " where a.getdutycode = c.getdutycode and b.dutycode = c.dutycode "
								+ " and d.getdutycode = c.getdutycode "
								+ " and d.polno ='"
								+ tLCGetSchema.getPolNo()
								+ "' " + " and afterget ='003' ";
						SSRS kindSSRS = mExeSQL.execSQL(kindsql);
						if (kindSSRS.getMaxRow() > 0) {
							System.out.println("责任已经理赔，不能做满期给付。");
							continue;
						}
					}
					// 判断主险是否出险,满期给付责任在主险。
					String polSQL = " select subriskflag from lcpol a,lmriskapp b where  a.riskcode = b.riskcode and a.polno ='"+tLCGetSchema.getPolNo()+"'";
					String PolAttribute=mExeSQL.getOneValue(polSQL);
					if ("M".equals(PolAttribute)
							&& tLMDutyGetAliveSchema.getDiscntFlag()
									.equals("0")) {
						// 得到该险种的附加险种
						String annexPolNoSQL = "select code From ldcode1 where code1 =(select riskcode from lcpol where polno ='"
								+ tLCGetSchema.getPolNo()
								+ "')  and codetype='checkappendrisk' ";
						String annexRisk = mExeSQL.getOneValue(annexPolNoSQL);
						String annexPolNo = "select polno from lcpol where contno='"
								+ tLCGetSchema.getContNo()
								+ "' and riskcode ='" + annexRisk + "'";
						if (annexRisk.equals("") && annexRisk == "") {
							System.out.println("没有得到附加险信息。");
							continue;
						}
						// 1."如果附加的有LP就不做给付";
						String sqlcliam = "select polNo From ljagetclaim where polno ='"
								+ new ExeSQL().getOneValue(annexPolNo) + "'";
						String cliam = mExeSQL.getOneValue(sqlcliam);
						if (!cliam.equals("") && cliam != "" && cliam != "null") {
							System.out.println("附加险已经出险，不能做满期给付。");
							continue;
						}
						// 2."主险出现了并且是死亡责任的不做给付"
						String mainSQL = "select polNo From ljagetclaim where polno ='"
								+ tLCGetSchema.getPolNo()
								+ "'"
								+ " and getdutykind in('501','502','503','504') ";
						String main = mExeSQL.getOneValue(mainSQL);
						if (!main.equals("") && main != "null") {
							System.out.println("主险责任出现理赔，并且是死亡责任，不能满期给付。");
							continue;
						}
						// 3.如果满期金方式的责任出险，不做给付。
						if (tLMDutyGetAliveSchema.getGetDutyKind().equals("0")) {
							String SQL = "select polNo From ljagetclaim where polno ='"
									+ tLCGetSchema.getPolNo() + "'";
							String polSLQ = mExeSQL.getOneValue(SQL);
							if (!polSLQ.equals("") && polSLQ != "null") {
								System.out.println("满期责任已经出险，不做给付。");
								continue;
							}
						}
					}
					tCalculator.addBasicFactor("Amnt", amnt);
					tCalculator.addBasicFactor("Get", CommonBL
							.bigDoubleToCommonString(tLCGetSchema.getActuGet(),
									"0.00"));
					tCalculator
							.addBasicFactor("PolNo", tLCGetSchema.getPolNo());
					// tCalculator.addBasicFactor("Prem",mPrem);
					String tStr = tCalculator.calculate();
					double tValue = 0;
					if (tStr == null || tStr.trim().equals("")) {
						tValue = 0;
					} else {
						tValue = Double.parseDouble(tStr);
					}
					sumGetMoney += tValue;
					// 计算迟领利息
					if ("294202".equals(tLCGetSchema.getGetDutyCode())
							|| "294203".equals(tLCGetSchema.getGetDutyCode())) {
						double getMoney = tValue;
						if (getMoney > 0) {
							FDate tfdate = new FDate();
							// 过了应领取日才会有给付利息
							if (tfdate.getDate(tLCGetSchema.getGettoDate())
									.compareTo(tfdate.getDate(mEdorValiDate)) < 0) {
								double interest = 0;
								// 迟发天数
								int days = PubFun.calInterval(tLCGetSchema
										.getGettoDate(), mEdorValiDate, "D");
								// 以2.5%的年复利计息
								double rate = 2.5 / 100;

								double sumMoney = Arith.mul(getMoney, Math.pow(
										Arith.add(1, rate), Arith
												.div(days, 365)));
								interest = Arith.sub(sumMoney, getMoney);
								sumLX += interest;
								System.out.println("******保单" + contNo
										+ "，的满期金：" + getMoney + ",迟发天数：" + days
										+ ",迟发利息：" + interest);

							}
						}
					}
					// 更新给付责任数据
					if (!tLCGetSchema.getGetDutyKind().equals("0")) {
						int intv=Integer.parseInt(tLCGetSchema.getGetDutyKind());
						String newGettoDate=PubFun.calDate(tLCGetSchema.getGettoDate(), intv, "M", null);
						tLCGetSet.get(j).setGettoDate(newGettoDate); // 得到下次给付日期
					}
				}
				//重新过滤给付项
				for(int k=1; k<=tLCGetSet.size(); k++){
					LCGetSchema temp=tLCGetSet.get(k);
					//若是一次领取的满期金肯定不能再领取了，若是3年领一次的满期金则要处理所有迟领的
					if("0".equals(temp.getGetDutyKind())){
						tLCGetSet.remove(temp);
					}else{
						FDate date=new FDate();
						//更新后的领至日期已经超过保单交至日或保全生效日的不需要再处理
						System.out.println(temp.getGettoDate()+"to"+getLPPolSchema.getPaytoDate());
						if(date.getDate(temp.getGettoDate()).compareTo(date.getDate(getLPPolSchema.getPaytoDate()))>0
							|| date.getDate(temp.getGettoDate()).compareTo(date.getDate(mEdorValiDate))>0){
							tLCGetSet.remove(temp);
						}
					}
				}
			}
			sumGetMoney=PubFun.setPrecision(sumGetMoney, "0.00");
			sumLX=PubFun.setPrecision(sumLX, "0.00");
			BqCalBL getCalBL=new BqCalBL();
			GlobalInput gi=new GlobalInput();
			gi.Operator="00";
			LJSGetEndorseSchema getLS=getCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),getLPPolSchema,null,"EB",-sumGetMoney,gi );
			LJSGetEndorseSchema getLxLS=getCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),getLPPolSchema,null,"EBLX",-sumLX,gi);
			getLJSGetEndorseSet.add(getLS);
			getLJSGetEndorseSet.add(getLxLS);
		}
		return getLJSGetEndorseSet;
	}
    
    /**
     * 获得金生无忧险种的给付责任
     * @param edorNo
     * @param contNo
     * @param insuredNo
     * @param firstFlag 第一次使用查询为true
     * @return 第一次查询给付责任时要查询出老年护理金和健康维护金 , 第二次之后查询的只查 健康维护金
     */
    private LCGetSet getLCGetNeedGet(String edorNo,String contNo, String insuredNo,boolean firstFalg) {
        String tSBql = " select b.* from lcpol a,lcget b , lmdutygetalive c  where 1=1 "
                        + " and a.polno = b.polno and b.getdutycode = c.getdutycode   "
                        + " and a.contno='" + contNo + "' "
                        + " and a.appflag='1' "
                        + " and a.riskcode ='340101' "
                        + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate))  "
                        + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno ) "
                        + " and ( "+ (firstFalg==false ? "" : "(b.getdutykind ='0' and  (b.gettodate<=b.getenddate or b.getenddate is null)) or ")
                        + " ( b.getdutykind !='0' and ((b.gettodate<b.getenddate) or ((select max(Curgettodate) from ljsgetdraw where contno=b.contno and getdutycode=b.getdutycode and insuredno=b.insuredno)<=b.getenddate))))"
                        + " and b.gettodate <= '"+mEdorValiDate+"'"
                        + " and b.gettodate <= a.paytodate "//用paytodate和edorvalidate均限制上，也就是按这两个日期中较早的那个限制。
                        + " and b.insuredno='"+insuredNo+"'"
                        + " with ur ";

        LCGetSet tLCGetSet = new LCGetSet();
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetSet = tLCGetDB.executeQuery(tSBql);
        System.out.println(tSBql.toString());

        if (tLCGetSet.size() == 0) {
            System.out.println("保单号为：" + contNo + "，没有查询到可给付的满期金");
            return new LCGetSet();
        }
        return tLCGetSet;
    }
    
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();
        EdorCalZTTestBL bl = new EdorCalZTTestBL();

        bl.setEdorValiDate("2035-10-28");
        bl.setCurPayToDateLongPol("2030-10-28");
        bl.setNeedBugetResultFlag(false);
        bl.setOperator("test");

        LJSGetEndorseSchema schema = bl.budgetOnePol("21056891390");
        System.out.println(schema.getGetMoney());

        System.out.println((System.currentTimeMillis() - startTime) / 1000 + "秒");
    }
}
