package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: 保全处理财务数据</p>
 * <p>Description: 把保全数据放入财务接口</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class FinanceDataBL
{
    /** 错误处理 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mNoticeType = null;

    private String mManageCom = null;

    private String mApproveCode = null;

    private String mApproveDate = null;

    private String mAgentCode = null;

    private String mAgentGroup = null;

    private String mAgentCom = null;

    private String mAgentType = null;

    private String mAppntNo = null;

    private String mDrawerID = null;

    private String mDrawer = null;

    private String mBalanceMethodValue = null;
    
    private String mSalechnl = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public FinanceDataBL(GlobalInput gi, String edorNo, String noticeType,
            String balanceMethodValue)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mNoticeType = noticeType;
        this.mBalanceMethodValue = balanceMethodValue;
        getOtherInfo();
    }

    /**
     * 返回计算结果但不提交到数据库
     * @return MMap
     */
    public MMap getSubmitData()
    {
        if (!dealData())
        {
            return null;
        }
        return mMap;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        double sumMoney = getSumMoney();
        //帐户处理begin
        AppAcc tAppAcc = new AppAcc();
        double accMoney = 0.0;
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        if (!tLPEdorAppDB.getInfo())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLPEdorAppDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "FinanceDataBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询保全信息时出错!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }

        String otherNoType = "";
        if (tLPEdorAppDB.getOtherNoType().equals("1"))
        {
            otherNoType = BQ.NOTICETYPE_P;
        }
        if (tLPEdorAppDB.getOtherNoType().equals("2"))
        {
            otherNoType = BQ.NOTICETYPE_G;
        }

        LCAppAccTraceSchema tLCAppAccTraceSchema = tAppAcc.getLCAppAccTrace(
                tLPEdorAppDB.getOtherNo(), mEdorNo, otherNoType);
        if (tLCAppAccTraceSchema == null)
        {
            if (!tAppAcc.mErrors.getLastError().equals("查询投保人帐户轨迹表记录不存在!"))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tAppAcc.mErrors);
                return false;
            }
        }
        else if (!tLCAppAccTraceSchema.getDestSource().equals("14"))
        { //如果使用了帐户余额
            accMoney = tLCAppAccTraceSchema.getMoney();
            if (tLCAppAccTraceSchema.getAccType().equals("0"))
            { //抵扣或领取
                if (sumMoney >= Math.abs(accMoney))
                {
                    sumMoney = sumMoney + accMoney; //accMoney为负值
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "FinanceDataBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "帐户余额抵扣计算出错!";
                    System.out.println("------" + tError);
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!setLJSGetEndorse(mEdorNo, tLPEdorAppDB.getOtherNo(),
                        BQ.FEEFINATYPE_YE0,
                        accMoney, "0", otherNoType))
                { //写批改补退费表
                    return false;
                }
            }
            if (tLCAppAccTraceSchema.getAccType().equals("1"))
            { //转入帐户余额
                if (!setLJSGetEndorse(mEdorNo, tLPEdorAppDB.getOtherNo(),
                        BQ.FEEFINATYPE_YEI,
                        accMoney, "1", otherNoType))
                { //写批改补退费表
                    return false;
                }
                sumMoney = 0;
            }
            if (sumMoney <= 0)
            {
                MMap map = tAppAcc.accConfirm(tLPEdorAppDB.getOtherNo(),
                        mEdorNo,
                        otherNoType);
                if (map == null)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tAppAcc.mErrors);
                    return false;
                }
                mMap.add(map);
            }
        }

        //如果为及时结算，插入一条LPEdorEspecialDataSchema记录，edorvalue = "1"
        if (this.mBalanceMethodValue.equals("1"))
        {
            LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new
                    LPEdorEspecialDataSchema();
            tLPEdorEspecialDataSchema.setEdorAcceptNo(this.mEdorNo);
            tLPEdorEspecialDataSchema.setEdorNo(this.mEdorNo);
            tLPEdorEspecialDataSchema.setEdorType("DJ");
            tLPEdorEspecialDataSchema.setDetailType("BALTYPE");
            tLPEdorEspecialDataSchema.setEdorValue("1");
            tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
            this.mMap.put(tLPEdorEspecialDataSchema, "DELETE&INSERT");
        }
        if (sumMoney > 0)
        {
            setLJSPay(sumMoney);
            setEdorState(BQ.EDORSTATE_WAITPAY);
        }
        else if (sumMoney < 0)
        {
            setLJAGet(sumMoney);
        }
        return true;
    }

    /**
     *  往批改补退费表（应收/应付）新增数据
     * @
     * @return boolean
     */
    private boolean setLJSGetEndorse(String edorNo, String customerNo,
            String FeeFinaType, double money, String getFlag,
            String otherNoType)
    {
        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
        tLJSGetEndorseDB.setEndorsementNo(edorNo);
        LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.query();
        if (tLJSGetEndorseDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJSGetEndorseDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "FinanceDataBL";
            tError.functionName = "dealDate";
            tError.errorMessage = "投保人帐户确认处理时出错!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLJSGetEndorseSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "FinanceDataBL";
            tError.functionName = "dealDate";
            tError.errorMessage = "投保人帐户确认处理时出错!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }
        LJSGetEndorseSchema tLJSGetEndorseSchema = tLJSGetEndorseSet.get(1);
        LJSGetEndorseSchema tSaveLJSGetEndorseSchema = initLJSGetEndorse(
                tLJSGetEndorseSchema, customerNo, FeeFinaType, money, getFlag,
                otherNoType);
        if (tSaveLJSGetEndorseSchema == null)
        {
            return false;
        }
        mMap.put(tSaveLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 生成交退费记录
     * @param aLPEdorItemSchema
     * @param aLPPolSchema
     * @param aLPDutySchema
     * @param aOperationType
     * @param aFeeType
     * @param aGetMoney
     * @param aGlobalInput
     * @return LJSGetEndorseSchema
     */
    public LJSGetEndorseSchema initLJSGetEndorse(LJSGetEndorseSchema
            pLJSGetEndorseSchema,
            String customerNo, String feeFinaType, double money, String getFlag,
            String otherNoType)
    {
        try
        {
            LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();

            //生成批改交退费表
            tLJSGetEndorseSchema.setGetNoticeNo(pLJSGetEndorseSchema.
                    getGetNoticeNo()); //给付通知书号码
            tLJSGetEndorseSchema.setEndorsementNo(pLJSGetEndorseSchema.
                    getEndorsementNo());
            tLJSGetEndorseSchema.setFeeOperationType(feeFinaType);
            tLJSGetEndorseSchema.setFeeFinaType(feeFinaType); //补退费财务类型
            tLJSGetEndorseSchema.setGrpContNo(BQ.FILLDATA);
            tLJSGetEndorseSchema.setContNo(BQ.FILLDATA);
            tLJSGetEndorseSchema.setGrpPolNo(BQ.FILLDATA);
            tLJSGetEndorseSchema.setPolNo(BQ.FILLDATA);
            tLJSGetEndorseSchema.setOtherNo(pLJSGetEndorseSchema.
                    getEndorsementNo()); //其他号码置为保全批单号
            tLJSGetEndorseSchema.setOtherNoType(otherNoType); //保全给付
            tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA); //无作用，但一定要，转ljagetendorse时非空
            tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA); //无作用
            tLJSGetEndorseSchema.setAppntNo(customerNo);
//            tLJSGetEndorseSchema.setInsuredNo(aLPPolSchema.getInsuredNo());
            tLJSGetEndorseSchema.setGetDate(pLJSGetEndorseSchema.getGetDate());
            tLJSGetEndorseSchema.setGetMoney(money);
//            tLJSGetEndorseSchema.setKindCode(aLPPolSchema.getKindCode());
//            tLJSGetEndorseSchema.setRiskCode(aLPPolSchema.getRiskCode());
//            tLJSGetEndorseSchema.setRiskVersion(aLPPolSchema.getRiskVersion());
            tLJSGetEndorseSchema.setManageCom(mManageCom); //新财务接口只能用8位管理机构。 modify by fuxin 2008-7-8
//            tLJSGetEndorseSchema.setAgentCode(aLPPolSchema.getAgentCode());
//            tLJSGetEndorseSchema.setAgentCom(aLPPolSchema.getAgentCom());
//            tLJSGetEndorseSchema.setAgentGroup(aLPPolSchema.getAgentGroup());
//            tLJSGetEndorseSchema.setAgentType(aLPPolSchema.getAgentType());
//            tLJSGetEndorseSchema.setGrpName("");
//            tLJSGetEndorseSchema.setHandler("");
//            tLJSGetEndorseSchema.setPolType("");
//            tLJSGetEndorseSchema.setApproveCode("");
//            tLJSGetEndorseSchema.setApproveDate(aLPPolSchema.getApproveDate());
//            tLJSGetEndorseSchema.setApproveTime(aLPPolSchema.getApproveTime());
            tLJSGetEndorseSchema.setGetFlag(getFlag);
//            tLJSGetEndorseSchema.setSerialNo("");
            tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
            tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
            return tLJSGetEndorseSchema;
        }
        catch (Exception ex)
        {
            mErrors.addOneError(new CError("建立批改补退费信息异常！"));
            return null;
        }
    }

    /**
     * 得到填充财务表所需数据
     */
    private void getOtherInfo()
    {
        if (mNoticeType.equals(BQ.NOTICETYPE_P))
        {
            getFromLCCont();
        }
        else if (mNoticeType.equals(BQ.NOTICETYPE_G))
        {
            getFromLCGrpCont();
        }
    }

    /**
     * 得到个单合同信息
     */
    private void getFromLCCont()
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mEdorNo);
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet.size() == 0)
        {
            return;
        }
        String contNo = tLPEdorItemSet.get(1).getContNo();
        LCContSchema tLCContSchema = CommonBL.getLCCont(contNo);
        if (tLCContSchema == null)
	        {
	            LBContDB tLBContDB = new LBContDB();
	            tLBContDB.setContNo(contNo);
	            if (!tLBContDB.getInfo())
	            {
	                return ;
	            }
	            LBContSchema tLBContSchema = tLBContDB.getSchema();
	            mManageCom = tLBContSchema.getManageCom();
	            mApproveCode = tLBContSchema.getApproveCode();
	            mApproveDate = tLBContSchema.getApproveDate();
	            mAgentCode = tLBContSchema.getAgentCode();
	            mAgentGroup = tLBContSchema.getAgentGroup();
	            mAgentCom = tLBContSchema.getAgentCom();
	            mAgentType = tLBContSchema.getAgentGroup();
	            mAppntNo = tLBContSchema.getAppntNo();
	            mDrawerID = tLBContSchema.getAppntIDNo();
	            mDrawer = tLBContSchema.getAppntName();
	            mSalechnl = tLBContSchema.getSaleChnl();
	            return ;
	        }
        mManageCom = tLCContSchema.getManageCom();
        mApproveCode = tLCContSchema.getApproveCode();
        mApproveDate = tLCContSchema.getApproveDate();
        mAgentCode = tLCContSchema.getAgentCode();
        mAgentGroup = tLCContSchema.getAgentGroup();
        mAgentCom = tLCContSchema.getAgentCom();
        mAgentType = tLCContSchema.getAgentGroup();
        mAppntNo = tLCContSchema.getAppntNo();
        mDrawerID = tLCContSchema.getAppntIDNo();
        mDrawer = tLCContSchema.getAppntName();
        mSalechnl = tLCContSchema.getSaleChnl();

        LLContDealDB tLLContDealDB = new LLContDealDB();
        tLLContDealDB.setEdorNo(mEdorNo);
        if (tLLContDealDB.getInfo()) {
            LJAGetDB tLJAGetDB = new LJAGetDB();
            tLJAGetDB.setOtherNo(tLLContDealDB.getCaseNo());
            tLJAGetDB.setOtherNoType("5");
            LJAGetSet tLJAGetSet = tLJAGetDB.query();
            if (tLJAGetSet.size()>0) {
                mDrawer = tLJAGetSet.get(1).getDrawer();
            }
        }
    }

    /**
     * 得到团单合同信息
     */
    private void getFromLCGrpCont()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mEdorNo);
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            return;
        }
        String grpContNo = tLPGrpEdorItemSet.get(1).getGrpContNo();
        LCGrpContSchema tLCGrpContSchema = CommonBL.getLCGrpCont(grpContNo);
        mManageCom = tLCGrpContSchema.getManageCom();
        mApproveCode = tLCGrpContSchema.getApproveCode();
        mApproveDate = tLCGrpContSchema.getApproveDate();
        mAgentCode = tLCGrpContSchema.getAgentCode();
        mAgentGroup = tLCGrpContSchema.getAgentGroup();
        mAgentCom = tLCGrpContSchema.getAgentCom();
        mAgentType = tLCGrpContSchema.getAgentGroup();
        mAppntNo = tLCGrpContSchema.getAppntNo();
    }

    /**
     * 得到补退费金额总计
     * @return double
     */
    private double getSumMoney()
    {
        String sql = "select sum(GetMoney) " +
                "from LJSGetEndorse " +
                "where EndorsementNo = '" + mEdorNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String money = tExeSQL.getOneValue(sql);
        if (money.equals(""))
        { //如果没有记录则设money为0
            money = "0";
        }
        return Double.valueOf(money).doubleValue();
    }

    /**
     * 设置应收表
     * @param sumMoney double
     */
    private void setLJSPay(double sumMoney)
    {
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo(createPayNoticeNo());
        tLJSPaySchema.setOtherNo(mEdorNo);
        tLJSPaySchema.setOtherNoType(mNoticeType);
        tLJSPaySchema.setManageCom(mManageCom);
        tLJSPaySchema.setApproveCode(mApproveCode);
        tLJSPaySchema.setApproveDate(mApproveDate);
        tLJSPaySchema.setAgentCode(mAgentCode);
        tLJSPaySchema.setAgentGroup(mAgentGroup);
        tLJSPaySchema.setAgentCom(mAgentCom);
        tLJSPaySchema.setAgentType(mAgentType);
        tLJSPaySchema.setAppntNo(mAppntNo);
        tLJSPaySchema.setSumDuePayMoney(Math.abs(sumMoney));
        tLJSPaySchema.setRiskCode(BQ.FILLDATA);
        tLJSPaySchema.setPayDate(PubFun.getCurrentDate());
        tLJSPaySchema.setOperator(mGlobalInput.Operator);
        tLJSPaySchema.setMakeDate(PubFun.getCurrentDate());
        tLJSPaySchema.setMakeTime(PubFun.getCurrentTime());
        tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
        
        ExeSQL tExeSQL = new ExeSQL();
        String sql = null;
        if(mNoticeType.equals(BQ.NOTICETYPE_P))
        {
        	sql = "select ContNo from LPEdorItem where EdorNo = '" + mEdorNo + "' ";
        	String tContNo = tExeSQL.getOneValue(sql);
        	sql = "select LF_SaleChnl('1', '" + tContNo + "'),'1' from dual where 1=1";
        }
        else
        {
        	sql = "select GrpContNo from LPGrpEdorItem where EdorNo = '" + mEdorNo + "' ";
        	String tGrpContNo = tExeSQL.getOneValue(sql);
        	sql = "select LF_SaleChnl('2', '"+ tGrpContNo+"'),LF_MarketType('"+ tGrpContNo+"') from dual where 1=1";
        }
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sql);
        String tSaleChnl = tSSRS.GetText(1, 1);
        String tMarketType = tSSRS.GetText(1, 2);
        tLJSPaySchema.setSaleChnl(tSaleChnl);
        tLJSPaySchema.setMarketType(tMarketType);
        
        mMap.put(tLJSPaySchema, "DELETE&INSERT");
    }

    /**
     * 设置实付表，对保全来说应付表(LJSGet)没有意义，已经不用
     * @return String
     */
    private void setLJAGet(double sumMoney)
    {
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        tLJAGetSchema.setActuGetNo(createGetNoticeNo());
        tLJAGetSchema.setOtherNo(mEdorNo);
        tLJAGetSchema.setOtherNoType(mNoticeType);
        tLJAGetSchema.setManageCom(mManageCom);
        tLJAGetSchema.setApproveCode(mApproveCode);
        tLJAGetSchema.setApproveDate(mApproveDate);
        tLJAGetSchema.setAgentCode(mAgentCode);
        tLJAGetSchema.setAgentGroup(mAgentGroup);
        tLJAGetSchema.setAgentCom(mAgentCom);
        tLJAGetSchema.setAgentType(mAgentType);
        tLJAGetSchema.setAppntNo(mAppntNo);
        tLJAGetSchema.setSumGetMoney(Math.abs(sumMoney));
        tLJAGetSchema.setDrawerID(mDrawerID);
        tLJAGetSchema.setDrawer(mDrawer);
        tLJAGetSchema.setSaleChnl(mSalechnl);
        tLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
        tLJAGetSchema.setOperator(mGlobalInput.Operator);
        tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
        tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJAGetSchema, "DELETE&INSERT");
    }

    /**
     * 产生交费记录号，如果原来LJSPay表中没有数据时才产生新的号
     * @return String
     */
    private String createPayNoticeNo()
    {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(mEdorNo);
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() > 0)
        {
            return tLJSPaySet.get(1).getGetNoticeNo();
        }
        else
        {
            return PubFun1.CreateMaxNo("PAYNOTICENO", null);
        }
    }

    /**
     * 产生退费记录号，如果原来LJAGet表中没有数据时才产生新的号
     * @return String
     */
    private String createGetNoticeNo()
    {
        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setOtherNo(mEdorNo);
        LJAGetSet tLJAGetSet = tLJAGetDB.query();
        if (tLJAGetSet.size() > 0)
        {
            return tLJAGetSet.get(1).getActuGetNo();
        }
        else
        {
            return PubFun1.CreateMaxNo("GETNO", null);
        }
    }

    /**
     * 设置保全状态,"9"为待收费状态
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
        String sql;
        sql = "  update LPEdorApp "
                + "set EdorState = '" + edorState + "', "
                + "    Operator = '" + mGlobalInput.Operator + "', "
                + "    ModifyDate = '" + PubFun.getCurrentDate() + "', "
                + "    ModifyTime = '" + PubFun.getCurrentTime() + "' "
                + "where EdorAcceptNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "  update LPEdorMain "
                + "set EdorState = '" + edorState + "', "
                + "    Operator = '" + mGlobalInput.Operator + "', "
                + "    ModifyDate = '" + PubFun.getCurrentDate() + "', "
                + "    ModifyTime = '" + PubFun.getCurrentTime() + "' "
                + "where EdorAcceptNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "  update LPEdorItem "
                + "set EdorState = '" + edorState + "', "
                + "    Operator = '" + mGlobalInput.Operator + "', "
                + "    ModifyDate = '" + PubFun.getCurrentDate() + "', "
                + "    ModifyTime = '" + PubFun.getCurrentTime() + "' "
                + "where EdorAcceptNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String args[])
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "pa0001";
        gi.ComCode = "86";
        gi.ManageCom = "86";
        FinanceDataBL tFinanceDataBL = new FinanceDataBL(gi, "20060825000014",
                BQ.NOTICETYPE_G, "0");
        if (!tFinanceDataBL.submitData())
        {
            return;
        }
    }

}
