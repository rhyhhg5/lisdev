package com.sinosoft.lis.bq;

import java.text.DecimalFormat;
import java.util.Arrays;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.tb.NIDiskImport;
import com.sinosoft.lis.tb.GrpDiskImport;

/**
 * <p>Title: 增加被保人磁盘导入类</p>
 * <p>Description: 把从磁盘导入的被保人清单添加到数据库 </p>
 * <p>增加功能，处理从磁盘导入的多Sheet文档</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @rewrite by Wulg
 * @version 1.0
 */

public class AddInsuredList {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 批次号 */
    private String mBatchNo = null;

    /** 团体合同号信息 */
    private LCGrpContSchema mLCGrpContSchema = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    /** 节点名 */
    private String[] sheetName = {"InsuredInfo", "Ver", "RiskInfo"};
    /** 配置文件名 */
    private String configName = "GrpDiskImport.xml";
    
    //职业代码和职业类别校验配置
    private SSRS occSSRS = new SSRS();
    private final static String[] OSNATIVECITY =  {"GHA","SOM","TZA","TUN","UGA","ZMB","TCD","CAF","AFG","ARE","OMN","AZE","PAK","PSE","BHR","BTN","PRK","TMP","PHL","GEO","KAZ","KOR","KGZ","KHM","QAT","KWT","LAO","LBN","MDV","MYS","MNG","BGD","MMR","NPL","JPN","SAU","LKA","TJK","THA","TUR","TKM","BRN","UZB","SGP","SYR","ARM","YEM","IRQ","IRN","ISR","IND","IDN","JOR","VNM","MAC","CHN","TWN","HKG","PLW","DZA","EGY","ETH","AGO","BEN","BWA","BFA","BDI","GNQ","TGO","ERI","CPV","GMB","COG","COD","DJI","GIN","GNB","GAB","ZWE","CMR","COM","CIV","KEN","LSO","LBR","LBY","RWA","MDG","MLI","MUS","MRT","MAR","MOZ","NAM","ZAF","SSD","NER","NGA","SLE","SEN","SYC","STP","SDN","SWZ","ESH","MWI","SHN","ALB","IRL","EST","AND","AUT","BEL","ISL","CYP","BIH","POL","BGR","BLR","DNK","DEU","RUS","FRA","FIN","NLD","MNE","CZE","HRV","LVA","LTU","LIE","LUX","ROM","MLT","MKD","MDA","MCO","NOR","PRT","SWE","CHE","SRB","SVK","SVN","SMR","UKR","ESP","GRC","HUN","ITA","GBR","VAT","FRO","GIB","REU","PYF","ATF","MTQ","GLP","MYT","SPM","WLF","NCL","VGB","IOT","SJM","ARG","ATG","BRB","BOL","BRA","DMA","ECU","COL","CUB","GRD","GUY","CAN","PER","USA","MEX","SUR","LCA","TTO","VEN","URY","JAM","CHL","BHS","ABW","AIA","PRY","PAN","BMU","PRI","BLZ","DOM","GUF","CRI","GRL","HTI","ANT","HND","CYM","VIR","MSR","NIC","SLV","VCT","TCA","GTM","UMI","MNP","MHL","FLK","SGS","GUM","KNA","AUS","PNG","FJI","COK","ASM","FSM","NRU","WSM","TON","VUT","NZL","HMD","CCK","NFK","SLB","PCN","NIU","TKL","TUV","KIR","BVT","CXR","ATA"};
    private final static String[] HKNATIVECITY =  {"1","2","3"};
    /**
     * 构造函数，保全入口
     * @param GrpContNo String
     * @param gi GlobalInput
     * @param edorNo StringD:\wuligang\workspace\doc
     */
    public AddInsuredList(String GrpContNo, GlobalInput gi, String edorNo,
                          String configName, String[] sheetName) {
        this.mGlobalInput = gi;
        this.mGrpContNo = GrpContNo;
        this.mEdorNo = edorNo;
        this.configName = configName;
        this.sheetName = sheetName;
        if ((edorNo != null) && (!edorNo.equals(""))) {
            LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
            tLPGrpEdorItemDB.setEdorNo(mEdorNo);
            tLPGrpEdorItemDB.setEdorType("NI");
            tLPGrpEdorItemDB.setGrpContNo(GrpContNo);
            LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
            if (tLPGrpEdorItemSet.size() > 0) {
                mLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
            }
        }
    }

    /**
     * 构造函数，新契约入口
     * @param GrpContNo String
     * @param gi GlobalInput
     */
    public AddInsuredList(String GrpContNo, GlobalInput gi) {
        this.mGlobalInput = gi;
        this.mGrpContNo = GrpContNo;
    }

    /**
     * 添加传入的多个Sheet数据
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName) {
    	System.out.println("导入开始时间:" + PubFun.getCurrentTime());
    	boolean flag=false;
    	boolean jycardflag=false;
    	

        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

        //从磁盘导入数据
        GrpDiskImport importFile = new GrpDiskImport(path + fileName,
                path + configName,
                sheetName);
        if (!importFile.doImport()) {
            this.mErrors.copyAllErrors(importFile.mErrors);
            return false;
        }


        LCInsuredListSet tLCInsuredListSet = (LCInsuredListSet) importFile.
                                             getSchemaSet();
        LCInsuredListPolSet tLCInsuredListPolSet = (LCInsuredListPolSet)
                importFile.
                getLCInsuredListPolSet();
        //简易团，idtype为护照，则赋值于othidtype
        String sql4="select 1 from lcgrpcont where grpcontno='"+mGrpContNo+"' and cardflag='0'";
    	ExeSQL exeSql4 = new ExeSQL();
        SSRS ssrs4 = new SSRS();
        ssrs4 = exeSql4.execSQL(sql4);
        if(ssrs4.getMaxRow()!=0){
        	jycardflag=true;
        }
        
        //add by zjd 校验汇交平台投保人信息
        if(tLCInsuredListSet.size() > 0 ){
        	String time2=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        	String tsq2 = "INSERT INTO ldtimetest VALUES ('"+mGrpContNo+"',current time,'"+time2+"','证件开始','2',NULL) ";
        	new ExeSQL().execUpdateSQL(tsq2);
        	String occsql = "select OccupationCode, OccupationType from LDOccupation where 1=1 with ur";
        	occSSRS = new ExeSQL().execSQL(occsql);
        	String conttypesql = "select ContPrintType from LCGrpCont where GrpContNo ='"+mGrpContNo+"' with ur";
        	SSRS conttypessrs = new ExeSQL().execSQL(conttypesql);
        	if(conttypessrs.getMaxRow()<1){
        		mErrors.addOneError("导入错误，缺少团体保单属性！");
                return false;
        	}
        	String conttype = conttypessrs.GetText(1, 1);
        	for(int i=1;i<=tLCInsuredListSet.size();i++){
             	String name=tLCInsuredListSet.get(i).getInsuredName();
                String IDType=tLCInsuredListSet.get(i).getIDType();
             	 String IDNo=tLCInsuredListSet.get(i).getIDNo();
             	 String OthIDType=tLCInsuredListSet.get(i).getOthIDType();
            	 if(jycardflag){
            		 if("1".equals(IDType)){
                	tLCInsuredListSet.get(i).setOthIDType(IDType);
                	tLCInsuredListSet.get(i).setOthIDNo(IDNo);
            		 }else{
            			tLCInsuredListSet.get(i).setOthIDType("");
                    	tLCInsuredListSet.get(i).setOthIDNo("");
            		 }
                 }
             	 String birthday=tLCInsuredListSet.get(i).getBirthday();
             	 String sex=tLCInsuredListSet.get(i).getSex();
             	 String insuredid=tLCInsuredListSet.get(i).getInsuredID();
             	 String nativeplace = tLCInsuredListSet.get(i).getNativePlace();
             	 String nativecity = tLCInsuredListSet.get(i).getNativeCity();
             	 String position2 = tLCInsuredListSet.get(i).getPosition2();
             	 String occupationcode = tLCInsuredListSet.get(i).getOccupationCode();
             	 String occupationtype = tLCInsuredListSet.get(i).getOccupationType();
             	 String chk = chkOccupation(nativeplace,position2,occupationcode,occupationtype,conttype,nativecity,IDType,IDNo);
             	 if(!"".equals(chk)&&chk!=null){
             		mErrors.addOneError("导入被保人信息列表中，被保人:"+name+" ,"+chk+"!");
             		this.mErrors.setContent("导入被保人信息列表中，被保人:"+name+" ,"+chk+"!");
             		return false;
             	 }else{
             		 if("ML".equals(nativeplace)){
             			tLCInsuredListSet.get(i).setNativeCity("");
             		 }
             	 }
             	 if(IDNo!=null&&!"".equals(IDNo)){
             		String chkIDNo=PubFun.CheckIDNo(IDType, IDNo, birthday, sex);
                	 if(!"".equals(chkIDNo)&&chkIDNo!=null){
                		 mErrors.addOneError("导入被保人信息列表中，被保人:"+name+" ,"+chkIDNo+"!");
                		 this.mErrors.setContent("导入被保人信息列表中，被保人:"+name+" ,"+chkIDNo+"!");
                		 return false;
                	 }
             	 }
             	//校验五要素是否缺失
          		String err="";
          	if(name==null||name.equals("")){          			
          			err+=",被保险人姓名缺失";
          	 }
          	if(IDType==null||IDType.equals("")){
          		err+=",被保险人证件类型缺失";
          	 }
          	if(IDNo==null||IDNo.equals("")){
          		err+=",被保险人证件号码缺失";
          	 }
          	if(birthday==null||birthday.equals("")){
          		err+=",被保险人出生日期缺失";
          	 }
          	if(sex==null||sex.equals("")){
          		err+=",被保险人性别缺失";
          	 }
          		if(err!=null&&!"".equals(err)){
          			mErrors.addOneError("导入被保人信息列表中,被保人号:"+insuredid+","+"被保险人姓名："+name+err+"!");
          		 return false;
          		}            	 
             }
        	
        	String time3=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        	String tsq3 = "INSERT INTO ldtimetest VALUES ('"+mGrpContNo+"',current time,'"+time3+"','证件,职业','2',NULL) ";
        	new ExeSQL().execUpdateSQL(tsq3);
        	
        	//add by zqt 一带一路
        	String sql="select 1 from lcgrppol where grpcontno='"+mGrpContNo+"' and riskcode='163001'";
        	ExeSQL exeSql = new ExeSQL();
            SSRS ssrs = new SSRS();
            ssrs = exeSql.execSQL(sql);
            if(ssrs.getMaxRow()!=0){
            	flag=true;
            	for(int i=1;i<=tLCInsuredListSet.size();i++){
            		String name=tLCInsuredListSet.get(i).getInsuredName();
                	String tOccupationType=tLCInsuredListSet.get(i).getOccupationType();
                	if(tOccupationType=="6"||"6".equals(tOccupationType)){
                		 mErrors.addOneError("针对险种163001，导入被保人信息列表中，被保人:"+name+"的职业类别为六类，此类拒保，请调整导入模板后重新导入!");
                		 this.mErrors.setContent("针对险种163001，导入被保人信息列表中，被保人:"+name+" ,的职业类别为六类，此类拒保，请调整导入模板后重新导入!");
                		 return false;
                	}
                }
            }
            String sql2="select 1 from lcgrppol where grpcontno='"+mGrpContNo+"' and riskcode='163002'";
        	ExeSQL exeSql2 = new ExeSQL();
            SSRS ssrs2 = new SSRS();
            ssrs2 = exeSql2.execSQL(sql2);
            if(ssrs2.getMaxRow()!=0){
            	flag=true;
            	for(int i=1;i<=tLCInsuredListSet.size();i++){
            		String name=tLCInsuredListSet.get(i).getInsuredName();
                	String tOccupationType=tLCInsuredListSet.get(i).getOccupationType();
                	if(tOccupationType=="6"||"6".equals(tOccupationType)){
                		 mErrors.addOneError("针对险种163002，导入被保人信息列表中，被保人:"+name+"的职业类别为六类，此类拒保，请调整导入模板后重新导入!");
                		 this.mErrors.setContent("针对险种163002，导入被保人信息列表中，被保人:"+name+" ,的职业类别为六类，此类拒保，请调整导入模板后重新导入!");
                		 return false;
                	}
                }
            }
        	
            String time4=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        	String tsq4 = "INSERT INTO ldtimetest VALUES ('"+mGrpContNo+"',current time,'"+time4+"','职业,连带','3',NULL) ";
        	new ExeSQL().execUpdateSQL(tsq4);
            
            if(getContPrintType(mGrpContNo)){
            	//汇交件连带被保人校验
            	int tcount=0;
            	for(int i=1 ;i<=tLCInsuredListSet.size();i++){
            		for(int j=i+1;j<=tLCInsuredListSet.size();j++){
            			if(tLCInsuredListSet.get(i).getContNo().equals(tLCInsuredListSet.get(j).getContNo())){
            				tcount++;
            			}
            		}
            	}
            	if(tcount > 0){
            		mErrors.addOneError("导入被保人信息列表中存在连带被保人，汇交件不支持连带被保人！");
                    return false;
            	}
            	//汇交件被保人校验
                for(int i=0 ;i<tLCInsuredListSet.size();i++){
                    LCInsuredListSchema tLCInsuredListSchema=new LCInsuredListSchema();
                    tLCInsuredListSchema.setSchema(tLCInsuredListSet.get(i+1));
                    if("".equals(tLCInsuredListSchema.getSchoolNmae()) || tLCInsuredListSchema.getSchoolNmae()==null ||
                    		"".equals(tLCInsuredListSchema.getClassName()) || tLCInsuredListSchema.getClassName()==null){
                        mErrors.addOneError("导入被保人信息列表中，第"+(i+1)+"行的汇交件被保人信息不全！");
                        return false;
                    }
                    if( 
                            "".equals(tLCInsuredListSchema.getAppntName()) || tLCInsuredListSchema.getAppntName()==null ||
                            "".equals(tLCInsuredListSchema.getAppntSex()) || tLCInsuredListSchema.getAppntSex()==null || 
                            "".equals(tLCInsuredListSchema.getAppntBirthday()) || tLCInsuredListSchema.getAppntBirthday()==null ||
                            "".equals(tLCInsuredListSchema.getAppntIdType()) || tLCInsuredListSchema.getAppntIdType()==null || 
                            "".equals(tLCInsuredListSchema.getAppntIdNo()) || tLCInsuredListSchema.getAppntIdNo()==null 
                           ){
                             mErrors.addOneError("导入被保人信息列表中，第"+(i+1)+"行的汇交件投保人信息不全！");
                             return false;
                         }
                    tLCInsuredListSet.get(i+1).setPosition2("");
                    tLCInsuredListSet.get(i+1).setOccupationCode("");
//                    tLCInsuredListSchema.setPosition2("");
//                    tLCInsuredListSchema.setOccupationCode("");
                }
            }else{
                for(int i=0 ;i<tLCInsuredListSet.size();i++){
                    LCInsuredListSchema tLCInsuredListSchema=new LCInsuredListSchema();
                    tLCInsuredListSchema.setSchema(tLCInsuredListSet.get(i+1));
                    if((!"".equals(tLCInsuredListSchema.getSchoolNmae()) && tLCInsuredListSchema.getSchoolNmae()!=null) || 
                        (!"".equals(tLCInsuredListSchema.getClassName()) && tLCInsuredListSchema.getClassName()!=null) || 
                        (!"".equals(tLCInsuredListSchema.getAppntName()) && tLCInsuredListSchema.getAppntName()!=null) ||
                        (!"".equals(tLCInsuredListSchema.getAppntSex()) && tLCInsuredListSchema.getAppntSex()!=null) || 
                        (!"".equals(tLCInsuredListSchema.getAppntBirthday()) && tLCInsuredListSchema.getAppntBirthday()!=null) ||
                        (!"".equals(tLCInsuredListSchema.getAppntIdType()) && tLCInsuredListSchema.getAppntIdType()!=null) || 
                        (!"".equals(tLCInsuredListSchema.getAppntIdNo()) && tLCInsuredListSchema.getAppntIdNo()!=null) 
                      ){
                        mErrors.addOneError("导入被保人信息列表中，第"+(i+1)+"行的非汇交件不能填写生效日期之后的内容！");
                        return false;
                    }
                }
            }
            String time5=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
        	String tsq5 = "INSERT INTO ldtimetest VALUES ('"+mGrpContNo+"',current time,'"+time5+"','连带','4',NULL) ";
        	new ExeSQL().execUpdateSQL(tsq5);
        }
        this.mResult.add(tLCInsuredListSet);
        this.mResult.add(tLCInsuredListPolSet);

        //标准团多线程导入被保人
        String tSql = "select 1 from lcgrpcont where grpcontno='"+mGrpContNo+"' and cardflag is null";
        ExeSQL texeSql = new ExeSQL();
        SSRS tssrs = new SSRS();
        tssrs = texeSql.execSQL(tSql); 
        if(tssrs.getMaxRow()!=0){
	        String tsql1 = "select prtno from lcgrpcont where grpcontno='"+mGrpContNo+"'";
	 		String tPrtNo = new ExeSQL().getOneValue(tsql1);
	        TransferData tTransferData = new TransferData();
	        tTransferData.setNameAndValue("BatchNo", mBatchNo);
	        tTransferData.setNameAndValue("GrpContNo", mGrpContNo);
	        tTransferData.setNameAndValue("PrtNo", tPrtNo);
	        tTransferData.setNameAndValue("FileName", fileName);
	        tTransferData.setNameAndValue("FilePath", path);
	        tTransferData.setNameAndValue("GlobalInput", mGlobalInput);
	        tTransferData.setNameAndValue("LCInsuredListSet", tLCInsuredListSet);
	        tTransferData.setNameAndValue("LCInsuredListPolSet", tLCInsuredListPolSet);
			LCGrpInputListBL tLGrpInputListBL = new LCGrpInputListBL();
			if(!tLGrpInputListBL.dealList(tTransferData, tPrtNo)){
				mErrors.addOneError("导入被保人失败！");
				return false;
			}
        }else{
	        //存放Insert Into语句的容器
	        MMap map = new MMap();
	        if ((mEdorNo != null) && (!mEdorNo.equals(""))) {
	            deleteInsuredList(map);
	        }
	        for (int i = 1; i <= tLCInsuredListSet.size(); i++) {
	            //添加一个被保人
	            addOneInsured(map, tLCInsuredListSet.get(i), i);
	        }       
	        for (int i = 1; i <= tLCInsuredListPolSet.size(); i++) {
	            deleteInsuredListPol(map, tLCInsuredListPolSet.get(i).getInsuredID(),
	                                 tLCInsuredListPolSet.get(i).getRiskCode());
	            addOneInsuredPol(map, tLCInsuredListPolSet.get(i), i);
	        }
	
	        //提交数据到数据库
	        if (!submitData(map)) {
	            return false;
	        }
        }
        
        if(flag){
        	MMap tmap = new MMap();
        	String tsql = "select * from lcgrpcontroad where prtno in (select prtno from lcgrpcont where "
        			    + "GrpContNo='"+mGrpContNo+"')";
        	LCGrpContRoadDB tLCGrpContRoadDB=new LCGrpContRoadDB();
        	LCGrpContRoadSet tLCGrpContRoadSet = new LCGrpContRoadSet();
        	tLCGrpContRoadSet = tLCGrpContRoadDB.executeQuery(tsql);
        	LCGrpContRoadSchema tLCGrpContRoadSchemma = new LCGrpContRoadSchema();        	
        	
        	if(tLCGrpContRoadSet.size()!=1){
        		String sql = "select riskcode from lcgrppol where riskcode in('163001','163002') and GrpContNo='"+mGrpContNo+"'";
        		SSRS ssrst = new ExeSQL().execSQL(sql);
        		mErrors.addOneError(ssrst.GetText(1, 1)+"险种需添加一带一路产品要素！");
        		return false;
        	}else{
        		tLCGrpContRoadSchemma=tLCGrpContRoadSet.get(1);
        		String ttsql1 = "select sum(yeardiff(a.CValiDate,b.birthday)) from lcinsuredlist b,lcgrpcont a "
        				   	  + "where a.grpcontno=b.grpcontno and a.grpcontno='"+mGrpContNo+"'";
        		String ttsql2 = "select count(*) from lcinsuredlist where grpcontno='"+mGrpContNo+"'";
        		ExeSQL exeSql = new ExeSQL();
                SSRS ssrs1 = new SSRS();
                SSRS ssrs2 = new SSRS();
                ssrs1 = exeSql.execSQL(ttsql1);
                ssrs2 = exeSql.execSQL(ttsql2);
                if(ssrs1.getMaxRow()!=0 && ssrs2.getMaxRow()!=0){
                	float sumage=Float.valueOf(ssrs1.GetText(1, 1));
                	float count=Float.valueOf(ssrs2.GetText(1, 1));
                	System.out.println("总年龄："+sumage);
                	float avage = sumage / count;
                	DecimalFormat   fnum   =   new   DecimalFormat("##0.00");  
                	String tavage=fnum.format(avage);      
          		  	System.out.println("平均年龄："+tavage);
                	tLCGrpContRoadSchemma.setAvage(tavage);
                	tLCGrpContRoadSchemma.setScaleFactor(ssrs2.GetText(1, 1));
                	tLCGrpContRoadSchemma.setModifyDate(mCurrentDate);
                	tLCGrpContRoadSchemma.setModifyTime(mCurrentTime);
                }else{
                	mErrors.addOneError("查询保单或被保人信息失败！");
            		return false;
                }
        	}
        	tmap.put(tLCGrpContRoadSchemma,SysConst.UPDATE);
        	//提交数据到数据库
            if (!submitData(tmap)) {
                return false;
            }
        }
        System.out.println("导入结束时间:" + PubFun.getCurrentTime());
        
        return true;
    }

    /**
     * 保全增人导入。单sheet added by huxl @ 2006-12-8
     * 保全差异增人。多sheet 导入 modify by qulq 2007-7-24
     * @param path String
     * @param fileName String
     * @param edortype String
     * @return boolean
     */
    public boolean doAdd(String path, String fileName, String edorType) {
        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

        //从磁盘导入数据
        NIDiskImport importFile = new NIDiskImport(path + fileName,path + configName,
                sheetName);

        if (!importFile.doImport()) {
            this.mErrors.copyAllErrors(importFile.mErrrors);
            return false;
        }

        String localVersion = importFile.getVersion();
        String strSql = "select 1 from lccontplandutyparam where calfactor ='CalRule' and calfactorvalue ='4' "
					  + " and contplancode not in ('00','11') and grpcontno ='"+mGrpContNo+"' and riskcode not in ('162401','162501')";
        ExeSQL exeSql = new ExeSQL();
        SSRS ssrs = new SSRS();
        ssrs = exeSql.execSQL(strSql);
        if(ssrs.getMaxRow()!=0)
        {
            String tstrSql = "select SysVarValue from LDSysvar where SysVar='NiDiskImportVer'";
            SSRS tssrs = exeSql.execSQL(tstrSql);
            if(!localVersion.equals(tssrs.GetText(1, 1)))
            {
                mErrors.addOneError("该保单下有险种为差异算费，请使用差异增人模板"+tssrs.GetText(1, 1));
                return false;
            }
        }
        else
        {
        	  String tstrSql = "select SysVarValue from LDSysvar where SysVar='NiDiskImportVer2.2'";
            SSRS tssrs = exeSql.execSQL(tstrSql);
            if(!localVersion.equals(tssrs.GetText(1, 1)))
            {
                mErrors.addOneError("导入模板错误请使用普通增人模板"+tssrs.GetText(1, 1));
                return false;
            }
        }

        LCInsuredListSet tLCInsuredListSet = (LCInsuredListSet) importFile.
                                             getSchemaSet();
        LCInsuredListPolSet tLCInsuredListPolSet = (LCInsuredListPolSet)
                importFile.getLCInsuredListPolSet();
        //2015-4-9 汇交件增人投保人信息校验
        if(tLCInsuredListSet.size() > 0 ){
            if(getContPrintType(mGrpContNo)){
            	//汇交件连带被保人校验
            	int tcount=0;
            	for(int i=1 ;i<=tLCInsuredListSet.size();i++){
            		for(int j=i+1;j<=tLCInsuredListSet.size();j++){
            			if(tLCInsuredListSet.get(i).getContNo().equals(tLCInsuredListSet.get(j).getContNo())){
            				tcount++;
            			}
            		}
            	}
            	if(tcount > 0){
            		mErrors.addOneError("导入被保人信息列表中存在连带被保人，汇交件不支持连带被保人！");
                    return false;
            	}
            	//汇交件被保人校验
                for(int i=0 ;i<tLCInsuredListSet.size();i++){
                    LCInsuredListSchema tLCInsuredListSchema=new LCInsuredListSchema();
                    tLCInsuredListSchema.setSchema(tLCInsuredListSet.get(i+1));
                    if("".equals(tLCInsuredListSchema.getSchoolNmae()) || tLCInsuredListSchema.getSchoolNmae()==null ||
                    		"".equals(tLCInsuredListSchema.getClassName()) || tLCInsuredListSchema.getClassName()==null){
                        mErrors.addOneError("导入被保人信息列表中，第"+(i+1)+"行的汇交件被保人信息不全！");
                        return false;
                    }
                    if( 
                            "".equals(tLCInsuredListSchema.getAppntName()) || tLCInsuredListSchema.getAppntName()==null ||
                            "".equals(tLCInsuredListSchema.getAppntSex()) || tLCInsuredListSchema.getAppntSex()==null || 
                            "".equals(tLCInsuredListSchema.getAppntBirthday()) || tLCInsuredListSchema.getAppntBirthday()==null ||
                            "".equals(tLCInsuredListSchema.getAppntIdType()) || tLCInsuredListSchema.getAppntIdType()==null || 
                            "".equals(tLCInsuredListSchema.getAppntIdNo()) || tLCInsuredListSchema.getAppntIdNo()==null 
                           ){
                             mErrors.addOneError("导入被保人信息列表中，第"+(i+1)+"行的汇交件投保人信息不全！");
                             return false;
                         }
                }
                
            }else{
                for(int i=0 ;i<tLCInsuredListSet.size();i++){
                    LCInsuredListSchema tLCInsuredListSchema=new LCInsuredListSchema();
                    tLCInsuredListSchema.setSchema(tLCInsuredListSet.get(i+1));
                    if((!"".equals(tLCInsuredListSchema.getSchoolNmae()) && tLCInsuredListSchema.getSchoolNmae()!=null) || 
                        (!"".equals(tLCInsuredListSchema.getClassName()) && tLCInsuredListSchema.getClassName()!=null) || 
                        (!"".equals(tLCInsuredListSchema.getAppntName()) && tLCInsuredListSchema.getAppntName()!=null) ||
                        (!"".equals(tLCInsuredListSchema.getAppntSex()) && tLCInsuredListSchema.getAppntSex()!=null) || 
                        (!"".equals(tLCInsuredListSchema.getAppntBirthday()) && tLCInsuredListSchema.getAppntBirthday()!=null) ||
                        (!"".equals(tLCInsuredListSchema.getAppntIdType()) && tLCInsuredListSchema.getAppntIdType()!=null) || 
                        (!"".equals(tLCInsuredListSchema.getAppntIdNo()) && tLCInsuredListSchema.getAppntIdNo()!=null) 
                      ){
                        mErrors.addOneError("导入被保人信息列表中，第"+(i+1)+"行的非汇交件不能填写学校以及之后的内容！");
                        return false;
                    }
                }
            }
            
        }
        
        
        
        this.mResult.add(tLCInsuredListSet);
				this.mResult.add(tLCInsuredListPolSet);
        //存放Insert Into语句的容器
        MMap map = new MMap();
        if ((mEdorNo != null) && (!mEdorNo.equals(""))) {
            deleteInsuredList(map);
        }
        ExeSQL 	tExeSQL  = new ExeSQL();
     	String time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
    	String insertSQL = "INSERT INTO ldtimetest VALUES ('TTZRTIME',current time,'"+time+"','团单:"+mGrpContNo+"开始增人','1','1') ";
    	tExeSQL.execUpdateSQL(insertSQL);
        for (int i = 1; i <= tLCInsuredListSet.size(); i++) {
            //添加一个被保人
            addOneInsured(map, tLCInsuredListSet.get(i), i);
        }
        map.put("delete from LCInsuredListPol where grpcontno ='"+mGrpContNo+"'","DELETE");
        for (int i = 1; i <= tLCInsuredListPolSet.size(); i++) {
            deleteInsuredListPol(map, tLCInsuredListPolSet.get(i).getInsuredID(),
                                 tLCInsuredListPolSet.get(i).getRiskCode());
            addOneInsuredPol(map, tLCInsuredListPolSet.get(i), i);
        }
        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }
        ExeSQL 	tExeSQL1  = new ExeSQL();
     	String time1 = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
    	String insertSQL1 = "INSERT INTO ldtimetest VALUES ('TTZRTIME',current time,'"+time1+"','团单:"+mGrpContNo+"结束增人','2','1') ";
    	tExeSQL1.execUpdateSQL(insertSQL1);
        return true;
    }


    /**
     * 先删除相关的数据
     * @param map MMap
     */
    private void deleteInsuredList(MMap map) {
        String sql = "delete from LCInsuredList " +
                     "where GrpContNo = '" + mGrpContNo + "' " +
                     "and EdorNo = '" + mEdorNo + "' ";
        map.put(sql, "DELETE");
    }

    private void deleteInsuredListPol(MMap map, String InsuredID,
                                      String RiskCode) {
        String sql = "delete from LCInsuredListPol where GrpContNo = '" + mGrpContNo + "' "
                     + " and InsuredID = '" + InsuredID + "' "
                     +	" and RiskCode = '" + RiskCode + "'"
                     ;
        map.put(sql, "DELETE");
    }

    /**
     * 校验导入数据
     * @param cLCInsuredListSet LCInsuredListSet
     * @return boolean
     */
    private boolean checkData(LCInsuredListSet cLCInsuredListSet) {
        for (int i = 1; i <= cLCInsuredListSet.size(); i++) {
            LCInsuredListSchema schema = cLCInsuredListSet.get(i);
//            String occupationType = schema.getOccupationType();
//            if ((occupationType == null) || (occupationType.equals(""))) {
//                mErrors.addOneError("导入错误，缺少职业类别！");
//                return false;
//            }

            String contPlanCode = schema.getContPlanCode();
            if ((contPlanCode == null) || (contPlanCode.equals(""))) {
                mErrors.addOneError("导入错误，缺少保险计划！");
                return false;
            }
        }
        return true;
    }

    /**
     * 添加一个被保人
     * @param map MMap
     * @param aLCInsuredSchema LCInsuredSchema
     */
    private void addOneInsured(MMap map,
                               LCInsuredListSchema cLCInsuredListSchema, int i) {
        LCInsuredListSchema tLCInsuredListSchema = cLCInsuredListSchema;
        if (tLCInsuredListSchema.getContNo() == null ||
            tLCInsuredListSchema.getContNo().equals("")) {
            return;
        }
        tLCInsuredListSchema.setGrpContNo(mGrpContNo);
        tLCInsuredListSchema.setState("0"); //0为未生效，1为有效
        tLCInsuredListSchema.setBatchNo(this.mBatchNo);
        //在磁盘投保时合同号码存储合同id
        if (StrTool.cTrim(tLCInsuredListSchema.getContNo()).equals("")) {
            tLCInsuredListSchema.setContNo(String.valueOf(i));
        }
        String edorValiDate = tLCInsuredListSchema.getEdorValiDate();
        if ((edorValiDate == null) || (edorValiDate.equals(""))) {
            if (mLPGrpEdorItemSchema != null) {
                tLCInsuredListSchema.setEdorValiDate(
                        mLPGrpEdorItemSchema.getEdorValiDate());
            }
        }
        tLCInsuredListSchema.setEdorNo(mEdorNo);
        tLCInsuredListSchema.setOperator(mGlobalInput.Operator);
        tLCInsuredListSchema.setMakeDate(mCurrentDate);
        tLCInsuredListSchema.setMakeTime(mCurrentTime);
        tLCInsuredListSchema.setModifyDate(mCurrentDate);
        tLCInsuredListSchema.setModifyTime(mCurrentTime);
        String prem = tLCInsuredListSchema.getPrem();
        if ((prem != null) && (!prem.equals("")) &&
            (Double.parseDouble(prem) > 0)) {
            tLCInsuredListSchema.setPublicAcc(Double.parseDouble(prem));
        }
        //设置计算方向
        if (tLCInsuredListSchema.getPublicAcc() > 0) {
            tLCInsuredListSchema.setCalRule("4");
        }
        map.put(tLCInsuredListSchema, "DELETE&INSERT");
    }

    private void addOneInsuredPol(MMap map,
                                  LCInsuredListPolSchema
                                  cLCInsuredListPolSchema, int i) {
        LCInsuredListPolSchema tLCInsuredListPolSchema =
                cLCInsuredListPolSchema;
        if (tLCInsuredListPolSchema.getContNo() == null ||
            tLCInsuredListPolSchema.getContNo().equals("")) {
            return;
        }
        if (tLCInsuredListPolSchema.getPrem() == null ||
            tLCInsuredListPolSchema.getPrem().equals("")) {
            tLCInsuredListPolSchema.setPrem("0");
        }
        if (tLCInsuredListPolSchema.getAmnt() == null ||
            tLCInsuredListPolSchema.getAmnt().equals("")) {
            tLCInsuredListPolSchema.setAmnt("0");
        }
        String strPrem =
                String.valueOf(Arith.round(Double.parseDouble(
                        tLCInsuredListPolSchema.getPrem()), 2));
        String strAmnt =
                String.valueOf(Arith.round(Double.parseDouble(
                        tLCInsuredListPolSchema.getAmnt()), 2));

        tLCInsuredListPolSchema.setPrem(strPrem);
        tLCInsuredListPolSchema.setAmnt(strAmnt);
        tLCInsuredListPolSchema.setGrpContNo(
                mGrpContNo);
        tLCInsuredListPolSchema.setMakeDate(mCurrentDate);
        tLCInsuredListPolSchema.setMakeTime(mCurrentTime);
        tLCInsuredListPolSchema.setModifyDate(mCurrentDate);
        tLCInsuredListPolSchema.setModifyTime(mCurrentTime);
        tLCInsuredListPolSchema.setOperator(mGlobalInput.Operator);
        tLCInsuredListPolSchema.setPolNo(i + "");
        map.put(tLCInsuredListPolSchema, "DELETE&INSERT");
    }
    
    /**
     * 校验团体保单属性 如果是5--汇交件 则校验被保人信息不能为空 其他类型被保人信息必须为空
     * @param tGrpContNo
     * @return
     */
    private boolean getContPrintType(String tGrpContNo){
        String strSql = "select ContPrintType from LCGrpCont where GrpContNo ='"+tGrpContNo+"' ";
        ExeSQL exeSql = new ExeSQL();
        SSRS ssrs = new SSRS();
        ssrs = exeSql.execSQL(strSql);
        if(ssrs.getMaxRow()>0){
            if("5".equals(ssrs.GetText(1, 1))){
                return true;
            }
            
        }else{
            mErrors.addOneError("导入错误，缺少团体保单属性！");
            return false;
        }
        System.out.println("错误了！");
        return false;   
    }
    
    private String chkOccupation(String nativePlace,String position2,String occupationCode,String occupationType,String conttype,String nativecity,String idtype,String idno){
    	String res = "";
    	if(nativePlace==null || "".equals(nativePlace)){
    		res = "国籍不能为空";
    		return res;
    	}
    	if(!"HK".equals(nativePlace)&&!"ML".equals(nativePlace)&&!"OS".equals(nativePlace)){
    		res = "国籍录入错误";
    		return res;
    	}
    	if("HK".equals(nativePlace) || "OS".equals(nativePlace)){
    		if(nativecity==null || "".equals(nativecity)){
    			res = "国家/地区不能为空";
    			return res;
    		}
    		if(!Arrays.asList(OSNATIVECITY).contains(nativecity) && !Arrays.asList(HKNATIVECITY).contains(nativecity)){
    			res = "国家/地区录入错误";
    			return res;
    		}
    		if("HK".equals(nativePlace)){
        		if(!Arrays.asList(HKNATIVECITY).contains(nativecity)){
        			res = "国籍与地区不匹配";
        			return res;
        		}
        	}else if("OS".equals(nativePlace)){
        		if(!Arrays.asList(OSNATIVECITY).contains(nativecity)){
        			res = "国籍与国家不匹配";
        			return res;
        		}
        	}
    	}
    	if(!"5".equals(conttype)){
    		if(position2==null || "".equals(position2)){
    			res = "岗位不能为空";
    			return res;
    		}
    		if(occupationCode==null || "".equals(occupationCode)){
    			res = "职业代码不能为空";
    			return res;
    		}
    		boolean b = true;
    		for(int i=1;i<=occSSRS.MaxRow;i++){
    			if(occupationCode.equals(occSSRS.GetText(i, 1))&&occupationType.equals(occSSRS.GetText(i, 2))){
    				b = false;
    			}
    		}
    		if(b){
    			res = "职业代码与职业类别不匹配";
    			return res;
    		}
    	}
    	if("a".equals(idtype)||"b".equals(idtype)){
    		 if(!"HK".equals(nativePlace)){
    			res = "国籍与证件类型不匹配";
    			return res;
    		 }
    		 if("a".equals(idtype)){
 				if(!"1".equals(nativecity)&&!"2".equals(nativecity)){
 					res = "证件类型和地区不匹配";
 					return res;
 				}
 			}
 			if("b".equals(idtype)){
 				if(!"3".equals(nativecity)){
 					res = "证件类型和地区不匹配";
 					return res;	
 				}
 			}
 			if("1".equals(nativecity)){
 				if(idno.indexOf("810000")!=0){
 					res = "证件号码和地区不匹配";
 					return res;
 				}
 			}
 			if("2".equals(nativecity)){
 				if(idno.indexOf("820000")!=0){
 					res = "证件号码和地区不匹配";
 					return res;
 				}
 			}
 			if("3".equals(nativecity)){
 				if(idno.indexOf("830000")!=0){
 					res = "证件号码和地区不匹配！";
 					return res;
 				}
 			}
    	 }
    	
    	return res;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86110000";
        tGI.Operator = "endor";
        String grpContNo = "1400003904";
        String path = "Y:\\ui\\temp\\";
        String fileName = "18061117009.xls";
        AddInsuredList tAddInsuredList = new AddInsuredList(grpContNo, tGI);
//        if (!tAddInsuredList.doAdd(path, fileName)) {
//            System.out.println(tAddInsuredList.mErrors.getFirstError());
//            System.out.println("磁盘导入失败！");
//        }
    }
}
