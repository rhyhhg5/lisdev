package com.sinosoft.lis.bq;

//程序名称：GedorNIZTPrintBL.java
//程序功能：个险保单继续率统计,查询保单继续率汇总,下载清单。
//创建日期：2016-10-25 
//创建人  ：LC
//更新记录：  更新人    更新日期     更新原因/内容

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GedorNIZTPrintBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private CreateExcelList mCreateExcelList = new CreateExcelList("");

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();

	// 对比起始日期
	private String mBatchNo = "";
	private String mEdorType = "";
	private String mGrpContNo = "";
	private String mAcceptDate = "";
	private SSRS tGrpContInfoSSRS = null;
	
	public GedorNIZTPrintBL() {
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到公共信息！");
			return false;
		}
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mBatchNo = (String) mTransferData.getValueByName("BatchNo");
			
		if ("".equals(mBatchNo) || mBatchNo.equals(null)) {
			buildError("getInputData", "没有得到足够的信息:批次号不能为空！");
			return false;
		}
		System.out.println("批次号："+ mBatchNo +" ");
		
		String ETSql = "select grpcontno,edortype,acceptdate from LPNIZTInsured "
				+ " where batchno='" + mBatchNo + "'";

		ExeSQL tExeSQL = new ExeSQL();
		tGrpContInfoSSRS = tExeSQL.execSQL(ETSql);
		
		mGrpContNo = tGrpContInfoSSRS.GetText(1, 1);
		mEdorType = tGrpContInfoSSRS.GetText(1, 2);
		mAcceptDate = tGrpContInfoSSRS.GetText(1, 3);		
//		System.out.println("保全类型："+ mEdorType +" ");
		return true;
	}

	/**
	 * 传输数据的公共方法
	 */
	public CreateExcelList getsubmitData(VData cInputData, String cOperate) {

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return null;
		}
		System.out.println("==================dealdata=================");
		if (BQ.EDORTYPE_NI.equals(mEdorType)) // 1--增人
		{
			if (!getPrintNIData()) {
				return null;
			}
		} 
		else if (BQ.EDORTYPE_ZT.equals(mEdorType))// 2--减人
		{
			if (!getPrintZTData()) {
				return null;
			}
		}
		if (mCreateExcelList == null) {
			buildError("submitData", "Excel数据为空");
			return null;
		}
		return mCreateExcelList;
	}

	public static void main(String[] args) {
		GedorNIZTPrintBL tbl = new GedorNIZTPrintBL();
		GlobalInput tGlobalInput = new GlobalInput();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("BatchNo", "11"); // 批次号
		// 0-全部
		tGlobalInput.ManageCom = "86";
		tGlobalInput.Operator = "lc";
		VData tData = new VData();
		tData.add(tGlobalInput);
		tData.add(tTransferData);

		CreateExcelList tCreateExcelList = new CreateExcelList();
		tCreateExcelList = tbl.getsubmitData(tData, "1");
		if (tCreateExcelList == null) {
			System.out.println("==================保全增减人测试=================");
		} else {
			try {
				tCreateExcelList.write("c:\\contactcompare.xls");
			} catch (Exception e) {
				System.out.println("EXCEL生成失败！");
			}
		}
	}

	public VData getResult() {
		return mResult;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "PrtContContinueNewBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean getPrintNIData() {
		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "list" };
		mCreateExcelList.addSheet(sheetName);

		// 设置表头
		String[][] tTitle = {
				{ "保单号：", mGrpContNo, "", "", "", "申请批次号：", mBatchNo, "", "",
						"", "", "", "", "", "" },
				{ "申请日期：", mAcceptDate, "", "", "", "保全项目：", "增人NI", "", "",
						"", "", "", "", "", "" },
				{ "合同ID", "员工姓名", "被保人姓名", "与员工关系", "性别", "出生日期", "证件类型",
						"证件号码", "保险计划", "职业类别", "理赔金转账银行", "户名", "账号", "生效日期",
						"联系电话" } };

		// 表头的显示属性
		int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
		// 数据的显示属性
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

		int row = mCreateExcelList.setData(tTitle, displayTitle);
		if (row == -1) {
			buildError("getPrintData", "EXCEL中指定数据失败！");
			return false;
		}

		// 获得EXCEL列信息
		StringBuffer tSQL = new StringBuffer();
			tSQL.append("select contno, employeename, insuredname, relation, sex, birthday, idtype, idno, ")
				.append("contplancode, occupationtype, bankcode, accname, bankaccno, edorvalidate, phone ")
				.append("from LBInsuredlist where 1=1 ")
				.append("and grpcontno = '" + mGrpContNo + "' ")
				.append("and serialno = '" + mBatchNo + "' ")
				.append(" with ur");

		System.out.println("查询sql：" + tSQL.toString());
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "PrtContContinueNewBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			return false;
		}

		String[][] tGetData = null;
		
		tGetData = tSSRS.getAllData();

		if (tGetData == null) {
			CError tError = new CError();
			tError.moduleName = "GedorNIZTPrintBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "没有查询到需要输出的数据";
			return false;
		}

		if (mCreateExcelList.setData(tGetData, displayData) == -1) {
			buildError("getPrintData", "EXCEL中设置数据失败！");
			return false;
		}
		return true;
	}

	//减人
	private boolean getPrintZTData() {
		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "list" };
		mCreateExcelList.addSheet(sheetName);

		// 设置表头
		String[][] tTitle = {
				{ "保单号：", mGrpContNo, "", "", "", "申请批次号：", mBatchNo, "", "",
						"", "", "", "", "", "" },
				{ "申请日期：", mAcceptDate, "", "", "", "保全项目：", "减人ZT", "", "",
						"", "", "", "", "", "" },
				{ "合同ID", "员工姓名", "被保人姓名", "与员工关系", "性别", "出生日期", "证件类型",
						"证件号码", "保险计划", "职业类别", "理赔金转账银行", "户名", "账号", "生效日期",
						"联系电话" } };

		// 表头的显示属性
		int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
		// 数据的显示属性
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

		int row = mCreateExcelList.setData(tTitle, displayTitle);
		if (row == -1) {
			buildError("getPrintData", "EXCEL中指定数据失败！");
			return false;
		}

		// 获得EXCEL列信息
		StringBuffer tSQL = new StringBuffer();
			tSQL.append("select serialno, employeename, insuredname, relation, sex, birthday, idtype, idno, ")
				.append("contplancode, occupationtype, bankcode, accname, bankaccno, edorvalidate, phone ")
				.append("from LPDiskImport where 1=1 ")
				.append("and grpcontno = '" + mGrpContNo + "' ")
				.append("and edorno = '" + mBatchNo + "' ")
				.append("and edortype = 'ZT' with ur");

		System.out.println("查询sql：" + tSQL.toString());
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "PrtContContinueNewBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			return false;
		}

		String[][] tGetData = null;
		
		tGetData = tSSRS.getAllData();

		if (tGetData == null) {
			CError tError = new CError();
			tError.moduleName = "GedorNIZTPrintBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "没有查询到需要输出的数据";
			return false;
		}

		if (mCreateExcelList.setData(tGetData, displayData) == -1) {
			buildError("getPrintData", "EXCEL中设置数据失败！");
			return false;
		}
		return true;
	}
}
