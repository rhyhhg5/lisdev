package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;

/**
* <p>Title: 追加保费清单</p>
* <p>Description:追加保费清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListTYUI
{
    PrtGrpInsuredListTYBL mPrtGrpInsuredListTYBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListTYUI(GlobalInput gi, String edorNo)
    {
        mPrtGrpInsuredListTYBL = new PrtGrpInsuredListTYBL(gi, edorNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPrtGrpInsuredListTYBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return mPrtGrpInsuredListTYBL.getInputStream();
    }
}
