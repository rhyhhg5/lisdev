package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 失效及满期终止校验类</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft</p>
 * @author luomin
 * @version 1.0
 */

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;

public class DisabledManageBL
{
    private String mErrInfo = "";

    private String mRiskcode = "";
    private String mInsuredno = "";
    private String mState = "";
    private String tsql = "";
    private LCContStateSet tLCContStateSet = new LCContStateSet();
    private LCContStateDB tLCContStateDB = new LCContStateDB();
    private LCGrpContStateSet tLCGrpContStateSet = new LCGrpContStateSet();
    private LCGrpContStateDB tLCGrpContStateDB = new LCGrpContStateDB();
    private LCPolSchema tLCPolSchema = new LCPolSchema();
    private LCPolDB tLCPolDB = new LCPolDB();

    public DisabledManageBL()
    {
    }

    public String getErrInfo()
    {
        return this.mErrInfo;
    }

    public String getRiskcode()
    {
        return this.mRiskcode;
    }

    public String getInsuredno()
    {
        return this.mInsuredno;
    }

    public String getState()
    {
        return this.mState;
    }

    /*
      保单层校验方法
     contno  -保单号码
     edortype-批改类型
     conttype-保单类型- 1-个单 2-团单
     */
    public boolean dealDisabledcont(String contno, String edortype,
                                    int conttype)
    {
        System.out.println("now in dealDisabledcont()**************");
        if(!"RB".equals(edortype))
        {
            if (conttype == 1) {
                LCContDB tLCContDB = new LCContDB();
                tLCContDB.setContNo(contno);
                tLCContDB.getInfo();

                String stateFlag = tLCContDB.getStateFlag();
                this.mState = stateFlag;

                //失效
                if (BQ.STATE_FLAG_AVAILABLE.equals(stateFlag)) {
                    if (edortype.equals(BQ.EDORTYPE_WT)
                        || edortype.equals(BQ.EDORTYPE_XB)
                        || edortype.equals(BQ.EDORTYPE_NS)) {
                        return false;
                    }
                }

//            tsql = "select * from LCContState where StateType='Available' and State='1' and ContNo='" +
//                   contno + "' and PolNo='000000'";
//            tLCContStateSet = tLCContStateDB.executeQuery(tsql);
//            if(tLCContStateSet.size() > 0)
//            {
//                if(edortype.equals("AD") || edortype.equals("WT") ||
//                   edortype.equals("XB"))
//                {
//                    this.mState = "失效";
//                    return false;
//                }
//            }

                //终止
                if (BQ.STATE_FLAG_TERMINATE.equals(stateFlag)) {
                    if ( edortype.equals(BQ.EDORTYPE_CF)
                        || edortype.equals(BQ.EDORTYPE_PR)
                        || edortype.equals(BQ.EDORTYPE_WT)
                        || edortype.equals(BQ.EDORTYPE_TB)
                        || edortype.equals(BQ.EDORTYPE_XB)
                        || edortype.equals(BQ.EDORTYPE_NS)
                        || edortype.equals(BQ.EDORTYPE_FX)
                        || edortype.equals(BQ.EDORTYPE_MF)) {
                        return false;
                    }
                }

//            tsql = "select * from LCContState where StateType='Terminate' and State='1' and ContNo='" +
//                   contno + "' and PolNo='000000'";
//            tLCContStateSet = tLCContStateDB.executeQuery(tsql);
//            if(tLCContStateSet.size() > 0)
//            {
//                if(edortype.equals("AD") || edortype.equals("CF") ||
//                   edortype.equals("PR") || edortype.equals("WT") ||
//                   edortype.equals("TB") || edortype.equals("XB"))
//                {
//                    return false;
//                }
//            }
            }
            if (conttype == 2) {
                LCGrpContDB tLCGrpContDB = new LCGrpContDB();
                tLCGrpContDB.setGrpContNo(contno);
                tLCGrpContDB.getInfo();

                String stateFlag = tLCGrpContDB.getStateFlag();
                this.mState = stateFlag;

                //终止
                if (BQ.STATE_FLAG_TERMINATE.equals(stateFlag)) {
                    if (edortype.equals(BQ.EDORTYPE_AC)
                        || edortype.equals(BQ.EDORTYPE_GA)
                        || edortype.equals(BQ.EDORTYPE_FP)
//                   || edortype.equals(BQ.EDORTYPE_NI)
                        || edortype.equals(BQ.EDORTYPE_WT)
//                   || edortype.equals(BQ.EDORTYPE_ZT)
//                        || edortype.equals(BQ.EDORTYPE_WZ)
//                        || edortype.equals(BQ.EDORTYPE_WJ)
                            ) {
                        return false;
                    }
                }

//            tsql = "select * from LCGrpContState where StateType='Terminate' and State='1' and GrpContNo='" +
//                   contno + "' and GrpPolNo='000000'";
//            tLCGrpContStateSet = tLCGrpContStateDB.executeQuery(tsql);
//            if(tLCGrpContStateSet.size() > 0)
//            {
//                if(edortype.equals("AC") || edortype.equals("GA") ||
//                   edortype.equals("FP") || edortype.equals("NI") ||
//                   edortype.equals("WT") || edortype.equals("ZT") ||
//                   edortype.equals("WZ") || edortype.equals("WJ"))
//                {
//                    this.mState = "满期终止";
//                    return false;
//                }
//            }

                //失效暂停
                if (BQ.STATE_FLAG_AVAILABLE.equals(stateFlag)) {
                    //团单失效暂停状态可以做特权复效，add by fuxin 2008-7-31
                	//团单失效暂停状态可以做激活卡客户资料变更add by yangtianzheng 2011-4-19
                	//团单失效暂停状态可以做无名单实名化add by yangtianzheng 2011-8-12
                    if (edortype.equals(BQ.EDORTYPE_TF)||edortype.equals("BJ")||edortype.equals("BB")||edortype.equals("JM")||edortype.equals("WS")||edortype.equals("WD"))
                    {
                        return true;
                    }
                    return false;
                }

//            tsql =
//                "select * from LCGrpContState where StateType='Pause' and State='2' and GrpContNo='" +
//                contno + "' and GrpPolNo='000000'";
//            tLCGrpContStateSet = tLCGrpContStateDB.executeQuery(tsql);
//            if(tLCGrpContStateSet.size() > 0)
//            {
//                this.mState = "暂停";
//                return false;
//            }
            }
        }else
        {
            if(conttype==1)
            {
                return true ;
//                LBPolSet tLBPolSet = new LBPolSet();
//                LBPolDB tLBPolDB = new LBPolDB();
//                tLBPolDB.setContNo(contno);
//                tLBPolSet = tLBPolDB.query();
//                if(tLBPolSet.size()==0)
//                {
//                    return false ;
//                }
            }
        }
        return true;
    }

    /*
       险种层校验方法1
      LPPolSet1  -保全险种表集合
      edortype   -批改类型
     */

    public boolean dealDisabledpol(LPPolSet LPPolSet1, String edortype)
    {
        System.out.println("now in dealDisabledpol1");

        for(int i = 1; i <= LPPolSet1.size(); i++)
        {
            if(!checkOnePol(LPPolSet1.get(i).getPolNo(), edortype))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 本方法只处理需要将LCPol和LPPol进行对比的满期失效校验
     * @param dealtLPPolSet LPPolSet
     * @return boolean
     */
    public boolean dealDisabledPolCompred(LPPolSet dealtLPPolSet)
    {
        for(int i = 1; i <= dealtLPPolSet.size(); i++)
        {
            LPPolSchema schema = dealtLPPolSet.get(i);

            tLCPolDB.setPolNo(tLCPolSchema.getPolNo());
            tLCPolSchema = tLCPolDB.query().get(1);
            String stateFlag = tLCPolSchema.getStateFlag();
            this.mState = stateFlag;

            //失效
            if(BQ.STATE_FLAG_AVAILABLE.equals(stateFlag))
            {
                if(schema.getEdorType().equals(BQ.EDORTYPE_BF))
                {
                    if(tLCPolSchema.getAmnt() < schema.getAmnt())
                    {
                        this.mInsuredno = tLCPolSchema.getInsuredNo();
                        this.mRiskcode = tLCPolSchema.getRiskCode();
                        mErrInfo += "被保人号" + mInsuredno + "的险种"
                                   + mRiskcode + "在失效状态下不能加保";

                        return false;
                    }
                }
            }

            if(BQ.STATE_FLAG_TERMINATE.equals(stateFlag))
            {

            }
        }

        return true;
    }

    /**
     * 校验险种polNo在stateFlag状态下能否做保全项目edorType
     * @param polNo String
     * @param edorType String
     * @return boolean
     */
    private boolean checkOnePol(String polNo,
                                String edorType)
    {
        tLCPolDB.setPolNo(polNo);
        tLCPolDB.getInfo();
        tLCPolSchema = tLCPolDB.getSchema();

        String stateFlag = tLCPolSchema.getStateFlag();
        this.mState = stateFlag;
        
        String riskcode = tLCPolSchema.getRiskCode();
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(riskcode);
        LMRiskAppSchema tLMRiskAppSchema = new LMRiskAppSchema();
        tLMRiskAppSchema = tLMRiskAppDB.query().get(1);

        //失效
        if(BQ.STATE_FLAG_AVAILABLE.equals(stateFlag))
        {
            if(edorType.equals(BQ.EDORTYPE_CT))
            {
                if(tLMRiskAppSchema != null)
                {
                    String RiskPeriod2 = tLMRiskAppSchema.getRiskPeriod();
                    String tRiskType = tLMRiskAppSchema.getRiskType();
                    String tRiskProp = tLMRiskAppSchema.getRiskProp();
                    String tRiskTPHI = tLMRiskAppSchema.getTaxOptimal();
                    if(tRiskProp.equals("I")) //个险
                    {
                    	if(!RiskPeriod2.equals("L") && !tRiskType.equals("M")&&!"121501".equals(riskcode) && !"Y".equals(tRiskTPHI))//支持税优失效状态下解约
                        {
                            this.mInsuredno = tLCPolSchema.getInsuredNo();
                            this.mRiskcode = tLCPolSchema.getRiskCode();
                            return false;
                        }
                    }
                    else if(tRiskProp.equals("G")) //团
                    {
                    	if(!RiskPeriod2.equals("L"))
                        {
                            this.mInsuredno = tLCPolSchema.getInsuredNo();
                            this.mRiskcode = tLCPolSchema.getRiskCode();
                            return false;
                        }
                    }
                    
                }
            }
        }

        //终止
        if(BQ.STATE_FLAG_TERMINATE.equals(stateFlag))
        {
            if(edorType.equals(BQ.EDORTYPE_BF)
               || edorType.equals(BQ.EDORTYPE_PC)
                || edorType.equals(BQ.EDORTYPE_FX)
                || edorType.equals(BQ.EDORTYPE_MF)
                || edorType.equals(BQ.EDORTYPE_TF))
            {
                tLCPolDB.setPolNo(tLCPolSchema.getPolNo());
                tLCPolSchema = tLCPolDB.query().get(1);
                this.mInsuredno = tLCPolSchema.getInsuredNo();
                this.mRiskcode = tLCPolSchema.getRiskCode();
                return false;
            }

            if(edorType.equals(BQ.EDORTYPE_CT))
            {
            	//长期险险种,已经失效两年,由终止批处理处理为终止状态的,放开解约保全项目的校验. add by xp 2010-5-13
                if(tLMRiskAppSchema != null)
                {
                    String RiskPeriod3 = tLMRiskAppSchema.getRiskPeriod();
                    String tRiskType = tLMRiskAppSchema.getRiskType();
                    String tRiskProp = tLMRiskAppSchema.getRiskProp();
                    String tRiskTPHI = tLMRiskAppSchema.getTaxOptimal();
                    if(tRiskProp.equals("I")) //个险
                    {
                    	if(!RiskPeriod3.equals("L") && !tRiskType.equals("M") && !"121501".equals(riskcode) && !"Y".equals(tRiskTPHI))
                        {
                            this.mInsuredno = tLCPolSchema.getInsuredNo();
                            this.mRiskcode = tLCPolSchema.getRiskCode();
                            return false;
                        }
                    }
                    else if(tRiskProp.equals("G")) //团
                    {
                    	if(!RiskPeriod3.equals("L"))
                        {
                            this.mInsuredno = tLCPolSchema.getInsuredNo();
                            this.mRiskcode = tLCPolSchema.getRiskCode();
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }
    
    //20080813 zhanggm 终止缴费的险种不能做FX TF BF PC MF
    public boolean checkPolState(LPPolSet LPPolSet1,String aEdorType)
    {
    	if(aEdorType.equals(BQ.EDORTYPE_BF)
                || aEdorType.equals(BQ.EDORTYPE_PC)
                || aEdorType.equals(BQ.EDORTYPE_FX)
                || aEdorType.equals(BQ.EDORTYPE_MF)
                || aEdorType.equals(BQ.EDORTYPE_TF))
    	{
    		for(int i = 1; i <= LPPolSet1.size(); i++)
            {
    			String tPolNo = LPPolSet1.get(i).getPolNo();
    			LCPolDB tLCPolDB = new LCPolDB();
    			tLCPolDB.setPolNo(tPolNo);
    			if(tLCPolDB.getInfo())
    			{
    				tLCPolSchema = tLCPolDB.getSchema();
    				this.mInsuredno = tLCPolSchema.getInsuredNo();
                    this.mRiskcode = tLCPolSchema.getRiskCode();
                    String tPolState = tLCPolSchema.getPolState();
                    if(BQ.POLSTATE_XBEND.equals(tPolState)||BQ.POLSTATE_ZFEND.equals(tPolState))
                    {
                    	return false;
                    }
    			}
            }
    	}
        return true;
    }
    /*
     险种层校验方法2
       LCPolSet1  -个人险种表集合
       edortype   -批改类型
     */

    public boolean dealDisabledpol(LCPolSet LCPolSet1, String edortype)
    {
        System.out.println("now in dealDisabledpol2");
        System.out.println("******" + LCPolSet1.size());
        for(int i = 1; i <= LCPolSet1.size(); i++)
        {
            if(!checkOnePol(LCPolSet1.get(i).getPolNo(), edortype))
            {
                return false;
            }
        }

        return true;
    }
}
