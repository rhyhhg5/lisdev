package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 被保人清单产生类</p>
 * <p>Description:产生被保人清单 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author qiuyang
 * @version 1.0
 */

public class PrtGrpInsuredList
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private String mGrpContNo = null;

    private String mGrpName = null;

    private String mEdorNo = null;

    private String mEdorValiDate = null;

    private GlobalInput mGlobalInput = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredList(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
    }

    /**
     * 执行操作
     * @return boolean
     */
    public boolean submitData()
    {
       MMap map = getSubmitData();
       if (map == null)
       {
           return false;
       }

       if (!submit(map))
       {
           return false;
       }
       return true;
    }

    /**
     * 得到要提交的数据，不执行submit操作
     * @return MMap
     */
    public MMap getSubmitData()
    {
        if (!prepareData())
        {
            return null;
        }
        XmlExport xml = createXML();
        return insertXML(xml);
    }

    /**
     * 为生成XML准备数据
     * @return boolean
     */
    private boolean prepareData()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mEdorNo);
        tLPGrpEdorItemDB.setEdorType("NI"); //增人
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            mErrors.addOneError("未找到保全增人项目信息！");
            return false;
        }
        //得到团体合同号
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
        this.mGrpContNo = tLPGrpEdorItemSchema.getGrpContNo();
        this.mEdorValiDate = tLPGrpEdorItemSchema.getEdorValiDate();

        //得到团体客户名称
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("未查到团体保单信息！");
            return false;
        }
        this.mGrpName = tLCGrpContDB.getGrpName();
        return true;
    }

    /**
     * 得到新增被保人列表
     * @return LCInsuredListSet
     */
    private LCInsuredListSet getInsuredList()
    {
        String sql = "select * from LCInsuredList " +
                     "where GrpContNo = '" + mGrpContNo + "' " +
                     "and EdorNo = '" + mEdorNo + "' " +
                     "and ContPlanCode <> 'FM' " +
                     "order by Int(InsuredId) ";
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        return tLCInsuredListDB.executeQuery(sql);
    }

    /**
     * 根据清单数据生成XML
     * @param xmlExport XmlExport
     */
    private XmlExport createXML()
    {
    	boolean xpFlag=isXP(mGrpContNo);
        XmlExport xml = new XmlExport();
        xml.createDocument("PrtGrpInsuredList.vts", "printer");
        //学平险汇交件保单使用单独模板
        int cLength=16;
        if(xpFlag){
        	xml.createDocument("PrtGrpInsuredList_xp.vts", "printer");
        	cLength=23;
        }

        //设置批单中TextTag要显示的信息
        TextTag tag = new TextTag();
        tag.add("GrpName", mGrpName);
        tag.add("EdorNo", mEdorNo);
        xml.addTextTag(tag);

        //设置被保人清单
        ListTable listTable = new ListTable();
        listTable.setName("NI");
        LCInsuredListSet tLCInsuredListSet = getInsuredList();
        for (int i = 1; i <= tLCInsuredListSet.size(); i ++)
        {
            LCInsuredListSchema tLCInsuredListSchema = tLCInsuredListSet.get(i);
            //得到期交保费
            String sql = "select sum(prem) from LCPol " +
                    "where GrpContNo = '" + tLCInsuredListSchema.getGrpContNo() + "' " +
                    "and ContNo = '" + tLCInsuredListSchema.getContNo() + "' " +
                    "and InsuredNo = '" + tLCInsuredListSchema.getInsuredNo() + "' ";
            String prem = (new ExeSQL()).getOneValue(sql);
            String edorPremSql = "select sum(GetMoney) from LJSGetEndorse " +
                    "where EndorsementNo = '" + tLCInsuredListSchema.getEdorNo() + "' " +
                    "and FeeOperationType = 'NI' " +
                    "and GrpContNo = '" + tLCInsuredListSchema.getGrpContNo() + "' " +
                    "and InsuredNo = '" + tLCInsuredListSchema.getInsuredNo() + "' ";
            String edorPrem = (new ExeSQL()).getOneValue(edorPremSql);
            String[] column = new String[cLength];
            column[0] = StrTool.cTrim(String.valueOf(i));
            column[1] = StrTool.cTrim(tLCInsuredListSchema.getInsuredName());
            column[2] = StrTool.cTrim(tLCInsuredListSchema.getInsuredNo());
            column[3] = ChangeCodeBL.getCodeName("Sex",
                    tLCInsuredListSchema.getSex());
            column[4] = StrTool.cTrim(tLCInsuredListSchema.getBirthday());
            column[5] = ChangeCodeBL.getCodeName("IDType",
                    tLCInsuredListSchema.getIDType());
            column[6] = StrTool.cTrim(tLCInsuredListSchema.getIDNo());
            column[7] = StrTool.cTrim(tLCInsuredListSchema.getContPlanCode());
            column[9] = ChangeCodeBL.getCodeName("Bank",
                    tLCInsuredListSchema.getBankCode(), "BankCode");
            column[10] = StrTool.cTrim(tLCInsuredListSchema.getBankAccNo());
            column[11] = StrTool.cTrim(tLCInsuredListSchema.getAccName());
            String state = tLCInsuredListSchema.getState();
            if (state.equals("1"))
            {
                column[8] = String.valueOf(prem);
                column[12] = String.valueOf(edorPrem);
                column[13] = "有效";
            }
            else
            {
                column[8] = "";
                column[12] = "";
                column[13] = "无效";
            }
            String edorValiDate = tLCInsuredListSchema.getEdorValiDate();
            if (edorValiDate == null)
            {
                edorValiDate = mEdorValiDate;
            }
            column[14] = StrTool.cTrim(edorValiDate);
            column[15] = StrTool.cTrim(tLCInsuredListSchema.getOthIDNo());
            //汇交件保单显示投保人信息
            if(xpFlag){
            	column[16]=StrTool.cTrim(tLCInsuredListSchema.getSchoolNmae());
            	column[17]=StrTool.cTrim(tLCInsuredListSchema.getClassName());
            	column[18]=StrTool.cTrim(tLCInsuredListSchema.getAppntName());
            	column[19]=StrTool.cTrim(tLCInsuredListSchema.getAppntSex());
            	column[20]=StrTool.cTrim(tLCInsuredListSchema.getAppntBirthday());
            	column[21]=StrTool.cTrim(tLCInsuredListSchema.getAppntIdType());
            	column[22]=StrTool.cTrim(tLCInsuredListSchema.getAppntIdNo());
            }
            
            listTable.add(column);
        }
        xml.addListTable(listTable, new String[15]);
        xml.outputDocumentToFile("c:\\", "InsuredList");
        return xml;
    }

    /**
     * 把产生的BolbXML插入数据库
     * @param xmlExport XmlExport
     */
    private MMap insertXML(XmlExport xml)
    {
        LPEdorPrint2Schema tLPEdorPrint2Schema = new LPEdorPrint2Schema();
        tLPEdorPrint2Schema.setEdorNo(mEdorNo);
        tLPEdorPrint2Schema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorPrint2Schema.setPrtFlag("N");
        tLPEdorPrint2Schema.setPrtTimes(0);
        tLPEdorPrint2Schema.setEdorInfo(xml.getInputStream());
        tLPEdorPrint2Schema.setOperator(mGlobalInput.Operator);
        tLPEdorPrint2Schema.setMakeDate(PubFun.getCurrentDate());
        tLPEdorPrint2Schema.setMakeTime(PubFun.getCurrentTime());
        tLPEdorPrint2Schema.setModifyDate(PubFun.getCurrentDate());
        tLPEdorPrint2Schema.setModifyTime(PubFun.getCurrentTime());

        MMap map = new MMap();
        map.put(tLPEdorPrint2Schema, "BLOBINSERT");
        return map;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit(MMap map)
    {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    /**
     * 判断是否是学平险汇交件保单
     * @return
     */
    private boolean isXP(String grpContno){
	    String strSQL="select ContPrintType from LCGrpCont where GrpContNo ='"+grpContno+"' with ur";
	    String contPrintType=new ExeSQL().getOneValue(strSQL);
	    if("5".equals(contPrintType)){
	    	return true;
	    }else{
	    	return false;
	    }
    	
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "endor";
        String edorNo = "20050727000048";
        PrtGrpInsuredList tPrtGrpInsuredList = new PrtGrpInsuredList(tGI, edorNo);
        tPrtGrpInsuredList.submitData();
    }
}
