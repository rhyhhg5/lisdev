package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:保全加费</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class AddPrem
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private MMap mMap = new MMap();

    private DetailDataQuery mQuery = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mPolNo = null;

    private String mDutyCode = null;

    private String mPayPlanCode = null;

    private double mAddPrem = 0.0;

    private LPPremSchema mLPPremSchema = null;

    private LCDutySchema mLCDutySchema = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回MMap
     * @return MMap
     */
    public MMap getMMap()
    {
        return mMap;
    }

    /**
     * 得到加费编码
     * @return String
     */
    public String getPayPlanCode()
    {
        return mPayPlanCode;
    }

    /**
     * 得到外部传入的数据
     * @return boolean
     */
    private boolean getInputData(VData data)
    {
        this.mGlobalInput = (GlobalInput)
                data.getObjectByObjectName("GlobalInput", 0);
        this.mLPPremSchema = (LPPremSchema)
                data.getObjectByObjectName("LPPremSchema", 0);
        this.mEdorNo = mLPPremSchema.getEdorNo();
        this.mEdorType = mLPPremSchema.getEdorType();
        this.mContNo = mLPPremSchema.getContNo();
        this.mPolNo = mLPPremSchema.getPolNo();
        this.mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        this.mLCDutySchema = getLCDuty();
        this.mDutyCode = mLCDutySchema.getDutyCode();
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        mAddPrem = getAddPrem();
        setLPPrem();
        //因为在如果这里设置LPDuty等表加费信息不能重复保存，所以放到核保确认(PEdorManuUWBL)中
        //setLPDuty();
        //setLPPol();
        //setLPCont();
        return true;
    }

    /**
     * 得到LCDuty表中的相关数据
     * @return LCDutySchema
     */
    private LCDutySchema getLCDuty()
    {
        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(mLPPremSchema.getPolNo());
        LCDutySet tLCDutySet = tLCDutyDB.query();
        return tLCDutySet.get(1);
    }

    /**
     * 得到加保的保费
     * @return double
     */
    private double getAddPrem()
    {
        if (mLPPremSchema.getRate() > 0) //按比例算
        {
            return mLCDutySchema.getPrem() * mLPPremSchema.getRate();
        }
        else //按录入保费算
        {
            return mLPPremSchema.getPrem();
        }
    }

    /**
     * 删除上一条数据，PayPlanCode为like '000000%'
     */
    private void deleteLastPrem()
    {
        String sql = "delete from LPPrem " +
                "where EdorNo = '" + mLPPremSchema.getEdorNo() + "' " +
                "and EdorType = '" + mLPPremSchema.getEdorType() + "' " +
                "and PolNo = '" + mLPPremSchema.getPolNo() + "' " +
                "and DutyCode = '" + mLCDutySchema.getDutyCode() + "' " +
                "and PayPlanCode like '" + BQ.FILLDATA + "%' ";
        mMap.put(sql, "DELETE");
    }

    /**
     * 生成保费项号，新契约加费是和险种关联的，一个险种有一个唯一的加费
     * @return String
     */
    private String createPayPlanCode()
    {
        String sql = "select PayPlanCode from LCPrem " +
                "where PolNo = '" + mLPPremSchema.getPolNo() + "' " +
                "and PayPlanCode like '000000%'";
        String payPlanCode = (new ExeSQL()).getOneValue(sql);
        if (payPlanCode.equals(""))
        {
            return "00000001";
        }
        else
        {
            String code = payPlanCode.substring(payPlanCode.length() - 2, payPlanCode.length());
            String newCode = String.valueOf(Integer.parseInt(code) + 1);
            if (newCode.length() < 2)
            {
                newCode = "0"  + newCode;
            }
            return ("000000" + newCode);
        }
    }

    /**
     * 为插入LPPrem准备数据
     */
    private void setLPPrem()
    {
        try
        {
            mPayPlanCode = createPayPlanCode();
            System.out.println("mPayPlanCode" + mPayPlanCode);
            LPPremSchema tLPPremSchema = new LPPremSchema();
            tLPPremSchema.setEdorNo(mEdorNo);
            tLPPremSchema.setEdorType(mEdorType);
            tLPPremSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLPPremSchema.setContNo(mContNo);
            tLPPremSchema.setPolNo(mPolNo);
            tLPPremSchema.setDutyCode(mLCDutySchema.getDutyCode());
            tLPPremSchema.setPayPlanCode(mPayPlanCode);
            tLPPremSchema.setPayPlanType("1"); //健康加费
            tLPPremSchema.setAppntType("1"); //个单
            tLPPremSchema.setAppntNo(mLPPremSchema.getAppntNo());
            tLPPremSchema.setUrgePayFlag("Y"); //催收
            //tLPPremSchema.setNeedAcc("0");
            tLPPremSchema.setPayTimes("0"); //0是未生效，确认之后变为1
            tLPPremSchema.setRate(mLPPremSchema.getRate());
            tLPPremSchema.setPayStartDate(mLPPremSchema.getPayStartDate());
            tLPPremSchema.setPayEndDate(mLPPremSchema.getPayEndDate());
            tLPPremSchema.setPaytoDate(mLCDutySchema.getPaytoDate());
            tLPPremSchema.setPayIntv(mLCDutySchema.getPayIntv());
            tLPPremSchema.setPrem(mAddPrem); //这里不管StandPrem
            tLPPremSchema.setSumPrem(mAddPrem);
            tLPPremSchema.setState("1"); //无意义
            tLPPremSchema.setManageCom(mLPPremSchema.getManageCom());
            tLPPremSchema.setOperator(mGlobalInput.Operator);
            tLPPremSchema.setMakeDate(mCurrentDate);
            tLPPremSchema.setMakeTime(mCurrentTime);
            tLPPremSchema.setModifyDate(mCurrentDate);
            tLPPremSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPPremSchema, "DELETE&INSERT");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
