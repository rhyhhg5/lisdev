package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 部分退保业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Alex
 * @version 1.0
 */
public class PEdorPTAppConfirmBL
        implements EdorAppConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /**  */
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    private LPInsuredSchema mLPInsuredSchema = new LPInsuredSchema();
    private LCPremSet mLCPremSet = new LCPremSet();
    private LPPremSet mLPPremSet = new LPPremSet();
    private LCGetSet mLCGetSet = new LCGetSet();
    private LCGetSet mLPGetSet = new LCGetSet();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LCDutySet mLCDutySet = new LCDutySet();
    private LPDutySet mLPDutySet = new LPDutySet();
    private LJSGetEndorseSchema mLJSGetEndorseSchema = new LJSGetEndorseSchema();

    private LPPremSet aLPPremSet = new LPPremSet();
    private LPGetSet aLPGetSet = new LPGetSet();
    private LPPolSet aLPPolSet = new LPPolSet();
    private LPDutySet aLPDutySet = new LPDutySet();
    private LJSGetEndorseSet aLJSGetEndorseSet = new LJSGetEndorseSet();
    double mGetMoney = 0, mChgPrem = 0;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public PEdorPTAppConfirmBL()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())return false;
        System.out.println("---End getInputData---");

        //数据复杂业务处理
        if (!dealData())return false;
        System.out.println("---End dealData---");

        this.prepareData();

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData()
    {
        try
        {
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData.
                    getObjectByObjectName("LPEdorItemSchema", 0);
            mGlobalInput.setSchema((GlobalInput) mInputData.
                    getObjectByObjectName("GlobalInput", 0));
        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData()
    {
        try
        {
            LPPolSchema tLPPolSchema = new LPPolSchema();
            tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPPolSchema.setContNo(mLPEdorItemSchema.getContNo());
            tLPPolSchema.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
            tLPPolSchema.setPolNo(mLPEdorItemSchema.getPolNo());

            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setSchema(tLPPolSchema);
            LPPolSet tLPPolSet = tLPPolDB.query();
            if (tLPPolSet == null || tLPPolSet.size() <= 0)
            {
                CError.buildErr(this, "保单查询失败");
                return false;
            }
            System.out.println("tLPPolSet size is :" + tLPPolSet.size());

            boolean tCalFlag = false;
            double tChgAmnt = 0;
            mInputData.clear();
            for (int i = 1; i <= tLPPolSet.size(); i++)
            {
                //准备保单的客户相关信息
                tLPPolSchema = tLPPolSet.get(i);

                LCPolSchema tLCPolSchema = new LCPolSchema();
                LCPolBL tLCPolBL = new LCPolBL();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLCPolSchema, tLPPolSchema);

                tLCPolBL.setSchema(tLCPolSchema);
                mLCPolSchema.setSchema(tLCPolSchema);

                //得到险种责任
                if (!getCalDuty(tLPPolSchema))
                    return false;

                //判断是否需要重算
                if (!getCalPrem(tLPPolSchema))
                    return false;

                //传入给付项信息
                if (!getCalGet(tLPPolSchema))
                    return false;

                LCDutyBLSet tLCDutyBLSet = new LCDutyBLSet();
                tLCDutyBLSet.set(mLCDutySet);

                LCGetBLSet tLCGetBLSet = new LCGetBLSet();
                tLCGetBLSet.set(mLCGetSet);

                String aCalFlag = "";

                //只有在一个duty时可以，设置为余留的份数
                if (tLCPolSchema.getMult() > 0)
                    tLCDutyBLSet.get(1).setMult(tLCPolSchema.getMult());

                    //根据保额计算部分退保后应该剩下的保费
                CalBL tCalBL = new CalBL(tLCPolBL, tLCDutyBLSet, tLCGetBLSet,
                        aCalFlag);
                if (!tCalBL.calPol())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tCalBL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "PEdorPTAppConfirmBL";
                    tError.functionName = "prepareData";
                    tError.errorMessage = "计算失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                //为tCalBL中的actuget字段赋值
                for (int getIndex = 1; getIndex <= tCalBL.getLCGet().size();
                        getIndex++)
                {
                    double actuget = tCalBL.getLCGet().get(getIndex).
                            getStandMoney();
                    System.out.println(":::actuget: " + actuget);
                    tCalBL.getLCGet().get(getIndex).setActuGet(actuget);
                }

                // 补上由于重算导致的丢失的责任数据
                if (tCalBL.getLCDuty().size() == 1)
                { // 此时dutycode 为空
                    tCalBL.getLCDuty().get(1).setSumPrem(mLCDutySet.get(1).
                            getSumPrem());
                    tCalBL.getLCDuty().get(1).setPaytoDate(mLCDutySet.get(1).
                            getPaytoDate());
                }
                else
                {
                    for (int index = 1; index <= mLCDutySet.size(); index++)
                    {
                        LCDutySchema srcDutySchema = mLCDutySet.get(index);

                        for (int j = 1; j <= tCalBL.getLCDuty().size(); j++)
                        {
                            if (tCalBL.getLCDuty().get(j).getDutyCode().equals(
                                    srcDutySchema.getDutyCode()))
                            {

                                tCalBL.getLCDuty().get(j).setSumPrem(
                                        srcDutySchema.getSumPrem());
                                tCalBL.getLCDuty().get(j).setPaytoDate(
                                        srcDutySchema.getPaytoDate());
                                break;
                            }
                        }
                    }
                }

                //得到计算结果
                getCalResult(tCalBL.getLCPol(), tCalBL.getLCDuty(),
                        tCalBL.getLCPrem(), tCalBL.getLCGet());
                tChgAmnt = tChgAmnt + tCalBL.getLCPol().getAmnt() -
                        mLCPolSchema.getAmnt();

                //补上因重算丢失的保费信息（承保、核保加费）
                double oldSumPrem = 0; //原标准保费
                double newSumPrem = 0; //新标准保费
                for (int n = 0; n < aLPPremSet.size(); n++)
                {
                    for (int m = 0; m < mLPPremSet.size(); m++)
                    {
                        if (aLPPremSet.get(n +
                                1).getDutyCode().equals(mLPPremSet.get(m + 1).
                                getDutyCode())
                                &&
                                aLPPremSet.get(n +
                                1).getPayPlanCode().equals(mLPPremSet.get(m + 1).
                                getPayPlanCode()))
                        {
                            oldSumPrem = oldSumPrem +
                                    mLPPremSet.get(m + 1).getPrem();
                            newSumPrem = newSumPrem +
                                    aLPPremSet.get(n + 1).getPrem();
                        }
                    }
                }
                //新旧保费比例,用来由于加费生成的保费相减额后的保费
                double rate = newSumPrem / oldSumPrem;

                for (int m = 0; m < mLPPremSet.size(); m++)
                {
                    boolean isExistPrem = false; //flase表示是加费生成的保费项
                    for (int n = 0; n < aLPPremSet.size(); n++)
                    {
                        //发现重算后的保费项集合中有数据，则退出内循环
                        if (aLPPremSet.get(n +
                                1).getDutyCode().equals(mLPPremSet.get(m + 1).
                                getDutyCode())
                                &&
                                aLPPremSet.get(n +
                                1).getPayPlanCode().equals(mLPPremSet.get(m + 1).
                                getPayPlanCode()))
                        {
                            isExistPrem = true;
                            break;
                        }
                    }

                    if (!isExistPrem)
                    {
                        mLPPremSet.get(m +
                                1).setPrem(mLPPremSet.get(m + 1).getPrem() *
                                rate);

//为责任项补上丢失保费项的保费信息
                        boolean isExistDuty = false;
                        for (int l = 0; l < aLPDutySet.size(); l++)
                        {
                            LPDutySchema tLPDutySchema = aLPDutySet.get(l + 1);
                            if (tLPDutySchema.getDutyCode().equals(mLPPremSet.
                                    get(m + 1).getDutyCode()))
                            {
                                tLPDutySchema.setPrem(tLPDutySchema.getPrem() +
                                        mLPPremSet.get(m + 1).getPrem());
                                tLPDutySchema.setSumPrem(tLPDutySchema.
                                        getSumPrem() +
                                        mLPPremSet.get(m + 1).getSumPrem());
                                isExistDuty = true;
                            }
                        }

                        if (!isExistDuty)
                        {
                            break;
                        }

                        //未找到保费项数据，则为丢失数据，加入到重算结果中
                        aLPPremSet.add(mLPPremSet.get(m + 1).getSchema());

                        //为保单补上丢失保费项的保费信息
                        for (int l = 0; l < aLPPolSet.size(); l++)
                        {
                            LPPolSchema tLPPolSchema2 = aLPPolSet.get(l + 1);
                            if (tLPPolSchema2.getPolNo().equals(mLPPremSet.get(
                                    m + 1).getPolNo()))
                            {
                                tLPPolSchema2.setPrem(tLPPolSchema2.getPrem() +
                                        mLPPremSet.get(m + 1).getPrem());
                                tLPPolSchema2.setSumPrem(tLPPolSchema2.
                                        getSumPrem() +
                                        mLPPremSet.get(m + 1).getSumPrem());
                            }
                        }
                    }
                }

            } // end of for

            mLPEdorItemSchema.setChgPrem(mChgPrem);
            System.out.println("mGetMoney3" + mGetMoney);
            mLPEdorItemSchema.setGetMoney(mGetMoney);
            mLPEdorItemSchema.setChgAmnt(tChgAmnt);
            mLPEdorItemSchema.setEdorState("2");
            mLPEdorItemSchema.setUWFlag("0");

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private void prepareData()
    {
        //暂不考虑帐户问题(要与保费项相关）
        MMap map = new MMap();
        map.put(mLPEdorItemSchema, "UPDATE");
        map.put(aLPPolSet, "DELETE&INSERT");
        map.put(aLPDutySet, "DELETE&INSERT");
        map.put(aLPPremSet, "DELETE&INSERT");
        map.put(aLPGetSet, "DELETE&INSERT");
        map.put(aLJSGetEndorseSet, "DELETE&INSERT");
        mResult.add(map);
    }

    /**
     * 目前不用（根据保费项确定要素并决定是否计算）
     * @param aLPPolSchema
     * @return
     */
    private boolean getCalPrem(LPPolSchema aLPPolSchema)
    {
        LCPremDB tLCPremDB = new LCPremDB();
        LCPremSet tLCPremSet = new LCPremSet();
        Reflections tRef = new Reflections();

        mLPPremSet.clear();
        tLCPremDB.setPolNo(aLPPolSchema.getPolNo());
        tLCPremSet = tLCPremDB.query();
        for (int i = 1; i <= tLCPremSet.size(); i++)
        {
            LPPremSchema tLPPremSchema = new LPPremSchema();
            tRef.transFields(tLPPremSchema, tLCPremSet.get(i));
            tLPPremSchema.setEdorNo(aLPPolSchema.getEdorNo());
            tLPPremSchema.setEdorType(aLPPolSchema.getEdorType());
            mLPPremSet.add(tLPPremSchema);
        }
        return true;
    }

    /**
     * 根据保费项确定要素并决定是否计算
     * @param aLPPolSchema
     * @return
     */
    private boolean getCalGet(LPPolSchema aLPPolSchema)
    {
        LCGetDB tLCGetDB = new LCGetDB();
        LCGetSet tLCGetSet = new LCGetSet();
        Reflections tRef = new Reflections();

        mLPGetSet.clear();
        tLCGetDB.setPolNo(aLPPolSchema.getPolNo());
        tLCGetSet = tLCGetDB.query();
        for (int i = 1; i <= tLCGetSet.size(); i++)
        {
            LPGetSchema tLPGetSchema = new LPGetSchema();
            LCGetSchema tLCGetSchema = new LCGetSchema();
            tRef.transFields(tLPGetSchema, tLCGetSet.get(i));
            tLPGetSchema.setEdorNo(aLPPolSchema.getEdorNo());
            tLPGetSchema.setEdorType(aLPPolSchema.getEdorType());
            LPGetBL tLPGetBL = new LPGetBL();
            tLPGetBL.queryLPGet(tLPGetSchema);
            tRef.transFields(tLCGetSchema, tLPGetBL.getSchema());
            mLCGetSet.add(tLCGetSchema);
            mLPGetSet.add(tLPGetBL.getSchema());
        }
        return true;
    }

    /**
     * 得到计算用的责任信息
     * @param aLPPolSchema
     * @return
     */
    private boolean getCalDuty(LPPolSchema aLPPolSchema)
    {
        LCDutyDB tLCDutyDB = new LCDutyDB();
        LCDutySet tLCDutySet = new LCDutySet();
        Reflections tRef = new Reflections();
        mLCDutySet.clear();
        tLCDutyDB.setPolNo(aLPPolSchema.getPolNo());
        tLCDutySet = tLCDutyDB.query();
        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            LPDutySchema tLPDutySchema = new LPDutySchema();

            tRef.transFields(tLPDutySchema, tLCDutySet.get(i));
            tLPDutySchema.setEdorNo(aLPPolSchema.getEdorNo());
            tLPDutySchema.setEdorType(aLPPolSchema.getEdorType());

            mLPDutySet.add(tLPDutySchema.getSchema());

            LPDutyBL tLPDutyBL = new LPDutyBL();
            tLPDutyBL.queryLPDuty(tLPDutySchema);
            LCDutySchema tLCDutySchema = new LCDutySchema();
            tRef.transFields(tLCDutySchema, tLPDutyBL.getSchema());
            if (tLCDutySet.size() > 1)
            {
                mLCDutySet.add(tLCDutySchema);
            }
            else
            {
                tLCDutySchema.setDutyCode(null);
                mLCDutySet.add(tLCDutySchema);
            }
        }
        return true;
    }

    //得到计算结果
    private boolean getCalResult(LCPolBL aLCPolBL, LCDutyBLSet aLCDutyBLSet,
            LCPremBLSet aLCPremBLSet,
            LCGetBLSet aLCGetBLSet)
    {
        //a表为重算后的结果，m表为重算前的数据
        Reflections tRef = new Reflections();
        LCPremSet tLCPremSet = new LCPremSet();

        //准备重算后的保单表数据
        LPPolSchema tLPPolSchema = new LPPolSchema();
        //得到原来的的保单未变更信息（原保单交至日期，签单机构，签单日期等信息是重算后也不应该变化的）
        tRef.transFields(tLPPolSchema, aLCPolBL.getSchema());
        tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPPolSchema.setPaytoDate(mLCPolSchema.getPaytoDate());
        tLPPolSchema.setSignCom(mLCPolSchema.getSignCom());
        tLPPolSchema.setSignDate(mLCPolSchema.getSignDate());
        tLPPolSchema.setAppFlag(mLCPolSchema.getAppFlag());
        tLPPolSchema.setHandler(mLCPolSchema.getHandler());
        //将重算后的保费设置为标准保费
        tLPPolSchema.setStandPrem(tLPPolSchema.getPrem());
        aLPPolSet.add(tLPPolSchema);

        //准备重算后的责任表数据
        LPDutySet tLPDutySet = new LPDutySet();
        for (int i = 1; i <= aLCDutyBLSet.size(); i++)
        {
            LPDutySchema tLPDutySchema = new LPDutySchema();
            tRef.transFields(tLPDutySchema, aLCDutyBLSet.get(i).getSchema());

            //得到原来的的未变更信息
            for (int j = 1; j <= mLPDutySet.size(); j++)
            {
                if (tLPDutySchema.getPolNo().trim().equals(mLPDutySet.get(j).
                        getPolNo().trim())
                        &&
                        tLPDutySchema.getDutyCode().trim().equals(mLPDutySet.
                        get(j).
                        getDutyCode().trim()))
                {
                    //此处不能修正，否则后面的退保计算会出错，2004-6-29日作112205时，发现需要，请确认
                    tLPDutySchema.setPrem(mLPDutySet.get(j).getPrem() -
                            tLPDutySchema.getPrem());
                    tLPDutySchema.setAmnt(mLPDutySet.get(j).getAmnt() -
                            tLPDutySchema.getAmnt());

                    tLPDutySchema.setPaytoDate(mLPDutySet.get(j).getPaytoDate());
                    tLPDutySchema.setSumPrem(mLPDutySet.get(j).getSumPrem());
                    break;
                }
            }

            //将重算后的保费设置为标准保费
//      tLPDutySchema.setStandPrem(tLPDutySchema.getPrem());

            tLPDutySchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPDutySchema.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPDutySchema.setModifyDate(PubFun.getCurrentDate());
            tLPDutySchema.setModifyTime(PubFun.getCurrentTime());

            tLPDutySet.add(tLPDutySchema);
            aLPDutySet.add(tLPDutySchema);
        }

        //得到prem的总额，以便以后进行退保rate的计算
        double dblGrossPrem = 0;
        for (int i = 1; i <= aLCPremBLSet.size(); i++)
        {
            dblGrossPrem += aLCPremBLSet.get(i).getPrem();
        }
        System.out.println("------gross prem : " + dblGrossPrem);

        //准备重算后的保费项表数据
        LPPremSet tLPPremSet = new LPPremSet();
        for (int i = 1; i <= aLCPremBLSet.size(); i++)
        {
            LPPremSchema tLPPremSchema = new LPPremSchema();
            tRef.transFields(tLPPremSchema, aLCPremBLSet.get(i).getSchema());

            //得到原来的的未变更信息
            double changePrem = 0;
            for (int j = 1; j <= mLPPremSet.size(); j++)
            {
                if (tLPPremSchema.getPolNo().trim().equals(mLPPremSet.get(j).
                        getPolNo().trim())
                        &&
                        tLPPremSchema.getDutyCode().trim().equals(mLPPremSet.
                        get(j).
                        getDutyCode().trim())
                        &&
                        tLPPremSchema.getPayPlanCode().trim().equals(mLPPremSet.
                        get(j).getPayPlanCode().trim()))
                {
//          tLPPremSchema.setPrem(mLPPremSet.get(j).getPrem()-tLPPremSchema.getPrem());
                    changePrem = mLPPremSet.get(j).getPrem() -
                            tLPPremSchema.getPrem();

                    tLPPremSchema.setPaytoDate(mLPPremSet.get(j).getPaytoDate());
                    tLPPremSchema.setSumPrem(mLPPremSet.get(j).getSumPrem());
                    tLPPremSchema.setMakeDate(mLPPremSet.get(j).getMakeDate());
                    tLPPremSchema.setMakeTime(mLPPremSet.get(j).getMakeTime());
                    break;
                }
            }

            //将重算后的保费设置为标准保费
//      tLPPremSchema.setStandPrem(tLPPremSchema.getPrem());

            tLPPremSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPPremSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPPremSchema.setOperator(mGlobalInput.Operator);
            tLPPremSchema.setModifyDate(PubFun.getCurrentDate());
            tLPPremSchema.setModifyTime(PubFun.getCurrentTime());

            //提交保存的保费项
            aLPPremSet.add(tLPPremSchema);

            //提交重算的保费项,要把保费改为差值
            LPPremSchema LPPremSchema2 = tLPPremSchema.getSchema();
            LPPremSchema2.setPrem(changePrem);
            tLPPremSet.add(LPPremSchema2);
        }

        //准备重算后的领取项表数据
        LPGetSet tLPGetSet = new LPGetSet();
        for (int i = 1; i <= aLCGetBLSet.size(); i++)
        {
            LPGetSchema tLPGetSchema = new LPGetSchema();
            tRef.transFields(tLPGetSchema, aLCGetBLSet.get(i).getSchema());

            tLPGetSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPGetSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPGetSchema.setModifyDate(PubFun.getCurrentDate());
            tLPGetSchema.setModifyTime(PubFun.getCurrentTime());

            tLPGetSet.add(tLPGetSchema);
            aLPGetSet.add(tLPGetSchema);
        }

        //产生交退费信息
        //如果是在犹豫期内的，直接退还减额部分的保费，而不用进行计算
        if (isInWTPeriod(aLCPolBL.getSchema()))
        {
            System.out.println("在犹豫期内的，直接退还减额部分的保费，而不用进行计算");
            LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
            tLJSGetEndorseSchema.setGetNoticeNo(mLPEdorItemSchema.getEdorNo());
            tLJSGetEndorseSchema.setEndorsementNo(mLPEdorItemSchema.getEdorNo());
            tLJSGetEndorseSchema.setFeeOperationType(mLPEdorItemSchema.
                    getEdorType());
            tLJSGetEndorseSchema.setGetDate(mLPEdorItemSchema.getEdorValiDate());
            tLJSGetEndorseSchema.setMakeDate(tLPPolSchema.getMakeDate());
            tLJSGetEndorseSchema.setMakeTime(tLPPolSchema.getMakeTime());
            tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
            tLJSGetEndorseSchema.setContNo(tLPPolSchema.getContNo());
            tLJSGetEndorseSchema.setGrpPolNo(tLPPolSchema.getGrpPolNo());
            tLJSGetEndorseSchema.setPolNo(tLPPolSchema.getPolNo());
            tLJSGetEndorseSchema.setAppntNo(tLPPolSchema.getAppntNo());
            tLJSGetEndorseSchema.setInsuredNo(tLPPolSchema.getInsuredNo());
            tLJSGetEndorseSchema.setAppntNo(tLPPolSchema.getAppntNo());
            tLJSGetEndorseSchema.setKindCode(tLPPolSchema.getKindCode());
            tLJSGetEndorseSchema.setRiskCode(tLPPolSchema.getRiskCode());
            tLJSGetEndorseSchema.setRiskVersion(tLPPolSchema.getRiskVersion());
            tLJSGetEndorseSchema.setAgentCom(tLPPolSchema.getAgentCom());
            tLJSGetEndorseSchema.setAgentCode(tLPPolSchema.getAgentCode());
            tLJSGetEndorseSchema.setAgentType(tLPPolSchema.getAgentType());
            tLJSGetEndorseSchema.setAgentGroup(tLPPolSchema.getAgentGroup());

            //从描述表中获取财务接口类型，modify by Minim at 2003-12-23
            String finType = BqCalBL.getFinType(mLPEdorItemSchema.getEdorType(),
                    "TB",
                    tLJSGetEndorseSchema.getPolNo());
            if (finType.equals(""))
            {
                CError.buildErr(this, "在LDCode1中缺少保全财务接口转换类型编码");
                return false;
            }
            tLJSGetEndorseSchema.setFeeFinaType(finType);

            tLJSGetEndorseSchema.setDutyCode("0");
            tLJSGetEndorseSchema.setPayPlanCode("0");
            tLJSGetEndorseSchema.setOtherNo("0");
            tLJSGetEndorseSchema.setOtherNoType("POL");
            tLJSGetEndorseSchema.setPolType("1");
            tLJSGetEndorseSchema.setSerialNo("");
            tLJSGetEndorseSchema.setGetFlag("1");
            tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
            tLJSGetEndorseSchema.setManageCom(tLPPolSchema.getManageCom());

            //计算退费
            double money = mLCPolSchema.getPrem() - tLPPolSchema.getPrem();
            tLJSGetEndorseSchema.setGetMoney( -money);

            aLJSGetEndorseSet.add(tLJSGetEndorseSchema);

            mGetMoney = -money;
            mChgPrem = -money;
             System.out.println("mGetMoney1" + mGetMoney);
            return true;
        }

        //在生存退保范围以内的，计算应该退的保费
        System.out.println("在生存退保范围以内的，计算应该退的保费");
        EdorCalZT tEdorCalZT = null;
        double tGetMoney = 0;

        for (int i = 0; i < tLPDutySet.size(); i++)
        {
            LPDutySchema tLPDutySchema = tLPDutySet.get(i + 1);

            //获取生存退保计算责任描述
            LMEdorZTDutyDB tLMEdorZTDutyDB = new LMEdorZTDutyDB();
            tLMEdorZTDutyDB.setRiskCode(aLCPolBL.getRiskCode());
            tLMEdorZTDutyDB.setDutyCode(tLPDutySchema.getDutyCode());
            if (!tLMEdorZTDutyDB.getInfo())
            {
                CError.buildErr(this, "获取生存退保计算责任描述表数据失败");
                return false;
            }

            //判断退保类型，是否按账户退保，0－－非帐户退保；1－－帐户退保
            if (tLMEdorZTDutyDB.getPayByAcc().equals("1"))
            {
                System.out.println("该责任项（" + tLPDutySchema.getDutyCode() +
                        "）要求按帐户退保，不能进行该流程");
                continue;
            }

            double totalMoney = 0;
            //判断退保计算类型，是按保费还是保额计算，0－－按保费；1－－按保额
            if (tLMEdorZTDutyDB.getPayCalType().equals("1"))
            {
                tEdorCalZT = new EdorCalZT("1");
                try
                {
                    LPEdorItemSchema tempLPEdorItemSchem = new LPEdorItemSchema();
                    tempLPEdorItemSchem.setSchema(mLPEdorItemSchema); //在LPEdorItem表中polno为000000
                    tempLPEdorItemSchem.setPolNo(tLPDutySchema.getPolNo());
                    tEdorCalZT.getDutyZTData(tempLPEdorItemSchem, tLPDutySchema);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
            else if (tLMEdorZTDutyDB.getPayCalType().equals("0"))
            {
                for (int j = 0; j < tLPPremSet.size(); j++)
                {
                    if (tLPPremSet.get(j +
                            1).getDutyCode().equals(tLPDutySchema.getDutyCode()))
                    {
                        tEdorCalZT = new EdorCalZT("1");
                        try
                        {
                            LPEdorItemSchema tempLPEdorItemSchem = new
                                    LPEdorItemSchema();
                            tempLPEdorItemSchem.setSchema(mLPEdorItemSchema); //在LPEdorItem表中polno为000000
                            tempLPEdorItemSchem.setPolNo(tLPDutySchema.getPolNo());
                            tEdorCalZT.getPremZTData(tempLPEdorItemSchem,
                                    tLPDutySchema, tLPPremSet.get(i + 1));
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                }
            }

        }

        System.out.println("tEdorCal getZTPoint:" + tEdorCalZT.getZTPoint());
        mLPEdorItemSchema.setEdorValiDate(tEdorCalZT.getZTPoint());

//    aLJSGetEndorseSet = tEdorCalZT.getLJSGetEndorseSet();
        aLJSGetEndorseSet.add(tEdorCalZT.mLJSGetEndorseSet);
        if (aLJSGetEndorseSet != null && aLJSGetEndorseSet.size() > 0)
        {
            for (int k = 1; k <= aLJSGetEndorseSet.size(); k++)
            {
                LJSGetEndorseSchema tLJSGetEndorseSchema = aLJSGetEndorseSet.
                        get(k);

                //从描述表中获取财务接口类型，modify by Minim at 2003-12-23
                String finType = BqCalBL.getFinType(mLPEdorItemSchema.
                        getEdorType(), "TB", tLJSGetEndorseSchema.getPolNo());
                if (finType.equals(""))
                {
                    CError.buildErr(this, "在LDCode1中缺少保全财务接口转换类型编码");
                    return false;
                }
                tLJSGetEndorseSchema.setFeeFinaType(finType);

                //取得部分退保的费用
                tLJSGetEndorseSchema.setManageCom(mLPEdorItemSchema.
                        getManageCom());
                tLJSGetEndorseSchema.setGetFlag("1");
                tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
                tGetMoney = tLJSGetEndorseSchema.getGetMoney();
            }
        }

        mGetMoney = tGetMoney;
        System.out.println("mGetMoney12" + mGetMoney);
        return true;
    }

    /**
     * 判断保单是否是在犹豫期内
     * @return
     */
    private boolean isInWTPeriod(LCPolSchema tLCPolSchema)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tLCPolSchema.getContNo());
        if (!tLCContDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLCContDB.mErrors);
            CError.buildErr(this, "查询lccont表失败");
            return false;
        }
        LMEdorWTDB tLMEdorWTDB = new LMEdorWTDB();
        tLMEdorWTDB.setRiskCode(tLCPolSchema.getRiskCode());
        tLMEdorWTDB.setRiskVersion(tLCPolSchema.getRiskVersion());
        if (tLMEdorWTDB.getInfo())
        {
            if (tLCContDB.getCustomGetPolDate() == null ||
                    "".equals(tLCContDB.getCustomGetPolDate()))
            {
                System.out.println("hestType : " + tLMEdorWTDB.getHesitateType());
                int tInterval = PubFun.calInterval(tLCContDB.getCValiDate(),
                        mLPEdorItemSchema.getEdorValiDate(), "D");
                if (tInterval < 0)
                {
                    System.out.println("保单送达日期不能小于保全生效日期!");
                    return false;
                }
                if (tInterval < 18)
                {
                    return true;
                }

            }
            else
            {
                int tInterval = PubFun.calInterval(tLCContDB.
                        getCustomGetPolDate(),
                        mLPEdorItemSchema.
                        getEdorValiDate(),
                        tLMEdorWTDB.getHesitateType());
                if (tInterval < 0)
                {
                    System.out.println("保单送达日期不能小于保全生效日期!");
                    return false;
                }
                if (tInterval <
                        (tLMEdorWTDB.getHesitateEnd() -
                        tLMEdorWTDB.getHesitateStart()))
                {
                    return true;
                }
            }

            return false;
        }
        return false;
    }

    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator="endor";
        gi.ManageCom = "86";

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo("20050721000009");
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        for (int i = 1; i <= tLPEdorItemSet.size(); i ++)
        {
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i);
            VData data = new VData();
            data.add(gi);
            data.add(tLPEdorItemSchema);
            PEdorPTAppConfirmBL tPEdorPTAppConfirmBL = new PEdorPTAppConfirmBL();
            tPEdorPTAppConfirmBL.submitData(data, "");
        }
    }
}
