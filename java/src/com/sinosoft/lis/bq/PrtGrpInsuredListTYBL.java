package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
* <p>Title: 追加保费清单</p>
* <p>Description:追加保费清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/


public class PrtGrpInsuredListTYBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private String mGrpContNo = null;

    private String mEdorNo = null;

    private String mEdorType = BQ.EDORTYPE_TY;

    private XmlExport xml = new XmlExport();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListTYBL(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = getGrpContNo(edorNo);
    }

    /**
     * 提交业务数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!createXML())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }

    /**
     * 根据清单数据生成XML
     * @param xmlExport XmlExport
     */
    private boolean createXML()
    {
        xml.createDocument("PrtGrpInsuredListTY.vts", "printer");

        //设置批单中TextTag要显示的信息
        TextTag tag = new TextTag();
        tag.add("GrpName", getGrpName(mGrpContNo));
        tag.add("EdorNo", mEdorNo);
        xml.addTextTag(tag);

        //设置被保人清单
        ListTable listTable = new ListTable();
        listTable.setName("TY");
        LPDiskImportSet tLPDiskImportSet = getInsuredList();
        for (int i = 1; i <= tLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(i);
            String[] column = new String[13];
            column[0] = StrTool.cTrim(tLPDiskImportSchema.getSerialNo());
            column[1] = StrTool.cTrim(tLPDiskImportSchema.getInsuredName());
            column[2] = StrTool.cTrim(tLPDiskImportSchema.getInsuredNo());
            column[3] = ChangeCodeBL.getCodeName("Sex",tLPDiskImportSchema.getSex());
            column[4] = StrTool.cTrim(tLPDiskImportSchema.getBirthday());
            column[5] = ChangeCodeBL.getCodeName("IDType",tLPDiskImportSchema.getIDType());
            column[6] = StrTool.cTrim(tLPDiskImportSchema.getIDNo());
//            单位缴费
            column[7] = StrTool.cTrim(CommonBL.bigDoubleToCommonString(tLPDiskImportSchema.getAppntPrem(),"0.00"));
//            个人缴费
            column[8] = StrTool.cTrim(CommonBL.bigDoubleToCommonString(tLPDiskImportSchema.getPersonPrem(),"0.00"));
            //20160127转double之前简单处理一下，避免导入无效的被保人没有该金额导致异常
            String money2=tLPDiskImportSchema.getMoney2();
            if(null == money2 || "".equals(money2)){
            	money2="0";
            }
//            初始费用
            column[9] = StrTool.cTrim(CommonBL.bigDoubleToCommonString(Double.parseDouble(money2),"0.00"));
//          账户原金额
            column[10] = StrTool.cTrim(CommonBL.bigDoubleToCommonString(tLPDiskImportSchema.getMoney(),"0.00"));
//          账户现金额
            column[11] = StrTool.cTrim(CommonBL.bigDoubleToCommonString(tLPDiskImportSchema.getGetMoney(),"0.00"));
            column[12] = CommonBL.decodeState(tLPDiskImportSchema.getState());
            listTable.add(column);
        }
        xml.addListTable(listTable, new String[9]);
        xml.outputDocumentToFile("c:\\", "TYInsuredList");
        return true;
    }

    /**
     * 得到团体合同号
     * @param edorNo String
     * @return String
     */
    private String getGrpContNo(String edorNo)
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(edorNo);
        tLPGrpEdorItemDB.setEdorType(BQ.EDORTYPE_TY); //增人
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            return null;
        }
        return tLPGrpEdorItemSet.get(1).getGrpContNo();
    }

    /**
     * 得到团体单位名称
     * @param grpContNo String
     * @return String
     */
    private String getGrpName(String grpContNo)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
        if (tLCGrpContSet.size() == 0)
        {
            return null;
        }
        return tLCGrpContSet.get(1).getGrpName();
    }

    /**
     * 得到被保人列表
     * @return LPDiskImportSet
     */
    private LPDiskImportSet getInsuredList()
    {
        String sql = "select * from LPDiskImport " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "union " +
                "select * from LBDiskImport " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "order by SerialNo ";
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        return tLPDiskImportDB.executeQuery(sql);
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        VData d = new VData();
        d.add(gi);

        PrtGrpInsuredListTYBL bl = new PrtGrpInsuredListTYBL(gi, "20070213000061");
        if(!bl.submitData())
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
