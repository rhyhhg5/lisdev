package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.MMap;
import java.util.List;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import java.util.ArrayList;
import com.sinosoft.lis.db.LPEdorAppDB;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.vschema.LGWorkSet;
import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.lis.vschema.LPEdorMainSet;
import com.sinosoft.lis.db.LPEdorMainDB;
import com.sinosoft.lis.schema.LGTraceNodeOpSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LMUWSet;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.db.LMUWDB;
import com.sinosoft.lis.schema.LMUWSchema;
import com.sinosoft.lis.schema.LPUWErrorSchema;
import com.sinosoft.lis.brms.databus.RuleTransfer;
import org.jdom.Document;
import com.sinosoft.lis.vschema.LPUWErrorSet;
import com.sinosoft.utility.SysConst;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.schema.LPEdorItemSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class RulePEdorAutoUWBL {

    /** 错误处理 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String xmlStr = null ;

    private MMap mMap = new MMap();

    private String mUWState = null;

    private LMUWSet mLMUWSet = new LMUWSet();

    private String mEdorType = "";

    /** 用于输出的核保错误信息列表 */
    private List mUwErrorList = new ArrayList();

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    public RulePEdorAutoUWBL() {
    }

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public RulePEdorAutoUWBL(GlobalInput gi, String edorNo) {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
    }

    public RulePEdorAutoUWBL(GlobalInput gi, String edorNo,LMUWSet tLMUWSet) {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mLMUWSet = tLMUWSet;
    }



    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData() {
        if (!getInputData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @return boolean
     */
    private boolean getInputData() {
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData() {
//        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
//        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
//        if (!tLPEdorAppDB.getInfo()) {
//            mErrors.addOneError("未找到该保全受理信息！");
//            return false;
//        }
    	
    	MMap tCekMap = null;
    	tCekMap = lockBQUW(mEdorNo,"UW", "3600");
        if (tCekMap == null)
        {
            return false;
        }
        mMap.add(tCekMap);

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        String strSQL = "select * From lpedoritem where edoracceptno ='"+mEdorNo+"'";
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.executeQuery(strSQL);

        LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(1).getSchema();
        mEdorType = tLPEdorItemSchema.getEdorType();
        LPUWErrorSet tLPUWErrorSet = new LPUWErrorSet();
//        LPPolDB tLPPolDB = new LPPolDB();
//        tLPPolDB.setEdorNo(mEdorNo);
//        LPPolSet tLPPolSet = tLPPolDB.query();
        String x ="1" ;
        for (int i = 1 ; i <= mLMUWSet.size() ; i++)
        {
            LMUWSchema tLMUWSchema = this.mLMUWSet.get(i).getSchema();
            if (!tLMUWSchema.getRiskCode().equals(BQ.FILLDATA)) {
                LPPolDB tLPPolDB = new LPPolDB();
                tLPPolDB.setEdorNo(mEdorNo);
                tLPPolDB.setRiskCode(tLMUWSchema.getRiskCode());
                tLPPolDB.setInsuredNo(tLMUWSchema.getOthCalCode());
                LPPolSet tLPPolSet = tLPPolDB.query();
                if (tLPPolSet.size()==0) {
                	System.out.println("此险种在LPPOL中不存在,跳过核保,险种号:"+tLMUWSchema.getRiskCode());
					continue;
				}
                LPPolSchema aLPPolSchema = tLPPolSet.get(1).getSchema();
                String tLimit = PubFun.getNoLimit(aLPPolSchema.getManageCom());
                //生成批次号
                String mserNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
                LPUWErrorSchema tLPUWErrorSchema = new LPUWErrorSchema();
                tLPUWErrorSchema.setEdorNo(aLPPolSchema.getEdorNo());
                tLPUWErrorSchema.setEdorType(aLPPolSchema.getEdorType());
                tLPUWErrorSchema.setGrpContNo(aLPPolSchema.getGrpContNo());
                tLPUWErrorSchema.setContNo(aLPPolSchema.getContNo());
                tLPUWErrorSchema.setProposalContNo(aLPPolSchema.getProposalNo());
                tLPUWErrorSchema.setPolNo(aLPPolSchema.getPolNo());
                tLPUWErrorSchema.setProposalNo(aLPPolSchema.getProposalNo());
                tLPUWErrorSchema.setSerialNo(mserNo); //对自核来说SerialNo没有意义
                tLPUWErrorSchema.setInsuredNo(aLPPolSchema.getInsuredNo());
                tLPUWErrorSchema.setInsuredName(aLPPolSchema.getInsuredName());
                tLPUWErrorSchema.setAppntNo(aLPPolSchema.getAppntNo());
                tLPUWErrorSchema.setAppntName(aLPPolSchema.getAppntName());
                tLPUWErrorSchema.setManageCom(mGlobalInput.ManageCom);
                tLPUWErrorSchema.setUWError(tLMUWSchema.getRemark());
                tLPUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode());
                tLPUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
                tLPUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
                tLPUWErrorSchema.setUWPassFlag(BQ.UWFLAG_FAIL); //核保未通过
                tLPUWErrorSet.add(tLPUWErrorSchema);
            }
            //add by fuxin 2010-5-6 保单遗失补发不生成P表，所以这个保全项目查C表。
            else if(tLMUWSchema.getRiskCode().equals(BQ.FILLDATA) && mEdorType.equals(BQ.EDORTYPE_LR))
            {
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setContNo(tLPEdorItemSchema.getContNo());
                LCPolSet tLCPolSet = tLCPolDB.query();
                for (int k = 1 ; k<=tLCPolSet.size(); k++)
                {
                    LCPolSchema tLCPolSchema = tLCPolSet.get(k).getSchema();
                    LPUWErrorSchema tLPUWErrorSchema = new LPUWErrorSchema();
                   tLPUWErrorSchema.setEdorNo(mEdorNo);
                   tLPUWErrorSchema.setEdorType(mEdorType);
                   tLPUWErrorSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                   tLPUWErrorSchema.setContNo(tLCPolSchema.getContNo());
                   tLPUWErrorSchema.setProposalContNo(tLCPolSchema.getProposalNo());
                   tLPUWErrorSchema.setPolNo(tLCPolSchema.getPolNo());
                   tLPUWErrorSchema.setProposalNo(tLCPolSchema.getProposalNo());
                   String tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
                   //生成批次号
                   String mserNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
                   tLPUWErrorSchema.setSerialNo(mserNo); //对自核来说SerialNo没有意义
                   tLPUWErrorSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
                   tLPUWErrorSchema.setInsuredName(tLCPolSchema.getInsuredName());
                   tLPUWErrorSchema.setAppntNo(tLCPolSchema.getAppntNo());
                   tLPUWErrorSchema.setAppntName(tLCPolSchema.getAppntName());
                   tLPUWErrorSchema.setManageCom(mGlobalInput.ManageCom);
                   tLPUWErrorSchema.setUWError(tLMUWSchema.getRemark());
                   tLPUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode());
                   tLPUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
                   tLPUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
                   tLPUWErrorSchema.setUWPassFlag(BQ.UWFLAG_FAIL); //核保未通过
                   tLPUWErrorSet.add(tLPUWErrorSchema);
                }
            }
            else {
                LPPolDB tLPPolDB = new LPPolDB();
                tLPPolDB.setEdorNo(mEdorNo);
                //对于传入被保人的核保情况增加对应的pol表关联,add by xp 2010-5-27
                if (!(tLMUWSchema.getOthCalCode()==""||tLMUWSchema.getOthCalCode()==null)) {
                	tLPPolDB.setInsuredNo(tLMUWSchema.getOthCalCode());
				}
                LPPolSet tLPPolSet = tLPPolDB.query();
                for (int k = 1; k <= tLPPolSet.size(); k++) {
                    LPPolSchema aLPPolSchema = tLPPolSet.get(k).getSchema();
                    LPUWErrorSchema tLPUWErrorSchema = new LPUWErrorSchema();
                    tLPUWErrorSchema.setEdorNo(aLPPolSchema.getEdorNo());
                    tLPUWErrorSchema.setEdorType(aLPPolSchema.getEdorType());
                    tLPUWErrorSchema.setGrpContNo(aLPPolSchema.getGrpContNo());
                    tLPUWErrorSchema.setContNo(aLPPolSchema.getContNo());
                    tLPUWErrorSchema.setProposalContNo(aLPPolSchema.getProposalNo());
                    tLPUWErrorSchema.setPolNo(aLPPolSchema.getPolNo());
                    tLPUWErrorSchema.setProposalNo(aLPPolSchema.getProposalNo());
                    String tLimit = PubFun.getNoLimit(aLPPolSchema.getManageCom());
                    //生成批次号
                    String mserNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
                    tLPUWErrorSchema.setSerialNo(mserNo); //对自核来说SerialNo没有意义
                    tLPUWErrorSchema.setInsuredNo(aLPPolSchema.getInsuredNo());
                    tLPUWErrorSchema.setInsuredName(aLPPolSchema.getInsuredName());
                    tLPUWErrorSchema.setAppntNo(aLPPolSchema.getAppntNo());
                    tLPUWErrorSchema.setAppntName(aLPPolSchema.getAppntName());
                    tLPUWErrorSchema.setManageCom(mGlobalInput.ManageCom);
                    tLPUWErrorSchema.setUWError(tLMUWSchema.getRemark());
                    tLPUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode());
                    tLPUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
                    tLPUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
                    tLPUWErrorSchema.setUWPassFlag(BQ.UWFLAG_FAIL); //核保未通过
                    tLPUWErrorSet.add(tLPUWErrorSchema);
                }

            }
            mMap.put(tLPUWErrorSet, SysConst.DELETE_AND_INSERT);
        }
        this.mUWState = BQ.UWFLAG_FAIL;
        if (!createWorkflow()) {
            return false;
        }
        createWorkTrace();
        setUWState(true);

         if (!submit())
         {
             return false;
         }

        return true;
    }

    /**
     * 创建工作流，向工作流表中插数据，送人工核保
     */
    private boolean createWorkflow() {
        LPEdorMainSchema tLPEdorMainSchema = getEodrMain();
        LCContSchema tLCContSchema = CommonBL.getLCCont(tLPEdorMainSchema.
                getContNo());
        if (tLCContSchema == null) {
            mErrors.addOneError("未找到LPEdorMain表的合同信息！");
            return false;
        }
        LGWorkSchema tLGWorkSchema = getWorkInfo();

        String missionId = PubFun1.CreateMaxNo("MissionId", 20);
        LWMissionSchema tLWMissionSchema = new LWMissionSchema();
        tLWMissionSchema.setMissionID(missionId);
        tLWMissionSchema.setSubMissionID("1");
        tLWMissionSchema.setProcessID("0000000003");
        tLWMissionSchema.setActivityID("0000001180");
        tLWMissionSchema.setActivityStatus("1");
        tLWMissionSchema.setMissionProp1(tLPEdorMainSchema.getEdorNo());
        tLWMissionSchema.setMissionProp2(tLPEdorMainSchema.getContNo());
        tLWMissionSchema.setMissionProp3(tLGWorkSchema.getCustomerNo());
        tLWMissionSchema.setMissionProp4(
                CommonBL.getAppntName(tLGWorkSchema.getCustomerNo()));
        tLWMissionSchema.setMissionProp5(
                CommonBL.getUserName(tLGWorkSchema.getAcceptorNo()));
        tLWMissionSchema.setMissionProp6(tLPEdorMainSchema.getEdorAppDate());
        tLWMissionSchema.setMissionProp7(
                CommonBL.getUserName(tLPEdorMainSchema.getOperator()));
        tLWMissionSchema.setMissionProp8(mCurrentDate);
        tLWMissionSchema.setMissionProp9(mCurrentTime);
        tLWMissionSchema.setMissionProp10(tLCContSchema.getManageCom());
        tLWMissionSchema.setMissionProp11(mEdorType);
        tLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
        tLWMissionSchema.setLastOperator(mGlobalInput.Operator);
        tLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
        tLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
        tLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
        tLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLWMissionSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 查询工单信息
     * @return LGWorkSchema
     */
    private LGWorkSchema getWorkInfo() {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setDetailWorkNo(mEdorNo);
        LGWorkSet tLGWorkSet = tLGWorkDB.query();
        if (tLGWorkSet.size() == 0) {
            mErrors.addOneError("未找到该保全工单信息！");
            return null;
        }
        return tLGWorkSet.get(1);
    }

    /**
     * 查询Main表
     * @return boolean
     */
    private LPEdorMainSchema getEodrMain() {
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mEdorNo);
        LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.query();
        if (tLPEdorMainSet.size() == 0) {
            mErrors.addOneError("未找到该保全受理信息！");
            return null;
        }
        return tLPEdorMainSet.get(1);
    }

    /**
     * 生成作业历史
     * @return boolean
     */
    private void createWorkTrace() {
        String sql = "select max(int(NodeNo)) from LGTraceNodeOp " +
                     "where WorkNo = '" + mEdorNo + "'";
        String nodeNo = (new ExeSQL()).getOneValue(sql);
        sql = "select max(int(OperatorNo)) + 1 from LGTraceNodeOp " +
              "where WorkNo = '" + mEdorNo + "'";
        String operatorNo = (new ExeSQL()).getOneValue(sql);

        //生成具体历史信息
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
        tLGTraceNodeOpSchema.setWorkNo(mEdorNo);
        tLGTraceNodeOpSchema.setNodeNo(nodeNo);
        tLGTraceNodeOpSchema.setOperatorNo(operatorNo);
        tLGTraceNodeOpSchema.setOperatorType("5"); //人工核保
        tLGTraceNodeOpSchema.setRemark("自动批注：送人工核保");
        tLGTraceNodeOpSchema.setFinishDate(mCurrentDate);
        tLGTraceNodeOpSchema.setFinishTime(mCurrentTime);
        tLGTraceNodeOpSchema.setOperator(mGlobalInput.Operator);
        tLGTraceNodeOpSchema.setMakeDate(mCurrentDate);
        tLGTraceNodeOpSchema.setMakeTime(mCurrentTime);
        tLGTraceNodeOpSchema.setModifyDate(mCurrentDate);
        tLGTraceNodeOpSchema.setModifyTime(mCurrentTime);
        mMap.put(tLGTraceNodeOpSchema, "DELETE&INSERT");
    }

    /**
     * 设置核保状态
     */
    private void setUWState(boolean changeEdorState) {
        //更新App表
        String sql = "update LPEdorApp " +
                     "set UWState = '" + mUWState + "' ";
        if (changeEdorState == true) {
            sql += ", EdorState = '" + BQ.EDORSTATE_SENDUW + "' ";
        }
        sql += "where EdorAcceptNo = '" + mEdorNo + "'";
        mMap.put(sql, "UPDATE");
        //更新Main表
        sql = "update LPEdorMain " +
              "set UWState = '" + mUWState + "' ";
        if (changeEdorState == true) {
            sql += ", EdorState = '" + BQ.EDORSTATE_SENDUW + "' ";
        }
        sql += "where EdorNo = '" + mEdorNo + "'";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 得到核保标志
     * @return String
     */
    public String getUWState() {
        return mUWState;
    }

    /**
     * 得到自动核未通过保原因
     * @return String
     */
    public String getUWErrors() {
        String uwErrors = "";
        for (int i = 0; i < mUwErrorList.size(); i++) {
            uwErrors += (String) mUwErrorList.get(i) + "\n";
        }
        return uwErrors;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit() {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

//    /**
//     * 对险种进行核保
//     */
//    private boolean autoUWPols() {
//        boolean flag = true;
//        LPPolDB tLPPolDB = new LPPolDB();
//        tLPPolDB.setEdorNo(mEdorNo);
//        LPPolSet tLPPolSet = tLPPolDB.query();
//        for (int i = 1; i <= tLPPolSet.size(); i++) {
//            if (!autoUWOnePol(tLPPolSet.get(i))) {
//                flag = false;
//            }
//        }
//        return flag;
//    }

//    /**
//     * 对一个险种进行核保
//     * @param tLPPolSchema LPPolSchema
//     */
//    private boolean autoUWOnePol(LPPolSchema aLPPolSchema) {
//        LCPolSchema tLCPolSchema = CommonBL.getLCPol(aLPPolSchema.getPolNo());
//        RuleTransfer ruleTransfer = new RuleTransfer();
//        LMUWSet tLMUWSet = ruleTransfer.getAllPolUWMSG(this.mDoc);
//
//        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
//        tLPEdorItemDB.setEdorNo(aLPPolSchema.getEdorNo());
//        tLPEdorItemDB.setEdorType(aLPPolSchema.getEdorType());
//        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
//        LMUWSet tLMUWSetPolUnpass = ruleTransfer.getPolUWMSG(tLMUWSet, tLCPolSchema.getRiskCode());
//        if (tLPEdorItemSet.size() == 0) {
//            mErrors.addOneError("没有查询到保全项目信息" + aLPPolSchema.getEdorNo()
//                                + "," + aLPPolSchema.getEdorType());
//            return false;
//        }
//
//        setUWError(tLMUWSetPolUnpass, aLPPolSchema);
//        mUwErrorList.add(this.mMessage);
//        return true;
//    }

    /**
     * 记录自核错误信息
     * @param error String
     * @param aLPPolSchema LPPolSchema
     */
    private void setUWError(LMUWSchema tLMUWSchema, LPPolSchema aLPPolSchema) {

            LPUWErrorSchema tLPUWErrorSchema = new LPUWErrorSchema();
            tLPUWErrorSchema.setEdorNo(aLPPolSchema.getEdorNo());
            tLPUWErrorSchema.setEdorType(aLPPolSchema.getEdorType());
            tLPUWErrorSchema.setGrpContNo(aLPPolSchema.getGrpContNo());
            tLPUWErrorSchema.setContNo(aLPPolSchema.getContNo());
            tLPUWErrorSchema.setProposalContNo(aLPPolSchema.getProposalNo());
            tLPUWErrorSchema.setPolNo(aLPPolSchema.getPolNo());
            tLPUWErrorSchema.setProposalNo(aLPPolSchema.getPolNo());
            tLPUWErrorSchema.setSerialNo("1"); //对自核来说SerialNo没有意义
            tLPUWErrorSchema.setInsuredNo(aLPPolSchema.getInsuredNo());
            tLPUWErrorSchema.setInsuredName(aLPPolSchema.getInsuredName());
            tLPUWErrorSchema.setAppntNo(aLPPolSchema.getAppntNo());
            tLPUWErrorSchema.setAppntName(aLPPolSchema.getAppntName());
            tLPUWErrorSchema.setManageCom(mGlobalInput.ManageCom);
            tLPUWErrorSchema.setUWError(tLMUWSchema.getRemark());
            tLPUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
            tLPUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
            tLPUWErrorSchema.setUWPassFlag(BQ.UWFLAG_FAIL); //核保未通过
            mMap.put(tLPUWErrorSchema, "DELETE&INSERT");
    }    
    /**
     * 锁定动作
     * @param aNoLimit 工单号
     * @param aNoType 类型
     * @param aAIS 锁定有效时间（秒）
     * @return MMap
     */
    private MMap lockBQUW(String aNoLimit,String aNoType, String aAIS)
    {
        MMap tMMap = null;
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", aNoLimit);
        tTransferData.setNameAndValue("LockNoType", aNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", aAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }

}
