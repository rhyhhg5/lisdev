package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单复效项目明细</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class PEdorFXDetailBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //复效日期
    private String edorAppDate;
    //利率
    private String rates;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = null;
    private LPEdorItemSchema mLPEdorItemSchema_in = null;
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LPPolSet mLPPolSet = null;
//    private LPPolSchema mMainLPPolSchema = null;
    private LPInsureAccSchema mLPInsureAccSchema = null;
    private LPInsureAccClassSchema mLPInsureAccClassSchema = null;
    private LPDutySet mLPDutySet = null;
    private LPPremSet mLPPremSet = null;
    private boolean mUARFlag = false;
    private LPPolSet mAllattchpolSet = null;
    private LPEdorEspecialDataSchema[] mLPEdorEspecialDataSchema = null;

    public PEdorFXDetailBL()
    {

    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if(!getInputData())
        {
            return false;
        }
        System.out.print("getinput end");
        if(!checkData())
        {
            return false;
        }
        System.out.print("checkdata end");
        //进行业务处理
        if(!dealData())
        {
            return false;
        }
        System.out.print("dealdata end");
        //准备往后台的数据
        if(!prepareData())
        {
            return false;
        }

        PubSubmit tSubmit = new PubSubmit();
        if(!tSubmit.submitData(mResult, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorFXDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            mGlobalInput = (GlobalInput) mInputData
                           .getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema_in = (LPEdorItemSchema) mInputData.
                                   getObjectByObjectName("LPEdorItemSchema", 0);
            mLPPolSet = (LPPolSet) mInputData.
                        getObjectByObjectName("LPPolSet", 0);
//          mLPPENoticeSet = (LPPENoticeSet) mInputData.getObjectByObjectName("LPPENoticeSet", 0);
//          mLPEdorEspecialDataSchema = (LPEdorEspecialDataSchema)mInputData.getObjectByObjectName("LPEdorEspecialDataSchema", 0);
            LPEdorEspecialDataSchema[] tLPEdorEspecialDataSchema = new
                LPEdorEspecialDataSchema[2];
            TransferData tTransferData = (TransferData) mInputData.
                                         getObjectByObjectName("TransferData",
                0);

            tLPEdorEspecialDataSchema[0] = new LPEdorEspecialDataSchema();
            tLPEdorEspecialDataSchema[0].setEdorNo(mLPEdorItemSchema_in.
                getEdorNo());

            tLPEdorEspecialDataSchema[0].setEdorAcceptNo(mLPEdorItemSchema_in.
                getEdorAcceptNo());

            tLPEdorEspecialDataSchema[0].setEdorType(mLPEdorItemSchema_in.
                getEdorType());
            tLPEdorEspecialDataSchema[0].setPolNo(BQ.FILLDATA);
            tLPEdorEspecialDataSchema[0].setDetailType("FX_R");

            tLPEdorEspecialDataSchema[0].setEdorValue((String) tTransferData.
                getValueByName("rates"));
            tLPEdorEspecialDataSchema[1] = new LPEdorEspecialDataSchema();
            tLPEdorEspecialDataSchema[1].setEdorAcceptNo(mLPEdorItemSchema_in.
                getEdorAcceptNo());
            tLPEdorEspecialDataSchema[1].setEdorNo(mLPEdorItemSchema_in.
                getEdorNo());
            tLPEdorEspecialDataSchema[1].setEdorType(mLPEdorItemSchema_in.
                getEdorType());
            tLPEdorEspecialDataSchema[1].setPolNo(BQ.FILLDATA);
            tLPEdorEspecialDataSchema[1].setDetailType("FX_D");
            tLPEdorEspecialDataSchema[1].setEdorValue((String) tTransferData.
                getValueByName("edorAppDate"));
            mLPEdorEspecialDataSchema = tLPEdorEspecialDataSchema;
            System.out.print(mLPEdorEspecialDataSchema);
            for(int i = 0; i < 2; i++)
            {
                if(mLPEdorEspecialDataSchema[i].getDetailType().equals("FX_R"))
                    rates = mLPEdorEspecialDataSchema[i].getEdorValue();
                else
                    edorAppDate = mLPEdorEspecialDataSchema[i].getEdorValue();
            }
        }
        catch(Exception e)
        {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorFXDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if(mGlobalInput == null || mLPEdorItemSchema_in == null)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorFXDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "输入数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行校验处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean checkData()
    {
        DisabledManageBL tDisabledManageBL = new DisabledManageBL();
        if(!tDisabledManageBL.dealDisabledpol(mLPPolSet,
                                              mLPEdorItemSchema_in.getEdorType()))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorFXDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = tDisabledManageBL.getInsuredno() +
                                  "号被保人投报险种'" + tDisabledManageBL.getRiskcode() +
                                  "'下FX项目在"
                                  + CommonBL.getCodeName("stateflag",
                tDisabledManageBL.getState()) + "状态下不能添加!";
            this.mErrors.addOneError(tError);
            return false;

        }
        
        //20080813 zhanggm 终止缴费的险种不能复效
        //20081110 zhanggm 终止缴费的险种可以复效
        /*
        if(!tDisabledManageBL.checkPolState(mLPPolSet,mLPEdorItemSchema_in.getEdorType()))
        {
        	CError tError = new CError();
        	tError.moduleName = "PEdorFXDetailBL";
        	tError.functionName = "checkData";
        	tError.errorMessage = tDisabledManageBL.getInsuredno() +
        	    "号被保人投保险种'" + tDisabledManageBL.getRiskcode() +
        	    "'已经是续保终止或缴费终止状态，不能做复效！";
        	this.mErrors.addOneError(tError);
        	return false;
        }
        */

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        String reasonCode = mLPEdorItemSchema_in.getReasonCode();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema_in.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema_in.getEdorType());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema_in.getContNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();

        if(tLPEdorItemSet == null || tLPEdorItemSet.size() < 1)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorFXDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "输入数据有误,LPEdorItem中没有相关数据!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLPEdorItemSchema = tLPEdorItemSet.get(1);
        mLPEdorItemSchema.setEdorState("1");
        mLPEdorItemSchema.setReasonCode(reasonCode);

        if(mLPEdorItemSchema.getEdorState().trim().equals("2"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorFXDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保全已经理算确认不能修改!";
            this.mErrors.addOneError(tError);
            return false;
        }

        for(int i = 1; i <= mLPPolSet.size(); i++)
        {
            LPEdorItemSchema tmLPEdorItemSchema = new LPEdorItemSchema();
            tmLPEdorItemSchema.setSchema(mLPEdorItemSchema);
            tmLPEdorItemSchema.setPolNo(mLPPolSet.get(i).getPolNo());

            LPPolSchema schema = getLPPol(mLPPolSet.get(i));
            if(schema == null)
            {
                return false;
            }

            mLPPolSet.get(i).setSchema(schema);
            
            if("231001".equals(schema.getRiskCode()) || "231201".equals(schema.getRiskCode()) || "231801".equals(schema.getRiskCode())){
            	
            	mUARFlag = true;
//            	mMainLPPolSchema = getMainLPPol(schema);
            	mAllattchpolSet = getAllpolSet(schema);
            	mLPInsureAccSchema = getLPInsureAcc(schema);
            	mLPInsureAccClassSchema = getLPInsureAccClass(schema);
            	
            	
            }
        }


        if(rates == null || rates.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorFXDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "输入数据有误,没有利率信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //复效申请日不能小于欠费应收日
        System.out.print(mLPPolSet.size());
        for(int i = 1; i <= this.mLPPolSet.size(); i++)
        {
            LPPolSchema schema = mLPPolSet.get(i);

            String sql = "select PayToDate from LCPol where PayToDate >'"
                         + edorAppDate
                         + "' and polno = '" + schema.getPolNo() + "'";
            String temp = new ExeSQL().getOneValue(sql);
            if(temp != null && !temp.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "PEdorFXDetailBL";
                tError.functionName = "checkData";
                tError.errorMessage = "复效生效日不能小于险种交至日："
                                      + schema.getInsuredName() + "，"
                                      + schema.getRiskCode();
                this.mErrors.addOneError(tError);
                return false;
            }

            sql = "select 1 from LCPol "
                  + "where PolNo = '" + schema.getPolNo() + "' "
                  + "   and EndDate < '" + edorAppDate + "' ";
            temp = new ExeSQL().getOneValue(sql);
            if(temp != null && !temp.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "PEdorFXDetailBL";
                tError.functionName = "checkData";
                tError.errorMessage = "复效生效日不能大于险种满期日："
                                      + schema.getInsuredName() + "，"
                                      + schema.getRiskCode();
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        //验证健康录入
        ExeSQL tExeSQL = new ExeSQL();
        for(int i = 1; i <= mLPPolSet.size(); i++)
        {
            StringBuffer sql = new StringBuffer(128);
            sql.append(
                "select 1 from LPCustomerImpart where EdorType ='FX' and EdorNo ='")
                .append(mLPEdorItemSchema_in.getEdorAcceptNo())
                .append("' and CustomerNoType = '1' ")
                .append("and CustomerNo ='")
                .append(mLPPolSet.get(i).getInsuredNo())
                .append("'")
                ;
            String resu = tExeSQL.getOneValue(sql.toString());
            if(resu == null || resu.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "PEdorFXDetailBL";
                tError.functionName = "checkData";
                tError.errorMessage = "复效险种有被保人健康告知未录入!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        return true;
    }

    /**
     * 查询相应的险种信息
     * @param tLPPolSchema LPPolSchema
     * @return LPPolSchema
     */
    private LPPolSchema getLPPol(LPPolSchema tLPPolSchema)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tLPPolSchema.getPolNo());
        if(!tLCPolDB.getInfo())
        {
            mErrors.addOneError("查询险种信息出错");
            return null;
        }

        //交换LPPolSchema 和 LCPolSchema的数据
        LPPolSchema aLPPolSchema = new LPPolSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(aLPPolSchema, tLCPolDB.getSchema());

        aLPPolSchema.setEdorNo(tLPPolSchema.getEdorNo());
        aLPPolSchema.setEdorType(tLPPolSchema.getEdorType());

        return aLPPolSchema;
    }
    
    /**
     * 查询相应的险种信息
     * @param tLPPolSchema LPPolSchema
     * @return LPPolSchema
     */
    private LPPolSchema getMainLPPol(LPPolSchema tLPPolSchema)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tLPPolSchema.getMainPolNo());
        tLCPolDB.setContNo(tLPPolSchema.getContNo());
        if(!tLCPolDB.getInfo())
        {
            mErrors.addOneError("查询险种信息出错");
            return null;
        }

        //交换LPPolSchema 和 LCPolSchema的数据
        LPPolSchema aLPPolSchema = new LPPolSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(aLPPolSchema, tLCPolDB.getSchema());

        aLPPolSchema.setEdorNo(tLPPolSchema.getEdorNo());
        aLPPolSchema.setEdorType(tLPPolSchema.getEdorType());

        return aLPPolSchema;
    }
    
    
    
    /**
     * 查询相应的险种信息
     * @param tLPPolSchema LPPolSchema
     * @return LPPolSchema
     */
    private LPPolSet getAllpolSet(LPPolSchema tLPPolSchema)
    {
    	LCPolDB tLCPolDB = new LCPolDB();
    	tLCPolDB.setPolNo(tLPPolSchema.getMainPolNo());
    	tLCPolDB.setContNo(tLPPolSchema.getContNo());
    	LPPolSet tLPPolSet = new LPPolSet();
    	if(!tLCPolDB.getInfo())
    	{
    		mErrors.addOneError("查询险种信息出错");
    		return null;
    	}
    	LPPolSchema aLPPolSchema = new LPPolSchema();
    	Reflections tReflections = new Reflections();
    	tReflections.transFields(aLPPolSchema, tLCPolDB.getSchema());
    	
    	aLPPolSchema.setEdorNo(tLPPolSchema.getEdorNo());
    	aLPPolSchema.setEdorType(tLPPolSchema.getEdorType());
    	tLPPolSet.add(aLPPolSchema);
    	//交换LPPolSchema 和 LCPolSchema的数据
    	LCPolDB ttLCPolDB = new LCPolDB();
    	LCPolSet tLCPolSet = new LCPolSet();
    	//查询保单主险的附加险
    	String querySQL= " select * from lcpol where contno = '"+tLPPolSchema.getContNo()+"' and riskcode in ( " +
    			"select code from ldcode1 where codetype='mainsubriskrela' and code1='"+tLCPolDB.getRiskCode()+"') with ur";
    	tLCPolSet = ttLCPolDB.executeQuery(querySQL);
    	for(int i=1;i<=tLCPolSet.size();i++){
    		LPPolSchema ttLPPolSchema = new LPPolSchema();
    		Reflections ttReflections = new Reflections();
    		ttReflections.transFields(ttLPPolSchema, tLCPolSet.get(i).getSchema());
    		
    		ttLPPolSchema.setEdorNo(tLPPolSchema.getEdorNo());
    		ttLPPolSchema.setEdorType(tLPPolSchema.getEdorType());
    		
    		tLPPolSet.add(ttLPPolSchema);
    		
    	}
    	
    	return tLPPolSet;
    }
    
    
    /**
     * 
     * @param tLPPolSchema
     * @return
     */
    private LPInsureAccSchema getLPInsureAcc(LPPolSchema tLPPolSchema)
    {
    	LCInsureAccDB LCInsureAccDB = new LCInsureAccDB();
    	LCInsureAccDB.setContNo(tLPPolSchema.getContNo());
    	LCInsureAccSet tLCInsureAccSet = LCInsureAccDB.query();
        if(tLCInsureAccSet == null || tLCInsureAccSet.size() < 1)
        {
        	 mErrors.addOneError("查询账户信息出错");
             return null;
        }

        //交换LPInsureAccSchema 和 LCInsureAccSchema的数据
        LPInsureAccSchema aLPInsureAccSchema = new LPInsureAccSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(aLPInsureAccSchema, tLCInsureAccSet.get(1).getSchema());

        aLPInsureAccSchema.setEdorNo(tLPPolSchema.getEdorNo());
        aLPInsureAccSchema.setEdorType(tLPPolSchema.getEdorType());

        return aLPInsureAccSchema;
    }
    
    /**
     * 
     * @param tLPPolSchema
     * @return
     */
    private LPInsureAccClassSchema getLPInsureAccClass(LPPolSchema tLPPolSchema)
    {
    	LCInsureAccClassDB LCInsureAccClassDB = new LCInsureAccClassDB();
    	LCInsureAccClassDB.setContNo(tLPPolSchema.getContNo());
    	LCInsureAccClassSet tLCInsureAccClassSet = LCInsureAccClassDB.query();
        if(tLCInsureAccClassSet == null || tLCInsureAccClassSet.size() < 1)
        {
        	 mErrors.addOneError("查询账户信息出错");
             return null;
        }
    	

        //交换LPInsureAccSchema 和 LCInsureAccSchema的数据
        LPInsureAccClassSchema aLPInsureAccClassSchema = new LPInsureAccClassSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(aLPInsureAccClassSchema, tLCInsureAccClassSet.get(1).getSchema());

        aLPInsureAccClassSchema.setEdorNo(tLPPolSchema.getEdorNo());
        aLPInsureAccClassSchema.setEdorType(tLPPolSchema.getEdorType());

        return aLPInsureAccClassSchema;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if(!dealOtherLPInfo())
        {
            return false;
        }

        mLPEdorItemSchema.setEdorValiDate(edorAppDate);
        mLPEdorItemSchema.setEdorState("1");
        return true;
    }

    /**
     * dealOtherLPInfo
     * 生成其他LP表信息
     * @return boolean
     */
    private boolean dealOtherLPInfo()
    {
        String sql = "delete from LPCont "
                     + "where EdorNo = '" + mLPEdorItemSchema.getEdorNo() + "'"
                     + "   and EdorType = '"
                     + mLPEdorItemSchema.getEdorType() + "' "
                     + "   and ContNo = '"
                     + mLPEdorItemSchema.getContNo() + "' ";
        mMap.put(sql, SysConst.DELETE);
        
        String sqlLcpol = "delete from LPpol "
            + "where EdorNo = '" + mLPEdorItemSchema.getEdorNo() + "'"
            + "   and EdorType = '"
            + mLPEdorItemSchema.getEdorType() + "' "
            + "   and ContNo = '"
            + mLPEdorItemSchema.getContNo() + "' ";
        mMap.put(sqlLcpol, SysConst.DELETE);

        sql = "insert into LPCont (select '"
              + mLPEdorItemSchema.getEdorNo()
              + "', '" + mLPEdorItemSchema.getEdorType()
              + "', LCCont.* from LCCont "
              + " where ContNo='" + mLPEdorItemSchema.getContNo() + "')";
        mMap.put(sql, SysConst.INSERT);

        String[] tables =
            {"LCDuty", "LCPrem"};

        for(int i = 0; i < tables.length; i++)
        {
            sql = "delete from " + tables[i].replaceFirst("C", "P")
                  + " "
                  + "where EdorNo = '" + mLPEdorItemSchema.getEdorNo() + "'"
                  + "   and EdorType = '"
                  + mLPEdorItemSchema.getEdorType() + "' "
                  + "   and ContNo = '" + mLPEdorItemSchema.getContNo() + "' ";
            mMap.put(sql, SysConst.DELETE);

            sql = "insert into " + tables[i].replaceFirst("C", "P")
                  + " (select '" + mLPEdorItemSchema.getEdorNo()
                  + "', '" + mLPEdorItemSchema.getEdorType()
                  + "', " + tables[i] + ".* from " + tables[i] + " "
                  + " where ContNo='" + mLPEdorItemSchema.getContNo() + "')";
            mMap.put(sql, SysConst.DELETE);
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareData()
    {
        mMap.put(this.mLPEdorEspecialDataSchema[0], "DELETE&INSERT");
        mMap.put(this.mLPEdorEspecialDataSchema[1], "DELETE&INSERT");
        mMap.put(UpdateEdorState.getUpdateEdorStateSql(mLPEdorItemSchema),
                 "UPDATE");
        mMap.put(mLPPolSet, "DELETE&INSERT");
        mMap.put(mLPEdorItemSchema, "UPDATE");
		if(mUARFlag){
//			mMap.put(mMainLPPolSchema, "DELETE&INSERT");
			mMap.put(mAllattchpolSet, "DELETE&INSERT");
			mMap.put(mLPInsureAccSchema, "DELETE&INSERT");
			mMap.put(mLPInsureAccClassSchema, "DELETE&INSERT");
		}
        mResult.clear();
        mResult.add(mMap);

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        String edorNo = "20070302000004";
        String contNo = "00001098007";

        PEdorFXDetailBL test = new PEdorFXDetailBL();

        LPPolSet mLPPolSet = new LPPolSet();

        LPPolSchema tLPPolSchema = new LPPolSchema();
        tLPPolSchema.setPolNo("21000009994");
        mLPPolSet.add(tLPPolSchema);

        LPPolSchema tLPPolSchema2 = new LPPolSchema();
        tLPPolSchema2.setPolNo("21000009993");
        mLPPolSet.add(tLPPolSchema2);

        LPPolSchema tLPPolSchema3 = new LPPolSchema();
        tLPPolSchema3.setPolNo("21000009992");
        mLPPolSet.add(tLPPolSchema3);

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("rates", "0.027");
        tTransferData.setNameAndValue("edorAppDate", "2007-02-01");

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorAcceptNo(edorNo);
        tLPEdorItemSchema.setEdorNo(edorNo);
        tLPEdorItemSchema.setContNo(contNo);
        tLPEdorItemSchema.setEdorType("FX");
        VData tVData = new VData();
        GlobalInput t = new GlobalInput();
        t.ManageCom = "86";
        t.Operator = "pa001";
        tVData.add(t);
        tVData.add(tLPEdorItemSchema);
        tVData.add(mLPPolSet);
        tVData.add(tTransferData);
        test.submitData(tVData, "");
    }

}
