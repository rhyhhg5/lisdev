package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体特需医疗提前领取</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class GrpEdorZEAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = null;

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保全生效日期 */
    private String mEdorValiDate = null;

    /** 保全交费总额 */
    private double mGetMoney = 0.00;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mSpecialData = null;

    private DetailDataQuery mQuery = null;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mVtsData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mEdorNo = edorItem.getEdorNo();
        mEdorType = edorItem.getEdorType();
        mGrpContNo = edorItem.getGrpContNo();
        mEdorValiDate = edorItem.getEdorValiDate();
        mVtsData = new EdorItemSpecialData(edorItem);
        mSpecialData = new EdorItemSpecialData(edorItem);
        mSpecialData.query();
        mQuery = new DetailDataQuery(mEdorNo, mEdorType);
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!setInsuredAcc())
        {
            return false;
        }
        setEdorInfo();
        setGrpEdorItem();
        return true;
    }


    /**
     * 设置账户余额
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param getMoney double
     */
    private void setLPInsureAcc(LCInsureAccSchema aLCInsureAccSchema, double money, double leftMoney)
    {
        double sumPaym = aLCInsureAccSchema.getSumPaym() + money; 
        LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPInsureAccSchema, aLCInsureAccSchema);
        tLPInsureAccSchema.setEdorNo(mEdorNo);
        tLPInsureAccSchema.setEdorType(mEdorType);
        tLPInsureAccSchema.setInsuAccBala(leftMoney);
        tLPInsureAccSchema.setSumPaym(sumPaym);
        tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccSchema.setModifyDate(mCurrentDate);
        tLPInsureAccSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
    }

    /**
     * 设置帐户领取轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTrace(LCInsureAccSchema aLCInsureAccSchema, double grpMoney)
    {
        String serialNo;
        LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
        tLPInsureAccTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccTraceDB.setEdorType(mEdorType);
        tLPInsureAccTraceDB.setGrpContNo(mGrpContNo);
        tLPInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
        if (tLPInsureAccTraceSet.size() > 0)
        {
            serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccTrace " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        }
        else
        {
            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        }
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(BQ.FILLDATA);
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(BQ.FEEFINATYPE_TF);
        tLPInsureAccTraceSchema.setMoney(-Math.abs(grpMoney));  //退费
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }


    /**
     * 设置管理费分类 轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param manageMoney double
     * @param feeRate double
     */
    private void setLPInsureAccClassFee(LCInsureAccSchema aLCInsureAccSchema, double manageMoney, double feeRate)
    {
        LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
        tLPInsureAccClassFeeSchema.setEdorNo(mEdorNo);
        tLPInsureAccClassFeeSchema.setEdorType(mEdorType);
        tLPInsureAccClassFeeSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccClassFeeSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccClassFeeSchema.setPayPlanCode(BQ.FILLDATA);
        tLPInsureAccClassFeeSchema.setOtherNo(mEdorNo);
        tLPInsureAccClassFeeSchema.setOtherType("3"); //3是保全
        tLPInsureAccClassFeeSchema.setAccAscription("0");
        tLPInsureAccClassFeeSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
        tLPInsureAccClassFeeSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccClassFeeSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccClassFeeSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccClassFeeSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccClassFeeSchema.setInsuredNo(aLCInsureAccSchema.getInsuredNo());
        tLPInsureAccClassFeeSchema.setAppntNo(aLCInsureAccSchema.getAppntNo());
        tLPInsureAccClassFeeSchema.setAccType(aLCInsureAccSchema.getAccType());
        tLPInsureAccClassFeeSchema.setAccComputeFlag(aLCInsureAccSchema.getAccComputeFlag());
        tLPInsureAccClassFeeSchema.setAccFoundDate(aLCInsureAccSchema.getAccFoundDate());
        tLPInsureAccClassFeeSchema.setAccFoundTime(aLCInsureAccSchema.getAccFoundTime());
        tLPInsureAccClassFeeSchema.setBalaDate(aLCInsureAccSchema.getBalaDate());
        tLPInsureAccClassFeeSchema.setBalaTime(aLCInsureAccSchema.getBalaTime());
        tLPInsureAccClassFeeSchema.setFeeRate(feeRate);
        tLPInsureAccClassFeeSchema.setFee(manageMoney);
        tLPInsureAccClassFeeSchema.setFeeUnit(0);
        tLPInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccClassFeeSchema.setMakeDate(mCurrentDate);
        tLPInsureAccClassFeeSchema.setMakeTime(mCurrentTime);
        tLPInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
        tLPInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccClassFeeSchema, "DELETE&INSERT");
    }

    /**
     * 设置个人账户减少保险金额
     * @return boolean
     */
    private boolean setInsuredAcc()
    {
        LCGrpPolSet tLCGrpPolSet = CommonBL.getLCGrpPolSet(mGrpContNo);
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            String riskcode = tLCGrpPolSet.get(i).getRiskCode();

            String grpPolNo = tLCGrpPolSchema.getGrpPolNo();
            String riskSeqNo = tLCGrpPolSchema.getRiskSeqNo();
            LPDiskImportSet tLPDiskImportSet = CommonBL.getLPDiskImportSet
                    (mEdorNo, mEdorType, mGrpContNo, null,
                    BQ.IMPORTSTATE_SUCC);
            double insuredGetMoney = 0.0;
            for (int j = 1; j <= tLPDiskImportSet.size(); j++)
            {
                LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(j);
                String insuredNo = tLPDiskImportSchema.getInsuredNo();
                LCInsureAccSchema tLCInsureAccSchema =
                        CommonBL.getLCInsureAcc(grpPolNo, insuredNo,
                        BQ.ACCTYPE_INSURED);
                if (tLCInsureAccSchema == null)
                {
                    mErrors.addOneError("找不到客户" + insuredNo + "的个人账户信息！");
                    return false;
                }
                double insuredMoney = tLPDiskImportSchema.getMoney();
                double leftMoney = tLCInsureAccSchema.getInsuAccBala() - insuredMoney; //计算余额
                double getMoney = insuredMoney;  //计算实领金额

                if (leftMoney < 0)
                {
                    mErrors.addOneError("被保人" + tLCInsureAccSchema.getInsuredNo() + "的保险金额不足！");
                    return false;
                }
                LCPolSchema tLCPolSchema = CommonBL.getLCPol(grpPolNo,
                        insuredNo);
                if (tLCPolSchema == null)
                {
                    mErrors.addOneError("找不到客户" + insuredNo + "的险种信息！");
                    return false;
                }
                setLPInsureAcc(tLCInsureAccSchema, insuredMoney, leftMoney);
                setLPInsureAccTrace(tLCInsureAccSchema, insuredMoney);
                setGetEndorse(tLCPolSchema, getMoney);
                mGetMoney += getMoney;
                insuredGetMoney += insuredMoney;
                setDiskImport(tLPDiskImportSchema, leftMoney, getMoney);
            }
            //设置批单显示数据
            mVtsData.setGrpPolNo(grpPolNo);
            mVtsData.add("InsuredGetMoney", CommonBL.bigDoubleToCommonString(insuredGetMoney, "0.00"));
        }
        return true;
    }
    

    /**
     * 保存要在批单中显示的数据
     */
    private void setEdorInfo() {
        mMap.put(mVtsData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     * 设置批改补退费表
     * @return boolean
     */
    private boolean setGetEndorse(LCPolSchema aLCPolSchema, double edorPrem)
    {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGrpContNo(mGrpContNo);
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
        tLJSGetEndorseSchema.setGetMoney(-Math.abs(edorPrem));
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_TF);
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 设置item表中的费用和状态
     */
    private void setGrpEdorItem()
    {
        System.out.println(mGetMoney);
        String sql = "update LPGrpEdorItem " +
                "set GetMoney = -" + mGetMoney + ", " + //这里是负数
                "EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                "ModifyDate = '" + mCurrentDate + "', " +
                "ModifyTime = '" + mCurrentTime + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql.toString(), "UPDATE");
    }

    /**
     * 设置LPDiskImport中显示的信息
     */
    private void setDiskImport(LPDiskImportSchema tLPDiskImportSchema, double leftMoney, double getMoney)
    {
        tLPDiskImportSchema.setMoney2(String.valueOf(getMoney));
        tLPDiskImportSchema.setGetMoney(leftMoney);
        tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
        tLPDiskImportSchema.setModifyDate(mCurrentDate);
        tLPDiskImportSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPDiskImportSchema, "DELETE&INSERT");
    }

 }
