package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 生成需要修改理赔金帐户的被保人清单
 * 若险种产生过理赔，则退费为0
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class PrtAppAccGetBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;
    private LCAppAccTraceSchema mLCAppAccTraceSchema = null;
    private LCAppAccGetTraceSchema mLCAppAccGetTraceSchema=null;
    private XmlExport xmlexport = null;
    private TextTag mTag = new TextTag();
    private VData mResult = null;
    ListTable mtListTable = new ListTable();
    private String mOperate=null;
    public PrtAppAccGetBL()
    {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate=cOperate;
        if(!getInputData(cInputData))
        {
            return false;
        }

        //校验数据合法性
        if(!checkData())
        {
            return false;
        }

        //获取打印所需数据
        if(!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mTransferData = (TransferData) cInputData.
                            getObjectByObjectName("TransferData", 0);
            mLCAppAccTraceSchema = (LCAppAccTraceSchema) cInputData.
                               getObjectByObjectName("LCAppAccTraceSchema", 0);
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }

        return true;
    }

    //获取打印所需数据
    private boolean getPrintData()
    {

        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PrtAppAccGet.vts", "printer"); //最好紧接着就初始化xml文档

        if(!getCustomerInfo())
        {
            return false;
        }

        setFixedInfo(mLCAppAccGetTraceSchema.getOperator());
        mTag.add("CustomerNo", mLCAppAccGetTraceSchema.getCustomerNo());
        mTag.add("ConfirmDate", CommonBL.decodeDate(mLCAppAccGetTraceSchema.getAccGetDate()));
        mTag.add("AppDate", CommonBL.decodeDate(mLCAppAccGetTraceSchema.getAppDate()));

        mTag.add("AccBala", mLCAppAccGetTraceSchema.getAccBala());
        mTag.add("AccGetMoney", mLCAppAccGetTraceSchema.getAccGetMoney());
        mTag.add("AccGetName", mLCAppAccGetTraceSchema.getName());
        mTag.add("AccGetIDNo", mLCAppAccGetTraceSchema.getIDNo());
        mTag.add("NoticeNo", mLCAppAccGetTraceSchema.getNoticeNo());
        mTag.add("BarCode1", mLCAppAccGetTraceSchema.getNoticeNo()); //受理号
        mTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        mTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));

        if(mLCAppAccGetTraceSchema.getAccGetMode().equals("4"))
        {
            xmlexport.addDisplayControl("displayTransfer");
            mTag.add("BankName", mLCAppAccGetTraceSchema.getBankCode());
            mTag.add("AccNo", mLCAppAccGetTraceSchema.getBankAccNo());
            mTag.add("AccName", mLCAppAccGetTraceSchema.getAccName());
            mTag.add("AccTransferDate", mLCAppAccGetTraceSchema.getTransferDate());
        }else
        {
            xmlexport.addDisplayControl("displayCash");
        }
        xmlexport.addTextTag(mTag);
        xmlexport.outputDocumentToFile("C:\\", "PrtAppAccListBL");

        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }


    private void setFixedInfo(String operator)
    {
        //查询受理机构
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(operator);
        tLDUserDB.getInfo();

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLDUserDB.getComCode());
        tLDComDB.getInfo();

        mTag.add("Operator", tLDUserDB.getUserName());
        mTag.add("ServicePhone", tLDComDB.getServicePhone());
        mTag.add("ServiceFax", tLDComDB.getFax());
        mTag.add("ServiceAddress", tLDComDB.getLetterServicePostAddress());
        mTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        mTag.add("ComName", tLDComDB.getLetterServiceName());
        mTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
    }

    //查询客户名称
    private boolean getCustomerInfo()
    {
        if(mOperate.equals("0"))
        {
            LCAddressSchema tLCAddressSchema = null;
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(mLCAppAccTraceSchema.getCustomerNo());
            if (!tLDPersonDB.getInfo()) {
                mErrors.addOneError("没有查询到客户信息。");
                return false;
            }
            mTag.add("CustomerName", tLDPersonDB.getName());

            LCAddressDB tLCAddressDB = new LCAddressDB();
            LCAddressSet tLCAddressSet = new LCAddressSet();
            String tSql = "select * from LCAddress "
                          + "where customerNo='" +
                          mLCAppAccTraceSchema.getCustomerNo() + "' "
                          + "and int(addressNo) =(select max(int(addressNo)) "
                          + "from LCAddress "
                          + "where customerNo='" +
                          mLCAppAccTraceSchema.getCustomerNo() + "')";

            tLCAddressSet = tLCAddressDB.executeQuery(tSql);
            if (tLCAddressDB.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCAddressDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrtAppAccGetBL";
                tError.functionName = "getCustomerInfo";
                tError.errorMessage = "查询地址信息出错!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLCAddressSet.size() > 0) {
                tLCAddressSchema = tLCAddressSet.get(1);
                mTag.add("AppZipCode", tLCAddressSchema.getZipCode());
                mTag.add("AppAddress", tLCAddressSchema.getPostalAddress());
                mTag.add("AppName", tLDPersonDB.getName());
            } else {
                CError tError = new CError();
                tError.moduleName = "PrtAppAccGetBL";
                tError.functionName = "getCustomerInfo";
                tError.errorMessage = "查询地址信息无记录!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
        }else if(mOperate.equals("1"))
        {
            LCGrpAddressSchema tLCGrpAddressSchema = null;
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(mLCAppAccTraceSchema.getCustomerNo());
            if (!tLDGrpDB.getInfo()) {
                mErrors.addOneError("没有查询到客户信息。");
                return false;
            }
            mTag.add("CustomerName", tLDGrpDB.getGrpName());

            LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
            LCGrpAddressSet tLCGrpAddressSet = new LCGrpAddressSet();
            String tSql = "select * from LCGrpAddress "
                          + "where customerNo='" +
                          mLCAppAccTraceSchema.getCustomerNo() + "' "
                          + "and int(addressNo) =(select max(int(addressNo)) "
                          + "from LCGrpAddress "
                          + "where customerNo='" +
                          mLCAppAccTraceSchema.getCustomerNo() + "')";

            tLCGrpAddressSet = tLCGrpAddressDB.executeQuery(tSql);
            if (tLCGrpAddressDB.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCGrpAddressDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrtAppAccGetBL";
                tError.functionName = "getCustomerInfo";
                tError.errorMessage = "查询地址信息出错!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLCGrpAddressSet.size() > 0) {
                tLCGrpAddressSchema = tLCGrpAddressSet.get(1);
                mTag.add("AppZipCode", tLCGrpAddressSchema.getGrpZipCode());
                mTag.add("AppAddress", tLCGrpAddressSchema.getGrpAddress());
                mTag.add("AppName", tLCGrpAddressSchema.getLinkMan1());
            } else {
                CError tError = new CError();
                tError.moduleName = "PrtAppAccGetBL";
                tError.functionName = "getCustomerInfo";
                tError.errorMessage = "查询地址信息无记录!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    private boolean checkData()
    {
        AppAcc tAppAcc=new AppAcc();
        mLCAppAccTraceSchema = tAppAcc.getLCAppAccTrace(mLCAppAccTraceSchema.getCustomerNo(),mLCAppAccTraceSchema.getSerialNo());
        if(mLCAppAccTraceSchema == null)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tAppAcc.mErrors);
            CError tError = new CError();
            tError.moduleName = "PrtAppAccGetBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询投保人帐户轨迹表时出错!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCAppAccGetTraceSchema = tAppAcc.getLCAppAccGetTrace(mLCAppAccTraceSchema.getCustomerNo(),mLCAppAccTraceSchema.getSerialNo());
        if(mLCAppAccGetTraceSchema == null)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tAppAcc.mErrors);
            CError tError = new CError();
            tError.moduleName = "PrtAppAccGetBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询投保人帐户领取轨迹表时出错!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }
}
