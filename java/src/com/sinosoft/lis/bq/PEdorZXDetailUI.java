package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorZXDetailUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public PEdorZXDetailUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    PEdorZXDetailBL tPEdorZXDetailBL = new PEdorZXDetailBL();
    if (tPEdorZXDetailBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tPEdorZXDetailBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "PEdorZXDetailUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
    {
        mResult = tPEdorZXDetailBL.getResult();
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

}
