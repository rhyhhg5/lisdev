package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.LJSGetSchema;
import com.sinosoft.lis.operfee.Cal330501Bonus;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSGetDrawSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.vschema.LMDutyGetAliveSet;
import java.util.*;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.db.LJSGetDB;
import com.sinosoft.lis.db.LMDutyGetAliveDB;
import com.sinosoft.lis.db.LJSGetDrawDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContStateSchema;
import com.sinosoft.lis.schema.LMDutyGetAliveSchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.utility.SysConst;

//程序名称：ExpirBenefitCalBL.java
//程序功能：常无忧B给付补费重算忠诚奖
//创建日期：2010-07-21 16:06
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class PEdorCBCalBL {

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData saveData = new VData();
    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    
    private String mFormula = null;
    
    private String mRateFlag = null;
    
    private String mPolNo = null;
    
    private double mBonus = 0.0;
    
    private LCPolSchema mLCPolSchema = new LCPolSchema();

    public PEdorCBCalBL() {
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	
    	System.out.println("in PEdorCBCalBL----------------");

        VData data = getSubmitData(cInputData, cOperate);
        if (data == null) 
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) {
            this.mErrors.addOneError("PubSubmit:生成给付失败!");
            return false;
        }
        return true;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public VData getSubmitData(VData cInputData, String cOperate) 
    {
        if (!getInputData(cInputData)) 
        {
            return null;
        }
        System.out.println("After PEdorCBCalBL getInputData");
        
        if (!checkData()) 
        {
            return null;
        }
        System.out.println("After PEdorCBCalBL checkData");
        
        //进行业务处理
        if (!dealData()) 
        {
            return null;
        }
        System.out.println("After PEdorCBCalBL dealData");
        
        return saveData;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) 
    {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0); 
        
        if ((tGI == null) || mTransferData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        mFormula = (String)mTransferData.getValueByName("formula");
        mRateFlag = (String)mTransferData.getValueByName("rateflag");
        mPolNo = (String)mTransferData.getValueByName("polno");
        
        if (mPolNo == null || mPolNo.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorCBCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到PolNo，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mFormula == null || mFormula.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorCBCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mFormula，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mRateFlag == null || mRateFlag.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorCBCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mRateFlag，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if(mFormula.equals("1"))
        {
          mFormula="A";
        }
         if(mFormula.equals("2"))
        {
          mFormula="B";
        }
         if(mFormula.equals("3"))
        {
          mFormula="C";
        }
         if(mFormula.equals("4"))
        {
          mFormula="D";
        }
         if(mFormula.equals("5"))
        {
          mFormula="E";
        }
        
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mPolNo);
        if(!tLCPolDB.getInfo())
        {
//        	 @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorCBCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "通过PolNo查询LCPol失败，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }

    	mLCPolSchema.setSchema(tLCPolDB.getSchema());
        return true;
    }

    /**
     *  数据校验
     */
    private boolean checkData() 
    {
        return true;
    }

    /**
     * 业务处理
     */
    private boolean dealData() 
    {
    	Cal330501Bonus t = new Cal330501Bonus(mLCPolSchema);
   	    HashMap hashMap3 = t.getBonus(mFormula,mRateFlag); //忠诚奖
   	    double tInitMoney = Double.parseDouble((String)hashMap3.get("InitMoney"));
   	    double tAddMoney = Double.parseDouble((String)hashMap3.get("AddMoney"));
   	    mBonus = tInitMoney + tAddMoney;
   	    return true;
    }
    
//  得到忠诚奖
    public String getBonus()
    {
    	return String.valueOf(mBonus);
    }
}
