package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PEdorPTConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCPolSet mLCPolSet = new LCPolSet();
    private LPPolSet mLPPolSet = new LPPolSet();
    private LPDutySet mLPDutySet = new LPDutySet();
    private LCDutySet mLCDutySet = new LCDutySet();
    private LPGetSet mLPGetSet = new LPGetSet();
    private LCGetSet mLCGetSet = new LCGetSet();
    private LPPremSet mLPPremSet = new LPPremSet();
    private LCPremSet mLCPremSet = new LCPremSet();

    public PEdorPTConfirmBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public void setOperate(String cOperate)
    {
        this.mOperate = cOperate;
    }

    public String getOperate()
    {
        return this.mOperate;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("PEdorPTConfirm------------");
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.setOperate(cOperate);

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        if (!dealData())
            return false;
        System.out.println("after dealData");

        if (!prepareOutData())
            return false;
        System.out.println("after prepareOutData");

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {
        boolean flag = true;
        return flag;

    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData()
    {
        System.out.println("start PEdorPTConfirmBL preapre....");
        int m;
        m = 0;

        Reflections tReflections = new Reflections();
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();

        LPPolSchema aLPPolSchema = new LPPolSchema();
        LCPolSchema aLCPolSchema = new LCPolSchema();

        LPPolSchema tLPPolSchema = new LPPolSchema();
        LCPolSchema tLCPolSchema = new LCPolSchema();

        LCPolSet tLCPolSet = new LCPolSet();
        LPPolSet tLPPolSet = new LPPolSet();

        LPDutySchema aLPDutySchema = new LPDutySchema();
        LCDutySchema aLCDutySchema = new LCDutySchema();

        LPDutySchema tLPDutySchema = new LPDutySchema();
        LCDutySchema tLCDutySchema = new LCDutySchema();

        LPGetSchema aLPGetSchema = new LPGetSchema();
        LCGetSchema aLCGetSchema = new LCGetSchema();

        LPGetSchema tLPGetSchema = new LPGetSchema();
        LCGetSchema tLCGetSchema = new LCGetSchema();

        LPPremSchema aLPPremSchema = new LPPremSchema();
        LCPremSchema aLCPremSchema = new LCPremSchema();

        LPPremSchema tLPPremSchema = new LPPremSchema();
        LCPremSchema tLCPremSchema = new LCPremSchema();

        //得到当前保全需要更新的保全数据。

        tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPPolSchema.setPolNo(mLPEdorItemSchema.getPolNo());//由于

        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setSchema(tLPPolSchema);

        tLPPolSet = tLPPolDB.query();
        m = tLPPolSet.size();
        System.out.println("tLPPolSet size is " + m);

        //针对picc 可能产生了多个减额的险种，将多个险种进行c表和p表互换 Modify by lanjun 2005-05-16
        for (int i = 1; i <= m; i++)
        {
            //转换险种信息。
            aLPPolSchema = tLPPolSet.get(i);
            tReflections.transFields(tLCPolSchema, aLPPolSchema);
            tLCPolSchema.setModifyDate(PubFun.getCurrentDate());
            tLCPolSchema.setModifyTime(PubFun.getCurrentTime());
            mLCPolSet.add(tLCPolSchema);

            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(aLPPolSchema.getPolNo());
            if (!tLCPolDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "PEdorPTConfirmBL";
                tError.functionName = "prepareData";
                tError.errorMessage = "查询保单号为aLPPolSchema.getPolNo()的保单失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            aLCPolSchema = tLCPolDB.getSchema();
            tLPPolSchema = new LPPolSchema();
            tReflections.transFields(tLPPolSchema, aLCPolSchema);
            tLPPolSchema.setEdorNo(aLPPolSchema.getEdorNo());
            tLPPolSchema.setEdorType(aLPPolSchema.getEdorType());
            mLPPolSet.add(tLPPolSchema);

            //Duty process
            tLPDutySchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPDutySchema.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPDutySchema.setPolNo(aLPPolSchema.getPolNo());

            LPDutyDB tLPDutyDB = new LPDutyDB();
            tLPDutyDB.setSchema(tLPDutySchema);
            LPDutySet tLPDutySet = tLPDutyDB.query();
            for (int j = 1; j <= tLPDutySet.size(); j++)
            {
                //转换责任信息。
                aLPDutySchema = tLPDutySet.get(j);
                tReflections.transFields(tLCDutySchema, aLPDutySchema);
                tLCDutySchema.setModifyDate(PubFun.getCurrentDate());
                tLCDutySchema.setModifyTime(PubFun.getCurrentTime());
                mLCDutySet.add(tLCDutySchema);

                LCDutyDB tLCDutyDB = new LCDutyDB();
                tLCDutyDB.setPolNo(aLPDutySchema.getPolNo());
                tLCDutyDB.setDutyCode(aLPDutySchema.getDutyCode());
                if (!tLCDutyDB.getInfo())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "PEdorPTConfirmBL";
                    tError.functionName = "prepareData";
                    tError.errorMessage = "查询LCDuty表失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                aLCDutySchema = tLCDutyDB.getSchema();
                tLPDutySchema = new LPDutySchema();
                tReflections.transFields(tLPDutySchema, aLCDutySchema);
                tLPDutySchema.setEdorNo(aLPDutySchema.getEdorNo());
                tLPDutySchema.setEdorType(aLPDutySchema.getEdorType());
                mLPDutySet.add(tLPDutySchema);
            }

            tLPGetSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPGetSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPGetSchema.setPolNo(aLPPolSchema.getPolNo());
            LPGetDB tLPGetDB = new LPGetDB();
            tLPGetDB.setSchema(tLPGetSchema);

            LPGetSet tLPGetSet = tLPGetDB.query();
            //m = tLPGetSet.size();

            for (int k = 1; k <= tLPGetSet.size(); k++)
            {

                aLPGetSchema = tLPGetSet.get(k);
                //转换领取信息。
                tReflections.transFields(tLCGetSchema, aLPGetSchema);
                tLCGetSchema.setModifyDate(PubFun.getCurrentDate());
                tLCGetSchema.setModifyTime(PubFun.getCurrentTime());
                mLCGetSet.add(tLCGetSchema.getSchema());

                LCGetDB tLCGetDB = new LCGetDB();
                tLCGetDB.setPolNo(aLPGetSchema.getPolNo());
                tLCGetDB.setDutyCode(aLPGetSchema.getDutyCode());
                tLCGetDB.setGetDutyCode(aLPGetSchema.getGetDutyCode());
                if (!tLCGetDB.getInfo())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "PEdorPTConfirmBL";
                    tError.functionName = "prepareData";
                    tError.errorMessage = "查询LCGet表失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                aLCGetSchema = tLCGetDB.getSchema();
                tLPGetSchema = new LPGetSchema();
                tReflections.transFields(tLPGetSchema, aLCGetSchema);
                tLPGetSchema.setEdorNo(aLPGetSchema.getEdorNo());
                tLPGetSchema.setEdorType(aLPGetSchema.getEdorType());
                mLPGetSet.add(tLPGetSchema.getSchema());
            }

            //Prem process
            tLPPremSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPPremSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPPremSchema.setPolNo(aLPPolSchema.getPolNo());
            LPPremDB tLPPremDB = new LPPremDB();
            tLPPremDB.setSchema(tLPPremSchema);
            LPPremSet tLPPremSet = tLPPremDB.query();
            //m = tLPPremSet.size();

            for (int n = 1; n <= tLPPremSet.size(); n++)
            {
                aLPPremSchema = tLPPremSet.get(n);

                //转换保费项信息。
                tReflections.transFields(tLCPremSchema, aLPPremSchema);
                tLCPremSchema.setModifyDate(PubFun.getCurrentDate());
                tLCPremSchema.setModifyTime(PubFun.getCurrentTime());
                mLCPremSet.add(tLCPremSchema.getSchema());

                LCPremDB tLCPremDB = new LCPremDB();
                tLCPremDB.setPolNo(aLPPremSchema.getPolNo());
                tLCPremDB.setDutyCode(aLPPremSchema.getDutyCode());
                tLCPremDB.setPayPlanCode(aLPPremSchema.getPayPlanCode());
                if (!tLCPremDB.getInfo())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "PEdorPTConfirmBL";
                    tError.functionName = "prepareData";
                    tError.errorMessage = "查询LCPrem表失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                aLCPremSchema = tLCPremDB.getSchema();
                tLPPremSchema = new LPPremSchema();
                tReflections.transFields(tLPPremSchema, aLCPremSchema);
                tLPPremSchema.setEdorNo(aLPPremSchema.getEdorNo());
                tLPPremSchema.setEdorType(aLPPremSchema.getEdorType());
                mLPPremSet.add(tLPPremSchema.getSchema());

            }

        }

        mLPEdorItemSchema.setEdorState("0");
        mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);

        return true;

    }

    private boolean prepareOutData()
    {
        map.put(mLPEdorItemSchema, "UPDATE");
        map.put(mLCPolSet, "UPDATE");
        map.put(mLPPolSet, "UPDATE");
        map.put(mLCDutySet, "UPDATE");
        map.put(mLPDutySet, "UPDATE");
        map.put(mLCPremSet, "UPDATE");
        map.put(mLPPremSet, "UPDATE");
        map.put(mLCGetSet, "UPDATE");
        map.put(mLPGetSet, "UPDATE");
        mResult.clear();
        mResult.add(map);
        return true;

    }

}
