package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCAppAccDB;

/**s
 * <p>Title: 续期预收保费</p>
 * <p>Description: 续期预收保费</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorYSDetailBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private AppAcc mAppAcc = new AppAcc();

    private LCAppAccTraceSchema mLCAppAccTraceSchema = null;

    private String mCustomerNo = null;

    private String mOtherNoType = null;

    private String mOtherNo = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData(VData data, String operator)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到磁盘导入数据
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            LPEdorItemSchema tLPEdorItemSchema = (LPEdorItemSchema)
                    cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
            mLCAppAccTraceSchema = (LCAppAccTraceSchema)
                    cInputData.getObjectByObjectName("LCAppAccTraceSchema", 0);
            mEdorNo = tLPEdorItemSchema.getEdorNo();
            mEdorType = tLPEdorItemSchema.getEdorType();
            mContNo = tLPEdorItemSchema.getContNo();
            mCustomerNo = mLCAppAccTraceSchema.getCustomerNo();
            mOtherNo = mLCAppAccTraceSchema.getOtherNo();
            mOtherNoType = mLCAppAccTraceSchema.getOtherType();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入参数错误！");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if(!checkDate())
        {
            return false;
        }

        String sql = "delete from LCAppAccTrace " +
                "where CustomerNo = '" + mCustomerNo + "' " +
                "and OtherNo = '" + mEdorNo + "' " +
                "and OtherType = '" + BQ.NOTICETYPE_P + "' " +
                "and State = '0'";
        mMap.put(sql, "DELETE");
        MMap map = mAppAcc.accShiftToXSYUJ(mLCAppAccTraceSchema);
//        mAppAcc.accConfirm(mCustomerNo, mOtherNo, mOtherNoType);
        mMap.add(map);
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
    }

    /**
     * 校验投保人是否有账户，若无责立即生成
     * @return boolean
     */
    private boolean checkDate()
    {
        if(mLCAppAccTraceSchema.getMoney() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorYSDetailBL";
            tError.functionName = "checkDate";
            tError.errorMessage = "请录入本次预交保费";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        LCAppAccDB tLCAppAccDB = new LCAppAccDB();
        tLCAppAccDB.setCustomerNo(mCustomerNo);
        if(tLCAppAccDB.query().size() == 0)
        {
            LCAppAccTraceSchema schema = new LCAppAccTraceSchema();
            schema.setCustomerNo(mCustomerNo);
            schema.setOtherNo("autoCreate");
            schema.setOtherNo("1");
            schema.setMoney(0);
            schema.setOperator("endor0");

            AppAcc tAppAcc = new AppAcc();
            MMap tMMap = tAppAcc.accShiftToXQY(schema, "0");

            VData data = new VData();
            data.add(tMMap);

            PubSubmit p = new PubSubmit();
            if(!p.submitData(data, ""))
            {
                CError tError = new CError();
                tError.moduleName = "PEdorYSDetailBL";
                tError.functionName = "checkAppAcc";
                tError.errorMessage = "投保人没有账户，且生成账户失败";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
        }

        return true;
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
        String sql = "update  LPEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "and ContNo = '" + mContNo + "'";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
