package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class EdorCancelForHasten implements EdorCancel
{
    private VData mResult = null;
    private GlobalInput mGlobalInput = null;
    private String mEdorAcceptNo = null;

    public EdorCancelForHasten()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }

        //删除保全项目, 删除批单, 删除申请
        if (!cancelItem(mEdorAcceptNo)
            || !cancelMain(mEdorAcceptNo)
            || !cancelApp(mEdorAcceptNo))
        {
            return false;
        }

        return true;
    }

    /**
     * getInputData
     * param VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.mGlobalInput = (GlobalInput) cInputData.
                                getObjectByObjectName("GlobalInput", 0);
            this.mEdorAcceptNo = (String) cInputData.
                                 getObjectByObjectName("String", 0);
        }
        catch(Exception e)
        {
            mErrors.addOneError("传给保全撤销程序的数据不完整");
            System.out.println("传给保全撤销程序的数据不完整 " + e.toString());

            return false;
        }

        return true;
    }

    private boolean cancelItem(String edorAcceptNo)
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        TransferData tTransferData = new TransferData();

        tLPEdorItemDB.setEdorAcceptNo(edorAcceptNo);
        LPEdorItemSet set = tLPEdorItemDB.query();
        tTransferData.setNameAndValue("DelReason", "且催缴过一次的保全收费过期");
        tTransferData.setNameAndValue("ReasonCode", "3");

        //I&EDORAPP,I&EDORMAIN,I&EDORITEM
        for(int i = 0; i < set.size(); i++)
        {
            VData tVData = new VData();
            tVData.addElement(this.mGlobalInput);
            tVData.addElement(set.get(i + 1));
            tVData.addElement(tTransferData);

            //调用保全撤销程序
            PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
            if(!tPGrpEdorCancelUI.submitData(tVData, "I&EDORITEM"))
            {
                this.mErrors.copyAllErrors(tPGrpEdorCancelUI.mErrors);
                System.out.println(this.mErrors.getErrContent());

                return false;
            }

        }

        return true;
    }

    /**
     * cancelMain
     *
     * @param mEdorAcceptNo String
     * @return boolean
     */
    private boolean cancelMain(String mEdorAcceptNo)
    {
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        TransferData tTransferData = new TransferData();

        tLPEdorMainDB.setEdorAcceptNo(mEdorAcceptNo);
        LPEdorMainSet set = tLPEdorMainDB.query();
        tTransferData.setNameAndValue("DelReason", "且催缴过一次的保全收费过期");
        tTransferData.setNameAndValue("ReasonCode", "3");

        //I&EDORAPP,I&EDORMAIN,I&EDORITEM
        for (int i = 0; i < set.size(); i++)
        {
            VData tVData = new VData();
            tVData.addElement(this.mGlobalInput);
            tVData.addElement(set.get(i + 1));
            tVData.addElement(tTransferData);

            //调用保全撤销程序
            PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
            if (!tPGrpEdorCancelUI.submitData(tVData, "I&EDORMAIN"))
            {
                this.mErrors.copyAllErrors(tPGrpEdorCancelUI.mErrors);
                System.out.println(this.mErrors.getErrContent());

                return false;
            }
        }

        return true;
    }

    /**
    * cancelApp
    *
    * @param mEdorAcceptNo String
    * @return boolean
    */
   private boolean cancelApp(String mEdorAcceptNo)
   {
       LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        TransferData tTransferData = new TransferData();

        tLPEdorAppDB.setEdorAcceptNo(this.mEdorAcceptNo);
        LPEdorAppSet set = tLPEdorAppDB.query();
        tTransferData.setNameAndValue("DelReason", "且催缴过一次的保全收费过期");
        tTransferData.setNameAndValue("ReasonCode", "3");

        //I&EDORAPP,I&EDORMAIN,I&EDORITEM
        for (int i = 0; i < set.size(); i++)
        {
            VData tVData = new VData();
            tVData.addElement(this.mGlobalInput);
            tVData.addElement(set.get(i + 1));
            tVData.addElement(tTransferData);

            //调用保全撤销程序
            PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
            if (!tPGrpEdorCancelUI.submitData(tVData, "I&EDORAPP"))
            {
                this.mErrors.copyAllErrors(tPGrpEdorCancelUI.mErrors);
                System.out.println(this.mErrors.getErrContent());

                return false;
            }
        }

        return true;
   }

   /**
    * 返回处理后的结果
    * */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        EdorCancelForHasten edorcancelforhasten = new EdorCancelForHasten();
    }
}
