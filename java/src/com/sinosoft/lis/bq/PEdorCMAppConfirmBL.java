package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全项目理算</p>
 * <p>Description: 客户资料变更</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @ReWrite ZhangRong, QiuYang
 * @version 1.0
 */
public class PEdorCMAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private VData mInputData = null;

    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到输入数据
     * @param cInputData VData
     * @return boolean
     */
    private void getInputData(VData cInputData)
    {
        mInputData = cInputData;
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.
                getObjectByObjectName("LPEdorItemSchema", 0);
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (isInsured())
        {
            if (!doInsuredEdor())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断判断保全变更的客户是否是被保人
     * @return boolean
     */
    private boolean isInsured()
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mLPEdorItemSchema.getContNo());
        tLCInsuredDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        if (tLCInsuredDB.query().size() == 0)
        {
            return false;
        }
        return true;
    }

    /**
     * 做被保人客户资料变更
     * @return boolean
     */
    private boolean doInsuredEdor()
    {
        PEdorICAppConfirmBL tPEdorICAppConfirmBL = new
                PEdorICAppConfirmBL();
        if (!tPEdorICAppConfirmBL.submitData(mInputData, ""))
        {
            mErrors.copyAllErrors(tPEdorICAppConfirmBL.mErrors);
            return false;
        }
        mMap.add((MMap) tPEdorICAppConfirmBL.getResult().
                getObjectByObjectName("MMap", 0));
        //直接利用理算完成的数据会有问题
        //同样也会造成payenddate错误的问题,添加lpprem,lpduty,lppol中的相关日期修改 add by xp 101014
        String tUpdateLCprem = " update lpprem a set (urgepayflag,paytimes,rate,needacc,payenddate) = (select urgepayflag,paytimes,rate,needacc,payenddate from lcprem where polno = a.polno and dutycode = a.dutycode and payplancode = a.payplancode) "
                     		   + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
                               ;
        String tUpdateLCget = " update lpget a set (summoney,baladate,state) = (select summoney,baladate,state from lcget where polno = a.polno and dutycode = a.dutycode and getdutycode = a.getdutycode) "
                               + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
                               ;
        String tUpdateLCpol = " update lppol a set payenddate = (select payenddate from lcpol where polno = a.polno) "
        					   + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
        					   ;
        String tUpdateLCduty = " update lpduty a set payenddate = (select payenddate from lcduty where polno = a.polno and dutycode=a.dutycode) "
			   + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
			   ;
        mMap.put(tUpdateLCget,"UPDATE");
        mMap.put(tUpdateLCpol,"UPDATE");
        mMap.put(tUpdateLCprem,"UPDATE");
        mMap.put(tUpdateLCduty,"UPDATE");
        return true;
    }
}
