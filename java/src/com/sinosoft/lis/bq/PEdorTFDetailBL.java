package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单特权复效项目明细</p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: Sinosoft</p>
 * @author Cz
 * @version 1.0
 */

import java.util.Date;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class PEdorTFDetailBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //复效日期
    private String edorAppDate;
    //利率
//    private String rates;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = null;
    private LPEdorItemSchema mLPEdorItemSchema_in = null;
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LPPolSet mLPPolSet = null;
    private LPEdorEspecialDataSchema[] mLPEdorEspecialDataSchema = null;

    public PEdorTFDetailBL()
    {

    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if(!getInputData())
        {
            return false;
        }
        System.out.print("getinput end");
        if(!checkData())
        {
            return false;
        }
        System.out.print("checkdata end");
        //进行业务处理
        if(!dealData())
        {
            return false;
        }
        System.out.print("dealdata end");
        //准备往后台的数据
        if(!prepareData())
        {
            return false;
        }

        PubSubmit tSubmit = new PubSubmit();
        if(!tSubmit.submitData(mResult, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorTFDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            mGlobalInput = (GlobalInput) mInputData
                           .getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema_in = (LPEdorItemSchema) mInputData.
                                   getObjectByObjectName("LPEdorItemSchema", 0);
            mLPPolSet = (LPPolSet) mInputData.
                        getObjectByObjectName("LPPolSet", 0);
            LPEdorEspecialDataSchema[] tLPEdorEspecialDataSchema = new
                LPEdorEspecialDataSchema[2];

            tLPEdorEspecialDataSchema[0] = new LPEdorEspecialDataSchema();
            tLPEdorEspecialDataSchema[0].setEdorNo(mLPEdorItemSchema_in.
                getEdorNo());

            tLPEdorEspecialDataSchema[0].setEdorAcceptNo(mLPEdorItemSchema_in.
                getEdorAcceptNo());

            tLPEdorEspecialDataSchema[0].setEdorType(mLPEdorItemSchema_in.
                getEdorType());
            tLPEdorEspecialDataSchema[0].setPolNo(BQ.FILLDATA);
            tLPEdorEspecialDataSchema[0].setDetailType("TF_R");

            tLPEdorEspecialDataSchema[1] = new LPEdorEspecialDataSchema();
            tLPEdorEspecialDataSchema[1].setEdorAcceptNo(mLPEdorItemSchema_in.
                getEdorAcceptNo());
            tLPEdorEspecialDataSchema[1].setEdorNo(mLPEdorItemSchema_in.
                getEdorNo());
            tLPEdorEspecialDataSchema[1].setEdorType(mLPEdorItemSchema_in.
                getEdorType());
            tLPEdorEspecialDataSchema[1].setPolNo(BQ.FILLDATA);

            tLPEdorEspecialDataSchema[1].setDetailType("TF_D");

            mLPEdorEspecialDataSchema = tLPEdorEspecialDataSchema;
            System.out.print(mLPEdorEspecialDataSchema);
        }
        catch(Exception e)
        {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorTFDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if(mGlobalInput == null || mLPEdorItemSchema_in == null)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorTFDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "输入数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行校验处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean checkData()
    {
        DisabledManageBL tDisabledManageBL = new DisabledManageBL();
        if(!tDisabledManageBL.dealDisabledpol(mLPPolSet,
                                              mLPEdorItemSchema_in.getEdorType()))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorTFDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = tDisabledManageBL.getInsuredno() +
                                  "号被保人投保险种'" + tDisabledManageBL.getRiskcode() +
                                  "'下TF项目在"
                                  + CommonBL.getCodeName("stateflag",
                tDisabledManageBL.getState()) + "状态下不能添加!";
            this.mErrors.addOneError(tError);
            return false;

        }
        
        //20080813 zhanggm 终止缴费的险种不能特权复效
        //20081027 zhanggm 终止缴费的险种可以特权复效 
        /*
        if(!tDisabledManageBL.checkPolState(mLPPolSet,mLPEdorItemSchema_in.getEdorType()))
        {
        	CError tError = new CError();
        	tError.moduleName = "PEdorTFDetailBL";
        	tError.functionName = "checkData";
        	tError.errorMessage = tDisabledManageBL.getInsuredno() +
        	    "号被保人投保险种'" + tDisabledManageBL.getRiskcode() +
        	    "'已经是续保终止或缴费终止状态，不能做特权复效！";
        	this.mErrors.addOneError(tError);
        	return false;
        }
        */
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        String reasonCode = mLPEdorItemSchema_in.getReasonCode();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema_in.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema_in.getEdorType());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema_in.getContNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();

        if(tLPEdorItemSet == null || tLPEdorItemSet.size() < 1)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorTFDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "输入数据有误,LPEdorItem中没有相关数据!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLPEdorItemSchema = tLPEdorItemSet.get(1);
        mLPEdorItemSchema.setEdorState("1");
        mLPEdorItemSchema.setReasonCode(reasonCode);

        if(mLPEdorItemSchema.getEdorState().trim().equals("2"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorTFDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保全已经理算确认不能修改!";
            this.mErrors.addOneError(tError);
            return false;
        }

        for(int i = 1; i <= mLPPolSet.size(); i++)
        {
            LPEdorItemSchema tmLPEdorItemSchema = new LPEdorItemSchema();
            tmLPEdorItemSchema.setSchema(mLPEdorItemSchema);
            tmLPEdorItemSchema.setPolNo(mLPPolSet.get(i).getPolNo());

            LPPolSchema schema = getLPPol(mLPPolSet.get(i));
            if(schema == null)
            {
                return false;
            }
            if(schema.getStateFlag().equals(BQ.STATE_FLAG_SIGN))
            {
                CError tError = new CError();
                tError.moduleName = "PEdorTFDetailBL";
                tError.functionName = "checkData";
                tError.errorMessage = "有效险种不能做特权复效!";
                this.mErrors.addOneError(tError);
                return false;
            }

            mLPPolSet.get(i).setSchema(schema);
        }

        return true;
    }

    /**
     * 查询相应的险种信息
     * @param tLPPolSchema LPPolSchema
     * @return LPPolSchema
     */
    private LPPolSchema getLPPol(LPPolSchema tLPPolSchema)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tLPPolSchema.getPolNo());
        if(!tLCPolDB.getInfo())
        {
            mErrors.addOneError("查询险种信息出错");
            return null;
        }

        //交换LPPolSchema 和 LCPolSchema的数据
        LPPolSchema aLPPolSchema = new LPPolSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(aLPPolSchema, tLCPolDB.getSchema());

        aLPPolSchema.setEdorNo(tLPPolSchema.getEdorNo());
        aLPPolSchema.setEdorType(tLPPolSchema.getEdorType());

        return aLPPolSchema;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if(!dealOtherLPInfo())
        {
            return false;
        }

        mLPEdorItemSchema.setEdorValiDate(edorAppDate);
        mLPEdorItemSchema.setEdorState("1");
        return true;
    }

    /**
     * dealOtherLPInfo
     * 生成其他LP表信息
     * @return boolean
     */
    private boolean dealOtherLPInfo()
    {
        String sql = "delete from LPCont "
                     + "where EdorNo = '" + mLPEdorItemSchema.getEdorNo() + "'"
                     + "   and EdorType = '"
                     + mLPEdorItemSchema.getEdorType() + "' "
                     + "   and ContNo = '"
                     + mLPEdorItemSchema.getContNo() + "' ";
        mMap.put(sql, SysConst.DELETE);

        sql = "insert into LPCont (select '"
              + mLPEdorItemSchema.getEdorNo()
              + "', '" + mLPEdorItemSchema.getEdorType()
              + "', LCCont.* from LCCont "
              + " where ContNo='" + mLPEdorItemSchema.getContNo() + "')";
        mMap.put(sql, SysConst.INSERT);

        String[] tables =
            {"LCDuty", "LCPrem"};

        for(int i = 0; i < tables.length; i++)
        {
            sql = "delete from " + tables[i].replaceFirst("C", "P")
                  + " "
                  + "where EdorNo = '" + mLPEdorItemSchema.getEdorNo() + "'"
                  + "   and EdorType = '"
                  + mLPEdorItemSchema.getEdorType() + "' "
                  + "   and ContNo = '" + mLPEdorItemSchema.getContNo() + "' ";
            mMap.put(sql, SysConst.DELETE);

            sql = "insert into " + tables[i].replaceFirst("C", "P")
                  + " (select '" + mLPEdorItemSchema.getEdorNo()
                  + "', '" + mLPEdorItemSchema.getEdorType()
                  + "', " + tables[i] + ".* from " + tables[i] + " "
                  + " where ContNo='" + mLPEdorItemSchema.getContNo() + "')";
            mMap.put(sql, SysConst.DELETE);
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareData()
    {
        mMap.put(this.mLPEdorEspecialDataSchema[0], "DELETE&INSERT");
        mMap.put(this.mLPEdorEspecialDataSchema[1], "DELETE&INSERT");
        mMap.put(UpdateEdorState.getUpdateEdorStateSql(mLPEdorItemSchema),
                 "UPDATE");
        mMap.put(mLPPolSet, "DELETE&INSERT");
        mMap.put(mLPEdorItemSchema, "UPDATE");

        mResult.clear();
        mResult.add(mMap);

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }
}
