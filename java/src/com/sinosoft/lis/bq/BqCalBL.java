package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;

/*
 * <p>Title: 保全计算类 </p>
 * <p>Description: 通过传入的保单信息和保费项目信息计算出交退费变动信息，或保额变动信息 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author TJJ
 * @version 1.0
 * @date 2002-07-01
 */

public class BqCalBL
{
    // @Field
    private FDate fDate = new FDate();
    public CErrors mErrors = new CErrors(); // 错误信息

    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LJSGetEndorseSchema mLJSGetEndorseSchema = new LJSGetEndorseSchema();

    private String mCalCode;
    private String mFlag;
    private BqCalBase mBqCalBase = new BqCalBase();

    public BqCalBL()
    {}

    public BqCalBL(LPEdorItemSchema aLPEdorItemSchema, BqCalBase aBqCalBase, String aCalCode, String aFlag)
    {
        mLPEdorItemSchema.setSchema(aLPEdorItemSchema);
        mBqCalBase = aBqCalBase;
        mCalCode = aCalCode;
        mFlag = aFlag;
    }

    public BqCalBL(BqCalBase aBqCalBase, String aCalCode, String aFlag)
    {
        mBqCalBase = aBqCalBase;
        mCalCode = aCalCode;
        mFlag = aFlag;
    }

    /**
     * 判断自动核保是否通过
     * @return
     */
    public boolean calUWEndorse()
    {
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("Interval", mBqCalBase.getInterval());
        mCalculator.addBasicFactor("GetMoney", mBqCalBase.getGetMoney());
        mCalculator.addBasicFactor("Get", mBqCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mBqCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mBqCalBase.getPrem());
        mCalculator.addBasicFactor("PayIntv", mBqCalBase.getPayIntv());
        mCalculator.addBasicFactor("GetIntv", mBqCalBase.getGetIntv());
        mCalculator.addBasicFactor("AppAge", mBqCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mBqCalBase.getSex());
        mCalculator.addBasicFactor("Job", mBqCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mBqCalBase.getPayEndYear());
        mCalculator.addBasicFactor("PayEndYearFlag", mBqCalBase.getPayEndYearFlag());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("GetYear", mBqCalBase.getGetYear());
        mCalculator.addBasicFactor("Years", mBqCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mBqCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mBqCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("AddRate", mBqCalBase.getAddRate());
        mCalculator.addBasicFactor("GDuty", mBqCalBase.getGDuty());
        mCalculator.addBasicFactor("PolNo", mBqCalBase.getPolNo());
        mCalculator.addBasicFactor("EdorNo", mBqCalBase.getEdorNo());
        mCalculator.addBasicFactor("EdorType", mBqCalBase.getEdorType());
        mCalculator.addBasicFactor("GrpContNo", mBqCalBase.getGrpContNo());

        String tStr = "";
        tStr = mCalculator.calculate();
        System.out.println("---str" + tStr);
        if (tStr == null || tStr.trim().equals("") || tStr.trim().equals("0"))
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    /**
     * 生成交退费记录
     * @return
     */
    public LJSGetEndorseSchema calGetEndorse(double pValue)
    {
        LJSGetEndorseSchema aLJSGetEndorseSchema = new LJSGetEndorseSchema();
        initGetEndorse();
        mLJSGetEndorseSchema.setGetMoney(pValue);
        aLJSGetEndorseSchema.setSchema(mLJSGetEndorseSchema);

        return aLJSGetEndorseSchema;
    }

    /**
     * 按照比例法计算退保
     * @return
     */
    public double calRateEndorse()
    {
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);

        //增加基本要素
        mCalculator.addBasicFactor("Interval", mBqCalBase.getInterval());
        mCalculator.addBasicFactor("Get", mBqCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mBqCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mBqCalBase.getPrem());
        mCalculator.addBasicFactor("PayIntv", mBqCalBase.getPayIntv());
        mCalculator.addBasicFactor("GetIntv", mBqCalBase.getGetIntv());
        mCalculator.addBasicFactor("AppAge", mBqCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mBqCalBase.getSex());
        mCalculator.addBasicFactor("Job", mBqCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mBqCalBase.getPayEndYear());
        mCalculator.addBasicFactor("PayEndYearFlag", mBqCalBase.getPayEndYearFlag());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("GetYear", mBqCalBase.getGetYear());
        mCalculator.addBasicFactor("Years", mBqCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mBqCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mBqCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("AddRate", mBqCalBase.getAddRate());
        mCalculator.addBasicFactor("GDuty", mBqCalBase.getGDuty());
        mCalculator.addBasicFactor("PolNo", mBqCalBase.getPolNo());
        mCalculator.addBasicFactor("GetBalance", mBqCalBase.getGetBalance());
        mCalculator.addBasicFactor("GetMoney", mBqCalBase.getGetMoney());
        mCalculator.addBasicFactor("PayEndYearFlag", mBqCalBase.getPayEndYearFlag());
        String tStr = mCalculator.calculate();
        System.out.println("按照比例法计算退保: " + tStr);

        if (tStr == null || tStr.trim().equals(""))
        {
            return 0;
        }
        else
        {
            return Double.parseDouble(tStr);
        }
    }

    /**
     * 只计算保全金额
     * @param aFlag
     * @return
     */
    public double calGetEndorse(String aFlag)
    {
        LJSGetEndorseSchema aLJSGetEndorseSchema = new LJSGetEndorseSchema();
        double mValue = -1;
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("Interval", mBqCalBase.getInterval());
        mCalculator.addBasicFactor("Get", mBqCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mBqCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mBqCalBase.getPrem());
        mCalculator.addBasicFactor("PayIntv", mBqCalBase.getPayIntv());
        mCalculator.addBasicFactor("GetIntv", mBqCalBase.getGetIntv());
        mCalculator.addBasicFactor("AppAge", mBqCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mBqCalBase.getSex());
        mCalculator.addBasicFactor("Job", mBqCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mBqCalBase.getPayEndYear());
        mCalculator.addBasicFactor("PayEndYearFlag", mBqCalBase.getPayEndYearFlag());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("GetYear", mBqCalBase.getGetYear());
        mCalculator.addBasicFactor("Years", mBqCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mBqCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mBqCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("AddRate", mBqCalBase.getAddRate());
        mCalculator.addBasicFactor("GDuty", mBqCalBase.getGDuty());
        mCalculator.addBasicFactor("PolNo", mBqCalBase.getPolNo());
        mCalculator.addBasicFactor("GetBalance", mBqCalBase.getGetBalance());
        mCalculator.addBasicFactor("GetMoney", mBqCalBase.getGetMoney());

        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<");
        System.out.println("Prem : " + mBqCalBase.getPrem());
        System.out.println("get: " + mBqCalBase.getGet());
        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr == null || tStr.trim().equals(""))
        {
            mValue = 0;
        }
        else
        {
            mValue = Double.parseDouble(tStr);
        }
        System.out.println("----------cal:" + mValue);

        return mValue;
    }

    /**
     * 只计算保全金额
     * @param pCalCode 计算编码
     * @param pBqCalBase 计算要素类
     * @return
     */
    public double calGetEndorse(String pCalCode, BqCalBase pBqCalBase)
    {
        //设置计算编码
        Calculator tCalculator = new Calculator();
        tCalculator.setCalCode(pCalCode);

        //增加基本要素
        tCalculator.addBasicFactor("Interval", pBqCalBase.getInterval());
        tCalculator.addBasicFactor("Get", pBqCalBase.getGet());
        tCalculator.addBasicFactor("Mult", pBqCalBase.getMult());
        tCalculator.addBasicFactor("Prem", pBqCalBase.getPrem());
        tCalculator.addBasicFactor("SumPrem", pBqCalBase.getSumPrem());
        tCalculator.addBasicFactor("PayIntv", pBqCalBase.getPayIntv());
        tCalculator.addBasicFactor("GetIntv", pBqCalBase.getGetIntv());
        tCalculator.addBasicFactor("AppAge", pBqCalBase.getAppAge());
        tCalculator.addBasicFactor("Sex", pBqCalBase.getSex());
        tCalculator.addBasicFactor("Job", pBqCalBase.getJob());
        tCalculator.addBasicFactor("PayEndYear", pBqCalBase.getPayEndYear());
        tCalculator.addBasicFactor("PayEndYearFlag", pBqCalBase.getPayEndYearFlag());
        tCalculator.addBasicFactor("GetStartDate", "");
        tCalculator.addBasicFactor("GetYear", pBqCalBase.getGetYear());
        tCalculator.addBasicFactor("Years", pBqCalBase.getYears());
        tCalculator.addBasicFactor("Grp", "");
        tCalculator.addBasicFactor("GetFlag", "");
        tCalculator.addBasicFactor("CValiDate", pBqCalBase.getCValiDate());
        tCalculator.addBasicFactor("Count", pBqCalBase.getCount());
        tCalculator.addBasicFactor("FirstPayDate", "");
        tCalculator.addBasicFactor("AddRate", pBqCalBase.getAddRate());
        tCalculator.addBasicFactor("GDuty", pBqCalBase.getGDuty());
        tCalculator.addBasicFactor("PolNo", pBqCalBase.getPolNo());
        tCalculator.addBasicFactor("EdorNo", pBqCalBase.getEdorNo());
        tCalculator.addBasicFactor("EdorType", pBqCalBase.getEdorType());
        tCalculator.addBasicFactor("EdorValiDate", pBqCalBase.getEdorValiDate());
        tCalculator.addBasicFactor("EdorAppDate", pBqCalBase.getEdorAppDate());
        tCalculator.addBasicFactor("GetBalance", pBqCalBase.getGetBalance());
        tCalculator.addBasicFactor("GetMoney", pBqCalBase.getGetMoney());
        tCalculator.addBasicFactor("GrpContNo", pBqCalBase.getGrpContNo());
        tCalculator.addBasicFactor("CInValiDate", pBqCalBase.getCInValiDate());
        
        String getPrtno="select prtno from lccont where contno='"+pBqCalBase.getContNo()+"'";
    	String valuePrtno =  new ExeSQL().getOneValue(getPrtno);
    	tCalculator.addBasicFactor("prtno",valuePrtno);

        tCalculator.addBasicFactor("InsuYear", pBqCalBase.getInsuYear());
        tCalculator.addBasicFactor("InsuYearFlag", pBqCalBase.getInsuYearFlag());
        tCalculator.addBasicFactor("GetStartYear", pBqCalBase.getGetStartYear());
        tCalculator.addBasicFactor("StandbyFlag1", pBqCalBase.getStandByFlag1());
        tCalculator.addBasicFactor("PayToDate", pBqCalBase.getPayToDate());
        tCalculator.addBasicFactor("LastPayToDate", pBqCalBase.getLastPayToDate());
        tCalculator.addBasicFactor("CalType", pBqCalBase.getCalType());
        tCalculator.addBasicFactor("InsuAccBala", pBqCalBase.getInsuAccBala());
        tCalculator.addBasicFactor("AccRate", pBqCalBase.getAccRate());
        tCalculator.addBasicFactor("AccManageFeeRateZT", pBqCalBase.getAccManageFeeRateZT());
        tCalculator.addBasicFactor("UpPolYears", pBqCalBase.getUpPolYears());
        tCalculator.addBasicFactor("CalTime", pBqCalBase.getCalTime());
        tCalculator.addBasicFactor("FeeRate", pBqCalBase.getFeeRate());
        tCalculator.addBasicFactor("InsuredApp",pBqCalBase.getInsuredApp());
        tCalculator.addBasicFactor("InsuredAppAge",pBqCalBase.getInsuredAppAge());
        tCalculator.addBasicFactor("LpGetFlag",pBqCalBase.getLpGetFlag());
        tCalculator.addBasicFactor("SYTaxMoney",pBqCalBase.getSYTaxMoney());
        System.out.println("Prem : " + pBqCalBase.getPrem());
        System.out.println("get: " + pBqCalBase.getGet());
        
        //计算退保时的--累计已给付保险金
        String sumJiFuBaoXianJin="select nvl(sum(pay),0)  from ljagetclaim where contno='"+mLPEdorItemSchema.getContNo()
                                +"' and polno in (select polno from lcpol where contno='"+mLPEdorItemSchema.getContNo()
                                +"' and mainpolno=polno) and othernotype='5' with ur";
        SSRS sumJiFuBaoXianJinSSRS =new ExeSQL().execSQL(sumJiFuBaoXianJin); 
        tCalculator.addBasicFactor("SumPayMoney",sumJiFuBaoXianJinSSRS.GetText(1, 1));
        
               
        //进行计算
        String tStr = tCalculator.calculate();
        double tValue = 0;
        if (tCalculator.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tCalculator.mErrors);
            return tValue;
        }

        if (tStr == null || tStr.trim().equals(""))
        {
            tValue = 0;
        }
        else
        {
            tValue = Double.parseDouble(tStr);
        }

        System.out.println("Cal Result:" + tValue);
        return tValue;
    }

    /**
     * 得到校验结果
     * @param aFlag
     * @return
     */
    public boolean calValiEndorse(String aFlag)
    {
        boolean aValiFlag = true;
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("Interval", mBqCalBase.getInterval());
        mCalculator.addBasicFactor("LimitDay", mBqCalBase.getLimitDay());
        mCalculator.addBasicFactor("PolValiFlag", mBqCalBase.getLimitDay());
        mCalculator.addBasicFactor("LoanMoney", mBqCalBase.getLoanMoney());
        mCalculator.addBasicFactor("TrayMoney", mBqCalBase.getTrayMoney());
        mCalculator.addBasicFactor("Operator", mBqCalBase.getOperator());
        mCalculator.addBasicFactor("EdorValiDate", mBqCalBase.getEdorValiDate());
        mCalculator.addBasicFactor("Get", mBqCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mBqCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mBqCalBase.getPrem());
        mCalculator.addBasicFactor("PayIntv", mBqCalBase.getPayIntv());
        mCalculator.addBasicFactor("GetIntv", mBqCalBase.getGetIntv());
        mCalculator.addBasicFactor("AppAge", mBqCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mBqCalBase.getSex());
        mCalculator.addBasicFactor("Job", mBqCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mBqCalBase.getPayEndYear());
        mCalculator.addBasicFactor("PayEndYearFlag", mBqCalBase.getPayEndYearFlag());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("GetYear", mBqCalBase.getGetYear());
        mCalculator.addBasicFactor("Years", mBqCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mBqCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mBqCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("AddRate", mBqCalBase.getAddRate());
        mCalculator.addBasicFactor("GDuty", mBqCalBase.getGDuty());
        mCalculator.addBasicFactor("PolNo", mBqCalBase.getPolNo());
        mCalculator.addBasicFactor("GetBalance", mBqCalBase.getGetBalance());
        mCalculator.addBasicFactor("GetMoney", mBqCalBase.getGetMoney());
        mCalculator.addBasicFactor("RiskCode", mBqCalBase.getRiskCode());
        mCalculator.addBasicFactor("EdorNo", mBqCalBase.getEdorNo());
        mCalculator.addBasicFactor("EdorAcceptNo", mBqCalBase.getEdorAcceptNo());
        mCalculator.addBasicFactor("ContNo", mBqCalBase.getContNo());
        mCalculator.addBasicFactor("EdorType", mBqCalBase.getEdorType());
        mCalculator.addBasicFactor("GrpContNo", mBqCalBase.getGrpContNo());
        String tStr = "";
        tStr = mCalculator.calculate();
        int mValue = -1;
        if (tStr == null || tStr.trim().equals(""))
        {
            mValue = 0;
        }
        else
        {
            mValue = Integer.parseInt(tStr);
        }

        if (aFlag == "N")
        {
            if (mValue == 0)
            {
                aValiFlag = true;
            }
            else
            {
                aValiFlag = false;
            }
        }
        else
        {
            if (mValue == 0)
            {
                aValiFlag = false;
            }
            else
            {
                aValiFlag = true;
            }
        }

        System.out.println("----------ValiFlag:" + aValiFlag);

        return aValiFlag;
    }

    /**
     * 生存领取计算
     * @return
     */
    public double calGetDraw()
    {
        LJSGetDrawSchema aLJSGetDrawSchema = new LJSGetDrawSchema();
        double mValue = -1;
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("Interval", mBqCalBase.getInterval());
        mCalculator.addBasicFactor("Get", mBqCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mBqCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mBqCalBase.getPrem());
        mCalculator.addBasicFactor("PayIntv", mBqCalBase.getPayIntv());
        mCalculator.addBasicFactor("GetIntv", mBqCalBase.getGetIntv());
        mCalculator.addBasicFactor("AppAge", mBqCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mBqCalBase.getSex());
        mCalculator.addBasicFactor("Job", mBqCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mBqCalBase.getPayEndYear());
        mCalculator.addBasicFactor("PayEndYearFlag", mBqCalBase.getPayEndYearFlag());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("GetYear", mBqCalBase.getGetYear());
        mCalculator.addBasicFactor("Years", mBqCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mBqCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mBqCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("AddRate", mBqCalBase.getAddRate());
        mCalculator.addBasicFactor("GDuty", mBqCalBase.getGDuty());
        mCalculator.addBasicFactor("PolNo", mBqCalBase.getPolNo());
        mCalculator.addBasicFactor("FRate", "1");

        mCalculator.addBasicFactor("GetBalance", mBqCalBase.getGetBalance());

        String tStr = "";
        tStr = mCalculator.calculate();
        System.out.println("tStr :" + tStr);
        if (tStr == null || tStr.trim().equals(""))
        {
            mValue = 0;
        }
        else
        {
            mValue = Double.parseDouble(tStr);
        }
        System.out.println("----------cal:" + mValue);

        return mValue;
    }

    /**
     * 生成交退费记录(查表）
     * @return
     */
    private boolean initGetEndorse()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        mLJSGetEndorseSchema = new LJSGetEndorseSchema();

        tLCPolDB.setPolNo(mLPEdorItemSchema.getPolNo());

        if (!tLCPolDB.getInfo())
        {
            return false;
        }

        //生成批改交退费表
        tLJSGetEndorseSchema.setGetNoticeNo(mLPEdorItemSchema.getEdorNo());
        tLJSGetEndorseSchema.setEndorsementNo(mLPEdorItemSchema.getEdorNo());
        tLJSGetEndorseSchema.setFeeOperationType(mLPEdorItemSchema.getEdorType());
        tLJSGetEndorseSchema.setGetDate(mLPEdorItemSchema.getEdorValiDate());
        tLJSGetEndorseSchema.setContNo(tLCPolDB.getContNo());
        tLJSGetEndorseSchema.setGrpContNo(tLCPolDB.getGrpContNo());
        tLJSGetEndorseSchema.setPolNo(tLCPolDB.getPolNo());
        tLJSGetEndorseSchema.setAppntNo(tLCPolDB.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(tLCPolDB.getInsuredNo());
        tLJSGetEndorseSchema.setAppntNo(tLCPolDB.getAppntNo());
        tLJSGetEndorseSchema.setGetMoney(tLCPolDB.getSumPrem());
        tLJSGetEndorseSchema.setKindCode(tLCPolDB.getKindCode());
        tLJSGetEndorseSchema.setRiskCode(tLCPolDB.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(tLCPolDB.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(tLCPolDB.getAgentCom());
        tLJSGetEndorseSchema.setAgentCode(tLCPolDB.getAgentCode());
        tLJSGetEndorseSchema.setAgentType(tLCPolDB.getAgentType());
        tLJSGetEndorseSchema.setAgentGroup(tLCPolDB.getAgentGroup());
        tLJSGetEndorseSchema.setDutyCode(" ");
        mLJSGetEndorseSchema.setSchema(tLJSGetEndorseSchema);

        return true;
    }

    /**
     * 生成交退费记录
     * @param aLPEdorItemSchema
     * @param aLPPolSchema
     * @param aLPDutySchema
     * @param aOperationType
     * @param aFeeType
     * @param aGetMoney
     * @param aGlobalInput
     * @return LJSGetEndorseSchema
     */
    public LJSGetEndorseSchema initLJSGetEndorse(LPEdorItemSchema aLPEdorItemSchema, LPPolSchema aLPPolSchema, LPDutySchema aLPDutySchema,
                                                 String aFeeType, double aGetMoney, GlobalInput aGlobalInput)
    {
        try
        {
            LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
            mLJSGetEndorseSchema = new LJSGetEndorseSchema();

            //生成批改交退费表
            tLJSGetEndorseSchema.setGetNoticeNo(aLPPolSchema.getEdorNo()); //给付通知书号码
            tLJSGetEndorseSchema.setEndorsementNo(aLPPolSchema.getEdorNo());
            tLJSGetEndorseSchema.setGrpContNo(aLPPolSchema.getGrpContNo());
            tLJSGetEndorseSchema.setContNo(aLPPolSchema.getContNo());
            tLJSGetEndorseSchema.setGrpPolNo(aLPPolSchema.getGrpPolNo());
            tLJSGetEndorseSchema.setPolNo(aLPPolSchema.getPolNo());
            tLJSGetEndorseSchema.setFeeOperationType(aLPPolSchema.getEdorType());
            tLJSGetEndorseSchema.setGetDate(aLPEdorItemSchema.getEdorValiDate());
            tLJSGetEndorseSchema.setGetMoney(aGetMoney);
            tLJSGetEndorseSchema.setFeeOperationType(aLPPolSchema.getEdorType()); //补退费业务类型
            tLJSGetEndorseSchema.setFeeFinaType(aFeeType); //补退费财务类型
            tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA); //无作用
            if (aLPDutySchema == null || aLPDutySchema.getDutyCode() == null || aLPDutySchema.getDutyCode().equals(""))
            {
                tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA); //无作用，但一定要，转ljagetendorse时非空
            }
            else
            {
                tLJSGetEndorseSchema.setDutyCode(aLPDutySchema.getDutyCode());
            }
            tLJSGetEndorseSchema.setOtherNo(aLPEdorItemSchema.getEdorNo()); //其他号码置为保全批单号
            tLJSGetEndorseSchema
                .setOtherNoType(aLPPolSchema.getContType().equals("1") ? "10"
                                : "3"); //保全给付
            tLJSGetEndorseSchema.setGetFlag("0");
            tLJSGetEndorseSchema.setManageCom(aLPPolSchema.getManageCom());
            tLJSGetEndorseSchema.setAgentCode(aLPPolSchema.getAgentCode());
            tLJSGetEndorseSchema.setAgentCom(aLPPolSchema.getAgentCom());
            tLJSGetEndorseSchema.setAgentGroup(aLPPolSchema.getAgentGroup());
            tLJSGetEndorseSchema.setAgentType(aLPPolSchema.getAgentType());
            tLJSGetEndorseSchema.setInsuredNo(aLPPolSchema.getInsuredNo());
            tLJSGetEndorseSchema.setKindCode(aLPPolSchema.getKindCode());
            tLJSGetEndorseSchema.setAppntNo(aLPPolSchema.getAppntNo());
            tLJSGetEndorseSchema.setRiskCode(aLPPolSchema.getRiskCode());
            tLJSGetEndorseSchema.setRiskVersion(aLPPolSchema.getRiskVersion());
            tLJSGetEndorseSchema.setHandler(aLPPolSchema.getHandler());
            tLJSGetEndorseSchema.setApproveCode(aLPPolSchema.getApproveCode());
            tLJSGetEndorseSchema.setApproveDate(aLPPolSchema.getApproveDate());
            tLJSGetEndorseSchema.setApproveTime(aLPPolSchema.getApproveTime());
            tLJSGetEndorseSchema.setOperator(aGlobalInput.Operator);
            tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
            mLJSGetEndorseSchema.setSchema(tLJSGetEndorseSchema);
        }
        catch (Exception ex)
        {
            mErrors.addOneError(new CError("建立批改补退费信息异常！"));
            return null;
        }
        return mLJSGetEndorseSchema;
    }

    /**
     * 生成交退费记录
     * @param aLPEdorItemSchema
     * @param aLCPolSchema
     * @param aLCDutySchema
     * @param aOperationType
     * @param aFeeType
     * @param aGetMoney
     * @param aGlobalInput
     * @return
     */
    public LJSGetEndorseSchema initLJSGetEndorse(LPEdorItemSchema aLPEdorItemSchema, LCPolSchema aLCPolSchema, LCDutySchema aLCDutySchema,
                                                 String aFeeType, double aGetMoney, GlobalInput aGlobalInput)
    {
        try
        {
            LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
            mLJSGetEndorseSchema = new LJSGetEndorseSchema();

            //生成批改交退费表
            tLJSGetEndorseSchema.setGetNoticeNo(aLPEdorItemSchema.getEdorNo()); //给付通知书号码
            tLJSGetEndorseSchema.setEndorsementNo(aLPEdorItemSchema.getEdorNo());
            tLJSGetEndorseSchema.setGrpContNo(aLCPolSchema.getGrpContNo());
            tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
            tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
            tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
            tLJSGetEndorseSchema.setFeeOperationType(aLPEdorItemSchema.getEdorType());
            tLJSGetEndorseSchema.setGetDate(aLPEdorItemSchema.getEdorValiDate());
            tLJSGetEndorseSchema.setGetMoney(aGetMoney);
            tLJSGetEndorseSchema.setFeeOperationType(aLPEdorItemSchema.getEdorType()); //补退费业务类型
            tLJSGetEndorseSchema.setFeeFinaType(aFeeType); //补退费财务类型
            tLJSGetEndorseSchema.setPayPlanCode("00000000"); //无作用
            if (aLCDutySchema == null || aLCDutySchema.getDutyCode() == null || aLCDutySchema.getDutyCode().equals(""))
            {
                tLJSGetEndorseSchema.setDutyCode("0"); //无作用，但一定要，转ljagetendorse时非空
            }
            else
            {
                tLJSGetEndorseSchema.setDutyCode(aLCDutySchema.getDutyCode());
            }
            tLJSGetEndorseSchema.setOtherNo(aLPEdorItemSchema.getEdorNo()); //其他号码置为保全批单号
            tLJSGetEndorseSchema
                .setOtherNoType(aLCPolSchema.getContType().equals("1") ? "10"
                                : "3"); //保全给付
            tLJSGetEndorseSchema.setGetFlag("0");
            tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
            tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
            tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
            tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
            tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
            tLJSGetEndorseSchema.setKindCode(aLCPolSchema.getKindCode());
            tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
            tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
            tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
            tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
            tLJSGetEndorseSchema.setHandler(aLCPolSchema.getHandler());
            tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
            tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
            tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
            tLJSGetEndorseSchema.setOperator(aGlobalInput.Operator);
            tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
            mLJSGetEndorseSchema.setSchema(tLJSGetEndorseSchema);
        }
        catch (Exception ex)
        {
            mErrors.addOneError(new CError("建立批改补退费信息异常！"));
            return null;
        }
        return mLJSGetEndorseSchema;
    }

    /**
     * 生成交退费记录
     * @param aLPGrpEdorItemSchema
     * @param aLCGrpPolSchema
     * @param aOperationType
     * @param aFeeType
     * @param aGetMoney
     * @param aGlobalInput
     * @return
     */
    public LJSGetEndorseSchema initLJSGetEndorse(LPGrpEdorItemSchema aLPGrpEdorItemSchema, LCGrpPolSchema aLCGrpPolSchema, String aOperationType,
                                                 String aFeeType, double aGetMoney, GlobalInput aGlobalInput)
    {

        try
        {
            LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
            mLJSGetEndorseSchema = new LJSGetEndorseSchema();

            //生成批改交退费表
            tLJSGetEndorseSchema.setGetNoticeNo(aLPGrpEdorItemSchema.getEdorNo()); //给付通知书号码
            tLJSGetEndorseSchema.setEndorsementNo(aLPGrpEdorItemSchema.getEdorNo());
            tLJSGetEndorseSchema.setGrpContNo(aLPGrpEdorItemSchema.getGrpContNo());
            tLJSGetEndorseSchema.setGrpPolNo(aLCGrpPolSchema.getGrpPolNo());
            tLJSGetEndorseSchema.setContNo(BQ.FILLDATA);
            tLJSGetEndorseSchema.setInsuredNo(BQ.FILLDATA);
            tLJSGetEndorseSchema.setPolNo(BQ.FILLDATA);
            tLJSGetEndorseSchema.setFeeOperationType(aLPGrpEdorItemSchema.getEdorType());
            tLJSGetEndorseSchema.setGetDate(aLPGrpEdorItemSchema.getEdorValiDate());
            tLJSGetEndorseSchema.setGetMoney(aGetMoney);
            tLJSGetEndorseSchema.setFeeOperationType(aOperationType); //补退费业务类型
            tLJSGetEndorseSchema.setFeeFinaType(aFeeType); //补退费财务类型
            tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA); //无作用
            tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA); //无作用，但一定要，转ljagetendorse时非空
            tLJSGetEndorseSchema.setOtherNo(aLPGrpEdorItemSchema.getEdorNo()); //其他号码置为保全批单号
            tLJSGetEndorseSchema.setOtherNoType("3"); //保全给付
            tLJSGetEndorseSchema.setGetFlag("0");
            tLJSGetEndorseSchema.setGrpName(aLCGrpPolSchema.getGrpName());
            tLJSGetEndorseSchema.setManageCom(aLCGrpPolSchema.getManageCom());
            tLJSGetEndorseSchema.setAgentCode(aLCGrpPolSchema.getAgentCode());
            tLJSGetEndorseSchema.setAgentCom(aLCGrpPolSchema.getAgentCom());
            tLJSGetEndorseSchema.setAgentGroup(aLCGrpPolSchema.getAgentGroup());
            tLJSGetEndorseSchema.setAgentType(aLCGrpPolSchema.getAgentType());
            tLJSGetEndorseSchema.setKindCode(aLCGrpPolSchema.getKindCode());
            tLJSGetEndorseSchema.setAppntNo(aLCGrpPolSchema.getCustomerNo());
            tLJSGetEndorseSchema.setRiskCode(aLCGrpPolSchema.getRiskCode());
            tLJSGetEndorseSchema.setRiskVersion(aLCGrpPolSchema.getRiskVersion());
            tLJSGetEndorseSchema.setApproveCode(aLCGrpPolSchema.getApproveCode());
            tLJSGetEndorseSchema.setApproveDate(aLCGrpPolSchema.getApproveDate());
            tLJSGetEndorseSchema.setApproveTime(aLCGrpPolSchema.getApproveTime());
            tLJSGetEndorseSchema.setOperator(aGlobalInput.Operator);
            tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
            mLJSGetEndorseSchema.setSchema(tLJSGetEndorseSchema);
        }
        catch (Exception ex)
        {
            mErrors.addOneError(new CError("建立批改补退费信息异常！"));
            return null;
        }
        return mLJSGetEndorseSchema;
    }

    /**
     * 生成0退费的财务数据
     * @param tLPDutySchema LPDutySchema
     * @return boolean
     */
    public LJSGetEndorseSchema getZeroTFLJSGetEndorseSet(
        LPEdorItemSchema aLPEdorItemSchema,
        LPPolSchema aLPPolSchema,
        LPDutySchema aLPDutySchema)
    {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        try
        {
            //生成批改交退费表
            tLJSGetEndorseSchema.setGetNoticeNo(aLPPolSchema.getEdorNo()); //给付通知书号码
            tLJSGetEndorseSchema.setEndorsementNo(aLPPolSchema.getEdorNo());
            tLJSGetEndorseSchema.setGrpContNo(aLPPolSchema.getGrpContNo());
            tLJSGetEndorseSchema.setContNo(aLPPolSchema.getContNo());
            tLJSGetEndorseSchema.setGrpPolNo(aLPPolSchema.getGrpPolNo());
            tLJSGetEndorseSchema.setPolNo(aLPPolSchema.getPolNo());
            tLJSGetEndorseSchema.setFeeOperationType(aLPPolSchema.getEdorType());
            tLJSGetEndorseSchema.setGetDate(aLPEdorItemSchema.getEdorValiDate());
            tLJSGetEndorseSchema.setGetMoney(0);
            tLJSGetEndorseSchema.setFeeOperationType(aLPPolSchema.getEdorType()); //补退费业务类型
            tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_TF); //补退费财务类型
            tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA); //无作用
            if (aLPDutySchema == null || aLPDutySchema.getDutyCode() == null
                || aLPDutySchema.getDutyCode().equals(""))
            {
                tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA); //无作用，但一定要，转ljagetendorse时非空
            }
            else
            {
                tLJSGetEndorseSchema.setDutyCode(aLPDutySchema.getDutyCode());
            }
            tLJSGetEndorseSchema.setOtherNo(aLPEdorItemSchema.getEdorNo()); //其他号码置为保全批单号
            tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_P); //保全给付
            tLJSGetEndorseSchema.setGetFlag("0");
            tLJSGetEndorseSchema.setManageCom(aLPPolSchema.getManageCom());
            tLJSGetEndorseSchema.setAgentCode(aLPPolSchema.getAgentCode());
            tLJSGetEndorseSchema.setAgentCom(aLPPolSchema.getAgentCom());
            tLJSGetEndorseSchema.setAgentGroup(aLPPolSchema.getAgentGroup());
            tLJSGetEndorseSchema.setAgentType(aLPPolSchema.getAgentType());
            tLJSGetEndorseSchema.setInsuredNo(aLPPolSchema.getInsuredNo());
            tLJSGetEndorseSchema.setKindCode(aLPPolSchema.getKindCode());
            tLJSGetEndorseSchema.setAppntNo(aLPPolSchema.getAppntNo());
            tLJSGetEndorseSchema.setRiskCode(aLPPolSchema.getRiskCode());
            tLJSGetEndorseSchema.setRiskVersion(aLPPolSchema.getRiskVersion());
            tLJSGetEndorseSchema.setHandler(aLPPolSchema.getHandler());
            tLJSGetEndorseSchema.setApproveCode(aLPPolSchema.getApproveCode());
            tLJSGetEndorseSchema.setApproveDate(aLPPolSchema.getApproveDate());
            tLJSGetEndorseSchema.setApproveTime(aLPPolSchema.getApproveTime());
            tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError(new CError("建立批改补退费信息异常！"));
            return null;
        }
        return tLJSGetEndorseSchema;
    }

    /**
     * 本函数中polNo不能为"000000"
     * 1、	犹豫期退保 对应 冲退保费 TF。
     * 2、	一年期及以内险种 对应 冲退保费 TB。
     * 3、	一年期以上险种 对应 冲退保金 TF。
     * @param edorType String
     * @param polNo String
     * @return String
     */
    public static String getFinType(String edorType, String riskCode)
    {
        //犹豫期退保冲退保费 TF
        if(edorType.equals(BQ.EDORTYPE_WT))
        {
            return BQ.FEEFINATYPE_TF;
        }
        //退保、减人、无名单减人
        else if(edorType.equals(BQ.EDORTYPE_CT)
            || edorType.equals(BQ.EDORTYPE_ZT)
            || edorType.equals(BQ.EDORTYPE_WJ))
        {
            //通过险种代码查询险种期限
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(riskCode);
            if(!tLMRiskAppDB.getInfo())
            {
                return "";
            }

            //一年期及以内险种冲退保费 TF。
            if (tLMRiskAppDB.getRiskPeriod().equals("M")
                || tLMRiskAppDB.getRiskPeriod().equals("S"))
            {
                return BQ.FEEFINATYPE_TF;
            }
            //一年期以上险种冲退保金 TB
            else if(tLMRiskAppDB.getRiskPeriod().equals("L"))
            {
                return BQ.FEEFINATYPE_TB;
            }
        }
        return "";
    }

    /**
     * 获取转换财务接口的描述
     * @param edorType
     * @param sysType
     * @param riskCode
     * @return
     */
    public static String getFinType(String edorType, String sysType, String polNo)
    {
        String riskCode;

        //查询个单险种代码
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(polNo);
        if (!tLCPolDB.getInfo())
        {
            riskCode = BQ.FILLDATA;
        }
        else
        {
            riskCode = tLCPolDB.getRiskCode();
        }

        //若取不到险种代码，直接返回类型
        if(riskCode.equals(BQ.FILLDATA))
        {
            return sysType;
        }

        String finType = getFinType(edorType, riskCode);
        if(finType.equals(""))
        {
            finType = sysType;
        }

        return finType;

         /*
        //根据保单号获取险种代码
        String riskCode = "";

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(polNo);
        if (!tLCPolDB.getInfo())
        {
            if (tLCPolDB.mErrors.needDealError())
            {
                return "";
            }
            //如果个单查询失败，则可能是团单
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(polNo);
            tLCGrpContDB.getInfo();
            if (tLCGrpContDB.mErrors.needDealError())
            {
                return "";

            }
            else
            {
                riskCode = "000000";
            }
        }
        else
        {
            riskCode = tLCPolDB.getRiskCode();
        }

        //根据险种代码获取对应的财务类型编码
        LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCodeType(edorType);
        tLDCode1DB.setCode(sysType);
        tLDCode1DB.setCode1(riskCode);
        if (tLDCode1DB.getInfo())
        {
            return tLDCode1DB.getCodeName();
        }

        //如果该险种未描述，则该项目的通用财务类型
        tLDCode1DB.setCode1("000000");
        if (tLDCode1DB.getInfo())
        {
            return tLDCode1DB.getCodeName();
        }
*/

    }

    public double calChgMoney(String aCalCode, BqCalBase aBqCalBase)
    {
		if (aCalCode == null || aCalCode.equals(""))
		{
//			mErrors.addOneError(new CError("计算代码为空，无法计算！"));
			return 0;
		}
        //设置计算编码
        Calculator tCalculator = new Calculator();
        tCalculator.setCalCode(aCalCode);

        //增加基本要素
        if (!setBasicFactor(tCalculator, aBqCalBase))
        {
            mErrors.addOneError(new CError("设置计算要素失败！"));
            return 0;
        }
        //进行计算
        String tStr = tCalculator.calculate();
        double tValue = 0;
        if (tStr == null || tStr.trim().equals(""))
        {
            tValue = 0;
        }
        else
        {
            tValue = Double.parseDouble(tStr);
        }

        return tValue;
    }

    private boolean setBasicFactor(Calculator aCalculator, BqCalBase aBqCalBase)
    {
        aCalculator.addBasicFactor("Interval", aBqCalBase.getInterval());
        aCalculator.addBasicFactor("Get", aBqCalBase.getGet());
        aCalculator.addBasicFactor("Mult", aBqCalBase.getMult());
        aCalculator.addBasicFactor("Prem", aBqCalBase.getPrem());
        aCalculator.addBasicFactor("SumPrem", aBqCalBase.getSumPrem());
        aCalculator.addBasicFactor("NewPrem", aBqCalBase.getNewPrem());
        aCalculator.addBasicFactor("PayIntv", aBqCalBase.getPayIntv());
        aCalculator.addBasicFactor("GetIntv", aBqCalBase.getGetIntv());
        aCalculator.addBasicFactor("AppAge", aBqCalBase.getAppAge());
        aCalculator.addBasicFactor("Sex", aBqCalBase.getSex());
        aCalculator.addBasicFactor("Job", aBqCalBase.getJob());
        aCalculator.addBasicFactor("PayEndYear", aBqCalBase.getPayEndYear());
        aCalculator.addBasicFactor("PayEndYearFlag", aBqCalBase.getPayEndYearFlag());
        aCalculator.addBasicFactor("GetStartDate", "");
        aCalculator.addBasicFactor("GetYear", aBqCalBase.getGetYear());
        aCalculator.addBasicFactor("Years", aBqCalBase.getYears());
        aCalculator.addBasicFactor("Grp", "");
        aCalculator.addBasicFactor("GetFlag", "");
        aCalculator.addBasicFactor("CValiDate", aBqCalBase.getCValiDate());
        aCalculator.addBasicFactor("Count", aBqCalBase.getCount());
        aCalculator.addBasicFactor("FirstPayDate", "");
        aCalculator.addBasicFactor("AddRate", aBqCalBase.getAddRate());
        aCalculator.addBasicFactor("GDuty", aBqCalBase.getGDuty());
        aCalculator.addBasicFactor("PolNo", aBqCalBase.getPolNo());
        aCalculator.addBasicFactor("EdorNo", aBqCalBase.getEdorNo());
        aCalculator.addBasicFactor("EdorType", aBqCalBase.getEdorType());
        aCalculator.addBasicFactor("EdorValiDate", aBqCalBase.getEdorValiDate());
        aCalculator.addBasicFactor("EdorAppDate", aBqCalBase.getEdorAppDate());
        aCalculator.addBasicFactor("GetBalance", aBqCalBase.getGetBalance());
        aCalculator.addBasicFactor("GetMoney", aBqCalBase.getGetMoney());
        aCalculator.addBasicFactor("InsuYear", aBqCalBase.getInsuYear());
        aCalculator.addBasicFactor("InsuYearFlag", aBqCalBase.getInsuYearFlag());
        aCalculator.addBasicFactor("GetStartYear", aBqCalBase.getGetStartYear());
        aCalculator.addBasicFactor("StandbyFlag1", aBqCalBase.getStandByFlag1());
        aCalculator.addBasicFactor("LastPayToDate", aBqCalBase.getLastPayToDate());
        aCalculator.addBasicFactor("PayToDate", aBqCalBase.getPayToDate());
        aCalculator.addBasicFactor("InsuredApp",aBqCalBase.getInsuredApp());
        aCalculator.addBasicFactor("UpPolYears", aBqCalBase.getUpPolYears());
        aCalculator.addBasicFactor("Get", aBqCalBase.getGet());
        aCalculator.addBasicFactor("customgetpoldate", aBqCalBase.getCustomgetpoldate());//add by xp 08-09-23
        aCalculator.addBasicFactor("signdate", aBqCalBase.getSignDate());//add by xp 08-09-23
        return true;
    }

    /**
     * 测试函数
     * @param args
     */
    public static void main(String args[])
    {
        System.out.println(getFinType("CT", "11000038946"));
    }
}
