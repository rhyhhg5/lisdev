package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;


public class NewDataImport {
	
    private MMap mMap = new MMap();
	
	private VData mResult = new VData();
	
	private String mCurrentDate = PubFun.getCurrentDate();

//    private String mCurrentTime = PubFun.getCurrentTime();
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
      new NewDataImport().getEdorData();
	}
	
	
	public boolean dealData()
	{
		if(getData())
		{

		}
//		add by xp 添加保全部分提数
		if(getEdorData())
		{

		}
	
		 return true;
	}
	
	public boolean getEdorData()
	{
		
		
		String getDataSql="select trim(contno),'1','01',trim(appntno),trim(appntname),"
			             +"trim(managecom),"
			             +"(select phone from lcaddress where customerno=lcc1.appntno and addressno=(select addressno from lcappnt where contno=lcc1.contno)),"
			             +"(select MOBILE from lcaddress where customerno=lcc1.appntno and addressno=(select addressno from lcappnt where contno=lcc1.contno)),"
			             +"(select COMPANYPHONE from lcaddress where customerno=lcc1.appntno and addressno=(select addressno from lcappnt where contno=lcc1.contno)),"
			             +"(select HOMEPHONE from lcaddress where customerno=lcc1.appntno and addressno=(select addressno from lcappnt where contno=lcc1.contno)),"
			             +"(select MOBILE2 from lcaddress where customerno=lcc1.appntno and addressno=(select addressno from lcappnt where contno=lcc1.contno)),"
			             +"'',"
			             +"appntidno,"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'Server',"
			             +"current date,"
			             +"current time,"
			             +"current date,"
			             +"current time "
			             +"from lccont lcc1 "
			             +"where managecom like '8644%' and appflag='1' and stateflag='1' and conttype='1' " +
			             "and contno in (select distinct contno from lpedoritem where edortype in ('AD','AE','CM') and managecom like '8644%' and contno like '0%' and (makedate = '" + mCurrentDate +"' or modifydate= '" + mCurrentDate +"') ) "
			             +" union all "
			             +"select trim(grpcontno),'2','04',trim(appntno),trim(grpname),"
			             +"trim(managecom),"
			             +"(select phone1 from lcgrpaddress where customerno=lgc2.appntno and addressno=(select addressno from lcgrpappnt where grpcontno=lgc2.grpcontno)),"
			             +"(select MOBILE1 from lcgrpaddress where customerno=lgc2.appntno and addressno=(select addressno from lcgrpappnt where grpcontno=lgc2.grpcontno)),"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'Server',"
			             +"current date,"
			             +"current time,"
			             +"current date,"
			             +"current time "
			             +"from lcgrpcont lgc2 "
			             +"where managecom like '8644%' and appflag='1' and stateflag='1'" +
			             "and grpcontno in (select distinct grpcontno from lpgrpedoritem where edortype in ('AC') and managecom like '8644%' and (makedate = '" + mCurrentDate +"' or modifydate= '" + mCurrentDate +"') ) "
//			            +"fetch first 1 rows only "
			             +"with ur "			             
			             ;
		

		
			             
		RSWrapper rswrapper = new RSWrapper();
		rswrapper.prepareData(null, getDataSql);
        SSRS getDataSqlSSRS = null;
        do
        {
        	getDataSqlSSRS = rswrapper.getSSRS();
        	LCContactSet tLCContactSet=new LCContactSet();
	        for (int i=1;i<getDataSqlSSRS.getMaxRow()+1;i++)
	        {
	        	
				LCContactSchema  tLCContactSchema=new LCContactSchema();
				tLCContactSchema.setContNo(getDataSqlSSRS.GetText(i, 1));
				tLCContactSchema.setContType(getDataSqlSSRS.GetText(i, 2));
				tLCContactSchema.setContactType(getDataSqlSSRS.GetText(i, 3));
				tLCContactSchema.setContactNo(getDataSqlSSRS.GetText(i, 4));
				tLCContactSchema.setContactName(getDataSqlSSRS.GetText(i, 5));
				tLCContactSchema.setManageCom(getDataSqlSSRS.GetText(i, 6));
				tLCContactSchema.setPhone(getDataSqlSSRS.GetText(i, 7));
				tLCContactSchema.setMobile(getDataSqlSSRS.GetText(i, 8));
				tLCContactSchema.setCompanyPhone(getDataSqlSSRS.GetText(i, 9));
				tLCContactSchema.setHomePhone(getDataSqlSSRS.GetText(i, 10));
				tLCContactSchema.setMobileOther(getDataSqlSSRS.GetText(i, 11));
				tLCContactSchema.setStandbyFlag1(getDataSqlSSRS.GetText(i, 13));
				tLCContactSchema.setOperator(getDataSqlSSRS.GetText(i, 17));
				tLCContactSchema.setMakeDate(PubFun.getCurrentDate());
				tLCContactSchema.setMakeTime(PubFun.getCurrentTime());
				tLCContactSchema.setModifyDate(PubFun.getCurrentDate());
				tLCContactSchema.setModifyTime(PubFun.getCurrentTime());
				
				tLCContactSet.add(tLCContactSchema);
			}
		mMap.put(tLCContactSet, SysConst.DELETE_AND_INSERT);
	    mResult.add(mMap);
					//提交数据
		if(!submitMap())
		{
			return false;
		}
		mResult.clear();
				
		}
        while(getDataSqlSSRS.getMaxRow()>0);
	
		mResult.add(mMap);
		return true;
	}
	
	public boolean getData()
	{
		
		
		String getDataSql="select trim(contno),'1','01',trim(appntno),trim(appntname),"
			             +"trim(managecom),"
			             +"(select phone from lcaddress where customerno=lcc1.appntno and addressno=(select addressno from lcappnt where contno=lcc1.contno)),"
			             +"(select MOBILE from lcaddress where customerno=lcc1.appntno and addressno=(select addressno from lcappnt where contno=lcc1.contno)),"
			             +"(select COMPANYPHONE from lcaddress where customerno=lcc1.appntno and addressno=(select addressno from lcappnt where contno=lcc1.contno)),"
			             +"(select HOMEPHONE from lcaddress where customerno=lcc1.appntno and addressno=(select addressno from lcappnt where contno=lcc1.contno)),"
			             +"(select MOBILE2 from lcaddress where customerno=lcc1.appntno and addressno=(select addressno from lcappnt where contno=lcc1.contno)),"
			             +"'',"
			             +"appntidno,"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'Server',"
			             +"current date,"
			             +"current time,"
			             +"current date,"
			             +"current time "
			             +"from lccont lcc1 "
			             +"where managecom like '8644%' and appflag='1' and stateflag='1' and conttype='1' " +
			             "and signdate = '" + mCurrentDate +"' "
			             +"union all "
			             +"select trim(contno),'1','03',trim(agentcode),(select trim(name) from laagent where agentcode=lcc2.agentcode),trim(managecom),"
			             +"(select phone from laagent where agentcode=lcc2.agentcode),"
			             +"(select mobile from laagent where agentcode=lcc2.agentcode),"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"(select idno from laagent where agentcode=lcc2.agentcode),"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'Server',"
			             +"current date,"
			             +"current time,"
			             +"current date,"
			             +"current time "
			             +" from lccont lcc2 "
			             +"where managecom like '8644%' and appflag='1' and stateflag='1' and conttype='1' " +
			             "and signdate = '" + mCurrentDate +"' "
			             +" union all "
			             +"select trim(grpcontno),'2','05',trim(agentcode),(select trim(name) from laagent where agentcode=lgc1.agentcode),trim(managecom), "
			             +"(select phone from laagent where agentcode=lgc1.agentcode),"
			             +"(select mobile from laagent where agentcode=lgc1.agentcode),"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"(select idno from laagent where agentcode=lgc1.agentcode),"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'Server',"
			             +"current date,"
			             +"current time,"
			             +"current date,"
			             +"current time "
			             +"from lcgrpcont lgc1 "
			             +"where managecom like '8644%' and appflag='1' and stateflag='1' " +
			             "and signdate = '" + mCurrentDate +"' "
			             +" union all "
			             +"select trim(grpcontno),'2','04',trim(appntno),trim(grpname),"
			             +"trim(managecom),"
			             +"(select phone1 from lcgrpaddress where customerno=lgc2.appntno and addressno=(select addressno from lcgrpappnt where grpcontno=lgc2.grpcontno)),"
			             +"(select MOBILE1 from lcgrpaddress where customerno=lgc2.appntno and addressno=(select addressno from lcgrpappnt where grpcontno=lgc2.grpcontno)),"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'',"
			             +"'Server',"
			             +"current date,"
			             +"current time,"
			             +"current date,"
			             +"current time "
			             +"from lcgrpcont lgc2 "
			             +"where managecom like '8644%' and appflag='1' and stateflag='1'" +
			             "and signdate = '" + mCurrentDate +"' "
//			            +"fetch first 1 rows only "
			             +"with ur "			             
			             ;
		

		
			             
		RSWrapper rswrapper = new RSWrapper();
		rswrapper.prepareData(null, getDataSql);
        SSRS getDataSqlSSRS = null;
        do
        {
        	getDataSqlSSRS = rswrapper.getSSRS();
        	LCContactSet tLCContactSet=new LCContactSet();
	        for (int i=1;i<getDataSqlSSRS.getMaxRow()+1;i++)
	        {
	        	
				LCContactSchema  tLCContactSchema=new LCContactSchema();
				tLCContactSchema.setContNo(getDataSqlSSRS.GetText(i, 1));
				tLCContactSchema.setContType(getDataSqlSSRS.GetText(i, 2));
				tLCContactSchema.setContactType(getDataSqlSSRS.GetText(i, 3));
				tLCContactSchema.setContactNo(getDataSqlSSRS.GetText(i, 4));
				tLCContactSchema.setContactName(getDataSqlSSRS.GetText(i, 5));
				tLCContactSchema.setManageCom(getDataSqlSSRS.GetText(i, 6));
				tLCContactSchema.setPhone(getDataSqlSSRS.GetText(i, 7));
				tLCContactSchema.setMobile(getDataSqlSSRS.GetText(i, 8));
				tLCContactSchema.setCompanyPhone(getDataSqlSSRS.GetText(i, 9));
				tLCContactSchema.setHomePhone(getDataSqlSSRS.GetText(i, 10));
				tLCContactSchema.setMobileOther(getDataSqlSSRS.GetText(i, 11));
				tLCContactSchema.setStandbyFlag1(getDataSqlSSRS.GetText(i, 13));
				tLCContactSchema.setOperator(getDataSqlSSRS.GetText(i, 17));
				tLCContactSchema.setMakeDate(PubFun.getCurrentDate());
				tLCContactSchema.setMakeTime(PubFun.getCurrentTime());
				tLCContactSchema.setModifyDate(PubFun.getCurrentDate());
				tLCContactSchema.setModifyTime(PubFun.getCurrentTime());
				
				tLCContactSet.add(tLCContactSchema);
			}
		mMap.put(tLCContactSet, "INSERT");
	    mResult.add(mMap);
					//提交数据
		if(!submitMap())
		{
			return false;
		}
		mResult.clear();
				
		}
        while(getDataSqlSSRS.getMaxRow()>0);
	
		mResult.add(mMap);
		return true;
	}
	
	   private boolean submitMap() 
	    {        
	         PubSubmit tPubsubmit = new PubSubmit();
	         if(!tPubsubmit.submitData(mResult, SysConst.INSERT))
	         {
	         	System.out.println("提交数据失败！"); 
	         	return false;
	         }
	         this.mMap = new MMap();
	         System.out.println("提交数据成功！");
	         return true;
	     }  

}