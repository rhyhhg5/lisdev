package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class GrpEdorACConfirmBL
		implements EdorConfirm
{
	/** 往后面传输数据的容器 */
	private VData mInputData;

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/**  */
	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private MMap map = new MMap();
	boolean newaddrFlag = false;

	public GrpEdorACConfirmBL()
	{}

/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据
	 *         cOperate 数据操作
	 * @return:
	 */

	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		mOperate = cOperate;

		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData())
		{
			return false;
		}
		System.out.println("---End getInputData---");

		//数据准备操作
		if (!prepareData())
		{
			return false;
		}
		System.out.println("---End prepareData---");
		if (!prepareOutputData())
		{
			return false;
		}

//        // 数据操作业务处理
//        PEdorConfirmBLS tPEdorConfirmBLS = new PEdorConfirmBLS();
//        System.out.println("Start Confirm BB BL Submit...");
//        if (!tPEdorConfirmBLS.submitData(mInputData, mOperate))
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tPEdorConfirmBLS.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "PEdorConfirmBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "数据提交失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }

		return true;
	}

	public VData getResult()
	{
		return mResult;
	}

	private boolean prepareOutputData()
	{

		mResult.clear();
		mResult.add(map);

		return true;
	}

/**
	 * 从输入数据中得到所有对象
	 * @return
	 */
	private boolean getInputData()
	{
		try
		{
			mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) mInputData.
					getObjectByObjectName("LPGrpEdorItemSchema",
										  0);
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
					"GlobalInput", 0);
		}
		catch (Exception e)
		{
			CError.buildErr(this, "接收数据失败");
			return false;
		}

		return true;
	}

/**
	 * 准备需要保存的数据
	 */
	private boolean prepareData()
	{
		VData tVData = new VData();
		LPGrpContSchema tLPGrpContSchema = new LPGrpContSchema();
		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		Reflections tRef = new Reflections();

//		String tGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
		String tEdorNo = mLPGrpEdorItemSchema.getEdorNo();
		String tEdorType = mLPGrpEdorItemSchema.getEdorType();

		LPGrpAppntDB tLPGrpAppntDB = new LPGrpAppntDB();
//		tLPGrpAppntDB.setGrpContNo(tGrpContNo);
		tLPGrpAppntDB.setEdorNo(tEdorNo);
		tLPGrpAppntDB.setEdorType(tEdorType);
                LPGrpAppntSet tLPGrpAppntSet = tLPGrpAppntDB.query();
		if (tLPGrpAppntSet==null||tLPGrpAppntSet.size()==0)
		{
			this.mErrors.copyAllErrors(tLPGrpAppntDB.mErrors);

			return false;
		}

		LPGrpContDB tLPGrpContDB = new LPGrpContDB();
//		tLPGrpContDB.setGrpContNo(tGrpContNo);
		tLPGrpContDB.setEdorNo(tEdorNo);
		tLPGrpContDB.setEdorType(tEdorType);
                LPGrpContSet tLPGrpContSet = tLPGrpContDB.query();
		if (tLPGrpContSet==null||tLPGrpContSet.size()==0)
		{
			this.mErrors.copyAllErrors(tLPGrpContDB.mErrors);

			return false;
		}

		LPGrpDB tLPGrpDB = new LPGrpDB();
		tLPGrpDB.setEdorNo(tEdorNo);
		tLPGrpDB.setEdorType(tEdorType);
		tLPGrpDB.setCustomerNo(tLPGrpContSet.get(1).getAppntNo());
		if (!tLPGrpDB.getInfo())
		{
			this.mErrors.copyAllErrors(tLPGrpDB.mErrors);

			return false;
		}

		LPGrpAddressDB tLPGrpAddressDB = new LPGrpAddressDB();
		LPGrpAddressSet tLPGrpAddressSet = new LPGrpAddressSet();
		tLPGrpAddressDB.setEdorNo(tEdorNo);
		tLPGrpAddressDB.setEdorType(tEdorType);
		tLPGrpAddressDB.setCustomerNo(tLPGrpContSet.get(1).getAppntNo());
		tLPGrpAddressDB.setAddressNo(tLPGrpContSet.get(1).getAddressNo());
		tLPGrpAddressSet = tLPGrpAddressDB.query();
		if (tLPGrpAddressDB.mErrors.needDealError())
		{
			this.mErrors.copyAllErrors(tLPGrpAddressDB.mErrors);
			return false;
		}
		if (tLPGrpAddressSet.size() > 0)
		{
			//newaddrFlag=true;
			tLPGrpAddressDB.setSchema(tLPGrpAddressSet.get(1));
		}

		LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
		tLCGrpAddressDB.setCustomerNo(tLPGrpAddressDB.getCustomerNo());
		tLCGrpAddressDB.setAddressNo(tLPGrpAddressDB.getAddressNo());
		if (!tLCGrpAddressDB.getInfo())
		{
			newaddrFlag = true;
		}

//		LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
//		tLCGrpAppntDB.setGrpContNo(tGrpContNo);
//		if (!tLCGrpAppntDB.getInfo())
//		{
//			this.mErrors.copyAllErrors(tLCGrpAppntDB.mErrors);

//			return false;
//		}
                LCGrpAppntDB ttLCGrpAppntDB = new LCGrpAppntDB();
                ttLCGrpAppntDB.setCustomerNo(tLPGrpAddressDB.getCustomerNo());
                LCGrpAppntSet tLCGrpAppntSet = ttLCGrpAppntDB.query();
                if(tLCGrpAppntSet==null||tLCGrpAppntSet.size()==0)
                {
                    this.mErrors.copyAllErrors(ttLCGrpAppntDB.mErrors);

                    return false;

                }

		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setAppntNo(tLPGrpAddressDB.getCustomerNo());
                LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
		if (tLCGrpContSet==null||tLCGrpContSet.size()==0)
		{
			this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);

			return false;
		}

		LDGrpDB tLDGrpDB = new LDGrpDB();
		tLDGrpDB.setCustomerNo(tLCGrpContDB.getAppntNo());
		if (!tLDGrpDB.getInfo())
		{
			this.mErrors.copyAllErrors(tLDGrpDB.mErrors);

			return false;
		}



		LDGrpSchema afterLDGrpSchema = new LDGrpSchema();
		LCGrpAddressSchema afterLCGrpAddressSchema = new LCGrpAddressSchema();



		LPGrpSchema afterLPGrpSchema = new LPGrpSchema();
		LPGrpAddressSchema afterLPGrpAddressSchema = new LPGrpAddressSchema();

                for(int i =1;i<=tLPGrpAppntSet.size();i++)
                {
                    LCGrpAppntSchema afterLCGrpAppntSchema = new LCGrpAppntSchema();
                    tRef.transFields(afterLCGrpAppntSchema, tLPGrpAppntSet.get(i).getSchema());
                    map.put(afterLCGrpAppntSchema, "UPDATE");
                    LPGrpAppntSchema afterLPGrpAppntSchema = new LPGrpAppntSchema();
                    afterLPGrpAppntSchema.setSchema(tLPGrpAppntSet.get(i).getSchema());
                    tRef.transFields(afterLPGrpAppntSchema, tLCGrpAppntSet.get(i).getSchema());
                    map.put(afterLPGrpAppntSchema, "UPDATE");

                }
                for(int i =1;i<=tLPGrpContSet.size();i++)
                {
                    LCGrpContSchema afterLCGrpContSchema = new LCGrpContSchema();
                    tRef.transFields(afterLCGrpContSchema, tLPGrpContSet.get(i).getSchema());
                    map.put(afterLCGrpContSchema, "UPDATE");
                    LPGrpContSchema afterLPGrpContSchema = new LPGrpContSchema();
                    afterLPGrpContSchema.setSchema(tLPGrpContSet.get(i).getSchema());
                    tRef.transFields(afterLPGrpContSchema,
                                     tLCGrpContSet.get(i).getSchema());
                    map.put(afterLPGrpContSchema, "UPDATE");

                }

		tRef.transFields(afterLDGrpSchema, tLPGrpDB.getSchema());
		map.put(afterLDGrpSchema, "UPDATE");
		if (newaddrFlag) //更新地址
		{
			tRef.transFields(afterLCGrpAddressSchema, tLPGrpAddressDB.getSchema());
			map.put(afterLCGrpAddressSchema, "INSERT");
                        for(int i=1 ;i<=tLPGrpContSet.size();i++)
                        {
                            String sql = "update LCGrpCont set AddressNo = '" +
                                         afterLCGrpAddressSchema.getAddressNo() +
                                         "' " +
                                         "where GrpContNo = '" +
                                         tLPGrpContSet.get(i).getGrpContNo() +
                                         "' ";
                            map.put(sql, "UPDATE");
                            sql = "update LCGrpAppnt set AddressNo = '" +
                                  afterLCGrpAddressSchema.getAddressNo() + "' " +
                                  "where GrpContNo = '" +
                                  tLPGrpContSet.get(i).getGrpContNo() + "' ";
                            map.put(sql, "UPDATE");
                            sql = "update LCGrpPol set AddressNo = '" +
                                  afterLCGrpAddressSchema.getAddressNo() + "' " +
                                  "where GrpContNo = '" +
                                  tLPGrpContSet.get(i).getGrpContNo() + "' ";
                            map.put(sql, "UPDATE");
                        }
		}


		afterLPGrpSchema.setSchema(tLPGrpDB.getSchema());
		tRef.transFields(afterLPGrpSchema, tLDGrpDB.getSchema());
		map.put(afterLPGrpSchema, "UPDATE");

		//需要更新团单下所有个人保单的投保人号和投保人姓名
		String grpName = afterLDGrpSchema.getGrpName();
		String wherePart = " where AppntNo='" +afterLDGrpSchema.getCustomerNo() + "'";
		for(int i=1 ;i<=tLPGrpContSet.size();i++)
         {
		   String updateLCContSQL = "update LCCont set AppntName='" + grpName + "'" + wherePart+" and  GrpContNo ='"+tLPGrpContSet.get(i).getGrpContNo()+"'";
           String updateLCGrpPol = "update LCGrpPol set GrpName='" + grpName + "'" +" where GrpContNo ='"+tLPGrpContSet.get(i).getGrpContNo()+"'";
		   String updateLCPol = "update LCPol set AppntName='" + grpName + "'" + wherePart+" and GrpContNo ='"+tLPGrpContSet.get(i).getGrpContNo()+"'";
		   
		map.put(StrTool.GBKToUnicode(updateLCContSQL), "UPDATE");
		map.put(StrTool.GBKToUnicode(updateLCGrpPol), "UPDATE");
		map.put(StrTool.GBKToUnicode(updateLCPol), "UPDATE");
         }
		return true;
	}

	public TransferData getReturnTransferData()
	{
		return null;
	}

	public CErrors getErrors()
	{
		return mErrors;
	}

	public static void main(String[] args)
	{
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.Operator = "001";
		tGlobalInput.ManageCom = "86";

		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorAcceptNo("86110000000436");
		tLPGrpEdorItemSchema.setEdorNo("430110000000143");
		tLPGrpEdorItemSchema.setEdorAppNo("430110000000143");
		tLPGrpEdorItemSchema.setGrpContNo("0000034201");
		tLPGrpEdorItemSchema.setEdorType("AC");

		VData tVData = new VData();
		tVData.add(tGlobalInput);
		tVData.add(tLPGrpEdorItemSchema);

		GrpEdorACConfirmBL tGrpEdorACConfirmBL = new GrpEdorACConfirmBL();
		if (!tGrpEdorACConfirmBL.submitData(tVData, ""))
		{
			System.out.println(tGrpEdorACConfirmBL.mErrors.getErrContent());
		}
	}

}
