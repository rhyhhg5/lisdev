package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单遗失补发项目明细</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * ReWrite ZhangRong
 * @version 1.0
 */

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class GEdorBBDetailBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    // 保费回补金额
    private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = null;  
    /** 传出数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
    private String mNeedGetMoney = "Yes";
    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema_input = new
            LPGrpEdorItemSchema();

    public GEdorBBDetailBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

            //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareData())
        {
            return false;
        }
        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "GEdorBBDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("GEdorBBDetailBL End PubSubmit");
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPGrpEdorItemSchema_input = (LPGrpEdorItemSchema) mInputData.
                    getObjectByObjectName(
"LPGrpEdorItemSchema",
0);
            mLPEdorEspecialDataSchema = (LPEdorEspecialDataSchema) mInputData
            .getObjectByObjectName(
                "LPEdorEspecialDataSchema", 0);

        }
        catch (Exception e)
        {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "GEdorBBDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mGlobalInput == null || mLPEdorEspecialDataSchema == null)
        {
            CError tError = new CError();
            tError.moduleName = "GEdorBBDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "输入数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	//处理保费回补金额
    	dealGBInfo();
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorNo(mLPGrpEdorItemSchema_input.getEdorNo());
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
        if (tLPGrpEdorMainSet == null || tLPGrpEdorMainSet.size() != 1)
        {
            // @@错误处理
            mErrors.copyAllErrors(tLPGrpEdorMainDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpEdorLRDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "无保全申请数据!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLPGrpEdorMainSchema.setSchema(tLPGrpEdorMainSet.get(1));
        mLPGrpEdorMainSchema.setGetMoney(mLPGrpEdorMainSchema.getGetMoney()+mLPGrpEdorItemSchema_input.getGetMoney());
        mLPGrpEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
        mLPGrpEdorMainSchema.setOperator(mGlobalInput.Operator);

        if (!mLPGrpEdorMainSchema.getEdorState().trim().equals("1"))
        {
            // @@错误处理
            mErrors.copyAllErrors(tLPGrpEdorMainDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpEdorLRDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保全已经申请确认不能修改!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema_input.getEdorNo());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema_input.getGrpContNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema_input.getEdorType());
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1)
        {
            // @@错误处理
            mErrors.copyAllErrors(tLPGrpEdorItemDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpEdorLRDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "无保全申请数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
        mLPGrpEdorItemSchema.setEdorState("1");
        mLPGrpEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
        mLPGrpEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPGrpEdorItemSchema.setGetMoney(mLPGrpEdorItemSchema_input.getGetMoney());
        mLPGrpEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSchema.setModifyTime(PubFun.getCurrentTime());


        return true;
    }
    private void dealGBInfo(){
    	 mMap.put("delete from LPEdorEspecialData "
                 + "where edorAcceptNo = '"
                 + mLPEdorEspecialDataSchema.getEdorNo() 
                 + "'  and edorType ='" + mLPEdorEspecialDataSchema.getEdorType()
                 + "'  and detailType = 'BBMONEY' ",
                 "DELETE");

        if(mLPEdorEspecialDataSchema == null)
        {
            return;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareData()
    {
    	mMap.put(mLPGrpEdorMainSchema, "UPDATE");
    	mMap.put(mLPGrpEdorItemSchema, "UPDATE");
        mMap.put(mLPEdorEspecialDataSchema, mOperate);

        mResult.clear();
        mResult.add(mMap);

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

}
