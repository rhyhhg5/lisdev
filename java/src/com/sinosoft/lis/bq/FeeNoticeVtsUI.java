package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;

/**
 * <p>Title: 生成交退费通知书</p>
 * <p>Description: 新生成的批单中包含了交费方式等内容 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */
public class FeeNoticeVtsUI
{
    private FeeNoticeVtsBL mFeeNoticeVtsBL = null;

    /**
     * 构造函数
     * @param edorAcceptNo String
     */
    public FeeNoticeVtsUI(String edorAcceptNo)
    {
        mFeeNoticeVtsBL = new FeeNoticeVtsBL(edorAcceptNo);
    }

    /**
     * 调用业务逻辑
     * @param payInfo TransferData
     * @return boolean
     */
    public boolean submitData(TransferData payInfo)
    {
        if (!mFeeNoticeVtsBL.submitData(payInfo))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mFeeNoticeVtsBL.mErrors.getFirstError();
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        TransferData payData = new TransferData();
        payData.setNameAndValue("payMode", "1");
        payData.setNameAndValue("payDate", "");
        payData.setNameAndValue("endDate", "2005-04-10");
        payData.setNameAndValue("bank", "");

        FeeNoticeVtsUI tFeeNoticeVtsUI = new FeeNoticeVtsUI("20060921000010");
        tFeeNoticeVtsUI.submitData(payData);
    }
}
