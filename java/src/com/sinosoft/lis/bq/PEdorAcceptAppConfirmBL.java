package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全受理申请确认</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class PEdorAcceptAppConfirmBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    String mPayPrintParams = "";




    /** 全局数据 */
    MMap map = new MMap();
    private GlobalInput mGlobalInput;
    private String mGrpContNo = "";
    private LPEdorAppSchema mLPEdorAppchema = new LPEdorAppSchema();
    //private LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
    private LPEdorMainSet mLPEdorMainSet = new LPEdorMainSet();

    public PEdorAcceptAppConfirmBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;


        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())return false;
        System.out.println("---End getInputData---");

        if (!checkData())return false;
        System.out.println("---End checkData---");

        //数据准备操作,校验该团体保全申请的明细信息是否完整
        if (!prepareData())return false;
        System.out.println("---End prepareData---");

        // 数据操作业务处理
        if (cOperate.equals("INSERT||EDORAPPCONFIRM"))
        {
            if (!dealData())return false;
            System.out.println("---End dealData---");
         }

         if (!prepareOutputData())
            return false;
        System.out.println("---End prepareOutputData---");


         PubSubmit tPubSubmit = new PubSubmit();
         System.out.println("Start PEdorAcceptAppConfirmBL Submit...");

         if (!tPubSubmit.submitData(mInputData, mOperate))
         {
             // @@错误处理
             this.mErrors.copyAllErrors(tPubSubmit.mErrors);

             CError tError = new CError();
             tError.moduleName = "PEdorAcceptAppConfirmBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据提交失败!";

             this.mErrors.addOneError(tError);
             return false;
         }


        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * 主要思路：根据传入的受理号（endorAcceptNo）查询到LpEdorMain表，
     * 然后根据主表下的项目进行理算 ，理算结束后更新LpedorApp下的费用。
     * @return
     */

    public boolean dealData()
    {
        VData pInputData = new VData();

        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tLPEdorMainSchema.setEdorAcceptNo(mLPEdorAppchema.getEdorAcceptNo());


        System.out.println("\nStart 受理下个人的理算 BL...");
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mLPEdorAppchema.getEdorAcceptNo());
        tLPEdorMainDB.setEdorState("1"); //保全申请未确认
        mLPEdorMainSet = tLPEdorMainDB.query();
        if (tLPEdorMainDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询个人保全失败！");
        }
        //处理保全受理下的项目，每一个项目都调用与之对应的确认类。
        for (int i = 1; i <= mLPEdorMainSet.size(); i++)
        {
            System.out.println("处理个人保全主表");
            pInputData.clear();
            pInputData.addElement(mGlobalInput);
            pInputData.addElement(mLPEdorAppchema);
            pInputData.addElement(mLPEdorMainSet.get(i));
            //调用EdorAppConfirmBL 处理具体项目的理算。
            EdorAppConfirmBL tEdorAppConfirmBL = new EdorAppConfirmBL();
            if (!tEdorAppConfirmBL.submitData(pInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tEdorAppConfirmBL.mErrors);
                return false;
            }
        }
        //理算结束后，更新状态，计算总的费用。
        String tEdorAcceptNo = mLPEdorAppchema.getEdorAcceptNo();
        mLPEdorAppchema.setEdorState("2");
        mLPEdorAppchema.setUWState("0");
        mLPEdorAppchema.setOperator(mGlobalInput.Operator);
        mLPEdorAppchema.setModifyDate(PubFun.getCurrentDate());
        mLPEdorAppchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLPEdorAppchema, "UPDATE");
        String wherePart = "where EdorAcceptNo='" +
                           tEdorAcceptNo + "'";
        map.put(
                "update LPEdorApp set ChgPrem= (select sum(ChgPrem) from LPEdorMain "
                + wherePart + ") " + wherePart, "UPDATE");
        map.put(
                "update LPEdorApp set ChgAmnt= (select sum(ChgAmnt) from LPEdorMain "
                + wherePart + ") " + wherePart, "UPDATE");
        map.put(
                "update LPEdorApp set GetMoney= (select sum(GetMoney) from LPEdorMain "
                + wherePart + ") " + wherePart, "UPDATE");
        map.put(
                "update LPEdorApp set GetInterest= (select sum(GetInterest) from LPEdorMain "
                + wherePart + ") " + wherePart, "UPDATE");

        System.out.println("End 申请下个人的申请确认 BL");

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData()
    {
        try
        {
            //得到保单号（避免依靠前台）
            mLPEdorAppchema = (LPEdorAppSchema) mInputData.
                                   getObjectByObjectName("LPEdorAppSchema",
                    0);
            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }
        LPEdorAppDB tLPEdorAppDB=new LPEdorAppDB();
        tLPEdorAppDB.setSchema(mLPEdorAppchema);
        if (!tLPEdorAppDB.getInfo())
        {
            CError.buildErr(this, "查询保全受理表失败!");
            return false;
        }
        else
        {
            mLPEdorAppchema.setSchema(tLPEdorAppDB.getSchema());
        }

        return true;
    }

    /**
     * 校验该团体保全申请的明细信息是否完整
     * @param aLPGrpEdorMainSchema
     * @return
     */
    private boolean checkData()
    {
       LPEdorItemDB tLPEdorItemDB=new LPEdorItemDB();
       LPEdorItemSet tLPEdorItemSet=new LPEdorItemSet();
       tLPEdorItemDB.setEdorAcceptNo(mLPEdorAppchema.getEdorAcceptNo());
       tLPEdorItemDB.setEdorState("3");//保全明细未录入
       tLPEdorItemSet=tLPEdorItemDB.query();
       if (tLPEdorItemDB.mErrors.needDealError())
       {
           CError.buildErr(this, "查询保全项目表失败!");
           return false;
       }
       if (tLPEdorItemSet.size()>0)
       {
           CError.buildErr(this, "该保全受理下还有保全项目明细没有录入!");
           return false;
       }


        return true;
    }

    /**
     * 准备需要保存的数据
     * @return

     */
    private boolean prepareData()
    {
//        //查询该申请下处于申请状态的保全数据
//        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
//        String strSql = "select * from LPGrpEdorItem "
//                        + " where EdorState='1' and GrpContNo='" +
//                        mLPEdorAppchema.getGrpContNo()
//                        + "' and EdorNo='" + mLPEdorAppchema.getEdorNo()
//                        + "' and ManageCom like '" + mGlobalInput.ManageCom
//                        + "%' order by EdorNo, MakeDate, MakeTime"; //去掉按EdorValiDate排序
//        System.out.println("sql:" + strSql);
//        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(
//                strSql);
//        if (tLPGrpEdorItemDB.mErrors.needDealError())
//        {
//            CError.buildErr(this,
//                            "查询批单号为" + mLPEdorAppchema.getEdorNo()
//                            + "的保全项目时失败！");
//            return false;
//        }
//
//        if (tLPGrpEdorItemSet == null)return false;
//
//        String tEdorNo = null;
//        for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++)
//        {
//            LPGrpEdorItemSchema tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(i);
//
//            //校验该团体保全申请的明细信息是否完整
//            if (!checkData(tLPGrpEdorItemSchema))return false;
//            System.out.println("End 校验该团体保全申请的明细信息是否完整");
//
////      if (i==1)
////      {
////        mLPGrpEdorItemSet.add(tLPGrpEdorItemSchema);
////        tEdorNo = tLPGrpEdorItemSchema.getEdorNo();
////      }
////      else
////      {
////        //确保批单号在mLpGrpEdorMainSet中唯一，前提是在前面的Sql语句中排了序
////        if (tLPGrpEdorItemSchema.getEdorNo().equals(tEdorNo))
////        {
////          continue;
////        }
////
////        mLPGrpEdorItemSet.add(tLPGrpEdorItemSchema);
////        tEdorNo = tLPGrpEdorItemSchema.getEdorNo();
////      }
//        }
//        System.out.println("团体保全主表记录数:" + mLPGrpEdorItemSet.size());

        return true;
    }

    private boolean prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
        mResult.add(map);
        return true;
    }


    /**
     * 取得相应的打印交费表需要的参数
     * @return
     */
    public String getPrtParams()
    {
        return mPayPrintParams;
    }

    public static void main(String[] args)
    {
        VData tInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        PEdorAcceptAppConfirmBL aPEdorAcceptAppConfirmBL = new PEdorAcceptAppConfirmBL();
        LPEdorAppSchema tLPEdorMainSchema = new LPEdorAppSchema();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "pa0001";

        tLPEdorMainSchema.setEdorAcceptNo("20050912000010");

        tInputData.addElement(tLPEdorMainSchema);
        tInputData.addElement(tGlobalInput);

        aPEdorAcceptAppConfirmBL.submitData(tInputData, "INSERT||EDORAPPCONFIRM");
    }

}
