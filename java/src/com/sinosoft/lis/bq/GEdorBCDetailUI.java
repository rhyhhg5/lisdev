package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:团体保全集体下个人功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */
public class GEdorBCDetailUI {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public GEdorBCDetailUI() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate)	{
    // 数据操作字符串拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    System.out.println("---GEdorDetail BL BEGIN---");
    GEdorBCDetailBL tGEdorBCDetailBL = new GEdorBCDetailBL();
System.out.println("-fff");
    if(tGEdorBCDetailBL.submitData(cInputData, cOperate) == false)	{
      // @@错误处理
      this.mErrors.copyAllErrors(tGEdorBCDetailBL.mErrors);
      mResult.clear();
      mResult.add(mErrors.getFirstError());
      return false;
    }	else {
      mResult = tGEdorBCDetailBL.getResult();
    }
    System.out.println("---GEdorDetail BL END---");

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 主函数，测试用
   */
  public static void main(String[] args) {
    System.out.println("-------test...");

    LPEdorMainSet tLPEdorMainSet   = new LPEdorMainSet();
     LPGrpEdorItemSchema tLPGrpEdorItemSchema   = new LPGrpEdorItemSchema();

     tLPGrpEdorItemSchema.setEdorAcceptNo("20050616000053");
     tLPGrpEdorItemSchema.setGrpContNo("0000000804");
     tLPGrpEdorItemSchema.setEdorNo("20050616000053");
     tLPGrpEdorItemSchema.setEdorType("BC");

     LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
            //个人保单批改主表信息
            tLPEdorMainSchema.setEdorAcceptNo("20050616000053");
            tLPEdorMainSchema.setEdorNo("20050616000053");
                tLPEdorMainSchema.setContNo("230110000000303");
            tLPEdorMainSet.add(tLPEdorMainSchema);
    LPEdorMainSchema tLPEdorMainSchema1 = new LPEdorMainSchema();

    tLPEdorMainSchema1.setEdorAcceptNo("86110000000247");
            tLPEdorMainSchema1.setEdorNo("430110000000044");
                tLPEdorMainSchema1.setContNo("230110000000301");
            tLPEdorMainSet.add(tLPEdorMainSchema1);



    GEdorBCDetailUI tGEdorBCDetailUI = new GEdorBCDetailUI();

    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.Operator = "001";
    tGlobalInput.ComCode = "86";
    tGlobalInput.ManageCom = "86";

    // 准备传输数据 VData

    VData tVData = new VData();

    //保存集体保单信息(保全)
    tVData.addElement(tLPEdorMainSet);
    tVData.addElement(tLPGrpEdorItemSchema);
    tVData.addElement(tGlobalInput);
    tGEdorBCDetailUI.submitData(tVData, "DELETE||EDOR");
  }
}
