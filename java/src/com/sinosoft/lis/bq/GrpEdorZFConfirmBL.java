package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LPGrpContDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.utility.VData;

import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.xb.PRnewAppCancelBL;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.xb.XBConst;
import com.sinosoft.lis.vschema.LPGrpContSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.SysConst;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpEdorZFConfirmBL implements EdorConfirm {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    public CErrors mCErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
  
    private LPGrpContSet mLPGrpContSet = null;
   

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();

    public GrpEdorZFConfirmBL() {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!dealData())
        {
            return false;
        }
        if (!prepareData())
        {
            return false ;
        }

        PubSubmit  tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, ""))
        {
        //数据提交 @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorCTDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("PEdorZFConfirmBL End PubSubmit");
        return true;

    }
    public VData getResult() {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData) {
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.getObjectByObjectName(
                "LPGrpEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData()
    {
        String EdorNo = mLPGrpEdorItemSchema.getEdorAcceptNo();
        /*
        if (!dealPRnew())
        {
            return false;
        }
        */
        //保单
        String Sqlpol = "select a.* from LPGrpCont a where a.EdorNo='" + EdorNo + "'";
        LPGrpContDB mLPGrpContDB = new LPGrpContDB();
        mLPGrpContSet = mLPGrpContDB.executeQuery(Sqlpol);
        if(mLPGrpContSet.size()==0)
        {
            this.mErrors.addOneError("dealData保单:" + mLPGrpEdorItemSchema.getGrpContNo() +
                                     "操作发生错误:保单不存在");
            return false;
        }
        for(int i=1 ; i<=mLPGrpContSet.size() ; i++)
        {
            String updatesql =" update lcgrpcont set state ='"+BQ.POLSTATE_ZFEND+"'"
                             +" ,modifydate='"+theCurrentDate+"'"
                             +" ,modifytime='"+theCurrentTime+"'"
                             +" where grpcontno ='"+mLPGrpContSet.get(i).getGrpContNo()+"'"
                             ;
            map.put(updatesql,SysConst.UPDATE);
        }

        return true ;
    }

    
    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareData() {
        mResult.clear();
        mResult.add(map);
        return true;
    }
}
