package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**s
 * <p>Title: 保全无名单实名化</p>
 * <p>Description: 项目明细录入</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorWSDetailBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = "WS";

    private String mGrpContNo = null;

    private String mMessage = null;

    private LPDiskImportSet mLPDiskImportSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    public int mImportPersonsForZTCF=0;    //重复人数  ******.

    public LPDiskImportSet mLPDiskImportSetForZTCF=null;   //重复人记录 ******


    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     * @param grpContNo String
     */
    public GrpEdorWSDetailBL(GlobalInput gi, String edorNo, String grpContNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = grpContNo;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到错误信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到磁盘导入数据
     * @return boolean
     */
    private boolean getInputData()
    {
    	//ytz---- limited all of the bug about badly case~~~20110810
    	if (mEdorNo==null||mEdorNo.equals("")||mEdorNo.equals("null"))
    	{
    		mErrors.addOneError("工单号为空，请重新导入数据。");
            return false;
    	}
    	if (mGrpContNo==null||mGrpContNo.equals("")||mGrpContNo.equals("null"))
    	{
    		mErrors.addOneError("团单号为空，请重新导入数据。");
            return false;
    	}
    
        mLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, mEdorType,
                mGrpContNo, BQ.IMPORTSTATE_SUCC);
        if (mLPDiskImportSet.size() == 0)
        {
            mErrors.addOneError("找不到磁盘导入数据！");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!checkData())
        {
            return false;
        }
        changeEdorState(BQ.EDORSTATE_INPUT);
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
    	 if(!checkValidate())
         {
         	return false;
         }
        if (!checkInsured())
        {
            return false;
        }
        if(!checkPerson())
        {
            return false;
        }
       

        return true;
    }
    
    /**
     * 无名单增人模板的Validate不能在交至日之后
     *  @return boolean
     */
    private boolean checkValidate()
    {
        boolean flag = true;
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            String errorReason = "";
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCContSchema tLCContSchema=new LCContSchema();
    		String date = new ExeSQL().getOneValue("select min(PaytoDate) from lccont where grpcontno='"+mGrpContNo+"' and poltype='1'");  	
    		int cha=PubFun.calInterval(tLPDiskImportSchema.getEdorValiDate(),date,"D");
    		if(cha<0)
    		{
    			errorReason += "保全的生效日期不能晚于保单的交至日";
                flag = false;
    		}
    		if (!errorReason.equals(""))
            {
                String sql = "update LPDiskImport " +
                        "set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                        " ErrorReason = '" + errorReason + "', " +
                        " Operator = '" + mGlobalInput.Operator + "', " +
                        " ModifyDate = '" + mCurrentDate + "', " +
                        " ModifyTime = '" + mCurrentTime + "' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + mEdorType + "' " +
                        "and GrpContNo = '" + mGrpContNo + "' " +
                        "and SerialNo = '" + tLPDiskImportSchema.getSerialNo() +
                        "' ";
                mMap.put(sql, "UPDATE");
            }
   
            
        }
        if (flag == false)
        {
            mMessage = "保全的生效日期不能晚于保单的交至日！";
            System.out.println(mMessage);
        }
        return true;
    }
    

    private boolean checkInsured()
    {
        boolean flag = true;
        LPDiskImportSet t = getIn(mLPDiskImportSet);
        mLPDiskImportSet.clear();
        mLPDiskImportSet.add(t);

        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            String errorReason = "";
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            tLCInsuredSchema.setGrpContNo(mGrpContNo);
            tLCInsuredSchema.setName(tLPDiskImportSchema.getInsuredName());
            tLCInsuredSchema.setSex(tLPDiskImportSchema.getSex());
            tLCInsuredSchema.setBirthday(tLPDiskImportSchema.getBirthday());
            tLCInsuredSchema.setIDType(tLPDiskImportSchema.getIDType());
            tLCInsuredSchema.setIDNo(tLPDiskImportSchema.getIDNo());
            OldCustomerCheck tOldCustomerCheck = new OldCustomerCheck(tLCInsuredSchema);
            if(("".equals(tLPDiskImportSchema.getSex()) || null == tLPDiskImportSchema.getSex()) || ("".equals(tLPDiskImportSchema.getBirthday()) || null == tLPDiskImportSchema.getBirthday())){
                errorReason += "被保人" + tLCInsuredSchema.getName() +
                "，性别或出生日期录入有误！";
                flag = false;
            }
            if(("".equals(tLPDiskImportSchema.getIDType()) || null == tLPDiskImportSchema.getIDType()) || ("".equals(tLPDiskImportSchema.getIDNo()) || null == tLPDiskImportSchema.getIDNo())){
                errorReason += "被保人" + tLCInsuredSchema.getName() +
                "，证件类型或证件号码录入有误！";
                flag = false;
            }
            if("".equals(tLPDiskImportSchema.getInsuredName()) || null == tLPDiskImportSchema.getInsuredName()){
                errorReason += "被保人" + tLCInsuredSchema.getName() +
                "，姓名录入有误！";
                flag = false;
            }
            if (tOldCustomerCheck.checkInsured() == OldCustomerCheck.OLD)
            {
                errorReason += "保单中已存在被保人" + tLCInsuredSchema.getName() +
                        "，不能重复导入！";
                flag = false;
            }else if("0".equals(tLPDiskImportSchema.getIDType()) || "5".equals(tLPDiskImportSchema.getIDType())){
            	String errorInfo=PubFun.CheckIDNo(tLPDiskImportSchema.getIDType(), tLPDiskImportSchema.getIDNo(), tLPDiskImportSchema.getBirthday(), tLPDiskImportSchema.getSex());
                if (!"".equals(errorInfo) && null != errorInfo)
                {
                	errorReason +="被保人" + tLPDiskImportSchema.getInsuredName()
                            + "的身份证信息录入有误，" + errorInfo;
                	flag = false;
                }
            }
            if (!errorReason.equals(""))
            {
                String sql = "update LPDiskImport " +
                        "set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                        " ErrorReason = '" + errorReason + "', " +
                        " Operator = '" + mGlobalInput.Operator + "', " +
                        " ModifyDate = '" + mCurrentDate + "', " +
                        " ModifyTime = '" + mCurrentTime + "' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + mEdorType + "' " +
                        "and GrpContNo = '" + mGrpContNo + "' " +
                        "and SerialNo = '" + tLPDiskImportSchema.getSerialNo() +
                        "' ";
                mMap.put(sql, "UPDATE");
            }
        }
        if (flag == false)
        {
            mMessage = "导入数据中有无效的被保人信息！";
            System.out.println(mMessage);
        }
        return true;
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void changeEdorState(String edorState)
    {
        String sql = "update  LPGrpEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }


    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    private boolean checkPerson()
    {
        if(mLPDiskImportSetForZTCF.size()>0)
        {
            for (int i = 1; i <= mLPDiskImportSetForZTCF.size(); i++)
            {
                String errorReason = "";
                LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSetForZTCF.get(i);
                System.out.println("重复人姓名："+tLPDiskImportSchema.getInsuredName());
                errorReason +="被保人" +tLPDiskImportSchema.getInsuredName()+"是重复导入的.";
                String sql = "update LPDiskImport " +
                                        "set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                                        " ErrorReason = '" + errorReason + "', " +
                                        " Operator = '" + mGlobalInput.Operator + "', " +
                                        " ModifyDate = '" + mCurrentDate + "', " +
                                        " ModifyTime = '" + mCurrentTime + "' " +
                                        "where EdorNo = '" + mEdorNo + "' " +
                                        "and EdorType = '" + mEdorType + "' " +
                                        "and GrpContNo = '" + mGrpContNo + "' " +
                                        "and SerialNo = '" + tLPDiskImportSchema.getSerialNo() +
                        "' ";
                  mMap.put(sql, "UPDATE");
                  mMessage = "有重复人被导入！";
          }
        }
        else
        {
            System.out.println("啥也没有,else");
            return true ;
        }
        return true ;
    }
/*
    // 校验重复人的方法:
    public LPDiskImportSet getIn(LPDiskImportSet mLPDiskImportSet)
    {
        HashMap hashmap = new HashMap();
        HashMap hashmap1 = new HashMap();
        LPDiskImportSet tLPDiskImportSet = new LPDiskImportSet();
        LPDiskImportSet tLPDiskImportSet1 = new LPDiskImportSet(); //用来记录重复的人信息
        for (int i = 1; i <= mLPDiskImportSet.size(); i++) {
            if (!hashmap.containsKey(mLPDiskImportSet.get(i).getIDNo()))
            {
                hashmap.put(mLPDiskImportSet.get(i).getIDNo(), "");
                tLPDiskImportSet.add(mLPDiskImportSet.get(i));
            }
            else
            {
                if (!hashmap1.containsKey(mLPDiskImportSet.get(i).getIDNo()))
                {
                    hashmap1.put(mLPDiskImportSet.get(i).getIDNo(), "");
                    tLPDiskImportSet1.add(mLPDiskImportSet.get(i)); //重复人的记录
                }
            }
    }

    mImportPersonsForZTCF = tLPDiskImportSet1.size();  //重复人数
    mLPDiskImportSetForZTCF=tLPDiskImportSet1; //重复人记录

    return tLPDiskImportSet;
}
 */   
    //20080821 add zhanggm 校验重复人的方法:
    public LPDiskImportSet getIn(LPDiskImportSet mLPDiskImportSet)
    {
        LPDiskImportSet tLPDiskImportSet = new LPDiskImportSet();
        LPDiskImportSet tLPDiskImportSet1 = new LPDiskImportSet(); //用来记录重复的人信息
        for (int i = 1; i <= mLPDiskImportSet.size(); i++) 
        {
        	LPDiskImportSchema mLPDiskImportSchema = mLPDiskImportSet.get(i);
        	if(checkLPDiskImport(tLPDiskImportSet,mLPDiskImportSchema))
    		{
    			tLPDiskImportSet1.add(mLPDiskImportSet.get(i)); //重复人的记录
    		}
    		else
    		{
    			tLPDiskImportSet.add(mLPDiskImportSet.get(i));
    		}
        }
        mImportPersonsForZTCF = tLPDiskImportSet1.size();  //重复人数
        mLPDiskImportSetForZTCF=tLPDiskImportSet1; //重复人记录

        return tLPDiskImportSet;

    }
    //20080821 add zhanggm 校验aLPDiskImportSchema是否在aLPDiskImportSet中,是:true,否：false
    private boolean checkLPDiskImport(LPDiskImportSet aLPDiskImportSet,LPDiskImportSchema aLPDiskImportSchema)
    {
    	if(aLPDiskImportSet.size()<1)
    	{
    		return false;
    	}
    	String mInsuredName = aLPDiskImportSchema.getInsuredName();
    	String mSex = aLPDiskImportSchema.getSex();
    	String mBirthday = aLPDiskImportSchema.getBirthday();
    	String mIDType = aLPDiskImportSchema.getIDType();
    	String mIDNo = aLPDiskImportSchema.getIDNo();
    	for (int i=1;i<=aLPDiskImportSet.size();i++)
    	{
			LPDiskImportSchema tLPDiskImportSchema = aLPDiskImportSet.get(i);
			String tInsuredName = tLPDiskImportSchema.getInsuredName();
        	String tSex = tLPDiskImportSchema.getSex();
        	String tBirthday = tLPDiskImportSchema.getBirthday();
        	String tIDType = tLPDiskImportSchema.getIDType();
        	String tIDNo = tLPDiskImportSchema.getIDNo();
        	if(mInsuredName.equals(tInsuredName) && mSex.equals(tSex) && mBirthday.equals(tBirthday) 
        			&& mIDType.equals(tIDType) && mIDNo.equals(tIDNo))
        	{
        		System.out.println("被保人"+tInsuredName+"是重复导入的！");
        		return true;
        	}
    	}
    	return false;
    }
}
