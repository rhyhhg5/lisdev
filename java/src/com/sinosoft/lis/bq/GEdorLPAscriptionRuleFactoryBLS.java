package com.sinosoft.lis.bq;


	import java.sql.Connection;

	import com.sinosoft.lis.vdb.LPAscriptionRuleFactoryDBSet;
	import com.sinosoft.lis.vdb.LPAscriptionRuleParamsDBSet;
	import com.sinosoft.lis.vschema.LPAscriptionRuleFactorySet;
	import com.sinosoft.lis.vschema.LPAscriptionRuleParamsSet;
	import com.sinosoft.utility.*;


	/**
	 * 保障计划要素后台提交
	 * <p>Title: </p>
	 * <p>Description: 根据操作类型，进行新增、删除、修改操作 </p>
	 * <p>Copyright: Copyright (c) 2004</p>
	 * <p>Company: SINOSOFT</p>
	 * @author ZHUXF
	 * @version 1.0
	 */
	public class GEdorLPAscriptionRuleFactoryBLS
	{
	    /** 错误处理类，每个需要错误处理的类中都放置该类 */
	    public CErrors mErrors = new CErrors();


	    /** 数据操作字符串 */
	    private String mOperate;
	    public GEdorLPAscriptionRuleFactoryBLS()
	    {
	    }

	    public static void main(String[] args)
	    {
	    }


	    /**
	      传输数据的公共方法
	     */
	    public boolean submitData(VData cInputData, String cOperate)
	    {
	        boolean tReturn = false;
	        //将操作数据拷贝到本类中
	        this.mOperate = cOperate;

	        System.out.println("Start LpAscriptionRuleFactoryBLS Submit...");
	        if (this.mOperate.equals("INSERT||MAIN"))
	        {
	            tReturn = saveLCAscriptionRule(cInputData);
	        }
	        if (this.mOperate.equals("DELETE||MAIN"))
	        {
	            tReturn = deleteLCAscriptionRule(cInputData);
	        }
	        if (this.mOperate.equals("UPDATE||MAIN"))
	        {
	            tReturn = updateLCAscriptionRule(cInputData);
	        }
	        if (tReturn)
	            System.out.println(" sucessful");
	        else
	            System.out.println("Save failed");
	        System.out.println("End LpAscriptionRuleFactoryBLS Submit...");
	        return tReturn;
	    }


	    /**
	     * 新增处理
	     * @param mInputData VData
	     * @return boolean
	     */
	    private boolean saveLCAscriptionRule(VData mInputData)
	    {
	        boolean tReturn = true;
	        System.out.println("Start Save...");
	        Connection conn;
	        conn = null;
	        conn = DBConnPool.getConnection();
	        if (conn == null)
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "LpAscriptionRuleFactoryBLS";
	            tError.functionName = "saveData";
	            tError.errorMessage = "数据库连接失败!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        try
	        {
	            conn.setAutoCommit(false);
	            System.out.println("Start 保存...");
	            LPAscriptionRuleFactoryDBSet tNewLPAscriptionRuleFactoryDBSet = new
	                    LPAscriptionRuleFactoryDBSet(conn);
	          //  LPAscriptionRuleFactoryDBSet tOldLPAscriptionRuleFactoryDBSet = new
	             //       LPAscriptionRuleFactoryDBSet(conn);
	            //获得删除数据set和新增数据set
	            tNewLPAscriptionRuleFactoryDBSet.set((LPAscriptionRuleFactorySet) mInputData.
	                                           get(1));
	          //  tOldLPAscriptionRuleFactoryDBSet.set((LPAscriptionRuleFactorySet) mInputData.
	          //                                 get(3));
	            //删除旧有数据
	        //    if (!tOldLPAscriptionRuleFactoryDBSet.delete())
	          //  {
	                // @@错误处理
	          //      this.mErrors.copyAllErrors(tOldLPAscriptionRuleFactoryDBSet.mErrors);
	          //      CError tError = new CError();
	         //       tError.moduleName = "LpAscriptionRuleFactoryBLS";
	         //       tError.functionName = "saveData";
	         //       tError.errorMessage = "数据保存失败!";
	         //       this.mErrors.addOneError(tError);
	           ///     conn.rollback();
	          //      conn.close();
	           //     return false;
	           // }
	            //插入新增数据
	            if (!tNewLPAscriptionRuleFactoryDBSet.insert())
	            {
	                // @@错误处理
	                this.mErrors.copyAllErrors(tNewLPAscriptionRuleFactoryDBSet.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "LpAscriptionRuleFactoryBLS";
	                tError.functionName = "saveData";
	                tError.errorMessage = "数据保存失败!";
	                this.mErrors.addOneError(tError);
	                conn.rollback();
	                conn.close();
	                return false;
	            }

	            LPAscriptionRuleParamsDBSet tNewLPAscriptionRuleParamsDBSet = new
	                    LPAscriptionRuleParamsDBSet(conn);
	         //   LPAscriptionRuleParamsDBSet tOldLCAscriptionRuleParamDBSet = new
	        //            LPAscriptionRuleParamsDBSet(conn);
	            //获得删除数据set和新增数据set
	            tNewLPAscriptionRuleParamsDBSet.set((LPAscriptionRuleParamsSet) mInputData.get(2));
	         //   tOldLCAscriptionRuleParamDBSet.set((LPAscriptionRuleParamsSet) mInputData.get(4));
	            //删除旧有数据
	         //   if (!tOldLCAscriptionRuleParamDBSet.delete())
	         //   {
	                // @@错误处理
	          //      this.mErrors.copyAllErrors(tOldLCAscriptionRuleParamDBSet.mErrors);
	          //      CError tError = new CError();
	           //     tError.moduleName = "LpAscriptionRuleFactoryBLS";
	          ////      tError.functionName = "saveData";
	            //    tError.errorMessage = "数据保存失败!";
	            //    this.mErrors.addOneError(tError);
	           //     conn.rollback();
	           //     conn.close();
	           //     return false;
	          //  }
	            //插入新增数据
	            if (!tNewLPAscriptionRuleParamsDBSet.insert())
	            {
	                // @@错误处理
	                this.mErrors.copyAllErrors(tNewLPAscriptionRuleParamsDBSet.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "LpAscriptionRuleFactoryBLS";
	                tError.functionName = "saveData";
	                tError.errorMessage = "数据保存失败!";
	                this.mErrors.addOneError(tError);
	                conn.rollback();
	                conn.close();
	                return false;
	            }
	            conn.commit();
	            conn.close();
	        }
	        catch (Exception ex)
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "LpAscriptionRuleFactoryBLS";
	            tError.functionName = "submitData";
	            tError.errorMessage = ex.toString();
	            this.mErrors.addOneError(tError);
	            tReturn = false;
	            try
	            {
	                conn.rollback();
	                conn.close();
	            }
	            catch (Exception e)
	            {}
	        }
	        return tReturn;
	    }


	    /**
	     * 删除处理
	     * @param mInputData VData
	     * @return boolean
	     */
	    private boolean deleteLCAscriptionRule(VData mInputData)
	    {
	        boolean tReturn = true;
	        System.out.println("Start Del...");
	        Connection conn;
	        conn = null;
	        conn = DBConnPool.getConnection();
	        if (conn == null)
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "LpAscriptionRuleFactoryBLS";
	            tError.functionName = "saveData";
	            tError.errorMessage = "数据库连接失败!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        try
	        {
	            conn.setAutoCommit(false);
	            System.out.println("Del 删除...");

	            LPAscriptionRuleFactoryDBSet tOldLPAscriptionRuleFactoryDBSet = new
	                    LPAscriptionRuleFactoryDBSet(conn);
	            //获得删除数据set
	            tOldLPAscriptionRuleFactoryDBSet.set((LPAscriptionRuleFactorySet) mInputData.
	                                           get(3));
	            System.out.println("---old ascriptiont set.size:"+tOldLPAscriptionRuleFactoryDBSet.size());
	            System.out.println("tOldLPAscriptionRuleFactoryDBSet:"+tOldLPAscriptionRuleFactoryDBSet.encode());
	            //删除旧有数据
	            if (!tOldLPAscriptionRuleFactoryDBSet.delete())
	            {
	                // @@错误处理
	                this.mErrors.copyAllErrors(tOldLPAscriptionRuleFactoryDBSet.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "LpAscriptionRuleFactoryBLS";
	                tError.functionName = "saveData";
	                tError.errorMessage = "数据保存失败!";
	                this.mErrors.addOneError(tError);
	                conn.rollback();
	                conn.close();
	                return false;
	            }

	            LPAscriptionRuleParamsDBSet tOldLPAscriptionRuleParamsDBSet = new
	                    LPAscriptionRuleParamsDBSet(conn);
	            //获得删除数据set
	            tOldLPAscriptionRuleParamsDBSet.set((LPAscriptionRuleParamsSet) mInputData.get(4));
	            //删除旧有数据
	            if (!tOldLPAscriptionRuleParamsDBSet.delete())
	            {
	                // @@错误处理
	                this.mErrors.copyAllErrors(tOldLPAscriptionRuleParamsDBSet.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "LpAscriptionRuleFactoryBLS";
	                tError.functionName = "saveData";
	                tError.errorMessage = "数据保存失败!";
	                this.mErrors.addOneError(tError);
	                conn.rollback();
	                conn.close();
	                return false;
	            }

	            conn.commit();
	            conn.close();
	        }
	        catch (Exception ex)
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "LpAscriptionRuleFactoryBLS";
	            tError.functionName = "submitData";
	            tError.errorMessage = ex.toString();
	            this.mErrors.addOneError(tError);
	            tReturn = false;
	            try
	            {
	                conn.rollback();
	                conn.close();
	            }
	            catch (Exception e)
	            {}
	        }
	        return tReturn;
	    }


	    /**
	     * 保存函数
	     */
	    private boolean updateLCAscriptionRule(VData mInputData)
	    {
	        boolean tReturn = true;
	        System.out.println("Start Save...");
	        Connection conn;
	        conn = null;
	        conn = DBConnPool.getConnection();
	        if (conn == null)
	        {
	            CError tError = new CError();
	            tError.moduleName = "LpAscriptionRuleFactoryBLS";
	            tError.functionName = "updateData";
	            tError.errorMessage = "数据库连接失败!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        try
	        {
	          conn.setAutoCommit(false);
	          System.out.println("Start 修改...");
	          LPAscriptionRuleFactoryDBSet tLPAscriptionRuleFactoryDBSet = new LPAscriptionRuleFactoryDBSet(conn);
	          tLPAscriptionRuleFactoryDBSet.set((LPAscriptionRuleFactorySet) mInputData.
	                               getObjectByObjectName(
	                  "LPAscriptionRuleFactorySet", 0));
	          //先执行删除操作   
	          ExeSQL tExeSQL = new ExeSQL(conn);
	          String tSql = "delete from LpAscriptionRuleFactory where GrpContNo='" +
	                         tLPAscriptionRuleFactoryDBSet.get(1).getGrpContNo() +
//	chenwm20070924 不知道这为什么要用sql语句来写,前面不是已经把需要删除的set已经放到mInputData里了, 
	                         //还有关联了险种,如果是改险种的话就会导致旧的不能删,又添加了新的                    
//	                         "' and RiskCode='" +
//	                         tLPAscriptionRuleFactoryDBSet.get(1).getRiskCode() +
	                         "' and AscriptionRuleCode='" +
	                         tLPAscriptionRuleFactoryDBSet.get(1).getAscriptionRuleCode() + "'";
	          if (!tExeSQL.execUpdateSQL(tSql))
	          {
	              // @@错误处理
	              CError tError = new CError();
	              tError.moduleName = "LpAscriptionRuleFactoryBLS";
	              tError.functionName = "saveData";
	              tError.errorMessage = "数据保存失败!";
	              this.mErrors.addOneError(tError);
	              conn.rollback();
	              conn.close();
	              return false;
	          }

	          tSql = "delete from LpAscriptionRuleParams where GrpContNo='" +
	                  tLPAscriptionRuleFactoryDBSet.get(1).getGrpContNo() +
//	                  "' and RiskCode='" +
//	                  tLPAscriptionRuleFactoryDBSet.get(1).getRiskCode() +
	                  "' and AscriptionRuleCode='" +
	                  tLPAscriptionRuleFactoryDBSet.get(1).getAscriptionRuleCode() + "'";
	           if (!tExeSQL.execUpdateSQL(tSql))
	           {
	               // @@错误处理
	               CError tError = new CError();
	               tError.moduleName = "LpAscriptionRuleFactoryBLS";
	               tError.functionName = "saveData";
	               tError.errorMessage = "数据保存失败!";
	               this.mErrors.addOneError(tError);
	               conn.rollback();
	               conn.close();
	               return false;
	           }

	           //System.out.println("-------factoryset:"+tLPAscriptionRuleFactoryDBSet.size());
	           //System.out.println("-------grppolno:"+tLPAscriptionRuleFactoryDBSet.get(1).getGrpPolNo());
	           //删除成功，才执行新增操作，实现修改目的
	          if (!tLPAscriptionRuleFactoryDBSet.insert())
	          {
	              // @@错误处理
	              CError tError = new CError();
	              tError.moduleName = "LpAscriptionRuleFactoryBLS";
	              tError.functionName = "saveData";
	              tError.errorMessage = "数据保存失败!";
	              this.mErrors.addOneError(tError);
	              conn.rollback();
	              conn.close();
	              return false;
	          }

	          LPAscriptionRuleParamsDBSet tLPAscriptionRuleParamsDBSet = new LPAscriptionRuleParamsDBSet(conn);
	          tLPAscriptionRuleParamsDBSet.set((LPAscriptionRuleParamsSet)mInputData.
	                                    getObjectByObjectName("LPAscriptionRuleParamsSet",0));
	          //System.out.println("-------params set:"+tLPAscriptionRuleParamsDBSet.size());
	          //System.out.println("--grppolno:"+tLPAscriptionRuleParamsDBSet.get(1).getGrpPolNo());
	          if(!tLPAscriptionRuleParamsDBSet.insert())
	          {
	              // @@错误处理
	              CError tError = new CError();
	              tError.moduleName = "LpAscriptionRuleFactoryBLS";
	              tError.functionName = "saveData";
	              tError.errorMessage = "数据保存失败!";
	              this.mErrors.addOneError(tError);
	              conn.rollback();
	              conn.close();
	              return false;
	          }

	            conn.commit();
	            conn.close();
	        }
	        catch (Exception ex)
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "LpAscriptionRuleFactoryBLS";
	            tError.functionName = "submitData";
	            tError.errorMessage = ex.toString();
	            this.mErrors.addOneError(tError);
	            tReturn = false;
	            try
	            {
	                conn.rollback();
	                conn.close();
	            }
	            catch (Exception e)
	            {}
	        }
	        return tReturn;
	    }
	}

