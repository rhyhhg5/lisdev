package com.sinosoft.lis.bq;

//程序名称：PEdorBPConfirmBL.java
//程序功能：
//创建日期：2009-07-06
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LPContDB;
import com.sinosoft.lis.db.LPDutyDB;
import com.sinosoft.lis.db.LPGetDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.db.LPPremDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPDutySchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPGetSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.schema.LPPremSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.lis.vschema.LPDutySet;
import com.sinosoft.lis.vschema.LPGetSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.lis.vschema.LPPremSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;


public class PEdorTPConfirmBL implements EdorConfirm
{
private MMap mMap = new MMap();

private GlobalInput mGlobalInput = null;

private LPEdorItemSchema mLPEdorItemSchema = null;

private String mEdorNo = null;

private String mEdorType = null;

private Reflections ref = new Reflections();
private String mContNo = null;

private ValidateEdorData2 mValidateEdorData = null;

/**
 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
 * @param cInputData VData
 * @param cOperate String
 * @return boolean
 */
public boolean submitData(VData cInputData, String cOperate)
{
    getInputData(cInputData);
    if (!dealData())
    {
        return false;
    }
    return true;
}

/**
 * 返回计算结果
 * @return VData
 */
public VData getResult()
{
    VData data = new VData();
    data.add(mMap);
    return data;
}

/**
 * 将外部传入的数据分解到本类的属性中
 * @param cInputData VData
 */
private void getInputData(VData cInputData)
{
    mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    mLPEdorItemSchema = (LPEdorItemSchema)cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
    mEdorNo = mLPEdorItemSchema.getEdorNo();
    mEdorType = mLPEdorItemSchema.getEdorType();
    mContNo = mLPEdorItemSchema.getContNo();
    mValidateEdorData = new ValidateEdorData2(mGlobalInput, mEdorNo,mEdorType, mContNo, "ContNo");
}

/**
 * 处理业务逻辑
 * @return boolean
 */
private boolean dealData()
{
	//处理lcget
	if(!dealGET()){
		return false;
	}
	//处理lcprem
	if(!dealPrem()){
		return false;
	}
	//处理lcduty
	if(!dealDuty()){
		return false;
	}
	//处理lcpol
	if(!dealPol()){
		return false;
	}
	//处理lccont
	if(!dealCont()){
		return false;
	}
	return true;
}

private boolean dealGET(){
	String getCSQL = "select * from lcget where contno='"+mContNo+"'";
	LCGetDB tLCGetDB = new LCGetDB();
	String getPSQL = "select * from lpget where contno='"+mContNo+"' and edorno='"+mEdorNo+"'";
	LPGetDB tLPGetDB = new LPGetDB();
	LCGetSet tLCGetSet = new LCGetSet();
	LCGetSet tILCGetSet = new LCGetSet();
	LPGetSet tLPGetSet = new LPGetSet();
	LPGetSet tILPGetSet = new LPGetSet();
	tLCGetSet = tLCGetDB.executeQuery(getCSQL);
	tLPGetSet = tLPGetDB.executeQuery(getPSQL);
	for(int i=1;i<=tLCGetSet.size();i++){
		LCGetSchema tLCGetSchema = tLCGetSet.get(i);
		LPGetSchema tLPGetSchema = new LPGetSchema();
		ref.transFields(tLPGetSchema,tLCGetSchema);
		tLPGetSchema.setEdorNo(mEdorNo);
		tLPGetSchema.setEdorType(mEdorType);
		tILPGetSet.add(tLPGetSchema);
		
	}
	for(int i=1;i<=tLPGetSet.size();i++){
		LPGetSchema tLPGetSchema = tLPGetSet.get(i);
		LCGetSchema tLCGetSchema = new LCGetSchema();
		ref.transFields(tLCGetSchema,tLPGetSchema);
		tILCGetSet.add(tLCGetSchema);
	}
	//删除原来的数据
	String deletePGetSQL = "delete from lcget where contno='"+mContNo+"'";
	String deleteCGetSQL = "delete from lpget where contno='"+mContNo+"' and edorno='"+mEdorNo+"'";
	mMap.put(deletePGetSQL, SysConst.DELETE);
	mMap.put(deleteCGetSQL, SysConst.DELETE);
	mMap.put(tILCGetSet, SysConst.DELETE_AND_INSERT);
	mMap.put(tILPGetSet, SysConst.DELETE_AND_INSERT);
	return true;
}

private boolean dealPrem(){
	String PremCSQL = "select * from lcPrem where contno='"+mContNo+"'";
	LCPremDB tLCPremDB = new LCPremDB();
	String PremPSQL = "select * from lpPrem where contno='"+mContNo+"' and edorno='"+mEdorNo+"'";
	LPPremDB tLPPremDB = new LPPremDB();
	LCPremSet tLCPremSet = new LCPremSet();
	LCPremSet tILCPremSet = new LCPremSet();
	LPPremSet tLPPremSet = new LPPremSet();
	LPPremSet tILPPremSet = new LPPremSet();
	tLCPremSet = tLCPremDB.executeQuery(PremCSQL);
	tLPPremSet = tLPPremDB.executeQuery(PremPSQL);
	for(int i=1;i<=tLCPremSet.size();i++){
		LCPremSchema tLCPremSchema = tLCPremSet.get(i);
		LPPremSchema tLPPremSchema = new LPPremSchema();
		ref.transFields(tLPPremSchema,tLCPremSchema);
		tLPPremSchema.setEdorNo(mEdorNo);
		tLPPremSchema.setEdorType(mEdorType);
		tILPPremSet.add(tLPPremSchema);
		
	}
	for(int i=1;i<=tLPPremSet.size();i++){
		LPPremSchema tLPPremSchema = tLPPremSet.get(i);
		LCPremSchema tLCPremSchema = new LCPremSchema();
		ref.transFields(tLCPremSchema,tLPPremSchema);
		tILCPremSet.add(tLCPremSchema);
	}
	//删除原来的数据
	String deletePPremSQL = "delete from lcPrem where contno='"+mContNo+"'";
	String deleteCPremSQL = "delete from lpPrem where contno='"+mContNo+"' and edorno='"+mEdorNo+"'";
	mMap.put(deletePPremSQL, SysConst.DELETE);
	mMap.put(deleteCPremSQL, SysConst.DELETE);
	mMap.put(tILCPremSet, SysConst.DELETE_AND_INSERT);
	mMap.put(tILPPremSet, SysConst.DELETE_AND_INSERT);
	return true;
}

private boolean dealDuty(){
	String DutyCSQL = "select * from lcDuty where contno='"+mContNo+"'";
	LCDutyDB tLCDutyDB = new LCDutyDB();
	String DutyPSQL = "select * from lpDuty where contno='"+mContNo+"' and edorno='"+mEdorNo+"'";
	LPDutyDB tLPDutyDB = new LPDutyDB();
	LCDutySet tLCDutySet = new LCDutySet();
	LCDutySet tILCDutySet = new LCDutySet();
	LPDutySet tLPDutySet = new LPDutySet();
	LPDutySet tILPDutySet = new LPDutySet();
	tLCDutySet = tLCDutyDB.executeQuery(DutyCSQL);
	tLPDutySet = tLPDutyDB.executeQuery(DutyPSQL);
	for(int i=1;i<=tLCDutySet.size();i++){
		LCDutySchema tLCDutySchema = tLCDutySet.get(i);
		LPDutySchema tLPDutySchema = new LPDutySchema();
		ref.transFields(tLPDutySchema,tLCDutySchema);
		tLPDutySchema.setEdorNo(mEdorNo);
		tLPDutySchema.setEdorType(mEdorType);
		tILPDutySet.add(tLPDutySchema);
		
	}
	for(int i=1;i<=tLPDutySet.size();i++){
		LPDutySchema tLPDutySchema = tLPDutySet.get(i);
		LCDutySchema tLCDutySchema = new LCDutySchema();
		ref.transFields(tLCDutySchema,tLPDutySchema);
		tILCDutySet.add(tLCDutySchema);
	}
	
	//删除原来的数据
	String deletePDutySQL = "delete from lcDuty where contno='"+mContNo+"'";
	String deleteCDutySQL = "delete from lpDuty where contno='"+mContNo+"' and edorno='"+mEdorNo+"'";
	mMap.put(deletePDutySQL, SysConst.DELETE);
	mMap.put(deleteCDutySQL, SysConst.DELETE);
	mMap.put(tILCDutySet, SysConst.DELETE_AND_INSERT);
	mMap.put(tILPDutySet, SysConst.DELETE_AND_INSERT);
	return true;
}


private boolean dealPol(){
	String PolCSQL = "select * from lcPol where contno='"+mContNo+"'";
	LCPolDB tLCPolDB = new LCPolDB();
	String PolPSQL = "select * from lpPol where contno='"+mContNo+"' and edorno='"+mEdorNo+"'";
	LPPolDB tLPPolDB = new LPPolDB();
	LCPolSet tLCPolSet = new LCPolSet();
	LCPolSet tILCPolSet = new LCPolSet();
	LPPolSet tLPPolSet = new LPPolSet();
	LPPolSet tILPPolSet = new LPPolSet();
	tLCPolSet = tLCPolDB.executeQuery(PolCSQL);
	tLPPolSet = tLPPolDB.executeQuery(PolPSQL);
	for(int i=1;i<=tLCPolSet.size();i++){
		LCPolSchema tLCPolSchema = tLCPolSet.get(i);
		LPPolSchema tLPPolSchema = new LPPolSchema();
		ref.transFields(tLPPolSchema,tLCPolSchema);
		tLPPolSchema.setEdorNo(mEdorNo);
		tLPPolSchema.setEdorType(mEdorType);
		tILPPolSet.add(tLPPolSchema);
		
	}
	for(int i=1;i<=tLPPolSet.size();i++){
		LPPolSchema tLPPolSchema = tLPPolSet.get(i);
		LCPolSchema tLCPolSchema = new LCPolSchema();
		ref.transFields(tLCPolSchema,tLPPolSchema);
		tILCPolSet.add(tLCPolSchema);
	}
	mMap.put(tILCPolSet, SysConst.DELETE_AND_INSERT);
	mMap.put(tILPPolSet, SysConst.DELETE_AND_INSERT);
	return true;
}

private boolean dealCont(){
	String ContCSQL = "select * from lcCont where contno='"+mContNo+"'";
	LCContDB tLCContDB = new LCContDB();
	String ContPSQL = "select * from lpCont where contno='"+mContNo+"' and edorno='"+mEdorNo+"'";
	LPContDB tLPContDB = new LPContDB();
	LCContSet tLCContSet = new LCContSet();
	LCContSet tILCContSet = new LCContSet();
	LPContSet tLPContSet = new LPContSet();
	LPContSet tILPContSet = new LPContSet();
	tLCContSet = tLCContDB.executeQuery(ContCSQL);
	tLPContSet = tLPContDB.executeQuery(ContPSQL);
	for(int i=1;i<=tLCContSet.size();i++){
		LCContSchema tLCContSchema = tLCContSet.get(i);
		LPContSchema tLPContSchema = new LPContSchema();
		ref.transFields(tLPContSchema,tLCContSchema);
		tLPContSchema.setEdorNo(mEdorNo);
		tLPContSchema.setEdorType(mEdorType);
		tILPContSet.add(tLPContSchema);
		
	}
	for(int i=1;i<=tLPContSet.size();i++){
		LPContSchema tLPContSchema = tLPContSet.get(i);
		LCContSchema tLCContSchema = new LCContSchema();
		ref.transFields(tLCContSchema,tLPContSchema);
		tILCContSet.add(tLCContSchema);
	}
	mMap.put(tILCContSet, SysConst.DELETE_AND_INSERT);
	mMap.put(tILPContSet, SysConst.DELETE_AND_INSERT);
	return true;
}

/**
 * 使保全数据生效
 * @return boolean
 */
private boolean validateEdorData()
{
    String[] chgTables = {"LCPol","LCPol", "LCCont","LCPrem","LCGet"};
    mValidateEdorData.changeData(chgTables);
    mMap.add(mValidateEdorData.getMap());
    return true;
}
}
