package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

//程序名称：PEdorZXAppConfirm.java
//程序功能：
//创建日期：2010-07-29
//创建人  ：XP
//更新记录：  更新人    更新日期     更新原因/内容
public class PEdorZXAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = null;

    /** 合同号 */
    private String mContNo = null;

    /** 保单险种号 */
    private String mPolNo = null;

    /** 险种代码 */
    private String mRiskCode = null;

    /** 保单险种 */
    private LCPolSchema mLCPolSchema = null;

    /** 保全生效日期 */
    private String mEdorValiDate = null;

    /** 保全交费总额 */
    private double mGetMoney = 0.00;

    /** 保全项目特殊数据 */
    private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = null;

    private DetailDataQuery mQuery = null;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mVtsData = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private LCInsureAccSchema mLCInsureAccSchema = null;

    private LCInsureAccClassSchema mLCInsureAccClassSchema = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            System.out.println("PEdorZXAppConfirm.java->getInputData(cInputData)失败");
        	return false;
        }

        if (!dealData())
        {
        	System.out.println("PEdorZXAppConfirm.java->dealData()失败");
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema)cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
        mEdorNo = mLPEdorItemSchema.getEdorNo();
        mEdorType = mLPEdorItemSchema.getEdorType();
        mContNo = mLPEdorItemSchema.getContNo();
        mEdorValiDate = mLPEdorItemSchema.getEdorValiDate();
        String sql = "select * from LCPol a where ContNo = '" + mLPEdorItemSchema.getContNo() + "' "
                   + "and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') "
                   + "with ur";
        System.out.println("查询万能险种是否存在：" + sql);
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolSet = tLCPolDB.executeQuery(sql);
        if (tLCPolSet.size() == 0)
        {
         mErrors.addOneError("找不到万能险种信息！");
         return false;
        }
        else
        {
        	mPolNo = tLCPolSet.get(1).getPolNo();
        	mRiskCode = tLCPolSet.get(1).getRiskCode();
        }
        mLCPolSchema = tLCPolSet.get(1).getSchema();
        mLCInsureAccSchema = CommonBL.getLCInsureAcc(mPolNo,CommonBL.getInsuAccNo("002", mRiskCode));
        if (mLCInsureAccSchema == null)
        {
            mErrors.addOneError("找不到万能账户信息！");
            return false;
        }

        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setPolNo(mPolNo);
        tLCInsureAccClassDB.setInsuAccNo(mLCInsureAccSchema.getInsuAccNo());
        mLCInsureAccClassSchema = tLCInsureAccClassDB.query().get(1);
        if (mLCInsureAccClassSchema == null)
        {
        	mErrors.addOneError("找不到万能账户分类！");
            return false;
        }

        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(mEdorNo);
        tLPEdorEspecialDataDB.setEdorType(mEdorType);
        tLPEdorEspecialDataDB.setDetailType("AppMoney");
        mVtsData = new EdorItemSpecialData(mLPEdorItemSchema);
        mLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
        mLPEdorEspecialDataSchema = tLPEdorEspecialDataDB.query().get(1);
        mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!setInsuredAcc())
        {
            return false;
        }
        setEdorItem();
        return true;
    }

    /**
     * 设置被保人账户保费金额
     * @return boolean
     */
    private boolean setInsuredAcc()
    {
        double insuredMoney = Double.parseDouble(mLPEdorEspecialDataSchema.getEdorValue());
        double leftMoney = CommonBL.carry(mLCInsureAccSchema.getInsuAccBala() - insuredMoney); //计算余额
        double getMoney = insuredMoney ;  //计算实领金额
        if (leftMoney < 0)
        {
            mErrors.addOneError("被保人" + mLCInsureAccSchema.getInsuredNo() + "的账户余额不足！");
            return false;
        }
        LCPolSchema tLCPolSchema = CommonBL.getLCPol(mPolNo);
        if (tLCPolSchema == null)
        {
            mErrors.addOneError("找不到客户" + mLCInsureAccSchema.getInsuredNo() + "的险种信息！");
            return false;
        }
        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema1 = new LPEdorEspecialDataSchema();
        tLPEdorEspecialDataSchema1.setEdorAcceptNo(mEdorNo);
        tLPEdorEspecialDataSchema1.setEdorNo(mEdorNo);
        tLPEdorEspecialDataSchema1.setEdorType(mEdorType);
        tLPEdorEspecialDataSchema1.setDetailType("LeftMoney");
        tLPEdorEspecialDataSchema1.setPolNo("000000");
        tLPEdorEspecialDataSchema1.setEdorValue(String.valueOf(leftMoney));

        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema3 = new LPEdorEspecialDataSchema();
        tLPEdorEspecialDataSchema3.setEdorAcceptNo(mEdorNo);
        tLPEdorEspecialDataSchema3.setEdorNo(mEdorNo);
        tLPEdorEspecialDataSchema3.setEdorType(mEdorType);
        tLPEdorEspecialDataSchema3.setDetailType("GetMoney");
        tLPEdorEspecialDataSchema3.setPolNo("000000");
        tLPEdorEspecialDataSchema3.setEdorValue(String.valueOf(getMoney));
        mMap.put(tLPEdorEspecialDataSchema1, "DELETE&INSERT");
        mMap.put(tLPEdorEspecialDataSchema3, "DELETE&INSERT");

        setLPInsureAcc(mLCInsureAccClassSchema, insuredMoney, leftMoney);
        setLPInsureAccTrace(mLCInsureAccClassSchema, insuredMoney);
        setLPInsureAccClass(mLCInsureAccClassSchema, insuredMoney, leftMoney);
        setGetEndorse(tLCPolSchema, getMoney);
        mGetMoney = getMoney;
        return true;
    }

    private void setLPInsureAcc(LCInsureAccClassSchema aLCInsureAccClassSchema, double money, double leftMoney)
    {
        double sumPaym = aLCInsureAccClassSchema.getSumPaym() + money; //累计领取
        LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPInsureAccSchema, aLCInsureAccClassSchema);
        tLPInsureAccSchema.setEdorNo(mEdorNo);
        tLPInsureAccSchema.setEdorType(mEdorType);
        tLPInsureAccSchema.setInsuAccBala(leftMoney);
        tLPInsureAccSchema.setSumPaym(sumPaym);
        tLPInsureAccSchema.setPrtNo(mLCInsureAccSchema.getPrtNo());
        tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccSchema.setModifyDate(mCurrentDate);
        tLPInsureAccSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
    }

    /**
     * 设置帐户领取轨迹
     * @param aLCInsureAccClassSchema LCInsureAccClassSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTrace(LCInsureAccClassSchema aLCInsureAccClassSchema, double grpMoney)
    {
        String serialNo;
        LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
        tLPInsureAccTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccTraceDB.setEdorType(mEdorType);
        tLPInsureAccTraceDB.setContNo(mContNo);
        tLPInsureAccTraceDB.setPolNo(mPolNo);
        tLPInsureAccTraceDB.setInsuAccNo(aLCInsureAccClassSchema.getInsuAccNo());
        LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
        if (tLPInsureAccTraceSet.size() > 0)
        {
            serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccTrace " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        }
        else
        {
        	serialNo = PubFun1.CreateMaxNo("SERIALNO", aLCInsureAccClassSchema.getManageCom());
        }
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccClassSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccClassSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(aLCInsureAccClassSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(aLCInsureAccClassSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccClassSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccClassSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(aLCInsureAccClassSchema.getPayPlanCode());
        tLPInsureAccTraceSchema.setManageCom(aLCInsureAccClassSchema.getManageCom());
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(BQ.EDORTYPE_LQ);
        tLPInsureAccTraceSchema.setMoney(-Math.abs(grpMoney));  //退费
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("10");
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }

    private void setLPInsureAccClass(LCInsureAccClassSchema aLCInsureAccClassSchema, double money, double leftMoney)
    {
        double sumPaym = aLCInsureAccClassSchema.getSumPaym() + money; //累计领取
        LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPInsureAccClassSchema, aLCInsureAccClassSchema);
        tLPInsureAccClassSchema.setEdorNo(mEdorNo);
        tLPInsureAccClassSchema.setEdorType(mEdorType);
        tLPInsureAccClassSchema.setInsuAccBala(leftMoney);
        tLPInsureAccClassSchema.setSumPaym(sumPaym);
        tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
        tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
        tLPInsureAccClassSchema.setAccAscription("0");
        mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
    }



	/**
     * 设置批改补退费表
     * @return boolean
     */
    private boolean setGetEndorse(LCPolSchema aLCPolSchema, double edorPrem)
    {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGrpContNo(aLCPolSchema.getGrpContNo());
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
        tLJSGetEndorseSchema.setGetMoney(-Math.abs(edorPrem));
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_TB);//万能部分领取的补/退费财务类型属于冲退保金
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(mLCInsureAccClassSchema.getPayPlanCode());
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType("10");
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 设置item表中的费用和状态
     */
    private void setEdorItem()
    {
        System.out.println(mGetMoney);
        String sql = "update LPEdorItem " +
                "set GetMoney = -" + mGetMoney + ", " + //这里是负数
                "EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                "ModifyDate = '" + mCurrentDate + "', " +
                "ModifyTime = '" + mCurrentTime + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql.toString(), "UPDATE");
    }
 }
