package com.sinosoft.lis.bq;

import java.io.*;
import org.jdom.input.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 生成交退费通知书</p>
 * <p>Description: 新生成的批单中包含了交费方式等内容 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */
public class PrtManuUWInfo
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    /** 保全受理号 */
    private String mEdorAcceptNo = null;

    /** 保存批单数据的Schema */
    private LPEdorAppPrintSchema mPrintSchema = null;

    /** XML操作 */
    private XmlExport xmlExport = new XmlExport();

    /**
     * 构造函数
     * @param edorAcceptNo String
     */
    public PrtManuUWInfo(String edorAcceptNo)
    {
        this.mEdorAcceptNo = edorAcceptNo;
    }

    /**
     * 得到待提交的数据
     * @return MMap
     */
    public MMap getSubmitData()
    {
        if (!dealData())
        {
            return null;
        }
        return mMap;
    }

    /**
     * 提交数据
     * @param payInfo TransferData
     * @return boolean
     */
    public boolean submitData()
    {
        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        try
        {
            xmlExport.setDocument(getDocument());
            getUWInfo(xmlExport);
            updateData();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 把xml数据转换成Document
     * @return Document
     */
    private org.jdom.Document getDocument()
            throws Exception
    {
        InputStream xmlStream = getXmlStream();
        SAXBuilder saxBuilder = new SAXBuilder();
        return saxBuilder.build(xmlStream);
    }

    /**
     * 从数据库中查出生成批单的xml数据
     * @return InputStream
     */
    private InputStream getXmlStream()
    {
        LPEdorAppPrintDB tLPEdorAppPrintDB = new LPEdorAppPrintDB();
        tLPEdorAppPrintDB.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorAppPrintDB.getInfo();
        //把查出的数据保存到mPrintSchema中
        mPrintSchema = tLPEdorAppPrintDB.getSchema();
        return tLPEdorAppPrintDB.getEdorInfo();
    }


    /**
     * 生成交退费通知书
     * @return boolean
     */
    public boolean getUWInfo(XmlExport xml)
    {
        ListTable listTable = new ListTable();
        listTable.setName("UW");
        String[] uwInfo = new String[1];
        uwInfo[0] = "人工核保结论：";
        LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
        tLPUWMasterDB.setEdorNo(mEdorAcceptNo);
        LPUWMasterSet tLPUWMasterSet = tLPUWMasterDB.query();
        boolean uwflag = true;
        for (int i = 1; i <= tLPUWMasterSet.size(); i++)
        {
            LPUWMasterSchema tLPUWMasterSchema = tLPUWMasterSet.get(i);
            String passFlag = tLPUWMasterSchema.getPassFlag();
            String edorNo = tLPUWMasterSchema.getEdorNo();
            String edorType = tLPUWMasterSchema.getEdorType();
            LMEdorItemDB tLMEdorItemDB = new LMEdorItemDB();
            tLMEdorItemDB.setEdorCode(edorType);
            LMEdorItemSchema tLMEdorItemSchema = tLMEdorItemDB.query().get(1);
            String edorTypeName = "";
            if (tLMEdorItemSchema != null)
            {
                edorTypeName = tLMEdorItemSchema.getEdorName();
            }
            String polNo = tLPUWMasterSchema.getPolNo();
            LCPolSchema tLCPolSchema = CommonBL.getLCPol(polNo);
            if (tLCPolSchema == null)
            {
                mErrors.addOneError("未找到险种信息！");
                return false;
            }
            String riskCode = tLCPolSchema.getRiskCode();
            String riskSeqNo = tLCPolSchema.getRiskSeqNo();
            if (!passFlag.equals(BQ.PASSFLAG_PASS))
            {
                uwflag = false;
                //uwInfo[0] += "正常通过";
            }
            if (passFlag.equals(BQ.PASSFLAG_STOP))
            {
                uwInfo[0] += "\n撤销" + edorTypeName + "项目的申请。";
            }
            else if (passFlag.equals(BQ.PASSFLAG_CANCEL))
            {
                uwInfo[0] += "\n序号" + riskSeqNo + "险种" + riskCode + "解约处理。";
            }
            else if (passFlag.equals(BQ.PASSFLAG_CONDITION))
            {
                uwInfo[0] += "\n序号" + riskSeqNo + "险种" + riskCode;
                if (tLPUWMasterSchema.getSubMultFlag() != null &&
                        tLPUWMasterSchema.getSubMultFlag().equals(BQ.TRUE))
                {
                    uwInfo[0] += "档次变为" + tLPUWMasterSchema.getMult() + "档， ";
                }
                if (tLPUWMasterSchema.getSubAmntFlag() != null &&
                        tLPUWMasterSchema.getSubAmntFlag().equals(BQ.TRUE))
                {
                    uwInfo[0] += "保额变为" + tLPUWMasterSchema.getAmnt() + "元， ";
                }
                if (tLPUWMasterSchema.getAddPremFlag() != null &&
                        tLPUWMasterSchema.getAddPremFlag().equals(BQ.TRUE))
                {
                    String addPremsql = "select prem from lpprem " +
                            " where payplancode like '000000%' " +
                            "and edorno='" + mEdorAcceptNo +
                            "' and edortype='" + edorType + "'" +
                            " and polno='" + polNo + "'";
                    uwInfo[0] += "加费：" + (new ExeSQL()).getOneValue(addPremsql) +
                            "元。";
                }
                LPPolDB tLPPolDB = new LPPolDB();
                tLPPolDB.setEdorNo(edorNo);
                tLPPolDB.setEdorType(edorType);
                tLPPolDB.setPolNo(polNo);
                if (tLPPolDB.getInfo())
                {
                    uwInfo[0] += "期交保费变为" + tLPPolDB.getPrem() + "元，";
                }
                if (tLPUWMasterSchema.getSpecFlag() != null &&
                        tLPUWMasterSchema.getSpecFlag().equals(BQ.TRUE))
                {
                    uwInfo[0] += "特别约定：";
                    String specsql =
                            "select speccontent from lpspec " +
                            " where edorno='" + mEdorAcceptNo +
                            "' and edortype='" + edorType + "'" +
                            " and polno='" + polNo + "'";
                    SSRS tSSRS = new SSRS();
                    tSSRS = (new ExeSQL()).execSQL(specsql);
                    for (int k = 1; k <= tSSRS.getMaxRow(); k++)
                    {
                        String SpecTemp = tSSRS.GetText(k, 1);
                        uwInfo[0] += SpecTemp + "\n";
                    }
                }
            }
        }
        if (uwflag == true)
        {
            uwInfo[0] += "正常通过";
        }
        listTable.add(uwInfo);
        xml.addListTable(listTable, new String[1]);
        xml.addDisplayControl("displayUW");
        //xml.outputDocumentToFile("c:\\", "PEdorUWInfo.vts");
        return true;
    }

    /**
     * 向数据库中更新批单的Blob
     * @param xmlExport XmlExport
     */
    private boolean updateData()
    {
        LPEdorAppPrintSchema printSchema = new LPEdorAppPrintSchema();
        printSchema.setSchema(mPrintSchema);
        printSchema.setEdorInfo(xmlExport.getInputStream());
        printSchema.setModifyDate(PubFun.getCurrentDate());
        printSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(printSchema, "BLOBUPDATE");
        return true;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
