package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 个单迁移</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 1.0
 */
public class PEdorTypePROutSaveBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    private MMap mMap = new MMap();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LPAddressSchema mLPAddressSchema = new LPAddressSchema();
    private GlobalInput mGlobalInput = new GlobalInput();
    private EdorItemSpecialData mEdorItemSpecialData;
    private String currDate = PubFun.getCurrentDate();
    private String currTime = PubFun.getCurrentTime();

    public PEdorTypePROutSaveBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (getSubmitData(cInputData, cOperate) == null) {
            return false;
        }
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorTypePROutSaveBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    public MMap getSubmitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return null;
        }

        //数据准备操作
        if (!dealData()) {
            return null;
        }
        return mMap;
    }

    public VData getResult() {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData() {
        try {
            mLPAddressSchema = (LPAddressSchema) mInputData
                               .getObjectByObjectName("LPAddressSchema", 0);
            mLPEdorItemSchema.setSchema((LPEdorItemSchema) mInputData.
                                        getObjectByObjectName(
                    "LPEdorItemSchema", 0));

            mGlobalInput = (GlobalInput) mInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mEdorItemSpecialData = (EdorItemSpecialData) mInputData.
                                   getObjectByObjectName("EdorItemSpecialData",
                    0);

            if (mLPAddressSchema == null || mLPEdorItemSchema == null ||
                mEdorItemSpecialData == null) {
                CError.buildErr(this, "接收数据失败");
                return false;
            }
            if (mLPEdorItemSchema.getEdorNo() == null ||
                mLPEdorItemSchema.getContNo() == null) {
                CError.buildErr(this, "接收数据失败,没有传入保全号或者保单号");
                return false;
            }
        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData() {
        //数据转移对象
        String inCom = mEdorItemSpecialData.getEdorValue("INCOM");
        String contNo = mLPEdorItemSchema.getContNo();
        String edorNo = mLPEdorItemSchema.getEdorNo();
        String edorType = mLPEdorItemSchema.getEdorType();
        Reflections ref = new Reflections();
        //备份保单表
        LCContDB tLCContDB = new LCContDB();
        LPContSchema tLPContSchema = new LPContSchema();
        tLCContDB.setContNo(contNo);
        if (!tLCContDB.getInfo()) {
            CError.buildErr(this, "查询保单失败");
            return false;
        }
        ref.transFields(tLPContSchema, tLCContDB.getSchema());
        tLPContSchema.setManageCom(inCom);
//        modify by lzy #2473取消保单迁移对缴费方式的修改
//        tLPContSchema.setPayMode("1");
        tLPContSchema.setEdorNo(edorNo);
        tLPContSchema.setEdorType(edorType);
        tLPContSchema.setOperator(mGlobalInput.Operator);
        tLPContSchema.setMakeDate(PubFun.getCurrentDate());
        tLPContSchema.setMakeTime(PubFun.getCurrentTime());
        tLPContSchema.setModifyDate(PubFun.getCurrentDate());
        tLPContSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLPContSchema, "DELETE&INSERT");

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(contNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++) {
            LPPolSchema tLPPolSchema = new LPPolSchema();
            ref.transFields(tLPPolSchema, tLCPolSet.get(i));
            tLPPolSchema.setManageCom(inCom);
            //modify by lzy #2473取消保单迁移对缴费方式的修改
//            tLPPolSchema.setPayMode("1");
            tLPPolSchema.setEdorNo(edorNo);
            tLPPolSchema.setEdorType(edorType);
            tLPPolSchema.setOperator(mGlobalInput.Operator);
            tLPPolSchema.setMakeDate(PubFun.getCurrentDate());
            tLPPolSchema.setMakeTime(PubFun.getCurrentTime());
            tLPPolSchema.setModifyDate(PubFun.getCurrentDate());
            tLPPolSchema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(tLPPolSchema, "DELETE&INSERT");
        }

        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setContNo(contNo);
        LCGetSet tLCGetSet = tLCGetDB.query();
        for (int i = 1; i <= tLCGetSet.size(); i++) {
            LPGetSchema tLPGetSchema = new LPGetSchema();
            ref.transFields(tLPGetSchema, tLCGetSet.get(i));
            tLPGetSchema.setManageCom(inCom);
            tLPGetSchema.setEdorNo(edorNo);
            tLPGetSchema.setEdorType(edorType);
            tLPGetSchema.setOperator(mGlobalInput.Operator);
            tLPGetSchema.setMakeDate(PubFun.getCurrentDate());
            tLPGetSchema.setMakeTime(PubFun.getCurrentTime());
            tLPGetSchema.setModifyDate(PubFun.getCurrentDate());
            tLPGetSchema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(tLPGetSchema, "DELETE&INSERT");
        }

        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setContNo(contNo);
        LCPremSet tLCPremSet = tLCPremDB.query();
        for (int i = 1; i <= tLCPremSet.size(); i++) {
            LPPremSchema tLPPremSchema = new LPPremSchema();
            ref.transFields(tLPPremSchema, tLCPremSet.get(i));
            tLPPremSchema.setManageCom(inCom);
            tLPPremSchema.setEdorNo(edorNo);
            tLPPremSchema.setEdorType(edorType);
            tLPPremSchema.setOperator(mGlobalInput.Operator);
            tLPPremSchema.setMakeDate(PubFun.getCurrentDate());
            tLPPremSchema.setMakeTime(PubFun.getCurrentTime());
            tLPPremSchema.setModifyDate(PubFun.getCurrentDate());
            tLPPremSchema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(tLPPremSchema, "DELETE&INSERT");
        }
        //处理被保人
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(contNo);
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        for (int i = 1; i <= tLCInsuredSet.size(); i++) {
            LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
            ref.transFields(tLPInsuredSchema, tLCInsuredSet.get(i));
            tLPInsuredSchema.setManageCom(inCom);
            tLPInsuredSchema.setEdorNo(edorNo);
            tLPInsuredSchema.setEdorType(edorType);
            tLPInsuredSchema.setOperator(mGlobalInput.Operator);
            tLPInsuredSchema.setMakeDate(PubFun.getCurrentDate());
            tLPInsuredSchema.setMakeTime(PubFun.getCurrentTime());
            tLPInsuredSchema.setModifyDate(PubFun.getCurrentDate());
            tLPInsuredSchema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(tLPInsuredSchema, "DELETE&INSERT");
        }
        //处理地址编号
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(contNo);
        if (!tLCAppntDB.getInfo()) {
            CError.buildErr(this, "查询保单投保人失败");
            return false;
        }

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String sql = "  Select Case When max(int(AddressNo)) Is Null Then 1 "
                     + "Else max(int(AddressNo))  End "
                     + "from LCAddress "
                     + "where CustomerNo='"
                     + tLCAppntDB.getAppntNo() + "'";
        tSSRS = tExeSQL.execSQL(sql);
        Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
        int ttNo = firstinteger.intValue() + 1;
        System.out.println("得到的地址码是：" + ttNo);
        //查询原地址
        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
        tLCAddressDB.setCustomerNo(tLCAppntDB.getAppntNo());
        tLCAddressDB.getInfo();
        LPAddressSchema tLPAddressSchema = new LPAddressSchema();
        ref.transFields(tLPAddressSchema, tLCAddressDB.getSchema());
        tLPAddressSchema.setAddressNo("" + ttNo);
        tLPAddressSchema.setCustomerNo(tLCAppntDB.getAppntNo());
        tLPAddressSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPAddressSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPAddressSchema.setPostalAddress(mLPAddressSchema.getPostalAddress());
        tLPAddressSchema.setZipCode(mLPAddressSchema.getZipCode());
        tLPAddressSchema.setPhone(mLPAddressSchema.getPhone());
        tLPAddressSchema.setOperator(mGlobalInput.Operator);
        tLPAddressSchema.setMakeDate(PubFun.getCurrentDate());
        tLPAddressSchema.setMakeTime(PubFun.getCurrentTime());
        tLPAddressSchema.setModifyDate(PubFun.getCurrentDate());
        tLPAddressSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLPAddressSchema, "DELETE&INSERT");

        //处理投保人表

        LPAppntSchema tLPAppntSchema = new LPAppntSchema();
        ref.transFields(tLPAppntSchema, tLCAppntDB.getSchema());
        tLPAppntSchema.setManageCom(inCom);
        tLPAppntSchema.setAddressNo("" + ttNo);
        tLPAppntSchema.setEdorNo(edorNo);
        tLPAppntSchema.setEdorType(edorType);
        tLPAppntSchema.setOperator(mGlobalInput.Operator);
        tLPAppntSchema.setMakeDate(PubFun.getCurrentDate());
        tLPAppntSchema.setMakeTime(PubFun.getCurrentTime());
        tLPAppntSchema.setModifyDate(PubFun.getCurrentDate());
        tLPAppntSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLPAppntSchema, "DELETE&INSERT");

        //生成函件
        int serialNumber = getSerialNumber(edorNo);
        LGLetterSchema tLGLetterSchema = new LGLetterSchema();
        tLGLetterSchema.setEdorAcceptNo(edorNo);
        tLGLetterSchema.setSerialNumber(String.valueOf(serialNumber++));
        tLGLetterSchema.setLetterType("1"); //非核保函件
        tLGLetterSchema.setSendObj("0");//下发对象为投保人
        tLGLetterSchema.setBackFlag("0");//不需要回销
        tLGLetterSchema.setLetterSubType("9"); //个单迁移
        tLGLetterSchema.setState("0"); //待下发
        tLGLetterSchema.setAddressNo(tLCAppntDB.getAddressNo());//为了函件显示
        //提示内容
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(inCom);
        if(!tLDComDB.getInfo())
        {
            CError.buildErr(this, "查询转移机构失败");
            return false;
        }
        String title = "您的保单"+contNo+" 申请迁移到"+tLDComDB.getName()+"信息变更为："
                       +" 联系地址："+mLPAddressSchema.getPostalAddress()
                       +" 联系邮编："+mLPAddressSchema.getZipCode()
                       +"联系电话:"+mLPAddressSchema.getPhone()
                       ;
        tLGLetterSchema.setRemark("投保人告知");
        tLGLetterSchema.setLetterInfo(title);
        tLGLetterSchema.setOperator(mGlobalInput.Operator);
        tLGLetterSchema.setMakeDate(PubFun.getCurrentDate());
        tLGLetterSchema.setMakeTime(PubFun.getCurrentTime());
        tLGLetterSchema.setModifyDate(PubFun.getCurrentDate());
        tLGLetterSchema.setModifyTime(PubFun.getCurrentTime());
        //防止多次点击“保存”生成多次函件，所以删除
        String delLet = "delete from LGLetter where LetterType = '1' and LetterSubType = '9'"
                        + " and Remark ='投保人告知' and EdorAcceptNo ='"+edorNo+"'";
        mMap.put(delLet,"DELETE");
        mMap.put(tLGLetterSchema, "DELETE&INSERT");
        //迁出机构销售函件
        tLGLetterSchema = new LGLetterSchema();
        tLGLetterSchema.setEdorAcceptNo(edorNo);
        tLGLetterSchema.setSerialNumber(String.valueOf(serialNumber++));
        tLGLetterSchema.setLetterType("1"); //非核保函件
        tLGLetterSchema.setSendObj("3");//下发对象为销售部门
        tLGLetterSchema.setBackFlag("0");//不需要回销
        tLGLetterSchema.setLetterSubType("8"); //内部流转
        tLGLetterSchema.setState("0"); //待下发
        tLGLetterSchema.setRemark("迁出销售");
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tLCContDB.getAgentCode());
        if(!tLAAgentDB.getInfo())
        {
            CError.buildErr(this, "查询保单业务员失败");
            return false;
        }
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("lcsalechnl");
        tLDCodeDB.setCode(tLCContDB.getSaleChnl());
        if(!tLDCodeDB.getInfo())
        {
            CError.buildErr(this, "销售渠道查询错误");
            return false;
        }
        title = "销售渠道为"+tLDCodeDB.getCodeName()+"("+tLCContDB.getSaleChnl()+")，业务员"+tLAAgentDB.getName()+"("+tLAAgentDB.getAgentCode()+")服务的保单"+contNo+" 申请迁移到"+tLDComDB.getName()
                ;
        tLGLetterSchema.setLetterInfo(title);
        tLGLetterSchema.setOperator(mGlobalInput.Operator);
        tLGLetterSchema.setMakeDate(PubFun.getCurrentDate());
        tLGLetterSchema.setMakeTime(PubFun.getCurrentTime());
        tLGLetterSchema.setModifyDate(PubFun.getCurrentDate());
        tLGLetterSchema.setModifyTime(PubFun.getCurrentTime());
        //防止多次点击“保存”生成多次函件，所以删除
        delLet = "delete from LGLetter where LetterType = '1' and LetterSubType = '8'"
                        + " and Remark ='迁出销售' and EdorAcceptNo ='"+edorNo+"'";
        mMap.put(delLet,"DELETE");
        mMap.put(tLGLetterSchema, "DELETE&INSERT");
	mEdorItemSpecialData.add("dealstate","0");
        mEdorItemSpecialData.add("outcom",tLCContDB.getManageCom());
        mMap.put(mEdorItemSpecialData.getSpecialDataSet(),"DELETE&INSERT");
        mResult.clear();
        mResult.add(mMap);

        return true;
    }
     /**
     * 获得已有函件的序号
     * @return String
     */
    private int getSerialNumber(String aEdorNo)
    {
        //得到序号
        String sql = "select * from LGLetter " +
                "where EdorAcceptNo = '" + aEdorNo + "' " +
                "order by int(SerialNumber) desc ";
        LGLetterDB db = new LGLetterDB();
        LGLetterSet set = db.executeQuery(sql);
        if (set.size() == 0)
        {
            return 1;
        }
        return Integer.parseInt(set.get(1).getSerialNumber())+1;

    }
}
