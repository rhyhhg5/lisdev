package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class PEdorLRAppConfirmBL implements EdorAppConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    private GlobalInput mGlobalInput = new GlobalInput();

    public PEdorLRAppConfirmBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括""和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //数据准备操作
        if (!prepareData())
        {
            return false;
        }

        //数据操作业务处理
//        PEdorAppConfirmBLS tPEdorAppConfirmBLS = new PEdorAppConfirmBLS();
//        if (!tPEdorAppConfirmBLS.submitData(mInputData, mOperate))
//        {
//            CError.buildErr(this, "数据提交失败", tPEdorAppConfirmBLS.mErrors);
//            return false;
//        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData.
                                getObjectByObjectName("LPEdorItemSchema",
                    0);
            mGlobalInput.setSchema((GlobalInput) mInputData.
                                   getObjectByObjectName("GlobalInput", 0));
        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        if (mLPEdorItemSchema == null || mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorLRAppConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
        {
            LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
            tLPGrpEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPGrpEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPGrpEdorItemDB.setGrpContNo(mLPEdorItemSchema.getGrpContNo());
            LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
            if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1) //团单项目表也没有LR项目，说明保全项目查询失败
            {
                mErrors.copyAllErrors(tLPGrpEdorItemDB.mErrors);
                mErrors.addOneError(new CError("查询保全项目信息失败！"));
                return false;
            }
            mResult.clear();
            MMap map = new MMap();
            mResult.add(map);
            return true; //存在团单LR项目，此时是团单LR申请确认调用了个单LR申请确认，添加空map并返回true
        }
        mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));

        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorMainDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorMainDB.setContNo(mLPEdorItemSchema.getContNo());
        if (!tLPEdorMainDB.getInfo())
        {
            mErrors.copyAllErrors(tLPEdorMainDB.mErrors);
            mErrors.addOneError(new CError("查询保全信息失败！"));
            return false;
        }
        mLPEdorMainSchema = tLPEdorMainDB.getSchema();

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //设置批改补退费表
    	//保单补发累计退费 add by hyy
    	LJSGetEndorseBL tLJSGetEndorseBL = new LJSGetEndorseBL();
    	//工本费单独的类 add by hyy
    	LJSGetEndorseBL aLJSGetEndorseBL = new LJSGetEndorseBL();
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mLPEdorItemSchema.getEdorNo()); //给付通知书号码
        tLJSGetEndorseSchema.setEndorsementNo(mLPEdorItemSchema.
                                              getEdorNo());
        tLJSGetEndorseSchema.setContNo(mLPEdorItemSchema.getContNo());
        tLJSGetEndorseSchema.setPolNo("000000");
        tLJSGetEndorseSchema.setFeeOperationType(mLPEdorItemSchema.
                                                 getEdorType());
        tLJSGetEndorseSchema.setGetDate(mLPEdorItemSchema.
                                        getEdorValiDate());
        tLJSGetEndorseSchema.setGetMoney(mLPEdorItemSchema.getGetMoney());
        tLJSGetEndorseSchema.setFeeOperationType("LR");
        tLJSGetEndorseSchema.setRiskCode("000000");
        BqCalBL tBqCalBl = new BqCalBL();
        String feeFinaType = tBqCalBl.getFinType("LR", "GB",
                                                 mLPEdorItemSchema.getPolNo());
        if (feeFinaType.equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.errorMessage = "在LDCode1中缺少保全财务接口转换类型编码!";
            this.mErrors.addOneError(tError);
            return false;
        }

        tLJSGetEndorseSchema.setFeeFinaType(feeFinaType);
        tLJSGetEndorseSchema.setPayPlanCode("00000000"); //无作用
        tLJSGetEndorseSchema.setDutyCode("0"); //无作用，但一定要，转ljagetendorse时非空
        tLJSGetEndorseSchema.setOtherNo(mLPEdorItemSchema.getEdorNo());
        tLJSGetEndorseSchema.setOtherNoType("10"); //个人保全
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(mGlobalInput.ManageCom);
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setGrpContNo(mLPEdorItemSchema.getContNo());
        if (tLCContDB.getInfo())
        {
            tLJSGetEndorseSchema.setAgentCode(tLCContDB.getAgentCode());
            tLJSGetEndorseSchema.setAgentCom(tLCContDB.getAgentCom());
            tLJSGetEndorseSchema.setAgentGroup(tLCContDB.getAgentGroup());
            tLJSGetEndorseSchema.setAgentType(tLCContDB.getAgentType());
        }

        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        //为收取工本费做准备
        aLJSGetEndorseBL.setSchema(tLJSGetEndorseSchema);
        tLJSGetEndorseBL.setSchema(aLJSGetEndorseBL);
        tLJSGetEndorseBL.setDefaultFields();
      //计算工本费
        if(!setGBInfo(aLJSGetEndorseBL))
        {
            return false;
        }

        //end
       // mLJSGetEndorseSet.add(tLJSGetEndorseBL.getSchema());
        mLPEdorItemSchema.setEdorState("2");
        mLPEdorItemSchema.setUWFlag("0");
        return true;
    }

    /**
     * 计算工本费
     * @param tLJSGetEndorseBL LJSGetEndorseBL
     * @return boolean
     * @author hyy
     */
    private boolean setGBInfo(LJSGetEndorseBL tLJSGetEndorseBL)
    {
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorEspecialDataDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorEspecialDataDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPEdorEspecialDataDB.setDetailType(BQ.DETAILTYPE_GB);
        tLPEdorEspecialDataDB.setPolNo(BQ.FILLDATA);
        if (!tLPEdorEspecialDataDB.getInfo())
        {
            return true;
        }
        if (Double.parseDouble(tLPEdorEspecialDataDB.getEdorValue()) <= 0)
        {
            return true;
        }

        LJSGetEndorseBL lJSGetEndorseBL = new LJSGetEndorseBL();
        lJSGetEndorseBL.setSchema(tLJSGetEndorseBL);
        lJSGetEndorseBL.setGrpPolNo(BQ.FILLDATA);
        lJSGetEndorseBL.setPolNo(BQ.FILLDATA);
        lJSGetEndorseBL.setGetMoney(Double.parseDouble(
                tLPEdorEspecialDataDB.getEdorValue()));
        lJSGetEndorseBL.setPayPlanCode(BQ.FILLDATA);
        lJSGetEndorseBL.setDutyCode(BQ.FILLDATA);
        lJSGetEndorseBL.setInsuredNo(BQ.FILLDATA);
        lJSGetEndorseBL.setInsuredNo(BQ.FILLDATA);
        lJSGetEndorseBL.setRiskCode(BQ.FILLDATA);
        lJSGetEndorseBL.setRiskVersion(BQ.FILLDATA);
        lJSGetEndorseBL.setKindCode("0");
        lJSGetEndorseBL.setRiskCode("0");
        lJSGetEndorseBL.setRiskVersion("0");

        String finType = BqCalBL.getFinType(mLPEdorItemSchema.getEdorType(),
                                     BQ.FEEFINATYPE_GB,
                                     mLPEdorItemSchema.getPolNo());
        if (finType.equals(""))
        {
            CError tError = new CError();
            tError.errorMessage = "计算工本费有错误";
            this.mErrors.addOneError(tError);
            return false;
        }
        lJSGetEndorseBL.setFeeFinaType(finType);
        lJSGetEndorseBL.setGetMoney(tLPEdorEspecialDataDB.getEdorValue());
        mLJSGetEndorseSet.add(lJSGetEndorseBL);
        mLPEdorItemSchema.setGetMoney(
                mLPEdorItemSchema.getGetMoney()
                + Double.parseDouble(tLPEdorEspecialDataDB.getEdorValue()));
        return true;
    }
    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        mResult.clear();
        MMap map = new MMap();
        map.put(mLJSGetEndorseSet, "INSERT");
        map.put(mLPEdorItemSchema, "UPDATE");
        mResult.add(map);
        return true;
    }


    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        tGlobalInput.ManageCom = "86";

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorNo("410000000000066");
        tLPEdorItemSchema.setContNo("230110000000179");
        tLPEdorItemSchema.setEdorType("LR");

        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPEdorItemSchema);

        PEdorLRAppConfirmBL tPEdorLRAppConfirmBL = new
                PEdorLRAppConfirmBL();
        if (!tPEdorLRAppConfirmBL.submitData(tVData, "INSERT"))
        {
            System.out.println(tPEdorLRAppConfirmBL.mErrors.getErrContent());
        }

    }

}
