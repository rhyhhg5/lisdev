package com.sinosoft.lis.bq;

import java.util.regex.Pattern;

import com.sinosoft.lis.bq.mm.face.CheckMailAndCell;

public class CheckMailAndCellImpl implements CheckMailAndCell {

	public boolean checkCellNumber(String cellNumber) {
		return Pattern.matches("^0{0,1}13[0-9]{9}$|^0{0,1}15[0-9]{9}$|^\\+8613[0-9]{9}$|^\\+8615[0-9]{9}$", cellNumber);
	}

	public boolean checkMail(String mailAddress) {
		return(Pattern.matches("^[_\\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\\.)+[a-z]{2,3}$",mailAddress));
	}
	public static void main(String[] args){
		//System.out.println(new CheckMailAndCellImpl().checkCellNumber("+8615412345698"));
		System.out.println(new CheckMailAndCellImpl().checkMail("hecic@126.com"));
	}

}
