package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class ChangeEdorStateBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mEdorState = null;

    public ChangeEdorStateBL(GlobalInput gi, LPGrpEdorItemSchema edorItem)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorItem.getEdorNo();
        this.mEdorType = edorItem.getEdorType();
        this.mEdorState = edorItem.getEdorState();
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        setEdorState();
        setEdorMain();

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 设置item状态
     */
    private void setEdorState()
    {
        String sql = "update  LPGrpEdorItem " +
                "set EdorState = '" + mEdorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }

    private void setEdorMain()
    {
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorNo(mEdorNo);
        if (tLPEdorMainDB.query().size() == 0)
        {
            return;
        }
        String sql = "update LPEdorMain " +
                "set EdorState = '" + mEdorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where  EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
