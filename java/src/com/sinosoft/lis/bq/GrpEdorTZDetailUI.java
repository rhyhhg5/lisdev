package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;

/**
 * <p>Title: 保全新增被保人项目</p>
 * <p>Description: 录入保全明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorTZDetailUI
{
    GrpEdorNIDetailBL mGrpEdorNIDetailBL = null;

    /**
     * 调用业务逻辑
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        mGrpEdorNIDetailBL = new GrpEdorNIDetailBL();
        if (!mGrpEdorNIDetailBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mGrpEdorNIDetailBL.mErrors.getFirstError();
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mGrpEdorNIDetailBL.getMessage();
    }

    /**
     * 主函数，测试用
     */
    public static void main(String[] args)
    {
    }
}
