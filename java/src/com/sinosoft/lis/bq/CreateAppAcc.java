package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 生成账户数据
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class CreateAppAcc
{
    public CreateAppAcc()
    {
    }

    private void createIndiAppAcc()
    {
        AppAcc tAppAcc = new AppAcc();
        LDPersonDB tLDPersonDB = new LDPersonDB();
        LDPersonSet tLDPersonSet = null;

        //个单
        String sql = "select distinct a.* from LDPerson a, LCCont b where a.customerNo = b.appntNo "
                     + "   and b.contType = '1' and customerNo not in(select customerNo from LCAppAcc) order by a.customerNo";
        System.out.println(sql);
        int start = 1;
        int count = 5000;

        do
        {
            MMap map = new MMap();
            tLDPersonSet = tLDPersonDB.executeQuery(sql, start, count);
            for(int i = 1; i <= tLDPersonSet.size(); i++)
            {
                System.out.println((start + i));
                LCAppAccTraceSchema schema = new LCAppAccTraceSchema();
                schema.setCustomerNo(tLDPersonSet.get(i).getCustomerNo());
                schema.setOtherNo("autoCreate");
                schema.setOtherNo("1");
                schema.setMoney(0);
                schema.setOperator("endor0");

                map.add(tAppAcc.accShiftToXQY(schema, "0"));
            }
            VData data = new VData();
            data.add(map);

            PubSubmit p = new PubSubmit();
            if(!p.submitData(data, ""))
            {
                System.out.println(p.mErrors.getErrContent());
            }
            start += count;
        }
        while(tLDPersonSet.size() > 0);
    }

    private void createGrpAppAcc()
    {
        AppAcc tAppAcc = new AppAcc();
        LDGrpDB tLDGrpDB = new LDGrpDB();
        LDGrpSet tLDGrpSet = null;

        //个单
        String sql = "select distinct a.* from LDGrp a, LCGrpCont b where a.customerNo = b.appntNo "
                     + "   and customerNo not in(select customerNo from LCAppAcc) order by a.customerNo";
        System.out.println(sql);
        int start = 1;
        int count = 5000;

        do
        {
            MMap map = new MMap();
            tLDGrpSet = tLDGrpDB.executeQuery(sql, start, count);
            for(int i = 1; i <= tLDGrpSet.size(); i++)
            {
                System.out.println((start + i));
                LCAppAccTraceSchema schema = new LCAppAccTraceSchema();
                schema.setCustomerNo(tLDGrpSet.get(i).getCustomerNo());
                schema.setOtherNo("autoCreate");
                schema.setOtherNo("1");
                schema.setMoney(0);
                schema.setOperator("endor0");

                map.add(tAppAcc.accShiftToXQY(schema, "1"));
            }
            VData data = new VData();
            data.add(map);

            PubSubmit p = new PubSubmit();
            if(!p.submitData(data, ""))
            {
                System.out.println(p.mErrors.getErrContent());
            }
            start += count;
        }
        while(tLDGrpSet.size() > 0);
    }

    public static void main(String[] args)
    {
        CreateAppAcc createappacc = new CreateAppAcc();
    }
}
