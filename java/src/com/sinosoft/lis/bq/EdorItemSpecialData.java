package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全明细特殊数据存取类，仅用于团险</p>
 * <p>Description: LPEdorEspecialData表用来存放各个项目明细录入的特殊数据，
 * 这样当有项目有新的数据保存时不用再建表或加字段，因此它和Item表相关联，
 * 而EdorItemSpecialData类则是LPEdorEspecialData表数据的维护者。
 * </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class EdorItemSpecialData
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private LPEdorEspecialDataSet mSpecialDataSet = null;

    private String mEdorAcceptNo = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mPolNo = BQ.FILLDATA;

    /**
     * 构造函数
     * @param edorAcceptNo String
     * @param edorNo String
     * @param edorType String
     */
    public EdorItemSpecialData(String edorAcceptNo, String edorNo, String edorType)
    {
        this.mEdorAcceptNo = edorAcceptNo;
        this.mEdorNo = edorNo;
        this.mEdorType = edorType;
        this.mSpecialDataSet = new LPEdorEspecialDataSet();
    }

    /**
     * 构造函数
     * @param edorNo String
     * @param edorType String
     */
    public EdorItemSpecialData(String edorNo, String edorType)
    {
        this(edorNo, edorNo, edorType);
    }

    /**
     * 构造函数，团单用
     * @param edorItem LPEdorItemSchema
     */
    public EdorItemSpecialData(LPGrpEdorItemSchema edorItem)
    {
        this(edorItem.getEdorNo(), edorItem.getEdorType());
    }

    /**
     * 构造函数，个单用
     * @param edorItem LPEdorItemSchema
     */
    public EdorItemSpecialData(LPEdorItemSchema edorItem)
    {
        this(edorItem.getEdorNo(), edorItem.getEdorType());
    }

    /**
     * 设置团体险种号
     * @param grpPolNo String
     */
    public void setGrpPolNo(String grpPolNo)
    {
        this.mPolNo = grpPolNo;
    }

    /**
     * 设置个人险种号
     * @param polNo String
     */
    public void setPolNo(String polNo)
    {
        this.mPolNo = polNo;
    }

    /**
     * 向LPEdorEspecialDataSet中添加一条数据
     * @param detailType String
     * @param edorValue String
     */
    public void add(String detailType, String edorValue)
    {
        if ((edorValue == null) || (edorValue.equals("")))
        {
            return;
        }
        LPEdorEspecialDataSchema tEspecialSchema = new LPEdorEspecialDataSchema();
        tEspecialSchema.setEdorAcceptNo(mEdorAcceptNo);
        tEspecialSchema.setEdorNo(mEdorNo);
        tEspecialSchema.setEdorType(mEdorType);
        tEspecialSchema.setPolNo(mPolNo);
        tEspecialSchema.setDetailType(detailType.toUpperCase());
        tEspecialSchema.setEdorValue(edorValue);
        mSpecialDataSet.add(tEspecialSchema);
    }

    /**
     * 根据detailType得到相应的EdorValue
     * @param detailType String
     * @return String
     */
    public String getEdorValue(String detailType)
    {
        for (int i = 1; i <= mSpecialDataSet.size(); i++)
        {
            LPEdorEspecialDataSchema tEspecialSchema = mSpecialDataSet.get(i);
            String polNo = tEspecialSchema.getPolNo();
            String dt = tEspecialSchema.getDetailType();
            if ((dt != null) && (dt.equalsIgnoreCase(detailType)) &&
                    (polNo != null) && (polNo.equals(mPolNo)))
            {
                return tEspecialSchema.getEdorValue();
            }
        }
        return null;
    }

    /**
     * 得到险种号
     * @return String[]
     */
    public String[] getGrpPolNos()
    {
        Set polNoSet = new HashSet();
        for (int i = 1; i <= mSpecialDataSet.size(); i++)
        {
            polNoSet.add(mSpecialDataSet.get(i).getPolNo());
        }
        return (String[]) polNoSet.toArray(new String[0]);
    }

    /**
     * 得到个人险种号
     * @return String[]
     */
    public String[] getPolNos()
    {
        return getGrpPolNos();
    }

    /**
     * 从库中查出SpecialDataSet
     * @return boolean
     */
    public boolean query()
    {
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorEspecialDataDB.setEdorNo(mEdorNo);
        tLPEdorEspecialDataDB.setEdorType(mEdorType);
        mSpecialDataSet = tLPEdorEspecialDataDB.query();
        if (mSpecialDataSet.size() == 0)
        {
            return false;
        }
        return true;
    }

    /**
     * 得到LPEdorEspecialDataSet
     * @return LPEdorEspecialDataSet
     */
    public LPEdorEspecialDataSet getSpecialDataSet()
    {
        return mSpecialDataSet;
    }

    /**
     * 清空数据
     */
    public void clear()
    {
        mSpecialDataSet.clear();
    }

    /**
     * 将特殊数据存库
     * @param set LPEdorEspecialDataSet
     * @return boolean
     */
    public boolean insert()
    {
        MMap map = new MMap();
        map.put(mSpecialDataSet, "DELETE&INSERT");

        VData tVData = new VData();
        tVData.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(tVData, ""))
        {
            mErrors.copyAllErrors(p.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mErrors.getFirstError();
    }
}
