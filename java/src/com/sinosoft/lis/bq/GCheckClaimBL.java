package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.util.ArrayList;

/**
 * <p>Title: </p>
 *
 * <p>Description: 团体解约、减人，发生理赔的险种不退费,本类用来检测客户险种是否发生过理赔
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GCheckClaimBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private ArrayList claimedPolNoArrayList = new ArrayList();

    public GCheckClaimBL()
    {
    }

    /**
     * 通过险种号检测客户险种是否发生过理赔
     * @param polNo String
     * @return boolean
     */
    public boolean checkClaimed(String polNo)
    {//qulq mofidy 2007-7-21
        if(polNo == null || polNo.equals(""))
        {
            this.mErrors.addOneError("GCheckClaimBL_checkClaimed传入参数为空！");
            return false;
        }
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(polNo);
        String tCvalidate = "";
        if(!tLCPolDB.getInfo())
        {
            LBPolDB tLBPolDB = new LBPolDB();
            tLBPolDB.setPolNo(polNo);
            if(!tLBPolDB.getInfo())
            {
                this.mErrors.addOneError("GCheckClaimBL_checkClaimed查询险种信息失败！");
                return false;
            }
            else
            {
                tCvalidate = tLBPolDB.getCValiDate();
            }
        }
        else
        {
            tCvalidate = tLCPolDB.getCValiDate();
        }
        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setPolNo(polNo);
        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();

        if (tLLClaimPolicySet.mErrors.needDealError())
        {
            this.mErrors.addOneError("查询理赔记录时出错");
            return false;
        }

        //校验理赔受理是否已结案 原有检验没有校验理赔事件发生时间，会导致发生理赔的保单在续保后退保出错
        ExeSQL tExeSQL = new ExeSQL();
        for(int i = 1; i <= tLLClaimPolicySet.size(); i++)
        {
            String sql = "  select caseNo from LLCase a where caseNo = '"
                         + tLLClaimPolicySet.get(i).getCaseNo() + "' "
                         + "   and endCaseDate is not null and '"
                         + tLLClaimPolicySet.get(i).getGiveType()+"' != '3' "
                         + "and exists (select 1 from llcaserela b,llsubreport c "
                         + " where b.Subrptno = c.Subrptno and b.caseno =a.caseno "
                         + " and c.accdate >='"
                         + tCvalidate +"')"
                         ;
            SSRS tSSRS = tExeSQL.execSQL(sql);
            tLLClaimPolicySet.get(i).getGiveType();
            if(tSSRS != null && tSSRS.getMaxRow() > 0)
            {
                return true;
            }

        }

        return false;
    }

    /**
     * 通过险种号检测客户险种是否发生过理赔
     * *****仅限减人校验*****
     * 保全当时没有正在受理赔案的被保人才能操作减人,若被保人存在已经结案理赔,在算费时分为4种情况:
	 *	1.在团单减人算费时，被保人若存在理赔纠错，且当前险种赔款合计为0，则认为被保人该险种未发生过理赔,按正常减人算法计算退费.
	 *	2.在团单减人算费时，被保人若存在理赔纠错，且当前险种赔款合计不为0，则认为被保人该险种发生过理赔,该险种退费金额为0.
	 *	3.在团单减人算费时，被保人若不存在理赔纠错，但当前险种存在非全额拒付且成功结案的理赔,不论金额是否为0,均认为被保人该险种发生过理赔,该险种退费金额为0.
	 *	4.在团单减人算费时，被保人若不存在理赔纠错，但当前险种理赔均为全额拒付,则认为被保人该险种未发生过理赔,按正常减人算法计算退费.
     * @param polNo String
     * @return boolean
     */
    public boolean checkClaimed_ZT(String polNo)
    {//xp mofidy 2011-12-27
        if(polNo == null || polNo.equals(""))
        {
            this.mErrors.addOneError("GCheckClaimBL_checkClaimed传入参数为空！");
            return false;
        }
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(polNo);
        String tCvalidate = "";
        if(!tLCPolDB.getInfo())
        {
            LBPolDB tLBPolDB = new LBPolDB();
            tLBPolDB.setPolNo(polNo);
            if(!tLBPolDB.getInfo())
            {
                this.mErrors.addOneError("GCheckClaimBL_checkClaimed查询险种信息失败！");
                return false;
            }
            else
            {
                tCvalidate = tLBPolDB.getCValiDate();
            }
        }
        else
        {
            tCvalidate = tLCPolDB.getCValiDate();
        }
        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setPolNo(polNo);
        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();

        if (tLLClaimPolicySet.mErrors.needDealError())
        {
            this.mErrors.addOneError("查询理赔记录时出错");
            return false;
        }

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS =new SSRS();
        tSSRS=tExeSQL.execSQL("select a.rgttype,sum(b.realpay) " +
        					" from llcase a,llclaimdetail b " +
        					" where a.caseno=b.caseno " +
        					" and a.rgtstate in ('11','12') " +
        					" and b.polno='"+polNo+"' " +
//        					排除全额拒付的.
        					" and ((b.givetype='3' and b.realpay<>0) or b.givetype!='3') " +
        					" group by b.polno,a.rgttype " +
        					" with ur ");
//      理赔纠错FLAG
        boolean tflag=false;
        double sumpay=0;
        for (int i = 1; i <= tSSRS.MaxRow; i++) {
        	if(tSSRS.GetText(i, 1).equals("4")||tSSRS.GetText(i, 1).equals("5")){
        		tflag=true;
        	}
        	sumpay+=Double.parseDouble(tSSRS.GetText(i, 2));			
		}
        if(tflag){
        	if(sumpay==0) return false;
        	else return true;
        }else{
        	if(tSSRS.MaxRow==0) return false;
        	else return true;
        }
    }
    
    /**
     * 通过险种号检测客户险种是否发生过理赔
     * @param VData
     * @param String
     * @return boolean
     */
    public boolean checkClaimed(LJSGetEndorseSchema cLJSGetEndorseSchema)
    {
    	if(cLJSGetEndorseSchema.getFeeOperationType()!=null&&cLJSGetEndorseSchema.getFeeOperationType().equals("ZT")){
    		return checkClaimed_ZT(cLJSGetEndorseSchema.getPolNo());
    	}
        return checkClaimed(cLJSGetEndorseSchema.getPolNo());
    }

    public static void main(String s[])
    {
        GCheckClaimBL tGCheckClaimBL = new GCheckClaimBL();

//        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
//        tLJSGetEndorseDB.setOtherNo("20051222000013");
//        LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.query();

//        for(int i = 1; i <= tLJSGetEndorseSet.size(); i++)
//        {
            System.out.println(tGCheckClaimBL.checkClaimed("21000417252"));
//        }

        LCPolSet set = tGCheckClaimBL.getLCPolClaimed("2200000105");
        for(int i = 1; i <= set.size(); i++)
        {
            System.out.println(i);
        }
    }

    /**
     * 校验团险是否正在发生理赔
     * @param grpPolNo String
     * @return boolean
     */
    public LCPolSet getLCPolClaimed(String grpPolNo)
    {
        String sql = "select b.* " +
        		" from llcase a,llclaimdetail b " +
        		" where a.caseno=b.caseno and a.rgtstate not in ('11','12','14') " +
//        		" and a.endcasedate is not null " +
                "  and b.grpPolNo = '" + grpPolNo + "' "+
        		"  with ur" ;
        
        System.out.println(sql);
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);
        return tLCPolSet;
    }

    public boolean checkClaimedCustomer(String insuredNo)
    {
        ArrayList tArrayList = new ArrayList();
        LPDiskImportSet tDiskImportSet = new LPDiskImportSet();
        ExeSQL tExeSQL = new ExeSQL();
        String sql ="select distinct  customerno from llcase where customerno ='" +insuredNo +"'"
                    +"and rgtstate not in ('11','12','14') with ur"
                    ;

         SSRS tSSRS = tExeSQL.execSQL(sql);
        if(tSSRS != null && tSSRS.getMaxRow() > 0)
        {
            return true;
        }
        return false ;
    }

}
