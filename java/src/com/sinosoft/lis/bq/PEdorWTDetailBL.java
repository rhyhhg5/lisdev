package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单犹豫期限退保项目明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Alex
 * @version 1.0
 */

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorWTDetailBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = null;
    private LPEdorItemSchema mLPEdorItemSchema_in = null;
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LPContSet mLPContSet= new LPContSet();
    private LPInsuredSet mLPInsuredSet= new LPInsuredSet();
    private LPPolSet mLPPolSet= null;
    private LPPENoticeSet mLPPENoticeSet = null;
    private LPPENoticeItemSet mLPPENoticeItemSet = new LPPENoticeItemSet();
    private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = null;
    private Reflections ref=new Reflections();
    
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    

    public PEdorWTDetailBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareData())
        {
            return false;
        }

        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            mGlobalInput = (GlobalInput) mInputData
                           .getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema_in = (LPEdorItemSchema) mInputData.
                                   getObjectByObjectName("LPEdorItemSchema", 0);
            mLPPolSet = (LPPolSet) mInputData.
                        getObjectByObjectName("LPPolSet",0);
            mLPPENoticeSet = (LPPENoticeSet) mInputData.
                             getObjectByObjectName("LPPENoticeSet", 0);
            mLPEdorEspecialDataSchema = (LPEdorEspecialDataSchema)mInputData
                                        .getObjectByObjectName("LPEdorEspecialDataSchema", 0);
        }
        catch (Exception e)
        {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorWTDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mGlobalInput == null || mLPEdorItemSchema_in == null)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorWTDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "输入数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    /**
     * 根据前面的输入数据，进行校验处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean checkData()
    {
    	//校验银保通或信保通是否经过对账 add by xp 091218
        if (!checkYBTXBT())
        {
            return false;
        }


        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
       // double tGetMoney = mLPEdorItemSchema_in.getGetMoney();
        String reasonCode = mLPEdorItemSchema_in.getReasonCode();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema_in.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema_in.getEdorType());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema_in.getContNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();


        if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorWTDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "输入数据有误,LPEdorItem中没有相关数据!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLPEdorItemSchema = tLPEdorItemSet.get(1);
        mLPEdorItemSchema.setEdorState("1");
        //mLPEdorItemSchema.setGetMoney(tGetMoney);
        mLPEdorItemSchema.setReasonCode(reasonCode);


        if (mLPEdorItemSchema.getEdorState().trim().equals("2"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorWTDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保全已经申请确认不能修改!";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= mLPPolSet.size(); i++)
        {
            LPEdorItemSchema tmLPEdorItemSchema = new LPEdorItemSchema();
            tmLPEdorItemSchema.setSchema(mLPEdorItemSchema);
            tmLPEdorItemSchema.setPolNo(mLPPolSet.get(i).getPolNo());

            LPPolSchema schema = getLPPol(mLPPolSet.get(i));
            if (schema == null)
            {
                return false;
            }

            mLPPolSet.get(i).setSchema(schema);
        }
        /*
                 for (int i=1;i<=mLPPolSet.size();i++)
                 {
            LPPolSchema tLPPolSchema=new LPPolSchema();
            tLPPolSchema=mLPPolSet.get(i);
            //如果是附加险
            if (!tLPPolSchema.getMainPolNo().equals(tLPPolSchema.getPolNo()))
            {
                for (int j=1;j<=mLPPolSet.size();j++)
                {
         if (mLPPolSet.get(j).getPolNo().equals(tLPPolSchema.getMainPolNo()))
                    {
                        break;
                    }
         CError.buildErr(this,"保单险种号为"+tLPPolSchema.getPolNo()+"的险种为附加险，请选择其主险一起退保！");
                    return false;
                }
            }
                 }
         */

        if (!checkAppendPol())
        {
            return false;
        }
        
        //20101220 校验万能险
        if(!checkULIWT())
        {
        	return false;
        }

        return true;
    }
    

	/**
     * 校验当前险种是否有附加险，附加险不能单独存在，但可以单独退保
     * @return boolean
     */
    private boolean checkAppendPol()
    {
        //未选择的险种
        LCPolSet tLCPolSet = CommonBL.getLeavingPolNoInfo(mLPPolSet);
        if(tLCPolSet == null)
        {
            return true;
        }

        //若未选择的险种是附加险且其主险已选择，则提示
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);

//            调整需求,犹豫期退保也可以附加险单独退保 20110830
//            if(tLCPolSchema.getRiskCode().equals("730101")||tLCPolSchema.getRiskCode().equals("332401")){
//            	mErrors.addOneError("保单必须整单退保。");
//            	return false;
//            }
            
            if (tLCPolSchema.getMainPolNo().equals(tLCPolSchema.getPolNo()))
            {
                continue;  //不是附加险
            }

            for (int j = 1; j <= mLPPolSet.size(); j++)
            {
                LPPolSchema tLPPolSchema = mLPPolSet.get(j);
                if (tLPPolSchema.getPolNo().equals(tLCPolSchema.getMainPolNo())
                    && tLPPolSchema.getInsuredNo()
                    .equals(tLCPolSchema.getInsuredNo()))
                {
                    mErrors.addOneError("险种 " + tLPPolSchema.getRiskCode()
                                        + " 有附加险 " + tLCPolSchema.getRiskCode()
                                        + "，请选择其附加险一起退保。");
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 查询相应的险种信息
     * @param tLPPolSchema LPPolSchema
     * @return LPPolSchema
     */
    private LPPolSchema getLPPol(LPPolSchema tLPPolSchema)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tLPPolSchema.getPolNo());
        if (!tLCPolDB.getInfo())
        {
            mErrors.addOneError("查询险种信息出错");
            return null;
        }

        //交换LPPolSchema 和 LCPolSchema的数据
        LPPolSchema aLPPolSchema = new LPPolSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(aLPPolSchema, tLCPolDB.getSchema());

        aLPPolSchema.setEdorNo(tLPPolSchema.getEdorNo());
        aLPPolSchema.setEdorType(tLPPolSchema.getEdorType());

        return aLPPolSchema;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //获得保单险种数
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
        tLCPolSet = tLCPolDB.query();
        if (tLCPolDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询保单险种出错！");
            return false;
        }
        if (tLCPolSet.size() == mLPPolSet.size()) //整个保单退保
        {
            LPContBL tLPContBL = new LPContBL();
            tLPContBL.queryLPCont(mLPEdorItemSchema);
            if (tLPContBL.mErrors.needDealError())
            {
                CError.buildErr(this, "查询个人保单出错！");
                return false;
            }
            mLPContSet.add(tLPContBL.getSchema());

            //若是整单退保，则需处理工本费信息
            if(!getGBInfo())
            {
                return false;
            }
        }
        else
        {
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setContNo(mLPEdorItemSchema.getContNo());
            LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();

            //依次检验保单的每个被保人，若其所有险种都退保了，则该被保险人也被退掉
            for (int i = 1; i <= tLCInsuredSet.size(); i++)
            {
                LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(i);

                LCPolDB tempLCPolDB = new LCPolDB();
                tempLCPolDB.setContNo(tLCInsuredSchema.getContNo());
                tempLCPolDB.setInsuredNo(tLCInsuredSchema.getInsuredNo());
                LCPolSet tempLCPolSet = tempLCPolDB.query();
                if (tempLCPolDB.mErrors.needDealError())
                {
                    CError.buildErr(this, "查询保单险种出错！");
                    return false;
                }

                int polCount = 0;  //某被保人被退掉的险种数
                for (int j = 1; j <= mLPPolSet.size(); j++)
                {
                    if (mLPPolSet.get(j).getInsuredNo().equals(tLCInsuredSchema.
                            getInsuredNo()))
                    {
                        polCount++;
                    }
                }
                //如果该被保险人下所有险种都退保了，该被保险人也被退掉
                if (tempLCPolSet.size() == polCount)
                {
                    LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
                    ref.transFields(tLPInsuredSchema, tLCInsuredSchema);
                    tLPInsuredSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
                    tLPInsuredSchema.setEdorType(mLPEdorItemSchema.getEdorType());
                    mLPInsuredSet.add(tLPInsuredSchema);
                }
            }
        }

        //处理体检费信息
        if(!getTJInfo())
        {
            return false;
        }
        
//      20101220 zhanggm 处理万能附加重疾犹豫期退保，将月结扣除的风险保费返回到账户中
        if(!dealULIAppendFee(mLPPolSet))
        {
        	return false;
        }

        return true;
    }

    /**
     * 处理体检费
     * @return boolean
     */
    private boolean getTJInfo()
    {
        //若没有录入体检费信息，则删除之前可能存储了的体检费信息
        if(mLPPENoticeSet == null || mLPPENoticeSet.size() == 0)
        {
            LPPENoticeDB tLPPENoticeDB = new LPPENoticeDB();
            tLPPENoticeDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
            tLPPENoticeDB.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());
            tLPPENoticeDB.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPPENoticeDB.setContNo(mLPEdorItemSchema.getContNo());
            mMap.put(tLPPENoticeDB.query(), "DELETE");

            LPPENoticeItemDB tLPPENoticeItemDB = new LPPENoticeItemDB();
            tLPPENoticeItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
            tLPPENoticeItemDB.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());
            tLPPENoticeItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPPENoticeItemDB.setContNo(mLPEdorItemSchema.getContNo());
            mMap.put(tLPPENoticeItemDB.query(), "DELETE");
        }
        else
        {
            //查询体检费信息
            for (int i = 1; i <= mLPPENoticeSet.size(); i++)
            {
                LCPENoticeDB db = new LCPENoticeDB();
                db.setProposalContNo(mLPPENoticeSet.get(i).getProposalContNo());
                db.setPrtSeq(mLPPENoticeSet.get(i).getPrtSeq());
                if (!db.getInfo())
                {
                    mErrors.addOneError("体检费录入有误");
                    return false;
                }
                Reflections r = new Reflections();
                r.transFields(mLPPENoticeSet.get(i), db.getSchema());

                LCPENoticeItemDB tLCPENoticeItemDB = new LCPENoticeItemDB();
                tLCPENoticeItemDB.setProposalContNo(db.getProposalContNo());
                tLCPENoticeItemDB.setPrtSeq(db.getPrtSeq());
                LCPENoticeItemSet tLCPENoticeItemSet = tLCPENoticeItemDB.query();

                for (int j = 1; j <= tLCPENoticeItemSet.size(); j++)
                {
                    LPPENoticeItemSchema schema = new LPPENoticeItemSchema();
                    schema.setEdorAcceptNo(mLPPENoticeSet.get(i).
                                           getEdorAcceptNo());
                    schema.setEdorNo(mLPPENoticeSet.get(i).getEdorNo());
                    schema.setEdorType(mLPPENoticeSet.get(i).getEdorType());
                    r.transFields(schema, tLCPENoticeItemSet.get(j));
                    mLPPENoticeItemSet.add(schema);
                }
            }
        }

        return true;
    }

    /**
     * 处理工本费信息
     * @return boolean
     */
    private boolean getGBInfo()
    {
        //工本费记录
        if(mLPEdorEspecialDataSchema != null)
        {

            mMap.put(mLPEdorEspecialDataSchema, "DELETE&INSERT");
        }
        else
        {
            //若没有录入工本费信息，则删除之前可能存储了的工本费信息
            LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
            tLJSGetEndorseDB.setEndorsementNo(mLPEdorItemSchema.getEdorAcceptNo());
            tLJSGetEndorseDB.setFeeOperationType(mLPEdorItemSchema.getEdorType());
            tLJSGetEndorseDB.setFeeFinaType(BQ.FEEFINATYPE_GB);
            tLJSGetEndorseDB.setContNo(mLPEdorItemSchema.getContNo());
            mMap.put(tLJSGetEndorseDB.query(), "DELETE");

            LPEdorEspecialDataDB db = new LPEdorEspecialDataDB();
            db.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
            db.setEdorNo(mLPEdorItemSchema.getEdorNo());
            db.setEdorType(mLPEdorItemSchema.getEdorType());
            db.setPolNo(BQ.FILLDATA);
            db.setDetailType(BQ.DETAILTYPE_GB);
            mMap.put(db.query(), "DELETE");
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareData()
    {
        mMap.put(UpdateEdorState.getUpdateEdorStateSql(mLPEdorItemSchema),"UPDATE");
        String edorno=mLPEdorItemSchema.getEdorNo();
        String edortype=mLPEdorItemSchema.getEdorType();
        String sql=" edorno='"+edorno+"' and edortype='"+edortype+"'";
        mMap.put("delete from lppol where"+sql,"DELETE");
        mMap.put("delete from lpcont where"+sql,"DELETE");
        mMap.put("delete from lpinsured where"+sql,"DELETE");

        mMap.put(mLPContSet,"DELETE&INSERT");
        mMap.put(mLPInsuredSet,"DELETE&INSERT");
        mMap.put(mLPPolSet,"DELETE&INSERT");
        //mMap.put(mLJSGetEndorseSchema, "DELETE&INSERT");
        mMap.put(mLPEdorItemSchema,"UPDATE");
        mMap.put(mLPPENoticeSet, "DELETE&INSERT");
        mMap.put(mLPPENoticeItemSet, "DELETE&INSERT");

        mResult.clear();
        mResult.add(mMap);

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 判断YBT保单是否日终
     * 日终返回：true
     * 否则返回: false
     * @return boolean
     * add by fuxin 2008-7-10
     */
    private boolean checkYBTXBT()
    {
        //先得到保单信息
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLPEdorItemSchema_in.getContNo());
        if (!tLCContDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "PEdorWTDetailBL";
            tError.functionName = "checkYBT";
            tError.errorMessage = "查询保单信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCContSchema tLCContSchema = tLCContDB.getSchema();
        if(tLCContSchema.getCardFlag().equals("9") && tLCContSchema.getSaleChnl().equals("04"))
        {
            String sql = " select count(1) From db2inst1.lktransstatus a,lccont b where  resultbalance ='0' "
                       + " and a.polno = b.contno "
                       + " and b.contno='"+mLPEdorItemSchema_in.getContNo()+"'"
                       + " with ur "
                       ;
          SSRS tSSRS = new SSRS();
          ExeSQL tExeSQL = new ExeSQL();
          tSSRS = tExeSQL.execSQL(sql);
          if(tSSRS.GetText(1,1).equals("0"))
          {
              CError tError = new CError();
              tError.errorMessage = "该保单为银保通出单，尚未经过对帐，暂时不能退保!";
              this.mErrors.addOneError(tError);
              return false;
          }
        }
        if(tLCContSchema.getCardFlag().equals("a") && tLCContSchema.getSaleChnl().equals("03"))
        {
            String sql = " select count(1) From db2inst1.lktransstatus a,lccont b where  resultbalance ='0' "
                       + " and a.polno = b.contno "
                       + " and b.contno='"+mLPEdorItemSchema_in.getContNo()+"'"
                       + " with ur "
                       ;
          SSRS tSSRS = new SSRS();
          ExeSQL tExeSQL = new ExeSQL();
          tSSRS = tExeSQL.execSQL(sql);
          if(tSSRS.GetText(1,1).equals("0"))
          {
              CError tError = new CError();
              tError.errorMessage = "该保单为信保通出单，尚未经过对帐，暂时不能退保!";
              this.mErrors.addOneError(tError);
              return false;
          }
        }
        return true ;
    }

//	万能险套餐，所谓的主险不能单独退保=退保险种中包含主险，且不是整单退保，返回报错    
//  附加重疾可以单独退保
//  附加意外不可以单独退保，只能随主险一同退保。
//  主险退保时，两个附加险必须随主险一同退保。--已有
//  做过保全ZB-追加保费、LQ-部分领取的保单。只允许附加重疾单独犹豫期退保，不允许主险或整单犹豫期退保。
    private boolean checkULIWT() 
    {
    	String tContNo = mLPEdorItemSchema_in.getContNo();
    	
    	if (tContNo == null || tContNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorWTDetailBL";
            tError.functionName = "checkULIWT";
            tError.errorMessage = "保单号查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	
//      若选择的险种包含主险,判断附加险能否退保
//      做过保全ZB-追加保费、LQ-部分领取的保单。只允许附加重疾单独犹豫期退保，不允许主险或整单犹豫期退保。
        for (int i = 1; i <= mLPPolSet.size(); i++)
        {
        	if(CommonBL.isULIRisk(mLPPolSet.get(i).getRiskCode()))
        	{
        		String sql = "select 1 from LPEdorItem where Contno = '" + tContNo + "' and EdorType in ('LQ','ZB') " ;
            	String zbFlag = new ExeSQL().getOneValue(sql);
            	if(zbFlag.equals("1"))
            	{
            		CError tError = new CError();
                    tError.moduleName = "PEdorWTDetailBL";
                    tError.functionName = "checkULIWT";
                    tError.errorMessage = "保单"+ tContNo + "做过保全项目(部分领取或追加保费)，不能对主险进行犹豫期退保操作！";
                    this.mErrors.addOneError(tError);
                    return false;
            	}
        	}
        }
    	
    	 LCPolSet tLCPolSet = CommonBL.getLeavingPolNoInfo(mLPPolSet);
    	 if(tLCPolSet == null || tLCPolSet.size()==0)
         {
             return true;
         }
         else //非整单退保
         {
       	  for(int i=1;i<=mLPPolSet.size();i++)
             {   	
           	  //分红险主险和附加险必须同时退保
           	  if(mLPPolSet.get(i).getRiskCode().equals("730101"))
           	  {
           		  CError tError = new CError();
                     tError.moduleName = "PEdorCTDetailBL";
                     tError.functionName = "checkULICT";
                     tError.errorMessage = "保单号查询失败!";
                     this.mErrors.addOneError("险种"+mLPPolSet.get(i).getRiskCode()+"只能同分红险的附加险一同退保!");
                     return false;
           	  }
             }
         }
//    	保单不包含万能险，不判断
    	if(!CommonBL.hasULIRisk(tContNo))
    	{
    		return true;
    	}
    	    	
    	
        //未选择的险种数量为0，整单退保
       
        if(tLCPolSet == null || tLCPolSet.size()==0)
        {
            return true;
        }
        else //非整单退保
        {
      	  for(int i=1;i<=mLPPolSet.size();i++)
            {
      		  //万能险套餐，所谓的主险不能单独退保=退保险种中包含主险，且不是整单退保，返回报错
          	  if(CommonBL.isULIRisk(mLPPolSet.get(i).getRiskCode()))
          	  {
          		  CError tError = new CError();
                    tError.moduleName = "PEdorCTDetailBL";
                    tError.functionName = "checkULICT";
                    tError.errorMessage = "保单号查询失败!";
                    this.mErrors.addOneError("险种"+mLPPolSet.get(i).getRiskCode()+"只能同万能主险一同退保");
                    return false;
          	  }
          	  //6201不准单独退保        	  
          	  if(CommonBL.is62Risk(mLPPolSet.get(i).getRiskCode()))
          	  {
          		  CError tError = new CError();
                    tError.moduleName = "PEdorCTDetailBL";
                    tError.functionName = "checkULICT";
                    tError.errorMessage = "保单号查询失败!";
                    this.mErrors.addOneError("险种"+mLPPolSet.get(i).getRiskCode()+"只能同万能主险一同退保");
                    return false;
          	  }
            }
        }
        
        //若未选择的险种包含主险,判断附加险能否退保
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            if(CommonBL.isULIRisk(tLCPolSchema.getRiskCode()))
        	{
        		for (int j = 1; j <= mLPPolSet.size(); j++)
                {
        			LPPolSchema tLPPolSchema = mLPPolSet.get(j);
        			LMRiskAppDB subLMRiskAppDB = new LMRiskAppDB();
        			subLMRiskAppDB.setRiskCode(tLPPolSchema.getRiskCode());
        			if(subLMRiskAppDB.getInfo())
        			{
        				String tRiskType3 = subLMRiskAppDB.getRiskType3();
        				if(tRiskType3.equals("3")) //重疾
        				{
        					continue;
        				}
        				else if(tRiskType3.equals("4")) //意外
        				{
        					CError tError = new CError();
        		            tError.errorMessage = "附加险"+subLMRiskAppDB.getRiskCode()+"只能和主险同时退保!";
        		            this.mErrors.addOneError(tError);
        		            return false;
        				}
        			}
                }
            }
        }
        return true;
	}
    
//  20101220 zhanggm 处理万能附加重疾犹豫期退保，将月结扣除的风险保费返回到账户中
    private boolean dealULIAppendFee(LPPolSet cLPPolSet)
    {
    	
    	String tContNo = mLPEdorItemSchema.getContNo();
    	
    	if (tContNo == null || tContNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorWTDetailBL";
            tError.functionName = "dealULIAppendFee";
            tError.errorMessage = "保单号查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	
//    	保单不包含万能险，不需要处理
    	if(!CommonBL.hasULIRisk(tContNo))
    	{
    		return true;
    	}
    	
    	// 退保的险种,含有万能主险，整单退保，不需要处理
    	for(int i=1; i<=cLPPolSet.size(); i++)
    	{
    		if(CommonBL.isULIRisk(cLPPolSet.get(i).getRiskCode()))
    		{
				String deltrace = "delete from LPInsureAccTrace where EdorNo = '" + mLPEdorItemSchema.getEdorNo() 
				+ "' and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' ";
				String delfeetrace = "delete from LPInsureAccFeeTrace where EdorNo = '" + mLPEdorItemSchema.getEdorNo()
				+ "' and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' ";
				String delacc = "delete from LPInsureAcc where EdorNo = '" + mLPEdorItemSchema.getEdorNo() 
				+ "' and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' ";
				String delclass = "delete from LPInsureAccClass where EdorNo = '" + mLPEdorItemSchema.getEdorNo()
				+ "' and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' ";
				String delfee = "delete from LPInsureAccFee where EdorNo = '" + mLPEdorItemSchema.getEdorNo() 
				+ "' and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' ";
				String delclassfee = "delete from LPInsureAccClassFee where EdorNo = '" + mLPEdorItemSchema.getEdorNo()
				+ "' and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' ";
				
				mMap.put(deltrace, SysConst.DELETE);
				mMap.put(delfeetrace, SysConst.DELETE);
				mMap.put(delacc, SysConst.DELETE);
				mMap.put(delclass, SysConst.DELETE);
				mMap.put(delfee, SysConst.DELETE);
				mMap.put(delclassfee, SysConst.DELETE);
    			return true ;
    		}
    	}
    	
    	//不含主险，则只能是附加重疾单独退保
    	for(int j=1; j<=cLPPolSet.size(); j++)
    	{
    		LPPolSchema tLPPolSchema = cLPPolSet.get(j);
            String tRiskCode = tLPPolSchema.getRiskCode();
            String tPolNo = tLPPolSchema.getPolNo();
            String tMainPolNo = tLPPolSchema.getMainPolNo();//主险PolNo
           
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tRiskCode);
            if(tLMRiskAppDB.getInfo())
            {
            	String tRiskType3 = tLMRiskAppDB.getRiskType3();
            	if(tRiskType3.equals("3")) //重疾
				{
					LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
					tLCInsureAccTraceDB.setContNo(tContNo);
					tLCInsureAccTraceDB.setPolNo(tMainPolNo);
					tLCInsureAccTraceDB.setPayPlanCode(tRiskCode);
					LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
					if(tLCInsureAccTraceSet==null || tLCInsureAccTraceSet.size()==0)
					{
						continue;
					}
					double sumFee = 0.0;
					for(int k=1; k<=tLCInsureAccTraceSet.size(); k++)
					{
						sumFee += tLCInsureAccTraceSet.get(k).getMoney();
					}
					
					if(sumFee==0.0)
					{
						continue;
					}
					
					LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
					tLCInsureAccDB.setContNo(tContNo);
					tLCInsureAccDB.setPolNo(tMainPolNo);
					LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
					if(tLCInsureAccSet == null || tLCInsureAccSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTDetailBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败Acc!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccSet.get(1).getSchema();
					LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
					ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
					double oldInsuAccBala = tLCInsureAccSchema.getInsuAccBala();
					double newInsuAccBala = oldInsuAccBala-sumFee;
					tLPInsureAccSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccSchema.setEdorType(mLPEdorItemSchema.getEdorType());
					tLPInsureAccSchema.setInsuAccBala(newInsuAccBala);
					tLPInsureAccSchema.setMakeDate(mCurrentDate);
					tLPInsureAccSchema.setMakeTime(mCurrentTime);
					tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccSchema.setModifyDate(mCurrentDate);
					tLPInsureAccSchema.setModifyTime(mCurrentTime);
					
					LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
					tLCInsureAccClassDB.setContNo(tContNo);
					tLCInsureAccClassDB.setPolNo(tMainPolNo);
					LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
					if(tLCInsureAccClassSet == null || tLCInsureAccClassSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTDetailBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败Class!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LCInsureAccClassSchema tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1).getSchema();
					LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
					ref.transFields(tLPInsureAccClassSchema, tLCInsureAccClassSchema);
					tLPInsureAccClassSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccClassSchema.setEdorType(mLPEdorItemSchema.getEdorType());
					tLPInsureAccClassSchema.setInsuAccBala(newInsuAccBala);
					tLPInsureAccClassSchema.setMakeDate(mCurrentDate);
					tLPInsureAccClassSchema.setMakeTime(mCurrentTime);
					tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
					tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
					
					LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
					tLCInsureAccFeeDB.setContNo(tContNo);
					tLCInsureAccFeeDB.setPolNo(tMainPolNo);
					LCInsureAccFeeSet tLCInsureAccFeeSet = tLCInsureAccFeeDB.query();
					if(tLCInsureAccFeeSet == null || tLCInsureAccFeeSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTDetailBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败Fee!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LCInsureAccFeeSchema tLCInsureAccFeeSchema = tLCInsureAccFeeSet.get(1).getSchema();
					LPInsureAccFeeSchema tLPInsureAccFeeSchema = new LPInsureAccFeeSchema();
					ref.transFields(tLPInsureAccFeeSchema, tLCInsureAccFeeSchema);
					tLPInsureAccFeeSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccFeeSchema.setEdorType(mLPEdorItemSchema.getEdorType());
					double oldFee = tLCInsureAccFeeSchema.getFee();
					double newFee = oldFee+sumFee;
					tLPInsureAccFeeSchema.setFee(newFee);
					tLPInsureAccFeeSchema.setMakeDate(mCurrentDate);
					tLPInsureAccFeeSchema.setMakeTime(mCurrentTime);
					tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccFeeSchema.setModifyDate(mCurrentDate);
					tLPInsureAccFeeSchema.setModifyTime(mCurrentTime);
					
					LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
					tLCInsureAccClassFeeDB.setContNo(tContNo);
					tLCInsureAccClassFeeDB.setPolNo(tMainPolNo);
					LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
					if(tLCInsureAccClassFeeSet == null || tLCInsureAccClassFeeSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTDetailBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败ClassFee!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = tLCInsureAccClassFeeSet.get(1).getSchema();
					LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
					ref.transFields(tLPInsureAccClassFeeSchema, tLCInsureAccClassFeeSchema);
					tLPInsureAccClassFeeSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccClassFeeSchema.setEdorType(mLPEdorItemSchema.getEdorType());
					tLPInsureAccClassFeeSchema.setFee(newFee);
					tLPInsureAccClassFeeSchema.setMakeDate(mCurrentDate);
					tLPInsureAccClassFeeSchema.setMakeTime(mCurrentTime);
					tLPInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
					tLPInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
					
					String tLimit = PubFun.getNoLimit(mLPEdorItemSchema.getManageCom());
		            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
		            
					LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
					ref.transFields(tLPInsureAccTraceSchema, tLPInsureAccClassSchema);
					tLPInsureAccTraceSchema.setSerialNo(serNo);
					tLPInsureAccTraceSchema.setOtherNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccTraceSchema.setOtherType(BQ.NOTICETYPE_P);
					tLPInsureAccTraceSchema.setMoneyType(BQ.EDORTYPE_WT);
					tLPInsureAccTraceSchema.setMoney(-sumFee);
					tLPInsureAccTraceSchema.setPayDate(mLPEdorItemSchema.getEdorValiDate());
					tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
					tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
					tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
					tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
					
					String tLimit1 = PubFun.getNoLimit(mLPEdorItemSchema.getManageCom());
		            String serNo1 = PubFun1.CreateMaxNo("SERIALNO", tLimit1);
		            
					LPInsureAccFeeTraceSchema tLPInsureAccFeeTraceSchema = new LPInsureAccFeeTraceSchema();
					ref.transFields(tLPInsureAccFeeTraceSchema, tLPInsureAccClassFeeSchema);
					tLPInsureAccFeeTraceSchema.setSerialNo(serNo1);
					tLPInsureAccFeeTraceSchema.setOtherNo(mLPEdorItemSchema.getEdorNo());
					tLPInsureAccFeeTraceSchema.setOtherType(BQ.NOTICETYPE_P);
					tLPInsureAccFeeTraceSchema.setMoneyType(BQ.EDORTYPE_WT);
					tLPInsureAccFeeTraceSchema.setFee(sumFee);
					tLPInsureAccFeeTraceSchema.setPayDate(mLPEdorItemSchema.getEdorValiDate());
					tLPInsureAccFeeTraceSchema.setState("0");
					tLPInsureAccFeeTraceSchema.setFeeCode("000012");
					tLPInsureAccFeeTraceSchema.setMakeDate(mCurrentDate);
					tLPInsureAccFeeTraceSchema.setMakeTime(mCurrentTime);
					tLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
					tLPInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
					
					mMap.put(tLPInsureAccSchema, SysConst.DELETE_AND_INSERT);
					mMap.put(tLPInsureAccClassSchema, SysConst.DELETE_AND_INSERT);
					mMap.put(tLPInsureAccFeeSchema, SysConst.DELETE_AND_INSERT);
					mMap.put(tLPInsureAccClassFeeSchema, SysConst.DELETE_AND_INSERT);
					
					String deltrace = "delete from LPInsureAccTrace where EdorNo = '" + mLPEdorItemSchema.getEdorNo() 
					+ "' and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' ";
					String delfeetrace = "delete from LPInsureAccFeeTrace where EdorNo = '" + mLPEdorItemSchema.getEdorNo()
					+ "' and EdorType = '" + mLPEdorItemSchema.getEdorType() + "' ";
					
					mMap.put(deltrace, SysConst.DELETE);
					mMap.put(tLPInsureAccTraceSchema, SysConst.INSERT);
					mMap.put(delfeetrace, SysConst.DELETE);
					mMap.put(tLPInsureAccFeeTraceSchema, SysConst.INSERT);
					
				}
				else if(tRiskType3.equals("4")) //意外
				{
					continue;
				}
            }
    	}
    	
    	return true;
    }


}
