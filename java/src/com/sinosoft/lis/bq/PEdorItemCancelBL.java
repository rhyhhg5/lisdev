package com.sinosoft.lis.bq;

import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title:团单整单删除BL层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong modify by Alex
 * @version 1.0
 */
public class PEdorItemCancelBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate;
    private String mOperator;
    private String mManageCom;

    //状态 1-保全录入完成 2-保单申请确认 3-未录入 4-理算
    private String mEdorState;

    //显示的层级 1-仅保单 2-保单,被保人 3-保单,被保人,险种
    private String mDisplayType;

    //是否删除并复制

    /** 撤销申请原因*/
    private String delReason;
    private String reasonCode;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema(); //新的表结构
    private LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();

    private TransferData tTransferData = new TransferData();

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    public PEdorItemCancelBL()
    {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;


        //将VData数据还原成业务需要的类
        if (this.getInputData() == false)
        {
           // System.out.println("批改状态不可以");
            return false;

        }

        System.out.println("---getInputData successful---");

        if (this.dealData(tLPEdorItemSchema) == false)
        {
            return false;
        }


        //如果为个单,则执行该逻辑
        if(("00000000000000000000").equals(tLPEdorItemSchema.getGrpContNo()))
        {
          System.out.println("---dealData successful---");
          if (this.updateData() == false) {
            return false;
          }
        }
        System.out.println("---inputdata successful---");

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        PubSubmit tPubSubmit = new PubSubmit();

//        if (!tPubSubmit.submitData(mResult, cOperate))
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//
//            return false;
//        }

        return true;
    }

    /**
     * 将UI层传输来得数据根据业务还原成具体的类
     * @return boolean
     */
    private boolean getInputData()
    {
        //全局变量实例
        ExeSQL tExeSQL = new ExeSQL();

        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));

        //删除原因和原因编码
        tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
        delReason = (String) tTransferData.getValueByName("DelReason");
        reasonCode = (String) tTransferData.getValueByName("ReasonCode");

        if (mGlobalInput == null)
        {
            mErrors.addOneError(new CError("没有得到全局量信息"));

            return false;
        }

        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        //团体保单实例
        mLPEdorItemSchema.setSchema((LPEdorItemSchema) mInputData.
                                    getObjectByObjectName(
                                            "LPEdorItemSchema", 0));

        if (mLPEdorItemSchema == null)
        {
            this.mErrors.addOneError(new CError("传入的团单信息为空！"));

            return false;
        }
        this.mEdorState = mLPEdorItemSchema.getEdorType();
        System.out.println("class信息：PEdorItemCancelBL-->mEdorState＝" +
                           mEdorState);

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();


        String sql = "";
        if("EDORITEM".equals(mOperate))//删除项目时不能删除申请确认的项目
        {
         sql = "select * from LPEdorItem where EdorAcceptNo ='"+mLPEdorItemSchema.getEdorAcceptNo()+"'"
             +" and EdorNo='"+mLPEdorItemSchema.getEdorNo()+"'"
             +" and ContNo='"+mLPEdorItemSchema.getContNo()+"'"
             +" and EdorType='"+mLPEdorItemSchema.getEdorType() +"'"
             +" and PolNo = '"+mLPEdorItemSchema.getPolNo()+"'"
             +" and InsuredNo = '"+mLPEdorItemSchema.getInsuredNo()+"' and EdorState not in('0') ";
        }

        else if("EDORMAIN".equals(mOperate)) //删除团单时不能删除批改确认的项目
        {
          sql = "select * from LPEdorItem where EdorAcceptNo ='"+mLPEdorItemSchema.getEdorAcceptNo()+"'"
              + " and EdorNo='" + mLPEdorItemSchema.getEdorNo() + "'"
              + " and ContNo='" + mLPEdorItemSchema.getContNo() + "'"
              + " and EdorType='" + mLPEdorItemSchema.getEdorType() + "'"
              + " and PolNo = '"+mLPEdorItemSchema.getPolNo()+"'"
              + " and InsuredNo = '" + mLPEdorItemSchema.getInsuredNo() + "'"
              + " and EdorState not in('0')";
        }
        System.out.println(sql);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);

        if (tLPEdorItemSet.size() == 0 || tLPEdorItemSet == null)
        {
          CError tError = new CError();
          tError.moduleName = "ContCancel";
          tError.functionName = "dealData";
          tError.errorMessage = "没有相应的保全项目或者该保全项目已经确认!";
          this.mErrors.addOneError(tError);
          System.out.println(tError);
          return false;
        }
        tLPEdorItemSchema = tLPEdorItemSet.get(1);


        //增加在后台的校验
        String edorState = tLPEdorItemSchema.getEdorState();
        if ("EDORITEM".equals(mOperate) && ("0".equals(edorState))) {
          CError tError = new CError();
          tError.moduleName = "GEdorItemCancelBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "该项目已经保全确认,不能删除!";
          this.mErrors.addOneError(tError);
          System.out.println(tError);
          return false;
        }


        //判断选中批改项目是否为最近的批改项目

//        if (!mEdorState.equals("3")) //如果状态为3则表示“未录入”,不对是否为最近的批改项目进行判断
//        {
//            String strsql =
//                    "select Max(a.MakeDate) from lpedoritem a where a.edorno='" +
//                    mLPEdorItemSchema.getEdorNo() + "'";
//            String LatestDate = tExeSQL.getOneValue(strsql).substring(0, 10);
//            System.out.println(LatestDate + "         " +
//                               mLPEdorItemSchema.getMakeDate().toString());
//            if (mLPEdorItemSchema.getMakeDate().toString().equals(LatestDate))
//            {
//                System.out.println("日期相等");
//                strsql =
//                        "select Max(a.MakeTime) from lpedoritem a where a.edorno= '" +
//                        mLPEdorItemSchema.getEdorNo() + "'  and a.makeDate='" +
//                        LatestDate + "'";
//
//                String LatestTime = tExeSQL.getOneValue(strsql).toString();
//                System.out.println(LatestTime + "         " +
//                                   mLPEdorItemSchema.getMakeTime().toString());
//                if (!mLPEdorItemSchema.getMakeTime().toString().equals(
//                        LatestTime))
//                {
//                    this.mErrors.addOneError(new CError("该项目是最后一条，无法删除!"));
//                    return false;
//                }
//
//            }
//            else
//            {
//                this.mErrors.addOneError(new CError("该项目不是最后一条，无法删除!"));
//                return false;
//            }
//        }

        return true;
    }



    /**
     * 准备数据
     * 判断删除的项目是否是最后一条,如果本条保全项目之后还有被确认的
     * 和录入完成的项目(EdorState为1,2,4)
     * 则删除之后做的除item表中的数据,并将状态置为3-未录入
     * @return boolean
     */
    private boolean updateData()
   {
      LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
      LPEdorItemDB  tLPEdorItemDB = new LPEdorItemDB();

      String strSql = "select * from LPEdorItem where EdorState in ('1','2','4')"
                 +" and EdorNo='" +tLPEdorItemSchema.getEdorNo() + "'  and (MakeDate>'"
                 +tLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
                  tLPEdorItemSchema.getMakeDate() + "' and MakeTime>'" +
                  tLPEdorItemSchema.getMakeTime() +
                  "')) order by MakeDate,MakeTime desc";

              System.out.println(strSql);
     tLPEdorItemSet = tLPEdorItemDB.executeQuery(strSql);
     if(tLPEdorItemSet.size() != 0 && tLPEdorItemSet != null)
     {
       for(int i = 1 ; i <= tLPEdorItemSet.size(); i++)
       {
         boolean flag = dealData(tLPEdorItemSet.get(i));
         if (!flag)
         {
           CError tError = new CError();
           tError.moduleName = "ContCancel";
           tError.functionName = "dealData";
           tError.errorMessage = "更新该项目后的数据失败!";
           this.mErrors.addOneError(tError);
           System.out.println(tError);
           return false;
         }
         else
         {
             tLPEdorItemSet.get(i).setEdorState("3");//重新置为未录入
         }
       }
       mMap.put(tLPEdorItemSet,"UPDATE");

     }

//
//     //if(mEdorState.equals(""))

     return true;

   }

    /**
     * 对业务数据进行加工
     * @return boolean
     */
    private boolean dealData(LPEdorItemSchema aLPEdorItemSchema)
    {

        String delSql;
        String tEdorNo = aLPEdorItemSchema.getEdorNo();
        String tEdorType = aLPEdorItemSchema.getEdorType();
        String tContNo = aLPEdorItemSchema.getContNo();
        String tInsuredNo = aLPEdorItemSchema.getInsuredNo();
        String tPolNo = aLPEdorItemSchema.getPolNo();

            String[] EdorLevelTable =
                    {"LPEdorPrint","LPEdorPrint2","LPContSub"};

            String[] ContLevelTable =
                    {
                    "LPCont", "LPCustomerImpart",
                    "LPCustomerImpartDetail",
                    "LPCustomerImpartParams",
                    "LPCUWSub", "LPCUWMaster", "LPCUWError",
                    "LPPol", "LPBnf", "LPDuty", "LPGet", "LPPrem",
                    "LPSpec", "LPBudgetResult"};
            String[] CustomerLevelTable =
                    {
                    "LPAppnt", "LPInsured", "LPPerson",
                    "LPAddress", "LPAccount"};
            String[] PolLevelTable =
                    {
                    "LPInsuredRelated", "LPGetToAcc",
                    "LPPrem_1", "LPPremToAcc",
                    "LPInsureAccFee", "LPInsureAccClassFee",
                    "LPInsureAcc", "LPInsureAccClass", "LPInsureAccTrace",
                    "LPAccMove","LPAppntTrace",
                    "LPUWMaster","LPUWSub","LPUWError"};
//            String[] tableForDel =
//                    {
//                    "LPGeneral", "LPCont", "LPGeneralToRisk",
//                    "LPPol",
//                    "LPGetToAcc", "LPInsureAcc",
//                    "LPPremToAcc", "LPPrem", "LPGet", "LPDuty",
//                    "LPPrem_1",
//                    "LPInsureAccClass", "LPInsureAccTrace",
//                    "LPContPlanDutyParam",
//                    "LPContPlanRisk", "LPContPlan", "LPAppnt",
//                    "LPCustomerImpart",
//                    "LPInsuredRelated", "LPBnf", "LPInsured",
//                    "LPCustomerImpartParams", "LPInsureAccFee",
//                    "LPInsureAccClassFee",
//                    "LPContPlanFactory", "LPContPlanParam",
//                    "LPMove", "LPAppntTrace", "LPLoan",
//                    "LPReturnLoan", "LPEdorPrint3", "LPGUWError",
//                    "LPGUWMaster",
//                    "LPCUWError",
//                    "LPCUWMaster", "LPCUWSub", "LPUWError",
//                    "LPUWMaster",
//                    "LPUWSub",
//                    "LPGCUWError", "LPGCUWMaster", "LPGCUWSub",
//                    "LPGUWSub",
//                    "LPSpec", "LPIssuePol",
//                    "LPCSpec", "LPAccount", "LPPerson",
//                    "LPAddress",
//                    "LPPayRuleFactory", "LPPayRuleParams",
//                    "LPCustomerImpartDetail", "LPAccMove",
//                    "LPEdorItem"
//            };

            for (int i = 0; i < EdorLevelTable.length; i++)
            {
              delSql = "delete from  " + EdorLevelTable[i]
                  + " where  EdorNo = '" + tEdorNo +"'";
              mMap.put(delSql, "DELETE");
            }

            delSql = "delete from LPIssuePol where  EdorNo = '" + tEdorNo
                   + "' and edortype='"+tEdorType+"'" ;
            mMap.put(delSql, "DELETE");

            delSql = "delete from LPEdorEspecialData "
                     + "where  EdorNo = '" + tEdorNo + "' "
                     + "  and EdorType = '" + tEdorType + "' ";
            mMap.put(delSql, "DELETE");



            /******保单级*****************************************************/
            for (int i = 0; i < ContLevelTable.length; i++)
            {
              delSql = "delete from  " + ContLevelTable[i]
                  + " where  EdorNo = '" + tEdorNo
                  + "' and EdorType='" + tEdorType + "'"
                  + " and ContNo = '" + tContNo + "'";
              mMap.put(delSql, "DELETE");
            }
            //删除保单迁移
            delSql = "delete from LPMove where  EdorNo = '" + tEdorNo
                  + "' and EdorType='" + tEdorType + "'"
                  + " and ContNoOld = '" + tContNo + "'";
            mMap.put(delSql, "DELETE");


            /******被保人级****************************************************/
            //被保人号不为空,删除以下表
//          更改为被保人号为000000时，会删除部分P表信息。防止AE等项目时，删除不干净
            if (!aLPEdorItemSchema.getInsuredNo().equals("000000"))
            {
            	delSql = "delete from  LPInsured where EdorNo = '" +
                        tEdorNo + "' and EdorType='" + tEdorType +
                        "' and insuredno='" + tInsuredNo +
//                        "' and contNo='"+ tContNo +"'";
                        "'";
                    mMap.put(delSql, "DELETE");
                    delSql = "delete from  LPPerson where  EdorNo = '" +
                        tEdorNo + "' and EdorType='" + tEdorType +
                        "' and customerno='" + tInsuredNo +
                        "'";
                    mMap.put(delSql, "DELETE");
                    delSql = "delete from  LPAppnt where EdorNo = '" +
                        tEdorNo + "' and EdorType='" + tEdorType +
                        "' and appntno='" + tInsuredNo + "'";
                    mMap.put(delSql, "DELETE");

                    delSql = "delete from  LPAddress where EdorNo = '" +
                        tEdorNo + "' and EdorType='" + tEdorType +
                        "' and customerNo='" + tInsuredNo + "'";
                    mMap.put(delSql, "DELETE");

                    delSql = "delete from  LPAccount where EdorNo = '" +
                        tEdorNo + "' and EdorType='" + tEdorType +
                        "' and customerNo='" + tInsuredNo + "'";
                    mMap.put(delSql, "DELETE");
            }else {
            		delSql = "delete from  LPInsured where EdorNo = '" +
                            tEdorNo + "' and EdorType='" + tEdorType +
//                            "' and insuredno='" + tInsuredNo +
                            "'";
                        mMap.put(delSql, "DELETE");
                        delSql = "delete from  LPPerson where  EdorNo = '" +
                            tEdorNo + "' and EdorType='" + tEdorType +"'";
                        mMap.put(delSql, "DELETE");
                        delSql = "delete from  LPAppnt where EdorNo = '" +
                            tEdorNo + "' and EdorType='" + tEdorType + "'";
                        mMap.put(delSql, "DELETE");

                        delSql = "delete from  LPAddress where EdorNo = '" +
                            tEdorNo + "' and EdorType='" + tEdorType + "'";
                        mMap.put(delSql, "DELETE");

                        delSql = "delete from  LPAccount where EdorNo = '" +
                            tEdorNo + "' and EdorType='" + tEdorType + "'";
                        mMap.put(delSql, "DELETE");
            }

            /******险种级****************************************************/
            //如果险种号不为空则删除以下表
            if(!aLPEdorItemSchema.getPolNo().equals("000000"))
            {
               for (int i = 0; i < PolLevelTable.length; i++) {
                delSql = "delete from  " + PolLevelTable[i]
                    + " where  EdorNo = '" + tEdorNo
                    + "' and EdorType='" + tEdorType + "'"
                    + " and polno = '" + tPolNo + "'";
                mMap.put(delSql, "DELETE");
              }
            }


//            String sqlDel = "delete from LPPENoticeItem where edorno= '" +
//                            tEdorNo + "'";
//            mMap.put(sqlDel, "DELETE");
//            sqlDel = "delete from LPPENotice where edorno= '" + tEdorNo + "'";
//            mMap.put(sqlDel, "DELETE");


        mMap.put("delete from lpedorMain where EdorNo= '" + tEdorNo +
                 "' and 0=(select count(*) from LpEdorItem where lpedorItem.EdorNo='" +
                 tEdorNo + "' and lpedorItem.edortype='"+tEdorType+"')",
                 "DELETE"); //若是最后一条项目，则删除对应批单

        //qulq 061221 设定允许银行转帐标志 (撤消的是最后一个就)
        mMap.put("update ljspay set cansendbank ='0' where othernotype ='2' and otherno ='"+tContNo+
                        "' and 1 = (select count(*) from LpEdorItem where lpedorItem.EdorNo='"+
                        tEdorNo + "' and lpedorItem.contno='"+tContNo+"')",
                        "UPDATE");

        if("EDORITEM".equals(mOperate))
        {
          //更新主表相关费用字段
          LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
          tLPEdorMainDB.setEdorNo(tEdorNo);
    //        tLPEdorMainDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
          tLPEdorMainDB.setContNo(mLPEdorItemSchema.getContNo());
          LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.query();
          if (tLPEdorMainSet == null || tLPEdorMainSet.size() < 1) {
            this.mErrors.addOneError(new CError("批单主表中没有相关记录"));
            return false;
          }
          LPEdorMainSchema tLPEdorMainSchema = tLPEdorMainSet.get(1);
          ExeSQL tExeSQL = new ExeSQL();
          SSRS tSSRS = new SSRS();
          String strsql =
              "select sum(ChgPrem),sum(ChgAmnt),sum(GetMoney),sum(GetInterest) from LPEdorItem a where a.edorno='" +
              mLPEdorItemSchema.getEdorNo() + "' ";
          tSSRS = tExeSQL.execSQL(strsql);
          tLPEdorMainSchema.setChgPrem(tSSRS.GetText(1, 1));
          tLPEdorMainSchema.setChgAmnt(tSSRS.GetText(1, 2));
          tLPEdorMainSchema.setGetMoney(tSSRS.GetText(1, 3));
          tLPEdorMainSchema.setGetInterest(tSSRS.GetText(1, 4));
          tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
          PubFun.fillDefaultField(tLPEdorMainSchema);
          mMap.put(tLPEdorMainSchema, "UPDATE");
        }

        //删除批改补退费表的相关记录
        mMap.put("delete from LJSGetEndorse where EndorsementNo='" +
                 aLPEdorItemSchema.getEdorNo() + "' and FeeOperationType='" +
                 aLPEdorItemSchema.getEdorType() + "'", "DELETE");

        try
        {
            Class tClass = Class.forName("com.sinosoft.lis.bq.PEdor" +
                                         tEdorType + "CancelBL");
            EdorCancel tGrpEdorCancel = (EdorCancel) tClass.newInstance();
            VData tVData = new VData();
            System.out.println("%%%%%%%%%%%%%%" + tEdorType);
            tVData.add(mLPEdorItemSchema);
            tVData.add(mGlobalInput);

            if (!tGrpEdorCancel.submitData(tVData,
                                           "EDORITEMCANCEL||" + tEdorType))
            {
                return false;
            }
            else
            {
                VData rVData = tGrpEdorCancel.getResult();
                MMap tMap = new MMap();
                tMap = (MMap) rVData.getObjectByObjectName("MMap", 0);
                if (tMap == null)
                {
                    mErrors.addOneError(new CError("得到保单号为：" +
                            mLPEdorItemSchema.getGrpContNo() +
                            "保全项目为:" +
                            tEdorType + "的保全申请确认结果时失败！"));
                    return false;

                }
                else
                {
                    mMap.add(tMap);
                }
            }
        }
        catch (ClassNotFoundException ex)
        {
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        if(!AppAccUoDo())
        {
            return false;
        }

        return true;
    }

    private boolean AppAccUoDo()
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(tLPEdorItemSchema.getEdorNo());
        if (!tLPEdorAppDB.getInfo()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLPEdorAppDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorItemCancelBL";
            tError.functionName = "AppAccUoDo";
            tError.errorMessage = "查询保全信息主表出错!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }

        AppAcc tAppAcc = new AppAcc();
        String noticeType="";
        if(tLPEdorAppDB.getOtherNoType().equals("1"))
        {
            noticeType=BQ.NOTICETYPE_P;
        }
        if (tLPEdorAppDB.getOtherNoType().equals("2")) {
            noticeType=BQ.NOTICETYPE_G;
        }
        MMap map = tAppAcc.accUnDo(tLPEdorAppDB.getOtherNo(), tLPEdorItemSchema.getEdorNo(),
                               noticeType);

        if (map == null) {
            if (!tAppAcc.mErrors.getLastError().equals("查询投保人帐户轨迹表记录不存在!")) {
                // @@错误处理
                this.mErrors.copyAllErrors(tAppAcc.mErrors);
                return false;
            }
        }
        mMap.add(map);

        return true;
    }


    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData()
    {

      //删除并备份
      LOBEdorItemSchema cLOBEdorItemSchema = new LOBEdorItemSchema();
      Reflections crf = new Reflections();
      crf.transFields(cLOBEdorItemSchema, tLPEdorItemSchema); //将一条记录整体复制
      cLOBEdorItemSchema.setReason(delReason); //添加撤销原因
      cLOBEdorItemSchema.setMakeDate(theCurrentDate); //设置修改时间为当前时间
      cLOBEdorItemSchema.setMakeTime(theCurrentTime);
      cLOBEdorItemSchema.setReasonCode(reasonCode); //添加撤销原因

      mMap.put(cLOBEdorItemSchema, "DELETE&INSERT");
      mMap.put(tLPEdorItemSchema, "DELETE");
      
//    若是最后一条保全项目，则删除对应Main表,080515,Zhanggm
      if(!delLPEdorMain(tLPEdorItemSchema))
      {
    	  CError tError = new CError();
	      tError.moduleName = "PEdorItemBL";
	      tError.functionName = "checkData";
	      tError.errorMessage = "删除保全项目对应Main表失败！";
	      this.mErrors.addOneError(tError);
      }
      
      mResult.clear();
      mResult.add(mMap);
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public MMap getMap()
    {
      return mMap;
    }

//  若是最后一条保全项目，则删除对应Main表,080515,Zhanggm
    private boolean delLPEdorMain(LPEdorItemSchema tItemSchema)
    {
    	System.out.println("若是最后一条保全项目，则删除对应Main表,080515,Zhanggm");
        String sql = "  delete from LPEdorMain a "
      	           + "  where EdorNo = '" + tItemSchema.getEdorNo() + "'"
                   + "      and EdorAcceptNo = '"+ tItemSchema.getEdorAcceptNo() + "'"
                   + "      and 0 = (select count(1) from LPEdorItem "
                   + "               where EdorNo = a.EdorNo "
                   + "                   and EdorAcceptNo = a.EdorAcceptNo)";
        System.out.println(sql);
        mMap.put(sql , "DELETE"); 
        return true;
    }

    public static void main(String[] args)
    {
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorNo("410000000000190");
        tLPEdorItemSchema.setGrpContNo("240110000000123");
        tLPEdorItemSchema.setEdorType("BB");
        tLPEdorItemSchema.setEdorState("1");
        tLPEdorItemSchema.setMakeDate("2005-3-26");
        tLPEdorItemSchema.setMakeTime("10:23:15");
        tVData.addElement(tGlobalInput);
        tVData.addElement(tLPEdorItemSchema);

        GEdorItemCancelBL tGEdorItemCancelBL = new GEdorItemCancelBL();
        tGEdorItemCancelBL.submitData(tVData, "DELETE||EDOR");

    }

}
