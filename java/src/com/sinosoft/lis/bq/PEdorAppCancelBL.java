package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.task.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全删除业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author lh Modify Lanjun 2005-04-25
 * @version 1.0
 */

//功能：查询出个人批改主表中本次申请的批改类型
//入口参数：个单的保单号、批单号
//出口参数：每条记录的个单的保单号、批单号和批改类型
public class PEdorAppCancelBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;
  private MMap mMap = new MMap();

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 撤销申请原因 */
  private String delReason;
  private String reasonCode;

  /** 数据操作字符串 */
  private String mOperate;
  private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();
  private GlobalInput mGlobalInput = new GlobalInput();

 private TransferData tTransferData = new TransferData();
  //统一更新日期，时间
  private String theCurrentDate = PubFun.getCurrentDate();
  private String theCurrentTime = PubFun.getCurrentTime();


  public PEdorAppCancelBL() {
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;


    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if (cOperate.equals("EDORAPP")) {
      if (!checkData()) {
        return false;
      }

      System.out.println("after checkData...");

      if (!prepareData()) {
        return false;
      }
      // 装配处理好的数据，准备给后台进行保存
      this.prepareOutputData();
      System.out.println("---prepareOutputData---");

//      PubSubmit tPubSubmit = new PubSubmit();
//
//      if (!tPubSubmit.submitData(mResult, cOperate)) {
//        // @@错误处理
//        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//
//        return false;
//      }

      //      EdorAppCancelBLS tEdorAppCancelBLS = new EdorAppCancelBLS();
      //          if (!tEdorAppCancelBLS.submitData(mInputData,mOperate))
      //            return false;
      //
      //      System.out.println("----submit---");
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    // 查询条件
    System.out.println("moperator:" + mOperate);
    if (mOperate.equals("EDORAPP")) {
      mLPEdorAppSchema.setSchema( (LPEdorAppSchema) cInputData
                                 .getObjectByObjectName("LPEdorAppSchema", 0));
      tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
      //撤销申请原因
      this.delReason = (String) tTransferData.getValueByName("DelReason");
      this.reasonCode = (String) tTransferData.getValueByName("ReasonCode");

    }
    mGlobalInput = (GlobalInput)cInputData
                   .getObjectByObjectName("GlobalInput", 0);
    if(mGlobalInput == null || mGlobalInput.Operator == null)
    {
        CError tError = new CError();
        tError.moduleName = "PEdorAppCancelBL";
        tError.functionName = "getInputData";
        tError.errorMessage = "请传入操作员信息";
        mErrors.addOneError(tError);
        System.out.println(tError.errorMessage);
        return false;
    }

    return true;
  }

  /**
   *检查数据的合法性
   **/
  private boolean checkData() {
    System.out.println("start check data");

    String strSql = "select * from LPEdorApp where EdorAcceptNo = '"
        + mLPEdorAppSchema.getEdorAcceptNo() + "'";
    System.out.println(strSql);
    LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
    LPEdorAppSet tLPEdorAppSet = tLPEdorAppDB.executeQuery(strSql);

    if ( (tLPEdorAppSet == null) || (tLPEdorAppSet.size() < 1)) {
      CError.buildErr(this, "不存在此保全申请号的记录");
      return false;
    }

    if (mLPEdorAppSchema.getEdorState().equals("0")) {
      CError.buildErr(this, "保全申请已确认，不可进行申请撤销！");
      return false;
    }

    return true;
  }

  /**
   * 准备需要保存的数据
   */
  private boolean prepareData() {
//        int m;
//        int i;
//        LPEdorMainSchema aLPEdorMainSchema = new LPEdorMainSchema();
//        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
//        LPEdorMainSet tLPEdorMainSet = new LPEdorMainSet();
//        tLPEdorMainDB.setSchema(mLPEdorAppSchema);
//
//        //    tLPEdorMainDB.setEdorState("1");
//        tLPEdorMainSet.set(tLPEdorMainDB.query());
//        if (tLPEdorMainDB.mErrors.needDealError() == true)
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tLPEdorMainDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "PEdorAppCancelBL";
//            tError.functionName = "prepareData";
//            tError.errorMessage = "个人批改主表查询失败!";
//            this.mErrors.addOneError(tError);
//            tLPEdorMainSet.clear();
//            return false;
//        }
//        if (tLPEdorMainSet.size() == 0)
//        {
//            // @@错误处理
//            CError tError = new CError();
//            tError.moduleName = "PEdorAppCancelBL";
//            tError.functionName = "prepareData";
//            tError.errorMessage = "未找到相关数据!";
//            this.mErrors.addOneError(tError);
//            tLPEdorMainSet.clear();
//            return false;
//        }
//        m = tLPEdorMainSet.size();
//        for (i = 1; i <= m; i++)
//        {
//            aLPEdorMainSchema = tLPEdorMainSet.get(i);
//            mInputData.clear();
//            mInputData.add(aLPEdorMainSchema);
//
//            EdorAppCancelBL tEdorAppCancelBL = new EdorAppCancelBL();
//
//            if (!tEdorAppCancelBL.submitData(mInputData, mOperate))
//            {
//                return false;
//            }
//        }
    String delSql;
    String tEdorAcceptNo = mLPEdorAppSchema.getEdorAcceptNo();

    //将将数据备份，并存储备份信息
    LPEdorAppSchema cLPEdorAppSchema = new LPEdorAppSchema();
    LOBEdorAppSchema cLOBEdorAppSchema = new LOBEdorAppSchema();
    LPEdorAppDB cLPEdorAppDB = new LPEdorAppDB();
    LPEdorAppSet cLPEdorAppSet = new LPEdorAppSet();

    //选出当前记录
    String sql = "select * from LpedorApp where edorAcceptNo='" + tEdorAcceptNo +
        "'";
    cLPEdorAppSet = cLPEdorAppDB.executeQuery(sql);
    if (cLPEdorAppSet.size() > 0 && cLPEdorAppSet != null) {
      for (int k = 1; k <= cLPEdorAppSet.size(); k++) {
        cLPEdorAppSchema = cLPEdorAppSet.get(k);
        System.out.println("cLPEdorMainSchema:" + cLPEdorAppSchema.getAccName()); //测试用
        Reflections crf = new Reflections();
        crf.transFields(cLOBEdorAppSchema, cLPEdorAppSchema); //将一条记录整体复制
        cLOBEdorAppSchema.setReason(delReason); //添加撤销原因
        cLOBEdorAppSchema.setReasonCode(reasonCode); //添加撤销原因编码
        cLOBEdorAppSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(cLOBEdorAppSchema);
      }
      mMap.put(cLOBEdorAppSchema, "DELETE&INSERT");
    }

    String[] tableForDel = {
        "LPEdorAppPrint", "LPEdorAppPrint3", "LPEdorApp",
        "LPAppDiseaseResult","LPAppPENoticeResult","LPAppPENoticeItem",
        "LPAppPENotice","LPAppRReportItem","LPAppRReport",
        "LPAppRReportResult","LPAppUWSubMain","LPAppUWMasterMain"};

    for (int i = 0; i < tableForDel.length; i++) {
      delSql = "delete from  " + tableForDel[i] + " where EdorAcceptNo = '" +
          tEdorAcceptNo + "'";
      mMap.put(delSql, "DELETE");
    }
    mMap.put("delete from LJSGet where otherno='" + tEdorAcceptNo +
             "' and othernotype='10'", "DELETE");
    mMap.put("delete from LJSPay where otherno='" + tEdorAcceptNo +
             "' and othernotype='10'", "DELETE");
    //更新工单状态
     mMap.put("update LGWork set StatusNo='" + Task.WORKSTATUS_CANCEL + "', "
              + "   operator = '" + mGlobalInput.Operator + "', "
              + "   modifyDate = '" + theCurrentDate + "', "
              + "   modifyTime = '" + theCurrentTime + "' "
              + "where AcceptNo='"+tEdorAcceptNo+"'","UPDATE");

    return true;
  }

  /**
   * 准备数据，重新填充数据容器中的内容
   */
  private void prepareOutputData() {
    //记录当前操作员
    mResult.clear();
    mResult.add(mMap);
  }

  /**
   * 取得sql集
   * @return MMap
   */
  public MMap getMap()
  {
    return mMap;
  }

  public static void main(String[] args) {
    PEdorAppCancelBL tPEdorAppCancelBL = new PEdorAppCancelBL();
    VData tVData = new VData();
    LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
    tLPEdorAppSchema.setEdorAcceptNo("86000000000293");
    tLPEdorAppSchema.setEdorState("1");
    tVData.addElement(tLPEdorAppSchema);
    tPEdorAppCancelBL.submitData(tVData, "DELETE||EDOR");
  }
}
