package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体特需医疗追加保费</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class GrpEdorZBAppConfirmBL implements EdorAppConfirm {
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = null;

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保全生效日期 */
    private String mEdorValiDate = null;

    /** 保全交费总额 */
    private double mGetMoney = 0.00;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mSpecialData = null;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mVtsData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        getInputData(cInputData);

        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult() {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private void getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData.
                                       getObjectByObjectName(
                                               "LPGrpEdorItemSchema", 0);
        mEdorNo = edorItem.getEdorNo();
        mEdorType = edorItem.getEdorType();
        mGrpContNo = edorItem.getGrpContNo();
        mEdorValiDate = edorItem.getEdorValiDate();
        mSpecialData = new EdorItemSpecialData(edorItem);
        mSpecialData.query();
        mVtsData = new EdorItemSpecialData(edorItem);
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData() {
        if (!setGrpAcc()) {
            return false;
        }
        if (!setInsuredAcc()) {
            return false;
        }
        setEdorInfo();
        setGrpEdorItem();
        return true;
    }

    /**
     * 设置团体账户追加保费金额
     */
    private boolean setGrpAcc() {
        String[] grpPolNos = mSpecialData.getGrpPolNos();
        for (int i = 0; i < grpPolNos.length; i++) {
            String grpPolNo = grpPolNos[i];
            //考虑特需和普通险种混合，只处理特需险种。added by huxl 2006-9-21
            String sqlEspecialRiskcode =
                    "select b.risktype3,a.riskcode,b.RiskType8 from lcgrppol a,lmriskapp b where a.grppolno = '"
                    + grpPolNo + "' and a.riskcode = b.riskcode  ";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(sqlEspecialRiskcode);
            if (tSSRS.getMaxRow() != 0 && (tSSRS.GetText(1, 1).equals("7") || tSSRS.GetText(1,3).equals(BQ.RISKTYPE8_TPA))) {
                String polNo = CommonBL.getPubliAccPolNo(grpPolNo);
                //added by huxl 2006-11-21 多个账户险的出现，需要关联险种到账户类型
                LCInsureAccSchema tLCInsureAccSchema =
                        CommonBL.getLCInsureAcc(polNo,
                                                CommonBL.getInsuAccNo(BQ.
                        ACCTYPE_GROUP,tSSRS.GetText(1, 2)));
                if (tLCInsureAccSchema == null) {
                    mErrors.addOneError("找不到团体账户信息！");
                    return false;
                }
                mSpecialData.setGrpPolNo(grpPolNo);
                double getMoney =
                        Double.parseDouble(mSpecialData.getEdorValue(
                                "GroupMoney"));
                System.out.println("getMoney" + getMoney);
                if (getMoney == 0) {
                    continue;
                }
                double manageMoney = CommonBL.carry(getMoney *
                        CommonBL.getManageFeeRate(grpPolNo, BQ.FEECODE_GROUP));
                System.out.println("manageMoney" + manageMoney);
                //管理费收取方式为账户外扣型时不收取
                /*String strSQL="select CalFactorValue from lccontplandutyparam a where grppolno='"+ grpPolNo +"'  " 
                		+" and calfactor='ChargeFeeRateType' and CalFactorValue='04' "
                		+" and contplancode=(select contplancode from lcpol "
                		+" where grppolno=a.grppolno and poltypeflag='2' fetch first 1 rows only)"
                		+" and exists (select 1 from ldcode where codetype='ChargeFeeRateType' and code=a.riskcode) "
                		+" with ur";*/
                String strSQL = " select 1 from ldcode a ,lcgrppol b where a.code=b.riskcode " +
                		" and a.codetype='ChargeFeeRateType' " +
                		" and b.grppolno='"+grpPolNo+"' " +
                		" with ur ";
                String chargeFeeType=tExeSQL.getOneValue(strSQL);
                if(null!=chargeFeeType && !"".equals(chargeFeeType)){
                	manageMoney=0.0;
                	System.out.println("委托管理产品不扣手续费 ：，manageMoney="+manageMoney);
                }
                
                double chgMoney = CommonBL.carry(getMoney - manageMoney);
                double leftMoney = CommonBL.carry(tLCInsureAccSchema.
                                                  getInsuAccBala() + chgMoney);
                double sumMoney = CommonBL.carry(tLCInsureAccSchema.getSumPay() +
                                                 chgMoney);
                LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
                Reflections ref = new Reflections();
                ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
                tLPInsureAccSchema.setEdorNo(mEdorNo);
                tLPInsureAccSchema.setEdorType(mEdorType);
                tLPInsureAccSchema.setInsuAccBala(leftMoney);
                tLPInsureAccSchema.setSumPay(sumMoney);
                tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
                tLPInsureAccSchema.setModifyDate(mCurrentDate);
                tLPInsureAccSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
                LCPolSchema tLCPolSchema = CommonBL.getLCPol(polNo);
                if (tLCPolSchema == null) {
                    mErrors.addOneError("找不到团体公共账户的险种信息！");
                    return false;
                }
                setGetEndorse(chgMoney, tLCPolSchema);
                setGetEndorseMF(manageMoney,tLCPolSchema); //add by fuxin 2008-6-30 管理费
                mGetMoney += getMoney;
                //设置批单显示数据
                mVtsData.setGrpPolNo(grpPolNos[i]);
                mVtsData.add("GrpGetMoney", CommonBL.bigDoubleToCommonString(getMoney,"0.00"));
                mVtsData.add("GrpManageMoney", CommonBL.bigDoubleToCommonString(manageMoney,"0.00"));
                mVtsData.add("GrpLeftMoney", CommonBL.bigDoubleToCommonString(leftMoney,"0.00"));
                setLPInsureAccTrace(tLCInsureAccSchema, chgMoney);
                setLPInsureAccFee(tLCInsureAccSchema, manageMoney);
                setLPInsureAccClassFee(tLCInsureAccSchema, manageMoney,
                                       CommonBL.getManageFeeRate(grpPolNo, BQ.FEECODE_GROUP));
                setLPInsureAccFeeTrace(tLCInsureAccSchema,manageMoney,
                                       CommonBL.getManageFeeRate(grpPolNo, BQ.FEECODE_GROUP),BQ.FEECODE_GROUP);

            }
        }
        return true;
    }
    /**
     * 设置帐户领取轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTrace(LCInsureAccSchema aLCInsureAccSchema, double grpMoney)
    {
        String serialNo;
        LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
        tLPInsureAccTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccTraceDB.setEdorType(mEdorType);
        tLPInsureAccTraceDB.setGrpContNo(mGrpContNo);
        tLPInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
        if (tLPInsureAccTraceSet.size() > 0)
        {
            serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccTrace " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        }
        else
        {
            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        }
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(BQ.FILLDATA);
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(BQ.FEEFINATYPE_BF);
        tLPInsureAccTraceSchema.setMoney(Math.abs(grpMoney));  //退费
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }

    /**
  * 设置管理费
  * @param aLCInsureAccSchema LCInsureAccSchema
  * @param manageMoney double
  */
 private void setLPInsureAccFee(LCInsureAccSchema aLCInsureAccSchema, double manageMoney)
 {
     String[] keys = new String[2];
     DetailDataQuery mQuery = new DetailDataQuery(mEdorNo, mEdorType);
     keys[0] = aLCInsureAccSchema.getPolNo();
     keys[1] = aLCInsureAccSchema.getInsuAccNo();
     LPInsureAccFeeSchema tLPInsureAccFeeSchema = (LPInsureAccFeeSchema)
             mQuery.getDetailData("LCInsureAccFee", keys);

    //若没有管理费，则生成一条
    if(tLPInsureAccFeeSchema.getGrpContNo() == null)
    {
        Reflections ref = new Reflections();
        ref.transFields(tLPInsureAccFeeSchema, aLCInsureAccSchema);
        tLPInsureAccFeeSchema.setFeeRate(0);
        tLPInsureAccFeeSchema.setFee(0);
        tLPInsureAccFeeSchema.setFeeUnit(0);
        tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tLPInsureAccFeeSchema);

        LCInsureAccFeeSchema schemaC = new LCInsureAccFeeSchema();
        ref.transFields(schemaC, tLPInsureAccFeeSchema);
        mMap.put(schemaC, SysConst.INSERT);
    }

     tLPInsureAccFeeSchema.setFee(tLPInsureAccFeeSchema.getFee() + manageMoney);
     tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
     tLPInsureAccFeeSchema.setModifyDate(mCurrentDate);
     tLPInsureAccFeeSchema.setModifyTime(mCurrentTime);
     mMap.put(tLPInsureAccFeeSchema, "DELETE&INSERT");
 }

 /**
  * 设置管理费分类 轨迹
  * @param aLCInsureAccSchema LCInsureAccSchema
  * @param manageMoney double
  * @param feeRate double
  */
 private void setLPInsureAccClassFee(LCInsureAccSchema aLCInsureAccSchema, double manageMoney, double feeRate)
 {
     LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
     tLPInsureAccClassFeeSchema.setEdorNo(mEdorNo);
     tLPInsureAccClassFeeSchema.setEdorType(mEdorType);
     tLPInsureAccClassFeeSchema.setPolNo(aLCInsureAccSchema.getPolNo());
     tLPInsureAccClassFeeSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
     tLPInsureAccClassFeeSchema.setPayPlanCode(BQ.FILLDATA);
     tLPInsureAccClassFeeSchema.setOtherNo(mEdorNo);
     tLPInsureAccClassFeeSchema.setOtherType("3"); //3是保全
     tLPInsureAccClassFeeSchema.setAccAscription("0");
     tLPInsureAccClassFeeSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
     tLPInsureAccClassFeeSchema.setContNo(aLCInsureAccSchema.getContNo());
     tLPInsureAccClassFeeSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
     tLPInsureAccClassFeeSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
     tLPInsureAccClassFeeSchema.setManageCom(aLCInsureAccSchema.getManageCom());
     tLPInsureAccClassFeeSchema.setInsuredNo(aLCInsureAccSchema.getInsuredNo());
     tLPInsureAccClassFeeSchema.setAppntNo(aLCInsureAccSchema.getAppntNo());
     tLPInsureAccClassFeeSchema.setAccType(aLCInsureAccSchema.getAccType());
     tLPInsureAccClassFeeSchema.setAccComputeFlag(aLCInsureAccSchema.getAccComputeFlag());
     tLPInsureAccClassFeeSchema.setAccFoundDate(aLCInsureAccSchema.getAccFoundDate());
     tLPInsureAccClassFeeSchema.setAccFoundTime(aLCInsureAccSchema.getAccFoundTime());
     tLPInsureAccClassFeeSchema.setBalaDate(aLCInsureAccSchema.getBalaDate());
     tLPInsureAccClassFeeSchema.setBalaTime(aLCInsureAccSchema.getBalaTime());
     tLPInsureAccClassFeeSchema.setFeeRate(feeRate);
     tLPInsureAccClassFeeSchema.setFee(manageMoney);
     tLPInsureAccClassFeeSchema.setFeeUnit(0);
     tLPInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
     tLPInsureAccClassFeeSchema.setMakeDate(mCurrentDate);
     tLPInsureAccClassFeeSchema.setMakeTime(mCurrentTime);
     tLPInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
     tLPInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
     mMap.put(tLPInsureAccClassFeeSchema, "DELETE&INSERT");
    }


    /**
     * 设置个人账户追加保费金额
     * @return boolean
     */
    private boolean setInsuredAcc() {
        LCGrpPolSet tLCGrpPolSet = CommonBL.getLCGrpPolSet(mGrpContNo);
        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            String grpPolNo = tLCGrpPolSchema.getGrpPolNo();
            //考虑特需和普通险种混合，只处理特需险种。
            String sqlEspecialRiskcode =
                    "select b.risktype3,b.RiskType8 from lcgrppol a,lmriskapp b where a.grppolno = '"
                    + grpPolNo + "' and a.riskcode = b.riskcode  ";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(sqlEspecialRiskcode);
            if (tSSRS.getMaxRow() != 0 && (tSSRS.GetText(1, 1).equals("7")||tSSRS.GetText(1,2).equals(BQ.RISKTYPE8_TPA))) {
                //String riskSeqNo = tLCGrpPolSchema.getRiskSeqNo();
                LPDiskImportSet tLPDiskImportSet = CommonBL.getLPDiskImportSet
                        (mEdorNo, mEdorType, mGrpContNo, null,
                         BQ.IMPORTSTATE_SUCC);
                double insuredGetMoney = 0.0;
                double insuredManageMoney = 0.0;
                for (int j = 1; j <= tLPDiskImportSet.size(); j++) {
                    LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.
                            get(j);
                    String insuredNo = tLPDiskImportSchema.getInsuredNo();
                    LCInsureAccSchema tLCInsureAccSchema =
                            CommonBL.getLCInsureAcc(grpPolNo, insuredNo,
                            BQ.ACCTYPE_INSURED);
                    if (tLCInsureAccSchema == null) {
                        mErrors.addOneError("找不到客户" + insuredNo + "的个人账户信息！");
                        return false;
                    }
                    double getMoney = tLPDiskImportSchema.getMoney();
                    double manageMoney = CommonBL.carry(getMoney *
                            CommonBL.getManageFeeRate(grpPolNo,
                            BQ.FEECODE_INSURED));
                    //管理费收取方式为账户外扣型
                    /*String StrSQL="select CalFactorValue from lccontplandutyparam a where grppolno='" + grpPolNo + "'"
                    		+" and calfactor='ChargeFeeRateType' and CalFactorValue='04'"
                    		+" and contplancode=(select contplancode from lcpol "
                    		+" where grppolno=a.grppolno and insuredno='" + insuredNo + "' fetch first 1 rows only)"
                    		+" and exists (select 1 from ldcode where codetype='ChargeFeeRateType' and code=a.riskcode)"
                    		+" with ur";*/
                    String StrSQL = " select 1 from ldcode a ,lcgrppol b where a.code=b.riskcode " +
            		" and a.codetype='ChargeFeeRateType' " +
            		" and b.grppolno='"+grpPolNo+"' " +
            		" with ur ";
                    String chargeFeeType=tExeSQL.getOneValue(StrSQL);
                    if(null!=chargeFeeType && !"".equals(chargeFeeType)){
                    	manageMoney=0.0;
                    	System.out.println("委托管理产品不扣手续费 ：，manageMoney="+manageMoney);
                    }
                    
                    double chgMoney = getMoney - manageMoney;
                    double leftMoney = CommonBL.carry(tLCInsureAccSchema.
                            getInsuAccBala() +
                            chgMoney);
                    double sumMoney = CommonBL.carry(tLCInsureAccSchema.
                            getSumPay() + chgMoney);
                    LPInsureAccSchema tLPInsureAccSchema = new
                            LPInsureAccSchema();
                    Reflections ref = new Reflections();
                    ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
                    tLPInsureAccSchema.setEdorNo(mEdorNo);
                    tLPInsureAccSchema.setEdorType(mEdorType);
                    tLPInsureAccSchema.setInsuAccBala(leftMoney);
                    tLPInsureAccSchema.setSumPay(sumMoney);
                    tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
                    tLPInsureAccSchema.setModifyDate(mCurrentDate);
                    tLPInsureAccSchema.setModifyTime(mCurrentTime);
                    mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
                    LCPolSchema tLCPolSchema = CommonBL.getLCPol(grpPolNo,
                            insuredNo);
                    if (tLCPolSchema == null) {
                        mErrors.addOneError("找不到客户" + insuredNo + "的险种信息！");
                        return false;
                    }
                    setGetEndorse(chgMoney, tLCPolSchema);
                    setGetEndorseMF(manageMoney,tLCPolSchema);  //add by fuxin 2008-6-30 管理费
                    mGetMoney += getMoney;
                    insuredGetMoney += getMoney;
                    insuredManageMoney += manageMoney;
                    setDiskImport(tLPDiskImportSchema, leftMoney);
                    setLPInsureAccTrace(tLCInsureAccSchema, chgMoney);
                    setLPInsureAccFee(tLCInsureAccSchema, manageMoney);
                    setLPInsureAccClassFee(tLCInsureAccSchema, manageMoney,
                                           CommonBL.getManageFeeRate(grpPolNo,
                            BQ.FEECODE_INSURED));
                    setLPInsureAccFeeTrace(tLCInsureAccSchema,manageMoney,
                                       CommonBL.getManageFeeRate(grpPolNo, BQ.FEECODE_INSURED), BQ.FEECODE_INSURED);

                }
                //设置批单显示数据
                mVtsData.setGrpPolNo(grpPolNo);
                mVtsData.add("InsuredGetMoney", CommonBL.bigDoubleToCommonString(insuredGetMoney,"0.00"));
                mVtsData.add("InsuredManageMoney",CommonBL.bigDoubleToCommonString(insuredManageMoney,"0.00"));


            }
        }
        return true;
    }

    /**
     * 设置批改补退费表
     * @return boolean
     */
    private void setGetEndorse(double edorPrem, LCPolSchema aLCPolSchema) {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGrpContNo(mGrpContNo);
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
        tLJSGetEndorseSchema.setGetMoney(edorPrem);
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_BF);
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
    }

    /**
     * 设置批改补退费表
     * add by fuxin 2008-6-30 管理费
     * @return boolean
     */
    private void setGetEndorseMF(double edorPrem, LCPolSchema aLCPolSchema) {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGrpContNo(mGrpContNo);
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
        tLJSGetEndorseSchema.setGetMoney(edorPrem);
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_MF);
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
    }


    /**
     * 设置item表中的费用和状态
     */
    private void setGrpEdorItem() {
        String sql = "update LPGrpEdorItem " +
                     "set GetMoney = " + mGetMoney + ", " +
                     "EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                     "UWFlag = '" + BQ.UWFLAG_PASS + "', " +
                     "ModifyDate = '" + mCurrentDate + "', " +
                     "ModifyTime = '" + mCurrentTime + "' " +
                     "where EdorNo = '" + mEdorNo + "' " +
                     "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql.toString(), "UPDATE");
    }

    /**
     * 设置LPDiskImport中显示的信息
     */
    private void setDiskImport(LPDiskImportSchema tLPDiskImportSchema,
                               double leftMoney) {
        tLPDiskImportSchema.setGetMoney(leftMoney);
        tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
        tLPDiskImportSchema.setModifyDate(mCurrentDate);
        tLPDiskImportSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPDiskImportSchema, "DELETE&INSERT");
    }

    /**
     * 保存要在批单中显示的数据
     */
    private void setEdorInfo() {
        mMap.put(mVtsData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     *
     * 管理费轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param grpFee double
     * @param feeRate double
     * @param tFeeCode String
     */
    private void setLPInsureAccFeeTrace(LCInsureAccSchema aLCInsureAccSchema, double grpFee, double feeRate,String tFeeCode){
        String serialNo;
        LPInsureAccFeeTraceDB tLPInsureAccFeeTraceDB = new LPInsureAccFeeTraceDB();
        tLPInsureAccFeeTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccFeeTraceDB.setEdorType(mEdorType);
        tLPInsureAccFeeTraceDB.setGrpContNo(mGrpContNo);
        tLPInsureAccFeeTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccFeeTraceDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        LPInsureAccFeeTraceSet tLPInsureAccFeeTraceSet = tLPInsureAccFeeTraceDB.query();
        if (tLPInsureAccFeeTraceSet.size() > 0){
            serialNo = tLPInsureAccFeeTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccFeeTrace " +
                         "where EdorNo = '" + mEdorNo + "' " +
                         "and EdorType = '" + mEdorType + "' " +
                         "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        } else {
            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        }
        LPInsureAccFeeTraceSchema tLPInsureAccFeeTraceSchema = new LPInsureAccFeeTraceSchema();
        tLPInsureAccFeeTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccFeeTraceSchema.setEdorType(mEdorType);
        tLPInsureAccFeeTraceSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccFeeTraceSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccFeeTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccFeeTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccFeeTraceSchema.setSerialNo(serialNo);
        tLPInsureAccFeeTraceSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccFeeTraceSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
        tLPInsureAccFeeTraceSchema.setPayPlanCode(BQ.FILLDATA);
        tLPInsureAccFeeTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccFeeTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccFeeTraceSchema.setAccAscription("0");
        tLPInsureAccFeeTraceSchema.setMoneyType(BQ.FEEFINATYPE_MF);
        tLPInsureAccFeeTraceSchema.setFeeRate(feeRate);
        tLPInsureAccFeeTraceSchema.setFee(Math.abs(grpFee));  //退费
        tLPInsureAccFeeTraceSchema.setFeeUnit("0");
        tLPInsureAccFeeTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccFeeTraceSchema.setState("0");
        tLPInsureAccFeeTraceSchema.setFeeCode(tFeeCode);
        tLPInsureAccFeeTraceSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccFeeTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccFeeTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccFeeTraceSchema, "DELETE&INSERT");
}


    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
//        GlobalInput gi = new GlobalInput();
//        gi.Operator = "endor0";
//        gi.ComCode = "86";
//
//        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
//        tLPGrpEdorItemDB.setEdorNo("20070213000061");
//
//        VData d = new VData();
//        d.add(gi);
//        d.add(tLPGrpEdorItemDB.query().get(1));
//
//        GrpEdorZBAppConfirmBL bl = new GrpEdorZBAppConfirmBL();
//        if(!bl.submitData(d, ""))
//        {
//            System.out.println(bl.mErrors.getErrContent());
//        }
    	String grpPolNo = "2201169996"; 
    	ExeSQL ES = new ExeSQL();
        String strSQL = " select b.* from ldcode a ,lcgrppol b where a.code=b.riskcode " +
		" and a.codetype='ChargeFeeRateType' " +
		" and b.grppolno='"+grpPolNo+"' " +
		" with ur ";
        String insuredNo= "00156397" ;
        String StrSQL = " select b.* from ldcode a ,lcgrppol b where a.code=b.riskcode " +
		" and a.codetype='ChargeFeeRateType' " +
		" and b.grppolno='"+grpPolNo+"' " +
		" with ur ";
        String chargeFeeType=ES.getOneValue(StrSQL);
        System.out.println(chargeFeeType);
    }
}
