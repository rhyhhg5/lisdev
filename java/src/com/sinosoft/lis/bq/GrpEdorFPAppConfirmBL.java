package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *  服务频次变更空理算类
 * <p>Description: </p>
 *  不做任何操作
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 * @author Huxl
 * @version 1.0
 */
public class GrpEdorFPAppConfirmBL implements EdorAppConfirm {
    private MMap mMap = new MMap();

    public GrpEdorFPAppConfirmBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        getInputData(cInputData);

        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult() {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private void getInputData(VData cInputData) {
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData() {
        return true;
    }
}
