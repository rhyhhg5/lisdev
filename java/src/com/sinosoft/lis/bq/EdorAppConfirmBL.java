package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.lang.reflect.*;
import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 个单保全申请确认</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class EdorAppConfirmBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    ;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */

    MMap map = new MMap();

    private GlobalInput mGlobalInput = new GlobalInput();
    LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
    LPEdorMainSet mLPEdorMainSet = new LPEdorMainSet();
    LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
    // LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

    public EdorAppConfirmBL() {
    }

    /**
     * 设置本次个人保全确认的个人保全主表
     * @param LPEdorMainSchema
     * @return void
     */
    public void setLPEdorMainSchema(LPEdorMainSchema tLPEdorMainSchema) {
        mLPEdorMainSchema.setSchema(tLPEdorMainSchema);
    }

    /**
     * 设置操作的全局变量
     * @param GlobalInput
     * @return void
     */
    public void setGlobalInput(GlobalInput tGlobalInput) {
        mGlobalInput.setSchema(tGlobalInput);
    }

    /**
     * 设置操作的类型
     * @String cOperate
     * @return void
     */
    public void setOperate(String cOperate) {
        mOperate = cOperate;
    }


    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }
        System.out.println("---End getInputData---");

        //数据准备操作,查询个人保全主表中，该保单处于申请状态的保全数据
        if (!prepareData()) {
            return false;
        }
        System.out.println("---End prepareData---");
        if (!dealData()) {
            return false;
        }
        // 数据操作业务处理
        //　数据提交、保存
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("---End prepareData---");

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start tPRnewManualDunBLS Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData
     * @param cOperate
     * @return
     */
    public MMap getEdorAppConfirmData() {
        //数据准备操作,查询个人保全主表中，该保单处于申请状态的保全数据
        if (!prepareData()) {
            return null;
        }
        System.out.println("---End prepareData---");
        if (!dealData()) {
            return null;
        }
        // 数据操作业务处理
        //　数据提交、保存
        if (!prepareOutputData()) {
            return null;
        }
        System.out.println("---End prepareData---");

        return map;
    }


    private boolean prepareOutputData() {

        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
        mResult.add(map);

        return true;
    }


    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public boolean dealData() {
        if (mOperate.equals("INSERT||EDORAPPCONFIRM") ||
            mOperate.equals("INSERT||GEDORAPPCONFIRM")) {
            VData pInputData = new VData();
            String edortype="";
            for (int i = 1; i <= mLPEdorItemSet.size(); i++) {
                LPEdorItemSchema mLPEdorItemSchema = mLPEdorItemSet.get(i);
                //校验个单保全申请的明细信息是否完整 暂时注掉 by pq
//                if (!checkData(mLPEdorItemSchema))return false;
//                System.out.println("End 校验该个单保全申请的明细信息是否完整");

                //里利用类的映射，调用用每个项目对应的理算类。
                edortype = mLPEdorItemSchema.getEdorType();
                try {
                    Class tClass = Class.forName("com.sinosoft.lis.bq.PEdor" +
                                                 edortype +
                                                 "AppConfirmBL");
                    EdorAppConfirm tPEdorAppConfirm = (EdorAppConfirm)
                            tClass.
                            newInstance();
                    VData tVData = new VData();

                    tVData.add(mLPEdorItemSchema);
                    //tVData.add(mLJSGetEndorseSchema);
                    tVData.add(mGlobalInput);

                    if (!tPEdorAppConfirm.submitData(tVData,
                            "APPCONFIRM||" + edortype)) {
                        System.out.println("理算错误,请查看com.sinosoft.lis.bq.PEdor" +
                                           edortype +
                                           "AppConfirmBL");
                        mErrors.copyAllErrors(tPEdorAppConfirm.mErrors);
                        return false;
                    } else {
                        VData rVData = tPEdorAppConfirm.getResult();
                        MMap tMap = new MMap();
                        tMap = (MMap) rVData.getObjectByObjectName("MMap", 0);
                        if (tMap == null) {
                            CError.buildErr(this,
                                            "得到保单号为：" +
                                            mLPEdorItemSchema.getPolNo()
                                            + "保全项目为:" + edortype +
                                            "的保全理算结果时失败！");
                            return false;

                        } else {
                            map.add(tMap);
                        }
                    }

                } catch (ClassNotFoundException ex) {
                    //添加默认处理方法 PQ
                    map.put(mLPEdorItemSchema, "UPDATE");
                } catch (Exception ex) {
                    ex.printStackTrace();
                    CError.buildErr(this, "调用保全项目" + edortype + "的理算方法时失败！");
                    return false;
                }
                mLPEdorItemSchema.setEdorState("2");
                mLPEdorItemSchema.setUWFlag("0");
            }

            //将LPEdorItem 表中的费用累加到LpEdorMain 然后会在PEdorAcceptAppConfirm.java中将费用累加到LPEdorApp中。
            String tEdorNo = mLPEdorMainSchema.getEdorNo();
            String tContNo = mLPEdorMainSchema.getContNo();
            //下面要增加处理个人批单主表的逻辑
            mLPEdorMainSchema.setEdorState("2");
            mLPEdorMainSchema.setConfOperator(mGlobalInput.Operator);
            mLPEdorMainSchema.setConfDate(PubFun.getCurrentDate());
            mLPEdorMainSchema.setConfTime(PubFun.getCurrentTime());
            mLPEdorMainSchema.setOperator(mGlobalInput.Operator);
            mLPEdorMainSchema.setModifyDate(PubFun.getCurrentDate());
            mLPEdorMainSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLPEdorMainSchema, "UPDATE");
            String wherePart = "where EdorNo='" +
                               tEdorNo + "' and ContNo='" + tContNo + "'";
            map.put(
                    "update LPEdorMain set ChgPrem= (select sum(ChgPrem) from LPEdorItem "
                    + wherePart + ") " + wherePart, "UPDATE");
            map.put(
                    "update LPEdorMain set ChgAmnt= (select sum(ChgAmnt) from LPEdorItem "
                    + wherePart + ") " + wherePart, "UPDATE");
            /*
                         map.put(
                    "update LPEdorMain set GetMoney= (select round(sum(GetMoney), 0) from LPEdorItem "
                    + wherePart + ") " + wherePart, "UPDATE");
             */
            //modified by luomin   --2005-8-3
            map.put(
                    "update LPEdorMain set GetMoney= (select sum(GetMoney) from LPEdorItem "
                    + wherePart + ") " + wherePart, "UPDATE");

            if(edortype.equals("TQ")){
            	map.put(
                        "update LPEdorMain set edorvalidate= (select edorvalidate from LPEdorItem "
                        + wherePart + ") " + wherePart, "UPDATE");
            }
            map.put(
                    "update LPEdorMain set GetInterest= (select sum(GetInterest) from LPEdorItem "
                    + wherePart + ") " + wherePart, "UPDATE");

        }
        return true;

    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData() {
        try {
            //得到保单号（避免依靠前台）
            mLPEdorMainSchema = (LPEdorMainSchema) mInputData.
                                getObjectByObjectName("LPEdorMainSchema", 0);
            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PGrpEdorAppConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 校验个单保全申请的明细信息是否完整
     * @return
     */
    private boolean checkData(LPEdorItemSchema tLPEdorItemSchema) {
        VerifyEndorsement tVer = new VerifyEndorsement();
        VData tVData = new VData();
        tVData.addElement(tLPEdorItemSchema);

        if (!tVer.verifyEdorDetail(tVData, "VERIFY||DETAIL")) {
            this.mErrors.copyAllErrors(tVer.mErrors);
            return false;
        } else {
            if (tVer.getResult() != null && tVer.getResult().size() > 0) {
                String tErr = "";
                for (int i = 0; i < tVer.getResult().size(); i++) {
                    tErr = tErr + (String) tVer.getResult().get(i);
                }

                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "VerifyEndorsement";
                tError.functionName = "verifyEdorDetail";
                tError.errorMessage = tErr;
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        return true;
    }

    /**
     * 准备需要保存的数据,查询个人保全主表中，该保单处于申请状态的保全数据
     * @return
     */
    private boolean prepareData() {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        //查询个人保全主表中，该保单处于申请状态的保全数据
        String strSql = "select * from LPEdorItem "
                        + " where EdorState='1' and EdorNo='" +
                        mLPEdorMainSchema.getEdorNo() + "' and contno='" +
                        mLPEdorMainSchema.getContNo() +
                        "' order by Makedate,MakeTime"; //去掉按EdorValiDate排序
        System.out.println("strSql:" + strSql);
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setSchema(mLPEdorMainSchema);
        tLPEdorMainDB.getInfo();
        if (tLPEdorMainDB.mErrors.needDealError()) {
            CError.buildErr(this, "查询个人批改主表失败！");
            return false;
        }
        mLPEdorItemSet.clear();
        mLPEdorItemSet = tLPEdorItemDB.executeQuery(strSql);
        System.out.println("个单保全主表记录数:" + mLPEdorItemSet.size());

        return true;
    }

    public static void main(String[] args) {
        VData tInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        EdorAppConfirmBL aEdorAppConfirmBL = new EdorAppConfirmBL();
        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tGlobalInput.ManageCom = "86110000";
        tGlobalInput.Operator = "endor";
        tLPEdorMainSchema.setEdorNo("20050621000053");
        tLPEdorMainSchema.setContNo("00000903201");
//    tLPEdorMainSchema.setPolNo("86110020030210001798");

        tInputData.addElement(tLPEdorMainSchema);
        tInputData.addElement(tGlobalInput);

        aEdorAppConfirmBL.submitData(tInputData, "INSERT||EDORAPPCONFIRM");
    }

}
