package com.sinosoft.lis.bq;

//程序名称：OmDayBalaNewDownloadBL.java
//程序功能：万能可转投资净额日结报表（新）
//创建日期：2010-10-25
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

import java.util.ArrayList;

import com.sinosoft.lis.db.LCContactDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class OmDayBalaNewDownloadBL {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();

  private VData mResult = new VData();
  
  private CreateExcelList mCreateExcelList = new CreateExcelList("");
  
  private TransferData mTransferData = new TransferData();
  
  private GlobalInput mGlobalInput =new GlobalInput();
  
  //对比起始日期
  private String mStartDate="";
  private String mEndDate="";
  private String mManageCom="";
  private String mRiskCode=""; 
  
  ExeSQL mExeSQL = new ExeSQL();
  
  //查询出的问题保单
  private String[][] mExcelData=null;
  
  public OmDayBalaNewDownloadBL() {
  }

/**
  传输数据的公共方法
*/
    public CreateExcelList getsubmitData(VData cInputData, String cOperate)
    {
    	if( !getInputData(cInputData) ) 
        {
    		return null;
        }

        if( !getPrintData() ) 
        {
        	return null;
      	}

        if(mCreateExcelList==null)
        {
        	buildError("submitData", "Excel数据为空");
            return null;
        }
        return mCreateExcelList;
    }
  
  public static void main(String[] args)
  {
      
	  OmDayBalaNewDownloadBL tbl =new OmDayBalaNewDownloadBL();
      GlobalInput tGlobalInput = new GlobalInput();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("StartDate", "2010-10-01");
      tTransferData.setNameAndValue("EndDate", "2010-11-01");
      tTransferData.setNameAndValue("ManageCom", "8611");
      tTransferData.setNameAndValue("RiskCode", "331601");
      tGlobalInput.ManageCom="86";
      tGlobalInput.Operator="zgm";
      VData tData = new VData();
      tData.add(tGlobalInput);
      tData.add(tTransferData);

      CreateExcelList tCreateExcelList=new CreateExcelList();
      tCreateExcelList=tbl.getsubmitData(tData,"");
      if(tCreateExcelList==null)
      {
    	  System.out.println("tCreateExcelList==null");
      }
      else
      {
	      try
	      {
	    	  tCreateExcelList.write("c:\\contactcompare.xls");
	      }
	      catch(Exception e)
	      {
	    	  System.out.println("EXCEL生成失败！");
	      }
      }
  }


  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
	  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
      mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
      if( mGlobalInput==null || mTransferData==null )
      {
          CError tError = new CError();
          tError.moduleName = "OmDayBalaNewDownloadBL.java";
          tError.functionName = "submitData";
          tError.errorMessage = "获取数据失败！";
          mErrors.addOneError(tError);
          return false;
      }
      mManageCom = (String)mTransferData.getValueByName("ManageCom");
      mRiskCode = (String)mTransferData.getValueByName("RiskCode");
      mStartDate = (String)mTransferData.getValueByName("StartDate");
      mEndDate = (String)mTransferData.getValueByName("EndDate"); 
      
      return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "LCGrpContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean getPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"分公司代码", "分公司", "支公司代码", "支公司名称", " 险种", "首期保费", "首期管理费", "首期追加保费", 
    	  "首期追加保费扣费", "续期保费", "续期管理费", "犹豫期退保及CM扣费", "保全追加保费和CM收费", "保全追加保费扣费", "解约金额", "解约金管理费", 
    	  "协议退保", "部分领取", "部分领取手续费", "月度收益", "保单管理费", "风险管理费"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) 
      {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
      String riskPart = null;
      String aRiskPart = null;
      String bRiskPart = null;
      if(mRiskCode.equals("") || mRiskCode.equals("null") || mRiskCode==null)
      {
    	  riskPart = "";
    	  aRiskPart = "";
    	  bRiskPart = "";
      }
      else
      {
    	  riskPart = " AND RiskCode = '" + mRiskCode + "' ";
    	  aRiskPart = " AND a.RiskCode = '" + mRiskCode + "' ";
    	  bRiskPart = " AND b.RiskCode = '" + mRiskCode + "' ";
      }
      
      //获得EXCEL列信息
      StringBuffer sql = new StringBuffer();
      sql.append("SELECT SUBSTR(ComCode,1,4),(SELECT Name FROM LDCom WHERE ComCode= SUBSTR(a.ComCode,1,4)),ComCode ,Name ,b.RiskCode, ")
		  .append("5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21 ")
		  .append("FROM LDCom a , LMRiskApp b " )
		  .append("WHERE a.Sign = '1' AND a.ComGrade = '03' AND b.RiskType4 = '4' AND a.ComCode LIKE '"+mManageCom+"%' " + riskPart) 
		  .append("ORDER BY a.ComCode, b.RiskCode WITH UR");
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null)
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到机构险种数据";
    	  return false;
      }
      
//    首期保费 首期管理费 首期追加保费 首期追加保费扣费 LCInsureAccTrace
      sql = new StringBuffer(); 
      sql.append("SELECT b.ManageCom,b.RiskCode, ")
	  .append("NVL(SUM(CASE WHEN MoneyType IN ('BF','GL') THEN b.Money ELSE 0 END),0) 首期保费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'GL' THEN -b.Money ELSE 0 END),0) 首期管理费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'ZF' THEN b.Money ELSE 0 END),0) 首期追加保费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'KF' THEN b.Money ELSE 0 END),0) 首期追加保费扣费 ")
	  .append("FROM  ")
	  .append("(SELECT DISTINCT ContNo FROM( ")
	  .append("SELECT ContNo,SUM(CASE FinItemType WHEN 'C' THEN SumMoney ELSE -SumMoney END) Money FROM LIDataTransResult a  ")
	  .append("WHERE AccountDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "'  ")
	  .append("AND AccountCode='6031000000' ")
	  .append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
	  .append("AND ClassType='N-03' ")
	  .append("and exists (select 1 from licodetrans where codetype = 'ManageCom' and codealias = a.managecom and code like '"+mManageCom+ "%' ) ")
	  .append(riskPart)
	  .append("GROUP BY ContNo ")
	  .append(") as aa ")
	  .append("WHERE Money<>0) as a LEFT JOIN ")
	  .append("LCInsureAccTrace b on a.ContNo = b.ContNo WHERE OtherType = '1' ")
	  .append("AND MoneyType IN ('ZF','BF','KF')  ")
	  .append("AND b.ManageCom LIKE '"+mManageCom+"%' " + riskPart )
	  .append("GROUP BY b.ManageCom,b.RiskCode WITH UR");
      
      SSRS tbcSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要首期保费的数据LBInsureAccTrace";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  mExcelData[i][5] = "0.00";
    	  mExcelData[i][6] = "0.00";
		  mExcelData[i][7] = "0.00";
		  mExcelData[i][8] = "0.00";
    	  for(int j=1; j<=tbcSSRS.getMaxRow(); j++)
    	  {
    		  if(tbcSSRS.GetText(j, 1).equals(mExcelData[i][2]) && tbcSSRS.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][5]=PubFun.setPrecision2(tbcSSRS.GetText(j, 3));
    			  mExcelData[i][6]=PubFun.setPrecision2(tbcSSRS.GetText(j, 4));
    			  mExcelData[i][7]=PubFun.setPrecision2(tbcSSRS.GetText(j, 5));
    			  mExcelData[i][8]=PubFun.setPrecision2(tbcSSRS.GetText(j, 6));
    		  }
    	  }
      }
     
      
//    首期保费 首期管理费 首期追加保费 首期追加保费扣费 LBInsureAccTrace
    sql = new StringBuffer(); 
    sql.append("SELECT b.ManageCom,b.RiskCode, ")
	  .append("NVL(SUM(CASE WHEN MoneyType IN ('BF','GL') THEN b.Money ELSE 0 END),0) 首期保费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'GL' THEN -b.Money ELSE 0 END),0) 首期管理费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'ZF' THEN b.Money ELSE 0 END),0) 首期追加保费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'KF' THEN b.Money ELSE 0 END),0) 首期追加保费扣费 ")
	  .append("FROM  ")
	  .append("(SELECT DISTINCT ContNo FROM( ")
	  .append("SELECT ContNo,SUM(CASE FinItemType WHEN 'C' THEN SumMoney ELSE -SumMoney END) Money FROM LIDataTransResult a  ")
	  .append("WHERE AccountDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "'  ")
	  .append("AND AccountCode='6031000000' ")
	  .append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
	  .append("AND ClassType='N-03' ")
	  .append("and exists (select 1 from licodetrans where codetype = 'ManageCom' and codealias = a.managecom and code like '"+mManageCom+ "%' ) ")
	  .append(riskPart)
	  .append("GROUP BY ContNo ")
	  .append(") as aa ")
	  .append("WHERE Money<>0) as a LEFT JOIN ")
	  .append("LBInsureAccTrace b on a.ContNo = b.ContNo WHERE OtherType = '1' ")
	  .append("AND MoneyType IN ('ZF','BF','KF')  ")
	  .append("AND b.ManageCom LIKE '"+mManageCom+"%' " + riskPart )
	  .append("GROUP BY b.ManageCom,b.RiskCode WITH UR");
    
    SSRS tbbSSRS = tExeSQL.execSQL(sql.toString());
    if (tExeSQL.mErrors.needDealError()) 
    {
  	  CError tError = new CError();
  	  tError.moduleName = "OmDayBalaNewDownloadBL";
  	  tError.functionName = "getPrintData";
  	  tError.errorMessage = "没有查询到需要首期保费的数据LBInsureAccTrace";
  	  mErrors.addOneError(tError);
  	  return false;
    }
    
    for(int i=0; i<mExcelData.length; i++)
    {
  	  for(int j=1; j<=tbbSSRS.getMaxRow(); j++)
  	  {
  		  if(tbbSSRS.GetText(j, 1).equals(mExcelData[i][2]) && tbbSSRS.GetText(j, 2).equals(mExcelData[i][4]))
  		  {
  			  mExcelData[i][5]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][5]) + Double.parseDouble(tbbSSRS.GetText(j, 3)));
  			  mExcelData[i][6]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][6]) + Double.parseDouble(tbbSSRS.GetText(j, 4)));
  			  mExcelData[i][7]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][7]) + Double.parseDouble(tbbSSRS.GetText(j, 5)));
  			  mExcelData[i][8]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][8]) + Double.parseDouble(tbbSSRS.GetText(j, 6)));
  		  }
  	  }
    }
    
//  首期保费 首期管理费 首期追加保费 首期追加保费扣费 LCInsureAccFeeTrace
    sql = new StringBuffer(); 
    sql.append("SELECT b.ManageCom,b.RiskCode, ")
	  .append("NVL(SUM(CASE WHEN MoneyType IN ('BF','GL') THEN b.fee ELSE 0 END),0) 首期保费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'GL' THEN -b.fee ELSE 0 END),0) 首期管理费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'ZF' THEN b.fee ELSE 0 END),0) 首期追加保费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'KF' THEN b.fee ELSE 0 END),0) 首期追加保费扣费 ")
	  .append("FROM  ")
	  .append("(SELECT DISTINCT ContNo FROM( ")
	  .append("SELECT ContNo,SUM(CASE FinItemType WHEN 'C' THEN SumMoney ELSE -SumMoney END) Money FROM LIDataTransResult a  ")
	  .append("WHERE AccountDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "'  ")
	  .append("AND AccountCode='6031000000' ")
	  .append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
	  .append("AND ClassType='N-03' ")
	  .append("and exists (select 1 from licodetrans where codetype = 'ManageCom' and codealias = a.managecom and code like '"+mManageCom+ "%' ) ")
	  .append(riskPart)
	  .append("GROUP BY ContNo ")
	  .append(") as aa ")
	  .append("WHERE Money<>0) as a LEFT JOIN ")
	  .append("LCInsureAccFeeTrace b on a.ContNo = b.ContNo WHERE OtherType = '1' ")
	  .append("AND MoneyType = 'GL'  ")
	  .append("AND b.ManageCom LIKE '"+mManageCom+"%' " + riskPart )
	  .append("GROUP BY b.ManageCom,b.RiskCode WITH UR");
    
    SSRS tbcSSRS1 = tExeSQL.execSQL(sql.toString());
    if (tExeSQL.mErrors.needDealError()) 
    {
  	  CError tError = new CError();
  	  tError.moduleName = "OmDayBalaNewDownloadBL";
  	  tError.functionName = "getPrintData";
  	  tError.errorMessage = "没有查询到需要首期保费的数据LCInsureAccFeeTrace";
  	  mErrors.addOneError(tError);
  	  return false;
    }
    
    for(int i=0; i<mExcelData.length; i++)
    {
  	  for(int j=1; j<=tbcSSRS1.getMaxRow(); j++)
  	  {
  		  if(tbcSSRS1.GetText(j, 1).equals(mExcelData[i][2]) && tbcSSRS1.GetText(j, 2).equals(mExcelData[i][4]))
  		  {
  			  mExcelData[i][5]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][5]) + Double.parseDouble(tbcSSRS1.GetText(j, 3)));
			  mExcelData[i][6]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][6]) + Double.parseDouble(tbcSSRS1.GetText(j, 4)));
			  mExcelData[i][7]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][7]) + Double.parseDouble(tbcSSRS1.GetText(j, 5)));
			  mExcelData[i][8]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][8]) + Double.parseDouble(tbcSSRS1.GetText(j, 6)));
  		  }
  	  }
    }
    
//  首期保费 首期管理费 首期追加保费 首期追加保费扣费 LBInsureAccFeeTrace
    sql = new StringBuffer(); 
    sql.append("SELECT b.ManageCom,b.RiskCode, ")
	  .append("NVL(SUM(CASE WHEN MoneyType IN ('BF','GL') THEN b.fee ELSE 0 END),0) 首期保费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'GL' THEN -b.fee ELSE 0 END),0) 首期管理费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'ZF' THEN b.fee ELSE 0 END),0) 首期追加保费, ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'KF' THEN b.fee ELSE 0 END),0) 首期追加保费扣费 ")
	  .append("FROM  ")
	  .append("(SELECT DISTINCT ContNo FROM( ")
	  .append("SELECT ContNo,SUM(CASE FinItemType WHEN 'C' THEN SumMoney ELSE -SumMoney END) Money FROM LIDataTransResult a  ")
	  .append("WHERE AccountDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "'  ")
	  .append("AND AccountCode='6031000000' ")
	  .append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
	  .append("AND ClassType='N-03' ")
	  .append("and exists (select 1 from licodetrans where codetype = 'ManageCom' and codealias = a.managecom and code like '"+mManageCom+ "%' ) ")
	  .append(riskPart)
	  .append("GROUP BY ContNo ")
	  .append(") as aa ")
	  .append("WHERE Money<>0) as a LEFT JOIN ")
	  .append("LBInsureAccFeeTrace b on a.ContNo = b.ContNo WHERE OtherType = '1' ")
	  .append("AND MoneyType = 'GL'  ")
	  .append("AND b.ManageCom LIKE '"+mManageCom+"%' " + riskPart )
	  .append("GROUP BY b.ManageCom,b.RiskCode WITH UR");
    
    SSRS tbbSSRS1 = tExeSQL.execSQL(sql.toString());
    if (tExeSQL.mErrors.needDealError()) 
    {
  	  CError tError = new CError();
  	  tError.moduleName = "OmDayBalaNewDownloadBL";
  	  tError.functionName = "getPrintData";
  	  tError.errorMessage = "没有查询到需要首期保费的数据LBInsureAccFeeTrace";
  	  mErrors.addOneError(tError);
  	  return false;
    }
    
    for(int i=0; i<mExcelData.length; i++)
    {
  	  for(int j=1; j<=tbbSSRS1.getMaxRow(); j++)
  	  {
  		  if(tbbSSRS1.GetText(j, 1).equals(mExcelData[i][2]) && tbbSSRS1.GetText(j, 2).equals(mExcelData[i][4]))
  		  {
  			  mExcelData[i][5]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][5]) + Double.parseDouble(tbbSSRS1.GetText(j, 3)));
			  mExcelData[i][6]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][6]) + Double.parseDouble(tbbSSRS1.GetText(j, 4)));
			  mExcelData[i][7]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][7]) + Double.parseDouble(tbbSSRS1.GetText(j, 5)));
			  mExcelData[i][8]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][8]) + Double.parseDouble(tbbSSRS1.GetText(j, 6)));
  		  }
  	  }
    }

    
    // 续期保费 续期管理费 LCInsureAccTrace
      sql = new StringBuffer(); 
      sql.append("SELECT b.ManageCom,b.RiskCode,  ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'BF' THEN b.Money ELSE 0 END),0) 续期保费,  ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'GL' THEN b.Money ELSE 0 END),0) 续期管理费  ")
	  .append("FROM  ")
	  .append("(SELECT DISTINCT ContNo FROM( ")
	  .append("SELECT ContNo,SUM(CASE FinItemType WHEN 'C' THEN SumMoney ELSE -SumMoney END) Money FROM LIDataTransResult a  ")
	  .append("WHERE AccountDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "'  ")
	  .append("AND AccountCode='6031000000' ")
	  .append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
	  .append("AND ClassType LIKE 'X%' ")
	  .append("and exists (select 1 from licodetrans where codetype = 'ManageCom' and codealias = a.managecom and code like '"+mManageCom+ "%' ) ")
	  .append(riskPart)
	  .append("GROUP BY ContNo ")
	  .append(") as aa ")
	  .append("WHERE Money<>0 ) as a LEFT JOIN ")
	  .append("LCInsureAccTrace b on a.ContNo = b.ContNo WHERE OtherType = '2'  ")
	  .append("AND MoneyType IN ('BF','GL') AND OtherType ='2'  ")
	  .append("AND MakeDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "'  ")
	  .append("AND b.ManageCom LIKE '"+mManageCom+"%' " + riskPart )
	  .append("GROUP BY b.ManageCom,b.RiskCode WITH UR");
      
      SSRS xqcSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要续期保费的数据LCInsureAccTrace";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  mExcelData[i][9] = "0.00";
		  mExcelData[i][10] = "0.00";
    	  for(int j=1; j<=xqcSSRS.getMaxRow(); j++)
    	  {
    		  if(xqcSSRS.GetText(j, 1).equals(mExcelData[i][2]) && xqcSSRS.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][9]=PubFun.setPrecision2(xqcSSRS.GetText(j, 3));
    			  mExcelData[i][10]=PubFun.setPrecision2(xqcSSRS.GetText(j, 4));
    		  }
    	  }
      }
      
      
      
//    续期保费 续期管理费 LBInsureAccTrace
      sql = new StringBuffer(); 
      sql.append("SELECT b.ManageCom,b.RiskCode,  ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'BF' THEN b.Money ELSE 0 END),0) 续期保费,  ")
	  .append("NVL(SUM(CASE WHEN MoneyType = 'GL' THEN b.Money ELSE 0 END),0) 续期管理费  ")
	  .append("FROM  ")
	  .append("(SELECT DISTINCT ContNo FROM( ")
	  .append("SELECT ContNo,SUM(CASE FinItemType WHEN 'C' THEN SumMoney ELSE -SumMoney END) Money FROM LIDataTransResult a  ")
	  .append("WHERE AccountDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "'  ")
	  .append("AND AccountCode='6031000000' ")
	  .append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
	  .append("AND ClassType LIKE 'X%' ")
	  .append("and exists (select 1 from licodetrans where codetype = 'ManageCom' and codealias = a.managecom and code like '"+mManageCom+ "%' ) ")
	  .append(riskPart)
	  .append("GROUP BY ContNo ")
	  .append(") as aa ")
	  .append("WHERE Money<>0 ) as a LEFT JOIN ")
	  .append("LBInsureAccTrace b on a.ContNo = b.ContNo WHERE OtherType = '2' ")
	  .append("AND MoneyType IN ('BF','GL') AND OtherType ='2'  ")
	  .append("AND MakeDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "' ")
	  .append("AND b.ManageCom LIKE '"+mManageCom+"%' " + riskPart )
	  .append("GROUP BY b.ManageCom,b.RiskCode WITH UR");
      
      SSRS xqbSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要续期保费的数据LBInsureAccTrace";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  for(int j=1; j<=xqbSSRS.getMaxRow(); j++)
    	  {
    		  if(xqbSSRS.GetText(j, 1).equals(mExcelData[i][2]) && xqbSSRS.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][9] = PubFun.setPrecision2(Double.parseDouble(mExcelData[i][9]) + Double.parseDouble(xqbSSRS.GetText(j, 3)));
    			  mExcelData[i][10]= PubFun.setPrecision2(Double.parseDouble(mExcelData[i][10]) + Double.parseDouble(xqbSSRS.GetText(j, 4)));
    		  }
    	  }
      }
      
      
//    犹豫期退保和CM付费 lccont
      sql = new StringBuffer(); 
      sql.append("SELECT  ")
	  .append("b.ManageCom, RiskCode, ")
	  .append("SUM(CASE FinItemType WHEN 'C' THEN SumMoney ELSE -SumMoney END) Money  ")
	  .append("FROM LIDataTransResult a  LEFT JOIN ")
	  .append("LCCont as b ")
	  .append(" on a.ContNo = b.ContNo ")
	  .append("WHERE AccountDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "' ")
	  .append("AND AccountCode='6031000000' ")
	  .append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
	  .append("AND ClassType='B-02' ")
	  .append("AND FinItemType='D' ")
	  .append("AND SumMoney <> 0  ")
	  .append("and exists (select 1 from licodetrans where codetype = 'ManageCom' and codealias = a.managecom and code like '"+mManageCom+ "%' ) ")
	  .append("AND b.ManageCom LIKE '"+mManageCom+"%' " + riskPart )
	  .append("GROUP BY b.ManageCom, RiskCode WITH UR ");
      
      SSRS wtcSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要保全退费的数据lccont";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  mExcelData[i][11] = "0.00";
    	  for(int j=1; j<=wtcSSRS.getMaxRow(); j++)
    	  {
    		  if(wtcSSRS.GetText(j, 1).equals(mExcelData[i][2]) && wtcSSRS.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][11]=PubFun.setPrecision2(wtcSSRS.GetText(j, 3));
    		  }
    	  }
      }
      
      
//    犹豫期退保和CM付费 lbcont
      sql = new StringBuffer(); 
      sql.append("SELECT  ")
	  .append("b.ManageCom, RiskCode, ")
	  .append("SUM(CASE FinItemType WHEN 'C' THEN SumMoney ELSE -SumMoney END) Money  ")
	  .append("FROM LIDataTransResult a  LEFT JOIN ")
	  .append("LBCont as b ")
	  .append(" on a.ContNo = b.ContNo ")
	  .append("WHERE AccountDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "' ")
	  .append("AND AccountCode='6031000000' ")
	  .append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
	  .append("AND ClassType='B-02' ")
	  .append("AND FinItemType='D' ")
	  .append("AND SumMoney <> 0  ")
	  .append("and exists (select 1 from licodetrans where codetype = 'ManageCom' and codealias = a.managecom and code like '"+mManageCom+ "%' ) ")
	  .append("AND b.ManageCom LIKE '"+mManageCom+"%' " + riskPart )
	  .append("GROUP BY b.ManageCom, RiskCode WITH UR ");
      
      SSRS wtbSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要保全退费的数据lbcont";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  for(int j=1; j<=wtbSSRS.getMaxRow(); j++)
    	  {
    		  if(wtbSSRS.GetText(j, 1).equals(mExcelData[i][2]) && wtbSSRS.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][11]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][11]) + Double.parseDouble(wtbSSRS.GetText(j, 3)));
    		  }
    	  }
      }
      
//    保全追加保费 保全追加保费扣费 LCInsureAccTrace
      sql = new StringBuffer(); 
      sql.append("SELECT b.ManageCom,b.RiskCode, ")
	  .append( "NVL(SUM(CASE WHEN b.MoneyType IN ('ZF','CM') THEN b.Money ELSE 0 END),0) 保全追加保费,  ")
	  .append( "NVL(SUM(CASE WHEN b.MoneyType = 'KF' THEN b.Money ELSE 0 END),0) 保全追加保费扣费 ")
	  .append( "FROM LIDataTransResult a  LEFT JOIN LCInsureAccTrace b on a.ContNo = b.ContNo and substr(keyunionvalue,13,14) = b.otherno ")
	  .append( bRiskPart )
	  .append( "AND b.OtherType = '10' ")
	  .append( "And b.ManageCom LIKE '"+mManageCom+"%' "  )
	  .append( "WHERE AccountDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "' ")
	  .append( "AND AccountCode='6031000000' ")
	  .append( "AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
	  .append( "AND ClassType='B-02' ")
	  .append( "AND FinItemType='C' ")
	  .append( "and exists (select 1 from licodetrans where codetype = 'ManageCom' and codealias = a.managecom and code like '"+mManageCom+ "%' ) ")
	  .append( aRiskPart )
	  .append( "AND SumMoney <> 0  ")
	  .append( "AND MoneyType IN ('ZF','CM','KF') ")
	  .append( "GROUP BY b.ManageCom,b.RiskCode WITH UR ") ;
      
      SSRS zbcSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到保全保费的数据LCInsureAccTrace";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  mExcelData[i][12] = "0.00";
    	  mExcelData[i][13] = "0.00";
    	  for(int j=1; j<=zbcSSRS.getMaxRow(); j++)
    	  {
    		  if(zbcSSRS.GetText(j, 1).equals(mExcelData[i][2]) && zbcSSRS.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][12]=PubFun.setPrecision2(zbcSSRS.GetText(j, 3));
    			  mExcelData[i][13]=PubFun.setPrecision2(zbcSSRS.GetText(j, 4));
    		  }
    	  }
      }
      
//    保全追加保费 保全追加保费扣费 LBInsureAccTrace
      sql = new StringBuffer(); 
      sql.append("SELECT b.ManageCom,b.RiskCode, ")
	  .append( "NVL(SUM(CASE WHEN b.MoneyType IN ('ZF','CM') THEN b.Money ELSE 0 END),0) 保全追加保费,  ")
	  .append( "NVL(SUM(CASE WHEN b.MoneyType = 'KF' THEN b.Money ELSE 0 END),0) 保全追加保费扣费 ")
	  .append( "FROM LIDataTransResult a  LEFT JOIN LBInsureAccTrace b on a.ContNo = b.ContNo and substr(keyunionvalue,13,14) = b.otherno ")
	  .append( bRiskPart )
	  .append( "AND b.OtherType = '10' ")
	  .append( "And b.ManageCom LIKE '"+mManageCom+"%' "  )
	  .append( "WHERE AccountDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "' ")
	  .append( "AND AccountCode='6031000000' ")
	  .append( "AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
	  .append( "AND ClassType='B-02' ")
	  .append( "AND FinItemType='C' ")
	  .append( "and exists (select 1 from licodetrans where codetype = 'ManageCom' and codealias = a.managecom and code like '"+mManageCom+ "%' ) ")
	  .append( aRiskPart )
	  .append( "AND SumMoney <> 0  ")
	  .append( "AND MoneyType IN ('ZF','CM','KF') ")
	  .append( "GROUP BY b.ManageCom,b.RiskCode WITH UR ") ;
      
      SSRS zbbSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到保全保费的数据LBInsureAccTrace";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  for(int j=1; j<=zbbSSRS.getMaxRow(); j++)
    	  {
    		  if(zbbSSRS.GetText(j, 1).equals(mExcelData[i][2]) && zbbSSRS.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][12]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][12]) + Double.parseDouble(zbbSSRS.GetText(j, 3)));
    			  mExcelData[i][13]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][13]) + Double.parseDouble(zbbSSRS.GetText(j, 4)));
    		  }
    	  }
      }   
      
      
//    解约金额  协议退保 　
      sql = new StringBuffer(); 
      sql.append("SELECT ManageCom, RiskCode, ")
      	.append(" NVL(SUM(CASE WHEN FeeOperationType = 'CT' THEN GetMoney ELSE 0 END),0) 解约金额, ") 
      	.append(" NVL(SUM(CASE WHEN FeeOperationType = 'XT' THEN GetMoney ELSE 0 END),0) 协议退保 ")
		.append("FROM LJAGetEndorse a WHERE FeeOperationType IN ('CT','XT') ")
		.append("AND MakeDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "' ") 
		.append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
		.append("AND GetMoney <> 0 ")
		.append("AND ManageCom LIKE '"+mManageCom+"%' " + aRiskPart )
		.append("GROUP BY ManageCom, RiskCode WITH UR ") ;
      
      SSRS ctSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到 解约金额  协议退保的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  mExcelData[i][14] = "0.00";
    	  mExcelData[i][16] = "0.00";
    	  for(int j=1; j<=ctSSRS.getMaxRow(); j++)
    	  {
    		  if(ctSSRS.GetText(j, 1).equals(mExcelData[i][2]) && ctSSRS.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][14]=PubFun.setPrecision2(ctSSRS.GetText(j, 3));
    			  mExcelData[i][16]=PubFun.setPrecision2(ctSSRS.GetText(j, 4));
    		  }
    	  }
      }
      
//    解约金管理费  　
      sql = new StringBuffer(); 
      sql.append("SELECT a.ManageCom, a.RiskCode, sum(b.money)  ")
		.append("FROM LJAGetEndorse a , LBInsureaccTrace b WHERE a.endorsementno = b.otherno ")
		.append("AND b.moneytype = 'TM' and b.OtherType = '10' ")
		.append("AND FeeOperationType in ('CT','XT') ")
		.append("AND a.MakeDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "' ")
		.append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode = 'U' AND RiskCode = a.RiskCode) ")
      	.append("AND a.ManageCom LIKE '"+mManageCom+"%' " + aRiskPart )
      	.append("GROUP BY a.ManageCom, a.RiskCode WITH UR ");
      
      SSRS ctSSRS1 = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到解约金管理费的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  mExcelData[i][15] = "0.00";
    	  for(int j=1; j<=ctSSRS1.getMaxRow(); j++)
    	  {
    		  if(ctSSRS1.GetText(j, 1).equals(mExcelData[i][2]) && ctSSRS1.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][15]=PubFun.setPrecision2(ctSSRS1.GetText(j, 3));
    		  }
    	  }
      }
      
    
//部分领取	部分领取管理费 LCInsureAccFeeTrace
      sql = new StringBuffer(); 
      sql.append("SELECT a.ManageCom,a.RiskCode, ")
		.append("NVL(SUM(a.Money),0) 部分领取, ")
		.append("NVL(SUM(b.Fee),0) 部分领取管理费 ")
		.append("FROM LCInsureAccTrace a LEFT JOIN LCInsureAccFeeTrace b ON a.ContNo = b.ContNo AND a.OtherNo = b.OtherNo " )
		.append("WHERE a.MakeDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "' ")
		.append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode = 'U' AND RiskCode = a.RiskCode) ")
		.append("AND a.ManageCom LIKE '"+mManageCom+"%' " + aRiskPart )
		.append("AND a.MoneyType = 'LQ' AND a.OtherType = '10' ")
		.append("AND b.ManageCom LIKE '"+mManageCom+"%' " + bRiskPart )
		.append("AND b.MoneyType = 'LQ' AND b.OtherType = '10'  ")
		.append("GROUP BY a.ManageCom,a.RiskCode WITH UR ") ;
      
      SSRS lqcSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到部分领取的数据LCInsureAccFeeTrace";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  mExcelData[i][17] = "0.00";
    	  mExcelData[i][18] = "0.00";
    	  for(int j=1; j<=lqcSSRS.getMaxRow(); j++)
    	  {
    		  if(lqcSSRS.GetText(j, 1).equals(mExcelData[i][2]) && lqcSSRS.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][17]=PubFun.setPrecision2(lqcSSRS.GetText(j, 3));
    			  mExcelData[i][18]=PubFun.setPrecision2(lqcSSRS.GetText(j, 4));
    		  }
    	  }
      }
      
//    部分领取	部分领取管理费 LBInsureAccFeeTrace
      sql = new StringBuffer(); 
      sql.append("SELECT a.ManageCom,a.RiskCode, ")
		.append("NVL(SUM(a.Money),0) 部分领取, ")
		.append("NVL(SUM(b.Fee),0) 部分领取管理费 ")
		.append("FROM LBInsureAccTrace a LEFT JOIN LBInsureAccFeeTrace b ON a.ContNo = b.ContNo AND a.OtherNo = b.OtherNo ")
		.append("WHERE a.MakeDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "' ")
		.append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode = 'U' AND RiskCode = a.RiskCode) ")
		.append("AND a.ManageCom LIKE '"+mManageCom+"%' " + aRiskPart )
		.append("AND a.MoneyType = 'LQ' AND a.OtherType = '10' ")
		.append("AND b.ManageCom LIKE '"+mManageCom+"%' " + bRiskPart )
		.append("AND b.MoneyType = 'LQ' AND b.OtherType = '10'  ")
		.append("GROUP BY a.ManageCom,a.RiskCode WITH UR ") ;
      
      SSRS lqbSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到部分领取的数据LBInsureAccFeeTrace";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  for(int j=1; j<=lqbSSRS.getMaxRow(); j++)
    	  {
    		  if(lqbSSRS.GetText(j, 1).equals(mExcelData[i][2]) && lqbSSRS.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][17]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][17]) + Double.parseDouble(lqbSSRS.GetText(j, 3)));
    			  mExcelData[i][18]=PubFun.setPrecision2(Double.parseDouble(mExcelData[i][18]) + Double.parseDouble(lqbSSRS.GetText(j, 4)));
    		  }
    	  }
      }
      
//    月度收益	保单管理费	风险保费 
      sql = new StringBuffer(); 
      sql.append("SELECT b.ManageCom,b.RiskCode, ")
	  .append("NVL(SUM(CASE WHEN b.MoneyType = 'LX' THEN b.Money ELSE 0 END),0) 月度收益, ")
	  .append("NVL(SUM(CASE WHEN b.MoneyType = 'MF' THEN b.Money ELSE 0 END),0) 保单管理费, ")
	  .append("NVL(SUM(CASE WHEN b.MoneyType = 'RP' THEN b.Money ELSE 0 END),0) 风险管理费 ")
	  .append("FROM  ")
	  .append("(SELECT * FROM Tempwt0  ")
	  .append("UNION ALL SELECT DISTINCT ContNo FROM( ")
	  .append("SELECT ContNo,SUM(CASE FinItemType WHEN 'C' THEN SumMoney ELSE -SumMoney END) Money FROM LIDataTransResult a  ")
	  .append("WHERE AccountDate BETWEEN '2008-07-01' AND '" + mEndDate + "' ")
	  .append("AND AccountCode='6031000000' ")
	  .append("AND EXISTS (SELECT 1 FROM LMRiskApp WHERE KindCode='U' AND RiskCode=a.RiskCode) ")
	  .append("AND exists (select 1 from licodetrans where codetype = 'ManageCom' and codealias = a.managecom and code like '"+mManageCom+ "%' ) ")
	  .append( aRiskPart )
	  .append("AND ClassType='N-03' ")
	  .append("GROUP BY ContNo ")
	  .append(")as aa ")
	  .append("WHERE Money<>0 ")
	  .append(")  ")
	  .append("as a LEFT JOIN ")
	  .append("(SELECT ManageCom,ContNo,OtherType ,MoneyType,Money,RiskCode  ")
	  .append("FROM LCInsureAccTrace WHERE OtherType = '6' ")
	  .append("AND PayDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "' ")
	  .append("AND ManageCom LIKE '"+mManageCom+"%' " + riskPart)
	  .append("UNION ALL  ")
	  .append("SELECT ManageCom,ContNo,OtherType ,MoneyType,Money,RiskCode  ")
	  .append("FROM LBInsureAccTrace WHERE OtherType = '6' ")
	  .append("AND PayDate BETWEEN '" + mStartDate + "' AND '" + mEndDate + "' ")
	  .append("AND ManageCom LIKE '"+mManageCom+"%' " + riskPart)
	  .append(") as b ON a.ContNo = b.ContNo  ")
	  .append("GROUP BY b.ManageCom,b.RiskCode WITH UR ") ;
      
      SSRS balaSSRS = tExeSQL.execSQL(sql.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "OmDayBalaNewDownloadBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到月结数据LCInsureAccTrace";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      for(int i=0; i<mExcelData.length; i++)
      {
    	  mExcelData[i][19] = "0.00";
    	  mExcelData[i][20] = "0.00";
    	  mExcelData[i][21] = "0.00";
    	  for(int j=1; j<=balaSSRS.getMaxRow(); j++)
    	  {
    		  if(balaSSRS.GetText(j, 1).equals(mExcelData[i][2]) && balaSSRS.GetText(j, 2).equals(mExcelData[i][4]))
    		  {
    			  mExcelData[i][19]=PubFun.setPrecision2(balaSSRS.GetText(j, 3));
    			  mExcelData[i][20]=PubFun.setPrecision2(balaSSRS.GetText(j, 4));
    			  mExcelData[i][21]=PubFun.setPrecision2(balaSSRS.GetText(j, 5));
    		  }
    	  }
      }
      
      if(mCreateExcelList.setData(mExcelData,displayData)==-1)
      {
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

    return true;
  }

}

