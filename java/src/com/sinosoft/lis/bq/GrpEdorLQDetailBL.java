package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


public class GrpEdorLQDetailBL
{
    public CErrors mErrors = new CErrors();

    private String mMessage = null;

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private EdorItemSpecialData mSpecialData = null;

    private String mEdorNo = null;

    private static String mEdorType = "LQ";

    private String mGrpContNo = null;

    /** 特需险种号 */
    private String mGrpPolNo = null;

    private LPDiskImportSet mLPDiskImportSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     * @param grpContNo String
     */
    public GrpEdorLQDetailBL(GlobalInput gi, String edorNo,
            String grpContNo, EdorItemSpecialData specialData)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = grpContNo;
        this.mSpecialData = specialData;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到输入数据
     * @return boolean
     */
    private boolean getInputData()
    {
        mLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, mEdorType,
                mGrpContNo, BQ.IMPORTSTATE_SUCC);
        //if (mLPDiskImportSet.size() == 0)
        //{
        //    mErrors.addOneError("找不到磁盘导入数据！");
        //    return false;
        //}
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!checkData())
        {
            return false;
        }
        setSpecialData();
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if (!checkClaiming())
        {
            return false;
        }
        if (!checkRiskCoede())
        {
            return false;
        }
        if (!checkGrpMoney())
        {
            return false;
        }
        if (!checkCustomers())
        {
            return false;
        }
        return true;
    }

    /**
     * 检查特需险种
     * @return boolean
     */
    private boolean checkRiskCoede()
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        if (tLCGrpPolSet.size() == 0)
        {
            mErrors.addOneError("未找到集体险种信息，请检查LCGrpPol表的数据！");
            return false;
        }
        int n = 0;
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            String riskCode = tLCGrpPolSchema.getRiskCode();
            if (CommonBL.isEspecialPol(riskCode))
            {
                mGrpPolNo = tLCGrpPolSchema.getGrpPolNo();
                n++;
            }
        }
        if (n > 1)
        {
            mErrors.addOneError("此保单含有多个特需险种，不能处理！");
            return false;
        }
        return true;
    }

    private boolean checkGrpMoney()
    {
        String[] grpPolNos = mSpecialData.getGrpPolNos();
        for (int i = 0; i < grpPolNos.length; i++)
        {
            String grpPolNo = grpPolNos[i];
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpPolNo(grpPolNo);
            if (!tLCGrpPolDB.getInfo())
            {
                mErrors.addOneError("找不到团体险种信息！");
                return false;
            }
            String riskCode = tLCGrpPolDB.getRiskCode();
            String polNo = CommonBL.getPubliAccPolNo(grpPolNo);
            LCInsureAccSchema tLCInsureAccSchema =
                    CommonBL.getLCInsureAcc(polNo,
                    CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP, riskCode));
            if (tLCInsureAccSchema == null)
            {
                mErrors.addOneError("找不到团体账户信息！");
                return false;
            }
            mSpecialData.setGrpPolNo(grpPolNo);
            double grpMoney = Double.parseDouble(mSpecialData.getEdorValue("GrpMoney")); //应领金额
            if (grpMoney > tLCInsureAccSchema.getInsuAccBala())
            {
                mErrors.addOneError("团体账户领取金额不能多于账户余额！");
                return false;
            }

            LCInsureAccSchema tLCInsureAccSchema2 =
                    CommonBL.getLCInsureAcc(polNo,
                    CommonBL.getInsuAccNo(BQ.ACCTYPE_FIXED, riskCode));
            if (tLCInsureAccSchema2 == null)
            {
                mErrors.addOneError("找不到团体固定账户信息！");
                return false;
            }
            mSpecialData.setGrpPolNo(grpPolNo);
            double grpFixMoney = Double.parseDouble(mSpecialData.getEdorValue(
                    "GrpFixMoney")); //应领金额
            if (grpFixMoney > tLCInsureAccSchema2.getInsuAccBala())
            {
                mErrors.addOneError("团体固定账户领取金额不能多于账户余额！");
                return false;
            }
        }
        return true;
    }

    /**
     * 检查客户的有效性
     * @return boolean
     */
    private boolean checkCustomers()
    {
        boolean flag = true;
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            String errorReason = "";
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            tLCInsuredSchema.setGrpContNo(mGrpContNo);
            tLCInsuredSchema.setName(tLPDiskImportSchema.getInsuredName());
            tLCInsuredSchema.setSex(tLPDiskImportSchema.getSex());
            tLCInsuredSchema.setBirthday(tLPDiskImportSchema.getBirthday());
            tLCInsuredSchema.setIDType(tLPDiskImportSchema.getIDType());
            tLCInsuredSchema.setIDNo(tLPDiskImportSchema.getIDNo());
            OldCustomerCheck customerCheck = new OldCustomerCheck(
                    tLCInsuredSchema);
            if (customerCheck.checkInsured() != OldCustomerCheck.OLD)
            {
                errorReason += "保单中不存在被保人" + tLCInsuredSchema.getName() +
                        "，导入无效！";
                flag = false;
            }
            else
            {
                LCInsureAccSchema tLCInsureAccSchema =
                        CommonBL.getLCInsureAcc(mGrpPolNo, customerCheck.getCustomerNo(),
                        BQ.ACCTYPE_INSURED);
                double leftMoney = tLCInsureAccSchema.getInsuAccBala();
                double importMoney = tLPDiskImportSchema.getMoney();
                if (importMoney <= 0)
                {
                    errorReason += "被保人领取金额不正确！";
                    flag = false;
                }
                else if (importMoney > leftMoney)
                {
                    errorReason += "被保人余额不足，当前余额为" + leftMoney + "元！";
                    flag = false;
                }
            }
            if (!errorReason.equals(""))
            {
                StringBuffer sql = new StringBuffer("Update LPDiskImport ");
                sql.append("Set State = '")
                        .append(BQ.IMPORTSTATE_FAIL).append("', ")
                        .append(" ErrorReason = '" + errorReason + "', ")
                        .append(" Operator = '")
                        .append(mGlobalInput.Operator).append("', ")
                        .append(" ModifyDate = '")
                        .append(mCurrentDate ).append("', ")
                        .append(" ModifyTime = '")
                        .append(mCurrentTime).append("' ")
                        .append("where EdorNo = '")
                        .append(mEdorNo).append("' ")
                        .append("and EdorType = '")
                        .append(mEdorType).append("' ")
                        .append("and GrpContNo = '")
                        .append(mGrpContNo).append("' ")
                        .append("and SerialNo = '")
                        .append(tLPDiskImportSchema.getSerialNo())
                        .append("' ");
                mMap.put(sql.toString(), "UPDATE");
                flag = false;
            }
            else
            {
                StringBuffer sql = new StringBuffer("Update LPDiskImport ");
                sql.append("Set State = '")
                        .append(BQ.IMPORTSTATE_SUCC).append("', ")
                        .append("InsuredNo = '")
                        .append(StrTool.cTrim(customerCheck.getCustomerNo()))
                        .append("', ")
                        .append("Operator = '")
                        .append(mGlobalInput.Operator).append("', ")
                        .append("ModifyDate = '")
                        .append(mCurrentDate).append("', ")
                        .append("ModifyTime = '")
                        .append(mCurrentTime).append("' ")
                        .append("where EdorNo = '")
                        .append(mEdorNo).append("' ")
                        .append("and EdorType = '")
                        .append(mEdorType).append("' ")
                        .append("and GrpContNo = '")
                        .append(mGrpContNo).append("' ")
                        .append("and SerialNo = '")
                        .append(tLPDiskImportSchema.getSerialNo())
                        .append("' ");
                mMap.put(sql.toString(), "UPDATE");
            }
        }
        if (flag == false)
        {
            mMessage = "导入数据中有无效的被保人信息！";
        }
        return true;
    }

    /**
     * 保存团单追加金额
     */
    private void setSpecialData()
    {
        mMap.put(mSpecialData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
        String sql = "update  LPGrpEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        System.out.println(sql);
        mMap.put(sql, "UPDATE");
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     *
     * @return boolean
     */

    private boolean checkClaiming() {
        String sql = " select rgtno from llregister where rgtobjno='"
                   + mGrpContNo +"' and rgtstate not in ('04','05') and declineflag is null ";
        ExeSQL temp = new ExeSQL();

        SSRS rs = temp.execSQL(sql);
        if (rs == null || rs.getMaxRow() == 0) {
            LCInsureAccTraceDB db = new LCInsureAccTraceDB();
            db.setGrpContNo(mGrpContNo);
            db.setState("temp");
            LCInsureAccTraceSet tempSet = db.query();
            if (tempSet == null || tempSet.size() == 0) {
                return true;
            } else {
                mErrors.addOneError("帐户轨迹表中有理赔临时记录，不能操作");
                return false;
            }
        } else {
            StringBuffer tem = new StringBuffer();
            tem.append("该单正在理赔或有未撤件的理赔申请，不能操作,批次号");
            for (int i = 1; i <= rs.getMaxRow(); i++) {
                tem.append(rs.GetText(i, 1));
                tem.append("  ");
            }
            mErrors.addOneError(tem.toString());
            return false;
        }
    }

}
