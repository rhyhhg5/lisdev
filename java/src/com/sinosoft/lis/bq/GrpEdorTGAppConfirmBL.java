package com.sinosoft.lis.bq;

import sqlj.runtime.error.Errors;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description: 团险协议退保项目理算处理类
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @author Yang Yalin
 * @version 1.1
 */
public class GrpEdorTGAppConfirmBL implements EdorAppConfirm {
	/** 全局信息 */
	private GlobalInput mGI = null;
	/** 团单协议退保项目 */
	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

	/** 处理后的数据 */
	private VData mResult = new VData();
	private BqCalBL mBqCalBL = new BqCalBL();
	private String mFinType = null;
	private double mGetMoney = 0; // 本次退费
	private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
	private MMap map = new MMap();
	private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    
	public GrpEdorTGAppConfirmBL() {
	}

	public boolean submitData(VData cInputData, String operate) {
		// 接受传入的数据
		if (!getInputData(cInputData)) {
			return false;
		}
		// 业务逻辑的处理
		if (!dealData()) {
			return false;
		}

		prepareOutputData();

		return true;
	}

	/**
	 * 接受传入的数据
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData
				.getObjectByObjectName("LPGrpEdorItemSchema", 0);
		mGI = ((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		if (mLPGrpEdorItemSchema == null || mGI == null) {
			mErrors.addOneError("传入的数据不完整");
			return false;
		}
		return true;

	}

	/**
	 * 业务逻辑的处理
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		
		if(dealAccTrace()){
			
		}
		
		// 生成团单协议退保退费财务接口
		if (!setXTFee()) {
			return false;
		}

		changeState();

		return true;
	}

	/**
	 * 生成团单协议退保退费财务接口
	 * 
	 * @return boolean
	 */
	private boolean setXTFee() {
		LCPolSchema tLCPolSchema = null;

		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
		Reflections ref = new Reflections();
		ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);

		// 查询团险协议退费金额
		EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(
				mLPGrpEdorItemSchema);
		tEdorItemSpecialData.query();
		LPEdorEspecialDataSet set = tEdorItemSpecialData.getSpecialDataSet();
		String aFeeType = "TB";
		double xtMoney = 0;
		String accType = "";
//		String accType_G = "671803"; // 公共账户
//		String accType_P1 = "671804"; // 个人账户单位交费部分
//		String accType_P2 = "671805"; // 个人账户个人交费部分
//      modify 2016-3-17 对照明细录入： 个人账户个人交费部分 对应accType_P1   个人账户单位交费部分  对应accType_P2
		String accType_G=new ExeSQL().getOneValue("select insuaccno from LMRiskAccPay where riskcode in " +
				"(select riskcode from lcgrppol where grpcontno ='"+mLPGrpEdorItemSchema.getGrpContNo()+"') and  payplancode in " +
				"(select payplancode from LMDutyPay where AccPayClass ='3') with ur " );
		String accType_P1=new ExeSQL().getOneValue("select insuaccno from LMRiskAccPay where riskcode in " +
				"(select riskcode from lcgrppol where grpcontno ='"+mLPGrpEdorItemSchema.getGrpContNo()+"') and  payplancode in " +
				"(select payplancode from LMDutyPay where AccPayClass ='5') with ur " );
		String accType_P2=new ExeSQL().getOneValue("select insuaccno from LMRiskAccPay where riskcode in " +
				"(select riskcode from lcgrppol where grpcontno ='"+mLPGrpEdorItemSchema.getGrpContNo()+"') and  payplancode in " +
				"(select payplancode from LMDutyPay where AccPayClass ='4') with ur " );
		for (int i = 1; i <= set.size(); i++) {
			LCPolDB tLCPolDB = new LCPolDB();
			tLCPolDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
			tLCPolDB.setPolTypeFlag("2");
			LCPolSet tLCPolSet = tLCPolDB.query();
			if (tLCPolSet.size() > 0) {
				tLCPolSchema = tLCPolSet.get(1);// 公共账户信息
			} else {
				mErrors.addOneError("没有查询保单险种信息!");
				return false;
			}
			LPEdorEspecialDataSchema schema = set.get(i);
			xtMoney = -Double.parseDouble(schema.getEdorValue());//协议退保金额转换为负值
			if (schema.getDetailType().equals("XTFEE_G")) {
				accType = accType_G;
			} else if (schema.getDetailType().equals("XTFEE_P1")) {
				accType = accType_P1;
				tLCPolSchema.setContNo(BQ.FILLDATA);
				tLCPolSchema.setPolNo(BQ.FILLDATA);
				tLCPolSchema.setInsuredNo(BQ.FILLDATA);
			} else if (schema.getDetailType().equals("XTFEE_P2")) {
				accType = accType_P2;
				tLCPolSchema.setContNo(BQ.FILLDATA);
				tLCPolSchema.setPolNo(BQ.FILLDATA);
				tLCPolSchema.setInsuredNo(BQ.FILLDATA);
			}else{
				continue;
			}
			mGetMoney+=xtMoney;
			LJSGetEndorseSchema tLJSGetEndorseSchema = initLJSGetEndorse(
					tLCPolSchema, accType, aFeeType, xtMoney, mGI);
			mLJSGetEndorseSet.add(tLJSGetEndorseSchema);
		}
		return true;
	}
	
	private boolean dealAccTrace(){
		LPInsureAccClassSchema tLPInsureAccClassSchema=null;
		LPInsureAccClassDB tLPInsureAccClassDB = new LPInsureAccClassDB();
        tLPInsureAccClassDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPInsureAccClassDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        LPInsureAccClassSet tLPInsureAccClassSet= tLPInsureAccClassDB.query();
        for(int i=1;i<=tLPInsureAccClassSet.size();i++){
        	tLPInsureAccClassSchema=tLPInsureAccClassSet.get(i);
        	setLPAccTrace(tLPInsureAccClassSchema, -tLPInsureAccClassSchema.getInsuAccBala(),mLPGrpEdorItemSchema.getEdorType(),null);
        	setLPAcc(tLPInsureAccClassSchema,0);
        }
		
		return true;
	}
	
	/**
     * 设置帐户金额轨迹
     * @param tLPInsureAccClassSchema LPInsureAccClassSchema 轨迹对应的当前账户
     * @param tMoney double
     * @param tMoneyType String 
     * @param tAILPInsureAccClassSchema LPInsureAccClassSchema 轨迹来源或者去向账户
     */
    private void setLPAccTrace(LPInsureAccClassSchema tLPInsureAccClassSchema, double tMoney ,String tMoneyType,LPInsureAccClassSchema tAILPInsureAccClassSchema)
    {
        String serialNo;

            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPInsureAccTraceSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPInsureAccTraceSchema.setGrpContNo(tLPInsureAccClassSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(tLPInsureAccClassSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(tLPInsureAccClassSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(tLPInsureAccClassSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(tLPInsureAccClassSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(tLPInsureAccClassSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(tLPInsureAccClassSchema.getPayPlanCode());
        tLPInsureAccTraceSchema.setOtherNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(tMoneyType);
        tLPInsureAccTraceSchema.setMoney(tMoney);  
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mLPGrpEdorItemSchema.getEdorValiDate());
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setManageCom(tLPInsureAccClassSchema.getManageCom());
        tLPInsureAccTraceSchema.setOperator(mGI.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        if(!(tAILPInsureAccClassSchema==null)){
        tLPInsureAccTraceSchema.setAIPolNo(tAILPInsureAccClassSchema.getPolNo());
        tLPInsureAccTraceSchema.setAIInsuAccNo(tAILPInsureAccClassSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setAIPayPlanCode(tAILPInsureAccClassSchema.getPayPlanCode());
        }
        map.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }
    
    /**
     * 设置账户余额,包括acc和accclass两个表的P表
     * @param tLPInsureAccClassSchema LPInsureAccClassSchema
     * @param getMoney double
     */
    private boolean setLPAcc(LPInsureAccClassSchema tLPInsureAccClassSchema, double leftMoney)
    {
        LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
        LPInsureAccDB tLPInsureAccDB = new LPInsureAccDB();
        tLPInsureAccDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPInsureAccDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPInsureAccDB.setPolNo(tLPInsureAccClassSchema.getPolNo());
        tLPInsureAccDB.setInsuAccNo(tLPInsureAccClassSchema.getInsuAccNo());
        if(!tLPInsureAccDB.getInfo()){
        	CError.buildErr(this, "获取账户P表失败！");
        	return false;
        }
        tLPInsureAccSchema=tLPInsureAccDB.getSchema();
        tLPInsureAccSchema.setInsuAccBala(leftMoney);
        map.put(tLPInsureAccSchema, "DELETE&INSERT");
        tLPInsureAccClassSchema.setInsuAccBala(leftMoney);
        map.put(tLPInsureAccClassSchema, "DELETE&INSERT");
        return true;
    }

	/**
	 * 改变业务状态
	 */
	private void changeState() {
		mLPGrpEdorItemSchema.setGetMoney(mGetMoney);
		mLPGrpEdorItemSchema.setEdorState(BQ.EDORSTATE_CAL);
		mLPGrpEdorItemSchema.setOperator(mGI.Operator);
		mLPGrpEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
		mLPGrpEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
	}

	/**
	 * 将处理后的数据放入到VData容器中，为输出数据处理结果做准备
	 */
	private void prepareOutputData() {
		map.put(mLJSGetEndorseSet, "DELETE&INSERT");
		map.put(mLPGrpEdorItemSchema, "UPDATE");

		mResult.clear();
		mResult.add(map);
	}

	public VData getResult() {
		return mResult;
	}

	/**
	 * 1、 一年期及以内险种 对应 冲退保费 TF。 2、 一年期以上险种 对应 冲退保金 TB。
	 * 
	 * @param String
	 * @return String
	 */
	private String getRiskCodeFinType(String riskCode) {

		// 通过险种代码查询险种期限
		LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
		tLMRiskAppDB.setRiskCode(riskCode);
		if (!tLMRiskAppDB.getInfo()) {
			return null;
		}
		System.out.println("L长S短M一年:" + tLMRiskAppDB.getRiskPeriod());
		// 一年期及以内险种冲退保费 TF。
		if (tLMRiskAppDB.getRiskPeriod().equals("M")
				|| tLMRiskAppDB.getRiskPeriod().equals("S")) {
			return BQ.FEEFINATYPE_TF;
		}
		// 一年期以上险种冲退保金 TB
		else if (tLMRiskAppDB.getRiskPeriod().equals("L")) {
			return BQ.FEEFINATYPE_TB;
		}
		return null;
	}

	public LJSGetEndorseSchema initLJSGetEndorse(LCPolSchema aLCPolSchema,
			String insuAccNo, String aFeeType, double aGetMoney,
			GlobalInput aGlobalInput) {
		LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
		// 生成批改交退费表
		tLJSGetEndorseSchema.setGetNoticeNo(mLPGrpEdorItemSchema.getEdorNo()); // 给付通知书号码
		tLJSGetEndorseSchema.setEndorsementNo(mLPGrpEdorItemSchema.getEdorNo());
		tLJSGetEndorseSchema.setGrpContNo(aLCPolSchema.getGrpContNo());
		tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
		tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
		tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
		tLJSGetEndorseSchema.setFeeOperationType(mLPGrpEdorItemSchema
				.getEdorType());
		tLJSGetEndorseSchema.setGetDate(mLPGrpEdorItemSchema.getEdorValiDate());
		tLJSGetEndorseSchema.setGetMoney(aGetMoney);
		tLJSGetEndorseSchema.setFeeOperationType(mLPGrpEdorItemSchema
				.getEdorType()); // 补退费业务类型
		tLJSGetEndorseSchema.setFeeFinaType(aFeeType); // 补退费财务类型
		tLJSGetEndorseSchema.setPayPlanCode(insuAccNo); // 账户类型
		tLJSGetEndorseSchema.setDutyCode("0"); // 无作用，但一定要，转ljagetendorse时非空
		tLJSGetEndorseSchema.setOtherNo(mLPGrpEdorItemSchema.getEdorNo()); // 其他号码置为保全批单号
		tLJSGetEndorseSchema.setOtherNoType("10");
		tLJSGetEndorseSchema.setGetFlag("0");
		tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
		tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
		tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
		tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
		tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
		tLJSGetEndorseSchema.setKindCode(aLCPolSchema.getKindCode());
		tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
		tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
		tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
		tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
		tLJSGetEndorseSchema.setHandler(aLCPolSchema.getHandler());
		tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
		tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
		tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
		tLJSGetEndorseSchema.setOperator(aGlobalInput.Operator);
		tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
		tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
		tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
		tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
		return tLJSGetEndorseSchema;
	}

	public static void main(String[] args) {
	}
}
