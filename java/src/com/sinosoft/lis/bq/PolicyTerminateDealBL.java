package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

//程序名称：PolicyTerminateDealBL.java
//程序功能：失效满2年的保单设置“终止”状态
//创建日期：2010-03-26
//创建人  ：zhanggm
//更新记录：

public class PolicyTerminateDealBL {
    private GlobalInput mG = new GlobalInput();
    private String mManageCom = "";
    private LLCaseCommon mLLCaseCommon = new LLCaseCommon();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private ExeSQL mExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public PolicyTerminateDealBL() {}

    private boolean getInputData(VData cInputData) 
    {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mManageCom = mG.ManageCom;
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {
        cInputData = (VData) cInputData.clone();
        if (!getInputData(cInputData)) 
        {
            return false;
        }
        try
        {
        	callSingleTerminate();
        }
        catch(Exception ex)
        {
            System.out.print(ex.toString());
        }
        return true;
    }


    /***************************个单满期终止处理********************************/
    private boolean callSingleTerminate() 
    {
        System.out.println("个单长险种满期终止(永久失效)批处理运行开始。。。。");
        String subSql = getSQLManageCom();
        String strSql = "select * from LCPol a where  "
                      + " conttype = '1' and a.paytodate + 2 years + 60 days <= '" + mCurrentDate + "' " //cbs00040508 应缴日到可恢复有效的时间中间跨度是2年60天,跟条款保持一致
                      + "and exists (select 1 from LMRiskApp where RiskPeriod ='L' and riskcode = a.riskCode)  " //长期险满期终止
                      + subSql + " and stateflag = '2' with ur ";
        System.out.println("Sql ：" + strSql);
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolSet = tLCPolDB.executeQuery(strSql);
        
        try
        {
//        	逐条调用险种保单终止处理
        	SingleTerminateDeal(tLCPolSet);
        }
        catch(Exception ex)
        {
            System.out.print(ex.toString());
        }
        
        System.out.println("个单保单满期终止批处理运行开始。。。。");
        
        //查询保单
        String Sql = "select * from LCCont a where  1=1  "
                   + "and conttype = '1' and a.paytodate + 2 years + 60 days <= '" + mCurrentDate + "' "
                   + "and stateflag = '2'  "
                   + "and exists (select 1 from lccontstate where statereason = '09' and state = '1' " 
                   + "and statetype = 'Terminate' and contno = a.contno and polno <> '" + BQ.FILLDATA + "')"
                   + subSql + " with ur";
        System.out.println("Sql ：" + Sql);
        LCContSet tLCContSet = new LCContSet();
        LCContDB tLCContDB = new LCContDB();
        tLCContSet = tLCContDB.executeQuery(Sql);
        
        try
        {
//        	保单永久失效处理
        	SinglePolicyTerminateDeal(tLCContSet);
        }
        catch(Exception ex)
        {
            System.out.print(ex.toString());
        }
        System.out.println("满期终止(永久失效)批处理运行结束。。。。");

        return true;
    }

    private boolean SingleTerminateDeal(LCPolSet aLCPolSet) 
    {
        for (int i = 1; i <= aLCPolSet.size(); i++) 
        {
        	LCContStateSchema tLCContStateSchema = new LCContStateSchema();
            MMap tMap = new MMap();
            VData tData = new VData();
            PubSubmit tPubSubmit = new PubSubmit();
            
            // 判断是否满足剩余的条件
            LCPolSchema tLCPolSchema = new LCPolSchema();
            tLCPolSchema.setSchema(aLCPolSet.get(i));
            if (!canTerminate(tLCPolSchema)) 
            {
                continue;
            }
            
            //永久失效时间
            String tStartDate = PubFun.calDate(tLCPolSchema.getPaytoDate(), 2, "Y", null);
            tStartDate = PubFun.calDate(tStartDate, 60, "D", null);
            
//          处理旧状态，终止时间设置为新状态的起始时间
            String upsql = "update lccontstate set enddate = '" + tStartDate + "', state = '0', modifydate = '" + mCurrentDate 
                         + "', modifytime = '" + mCurrentTime + "' where state = '1' and contno = '" 
                         + tLCPolSchema.getContNo() + "' and polno = '" + tLCPolSchema.getPolNo() + "' ";
            
            tMap.put(upsql, SysConst.UPDATE);

            tLCContStateSchema.setContNo(tLCPolSchema.getContNo());
            tLCContStateSchema.setPolNo(tLCPolSchema.getPolNo());

            tLCContStateSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
            tLCContStateSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
            tLCContStateSchema.setStateType("Terminate");
            tLCContStateSchema.setOtherNoType("");
            tLCContStateSchema.setOtherNo("");
            tLCContStateSchema.setState("1");
            tLCContStateSchema.setStateReason("09");//Terminate: 09 失效满两年，永久失效 01 满期终止 02 退保终止 03 解约终止 04 理赔终止 05 自垫终止 06 贷款终止 07 失效终止 08 其他终止 10 续保
            tLCContStateSchema.setStartDate(tStartDate);
            tLCContStateSchema.setRemark("失效满两年，永久失效。");
            tLCContStateSchema.setOperator("000");
            tLCContStateSchema.setMakeDate(mCurrentDate);
            tLCContStateSchema.setMakeTime(mCurrentTime);
            tLCContStateSchema.setModifyDate(mCurrentDate);
            tLCContStateSchema.setModifyTime(mCurrentTime);
            tMap.put(tLCContStateSchema, "DELETE&INSERT");
            
            setLCPol(tLCPolSchema, "3",tMap);
            System.out.println(tLCPolSchema.getPolNo() + "-满期终止-" + tLCPolSchema.getRiskCode());
            tData.clear();
            tData.add(tMap);
            tPubSubmit.submitData(tData, "");
        }
        return true;
    }
    
    private boolean SinglePolicyTerminateDeal(LCContSet tLCContSet) 
    {
        for (int i = 1; i <= tLCContSet.size(); i++) 
        {
        	MMap tMap = new MMap();
            LCContSchema tLCContSchema = new LCContSchema();
            tLCContSchema.setSchema(tLCContSet.get(i));
            if(!canTerminate(tLCContSchema))
            {
            	continue;
            }
            
            String tContNo = tLCContSchema.getContNo();
            //永久失效起始时间为
            String sql = "select max(StartDate) from lccontstate where statetype = 'Terminate' and StateReason = '09' " 
            	          + "and state = '1' and contno = '" + tContNo + "' and polno <> '" + BQ.FILLDATA + "' ";
            
            String tStartDate = mExeSQL.getOneValue(sql);
            
            //处理旧状态，终止时间设置为新状态的起始时间
            String upsql = "update lccontstate set enddate = '" + tStartDate + "',state = '0',modifydate = '" + mCurrentDate 
                         + "', modifytime = '" + mCurrentTime + "' where state = '1' and contno = '" 
                         + tContNo + "' and polno = '" + BQ.FILLDATA + "' ";
            
            tMap.put(upsql, SysConst.UPDATE);
            
            PubSubmit tPubSubmit = new PubSubmit();
            VData tData = new VData();
            
            LCContStateSchema tLCContStateSchema = new LCContStateSchema();
            tLCContStateSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLCContStateSchema.setContNo(tLCContSchema.getContNo());
            tLCContStateSchema.setInsuredNo(tLCContSchema.getInsuredNo());
            tLCContStateSchema.setPolNo(BQ.FILLDATA);
            tLCContStateSchema.setStateType("Terminate");
            tLCContStateSchema.setStateReason("09");
            tLCContStateSchema.setState("1");
            tLCContStateSchema.setOperator("000");
            tLCContStateSchema.setStartDate(tStartDate);
            tLCContStateSchema.setRemark("失效满两年，永久失效。");
            tLCContStateSchema.setMakeDate(mCurrentDate);
            tLCContStateSchema.setMakeTime(mCurrentTime);
            tLCContStateSchema.setModifyDate(mCurrentDate);
            tLCContStateSchema.setModifyTime(mCurrentTime);
            
            tMap.put(tLCContStateSchema, "DELETE&INSERT");
            
            setLCCont(tLCContSchema, "3", tMap);
            
            sendTerminateNotice(tLCContSchema, mManageCom, "210",tStartDate,tMap);
            
            tData.clear();
            tData.add(tMap);
            tPubSubmit.submitData(tData,"");
            
            
        }
        return true;
    }
    

    /*发送永久失效通知书*/
    private boolean sendTerminateNotice(LCContSchema tLCContSchema, String tCom, String tCode, String tStartDate,MMap tMap) 
    {
        String tLimit = PubFun.getNoLimit(tCom);
        String serNo = PubFun1.CreateMaxNo("YJXXNO", tLimit);//永久失效号
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq(serNo);
        tLOPRTManagerSchema.setOtherNo(tLCContSchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //个人保单号
        tLOPRTManagerSchema.setCode(tCode); //210为永久失效通知书的代码，select * from ldcode where codetype = 'pausecode';
        tLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(tCom);
        tLOPRTManagerSchema.setReqOperator("000");
        tLOPRTManagerSchema.setPrtType("0"); //前台打印
        tLOPRTManagerSchema.setStateFlag("0"); //提交打印
        tLOPRTManagerSchema.setStandbyFlag1(tStartDate);  //保单
        tLOPRTManagerSchema.setMakeDate(mCurrentDate);
        tLOPRTManagerSchema.setMakeTime(mCurrentTime);
        tMap.put(tLOPRTManagerSchema, "DELETE&INSERT");
        return true;
    }


    /**
     * @param tLCPolSchema LCPolSchema
     * @return boolean
     */
    private boolean canTerminate(LCPolSchema tLCPolSchema) 
    {
    	String tPolno = tLCPolSchema.getPolNo();
        //正在做保全不终止
        if (edorseState(tLCPolSchema.getContNo(),tLCPolSchema.getContType())) 
        {
        	System.out.println(tPolno + "——在做保全项目，不能永久失效。");
            return false;
        }
        
//      正在做理赔不终止
        String claimState = mLLCaseCommon.getClaimState(tLCPolSchema.getPolNo());
        if (claimState.equals("1") || claimState.equals("2")) 
        {
            System.out.println(tPolno + "——在做理赔项目，不能永久失效。");
            return false;
        }
        
        //已经终止或永久失效的保单不参加永久失效
        String sql = "select count(1) from lccontstate where state = '1' and statetype = 'Terminate' and polno = '" + tPolno + "' ";
        String tCount = mExeSQL.getOneValue(sql);
        if (!"0".equals(tCount))
        {
        	System.out.println(tPolno + "——已经终止或永久失效的保单不参加永久失效。");
            return false;
        }
        
        //保单下含有万能险，不参加永久失效
        //加入对万能险附加重疾判断的校验。231001险种现在需要进行永久失效处理
        if(!"231001".equals(tLCPolSchema.getRiskCode()) && !"231201".equals(tLCPolSchema.getRiskCode())){
        	if(CommonBL.hasULIRisk(tLCPolSchema.getContNo()))
            {
            	System.out.println(tPolno + "——保单下含有万能险，不参加永久失效。");
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * @param tLCContSchema LCContSchema
     * @return boolean
     */
    private boolean canTerminate(LCContSchema tLCContSchema) 
    {
    	String tContNo = tLCContSchema.getContNo();

        //若存在有效险种，保单不终止
        String sql = "select count(1) from lcpol a where stateflag = '" + BQ.STATE_FLAG_SIGN 
                   + "' and contno = '" + tContNo + "'  with ur";
        
        String tCount = mExeSQL.getOneValue(sql);
        if (!"0".equals(tCount))
        {
        	System.out.println(tContNo + "——保单存在有效险种，不能永久失效。");
            return false;
        	
        }
        //若存在失效长期险，保单不终止
        sql = "select count(1) from lcpol a where stateflag = '" + BQ.STATE_FLAG_AVAILABLE + "' and contno = '" + tContNo 
            + "' and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskperiod = 'L') with ur";
        
        tCount = mExeSQL.getOneValue(sql);
        if (!"0".equals(tCount))
        {
        	System.out.println(tContNo + "——保单存在未永久失效的长期险种，不能永久失效。");
            return false;
        	
        }
        
        return true;
    }

    /*保全状态*/
    public boolean edorseState(String aContNo,String aContType) 
    {
        String tableName = "";
        String contType = "";
        //个单
        if ("1".equals(aContType)) 
        {
            tableName = " LPEdorMain ";
            contType = " contNo ";
        } 
        else 
        {
            tableName = " LPGrpEdorMain ";
            contType = " grpContNo ";
        }

        StringBuffer sql = new StringBuffer();
        sql.append("select a.edorAcceptNo ")
                .append("from LPEdorApp a, ")
                .append(tableName).append(" b ")
                .append("where a.edorAcceptNo = b.edorAcceptNo ")
                .append("    and b.").append(contType).append(" = '")
                .append(aContNo).append("' ")
                .append("    and a.edorState != '")
                .append(BQ.EDORSTATE_CONFIRM).append("' ")
                .append("   and not exists(select 1 from LGwork ")
                .append("           where WorkNo = a.EdorAcceptNo and StatusNo = '8') "); //撤销
        System.out.println(sql.toString());
        ExeSQL e = new ExeSQL();
        SSRS tSSRS = e.execSQL(sql.toString());
        if (tSSRS != null && tSSRS.getMaxRow() > 0) 
        {
            return true;
        }

        return false;
    }

    /*SQL中取管理结构工具*/
    private String getSQLManageCom() 
    {
        String mSQL = "";
        if (mManageCom != null && !mManageCom.trim().equals("")) 
        {
            if (mManageCom.length() >= 4) 
            {

                mSQL = " and ManageCom like '" + mManageCom.substring(0, 4) +
                       "%'";
            } 
            else 
            {
                mSQL = " and ManageCom like '86%'";
            }
        } 
        else 
        {
            mSQL = " ";
        }
        return mSQL;
    }


    /*险种状态设置*/
    private boolean setLCPol(LCPolSchema tLCPolSchema, String StateFlag,MMap tMap) 
    {
        String updatepol = "update LCPol Set StateFlag = '"
                           + StateFlag + "',ModifyDate = '" + mCurrentDate
                           + "',ModifyTime = '" + mCurrentTime +
                           "' where PolNo = '" + tLCPolSchema.getPolNo() + "'";
        tMap.put(updatepol, "UPDATE");
        return true;
    }

    /*个单保单设置状态*/
    private boolean setLCCont(LCContSchema tLCContSchema, String StateFlag,MMap tMap) 
    {
        String updatecont = "update LCCont Set StateFlag = '"
                            + StateFlag + "',ModifyDate = '" + mCurrentDate
                            + "',ModifyTime = '" + mCurrentTime +
                            "' where Contno = '" + tLCContSchema.getContNo() +
                            "'";
        tMap.put(updatecont, "UPDATE");
        return true;
    }

    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
//        LCGrpContSchema ttLCGrpContSchema = new LCGrpContSchema();
//        ttLCGrpContSchema.setGrpContNo("0000486001");
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo("00001490205");
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);

        PolicyTerminateDealBL mPolicyAbateDealBL = new PolicyTerminateDealBL();

//        mPolicyAbateDealBL.submitData(mVData,"Available");
        mPolicyAbateDealBL.submitData(mVData, "Terminate");
//       System.out.print(mPolicyAbateDealBL.edorseState("00000619801"));
//         mPolicyAbateDealBL.edorseState("0000126302");
    }


}
