package com.sinosoft.lis.bq;

import com.cbsws.obj.LCContTable;
import com.cbsws.obj.LGWorkTable;
import com.cbsws.obj.LPEdorItemTable;
import com.cbsws.obj.ParamInfo;
import com.sinosoft.lis.bl.LJSGetEndorseBL;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LJSGetEndorseDB;
import com.sinosoft.lis.db.LPEdorEspecialDataDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.schema.LPInsureAccSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.task.TaskAutoFinishBL;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class BQWTQNInfaceBL {
	
	private LCContTable cLCContTable;
	private GlobalInput mGlobalInput = new GlobalInput();//公共信息
	private String mEdorAcceptNo;
	private LGWorkTable mLgWorkTable;
	private LPEdorItemTable mLPedorItemTable;
	private ParamInfo mParamInfo;
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
    private LCContSchema mLCContSchema;
    private LPPolSet mLPPolSet = new LPPolSet();
    public CErrors mErrors = new CErrors();
    private LPContSchema mLPContschema = new LPContSchema();
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
	
        public BQWTQNInfaceBL() {
        }
        
        public BQWTQNInfaceBL(GlobalInput tGlobalInput,LCContTable tLCContTable,LCContSchema tLCContSchema){
        	this.mGlobalInput = tGlobalInput;
        	this.cLCContTable = tLCContTable;
        	this.mLCContSchema = tLCContSchema;
        }
        
        private boolean getInputData(VData cInputData){
        	mGlobalInput = (GlobalInput) cInputData
            .getObjectByObjectName("GlobalInput", 0);
        	mLgWorkTable = (LGWorkTable) cInputData
		                           .getObjectByObjectName("LGWorkTable", 0);
        	cLCContTable = (LCContTable) cInputData
        	.getObjectByObjectName("LCContTable", 0);
        	mLPedorItemTable = (LPEdorItemTable) cInputData
		                           .getObjectByObjectName("LPedorItemTable", 0);
        	mParamInfo = (ParamInfo) cInputData
		                          .getObjectByObjectName("ParamInfo", 0);
        	mLCContSchema = (LCContSchema) cInputData
        	.getObjectByObjectName("LCContSchema", 0);
		
		      if (mGlobalInput == null)
		      {
		    	  mErrors.addOneError("请传入参数信息错误");
		          return false;
		      }
		      return true;
        }
        

        public boolean submit(VData cInputData){
            if (!getInputData(cInputData))
            {
                return false;
            }

            if (!deal())
            {
                return false;
            }

            return true;
        }
        
        private boolean deal()  {	        
        	//申请工单
        	if(!createWorkNo()){
        		return false;
        	}
        	
        	//添加保全
        	if(!addEdorItem()){
        		//撤销工单
        		if(!cancelEdorItem("1","I&EDORAPP")){
        			return false;
        		}
        		return false;
        	}
        	
        	//录入明细
        	if(!saveDetail()){
        		//撤销工单
        		if(!cancelEdorItem("1","I&EDORMAIN")){
        			return false;
        		}else{
            		if(!cancelEdorItem("1","I&EDORAPP")){
            			return false;
            		}
        		}
        		return false;
        	}
        	
        	//理算确认
        	if(!appConfirm()){
        		//撤销工单
        		if(!cancelEdorItem("1","I&EDORMAIN")){
        			return false;
        		}else{
            		if(!cancelEdorItem("1","I&EDORAPP")){
            			return false;
            		}
        		}
        		return false;
        	}
        	
            creatPrintVts();
        	
        	//保全确认
            if (!edorConfirm())
            {
        		//撤销工单
        		if(!cancelEdorItem("1","I&EDORMAIN")){
        			return false;
        		}else{
            		if(!cancelEdorItem("1","I&EDORAPP")){
            			return false;
            		}
        		}
                return false;
            }
            
            //修改批单以及财务数据
            if(!endEdor()){
            	return false;
            }
            
            return true;
        }
        
        public LPEdorItemSchema getEdorItem(){
        	return mLPEdorItemSchema;
        }
        
        private boolean cancelEdorItem(String edorstate,String transact){
        	  PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
        	  LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        	  LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
        	  TransferData tTransferData = new  TransferData();  
        	  VData tVData = new VData();
        	  tVData.addElement(mGlobalInput);
        	  
        	  if("I&EDORAPP".equals(transact)){
        		  tLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);     
        		  tLPEdorAppSchema.setEdorState(edorstate);
        		  
        		  String delReason = "";
        		  String reasonCode ="002";
        		  
        		  tTransferData.setNameAndValue("DelReason",delReason);
        		  tTransferData.setNameAndValue("ReasonCode",reasonCode);
        		  tVData.addElement(tLPEdorAppSchema);
        		  // 准备传输数据 VData
        		  tVData.addElement(tTransferData);
        	  }else if ("I&EDORMAIN".equals(transact)){
        		     tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo); 
        		     tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
        		     tLPEdorMainSchema.setEdorState(edorstate);     
        		     tLPEdorMainSchema.setContNo(mLCContSchema.getContNo());     
        		     String delReason = "";
        		     String reasonCode ="002";
        		     System.out.println(delReason);  
        		     
        		     tTransferData.setNameAndValue("DelReason",delReason);
        		     tTransferData.setNameAndValue("ReasonCode",reasonCode);
        		     tVData.addElement(tLPEdorMainSchema);
        		     tVData.addElement(tTransferData);
        	  }
            try {
				//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
				System.out.println("hello");
				tPGrpEdorCancelUI.submitData(tVData,transact);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mErrors.addOneError("撤销工单失败"+e);
				return false;
			}
        	return true;
        }

        private boolean createWorkNo(){
        	
            //得到工单信息
            LGWorkSchema tLGWorkSchema = new LGWorkSchema();
//            if(mLgWorkTable.getCustomerNo().equals(mLCContSchema.getAppntNo())){
            	tLGWorkSchema.setCustomerNo(mLCContSchema.getAppntNo());
//            }else{
//           	 mErrors.addOneError("传入的投保人客户号与保单号不对应。");
//             return false;
//            }
            tLGWorkSchema.setTypeNo(mLgWorkTable.getTypeNo());
            tLGWorkSchema.setContNo(cLCContTable.getContNo());
            tLGWorkSchema.setApplyTypeNo(mLgWorkTable.getApplyTypeNo());
            tLGWorkSchema.setAcceptWayNo(mLgWorkTable.getAcceptWayNo());
            tLGWorkSchema.setAcceptDate(mLgWorkTable.getAcceptDate());
            tLGWorkSchema.setRemark("专为去哪网犹豫期退保生成");
            
            mGlobalInput.Operator = mParamInfo.getOperator();
            mGlobalInput.ManageCom = mParamInfo.getManageCom();
            mGlobalInput.ComCode = mGlobalInput.ManageCom;
            
            VData data = new VData();
            data.add(mGlobalInput);
            data.add(tLGWorkSchema);
            TaskInputBL tTaskInputBL = new TaskInputBL();
            if (!tTaskInputBL.submitData(data, ""))
            {
            	 mErrors.addOneError("生成保全工单失败"+tTaskInputBL.mErrors);
                return false;
            }
            mEdorAcceptNo = tTaskInputBL.getWorkNo();
            
        	return true;
        }
        
        private boolean addEdorItem(){
        	//校验能否添加保全项目
        	if(!checkEdorItem()){
        		return false;
        	}
        	MMap map = new MMap();
            mLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
            mLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
            mLPEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
            mLPEdorItemSchema.setDisplayType("1");
            mLPEdorItemSchema.setEdorType(mLPedorItemTable.getEdorType());
            mLPEdorItemSchema.setGrpContNo(BQ.GRPFILLDATA);
            mLPEdorItemSchema.setContNo(cLCContTable.getContNo());
            mLPEdorItemSchema.setEdorState("3");
            mLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
            mLPEdorItemSchema.setPolNo(BQ.FILLDATA);
            mLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
            mLPEdorItemSchema.setEdorValiDate(mLPedorItemTable.getEdorValidate());
            mLPEdorItemSchema.setEdorAppDate(mLgWorkTable.getAcceptDate());
            mLPEdorItemSchema.setReasonCode(mLPedorItemTable.getReasonCode());
            mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
            mLPEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
            mLPEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
            mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
            mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLPEdorItemSchema, "INSERT");
            
            mLPEdorMainSchema.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
            mLPEdorMainSchema.setContNo(mLPEdorItemSchema.getContNo());
            mLPEdorMainSchema.setEdorNo(mLPEdorMainSchema.
                    getEdorAcceptNo());
            mLPEdorMainSchema.setEdorAppNo(
                    mLPEdorMainSchema.getEdorAcceptNo());
            mLPEdorMainSchema.setContNo(cLCContTable.getContNo());
            mLPEdorMainSchema.setEdorAppDate(mLgWorkTable.getAcceptDate());
            mLPEdorMainSchema.setEdorValiDate(mLPedorItemTable.getEdorValidate());
            mLPEdorMainSchema.setEdorState("3");
            mLPEdorMainSchema.setUWState("0");
            mLPEdorMainSchema.setOperator(mGlobalInput.Operator);
            mLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
            mLPEdorMainSchema.setMakeDate(PubFun.getCurrentDate());
            mLPEdorMainSchema.setMakeTime(PubFun.getCurrentTime());
            mLPEdorMainSchema.setModifyDate(PubFun.getCurrentDate());
            mLPEdorMainSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLPEdorMainSchema, "INSERT");
            if (!submit(map))
            {
            	mErrors.addOneError("添加保全项目失败");
                return false;
            }
        	return true;
        }

        private boolean saveDetail(){
        	MMap map = new MMap();
        	String sql=" edorno='"+mEdorAcceptNo+"' and edortype='WT'";
        	map.put("delete from lppol where"+sql,"DELETE");
        	map.put("delete from lpcont where"+sql,"DELETE");
        	map.put("delete from lpinsured where"+sql,"DELETE");
        	//生成保全P表
        	LCPolDB tLCPolDB = new LCPolDB();
        	LCPolSet tLCPolSet = new LCPolSet();
        	Reflections tReflections = new Reflections();
        	tLCPolDB.setContNo(cLCContTable.getContNo());
        	tLCPolSet = tLCPolDB.query();
        	
        	if(tLCPolSet!=null&&tLCPolSet.size()>0){
        		for(int i=1;i<=tLCPolSet.size();i++){
        			LPPolSchema tLPPolSchema = new LPPolSchema();
        			tReflections.transFields(tLPPolSchema, tLCPolSet.get(i).getSchema());
        			
        			tLPPolSchema.setEdorNo(mEdorAcceptNo);
        			tLPPolSchema.setEdorType("WT");
        			mLPPolSet.add(tLPPolSchema);
        		}
        	}else{
        		mErrors.addOneError("查询险种信息失败");
        		return false;
        	}
        	
        	tReflections.transFields(mLPContschema, mLCContSchema);
        	mLPContschema.setEdorNo(mEdorAcceptNo);
        	mLPContschema.setEdorType("WT");
        	
            //若没有录入工本费信息，则删除之前可能存储了的工本费信息
            LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
            tLJSGetEndorseDB.setEndorsementNo(mLPEdorItemSchema.getEdorAcceptNo());
            tLJSGetEndorseDB.setFeeOperationType(mLPEdorItemSchema.getEdorType());
            tLJSGetEndorseDB.setFeeFinaType(BQ.FEEFINATYPE_GB);
            tLJSGetEndorseDB.setContNo(mLPEdorItemSchema.getContNo());
            map.put(tLJSGetEndorseDB.query(), "DELETE");

            LPEdorEspecialDataDB db = new LPEdorEspecialDataDB();
            db.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
            db.setEdorNo(mLPEdorItemSchema.getEdorNo());
            db.setEdorType(mLPEdorItemSchema.getEdorType());
            db.setPolNo(BQ.FILLDATA);
            db.setDetailType(BQ.DETAILTYPE_GB);
            map.put(db.query(), "DELETE");
        	
        	//修改保全状态
            mLPEdorItemSchema.setEdorState("1");
            mLPEdorItemSchema.setReasonCode("070");
            map.put(mLPEdorItemSchema,"UPDATE");
            map.put(mLPPolSet, SysConst.DELETE_AND_INSERT);
            map.put(mLPContschema, SysConst.DELETE_AND_INSERT);
            
            //提交数据库
        	if(!submit(map)){
        		return false;
        	}
        	return true;
        }
        
        /**
         * 产生打印数据
         * @return boolean
         */
        private boolean creatPrintVts()
        {
            //生成打印数据
            VData data = new VData();
            data.add(mGlobalInput);
            PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
                    mEdorAcceptNo);
            if (!tPrtAppEndorsementBL.submitData(data, ""))
            {
            	 mErrors.addOneError("数据保存成功！但没有生成保全服务批单！");                	
                return false;
            }
            return true;
        }
        
        private boolean appConfirm(){
        	 //生成退费信息
        	 for(int i=1;i<=mLPPolSet.size();i++){
                 //查询是否发生了加费
                 double sumAddFee = getSumAddFee(mLPPolSet.get(i).getPolNo());
                 
                 double supplementaryPrem =mLPPolSet.get(i).getSupplementaryPrem();
                 
                 //生成批改交退费表
                 //退保累计交费
                 LJSGetEndorseBL tLJSGetEndorseBL = new LJSGetEndorseBL();
                 LJSGetEndorseSchema tLJSGetEndorseSchema = new
                         LJSGetEndorseSchema();
                 tLJSGetEndorseSchema.setGetNoticeNo(mLPEdorItemSchema.getEdorNo());
                 tLJSGetEndorseSchema.setEndorsementNo(mLPEdorItemSchema.
                         getEdorNo());
                 tLJSGetEndorseSchema.setFeeOperationType(mLPEdorItemSchema.
                         getEdorType());
                 tLJSGetEndorseSchema.setGetDate(mLPEdorItemSchema.
                                                 getEdorValiDate());
                 tLJSGetEndorseSchema.setAppntNo(mLPPolSet.get(i).getAppntNo());
                 tLJSGetEndorseSchema.setInsuredNo(mLPPolSet.get(i).getInsuredNo());
                 tLJSGetEndorseSchema.setRiskCode(mLPPolSet.get(i).getRiskCode());
                 tLJSGetEndorseSchema.setRiskVersion(
                		 mLPPolSet.get(i).getRiskVersion());
                 tLJSGetEndorseSchema.setAgentCom(mLPPolSet.get(i).getAgentCom());
                 tLJSGetEndorseSchema.setAgentType(mLPPolSet.get(i).getAgentType());
                 tLJSGetEndorseSchema.setPolType(mLPPolSet.get(i).getPolTypeFlag());
                 tLJSGetEndorseSchema.setGrpContNo(
                		 mLPPolSet.get(i).getGrpContNo());
                 tLJSGetEndorseSchema.setApproveCode(
                		 mLPPolSet.get(i).getApproveCode());
                 tLJSGetEndorseSchema.setApproveDate(
                		 mLPPolSet.get(i).getApproveDate());
                 tLJSGetEndorseSchema.setApproveTime(
                		 mLPPolSet.get(i).getApproveTime());
                 tLJSGetEndorseSchema.setHandler(mLPPolSet.get(i).getHandler());
                 tLJSGetEndorseSchema.setContNo(mLPPolSet.get(i).getContNo());
                 tLJSGetEndorseSchema.setGrpPolNo(mLPPolSet.get(i).getGrpPolNo());
                 tLJSGetEndorseSchema.setPolNo(mLPPolSet.get(i).getPolNo());
                 tLJSGetEndorseSchema.setAppntNo(mLPPolSet.get(i).getAppntNo());
                 tLJSGetEndorseSchema.setInsuredNo(mLPPolSet.get(i).getInsuredNo());
                 tLJSGetEndorseSchema.setAppntNo(mLPPolSet.get(i).getAppntNo());
                 tLJSGetEndorseSchema.setKindCode(mLPPolSet.get(i).getKindCode());
                 tLJSGetEndorseSchema.setRiskCode(mLPPolSet.get(i).getRiskCode());
                 tLJSGetEndorseSchema.setRiskVersion(mLPPolSet.get(i).
                         getRiskVersion());
                 tLJSGetEndorseSchema.setAgentCom(mLPPolSet.get(i).getAgentCom());
                 tLJSGetEndorseSchema.setAgentCode(mLPPolSet.get(i).getAgentCode());
                 tLJSGetEndorseSchema.setAgentType(mLPPolSet.get(i).getAgentType());
                 tLJSGetEndorseSchema.setAgentGroup(mLPPolSet.get(i).
                         getAgentGroup());
                 tLJSGetEndorseSchema.setGetMoney(
                         -(mLPPolSet.get(i).getPrem() - sumAddFee));//扣除加费和
                 //从描述表中获取财务接口类型
                 String finType = BqCalBL.getFinType(mLPEdorItemSchema.
                         getEdorType(),
                         "TF",
                         mLPEdorItemSchema.getPolNo());
                 if (finType.equals(""))
                 {
                     // @@错误处理
                	 mErrors.addOneError("在LDCode1中缺少保全财务接口转换类型编码!");                	 
                     return false;
                 }
                 tLJSGetEndorseSchema.setFeeFinaType(finType);
                 tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
                 tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
                 tLJSGetEndorseSchema.setOtherNo(mLPEdorItemSchema.getEdorNo());
                 tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_P);
                 tLJSGetEndorseSchema.setPolType("1");
                 tLJSGetEndorseSchema.setSerialNo("");
                 tLJSGetEndorseSchema.setGetFlag("1");
                 tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
                 tLJSGetEndorseSchema.setManageCom(mLPPolSet.get(i).getManageCom());
                 tLJSGetEndorseBL.setSchema(tLJSGetEndorseSchema);
                 tLJSGetEndorseBL.setDefaultFields();

                 //生理赔的险种不退钱
                 GCheckClaimBL tGCheckClaimBL = new GCheckClaimBL();
                 if(tGCheckClaimBL.checkClaimed(tLJSGetEndorseBL.getPolNo()))
                 {
                     //tLJSGetEndorseBL.setGetMoney(0);
                 }

                 mLJSGetEndorseSet.add(tLJSGetEndorseBL.getSchema());

                 //若发生加费，则生成加费的退费财务接口数据：
                 //销售提取佣金的时候，需去除加费和后再算佣金，
                 //在此以PAYPLANCODE_ADDFEE做标识
                 LJSGetEndorseSchema tLJSGetEndorseSchemaAddFee = null;
                 if(sumAddFee != 0)
                 {
                     tLJSGetEndorseSchemaAddFee = tLJSGetEndorseBL.getSchema();
                     tLJSGetEndorseSchemaAddFee.
                         setPayPlanCode(BQ.PAYPLANCODE_ADDFEE);
                     tLJSGetEndorseSchemaAddFee.setGetMoney(-sumAddFee);
                     mLJSGetEndorseSet.add(tLJSGetEndorseSchemaAddFee);
                 }
                 if(supplementaryPrem!=0){
                 	tLJSGetEndorseSchemaAddFee = tLJSGetEndorseBL.getSchema();
                     tLJSGetEndorseSchemaAddFee.
                         setPayPlanCode(BQ.PAYPLANCODE_SUPPLEMENTARY);
                     tLJSGetEndorseSchemaAddFee.setGetMoney(-supplementaryPrem);
                     mLJSGetEndorseSet.add(tLJSGetEndorseSchemaAddFee);
                 }



                 //主表信息更新
                 mLPEdorItemSchema.setChgPrem(
                     mLPEdorItemSchema.getChgPrem()
                     + tLJSGetEndorseSchema.getGetMoney()
                     + (tLJSGetEndorseSchemaAddFee == null ? 0
                        : tLJSGetEndorseSchemaAddFee.getGetMoney()));
                 mLPEdorItemSchema.setGetMoney(
                     mLPEdorItemSchema.getGetMoney()
                     + tLJSGetEndorseSchema.getGetMoney()
                     + (tLJSGetEndorseSchemaAddFee == null ? 0
                        : tLJSGetEndorseSchemaAddFee.getGetMoney()));
                 
        	 }
        	 
        	 mLPEdorItemSchema.setEdorState("2");
        	 mLPEdorItemSchema.setUWFlag("0");
        	 
        	 mLPEdorMainSchema.setEdorState("2");
        	 mLPEdorMainSchema.setGetMoney(mLPEdorItemSchema.getGetMoney());
        	 
        	 String updateApp = "update lpedorapp set edorstate='2',getmoney='"+mLPEdorItemSchema.getGetMoney()+"',modifydate=current date,modifytime=current time where edoracceptno='"+mEdorAcceptNo+"' ";
        	 
        	 MMap map = new MMap();
        	 map.put(mLJSGetEndorseSet, "DELETE&INSERT");
        	 map.put(mLPEdorItemSchema, "UPDATE");
        	 map.put(mLPEdorMainSchema, "UPDATE");
        	 map.put(updateApp, "UPDATE");
        	 
             //提交数据库
         	if(!submit(map)){
         		return false;
         	}        	 
        	 
        	return true;
        }
        
        /**
         *  查询险种polNo发生的加费和
         * @param polNo String
         * @return double
         */
        private double getSumAddFee(String polNo)
        {
            String sql = "select sum(SumActuPayMoney) "
                         + "from LJAPayPerson "
                         + "where payPlanCode like '000000%' "
                         + "    and  polNo = '" + polNo+ "' ";
            ExeSQL e = new ExeSQL();
            String sumActuPayMoney = e.getOneValue(sql);

            return sumActuPayMoney.equals("") ? 0
                    : Double.parseDouble(sumActuPayMoney);
        }
        
        private boolean checkEdorItem(){
        	//已生效的保单不能通过接口退保
//        	if(CommonBL.stringToDate(mLCContSchema.getCValiDate()).before(CommonBL.stringToDate(PubFun.getCurrentDate()))){
//        		mErrors.addOneError("已生效的保单不能通过接口进行犹豫期退保");
//        		return false;
//        	}
        	//正在操作保全的无法添加犹豫期退保
        	String edorSQL = " select edoracceptno from lpedoritem where contno='"+mLCContSchema.getContNo()+"' and edoracceptno!='"+mEdorAcceptNo+"' " +
        			" and exists (select 1 from lpedorapp where edoracceptno=lpedoritem.edoracceptno and edorstate!='0') with ur  ";
        	ExeSQL tExeSQL =new ExeSQL();
        	String edorFlag = tExeSQL.getOneValue(edorSQL);
        	if(edorFlag!=null&&!"".equals(edorFlag)){
        		mErrors.addOneError("保单"+mLCContSchema.getContNo()+"正在操作保全，工单号为："+edorFlag+"无法做犹豫期退保");
        		return false;
        	}
        	//只有承保有效状态下的保单才能犹豫期退保
        	if((!"1".equals(mLCContSchema.getAppFlag()))||"2".equals(mLCContSchema.getStateFlag())||"3".equals(mLCContSchema.getStateFlag())){
        		mErrors.addOneError("保单只有在签单并承保有效的状态下才能犹豫期退保");
        		return false;
        	}
        	return true;
        }

        /**
         * 提交数据到数据库
         * @return boolean
         */
        private boolean submit(MMap map)
        {
            VData data = new VData();
            data.add(map);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(data, ""))
            {
            	mErrors.addOneError("提交数据库发生错误"+tPubSubmit.mErrors);
                return false;
            }
            return true;
        }
        
        /**
         * 保全确认
         * @param edorAcceptNo String
         * @return boolean
         */
        private boolean edorConfirm()
        {
            MMap map = new MMap();
            
            //生成财务数据
            FinanceDataBL tFinanceDataBL = new FinanceDataBL(mGlobalInput, mEdorAcceptNo,
            		BQ.NOTICETYPE_P, "");
            if (!tFinanceDataBL.submitData())
            {
            	mErrors.addOneError("生成财务数据错误"+tFinanceDataBL.mErrors);
                return false;
            }
            
            //个人保全结案
            PEdorConfirmBL tPEdorConfirmBL =
                new PEdorConfirmBL(mGlobalInput, mEdorAcceptNo);
             map = tPEdorConfirmBL.getSubmitData();
            if (map == null)
            {
            	mErrors.addOneError("保全结案发生错误"+tFinanceDataBL.mErrors);
                return false;
            }            
            
            //工单结案
            LGWorkSchema tLGWorkSchema = new LGWorkSchema();
            tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
            tLGWorkSchema.setTypeNo("03"); //结案状态

            VData data = new VData();
            data.add(mGlobalInput);
            data.add(tLGWorkSchema);
            TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
            MMap tmap = tTaskAutoFinishBL.getSubmitData(data, "");
            if (tmap == null)
            {
            	mErrors.addOneError("工单结案失败"+tFinanceDataBL.mErrors);
                return false;
            }
            map.add(tmap);
            if (!submit(map))
            {
                return false;
            }
            return true;
        }
        
        private boolean endEdor(){
	    	System.out.println("交退费通知书" + mEdorAcceptNo);
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("payMode","12");	
			tTransferData.setNameAndValue("endDate", "");
			tTransferData.setNameAndValue("payDate", PubFun.getCurrentDate());
			tTransferData.setNameAndValue("bank", mLCContSchema.getBankCode());
			tTransferData.setNameAndValue("bankAccno", mLCContSchema.getBankAccNo());
			tTransferData.setNameAndValue("accName", mLCContSchema.getAccName());
			tTransferData.setNameAndValue("chkYesNo", "no");
  		
  		//生成交退费通知书
			FeeNoticeVtsUI tFeeNoticeVtsUI = new FeeNoticeVtsUI(mEdorAcceptNo);
			if (!tFeeNoticeVtsUI.submitData(tTransferData))
			{
				mErrors.addOneError("生成批单失败！原因是："+tFeeNoticeVtsUI.getError());
			} 		

			VData data = new VData();
			data.add(mGlobalInput);
			data.add(tTransferData);
			SetPayInfo spi = new SetPayInfo(mEdorAcceptNo);
			if (!spi.submitDate(data, "0"))
			{
				System.out.println("设置收退费方式失败！");
				mErrors.addOneError("设置收退费方式失败！原因是："+spi.mErrors.getFirstError());
			}
			
        	return true;
        }

}
