package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.LPBnfDBSet;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全减人存申请</p>
 * <p>Description: 把保全状态置为申请确认状态</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GEdorZFDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpcontNo = null;

    private Date mCValiDate = null;

    private Date mLastPayToDate = null;

    private Date mCurPayToDate = null;

    private String mMessage = "";
    /** 传出数据的容器 */
    private VData mResult = new VData();

    
    private LPGrpContSet mLPGrpContSet = null;
    
    private String mGrpContNo=null;
    
    private String mDeathDate=null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data,String cOperate)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!prepareData()) {
            return false;
        }
        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, "")) { //数据提交
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorCTDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("PEdorZFDetailBL End PubSubmit");
        return true;
       
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareData() {
        mResult.clear();
        mResult.add(mMap);
        return true;
    }
    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到输入数据
     * @param data VData
     * @return boolean
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            
            LPEdorItemSchema tLPEdorItemSchema = (LPEdorItemSchema) data.
                    getObjectByObjectName("LPEdorItemSchema", 0);
            
           
            
            mEdorNo = tLPEdorItemSchema.getEdorNo();
            mEdorType = tLPEdorItemSchema.getEdorType();
            mGrpcontNo = tLPEdorItemSchema.getGrpContNo();
            
            mLPGrpContSet = (LPGrpContSet)data.
            getObjectByObjectName("LPGrpContSet", 0);
            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 校验传入的数据的合法性
     * @return boolean
     */
    private boolean checkData()
    {
        if (!checkEdorValiDate())
        {
            return false;
        }
        
       
        return true;
    }

   

     /**
      * 检查保全生效日期
      * @return boolean
      */
     private boolean checkEdorValiDate()
     {
    	  return true;
     }
     

   

   



    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return boolean 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (!checkEdorValiDate())
        {
            return false;
        }
        //修改批次等一些列表
        setEdorState(BQ.EDORSTATE_INPUT);
        
        //修改 lpperson中的 deathdate
        setLPPerson();
        
       
        return true;
    }

   

	private void setLPPerson() {
		LCGrpContDB  tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mGrpcontNo);
		
		LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
		if(tLCGrpContSet.size()==1){
			LPGrpContSchema tLPGrpContSchema = new LPGrpContSchema();
			Reflections ref = new Reflections();
			ref.transFields(tLPGrpContSchema, tLCGrpContSet.get(1));
			tLPGrpContSchema.setEdorNo(mEdorNo);
			tLPGrpContSchema.setEdorType(mEdorType);
			tLPGrpContSchema.setOperator(mGlobalInput.Operator);
			
			tLPGrpContSchema.setModifyDate(PubFun.getCurrentDate());
			tLPGrpContSchema.setModifyTime(PubFun.getCurrentTime());
			mMap.put(tLPGrpContSchema, "DELETE&INSERT");
		}else {
			mErrors.addOneError("数据库中数据异常");
		}
	}

	/**
     * 把保全状态设为已申请状态
     * @return boolean
     */
    private void setEdorState(String edorState)
    {
        String sql;
       
        String sql3;
        
        String sql5;
        
        //更新main表
        sql = "update LPGrpEdorMain " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        
               

        //更新item表
        sql3 = "update LPGrpEdorItem  " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        
        mMap.put(sql3, "UPDATE");
        
       
        
        //修正lpedorapp
        sql5 = "update LPEdorapp  " +
        "set EdorState = '" + edorState + "', " +
        "    Operator = '" + mGlobalInput.Operator + "', " +
        "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
        "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
        "where edoracceptno = '" + mEdorNo + "' " ;
        mMap.put(sql5, "UPDATE");
    }

   

    public VData getResult()
    {
        return mResult;
    }
    
 
}
