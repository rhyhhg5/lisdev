package com.sinosoft.lis.bq;

import java.util.Date;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全个单新收费方式变更</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * author Lanjun, QiuYang
 * @version 1.0
 */

public class PEdorBFDetailBL {
  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  private MMap mMap = new MMap();

  private GlobalInput mGlobalInput = null;

  private DetailDataQuery mQuery = null;

  private String mOperator = null;

  private String mEdorNo = null;

  private String mEdorType = "BF";

  private String mContNo = null;

  private String mEdorValiDate = null;

  private LCPolSet mLCPolSet = new LCPolSet();

  private EdorItemSpecialData mSpecialData = null;

  private String mCurrentDate = PubFun.getCurrentDate();

  private String mCurrentTime = PubFun.getCurrentTime();

  private DisabledManageBL tDisabledManageBL = new DisabledManageBL();
  LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

  /**
   * 数据提交的公共方法
   * @param cInputData VData
   * @return boolean
   */
  public boolean submitData(VData cInputData, String operator) {
    this.mOperator = operator;
    if (mOperator == null) {
      mErrors.addOneError("缺少控制标志！");
      return false;
    }

    if (!getInputData(cInputData)) {
      return false;
    }

    if (!checkData()) {
      return false;
    }

    if (!dealData()) {
      return false;
    }

    if (!submit()) {
      return false;
    }
    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param cInputData VData
   */
  private boolean getInputData(VData cInputData) {
    try {
      mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
          "GlobalInput", 0);
      mLCPolSet = (LCPolSet) cInputData.getObjectByObjectName(
          "LCPolSet", 0);
      mSpecialData = (EdorItemSpecialData) cInputData.getObjectByObjectName(
          "EdorItemSpecialData", 0);
      LPEdorItemSchema tLPEdorItemSchema = (LPEdorItemSchema)
          cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
      mLPEdorItemSchema = tLPEdorItemSchema;

      mEdorNo = tLPEdorItemSchema.getEdorNo();
      mContNo = tLPEdorItemSchema.getContNo();
      LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
      tLPEdorItemDB.setEdorNo(mEdorNo);
      tLPEdorItemDB.setEdorType(mEdorType);
      LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
      if (tLPEdorItemSet.size() == 0) {
        mErrors.addOneError("未找到项目申请信息！");
        return false;
      }
      mEdorValiDate = tLPEdorItemSet.get(1).getEdorValiDate();
      mQuery = new DetailDataQuery(mEdorNo, mEdorType);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      mErrors.addOneError("传入参数错误！");
      return false;
    }
    return true;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    if (mOperator.equals("ADD")) {
      for (int i = 1; i <= mLCPolSet.size(); i++) {
        LCPolSchema tLCPolSchema = mLCPolSet.get(i);
        LPPolSchema tLPPolSchema = (LPPolSchema)
            mQuery.getDetailData("LCPol", tLCPolSchema.getPolNo());
        tLPPolSchema.setAmnt(0);
        tLPPolSchema.setMult(0);
//        tLPPolSchema.setInsuredAppAge(PubFun.calInterval(tLCPolSchema.getInsuredBirthday(),
//        		tLCPolSchema.getPolApplyDate(), "Y"));
        tLPPolSchema.setOperator(mGlobalInput.Operator);
        tLPPolSchema.setMakeDate(mCurrentDate);
        tLPPolSchema.setMakeTime(mCurrentTime);
        tLPPolSchema.setModifyDate(mCurrentDate);
        tLPPolSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPPolSchema, "DELETE&INSERT");
      }
    }
    else if (mOperator.equals("DEL")) {

      for (int i = 1; i <= mLCPolSet.size(); i++) {
        System.out.println("ggggggg");
        LCPolSchema tLCPolSchema = mLCPolSet.get(i);
        String insuredNo = tLCPolSchema.getInsuredNo();
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        tLPPolDB.setEdorType(mEdorType);
        tLPPolDB.setInsuredNo(insuredNo);
        System.out.println(mLCPolSet.size());
        System.out.println(tLPPolDB.query().size());
        if (getInsuredSize(mLCPolSet, insuredNo) == tLPPolDB.query().size()) {
          String delSql = "delete from LPCustomerImpartParams " +
              "where EdorNo = '" + mEdorNo + "' " +
              "and EdorType = '" + mEdorType + "' " +
              "and CustomerNo = '" + insuredNo + "' " +
              "and " + i + " = " + i;
          mMap.put(delSql, "DELETE");
          System.out.println(delSql);
          delSql = "delete from LPCustomerImpart " +
              "where EdorNo = '" + mEdorNo + "' " +
              "and EdorType = '" + mEdorType + "' " +
              "and CustomerNo = '" + insuredNo + "' " +
              "and " + i + " = " + i;
          mMap.put(delSql, "DELETE");
          System.out.println(delSql);
        }
        String sql = "delete from LPPol " +
            "where PolNo = '" + tLCPolSchema.getPolNo() + "' " +
            "and EdorNo = '" + mEdorNo + "' " +
            "and EdorType = '" + mEdorType + "'";
        System.out.println(sql);
        mMap.put(sql, "DELETE");
      }
    }
    else if (mOperator.equals("SAVE")) {
      for (int i = 1; i <= mLCPolSet.size(); i++) {
        LCPolSchema tLCPolSchema = mLCPolSet.get(i);
        LPPolSchema tLPPolSchema = (LPPolSchema)
            mQuery.getDetailData("LCPol", tLCPolSchema.getPolNo());
        tLPPolSchema.setAmnt(tLCPolSchema.getAmnt());
        tLPPolSchema.setMult(tLCPolSchema.getMult());
        
    	  LCPolDB tLCPolDB =new LCPolDB();
    	  tLCPolDB.setPolNo(tLCPolSchema.getPolNo());
    	  tLCPolDB.getInfo();
    	  LCPolSchema ttLCPolSchema = new LCPolSchema();
    	  ttLCPolSchema.setSchema(tLCPolDB.getSchema());
        
        tLPPolSchema.setInsuredAppAge(PubFun.calInterval(ttLCPolSchema.getInsuredBirthday(),
        		ttLCPolSchema.getPolApplyDate(), "Y"));
        tLPPolSchema.setOperator(mGlobalInput.Operator);
        tLPPolSchema.setMakeDate(mCurrentDate);
        tLPPolSchema.setMakeTime(mCurrentTime);
        tLPPolSchema.setModifyDate(mCurrentDate);
        tLPPolSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPPolSchema, "DELETE&INSERT");
      }
    }
    mMap.put(mSpecialData.getSpecialDataSet(), "DELETE&INSERT");
    setEdorState(BQ.EDORSTATE_INPUT);
    return true;
  }

  /**
   * 得到LCPolSet中某一被保人的个数
   * @param aLCPolSet LCPolSet
   * @param insuredNo String
   * @return int
   */
  private int getInsuredSize(LCPolSet aLCPolSet, String insuredNo) {
    if (insuredNo == null) {
      throw new RuntimeException();
    }
    int num = 0;
    for (int i = 1; i <= aLCPolSet.size(); i++) {
      LCPolSchema tLCPolSchema = aLCPolSet.get(i);
      if (insuredNo.equals(tLCPolSchema.getInsuredNo())) {
        num++;
      }
    }
    return num;
  }

  private boolean checkData() {
    if (mOperator.equals("SAVE")) {
      for (int i = 1; i <= mLCPolSet.size(); i++) {
        LCPolSchema tLCPolSchema = mLCPolSet.get(i);
        double amnt = tLCPolSchema.getAmnt();
        double mult = tLCPolSchema.getMult();
        LCPolSchema tOldLCPolSchema = CommonBL.getLCPol(tLCPolSchema.getPolNo());
        if (tOldLCPolSchema == null) {
          mErrors.addOneError("找不到险种信息！");
          return false;
        }
        String riskSeqNo = tOldLCPolSchema.getRiskSeqNo();
        double oldAmnt = tOldLCPolSchema.getAmnt();
        double oldMult = tOldLCPolSchema.getMult();
        if (amnt < 0) {
          mErrors.addOneError("序号" + riskSeqNo + "险种保额不能小于零！");
          return false;
        }
        if ( (oldMult > 0) && (mult <= 0)) {
          mErrors.addOneError("序号" + riskSeqNo + "险种档次必须大于零！");
          return false;
        }
        if ( (oldMult == 0) && (amnt == oldAmnt)) {
          mErrors.addOneError("序号" + riskSeqNo +
                              "险种保额未发生改变！");
          return false;

        }
        if ( (oldMult > 0) && (mult == oldMult)) {
          mErrors.addOneError("序号" + riskSeqNo +
                              "险种档次未发生改变！");
          return false;
        }

        mSpecialData.setPolNo(tLCPolSchema.getPolNo());
        String validateTime = mSpecialData.getEdorValue("ValiDateTime");
        if ( (validateTime == null) || (validateTime.equals(""))) {
          mErrors.addOneError("序号" + riskSeqNo +
                              "险种未录入生效时间！");
          return false;
        }
        if (! (validateTime.equals("1") || (validateTime.equals("2")))) {
          mErrors.addOneError("序号" + riskSeqNo +
                              "险种生效时间录入不正确 ！");
          return false;
        }

        //校验保全生效日期
        Date payToDate = CommonBL.stringToDate(tOldLCPolSchema.getPaytoDate());
        if (payToDate == null) {
          mErrors.addOneError("未找到序号" + riskSeqNo + "险种交至日期！");
          return false;
        }
        Date lastPayToDate = CommonBL.stringToDate(CommonBL.getLastPayToDate(
            tOldLCPolSchema.getPolNo(), tOldLCPolSchema.getPaytoDate()));
        if (lastPayToDate == null) {
          mErrors.addOneError("未找到序号" + riskSeqNo + "险种交费起始日期！");
          return false;
        }
        Date edorValidate = CommonBL.stringToDate(mEdorValiDate);
        if (edorValidate == null) {
          mErrors.addOneError("未找到保全生效日期！");
          return false;
        }
        if (edorValidate.before(lastPayToDate)) {
          mErrors.addOneError("保全生效日不能早于序号" + riskSeqNo +
                              "险种的交费起始日期！");
          return false;
        }
        if (edorValidate.after(payToDate)) {
          mErrors.addOneError("保全生效日不能晚于序号" + riskSeqNo +
                              "险种的交费截止日期！");
          return false;
        }

        //校验是否录入健康告知
        if ( (amnt > oldAmnt) || (mult > oldMult)) {
        //qulq 对于健管计划的档次调整和新增险种都不需要进行健康告知。见CQ5412
	//查询险种是否是健管
	String sql = "Select 1 From LMRiskApp Where RiskType = 'M' and RiskCode ='"+tOldLCPolSchema.getRiskCode()+"'";
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        LMRiskAppSet resultSet = tLMRiskAppDB.executeQuery(sql);
        if(resultSet==null||resultSet.size()==0)
        {
            LPCustomerImpartDB tLPCustomerImpartDB = new
                    LPCustomerImpartDB();
            tLPCustomerImpartDB.setEdorNo(mEdorNo);
            tLPCustomerImpartDB.setCustomerNo(tLCPolSchema.getInsuredNo());
            if (tLPCustomerImpartDB.query().size() == 0) {
                mErrors.addOneError("请录入被保人" +
                                    tOldLCPolSchema.getInsuredName() + "的健康告知！");
                return false;
            }
        }
        }
      }

      //校验满期失效
      Reflections ref = new Reflections();
      LCPolDB tLCPolDB = new LCPolDB();
      LPPolSet tLPPolSet = new LPPolSet();
      for(int i = 1; i <= mLCPolSet.size(); i++)
      {
          LPPolSchema schema = new LPPolSchema();

          ref.transFields(schema, mLCPolSet.get(i));
          schema.setEdorNo(mLPEdorItemSchema.getEdorNo());
          schema.setEdorType(mLPEdorItemSchema.getEdorType());
          tLPPolSet.add(schema);
      }

      //add by luomin
      if (!tDisabledManageBL.dealDisabledpol(mLCPolSet, "BF")
          || !tDisabledManageBL.dealDisabledPolCompred(tLPPolSet)) {
        CError tError = new CError();
        tError.moduleName = "PEdorItemBL";
        tError.functionName = "checkData";
        if(!"".equals(tDisabledManageBL.getErrInfo()))
        {
            tError.errorMessage = tDisabledManageBL.getErrInfo();
        }

        else if (tDisabledManageBL.getState().equals(BQ.STATE_FLAG_AVAILABLE))
        {
          tError.errorMessage = tDisabledManageBL.getInsuredno() +
              "号被保人投报险种'" + tDisabledManageBL.getRiskcode() +
              "'下BF项目加保在险种失效状态下不能添加!";
        }
        else if (tDisabledManageBL.getState().equals(BQ.STATE_FLAG_TERMINATE)) {
          tError.errorMessage = tDisabledManageBL.getInsuredno() +
              "号被保人投报险种'" + tDisabledManageBL.getRiskcode() +
              "'下BF项目在终止状态下不能添加!";
        }

        this.mErrors.addOneError(tError);
        return false;

      }
    }
    return true;
  }

  /**
   * 把保全状态设为已录入
   * @param edorState String
   */
  private void setEdorState(String edorState) {
    String sql = "update  LPEdorItem " +
        "set EdorState = '" + edorState + "', " +
        "    Operator = '" + mGlobalInput.Operator + "', " +
        "    ModifyDate = '" + mCurrentDate + "', " +
        "    ModifyTime = '" + mCurrentTime + "' " +
        "where  EdorNo = '" + mEdorNo + "' " +
        "and EdorType = '" + mEdorType + "' " +
        "and ContNo = '" + mContNo + "'";
    mMap.put(sql, "UPDATE");
  }

  /**
   * 提交数据到数据库
   * @return boolean
   */
  private boolean submit() {
    VData data = new VData();
    data.add(mMap);
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(data, "")) {
      mErrors.copyAllErrors(tPubSubmit.mErrors);
      return false;
    }
    return true;
  }
}
