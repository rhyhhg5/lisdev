package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

import java.util.*;
import java.sql.Connection;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单遗失补发申请确认</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class GrpEdorBBAppConfirmBL implements EdorAppConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;
    
    MMap map = new MMap();

//    private LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
    private LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
    private LPEdorEspecialDataSet tLPEdorEspecialDataSet = new LPEdorEspecialDataSet();
    private LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private String mGBFee = null;  //工本费

    /** 全局数据 */
    //private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    private GlobalInput mGlobalInput = new GlobalInput();

    public GrpEdorBBAppConfirmBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括""和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //数据准备操作
        if (!prepareData())
        {
            return false;
        }

        return true;
    }
    
    
    /**
     * 设置批改补退费表
     * @return boolean
     */
//    private LJSGetEndorseSchema  setGetEndorse(LCGrpContSchema aLCGrpContSchema, double edorPrem)
    private void  setGetEndorse(LCGrpContSchema aLCGrpContSchema, double edorPrem)
    {
//        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        //得到lcgrppolSet
//        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
//        tLCGrpPolDB.setGrpContNo(aLCGrpContSchema.getGrpContNo());
//        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
//        tLCGrpPolSet = tLCGrpPolDB.query();
        ExeSQL aExeSQL = new ExeSQL();
        String sqlw22 = "select count(1)  from ljapaygrp where payno=(select payno from ljapay "
		+ " where incometype='1' and duefeetype='0' and incomeno='"+aLCGrpContSchema.getGrpContNo()+"')";
        int xznum = Integer.parseInt(aExeSQL.getOneValue(sqlw22));
        //得到每个grppolno对应的首期费用。用数组盛放，其中数组最后一个存放总数。
        double prem = 0;
//        for(int i=1;i<=xznum;i++){
//        	String sqlw = "select (case when sumduepaymoney>0 then sumduepaymoney else 0 end)  from ljapaygrp where payno=(select payno from ljapay "
//        			+ " where incometype='1' and duefeetype='0' and incomeno='"+aLCGrpContSchema.getGrpContNo()+"') and grppolno='"+tLCGrpPolSet.get(i).getGrpPolNo()+"' "; 
//        	try{
//        	prem +=Double.parseDouble(aExeSQL.getOneValue(sqlw));
//        	}catch(Exception e){
//        		 System.out.println( "团单下此险种首期数据查询失败");
//        	}
//        }
        //得到首期总保费
      	String sqlw = "select  sum(sumduepaymoney)  from ljapaygrp where payno=(select payno from ljapay "
      			+ " where incometype='1' and duefeetype='0' and incomeno='"+aLCGrpContSchema.getGrpContNo()+"')  "; 
      	prem =Double.parseDouble(aExeSQL.getOneValue(sqlw));
//      	查找到
      	String sqlw1 = "select grppolno,riskcode, sumduepaymoney  from ljapaygrp where payno=(select payno from ljapay "
      			+ " where incometype='1' and duefeetype='0' and incomeno='"+aLCGrpContSchema.getGrpContNo()+"')  ";
      	SSRS tSSRS = aExeSQL.execSQL(sqlw1);
      	
        LJSGetEndorseSet tLJSGetEndorseSet = new LJSGetEndorseSet();
        double money = edorPrem;
        for(int i=1;i<=tSSRS.getMaxRow();i++){
        	LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        	tLJSGetEndorseSchema.setGetNoticeNo(mLPGrpEdorItemSchema.getEdorNo()); //给付通知书号码，没意义
            tLJSGetEndorseSchema.setEndorsementNo(mLPGrpEdorItemSchema.getEdorNo());
            tLJSGetEndorseSchema.setFeeOperationType(mLPGrpEdorItemSchema.getEdorType());
            tLJSGetEndorseSchema.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
            tLJSGetEndorseSchema.setContNo(BQ.FILLDATA);
            tLJSGetEndorseSchema.setGrpPolNo(tSSRS.GetText(i, 1));
            tLJSGetEndorseSchema.setPolNo(tSSRS.GetText(i, 1));
            tLJSGetEndorseSchema.setGetDate(mLPGrpEdorItemSchema.getEdorValiDate());
            if(i != tSSRS.getMaxRow()){
//            	String sqlw3 = "select (case when sumduepaymoney>0 then sumduepaymoney else 0 end)  from ljapaygrp where payno=(select payno from ljapay "
//            			+ " where incometype='1' and duefeetype='0' and incomeno='"+aLCGrpContSchema.getGrpContNo()+"') and grppolno='"+tSSRS.GetText(1, i)+"' "; 
//            	double money1 = Double.parseDouble(aExeSQL.getOneValue(sqlw1));
            	double money1 = Double.parseDouble(tSSRS.GetText(i, 3));
            	double amoney = edorPrem*money1/prem;
            	tLJSGetEndorseSchema.setGetMoney(amoney);
            	money-=amoney;
            }else {
            	tLJSGetEndorseSchema.setGetMoney(money);
            }
            
            tLJSGetEndorseSchema.setRiskCode(tSSRS.GetText(i, 2));
//            tLJSGetEndorseSchema.setRiskVersion(tLCGrpPolSet.get(i).getRiskVersion());
            tLJSGetEndorseSchema.setRiskVersion(BQ.FILLDATA);
            tLJSGetEndorseSchema.setAgentCom(aLCGrpContSchema.getAgentCom());
            tLJSGetEndorseSchema.setAgentType(aLCGrpContSchema.getAgentType());
            tLJSGetEndorseSchema.setAgentCode(aLCGrpContSchema.getAgentCode());
            tLJSGetEndorseSchema.setAgentGroup(aLCGrpContSchema.getAgentGroup());
            tLJSGetEndorseSchema.setApproveCode(aLCGrpContSchema.getApproveCode());
            tLJSGetEndorseSchema.setApproveDate(aLCGrpContSchema.getApproveDate());
            tLJSGetEndorseSchema.setApproveTime(aLCGrpContSchema.getApproveTime());
            tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_BF);
            tLJSGetEndorseSchema.setAppntNo(aLCGrpContSchema.getAppntNo());
            tLJSGetEndorseSchema.setInsuredNo(BQ.FILLDATA);
            tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
            tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
            tLJSGetEndorseSchema.setOtherNo(mLPGrpEdorItemSchema.getEdorNo());
            tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
            tLJSGetEndorseSchema.setGetFlag("0");
            tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
            tLJSGetEndorseSchema.setManageCom(aLCGrpContSchema.getManageCom());
            tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
            tLJSGetEndorseSet.add(tLJSGetEndorseSchema);
        }
        map.put(tLJSGetEndorseSet, SysConst.DELETE_AND_INSERT);
//        return tLJSGetEndorseSchema;
    }


    public VData getResult()
    {
        return mResult;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) mInputData.
                                   getObjectByObjectName("LPGrpEdorItemSchema",
                    0);
            mGlobalInput.setSchema((GlobalInput) mInputData.
                                   getObjectByObjectName("GlobalInput", 0));
        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        if (mLPGrpEdorItemSchema == null || mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "GrpEdorLRAppConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1)
        {
            mErrors.copyAllErrors(tLPGrpEdorItemDB.mErrors);
            mErrors.addOneError(new CError("查询保全项目信息失败！"));
            return false;
        }
        mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemSet.get(1));
        
        tLPEdorEspecialDataDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPEdorEspecialDataDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPEdorEspecialDataDB.setDetailType("BBMONEY"); 
        tLPEdorEspecialDataSet = tLPEdorEspecialDataDB.query();
        tLPEdorEspecialDataSchema = tLPEdorEspecialDataSet.get(1);
        if(tLPEdorEspecialDataSchema == null ||tLPGrpEdorItemSet.size() != 1){
        	mErrors.copyAllErrors(tLPEdorEspecialDataDB.mErrors);
            mErrors.addOneError(new CError("查询保全项目信息失败！"));
            return false;
        }

        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPGrpEdorMainDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        if (!tLPGrpEdorMainDB.getInfo())
        {
            mErrors.copyAllErrors(tLPGrpEdorMainDB.mErrors);
            mErrors.addOneError(new CError("查询保全信息失败！"));
            return false;
        }
        mLPGrpEdorMainSchema = tLPGrpEdorMainDB.getSchema();

        return true;
    }


    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	mLCGrpContSchema =CommonBL.getLCGrpCont(mLPGrpEdorItemSchema.getGrpContNo());
    	double bbmoney = Double.parseDouble(tLPEdorEspecialDataSchema.getEdorValue());
//    	tLJSGetEndorseSchema.setSchema(setGetEndorse(mLCGrpContSchema, mLPGrpEdorItemSchema.getGetMoney()));
    	setGetEndorse(mLCGrpContSchema, bbmoney);
        /*//设置批改补退费表
        //LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mLPGrpEdorItemSchema.getEdorNo()); //给付通知书号码
        tLJSGetEndorseSchema.setEndorsementNo(mLPGrpEdorItemSchema.
                                              getEdorNo());
        tLJSGetEndorseSchema.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        tLJSGetEndorseSchema.setPolNo("000000");
        tLJSGetEndorseSchema.setFeeOperationType(mLPGrpEdorItemSchema.
                                                 getEdorType());
        tLJSGetEndorseSchema.setGetDate(mLPGrpEdorItemSchema.
                                        getEdorValiDate());
        tLJSGetEndorseSchema.setGetMoney(mLPGrpEdorItemSchema.getGetMoney());
        tLJSGetEndorseSchema.setFeeOperationType("BB");
        tLJSGetEndorseSchema.setRiskCode("000000");
        BqCalBL tBqCalBl = new BqCalBL();
        String feeFinaType = tBqCalBl.getFinType("BB", "GB",
                                                 mLPGrpEdorItemSchema.
                                                 getGrpContNo());
        if (feeFinaType.equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.errorMessage = "在LDCode1中缺少保全财务接口转换类型编码!";
            this.mErrors.addOneError(tError);
            return false;
        }

        tLJSGetEndorseSchema.setFeeFinaType(feeFinaType);
        tLJSGetEndorseSchema.setPayPlanCode("00000000"); //无作用
        tLJSGetEndorseSchema.setDutyCode("0"); //无作用，但一定要，转ljagetendorse时非空
        tLJSGetEndorseSchema.setOtherNo(mLPGrpEdorItemSchema.getEdorNo());
        tLJSGetEndorseSchema.setOtherNoType("3");
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(mLCGrpContSchema.getManageCom());
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        if (tLCGrpContDB.getInfo())
        {
            tLJSGetEndorseSchema.setAgentCode(tLCGrpContDB.getAgentCode());
            tLJSGetEndorseSchema.setAgentCom(tLCGrpContDB.getAgentCom());
            tLJSGetEndorseSchema.setAgentGroup(tLCGrpContDB.getAgentGroup());
            tLJSGetEndorseSchema.setAgentType(tLCGrpContDB.getAgentType());
        }
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());*/

        //modify by hyy
        //mLJSGetEndorseSet.add(tLJSGetEndorseSchema);
    	mLPGrpEdorItemSchema.setGetMoney(bbmoney);
        mLPGrpEdorItemSchema.setEdorState("2");
        mLPGrpEdorItemSchema.setUWFlag("0");
        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        mResult.clear();
//        MMap map = new MMap();
//        map.put(tLJSGetEndorseSchema, SysConst.DELETE_AND_INSERT);
        map.put(mLPGrpEdorItemSchema, SysConst.UPDATE);
        mResult.add(map);
        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        tGlobalInput.ManageCom = "86";

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorNo("410110000000195");
        tLPGrpEdorItemSchema.setGrpContNo("240110000000166");
        tLPGrpEdorItemSchema.setEdorType("LR");

        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPGrpEdorItemSchema);

        GrpEdorBBAppConfirmBL tGrpEdorLRAppConfirmBL = new
                GrpEdorBBAppConfirmBL();
        if (!tGrpEdorLRAppConfirmBL.submitData(tVData, "INSERT"))
        {
            System.out.println(tGrpEdorLRAppConfirmBL.mErrors.getErrContent());
        }

    }


}
