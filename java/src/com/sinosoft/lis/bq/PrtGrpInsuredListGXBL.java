package com.sinosoft.lis.bq;

import java.io.InputStream;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LPContDB;
import com.sinosoft.lis.db.LPGrpEdorItemDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.XmlExport;

/**
* <p>Title: 特需医疗账户资金分配</p>
* <p>Description: 个人账户资金分配清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListGXBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private String mGrpContNo = null;

    private String mEdorNo = null;

    private String mEdorType = "GX";

    private XmlExport xml = new XmlExport();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListGXBL(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = getGrpContNo(edorNo);
    }

    /**
     * 提交业务数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!createXML())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }

    /**
     * 根据清单数据生成XML
     * @param xmlExport XmlExport
     */
    private boolean createXML()
    {
        xml.createDocument("PrtGrpInsuredListGX.vts", "printer");

        //设置批单中TextTag要显示的信息
        TextTag tag = new TextTag();
        tag.add("GrpName", getGrpName(mGrpContNo));
        tag.add("EdorNo", mEdorNo);
        xml.addTextTag(tag); 

        //设置被保人清单
        ListTable listTable = new ListTable();
        listTable.setName("GX");
        LPContSet tLPContSet = getContList();
        for (int i = 1; i <= tLPContSet.size(); i++)
        {
        	LPContSchema tLPContSchema = tLPContSet.get(i);
            String[] column = new String[9];
            column[0] = StrTool.cTrim(String.valueOf(i)); //序号
            column[1] = StrTool.cTrim(tLPContSchema.getContNo());//保单号
            column[2] = StrTool.cTrim(tLPContSchema.getInsuredNo());//客户号
            column[3] = ChangeCodeBL.getCodeName("Sex",tLPContSchema.getInsuredSex());
            column[4] = CommonBL.decodeDate(tLPContSchema.getInsuredBirthday());
            column[5] = ChangeCodeBL.getCodeName("IDType",tLPContSchema.getInsuredIDType());
            System.out.println("我测试，我的证件类型："+tLPContSchema.getInsuredIDType());
            System.out.println("我测试，我的证件类型（decodeDate）："+ChangeCodeBL.getCodeName("IDType",tLPContSchema.getInsuredIDType()));
            column[6] = StrTool.cTrim(tLPContSchema.getInsuredIDNo());
            column[7] = StrTool.cTrim(tLPContSchema.getInsuredName());
            column[8] = CommonBL.decodeState("1");
            listTable.add(column);
        }
        xml.addListTable(listTable, new String[8]);
        //xml.outputDocumentToFile("c:\\", "GAInsuredList");
        return true;
    }

    /**
     * 得到团体合同号
     * @param edorNo String
     * @return String
     */
    private String getGrpContNo(String edorNo)
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(edorNo);
        tLPGrpEdorItemDB.setEdorType("GX");
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            return null;
        }
        return tLPGrpEdorItemSet.get(1).getGrpContNo();
    }

    /**
     * 得到团体单位名称
     * @param grpContNo String
     * @return String
     */
    private String getGrpName(String grpContNo)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
        if (tLCGrpContSet.size() == 0)
        {
            return null;
        }
        return tLCGrpContSet.get(1).getGrpName();
    }

    /**
     * 得到被保人列表
     * @return LPDiskImportSet
     */
    private LPContSet getContList()
    {
        String sql = "select * from LPCont " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "order by contno ";
        LPContDB tLPContDB = new LPContDB();
        return tLPContDB.executeQuery(sql);
    }
    public static void main(String[] args) {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "pa0005";
        String edorno = "20061101000010";
        PrtGrpInsuredListGXBL tPrtGrpInsuredListGXBL = new PrtGrpInsuredListGXBL(tGlobalInput,edorno);
        tPrtGrpInsuredListGXBL.submitData();
    }

}
