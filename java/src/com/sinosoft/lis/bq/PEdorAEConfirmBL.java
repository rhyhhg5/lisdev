package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全变更投保人项目</p>
 * <p>Description: 保全确认</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorAEConfirmBL implements EdorConfirm
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;
    
    private String mCustomerNo = null;

    private ValidateEdorData2 mValidateEdorData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    AppAcc aa = new AppAcc();

    /**
     * 提交数据
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        getInputData(data);

        try {
			if (!dealData())
			{
			    return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private void getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                "GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) data.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        mEdorNo = mLPEdorItemSchema.getEdorNo();
        mContNo = mLPEdorItemSchema.getContNo();
        mEdorType = mLPEdorItemSchema.getEdorType();
        mValidateEdorData =
                new ValidateEdorData2(mGlobalInput, mEdorNo, mEdorType,
                mContNo, "ContNo");

    }

    /**
     * 处理业务数据
     * @return boolean
     * @throws Exception 
     */
    private boolean dealData() throws Exception
    {
//        setBakTables();
        if (!validateEdorData())
        {
            return false;
        }
        if (!createAppAcc()){
        	return false;
        }
//        updateContNo();
        return true;
    }

    /**
     * 保单相关信息存到备份表
     */
/*
    private void setBakTables()
    {
        String[] lcTables =
                {"LCAppnt", "LCCont", "LCInsured", "LCPol", "LCPrem", "LCGet", "LCBnf",
                "LCCustomerImpart", "LCCustomerImpartDetail"};
        mValidateEdorData.bakData(lcTables, mContNo);
        mMap.add(mValidateEdorData.getMap());
//        for (int i = 0; i < lcTables.length; i++)
//        {
//            String lcTable = lcTables[i];
//            String lbTable = "LB" + lcTable.substring(2);
//            StringBuffer sql = new StringBuffer();
//            sql.append("insert into ").append(lbTable)
//                    .append(" (select '").append(mEdorNo).append("', ")
//                    .append(lcTable).append(".* ")
//                    .append("from ").append(lcTable)
//                    .append(" where ContNo = '").append(mContNo).append("')");
//            mMap.put(sql.toString(), "INSERT");
//        }
    }
*/
    /**
     * 使保全数据生效
     */
    private boolean validateEdorData()
    {
//        mValidateEdorData =
//                new ValidateEdorData2(mGlobalInput, mEdorNo, mEdorType,
//                getNewContNo(), "ContNo");
        String[] tables = {"LCAppnt", "LCCont", "LCPol", "LCInsured"};
        mValidateEdorData.changeData(tables);
        mMap.add(mValidateEdorData.getMap());

        //对LDPerson表和LCAddress表要单独处理
        setLDPerson();
        if (!setLCAddress())
        {
            return false;
        }
        return true;
    }
    
    /**
     * 设置账户信息
     * @throws ClassNotFoundException 
     * @throws Exception 
     * @throws SecurityException 
     */
    private boolean createAppAcc() throws Exception{
    	System.out.println("-----------into PedorAeconformBL -- createAppAcc-----------");
    	LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
    	tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
    	LPEdorAppSet tLPEdorAppSet = tLPEdorAppDB.query();
    	LPEdorAppSchema tLPEdorAppSchema = tLPEdorAppSet.get(1);
    	mCustomerNo = tLPEdorAppSchema.getOtherNo();
//    	mCustomerNo = "014073364";
    	if(aa.getLCAppAcc(mCustomerNo) == null || "".equals(aa.getLCAppAcc(mCustomerNo))){
//    		AppAcc aacc = new AppAcc();
//    		Class<?> classType = aacc.getClass();
//    		Method m = classType.getDeclaredMethod("accCreate", new Class[]{String.class , String.class ,String.class});
//    		m.setAccessible(true);
//    		LCAppAccSchema tLCAppAccSchema = (LCAppAccSchema)m.invoke(aacc, new Object[]{mCustomerNo,mGlobalInput.Operator,"0"});
    		if (mCustomerNo == null || mCustomerNo.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "AppAcc";
                tError.functionName = "accCreate";
                tError.errorMessage = "操作员为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
    		if (mGlobalInput.Operator == null || mGlobalInput.Operator.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "AppAcc";
                tError.functionName = "accCreate";
                tError.errorMessage = "操作员为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
    		LCAppAccSchema tLCAppAccSchema = new LCAppAccSchema();
            tLCAppAccSchema.setCustomerNo(mCustomerNo);
            tLCAppAccSchema.setCustomerNoType("1");
            tLCAppAccSchema.setAccFoundDate(PubFun.getCurrentDate());
            tLCAppAccSchema.setAccFoundTime(PubFun.getCurrentTime());
            tLCAppAccSchema.setAccBala(0);
            tLCAppAccSchema.setAccGetMoney(0);
            tLCAppAccSchema.setState("1");
            tLCAppAccSchema.setOperator(mGlobalInput.Operator);
            tLCAppAccSchema.setMakeDate(PubFun.getCurrentDate());
            tLCAppAccSchema.setMakeTime(PubFun.getCurrentTime());
            tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
            tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(tLCAppAccSchema, "DELETE&INSERT");
    		return true;
    	}else{
    		return true;
    	}
    }
    /**
     * 设置客户信息
     */
    private void setLDPerson()
    {
        LPPersonDB tLPPersonDB = new LPPersonDB();
        tLPPersonDB.setEdorNo(mEdorNo);
        tLPPersonDB.setEdorType(mEdorType);
        LPPersonSet tLPPersonSet = tLPPersonDB.query();
        for (int i = 1; i <= tLPPersonSet.size(); i++)
        {
            LDPersonSchema tLDPersonSchema = new LDPersonSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLDPersonSchema, tLPPersonSet.get(i));
            mMap.put(tLDPersonSchema, "DELETE&INSERT");
        }
    }

    /**
     * 设置保单地址信息
     */
    private boolean setLCAddress()
    {
        LPAddressDB tLPAddressDB = new LPAddressDB();
        tLPAddressDB.setEdorNo(mEdorNo);
        tLPAddressDB.setEdorType(mEdorType);
        LPAddressSet tLPAddressSet = tLPAddressDB.query();
        if (tLPAddressSet.size() == 0)
        {
            mErrors.addOneError("未找到新的保单地址信息！");
            return false;
        }
        LPAddressSchema tLPAddressSchema = tLPAddressSet.get(1);

        String sql = "select max(int(AddressNo)) from LCAddress " +
                "where CustomerNo = '" +  tLPAddressSchema.getCustomerNo() + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String addressNo = tExeSQL.getOneValue(sql);

        //对新客户的保单地址和老客户的保单地址做不同处理
        if (addressNo.equals("")) //新客户
        {
            addressNo = "1";
        }
        else //老客户
        {
            int max = Integer.parseInt(addressNo) + 1;
            addressNo = String.valueOf(max);
        }
        LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLCAddressSchema, tLPAddressSchema);
        tLCAddressSchema.setAddressNo(addressNo);
        tLCAddressSchema.setOperator(mGlobalInput.Operator);
        tLCAddressSchema.setModifyDate(mCurrentDate);
        tLCAddressSchema.setModifyTime(mCurrentTime);
        mMap.put(tLCAddressSchema, "DELETE&INSERT");

        //更新LCAppnt表中的AddressNo
        sql = "update LCAppnt set AddressNo = '" + addressNo + "' " +
                "where ContNo = '" + mContNo + "'";
        mMap.put(sql, "UPDATE");
        return true;
    }

    /**
     * 得到新合同号
     * @return String
     */
/*
    private String getNewContNo()
    {
        LPAppntDB tLPAppntDB = new LPAppntDB();
        tLPAppntDB.setEdorNo(mEdorNo);
        tLPAppntDB.setEdorType(mEdorType);
        tLPAppntDB.setContNo(mContNo);
        tLPAppntDB.getInfo();
        return  PubFun1.CreateMaxNo("ContNo", tLPAppntDB.getAppntNo());
    }
*/
    /**
     * 更新保单号
     */
/*
    private void updateContNo()
    {
        String[] lcTables =
                {"LCAppnt", "LCCont", "LCInsured", "LCPol",
                "LCDuty", "LCPrem", "LCGet", "LCCustomerImpart",
                "LCCustomerImpartDetail", "LCCustomerImpartParams", "LCBnf"};

        String contNo = getNewContNo();
        for (int i = 0; i < lcTables.length; i++)
        {
            StringBuffer sql = new StringBuffer();
            sql.append("update ").append(lcTables[i])
                    .append(" set ContNo = '").append(contNo).append("', ")
                    .append("Operator = '").append(mGlobalInput.Operator).append("', ")
                    .append("ModifyDate = '").append(mCurrentDate).append("', ")
                    .append("ModifyTime = '").append(mCurrentTime).append("' ")
                    .append("where ContNo = '").append(mContNo).append("'");
            mMap.put(sql.toString(), "UPDATE");
        }
    }
*/
    public static void main(String args[]){
    	PEdorAEConfirmBL t = new PEdorAEConfirmBL();
    	try {
			t.createAppAcc();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
