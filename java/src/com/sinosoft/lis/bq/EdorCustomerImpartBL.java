package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class EdorCustomerImpartBL
{
    /** 错误处理类*/
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mInsuredNo = null;

    private String mProposalContNo = null;

    private String mPrtNo = null;

    private LPCustomerImpartSet mLPCustomerImpartSet = null;

    private LPCustomerImpartParamsSet mLPCustomerImpartParamsSet = null;

    private GlobalInput mGlobalInput = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();


    /**mTransferData
     * 数据提交的公共方法
     * @param gi GlobalInput
     * @param edorAcceptNo String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到输入数据
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.
                    getObjectByObjectName("GlobalInput", 0);
            mLPCustomerImpartSet =  (LPCustomerImpartSet) data.
                    getObjectByObjectName("LPCustomerImpartSet", 0);
            mLPCustomerImpartParamsSet = (LPCustomerImpartParamsSet) data.
                    getObjectByObjectName("LPCustomerImpartParamsSet", 0);
            LPEdorItemSchema tLPEdorItemSchema = (LPEdorItemSchema) data.
                    getObjectByObjectName("LPEdorItemSchema", 0);
            mEdorNo = tLPEdorItemSchema.getEdorNo();
            mEdorType = tLPEdorItemSchema.getEdorType();
            mContNo = tLPEdorItemSchema.getContNo();

            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(mContNo);
            if (!tLCContDB.getInfo())
            {
                mErrors.addOneError("未找到保单信息！");
                return false;
            }
            mProposalContNo = tLCContDB.getProposalContNo();
            mPrtNo = tLCContDB.getPrtNo();
            ExeSQL tExSql = new ExeSQL();    
            String str = "select riskcode lcpol  where  appflag ='2' and contno ='"+tLPEdorItemSchema.getContNo() +"' ";  
            String sriskcode = tExSql.getOneValue(str);
            if (null != sriskcode && sriskcode.equals("232701")){
             mInsuredNo = tLCContDB.getAppntIDNo();
            }else {
            mInsuredNo = tLPEdorItemSchema.getInsuredNo();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        setLPCustomerImpart();
        setLPCustomerImpartParams();
        return true;
    }

    private void setLPCustomerImpart()
    {
        for (int i = 1; i <= mLPCustomerImpartSet.size(); i++)
        {
            LPCustomerImpartSchema tLPCustomerImpartSchema = mLPCustomerImpartSet.get(i);
            tLPCustomerImpartSchema.setEdorNo(mEdorNo);
            tLPCustomerImpartSchema.setEdorType(mEdorType);
            tLPCustomerImpartSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLPCustomerImpartSchema.setContNo(mContNo);
            tLPCustomerImpartSchema.setProposalContNo(mProposalContNo);
            tLPCustomerImpartSchema.setPrtNo(mPrtNo);
            tLPCustomerImpartSchema.setImpartVer("101");  //保全健康告知
            tLPCustomerImpartSchema.setCustomerNo(mInsuredNo);
            tLPCustomerImpartSchema.setCustomerNoType("1"); //个单
            tLPCustomerImpartSchema.setOperator(mGlobalInput.Operator);
            tLPCustomerImpartSchema.setMakeDate(mCurrentDate);
            tLPCustomerImpartSchema.setMakeTime(mCurrentTime);
            tLPCustomerImpartSchema.setModifyDate(mCurrentDate);
            tLPCustomerImpartSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPCustomerImpartSchema, "DELETE&INSERT");
        }
    }

    private void setLPCustomerImpartParams()
    {
        for (int i = 1; i <= mLPCustomerImpartParamsSet.size(); i++)
        {
            LPCustomerImpartParamsSchema tLPCustomerImpartParamsSchema =
                    mLPCustomerImpartParamsSet.get(i);
            tLPCustomerImpartParamsSchema.setEdorNo(mEdorNo);
            tLPCustomerImpartParamsSchema.setEdorType(mEdorType);
            tLPCustomerImpartParamsSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLPCustomerImpartParamsSchema.setContNo(mContNo);
            tLPCustomerImpartParamsSchema.setProposalContNo(mProposalContNo);
            tLPCustomerImpartParamsSchema.setPrtNo(mPrtNo);
            tLPCustomerImpartParamsSchema.setImpartVer("101"); //保全健康告知
            tLPCustomerImpartParamsSchema.setCustomerNo(mInsuredNo);
            tLPCustomerImpartParamsSchema.setCustomerNoType("1"); //个单
            if (tLPCustomerImpartParamsSchema.getImpartParamName() == null)
            {
                tLPCustomerImpartParamsSchema.setImpartParamName("PEdorHealth");
            }
            tLPCustomerImpartParamsSchema.setOperator(mGlobalInput.Operator);
            tLPCustomerImpartParamsSchema.setMakeDate(mCurrentDate);
            tLPCustomerImpartParamsSchema.setMakeTime(mCurrentTime);
            tLPCustomerImpartParamsSchema.setModifyDate(mCurrentDate);
            tLPCustomerImpartParamsSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPCustomerImpartParamsSchema, "DELETE&INSERT");
        }
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
