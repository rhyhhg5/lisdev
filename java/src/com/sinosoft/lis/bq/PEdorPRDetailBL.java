package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 个单迁移</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 1.0
 */
public class PEdorPRDetailBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    private MMap mMap = new MMap();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LPAddressSchema mLPAddressSchema = new LPAddressSchema();
    private GlobalInput mGlobalInput = new GlobalInput();
    private EdorItemSpecialData mEdorItemSpecialData;
    private String currDate = PubFun.getCurrentDate();
    private String currTime = PubFun.getCurrentTime();

    public PEdorPRDetailBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (getSubmitData(cInputData, cOperate) == null) {
            return false;
        }
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorPRDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    public MMap getSubmitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return null;
        }
        //数据准备操作
        if (!dealData()) {
            return null;
        }
        return mMap;
    }

    public VData getResult() {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData() {
        try {
            mLPAddressSchema = (LPAddressSchema) mInputData
                               .getObjectByObjectName("LPAddressSchema", 0);
            mLPEdorItemSchema.setSchema((LPEdorItemSchema) mInputData.
                                        getObjectByObjectName(
                                                "LPEdorItemSchema", 0));

            mGlobalInput = (GlobalInput) mInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mEdorItemSpecialData = (EdorItemSpecialData) mInputData.
                                   getObjectByObjectName("EdorItemSpecialData",
                    0);

            if (mLPAddressSchema == null || mLPEdorItemSchema == null ||
                mEdorItemSpecialData == null) {
                CError.buildErr(this, "接收数据失败");
                return false;
            }
            if (mLPEdorItemSchema.getEdorNo() == null ||
                mLPEdorItemSchema.getContNo() == null) {
                CError.buildErr(this, "接收数据失败,没有传入保全号或者保单号");
                return false;
            }
        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData() {
        //数据转移对象
        String agentCode = mEdorItemSpecialData.getEdorValue("AgentCode");
        
        String contNo = mLPEdorItemSchema.getContNo();
        String edorNo = mLPEdorItemSchema.getEdorNo();
        String edorType = mLPEdorItemSchema.getEdorType();

        
               
        
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(agentCode);
        if (!tLAAgentDB.getInfo()) {
            CError.buildErr(this, "查询保单业务员失败");
            return false;
        }
        
        String incom = new ExeSQL().getOneValue("select EdorValue from lpedorespecialdata where edorno='"+edorNo+"' and edortype='PR' and detailtype='INCOM'");
        if(incom.equals("")||incom==null||(!incom.equals(tLAAgentDB.getManageCom()))){
        	LDComDB tLDComDB=new LDComDB();
        	tLDComDB.setComCode(incom);
        	LDComSet tLDComSet=tLDComDB.query();
        	String incomName=null;
        	if(tLDComSet.size()>0)
        	{
        	    incomName=tLDComSet.get(1).getName();
        	}
        	
        	tLDComDB.setComCode(tLAAgentDB.getManageCom());
        	LDComSet ttLDComSet=tLDComDB.query();
        	String lAAgentName=null;
        	if(ttLDComSet.size()>0)
        	{
        		lAAgentName=ttLDComSet.get(1).getName();
        	}
        	if((lAAgentName==null)||(incomName==null))
        	{
        		CError.buildErr(this, "指定的业务员管理机构和迁入管理机构不能为空,请重新录入");
                return false;
        	}

        	CError.buildErr(this, "指定的业务员（"+tLAAgentDB.getName()+"）管理机构为"+lAAgentName+"，迁入管理机构为"+incomName+"," +
        			"指定的业务员管理机构和迁入管理机构不一致，请重新选择业务员。");
            return false;
        }
        
        
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(contNo);
        if (!tLCContDB.getInfo()) {
            CError.buildErr(this, "查询保单失败");
            return false;
        }
        //调用销售转换类判断销售机构
        AgentPubFun tAgentPubFun = new AgentPubFun();
        String []agentSel = tAgentPubFun.switchSalChnl(tLCContDB.getSaleChnl(),tLCContDB.getSaleChnlDetail(),null);
        if(agentSel[0]==null||agentSel[1]==null)
        {
            CError.buildErr(this, "销售渠道转换失败！");
            return false;
        }
        if (!(agentSel[0].equals(tLAAgentDB.getBranchType())&&agentSel[1].equals(tLAAgentDB.getBranchType2()))) {
        	
        	LDCodeDB tLDCodeDB = new LDCodeDB();
        	tLDCodeDB.setCode(agentSel[1]);
        	tLDCodeDB.setCodeType("branchtype2"); 
        	LDCodeSet tLDCodeSet=tLDCodeDB.query();
        	String lccontBranchType2Name=null;
        	if(tLDCodeSet.size()>0)
        	{
        		lccontBranchType2Name=tLDCodeSet.get(1).getCodeName();
        	}
        	tLDCodeDB.setCode(tLAAgentDB.getBranchType2());
        	tLDCodeDB.setCodeType("branchtype2"); 
        	LDCodeSet tLDCodeSet1=tLDCodeDB.query();
        	String laAgentBranchType2Name=null;
        	if(tLDCodeSet1.size()>0)
        	{
        		laAgentBranchType2Name=tLDCodeSet1.get(1).getCodeName();
        	}
        	if((laAgentBranchType2Name==null)||(lccontBranchType2Name==null))
        	{
        		CError.buildErr(this, "指定的业务员销售渠道和原保单销售渠道为空,请重新录入!");
                return false;
        	}
        	
        	CError.buildErr(this, "指定的业务员（"+tLAAgentDB.getName()+"）销售渠道为"+laAgentBranchType2Name+"，原保单销售渠道为"+lccontBranchType2Name+"," +
			"指定的业务员与原保单渠道不同，请重新选择业务员。");
            return false;
        }

        String updateLPCont = "Update lpcont set agentCode ='"
                              + agentCode + "' ,agentGroup ='"
                              + tLAAgentDB.getAgentGroup() + "' ,Operator ='"
                              + mGlobalInput.Operator + "',modifydate ='"
                              + PubFun.getCurrentDate() + "',modifytime ='"
                              + PubFun.getCurrentTime() + "' where edorno ='"
                              + edorNo + "'and edortype ='"
                              + edorType + "' and contno ='" + contNo + "'"
                              ;
        mMap.put(updateLPCont, "UPDATE");

        String updateLPPol = "Update lppol set agentCode ='"
                             + agentCode + "' ,agentGroup ='"
                             + tLAAgentDB.getAgentGroup() + "' ,Operator ='"
                             + mGlobalInput.Operator + "',modifydate ='"
                             + PubFun.getCurrentDate() + "',modifytime ='"
                             + PubFun.getCurrentTime() + "' where edorno ='"
                             + edorNo + "'and edortype ='"
                             + edorType + "' and contno ='" + contNo + "'"
                             ;
        mMap.put(updateLPPol, "UPDATE");

        LPAddressDB tLPAddressDB = new LPAddressDB();
        tLPAddressDB.setEdorNo(edorNo);
        tLPAddressDB.setEdorType(edorType);
        LPAddressSet tLPAddressSet = tLPAddressDB.query();
        if (tLPAddressSet.size() < 1) {
            CError.buildErr(this, "查询保单地址信息失败");
            return false;
        }
        LPAddressSchema tLPAddressSchema = tLPAddressSet.get(1);
        tLPAddressSchema.setPostalAddress(mLPAddressSchema.getPostalAddress());
        tLPAddressSchema.setZipCode(mLPAddressSchema.getZipCode());
        tLPAddressSchema.setPhone(mLPAddressSchema.getPhone());
        tLPAddressSchema.setOperator(mGlobalInput.Operator);
        tLPAddressSchema.setMakeDate(PubFun.getCurrentDate());
        tLPAddressSchema.setMakeTime(PubFun.getCurrentTime());
        tLPAddressSchema.setModifyDate(PubFun.getCurrentDate());
        tLPAddressSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLPAddressSchema, "DELETE&INSERT");
        String sql = "update LPEdorItem set edorstate ='1',modifydate ='"
                     +currDate+"',modifytime ='"+currTime+"',operator='"
                     +mGlobalInput.Operator+"' where EdorNo='"+edorNo+"'";
        mMap.put(sql, "UPDATE");

        mEdorItemSpecialData.add("dealstate", "3");
        mMap.put(mEdorItemSpecialData.getSpecialDataSet(), "DELETE&INSERT");
        mResult.clear();
        mResult.add(mMap);

        return true;
    }


}
