package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全理算,以前叫保全受理申请确认</p>
 * <p>Description: 对保全变更进行计算,并把计算结果放入P表,保全确认后生效</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class PEdorAppConfirmUI
{
    private PEdorAppConfirmBL mPEdorAppConfirmBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorAcceptNo String
     */
    public PEdorAppConfirmUI(GlobalInput gi, String edorAcceptNo)
    {
        mPEdorAppConfirmBL = new PEdorAppConfirmBL(gi, edorAcceptNo);
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPEdorAppConfirmBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mPEdorAppConfirmBL.mErrors.getLastError();
    }


    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.ManageCom = "86";
        gi.Operator = "pa0003";
        String edorAcceptNo = "20061206000008";
        PEdorAppConfirmUI tPEdorAppConfirmUI = new PEdorAppConfirmUI(gi, edorAcceptNo);
        if (!tPEdorAppConfirmUI.submitData())
        {
            System.out.println(tPEdorAppConfirmUI.getError());
        }
    }
}
