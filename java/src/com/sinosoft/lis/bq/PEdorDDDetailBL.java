package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全个单分红险红利变更</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * author Lanjun, QiuYang
 * @version 1.0
 */

public class PEdorDDDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private DetailDataQuery mQuery = null;

    private String mEdorNo = null;
    private String mPolNo =null;

    private String mEdorType = BQ.EDORTYPE_DD;

    private String mContNo = null;

    private LPPolSchema mLPPolSchema = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        System.out.println("来到了BL层");
        if (!dealData())
        {
            return false;
        }
        System.out.println("完成了BL层");
        
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPPolSchema = (LPPolSchema) cInputData.getObjectByObjectName(
                    "LPPolSchema", 0);
            mEdorNo = mLPPolSchema.getEdorNo();
            mContNo = mLPPolSchema.getContNo();
            mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入参数错误！");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        setBonusGetModeInfo();
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
   }

   /**
    * 更新交费资料
    */
   private boolean setBonusGetModeInfo()
   {
	   String getPolnoSql="select polno from lcpol a where contno='"+mContNo+"' " +
	   		"and exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype4='2')";
	   ExeSQL tExeSQL =new ExeSQL();
	   String tPolNo=tExeSQL.getOneValue(getPolnoSql);
	   System.out.println("险种号："+tPolNo);
	   mPolNo=tPolNo;
	   LPPolSchema tLPPolSchema = (LPPolSchema)
               mQuery.getDetailData("LCPol", tPolNo);
	   tLPPolSchema.setBonusGetMode(mLPPolSchema.getBonusGetMode());
	   tLPPolSchema.setOperator(mGlobalInput.Operator);
	   tLPPolSchema.setModifyDate(mCurrentDate);
	   tLPPolSchema.setModifyTime(mCurrentTime);
       mMap.put(tLPPolSchema, "DELETE&INSERT");
       return true;
   }

   /**
    * 把保全状态设为已录入
    * @param edorState String
    */
   private void setEdorState(String edorState)
   {
       String sql = "update  LPEdorItem " +
               "set EdorState = '" + edorState + "', " +
               "    polno     = '"+mPolNo+"', "+
               "    Operator = '" + mGlobalInput.Operator + "', " + 
               "    ModifyDate = '" + mCurrentDate + "', " +
               "    ModifyTime = '" + mCurrentTime + "' " +
               "where  EdorNo = '" + mEdorNo + "' " +
               "and EdorType = '" + mEdorType + "' " +
               "and ContNo = '" + mContNo + "'";
       mMap.put(sql, "UPDATE");
   }


   /**
    * 提交数据到数据库
    * @return boolean
    */
   private boolean submit()
   {
       VData data = new VData();
       data.add(mMap);
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(data, ""))
       {
           mErrors.copyAllErrors(tPubSubmit.mErrors);
           return false;
       }
       return true;
   }
}
