package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCInsureAccClassFeeDB;
import com.sinosoft.lis.db.LCInsureAccFeeDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJSPayPersonDB;
import com.sinosoft.lis.db.LMRiskFeeDB;
import com.sinosoft.lis.db.LPEdorEspecialDataDB;
import com.sinosoft.lis.db.LPInsureAccFeeTraceDB;
import com.sinosoft.lis.db.LPInsureAccTraceDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsureAccClassFeeSchema;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCInsureAccFeeSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LJSPayPersonSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LMRiskFeeSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPDutySchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPInsureAccClassFeeSchema;
import com.sinosoft.lis.schema.LPInsureAccClassSchema;
import com.sinosoft.lis.schema.LPInsureAccFeeSchema;
import com.sinosoft.lis.schema.LPInsureAccFeeTraceSchema;
import com.sinosoft.lis.schema.LPInsureAccSchema;
import com.sinosoft.lis.schema.LPInsureAccTraceSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.schema.LPPremSchema;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCInsureAccClassFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccClassSet;
import com.sinosoft.lis.vschema.LCInsureAccFeeSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LMRiskFeeSet;
import com.sinosoft.lis.vschema.LPDutySet;
import com.sinosoft.lis.vschema.LPEdorEspecialDataSet;
import com.sinosoft.lis.vschema.LPInsureAccClassFeeSet;
import com.sinosoft.lis.vschema.LPInsureAccClassSet;
import com.sinosoft.lis.vschema.LPInsureAccFeeTraceSet;
import com.sinosoft.lis.vschema.LPInsureAccTraceSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.lis.vschema.LPPremSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PEdorRVAppConfirmBL implements EdorAppConfirm 
{
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	private MMap mMap = new MMap();
  private String delFeeTrace = "0";
	private GlobalInput mGlobalInput = null;
	private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
	/** 保全号 */
	private String aEdorNo = null;
	/** 保全类型 */
	private String aEdorType = null;
	/** 合同号 */
	private String mContNo = null;
	private String mFeeCode = null;
	private String mInsuAccNo = null;
	private double rate = 0.0;
	private double mAppMoney;// 追加保费
	private double mLoanMoney;// 补交保费
	private String mValaibleDate;// 复效生效日
	private Reflections ref = new Reflections();
	/** 保单相关信息表 */
	private LPPremSet mLPPremSet = new LPPremSet();
	private LPDutySet mLPDutySet = new LPDutySet();
	private LPPolSet mLPPolSet = new LPPolSet();
	private LPContSchema mLPContSchema = new LPContSchema();
	private int mCount = 0;// 欠费期数
	/** 帐户相关信息表 */
	private LPInsureAccSchema mLPInsureAccSchema = new LPInsureAccSchema();
	private LPInsureAccClassSet mLPInsureAccClassSet = new LPInsureAccClassSet();
	private LPInsureAccFeeSchema mLPInsureAccFeeSchema = new LPInsureAccFeeSchema();
	private LPInsureAccClassFeeSet mLPInsureAccClassFeeSet = new LPInsureAccClassFeeSet();

	/** 存放主险保单数据 */
	private LCPolSchema mLCPolSchema = null;
	private LJSPaySet mLJSPaySet = new LJSPaySet();

	/** 保全生效日期 */
	private String mEdorValiDate = null;
	private String mEdorAcceptNo = "";
	/** 保全交费总额 */
	private double mGetMoney = 0.00;
	private LPEdorEspecialDataSet mLPEdorEspecialDataSet = new LPEdorEspecialDataSet();

	/** 保全项目特殊数据 */

	private DetailDataQuery mQuery = null;
	/** 保全项目特殊数据 */
	private EdorItemSpecialData mVtsData = null;
	// private double mSumBFMoney = 0.0;
	private LPEdorItemSchema mLPEdorItemSchema = null;
	private TransferData mTransferData = new TransferData();

	private String aMakeDate = PubFun.getCurrentDate();
	private String aMakeTime = PubFun.getCurrentTime();

	private String aPaytoDate = null;
	private int aPayIntv = 0;
	private String aOperator;
	// 删除insureacctrace标记
	private String delAccTrace = "0";
	private LPInsureAccFeeTraceSet mLPInsureAccFeeTraceSet=new LPInsureAccFeeTraceSet();
	private LPInsureAccTraceSet mLPInsureAccTraceSet=new LPInsureAccTraceSet();
	/**
	 * 提交数据
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		
		if (!getInputData(cInputData)) {			
			return false;
		}

		if (!dealData()) {
			
			return false;
		}

		return true;
	}



	/**
	 * 得到传入参数
	 * 
	 * @param cInputData
	 *            VData
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);

		mLPEdorItemSchema = (LPEdorItemSchema) cInputData
				.getObjectByObjectName("LPEdorItemSchema", 0);
		aEdorNo = mLPEdorItemSchema.getEdorNo();
		aEdorType = mLPEdorItemSchema.getEdorType();
		mContNo = mLPEdorItemSchema.getContNo();
		mEdorAcceptNo = mLPEdorItemSchema.getEdorAcceptNo();
		mEdorValiDate = mLPEdorItemSchema.getEdorValiDate();// 保全生效日期
		aOperator = mGlobalInput.Operator;
		LCPolDB tLCPolDB = new LCPolDB();
		String tSQL = "select * from lcpol where contno='" + mContNo
				+ "' and polno= mainpolno";
		LCPolSet tLCPolSet = tLCPolDB.executeQuery(tSQL);
		if (tLCPolSet.size() < 1) {
			CError tError = new CError();
			tError.moduleName = "PEdorRVAppConfirmBL";
			tError.functionName = "inputData";
			tError.errorMessage = "获取个人险种数据失败!";
			mErrors.addOneError(tError);
			return false;
		}
		this.mLCPolSchema = tLCPolSet.get(1);// 获得主险信息

		this.mInsuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,
				mLCPolSchema.getRiskCode());
		this.aPaytoDate = mLCPolSchema.getPaytoDate();
		this.aPayIntv = mLCPolSchema.getPayIntv();

		LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
		tLPEdorEspecialDataDB.setEdorNo(aEdorNo);
		tLPEdorEspecialDataDB.setEdorType(aEdorType);
		tLPEdorEspecialDataDB.setEdorAcceptNo(mEdorAcceptNo);

		mVtsData = new EdorItemSpecialData(mLPEdorItemSchema);// TODO WHY?

		mLPEdorEspecialDataSet = (LPEdorEspecialDataSet) tLPEdorEspecialDataDB
				.query();
		for (int i = 1; i <= mLPEdorEspecialDataSet.size(); i++) {
//			if (mLPEdorEspecialDataSet.get(i).getDetailType().equals("RV_R")) {
//				this.rate = Double.parseDouble(mLPEdorEspecialDataSet.get(i)
//						.getEdorValue());
//			}
			if (mLPEdorEspecialDataSet.get(i).getDetailType().equals(
					"RV_APPMONEY")) {
				this.mAppMoney = Double.parseDouble(mLPEdorEspecialDataSet.get(
						i).getEdorValue());
			}
			if (mLPEdorEspecialDataSet.get(i).getDetailType().equals("RV_D")) {
				this.mValaibleDate = mLPEdorEspecialDataSet.get(i)
						.getEdorValue();
			}
			if (mLPEdorEspecialDataSet.get(i).getDetailType().equals(
					"RV_LOANMONEY")) {
				this.mLoanMoney = Double.parseDouble(mLPEdorEspecialDataSet
						.get(i).getEdorValue());
			}
		}
		mQuery = new DetailDataQuery(aEdorNo, aEdorType);// TODO WHY?
		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		
		// 保单需要补费
		if (this.mLoanMoney >0) {

			VData tVData = new VData();
			tVData.add(mGlobalInput);
			tVData.add(mLPEdorItemSchema);

			// 重新生成欠交保费的应收记录，防止重新理算的时候删除应收记录，如果已存在，不会重复生成
			PEdorRVDetailBL tPEdorRVDetailBL = new PEdorRVDetailBL();
			if (!tPEdorRVDetailBL.submitData(tVData, "init")) {
				CError tError = new CError();
				tError.moduleName = "PEdorRVAppConfirmBL";
				tError.functionName = "dealData";
				tError.errorMessage = "催收续期保费失败!";
				mErrors.addOneError(tError);
				return false;
			}
			String tLJSPaySQL = "select * from ljspay where otherno='"
					+ aEdorNo + "' and othernotype='10' order by paydate desc";
			LJSPayDB tLJSPayDB = new LJSPayDB();
			mLJSPaySet = tLJSPayDB.executeQuery(tLJSPaySQL);
			
			if (mLJSPaySet.size() < 1) {
				CError tError = new CError();
				tError.moduleName = "PEdorRVAppConfirmBL";
				tError.functionName = "dealData";
				tError.errorMessage = "获取应收数据失败!";
				mErrors.addOneError(tError);
				return false;
			}
		}

		if (!setInsuredAcc()) {
			return false;
		}
		
		if (!setContInfo()) {
			return false;
		}
		
		setEdorItem();
		return true;
	}

	/** 处理保单信息 */
	private boolean setContInfo() {
		LCContSchema tLCContSchema = new LCContSchema();
		tLCContSchema = CommonBL.getLCCont(mContNo);
		ref.transFields(mLPContSchema, tLCContSchema);
		// 处理保单信息
		mLPContSchema.setEdorNo(aEdorNo);
		mLPContSchema.setEdorType(aEdorType);
		mLPContSchema.setSumPrem(tLCContSchema.getSumPrem() + mLoanMoney);
		if(mLoanMoney >0){
			mLPContSchema.setPaytoDate(aPaytoDate);			
		}
		
		mLPContSchema.setModifyDate(aMakeDate);
		mLPContSchema.setModifyTime(aMakeTime);
		mLPContSchema.setStateFlag("1"); //
		mMap.put(mLPContSchema, "DELETE&INSERT");
		LCPolSet tLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(mContNo);
		tLCPolSet = tLCPolDB.query();
		// 处理险种信息
		for (int i = 1; i <= tLCPolSet.size(); i++) {
			LCPolSchema tLCPolSchema = new LCPolSchema();
			LPPolSchema tLPPolSchema = new LPPolSchema();
			tLCPolSchema = tLCPolSet.get(i);
			ref.transFields(tLPPolSchema, tLCPolSchema);

			LCPremSet tLCPremSet = new LCPremSet();
			LCPremDB tLCPremDB = new LCPremDB();
			tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
			tLCPremSet = tLCPremDB.query();
			// 处理保费信息
			for (int j = 1; j <= tLCPremSet.size(); j++) {
				LPPremSchema tLPPremSchema = new LPPremSchema();
				ref.transFields(tLPPremSchema, tLCPremSet.get(j));
				tLPPremSchema.setEdorNo(aEdorNo);

				tLPPremSchema.setEdorType(aEdorType);
				if (tLCPremSet.get(j).getSumPrem() > 0) {
					tLPPremSchema.setSumPrem(tLCPremSet.get(j).getSumPrem()
							+ mLoanMoney);
				}
				tLPPremSchema.setModifyDate(aMakeDate);
				tLPPremSchema.setModifyTime(aMakeTime);
				if(mLoanMoney >0){
				tLPPremSchema.setPaytoDate(aPaytoDate);
				}
				mLPPremSet.add(tLPPremSchema);

			}

			LCDutySet tLCDutySet = new LCDutySet();
			LCDutyDB tLCDutyDB = new LCDutyDB();
			tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
			tLCDutySet = tLCDutyDB.query();
			// 处理责任信息
			for (int k = 1; k <= tLCDutySet.size(); k++) {
				LPDutySchema tLPDutySchema = new LPDutySchema();
				ref.transFields(tLPDutySchema, tLCDutySet.get(k));
				tLPDutySchema.setEdorNo(aEdorNo);
				tLPDutySchema.setEdorType(aEdorType);
				if (tLCDutySet.get(k).getPrem() > 0) {
					tLPDutySchema.setSumPrem(tLCDutySet.get(k).getSumPrem()
							+ mLoanMoney);
				}
				tLPDutySchema.setModifyDate(aMakeDate);
				tLPDutySchema.setModifyTime(aMakeTime);
				if(mLoanMoney >0){
				tLPDutySchema.setPaytoDate(aPaytoDate);
				}
				mLPDutySet.add(tLPDutySchema);

			}
			if (tLCPolSchema.getPrem() > 0) {

				tLPPolSchema.setSumPrem(tLCPolSchema.getSumPrem() + mLoanMoney);
			}
			tLPPolSchema.setStandbyFlag1("0");
			tLPPolSchema.setStateFlag("1");
			tLPPolSchema.setEdorNo(aEdorNo);
			tLPPolSchema.setEdorType(aEdorType);
			tLPPolSchema.setModifyDate(aMakeDate);
			tLPPolSchema.setModifyTime(aMakeTime);
			if(mLoanMoney >0){
			tLPPolSchema.setPaytoDate(aPaytoDate);
			}
			mLPPolSet.add(tLPPolSchema);

		}
		mMap.put(mLPPolSet, "DELETE&INSERT");
		mMap.put(mLPPremSet, "DELETE&INSERT");
		mMap.put(mLPDutySet, "DELETE&INSERT");
		return true;
	}

	/**
	 * 设置被保人账户保费金额
	 * 
	 * @return boolean
	 */
	private boolean setInsuredAcc() {
		// 进入帐户金额
		double tSumMoney = 0.0;
		// 进入帐户后初始扣费金额金额
		double tSumFee = 0.0;
		double tSumPrem = 0.0;
		String tMoneyType = "";
		// 处理帐户主表,设置LPInsureAccSchema
		LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
		tLCInsureAccSchema = CommonBL.getLCInsureAcc(mLCPolSchema.getPolNo(),
				CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED, mLCPolSchema
						.getRiskCode()));
		ref.transFields(mLPInsureAccSchema, tLCInsureAccSchema);
		mLPInsureAccSchema.setEdorNo(aEdorNo);
		mLPInsureAccSchema.setEdorType(aEdorType);
		mLPInsureAccSchema.setModifyDate(aMakeDate);
		mLPInsureAccSchema.setModifyTime(aMakeTime);

		// 补费部分
		if (mLoanMoney >0) {
			tMoneyType = "BF";
			String mainPolNo = mLCPolSchema.getPolNo();
			String tSql = "select * from ljspayperson where getnoticeno='"
					+ mLJSPaySet.get(1).getGetNoticeNo() + "' and polno='"
					+ mainPolNo
					+ "' and payplancode not like '0000%' order by paycount";
			LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
			LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
			tLJSPayPersonSet = tLJSPayPersonDB.executeQuery(tSql);
			if (tLJSPayPersonSet.size() < 1) {
				CError tError = new CError();
				tError.moduleName = "PEdorRVAppConfirmBL";
				tError.functionName = "setInsuredAcc";
				tError.errorMessage = "获取应收个人缴费表失败!";
				mErrors.addOneError(tError);
				return false;
			}
			for (int i = 1; i <= tLJSPayPersonSet.size(); i++) {
				LJSPayPersonSchema tLJSPayPersonSchema = tLJSPayPersonSet
						.get(i);
				if (tLJSPayPersonSchema.getSumDuePayMoney() == 0) {
					continue;
				}
				double tPrem = tLJSPayPersonSchema.getSumDuePayMoney();// 本期保费
				double tFee = getFee(tLJSPayPersonSchema);// 计算初始费用
				if (tFee == -1) {
					CError tError = new CError();
					tError.moduleName = "PEdorRVAppConfirmBL";
					tError.functionName = "setInsuredAcc";
					tError.errorMessage = "计算初始扣费失败!";
					mErrors.addOneError(tError);
					return false;
				}
				tSumFee += tFee;
				// tFee = Double.parseDouble("-" + tFee);
				double tMoney = tLJSPayPersonSchema.getSumDuePayMoney()
						+ Double.parseDouble("-" + tFee);
				tSumPrem += tLJSPayPersonSchema.getSumDuePayMoney();
				// 累计进入帐户的保费和初始扣费金额
				
				tSumMoney += tMoney;

				// 处理持续奖励
				double tJL = getJL(tLJSPayPersonSchema);
				if (tJL == -2) {
					CError tError = new CError();
					tError.moduleName = "PEdorRVAppConfirmBL";
					tError.functionName = "setInsuredAcc";
					tError.errorMessage = "计算持续奖励失败!";
					mErrors.addOneError(tError);
					return false;
				}
				if (tJL > 0) {
					tSumMoney += tJL;
					 
					
				}
				// 处理帐户分类表
				LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
				LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
				tLCInsureAccClassDB.setPolNo(mLCPolSchema.getPolNo());
				tLCInsureAccClassDB.setAccType(BQ.ACCTYPE_INSURED);
				tLCInsureAccClassSet = tLCInsureAccClassDB.query();
				if (tLCInsureAccClassSet.size() < 1) {
					CError tError = new CError();
					tError.moduleName = "PEdorRVAppConfirmBL";
					tError.functionName = "setInsuredAcc";
					tError.errorMessage = "查询帐户分类表数据失败!";
					mErrors.addOneError(tError);
					return false;
				}
				// 生成trace与feetrace
				for (int j = 1; j <= tLCInsureAccClassSet.size(); j++) {
					this.setLPInsureAccTrace(tLCInsureAccClassSet.get(j),
							tPrem, tMoneyType); // 催收的一期保费
					this.setLPInsureAccTrace(tLCInsureAccClassSet.get(j),
							Double.parseDouble("-" + tFee), "GL"); // 本次保费的初始扣费
					this.setLPInsureAccFeeTrace(tLCInsureAccClassSet.get(j),
							tFee, 0, mFeeCode, "GL"); // 扣费轨迹
					if (tJL > 0) {
						this.setLPInsureAccTrace(tLCInsureAccClassSet.get(j),
								tJL, "B"); // 持续奖励轨迹(acctrace)
					}
				}
				// 计算最后一期的交至日期
				this.aPaytoDate = PubFun.calDate(aPaytoDate, aPayIntv, "M", "");
			}
			// 处理ljsgetendorse
			if (!setGetEndorse(mLCPolSchema, tSumPrem, "000000", "BF")) {
				CError tError = new CError();
				tError.moduleName = "PEdorRVAppConfirmBL";
				tError.functionName = "setInsuredAcc";
				tError.errorMessage = "生成LjsGetEndorse失败!";
				mErrors.addOneError(tError);
				return false;
			}
		}else{
			tMoneyType = "BF";
			LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
			LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
			tLCInsureAccClassDB.setPolNo(mLCPolSchema.getPolNo());
			tLCInsureAccClassDB.setAccType(BQ.ACCTYPE_INSURED);
			tLCInsureAccClassSet = tLCInsureAccClassDB.query();
			if (tLCInsureAccClassSet.size() < 1) {
				CError tError = new CError();
				tError.moduleName = "PEdorRVAppConfirmBL";
				tError.functionName = "setInsuredAcc";
				tError.errorMessage = "查询帐户分类表数据失败!";
				mErrors.addOneError(tError);
				return false;
			}
			for (int j = 1; j <= tLCInsureAccClassSet.size(); j++) {
			this.setLPInsureAccTrace(tLCInsureAccClassSet.get(j),
					0, tMoneyType); 
			this.setLPInsureAccFeeTrace(tLCInsureAccClassSet.get(j),
					0, 0, mFeeCode, "GL"); // 扣费轨迹
			}
			if (!setGetEndorse(mLCPolSchema, 0.0, "000000", "BF")) {
				CError tError = new CError();
				tError.moduleName = "PEdorRVAppConfirmBL";
				tError.functionName = "setInsuredAcc";
				tError.errorMessage = "生成LjsGetEndorse失败!";
				mErrors.addOneError(tError);
				return false;
			}
			 
		}
		// 追加保费部分
		if (mAppMoney > 0) {
			if(mLoanMoney >0){
				LJSPaySchema tLJSPaySchema= mLJSPaySet.get(1);
				tLJSPaySchema.setSumDuePayMoney(tLJSPaySchema.getSumDuePayMoney()+mAppMoney);
				tLJSPaySchema.setModifyDate(aMakeDate);
				tLJSPaySchema.setModifyTime(aMakeTime);
				mMap.put(tLJSPaySchema, "UPDATE");
			}else{
				LJSPaySchema tLJSPaySchema= new LJSPaySchema();
				String getNoticeNo;				
				String mLimit = PubFun.getNoLimit(mLCPolSchema.getManageCom());
				String  tSerialNo = PubFun1.CreateMaxNo("SERIALNO", mLimit); 
				getNoticeNo = PubFun1.CreateMaxNo("PAYNOTICENO", mLimit);
				tLJSPaySchema.setGetNoticeNo(getNoticeNo);
				tLJSPaySchema.setOtherNo(mLPEdorItemSchema.getEdorAcceptNo());
		    tLJSPaySchema.setOtherNoType("10");
		    tLJSPaySchema.setAppntNo(mLCPolSchema.getAppntNo());
		    tLJSPaySchema.setSumDuePayMoney(mAppMoney);
		    tLJSPaySchema.setPayDate(aMakeDate);
		    tLJSPaySchema.setBankOnTheWayFlag("0");
		    tLJSPaySchema.setBankSuccFlag("0");
		    tLJSPaySchema.setSendBankCount(0);
		    tLJSPaySchema.setApproveCode(mLCPolSchema.getApproveCode());
		    tLJSPaySchema.setApproveDate(mLCPolSchema.getApproveDate());
		    tLJSPaySchema.setSerialNo(tSerialNo);
		    tLJSPaySchema.setOperator(aOperator);	
		    tLJSPaySchema.setMakeDate(aMakeDate);
		    tLJSPaySchema.setMakeTime(aMakeTime);
		    tLJSPaySchema.setModifyDate(aMakeDate);
		    tLJSPaySchema.setModifyTime(aMakeTime);		      	
		    tLJSPaySchema.setManageCom(mLCPolSchema.getManageCom());		      	
				tLJSPaySchema.setAgentCom(mLCPolSchema.getAgentCom());
				tLJSPaySchema.setAgentType(mLCPolSchema.getAgentType());
				tLJSPaySchema.setBankCode("");
				tLJSPaySchema.setRiskCode("000000");				
				tLJSPaySchema.setAgentGroup(mLCPolSchema.getAgentGroup());
				tLJSPaySchema.setAgentCode(mLCPolSchema.getAgentCode1());
				tLJSPaySchema.setAccName("");
				tLJSPaySchema.setPayDate(aMakeDate);
		      	
		    mMap.put(tLJSPaySchema, "DELETE&INSERT");
			}
			
			
			String tMoneyType2 = "ZF";
			double tFeeZF = getFee2();// 获得追加保费的初始扣费
			double tMoney = mAppMoney + Double.parseDouble("-" + tFeeZF);
			tSumMoney += tMoney;
			tSumFee += tFeeZF;
			LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
			LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
			tLCInsureAccClassDB.setPolNo(mLCPolSchema.getPolNo());
			tLCInsureAccClassDB.setAccType(BQ.ACCTYPE_INSURED);
			tLCInsureAccClassSet = tLCInsureAccClassDB.query();
			if (tLCInsureAccClassSet.size() < 1) {
				CError tError = new CError();
				tError.moduleName = "PEdorRVAppConfirmBL";
				tError.functionName = "setInsuredAcc";
				tError.errorMessage = "查询帐户分类表数据失败!";
				mErrors.addOneError(tError);
				return false;
			}
			// 生成trace与feetrace
			for (int j = 1; j <= tLCInsureAccClassSet.size(); j++) {
				this.setLPInsureAccTrace(tLCInsureAccClassSet.get(j),
						mAppMoney, tMoneyType2);
				this.setLPInsureAccTrace(tLCInsureAccClassSet.get(j),Double.parseDouble("-" + tFeeZF),
						"KF");
				this.setLPInsureAccFeeTrace(tLCInsureAccClassSet.get(j),
						tFeeZF, 0, mFeeCode, "KF");
			}
			// 处理ljsgetendorse,这里销售需要处理追加保费的佣金，所以PayPlanCode='222222'
			if (!setGetEndorse(mLCPolSchema, mAppMoney,
					BQ.PAYPLANCODE_SUPPLEMENTARY, "ZF")) {
				CError tError = new CError();
				tError.moduleName = "PEdorRVAppConfirmBL";
				tError.functionName = "setInsuredAcc";
				tError.errorMessage = "生成LjsGetEndorse失败!";
				mErrors.addOneError(tError);
				return false;
			}

		}else{
			if (!setGetEndorse(mLCPolSchema, 0,
					BQ.PAYPLANCODE_SUPPLEMENTARY, "ZF")) {
				CError tError = new CError();
				tError.moduleName = "PEdorRVAppConfirmBL";
				tError.functionName = "setInsuredAcc";
				tError.errorMessage = "生成LjsGetEndorse失败!";
				mErrors.addOneError(tError);
				return false;
			}
		}
		// 追加保费结束
    mMap.put(mLPInsureAccTraceSet, "DELETE&INSERT");
		mMap.put(mLPInsureAccFeeTraceSet, "DELETE&INSERT");
		// 设置帐户轨迹,轨迹记录包括本次加费总和,初始扣费
		// 循环ljspayset,计算每期的进入帐户金额及初始扣费

		// 如果是追加保费,调用追加的初始口费算法，置轨迹表类型为追加保费
		mLPInsureAccSchema
				.setSumPay(mLPInsureAccSchema.getSumPay() + tSumMoney);
		mLPInsureAccSchema.setInsuAccBala(mLPInsureAccSchema.getInsuAccBala()
				+ tSumMoney);
		mMap.put(mLPInsureAccSchema, "DELETE&INSERT");

		// 处理帐户分类表
		LCInsureAccClassSet tLCInsureAccClassSet2 = new LCInsureAccClassSet();
		LCInsureAccClassDB tLCInsureAccClassDB2 = new LCInsureAccClassDB();
		tLCInsureAccClassDB2.setPolNo(mLCPolSchema.getPolNo());
		tLCInsureAccClassDB2.setAccType(BQ.ACCTYPE_INSURED);
		tLCInsureAccClassSet2 = tLCInsureAccClassDB2.query();
		if (tLCInsureAccClassSet2.size() < 1) {
			CError tError = new CError();
			tError.moduleName = "PEdorRVAppConfirmBL";
			tError.functionName = "setInsuredAcc";
			tError.errorMessage = "查询帐户分类表数据失败!";
			mErrors.addOneError(tError);
			return false;
		}
		// 现在的主表和分类表是一一对应的
		
		for (int i = 1; i <= tLCInsureAccClassSet2.size(); i++) {
			LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
			LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
			tLCInsureAccClassSchema = tLCInsureAccClassSet2.get(i);
			ref.transFields(tLPInsureAccClassSchema, tLCInsureAccClassSchema);
			tLPInsureAccClassSchema.setEdorNo(aEdorNo);
			tLPInsureAccClassSchema.setEdorType(aEdorType);
			tLPInsureAccClassSchema.setModifyDate(aMakeDate);
			tLPInsureAccClassSchema.setModifyTime(aMakeTime);

			// 更新class的sumpay,insuaccbala累计trace条件payplancode,非初始扣费的
			tLPInsureAccClassSchema.setSumPay(tLPInsureAccClassSchema
					.getSumPay()
					+ tSumMoney);
			tLPInsureAccClassSchema.setInsuAccBala(tLPInsureAccClassSchema
					.getInsuAccBala()
					+ tSumMoney);
			mLPInsureAccClassSet.add(tLPInsureAccClassSchema);
			mMap.put(mLPInsureAccClassSet, "DELETE&INSERT");
		}

		// 根据补费和追费处理 初始扣费
		// 处理管理费帐户主表
		LCInsureAccFeeSchema tLCInsureAccFeeSchema = new LCInsureAccFeeSchema();
		LCInsureAccFeeSet tLCInsureAccFeeSet = new LCInsureAccFeeSet();
		LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
		tLCInsureAccFeeDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
		tLCInsureAccFeeDB.setPolNo(tLCInsureAccSchema.getPolNo());
		tLCInsureAccFeeDB.setRiskCode(tLCInsureAccSchema.getRiskCode());
		tLCInsureAccFeeSet = tLCInsureAccFeeDB.query();
		if (tLCInsureAccFeeSet.size() < 1) {
			CError tError = new CError();
			tError.moduleName = "PEdorZBAppConfirmBL";
			tError.functionName = "setInsuredAcc";
			tError.errorMessage = "查询帐户管理费分类表数据失败!";
			mErrors.addOneError(tError);
			return false;
		}
		tLCInsureAccFeeSchema = tLCInsureAccFeeSet.get(1);
		ref.transFields(mLPInsureAccFeeSchema, tLCInsureAccFeeSchema);
		mLPInsureAccFeeSchema.setEdorNo(aEdorNo);
		mLPInsureAccFeeSchema.setEdorType(aEdorType);
		mLPInsureAccFeeSchema.setFee(mLPInsureAccFeeSchema.getFee()
				+ Math.abs(tSumFee));
		mLPInsureAccFeeSchema.setModifyDate(aMakeDate);
		mLPInsureAccFeeSchema.setModifyTime(aMakeTime);
		mMap.put(mLPInsureAccFeeSchema, "DELETE&INSERT");
        //
		String sql = "delete from LPEdorEspecialData where EdorAcceptNo='"+mEdorAcceptNo+"' and EdorNo = '"
		+ aEdorNo + "'  and EdorType = '" + aEdorType + "' and DetailType='RV_FEE' with ur";
	
        mMap.put(sql, "DELETE");
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema=new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
		tLPEdorEspecialDataSchema.setDetailType("RV_FEE");
		tLPEdorEspecialDataSchema.setEdorNo(aEdorNo);
		tLPEdorEspecialDataSchema.setEdorValue(Double.toString(Math.abs(tSumFee)));
		tLPEdorEspecialDataSchema.setEdorType(aEdorType);
		mMap.put(tLPEdorEspecialDataSchema, "DELETE&INSERT");
		// 处理管理费帐户分类表
		LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
		LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
		tLCInsureAccClassFeeDB.setPolNo(mLCPolSchema.getPolNo());
		tLCInsureAccClassFeeDB.setAccType(BQ.ACCTYPE_INSURED);
		tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
		if (tLCInsureAccClassFeeSet.size() < 1) {
			CError tError = new CError();
			tError.moduleName = "PEdorRVAppConfirmBL";
			tError.functionName = "setInsuredAcc";
			tError.errorMessage = "查询帐户分类表数据失败!";
			mErrors.addOneError(tError);
			return false;
		}
		// TODO
		for (int i = 1; i <= tLCInsureAccClassFeeSet.size(); i++) {
			LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = new LCInsureAccClassFeeSchema();
			LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
			tLCInsureAccClassFeeSchema = tLCInsureAccClassFeeSet.get(i);
			ref.transFields(tLPInsureAccClassFeeSchema,
					tLCInsureAccClassFeeSchema);
			tLPInsureAccClassFeeSchema.setEdorNo(aEdorNo);
			tLPInsureAccClassFeeSchema.setEdorType(aEdorType);
			tLPInsureAccClassFeeSchema.setModifyDate(aMakeDate);
			tLPInsureAccClassFeeSchema.setModifyTime(aMakeTime);
			tLPInsureAccClassFeeSchema.setFee(tLPInsureAccClassFeeSchema
					.getFee()
					+ Math.abs(tSumFee));
			mLPInsureAccClassFeeSet.add(tLPInsureAccClassFeeSchema);
			
		}
		mMap.put(mLPInsureAccClassFeeSet, "DELETE&INSERT");
		return true;
	}

	/**
	 * 设置帐户领取轨迹
	 * 
	 * @param aLCInsureAccClassSchema
	 *            LCInsureAccClassSchema
	 * @param grpMoney
	 *            double
	 */
	private void setLPInsureAccTrace(
			LCInsureAccClassSchema aLCInsureAccClassSchema, double grpMoney,
			String MoneyType) {
		String serialNo;
		LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
		tLPInsureAccTraceDB.setEdorNo(aEdorNo);
		tLPInsureAccTraceDB.setEdorType(aEdorType);
		tLPInsureAccTraceDB.setContNo(aLCInsureAccClassSchema.getContNo());
		tLPInsureAccTraceDB.setPolNo(aLCInsureAccClassSchema.getPolNo());
		tLPInsureAccTraceDB
				.setInsuAccNo(aLCInsureAccClassSchema.getInsuAccNo());
		LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
		if (tLPInsureAccTraceSet.size() > 0 && delAccTrace.equals("0")) {
			// 删除lpinsuacctrace纪录
			// serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
			String sql = "delete from LPInsureAccTrace " + "where EdorNo = '"
					+ aEdorNo + "' " + "and EdorType = '" + aEdorType + "' ";
			// +"and SerialNo = '" + serialNo + "' ";
			mMap.put(sql, "DELETE");
			delAccTrace = "1";
		}

		serialNo = PubFun1.CreateMaxNo("SERIALNO", aLCInsureAccClassSchema
				.getManageCom());

		LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
		tLPInsureAccTraceSchema.setEdorNo(aEdorNo);
		tLPInsureAccTraceSchema.setEdorType(aEdorType);
		tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccClassSchema
				.getGrpContNo());
		tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccClassSchema
				.getGrpPolNo());
		tLPInsureAccTraceSchema.setContNo(aLCInsureAccClassSchema.getContNo());
		tLPInsureAccTraceSchema.setPolNo(aLCInsureAccClassSchema.getPolNo());
		tLPInsureAccTraceSchema.setSerialNo(serialNo);
		tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccClassSchema
				.getInsuAccNo());
		tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccClassSchema
				.getRiskCode());
		tLPInsureAccTraceSchema.setPayPlanCode(aLCInsureAccClassSchema
				.getPayPlanCode());
		tLPInsureAccTraceSchema.setManageCom(aLCInsureAccClassSchema
				.getManageCom());
		tLPInsureAccTraceSchema.setAccAscription("0");
		tLPInsureAccTraceSchema.setMoneyType(MoneyType);
		tLPInsureAccTraceSchema.setMoney(grpMoney);
		tLPInsureAccTraceSchema.setUnitCount("0");
		tLPInsureAccTraceSchema.setPayDate(mEdorValiDate);
		tLPInsureAccTraceSchema.setState("0");
		tLPInsureAccTraceSchema.setOtherNo(aEdorNo);
		tLPInsureAccTraceSchema.setOtherType("10");
		tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccTraceSchema.setMakeDate(aMakeDate);
		tLPInsureAccTraceSchema.setMakeTime(aMakeTime);
		tLPInsureAccTraceSchema.setModifyDate(aMakeDate);
		tLPInsureAccTraceSchema.setModifyTime(aMakeTime);
		mLPInsureAccTraceSet.add(tLPInsureAccTraceSchema);

	}


	/**
	 * 设置帐户领取轨迹
	 * 
	 * @param aLCInsureAccClassSchema
	 *            LCInsureAccClassSchema
	 * @param grpMoney
	 *            double
	 */
		private void setLPInsureAccFeeTrace(LCInsureAccClassSchema
            aLCInsureAccClassSchema,
            double manageMoney, double feeRate,
            String tFeeCode, String tMoneyType)
{
	String serialNo;
	LPInsureAccFeeTraceDB tLPInsureAccFeeTraceDB = new
	LPInsureAccFeeTraceDB();
	tLPInsureAccFeeTraceDB.setEdorNo(aEdorNo);
	tLPInsureAccFeeTraceDB.setEdorType(aEdorType);
	tLPInsureAccFeeTraceDB.setContNo(aLCInsureAccClassSchema.getContNo());
	tLPInsureAccFeeTraceDB.setPolNo(aLCInsureAccClassSchema.getPolNo());
	tLPInsureAccFeeTraceDB.setInsuAccNo(aLCInsureAccClassSchema.
                getInsuAccNo());
	LPInsureAccFeeTraceSet tLPInsureAccFeeTraceSet = tLPInsureAccFeeTraceDB.query();
	if (tLPInsureAccFeeTraceSet.size() > 0 && delFeeTrace.equals("0"))
	{

	String sql = "delete from LPInsureAccFeeTrace " +
				"where EdorNo = '" + aEdorNo + "' " +
				"and EdorType = '" + aEdorType + "' ";

				mMap.put(sql, "DELETE");
				this.delFeeTrace = "1";
	}

	serialNo = PubFun1.CreateMaxNo("SERIALNO",
           aLCInsureAccClassSchema.getManageCom());

	LPInsureAccFeeTraceSchema tLPInsureAccFeeTraceSchema = new
	LPInsureAccFeeTraceSchema();
	tLPInsureAccFeeTraceSchema.setEdorNo(aEdorNo);
	tLPInsureAccFeeTraceSchema.setEdorType(aEdorType);
	tLPInsureAccFeeTraceSchema.setGrpContNo(aLCInsureAccClassSchema.
                    getGrpContNo());
	tLPInsureAccFeeTraceSchema.setGrpPolNo(aLCInsureAccClassSchema.
                   getGrpPolNo());
	tLPInsureAccFeeTraceSchema.setContNo(aLCInsureAccClassSchema.getContNo());
	tLPInsureAccFeeTraceSchema.setPolNo(aLCInsureAccClassSchema.getPolNo());
	tLPInsureAccFeeTraceSchema.setSerialNo(serialNo);
	tLPInsureAccFeeTraceSchema.setInsuAccNo(aLCInsureAccClassSchema.
                    getInsuAccNo());
	tLPInsureAccFeeTraceSchema.setRiskCode(aLCInsureAccClassSchema.
                   getRiskCode());
	tLPInsureAccFeeTraceSchema.setOtherNo(aEdorNo);
	tLPInsureAccFeeTraceSchema.setOtherType("10");
	tLPInsureAccFeeTraceSchema.setPayPlanCode(aLCInsureAccClassSchema.
                      getPayPlanCode());
	tLPInsureAccFeeTraceSchema.setAccAscription("0");
	tLPInsureAccFeeTraceSchema.setMoneyType(tMoneyType);
	tLPInsureAccFeeTraceSchema.setPayDate(mEdorValiDate);
	tLPInsureAccFeeTraceSchema.setState("0");
	tLPInsureAccFeeTraceSchema.setManageCom(aLCInsureAccClassSchema.
                    getManageCom());
	tLPInsureAccFeeTraceSchema.setFeeCode(tFeeCode);

	tLPInsureAccFeeTraceSchema.setFee(Math.abs(manageMoney));
	tLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
	tLPInsureAccFeeTraceSchema.setMakeDate(aMakeDate);
	tLPInsureAccFeeTraceSchema.setMakeTime(aMakeTime);
	tLPInsureAccFeeTraceSchema.setModifyDate(aMakeDate);
	tLPInsureAccFeeTraceSchema.setModifyTime(aMakeTime);
	mLPInsureAccFeeTraceSet.add(tLPInsureAccFeeTraceSchema);

}
	/**
	 * 设置批改补退费表
	 * 
	 * @return boolean
	 */
	private boolean setGetEndorse(LCPolSchema aLCPolSchema, double edorPrem,
			String aPayPlanCode, String aFeeFinaType) {
		
		LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
		tLJSGetEndorseSchema.setGetNoticeNo(aEdorNo); // 给付通知书号码，没意义
		tLJSGetEndorseSchema.setEndorsementNo(aEdorNo);
		tLJSGetEndorseSchema.setFeeOperationType(aEdorType);
		tLJSGetEndorseSchema.setGrpContNo(aLCPolSchema.getGrpContNo());
		tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
		tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
		tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
		tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
		tLJSGetEndorseSchema.setGetMoney(Math.abs(edorPrem));
		tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
		tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
		tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
		tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
		tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
		tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
		tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
		tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
		tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
		tLJSGetEndorseSchema.setFeeFinaType(aFeeFinaType);
		tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
		tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
		tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
		tLJSGetEndorseSchema.setPayPlanCode(aPayPlanCode);
		tLJSGetEndorseSchema.setOtherNo(aEdorNo);
		tLJSGetEndorseSchema.setOtherNoType("10");
		tLJSGetEndorseSchema.setGetFlag("0");
		tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
		tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
		tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
		tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
		tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
		tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
		mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");		
		return true;
	}

	/**
	 * 设置item表中的费用和状态
	 */
	private void setEdorItem() {
		
		String sql = "update LPEdorItem "
				+ "set GetMoney = "
				+ (this.mLoanMoney + this.mAppMoney)
				+ ", "
				+ // 这里是负数
				"EdorState = '" + BQ.EDORSTATE_CAL + "', " + "ModifyDate = '"
				+ aMakeDate + "', " + "ModifyTime = '" + aMakeTime + "' "
				+ "where EdorNo = '" + aEdorNo + "' " + "and EdorType = '"
				+ aEdorType + "' ";
		mMap.put(sql.toString(), "UPDATE");
	}

	/** 获取初始费用 */
	private double getFee(LJSPayPersonSchema tLJSPayPersonSchema) {
		ExeSQL tExeSQL = new ExeSQL();
		double tFee = 0;
		int tPayCount = 0;
		double tPrem;

		// 欠费应交至日期
		String tPayDate = tLJSPayPersonSchema.getCurPayToDate();
		// 欠费的缴费期数
		tPayCount = PubFun.calInterval(mLCPolSchema.getCValiDate(), tPayDate,
				"M")
				/ mLCPolSchema.getPayIntv();
		// 期交保费金额
		tPrem = tLJSPayPersonSchema.getSumDuePayMoney();

		String tInsuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,
				mLCPolSchema.getRiskCode());
		// 查询管理费
		LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
		tLMRiskFeeDB.setInsuAccNo(tInsuAccNo);
		// tLMRiskFeeDB.setPayPlanCode(mPayPlanCode);
		tLMRiskFeeDB.setFeeKind("03"); // 03-个单管理费
		tLMRiskFeeDB.setFeeItemType("04"); // 04-初始扣费

		tLMRiskFeeDB.setFeeTakePlace("01"); // 01－交费时

		LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
		if (tLMRiskFeeDB.mErrors.needDealError()) {
			CError.buildErr(this, "账户管理费查询失败!");
			return -1;
		}
		mFeeCode = tLMRiskFeeSet.get(1).getFeeCode();
		tFee = calRiskFee(tLMRiskFeeSet.get(1), tPrem, tPayCount);
		return tFee;
	}

	private double getFee2() {
		double tFee = 0;
		double tPrem = mAppMoney;
		int tPayCount = 0;
		String tInsuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,
				mLCPolSchema.getRiskCode());
		// 查询管理费
		LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
		tLMRiskFeeDB.setInsuAccNo(tInsuAccNo);

		tLMRiskFeeDB.setFeeKind("03"); // 03-个单管理费
		tLMRiskFeeDB.setFeeItemType("04"); // 04-初始扣费
		tLMRiskFeeDB.setFeeTakePlace("06"); // 06－保全追加保费
		LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
		if (tLMRiskFeeDB.mErrors.needDealError()) {
			CError.buildErr(this, "账户管理费查询失败!");
			return -1;
		}
		mFeeCode = tLMRiskFeeSet.get(1).getFeeCode();
		tFee = calRiskFee(tLMRiskFeeSet.get(1), tPrem, tPayCount);
		return tFee;
	}

	private double calRiskFee(LMRiskFeeSchema pLMRiskFeeSchema,
			double dSumPrem, int CountTime) {
		double dRiskFee = 0.0;
		if (pLMRiskFeeSchema.getFeeCalModeType().equals("0")) // 0-直接取值
		{
			if (pLMRiskFeeSchema.getFeeCalMode().equals("01")) // 固定值内扣
			{
				dRiskFee = pLMRiskFeeSchema.getFeeValue();
			} else if (pLMRiskFeeSchema.getFeeCalMode().equals("02")) // 固定比例内扣
			{
				dRiskFee = dSumPrem * pLMRiskFeeSchema.getFeeValue();
			} else {
				dRiskFee = pLMRiskFeeSchema.getFeeValue(); // 默认情况
			}
		} else if (pLMRiskFeeSchema.getFeeCalModeType().equals("1")) // 1-SQL算法描述
		{
			// 准备计算要素
			Calculator tCalculator = new Calculator();
			tCalculator.setCalCode(pLMRiskFeeSchema.getFeeCalCode());
			// 累计已交保费
			tCalculator.addBasicFactor("Prem", String.valueOf(dSumPrem));
			// 下面三个要素用来计算个险万能首期交费的初始费用计算。added by huxl @20080512
			tCalculator.addBasicFactor("Amnt", String.valueOf(mLCPolSchema
					.getAmnt()));
			tCalculator.addBasicFactor("PayIntv", String.valueOf(mLCPolSchema
					.getPayIntv()));
			tCalculator.addBasicFactor("CountTime", String.valueOf(CountTime)); //

			String sCalResultValue = tCalculator.calculate();
			if (tCalculator.mErrors.needDealError()) {
				CError.buildErr(this, "管理费计算失败!");
				return -1;
			}

			try {
				dRiskFee = Double.parseDouble(sCalResultValue);
			} catch (Exception e) {
				CError.buildErr(this, "管理费计算结果错误!" + "错误结果：" + sCalResultValue);
				return -1;
			}
		}
		return dRiskFee;
	}

	private double getJL(LJSPayPersonSchema tLJSPayPersonSchema) {
		double fee = 0.0;
		double tPrem = tLJSPayPersonSchema.getSumDuePayMoney();
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		String tSql = "";
		// String tSql="select distinct CurPayToDate from ljspayperson where
		// getnoticeno='"+tLJSPaySchema.getGetNoticeNo()+"'";
		// 欠费应交至日期
		String tPayDate = tLJSPayPersonSchema.getCurPayToDate();
		int tInterval = PubFun.calInterval(mLCPolSchema.getCValiDate(),
				tPayDate, "Y");
		if (tInterval <= 4) {
			return -1;
		} else {
			// 累计部分领取的个人账户价值不超过累计追加保险费与累计已领取的持续奖金二项之和
			tSql = "select count(*) from ldsysvar where sysvar='onerow' and (select value(sum(money),0) from lcinsureacctrace where polno='"
					+ this.mLCPolSchema.getPolNo()
					+ "' and insuaccno='"
					+ this.mInsuAccNo
					+ "' and moneytype='LQ')<=(select value(sum(money),0) from lcinsureacctrace where polno='"
					+ this.mLCPolSchema.getPolNo()
					+ "' and insuaccno='"
					+ this.mInsuAccNo + "' and moneytype in('ZF','B'))";
			tSSRS = tExeSQL.execSQL(tSql);
			if (tSSRS == null) {
				
				return -2;
			}
			if (tSSRS.GetText(1, 1).equals("0")) {
				return -1;
			}
			fee = tPrem * 0.02;
		}
		return fee;
	}

	/**
	 * 返回理算结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		VData data = new VData();
		data.add(mMap);
		return data;
	}
}
