package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全减人存申请</p>
 * <p>Description: 把保全状态置为申请确认状态</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorTQDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;

    private Date mCValiDate = null;

    private Date mLastPayToDate = null;

    private Date mCurPayToDate = null;

    private String mMessage = "";

    private EdorItemSpecialData mSpecialData = null;

    private LPDiskImportSet mLPDiskImportSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到输入数据
     * @param data VData
     * @return boolean
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            LPGrpEdorItemSchema tLPGrpEdorItemSchema = (LPGrpEdorItemSchema) data.
                    getObjectByObjectName("LPGrpEdorItemSchema", 0);
            mSpecialData = (EdorItemSpecialData)data.
                    getObjectByObjectName("EdorItemSpecialData", 0);
            mEdorNo = tLPGrpEdorItemSchema.getEdorNo();
            mEdorType = tLPGrpEdorItemSchema.getEdorType();
            mGrpContNo = tLPGrpEdorItemSchema.getGrpContNo();
            LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
            tLPDiskImportDB.setEdorNo(mEdorNo);
            tLPDiskImportDB.setEdorType(mEdorType);
            tLPDiskImportDB.setGrpContNo(mGrpContNo);
            mLPDiskImportSet = tLPDiskImportDB.query();
            if (mLPDiskImportSet.size() == 0)
            {
                mErrors.addOneError("未录入被保人明细！");
                return false;
            }
            LCGrpPolSet tLCGrpPolSet = CommonBL.getLCGrpPolSet(mGrpContNo);
            if (tLCGrpPolSet.size() == 0)
            {
                mErrors.addOneError("未找到团体险种信息！");
                return false;
            }
            String cValiDate = tLCGrpPolSet.get(1).getCValiDate();
            System.out.println("生效日期" + cValiDate);
            if (cValiDate == null)
            {
                mErrors.addOneError("未找到保单生效日期，请检保单数据！");
                return false;
            }
            this.mCValiDate = CommonBL.stringToDate(cValiDate);
            LJAPayGrpSchema tLJAPayGrpSchema =
                    CommonBL.getPayToDate(tLCGrpPolSet.get(1).getGrpPolNo());
            this.mLastPayToDate = CommonBL.stringToDate(tLJAPayGrpSchema.
                    getLastPayToDate());
            this.mCurPayToDate = CommonBL.stringToDate(tLJAPayGrpSchema.
                    getCurPayToDate());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 校验传入的数据的合法性
     * @return boolean
     */
    private boolean checkData()
    {
        if (!checkEdorValiDate())
        {
            return false;
        }
        if (!checkHealthPol())
        {
            return false;
        }
        if (!checkClaim())
        {
            return false;
        }
        if (!checkLastPayToDate())
        {
            return false;
        }

        if (!checkBalIntv())
        {
            return false;
        }
        return true;
    }

    /**
      * 获得当前函件的序号
      * @return String
      */
     private String getSerialNumber()
     {
         //得到序号
         String sql = "select * from LGLetter " +
                 "where EdorAcceptNo = '" + mEdorNo + "' " +
                 "order by int(SerialNumber) desc ";
         LGLetterDB db = new LGLetterDB();
         LGLetterSet set = db.executeQuery(sql);
         if (set.size() == 0)
         {
             return "1";
         }
         int sn = Integer.parseInt(set.get(1).getSerialNumber());
         return String.valueOf(sn + 1);
     }

     /**
      * 检查保全生效日期
      * @return boolean
      */
     private boolean checkEdorValiDate()
     {
         try
         {
             for (int i = 1; i <= mLPDiskImportSet.size(); i++)
             {
                 LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.
                         get(i);
                 System.out.println("EdorValiDate:" + tLPDiskImportSchema.
                         getEdorValiDate());
                 System.out.println("EdorValiDate2:" + FDate.toString(mCValiDate));

                 Date edorValiDate = CommonBL.stringToDate(tLPDiskImportSchema.
                         getEdorValiDate());
                 if (edorValiDate == null)
                 {
                     continue;
                 }
                 if (edorValiDate.before(mCValiDate))
                 {
                     mErrors.addOneError("被保人" +
                             tLPDiskImportSchema.getInsuredName()
                             + "的保全生效日期不能早于保单生效日期！");
                     return false;
                 }
	             if (!has280101(mGrpContNo,tLPDiskImportSchema.getInsuredNo()) && edorValiDate.after(mCurPayToDate))
	             {
	                     mErrors.addOneError("被保人" +
	                             tLPDiskImportSchema.getInsuredName()
	                             + "保全生效日期不能晚于交至日期！");
	                     return false;
	             }
                 

                 //导入保费不能大于生效日期开始的实收保费
                 String edorPrem = tLPDiskImportSchema.getMoney2();
                 String insuredNo = tLPDiskImportSchema.getInsuredNo();
                 String contPlanCode = tLPDiskImportSchema.getContPlanCode();
                 double sumPayMoney = 0;
                 if (edorValiDate.before(mLastPayToDate) && (edorPrem != null)
                         && (!edorPrem.equals("")))
                 {
                     String sql = "select sum(a.SumActuPayMoney), a.LastPayToDate, a.CurPayToDate " +
                             "from LJAPayPerson a, LCPol b " +
                             "where a.PolNo = b.PolNo " +
                             "and a.GrpContNo = '" + mGrpContNo + "' " +
                             "and b.InsuredNo = '" + insuredNo + "' " +
                             "and b.ContPlanCode = '" + contPlanCode + "' " +
                             "and a.CurPayToDate > '" + tLPDiskImportSchema.getEdorValiDate() + "' " +
                             "group by a.PayCount, a.LastPayToDate, a.CurPayToDate " +
                             "order by PayCount ";
                     System.out.println(sql);
                     SSRS tSSRS = (new ExeSQL()).execSQL(sql);
                     for (int j = 1; j <= tSSRS.getMaxRow(); j++)
                     {
                         String sumActuPayMoney = tSSRS.GetText(j, 1);
                         String lastPayToDate = tSSRS.GetText(j, 2);
                         String curPayToDate = tSSRS.GetText(j, 3);
                         if (j == 1)
                         {
                             double sumMoney = Double.parseDouble(
                                     sumActuPayMoney) /
                                     PubFun.calInterval(lastPayToDate,
                                     curPayToDate, "D") *
                                     PubFun.calInterval(tLPDiskImportSchema.
                                     getEdorValiDate(), curPayToDate, "D");
                             sumPayMoney += sumMoney;
                         }
                         else
                         {
                             double sumMoney = Double.parseDouble(
                                     sumActuPayMoney);
                             sumPayMoney += sumMoney;
                         }
                     }
                     if (Double.parseDouble(edorPrem) > CommonBL.carry(sumPayMoney))
                     {
                         mErrors.addOneError("导入保费不能大于生效日期开始的实收保费" + CommonBL.carry(sumPayMoney) + "元！");
                         return false;
                     }
                 }
             }
         }
         catch (Exception ex)
         {
             mErrors.addOneError("保全生效日期或导入保费校验出错！");
             ex.printStackTrace();
             return false;
         }
         return true;
     }

    /**
     * 检查是不是有健管的产品
     * @return boolean
     */
    private boolean checkHealthPol()
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            String appflag = tLCGrpPolSchema.getAppFlag();
            String riskCode = tLCGrpPolSchema.getRiskCode();
            if ((appflag != null) && (appflag.equals("1")) &&
                    (riskCode != null) && (riskCode.equals("170101")))
            {
                mMessage = "本单有" + riskCode + "险种，请打印内部流转单，发送健管人员\n";
                LGLetterSchema tLGLetterSchema = new LGLetterSchema();
                tLGLetterSchema.setEdorAcceptNo(mEdorNo);
                tLGLetterSchema.setSerialNumber(getSerialNumber());
                tLGLetterSchema.setLetterType("1"); //核保函件
                tLGLetterSchema.setLetterSubType("8"); //内部流转
                tLGLetterSchema.setState("0"); //待下发
                tLGLetterSchema.setOperator(mGlobalInput.Operator);
                tLGLetterSchema.setMakeDate(mCurrentDate);
                tLGLetterSchema.setMakeTime(mCurrentTime);
                tLGLetterSchema.setModifyDate(mCurrentDate);
                tLGLetterSchema.setModifyTime(mCurrentTime);
                mMap.put(tLGLetterSchema, "DELETE&INSERT");
                return true;
            }
        }
        return true;
    }

    /**
     * 校验是否发生过理赔
     * @return boolean
     */
    private boolean checkClaim()
    {
        GCheckClaimBL check = new GCheckClaimBL();
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.
                        get(i);
            String grpContNo = tLPDiskImportSchema.getGrpContNo();
            String insuredNo = tLPDiskImportSchema.getInsuredNo();
            String insuredName = tLPDiskImportSchema.getInsuredName();
            Date edorValiDate = CommonBL.stringToDate(tLPDiskImportSchema.
                        getEdorValiDate());
            if (edorValiDate != null)
            {
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setGrpContNo(grpContNo);
                tLCPolDB.setInsuredNo(insuredNo);
                LCPolSet tLCPolSet = tLCPolDB.query();
                for (int j = 1; j <= tLCPolSet.size(); j++)
                {
                    String polNo = tLCPolSet.get(j).getPolNo();
                    if (check.checkClaimed(polNo))
                    {
                        mMessage += "被保人" + insuredName + tLCPolSet.get(j).getRiskCode()
                                + "险种发生理赔，（该险种期交保费" + tLCPolSet.get(j).getPrem()
                                + "元），请审核录入的退费。\n";
                    }
                }
            }
        }
        return true;
    }

    /**
     * 校验保全生效日期
     * @return boolean
     */
    private boolean checkLastPayToDate()
    {
        try
        {
            for (int i = 1; i <= mLPDiskImportSet.size(); i++)
            {
                LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.
                        get(i);
                Date edorValiDate = CommonBL.stringToDate(tLPDiskImportSchema.
                        getEdorValiDate());
                if ((edorValiDate != null) && (edorValiDate.before(mLastPayToDate)))
                {
                    mMessage += "被保人" + tLPDiskImportSchema.getInsuredName() +
                        "申请的生效日早于当期保费应交日，如有需要，请特殊处理。\n";
                }
            }
        }
        catch (Exception ex)
        {
            mErrors.addOneError("保全生效日期校验出错！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 检验结算频次
     * @return boolean
     */
    private boolean checkBalIntv()
    {
        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpBalPlanDB.getInfo())
        {
            return true;
        }
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        if (!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("未找到保全受理信息！");
            return false;
        }
        Date edorAppDate = CommonBL.stringToDate(tLPEdorAppDB.getEdorAppDate());
        if (edorAppDate == null)
        {
            mErrors.addOneError("未找到保全受理日期！");
            return false;
        }
        int balIntv = tLCGrpBalPlanDB.getBalIntv();
//        System.out.println("balIntv" + balIntv);
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.
                    get(i);
            Date edorValiDate = CommonBL.stringToDate(tLPDiskImportSchema.
                    getEdorValiDate());
            if ((edorValiDate != null) && (edorValiDate.before(edorAppDate)))
            {
                int interval = PubFun.calInterval(edorValiDate, edorAppDate,
                        "M");
                System.out.println("interval" + interval);
                if (balIntv == 0)
                {
                    if (interval > 3)
                    {
                        mMessage += "被保人" + tLPDiskImportSchema.getInsuredName() +
                                "的保全生效日期超过保全受理日期三个月！\n";
                    }
                }
                else if ((balIntv == 1) || (balIntv == 3) || (balIntv == 12))
                {
                    if (interval > balIntv)
                    {
                        mMessage += "被保人" + tLPDiskImportSchema.getInsuredName() +
                                "生效日期与受理日期间隔超过保单服务提交周期！";
                    }
                }
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return boolean 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (!checkData())
        {
            return false;
        }
        setEdorState(BQ.EDORSTATE_INPUT);
        setSpecialData();
        return true;
    }

    /**
     * 把保全状态设为已申请状态
     * @return boolean
     */
    private void setEdorState(String edorState)
    {
        String sql;
        //更新main表
        sql = "update LPGrpEdorMain " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");

        //更新item表
        sql = "update LPGrpEdorItem  " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 设置保全项目特殊数据
     */
    public void setSpecialData()
    {
        mMap.put(mSpecialData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    
    /**
     * 重疾险种并且缴费频次为趸交的不受保全生效日期不能晚于交至日期的限制。
     * @return boolean
     */
    public static boolean has280101(String grpContNo, String InsuredNo)
    {
        
    	SSRS tSSRS = new ExeSQL().execSQL("select payintv,RISKCODE From lcpol where grpcontno ='"+grpContNo+"' and insuredno ='"+InsuredNo+"' ");
    	for( int i = 1 ; i<= tSSRS.getMaxRow() ; i++)
    	{
    		String payintv = tSSRS.GetText(i, 1);
    		String riskcode = tSSRS.GetText(i, 2);
    		if(payintv.equals("0")&& riskcode.equals("280101"))
    		{
    			return true;
    		}
    	}
        return false;
    }
}
