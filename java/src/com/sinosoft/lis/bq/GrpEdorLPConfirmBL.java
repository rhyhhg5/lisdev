package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class GrpEdorLPConfirmBL
		implements EdorConfirm
{
	/** 往后面传输数据的容器 */
	private VData mInputData;

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

        /** 保全号 */
        private String mEdorNo = null;

        /** 保全类型 */
        private String mEdorType = "";

        /** 磁盘导入数据  */
        private LPDiskImportSet mLPDiskImportSet = null;

        /** 团体合同号 */
        private String mGrpContNo = null;

	/**  */
	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private MMap map = new MMap();
	boolean newaddrFlag = false;

        private String mCurrentDate = PubFun.getCurrentDate();

        private String mCurrentTime = PubFun.getCurrentTime();

	public GrpEdorLPConfirmBL()
	{}

/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据
	 *         cOperate 数据操作
	 * @return:
	 */

	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		mOperate = cOperate;

		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData())
		{
			return false;
		}
		System.out.println("---End getInputData---");

		//数据准备操作
		if (!prepareData())
		{
			return false;
		}
		System.out.println("---End prepareData---");
		if (!prepareOutputData())
		{
			return false;
		}

//        // 数据操作业务处理
//        PEdorConfirmBLS tPEdorConfirmBLS = new PEdorConfirmBLS();
//        System.out.println("Start Confirm BB BL Submit...");
//        if (!tPEdorConfirmBLS.submitData(mInputData, mOperate))
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tPEdorConfirmBLS.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "PEdorConfirmBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "数据提交失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }

		return true;
	}

	public VData getResult()
	{
		return mResult;
	}

	private boolean prepareOutputData()
	{

		mResult.clear();
		mResult.add(map);

		return true;
	}

/**
	 * 从输入数据中得到所有对象
	 * @return
	 */
	private boolean getInputData()
	{
		try
		{
			mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) mInputData.
					getObjectByObjectName("LPGrpEdorItemSchema",
										  0);
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
					"GlobalInput", 0);
                        mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
                        mEdorType = mLPGrpEdorItemSchema.getEdorType();
                        mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
                        mLPDiskImportSet = CommonBL.getLPDiskImportSet
                                           (mEdorNo, mEdorType, mGrpContNo,
                                            BQ.IMPORTSTATE_SUCC);

		}
		catch (Exception e)
		{
			CError.buildErr(this, "接收数据失败");
			return false;
		}

		return true;
	}

/**
	 * 准备需要保存的数据
	 */
	private boolean prepareData()
	{

            String[] tables = {"LCInsured","LCGrpAppnt"};
            ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,mEdorNo, mEdorType,mGrpContNo,"GrpContNo");
//            if (!validate.submitData()) {
//                mErrors.copyAllErrors(validate.mErrors);
//                return false;
//            }
            validate.changeData(tables);
            map.add(validate.getMap());

            for (int i = 1; i <= mLPDiskImportSet.size(); i++) {
                LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
                LBDiskImportSchema tLBDiskImportSchema = new LBDiskImportSchema();
                Reflections ref = new Reflections();
                ref.transFields(tLBDiskImportSchema, tLPDiskImportSchema);
                map.put(tLBDiskImportSchema, "DELETE&INSERT");
            }

            String delDiskImport="delete from lpdiskimport where edorno ='"+
                                 this.mEdorNo+"' and edortype='"+this.mEdorType+"'";
            map.put(delDiskImport,"DELETE");
            return true;
	}

        /**
         *
         * @param aLPDiskImportSchema LPDiskImportSchema
         * @return LCInsuredSet
         */
        private LCInsuredSet getLCInsured(LPDiskImportSchema aLPDiskImportSchema) {
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setGrpContNo(aLPDiskImportSchema.getGrpContNo());
            String insuredNo = aLPDiskImportSchema.getInsuredNo();
            if (insuredNo != null) {
                tLCInsuredDB.setInsuredNo(insuredNo);
                return tLCInsuredDB.query();
            } else {
                tLCInsuredDB.setName(aLPDiskImportSchema.getInsuredName());
                tLCInsuredDB.setSex(aLPDiskImportSchema.getSex());
                tLCInsuredDB.setBirthday(aLPDiskImportSchema.getBirthday());
                tLCInsuredDB.setIDType(aLPDiskImportSchema.getIDType());
                tLCInsuredDB.setIDNo(aLPDiskImportSchema.getIDNo());
                return tLCInsuredDB.query();
            }
        }

	public TransferData getReturnTransferData()
	{
		return null;
	}

	public CErrors getErrors()
	{
		return mErrors;
	}

	public static void main(String[] args)
	{
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.Operator = "001";
		tGlobalInput.ManageCom = "86";

		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorAcceptNo("86110000000436");
		tLPGrpEdorItemSchema.setEdorNo("430110000000143");
		tLPGrpEdorItemSchema.setEdorAppNo("430110000000143");
		tLPGrpEdorItemSchema.setGrpContNo("0000034201");
		tLPGrpEdorItemSchema.setEdorType("AC");

		VData tVData = new VData();
		tVData.add(tGlobalInput);
		tVData.add(tLPGrpEdorItemSchema);

		GrpEdorACConfirmBL tGrpEdorACConfirmBL = new GrpEdorACConfirmBL();
		if (!tGrpEdorACConfirmBL.submitData(tVData, ""))
		{
			System.out.println(tGrpEdorACConfirmBL.mErrors.getErrContent());
		}
	}

}
