package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 计算账户利息
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GEdorMJCalInterestBL
{
    /** 错误的容器*/
    public CErrors mErrors = new CErrors();

    /**团体险种号*/
    private String mGrpPolNo = null;
    /**账户类型，见BQ类中ACCTYPE_*定义*/
    private String mAccType = null;
    /**账户余额预定利息*/
    private double mAccRate = 0;
    /**账户利息*/
    private double mInterest = 0;

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
    private EdorItemSpecialData mEdorItemSpecialData = null;

    public GEdorMJCalInterestBL()
    {
    }

    /**
     * 公共的数据提交方法
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!check())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        if(!submit())
        {
            return false;
        }

        return true;
    }

    /**
     * 提交操作
     * @return boolean
     */
    private boolean submit()
    {
        MMap map = new MMap();
        map.put(mEdorItemSpecialData.getSpecialDataSet(), "DELETE&INSERT");
        VData tVData = new VData();
        tVData.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(tVData, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 校验操作的合法性
     * @return boolean
     */
    private boolean check()
    {


        return true;
    }

    /**
     * 处理数据，为计算做准备
     * @return boolean
     */
    private boolean dealData()
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(mGrpPolNo);
        if (!tLCGrpPolDB.getInfo())
        {
            mErrors.addOneError("没有查询到团体险种信息");
            System.out.println("没有查询到团体险种好为" + mGrpPolNo + "的险种信息");
            return false;
        }
        int payIntV = tLCGrpPolDB.getPayIntv();
        AccountManage tAccountManage = new AccountManage();
        mInterest = tAccountManage.
                    getAccInterest(mGrpPolNo, payIntV, mAccType, mAccRate);

        //生成特殊数据
        mEdorItemSpecialData = new EdorItemSpecialData(mLPGrpEdorItemSchema);
        mEdorItemSpecialData.add(BQ.DETAILTYPE_ACCRATE, String.valueOf(mAccRate)); //利率
        String interestType = "";
        if(mAccType.equals(BQ.ACCTYPE_FIXED))
        {
            interestType = BQ.DETAILTYPE_INTEREST_FIXED;  //团体固定账户利息
        }
        else if(mAccType.equals(BQ.ACCTYPE_GROUP))
        {
            interestType = BQ.DETAILTYPE_INTEREST_GROUP; //团体医疗账户利息
        }
        else if(mAccType.equals(BQ.ACCTYPE_INSURED))
        {
            interestType = BQ.DETAILTYPE_INTEREST_INSURED;  //个人账户利息
        }
        mEdorItemSpecialData.add(interestType, String.valueOf(mInterest));

        return true;
    }

    /**
     * 得到传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput)cInputData
                       .getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData)cInputData
                        .getObjectByObjectName("TransferData", 0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema)cInputData
                        .getObjectByObjectName("LPGrpEdorItemSchema", 0);

        if(mGlobalInput == null
           || mTransferData == null
            || mLPGrpEdorItemSchema == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        mGrpPolNo = (String)mTransferData.getValueByName("grpPolNo");
        mAccType = (String)mTransferData.getValueByName("accType");
        try
        {
            mAccRate = Double.parseDouble((String) mTransferData
                                          .getValueByName("accRate"));
        }
        catch(Exception e)
        {
            e.printStackTrace();
            mErrors.addOneError("传入的数据不完整");
        }

        return true;
    }

    /**
     * 得到账户利息
     * @return double
     */
    public double getInterest()
    {
        return mInterest;
    }

    public static void main(String args[])
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorNo("20060112000003");
        tLPGrpEdorItemSchema.setEdorAcceptNo(tLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemSchema.setEdorType("MJ");

        //团体医疗
        TransferData t = new TransferData();
        t.setNameAndValue("grpPolNo", "2200005569");
        t.setNameAndValue("accType", BQ.ACCTYPE_FIXED);
        t.setNameAndValue("accRate", "0.01");

        VData v = new VData();
        v.add(g);
        v.add(t);
        v.add(tLPGrpEdorItemSchema);

        GEdorMJCalInterestBL bl = new GEdorMJCalInterestBL();
        if(!bl.submitData(v, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println(bl.getInterest());
        }

        //团体固定
        v.remove(t);
        t.removeByName("accType");
        t.setNameAndValue("accType", BQ.ACCTYPE_GROUP);
        v.add(t);
        if(!bl.submitData(v, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println(bl.getInterest());
        }

        //个人
        v.remove(t);
        t.removeByName("accType");
        t.setNameAndValue("accType", BQ.ACCTYPE_INSURED);
        v.add(t);
        if(!bl.submitData(v, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println(bl.getInterest());
        }


    }
}
