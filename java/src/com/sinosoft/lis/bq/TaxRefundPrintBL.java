package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TaxRefundPrintBL  implements PrintService{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private VData mResult = new VData();
    private LCContSchema mLCContSchema = new LCContSchema();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    TextTag textTag = new TextTag(); //新建一个TextTag的实例
    private String tPolYear;//页面传的年度
    private String tContNo;//页面传的保单号
    
    private ExeSQL mExeSQL=new ExeSQL();
    private String mOperate="";

    public TaxRefundPrintBL() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("TaxRefundPrintBL begin");
        mOperate=cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();
        
        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        
        if (!dealPrintMag()) {
            return false;
        }

        System.out.println("TaxRefundPrintBL end");
        return true;
    }



    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                               getObjectByObjectName("LOPRTManagerSchema", 0);
        //页面传的保单号
        tContNo=ttLOPRTManagerSchema.getStandbyFlag2();
        //页面传的年度
        tPolYear = ttLOPRTManagerSchema.getStandbyFlag1();
       
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        if (!tLCContDB.getInfo()) {
            // @@错误处理
			System.out.println("TaxRefundPrintBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "TaxRefundPrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "数据为空--打印入口没有得到足够的信息！";
			mErrors.addOneError(tError);
			return false;
        }
        mLCContSchema.setSchema(tLCContDB.getSchema());
        if(mLCContSchema==null){
//        	 @@错误处理
			System.out.println("TaxRefundPrintBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "TaxRefundPrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "数据表为空--VTS打印入口没有得到足够的信息！";
			mErrors.addOneError(tError);
			return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }
    
    private boolean getSchema() {
    	//获取投保人地址
        LCAppntDB tLCAppntDB = new LCAppntDB();
        //通过保单号(主键)获取客户表
        tLCAppntDB.setContNo(tContNo);
        if (!tLCAppntDB.getInfo()) {
            CError.buildErr(this,
                            "投保人表中缺少数据");
            return false;
        }
        LCAddressDB tLCAddressDB = new LCAddressDB();
        //通过客户号和地址号码获取地址表(这两个字段为主键)
        tLCAddressDB.setCustomerNo(tLCAppntDB.getAppntNo());
        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
        if(!tLCAddressDB.getInfo()){
        	// @@错误处理
			System.out.println("TaxRefundPrintBL+getPrintData++--");
			CError tError = new CError();
			tError.moduleName = "TaxRefundPrintBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "地址表缺少数据!";
			mErrors.addOneError(tError);
			return false;
        }
        mLCAddressSchema.setSchema(tLCAddressDB.getSchema());
        //查询LCPol的SQL
        String LCPolSQL = "select * from lcpol lcp "
        	       + "where conttype='1' and appflag='1' and "
        	       + "exists (select 1 from lmriskapp "
        	       + "where riskcode=lcp.riskcode and TaxOptimal='Y') and contno='"+ tContNo +"' "
        	       + "with ur";
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(LCPolSQL);
        if(null == tLCPolSet || tLCPolSet.size() ==0){
        	// @@错误处理
			System.out.println("TaxRefundPrintBL+getPrintData++--");
			CError tError = new CError();
			tError.moduleName = "TaxRefundPrintBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "获取保单信息失败!";
			mErrors.addOneError(tError);
        }else{
        	mLCPolSchema.setSchema(tLCPolSet.get(1));
        }
        return true;
    }

    private boolean getPrintData() {
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("TaxRefundPrint.vts", "printer");
        
        textTag.add("JetFormType", "SY001");
        if ("batch".equals(mOperate)) {
            textTag.add("previewflag", "0");
        } else {
            textTag.add("previewflag", "1");
        }
        if(!getSchema()){
        	return false;
        }
        
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            // @@错误处理
			System.out.println("TaxRefundPrintBL+getPrintData++--");
			CError tError = new CError();
			tError.moduleName = "TaxRefundPrintBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "操作员机构查询出错！";
			mErrors.addOneError(tError);
			return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);
        textTag.add("ManageComLength4", printcode);
        textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        textTag.add("Operator", mGlobalInput.Operator);

//      页眉
        textTag.add("ZipCode", mLCAddressSchema.getZipCode());
        textTag.add("Address", mLCAddressSchema.getPostalAddress());
        textTag.add("AppntName", mLCContSchema.getAppntName());

//		正文        
//      AppntName已有
        textTag.add("PolYear", tPolYear);//年度
        textTag.add("ContNo", tContNo);//保单号
        //根据险种编码去LMRisk表只中查险种名称
        textTag.add("RiskName", mExeSQL.getOneValue("select riskname from LMRisk where riskcode='"+mLCPolSchema.getRiskCode()+"'"));
        //得到保单生效日的SQL   
        String CValiDateSQL = "select max(temp.cvalidate) "
		        	     	+ "from (select cvalidate cvalidate,cinvalidate cinvalidate "
		        	     	+ "from lccont "
		        	     	+ "where conttype='1' and appflag='1' and contno='"+ tContNo +"' "
		        	     	+ "union all select cvalidate cvalidate,cinvalidate cinvalidate "
		        	     	+ "from lbcont where conttype='1' and appflag='1' and edorno like 'xb%' and contno "
		        	     	+ "in (select newcontno from lcrnewstatelog where contno='"+ tContNo +"' and state='6')) temp "
		        	     	+ "where temp.cvalidate <= date('"+ tPolYear +"'||'-12'||'-31') "
		        	     	+ "with ur";
        
        String CValiDate = mExeSQL.getOneValue(CValiDateSQL);
        textTag.add("CValiDate", CValiDate == "" ? mLCContSchema.getCValiDate() : CValiDate);//保单生效日
        //AppntName投保人已有
        textTag.add("InsuredName", mLCPolSchema.getInsuredName());//被保人姓名
        
        String SQL = "select sum(money) from LCInsureAccTrace where Contno='"+ tContNo+"' "
      	           + "and MoneyType='BR' and year(PayDate)='"+ Integer.toString(Integer.parseInt(tPolYear)+1) +"'";
        textTag.add("ReturnMoney", mExeSQL.getOneValue(SQL));//返还金额
        
        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("AgentPhone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
                           +
                " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                           +
                " (select agentgroup from laagent where agentcode ='"
                           + tLaAgentDB.getAgentCode() + "'))"
                           ;
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                branchSQL);
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());
        textTag.add("BarCode1", tContNo); //条形码下的保单号
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");//条形码

        setFixedInfo();
        
        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }
        
        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlexport.addListTable(tEndTable, tEndTitle);
        
        xmlexport.outputDocumentToFile("F:\\", "税优差额返还通知书");
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    private void setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLCContSchema.getManageCom());
        tLDComDB.getInfo();
      //  textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
      //  textTag.add("Fax", tLDComDB.getFax());
    }
    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(tContNo);
        mLOPRTManagerSchema.setOtherNoType("00");
        mLOPRTManagerSchema.setCode("SY001");
        mLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mLOPRTManagerSchema.setStandbyFlag2(tContNo); //这里存放保单号
        mResult.addElement(mLOPRTManagerSchema);
        
        return true;
    }

}
