package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.ListTable;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.schema.LPPolSchema;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 得到传入险种的现价表的XMlExport数据，不包括其他相关信息
 * 本类只支持一页现价的显示，分两栏，本类尚未实现动态分页的功能
 * 若纸张不是A4，则需要通过方法setRowCount来进行页行数的调整，调整到适合的行数，则可以打印多页
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class GetCashValueTableBL
{
    /** 错误的容器 */
    public CErrors mErrors = new CErrors();

    private int mRowCount = 40;  //每一栏显示的行数

    public GetCashValueTableBL()
    {
    }

    /**
     * 设计每一栏的最大行数
     * @param rowCount int：最大行数
     */
    public void setRowCount(int rowCount)
    {
        mRowCount = rowCount;
    }

    /**
     * 得到传入险种的现价表的XMlExport数据
     * @param tLCPolSchema LCPolSchema：待打印的现价险种
     * @return ListTable[]：左右两栏的现价表数据：失败则为null
     */
    public ListTable[] getCashTable(LCPolSchema tLCPolSchema)
    {
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();

        //获取现金价值计算方式
        tLMCalModeDB.setRiskCode(tLCPolSchema.getRiskCode());
        tLMCalModeDB.setType("X");
        LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();

        //解析得到的SQL语句
        String sql = "";
        //如果描述记录唯一时处理
        if(tLMCalModeSet.size() == 1)
        {
            sql = tLMCalModeSet.get(1).getCalSQL();
        }
        if(sql.equals(""))
        {
            mErrors.addOneError("险种没有现金价值表："
                                + tLCPolSchema.getRiskCode());
            return null;
        }

        Calculator calculator = new Calculator();
        //设置基本的计算参数
        calculator.addBasicFactor("InsuredSex", tLCPolSchema.getInsuredSex());
        calculator.addBasicFactor("InsuredAppAge",
                                  String.valueOf(tLCPolSchema.
                                                 getInsuredAppAge()));
        calculator.addBasicFactor("PayIntv",
                                  String.valueOf(tLCPolSchema.getPayIntv()));
        calculator.addBasicFactor("PayEndYear",
                                  String.valueOf(tLCPolSchema.getPayEndYear()));
        calculator.addBasicFactor("PayEndYearFlag",
                                  String.valueOf(tLCPolSchema.
                                                 getPayEndYearFlag()));
        calculator.addBasicFactor("PayYears",
                                  String.valueOf(tLCPolSchema.getPayYears()));
        calculator.addBasicFactor("InsuYear",
                                  String.valueOf(tLCPolSchema.getInsuYear()));
        calculator.addBasicFactor("Prem",
                                  String.valueOf(tLCPolSchema.getPrem()));
        calculator.addBasicFactor("Amnt",
                                  String.valueOf(tLCPolSchema.getAmnt()));
        calculator.addBasicFactor("FloatRate",
                                  String.valueOf(tLCPolSchema.getFloatRate()));
        calculator.addBasicFactor("InsuYearFlag",
                                  String.valueOf(tLCPolSchema.getInsuYearFlag()));
        calculator.addBasicFactor("GetYear",
                                  String.valueOf(tLCPolSchema.getGetYear()));
        calculator.addBasicFactor("GetYearFlag",
                                  String.valueOf(tLCPolSchema.getGetYearFlag()));
        calculator.addBasicFactor("CValiDate",
                                  String.valueOf(tLCPolSchema.getCValiDate()));
        calculator.setCalCode(tLMCalModeSet.get(1).getCalCode());
        sql = calculator.getCalSQL();
        System.out.println("Calculate Sql: " + sql);

        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("没有查询到险种现金价值表："
                                + tLCPolSchema.getRiskCode());
            return null;
        }

        return setTable(tSSRS);
    }

    /**
     * 设置现价表
     * @param tSSRS SSRS：查询得到的现价信息
     * @return ListTable[]：生成的现价表结构，分左右两栏，
     * 若一页满了之后开始下一页，仍按两栏显示，但新页的左栏数据要接着上页的右栏数据
     */
    private ListTable[] setTable(SSRS tSSRS)
    {
        ListTable tListTableL = new ListTable();
        tListTableL.setName("CASHL");

        ListTable tListTableR = new ListTable();
        tListTableR.setName("CASHR");

        int fenceRows = 0;  //记录每一页当前处理的行数，若超过mRowCount*2，则初始化为1
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            System.out.println("cash index:" + i);

            fenceRows++;

            String[] info = new String[tSSRS.getMaxCol()];
            for(int t = 0; t < info.length; t++)
            {
                info[t] = tSSRS.GetText(i, t + 1);
            }
            info[info.length - 1] = CommonBL.bigDoubleToCommonString(
                Double.parseDouble(info[info.length - 1]), "0.00");

            //奇数个mRowCount行显示在左栏
            if(fenceRows <= mRowCount)
            {
                tListTableL.add(info);
            }
            //偶数个 mRowCount 行显示在右栏
            else if(fenceRows > mRowCount && fenceRows <= mRowCount * 2)
            {
                tListTableR.add(info);
            }
            //若大于fenceRows > mRowCount * 2 条数据，即左右都排满了，则下生成下一页数据
            else if(fenceRows > mRowCount * 2)
            {
                tListTableL.add(info);
                fenceRows = 1;  //这样可以保证下页数据放在了代表左栏的ListTable中
            }
        }

        ListTable tListTable[] = new ListTable[2];
        tListTable[0] = tListTableL;
        tListTable[1] = tListTableR;

        return tListTable;
    }

    /**
     * 得到传入险种的现价表的XMlExport数据
     * @param tLCPolSchema LCPolSchema：待打印的现价险种
     * @return ListTable[]：左右两栏的现价表数据：失败则为null
     */
    public ListTable[] getCashTable(LPPolSchema tLPPolSchema)
    {
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();

        //获取现金价值计算方式
        tLMCalModeDB.setRiskCode(tLPPolSchema.getRiskCode());
        tLMCalModeDB.setType("X");
        LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();

        //解析得到的SQL语句
        String sql = "";
        //如果描述记录唯一时处理
        if(tLMCalModeSet.size() == 1)
        {
            sql = tLMCalModeSet.get(1).getCalSQL();
        }
        else
        {
            mErrors.addOneError("险种没有现金价值表："
                                + tLPPolSchema.getRiskCode());
            return null;
        }
        if(sql.equals(""))
        {
            mErrors.addOneError("没有查询到现金价值表的获取方式："
                                + tLPPolSchema.getRiskCode());
            return null;
        }

        Calculator calculator = new Calculator();
        //设置基本的计算参数
        calculator.addBasicFactor("InsuredSex", tLPPolSchema.getInsuredSex());
        calculator.addBasicFactor("InsuredAppAge",
                                  String.valueOf(tLPPolSchema.
                                                 getInsuredAppAge()));
        calculator.addBasicFactor("PayIntv",
                                  String.valueOf(tLPPolSchema.getPayIntv()));
        calculator.addBasicFactor("PayEndYear",
                                  String.valueOf(tLPPolSchema.getPayEndYear()));
        calculator.addBasicFactor("PayEndYearFlag",
                                  String.valueOf(tLPPolSchema.
                                                 getPayEndYearFlag()));
        calculator.addBasicFactor("PayYears",
                                  String.valueOf(tLPPolSchema.getPayYears()));
        calculator.addBasicFactor("InsuYear",
                                  String.valueOf(tLPPolSchema.getInsuYear()));
        calculator.addBasicFactor("Prem",
                                  String.valueOf(tLPPolSchema.getPrem()));
        calculator.addBasicFactor("Amnt",
                                  String.valueOf(tLPPolSchema.getAmnt()));
        calculator.addBasicFactor("FloatRate",
                                  String.valueOf(tLPPolSchema.getFloatRate()));
        calculator.addBasicFactor("InsuYearFlag",
                                  String.valueOf(tLPPolSchema.getInsuYearFlag()));
        calculator.addBasicFactor("GetYear",
                                  String.valueOf(tLPPolSchema.getGetYear()));
        calculator.addBasicFactor("GetYearFlag",
                                  String.valueOf(tLPPolSchema.getGetYearFlag()));
        calculator.addBasicFactor("CValiDate",
                                  String.valueOf(tLPPolSchema.getCValiDate()));
        calculator.setCalCode(tLMCalModeSet.get(1).getCalCode());
        sql = calculator.getCalSQL();
        System.out.println("Calculate Sql: " + sql);

        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("没有查询到险种现金价值表："
                                + tLPPolSchema.getRiskCode());
            return null;
        }

        return setTable(tSSRS);
    }


    public static void main(String[] args)
    {
        GetCashValueTableBL tGetCashValueTableBL = new GetCashValueTableBL();
    }
}
