package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:预收保费理算类</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorYSAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private AppAcc mAppAcc = new AppAcc();

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到输入数据
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.
                    getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) cInputData.
                    getObjectByObjectName("LPEdorItemSchema", 0);
            mEdorNo = mLPEdorItemSchema.getEdorNo();
            mEdorType = mLPEdorItemSchema.getEdorType();
            mContNo = mLPEdorItemSchema.getContNo();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mContNo);
        if (!tLCAppntDB.getInfo())
        {
            mErrors.addOneError("未找到投保客户信息！");
            return false;
        }
        String customerNo = tLCAppntDB.getAppntNo();
        LCAppAccTraceSchema tLCAppAccTraceSchema =
                mAppAcc.getLCAppAccTrace(customerNo, mEdorNo, BQ.NOTICETYPE_P);
        setGetEndorse(tLCAppAccTraceSchema);
        setEdorItem(tLCAppAccTraceSchema.getMoney());
        return true;
    }

    /**
     * 设置批改补退费表
     * @return boolean
     */
    private boolean setGetEndorse(LCAppAccTraceSchema tLCAppAccTraceSchema)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if (!tLCContDB.getInfo())
        {
            mErrors.addOneError("未找到保单信息！");
            return false;
        }
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_BF);
        tLJSGetEndorseSchema.setGrpContNo(BQ.GRPFILLDATA);
        tLJSGetEndorseSchema.setContNo(mContNo);
        tLJSGetEndorseSchema.setGrpPolNo(BQ.GRPFILLDATA);
        tLJSGetEndorseSchema.setPolNo(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_P);
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setAppntNo(tLCAppAccTraceSchema.getCustomerNo());
        tLJSGetEndorseSchema.setInsuredNo(BQ.FILLDATA);
        tLJSGetEndorseSchema.setGetDate(mLPEdorItemSchema.getEdorValiDate());
        tLJSGetEndorseSchema.setGetMoney(tLCAppAccTraceSchema.getMoney());
        tLJSGetEndorseSchema.setRiskCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setRiskVersion(BQ.FILLDATA);
        tLJSGetEndorseSchema.setAgentCom(tLCContDB.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(tLCContDB.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(tLCContDB.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(tLCContDB.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(tLCContDB.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(tLCContDB.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(tLCContDB.getApproveTime());
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(tLCContDB.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 更新item表的保费
     */
    private void setEdorItem(double getMoney)
    {
        mLPEdorItemSchema.setGetMoney(getMoney);
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setModifyDate(mCurrentDate);
        mLPEdorItemSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPEdorItemSchema, "DELETE&INSERT");
    }
}
