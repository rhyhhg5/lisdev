package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 团单离职领取保全确认
 * </p>
 * <p>
 * Copyright: Copyright (c) 2006
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author qulq
 * @version 2.0
 */
public class GrpEdorSGConfirmBL implements EdorConfirm {
	/** 全局数据 */
	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null; // 保全项目信息

	private GlobalInput mGI = null; // 完整的操作员信息

	private ExeSQL mExeSQL = new ExeSQL();

	private String mEdorNo = null; // 受理号

	private String mEdorType = null; // 项目类型

	private String mGrpContNo = null; // 团单号

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	private MMap map = new MMap();

	public GrpEdorSGConfirmBL() {
	}

	/**
	 * 进行团单退保保全确认业务逻辑的处理并提交数据库。 将险种转移到B表，并存储保单状态表信息
	 * 
	 * @param cInputData
	 *            VData：包含： A. GlobalInput对象，完整的登陆用户信息。 B.
	 *            LPGrpEdorItem对象，保全项目信息。
	 * @param cOperate
	 *            String:此为“”
	 * @return boolean, 成功true，否则false
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据,将数据备份到本类中
		System.out.println("qulq ctconfirm");
		if (!getInputData(cInputData)) {
			return false;
		}
		// 数据准备操作
		if (!dealData()) {
			return false;
		}
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param: 无
	 * @return: boolean，操作成功true，否则false
	 */
	private boolean getInputData(VData data) {
		mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) data
				.getObjectByObjectName("LPGrpEdorItemSchema", 0);
		mGI = ((GlobalInput) data.getObjectByObjectName("GlobalInput", 0));

		if (mLPGrpEdorItemSchema == null || mGI == null) {
			this.mErrors.addOneError("接收数据失败");
			return false;
		}

		LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
		tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
		tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
		tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
		LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
		if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1) {
			mErrors.addOneError("查询保全项目信息失败！");
			System.out.println("GrpEdorWTConfirmBL->getInputData: "
					+ tLPGrpEdorItemDB.mErrors.getErrContent());
			return false;
		}
		mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemSet.get(1));

		mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
		mEdorType = mLPGrpEdorItemSchema.getEdorType();
		mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
		if (mEdorNo == null || mEdorType == null || mGrpContNo == null) {
			mErrors.addOneError("传入的数据不完整");
			return false;
		}

		return true;
	}

	/**
	 * 处理豫期撤保保全确认业务逻辑： A．若被保人险种全部全部撤保则被保人退保。 B．若个单保单被保人全部撤保，则个单撤保。
	 * C．若团单险种下个险全部退保，则团单险种退保。 D．若团单下所有个险全部退保，则团单退保。
	 * 
	 * @return boolean，操作成功true，否则false
	 */
	private boolean dealData() {
//		获取团单公共账户分单号
		String tContNo=mExeSQL.getOneValue("select contno from lcpol where poltypeflag='2' and grpcontno='"+mGrpContNo+"'");
		
	    ValidateEdorData2 mValidateEdorData = new ValidateEdorData2(mGI, mEdorNo,mEdorType, tContNo, "ContNo");

//	    在公共类中转移团单的数据
        String[] addTables = {"LCInsureAccTrace","LCInsureAccFeeTrace"};
        mValidateEdorData.addData(addTables);
        String[] chgTables = {"LCInsureAcc","LCInsureAccFee","LCInsureAccClass","LCInsureAccClassFee"};
        mValidateEdorData.changeData(chgTables);
        map.add(mValidateEdorData.getMap());
		mResult.clear();
		mResult.add(map);

		return true;
	}

	public static void main(String[] a) {
		LPGrpEdorItemDB db = new LPGrpEdorItemDB();
		db.setEdorNo("20061025000004");
		db.setEdorType("CT");

		GlobalInput gi = new GlobalInput();
		gi.Operator = "endor0";
		gi.ComCode = "86";

		VData data = new VData();
		data.add(db.query().get(1));
		data.add(gi);

		GrpEdorSGConfirmBL bl = new GrpEdorSGConfirmBL();
		if (!bl.submitData(data, "")) {
			System.out.println(bl.mErrors.getErrContent());
		}
	}

}
