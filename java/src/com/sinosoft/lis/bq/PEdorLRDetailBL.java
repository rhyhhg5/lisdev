package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单遗失补发项目明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Alex
 * @version 1.0
 */

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorLRDetailBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mNeedGetMoney = "Yes";
    private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LPEdorItemSchema mLPEdorItemSchema_input = new
            LPEdorItemSchema();
    //add by hyy
    private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();


    public PEdorLRDetailBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

            //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareData())
        {
            return false;
        }
        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorLRDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("PEdorLRDetailBL End PubSubmit");
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPEdorItemSchema_input = (LPEdorItemSchema) mInputData.
                                         getObjectByObjectName(
                    "LPEdorItemSchema",
                    0);
            // add by hyy
            mLPEdorEspecialDataSchema = (LPEdorEspecialDataSchema)mInputData
            .getObjectByObjectName("LPEdorEspecialDataSchema", 0);

        }
        catch (Exception e)
        {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorLRDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mGlobalInput == null || mLPEdorItemSchema == null)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorLRDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "输入数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorNo(mLPEdorItemSchema_input.getEdorNo());
        tLPEdorMainDB.setContNo(mLPEdorItemSchema_input.getContNo());
        LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.query();
        if (tLPEdorMainSet == null || tLPEdorMainSet.size() != 1)
        {
            // @@错误处理
            mErrors.copyAllErrors(tLPEdorMainDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorLRDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "无保全申请数据!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLPEdorMainSchema.setSchema(tLPEdorMainSet.get(1));
        mLPEdorMainSchema.setGetMoney(mLPEdorMainSchema.getGetMoney()+mLPEdorItemSchema_input.getGetMoney());
        if (!mLPEdorMainSchema.getEdorState().trim().equals("1"))
        {
            // @@错误处理
            mErrors.copyAllErrors(tLPEdorMainDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorLRDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保全已经申请确认不能修改!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema_input.getEdorNo());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema_input.getContNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema_input.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
        {
            // @@错误处理
            mErrors.copyAllErrors(tLPEdorItemDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorLRDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "无保全申请数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLPEdorItemSchema = tLPEdorItemSet.get(1);
        mLPEdorItemSchema.setGetMoney(mLPEdorItemSchema_input.getGetMoney());
        mLPEdorItemSchema.setEdorState("1");
        //处理工本费信息 add by hyy
        if(!getGBInfo())
        {
            return false;
        }
        return true;
    }

    /**
     * 处理工本费信息
     * @author hyy
     * @return boolean
     */
    private boolean getGBInfo()
    {
        //工本费记录
        if(mLPEdorEspecialDataSchema != null)
        {

            mMap.put(mLPEdorEspecialDataSchema, "DELETE&INSERT");
        }
        else
        {
            //若没有录入工本费信息，则删除之前可能存储了的工本费信息
            LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
            tLJSGetEndorseDB.setEndorsementNo(mLPEdorItemSchema.getEdorAcceptNo());
            tLJSGetEndorseDB.setFeeOperationType(mLPEdorItemSchema.getEdorType());
            tLJSGetEndorseDB.setFeeFinaType(BQ.FEEFINATYPE_GB);
            tLJSGetEndorseDB.setContNo(mLPEdorItemSchema.getContNo());
            mMap.put(tLJSGetEndorseDB.query(), "DELETE");

            LPEdorEspecialDataDB db = new LPEdorEspecialDataDB();
            db.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
            db.setEdorNo(mLPEdorItemSchema.getEdorNo());
            db.setEdorType(mLPEdorItemSchema.getEdorType());
            db.setPolNo(BQ.FILLDATA);
            db.setDetailType(BQ.DETAILTYPE_GB);
            mMap.put(db.query(), "DELETE");
        }

        return true;
    }
    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareData()
    {
        mLPEdorMainSchema.setEdorState("1");
        mLPEdorItemSchema.setEdorState("1");
        mMap.put(mLPEdorMainSchema, "UPDATE");
        mMap.put(mLPEdorItemSchema, "UPDATE");
        mResult.clear();
        mResult.add(mMap);

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

}
