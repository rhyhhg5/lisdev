package com.sinosoft.lis.bq;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContHangUpStateSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.SysConst;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PEdorFCConfirmBL  implements EdorConfirm  {

        /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LCContHangUpStateSchema mLCContHangUpStateSchema = new LCContHangUpStateSchema();
    StringBuffer sql = new StringBuffer();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();


    public PEdorFCConfirmBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        getInputData(cInputData);

        if (!prepareData()) {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData) {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
    }

    private boolean prepareData() {


        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "PEdorFCConfirmBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "查询保单信息失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCContSchema tLCContSchema = tLCContDB.getSchema();
        mLCContHangUpStateSchema.setContNo(tLCContSchema.getContNo());
        mLCContHangUpStateSchema.setAppntNo(tLCContSchema.getAppntNo());
        mLCContHangUpStateSchema.setHangUpType(BQ.HANGUPTYPE_BQ);
        mLCContHangUpStateSchema.setHangUpNo(mLPEdorItemSchema.getEdorAcceptNo());
        mLCContHangUpStateSchema.setState(BQ.HANGUSTATE_ON);
        mLCContHangUpStateSchema.setOperator(mGlobalInput.Operator);
        mLCContHangUpStateSchema.setMakeDate(PubFun.getCurrentDate());
        mLCContHangUpStateSchema.setMakeTime(PubFun.getCurrentTime());
        mLCContHangUpStateSchema.setModifyDate(PubFun.getCurrentDate());
        mLCContHangUpStateSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLCContHangUpStateSchema,SysConst.DELETE_AND_INSERT);
        return true ;
    }


    public VData getResult() {
    VData tVData = new VData();
    tVData.add(map);
    return tVData;

}

}
