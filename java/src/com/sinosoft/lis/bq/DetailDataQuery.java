package com.sinosoft.lis.bq;

import java.lang.reflect.Method;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;

/**
 * <p>Title: 保全明细查询类</p>
 * <p>Description: 先查P表，如果P表没有才查C表，每次根据主键得到一条记录</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class DetailDataQuery
{
    /** 保全号 */
    private String edorNo = null;

    /** 项目类型 */
    private String edorType = null;

    /** C表名称 */
    private String lcTableName = null;

    /** P表名称 */
    private String lpTableName = null;

    /** C表关键字的值 */
    private String[] keyValues = null;

    /**
     * 构造函数
     * @param edorNo String
     * @param edorType String
     */
    public DetailDataQuery(String edorNo, String edorType)
    {
        this.edorNo = edorNo;
        this.edorType = edorType;
    }

    /**
     * 得到明细数据
     * @param lcTableName String
     * @param keyValue String
     * @return Schema
     */
    public Schema getDetailData(String lcTableName, String keyValue)
    {
        String[] keyValues = new String[1];
        keyValues[0] = keyValue;
        return getDetailData(lcTableName, keyValues);
    }

    /**
     * 得到明细数据
     * @param lcTableName String
     * @param keyValues String[]
     * @return Schema
     */
    public Schema getDetailData(String lcTableName, String[] keyValues)
    {
        this.lcTableName = lcTableName;
        this.lpTableName = "LP" + lcTableName.substring(2);
        this.keyValues = keyValues;
        Schema lpSchema = queryFromLPTable();
        if (lpSchema != null)
        {
            return lpSchema;
        }
        Schema lcSchema = queryFromLCTables();
        if (lcSchema == null)
        {
            return null;
        }
        return transferSchema(lcSchema);
    }

    /**
     * 得到Schema主键查询条件
     * @return String
     */
    private String getKeyConditions()
    {
        String conditions = "";
        String schemaClassName = "com.sinosoft.lis.schema." + lcTableName +
                "Schema";
        try
        {
            Class schemaClass = Class.forName(schemaClassName);
            Schema schemaObject = (Schema) schemaClass.newInstance();
            String[] pk = schemaObject.getPK();
            for (int i = 0; i < pk.length; i++)
            {
                conditions += "and " + pk[i] + " = '" + keyValues[i] + "' ";
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
        return conditions;
    }

    /**
     * 从P表查询
     * @return Schema
     */
    private Schema queryFromLPTable()
    {
        String sql = "select * from " + lpTableName + " " +
                "where EdorNo = '" + edorNo + "' " +
                getKeyConditions() +
                "order by ModifyDate, ModifyTime desc ";
        String dbClassName = "com.sinosoft.lis.db." + lpTableName + "DB";
        try
        {
            Class dbClass = Class.forName(dbClassName);
            Object dbObject = (Object) dbClass.newInstance();
            Class[] paramType = new Class[1];
            paramType[0] = Class.forName("java.lang.String");
            Method queryMethod = dbClass.getMethod("executeQuery", paramType);
            Object[] args = new Object[1];
            args[0] = sql;
            SchemaSet set = (SchemaSet) queryMethod.invoke(dbObject, args);
            if (set.size() == 0)
            {
                return null;
            }
            return (Schema) set.getObj(1);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * 从C表查询
     * @return Schema
     */
    private Schema queryFromLCTables()
    {
        String dbClassName = "com.sinosoft.lis.db." + lcTableName + "DB";
        try
        {
            Class dbClass = Class.forName(dbClassName);
            Schema dbObject = (Schema) dbClass.newInstance();
            Class[] paramType = new Class[1];
            paramType[0] = Class.forName("java.lang.String");
            String[] pk = dbObject.getPK();
            if (pk.length != keyValues.length)
            {
                System.out.println("参数keyValues不正确！");
                return null;
            }
            for (int i = 0; i < pk.length; i++)
            {
                String methodName = "set" + pk[i];
                Method setMethod = dbClass.getMethod(methodName, paramType);
                Object[] args = new Object[1];
                args[0] = keyValues[i];
                setMethod.invoke(dbObject, args);
            }
            Method queryMethod = dbClass.getMethod("getInfo", null);
            queryMethod.invoke(dbObject, null);
            Method getSchemaMethod = dbClass.getMethod("getSchema", null);
            getSchemaMethod.invoke(dbObject, null);
            return (Schema) getSchemaMethod.invoke(dbObject, null);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * 把lcSchema转为lpSchema
     * @param lcSchema Schema
     * @return Schema
     */
    private Schema transferSchema(Schema lcSchema)
    {
        String schemaClassName = "com.sinosoft.lis.schema." + lpTableName + "Schema";
        String dbClassName = "com.sinosoft.lis.db." + lpTableName + "DB";
        try
        {
            Class schemaClass = Class.forName(schemaClassName);
            Schema schemaObject = (Schema) schemaClass.newInstance();
            Reflections ref = new Reflections();
            ref.transFields(schemaObject, lcSchema);

            Class dbClass = Class.forName(dbClassName);
            Class[] paramType = new Class[1];
            paramType[0] = Class.forName("java.lang.String");
            Method setEdorNoMethod = dbClass.getMethod("setEdorNo", paramType);
            Object[] args = new Object[1];
            args[0] = edorNo;
            setEdorNoMethod.invoke(schemaObject, args);
            Method setEdorTypeMethod = dbClass.getMethod("setEdorType",
                    paramType);
            args[0] = edorType;
            setEdorTypeMethod.invoke(schemaObject, args);
            return schemaObject;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args)
    {
        DetailDataQuery query = new DetailDataQuery("20060109000038", "PC");
        LPPolSchema tLPPolSchema = (LPPolSchema) query.getDetailData(
                "LCPol", "21000013867");
        System.out.println(tLPPolSchema.getPolNo());
    }
}
