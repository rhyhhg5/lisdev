package com.sinosoft.lis.bq;

import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 客户资料变更
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Tjj
 * @ReWrite ZhangRong,Luomin
 * @author rewrite by QiuYang 2005
 * @version 1.0
 */

public class GrpEdorRRConfirmBL implements EdorConfirm {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	private MMap mMap = new MMap();

	private DetailDataQuery mQuery = null;

	private GlobalInput mGlobalInput = null;

	private String mTypeFlag = null;

	private String stateFlag = null;

	private String mEdorNo = null;

	private String mEdorType = null;

	private String mStopDate = null;
	
	private String mResumeDate = null;

	private String mGrpContNo = null;

	private LCGrpContStateSchema mLCGrpContStateSchema = new LCGrpContStateSchema();
	
	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	
//	互换的数据
    private LCDutySet mLCDutySet = new LCDutySet();
    private LPDutySet mLPDutySet = new LPDutySet();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/**
	 * 提交数据
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	public boolean submitData(VData cInputData ,String a) {
		System.out.println("Begin --------GrpEdorRRConfirmlBL.java");
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData())// 检验数据未处理
		{
			return false;
		}

		if (!dealData()) {
			return false;
		}
		return true;
	}

    /**
     * 返回计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }
    
    
	/**
	 * 得到传入数据
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		try {
	        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
	                "GlobalInput", 0);
	        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.
	                getObjectByObjectName("LPGrpEdorItemSchema", 0);
	        mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
	        mEdorType = mLPGrpEdorItemSchema.getEdorType();
	        mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	/*
     * 
     * */
	private String getOccupationType(String cOccupationCode) {
		String sql = "select OccupationType from LDOccupation where '1218610553000'='1218610553000' and  OccupationCode = '"
				+ cOccupationCode + "'  fetch first 2000 rows only ";
		ExeSQL tExeSQL = new ExeSQL();
		String tOccupationType = tExeSQL.getOneValue(sql);
		return tOccupationType;
	}

	/**
	 * 校验数据
	 * 
	 * @return boolean
	 */
	private boolean checkData() {

		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		//查询 LCGrpContState
        String strSql = "select * from LCGrpContState where statetype = 'Stop' and state = '1' "
            +"and enddate is null and GrpContNo = '"+mGrpContNo+"'";
        LCGrpContStateDB tLCGrpContStateDB = new LCGrpContStateDB();
        try
        {
        	mLCGrpContStateSchema=tLCGrpContStateDB.executeQuery(strSql).get(1);
        }
        catch (Exception ex)
        {
            CError.buildErr(this, "查询团体保单状态表出现异常！");
            return false;
        }
        mStopDate=mLCGrpContStateSchema.getStartDate();
        strSql = "select edorvalue from lpedorespecialdata where detailtype = 'RESUMEDATE' and edorno = '"+mEdorNo+"' "
            +" and PolNo = '"+mGrpContNo+"'";
        mResumeDate=new ExeSQL().getOneValue(strSql);
        
        mLCGrpContStateSchema.setEndDate(mResumeDate);
        mLCGrpContStateSchema.setModifyDate(mCurrentDate);
        mLCGrpContStateSchema.setModifyTime(mCurrentTime);
        mLCGrpContStateSchema.setOtherNo(mEdorNo);
        mLCGrpContStateSchema.setOtherNoType("RR");
        mMap.put(mLCGrpContStateSchema, "UPDATE");
        
        //判断满期并设置相关状态
        strSql = "select edorvalue from lpedorespecialdata where detailtype = 'FLAG' and edorno = '"+mEdorNo+"' "
        +" and PolNo = '"+mGrpContNo+"'";
        String tFlag=new ExeSQL().getOneValue(strSql);
        if(tFlag.equals("1")){
        	LCGrpEdorDB tLCGrpEdorDB = new LCGrpEdorDB();
        	tLCGrpEdorDB.setEdorType("RS");
        	tLCGrpEdorDB.setGrpContNo(mGrpContNo);
        	tLCGrpEdorDB.getInfo();
        	LCGrpEdorSchema tLCGrpEdorSchema = new LCGrpEdorSchema();
        	tLCGrpEdorSchema=tLCGrpEdorDB.getSchema();
        	//设置配置表满期状态
        	tLCGrpEdorSchema.setState("1");
        	mMap.put(tLCGrpEdorSchema, "UPDATE");
        }
        

        Reflections tReflections = new Reflections();


        ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,
                mEdorNo, mEdorType,mGrpContNo,"GrpContNo");

        String[] tables = {"LCGrpCont", "LCGrpPol","LCCont", "LCPol", "LCPrem", "LCGet"};

        if (!validate.changeData(tables))
        {
            mErrors.copyAllErrors(validate.mErrors);
            return false;
        }
        MMap map = validate.getMap();
        mMap.add(map);

//      LCDUTY中没有grpcontno,土法子更新LCDuty和LPDuty表
        LPDutyDB tLPDutyDB = new LPDutyDB();
        LPDutySet tLPDutySet = new LPDutySet();
        tLPDutyDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPDutyDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPDutySet = tLPDutyDB.query();
        if (tLPDutySet.size() < 1)
        {
            CError.buildErr(this, "没有得到LPDuty表信息");
            return false;
        }
        for (int j = 1; j <= tLPDutySet.size(); j++)
        {
            LPDutySchema tLPDutySchema = new LPDutySchema();
            LCDutySchema tLCDutySchema = new LCDutySchema();
            tLPDutySchema = tLPDutySet.get(j);
            tReflections.transFields(tLCDutySchema, tLPDutySchema);
            tLCDutySchema.setModifyDate(mCurrentDate);
            tLCDutySchema.setModifyTime(mCurrentTime);
            mLCDutySet.add(tLCDutySchema);
        }

        LCDutyDB aLCDutyDB = new LCDutyDB();
        LCDutySet aLCDutySet = new LCDutySet();
        aLCDutySet = aLCDutyDB.executeQuery("select * from lcduty where contno in (select contno from lccont where grpcontno = '"+mGrpContNo+"' )");
        if (aLCDutySet.size() < 1)
        {
            CError.buildErr(this, "没有得到LCDuty表信息");
            return false;
        }
        for (int j = 1; j <= aLCDutySet.size(); j++)
        {
            LCDutySchema aLCDutySchema = new LCDutySchema();
            LPDutySchema aLPDutySchema = new LPDutySchema();
            aLCDutySchema = aLCDutySet.get(j);
            tReflections.transFields(aLPDutySchema, aLCDutySchema);
            aLPDutySchema.setEdorNo(mEdorNo);
            aLPDutySchema.setEdorType(mEdorType);
            mLPDutySet.add(aLPDutySchema);
        }

        mMap.put(mLCDutySet, "UPDATE");
        mMap.put(mLPDutySet, "DELETE&INSERT");

        System.out.println("GrpEdorRRConfirmBL.dealData() 成功");
        return true;
	}


	
	


	private void setLPEdoreSpecialData(String ZiDuanName, String value) {
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchema.setEdorNo(mEdorNo);
		tLPEdorEspecialDataSchema.setEdorType(mEdorType);
		tLPEdorEspecialDataSchema.setPolNo("000000");
		tLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorNo);
		tLPEdorEspecialDataSchema.setDetailType(ZiDuanName);
		tLPEdorEspecialDataSchema.setEdorValue(value);
		mMap.put(tLPEdorEspecialDataSchema, "DELETE&INSERT");
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit() {
		VData data = new VData();
		data.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}
}
