package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LPLoanDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPLoanSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

//程序名称：PEdorLQAppConfirm.java
//程序功能：
//创建日期：2008-04-10
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
public class PEdorLNAppConfirmBL implements EdorAppConfirm {
	private MMap mMap = new MMap();

	private GlobalInput mGlobalInput = null;

	/** 保全号 */
	private String mEdorNo = null;

	/** 保全类型 */
	private String mEdorType = null;

	/** 保全项目特殊数据 */
	private LPLoanSchema mLPLoanSchema = null;

	private LPEdorItemSchema mLPEdorItemSchema = null;

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/**
	 * 提交数据
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			System.out.println("PEdorLQAppConfirm.java->getInputData(cInputData)失败");
			return false;
		}

		if (!dealData()) {
			System.out.println("PEdorLQAppConfirm.java->dealData()失败");
			return false;
		}
		return true;
	}

	/**
	 * 返回理算结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		VData data = new VData();
		data.add(mMap);
		return data;
	}

	/**
	 * 得到传入参数
	 * 
	 * @param cInputData
	 *            VData
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		if (!getLPLoan()) {
			return false;
		}
		//在此处重新设置借款和还款日期
		String tLoanDate=PubFun.calDate(mCurrentDate, 3, "D", mCurrentDate);
		String tPayOffDate = PubFun.calDate(tLoanDate, 6, "M", tLoanDate);
		mLPLoanSchema.setLoanDate(tLoanDate);
		mLPLoanSchema.setPayOffDate(tPayOffDate);
		mLPLoanSchema.setModifyDate(mCurrentDate);
		mLPLoanSchema.setModifyTime(mCurrentTime);
		mMap.put(mLPLoanSchema, "UPDATE");
		
		//判断保单贷款是否需要收取印花税
		double stamp = 0;
		double getMoney = mLPLoanSchema.getSumMoney();
		double loanMoney = mLPLoanSchema.getSumMoney();
		if(loanMoney>2000){
			//贷款金额超过2000元，需要代扣印花税,
			stamp = (loanMoney-2000)*0.05/1000;
			
			getMoney = CommonBL.carry(loanMoney -stamp);
			
		}
		
		mLPEdorItemSchema.setGetMoney(-getMoney);
		mLPEdorItemSchema.setEdorValiDate(tLoanDate);
		mLPEdorItemSchema.setModifyDate(mCurrentDate);
		mLPEdorItemSchema.setModifyTime(mCurrentTime);
		mMap.put(mLPEdorItemSchema, "UPDATE");
		
		//设置财务数据
		LCPolSchema tLCPolSchema=new LCPolSchema();
		LCPolDB tLCPolDB=new LCPolDB();
		tLCPolDB.setPolNo(mLPLoanSchema.getPolNo());
		if(!tLCPolDB.getInfo()){
			// @@错误处理
			System.out.println("PEdorLNAppConfirmBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorLNAppConfirmBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取险种信息失败";
			mErrors.addOneError(tError);
			return false;
		}
		tLCPolSchema=tLCPolDB.getSchema();
//		if(stamp!=0){
			setGetEndorse(tLCPolSchema, stamp,BQ.FEEFINATYPE_MF);
//		}
        setGetEndorse(tLCPolSchema, mLPLoanSchema.getSumMoney(),BQ.FEEFINATYPE_TF);
		return true;
	}

	
	/**
	 * 设置item表中的费用和状态
	 */
	private boolean getLPLoan() {
		String tPolNo=new ExeSQL().getOneValue("select polno from lpedorespecialdata where edorno='"+mLPEdorItemSchema.getEdorNo()+"' and detailtype='LOAN'");
		if(tPolNo==null||tPolNo.equals("")){
			// @@错误处理
			System.out.println("PEdorLNAppConfirmBL+getLPLoan++--");
			CError tError = new CError();
			tError.moduleName = "PEdorLNAppConfirmBL";
			tError.functionName = "getLPLoan";
			tError.errorMessage = "险种号获取失败";
			mErrors.addOneError(tError);
			return false;
		}
		LPLoanDB tLPLoanDB=new LPLoanDB();
		tLPLoanDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPLoanDB.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPLoanDB.setPolNo(tPolNo);
		if(!tLPLoanDB.getInfo()){
			// @@错误处理
			System.out.println("PEdorLNAppConfirmBL+getLPLoan++--");
			CError tError = new CError();
			tError.moduleName = "PEdorLNAppConfirmBL";
			tError.functionName = "getLPLoan";
			tError.errorMessage = "获取贷款表失败";
			mErrors.addOneError(tError);
			return false;
		}
		mLPLoanSchema=tLPLoanDB.getSchema();
		return true;
	}

	/**
     * 设置批改补退费表
     * @return boolean
     */
    private boolean setGetEndorse(LCPolSchema aLCPolSchema, double edorPrem,String FeeFinaType)
    {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mLPEdorItemSchema.getEdorNo()); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mLPEdorItemSchema.getEdorNo());
        tLJSGetEndorseSchema.setFeeOperationType(mLPEdorItemSchema.getEdorType());
        tLJSGetEndorseSchema.setGrpContNo(aLCPolSchema.getGrpContNo());
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mLPEdorItemSchema.getEdorValiDate());
        if(BQ.FEEFINATYPE_MF.equals(FeeFinaType)){
        	tLJSGetEndorseSchema.setGetMoney(Math.abs(edorPrem));
        }else{
        	tLJSGetEndorseSchema.setGetMoney(-Math.abs(edorPrem));
        }
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(FeeFinaType);//万能部分领取的补/退费财务类型属于冲退保金
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mLPEdorItemSchema.getEdorNo());
        tLJSGetEndorseSchema.setOtherNoType("10");
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

}
