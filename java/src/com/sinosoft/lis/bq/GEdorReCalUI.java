package com.sinosoft.lis.bq;

/**
 * <p>Title: 个单保全重复理算</p>
 * <p>Description: 个单保全重复理算</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GEdorReCalUI
{
    private GEdorReCalBL mGEdorReCalBL = null;

    /**
     * 构造函数
     * @param edorAcceptNo String
     */
    public GEdorReCalUI(String edorNo)
    {
        mGEdorReCalBL = new GEdorReCalBL(edorNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mGEdorReCalBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mGEdorReCalBL.mErrors.getFirstError();
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String args[])
    {
        GEdorReCalUI tGEdorReCalUI = new GEdorReCalUI("20070416000004");
        tGEdorReCalUI.submitData();
    }
}
