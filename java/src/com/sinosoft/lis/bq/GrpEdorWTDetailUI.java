package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单犹豫期退保明细</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * ReWrite ZhangRong
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

public class GrpEdorWTDetailUI
{
    public CErrors mErrors = new CErrors();

    public GrpEdorWTDetailUI()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        GrpEdorWTDetailBL tGrpEdorWTDetailBL = new GrpEdorWTDetailBL();
        if(!tGrpEdorWTDetailBL.submitData(cInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpEdorWTDetailBL.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 主函数，测试用
     */
    public static void main(String[] args)
    {
        LPPENoticeSet tLPPENoticeSet = new LPPENoticeSet();

        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "endor0";
        tGlobalInput.ManageCom = "86";

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorAcceptNo("20060323000007");
        tLPGrpEdorItemSchema.setEdorNo(tLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPGrpEdorItemSchema.setGrpContNo("0000029304");
        tLPGrpEdorItemSchema.setEdorType("WT");

        LPContPlanRiskSet tLPContPlanRiskSet = new LPContPlanRiskSet();
        LPContPlanRiskSchema tLPContPlanRiskSchema = new LPContPlanRiskSchema();
        tLPContPlanRiskSchema.setGrpContNo(tLPGrpEdorItemSchema.getGrpContNo());
        tLPContPlanRiskSchema.setContPlanCode("A");
        tLPContPlanRiskSchema.setRiskCode("1601");
        tLPContPlanRiskSet.add(tLPContPlanRiskSchema);

        /*
         LPContPlanRiskSchema tLPContPlanRiskSchema2 = new LPContPlanRiskSchema();
         tLPContPlanRiskSchema2.setGrpContNo(tLPGrpEdorItemSchema.getGrpContNo());
              tLPContPlanRiskSchema2.setContPlanCode("11");
              tLPContPlanRiskSchema2.setRiskCode("1605");
              tLPContPlanRiskSet.add(tLPContPlanRiskSchema2);





              LPPENoticeSchema schema = new LPPENoticeSchema();
              schema.setEdorAcceptNo(tLPGrpEdorItemSchema.getEdorAcceptNo());
              schema.setEdorNo(tLPGrpEdorItemSchema.getEdorNo());
              schema.setEdorType(tLPGrpEdorItemSchema.getEdorType());
              schema.setProposalContNo("2300009646");
              schema.setPrtSeq("81000000029");
              tLPPENoticeSet.add(schema);

              LPPENoticeSchema schema2 = new LPPENoticeSchema();
              schema2.setEdorAcceptNo(tLPGrpEdorItemSchema.getEdorAcceptNo());
              schema2.setEdorNo(tLPGrpEdorItemSchema.getEdorNo());
              schema2.setEdorType(tLPGrpEdorItemSchema.getEdorType());
              schema2.setProposalContNo("2300009648");
              schema2.setPrtSeq("81000000020");
              tLPPENoticeSet.add(schema2);
         */

        LPEdorEspecialDataSchema special = new LPEdorEspecialDataSchema();
        special.setEdorAcceptNo(tLPGrpEdorItemSchema.getEdorAcceptNo());
        special.setEdorNo(tLPGrpEdorItemSchema.getEdorNo());
        special.setEdorType(tLPGrpEdorItemSchema.getEdorType());
        special.setDetailType(BQ.DETAILTYPE_GB);
        special.setEdorValue("10");

        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPGrpEdorItemSchema);
        tVData.add(tLPPENoticeSet);
        tVData.add(special);
        tVData.add(tLPContPlanRiskSet);

        GrpEdorWTDetailUI tGrpEdorWTDetailUI = new GrpEdorWTDetailUI();
        if(!tGrpEdorWTDetailUI.submitData(tVData, "INSERT"))
        {
            System.out.println(tGrpEdorWTDetailUI.mErrors.getErrContent());
        }
    }
}
