package com.sinosoft.lis.bq;

//程序名称：PEdorCBDetailBL.java
//程序功能：个人保全常无忧B给付补费
//创建日期：2010-07-21 16:49:22
//创建人  ：About Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorCBDetailBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 错误处理类 */
  public CErrors mErrors = new CErrors();
  /** 全局基础数据 */
  private GlobalInput mGlobalInput = null;
  private LPEdorItemSchema mLPEdorItemSchema = null;
  private MMap map = new MMap();
  private Reflections ref = new Reflections();
  private TransferData mTransferData = new TransferData();

  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();


  public PEdorCBDetailBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {

    //将操作数据拷贝到本类中
    this.mInputData = (VData) cInputData.clone();

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) 
    {
      return false;
    }

    if (!checkData()) 
    {
      return false;
    }

    //进行业务处理
    if (!dealData()) 
    {
      return false;
    }

    //准备往后台的数据
    if (!prepareData()) 
    {
      return false;
    }
    PubSubmit tSubmit = new PubSubmit();

    if (!tSubmit.submitData(mResult, "")) 
    { //数据提交
      // @@错误处理
      this.mErrors.copyAllErrors(tSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "PEdorCBDetailBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("PEdorCBDetailBL End PubSubmit");
    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData() {
    try 
    {
      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
      mLPEdorItemSchema = (LPEdorItemSchema)mInputData.getObjectByObjectName("LPEdorItemSchema", 0);
      mTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0); 
    }
    catch (Exception e) 
    {
      // @@错误处理
      e.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "PEdorCBDetailBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (mGlobalInput == null || mLPEdorItemSchema == null) 
    {
      CError tError = new CError();
      tError.moduleName = "PEdorCBDetailBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "输入数据有误!";
      this.mErrors.addOneError(tError);
      return false;
    }
    
    LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
    tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
    tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
    tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
    LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();

    if(tLPEdorItemSet == null || tLPEdorItemSet.size() < 1)
    {
        CError tError = new CError();
        tError.moduleName = "PEdorCBDetailBL";
        tError.functionName = "checkData";
        tError.errorMessage = "输入数据有误,LPEdorItem中没有相关数据!";
        this.mErrors.addOneError(tError);
        return false;
    }
    
    mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1).getSchema());
    if(mLPEdorItemSchema.getEdorState().trim().equals(BQ.EDORSTATE_CAL))
    {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "PEdorCBDetailBL";
        tError.functionName = "checkData";
        tError.errorMessage = "该保全已经保全理算，不能修改!";
        this.mErrors.addOneError(tError);
        return false;
    }
    
    return true;
  }
  
  /**
   * 根据前面的输入数据，进行校验处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean checkData()
  {
      return true;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * 因为退保项目必须在其他项目都结案之后才能进行，所以相应的保全数据可以从C表中得到
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() 
  {
	  
	  EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(mLPEdorItemSchema);
	  
	  tEdorItemSpecialData.setPolNo((String)mTransferData.getValueByName("polNo"));
	  tEdorItemSpecialData.add("OLDBONUS", (String)mTransferData.getValueByName("oldBonus"));
	  tEdorItemSpecialData.add("NEWBONUS", (String)mTransferData.getValueByName("newBonus"));
	  tEdorItemSpecialData.add("GETMONEY", (String)mTransferData.getValueByName("getMoney"));
	  
	  LPEdorEspecialDataSet tLPEdorEspecialDataSet = tEdorItemSpecialData.getSpecialDataSet();
	  map.put(tLPEdorEspecialDataSet, SysConst.DELETE_AND_INSERT);
	  
//	首先更新其后的保全项目的状态
	  mLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
	  mLPEdorItemSchema.setModifyDate(CurrentDate);
	  mLPEdorItemSchema.setModifyTime(CurrentTime);
	  map.put(mLPEdorItemSchema, "UPDATE");
	  
	  return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareData() {
    //mMap.put(mLPEdorMainSchema, "UPDATE");
    //mMap.put(mLPEdorItemSchema, "UPDATE");
    mResult.clear();
    mResult.add(map);

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

}
