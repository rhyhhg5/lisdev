package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.rmi.*;
import javax.xml.rpc.*;
import com.sinosoft.lis.message.*;

import java.util.*;

/**
 * <p>Title: 客户理赔短信通知</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class EdorGetMsgBL {
    private GlobalInput mG = new GlobalInput();
    private String mCurrentDate = PubFun.getCurrentDate();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public EdorGetMsgBL() {}

    private boolean getInputData(VData cInputData) 
    {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if(mG.Operator==null || mG.Operator.equals("") || mG.Operator.equals("null"))
    	{
    		mG.Operator="001";
    	}
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) {
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("XQMSG")) {
            sendMsg();
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean sendMsg() {
        System.out.println("客户领取短信通知批处理开始......");
        SMSClient smsClient = new SMSClient();
        
        Vector vec = new Vector();
        vec = getMessage();
        Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数
//      拆分Vector
        System.out.println("---------vec="+vec.size());

        if (!vec.isEmpty()) 
        {
        	for(int i=0;i<vec.size();i++)
            {            	
            	tempVec.clear();
            	tempVec.add(vec.get(i));
            	
            	SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
                msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                msgs.setExtension("024"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                msgs.setTaskValue(SMSClient.mes_taskVlaue15);
                msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                
                msgs.setMessages((SmsMessageNew[]) tempVec.toArray(new SmsMessageNew[tempVec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
            	smsClient.sendSMS(msgs);        	
            }
        } 
        else 
        {
            System.out.print("无符合条件的短信！");
        }
        return true;
    }

    private Vector getMessage() {
        Vector tVector = new Vector();
 
        
/* 
 * 短信发送条件:对客户在办理领取时所申领的金额＞1000元，且为委托代办，普通领取
 * 每种方式同样包含一条短信
 * 需要发给投保人
 *  
*/ 
       String tSQLnotify = "select "
    	     		 	 + "(select c.Mobile from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=b.contno "
    	     			 + " union select c.Mobile from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=b.contno), "
				       	 + "(select d.AppntName from lccont d where d.ContNo=b.ContNo union select d.AppntName from lbcont d where d.ContNo=b.ContNo),  "
				       	 + "(select (case appntsex when '1' then '女士' when '0' then '先生' else '先生/女士' end) from lccont d where d.ContNo=b.ContNo union select (case appntsex when '1' then '女士' when '0' then '先生' else '先生/女士' end) from lbcont d where d.ContNo=b.ContNo),  "
				       	 + "a.makedate , " 
				       	 + "b.contno, "
				       	 + "a.sumgetmoney, "
				       	 + "a.managecom, "
				       	 + "a.actugetno ,"
				       	 + "b.edoracceptno "
				       	 + "from ljaget a,lpedormain b  "
				       	 + "where a.otherno = b.edoracceptno and othernotype = '10'  "	
				       	 + "and a.sumgetmoney > 1000  "//申领金额大于1000
				       	 + "and exists ( select 1 from LGWork where b.edoracceptno=workno and ApplyTypeNo in ('1','2','4')) " //限制为委托代办
				         + "and not exists (select 1 from LCCSpec d where d.contno=b.contno and d.SpecType='bq01' and endorsementno = b.edoracceptno) "//没有发送过短信
				         + "and a.makedate <= current date "
				         + "and a.makedate > current date - 8 days " //暂时注释掉
				         //更新时去掉
				         //+ "fetch first 10 rows only "
				         + "with ur ";  //更新时取消
        ExeSQL tExeSQL=new ExeSQL();
        
        System.out.println(tSQLnotify);
        SSRS tMsgNotifySSRS =tExeSQL.execSQL(tSQLnotify);
        System.out.println("tMsgSuccSSRS.getMaxRow()共这些条："+tMsgNotifySSRS.getMaxRow());
        for (int i = 1; i <= tMsgNotifySSRS.getMaxRow(); i++) {
        	String tCustomerMobile = tMsgNotifySSRS.GetText(i, 1);
        	String tCustomerName = tMsgNotifySSRS.GetText(i, 2);
        	String tCustomerSex = tMsgNotifySSRS.GetText(i, 3);
        	String tMakeDate = tMsgNotifySSRS.GetText(i, 4);
        	String tContNo = tMsgNotifySSRS.GetText(i, 5);
        	String tSumGetMoney = tMsgNotifySSRS.GetText(i,6).replaceAll("-", "");
        	String tManageCom = tMsgNotifySSRS.GetText(i, 7);
        	String tActuGetNo = tMsgNotifySSRS.GetText(i, 8);
        	String tEndorsementNo = tMsgNotifySSRS.GetText(i, 9);
                if (tCustomerMobile == null || "".equals(tCustomerMobile) ||
                    "null".equals(tCustomerMobile)) {
                    continue;
                }

               if (tCustomerName == null || "".equals(tCustomerName) ||
                        "null".equals(tCustomerName)) {
                        continue;
                    }
                
                if (tCustomerSex == null || "".equals(tCustomerSex) ||
                    "null".equals(tCustomerSex)) {
                    continue;
                }
                
                if (tContNo == null || "".equals(tContNo) ||
                        "null".equals(tContNo)) {
                        continue;
                    }

                if (tMakeDate == null || "".equals(tMakeDate) ||
                        "null".equals(tMakeDate)) {
                        continue;
                    }
                
                if (tSumGetMoney == null || "".equals(tSumGetMoney) ||
                        "null".equals(tSumGetMoney)) {
                        continue;
                    }
                
//              发送给投保人的短信            
                SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
                String tCustomerContents = "尊敬的"+tCustomerName+tCustomerSex 
                                         + "，您好！您的保单号为"+tContNo
                                         +"的保单已于"+tMakeDate
                                         +"由您的委托代办人进行领取，领取金额为"+tSumGetMoney
                                         +"元。为维护您的权益，请您核实此情况。感谢您的支持，祝您健康。客服电话：95591。";      
                System.out.println("现金发送短信：" + tCustomerName + tContNo +"日期:"+tMakeDate +"金额:"+ tSumGetMoney+"投保人手机号："+ tCustomerMobile);
                tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                tCustomerMsg.setReceiver("18500905130");
                tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                //和IT讨论后，归类到二级机构
                tCustomerMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                tVector.add(tCustomerMsg);
            		          	
                //在LCCSpec表中插入值  ********************************************************  
                LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                tLCCSpecSchema.setGrpContNo("00000000000000000000");
                tLCCSpecSchema.setContNo(tContNo);
                tLCCSpecSchema.setProposalContNo("00000000000000000000");
                tLCCSpecSchema.setPrtSeq(tActuGetNo);//催收的 放 ljspay--actugetno                              
                String tLimit = PubFun.getNoLimit(tManageCom);
    	        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
    	        tLCCSpecSchema.setSerialNo(serNo); //流水号
                tLCCSpecSchema.setEndorsementNo(tEndorsementNo);//lccont--endorsementno 
                tLCCSpecSchema.setSpecType("bq01"); //保全客户在办理领取时所申领的金额＞1000元，且为委托代办时，发送短信至投保人短信类型
                tLCCSpecSchema.setSpecCode("001");//001-投保人 002-被保人 003-业务员
                tLCCSpecSchema.setSpecContent(tCustomerContents+"投保人手机号："+ tCustomerMobile);//短信内容
                tLCCSpecSchema.setOperator(mG.Operator);
                tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                tLCCSpecSchema.getDB().insert();
        }
        System.out.println("1------------"+tVector.size());
/* 
* 短信发送条件:对客户在办理领取时所申领的金额＞1000元，且为委托代办，满期领取
* 每种方式同样包含两条条短信
* 需要发给投保人和被保人
*  
*/ 
               String ttSQLnotify = "select "
        				       	 + "(select mobile from lcaddress x ,lcappnt y where x.customerno = y.appntno and x.addressno = y.addressno and y.contno = c.contno fetch first 1 rows only),  "
        				       	 + "c.appntname,  "
        				       	 + "(case c.appntsex when '1' then '女士' when '0' then '先生' else '先生/女士' end),  "
        				       	 + "(select mobile from lcaddress x ,lcinsured y where x.customerno = y.insuredno and x.addressno = y.addressno and y.contno = c.contno and y.insuredno = c.insuredno fetch first 1 rows only),  "
        				       	 + "c.Insuredname,  "
        				       	 + "(case c.Insuredsex when '1' then '女士' when '0' then '先生' else '先生/女士' end),  "
        				       	 + "(select max(makedate) from ljaget where actugetno = b.getnoticeno), " 
        				       	 + "c.contno,"
        				       	 + "sum(b.getmoney), "
        				       	 + "c.managecom, "
        				       	 + "b.getnoticeno, "
        				       	 + "c.appntno,c.insuredno "
        				       	 + "from  lccont c, ljsgetdraw b "
        				       	 + "where 1=1  "
        				       	 + "and (select max(makedate) from ljaget where actugetno = b.getnoticeno) between current date - 7 days and current date "//满期
        				       	 + "and b.feeoperationtype = 'EB' and feefinatype <> 'YEI' "
        				       	 + "and b.contno=c.contno and c.conttype='1' " //个单        		
        				       	 + "and b.handler='1' " //限制为委托代办
        				         + "and not exists (select 1 from LCCSpec d where d.prtseq=b.getnoticeno and d.SpecType='mj01' ) "//没有发送过短信
        				         + "and exists (select 1 from ljaget where actugetno = b.getnoticeno) " //已经给付确认的
        				         + "group by c.appntname, c.Insuredname, c.contno,c.managecom, b.getnoticeno, c.appntno,c.insuredno ,c.appntsex,c.Insuredsex " 
        				         + "having sum(b.getmoney) >= 1000 " 
        				         //更新时去掉
        				         //+ "fetch first 10 rows only "
        				         + "with ur ";
                ExeSQL ttExeSQL=new ExeSQL();
                
                System.out.println(ttSQLnotify);
                SSRS ttMsgNotifySSRS =ttExeSQL.execSQL(ttSQLnotify);
                System.out.println("tMsgSuccSSRS.getMaxRow()共这些条："+ttMsgNotifySSRS.getMaxRow());
                for (int i = 1; i <= ttMsgNotifySSRS.getMaxRow(); i++) {
                	String tCustomerMobile = ttMsgNotifySSRS.GetText(i, 1);
                	String tCustomerName = ttMsgNotifySSRS.GetText(i, 2);
                	String tCustomerSex = ttMsgNotifySSRS.GetText(i, 3);
                	String tInsuredMobile = ttMsgNotifySSRS.GetText(i, 4);
                	String tInsuredName = ttMsgNotifySSRS.GetText(i, 5);
                	String tInsuredSex = ttMsgNotifySSRS.GetText(i, 6);
                	String tMakeDate = ttMsgNotifySSRS.GetText(i, 7);
                	String tContNo = ttMsgNotifySSRS.GetText(i, 8);
                	String tSumGetMoney = ttMsgNotifySSRS.GetText(i,9).replaceAll("-", "");
                	String tManageCom = ttMsgNotifySSRS.GetText(i, 10);
                	String tActuGetNo = ttMsgNotifySSRS.GetText(i, 11);
                	String tAppntNo = ttMsgNotifySSRS.GetText(i, 12);
                	String tInsurdNo = ttMsgNotifySSRS.GetText(i, 13);
                	
                	if (tContNo == null || "".equals(tContNo) ||
                            "null".equals(tContNo)) {
                            continue;
                        }

                    if (tMakeDate == null || "".equals(tMakeDate) ||
                            "null".equals(tMakeDate)) {
                            continue;
                        }
                    
                    if (tSumGetMoney == null || "".equals(tSumGetMoney) ||
                            "null".equals(tSumGetMoney)) {
                            continue;
                        }
                    
                    if (tInsurdNo == null || "".equals(tInsurdNo) ||
                            "null".equals(tInsurdNo)) {
                            continue;
                        }
                    
                    if (tAppntNo == null || "".equals(tAppntNo) ||
                            "null".equals(tAppntNo)) {
                            continue;
                        }
                    
                    boolean customerFlag = true;
                    boolean insuredFlag = true;
                    
                    if (tCustomerMobile == null || "".equals(tCustomerMobile) ||
                            "null".equals(tCustomerMobile)) {
                    	customerFlag = false;
                        }
                    
                    if (tCustomerName == null || "".equals(tCustomerName) ||
                            "null".equals(tCustomerName)) {
                    	customerFlag = false;
                        }
                    
                    if (tCustomerSex == null || "".equals(tCustomerSex) ||
                            "null".equals(tCustomerSex)) {
                    	customerFlag = false;
                        }
                    
                    if(customerFlag)
                    {
//                      发送给投保人的短信            
                    	SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
                        
                        //短信内容
                        String tCustomerContents = "尊敬的"+tCustomerName+tCustomerSex 
                                                 + "，您好！您的保单号为"+tContNo
                                                 +"的保单已于"+tMakeDate
                                                 +"由您的委托代办人进行领取，领取金额为"+tSumGetMoney
                                                 +"元。为维护您的权益，请您核实此情况。感谢您的支持，祝您健康。客服电话：95591。";      
                        System.out.println("现金发送短信：" + tCustomerName + tContNo +"日期:"+tMakeDate +"金额:"+ tSumGetMoney +"投保人手机号："+ tCustomerMobile);
                        tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                        tCustomerMsg.setReceiver("18500905130");
                        tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                        //和IT讨论后，归类到二级机构
                        tCustomerMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素

                        tVector.add(tCustomerMsg);
                        
//                      在LCCSpec表中插入值  ******************************************************** 
                        LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                        tLCCSpecSchema.setGrpContNo("00000000000000000000");
                        tLCCSpecSchema.setContNo(tContNo);
                        tLCCSpecSchema.setProposalContNo("00000000000000000000");
                        tLCCSpecSchema.setPrtSeq(tActuGetNo);//催收的 放 ljspay--actugetno                              
                        String tLimit = PubFun.getNoLimit(tManageCom);
            	        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
            	        tLCCSpecSchema.setSerialNo(serNo); //流水号
                        tLCCSpecSchema.setSpecType("mj01"); //保全客户在办理领取时所申领的金额＞1000元，且为委托代办时，发送短信至投保人及被保险人短信类型
                        tLCCSpecSchema.setSpecCode("001");//001-投保人 002-被保人 003-业务员
                        tLCCSpecSchema.setSpecContent(tCustomerContents+"投保人手机号："+ tCustomerMobile);//短信内容
                        tLCCSpecSchema.setOperator(mG.Operator);
                        tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.getDB().insert();
                    }
                    
                    if (tInsuredMobile == null || "".equals(tInsuredMobile) ||
                            "null".equals(tInsuredMobile)) {
                    	insuredFlag = false;
                        }
                	if (tInsuredName == null || "".equals(tInsuredName) ||
                            "null".equals(tInsuredName)) {
                		insuredFlag = false;
                        }
                	if (tInsuredSex == null || "".equals(tInsuredSex) ||
                            "null".equals(tInsuredSex)) {
                		insuredFlag = false;
                        }
                	
                	if(insuredFlag)
                	{
                		if (!tAppntNo.equals(tInsurdNo) || (tAppntNo.equals(tInsurdNo)&&!customerFlag)) 
                        {
                			SmsMessageNew tInsurdMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
                            
                            //短信内容
                            String tInsurdContents = "尊敬的"+tInsuredName+tInsuredSex 
                                                  + "，您好！您的保单号为"+tContNo
                                                  +"的保单已于"+tMakeDate
                                                  +"由您的委托代办人进行领取，领取金额为"+tSumGetMoney
                                                  +"元。为维护您的权益，请您核实此情况。感谢您的支持，祝您健康。客服电话：95591。"
                                                  ;
                            
                            System.out.println("现金发送短信：" + tInsuredName + tContNo +"日期:"+ tMakeDate+"金额:"+ tSumGetMoney +"被保人手机号："+ tInsuredMobile);
                            tInsurdMsg.setReceiver(tInsuredMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                            tInsurdMsg.setReceiver("18500905130");
                            tInsurdMsg.setContents(tInsurdContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                            //和IT讨论后，归类到二级机构
                            tInsurdMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                            tVector.add(tInsurdMsg);
                            
//                          在LCCSpec表中插入值  ******************************************************** 
                            LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                            tLCCSpecSchema.setGrpContNo("00000000000000000000");
                            tLCCSpecSchema.setContNo(tContNo);
                            tLCCSpecSchema.setProposalContNo("00000000000000000000");
                            tLCCSpecSchema.setPrtSeq(tActuGetNo);//催收的 放 ljspay--actugetno                              
                            String tLimit = PubFun.getNoLimit(tManageCom);
                	        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
                	        tLCCSpecSchema.setSerialNo(serNo); //流水号
                            tLCCSpecSchema.setSpecType("mj01"); //保全客户在办理领取时所申领的金额＞1000元，且为委托代办时，发送短信至投保人及被保险人短信类型
                            tLCCSpecSchema.setSpecCode("002");//001-投保人 002-被保人 003-业务员
                            tLCCSpecSchema.setSpecContent(tInsurdContents+"被保人手机号："+ tInsuredMobile);//短信内容
                            tLCCSpecSchema.setOperator(mG.Operator);
                            tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                            tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                            tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                            tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                            tLCCSpecSchema.getDB().insert();
                        }
                	}
                } 
                System.out.println("2------------"+tVector.size());
        return tVector;
    }

    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        
        EdorGetMsgBL tEdorGetMsgBL = new EdorGetMsgBL();
        tEdorGetMsgBL.submitData(mVData, "XQMSG");
    }


}
