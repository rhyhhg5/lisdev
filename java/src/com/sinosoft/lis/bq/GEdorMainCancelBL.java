package com.sinosoft.lis.bq;

import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.Task;


/**
 * <p>Title:团单整单删除UI层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong
 * @version 1.0
 */
public class GEdorMainCancelBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 撤销申请原因*/
    private String delReason;
    private String reasonCode;

    /** 数据操作字符串 */
    private String mOperate;
    private String mOperator;
    private String mManageCom;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();
    //private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema(); //新的表结构
    private TransferData tTransferData = new TransferData();
    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    public GEdorMainCancelBL()
    {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        //System.out.println("PEdorItemCancelBL报告："+delReason);
        System.out.println("reasonCode in GEdorMainCancel is:"+reasonCode);

        //将VData数据还原成业务需要的类
        if (this.getInputData() == false)
        {
            return false;
        }

        System.out.println("---getInputData successful---");

        if (this.dealData() == false)
        {
            return false;
        }

        System.out.println("---dealdata successful---");

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

//        PubSubmit tPubSubmit = new PubSubmit();
//
//        if (!tPubSubmit.submitData(mResult, cOperate))
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//
//            return false;
//        }

        return true;
    }

    /**
     * 将UI层传输来得数据根据业务还原成具体的类
     * @return boolean
     */
    private boolean getInputData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        //全局变量实例
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            mErrors.addOneError(new CError("没有得到全局量信息"));

            return false;
        }

        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        //团体保单实例
        mLPGrpEdorMainSchema.setSchema((LPGrpEdorMainSchema) mInputData.
                                       getObjectByObjectName(
                                               "LPGrpEdorMainSchema", 0));

        if (mLPGrpEdorMainSchema == null)
        {
            this.mErrors.addOneError(new CError("传入的团单信息为空！"));

            return false;
        }
        tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
        delReason = (String)tTransferData.getValueByName("DelReason");
        reasonCode = (String)tTransferData.getValueByName("ReasonCode");

        String strSql = "select Max(a.edorno) from lpgrpedormain a where a.EdorState<>'0' and a.GrpContNo='" +
                        mLPGrpEdorMainSchema.getGrpContNo() 
                        + "' and edorno > '" + mLPGrpEdorMainSchema.getEdorNo() + "'";

        String MaxEdorNo = tExeSQL.getOneValue(strSql);

        System.out.println("MaxEdorNo="+MaxEdorNo);

        if ((MaxEdorNo == null) || (MaxEdorNo.trim().equals("")) || MaxEdorNo.equals("null"))
        {
            //this.mErrors.addOneError(new CError("该批单不是最近一条，无法删除!最大的批单号为："+MaxEdorNo));
            //return false;
        	MaxEdorNo=mLPGrpEdorMainSchema.getEdorNo();
        }

        if (!MaxEdorNo.equals(mLPGrpEdorMainSchema.getEdorNo()))
        {
            this.mErrors.addOneError(new CError("该批单不是最近一条，无法删除!!最大的批单号为："+MaxEdorNo));
            return false;
        }

        return true;
    }

    /**
     * 对业务数据进行加工
     * 对于新增的操作，这里需要有生成新合同号和新客户号的操作。
     * @return boolean
     */
    private boolean dealData()
    {
        String delSql;
        String tEdorNo = mLPGrpEdorMainSchema.getEdorNo();


        //将将数据备份，并存储备份信息
        LPGrpEdorMainSchema cLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
        LOBGrpEdorMainSchema cLOBGrpEdorMainSchema = new LOBGrpEdorMainSchema();
        LPGrpEdorMainDB cLPGrpEdorMainDB = new LPGrpEdorMainDB();
        LPGrpEdorMainSet cLPGrpEdorMainSet = new LPGrpEdorMainSet();

    //选出当前记录
        System.out.println("%%%%%%%%"+delReason);
        String sql = "select * from LpGrpEdorMain where EdorNo='" + tEdorNo + "'";
        cLPGrpEdorMainSet = cLPGrpEdorMainDB.executeQuery(sql);
        if (cLPGrpEdorMainSet.size() > 0 && cLPGrpEdorMainSet != null) {
          for (int k = 1; k <= cLPGrpEdorMainSet.size(); k++) {
            cLPGrpEdorMainSchema = cLPGrpEdorMainSet.get(k);
            System.out.println("cLPGrpEdorMainSchema:" + cLPGrpEdorMainSchema.getFieldCount()); //测试用
            Reflections crf = new Reflections();
            crf.transFields(cLOBGrpEdorMainSchema, cLPGrpEdorMainSchema); //将一条记录整体复制
            cLOBGrpEdorMainSchema.setReason(delReason); //添加撤销原因
            cLOBGrpEdorMainSchema.setReasonCode(reasonCode); //添加撤销原因编码
            cLOBGrpEdorMainSchema.setOperator(mGlobalInput.Operator);
            cLOBGrpEdorMainSchema.setModifyDate(theCurrentDate);
            cLOBGrpEdorMainSchema.setModifyTime(theCurrentTime);
            cLOBGrpEdorMainSchema.setMakeDate(theCurrentDate); //设置修改时间为当前时间
            cLOBGrpEdorMainSchema.setMakeTime(theCurrentTime);

            // cLOBGrpEdorItemSet.add(cLOBGrpEdorItemSchema);
          }
          mMap.put(cLOBGrpEdorMainSchema, "DELETE&INSERT");
        }


        String[] tableForDel =
                {
                "LPGeneral", "LPGrpCont", "LPCont",
                "LPGeneralToRisk", "LPPol",
                "LPGrpAppnt", "LPGrpPol", "LPGetToAcc",
                "LPInsureAcc",
                "LPPremToAcc", "LPPrem", "LPGet",
                "LPDuty", "LPPrem_1", "LPInsureAccClass",
                "LPInsureAccTrace","LPInsureAccFeeTrace",
                "LPContPlanDutyParam", "LPContPlanRisk",
                "LPContPlan",
                "LPAppnt", "LPCustomerImpart",
                "LPInsuredRelated",
                "LPBnf", "LPInsured", "LPCustomerImpartParams",
                "LPInsureAccFee", "LPInsureAccClassFee",
                "LPContPlanFactory",
                "LPContPlanParam", "LPGrpFee", "LPGrpFeeParam",
                "LPMove",
                "LPEdorPrint", "LPAppntTrace",
                "LPLoan", "LPReturnLoan", "LPEdorPrint2",
                "LPEdorPrint3",
                "LPGUWError", "LPGUWMaster", "LPCUWError",
                "LPCUWMaster",
                "LPCUWSub",
                "LPUWError", "LPUWMaster", "LPUWSub",
                "LPPENoticeItem", "LPPENotice", "LPGCUWError",
                "LPGCUWMaster",
                "LPGCUWSub", "LPGUWSub", "LPSpec", "LPIssuePol",
                "LPGrpIssuePol", "LPCGrpSpec", "LPCSpec",
                "LPAccount",
                "LPPerson", "LPAddress", "LPGrpAddress", "LPGrp",
                "LPPayRuleFactory", "LPPayRuleParams",
                "LPRReportResult",
                "LPRReport", "LPRReportItem",
                "LPCustomerImpartDetail",
                "LPAccMove", "LPEdorItem",
                "LPAccMove", "LPEdorItem",
                "LPEdorMain", "LPGrpEdorItem", "LPGrpEdorMain",
                "LPBudgetResult", "LCInsuredList"};
        for (int i = 0; i < tableForDel.length; i++)
        {
            delSql = "delete from  " + tableForDel[i] +
                     " where EdorNo = '" + tEdorNo + "'";
			 System.out.println(delSql);
            mMap.put(delSql, "DELETE");
        }

        //对暂交费表操作
        String delFeeClass = "delete from LJTempFeeClass where"
            + " TempFeeNo in (select TempFeeNo from LJTempFee where otherno='" +
            tEdorNo
            + "' and otherNoType='3')";
        mMap.put(delFeeClass, "DELETE");

        String delFee = "delete from LJTempFee where otherno='" + tEdorNo
            + "' and otherNoType='3'";
        mMap.put(delFee, "DELETE");


        //删除批改补退费表中相关记录
        delSql = "delete from  LJSGetEndorse where EndorsementNo = '" + tEdorNo +
                 "'";
        mMap.put(delSql, "DELETE");

        //对于一些特殊的保全项目调用特殊的保全项目取消类
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(tEdorNo);
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        for (int i = 0; i < tLPGrpEdorItemSet.size(); i++)
        {
            tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(i + 1);
            try
            {
                Class tClass = Class.forName("com.sinosoft.lis.bq.GrpEdor" +tLPGrpEdorItemSchema.getEdorType()+"CancelBL");
                EdorCancel tGrpEdorCancel = (EdorCancel) tClass.newInstance();
                VData tVData = new VData();

                tVData.add(tLPGrpEdorItemSchema);
                tVData.add(mGlobalInput);

                if (!tGrpEdorCancel.submitData(tVData,"EDORCANCEL||"+tLPGrpEdorItemSchema.getEdorType()))
                {
                    return false;
                }
                else
                {
                    VData rVData = tGrpEdorCancel.getResult();
                    MMap tMap = new MMap();
                    tMap = (MMap) rVData.getObjectByObjectName("MMap", 0);
                    if (tMap == null)
                    {
                        mErrors.addOneError(new CError("得到保单号为：" +
                                tLPGrpEdorItemSchema.getGrpContNo() + "保全项目为:" +
                                tLPGrpEdorItemSchema.getEdorType() +
                                "的保全申请确认结果时失败！"));
                        return false;

                    }
                    else
                    {
                        mMap.add(tMap);
                    }
                }

            }
            catch (ClassNotFoundException ex)
            {
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                return false;
            }

        }
//        20140916删除并备份保全申请表
        LPEdorAppDB tLPEdorAppDB=new LPEdorAppDB();
        String appSQL = "select * from LPedorApp where EdorAcceptNo='" + tEdorNo + "'";
        LPEdorAppSet tLPEdorAppSet=tLPEdorAppDB.executeQuery(appSQL);
        if(tLPEdorAppSet.size()>0){
        	LPEdorAppSchema tLPEdorAppSchema=tLPEdorAppSet.get(1);
        	LOBEdorAppSchema tLOBEdorAppSchema=new LOBEdorAppSchema();
        	Reflections crf = new Reflections();
            crf.transFields(tLOBEdorAppSchema, tLPEdorAppSchema);
            tLOBEdorAppSchema.setReason(delReason); //添加撤销原因                       
            tLOBEdorAppSchema.setReasonCode(reasonCode); //添加撤销原因编码                
            tLOBEdorAppSchema.setOperator(mGlobalInput.Operator);                  
            tLOBEdorAppSchema.setModifyDate(theCurrentDate);                       
            tLOBEdorAppSchema.setModifyTime(theCurrentTime);                         
            tLOBEdorAppSchema.setMakeDate(theCurrentDate); //设置修改时间为当前时间           
            tLOBEdorAppSchema.setMakeTime(theCurrentTime);  
            mMap.put(tLOBEdorAppSchema, "DELETE&INSERT");
            mMap.put(tLPEdorAppSchema, "DELETE");
        }
        

        //更新工单状态
        mMap.put("update LGWork "
                 + "set StatusNo='8', "
                 + "   operator = '" + mGlobalInput.Operator + "', "
                 + "   modifyDate='" + PubFun.getCurrentDate() + "', "
                 + "   modifyTime='" + PubFun.getCurrentTime() + "' "
                 + "where AcceptNo='" + tEdorNo + "' ", "UPDATE");

        //createTraceNodeOp();
        return true;
    }

    /**
     * 生成作业历史
     */
    private void createTraceNodeOp()
    {
        String sql = "  select * "
                     + "from LGTraceNodeOp "
                     + "where workNo = '" + mLPGrpEdorMainSchema.getEdorNo()
                     + "' "
                     + "order by int(nodeNo) desc, int(operatorNo) desc ";
        LGTraceNodeOpDB tLGTraceNodeOpDB = new LGTraceNodeOpDB();
        LGTraceNodeOpSet set = tLGTraceNodeOpDB.executeQuery(sql);
        if(set.size() > 0)
        {
            int operatorNo = Integer.parseInt(set.get(1).getOperatorNo());
            set.get(1).setOperatorNo(String.valueOf(operatorNo + 1));
            set.get(1).setOperatorType(Task.HISTORY_TYPE_DELETEEDOR);
            set.get(1).setRemark("撤销保全申请");
            set.get(1).setOperator(mGlobalInput.Operator);
            set.get(1).setMakeDate(this.theCurrentDate);
            set.get(1).setMakeTime(this.theCurrentTime);
            set.get(1).setModifyDate(this.theCurrentDate);
            set.get(1).setModifyTime(this.theCurrentTime);
            mMap.put(set.get(1), "INSERT");
        }
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData()
    {
        //记录当前操作员
        mResult.clear();
        mResult.add(mMap);
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }
    public MMap getMap()
    {
      return mMap;
    }


}
