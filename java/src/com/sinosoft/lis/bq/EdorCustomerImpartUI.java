package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:健康告知录入</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class EdorCustomerImpartUI
{
    private EdorCustomerImpartBL mEdorCustomerImpartBL = null;


    public EdorCustomerImpartUI()
    {
        this.mEdorCustomerImpartBL = new EdorCustomerImpartBL();
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!mEdorCustomerImpartBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mEdorCustomerImpartBL.mErrors.getFirstError();
    }

    /**
      * 主函数，测试用
      * @param args String[]
      */
     public static void main(String[] args)
     {
         GlobalInput gi = new GlobalInput();
         gi.ManageCom = "86";
         gi.Operator = "endor";
     }
}
