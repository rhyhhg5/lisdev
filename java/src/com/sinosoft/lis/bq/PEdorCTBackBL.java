package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 个单退保回退功能：
 * 将保单数据从B表放到C表，本类不处理财务数据
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PEdorCTBackBL
{
    /**错误信息*/
    public CErrors mErrors = new CErrors();

    private ExeSQL mExeSQL = new ExeSQL();

    private MMap map = new MMap();

    public PEdorCTBackBL()
    {
    }

    /**
     * 回退且仅回退保单级别的数据
     * @param contNo String
     * @param edorNo String
     * @return MMap
     */
    private MMap backContLevelJust(String contNo, String edorNo)
    {
        MMap tMMap = new MMap();

        String[] tableB = {"LBCont", "LBAppnt"};

        for(int i = 0; i < tableB.length; i++)
        {
            String tableNameC = tableB[i].replaceFirst("LB", "LC");

            //查询B表数据
            String sql = "select * from " + tableB[i]
                         + " where contNo = '" + contNo + "' "
                         + "   and edorNo = '" + edorNo + "' ";
            System.out.println(sql);
            SSRS tSSRS = mExeSQL.execSQL(sql);

            MMap aMMap = setDataToCFromB(tSSRS, tableNameC);
            if(aMMap == null)
            {
                return null;
            }
            tMMap.add(aMMap);

            String sql2 = "delete from " + tableB[i]
                          + " where contNo = '" + contNo + "' "
                          + "   and edorNo = '" + edorNo + "' ";
            tMMap.put(sql2, SysConst.DELETE);
        }

        return tMMap;
    }

    /**
     * 回退且仅回退被保人级别的数据
     * @return MMap
     */
    private MMap backInsurdLevelJust(String contNo,
                                        String insuredNo,
                                        String edorNo)
    {
        MMap tMMap = new MMap();

        String[] tablesB =
            {"LBInsured", "LBCustomerImpart",
            "LBCustomerImpartParams"};


        String sql = "select * from " + tablesB[0]
                     + " where contNo = '" + contNo + "' "
                     + "   and insuredNo = '" + insuredNo + "' "
                     + "   and edorNo = '" + edorNo + "' ";
        System.out.println(sql);
        SSRS tSSRS = mExeSQL.execSQL(sql);

        String tableNameC = tablesB[0].replaceFirst("LB", "LC");
        MMap aMMap = setDataToCFromB(tSSRS, tableNameC);
        if(aMMap == null)
        {
            return null;
        }
        tMMap.add(aMMap);

        String sql2 = "delete from " + tablesB[0]
                      + " where contNo = '" + contNo + "' "
                      + "   and insuredNo = '" + insuredNo + "' "
                      + "   and edorNo = '" + edorNo + "' ";
        tMMap.put(sql2, SysConst.DELETE);


        for(int i = 1; i < tablesB.length; i++)
        {
            //查询B表数据
            sql = "select * from " + tablesB[i]
                         + " where contNo = '" + contNo + "' "
                         + "   and customerNo = '" + insuredNo + "' "
                         + "   and edorNo = '" + edorNo + "' ";
            System.out.println(sql);
            tSSRS = mExeSQL.execSQL(sql);

            tableNameC = tablesB[i].replaceFirst("LB", "LC");
            MMap mMap = setDataToCFromB(tSSRS, tableNameC);
            if(mMap == null)
            {
                return null;
            }
            tMMap.add(mMap);

            sql2 = "delete from " + tablesB[i]
                          + " where contNo = '" + contNo + "' "
                          + "   and customerNo = '" + insuredNo + "' "
                          + "   and edorNo = '" + edorNo + "' ";
            tMMap.put(sql2, SysConst.DELETE);
        }

        return tMMap;
    }

    /**
     * 险种回退，将险种的相关信息从B表放到C表，执行与CongCancel相反的动作
     * @param tLBPolSet LBPolSet：待回退的险种
     * @return boolean：成功true，否则false
     */
    private MMap backPol(String polNo, String edorNo)
    {
        if(polNo == null || polNo.equals("")
           || edorNo == null || edorNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorCTBackBL";
            tError.functionName = "backPol";
            tError.errorMessage = "请传入需回退的险种";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        MMap tMMap = new MMap();

        String[] lbTable =
            {
            "LBPol", "LBInsureAcc", "LBInsureAccTrace", "LBDuty", "LBPrem",
            "LBGet", "LBBnf", "LBInsuredRelated", "LBPremToAcc",
            "LBGetToAcc", "LBInsureAccFee", "LBInsureAccClassFee",
            "LBInsureAccClass"};

        for(int i = 0; i < lbTable.length; i++)
        {
            String tableNameC = lbTable[i].replaceFirst("LB", "LC");

            //查询B表数据
            String sql = "select * from " + lbTable[i]
                         + " where polNo = '" + polNo + "' "
                         + "   and edorNo = '" + edorNo + "' ";
            System.out.println(sql);
            SSRS tSSRS = mExeSQL.execSQL(sql);

            MMap aMMap = setDataToCFromB(tSSRS, tableNameC);
            if(aMMap == null)
            {
                return null;
            }
            tMMap.add(aMMap);

            String sql2 = "delete from " + lbTable[i]
                          + " where polNo = '" + polNo + "' "
                          + "   and edorNo = '" + edorNo + "' ";
            tMMap.put(sql2, SysConst.DELETE);
        }

        return tMMap;
    }

    /**
     * 将数据从tSSRS放到表tableNameC
     * @param tSSRS SSRS：将从B得到的数据放到表tableNameC
     * @param tableNameC String
     * @return MMap
     */
    private MMap setDataToCFromB(SSRS tSSRS, String tableNameC)
    {
        MMap tMMap = new MMap();
        try
        {
            //得到C表的Schema类
            String classNameC = "com.sinosoft.lis.schema."
                                + tableNameC + "Schema";
            Class confirmClass = Class.forName(classNameC);

            for(int row = 1; row <= tSSRS.getMaxRow(); row++)
            {
                //将查询到的数据放入C表Schema
                Schema tableSchemaC = (Schema) confirmClass.newInstance();

                for(int col = 0; col < tableSchemaC.getFieldCount(); col++)
                {
                    //B表结构为在C表第一字段前加字段EdorNo，且GetText是从(1, 1)开始,所以这个地方要col + 2
                    tableSchemaC.setV(tableSchemaC.getFieldName(col),
                                      tSSRS.GetText(row, col + 2));
                }
                tMMap.put(tableSchemaC, SysConst.INSERT);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorCTBackBL";
            tError.functionName = "backPol";
            tError.errorMessage = "没有查询到下表的Schema" + tableNameC;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return tMMap;
    }

    public static void main(String[] args)
    {
        String edorNo = "20061102000008";
        String contNo = "00091836501";

        MMap tMMap = new MMap();
        PEdorCTBackBL bl = new PEdorCTBackBL();

//        String sql = "select * from LBPol a "
//                     + "where edorNo = '" + edorNo + "' "
//                     + "   and contNo = '" + contNo + "' "
//                     + "   and polNo not in "
//                     + "      (select polNo from LPPol "
//                     + "      where edorNo = a.edorNo "
//                     + "         and contNo = a.contNo and edorType = 'CT') ";
//        System.out.println(sql);
//        LBPolSet set = new LBPolDB().executeQuery(sql);

//        LBPolDB tLBPolDB = new LBPolDB();
//        tLBPolDB.setEdorNo("20061103000002");
//        tLBPolDB.setContNo("00001541901");
//        LBPolSet set = tLBPolDB.query();

//        for(int i = 1; i <= set.size(); i++)
//        {
//            MMap mMap = bl.backPol(set.get(i).getPolNo(), set.get(i).getEdorNo());
//            if(mMap != null)
//            {
//                tMMap.add(mMap);
//            }
//            else
//            {
//                System.out.println(bl.mErrors.getErrContent());
//            }
//        }

//        LBInsuredDB tLBInsuredDB = new LBInsuredDB();
//        tLBInsuredDB.setEdorNo(edorNo);
//        tLBInsuredDB.setContNo(contNo);
//
//        LBInsuredSet set = tLBInsuredDB.query();
//        for(int i = 1; i <= set.size(); i++)
//        {
//            MMap mMap = bl.backInsurdLevelJust(set.get(i).getContNo(),
//                        set.get(i).getInsuredNo(), set.get(i).getEdorNo());
//            if(mMap != null)
//            {
//                tMMap.add(mMap);
//            }
//            else
//            {
//                System.out.println(bl.mErrors.getErrContent());
//            }
//        }

        MMap mMap = bl.backContLevelJust(contNo, edorNo);
        if(mMap != null)
        {
            tMMap.add(mMap);
        }
        else
        {
            System.out.println(bl.mErrors.getErrContent());
        }


        VData data = new VData();
        data.add(tMMap);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, ""))
        {
            System.out.println(p.mErrors.getErrContent());
        }
    }
}
