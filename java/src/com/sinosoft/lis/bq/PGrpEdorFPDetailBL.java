package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.sql.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PGrpEdorFPDetailBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    private LCGrpServInfoSet mLCGrpServInfoSet = new LCGrpServInfoSet();
    private LCGrpContSchema mLCGrpContSchema =new LCGrpContSchema();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    MMap map = new MMap();


    public PGrpEdorFPDetailBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //数据校验操作（checkdata)
        if (!checkData())
            return false;

        //数据准备操作（preparedata())
        if (!dealData())
            return false;

        //数据准备操作（preparedata())
        if (!prepareData())
            return false;

        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(mResult, cOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        // 数据操作业务处理(新增insertData();修改updateData();删除deletedata())
        if (mOperate.equals("INSERT||EDORFP")) {
            map.put("delete from LPGrpServInfo where edorno='"+mLPGrpEdorItemSchema.getEdorNo()+"' and edortype='"+mLPGrpEdorItemSchema.getEdorType()+"'"
                 , "DELETE");
            for (int i= 1; i <= mLCGrpServInfoSet.size(); i++) {
                LCGrpServInfoSet tLCGrpServInfoSet = new LCGrpServInfoSet();
                LCGrpServInfoDB tLCGrpServInfoDB = new LCGrpServInfoDB();
                tLCGrpServInfoDB.setGrpContNo(mLCGrpServInfoSet.get(i).getGrpContNo());
                tLCGrpServInfoDB.setServKind(mLCGrpServInfoSet.get(i).getServKind());
                tLCGrpServInfoDB.setServDetail(mLCGrpServInfoSet.get(i).
                                               getServDetail());
                tLCGrpServInfoSet = tLCGrpServInfoDB.query();
                if (tLCGrpServInfoDB.mErrors.needDealError()) {
                    // @@错误处理
                    mErrors.copyAllErrors(tLCGrpServInfoDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "GrpEdorFPDetailBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询团单服务信息表出错!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tLCGrpServInfoSet.size() == 0) {
                    LPGrpServInfoSchema tLPGrpServInfoSchema = new
                            LPGrpServInfoSchema();
                    tLPGrpServInfoSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
                    tLPGrpServInfoSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
                    tLPGrpServInfoSchema.setGrpContNo(mLCGrpServInfoSet.get(i).getGrpContNo());

                    tLPGrpServInfoSchema.setProposalGrpContNo(mLCGrpContSchema.getProposalGrpContNo());
                    tLPGrpServInfoSchema.setPrtNo(mLCGrpContSchema.getPrtNo());

                    tLPGrpServInfoSchema.setServKind(tLCGrpServInfoDB.getServKind());
                    tLPGrpServInfoSchema.setServDetail(tLCGrpServInfoDB.
                            getServDetail());
                    tLPGrpServInfoSchema.setServChoose(mLCGrpServInfoSet.get(i).
                            getServChoose());
                    tLPGrpServInfoSchema.setMakeDate(PubFun.getCurrentDate());
                    tLPGrpServInfoSchema.setMakeTime(PubFun.getCurrentTime());
                    tLPGrpServInfoSchema.setModifyDate(PubFun.getCurrentDate());
                    tLPGrpServInfoSchema.setModifyTime(PubFun.getCurrentTime());
                    map.put(tLPGrpServInfoSchema, "INSERT");
                } else {
                    LPGrpServInfoSchema tLPGrpServInfoSchema=new LPGrpServInfoSchema();
                    Reflections ref = new Reflections();
                    ref.transFields(tLPGrpServInfoSchema, tLCGrpServInfoSet.get(1));
                    tLPGrpServInfoSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
                    tLPGrpServInfoSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
                    tLPGrpServInfoSchema.setServChoose(mLCGrpServInfoSet.get(i).
                            getServChoose());
                    tLPGrpServInfoSchema.setModifyDate(PubFun.getCurrentDate());
                    tLPGrpServInfoSchema.setModifyTime(PubFun.getCurrentTime());
                    map.put(tLPGrpServInfoSchema, "INSERT");
                }
            }
        }
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void setEdorState(String edorState) {
        String sql = "update  LPGrpEdorItem " +
                     "set EdorState = '" + edorState + "', " +
                     "    Operator = '" + mGlobalInput.Operator + "', " +
                     "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                     "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                     "where  EdorNo = '" + mLPGrpEdorItemSchema.getEdorNo() +
                     "' " +
                     "and EdorType = '" + mLPGrpEdorItemSchema.getEdorType() +
                     "' ";
        map.put(sql, "UPDATE");
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData)
    {

        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mLCGrpServInfoSet = (LCGrpServInfoSet) cInputData.getObjectByObjectName(
                "LCGrpServInfoSet", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {

        boolean flag = true;
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setSchema(mLPGrpEdorItemSchema);
        if (!tLPGrpEdorItemDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PGrpEdorFPDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "无保全项目数据!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }
        mLPGrpEdorItemSchema = tLPGrpEdorItemDB.getSchema();
        if (tLPGrpEdorItemDB.getEdorState().trim().equals("2"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PGrpEdorFPDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保全项目已经申请确认不能修改!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }

        LCGrpContDB tLCGrpContDB=new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        if(!tLCGrpContDB.getInfo())
        {
            if (tLCGrpContDB.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PGrpEdorFPDetailBL";
                tError.functionName = "checkData";
                tError.errorMessage = "查询团体合同信息出错!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }else
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "PGrpEdorFPDetailBL";
                tError.functionName = "checkData";
                tError.errorMessage = "无团体合同信息!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
        }else
        {
            this.mLCGrpContSchema.setSchema(tLCGrpContDB);
        }

        return flag;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        mResult.clear();
        mResult.add(map);
        return true;
    }
}
