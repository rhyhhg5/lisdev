package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

//程序名称：PEdorCBAppConfirmBL.java
//程序功能：个人保全常无忧B给付补费理算
//创建日期：2010-07-21 16:49:22
//创建人  ：About Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class PEdorCBAppConfirmBL implements EdorAppConfirm
{
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /** 业务对象 */
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();

    public PEdorCBAppConfirmBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }
        //数据准备操作
        if (!prepareData())
        {
            return false;
        }
        prepareOutputData();

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData()
    {
        mResult.add(map);
    }


    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData()
    {
        try
        {
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName("LPEdorItemSchema", 0);
            mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        }
        catch (Exception e)
        {
        	CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
    	EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(mLPEdorItemSchema);
    	
    	if(!tEdorItemSpecialData.query())
        {
        	mErrors.addOneError("SpecialData未找到本次金额！");
            return false;
        }
    	
    	String [] polnos = tEdorItemSpecialData.getPolNos();
        String tPolNo = polnos[0];
        
        tEdorItemSpecialData.setPolNo(tPolNo);
       
    	double tGetMoney = -Double.parseDouble(tEdorItemSpecialData.getEdorValue("GETMONEY"));

        //付款要用负数，打印团体变动清单时统计要用，by Minim at 2003-12-11
        mLPEdorItemSchema.setGetMoney(tGetMoney);
        mLPEdorItemSchema.setEdorState(BQ.EDORSTATE_CAL);
        mLPEdorItemSchema.setUWFlag("0");

        map.put(mLPEdorItemSchema, "UPDATE");
        
       
        
        LCPolSchema tLCPolSchema = CommonBL.getLCPol(tPolNo);
        if (tLCPolSchema == null)
        {
            mErrors.addOneError("找不到客户的险种信息！");
            return false;
        }
        
        setGetEndorse(tLCPolSchema,tGetMoney);

        return true;
    }
    
    /**
     * 设置批改补退费表
     * @return boolean
     */
    private void setGetEndorse(LCPolSchema aLCPolSchema, double edorPrem)
    {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mLPEdorItemSchema.getEdorAcceptNo()); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLJSGetEndorseSchema.setFeeOperationType(mLPEdorItemSchema.getEdorType());
        tLJSGetEndorseSchema.setGrpContNo(aLCPolSchema.getGrpContNo());
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mLPEdorItemSchema.getEdorValiDate());
        tLJSGetEndorseSchema.setGetMoney(mLPEdorItemSchema.getGetMoney());
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_TF);
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLJSGetEndorseSchema.setOtherNoType("10");
        tLJSGetEndorseSchema.setGetFlag("1");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLJSGetEndorseSchema, "DELETE&INSERT");
    }


    public static void main(String[] args)
    {
        LPEdorItemDB mLPEdorItemDB = new LPEdorItemDB();
        mLPEdorItemDB.setEdorNo("20070309000001");
        mLPEdorItemDB.setEdorType("CB");
        mLPEdorItemDB.setContNo("2300013005");

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.Operator = "edor";
        mGlobalInput.ManageCom = "86";

        VData tVata = new VData();
        tVata.add(mLPEdorItemDB.query().get(1));
        tVata.add(mGlobalInput);

        PEdorCBAppConfirmBL tPEdorCBAppConfirmBL = new PEdorCBAppConfirmBL();
        if(!tPEdorCBAppConfirmBL.submitData(tVata, ""))
        {
            System.out.println(tPEdorCBAppConfirmBL.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }

}
