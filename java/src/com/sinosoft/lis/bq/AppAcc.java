package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.LCAppAccSchema;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.db.LCAppAccDB;
import com.sinosoft.lis.db.LCAppAccTraceDB;
import com.sinosoft.lis.vschema.LCAppAccSet;
import com.sinosoft.lis.vschema.LCAppAccTraceSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.schema.LCAppAccGetTraceSchema;
import com.sinosoft.lis.db.LCAppAccGetTraceDB;
import com.sinosoft.lis.vschema.LCAppAccGetTraceSet;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: 投保人帐户处理</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006 </p>
 * <p>Company: Sinosoft </p>
 * @author MoJiao
 * @version 1.0
 */
public class AppAcc
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public MMap map = new MMap();

    public AppAcc()
    {
    }

    /** 检查转入或转出时传入的参数是否完整，代码类型的变量是否正确*/
    private boolean checkInputGet(LCAppAccGetTraceSchema pLCAppAccGetTraceSchema, LCAppAccSchema pLCAppAccSchema)
    {
        if (pLCAppAccGetTraceSchema.getCustomerNo() == null || pLCAppAccGetTraceSchema.getCustomerNo().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppAccGetBL";
            tError.functionName = "checkInputGet";
            tError.errorMessage = "客户号码为空!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }
        if (pLCAppAccGetTraceSchema.getName() == null || pLCAppAccGetTraceSchema.getName().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppAccGetBL";
            tError.functionName = "checkInputGet";
            tError.errorMessage = "领款人姓名为空!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }
        if (pLCAppAccGetTraceSchema.getIDNo() == null || pLCAppAccGetTraceSchema.getIDNo().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppAccGetBL";
            tError.functionName = "checkInputGet";
            tError.errorMessage = "领款人证件为空!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }
        if (pLCAppAccGetTraceSchema.getAccGetMode() == null || pLCAppAccGetTraceSchema.getAccGetMode().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppAccGetBL";
            tError.functionName = "checkInputGet";
            tError.errorMessage = "领取方式为空!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }
        if (pLCAppAccGetTraceSchema.getAccGetMode().equals("4"))
        {
            if (pLCAppAccGetTraceSchema.getBankCode() == null || pLCAppAccGetTraceSchema.getBankCode().equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LCAppAccGetBL";
                tError.functionName = "checkInputGet";
                tError.errorMessage = "银行编码为空!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
            if (pLCAppAccGetTraceSchema.getBankAccNo() == null || pLCAppAccGetTraceSchema.getBankAccNo().equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LCAppAccGetBL";
                tError.functionName = "checkInputGet";
                tError.errorMessage = "银行帐号为空!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
            if (pLCAppAccGetTraceSchema.getAccName() == null || pLCAppAccGetTraceSchema.getAccName().equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LCAppAccGetBL";
                tError.functionName = "checkInputGet";
                tError.errorMessage = "银行户名为空!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
            if (pLCAppAccGetTraceSchema.getTransferDate() == null
                    || pLCAppAccGetTraceSchema.getTransferDate().equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LCAppAccGetBL";
                tError.functionName = "checkInputGet";
                tError.errorMessage = "转帐日期为空!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        if (pLCAppAccGetTraceSchema.getAccGetMoney() <= 0
                || Math.abs(pLCAppAccGetTraceSchema.getAccGetMoney()) > pLCAppAccSchema.getAccGetMoney())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppAccGetBL";
            tError.functionName = "checkInputGet";
            tError.errorMessage = "领取金额输入不正确!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /** 检查转入或转出时传入的参数是否完整，代码类型的变量是否正确*/
    private boolean checkInput(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        if (pLCAppAccTraceSchema.getCustomerNo() == null || pLCAppAccTraceSchema.getCustomerNo().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "checkInput";
            tError.errorMessage = "客户号为空!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (pLCAppAccTraceSchema.getAccType() == null || pLCAppAccTraceSchema.getAccType().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "checkInput";
            tError.errorMessage = "帐户类型为空!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (pLCAppAccTraceSchema.getOtherNo() == null || pLCAppAccTraceSchema.getOtherNo().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "checkInput";
            tError.errorMessage = "对应其他号码为空!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (pLCAppAccTraceSchema.getOtherType() == null || pLCAppAccTraceSchema.getOtherType().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "checkInput";
            tError.errorMessage = "对应其他号码类型为空!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (pLCAppAccTraceSchema.getDestSource() == null || pLCAppAccTraceSchema.getDestSource().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "checkInput";
            tError.errorMessage = "用途或来源为空!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LDCodeDB tLDCodeDB = new LDCodeDB();
        LDCodeSet tLDCodeSet = new LDCodeSet();
        tLDCodeDB.setCodeType("appaccdestsource");
        tLDCodeDB.setCode(pLCAppAccTraceSchema.getDestSource());
        tLDCodeSet = tLDCodeDB.query();
        if (tLDCodeDB.mErrors.needDealError())
        {
            // @@错误处理
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "checkInput";
            tError.errorMessage = "查询代码表出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLDCodeSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "checkInput";
            tError.errorMessage = "用途或来源的代码设置不正确!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (pLCAppAccTraceSchema.getMoney() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "checkInput";
            tError.errorMessage = "金额增减为零!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (pLCAppAccTraceSchema.getOperator() == null || pLCAppAccTraceSchema.getOperator().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "checkInput";
            tError.errorMessage = "操作员为空!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /** 建立帐户
     * @param customerNo String
     * @param operator String
     * @param customerType String 0--表示个险客户，1--表示团险客户
     * @return LCAppAccSchema
     */
    private LCAppAccSchema accCreate(String customerNo, String operator, String customerType)
    {
        if (customerNo == null || customerNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "accCreate";
            tError.errorMessage = "操作员为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (operator == null || operator.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "accCreate";
            tError.errorMessage = "操作员为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (customerType == null || customerType.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "accCreate";
            tError.errorMessage = "客户类型为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (!customerType.equals("0") && !customerType.equals("1"))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "accCreate";
            tError.errorMessage = "客户类型不正确!";
            this.mErrors.addOneError(tError);
            return null;
        }
        LCAppAccSchema tLCAppAccSchema = new LCAppAccSchema();
        tLCAppAccSchema.setCustomerNo(customerNo);
        tLCAppAccSchema.setCustomerNoType(customerType);
        tLCAppAccSchema.setAccFoundDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setAccFoundTime(PubFun.getCurrentTime());
        tLCAppAccSchema.setAccBala(0);
        tLCAppAccSchema.setAccGetMoney(0);
        tLCAppAccSchema.setState("1");
        tLCAppAccSchema.setOperator(operator);
        tLCAppAccSchema.setMakeDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setMakeTime(PubFun.getCurrentTime());
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());
        return tLCAppAccSchema;
    }

    /** 帐户失效
     * @param customerNo String
     * @param operator String
     * @return MMap
     */
    public MMap accCancel(String customerNo, String operator)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(customerNo);
        if (tLCAppAccSchema == null)
        {
            return null;
        }
        if (operator == null || operator.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAcc";
            tError.errorMessage = "操作员为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        tLCAppAccSchema.setState("0");
        tLCAppAccSchema.setOperator(operator);
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCAppAccSchema, "UPDATE");
        return map;
    }

    /** 投保人帐户保单服务退费转入
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@return MMap
     */
    public MMap accShiftToFW(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(pLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccSchema == null)
        {
            return null;
        }
        pLCAppAccTraceSchema.setAccType("1");
        pLCAppAccTraceSchema.setDestSource("11");

        LCAppAccTraceSchema tLCAppAccTraceSchema = accShiftTo(pLCAppAccTraceSchema);
        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }
        tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额

        map.put(tLCAppAccTraceSchema, "INSERT");
        return map;
    }

    /** 投保人帐户新契约溢交转入
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@param customerType String 0--表示个险客户，1--表示团险客户
     *@return MMap
     */
    public MMap accShiftToXQY(LCAppAccTraceSchema pLCAppAccTraceSchema, String customerType)
    {
        boolean isNew = false;
        //查询投保人帐户表，如果不存在就插入新记录
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(pLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccSchema == null)
        {
            if (this.mErrors.getLastError().equals("查询投保人帐户表对应记录不存在!"))
            { //不存在
                tLCAppAccSchema = accCreate(pLCAppAccTraceSchema.getCustomerNo(), pLCAppAccTraceSchema.getOperator(),
                        customerType);
                if (tLCAppAccSchema == null)
                { //建立帐户出错
                    return null;
                }
                isNew = true;
            }
            else
            { //查询出错
                return null;
            }
        }
        if (pLCAppAccTraceSchema.getMoney() != 0)
        {
            pLCAppAccTraceSchema.setAccType("1");
            pLCAppAccTraceSchema.setDestSource("12");
            LCAppAccTraceSchema tLCAppAccTraceSchema = accShiftTo(pLCAppAccTraceSchema);
            if (tLCAppAccTraceSchema == null)
            {
                return null;
            }
            //修改投保人帐户表中的可领取金额
            tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() + tLCAppAccTraceSchema.getMoney());
            //确认同步完成
            tLCAppAccTraceSchema.setState("1");
            tLCAppAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
            tLCAppAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());
            tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
            tLCAppAccSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney());

            map.put(tLCAppAccTraceSchema, "INSERT");

            if (isNew == true)
            {
                map.put(tLCAppAccSchema, "INSERT");
            }
            else
            {
                tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
                tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());
                map.put(tLCAppAccSchema, "UPDATE");
            }
        }
        else
        {
            if (isNew == true)
            {
                map.put(tLCAppAccSchema, "INSERT");
            }
        }
        return map;
    }

    /** 投保人帐户续收溢交转入
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@return MMap
     */
    public MMap accShiftToXSYIJ(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(pLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccSchema == null)
        {
            return null;
        }
        pLCAppAccTraceSchema.setAccType("1");
        pLCAppAccTraceSchema.setDestSource("13");

        LCAppAccTraceSchema tLCAppAccTraceSchema = accShiftTo(pLCAppAccTraceSchema);
        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }

        //修改投保人帐户表中的可领取金额
        tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() + tLCAppAccTraceSchema.getMoney());

        //确认同步完成
        tLCAppAccTraceSchema.setState("1");
        tLCAppAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());
        tLCAppAccTraceSchema.setBakNo(pLCAppAccTraceSchema.getBakNo());
        tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
        tLCAppAccSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney());
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(tLCAppAccTraceSchema, "INSERT");
        map.put(tLCAppAccSchema, "UPDATE");
        return map;
    }

    //该方法能确保2次以上作废可以转账成功 modify by whl
    /** 投保人帐户续收溢交转入
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@return MMap
     */
    public MMap accShiftToXSYIJ(LCAppAccTraceSchema pLCAppAccTraceSchema, boolean isTempfee)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(pLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccSchema == null)
        {
            return null;
        }
        pLCAppAccTraceSchema.setAccType("1");
        pLCAppAccTraceSchema.setDestSource("13");

        LCAppAccTraceSchema tLCAppAccTraceSchema = accShiftTo(pLCAppAccTraceSchema);
        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }

        //修改投保人帐户表中的可领取金额
        tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() + tLCAppAccTraceSchema.getMoney());

        //确认同步完成
        tLCAppAccTraceSchema.setState("1");
        tLCAppAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());
        tLCAppAccTraceSchema.setBakNo(pLCAppAccTraceSchema.getBakNo());
        if (isTempfee)
        {
            tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
            tLCAppAccSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney());
        }
        else
        {
            tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala()); //设置当前的帐户余额
            tLCAppAccSchema.setAccBala(tLCAppAccSchema.getAccBala());
        }
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(tLCAppAccTraceSchema, "INSERT");
        map.put(tLCAppAccSchema, "UPDATE");
        return map;
    }

    /** 投保人帐户实收保费转出
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@return MMap
     */
    public MMap accShiftToSSZC(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(pLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccSchema == null)
        {
            return null;
        }
        pLCAppAccTraceSchema.setAccType("1");
        pLCAppAccTraceSchema.setDestSource("15");

        LCAppAccTraceSchema tLCAppAccTraceSchema = accShiftTo(pLCAppAccTraceSchema);
        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }

        //修改投保人帐户表中的可领取金额
        tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() + tLCAppAccTraceSchema.getMoney());

        //确认同步完成
        tLCAppAccTraceSchema.setState("1");
        tLCAppAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());
        tLCAppAccTraceSchema.setBakNo(pLCAppAccTraceSchema.getBakNo());
        tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
        tLCAppAccSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney());
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(tLCAppAccTraceSchema, "INSERT");
        map.put(tLCAppAccSchema, "UPDATE");
        return map;
    }

    /** 投保人帐户续收预交转入
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@return MMap
     */
    public MMap accShiftToXSYUJ(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(pLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccSchema == null)
        {
            return null;
        }
        pLCAppAccTraceSchema.setAccType("1");
        pLCAppAccTraceSchema.setDestSource("14");
        LCAppAccTraceSchema tLCAppAccTraceSchema = accShiftTo(pLCAppAccTraceSchema);
        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }
        //修改投保人帐户表中的可领取金额
        //        tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() +
        //                tLCAppAccTraceSchema.getMoney());

        //交费后结案
        tLCAppAccTraceSchema.setState("0");
        tLCAppAccTraceSchema.setBakNo(pLCAppAccTraceSchema.getBakNo());
        tLCAppAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());
        tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(tLCAppAccTraceSchema, "DELETE&INSERT");
        map.put(tLCAppAccSchema, "UPDATE");
        return map;

    }

    /** 投保人帐户转入
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * AccType 帐户类型
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * DestSource 用途或来源
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@return LCAppAccTraceSchema
     */
    private LCAppAccTraceSchema accShiftTo(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();

        if (!checkInput(pLCAppAccTraceSchema))
        {
            return null;
        }

        if (!pLCAppAccTraceSchema.getAccType().equals("1"))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "AccShiftTo";
            tError.errorMessage = "帐户类型不是转入!";
            this.mErrors.addOneError(tError);
            return null;
        }

        if (!pLCAppAccTraceSchema.getDestSource().substring(0, 1).equals("1"))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "AccShiftTo";
            tError.errorMessage = "用途或来源定义不是转入!";
            this.mErrors.addOneError(tError);
            return null;
        }

        if (pLCAppAccTraceSchema.getMoney() <= 0)
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "AccShiftTo";
            tError.errorMessage = "本次金额应该大于0!";
            this.mErrors.addOneError(tError);
            return null;
        }

        tLCAppAccTraceSchema.setCustomerNo(pLCAppAccTraceSchema.getCustomerNo());
        tLCAppAccTraceSchema.setAccType(pLCAppAccTraceSchema.getAccType());
        String serialNo = getSerialNo(pLCAppAccTraceSchema.getCustomerNo()); //获得客户下的流水号
        if (serialNo == null || serialNo.equals(""))
        {
            return null;
        }
        tLCAppAccTraceSchema.setSerialNo(serialNo);
        tLCAppAccTraceSchema.setOtherNo(pLCAppAccTraceSchema.getOtherNo());
        tLCAppAccTraceSchema.setOtherType(pLCAppAccTraceSchema.getOtherType());
        tLCAppAccTraceSchema.setDestSource(pLCAppAccTraceSchema.getDestSource());
        tLCAppAccTraceSchema.setMoney(pLCAppAccTraceSchema.getMoney());
        //tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala());//设置当前的帐户余额
        tLCAppAccTraceSchema.setState("0");
        tLCAppAccTraceSchema.setBakNo(pLCAppAccTraceSchema.getBakNo());
        tLCAppAccTraceSchema.setOperator(pLCAppAccTraceSchema.getOperator());
        tLCAppAccTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLCAppAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setModifyTime(PubFun.getCurrentTime());

        return tLCAppAccTraceSchema;
    }

    /** 投保人帐户保单服务费用抵扣
     * 说明：会修改投保人帐户的可领金额
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@return MMap
     */
    public MMap accTakeOutFW(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(pLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccSchema == null)
        {
            return null;
        }
        pLCAppAccTraceSchema.setAccType("0");
        pLCAppAccTraceSchema.setDestSource("01");
        LCAppAccTraceSchema tLCAppAccTraceSchema = accTakeOut(pLCAppAccTraceSchema);
        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }
        tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
        //修改投保人帐户表中的可领取金额
        tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() + tLCAppAccTraceSchema.getMoney());
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCAppAccTraceSchema, "INSERT");
        map.put(tLCAppAccSchema, "UPDATE");
        return map;

    }

    /** 投保人帐户续期抵扣
     * 说明：会修改投保人帐户的可领金额
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@return MMap
     */
    public MMap accTakeOutXQ(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(pLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccSchema == null)
        {
            return null;
        }
        pLCAppAccTraceSchema.setAccType("0");
        pLCAppAccTraceSchema.setDestSource("02");
        LCAppAccTraceSchema tLCAppAccTraceSchema = accTakeOut(pLCAppAccTraceSchema);

        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }

        //修改投保人帐户表中的可领取金额
        tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() + tLCAppAccTraceSchema.getMoney());

        //确认同步完成
        tLCAppAccTraceSchema.setState("1");
        tLCAppAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());
        tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
        tLCAppAccSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney());
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCAppAccTraceSchema, "INSERT");
        map.put(tLCAppAccSchema, "UPDATE");
        return map;
    }
    /** 投保人帐户续期抵扣
     * 说明：会修改投保人帐户的可领金额
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * Money 金额增减
     * Operator 操作员 
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@return MMap   支持账户钱多余续期费用退到账户轨迹
     */
    public MMap accTakeOutXQYET(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(pLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccSchema == null)
        {
            return null;
        }
        pLCAppAccTraceSchema.setAccType("0");
        pLCAppAccTraceSchema.setDestSource("02");
        LCAppAccTraceSchema tLCAppAccTraceSchema = accTakeOutYET(pLCAppAccTraceSchema);

        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }

        //修改投保人帐户表中的可领取金额
//        tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() + tLCAppAccTraceSchema.getMoney());

        //确认同步完成
        tLCAppAccTraceSchema.setState("1");
        tLCAppAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());
//        tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
        tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala()); //设置当前的帐户余额
        tLCAppAccSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney());
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCAppAccTraceSchema, "INSERT");
        map.put(tLCAppAccSchema, "UPDATE");
        return map;
    }
    /** 投保人帐户新契约抵扣
     * 说明：会修改投保人帐户的可领金额
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@return MMap
     */
    public MMap accTakeOutXQY(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(pLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccSchema == null)
        {
            return null;
        }
        pLCAppAccTraceSchema.setAccType("0");
        pLCAppAccTraceSchema.setDestSource("03");
        LCAppAccTraceSchema tLCAppAccTraceSchema = accTakeOut(pLCAppAccTraceSchema);

        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }
        //修改投保人帐户表中的可领取金额
        tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() + tLCAppAccTraceSchema.getMoney());

        //确认同步完成
        tLCAppAccTraceSchema.setState("1");
        tLCAppAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());
        tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
        tLCAppAccSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney());
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());

        // 校验帐户余额不能为负
        double tAccGetMoney = tLCAppAccSchema.getAccGetMoney();
        if (tAccGetMoney < 0)
        {
            CError tError = new CError();
            tError.moduleName = "LCAppAccGetBL";
            tError.functionName = "accTakeOutXQY";
            tError.errorMessage = "帐户余额不足。";
            this.mErrors.addOneError(tError);            
            return null;
        }
        // --------------------

        map.put(tLCAppAccTraceSchema, "INSERT");
        map.put(tLCAppAccSchema, "UPDATE");
        return map;
    }

    /** 投保人帐户领取
     * 说明：会修改投保人帐户的可领金额
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@return MMap
     */
    public MMap accTakeOutLQ(LCAppAccTraceSchema pLCAppAccTraceSchema, LCAppAccGetTraceSchema pLCAppAccGetTraceSchema)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(pLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccSchema == null)
        {
            return null;
        }
        pLCAppAccTraceSchema.setAccType("0");
        pLCAppAccTraceSchema.setDestSource("04");
        LCAppAccTraceSchema tLCAppAccTraceSchema = accTakeOut(pLCAppAccTraceSchema);

        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }

        if (!checkInputGet(pLCAppAccGetTraceSchema, tLCAppAccSchema))
        {
            return null;
        }

        //修改投保人帐户表中的可领取金额,帐户余额
        tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() + tLCAppAccTraceSchema.getMoney());

        //确认同步完成
        tLCAppAccTraceSchema.setState("1");
        tLCAppAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());
        tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
        tLCAppAccSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney());
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());
        LCAppAccGetTraceSchema tLCAppAccGetTraceSchema = addAppAccGetTrace(tLCAppAccTraceSchema,
                pLCAppAccGetTraceSchema);
        if (tLCAppAccGetTraceSchema == null)
        {
            return null;
        }
        map.put(tLCAppAccTraceSchema, "INSERT");
        map.put(tLCAppAccSchema, "UPDATE");
        map.put(tLCAppAccGetTraceSchema, "INSERT");
        return map;
    }
    /** 投保人帐户抵扣
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * AccType 帐户类型
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * DestSource 用途或来源
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@param pLCAppAccSchema LCAppAccSchema
     *@return MMap
     */
    private LCAppAccTraceSchema accTakeOut(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();

        if (!checkInput(pLCAppAccTraceSchema))
        {
            return null;
        }

        if (!pLCAppAccTraceSchema.getAccType().equals("0"))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "AccTakeOut";
            tError.errorMessage = "帐户类型不是抵扣或领取!";
            this.mErrors.addOneError(tError);
            return null;
        }

        if (!pLCAppAccTraceSchema.getDestSource().substring(0, 1).equals("0"))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "AccShiftTo";
            tError.errorMessage = "用途或来源定义不是抵扣或领取!";
            this.mErrors.addOneError(tError);
            return null;
        }

        if (pLCAppAccTraceSchema.getMoney() >= 0)
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "AccShiftTo";
            tError.errorMessage = "本次金额应该小于0!";
            this.mErrors.addOneError(tError);
            return null;
        }

        tLCAppAccTraceSchema.setCustomerNo(pLCAppAccTraceSchema.getCustomerNo());
        tLCAppAccTraceSchema.setAccType(pLCAppAccTraceSchema.getAccType());
        String serialNo = getSerialNo(pLCAppAccTraceSchema.getCustomerNo()); //获得客户下的流水号
        if (serialNo == null || serialNo.equals(""))
        {
            return null;
        }
        tLCAppAccTraceSchema.setSerialNo(serialNo);
        tLCAppAccTraceSchema.setOtherNo(pLCAppAccTraceSchema.getOtherNo());
        tLCAppAccTraceSchema.setOtherType(pLCAppAccTraceSchema.getOtherType());
        tLCAppAccTraceSchema.setDestSource(pLCAppAccTraceSchema.getDestSource());
        tLCAppAccTraceSchema.setMoney(pLCAppAccTraceSchema.getMoney());
        //tLCAppAccTraceSchema.setAccBala(pLCAppAccSchema.getAccBala());//设置当前的帐户余额
        tLCAppAccTraceSchema.setState("0");
        tLCAppAccTraceSchema.setBakNo(pLCAppAccTraceSchema.getBakNo());
        tLCAppAccTraceSchema.setOperator(pLCAppAccTraceSchema.getOperator());
        tLCAppAccTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLCAppAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setModifyTime(PubFun.getCurrentTime());

        return tLCAppAccTraceSchema;
    }
    /** 投保人帐户抵扣
     * 只需要设置LCAppAccTraceSchema中的
     * CustomerNo 客户号码
     * AccType 帐户类型
     * OtherNo 对应其它号码
     * OtherType 对应其它号码类型
     * DestSource 用途或来源
     * Money 金额增减
     * Operator 操作员
     *@param pLCAppAccTraceSchema LCAppAccTraceSchema
     *@param pLCAppAccSchema LCAppAccSchema
     *@return MMap
     */
    private LCAppAccTraceSchema accTakeOutYET(LCAppAccTraceSchema pLCAppAccTraceSchema)
    {
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();

        if (!checkInput(pLCAppAccTraceSchema))
        {
            return null;
        }

        if (!pLCAppAccTraceSchema.getAccType().equals("0"))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "AccTakeOut";
            tError.errorMessage = "帐户类型不是抵扣或领取!";
            this.mErrors.addOneError(tError);
            return null;
        }

        if (!pLCAppAccTraceSchema.getDestSource().substring(0, 1).equals("0"))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "AccShiftTo";
            tError.errorMessage = "用途或来源定义不是抵扣或领取!";
            this.mErrors.addOneError(tError);
            return null;
        }


        tLCAppAccTraceSchema.setCustomerNo(pLCAppAccTraceSchema.getCustomerNo());
        tLCAppAccTraceSchema.setAccType(pLCAppAccTraceSchema.getAccType());
        String serialNo = getSerialNoYET(pLCAppAccTraceSchema.getCustomerNo()); //获得客户下的流水号
        if (serialNo == null || serialNo.equals(""))
        {
            return null;
        }
        tLCAppAccTraceSchema.setSerialNo(serialNo);
        tLCAppAccTraceSchema.setOtherNo(pLCAppAccTraceSchema.getOtherNo());
        tLCAppAccTraceSchema.setOtherType(pLCAppAccTraceSchema.getOtherType());
        tLCAppAccTraceSchema.setDestSource(pLCAppAccTraceSchema.getDestSource());
        tLCAppAccTraceSchema.setMoney(pLCAppAccTraceSchema.getMoney());
        //tLCAppAccTraceSchema.setAccBala(pLCAppAccSchema.getAccBala());//设置当前的帐户余额
        tLCAppAccTraceSchema.setState("0");
        tLCAppAccTraceSchema.setBakNo(pLCAppAccTraceSchema.getBakNo());
        tLCAppAccTraceSchema.setOperator(pLCAppAccTraceSchema.getOperator());
        tLCAppAccTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLCAppAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setModifyTime(PubFun.getCurrentTime());

        return tLCAppAccTraceSchema;
    }

    //写投保人人帐户领取轨迹表
    private LCAppAccGetTraceSchema addAppAccGetTrace(LCAppAccTraceSchema pLCAppAccTraceSchema,
            LCAppAccGetTraceSchema pLCAppAccGetTraceSchema)
    {
        LCAppAccGetTraceSchema saveLCAppAccGetTraceSchema = new LCAppAccGetTraceSchema();
        saveLCAppAccGetTraceSchema.setCustomerNo(pLCAppAccTraceSchema.getCustomerNo());
        saveLCAppAccGetTraceSchema.setSerialNo(pLCAppAccTraceSchema.getSerialNo());
        saveLCAppAccGetTraceSchema.setNoticeNo(pLCAppAccGetTraceSchema.getNoticeNo());
        saveLCAppAccGetTraceSchema.setAppDate(PubFun.getCurrentDate());
        saveLCAppAccGetTraceSchema.setName(pLCAppAccGetTraceSchema.getName());
        saveLCAppAccGetTraceSchema.setIDType(pLCAppAccGetTraceSchema.getIDType());
        saveLCAppAccGetTraceSchema.setIDNo(pLCAppAccGetTraceSchema.getIDNo());
        saveLCAppAccGetTraceSchema.setAccGetMode(pLCAppAccGetTraceSchema.getAccGetMode());
        saveLCAppAccGetTraceSchema.setBankCode(pLCAppAccGetTraceSchema.getBankCode());
        saveLCAppAccGetTraceSchema.setBankAccNo(pLCAppAccGetTraceSchema.getBankAccNo());
        saveLCAppAccGetTraceSchema.setAccName(pLCAppAccGetTraceSchema.getAccName());
        saveLCAppAccGetTraceSchema.setTransferDate(pLCAppAccGetTraceSchema.getTransferDate());
        saveLCAppAccGetTraceSchema.setAccGetMoney(pLCAppAccGetTraceSchema.getAccGetMoney());
        saveLCAppAccGetTraceSchema.setAccBala(pLCAppAccTraceSchema.getAccBala());
        saveLCAppAccGetTraceSchema.setAccGetDate(PubFun.getCurrentDate());
        saveLCAppAccGetTraceSchema.setAccGetTime(PubFun.getCurrentTime());
        saveLCAppAccGetTraceSchema.setOperator(pLCAppAccTraceSchema.getOperator());
        saveLCAppAccGetTraceSchema.setMakeDate(PubFun.getCurrentDate());
        saveLCAppAccGetTraceSchema.setMakeTime(PubFun.getCurrentTime());
        saveLCAppAccGetTraceSchema.setModifyDate(PubFun.getCurrentDate());
        saveLCAppAccGetTraceSchema.setModifyTime(PubFun.getCurrentTime());
        return saveLCAppAccGetTraceSchema;
    }

    /** 投保人帐户轨迹回退
     * 说明：如果mErrors.needDealError()说明查询数据库发生异常
     * 会修改投保人帐户的可领金额
     * @param customerNo String
     * @param otherNo String
     * @param otherNoType String
     *@return MMap
     */
    public MMap accUnDo(String customerNo, String otherNo, String otherNoType)
    {
        MMap map = new MMap();
        LCAppAccSchema tLCAppAccSchema = null;
        LCAppAccTraceSchema tLCAppAccTraceSchema = getLCAppAccTrace(customerNo, otherNo, otherNoType);
        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }

        if (tLCAppAccTraceSchema.getAccType().equals("0"))
        { //取消抵抠，增加可领金额
            tLCAppAccSchema = getLCAppAcc(customerNo);
            if (tLCAppAccSchema == null)
            {
                return null;
            }
            tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() - tLCAppAccTraceSchema.getMoney());
            tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
            tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(tLCAppAccSchema, "UPDATE");
        }

        map.put(tLCAppAccTraceSchema, "DELETE");
        return map;
    }

    /** 投保人帐户确认
     * 说明：如果mErrors.needDealError()说明查询数据库发生异常
     * 会修改投保人帐户的可领金额
     * @param customerNo String
     * @param otherNo String
     * @param otherNoType String
     * @return MMap
     */
    public MMap accConfirm(String customerNo, String otherNo, String otherNoType)
    {
        LCAppAccSchema tLCAppAccSchema = getLCAppAcc(customerNo);
        if (tLCAppAccSchema == null)
        {
            return null;
        }

        LCAppAccTraceSchema tLCAppAccTraceSchema = getLCAppAccTrace(customerNo, otherNo, otherNoType);
        if (tLCAppAccTraceSchema == null)
        {
            return null;
        }

        tLCAppAccTraceSchema.setState("1");
        tLCAppAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());

        if (tLCAppAccTraceSchema.getAccType().equals("0"))
        { //抵扣或领取
            tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
            tLCAppAccSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney());
        }
        if (tLCAppAccTraceSchema.getAccType().equals("1"))
        { //转入
            tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney()); //设置当前的帐户余额
            tLCAppAccSchema.setAccBala(tLCAppAccSchema.getAccBala() + tLCAppAccTraceSchema.getMoney());
            tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney() + tLCAppAccTraceSchema.getMoney());
        }
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCAppAccTraceSchema, "UPDATE");
        map.put(tLCAppAccSchema, "UPDATE");
        return map;
    }

    /** 根据客户号码,流水号查询投保人帐户领取轨迹
     * 如果mErrors.needDealError()说明查询数据库发生异常
     * 返回为空说明无记录
     * 不为空说明有记录
     * @param customerNo String
     * @param serialNo String
     * @return LCAppAccGetTraceSchema
     */
    public LCAppAccGetTraceSchema getLCAppAccGetTrace(String customerNo, String serialNo)
    {
        if (customerNo == null || customerNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccGetTrace";
            tError.errorMessage = "客户号为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (serialNo == null || serialNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccGetTrace";
            tError.errorMessage = "流水号为空!";
            this.mErrors.addOneError(tError);
            return null;
        }

        LCAppAccGetTraceDB tLCAppAccGetTraceDB = new LCAppAccGetTraceDB();
        tLCAppAccGetTraceDB.setCustomerNo(customerNo);
        tLCAppAccGetTraceDB.setSerialNo(serialNo);
        LCAppAccGetTraceSet tLCAppAccGetTraceSet = tLCAppAccGetTraceDB.query();
        if (tLCAppAccGetTraceDB.mErrors.needDealError())
        {
            // @@错误处理
            mErrors.copyAllErrors(tLCAppAccGetTraceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccGetTrace";
            tError.errorMessage = "查询投保人帐户轨迹表出错!";
            this.mErrors.addOneError(tError);
            return null;
        }

        if (tLCAppAccGetTraceSet.size() == 1)
        {
            return tLCAppAccGetTraceSet.get(1);
        }
        else if (tLCAppAccGetTraceSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccGetTrace";
            tError.errorMessage = "查询投保人帐户领取轨迹表记录不存在!";
            this.mErrors.addOneError(tError);
            return null;
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccGetTrace";
            tError.errorMessage = "查询投保人帐户领取轨迹表结果不正确!";
            this.mErrors.addOneError(tError);
            return null;
        }
    }

    /** 根据客户号码,其它号码，其它号码类型查询投保人帐户轨迹未确认记录
     * 如果mErrors.needDealError()说明查询数据库发生异常
     * 返回为空说明无记录
     * 不为空说明有记录
     * @param customerNo String
     * @param otherNo String
     * @param otherNoType String
     * @return LCAppAccTraceSchema
     */
    public LCAppAccTraceSchema getLCAppAccTrace(String customerNo, String otherNo, String otherNoType)
    {
        if (customerNo == null || customerNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "客户号为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (otherNo == null || otherNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "其它号码为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (otherNoType == null || otherNoType.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "其它号码类型为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        LCAppAccTraceDB tLCAppAccTraceDB = new LCAppAccTraceDB();
        tLCAppAccTraceDB.setCustomerNo(customerNo);
        tLCAppAccTraceDB.setOtherNo(otherNo);
        tLCAppAccTraceDB.setOtherType(otherNoType);
        tLCAppAccTraceDB.setState("0"); //未确定
        LCAppAccTraceSet tLCAppAccTraceSet = tLCAppAccTraceDB.query();
        if (tLCAppAccTraceDB.mErrors.needDealError())
        {
            // @@错误处理
            mErrors.copyAllErrors(tLCAppAccTraceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "查询投保人帐户轨迹表出错!";
            this.mErrors.addOneError(tError);
            return null;
        }

        if (tLCAppAccTraceSet.size() == 1)
        {
            return tLCAppAccTraceSet.get(1);
        }
        else if (tLCAppAccTraceSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "查询投保人帐户轨迹表记录不存在!";
            this.mErrors.addOneError(tError);
            System.out.println("qq:查询投保人帐户轨迹表记录不存在!");
            return null;
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "查询投保人帐户轨迹表结果不正确!";
            this.mErrors.addOneError(tError);
            return null;
        }
    }

    /** 根据客户号码,其它号码，其它号码类型查询投保人帐户轨迹已经确认记录
     * 如果mErrors.needDealError()说明查询数据库发生异常
     * 返回为空说明无记录
     * 不为空说明有记录
     * @param customerNo String
     * @param otherNo String
     * @param otherNoType String
     * @return LCAppAccTraceSchema
     */
    public LCAppAccTraceSchema getLCAppAccTraceConfirm(String customerNo, String otherNo, String otherNoType)
    {
        if (customerNo == null || customerNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "客户号为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (otherNo == null || otherNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "其它号码为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (otherNoType == null || otherNoType.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "其它号码类型为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        LCAppAccTraceDB tLCAppAccTraceDB = new LCAppAccTraceDB();
        tLCAppAccTraceDB.setCustomerNo(customerNo);
        tLCAppAccTraceDB.setOtherNo(otherNo);
        tLCAppAccTraceDB.setOtherType(otherNoType);
        //        tLCAppAccTraceDB.setState("1"); //未确定
        LCAppAccTraceSet tLCAppAccTraceSet = tLCAppAccTraceDB.query();
        if (tLCAppAccTraceDB.mErrors.needDealError())
        {
            // @@错误处理
            mErrors.copyAllErrors(tLCAppAccTraceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "查询投保人帐户轨迹表出错!";
            this.mErrors.addOneError(tError);
            return null;
        }

        if (tLCAppAccTraceSet.size() == 1)
        {
            return tLCAppAccTraceSet.get(1);
        }
        else if (tLCAppAccTraceSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "查询投保人帐户轨迹表记录不存在!";
            this.mErrors.addOneError(tError);
            return null;
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "查询投保人帐户轨迹表结果不正确!";
            this.mErrors.addOneError(tError);
            return null;
        }
    }

    /** 根据客户号码,流水号查询投保人帐户轨迹
     * 如果mErrors.needDealError()说明查询数据库发生异常
     * 返回为空说明无记录
     * 不为空说明有记录
     * @param customerNo String
     * @param serialNo String
     * @return LCAppAccTraceSchema
     */
    public LCAppAccTraceSchema getLCAppAccTrace(String customerNo, String serialNo)
    {
        if (customerNo == null || customerNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "客户号为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (serialNo == null || serialNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "流水号为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        LCAppAccTraceDB tLCAppAccTraceDB = new LCAppAccTraceDB();
        tLCAppAccTraceDB.setCustomerNo(customerNo);
        tLCAppAccTraceDB.setSerialNo(serialNo);
        LCAppAccTraceSet tLCAppAccTraceSet = tLCAppAccTraceDB.query();
        if (tLCAppAccTraceDB.mErrors.needDealError())
        {
            // @@错误处理
            mErrors.copyAllErrors(tLCAppAccTraceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "查询投保人帐户轨迹表出错!";
            this.mErrors.addOneError(tError);
            return null;
        }

        if (tLCAppAccTraceSet.size() == 1)
        {
            return tLCAppAccTraceSet.get(1);
        }
        else if (tLCAppAccTraceSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "查询投保人帐户轨迹表记录不存在!";
            this.mErrors.addOneError(tError);
            return null;
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAccTrace";
            tError.errorMessage = "查询投保人帐户轨迹表结果不正确!";
            this.mErrors.addOneError(tError);
            return null;
        }
    }

    /** 根据客户号码查询投保人帐户
     * 如果mErrors.needDealError()说明查询数据库发生异常
     * 返回为空说明无记录
     * 不为空说明有记录
     * @param tCustomerNo String
     * @return LCAppAccSchema
     */
    public LCAppAccSchema getLCAppAcc(String tCustomerNo)
    {
        if (tCustomerNo == null || tCustomerNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAcc";
            tError.errorMessage = "客户号为空!";
            this.mErrors.addOneError(tError);
            return null;
        }

        LCAppAccDB tLCAppAccDB = new LCAppAccDB();
        LCAppAccSet tLCAppAccSet = new LCAppAccSet();
        tLCAppAccDB.setCustomerNo(tCustomerNo);
        tLCAppAccSet = tLCAppAccDB.query();
        if (tLCAppAccDB.mErrors.needDealError())
        {
            // @@错误处理
            mErrors.copyAllErrors(tLCAppAccDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAcc";
            tError.errorMessage = "查询投保人帐户表出错!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (tLCAppAccSet.size() == 0)
        {
            // @@错误处理
            mErrors.copyAllErrors(tLCAppAccDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getLCAppAcc";
            tError.errorMessage = "查询投保人帐户表对应记录不存在!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLCAppAccSet.get(1);
    }

    /** 根据返回客户号码下的流水号
     * @param tCustomerNo String
     * @return String
     */
    private String getSerialNo(String customerNo)
    {
        String sql = "  select coalesce(max(cast(serialno as integer)),0)+1 serialno from lcappacctrace "
                + "where customerno = '" + customerNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS != null && tSSRS.getMaxRow() > 0)
        {
            return tSSRS.GetText(1, 1);
        }
        else
        {
            // @@错误处理
            mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getSerialNo";
            tError.errorMessage = "查询最大流水号出错!";
            this.mErrors.addOneError(tError);
            return null;
        }
    }
    /** 根据返回客户号码下的流水号
     * @param tCustomerNo String
     * @return String
     */
    private String getSerialNoYET(String customerNo)
    {
        String sql = "  select coalesce(max(cast(serialno as integer)),0)+2 serialno from lcappacctrace "
                + "where customerno = '" + customerNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS != null && tSSRS.getMaxRow() > 0)
        {
            return tSSRS.GetText(1, 1);
        }
        else
        {
            // @@错误处理
            mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AppAcc";
            tError.functionName = "getSerialNo";
            tError.errorMessage = "查询最大流水号出错!";
            this.mErrors.addOneError(tError);
            return null;
        }
    }

    public static void main(String[] args)
    {
        AppAcc tAppAcc = new AppAcc();
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo("000000140");
        tLCAppAccTraceSchema.setOtherNo("111");
        tLCAppAccTraceSchema.setOtherType("1");
        tLCAppAccTraceSchema.setMoney(100);
        tLCAppAccTraceSchema.setOperator("mj");
        MMap tMap = tAppAcc.accShiftToXQY(tLCAppAccTraceSchema, "1");
        if (tMap == null)
        {

        }
        else
        {
            VData data = new VData();
            data.add(tMap);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(data, ""))
            {
                return;
            }
        }
        return;
    }
}
