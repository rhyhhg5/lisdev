package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.*;

public class InureUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private InureBL tInureBL = null;
    
    public InureUI(GlobalInput gi,String contNo,String contType){
    	tInureBL = new InureBL(gi,contNo,contType);
    	System.out.println("UUUUUUIIIIIII "+contType);
    }
    
    /**
    传输数据的公共方法
    */
    public boolean submitData(){
        if(!tInureBL.submitData()){
            this.mErrors.copyAllErrors(tInureBL.mErrors);
            return false;
        }

        return true;
    }
    
    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult(){
        return this.mResult;
    }
}
