package com.sinosoft.lis.bq;

import java.util.Hashtable;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.operfee.CalInitialFee;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Sinosoft</p>
 * @author LC
 * @version 1.0
 */
public class PEdorMFAppConfirmBL implements EdorAppConfirm
{
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 全局数据 */
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    private LPEdorItemSchema mLPEdorItemSchema = null;
    private String mPolPayToDate;
    private String mCurPayToDate;
    
    private LPInsureAccFeeTraceSet mLPInsureAccFeeTraceSet = new LPInsureAccFeeTraceSet();
    
	//保险帐户表
    private LPInsureAccSchema mLPInsureAccSchema = new LPInsureAccSchema();
    private LPInsureAccTraceSet mLPInsureAccTraceSet = new LPInsureAccTraceSet();
    private LPInsureAccClassSchema mLPInsureAccClassSchema = new LPInsureAccClassSchema();    
    
	//保险帐户表记价履历表
    private LPInsureAccFeeSchema mLPInsureAccFeeSchema =new LPInsureAccFeeSchema();
    private LPInsureAccClassFeeSchema mLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();    
    
    LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
    
	private String mFeeCode;
    private boolean riskULIFlag = false;  //万能险复效标志

    private GlobalInput mGlobalInput = null;

    public PEdorMFAppConfirmBL()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //处理数据
        if (!dealData())
        {
            return false;
        }
        System.out.println("after dealData data....");

        //数据准备操作
        if (!prepareData())
        {
            return false;
        }
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorFXAppConfirmBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
	private void getInputData(VData cInputData) {
		mLPEdorItemSchema = (LPEdorItemSchema) cInputData
				.getObjectByObjectName("LPEdorItemSchema", 0);
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
	}

    /**
     * 本次交费=缴费期数*期交保费 + 利息
     */
    private boolean dealData()
    {
        try
        {
        	//期缴总保费
        	double tSumDuePayMoney = 0;
        	
            //得到复效险种
            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPPolDB.setEdorType(mLPEdorItemSchema.getEdorType());
            LPPolSet tLPPolSet = tLPPolDB.query();
            if(tLPPolSet == null || tLPPolSet.size() < 1)
            {
                CError tError = new CError();
                tError.errorMessage = "保全个人险种表中没有对应的记录!";
                this.mErrors.addOneError(tError);
                return false;
            }

            BqCalBL tBqCalBL = new BqCalBL();

            EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(
                mLPEdorItemSchema.getEdorNo(), mLPEdorItemSchema.getEdorType());
            tEdorItemSpecialData.query();
            for(int i = 1; i <= tLPPolSet.size(); i++)
            {
                double count = 0.0;
                //计算补交保费
                double prem = tLPPolSet.get(i).getPrem();
                String appDate = tEdorItemSpecialData.getEdorValue("MF_D");
                double rate = Double.parseDouble(tEdorItemSpecialData.
                                                 getEdorValue("MF_R"));

                String date = tLPPolSet.get(i).getPaytoDate();

                double temp = 0.0;
                int m = 0;  //第几期欠交
                String payEndDate=tLPPolSet.get(i).getPayEndDate();
                int m1=0; 
                
                if(tLPPolSet.get(i).getPayIntv() != 0)
                {
                    if(!date.equals(appDate))
                    {
                    m = PubFun.calInterval(date, appDate, "M")
                        / tLPPolSet.get(i).getPayIntv() + 1;
                    }
                    m1=m;
                    if(PubFun.calInterval(appDate,payEndDate,"M")<=0)
                    {
                    	m1=PubFun.calInterval(date, payEndDate, "M")
                        / tLPPolSet.get(i).getPayIntv() ;
                    }
                    //计算利息
                    for(int x = 0; x < m1; x++)
                    {
                        temp += prem
                            * (PubFun.calInterval(PubFun.calDate(date,
                            tLPPolSet.get(i).getPayIntv() * x, "M", ""),
                                                  appDate, "D")) * rate / 365.0;
                    }
                    
                    //计算下次交至日期
                     mPolPayToDate = PubFun.calDate(tLPPolSet.get(i).getPaytoDate(),
                    		(m * tLPPolSet.get(i).getPayIntv()),
                    		"M", "");
                     
                     //计算下一年度的交至日期
                     mCurPayToDate = PubFun.calDate(tLPPolSet.get(i).getPaytoDate(),
                    		( tLPPolSet.get(i).getPayIntv()),
                    		"M", "");
                    
                }
                

                //期交保费补费
                
                LJSGetEndorseSchema tLJSGetEndorseSchema = new
                    LJSGetEndorseSchema();
                	
                tSumDuePayMoney +=prem ;

                
                if("231001".equals(tLPPolSet.get(i).getRiskCode()) || "231201".equals(tLPPolSet.get(i).getRiskCode())|| "231801".equals(tLPPolSet.get(i).getRiskCode())){
                	String sql = "select startdate from lccontstate where " +
                			" polno = '" + tLPPolSet.get(i).getPolNo() + "'" +
                					" and statetype='Available' and state='1' with ur";
		               String tstartdate = new ExeSQL().getOneValue(sql);
		               if(tstartdate == null || tstartdate.equals(""))
		               {
		                   CError tError = new CError();
		                   tError.moduleName = "PEdorMFAppConfirmBL";
		                   tError.functionName = "checkData";
		                   tError.errorMessage = "未查到附加重疾险的失效日期";
		                   
		                   return false;
		                }
		               if(!date.equals(appDate)){
		            	   riskULIFlag = true;
		               }
		               
		               LCPolSchema tLCPolSchema = new LCPolSchema();
		               Reflections tReflections = new Reflections();
		               tReflections.transFields(tLCPolSchema, tLPPolSet.get(i).getSchema());
		               UARCalRPBL tUARCalRPBL = new UARCalRPBL();
		               if (tUARCalRPBL.submitDataP(tLPPolSet.get(i).getSchema(),tLCPolSchema,tstartdate,"12P")) {
		    	           System.out.println("万能附加险种风险扣费计算成功");
		    	       }
                }
                //判断保单是否有附加重疾险种
                String querySQL = "select 1 from lcpol where contno='"+tLPPolSet.get(i).getContNo()+"' and riskcode in ('231001','231201','231801') ";
                //判断是否需要缴一期保费
                if(new ExeSQL().getOneValue(querySQL)!=null&&!"".equals(new ExeSQL().getOneValue(querySQL))&&!CommonBL.stringToDate(mCurPayToDate).after(CommonBL.stringToDate(mPolPayToDate))){
                	
                	tLJSGetEndorseSchema = new BqCalBL().initLJSGetEndorse(
                			mLPEdorItemSchema,
                			tLPPolSet.get(i), null,
                			
                			tBqCalBL.getFinType(mLPEdorItemSchema.getEdorType(),
                					"BF", tLPPolSet.get(i).getPolNo()),
                					prem, mGlobalInput);
                	//判断是否需要万能续期抽挡
                	if(!CommonBL.stringToDate(mCurPayToDate).after(CommonBL.stringToDate(PubFun.getCurrentDate()))&&("231201".equals(tLPPolSet.get(i).getRiskCode())||"231001".equals(tLPPolSet.get(i).getRiskCode()) || "231801".equals(tLPPolSet.get(i).getRiskCode()))){
                		//添加lpespecialdate
                		mLPEdorEspecialDataSchema.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());

                		mLPEdorEspecialDataSchema.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());

                		mLPEdorEspecialDataSchema.setEdorType(mLPEdorItemSchema.getEdorType());
                		mLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
                		mLPEdorEspecialDataSchema.setDetailType("MF_XUQI");

                		mLPEdorEspecialDataSchema.setEdorValue("1");
                         
                	}
                }else{
                	tLJSGetEndorseSchema = new BqCalBL().initLJSGetEndorse(
                			mLPEdorItemSchema,
                			tLPPolSet.get(i), null,
                			
                			tBqCalBL.getFinType(mLPEdorItemSchema.getEdorType(),
                					"BF", tLPPolSet.get(i).getPolNo()),
                					prem * m1, mGlobalInput);
                }
                mLJSGetEndorseSet.add(tLJSGetEndorseSchema);
                //利息
                LJSGetEndorseSchema schemaLX = tLJSGetEndorseSchema.getSchema();
                schemaLX.setFeeFinaType(
                		tBqCalBL.getFinType(mLPEdorItemSchema.getEdorType(),
                				"MFLX", tLPPolSet.get(i).getPolNo()));
                schemaLX.setGetMoney(CommonBL.carry(temp));
                mLJSGetEndorseSet.add(schemaLX);
                mLPEdorItemSchema.setGetMoney(
                		mLPEdorItemSchema.getGetMoney()
                		+ tLJSGetEndorseSchema.getGetMoney()
                		+ schemaLX.getGetMoney());
            }

            //处理万能续期账户表
            if(riskULIFlag){
            	
            	if(!setInsuredAcc(mLPEdorItemSchema.getContNo(),tSumDuePayMoney)){
            		return false;
            	}
            }
            mLPEdorItemSchema.setEdorState("2");
            mLPEdorItemSchema.setUWFlag("0");
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
            CError tError = new CError();
            tError.errorMessage = "MF项目申请确认时出现异常错误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    
    private boolean setInsuredAcc(String contno,double ttSumDuePayMoney){
    	//查询保单万能账户分类表
		LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
		LCInsureAccClassDB  tLCInsureAccClassDB = new LCInsureAccClassDB();
		LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
		tLCInsureAccClassDB.setContNo(contno);
		tLCInsureAccClassSet = tLCInsureAccClassDB.query();
		double tSumMoney=0.0; // 余额
		if(tLCInsureAccClassSet!=null && tLCInsureAccClassSet.size()>0){
			tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1).getSchema();
		}else{
            CError tError = new CError();
            tError.errorMessage = "MF项目申请确认时出现异常错误,万能保单的万能账户分类信息查询错误!";
            this.mErrors.addOneError(tError);
            return false;
		}
		
		double tFee=0.0;  //初始扣费
		
		//获取主险险种
		String mainRiskCode = tLCInsureAccClassSchema.getRiskCode();
		
		ExeSQL tExeSQL = new ExeSQL();
		
     	String sql = "select count(1) from ldcode where codetype = 'ulifee' and code = '" + mainRiskCode + "' with ur";
     	String tCount = tExeSQL.getOneValue(sql);
		
     	//查询主险险种的schema
     	LCPolSchema tLCPolSchema = new LCPolSchema();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(contno);
		tLCPolDB.setRiskCode(mainRiskCode);
		LCPolSet tLCPolSet = tLCPolDB.query();
		if(tLCPolSet!=null&&tLCPolSet.size()>0){
			tLCPolSchema = tLCPolSet.get(1).getSchema();
		}
		double tAmnt = tLCPolSchema.getAmnt();
		
     	if(!"0".equals(tCount))
     	{
     		tFee=getULIFee(tLCPolSchema ,ttSumDuePayMoney,tLCInsureAccClassSchema.getInsuAccNo(),tAmnt);
     	}else{
 			tFee=getFee(tLCPolSchema ,ttSumDuePayMoney,tLCInsureAccClassSchema.getInsuAccNo(),mainRiskCode);     		
     	}
     	
     	if(tFee == -1)
 		{
 			CError tError = new CError();
    		tError.moduleName = "OmnipFinUrgeVerifyBL";
    		tError.functionName = "setInsuredAcc";
    		tError.errorMessage = "计算初始费用失败!";
    		mErrors.addOneError(tError);
    		return false;
 		}
     	
     	System.out.println("得到扣费***********"+tFee);
     	double ctFee = Double.parseDouble("-"+tFee);  
     	double tempMoney =ttSumDuePayMoney+ctFee;	
     	
     	tSumMoney =tLCInsureAccClassSchema.getInsuAccBala()+tempMoney;
		
 		// 处理持续奖励
    	double tJL=getJL(tLCPolSchema,ttSumDuePayMoney);
    	if(tJL==-2){
    		CError tError = new CError();
    		tError.moduleName = "PEdorZBAppConfirmBL";
    		tError.functionName = "setInsuredAcc";
    		tError.errorMessage = "计算持续奖励失败!";
    		mErrors.addOneError(tError);
    		return false;
    		
    	}
    	if(tJL>0){
    		tSumMoney+=tJL;
    		tempMoney+=tJL;
    	}  
    	Reflections ref = new Reflections();
    	
    	//处理附加重疾风险保费
    	LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
    	LPInsureAccTraceSet tLPInsureAccTraceSet = new LPInsureAccTraceSet();
    	
    	tLPInsureAccTraceDB.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());
    	tLPInsureAccTraceDB.setOtherNo(mLPEdorItemSchema.getEdorAcceptNo());
    	tLPInsureAccTraceDB.setMoneyType("RP");
    	tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
    	if(tLPInsureAccTraceSet!=null&&tLPInsureAccTraceSet.size()>0){
    		tSumMoney += tLPInsureAccTraceSet.get(1).getMoney();
    	}
    	
    	System.out.println("持续奖金是*******tJL = "+tJL);
    	tLCInsureAccClassSchema.setInsuAccBala(tSumMoney);
    	
    	tLCInsureAccClassSchema.setModifyDate(PubFun.getCurrentDate());
    	tLCInsureAccClassSchema.setModifyTime(PubFun.getCurrentTime());  
    	LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
		// 相同部分
		ref.transFields(tLPInsureAccClassSchema, tLCInsureAccClassSchema);
		// 不同部分
		tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccClassSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPInsureAccClassSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		mLPInsureAccClassSchema=tLPInsureAccClassSchema;
    	
    	
    	LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
    	LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
    	LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
    	LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
    	tLCInsureAccDB.setContNo(tLCInsureAccClassSchema.getContNo());
    	tLCInsureAccSet = tLCInsureAccDB.query();
    	if(tLCInsureAccSet!=null&&tLCInsureAccSet.size()>0){
    		tLCInsureAccSchema = tLCInsureAccSet.get(1).getSchema();
    	}else{
    		CError tError = new CError();
    		tError.moduleName = "PEdorZBAppConfirmBL";
    		tError.functionName = "setInsuredAcc";
    		tError.errorMessage = "万能账户表查询失败!";
    		mErrors.addOneError(tError);
    		return false;   		
    	}
		// 相同部分
		ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
		// 不同部分
		tLPInsureAccSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPInsureAccSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPInsureAccSchema.setInsuAccBala(tSumMoney);
		tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccSchema.setModifyDate(PubFun.getCurrentDate());
		tLPInsureAccSchema.setModifyTime(PubFun.getCurrentTime());  		
		mLPInsureAccSchema=tLPInsureAccSchema;
		
        this.setLPInsureAccTrace(tLCInsureAccClassSchema, "BF", ttSumDuePayMoney,mLPEdorItemSchema,tLCPolSchema);	
        this.setLPInsureAccTrace(tLCInsureAccClassSchema, "GL", ctFee,mLPEdorItemSchema,tLCPolSchema);
        this.setLPInsureAccFeeTrace(tLCInsureAccClassSchema,"KF",tFee,mLPEdorItemSchema,tLCPolSchema);// 扣费轨迹    	         	    
        if(tJL>0){
          this.setLPInsureAccTrace(tLCInsureAccClassSchema,  "B",tJL,mLPEdorItemSchema,tLCPolSchema);// 持续奖励轨迹(acctrace)	    	         
        }  
		
        
        // 处理管理费帐户主表
        LCInsureAccFeeSchema tLCInsureAccFeeSchema = new LCInsureAccFeeSchema();
        LPInsureAccFeeSchema tLPInsureAccFeeSchema = new LPInsureAccFeeSchema();
        LCInsureAccFeeSet tLCInsureAccFeeSet = new LCInsureAccFeeSet();
        LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
        tLCInsureAccFeeDB.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
        tLCInsureAccFeeDB.setPolNo(tLCInsureAccClassSchema.getPolNo());
        
        tLCInsureAccFeeSet= tLCInsureAccFeeDB.query();
        if(tLCInsureAccFeeSet.size()<1)
        {
       		CError tError = new CError();
     		tError.moduleName = "PEdorZBAppConfirmBL";
     		tError.functionName = "setInsuredAcc";
     		tError.errorMessage = "查询帐户管理费分类表数据失败!";
     		mErrors.addOneError(tError);
     		return false;
     	 }
       tLCInsureAccFeeSchema=tLCInsureAccFeeSet.get(1);     
		// 相同部分
		ref.transFields(tLPInsureAccFeeSchema, tLCInsureAccFeeSchema);
		// 不同部分
		tLPInsureAccFeeSchema.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());
		tLPInsureAccFeeSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPInsureAccFeeSchema.setFee(tLCInsureAccFeeSchema.getFee()+Math.abs(tFee));
		tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccFeeSchema.setModifyDate(PubFun.getCurrentDate());
		tLPInsureAccFeeSchema.setModifyTime(PubFun.getCurrentTime());
		mLPInsureAccFeeSchema = tLPInsureAccFeeSchema;
		
		 // 处理管理费帐户分类表
	       LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
	       LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
	       tLCInsureAccClassFeeDB.setPolNo(tLCInsureAccClassSchema.getPolNo());
	       //tLCInsureAccClassFeeDB.setAccType(BQ.ACCTYPE_INSURED);
	       tLCInsureAccClassFeeDB.setPayPlanCode(tLCInsureAccClassSchema.getPayPlanCode());
	       tLCInsureAccClassFeeDB.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
	       tLCInsureAccClassFeeSet=tLCInsureAccClassFeeDB.query();
	       if(tLCInsureAccClassFeeSet.size()<1)
	       {
	       		CError tError = new CError();
	     		tError.moduleName = "PEdorZBAppConfirmBL";
	     		tError.functionName = "setInsuredAcc";
	     		tError.errorMessage = "查询帐户分类表数据失败!";
	     		mErrors.addOneError(tError);
	     		return false;
	     			
	       }  
	       LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema= new LCInsureAccClassFeeSchema();
	       LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema= new LPInsureAccClassFeeSchema();
	       tLCInsureAccClassFeeSchema=tLCInsureAccClassFeeSet.get(1);
	       ref.transFields(tLPInsureAccClassFeeSchema, tLCInsureAccClassFeeSchema);
	       tLPInsureAccClassFeeSchema.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());
	       tLPInsureAccClassFeeSchema.setEdorType(mLPEdorItemSchema.getEdorType());	  
	       tLPInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
	       tLPInsureAccClassFeeSchema.setModifyDate(PubFun.getCurrentDate());
	       tLPInsureAccClassFeeSchema.setModifyTime(PubFun.getCurrentTime());
	       tLPInsureAccClassFeeSchema.setFee(tLCInsureAccClassFeeSchema.getFee()+Math.abs(tFee));
	       mLPInsureAccClassFeeSchema=tLPInsureAccClassFeeSchema;
		
    	return true;
    }
    
	/**
	*健康宝A（万能型）期交保险费的减少应从最新增加的部分开始减；期交保险费的增加，增加部分都归属第一年度。根据你所举的例子具体初始费用的扣除如下：
	*首期期交保险费为3000元，基本保险金额为9万元。第二年期交保险费变更为2000元，第三年期交保险费增加至6000元，第四年期交保险费减少至5000元。
	*1、第一年初始费用，3000元*50%=1500元；
	*2、第二年初始费用，2000元*25%=500元；
	*3、第三年初始费用，2000元*15%+2500元*50%+1500元*5%=300元+1250元+75元=1625元
	*（新增的4000元按首年的初始费用比例扣除，同时按投保时的基本保险金额的1/20区分不同的比例）
	*4、第四年初始费用，2000元*10%+2500元*25%+500元*5%=200元+625元+25元=850元。
	*/
	private double getULIFee(LCPolSchema tLCPolSchema,double ttSumDuePayMoney,String cInsuAccNo, double cAmnt) 
	{
		
		double fee = 0; //管理费
		
		double curPrem = ttSumDuePayMoney; //本期保费
		
		double basePrem = 0;//基本保费：期交保费，保额的1/20和6000较小的一个
		
		if(cAmnt != 0.0 && (cAmnt/20)<6000)
		{
			basePrem = (cAmnt/20);
		}
		else
		{
			basePrem = 6000;
		}
		
		if(curPrem < basePrem)
		{
			basePrem = curPrem;
		}
		
		CalInitialFee tCalInitialFee = new CalInitialFee(tLCPolSchema.getPolNo(),ttSumDuePayMoney,mCurPayToDate,basePrem,"MF");
		Hashtable cur = tCalInitialFee.calTest();
		
		if(cur == null)
		{
			return -1;
		}
		
		for (int j = 1; j <= cur.size(); j++) 
		{
			String PremPeriod = (String)cur.get(String.valueOf(j));
			String[] sPremPeriod = new String[2];
			sPremPeriod = PremPeriod.split("@");
			
			//缴费金额
			double tCurPrem = Double.parseDouble(sPremPeriod[0]);
			
			//第几次缴费
			int tPayCount = Integer.parseInt(sPremPeriod[1]);
			
			System.out.println("curPrem == " + tCurPrem);
			System.out.println("curPeriod == " + tPayCount);
			
//			 查询管理费
			LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
			tLMRiskFeeDB.setInsuAccNo(cInsuAccNo);
			tLMRiskFeeDB.setFeeKind("03"); // 03-个单管理费
			tLMRiskFeeDB.setFeeItemType("04");// 04-初始扣费
			tLMRiskFeeDB.setFeeTakePlace("01"); // 01－交费时		
			LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
			if (tLMRiskFeeDB.mErrors.needDealError()) 
			{
				CError.buildErr(this, "账户管理费查询失败!");
				return -1;
			}
			
			double tempFee = calRiskFee(tLMRiskFeeSet.get(1), tCurPrem,tLCPolSchema, tPayCount);
			System.out.println("缴费金额"+tCurPrem+"按照第 "+tPayCount+" 次计算管理费 tempFee = " + tempFee);
			fee += tempFee;
		}
		
//			本次续期存在额外保费的才进行处理
			if (curPrem > basePrem) 
			{
				CalInitialFee tminusCalInitialFee = new CalInitialFee(tLCPolSchema.getPolNo(),ttSumDuePayMoney,mCurPayToDate,basePrem,"MF");
				Hashtable minuscur = tminusCalInitialFee.calMinus();
				if(minuscur == null)
				{
					return -1;
				}
				
				for (int j = 1; j <= minuscur.size(); j++) 
				{
					String PremPeriod = (String)minuscur.get(String.valueOf(j));
					String[] sPremPeriod = new String[2];
					sPremPeriod = PremPeriod.split("@");
					
					//缴费金额
					double tCurMinusPrem = Double.parseDouble(sPremPeriod[0]);
					
					//第几次缴费
					int tPayCount = Integer.parseInt(sPremPeriod[1]);
					
					System.out.println("curPrem == " + tCurMinusPrem);
					System.out.println("curPeriod == " + tPayCount);
					
//					额外保费的费率在描述表中取
					String sqlFeeLv="select rate from rate"+tLCPolSchema.getRiskCode()+" where type='1' and "+tPayCount
					               +"  >= mincount and "+tPayCount+"<=maxcount ";
					if(!tLCPolSchema.getRiskCode().equals("331701")){
						sqlFeeLv+=" and payintv=(select payintv from lccont where contno ='"
					               +tLCPolSchema.getContNo()+"')  with ur";
					}
					String resultMinusRate = new ExeSQL().getOneValue(sqlFeeLv);
					if (resultMinusRate != null && !resultMinusRate.equals("")) 
					{
						double minusRate=Double.parseDouble(resultMinusRate);
						double minusFee=tCurMinusPrem*minusRate;
						System.out.println("缴费金额的额外保费金额中"+tCurMinusPrem+"按照第 "+tPayCount+" 次计算管理费 tempFee = " + minusFee);
						fee += minusFee;
					}
					else
					{
						CError.buildErr(this, "额外保费费率查询有误!");
						return -1;
					}
				}
				
			}
//		}
		System.out.println("拢共收取续期管理费 = " + fee);
		return fee;
	}    
	
	// 设置LCInsuAccTrace
	private void setLPInsureAccTrace(
			LCInsureAccClassSchema aLCInsureAccClassSchema, String aMoneyType,
			double aMoney,LPEdorItemSchema tLPEdorItemSchema,LCPolSchema tLCPolSchema) {

		Reflections ref = new Reflections();
		LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
		// 相同部分
		ref.transFields(tLPInsureAccTraceSchema, aLCInsureAccClassSchema);
		// 不同部分
		String serialNo = PubFun1.CreateMaxNo("SERIALNO",
				aLCInsureAccClassSchema.getManageCom());
		tLPInsureAccTraceSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPInsureAccTraceSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPInsureAccTraceSchema.setSerialNo(serialNo);
		tLPInsureAccTraceSchema.setMoneyType(aMoneyType);
		tLPInsureAccTraceSchema.setPayDate(tLPEdorItemSchema.getEdorValiDate());
		
		//20090817 zhanggm 为了区别首期保费，将续期othertype设置为‘2’，otherno = 应收号。
		tLPInsureAccTraceSchema.setOtherNo(tLPEdorItemSchema.getEdorAcceptNo());
		tLPInsureAccTraceSchema.setOtherType("10");
		tLPInsureAccTraceSchema.setAccAscription("0");
		tLPInsureAccTraceSchema.setUnitCount("0");
		tLPInsureAccTraceSchema.setState("0");
        
		tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccTraceSchema.setMakeDate(PubFun.getCurrentDate());
		tLPInsureAccTraceSchema.setMakeTime(PubFun.getCurrentTime());
		tLPInsureAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
		tLPInsureAccTraceSchema.setModifyTime(PubFun.getCurrentTime());
		tLPInsureAccTraceSchema.setMoney(aMoney);

		mLPInsureAccTraceSet.add(tLPInsureAccTraceSchema);
	}
	
	
	private void setLPInsureAccFeeTrace(
			LCInsureAccClassSchema aLCInsureAccClassSchema, String moneyType,
			 double manageMoney,LPEdorItemSchema tLPEdorItemSchema,LCPolSchema tLCPolSchema) {

		Reflections ref = new Reflections();
		LPInsureAccFeeTraceSchema tLPInsureAccFeeTraceSchema = new LPInsureAccFeeTraceSchema();
		// 相同部分
		ref.transFields(tLPInsureAccFeeTraceSchema, aLCInsureAccClassSchema);
		// 不同部分
		tLPInsureAccFeeTraceSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPInsureAccFeeTraceSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		String serialNo = PubFun1.CreateMaxNo("SERIALNO",
				aLCInsureAccClassSchema.getManageCom());
		tLPInsureAccFeeTraceSchema.setSerialNo(serialNo);
		tLPInsureAccFeeTraceSchema.setMoneyType(moneyType);
		tLPInsureAccFeeTraceSchema.setFeeRate(0);
		tLPInsureAccFeeTraceSchema.setFee(manageMoney);
		tLPInsureAccFeeTraceSchema.setFeeUnit(aLCInsureAccClassSchema
				.getUnitCount());
		tLPInsureAccFeeTraceSchema.setManageCom(aLCInsureAccClassSchema.getManageCom());
		tLPInsureAccFeeTraceSchema.setPayDate(tLPEdorItemSchema.getEdorValiDate());
		
		//tLCInsureAccFeeTraceSchema.setFeeCode("");
		tLPInsureAccFeeTraceSchema.setInerSerialNo("");
		
//		20090817 zhanggm 为了区别首期保费，将续期othertype设置为‘2’，otherno = 应收号。
		tLPInsureAccFeeTraceSchema.setOtherNo(tLPEdorItemSchema.getEdorAcceptNo());
		tLPInsureAccFeeTraceSchema.setOtherType("10");
		
		tLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccFeeTraceSchema.setMakeDate(PubFun.getCurrentDate());
		tLPInsureAccFeeTraceSchema.setMakeTime(PubFun.getCurrentTime());
		tLPInsureAccFeeTraceSchema.setModifyDate(PubFun.getCurrentDate());
		tLPInsureAccFeeTraceSchema.setModifyTime(PubFun.getCurrentTime());
		
		mLPInsureAccFeeTraceSet.add(tLPInsureAccFeeTraceSchema);
	}	
	
	
	
	private double calRiskFee(LMRiskFeeSchema pLMRiskFeeSchema,
			double dSumPrem,LCPolSchema tLCPolSchema, int CountTime) {
		double dRiskFee = 0.0;
		if (pLMRiskFeeSchema.getFeeCalModeType().equals("0")) // 0-直接取值
		{
			if (pLMRiskFeeSchema.getFeeCalMode().equals("01")) // 固定值内扣
			{
				dRiskFee = pLMRiskFeeSchema.getFeeValue();
			} else if (pLMRiskFeeSchema.getFeeCalMode().equals("02")) // 固定比例内扣
			{
				dRiskFee = dSumPrem * pLMRiskFeeSchema.getFeeValue();
			} else {
				dRiskFee = pLMRiskFeeSchema.getFeeValue(); // 默认情况
			}
		} else if (pLMRiskFeeSchema.getFeeCalModeType().equals("1")) // 1-SQL算法描述
		{
			// 准备计算要素
			Calculator tCalculator = new Calculator();
			tCalculator.setCalCode(pLMRiskFeeSchema.getFeeCalCode());
			// 累计已交保费
			tCalculator.addBasicFactor("Prem", String.valueOf(dSumPrem));
			// 下面三个要素用来计算个险万能首期交费的初始费用计算。added by huxl @20080512
			
			double tAmnt = CommonBL.getTBAmnt(tLCPolSchema);
			tCalculator.addBasicFactor("Amnt", String.valueOf(tAmnt));
			tCalculator.addBasicFactor("PayIntv", String.valueOf(tLCPolSchema
					.getPayIntv()));
			tCalculator.addBasicFactor("CountTime", String.valueOf(CountTime)); //

			String sCalResultValue = tCalculator.calculate();
			if (tCalculator.mErrors.needDealError()) {
				CError.buildErr(this, "管理费计算失败!");
				return -1;
			}

			try {
				dRiskFee = Double.parseDouble(sCalResultValue);
			} catch (Exception e) {
				CError.buildErr(this, "管理费计算结果错误!" + "错误结果：" + sCalResultValue);
				return -1;
			}
		}

		return dRiskFee;
	}
    
	
	private double getFee(LCPolSchema tLCPolSchema,
			 double currentFee,String cInsuAccNo,String RiskCode) {
		double tFee;
		String tInsuAccNo = cInsuAccNo;

		// 查询管理费
		LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
		tLMRiskFeeDB.setInsuAccNo(tInsuAccNo);
		// tLMRiskFeeDB.setPayPlanCode(mPayPlanCode);
		tLMRiskFeeDB.setFeeKind("03"); // 03-个单管理费
		tLMRiskFeeDB.setFeeItemType("04");// 04-初始扣费
		tLMRiskFeeDB.setFeeTakePlace("01"); // 01－交费时		
		LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
		if (tLMRiskFeeDB.mErrors.needDealError()) {
			CError.buildErr(this, "账户管理费查询失败!");
			return -1;
		}
		int tPayCount=0;
		String tPayDate =mPolPayToDate;
		//TODO 
		tPayCount=PubFun.calInterval(tLCPolSchema.getCValiDate(),tPayDate,"M")/tLCPolSchema.getPayIntv();
		mFeeCode = tLMRiskFeeSet.get(1).getFeeCode();
		tFee = calRiskFee(tLMRiskFeeSet.get(1), currentFee, tLCPolSchema,tPayCount);
		System.out.println("@@@@@@@@@@@ fee = "+tFee);
		return tFee;
	}
    

    private boolean prepareData()
    {
        MMap map = new MMap();
        map.put(mLJSGetEndorseSet, "DELETE&INSERT");
        if(riskULIFlag){
        	if(mLPInsureAccFeeTraceSet!=null&&mLPInsureAccFeeTraceSet.size()>0){
        		map.put(mLPInsureAccFeeTraceSet, "DELETE&INSERT");
        	}
        	if(mLPInsureAccTraceSet!=null&&mLPInsureAccTraceSet.size()>0){
        		map.put(mLPInsureAccTraceSet, "DELETE&INSERT");
        	}
        	if(mLPInsureAccSchema!=null&&mLPInsureAccSchema.getContNo()!=null){
        		map.put(mLPInsureAccSchema, "DELETE&INSERT");
        	}
        	if(mLPInsureAccClassSchema!=null&&mLPInsureAccClassSchema.getContNo()!=null){
        		map.put(mLPInsureAccClassSchema, "DELETE&INSERT");
        	}
        	if(mLPInsureAccFeeSchema!=null&&mLPInsureAccFeeSchema.getContNo()!=null){
        		map.put(mLPInsureAccFeeSchema, "DELETE&INSERT");
        	}
        	if(mLPInsureAccClassFeeSchema!=null&&mLPInsureAccClassFeeSchema.getContNo()!=null){
        		map.put(mLPInsureAccClassFeeSchema, "DELETE&INSERT");
        	}
        	if(mLPEdorEspecialDataSchema.getDetailType()!=null){
        		map.put(mLPEdorEspecialDataSchema, "DELETE&INSERT");
        	}
        }
        map.put(mLPEdorItemSchema, "UPDATE");
        

        mResult.clear();
        mResult.add(map);
        return true;
    }
    
	private double getJL(LCPolSchema tLCPolSchema,double ttSumDuePayMoney ) {
		double fee = 0.0;
		double tPrem = ttSumDuePayMoney;
		System.out.println("本次保费*******tPrem = "+tPrem);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		
		String tLapseDate = PubFun.calDate(tLCPolSchema.getPaytoDate(),60, "D", null);
		FDate tFDate= new FDate();
		if(tFDate.getDate(PubFun.getCurrentDate()).after(tFDate.getDate(tLapseDate)))
		{
			System.out.println("已过缴费宽限期，无需赠送!");
			return -1;
		}
		
/*		获取持续奖励描述
 *	     添加至ldcode1表中
	    CodeType =‘CXJL ’
	    Code1 -险种编码
	    CodeName -开始领取保单年度
	    CodeAlias ：0-趸交，1-期交
	    ComCode ：赠送比例
	    */
		LDCode1Schema tLDCode1Schema=new LDCode1Schema();
		LDCode1DB tLDCode1DB=new LDCode1DB();
		tLDCode1DB.setCodeType("CXJL");
		tLDCode1DB.setCode1(tLCPolSchema.getRiskCode());
		if(tLDCode1DB.query().size()>1){
			System.out.println("持续奖励查询描述表失败!");
			return -2;
		}else if(tLDCode1DB.query().size()<1){
			return -1;
		}
		tLDCode1Schema=tLDCode1DB.query().get(1);
		
		if(tLDCode1Schema.getCodeAlias().equals("0")){
			System.out.println("趸交不在此处处理持续奖金!");
			return -1;
		}
		
		String tSql = "select 1 from llclaimdetail where (getdutykind like '5%'  or getdutykind='800') and contno='"
					+tLCPolSchema.getContNo()+"' and riskcode='"+tLCPolSchema.getRiskCode()+"'  with ur";
		String tResult=tExeSQL.getOneValue(tSql);
		if(tResult!=null&&tResult.equals("1")){
			System.out.println("被保人已经身故，无需给付!");
			return -1;
		}
		
//		起始保单年度,在产品条款中定义的赠送持续奖金的年份上加1
		int tShouldYear=Integer.parseInt(tLDCode1Schema.getCodeName());
//		给付比例
		double trate=Double.parseDouble(tLDCode1Schema.getComCode());
		
		String tPayDate = mPolPayToDate;
		int tInterval = PubFun.calInterval( tLCPolSchema.getCValiDate(),tPayDate, "Y");
		System.out.println("交费年度*******tInterval = "+tInterval);
		if (tInterval < tShouldYear) {
			return -1;
		} else {
			// 累计部分领取的个人账户价值不超过累计追加保险费与累计已领取的持续奖金二项之和
			tSql = "select count(*) from ldsysvar where sysvar='onerow' and (select value(sum(-getmoney),0) from "
					+ "ljagetendorse where polno='"
					+ tLCPolSchema.getPolNo()
					+ "'  and "
					+ "feeoperationtype='LQ')<=(select value(sum(money),0) from lcinsureacctrace where polno='"
					+ tLCPolSchema.getPolNo()
					+ "' and moneytype in('KF','ZF','B'))";
			tSSRS = tExeSQL.execSQL(tSql);
			if (tSSRS == null) {
				System.out.println("持续奖励查询帐户表失败");
				return -2;
			}
			if (tSSRS.GetText(1, 1).equals("0")) {
				return -1;
			}
			fee = CommonBL.carry(Arith.mul(tPrem, trate));
		}
			return fee;
		}   
    
    

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "pa0001";
        tGlobalInput.ManageCom = "86";

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo("20070320000002");
        tLPEdorItemDB.setEdorType(BQ.EDORTYPE_MF);

        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPEdorItemDB.query().get(1));

        PEdorMFAppConfirmBL tPEdorMFAppConfirmBL = new
                                                   PEdorMFAppConfirmBL();
        if (!tPEdorMFAppConfirmBL.submitData(tVData, "INSERT"))
        {
            System.out.println(tPEdorMFAppConfirmBL.mErrors.getErrContent());
        }

    }
}
