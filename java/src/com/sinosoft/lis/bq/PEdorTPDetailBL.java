package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LBInsuredDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBAppntSchema;
import com.sinosoft.lis.schema.LBBnfSchema;
import com.sinosoft.lis.schema.LBInsuredSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LPAppntSchema;
import com.sinosoft.lis.schema.LPBnfSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPDutySchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPGetSchema;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.schema.LPPremSchema;
import com.sinosoft.lis.vschema.LBAppntSet;
import com.sinosoft.lis.vschema.LBBnfSet;
import com.sinosoft.lis.vschema.LBInsuredSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LPAppntSet;
import com.sinosoft.lis.vschema.LPBnfSet;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.lis.vschema.LPDutySet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPGetSet;
import com.sinosoft.lis.vschema.LPInsuredSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.lis.vschema.LPPremSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author fuxin
 * @version 1.0
 */
public class PEdorTPDetailBL {

    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LPInsuredSet mLPInsuredSet = new LPInsuredSet();
    private LPContSet mLPContSet = new LPContSet();
    private LPPolSet mLPPolSet = new LPPolSet();
    private LPPremSet mLPPremSet = new LPPremSet();
    private LPGetSet mLPGetSet = new LPGetSet();
    private LPDutySet mLPDutySet = new LPDutySet();
    private LPAppntSet mLPAppntSet = new LPAppntSet();
    private LPBnfSet mLPBnfSet = new LPBnfSet();


    private MMap map = new MMap();
    private Reflections ref = new Reflections();
    TransferData tempTransferData = new TransferData();
    VData  mVData = new VData();
    private String oldEdorNo = null ;

    private String currDate = PubFun.getCurrentDate();
    private String currTime = PubFun.getCurrentTime();



    public PEdorTPDetailBL() {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        //准备往后台的数据
        if (!prepareData()) {
            return false;
        }
        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, "")) { //数据提交
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("PEdorRBDetailBL End PubSubmit");
        return true;
    }

    public boolean getInputData() {
        try {
            mGlobalInput = (GlobalInput) mInputData
                           .getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData
            .getObjectByObjectName("LPEdorItemSchema", 0);            
        } catch (Exception e) {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mGlobalInput == null || mLPEdorItemSchema == null) {
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "输入数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public boolean checkData()
    {
        if (!checkEdorItem())
        {
            return false ;
        }
        return true;
    }

    public boolean dealData()
    {
        String edorno = mLPEdorItemSchema.getEdorNo();
        String edortype = mLPEdorItemSchema.getEdorType();
        String sql = " edorno='" + edorno + "' and edortype='" + edortype + "'";
        map.put("delete from lpcont where" + sql, "DELETE");
        map.put("delete from lppol where" + sql, "DELETE");
        map.put("delete from lpprem where" + sql, "DELETE");
        map.put("delete from lpduty where" + sql, "DELETE");
        map.put("delete from lpget where" + sql, "DELETE");
        map.put("delete from lpappnt where" + sql, "DELETE");
        map.put("delete from lpinsured where" + sql, "DELETE");

        //备份保单信息
        LCContSet tLCContSet = getLCCcont();
        for(int i = 1; i <= tLCContSet.size() ;i++)
        {
            LCContSchema tLCContSchema = tLCContSet.get(i);
            LPContSchema tLPContSchema = new LPContSchema();
            ref.transFields(tLPContSchema,tLCContSchema);
            tLPContSchema.setOperator(mGlobalInput.Operator);
            tLPContSchema.setEdorNo(edorno);
            tLPContSchema.setEdorType(edortype);
            mLPContSet.add(tLPContSchema);

            map.put(mLPContSet,"DELETE&INSERT");
        }
        //备份险种信息
        LCPolSet tLCPolSet = getLCPol();
        if(tLCPolSet.size()==0)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
//            tError.functionName = "";
            tError.errorMessage = "没有需要回退的险种！请删除工单！";
            this.mErrors.addOneError(tError);
            return false;
        }
        for(int i = 1 ;i <= tLCPolSet.size() ; i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            LPPolSchema tLPPolSchema = new LPPolSchema();
            ref.transFields(tLPPolSchema,tLCPolSchema);
            tLPPolSchema.setEdorNo(edorno);
            tLPPolSchema.setEdorType(edortype);
            tLPPolSchema.setOperator(mGlobalInput.Operator);
            mLPPolSet.add(tLPPolSchema);

            map.put(mLPPolSet,"DELETE&INSERT");
            
        }
        //备份prem中的数据到P表
        LCPremSet tLCPremSet = getLCPrem();
        for(int i=1 ; i<= tLCPremSet.size(); i++)
        {
            LCPremSchema tLCPremSchema = tLCPremSet.get(i);
            LPPremSchema tLPPremSchema = new LPPremSchema();
            ref.synchronizeFields(tLPPremSchema,tLCPremSchema);
            tLPPremSchema.setEdorNo(edorno);
            tLPPremSchema.setEdorType(edortype);
            tLPPremSchema.setOperator(mGlobalInput.Operator);
            mLPPremSet.add(tLPPremSchema);

            map.put(mLPPremSet,"DELETE&INSERT");
        }
        // 备份责任表Lpduty
        LCDutySet tLCDutySet = getLCDuty();
        for(int i=1 ; i<=tLCDutySet.size() ; i++)
        {
            LCDutySchema tLCDutySchema = tLCDutySet.get(i);
            LPDutySchema tLPDutySchema = new LPDutySchema();
            ref.synchronizeFields(tLPDutySchema,tLCDutySchema);
            tLPDutySchema.setEdorNo(edorno);
            tLPDutySchema.setEdorType(edortype);
            tLPDutySchema.setOperator(mGlobalInput.Operator);
            mLPDutySet.add(tLPDutySchema);

            map.put(mLPDutySet,"DELETE&INSERT");
        }
        // 备份给付责任表 lpget
        LCGetSet tLCGetSet = getLCGet();
        for (int i=1 ; i <= tLCGetSet.size() ; i++)
        {
            LCGetSchema  tLCGetSchema = tLCGetSet.get(i);
            LPGetSchema  tLPGetSchema = new LPGetSchema();
            ref.synchronizeFields(tLPGetSchema,tLCGetSchema);
            tLPGetSchema.setEdorNo(edorno);
            tLPGetSchema.setEdorType(edortype);
            tLPGetSchema.setOperator(mGlobalInput.Operator);
            mLPGetSet.add(tLPGetSchema);

            map.put(mLPGetSet,"DELETE&INSERT");
        }
       
        changeNo();
        
        mLPEdorItemSchema.setEdorState("1");
        mLPEdorItemSchema.setModifyDate(currDate);
        mLPEdorItemSchema.setModifyTime(currTime);
        map.put(mLPEdorItemSchema, "UPDATE");
               
        return true ;
    }
    
    public void changeNo(){
    	//修改险种号
    	String updatePOL = " update lppol set riskcode= " +
    			" (select code1 from ldcode1 where codetype = 'riskedortp' and codename='riskcode' and lppol.riskcode=code )," +
    			" modifydate=current date,modifytime=current time where edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"' " +
    					" and riskcode in (select code from ldcode1 where codetype = 'riskedortp' and codename='riskcode') ";
    	map.put(updatePOL, "UPDATE");
    	//修改责任代码
    	String updateDuty = " update lpduty set dutycode= " +
    			" (select code1 from ldcode1 where codetype = 'riskedortp' and codename='dutycode' and lpduty.dutycode=code ), " +
    			" modifydate=current date,modifytime=current time where edorno= '"+mLPEdorItemSchema.getEdorAcceptNo()+"' " +
    					" and dutycode in ( select code from ldcode1 where codetype = 'riskedortp' and codename='dutycode' ) ";
    	map.put(updateDuty, "UPDATE");
    	//修改保费责任
    	String updateprem = " update lpprem set dutycode=  " +
    			" (select code1 from ldcode1 where codetype = 'riskedortp' and codename='dutycode' and lpprem.dutycode=code ), " +
    			" payplancode=nvl((select code1 from ldcode1 where codetype = 'riskedortp' and codename='payplancode' and lpprem.payplancode=code ),lpprem.payplancode), " +
    			" modifydate=current date,modifytime=current time where edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"' " +
    					" and dutycode in ( select code from ldcode1 where codetype = 'riskedortp' and codename='dutycode' ) ";
    	map.put(updateprem, "UPDATE");
    	//修改给付责任
    	String updateGET = " update lpget set dutycode= " +
    			" (select code1 from ldcode1 where codetype = 'riskedortp' and codename='dutycode' and lpget.dutycode=code ), " +
    			" getdutycode=(select code1 from ldcode1 where codetype = 'riskedortp' and codename='getdutycode' and lpget.getdutycode=code ), " +
    			" modifydate=current date,modifytime=current time where edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"' " +
    					" and dutycode in ( select code from ldcode1 where codetype = 'riskedortp' and codename='dutycode' ) ";
    	map.put(updateGET, "UPDATE");
    }


    public boolean prepareData()
    {
        mResult.clear();
//        tempTransferData.setNameAndValue("oldEdorNo",oldEdorNo);
//        mResult.add(tempTransferData);
        mResult.add(map);
        return true ;
    }

    private boolean checkEdorItem()
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();

        if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
            CError tError = new CError();
            tError.moduleName = "PEdorRBDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "输入数据有误,LPEdorItem中没有相关数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLPEdorItemSchema = tLPEdorItemSet.get(1);
        mLPEdorItemSchema.setEdorState("1");

        return true ;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult() {
        return mResult;
    }
    /**
     * 得到保单信息
     */
    private LCContSet getLCCcont()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LCCont ")
                .append("where CONTNO = '")
                .append(mLPEdorItemSchema.getContNo())
                .append("' ");
        System.out.println(sql.toString());
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = tLCContDB.executeQuery(sql.toString());
        return tLCContSet;
    }

    /**
     * 得到险种信息
     */
    private LCPolSet getLCPol()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LCPol ")
                .append("where CONTNO = '")
                .append(mLPEdorItemSchema.getContNo())
                .append("' ");
        System.out.println(sql.toString());
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql.toString());
        return tLCPolSet ;
    }
    /**
     * Prem 表
     * @return LBPremSet
     */
    private LCPremSet getLCPrem()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LCPrem ")
                .append("where CONTNO = '")
                .append(mLPEdorItemSchema.getContNo())
                .append("' ");
        System.out.println(sql.toString());
        LCPremDB tLCPremDB = new LCPremDB();
        LCPremSet tLCPremSet = tLCPremDB.executeQuery(sql.toString());
        return tLCPremSet;
    }
    /**
     *
     */
    private LCDutySet getLCDuty()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LCDuty ")
                .append("where CONTNO = '")
                .append(mLPEdorItemSchema.getContNo())
                .append("' ");
        System.out.println(sql.toString());
        LCDutyDB tLCDutyDB = new LCDutyDB();
        LCDutySet tLCDutySet = tLCDutyDB.executeQuery(sql.toString());
        return tLCDutySet ;
    }
    /**
     * lbget信息
     * @return LBGetSet
     */
    private LCGetSet getLCGet()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LCGet ")
                .append("where CONTNO = '")
                .append(mLPEdorItemSchema.getContNo())
                .append("' ");
        System.out.println(sql.toString());
        LCGetDB tLCGetDB =new LCGetDB();
        LCGetSet tLCGetSet = tLCGetDB.executeQuery(sql.toString());
        return tLCGetSet;
    }



    

}
