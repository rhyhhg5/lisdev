package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

import java.io.*;

import java.sql.*;


/**
 * <p>Title: Web业务系统保全申请确认变动功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author Tjj
 * @version 1.0
 */
public class GEdorConfirmBL
{
    //传输数据类
    private VData mInputData;

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    //操作符
    public String mOperate;
    public String mStrTemplatePath = "";
    private VData mResult = new VData();
    private LJAPaySchema mLJAPaySchema = null;

    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    private LPGrpEdorMainSet tLPGrpEdorMainSet = new LPGrpEdorMainSet();

    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();


    public GEdorConfirmBL()
    {
    }

    protected boolean LRPolPrint(String GrpPolNo)
    {
        System.out.println("start LRPolPrint...");
        //LCGrpPolF1PUI tLCGrpPolF1PUI = new LCGrpPolF1PUI();
        //LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();

        //tLCGrpPolSchema.setGrpPolNo(GrpPolNo); //必须使用主险保单号
        //tLCGrpPolSet.add(tLCGrpPolSchema);
        //更新LCGrpPol表中的printcount字段为0
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(GrpPolNo);
        tLCGrpPolSchema = tLCGrpPolDB.query().get(1);

        String strSql =
                "update LCGrpPol set printcount = 0 where grppolno in (select grppolno from lcgrppol where prtno = '" +
                tLCGrpPolSchema.getPrtNo() + "')";
        System.out.println(strSql);
        tLCGrpPolDB.executeQuery(strSql);

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult()
    {
        return mResult;
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {

         LPGrpEdorMainSchema aLPGrpEdorMainSchema = new LPGrpEdorMainSchema();

        System.out.println("Start GrpEdorConfirm BL Submit...");
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        aLPGrpEdorMainSchema = (LPGrpEdorMainSchema) mInputData.
                               getObjectByObjectName("LPGrpEdorMainSchema",
                0);
        mLJAPaySchema = (LJAPaySchema)
                mInputData.getObjectByObjectName("LJAPaySchema", 0);
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setSchema(aLPGrpEdorMainSchema);
        tLPGrpEdorMainDB.getInfo();
        if (tLPGrpEdorMainDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询团体批单主表时失败");
            return false;
        }
        mLPGrpEdorMainSchema.setSchema(tLPGrpEdorMainDB);
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput",
                0);
        mStrTemplatePath = (String) mInputData.get(2);

        //校验团体保全主表的核保标记，校验补费、退费核销
       // if (!checkData())
       // {
       //     return false;
       // }

        if (!dealData())
        {
            return false;
        }

        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("End 校验团体保全主表的核保标记，校验补费、退费核销");

        return true;
    }

    private boolean dealData()
    {
        String execSql = "";
        ExeSQL aExeSQL = new ExeSQL();
        LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();

        LPGrpEdorItemDB aLPGrpEdorItemDB = new LPGrpEdorItemDB();
        String strsqlp = "select * from LPGrpEdorItem where GrpContNo ='" +
                         mLPGrpEdorMainSchema.getGrpContNo() +
                         "' and Edorno = '" +
                         mLPGrpEdorMainSchema.getEdorNo() +
                         "' order by MakeDate,MakeTime";
        tLPGrpEdorItemSet = aLPGrpEdorItemDB.executeQuery(strsqlp);

        //按照保全项目来进行确认（tjj chg 1024）
        System.out.println("按照保全项目来进行确认 tLPGrpEdorItemSet.size()" +
                           tLPGrpEdorItemSet.size());
        String tGrpContNo = mLPGrpEdorMainSchema.getGrpContNo();
        String tEdorNo = mLPGrpEdorMainSchema.getEdorNo();
        for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++)
        {

            LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
            tLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemSet.get(i));

            String tEdorType = tLPGrpEdorItemSchema.getEdorType();
            //检查个单是否全部通过确认
            LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
            tLPGrpEdorItemDB.setGrpContNo(tGrpContNo);
            tLPGrpEdorItemDB.setEdorNo(tEdorNo);
            tLPGrpEdorItemDB.setEdorType(tEdorType);
            tLPGrpEdorItemDB.setEdorState("1");
            int aCount = tLPGrpEdorItemDB.getCount();
            if (tLPGrpEdorItemDB.mErrors.needDealError())
            {
                CError.buildErr(this,
                                "查询批单号为" + tEdorNo + ",项目为" + tEdorType +
                                "下个单是否全部通过确认时失败");
                return false;
            }

            System.out.println("acount: " + aCount);

            //if (aCount == 0)
            //{

                tLPGrpEdorItemSchema.setEdorState("0");
                tLPGrpEdorItemSchema.setModifyDate(theCurrentDate);
                tLPGrpEdorItemSchema.setModifyTime(theCurrentTime);
                tLPGrpEdorItemSchema.setOperator(mGlobalInput.Operator);

                //按照保全项目对集体单进行特殊处理
                System.out.println("按照保全项目对集体单进行特殊处理 edortype:" +
                                   tLPGrpEdorItemSchema.getEdorType());

                try
                {
                    Class tClass = Class.forName("com.sinosoft.lis.bq.GrpEdor" +
                                                 tEdorType +
                                                 "ConfirmBL");
                    EdorConfirm tGrpEdorConfirm = (EdorConfirm)
                                                  tClass.newInstance();
                    VData tVData = new VData();

                    tVData.add(tLPGrpEdorItemSchema);
                    // tVData.add(mLJSGetEndorseSchema);
                    tVData.add(mGlobalInput);
                    tVData.add(tLPGrpEdorMainSet);
                    tVData.add(mLJAPaySchema);
                    if (!tGrpEdorConfirm.submitData(tVData,
                            "CONFIRM||" + tEdorType))
                    {
                        mErrors.copyAllErrors(tGrpEdorConfirm.mErrors);
                        return false;
                    }
                    else
                    {
                        VData rVData = tGrpEdorConfirm.getResult();
                        MMap tMap = new MMap();
                        tMap = (MMap) rVData.getObjectByObjectName("MMap", 0);
                        if (tMap == null)
                        {
                            CError.buildErr(this,
                                            "得到保全项目为:" + tEdorType +
                                            "的保全确认结果时失败！");
                            return false;

                        }
                        else
                        {
                            map.add(tMap);
                        }
                    }

                }
                catch (ClassNotFoundException ex)
                {
                	ex.printStackTrace();
                    map.put(tLPGrpEdorItemSchema, "UPDATE");
                }
                catch (Exception ex)
                {
                	ex.printStackTrace();
                    CError.buildErr(this, "保全项目" + tEdorType + "确认时失败！");
                    return false;
                }
            }
        //}
        mLPGrpEdorMainSchema.setEdorState("0");
        mLPGrpEdorMainSchema.setConfDate(theCurrentDate);
        mLPGrpEdorMainSchema.setConfTime(theCurrentTime);
        mLPGrpEdorMainSchema.setConfOperator(mGlobalInput.Operator);
        map.put(mLPGrpEdorMainSchema, "UPDATE");

        //更新LPEdorApp表中的保全状态
        String sql = "update LPEdorApp " +
                     "set EdorState = '0', " +
                     "    ConfDate = '" + PubFun.getCurrentDate() + "', " +
                     //"    ConfTime = '" + PubFun.getCurrentTime() + "', " +
                     "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                     "    Operator = '" + mGlobalInput.Operator + "', " +
                     "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                     "where EdorAcceptNo = '"
                     + mLPGrpEdorMainSchema.getEdorAcceptNo() + "'";
        map.put(sql, "UPDATE");

        tLPGrpEdorMainSet.add(mLPGrpEdorMainSchema);

        updateContInfo();

        return true;
    }

    /**
     * 维护保单数据
     */
    private void updateContInfo()
    {
    	//为了防止汇总sql不执行，先执行一次，下面再执行一次
    	System.out.println("先汇总一次LCGrpCont、LCGrpPol、LCContPlanRisk，看你还更不更新111111111111");
    	StringBuffer sql_pro1 = new StringBuffer(128);
    	sql_pro1.append("update LCGrpCont a set (Prem,Amnt,SumPrem) = ")
            .append("(select sum(Prem),sum(Amnt),sum(SumPrem) from LCCont ")
            .append("where appFlag = '1' and grpcontno = a.grpcontno), ")
            .append("LastEdorDate='").append(mLPGrpEdorMainSchema.getEdorValiDate())
            .append("',Operator = '").append(mGlobalInput.Operator)
            .append("',ModifyDate = '").append(theCurrentDate)
            .append("', ModifyTime = '").append(theCurrentTime)
            .append("'  where GrpContNo = '")
            .append(mLPGrpEdorMainSchema.getGrpContNo()).append("' ");
        map.put(sql_pro1.toString(), "UPDATE");
        
        StringBuffer sql_pro2 = new StringBuffer(128);
        sql_pro2.append("update LCGrpPol a set (Prem,Amnt,SumPrem) = ")
        	.append("(select sum(Prem),sum(Amnt),sum(SumPrem) from LCPol ")
        	.append("where appFlag = '1' and grppolno = a.grppolno), ")
        	.append("Operator = '").append(mGlobalInput.Operator)
        	.append("',ModifyDate = '").append(theCurrentDate)
        	.append("', ModifyTime = '").append(theCurrentTime)
        	.append("'  where a.GrpContNo = '")
        	.append(mLPGrpEdorMainSchema.getGrpContNo()).append("' ");
        map.put(sql_pro2.toString(), "UPDATE");

        String sql_pro3 = "  update LCContPlanRisk a "
                  + "set riskPrem = (select sum(prem) from LCPol where grpContNo = a.grpContNo and riskCode = a.riskCode and contPlanCode = a.contPlanCode and appFlag = '1'), "
                  + "   Operator = '" + mGlobalInput.Operator + "', "
                  + "   ModifyDate = '" + theCurrentDate + "', "
                  + "   ModifyTime = '" + theCurrentTime + "' "
                  + "where grpContNo = '"
                  + mLPGrpEdorMainSchema.getGrpContNo() + "' ";
        map.put(sql_pro3, "UPDATE");
        System.out.println("第一次汇总LCGrpCont、LCGrpPol、LCContPlanRisk完毕。\n"+sql_pro1+"\n"+sql_pro2+"\n"+sql_pro3);
    	
        String sql = "update LCPol a "
                   + "set contPlanCode = (select contPlanCode from LCInsured where contNo = a.contNo and insuredNo = a.insuredNo) "
                   + "where grpContNo = '"
                   + mLPGrpEdorMainSchema.getGrpContNo() + "' ";
        map.put(sql, "UPDATE");
        
        /*
         * 2011-11-11 <Happy Singles' Awareness Day for everyone>
         * When the NI & ZT was made together at a LCCont,the prem will be inaccuracy.
         * It need to debug for this. 
         * So,it renew the prem after all of the  transaction be finished.
         */
        //------------------------------------------------------------------
        //0.Updating the LCCont.prem by  amount to the LCPol.prem
        String LCPolprem_SQL = "update LCCont a "
            + "set prem = (select sum(prem) from lcpol where contNo = a.contNo) "
            + "where grpContNo = '"
            + mLPGrpEdorMainSchema.getGrpContNo() + "' ";
         map.put(LCPolprem_SQL, "UPDATE");
         //-------------------------THE END------------------------------------
        
         System.out.println("再汇总一次LCGrpCont、LCGrpPol、LCContPlanRisk，看你还更不更新2222222222222");
        StringBuffer sql1 = new StringBuffer(128);
        sql1.append("update LCGrpCont a set (Prem,Amnt,SumPrem) = ")
            .append("(select sum(Prem),sum(Amnt),sum(SumPrem) from LCCont ")
            .append("where appFlag = '1' and grpcontno = a.grpcontno), ")
            .append("LastEdorDate='").append(mLPGrpEdorMainSchema.getEdorValiDate())
            .append("',Operator = '").append(mGlobalInput.Operator)
            .append("',ModifyDate = '").append(theCurrentDate)
            .append("', ModifyTime = '").append(theCurrentTime)
            .append("'  where GrpContNo = '")
            .append(mLPGrpEdorMainSchema.getGrpContNo()).append("' ");
        map.put(sql1.toString(), "UPDATE");
        
        StringBuffer sql2 = new StringBuffer(128);
        sql2.append("update LCGrpPol a set (Prem,Amnt,SumPrem) = ")
        	.append("(select sum(Prem),sum(Amnt),sum(SumPrem) from LCPol ")
        	.append("where appFlag = '1' and grppolno = a.grppolno), ")
        	.append("Operator = '").append(mGlobalInput.Operator)
        	.append("',ModifyDate = '").append(theCurrentDate)
        	.append("', ModifyTime = '").append(theCurrentTime)
        	.append("'  where a.GrpContNo = '")
        	.append(mLPGrpEdorMainSchema.getGrpContNo()).append("' ");
        map.put(sql2.toString(), "UPDATE");

            sql = "  update LCContPlanRisk a "
                  + "set riskPrem = (select sum(prem) from LCPol where grpContNo = a.grpContNo and riskCode = a.riskCode and contPlanCode = a.contPlanCode and appFlag = '1'), "
                  + "   Operator = '" + mGlobalInput.Operator + "', "
                  + "   ModifyDate = '" + theCurrentDate + "', "
                  + "   ModifyTime = '" + theCurrentTime + "' "
                  + "where grpContNo = '"
                  + mLPGrpEdorMainSchema.getGrpContNo() + "' ";
            map.put(sql, "UPDATE");
            System.out.println("第二次汇总LCGrpCont、LCGrpPol、LCContPlanRisk完毕。\n"+sql_pro1+"\n"+sql_pro2+"\n"+sql_pro3);
            
            System.out.println("*********************UpdatePeoples****************************");
            
            LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
            tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorMainSchema.getEdorNo());
            LPGrpEdorItemSet set = tLPGrpEdorItemDB.query();
            if(set.size() > 0)
            {
                UpdatePeoplesBL tUpdatePeoplesBL =
                    new UpdatePeoplesBL(set.get(1).getGrpContNo());
                MMap tMMap = tUpdatePeoplesBL.getSubmitData();
                if(tUpdatePeoplesBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tUpdatePeoplesBL.mErrors);
                }
                map.add(tMMap);
            }
            System.out.println("****************UpdatePeoples*********************************");
    }

    private boolean prepareOutputData()
    {

        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
        mResult.add(map);

        return true;
    }

    public void setOperate(String cOperate)
    {
        this.mOperate = cOperate;
    }

    public String getOperate()
    {
        return this.mOperate;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {
        boolean flag = true;
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        LPGrpEdorMainSet tLPGrpEdorItemSet = new LPGrpEdorMainSet();
        String sql = "select * from LPGrpEdorMain where edorno='" +
                     mLPGrpEdorMainSchema.getEdorNo() +
                     "' and uwstate in ('0','5')";
        tLPGrpEdorItemSet.clear();
        tLPGrpEdorItemSet = tLPGrpEdorMainDB.executeQuery(sql);

        if ((tLPGrpEdorItemSet != null) && (tLPGrpEdorItemSet.size() > 0))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "EdorConfirmBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该申请核保没有通过!";
            this.mErrors.addOneError(tError);

            return false;
        }
        return flag;
    }

    public static void main(String[] args)
    {
        VData tInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        GEdorConfirmBL aGEdorConfirmBL = new GEdorConfirmBL();
        LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "endor";
		tLPGrpEdorMainSchema.setEdorAcceptNo("20050730000025");
        tLPGrpEdorMainSchema.setEdorNo("20050730000025");

        String strTemplatePath = "xerox/printdata/";
        tInputData.addElement(tLPGrpEdorMainSchema);
        tInputData.addElement(tGlobalInput);
		tInputData.addElement(strTemplatePath);
        aGEdorConfirmBL.submitData(tInputData, "INSERT||GRPEDORCONFIRM");
    }
}
