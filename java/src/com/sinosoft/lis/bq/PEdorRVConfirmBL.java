package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.db.LPEdorMainDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class PEdorRVConfirmBL implements EdorConfirm {

	public PEdorRVConfirmBL() {
		// TODO Auto-generated constructor stub
	}

	/** 往后面传输数据的容器 */
	private VData mInputData;
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	private LPEdorItemSchema mLPEdorItemSchema = null;
	private ValidateEdorData2 mValidateEdorData = null;
	private String mCurDate = PubFun.getCurrentDate();
	private String mCurTime = PubFun.getCurrentTime();
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mEdorNo = null;
	private MMap mMap = new MMap();
	private String mEdorType = null;
	private String mContNo = null;

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public void setOperate(String cOperate) {
		this.mOperate = cOperate;
	}

	public String getOperate() {
		return this.mOperate;
	}

	/**
	 * 返回计算结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		VData data = new VData();
		data.add(mMap);
		return data;
	}

	 public boolean submitData(VData cInputData, String cOperate)
	  {
	      getInputData(cInputData);
	      if (!dealData())
	      {
	          return false;
	      }
	      return true;
	  }

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		 mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
	      mLPEdorItemSchema = (LPEdorItemSchema)cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
	      mEdorNo = mLPEdorItemSchema.getEdorNo();
	      mEdorType = mLPEdorItemSchema.getEdorType();
	      mContNo = mLPEdorItemSchema.getContNo();
	      mValidateEdorData = new ValidateEdorData2(mGlobalInput, mEdorNo,mEdorType, mContNo, "ContNo");
		return true;
	}

	/**
	 * 准备需要保存的数据
	 */
	private boolean dealData() {
		if (!validateEdorData()) {
			return false;
		}
		 return true;
	}

	/**
	 * 使保全数据生效
	 * 
	 * @return boolean
	 */
	private boolean validateEdorData() {
		String[] addTables = { "LCInsureAccTrace", "LCInsureAccFeeTrace" };
		mValidateEdorData.addData(addTables);
		String[] chgTables = { "LCPol", "LCCont", "LCPrem", "LCDuty",
				"LCInsureAcc", "LCInsureAccFee", "LCInsureAccClass",
				"LCInsureAccClassFee" };
		mValidateEdorData.changeData(chgTables);
		
		String sqledortyp = " select enteraccdate from ljtempfee where otherno='"+mEdorNo+"' with ur";
		String tPayDate = new ExeSQL().getOneValue(sqledortyp);
        String  tValidDate = PubFun.calDate(tPayDate, 1, "D", "");
		String sql = "UPDATE LCContState set state='0',enddate={d '"+tValidDate+"'} where contno = '"+mContNo+"' with ur" ;
        System.out.println(tValidDate);
		mMap.add(mValidateEdorData.getMap());
		mMap.put(sql, "UPDATE");
		return true;
	}
    public static void main(String [] args){
    	PEdorRVConfirmBL tPEdorRVConfirmBL =new PEdorRVConfirmBL();
    	tPEdorRVConfirmBL.validateEdorData();
    	
    }
}
