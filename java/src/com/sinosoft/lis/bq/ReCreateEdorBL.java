package com.sinosoft.lis.bq;

import java.io.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ReCreateEdorBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap map = new MMap();
    /** 数据操作字符串 */
    private String mOperate;
    /**处理中用到的数据*/
    private String deal = "";
    private String newDate = "";
    private String type = "";
    private String getPerson = "";
    private String appKind = "";
    private String no = "";
    private String actuGetNo = "";
    private String gpflag = "";
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private XmlExport tXmlExport;
    public ReCreateEdorBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        //得到外部传入的数据,将数据备份到本类中
        if(!getInputData())
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }
        mResult.add(map);
        PubSubmit tSubmit = new PubSubmit();
        if(!tSubmit.submitData(mResult, ""))
        { //数据提交
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "TransferFailDealBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean getInputData()
    {
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
            "GlobalInput", 0));
        TransferData temp = (TransferData) mInputData.getObjectByObjectName(
            "TransferData", 0);
        actuGetNo = (String) temp.getValueByName("actuGetNo");
        newDate = (String) temp.getValueByName("newDate");
        deal = (String) temp.getValueByName("deal");
        type = (String) temp.getValueByName("type");
        gpflag = (String) temp.getValueByName("gpflago");
        if(actuGetNo == null || actuGetNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "TransferFailDealBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
        MMap map = new MMap();

        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setActuGetNo(actuGetNo);
        LJAGetSet set = tLJAGetDB.query();
        if(set == null || set.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "TransferFailDealBL";
            tError.functionName = "dealData";
            tError.errorMessage = "LJAGet查询失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(mGlobalInput.Operator);
        tLDUserDB.getInfo();

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLDUserDB.getComCode());
        tLDComDB.getInfo();

        //团险无转账
        if(set.get(1).getOtherNoType().equals("3"))
        {
//            //更新批单信息
//            LPEdorPrintDB tLPEdorPrintDB = new LPEdorPrintDB();
//            tLPEdorPrintDB.setEdorNo(set.get(1).getOtherNo());
//            LPEdorPrintSet tLPEdorPrintSet = tLPEdorPrintDB.query();
//            if(tLPEdorPrintSet == null || tLPEdorPrintSet.size() < 1)
//            {
//                CError tError = new CError();
//                tError.moduleName = "ReCreateEdorBL";
//                tError.functionName = "dealData";
//                tError.errorMessage = "查找批单信息错误!";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
//            LPEdorPrintSchema tLPEdorPrintSchema = tLPEdorPrintSet.get(1);
//            if(tLPEdorPrintSchema.getEdorInfo().equals(""))
//            {
//                CError tError = new CError();
//                tError.moduleName = "ReCreateEdorBL";
//                tError.functionName = "dealData";
//                tError.errorMessage = "查找批单信息错误!";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
//            //修改批单信息
//
//
//            XmlUpdate tXmlUpdate = new XmlUpdate(tLPEdorPrintSchema.getEdorInfo());
//            TextTag textTag = new TextTag();
//            textTag.add("ConfDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
//            textTag.add("Operator", tLDUserDB.getUserName());
//            textTag.add("ComName", tLDComDB.getLetterServiceName());
//            textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
//            tXmlUpdate.addTextTag(textTag);
//            tXmlUpdate.update();
//            InputStream ins = tXmlUpdate.getInputStream();
//            tLPEdorPrintSchema.setEdorInfo(ins);
//            System.out.println(tXmlUpdate.getXmlString().toString());
//
//            tLPEdorPrintSchema.setModifyDate(PubFun.getCurrentDate());
//            tLPEdorPrintSchema.setModifyTime(PubFun.getCurrentTime());
//            tLPEdorPrintSchema.setOperator(mGlobalInput.Operator);
////            tLPEdorPrintDB.delete();
//
//            map.put("delete from LPEdorPrint where EdorNo = '"
//                    + tLPEdorPrintSchema.getEdorNo() + "' ", SysConst.DELETE);
//            map.put(tLPEdorPrintSchema.getSchema(), "BLOBINSERT");
//
//            FeeNoticeGrpVtsBL tFeeNoticeGrpVtsBL = new FeeNoticeGrpVtsBL(set.
//                get(1).getOtherNo());
//            TransferData mPayInfo = new TransferData();
//
//            if("reset".equals(deal))
//            {
//                mPayInfo.setNameAndValue("payMode", "4");
//                mPayInfo.setNameAndValue("payDate", set.get(1).getApproveDate()); //转帐日期
//                mPayInfo.setNameAndValue("endDate", ""); //截止日期
//                mPayInfo.setNameAndValue("bank", set.get(1).getBankCode());
//                mPayInfo.setNameAndValue("bankAccno", set.get(1).getBankAccNo());
//                mPayInfo.setNameAndValue("accName", set.get(1).getAccName());
//            }
//            else
//            {
//                mPayInfo.setNameAndValue("payMode", "1");
//            }
//
//            tFeeNoticeGrpVtsBL.submitData(mPayInfo);
        }
        else
        {
//更新批单信息
            LPEdorAppPrintDB tLPEdorAppPrintDB = new LPEdorAppPrintDB();
            tLPEdorAppPrintDB.setEdorAcceptNo(set.get(1).getOtherNo());
            LPEdorAppPrintSet tLPEdorAppPrintSet = tLPEdorAppPrintDB.query();
            if(tLPEdorAppPrintSet == null || tLPEdorAppPrintSet.size() < 1)
            {
                CError tError = new CError();
                tError.moduleName = "ReCreateEdorBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查找批单信息错误!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LPEdorAppPrintSchema tLPEdorAppPrintSchema = tLPEdorAppPrintSet.get(
                1);
            if(tLPEdorAppPrintSchema.getEdorInfo().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ReCreateEdorBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查找批单信息错误!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //修改批单信息

            XmlUpdate tXmlUpdate = new XmlUpdate(tLPEdorAppPrintSchema.
                                                 getEdorInfo());
            TextTag textTag = new TextTag();
            textTag.add("ConfDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
            textTag.add("Operator", tLDUserDB.getUserName());
            textTag.add("ComName", tLDComDB.getLetterServiceName());
            textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
            tXmlUpdate.addTextTag(textTag);
            tXmlUpdate.update();
            InputStream ins = tXmlUpdate.getInputStream();
            tLPEdorAppPrintSchema.setEdorInfo(ins);
            System.out.println(tXmlUpdate.getXmlString());
            tLPEdorAppPrintSchema.setModifyDate(PubFun.getCurrentDate());
            tLPEdorAppPrintSchema.setModifyTime(PubFun.getCurrentTime());
            tLPEdorAppPrintSchema.setOperator(mGlobalInput.Operator);
//            tLPEdorAppPrintDB.delete();

            map.put("delete from LPEdorAppPrint where EdorAcceptNo = '"
                    + tLPEdorAppPrintSchema.getEdorAcceptNo() + "' ",
                    SysConst.DELETE);
            map.put(tLPEdorAppPrintSchema.getSchema(), "BLOBINSERT");

            VData vdata = new VData();
            vdata.add(map);
            PubSubmit tPubSubmit = new PubSubmit();
            if(!tPubSubmit.submitData(vdata, ""))
            {
                CError tError = new CError();
                tError.moduleName = "ReCreateEdorBL";
                tError.functionName = "dealData";
                tError.errorMessage = "更新批单信息错误!";
                this.mErrors.addOneError(tError);
                return false;
            }
            FeeNoticeVtsBL tFeeNoticeVtsBL = new FeeNoticeVtsBL(set.get(1).
                getOtherNo());

            TransferData mPayInfo = new TransferData();
            if("reset".equals(deal))
            {
                mPayInfo.setNameAndValue("payMode", "4");
                mPayInfo.setNameAndValue("payDate", newDate); //转帐日期
                mPayInfo.setNameAndValue("endDate", ""); //截止日期
                mPayInfo.setNameAndValue("bank", set.get(1).getBankCode());
                mPayInfo.setNameAndValue("bankAccno", set.get(1).getBankAccNo());
                tFeeNoticeVtsBL.submitData(mPayInfo);
            }
            else if("money".equals(type))
            {
                mPayInfo.setNameAndValue("payMode", "1");
                tFeeNoticeVtsBL.submitData(mPayInfo);
            }
            else
            {
                //删除通知书PayMode节点
                if(!changePayMode(set.get(1)))
                {
                    return false;
                }
            }

        }
        return true;
    }

    /**
     * deletePayMode
     * 删除缴费方式节点
     * @return MMap
     */
    private boolean changePayMode(LJAGetSchema tLJAGetSchema)
    {
        InputStream ins = null;

        LPEdorAppPrintDB tLPEdorAppPrintDB = new LPEdorAppPrintDB();
        tLPEdorAppPrintDB.setEdorAcceptNo(tLJAGetSchema.getOtherNo());
        tLPEdorAppPrintDB.getInfo();


        String appntNo = tLJAGetSchema.getAppntNo();

        String payMode =
            "自动转到超收保费余额帐户，转帐后客户"
            + appntNo + "的超收保费余额为"
            + CommonBL.bigDoubleToCommonString(
                getAccBala(appntNo) + tLJAGetSchema.getSumGetMoney(), "0.00")
            + "元。";

        TextTag textTag = new TextTag();
        textTag.add("AccFee", payMode);

        XmlUpdate xmlUpdate = new XmlUpdate(tLPEdorAppPrintDB.getEdorInfo());
        xmlUpdate.addTextTag(textTag);
        xmlUpdate.updateV2();
        ins = xmlUpdate.getInputStream();

        textTag = new TextTag();
        textTag.add("PayMode", "");
        xmlUpdate = new XmlUpdate(ins);
        xmlUpdate.addTextTag(textTag);
        xmlUpdate.deleteNode();
        ins = xmlUpdate.getInputStream();

        String xml = xmlUpdate.getXmlString();
        System.out.println(xml);

        tLPEdorAppPrintDB.setEdorInfo(ins);

        MMap tMMap = new MMap();

        String sql = "delete from LPEdorAppPrint " +
                     "where EdorAcceptNo='" + tLJAGetSchema.getOtherNo() + "' ";
        System.out.println(sql);
        tMMap.put(sql, SysConst.DELETE);
        tMMap.put(tLPEdorAppPrintDB.getSchema(), "BLOBINSERT");
        VData d = new VData ();
        d.add(tMMap);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            System.out.println(p.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "ReCreateEdorBL";
            tError.functionName = "changePayMode";
            tError.errorMessage = "更新保单信息出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * getAccBala
     * 查询投保人账户余额
     * @return String
     */
    private double getAccBala(String customerNo)
    {
        LCAppAccSchema schema = new AppAcc().getLCAppAcc(customerNo);

        if(schema == null)
        {
            return 0;
        }

        return schema.getAccBala();
    }

    private boolean checkData()
    {
        return true;
    }

}
