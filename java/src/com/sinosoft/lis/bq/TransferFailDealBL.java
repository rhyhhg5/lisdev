package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.task.TaskTraceNodeOpBL;
import com.sinosoft.task.Task;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TransferFailDealBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap map = new MMap();
    /** 数据操作字符串 */
    private String mOperate;
    /**处理中用到的数据*/
    private String deal = "";
    private String newDate = "";
    private String type = "";
    private String getPerson = "";
    private String appKind = "";
    private String no = "";
    private String actuGetNo = "";
    private String gpflag = "";
    private LJAGetSchema mLJAGetSchema = null;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public TransferFailDealBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        //得到外部传入的数据,将数据备份到本类中
        if(!getInputData())
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }
        mResult.add(map);
        PubSubmit tSubmit = new PubSubmit();
        if(!tSubmit.submitData(mResult, ""))
        { //数据提交
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "TransferFailDealBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //2006-11-15 add  添加处理状态、处理人、最后处理时间
        EdorItemSpecialData tEdorItemSpecialData =
            new EdorItemSpecialData(this.mLJAGetSchema.getOtherNo(),
                                    this.mLJAGetSchema.getOtherNo(), "QQ");
        tEdorItemSpecialData.add("DEALSTATE", "已处理");
        tEdorItemSpecialData.add("OPERATOR", mGlobalInput.Operator);
        tEdorItemSpecialData.add("LASTDATE", PubFun.getCurrentDate());
        if(!tEdorItemSpecialData.insert())
        {
            CError tError = new CError();
            tError.moduleName = "TransferFailDealBL";
            tError.functionName = "dealData";
            tError.errorMessage = "生成处理状态失败!!";
            this.mErrors.addOneError(tError);
            return false;

        }

        return true;
    }

    private boolean getInputData()
    {
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
            "GlobalInput", 0));
        TransferData temp = (TransferData) mInputData.getObjectByObjectName(
            "TransferData", 0);
        deal = (String) temp.getValueByName("deal");
        actuGetNo = (String) temp.getValueByName("actuGetNo");
        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setActuGetNo(actuGetNo);
        LJAGetSet set = tLJAGetDB.query();
        if(set == null || set.size() < 1)
        {
            CError tError = new CError();
            tError.moduleName = "TransferFailDealBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "查询LJAGet失败!!";
            this.mErrors.addOneError(tError);
            return false;

        }
        else
        {
            mLJAGetSchema = set.get(1).getSchema();
        }

        gpflag = (String) temp.getValueByName("gpflag");
        if(deal.equals("reset"))
        {
            newDate = (String) temp.getValueByName("newDate");
            if(newDate == null || newDate.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "TransferFailDealBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "接收数据失败!!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else
        {
            type = (String) temp.getValueByName("type");
            if(type.equals("money"))
            {
                getPerson = (String) temp.getValueByName("getPerson");
                appKind = (String) temp.getValueByName("appKind");
                no = (String) temp.getValueByName("no");
                if(getPerson == null || getPerson.equals("") ||
                   appKind == null || appKind.equals("") || no == null
                   || no.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "TransferFailDealBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "接收数据失败!!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        return true;
    }

    private boolean dealData()
    {
        StringBuffer sql = new StringBuffer();
        TaskTraceNodeOpBL tTaskTraceNodeOpBL = new TaskTraceNodeOpBL();
        VData tVData = new VData();
        tVData.add(this.mGlobalInput);
        LGTraceNodeOpSchema mLGTraceNodeOpSchema = new LGTraceNodeOpSchema();

        if(deal.equals("reset"))
        {
            sql.append(
                "update ljaget set(BankOnTheWayFlag,CanSendBank,ShouldDate) =('0','0','")
                .append(newDate)
                .append("') where actuGetNo = '")
                .append(actuGetNo)
                .append("'");
            map.put(sql.toString(), "UPDATE");
            mLGTraceNodeOpSchema.setWorkNo(this.mLJAGetSchema.getOtherNo());
            mLGTraceNodeOpSchema.setOperatorType(Task.HISTORY_TYPE_BANKFAILDEAL);
            mLGTraceNodeOpSchema.setRemark("处理转帐失败->修改转帐时间，重新转帐");
            tVData.add(mLGTraceNodeOpSchema);
            mLGTraceNodeOpSchema = tTaskTraceNodeOpBL.getSchema(tVData, "");
        }
        else
        {
            if(type.equals("money"))
            {
                sql.append(
                    "update Ljaget set(BankOnTheWayFlag, PayMode,Drawer,DrawerID) =('0', '1','")
                    .append(getPerson)
                    .append("','")
                    .append(no)
                    .append("') where actuGetNo = '")
                    .append(actuGetNo)
                    .append("'")
                    ;
                map.put(sql.toString(), "UPDATE");
                mLGTraceNodeOpSchema.setWorkNo(this.mLJAGetSchema.getOtherNo());
                mLGTraceNodeOpSchema.setOperatorType(Task.
                    HISTORY_TYPE_BANKFAILDEAL);
                mLGTraceNodeOpSchema.setRemark("处理转帐失败->修改为现金领取");
                tVData.add(mLGTraceNodeOpSchema);
                mLGTraceNodeOpSchema = tTaskTraceNodeOpBL.getSchema(tVData, "");

            }
            else
            {
                LJAGetDB tLJAGetDB = new LJAGetDB();
                tLJAGetDB.setActuGetNo(actuGetNo);
                LJAGetSet set = tLJAGetDB.query();
                if(set == null || set.size() == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TransferFailDealBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "LJAGet查询失败!!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                //生成帐户数据
                if(!dealAccBala(set))
                {
                    return false;
                }

                //生成余额转入类型财务数据
                LJAGetEndorseDB db = new LJAGetEndorseDB();
                db.setActuGetNo(actuGetNo);
                LJAGetEndorseSet tLJAGetEndorseSet = db.query();
                if(tLJAGetEndorseSet.size() != 0)
                {
                    //删除LJAGet 数据
                    sql = new StringBuffer();
                    sql.append("delete from ljaget where actugetno ='")
                        .append(actuGetNo)
                        .append("'");
                    map.put(sql.toString(), "DELETE");

                    LJAGetEndorseSchema schema = tLJAGetEndorseSet.get(1);
                    schema.setFeeFinaType("YEI");
                    schema.setGetMoney(set.get(1).getSumGetMoney());
                    schema.setFeeOperationType("YEI");
                    schema.setGrpContNo(BQ.FILLDATA);
                    schema.setContNo(BQ.FILLDATA);
                    schema.setGrpPolNo(BQ.FILLDATA);
                    schema.setPolNo(BQ.FILLDATA);
                    schema.setRiskCode(BQ.FILLDATA);
                    schema.setPayPlanCode(BQ.FILLDATA);
                    schema.setMakeDate(PubFun.getCurrentDate());
                    schema.setMakeTime(PubFun.getCurrentTime());
                    PubFun.fillDefaultField(schema);

                    map.put(schema, SysConst.DELETE_AND_INSERT);
                }

                mLGTraceNodeOpSchema.setWorkNo(this.mLJAGetSchema.getOtherNo());
                mLGTraceNodeOpSchema.setOperatorType(Task.
                    HISTORY_TYPE_BANKFAILDEAL);
                mLGTraceNodeOpSchema.setRemark("处理转帐失败->修改为转入投保人帐户");
                tVData.add(mLGTraceNodeOpSchema);
                mLGTraceNodeOpSchema = tTaskTraceNodeOpBL.getSchema(tVData, "");
            }
        }

        map.put(mLGTraceNodeOpSchema, SysConst.INSERT);


        //重新生成批单信息
        ReCreateEdorBL temp = new ReCreateEdorBL();
        if(!temp.submitData(this.mInputData, ""))
        {
            CError tError = new CError();
            tError.moduleName = "TransferFailDealBL";
            tError.functionName = "dealData";
            tError.errorMessage = "重新生成批单信息失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealAccBala(LJAGetSet s)
    {
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();

        tLCAppAccTraceSchema.setCustomerNo(s.get(1).getAppntNo());
        tLCAppAccTraceSchema.setOtherType(s.get(1).getOtherNoType());
        tLCAppAccTraceSchema.setOtherNo(s.get(1).getOtherNo());
        tLCAppAccTraceSchema.setMoney(s.get(1).getSumGetMoney());
        tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);

        AppAcc tAppAcc = new AppAcc();
        MMap tMMap = tAppAcc.accShiftToFW(tLCAppAccTraceSchema);
        //这里得到的是引用
        if(tMMap == null)
        {
            CError tError = new CError();
            tError.moduleName = "TransferFailDealBL";
            tError.functionName = "dealData";
            tError.errorMessage = "投保人帐户查询失败!!";
            this.mErrors.addOneError(tError);
            return false;

        }

        tLCAppAccTraceSchema = (LCAppAccTraceSchema) tMMap
                               .getObjectByObjectName("LCAppAccTraceSchema", 0);
        tLCAppAccTraceSchema.setState("1");
        tLCAppAccTraceSchema.setBakNo(s.get(1).getActuGetNo());
        tLCAppAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
        tLCAppAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());
        map.put(tLCAppAccTraceSchema, "INSERT");

        LCAppAccSchema tLCAppAccSchema
            = tAppAcc.getLCAppAcc(tLCAppAccTraceSchema.getCustomerNo());
        tLCAppAccSchema.setAccBala(s.get(1).getSumGetMoney()
                                   + tLCAppAccSchema.getAccBala());
        tLCAppAccSchema.setAccGetMoney(tLCAppAccSchema.getAccGetMoney()
            + tLCAppAccTraceSchema.getMoney());
        tLCAppAccSchema.setOperator(mGlobalInput.Operator);
        tLCAppAccSchema.setModifyDate(PubFun.getCurrentDate());
        tLCAppAccSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCAppAccSchema, "UPDATE");

        return true;
    }

    public static void main(String arg[])
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "qulq";
        TransferData temp = new TransferData();
        temp.setNameAndValue("deal", "modify");
        temp.setNameAndValue("type", "acc");
        temp.setNameAndValue("gpflag", "1");
//    temp.setNameAndValue("newDate","2007-3-1");
        temp.setNameAndValue("actuGetNo", "37000000457");
    temp.setNameAndValue("getPerson","156432");
    temp.setNameAndValue("appKind","1");
    temp.setNameAndValue("no","140202198211010534");

        VData tVData = new VData();
        tVData.add(temp);
        tVData.add(tG);
        TransferFailDealBL tTransferFailDealBL = new TransferFailDealBL();
        if(!tTransferFailDealBL.submitData(tVData, ""))
        {
            System.out.println(tTransferFailDealBL.mErrors.getErrContent());
        }

    }

}
