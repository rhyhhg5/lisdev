package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 * 新万能产品续期自动缓交功能
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author lzy
 * @version 1.0
 */
public class ULIDeferDealBL 
{
    private GlobalInput mG = new GlobalInput();
    private String mManageCom = "";
    private MMap mMap = null;
    private VData mResult = new VData();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mOperator = "Server";
    private LLCaseCommon mLLCaseCommon = new LLCaseCommon();
    private ExeSQL mExeSQL = new ExeSQL();
    private SSRS mSSRS = new SSRS();
    public CErrors mErrors = new CErrors();
    private int mGracePeriod = 60;	//交至日后60天的缴费期
    private String mContNo=null;
    
    public ULIDeferDealBL() {}

    private boolean getInputData(VData cInputData) 
    {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mManageCom = mG.ManageCom;
        TransferData tTransferData=(TransferData)cInputData.getObjectByObjectName("TransferData",0);
        if(null!=tTransferData){
        	mContNo=(String)tTransferData.getValueByName("ContNo");
        }
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {
    	if(!getInputData(cInputData)){
    		return false;
    	}
    	if(!dealData()){
    		return false;
    	}
        return true;
    }

    private boolean dealData(){
    	String sql="select ContNo,PolNo from LCPol a where ContType='1' and Appflag='1' "
    				+ " and (standbyflag1 is null or standbyflag1<>'"+BQ.ULI_DEFER+"') "
    				+ " and PaytoDate < payEndDate and PayIntv>0 and StateFlag='1' "
    				+ " and PaytoDate + 60 days < current date "
    				+ " and exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4')"
    				+ " and exists (select 1 from ldcode where codetype='ULIDEFER' and code=a.riskcode)"
    				+ " and not exists (select 1 from LCContState where polno=a.polno and statetype='"+BQ.STATETYPE_ULIDEFER+"' "
    				+ "  	and (EndDate is null or state='1'))"
    				+ " and a.ManageCom like '" + mManageCom + "%' "
    				+ (mContNo != null ? (" and a.ContNo = '" + mContNo + "' ") : "")
    				+ " with ur";
    	System.out.println(sql.toString());
    	mSSRS = mExeSQL.execSQL(sql.toString());
    	System.out.println("续期缓交：拢共需要处理"+mSSRS.MaxRow+"条");
    	if(mSSRS.MaxRow == 0)
    	{
    		System.out.println("没有需要自动续期缓交的万能保单。");
    		return false;
    	}
    	for(int i=1;i<=mSSRS.MaxRow;i++)
    	{
    		mMap=new MMap();
    		String tContNo = mSSRS.GetText(i, 1);
    		String tPolNo=mSSRS.GetText(i, 2);
    		try {
    			//保全校验
        		if(edorseState(tContNo)){
        			System.out.println("保单"+tContNo+"下有正在处理的保全，暂不处理");
        			continue;
        		}
        		//理赔校验
        		if (mLLCaseCommon.getClaimState(tPolNo).equals("1") ||
                        mLLCaseCommon.getClaimState(tPolNo).equals("2")) 
                {
                        System.out.println(tPolNo + "在做理赔项目");
                        continue;
                }
        		//校验是否过了缴费期
        		if(!checkGracePeriod(tPolNo)){
        			continue;
        		}
        		//校验应收
        		if(!checkLJSPay(tContNo)){
        			continue;
        		}
        		//处理险种缓交
        		if(!updatePolState(tContNo,tPolNo)){
        			continue;
        		}
        		//撤销应收
        		if(!cancelLJSPay(tContNo)){
        			continue;
        		}
	    		try {
	    			if(!submitDate(mMap)){
	        			continue;
	        		}
				} catch (Exception e) {
					System.out.println("保单"+tContNo+"，处理失败："+e.getMessage());
				}
    		} catch (Exception e) {
				System.out.println("续期自动缓交出错："+e.getMessage());
			}
    	}
    	return true;
    }

    /**
     * 应收记录校验
     * @param aContNo
     * @return
     */
    private boolean checkLJSPay(String aContNo)
    {
    	ExeSQL tExeSQL = new ExeSQL();
		LJSPayDB tLJSPayDB = new LJSPayDB();
		tLJSPayDB.setOtherNo(aContNo);
		tLJSPayDB.setOtherNoType("2");
		LJSPaySet tLJSPaySet = new LJSPaySet();
		tLJSPaySet = tLJSPayDB.query();
		if(tLJSPaySet.size()==0)
		{
			System.out.println("保单"+aContNo+"无应收");
			return true;
		}
		else
		{
			for(int j=1;j<=tLJSPaySet.size();j++)
			{
				LJSPaySchema tLJSPaySchema = tLJSPaySet.get(j);
				String tBankOnTheWay = tLJSPaySchema.getBankOnTheWayFlag();
				
				String sql1 = "select count(1) from ljtempfee where tempfeeno = '" + tLJSPaySchema.getGetNoticeNo() + "' with ur";
				String tCount1 = tExeSQL.getOneValue(sql1);
				if(!"0".equals(tCount1))
				{
					System.out.println("保单"+aContNo+"，应收记录：" + tLJSPaySchema.getGetNoticeNo() + "已经收费");
					return false;
				}
				if(tLJSPaySchema.getSumDuePayMoney()==0){
					System.out.println("保单应收为0");
					return false;
				}
				
				if("1".equals(tBankOnTheWay)|| "2".equals(tBankOnTheWay) )
				{
					System.out.println("保单"+aContNo+"已经发盘或生成电话催缴工单");
					return false;
				}
				FDate tFDate = new FDate();
				if(tFDate.getDate(tLJSPaySchema.getPayDate()).after(tFDate.getDate(mCurrentDate)))
	            {
	                System.out.println(aContNo + "续期未到缴费截止期");
	                return false;
	            }
				
			}
			String sql = "select count(1) from LYSendToBank where PolNo = '" + aContNo + "' with ur ";
			String tCount = tExeSQL.getOneValue(sql);
			if(!"0".equals(tCount))
			{
				System.out.println("保单"+aContNo+"已经发盘成功");
				return false;
			}
			return true;
		}
    }
    
    /**
     * 保全状校验
     * @param aContNo
     * @return
     */
    private boolean edorseState(String aContNo) {

        StringBuffer sql = new StringBuffer();
        sql.append("select a.edorAcceptNo ")
                .append("from LPEdorApp a,LPEdorItem b ")
                .append("where a.edorAcceptNo = b.edorAcceptNo ")
                .append("    and b.ContNo = '")
                .append(aContNo).append("' ")
                .append("    and a.edorState != '")
                .append(BQ.EDORSTATE_CONFIRM).append("' ")
                .append("   and not exists(select 1 from LGwork ")
                .append("           where WorkNo = a.EdorAcceptNo and StatusNo = '8') "); //撤销
        System.out.println(sql.toString());
        ExeSQL e = new ExeSQL();
        SSRS tSSRS = e.execSQL(sql.toString());
        if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            return true;
        }

        return false;
    }
    
    /**
     * 校验保单是否超过60天的缴费期限
     * @return
     */
    private boolean checkGracePeriod(String aPolNo){
    	String strPol="select * from LCPol where PolNo='"+aPolNo+"' ";
		LCPolDB tLCPolDB=new LCPolDB();
		LCPolSchema tLCPolSchema=new LCPolSchema();
		LCPolSet tLCPolSet=tLCPolDB.executeQuery(strPol);
		if(tLCPolSet.size()>0){
			tLCPolSchema=tLCPolSet.get(1);
		}else{
			System.out.println(aPolNo+"险种信息查询失败！");
			return false;
		}
		FDate tFDate = new FDate();
		String GraceDate=PubFun.calDate(tLCPolSchema.getPaytoDate(), mGracePeriod+1 , "D", "");
    	if(tFDate.getDate(GraceDate).after(tFDate.getDate(mCurrentDate))){
    		System.out.println(aPolNo+"险种未过60天的缴费期");
    		return false;
    	}
    	return true;
    }
    
    //作废应收
    private boolean cancelLJSPay(String aContNo)
    {
    	String sql = "select getnoticeno from ljspay where otherno = '" + aContNo + "' "
    	           + "and (bankonthewayflag = '0' or bankonthewayflag is null or bankonthewayflag = '') with ur";
    	SSRS tSSRS = mExeSQL.execSQL(sql);
    	for(int i=1;i<=tSSRS.MaxRow;i++)
    	{
    		String aGetNoticeNo = tSSRS.GetText(i, 1);
    		String sql1 = "delete from ljspay where getnoticeno = '" + aGetNoticeNo + "' ";
        	String sql2 = "delete from ljspayperson where getnoticeno = '" + aGetNoticeNo + "' ";
        	String sql3 = "update ljspayb set dealstate = '2',cancelreason='1',operator = '" + mOperator 
        	            + "', modifydate = '" + mCurrentDate + "', modifytime = '" + mCurrentTime 
        	            + "' where getnoticeno = '" + aGetNoticeNo + "' ";
        	String sql4 = "update ljspaypersonb set dealstate = '2',cancelreason='1',operator = '" + mOperator 
                        + "', modifydate = '" + mCurrentDate + "', modifytime = '" + mCurrentTime 
                        + "' where getnoticeno = '" + aGetNoticeNo + "' ";
        	mMap.put(sql1, SysConst.DELETE);
        	mMap.put(sql2, SysConst.DELETE);
        	mMap.put(sql3, SysConst.UPDATE);
        	mMap.put(sql4, SysConst.UPDATE);
    	}
    	return true;
    }
    
    /**
     * 置缓交状态，且记录缓交起始日期
     * @param aContNo
     * @param aPolNo
     * @return
     */
    private boolean updatePolState(String aContNo,String aPolNo)
    {
    	String startDate="";
		String strPol="select * from LCPol where ContNo='"+aContNo+"' and PolNo='"+aPolNo+"' ";
		LCPolDB tLCPolDB=new LCPolDB();
		LCPolSchema tLCPolSchema=new LCPolSchema();
		LCPolSet tLCPolSet=tLCPolDB.executeQuery(strPol);
		if(tLCPolSet.size()>0){
			tLCPolSchema=tLCPolSet.get(1);
		}else{
			System.out.println(aPolNo+"险种信息查询失败！");
			return false;
		}
		int polYear=PubFun.calInterval(tLCPolSchema.getCValiDate(), 
								tLCPolSchema.getPaytoDate(), "Y");//当前保单年度
		//保单在2-5保单年度内新缓交-账户分缓交区间结算，5保单年度以后老缓交，账户正常结算
		String polState=BQ.ULI_DEFER;
		if(polYear>=5){
			polState=BQ.ULI_HUAN;
		}
		StringBuffer updateSql = new StringBuffer();
		updateSql.append("update lcpol a set standbyflag1 = '").append(polState)
				 .append("', modifydate = '")
				 .append(mCurrentDate).append("', modifytime = '").append(mCurrentTime)
		         .append("' where contno = '").append(aContNo)
				 .append("'");
//		if(BQ.ULI_DEFER.equals(polState)){
//			updateSql.append("' and polno='").append(aPolNo).append("'");
//		}
		         
		mMap.put(updateSql.toString(), SysConst.UPDATE);
		if(polYear>=5){
			return true;
		}
		startDate=PubFun.calDate(tLCPolSchema.getPaytoDate(), mGracePeriod + 1 , "D", "");//于交至日的第61日为缓交的起始日期
		LCContStateSchema tLCContStateSchema=new LCContStateSchema();
		tLCContStateSchema.setGrpContNo("000000");
		tLCContStateSchema.setContNo(aContNo);
		tLCContStateSchema.setPolNo(tLCPolSchema.getPolNo());
		tLCContStateSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
        tLCContStateSchema.setStateType(BQ.STATETYPE_ULIDEFER);
//        tLCContStateSchema.setOtherNoType("");
//        tLCContStateSchema.setOtherNo("");
        tLCContStateSchema.setState("1");
        tLCContStateSchema.setStateReason("02");
        tLCContStateSchema.setStartDate(startDate);
//        tLCContStateSchema.setEndDate("");
//        tLCContStateSchema.setRemark("");
        tLCContStateSchema.setOperator(mOperator);
        tLCContStateSchema.setMakeDate(mCurrentDate);
        tLCContStateSchema.setMakeTime(mCurrentTime);
        tLCContStateSchema.setModifyDate(mCurrentDate);
        tLCContStateSchema.setModifyTime(mCurrentTime);
        mMap.put(tLCContStateSchema,SysConst.DELETE_AND_INSERT);
		return true;
    }
    
    private boolean submitDate(MMap map)
    {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult.clear();
        mResult.add(mMap);
        if(!tPubSubmit.submitData(mResult, ""))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("提交数据失败");
        }
        this.mMap=new MMap();
        return true;
    }
    
    public static void main(String[] args) 
    {
    	GlobalInput mG = new GlobalInput();
        VData mVData = new VData();
        mG.Operator = "001";
        mG.ManageCom = "86";
        TransferData tTransferData=new TransferData();
        tTransferData.setNameAndValue("ContNo", "014059291000001");
        mVData.add(mG);
        mVData.add(tTransferData);
        ULIDeferDealBL mUliDeferDealBL = new ULIDeferDealBL();
        mUliDeferDealBL.submitData(mVData,"");
    	
    }
}
