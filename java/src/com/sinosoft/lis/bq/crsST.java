package com.sinosoft.lis.bq;

import utils.system;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.tb.NIDiskImport;
import com.sinosoft.lis.tb.GrpDiskImport;
import com.sinosoft.lis.tb.crsDiskImport;

import examples.newsgroups;

/**
 * <p>Title: 增加被保人磁盘导入类</p>
 * <p>Description: 把从磁盘导入添加到数据库 </p>
 * <p>增加功能，处理从磁盘导入的多Sheet文档</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @rewrite by LiWei
 * @version 1.0
 */

public class crsST {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保全号 */
    private String mEdorNo = null;
    private String mPrtNo="18170227666";
    private String mPROPOSALCONTNO="18170227666";
    

    /** 批次号 */
    private String mBatchNo = null;
    
    private String mCrs_BussType=null;
    
    private String mGrpagentcom=null;
    
    private String mGrpagentcode=null;
    
    private String mGrpagentname=null;
    
    private String mGrpagentidno=null;
    /** 团体合同号信息 */
    private LCGrpContSchema mLCGrpContSchema = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    /** 节点名 */
    private String[] sheetName = {"InsuredInfo","Ver"};
    /** 配置文件名 */
    private String configName = "STImport.xml";
    private int i=0;
    private int j=0;
    private int imax=0;
	private int imin=0;
	private int g=0;
   
    public crsST(String GrpContNo, GlobalInput gi) {
        this.mGlobalInput = gi;
        this.mGrpContNo = GrpContNo;
    }

    /**
     * 添加传入的多个Sheet数据
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName) {
        //处理批次号：批次号码为文件名（不含扩展名）
        

        //从磁盘导入数据
        crsDiskImport importFile = new crsDiskImport(path + fileName,
                path + configName,
                sheetName);
        System.out.println(sheetName);
        System.out.println(path + configName);
        if (!importFile.doImport()) {
            this.mErrors.copyAllErrors(importFile.mErrors);
            return false;
        }

        CrsInfoListSet tCrsInfoListSet = importFile.getSchemaSet();//需要修改
       
        this.mResult.add(tCrsInfoListSet);
       

        //存放Insert Into语句的容器
        
        System.out.println(tCrsInfoListSet.size());
        for ( i = 1; ; i++) {
        	MMap map = new MMap();
            //添加一个被保人
        	
        	if(i*200>tCrsInfoListSet.size()){
        		imax = tCrsInfoListSet.size();
        	}else{
        		imax=i*200;
        	}
        	if((i-1)*200>tCrsInfoListSet.size()){
        		imin = tCrsInfoListSet.size();
        	}else{
        		imin=(i-1)*200+1;
        	}
        	for(j=imin;j<=imax;j++){
        		addOneInsured(map, tCrsInfoListSet.get(j), j);
        		g=g+1;
        	}
            
            //提交数据到数据库
            if (!submitData(map)) {
                return false;
            }
            g=0;
            if(imax== tCrsInfoListSet.size()){
            	break;
            }
            
        }
        

       
        return true;
    }    

   
   

   
    /**
     * 添加一个被保人
     * @param map MMap
     * @param aLCInsuredSchema LCInsuredSchema
     */
    private void addOneInsured(MMap map,
                               CrsInfoListSchema cLCInsuredListSchema, int i) {
    	CrsInfoListSchema tCrsInfoListSchema = cLCInsuredListSchema;
    	 ExeSQL eSql=new ExeSQL();
         SSRS ssrs=eSql.execSQL("select max(int(EdorNo)) from CrsInfoList where 1=1");
         String conttype="";
         int max=1;
         int maxnumber1=ssrs.getMaxNumber();
     	if(maxnumber1==0){
     		max=1;
     	}else{
     		 String num=ssrs.GetText(1, 1);
             if(num==""||num==null){
             	max=1;
             }else{
             	max=Integer.parseInt(num)+1;
             }
     	}
         
        
        
         
         System.out.println(max+g);
         mEdorNo=Integer.toString(max+g);
    	
			
        String contno=cLCInsuredListSchema.getContNo();
       
    	ssrs=eSql.execSQL("select distinct conttype from lccont where "
        		+ "contno='"+contno+"' or grpcontno='"+contno+"'");
    	int maxnumber=ssrs.getMaxNumber();
    	if(maxnumber==0){
    		tCrsInfoListSchema.setEdorNo(mEdorNo);
            tCrsInfoListSchema.setMakeDate(PubFun.getCurrentDate());
            tCrsInfoListSchema.setModifyDate(PubFun.getCurrentDate());
            tCrsInfoListSchema.setMakeTime(PubFun.getCurrentTime());
            tCrsInfoListSchema.setModifyTime( PubFun.getCurrentTime());
            tCrsInfoListSchema.setFalseReason("请核实合同号");
    		tCrsInfoListSchema.setStateFlag("4");
    		map.put(tCrsInfoListSchema, "INSERT");
    		return;
    	}else{
    		conttype=ssrs.GetText(1, 1);
        	
    	}
    	
    	tCrsInfoListSchema.setContType(conttype);
    	tCrsInfoListSchema.setEdorNo(mEdorNo);
        tCrsInfoListSchema.setMakeDate(PubFun.getCurrentDate());
        tCrsInfoListSchema.setModifyDate(PubFun.getCurrentDate());
        tCrsInfoListSchema.setMakeTime(PubFun.getCurrentTime());
        tCrsInfoListSchema.setModifyTime( PubFun.getCurrentTime());
        String state=tCrsInfoListSchema.getState();
        System.out.println(state);
        if("1".equals(state)||"2".equals(state)){
        	
        
        
        try {
    	String agentgroup=tCrsInfoListSchema.getAgentGroup();
    	System.out.println(agentgroup);
    	if((agentgroup==null||"".equals(agentgroup))&&(!"".equals(tCrsInfoListSchema.getAgentCode()))){
    		ssrs=eSql.execSQL("select agentgroup from laagent where groupagentcode='"
    				+tCrsInfoListSchema.getAgentCode()+"'" );
    		String magentgroup=ssrs.GetText(1, 1);
    		System.out.println(magentgroup);
    		tCrsInfoListSchema.setAgentGroup(magentgroup);    		
    	}
    	if("".equals(tCrsInfoListSchema.getAgentCode())||tCrsInfoListSchema.getAgentCode()==null){
    		
    	}else{
    		ssrs=eSql.execSQL("select agentcode from laagent where groupagentcode='"
    				+tCrsInfoListSchema.getAgentCode()+"'" );
    		String magentcode=ssrs.GetText(1, 1);
    		System.out.println(magentcode);
    		tCrsInfoListSchema.setAgentCode(magentcode); 
    		
    	}
	
        
        
        
        tCrsInfoListSchema.setStateFlag("0");
        } catch (Exception e) {
        	tCrsInfoListSchema.setStateFlag("4");        
            tCrsInfoListSchema.setFalseReason("业务员代码没有查询到对应组别");
            tCrsInfoListSchema.setOther("0");
		}
        }else{
        	tCrsInfoListSchema.setStateFlag("4");
        	tCrsInfoListSchema.setFalseReason("状态类型不正确");
        }
    	
    	
        
        
        map.put(tCrsInfoListSchema, "INSERT");
       
        		
    }   
 
    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }
    
    

    
}

