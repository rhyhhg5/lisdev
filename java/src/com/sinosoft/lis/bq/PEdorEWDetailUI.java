package com.sinosoft.lis.bq;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:被保险人资料变更功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PEdorEWDetailUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public PEdorEWDetailUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    PEdorEWDetailBL tPEdorEWDetailBL = new PEdorEWDetailBL();
    System.out.println("---BB UI BEGIN---"+mOperate);
    if (tPEdorEWDetailBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tPEdorEWDetailBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "PEdorEWDetailUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
      mResult = tPEdorEWDetailBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {

      LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
      PEdorEWDetailUI tPEdorEWDetailUI = new PEdorEWDetailUI();

      LPAddressSchema tLPAddressSchema = new LPAddressSchema();
      LPContSchema tLPContSchema = new LPContSchema();
      LPContSet tLPContSet = new LPContSet();

      //后面要执行的动作：添加，修改

      GlobalInput tG = new GlobalInput();
      tG.ManageCom = "86";
      tG.Operator = "endor0";
      tLPEdorItemDB.setEdorAcceptNo("20050912000008");
      tLPEdorItemDB.setEdorNo("20050912000008");
      tLPEdorItemDB.setContNo("00000005801");

      tLPEdorItemDB.setInsuredNo("000000");
      tLPEdorItemDB.setPolNo("000000");
      tLPEdorItemDB.setEdorType("EW");
      tLPEdorItemDB.getInfo();



      //个单合同
      tLPContSchema.setEdorNo("20050912000008");
      tLPContSchema.setContNo("00000005801");
      tLPContSet.add(tLPContSchema);

      // 准备传输数据 VData
      VData tVData = new VData();
      //保存个人保单信息(保全)
      tVData.addElement(tG);
      tVData.addElement(tLPEdorItemDB.getSchema());

      tVData.addElement(tLPAddressSchema);
      tVData.addElement(tLPContSet);

      if(!tPEdorEWDetailUI.submitData(tVData, "INSERT|Main"))
      {
          System.out.println(tPEdorEWDetailUI.mErrors.getErrContent());
      }
      else
      {
          System.out.println("OK");
      }
  }
}
