package com.sinosoft.lis.bq;

import java.io.InputStream;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 被保人清单产生类</p>
 * <p>Description:产生被保人清单 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author qiuyang
 * @version 1.0
 */

public class PrtGrpInsuredListJYBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private String mGrpContNo = null;

    private String mGrpName = null;

    private String mEdorNo = null;

    private String mEdorValiDate = null;

    private GlobalInput mGlobalInput = null;
    
    private XmlExport xml = new XmlExport();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListJYBL(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }
    
    /**
     * 执行操作
     * @return boolean
     */
    public boolean submitData()
    {
       MMap map = getSubmitData();
       if (map == null)
       {
           return false;
       }

       if (!submit(map))
       {
           return false;
       }
       return true;
    }

    /**
     * 得到要提交的数据，不执行submit操作
     * @return MMap
     */
    public MMap getSubmitData()
    {
        if (!prepareData())
        {
            return null;
        }
        xml = createXML();
        return insertXML(xml);
    }
    
    /**
     * 提交业务数据
     * @return boolean
     */
    public boolean getXML()
    {
        if (!prepareData())
        {
            return false;
        }
        xml = createXML();
        return true;
    }

    /**
     * 为生成XML准备数据
     * @return boolean
     */
    private boolean prepareData()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mEdorNo);
        tLPGrpEdorItemDB.setEdorType("JY"); //万能解约
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            mErrors.addOneError("未找到保全部分领取项目信息！");
            return false;
        }
        //得到团体合同号
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
        this.mGrpContNo = tLPGrpEdorItemSchema.getGrpContNo();
        this.mEdorValiDate = tLPGrpEdorItemSchema.getEdorValiDate();

        //得到团体客户名称
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("未查到团体保单信息！");
            return false;
        }
        this.mGrpName = tLCGrpContDB.getGrpName();
        return true;
    }

    /**
     * 根据清单数据生成XML
     * @param xmlExport XmlExport
     */
    private XmlExport createXML()
    {
    	xml = new XmlExport();
        xml.createDocument("PrtGrpInsuredListJY.vts", "printer");

        //设置批单中TextTag要显示的信息
        TextTag tag = new TextTag();
        tag.add("GrpName", mGrpName);
        tag.add("EdorNo", mEdorNo);
        xml.addTextTag(tag);

        //设置被保人清单
        ListTable listTable = new ListTable();
        listTable.setName("JY");
        LPDiskImportSet tLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, "JY",mGrpContNo, BQ.IMPORTSTATE_SUCC);
        for (int i = 1; i <= tLPDiskImportSet.size(); i ++)
        {
        	LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(i);
            String[] column = new String[14];
/*		清单详细字段：序号，被保险人姓名，证件号码，客户号码，离职领取日期，
 *		被保险人离职时结算后总金额，个人账户单位交费归属比例，转入公共帐户金额，转入个人账户金额，离职领取费用，被保人实际领取金额。
 *		归属比例 money2
 *
 */
            /*	将各项金额数据存到导入表中,对应字段如下:
             *	归属比例 money2
             *	个人账户单位部分公共账户归属 appntprem
             *	个人账户单位部分个人账户归属 PersonOwnPrem
             *	个人账户个人部分 personprem
             *	退保费用(退保手续费) money
             *	保单管理费 getmoney
             *	本保单退费 insuaccbala
             *
             *  每进来一个人 就插入到lpdiskimport表中1条数据，主键共有4个，edorno eodrtype grpcontno serialno 
             *  就用serialno来区分不同的人 ，serialno记录的是个人的contno
             *  该表中，记录为 团单进行解约之后，每个人的钱的走向的信息，
             *  这样可以方便的在清单中和批单中获得并展现。
             */
            String insuredName="";
            String idNo="";
            String insuredNo="";
            String bankAccNo="";
            String accName="";
            SSRS edorSSRS2 = new ExeSQL().execSQL("select insuredname,insuredidno,insuredno,bankaccno,accname from lccont where grpcontno='"+tLPDiskImportSchema.getGrpContNo()+"' and contno='"+tLPDiskImportSchema.getSerialNo()+"'");
            
            column[0] = StrTool.cTrim(String.valueOf(i));
            column[1] = StrTool.cTrim(edorSSRS2.GetText(1, 1));
            column[2] = StrTool.cTrim(edorSSRS2.GetText(1, 2));
            column[3] = StrTool.cTrim(edorSSRS2.GetText(1, 3));
            column[12]= StrTool.cTrim(edorSSRS2.GetText(1, 4));
            column[13]= StrTool.cTrim(edorSSRS2.GetText(1, 5));
            
            
            column[4] = StrTool.cTrim(PubFun.format((CommonBL.carry(tLPDiskImportSchema.getPersonPrem()))+(CommonBL.carry(tLPDiskImportSchema.getPersonOwnPrem()))+CommonBL.carry(tLPDiskImportSchema.getAppntPrem())));
            column[5] = StrTool.cTrim(Double.toString(CommonBL.carry(tLPDiskImportSchema.getPersonPrem())));
            column[6] = StrTool.cTrim(PubFun.format((CommonBL.carry(tLPDiskImportSchema.getPersonOwnPrem()))+CommonBL.carry(tLPDiskImportSchema.getAppntPrem())));
            column[7] = StrTool.cTrim(tLPDiskImportSchema.getMoney2());
            column[8] = StrTool.cTrim(Double.toString(CommonBL.carry(tLPDiskImportSchema.getPersonOwnPrem())));
            column[9]= StrTool.cTrim(Double.toString(CommonBL.carry(tLPDiskImportSchema.getAppntPrem())));
            column[10]= StrTool.cTrim(Double.toString(CommonBL.carry(tLPDiskImportSchema.getMoney())));
            column[11]= StrTool.cTrim(Double.toString(CommonBL.carry(tLPDiskImportSchema.getInsuAccBala())));

            listTable.add(column);
        }
        xml.addListTable(listTable, new String[14]);
        xml.outputDocumentToFile("c:\\", "InsuredList");
        return xml;
    }

    /**
     * 把产生的BolbXML插入数据库
     * @param xmlExport XmlExport
     */
    private MMap insertXML(XmlExport xml)
    {
        LPEdorPrint2Schema tLPEdorPrint2Schema = new LPEdorPrint2Schema();
        tLPEdorPrint2Schema.setEdorNo(mEdorNo);
        tLPEdorPrint2Schema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorPrint2Schema.setPrtFlag("N");
        tLPEdorPrint2Schema.setPrtTimes(0);
        tLPEdorPrint2Schema.setEdorInfo(xml.getInputStream());
        tLPEdorPrint2Schema.setOperator(mGlobalInput.Operator);
        tLPEdorPrint2Schema.setMakeDate(PubFun.getCurrentDate());
        tLPEdorPrint2Schema.setMakeTime(PubFun.getCurrentTime());
        tLPEdorPrint2Schema.setModifyDate(PubFun.getCurrentDate());
        tLPEdorPrint2Schema.setModifyTime(PubFun.getCurrentTime());

        MMap map = new MMap();
        map.put(tLPEdorPrint2Schema, "BLOBINSERT");
        return map;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit(MMap map)
    {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    
    
    /**
     * 得到该人的银行账户
     */
    private String  getBankAccNo(LPDiskImportSchema tLPDiskImportSchema){
    	String sqlString = "select bankaccno from lccont where grpcontno='"+tLPDiskImportSchema.getGrpContNo()+"' and insuredno in(select insuredno from lpdiskimport where edorno='"+tLPDiskImportSchema.getEdorNo()+"' and edortype='TQ')";
    	ExeSQL exeSQL = new ExeSQL();
    	String str = exeSQL.getOneValue(sqlString);
    	return str;
    }
    /**
     * 得到该人的开户名
     */
    private String  getAccName(LPDiskImportSchema tLPDiskImportSchema){
    	String sqlString = "select accname from lccont where grpcontno='"+tLPDiskImportSchema.getGrpContNo()+"' and insuredno in(select insuredno from lpdiskimport where edorno='"+tLPDiskImportSchema.getEdorNo()+"' and edortype='TQ')";
    	ExeSQL exeSQL = new ExeSQL();
    	String str = exeSQL.getOneValue(sqlString);
    	return str;
    }
    /**
     * 得到用户可以拿到手里的钱
     */
    private double  getReturnMoney(LPDiskImportSchema tLPDiskImportSchema){
    	String sqlString ="select getMoney from LPEdorApp where edoracceptno='" +tLPDiskImportSchema.getEdorNo() + "'";
    	ExeSQL exeSQL = new ExeSQL();
    	String str = exeSQL.getOneValue(sqlString);
    	double tmoney = Double.parseDouble(str);
    	return -tmoney;
    }
    
    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        /*GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "8644";
        tGI.Operator = "pa4403";
        String edorNo = "20090927000461";
        PrtGrpInsuredListTQBL tPrtGrpInsuredList = new PrtGrpInsuredListTQBL(tGI, edorNo);
        tPrtGrpInsuredList.submitData();*/
    	PrtGrpInsuredListJYBL prtGrpInsuredListTQBL = new PrtGrpInsuredListJYBL();
    	LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
    	tLPDiskImportSchema.setEdorNo("20110520000074");
    	tLPDiskImportSchema.setGrpContNo("00003554000011");
    	System.out.println(prtGrpInsuredListTQBL.getReturnMoney(tLPDiskImportSchema));
    }

	public PrtGrpInsuredListJYBL() {
		super();
	}
    
}
