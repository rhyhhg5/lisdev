package com.sinosoft.lis.bq;

import java.sql.*;
import java.io.*;
import com.sinosoft.utility.DBConnPool;

public class UpdateBlobBL
{
    private String edorNo = null;

    private String fileName = null;

    private String tableName = null;

    private String fieldName = null;

    public UpdateBlobBL(String fileName, String tableName, String fieldName, String edorNo)
    {
        this.fileName = fileName;
        this.tableName = tableName;
        this.edorNo = edorNo;
        this.fieldName = fieldName;
    }

    public boolean  exportBlob()
    {
        String sql = "select EdorInfo from " + tableName +
                " where " + fieldName + " = '" + edorNo + "'";
        Connection conn = DBConnPool.getConnection();
        boolean a = true;
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next())
            {
                Blob tBlob = rs.getBlob("EdorInfo");
                InputStream ins = tBlob.getBinaryStream();
                byte[] data = new byte[ins.available()];
                ins.read(data);
                OutputStream out = new FileOutputStream(fileName);
                out.write(data);
                out.close();
                a = true;
            }else{
            	a=false;
            }
            rs.close();
            stmt.close();
            conn.close();

        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        return a;
    }

    public void importBlob()
    {
        Connection conn = DBConnPool.getConnection();
        try
        {
            InputStream in = new FileInputStream(fileName);
            String sql = "update " + tableName + " set EdorInfo = ? " +
                    "where " + fieldName + " = '" + edorNo + "'";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setBinaryStream(1, in, in.available());
            ps.execute();
            ps.close();
            conn.close();
            in.close();
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private static void doExport()
    {

        String tableName, fieldName, edorNo;

//        tableName = "LPEdorAppPrint";//个单
//        fieldName = "EdorAcceptNo";//个单
        edorNo = "20070605000158";
        String fileName = "c:/bak/"+edorNo+"export.xml";
       tableName = "LPEdorPrint";//团单
       fieldName = "EdorNo";//团单
//       edorNo = "20070205000034";

//        tableName = "LPEdorPrint2";//团单增人清单

       UpdateBlobBL ub = new UpdateBlobBL(fileName, tableName,
                fieldName, edorNo);
        ub.exportBlob();
    }

    private static void doImport()
    {

        String tableName, fieldName, edorNo;

//        tableName = "LPEdorAppPrint";//个单
//       fieldName = "EdorAcceptNo";//个单
        edorNo = "20070605000158";
        String fileName = "c:/bak/"+edorNo+"import.xml";
//        tableName = "LPEdorPrint2";//团单增人清单
      tableName = "LPEdorPrint";//团单
       fieldName = "EdorNo";//团单
//       edorNo = "20070205000034";
       UpdateBlobBL ub = new UpdateBlobBL(fileName, tableName,
                fieldName, edorNo);
        ub.importBlob();
    }


    public static void main(String[] args)
    {
        doExport();
        doImport();
    }
}
