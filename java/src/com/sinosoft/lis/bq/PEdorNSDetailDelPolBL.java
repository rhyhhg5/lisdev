package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.ContInsuredBL;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vschema.LCPolSet;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 删除新增的险种信息
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PEdorNSDetailDelPolBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private String mOperate = null;   //操作方式
    private GlobalInput mGlobalInput = new GlobalInput();  //完整的用户信息
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();  //项目信息

    private MMap map = new MMap();

    public PEdorNSDetailDelPolBL()
    {
    }

    /**
     * 数据提交的公共方法，进行新增险种明细录入逻辑处理并提交数据库，
     * 提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象：包括：
     * A.GlobalInput对象，完整的登陆用户信息。
     * B.LPEdorItemSchema对象，包含EdorNo,EdorType,InsuredNo和PolNo
     * @param cOperate 数据操作字符串，此为"DELETE||INSUREDRISK"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("---PEdorNSDetailDelPolBL BL BEGIN---");

        if(getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit tSubmit = new PubSubmit();
        if(!tSubmit.submitData(data, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PGrpEdorAutoUWBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }


        return true;
    }

    /**
     * 数据提交的公共方法，进行新增险种明细录入逻辑处理并提交数据库，
     * 提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象：包括：
     * A.GlobalInput对象，完整的登陆用户信息。
     * B.LPEdorItemSchema对象，包含EdorNo,EdorType,InsuredNo和PolNo
     * @param cOperate 数据操作字符串，此为"DELETE||INSUREDRISK"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        //进行业务处理
        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {
        if(!"DELETE||INSUREDRISK".equals(this.mOperate))
        {
            mErrors.addOneError("传入的操作符不正确，需传入\"DELETE||INSUREDRISK\""
                                + "才能进行险种删除操作");
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * 将P原数据删除后，删除C表数据,若该被保人没有险种,则删除其健康告知信息
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if(!dealLPInfo())
        {
            return false;
        }
        if(!dealLCInfo())
        {
            return false;
        }

        return true;
    }

    /**
     * 删除线中的LC表信息，调用契约程序进行处理
     * @return boolean
     */
    private boolean dealLCInfo()
    {
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("InsuredNo", mLPEdorItemSchema.getInsuredNo()); //保全保存标记，默认为0，标识非保全
        tTransferData.setNameAndValue("PolNo", mLPEdorItemSchema.getPolNo());

        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tTransferData);


        //调用契约程序
        ContInsuredBL tContInsuredBL = new ContInsuredBL();
        MMap tMMap = tContInsuredBL.getSubmitMap(data, mOperate);
        if(tMMap == null)
        {
            mErrors.copyAllErrors(tContInsuredBL.mErrors);
            return false;
        }
        map.add(tMMap);
        
        
        
        String spolno = "select riskcode,appntno,contno from lcpol where appflag ='2' and polno='" + mLPEdorItemSchema.getPolNo() +"'";
    	SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(spolno);
        if(tSSRS.getMaxRow()>0 ){
        	for (int i=1 ; i<=tSSRS.getMaxRow(); i++){
        		if(tSSRS.GetText(i, 1).equals("232701")){
        			
//        	        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
//        	        tLCInsuredDB.setContNo(tSSRS.GetText(i, 3));
//        	        tLCInsuredDB.setInsuredNo(tSSRS.GetText(i, 2));
// 
//        	        LCInsuredSet set = tLCInsuredDB.query();
              	String str = "delete  from lcinsured where contno ='" + tSSRS.GetText(i, 3)+"' and insuredno ='"+tSSRS.GetText(i, 2)+"'";
        	    map.put(str, SysConst.DELETE);
        	    
        		}
        	}
        }

        return true;
    }

    /**
     * 删除保全险种信息
     * @return boolean：如果发生错误则返回false,否则返回true
     */
    private boolean dealLPInfo()
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPEdorItemSchema.setPolNo(mLPEdorItemSchema.getPolNo());

        MMap tMMap = CommonBL.delPolInfoFromP(tLPEdorItemSchema);
        if(tMMap == null)
        {
            mErrors.addOneError("准备删除保全险种信息出错。");
            return false;
        }
        map.add(tMMap);

        //若删除的是最后一个新增险种，则删除健康告知
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
        tLCPolDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        tLCPolDB.setAppFlag(BQ.APPFLAG_SIGN);
        LCPolSet set = tLCPolDB.query();
        if(set.size() == 1)
        {
            String[] tables =
                              {" LPCustomerImpart ", " LPCustomerImpartDetail ",
                              " LPCustomerImpartParams "};
            String sinsuredno = "";
//            LCPolDB sLCPolDB = new LCPolDB();
//            sLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
//            sLCPolDB.setAppFlag(BQ.APPFLAG_EDOR);
//            LCPolSet sLCPolset = sLCPolDB.query();
            ExeSQL  tExSql = new ExeSQL();
            String spolno = "selcect riskcode from lcpol where appflag ='2' and polno='" + mLPEdorItemSchema.getPolNo() +"'";
            String spaytodate = tExSql.getOneValue(spolno);
            if(null != spaytodate  &&  spaytodate.equals("232701")){
            	sinsuredno = tLCPolDB.getAppntNo();
            }else {
             	sinsuredno = tLCPolDB.getInsuredNo();
            }
            
            for(int i = 0; i < tables.length; i++)
            {
                String sql = "delete from " + tables[i]
                             + "where edorNo = '"
                             + mLPEdorItemSchema.getEdorNo() + "' "
                             + "   and edorType = '"
                             + mLPEdorItemSchema.getEdorType() + "'"
                             + "   and contNo = '"
                             + tLCPolDB.getContNo() + "' "
                             + "   and customerNo = '"
                             + sinsuredno + "' ";
                map.put(sql, SysConst.DELETE);
            }
        }

        return true;
    }

    /**
     * @param cInputData VData:getSubmitMap中传入的VData
     * @return boolean: 成功true
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data
                           .getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) data
                                .getObjectByObjectName("LPEdorItemSchema", 0);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError("接收数据失败");
            return false;
        }

        if(mGlobalInput == null || mLPEdorItemSchema == null)
        {
            this.mErrors.addOneError("输入数据有误");
            return false;
        }
        System.out.println("Time " + PubFun.getCurrentDate() + " "
                           + PubFun.getCurrentTime() + ": "
                           + mGlobalInput.Operator + " ");

        return true;
    }

    public static void main(String[] args)
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorNo("20061016000004");
        tLPEdorItemSchema.setEdorType("NS");
        tLPEdorItemSchema.setInsuredNo("000025811");
        tLPEdorItemSchema.setPolNo("11000095593");

        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "endor0";
        tGlobalInput.ManageCom = "86";

        VData tVData = new VData();
        tVData.add(tLPEdorItemSchema);
        tVData.add(tGlobalInput);

        PEdorNSDetailDelPolBL tPEdorNSDetailDelPolBL = new
            PEdorNSDetailDelPolBL();
        if(!tPEdorNSDetailDelPolBL.submitData(tVData, "DELETE||INSUREDRISK"))
        {
            System.out.println("Submit Failed! "
                               + tPEdorNSDetailDelPolBL.mErrors.getErrContent());
        }
        else
        {
            System.out.println("Submit Succed!");
        }

        PEdorNSDetailDelPolBL bl = new PEdorNSDetailDelPolBL();
    }
}
