package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p> Title: Web业务系统 </p>
 * <p> Description: UI功能类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class PEdorAppItemUI
{
    private PEdorAppItemBL mPEdorAppItemBL = null;

    /** 数据操作字符串 */
    private String mOperate;

    private VData mResult = null;


    /** 错误处理类 */
    public CErrors mErrors = new CErrors();


    // @Constructor
    public PEdorAppItemUI()
    {}


    // @Method
    /**
     * 数据提交方法
     * @param: cInputData 传入的数据
     *		  cOperate 数据操作字符串
     * @return: boolean
     **/
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 数据操作字符串拷贝到本类中
        this.mOperate = cOperate;
        mPEdorAppItemBL = new PEdorAppItemBL();
        if (!mPEdorAppItemBL.submitData(cInputData, mOperate))
        {
            this.mErrors.copyAllErrors(mPEdorAppItemBL.mErrors);
            return false;
        }
        mResult = mPEdorAppItemBL.getResult();
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mPEdorAppItemBL.mErrors.getFirstError();
    }


    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorAcceptNo("20051117000036");
        tLPEdorItemSchema.setEdorNo("20051117000036");
        tLPEdorItemSchema.setEdorType("AD");
        tLPEdorItemSchema.setContNo("00000005801");
        LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
        mLPEdorItemSet.add(tLPEdorItemSchema);

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "endor5";
        tGI.ManageCom = "8611";
        VData tVData = new VData();
        tVData.add(mLPEdorItemSet);
        tVData.add(tGI);
        PEdorAppItemUI tPEdorAppItemUI = new PEdorAppItemUI();
        if (!tPEdorAppItemUI.submitData(tVData, ""))
        {

        }
    }
}
