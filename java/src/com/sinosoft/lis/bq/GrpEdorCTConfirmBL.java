package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团单退保保全确认</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 2.0
 */
public class GrpEdorCTConfirmBL implements EdorConfirm
{
    /** 全局数据 */
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null; //保全项目信息
    private GlobalInput mGI = null; //完整的操作员信息
    private ExeSQL mExeSQL = new ExeSQL();
    private LPPolDB mLPPolDB = new LPPolDB();

    private ContCancel mContCalcel = new ContCancel(); //退保保单转移核心类

    private String mEdorNo = null; //受理号
    private String mEdorType = null; //项目类型
    private String mGrpContNo = null; //团单号
    private String mCrrDate = PubFun.getCurrentDate();
    private String mCrrTime = PubFun.getCurrentTime();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private MMap map = new MMap();

    public GrpEdorCTConfirmBL()
    {
    }

    /**
     * 进行团单退保保全确认业务逻辑的处理并提交数据库。
     * 将险种转移到B表，并存储保单状态表信息
     * @param cInputData VData：包含：
     A.	GlobalInput对象，完整的登陆用户信息。
     B.	LPGrpEdorItem对象，保全项目信息。
     * @param cOperate String:此为“”
     * @return boolean, 成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        System.out.println("qulq ctconfirm");
        if(!getInputData(cInputData))
        {
            return false;
        }
				if(!checkData())
				{
						return false;
				}
        //数据准备操作
        if(!dealData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @param: 无
     * @return: boolean，操作成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) data
                               .getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mGI = ((GlobalInput) data
               .getObjectByObjectName("GlobalInput", 0));

        if(mLPGrpEdorItemSchema == null || mGI == null)
        {
            this.mErrors.addOneError("接收数据失败");
            return false;
        }

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if(tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1)
        {
            mErrors.addOneError("查询保全项目信息失败！");
            System.out.println("GrpEdorWTConfirmBL->getInputData: "
                               + tLPGrpEdorItemDB.mErrors.getErrContent());
            return false;
        }
        mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemSet.get(1));

        mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
        mEdorType = mLPGrpEdorItemSchema.getEdorType();
        mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
        if(mEdorNo == null || mEdorType == null || mGrpContNo == null)
        {
            mErrors.addOneError("传入的数据不完整");
            return false;
        }

        return true;
    }

    /**
     * 得到待退保的保障计划
     * @return LPContPlanRiskSet：待退保的保张计划
     */
    private LPContPlanRiskSet getLPContPlanRisk()
    {
        LPContPlanRiskDB tLPContPlanRiskDB = new LPContPlanRiskDB();
        tLPContPlanRiskDB.setEdorNo(mEdorNo);
        tLPContPlanRiskDB.setEdorType(mEdorType);
        tLPContPlanRiskDB.setGrpContNo(mGrpContNo);
        LPContPlanRiskSet tLPContPlanRiskSet = tLPContPlanRiskDB.query();
        if(tLPContPlanRiskSet.size() == 0)
        {
            mErrors.addOneError("没有查询到需要犹豫期撤保的数据");
            return null;
        }
        return tLPContPlanRiskSet;
    }

    /**
     * 处理豫期撤保保全确认业务逻辑：
     A．若被保人险种全部全部撤保则被保人退保。
     B．若个单保单被保人全部撤保，则个单撤保。
     C．若团单险种下个险全部退保，则团单险种退保。
     D．若团单下所有个险全部退保，则团单退保。
     * @return boolean，操作成功true，否则false
     */
    private boolean dealData()
    {
        mContCalcel.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        //团险万能退保
        if(jugdeUliByGrpContNo(mLPGrpEdorItemSchema)){
        	if(!cancelULIContULI()){
        		return false;
        	}
        }else{
        	LPContPlanRiskSet tLPContPlanRiskSet = getLPContPlanRisk();
        	if(tLPContPlanRiskSet == null)
        	{
        		return false;
        	}
        	
        	for(int i = 1; i <= tLPContPlanRiskSet.size(); i++)
        	{
        		if(!cancelCont(tLPContPlanRiskSet.get(i)))
        		{
        			return false;
        		}
        		
        		if(!dealOneContPlanRisk(tLPContPlanRiskSet.get(i)))
        		{
        			return false;
        		}
        		
        	} //for
        }

        if(!dealLCGrpCont())
        {
            return false;
        }

        //个单退保
//       select * from LCCont where contNo not in(
//           select contNo from LCPol a where polNo not in
//           (select polNo from LPPOl where grpContNo = a.grpContNo and edorNo = '2
//            0061025000004')
//           )
//           and grpContNo = '0000019901';


        //将体检通知状态置为已扣除
//        if(!changeState())
//        {
//            return false;
//        }

        mResult.clear();
        mResult.add(map);

        return true;
    }

    /**
     * dealLCGrpPol
     * 1、若团险下所有个险退保，则团险退保
     * @return boolean：成功true；否则false
     */
    private boolean dealLCGrpCont()
    {
        int grpPolCTCount = 0;  //团险种退保个数

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for(int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            //该团险下个险数
            int polCountC = getLCPolCount(tLCGrpPolSet.get(i).getGrpPolNo());
            if(polCountC == Integer.MIN_VALUE)
            {
                return false;
            }

            //该团险下退保的个险数
            int polCountP = getLPPolCount(tLCGrpPolSet.get(i).getGrpPolNo());
            if(polCountP == Integer.MIN_VALUE)
            {
                return false;
            }

            //若团险下所有个险退保，则团险退保
            if(polCountC == polCountP)
            {
                MMap tMMap = mContCalcel.prepareGrpPolData(
                    tLCGrpPolSet.get(i).getGrpPolNo(), mEdorNo);
                if(tMMap == null)
                {
                    return false;
                }
                map.add(tMMap);

                grpPolCTCount++;
            }
        }

        //若团险下所有团险退保，则团单退保
        if(tLCGrpPolSet.size() == grpPolCTCount)
        {
            MMap tMMap = mContCalcel.prepareGrpContData(mGrpContNo, mEdorNo);
            if(tMMap == null)
            {
                return false;
            }
            map.add(tMMap);
        }

        return true;
    }

    /**
     * 处理个单退保
     * @return boolean：成功true；否则false
     */
    private boolean cancelCont(LPContPlanRiskSchema tLPContPlanRiskSchema)
    {
        MMap contPlanMap = new MMap();

        //得到所有保单
        String sql = "select a.* "
                     + "from LCCont a, LPPol b "
                     + "where a.contNo = b.contNo "
                     + "   and b.edorNo = '" + mEdorNo + "' "
                     + "   and b.edorType = '" + mEdorType + "' "
                     + "   and b.grpContNo = '" + mGrpContNo + "' "
                     + "   and b.contPlanCode = '"
                     + tLPContPlanRiskSchema.getContPlanCode() + "' "
                     + "   and b.riskCode = '"
                     + tLPContPlanRiskSchema.getRiskCode() + "' ";
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = null;
        VData data = new VData(); //入库数据的容器

        int start = 1;
        int count = 10000; //每次取count条个险数据进行处理

        do
        {
            tLCContSet = tLCContDB.executeQuery(sql, start, count);
            for(int j = 1; j <= tLCContSet.size(); j++)
            {
                MMap tMMap = cancelOneCont(tLCContSet.get(j));
                if(tMMap == null)
                {
                    return false;
                }
                contPlanMap.add(tMMap);
            }
            //若本次处理保单数 <= count，则所有数据一起提交，否则直接提交
            if(tLCContSet.size() <= count && start < count)
            {
                break;
            }

            data.clear();
            data.add(contPlanMap);
            PubSubmit p = new PubSubmit();
            if(!p.submitData(data, ""))
            {
                System.out.println(p.mErrors.getErrContent());
                CError tError = new CError();
                tError.moduleName = "GrpEdorWTAppConfirmBL";
                tError.functionName = "setLJSGetEndorse";
                tError.errorMessage = "生成财务数据失败";
                mErrors.addOneError(tError);
                return false;
            }

            contPlanMap = new MMap(); //用这种方式清空待提交的数据
            start += count;
        }
        while(tLCContSet.size() > 0);

        map.add(contPlanMap);

        return true;
    }

    /**
     * 处理个单退保
     * @return boolean：成功true；否则false
     */
    private boolean cancelULIContULI()
    {
        MMap contPlanMap = new MMap();

        //得到所有保单
        String sql = "select a.* "
                     + "from LCCont a, LPPol b "
                     + "where a.contNo = b.contNo "
                     + "   and b.edorNo = '" + mEdorNo + "' "
                     + "   and b.edorType = '" + mEdorType + "' "
                     + "   and b.grpContNo = '" + mGrpContNo + "' ";
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = null;
        VData data = new VData(); //入库数据的容器

        int start = 1;
        int count = 10000; //每次取count条个险数据进行处理

        do
        {
            tLCContSet = tLCContDB.executeQuery(sql, start, count);
            for(int j = 1; j <= tLCContSet.size(); j++)
            {
                MMap tMMap = cancelOneCont(tLCContSet.get(j));
                if(tMMap == null)
                {
                    return false;
                }
                contPlanMap.add(tMMap);
            }
            //若本次处理保单数 <= count，则所有数据一起提交，否则直接提交
            if(tLCContSet.size() <= count && start < count)
            {
                break;
            }

            data.clear();
            data.add(contPlanMap);
            PubSubmit p = new PubSubmit();
            if(!p.submitData(data, ""))
            {
                System.out.println(p.mErrors.getErrContent());
                CError tError = new CError();
                tError.moduleName = "GrpEdorWTAppConfirmBL";
                tError.functionName = "setLJSGetEndorse";
                tError.errorMessage = "生成财务数据失败";
                mErrors.addOneError(tError);
                return false;
            }

            contPlanMap = new MMap(); //用这种方式清空待提交的数据
            start += count;
        }
        while(tLCContSet.size() > 0);

        map.add(contPlanMap);

        return true;
    }
    
    

    /**
     * dealOneContPlanRisk
     * 处理保障计划退保
     * @param lPContPlanRiskSchema LPContPlanRiskSchema：待退保的保障计划
     * @return boolean
     */
    private boolean dealOneContPlanRisk(LPContPlanRiskSchema
                                        tPContPlanRiskSchema)
    {
        String[] tLCContPlanTables =
            {"LCContPlanRisk", "LCContPlanFactory",
            "LCContPlanParam", "LCContPlanDutyParam"};
        for(int i = 0; i < tLCContPlanTables.length; i++)
        {
            String tableB = tLCContPlanTables[i].replaceFirst("LC", "LB");

            StringBuffer sqlBuff = new StringBuffer();
            sqlBuff.append("insert into ").append(tableB)
                .append(" (select '").append(mEdorNo).append("', ")
                .append(tLCContPlanTables[i]).append(".* ")
                .append("from ").append(tLCContPlanTables[i])
                .append(" where grpContNo = '").append(mGrpContNo)
                .append("'  and contPlanCode = '")
                .append(tPContPlanRiskSchema.getContPlanCode())
                .append("'  and riskCode = '")
                .append(tPContPlanRiskSchema.getRiskCode()).append("' )");
            map.put(sqlBuff.toString(), "INSERT");

            StringBuffer sql = new StringBuffer();
            sql.append("delete from ").append(tLCContPlanTables[i])
                .append(" where grpContNo = '").append(mGrpContNo)
                .append("'  and contPlanCode = '")
                .append(tPContPlanRiskSchema.getContPlanCode())
                .append("'  and riskCode = '")
                .append(tPContPlanRiskSchema.getRiskCode()).append("' ");
            map.put(sql.toString(), "DELETE");
        }

        return true;
    }

    /**
     * 处理个单退保
     * 1、若被保人所有险种退保，则被保人退保。
     * 2、由于被保人退保时会调用险种退保程序，所以被保人退保时不用存储preparePolData得到的数据
     * 3、若该保单所有被保人退保，则保单退保
     * 4、由于保单退保时会调用被保人退保程序，所以保单退保时不用存储prepareInsuredData得到的数据
     * @param tLCContSchema LCContSchema：需要退保的保单
     * @return MMap
     */
    private MMap cancelOneCont(LCContSchema tLCContSchema)
    {
        MMap contCTMap = new MMap(); //个单退保数据，只存储整单退保prepareContData得到的数据
        MMap insuredCTMap = new MMap(); //被保人退保数据，只存储被保人退保prepareInsuredData得到的数据
        int insuredCount = 0; //该保单退保人数

        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(tLCContSchema.getContNo());
        
     // 团单下正在进行查询的个单号
     		ExeSQL tExeSQL = new ExeSQL();
     		String time = PubFun.getCurrentDate() + " " + PubFun.getCurrentTime();
     		String insertSQL = "INSERT INTO ldtimetest VALUES ('CONTNO',current time,'"
     				+ time
     				+ "','个单:"
     				+ tLCContSchema.getContNo()
     				+ "进行查询中...',NULL,NULL) ";
     		tExeSQL.execUpdateSQL(insertSQL);
        
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        for(int i = 1; i <= tLCInsuredSet.size(); i++)
        {
            MMap polCTMap = new MMap(); //险种退保数据，只存储preparePolData得到的数据

            //该被保人需要退保的险种退保
            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setEdorNo(mEdorNo);
            tLPPolDB.setEdorType(mEdorType);
            tLPPolDB.setContNo(tLCContSchema.getContNo());
            tLPPolDB.setInsuredNo(tLCInsuredSet.get(i).getInsuredNo());
            LPPolSet tLPPolSet = tLPPolDB.query();
            for(int t = 1; t <= tLPPolSet.size(); t++)
            {
                MMap tMMap = mContCalcel.preparePolData(
                    tLPPolSet.get(t).getPolNo(), mEdorNo);
                polCTMap.add(tMMap);
            }

            //若被保人所有险种退保，则被保人退保。
            //由于被保人退保时会调用险种退保程序，所以被保人退保时不用存储preparePolData得到的数据
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setContNo(tLCContSchema.getContNo());
            tLCPolDB.setInsuredNo(tLCInsuredSet.get(i).getInsuredNo());
            LCPolSet tLCPolSet = tLCPolDB.query();
            if(tLCPolSet.size() == tLPPolSet.size())
            {
                MMap tMMap = mContCalcel.prepareInsuredData(
                    tLCContSchema.getContNo(),
                    tLCInsuredSet.get(i).getInsuredNo(),
                    mEdorNo);
                insuredCTMap.add(tMMap);
                insuredCount++;
            }
            else
            {
                insuredCTMap.add(polCTMap);
            }
        }

        //若该保单所有被保人退保，则保单退保
        //由于保单退保时会调用被保人退保程序，所以保单退保时不用存储prepareInsuredData得到的数据
        if(tLCInsuredSet.size() == insuredCount)
        {
            MMap tMMap = mContCalcel.prepareContData(
                tLCContSchema.getContNo(), mEdorNo);
            contCTMap.add(tMMap);
        }
        else
        {
            contCTMap.add(insuredCTMap);
        }

        return contCTMap;
    }

    /**
     * 退保个险数量
     * @return int
     */
    private int getLPPolCount(String tGrpPolNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.append(" select count(1) ")
            .append("from LPPol ")
            .append("where edorNo = '").append(mEdorNo)
            .append("'  and edorType = '").append(mEdorType)
            .append("'  and grpPolNo = '").append(tGrpPolNo).append("' ");
        String tLPPolCount = mExeSQL.getOneValue(sql.toString());

        if(mExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTConfirmBL";
            tError.functionName = "getLPPolCount";
            tError.errorMessage = "查询退保险种数出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return Integer.MIN_VALUE;
        }

        if(tLPPolCount.equals("") || tLPPolCount.equals("null"))
        {
            return 0;
        }
        return Integer.parseInt(tLPPolCount);
    }

    /**
     * 团险下个险数
     * @param tGrpPolNo String
     * @return int
     */
    private int getLCPolCount(String tGrpPolNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.append(" select count(1) ")
            .append("from LCPol ")
            .append("where grpPolNo = '").append(tGrpPolNo).append("' ");
        String tLPPolCount = mExeSQL.getOneValue(sql.toString());
        if(mExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTConfirmBL";
            tError.functionName = "getLPPolCount";
            tError.errorMessage = "查询退保险种数出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return Integer.MIN_VALUE;
        }

        if(tLPPolCount.equals("") || tLPPolCount.equals("null"))
        {
            return 0;
        }
        return Integer.parseInt(tLPPolCount);
    }

    /**
     * 得到团险个数
     * @return int
     */
    private int getLCGrpPolCount()
    {
        StringBuffer sql = new StringBuffer();
        sql.append(" select sum(1) ")
            .append("from LCGrpPol ")
            .append("where grpContNo = '").append(mGrpContNo).append("' ");
        String tLPPolCount = mExeSQL.getOneValue(sql.toString());
        if(tLPPolCount.equals("") || tLPPolCount.equals("null"))
        {
            return 0;
        }
        return Integer.parseInt(tLPPolCount);

    }

    /**
     * 将体检通知状态置为已扣除
     * @return boolean
     */
    private boolean changeState()
    {
        LPPENoticeDB tLPPENoticeDB = new LPPENoticeDB();
        tLPPENoticeDB.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPPENoticeDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPPENoticeDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        LPPENoticeSet tLPPENoticeSet = tLPPENoticeDB.query();

        LCPENoticeSet tLCPENoticeSet = new LCPENoticeSet();
        for(int i = 1; i <= tLPPENoticeSet.size(); i++)
        {
            LCPENoticeDB db = new LCPENoticeDB();
            db.setProposalContNo(tLPPENoticeSet.get(i).getProposalContNo());
            db.setPrtSeq(tLPPENoticeSet.get(i).getPrtSeq());
            if(!db.getInfo())
            {
                mErrors.addOneError(
                    "没有查询到" + tLPPENoticeSet.get(i).getAppName()
                    + "的体检记录");
                return false;
            }
            else
            {
                db.setPEState("06");
                tLCPENoticeSet.add(db.getSchema());
                map.put(tLCPENoticeSet, "UPDATE");
            }
        }

        mLPGrpEdorItemSchema.setEdorState(BQ.EDORSTATE_CONFIRM);
        mLPGrpEdorItemSchema.setOperator(mGI.Operator);
        mLPGrpEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLPGrpEdorItemSchema, "UPDATE");

        return true;
    }
private boolean checkData()
    {
        return true;
    }
    /**
     * 处理保障计划
     * 若保障计划下的所有险种都退保，则保障计划退保
     * @return boolean
     */
    private boolean contPlanCT()
    {
        String[] tLCContPlanTables =
            {"LCContPlanRisk", "LCContPlanFactory",
            "LCContPlanParam", "LCContPlanDutyParam"};
        String[] tLBContPlanTables =
            {"LBContPlanRisk", "LBContPlanFactory",
            "LBContPlanParam", "LBContPlanDutyParam"};

        LPContPlanRiskDB tLPContPlanRiskDB = new LPContPlanRiskDB();
        tLPContPlanRiskDB.setEdorNo(mEdorNo);
        tLPContPlanRiskDB.setEdorType(mEdorType);
        tLPContPlanRiskDB.setGrpContNo(mGrpContNo);
        LPContPlanRiskSet tLPContPlanRiskSet = tLPContPlanRiskDB.query();
        if(tLPContPlanRiskSet.size() == 0)
        {
            mErrors.addOneError("没有查询到保障计划。");
            return false;
        }

        for(int temp = 1; temp <= tLPContPlanRiskSet.size(); temp++)
        {
            LPContPlanRiskSchema schema = tLPContPlanRiskSet.get(temp);
            for(int i = 0; i < tLBContPlanTables.length; i++)
            {
                StringBuffer sql = new StringBuffer();
                sql.append("insert into ").append(tLBContPlanTables[i])
                    .append(" (select '").append(mEdorNo).append("', ")
                    .append(tLCContPlanTables[i]).append(".* ")
                    .append("from ").append(tLCContPlanTables[i])
                    .append(" where grpContNo = '").append(mGrpContNo)
                    .append("'  and contPlanCode = '")
                    .append(schema.getContPlanCode())
                    .append("'  and mainRiskCode = '")
                    .append(schema.getMainRiskCode())
                    .append("'  and riskCode = '").append(schema.getRiskCode())
                    .append("'  and planType = '").append(schema.getPlanType())
                    .append("' )");
                map.put(sql.toString(), "INSERT");
            }
            for(int i = 0; i < tLCContPlanTables.length; i++)
            {
                StringBuffer sql = new StringBuffer();
                sql.append("delete from ").append(tLCContPlanTables[i])
                    .append(" where grpContNo = '").append(mGrpContNo)
                    .append("'  and contPlanCode = '")
                    .append(schema.getContPlanCode())
                    .append("'  and mainRiskCode = '")
                    .append(schema.getMainRiskCode())
                    .append("'  and riskCode = '").append(schema.getRiskCode())
                    .append("'  and planType = '").append(schema.getPlanType())
                    .append("' ");
                map.put(sql.toString(), "DELETE");
            }
        }

        //LCContPlan单独处理
        //若整单退保，则在退保程序中已对LCContPlan进行处理，所以此处可以不考录整单退保的情况
        StringBuffer getLCContPlanSql = new StringBuffer();
        getLCContPlanSql.append("select distinct a.* ")
            .append("from LCContPlan a, LPContPlanRisk b ")
            .append("where a.grpContNo = b.grpContNo")
            .append("   and a.contPlanCode = b.contPlanCode ")
            .append("   and a.planType = b.planType ")
            .append("   and b.grpContNo = '").append(mGrpContNo)
            .append("'  and b.edorType = '").append(mEdorType)
            .append("'  and b.edorNo = '").append(mEdorNo).append("' ");
        System.out.println(getLCContPlanSql.toString());
        LCContPlanDB tLCContPlanDB = new LCContPlanDB();
        LCContPlanSet tLCContPlanSet = tLCContPlanDB
                                       .executeQuery(getLCContPlanSql.toString());
        if(tLCContPlanSet.size() == 0)
        {
            System.out.println(tLCContPlanDB.mErrors.getErrContent());
            mErrors.addOneError("没有查询到保障计划保全信息。");
            return false;
        }
        for(int i = 1; i <= tLCContPlanSet.size(); i++)
        {
            LCContPlanSchema schema = tLCContPlanSet.get(i);

            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
            tLCContPlanRiskDB.setGrpContNo(mGrpContNo);
            tLCContPlanRiskDB.setContPlanCode(schema.getContPlanCode());
            tLCContPlanRiskDB.setPlanType(schema.getPlanType());
            LCContPlanRiskSet tLCContPlanRiskSet = tLCContPlanRiskDB.query();

            tLPContPlanRiskDB.setContPlanCode(schema.getContPlanCode());
            tLPContPlanRiskDB.setPlanType(schema.getPlanType());
            tLPContPlanRiskSet = tLPContPlanRiskDB.query();

            //若保障计划下的险种全部退保，则保障计划退保
            if(tLPContPlanRiskSet.size() == tLCContPlanRiskSet.size())
            {
                StringBuffer sql = new StringBuffer();
                sql.append("insert into LBContPlan ")
                    .append(" (select '").append(mEdorNo).append("', ")
                    .append("LCContPlan.* ")
                    .append("from LCContPlan ")
                    .append(" where grpContNo = '").append(mGrpContNo)
                    .append("'  and contPlanCode = '")
                    .append(schema.getContPlanCode())
                    .append("'  and planType = '").append(schema.getPlanType())
                    .append("' )");
                map.put(sql.toString(), "INSERT");
                map.put(schema.getSchema(), "DELETE");
            }

        }

        return true;
    }
    
    
    //增加一个方法用来判断是否是万能险还是之前的险种，根据此方法区分
    public boolean jugdeUliByGrpContNo(LPGrpEdorItemSchema tLPGrpEdorItemSchema)
    {
        StringBuffer tSBql2 = new StringBuffer(256);
        tSBql2.append("select 1 from lcgrppol where GrpContNo = '");
        tSBql2.append(tLPGrpEdorItemSchema.getGrpContNo());
        tSBql2.append("' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')");
        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
        if (tSSRS5.getMaxRow() < 1)
        {
        	System.out.println("此团单不是团体万能");
            return false;
        }
        System.out.println("此团单是团体万能");
        return true;
    }

    /**
     * 查询需要做犹豫期退保的个单保单号
     * @return SSRS
     */
    private SSRS getLPCont()
    {
        String sql = "  select contNo "
                     + "from LPCont "
                     + "where grpContNo = '" + mGrpContNo
                     + "'  and edorType = '" + mEdorType
                     + "'  and edorNo = '" + mEdorNo + "' ";
        ExeSQL e = new ExeSQL();
        SSRS tSSRS = e.execSQL(sql);
        if(tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("没有查询到个单保全数据。");
        }
        return tSSRS;
    }

    private LPInsuredSet getLPInsured(String contNo)
    {
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        tLPInsuredDB.setEdorNo(mEdorNo);
        tLPInsuredDB.setEdorType(mEdorType);
        tLPInsuredDB.setContNo(contNo);
        LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();
        if(tLPInsuredSet.size() == 0)
        {
            CError.buildErr(this, "查询个人保单被保险人数据失败！");
        }

        return tLPInsuredSet;
    }

    private LPPolSet getLPPol(LPInsuredSchema schema)
    {
        if(mLPPolDB == null)
        {
            mLPPolDB = new LPPolDB();
        }
        mLPPolDB.setEdorNo(schema.getEdorNo());
        mLPPolDB.setEdorType(schema.getEdorType());
        mLPPolDB.setContNo(schema.getContNo());
        mLPPolDB.setInsuredNo(schema.getInsuredNo());
        LPPolSet set = mLPPolDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("查询被保人" + schema.getName() + "失败");
        }
        return set;
    }

    private int getLCPolCount(LPInsuredSchema schema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select count(1) ")
            .append("from LCPol ")
            .append("where contNo = '").append(schema.getContNo())
            .append("'   and insuredNo = '").append(schema.getInsuredNo())
            .append("' ");
        if(mExeSQL == null)
        {
            mExeSQL = new ExeSQL();
        }
        String polCount = mExeSQL.getOneValue(sql.toString());
        if(polCount.equals("") || polCount.equals("null"))
        {
            return 0;
        }
        return Integer.parseInt(polCount);
    }

    private int getLCInsuredCount(String contNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select count(1) ")
            .append("from LCInsured ")
            .append("where contNo = '").append(contNo)
            .append("' ");
        if(mExeSQL == null)
        {
            mExeSQL = new ExeSQL();
        }
        String insuredCount = mExeSQL.getOneValue(sql.toString());
        if(insuredCount.equals("") || insuredCount.equals("null"))
        {
            return 0;
        }
        return Integer.parseInt(insuredCount);
    }

    public static void main(String[] a)
    {
        LPGrpEdorItemDB db = new LPGrpEdorItemDB();
        db.setEdorNo("20061025000004");
        db.setEdorType("CT");

        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        VData data = new VData();
        data.add(db.query().get(1));
        data.add(gi);

        GrpEdorCTConfirmBL bl = new GrpEdorCTConfirmBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }

}
