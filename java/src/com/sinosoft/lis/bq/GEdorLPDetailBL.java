package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LCInsuredDB;


public class GEdorLPDetailBL
{
    public CErrors mErrors = new CErrors();

    private String mMessage = null;

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private static String mEdorType = "LP";

    private String mGrpContNo = null;

    private LPDiskImportSet mLPDiskImportSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     * @param grpContNo String
     */
    public GEdorLPDetailBL(GlobalInput gi, String edorNo, String grpContNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = grpContNo;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        mLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, mEdorType,
                mGrpContNo, BQ.IMPORTSTATE_SUCC);
        if (mLPDiskImportSet.size() == 0)
        {
            mErrors.addOneError("找不到磁盘导入数据！");
            return false;
        }
        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean dealData()
    {
        mMessage=null;
        if (!checkData())
        {
            return false;
        }
        if(mMessage==null||mMessage.equals(""))
        {
            changeEdorState(BQ.EDORSTATE_INPUT);
        }
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if (!checkPol())
        {
            return false;
        }

        if (!checkCustomers())
        {
            return false;
        }

        if (!checkBank())
        {
            return false;
        }
        return true;
    }

    /**
     * 检查保单的有效性
     * @return boolean
     */
    private boolean checkPol()
    {
        LCGrpContDB tLCGrpContDB=new LCGrpContDB();
        LCGrpContSchema tLCGrpContSchema=new LCGrpContSchema();
        LCGrpContSet tLCGrpContSet=new LCGrpContSet();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        tLCGrpContSet=tLCGrpContDB.query();
        if (tLCGrpContDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "GEdorLPDetailBL";
            tError.functionName = "checkPol";
            tError.errorMessage = "查询团体保单表出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(tLCGrpContSet.size()==0){
            CError tError = new CError();
            tError.moduleName = "GEdorLPDetailBL";
            tError.functionName = "checkPol";
            tError.errorMessage = "团体合同号码不存在！";
            this.mErrors.addOneError(tError);
            return false;
        }else
        {
           tLCGrpContSchema=tLCGrpContSet.get(1);
           if (!tLCGrpContSchema.getAppFlag().equals("1")) {
               CError tError = new CError();
               tError.moduleName = "GEdorLPDetailBL";
               tError.functionName = "checkPol";
               tError.errorMessage = "该保单不在有效期内！";
               this.mErrors.addOneError(tError);
               return false;
           }
        }

        return true;
    }

    /**
     * 检查客户的有效性
     * @return boolean
     */
    private boolean checkCustomers()
    {
        boolean flag = true;
        String tErrorReason;
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            tErrorReason="";
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            if(tLPDiskImportSchema.getInsuredNo()!=null&&!tLPDiskImportSchema.getInsuredNo().equals(""))
            {//如果InsuredNo不为空
                tLCInsuredSchema=checkInsuredNo(tLPDiskImportSchema.getInsuredNo());
                if(tLCInsuredSchema!=null)
                {//如果InsuredNo正确,如果五要素都为空则回写五要素
                    if ((tLPDiskImportSchema.getInsuredName() == null ||
                         tLPDiskImportSchema.getInsuredName().equals("")) &&
                        (tLPDiskImportSchema.getSex() == null ||
                         tLPDiskImportSchema.getSex().equals("")) &&
                        (tLPDiskImportSchema.getBirthday() == null ||
                         tLPDiskImportSchema.getBirthday().equals("")) &&
                        (tLPDiskImportSchema.getIDType() == null ||
                         tLPDiskImportSchema.getIDType().equals("")) &&
                        (tLPDiskImportSchema.getIDNo() == null ||
                         tLPDiskImportSchema.getIDNo().equals("")))
                    {
                        tLPDiskImportSchema.setInsuredName(tLCInsuredSchema.getName());
                        tLPDiskImportSchema.setSex(tLCInsuredSchema.getSex());
                        tLPDiskImportSchema.setBirthday(tLCInsuredSchema.getBirthday());
                        tLPDiskImportSchema.setIDType(tLCInsuredSchema.getIDType());
                        tLPDiskImportSchema.setIDNo(tLCInsuredSchema.getIDNo());
                        mMap.put(tLPDiskImportSchema, "UPDATE");
                    }else
                    {//如果不为空校验是否正确
                        if(tLCInsuredSchema.getName()!=null){
                            if(!tLCInsuredSchema.getName().equals(tLPDiskImportSchema.getInsuredName())){
                                tErrorReason = "被保人姓名不正确，导入无效！";
                            }
                        }
                        if(tLCInsuredSchema.getSex()!=null){
                            if(!tLCInsuredSchema.getSex().equals(tLPDiskImportSchema.getSex())){
                                tErrorReason = "被保人性别不正确，导入无效！";
                            }
                        }
                        if(tLCInsuredSchema.getBirthday()!=null){
                            if(!tLCInsuredSchema.getBirthday().equals(tLPDiskImportSchema.getBirthday())){
                                tErrorReason = "被保人出生日期 不正确，导入无效！";
                            }
                        }
                        if(tLCInsuredSchema.getIDType()!=null){
                            if(!tLCInsuredSchema.getIDType().equals(tLPDiskImportSchema.getIDType())){
                                tErrorReason = "被保人证件类型不正确，导入无效！";
                            }
                        }
                        if(tLCInsuredSchema.getIDNo()!=null){
                            if(!tLCInsuredSchema.getIDNo().equals(tLPDiskImportSchema.getIDNo())){
                                tErrorReason = "被保人证件号码不正确，导入无效！";
                            }
                        }
                    }
                }else
                {//如果InsuredNo不正确
                    tErrorReason = "被保人客户号不正确，导入无效！";
                }
            }else
            {//如果InsuredNo为空，校验五要素
                tLCInsuredSchema=checkInsuredInfo(tLPDiskImportSchema);
                if(tLCInsuredSchema!=null)
                {//如果正确更新InsuredNo
                    tLPDiskImportSchema.setInsuredNo(tLCInsuredSchema.getInsuredNo());
                    mMap.put(tLPDiskImportSchema, "UPDATE");
                }else
                {
                    tErrorReason = "保单下不存在该客户，导入无效！";
                }
            }

            if (!tErrorReason.equals(""))
            {
                String sql = "Update LPDiskImport " +
                        "Set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                        " ErrorReason = '"+tErrorReason+"', " +
                        " Operator = '" + mGlobalInput.Operator + "', " +
                        " ModifyDate = '" + mCurrentDate + "', " +
                        " ModifyTime = '" + mCurrentTime + "' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + mEdorType + "' " +
                        "and GrpContNo = '" + mGrpContNo + "' " +
                        "and SerialNo = '" +
                        tLPDiskImportSchema.getSerialNo() +
                        "' ";
                mMap.put(sql, "UPDATE");
                flag = false;
            }
        }
        if (flag == false)
        {
            mMessage = "导入数据中有无效客户！";
        }
        return true;
    }

    private LCInsuredSchema checkInsuredNo(String insuredNo)
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setGrpContNo(mGrpContNo);
        tLCInsuredDB.setInsuredNo(insuredNo);
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() == 0) {
            return null;
        }
        return tLCInsuredSet.get(1);
    }

    private LCInsuredSchema checkInsuredInfo(LPDiskImportSchema pLPDiskImportSchema) {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setGrpContNo(pLPDiskImportSchema.getGrpContNo());
        tLCInsuredDB.setName(pLPDiskImportSchema.getInsuredName());
        tLCInsuredDB.setSex(pLPDiskImportSchema.getSex());
        tLCInsuredDB.setBirthday(pLPDiskImportSchema.getBirthday());
        tLCInsuredDB.setIDType(pLPDiskImportSchema.getIDType());
        tLCInsuredDB.setIDNo(pLPDiskImportSchema.getIDNo());
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() == 0) {
            return null;
        }
        return tLCInsuredSet.get(1);
    }


    /**
     * 检查银行的有效性
     * @return boolean
     */
    private boolean checkBank()
    {
        boolean flag = true;
        for (int i = 1; i <= mLPDiskImportSet.size(); i++) {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            String bankCode = tLPDiskImportSchema.getBankCode();
            String bankAccNo = tLPDiskImportSchema.getBankAccNo();
            String accName = tLPDiskImportSchema.getAccName();

            if (bankCode != null && !bankCode.equals("") &&
                bankAccNo != null && !bankAccNo.equals("") &&
                accName != null && !accName.equals("")) //如果银行代码，银行帐号，户名都不为空，则校验银行代码
            {
                LDBankDB tLDBankDB = new LDBankDB();
                tLDBankDB.setBankCode(bankCode);
                if (tLDBankDB.query().size() == 0)
                {
                    String sql = "Update LPDiskImport " +
                                 "Set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                                 " ErrorReason = '银行代码无效！', " +
                                 " Operator = '" + mGlobalInput.Operator +
                                 "', " +
                                 " ModifyDate = '" + mCurrentDate + "', " +
                                 " ModifyTime = '" + mCurrentTime + "' " +
                                 "where EdorNo = '" + mEdorNo + "' " +
                                 "and EdorType = '" + mEdorType + "' " +
                                 "and GrpContNo = '" + mGrpContNo + "' " +
                                 "and SerialNo = '" +
                                 tLPDiskImportSchema.getSerialNo() +
                                 "' ";
                    mMap.put(sql, "UPDATE");
                    flag = false;
                }
            } else if ((bankCode == null || bankCode.equals("")) &&
                       (bankAccNo == null || bankAccNo.equals("")) &&
                       (accName == null || accName.equals(""))) ////如果银行代码，银行帐号，户名都为空
            {
                flag = true;
            } else
            {
                String sql = "Update LPDiskImport " +
                             "Set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                             " ErrorReason = '银行代码,户名,帐号填写不完整!', " +
                             " Operator = '" + mGlobalInput.Operator +
                             "', " +
                             " ModifyDate = '" + mCurrentDate + "', " +
                             " ModifyTime = '" + mCurrentTime + "' " +
                             "where EdorNo = '" + mEdorNo + "' " +
                             "and EdorType = '" + mEdorType + "' " +
                             "and GrpContNo = '" + mGrpContNo + "' " +
                             "and SerialNo = '" +
                             tLPDiskImportSchema.getSerialNo() +
                             "' ";
                mMap.put(sql, "UPDATE");
                flag = false;
            }
        }
        if (flag == false) {
            mMessage = "导入数据中有无效的银行帐户信息！";
        }
        return true;
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void changeEdorState(String edorState)
    {
        String sql = "update  LPGrpEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }


    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
