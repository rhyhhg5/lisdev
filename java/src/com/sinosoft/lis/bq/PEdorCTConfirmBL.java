package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PEdorCTConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LPInsuredDB mLPInsuredDB = null;
    private LPPolDB mLPPolDB = null;
    private LCPolDB mLCPolDB = null;
    private ExeSQL mExeSQL = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();

    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!prepareData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
    	
    	LPContSchema tLPContSchema = getLPCont();
    	if (tLPContSchema == null)
        {
            return false;
        }
    	
    	LCContSchema tLCContSchema = getLCCont();
    	if (tLCContSchema == null)
        {
            return false;
        }

    	
    	String updateSQL = "";
    	String updateSQL2 = "";
        
        if ("4".equals(tLPContSchema.getPayMode())){
        	updateSQL = " update LCCont set BankCode='"+tLPContSchema.getBankCode()+"',BankAccNo='"+tLPContSchema.getBankAccNo()+"',AccName='"+tLPContSchema.getAccName()+"' where contno='"+mLPEdorItemSchema.getContNo()+"' and (select 1 from LPCont where Edorno = '"+mLPEdorItemSchema.getEdorNo()+"') is not null ";
        	updateSQL2 = " update LPCont set BankCode='"+tLCContSchema.getBankCode()+"',BankAccNo='"+tLCContSchema.getBankAccNo()+"',AccName='"+tLCContSchema.getAccName()+"' where contno='"+mLPEdorItemSchema.getContNo()+"' and (select 1 from LPCont where Edorno = '"+mLPEdorItemSchema.getEdorNo()+"') is not null";
	        map.put(updateSQL, SysConst.UPDATE);
	        map.put(updateSQL2, SysConst.UPDATE);
        }
        
        
    	
    	
        //由于万能险解约可能对帐户进行了结算，所以这里要交换保全数据
        if (!validateEdorData())
        {
            return false;
        }
        String aEdorNo = mLPEdorItemSchema.getEdorNo();
        ContCancel tContCancel = new ContCancel();
        tContCancel.setEdorType(mLPEdorItemSchema.getEdorType());
        LPPolSet tLPPolSet = null;


        MMap tMMap = new MMap(); //退保信息
        int ctLPInsuredCount = 0; //退保的被保人数量
        LPInsuredSet tLPInsuredSet = getLPInsured();
        if (tLPInsuredSet.size() == 0)
        {
            return false;
        }

       
        for (int j = 1; j <= tLPInsuredSet.size(); j++)
        {
            tLPPolSet = getLPPol(tLPInsuredSet.get(j));
            int lcpolCount = getLCPolCount(tLPInsuredSet.get(j)); //个人险种数
            if (tLPPolSet.size() == 0 || lcpolCount == 0)
            {
                mErrors.addOneError("险种退保信息不完整。");
                return false;
            }
            //被保人退保
            if (tLPPolSet.size() == lcpolCount)
            {
                ctLPInsuredCount++;
                tMMap.add(tContCancel.prepareInsuredData(
                        tLPInsuredSet.get(j).getContNo(),
                        tLPInsuredSet.get(j).getInsuredNo(),
                        aEdorNo));

                if (tContCancel.mErrors.needDealError())
                {
                    CError.buildErr(this, "准备被保险人数据失败！");
                    return false;
                }
            }
            //险种退保
            else
            {
                for (int t = 1; t <= tLPPolSet.size(); t++)
                {
                    tMMap.add(tContCancel.preparePolData(
                            tLPPolSet.get(t).getPolNo(),
                            aEdorNo));
                    if (tContCancel.mErrors.needDealError())
                    {
                        CError.buildErr(this, "准备个人险种数据失败！");
                        return false;
                    }
                }
            } //险种退保
        } //保单下需要处理的被保人

        //若被保人都被退保，则个单退保，tMMap可以直接通过prepareContData方法得到
        int lcInsuredCount = getLCInsuredCount(tLPContSchema);
        if (ctLPInsuredCount == lcInsuredCount)
        {
            tMMap = tContCancel.prepareContData(mLPEdorItemSchema.getContNo(),
                                                aEdorNo);
            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备个人保单数据失败！");
                return false;
            }
        }
        
        //若为银保通申请工单保全确认成功，则修改银保通表信息。
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mLPEdorItemSchema.getContNo());
        LCAppntSet tLCAppntSet = tLCAppntDB.query();
        if(tLCAppntSet==null||tLCAppntSet.size()<1){
        		System.out.println("修改银保通数据失败");
        }else{
        	LCAppntSchema tLCAppntSchema = tLCAppntSet.get(1).getSchema();
        	
        	//查询银保通犹豫期退保数据
        	LPYBTAppWTDB tLPYBTAppWTDB = new LPYBTAppWTDB();
        	tLPYBTAppWTDB.setContNo(mLPEdorItemSchema.getContNo());
        	tLPYBTAppWTDB.setAppntName(tLCAppntSchema.getAppntName());
        	tLPYBTAppWTDB.setAppntSex(tLCAppntSchema.getAppntSex());
        	tLPYBTAppWTDB.setAppntBirthday(tLCAppntSchema.getAppntBirthday());
        	tLPYBTAppWTDB.setAppntIDType(tLCAppntSchema.getIDType());
        	tLPYBTAppWTDB.setAppntIDNo(tLCAppntSchema.getIDNo());
        	LPYBTAppWTSet tLPYBTAppWTSet = tLPYBTAppWTDB.query();
        	if(tLPYBTAppWTSet!=null&&tLPYBTAppWTSet.size()>0){
        		LPYBTAppWTSchema tLPYBTAppWTSchema = tLPYBTAppWTSet.get(1).getSchema();
        		tLPYBTAppWTSchema.setAppState("2");
        		tLPYBTAppWTSchema.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());
        		tLPYBTAppWTSchema.setModifyDate(PubFun.getCurrentDate());
        		tLPYBTAppWTSchema.setModifyTime(PubFun.getCurrentTime());
        		map.put(tLPYBTAppWTSchema, SysConst.UPDATE);
        	}
        }
        
       
        
        map.add(tMMap);

//      添加对于存在未还贷款情况下解约的处理
        map.add(getLoanMap());
        
        //添加保费留存的数据修改
        map.add(getRemainMap(tLPContSchema));
        
        updateEdorState();
        mResult.clear();
        mResult.add(map);

    	
        return true;
    }
    
    private MMap getRemainMap(LPContSchema tLPContSchema){
    	MMap tMMap = new MMap();
    	
    	String updateSQL = "";
    	
    	String remainSQL = "select 1 from lccontremain where remainstate='1' " +
    			" and exists (select 1 from lpcont where contno=lccontremain.contno " +
    			" and (cvalidate + 2 years) > '"+mLPEdorItemSchema.getEdorValiDate()+"' " +
    			" and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"') " +
    			" and applyyears='2' " +
    			" and contno='"+tLPContSchema.getContNo()+"' ";
    	
    	ExeSQL tExeSQL = new ExeSQL();
    	
    	String remainFlag = tExeSQL.getOneValue(remainSQL);
    	
    	if(remainFlag!=null&&"1".equals(remainFlag)){
    		 updateSQL = " update  lccontremain set remainstate='3',edorZTdate='"+PubFun.getCurrentDate()+"',edorZTfee="+mLPEdorItemSchema.getGetMoney()+", " +
    				" modifydate='"+PubFun.getCurrentDate()+"',modifytime='"+PubFun.getCurrentTime()+"' where contno='"+tLPContSchema.getContNo()+"' and applyyears='2' ";
    		 tMMap.put(updateSQL, SysConst.UPDATE);
    	}
    	
    	
    	return tMMap;
    }
    
    /**
     * 准备需要保存的数据 (贷款信息提交)
     * @return VData
     */
    private MMap getLoanMap(){
    	try {
			// 交换借款表数据
			LOLoanSchema tnewLOLoanSchema = new LOLoanSchema();
			LPLoanSchema tnewLPLoanSchema = new LPLoanSchema();
			Reflections tReflections=new Reflections();
			MMap tMap =new MMap();

			LPLoanDB tLPLoanDB = new LPLoanDB();
			LPLoanSet tLPLoanSet = new LPLoanSet();
			LPLoanSchema tLPLoanSchema = new LPLoanSchema();
			tLPLoanDB.setActuGetNo(mLPEdorItemSchema.getEdorNo());
			tLPLoanDB.setEdorType(mLPEdorItemSchema.getEdorType());
			tLPLoanSet = tLPLoanDB.query();
			if (tLPLoanSet.size() != 1) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "PEdorLNConfirmBL";
				tError.functionName = "dealData";
				tError.errorMessage = "查询保全贷款记录信息失败！";
				this.mErrors.addOneError(tError);
				return null;
			}
			tLPLoanSchema = tLPLoanSet.get(1);
			tReflections.transFields(tnewLOLoanSchema, tLPLoanSchema);
			tMap.put(tnewLOLoanSchema, "DELETE&INSERT");

			LOLoanDB tLOLoanDB = new LOLoanDB();
			LOLoanSet tLOLoanSet = new LOLoanSet();
			LOLoanSchema tLOLoanSchema = new LOLoanSchema();
			tLOLoanDB.setPolNo(tLPLoanSchema.getPolNo());
			tLOLoanDB.setPayOffFlag("0");
			tLOLoanSet = tLOLoanDB.query();
			if (tLOLoanSet.size() != 1) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "PEdorLNConfirmBL";
				tError.functionName = "dealData";
				tError.errorMessage = "查询保全贷款记录信息失败！";
				this.mErrors.addOneError(tError);
				return null;
			}
			tLOLoanSchema = tLOLoanSet.get(1);
			tReflections.transFields(tnewLPLoanSchema, tLOLoanSchema);
			//还款后loloan的P表中备份的edorno为还款理算
			tnewLPLoanSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
			tnewLPLoanSchema.setEdorType(mLPEdorItemSchema.getEdorType());
			tMap.put(tnewLPLoanSchema, "DELETE&INSERT");

			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
			if (!tLCContDB.getInfo()) {
				CError tError = new CError();
				tError.moduleName = "PEdorFCConfirmBL";
				tError.functionName = "prepareData";
				tError.errorMessage = "查询保单信息失败！";
				this.mErrors.addOneError(tError);
				return null;
			}
			String SQL = "select * From lcconthangupstate where HangupType ='" + BQ.HANGUPTYPE_BQ + "' and State ='" + BQ.HANGUSTATE_ON + "' and ContNo ='" + mLPEdorItemSchema.getContNo() + "'";
			LCContHangUpStateDB tLCContHangUpStateDB = new LCContHangUpStateDB();
			LCContHangUpStateSchema tLCContHangUpStateSchema=new LCContHangUpStateSchema();
			LCContHangUpStateSet tLCContHangUpStateSet = tLCContHangUpStateDB.executeQuery(SQL);
			if (tLCContHangUpStateSet.size() != 1) {
				CError tError = new CError();
				tError.moduleName = "PEdorFCConfirmBL";
				tError.functionName = "prepareData";
				tError.errorMessage = "没有查询到已经挂起保单信息！";
				this.mErrors.addOneError(tError);
			}
			tLCContHangUpStateSchema = tLCContHangUpStateSet.get(1).getSchema();
			tLCContHangUpStateSchema.setState(BQ.HANGUSTATE_OFF);
			tLCContHangUpStateSchema.setModifyDate(PubFun.getCurrentDate());
			tLCContHangUpStateSchema.setModifyTime(PubFun.getCurrentTime());
			tMap.put(tLCContHangUpStateSchema, SysConst.UPDATE);

			return tMap;
		} catch (Exception e) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorRFConfirmBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理错误! " + e.getMessage();
			this.mErrors.addOneError(tError);
			return null;
		}
    }

    private void updateEdorState()
    {
        map.put("  update LPEdorItem "
                + "set edorState = '" + BQ.EDORSTATE_CONFIRM
                + "',   operator = '" + mGlobalInput.Operator
                + "',  modifyDate = '" + PubFun.getCurrentDate()
                + "',  modifyTime = '" + PubFun.getCurrentTime()
                + "' where edorNo = '" + mLPEdorItemSchema.getEdorNo()
                + "'   and edorType = '" + mLPEdorItemSchema.getEdorType()
                + "'   and contNo = '" + mLPEdorItemSchema.getContNo() + "' ",
                "UPDATE");
    }

    private LPContSchema getLPCont()
    {
        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPContDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPContDB.setContNo(mLPEdorItemSchema.getContNo());
        LPContSet tLPContSet = tLPContDB.query();

        if (tLPContSet.size() == 0)
        {
            CError.buildErr(this, "查询个人保单数据失败！");
            return null;
        }

        return tLPContSet.get(1);
    }
    
    private LCContSchema getLCCont()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
        LCContSet tLCContSet = tLCContDB.query();

        if (tLCContSet.size() == 0)
        {
            CError.buildErr(this, "查询个人保单数据失败！");
            return null;
        }

        return tLCContSet.get(1);
    }

    private LPInsuredSet getLPInsured()
    {
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        tLPInsuredDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPInsuredDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPInsuredDB.setContNo(mLPEdorItemSchema.getContNo());
        LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();
        if (tLPInsuredSet.size() == 0)
        {
            CError.buildErr(this, "查询个人保单被保险人数据失败！");
        }

        return tLPInsuredSet;
    }

    private LPPolSet getLPPol(LPInsuredSchema schema)
    {
        if (mLPPolDB == null)
        {
            mLPPolDB = new LPPolDB();
        }
        mLPPolDB.setEdorNo(schema.getEdorNo());
        mLPPolDB.setEdorType(schema.getEdorType());
        mLPPolDB.setContNo(schema.getContNo());
        mLPPolDB.setInsuredNo(schema.getInsuredNo());
        LPPolSet set = mLPPolDB.query();
        if (set.size() == 0)
        {
            mErrors.addOneError("查询被保人" + schema.getName() + "失败");
        }
        return set;
    }

    private int getLCPolCount(LPInsuredSchema schema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select count(1) ")
                .append("from LCPol ")
                .append("where contNo = '").append(schema.getContNo())
                .append("'   and insuredNo = '").append(schema.getInsuredNo())
                .append("' ");
        if (mExeSQL == null)
        {
            mExeSQL = new ExeSQL();
        }
        String polCount = mExeSQL.getOneValue(sql.toString());
        if (polCount.equals("") || polCount.equals("null"))
        {
            return 0;
        }
        return Integer.parseInt(polCount);
    }

    private int getLCInsuredCount(LPContSchema schema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select count(1) ")
                .append("from LCInsured ")
                .append("where contNo = '").append(schema.getContNo())
                .append("' ");
        if (mExeSQL == null)
        {
            mExeSQL = new ExeSQL();
        }
        String insuredCount = mExeSQL.getOneValue(sql.toString());
        if (insuredCount.equals("") || insuredCount.equals("null"))
        {
            return 0;
        }
        return Integer.parseInt(insuredCount);
    }

    private boolean validateEdorData()
    {

    	ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,
                mLPEdorItemSchema.getEdorNo(), mLPEdorItemSchema.getEdorType(),
                mLPEdorItemSchema.getContNo(), "ContNo");
        if(CommonBL.hasBonusRisk(mLPEdorItemSchema.getContNo())){
        	String[] tables1 =
            {"LCInsureAcc", "LCInsureAccClass"};
            if (!validate.changeData(tables1))
            {
                mErrors.copyAllErrors(validate.mErrors);
                return false;
            }
        }else{
        	String[] tables1 =
            {"LCInsureAcc", "LCInsureAccClass", "LCInsureAccFee",
            "LCInsureAccClassFee"};
            if (!validate.changeData(tables1))
            {
                mErrors.copyAllErrors(validate.mErrors);
                return false;
            }
        }        
        MMap tmap = validate.getMap();
        map.add(tmap);
        String[] tables2 =
                {"LCInsureAccTrace", "LCInsureAccFeeTrace"};
        validate = new ValidateEdorData2(mGlobalInput,
                                         mLPEdorItemSchema.getEdorNo(),
                                         mLPEdorItemSchema.getEdorType(),
                                         mLPEdorItemSchema.getContNo(),
                                         "ContNo");
        if (!validate.addData(tables2))
        {
            mErrors.copyAllErrors(validate.mErrors);
            return false;
        }
        map.add(validate.getMap());
        return true;
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo("20061102000023");

        VData data = new VData();
        data.add(gi);
        data.add(tLPEdorItemDB.query().get(1));

        PEdorCTConfirmBL bl = new PEdorCTConfirmBL();
        if (!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        } else
        {
            System.out.println("All OK");
        }
    }
}
