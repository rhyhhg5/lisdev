package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.LJFIGetBL;

/**
 * <p>Title: 保全业务处理类</p>
 * <p>Description: 使财务数据生效，把应收应付转为实收实付</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class ValidateFinanceData {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private ExeSQL mExeSQL = new ExeSQL();

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全项目类型 */
    private String mEdorType = null;

    /** 财务交费类型，"10"是保全个单，"3"是保全团单 */
    private String mContType = null;

    private String mManageCom = null;

    private String mApproveCode = null;

    private String mApproveDate = null;

    private String mAgentCode = null;

    private String mAgentGroup = null;

    private String mAgentCom = null;

    private String mAgentType = null;

    private String mAppntNo = null;

    private String mContNo=null;
    
    private String mSalechnl = null;


    /** 收费记录号 */
    private String mPayNo = null;

    /** 是否是定期结算的保单，白条类型 */
    private boolean mBT = false;

    private LJAPaySchema mLJAPaySchema = null;

    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     * @param contType String
     */
    public ValidateFinanceData(GlobalInput gi, String edorNo, String contType) {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mContType = contType;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData() {
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 得到待提交的数据
     * @return MMap
     */
    public MMap getMap() {
        return mMap;
    }

    public LJAPaySchema getLJAPay() {
        return mLJAPaySchema;
    }


    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData() {

        if(!getObjectVar())
        {
            return false;
        }

        int ret = queryLCGrpBalPlan();
        if (ret == -1) { //查询定期结算计划出错
            return false;
        }
        if (ret == 0) { //不存在定期结算计划或者为随时结算
            mBT = false;
        }
//        if (ret == 2) {
//            return false; //此合同正在定期结算不能进行保全操作
//        }
        if (ret == 1) { //定期结算处理
            mBT = true;
        }

        double sumGetMoney = CommonBL.getSumGetMoney(mEdorNo);
        //万能补交保费标记
        String tSql="select count(*) from LPEdorEspecialData where edorno='"+this.mEdorNo+"' and edortype='ZB' and DetailType='BF'";
        ExeSQL tExeSQL=new ExeSQL();
        String tDetailType=tExeSQL.getOneValue(tSql);
         if("RV".equals(this.mEdorType)){
	        	if (!setRVLJAPay())// 如果是万能补交保费特殊处理
				{
					return false;
				}
	        }else{
        //0也当作收费
        if(sumGetMoney > -0.0001)
        {
        	if(tDetailType.equals("0")){
        		if(!setLJAPay())
                {
                    return false;
                }
        	}else{
        		if(!setBFLJAPay())//如果是万能补交保费特殊处理
                {
                    return false;
                }
        	}

        }
        else
        {
            if(!setLJAGet())
            {
                return false;
            }
        }
        if(tDetailType.equals("0")){//如果不是万能补交保费正常处理，如果是万能补交保费在setBFLJAPay()中进行处理
        	setLJAGetEndorse();
        }
   }
        return true;
    }
    private boolean setRVLJAPay() {
		LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
		tLPEdorEspecialDataDB.setEdorNo(mEdorNo);
		tLPEdorEspecialDataDB.setEdorType(mEdorType);
		LPEdorEspecialDataSet tLPEdorEspecialDataSet = new LPEdorEspecialDataSet();
		tLPEdorEspecialDataSet = (LPEdorEspecialDataSet) tLPEdorEspecialDataDB
				.query();
		double rate = 0.0;
		double tAppMoney = 0.0;
		String tValaibleDate = "";
		double tLoanMoney = 0.0;
		for (int i = 1; i <= tLPEdorEspecialDataSet.size(); i++) {
			if (tLPEdorEspecialDataSet.get(i).getDetailType()
					.equals("RV_R")) {
				rate = Double.parseDouble(tLPEdorEspecialDataSet.get(i)
						.getEdorValue());
			}
			if (tLPEdorEspecialDataSet.get(i).getDetailType().equals(
					"RV_APPMONEY")) {
				tAppMoney = Double.parseDouble(tLPEdorEspecialDataSet
						.get(i).getEdorValue());
			}
			if (tLPEdorEspecialDataSet.get(i).getDetailType()
					.equals("RV_D")) {
				tValaibleDate = tLPEdorEspecialDataSet.get(i)
						.getEdorValue();
			}
			if (tLPEdorEspecialDataSet.get(i).getDetailType().equals(
					"RV_LOANMONEY")) {
				tLoanMoney = Double.parseDouble(tLPEdorEspecialDataSet.get(
						i).getEdorValue());
			}
		}
		
		LJSGetEndorseSet tLJSGetEndorseSet = getLJSGetEndorseSet();
		System.out.println(tLJSGetEndorseSet.size());
		if (tLJSGetEndorseSet.size() == 0) {
			CError tError = new CError();
			tError.moduleName = "ValidateFinanceData";
			tError.functionName = "setRVLJAPay";
			tError.errorMessage = "查询批改不退费失败，不能进行保全操作!";
			this.mErrors.addOneError(tError);
			return false;
		}		
		LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
		LJSGetEndorseSchema tLJSGetEndorseSchema2 = new LJSGetEndorseSchema();
		for (int j = 1; j <= tLJSGetEndorseSet.size(); j++) {
			if ("BF".equals(tLJSGetEndorseSet.get(j).getFeeFinaType())) {
				tLJSGetEndorseSchema = tLJSGetEndorseSet.get(j);
			}
			if ("ZF".equals(tLJSGetEndorseSet.get(j).getFeeFinaType())) {
				tLJSGetEndorseSchema2 = tLJSGetEndorseSet.get(j);
			}
		}
		Reflections ref = new Reflections();
		LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
		if(tLoanMoney>0&&tAppMoney>0){			
			LJSPaySchema tLJSPaySchema = getLJSPay();
			if (tLJSPaySchema == null) {
				CError tError = new CError();
				tError.moduleName = "ValidateFinanceData";
				tError.functionName = "setBFLJAPay";
				tError.errorMessage = "查询应收总表失败，不能进行保全操作!";
				this.mErrors.addOneError(tError);
				return false;
			}
			String tSql = "select paycount from ljspayperson where getnoticeno='"
					+ tLJSPaySchema.getGetNoticeNo()
					+ "' group by paycount order by paycount";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = tExeSQL.execSQL(tSql);
			System.out.println(tSSRS.getMaxRow());
			if (tSSRS.getMaxRow() < 1) {
				CError tError = new CError();
				tError.moduleName = "ValidateFinanceData";
				tError.functionName = "setBFLJAPay";
				tError.errorMessage = "查询应收个人缴费表失败，不能进行保全操作!";
				this.mErrors.addOneError(tError);
				return false;
			}

			LJAPaySet tLJAPaySet = new LJAPaySet();
			LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
			
			// 用于后面删除应收的set
			LJSPayPersonSet ttLJSPayPersonSet = new LJSPayPersonSet();
		
			// 按缴费期数生成实收数据，每一期实收对应一个payno，生成n条ljapayperson,1条ljspay,1条ljagetendorse
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				LJAPaySchema tLJAPaySchema = new LJAPaySchema();

				double tSumMoney = 0;
				String tPayNo = PubFun1.CreateMaxNo("PAYNO", null);

				String tPayCount = tSSRS.GetText(i, 1);
				LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
				tLJSPayPersonDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
				tLJSPayPersonDB.setPayCount(tPayCount);
				LJSPayPersonSet tLJSPayPersonSet = tLJSPayPersonDB.query();
				for (int j = 1; j <= tLJSPayPersonSet.size(); j++) {
					LJSPayPersonSchema tLJSPayPersonSchema = tLJSPayPersonSet
							.get(j);
					ttLJSPayPersonSet.add(tLJSPayPersonSchema);
					LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
					ref.transFields(tLJAPayPersonSchema, tLJSPayPersonSchema);
					tLJAPayPersonSchema.setPayNo(tPayNo);
					if (tLJAPayPersonSchema.getEnterAccDate() == null) {
						tLJAPayPersonSchema.setEnterAccDate(mCurrentDate);
					}
					tLJAPayPersonSchema.setConfDate(mCurrentDate);
					tLJAPayPersonSchema.setMakeDate(mCurrentDate);
					tLJAPayPersonSchema.setMakeTime(mCurrentTime);
					tLJAPayPersonSchema.setModifyDate(mCurrentDate);
					tLJAPayPersonSchema.setModifyTime(mCurrentTime);
					tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
					tSumMoney += tLJAPayPersonSchema.getSumDuePayMoney();
					tLJAPayPersonSet.add(tLJAPayPersonSchema);
				}
				ref.transFields(tLJAPaySchema, tLJSPaySchema);
				tLJAPaySchema.setPayNo(tPayNo);
				tLJAPaySchema.setSumActuPayMoney(tSumMoney);
				tLJAPaySchema.setIncomeNo(tLJSPaySchema.getOtherNo());
				tLJAPaySchema.setIncomeType(tLJSPaySchema.getOtherNoType());
				tLJAPaySchema.setEnterAccDate(mCurrentDate);
				tLJAPaySchema.setConfDate(mCurrentDate);
				tLJAPaySchema.setMakeDate(mCurrentDate);
				tLJAPaySchema.setMakeTime(mCurrentTime);
				tLJAPaySchema.setModifyDate(mCurrentDate);
				tLJAPaySchema.setModifyTime(mCurrentTime);
				tLJAPaySet.add(tLJAPaySchema);
				
				LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
				ref.transFields(tLJAGetEndorseSchema, tLJSGetEndorseSchema);
				tLJAGetEndorseSchema.setActuGetNo(tPayNo);
				tLJAGetEndorseSchema.setGetMoney(tSumMoney);
				tLJAGetEndorseSet.add(tLJAGetEndorseSchema);
				
			}
			
			//	
			String tPayNo = PubFun1.CreateMaxNo("PAYNO", null);
			LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
			ref.transFields(tLJAGetEndorseSchema, tLJSGetEndorseSchema2);
			tLJAGetEndorseSchema.setActuGetNo(tPayNo);
			tLJAGetEndorseSet.add(tLJAGetEndorseSchema);

			LJAPaySchema tLJAPaySchema = new LJAPaySchema();
			ref.transFields(tLJAPaySchema, tLJSPaySchema);
			tLJAPaySchema.setPayNo(tPayNo);
			tLJAPaySchema.setSumActuPayMoney(tLJSPaySchema.getSumDuePayMoney()
					- tLJSGetEndorseSchema.getGetMoney());
			tLJAPaySchema.setIncomeNo(tLJSPaySchema.getOtherNo());
			tLJAPaySchema.setIncomeType(tLJSPaySchema.getOtherNoType());
			tLJAPaySchema.setEnterAccDate(mCurrentDate);
			tLJAPaySchema.setConfDate(mCurrentDate);
			tLJAPaySchema.setMakeDate(mCurrentDate);
			tLJAPaySchema.setMakeTime(mCurrentTime);
			tLJAPaySchema.setModifyDate(mCurrentDate);
			tLJAPaySchema.setModifyTime(mCurrentTime);
			tLJAPaySet.add(tLJAPaySchema);

			mMap.put(ttLJSPayPersonSet, "DELETE");
			mMap.put(tLJAPayPersonSet, "DELETE&INSERT");
			mMap.put(tLJSPaySchema, "DELETE");
			mMap.put(tLJAPaySet, "DELETE&INSERT");
			mMap.put(tLJSGetEndorseSet, "DELETE");
			mMap.put(tLJAGetEndorseSet, "DELETE&INSERT");
		}else{
			if(tLoanMoney>0){
				LJSPaySchema tLJSPaySchema = getLJSPay();
				if (tLJSPaySchema == null) {
					CError tError = new CError();
					tError.moduleName = "ValidateFinanceData";
					tError.functionName = "setBFLJAPay";
					tError.errorMessage = "查询应收总表失败，不能进行保全操作!";
					this.mErrors.addOneError(tError);
					return false;
				}
				String tSql = "select paycount from ljspayperson where getnoticeno='"
						+ tLJSPaySchema.getGetNoticeNo()
						+ "' group by paycount order by paycount";
				ExeSQL tExeSQL = new ExeSQL();
				SSRS tSSRS = tExeSQL.execSQL(tSql);
				System.out.println(tSSRS.getMaxRow());
				if (tSSRS.getMaxRow() < 1) {
					CError tError = new CError();
					tError.moduleName = "ValidateFinanceData";
					tError.functionName = "setBFLJAPay";
					tError.errorMessage = "查询应收个人缴费表失败，不能进行保全操作!";
					this.mErrors.addOneError(tError);
					return false;
				}

				LJAPaySet tLJAPaySet = new LJAPaySet();
				LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
				
				// 用于后面删除应收的set
				LJSPayPersonSet ttLJSPayPersonSet = new LJSPayPersonSet();
			
				// 按缴费期数生成实收数据，每一期实收对应一个payno，生成n条ljapayperson,1条ljspay,1条ljagetendorse
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					LJAPaySchema tLJAPaySchema = new LJAPaySchema();

					double tSumMoney = 0;
					String tPayNo = PubFun1.CreateMaxNo("PAYNO", null);

					String tPayCount = tSSRS.GetText(i, 1);
					LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
					tLJSPayPersonDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
					tLJSPayPersonDB.setPayCount(tPayCount);
					LJSPayPersonSet tLJSPayPersonSet = tLJSPayPersonDB.query();
					for (int j = 1; j <= tLJSPayPersonSet.size(); j++) {
						LJSPayPersonSchema tLJSPayPersonSchema = tLJSPayPersonSet
								.get(j);
						ttLJSPayPersonSet.add(tLJSPayPersonSchema);
						LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
						ref.transFields(tLJAPayPersonSchema, tLJSPayPersonSchema);
						tLJAPayPersonSchema.setPayNo(tPayNo);
						if (tLJAPayPersonSchema.getEnterAccDate() == null) {
							tLJAPayPersonSchema.setEnterAccDate(mCurrentDate);
						}
						tLJAPayPersonSchema.setConfDate(mCurrentDate);
						tLJAPayPersonSchema.setMakeDate(mCurrentDate);
						tLJAPayPersonSchema.setMakeTime(mCurrentTime);
						tLJAPayPersonSchema.setModifyDate(mCurrentDate);
						tLJAPayPersonSchema.setModifyTime(mCurrentTime);
						tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
						tSumMoney += tLJAPayPersonSchema.getSumDuePayMoney();
						tLJAPayPersonSet.add(tLJAPayPersonSchema);
					}
					ref.transFields(tLJAPaySchema, tLJSPaySchema);
					tLJAPaySchema.setPayNo(tPayNo);
					tLJAPaySchema.setSumActuPayMoney(tSumMoney);
					tLJAPaySchema.setIncomeNo(tLJSPaySchema.getOtherNo());
					tLJAPaySchema.setIncomeType(tLJSPaySchema.getOtherNoType());
					tLJAPaySchema.setEnterAccDate(mCurrentDate);
					tLJAPaySchema.setConfDate(mCurrentDate);
					tLJAPaySchema.setMakeDate(mCurrentDate);
					tLJAPaySchema.setMakeTime(mCurrentTime);
					tLJAPaySchema.setModifyDate(mCurrentDate);
					tLJAPaySchema.setModifyTime(mCurrentTime);
					tLJAPaySet.add(tLJAPaySchema);
					
					LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
					ref.transFields(tLJAGetEndorseSchema, tLJSGetEndorseSchema);
					tLJAGetEndorseSchema.setActuGetNo(tPayNo);
					tLJAGetEndorseSchema.setGetMoney(tSumMoney);
					tLJAGetEndorseSet.add(tLJAGetEndorseSchema);
					
				}
				String tPayNo = PubFun1.CreateMaxNo("PAYNO", null);
				LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
				ref.transFields(tLJAGetEndorseSchema, tLJSGetEndorseSchema2);
				tLJAGetEndorseSchema.setActuGetNo(tPayNo);
				tLJAGetEndorseSet.add(tLJAGetEndorseSchema);
				mMap.put(tLJSGetEndorseSet, "DELETE");
				mMap.put(tLJAGetEndorseSet, "DELETE&INSERT");
				mMap.put(ttLJSPayPersonSet, "DELETE");
				mMap.put(tLJAPayPersonSet, "DELETE&INSERT");
				mMap.put(tLJSPaySchema, "DELETE");
				mMap.put(tLJAPaySet, "DELETE&INSERT");				
				
			}else{
				String tPayNo = PubFun1.CreateMaxNo("PAYNO", null);
				LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
				ref.transFields(tLJAGetEndorseSchema, tLJSGetEndorseSchema2);
				tLJAGetEndorseSchema.setActuGetNo(tPayNo);
				tLJAGetEndorseSet.add(tLJAGetEndorseSchema);
				
				LJAGetEndorseSchema tLJAGetEndorseSchema2 = new LJAGetEndorseSchema();
				ref.transFields(tLJAGetEndorseSchema2, tLJSGetEndorseSchema);
				tLJAGetEndorseSchema.setActuGetNo(tPayNo);
				tLJAGetEndorseSchema.setGetMoney("0.0");
				tLJAGetEndorseSet.add(tLJAGetEndorseSchema2);
				mMap.put(tLJSGetEndorseSet, "DELETE");
				mMap.put(tLJAGetEndorseSet, "DELETE&INSERT");
			}
			
		}
		
		return true;
	}

    /**
     * 查询实例便利的值
     * 主要是生成实付应付所需的保单变量
     * @return boolean
     */
    private boolean getObjectVar()
    {
        String sql = "";
        if(BQ.NOTICETYPE_P.equals(mContType))
        {
            String EdorType = mExeSQL.getOneValue(" select EdorType From LPedorItem where edorno ='"+this.mEdorNo+"'");
            if(!EdorType.equals("RB")&&!EdorType.equals(BQ.EDORTYPE_TB))
            {
                sql = "select a.* from LCCont a, LPEdorItem b "
                      + "where a.ContNo = b.ContNo "
                      + "   and b.EdorNo = '" + this.mEdorNo + "' ";
                LCContSet set = new LCContDB().executeQuery(sql);
                if (set.size() == 0) {
                    CError tError = new CError();
                    tError.moduleName = "ValidateFinanceData";
                    tError.functionName = "getObjectVar";
                    tError.errorMessage = "生成财务数据时没有查询到保单信息";
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                    return false;
                }
                mManageCom = set.get(1).getManageCom();
                mApproveCode = set.get(1).getApproveCode();
                mApproveDate = set.get(1).getApproveDate();
                mAgentCode = set.get(1).getAgentCode();
                mAgentGroup = set.get(1).getAgentGroup();
                mAgentCom = set.get(1).getAgentCom();
                mAgentType = set.get(1).getAgentGroup();
                mAppntNo = set.get(1).getAppntNo();
                mContNo=set.get(1).getContNo();
                mSalechnl = set.get(1).getSaleChnl();
                
            }else{
                sql = "select a.ManageCom,a.ApproveCode,a.ApproveDate,a.AgentCode,a.AgentGroup,a.AgentCom,a.AgentType,a.AppntNo from LCCont a, LPEdorItem b "
                      + "where a.ContNo = b.ContNo "
                      + "   and b.EdorNo = '" + this.mEdorNo + "' "
                      +" union "
                      +" select a.ManageCom,a.ApproveCode,a.ApproveDate,a.AgentCode,a.AgentGroup,a.AgentCom,a.AgentType,a.AppntNo from LBCont a, LPEdorItem b "
                      + " where a.ContNo = b.ContNo "
                      + "   and b.EdorNo = '" + this.mEdorNo + "' "
                      ;
//                LCContSet set = new LCContDB().executeQuery(sql);
                SSRS set = new SSRS();
                set = mExeSQL.execSQL(sql);
                if (set.getMaxRow() == 0) {
                    CError tError = new CError();
                    tError.moduleName = "ValidateFinanceData";
                    tError.functionName = "getObjectVar";
                    tError.errorMessage = "生成财务数据时没有查询到保单信息";
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                    return false;
                }
                mManageCom = set.GetText(1,1);
                mApproveCode = set.GetText(1,2);
                mApproveDate = set.GetText(1,3);
                mAgentCode = set.GetText(1,4);
                mAgentGroup = set.GetText(1,5);
                mAgentCom = set.GetText(1,6);
                mAgentType = set.GetText(1,7);
                mAppntNo = set.GetText(1,8);
            }
        }
        else
        {
            sql = "select a.* from LCGrpCont a, LPGrpEdorItem b "
                  + "where a.GrpContNo = b.GrpContNo "
                  + "   and b.EdorNo = '" + this.mEdorNo + "' ";

            LCGrpContSet set = new LCGrpContDB().executeQuery(sql);
            if(set.size() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "ValidateFinanceData";
                tError.functionName = "getObjectVar";
                tError.errorMessage = "生成财务数据时没有查询到保单信息";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            mManageCom = set.get(1).getManageCom();
            mApproveCode = set.get(1).getApproveCode();
            mApproveDate = set.get(1).getApproveDate();
            mAgentCode = set.get(1).getAgentCode();
            mAgentGroup = set.get(1).getAgentGroup();
            mAgentCom = set.get(1).getAgentCom();
            mAgentType = set.get(1).getAgentGroup();
            mAppntNo = set.get(1).getAppntNo();
        }

        return true;
    }

    /**
     * 生成实收号
     * @return String
     */
    private String createPayNo() {
        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setIncomeNo(mEdorNo);
        tLJAPayDB.setIncomeType(mContType);
        LJAPaySet tLJAPaySet = tLJAPayDB.query();
        if (tLJAPaySet.size() == 0) {
            return PubFun1.CreateMaxNo("PAYNO", null);
        } else {
            return tLJAPaySet.get(1).getPayNo();
        }
    }

    /**
     * 得到应收信息
     * @return LJSPaySet
     */
    private LJSPaySchema getLJSPay() {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(mEdorNo);
        tLJSPayDB.setOtherNoType(mContType);
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() == 0) {
            return null;
        }
        return tLJSPaySet.get(1);
    }

    /**
     * 得到实付信息
     * @return LJAGetSchema
     */
    private LJAGetSchema getLJAGet() {
        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setOtherNo(mEdorNo);
        tLJAGetDB.setOtherNoType(mContType);
        LJAGetSet tLJAGetSet = tLJAGetDB.query();
        if (tLJAGetSet.size() == 0) {
            return null;
        }
        return tLJAGetSet.get(1);
    }


    /**
     * 保全确认时把应转为实收
     * 若非定期结算，则将实付修改为总保费
     */
    private boolean setLJAGet()
    {
        LJSGetEndorseSet tLJSGetEndorseSet = getLJSGetEndorseSet();
        if (tLJSGetEndorseSet.size() == 0)
        {
            return true;
        }

        LJAGetSchema tLJAGetSchema = getLJAGet();

        if(mBT)
        { //定期结算处理
            if(tLJAGetSchema == null)
            {
                return true;
            }

            if(!simulateGet(tLJAGetSchema))
            {
                return false;
            }
        }
        else
        {
            if(tLJAGetSchema == null)
            {
                tLJAGetSchema = new LJAGetSchema();
                tLJAGetSchema.setActuGetNo(createGetNoticeNo());
                tLJAGetSchema.setOtherNo(mEdorNo);
                tLJAGetSchema.setOtherNoType(mContType);
                tLJAGetSchema.setPayMode("1");  //现金
                tLJAGetSchema.setManageCom(mManageCom);
                tLJAGetSchema.setApproveCode(mApproveCode);
                tLJAGetSchema.setApproveDate(mApproveDate);
                tLJAGetSchema.setAgentCode(mAgentCode);
                tLJAGetSchema.setAgentGroup(mAgentGroup);
                tLJAGetSchema.setAgentCom(mAgentCom);
                tLJAGetSchema.setAgentType(mAgentType);
                tLJAGetSchema.setAppntNo(mAppntNo);
                tLJAGetSchema.setSaleChnl(mSalechnl);
                tLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
                tLJAGetSchema.setEnterAccDate(PubFun.getCurrentDate());
                tLJAGetSchema.setConfDate(PubFun.getCurrentDate());
                tLJAGetSchema.setOperator(mGlobalInput.Operator);
                tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
                tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
                tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
                 
            }

            tLJAGetSchema.setSumGetMoney(
                Math.abs(CommonBL.getSumGetMoney(mEdorNo)));
            mMap.put(tLJAGetSchema, "DELETE&INSERT");
        }

        return true;
    }

    //白条模拟付费
    private boolean simulateGet(LJAGetSchema tLJAGetSchema) {
        tLJAGetSchema = setBTLJAGet(tLJAGetSchema);
        if (tLJAGetSchema == null) {
            return false;
        }
        LJFIGetSchema tLJFIGetSchema = setBTLJFIGet(tLJAGetSchema);
        if (tLJFIGetSchema == null) {
            return false;
        }
        mMap.put(tLJFIGetSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 设置实付表，对保全来说应付表(LJSGet)没有意义，已经不用
     * @return String
     */
    private LJAGetSchema setBTLJAGet(LJAGetSchema tLJAGetSchema) {
        String CurrentDate = PubFun.getCurrentDate();
        String CurrentTime = PubFun.getCurrentTime();
        tLJAGetSchema.setPayMode("B");
        tLJAGetSchema.setModifyDate(CurrentDate);
        tLJAGetSchema.setModifyTime(CurrentTime);
        //到帐确认日期为空
        tLJAGetSchema.setEnterAccDate(CurrentDate);
        tLJAGetSchema.setConfDate(CurrentDate);
        tLJAGetSchema.setStartGetDate(CurrentDate);
        mMap.put(tLJAGetSchema, "UPDATE");
        return tLJAGetSchema;
    }

    private LJFIGetSchema setBTLJFIGet(LJAGetSchema tLJAGetSchema) {
        //1-处理财务给付表
        String CurrentDate = PubFun.getCurrentDate();
        String CurrentTime = PubFun.getCurrentTime();
        LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();

        tLJFIGetSchema.setActuGetNo(tLJAGetSchema.getActuGetNo());
        tLJFIGetSchema.setPayMode("B");
        tLJFIGetSchema.setEnterAccDate(CurrentDate);
        tLJFIGetSchema.setConfDate(CurrentDate);
        tLJFIGetSchema.setConfMakeDate(CurrentDate);
        tLJFIGetSchema.setConfMakeTime(CurrentTime);

        tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
        tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
        tLJFIGetSchema.setGetMoney(tLJAGetSchema.getSumGetMoney());
        tLJFIGetSchema.setShouldDate(tLJAGetSchema.getShouldDate());
        tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
        tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
        tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
        tLJFIGetSchema.setSerialNo(tLJAGetSchema.getSerialNo());
        //          tLJFIGetSchema.setDrawer(Drawer);
        //          tLJFIGetSchema.setDrawerID(DrawerID);
        tLJFIGetSchema.setOperator(mGlobalInput.Operator);
        tLJFIGetSchema.setManageCom(mGlobalInput.ManageCom);
        //          tLJFIGetSchema.setBankCode(BankCode);
        //          tLJFIGetSchema.setChequeNo(ChequeNo);
        //          tLJFIGetSchema.setBankAccNo(BankAccNo);
        //          tLJFIGetSchema.setAccName(AccName);
        tLJFIGetSchema.setMakeDate(CurrentDate); //入机日期
        tLJFIGetSchema.setMakeTime(CurrentTime); //入机时间
        tLJFIGetSchema.setModifyDate(CurrentDate); //最后一次修改日期
        tLJFIGetSchema.setModifyTime(CurrentTime); //最后一次修改时间
        return tLJFIGetSchema;
    }

    /**
     * 保全确认时把应收转为实收
     */
    private boolean setLJAPay() {

        LJSGetEndorseSet tLJSGetEndorseSet = getLJSGetEndorseSet();
        if (tLJSGetEndorseSet.size() == 0)
        {
            return true;
        }

        LJSPaySchema tLJSPaySchema = getLJSPay();

        //若没有应收记录且是收费类型为定期结算，则直接退出
        if (tLJSPaySchema == null && mBT) {
            return true;
        }

        if (tLJSPaySchema == null)
        {
            tLJSPaySchema = new LJSPaySchema();
            tLJSPaySchema.setGetNoticeNo(PubFun1.CreateMaxNo("PAYNOTICENO", null));
            tLJSPaySchema.setOtherNo(mEdorNo);
            tLJSPaySchema.setOtherNoType(mContType);
            tLJSPaySchema.setManageCom(mManageCom);
            tLJSPaySchema.setApproveCode(mApproveCode);
            tLJSPaySchema.setApproveDate(mApproveDate);
            tLJSPaySchema.setAgentCode(mAgentCode);
            tLJSPaySchema.setAgentGroup(mAgentGroup);
            tLJSPaySchema.setAgentCom(mAgentCom);
            tLJSPaySchema.setAgentType(mAgentType);
            tLJSPaySchema.setAppntNo(mAppntNo);
            tLJSPaySchema.setRiskCode(BQ.FILLDATA);
            tLJSPaySchema.setPayDate(PubFun.getCurrentDate());
            tLJSPaySchema.setOperator(mGlobalInput.Operator);
            tLJSPaySchema.setMakeDate(PubFun.getCurrentDate());
            tLJSPaySchema.setMakeTime(PubFun.getCurrentTime());
            tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
            tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
        }
        
        ExeSQL tExeSQL = new ExeSQL();
        String sql = null;
        if(mContType.equals(BQ.NOTICETYPE_P))
        {
        	sql = "select ContNo from LPEdorItem where EdorNo = '" + mEdorNo + "' ";
        	String tContNo = tExeSQL.getOneValue(sql);
        	sql = "select LF_SaleChnl('1', '" + tContNo + "'),'1' from dual where 1=1";
        }
        else
        {
        	sql = "select GrpContNo from LPGrpEdorItem where EdorNo = '" + mEdorNo + "' ";
        	String tGrpContNo = tExeSQL.getOneValue(sql);
        	sql = "select LF_SaleChnl('2', '"+ tGrpContNo+"'),LF_MarketType('"+ tGrpContNo+"') from dual where 1=1";
        }
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sql);
        String tSaleChnl = tSSRS.GetText(1, 1);
        String tMarketType = tSSRS.GetText(1, 2);
        tLJSPaySchema.setSaleChnl(tSaleChnl);
        tLJSPaySchema.setMarketType(tMarketType);

        this.mLJAPaySchema = new LJAPaySchema();
        Reflections ref = new Reflections();
        ref.transFields(mLJAPaySchema, tLJSPaySchema);

        this.mPayNo = createPayNo();
        mLJAPaySchema.setPayNo(mPayNo);
        mLJAPaySchema.setIncomeNo(tLJSPaySchema.getOtherNo());
        if (this.mBT == true) {
            //若是定期结算，则余额抵扣部分先不放在LJAPay
            mLJAPaySchema.setIncomeType("B");
            mLJAPaySchema.setSumActuPayMoney(CommonBL.getSumGetMoney(mEdorNo, true));
        } else {
            mLJAPaySchema.setIncomeType(tLJSPaySchema.getOtherNoType());
            mLJAPaySchema.setSumActuPayMoney(CommonBL.getSumGetMoney(mEdorNo));
        }
//        mLJAPaySchema.setSumActuPayMoney(tLJSPaySchema.getSumDuePayMoney());
        if (this.mBT == true) {
            mLJAPaySchema.setEnterAccDate(mCurrentDate);
        } else {
            mLJAPaySchema.setEnterAccDate(CommonBL.getEnterAccDate(mEdorNo));
        }

        //没有暂收，通过余额抵扣
        if(mLJAPaySchema.getEnterAccDate() == null)
        {
            mLJAPaySchema.setEnterAccDate(mCurrentDate);
        }
        mLJAPaySchema.setConfDate(mCurrentDate);
        mLJAPaySchema.setOperator(mGlobalInput.Operator);
        mLJAPaySchema.setMakeDate(mCurrentDate);
        mLJAPaySchema.setMakeTime(mCurrentTime);
        mLJAPaySchema.setModifyDate(mCurrentDate);
        mLJAPaySchema.setModifyTime(mCurrentTime);
        
        mLJAPaySchema.setMarketType(tMarketType);
        mLJAPaySchema.setSaleChnl(tSaleChnl);
        
        //add by fuxin 确认暂收
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
        tLJTempFeeDB.setTempFeeNo(mLJAPaySchema.getGetNoticeNo());
        tLJTempFeeSet = tLJTempFeeDB.query();
        if (tLJTempFeeSet.size()!= 0)
        {
        	for (int i = 1 ; i <= tLJTempFeeSet.size() ; i++ )
            {
                LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(i);
                tLJTempFeeSchema.setConfDate(mCurrentDate);
                tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
                tLJTempFeeSchema.setModifyDate(mCurrentDate);
                tLJTempFeeSchema.setModifyTime(mCurrentTime);
                mMap.put(tLJTempFeeSchema, SysConst.UPDATE);
            }
        }
        
        ExeSQL tExeSQLLJAPay = new ExeSQL();
        String sqlLJAPay = null;
        if(mContType.equals(BQ.NOTICETYPE_P))
        {
        	sqlLJAPay = "select ContNo from LPEdorItem where EdorNo = '" + mEdorNo + "' and edortype='YS' ";
        }
        else
        {
        	sqlLJAPay = "select GrpContNo from LPGrpEdorItem where EdorNo = '" + mEdorNo + "' and edortype='YS' ";
        }
        SSRS tSSRSLJAPay = new SSRS();
        tSSRSLJAPay = tExeSQLLJAPay.execSQL(sqlLJAPay);
        //预收保费不进入实收
        if(tSSRSLJAPay.getMaxNumber()<=0)
        {
        	mMap.put(mLJAPaySchema, "DELETE&INSERT");
        }
        mMap.put(mLJAPaySchema, "DELETE&INSERT");

        //定期结算处理
        if (mBT) {
            if (!simulatePay(mLJAPaySchema)) {
                return false;
            }
        }
        return true;
    }

    //白条模拟收费
    private boolean simulatePay(LJAPaySchema tLJSPaySchema) {
        LJTempFeeClassSchema tLJTempFeeClassSchema = setBTLJTempFeeClass(
                tLJSPaySchema);
        if (tLJTempFeeClassSchema == null) {
            return false;
        }
        mMap.put(tLJTempFeeClassSchema, "DELETE&INSERT");
        LJTempFeeSchema tLJTempFeeSchema = setBTLJTempFee(tLJTempFeeClassSchema);
        if (tLJTempFeeSchema == null) {
            return false;
        }
        mMap.put(tLJTempFeeSchema, "DELETE&INSERT");
//        LJAGetEndorseSet tLJAGetEndorseSet=updateLJAGetEndorse(tLJAGetSchema);
//        if(tLJAGetEndorseSet==null)
//        {
//            return false;
//        }
//        mMap.put(tLJAGetEndorseSet, "UPDATE");


        return true;
    }

    private LJTempFeeClassSchema setBTLJTempFeeClass(LJAPaySchema tLJSPaySchema) {
        String CurrentDate = PubFun.getCurrentDate();
        String CurrentTime = PubFun.getCurrentTime();

        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(tLJSPaySchema.getGetNoticeNo());
        tLJTempFeeClassSchema.setPayMode("B");
        tLJTempFeeClassSchema.setPayDate(CurrentDate);
        String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        tLJTempFeeClassSchema.setSerialNo(serNo);
        tLJTempFeeClassSchema.setPayMoney(tLJSPaySchema.getSumActuPayMoney());
        //            tLJTempFeeClassSchema.setChequeNo(tCChequeNo[i]);
        //            tLJTempFeeClassSchema.setChequeDate(tCChequeDate[i]);
        tLJTempFeeClassSchema.setEnterAccDate(CurrentDate);
        tLJTempFeeClassSchema.setConfDate(CurrentDate);
        tLJTempFeeClassSchema.setConfMakeDate(CurrentDate);
        tLJTempFeeClassSchema.setConfMakeTime(CurrentTime);
        tLJTempFeeClassSchema.setManageCom(mGlobalInput.ManageCom);
        //            tLJTempFeeClassSchema.setBankCode(tBankcode[i]);
        //            tLJTempFeeClassSchema.setBankAccNo(tBankaccno[i]);
        //            tLJTempFeeClassSchema.setAccName(tAccname[i]);
        //            tLJTempFeeClassSchema.setInsBankCode(tInsBankcode[i]);

        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeClassSchema.setMakeDate(CurrentDate);
        tLJTempFeeClassSchema.setMakeTime(CurrentTime);
        tLJTempFeeClassSchema.setModifyDate(CurrentDate);
        tLJTempFeeClassSchema.setModifyTime(CurrentTime);
        return tLJTempFeeClassSchema;
    }

    private LJTempFeeSchema setBTLJTempFee(LJTempFeeClassSchema
                                           tLJTempFeeClassSchema) {
        String CurrentDate = PubFun.getCurrentDate();
        String CurrentTime = PubFun.getCurrentTime();

        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setTempFeeNo(tLJTempFeeClassSchema.getTempFeeNo());
        tLJTempFeeSchema.setTempFeeType("7");
        tLJTempFeeSchema.setRiskCode("000000");
//            tLJTempFeeSchema.setAgentGroup(tAgentGroup[i]);
//            tLJTempFeeSchema.setAgentCode(tAgentCode[i]);
        tLJTempFeeSchema.setPayDate(CurrentDate);
        tLJTempFeeSchema.setEnterAccDate(CurrentDate); //到帐日期
        tLJTempFeeSchema.setConfDate(CurrentDate);
        tLJTempFeeSchema.setConfMakeDate(CurrentDate);
        tLJTempFeeSchema.setConfMakeTime(CurrentTime);
        tLJTempFeeSchema.setPayMoney(tLJTempFeeClassSchema.getPayMoney());
        tLJTempFeeSchema.setManageCom(mGlobalInput.ManageCom);
        tLJTempFeeSchema.setOtherNo(mEdorNo);
        tLJTempFeeSchema.setOtherNoType("3");
        tLJTempFeeSchema.setSerialNo(tLJTempFeeClassSchema.getSerialNo());
        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeSchema.setMakeDate(CurrentDate);
        tLJTempFeeSchema.setMakeTime(CurrentTime);
        tLJTempFeeSchema.setModifyDate(CurrentDate);
        tLJTempFeeSchema.setModifyTime(CurrentTime);
        return tLJTempFeeSchema;
    }

    //查询是否是定期结算的保单
    private int queryLCGrpBalPlan() {
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(mEdorNo);
        tLPGrpEdorMainDB.setEdorNo(mEdorNo);
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
        if (tLPGrpEdorMainSet == null) {
            if (tLPGrpEdorMainDB.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLPGrpEdorMainDB.mErrors);
                return -1;
            }
        }
        if (tLPGrpEdorMainSet.size() == 0) {
            return 0;
        }
        //获得团体合同号
        String grpContNo = tLPGrpEdorMainSet.get(1).getGrpContNo();

        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(grpContNo);
        LCGrpBalPlanSet tLCGrpBalPlanSet = tLCGrpBalPlanDB.query();
        //查询定期结算计划出错
        if (tLCGrpBalPlanSet == null) {
            if (tLCGrpBalPlanDB.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLCGrpBalPlanDB.mErrors);
                return -1;
            }
        }
        //不存在定期结算计划或者为随时结算
        if (tLCGrpBalPlanSet.size() == 0) {
            return 0;
        }
        LCGrpBalPlanSchema tLCGrpBalPlanSchema = tLCGrpBalPlanSet.get(1);
//        if (!tLCGrpBalPlanSchema.getState().equals("0")) {
//            CError tError = new CError();
//            tError.moduleName = "ValidateFinanceData";
//            tError.functionName = "queryLCGrpBalPlan";
//            tError.errorMessage = "该保单正处于结算过程当中，不能进行保全操作!";
//            this.mErrors.addOneError(tError);
//            return 2;
//        }
        //为随时结算
        if (tLCGrpBalPlanSchema.getBalIntv() == 0) {
            return 0;
        }
        //如果选择了延期计算
        if (this.queryEspecialDate() == 1) {
            return 0;
        }
        return 1;
    }

    /**
     * 返回值1，表示即时计算。
     * @return int
     */
    private int queryEspecialDate() {
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(this.mEdorNo);
        tLPEdorEspecialDataDB.setEdorNo(this.mEdorNo);
        tLPEdorEspecialDataDB.setEdorType("DJ");
        tLPEdorEspecialDataDB.setDetailType("BALTYPE");
        LPEdorEspecialDataSet tLPEdorEspecialDataSet = tLPEdorEspecialDataDB.
                query();

        if (tLPEdorEspecialDataSet == null) {
            if (tLPEdorEspecialDataSet.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLPEdorEspecialDataSet.mErrors);
                return -1;
            }
        }
        if (tLPEdorEspecialDataSet.size() == 0) {
            return 0;
        } else if (tLPEdorEspecialDataSet.get(1).getEdorValue().equals("1")) {
            return 1;
        }
        return 0;
    }


    /**
     * 得到保全批改补退费表的数据
     * @return LJSGetEndorseSet
     */
    private LJSGetEndorseSet getLJSGetEndorseSet() {
        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
        tLJSGetEndorseDB.setEndorsementNo(mEdorNo);
        return tLJSGetEndorseDB.query();
    }

    /**
     * 保全确认时把LJSGetEndorse中的数据放到LJAGetEndorse中
     */
    private void setLJAGetEndorse() {
        String actuGetNo = getActuGetNo();
        LJSGetEndorseSet tLJSGetEndorseSet = getLJSGetEndorseSet();
        if ((actuGetNo == null) && (tLJSGetEndorseSet.size() > 0)) {
            actuGetNo = createPayNo();
        }
        if (mLJAPaySchema == null) {
            mLJAPaySchema = new LJAPaySchema();
            mLJAPaySchema.setPayNo(actuGetNo);
            mLJAPaySchema.setPayDate(mCurrentDate);
            mLJAPaySchema.setEnterAccDate(mCurrentDate);
            mLJAPaySchema.setConfDate(mCurrentDate);
        }

        for (int i = 1; i <= tLJSGetEndorseSet.size(); i++) {
            LJSGetEndorseSchema tLJSGetEndorseSchema = tLJSGetEndorseSet.get(i);
            LJAGetEndorseSchema tLJAGetEndorseSchema =
                    getLJAGetEndorse(actuGetNo, tLJSGetEndorseSchema);
            mMap.put(tLJAGetEndorseSchema, "DELETE&INSERT");
            mMap.put(tLJSGetEndorseSchema, "DELETE");
        }
    }

    /**
     * 产生退费记录号，如果原来LJAGet表中没有数据时才产生新的号
     * @return String
     */
    private String createGetNoticeNo() {
        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setOtherNo(mEdorNo);
        LJAGetSet tLJAGetSet = tLJAGetDB.query();
        if (tLJAGetSet.size() > 0) {
            return tLJAGetSet.get(1).getActuGetNo();
        } else {
            return PubFun1.CreateMaxNo("GETNO", null);
        }
    }


    /**
     * 得到LJAGetEndorse
     * @param actuGetNo String
     * @param aLJSGetEndorseSchema LJSGetEndorseSchema
     * @return LJAGetEndorseSchema
     */
    private LJAGetEndorseSchema getLJAGetEndorse(String actuGetNo,
                                                 LJSGetEndorseSchema
                                                 aLJSGetEndorseSchema) {
        String CurrentDate = PubFun.getCurrentDate();
        String CurrentTime = PubFun.getCurrentTime();
        LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLJAGetEndorseSchema, aLJSGetEndorseSchema);
        //setRealContNo(aLJSGetEndorseSchema);
        tLJAGetEndorseSchema.setActuGetNo(actuGetNo);
        tLJAGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJAGetEndorseSchema.setMakeDate(mCurrentDate);
        tLJAGetEndorseSchema.setMakeTime(mCurrentTime);
        tLJAGetEndorseSchema.setModifyDate(mCurrentDate);
        tLJAGetEndorseSchema.setModifyTime(mCurrentTime);
        if (mBT == true) {
            tLJAGetEndorseSchema.setEnterAccDate(CurrentDate);
            tLJAGetEndorseSchema.setGetConfirmDate(CurrentDate);
        }

        return tLJAGetEndorseSchema;
    }

    /**
     * 如果是增人项目要换号，仅用于增人，以后要加判断是否是增人
     * @param aLJSGetEndorseSchema LJSGetEndorseSchema
     */
    private void setRealContNo(LJSGetEndorseSchema aLJSGetEndorseSchema) {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setProposalNo(aLJSGetEndorseSchema.getPolNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() == 0) {
            return;
        }
        aLJSGetEndorseSchema.setContNo(tLCPolSet.get(1).getContNo());
        aLJSGetEndorseSchema.setPolNo(tLCPolSet.get(1).getPolNo());
    }

    /**
     * 得到实收或实付号
     * @return String
     */
    private String getActuGetNo() {
        if (mPayNo != null) { //如果有实收号
            return mPayNo;
        } else { //得到实付号
            LJAGetDB tLJAGetDB = new LJAGetDB();
            tLJAGetDB.setOtherNo(mEdorNo);
            tLJAGetDB.setOtherNoType(mContType);
            LJAGetSet tLJAGetSet = tLJAGetDB.query();
            if (tLJAGetSet.size() == 0) {
                return null;
            }
            return tLJAGetSet.get(1).getActuGetNo();
        }
    }

    /**huxl 为了测试
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit() {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    /**万能补交保费根据期数拆分应收转实收
     *
     * @return boolean
     */
    private boolean setBFLJAPay(){
        LJSGetEndorseSet tLJSGetEndorseSet = getLJSGetEndorseSet();
        if (tLJSGetEndorseSet.size() == 0)
        {
        	CError tError = new CError();
          tError.moduleName = "ValidateFinanceData";
          tError.functionName = "setBFLJAPay";
          tError.errorMessage = "查询批改不退费失败，不能进行保全操作!";
          this.mErrors.addOneError(tError);
            return false;
        }
        LJSGetEndorseSchema tLJSGetEndorseSchema=tLJSGetEndorseSet.get(1);
        LJSPaySchema tLJSPaySchema = getLJSPay();
        if(tLJSPaySchema==null){
        	CError tError = new CError();
            tError.moduleName = "ValidateFinanceData";
            tError.functionName = "setBFLJAPay";
            tError.errorMessage = "查询应收总表失败，不能进行保全操作!";
            this.mErrors.addOneError(tError);
              return false;
        }
        String tSql="select paycount from ljspayperson where getnoticeno='"+tLJSPaySchema.getGetNoticeNo()+"' group by paycount order by paycount";
        ExeSQL tExeSQL=new ExeSQL();
        SSRS tSSRS=tExeSQL.execSQL(tSql);
        if(tSSRS.getMaxRow()<1){
        	CError tError = new CError();
            tError.moduleName = "ValidateFinanceData";
            tError.functionName = "setBFLJAPay";
            tError.errorMessage = "查询应收个人缴费表失败，不能进行保全操作!";
            this.mErrors.addOneError(tError);
              return false;
        }

        LJAPaySet tLJAPaySet = new LJAPaySet();
        LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
        LJAGetEndorseSet tLJAGetEndorseSet=new LJAGetEndorseSet();
        //用于后面删除应收的set
        LJSPayPersonSet ttLJSPayPersonSet= new LJSPayPersonSet();
        Reflections ref = new Reflections();
        //按缴费期数生成实收数据，每一期实收对应一个payno，生成n条ljapayperson,1条ljspay,1条ljagetendorse
        	for(int i=1;i<=tSSRS.getMaxRow();i++){
        		LJAPaySchema tLJAPaySchema=new LJAPaySchema();
        		LJAGetEndorseSchema tLJAGetEndorseSchema=new LJAGetEndorseSchema();
        		double tSumMoney=0;
//        		String tPayNo = createPayNo();
        		String tPayNo=PubFun1.CreateMaxNo("PAYNO", null);
        		String tPayCount=tSSRS.GetText(i, 1);
        		LJSPayPersonDB tLJSPayPersonDB=new LJSPayPersonDB();
        		tLJSPayPersonDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
        		tLJSPayPersonDB.setPayCount(tPayCount);
        		LJSPayPersonSet tLJSPayPersonSet=tLJSPayPersonDB.query();
        		for(int j=1;j<=tLJSPayPersonSet.size();j++){
        			LJSPayPersonSchema tLJSPayPersonSchema=tLJSPayPersonSet.get(j);
        			ttLJSPayPersonSet.add(tLJSPayPersonSchema);
            		LJAPayPersonSchema tLJAPayPersonSchema=new LJAPayPersonSchema();
            		ref.transFields(tLJAPayPersonSchema, tLJSPayPersonSchema);
            		tLJAPayPersonSchema.setPayNo(tPayNo);
            		if(tLJAPayPersonSchema.getEnterAccDate()==null){
            			tLJAPayPersonSchema.setEnterAccDate(mCurrentDate);
            		}
            		tLJAPayPersonSchema.setConfDate(mCurrentDate);
            		tLJAPayPersonSchema.setMakeDate(mCurrentDate);
            		tLJAPayPersonSchema.setMakeTime(mCurrentTime);
            		tLJAPayPersonSchema.setModifyDate(mCurrentDate);
            		tLJAPayPersonSchema.setModifyTime(mCurrentTime);
            		tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
            		tSumMoney+=tLJAPayPersonSchema.getSumDuePayMoney();
            		tLJAPayPersonSet.add(tLJAPayPersonSchema);
        		}



            		ref.transFields(tLJAPaySchema, tLJSPaySchema);
            		tLJAPaySchema.setPayNo(tPayNo);
            		tLJAPaySchema.setSumActuPayMoney(tSumMoney);
            		tLJAPaySchema.setIncomeNo(tLJSPaySchema.getOtherNo());
            		tLJAPaySchema.setIncomeType(tLJSPaySchema.getOtherNoType());
            		tLJAPaySchema.setEnterAccDate(mCurrentDate);
            		tLJAPaySchema.setConfDate(mCurrentDate);
            		tLJAPaySchema.setMakeDate(mCurrentDate);
            		tLJAPaySchema.setMakeTime(mCurrentTime);
            		tLJAPaySchema.setModifyDate(mCurrentDate);
            		tLJAPaySchema.setModifyTime(mCurrentTime);
            		tLJAPaySet.add(tLJAPaySchema);

            		ref.transFields(tLJAGetEndorseSchema, tLJSGetEndorseSchema);
            		tLJAGetEndorseSchema.setActuGetNo(tPayNo);
            		tLJAGetEndorseSchema.setGetMoney(tSumMoney);
            		tLJAGetEndorseSet.add(tLJAGetEndorseSchema);
        	}

        mMap.put(ttLJSPayPersonSet, "DELETE");
        mMap.put(tLJAPayPersonSet, "DELETE&INSERT");
        mMap.put(tLJSPaySchema, "DELETE");
        mMap.put(tLJAPaySet, "DELETE&INSERT");
        mMap.put(tLJSGetEndorseSchema, "DELETE");
        mMap.put(tLJAGetEndorseSet, "DELETE&INSERT");
        return true;
    }

    public boolean submitDataForMain() {
        if (!dealData()) {
            return false;
        }
        if (!submit()) {
            return false;
        }
        return true;
    }


    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String args[]) {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "pa0001";
        gi.ComCode = "86";
        gi.ManageCom = "86";
        ValidateFinanceData tValidateFinanceData = new ValidateFinanceData(gi,
                "20060825000014",
                BQ.NOTICETYPE_G);
        if (!tValidateFinanceData.submitDataForMain()) {
            return;
        }
    }


}
