package com.sinosoft.lis.bq;

import java.text.DecimalFormat;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sinosoft.lis.db.LCGrpContRoadDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContRoadSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.tb.GrpDiskImport;
import com.sinosoft.lis.vschema.LCGrpContRoadSet;
import com.sinosoft.lis.vschema.LCInsuredListPolSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 调度入口（Service）
 * 
 * @author LY
 * 
 */
public class LCDFService {

	private TransferData mTransferData = new TransferData();

	private int mCurThreadCount = 0;

	public static final int Job_MaxCount = new Integer(LCGrpThreadConfig.threadMaxCount);// 线程数

	public static final int Job_MaxDataCount = new Integer(LCGrpThreadConfig.threadDataSize);// 取数数量

	private static int idx = 0;

	private String path;
	
	public CErrors mErrors = new CErrors();
	
	private boolean flag=false;
	
	private String mBatchNo;
	
	/** 节点名 */
    private String[] sheetName = {"InsuredInfo", "Ver", "RiskInfo"};
    /** 配置文件名 */
    private String configName = "GrpDiskImport.xml";
	
	/** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

	private LCDFService(String path, TransferData tTransferData) {
		this.path = path;
		this.mTransferData = tTransferData;
	}

	private boolean run() {
		int count =0;
		ExecutorService exec = null;
		String mGrpContNo = (String)this.mTransferData.getValueByName("GrpContNo");

		try {
			exec = Executors.newFixedThreadPool(LCDFService.Job_MaxCount);

					
					LCInsuredListSet tLCInsuredListSet = (LCInsuredListSet) mTransferData.getValueByName("LCInsuredListSet");
					LCInsuredListPolSet tLCInsuredListPolSet = (LCInsuredListPolSet) mTransferData.getValueByName("LCInsuredListPolSet");
					
//					// 取数
			        LCGrpDataCatchService tSrcDatas = new LCGrpDataCatchService(tLCInsuredListSet);
			        while(tSrcDatas.hasNext()){
			        	this.UpCount();
			        	List tCurDatas = tSrcDatas.getDatas(LCDFService.Job_MaxDataCount);
			        	Runnable run = new LCJobThread(String.valueOf(count), tCurDatas, this, mTransferData);
						exec.submit(run);
//
						count++;
						System.out.println("当前线程个数："+count);

						if (count >= LCDFService.Job_MaxCount) {
							boolean tFlag = true;
							while (tFlag) {
								Thread.sleep(100);
								if (count < LCDFService.Job_MaxCount) {
									tFlag = false;
								}
							}
						}
					}
			        
			        System.out.println("共运行任务：" + count);
					exec.shutdown();
					System.out.println("启动一次顺序关闭，执行以前提交的任务，但不接受新任务。");
			        while (true) {
			        	if (exec.isTerminated()) {
			        		System.out.println("所有的子线程都结束了！");
			        		break;
			        	}
			        	Thread.sleep(100);
			        }

			
		} catch (Exception e) {
			e.printStackTrace();
		} catch (Error e){
			e.printStackTrace();
		} finally {
			if (exec != null) {
				exec.shutdown();
			}
			String time=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
			String tsq = "INSERT INTO ldtimetest VALUES ('"+mTransferData.getValueByName("GrpContNo")+"',current time,'"+time+"','结束','1',NULL) ";
			new ExeSQL().execUpdateSQL(tsq);
		}

		return true;
	}
	

	public static LCDFService getService(String path, TransferData tTransferData) {
		return new LCDFService(path, tTransferData);
	}

	public boolean startService() {
		boolean tResultFlag = false;

		try {
			tResultFlag = run();
		} catch (Exception e) {
			e.printStackTrace();
			tResultFlag = false;
		}

		return tResultFlag;
	}

	public int getCurThreadCount() {
		return mCurThreadCount;
	}

	public void UpCount() {
		synchronized (this) {
			mCurThreadCount++;
		}
	}

	public void DownCount() {
		synchronized (this) {
			mCurThreadCount--;
		}
	}
	

}
