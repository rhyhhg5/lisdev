package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:被保险人资料变更功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong
 * @version 1.0
 */
public class PEdorCTDetailUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public PEdorCTDetailUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    PEdorCTDetailBL tPEdorCTDetailBL = new PEdorCTDetailBL();
    if (tPEdorCTDetailBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tPEdorCTDetailBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "PInsuredUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
      mResult = tPEdorCTDetailBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
    System.out.println("-------test...");
    LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
    LPPolSchema tLPPolSchema = new LPPolSchema();
    tLPEdorItemSchema.setEdorNo("20060313000020");
	tLPEdorItemSchema.setEdorAcceptNo(tLPEdorItemSchema.getEdorNo());
    tLPEdorItemSchema.setContNo("00001526201");
    tLPEdorItemSchema.setInsuredNo("000000");
    tLPEdorItemSchema.setPolNo("000000");
    tLPEdorItemSchema.setEdorType("CT");

    tLPPolSchema.setContNo(tLPEdorItemSchema.getContNo());
    tLPPolSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
    tLPPolSchema.setEdorType(tLPEdorItemSchema.getEdorType());
    tLPPolSchema.setInsuredNo("000015262");
    tLPPolSchema.setPolNo("21000008499");
	tLPPolSchema.setMainPolNo("21000008499");

    LPPolSet tLPPolSet=new LPPolSet();
    tLPPolSet.add(tLPPolSchema);

	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput.Operator = "endor0";
	tGlobalInput.ManageCom = "86";

    VData tVData = new VData();
    tVData.addElement(tLPEdorItemSchema);
    tVData.addElement(tLPPolSet);
	tVData.addElement(tGlobalInput);
    PEdorCTDetailUI tPEdorCTDetailUI = new PEdorCTDetailUI();
	if(!tPEdorCTDetailUI.submitData(tVData,"INSERT||MAIN"))
    {
        System.out.println(tPEdorCTDetailUI.mErrors.getErrContent());
    }
//	tPEdorCTDetailUI.submitData(tVData,"QUERY||DETAIL");
  }
}
