package com.sinosoft.lis.bq;

import com.sinosoft.lis.bq.mm.face.ICommonMM;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LIWaitSendMessageSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 这个是保单即将失效给投保人发送的 短信通知类
 * 
 * @author hyy
 * 
 */
public class ContLosted {
	// 保存即将进行电话催缴以及日期为当前日期前一天的保单号码以及通知书号码
	private SSRS ssrs1 = new SSRS();
	private SSRS ssrs2 = new SSRS();
	private GlobalInput tGI = new GlobalInput();
	
	public ContLosted(){
		tGI.AgentCom="86" ;
		//tGI.ClientIP="" ;
		tGI.ComCode="86";
		tGI.Operator="server";
		//tGI.ServerIP="";
		
	}

	public void sendMessageForLosted() {
		// 查询即将进行电话催缴任务的 并且是当前日期前一天的保单号码以及通知书号码
		String sql1 = "select a.OtherNo,a.GetNoticeNo from LJSPay a"
				+ " where otherNoType = '2' and PayDate < current date"
				+ " and payDate > current date -1 days"
				+ " and (bankOnTheWayFlag='0' or bankOnTheWayFlag is null )    and SumDuePayMoney > 0 "
				+ " and not exists (select 1 from LJTempFee  where tempFeeNo = a.getNoticeNo and ConfFlag='0')"
				+ " and getNoticeNo not in(select case when getNoticeNo is null then '' else getNoticeNo end from LGPhoneHasten)"
				+ " order by getNoticeNo";
		ssrs1 = new ExeSQL().execSQL(sql1);
		//System.out.println(ssrs1.getMaxRow());
		for (int i = 1; i <= ssrs1.getMaxRow(); i++) {
			String contNo = ssrs1.GetText(i, 1);
			String sql2 = "select distinct lca.AppntName,lca.AppntNo,lc.Email,lc.Mobile"
					+ " from LCAddress lc,lcAppnt lca"
					+ " where lca.contNo = '"
					+ contNo
					+ "'"
					+ " and lca.appntNo=lc.CustomerNo"
			        + " and lc.MakeDate=(select Max(MakeDate) from LCAddress where CustomerNO=(select AppntNo from LcAppnt where contNo='"+contNo+"'))";
			System.out.println(sql2+"888888888888888888888888888888888888888888888888888");
			ssrs2 = new ExeSQL().execSQL(sql2);
			for (int j = 1; j <= ssrs2.getMaxRow(); j++) {
				String content = "[ 尊敬的 "+ssrs2.GetText(j, 1)+" 先生/女士，您的保单 "+contNo+" 由于未按规定交费即将失效，为保证您的保障权益，请尽快拨打服务热线95518联系交费事宜.]";
                System.out.println(content);
				// 一下是发送短信的代码
				LIWaitSendMessageSchema soonSend = new LIWaitSendMessageSchema();
				soonSend.setBussNo(ssrs1.GetText(i, 2));
				soonSend.setContent(content);
				soonSend.setCustomerNo(ssrs2.GetText(j, 2));
				soonSend.setEmail(ssrs2.GetText(j, 3));
				soonSend.setFunctionCode("C");// 保单失效通知
				soonSend.setMakeDate(PubFun.getCurrentDate());
				soonSend.setMakeTime(PubFun.getCurrentTime());
				// 这里主键是怎么生成
				soonSend.setMessageId(PubFun1.CreateMaxNo("C0000", 15));
				soonSend.setMobile(ssrs2.GetText(j, 4));
				soonSend.setModelCode("BQ");
				soonSend.setModifyDate(PubFun.getCurrentDate());
				soonSend.setModifyTime(PubFun.getCurrentTime());
				soonSend.setName(ssrs2.GetText(j, 1));
				soonSend.setOperator(tGI.Operator);
				soonSend.setSendTime(PubFun.getCurrentTime());
				soonSend.setSendDate(PubFun.getCurrentDate());
				ICommonMM mm = new CommonMMImpl();
				try {
					mm.sendMMSoon(tGI, soonSend);
				} catch (Exception e) {
					e.printStackTrace() ;
				}
			}
		}

	}

	public static void main(String[] args) {
		new ContLosted().sendMessageForLosted();
	}

}
