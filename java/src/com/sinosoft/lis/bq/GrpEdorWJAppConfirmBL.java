package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LPGrpEdorItemDB;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 减人项目理算类，对录入的减人信息进行预处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.3
 */
public class GrpEdorWJAppConfirmBL implements EdorAppConfirm
{
    //理算处理的结果集合
    private VData mResult;

    public GrpEdorWJAppConfirmBL()
    {
    }

    /**
     * getResult
     * 得到理算结果
     * @return VData：理算结果集合
     * @todo Implement this com.sinosoft.lis.bq.EdorAppConfirm method
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * submitData
     * 外部操作的提交方法，进行业务处理
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     * @todo Implement this com.sinosoft.lis.bq.EdorAppConfirm method
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        GrpEdorWZAppConfirmBL bl = new GrpEdorWZAppConfirmBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        mResult = bl.getResult();

        return true;
    }

    /**
     * 调试方法
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        LPGrpEdorItemDB db = new LPGrpEdorItemDB();
        db.setEdorNo("20060710000006");

        VData data = new VData();
        data.add(g);
        data.add(db.query().get(1));

        GrpEdorWJAppConfirmBL bl = new GrpEdorWJAppConfirmBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
