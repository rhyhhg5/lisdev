package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全人工核保</p>
 * <p>Description: 体检通知书录入</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorUWManuHealthQBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPPENoticeResultSet mLPPENoticeResultSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPPENoticeResultSet = (LPPENoticeResultSet) data.
                    getObjectByObjectName("LPPENoticeResultSet", 0);
        }
        catch (Exception ex)
        {
            mErrors.addOneError("传入参数错误！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
        setPENoticeResult();
        return true;
    }

    /**
     * 设置体检通知书主表
     */
    private void setPENoticeResult()
    {
        for (int i = 1; i <= mLPPENoticeResultSet.size(); i++)
        {
            LPPENoticeResultSchema tLPPENoticeResultSchema =
                    mLPPENoticeResultSet.get(i);
            tLPPENoticeResultSchema.setOperator(mGlobalInput.Operator);
            tLPPENoticeResultSchema.setMakeDate(mCurrentDate);
            tLPPENoticeResultSchema.setMakeTime(mCurrentTime);
            tLPPENoticeResultSchema.setModifyDate(mCurrentDate);
            tLPPENoticeResultSchema.setModifyTime(mCurrentTime);
            mMap.put(mLPPENoticeResultSet, "DELETE&INSERT");
        }
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
