package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.bq.GedorNIZTDealBL;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 应收作废用户接口
 * 接受页面传入的数据，并出入后台处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author LC
 * @version 1.2
 */
public class GedorNIZTDealUI {

    public CErrors mCErrors = new CErrors();

    private VData mInputData = new VData();

    private String mOperate = "";


    public GedorNIZTDealUI() {
    }

    /**
     * 操作的提交方法，作为页面数据的入口
     * @param cInputData VData：
     * 1、LJSPaySchem：a交费信息
     * @param cOperate String：""
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mInputData = (VData) cInputData.clone();

        mOperate = cOperate;

        GedorNIZTDealBL tGedorNIZTDealBL = new GedorNIZTDealBL();

        if (!tGedorNIZTDealBL.submitData(mInputData, mOperate)) {
            this.mCErrors.copyAllErrors(tGedorNIZTDealBL.mCErrors);
            return false;
        }

        return true;
    }


    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "wuser";
        tGI.ManageCom = "86";

        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo("31000002916");

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("CancelMode", "0");
        GedorNIZTDealUI tGedorNIZTDealUI = new GedorNIZTDealUI();


        VData tVData = new VData();
        tVData.addElement(tLJSPaySchema);
        tVData.addElement(tGI);
        tVData.addElement(tTransferData);
        tGedorNIZTDealUI.submitData(tVData,"INSERT");

    }
}
