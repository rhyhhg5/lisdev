package com.sinosoft.lis.bq;
//程序名称：LMInterestRateBL.java
//程序功能：
//创建日期：2011-11-23
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.util.*;

public class LMInterestRateBL 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	
	private VData mResult = new VData();
	
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	private MMap map = new MMap();
	
	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	
	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput() ;
	
	/** 数据操作字符串 */
	private String mOperate ;
	
	/** 操作员 */
	private String mOperater ;
	
	/** 业务处理相关变量 */
	private LMInterestRateSchema mLMInterestRateSchema = new LMInterestRateSchema();
	
	private LMInterestRateSet mLMInterestRateSet = new LMInterestRateSet();
	
	
	/** 结算月份 */
//	private String mBalaMonth = null;
	
	/** 需要删除的业务处理相关变量 */
	private LMInterestRateSchema mDLMInterestRateSchema = new LMInterestRateSchema();
	
	/** 需要删除的结算月份 */
	private String mDBalaMonth = null;
	
	public LMInterestRateBL() 
	{
	}
	
	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
  public boolean submitData(VData cInputData,String cOperate)
	{
      //将操作数据拷贝到本类中
	    this.mOperate = cOperate;
	    if (!getInputData(cInputData))
	    {
	    	CError tError = new CError();
          tError.moduleName = "LMInsuAccRateBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "数据处理失败LMInsuAccRateBL-->getInputData!";
          this.mErrors.addOneError(tError);
	        return false;
	    }
	    
      //数据校验
      if (!checkData()) {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LMInterestRateBL";
          tError.functionName = "checkData";
          tError.errorMessage = "数据处理失败LMInterestRateBL-->checkData!";
          this.mErrors.addOneError(tError);
          return false;
      }
	    
	    //进行业务处理
	    if (!dealData())
	    {
	        // @@错误处理
          CError tError = new CError();
	        tError.moduleName = "LMInterestRateBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败LMInterestRateBL-->dealData!";
	        this.mErrors .addOneError(tError) ;
	        return false;
	    }

	    if (!prepareOutputData())
	    {
	        return false;
	    }
	    
	    PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "LMInterestRateBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";

          this.mErrors.addOneError(tError);
          return false;
      }
	    
	    mInputData = null;
	    return true;
	}
	 /**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean dealData()
	{
		System.out.println("进行数据处理.....");
	    if (mOperate.equals("INSERT||MAIN"))
	    {
	    	return insertData();
	    }
	    if (this.mOperate.equals("UPDATE||MAIN"))
	    {
	    	return updateData(); //删除
	    }	    
	    if (this.mOperate.equals("DELETE||MAIN"))
	    {
	    	return deleteData(); //删除
	    }
	    
	    return true;
	}
	
	 /**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
	    this.mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0);
	    this.mOperater = this.mGlobalInput.Operator;
	    
	    TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
	    this.mLMInterestRateSchema = (LMInterestRateSchema)tTransferData.getValueByName("tLMInterestRateSchema");
		this.mDLMInterestRateSchema = (LMInterestRateSchema)tTransferData.getValueByName("dLMInterestRateSchema");
		return true;
	}
	 
  private boolean prepareOutputData()
  {
      try
      {
          this.mInputData.clear();
          this.mInputData.add(this.mLMInterestRateSchema);
          this.mInputData.add(this.mDLMInterestRateSchema);
          mInputData.add(this.map);
          mResult.clear();
          mResult.add(this.mLMInterestRateSchema);
          mResult.add(this.mDLMInterestRateSchema);
      }
		catch(Exception ex)
		{
	 		// @@错误处理
			CError tError = new CError();
	 		tError.moduleName = "LMInterestRateBL";
	 		tError.functionName = "prepareOutputData";
	 		tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
	 		this.mErrors .addOneError(tError) ;
			return false;
		}
		return true;
  }
  
  /**
   * checkData
   *
   * @return boolean
   */
  private boolean checkData() 
  {
      System.out.println("进行校验.....");
      if (mLMInterestRateSchema == null) {
      	System.out.println("mLMInterestRateSchema == null");
          return false;
      }
      if (mGlobalInput == null) {
      	System.out.println("mGlobalInput == null");
          return false;
      }
      
      if(this.mOperate.equals("INSERT||MAIN")){
    	  //查询该机构下现有贷款利率最晚终止日期对应的那条记录
    	  String tSql = "select * from Lminterestrate where comcode = '"+mLMInterestRateSchema.getComCode()+"' " +
    	  		" and riskcode = '"+mLMInterestRateSchema.getRiskCode()+"'  order by EndDate desc,int(MinPremLimit) desc ";
    	  LMInterestRateDB tLMInterestRateDB = new LMInterestRateDB();
    	  LMInterestRateSet aLMInterestRateSet = new LMInterestRateSet();
    	  LMInterestRateSchema aLMInterestRateSchema = new LMInterestRateSchema();
    	  aLMInterestRateSet = tLMInterestRateDB.executeQuery(tSql);
    	  if(aLMInterestRateSet!=null&&aLMInterestRateSet.size()>0){
    		  aLMInterestRateSchema = aLMInterestRateSet.get(1);
    		  //【最大保费金额】存在为空
    		  if(aLMInterestRateSchema.getMaxPremLimit()==null||"".equals(aLMInterestRateSchema.getMaxPremLimit())){
    			  //【利率起始日期】固定为该管理机构下现有贷款利率最晚终止日期加一天
    			  if(!mLMInterestRateSchema.getStartDate().equals(PubFun.getLastDate(aLMInterestRateSchema.getEndDate(), 1, "D"))){
    			 		// @@错误处理
    					CError tError = new CError();
    			 		tError.moduleName = "LMInterestRateBL";
    			 		tError.functionName = "checkData";
    			 		tError.errorMessage = "【利率起始日期】应为该管理机构下现有贷款利率最晚终止日期加一天!";
    			 		this.mErrors .addOneError(tError) ;    				  	
    				  return false;
    			  }
    			  //【最大保费金额】不存在为空
    		  }else{
    			  //【利率起始日期】固定为该管理机构下现有贷款利率的最晚【利率起始日期】
    			  if(!mLMInterestRateSchema.getStartDate().equals(aLMInterestRateSchema.getStartDate())){
  			 		// @@错误处理
  					CError tError = new CError();
  			 		tError.moduleName = "LMInterestRateBL";
  			 		tError.functionName = "checkData";
  			 		tError.errorMessage = "【利率起始日期】应为该管理机构下现有贷款利率的最晚【利率起始日期】!";
  			 		this.mErrors .addOneError(tError) ;    				  	
  			 		return false;    				  
    			  }
    			  if(!mLMInterestRateSchema.getEndDate().equals(aLMInterestRateSchema.getEndDate())){
    			 		// @@错误处理
    					CError tError = new CError();
    			 		tError.moduleName = "LMInterestRateBL";
    			 		tError.functionName = "checkData";
    			 		tError.errorMessage = "【利率终止日期】应为该管理机构下现有贷款利率的最晚【利率终止日期】!";
    			 		this.mErrors .addOneError(tError) ;    				  	
    			 		return false;        				  
    			  }
    		  }
    	  }
    	 //查询该管理机构下相同险种现有贷款利率中存在和待保存记录的贷款起始日期和贷款终止日期均相同的记录 
    	  String tSql1 = " select * from Lminterestrate where comcode = '"+mLMInterestRateSchema.getComCode()+"' " +
    	  				 " and  riskcode = '"+mLMInterestRateSchema.getRiskCode()+"' and startdate = '"+mLMInterestRateSchema.getStartDate()+"' " +
    	  				 " and endDate = '"+mLMInterestRateSchema.getEndDate()+"' order by EndDate desc,int(MinPremLimit) desc";
    	  LMInterestRateSet aaLMInterestRateSet = new LMInterestRateSet();
    	  LMInterestRateSchema aaLMInterestRateSchema = new LMInterestRateSchema();
    	  aaLMInterestRateSet = tLMInterestRateDB.executeQuery(tSql1); 
    	  if(aaLMInterestRateSet!=null&&aaLMInterestRateSet.size()>0){
    		  aaLMInterestRateSchema = aaLMInterestRateSet.get(1);
    		  //【最小保费金额】必须等于日期相同的记录中最大的【最大保费金额】
    		  if(!mLMInterestRateSchema.getMinPremLimit().equals(aaLMInterestRateSchema.getMaxPremLimit())){
			 		// @@错误处理
					CError tError = new CError();
			 		tError.moduleName = "LMInterestRateBL";
			 		tError.functionName = "checkData";
			 		tError.errorMessage = "【最小保费金额】必须等于日期相同的记录中最大的【最大保费金额】!";
			 		this.mErrors .addOneError(tError) ;    				  	
			 		return false;        			  
    		  }
    	  }else{
    		  if(!"0".equals(mLMInterestRateSchema.getMinPremLimit())){
			 		// @@错误处理
					CError tError = new CError();
			 		tError.moduleName = "LMInterestRateBL";
			 		tError.functionName = "checkData";
			 		tError.errorMessage = "【最小保费金额】必须从0开始录入!";
			 		this.mErrors .addOneError(tError) ;    				  	
			 		return false;        			  
    		  }
    	  }
      }else if(this.mOperate.equals("UPDATE||MAIN")){
    	  //【利率终止日期】大于当前日期的一条记录
    	  if(CommonBL.stringToDate(mLMInterestRateSchema.getEndDate()).before(CommonBL.stringToDate(PubFun.getCurrentDate()))){
		 		// @@错误处理
				CError tError = new CError();
		 		tError.moduleName = "LMInterestRateBL";
		 		tError.functionName = "checkData";
		 		tError.errorMessage = "【利率终止日期】必须大于当前日期!";
		 		this.mErrors .addOneError(tError) ;    				  	
		 		return false;   
    	  }
      }
      return true;
  }
  
  //插入数据
  private boolean insertData()
  {
	String currentDate = PubFun.getCurrentDate();
	String currentTime = PubFun.getCurrentTime();
	//创建主键流水线号
	String SerialNo = PubFun1.CreateMaxNo("SERIALNO", "");
	mLMInterestRateSchema.setSerialno(SerialNo);
  	mLMInterestRateSchema.setMakeDate(currentDate);
  	mLMInterestRateSchema.setMakeTime(currentTime);
  	mLMInterestRateSchema.setModifyDate(currentDate);
  	mLMInterestRateSchema.setModifyTime(currentTime);
  	mLMInterestRateSchema.setOperator(this.mOperater);
  	PubFun.fillDefaultField(mLMInterestRateSchema);
      map.put(mLMInterestRateSchema, "INSERT");
  	return true;
  }
  
  private boolean updateData(){
	  //查询出相同机构、相同险种下的相同【利率起始日期】、【利率终止日期】的记录
	  LMInterestRateDB ddLMInterestRateDB = new LMInterestRateDB();
	  LMInterestRateSet ddLMInterestRateSet = new LMInterestRateSet();
	  LMInterestRateSchema ddLMInterestRateSchema = new LMInterestRateSchema();
	  ddLMInterestRateDB.setComCode(mDLMInterestRateSchema.getComCode());
	  ddLMInterestRateDB.setRiskCode(mDLMInterestRateSchema.getRiskCode());
	  ddLMInterestRateDB.setStartDate(mDLMInterestRateSchema.getStartDate());
	  ddLMInterestRateDB.setEndDate(mDLMInterestRateSchema.getEndDate());
	  ddLMInterestRateSet = ddLMInterestRateDB.query();
	  if(ddLMInterestRateSet!=null&&ddLMInterestRateSet.size()>0){
		  for(int i=1;i<=ddLMInterestRateSet.size();i++){
			  ddLMInterestRateSchema = ddLMInterestRateSet.get(i);
			  ddLMInterestRateSchema.setEndDate(mLMInterestRateSchema.getEndDate());
			  mLMInterestRateSet.add(ddLMInterestRateSchema);
		  }
		  map.put(mLMInterestRateSet, "UPDATE");
	  }else{
	 		// @@错误处理
			CError tError = new CError();
	 		tError.moduleName = "LMInterestRateBL";
	 		tError.functionName = "checkData";
	 		tError.errorMessage = "您选中的记录不存在!";
	 		this.mErrors .addOneError(tError) ;    				  	
	 		return false;  		  
	  }
	  return true;
  }
  
  //删除数据
  private boolean deleteData()
  {
	  //查询出相同机构、相同险种下的相同【利率起始日期】、【利率终止日期】的记录
	  LMInterestRateDB ddLMInterestRateDB = new LMInterestRateDB();
	  LMInterestRateSet ddLMInterestRateSet = new LMInterestRateSet();
	  LMInterestRateSchema ddLMInterestRateSchema = new LMInterestRateSchema();
	  ddLMInterestRateDB.setComCode(mDLMInterestRateSchema.getComCode());
	  ddLMInterestRateDB.setRiskCode(mDLMInterestRateSchema.getRiskCode());
	  ddLMInterestRateDB.setStartDate(mDLMInterestRateSchema.getStartDate());
	  ddLMInterestRateDB.setEndDate(mDLMInterestRateSchema.getEndDate());
	  ddLMInterestRateSet = ddLMInterestRateDB.query();
	  if(ddLMInterestRateSet!=null&&ddLMInterestRateSet.size()>0){
		  map.put(ddLMInterestRateSet, "DELETE");
	  }else{
	 		// @@错误处理
			CError tError = new CError();
	 		tError.moduleName = "LMInterestRateBL";
	 		tError.functionName = "checkData";
	 		tError.errorMessage = "您选中的记录不存在!";
	 		this.mErrors .addOneError(tError) ;    				  	
	 		return false;  			  
	  }
  	return true;
  }
  public VData getResult()
  {
      return this.mResult;
	}
  
  public static void main(String[] args) {
	System.out.println(PubFun.getLastDate("2013-10-31", 1, "D"));
}
}
