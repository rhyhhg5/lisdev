package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

/**
 * <p>Title: 保全重算类 </p>
 * <p>Description: 通过传入的保单信息、责任信息、保费信息和领取信息进行重算 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author HST
 * @version 1.0
 * @date 2002-07-01
 */
public class ReCalBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    Reflections mReflections = new Reflections();
    LPPolSchema mLPPolSchema;
    LPEdorItemSchema mLPEdorItemSchema;
    LJSGetEndorseSchema mLJSGetEndorseSchema;

    //重算前数据
    LCPolSchema preLCPolSchema = new LCPolSchema();
    LCDutySet preLCDutySet = new LCDutySet();
    LCPremSet preLCPremSet = new LCPremSet();
    LCGetSet preLCGetSet = new LCGetSet();

    //重算后数据
    public LPContSet aftLPContSet = new LPContSet();
    public LPPolSet aftLPPolSet = new LPPolSet();
    public LPDutySet aftLPDutySet = new LPDutySet();
    public LPPremSet aftLPPremSet = new LPPremSet();
    public LPGetSet aftLPGetSet = new LPGetSet();
    public LJSGetEndorseSet aftLJSGetEndorseSet = new LJSGetEndorseSet();
    double mGetMoney = 0;
    double mChgPrem = 0;

    public ReCalBL(LPPolSchema pLPPolSchema, LPEdorItemSchema pLPEdorItemSchema)
    {
        mLPPolSchema = pLPPolSchema.getSchema();
        mLPEdorItemSchema = pLPEdorItemSchema.getSchema();
    }

    /**
     * 准备数据，然后重算
     * @return
     */
    public boolean recal()
    {
        try
        {
            LPEdorItemSchema tLPEdorItemSchema = mLPEdorItemSchema.getSchema();
            tLPEdorItemSchema.setPolNo(mLPPolSchema.getPolNo());

            //如果责任的标准保费为零。CALBL中会取lcpol的prem，不知道为什么。所以在这里清零。
            mLPPolSchema.setPrem(0.0);
            //准备重算需要的保单表数据
            LCPolBL tLCPolBL = getRecalPol(mLPPolSchema);

            //准备重算需要的责任表数据
            LCDutyBLSet tLCDutyBLSet = getRecalDuty(tLPEdorItemSchema);

            //准备重算需要的保费项表数据
            LCPremSet tLCPermSet = getRecalPrem(tLPEdorItemSchema);

            //准备重算需要的领取项表数据
            LCGetBLSet tLCGetBLSet = getRecalGet(tLPEdorItemSchema);

            //重算
            if (!recalWithData(tLCPolBL, tLCDutyBLSet, tLCPermSet, tLCGetBLSet, tLPEdorItemSchema))
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError.buildErr(this, "保全重算失败");

            return false;
        }

        return true;
    }

    /**
     * 用准备好的数据进行重算
     * @param pLCPolBL
     * @param pLCDutyBLSet
     * @param pLCPermSet
     * @param pLCGetBLSet
     * @return
     */
    public boolean recalWithData(LCPolBL pLCPolBL, LCDutyBLSet pLCDutyBLSet,
                                 LCPremSet pLCPermSet, LCGetBLSet pLCGetBLSet,
								 LPEdorItemSchema pLPEdorItemSchema)
    {
        try
        {
            if(!pLPEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_BF)&&!pLPEdorItemSchema.getEdorType().equals("BA")&&!pLPEdorItemSchema.getEdorType().equals("TB")&&!pLPEdorItemSchema.getEdorType().equals("TP")){
                pLCPolBL.setAmnt(0.0);
                pLCPolBL.setRiskAmnt(0.0);
            }
            //qulq 2007-4-28 添加被保人状态
            LPInsuredDB tLPInsuredDB = new LPInsuredDB();
            tLPInsuredDB.setEdorNo(pLPEdorItemSchema.getEdorNo());
            tLPInsuredDB.setEdorType(pLPEdorItemSchema.getEdorType());
            tLPInsuredDB.setContNo(mLPPolSchema.getContNo());
            tLPInsuredDB.setInsuredNo(mLPPolSchema.getInsuredNo());
            tLPInsuredDB.getInfo();
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("InsuredState",tLPInsuredDB.getInsuredStat());
            //开始重算

            CalBL tCalBL = new CalBL(pLCPolBL, pLCDutyBLSet, pLCGetBLSet, pLPEdorItemSchema.getEdorType(),tTransferData);
			tCalBL.setOperator(pLPEdorItemSchema.getOperator());

			//add by xp 091130 添加保额计算要素:套餐编码
			tCalBL.setRiskWrapCode(new ExeSQL().getOneValue("select riskwrapcode from lcriskdutywrap where contno ='"+pLCPolBL.getContNo()+"'"));

            if (CheckRiskAssociate(pLCPolBL.getRiskCode()) == 1)
            {
                if (!tCalBL.calPol())
                {
                    //CError.buildErr(this, "CalBL.calPol()计算失败", tCalBL.mErrors);
                    mErrors.copyAllErrors(tCalBL.mErrors);
					return false;
                }
            }
            else if (CheckRiskAssociate(pLCPolBL.getRiskCode()) == 2)
            {
                if (tCalBL.calPol2(pLCPermSet) == false)
                {
                    CError.buildErr(this, "CalBL.calPol2()计算失败", tCalBL.mErrors);
					return false;
                }
            }
			else
			{
				CError.buildErr(this, "获取险种信息失败！");
				return false;
			}

            //得到计算结果
            getCalResult(tCalBL.getLCPol(), tCalBL.getLCDuty(),
                         tCalBL.getLCPrem(), tCalBL.getLCGet());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError.buildErr(this, "保全重算失败");

            return false;
        }

        return true;
    }

    /**
     * 获取保全重算需要的保单表数据
     * @param pLPEdorItemSchema
     * @return
     */
    public LCPolBL getRecalPol(LPPolSchema pLPPolSchema)
    {
        //准备重算需要的保单表数据
        LCPolSchema tLCPolSchema = new LCPolSchema();
        mReflections.transFields(tLCPolSchema, pLPPolSchema);

        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setSchema(tLCPolSchema);

        //备份重算前数据
        preLCPolSchema.setSchema(tLCPolSchema);

        return tLCPolBL;
    }

    /**
     * 获取保全重算需要的责任表数据
     * @param pLPEdorItemSchema
     * @return
     */
    public LCDutyBLSet getRecalDuty(LPEdorItemSchema pLPEdorItemSchema)
    {
        /*
        LCDutyBLSet tLCDutyBLSet = new LCDutyBLSet();
        DetailDataQuery query =
                new DetailDataQuery(pLPEdorItemSchema.getEdorNo(),
                pLPEdorItemSchema.getEdorType());
        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(mLPPolSchema.getPolNo());
        LCDutySet tLCDutySet = tLCDutyDB.query();
        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            LCDutySchema tLCDutySchema = tLCDutySet.get(i);
            String[] keys = new String[2];
            keys[0] = tLCDutySchema.getPolNo();
            keys[1] = tLCDutySchema.getDutyCode() ;
            LPDutySchema tLPDutySchema = (LPDutySchema)
                    query.getDetailData("LCDuty", keys);
            LCDutySchema tTempSchema = new LCDutySchema();
            mReflections.transFields(tTempSchema, tLPDutySchema);
            tLCDutyBLSet.add(tTempSchema);
        }
        */
    	LCDutyBLSet tLCDutyBLSet = new LCDutyBLSet();
    	if(BQ.EDORTYPE_TP.equals(pLPEdorItemSchema.getEdorType())){
    		LPDutyBL tLPDutyBL = new LPDutyBL();
    		
    		//豁免险的转保采用保单的非豁免险做保额
    		String  hmFlag = new ExeSQL().getOneValue("SELECT 1 FROM LMRISKAPP WHERE RISKCODE='"+mLPPolSchema.getRiskCode()+"' AND RISKTYPE8='9' WITH UR");
    		
    		LPDutyDB tLPDutyDB = new LPDutyDB();
    		tLPDutyDB.setPolNo(pLPEdorItemSchema.getPolNo());
    		tLPDutyDB.setEdorNo(pLPEdorItemSchema.getEdorAcceptNo());
    		LPDutySet tLPDutySet = tLPDutyDB.query();
    		if(tLPDutySet.size()>=1){
    			for(int i=1;i<=tLPDutySet.size();i++){
    				LCDutySchema tLCDutySchema = new LCDutySchema();
    				mReflections.transFields(tLCDutySchema, tLPDutySet.get(i));
    				if("1".equals(hmFlag)){
    					//豁免险的转保采用保单的非豁免险保费做保额
    					tLCDutySchema.setAmnt(mLPPolSchema.getAmnt());
    				}
    				tLCDutyBLSet.add(tLCDutySchema);
    				preLCDutySet.add(tLCDutySchema.getSchema());
    			}
    		}
    		
    	}else{
    		LPDutyBL tLPDutyBL = new LPDutyBL();
    		LPDutySet tLPDutySet = tLPDutyBL.queryAllLPDutyForReCal(pLPEdorItemSchema);
    		
    		if (tLPDutySet.size() > 1)
    		{
    			for (int i = 0; i < tLPDutySet.size(); i++)
    			{
    				LCDutySchema tLCDutySchema = new LCDutySchema();
    				mReflections.transFields(tLCDutySchema, tLPDutySet.get(i + 1));
    				if(pLPEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_BF)||pLPEdorItemSchema.getEdorType().equals("BA")||pLPEdorItemSchema.getEdorType().equals("TB")){
    					tLCDutySchema.setAmnt(mLPPolSchema.getAmnt());
    					tLCDutySchema.setMult(mLPPolSchema.getMult());
    				}
    				if(pLPEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_PC)){
    					tLCDutySchema.setPayIntv(mLPPolSchema.getPayIntv());
    				}
//              20080717 zhanggm TB人工核保改变险种档次后，重新算费
    				if(pLPEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_TB)){
    					tLCDutySchema.setMult(mLPPolSchema.getMult());
    				}
    				//------------------------------------------------
    				tLCDutyBLSet.add(tLCDutySchema);
    				
    				//备份重算前数据
    				preLCDutySet.add(tLCDutySchema.getSchema());
    			}
    		}
    		else if (tLPDutySet.size() == 1)
    		{
    			LCDutySchema tLCDutySchema = new LCDutySchema();
    			mReflections.transFields(tLCDutySchema, tLPDutySet.get(1));
    			//tLCDutySchema.setDutyCode(null);
    			if(pLPEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_BF)||pLPEdorItemSchema.getEdorType().equals("BA")||pLPEdorItemSchema.getEdorType().equals("TB")){
    				tLCDutySchema.setAmnt(mLPPolSchema.getAmnt());
    				tLCDutySchema.setMult(mLPPolSchema.getMult());
    			}
    			if (pLPEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_PC))
    			{
    				tLCDutySchema.setPayIntv(mLPPolSchema.getPayIntv());
    			}
//          20080717 zhanggm TB人工核保改变险种档次后，重新算费
    			if(pLPEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_TB))
    			{
    				tLCDutySchema.setMult(mLPPolSchema.getMult());
    			}
    			//------------------------------------------------
    			
    			tLCDutyBLSet.add(tLCDutySchema);
    			
    			//备份重算前数据
    			preLCDutySet.add(tLCDutySchema.getSchema());
    		}
    		
    	}
        return tLCDutyBLSet;
    }

    /**
     * 获取保全重算需要的保费项表数据
     * @param pLPEdorItemSchema
     * @return
     */
    public LCPremSet getRecalPrem(LPEdorItemSchema pLPEdorItemSchema)
    {
    	LCPremSet tLCPremSet = new LCPremSet();
    	
    	if(BQ.EDORTYPE_TP.equals(pLPEdorItemSchema.getEdorType())){
    		
    		LPPremDB tLPPremDB = new LPPremDB();

    		String premSQL = "select * from lpprem where edorno='"+pLPEdorItemSchema.getEdorAcceptNo()+"' and polno='"+pLPEdorItemSchema.getPolNo()+"' and payplancode not like '000000%' with ur ";
    		LPPremSet tLPPremSet = tLPPremDB.executeQuery(premSQL);
    		if(tLPPremSet.size()>=1){
    			for(int i=1;i<=tLPPremSet.size();i++){
    				LCPremSchema tLCPremSchema = new LCPremSchema();
    				mReflections.transFields(tLCPremSchema, tLPPremSet.get(i));
    				
    				tLCPremSet.add(tLCPremSchema);
    				preLCPremSet.add(tLCPremSchema.getSchema());
    			}
    		}
    		
    	}else {
    		LPPremBL tLPPremBL = new LPPremBL();
    		LPPremSet tLPPremSet = tLPPremBL.queryAllLPPremForReCal(pLPEdorItemSchema);
    		
    		
    		for (int i = 0; i < tLPPremSet.size(); i++)
    		{
    			LCPremSchema tLCPremSchema = new LCPremSchema();
    			mReflections.transFields(tLCPremSchema, tLPPremSet.get(i + 1));
    			if(pLPEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_PC)){
    				tLCPremSchema.setPayIntv(mLPPolSchema.getPayIntv());
    			}
    			tLCPremSet.add(tLCPremSchema);
    			//备份重算前数据
    			preLCPremSet.add(tLCPremSchema.getSchema());
    		}
    	}
        return tLCPremSet;
    }

    /**
     * 获取保全重算需要的领取项表数据
     * @param pLPEdorItemSchema
     * @return
     */
    public LCGetBLSet getRecalGet(LPEdorItemSchema pLPEdorItemSchema)
    {
    	LCGetBLSet tLCGetBLSet = new LCGetBLSet();
    	if(BQ.EDORTYPE_TP.equals(pLPEdorItemSchema.getEdorType())){
    		LPGetDB tLPGetDB = new LPGetDB();
    		tLPGetDB.setEdorNo(pLPEdorItemSchema.getEdorAcceptNo());
    		tLPGetDB.setEdorType(pLPEdorItemSchema.getEdorType());
    		tLPGetDB.setPolNo(pLPEdorItemSchema.getPolNo());
    		
    		LPGetSet tLPGetSet = tLPGetDB.query();
    		if(tLPGetSet.size()>0){
    			for(int i=1;i<=tLPGetSet.size();i++){
    				LCGetSchema tLCGetSchema = new LCGetSchema();
    				mReflections.transFields(tLCGetSchema, tLPGetSet.get(i));
    				tLCGetBLSet.add(tLCGetSchema);
    				preLCGetSet.add(tLCGetSchema.getSchema());
    			}
    		}
    	}else{
    		LPGetBL tLPGetBL = new LPGetBL();
    		LPGetSet tLPGetSet = tLPGetBL.queryAllLPGetForReCal(pLPEdorItemSchema);
    		
    		for (int i = 0; i < tLPGetSet.size(); i++)
    		{
    			LCGetSchema tLCGetSchema = new LCGetSchema();
    			mReflections.transFields(tLCGetSchema, tLPGetSet.get(i + 1));
    			tLCGetBLSet.add(tLCGetSchema);
    			
    			//备份重算前数据
    			preLCGetSet.add(tLCGetSchema.getSchema());
    		}
    	}

        return tLCGetBLSet;
    }

    /**
     * 判断险种是否和帐户关联-如果和帐户关联，那么在重算的时候需要传入保费项作为计算要素
     * @param RiskcCode
     * @return
     */
    public int CheckRiskAssociate(String aRiskcCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(aRiskcCode);
        if (!tLMRiskAppDB.getInfo())
        {
            mErrors.copyAllErrors(tLMRiskAppDB.mErrors);
            mErrors.addOneError(new CError("查询险种信息失败！"));
            return -1;
        }

        if (tLMRiskAppDB.getInpPayPlan() == null || tLMRiskAppDB.getInpPayPlan().equals(""))
        {
            return 1;
        }
		else if (tLMRiskAppDB.getInpPayPlan().equals("N"))
		{
			return 1;
		}
		else if (tLMRiskAppDB.getInpPayPlan().equals("Y"))
		{
			return 2;
		}
		else
		{
			return -1;
		}
    }

    /**
     * 处理重算结果
     * @param pLCPolBL
     * @param pLCDutyBLSet
     * @param pLCPremBLSet
     * @param pLCGetBLSet
     * @return
     */
    private boolean getCalResult(LCPolBL pLCPolBL, LCDutyBLSet pLCDutyBLSet,
                                 LCPremBLSet pLCPremBLSet,
                                 LCGetBLSet pLCGetBLSet)
    {
        //获取重算后保单表数据，并重置原来的的保单表未变更信息
        LPPolSchema tLPPolSchema = new LPPolSchema();
        mReflections.transFields(tLPPolSchema, pLCPolBL.getSchema());
        tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPPolSchema.setPaytoDate(preLCPolSchema.getPaytoDate());
        tLPPolSchema.setSignCom(preLCPolSchema.getSignCom());
        tLPPolSchema.setSignDate(preLCPolSchema.getSignDate());
        tLPPolSchema.setAppFlag(preLCPolSchema.getAppFlag());
        tLPPolSchema.setHandler(preLCPolSchema.getHandler());
        String sql="select 1 from lcgrppol where grpcontno='"+preLCPolSchema.getGrpContNo()+"' and riskcode='163001'";
    	ExeSQL exeSql = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = exeSql.execSQL(sql);
        if(tSSRS.getMaxRow()!=0 && "CM".equals(mLPEdorItemSchema.getEdorType())){
        	tLPPolSchema.setStandPrem(tLPPolSchema.getStandPrem());
        }else{
        	tLPPolSchema.setStandPrem(tLPPolSchema.getPrem());
        }		
		tLPPolSchema.setPrem(tLPPolSchema.getPrem());
		tLPPolSchema.setAmnt(tLPPolSchema.getAmnt());
        aftLPPolSet.add(tLPPolSchema);

        //获取重算后责任表数据，并重置原来的的责任表未变更信息
        for (int i = 1; i <= pLCDutyBLSet.size(); i++)
        {
            LPDutySchema tLPDutySchema = new LPDutySchema();
            mReflections.transFields(tLPDutySchema,
                                     pLCDutyBLSet.get(i).getSchema());

            tLPDutySchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPDutySchema.setEdorType(mLPEdorItemSchema.getEdorType());
			tLPDutySchema.setOperator(mLPEdorItemSchema.getOperator());
            tLPDutySchema.setModifyDate(PubFun.getCurrentDate());
            tLPDutySchema.setModifyTime(PubFun.getCurrentTime());

            aftLPDutySet.add(tLPDutySchema);
        }

        //补上由于重算导致的丢失的责任数据
        if (aftLPDutySet.size() == 1)
        {
            // 此时dutycode 为空
            aftLPDutySet.get(1).setSumPrem(preLCDutySet.get(1).getSumPrem());
            aftLPDutySet.get(1).setPaytoDate(preLCDutySet.get(1).getPaytoDate());
        }
        else
        {
            for (int index = 1; index <= preLCDutySet.size(); index++)
            {
                LCDutySchema preDutySchema = preLCDutySet.get(index);

                for (int j = 1; j <= aftLPDutySet.size(); j++)
                {
                    if (aftLPDutySet.get(j).getDutyCode().equals(preDutySchema
                        .getDutyCode()))
                    {
                        aftLPDutySet.get(j).setSumPrem(preDutySchema.getSumPrem());
                        aftLPDutySet.get(j).setPaytoDate(preDutySchema
                            .getPaytoDate());

                        break;
                    }
                }
            }
        }

        //获取重算后保费项表数据，,补上保费项丢失的信息(交至日期)
        for (int i = 1; i <= pLCPremBLSet.size(); i++)
        {
            LPPremSchema tLPPremSchema = new LPPremSchema();
            mReflections.transFields(tLPPremSchema,
                                     pLCPremBLSet.get(i).getSchema());

            for (int j = 1; j <= preLCPremSet.size(); j++)
            {
                if (tLPPremSchema.getPolNo().equals(preLCPremSet.get(j)
                    .getPolNo())
                    && tLPPremSchema.getDutyCode().equals(preLCPremSet.get(j)
                    .getDutyCode())
                    && tLPPremSchema.getPayPlanCode().equals(preLCPremSet.get(j)
                    .getPayPlanCode()))
                {
                    tLPPremSchema.setPaytoDate(preLCPremSet.get(j).getPaytoDate());
                    tLPPremSchema.setSumPrem(preLCPremSet.get(j).getSumPrem());
                }
            }

            tLPPremSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPPremSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            //add by fuxin 2010-2-5 契约重算保费时calbl中将lcprem.paytimes置0，导致续期抽档是ljapayperson.paycount错误
            //修改契约程序风险较大，所以在保全重算的最后重新更新lcprem.paytimes。
            tLPPremSchema.setPayTimes(new ExeSQL().getOneValue("select paytimes from lcprem where polno ='"+preLCPremSet.get(1).getPolNo()+"' with ur"));
            tLPPremSchema.setOperator(mLPEdorItemSchema.getOperator());
            tLPPremSchema.setOperator(mLPEdorItemSchema.getOperator());
            tLPPremSchema.setModifyDate(PubFun.getCurrentDate());
            tLPPremSchema.setModifyTime(PubFun.getCurrentTime());

            aftLPPremSet.add(tLPPremSchema);
        }

        //补上因重算丢失的保费信息（承保、核保加费）
        for (int m = 0; m < preLCPremSet.size(); m++)
        {
            boolean isExistPrem = false;
            double dutyPrem = 0.0;//计算得到的责任保费
            for (int n = 0; n < aftLPPremSet.size(); n++)
            {
                if (aftLPPremSet.get(n + 1).getDutyCode().equals(preLCPremSet.get(m
                        + 1)
                        .getDutyCode())
                    &&(!"000000".equals(aftLPPremSet.get(n + 1).getPayPlanCode().substring(0, 6))))
                {
                        dutyPrem += aftLPPremSet.get(n + 1).getPrem();
                }
            }

            for (int n = 0; n < aftLPPremSet.size(); n++)
            {
                //发现重算后的保费项集合中有数据，则退出内循环
                if (aftLPPremSet.get(n + 1).getDutyCode().equals(preLCPremSet.get(m
                    + 1)
                    .getDutyCode())
                    && aftLPPremSet.get(n + 1).getPayPlanCode().equals(preLCPremSet.get(m
                    + 1)
                    .getPayPlanCode()))
                {
                    isExistPrem = true;

                    break;
                }
            }

            if (!isExistPrem)
            {
                //为责任项补上丢失保费项的保费信息
                boolean isExistDuty = false;
                if("000000".equals(preLCPremSet.get(m + 1).getPayPlanCode().substring(0,6)))
                {
                    if (!(Math.abs(preLCPremSet.get(m + 1).getRate()) <0.000001))
                    { //判断是否为零
                        //重算加费
                        double addprem = dutyPrem * preLCPremSet.get(m + 1).getRate();
                            preLCPremSet.get(m + 1).setPrem(addprem);
                            preLCPremSet.get(m + 1).setStandPrem(addprem);
                            preLCPremSet.get(m + 1).setSumPrem(addprem);
                    }
                }
                for (int l = 0; l < aftLPDutySet.size(); l++)
                {
                    LPDutySchema tLPDutySchema = aftLPDutySet.get(l + 1);

                    if (tLPDutySchema.getDutyCode().equals(preLCPremSet.get(m
                        + 1)
                        .getDutyCode()))
                    {
                        tLPDutySchema.setPrem(tLPDutySchema.getPrem()
                                              + preLCPremSet.get(m + 1).getPrem());
                        tLPDutySchema.setSumPrem(tLPDutySchema.getSumPrem()
                                                 + preLCPremSet.get(m + 1)
                                                 .getSumPrem());
                        isExistDuty = true;
                    }
                }

                if (!isExistDuty)
                {
                    break;
                }

                //未找到保费项数据，则为丢失数据，加入到重算结果中
                LPPremSchema tLPPremSchema = new LPPremSchema();
                mReflections.transFields(tLPPremSchema,
                                         preLCPremSet.get(m + 1).getSchema());
                tLPPremSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
                tLPPremSchema.setEdorType(mLPEdorItemSchema.getEdorType());
				tLPPremSchema.setOperator(mLPEdorItemSchema.getOperator());
                tLPPremSchema.setModifyDate(PubFun.getCurrentDate());
                tLPPremSchema.setModifyTime(PubFun.getCurrentTime());
                aftLPPremSet.add(tLPPremSchema);

                //为保单补上丢失保费项的保费信息
                for (int l = 0; l < aftLPPolSet.size(); l++)
                {
                    tLPPolSchema = aftLPPolSet.get(l + 1);

                    if (tLPPolSchema.getPolNo().equals(preLCPremSet.get(m + 1)
                        .getPolNo()))
                    {
                        tLPPolSchema.setPrem(tLPPolSchema.getPrem()
                                             + preLCPremSet.get(m + 1).getPrem());
                        tLPPolSchema.setSumPrem(tLPPolSchema.getSumPrem()
                                                + preLCPremSet.get(m + 1)
                                                .getSumPrem());
                    }
                }
            }
        }

        //处理领取项数据
        for (int i = 1; i <= pLCGetBLSet.size(); i++)
        {
            LPGetSchema tLPGetSchema = new LPGetSchema();
            mReflections.transFields(tLPGetSchema,
                                     pLCGetBLSet.get(i).getSchema());

            tLPGetSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPGetSchema.setEdorType(mLPEdorItemSchema.getEdorType());
			tLPGetSchema.setOperator(mLPEdorItemSchema.getOperator());
            tLPGetSchema.setModifyDate(PubFun.getCurrentDate());
            tLPGetSchema.setModifyTime(PubFun.getCurrentTime());
            tLPGetSchema.setMakeDate(PubFun.getCurrentDate());
            tLPGetSchema.setMakeTime(PubFun.getCurrentTime());

            aftLPGetSet.add(tLPGetSchema);
        }
        return true;
    }
}
