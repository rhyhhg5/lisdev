package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全项目理算</p>
 * <p>Description: 交费频次变更</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorPCAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;
    
    private String mPolNo = null;
    
    private String mNewPrem = null;

    private double mGetMoney = 0.0;

    private double mChgPrem = 0.0;

    private ReCalBL mReCalBL = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到输入数据
     * @param cInputData VData
     * @return boolean
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.
                    getObjectByObjectName("GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.
                getObjectByObjectName("LPEdorItemSchema", 0);
        mEdorNo = mLPEdorItemSchema.getEdorNo();
        mEdorType = mLPEdorItemSchema.getEdorType();
        mContNo = mLPEdorItemSchema.getContNo();
        EdorItemSpecialData tSpecialData = new EdorItemSpecialData(mEdorNo,mEdorType);
        if(tSpecialData.query())
        {
        	mNewPrem = tSpecialData.getEdorValue("NEWPREM");
            mPolNo = tSpecialData.getEdorValue("POLNO");
        }
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        LPPolSet tLPPolSet = getLPPolSet();
        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
            if (!dealOnePol(tLPPolSet.get(i)))
            {
                return false;
            }
        }
        setLPCont();
        setEdorItem();
        return true;
    }

    /**
     * 得到LPPol表的信息
     * @return LPPolSet
     */
    private LPPolSet getLPPolSet()
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        tLPPolDB.setEdorType(mEdorType);
        tLPPolDB.setContNo(mContNo);
        return tLPPolDB.query();
    }


    /**
     * 处理一个险种
     * @param aLPPolSchema LPPolSchema
     * @return boolean
     */
    private boolean dealOnePol(LPPolSchema aLPPolSchema)
    {
        try
        {
            LCPolSchema tLCPolSchema = CommonBL.getLCPol(aLPPolSchema.getPolNo());
            double oldPrem = aLPPolSchema.getPrem();
            double newPrem ;

            int oldIntv = tLCPolSchema.getPayIntv();
            int newIntv = aLPPolSchema.getPayIntv();
            if(null != mPolNo && !mPolNo.equals("") && null != mNewPrem && !mNewPrem.equals("")){
            	newPrem = Double.valueOf(mNewPrem);
            }else{
            	newPrem = calNewPrem(aLPPolSchema);
            }   
                mChgPrem += newPrem - oldPrem;
           
/*
						不需要交费，因为修改后的交至日期必须和修改前的一致 qulq 070214
            if (newIntv > oldIntv) //如果交费频次变大要补费，如果交费频次变小从下期开始生效
            {
                double getMoney = ((newIntv - oldIntv) / oldIntv) * oldPrem;
                mGetMoney += getMoney;
                mChgPrem += newPrem - oldPrem;
                setLJSGetEndorse(aLPPolSchema, getMoney);
            }
            Date payToDate = CommonBL.changeMonth(
                        tLCPolSchema.getCValiDate(), newIntv);

*/
            //日期也不需要变 qulq 070214
            Date payToDate = CommonBL.changeMonth(tLCPolSchema.getPaytoDate(), 0);
            setLPTables(newPrem, payToDate);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 重算保费
     * @param aLPPolSchema LPPolSchema
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private double calNewPrem(LPPolSchema aLPPolSchema) throws Exception
    {
        //重算保费
        mReCalBL = new ReCalBL(aLPPolSchema, mLPEdorItemSchema);
        if (!mReCalBL.recal())
        {
            mErrors.copyAllErrors(mReCalBL.mErrors);
            throw new Exception();
        }
        return mReCalBL.aftLPPolSet.get(1).getPrem();
    }

    /**
     *  往批改补退费表（应收/应付）新增数据
     * @param aLPPolSchema LPPolSchema
     * @param changePrem double
     * @return boolean
     */
    private boolean setLJSGetEndorse(LPPolSchema aLPPolSchema,
            double changePrem)
    {
        String feeType = CommonBL.getFeeType(mEdorType, BQ.FEEFINATYPE_BF);
        if (feeType == null)
        {
            return false;
        }
        BqCalBL bqCalBL = new BqCalBL();
        LJSGetEndorseSchema tLJSGetEndorseSchema = bqCalBL.initLJSGetEndorse(
                mLPEdorItemSchema, aLPPolSchema, null, feeType, changePrem,
                mGlobalInput);
        tLJSGetEndorseSchema.setManageCom(aLPPolSchema.getManageCom());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }


    /**
     * 设置P表的保费和PayToDate
     * @param getMoney double
     * @param payToDate Date
     */
    private void setLPTables(double getMoney, Date payToDate)
    {
        for (int i = 1; i <= mReCalBL.aftLPPolSet.size(); i++)
        {
            String sql = "update LPPol set PayIntv = "
                         + mReCalBL.aftLPPolSet.get(i).getPayIntv()
                         + ",  Prem = "
                         + mReCalBL.aftLPPolSet.get(i).getPrem() + " "
                         + "where PolNo = '"
                         + mReCalBL.aftLPPolSet.get(i).getPolNo() + "' "
                         + "   and EdorNo = '"
                         + mReCalBL.aftLPPolSet.get(i).getEdorNo() + "' "
                         + "   and EdorType = '"
                         + mReCalBL.aftLPPolSet.get(i).getEdorType() + "' ";
             mMap.put(sql, SysConst.UPDATE);

//            mReCalBL.aftLPPolSet.get(i).setSumPrem(getMoney);
//            mReCalBL.aftLPPolSet.get(i).setPaytoDate(payToDate);
        }
        for (int i = 1; i <= mReCalBL.aftLPDutySet.size(); i++)
        {
            String sql = "update LPDuty set PayIntv = "
                         + mReCalBL.aftLPDutySet.get(i).getPayIntv()
                         + ",  Prem = "
                         + mReCalBL.aftLPDutySet.get(i).getPrem() + " "
                         + "where PolNo = '"
                         + mReCalBL.aftLPDutySet.get(i).getPolNo() + "' "
                         + "   and DutyCode = '"
                         + mReCalBL.aftLPDutySet.get(i).getDutyCode() + "' "
                         + "   and EdorNo = '"
                         + mReCalBL.aftLPDutySet.get(i).getEdorNo() + "' "
                         + "   and EdorType = '"
                         + mReCalBL.aftLPDutySet.get(i).getEdorType() + "' ";
             mMap.put(sql, SysConst.UPDATE);

//            mReCalBL.aftLPDutySet.get(i).setPaytoDate(payToDate);
        }
        for (int i = 1; i <= mReCalBL.aftLPPremSet.size(); i++)
        {
            String sql = "update LPPrem set PayIntv = "
                         + mReCalBL.aftLPPremSet.get(i).getPayIntv()
                         + ",  Prem = "
                         + mReCalBL.aftLPPremSet.get(i).getPrem() + " "
                         + "where PolNo = '"
                         + mReCalBL.aftLPPremSet.get(i).getPolNo() + "' "
                         + "   and DutyCode = '"
                         + mReCalBL.aftLPPremSet.get(i).getDutyCode() + "' "
                         + "   and PayPlanCode = '"
                         + mReCalBL.aftLPPremSet.get(i).getPayPlanCode() + "' "
                         + "   and EdorNo = '"
                         + mReCalBL.aftLPPremSet.get(i).getEdorNo() + "' "
                         + "   and EdorType = '"
                         + mReCalBL.aftLPPremSet.get(i).getEdorType() + "' ";
             mMap.put(sql, SysConst.UPDATE);

//            mReCalBL.aftLPPremSet.get(i).setPaytoDate(payToDate);
        }
//        mMap.put(mReCalBL.aftLPPolSet, "DELETE&INSERT");
//        mMap.put(mReCalBL.aftLPDutySet, "DELETE&INSERT");
//        mMap.put(mReCalBL.aftLPPremSet, "DELETE&INSERT");
    }

    /**
     * 更新item表的保费
     */
    private void setEdorItem()
    {
        mLPEdorItemSchema.setGetMoney(mGetMoney);
        mLPEdorItemSchema.setChgPrem(mChgPrem);
        mLPEdorItemSchema.setEdorState(BQ.EDORSTATE_CAL);
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setModifyDate(mCurrentDate);
        mLPEdorItemSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPEdorItemSchema, "DELETE&INSERT");
    }

    /**
     * 更新LPCont表的保费
     */
    private void setLPCont()
    {
        String sql = "update LPCont a " +
                "set (Prem, PayIntv) = "
                + "  (select sum(Prem), min(PayIntv) from LPPol "
                + "  where EdorNo = a.EdorNo "
                + "      and EdorType = a.EdorType "
                + "      and ContNo = a.ContNo) " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "pa0001";
        gi.ComCode = "86";

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo("20070215000001");

        VData data = new VData();
        data.add(gi);
        data.add(tLPEdorItemDB.query().get(1));

        PEdorPCAppConfirmBL bl = new PEdorPCAppConfirmBL();

        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}

