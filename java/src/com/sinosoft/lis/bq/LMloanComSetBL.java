package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMInterestRateDB;
import com.sinosoft.lis.db.LPEdorMainDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LMInterestRateSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LMInterestRateSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class LMloanComSetBL {


    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;
    
    private String mManageCom = "";
    
    private String CODETYPE = "loancom";
    
    private LMInterestRateSet mLMInterestRateSet = new LMInterestRateSet();
    
    public LMloanComSetBL(GlobalInput gi, String aManageCom){
    	mGlobalInput = gi;
    	mManageCom = aManageCom;
    }

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData()
    {     
        if(!checked()){
        	return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }
    
    /**
     * 医保卡校验
     * wujun
     * @return
     */
    public boolean checked(){
    	if(mGlobalInput==null||"".equals(mGlobalInput)){
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "checkData";
            tError.errorMessage = "设置错误！";
            this.mErrors.addOneError(tError);        	
            return false;
    	}
    	if(mManageCom == null||"".equals(mManageCom)){
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "checkData";
            tError.errorMessage = "请选择设置机构！";
            this.mErrors.addOneError(tError);        	
            return false;    		
    	}
    	LDCodeDB mmLDCodeDB = new LDCodeDB();
    	LDCodeSet tLDCodeSet = new LDCodeSet();
    	mmLDCodeDB.setCodeType(this.CODETYPE);
    		mmLDCodeDB.setCode(mManageCom);
    	tLDCodeSet = mmLDCodeDB.query();
    	if(tLDCodeSet!=null&&tLDCodeSet.size()>0){
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "checkData";
            tError.errorMessage = "请不要重复设置利率机构！";
            this.mErrors.addOneError(tError);        	
            return false;       		
    	}
    	
    	return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	//设置机构信息
       if(!setLdCode()){
    	   return false;
       }
        //赋值lminterestrate
        if(!setLminterestrate()){
        	return false;
        }
        return true;
   }


    private boolean setLdCode(){
    	
    	LDComDB tLDComDB = new LDComDB();
    	tLDComDB.setComCode(mManageCom);
    	if(!tLDComDB.getInfo()){
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "checkData";
            tError.errorMessage = "选择的机构不存在！";
            this.mErrors.addOneError(tError);        	
            return false;      		
    	}
    	LDComSchema tLDComSchema = new LDComSchema();
    	tLDComSchema = tLDComDB.getSchema();
    	
    	LDCodeSchema tLDCodeSchema = new LDCodeSchema();
    	tLDCodeSchema.setCodeType(this.CODETYPE);
    	tLDCodeSchema.setCode(mManageCom);
    	tLDCodeSchema.setCodeName(tLDComSchema.getName());
    	tLDCodeSchema.setCodeAlias("贷款利率特殊机构配置");
    	mMap.put(tLDCodeSchema, "INSERT");
    	return true;
    }
    
    private boolean setLminterestrate(){

    	//将总公司的利率数据法制到分公司中
    	LMInterestRateDB tLMInterestRateDB = new LMInterestRateDB();
    	LMInterestRateSet tLMInterestRateSet = new LMInterestRateSet();
    	LMInterestRateSchema tLMInterestRateSchema = new LMInterestRateSchema();
    	tLMInterestRateDB.setComCode("86");
    	tLMInterestRateSet = tLMInterestRateDB.query();
    	if(tLMInterestRateSet!=null&&tLMInterestRateSet.size()>0){
    		for(int i=1;i<=tLMInterestRateSet.size();i++){
    			tLMInterestRateSchema = tLMInterestRateSet.get(i);
    			//创建主键流水线号
    			String SerialNo = PubFun1.CreateMaxNo("SERIALNO", "");
    			tLMInterestRateSchema.setComCode(mManageCom);
    			tLMInterestRateSchema.setSerialno(SerialNo);
    			mLMInterestRateSet.add(tLMInterestRateSchema);
    		}
    		mMap.put(mLMInterestRateSet, "INSERT");
    	}else{
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "checkData";
            tError.errorMessage = "请先为总公司配置！";
            this.mErrors.addOneError(tError);        	
            return false;      		
    	}
    	return true;
    }
    
   /**
    * 提交数据到数据库
    * @return boolean
    */
   private boolean submit()
   {
       VData data = new VData();
       data.add(mMap);
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(data, ""))
       {
           mErrors.copyAllErrors(tPubSubmit.mErrors);
           return false;
       }
       return true;
   }


}
