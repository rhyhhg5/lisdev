package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class PEdorSaveDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mEdorState = BQ.EDORSTATE_INPUT;

    public PEdorSaveDetailBL(GlobalInput gi, LPEdorItemSchema edorItem)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorItem.getEdorNo();
        this.mEdorType = edorItem.getEdorType();
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        setEdorState();
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 设置item状态
     */
    private void setEdorState()
    {
        String sql = "update  LPEdorItem " +
                "set EdorState = '" + mEdorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
