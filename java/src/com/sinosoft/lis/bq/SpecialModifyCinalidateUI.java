package com.sinosoft.lis.bq;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class SpecialModifyCinalidateUI {
    public SpecialModifyCinalidateUI() {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        SpecialModifyCinalidateBL tSpecialModifyCinalidateBL = new SpecialModifyCinalidateBL();
        if (tSpecialModifyCinalidateBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tSpecialModifyCinalidateBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "tSpecialModifyCinalidateUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据查询失败!";
            this.mErrors.addOneError(tError);
            mResult.clear();
            return false;
        } else
            mResult = tSpecialModifyCinalidateBL.getResult();
        return true;
    }

    public VData getResult() {
        return mResult;
    }

}
