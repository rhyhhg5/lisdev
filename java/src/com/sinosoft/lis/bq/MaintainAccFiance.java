package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.operfee.FeeConst;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 早期特需续保没有处理财务数据，现改为：
 * 1、通过自动执行先退费再收费（按续保原则）的动作实现满期给付金专续保保费动作
 * 2、生成实付：LJAGet、LJAGetEndorse
 * 3、生成应收备份表、实收、暂收：LJSPayB、LJSPayGrpB、LJAPay、LJAPayGrp、
 * LJAPayPerson、LJTempFeeClass、LJTempFee
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class MaintainAccFiance
{
    private String mCurDate = "2006-3-15";
    private String mBGrpPolNo = "2200001512";

    public String mEdorNo = null;
    public String mEdorType = null;
    public String mOperator = null;
    public String mSerialNo = null;
    private String mGetNo = null;
    private String mGetNoticeNo = null;
    private String mCurTime = PubFun.getCurrentTime();
    private LPGrpContSchema mLPGrpContSchema = null;

    private Reflections ref = new Reflections();
    public String mPayNo = null;
    private String mManageCom = null;
    private double mSumGetMoney = 0;

    private MMap map = new MMap();

    public MaintainAccFiance()
    {
    }

    /**
     * 生成财务数据
     * @param tLCGrpContSchema LCGrpContSchema
     * @return boolean
     */
    public MMap setFiance(LPGrpContSchema tLPGrpContSchema)
    {
        mLPGrpContSchema = tLPGrpContSchema;
        this.mManageCom = mLPGrpContSchema.getManageCom();
        this.mEdorNo = mLPGrpContSchema.getEdorNo();
        this.mEdorType = mLPGrpContSchema.getEdorType();

        //生成实退数据
        LPGrpPolDB tLPGrpPolDB = new LPGrpPolDB();
        tLPGrpPolDB.setEdorNo(mEdorNo);
        tLPGrpPolDB.setEdorType(mEdorType);
        tLPGrpPolDB.setGrpContNo(mLPGrpContSchema.getGrpContNo());
        LPGrpPolSet tLPGrpPolSet = tLPGrpPolDB.query();
        for(int i = 1; i <= tLPGrpPolSet.size(); i++)
        {
            setLJAGetEndorseInfo(tLPGrpPolSet.get(i));
            //实收
        }

        if(!setLJAGetInfo())
        {
            return null;
        }

        return map;
    }

    /**
     * 生成财务数据
     * 若为续保，则需要同时生成实付和实收数据
     * 若为满期终止，则只需要生成实付数据
     * @return boolean
     */
    private boolean setLJAGetEndorseInfo(LPGrpPolSchema tLPGrpPolSchema)
    {
        //实付
        LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        tLJAGetEndorseSchema.setActuGetNo(getActuGetNo());
        tLJAGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJAGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJAGetEndorseSchema.setFeeFinaType("TF");
        tLJAGetEndorseSchema.setGrpContNo(tLPGrpPolSchema.getGrpContNo());
        tLJAGetEndorseSchema.setContNo(BQ.FILLDATA);
        tLJAGetEndorseSchema.setGrpPolNo(tLPGrpPolSchema.getGrpPolNo());
        tLJAGetEndorseSchema.setPolNo(BQ.FILLDATA);
        tLJAGetEndorseSchema.setOtherNo(mEdorNo);
        tLJAGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJAGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJAGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJAGetEndorseSchema.setAppntNo(mLPGrpContSchema.getAppntNo());
        tLJAGetEndorseSchema.setInsuredNo(BQ.FILLDATA);
        tLJAGetEndorseSchema.setGetNoticeNo(mEdorNo);
        tLJAGetEndorseSchema.setGetDate(this.mCurDate);
        tLJAGetEndorseSchema.setGetMoney( -getGetMoney());
        tLJAGetEndorseSchema.setKindCode(tLPGrpPolSchema.getKindCode());
        tLJAGetEndorseSchema.setRiskCode(tLPGrpPolSchema.getRiskCode());
        tLJAGetEndorseSchema.setRiskVersion(tLPGrpPolSchema.getRiskVersion());
        tLJAGetEndorseSchema.setManageCom(tLPGrpPolSchema.getManageCom());
        tLJAGetEndorseSchema.setAgentCode(tLPGrpPolSchema.getAgentCode());
        tLJAGetEndorseSchema.setAgentCom(tLPGrpPolSchema.getAgentCom());
        tLJAGetEndorseSchema.setAgentType(tLPGrpPolSchema.getAgentType());
        tLJAGetEndorseSchema.setAgentGroup(tLPGrpPolSchema.getAgentGroup());
        tLJAGetEndorseSchema.setGrpName(tLPGrpPolSchema.getGrpName());
        tLJAGetEndorseSchema.setApproveCode(tLPGrpPolSchema.getApproveCode());
        tLJAGetEndorseSchema.setApproveDate(tLPGrpPolSchema.getApproveDate());
        tLJAGetEndorseSchema.setApproveTime(tLPGrpPolSchema.getApproveTime());
        tLJAGetEndorseSchema.setOperator(mOperator);
        tLJAGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        tLJAGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJAGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAGetEndorseSchema.setGetFlag("1");
        tLJAGetEndorseSchema.setEnterAccDate(this.mCurDate);
        tLJAGetEndorseSchema.setGetConfirmDate(this.mCurDate);

        map.put("delete from LJAGetEndorse where endorsementNo = '" + mEdorNo
                + "'  and feeOperationType = '" + BQ.EDORTYPE_MJ
                + "'  and grpContNo = '"
                + tLJAGetEndorseSchema.getGrpContNo() + "' ",
                "DELETE");
        map.put(tLJAGetEndorseSchema, "INSERT");

        map.add(getLJAPayGrp(tLJAGetEndorseSchema, tLPGrpPolSchema));
        map.add(getLJTempFee(tLJAGetEndorseSchema, tLPGrpPolSchema));

        mSumGetMoney += Math.abs(tLJAGetEndorseSchema.getGetMoney());

        return true;
    }

    /**
     * 生成实退费信息
     * @param tLPGrpContSchema LPGrpContSchema
     * @return boolean
     */
    private boolean setLJAGetInfo()
    {
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        tLJAGetSchema.setActuGetNo(getActuGetNo());
        tLJAGetSchema.setOtherNo(mEdorNo);
        tLJAGetSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJAGetSchema.setManageCom(mLPGrpContSchema.getManageCom());
        tLJAGetSchema.setApproveCode(mLPGrpContSchema.getApproveCode());
        tLJAGetSchema.setApproveDate(mLPGrpContSchema.getApproveDate());
        tLJAGetSchema.setAgentCode(mLPGrpContSchema.getAgentCode());
        tLJAGetSchema.setAgentGroup(mLPGrpContSchema.getAgentGroup());
        tLJAGetSchema.setAgentCom(mLPGrpContSchema.getAgentCom());
        tLJAGetSchema.setAgentType(mLPGrpContSchema.getAgentType());
        tLJAGetSchema.setAppntNo(mLPGrpContSchema.getAppntNo());
        tLJAGetSchema.setSumGetMoney(Math.abs(mSumGetMoney));
        tLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
        tLJAGetSchema.setOperator(mOperator);
        tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
        tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
        tLJAGetSchema.setPayMode("4");
        tLJAGetSchema.setEnterAccDate(this.mCurDate);
        tLJAGetSchema.setConfDate(this.mCurDate);

        map.put("delete from LJAGet where otherNo = '"
                + tLJAGetSchema.getOtherNo() + "' ",
                "DELETE");
        map.put(tLJAGetSchema, "INSERT");

        map.add(getLJAPayInfo(tLJAGetSchema));
        map.add(getLJTempClass(tLJAGetSchema));

        return true;
    }

    /**
     * 生成暂收费分类表
     * @param tLJAGetSchema LJAGetSchema
     * @param tLPGrpContSchema LPGrpContSchema
     * @return MMap
     */
    private MMap getLJTempClass(LJAGetSchema tLJAGetSchema)
    {
        MMap tMMap = new MMap();

        LJTempFeeClassSchema LJTempFeeClassSchema = new LJTempFeeClassSchema();
        LJTempFeeClassSchema.setTempFeeNo(this.getGetNoticeNo());
        LJTempFeeClassSchema.setPayMode("5"); //内部转帐
        LJTempFeeClassSchema.setPayMoney(tLJAGetSchema.getSumGetMoney());
        LJTempFeeClassSchema.setAppntName(mLPGrpContSchema.getGrpName());
        LJTempFeeClassSchema.setPayDate(this.mCurDate);
        LJTempFeeClassSchema.setConfDate(mCurDate);
        LJTempFeeClassSchema.setApproveDate(mCurDate);
        LJTempFeeClassSchema.setEnterAccDate(mCurDate);
        LJTempFeeClassSchema.setConfFlag("1");
        LJTempFeeClassSchema.setSerialNo(this.getSerialNo());
        LJTempFeeClassSchema.setManageCom(mLPGrpContSchema.getManageCom());
        LJTempFeeClassSchema.setConfMakeDate(mCurDate);
        LJTempFeeClassSchema.setConfMakeTime(this.mCurTime);
        LJTempFeeClassSchema.setOperator(mOperator);
        PubFun.fillDefaultField(LJTempFeeClassSchema);
        tMMap.put(LJTempFeeClassSchema, SysConst.INSERT);

        return tMMap;
    }

    //得到实收保费
    private MMap getLJAPayInfo(LJAGetSchema cLJAGetSchema)
    {
        MMap tMMap = new MMap();

        //应收备份
        LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
        tLJSPayBSchema.setGetNoticeNo(getGetNoticeNo()); // 通知书号
        tLJSPayBSchema.setOtherNo(mLPGrpContSchema.getGrpContNo());
        tLJSPayBSchema.setOtherNoType("1");
        tLJSPayBSchema.setAppntNo(mLPGrpContSchema.getAppntNo());
        tLJSPayBSchema.setSumDuePayMoney(cLJAGetSchema.getSumGetMoney());
        tLJSPayBSchema.setPayDate(this.mCurDate); //有冲突,暂定为取集体险种最早的日期
        tLJSPayBSchema.setStartPayDate(this.mCurDate); //交费最早应缴日期保存上次交至日期/暂定为取集体险种最晚的交至日期
        tLJSPayBSchema.setBankOnTheWayFlag("0");
        tLJSPayBSchema.setBankSuccFlag("0");
        tLJSPayBSchema.setSendBankCount(0);
        tLJSPayBSchema.setApproveCode(mLPGrpContSchema.getApproveCode());
        tLJSPayBSchema.setApproveDate(mLPGrpContSchema.getApproveDate());
        tLJSPayBSchema.setRiskCode(BQ.FILLDATA);
        tLJSPayBSchema.setBankAccNo(mLPGrpContSchema.getBankAccNo());
        tLJSPayBSchema.setBankCode(mLPGrpContSchema.getBankCode());
        tLJSPayBSchema.setSerialNo(this.getSerialNo());
        tLJSPayBSchema.setOperator(mOperator);
        tLJSPayBSchema.setManageCom(mLPGrpContSchema.getManageCom());
        tLJSPayBSchema.setAgentCom(mLPGrpContSchema.getAgentCom());
        tLJSPayBSchema.setAgentCode(mLPGrpContSchema.getAgentCode());
        tLJSPayBSchema.setAgentType(mLPGrpContSchema.getAgentType());
        tLJSPayBSchema.setAgentGroup(mLPGrpContSchema.getAgentGroup());
        tLJSPayBSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSPayBSchema.setMakeTime(mCurTime);
        tLJSPayBSchema.setModifyDate(mCurDate);
        tLJSPayBSchema.setModifyTime(mCurTime);
        tLJSPayBSchema.setDealState(FeeConst.DEALSTATE_URGESUCCEED);
        tLJSPayBSchema.setConfFlag("1");
        tMMap.put(tLJSPayBSchema, SysConst.INSERT);

        //实收
        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        tLJAPaySchema.setPayNo(getPayNo()); //交费收据号码
        tLJAPaySchema.setIncomeNo(mLPGrpContSchema.getGrpContNo()); //应收/实收编号,集体合同号
        tLJAPaySchema.setIncomeType("1"); //应收/实收编号类型
        tLJAPaySchema.setAppntNo(mLPGrpContSchema.getAppntNo()); //  投保人客户号码
        tLJAPaySchema.setSumActuPayMoney(cLJAGetSchema.getSumGetMoney()); // 总实交金额
        tLJAPaySchema.setGetNoticeNo(getGetNoticeNo()); //交费收据号,暂时先取应收总表的通知号
        tLJAPaySchema.setEnterAccDate(this.mCurDate); // 到帐日期
        tLJAPaySchema.setPayDate(this.mCurDate); //交费日期,仍然取了最小日期
        tLJAPaySchema.setConfDate(this.mCurDate); //确认日期
        tLJAPaySchema.setApproveCode(mLPGrpContSchema.getApproveCode()); //复核人编码
        tLJAPaySchema.setApproveDate(mLPGrpContSchema.getApproveDate()); //  复核日期
        tLJAPaySchema.setSerialNo(getSerialNo()); //流水号
        tLJAPaySchema.setOperator(mOperator); // 操作员
        tLJAPaySchema.setMakeDate(PubFun.getCurrentDate()); //入机时间
        tLJAPaySchema.setMakeTime(this.mCurTime); //入机时间
        tLJAPaySchema.setModifyDate(this.mCurDate); //最后一次修改日期
        tLJAPaySchema.setModifyTime(this.mCurTime); //最后一次修改时间
        tLJAPaySchema.setRiskCode(BQ.FILLDATA); // 险种编码
        tLJAPaySchema.setManageCom(mLPGrpContSchema.getManageCom());
        tLJAPaySchema.setAgentCom(mLPGrpContSchema.getAgentCom());
        tLJAPaySchema.setAgentType(mLPGrpContSchema.getAgentType());
        tLJAPaySchema.setBankCode(mLPGrpContSchema.getBankCode());
        tLJAPaySchema.setBankAccNo(mLPGrpContSchema.getBankAccNo());
        tLJAPaySchema.setAccName(mLPGrpContSchema.getAccName());
        tLJAPaySchema.setStartPayDate(this.mCurDate);
        tLJAPaySchema.setAgentCode(mLPGrpContSchema.getAgentCode());
        tLJAPaySchema.setAgentGroup(mLPGrpContSchema.getAgentGroup());
        tMMap.put(tLJAPaySchema, SysConst.INSERT);

        return tMMap;
    }

    /**
     * 生成暂收数据
     * @param tLJAGetEndorseSchema LJAGetEndorseSchema
     * @param tLPGrpPolSchema LPGrpPolSchema
     * @return MMap
     */
    private MMap getLJTempFee(LJAGetEndorseSchema tLJAGetEndorseSchema,
                              LPGrpPolSchema tLPGrpPolSchema)
    {
        MMap tMMap = new MMap();

        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setTempFeeNo(this.getGetNoticeNo());
        tLJTempFeeSchema.setTempFeeType("2");
        tLJTempFeeSchema.setRiskCode(tLPGrpPolSchema.getRiskCode());
        tLJTempFeeSchema.setPayIntv(tLPGrpPolSchema.getPayIntv());
        tLJTempFeeSchema.setOtherNo(tLPGrpPolSchema.getGrpPolNo());
        tLJTempFeeSchema.setOtherNoType("1");
        tLJTempFeeSchema.setPayMoney(Math.abs(tLJAGetEndorseSchema.getGetMoney()));
        tLJTempFeeSchema.setPayDate(this.mCurDate);
        tLJTempFeeSchema.setEnterAccDate(mCurDate);
        tLJTempFeeSchema.setConfDate(mCurDate);
        tLJTempFeeSchema.setConfMakeDate(mCurDate);
        tLJTempFeeSchema.setConfMakeTime(this.mCurTime);
        tLJTempFeeSchema.setSaleChnl(tLPGrpPolSchema.getSaleChnl());
        tLJTempFeeSchema.setManageCom(tLPGrpPolSchema.getManageCom());
        tLJTempFeeSchema.setAgentCom(tLPGrpPolSchema.getAgentCom());
        tLJTempFeeSchema.setAgentType(tLPGrpPolSchema.getAgentType());
        tLJTempFeeSchema.setAgentGroup(tLPGrpPolSchema.getAgentGroup());
        tLJTempFeeSchema.setAgentCode(tLPGrpPolSchema.getAgentCode());
        tLJTempFeeSchema.setConfFlag("1");
        tLJTempFeeSchema.setSerialNo(this.getSerialNo());
        tLJTempFeeSchema.setOperator(mOperator);
        PubFun.fillDefaultField(tLJTempFeeSchema);
        tMMap.put(tLJTempFeeSchema, SysConst.INSERT);

        return tMMap;
    }

    /**
     * 生成险种收费记录LJSPayGrpB,LJAPayGrp
     * @param tLJAGetEndorseSchema LJAGetEndorseSchema
     * @param tLPGrpPolSchema LPGrpPolSchema
     * @return MMap
     */
    private MMap getLJAPayGrp(LJAGetEndorseSchema tLJAGetEndorseSchema,
                              LPGrpPolSchema tLPGrpPolSchema)
    {
        MMap tMMap = new MMap();

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(tLPGrpPolSchema.getGrpPolNo());
        if(!tLCGrpPolDB.getInfo())
        {
            return null;
        }

        //团险
        LJSPayGrpBSchema tLJSPayGrpBSchema = new LJSPayGrpBSchema();
        tLJSPayGrpBSchema.setGrpPolNo(tLPGrpPolSchema.getGrpPolNo());
        tLJSPayGrpBSchema.setGrpContNo(tLPGrpPolSchema.getGrpContNo());
        tLJSPayGrpBSchema.setAppntNo(tLJAGetEndorseSchema.getAppntNo()); //随便取一个lcpol的投保人
        tLJSPayGrpBSchema.setPayCount(1); //交费次数
        tLJSPayGrpBSchema.setGetNoticeNo(this.getGetNoticeNo()); // 通知书号
        tLJSPayGrpBSchema.setSumDuePayMoney(Math.abs(tLJAGetEndorseSchema.
            getGetMoney()));
        tLJSPayGrpBSchema.setSumActuPayMoney(tLJSPayGrpBSchema.
                                             getSumDuePayMoney());
        tLJSPayGrpBSchema.setPayIntv(tLPGrpPolSchema.getPayIntv());
        tLJSPayGrpBSchema.setPayDate(this.mCurDate); //交费日期
        tLJSPayGrpBSchema.setPayType("ZC"); //交费类型=ZC 正常交费
        tLJSPayGrpBSchema.setLastPayToDate(tLPGrpPolSchema.getPaytoDate());
        tLJSPayGrpBSchema.setCurPayToDate(tLCGrpPolDB.getPaytoDate());
        tLJSPayGrpBSchema.setApproveCode(tLPGrpPolSchema.getApproveCode());
        tLJSPayGrpBSchema.setApproveDate(tLPGrpPolSchema.getApproveDate());
        tLJSPayGrpBSchema.setApproveTime(tLPGrpPolSchema.getApproveTime());
        tLJSPayGrpBSchema.setManageCom(tLCGrpPolDB.getManageCom());
        tLJSPayGrpBSchema.setAgentCom(tLPGrpPolSchema.getAgentCom());
        tLJSPayGrpBSchema.setAgentType(tLPGrpPolSchema.getAgentType());
        tLJSPayGrpBSchema.setRiskCode(tLPGrpPolSchema.getRiskCode());
        tLJSPayGrpBSchema.setSerialNo(this.getSerialNo());
        tLJSPayGrpBSchema.setInputFlag("1");
        tLJSPayGrpBSchema.setConfFlag("1");
        tLJSPayGrpBSchema.setOperator(mOperator);
        tLJSPayGrpBSchema.setAgentCode(tLPGrpPolSchema.getAgentCode());
        tLJSPayGrpBSchema.setAgentGroup(tLPGrpPolSchema.getAgentGroup());
        PubFun.fillDefaultField(tLJSPayGrpBSchema);
        tLJSPayGrpBSchema.setDealState(FeeConst.DEALSTATE_URGESUCCEED); //催收状态：待核销
        tMMap.put(tLJSPayGrpBSchema, SysConst.INSERT);

        //团险实收
        LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
        ref.transFields(tLJAPayGrpSchema, tLJSPayGrpBSchema);
        tLJAPayGrpSchema.setPayNo(getPayNo());
        tLJAPayGrpSchema.setEnterAccDate(this.mCurDate);
        tLJAPayGrpSchema.setConfDate(this.mCurDate);
        tMMap.put(tLJAPayGrpSchema, SysConst.INSERT);

        //团险下个险
        LBInsureAccClassDB tLBInsureAccClassDB = new LBInsureAccClassDB();
        tLBInsureAccClassDB.setGrpPolNo(mBGrpPolNo);
        LBInsureAccClassSet tLBInsureAccClassSet = tLBInsureAccClassDB.query();
        for(int i = 1; i <= tLBInsureAccClassSet.size(); i++)
        {
            LBInsureAccClassSchema classchema = tLBInsureAccClassSet.get(i);

            //得到DutyCode与PlanPlanCode的关系
            LMDutyPayRelaDB tLMDutyPayRelaDB = new LMDutyPayRelaDB();
            tLMDutyPayRelaDB.setPayPlanCode(classchema.getPayPlanCode());
            LMDutyPayRelaSet set = tLMDutyPayRelaDB.query();

            //生成个人应收
            LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
            tLJAPayPersonSchema.setPolNo(classchema.getPolNo()); // 保单号码
            tLJAPayPersonSchema.setPayCount("1"); // 第几次交费
            tLJAPayPersonSchema.setGrpContNo(classchema.getGrpContNo()); // 集体保单号码
            tLJAPayPersonSchema.setGrpPolNo(classchema.getGrpPolNo()); // 集体保单号码
            tLJAPayPersonSchema.setContNo(classchema.getContNo()); // 总单/合同号码
            tLJAPayPersonSchema.setAppntNo(tLJSPayGrpBSchema.getAppntNo()); // 投保人客户号码
            tLJAPayPersonSchema.setPayNo(getPayNo()); // 交费收据号码
            tLJAPayPersonSchema.setPayTypeFlag("1");
            tLJAPayPersonSchema.setPayAimClass("2"); // 交费目的分类
            tLJAPayPersonSchema.setDutyCode(set.get(1).getDutyCode()); // 责任编码
            tLJAPayPersonSchema.setPayPlanCode(classchema.getPayPlanCode()); // 交费计划编码
            tLJAPayPersonSchema.setSumDuePayMoney(classchema.getInsuAccBala()); // 总应交金额
            tLJAPayPersonSchema.setSumActuPayMoney(classchema.getInsuAccBala()); // 总实交金额
            tLJAPayPersonSchema.setPayIntv(tLPGrpPolSchema.getPayIntv()); // 交费间隔
            tLJAPayPersonSchema.setPayDate(this.mCurDate); // 交费日期
            tLJAPayPersonSchema.setPayType("ZC"); // 交费类型
            tLJAPayPersonSchema.setEnterAccDate(this.mCurDate); // 到帐日期
            tLJAPayPersonSchema.setConfDate(this.mCurDate); // 确认日期
            tLJAPayPersonSchema.setLastPayToDate(tLJSPayGrpBSchema.
                                                 getLastPayToDate()); // 原交至日期
            tLJAPayPersonSchema.setCurPayToDate(tLJSPayGrpBSchema.
                                                getCurPayToDate()); // 现交至日期
            tLJAPayPersonSchema.setApproveCode(tLPGrpPolSchema.getApproveCode()); // 复核人编码
            tLJAPayPersonSchema.setApproveDate(tLPGrpPolSchema.getApproveDate()); // 复核日期
            tLJAPayPersonSchema.setApproveTime(tLPGrpPolSchema.getApproveTime()); // 复核时间
            tLJAPayPersonSchema.setAgentCode(tLPGrpPolSchema.getAgentCode());
            tLJAPayPersonSchema.setAgentGroup(tLPGrpPolSchema.getAgentGroup());
            tLJAPayPersonSchema.setSerialNo(this.getSerialNo()); // 流水号
            tLJAPayPersonSchema.setOperator(mOperator); // 实际截转为实收的操作员
            tLJAPayPersonSchema.setMakeDate(PubFun.getCurrentDate()); // 入机日期
            tLJAPayPersonSchema.setMakeTime(this.mCurTime); // 入机时间
            tLJAPayPersonSchema.setModifyDate(mCurDate); // 最后一次修改日期
            tLJAPayPersonSchema.setModifyTime(this.mCurTime); // 最后一次修改时间
            tLJAPayPersonSchema.setGetNoticeNo(this.getGetNoticeNo()); // 通知书号码
            tLJAPayPersonSchema.setManageCom(tLPGrpPolSchema.getManageCom()); // 管理机构
            tLJAPayPersonSchema.setAgentCom(tLPGrpPolSchema.getAgentCom()); // 代理机构
            tLJAPayPersonSchema.setAgentType(tLPGrpPolSchema.getAgentType()); // 代理机构内部分类
            tLJAPayPersonSchema.setRiskCode(tLPGrpPolSchema.getRiskCode()); // 险种编码
            tMMap.put(tLJAPayPersonSchema, SysConst.INSERT);
        }

        return tMMap;
    }

    /**
     * 得到应收收据号
     * @return String
     */
    private String getPayNo()
    {
        if(mPayNo == null)
        {
            mPayNo = PubFun1.CreateMaxNo("PayNo", PubFun.getNoLimit(mManageCom));
        }
        return mPayNo;
    }

    /**
     * 得到实收序号
     * @return String
     */
    private String getSerialNo()
    {
        if(mSerialNo == null)
        {
            mSerialNo = PubFun1.CreateMaxNo("SERIALNO",
                                            PubFun.getNoLimit(mManageCom));
        }
        return mSerialNo;
    }

    /**
     * 得到应收记录号
     * @return String
     */
    private String getGetNoticeNo()
    {
        if(mGetNoticeNo == null)
        {
            mGetNoticeNo = PubFun1.CreateMaxNo("PayNoticeNo",
                                               PubFun.getNoLimit(mManageCom));
        }
        return mGetNoticeNo;
    }

    /**
     * 得到退费号
     * @return String
     */
    private String getActuGetNo()
    {
        if(this.mGetNo == null || this.mGetNo.equals(""))
        {
            this.mGetNo = PubFun1.CreateMaxNo("GetNO", null);
        }

        return mGetNo;
    }

    /**
     * 计算险种退费
     * @return double
     */
    private double getGetMoney()
    {
        String detailTypes = "";
        String[] detailType =
            {BQ.ACCBALA_FIEXD, BQ.DETAILTYPE_INTEREST_FIXED,
            BQ.ACCBALA_GROUP, BQ.DETAILTYPE_INTEREST_GROUP,
            BQ.ACCBALA_INSURED, BQ.DETAILTYPE_INTEREST_INSURED};

        for(int i = 0; i < detailType.length; i++)
        {
            detailTypes += "'" + detailType[i] + "',";
        }
        detailTypes = detailTypes.substring(0, detailTypes.lastIndexOf(","));

        StringBuffer sql = new StringBuffer();
        sql.append("select sum(decimal(edorValue, 20, 3)) ")
            .append("from LPEdorEspecialData ")
            .append("where edorAcceptNo = '")
            .append(mEdorNo)
            .append("'  and edorType = '")
            .append(BQ.EDORTYPE_MJ)
            .append("'  and detailType in (").append(detailTypes).append(
                ")");
        ExeSQL tExeSQL = new ExeSQL();
        String result = tExeSQL.getOneValue(sql.toString());
        if(result == null || result.equals(""))
        {
            result = "0";
        }

        return PubFun.setPrecision(Double.parseDouble(result), "0.00");
    }

    public static void main(String[] args)
    {
        LPGrpContDB db = new LPGrpContDB();
        db.setEdorNo("20060125000008");
        db.setEdorType("MJ");
        db.setGrpContNo("0000020101");
        db.getInfo();

        MaintainAccFiance bl = new MaintainAccFiance();
        bl.mOperator = "pa1101";
        MMap tMMap = bl.setFiance(db.getSchema());

        VData d = new VData();
        d.add(tMMap);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            System.out.println(p.mErrors.getErrContent());
        }
    }
}
