package com.sinosoft.lis.bq;

import java.sql.*;
import java.util.*;

/**
 * <p>Title: 保全团险新增被保人保存申请</p>
 * <p>Description: 把保全状态置为申请确认状态</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;

public class GrpEdorTZCheckBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private MMap mMap = new MMap();

    private VData mResult = new VData();

    private String mGrpContNo;

    private String mEdorNo;

    List mOutputList = new ArrayList();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局基础数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();

    public GrpEdorTZCheckBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData VData 传入的数据
     * @param cOperate String 数据操作字符串
     * @return boolean  true--提交成功, false--提交失败
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getSubmitData(cInputData, cOperate))
        {
            return false;
        }

        //数据提交
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpEdorNICheckBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public boolean getSubmitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @return boolean
     */
    private boolean getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mGrpContNo = (String) mInputData.get(1);
        mEdorNo = (String) mInputData.get(2);
        return true;
    }

    private boolean getInsuredList()
    {
        String sql = "select * from LCInsuredList " +
                "where GrpContNo = '" + mGrpContNo + "' " +
                "and State = '0' " +
                "and EdorNo = '" + mEdorNo + "' " +
                "and (ContPlanCode <> 'FM' or ContPlanCode is null) ";
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        mLCInsuredListSet = tLCInsuredListDB.executeQuery(sql);
        System.out.println(sql);
        if (mLCInsuredListSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "GrpEdorNICheckBL";
            tError.functionName = "submitData";
            tError.errorMessage = "查不到导入的数据！";
            mErrors.addOneError(tError);
            return false;
        }
        for(int i=1;i<=mLCInsuredListSet.size();i++)
        {
        	if(mLCInsuredListSet.get(i).getAppntPrem()!=0
        			||mLCInsuredListSet.get(i).getPersonPrem()!=0.0
        			||mLCInsuredListSet.get(i).getPersonOwnPrem()!=0.0
        		)
        	{
        		CError tError = new CError();
                tError.moduleName = "GrpEdorNICheckBL";
                tError.functionName = "submitData";
                tError.errorMessage = "万能增人不会产生费用，请调整模板后重新导入模板。";
                mErrors.addOneError(tError);
                return false;
        	}
        }
        return true;
    }

    /*
     *校验关系录入是否正确，避免造成重复投保
     *2007-3-20 15:35 qulq add
     */

    private boolean checkImportData()
    {
    	String sql = "select InsuredId,InsuredName from LCInsuredList where Grpcontno ='"+mGrpContNo+"'"+
                         " and EdorNo ='"+mEdorNo+"'"+
                         " and EmployeeName != InsuredName and Relation = '00'";
    	ExeSQL checkSQL = new ExeSQL();

    	SSRS rs = checkSQL.execSQL(sql);
        if(rs==null||rs.getMaxRow()==0)
        {
            return true;
        }
        else
        {

            for(int i = 1;i<=rs.getMaxRow();i++)
            {
                TransferData tTransferData = new TransferData();
                tTransferData.setNameAndValue("INSUREDNO",
                                              StrTool.cTrim(rs.GetText(i,1)));
                tTransferData.setNameAndValue("INSUREDNAME",
                                              StrTool.cTrim(rs.GetText(i,2)));
                mOutputList.add(tTransferData);
            }
            return false;
        }

    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return boolean 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	
        if (!getInsuredList())
        {
        	return false;
        }
        boolean ret = true;
        if(!checkImportData())
        {
            mOperate = "RELATION";
            ret = false;
        }
        if (mOperate.equals("CONTPLAN"))
        {
            if (!checkContPlan())
            {
                ret = false;
            }
        }
        else if (mOperate.equals("WAITPERIOD"))
        {
            if (!checkWaitPeriod())
            {
                ret = false;
            }
        }
        else if (mOperate.equals("INSURED"))
        {
            if (!checkInsured())
            {
                ret = false;
            }
        }
        mResult.add(mOperate);
        mResult.add(mOutputList);
        return ret;
    }

    /**
     * 判断剩余保险期间是否超过等待期
     * @param contPlanSet HashSet
     * @return boolean
     */
    private boolean checkWaitPeriod()
    {
        boolean ret = true;
        int currentPeriod = getCurrentPeriod(); //得到已过保险期

        //得到险种，用HashSet是为了除去重复的保障计划
        HashSet contPlanSet = new HashSet();
        for (int i = 1; i <= mLCInsuredListSet.size(); i++)
        {
            contPlanSet.add(mLCInsuredListSet.get(i).getContPlanCode());
        }
        HashSet riskCodeSet = getRiskCode(contPlanSet); //根据保障计划得到险种
        Iterator it = riskCodeSet.iterator();
        while(it.hasNext())
        {
            String riskCode = (String)it.next();
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(riskCode);
            tLMRiskAppDB.getInfo();
            int maxEndPeriod = tLMRiskAppDB.getMaxEndPeriod();
            if (maxEndPeriod == 0) //无等待期
            {
                continue;
            }
            else if (maxEndPeriod == -1) //从页面录入等待期
            {
                LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
                tLCGrpPolDB.setGrpContNo(mGrpContNo);
                tLCGrpPolDB.setRiskCode(riskCode);
                LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
                for (int i = 1; i <= tLCGrpPolSet.size(); i++)
                {
                    if (tLCGrpPolSet.get(i).getWaitPeriod() < currentPeriod)
                    {
                        //添加返回结果
                        TransferData tTransferData = new TransferData();
                        tTransferData.setNameAndValue("FLAG", "WAITPERIOD");
                        tTransferData.setNameAndValue("RISKCODE",
                                StrTool.cTrim(tLMRiskAppDB.getRiskCode()));
                        tTransferData.setNameAndValue("RISKNAME",
                                StrTool.cTrim(tLMRiskAppDB.getRiskName()));
                        mOutputList.add(tTransferData);
                        ret = false;
                    }
                }
            }
            else //maxEndPeriod就是等待期
            {
                if (maxEndPeriod < currentPeriod)
                {
                    //添加返回结果
                    TransferData tTransferData = new TransferData();
                    tTransferData.setNameAndValue("FLAG", "WAITPERIOD");
                    tTransferData.setNameAndValue("RISKCODE",
                            tLMRiskAppDB.getRiskCode());
                    tTransferData.setNameAndValue("RISKNAME",
                            tLMRiskAppDB.getRiskName());
                    mOutputList.add(tTransferData);
                    ret = false;
                }
            }
        }
        return ret;
    }

    /**
     * 得到当前日期距保单生效日期已过几天
     * @return int
     */
    private int getCurrentPeriod()
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        tLCGrpContDB.getInfo();
        FDate tFDate = new FDate();
        System.out.println(tLCGrpContDB.getCValiDate());
        java.util.Date cInValiDate = tFDate.getDate(tLCGrpContDB.getCValiDate());
        java.util.Date currentDate = new java.util.Date();
        long currentPerod = currentDate.getTime() - cInValiDate.getTime();
        return (int) (currentPerod) / (1000*60*60*24);
    }

    /**
     * 根据保障计划得到险种代码
     * @param contPlanSet HashSet
     * @return List
     */
    private HashSet getRiskCode(HashSet contPlanSet)
    {
        HashSet riskCodeSet = new HashSet();
        Iterator it = contPlanSet.iterator();
        while(it.hasNext())
        {
            String contPlanCode = (String) it.next();
            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
            tLCContPlanRiskDB.setGrpContNo(mGrpContNo);
            tLCContPlanRiskDB.setContPlanCode(contPlanCode);
            LCContPlanRiskSet tLCContPlanRiskSet = tLCContPlanRiskDB.query();
            for (int i = 1; i <= tLCContPlanRiskSet.size(); i++)
            {
                riskCodeSet.add(tLCContPlanRiskSet.get(i).getRiskCode());
            }
        }
        return riskCodeSet;
    }

    /**
     * 检查客户资料是否重复，一旦有客户资料重复就返回
     * @return boolean
     */
    private boolean checkInsured()
    {
        boolean ret = true;
        for (int i = 1; i <= mLCInsuredListSet.size(); i++)
        {
            LCInsuredListSchema tLCInsuredListSchema = mLCInsuredListSet.get(i);
            String othIdNo = tLCInsuredListSchema.getOthIDNo();
            String sql;
            if ((othIdNo == null) || (othIdNo.equals("")))
            {
                sql = "select * from LCInsured " +
                        "where Name = '" + tLCInsuredListSchema.getInsuredName() +
                        "' " +
                        "and Sex = '" + tLCInsuredListSchema.getSex() + "' " +
                        "and Birthday = '" + tLCInsuredListSchema.getBirthday() +
                        "' " +
                        "union " +
                        "select * from LCInsured " +
                        "where IdNo = '" + tLCInsuredListSchema.getIDNo() +
                        "' ";
            }
            else
            {
                sql = "select * from LCInsured " +
                        "where GrpContNo = '" +
                        tLCInsuredListSchema.getGrpContNo() + "' " +
                        "and Name = '" + tLCInsuredListSchema.getInsuredName() +
                        "' " +
                        "and OthIdNo = '" + tLCInsuredListSchema.getOthIDNo();
            }
            try
            {
                Connection con = DBConnPool.getConnection();
                PreparedStatement pstmt = con.prepareStatement(sql,
                        ResultSet.TYPE_FORWARD_ONLY,
                        ResultSet.CONCUR_READ_ONLY);
                System.out.println(sql);
                ResultSet rs = pstmt.executeQuery();
                if (rs.next())
                {
                    ret = false;
                }

                if (rs != null)
                {
                    rs.close();
                }
                if (pstmt != null)
                {
                    pstmt.close();
                }
                con.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
        return ret;
    }

    /**
     * 判断是否已有保障计划，如果姓名，性别，生日，证件类型，证件号码都一样被认为是同一客户
     * @param tLCInsuredListSchema LCInsuredListSchema
     * @return boolean
     */
    private boolean checkContPlan()
    {
        boolean ret = true;
        for (int i = 1; i <= mLCInsuredListSet.size(); i++)
        {
            LCInsuredListSchema tLCInsuredListSchema = mLCInsuredListSet.get(i);
            String othIdNo = tLCInsuredListSchema.getOthIDNo();

            String sql;
            if ((othIdNo == null) || (othIdNo.equals("")))
            {
                sql = "select * from LCInsured a, LCCont b " +
                    "where a.GrpContNo = b.GrpContNo " +
                    "and a.ContNo = b.ContNo " +
                    "and a.GrpContNo = '" + tLCInsuredListSchema.getGrpContNo() +
                    "' " +
                    "and (b.AppFlag = '1' or b.AppFlag = '2')" +
                    "and Name = '" + tLCInsuredListSchema.getInsuredName() +
                    "' " +
                    "and Sex = '" + tLCInsuredListSchema.getSex() + "' " +
                    "and Birthday = '" + tLCInsuredListSchema.getBirthday() +
                    "' " +
                    //"and IdType = '" + tLCInsuredListSchema.getIDType() + "' " +
                    //"and IdNo = '" + tLCInsuredListSchema.getIDNo() + "' " +
                    "and ContPlanCode is not null ";
            }
            else
            {
                sql = "select * from LCInsured " +
                        "where GrpContNo = '" +
                        tLCInsuredListSchema.getGrpContNo() + "' " +
                        "and Name = '" + tLCInsuredListSchema.getInsuredName() +
                        "' " +
                        "and OthIdNo = '" + tLCInsuredListSchema.getOthIDNo() +
                        "' " +
                        "and ContPlanCode is not null ";
            }
            Connection con = null;
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            try
            {
                con = DBConnPool.getConnection();
                pstmt = con.prepareStatement(sql,
                        ResultSet.TYPE_FORWARD_ONLY,
                        ResultSet.CONCUR_READ_ONLY);
                rs = pstmt.executeQuery();

                if (rs.next())
                {
                    //添加返回结果
                    TransferData tTransferData = new TransferData();
                    tTransferData.setNameAndValue("INSUREDNO",
                            StrTool.cTrim(rs.getString("InsuredNo")));
                    tTransferData.setNameAndValue("INSUREDNAME",
                            StrTool.cTrim(rs.getString("Name")));
                    tTransferData.setNameAndValue("IDTYPE",
                            StrTool.cTrim(rs.getString("IdType")));
                    tTransferData.setNameAndValue("Sex",
                            StrTool.cTrim(rs.getString("Sex")));
                    tTransferData.setNameAndValue("Birthday",
                            StrTool.cTrim(rs.getString("Birthday")));
                   // tTransferData.setNameAndValue("IDNO",
                   //         StrTool.cTrim(rs.getString("IdNo")));
                    mOutputList.add(tTransferData);
                    ret = false;
                }

                rs.close();
                pstmt.close();
                con.close();
            }
            catch (Exception e)
            {
            	ret = false;
            	try {
					pstmt.close();
                } catch (Exception e1) {}
                try {
					con.close();
                } catch (Exception e1) {}
            	try {
					rs.close();
				} catch (Exception e1) {}
                e.printStackTrace();
            }
        }
        return ret;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.add(mMap);
        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

}
