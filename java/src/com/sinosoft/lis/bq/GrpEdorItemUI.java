package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p> Title: Web业务系统 </p>
 * <p> Description: UI功能类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class GrpEdorItemUI
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();


    /** 往前面传输数据的容器 */
    private VData mResult = new VData();


    /** 数据操作字符串 */
    private String mOperate;


    /** 错误处理类 */
    public CErrors mErrors = new CErrors();


    // @Constructor
    public GrpEdorItemUI()
    {}


    // @Main
    public static void main(String[] args)
    {
        LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
        tLPGrpEdorMainSchema.setEdorNo("20070312000003");
        tLPGrpEdorMainSchema.setEdorAcceptNo(tLPGrpEdorMainSchema.getEdorNo());
        tLPGrpEdorMainSchema.setGrpContNo("0000350201");
        tLPGrpEdorMainSchema.setEdorAppDate("2007-03-12");
        tLPGrpEdorMainSchema.setEdorValiDate(tLPGrpEdorMainSchema.getEdorAppDate());

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();
        tLPGrpEdorItemSchema.setEdorAcceptNo(tLPGrpEdorMainSchema.getEdorNo());
        tLPGrpEdorItemSchema.setEdorNo(tLPGrpEdorMainSchema.getEdorNo());
        tLPGrpEdorItemSchema.setEdorAppNo(tLPGrpEdorMainSchema.getEdorNo());
        tLPGrpEdorItemSchema.setEdorType("AC");
        tLPGrpEdorItemSchema.setGrpContNo(tLPGrpEdorMainSchema.getGrpContNo());
        tLPGrpEdorItemSchema.setEdorAppDate(tLPGrpEdorMainSchema.getEdorValiDate());
        tLPGrpEdorItemSchema.setEdorValiDate(tLPGrpEdorMainSchema.getEdorValiDate());
        tLPGrpEdorItemSchema.setManageCom("86");
        tLPGrpEdorItemSet.add(tLPGrpEdorItemSchema);

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.Operator = "test";

        VData tVData = new VData();
        tVData.add(tLPGrpEdorMainSchema);
        tVData.add(tLPGrpEdorItemSet);
        tVData.add(mGlobalInput);
        GrpEdorItemUI tGrpEdorItemUI = new GrpEdorItemUI();
        if (tGrpEdorItemUI.submitData(tVData, "INSERT||GRPEDORITEM") == false)
        {
            System.out.println(tGrpEdorItemUI.mErrors.getFirstError().toString());

        }

    }


    // @Method
    /**
     * 数据提交方法
     * @param: cInputData 传入的数据
     *		  cOperate 数据操作字符串
     * @return: boolean
     **/
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 数据操作字符串拷贝到本类中
        this.mOperate = cOperate;

        GrpEdorItemBL tGrpEdorItemBL = new GrpEdorItemBL();

        System.out.println("---UI BEGIN---");
        if (tGrpEdorItemBL.submitData(cInputData, mOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpEdorItemBL.mErrors);
            return false;
        }
        else
        {
            mResult = tGrpEdorItemBL.getResult();
        }
        System.out.print("\n\n\n\n\n\n\n\n" + mErrors.toString());
        return true;
    }


    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult()
    {
        return mResult;
    }

}
