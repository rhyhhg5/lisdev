package com.sinosoft.lis.bq;

import java.io.InputStream;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 被保人清单产生类</p>
 * <p>Description:产生被保人清单 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author qiuyang
 * @version 1.0
 */

public class PrtGrpInsuredListTLBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private String mGrpContNo = null;

    private String mGrpName = null;

    private String mEdorNo = null;

    private String mEdorValiDate = null;

    private GlobalInput mGlobalInput = null;
    
    private XmlExport xml = new XmlExport();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListTLBL(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }
    
    /**
     * 执行操作
     * @return boolean
     */
    public boolean submitData()
    {
       MMap map = getSubmitData();
       if (map == null)
       {
           return false;
       }

       if (!submit(map))
       {
           return false;
       }
       return true;
    }

    /**
     * 得到要提交的数据，不执行submit操作
     * @return MMap
     */
    public MMap getSubmitData()
    {
        if (!prepareData())
        {
            return null;
        }
        xml = createXML();
        return insertXML(xml);
    }
    
    /**
     * 提交业务数据
     * @return boolean
     */
    public boolean getXML()
    {
        if (!prepareData())
        {
            return false;
        }
        xml = createXML();
        return true;
    }

    /**
     * 为生成XML准备数据
     * @return boolean
     */
    private boolean prepareData()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mEdorNo);
        tLPGrpEdorItemDB.setEdorType("TL"); //增人
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            mErrors.addOneError("未找到保全部分领取项目信息！");
            return false;
        }
        //得到团体合同号
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
        this.mGrpContNo = tLPGrpEdorItemSchema.getGrpContNo();
        this.mEdorValiDate = tLPGrpEdorItemSchema.getEdorValiDate();

        //得到团体客户名称
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("未查到团体保单信息！");
            return false;
        }
        this.mGrpName = tLCGrpContDB.getGrpName();
        return true;
    }

    /**
     * 根据清单数据生成XML
     * @param xmlExport XmlExport
     */
    private XmlExport createXML()
    {
    	xml = new XmlExport();
        xml.createDocument("PrtGrpInsuredListTL.vts", "printer");

        //设置批单中TextTag要显示的信息
        TextTag tag = new TextTag();
        tag.add("GrpName", mGrpName);
        tag.add("EdorNo", mEdorNo);
        xml.addTextTag(tag);

        //设置被保人清单
        ListTable listTable = new ListTable();
        listTable.setName("TL");
        LPDiskImportSet tLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, "TL",mGrpContNo, BQ.IMPORTSTATE_SUCC);
        for (int i = 1; i <= tLPDiskImportSet.size(); i ++)
        {
        	LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(i);
            String[] column = new String[9];
//            清单详细字段：序号，被保险人姓名，证件号码，客户号码，部分领取日期，个人缴费部分领取前余额，领取金额，退保费用，个人缴费部分领取后余额。
            column[0] = StrTool.cTrim(String.valueOf(i));
            column[1] = StrTool.cTrim(tLPDiskImportSchema.getInsuredName());
            column[2] = StrTool.cTrim(tLPDiskImportSchema.getIDNo());
            column[3] = StrTool.cTrim(tLPDiskImportSchema.getInsuredNo());
            column[4] = mEdorValiDate;
            column[5] = StrTool.cTrim(tLPDiskImportSchema.getInsuAccBala());;
            column[6] = StrTool.cTrim(Double.toString(tLPDiskImportSchema.getMoney()));
            column[7] = StrTool.cTrim(tLPDiskImportSchema.getInsuAccInterest()+"");
            column[8] = StrTool.cTrim(Double.toString(tLPDiskImportSchema.getGetMoney()));
//            String state = tLCInsuredListSchema.getState();
//            if (state.equals("1"))
//            {
//                column[17] = "有效";
//            }
//            else
//            {
//                column[17] = "无效";
//            }
            listTable.add(column);
        }
        xml.addListTable(listTable, new String[18]);
        xml.outputDocumentToFile("c:\\", "InsuredList");
        return xml;
    }

    /**
     * 把产生的BolbXML插入数据库
     * @param xmlExport XmlExport
     */
    private MMap insertXML(XmlExport xml)
    {
        LPEdorPrint2Schema tLPEdorPrint2Schema = new LPEdorPrint2Schema();
        tLPEdorPrint2Schema.setEdorNo(mEdorNo);
        tLPEdorPrint2Schema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorPrint2Schema.setPrtFlag("N");
        tLPEdorPrint2Schema.setPrtTimes(0);
        tLPEdorPrint2Schema.setEdorInfo(xml.getInputStream());
        tLPEdorPrint2Schema.setOperator(mGlobalInput.Operator);
        tLPEdorPrint2Schema.setMakeDate(PubFun.getCurrentDate());
        tLPEdorPrint2Schema.setMakeTime(PubFun.getCurrentTime());
        tLPEdorPrint2Schema.setModifyDate(PubFun.getCurrentDate());
        tLPEdorPrint2Schema.setModifyTime(PubFun.getCurrentTime());

        MMap map = new MMap();
        map.put(tLPEdorPrint2Schema, "BLOBINSERT");
        return map;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit(MMap map)
    {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "8644";
        tGI.Operator = "pa4403";
        String edorNo = "20090927000461";
        PrtGrpInsuredListTLBL tPrtGrpInsuredList = new PrtGrpInsuredListTLBL(tGI, edorNo);
        tPrtGrpInsuredList.submitData();
    }
}
