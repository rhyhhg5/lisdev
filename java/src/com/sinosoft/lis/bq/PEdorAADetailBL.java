package com.sinosoft.lis.bq;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.tb.*;
import java.util.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体保全集体下个人功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */
public class PEdorAADetailBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;
  private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
  private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
  private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
  private LPDutySchema mLPDutySchema = new LPDutySchema();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private Reflections ref = new Reflections();
  private String currDate = PubFun.getCurrentDate();
  private String currTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public PEdorAADetailBL() {
  }

  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    mOperate = cOperate;

    //得到外部传入的数据
    if (!getInputData())
      return false;
    System.out.println("---End getInputData---");

    //数据校验操作
    if (!checkData())
      return false;
    System.out.println("---End checkData---");

    //数据准备操作
    if (mOperate.equals("INSERT||EDOR")) {
      if (!prepareData())
        return false;
      System.out.println("---End prepareData---");
    }

    //　数据提交、保存
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start tPRnewManualDunBLS Submit...");

    if (!tPubSubmit.submitData(mResult, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);

      CError tError = new CError();
      tError.moduleName = "ContBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";

      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData() {
    try {
      mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName(
          "LPEdorItemSchema", 0);
      mLPDutySchema = (LPDutySchema) mInputData.getObjectByObjectName(
          "LPDutySchema", 0);
      mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
          "GlobalInput", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PEdorAADetailBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 校验传入的数据的合法性
   * @return
   */
  private boolean checkData() {
    LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
    tLPEdorItemDB.setSchema(mLPEdorItemSchema);
    if (!tLPEdorItemDB.getInfo()) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PInsuredBL";
      tError.functionName = "Preparedata";
      tError.errorMessage = "无保全申请数据!";
      System.out.println("------" + tError);
      this.mErrors.addOneError(tError);
      return false;
    }

    //将查询出来的保全主表数据保存至模块变量中，省去其它的重复查询
    mLPEdorItemSchema.setSchema(tLPEdorItemDB.getSchema());
    if (tLPEdorItemDB.getEdorState().trim().equals("2")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PInsuredBL";
      tError.functionName = "Preparedata";
      tError.errorMessage = "该保全已经申请确认不能修改!";
      System.out.println("------" + tError);
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备需要保存的数据
   * @return
   */
  private boolean prepareData() {

    //根据polno dutycode准备LCDutyBLSet
    LCDutyDB tLCDutyDB = new LCDutyDB();
    LCDutySet tLCDutySet = new LCDutySet();
    LCDutySchema tLCDutySchema = new LCDutySchema();
    tLCDutyDB.setDutyCode(mLPDutySchema.getDutyCode());
    tLCDutyDB.setPolNo(mLPEdorItemSchema.getPolNo());
    tLCDutySet = tLCDutyDB.query();
    tLCDutySchema = tLCDutySet.get(1);
    String str1=tLCDutySchema.getInsuYearFlag();
    double initAmnt=tLCDutySchema.getAmnt();
    double initPrem=tLCDutySchema.getPrem();
    if (PubFun.calInterval(mLPEdorItemSchema.getEdorValiDate(),
                           tLCDutySchema.getEndDate(), "D") < 15)
    {
      tLCDutySchema.setInsuYear(PubFun.calInterval
      (mLPEdorItemSchema.getEdorValiDate(),tLCDutySchema.getEndDate(),"D"));
         tLCDutySchema.setInsuYearFlag("D");
    }
   else{
        tLCDutySchema.setInsuYear(PubFun.calInterval
        (mLPEdorItemSchema.getEdorValiDate(),tLCDutySchema.getEndDate(),"M"));
          tLCDutySchema.setInsuYearFlag("M");
}
      tLCDutySchema.setAmnt(mLPDutySchema.getAmnt());
      tLCDutySet.set(1,tLCDutySchema);
      LCDutyBLSet tLCDutyBLSet=new LCDutyBLSet();
      tLCDutyBLSet.set(tLCDutySet);

      //根据polno查polschema,准备LCPolBL
     LCPolDB tLCPolDB = new LCPolDB();
     LCPolSet tLCPolSet = new LCPolSet();
     LCPolSchema tLCPolSchema=new LCPolSchema();
     tLCPolDB.setPolNo(mLPEdorItemSchema.getPolNo());
     tLCPolSet = tLCPolDB.query();
     tLCPolSchema=tLCPolSet.get(1);
     LCPolBL tLCPolBL=new LCPolBL();
     tLCPolBL.setSchema(tLCPolSchema);
     //准备calFlag
     String calFlag="AA";
    //调用calbl类计算prem和get
     LCGetSchema tLCGetSchema=new LCGetSchema();
     LCPremSchema tLCPremSchema=new LCPremSchema();
     LCDutySchema bLCDutySchema=new LCDutySchema();
     LCPolSchema bLCPolSchema=new LCPolSchema();
     CalBL aCalBL= new CalBL(tLCPolBL,tLCDutyBLSet,calFlag);
     if(!aCalBL.calPol())
     {
       System.out.println("相关表信息不全，保费计算错误!");
      }
      bLCPolSchema.setSchema(aCalBL.getLCPol().getSchema());
      bLCDutySchema.setSchema(aCalBL.getLCDuty().get(1));
      tLCGetSchema.setSchema(aCalBL.getLCGet().get(1));
      tLCPremSchema.setSchema(aCalBL.getLCPrem().get(1));
     //调用caloneduty后更新lpduty表
     LPDutySchema tLPDutySchema = new LPDutySchema();
     ref.transFields(tLPDutySchema,bLCDutySchema);
     tLPDutySchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
     tLPDutySchema.setEdorType(mLPEdorItemSchema.getEdorType());
     tLPDutySchema.setDutyCode("0000"+mLPDutySchema.getDutyCode());
     tLPDutySchema.setAmnt(initAmnt+mLPDutySchema.getAmnt());
     tLPDutySchema.setPrem(initPrem+tLPDutySchema.getPrem());
     tLPDutySchema.setInsuYear(PubFun.calInterval
      (mLPEdorItemSchema.getEdorValiDate(),tLCDutySchema.getEndDate(),str1));

    //更新lppol表
     LPPolSchema tLPPolSchema = new LPPolSchema();
     ref.transFields(tLPPolSchema,bLCPolSchema);
     tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
     tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());

     //更新LPPrem表
     LPPremSchema tLPPremSchema = new LPPremSchema();
     ref.transFields(tLPPremSchema,tLCPremSchema);
     tLPPremSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
     tLPPremSchema.setEdorType(mLPEdorItemSchema.getEdorType());
     tLPPremSchema.setOperator(mGlobalInput.Operator);
     tLPPremSchema.setMakeDate(currDate);
     tLPPremSchema.setMakeTime(currTime);
     tLPPremSchema.setModifyDate(currDate);
     tLPPremSchema.setModifyTime(currTime);

   //更新LPGet表
    LPGetSchema tLPGetSchema = new LPGetSchema();
    ref.transFields(tLPGetSchema,tLCGetSchema);
    tLPGetSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
    tLPGetSchema.setEdorType(mLPEdorItemSchema.getEdorType());
    tLPGetSchema.setOperator(mGlobalInput.Operator);
    tLPGetSchema.setMakeDate(currDate);
    tLPGetSchema.setMakeTime(currTime);
    tLPGetSchema.setModifyDate(currDate);
    tLPGetSchema.setModifyTime(currTime);

    //生成批改补退费表
      LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
      tLJSGetEndorseSchema.setGetNoticeNo(mLPEdorItemSchema.getEdorNo()); //给付通知书号码
      tLJSGetEndorseSchema.setEndorsementNo(mLPEdorItemSchema.getEdorNo());
      tLJSGetEndorseSchema.setPolNo(mLPEdorItemSchema.getPolNo());
      tLJSGetEndorseSchema.setFeeOperationType("AA");
      BqCalBL tBqCalBl = new BqCalBL();
      String feeFinaType = tBqCalBl.getFinType("AA", "BF",mLPEdorItemSchema.getPolNo());
      if (feeFinaType.equals("")) {
        // @@错误处理
        CError tError = new CError();
        tError.errorMessage = "在LDCode1中缺少保全财务接口转换类型编码!";
        this.mErrors.addOneError(tError);
        return false;
      }
      tLJSGetEndorseSchema.setFeeFinaType(feeFinaType);
      tLJSGetEndorseSchema.setPayPlanCode(tLPPremSchema.getPayPlanCode()); //无作用
      tLJSGetEndorseSchema.setDutyCode(mLPDutySchema.getDutyCode()); //无作用，但一定要，转ljagetendorse时非空
      tLJSGetEndorseSchema.setOtherNo(mLPDutySchema.getDutyCode()); //无作用
      tLJSGetEndorseSchema.setOtherNoType("3");

      LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
      tLJSGetEndorseDB.setSchema(tLJSGetEndorseSchema);
      if (tLJSGetEndorseDB.getInfo()) {
      tLJSGetEndorseSchema.setAgentCode(tLJSGetEndorseDB.getAgentCode());
      tLJSGetEndorseSchema.setAgentCom(tLJSGetEndorseDB.getAgentCom());
      tLJSGetEndorseSchema.setAgentGroup(tLJSGetEndorseDB.getAgentGroup());
      tLJSGetEndorseSchema.setAgentType(tLJSGetEndorseDB.getAgentType());
      tLJSGetEndorseSchema.setMakeDate(tLJSGetEndorseDB.getMakeDate());
      tLJSGetEndorseSchema.setMakeTime(tLJSGetEndorseDB.getMakeTime());
       }
      else{
      tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
      tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
      }
      tLJSGetEndorseSchema.setContNo(mLPEdorItemSchema.getContNo());
      tLJSGetEndorseSchema.setFeeOperationType(mLPEdorItemSchema.
                                               getEdorType());
      tLJSGetEndorseSchema.setGetDate(mLPEdorItemSchema.getEdorValiDate());
      tLJSGetEndorseSchema.setGetFlag("0");
      tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
      tLJSGetEndorseSchema.setManageCom(mGlobalInput.ManageCom);
      tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
      tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
      tLJSGetEndorseSchema.setGetMoney(bLCDutySchema.getPrem());

    //更新LPEdorItem表
    LPEdorItemSchema tLPEdorItemSchema=new LPEdorItemSchema();
    ref.transFields(tLPEdorItemSchema, mLPEdorItemSchema);
    tLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
    tLPEdorItemSchema.setOperator(mGlobalInput.Operator);
    tLPEdorItemSchema.setUWFlag("0");
    tLPEdorItemSchema.setEdorState("1");
    tLPEdorItemSchema.setMakeDate(currDate);
    tLPEdorItemSchema.setMakeTime(currTime);
    tLPEdorItemSchema.setModifyDate(currDate);
    tLPEdorItemSchema.setModifyTime(currTime);
    tLPEdorItemSchema.setChgAmnt(mLPDutySchema.getAmnt());

    //生成LPEdorMain表
             LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
             tLPEdorMainDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
             tLPEdorMainDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
             tLPEdorMainDB.setContNo(mLPEdorItemSchema.getContNo());
             LPEdorMainSet tLPEdorMainSet = new LPEdorMainSet();
             tLPEdorMainSet = tLPEdorMainDB.query();
             if (tLPEdorMainDB.mErrors.needDealError())
             {
                 CError.buildErr(this, "查询个人保全主表失败!");
                 return false;
             }
             LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
             if (tLPEdorMainSet.size() == 0)
             {   ref.transFields(tLPEdorMainSchema, mLPEdorItemSchema);
                 tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
                 tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
                 tLPEdorMainSchema.setEdorState("1");
                 tLPEdorMainSchema.setUWState("0");
                 tLPEdorMainSchema.setMakeDate(currDate);
                 tLPEdorMainSchema.setMakeTime(currTime);
                 tLPEdorMainSchema.setModifyDate(currDate);
                 tLPEdorMainSchema.setModifyTime(currTime);
                 tLPEdorMainSchema.setChgAmnt(mLPDutySchema.getAmnt());
             }
             if (tLPEdorMainSet.size() == 1)
            {
               tLPEdorMainSchema=tLPEdorMainSet.get(1);
               tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
               tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
               tLPEdorMainSchema.setEdorState("1");
               tLPEdorMainSchema.setUWState("0");
               tLPEdorMainSchema.setModifyDate(currDate);
               tLPEdorMainSchema.setModifyTime(currTime);
               tLPEdorMainSchema.setChgAmnt(mLPDutySchema.getAmnt());
            }
        //更新LPGrpEdorItem表
    ref.transFields(mLPGrpEdorItemSchema,tLPEdorItemSchema);

    map.put(mLPGrpEdorItemSchema, "UPDATE");
    map.put(tLPEdorMainSchema, "DELETE&INSERT");
    map.put(tLPEdorItemSchema, "DELETE&INSERT");
    map.put(tLPPolSchema, "DELETE&INSERT");
    map.put(tLPDutySchema, "DELETE&INSERT");
    map.put(tLPPremSchema, "DELETE&INSERT");
    map.put(tLPGetSchema, "DELETE&INSERT");
    map.put(tLJSGetEndorseSchema, "DELETE&INSERT");
    mResult.clear();
    mResult.add(map);
    return true;
  }
}



