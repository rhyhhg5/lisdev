package com.sinosoft.lis.bq;

import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import utils.system;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:个单保全删除BL类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2018
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author XunZhiHan
 * @version 1.0
 */

public class PGrpEdorDeleteBL {
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	/** 传输到后台处理的map */
	private MMap mMap = new MMap();

	/** 数据操作字符串 */
	private String mOperate;
	private String mOperator;
	private String mManageCom;

	private String mEdorNo = null;
	private String mContNo = null;
	/** 刪除判斷 */
	private String delFlag;
	/* 状态 */
	private String mEdorState;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 业务处理相关变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

	private LGWorkSchema mLGWorkSchema = new LGWorkSchema(); // 新的表结构

	TransferData tTransferData = new TransferData();
	// 统一更新日期，时间
	private String theCurrentDate = PubFun.getCurrentDate();
	private String theCurrentTime = PubFun.getCurrentTime();

	public PGrpEdorDeleteBL()
    {
    }

	/**
	 * 处理实际的业务逻辑。
	 * 
	 * @param cInputData
	 *            VData 从前台接收的表单数据
	 * @param cOperate
	 *            String 操作字符串
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将数据取到本类变量中
		mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 将VData数据还原成业务需要的类
		if (this.getInputData() == false) {
			System.out.println("批改状态不可以");
			return false;

		}

		if (!checkData()) {
			System.out.println("不可删除");
			return false;
		}

		System.out.println("---getInputData successful---");

		if (delete() == false) {
			System.out.println("删除失败");
			return false;
		}
		System.out.println("---delete successful---");
		// 装配处理好的数据，准备给后台进行保存
		this.prepareOutputData();
		System.out.println("---prepareOutputData---");

		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mResult, cOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}

		return true;
	}

	private boolean delete() {
    //工單刪除		
	if(delFlag.equals("1")){
    
    String strSQL1="";
    strSQL1="delete from lgwork where workno='"
    		+mEdorNo+"'";
    String strSQL2="";
    strSQL2="delete from lgworktrace where workno='"
    		+mEdorNo+"'";
    String strSQL3="";
    strSQL3="delete from lgtracenodeop where workno='"
    		+mEdorNo+"'";
    MMap tMMap=new MMap();
    tMMap.put(strSQL1,"DELETE");
    tMMap.put(strSQL2,"DELETE");
    tMMap.put(strSQL3,"DELETE");
	  VData tInputData = new VData();
      tInputData.add(tMMap);
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(tInputData, "")) {
          // @@错误处理
         System.out.println(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "Delete";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
     //     this.mErrors.addOneError(tError);
          return false;
      }
      	return true;
    }
    	
		//保單刪除
    	if(delFlag.equals("2")){
    String strSQL1="";
    strSQL1="delete from lpedoritem where edorno='"
    		+mEdorNo+"'";
    String strSQL2="";
    strSQL2="delete from lpedormain where edoracceptno='"
    		+mEdorNo+"'";
   
    MMap tMMap=new MMap();
    tMMap.put(strSQL1,"DELETE");
    tMMap.put(strSQL2,"DELETE");
    
	  VData tInputData = new VData();
      tInputData.add(tMMap);
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(tInputData, "")) {
          // @@错误处理
         System.out.println(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "Delete";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
     //     this.mErrors.addOneError(tError);
          return false;
      }
      	return true;
    }	
    	return false;
    }

	private boolean checkData()
    {

        try
        {
        	LGWorkSchema tLGWorkSchema
                    = (LGWorkSchema) mInputData
                    .getObjectByObjectName("LGWorkSchema", 0);
        	
            if (tLGWorkSchema != null)
            {
            	mEdorNo = tLGWorkSchema.getWorkNo();
            	
            	System.out.println(delFlag);
            	 if (!checkMaxEdor(mEdorNo,delFlag)){
                     return true;
                 }
            }
        }
             catch (Exception ex)
             {
            ex.printStackTrace();
        }
        return true;
    }

	private boolean checkMaxEdor(String edorNo, String delFlag) {
		System.out.println(edorNo);
		System.out.println(delFlag);
		if (delFlag.equals("1")&&delFlag=="1") {
			System.out.println(edorNo);
			System.out.println(delFlag);
				String sql = " select * from lpedoritem " + " where edorno = '" + mEdorNo + "' with ur"; //// 判斷是否有item表
				LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
				LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
				tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
				if (tLPEdorItemSet.size() > 0) {
					CError tError = new CError();
					tError.moduleName = "PGrpEdorDeleteBL";
					tError.functionName = "checkMaxEdor";
					tError.errorMessage = "存在保全单，不能刪除工单！";
					this.mErrors.addOneError(tError);
					return false;
				}
				return true;
			}
		
		if (delFlag.equals("2")&&delFlag=="2") {
			
				String sql = " select * from lpedorApp" + "  where edoracceptno = '" + mEdorNo + "' with ur"; // 判斷是否有APP表
				System.out.println("sql:" + sql);
				LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
				LPEdorAppSet tLPEdorAppSet = new LPEdorAppSet();
				tLPEdorAppSet = tLPEdorAppDB.executeQuery(sql);
				if (tLPEdorAppSet.size() > 0) {
					CError tError = new CError();
					tError.moduleName = "PGrpEdorDeleteBL";
					tError.functionName = "checkMaxEdor";
					tError.errorMessage = "存在APP单，不能刪除保单！";
					this.mErrors.addOneError(tError);
					return false;
				}
				return true;
		}
		return false;
	}

	/**
	 * 将UI层传输来得数据根据业务还原成具体的类
	 * 
	 * @return boolean
	 */
	private boolean getInputData() {
		// 全局变量实例
	     mGlobalInput = (GlobalInput) mInputData
                 .getObjectByObjectName("GlobalInput", 0);
	//	mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
		tTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);

		delFlag = (String) tTransferData.getValueByName("DelFlag");
		mOperator = mGlobalInput.Operator;
		mManageCom = mGlobalInput.ManageCom;

		return true;
	}

	/**
	 * 准备数据，重新填充数据容器中的内容
	 */
	private void prepareOutputData() {
		// 记录当前操作员
		mResult.clear();
		mResult.add(mMap);
	}

	/**
	 * 操作结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args) {
		VData tVData = new VData();
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ManageCom = "86";
		tGlobalInput.Operator = "endor";

		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		// tLGWorkSchema.setWorkNo("20070118000017");
		TransferData t = new TransferData();
		// t.setNameAndValue("DelFlag", "2");

		tVData.addElement(tGlobalInput);
		tVData.add(tLGWorkSchema);
		tVData.add(t);

		PGrpEdorDeleteBL tPGrpEdorDeleteBL = new PGrpEdorDeleteBL();
		tPGrpEdorDeleteBL.submitData(tVData, "DELETE");
	}

}