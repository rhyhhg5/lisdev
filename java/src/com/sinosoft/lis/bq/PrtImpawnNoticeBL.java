package com.sinosoft.lis.bq;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TextTag;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.ExeSQL;
import java.io.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PrtImpawnNoticeBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCContSchema mLCContSchema = new LCContSchema();

    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    SSRS mSSRS = new SSRS();

    SSRS tSSRS = new SSRS();

    private String HanDigiStr[] = new String[] {"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒",
                          "捌", "玖"};

    private String HanDiviStr[] = new String[] {"", "拾", "佰", "仟", "万", "拾", "佰", "仟",
                          "亿", "拾", "佰", "仟", "万", "拾", "佰", "仟", "亿",
                          "拾", "佰", "仟", "万", "拾", "佰", "仟"};


    private String mOperate = "";


    public PrtImpawnNoticeBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("==PrtImpawnNoticeBL start==");

        this.mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();


        // 准备所有要打印的数据
    if (!getPrintData()) {
        return false;
    }

        //加入到打印列表
        if (!dealPrintMag()) {
            return false;
        }

        System.out.println("==PrtImpawnNoticeBL end==");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                              getObjectByObjectName(
                                      "LOPRTManagerSchema", 0);
        if (mGlobalInput == null || mLOPRTManagerSchema == null) {
            CError tError = new CError();
            tError.moduleName = "PrtImpawnNoticeBL.java";
            tError.functionName = "submitData";
            tError.errorMessage = "获取数据失败！";
            mErrors.addOneError(tError);
            return false;
        }

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());
        if (!tLCContDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "PrtImpawnNoticeBL.java";
            tError.functionName = "dealPrintMag";
            tError.errorMessage = "获得保单信息失败！";
            mErrors.addOneError(tError);
            return false;
        }
        mLCContSchema = tLCContDB.getSchema();

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return this.mErrors;
    }

    private boolean getPrintData()
    {
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("OmnipAnnalsFX.vts", "printer");
        textTag.add("JetFormType", "ZY001");

        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";

        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            CError.buildErr(this,"操作员机构查询出错！");
            return false;
        }

         String printcom ="select codename from ldcode where codetype='pdfprintcom' and code='"
                          +comcode + "' with ur";
         String printcode = new ExeSQL().getOneValue(printcom);
         textTag.add("ManageComLength4", printcode);
         textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));

         if ("batch".equals(mOperate)) {
             textTag.add("previewflag", "0");
         } else {
             textTag.add("previewflag", "1");
         }

         String AppntName = new ExeSQL().getOneValue("select AppntName From LCCont where ContNo ='"+mLCContSchema.getContNo()+"'");
         textTag.add("AppntName",AppntName);

         String Insuredname = new ExeSQL().getOneValue("select Insuredname From LCCont where ContNo ='"+mLCContSchema.getContNo()+"'");
         textTag.add("Insuredname",Insuredname);

         String Cvalidate = new ExeSQL().getOneValue("select Cvalidate From LCCont where ContNo ='"+mLCContSchema.getContNo()+"'");
         textTag.add("Cvalidate",Cvalidate);

         String Years = new ExeSQL().getOneValue("select max(years) From lcpol where contno ='"+mLCContSchema.getContNo()+"'");
         textTag.add("Years",Years);

         String Prem = new ExeSQL().getOneValue("select SUMPREM From LCCont where ContNo ='"+mLCContSchema.getContNo()+"'".toString());
         String endPrem = NumToRMBStr(Double.parseDouble(Prem));
         textTag.add("endPrem",endPrem);

         double CTMoney =Double.parseDouble(mLOPRTManagerSchema.getStandbyFlag1());

         String endMoney = NumToRMBStr(CTMoney);
         if (endMoney==null || endMoney.equals(""))
         {
             CError.buildErr(this,"退费类型错误！");
             return false ;
         }
         textTag.add("endMony",endMoney);

         textTag.add("Operator",mGlobalInput.Operator);//经办人

         textTag.add("ContNo",mLCContSchema.getContNo());

         textTag.add("PrtDate",CommonBL.decodeDate(PubFun.calDate(PubFun.getCurrentDate2(), 0, "D", null)));

         String  ServerName = new ExeSQL().getOneValue("select name From ldcom where comcode ='"+mGlobalInput.ManageCom+"'");

         textTag.add("ServerName",ServerName);
//         String ApproveCode =mLOPRTManagerSchema.getStandbyFlag3();
//        try {
//            System.out.println(new String(ApproveCode.getBytes("GBK"), "GB2312"));
//        } catch (UnsupportedEncodingException ex) {
//            ex.getMessage();
//        }
         textTag.add("ApproveCode",""); //审核人

//         String bank ="";
//         String bankSQL = new ExeSQL().getOneValue("select bankname From LDBank where bankcode ='"+mLOPRTManagerSchema.getStandbyFlag2()+"'");
         textTag.add("Bank","");
         String bak1 = new ExeSQL().getOneValue("select wrapname from ldwrap where riskwrapcode in("
                     +" select distinct riskwrapcode From lcriskdutywrap a,lcpol b where a.riskcode = b.riskcode "
                     +" and b.ContNo ='"+mLCContSchema.getContNo()+"' ) WITH ur");
         if (bak1.equals("") || bak1=="")
         {
             CError.buildErr(this,"获得套餐名称失败！");
             return false ;
         }
         textTag.add("bak1",bak1);

         //添加节点
         if (textTag.size() > 0) {
             xmlexport.addTextTag(textTag);
         }

         xmlexport.outputDocumentToFile("C:\\", "PrtImpawnNoticeFX");
         mResult.clear();
         mResult.addElement(xmlexport);

         return true;
    }

    private boolean dealPrintMag() {
        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        mLOPRTManagerSchema.setOtherNoType("00");
        mLOPRTManagerSchema.setCode("ZY001");
        mLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
//        mLOPRTManagerSchema.setStandbyFlag2(tLJSGetSchema.getGetNoticeNo()); //这里存放 （也为交费收据号）
//        mLOPRTManagerSchema.setStandbyFlag1(tLJSGetSchema.getOtherNo());
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }


    private String PositiveIntegerToHanStr(String NumStr) { // 输入字符串必须正整数，只允许前导空格(必须右对齐)，不宜有前导零
        String RMBStr = "";
        boolean lastzero = false;
        boolean hasvalue = false; // 亿、万进位前有数值标记
        int len, n;
        len = NumStr.length();
        if (len > 15)return "数值过大!";
        for (int i = len - 1; i >= 0; i--) {
            if (NumStr.charAt(len - i - 1) == ' ')continue;
            n = NumStr.charAt(len - i - 1) - '0';
            if (n < 0 || n > 9)return "输入含非数字字符!";

            if (n != 0) {
                if (lastzero) RMBStr += HanDigiStr[0]; // 若干零后若跟非零值，只显示一个零
                // 除了亿万前的零不带到后面
                //if( !( n==1 && (i%4)==1 && (lastzero || i==len-1) ) )    // 如十进位前有零也不发壹音用此行
                if (!(n == 1 && (i % 4) == 1 && i == len - 1)) // 十进位处于第一位不发壹音
                    RMBStr += HanDigiStr[n];
                RMBStr += HanDiviStr[i]; // 非零值后加进位，个位为空
                hasvalue = true; // 置万进位前有值标记

            } else {
                if ((i % 8) == 0 || ((i % 8) == 4 && hasvalue)) // 亿万之间必须有非零值方显示万
                    RMBStr += HanDiviStr[i]; // “亿”或“万”
            }
            if (i % 8 == 0) hasvalue = false; // 万进位前有值标记逢亿复位
            lastzero = (n == 0) && (i % 4 != 0);
        }

        if (RMBStr.length() == 0)return HanDigiStr[0]; // 输入空字符或"0"，返回"零"
        return RMBStr;
    }

    private  String NumToRMBStr(double val) {
        String SignStr = "";
        String TailStr = "";
        long fraction, integer;
        int jiao, fen;

        if (val < 0) {
            val = -val;
            SignStr = "负";
        }
        if (val > 99999999999999.999 || val < -99999999999999.999)return
                "数值位数过大!";
        // 四舍五入到分
        long temp = Math.round(val * 100);
        integer = temp / 100;
        fraction = temp % 100;
        jiao = (int) fraction / 10;
        fen = (int) fraction % 10;
        if (jiao == 0 && fen == 0) {
            TailStr = "整";
        } else {
            TailStr = HanDigiStr[jiao];
            if (jiao != 0)
                TailStr += "角";
            if (integer == 0 && jiao == 0) // 零元后不写零几分
                TailStr = "";
            if (fen != 0)
                TailStr += HanDigiStr[fen] + "分";
        }

        return SignStr + PositiveIntegerToHanStr(String.valueOf(integer)) +
                "元" + TailStr;
    }

}
