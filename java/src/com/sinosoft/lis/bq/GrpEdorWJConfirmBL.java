package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 无名单减人保全确认类
 * 将无名单保全理算类处理的保全临时数据LPCont、LPPol、LPDuty、LPPrem、与对应的C表数据互换
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class GrpEdorWJConfirmBL implements EdorConfirm
{
    private VData mResult = null;   //处理结果集合

    public GrpEdorWJConfirmBL()
    {
    }

    /**
     * getResult
     * 得到无名单减人确认操作结果集合
     * @return VData：处理后的数据集合
     * @todo Implement this com.sinosoft.lis.bq.EdorConfirm method
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * submitData
     * 无名单减人外部操作提交方法，处理无名单保全确认业务逻辑
     * 由于无名单减人后台逻辑与无名单赠人后台逻辑刚好相反，故直接调用无名单增人确认类
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     * @todo Implement this com.sinosoft.lis.bq.EdorConfirm method
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        GrpEdorWZConfirmBL bl = new GrpEdorWZConfirmBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        mResult = bl.getResult();

        return true;
    }
}
