package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 分红险红利领取方式变更保全确认处理类</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Lanjun
 * @version 1.0
 */
public class PEdorDDConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /**  */

    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private ValidateEdorData2 mValidateEdorData = null;

    public PEdorDDConfirmBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public void setOperate(String cOperate)
    {
        this.mOperate = cOperate;
    }

    public String getOperate()
    {
        return this.mOperate;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.setOperate(cOperate);

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //数据操作
        if (!dealData())
        {
            return false;
        }

        //输出数据准备
        if (!prepareOutputData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**数据查询业务处理(queryData())
     *
     */


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mLPEdorItemSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError(new CError("传入数据不完全！"));
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        tLPEdorItemDB.setPolNo(mLPEdorItemSchema.getPolNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
        {
            mErrors.addOneError(new CError("查询批改项目信息失败！"));
            return false;
        }
        mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));
        mValidateEdorData = new ValidateEdorData2(mGlobalInput, mLPEdorItemSchema.getEdorNo(),mLPEdorItemSchema.getEdorType(), mLPEdorItemSchema.getContNo(), "ContNo");
        return true;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {

        boolean flag = true;
        return flag;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData()
    {
        Reflections tRef = new Reflections();

        LPPolSchema tLPPolSchema = new LPPolSchema();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LPPolSchema aLPPolSchema = new LPPolSchema();
        LCPolSchema aLCPolSchema = new LCPolSchema();

        tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPPolSchema.setPolNo(mLPEdorItemSchema.getPolNo());
        
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setPolNo(mLPEdorItemSchema.getPolNo());
        tLPPolDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(mLPEdorItemSchema.getEdorType());

        if(!tLPPolDB.getInfo())
        {
            mErrors.addOneError(new CError("合同信息不完整，请确保保单信息的完整性！"));
            return false;
        }

        aLPPolSchema = tLPPolDB.getSchema();
        tLCPolSchema = new LCPolSchema();
        tLPPolSchema = new LPPolSchema();

        LCPolDB aLCPolDB = new LCPolDB();
        aLCPolDB.setPolNo(aLPPolSchema.getPolNo());
        if (!aLCPolDB.getInfo())
        {
            mErrors.copyAllErrors(aLCPolDB.mErrors);
            mErrors.addOneError(new CError("查询险种信息失败！"));
            return false;
        }

        //交换c表和p表
        aLCPolSchema = aLCPolDB.getSchema();
        tRef.transFields(tLPPolSchema, aLCPolSchema);
        tLPPolSchema.setEdorNo(aLPPolSchema.getEdorNo());
        tLPPolSchema.setEdorType(aLPPolSchema.getEdorType());

        tRef.transFields(tLCPolSchema, aLPPolSchema);
        tLCPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCPolSchema.setModifyTime(PubFun.getCurrentTime());

        //得到当前LPEdorItem保单信息主表的状态，并更新状态为申请确认。
        mLPEdorItemSchema.setEdorState("0");
        mMap.put(mLPEdorItemSchema, "UPDATE");
        mMap.put(tLCPolSchema, "UPDATE");
        mMap.put(tLPPolSchema, "UPDATE");
        
        String[] addTables = {"LCInsureAccTrace"};
        mValidateEdorData.addData(addTables);
        String[] chgTables = {"LCInsureAcc","LCInsureAccClass"};
        mValidateEdorData.changeData(chgTables);
        mMap.add(mValidateEdorData.getMap());
        
        return true;
    }

    private boolean prepareOutputData()
    {
        mResult.clear();
        mResult.add(mMap);
        return true;
    }
}
