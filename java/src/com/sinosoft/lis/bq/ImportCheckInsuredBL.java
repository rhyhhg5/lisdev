package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LPDiskImportDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.diskimport.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: 团单保全磁盘导入后校验客户信息</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author lzy
 * @version 1.0
 */

public class ImportCheckInsuredBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;
    
    private GlobalInput mGlobalInput=null;
    
    private LPDiskImportSet tLPDiskImportSet=null;

    private static String mCurrentDate = PubFun.getCurrentDate();

    private static String mCurrentTime = PubFun.getCurrentTime();


    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到输入数据
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
        	mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            TransferData transferData=(TransferData)data.getObjectByObjectName("TransferData", 0);
            mEdorNo = (String)transferData.getValueByName("EdorNo");
            mEdorType = (String)transferData.getValueByName("EdorType");
            mGrpContNo = (String)transferData.getValueByName("GrpContNo");
            if(null==mEdorNo || null==mEdorType || null==mGrpContNo){
            	mErrors.addOneError("传入数据错误！");
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
    	//查询出导入有效的数据
    	tLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, mEdorType,
                mGrpContNo, BQ.IMPORTSTATE_SUCC);
    	//若没有有效的数据直接返回，不报错
    	if(tLPDiskImportSet.size()<=0){
    		System.out.println("没有查询到有效的导入数据***00***");
            return true;
    	}
    	
    	for(int i=1;i<=tLPDiskImportSet.size();i++){
    		LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(i);
    		if(null==tLPDiskImportSchema.getInsuredName() || "".equals(tLPDiskImportSchema.getInsuredNo())
    		|| null==tLPDiskImportSchema.getIDNo() || "".equals(tLPDiskImportSchema.getIDNo()) 
    		|| null==tLPDiskImportSchema.getIDType() || "".equals(tLPDiskImportSchema.getIDType())
    		|| null==tLPDiskImportSchema.getBirthday() || "".equals(tLPDiskImportSchema.getBirthday())
    		|| null==tLPDiskImportSchema.getSex() || "".equals(tLPDiskImportSchema.getSex())){
    			setImportState(tLPDiskImportSchema.getSerialNo(),"",BQ.IMPORTSTATE_FAIL,"被保人信息不完整，导入无效！");
    			continue;
    		}
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            tLCInsuredSchema.setGrpContNo(mGrpContNo);
            tLCInsuredSchema.setName(tLPDiskImportSchema.getInsuredName());
            tLCInsuredSchema.setSex(tLPDiskImportSchema.getSex());
            tLCInsuredSchema.setBirthday(tLPDiskImportSchema.getBirthday());
            tLCInsuredSchema.setIDType(tLPDiskImportSchema.getIDType());
            tLCInsuredSchema.setIDNo(tLPDiskImportSchema.getIDNo());
            OldCustomerCheck customerCheck = new OldCustomerCheck(tLCInsuredSchema);
            if (customerCheck.checkInsured() != OldCustomerCheck.OLD)
            {
            	setImportState(tLPDiskImportSchema.getSerialNo(),"",BQ.IMPORTSTATE_FAIL,"保单下不存在该客户，导入无效！");
            }
            else
            {
            	if(null != tLPDiskImportSchema.getPhone() && !"".equals(tLPDiskImportSchema.getPhone())){
            		setImportState(tLPDiskImportSchema.getSerialNo(),customerCheck.getCustomerNo(),BQ.IMPORTSTATE_SUCC,"");
            	}else{
            		setImportState(tLPDiskImportSchema.getSerialNo(),"",BQ.IMPORTSTATE_FAIL,"联系电话为空，导入无效！");
            	}
            }
    	}
    	
        return true;
    }
    
    private boolean setImportState(String serialNo,String customerno,String state,String errorInfo){
    	if(BQ.IMPORTSTATE_SUCC.equals(state)){
    		StringBuffer sql = new StringBuffer("Update LPDiskImport ");
            sql.append("Set State = '")
                    .append(BQ.IMPORTSTATE_SUCC).append("', ")
                    .append("InsuredNo = '")
                    .append(StrTool.cTrim(customerno))
                    .append("', ")
                    .append("Operator = '")
                    .append(mGlobalInput.Operator).append("', ")
                    .append("ModifyDate = '")
                    .append(mCurrentDate).append("', ")
                    .append("ModifyTime = '")
                    .append(mCurrentTime).append("' ")
                    .append("where EdorNo = '")
                    .append(mEdorNo).append("' ")
                    .append("and EdorType = '")
                    .append(mEdorType).append("' ")
                    .append("and GrpContNo = '")
                    .append(mGrpContNo).append("' ")
                    .append("and SerialNo = '")
                    .append(serialNo)
                    .append("' ");
            mMap.put(sql.toString(), "UPDATE");
    	}else if(BQ.IMPORTSTATE_FAIL.equals(state)){
    		StringBuffer sql = new StringBuffer("Update LPDiskImport ");
            sql.append("Set State = '")
                    .append(BQ.IMPORTSTATE_FAIL).append("', ")
                    .append(" ErrorReason = '")
                    .append(errorInfo).append("', ")
                    .append(" Operator = '")
                    .append(mGlobalInput.Operator).append("', ")
                    .append(" ModifyDate = '")
                    .append(mCurrentDate ).append("', ")
                    .append(" ModifyTime = '")
                    .append(mCurrentTime).append("' ")
                    .append("where EdorNo = '")
                    .append(mEdorNo).append("' ")
                    .append("and EdorType = '")
                    .append(mEdorType).append("' ")
                    .append("and GrpContNo = '")
                    .append(mGrpContNo).append("' ")
                    .append("and SerialNo = '")
                    .append(serialNo)
                    .append("' ");
            mMap.put(sql.toString(), "UPDATE");
    	}
    	
    	return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    
}
