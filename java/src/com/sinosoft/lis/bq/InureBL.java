package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;

import com.sinosoft.lis.pubfun.*;

public class InureBL {
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mContNo;
    private String mContType;
    private PEdorInureBL tPEdorInureBL = null;
    private PGrpEdorInureBL tPGrpEdorInureBL = null;
    
    public InureBL(GlobalInput gi,String contNo,String conttype){
    	this.mGlobalInput = gi;
    	this.mContNo = contNo;
    	this.mContType = conttype;
    }
    
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(){
  
    	if (!getInputData()){
            return false;
        }

    	if(!checkData()){
    		return false;
    	}
    	
    	//进行业务处理
    	if(!dealData()){
    		
            return false;
    	}
    	
    	//装配处理好的数据，准备给后台进行保存    	
    	if (!prepareOutputData()){
            return false;
        }
        
    	return true;
    }
    
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(){
    	
        return true;
    }
    
    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData(){
    	
        return true;
    }
    
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData(){
        if("1".equals(mContType)){
        	tPEdorInureBL = new PEdorInureBL(mGlobalInput,mContNo);
        	if(!tPEdorInureBL.submitData()){
                this.mErrors.copyAllErrors(tPEdorInureBL.mErrors);
                return false;
            }
        }else if("2".equals(mContType)){
        	tPGrpEdorInureBL = new PGrpEdorInureBL(mGlobalInput,mContNo);
        	if(!tPGrpEdorInureBL.submitData()){
                this.mErrors.copyAllErrors(tPGrpEdorInureBL.mErrors);
                return false;
            }
        }
        
    	return true;
    }
    
    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private boolean prepareOutputData(){
    	
    	return true;
    }
    
    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult(){
    	
  	    return this.mResult;
	}
}