package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全客户资料变更项目</p>
 * <p>Description: 保全确认</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorJCConfirmBL implements EdorConfirm
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mCustomerNo = null;
    
    private String mName = null;

    private String mSex = null;

    private String mBirthday = null;

    private String mCValiDate = null;

    private String mIdNo = null;
    
    private String mMobile = null;
    
    private String mPhone = null;
    
    private String mPostalAddress = null;
    
    private String mZipCode = null;
    
    private String mOccupationCode = null;
    
    private String mOccupationType = null;
    
    private String mIDType = null;
    
    private String mEMail = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    private LICardActiveInfoListSchema mLICardActiveInfoListSchema=null;

    /**
     * 提交数据
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        getInputData(data);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private void getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                "GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) data.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        mEdorNo = mLPEdorItemSchema.getEdorNo();
        mContNo = mLPEdorItemSchema.getContNo();
        mEdorType = mLPEdorItemSchema.getEdorType();
        mCustomerNo = mLPEdorItemSchema.getInsuredNo();
        
        mName = getEdorValueByDetailType("Name");
        mSex = getEdorValueByDetailType("Sex");
        mBirthday = getEdorValueByDetailType("Birthday");
//        mCValiDate = getEdorValueByDetailType("CValiDate");
        mIdNo = getEdorValueByDetailType("IdNo");
        mMobile = getEdorValueByDetailType("Mobile");
        mPhone = getEdorValueByDetailType("Phone");
        mPostalAddress = getEdorValueByDetailType("PostalAddress");
        mZipCode = getEdorValueByDetailType("ZipCode");
        mEMail = getEdorValueByDetailType("EMail");
        mOccupationCode = getEdorValueByDetailType("OccupationCode");
        mOccupationType = getEdorValueByDetailType("OccupationType");
        mIDType = getEdorValueByDetailType("IDType");
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        setLDPerson();
        setLICertifyInsured();
        setLICardActiveIfoList();
//        setWFAppntList();
        setWFInsuList();
        if (!validateEdorData())
        {
            return false;
        }
//        if(!validateOtherCont())
//        {
//        	return false;
//        }
        return true;
    }

    /**
     * 使保全数据生效
     */
    private boolean validateEdorData()
    {
        String[] tables = { "LCInsured", "LCCont", "LCPol"};
        ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,  mEdorNo, mEdorType,mContNo,"ContNo");
        if (!validate.changeData(tables))
        {
            mErrors.copyAllErrors(validate.mErrors);
            return false;
        }
        MMap map = validate.getMap();
        mMap.add(map);
        return true;
    }

    /**
     * 设置客户信息
     */
    private void setLDPerson()
    {
        LPPersonDB tLPPersonDB = new LPPersonDB();
        tLPPersonDB.setEdorNo(mEdorNo);
        tLPPersonDB.setEdorType(mEdorType);
        tLPPersonDB.setCustomerNo(mCustomerNo);
        LPPersonSet tLPPersonSet = tLPPersonDB.query();
        for (int i = 1; i <= tLPPersonSet.size(); i++)
        {
            LPPersonSchema tLPPersonSchema = tLPPersonSet.get(i);
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(tLPPersonSchema.getCustomerNo());
            tLDPersonDB.getInfo();
            LDPersonSchema tLDPersonSchema = new LDPersonSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLDPersonSchema, tLPPersonSchema);
            ref.transFields(tLPPersonSchema, tLDPersonDB.getSchema());
            mMap.put(tLDPersonSchema, "DELETE&INSERT");
            mMap.put(tLPPersonSchema, "DELETE&INSERT");
        }
    }
    //添加此方法对于多被保人的支持
    private void  setLICertifyInsured()
    {
    	LPCertifyInsuredDB tLPCertifyInsuredDB = new LPCertifyInsuredDB();
    	tLPCertifyInsuredDB.setEdorNo(mEdorNo);
    	tLPCertifyInsuredDB.setEdorType(mEdorType);
    	tLPCertifyInsuredDB.setCardNo(mContNo);
    	tLPCertifyInsuredDB.setCustomerNo(mCustomerNo);
    	LPCertifyInsuredSet tLPCertifyInsuredSet = tLPCertifyInsuredDB.query();
//        for (int i = 1; i <= tLPCertifyInsuredSet.size(); i++)
//        {
        	LPCertifyInsuredSchema tLPCertifyInsuredSchema = tLPCertifyInsuredSet.get(1);
        	LICertifyInsuredDB tLICertifyInsuredDB = new LICertifyInsuredDB();
        	tLICertifyInsuredDB.setCardNo(tLPCertifyInsuredSchema.getCardNo());
        	tLICertifyInsuredDB.setSequenceNo(tLPCertifyInsuredSchema.getSequenceNo());
        	tLICertifyInsuredDB.getInfo();
        	LICertifyInsuredSchema tLICertifyInsuredSchema = new LICertifyInsuredSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLICertifyInsuredSchema, tLPCertifyInsuredSchema);
            ref.transFields(tLPCertifyInsuredSchema, tLICertifyInsuredDB.getSchema());
            mMap.put(tLICertifyInsuredSchema, "DELETE&INSERT");
            mMap.put(tLPCertifyInsuredSchema, "DELETE&INSERT");
//        }
    	
    }
    
    private void setWFInsuList()
    {
    	String tname=new ExeSQL().getOneValue("select name from licertifyinsured where cardno='"+mContNo+"' and customerno='"+mCustomerNo+"'");
    	String updateWFInsuListSql="update WFInsuList set name ='"+mName+"', " 
							        +"sex='"+mSex+"', "
							        +"Birthday='"+mBirthday+"', "
							        +"IdNo='"+mIdNo+"', "
							        +"PostalAddress='"+mPostalAddress+"', "
							        +"Phont='"+mPhone+"',"
							        +"ZipCode='"+mZipCode+"', "
							        +"Mobile='"+mMobile+"', "
							        +"EMail='"+mEMail+"', "
							        +"OccupationCode='"+mOccupationCode+"', "
	 	                            +"OccupationType='"+mOccupationType+"', "
	 	                            +"IDType='"+mIDType+"'  "
							        +"where cardno='"+mContNo+"' and name='"+tname+"'";
    	mMap.put(updateWFInsuListSql, "UPDATE");
    	
    }
//    private void setWFAppntList()
//    {
//    	WFAppntListDB tWFAppntListDB=new WFAppntListDB();
//    	tWFAppntListDB.setCardNo(mContNo);
//    	tWFAppntListDB.query();
//    	WFAppntListSchema tWFAppntListSchema=tWFAppntListDB.getSchema();
//    	
//    	WFInsuListDB tWFInsuListDB=new WFInsuListDB();
//    	tWFInsuListDB.setCardNo(mContNo);
//    	tWFInsuListDB.query();
//    	WFInsuListSchema tWFInsuListSchema=tWFInsuListDB.getSchema();
//    	if(tWFAppntListSchema.getName().equals(tWFInsuListSchema.getName()))
//    	{
//	    	String updateWFAppntListSql="update wfappntlist set name ='"+mName+"', " 
//	    	                           +"sex='"+mSex+"', "
//	    	                           +"Birthday='"+mBirthday+"', "
//	    	                           +"IdNo='"+mIdNo+"', "
//	    	                           +"PostalAddress='"+mPostalAddress+"', "
//	    	                           +"Phont='"+mPhone+"',"
//	    	                           +"ZipCode='"+mZipCode+"', "
//	    	                           +"Mobile='"+mMobile+"', "
//	    	                           +"EMail='"+mEMail+"', "
//	    	                           +"OccupationCode='"+mOccupationCode+"', "
//	 	                               +"OccupationType='"+mOccupationType+"', "
//	 	                               +"IDType='"+mIDType+"'  "
//	    	                           +"where cardno='"+mContNo+"'";
//	    	mMap.put(updateWFAppntListSql, "UPDATE");
//    	}
//    }
//    
    private void setLICardActiveIfoList()
    {   	
// 添加对于多被保人的支持
    	String tname=new ExeSQL().getOneValue("select name from licertifyinsured where cardno='"+mContNo+"' and customerno='"+mCustomerNo+"'");
    	String tLICardActiveIfoListSQL="update LICARDACTIVEInFOLIST set modifydate='"+mCurrentDate+"', "
    	                              +"modifytime='"+mCurrentTime+"', "
    	                              +"Name='"+mName+"', "
    	                              +"Sex='"+mSex+"', "
    	                              +"Birthday='"+mBirthday+"', "
    	                             // +"CValiDate='"+mCValiDate+"',"
    	                              +"IdNo='"+mIdNo+"', "
    	                              +"Mobile='"+mMobile+"', "
    	                              +"Phone='"+mPhone+"', "
    	                              +"PostalAddress='"+mPostalAddress+"', "
    	                              +"ZipCode='"+mZipCode+"', "
    	                              +"EMail='"+mEMail+"', "
    	                              +"OccupationCode='"+mOccupationCode+"', "
    	                              +"OccupationType='"+mOccupationType+"', "
    	                              +"IDType='"+mIDType+"'  "
    	                              +"where cardno='"+mContNo+"' and name='"+tname+"'";
    	mMap.put(tLICardActiveIfoListSQL, "UPDATE");
    }
    
    private String getEdorValueByDetailType(String DetailType)
    {
    	LPEdorEspecialDataDB tLPEdorEspecialDataDB=new LPEdorEspecialDataDB();
    	tLPEdorEspecialDataDB.setDetailType(DetailType);
    	tLPEdorEspecialDataDB.setEdorNo(mEdorNo);
    	tLPEdorEspecialDataDB.setEdorType("JM");
    	LPEdorEspecialDataSet tLPEdorEspecialDataSet=tLPEdorEspecialDataDB.query();
    	if(tLPEdorEspecialDataSet!=null&&tLPEdorEspecialDataSet.size()>0)
    	{
    		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema=tLPEdorEspecialDataSet.get(1);
    		return tLPEdorEspecialDataSchema.getEdorValue();
    	}
    	else
    	{
    		return null;
    	}
    	
    }
    
    /**
     * 同时处理该客户作为其它团单被保人的资料变更
     * 20090320 zhanggm cbs00024868 
     */
    private boolean validateOtherCont()
    {
    	StringBuffer sql = new StringBuffer();
    	sql.append("select contno from lcinsured where insuredno = '" +mCustomerNo + "' ");
    	sql.append("and grpcontno <> '" + BQ.GRPFILLDATA + "' ");
    	sql.append("and contno <> '" + mContNo + "'");
    	System.out.println("查询关联保单："+sql);
    	SSRS tSSRS = new SSRS();
    	tSSRS = new ExeSQL().execSQL(sql.toString());
    	
    	if(tSSRS.MaxRow==0)
    	{
    		System.out.println("被保人"+mCustomerNo+"没有其它关联团单需要变更");
    		return true;
    	}
    	else
    	{
    		for(int i=1;i<=tSSRS.MaxRow;i++)
    		{
    			String tContNo = tSSRS.GetText(i, 1);
    			System.out.println("PEdorJCConfirmBL.java-L234->保单："+tContNo);
    	        String[] tables = {"LCInsured","LCCont", "LCPol"};
    	        ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput,
    	                mEdorNo, mEdorType,tContNo,"ContNo");
    	        if (!validate.changeData(tables))
    	        {
    	            mErrors.copyAllErrors(validate.mErrors);
    	            return false;
    	        }
    	        MMap map = validate.getMap();
    	        mMap.add(map);
    	        mContNo=tContNo;
    	        setLICertifyInsured();
    	        setLICardActiveIfoList();
//    	        setWFAppntList();
    	        setWFInsuList();
    		}
    	}
        return true;
    }
}
