package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;

/**
 * <p>Title: lis</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class GEdorWZDetailUI
{
    /**错误的容器，保存错误信息*/
    public CErrors mErrors = null;

    public GEdorWZDetailUI()
    {
    }

    /**
     * 操作提交的方法，对外的接口
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        GEdorWZDetailBL bl = new GEdorWZDetailBL();
        if(!bl.submitData(data, operate))
        {
            mErrors = bl.mErrors;
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        GEdorWZDetailUI gedorwzdetailui = new GEdorWZDetailUI();
    }
}
