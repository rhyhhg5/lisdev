package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 团单保全重复理算</p>
 * <p>Description: 团单保全重复理算</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GEdorReCalBL
{
	/**时间变量*/
	private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    private GlobalInput mGlobalInput = null;
    
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    /** 保全号 */
    private String mEdorNo = null;

    /**
     * 构造函数
     * @param edorNo String
     */
    public GEdorReCalBL(String edorNo)
    {
        this.mEdorNo = edorNo;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
    	if(this.mEdorNo==null|| "".equals(this.mEdorNo)||"null".equals(this.mEdorNo))
    	{
    		return false;
    	}
    	if (!dealUrgeLog("1", "INSERT"))
		   {
		         return false;
		   }
        if (!dealData())
        {
        	dealUrgeLog("1", "DELETE");
            return false;
        }
        if (!submit())
        {
        	dealUrgeLog("1", "DELETE");
            return false;
        }
        dealUrgeLog("1", "DELETE");
        return true;
    }

    /**
     * 得到提交数据
     * @return MMap
     */
    public MMap getSubmitData()
    {
    	if(this.mEdorNo==null|| "".equals(this.mEdorNo)||"null".equals(this.mEdorNo))
    	{
    		return null;
    	}
        if (!dealData())
        {
            return null;
        }
        return mMap;
    }


    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        setPrem();
        setEdorState(BQ.EDORSTATE_INPUT);
        deleteFinaceData();
        deleteEdorPrint();
        return dealEachItem();
    }


    /**
     * 重设P表的保费
     */
    private void setPrem()
    {
        setContPrem();
        setPolPrem();
        setDutyPrem();
        setPremPrem();
    }


    /**
     * 设置Cont表的保费
     */
    private void setContPrem()
    {
    	/*
        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setEdorNo(mEdorNo);
        LPContSet tLPContSet = tLPContDB.query();
        for (int i = 1; i <= tLPContSet.size(); i++)
        {
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(tLPContSet.get(i).getContNo());
            if (tLCContDB.getInfo())
            {
                setLPContPrem(tLCContDB.getSchema());
            }
        }
        */
    	//20081031 modify by zhanggm 原程序占用内存太多，导致宕机
        String sql = "select ContNo from LPCont where EdorNo = '" + mEdorNo + "' ";
        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(sql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(tSSRS.GetText(i, 1));
            if (tLCContDB.getInfo())
            {
                setLPContPrem(tLCContDB.getSchema());
            }
        }
    }

    /**
     * 设置LPCont表的保费
     * @param aLCContSchema LCContSchema
     */
    private void setLPContPrem(LCContSchema aLCContSchema)
    {
        String sql = "update LPCont " +
                "set Prem = " + aLCContSchema.getPrem() + ", " +
                "    SumPrem = " + aLCContSchema.getSumPrem() + ", " +
                "    Peoples = " + aLCContSchema.getPeoples() + " " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and ContNo = '" + aLCContSchema.getContNo() + "'";
        mMap.put(sql, "UPDATE");
    }

    /**
        * 设置Pol表的保费
        */
       private void setPolPrem()
       {
    	   /*
           LPPolDB tLPPolDB = new LPPolDB();
           tLPPolDB.setEdorNo(mEdorNo);
           LPPolSet tLPPolSet = tLPPolDB.query();
           for (int i = 1; i <= tLPPolSet.size(); i++)
           {
               LCPolDB tLCPolDB = new LCPolDB();
               tLCPolDB.setPolNo(tLPPolSet.get(i).getPolNo());
               if (tLCPolDB.getInfo())
               {
                   setLPPolPrem(tLCPolDB.getSchema());
               }
           }
           */
//         20081031 modify by zhanggm 原程序占用内存太多，导致宕机
           String sql = "select PolNo from LPPol where EdorNo = '" + mEdorNo + "' ";
           SSRS tSSRS = new SSRS();
           tSSRS = new ExeSQL().execSQL(sql);
           for (int i = 1; i <= tSSRS.getMaxRow(); i++)
           {
        	   LCPolDB tLCPolDB = new LCPolDB();
               tLCPolDB.setPolNo(tSSRS.GetText(i, 1));
               if (tLCPolDB.getInfo())
               {
            	   setLPPolPrem(tLCPolDB.getSchema());
               }
           }
       }

       /**
        * 设置LPol表的保费
        */
       private void setLPPolPrem(LCPolSchema aLCPolSchema)
       {
           String sql = "update LPPol " +
                   "set StandPrem = " + aLCPolSchema.getStandPrem() + ", " +
                   "    Prem = " + aLCPolSchema.getPrem() + ", " +
                   "    SumPrem = " + aLCPolSchema.getSumPrem() + ", " +
                   "    insuredPeoples = "
                   + aLCPolSchema.getInsuredPeoples() + " " +
                   "where EdorNo = '" + mEdorNo + "' " +
                   "and PolNo = '" + aLCPolSchema.getPolNo() + "'";
           mMap.put(sql, "UPDATE");
       }

       /**
        * 设置Duty表的保费
        */
       private void setDutyPrem()
       {
           String sql = "  update LPDuty a "
                        + "set (prem, sumPrem) = "
                        + "      (select prem, sumPrem "
                        + "      from LCDuty "
                        + "      where polNo = a.polNo "
                        + "         and dutyCode = a.dutyCode) "
                        + "where edorNo = '" + mEdorNo + "' ";
           mMap.put(sql, "UPDATE");
       }

       /**
        * 设置LPPrem的保费
        */
       private void setPremPrem()
       {
           String sql = "  update LPPrem a "
                        + "set (prem, sumPrem) = "
                        + "      (select prem, sumPrem "
                        + "      from LCPrem where polNo = a.polNo "
                        + "         and dutyCode = a.dutyCode "
                        + "         and payPlanCode = a.payPlanCode) "
                        + "where edorNo = '" + mEdorNo + "' ";
           mMap.put(sql, "UPDATE");
       }

    /**
     * 设置保全状态，并使保费清零
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
        String sql;
        sql = "update LPEdorApp set EdorState = '" + edorState + "', " +
                "ChgPrem = 0, ChgAmnt = 0, GetMoney = 0, GetInterest = 0 " +
                "where EdorAcceptNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "update LPGrpEdorMain set EdorState = '" + edorState + "', " +
                "ChgPrem = 0, ChgAmnt = 0, GetMoney = 0, GetInterest = 0 " +
                "where EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "update LPGrpEdorItem set EdorState = '" + edorState + "', " +
                "ChgPrem = 0, ChgAmnt = 0, GetMoney = 0, GetInterest = 0 " +
                "where EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        //团单下的个单
        sql = "update LPEdorMain set EdorState = '" + edorState + "', " +
                "ChgPrem = 0, ChgAmnt = 0, GetMoney = 0, GetInterest = 0 " +
                "where EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "update LPEdorItem set EdorState = '" + edorState + "', " +
                "ChgPrem = 0, ChgAmnt = 0, GetMoney = 0, GetInterest = 0 " +
                "where EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 删除财务数据
     */
    private void deleteFinaceData()
    {
        String[] tables = {"LJSGetEndorse", "LJSPay"};
        for (int i = 0; i < tables.length; i++)
        {
            StringBuffer sql = new StringBuffer("delete ");
            sql.append("from ").append(tables[i])
                    .append(" where OtherNo = '")
                    .append(mEdorNo)
//                    .append("' ")
//                    .append("and OtherNoType = '")
//                    .append(BQ.NOTICETYPE_G)
                    .append("'");
            mMap.put(sql.toString(), "DELETE");
        }
//        String sql = "delete from LJTempFeeClass " +
//                "where TempFeeNo in (select TempFeeNo from LJTempFee " +
//                "      where OtherNo = '" + mEdorNo + "' " +
//                "      and OtherNoType = '" + BQ.NOTICETYPE_G + "') ";
//        mMap.put(sql, "DELETE");
        // 数据校验处理 2010-9-21
        String sql = "delete from LJaget " 
        	       +" where OtherNo = '" + mEdorNo + "' " 
        	       +" and paymode is null ";
        mMap.put(sql, "DELETE");
    }

    /**
     * 删除保全批单
     */
    private void deleteEdorPrint()
    {
        String sql = "delete from LPEdorPrint " +
                "where EdorNo = '" + mEdorNo + "'";
        mMap.put(sql, "DELETE");
    }

    /**
     * 得到保全项目信息
     * @return LPGrpEdorItemSet
     */
    private LPGrpEdorItemSet getEdorItemSet()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mEdorNo);
        return tLPGrpEdorItemDB.query();
    }

    /**
     * 对每个项目进行特殊处理
     */
    private boolean dealEachItem()
    {
        LPGrpEdorItemSet tLPGrpEdorItemSet = getEdorItemSet();
        for (int i = 1; i <= tLPGrpEdorItemSet.size(); i ++)
        {
            LPGrpEdorItemSchema tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(i);
            String edorType = tLPGrpEdorItemSchema.getEdorType();
            if (edorType.equals(BQ.EDORTYPE_NI))
            {
                dealNIData(tLPGrpEdorItemSchema);
            }
            else if (edorType.equals(BQ.EDORTYPE_TZ))
            {
                dealTZData(tLPGrpEdorItemSchema);
            }
            else if (edorType.equals(BQ.EDORTYPE_CM))
            {
                dealCMData(tLPGrpEdorItemSchema);
            }
            else if (edorType.equals(BQ.EDORTYPE_GA))
            {
                dealGAData(tLPGrpEdorItemSchema);
            }
            else if (edorType.equals(BQ.EDORTYPE_TQ) || edorType.equals(BQ.EDORTYPE_SG) || edorType.equals(BQ.EDORTYPE_UM)) //两者处理一样
            {
                dealTQData(tLPGrpEdorItemSchema);
            }
            else if (edorType.equals(BQ.EDORTYPE_TY))
            {
                dealTYData(tLPGrpEdorItemSchema);
            }
            else if (edorType.equals(BQ.EDORTYPE_TL))
            {
                dealTLData(tLPGrpEdorItemSchema);
            }
            if(edorType.equals(BQ.EDORTYPE_WZ))
            {
                dealWZData(tLPGrpEdorItemSchema);
            }
            if (edorType.equals(BQ.EDORTYPE_WS))
            {
               if(!dealWSData(tLPGrpEdorItemSchema))
               return false;
               ;
            }
            if (edorType.equals("GX"))
            {
            	if(!dealGXData(tLPGrpEdorItemSchema))
            	{
            		return false;
            	}
            		
            }
            if (edorType.equals("HL"))
            {
            	if(!dealHLData(tLPGrpEdorItemSchema))
            	{
            		return false;
            	}
            		
            }
            if(edorType.equals("CT")){
            	//判断是否为团险万能
            	if(jugdeUliByGrpContNo(tLPGrpEdorItemSchema) || isTDBCPol(tLPGrpEdorItemSchema) ){
                	if(!dealHLData(tLPGrpEdorItemSchema))
                	{
                		return false;
                	}
            	}
            }
            if(edorType.equals("ZT")){
            	//判断是否为团体补充医疗保险
            	if(isTDBCPol(tLPGrpEdorItemSchema) ){
                	if(!dealZTData(tLPGrpEdorItemSchema))
                	{
                		return false;
                	}
            	}
            }
        }
        return true;
    }

    //增加一个方法用来判断是否是万能险还是之前的险种，根据此方法区分
    public boolean jugdeUliByGrpContNo(LPGrpEdorItemSchema tLPGrpEdorItemSchema)
    {
        StringBuffer tSBql2 = new StringBuffer(256);
        tSBql2.append("select 1 from lcgrppol where GrpContNo = '");
        tSBql2.append(tLPGrpEdorItemSchema.getGrpContNo());
        tSBql2.append("' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')");
        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
        if (tSSRS5.getMaxRow() < 1)
        {
        	System.out.println("此团单不是团体万能");
            return false;
        }
        System.out.println("此团单是团体万能");
        return true;
    }
    
    //判断是否为补充团体医疗保单
    public boolean isTDBCPol(LPGrpEdorItemSchema tLPGrpEdorItemSchema)
    {
        StringBuffer tSBql2 = new StringBuffer(256);
        tSBql2.append("select 1 from lcgrppol where GrpContNo = '");
        tSBql2.append(tLPGrpEdorItemSchema.getGrpContNo());
        tSBql2.append("' and riskcode in (select code from ldcode where codetype='tdbc')");
        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
        if (tSSRS5.getMaxRow() < 1)
        {
        	System.out.println("非补充团体医疗");
            return false;
        }
        System.out.println("此团单是补充团体医疗");
        return true;
    }
    
    
    
    /**
     * 处理团险万能增加被保人项目的数据
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     */
    private void dealTZData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        //删除被保人清单的xml数据
        String sql = "delete from LPEdorPrint2 " +
                "where EdorNo = '" + mEdorNo + "'";
        mMap.put(sql, "DELETE");

        //更新LCInsuredList的状态
        sql = "update LCInsuredList " +
                "set State = '0' " + //, Publicacc = (CASE WHEN Edorprem is null then 0 else decimal(EdorPrem) end ) " +
                "where EdorNo = '" + mEdorNo + "'";
        mMap.put(sql, "UPDATE");

        //删除理算后插入C表的数据，要考虑连带被保人，删除LCCont要放在前面
//        sql = "delete from LCCont a " +
//                "where not exists " +
//                " ( select * from LCInsured " +
//                " where ContNo = a.ContNo " +
//                " and RelationToMainInsured <> '00') " +
//                "and AppFlag = '" + BQ.APPFLAG_EDOR + "' " +
//                "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() +
//                "'";
//        mMap.put(sql, "DELETE");

//        sql = "delete from LCCont a " +
//              "where exists (select * from LCInsuredList " +
//              " where ContNo = a.ContNo and Relation = '00'" +
//              " and ContPlanCode <> 'FM' and EdorNo = '" + mEdorNo +
//              "') and AppFlag = '" + BQ.APPFLAG_EDOR + "' " +
//              "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() +
//              "'";
        //20081028 modify zhanggm 删除LCCont表中所有增人数据。如果理算时产生重复数据，只删除LCInsuredList表中人的话,
        //冗余数据会留在LCCont表。添加保全项目时已经有校验，同一保单的增人项目没结案时不允许新添加增人项目。
        sql = "delete from LCCont a " 
        	+ "where AppFlag = '" + BQ.APPFLAG_EDOR + "' " 
        	+ "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' "
        	+ "and StateFlag is null ";
        mMap.put(sql, "DELETE");

//        sql = "select * from LCCont a " +
//                "where exists " +
//                " ( select * from LCInsuredList " +
//                " where ContNo = a.ContNo " +
//                " and Relation = '00' and ContPlanCode = 'FM' and EdorNo = '" + mEdorNo +
//                "') and AppFlag = '" + BQ.APPFLAG_EDOR + "' " +
//                "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "'";
        sql = "select * from LCCont a " 
        	+ "where AppFlag = '" + BQ.APPFLAG_EDOR + "' " 
        	+ "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' "
        	+ "and StateFlag is null ";
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = tLCContDB.executeQuery(sql);
        for (int i = 1; i <= tLCContSet.size(); i++)
        {
            LCContSchema tLCContSchema = tLCContSet.get(i);
            String premSql = "select a.prem from LCPol a, LCInsured b " +
                    "where a.ContNo = b.ContNo  " +
                    "and a.InsuredNo = b.InsuredNo " +
                    "and a.AppFlag = '" + BQ.APPFLAG_SIGN + "' " +
                    "and b.RelationToMainInsured = '00' " +
                    "and a.ContNo = '" + tLCContSchema.getContNo() + "' ";
            String prem = (new ExeSQL()).getOneValue(premSql);
            tLCContSchema.setPrem(prem);
            tLCContSchema.setAppFlag(BQ.APPFLAG_SIGN);
            mMap.put(tLCContSchema, "DELETE&INSERT");
        }

        String[] tables = {"LCPol", "LCDuty", "LCPrem", "LCGet"};
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        LCPolSet tLCPolSet = getLCPolSetTZ(grpContNo);
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            String polNo = tLCPolSet.get(i).getPolNo();
            for (int j = 0; j < tables.length; j++)
            {
                StringBuffer delSql = new StringBuffer("delete ");
                delSql.append("from ").append(tables[j])
                        .append(" where PolNo = '").append(polNo).append("'");
                mMap.put(delSql.toString(), "DELETE");
            }
            String contNo = tLCPolSet.get(i).getContNo();
            String insuredNo = tLCPolSet.get(i).getInsuredNo();
            //查询系统中是否存在该客户的其他保单，如果没有删除新增的客户。
            LCContDB mLCContDB =new LCContDB();
            mLCContDB.setInsuredNo(insuredNo);
            LCContSet mLCContSet = mLCContDB.query();
            LBContDB mLBContDB =new LBContDB();
            mLBContDB.setInsuredNo(insuredNo);
            LBContSet  mLBContSet = mLBContDB.query();
            if(mLCContSet.size()==0&&mLBContSet.size()==0){
                System.out.println("不存在该客户的其他保单");
                StringBuffer delPerson = new StringBuffer("delete ");
                delPerson.append("from ").append("LDPerson ")
                        .append(" where CustomerNo = '").append(insuredNo).
                        append("'");
                mMap.put(delPerson.toString(), "DELETE");
            }
            StringBuffer delInsured = new StringBuffer("delete ");
            delInsured.append("from ").append("LCInsured ")
                    .append(" where ContNo = '").append(contNo).append("' ").
                    append(" and InsuredNo = '").append(insuredNo).append("'");
            mMap.put(delInsured.toString(), "DELETE");
        }
    }

    /**
    *
    * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
    */
   private void dealTYData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
   {
       String sqlfeetrace = "delete from LPInsureAccFeeTrace " +
               "where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
               "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
               "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqlfeetrace, "DELETE");
       String sqltrace = "delete from LPInsureAccTrace " +
       		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
       		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
       		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqltrace, "DELETE");
       String sqlacc = "delete from LPInsureAcc " +
       "where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
       "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
       "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqlacc, "DELETE");
       String sqlfee = "delete from LPInsureAccFee " +
 		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
 		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
 		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqlfee, "DELETE");
       String sqlfeeclass = "delete from LPInsureAccClassFee " +
       	"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
       	"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
       	"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqlfeeclass, "DELETE");
       String sqlaccclass = "delete from LPInsureAccClass " +
       	"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
       	"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
       	"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqlaccclass, "DELETE");
       String sqlaccbalance = "delete from LPInsureAccBalance " +
     	"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
     	"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " ;
     mMap.put(sqlaccbalance, "DELETE");
       //删除被保人清单的xml数据
       String sql = "delete from LPEdorPrint2 " +
               "where EdorNo = '" + mEdorNo + "'";
       mMap.put(sql, "DELETE");
   }

    /**
    *
    * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
    */
   private void dealTQData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
   {
       //更新LCInsuredList的状态
       String importsql = "update lpdiskimport " +  "set State = '0' " + 
               "where EdorNo = '" + mEdorNo + "' and state = '1'";
       mMap.put(importsql, "UPDATE");
       String sqlfeetrace = "delete from LPInsureAccFeeTrace " +
               "where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
               "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
               "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqlfeetrace, "DELETE");
       String sqltrace = "delete from LPInsureAccTrace " +
       		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
       		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
       		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqltrace, "DELETE");
       String sqlacc = "delete from LPInsureAcc " +
       "where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
       "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
       "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqlacc, "DELETE");
       String sqlfee = "delete from LPInsureAccFee " +
 		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
 		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
 		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqlfee, "DELETE");
       String sqlfeeclass = "delete from LPInsureAccClassFee " +
       	"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
       	"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
       	"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqlfeeclass, "DELETE");
       String sqlaccclass = "delete from LPInsureAccClass " +
       	"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
       	"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
       	"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqlaccclass, "DELETE");
       String sqlaccbalance = "delete from LPInsureAccBalance " +
     	"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
     	"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " ;
     mMap.put(sqlaccbalance, "DELETE");
       //删除被保人清单的xml数据
       String sql = "delete from LPEdorPrint2 " +
               "where EdorNo = '" + mEdorNo + "'";
       mMap.put(sql, "DELETE");
   }
    
    
    
    private void dealWZData(LPGrpEdorItemSchema tLPGrpEdorItemSchema)
    {
        dealNIData(tLPGrpEdorItemSchema);
    }

    private LCPolSet getLCPolSet(String grpContNo)
    {
//        String sql = "select * from LCPol " +
//                "where GrpContNo = '" + grpContNo + "' " +
//                "and AppFlag = '" + BQ.APPFLAG_EDOR + "' " +
//                "and ContNo in (select ContNo from LCInsuredList " +
//                "   where GrpContNo = '" + grpContNo + "' " +
//                "   and EdorNo = '" + mEdorNo + "' ) ";
        String sql = "select * from LCPol "
        	+ "where GrpContNo = '" + grpContNo + "' " 
        	+ "and AppFlag = '" + BQ.APPFLAG_EDOR + "' " 
        	+ "and StateFlag = '" + BQ.APPFLAG_INIT + "' ";
        System.out.println("getLCPolSet:" + sql);
        LCPolDB tLCPolDB = new LCPolDB();
        return tLCPolDB.executeQuery(sql);
    }
    
    private LCPolSet getLCPolSetTZ(String grpContNo)
    {
//        String sql = "select * from LCPol " +
//                "where GrpContNo = '" + grpContNo + "' " +
//                "and AppFlag = '" + BQ.APPFLAG_EDOR + "' " +
//                "and ContNo in (select ContNo from LCInsuredList " +
//                "   where GrpContNo = '" + grpContNo + "' " +
//                "   and EdorNo = '" + mEdorNo + "' ) ";
        String sql = "select * from LCPol "
        	+ "where GrpContNo = '" + grpContNo + "' " 
        	+ "and AppFlag = '" + BQ.APPFLAG_EDOR + "' " 
        	+ "and StateFlag is null ";
        System.out.println("getLCPolSet:" + sql);
        LCPolDB tLCPolDB = new LCPolDB();
        return tLCPolDB.executeQuery(sql);
    }

    /**
     * 处理增加被保人项目的数据
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     */
    private void dealNIData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        //删除被保人清单的xml数据
    	long b = System.currentTimeMillis();
        String sql = "delete from LPEdorPrint2 " +
                "where EdorNo = '" + mEdorNo + "' and "+b+"="+b;
        mMap.put(sql, "DELETE");

        //更新LCInsuredList的状态
        sql = "update LCInsuredList " +
                "set State = '0' " + //, Publicacc = (CASE WHEN Edorprem is null then 0 else decimal(EdorPrem) end ) " +
                "where EdorNo = '" + mEdorNo + "' and state = '1'";
        mMap.put(sql, "UPDATE");
        
        //删除理算后插入C表的数据，要考虑连带被保人，删除LCCont要放在前面
//        sql = "delete from LCCont a " +
//                "where not exists " +
//                " ( select * from LCInsured " +
//                " where ContNo = a.ContNo " +
//                " and RelationToMainInsured <> '00') " +
//                "and AppFlag = '" + BQ.APPFLAG_EDOR + "' " +
//                "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() +
//                "'";
//        mMap.put(sql, "DELETE");

//        sql = "delete from LCCont a " +
//              "where exists (select * from LCInsuredList " +
//              " where ContNo = a.ContNo and Relation = '00'" +
//              " and ContPlanCode <> 'FM' and EdorNo = '" + mEdorNo +
//              "') and AppFlag = '" + BQ.APPFLAG_EDOR + "' " +
//              "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() +
//              "'";
        //20081028 modify zhanggm 删除LCCont表中所有增人数据。如果理算时产生重复数据，只删除LCInsuredList表中人的话,
        //冗余数据会留在LCCont表。添加保全项目时已经有校验，同一保单的增人项目没结案时不允许新添加增人项目。
        sql = "delete from LCCont a " 
        	+ "where AppFlag = '" + BQ.APPFLAG_EDOR + "' " 
        	+ "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' "
        	+ "and StateFlag = '" + BQ.APPFLAG_INIT + "' ";
        mMap.put(sql, "DELETE");

//        sql = "select * from LCCont a " +
//                "where exists " +
//                " ( select * from LCInsuredList " +
//                " where ContNo = a.ContNo " +
//                " and Relation = '00' and ContPlanCode = 'FM' and EdorNo = '" + mEdorNo +
//                "') and AppFlag = '" + BQ.APPFLAG_EDOR + "' " +
//                "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "'";
        sql = "select * from LCCont a " 
        	+ "where AppFlag = '" + BQ.APPFLAG_EDOR + "' " 
        	+ "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' "
        	+ "and StateFlag = '" + BQ.APPFLAG_SIGN + "' ";
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = tLCContDB.executeQuery(sql);
        for (int i = 1; i <= tLCContSet.size(); i++)
        {
            LCContSchema tLCContSchema = tLCContSet.get(i);
            String premSql = "select a.prem from LCPol a, LCInsured b " +
                    "where a.ContNo = b.ContNo  " +
                    "and a.InsuredNo = b.InsuredNo " +
                    "and a.AppFlag = '" + BQ.APPFLAG_SIGN + "' " +
                    "and b.RelationToMainInsured = '00' " +
                    "and a.ContNo = '" + tLCContSchema.getContNo() + "' ";
            String prem = (new ExeSQL()).getOneValue(premSql);
            tLCContSchema.setPrem(prem);
            tLCContSchema.setAppFlag(BQ.APPFLAG_SIGN);
            mMap.put(tLCContSchema, "DELETE&INSERT");
        }

        String[] tables = {"LCPol", "LCDuty", "LCPrem", "LCGet"};
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        LCPolSet tLCPolSet = getLCPolSet(grpContNo);
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            String polNo = tLCPolSet.get(i).getPolNo();
            for (int j = 0; j < tables.length; j++)
            {
                StringBuffer delSql = new StringBuffer("delete ");
                delSql.append("from ").append(tables[j])
                        .append(" where PolNo = '").append(polNo).append("'");
                mMap.put(delSql.toString(), "DELETE");
            }
            String contNo = tLCPolSet.get(i).getContNo();
            String insuredNo = tLCPolSet.get(i).getInsuredNo();
            //查询系统中是否存在该客户的其他保单，如果没有删除新增的客户。
//            LCContDB mLCContDB =new LCContDB();
//            mLCContDB.setInsuredNo(insuredNo);
//            LCContSet mLCContSet = mLCContDB.query();
//            LBContDB mLBContDB =new LBContDB();
//            mLBContDB.setInsuredNo(insuredNo);
//            LBContSet  mLBContSet = mLBContDB.query();
//            if(mLCContSet.size()==0&&mLBContSet.size()==0){
//                System.out.println("不存在该客户的其他保单");
//                StringBuffer delPerson = new StringBuffer("delete ");
//                delPerson.append("from ").append("LDPerson ")
//                        .append(" where CustomerNo = '").append(insuredNo).
//                        append("'");
//                mMap.put(delPerson.toString(), "DELETE");
//            }
            StringBuffer delInsured = new StringBuffer("delete ");
            delInsured.append("from ").append("LCInsured ")
                    .append(" where ContNo = '").append(contNo).append("' ").
                    append(" and InsuredNo = '").append(insuredNo).append("'");
            mMap.put(delInsured.toString(), "DELETE");
        }
        //将团单表里面的人数恢复到撤销之前
        sql = "update lcgrpcont " +
        		" set peoples2 = (" +
        		" select count(1) from LCInsured where GrpContNo='" + aLPGrpEdorItemSchema.getGrpContNo() + "') " +
        		" where GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' "
        		;
        mMap.put(sql, "UPDATE");
    }

    /**
     *
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     */
    private void dealGAData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        String sql = "delete from LPInsureAcc " +
                "where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
                "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
                "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        mMap.put(sql, "DELETE");
    }
    
    /**
    *
    * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
    */
   private void dealTLData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
   {
       String sqlfeetrace = "delete from LPInsureAccFeeTrace " +
               "where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
               "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
               "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqlfeetrace, "DELETE");
       String sqltrace = "delete from LPInsureAccTrace " +
       		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
       		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
       		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
       mMap.put(sqltrace, "DELETE");
       //删除被保人清单的xml数据
       String sql = "delete from LPEdorPrint2 " +
               "where EdorNo = '" + mEdorNo + "'";
       mMap.put(sql, "DELETE");
   }

    /**
     *
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     */
    private void dealCMData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        //更新LPEdorItem表状态
        String sql = "update LPEdorItem " +
                "set EdorState = '" + BQ.EDORSTATE_INPUT + "' " +
                "where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
                "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "'";
        mMap.put(sql, "UPDATE");

        //重设保费
//        sql = "update LPCont " +
//                "set (StandPrem, Prem, SumPrem, Ammnt) = " +
//                "    (select StandPrem, Prem, SumPrem, Ammnt " +
//                "     from LCCont where ContNo = )";
    }

    /**
     * 无名单实名化
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     */
    private boolean dealWSData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        String edorNo = aLPGrpEdorItemSchema.getEdorNo();
        String edorType = aLPGrpEdorItemSchema.getEdorType();
        String grpContNo = aLPGrpEdorItemSchema.getGrpContNo();
        //拆分为单个提交后的特殊处理,100人提交一次删除
        RSWrapper tRSWrapper = new RSWrapper();
        LPContSet tLPContSet = new LPContSet();
        String queryData = "select * from lpcont a where edorno ='"+edorNo+"' and edortype ='WS' "
                           +" and grpcontno ='"+grpContNo+"' and "
                           +" exists (select 1 from lccont b where b.proposalcontno = a.proposalcontno and b.grpcontno = a.grpcontno)"
                           ;

        tRSWrapper.prepareData(tLPContSet,queryData);
        do {
            tRSWrapper.getData();
            //循环取得5000条数据，进行处理
            MMap tMMap = new MMap();
            for(int i=1;i<=tLPContSet.size();i++)
            {
                LPContSchema tLPContSchema = tLPContSet.get(i);

                String delLCGet = " delete from lcget where grpcontno ='"+grpContNo+"'"
                                  + " and contno ='"+tLPContSchema.getContNo()+"'"
                                  ;
                String delLCPrem = " delete from lcprem where grpcontno ='"+grpContNo+"'"
                                   + " and contno ='"+tLPContSchema.getContNo()+"'"
                                   ;
                String delLCduty = " delete from lcduty where contno ='"+tLPContSchema.getContNo()+"'"
                                   ;
                String delLCPol =  " delete from lcpol where grpcontno ='"+grpContNo+"'"
                                   + " and contno ='"+tLPContSchema.getContNo()+"'"
                                   ;
                String delLCCont = " delete from lccont where grpcontno ='"+grpContNo+"'"
                                   + " and contno ='"+tLPContSchema.getContNo()+"'"
                                   ;
                String delLCInsured =  " delete from lcinsured where grpcontno ='"+grpContNo+"'"
                                       + " and contno ='"+tLPContSchema.getContNo()+"'"
                                       ;

                tMMap.put(delLCGet, "DELETE");
                tMMap.put(delLCPrem, "DELETE");
                tMMap.put(delLCduty, "DELETE");
                tMMap.put(delLCPol, "DELETE");
                tMMap.put(delLCCont, "DELETE");
                tMMap.put(delLCInsured, "DELETE");
                //提交100个人的删除
                if(i%100==0)
                {
                    if(!delWSSubmit(tMMap))
                        return false;
                    tMMap = new MMap();
                }
            }
            //处理不满足100条的
            if(tMMap.size()>0)
            {
                if(!delWSSubmit(tMMap))
                    return false;
            }
        }while (tLPContSet.size() > 0);



        //LPDuty没有GrpContNo，要特殊处理，连LPol表要放在前面进行
        String sql = "delete from LPDuty " +
                     "where EdorNo = '" + edorNo + "' " +
                     "and EdorType = '" + edorType + "' " +
                     "and PolNo = (select PolNo from LPPol " +
                     "    where EdorNo = '" + edorNo + "' " +
                     "    and EdorType = '" + edorType + "' " +
                     "    and GrpContNo = '" + grpContNo + "' " +
                     "    and PolNo = LPDuty.PolNo) ";
        mMap.put(sql, "DELETE");

        String[] tables = {"LPCont", "LPInsured", "LPPol", "LPPrem", "LPGet"};
        for (int i = 0; i < tables.length; i++)
        {
            sql = "delete from " + tables[i] +
                  " where EdorNo = '" + edorNo + "' " +
                  "and EdorType = '" + edorType + "' " +
                  "and GrpContNo = '" + grpContNo + "'";
            mMap.put(sql, "DELETE");
        }
        
        sql = "delete from lpaddress where EdorNo = '" + edorNo + "' " +
                "and EdorType = '" + edorType + "' ";
          mMap.put(sql, "DELETE");
        
        //将导入表的理算成功标记清除
        String updateImport = "update lpdiskimport set State ='1' where Edorno = '"+edorNo+"' "+
                              "and EdorType = '" + edorType + "' " +
                              "and GrpContNo = '" + grpContNo + "' and state ='2'"
                              ;
        mMap.put(updateImport, "UPDATE");
        return true;
    }
    private boolean delWSSubmit(MMap aMMap)
    {
            //提交数据库
            PubSubmit tPubSubmit = new PubSubmit();
            VData tInputData = new VData();
            tInputData.clear();
            tInputData.add(aMMap);
            if (!tPubSubmit.submitData(tInputData, ""))
            {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "GEdorReCalBL";
                    tError.functionName = "dealWSData";
                    tError.errorMessage = "实名化重复理算提交失败!";
                    this.mErrors.addOneError(tError);
                    return false;
            }
            return true;
    }
    
    private boolean dealGXData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
    	String SQL ="delete from lpcont where edorno ='"+aLPGrpEdorItemSchema.getEdorNo()+"'" ;
    	mMap.put(SQL, "DELETE");
    	return true;
    }
    /**
     * 删除P表的轨迹记录（acc.acctrace ,accclass,fee, feetrace classfee六个表)
     * @return boolean
     */
    private boolean dealHLData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    { 
    	String tUpdateLgget = " update lpget a set (ActuGet,SumMoney,StandMoney) = (select ActuGet,SumMoney,StandMoney from lcget where polno = a.polno and dutycode = a.dutycode and getdutycode = a.getdutycode) "
        + " WHERE a.EdorType = 'HL' and a.EdorNo = '"+aLPGrpEdorItemSchema.getEdorNo()+"'"
        ;
    	mMap.put(tUpdateLgget, "DELETE");
    	String sqlacc = "delete from LPInsureAcc " +
        "where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
        "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
        "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        mMap.put(sqlacc, "DELETE");
        String sqlaccclass = "delete from LPInsureAccclass " +
		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        mMap.put(sqlaccclass, "DELETE");
        
        String sqlaccctrace = "delete from LPInsureAcctrace " +
		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        mMap.put(sqlaccctrace, "DELETE");
        
    	 String sqlfeetrace = "delete from LPInsureAccFeeTrace " +
         "where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
         "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
         "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
         mMap.put(sqlfeetrace, "DELETE");
         String accclassfee = "delete from lpinsureaccclassfee " +
 		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
 		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
 		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
         mMap.put(accclassfee, "DELETE");
         String accfee = "delete from lpinsureaccfee " +
  		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
  		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
  		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
          mMap.put(accfee, "DELETE");
          String accbalance = "delete from lpinsureaccbalance " +
    	"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
    	"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "'";
         mMap.put(accbalance, "DELETE");
          
    	return true;
    }
    private boolean dealZTData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    { 
    	String sqlacc = "delete from LPInsureAcc " +
        "where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
        "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
        "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        mMap.put(sqlacc, "DELETE");
        String sqlaccclass = "delete from LPInsureAccclass " +
		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        mMap.put(sqlaccclass, "DELETE");
        
        String sqlaccctrace = "delete from LPInsureAcctrace " +
		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
        mMap.put(sqlaccctrace, "DELETE");
        
    	 String sqlfeetrace = "delete from LPInsureAccFeeTrace " +
         "where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
         "and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
         "and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
         mMap.put(sqlfeetrace, "DELETE");
         
         String accclassfee = "delete from lpinsureaccclassfee " +
 		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
 		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
 		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
         mMap.put(accclassfee, "DELETE");
         
         String accfee = "delete from lpinsureaccfee " +
  		"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
  		"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "' " +
  		"and GrpContNo = '" + aLPGrpEdorItemSchema.getGrpContNo() + "' ";
          mMap.put(accfee, "DELETE");
          
          String accbalance = "delete from lpinsureaccbalance " +
    	"where EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "' " +
    	"and EdorType = '" + aLPGrpEdorItemSchema.getEdorType() + "'";
         mMap.put(accbalance, "DELETE");
          
    	return true;
    }
    
    /**
    * 提交数据到数据库
    * @return boolean
    */
   private boolean submit()
   {
       VData data = new VData();
       data.add(mMap);
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(data, ""))
       {
           mErrors.copyAllErrors(tPubSubmit.mErrors);
           return false;
       }
       return true;
   }
   /**
    * 加入到保全结案日志表数据
    * @param
    * @return boolean
    */
   private boolean dealUrgeLog(String pmDealState, String pmOpreat) {

       LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
               LCUrgeVerifyLogSchema();
       //加到保全结案日志表
       tLCUrgeVerifyLogSchema.setSerialNo(mEdorNo);
       tLCUrgeVerifyLogSchema.setRiskFlag("1");
       tLCUrgeVerifyLogSchema.setEndDate(mCurrentDate);

       tLCUrgeVerifyLogSchema.setOperateType("3"); //1：续期催收操作,2：续期核销操作，3：保全结案操作
       tLCUrgeVerifyLogSchema.setOperateFlag("3"); //1：个案操作,2：批次操作  3：保全团单结案操作
       tLCUrgeVerifyLogSchema.setOperator("Server");
       tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成
       if (pmOpreat.equals("INSERT")) {
           tLCUrgeVerifyLogSchema.setMakeDate(mCurrentDate);
           tLCUrgeVerifyLogSchema.setMakeTime(mCurrentTime);
           tLCUrgeVerifyLogSchema.setModifyDate(mCurrentDate);
           tLCUrgeVerifyLogSchema.setModifyTime(mCurrentTime);
           tLCUrgeVerifyLogSchema.setErrReason("");

       } 
       else if(pmOpreat.equals("UPDATE"))
       {
           LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
           LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new LCUrgeVerifyLogSet();
           tLCUrgeVerifyLogDB.setSerialNo(mEdorNo);
           tLCUrgeVerifyLogDB.setOperateType("3");
           tLCUrgeVerifyLogDB.setOperateFlag("3");
           tLCUrgeVerifyLogDB.setRiskFlag("1");
           tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
           if (!(tLCUrgeVerifyLogSet == null) &&
               tLCUrgeVerifyLogSet.size() > 0) {
               tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
               tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
               tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
               tLCUrgeVerifyLogSchema.setDealState(pmDealState);
           }else{
               return false;
           }
       }


       MMap tMap = new MMap();
       tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
       VData tInputData = new VData();
       tInputData.add(tMap);
       PubSubmit tPubSubmit = new PubSubmit();
       if (tPubSubmit.submitData(tInputData, "") == false) {
           this.mErrors.addOneError("该工单正在进行保全确认!");
           return false;
       }
       return true;
   }
}
