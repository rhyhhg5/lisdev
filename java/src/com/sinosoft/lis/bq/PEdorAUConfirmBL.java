package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.db.LPPersonDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPPersonSchema;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPPersonSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 客户信息授权变更</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite liuyang
 * @version 1.0
 */
public class PEdorAUConfirmBL implements EdorConfirm {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData;
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	private MMap mMap = new MMap();

	private LPEdorItemSchema mLPEdorItemSchema = null;

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();

		// 得到外部传入的数据,将数据备份到本类中
		getInputData(cInputData);

		// 数据准备操作（preparedata())
		if (!prepareData()) {
			return false;
		}

		return true;
	}

	public VData getResult() {
		return mResult;
	}

	/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		mLPEdorItemSchema = (LPEdorItemSchema) cInputData
				.getObjectByObjectName("LPEdorItemSchema", 0);
		// 全局变量
		mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0));
		if (mLPEdorItemSchema == null || mGlobalInput == null) {
			mErrors.addOneError(new CError("传入数据不完全！"));
			return false;
		}
		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
		tLPEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
		tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
		tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
		tLPEdorItemDB.setPolNo(mLPEdorItemSchema.getPolNo());
		tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
		LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
		if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1) {
			mErrors.addOneError(new CError("查询批改项目信息失败！"));
			return false;
		}
		mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));
		return true;
	}

	/**
	 * 准备需要保存的数据
	 */
	private boolean prepareData() {

		LDPersonSet saveLDPersonSet = new LDPersonSet();
		LPPersonSet saveLPPersonSet = new LPPersonSet();
		Reflections tRef = new Reflections();

		// 得到当前保单的投保人资料
		boolean tNewAdd = false;
		LDPersonSchema aLDPersonSchema = new LDPersonSchema();
		LPPersonSchema aLPPersonSchema = new LPPersonSchema();
		LDPersonSet tLDPersonSet = new LDPersonSet();
		LPPersonSet tLPPersonSet = new LPPersonSet();

		LPPersonSchema tLPPersonSchema = new LPPersonSchema();
		tLPPersonSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPPersonSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPPersonSchema.setCustomerNo(mLPEdorItemSchema.getInsuredNo());
		LPPersonDB tLPPersonDB = new LPPersonDB();
		tLPPersonDB.setSchema(tLPPersonSchema);
		tLPPersonSet = tLPPersonDB.query();
		for (int j = 1; j <= tLPPersonSet.size(); j++) {
			aLPPersonSchema = tLPPersonSet.get(j);
			LDPersonSchema tLDPersonSchema = new LDPersonSchema();
			tLPPersonSchema = new LPPersonSchema();

			LDPersonDB aLDPersonDB = new LDPersonDB();
			aLDPersonDB.setCustomerNo(aLPPersonSchema.getCustomerNo());
			if (!aLDPersonDB.getInfo()) {
				if (aLDPersonDB.mErrors.needDealError()) {
					mErrors.copyAllErrors(aLDPersonDB.mErrors);
					mErrors.addOneError(new CError("查询客戶信息失败！"));
					return false;
				}
				tNewAdd = true;
				tRef.transFields(tLDPersonSchema, aLPPersonSchema);
				tLDPersonSchema.setModifyDate(PubFun.getCurrentDate());
				tLDPersonSchema.setModifyTime(PubFun.getCurrentTime());
				tLDPersonSet.add(tLDPersonSchema);
				saveLDPersonSet.add(tLDPersonSchema);
			} else {
				tNewAdd = false;
				aLDPersonSchema = aLDPersonDB.getSchema();
				tRef.transFields(tLPPersonSchema, aLDPersonSchema);
				tLPPersonSchema.setEdorNo(aLPPersonSchema.getEdorNo());
				tLPPersonSchema.setEdorType(aLPPersonSchema.getEdorType());

				// 转换成保单个人信息。
				tRef.transFields(tLDPersonSchema, aLPPersonSchema);
				tLDPersonSchema.setModifyDate(PubFun.getCurrentDate());
				tLDPersonSchema.setModifyTime(PubFun.getCurrentTime());

				saveLPPersonSet.add(tLPPersonSchema);
				tLDPersonSet.add(tLDPersonSchema);
				saveLDPersonSet.add(tLDPersonSchema);
			}
		}

		// 得到当前LPEdorItem保单信息主表的状态，并更新状态为申请确认。
		mLPEdorItemSchema.setEdorState("0");

		mMap.put(mLPEdorItemSchema, "UPDATE");

		if (tNewAdd) {
			mMap.put(saveLDPersonSet, "INSERT");
		} else {
			mMap.put(saveLDPersonSet, "UPDATE");
			mMap.put(saveLPPersonSet, "UPDATE");
		}
		mResult.add(mMap);
		return true;
	}
}
