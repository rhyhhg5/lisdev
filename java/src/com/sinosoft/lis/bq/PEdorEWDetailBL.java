package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong
 * @version 1.0
 */
public class PEdorEWDetailBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    private MMap mMap = new MMap();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

    private LPContSubSet mLPContSubSet = new LPContSubSet();
    private LCContSubSchema mLCContSubSchema  = new LCContSubSchema(); 
    private GlobalInput mGlobalInput = new GlobalInput();
    private String currDate=PubFun.getCurrentDate();
    private String currTime=PubFun.getCurrentTime();

    public PEdorEWDetailBL()
    {
    }

	public boolean submitData(VData cInputData, String cOperate)
	{
        if (getSubmitData(cInputData, cOperate) == null)
        {
            return false;
        }
		PubSubmit tSubmit = new PubSubmit();
		if (!tSubmit.submitData(mResult, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorICDetailBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}
	
	/**
	 * 
	 * @param cInputData
	 * @param cOperate
	 * @return
	 */
	public boolean submitInsuData(VData cInputData, String cOperate)
	{

        if (getSubmitInsuData(cInputData, cOperate) == null)
        {
            return false;
        }
		PubSubmit tSubmit = new PubSubmit();
		
		if (!tSubmit.submitData(mResult, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorEWDetailBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	public MMap getSubmitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		mOperate = cOperate;

		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData())
		{
			return null;
		}

		//数据准备操作
		if (!dealData())
		{
			return null;
		}
		return mMap;
	}
	public MMap getSubmitInsuData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		mOperate = cOperate;

		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData())
		{
			return null;
		}
		//数据查询业务处理
//		if (cOperate.equals("QUERY||MAIN") || cOperate.equals("QUERY||DETAIL"))
//		{
//			if (!queryInsuData())//被保人信息----
//			{
//				return null;
//			}
//			else
//			{
//				return null;
//			}
//		}
		
		//数据准备操作
		if (!dealData())
		{
			return null;
		}
		return mMap;
	}

    public VData getResult()
    {
        return mResult;
    }
  

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData()
    {
        try
        {

            mLPEdorItemSchema = (LPEdorItemSchema) mInputData
                                .getObjectByObjectName("LPEdorItemSchema", 0);
            mLCContSubSchema = (LCContSubSchema) mInputData
                         .getObjectByObjectName("LCContSubSchema", 0);
            mGlobalInput = (GlobalInput) mInputData.
                           getObjectByObjectName("GlobalInput", 0);
            
//            String StrSQL="select 1 from LCAppnt where contno='"+mLPEdorItemSchema.getContNo()+"' " +
//            		" and appntno='"+mLPEdorItemSchema.getInsuredNo()+"' with ur";
//            String isAppnt=new ExeSQL().getOneValue(StrSQL);
//            if(null==isAppnt||"".equals(isAppnt)){//非投保人即为被保人联系方式变更--BQ
//            	
////            	if(!BQ.FILLDATA.equals(mLPEdorItemSchema.getInsuredNo())){	//保全项目中客户号为空则认为为投保人变更
//////            		isInsured=true;
////            		System.out.println("bq---被保险人【'"+mLPEdorItemSchema.getInsuredNo()+"'】联系方式变更");
////            	}
//            }
//            //兼容简易保全
            String strInsuNo="select insuredno from lpedoritem " +
    			" where edorno='"+mLPEdorItemSchema.getEdorNo()+"' " +
    			" and edortype='"+mLPEdorItemSchema.getEdorType()+"' with ur";
            String customerno=new ExeSQL().getOneValue(strInsuNo);
            if(BQ.FILLDATA.equals(customerno)){
            	mLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
            }
//            

            	if (mLCContSubSchema == null 
                        || mLPEdorItemSchema == null )
                    {
                        CError.buildErr(this, "接收数据失败");
                        return false;
                    }

        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData()
    {
 
    	LCContSubSchema tLCContSubSchema = new LCContSubSchema();
    	LCContSubDB  fLCContSubDB = new LCContSubDB();  	
    	fLCContSubDB.setPrtNo(mLCContSubSchema.getPrtNo());
           if(!fLCContSubDB.getInfo()){
             CError tError = new CError();
             tError.moduleName = "PEdorEWDetailBL";
             tError.functionName = "dealData";
             tError.errorMessage = "查询保单信息失败！";
             this.mErrors.addOneError(tError);
             return false;
           }
           tLCContSubSchema = fLCContSubDB.getSchema();
    	
    	 LPContSubSchema tLPContSubSchema = new LPContSubSchema();
         Reflections ref = new Reflections();
         ref.transFields(tLPContSubSchema, tLCContSubSchema);
         
         tLPContSubSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
         tLPContSubSchema.setEdorType(mLPEdorItemSchema.getEdorType());
         tLPContSubSchema.setOperator(mGlobalInput.Operator);
         if(null != mLCContSubSchema.getIfAutoPay()){
         tLPContSubSchema.setIfAutoPay(mLCContSubSchema.getIfAutoPay());
         }
         if(null != mLCContSubSchema.getRenemalPayMethod()){
             tLPContSubSchema.setRenemalPayMethod(mLCContSubSchema.getRenemalPayMethod());
             }
         if(null != mLCContSubSchema.getMedicalCode()){
             tLPContSubSchema.setMedicalCode(mLCContSubSchema.getMedicalCode());
             }
         tLPContSubSchema.setMakeDate(PubFun.getCurrentDate());
         tLPContSubSchema.setMakeTime(PubFun.getCurrentTime());
         tLPContSubSchema.setModifyDate(PubFun.getCurrentDate());
         tLPContSubSchema.setModifyTime(PubFun.getCurrentTime());
         mLPContSubSet.add(tLPContSubSchema);

        //处理保全项目状态
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
      
        tLPEdorItemDB.setEdorType("EW");
        tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        mLPEdorItemSchema = tLPEdorItemSet.get(1);
        
        mLPEdorItemSchema.setEdorState("1"); //录入完成
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setModifyDate(currDate);
        mLPEdorItemSchema.setModifyTime(currTime);


		mMap.put(mLPContSubSet, "DELETE&INSERT");
		mMap.put(mLPEdorItemSchema, "UPDATE");
        mResult.clear();
        mResult.add(mMap);

        return true;
    }
}
