package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;

/**
 * <p>Title: 保全特需医疗团体帐户余额分配磁盘导入类</p>
 * <p>Description: 导入被保人分配金额 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class SolvencyImportUI
{
    private SolvencyImportBL mSolvencyImportBL = null;

    /**
     * 构造函数
     * @param fileName String
     * @param configFileName String
     */
    public SolvencyImportUI(String fileName, String configFileName)
    {
    	mSolvencyImportBL = new SolvencyImportBL(fileName, configFileName);
    }

    /**
     * 调用业务逻辑
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!mSolvencyImportBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mSolvencyImportBL.mErrors.getFirstError();
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        String fileName = "F:/PROJECT/lis_coding/ui/temp/偿付能力充足率清单.xls";
        String configFileName = "F:/PROJECT/lis_coding/ui/temp/SolvencyImport.xml";


        GlobalInput gi = new GlobalInput();
        gi.ManageCom = "86";
        gi.Operator = "endor";

        VData data = new VData();
        data.add(gi);
        SolvencyImportUI tSolvencyImportUI =
                new SolvencyImportUI(fileName, configFileName);
        if (!tSolvencyImportUI.submitData(data))
        {
            System.out.println(tSolvencyImportUI.getError());
        }
    }
}
