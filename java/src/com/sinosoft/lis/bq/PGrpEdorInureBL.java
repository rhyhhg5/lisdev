package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpContStateDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PGrpEdorInureBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    
    /** 往后面传输数据的容器 */
    private VData mInputData= new VData();
    
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();
    
    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();
    
    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();
    
    /** 数据操作字符串 */
    private String mOperate; 
    
    private String mContNo;
    
    public PGrpEdorInureBL(GlobalInput gi,String contNo){
    	this.mGlobalInput = gi;
    	this.mContNo = contNo;
    }
    
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(){
  
    	if (!getInputData()){
            return false;
        }

    	if(!checkData()){
    		return false;
    	}
    	
    	//进行业务处理
    	if(!dealData()){
    		
            return false;
    	}
    	
    	//装配处理好的数据，准备给后台进行保存    	
    	if (!prepareOutputData()){
            return false;
        }
        
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)){
            // @@错误处理
            System.out.println("PGrpEdorInureBL中提交数据出错");
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "TaskDeliverBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        
    	return true;
    }
    
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(){
    	
        return true;
    }
    
    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData(){
    	LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    	tLCGrpContDB.setGrpContNo(mContNo);
    	if(!tLCGrpContDB.getInfo()){
    		CError tError = new CError();
    	    tError.moduleName = "LCGrpContDB";
    	    tError.functionName = "LCGrpContDB";
    	    tError.errorMessage = "无数据!";
    	    System.out.println("------" + tError);
    	    this.mErrors.addOneError(tError);
    	    return false;
    	}
    	
    	LCContDB tLCContDB = new LCContDB();
    	tLCContDB.setGrpContNo(mContNo);
    	if(tLCContDB.query().size() <= 0){
    		CError tError = new CError();
    	    tError.moduleName = "LCContDB";
    	    tError.functionName = "LCContDB";
    	    tError.errorMessage = "无数据!";
    	    System.out.println("------" + tError);
    	    this.mErrors.addOneError(tError);
    	    return false;
    	}
    	
    	LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
    	tLCGrpPolDB.setGrpContNo(mContNo);
    	if(tLCGrpPolDB.query().size() <= 0){
    		CError tError = new CError();
    	    tError.moduleName = "LCGrpPolDB";
    	    tError.functionName = "LCGrpPolDB";
    	    tError.errorMessage = "无数据!";
    	    System.out.println("------" + tError);
    	    this.mErrors.addOneError(tError);
    	    return false;
    	}
    	
    	LCPolDB tLCPolDB = new LCPolDB();
    	tLCPolDB.setGrpContNo(mContNo);
    	if(tLCPolDB.query().size() <= 0){
    		CError tError = new CError();
    	    tError.moduleName = "LCPolDB";
    	    tError.functionName = "LCPolDB";
    	    tError.errorMessage = "无数据!";
    	    System.out.println("------" + tError);
    	    this.mErrors.addOneError(tError);
    	    return false;
    	}
    	
    	LCGrpContStateDB tLCGrpContStateDB = new LCGrpContStateDB();
    	tLCGrpContStateDB.setGrpContNo(mContNo);
    	if(tLCGrpContStateDB.query().size() <= 0){
    		CError tError = new CError();
    	    tError.moduleName = "LCGrpContStateDB";
    	    tError.functionName = "LCGrpContStateDB";
    	    tError.errorMessage = "无数据!";
    	    System.out.println("------" + tError);
    	    this.mErrors.addOneError(tError);
    	    return false;
    	}
    	
    	LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
    	tLOPRTManagerDB.setOtherNo(mContNo);
    	if(tLOPRTManagerDB.query().size() <= 0){
    		CError tError = new CError();
    	    tError.moduleName = "LOPRTManagerDB";
    	    tError.functionName = "LOPRTManagerDB";
    	    tError.errorMessage = "无数据!";
    	    System.out.println("------" + tError);
    	    this.mErrors.addOneError(tError);
    	    return false;
    	}
        return true;
    }
    
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData(){
    	
    	String sql = " update LCGrpCont "
            + "set StateFlag='1', "
            + "ModifyDate='" + mCurrentDate + "', "
            + "ModifyTime='" + mCurrentTime + "', "
            + "Operator='" + mGlobalInput.Operator + "' "
            + "where GrpContNo='" + mContNo + "' ";
    	map.put(sql, "UPDATE");
    	
    	String sql1 = " update LCCont "
                   + "set StateFlag='1', "
                   + "ModifyDate='" + mCurrentDate + "', "
                   + "ModifyTime='" + mCurrentTime + "', "
                   + "Operator='" + mGlobalInput.Operator + "' "
                   + "where GrpContNo='" + mContNo + "' ";
            
    	map.put(sql1, "UPDATE");
    	
    	String sql2 = " update LCGrpPol "
                    + "set StateFlag='1', "
                    + "ModifyDate='" + mCurrentDate+  "', "
                    + "ModifyTime='" + mCurrentTime + "', "
                    + "Operator='" + mGlobalInput.Operator + "' "
                    + "where GrpContNo='" + mContNo + "' ";
    	
    	map.put(sql2, "UPDATE");
    	
    	String sql3 = " update LCPol "
                    + "set StateFlag='1', "
                    + "ModifyDate='" + mCurrentDate + "', "
                    + "ModifyTime='" + mCurrentTime + "', "
                    + "Operator='" + mGlobalInput.Operator + "' "
                    + "where GrpContNo ='" + mContNo + "' ";
    	
    	map.put(sql3, "UPDATE");
    	
    	String sql4 = "delete from LCGrpContState where GrpContNo='"+mContNo+"' ";
        map.put(sql4, "DELETE");
        
        String sql5 = "delete from LOPrtManager where OtherNo='"+mContNo+"' and Code='21' ";
        map.put(sql5, "DELETE");
        
    	return true;
    }
    
    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private boolean prepareOutputData(){
    	try{
            this.mInputData.clear();
            mInputData.add(this.map); 
        }catch(Exception ex){
    		CError tError =new CError();
     		tError.moduleName="LGGroupBL";
     		tError.functionName="prepareData";
     		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
     		this.mErrors .addOneError(tError) ;
    		return false;
    	}
    	return true;
    }
    
    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult(){
  	    return this.mResult;
	}
}
