 package com.sinosoft.lis.bq;

import java.util.List;
import java.util.ArrayList;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全个险自动核保</p>
 * <p>Description: 自动核保，如未通过则送人工核保</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class PEdorAutoUWBL
{
    /** 错误处理 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private MMap mMap = new MMap();

    private String mUWState = null;

    /** 用于输出的核保错误信息列表 */
    private List mUwErrorList = new ArrayList();

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PEdorAutoUWBL(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
    }

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到核保标志
     * @return String
     */
    public String getUWState()
    {
        return mUWState;
    }

    /**
     * 得到自动核未通过保原因
     * @return String
     */
    public String getUWErrors()
    {
        String uwErrors = "";
        for (int i = 0; i < mUwErrorList.size(); i++)
        {
            uwErrors += (String) mUwErrorList.get(i) + "\n";
        }
        return uwErrors;
    }

    /**
     * 从输入数据中得到所有对象
     * @return boolean
     */
    private boolean getInputData()
    {
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        if (!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("未找到该保全受理信息！");
            return false;
        }

        this.mUWState = tLPEdorAppDB.getUWState();
        if ((mUWState == null) || mUWState.equals(BQ.UWFLAG_INIT))
        {
			ExeSQL tExeSQL = new ExeSQL();
            String isNeedHumanUW = tExeSQL . getOneValue("select 1 from lpedoritem where edortype in (select code from ldcode where codetype = 'BQUW') and edorno = '"+mEdorNo+"'");
            boolean needHumanUW = "1".equals(isNeedHumanUW);
            if ((!autoUWConts()) || (!autoUWPols()) || needHumanUW)  //如果自核没有通过则，则送人工核保
            {
            	if(needHumanUW){
            		if(!creatMFUWError()){
            			return false;
            		}
            	}
                this.mUWState = BQ.UWFLAG_FAIL;
                if (!createWorkflow())
                {
                    return false;
                }
                createWorkTrace();
                setUWState(true);
            }
            else
            {
                this.mUWState = BQ.UWFLAG_PASS;
                setUWState(false);
            }
            if (!submit())
            {
                return false;
            }
        }
        else if (mUWState.equals(BQ.UWFLAG_FAIL))
        {
            autoUWConts();
            autoUWPols();
        }
        return true;
    }

    private boolean isNeedAutoUW()
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        if (!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("未找到该保全受理信息！");
            return false;
        }
        this.mUWState = tLPEdorAppDB.getUWState();
        if ((mUWState == null) || (mUWState.equals("")) ||
                (mUWState.equals(BQ.UWFLAG_INIT)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * 如果已自核过则只是查出自核信息，不置状态
     * @return boolean
     */
    private boolean checkUWState()
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        if (!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("未找到该保全受理信息！");
            return false;
        }
        String uwState = tLPEdorAppDB.getUWState();
        if ((uwState != null) && (!uwState.equals(BQ.UWFLAG_INIT)))
        {
            mUWState = tLPEdorAppDB.getUWState();
            System.out.println("该保全已做过核保！");
            return false;
        }
        return true;
    }

    /**
     * 查询Main表
     * @return boolean
     */
    private LPEdorMainSchema getEodrMain()
    {
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mEdorNo);
        LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.query();
        if (tLPEdorMainSet.size() == 0)
        {
            mErrors.addOneError("未找到该保全受理信息！");
            return null;
        }
        return tLPEdorMainSet.get(1);
    }

    /**
     * 查询工单信息
     * @return LGWorkSchema
     */
    private LGWorkSchema getWorkInfo()
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setDetailWorkNo(mEdorNo);
        LGWorkSet tLGWorkSet = tLGWorkDB.query();
        if (tLGWorkSet.size() == 0)
        {
            mErrors.addOneError("未找到该保全工单信息！");
            return null;
        }
        return tLGWorkSet.get(1);
    }

    /**
     * 对保单进行核保
     */
    private boolean autoUWConts()
    {
        boolean flag = true;
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mEdorNo);
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        for (int i = 1; i <= tLPEdorItemSet.size(); i ++)
        {
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i);
            String contNo = tLPEdorItemSchema.getContNo();
            String edorType = tLPEdorItemSchema.getEdorType();
            if (!autoUWOneCont(contNo, edorType))
            {
                flag = false;
            }
        }
        return flag;
    }

    /**
     * 对一个保单进行核保
     * @param tLPPolSchema LPPolSchema
     */
    private boolean autoUWOneCont(String contNo, String edorType)
    {
        boolean flag = true;
        LMUWDB tLMUWDB = new LMUWDB();
        tLMUWDB.setRelaPolType(BQ.AUTOUWTYPE_IC);
        tLMUWDB.setRiskCode(BQ.FILLDATA);
        tLMUWDB.setUWType(edorType);
        LMUWSet tLMUWSet = tLMUWDB.query();
        //遍历自核规则
        for (int i = 1; i <= tLMUWSet.size(); i++)
        {
            LMUWSchema tLMUWSchema = tLMUWSet.get(i);
            Calculator cal = new Calculator();
            cal.setCalCode(tLMUWSchema.getCalCode());
            cal.addBasicFactor("EdorNo", mEdorNo);
            cal.addBasicFactor("EdorType", edorType);
            cal.addBasicFactor("ContNo", contNo);
            String result = cal.calculate();
            if (!result.equals("0")) //核保未通过
            {
                flag = false;
                LPContSchema tLPContSchema = new LPContSchema();
                LPContDB tLPContDB = new LPContDB();
                tLPContDB.setEdorNo(mEdorNo);
                tLPContDB.setEdorType(edorType);
                tLPContDB.setContNo(contNo);
                if (!tLPContDB.getInfo())
                {
                    LCContDB tLCContDB = new LCContDB();
                    tLCContDB.setContNo(contNo);
                    if (!tLCContDB.getInfo())
                    {
                        mErrors.addOneError("未找到保单" + contNo + "的信息！");
                        return false;
                    }
                    Reflections ref = new Reflections();
                    ref.transFields(tLPContSchema, tLCContDB.getSchema());
                    tLPContSchema.setEdorNo(mEdorNo);
                    tLPContSchema.setEdorType(edorType);
                }
                setContUWError(tLMUWSchema.getRemark(), tLPContSchema);
                mUwErrorList.add(tLMUWSchema.getRemark());
            }
        }
        return flag;
    }

    /**
     * 对险种进行核保
     */
    private boolean autoUWPols()
    {
        boolean flag = true;
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i ++)
        {
            if (!autoUWOnePol(tLPPolSet.get(i)))
            {
                flag = false;
            }
        }
        return flag;
    }

    /**
     * 对一个险种进行核保
     * @param tLPPolSchema LPPolSchema
     */
    private boolean autoUWOnePol(LPPolSchema aLPPolSchema)
    {
        boolean flag = true;
        LCPolSchema tLCPolSchema = CommonBL.getLCPol(aLPPolSchema.getPolNo());
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(aLPPolSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(aLPPolSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if(tLPEdorItemSet.size() == 0)
        {
            mErrors.addOneError("没有查询到保全项目信息" + aLPPolSchema.getEdorNo()
                + "," + aLPPolSchema.getEdorType());
            return false;
        }

        LMUWDB tLMUWDB = new LMUWDB();
        tLMUWDB.setRelaPolType(BQ.AUTOUWTYPE_IP);
        tLMUWDB.setRiskCode(aLPPolSchema.getRiskCode());
        tLMUWDB.setUWType(aLPPolSchema.getEdorType());
        LMUWSet tLMUWSet = tLMUWDB.query();
        //遍历自核规则
        for (int i = 1; i <= tLMUWSet.size(); i++)
        {
            LMUWSchema tLMUWSchema = tLMUWSet.get(i);
            Calculator cal = new Calculator();
            cal.setCalCode(tLMUWSchema.getCalCode());
            cal.addBasicFactor("EdorNo", aLPPolSchema.getEdorNo());
            cal.addBasicFactor("EdorType", aLPPolSchema.getEdorType());
            cal.addBasicFactor("ContNo", aLPPolSchema.getContNo());
            cal.addBasicFactor("PolNo", aLPPolSchema.getPolNo());
            cal.addBasicFactor("InsuredNo", aLPPolSchema.getInsuredNo());
            cal.addBasicFactor("Amnt", String.valueOf(aLPPolSchema.getAmnt()));
            cal.addBasicFactor("Mult", String.valueOf(aLPPolSchema.getMult()));
            cal.addBasicFactor("NewPrem", String.valueOf(aLPPolSchema.getPrem()));
            cal.addBasicFactor("Prem", String.valueOf(tLCPolSchema.getPrem()));
            cal.addBasicFactor("EdorValiDate",
                               tLPEdorItemSet.get(1).getEdorValiDate());
            String result = cal.calculate();
            if (!result.equals("0")) //核保未通过
            {
                flag = false;
                setUWError(tLMUWSchema.getRemark(), aLPPolSchema);
                mUwErrorList.add(tLMUWSchema.getRemark());
            }
        }
        return flag;
    }


    /**
     * 记录自核错误信息
     * @param error String
     * @param aLPPolSchema LPPolSchema
     */
    private void setContUWError(String error, LPContSchema aLPContSchema)
    {
        LPUWErrorSchema tLPUWErrorSchema = new LPUWErrorSchema();
        tLPUWErrorSchema.setEdorNo(aLPContSchema.getEdorNo());
        tLPUWErrorSchema.setEdorType(aLPContSchema.getEdorType());
        tLPUWErrorSchema.setGrpContNo(aLPContSchema.getGrpContNo());
        tLPUWErrorSchema.setContNo(aLPContSchema.getContNo());
        tLPUWErrorSchema.setProposalContNo(aLPContSchema.getProposalContNo());
        tLPUWErrorSchema.setPolNo(BQ.FILLDATA);
        tLPUWErrorSchema.setProposalNo(BQ.FILLDATA);
        tLPUWErrorSchema.setSerialNo("1"); //对自核来说SerialNo没有意义
        tLPUWErrorSchema.setInsuredNo(aLPContSchema.getInsuredNo());
        tLPUWErrorSchema.setInsuredName(aLPContSchema.getInsuredName());
        tLPUWErrorSchema.setAppntNo(aLPContSchema.getAppntNo());
        tLPUWErrorSchema.setAppntName(aLPContSchema.getAppntName());
        tLPUWErrorSchema.setManageCom(mGlobalInput.ManageCom);
        tLPUWErrorSchema.setUWError(error);
        tLPUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
        tLPUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
        tLPUWErrorSchema.setUWPassFlag(BQ.UWFLAG_FAIL); //核保未通过
        mMap.put(tLPUWErrorSchema, "DELETE&INSERT");
    }

    /**
     * 记录自核错误信息
     * @param error String
     * @param aLPPolSchema LPPolSchema
     */
    private void setUWError(String error, LPPolSchema aLPPolSchema)
    {
        LPUWErrorSchema tLPUWErrorSchema = new LPUWErrorSchema();
        tLPUWErrorSchema.setEdorNo(aLPPolSchema.getEdorNo());
        tLPUWErrorSchema.setEdorType(aLPPolSchema.getEdorType());
        tLPUWErrorSchema.setGrpContNo(aLPPolSchema.getGrpContNo());
        tLPUWErrorSchema.setContNo(aLPPolSchema.getContNo());
        tLPUWErrorSchema.setProposalContNo(aLPPolSchema.getProposalNo());
        tLPUWErrorSchema.setPolNo(aLPPolSchema.getPolNo());
        tLPUWErrorSchema.setProposalNo(aLPPolSchema.getPolNo());
        tLPUWErrorSchema.setSerialNo("1"); //对自核来说SerialNo没有意义
        tLPUWErrorSchema.setInsuredNo(aLPPolSchema.getInsuredNo());
        tLPUWErrorSchema.setInsuredName(aLPPolSchema.getInsuredName());
        tLPUWErrorSchema.setAppntNo(aLPPolSchema.getAppntNo());
        tLPUWErrorSchema.setAppntName(aLPPolSchema.getAppntName());
        tLPUWErrorSchema.setManageCom(mGlobalInput.ManageCom);
        tLPUWErrorSchema.setUWError(error);
        tLPUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
        tLPUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
        tLPUWErrorSchema.setUWPassFlag(BQ.UWFLAG_FAIL); //核保未通过
        mMap.put(tLPUWErrorSchema, "DELETE&INSERT");
    }

    /**
     * 设置核保状态
     */
    private void setUWState(boolean changeEdorState)
    {
        //更新App表
        String sql = "update LPEdorApp " +
                "set UWState = '" + mUWState + "' ";
        if (changeEdorState == true)
        {
            sql += ", EdorState = '" + BQ.EDORSTATE_SENDUW + "' ";
        }
        sql += "where EdorAcceptNo = '" + mEdorNo + "'";
        mMap.put(sql, "UPDATE");
        //更新Main表
        sql = "update LPEdorMain " +
                "set UWState = '" + mUWState + "' ";
        if (changeEdorState == true)
        {
            sql += ", EdorState = '" + BQ.EDORSTATE_SENDUW + "' ";
        }
        sql += "where EdorNo = '" + mEdorNo + "'";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 创建工作流，向工作流表中插数据，送人工核保
     */
    private boolean createWorkflow()
    {
        LPEdorMainSchema tLPEdorMainSchema = getEodrMain();
        LCContSchema tLCContSchema = CommonBL.getLCCont(tLPEdorMainSchema.getContNo());
        if (tLCContSchema == null)
        {
            mErrors.addOneError("未找到LPEdorMain表的合同信息！");
            return false;
        }
        LGWorkSchema tLGWorkSchema = getWorkInfo();

        String missionId = PubFun1.CreateMaxNo("MissionId", 20);
        LWMissionSchema tLWMissionSchema = new LWMissionSchema();
        tLWMissionSchema.setMissionID(missionId);
        tLWMissionSchema.setSubMissionID("1");
        tLWMissionSchema.setProcessID("0000000003");
        tLWMissionSchema.setActivityID("0000001180");
        tLWMissionSchema.setActivityStatus("1");
        tLWMissionSchema.setMissionProp1(tLPEdorMainSchema.getEdorNo());
        tLWMissionSchema.setMissionProp2(tLPEdorMainSchema.getContNo());
        tLWMissionSchema.setMissionProp3(tLGWorkSchema.getCustomerNo());
        tLWMissionSchema.setMissionProp4(
                CommonBL.getAppntName(tLGWorkSchema.getCustomerNo()));
        tLWMissionSchema.setMissionProp5(
                CommonBL.getUserName(tLGWorkSchema.getAcceptorNo()));
        tLWMissionSchema.setMissionProp6(tLPEdorMainSchema.getEdorAppDate());
        tLWMissionSchema.setMissionProp7(
                CommonBL.getUserName(tLPEdorMainSchema.getOperator()));
        tLWMissionSchema.setMissionProp8(mCurrentDate);
        tLWMissionSchema.setMissionProp9(mCurrentTime);
        tLWMissionSchema.setMissionProp10(tLCContSchema.getManageCom());
        tLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
        tLWMissionSchema.setLastOperator(mGlobalInput.Operator);
        tLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
        tLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
        tLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
        tLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLWMissionSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 生成作业历史
     * @return boolean
     */
    private void createWorkTrace()
    {
        String sql = "select max(int(NodeNo)) from LGTraceNodeOp " +
                "where WorkNo = '" + mEdorNo + "'";
        String nodeNo = (new ExeSQL()).getOneValue(sql);
        sql = "select max(int(OperatorNo)) + 1 from LGTraceNodeOp " +
                "where WorkNo = '" + mEdorNo + "'";
        String operatorNo = (new ExeSQL()).getOneValue(sql);

        //生成具体历史信息
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
        tLGTraceNodeOpSchema.setWorkNo(mEdorNo);
        tLGTraceNodeOpSchema.setNodeNo(nodeNo);
        tLGTraceNodeOpSchema.setOperatorNo(operatorNo);
        tLGTraceNodeOpSchema.setOperatorType("5"); //人工核保
        tLGTraceNodeOpSchema.setRemark("自动批注：送人工核保");
        tLGTraceNodeOpSchema.setFinishDate(mCurrentDate);
        tLGTraceNodeOpSchema.setFinishTime(mCurrentTime);
        tLGTraceNodeOpSchema.setOperator(mGlobalInput.Operator);
        tLGTraceNodeOpSchema.setMakeDate(mCurrentDate);
        tLGTraceNodeOpSchema.setMakeTime(mCurrentTime);
        tLGTraceNodeOpSchema.setModifyDate(mCurrentDate);
        tLGTraceNodeOpSchema.setModifyTime(mCurrentTime);
        mMap.put(tLGTraceNodeOpSchema, "DELETE&INSERT");
    }

    private boolean creatMFUWError(){
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i ++)
        {
        	setUWError("免息复效送人工核保！", tLPPolSet.get(i));
        }
        mUwErrorList.add("免息复效送人工核保！");
        return true;
    }
    
    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
