package com.sinosoft.lis.bq;

import com.sinosoft.lis.bq.mm.face.ICommonMM;
import com.sinosoft.lis.bq.mm.face.ISendEmail;
import com.sinosoft.lis.bq.mm.face.ISendMessage;
import com.sinosoft.lis.db.LIWaitSendMessageDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LIServiceRecordSchema;
import com.sinosoft.lis.schema.LIWaitSendMessageSchema;
import com.sinosoft.lis.vschema.LIWaitSendMessageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class CommonMMImpl implements ICommonMM {

	private ISendEmail sendEmail = new SendEmailImpl() ;
	private ISendMessage sendMessage = new SendMessageImpl() ;
	private MMap map = null;
    
	private VData mInputData ;
	public CErrors mErrors = new CErrors();
	//未初始化
	private LIWaitSendMessageSchema waitSchema ;
	private LIWaitSendMessageSet waitSet ;
	public boolean sendMM(GlobalInput cGlobalInput) { 
		try{
			//查询待发短信的记录
			queryWaitMessage();
			for(int i=0; i<waitSet.size();i++){
				waitSchema = waitSet.get(i+1) ;
				//发送邮件
				if(waitSchema.getEmail() != null && waitSchema.getEmail() !=""){
				    boolean mail= sendEmail.sendEmail(waitSchema.getEmail(), waitSchema.getContent(), cGlobalInput) ;
				}
				//发送短信
				if(waitSchema.getMobile() != null && waitSchema.getMobile() !=""){
				    boolean message = sendMessage.sendMessage(waitSchema.getMobile(), waitSchema.getContent()) ;
				}
				//备份数据
				this.InsertServiceRecordInf(waitSchema) ;
				//删除数据
				this.DeleteWaitSendInf(waitSchema) ;
			}
		}catch(Exception e){
			e.printStackTrace() ;
			return false ;
		}
		return true ;
	}
	public boolean sendMMSoon(GlobalInput globalInput,LIWaitSendMessageSchema soonSchema) {
		try{
		    //发送邮件
			if(soonSchema.getEmail() != null && soonSchema.getEmail() !=""){
			    boolean mail= sendEmail.sendEmail(soonSchema.getEmail(), soonSchema.getContent(), globalInput) ;
			}
			//发送短信
			if(soonSchema.getMobile() != null && soonSchema.getMobile() !=""){
			    boolean message = sendMessage.sendMessage(soonSchema.getMobile(), soonSchema.getContent()) ;
			}
			//备份数据
			this.InsertServiceRecordInf(soonSchema) ;
			
		}catch(Exception e){
			e.printStackTrace() ;
			return false ;
		}
		return true;
	}
	/**
	 * 查询待发记录表中的记录
	 * 并且发送信息
	 */
	public boolean queryWaitMessage(){
		LIWaitSendMessageDB waitDB = new LIWaitSendMessageDB() ;
		try{
			waitSet = waitDB.executeQuery("select * from LIWaitSendMessage") ;
		}catch(Exception e){
			e.printStackTrace() ;
		}
		return true ;
	}
	/**
	 * 将待发信息表中的数据转存到
	 * 已经发送信息的记录中
	 * @param waitSchema
	 * @return
	 */
	private boolean InsertServiceRecordInf(LIWaitSendMessageSchema waitSchema){
		
		LIServiceRecordSchema record = new LIServiceRecordSchema() ;
		//导入数据
		record.setBussNo(waitSchema.getBussNo()) ;
		//not null
		record.setContent(waitSchema.getContent()) ;
		//not null
		record.setCustomerNo(waitSchema.getCustomerNo()) ;
		record.setEmail(waitSchema.getEmail()) ;
		record.setFunctionCode(waitSchema.getFunctionCode()) ;
		//not null
		record.setMakeDate(waitSchema.getMakeDate()) ;
		//not null
		record.setMakeTime(waitSchema.getMakeTime()) ;
		//not null
		record.setMessageId(waitSchema.getMessageId()) ;
		//record.setMessageId("00000001") ;
		record.setMobile(waitSchema.getMobile()) ;
		record.setModelCode(waitSchema.getModelCode()) ;
		//not null
		record.setModifyDate(waitSchema.getModifyDate()) ;
		//not null
		record.setModifyTime(waitSchema.getModifyTime()) ;
		//not null
		record.setName(waitSchema.getName()) ;
		//not null
		record.setOperator(waitSchema.getOperator()) ;
		//not null
		record.setSendDate(waitSchema.getSendDate()) ;
		//not null
		record.setSendTime(waitSchema.getSendTime()) ;
		record.setSendTimes(waitSchema.getSendTimes()) ;
		//设置实际发送时间和日期
		record.setRealSendDate(PubFun.getCurrentDate());
		record.setRealSendTime(PubFun.getCurrentTime()) ;
		
		PubSubmit tPubSubmit =new PubSubmit() ;
		map = new MMap();
		map.put(record, "INSERT");
		mInputData = new VData();
		mInputData.add(map) ;
		if (!tPubSubmit.submitData(mInputData, "INSERT")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "CommonMMImpl";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
		return true ;
		
	}
	//删除待发信息表的内容
	private boolean DeleteWaitSendInf(LIWaitSendMessageSchema waitSchema){
		PubSubmit tPubSubmit =new PubSubmit() ;
		map = new MMap();
		map.put(waitSchema,"DELETE");
		mInputData = new VData();
		mInputData.add(map) ;
		if (!tPubSubmit.submitData(mInputData,"DELETE")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "CommonMMImpl";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
		return true ;
		
	}
	public boolean sendMMLate(GlobalInput globalInput,
			LIWaitSendMessageSchema lateSchema) {
		MMap map1 = new MMap();
		PubSubmit tPubSubmit =new PubSubmit() ;
		
		//System.out.println("~~~~~~~~~~~~~");
		map1.put(lateSchema, "INSERT");
		mInputData = new VData();
		mInputData.add(map1) ;
		if (!tPubSubmit.submitData(mInputData, "INSERT")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "CommonMMImpl";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
		return true ;
	}
	public static void main(String[] args){
		GlobalInput mG = new GlobalInput();
		mG.Operator="ccc" ;
		mG.ManageCom="86" ;
		CommonMMImpl common = new CommonMMImpl() ;
		common.sendMM(mG) ;
	}
	

}
