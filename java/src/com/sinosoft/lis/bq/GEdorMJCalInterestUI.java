package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 计算账户利息
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GEdorMJCalInterestUI
{
    /**错误的容器*/
    public CErrors mErrors = null;
    /**业务处理类*/
    private GEdorMJCalInterestBL mGEdorMJCalInterestBL = null;

    public GEdorMJCalInterestUI()
    {
    }

    /**
     * 公共的数据提交方法
     * @param tVData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData tVData, String operate)
    {
        mGEdorMJCalInterestBL = new GEdorMJCalInterestBL();
        if(!mGEdorMJCalInterestBL.submitData(tVData, operate))
        {
            mErrors = mGEdorMJCalInterestBL.mErrors;
            return false;
        }

        return true;
    }

    /**
     * 得到利息
     * @return VData
     */
    public double getInterest()
    {
        return mGEdorMJCalInterestBL.getInterest();
    }
}
