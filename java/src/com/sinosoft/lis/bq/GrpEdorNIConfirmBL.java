package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @ReWrite: ZhangRong
 * @author rewrite by QiuYang 2005
 * @version 1.0
 */

public class GrpEdorNIConfirmBL implements EdorConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;

    private String mEdorValiDate = null;

    private String mPayToDate = null;

    private LJAPaySchema mLJAPaySchema = null;

    /** 新契约签单类 */
    private LCContSignBL mLCContSignBL = null;

    /** 用Set来获得当前保险计划下的团体险种号 */
    private Set mGrpPolNoSet = new HashSet();

    private LJSPaySchema mLJSPaySchema = null;

    private String mActuGetNo = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    private LCContSet mLCContSet = new LCContSet();

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);
        System.out.println("");
        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mLJAPaySchema = (LJAPaySchema) cInputData.
                getObjectByObjectName("LJAPaySchema", 0);
        mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
        mEdorType = mLPGrpEdorItemSchema.getEdorType();
        mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
        mEdorValiDate = mLPGrpEdorItemSchema.getEdorValiDate();
        mLJSPaySchema = CommonBL.getLJSPay(mEdorNo, BQ.NOTICETYPE_G);
    }

    /**
     * 加入到保全结案日志表数据
     * @param
     * @return boolean
     */
    private boolean dealUrgeLog(String pmDealState, String pmOpreat) {
        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                LCUrgeVerifyLogSchema();
        //加到保全结案日志表
        tLCUrgeVerifyLogSchema.setSerialNo(mEdorNo);
        tLCUrgeVerifyLogSchema.setRiskFlag("2");//用2这个状态标志增人保全确认中,不能再操作
        tLCUrgeVerifyLogSchema.setEndDate(mCurrentDate);

        tLCUrgeVerifyLogSchema.setOperateType("3"); //1：续期催收操作,2：续期核销操作，3：保全结案操作
        tLCUrgeVerifyLogSchema.setOperateFlag("3"); //1：个案操作,2：批次操作  3：保全团单结案操作
        tLCUrgeVerifyLogSchema.setOperator(mGlobalInput.Operator);
        tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成
        if (pmOpreat.equals("INSERT")) {
        	LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mEdorNo);
            tLCUrgeVerifyLogDB.setOperateType("3");
            tLCUrgeVerifyLogDB.setOperateFlag("3");
            tLCUrgeVerifyLogDB.setRiskFlag("2");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
            if (!(tLCUrgeVerifyLogSet == null) &&
                tLCUrgeVerifyLogSet.size() > 0) {
            	return false;
            }
            tLCUrgeVerifyLogSchema.setMakeDate(mCurrentDate);
            tLCUrgeVerifyLogSchema.setMakeTime(mCurrentTime);
            tLCUrgeVerifyLogSchema.setModifyDate(mCurrentDate);
            tLCUrgeVerifyLogSchema.setModifyTime(mCurrentTime);
            tLCUrgeVerifyLogSchema.setErrReason("1");
        }
        else if(pmOpreat.equals("UPDATE"))
        {
            LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mEdorNo);
            tLCUrgeVerifyLogDB.setOperateType("3");
            tLCUrgeVerifyLogDB.setOperateFlag("3");
            tLCUrgeVerifyLogDB.setRiskFlag("2");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
            if (!(tLCUrgeVerifyLogSet == null) &&
                tLCUrgeVerifyLogSet.size() > 0) {
                tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                tLCUrgeVerifyLogSchema.setDealState(pmDealState);
                tLCUrgeVerifyLogSchema.setErrReason(String.valueOf(Integer.parseInt(tLCUrgeVerifyLogSchema.getErrReason())+1));
            }else{
                return false;
            }
        }
        
        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            return false;
        }
        return true;
    }	
    
    
    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
/*    	modify by xp 20140224  增人保全确认分步提交,若中间中断,则不允许撤销等操作
 * 		修改开始----------------------------------------------------
 */
    	ExeSQL 	tExeSQL  = new ExeSQL();
     	String time = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
    	String insertSQL = "INSERT INTO ldtimetest VALUES ('NITIME',current time,'"+time+"','团单:"+mGrpContNo+"开始签单','1','1') ";
    	tExeSQL.execUpdateSQL(insertSQL);
       
    	
    	mLCContSet = getContSet();
    	for (int i = 1; i <= mLCContSet.size(); i++) {
//    		先分步提交插入状态表,确认保全确认点击后保全项目不会再被撤销,重复理算或者重复保全确认.
//    		第二次提交会报错,不过不会阻断程序流程
    		if(!dealUrgeLog("4", "INSERT")){//dealstate为4表示增人保全确认中
    			dealUrgeLog("4", "UPDATE");//第二次提交用ErrReason表示提交次数
    		}
//    		新建一个lccontset,一次提交一个分单
    		LCContSet tLCContSet=new LCContSet();
    		tLCContSet.add(mLCContSet.get(i));
//			保险起见,循环开始前先清一下全局变量
        	mMap = new MMap();
        	mLCContSignBL = new LCContSignBL();
//        	循环开始        	
            if (!signCont(tLCContSet)) //签单
            {
                return false;
            }
            
            setLJAPayPerson();
//          执行分步提交
            VData vdata=new VData();
            vdata.add(mMap);
            PubSubmit tPubSubmit = new PubSubmit();
            if (tPubSubmit.submitData(vdata, "") == false) {
                return false;
            }
		}
    	
//    	清空相关对象
    	mMap = new MMap();
    	mLCContSignBL = new LCContSignBL();

    	changeContNo();//ljagetendorse提交不涉及分单,将原有业务提交顺序变动到下面
/*    	modify by xp 20140224  增人保全确认分步提交,若中间中断,则不允许撤销等操作
 * 		修改结束----------------------------------------------------
 */             	
    String time1 = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
	String insertSQL1 = "INSERT INTO ldtimetest VALUES ('NITIME',current time,'"+time1+"','团单:"+mGrpContNo+"结束签单','2','1') ";
	tExeSQL.execUpdateSQL(insertSQL1);
        setLJAPayGrp();
        setEdorState();
        if (CommonBL.hasSpecialRisk(mGrpContNo) && !CommonBL.hasNotSpecialRisk(mGrpContNo))
        {
            if (!dealSpecialRisk())
            {
                return false;
            }
        }
        String time2 = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
    	String insertSQL2 = "INSERT INTO ldtimetest VALUES ('NITIME',current time,'"+time2+"','团单:"+mGrpContNo+"setLJAPayGrp','3','1') ";
    	tExeSQL.execUpdateSQL(insertSQL2);
    	
        updatePeoples();
        updatePayToDate();
        if(has280101Risk(mGrpContNo))
        {
            setPayToDate();
        }
        //2015-4-10 add by lzy 学平险汇交件保单
        if(isSchoolCon(mGrpContNo)){
        	transInsuredList();
        }
        
        String time3 = PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
    	String insertSQL3 = "INSERT INTO ldtimetest VALUES ('NITIME',current time,'"+time3+"','团单:"+mGrpContNo+"结束','3','1') ";
    	tExeSQL.execUpdateSQL(insertSQL3);
        return true;
    }

    /**
     * 得到个人保单信息
     * @return LCContSet
     */
    private LCContSet getContSet()
    {
        String sql = "select * from LCCont a " +
                "where exists(select * from LJSGetEndorse " +
                "  where GrpContNo = '" + mGrpContNo + "' " +
                "  and ContNo = a.ContNo " +
                "  and EndorseMentNo = '" + mEdorNo + "' " +
                "  and FeeOperationType = '" + mEdorType + "') " +
                "and AppFlag = '" + BQ.APPFLAG_EDOR + "' ";
        System.out.println(sql);
        return new LCContDB().executeQuery(sql);
    }

    /**
     * 调用新契约签单
     * @return boolean
     */
    private boolean signCont(LCContSet tLCContSet)
    {
        
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("EdorNo", mEdorNo);
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLCContSet);
        data.add(tTransferData);
        mLCContSignBL = new LCContSignBL();
        if (!mLCContSignBL.submitData(data, ""))
        {
            mErrors.copyAllErrors(mLCContSignBL.mErrors);
            return false;
        }
        VData retData = mLCContSignBL.getResult();
        MMap map = (MMap) retData.getObjectByObjectName("MMap", 0);
        mMap.add(map);
        return true;
    }

    /**
     * 设置保全状态为已结案
     */
    private void setEdorState()
    {
        //更新LPGrpEdorItem表中的保全状态
        String sql = "update LPGrpEdorItem " +
                "set EdorState = '" + BQ.EDORSTATE_CONFIRM + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 得到险种下的保费项信息
     * @param tLCPolSchema LCPolSchema
     * @return LCPremSet
     */
    private LCPremSet getLCPremSet(LCPolSchema tLCPolSchema)
    {
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(tLCPolSchema.getProposalNo());
        return tLCPremDB.query();
    }

    private double getEdorPolPrem(String polNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select GetMoney from LJSGetEndorse ")
                .append("where EndorsementNo = '").append(mEdorNo).append("' ")
                .append("and FeeOperationType = '").append(mEdorType).append(
                "' ")
                .append("and PolNo = '").append(polNo).append("'");
        String prem = (new ExeSQL()).getOneValue(sql.toString());
        if ((prem == null) || (prem.equals("")))
        {
            return 0;
        }
        return Double.parseDouble(prem);
    }

    /**
     * 设置setLJAPayPerson表
     * <p>因为INSERT语句执行在查SELECT语句之后，此时查不到PayNo，
     * 所以之后再用一个UPDATE把PayNo加进去</p>
     */
    private void setLJAPayPerson()
    {
        LCPolSet tLCPolSet = mLCContSignBL.getLCPolSet();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            LCPremSet tLCPremSet = getLCPremSet(tLCPolSchema);

            double edorPolPrem = getEdorPolPrem(tLCPolSchema.getProposalNo());
            LJAPayGrpSchema tLJAPayGrpSchema =
                    CommonBL.getPayToDate(tLCPolSchema.getGrpPolNo());
            if (tLJAPayGrpSchema == null){
                String sql = "select a.* from LJAPayGrp a,LCGRPpol b where PayCount = " +
        		" (select max(PayCount) from LJAPayGrp where GrpPolNo = a.GrpPolNo " +
        		" and PayType = 'ZC') and a.grpcontno = '" + mGrpContNo + "'" +
        				" and a.curpaytodate = b.paytodate and a.GrpPolNo = b.GrpPolNo" +
        				" and PayType = 'ZC' " +
                " with ur ";
        		LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
        		LJAPayGrpSet tLJAPayGrpSet = tLJAPayGrpDB.executeQuery(sql);
        		System.out.println(sql);
        		if (tLJAPayGrpSet.size() == 0)
        		{
        			mErrors.addOneError("未找到团体保单缴费信息！");
        		}
        		tLJAPayGrpSchema = tLJAPayGrpSet.get(1);
            }
            String lastPayToDate = tLCPolSchema.getCValiDate();
            this.mPayToDate = tLJAPayGrpSchema.getCurPayToDate();
            double sumPrem = 0;
            for (int j = 1; j <= tLCPremSet.size(); j++)
            {
                LCPremSchema tLCPremSchema = tLCPremSet.get(j);
                double subPrem;
                if (j < tLCPremSet.size())  //消除误差
                {
                    if (tLCPolSchema.getPrem() == 0)
                    {
                        subPrem = 0;
                    }
                    else
                    {
                        subPrem = CommonBL.carry(edorPolPrem *
                                tLCPremSchema.getPrem() / tLCPolSchema.getPrem());
                    }
                    sumPrem += subPrem;
                }
                else
                {
                    subPrem = CommonBL.carry(edorPolPrem - sumPrem);
                }
                LJAPayPersonSchema tLJAPayPersonSchema = new
                        LJAPayPersonSchema();
                tLJAPayPersonSchema.setPolNo(tLCPolSchema.getPolNo());
                tLJAPayPersonSchema.setPayCount(1);
                tLJAPayPersonSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                tLJAPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                tLJAPayPersonSchema.setContNo(tLCPolSchema.getContNo());
                tLJAPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
                tLJAPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
                tLJAPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
                tLJAPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
                tLJAPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
                tLJAPayPersonSchema.setAgentGroup(tLCPolSchema.
                        getAgentGroup());
                tLJAPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());
                System.out.println("PayNo" + mLJAPaySchema.getPayNo());
                tLJAPayPersonSchema.setPayNo(mLJAPaySchema.getPayNo());
                tLJAPayPersonSchema.setPayAimClass("1");
                tLJAPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
                tLJAPayPersonSchema.setPayPlanCode(tLCPremSchema.
                        getPayPlanCode());
                tLJAPayPersonSchema.setSumDuePayMoney(subPrem);
                tLJAPayPersonSchema.setSumActuPayMoney(subPrem);
                tLJAPayPersonSchema.setPayIntv(tLCPolSchema.getPayIntv());
                tLJAPayPersonSchema.setPayDate(mLJAPaySchema.getPayDate());
                tLJAPayPersonSchema.setPayType("ZC");
                tLJAPayPersonSchema.setEnterAccDate(mLJAPaySchema.
                        getEnterAccDate());
                tLJAPayPersonSchema.setConfDate(mLJAPaySchema.getConfDate());
                tLJAPayPersonSchema.setLastPayToDate(lastPayToDate);
                tLJAPayPersonSchema.setCurPayToDate(mPayToDate);
                tLJAPayPersonSchema.setApproveCode(tLCPolSchema.
                        getApproveCode());
                tLJAPayPersonSchema.setApproveDate(tLCPolSchema.
                        getApproveDate());
                tLJAPayPersonSchema.setApproveTime(tLCPolSchema.
                        getApproveTime());
                tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
                tLJAPayPersonSchema.setMakeDate(mCurrentDate);
                tLJAPayPersonSchema.setMakeTime(mCurrentTime);
                tLJAPayPersonSchema.setModifyDate(mCurrentDate);
                tLJAPayPersonSchema.setModifyTime(mCurrentTime);
                mMap.put(tLJAPayPersonSchema, "INSERT");
            }
            //updatePayInfo(tLCPolSchema.getPolNo());
            mGrpPolNoSet.add(tLCPolSchema.getGrpPolNo());
        }
    }

    /**
     * 更新LJAPayPerson表的交费信息，PolNo要用新契约换号后的polNo
     * @param polNo String
     */
    private void updatePayInfo(String polNo)
    {
        if (mLJSPaySchema != null)
        {
            String sql = "update LJAPayPerson " +
                    "set (PayNo, PayDate, EnterAccDate, ConfDate) " +
                    " = (select PayNo, PayDate, EnterAccDate, ConfDate " +
                    "    from LJAPay " +
                    "    where IncomeNo = '" + mEdorNo + "' " +
                    "    and (IncomeType = '" + BQ.NOTICETYPE_G +
                    "' or IncomeType = 'B')), " +
                    "  PolNo = (select PolNo from LCPol " +
                    "  where PolNo = '" + polNo + "') " +
                    "where PolNo = '" + polNo + "' ";
            mMap.put(sql, "UPDATE");
        }
//        else  //如果增减人同时做不产生费用则查LJAGetEndorse
//        {
//            sql = "update LJAPayPerson " +
//                    "set (PayNo, PayDate, EnterAccDate, ConfDate) " +
//                    " = (select distinct ActugetNo, '" + mCurrentDate + "', '" +
//                    mCurrentDate + "', '" + mCurrentDate + "' " +
//                    "    from  LJAGetEndorse " +
//                    "    where EndorseMentNo = '" + mEdorNo + "' " +
//                    "    and FeeOperationType = 'NI' ), " +
//                    "  PolNo = (select PolNo from LCPol " +
//                    "  where PolNo = '" + polNo + "') " +
//                    "where PolNo = '" + polNo + "' ";
//        }
    }

    /**
     * 得到团体险种信息
     * @return LPGrpPolSet
     */
    private LCGrpPolSet getLCGrpPolSet()
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        return tLCGrpPolDB.query();
    }

    /**
     * 根据团体险种号得到该险种下增人增加的保费
     * @param grpPolNo String
     * @return double
     */
    private double getGrpPolPrem(String grpPolNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select sum(GetMoney) from LJSGetEndorse ")
                .append("where EndorsementNo = '").append(mEdorNo).append("' ")
                .append("and FeeOperationType = '").append(mEdorType).append(
                "' ")
                .append("and GrpPolNo = '").append(grpPolNo).append("'");
        ExeSQL tExeSQL = new ExeSQL();
        return Double.parseDouble(tExeSQL.getOneValue(sql.toString()));
    }

    /**
     * 更新LJAPayGrp表的保费
     */
    private void setLJAPayGrp()
    {
        String sql = "select sum(GetMoney) from LJSGetEndorse " +
                "where EndorseMentNo = '" + mEdorNo + "' " +
                "and FeeOperationType = '" + mEdorType + "' " +
                "and GrpContNo = '" + mGrpContNo + "' ";
        String money = new ExeSQL().getOneValue(sql);
        if (money.equals(""))
        {
            return;
        }
        try
        {
            if (Double.parseDouble(money) <= 0)
            {
                return;
            }
        }
        catch (NumberFormatException ex)
        {
            ex.printStackTrace();
            return;
        }
        LCGrpPolSet tLCGrpPolSet = getLCGrpPolSet();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            String grpPolNo = tLCGrpPolSchema.getGrpPolNo();
            System.out.println("grpPolNo:" + grpPolNo);
            if (!mGrpPolNoSet.contains(grpPolNo)) //判断险种是否在当前保险计划下
            {
                continue;
            }
            double grpPolPrem = getGrpPolPrem(grpPolNo);
            System.out.println("grpPolPrem:" + grpPolPrem);
            LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
            tLJAPayGrpSchema.setEndorsementNo(mEdorNo);
            tLJAPayGrpSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
            tLJAPayGrpSchema.setPayCount("1"); //PayCount对保全无意义
            tLJAPayGrpSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
            tLJAPayGrpSchema.setManageCom(tLCGrpPolSchema.getManageCom());
            tLJAPayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
            tLJAPayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentType());
            tLJAPayGrpSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
            tLJAPayGrpSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
            tLJAPayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
            tLJAPayGrpSchema.setAppntNo(tLCGrpPolSchema.getCustomerNo());
            tLJAPayGrpSchema.setSumDuePayMoney(grpPolPrem);
            tLJAPayGrpSchema.setSumActuPayMoney(grpPolPrem);
            tLJAPayGrpSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            tLJAPayGrpSchema.setPayNo(mLJAPaySchema.getPayNo());
            tLJAPayGrpSchema.setPayType("ZC");
            tLJAPayGrpSchema.setPayDate(mLJAPaySchema.getPayDate());
            tLJAPayGrpSchema.setConfDate(mLJAPaySchema.getConfDate());
            tLJAPayGrpSchema.setEnterAccDate(mLJAPaySchema.getEnterAccDate());
            tLJAPayGrpSchema.setLastPayToDate(tLCGrpPolSchema.getCValiDate());
            tLJAPayGrpSchema.setCurPayToDate(tLCGrpPolSchema.getPaytoDate());
            tLJAPayGrpSchema.setApproveCode(tLCGrpPolSchema.getApproveCode());
            tLJAPayGrpSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
            tLJAPayGrpSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
            tLJAPayGrpSchema.setOperator(mGlobalInput.Operator);
            tLJAPayGrpSchema.setMakeDate(mCurrentDate);
            tLJAPayGrpSchema.setMakeTime(mCurrentTime);
            tLJAPayGrpSchema.setModifyDate(mCurrentDate);
            tLJAPayGrpSchema.setModifyTime(mCurrentTime);
            mMap.put(tLJAPayGrpSchema, "INSERT");
            //updateGrpPayInfo(grpPolNo);
        }
    }

    /**
     * 更新LJAPayGrp中的交费信息
     * @param grpPolNo String
     */
    private void updateGrpPayInfo(String grpPolNo)
    {
        if (mLJSPaySchema != null)
        {
            String sql = "update LJAPayGrp " +
                    "set (PayNo, PayDate, EnterAccDate, ConfDate) " +
                    " = (select PayNo, PayDate, EnterAccDate, ConfDate " +
                    "    from LJAPay " +
                    "    where IncomeNo = '" + mEdorNo + "' " +
                    "    and (IncomeType = '" + BQ.NOTICETYPE_G +
                    "' or IncomeType = 'B') ) " +
                    "where GrpPolNo = '" + grpPolNo + "' " +
                    "and PayNo = '0' " +
                    "and PayType = 'ZC'";
            mMap.put(sql, "UPDATE");
        }
    }

    /**
     * 处理特需医疗团体账户
     * @return boolean
     */
    private boolean dealSpecialRisk()
    {
        EdorItemSpecialData specialData =
                new EdorItemSpecialData(mLPGrpEdorItemSchema);
        specialData.query();
        String source = specialData.getEdorValue("Source");
        if ((source != null) && (source.equals("2"))) //从团体帐户转出资金建立个人账户
        {
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setGrpContNo(mGrpContNo);
            tLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
            tLCPolDB.setAccType(BQ.ACCTYPE_PUBLIC);
            LCPolSet tLCPolSet = tLCPolDB.query();
            if (tLCPolSet.size() == 0)
            {
                mErrors.addOneError("找不到特需医疗公共账户！");
                return false;
            }
            String polNo = tLCPolSet.get(1).getPolNo();
            String riskcode = tLCPolSet.get(1).getRiskCode();
            String insuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP,riskcode);
            LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
            tLCInsureAccDB.setPolNo(polNo);
            tLCInsureAccDB.setInsuAccNo(insuAccNo);
            LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
            if(!tLCInsureAccDB.getInfo())
            {
            	mErrors.addOneError("找不到特需医疗团体理赔账户！");
                return false;
            }
            tLCInsureAccSchema = tLCInsureAccDB.getSchema();
            double grpAccountMoney = tLCInsureAccSchema.getInsuAccBala();
            double chgPrem = mLPGrpEdorItemSchema.getChgPrem();
            double leftMoney = grpAccountMoney - chgPrem;
            double traceMoney = -chgPrem;
            if (leftMoney <= 0)
            {
                leftMoney = 0.0;
                traceMoney = -grpAccountMoney;
            }
            tLCInsureAccSchema.setLastAccBala(grpAccountMoney);
            tLCInsureAccSchema.setInsuAccBala(leftMoney);
            tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
            tLCInsureAccSchema.setModifyDate(mCurrentDate);
            tLCInsureAccSchema.setModifyTime(mCurrentTime);
            mMap.put(tLCInsureAccSchema, SysConst.UPDATE);

            Reflections ref = new Reflections();
            LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
            ref.transFields(tLCInsureAccTraceSchema, tLCInsureAccSchema);
            String serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
            tLCInsureAccTraceSchema.setSerialNo(serialNo);
            tLCInsureAccTraceSchema.setPayPlanCode(BQ.FILLDATA);
            tLCInsureAccTraceSchema.setOtherNo(mEdorNo);
            tLCInsureAccTraceSchema.setOtherType(BQ.NOTICETYPE_G);
            tLCInsureAccTraceSchema.setAccAscription("0");
            tLCInsureAccTraceSchema.setMoneyType("BF");
            tLCInsureAccTraceSchema.setMoney(traceMoney);
            tLCInsureAccTraceSchema.setUnitCount("0");
            tLCInsureAccTraceSchema.setPayDate(mEdorValiDate);
            tLCInsureAccTraceSchema.setState("0");
            tLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
            tLCInsureAccTraceSchema.setMakeDate(mCurrentDate);
            tLCInsureAccTraceSchema.setMakeTime(mCurrentTime);
            tLCInsureAccTraceSchema.setModifyDate(mCurrentDate);
            tLCInsureAccTraceSchema.setModifyTime(mCurrentTime);
            mMap.put(tLCInsureAccTraceSchema, SysConst.INSERT);

            StringBuffer sql = new StringBuffer(128);
            sql.append("update LCInsureAccClass set sumpay = ").append(tLCInsureAccSchema.getSumPay())
               .append(", LastAccBala = ").append(tLCInsureAccSchema.getLastAccBala())
               .append(", InsuAccBala = ").append(tLCInsureAccSchema.getInsuAccBala())
               .append(", Operator = '").append(mGlobalInput.Operator).append("', ModifyDate = '")
               .append(mCurrentDate).append("', ModifyTime = '").append(mCurrentTime)
               .append("' where polno = '").append(polNo).append("' and insuaccno = '").append(insuAccNo)
               .append("' ");
            System.out.println(sql.toString());
            mMap.put(sql.toString(), SysConst.UPDATE);
        }
        return true;
    }

    private void changeContNo()
    {
        String sql = "update LJAGetEndorse set ContNo = " +
                " (select a.ContNo from LCCont a " +
                "  where a.ProposalContNo =  LJAGetEndorse.ContNo) " +
                "where EndorsementNo = '" + mEdorNo + "' " +
                "and FeeOperationType = '" + mEdorType + "' " +
                		" and contno like '13%'";
        mMap.put(sql, "UPDATE");

        sql = "update LJAGetEndorse set PolNo = " +
                " (select a.PolNo from LCPol a " +
                "  where a.ProposalNo =  LJAGetEndorse.PolNo) " +
                "where EndorsementNo = '" + mEdorNo + "' " +
                "and FeeOperationType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 更新被保人数
     */
    private void updatePeoples()
    {
        UpdatePeoplesBL up = new UpdatePeoplesBL(mGrpContNo);
        MMap map = up.getSubmitData();
        mMap.add(map);
    }

    /**
     * 重设置生效日期，交至日期和签单日期
     */
    private void updatePayToDate()
    {
        String sql = "update LCCont " +
                " set PayToDate = '" + mPayToDate + "', " +
                " SignDate = '" + mCurrentDate + "', " +
                " ModifyDate = '" + mCurrentDate + "', " +
                " ModifyTime = '" + mCurrentTime + "', " +
                " Operator = '" + mGlobalInput.Operator + "' " +
                "where GrpContNo = '" + mGrpContNo + "' " +
                "and exists (select * from LJAGetEndorse " +
                "    where ContNo = LCCont.ContNo " +
                "    and GrpContNo = '" + mGrpContNo + "' " +
                "    and EndorseMentNo = '" + mEdorNo + "' " +
                "    and FeeOperationType = '" + mEdorType + "') ";
        mMap.put(sql, "UPDATE");
        sql = "update LCPol " +
                " set PayToDate = '" + mPayToDate + "', " +
                " SignDate = '" + mCurrentDate + "', " +
                " ModifyDate = '" + mCurrentDate + "', " +
                " ModifyTime = '" + mCurrentTime + "', " +
                " Operator = '" + mGlobalInput.Operator + "' " +
                "where GrpContNo = '" + mGrpContNo + "' " +
                "and exists (select * from LJAGetEndorse " +
                "    where PolNo = LCPol.PolNo " +
                "    and GrpContNo = '" + mGrpContNo + "' " +
                "    and EndorseMentNo = '" + mEdorNo + "' " +
                "    and FeeOperationType = '" + mEdorType + "') ";
        mMap.put(sql, "UPDATE");
        sql = "update LCDuty " +
                " set PayToDate = '" + mPayToDate + "', " +
                " ModifyDate = '" + mCurrentDate + "', " +
                " ModifyTime = '" + mCurrentTime + "', " +
                " Operator = '" + mGlobalInput.Operator + "' " +
                "where exists (select * from LJAGetEndorse " +
                "    where PolNo = LCDuty.PolNo " +
                "    and GrpContNo = '" + mGrpContNo + "' " +
                "    and EndorseMentNo = '" + mEdorNo + "' " +
                "    and FeeOperationType = '" + mEdorType + "') ";
        mMap.put(sql, "UPDATE");
        sql = "update LCPrem " +
                " set PayToDate = '" + mPayToDate + "', " +
                " ModifyDate = '" + mCurrentDate + "', " +
                " ModifyTime = '" + mCurrentTime + "', " +
                " Operator = '" + mGlobalInput.Operator + "' " +
                "where exists (select * from LJAGetEndorse " +
                "    where PolNo = LCPrem.PolNo " +
                "    and GrpContNo = '" + mGrpContNo + "' " +
                "    and EndorseMentNo = '" + mEdorNo + "' " +
                "    and FeeOperationType = '" + mEdorType + "') ";
        mMap.put(sql, "UPDATE");

//        for (int i = 1; i <= mLCContSet.size(); i++)
//        {
//            String sql = "update LCCont " +
//                    " set PayToDate = '" + mPayToDate + "', " +
//                    " SignDate = '" + mCurrentDate + "', " +
//                    " ModifyDate = '" + mCurrentDate + "', " +
//                    " ModifyTime = '" + mCurrentTime + "', " +
//                    " Operator = '" + mGlobalInput.Operator + "' " +
//                    "where GrpContNo = '" + mGrpContNo + "' " +
//                    "and ProposalContNo = '" + mLCContSet.get(i).getContNo() +
//                    "' ";
//            mMap.put(sql, "UPDATE");
//        }
//        LCPolSet tLCPolSet = mLCContSignBL.getLCPolSet();
//        for (int i = 1; i <= tLCPolSet.size(); i++)
//        {
//            String sql = "update LCPol " +
//                    " set PayToDate = '" + mPayToDate + "', " +
//                    " SignDate = '" + mCurrentDate + "', " +
//                    " ModifyDate = '" + mCurrentDate + "', " +
//                    " ModifyTime = '" + mCurrentTime + "', " +
//                    " Operator = '" + mGlobalInput.Operator + "' " +
//                    "where GrpContNo = '" + mGrpContNo + "' " +
//                    "and PolNo = '" + tLCPolSet.get(i).getPolNo() +
//                    "' ";
//            mMap.put(sql, "UPDATE");
//
//            String[] tables =
//                    {"LCDuty", "LCPrem"};
//            for (int j = 0; j < tables.length; j++)
//            {
//                sql = "update " + tables[j] +
//                        " set PayToDate = '" + mPayToDate + "', " +
//                        " ModifyDate = '" + mCurrentDate + "', " +
//                        " ModifyTime = '" + mCurrentTime + "', " +
//                        " Operator = '" + mGlobalInput.Operator + "' " +
//                        "where PolNo = '" + tLCPolSet.get(i).getPolNo() + "' ";
//                mMap.put(sql, "UPDATE");
//            }
//        }
    }
    /**
     *
     * @param grpContNo String
     * @return boolean
     */
    private static boolean has280101Risk(String grpContNo)
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LCGrpPolSchema  tLCGrpPolSchema = tLCGrpPolSet.get(i).getSchema();
            if(tLCGrpPolSchema.getRiskCode().equals("280101"))
            {
                return true;
            }
        }
        return false;
    }

    private void setPayToDate()
    {

//        String LCPremUpdate =
//                " update LCPrem set paytodate = payenddate  where grpcontno='" +
//                mGrpContNo + "' and payintv = 0 and riskcode ='280101'";
        String LCPremUpdate = " update LCPrem a set paytodate = payenddate,prem = standprem , sumprem =standprem where contno in (select contno from lcpol where grpcontno ='" +
                              mGrpContNo + "' and riskcode ='280101' ) and payintv = 0  ";
        String LCDutyUpdate = " update lcduty a set paytodate = payenddate,prem = standprem , sumprem =standprem where contno in (select contno from lcpol where grpcontno ='" +
                              mGrpContNo + "'  and riskcode ='280101') and payintv = 0 ";
        String LCContUdpdate = " update lccont a set paytodate = cinvalidate,payintv = 0,  prem = (select sum(standprem)  from lcpol where a.contno = contno) " 
        					 + " , sumprem = (select sum(standprem) from lcpol where a.contno = contno) "
        		             +" where contno in (select contno from lcpol where grpcontno ='" +
                               mGrpContNo + "' and payintv = 0 and riskcode ='280101')";
        String LCPolUpdate =" update LCPol a set paytodate = payenddate ,prem = standprem , sumprem =standprem  where grpcontno='" +
                            mGrpContNo + "' and payintv = 0 and riskcode ='280101'";

        String LJAPersonUpdate = " update ljapayperson a set curpaytodate = (select cinvalidate from lccont where a.contno = contno) where grpcontno ='" +
                                 mGrpContNo + "' and riskcode ='280101' ";
        String LJAGrpUudate = " update ljapaygrp a set curpaytodate =(select max(curpaytodate) from ljapayperson where a.payno= payno),payintv = 0 where grpcontno='" +
                              mGrpContNo + "' and endorsementno ='" + mEdorNo + "'";

        mMap.put(LCPolUpdate,SysConst.UPDATE);
        mMap.put(LCPremUpdate,SysConst.UPDATE);
        mMap.put(LCDutyUpdate,SysConst.UPDATE);
        mMap.put(LCContUdpdate,SysConst.UPDATE);
        mMap.put(LJAPersonUpdate,SysConst.UPDATE);
        mMap.put(LJAGrpUudate,SysConst.UPDATE);
    }
    /**
     * 因为汇交件的投保人信息都存储在LCInsuredList中，
     * 在确认时将数据备份至B表，以便以后查询或变更投保人信息
     */
    private void transInsuredList(){
    	LCInsuredListDB tLCInsuredListDB=new LCInsuredListDB();
    	LCInsuredListSet tLCInsuredListSet=null;
    	LBInsuredListSet tLBInsuredListSet=new LBInsuredListSet();
    	String querySQL="select * from LCInsuredlist where grpcontno='"+mGrpContNo+"' and edorno='"+mEdorNo+"' ";
    	String delInsured="delete from LBInsuredlist where grpcontno='"+mGrpContNo+"' and edorno='"+mEdorNo+"' ";
    	tLCInsuredListSet=tLCInsuredListDB.executeQuery(querySQL);
    	String SerialNo = PubFun1.CreateMaxNo("SerialNo", 20);
    	Reflections tRef = new Reflections();
    	for(int i=1;i<=tLCInsuredListSet.size();i++){
    		LBInsuredListSchema tLBInsuredListSchema=new LBInsuredListSchema();
    		LCInsuredListSchema tLCInsuredListSchema=tLCInsuredListSet.get(i);
    		tRef.transFields(tLBInsuredListSchema, tLCInsuredListSchema);
    		tLBInsuredListSchema.setSerialNo(SerialNo);
    		tLBInsuredListSchema.setMakeDate(mCurrentDate);
    		tLBInsuredListSchema.setMakeTime(mCurrentTime);
    		tLBInsuredListSchema.setModifyDate(mCurrentDate);
    		tLBInsuredListSchema.setModifyTime(mCurrentTime);
    		tLBInsuredListSet.add(tLBInsuredListSchema);
    	}
    	mMap.put(delInsured, SysConst.DELETE);
    	mMap.put(tLBInsuredListSet, SysConst.INSERT);
    }
    
    /**
     * 校验保单属性，true:为学平险汇交件	false：非汇交件
     * @param tGrpContNo
     * @return
     */
    private boolean isSchoolCon(String tGrpContNo){
        String strSql = "select ContPrintType from LCGrpCont where GrpContNo ='"+tGrpContNo+"' and ContPrintType='5'";
        String flag= new ExeSQL().getOneValue(strSql);
        if(null!=flag && !"".equals(flag) && "5".equals(flag)){
        	System.out.println(mGrpContNo+"学平险汇交件保单增人-----处理投保人数据");
                return true;
        }else{
            System.out.println(mGrpContNo+"非汇交件增人不需要处理LCINSUREDLIST");
            return false;
        }
    }
    
    public static void main(String[] args) {
    	GrpEdorNIConfirmBL tGrpEdorNIConfirmBL=new GrpEdorNIConfirmBL();
    	tGrpEdorNIConfirmBL.mEdorNo="123456789";
    	tGrpEdorNIConfirmBL.mEdorType="NI";
    	tGrpEdorNIConfirmBL.mGrpContNo="00106245000005";
    	tGrpEdorNIConfirmBL.mGlobalInput=new GlobalInput();
    	tGrpEdorNIConfirmBL.mGlobalInput.Operator="001";
    	tGrpEdorNIConfirmBL.mGlobalInput.ManageCom="86";
		if(!tGrpEdorNIConfirmBL.dealUrgeLog("4", "INSERT")){//dealstate为4表示增人保全确认中
			tGrpEdorNIConfirmBL.dealUrgeLog("4", "UPDATE");//第二次提交用ErrReason表示提交次数
		}
//    	tGrpEdorNIConfirmBL.mLCContSet = tGrpEdorNIConfirmBL.getContSet();
//    	for (int i = 1; i <= tGrpEdorNIConfirmBL.mLCContSet.size(); i++) {
//    		LCContSchema tLCContSchema=new LCContSchema();
//    		LCContSet tLCContSet=new LCContSet();    		
//    		tLCContSchema.setSchema(tGrpEdorNIConfirmBL.mLCContSet.get(i));
//    		tLCContSet.add(tLCContSchema);
//    		TransferData tTransferData = new TransferData();
//            tTransferData.setNameAndValue("EdorNo", tGrpEdorNIConfirmBL.mEdorNo);
//            VData data = new VData();
//            data.add(tGrpEdorNIConfirmBL.mGlobalInput);
//            data.add(tLCContSet);
//            data.add(tTransferData);
//            LCContSignBL mLCContSignBL = new LCContSignBL();
//            if (!mLCContSignBL.submitData(data, ""))
//            {
//            	System.out.println("111");
////                mErrors.copyAllErrors(mLCContSignBL.mErrors);
////                return false;
//            }
//            VData retData = mLCContSignBL.getResult();
//            PubSubmit tPubSubmit = new PubSubmit();
//            if (!tPubSubmit.submitData(retData, ""))
//            {
//                System.out.println("2222");
//            }
//		}
	}

}
