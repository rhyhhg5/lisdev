package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.tb.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 业务系统给付申请功能部分
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Tjj
 * @version 1.0
 */
public class GPEdorHLDetailBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData;

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/**  */
	private Reflections tReflections = new Reflections();

	private LCInsureAccClassSet aLCInsureAccClassSet = new LCInsureAccClassSet();

	private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

	private LPGetSet mLPGetSet = new LPGetSet();

	private LPGetSchema mLPGetSchema = new LPGetSchema();

	private String mChangePercent = "";
	
    String mAgentCode = "";
    String mProductCode = "";
    String mAppntNo = "";
    String mBankCode = "";
    String mBankAccNo = "";
    String mAccName = "";
    String mBankInfo = "";
    String mProvince = "";
    String mCity = "";
    String mBnfInfo = ""; 
    String mReMark = "";
    String mPayDate = "";

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String currDate = PubFun.getCurrentDate();

	private String currTime = PubFun.getCurrentTime();

	private ExeSQL mExeSQL = new ExeSQL();

	private MMap map = new MMap();

	public GPEdorHLDetailBL() {
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public void setOperate(String cOperate) {
		this.mOperate = cOperate;
	}

	public String getOperate() {
		return this.mOperate;
	}

	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("   -----BL Begin!");
		// 将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		this.setOperate(cOperate);

		// 得到外部传入的数据,将数据备份到本类中
		getInputData(cInputData);

		// 数据校验操作（checkdata) --------------
		if (!checkData())
			return false;

		System.out.println("---operate:" + this.getOperate());

		// 数据查询业务处理
		if (!this.getOperate().equals("INSERT||MONEY")) {
			if (!queryData())
				return false;
		}

		if (this.getOperate().equals("INSERT||MONEY")) {
			if (!dealData())
				return false;

			if (!prepareData())
				return false;

			PubSubmit PubSubmitGA = new PubSubmit();
			if (!(PubSubmitGA.submitData(mResult, ""))) { // @@错误处理
				this.mErrors.copyAllErrors(PubSubmitGA.mErrors);
				return false;
			}
		}
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private void getInputData(VData cInputData) {
		mLPEdorItemSchema = (LPEdorItemSchema) cInputData
				.getObjectByObjectName("LPEdorItemSchema", 0);
		mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0));
		// 取得前台的值
		TransferData mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		mAgentCode = (String) mTransferData.getValueByName("AgentCode"); 
		mProductCode = (String) mTransferData.getValueByName("ProductCode");  
		mAppntNo = (String) mTransferData.getValueByName("AppntNo");  
		mBankCode = (String) mTransferData.getValueByName("BankCode");  
		mBankAccNo = (String) mTransferData.getValueByName("BankAccNo");  
		mAccName = (String) mTransferData.getValueByName("AccName");  
		mBankInfo = (String) mTransferData.getValueByName("BankInfo");  
		mProvince = (String) mTransferData.getValueByName("Province");  
		mCity = (String) mTransferData.getValueByName("City");  
		mBnfInfo = (String) mTransferData.getValueByName("BnfInfo");  
		mReMark = (String) mTransferData.getValueByName("ReMark");  
		mPayDate = (String) mTransferData.getValueByName("PayDate");  
	}

	/**
	 * 校验传入的数据的合法性 输出：如果发生错误则返回false,否则返回true
	 */
	private boolean checkData() {
		return true;
	}

	/**
	 * 得到变更后的给付责任
	 * 
	 * @param aLCGetSchema
	 * @param aLPGetSchema
	 * @return
	 */

	/**
	 * 查询业务处理
	 * 
	 * @return
	 */
	private boolean queryData() {
		return true;
	}

	/**
	 * 得到个人申请主表记录
	 * 
	 * @param aLPPolSchema
	 * @return
	 */
	private boolean dealData() {
		
		return true;
	}

	private boolean prepareData() {
		mResult.clear();
		map.put(mLPEdorItemSchema, "UPDATE");
		mResult.add(map);
		return true;
	}

	private static void main(String[] args) {
	}
}
