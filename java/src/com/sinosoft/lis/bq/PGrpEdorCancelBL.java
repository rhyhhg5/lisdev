package com.sinosoft.lis.bq;

import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.MMap;

/**
 * <p>Title:保全申请删除通用类</p>
 * <p>Description:删除保全项目通用类. </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: sinosoft</p>
 * @author Lanjun
 * @version 1.0
 */
public class PGrpEdorCancelBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate;
    private String mOperator;
    private String mManageCom;

    private String mEdorNo = null;
    private String mContNo = null;

    /*状态*/
    private String mEdorState;

    /** 撤销申请原因*/
    private String delReason;
    private String reasonCode;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema(); //新的表结构

    TransferData tTransferData= new  TransferData();

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    public PGrpEdorCancelBL()
    {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //将VData数据还原成业务需要的类
        if (this.getInputData() == false)
        {
            System.out.println("批改状态不可以");
            return false;

        }

        if(!checkData())
        {
            return false;
        }

        System.out.println("---getInputData successful---");

        if (this.dealData() == false)
        {
            return false;
        }

        System.out.println("---dealdata successful---");

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mResult, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        try
        {
            LPGrpEdorMainSchema tLPGrpEdorMainSchema
                    = (LPGrpEdorMainSchema) mInputData
                    .getObjectByObjectName("LPGrpEdorMainSchema", 0);
            if (tLPGrpEdorMainSchema != null)
            {
                mEdorNo = tLPGrpEdorMainSchema.getEdorNo();
                if (!checkMaxEdor(tLPGrpEdorMainSchema.getEdorNo(),
                        BQ.CONTTYPE_G,
                        tLPGrpEdorMainSchema.getGrpContNo()))
                {
                    return false;
                }
            }
            System.out.println("1");
            LPGrpEdorItemSchema tLPGrpEdorItemSchema
                    = (LPGrpEdorItemSchema) mInputData
                    .getObjectByObjectName("LPGrpEdorItemSchema", 0);
            if (tLPGrpEdorItemSchema != null)
            {
                mEdorNo = tLPGrpEdorItemSchema.getEdorNo();
                System.out.println("tLPGrpEdorItemSchema.getEdorNo():" +
                        mEdorNo);
                if (!checkMaxEdor(tLPGrpEdorItemSchema.getEdorNo(),
                        BQ.CONTTYPE_G,
                        tLPGrpEdorItemSchema.getGrpContNo()))
                {
                    return false;
                }
            }
            System.out.println("2");
            LPEdorMainSchema tLPEdorMainSchema
                    = (LPEdorMainSchema) mInputData
                    .getObjectByObjectName("LPEdorMainSchema", 0);
              System.out.println("3");
            if (tLPEdorMainSchema != null)
            {
                   System.out.println("4");
                mEdorNo = tLPEdorMainSchema.getEdorNo();
                if (!checkMaxEdor(tLPEdorMainSchema.getEdorNo(),
                        BQ.CONTTYPE_P,
                        tLPEdorMainSchema.getContNo()))
                {
                    return false;
                }
            }
            System.out.println("5");
            LPEdorItemSchema tLPEdorItemSchema
                    = (LPEdorItemSchema) mInputData
                    .getObjectByObjectName("LPEdorItemSchema", 0);
            if (tLPEdorItemSchema != null)
            {
                mEdorNo = tLPEdorItemSchema.getEdorNo();
                if (!checkMaxEdor(tLPEdorItemSchema.getEdorNo(),
                        BQ.CONTTYPE_P,
                        tLPEdorItemSchema.getContNo()))
                {
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return true;
    }


    /**
     * 将UI层传输来得数据根据业务还原成具体的类
     * @return boolean
     */
    private boolean getInputData()
    {
        //全局变量实例

        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));

        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        return true;
    }


    /**
     * 对业务数据进行加工
     * 对于新增的操作，这里需要有生成新合同号和新客户号的操作。
     * 撤消项目时一定要先重复理算来进行新数据回退，否者回有问题 qulq 2007-3-21
     * @return boolean
     */
    private boolean dealData()
    {
        String flag = "";
        flag = mOperate.substring(0,1);
        System.out.println("flag="+flag);
        if ("G".equals(flag))
        { //团单
          if ("G&EDORITEM".equals(mOperate))
          {
          	//重复理算
            GEdorReCalBL tGEdorReCalBL = new GEdorReCalBL(mEdorNo);
            MMap map = tGEdorReCalBL.getSubmitData();
            if (map == null)
            {
                mErrors.addOneError("重复理算失败！");
                return false;
            }
            else
            {
            	mMap.add(map);
            }
            
            GEdorItemCancelBL tGEdorItemCancelBL = new GEdorItemCancelBL();
            if (!tGEdorItemCancelBL.submitData(mInputData, "EDORITEM"))
            {
              this.mErrors.copyAllErrors(tGEdorItemCancelBL.mErrors);
              return false;
            }
            else
            {
              mMap.add(tGEdorItemCancelBL.getMap());
            }

          }
          else if("G&EDORMAIN".equals(mOperate))
          {
          	//重复理算 2007-3-21 qulq add
            GEdorReCalBL tGEdorReCalBL = new GEdorReCalBL(mEdorNo);
            MMap map = tGEdorReCalBL.getSubmitData();
            if (map == null)
            {
                mErrors.addOneError("重复理算失败！");
                return false;
            }
            else
            {
            	mMap.add(map);
            }
       
            GEdorMainCancelBL tGEdorMainCancelBL = new GEdorMainCancelBL();
            if (!tGEdorMainCancelBL.submitData(mInputData, "EDORMAIN"))
            {
              this.mErrors.copyAllErrors(tGEdorMainCancelBL.mErrors);
              return false;
            }
            else
            {
              mMap.add(tGEdorMainCancelBL.getMap());
            }

          }
        }
        else if("I".equals(flag)) //个单
        {
          //保全项目
          if("I&EDORITEM".equals(mOperate))
          {
            PEdorItemCancelBL tPEdorItemCancelBL = new PEdorItemCancelBL();
            if (!tPEdorItemCancelBL.submitData(mInputData, "EDORITEM")) {
              this.mErrors.copyAllErrors(tPEdorItemCancelBL.mErrors);
              return false;
            }
            else
            {
              mMap.add(tPEdorItemCancelBL.getMap());
            }
            //重复理算
            PEdorReCalBL tPEdorReCalBL = new PEdorReCalBL(mEdorNo);
            MMap map = tPEdorReCalBL.getSubmitData();
            if (map == null)
            {
                mErrors.addOneError("重复理算失败！");
                return false;
            }
            mMap.add(map);
            //催缴工单结案
            String reason = "待催缴工单" + mEdorNo + "撤销。";
            FinishPhoneHastenBL tFinishPhoneHastenBL =
                    new FinishPhoneHastenBL(mGlobalInput, mEdorNo,
                    reason);
            MMap phoneMap = tFinishPhoneHastenBL.getSubmitData();
            if (map == null)
            {
                mErrors.copyAllErrors(tFinishPhoneHastenBL.mErrors);
                return false;
            }
            mMap.add(phoneMap);
          }
          //保全批单
          else if("I&EDORMAIN".equals(mOperate))
          {
            PEdorMainCancelBL tPEdorMainCancelBL = new PEdorMainCancelBL();
            if (!tPEdorMainCancelBL.submitData(mInputData, "EDORMAIN"))
            {
              this.mErrors.copyAllErrors(tPEdorMainCancelBL.mErrors);
              return false;
            }
            else
            {
              mMap.add(tPEdorMainCancelBL.getMap());
            }
            System.out.println("mEdorNovvvv:" + mEdorNo);
            //重复理算
            PEdorReCalBL tPEdorReCalBL = new PEdorReCalBL(mEdorNo);
            MMap map = tPEdorReCalBL.getSubmitData();
            if (map == null)
            {
                mErrors.addOneError("重复理算失败！");
                return false;
            }
            mMap.add(map);
            //催缴工单结案
            String reason = "待催缴工单" + mEdorNo + "撤销。";
            FinishPhoneHastenBL tFinishPhoneHastenBL =
                    new FinishPhoneHastenBL(mGlobalInput, mEdorNo,
                    reason);
            MMap phoneMap = tFinishPhoneHastenBL.getSubmitData();
            if (map == null)
            {
                mErrors.copyAllErrors(tFinishPhoneHastenBL.mErrors);
                return false;
            }
            mMap.add(phoneMap);

          }
          //保全申请
          else if("I&EDORAPP".equals(mOperate))
          {
            PEdorAppCancelBL tPEdorAppCancelBL = new PEdorAppCancelBL();
            if(!tPEdorAppCancelBL.submitData(mInputData,"EDORAPP"))
            {
              this.mErrors.copyAllErrors(tPEdorAppCancelBL.mErrors);
              return false;
            }
            else
            {
              mMap.add(tPEdorAppCancelBL.getMap());
            }
          }
        }
        else //错误处理
        {
          CError tError = new CError();
          tError.moduleName = "PGrpEdorCancelBL";
          tError.functionName = "dealData";
          tError.errorMessage = "操作标志符非法,请确认传递的数据的完整性!";
          this.mErrors.addOneError(tError);
          System.out.println(tError);
          return false;
        }
        return true;
    }


    private boolean checkMaxEdor(String edorNo, String contType, String contNo)
    {
        System.out.println("mEdorNo:" + mEdorNo);
        if ((mEdorNo != null) && (!mEdorNo.equals("")))
        {
        	//LCUrgeVerifyLog
        	//select * from LCUrgeVerifyLog where serialno='20120412000072';
        	//2012-06-06 by 【OoO?】ytz
        //保全项目撤销及工单撤销增加保全确认锁，正在进行保全确认的工单及保全项目不允许撤销和删除（也就是说：上了保全确认所锁的工单不允许撤销）。
        	 String sqlLCUrgeVerifyLog = " select * from LCUrgeVerifyLog " +
             " where serialno = '" + mEdorNo + "' with ur" ; //不是银行转帐
     System.out.println("sql:" + sqlLCUrgeVerifyLog);
     LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
     LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.executeQuery(sqlLCUrgeVerifyLog);
     if (tLCUrgeVerifyLogSet.size() > 0)
     {
         CError tError = new CError();
         tError.moduleName = "PGrpEdorCancelBL";
         tError.functionName = "checkMaxEdor";
         tError.errorMessage = "已经点击保全确认，不能撤销工单！";
         this.mErrors.addOneError(tError);
         return false;
     }

        	
          //财务交费之后不能撤销
            String sql = "select a.* from LJTempFee a, LJTempFeeClass b  " +
                    "where a.TempFeeNo = b.TempFeeNo " +
                    "and a.OtherNo = '" + mEdorNo + "' " +
                    "and a.OtherNoType in ('3', '10') " +
                    "and b.PayMode <> '4'"; //不是银行转帐
            System.out.println("sql:" + sql);
            LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
            LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.executeQuery(sql);
            if (tLJTempFeeSet.size() > 0)
            {
                CError tError = new CError();
                tError.moduleName = "PGrpEdorCancelBL";
                tError.functionName = "checkMaxEdor";
                tError.errorMessage = "该保全已经财务交费，不能撤销！";
                this.mErrors.addOneError(tError);
                return false;
            }
            /*
             * 0.银行在途时也要干掉ljtempfee
             */
            //银行转账形式，财务交费之后不能撤销
            String sql_1 = "select a.* from LJTempFee a, LJTempFeeClass b  " +
                    "where a.TempFeeNo = b.TempFeeNo " +
                    "and a.OtherNo = '" + mEdorNo + "' " +
                    "and a.OtherNoType in ('3', '10') " +
                    "and b.PayMode = '4'" +
                    "and a.enteraccdate is not null with ur"; //不是银行转帐
            System.out.println("sql:" + sql_1);
            LJTempFeeDB ttLJTempFeeDB = new LJTempFeeDB();
            LJTempFeeSet ttLJTempFeeSet = ttLJTempFeeDB.executeQuery(sql_1);
            if (ttLJTempFeeSet.size() > 0)
            {
                CError tError = new CError();
                tError.moduleName = "PGrpEdorCancelBL";
                tError.functionName = "checkMaxEdor";
                tError.errorMessage = "该保全已经财务交费，不能撤销！";
                this.mErrors.addOneError(tError);
                return false;
            }
            /*
             * -------------------the end--------------------
             */
            //银行转帐在途不能撤销
            sql = "select b.* from LJTempFeeClass a, LJSPay b " +
                    "where a.TempFeeNo = b.GetNoticeNo " +
                    "and b.OtherNo = '" + mEdorNo + "' " +
                    "and b.OtherNoType in ('3', '10') " +
                    "and b.BankOnTheWayFlag = '1'";
            System.out.println("sql:" + sql);
            LJSPayDB tLJSPayDB = new LJSPayDB();
            LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql);
            if (tLJSPaySet.size() > 0)
            {
                CError tError = new CError();
                tError.moduleName = "PGrpEdorCancelBL";
                tError.functionName = "checkMaxEdor";
                tError.errorMessage = "银行转帐在途中，不能撤销保全！";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            //银行回盘了之后，也是不可以删除的
            
            String sql2B = " select * from lyreturnfrombankb a where " +
                    " polno = '" + mEdorNo + "' " +
                    " and exists ( select 1 from lpedoritem where edorno=a.polno)" +
                    " and banksuccflag='00' ";
            System.out.println("sql2B:" + sql2B);
            LYReturnFromBankBDB tLYReturnFromBankBDB = new LYReturnFromBankBDB();
            LYReturnFromBankBSet tLYReturnFromBankBSet = tLYReturnFromBankBDB.executeQuery(sql2B);
            if (tLYReturnFromBankBSet.size() > 0)
            {
                CError tError = new CError();
                tError.moduleName = "PGrpEdorCancelBL";
                tError.functionName = "checkMaxEdor";
                tError.errorMessage = "银行已回盘，不能撤销保全！";
                this.mErrors.addOneError(tError);
                return false;
            }
            
        }
        else
        {
        	this.mErrors.addOneError("保全工单号缺失，请重新操作");
            return false;
        }

        String sql = "  select Max(a.edorno) "
                     + "from tableName a "
                     + "where a.EdorState != '0' "
                     + "   and a.fieldName = '" + contNo + "' "
                     + "   and edorNo > '" + edorNo + "' ";
        if(contType.equals(BQ.CONTTYPE_G))
        {
            sql = sql.replaceAll("tableName", "LPGrpEdorMain")
                  .replaceAll("fieldName", "GrpContNo");
        }
        else
        {
            sql = sql.replaceAll("tableName", "LPEdorMain")
                  .replaceAll("fieldName", "ContNo");
        }
        System.out.println(sql);
        ExeSQL e = new ExeSQL();
        SSRS tSSRS = e.execSQL(sql);
        if(tSSRS.getMaxRow() > 0 && !tSSRS.GetText(1, 1).equals("null")
          && !tSSRS.GetText(1, 1).equals(""))
        {
            mErrors.addOneError("该批单不是最近一条，无法删除,最大的批单号为："
                                + tSSRS.GetText(1, 1));
            return false;
        }
        return true;
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData()
    {
        //记录当前操作员
        mResult.clear();
        mResult.add(mMap);
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "endor";

//        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
//        tLPEdorItemSchema.setEdorAcceptNo("20050920000042");
//        tLPEdorItemSchema.setEdorNo("20050920000042");
//        tLPEdorItemSchema.setContNo("00000003601");
//        tLPEdorItemSchema.setPolNo("000000");
//        tLPEdorItemSchema.setInsuredNo("000000");
//        //tLPEdorItemSchema.setGrpContNo("00000003601");
//        tLPEdorItemSchema.setEdorType("AD");
//        tLPEdorItemSchema.setEdorState("3");
//        tLPEdorItemSchema.setMakeDate("2005-3-26");
//        tLPEdorItemSchema.setMakeTime("10:23:15");

//        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
//        tLPGrpEdorItemSchema.setEdorAcceptNo("20061114000002");
//        tLPGrpEdorItemSchema.setEdorNo("20061114000002");
//        tLPGrpEdorItemSchema.setGrpContNo("000014914");

        LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
        tLPEdorAppSchema.setEdorAcceptNo("20070118000017");
        tLPEdorAppSchema.setEdorState("1");

        TransferData t = new TransferData();
        t.setNameAndValue("DelReason", "a");
        t.setNameAndValue("ReasonCode", "b");

        tVData.addElement(tGlobalInput);
//        tVData.addElement(tLPEdorItemSchema);
//        tVData.add(tLPGrpEdorItemSchema);
        tVData.add(tLPEdorAppSchema);
        tVData.add(t);

        PGrpEdorCancelBL tPGrpEdorCancelBL = new PGrpEdorCancelBL();
        tPGrpEdorCancelBL.submitData(tVData, "I&ITEM");
    }

}