package com.sinosoft.lis.bq;

	import java.util.ArrayList;
	import com.sinosoft.lis.db.LPAscriptionRuleFactoryDB;
	import com.sinosoft.lis.db.LPAscriptionRuleParamsDB;
	import com.sinosoft.lis.db.LMFactoryModeDB;
import com.sinosoft.lis.db.LPGrpEdorItemDB;
import com.sinosoft.lis.db.LPGrpEdorMainDB;
	import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
	import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
	import com.sinosoft.lis.schema.LPAscriptionRuleFactorySchema;
	import com.sinosoft.lis.schema.LPAscriptionRuleParamsSchema;
	import com.sinosoft.lis.schema.LMFactoryModeSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
	import com.sinosoft.lis.vschema.LPAscriptionRuleFactorySet;
	import com.sinosoft.lis.vschema.LPAscriptionRuleParamsSet;
	import com.sinosoft.lis.vschema.LMFactoryModeSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.lis.vschema.LPGrpEdorMainSet;
	import com.sinosoft.utility.CError;
	import com.sinosoft.utility.CErrors;
	import com.sinosoft.utility.ExeSQL;
	import com.sinosoft.utility.SSRS;
	import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;


	/**
	 * 保障计划要素数据准备类
	 * <p>Title: </p>
	 * <p>Description: 根据操作类型，进行数据校验、准备处理 </p>
	 * <p>Copyright: Copyright (c) 2004</p>
	 * <p>Company: SINOSOFT</p>
	 * @author ZHUXF
	 * @version 1.0
	 */
	public class GrpEdorGBDetailBL
	{
	    /** 错误处理类，每个需要错误处理的类中都放置该类 */
	    public CErrors mErrors = new CErrors();
	    private VData mResult = new VData();
	    private MMap mMap = new MMap();
	    private VData mResult1 = new VData();
	    /** 往后面传输数据的容器 */
	    private VData mInputData;


	    /** 全局数据 */
	    private GlobalInput mGlobalInput = new GlobalInput();


	    /** 数据操作字符串 */
	    private String mOperate;


	    /** 业务处理相关变量 */
	    private LPAscriptionRuleFactorySet mLPAscriptionRuleFactorySet = new
	            LPAscriptionRuleFactorySet();
	    private LPAscriptionRuleFactorySet mOldLPAscriptionRuleFactorySet = new
	            LPAscriptionRuleFactorySet();
	    private LPAscriptionRuleParamsSchema mLPAscriptionRuleParamsSchema = new
	            LPAscriptionRuleParamsSchema();
	    private LPAscriptionRuleParamsSet mLPAscriptionRuleParamsSet = new LPAscriptionRuleParamsSet();
	    private LPAscriptionRuleParamsSet mOldLPAscriptionRuleParamsSet = new LPAscriptionRuleParamsSet();
	    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
	    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	    private LPGrpEdorItemSchema mLPGrpEdorItemSchema_input = new
	            LPGrpEdorItemSchema();

	    private String mGrpContNo = "";
	    private String mRiskCode = "";
	    private String mFactoryType = "";
	    private String mFactoryCode = "";
	    private int mFactorySubCode = 0;


	    /** 时间信息*/
	    String mCurrentDate = PubFun.getCurrentDate(); //当前值
	    String mCurrentTime = PubFun.getCurrentTime();

	    public GrpEdorGBDetailBL()
	    {
	    }

	    public static void main(String[] args)
	    {
	    }


	    /**
	     * 传输数据的公共方法
	     * @param: cInputData 输入的数据
	     *         cOperate 数据操作
	     * @return:
	     */
	    public boolean submitData(VData cInputData, String cOperate)
	    {
	        //将操作数据拷贝到本类中
	        this.mOperate = cOperate;
	        //得到外部传入的数据,将数据备份到本类中
	        if (!getInputData(cInputData))
	        {
	            return false;
	        }
	        if(!checkData())
	        {
	            CError tError = new CError();
	            tError.moduleName = "LPAscriptionRuleFactoryBL";
	            tError.functionName = "submitData";
	            tError.errorMessage = "数据处理失败LPAscriptionRuleFactoryBL-->checkData!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }

	        //进行业务处理
	        if (!dealData())
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "LPAscriptionRuleFactoryBL";
	            tError.functionName = "submitData";
	            tError.errorMessage = "数据处理失败LPAscriptionRuleFactoryBL-->dealData!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        //准备往后台的数据
	        if (!prepareOutputData())
	        {
	            return false;
	        }
	        //准备往后台的数据
	        if (!prepareData())
	        {
	            return false;
	        }
	        PubSubmit tSubmit = new PubSubmit();

	        if (!tSubmit.submitData(mResult1, "")) //数据提交
	        {
	            // @@错误处理
	            this.mErrors.copyAllErrors(tSubmit.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "GrpEdorGBDetailBL";
	            tError.functionName = "submitData";
	            tError.errorMessage = "数据提交失败!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }

	        System.out.println("Start LPAscriptionRuleFactoryBL Submit...");
	        GEdorLPAscriptionRuleFactoryBLS tGEdorLPAscriptionRuleFactoryBLS = new GEdorLPAscriptionRuleFactoryBLS();
	        tGEdorLPAscriptionRuleFactoryBLS.submitData(mInputData, cOperate);
	        System.out.println("End LPAscriptionRuleFactoryBL Submit...");
	        //如果有需要处理的错误，则返回
	        if (tGEdorLPAscriptionRuleFactoryBLS.mErrors.needDealError())
	        {
	            // @@错误处理
	            this.mErrors.copyAllErrors(tGEdorLPAscriptionRuleFactoryBLS.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "LPAscriptionRuleFactoryBL";
	            tError.functionName = "submitDat";
	            tError.errorMessage = "数据提交失败!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        mInputData = null;
	        return true;
	    }

	    private boolean dealmain(){
	    	
	    	LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
	        tLPGrpEdorMainDB.setEdorNo(mLPGrpEdorItemSchema_input.getEdorNo());
	        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
	        if (tLPGrpEdorMainSet == null || tLPGrpEdorMainSet.size() != 1)
	        {
	            // @@错误处理
	            mErrors.copyAllErrors(tLPGrpEdorMainDB.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "GrpEdorGBDetailBL";
	            tError.functionName = "checkData";
	            tError.errorMessage = "无保全申请数据!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }

	        mLPGrpEdorMainSchema.setSchema(tLPGrpEdorMainSet.get(1));
	        mLPGrpEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
	        mLPGrpEdorMainSchema.setOperator(mGlobalInput.Operator);

	        if (!mLPGrpEdorMainSchema.getEdorState().trim().equals("1"))
	        {
	            // @@错误处理
	            mErrors.copyAllErrors(tLPGrpEdorMainDB.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "GrpEdorGBDetailBL";
	            tError.functionName = "checkData";
	            tError.errorMessage = "该保全已经申请确认不能修改!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }

	        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
	        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema_input.getEdorNo());
	        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema_input.getGrpContNo());
	        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema_input.getEdorType());
	        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
	        if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1)
	        {
	            // @@错误处理
	            mErrors.copyAllErrors(tLPGrpEdorItemDB.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "GrpEdorGBDetailBL";
	            tError.functionName = "checkData";
	            tError.errorMessage = "无保全申请数据!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        mLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
	        mLPGrpEdorItemSchema.setEdorState("1");
	        mLPGrpEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
	        mLPGrpEdorItemSchema.setOperator(mGlobalInput.Operator);
	        mLPGrpEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
	        mLPGrpEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
	      return true;
	    }

	    /**
	     * 根据前面的输入数据，进行BL逻辑处理
	     * 如果在处理过程中出错，则返回false,否则返回true
	     */
	    private boolean dealData()
	    {
	    	 dealmain();
	        boolean tReturn = true;
	        //新增处理
	        if (this.mOperate.compareTo("DELETE||MAIN") != 0)
	        {
	            //循环要素计算Sql集合，准备要素计算Sql中的计算子要素信息
	            for (int i = 1; i <= mLPAscriptionRuleFactorySet.size(); i++)
	            {
	                LPAscriptionRuleFactorySchema tLPAscriptionRuleFactorySchema = new
	                        LPAscriptionRuleFactorySchema();
	                tLPAscriptionRuleFactorySchema = mLPAscriptionRuleFactorySet.get(i);
	                if (prepareNewData(tLPAscriptionRuleFactorySchema, i) == false)
	                {
	                    return false;
	                }
	            }
	            //准备需要删除的数据
	            if (prepareOldData() == false)
	            {
	                return false;
	            }
	        }
	        //删除处理
	        if (this.mOperate.compareTo("DELETE||MAIN") == 0)
	        {
	            //准备需要删除的数据
	            if (prepareOldData() == false)
	            {
	                return false;
	            }
	        }
	        tReturn = true;
	        return tReturn;
	    }


	    /**
	     * 从输入数据中得到所有对象
	     * @param cInputData VData
	     * @return boolean
	     * 如果没有得到足够的业务数据对象，则返回false,否则返回true
	     */
	    private boolean getInputData(VData cInputData)
	    {
	        this.mLPAscriptionRuleFactorySet.set((LPAscriptionRuleFactorySet) cInputData.
	                                       getObjectByObjectName(
	                "LPAscriptionRuleFactorySet",
	                0));
	        this.mGlobalInput.setSchema((GlobalInput) cInputData.
	                                    getObjectByObjectName(
	                "GlobalInput", 0));
	        this.mGrpContNo = (String) cInputData.get(2);
	        
	        mLPGrpEdorItemSchema_input = (LPGrpEdorItemSchema) cInputData.
            getObjectByObjectName(
"LPGrpEdorItemSchema",
0);
	        return true;
	    }


	    /**
	     * 往BLS传送准备好的数据
	     * @return boolean
	     */
	    private boolean prepareOutputData()
	    {
	        try
	        {
	            this.mInputData = new VData();
	            this.mInputData.add(this.mGlobalInput);
	            this.mInputData.add(this.mLPAscriptionRuleFactorySet);//需要保存的缴费规则数据
	            this.mInputData.add(this.mLPAscriptionRuleParamsSet);//需要保存的缴费规则要素
	            this.mInputData.add(this.mOldLPAscriptionRuleFactorySet);//需要删除的旧有缴费规则数据
	            this.mInputData.add(this.mOldLPAscriptionRuleParamsSet);//需要删除的旧有缴费规则要素
	            System.out.println("--old factoryset size:"+mOldLPAscriptionRuleFactorySet.size());
	            System.out.println("--old paramsset size:"+mOldLPAscriptionRuleParamsSet.size());
	        }
	        catch (Exception ex)
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "LPAscriptionRuleFactoryBL";
	            tError.functionName = "prepareData";
	            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        return true;
	    }


	    /**
	     * 返回集
	     * @return VData
	     */
	    public VData getResult()
	    {
	        return this.mResult;
	    }


	    /**
	     * 逐条准备计算要素信息
	     * @param tLCFactorySchema LCFactorySchema
	     * @param tIndex int
	     * @return boolean
	     * 如果发生错误则返回false,否则返回true
	     */
	    private boolean prepareNewData(LPAscriptionRuleFactorySchema
	                                   tLPAscriptionRuleFactorySchema, int tIndex)
	    {
	        //基本录入信息校验
	        mRiskCode = tLPAscriptionRuleFactorySchema.getRiskCode(); //险种信息
	        mFactoryType = tLPAscriptionRuleFactorySchema.getFactoryType(); //要素类别
	        mFactoryCode = tLPAscriptionRuleFactorySchema.getFactoryCode(); //要素计算编码
	        mFactorySubCode = tLPAscriptionRuleFactorySchema.getFactorySubCode(); //要素计算编码小号
	        mGrpContNo = tLPAscriptionRuleFactorySchema.getGrpContNo();

	        //取要素描述信息
	        LMFactoryModeDB tLMFactoryModeDB = new LMFactoryModeDB();
	        LMFactoryModeSet tLMFactoryModeSet = new LMFactoryModeSet();

	        tLMFactoryModeDB.setRiskCode(mRiskCode);
	        tLMFactoryModeDB.setFactoryType(mFactoryType);
	        tLMFactoryModeDB.setFactoryCode(mFactoryCode);
	        tLMFactoryModeDB.setFactorySubCode(mFactorySubCode);
	        tLMFactoryModeSet = tLMFactoryModeDB.query();
	        //如果集合为空，或者集合数不唯一，则数据异常
	        if (tLMFactoryModeSet == null || tLMFactoryModeSet.size() != 1)
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "GrpHealthFactorySaveBL";
	            tError.functionName = "prepareHealth";
	            tError.errorMessage = "取计算编码为：" +
	                                  tLPAscriptionRuleFactorySchema.getFactoryCode() +
	                                  "的计算类型为：" +
	                                  tLPAscriptionRuleFactorySchema.getFactoryType() +
	                                  "的计算Sql失败!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        LMFactoryModeSchema tLMFactoryModeSchema = new LMFactoryModeSchema();
	        //取查询出的要素信息
	        tLMFactoryModeSchema = tLMFactoryModeSet.get(1);

//	    String tCalSql = tLMFactoryModeSchema.getCalSql();//sql描述部分，需要替换
	        int indexModeParams = tLMFactoryModeSchema.getParams().indexOf(","); //模板，位置
	        int indexRelaParams = tLPAscriptionRuleFactorySchema.getParams().indexOf(","); //实际数据，位置
	        String[] tParams = null;
	        String[] tRelParams = null;

	        String tNewCalSql = tLMFactoryModeSchema.getCalSql().trim();
	        //表示参数为一个的情况下
	        if (indexModeParams == -1 && indexRelaParams == -1)
	        {
	            tNewCalSql = StrTool.replace(tNewCalSql,
	                                         "?" + tLMFactoryModeSchema.getParams() +
	                                         "?",
	                                         tLPAscriptionRuleFactorySchema.getParams());
	        }
	        else

	        //参数为多个的情况下
	        if (indexModeParams != -1 && indexRelaParams != -1)
	        {
	            //根据，拆分字符串，返回数组
	            tParams = PubFun.split(tLMFactoryModeSchema.getParams(), ",");
	            tRelParams = PubFun.split(tLPAscriptionRuleFactorySchema.getParams(), ",");

	            if (tParams.length == tRelParams.length)
	            {
	                for (int i = 0; i < tParams.length; i++)
	                {
	                    tNewCalSql = StrTool.replace(tNewCalSql,
	                                                 "?" + tParams[i] + "?",
	                                                 tRelParams[i]);
	                   System.out.println();
	                }
	            }
	            else
	            {
	                // @@错误处理
	                CError tError = new CError();
	                tError.moduleName = "GrpHealthFactorySaveBL";
	                tError.functionName = "prepareHealth";
	                tError.errorMessage = "录入参数与计算编码为：" +
	                                      tLPAscriptionRuleFactorySchema.getFactoryCode() +
	                                      "的计算类型为：" +
	                                      tLPAscriptionRuleFactorySchema.getFactoryType() +
	                                      "的计算Sql描述中的参数个数不同!";
	                this.mErrors.addOneError(tError);
	                return false;

	            }

	        }
	        else
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "GrpHealthFactorySaveBL";
	            tError.functionName = "prepareHealth";
	            tError.errorMessage = "录入参数与计算编码为：" +
	                                  tLPAscriptionRuleFactorySchema.getFactoryCode() +
	                                  "的计算类型为：" +
	                                  tLPAscriptionRuleFactorySchema.getFactoryType() +
	                                  "的计算Sql描述中的参数个数不同!";
	            this.mErrors.addOneError(tError);
	            return false;

	        }
	        //准备计算要素信息数据
	        mLPAscriptionRuleFactorySet.get(tIndex).setCalSql(tNewCalSql);
	        mLPAscriptionRuleFactorySet.get(tIndex).setCalRemark(tLMFactoryModeSchema.
	                getCalRemark());
	        mLPAscriptionRuleFactorySet.get(tIndex).setInerSerialNo(String.valueOf(tIndex));
	        mLPAscriptionRuleFactorySet.get(tIndex).setOperator(mGlobalInput.Operator);
	        mLPAscriptionRuleFactorySet.get(tIndex).setMakeDate(mCurrentDate); //当前值
	        mLPAscriptionRuleFactorySet.get(tIndex).setMakeTime(mCurrentTime);
	        mLPAscriptionRuleFactorySet.get(tIndex).setModifyDate(mCurrentDate); //当前值
	        mLPAscriptionRuleFactorySet.get(tIndex).setModifyTime(mCurrentTime);

	        //如果数组不为空，表示参数有多个，对应的LCAscriptionRuleParam表数据有多条
	        if (tRelParams != null && tRelParams != null)
	        {
	            for (int i = 0; i < tRelParams.length; i++)
	            {
	                mLPAscriptionRuleParamsSchema = new LPAscriptionRuleParamsSchema();

	                //准备计算要素参数信息数据
	                mLPAscriptionRuleParamsSchema.setEdorNo(tLPAscriptionRuleFactorySchema.getEdorNo());
	                mLPAscriptionRuleParamsSchema.setEdorType(tLPAscriptionRuleFactorySchema.getEdorType());
	                mLPAscriptionRuleParamsSchema.setGrpContNo(mGrpContNo);
	                mLPAscriptionRuleParamsSchema.setGrpPolNo(tLPAscriptionRuleFactorySchema.getGrpPolNo());
	                mLPAscriptionRuleParamsSchema.setAscriptionRuleCode(tLPAscriptionRuleFactorySchema.
	                        getAscriptionRuleCode());
	                mLPAscriptionRuleParamsSchema.setAscriptionRuleName(tLPAscriptionRuleFactorySchema.
	                        getAscriptionRuleName());
	                mLPAscriptionRuleParamsSchema.setRiskCode(tLPAscriptionRuleFactorySchema.
	                        getRiskCode());
	                mLPAscriptionRuleParamsSchema.setFactoryType(tLPAscriptionRuleFactorySchema.
	                        getFactoryType());
	                mLPAscriptionRuleParamsSchema.setFactoryCode(tLPAscriptionRuleFactorySchema.
	                        getFactoryCode());
	                mLPAscriptionRuleParamsSchema.setOtherNo(tLPAscriptionRuleFactorySchema.
	                                                  getOtherNo());
	                mLPAscriptionRuleParamsSchema.setFactoryName(tLPAscriptionRuleFactorySchema.
	                        getFactoryName());
	                mLPAscriptionRuleParamsSchema.setInerSerialNo(String.valueOf(tIndex));
	                mLPAscriptionRuleParamsSchema.setFactorySubCode(
	                        tLPAscriptionRuleFactorySchema.
	                        getFactorySubCode());
	                mLPAscriptionRuleParamsSchema.setParamName(tParams[i]);
	                mLPAscriptionRuleParamsSchema.setParam(tRelParams[i]);
	                mLPAscriptionRuleParamsSchema.setOperator(mGlobalInput.Operator);
	                mLPAscriptionRuleParamsSchema.setMakeDate(mCurrentDate); //当前值
	                mLPAscriptionRuleParamsSchema.setMakeTime(mCurrentTime);
	                mLPAscriptionRuleParamsSchema.setModifyDate(mCurrentDate); //当前值
	                mLPAscriptionRuleParamsSchema.setModifyTime(mCurrentTime);

	                mLPAscriptionRuleParamsSet.add(mLPAscriptionRuleParamsSchema);
	            }
	        }
	        else
	        {
	            mLPAscriptionRuleParamsSchema = new LPAscriptionRuleParamsSchema();

	            //准备计算要素参数信息数据
	            mLPAscriptionRuleParamsSchema.setEdorNo(tLPAscriptionRuleFactorySchema.getEdorNo());
                mLPAscriptionRuleParamsSchema.setEdorType(tLPAscriptionRuleFactorySchema.getEdorType());
	            mLPAscriptionRuleParamsSchema.setGrpContNo(mGrpContNo);
	            mLPAscriptionRuleParamsSchema.setGrpPolNo(tLPAscriptionRuleFactorySchema.getGrpPolNo());
	            mLPAscriptionRuleParamsSchema.setAscriptionRuleCode(tLPAscriptionRuleFactorySchema.
	                    getAscriptionRuleCode());
	            mLPAscriptionRuleParamsSchema.setAscriptionRuleName(tLPAscriptionRuleFactorySchema.
	                    getAscriptionRuleName());
	            mLPAscriptionRuleParamsSchema.setRiskCode(tLPAscriptionRuleFactorySchema.
	                                               getRiskCode());
	            mLPAscriptionRuleParamsSchema.setFactoryType(tLPAscriptionRuleFactorySchema.
	                                                  getFactoryType());
	            mLPAscriptionRuleParamsSchema.setFactoryCode(tLPAscriptionRuleFactorySchema.
	                                                  getFactoryCode());
	            mLPAscriptionRuleParamsSchema.setOtherNo(tLPAscriptionRuleFactorySchema.
	                                              getOtherNo());
	            mLPAscriptionRuleParamsSchema.setFactoryName(tLPAscriptionRuleFactorySchema.
	                                                  getFactoryName());
	            mLPAscriptionRuleParamsSchema.setInerSerialNo(String.valueOf(tIndex));
	            mLPAscriptionRuleParamsSchema.setFactorySubCode(tLPAscriptionRuleFactorySchema.
	                    getFactorySubCode());
	            mLPAscriptionRuleParamsSchema.setParamName(tLMFactoryModeSchema.getParams());
	            mLPAscriptionRuleParamsSchema.setParam(tLPAscriptionRuleFactorySchema.getParams());
	            mLPAscriptionRuleParamsSchema.setOperator(mGlobalInput.Operator);
	            mLPAscriptionRuleParamsSchema.setMakeDate(mCurrentDate); //当前值
	            mLPAscriptionRuleParamsSchema.setMakeTime(mCurrentTime);
	            mLPAscriptionRuleParamsSchema.setModifyDate(mCurrentDate); //当前值
	            mLPAscriptionRuleParamsSchema.setModifyTime(mCurrentTime);

	            mLPAscriptionRuleParamsSet.add(mLPAscriptionRuleParamsSchema);
	        }
	        return true;
	    }


	    /**
	     * 准备删除的要素数据
	     * @return boolean
	     */
	    private boolean prepareOldData()
	    {
	        //删除旧的要素信息
	        LPAscriptionRuleFactoryDB tLPAscriptionRuleFactoryDB = new LPAscriptionRuleFactoryDB();
	        LPAscriptionRuleFactorySet tLPAscriptionRuleFactorySet = new LPAscriptionRuleFactorySet();
	        LPAscriptionRuleFactorySchema tLPAscriptionRuleFactorySchema = new LPAscriptionRuleFactorySchema();
	        tLPAscriptionRuleFactorySchema = mLPAscriptionRuleFactorySet.get(1);
	        tLPAscriptionRuleFactoryDB.setGrpContNo(mGrpContNo);
//	        tLPAscriptionRuleFactoryDB.setRiskCode(tLPAscriptionRuleFactorySchema.getRiskCode());
	        tLPAscriptionRuleFactoryDB.setAscriptionRuleCode(tLPAscriptionRuleFactorySchema.getAscriptionRuleCode());
	        tLPAscriptionRuleFactorySet = tLPAscriptionRuleFactoryDB.query();
	        System.out.println("size:"+tLPAscriptionRuleFactorySet.size());
	        if (tLPAscriptionRuleFactorySet == null)
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "LPAscriptionRuleFactoryBL";
	            tError.functionName = "dealData";
	            tError.errorMessage = "查询旧的要素项目信息出错!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        if (tLPAscriptionRuleFactorySet.size() > 0)
	        {
	            mOldLPAscriptionRuleFactorySet.add(tLPAscriptionRuleFactorySet);

	            //删除旧的计算要素参数信息
	        }
	        LPAscriptionRuleParamsDB tLPAscriptionRuleParamsDB = new LPAscriptionRuleParamsDB();
	        LPAscriptionRuleParamsSet tLCAscriptionRuleParamSet = new LPAscriptionRuleParamsSet();
	        tLPAscriptionRuleParamsDB.setGrpContNo(mGrpContNo);
//	        tLPAscriptionRuleParamsDB.setRiskCode(tLPAscriptionRuleFactorySchema.getRiskCode());
	        tLPAscriptionRuleParamsDB.setAscriptionRuleCode(tLPAscriptionRuleFactorySchema.getAscriptionRuleCode());
	        tLCAscriptionRuleParamSet = tLPAscriptionRuleParamsDB.query();
	        System.out.println("param size:"+tLCAscriptionRuleParamSet.size());
	        if (tLCAscriptionRuleParamSet == null)
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "GrpHealthFactorySaveBL";
	            tError.functionName = "dealData";
	            tError.errorMessage = "查询旧的计算要素参数信息出错!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        if (tLCAscriptionRuleParamSet.size() > 0)
	        {
	            mOldLPAscriptionRuleParamsSet.add(tLCAscriptionRuleParamSet);

	        }
	        return true;
	    }
	    /**
	     * 准备往后层输出所需要的数据
	     * @return 如果准备数据时发生错误则返回false,否则返回true
	     */
	    private boolean prepareData()
	    {
	        mMap.put(mLPGrpEdorMainSchema, "UPDATE");
	        mMap.put(mLPGrpEdorItemSchema, "UPDATE");
	        mResult1.clear();
	        mResult1.add(mMap);

	        return true;
	    }

	    /**
	     * 数据校验
	     * @return boolean
	     */
	    private boolean checkData()
	    {
	        LPAscriptionRuleFactorySchema tLPAscriptionRuleFactorySchema = new LPAscriptionRuleFactorySchema();
	        tLPAscriptionRuleFactorySchema = mLPAscriptionRuleFactorySet.get(1);
	        
	        //add by frost 2009-12-8
	        //添加归属规则校验，如果一个险种下存在投保人为个人缴费的缴费项没有定义归属规则，则不允许通过
	        if(this.mOperate.compareTo("DELETE||MAIN") != 0){
	        	int tCount = 0;
	        	if(mLPAscriptionRuleFactorySet != null){
	        		tCount = mLPAscriptionRuleFactorySet.size();
	        	}
	        	
	        	//找出所有的险种
	        	ArrayList tRiskCodeList = new ArrayList();
	        	for(int i = 1 ; i <= tCount ; i++){
	        		String tRiskcode = mLPAscriptionRuleFactorySet.get(i).getRiskCode();
	        		if(!tRiskCodeList.contains(tRiskcode)){
	        			tRiskCodeList.add(tRiskcode);
	        		}
	        	}      	
	        	//根据每个险种判断是否包含了所有的AccPayClass in ('4', '7', '8')的缴费项
	        	for(int i = 0 ; i < tRiskCodeList.size() ; i++){
	        		String tRiskcode = tRiskCodeList.get(i).toString();
	        		String tSql = "select PayPlanCode,PayPlanName "
	        					+"	from LMDutyPay"
	        					+"		where AccPayClass in ('4', '7', '8')"
	        					+"			and payplancode in"
	        					+"				(select payplancode from lmdutypayrela where dutycode in"
	        					+"						(select dutycode from lmriskduty where riskcode = '"+tRiskcode+"'))";
	        		ExeSQL tExeSQL = new ExeSQL();
	        		SSRS tSSRS = tExeSQL.execSQL(tSql);
	        		for(int j = 1 ; j <= tSSRS.getMaxRow() ; j++){
	        			String tPayPlanCode = tSSRS.GetText(j, 1);
	        			String tPayPlanName = tSSRS.GetText(j, 2);
	        			boolean tFlag = false;
	        			for(int k = 1 ; k <= tCount ; k++){
	                		if(tPayPlanCode.equals(mLPAscriptionRuleFactorySet.get(k).getOtherNo())){
	                			tFlag = true;
	                		}
	                	}  
	        			if(!tFlag){
	        				CError tError = new CError();
	                        tError.moduleName = "LPAscriptionRuleFactoryBL";
	                        tError.functionName = "checkData";
	                        tError.errorMessage = "险种" + tRiskcode + "的"
	                                            + tPayPlanCode + "-" +tPayPlanName
	                                            + "没有录入归属规则，请补充完整！";
	                        this.mErrors.addOneError(tError);
	                        return false;
	        			}
	        		}//每个缴费循环完成       		
	        	}//每个险种循环完成
	        }
	        
	        //如果是删除或修改操作，需要校验是否有被保人拥有该规则，如果有，则该规则不允许删除、修改
	        if (this.mOperate.compareTo("DELETE||MAIN") == 0 ||
	            this.mOperate.compareTo("UPDATE||MAIN") == 0)
	        {}
	        else
	        {
	            LPAscriptionRuleFactoryDB tLPAscriptionRuleFactoryDB = new LPAscriptionRuleFactoryDB();
	            LPAscriptionRuleFactorySet tLPAscriptionRuleFactorySet = new LPAscriptionRuleFactorySet();
	            tLPAscriptionRuleFactoryDB.setGrpContNo(mGrpContNo);
	            tLPAscriptionRuleFactoryDB.setRiskCode(tLPAscriptionRuleFactorySchema.getRiskCode());
	            tLPAscriptionRuleFactoryDB.setAscriptionRuleCode(tLPAscriptionRuleFactorySchema.getAscriptionRuleCode());
	            tLPAscriptionRuleFactorySet = tLPAscriptionRuleFactoryDB.query();
	            if(tLPAscriptionRuleFactorySet.size()>0)
	            {
	                CError tError = new CError();
	                tError.moduleName = "LPAscriptionRuleFactoryBL";
	                tError.functionName = "checkData";
	                tError.errorMessage = "该团单" + mGrpContNo + "的"
	                                    + tLPAscriptionRuleFactorySchema.getRiskCode()
	                                    + "险种下已经存在缴费规则"
	                                    + tLPAscriptionRuleFactorySchema.getAscriptionRuleCode()
	                                    + "，如果您要变动，请使用缴费规则修改功能！";
	                this.mErrors.addOneError(tError);
	                return false;
	            }

	        }
	        return true;
	    }

	}
