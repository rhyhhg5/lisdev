package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全处理财务数据</p>
 * <p>Description:把保全补退费数据放入财务接口</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GEdorZBDetailUI
{
    private GEdorZBDetailBL mGEdorZBDetailBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public GEdorZBDetailUI(GlobalInput gi, String edorNo,
            String grpContNo, EdorItemSpecialData specialData)
    {
        mGEdorZBDetailBL = new GEdorZBDetailBL(gi, edorNo, grpContNo, specialData);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mGEdorZBDetailBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public  String getError()
    {
        return mGEdorZBDetailBL.mErrors.getFirstError();
    }

    /**
     * 得到需显示的提示信息
     * @return String
     */
    public String getMessage()
    {
        return mGEdorZBDetailBL.getMessage();
    }
}
