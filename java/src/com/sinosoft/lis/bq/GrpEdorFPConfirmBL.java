package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class GrpEdorFPConfirmBL implements EdorConfirm {
    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = "";

    /** 团体合同号 */
    private String mGrpContNo = null;

    /**  */
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();
    boolean newaddrFlag = false;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    public GrpEdorFPConfirmBL() {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }
        System.out.println("---End getInputData---");

        //数据准备操作
        if (!prepareData()) {
            return false;
        }
        System.out.println("---End prepareData---");
        if (!prepareOutputData()) {
            return false;
        }

//        // 数据操作业务处理
//        PEdorConfirmBLS tPEdorConfirmBLS = new PEdorConfirmBLS();
//        System.out.println("Start Confirm BB BL Submit...");
//        if (!tPEdorConfirmBLS.submitData(mInputData, mOperate))
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tPEdorConfirmBLS.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "PEdorConfirmBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "数据提交失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    private boolean prepareOutputData() {

        mResult.clear();
        mResult.add(map);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData() {
        try {
            mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) mInputData.
                                   getObjectByObjectName("LPGrpEdorItemSchema",
                    0);
            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
            mEdorType = mLPGrpEdorItemSchema.getEdorType();
            mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData() {
        LPGrpServInfoSet outInsertLPGrpServInfoSet = new LPGrpServInfoSet();
        LCGrpServInfoSet outInsertLCGrpServInfoSet = new LCGrpServInfoSet();
        LCGrpServInfoSet outDeleteLCGrpServInfoSet = new LCGrpServInfoSet();

        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(this.mGrpContNo);
        tLCGrpContSet = tLCGrpContDB.query();
        String tProposalGrpcontno = tLCGrpContSet.get(1).getProposalGrpContNo();

        //从LP反射生成LC表记录
        LPGrpServInfoSet tLPGrpServInfoSet = new LPGrpServInfoSet();
        LPGrpServInfoDB tLPGrpServInfoDB = new LPGrpServInfoDB();
        tLPGrpServInfoDB.setEdorNo(this.mEdorNo);
        tLPGrpServInfoDB.setEdorType(this.mEdorType);
        tLPGrpServInfoDB.setGrpContNo(tProposalGrpcontno);
        tLPGrpServInfoSet = tLPGrpServInfoDB.query();
        if (tLPGrpServInfoDB.mErrors.needDealError()) {
            // @@错误处理
            mErrors.copyAllErrors(tLPGrpServInfoDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpEdorFPConfirmBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "查询团单服务信息表出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLPGrpServInfoSet.size() == 0) {
            // @@错误处理
            mErrors.copyAllErrors(tLPGrpServInfoDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpEdorFPConfirmBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "查询保全团单服务信息表无记录!";
            this.mErrors.addOneError(tError);
            return false;
        } else {
            for (int i = 1; i <= tLPGrpServInfoSet.size(); i++) {
                LPGrpServInfoSchema tLPGrpServInfoSchema =
                        tLPGrpServInfoSet.get(i);
                if (tLPGrpServInfoSchema.getServKind().equals("1") &&
                    tLPGrpServInfoSchema.getServDetail().equals("3"))
                {
                    exchangeLCGrpBalPlan(tLPGrpServInfoSchema.getServChoose());
                }
                LCGrpServInfoSchema tLCGrpServInfoSchema = new
                        LCGrpServInfoSchema();
                Reflections ref = new Reflections();
                ref.transFields(tLCGrpServInfoSchema, tLPGrpServInfoSchema);
                outInsertLCGrpServInfoSet.add(tLCGrpServInfoSchema);
            }
        }

        //从LP反射生成LC表记录
        LCGrpServInfoSet tLCGrpServInfoSet = new LCGrpServInfoSet();
        LCGrpServInfoDB tLCGrpServInfoDB = new LCGrpServInfoDB();
        tLCGrpServInfoDB.setGrpContNo(tProposalGrpcontno);
        tLCGrpServInfoSet = tLCGrpServInfoDB.query();
        if (tLCGrpServInfoDB.mErrors.needDealError()) {
            // @@错误处理
            mErrors.copyAllErrors(tLCGrpServInfoDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpEdorFPConfirmBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "查询团单服务信息表出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //可以无记录，说明在新单承保时没有输入
        if (tLCGrpServInfoSet.size() > 0) {
            for (int i = 1; i <= tLCGrpServInfoSet.size(); i++) {
                LCGrpServInfoSchema tLCGrpServInfoSchema =
                        tLCGrpServInfoSet.get(i);
                for (int j = 1; j <= tLPGrpServInfoSet.size(); j++) {
                    if (tLPGrpServInfoSet.get(j).getServKind().equals(
                            tLCGrpServInfoSchema.getServKind()) &&
                        tLPGrpServInfoSet.get(j).getServDetail().equals(
                                tLCGrpServInfoSchema.getServDetail())) {
                        LPGrpServInfoSchema tLPGrpServInfoSchema = new
                                LPGrpServInfoSchema();
                        Reflections ref = new Reflections();
                        ref.transFields(tLPGrpServInfoSchema,
                                        tLCGrpServInfoSchema);
                        tLPGrpServInfoSchema.setEdorNo(this.mEdorNo);
                        tLPGrpServInfoSchema.setEdorType(this.mEdorType);
                        outInsertLPGrpServInfoSet.add(tLPGrpServInfoSchema);
                        outDeleteLCGrpServInfoSet.add(tLCGrpServInfoSchema);
                    }
                }
            }
        }
        map.put(tLPGrpServInfoSet, "DELETE");
        map.put(outDeleteLCGrpServInfoSet, "DELETE");
        map.put(outInsertLCGrpServInfoSet, "INSERT");
        map.put(outInsertLPGrpServInfoSet, "INSERT");


        return true;
    }

    public boolean exchangeLCGrpBalPlan(String intv)
    {
        LCGrpBalPlanDB tLCGrpBalPlanDB=new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(this.mGrpContNo);
        LCGrpBalPlanSet tLCGrpBalPlanSet=tLCGrpBalPlanDB.query();
        if (tLCGrpBalPlanDB.mErrors.needDealError()) {
            // @@错误处理
            mErrors.copyAllErrors(tLCGrpBalPlanDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpEdorFPConfirmBL";
            tError.functionName = "exchangeLCGrpBalPlan";
            tError.errorMessage = "查询团单定期结算计划表出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(tLCGrpBalPlanSet.size()==0)
        {
            LCGrpContDB tLCGrpContDB=new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(this.mGrpContNo);
            if(!tLCGrpContDB.getInfo())
            {
                    // @@错误处理
                mErrors.copyAllErrors(tLCGrpBalPlanDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpEdorFPConfirmBL";
                tError.functionName = "exchangeLCGrpBalPlan";
                tError.errorMessage = "查询团单表出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            GrpBalanceOnTime tGrpBalanceOnTime=new GrpBalanceOnTime();
            MMap tMap=tGrpBalanceOnTime.addLCGrpBalPlan(
                    this.mGrpContNo,tLCGrpContDB.getCValiDate(),intv,
                    mGlobalInput.ManageCom,mGlobalInput.Operator);
            map.add(tMap);
            return true;
        }else
        {
            LCGrpBalPlanSchema tLCGrpBalPlanSchema=tLCGrpBalPlanSet.get(1);
            LPGrpBalPlanSchema tLPGrpBalPlanSchema=new LPGrpBalPlanSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLPGrpBalPlanSchema,tLCGrpBalPlanSchema);
            tLPGrpBalPlanSchema.setEdorNo(this.mEdorNo);
            tLPGrpBalPlanSchema.setEdorType(this.mEdorType);
            map.put(tLPGrpBalPlanSchema,"INSERT");
            GrpBalanceOnTime tGrpBalanceOnTime=new GrpBalanceOnTime();
            MMap tMap = tGrpBalanceOnTime.changeLCGrpBalIntrv(tLCGrpBalPlanSchema,
                    intv, mGlobalInput.ManageCom, mGlobalInput.Operator);
            map.add(tMap);
        }
        return true;
    }

    public TransferData getReturnTransferData() {
        return null;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    public static void main(String[] args) {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        tGlobalInput.ManageCom = "86";

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorAcceptNo("86110000000436");
        tLPGrpEdorItemSchema.setEdorNo("430110000000143");
        tLPGrpEdorItemSchema.setEdorAppNo("430110000000143");
        tLPGrpEdorItemSchema.setGrpContNo("0000034201");
        tLPGrpEdorItemSchema.setEdorType("AC");

        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPGrpEdorItemSchema);

        GrpEdorFPConfirmBL tGrpEdorFPConfirmBL = new GrpEdorFPConfirmBL();
        if (!tGrpEdorFPConfirmBL.submitData(tVData, "")) {
            System.out.println(tGrpEdorFPConfirmBL.mErrors.getErrContent());
        }
    }

}
