package com.sinosoft.lis.bq;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 协议退保明细</p>
 * <p>Copyright: Copyright (c) 2005.3.31</p>
 * <p>Company: Sinosoft</p>
 * @author LHS
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

public class GrpEdorXTDetailUI
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public GrpEdorXTDetailUI()
    {
    }


    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        GrpEdorXTDetailBL tPGrpEdorXTDetailBL = new GrpEdorXTDetailBL();
        if (!tPGrpEdorXTDetailBL.submitData(cInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPGrpEdorXTDetailBL.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 主函数，测试用
     */
    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        tGlobalInput.ManageCom = "86";

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorNo("430110000000047");
        tLPGrpEdorItemSchema.setGrpContNo("240000000000005");
        tLPGrpEdorItemSchema.setEdorType("XT");
        tLPGrpEdorItemSchema.setGetMoney(600);

        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPGrpEdorItemSchema);

       /* PGrpEdorXTDetailUI tGrpEdorXTDetailUI = new PGrpEdorXTDetailUI();
        if (!tGrpEdorXTDetailUI.submitData(tVData, "INSERT"))
        {
            System.out.println(tGrpEdorXTDetailUI.mErrors.getErrContent());
        }
       */
    }
}
