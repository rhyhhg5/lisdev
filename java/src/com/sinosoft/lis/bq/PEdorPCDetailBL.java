package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全明细录入</p>
 * <p>Description: 交费频次变更</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorPCDetailBL {
  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  private MMap mMap = new MMap();

  private GlobalInput mGlobalInput = null;

  private DetailDataQuery mQuery = null;

  private String mEdorNo = null;

  private String mContNo = null;
  private String mPolNo = null;
  private String mNewPrem  = null;
  private String mEdorType = BQ.EDORTYPE_PC;

  private LCPolSet mLCPolSet = null;

  private String mCurrentDate = PubFun.getCurrentDate();

  private String mCurrentTime = PubFun.getCurrentTime();
  private DisabledManageBL tDisabledManageBL = new DisabledManageBL();

  /**
   * 构造函数
   * @param gi GlobalInput
   * @param edorNo String
   */
  public PEdorPCDetailBL(GlobalInput gi, String edorNo, String contNo) {
    this.mGlobalInput = gi;
    this.mEdorNo = edorNo;
    this.mContNo = contNo;
    this.mQuery = new DetailDataQuery(mEdorNo, mEdorType);
  }

  /**
   * 提交数据
   * @param data VData
   * @return boolean
   */
  public boolean submitData(VData data) {
    if (!getInputData(data)) {
      return false;
    }

    if (!checkData()) {
      return false;
    }

    if (!dealData()) {
      return false;
    }

    if (!submit()) {
      return false;
    }
    return true;
  }

  /**
   * 得到输入数据
   * @param data VData
   * @return boolean
   */
  private boolean getInputData(VData data) {
    mLCPolSet = (LCPolSet) data.getObjectByObjectName("LCPolSet", 0);
    return true;
  }

//add by luomin
  private boolean checkData()
  {
      if(!tDisabledManageBL.dealDisabledpol(mLCPolSet, "PC"))
      {
          CError tError = new CError();
          tError.moduleName = "PEdorPCDetailBL";
          tError.functionName = "checkData";
          tError.errorMessage = tDisabledManageBL.getInsuredno() +
                                "号被保人投报险种'" + tDisabledManageBL.getRiskcode() +
                                "'下PC项目在"
                                + CommonBL.getCodeName("stateflag",
              tDisabledManageBL.getState()) + "状态下不能添加!";
          this.mErrors.addOneError(tError);
          return false;

      }

      int changeNum = 0;

      //add by qulq 070214 新的交至日期必须和旧的一致
      for(int i = 1; i <= mLCPolSet.size(); i++)
      {
          LCPolSchema tLCPolSchema = mLCPolSet.get(i);
          LCPolSchema oldLCPolSchema = CommonBL.getLCPol(tLCPolSchema.getPolNo());
          int passM = PubFun.calInterval(oldLCPolSchema.getCValiDate(),
                                         oldLCPolSchema.getPaytoDate(),
                                         "M");
          if(tLCPolSchema.getPayIntv() <= 0)
          {
              this.mErrors.addOneError("您好，交费频次不能变更为"
                                       + CommonBL.getCodeName("payintv",
                  "" + tLCPolSchema.getPayIntv()));
              return false;
          }

//          if(tLCPolSchema.getPayIntv() < oldLCPolSchema.getPayIntv())
//          {
//              CError tError = new CError();
//              tError.moduleName = "PEdorPCDetailBL";
//              tError.functionName = "checkData";
//              tError.errorMessage = "交费频率只能从高到低(如从月缴变更为年缴)变更";
//              mErrors.addOneError(tError);
//              System.out.println(tError.errorMessage);
//              return false;
//          }

          if((passM % (tLCPolSchema.getPayIntv())) != 0)
          {
              this.mErrors.addOneError("您好，交费频次变更后的保费交至日期必须和原交至日期一致。");
              return false;
          }

            if(tLCPolSchema.getPayIntv() != oldLCPolSchema.getPayIntv())
           {
               changeNum++;
           }
      }

      if(changeNum == 0)
      {
          CError tError = new CError();
          tError.moduleName = "PEdorPCDetailBL";
          tError.functionName = "checkData";
          tError.errorMessage = "请先做变更后再保存";
          mErrors.addOneError(tError);
          System.out.println(tError.errorMessage);
          return false;
      }

      return true;
  }
  
  /**
   * 税优计算风险保费
   * */
  private double getRiskPremSY(String aPolno,int payIntv,int aInsuredAge,String aInsuSex)
  {  
  	double rpPrem=0.0;
      String tInsuSex=aInsuSex;
      int tInsuredAge=aInsuredAge;
      int tpayIntv=payIntv;
      SSRS tSSRS = new SSRS();
      try
      {   

          
			String strSQL="select insuaccno from lcinsureacc  where  polno='"+ aPolno +"' ";
		    tSSRS = new ExeSQL().execSQL(strSQL);
			if(tSSRS.MaxRow == 0){
				mErrors.addOneError("获取账户信息失败！");
              return 1;
			}
			
          LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
          tLCInsureAccClassDB.setPolNo(aPolno);
          tLCInsureAccClassDB.setInsuAccNo(tSSRS.GetText(1, 1));
          LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.
                  query();
          if(tLCInsureAccClassSet.size()!=1){
          	// @@错误处理
				System.out.println("PEdorPCDetailBL+getRiskPrem2++--");
				CError tError = new CError();
				tError.moduleName = "PEdorPCDetailBL";
				tError.functionName = "getRiskPrem2";
				tError.errorMessage = "获取万能账户信息失败!";
				this.mErrors.addOneError(tError);
				return 1;
          }
//          mLCInsureAccClassSchema = tLCInsureAccClassSet.get(1);

      	//税优风险保费计算
			LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
			tLMRiskFeeDB.setInsuAccNo(tSSRS.GetText(1, 1));
			tLMRiskFeeDB.setFeeKind("03"); // 03-个单管理费
			tLMRiskFeeDB.setFeeItemType("02");// 04-初始扣费
			tLMRiskFeeDB.setFeeTakePlace("01"); // 01－交费时		
			LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
			if (tLMRiskFeeDB.mErrors.needDealError()) 
			{
				CError.buildErr(this, "账户管理费查询失败!");
				return 1;
			}
          Calculator tCalculator = new Calculator();
          tCalculator.setCalCode(tLMRiskFeeSet.get(1).getFeeCalCode());
          tCalculator.addBasicFactor("Sex", tInsuSex);
          tCalculator.addBasicFactor("PolNo",aPolno);
			tCalculator.addBasicFactor("PayIntv",""+ tpayIntv);
			tCalculator.addBasicFactor("AppAge",""+ tInsuredAge);
          
			strSQL="select a.payintv,a.insuredappage,b.totaldiscountfactor from lcpol a,lccontsub b where a.prtno=b.prtno and a.polno='"+ aPolno +"' ";
		    tSSRS = new ExeSQL().execSQL(strSQL);
			if(tSSRS.MaxRow == 0){
				mErrors.addOneError("获取保单详细信息失败！");
              return 1;
			}
			//增加折扣因子
			if(null != tSSRS.GetText(1, 3) && !"".equals(tSSRS.GetText(1, 3))){
				tCalculator.addBasicFactor("Totaldiscountfactor", tSSRS.GetText(1, 3));
			}else
			{
				tCalculator.addBasicFactor("Totaldiscountfactor", "1");
			}
			
          double fee = Math.abs(PubFun.setPrecision(
                      Double.parseDouble(tCalculator.calculate()), "0.00"));
          if (tCalculator.mErrors.needDealError())
          {
              System.out.println(tCalculator.mErrors.getErrContent());
              mErrors.addOneError("计算风险保费出错");
              return 1;
          }
          rpPrem += fee;

      } catch (Exception e)
      {
          //累计风险保费扣费一定小于0，如果返回1证明查询累计风险保费扣费失败
          return 1;
      }
      return rpPrem;
  }

  /**
   * 处理业务数据
   * @return boolean
   */
  private boolean dealData() {
    setLPCont();
    setLPPol();
    
 	String isTPHISQL = " SELECT polno FROM LCPOL a WHERE a.contno = '" +mContNo + 
	"' and riskcode in (select riskcode from lmriskapp where taxoptimal ='Y') " +
	" with ur  ";
    ExeSQL tExeSQL = new ExeSQL();
    String isTPHIRisk = tExeSQL.getOneValue(isTPHISQL);
   if(null!=isTPHIRisk && !isTPHIRisk.equals("")){
	   mPolNo = isTPHIRisk;
	   String  tSql ="select distinct insuredsex,insuredbirthday,payenddate from lcpol where  polno='"+mPolNo+"'";
       SSRS tSSRS = new ExeSQL().execSQL(tSql);
       String tInsuSex = tSSRS.GetText(1, 1);
       String tInsuBirthDay = tSSRS.GetText(1, 2);
       String tPayEndDate = tSSRS.GetText(1, 3);
       int mPayIntv = 12; 
       int tInsuredAge = PubFun.calInterval(tInsuBirthDay,tPayEndDate, "Y");
       mNewPrem =String.valueOf(getRiskPremSY(mPolNo,mPayIntv,tInsuredAge,tInsuSex));//下个年度的风险保费   
       if(null == mNewPrem || "".equals(mNewPrem)){
    	   mErrors.addOneError("计算风险保费出错");
    	   return false; 
       }
	   EdorItemSpecialData tSpecialData = new EdorItemSpecialData(mEdorNo,mEdorType);
	   tSpecialData.add("NEWPREM", mNewPrem);
	   tSpecialData.add("POLNO", mPolNo);
	   mMap.put(tSpecialData.getSpecialDataSet(), "DELETE&INSERT");
     }
    setEdorState(BQ.EDORSTATE_INPUT);
    return true;
  }

  /**
   * 设置LPCont
   */
  private void setLPCont() {
    LPContSchema tLPContSchema = (LPContSchema)
        mQuery.getDetailData("LCCont", mContNo);
    tLPContSchema.setOperator(mGlobalInput.Operator);
    tLPContSchema.setMakeDate(mCurrentDate);
    tLPContSchema.setMakeTime(mCurrentTime);
    tLPContSchema.setModifyDate(mCurrentDate);
    tLPContSchema.setModifyTime(mCurrentTime);
    mMap.put(tLPContSchema, "DELETE&INSERT");
  }

  /**
   * 设置LPPol表的交费频次
   */
  private void setLPPol() {
    String sql = "delete from LPPol " +
        "where EdorNo = '" + mEdorNo + "' " +
        "and EdorType = '" + mEdorType + "' " +
        "and ContNo = '" + mContNo + "'";
    mMap.put(sql, "DELETE");
    for (int i = 1; i <= mLCPolSet.size(); i++) {
      LCPolSchema tLCPolSchema = mLCPolSet.get(i);
//      int oldPayIntv = CommonBL.getLCPol(tLCPolSchema.getPolNo()).getPayIntv();
//      if (oldPayIntv == tLCPolSchema.getPayIntv()) {
//        continue;
//      }

      LPPolSchema tLPPolSchema = (LPPolSchema)
          mQuery.getDetailData("LCPol", tLCPolSchema.getPolNo());
      tLPPolSchema.setPayIntv(tLCPolSchema.getPayIntv());
      tLPPolSchema.setOperator(mGlobalInput.Operator);
      tLPPolSchema.setMakeDate(mCurrentDate);
      tLPPolSchema.setMakeTime(mCurrentTime);
      tLPPolSchema.setModifyDate(mCurrentDate);
      tLPPolSchema.setModifyTime(mCurrentTime);
      mMap.put(tLPPolSchema, "INSERT");
      setLPDuty(tLPPolSchema);
    }
  }

  /**
   * 设置LPDuty
   * @param polNo String
   */
  private void setLPDuty(LPPolSchema aLPPolSchema) {
    LCDutyDB tLCDutyDB = new LCDutyDB();
    tLCDutyDB.setPolNo(aLPPolSchema.getPolNo());
    LCDutySet tLCDutySet = tLCDutyDB.query();
    for (int i = 1; i <= tLCDutySet.size(); i++) {
      LCDutySchema tLCDutySchema = tLCDutySet.get(i);
      String[] keys = new String[2];
      keys[0] = aLPPolSchema.getPolNo();
      keys[1] = tLCDutySchema.getDutyCode();
      LPDutySchema tLPDutySchema = (LPDutySchema)
          mQuery.getDetailData("LCDuty", keys);
      tLPDutySchema.setPayIntv(aLPPolSchema.getPayIntv());
      tLPDutySchema.setOperator(mGlobalInput.Operator);
      tLPDutySchema.setMakeDate(mCurrentDate);
      tLPDutySchema.setMakeTime(mCurrentTime);
      tLPDutySchema.setModifyDate(mCurrentDate);
      tLPDutySchema.setModifyTime(mCurrentTime);
      mMap.put(tLPDutySchema, "DELETE&INSERT");
      setLPPrem(tLPDutySchema);
    }
  }

  /**
   * 设置LPPrem
   * @param aLPDutySchema LPDutySchema
   */
  private void setLPPrem(LPDutySchema aLPDutySchema) {
    LCPremDB tLCPremDB = new LCPremDB();
    tLCPremDB.setPolNo(aLPDutySchema.getPolNo());
    tLCPremDB.setDutyCode(aLPDutySchema.getDutyCode());
    LCPremSet tLCPremSet = tLCPremDB.query();
    for (int i = 1; i <= tLCPremSet.size(); i++) {
      LCPremSchema tLCPremSchema = tLCPremSet.get(i);
      String[] keys = new String[3];
      keys[0] = aLPDutySchema.getPolNo();
      keys[1] = aLPDutySchema.getDutyCode();
      keys[2] = tLCPremSchema.getPayPlanCode();
      LPPremSchema tLPPremSchema = (LPPremSchema)
          mQuery.getDetailData("LCPrem", keys);
      tLPPremSchema.setPayIntv(aLPDutySchema.getPayIntv());
      tLPPremSchema.setOperator(mGlobalInput.Operator);
      tLPPremSchema.setMakeDate(mCurrentDate);
      tLPPremSchema.setMakeTime(mCurrentTime);
      tLPPremSchema.setModifyDate(mCurrentDate);
      tLPPremSchema.setModifyTime(mCurrentTime);
      mMap.put(tLPPremSchema, "DELETE&INSERT");
    }
  }

  /**
   * 把保全状态设为已录入
   * @param edorState String
   */
  private void setEdorState(String edorState) {
    String sql = "update  LPEdorItem " +
        "set EdorState = '" + edorState + "', " +
        "    Operator = '" + mGlobalInput.Operator + "', " +
        "    ModifyDate = '" + mCurrentDate + "', " +
        "    ModifyTime = '" + mCurrentTime + "' " +
        "where  EdorNo = '" + mEdorNo + "' " +
        "and EdorType = '" + mEdorType + "' " +
        "and ContNo = '" + mContNo + "'";
    mMap.put(sql, "UPDATE");
  }

  /**
   * 提交数据到数据库
   * @return boolean
   */
  private boolean submit() {
    VData data = new VData();
    data.add(mMap);
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(data, "")) {
      mErrors.copyAllErrors(tPubSubmit.mErrors);
      return false;
    }
    return true;
  }
}
