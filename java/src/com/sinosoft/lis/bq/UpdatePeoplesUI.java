package com.sinosoft.lis.bq;

/**
 * <p>Title: 维护合同表中的人数字段</p>
 * <p>Description: 主要用在增人之后</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class UpdatePeoplesUI
{
    private UpdatePeoplesBL mUpdatePeoplesBL = null;

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData(String grpContNo)
    {
        mUpdatePeoplesBL = new UpdatePeoplesBL(grpContNo);
        if (!mUpdatePeoplesBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mUpdatePeoplesBL.mErrors.getFirstError();
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
    }
}
