package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class PEdorAppMainBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /**判断条件*/
    private String mEdorType;

    /**  */
    //保全申请主表
    private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();


    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();
    //统一更新日期，时间
   private String theCurrentDate=PubFun.getCurrentDate();
   private String theCurrentTime=PubFun.getCurrentTime();


    public PEdorAppMainBL()
    {}

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return null;

        //校验传入的是否合法
        if (!checkData())
            return null;

        // 数据操作业务处理
        if (cOperate.equals("INSERT||EDORAPP"))
        {
            if (!dealData())
                return null;
            System.out.println("---dealData---");
        }
        //准备给后台的数据
        if (prepareOutputData() == false)
        { //错误处理
            return null;
        }
        return map;
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        MMap tMMap = getSubmitMap(cInputData, cOperate);
        if(tMMap == null)
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "PEdorAppMainBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
        return true;


    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {

        System.out.println("End prepareOutputData...");
        //int m;
        //准备个人保单（保全）的信息


        //int n = mLPEdorAppMainBLSet.size();
        if (mLPEdorAppSchema.getEdorAcceptNo()==null||mLPEdorAppSchema.getEdorAcceptNo().equals(""))
        {

            String strLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
            String strEdorAcceptNo = PubFun1.CreateMaxNo("EdorAcceptNo",
                            strLimit);
            if (StrTool.compareString(strEdorAcceptNo,""))
            {
                CError.buildErr(this,"生成保全申请号错误！");
                return false;
            }
            else
            {
               mLPEdorAppSchema.setEdorAcceptNo(strEdorAcceptNo);
            }
        }
        //mLPEdorAppSchema.setEdorAppDate(theCurrentDate);

        //若没哟传入状态,则初始化为1
        if(mLPEdorAppSchema.getEdorState() == null
            || mLPEdorAppSchema.getEdorState().equals(""))
        {
            mLPEdorAppSchema.setEdorState("1");
        }
        mLPEdorAppSchema.setOperator(mGlobalInput.Operator);
        mLPEdorAppSchema.setManageCom(mGlobalInput.ManageCom);
        mLPEdorAppSchema.setMakeDate(theCurrentDate);
        mLPEdorAppSchema.setMakeTime(theCurrentTime);
        mLPEdorAppSchema.setModifyDate(theCurrentDate);
        mLPEdorAppSchema.setModifyTime(theCurrentTime);

       map.put(mLPEdorAppSchema,"INSERT");

        return true;

    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
       mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
               "GlobalInput", 0));

        if (mOperate.equals("INSERT||EDORAPP"))
        {
            mLPEdorAppSchema.setSchema((LPEdorAppSchema) cInputData.
                                       getObjectByObjectName(
                                               "LPEdorAppSchema", 0));
//      tLPEdorMainSchema.setSchema(mLPEdorAppMainBLSet.get(1));
        }

        return true;

    }

    /**
     * 校验传入的是否合法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {

        LCContSet tLCContSet=new LCContSet();
        LCContDB tLCContDB=new LCContDB();
        //qulq 保全回退修改
        String wherePart = "1=2";//这个没什么意义，是为了防止SQL报错。
        if (StrTool.compareString(mLPEdorAppSchema.getOtherNoType(), "") ||
            StrTool.compareString(mLPEdorAppSchema.getOtherNo(), ""))
        {
            CError.buildErr(this,"申请号码或者申请号码类型为空！");
                return false;
        }
        if (mLPEdorAppSchema.getOtherNoType().equals("1"))//客户号
        {
            tLCContDB.setAppntNo(mLPEdorAppSchema.getOtherNo());
            wherePart = " appntno = '"+mLPEdorAppSchema.getOtherNo()+"' ";
        }
        else if (mLPEdorAppSchema.getOtherNoType().equals("3"))//保单号
        {
            tLCContDB.setContNo(mLPEdorAppSchema.getOtherNo());
            wherePart = " ContNo = '"+mLPEdorAppSchema.getOtherNo()+"' ";
        }
        //需要增加查询有效保单条件
        tLCContDB.setAppFlag("1");
        tLCContSet = tLCContDB.query();
        if (tLCContDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询客户的保单信息失败！");
            return false;
        }
        if (tLCContSet.size() == 0)
        {
                //查询是否有可回退保单
                String querySql = " select 1 from LBCont a where " + wherePart
                                  + " and exists (select 1 from LPEdorItem where ContNo = a.contno "
                                  + " and edortype in ('WT','CT','XT') and edorno = a.edorno) "
                                  ;
                System.out.println("querySql==="+querySql);
                String re= new ExeSQL().getOneValue(querySql);
                if("".equals(re))
                {
                    CError.buildErr(this, "该客户没有有效保单,不能申请保全！");
                    return false;
                }
         }



//        boolean flag = true;
//        LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
//        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
//        LPEdorMainSet tLPEdorMainSet = new LPEdorMainSet();
//
//        tLPEdorMainSchema = mLPEdorMainSet.get(1).getSchema();
//        tLPEdorAppDB.setGrpPolNo(tLPEdorMainSchema.getGrpPolNo());
//
//        if (this.mOperate.equals("INSERT||EDORTYPE"))
//            tLPEdorAppDB.setEdorType(tLPEdorMainSchema.getEdorType());
//
//        tLPEdorAppDB.setEdorState("1");
//
//        tLPEdorMainSet.clear();
//        tLPEdorMainSet = tLPEdorAppDB.query();
//
//        if (tLPEdorMainSet != null)
//        {
//            System.out.println("----chksize:" + tLPEdorMainSet.size());
//            if (mOperate.equals("INSERT||EDOR") ||
//                mOperate.equals("INSERT||EDORTYPE"))
//            {
//                if (tLPEdorMainSet.size() > 0)
//                {
//                    // @@错误处理
//                    CError tError = new CError();
//                    tError.moduleName = "PEdorAppMainBL";
//                    tError.functionName = "Chkdata";
//                    tError.errorMessage = "此申请已经存在,不需再次申请!";
//                    this.mErrors.addOneError(tError);
//                    return false;
//                }
//            }
//        }
//
//        if (mOperate.equals("INSERT||EDOR") ||
//            mOperate.equals("INSERT||EDORTYPE") ||
//            mOperate.equals("UPDATE||EDOR"))
//        {
//            String tSql = "select * from LPEdormain where polno='" +
//                          tLPEdorMainSchema.getGrpPolNo() + "' and EdorNo='" +
//                          tLPEdorMainSchema.getEdorNo() +
//                          "' and EdorState in ('2','0')";
//            System.out.println("---sql:" + tSql);
//            LPEdorAppDB iLPEdorMainDB = new LPEdorAppDB();
//            LPEdorMainSet iLPEdorMainSet = new LPEdorMainSet();
//            iLPEdorMainSet = iLPEdorMainDB.executeQuery(tSql);
//            if (iLPEdorMainSet != null && iLPEdorMainSet.size() > 0)
//            {
//                // @@错误处理
//                CError tError = new CError();
//                tError.moduleName = "PEdorAppMainBL";
//                tError.functionName = "Chkdata";
//                tError.errorMessage = "此申请已经申请确认,不能添加/修改项目!";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
//        }
//
//        System.out.println("--------check_data");
        return true;

    }

    /**
     * 准备需要保存的集体数据
     */
    private boolean prepareOutputData()
    {
        try
       {
           mInputData.clear();
           mInputData.add(map);
           mResult.clear();
           mResult.add(mLPEdorAppSchema);
       }
       catch (Exception ex)
       {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "PEdorAppMainBL";
           tError.functionName = "prepareData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;


    }

}
