package com.sinosoft.lis.bq;

import java.io.IOException;
import java.net.MalformedURLException;

import com.sinosoft.lis.bq.mm.face.ISendEmail;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.SendMsgMail;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class SendEmailImpl implements ISendEmail{

	public boolean sendEmail(String address, String content,
			GlobalInput tG) {
		 TransferData SendElement = new TransferData();
         SendElement.setNameAndValue("mobile","");
         SendElement.setNameAndValue("content",content);
         SendElement.setNameAndValue("strFrom","service@picchealth.com");
         SendElement.setNameAndValue("strTo",address);
         SendElement.setNameAndValue("strTitle","picc");
         SendElement.setNameAndValue("strPassword","");

         VData aVData = new VData();
         aVData.add(tG);
         aVData.add(SendElement);

         SendMsgMail tSendMsgMail = new SendMsgMail();
         try{
			tSendMsgMail.submitData(aVData,"Email");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return false ;
		} catch (IOException e) {
			e.printStackTrace();
			return false ;
		}

		return false;
	}

}
