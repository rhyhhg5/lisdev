package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LPAscriptionRuleFactorySchema;

import com.sinosoft.lis.vschema.LPAscriptionRuleFactorySet;
import com.sinosoft.utility.*;


/**
 * 保障计划要素前台数据接收类
 * <p>Title: </p>
 * <p>Description: 接收前台数据，传入BL </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SINOSOFT</p>
 * @author ZHUXF
 * @version 1.0
 */
public class GrpEdorGBDetailUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();


    /** 数据操作字符串 */
    private String mOperate;


//业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
   public GrpEdorGBDetailUI()
    {
    }


    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	   this.mInputData = (VData)cInputData.clone();
    	    this.mOperate = cOperate;
        GrpEdorGBDetailBL tGrpEdorGBDetailBL = new GrpEdorGBDetailBL();
        System.out.println("Start GrpEdorGBDetailUI UI Submit...");
        tGrpEdorGBDetailBL.submitData(mInputData, mOperate);
        System.out.println("End GrpEdorGBDetailUI UI Submit...");
        //如果有需要处理的错误，则返回
        if (tGrpEdorGBDetailBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpEdorGBDetailBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpEdorGBDetailUIUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOperate.equals("QUERY||MAIN"))
        {
            this.mResult.clear();
            this.mResult = tGrpEdorGBDetailBL.getResult();
        }
        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
        LPAscriptionRuleFactorySet tLPAscriptionRuleFactorySet = new LPAscriptionRuleFactorySet();
        LPAscriptionRuleFactorySchema tLPAscriptionRuleFactorySchema = new
                LPAscriptionRuleFactorySchema();

        tLPAscriptionRuleFactorySchema = new LPAscriptionRuleFactorySchema();
        tLPAscriptionRuleFactorySchema.setGrpContNo("140110000000176");
        tLPAscriptionRuleFactorySchema.setGrpPolNo("120110000000403");
        tLPAscriptionRuleFactorySchema.setAscriptionRuleCode("A");
        tLPAscriptionRuleFactorySchema.setAscriptionRuleName("dsa");
        tLPAscriptionRuleFactorySchema.setRiskCode("212403");
        tLPAscriptionRuleFactorySchema.setFactoryType("000006");
        tLPAscriptionRuleFactorySchema.setOtherNo("692101");
        tLPAscriptionRuleFactorySchema.setFactoryCode("gs0001");
        tLPAscriptionRuleFactorySchema.setFactorySubCode("1");
        tLPAscriptionRuleFactorySchema.setFactoryName("ServiceYear");
        tLPAscriptionRuleFactorySchema.setCalRemark(
                "当员工在公司工作满a年不满b年时，归属比例为c");
        tLPAscriptionRuleFactorySchema.setParams("0,3,0.05");
        tLPAscriptionRuleFactorySet.add(tLPAscriptionRuleFactorySchema);

//        tLPAscriptionRuleFactorySchema = new LPAscriptionRuleFactorySchema();
//        tLPAscriptionRuleFactorySchema.setGrpContNo("86110020040120000104");
//        tLPAscriptionRuleFactorySchema.setGrpPolNo("111111");
//        tLPAscriptionRuleFactorySchema.setAscriptionRuleCode("A");
//        tLPAscriptionRuleFactorySchema.setAscriptionRuleName("dfsljfsajlkfjlk");
//        tLPAscriptionRuleFactorySchema.setRiskCode("211403");
//        tLPAscriptionRuleFactorySchema.setFactoryType("000005");
//        tLPAscriptionRuleFactorySchema.setOtherNo("672201");
//        tLPAscriptionRuleFactorySchema.setFactoryCode("000001");
//        tLPAscriptionRuleFactorySchema.setFactorySubCode("1");
//        tLPAscriptionRuleFactorySchema.setFactoryName("test");
//        tLPAscriptionRuleFactorySchema.setCalRemark(
//                "赔付比例：实际发生额大于等于 a  并且小于等于 b  时 赔付比例为 c");
//        tLPAscriptionRuleFactorySchema.setParams("100,200,200");
//        tLPAscriptionRuleFactorySet.add(tLPAscriptionRuleFactorySchema);

        GlobalInput tG = new GlobalInput();
        tG.ComCode = "8611";
        tG.ManageCom = "8611";
        tG.Operator = "ac";

        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tLPAscriptionRuleFactorySet);
        tVData.add("140110000000176");
//        tVData.add("86110020040990000041");
//    tVData.add("A");
//    tVData.add("select * from dual");

   

    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    
    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */


    public VData getResult()
    {
        return this.mResult;
    }
}
