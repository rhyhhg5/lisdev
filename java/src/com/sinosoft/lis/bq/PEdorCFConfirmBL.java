package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全变更投保人项目</p>
 * <p>Description: 保全确认</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorCFConfirmBL implements EdorConfirm
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mAppntNo = null;

    private LPPolSet mLPPolSet = null;
    
    private LPContSet mLPContSet = null;
    
    private LPInsuredSet mLPInsuredSet = null;
    
    private LPAppntSet mLPAppntSet = null;

    /**
     * 提交数据
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) data.getObjectByObjectName(
                    "LPEdorItemSchema", 0);
            mEdorNo = mLPEdorItemSchema.getEdorNo();
            mEdorType = mLPEdorItemSchema.getEdorType();
            mContNo = mLPEdorItemSchema.getContNo();
            
            //-----------POL------------
            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setEdorNo(mEdorNo);
            tLPPolDB.setEdorType(mEdorType);
            mLPPolSet = tLPPolDB.query();
            if (mLPPolSet.size() == 0)
            {
                mErrors.addOneError("未找到拆分后的险种信息！");
                return false;
            }
            //-----------POLEnd---------
//          -----------Cont------------
            LPContDB tLPContDB = new LPContDB();
            tLPContDB.setEdorNo(mEdorNo);
            tLPContDB.setEdorType(mEdorType);
            mLPContSet = tLPContDB.query();
            if (mLPContSet.size() == 0)
            {
                mErrors.addOneError("未找到拆分后的保单信息！");
                return false;
            }
            //-----------ContEnd---------
//          -----------Insured------------
            LPInsuredDB tLPInsuredDB = new LPInsuredDB();
            tLPInsuredDB.setEdorNo(mEdorNo);
            tLPInsuredDB.setEdorType(mEdorType);
            mLPInsuredSet = tLPInsuredDB.query();
            if (mLPInsuredSet.size() == 0)
            {
                mErrors.addOneError("未找到拆分后的保单信息！");
                return false;
            }
            //-----------InsuredEnd---------
//          -----------Appnt------------
            LPAppntDB tLPAppntDB = new LPAppntDB();
            tLPAppntDB.setEdorNo(mEdorNo);
            tLPAppntDB.setEdorType(mEdorType);
            mLPAppntSet = tLPAppntDB.query();
            if (mLPAppntSet.size() == 0)
            {
                mErrors.addOneError("未找到拆分后的保单信息！");
                return false;
            }
            
            //-----------AppntEnd---------
            mAppntNo = mLPPolSet.get(1).getAppntNo();
        }
        catch (Exception ex)
        {
            mErrors.addOneError("传入参数错误！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!validateEdorData())
        {
            return false;
        }
        
        if (!changeData())
        {
            return false;
        }
        
        return true;
    }

    /**
     * 使保全数据生效
     */
    private boolean validateEdorData()
    {
      for(int index=1;index<=mLPPolSet.size();index++)
      {
    	//先生成P表的备份信息
    	String[] lcTable =
        {"LCDuty", "LCPrem","LCGet","LCBnf"};
    	
    	String[] lpTable =
        {"LPDuty", "LPPrem","LPGet","LPBnf"};
    	
         for (int i = 0; i < lpTable.length; i++)
         {
        	 String sqlStr = "insert into " + lpTable[i] + " (select '" + mEdorNo +
             "','"+mEdorType+"'," + lcTable[i] + ".* from " + lcTable[i] +
             " where PolNo='" + mLPPolSet.get(index).getPolNo() + "')";
        	 
        	 System.out.println(sqlStr);
            
            mMap.put(sqlStr, "INSERT");
            
            String sqlStr2 = "Update " + lpTable[i] + " set modifydate=current date," +
            		" modifytime=current time  "+
            " where PolNo='" + mLPPolSet.get(index).getPolNo() + "' ";
   
            System.out.println(sqlStr2);
            
             mMap.put(sqlStr2, "UPDATE");
            
            String sqlStr3 = "Update " + lcTable[i] + " set contno = '" +
            		mLPPolSet.get(index).getContNo()+ "'," +
            	    " modifydate=current date,modifytime=current time  "+
            " where PolNo='" + mLPPolSet.get(index).getPolNo() + "' ";
            
            System.out.println(sqlStr3);
           
            mMap.put(sqlStr3, "UPDATE");
         }    	
       }    	
     
        return true;
    }
    
    /**
     * 使保全数据生效
     */
    private boolean changeData()
    {
    	 if (!changeContData())
         {
             return false;
         }
    	 if (!changePolData())
         {
             return false;
         }
    	 if (!changeInsuredData())
         {
             return false;
         }
    	 if (!changeAppntData())
         {
             return false;
         }
    
        return true;
    }
    
    private boolean changeContData()
    {
    	
//        for(int index=1;index<=mLPContSet.size();index++)
//        {
        	Reflections tRef = new Reflections();
        	LPContSchema tLPContSchema = new LPContSchema();
            LCContSchema tLCContSchema = new LCContSchema();
            
            tLPContSchema=mLPContSet.get(1).getSchema();
      
            //交换c表和p表

           tRef.transFields(tLCContSchema, tLPContSchema);

           System.out.println("check cont c:"+tLCContSchema.getContNo());
           
           tLCContSchema.setModifyDate(PubFun.getCurrentDate());
           tLCContSchema.setModifyTime(PubFun.getCurrentTime());

           mMap.put(tLCContSchema, "INSERT");
//        }
    
        
        LCContSchema aLCContSchema = new LCContSchema();
        LCContDB aLCContDB = new LCContDB();
        aLCContDB.setContNo(mLPEdorItemSchema.getContNo());
        
        if (!aLCContDB.getInfo())
        {
            mErrors.copyAllErrors(aLCContDB.mErrors);
            mErrors.addOneError(new CError("查询原保单信息失败！"));
            return false;
        }

        aLCContSchema = aLCContDB.getSchema();
        
        LCPolDB aLCPolDB = new LCPolDB();
        LCPolSet aLCPolSet = new LCPolSet();
        aLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
        
        aLCPolSet = aLCPolDB.query();
        
        for(int INDEX=1;INDEX<=aLCPolSet.size(); INDEX++)
        {
        	if(aLCContSchema.getInsuredNo().equals(aLCPolSet.get(INDEX).getInsuredNo()))
        	{
        		System.out.println("this is ok haha");
        	}
        	else
        	{
        		aLCContSchema.setInsuredNo(aLCPolSet.get(INDEX).getInsuredNo());
        		aLCContSchema.setInsuredName(aLCPolSet.get(INDEX).getInsuredName());
        		aLCContSchema.setInsuredBirthday(aLCPolSet.get(INDEX).getInsuredBirthday());
        		aLCContSchema.setInsuredIDNo("10000000");
        		aLCContSchema.setInsuredIDType("4");
        		aLCContSchema.setModifyDate(PubFun.getCurrentDate());
        		aLCContSchema.setModifyTime(PubFun.getCurrentTime());
        		mMap.put(aLCContSchema, "UPDATE");
        	}
        }

        
        return true;
    }
    
    private boolean changePolData()
    {
    	
        for(int index=1;index<=mLPPolSet.size();index++)
        {
        	Reflections tRef = new Reflections();
        	
        	LPPolSchema tLPPolSchema = new LPPolSchema();
            LCPolSchema tLCPolSchema = new LCPolSchema();
            LPPolSchema aLPPolSchema = new LPPolSchema();
            LCPolSchema aLCPolSchema = new LCPolSchema();
            
            tLPPolSchema=mLPPolSet.get(index).getSchema();
      
            //交换c表和p表

           tRef.transFields(tLCPolSchema, tLPPolSchema);
           
           System.out.println("check pol c:"+tLCPolSchema.getAppntNo());
           
           tLCPolSchema.setModifyDate(PubFun.getCurrentDate());
           tLCPolSchema.setModifyTime(PubFun.getCurrentTime());

           mMap.put(tLCPolSchema, "DELETE&INSERT");

        LCPolDB aLCPolDB = new LCPolDB();
        aLCPolDB.setPolNo(tLPPolSchema.getPolNo());
        if (!aLCPolDB.getInfo())
        {
            mErrors.copyAllErrors(aLCPolDB.mErrors);
            mErrors.addOneError(new CError("查询险种信息失败！"));
            return false;
        }

        //交换c表和p表
        aLCPolSchema = aLCPolDB.getSchema();
        tRef.transFields(aLPPolSchema, aLCPolSchema);
        
        System.out.println("check pol p:"+aLPPolSchema.getAppntNo());
        
        aLPPolSchema.setEdorNo(tLPPolSchema.getEdorNo());
        aLPPolSchema.setEdorType(tLPPolSchema.getEdorType());

        mMap.put(aLPPolSchema, "DELETE&INSERT");
        }
        return true;
    }
    
    private boolean changeInsuredData()
    {
    	for(int index=1;index<=mLPInsuredSet.size();index++)
        {
        	Reflections tRef = new Reflections();
        	LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
        	LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
        	LPInsuredSchema aLPInsuredSchema = new LPInsuredSchema();
        	LCInsuredSchema aLCInsuredSchema = new LCInsuredSchema();
            
        	tLPInsuredSchema=mLPInsuredSet.get(index).getSchema();
      
            //交换c表和p表

           tRef.transFields(tLCInsuredSchema, tLPInsuredSchema);

           System.out.println("check insured c:"+tLCInsuredSchema.getAppntNo());
           

           tLCInsuredSchema.setModifyDate(PubFun.getCurrentDate());
           tLCInsuredSchema.setModifyTime(PubFun.getCurrentTime());
           
           mMap.put(tLCInsuredSchema, "DELETE&INSERT");
           
           LCInsuredDB aLCInsuredDB = new LCInsuredDB();
           aLCInsuredDB.setInsuredNo(tLPInsuredSchema.getInsuredNo());
           aLCInsuredDB.setContNo(mContNo);
           
           if (!aLCInsuredDB.getInfo())
           {
               mErrors.copyAllErrors(aLCInsuredDB.mErrors);
               mErrors.addOneError(new CError("查询险种信息失败！"));
               return false;
           }

           //交换c表和p表
           aLCInsuredSchema = aLCInsuredDB.getSchema();
           tRef.transFields(aLPInsuredSchema, aLCInsuredSchema);
           
           System.out.println("check insured p:"+aLPInsuredSchema.getAppntNo());
           
           aLPInsuredSchema.setEdorNo(tLPInsuredSchema.getEdorNo());
           aLPInsuredSchema.setEdorType(tLPInsuredSchema.getEdorType());

           mMap.put(aLPInsuredSchema, "DELETE&INSERT");
           
           String wocao ="delete from lcinsured where contno='"+mContNo+"' and insuredno='"+tLPInsuredSchema.getInsuredNo()+"'";
           
           mMap.put(wocao, "DELETE");
        }
    
        return true;
    }
    
    private boolean changeAppntData()
    {
    	for(int index=1;index<=mLPAppntSet.size();index++)
        {
        	Reflections tRef = new Reflections();
        	
        	LPAppntSchema tLPAppntSchema = new LPAppntSchema();
        	LCAppntSchema tLCAppntSchema = new LCAppntSchema();
            
        	tLPAppntSchema=mLPAppntSet.get(index).getSchema();
      
            //交换c表和p表

           tRef.transFields(tLCAppntSchema, tLPAppntSchema);
           
           System.out.println("check appnt p"+tLCAppntSchema.getAppntNo());

           tLCAppntSchema.setModifyDate(PubFun.getCurrentDate());
           tLCAppntSchema.setModifyTime(PubFun.getCurrentTime());

           mMap.put(tLCAppntSchema, "INSERT");
        }
        return true;
    }
}
