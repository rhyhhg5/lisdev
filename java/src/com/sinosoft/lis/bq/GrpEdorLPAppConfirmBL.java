package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 特需医疗团体账户资金分配</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class GrpEdorLPAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = "";

    /** 磁盘导入数据  */
    private LPDiskImportSet mLPDiskImportSet = null;

    /** 团体合同号 */
    private String mGrpContNo = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mEdorNo = edorItem.getEdorNo();
        mEdorType = edorItem.getEdorType();
        mGrpContNo = edorItem.getGrpContNo();
        mLPDiskImportSet = CommonBL.getLPDiskImportSet
                (mEdorNo, mEdorType, mGrpContNo, BQ.IMPORTSTATE_SUCC);
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        mMap.put("delete from LPInsured where edorno='"+mEdorNo+"' and edortype='"+mEdorType+"'"
                 , "DELETE");
        setLPInsured();
        return true;
    }

    /**
     * 把LCInsure中的信息存入P表
     */
    private void setLPInsured() {
        for (int i = 1; i <= mLPDiskImportSet.size(); i++) {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSet tLCInsuredSet = getLCInsured(tLPDiskImportSchema);
            for (int j = 1; j <= tLCInsuredSet.size(); j++) {
                LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
                Reflections ref = new Reflections();
                ref.transFields(tLPInsuredSchema, tLCInsuredSet.get(j));
                tLPInsuredSchema.setBankCode(tLPDiskImportSchema.getBankCode());
                tLPInsuredSchema.setBankAccNo(tLPDiskImportSchema.getBankAccNo());
                tLPInsuredSchema.setAccName(tLPDiskImportSchema.getAccName());
                tLPInsuredSchema.setEdorNo(mEdorNo);
                tLPInsuredSchema.setEdorType(mEdorType);
                tLPInsuredSchema.setOperator(mGlobalInput.Operator);
                tLPInsuredSchema.setMakeDate(mCurrentDate);
                tLPInsuredSchema.setMakeTime(mCurrentTime);
                tLPInsuredSchema.setModifyDate(mCurrentDate);
                tLPInsuredSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPInsuredSchema, "INSERT");
            }
        }
    }

    /**
     *
     * @param aLPDiskImportSchema LPDiskImportSchema
     * @return LCInsuredSet
     */
    private LCInsuredSet getLCInsured(LPDiskImportSchema aLPDiskImportSchema)
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setGrpContNo(aLPDiskImportSchema.getGrpContNo());
        String insuredNo = aLPDiskImportSchema.getInsuredNo();
        if (insuredNo != null)
        {
            tLCInsuredDB.setInsuredNo(insuredNo);
            return tLCInsuredDB.query();
        }else
        {
            tLCInsuredDB.setName(aLPDiskImportSchema.getInsuredName());
            tLCInsuredDB.setSex(aLPDiskImportSchema.getSex());
            tLCInsuredDB.setBirthday(aLPDiskImportSchema.getBirthday());
            tLCInsuredDB.setIDType(aLPDiskImportSchema.getIDType());
            tLCInsuredDB.setIDNo(aLPDiskImportSchema.getIDNo());
            return tLCInsuredDB.query();
        }
    }
}
