package com.sinosoft.lis.bq;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 简易保全逻辑处理类</p>
 * <p>Description: 主要用于新契约投保时修改已生效保单的基本信息 </p>
 * <p>Copyright: Copyright (c) 2006 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class EasyEdorTbUI
{
    private EasyEdorTbBL mEasyEdorTbBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public EasyEdorTbUI()
    {
        mEasyEdorTbBL = new EasyEdorTbBL();
    }

    /**
     * 调用业务逻辑类
     * @param operator String
     * @return boolean
     */
    public boolean submitData(VData data, String operator)
    {
        if (!mEasyEdorTbBL.submitData(data, operator))
        {
            return false;
        }
        return true;
    }

    /**
     * 得到错误
     * @return String
     */
    public CErrors getErrors()
    {
        return mEasyEdorTbBL.mErrors;
    }

    public static void main(String args[])
     {
         String operator = BQ.EASYEDORTYPE_APPNT;
         GlobalInput gi = new GlobalInput();
         gi.Operator = "endor";
         gi.ManageCom = "86";

         TransferData td = new TransferData();
         td.setNameAndValue("ContNo", "00011848201");
         td.setNameAndValue("AppntNo", "000118482");
         //td.setNameAndValue("InsuredNo", "000000208");
         td.setNameAndValue("CustomerNo", "000118482");
         td.setNameAndValue("Name", "曹斌右");
         td.setNameAndValue("IDType", "0");
         td.setNameAndValue("IDNo", "310102196211010419");
         td.setNameAndValue("PostalAddress", "上海市浦东新区南码头路1675弄40号601室");
         td.setNameAndValue("ZipCode", "200125");

         //TransferData td = new TransferData();
         //td.setNameAndValue("ContNo", "13000000253");
         //td.setNameAndValue("AppntNo", "000000267");
         //td.setNameAndValue("InsuredNo", "000000267");
         //td.setNameAndValue("LCBnfSet", new LCBnfSet());

         VData data = new VData();
         data.add(gi);
         data.add(td);
         EasyEdorTbUI tEasyEdorTbUI = new EasyEdorTbUI();
         if (!tEasyEdorTbUI.submitData(data, operator))
         {
             System.out.println(tEasyEdorTbUI.getErrors().getFirstError());
         }
     }
}
