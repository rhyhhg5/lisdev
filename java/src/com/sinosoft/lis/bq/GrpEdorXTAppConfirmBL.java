package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 团险协议退保项目理算处理类
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GrpEdorXTAppConfirmBL implements EdorAppConfirm
{
    /**全局信息*/
    private GlobalInput mGI = null;
    /**团单协议退保项目*/
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    /**处理后的数据*/
    private VData mResult = new VData();
    private BqCalBL mBqCalBL = new BqCalBL();
    private String mFinType = null;
    private double mGetMoney = 0;  //本次退费

    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    private MMap map = new MMap();

    public GrpEdorXTAppConfirmBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {
        //接受传入的数据
        if(!getInputData(cInputData))
        {
            return false;
        }
        //业务逻辑的处理
        if(!dealData())
        {
            return false;
        }

        prepareOutputData();

        return true;
    }

    /**
     * 接受传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.
                               getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mGI = ((GlobalInput) cInputData.
                        getObjectByObjectName("GlobalInput", 0));
        if(mLPGrpEdorItemSchema == null || mGI == null)
        {
            mErrors.addOneError("传入的数据不完整");
            return false;
        }

        return true;
    }

    /**
     * 业务逻辑的处理
     * @return boolean
     */
    private boolean dealData()
    {
        //生成团单协议退保退费财务接口
        if(!setXTFee())
        {
            return false;
        }

        changeState();

        return true;
    }

    /**
     * 生成团单协议退保退费财务接口
     * @return boolean
     */
    private boolean setXTFee()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);

        //查询团险协议退费金额
        EdorItemSpecialData tEdorItemSpecialData =
            new EdorItemSpecialData(mLPGrpEdorItemSchema);
        tEdorItemSpecialData.query();
        LPEdorEspecialDataSet set = tEdorItemSpecialData.getSpecialDataSet();

        for(int i = 1; i <= set.size(); i++)
        {
            LPEdorEspecialDataSchema schema = set.get(i);
            if(!schema.getDetailType().equals(BQ.DETAILTYPE_XTFEEG))
            {
                continue;
            }

            String[] contPlanCodeRiskCode = schema.getPolNo().split(",");
            double xtFee = Double.parseDouble(schema.getEdorValue());

            double sumPrem = getSumPrem(contPlanCodeRiskCode[0],
                             contPlanCodeRiskCode[1]);


            //查询保障计划下险种信息
            tLCPolDB.setContPlanCode(contPlanCodeRiskCode[0]);
            tLCPolDB.setRiskCode(contPlanCodeRiskCode[1]);
            LCPolSet tLCPolSet = tLCPolDB.query();

            double xtFeeUsed = 0;
            for(int j = 1; j < tLCPolSet.size(); j++)
            {
            	double polXTFee=0;
                LCPolSchema tLCPolSchema = tLCPolSet.get(j);
                if(sumPrem==0){
                	 polXTFee = 0;
                }else{
                	
                     polXTFee = CommonBL.carry(xtFee
                                                 * tLCPolSchema.getPrem() / sumPrem);
                }
                if(!setOnePolFinanceData(tLPEdorItemSchema, tLCPolSchema,
                                         polXTFee))
                {
                    return false;
                }
                xtFeeUsed += polXTFee;
            }

            //为处理四舍五入导致的误差，最后一个险种单独处理
            if(!setOnePolFinanceData(tLPEdorItemSchema,
                                     tLCPolSet.get(tLCPolSet.size()),
                                     CommonBL.carry(xtFee - xtFeeUsed)))
            {
                return false;
            }

            map.put(mLJSGetEndorseSet, SysConst.DELETE_AND_INSERT);
        }

        return true;
    }

    /**
     * setOnePolFinanceData
     * 生成险种tLCPolSchema的退费polXTFee财务数据
     * @param tLCPolSchema LCPolSchema：待处理的险种
     * @param polXTFee double，退费
     */
    private boolean setOnePolFinanceData(LPEdorItemSchema tLPEdorItemSchema,
                                         LCPolSchema tLCPolSchema,
                                         double polXTFee)
    {
    	mFinType = getRiskCodeFinType(tLCPolSchema.getRiskCode());
        if(mFinType == null)
        {
            BqCalBL tBqCalBl = new BqCalBL();
            mFinType = tBqCalBl.getFinType(
                mLPGrpEdorItemSchema.getEdorType(), "TB",
                mLPGrpEdorItemSchema.getGrpContNo());
            if(mFinType.equals(""))
            {
                mErrors.addOneError("缺少保全财务类型编码!");
                return false;
            }
        }

        LJSGetEndorseSchema schema
            = mBqCalBL.initLJSGetEndorse(tLPEdorItemSchema, tLCPolSchema, null,
                                         mFinType, polXTFee, mGI);
        mLJSGetEndorseSet.add(schema);

        mGetMoney += polXTFee;

        return true;
    }

    /**
     * getSumPrem
     * 查询保障计划contPlanCode下riskCode险种的期交保费和
     * @param string contPlanCode
     * @param string1
     * @return double
     */
    private double getSumPrem(String contPlanCode, String riskCode)
    {
        ExeSQL tExeSQL = new ExeSQL();

        String sql = "select sum(prem) "
                     + "from LCPol "
                     + "where GrpContNo = '"
                     + mLPGrpEdorItemSchema.getGrpContNo() + "' "
                     + "   and ContPlanCode = '" + contPlanCode + "' "
                     + "   and RiskCode = '" + riskCode + "' ";
        String rs = tExeSQL.getOneValue(sql);

        if(tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "GrpEdorXTAppConfirmBL";
            tError.functionName = "getSumPrem";
            tError.errorMessage = "查询保障计划总保费出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return -1;
        }

        if(rs.equals("") || rs.equals("null"))
        {
            return 0.0;
        }

        return Double.parseDouble(rs);
    }

    /**
     * 改变业务状态
     */
    private void changeState()
    {
        mLPGrpEdorItemSchema.setGetMoney(mGetMoney);
        mLPGrpEdorItemSchema.setEdorState(BQ.EDORSTATE_CAL);
        mLPGrpEdorItemSchema.setOperator(mGI.Operator);
        mLPGrpEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
    }

    /**
     * 将处理后的数据放入到VData容器中，为输出数据处理结果做准备
     */
    private void prepareOutputData()
    {
        MMap map = new MMap();
        map.put(mLJSGetEndorseSet, "DELETE&INSERT");
        map.put(mLPGrpEdorItemSchema, "UPDATE");

        mResult.clear();
        mResult.add(map);
    }

    public VData getResult()
    {
        return mResult;
    }
    
    /**
     * 1、	一年期及以内险种 对应 冲退保费 TF。
     * 2、	一年期以上险种 对应 冲退保金 TB。
     * @param String
     * @return String
     */
     private String getRiskCodeFinType(String riskCode) {

            //通过险种代码查询险种期限
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(riskCode);
            if (!tLMRiskAppDB.getInfo()) {
                return null;
            }
            System.out.println("L长S短M一年:"+tLMRiskAppDB.getRiskPeriod());
            //一年期及以内险种冲退保费 TF。
            if (tLMRiskAppDB.getRiskPeriod().equals("M")
                || tLMRiskAppDB.getRiskPeriod().equals("S")) {
                return BQ.FEEFINATYPE_TF;
            }
            //一年期以上险种冲退保金 TB
            else if (tLMRiskAppDB.getRiskPeriod().equals("L")) {
                return BQ.FEEFINATYPE_TB;
            }
        return null;
    }


    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        LPGrpEdorItemDB db = new LPGrpEdorItemDB();
        db.setEdorNo("20070207000004");
        db.setEdorAcceptNo(db.getEdorNo());
        db.setEdorType("XT");

        VData v = new VData();
        v.add(g);
        v.add(db.query().get(1));

        GrpEdorXTAppConfirmBL bl = new GrpEdorXTAppConfirmBL();
        if(!bl.submitData(v, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}
